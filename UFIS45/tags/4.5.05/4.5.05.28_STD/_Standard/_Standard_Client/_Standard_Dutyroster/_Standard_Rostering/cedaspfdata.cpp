// CedaSpfData.cpp
 
#include <stdafx.h>

CedaSpfData ogSpfData;

void ProcessSpfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareSurnAndPrioAndTimes(const SPFDATA **e1, const SPFDATA **e2);


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSpfData::CedaSpfData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SPFDataStruct
	BEGIN_CEDARECINFO(SPFDATA,SPFDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Fctc,"CODE")
		FIELD_CHAR_TRIM	(Prio,"PRIO")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SPFDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SPFDataRecInfo)/sizeof(SPFDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SPFDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SPF");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,CODE,PRIO,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
	Register();
}

//---------------------------------------------------------------------------------------------------------

void CedaSpfData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSpfData::Register(void)
{
	DdxRegister((void *)this,BC_SPF_CHANGE,	"SPFDATA", "Spf-changed",	ProcessSpfCf);
	DdxRegister((void *)this,BC_SPF_NEW,	"SPFDATA", "Spf-new",		ProcessSpfCf);
	DdxRegister((void *)this,BC_SPF_DELETE,	"SPFDATA", "Spf-deleted",	ProcessSpfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSpfData::~CedaSpfData(void)
{
	TRACE("CedaSpfData::~CedaSpfData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSpfData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaSpfData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSpfData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Spf: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		SPFDATA *prlSpf = new SPFDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSpf)) == true)
		{
			if(prlSpf->Vpfr.GetStatus() != COleDateTime::valid || prlSpf->Vpto.GetStatus() == COleDateTime::invalid)
			{
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlSpf);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlSpf;
				continue;
			}
			
			omData.Add(prlSpf);//Update omData
			omUrnoMap.SetAt((void *)prlSpf->Urno,prlSpf);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlSpf);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlSpf;
		}
	}
	TRACE("Read-Spf: %d gelesen\n",ilCountRecord-1);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}
	ClearFastSocketBuffer();	
	
	CheckValidSpfData();

	// wir sortieren alles nach Surn & Prio & Time - das beschleunigt das Lesen zigfach
	omData.Sort(CompareSurnAndPrioAndTimes);

    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSpfData::Insert(SPFDATA *prpSpf)
{
	prpSpf->IsChanged = DATA_NEW;
	if(Save(prpSpf) == false) return false; //Update Database
	InsertInternal(prpSpf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSpfData::InsertInternal(SPFDATA *prpSpf)
{
	ogDdx.DataChanged((void *)this, SPF_NEW,(void *)prpSpf ); //Update Viewer
	omData.Add(prpSpf);//Update omData
	omUrnoMap.SetAt((void *)prpSpf->Urno,prpSpf);
	// und wir sortieren alles nach Surn & Prio & Time - das beschleunigt das Lesen zigfach
	omData.Sort(CompareSurnAndPrioAndTimes);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSpfData::Delete(long lpUrno)
{
	SPFDATA *prlSpf = GetSpfByUrno(lpUrno);
	if (prlSpf != NULL)
	{
		prlSpf->IsChanged = DATA_DELETED;
		if(Save(prlSpf) == false) return false; //Update Database
		DeleteInternal(prlSpf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSpfData::DeleteInternal(SPFDATA *prpSpf)
{
	ogDdx.DataChanged((void *)this,SPF_DELETE,(void *)prpSpf); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSpf->Urno);
	int ilSpfCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilSpfCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpSpf->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSpfData::Update(SPFDATA *prpSpf)
{
	if (GetSpfByUrno(prpSpf->Urno) != NULL)
	{
		if (prpSpf->IsChanged == DATA_UNCHANGED)
		{
			prpSpf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSpf) == false) return false; //Update Database
		UpdateInternal(prpSpf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSpfData::UpdateInternal(SPFDATA *prpSpf)
{
	SPFDATA *prlSpf = GetSpfByUrno(prpSpf->Urno);
	if (prlSpf != NULL)
	{
		*prlSpf = *prpSpf; //Update omData
		ogDdx.DataChanged((void *)this,SPF_CHANGE,(void *)prlSpf); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SPFDATA *CedaSpfData::GetSpfByUrno(long lpUrno)
{
	SPFDATA  *prlSpf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSpf) == TRUE)
	{
		return prlSpf;
	}
	return NULL;
}

/*****************************************************************************
opDate hat immer nur das Datum, die Zeit ist immer 0
vergleicht den Funktionscode opPfc mit dem des MAs lpSurn f�r das Datum opDate
bool bpMainFuncOnly - true - nur Stammfunktionen, sonst alle
R�ckgabe: true - Funktionen sind gleich, sonst false
*****************************************************************************/
bool CedaSpfData::ComparePfcBySurnWithTime(CString opPfc, long lpSurn, COleDateTime opDate, bool bpMainFuncOnly)
{
	if(!lpSurn) return false;
	CCSPtrArray<SPFDATA> olSpfData;
	GetSpfArrayBySurnWithTime(olSpfData,lpSurn,opDate,bpMainFuncOnly);

	for(int i=0; i<olSpfData.GetSize(); i++)
	{
		if(opPfc == olSpfData[i].Fctc)
		{
			olSpfData.RemoveAll();
			return true;
		}
	}
	olSpfData.RemoveAll();
	return false;
}

/*****************************************************************************
Sehr schnelles Suchen vom ersten Datensatz mit der angegebenen Surn
! wir nutzen die Tatsache, das omData nach Surn & Prio & Time sortiert ist !
*****************************************************************************/
int CedaSpfData::FindFirstOfSurn(long lpSurn)
{
	int ilSize = omData.GetSize();
	if(!ilSize) 
		return -1;
	
	int ilSearch;

	if(omData[ilSize-1].Surn != omData[0].Surn)
	{
		if(ilSize > 16)
		{
			// rekursives Suchen
			ilSearch = ilSize >> 1;
			int ilUp, ilDown;
			
			for(ilUp = ilSize-1, ilDown = 0;;)
			{
				if(lpSurn == omData[ilSearch].Surn)
				{
					if(!ilSearch || lpSurn != omData[ilSearch-1].Surn)
					{
						// gefunden!
						return ilSearch;
					}

					// lineares Suchen nach unten
					for(ilSearch--; ilSearch>=0;ilSearch--)
					{
						if(!ilSearch || lpSurn != omData[ilSearch-1].Surn)
						{
							// gefunden!
							return ilSearch;
						}
					}
				}
				
				if(ilUp-ilDown <= 1)
				{
					if(ilUp == ilDown)
					{
						// kein Surn in der Liste vorhanden!
						return -1;
					}

					if(lpSurn == omData[ilUp].Surn)
						return ilUp;
					else if(lpSurn == omData[ilDown].Surn)
						return ilDown;
					else 
						return -1;
				}

				if(lpSurn < omData[ilSearch].Surn)
				{
					ilUp = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
				else //  if(lpSurn > omData[ilSearch].Surn)
				{
					ilDown = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
			}
		}
		else
		{
			// lineares Suchen
			for(int ilSearch=0; ilSearch<ilSize; ilSearch++)
			{
				if(omData[ilSearch].Surn == lpSurn)
					return ilSearch;
			}
		}
	}
	else
	{
		return 0;	// alle gleich
	}
	
	return -1;	// kein gefunden!
}

/*****************************************************************************
opDate hat nur das Datum
gibt eine SPF-Liste zur�ck f�r den angegebenen MA lpSurn und das Datum opDate,
und R�ckgabewert: Anzahl der gespeicherten Datens�tze
bool bpMainFuncOnly - true - nur Stammfunktionen, sonst alle
Wenn alle Funktionen, dann sind sie nach Prio & Zeit sortiert
*****************************************************************************/
int CedaSpfData::GetSpfArrayBySurnWithTime(CCSPtrArray<SPFDATA> &opSpfData, long lpSurn, COleDateTime opDate, bool bpMainFuncOnly)
{
	if(opDate.GetStatus() != COleDateTime::valid) return 0;

	SPFDATA *prlSpf = NULL;
	SPFDATA *prlSpfLastPrio1 = NULL;
	int ilCount = FindFirstOfSurn(lpSurn);
	if(ilCount == -1) return 0;	// lpSurn nicht gefunden
	CString olFuncP1("");

	for(; ilCount < omData.GetSize(); ilCount++)
	{
		prlSpf = &omData[ilCount];

		if(prlSpf->Surn > lpSurn) 
			break;		// omData ist ja nach Surn-Prio-Vpfr sortiert
		
		if(bpMainFuncOnly && strcmp(prlSpf->Prio, "1"))
			break;	// 4. Diese Funktion hat Prio > 1, aber wir brauchen nur die Stammfunktionen (mit Prio == 1)

		if(prlSpf->Vpfr.GetStatus() != COleDateTime::valid) 
			continue;	// 1. Defekter Datensatz
		
		if(prlSpf->Vpfr > opDate) 
			continue;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opDate, fortsetzen
		
		switch(prlSpf->Vpto.GetStatus())
		{
		default:
			// case COleDateTime::invalid:
			continue;	// gibts nicht
		case COleDateTime::valid:
			if(prlSpf->Vpto < opDate) 
				continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opDate
			break;
		case COleDateTime::null:
			break;
		}
		
		if(!strcmp(prlSpf->Prio, "1"))
		{
			// nur der letzter Prio1 ist relevant
			prlSpfLastPrio1 = prlSpf;
		}
		else
		{
			if(prlSpfLastPrio1 != 0)
			{
				// zur Liste addieren
				opSpfData.Add(prlSpfLastPrio1);
				prlSpfLastPrio1 = 0;
			}

			// zur Liste addieren
			opSpfData.Add(prlSpf);
		}
	}

	if(prlSpfLastPrio1 != 0)
	{
		// zur Liste addieren
		opSpfData.Add(prlSpfLastPrio1);
	}
	
	return opSpfData.GetSize();
}

/****************************************************************************
aus der Liste opSpfData bildet ein String mit allen Funktionen, angefangen von ipStartPrio 
ipStartPrio startet mit 1
in der Form: "FUNC1 (1), FUNC2 (2), FUNC3 (2), FUNC4 (3)"
****************************************************************************/
CString CedaSpfData::FormatFuncs(CCSPtrArray<SPFDATA> &opSpfData, int ipStartPrio)
{
	CString olList, olSingleText;

	int ilSize = opSpfData.GetSize();
	ipStartPrio--;
	ipStartPrio = max(ipStartPrio,0);
	for(; ipStartPrio<ilSize; ipStartPrio++)
	{
		olSingleText.Format("%s (%s)", opSpfData[ipStartPrio].Fctc, opSpfData[ipStartPrio].Prio);
		olList += olSingleText;
		if(ipStartPrio < ilSize-1)
			olList += ", ";
	}
	if(olList.IsEmpty())
		olList = "-";
	return olList;
}

/*****************************************************************************
R�ckgabe: die allerneueste (aktuelle) Stammfunktion des Mitarbeiters
*****************************************************************************/
CString CedaSpfData::GetMainPfcBySurnWithTime(long lpSurn, COleDateTime opDate)
{

	SPFDATA *prlSpf = NULL;
	CString olPfc = "";
	int ilCount = FindFirstOfSurn(lpSurn);
	if(ilCount == -1 || opDate.GetStatus() != COleDateTime::valid) 
		return olPfc;

	for(; ilCount < omData.GetSize(); ilCount++)
	{
		prlSpf = &omData[ilCount];

		if(prlSpf->Surn > lpSurn) 
			break;		// omData ist ja nach Surn-Prio-Vpfr sortiert
		
		if(prlSpf->Vpfr.GetStatus() != COleDateTime::valid) 
			continue;	// 1. Defekter Datensatz
		
		if(prlSpf->Vpfr > opDate) 
			break;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opDate, kein Datensatz mehr kann uns interessieren
		
		switch(prlSpf->Vpto.GetStatus())
		{
		default:
			// case COleDateTime::invalid:
			continue;	// gibts nicht
		case COleDateTime::valid:
			if(prlSpf->Vpto < opDate) 
				continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opDate
			break;
		case COleDateTime::null:
			break;
		}
		
		if(strcmp(prlSpf->Prio, "1"))
			break;	// 4. Diese Funktion hat Prio > 1 - Ende der Prio1 Funktionsliste: dadurch erziehlen wir,
					// dass in olPfc die allerneueste Prio1-Funktion bleibt
		
		// gefunden, einsetzen/�berschreiben
		olPfc = prlSpf->Fctc;
	}
	return olPfc;	
}

/*****************************************************************************
Gibt eine Liste aller MA's aus, die in dem Zeitraum opStart -> opEnd eine der in der opPfcMap aufgelisteten Funktionen haben
bool bpSelectAllFunc=false - nur nach Stammfunktion filtern, =true - nach allen aktuellen Funktionen filtern
bool bpGetAllMAWithoutFunc - wenn true, alle MA ohne Funktion sollen auch rein
*****************************************************************************/
void CedaSpfData::GetStfuByPfcListWithTime(CMapStringToPtr& opPfcMap, COleDateTime opStart, COleDateTime opEnd, CMapPtrToPtr& opStfUrnoMap, bool bpSelectAllFunc, bool bpGetAllMAWithoutFunc)
{
	if(opPfcMap.IsEmpty() || opStart.GetStatus() != COleDateTime::valid || opEnd.GetStatus() != COleDateTime::valid || opStart > opEnd)
		return;
	
	CString olTmpCode, olUrnoString;
	COleDateTime olTmpStart,olTmpEnd;
	
	SPFDATA *prlSpf = NULL;
	
	CCSPtrArray<SPFDATA> olSpfData;

	CMapPtrToPtr olSpfUrnoWithFunc;	// alle Mitarbeiter mit beliebiger Funktion in diesem Zeitraum
	void  *prlVoid = NULL;
	long llSkipSurn = 0;
	// omData ist nach Surn/Prio/Time sortiert
	for(int i=0; i < omData.GetSize(); i++)
	{
		prlSpf = &omData[i];

		if(llSkipSurn == prlSpf->Surn)
			continue;	// den haben wir schon bearbeitet

		if(prlSpf->Vpfr.GetStatus() != COleDateTime::valid) 
			continue;	// 1. Defekter Datensatz

		if(prlSpf->Vpfr > opEnd) 
			continue;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opEnd

		switch(prlSpf->Vpto.GetStatus())
		{
		default:
			// case COleDateTime::invalid:
			continue;	// gibts nicht
		case COleDateTime::valid:
			if(prlSpf->Vpto < opStart) 
				continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opStart
			break;
		case COleDateTime::null:
			break;
		}
		
		if(!bpSelectAllFunc && strcmp(prlSpf->Prio, "1"))
		{
			// Prio != 1
			continue;	// 4. Wir brauchen nur die Stammfunktionen (mit Prio == 1)
		}

		// also, wenn die Funktion passt, geh�rt der MA immer dazu
		if(opPfcMap.Lookup(prlSpf->Fctc,(void *&)prlVoid) == TRUE)
		{
			// Funktion passt
			opStfUrnoMap.SetAt((void*)prlSpf->Surn,(void *&)prlVoid);
			llSkipSurn = prlSpf->Surn;	// mit diesem MA ist schon klar
		}
	
		if(bpGetAllMAWithoutFunc)
		{
			olSpfUrnoWithFunc.SetAt((void*)prlSpf->Surn,NULL);	// ersten immer spechern
		}
	}

	// olSpfUrnoWithFunc hat alle MAs mit einer Funktion in der gesuchten Zeit
	olSpfData.RemoveAll();
	
	if(bpGetAllMAWithoutFunc)
	{
		// Ohne Funktion
		// Alle Mitarbeiter sammeln, die nicht in der olSpfUrnoWithFunc gespeichert sind
		STFDATA *prlStf = NULL;
		for(POSITION rlPos = ogStfData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			ogStfData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlStf);
			if(!olSpfUrnoWithFunc.Lookup((void*)llUrno,(void *&)prlVoid))
			{
				opStfUrnoMap.SetAt((void*)llUrno,NULL);
			}
		}
	}
	olSpfUrnoWithFunc.RemoveAll();
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSpfData::ReadSpecialData(CCSPtrArray<SPFDATA> *popSpf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSpf != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			SPFDATA *prpSpf = new SPFDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSpf,CString(pclFieldList))) == true)
			{
				if(prpSpf->Vpfr.GetStatus() != COleDateTime::valid || prpSpf->Vpto.GetStatus() == COleDateTime::invalid)
				{
					TRACE("Read-Spf: %d gelesen, FEHLER IM DATENSATZ, Surn=%ld\n",ilCountRecord-1,prpSpf->Surn);
					delete prpSpf;
					continue;
				}
			
				popSpf->Add(prpSpf);
			}
			else
			{
				delete prpSpf;
			}
		}
		if(popSpf->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSpfData::Save(SPFDATA *prpSpf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSpf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSpf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSpf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSpf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSpf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSpf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Spf-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSpfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSpfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSpfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSpfData;
	prlSpfData = (struct BcStruct *) vpDataPointer;
	SPFDATA *prlSpf;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlSpfData->Cmd, prlSpfData->Object, prlSpfData->Twe, prlSpfData->Selection, prlSpfData->Fields, prlSpfData->Data,prlSpfData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_SPF_CHANGE:
		prlSpf = GetSpfByUrno(GetUrnoFromSelection(prlSpfData->Selection));
		if(prlSpf != NULL)
		{
			GetRecordFromItemList(prlSpf,prlSpfData->Fields,prlSpfData->Data);
			UpdateInternal(prlSpf);
			break;
		}
	case BC_SPF_NEW:
		prlSpf = new SPFDATA;
		GetRecordFromItemList(prlSpf,prlSpfData->Fields,prlSpfData->Data);
		InsertInternal(prlSpf);
		break;
	case BC_SPF_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlSpfData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlSpfData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlSpf = GetSpfByUrno(llUrno);
			if (prlSpf != NULL)
			{
				DeleteInternal(prlSpf);
			}
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndPrioAndTimes(const SPFDATA **e1, const SPFDATA **e2)
{
	if((**e1).Surn>(**e2).Surn) 
		return 1;
	else if((**e1).Surn<(**e2).Surn) 
		return -1;

	int iCmp = strcmp((**e1).Prio,(**e2).Prio);
	if(iCmp > 0)
		return 1;
	else if(iCmp < 0)
		return -1;

	if((**e1).Vpfr>(**e2).Vpfr) 
		return 1;
	else if((**e1).Vpfr<(**e2).Vpfr) 
		return -1;

	return 0;
}


/***********************************************************************************************
CheckValidSpfData: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und SPFs mit doppelten Prio 1
und l�scht alle defekten Datens�tze
***********************************************************************************************/
CedaSpfData::CheckValidSpfData()
{
	CCS_TRY;

	SPFDATA *prlSpf = NULL;
	
	for(int i=0; i < omData.GetSize(); i++)
	{
		prlSpf = &omData[i];

		if (strlen(prlSpf->Fctc) > FCTC_LEN || strlen(prlSpf->Prio) > PRIO_LEN)
		{
#ifdef _DEBUG
			// Detaillierte Ausgabe
			if( strlen(prlSpf->Fctc) > FCTC_LEN			)	TRACE("Defekte SPFDATA l�schen: strlen(prlSpf->Fctc) > FCTC_LEN			\n");
			if( strlen(prlSpf->Prio) > PRIO_LEN			)	TRACE("Defekte SPFDATA l�schen: strlen(prlSpf->Prio) > PRIO_LEN			\n");
#endif
			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prlSpf);
				WriteInRosteringLog(LOGFILE_TRACE);
			}

			omUrnoMap.RemoveKey((void *)prlSpf->Urno);
			omData.DeleteAt(i);
			i--;
			continue;	// 1. Defekter Datensatz
		}
			
		
		if(prlSpf->Vpfr.GetStatus() != COleDateTime::valid) 
		{
			TRACE("Defekte SPFDATA l�schen: Vpfr.GetStatus() != COleDateTime::valid\n");
			TraceSpf(prlSpf, (LPCTSTR)"SPF Defekt: ");

			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prlSpf, "Vpfr is not valid");
				WriteInRosteringLog(LOGFILE_TRACE);
			}

			omUrnoMap.RemoveKey((void *)prlSpf->Urno);
			omData.DeleteAt(i);
			i--;
			continue;	// 1. Defekter Datensatz
		}

		// Surn pr�fen
		if(!ogStfData.GetStfByUrno(prlSpf->Surn))
		{
			TRACE("SPFTAB.SURN in STFTAB not found\n");
			TraceSpf(prlSpf, (LPCTSTR)"SPF defekt: ");

			CString olErr;
			olErr.Format("Function Connection Table (SPFTAB) URNO '%d' is defect, Employee with URNO '%d' in STFTAB not found\n",
				prlSpf->Urno, prlSpf->Surn);
			ogErrlog += olErr;

			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prlSpf, "SPFTAB.SURN in STFTAB not found");
				WriteInRosteringLog(LOGFILE_TRACE);
			}

			omUrnoMap.RemoveKey((void *)prlSpf->Urno);
			omData.DeleteAt(i);
			i--;
			continue;	// 1. Defekter Datensatz
		}
		
		// Fctc pr�fen
		if(	strlen(prlSpf->Fctc) > 0 && !ogPfcData.GetPfcByFctc(CString(prlSpf->Fctc)))
		{
			TRACE("SPFTAB.FCTC in PFCTAB not found\n");
			TraceSpf(prlSpf, (LPCTSTR)"SPF defekt: ");

			STFDATA* prlStf = ogStfData.GetStfByUrno(prlSpf->Surn);

			CString olErr;
			olErr.Format("Function '%s' for %s,%s in Function Table not found\n",  
				prlSpf->Fctc, prlStf->Lanm, prlStf->Finm);
			ogErrlog += olErr;

			if(IsTraceLoggingEnabled())
			{
				GetDefectDataString(ogRosteringLogText, (void*)prlSpf, "SPFTAB.FCTC in PFCTAB not found");
				WriteInRosteringLog(LOGFILE_TRACE);
			}

			omUrnoMap.RemoveKey((void *)prlSpf->Urno);
			omData.DeleteAt(i);
			i--;
			continue;	// 1. Defekter Datensatz
		}

		// Prio pr�fen und korrigieren
		if(!strlen(prlSpf->Prio) || !atoi(prlSpf->Prio))
		{
			strcpy(prlSpf->Prio, "1");
		}
	}

	CCS_CATCH_ALL;
}

/********************************************************************************
Debug Trace Funktion
********************************************************************************/
#ifdef _DEBUG

bool TraceSpf(SPFDATA *popSpf, LPCTSTR popStr)
{
	// melden
	CString olVpfr, olVpto;
	
	if(popSpf->Vpfr.GetStatus() == COleDateTime::valid)
		olVpfr = popSpf->Vpfr.Format("%d.%m.%y %H:%M");
	else olVpfr = "ung�ltig      ";
	
	if(popSpf->Vpto.GetStatus() == COleDateTime::valid)
		olVpto = popSpf->Vpto.Format("%d.%m.%y %H:%M");
	else olVpto = "ung�ltig      ";

	// Name
	CString olName = "";
	STFDATA *prlStf;
	prlStf = ogStfData.GetStfByUrno(popSpf->Surn);
	
	if (prlStf > NULL)
	{
		olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
	}

	TRACE("%s \
		%s:  \
		Surn: %d \
		Urno: %d \
		Fctc: %s \
		Prio: %s \
		Vpfr: %s \
		Vpto: %s \n",
		popStr,
		olName,
		popSpf->Surn, 
		popSpf->Urno, 
		popSpf->Fctc, 
		popSpf->Prio, 
			  olVpfr, 
			  olVpto); 
	return false;
}
#endif _DEBUG



















