// SetupDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SetupDlg 
//---------------------------------------------------------------------------

SetupDlg::SetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SetupDlg)
	m_DutyRoster  = -1;
	m_ShiftRoster = -1;
	m_bVorlauf = FALSE;
	//}}AFX_DATA_INIT
}

void SetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SetupDlg)
	DDX_Control(pDX, IDC_SL_RELATION,		  m_Relation);
	DDX_Control(pDX, IDC_SL_TOTAL_WIDTH,	  m_TotalWidth);
	DDX_Control(pDX, IDC_STATICTEST,		  m_StaticTest);
	DDX_Control(pDX, IDC_SL_STATIC_PLANDAYS,  m_PlanDays);
	DDX_Control(pDX, IDC_S_USER,			  m_User);
	DDX_Control(pDX, IDC_SL_STATIC_COLUMNS,   m_Staticcolumns);
	DDX_Control(pDX, IDC_SL_DEBIT_ROWS,		  m_Debitrows);
	DDX_Control(pDX, IDC_S_ROWS,			  m_S_Debitrows);
	DDX_Control(pDX, IDC_S_COLUMNS,		      m_S_Staticcolumns);
	DDX_Control(pDX, IDC_S_PLANDAYS,		  m_S_Plandays);
	DDX_Radio(pDX, IDC_RADIO1,			      m_Monitors);
	DDX_Radio(pDX, IDC_RADIO4,			      m_Sortstatistic);
	DDX_Radio(pDX, IDC_DUTYROSTER1,		      m_DutyRoster);
	DDX_Radio(pDX, IDC_SHIFTROSTER1,	      m_ShiftRoster);
	DDX_Control(pDX, IDC_M_VIEWPL,	          m_ViewPl);
	DDX_Control(pDX, IDC_M_VIEWMI,			  m_ViewMi);
	DDX_Check(pDX, IDC_VORLAUF,				  m_bVorlauf);
	DDX_Check(pDX, IDC_ScrollTo,			  m_bScroll);
	//}}AFX_DATA_MAP
}

SetupDlg::~SetupDlg()
{
	omMonitorButtons1.RemoveAll();
	omMonitorButtons2.RemoveAll();
	omMonitorButtons3.RemoveAll();
}


BEGIN_MESSAGE_MAP(SetupDlg, CDialog)
	//{{AFX_MSG_MAP(SetupDlg)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	ON_BN_CLICKED(IDC_RADIO5, OnRadio5)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten SetupDlg 
//---------------------------------------------------------------------------
BOOL SetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);
	CString olTmp;

	// Setzen der statischen Texte
	SetStatics();
	 
	CWnd *polWnd;
//Mon 1
	polWnd = GetDlgItem(IDC_DUTYROSTER1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SHIFTROSTER1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
// Mon2 
	polWnd = GetDlgItem(IDC_DUTYROSTER2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SHIFTROSTER2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
//Mon 3
	polWnd = GetDlgItem(IDC_DUTYROSTER3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SHIFTROSTER3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}

	//-------------------------------------------------------------------
	// Daten aus der CEDACFG auslesen
	//-------------------------------------------------------------------
	m_Monitors			= ogCfgData.GetMonitorForWindow(MON_COUNT_STRING) - 1;
	m_DutyRoster		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_STRING)-1;
	m_ShiftRoster		= ogCfgData.GetMonitorForWindow(MON_SHIFTROSTER_STRING)-1;
	int ilColums		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_STATICCOLUMNS);
	int ilRows			= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_DEBITROWS);
	int ilViewMi		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_VIEWMI);
	int ilViewPl		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_VIEWPL);
	int ilVorlauf		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_VORLAUF);
	int ilScrollTo		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_SCROLL);
	int ilDayWidth		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_DAYWIDTH);
	m_Sortstatistic		= ogCfgData.GetMonitorForWindow(MON_SORTSTATISTIC);
	int ilLeftWidth		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_TOTWIDTH);
	int ilRelation		= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_RELATION);

	if (ilColums < 1) ilColums = 1;
	if (ilRows < 1) ilRows = 1;
	if (ilDayWidth < 20) ilDayWidth = 20;
	if (ilLeftWidth == 1) ilLeftWidth = 202;
	if (ilRelation == 1) ilRelation = 50;

	// Voreinstellung
	if(m_Sortstatistic != 1)
	{
		m_Sortstatistic = 0;
		OnRadio4();		// nach Schichtcode
	}
	else
	{
		OnRadio5();		// nach Schichtbeginn
	}

	if(m_Monitors == 0)
	{
		OnRadio1();
	}
	else if(m_Monitors == 1)
	{
		OnRadio2();
	}
	m_User.SetWindowText(ogBasicData.omUserID);

	// Slider f�r die Anzahl der Kontenspalten
	m_Staticcolumns.SetRange(1,6);
	m_Staticcolumns.SetTicFreq(1);
    m_Staticcolumns.SetPos(ilColums);

	olTmp.Format("%d",ilColums);
	m_S_Staticcolumns.SetWindowText(olTmp);

	// Slider f�r die Anzahl der Statistikzeilen
	m_Debitrows.SetRange(1,9);
	m_Debitrows.SetTicFreq(1);
    m_Debitrows.SetPos(ilRows);

	olTmp.Format("%d",ilRows);
	m_S_Debitrows.SetWindowText(olTmp);

	// Slider f�r die Auswahlt der Breite der Planungstage
	m_PlanDays.SetRange(20,100);
	m_PlanDays.SetTicFreq(5);
	m_PlanDays.SetPos(ilDayWidth);

	olTmp.Format("%d",ilDayWidth);
	m_S_Plandays.SetWindowText(olTmp);

	// Slider of total width of left 2 columns
	m_TotalWidth.SetRange (100,400);
	m_TotalWidth.SetTicFreq(10);
	m_TotalWidth.SetPos(ilLeftWidth);

	// slider of relation between left 2 columns
	m_Relation.SetRange (0,100);
	m_Relation.SetTicFreq(5);
	m_Relation.SetPos(ilRelation);

	// Anzeige des speziellen Vorlaufs in der Dienstplanung
	if (ilVorlauf == 0)
		m_bVorlauf = false;
	else
		m_bVorlauf = true;

	// Scrollen zur gerade gew�hlten Schicht
	if (ilScrollTo == 0)
		m_bScroll = false;
	else
		m_bScroll = true;

	olTmp.Format("%d",ilViewMi);
	m_ViewMi.SetTypeToInt(0,14);
	m_ViewMi.SetTextErrColor(RED);
	m_ViewMi.SetInitText(olTmp);

	olTmp.Format("%d",ilViewPl);
	m_ViewPl.SetTypeToInt(0,14);
	m_ViewPl.SetTextErrColor(RED);
	m_ViewPl.SetInitText(olTmp);

	// Texte setzten
	SetDlgItemText(IDC_VORLAUF,LoadStg(IDS_STRING1182));

	// Breite der Planungstage
	int ilWidth = m_PlanDays.GetPos();

	// Breite ver�ndert
	RECT olRect;
	m_StaticTest.GetWindowRect(&olRect);
	ScreenToClient(&olRect);
	m_StaticTest.MoveWindow(olRect.left,olRect.top,ilWidth,olRect.bottom-olRect.top);
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::OnRadio1() 
{
	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(1);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	 
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::OnRadio2() 
{

	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(1);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::OnRadio3() 
{
	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::OnRadio4() 
{
	((CButton*) GetDlgItem(IDC_RADIO4))->SetCheck(1);
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::OnRadio5() 
{
	((CButton*) GetDlgItem(IDC_RADIO5))->SetCheck(1);
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::OnOK() 
{
	if(UpdateData() == TRUE)
	{
		char plcS1[20]="";
		char plcS2[20]="";
		char plcS3[20]="";
		char plcS4[20]="";
		char plcS5[20]="";
		char plcS6[20]="";
		char plcS7[20]="";
		char plcS8[20]="";
		char plcSortStatistic[20]="";

		sprintf(plcS1, "%d", m_Monitors+1);
		sprintf(plcS2, "%d", m_DutyRoster+1);
		sprintf(plcS3, "%d", m_ShiftRoster+1);
		sprintf(plcS4, "%d", m_Staticcolumns.GetPos());
		sprintf(plcS5, "%d", m_Debitrows.GetPos());
		sprintf(plcS6, "%d", m_PlanDays.GetPos());
		sprintf(plcS7, "%d", m_TotalWidth.GetPos());
		sprintf(plcS8, "%d", m_Relation.GetPos());
		sprintf(plcSortStatistic, "%d", m_Sortstatistic);

		// spezieller Vorlauf
		CString olVorlauf;
		if (m_bVorlauf)
			olVorlauf = "1";
		else
			olVorlauf = "0";

		// spezieller Vorlauf
		CString olScrollTo;
		if (m_bScroll)
			olScrollTo = "1";
		else
			olScrollTo = "0";

		
		CString olViewPl;
		CString olViewMi;

		if (m_ViewPl.GetStatus() == true)
		{
			m_ViewPl.GetWindowText(olViewPl);
		}
		else
		{
			m_ViewPl.SetWindowText("14");
			olViewPl="14";
			UpdateData(FALSE);
		}
			
		if (m_ViewMi.GetStatus() == true)
		{
			m_ViewMi.GetWindowText(olViewMi);
		}
		else
		{
			m_ViewMi.SetWindowText("14");
			olViewMi="14";
			UpdateData(FALSE);
		}

		sprintf(ogCfgData.rmMonitorSetup.Text, "%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s",
												MON_COUNT_STRING,plcS1,
												MON_DUTYROSTER_STRING,plcS2,
												MON_SHIFTROSTER_STRING,plcS3,
												MON_DUTYROSTER_STATICCOLUMNS,plcS4,								
												MON_DUTYROSTER_DEBITROWS,plcS5,
												MON_DUTYROSTER_VIEWPL,olViewPl,
												MON_DUTYROSTER_VIEWMI,olViewMi,
												MON_DUTYROSTER_VORLAUF,olVorlauf,
												MON_DUTYROSTER_SCROLL,olScrollTo,
												MON_DUTYROSTER_DAYWIDTH,plcS6,
												MON_SORTSTATISTIC,plcSortStatistic,
												MON_DUTYROSTER_TOTWIDTH,plcS7,
												MON_DUTYROSTER_RELATION,plcS8
												);

		if(ogCfgData.rmMonitorSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmMonitorSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmMonitorSetup);
		CDialog::OnOK();
	}
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{

	CString olTmp;
	olTmp.Format("%d",m_Debitrows.GetPos());
	m_S_Debitrows.SetWindowText(olTmp);

	olTmp.Format("%d",m_Staticcolumns.GetPos());
	m_S_Staticcolumns.SetWindowText(olTmp);

	// Breite der Planungstage
	int ilWidth = m_PlanDays.GetPos();
	olTmp.Format("%d",ilWidth);
	m_S_Plandays.SetWindowText(olTmp);

	// Breite ver�ndert
	RECT olRect;
	m_StaticTest.GetWindowRect(&olRect);
	ScreenToClient(&olRect);
	m_StaticTest.MoveWindow(olRect.left,olRect.top,ilWidth,olRect.bottom-olRect.top);

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

//*****************************************************************************************
//
//*****************************************************************************************

void SetupDlg::SetStatics()
{
	// Statische Texte setzen.
	SetDlgItemText(IDC_STATIC1,LoadStg(IDS_STRING1168));
	SetDlgItemText(IDC_STATIC2,LoadStg(IDS_STRING1169));
	SetDlgItemText(IDC_STATIC3,LoadStg(IDS_STRING1172));
	SetDlgItemText(IDC_STATIC4,LoadStg(IDS_STRING1173));
	SetDlgItemText(IDC_STATIC5,LoadStg(IDS_STRING1175));
	SetDlgItemText(IDC_STATIC6,LoadStg(IDS_STRING1176));
	SetDlgItemText(IDC_STATIC7,LoadStg(IDS_STRING1174));
	SetDlgItemText(IDC_STATIC8,LoadStg(IDS_STRING1587));
	SetDlgItemText(IDC_STATIC9,LoadStg(IDS_STRING1586));
	SetDlgItemText(IDC_CHECKLEGEND,LoadStg(IDS_STRING1177));
	SetDlgItemText(IDOK,LoadStg(IDS_STRING1524));
	SetDlgItemText(IDCANCEL,LoadStg(IDS_C_CLOSE));
	SetDlgItemText(IDC_SORTSTATISTIC,LoadStg(IDS_STRING_148));		// Zeilen in der Statistik sortieren*INTXT*
	SetDlgItemText(IDC_RADIO4,LoadStg(IDS_STRING164));				// nach Schichtcode*INTXT*
	SetDlgItemText(IDC_RADIO5,LoadStg(IDS_STRING165));				// nach Schichtbeginn*INTXT*
	SetDlgItemText(IDC_S_Ansicht1,LoadStg(IDC_S_Ansicht1));
	SetDlgItemText(IDC_S_Ansicht3, LoadStg(IDC_S_Ansicht3));
	SetDlgItemText(IDC_ScrollTo, LoadStg(IDS_SCROLLTO));
	SetDlgItemText(IDC_STATIC10,LoadStg(IDS_STRING1600));

	SetDlgItemText(IDC_LEFT_COLUMNS,LoadStg(IDS_STRING235));
	SetDlgItemText(IDC_S_TOTAL_WIDTH,LoadStg(IDS_STRING236));
	SetDlgItemText(IDC_S_RELATION,LoadStg(IDS_STRING237));
}

