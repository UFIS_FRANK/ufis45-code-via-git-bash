#ifndef _CedaDrwData_H_
#define _CedaDrwData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define DRW_SEND_DDX	(true)
#define DRW_NO_SEND_DDX	(false)

// Felddimensionen
#define DRW_CLOS_LEN	(1)
#define DRW_HOPO_LEN	(3)
#define DRW_REME_LEN	(60)
#define DRW_SDAY_LEN	(8)
#define DRW_STFU_LEN	(10)
#define DRW_USEC_LEN	(32)

// Struktur eines Drw-Datensatzes
struct DRWDATA {
	COleDateTime	Cdat;					//Erstellungsdatum
	char			Clos[DRW_CLOS_LEN+2];	//Record Locked ('B')
	char			Hopo[DRW_HOPO_LEN+2];	//Hopo
	COleDateTime	Lstu;					//Datum letzte �nderung
	char			Prio[DRW_CLOS_LEN+2];	//Priority
	char			Reme[DRW_REME_LEN+2];	//External Remarks of Disponent
	char			Remi[DRW_REME_LEN+2];	//Internal Remarks of Disponent
	char			Rems[DRW_REME_LEN+2];	//Remarks of Employee
	char			Sday[DRW_SDAY_LEN+2];	//Shift Day
	char			Stfu[DRW_STFU_LEN+2];	//Urno Employee
	long			Urno;					//Urno
	char			Usec[DRW_USEC_LEN+2];	//Ersteller
	char			Useu[DRW_USEC_LEN+2];	//Anwender letzte �nderung
	char			Wisc[DRW_SDAY_LEN+2];	//Wish


	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	DRWDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaDrwData class
void ProcessDrwCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaDrwData: kapselt den Zugriff auf die Tabelle Drw (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaDrwData: public CCSCedaData
{
// Funktionen
public:
	bool ReadDrwData();
    // Konstruktor/Destruktor
	CedaDrwData(CString opTableName = "Drw", CString opExtName = "TAB");
	~CedaDrwData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<DRWDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_DRW_CHANGE,BC_DRW_DELETE und BC_DRW_NEW
	void ProcessDrwBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadDrwByUrno(long lpUrno, DRWDATA *prpDrw);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(DRWDATA *prpDrw, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(DRWDATA *prpDrw, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(DRWDATA *prpDrw, bool bpSave = true);

	
	// Datens�tze suchen
	// Drw nach Urno suchen
	DRWDATA* GetDrwByUrno(long lpUrno);

	// kopiert die Feldwerte eines Drw-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyDrw(DRWDATA* popDrwDataSource,DRWDATA* popDrwDataTarget);

	DRWDATA* GetDrwByStfuWithTime(CString opStfu, CString opTime);
protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(DRWDATA *prpDrw);
	// einen Broadcast DRW_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(DRWDATA *prpDrw, bool bpSendDdx);
	// einen Broadcast DRW_CHANGE versenden
	bool UpdateInternal(DRWDATA *prpDrw, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(DRWDATA *prpDRW, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Drws
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaDrwData_H_
