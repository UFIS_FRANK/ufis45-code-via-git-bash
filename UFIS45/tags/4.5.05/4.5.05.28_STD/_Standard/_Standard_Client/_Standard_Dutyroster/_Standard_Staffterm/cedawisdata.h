#ifndef _CEDAWISDATA_H_
#define _CEDAWISDATA_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>

/////////////////////////////////////////////////////////////////////////////
#define WIS_SEND_DDX	(true)
#define WIS_NO_SEND_DDX	(false)

// Struktur eines WIS-Datensatzes
struct WISDATA {
	CTime	Cdat;					// Datum der Erstellung
	char	Hopo[3+2];				// Home Airport
	CTime	Lstu;					// Datum der letzen �nderung
	char	Orgc[8+2];				// Code Organisationseinheit
	char	Rema[60+2];				// Bemerkung 
	long	Urno;					// Unro 
	char	Usec[32+2];				// Ersteller
	char	Useu[32+2];				// Anwender letzte �nderung
	char	Wisc[8+2];				// Wunschcode
	char	Wisd[40+2];				// Bezeichnung

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese
	bool		Redrawn;
	bool		IsNew;
	WISDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		Cdat = TIMENULL;		// anwesend von
		Lstu = TIMENULL;		// anwesend bis
		Redrawn = true;
		IsNew = false;
	}
};	


// the broadcast CallBack function, has to be outside the CedaWISDATA class
//void ProcessWisCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

class CedaWisData: public CCSCedaData
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaWisData(CString opTableName = "WIS", CString opExtName = "TAB");
	~CedaWisData();

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_WIS_CHANGE,BC_WIS_DELETE und BC_WIS_NEW
	void ProcessWisBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	int GetSize() {return (omData.GetSize());}
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze nach Datum gefiltert lesen
	//bool ReadFilteredByDate(COleDateTime opStart, COleDateTime opEnd);
	// Kommandos an CEDA senden
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);

	bool Read(char *pspWhere = NULL);
	bool ReadDrwByUrno(long lpUrno, WISDATA *prpDrr);
	bool Insert(WISDATA *prpGhs, BOOL bpWithSave = true);
	bool Update(WISDATA *prpGhs, BOOL bpWithSave = true);
	bool Delete(WISDATA *prpGhs, BOOL bpWithSave = true);
	bool Save(WISDATA *prpGhs);
	bool Reload(CString opDateFrom, CString opDateTo, CString opUrnos);
	
	bool InsertInternal(WISDATA *prpDrw, bool bpSendDdx);
	void UpdateInternal(WISDATA *prpDrw, bool bpSendDdx = true);
	void DeleteInternal(WISDATA *prpDrw, bool bpSendDdx = true);

	// Finden von Datens�tzen
	WISDATA* GetWisByUrno(long llUrno);
	WISDATA* GetWisByKey(CString opWisc);
	
	bool IsDataChanged(WISDATA *prpOld, WISDATA *prpNew);

 	bool GetValues(int ipRow, WISDATA **popFound) 
    { 
		WISDATA *polFound;
		bool bpRet = true;
		if ((omData.GetSize() > ipRow) && (ipRow >= 0))
		{
			polFound = &omData[ipRow];
			*popFound = polFound;
		} 
		else
		{
			bpRet = false;
		} 
		return(bpRet);
	} 

public:
    bool WisExist(long Urno,WISDATA **prpData);
	//void SetLoadStfuUrnoMap(CMapPtrToPtr *popLoadAts1UrnoMap);

// Daten
    // die geladenen Datens�tze
	CCSPtrArray<WISDATA> omData;
private:

protected:	
	// Filter: wenn nicht NULL, enth�lt diese Map die Urnos der Mitarbeiter, f�r die 
	// Daten geladen werden 
	CMapPtrToPtr *pomLoadStfuUrnoMap;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
    // Map mit den Prim�rschl�sseln (aus SDAY, DRRN und STFU) der Datens�tze
	CMapStringToPtr omKeyMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
};

//extern CedaWisData ogWisData;

#endif	// _CedaWISDATA_H_
