
// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>
#include <BasicData.h>
#include <CedaBasicData.h>

#define N_URNOS_AT_ONCE 500

static int CompareArray( const CString **e1, const CString **e2);

#include <GUILng.h>		// rdr  Jan.2K

//------------------------------------------------------------------------------------

//	used to get an GUI-String from DB or Res
CString LoadStg(UINT nID)
{
	CGUILng* ogGUILng = CGUILng::TheOne(); //!!!Singelton !!! es gibt nur ein Objekt !!!
	return ogGUILng->GetString(nID);
}

//------------------------------------------------------------------------------------

CTime COleDateTimeToCTime(COleDateTime opTime)
{
	CTime olTime = -1;
	bool blYear = false;
	int ilDay = 1,ilMonth =1, ilYear;
	if(opTime.GetStatus() == COleDateTime::valid)
	{
		if(opTime.GetYear()>=2038 || opTime.GetYear()<1970)
		{
			blYear = true;
			if(opTime.GetYear()>=2038)
			{
				ilYear = 2038;
				ilMonth = 1;
			}
			else
			{
				ilYear = 1970;
				ilMonth = opTime.GetMonth();
			}
		}
		if(blYear && (ilDay = opTime.GetDay())>18)
		{
			if(ilYear ==2038)
				ilDay = 18;
		}
		if(blYear)
		{
			olTime = CTime(ilYear, ilMonth, ilDay, 1, 0, 0);
		}
		else
		{
			olTime = CTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
		}
	}
	return olTime;
}

//************************************************************************************************************************************************
// YYYYMMDDToOleDateTime: erzeugt aus einem Datumsstring im Format 'JJJJMMTT'
//	ein COleDateTime-Obkjekt.
// R�ckgabe:	true	-> String g�ltig, <opTime> initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool YYYYMMDDToOleDateTime(CString opTimeString, COleDateTime &opTime)
{
CCS_TRY
	// String-Param pr�fen	
	if ((opTimeString == "") || (opTimeString.GetLength() < 8) || (opTimeString.SpanExcluding("0123456789") != "")){
		// <opTimeString> ung�ltig -> terminieren
		return false;
	}
	
	// COleDateTime-Objekt aus SDAY erzeugen...
	COleDateTime olTime(atoi(opTimeString.Left(4).GetBuffer(0)), // Jahr (YYYY)
						atoi(opTimeString.Mid(4,2).GetBuffer(0)), // Monat (MM)
						atoi(opTimeString.Mid(6,2).GetBuffer(0)), // Tag (DD)
						0,0,0);	// Stunde, Minute, Sekunde
	// ...und kopieren
	opTime = olTime;
	return true;
CCS_CATCH_ALL
return false;
}

//------------------------------------------------------------------------------------

COleDateTime CTimeToCOleDateTime(CTime opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);
	if(opTime != -1)
	{
		olTime = COleDateTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}

//-- ItemList-Functions --------------------------------------------------------------

int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}

//------------------------------------------------------------------------------------

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = olData[i];
				return true; 
		}
	}
	return false;
}

//------------------------------------------------------------------------------------

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = DBStringToDateTime(olData[i]);
				return true; 
		}
	}
	return false;
}

//------------------------------------------------------------------------------------

int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}

//------------------------------------------------------------------------------------

CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}

//------------------------------------------------------------------------------------

CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}

//------------------------------------------------------------------------------------

CString SortItemList(CString opSubString, char cpTrenner)
{
	CString *polText;
	CCSPtrArray<CString> olArray;

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			polText = new CString;
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				*polText = opSubString;
			}
			else
			{
				*polText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			olArray.Add(polText);
		}
	}

	CString olSortedString;
	olArray.Sort(CompareArray);
	for(int i=0; i<olArray.GetSize(); i++)
	{
		olSortedString += olArray[i] + cpTrenner;
	}
	
	olArray.DeleteAll();
	return olSortedString.Left(olSortedString.GetLength()-1);
}

//------------------------------------------------------------------------------------

int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner)
{

	CString olSubStr;

	popStrArray->RemoveAll();
	
	CStringArray olStrArray;

	int ilCount = ExtractItemList(opString, &olStrArray);
	int ilSubCount = 0;

	for(int i = 0; i < ilCount; i++)
	{
		if(ilSubCount >= ipMaxItem)
		{
			if(!olSubStr.IsEmpty())
				olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
			popStrArray->Add(olSubStr);
			ilSubCount = 0;
			olSubStr = "";
		}
		ilSubCount++;
		olSubStr = olSubStr + cpTrenner + olStrArray[i];
	}
	if(!olSubStr.IsEmpty())
	{
		olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
		popStrArray->Add(olSubStr);
	}
	
	return	popStrArray->GetSize();
}

//---------------------------------------------------------------------------

static int CompareArray( const CString **e1, const CString **e2)
{
	return (strcmp((**e1),(**e2)));
}

//***************************************************************************
// CBasicDat 
//***************************************************************************

CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(pcgAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
	char pclDaysToRead[10]="";
	GetPrivateProfileString(pcgAppName, "DAYSTOREAD", "1",
      pclDaysToRead, sizeof pclDaysToRead, pclConfigPath);
	igDaysToRead = atoi(pclDaysToRead);
}

//---------------------------------------------------------------------------

CBasicData::~CBasicData(void)
{
}

//---------------------------------------------------------------------------

long CBasicData::GetNextUrno(void)
{
	bool	olRc = true;
	long				llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(500);
	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);

		if ( (llNextUrno != 0L) && (olRc == true) )
		{
			return(llNextUrno);
		}
	}
	::MessageBox(NULL,LoadStg(IDS_STRING102),LoadStg(IDS_STRING103),MB_OK);
	return -1;	
}

//---------------------------------------------------------------------------

bool CBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];

	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

	ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= N_URNOS_AT_ONCE) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}

//---------------------------------------------------------------------------

int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}

//---------------------------------------------------------------------------

char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}

//---------------------------------------------------------------------------

void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}

//---------------------------------------------------------------------------

void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}

//---------------------------------------------------------------------------

void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS001");//opWsName;
}

//---------------------------------------------------------------------------

CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}

//---------------------------------------------------------------------------

bool CBasicData::GetWindowPosition(CRect& rlPos,CString olMonitor)
{

	int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{
		if (ogCfgData.rmUserSetup.RESO[0] == '1')
		{
			if (ogCfgData.rmUserSetup.RESO[1] == '0')
			{
				XResolution = 1024;
				YResolution = 768;
			}
		}
		else
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	}
  

	int ilMonitor = 0;
	if (olMonitor[0] == 'L')
		ilMonitor = 0;
	if (olMonitor[0] == 'M')
		ilMonitor = 1;
	if (olMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	return true;
}

//---------------------------------------------------------------------------

int CBasicData::GetUtcDifference(void)
{

	static int ilUtcDifference = 1;
	static BOOL blIsInitialized = FALSE;

	if (blIsInitialized == FALSE)
	{
		struct tm *_tm;
		time_t    now;
		
		int hour_gm,hour_local;
		blIsInitialized = TRUE;

		now = time(NULL);
		_tm = (struct tm *)gmtime(&now);
		hour_gm = _tm->tm_hour;
		_tm = (struct tm *)localtime(&now);
		hour_local = _tm->tm_hour;
		if (hour_gm > hour_local)
		{
			ilUtcDifference = ((hour_local+24-hour_gm)*3600);
		}
		else
		{
			ilUtcDifference = ((hour_local-hour_gm)*3600);
		}
	}

	return ilUtcDifference;

}

//---------------------------------------------------------------------------

void CBasicData::SetLocalDiff()
{
	int		ilTdi1;
	int		ilTdi2;
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime olCurr;
	RecordSet *prlRecord;	

	CCSPtrArray<RecordSet> olData;

	CString olWhere = CString("WHERE APC3 = '") + CString(pcgHome) + CString("'");

	if(ogBCD.ReadSpecial( "APT", "TICH,TDI1,TDI2", olWhere, olData))
	{
		if(olData.GetSize() > 0)
		{
			prlRecord = &olData[0];
		
			CString olTime = prlRecord->Values[0] + "00";
			olTich = DBStringToDateTime(olTime);
			omTich = olTich;
			ilTdi1 = atoi((*prlRecord)[1]);
			ilTdi2 = atoi((*prlRecord)[2]);
			
			ilHour = ilTdi1 / 60;
			ilMin  = ilTdi1 % 60;
			
			CTimeSpan	olTdi1(0,ilHour ,ilMin ,0);

			ilHour = ilTdi2 / 60;
			ilMin  = ilTdi2 % 60;
			
			CTimeSpan	olTdi2(0,ilHour ,ilMin ,0);

			olCurr = CTime::GetCurrentTime();

			if(olTich != TIMENULL) 
			{
				omLocalDiff1 = olTdi1;
				omLocalDiff2 = olTdi2;
			}
		}
	}
	olData.DeleteAll();
}

//---------------------------------------------------------------------------

void CBasicData::LocalToUtc(CTime &opTime)
{
	if(opTime != TIMENULL)
	{
		if(opTime < omTich)
		{
			opTime -= omLocalDiff1;
		}
		else
		{
			opTime -= omLocalDiff2;
		}
	}

}

//---------------------------------------------------------------------------

void CBasicData::UtcToLocal(CTime &opTime)
{
	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);

	if(opTime != TIMENULL)
	{
		if(opTime < olTichUtc)
		{
			opTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
		}
	}
}

//---------------------------------------------------------------------------
