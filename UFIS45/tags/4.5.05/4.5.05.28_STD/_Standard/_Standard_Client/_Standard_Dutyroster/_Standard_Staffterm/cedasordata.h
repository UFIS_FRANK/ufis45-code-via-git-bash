// CedaSorData.h

#ifndef __CEDASORDATA__
#define __CEDASORDATA__
 
#include <stdafx.h>
#include <CedaStfData.h>
#include <basicdata.h>
#include <CCSDdx.h>
#include <afxdisp.h>
#include <CCSCedaData.h>

//---------------------------------------------------------------------------------------------------------

struct SORDATA 
{
	long			Urno;
	long			Surn;
	char			Code[8+1];
	COleDateTime	Vpfr;
	COleDateTime	Vpto;
	char			Lead[1+1];
	char			Odgc[8+1];

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SORDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SORDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSorData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SORDATA> omData;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaSorData();
	~CedaSorData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SORDATA *prpSor);
	bool InsertInternal(SORDATA *prpSor);
	bool Update(SORDATA *prpSor);
	bool UpdateInternal(SORDATA *prpSor);
	bool Delete(long lpUrno);
	bool DeleteInternal(SORDATA *prpSor);
	bool ReadSpecialData(CCSPtrArray<SORDATA> *popSor,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SORDATA *prpSor);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SORDATA *GetSorByUrno(long lpUrno);
	void GetSorBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SORDATA> *popSorData);
	CString GetOrgBySurnWithTime(long lpSurn, COleDateTime opDate);
	CString GetOrgBySurnWithTime(long lpSurn, COleDateTime opDate, CCSPtrArray<SORDATA> *popSorData);
	CString GetOrgWithTime(COleDateTime opDate, CCSPtrArray<SORDATA> *popSorData);
	CString GetSorByOrgWithTime(CStringArray *popOrg, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData);
	CString GetStfWithoutOrgWithTime(COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<STFDATA> *popStfData);

	// Private methods
private:
    void PrepareSorData(SORDATA *prpSorData);

};

//---------------------------------------------------------------------------------------------------------

//extern CedaSorData ogSorData;

#endif //__CEDASORDATA__
