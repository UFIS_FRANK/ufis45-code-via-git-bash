// CedaAccData.h

#ifndef __CEDAACCDATA__
#define __CEDAACCDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
 
// COMMANDS FOR MEMBER-FUNCTIONS
#define ACC_SEND_DDX	(true)
#define ACC_NO_SEND_DDX	(false)

struct ACCDATA 
{
	CTime	Aato;
	char	Auby[32 + 2];
	CTime	Auda;
	CTime	Cdat;
	char	Cl01[8 + 2];
	char	Cl02[8 + 2];
	char	Cl03[8 + 2];
	char	Cl04[8 + 2];
	char	Cl05[8 + 2];
	char	Cl06[8 + 2];
	char	Cl07[8 + 2];
	char	Cl08[8 + 2];
	char	Cl09[8 + 2];
	char	Cl10[8 + 2];
	char	Cl11[8 + 2];
	char	Cl12[8 + 2];
	char	Co01[40 + 2]; // <- 40 is OK !
	char	Co02[8 + 2];
	char	Co03[8 + 2];
	char	Co04[8 + 2];
	char	Co05[8 + 2];
	char	Co06[8 + 2];
	char	Co07[8 + 2];
	char	Co08[8 + 2];
	char	Co09[8 + 2];
	char	Co10[8 + 2];
	char	Co11[8 + 2];
	char	Co12[8 + 2];
	char    Hopo[3 + 2];
	CTime	Lstu;
	char	Op01[8 + 2];
	char	Op02[8 + 2];
	char	Op03[8 + 2];
	char	Op04[8 + 2];
	char	Op05[8 + 2];
	char	Op06[8 + 2];
	char	Op07[8 + 2];
	char	Op08[8 + 2];
	char	Op09[8 + 2];
	char	Op10[8 + 2];
	char	Op11[8 + 2];
	char	Op12[8 + 2];
	char	Peno[20 + 2];
	long	Stfu;
	char	Type[10 + 2];
	long	Urno;
	char	Usec[32 + 2];
	char	Useu[32 + 2];
	char	Year[4 + 2];
	char	Yecu[8 +2];
	char	Yela[8 +2];

	//DataCreated by this class
	int      IsChanged;

	ACCDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Stfu		=   0;
		Urno		=   0;
		IsChanged	=	DATA_UNCHANGED;
		Aato		=	TIMENULL;
		Auda		=	TIMENULL;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end ACCDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaAccData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omKeyMap;

	bool bmCanSave;
    CCSPtrArray<ACCDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    CedaAccData();
	~CedaAccData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithUnRegistration = true);
    bool Read(char *pspWhere = NULL, bool bpDeleteAll = true);
	bool Insert(ACCDATA *prpAcc);
	bool InsertInternal(ACCDATA *prpAcc,  bool bpSendDdx = true);
	bool Update(ACCDATA *prpAcc,bool bpSendDdx = true);
	bool UpdateInternal(ACCDATA *prpAcc,  bool bpSendDdx = true);
	bool Delete(long lpUrno);
	bool DeleteInternal(ACCDATA *prpAcc, bool bpSendDdx = true);
	bool ReadSpecialData(CCSPtrArray<ACCDATA> *popAcc, char *pspWhere, char *pspFieldList, bool ipSYS = false);
	bool Save(ACCDATA *prpAcc);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	bool Reload(CString opDateFrom, CString opDateTo, CString opType, CString opUrnos, CString opCmd,bool bpWithUrnos = false);
	bool ReloadInternal();
	bool Release(CString opDateFrom, CString opDateTo, CStringArray* popTypeArray, CStringArray *popUrnoArray, CString opCmd = CString("R")); // R = Release (freigeben), G = Gather (zurücknehmen)

	ACCDATA* GetAccByUrno(long lpUrno);
	ACCDATA* GetAccByKey(long lpStfUrno, CString opYear, CString opType);

	CString GetFieldList();
	bool IsDataChanged(ACCDATA *prpOld, ACCDATA *prpNew);

	void SetLoadStfuUrnoMap(CMapPtrToPtr *popLoadStfuUrnoMap);



private:

    CMapPtrToPtr *pomLoadStfuUrnoMap;

};

//---------------------------------------------------------------------------------------------------------
//extern CedaAccData ogAccData;

#endif //__CEDAACCDATA__
