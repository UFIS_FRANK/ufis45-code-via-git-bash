#ifndef _CedaDrrData_H_
#define _CedaDrrData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define DRR_SEND_DDX	(true)
#define DRR_NO_SEND_DDX	(false)

// Felddimensionen
#define DRR_AVFR_LEN	(14)
#define DRR_BKDP_LEN	(1)
#define DRR_BSDU_LEN	(10)
#define DRR_BUFU_LEN	(15)
#define DRR_HOPO_LEN	(3)
#define DRR_REMA_LEN	(40)
#define DRR_SBLP_LEN	(4)
#define DRR_SCOD_LEN	(8)
#define DRR_USEC_LEN	(32)

// Struktur eines Drr-Datensatzes
struct DRRDATA {
	char			Avfr[DRR_AVFR_LEN+2];	//Anwesend von
	char			Avto[DRR_AVFR_LEN+2];	//Anwesend bis
	char			Bkdp[DRR_BKDP_LEN+2];	//1.Pause bezahlt?
	char			Bsdu[DRR_BSDU_LEN+2];	//Urno BSDTab
	char			Bufu[DRR_BUFU_LEN+2];	//Funknummer
	COleDateTime	Cdat;					//Erstellungsdatum
	char			Drrn[DRR_BKDP_LEN+2];	//Schichtnummer
	char			Drsf[DRR_BKDP_LEN+2];	//Flag f�r Zusatz-Infos
	char			Expf[DRR_BKDP_LEN+2];	//Export File
	char			Hopo[DRR_HOPO_LEN+2];	//Hopo
	COleDateTime	Lstu;					//Datum letzte �nderung
	char			Rema[DRR_REMA_LEN+2];	//Remarks
	char			Rosl[DRR_BKDP_LEN+2];	//Planungsstufe
	char			Ross[DRR_BKDP_LEN+2];	//Status der Planungsstufe
	char			Sbfr[DRR_AVFR_LEN+2];	//Pausenlage von
	char			Sblp[DRR_SBLP_LEN+2];	//2.Pause
	char			Sblu[DRR_SBLP_LEN+2];	//1.Pause
	char			Sbto[DRR_AVFR_LEN+2];	//Pausenlage bis
	char			Scod[DRR_SCOD_LEN+2];	//Schichtcode
	char			Sday[DRR_SCOD_LEN+2];	//Schichttag
	char			Stfu[DRR_BSDU_LEN+2];	//Urno STFTAB
	long			Urno;					//Urno
	char			Usec[DRR_USEC_LEN+2];	//Ersteller
	char			Useu[DRR_USEC_LEN+2];	//Anwender letzte �nderung
	
	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	DRRDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaDrrData class
void ProcessDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaDrrData: kapselt den Zugriff auf die Tabelle Drr (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaDrrData: public CCSCedaData
{
// Funktionen
public:
	bool ReadDrrData();
    // Konstruktor/Destruktor
	CedaDrrData(CString opTableName = "Drr", CString opExtName = "TAB");
	~CedaDrrData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<DRRDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_DRR_CHANGE,BC_DRR_DELETE und BC_DRR_NEW
	void ProcessDrrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadDrrByUrno(long lpUrno, DRRDATA *prpDrr);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(DRRDATA *prpDrr, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(DRRDATA *prpDrr, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(DRRDATA *prpDrr, bool bpSave = true);

	
	// Datens�tze suchen
	// Drr nach Urno suchen
	DRRDATA* GetDrrByUrno(long lpUrno);

	// kopiert die Feldwerte eines Drr-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyDrr(DRRDATA* popDrrDataSource,DRRDATA* popDrrDataTarget);

	//DRRDATA* GetDrrByStfuWithTime(CString opStfu, CString opTime);
	DRRDATA* GetDrrByStfuWithTimeAndRosl(CString opStfu, CString opTime, CString opRosl);
	int GetDrrByStfuWithTimeAndRosl(CString opStfu, CString opTime, CString opRosl, CCSPtrArray<DRRDATA> *popDrrData);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(DRRDATA *prpDrr);
	// einen Broadcast DRR_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(DRRDATA *prpDrr, bool bpSendDdx);
	// einen Broadcast DRR_CHANGE versenden
	bool UpdateInternal(DRRDATA *prpDrr, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(DRRDATA *prpDrr, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Drrs
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaDrrData_H_
