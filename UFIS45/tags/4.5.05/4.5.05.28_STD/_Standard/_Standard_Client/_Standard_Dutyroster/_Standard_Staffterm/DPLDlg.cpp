// DPLDlg.cpp : implementation file
//

#include <stdafx.h>
#include <staffterm.h>
#include <DPLDlg.h>
#include <CedaDrrData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//**************************************************************************************
// ProcessBC: globale Funktion als Callback f�r den Broadcast-Handler
//**************************************************************************************

static void ProcessBC(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CDPLDlg *polDPL = (CDPLDlg *)popInstance;

	/*if(ipDDXType == DRR_NEW)          polStaffTerm->ProcessDrrChange((DRRDATA*)vpDataPointer);
	if(ipDDXType == DRR_CHANGE)       polStaffTerm->ProcessDrrChange((DRRDATA*)vpDataPointer);
	if(ipDDXType == DRR_DELETE)       polStaffTerm->ProcessDrrDelete((DRRDATA*)vpDataPointer);
	
	if(ipDDXType == DRW_NEW)          polStaffTerm->ProcessDrwChange((DRWDATA*)vpDataPointer);
	if(ipDDXType == DRW_DELETE)       polStaffTerm->ProcessDrwDelete((DRWDATA*)vpDataPointer);
	if(ipDDXType == DRW_CHANGE)       polStaffTerm->ProcessDrwChange((DRWDATA*)vpDataPointer);
	*/
}
/////////////////////////////////////////////////////////////////////////////
// CDPLDlg dialog

CDPLDlg::CDPLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDPLDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDPLDlg)
	//}}AFX_DATA_INIT

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	StfUrno = 0;
	Name = CString("");
	pomDPL = NULL;
	
	// beim Broadcast-Handler anmelden
	Register();
}

CDPLDlg::~CDPLDlg()
{
	if(pomDPL != NULL)
		delete pomDPL;
}

void CDPLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDPLDlg)
	DDX_Control(pDX, IDC_SPIN_YEAR, m_Spin_Year);
	DDX_Control(pDX, IDC_SPIN_MONTH, m_Spin_Month);
	DDX_Control(pDX, IDC_EDIT_YEAR, m_Edit_Year);
	DDX_Control(pDX, IDC_EDIT_MONTH, m_Edit_Month);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDPLDlg, CDialog)
	//{{AFX_MSG_MAP(CDPLDlg)
	ON_WM_DESTROY()
	ON_WM_VSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDPLDlg message handlers

int CDPLDlg::GetDaysOfMonth(COleDateTime opDay)
{
CCS_TRY
	if(opDay.GetStatus() != COleDateTime::valid)
		return 0;

	COleDateTime olMonth = COleDateTime(opDay.GetYear(), opDay.GetMonth(),1,0,0,0);
	bool blNextMonth = false;
	int ilDays = 28;
	while(!blNextMonth)
	{
		COleDateTime olTmpDay = olMonth + COleDateTimeSpan(ilDays,0,0,0);
		if(olTmpDay.GetMonth() != opDay.GetMonth())
			blNextMonth = true;
		else
			ilDays++;
	}
	if(ilDays < 0 || ilDays > 31)
		ilDays = 0;
	return ilDays;
CCS_CATCH_ALL
	return 0;
}

void CDPLDlg::Register()
{

}

void CDPLDlg::IniGrid()
{
	//�berschriften setzten
	pomDPL->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1002));
	pomDPL->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1021));
	pomDPL->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING1022));
	pomDPL->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING1023));
	pomDPL->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING1024));
	pomDPL->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING1025));
	pomDPL->SetValueRange(CGXRange(0,7), LoadStg(IDS_STRING1026));
	pomDPL->SetValueRange(CGXRange(0,8), LoadStg(IDS_STRING1027));
	pomDPL->SetValueRange(CGXRange(0,9), LoadStg(IDS_STRING1028));
	pomDPL->SetValueRange(CGXRange(0,10), LoadStg(IDS_STRING1029));
	pomDPL->SetValueRange(CGXRange(0,11), LoadStg(IDS_STRING1030));
	pomDPL->SetValueRange(CGXRange(0,12), LoadStg(IDS_STRING1031));
	pomDPL->SetValueRange(CGXRange(0,13), LoadStg(IDS_STRING1032));
	pomDPL->SetValueRange(CGXRange(0,14), LoadStg(IDS_STRING1033));
	pomDPL->SetValueRange(CGXRange(0,15), LoadStg(IDS_STRING1034));
	
	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomDPL->GetParam()->EnableTrackColWidth(FALSE);
	pomDPL->GetParam()->EnableTrackRowHeight(FALSE);
    pomDPL->GetParam()->EnableSelection(FALSE);

	//Scrollbars anzeigen wenn n�tig
	pomDPL->SetScrollBarMode(SB_BOTH, gxnAutomatic, TRUE);

	pomDPL->SetStyleRange( CGXRange().SetTable() , CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC));


	//Keine Zeilennummerierung
	pomDPL->HideCols(0, 0);
	pomDPL->SetColWidth(1, 1, pomDPL->Width_LPtoDP(4 * GX_NXAVGWIDTH));
	pomDPL->SetColWidth(2, 11, pomDPL->Width_LPtoDP(8 * GX_NXAVGWIDTH));
	pomDPL->SetColWidth(12, 12, pomDPL->Width_LPtoDP(25 * GX_NXAVGWIDTH));
	pomDPL->SetColWidth(13, 15, pomDPL->Width_LPtoDP(8 * GX_NXAVGWIDTH));
	
	pomDPL->EnableAutoGrow(false);
	pomDPL->EnableSorting(false);
}

void CDPLDlg::NewMonth()
{
CCS_TRY
	
	CString csMonth, csYear, csDay, csStfUrno, csMtYr, csRosl, csWeekDay;
	CString csWkBeg, csWkEnd, csBrkMin, csBrkBeg, csBrkEnd, csScod, csDhrs;
	COleDateTime ctSelectedMonth, ctWkBeg, ctWkEnd;
	COleDateTimeSpan ctDhrs;
	CCSPtrArray<DRRDATA> opDrrData;
	DRRDATA olDrr;
	int iDays, iAnzRows, iBrkMin, iMinutes, iHours;
	long ilUrno;
	ROWCOL iRows;

	csRosl = CString("2");
	csStfUrno.Format("%d", StfUrno);	

	iRows = pomDPL->GetRowCount();
	if (iRows >= 1)
		pomDPL->RemoveRows(1, iRows);

	m_Edit_Month.GetWindowText(csMonth);
	m_Edit_Year.GetWindowText(csYear);

	ctSelectedMonth.SetDate(atoi(csYear), atoi(csMonth), 1);  	
	iDays = GetDaysOfMonth(ctSelectedMonth);
	
	pomDPL->SetRowCount(iDays);
	
	// Grid mit Daten f�llen.
	pomDPL->GetParam()->SetLockReadOnly(false);
	
	for ( int i=iDays-1; i>=0 ; i--)
	{
		//Tag
		csDay.Format("%.2d", i+1);
		pomDPL->SetValueRange(CGXRange(i+1,1), csDay);
		//Wochentag
		ctSelectedMonth.SetDate(atoi(csYear), atoi(csMonth), i+1);
		csWeekDay = GetDayStrg(ctSelectedMonth);
		pomDPL->SetValueRange(CGXRange(i+1,2), csWeekDay);

		//Kein Mitarbeiter dem User zugeordnet. Es werden keine Daten angezeigt.
		if (StfUrno != 0){
			csDay.Format("%4s%.2s%.2i", csYear, csMonth, i+1);
			iAnzRows = 0;
			iAnzRows = ogDrrData.GetDrrByStfuWithTimeAndRosl(csStfUrno, csDay, csRosl, &opDrrData);
			if(iAnzRows>0){
				if(iAnzRows>1)
					pomDPL->InsertRows(i+2, iAnzRows-1);
				for (int y=0; y<opDrrData.GetSize(); y++){
					olDrr = opDrrData.GetAt(y);
					
					// Urno auslesen
					ilUrno = olDrr.Urno;
					// Urno mit erstem Feld koppeln.						
					pomDPL->SetStyleRange (CGXRange(i+1+y,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
					
					//WEK BEG
					csWkBeg = olDrr.Avfr;
					ctWkBeg.SetDateTime(atoi(csWkBeg.Left(4)),atoi(csWkBeg.Mid(4,2)), atoi(csWkBeg.Mid(6,2)),
									atoi(csWkBeg.Mid(8,2)),atoi(csWkBeg.Mid(10,2)), atoi(csWkBeg.Right(2))); 
					//WEK END
					csWkEnd = olDrr.Avto;
					ctWkEnd.SetDateTime(atoi(csWkEnd.Left(4)),atoi(csWkEnd.Mid(4,2)), atoi(csWkEnd.Mid(6,2)),
									atoi(csWkEnd.Mid(8,2)), atoi(csWkEnd.Mid(10,2)), atoi(csWkEnd.Right(2))); 
					//Schichtcode
					csScod = olDrr.Scod;

					if (csWkBeg == csWkEnd)						
						pomDPL->SetValueRange(CGXRange(i+1+y,3), csScod);
					else{
						csWkBeg = csWkBeg.Mid(8, 4);
						csWkEnd = csWkEnd.Mid(8, 4);
						pomDPL->SetValueRange(CGXRange(i+1+y,3), csWkBeg);
						pomDPL->SetValueRange(CGXRange(i+1+y,4), csWkEnd);
						
						//WEK VAR
						pomDPL->SetValueRange(CGXRange(i+1+y,5), CString("???"));
						
						//BRK MIN
						csBrkMin = olDrr.Sblu;
						iBrkMin = atoi(csBrkMin);
						COleDateTimeSpan ctBrk(0,0,iBrkMin, 0);
						if(iBrkMin > 0){
							pomDPL->SetValueRange(CGXRange(i+1+y,6), csBrkMin);
							//BRK BEG
							csBrkBeg = olDrr.Sbfr;
							if(csBrkBeg.GetLength() >= 12){
								csBrkBeg = csBrkBeg.Mid(8, 4);
								pomDPL->SetValueRange(CGXRange(i+1+y,7), csBrkBeg);
							}
							//BRK END
							csBrkEnd = olDrr.Sbto;
							if(csBrkEnd.GetLength() >= 12){
								csBrkEnd = csBrkEnd.Mid(8, 4);
								pomDPL->SetValueRange(CGXRange(i+1+y,8), csBrkEnd);
							}
						}
						//DHRS
						if(ctWkBeg.GetStatus() == COleDateTime::valid && ctWkEnd.GetStatus() == COleDateTime::valid){
							if(ctBrk.GetStatus() == COleDateTimeSpan::valid){
								ctDhrs = ctWkEnd - ctWkBeg - ctBrk;
								if(ctDhrs.GetStatus() == COleDateTimeSpan::valid){
									iHours = ctDhrs.GetHours();
									iMinutes = ctDhrs.GetMinutes();
									csDhrs.Format("%.2i:%.2i", iHours, iMinutes);		
									pomDPL->SetValueRange(CGXRange(i+1+y,9), csDhrs);
								}
							}
						}
						//DUTY CMD
						pomDPL->SetValueRange(CGXRange(i+1+y,10), CString("???"));
						//DUTY DGRP
						pomDPL->SetValueRange(CGXRange(i+1+y,11), CString("???"));
						//FUNKTION
						
						//SLOT BEG
			
						//SLOT END

						//ABS
					}
				}
			}		
		}
	}
	
	// Anzeige einstellen
	pomDPL->GetParam()->SetLockReadOnly(true);
	//pomDPL->Redraw();

CCS_CATCH_ALL
}

BOOL CDPLDlg::OnInitDialog() 
{
	CString csPersnr, csUsid,csMonth, csYear, csCot, csTitle;
	COleDateTime ctToday = COleDateTime::GetCurrentTime();
	
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetDlgItemText(IDC_STATIC_YEAR, LoadStg(IDS_STRING1009));
	SetDlgItemText(IDC_STATIC_MONTH, LoadStg(IDS_STRING1010));
	SetDlgItemText(IDOK, LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
		
	csMonth.Format("%.2d", ctToday.GetMonth());
	csYear.Format("%i", ctToday.GetYear());
	
	m_Edit_Month.SetTextLimit(1,2);
	m_Edit_Month.SetTextErrColor(RED);
	m_Edit_Month.SetInitText(csMonth);
	m_Edit_Month.SetTypeToInt(0,12);

	m_Edit_Year.SetTextLimit(1,4);
	m_Edit_Year.SetTextErrColor(RED);
	m_Edit_Year.SetInitText(csYear);
	m_Edit_Year.SetTypeToInt(2000,2050);

	// Spins initialisieren
	m_Spin_Month.SetRange(1,12);
	m_Spin_Month.SetPos(ctToday.GetMonth());

	m_Spin_Year.SetRange(2000,2050);
	m_Spin_Year.SetPos(ctToday.GetYear());

	csTitle = LoadStg(IDS_DPL);
	if(!Name.IsEmpty())
		csTitle = csTitle + CString(" - ") + Name;
	SetWindowText(csTitle);

	//Grid initialisieren
	pomDPL = new CGridControl(this, IDC_DPL_GRID, 15, MINROWS);
	IniGrid();
	NewMonth();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDPLDlg::OnDestroy() 
{
	ogDdx.UnRegister(this, NOTUSED);
	//ogCommHandler.CleanUpComm();

	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

void CDPLDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
CCS_TRY
	// TODO: Add your message handler code here and/or call default
	CString csValue;
	
	//CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
	if (nSBCode == SB_ENDSCROLL)
		return;

	if(pScrollBar->GetDlgCtrlID() == IDC_SPIN_MONTH){
		csValue.Format("%.2d", nPos);
		m_Edit_Month.SetInitText(csValue);
	}

	if(pScrollBar->GetDlgCtrlID() == IDC_SPIN_YEAR){
		csValue.Format("%i", nPos);
		m_Edit_Year.SetInitText(csValue);
	}

	NewMonth();
	
CCS_CATCH_ALL

}

CString CDPLDlg::GetDayStrg(COleDateTime opDay)
{
	CString opText;
	COleDateTime olDay = opDay;
	
	int ilDayOfWeek = olDay.GetDayOfWeek();
	switch(ilDayOfWeek)
	{
		case 1:
			opText = LoadStg(IDS_SUN);
			break;
		case 2:
			opText = LoadStg(IDS_MON);
			break;
		case 3:
			opText = LoadStg(IDS_TUE);
			break;
		case 4:
			opText = LoadStg(IDS_WED);
			break;
		case 5:
			opText = LoadStg(IDS_THU);
			break;
		case 6:
			opText = LoadStg(IDS_FRI);
			break;
		case 7:
			opText = LoadStg(IDS_SAT);
			break;
	}
	return opText;
}
