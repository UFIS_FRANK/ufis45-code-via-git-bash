VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3780
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7590
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3780
   ScaleWidth      =   7590
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnWorkGroups 
      Caption         =   "Workgroups"
      Height          =   615
      Left            =   120
      TabIndex        =   9
      Top             =   2400
      Width           =   1755
   End
   Begin VB.CommandButton btnShiftPlanOverview2 
      Caption         =   "Shiftplan Overview 2"
      Height          =   615
      Left            =   3840
      TabIndex        =   8
      Top             =   960
      Width           =   1755
   End
   Begin VB.CommandButton btnShiftOverview 
      Caption         =   "Shift Overview"
      Height          =   615
      Left            =   2040
      TabIndex        =   7
      Top             =   1680
      Width           =   1755
   End
   Begin VB.CommandButton btnShiftPlanOverview1 
      Caption         =   "Shiftplan Overview 1"
      Height          =   615
      Left            =   2040
      TabIndex        =   6
      Top             =   960
      Width           =   1755
   End
   Begin VB.CommandButton btn_CompensationReport 
      Caption         =   "CompensationReport"
      Height          =   600
      Left            =   195
      TabIndex        =   5
      Top             =   1665
      Width           =   1650
   End
   Begin TABLib.TAB TabLanguage 
      Height          =   1680
      Left            =   4290
      TabIndex        =   4
      Top             =   1785
      Width           =   3075
      _Version        =   65536
      _ExtentX        =   5424
      _ExtentY        =   2963
      _StockProps     =   64
   End
   Begin VB.CommandButton btnTagesdienstplan_B 
      Caption         =   "Tagesdienstplan B"
      Height          =   615
      Left            =   120
      TabIndex        =   3
      Top             =   900
      Width           =   1755
   End
   Begin VB.CommandButton btnTagesdienstplanBVD 
      Caption         =   "Tagesdienstplan A"
      Height          =   615
      Left            =   3840
      TabIndex        =   2
      Top             =   120
      Width           =   1755
   End
   Begin VB.CommandButton btnRosterOverview 
      Caption         =   "Roster Overview"
      Height          =   615
      Left            =   1980
      TabIndex        =   1
      Top             =   120
      Width           =   1755
   End
   Begin VB.CommandButton btnShiftPlanOverview 
      Caption         =   "Shiftplan Overview"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1755
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public omFile As String
Public strReport As String
Private strLanguage As String
Private strFieldSeparator As String

Private Sub btn_CompensationReport_Click()
    Dim rpt As New rptCompensation
    rpt.CurrentFile = omFile
    Dim frmPreview As New frmPrintPreview
    'rpt.CurrentFile = "D:\Tmp\RPT_20111229_124227.UPF"
    frmPreview.viewer1.ReportSource = rpt
    frmPreview.Caption = "Compensation report"
    frmPreview.Show
End Sub

Private Sub btnRosterOverview_Click()
    Dim rpt As New rptRosterOverview
    Dim preview As New frmPrintPreview
    'rpt.CurrentFile = "D:\Tmp\DP_20120202_172437_remark.UPF"
    rpt.CurrentFile = omFile
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True
End Sub

Private Sub btnShiftOverview_Click()
    Dim rpt As New rptShiftOverview
    Dim preview As New frmPrintPreview
    
    rpt.CurrentFile = omFile
    Call rpt.BuildDynamicReport
    
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True
End Sub

Private Sub btnShiftPlanOverview_Click()
    Dim rpt As New rptShichtplan
    Dim preview As New frmPrintPreview
    
    rpt.CurrentFile = omFile
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True
End Sub

Private Sub btnShiftPlanOverview1_Click()
    Dim rpt As New rptShiftplan1
    Dim preview As New frmPrintPreview
    
    rpt.CurrentFile = omFile
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True
End Sub

Private Sub btnShiftPlanOverview2_Click()
    Dim rpt As New rptShiftplan2
    Dim preview As New frmPrintPreview
    
    rpt.CurrentFile = omFile
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True

End Sub

Private Sub btnTagesdienstplan_B_Click()
    Dim rpt As New rptTagesdienstplan_B
    Dim preview As New frmPrintPreview
    
    rpt.CurrentFile = omFile
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True
End Sub

Private Sub btnTagesdienstplanBVD_Click()
    Dim rpt As New rptTagesdienstplan_A
    Dim preview As New frmPrintPreview
    
    rpt.CurrentFile = omFile
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True
End Sub

Public Sub ExitApp()
    End
End Sub

Private Sub btnWorkGroups_Click()
    Dim rpt As New rptWorkgroups
    Dim preview As New frmPrintPreview
    
    rpt.CurrentFile = omFile
    preview.viewer1.ReportSource = rpt
    preview.Show
    SetFormOnTop preview, True
End Sub

Private Sub Form_Load()
    'Me.Move 100000, 100000
    Me.Visible = False
    ReadLanguageFile
    MakeReport Command
End Sub

Public Sub MakeReport(Data As String)
    Dim InputLine As String
    Dim strResult As String

    strReport = ""
    
    On Error GoTo FileErr:

    omFile = Data
    If omFile <> "" Then
        Open omFile For Input As #1     ' Open file for input.
        Line Input #1, InputLine        ' Read line of data.
        Close #1                        ' Close file.
    End If

    GetKeyItem strResult, InputLine, "<=NAME=>", "<=\=>"
    If strResult <> "" Then
        strReport = strResult
    End If

    Select Case strReport
        Case "DUTYOVERVIEW"
            btnRosterOverview_Click
        Case "SHIFTPLAN"
            btnShiftPlanOverview_Click
        Case "SHIFTPLAN1"
            btnShiftPlanOverview1_Click
        Case "SHIFTPLAN2"
            btnShiftPlanOverview2_Click
        Case "DAILYROSTERPLAN_A"
            btnTagesdienstplanBVD_Click
        Case "DAILYROSTERPLAN_B"
            btnTagesdienstplan_B_Click
        Case "RPT_COMPENSATION"
            btn_CompensationReport_Click
        Case "SHIFTOVERVIEW"
            btnShiftOverview_Click
        Case "SHIFTOVERVIEW1"
            btnShiftOverview_Click
        Case "SHIFTOVERVIEW2"
            btnShiftOverview_Click
        Case "WORKGROUPS"
            btnWorkGroups_Click
    End Select

    Exit Sub
FileErr:
    MsgBox Err.Description
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' reads the language-file and puts it into a TAB.
' the file is built like
' 'ID' <Tabulator> 'Language' <Tabulator> 'String'
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Private Function ReadLanguageFile()
    Dim strLanguageFile As String

    'init the TAB
    TabLanguage.ResetContent
    TabLanguage.ShowHorzScroller True
    TabLanguage.EnableHeaderSizing True
    TabLanguage.HeaderLengthString = "50,50,150"
    TabLanguage.ColumnWidthString = "10,2,20"
    TabLanguage.ColumnAlignmentString = "R,L,L"
    strFieldSeparator = Chr(9)
    TabLanguage.SetFieldSeparator strFieldSeparator
    TabLanguage.HeaderString = "STID" & strFieldSeparator & "COCO" & strFieldSeparator & "STRG"

    'read the strings
    'strLanguageFile = "c:\ufis\system\RosteringPrint.ulf"
    strLanguageFile = UFIS_SYSTEM & "\RosteringPrint.ulf"
    If ExistFile(strLanguageFile) = True Then
        TabLanguage.ReadFromFile strLanguageFile
    End If

    'init the language-parameter (default is US => English)
    strLanguage = GetIniEntry("", "ROSTERING", "GLOBAL", "LANGUAGE", "US")
End Function

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' delivers the string fitting to the ID
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Public Function LoadStg(ByRef sSTID As String) As String
    Dim strRet As String
    Dim strLineNo As String

    strLineNo = TabLanguage.GetLinesByMultipleColumnValue("0,1", sSTID & strFieldSeparator & strLanguage, 0)

    If IsNumeric(strLineNo) = True And strLineNo <> "" Then
        strRet = TabLanguage.GetColumnValue(CLng(strLineNo), 2)
    Else
        strRet = ""
    End If

    LoadStg = strRet
End Function
