-- HOUSEKEEPING
-- Create sequence for HOUSEKEEPING
-- run as user ceda
create sequence HOUSEKEEPING_SEQ
minvalue 1
maxvalue 999999999999
start with 1
increment by 1
nocache
cycle;
