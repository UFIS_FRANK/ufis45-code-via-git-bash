#ifndef _DEF_mks_version_fdiasg
  #define _DEF_mks_version_fdiasg
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdiasg[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdiasg.src 1.14 2008/04/11 20:42:55SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  AKL                                                      */
/* Date           :                                                           */
/* Description    :  INCLUDE PART FROM FDIHDL                                 */
/*                   Request Information Message                              */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* FDIASG handle functions                                                    */
/* -----------------------                                                    */
/* static int HandleRQM (char *pcpData)                                       */
/* static int HandleDIV (char *pcpData)                                       */
/* static int HandleTPM (char *pcpData)                                       */
/* static int HandleETM (char *pcpData)                                       */
/* static int HandleFWD (char *pcpData)                                       */
/* static int HandleAFTN_PVG (char *pcpData, char *pcpAftUrno)                */
/* static int HandleUBM (char *pcpData)                                       */
/* static int HandleCCM (char *pcpData)                                       */


static int HandleRQM(char *pcpData)
{
  int ilRC;
  int ilYear=0;
  int ilMonth=0;
  int ilLine;
  char pclFunc[]="HandleRQM:";
  char *pclData;
  char pclResult[100];
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT *prlResult;
  T_TLXRESULT prlTlxMonth;
  T_TLXRESULT prlTlxYear;

  memset(&prlTlxMonth,0x00,TLXRESULT);
  memset(&prlTlxYear,0x00,TLXRESULT);

  prlInfoCmd = &prgInfoCmd[0][0];

  pclData = pcpData;

  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 

  ilLine = 2;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"RQM") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     ilRC = PruneFlight(pclResult,prgMasterTlx,0);
     prlResult = CCSArrayGetData(prgMasterTlx,"FDate");
     if (ilRC == RC_SUCCESS &&
         prlResult != NULL &&
         rgTlxInfo.TlxDay[0] != 0x00 &&
         rgTlxInfo.TlxMonth[0] != 0x00)
     {
        if (atoi(prlResult->DValue) > atoi(rgTlxInfo.TlxDay))
        {
           dbg(DEBUG,"%s Change Day of flight ",pclFunc);
           ilMonth = atoi(rgTlxInfo.TlxMonth);
           dbg(DEBUG,"%s Month <%d>",pclFunc,ilMonth);
           switch (ilMonth)
           {
              case 1: /* in JAN switch back to DEC */
                 sprintf(prlTlxMonth.DValue,"12");
                 /* decrement year */
                 if (rgTlxInfo.TlxYear[0] != 0x00 )
                 {
                    ilYear = atoi(rgTlxInfo.TlxYear);
                    switch (ilYear)
                    {
                       case 0: /* if year 00 goto 99 */
                          sprintf(prlTlxYear.DValue,"99");
                          break;
                       default: /* else Year-- */
                          sprintf(prlTlxYear.DValue,"%2.2d",ilYear-1);
                       break;
                    } /* end switch year */
                    strcpy(prlTlxYear.DName,"FYear");
                    CCSArrayAddUnsort(prgMasterTlx,&prlTlxYear,TLXRESULT);
                 } /* end if Year */
                 break;
              default:
                 sprintf(prlTlxMonth.DValue,"%2.2d",ilMonth-1);
                 dbg(DEBUG,"%s Case Default TlxMonth-1 <%s> ",pclFunc,prlTlxMonth.DValue);
                 break;
           } /* end switch month */
           strcpy(prlTlxMonth.DName,"FMonth");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxMonth,TLXRESULT);
        } /* end if FDate > TlxDay */
     } /* end if all values available */
  } 

  ilRC = SendTlxRes(FDI_RQM);

  return ilRC;
} /* end of HandleRQM  */


static int HandleDIV(char *pcpData)
{
  int ilRC;
  int ilYear=0;
  int ilMonth=0;
  int ilLine;
  char pclFunc[]="HandleDIV:";
  char *pclData;
  char pclResult[100];
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT *prlResult;
  T_TLXRESULT prlTlxMonth;
  T_TLXRESULT prlTlxYear;

  memset(&prlTlxMonth,0x00,TLXRESULT);
  memset(&prlTlxYear,0x00,TLXRESULT);

  prlInfoCmd = &prgInfoCmd[0][0];

  pclData = pcpData;

  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 

  ilLine = 1;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"DIV") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     ilRC = PruneFlight(pclResult,prgMasterTlx,0);
     prlResult = CCSArrayGetData(prgMasterTlx,"FDate");
     if (ilRC == RC_SUCCESS &&
         prlResult != NULL &&
         rgTlxInfo.TlxDay[0] != 0x00 &&
         rgTlxInfo.TlxMonth[0] != 0x00)
     {
        if (atoi(prlResult->DValue) > atoi(rgTlxInfo.TlxDay))
        {
           dbg(DEBUG,"%s Change Day of flight ",pclFunc);
           ilMonth = atoi(rgTlxInfo.TlxMonth);
           dbg(DEBUG,"%s Month <%d>",pclFunc,ilMonth);
           switch (ilMonth)
           {
              case 1: /* in JAN switch back to DEC */
                 sprintf(prlTlxMonth.DValue,"12");
                 /* decrement year */
                 if (rgTlxInfo.TlxYear[0] != 0x00 )
                 {
                    ilYear = atoi(rgTlxInfo.TlxYear);
                    switch (ilYear)
                    {
                       case 0: /* if year 00 goto 99 */
                          sprintf(prlTlxYear.DValue,"99");
                          break;
                       default: /* else Year-- */
                          sprintf(prlTlxYear.DValue,"%2.2d",ilYear-1);
                       break;
                    } /* end switch year */
                    strcpy(prlTlxYear.DName,"FYear");
                    CCSArrayAddUnsort(prgMasterTlx,&prlTlxYear,TLXRESULT);
                 } /* end if Year */
                 break;
              default:
                 sprintf(prlTlxMonth.DValue,"%2.2d",ilMonth-1);
                 dbg(DEBUG,"%s Case Default TlxMonth-1 <%s> ",pclFunc,prlTlxMonth.DValue);
                 break;
           } /* end switch month */
           strcpy(prlTlxMonth.DName,"FMonth");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxMonth,TLXRESULT);
        } /* end if FDate > TlxDay */
     } /* end if all values available */
  } 

  ilRC = SendTlxRes(FDI_DIV);

  return ilRC;
} /* end of HandleDIV  */


static int HandleTPM(char *pcpData)
{
  int ilRC;
  int ilLine;
  char pclFunc[]="HandleTPM:";
  char *pclData;
  char pclResult[100];
  char pclResult2[100];
  char *pclTmpPtr;
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT rlTlxResult;

  prlInfoCmd = &prgInfoCmd[0][0];
  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  ilLine = 1;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"TPM") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');

     pclTmpPtr = pclResult2;
     /* Get Airline Code */
     ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
     /* Get Flight Number */
     ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
     /* Get Destination */
     GetDataItem(pclResult,pclResult2,3,',',"","\0\0");
     if (strlen(pclResult) == 3)
     {
        dbg(DEBUG,"%s Found Destination <%s>",pclFunc,pclResult); 
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pclResult);
        strcpy(rlTlxResult.FValue,pclResult);
        strcpy(rlTlxResult.DName,"Dest");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
     }
     else
     {
        GetDataItem(pclResult,pclResult2,4,',',"","\0\0");
        if (strlen(pclResult) == 3)
        {
           dbg(DEBUG,"%s Found Destination <%s>",pclFunc,pclResult); 
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pclResult);
           strcpy(rlTlxResult.FValue,pclResult);
           strcpy(rlTlxResult.DName,"Dest");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        }
     }
  } 

  ilRC = SendTlxRes(FDI_TPM);

  return ilRC;
} /* end of HandleTPM  */


static int HandleETM(char *pcpData)
{
  int ilRC;
  int ilLine;
  char pclFunc[]="HandleETM:";
  char *pclData;
  char pclResult[100];
  char pclResult2[100];
  char *pclTmpPtr;
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT rlTlxResult;

  prlInfoCmd = &prgInfoCmd[0][0];
  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  ilLine = 1;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"ETM") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');

     pclTmpPtr = pclResult2;
     /* Get Airline Code */
     ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
     /* Get Flight Number */
     ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
     /* Get Destination */
     GetDataItem(pclResult,pclResult2,3,',',"","\0\0");
     if (strlen(pclResult) == 3)
     {
        dbg(DEBUG,"%s Found Destination <%s>",pclFunc,pclResult); 
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pclResult);
        strcpy(rlTlxResult.FValue,pclResult);
        strcpy(rlTlxResult.DName,"Dest");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
     }
     else
     {
        GetDataItem(pclResult,pclResult2,4,',',"","\0\0");
        if (strlen(pclResult) == 3)
        {
           dbg(DEBUG,"%s Found Destination <%s>",pclFunc,pclResult); 
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pclResult);
           strcpy(rlTlxResult.FValue,pclResult);
           strcpy(rlTlxResult.DName,"Dest");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        }
     }
  } 

  ilRC = SendTlxRes(FDI_ETM);

  return ilRC;
} /* end of HandleETM  */


static int HandleFWD(char *pcpData)
{
  int ilRC;
  char pclFunc[]="HandleFWD:";
  char *pclData;
  char pclResult[100];
  char pclResult2[100];
  char *pclTmpPtr;
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT rlTlxResult;
  int ilCount;

  prlInfoCmd = &prgInfoCmd[0][0];
  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  pclTmpPtr = strstr(&pclData[rgTlxInfo.TxtStart],"ORIGINALLY");
  if (pclTmpPtr != NULL)
  {
     CopyLine(pclResult,pclTmpPtr);
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');
     ilCount = GetNoOfElements(pclResult2,',');
     pclTmpPtr = NULL;
     GetDataItem(pclResult,pclResult2,ilCount,',',"","\0\0");
     while (strlen(pclResult) == 0 && ilCount > 1)
     {
        ilCount--;
        GetDataItem(pclResult,pclResult2,ilCount,',',"","\0\0");
     }
     if (strlen(pclResult) == 5)
     { /* Found DDMMM */
        ilCount--;
     }
     else
     { /* Found MMM only */
        ilCount -= 2;
     }
     GetDataItem(pclResult,pclResult2,ilCount+1,',',"","\0\0");
     GetFlightDate(pclResult,1,1);
     GetDataItem(pclResult,pclResult2,ilCount,',',"","\0\0");
     ConvertFlightLine(pclResult);
     pclTmpPtr = pclResult;

     if (pclTmpPtr != NULL)
     {
        /* Get Airline Code */
        ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
        /* Get Flight Number */
        ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
        /* Destination is always Home Airport */
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pcgHomeAP);
        strcpy(rlTlxResult.FValue,pcgHomeAP);
        strcpy(rlTlxResult.DName,"Dest");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
     }
  } 

  ilRC = SendTlxRes(FDI_FWD);

  return ilRC;
} /* end of HandleFWD  */


static int HandleAFTN_PVG(char *pcpData, char *pcpAftUrno)
{
  int ilRC;
  char pclFunc[]="HandleAFTN_PVG:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[1024];
  char pclFlno[16];
  char pclAlc2[16];
  char pclAlc3[16];
  char pclFltn[16];
  char pclFlns[16];
  char pclAdid[16];
  char pclStoa[16];
  char pclStod[16];
  char *pclTmpPtr;
  char pclResult[16];
  T_TLXRESULT rlTlxResult;

  strcpy(pclSqlBuf,"SELECT FLNO,ALC2,ALC3,FLTN,FLNS,ADID,STOA,STOD FROM AFTTAB WHERE ");
  sprintf(pclSelectBuf,"URNO = %s",pcpAftUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",8,",");
     get_real_item(pclFlno,pclDataBuf,1);
     get_real_item(pclAlc2,pclDataBuf,2);
     get_real_item(pclAlc3,pclDataBuf,3);
     get_real_item(pclFltn,pclDataBuf,4);
     get_real_item(pclFlns,pclDataBuf,5);
     get_real_item(pclAdid,pclDataBuf,6);
     get_real_item(pclStoa,pclDataBuf,7);
     get_real_item(pclStod,pclDataBuf,8);
     dbg(DEBUG,"%s Found Flight",pclFunc);
     cgTlxStatus = FDI_STAT_ASSIGNED;
     strcpy(pcgAftUrno,pcpAftUrno);
     if (pclFlno[2] == ' ')
        strcpy(pclResult,pclAlc2);
     else
        strcpy(pclResult,pclAlc3);
     strcat(pclResult,pclFltn);
     strcat(pclResult,pclFlns);
     pclTmpPtr = pclResult;
     if (pclTmpPtr != NULL)
     {
        /* Get Airline Code */
        ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
        /* Get Flight Number */
        ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pcgHomeAP);
        strcpy(rlTlxResult.FValue,pcgHomeAP);
        if (*pclAdid == 'A')
           strcpy(rlTlxResult.DName,"Dest");
        else
           strcpy(rlTlxResult.DName,"Orig");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pcgAftnTelexType);
        strcpy(rlTlxResult.FValue,pcgAftnTelexType);
        strcpy(rlTlxResult.DName,"TlxTyp");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        if (*pclAdid == 'A')
           strcpy(pcgAftnSchedTime,pclStoa);
        else
           strcpy(pcgAftnSchedTime,pclStod);
     }
  }
  else
  {
     dbg(DEBUG,"%s No Flight found",pclFunc);
     cgTlxStatus = FDI_STAT_FLT_NOT_FOUND;
     strcpy(pcgAftUrno,"");
     strcpy(pcgAftnSchedTime,"");
  }
  close_my_cursor(&slCursor);
  strcpy(pcgAftFieldList,"");
  strcpy(pcgAftValueList,"");
  ilRC = SendTlxRes(FDI_AFTN);

  return ilRC;
} /* end of HandleAFTN_PVG  */


static int HandleUBM(char *pcpData)
{
  int ilRC;
  int ilLine;
  char pclFunc[]="HandleUBM:";
  char *pclData;
  char pclResult[100];
  char pclResult2[100];
  char *pclTmpPtr;
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT rlTlxResult;

  prlInfoCmd = &prgInfoCmd[0][0];
  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  ilLine = 1;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"UBM") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     pcgDataPtr = strstr(pcpData,pclResult);
     if (pcgDataPtr != NULL)
        pcgDataPtr = strstr(pcgDataPtr,"\n");
     if (pcgDataPtr != NULL)
        pcgDataPtr++;
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');

     pclTmpPtr = pclResult2;
     /* Get Airline Code */
     ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
     /* Get Flight Number */
     ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);

     /* It is always an arrival flight */
     memset(&rlTlxResult,0x00,TLXRESULT);
     strcpy(rlTlxResult.DValue,pcgHomeAP);
     strcpy(rlTlxResult.FValue,pcgHomeAP);
     strcpy(rlTlxResult.DName,"Dest");
     CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
  } 

  ilRC = SendTlxRes(FDI_UBM);

  return ilRC;
} /* end of HandleUBM  */


static int HandleCCM(char *pcpData)
{
  int ilRC;
  int ilLine;
  char pclFunc[]="HandleCCM:";
  char *pclData;
  char pclResult[100];
  char pclResult2[100];
  char *pclTmpPtr;
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT rlTlxResult;

  prlInfoCmd = &prgInfoCmd[0][0];
  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  ilLine = 1;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"CCM") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     pcgDataPtr = strstr(pcpData,pclResult);
     if (pcgDataPtr != NULL)
        pcgDataPtr = strstr(pcgDataPtr,"\n");
     if (pcgDataPtr != NULL)
        pcgDataPtr++;
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');

     pclTmpPtr = pclResult2;
     /* Get Airline Code */
     ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
     /* Get Flight Number */
     ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);

     /* It is always an arrival flight */
     memset(&rlTlxResult,0x00,TLXRESULT);
     strcpy(rlTlxResult.DValue,pcgHomeAP);
     strcpy(rlTlxResult.FValue,pcgHomeAP);
     strcpy(rlTlxResult.DName,"Dest");
     CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);

#if 0
     /* Get Destination */
     GetDataItem(pclResult,pclResult2,3,',',"","\0\0");
     if (strlen(pclResult) == 3)
     {
        dbg(DEBUG,"%s Found Destination <%s>",pclFunc,pclResult); 
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pclResult);
        strcpy(rlTlxResult.FValue,pclResult);
        strcpy(rlTlxResult.DName,"Dest");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
     }
     else
     {
        GetDataItem(pclResult,pclResult2,4,',',"","\0\0");
        if (strlen(pclResult) == 3)
        {
           dbg(DEBUG,"%s Found Destination <%s>",pclFunc,pclResult); 
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pclResult);
           strcpy(rlTlxResult.FValue,pclResult);
           strcpy(rlTlxResult.DName,"Dest");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        }
     }
#endif

  } 

  ilRC = SendTlxRes(FDI_CCM);

  return ilRC;
} /* end of HandleCCM  */


