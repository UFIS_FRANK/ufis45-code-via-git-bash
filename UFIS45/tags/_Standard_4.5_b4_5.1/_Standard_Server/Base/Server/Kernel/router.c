#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/router.c 1.4 2004/10/15 14:13:07SGT bst Exp  $";
#endif /* _DEF_mks_version */

/* ******************************************************************** */
/*																								*/
/* CEDA Program Skeleton																*/
/*																								*/
/* Author	:	GSA																		*/
/* Date		: 28/06/97																	*/
/* Description		:	Load sharing router											*/
/*																								*/
/* Update history	:																		*/
/*    20040810 jim : removed sccs_ version string                            */
/*																								*/
/* ******************************************************************** */


/* This program is a CEDA main program */
#define U_MAIN
#define CEDA_PRG
#define STH_USE

/* The master header file */
#include  "router.h"
#include  "tools.h"

/* ASCII-time GMT */
#define CTIME(t) asctime(gmtime(t))
#define MAX_ID_LEN 10 /* Max length of Queue id */
#define STR 7
#define INT 4
#define SHORT 3

/* ***************************************************************** */
/* External variables							*/
/* ***************************************************************** */
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp;*/
int debug_level=TRACE;

/* ************************************************************** */
/* Global variables							*/
/* ************************************************************** */
static	ITEM			*item  = NULL;			/* The Queue item pointer	*/
static	EVENT			*event = NULL;			/* The event pointer		*/
static	CMDTAB		*prgCmdTab;	/* Pointer to C3TAB entry */
static 	C1_CMDTAB 	*prgC1CmdTab; /* Pointer to C1TAB entry */
static	CHKTAB		*prgChkTab;	   /* Pointer to C4TAB entry */
static 	C2_CHKTAB 	*prgC2ChkTab; /* Pointer to C2TAB entry */
static	short 		igCmdTabCount; 
static 	short			igC1CmdTabCount;
static	short			igChkTabCount;
static 	short			igC2ChkTabCount;
static 	PN_HEAD 		prgQueTab[NO_OF_PNS];  /**/
static 	int 			igQueIndex=0, igRecCnt=0; 
static 	time_t 		*tp; /* time pointer */
QUE_LST 					*prgQueLst= NULL; /* Queue list */
int 		igQueLstSize=0;

/* BST 2004.10.14: Merged from ATHEN version */
/* ----------------------------------------- */
static char pcgCfgFile[256];
static char pcgCedaCfg[256];
static char pcgRoWksNames[512];
static char pcgRoUsrNames[512];
static char pcgReadOnlyCmd[512];
/* ----------------------------------------- */


/* ******************************************************** */
/* External functions							*/
/* ******************************************************** */
extern	int			init_que();		/* Attach to CEDA Queues	*/
extern	int			que(int, int, int, int, int, char*);
extern	int			queue_overview(int, int, int);
extern void DeleteOldQueues(char*);
extern	void			catch_all();		/* Catches all signals		*/
extern  	int			send_message(int,int,int,int,char*);

/* *********************************************************** */
/* Function prototypes							*/
/* *********************************************************** */
static int init_router();
static int	reset();		/* Reset program		*/
static void	terminate(int);		/* Terminate program		*/
static void	handle_sig();		/* Handles signals		*/
static void	HandleSignal(int ipSig);/* Handles signals		*/
static void	handle_err(int);	/* Handles general errors	*/
static void	handle_qerr(int);	/* Handles queuing errors	*/
static int	HandleData();		/* Handles event data		*/
static int 	ReadC1TAB (void);
static int 	ReadC2TAB (void);
static int 	ReadC3TAB (void);
static int 	ReadC4TAB (void);
static int 	GetDestination (char *command,char *obj);
static int      SearchQueTab(char *);
static int 	SetupQueTab(void);
static int 	BuildQueTab(char *, char *);
static void PrintQueTab(void);
static void PrintQueLst(int ,QUE_LST []);
static int 	GetShortestQueues(int len,QUE_LST que_lst[]);
static int 	GetLeastRecentQueue(int len,QUE_LST que_lst[]);
static void QueCpy(QUE_LST *to,QUE_LST *from);
static int GetCedaCfg(char *pcpGroup, char *pcpLine,
                      char *pcpDestBuff, char *pcpDefault);

/* ***************************************************************** */
/* The MAIN program							*/
/* **************************************************************** */
MAIN 
{
	int	rc;

	/* General initialization	*/
	INITIALIZE;

	/* system table handler init */
	STH_INIT();

	dbg(TRACE,"<MAIN><%s>",mks_version); 
	debug_level=DEBUG;
	/* for signal handling */
  (void)SetSignals(HandleSignal);
	debug_level=TRACE;

	/* get memory */
	if ((tp = (time_t*)malloc(sizeof(time_t))) == NULL)
	{
		dbg(TRACE,"MAIN: %05d Alloc failure time_t *tp", __LINE__);
		rc=RC_FAIL;
		return rc;
	}

	/* init something */
 	if ((rc = init_router()) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> %05d init_router failed, terminating now", __LINE__);
		terminate(45);
	}
	dbg(TRACE,"<MAIN> intializing OK..."); 

	/* forever */
  	while(1)  
	{
    dbg(TRACE,"================= START/END =========================");
    rc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char*)&item);
		/* don't forget to set event pointer */
		event = (EVENT*)item->text;
		/* check returncode */
		if (rc == RC_SUCCESS) 
		{
      	rc = que(QUE_ACK,0,mod_id,0,0,NULL);
      	if (rc != RC_SUCCESS) 
			{
				handle_qerr(rc);
      	}
      
      	switch (event->command) 
      	{
      		case	SHUTDOWN:
					terminate(0);
					break;
	
      		case	RESET:
					rc = reset();
					break;
	
      		case	EVENT_DATA:
					if (ctrl_sta==HSB_ACTIVE || ctrl_sta==HSB_STANDALONE )
					{
						rc = HandleData();
					}
					break;
				case TRACE_ON:
				case TRACE_OFF:
					dbg_handle_debug(event->command);
					break;

      		case	HSB_DOWN:
					terminate(0);
					break;

				case	HSB_STANDBY:
				case	HSB_ACTIVE:
				case	HSB_COMMING_UP:
				case	HSB_STANDALONE:
				case	HSB_ACT_TO_SBY:
					ctrl_sta = event->command;
					send_message(IPRIO_ADMIN,HSB_REQUEST,0,0,NULL);
					break;

				case 1000:
					PrintQueTab();
					break;

				default:
					break;
      	}
      
      	if (rc != RC_SUCCESS) 
				{
					handle_err(rc);
     	 	}
    	}
    	else 
			{
      	handle_qerr(rc);
    	}

				
    	}
  	
  exit(0);
}

/* ****************************************
* Function: init_router
* Parameter: void
* Return: void
* Description: The initialization routine
* *************************************** */
static int init_router() 
{
	int	i = 0;
	int	ilRC = 0;
	int	ilCnt = 0;
        int     ilGetRc = RC_SUCCESS;
        char pclActPath[128];
        char pclTmpLine[512];
        char *pclPtrPath = NULL;

	dbg(DEBUG,"<init_router> %05d ---- START ----", __LINE__);

	do
	{
		ilRC = init_que();
		if (ilRC != RC_SUCCESS)
		{
			ilCnt++;
			sleep(10);
			dbg(TRACE,"<init_router> %05d init_que failed, counter: %d", __LINE__, ilCnt);
		}
	} while ((ilRC != RC_SUCCESS) && ilCnt < 10); 

	if (ilRC != RC_SUCCESS)
		return ilRC;

 	if ((ilRC = SetupQueTab()) != RC_SUCCESS)
	{
		dbg(TRACE,"<init_router> %05d Error encountered whilst setting up que tab", __LINE__);
		return ilRC;
	}



	/* Ensure downward compatibility */
	(void) ReadC1TAB();
	(void) ReadC2TAB();
	(void) ReadC3TAB();
	(void) ReadC4TAB(); 

  /* BST 2004.10.14: Merged from ATHEN version */
  /* ----------------------------------------- */
  /* Attach Config File */
  if ((pclPtrPath = getenv("CFG_PATH")) == NULL)
  {
    strcpy(pclActPath,"/ceda/conf");
  } /* end if */
  else
  {
    strcpy(pclActPath,pclPtrPath);
  } /* end else */
  sprintf(pcgCfgFile, "%s/%s.cfg", pclActPath,mod_name);
  sprintf(pcgCedaCfg, "%s/ufis_ceda.cfg", pclActPath);
  dbg(TRACE,"CONFIG <%s>",pcgCfgFile);
  dbg(TRACE,"CONFIG <%s>",pcgCedaCfg);

  ilGetRc = GetCedaCfg("EVENT_MAPPING","READONLY_CMD",pclTmpLine,
       "RT,RTA,GPR,GFR,GFRC,SYS,SBC,RBC,RBS,GMU,GMN,GNU");
  sprintf(pcgReadOnlyCmd,",%s,",pclTmpLine);
  dbg(TRACE,"READONLY_CMD <%s>",pclTmpLine);

  ilGetRc = GetCedaCfg("EVENT_MAPPING","READONLY_WKS",pclTmpLine,".\0");
  sprintf(pcgRoWksNames,",%s,",pclTmpLine);
  dbg(TRACE,"READONLY_WKS <%s>",pclTmpLine);

  ilGetRc = GetCedaCfg("EVENT_MAPPING","READONLY_USR",pclTmpLine,".\0");
  sprintf(pcgRoUsrNames,",%s,",pclTmpLine);
  dbg(TRACE,"READONLY_USR <%s>",pclTmpLine);
  /* ----------------------------------------- */

	dbg(DEBUG,"<init_router> %05d ---- END ----", __LINE__);
	/* PrintQueTab(); */
  	return RC_SUCCESS;
}

/* *******************************
* Function: 	reset
* Parameter: 	void
* Return: 		RC_SUCCESS, RC_FAIL.
* Description: The reset routine	
* ******************************* */
static int reset() 
{
	dbg(DEBUG,"<reset> %05d ---- START ----", __LINE__);
	(void) ReadC1TAB();
  	(void) ReadC2TAB();
  	(void) ReadC3TAB();
  	(void) ReadC4TAB();
	dbg(DEBUG,"<reset> %05d ---- END ----", __LINE__);
  	return RC_SUCCESS;
}

/* **************************************
* Function: terminate
* Parameter: void
* Return:    void
* Description: The termination routine
**************************************** */
static void terminate(int ipSleepTime) 
{
	int ilRC;
	dbg(DEBUG,"<terminate> ---- START ----");
	if (ipSleepTime > 0)
	{
		dbg(TRACE,"<terminate> sleeping %d seconds...", ipSleepTime);
		sleep(ipSleepTime);
	}

	dbg(DEBUG,"<terminate> ---- END ----");
  	dbg(TRACE,"Now Leaving.....");
  	exit(0);
}
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
  if (ipSig != SIGALRM && ipSig != SIGCLD)
    dbg(TRACE,"<HandleSignal> signal <%d> received", ipSig);

  switch(ipSig)
  {
    case SIGCLD:
    case SIGALRM:
      /* don't call SetSignals here */
      break;

    default:
      exit(0);
      break;
  } 
  return;
}
#if 0
/* *************************************** *
* Function: handle_sig
* Parameter: void
* Return:    void
* Description: Handles signal routine			
* **************************************** */
static void handle_sig()
{
	dbg(DEBUG,"<handle_sig> ---- START ----");
  	exit(1);
}
#endif

/* **************************************** 
* Function: handle_err
* Parameter: IN err Error code
* Return:    void
* Description: The handle general error routine				
* ******************************************* */
static void handle_err(int err) 
{ 
	dbg(DEBUG,"<handle_err> ---- START ----");
  	dbg(TRACE,"<handle_err> receiving error code: %d", err);
	dbg(DEBUG,"<handle_err> ---- END ----");
  	return;
}

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void handle_qerr(int pipErr) 
{
	dbg(DEBUG,"<handle_qerr> ---- START ----");
	switch(pipErr) 
	{
		case	QUE_E_FUNC	:	/* Unknown function */
			dbg(TRACE,"<%d> : unknown function",pipErr);
			break;
		case	QUE_E_MEMORY	:	/* Malloc reports no memory */
			dbg(TRACE,"<%d> : malloc failed",pipErr);
			break;
		case	QUE_E_SEND	:	/* Error using msgsnd */
			dbg(TRACE,"<%d> : msgsnd failed",pipErr);
			break;
		case	QUE_E_GET	:	/* Error using msgrcv */
			dbg(TRACE,"<%d> : msgrcv failed",pipErr);
			break;
		case	QUE_E_EXISTS	:
			dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
			break;
		case	QUE_E_NOFIND	:
			dbg(TRACE,"<%d> : route not found ",pipErr);
			break;
		case	QUE_E_ACKUNEX	:
			dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
			break;
		case	QUE_E_STATUS	:
			dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
			break;
		case	QUE_E_INACTIVE	:
			dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
			break;
		case	QUE_E_MISACK	:
			dbg(TRACE,"<%d> : missing ack ",pipErr);
			break;
		case	QUE_E_NOQUEUES	:
			dbg(TRACE,"<%d> : queue does not exist",pipErr);
			break;
		case	QUE_E_RESP	:	/* No response on CREATE */
			dbg(TRACE,"<%d> : no response on create",pipErr);
			break;
		case	QUE_E_FULL	:
			dbg(TRACE,"<%d> : too many route destinations",pipErr);
			break;
		case	QUE_E_NOMSG	:	/* No message on queue */
			dbg(TRACE,"<%d> : no messages on queue",pipErr);
			break;
		case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
			dbg(TRACE,"<%d> : invalid originator=0",pipErr);
			break;
		case	QUE_E_NOINIT	:	/* Queues is not initialized*/
			dbg(TRACE,"<%d> : queues are not initialized",pipErr);
			break;
		case	QUE_E_ITOBIG	:
			dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
			break;
		case	QUE_E_BUFSIZ	:
			dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
			break;
		default			:	/* Unknown queue error */
			dbg(TRACE,"<%d> : unknown error",pipErr);
			break;
	}
	dbg(DEBUG,"<handle_qerr> ---- END ----");
	/* bye bye */
	return;
} 

/* **************************************************** 
* Function: 		HandleData
* Parameter: 		void
* Return:    		RC_FAIL, RC_SUCCESS
* Description:	The handle data routine	
****************************************************** */
static int HandleData() 
{
  	int		rc = 0;			/* Return code 			*/
  	int		len;			/* Len of buffer		*/
  	int		dest;			/* configured destination       */
  	BC_HEAD 	*bchd;			/* Broadcast header		*/
  	CMDBLK  	*cmdblk;		/* Command Block 		*/
  	DATABLK 	*datablk;		/* Data Block			*/
  	char 		buf[60];

        char    pclChkCmd[64];
        char    pclChkUsr[64];
        char    pclChkWks[64];
        char    pclDestName[64];
        char    pclRecvName[64];
        char *pclSelKey;
        char *pclFields;
        char *pclData;

  time_t tlTi = 0; /* time value */

	dbg(DEBUG,"<HandleData> ====================================");

	/* set local pointer */
  	bchd  = (BC_HEAD *) ((char *)event + sizeof(EVENT));
  	cmdblk= (CMDBLK  *) ((char *)bchd->data);
  	len = sizeof(EVENT)+event->data_length;

  /* BST 2004.10.14: Merged from ATHEN version */
  /* ----------------------------------------- */
  sprintf(pclChkCmd,",%s,",cmdblk->command);
  /* WorkStation */
  memset(pclRecvName, '\0', (sizeof(bchd->recv_name) + 1));
  strncpy(pclRecvName, bchd->recv_name, sizeof(bchd->recv_name));

  /* User */
  memset(pclDestName, '\0', (sizeof(bchd->dest_name) + 1));
  strncpy(pclDestName, bchd->dest_name, sizeof(bchd->dest_name));

  sprintf(pclChkWks,",%s,",pclRecvName);
  sprintf(pclChkUsr,",%s,",pclDestName);

  dbg(TRACE,"CMD <%s> QUE (%d) WKS <%s> USR <%s>",
             cmdblk->command,event->originator,pclRecvName,pclDestName);
  dbg(TRACE,"SPECIAL INFO TWS <%s> TWE <%s>",
             cmdblk->tw_start,cmdblk->tw_end);

  pclSelKey = (char *)cmdblk->data;
  pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  dbg(TRACE,"TABLE  <%s>", cmdblk->obj_name);
  dbg(TRACE,"FIELDS <%s>", pclFields);
  if (strcmp(pclChkCmd,",GPR,") != 0)
  {
    dbg(TRACE,"DATA   <%s>", pclData);
  } /* end if */
  dbg(TRACE,"SELECT <%s>", pclSelKey);


  if (strstr(pcgReadOnlyCmd,pclChkCmd) == NULL)
  {
    if ((strstr(pcgRoWksNames,pclChkWks) != NULL) ||
        (strstr(pcgRoUsrNames,pclChkUsr) != NULL))
    {
      dbg(TRACE,"ILLEGAL COMMAND OF A READ ONLY CLIENT REJECTED");
      dbg(TRACE,"WKS <%s> USR <%s> TWE <%s>",
               pclRecvName,pclDestName,cmdblk->tw_end);
      dbg(TRACE,"CMD <%s> TBL <%s>", cmdblk->command,cmdblk->obj_name);
      dbg(TRACE,"------------------------------");
      sprintf(buf,"CEDA ROUTER:\nIllegal transaction!\n(Read Only Client)");
      rc = tools_send_sql_rc(event->originator,"","","","",buf,RC_FAIL);
      return rc;
    } /* end if */
  } /* end if */
  /* ----------------------------------------- */


	/* dbg(TRACE,"TIME FROM SENDER <%s>",bchd->orig_name); */
	if ((bchd->orig_name[0] < '0') || (bchd->orig_name[0] > '9'))
	{
		tlTi = time(0L) % 10000;
		sprintf(bchd->orig_name,"%d,0",tlTi);
		/* dbg(TRACE,"MY TIME <%s>",bchd->orig_name); */
	} /* end if */

  dest = GetDestination(cmdblk->command,cmdblk->obj_name);

  if (dest == RC_FAIL) 
	{
    	sprintf(buf,"<%s>: Unknown CEDA command <%s>!",mod_name,cmdblk->command);
    	rc = tools_send_sql_rc(event->originator,"","","","",buf,RC_FAIL);
	dbg(TRACE,"NO DESTINATION FOUND. COMMAND REJECTED.");
  } 
	else 
	{
		dbg(TRACE,"EVENT FORWARDED TO %d PRIO=%d",dest,item->priority);
		rc=que(QUE_PUT,dest,mod_id,item->priority,len,(char *)event); 
  }
	/* bye bye */
  	return rc;
} 

/**************************** *
* Function: ReadC1TAB
* Parameter: void
* Return: RC_FAIL, RC_SUCCESS
* Description: Reads C1TAB.
* *************************** */
static int ReadC1TAB (void) 
{
  	int 		rc = RC_SUCCESS;
  	int 		i, ret = 0;
  	char 		buf[20];
  	int 		recno=0;

	dbg(DEBUG,"<ReadC1TAB> ---- START ----");

  	rc = sgs_get_no_of_record (C1TAB, buf);
	dbg(DEBUG,"ReadC1TAB/sgs_get_no_of_record, rc=%d", rc);
  	if (rc > 0) 
	{
    	igC1CmdTabCount = rc;
    	rc = RC_SUCCESS;
    	prgC1CmdTab=(C1_CMDTAB *)calloc(igC1CmdTabCount+1,sizeof(C1_CMDTAB));
    	if (prgC1CmdTab==NULL) 
		{
      	dbg(TRACE,"ReadC1TAB: Malloc failed "); 
      	rc = RC_FAIL;
    	}

    	for (recno=0; rc==RC_SUCCESS && recno<igC1CmdTabCount; recno++) 
		{
      	rc = sgs_get_record (C1TAB, recno, buf);
      	if (rc == RC_SUCCESS) 
			{
				strncpy (prgC1CmdTab[recno].command, buf, 5);
				prgC1CmdTab[recno].command[4] = EOS;
				prgC1CmdTab[recno].receiver=(int) *((short *) &(buf[5])); 
				dbg(DEBUG,"ReadC1TAB: Got %s %i", prgC1CmdTab[recno].command, 
				prgC1CmdTab[recno].receiver); 
      	}
    	}
  	}

  	if (rc == RC_NOT_FOUND)
    	rc = RC_SUCCESS;

	dbg(DEBUG,"<ReadC1TAB> ---- END ----");
	return rc;
} 

/**************************** *
* Function: ReadC2TAB
* Parameter: void
* Return: RC_FAIL, RC_SUCCESS
* Description: Reads C2TAB.
* *************************** */
static int ReadC2TAB (void) 
{
  	int 		rc = RC_SUCCESS;
  	int 		ret = 0;
  	char 		buf[20];
  	int 		recno=0;

	dbg(DEBUG,"<ReadC2TAB> ---- START ----");

  	rc = sgs_get_no_of_record (C2TAB, buf);
  	dbg (DEBUG, "ReadC2TAB: sgs_get_no_of_record: rc =%d ",rc); 
  	if (rc > 0) 
	{
    	igC2ChkTabCount = rc;
    	rc = RC_SUCCESS;
    	prgC2ChkTab = (C2_CHKTAB *) calloc (igC2ChkTabCount+1,sizeof(C2_CHKTAB));
    	if (prgC2ChkTab == NULL) 
		{
      	dbg(TRACE,"ReadC2TAB: Malloc failed "); 
      	rc = RC_FAIL;
    	}

    	for (recno=0; rc==RC_SUCCESS && recno<igC2ChkTabCount; recno++) 
		{
      	rc = sgs_get_record (C4TAB, recno, buf);
      	if (rc == RC_SUCCESS) 
			{
				strncpy (prgC2ChkTab[recno].table_name, buf, 6);
				prgC2ChkTab[recno].table_name[6] = EOS;
				strncpy (prgC2ChkTab[recno].cmd, buf+6, 5);
				prgC2ChkTab [recno].cmd[4] = EOS;
				prgC2ChkTab[recno].receiver = (int) ((short *) &(buf[11])); 
				dbg(DEBUG,"ReadC2TAB: Got %s %s %hd", prgChkTab[recno].table_name,
				prgChkTab[recno].cmd, prgChkTab[recno].receiver); 
      	}
    	}
  	}
  
  	if (rc == RC_NOT_FOUND)
    	rc = RC_SUCCESS;

	dbg(DEBUG,"<ReadC2TAB> ---- END ----");
  	return rc;
}

/**************************** *
* Function: ReadC3TAB
* Parameter: void
* Return: RC_FAIL, RC_SUCCESS
* Description: Reads C3TAB.
* *************************** */
static int ReadC3TAB (void) 
{
  	int 		rc = RC_SUCCESS;
  	int 		i,ret = 0,ilLen;
  	char 		buf[20];
  	int 		recno=0;

	dbg(DEBUG,"<ReadC3TAB> ---- START ----");

  	rc = sgs_get_no_of_record (C3TAB, buf);
  	dbg (DEBUG, "ReadC3TAB/sgs_get_no_of_record: rc =%d ",rc); 
  	if (rc > 0) 
	{
    	igCmdTabCount = rc;
    	rc = RC_SUCCESS;
    	prgCmdTab = (CMDTAB *) calloc (igCmdTabCount+1, sizeof(CMDTAB));
    	if (prgCmdTab == NULL) 
		{
      	dbg(TRACE,"ReadC3TAB: Malloc failed "); 
      	rc = RC_FAIL;
    	}

    	for (recno=0; rc == RC_SUCCESS && recno < igCmdTabCount; recno++) 
		{
      	rc = sgs_get_record (C3TAB, recno, buf);
      	if (rc == RC_SUCCESS) 
			{
				ilLen=strlen(buf);
				if(ilLen>=5)
					ilLen=4;
				strncpy(prgCmdTab[recno].command, buf, ilLen);
				prgCmdTab[recno].command[ilLen] = EOS;
				strncpy(prgCmdTab[recno].receiver,buf+5,7); 
      	}
    	}
  	}

  	if (rc == RC_NOT_FOUND)
    	rc = RC_SUCCESS;

	dbg(DEBUG,"<ReadC3TAB> ---- END ----");
  	return rc;
}

/**************************** *
* Function: ReadC4TAB
* Parameter: void
* Return: RC_FAIL, RC_SUCCESS
* Description: Reads C4TAB.
* *************************** */
static int ReadC4TAB (void) 
{
  	int 		rc = RC_SUCCESS;
  	int 		ret = 0;
  	char 		buf[30];
  	int 		recno=0;

	dbg(DEBUG,"<ReadC4TAB> ---- START ----");

  	rc = sgs_get_no_of_record (C4TAB, buf);
  	dbg (DEBUG, "ReadC4TAB/sgs_get_no_of_record: rc =%d ",rc); 
  	if (rc > 0) 
	{
    	igChkTabCount = rc;
    	rc = RC_SUCCESS;
    	prgChkTab = (CHKTAB *)calloc(igChkTabCount+1,sizeof(CHKTAB));
    	if (prgChkTab == NULL) 
		{
      	dbg(TRACE,"ReadC4TAB: Malloc failed "); 
      	rc = RC_FAIL;
    	}

    	for (recno=0; rc == RC_SUCCESS && recno < igChkTabCount; recno++) 
		{
      	rc = sgs_get_record (C4TAB, recno, buf);
      	if (rc == RC_SUCCESS) 
			{
				strncpy (prgChkTab[recno].table_name, buf, 6);
				prgChkTab [recno].table_name[6] = EOS;
				strncpy (prgChkTab[recno].cmd, buf+6, 5);
				prgChkTab [recno].cmd[4] = EOS;
				strncpy (prgChkTab[recno].receiver, buf+11, 7); 
      	}
    	}
  	}
  
  	if (rc == RC_NOT_FOUND)
    	rc = RC_SUCCESS;

	dbg(DEBUG,"<ReadC4TAB> ---- END ----");
  	return rc;
}


/**************************** *
* Function: SetupQueTab
* Parameter: void
* Return: RC_FAIL, RC_SUCCESS
* Description: Reads PNTAB.
* *************************** */
static int SetupQueTab (void) 
{
  	int 		i, rc = RC_SUCCESS;
  	char 		buf[MAX_REC_LEN];
  	int 		recno=0, ready=FALSE;
  	char 		pclPnam[8] /* Process name */;

	dbg(DEBUG,"<SetupQueTab> ---- START ----");

	igQueIndex=0;
	memset(buf,'\0',MAX_REC_LEN);
	memset(pclPnam,'\0',8); 
	rc = sgs_get_no_of_record (PNTAB, buf);
  	dbg (DEBUG,"SetupQueTab/sgs_get_no_of_record:no_of_recs(rc)=%d",rc); 
  	if (rc > 0) 
	{
    	igRecCnt=rc;
    	rc=RC_SUCCESS;
  	}
	else 
	{
		rc=RC_FAIL;
	}

  	for(recno=0; rc==RC_SUCCESS && recno<igRecCnt;recno++) 
	{
		memset(buf,'\0',MAX_REC_LEN);
      rc = sgs_get_record(PNTAB,recno,buf);
     	if (rc == RC_SUCCESS) 
		{
			memcpy(pclPnam,buf,8);
			rc=BuildQueTab(pclPnam,&buf[8]);
		}
  	} 

	dbg(DEBUG,"<SetupQueTab> ---- END ----");
  	return rc;
}

/******************************************************** *
* Function: 		BuildQueTab
* Parameter: 		IN char *pcpPnam Pointer to processs name
*								IN char *pcpIntString
* Return: 			RC_FAIL, RC_SUCCESS
* Description: 	Reads C1TAB.
* ******************************************************* */
static int BuildQueTab(char *pcpPnam,char *pcpIntString) 
{
	int 		found, rc=RC_SUCCESS;
	short 	i;
	int 		ilActIndex,ilQueId, ilIntVal ,ilFieldCnt;
	PN_QUE 	*prlQueElem, *prlQuePtr;

	dbg(DEBUG,"<BuildQueTab> ---- START ----");

	found=FALSE;	
	ilActIndex=igQueIndex; /* Set pointer to end of table */
	
	/* Set process name */
	for(i=0 ;i<igQueIndex ; i++) 
	{
		/* Search for entry in global table */
		if(!strncmp(prgQueTab[i].pcProcessName,pcpPnam,strlen(prgQueTab[i].pcProcessName)))
		{
			dbg(DEBUG,"BuildQueTab: Found %s in QueTab at index %hd",pcpPnam,i);
			ilActIndex=i;
			found=TRUE;
			break;
		}
	}

	if(found==FALSE) 
	{
		dbg(DEBUG,"BuildQueTab: %s not found in QueTab",pcpPnam);
		igQueIndex++;
 		if(strlen(pcpPnam) >=6 )
		{
			/* max. 6 char long */
			strncpy(prgQueTab[ilActIndex].pcProcessName,pcpPnam,6);
  		} 
		else 
		{ 
			/* max. 6 char long */
			strncpy(prgQueTab[ilActIndex].pcProcessName,pcpPnam,strlen(pcpPnam));
		}	

		prgQueTab[ilActIndex].iNoOfQueues=0;
  		prgQueTab[ilActIndex].prLastQueue=NULL;
		prgQueTab[ilActIndex].prQueue=NULL;
	}
 	
	/* No. of fields per record ?? */
#if defined(_SNI) || defined(_HPUX_SOURCE) || defined(_SOLARIS)
	/* read queue id */
  	ilQueId= *(short *)(pcpIntString);
#else
	/* read queue id */
  	ilQueId= *(int *)(pcpIntString);
#endif

	if (ilQueId <= 0) 
	{
		dbg(DEBUG,"BuildQueTab: No process id found<%i>",ilQueId);
		rc=RC_FAIL;
	} 
	else 
	{
		prgQueTab[ilActIndex].iNoOfQueues++;
		prlQueElem=(PN_QUE *)calloc(1,sizeof(PN_QUE));
		if(prlQueElem==NULL)
		{
			dbg(TRACE,"BuildQueTab: Malloc Error prlQueElem");
			rc=RC_FAIL;
		} 
		else 
		{
			if(prgQueTab[ilActIndex].prQueue==NULL)
			{
				/* Empty Table entry list */
				prgQueTab[ilActIndex].prQueue=(PN_QUE *) prlQueElem;
			} 
			else 
			{
				/* Insert element at the end of list */
				prlQuePtr=prgQueTab[ilActIndex].prQueue;
				while(prlQuePtr->prNext!=NULL) 
				{
					prlQuePtr=prlQuePtr->prNext;
				}
				prlQuePtr->prNext=prlQueElem; 
			} 

			/* Initialise Que Element */
			prlQueElem->prNext = NULL; 
			prlQueElem->lTime	 = time(tp);
			prlQueElem->iModID = ilQueId;
		} 
	} 
	dbg(DEBUG,"<BuildQueTab> ---- END ----");
	return rc;
} 


/********************************************** *
* Function: PrintQueTab
* Parameter: void
* Return: void
* Description: 	Prints content of global table
* 							Used for testing SetupQueTab. 
* ********************************************** */
static void PrintQueTab(void) 
{
	int 		i;
	PN_QUE 	*prlPtr;

	dbg(DEBUG,"<PrintQueTab> ---- START ----");
	for(i=0; i<igQueIndex; i++)
	{
		dbg(TRACE,"[%02d] QUE <%s>: CNT=%d MOD=%d",i,prgQueTab[i].pcProcessName,
                           prgQueTab[i].iNoOfQueues,prgQueTab[i].prQueue->iModID);

		if(prgQueTab[i].iNoOfQueues > 1) 
		{
			prlPtr=prgQueTab[i].prLastQueue;
			if(prlPtr!=NULL)
		                dbg(TRACE,"[%02d] QUE <%s>: CNT=%d MOD=%d",i,prgQueTab[i].pcProcessName,
                                          prgQueTab[i].iNoOfQueues,prlPtr->iModID);

			prlPtr=prgQueTab[i].prQueue;	
			do
			{	
				dbg(TRACE,"PrintQueTab: prgQueTab[%i].pcProcessName <%s> Mod id <%i> Time<%s>",i,prgQueTab[i].pcProcessName,prlPtr->iModID,ctime(&prlPtr->lTime));
				prlPtr=prlPtr->prNext;
			} while (prlPtr!=NULL);	 
		} 
		else 
		{
			dbg(TRACE,"PrintQueTab: Unique queue.");
		}
	}

	dbg(DEBUG,"<PrintQueTab> ---- END ----");
	return;
}

/************************************************************ *
* Function: 	PrintQueLst
* Parameter: 	IN int ipQueCnt No. of valid queue list entries
*							IN QUE_LST prpQueLst[] List of queue belonging 
*							process in question
* Return: void
* Description: Used to print global queue list
****************************************************************/
static void PrintQueLst(int ipQueCnt, QUE_LST prpQueLst[]) 
{
	int i;

	dbg(DEBUG,"<PrintQueLst> ---- START ----");
	for(i=0; i<ipQueCnt; i++)
	{
		dbg(DEBUG,"PrintQueLst: prpQueLst[%i].mod_id=<%i>",i,prpQueLst[i].prQueue->iModID);
		dbg(DEBUG,"PrintQueLst: prpQueLst[%i].iNoOfItems=<%i>",i,prpQueLst[i].iNoOfItems);
		*tp=prpQueLst[i].prQueue->lTime;
		dbg(DEBUG,"PrintQueLst: prpQueLst%i].time <%s>",i,ctime(tp));
	}

	dbg(DEBUG,"<PrintQueLst> ---- END ----");
	return;
}

/* ***************************************************** 
* Function: 		GetDestination()						
* Parameter: 		IN char *pcpCommand - CEDA Command
*								IN char *pcpObj  - DB Table
* Return: 			Mod_id or Fail code.
* Description:	Determines queue id of receiver process 
* ******************************************************* */
static int GetDestination (char *pcpCommand, char *pcpObj) 
{
	int			rc;
  short			i;
  CMDTAB		*work;
  CHKTAB		*chk;
  C1_CMDTAB 	*c1_work;
	C2_CHKTAB 	*c2_chk;

	dbg(DEBUG,"<GetDestination> ---------------- START --------------");
	dbg(DEBUG,"<GetDestination> CMD=<%s> OBJ=<%s>",pcpCommand, pcpObj);

	dbg(DEBUG,"<GetDestination> Checking <C4TAB> for CMD + OBJ.");
  for (chk=prgChkTab, i=0; i<igChkTabCount; i++,chk++ ) 
	{
		/******************
   	if(strncmp(pcpObj,chk->table_name,strlen(pcpObj)) == 0 && strncmp(pcpCommand,chk->cmd,strlen(pcpCommand)) == 0) 
		******************/
   	if (!strcmp(pcpObj,chk->table_name) && !strcmp(pcpCommand,chk->cmd))
		{
			dbg(TRACE,"C4TAB FOUND DEST: <%s>",chk->receiver);
			/* If receiver is numeric load shaing in SYSQCP */
			
			if (isdigit( (int) *chk->receiver))
			{
	 			rc = atoi(chk->receiver);
			} /* end if */
			else
			{
				rc = SearchQueTab(chk->receiver);
			}
			
	 		
		 	dbg(DEBUG,"<GetDestination> <C4TAB> Sending request to Queue <%i>",rc);
			dbg(DEBUG,"<GetDestination> ----------------- END ---------------");
			return rc;
   	}
  }
 	dbg(DEBUG,"<GetDestination> NOT FOUND in <C4TAB>."); 

	dbg(DEBUG,"<GetDestination> Checking <C3TAB> for CMD.");
  for (work=prgCmdTab, i=0; i<igCmdTabCount; i++,work++ ) 
	{
		/*****************
   	if (strncmp(pcpCommand,work->command,strlen(pcpCommand)) == 0 ) 
		*****************/
   	if (!strcmp(pcpCommand, work->command)) 
		{
			dbg(TRACE,"C3TAB FOUND DEST: <%s>",work->receiver);
			/* If receiver is numeric load shaing in SYSQCP */
			
			if (isdigit( (int) *work->receiver))
			{
	 			rc = atoi(work->receiver);
			} /* end if */
			else
			{
				rc = SearchQueTab(work->receiver);
			}
			
	 		
		 	dbg(DEBUG,"<GetDestination> <C3TAB> Sending request to Queue <%i>",rc);
			dbg(DEBUG,"<GetDestination> ----------------- END ---------------");
			return rc;
   	}
  }
 	dbg(DEBUG,"<GetDestination> NOT FOUND in <C3TAB>."); 

	dbg(DEBUG,"<GetDestination> Checking <C2TAB> for CMD + OBJ.");
	for (c2_chk=prgC2ChkTab, i=0; i<igC2ChkTabCount; i++,c2_chk++ )
	{
		/*******************
  		if(strncmp(pcpObj,chk->table_name,strlen(pcpObj)) == 0 && strncmp(pcpCommand,chk->cmd,strlen(pcpCommand)) == 0) 
		*******************/
  		if(!strcmp(pcpObj,chk->table_name) && !strcmp(pcpCommand,chk->cmd))
		{
	 		rc= c2_chk->receiver;
		 	dbg(DEBUG,"<GetDestination> <C2TAB> Sending request to Queue <%i>",rc);
			dbg(DEBUG,"<GetDestination> ----------------- END ---------------");
		 	return rc;
   	}
  }
 	dbg(DEBUG,"<GetDestination> NOT FOUND in <C2TAB>."); 

	dbg(DEBUG,"<GetDestination> Checking <C1TAB> for CMD.");
  for (c1_work=prgC1CmdTab, i=0; i<igC1CmdTabCount; i++,c1_work++ ) 
	{
		/**************
   	if (strncmp(pcpCommand,c1_work->command,strlen(pcpCommand)) == 0 ) 
		**************/
   	if (!strcmp(pcpCommand,c1_work->command))
		{
	 		rc=c1_work->receiver;
		 	dbg(DEBUG,"<GetDestination> <C1TAB> Sending request to Queue <%i>",rc);
			dbg(DEBUG,"<GetDestination> ----------------- END ---------------");
		 	return rc;
   	}
  }
 	dbg(DEBUG,"<GetDestination> NOT FOUND in <C1TAB>."); 
	dbg(DEBUG,"<GetDestination> ----------------- END ---------------");
  return RC_FAIL;
} 

/************************************************************* *
* Function: GetShortestQueue
* Parameter: 	IN int ipLen No. of valid list entries
*							IN QUE_LST prpQueLst[] List of queues
* Return: RC_FAIL, ilQueCnt (no. of valid queue list entries)
* Description: Determines shortest queue(s) and builds a 
* 						new prpQueLst.
* *************************************************************/
static int GetShortestQueues(int ipLen,QUE_LST prpQueLst[]) 
{
	int 	i, min_iNoOfItems=0;
	int 	que_cnt=1;

	dbg(DEBUG,"<GetShortestQueues> ---- START ----");

	min_iNoOfItems=prpQueLst[0].iNoOfItems;
	for(i=1; i<ipLen ; i++) 
	{
		if(min_iNoOfItems>prpQueLst[i].iNoOfItems) 
		{
			que_cnt=0;
			min_iNoOfItems=prpQueLst[i].iNoOfItems;
			QueCpy(&prpQueLst[que_cnt++],&prpQueLst[i]);
		} 
		else if (min_iNoOfItems==prpQueLst[i].iNoOfItems) 
		{
			QueCpy(&prpQueLst[que_cnt++],&prpQueLst[i]);
			dbg(DEBUG,"<GetShortestQueue> min_iNoOfItems <%d>",min_iNoOfItems);
		}	
	}
	dbg(DEBUG,"<GetShortestQueue> return <%d>.",que_cnt);
	dbg(DEBUG,"<GetShortestQueues> ---- END ----");
	return que_cnt;
} 

/********************************************************************* *
* Function: 		QueCpy
* Parameter: 		IN QUE_LST *prpTo Pointer to source queue list element 
*								IN QUE_LST *prpFrom Pointer to target queue list element
* Return: 			void
* Description: 	Copies queue list element within queue list
* *********************************************************************/
static void QueCpy(QUE_LST *prpTo,QUE_LST *prpFrom)
{
	dbg(DEBUG,"<QueCpy> ---- START ----");
	prpTo->prQueue=prpFrom->prQueue;
	prpTo->iNoOfItems=prpFrom->iNoOfItems;
	dbg(DEBUG,"<QueCpy> ---- END ----");
	return;
}

/* *****************************************************
* Function: GetLeastRecentQueue
* Parameter: 	IN int ipLen No. of valid list entries
*							IN QUE_LST prpQueLst[] List of queues
* Return: RC_FAIL, RC_SUCCESS
* Description: Determines least recently used queue
* ****************************************************/
static int GetLeastRecentQueue(int ipLen,QUE_LST prpQueLst[]) 
{
	double 	time_diff;
	int 		rc=RC_SUCCESS,i,min_index=0;

	dbg(DEBUG,"<GetLeastRecentQueues> ---- START ----");
	for(i=1; i<ipLen; i++) 
	{
		time_diff=difftime(prpQueLst[min_index].prQueue->lTime,prpQueLst[i].prQueue->lTime);
		if(time_diff>0) 
		{
			min_index=i;
		}
	}
	dbg(DEBUG,"<GetLeastRecentQueues> return <%d> as least recent used queue!"
		,prpQueLst[min_index].prQueue->iModID);
	dbg(DEBUG,"<GetLeastRecentQueues> ---- END ----");
	return prpQueLst[min_index].prQueue->iModID;
}

/*******************************************************/
/* BST 2004.10.14: Merged from ATHEN version */
/*******************************************************/
static int GetCedaCfg(char *pcpGroup, char *pcpLine,
                      char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;
  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow(pcgCedaCfg,pcpGroup,pcpLine,CFG_STRING,pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy(pcpDestBuff,pcpDefault);
  } /* end if */
} /* end GetCedaCfg */





/******************************************* *
* Function:             SearchQueTab
* Parameter:            IN char *pcpPnam
* Return:                       RC_FAIL, RC_SUCCESS
* Description:  Searches global queue table
*                                                               for process *pcpPnam
* ****************************************** */
static int SearchQueTab(char *pcpPnam)
{
        int             found, rc=RC_SUCCESS;
        short   i;
        int             ilPriorities=31; /* Search all priorities */
        int             ilQueId, ilIntVal ,ilFieldCnt;
        int             ilActIndex,ilNoOfQueues;
        PN_QUE  *prlQueElem, *prlQuePtr;

        dbg(DEBUG,"<SearchQueTab> ---- START ----");

        found=FALSE;
        for(i=0 ;i<igQueIndex ; i++)
        {
                /* Search for entry in global table */
                if(!strncmp(prgQueTab[i].pcProcessName,pcpPnam,strlen(prgQueTab[i].pcProcessName)))
                {
                        ilActIndex=i;
                        if(prgQueTab[i].prLastQueue!=NULL)
                        {
                                prlQuePtr=prgQueTab[i].prLastQueue;
                        }
                        else
                        {
                                prlQuePtr=prgQueTab[i].prQueue;
                        }

                        ilNoOfQueues=prgQueTab[i].iNoOfQueues;
                        found=TRUE;
                        break;
                }
        }

        if(found==FALSE)
        {
                dbg(DEBUG,"SearchQueTab:Error!Process %s not found in QueTab",pcpPnam);
                rc=RC_FAIL;
                dbg(DEBUG,"<SearchQueTab> ---- END ----");
                return rc;
        }
        else
        {
                if(ilNoOfQueues==1)
                {
                        tp=&prlQuePtr->lTime;
                        time(tp);
                        prlQuePtr->lTime=*tp;
                        rc= prlQuePtr->iModID;
                }
                else
                {
                        if (prgQueLst==NULL || igQueLstSize<ilNoOfQueues)
                        {
                                if(prgQueLst!=NULL)
                                {
                                        free(prgQueLst);
                                }

                                prgQueLst=(QUE_LST *)(calloc(ilNoOfQueues,sizeof(QUE_LST)));
                                if(prgQueLst==NULL)
                                {
                                        igQueLstSize=0;
                                        rc=RC_FAIL;
                                        dbg(DEBUG,"<SearchQueTab> ---- END ----");
                                        return rc;
                                }
                                igQueLstSize=ilNoOfQueues;
                        }

                        /* Build queue list */
                        for(i=0; i<ilNoOfQueues; i++)
                        {
                                prgQueLst[i].prQueue=prlQuePtr;
                                /*
                                removed by BERNI 20.04.2001
                                prgQueLst[i].iNoOfItems=get_que_items(prlQuePtr->iModID,ilPriorities);
                                */
                                /* and put into by BERNI : */
                                prgQueLst[i].iNoOfItems = 0;

                                if(prgQueLst[i].iNoOfItems<0)
                                {
                                        rc=prgQueLst[i].iNoOfItems;
                                        handle_qerr(rc);
                                        rc=RC_FAIL;
                                        dbg(DEBUG,"<SearchQueTab> ---- END ----");
                                        return rc;
                                }
                                else if (prgQueLst[i].iNoOfItems==0 && prlQuePtr!=prgQueTab[ilActIndex].prLastQueue)
                                {
                                        prgQueTab[ilActIndex].prLastQueue=prlQuePtr;
                                        prgQueLst[0].prQueue->lTime=time(tp);
                                        rc=prlQuePtr->iModID;
                                        dbg(DEBUG,"<SearchQueTab> ---- END ----");
                                        return rc;
                                }
                                if(prlQuePtr->prNext!=NULL)
                                {
                                        prlQuePtr=prlQuePtr->prNext;
                                }
                                else
                                {
                                        prlQuePtr=prgQueTab[ilActIndex].prQueue;
                                }
                        }

                        /* Reduce list to list of shortest queue(s) */
                        rc=GetShortestQueues(ilNoOfQueues,prgQueLst);
                        if (rc==1)
                        {
                                prgQueTab[ilActIndex].prLastQueue=prgQueLst[0].prQueue;
                                rc=prgQueLst[0].prQueue->iModID;
                        }
                        else
                        {
                                rc=GetLeastRecentQueue(rc,prgQueLst);
                                if (rc<=0)
                                {
                                        rc=RC_FAIL;
                                        dbg(DEBUG,"<SearchQueTab> ---- END ----");
                                        return rc;
                                }
                                else
                                {
                                        prgQueTab[ilActIndex].prLastQueue=prgQueLst[0].prQueue;
                                        rc=prgQueLst[0].prQueue->iModID;
                                }
                        }
                        prgQueLst[0].prQueue->lTime=time(tp);
                }
        }

        dbg(DEBUG,"<SearchQueTab> ---- END ----");
        return rc;
}


