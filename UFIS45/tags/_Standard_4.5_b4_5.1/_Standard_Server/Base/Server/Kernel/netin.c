#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/netin.c 1.18 2010/05/27 13:25:18SGT bst Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*                                  */
/* CEDA Program Skeleton                        */
/*                                  */
/* Author       : Ralf Hundertmark              */
/* Date         : 09/11/93                  */
/* Description      : Network Interface                 */
/*                                  */
/* Update history   :                       */
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/* 20020826 JIM: debug_level=0 as default */
/* 20040109 JIM: 'int MAIN' kollidiert mit dem neuen DEFINE 'MAIN' fuer      */
/*               Versionsausgabe in Zeile 0 jeder Log-Datei                  */
/* 20040810 jim : removed sccs_ version string                            */
/* 20040826 JIM: don't look on second queue for TERM or DEBUG/TRACE       */
/* 20040826 JIM: instead of loop for que()-timeout use alarm(timeout)     */
/* 20050113 JIM: TRACE error exit and queue delete                        */
/* 20050204 JIM: use pid of child netin for QUE_CREATE of child to avoid  */
/*               two NETIN using the same queue                           */
/* 20050204 JIM: use debug_level from parent NETIN, go to trace only if   */
/*               QUE_CREATE failes. Use "qput 1800 7" to modify debug_level*/
/*               of  parent NETIN debug_level                             */
/* 20050204 JIM: if no DBG written, delete logfile !                      */
/* 20050429 JIM: PRF 7209: if RBS detected, check and correct hex IP      */
/*               address in SEL against NETIN target IP address           */
/* 20050511 JIM: PRF 7291: debug_level for childs set to OFF again        */
/* 20070226 JIM: PRF8450 on AIX the pids are not continously increasing   */
/*               but random, so the modulo may create same tag at 2       */
/*               simultaniosly starting NETINs. Use complete PID          */
/* ******************************************************************** */


/* This program is a CEDA main program */

#define U_MAIN

#define CEDA_PRG    /* ---              */
#define STH_USE

/* New netin with sgs.tab-EXTAB entry*/
#define USE_EXTAB_HOSTNAME

/* Send performance messages to the CEDAMO process */
#undef SUPPORT_CEDAMO

#ifdef _HPUX_SOURCE
#ifndef USE_BCHEAD_NETWORK_BYTE_ORDER
#define USE_BCHEAD_NETWORK_BYTE_ORDER
#endif
#else
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER
#undef USE_BCHEAD_NETWORK_BYTE_ORDER    
#endif
#endif


/* The master header file */
 
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdarg.h>
#ifndef _WINNT
#include <arpa/inet.h>
#endif
#include  "netin.h"
#include  "tcputil.h"
#include  "bchdl.h"
#include  "monutl.h"
#include  "nmghdl.h"
#include  "new_catch.h"
#include  "debugrec.h"


#define UFIS_SERVICE_NAME       "UFIS"
#define MAX_NO_OF_CONNECTIONS   (20)
#define QUE_TIMEOUT         (480)
#define TCP_TIMEOUT         (10)

#ifdef MAX_EVENT_SIZE
#undef MAX_EVENT_SIZE
#endif
#define MAX_EVENT_SIZE 4096

/* Length for event struct */
#define PACKETDATALEN  (PACKET_LEN-sizeof(EVENT))

#define ALARM(ptimeout) alarm(ptimeout)

/* ******************************************************************** */
/* External variables                           */
/* ******************************************************************** */
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE  *outp;*/
extern int  errno;
int debug_level = 0;
extern long nap(long);

/* ******************************************************************** */
/* Global variables                         */
/* ******************************************************************** */

static  ITEM    *item;          /* The queue item pointer   */
static  EVENT   *event;         /* The event pointer        */
static  int item_len;       /* length of incoming item  */
static  int parent_mod_id;
static  int tag = 30000;
static  struct  sockaddr_in my_client;  /* IP addr of client */
static  char cgMyIpAddr[64];
static  char cgUserName[64];
static  char cgWksName[64];
static  char cgApplName[64];
static  char cgMsgBuf[1024];
static  unsigned long       my_ip=0;    /* my IP addr  */
static  int do_unreg=FALSE;  /* Before Shutdown, send unreg msg to bchdl */
static int igStartUpMode = TRACE;
static int igRuntimeMode = TRACE;
static  char pcgCfgFile[256];
static  char pcgCedaCfg[256];
static  char pcgConfDefaultDir[] = "/ceda/conf";

/* ******************************************************************** */
/* variables and defines used for security check */
/* ******************************************************************** */
#define         ISECUR_SIZE         128
#define         NETMASK_SIZE            10
#define         iMIN_SIZE           128
#define         iMAX_SIZE           8192
#define         IMAX_COUNT          3
#define         SGET_PRIVILEGES     "GPR"
#define         SCHECK_PERIOD       "CPE"
#define         SCHECK_PASSWORD     "CPW"
#define         SVALID1             "C" 
#define         SVAILD2             "N"
#define         SSECUR_DEFAULT      SVALID1
#define         SPASS_DEFAULT       "PASS"
static  int     igLoginOK   = 0;
static  int     igCounter   = 0;
static  int     igFirst     = 1;
static  int     igTerminate = 0;
static  char    prgSecurOption[ISECUR_SIZE];
static  char    pcgNetmask[NETMASK_SIZE];
static  unsigned int     igNetmask=0xffffff00;
static int    igTcpTimeOutAlarmFlag = FALSE;

/* ******************************************************************** */
/* External functions                           */
/* ******************************************************************** */

extern  int init_que();     /* Attach to CEDA queues    */
extern  int que(int, int, int, int, int, char*);
                    /* CEDA queuing routine */
extern  void    catch_all();        /* Catches all signals      */
extern  int send_message(int,int,int,int,char*);
                    /* CEDA message handler i/f    */
extern void snap (char *buf, int len, FILE *outp);

extern pid_t      getpid(void);
extern void       time_stamp(char *);
    
/* ******************************************************************** */
/* Function prototypes                          */
/* ******************************************************************** */

static int init_netin(void);
static void  terminate(int errcode);    /* Terminate program        */
static void  handle_sig(int sig);   /* Handles signals      */
static void  handle_qerr(int);      /* Handles queuing errors   */
static int   run_server(void);
static int   server_child(int socket);
static void terminate_child (int socket,int errcode);
static void handle_queues();
static int forward_data (int sock, COMMIF *data);
static void poll_sock (int sock);
static void poll_q_and_sock (int sock);
static void bchead_ntoh  (BC_HEAD *Pbc_head);
static void bchead_hton  (BC_HEAD *Pbc_head);
static int check_qcp (int sock);
static int GetLogFileMode (int *ipModeLevel, char *pcpLine, char *pcpDefault);
static void SpecialLog(char *fmt, ...);

/* ******************************************************************** */
/*                                  */
/* The MAIN program                         */
/*                                  */
/* ******************************************************************** */

/* 20040109 JIM: 'int MAIN' kollidiert mit dem neuen DEFINE 'MAIN', daher: */
MAIN
{
    int rc=RC_SUCCESS;
  int  ilSig = 0;

    INITIALIZE;         /* General initialization   */
    STH_INIT();

    rc = init_netin();
    if (rc == RC_SUCCESS)
    {
        SetSignals (handle_sig);
        /* BST 2008.09.09 */
        errno = 0;
        ilSig = sigignore(SIGCHLD);
        dbg(TRACE,"MAIN: sigingore(SIGCHLD) returns <%d>; ERRNO=<%s>",ilSig,strerror(errno));

        /*  catch_all(handle_sig);  */  /* handles signals      */
        /*  signal(SIGCHLD,handle_sig);*/

        while (TRUE)
        {
            switch ( ctrl_sta ) 
            {
            case    HSB_ACTIVE      :
                dbg(TRACE,"Now enter ACTIVE MODE ");
                rc = run_server();
                break;
            case    HSB_STANDALONE      :
                dbg(TRACE,"Now enter STAND ALONE MODE ");
                rc = run_server();
                break;
            case    HSB_DOWN        :
                /* wait for a state change */
                sleep(30);
                terminate(1);
                break;
            case    HSB_STANDBY     :
                /* wait for a state change */
                handle_queues();
                break;
            case    HSB_ACT_TO_SBY      :
                /* wait for a HSB_ACTIVE event */
                handle_queues();
                break;
            case    HSB_COMING_UP       :
                /* wait for a HSB_ACTIVE event */
                handle_queues();
                break;
            default         :
                break;
            } /* end switch */
        } /* while */
    } /* end if rc */
    terminate (1);
} /* end of main */
        
/* ******************************************************************** */
/*                                  */
/* The SERVER program                           */
/*                                  */
/* ******************************************************************** */

static int run_server(void)
{
  int   rc = RC_SUCCESS;    /* Return code          */
  int   len;    
  int   sock;
  int   current_socket;
#ifndef _WINNT
  struct mallinfo mi;
#endif
  char buf[10];
    struct in_addr* prlInAddr = NULL;

  dbg(DEBUG,"Trying to initialize the server"); 

  sock = tcp_create_socket (SOCK_STREAM, UFIS_SERVICE_NAME);
  if (sock == -1 ) {
    dbg(TRACE,"NETIN: Error open socket");
    return RC_FAIL;
  } /* end if */

  listen (sock,MAX_NO_OF_CONNECTIONS);
  
  while (1) 
  {
    rc = check_qcp(sock);
    /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
     * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
     * tag = ((getpid()%1000)+30000);
     */
    tag = getpid()+30000;

    len = sizeof(my_client);

/*
    mi = mallinfo();
    dbg(DEBUG,"SERVER: calling accept now:arena=%d ord=%d smb=%d hbl=%d\n"
        "hbld=%d usm=%d fsm=%d uord=%d ford=%d keep=%d",
        mi.arena, mi.ordblks, mi.smblks, mi.hblks, mi.hblkhd, mi.usmblks,
        mi.fsmblks, mi.uordblks, mi.fordblks, mi.keepcost); 
 */
    errno = 0;
    alarm(TCP_TIMEOUT);
    current_socket = accept(sock,(struct sockaddr*)&my_client, &len);
    alarm(0);

    if ( current_socket < 0 ) 
    {
      if (errno == EINTR)
      { /* Timeout */
    ;  /*dbg(DEBUG,"SERVER: accept Timeout: %s:",strerror(errno));*/
      }
      else
      {
    dbg(TRACE,"SERVER: accept failed because of : %s:",strerror(errno));
      } /* fi */
      
      continue;
    } /* end if */

#ifdef ODT5
    dbg(DEBUG,"SERVER: accepted on socket %d ipaddr=%x",current_socket, ccs_ntohl(my_client.sin_addr.s_addr) );
#else
    dbg(DEBUG,"SERVER: accepted on socket %d ipaddr=%x",current_socket, ntohl(my_client.sin_addr.s_addr) );
#endif

    prlInAddr = (struct in_addr *) &my_client.sin_addr.s_addr;
    sprintf(cgMyIpAddr,"%s",inet_ntoa(*prlInAddr));

    dbg(DEBUG,"SERVER: Socket addr   <%s>",cgMyIpAddr);

    do_unreg = FALSE;

#ifdef ODT5
    if ( (ccs_ntohl(my_client.sin_addr.s_addr) & igNetmask) == (my_ip & igNetmask))
    {
      dbg(DEBUG,"SERVER: Client in same network CL:%x MY:%x", ccs_ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
    }    
    else
    {
      dbg(DEBUG, "run_server: Client NOT in same network. Sending message to bchdl\n" "\tCL:%x MY:%x", ccs_ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
      sprintf (buf, "%08x", ccs_ntohl (my_client.sin_addr.s_addr));
      do_unreg = TRUE;
      rc = tools_send_sql (mod_id+2, BC_REGISTER_CMD, "", buf, "", "");
    }    
#else
    if ( (ntohl(my_client.sin_addr.s_addr) & igNetmask) == (my_ip & igNetmask))
    {
      dbg(DEBUG,"SERVER: Client in same network CL:%x MY:%x", ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
    }    
    else
    {
      dbg(DEBUG, "run_server: Client NOT in same network. Sending message to bchdl\n" "\tCL:%x MY:%x", ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
      sprintf (buf, "%08x", ntohl (my_client.sin_addr.s_addr));
      do_unreg = TRUE;
      rc = tools_send_sql (mod_id+2, BC_REGISTER_CMD, "", buf, "", "");
    }    
#endif

    if (  fork() == 0 ) {
      close(sock);
      rc = server_child (current_socket);
      dbg(TRACE,"SERVER: This message must not appear !:");
      
      exit (-1);
    }
    close(current_socket);
  } /* end while 1 */
} /* end of run_server */


/* ******************************************************************** */
/* The check_qcp routine                        */
/* ******************************************************************** */
static int check_qcp (int sock)
{
    int rc = RC_SUCCESS;
    int     len;
    int tmpDbgLevel;
    /*rc = que(QUE_GETNW,0,mod_id,0,item_len,(char *)item);*/
    len=0;
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/*  item=NULL; */
    rc = que(QUE_GETBIGNW,0,mod_id,0,len,(char *)&item);
    if (rc == RC_SUCCESS) 
    {
        rc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if ( rc != RC_SUCCESS ) 
        {
            handle_qerr(rc);
        } /* fi */
            event=(EVENT*) item->text;
        switch (event->command) 
        {
        case    HSB_ACTIVE      :
            dbg(TRACE,"got HSB_ACTIVE returning to run_server");
            break;

        case    HSB_STANDALONE      :
            dbg(TRACE,"got HSB_STANDALONE returning to run_server");
            break;

        case    HSB_DOWN        :
            dbg(TRACE,"got HSB_DOWN calling terminate()");
            close(sock);
            terminate(RC_SHUTDOWN);
            break;

        case    HSB_STANDBY     :
            dbg(TRACE,"got HSB_STANDBY calling handle_queues()");
            close(sock);
            handle_queues();
            break;

        case    HSB_ACT_TO_SBY      :
            dbg(TRACE,"got HSB_ACT_TO_SBY calling handle_queues()");
            close(sock);
            handle_queues();
            break;

        case    HSB_COMING_UP       :
            dbg(TRACE,"got HSB_COMING_UP calling handle_queues()");
            close(sock);
            handle_queues();
            break;

        case TRACE_ON:  /* 7 */
        case TRACE_OFF: /* 8 */
            dbg_handle_debug(event->command);
      tmpDbgLevel = debug_level; /* 20050207 JIM: TRACE config reread */
            debug_level = TRACE;
            dbg (TRACE,"NETIN: checking debug config [SYSTEM]: STARTUP_MODE");
            (void) GetLogFileMode (&igStartUpMode, "STARTUP_MODE", "TRACE");
            dbg (TRACE,"NETIN: checking debug config [SYSTEM]: RUNTIME_MODE");
            (void) GetLogFileMode (&igRuntimeMode, "RUNTIME_MODE", "TRACE");
      debug_level = tmpDbgLevel; 
            break;

        case    SHUTDOWN    :
            dbg(TRACE,"NETIN: Shutdown received ");
            close(sock);
            terminate(RC_SHUTDOWN);
            break;

        case    EVENT_DATA  :
            dbg(DEBUG,"NETIN: Eventdata received ");
            break;

        default         :
            break;

        } /* end switch */
            
    } /* end if */
    else 
    {
        /* Handle queuing errors */
        if (rc != QUE_E_NOMSG)
        {
            handle_qerr(rc);
            sleep (5);
        } 
        else
        {
            rc = RC_SUCCESS;
        } /* fi */
    } /* fi */
    
    return rc;  

} /* check_qcp */

/* ******************************************************************** */
/*                                  */
/* The SERVER_CHILD program                             */
/*                                  */
/* Handles one connection                                               */
/* Should never return                          */
/* ******************************************************************** */

static int server_child (int socket)
{
  int   rc = RC_SUCCESS;    /* Return code          */
  int packet_len = NETREAD_SIZE;
  char *Pold_packet;
  COMMIF    *packet;        /* transport data */
  COMMIF    *pdata;         /* transport data */
  COMMIF    ack;
  char      *cur_adr=NULL;
  char      *cur_pdata=NULL;
  int cur_len=0;
  int p_len=0;
    int tmpDbgLevel;

  /* Close and open new outp log file */
  REINITIALIZE

  packet = (COMMIF *) calloc(NETREAD_SIZE, 1);
  pdata  = (COMMIF *) calloc(PACKET_LEN, 1);  
  if (packet == NULL || pdata == NULL)
  {
    /* Fatal error */
    dbg(TRACE,"Server_child: Error at malloc");
    terminate_child (socket, -1);
  }

  cur_adr = (char *)(packet->data) ;
  packet ->length =0;
  ack.command = htons (PACKET_DATA);
  ack.length = htonl (sizeof(COMMIF));
      
  tmpDbgLevel = debug_level; /* 20050113 JIM: TRACE error when create fails: */
  debug_level = TRACE;

  /* 20050204 JIM: use pid of child netin for QUE_CREATE of child to avoid
   *               two NETIN using the same queue
   */
  /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
   * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
   * tag = ((getpid()%1000)+30000);
   */
  tag = getpid()+30000;
  if ( que(QUE_CREATE,0,tag,0,6,"_NTIF") != RC_SUCCESS ) {
    terminate(-2);
  } /* end if */
  
  if ( que(QUE_RESP,0,tag,0,sizeof(ITEM),(char *)item) != RC_SUCCESS ) {
    terminate(-3);
  } /* end if */
  parent_mod_id = mod_id;   
  mod_id = item->originator;
  
    debug_level = igStartUpMode; /* do a "qput 1800 7" to see more dbg */

    dbg(TRACE, "NETIN: Queue created: <%d> pid: <%d> tag <%d>",mod_id,getpid(),tag);

  while (1) {
    /* 20040826 JIM: don't look on queue
     * poll_q_and_sock (socket); 
     */
    poll_sock (socket);
    cur_pdata = (char *) pdata;
    cur_len=0;
    p_len = PACKET_LEN;
    do 
    {
      if ((rc = read(socket,cur_pdata,PACKET_LEN)) <= 0 ) 
      {
        dbg(DEBUG, "NETIN: read err:sock=%d rc=%d : %s"
                  ,socket,rc,strerror(errno)); 
        terminate_child (socket, -1);
      } /* end if */
      dbg(DEBUG,"NETIN: after read rc=%d\t",rc);
      cur_pdata += rc;
      cur_len += rc;
      if (cur_len >= sizeof (COMMIF) )
      {                      /* How many bytes will come ? */
#ifdef ODT5
    p_len = (int) ccs_ntohl(pdata->length) + sizeof(COMMIF);
#else
    p_len = (int) ntohl(pdata->length) + sizeof(COMMIF);
#endif
      } /* fi */
      rc = RC_SUCCESS;
    } while (rc == RC_SUCCESS && cur_len < PACKET_LEN && cur_len < p_len);
    dbg(DEBUG,"NETIN: after packet read curlen=%d",cur_len);
      
    dbg(DEBUG,"NETIN: command <%d> length <%d>",pdata->command,pdata->length);
    dbg(DEBUG,"NETIN: command <%02x> length <%08x>",pdata->command,pdata->length);

#ifdef ODT5
    /* ntohs does not work on SCO ! vbl */
    dbg(DEBUG,"NETIN: using own routine ccs_ntohs and ccs_ntohl");
    pdata->command = ccs_ntohs(pdata->command);
    pdata->length = ccs_ntohl(pdata->length);
#else
    dbg(DEBUG,"NETIN: using UNIX's routine ntohs and ntohl");
    pdata->command = ntohs(pdata->command);
    pdata->length = ntohl(pdata->length);
#endif

    dbg(DEBUG,"NETIN: command <%d> length <%d>",pdata->command,pdata->length);
    dbg(DEBUG,"NETIN: command <%02x> length <%08x>",pdata->command,pdata->length);

    if (pdata->command != NET_SHUTDOWN)
    {
      rc = write(socket,&ack,sizeof(COMMIF));
      dbg(DEBUG,"NETIN: Wrote ack rc=%d\t",rc);
    
      if (rc == -1)
      {
    terminate_child (socket, -1);
      } /* end if */
    } /* end if */
    
    switch ( pdata->command ) 
    {
    case NET_SHUTDOWN:
      free(packet); free(pdata);
      terminate_child (socket, 0);
      break;
      
    case PACKET_DATA:
      dbg(DEBUG,"PDATA(%d)",packet->length); 
      
      packet->length += pdata->length;
      if (packet->length > packet_len)
      {
    packet_len += NETREAD_SIZE;
    Pold_packet = (char *) packet;
    packet = realloc (packet, packet_len);
    if (packet == NULL)
    {
      /* Fatal error */
      dbg(TRACE,"NETIN: Error at realloc");
      terminate_child (socket, -1);
    }
    cur_adr = (char *) packet + (cur_adr - Pold_packet); /* adjust */
      }
      memcpy (cur_adr,pdata->data,pdata->length); 
      cur_adr += pdata->length;
      break;
      
    case PACKET_END:
      dbg(DEBUG,"PDATA_END(%d)",packet->length); 

      packet->length += pdata->length;
      if (packet->length > packet_len)
    {
    packet_len += NETREAD_SIZE;
    Pold_packet = (char *) packet;
    packet = realloc (packet, packet_len);
    if (packet == NULL)
    {
      /* Fatal error */
      dbg(TRACE,"NETIN: Error at realloc");
      terminate_child (socket, -1);
    }
    cur_adr = (char *) packet + (cur_adr - Pold_packet); /* adjust */
      }

      memcpy(cur_adr,pdata->data,pdata->length); 
      packet->length += pdata->length;
      /*snap ((char *)packet,MIN(220,packet->length),outp);*/

      rc = forward_data(socket,packet); 
      packet_len = NETREAD_SIZE;
      packet = realloc (packet, packet_len);
      if (packet == NULL)
      {
    /* Fatal error */
      terminate_child (socket, -1);
      }
      cur_adr = (char *)packet->data;
      /*           memset(packet->data,0x00,8192);*/
      packet->length =0;
      break;
      
    default:
      dbg(TRACE,"Server_child: Unknown Command received: %d",
          pdata->command); 
      /* Fatal error */
      if (debug_level == DEBUG)
      {
    snap ((char *)pdata, 1024, outp);
      } /* fi */
      terminate_child (socket, -1);
      break;
    } /* end switch */
  } /* end while 1 */
  /* Never reached ... */
} /* end server_child */



/*******************************************************/
/*******************************************************/
static int GetCfg (char *pcpGroup, char *pcpLine, short spNoOfLines, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;

  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcgCfgFile, pcpGroup, pcpLine, spNoOfLines, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}                               /* end GetCfg */

/*******************************************************/
/*******************************************************/
static int GetCedaCfg (char *pcpGroup, char *pcpLine, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;

  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcgCedaCfg, pcpGroup, pcpLine, CFG_STRING, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}                               /* end GetCedaCfg */

/*******************************************************/
/*******************************************************/
static int GetLogFileMode (int *ipModeLevel, char *pcpLine, char *pcpDefault)
{
  int ilRC;
  char pclTmp[32];

  ilRC = GetCfg ("SYSTEM", pcpLine, CFG_STRING, pclTmp, pcpDefault);
  if (strcmp (pclTmp, "TRACE") == 0)
  {
    *ipModeLevel = TRACE;
  }
  else if (strcmp (pclTmp, "DEBUG") == 0)
  {
    *ipModeLevel = DEBUG;
  }                             /* end else if */
  else
  {
    *ipModeLevel = 0;
  }                             /* end else */

  ilRC = RC_SUCCESS;
  return ilRC;
}                               /* end GetLogFileMode */

/* ******************************************************************** */
/* The initialization routine                       */
/* Allocates the evenb buffer and inits the que                         */
/* Return: RC_SUCCESS, RC_FAIL                                          */
/* ******************************************************************** */

static int init_netin(void)
{
  int   rc = RC_SUCCESS;    /* Return code */
  int   ilRC;
  char my_name[64];
  struct hostent *hp;
  unsigned int ilTmp=0;
  char *pclConfDir;
  
  if ((pclConfDir= getenv ("CFG_PATH")) == NULL)
  {
    pclConfDir= pcgConfDefaultDir;
  }
  sprintf (pcgCfgFile, "%s/%s.cfg", pclConfDir, mod_name);
  
  debug_level = TRACE;
  ilRC = GetLogFileMode (&igStartUpMode, "STARTUP_MODE", "TRACE");
  ilRC = GetLogFileMode (&igRuntimeMode, "RUNTIME_MODE", "TRACE");
  dbg(TRACE,"INIT NETIN:\n%s",mks_version);
  dbg (TRACE,"NETIN: checked debug config [SYSTEM]: STARTUP_MODE = %s ",
       (igStartUpMode == TRACE ? "TRACE" : 
       (igStartUpMode == DEBUG ? "DEBUG" :
       (igStartUpMode == 0 ? "OFF" :
       "UNKNOWN" ))) 
      );
  dbg (TRACE,"NETIN: checked debug config [SYSTEM]: RUNTIME_MODE = %s ",
       (igRuntimeMode == TRACE ? "TRACE" : 
       (igRuntimeMode == DEBUG ? "DEBUG" :
       (igRuntimeMode == 0 ? "OFF" : 
       "UNKNOWN" ))) 
      );
  debug_level = igStartUpMode;

  /* clear buffer */
  memset((void*)prgSecurOption, 0x00, ISECUR_SIZE);
  memset((void*)pcgNetmask, 0x00, NETMASK_SIZE);

  /* get entry from sgs.tab */
  if ((ilRC = tool_search_exco_data("NET","SECUR",
                prgSecurOption)) != RC_SUCCESS)
  {
        /* use default entry */
        strcpy(prgSecurOption, SSECUR_DEFAULT);
  }
  else
  {
        /* in this case we found an entry in sgs.tab */
        /* check for valid data */
        if (strcmp(prgSecurOption, SVALID1) &&
            strcmp(prgSecurOption, SVAILD2))
        {
            /* not a valid data */
            strcpy(prgSecurOption, SSECUR_DEFAULT);
        }
  }
  dbg(DEBUG,"SecurOption: %s", prgSecurOption);

  if ((ilRC = tool_search_exco_data("NET","MASK",
                pcgNetmask)) == RC_SUCCESS)
  {
    sscanf (pcgNetmask, "%x",&ilTmp);
    /* Simple plausi */
    if (ilTmp > 0xff000000)
    {
      igNetmask = ilTmp;
    } /* fi */
  } /* fi */
  dbg(DEBUG,"Netmask: %x", igNetmask);

  /* Allocate a dynamic buffer for max event size */
  item_len = MAX_EVENT_SIZE;
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/*  item = (ITEM *) malloc(MAX_EVENT_SIZE); */
  
/*    if (item == (ITEM *) NULL) {      */
    /* Handle malloc error */
/*      rc = RC_FAIL;
/*    } /end if */
  
  if (rc == RC_SUCCESS)
  {
/*      event = (EVENT *) item->text; * set event pointer            */
  
    /* Attach to the CEDA queues */
  
    while (rc = init_que()) {
      dbg (TRACE,"NETIN:Waiting for que creation %d", rc);
      sleep(5);        /* Wait for QCP to create queues */
    } /* end while */
    
    rc = RC_SUCCESS;
  }

#ifdef USE_EXTAB_HOSTNAME
  rc = tool_search_exco_data ("NET","HOST",my_name);
  /* Fallback */
  if (rc != RC_SUCCESS)
  {
    dbg(TRACE,"No HOST entry in sgs.tab: EXTAB! Please add");
    rc = gethostname(my_name, 64);
  } /* fi */

#else
  if (rc == RC_SUCCESS)
  {
    rc = gethostname(my_name, 64);
  } /* fi */
#endif
  
  if (rc == RC_SUCCESS)
  {
    hp = gethostbyname( my_name);
    if (hp == NULL)
    {
      dbg(TRACE,"Init: Gethostbyname(%s): %s",  my_name,strerror(errno));
      rc = RC_FAIL;
    }
    else
    {
#ifdef ODT5
      dbg (DEBUG,"Handle_tcp_req: Got host info: name=%s len=%d adr1=%x" ,hp->h_name, hp->h_length, ccs_ntohl (*((int *) (hp->h_addr))) );
      my_ip = ccs_ntohl (*((int *) (hp->h_addr)));
#else
      dbg (DEBUG,"Handle_tcp_req: Got host info: name=%s len=%d adr1=%x" ,hp->h_name, hp->h_length, ntohl (*((int *) (hp->h_addr))) );
      my_ip = ntohl (*((int *) (hp->h_addr)));
#endif
    } /* end if */
  } /* end if */

  dbg(DEBUG,"NETIN: returning %d from init ", rc);    
  debug_level = igRuntimeMode;
  
  return rc;
} /* end of initialize */


/* ******************************************************************** */
/* The termination routine                      */
/* ******************************************************************** */

static void terminate(int errcode)
{
  free(item);

  dbg(TRACE,"NETIN: queue <%d>: Now terminating %d  ", mod_id,errcode);       
  if (ftell(outp)<2)
  {
    fclose(outp);
    remove(__CedaLogFile);
  }
    
  exit(errcode);
} /* end of terminate */


/* ******************************************************************** */
/* The terminate_child routine                      */
/* ******************************************************************** */

static void terminate_child(int socket,int errcode)
{
    char buf[10];
    ITEM        *prlItem=NULL;
    EVENT   *prlEvent=NULL;
    int rc=RC_SUCCESS;

    if(get_system_state() != HSB_DOWN)
    {
        if (do_unreg)
        {
#ifdef ODT5
            dbg(DEBUG, "Terminate: Sending UNREG message to bchdl CL:%x MY:%x", ccs_ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
            sprintf (buf, "%08x", ccs_ntohl (my_client.sin_addr.s_addr));
#else
            dbg(DEBUG, "Terminate: Sending UNREG message to bchdl CL:%x MY:%x", ntohl (my_client.sin_addr.s_addr) & igNetmask, my_ip & igNetmask);
            sprintf (buf, "%08x", ntohl (my_client.sin_addr.s_addr));
#endif
            rc = tools_send_sql (parent_mod_id+2, BC_UNREGISTER_CMD, "", buf, "", "");
        } /* fi */
  
        dbg(DEBUG,"Terminate_child: shutdown and close");
        if (socket > 0)
        {
            /* BST 20080910 */
            /* shutdown (socket,2); */
            close(socket);
        }

        sleep(3); /*** wait for possible hsb_message ***/

        dbg(DEBUG,"Terminate_child: try to get a last message");

        /*rc = que(QUE_GETNW,0,mod_id,PRIORITY_3,item_len,(char *)item);*/
    rc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,0,(char *) &prlItem);
        if (rc == RC_SUCCESS)
        {
            rc = que(QUE_ACK,0,mod_id,0,0,NULL);

            dbg(TRACE,"Terminate_child: got a last message");

      prlEvent = (EVENT *) prlItem->text;
            DebugPrintItem(TRACE,prlItem);
            DebugPrintEvent(TRACE,prlEvent);
            free ((char *)prlItem);
        } /* end switch */

    /* do a "qput 1800 7" to set following childs into DBG mode */
    debug_level = igStartUpMode;
        dbg(TRACE,"Terminate_child: delete queue <%d> pid: <%d>",mod_id,getpid());

        que(QUE_DELETE,mod_id,mod_id,0,0,0);  
    }

  /* do a "qput 1800 7" to set following childs into DBG mode */
  debug_level = igStartUpMode;
    sprintf(cgMsgBuf,"%s;%s;%s",cgUserName,cgMyIpAddr,cgApplName);
    dbg(TRACE,"LOGOUT: user <%s> wks <%s> ip <%s> appl <%s> queue <%d> pid: <%d>",
                           cgUserName,cgWksName,cgMyIpAddr,cgApplName,mod_id,getpid());
    send_message(IPRIO_ASCII,NETIN_USER_LOGOUT,0,0,cgMsgBuf);

    dbg(TRACE,"Terminate_child: queue <%d>: Exiting with %d pid: <%d>",mod_id, errcode,getpid());
  if (ftell(outp)<2)
  {
    fclose(outp);
    remove(__CedaLogFile);
  }

    exit(errcode);
} /* end of terminate */


/* ******************************************************************** */
/* The handle signals routine                       */
/* ******************************************************************** */

static void handle_sig(int sig)
{
    int ilOldDebugLevel;
    int ilWaitTries;  /* Mei */
    int ilWaitPid;

    if (sig == SIGCHLD) {
        /***
      FILE *fpTest;

        fpTest = fopen("/ceda/debug/tst","r");
        if (fpTest != NULL)
        {
            dbg(TRACE,"SIGCHLD received, but don't do a waitpid for test");
            return;
        }
        ************/
        ilOldDebugLevel = debug_level;
        /* debug_level = TRACE; 
       * dbg(TRACE,"%05d:NETIN: received signal %d. Call WAITPID.",__LINE__,sig);
     */
      ilWaitTries = 0;
      while((ilWaitPid = waitpid(-1,(int *)NULL,WNOHANG)) > 0)
        {
          ilWaitTries++;
          if( ilWaitTries >= 100 )
            {
              SpecialLog( "<handle_sig> Too long a wait, return" );
              return;
            }
            SpecialLog( "<handle_sig> Child dead. pid <%d>", ilWaitPid );
        };
    /*
       * dbg(TRACE,"%05d:NETIN: received signal %d. After WAITPID.",__LINE__,sig);
     */
        debug_level = ilOldDebugLevel;
    } /* end if */

    if (sig == SIGTERM) {
      dbg(TRACE,"NETIN: received signal %d. Call TERMINATE_CHILD.",
          sig);
      terminate_child (-1, 0);
    } /* end if */

/* NEW */
    /*SetSignals (handle_sig);*/

#ifdef OLD
    if ( sig != SIGSEGV ) {
        signal(sig,handle_sig);
    } /* end if */
#endif


    if (sig != SIGALRM)
    {
      dbg(DEBUG,"NETIN: received signal %d. Reset signal. ",sig);
      
  }
  else
  {
     dbg(DEBUG,"SIGALARM");
     igTcpTimeOutAlarmFlag=TRUE;
    } /* fi */

        
    return ;
} /* end of handle_sig */

/* ******************************************************************** */
/* The handle queuing error routine                 */
/* ******************************************************************** */

static void handle_qerr(err)
int err;                /* Error code */
{
    
    switch(err)
    {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",err);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",err);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",err);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",err);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",err);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",err);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",err);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",err);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",err);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",err);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",err);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",err);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",err);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",err);*/
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",err);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",err);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",err);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",err);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",err);
        break;
    }
    return;
} /* end of handle_qerr */

/* ******************************************************************** */
/* Following the forward data function                  */
/* ******************************************************************** */
static  int forward_data (int sock, COMMIF *data)
{
    int         len;
    int         rc;
    int         t = 0;
    int         do_ack=TRUE;        /* Ack-Mode */
    char        *offs;
    ITEM        *prlItem=NULL;
    EVENT       *out_event;
    EVENT       *prlEvent=NULL;
    BC_HEAD     *bchd;                  /* Broadcast header             */
    BC_HEAD     *bchd_out;
    CMDBLK      *cmdblk;
    COMMIF      *packet;
    time_t      reqtim=0;

    /* *********************************************************** */
    /* variables used for performance and timeout check */
    /* *********************************************************** */
    int     ilTist = 0;
    int     ilNapOut = QUE_TIMEOUT;
    int     ilTimOut = QUE_TIMEOUT / 4;

    /* *********************************************************** */
    /* variables and defines used for security check */
    /* *********************************************************** */
    int         ilRC;
    int         ilPassNotInList;
    char            *pclSelection = NULL;
    char            *pclFields    = NULL;
    char            pclUPRFields[iMAX_SIZE]; /* this is enough */
    char            pclUPRCommand[iMIN_SIZE];
    BC_HEAD     *prlBCHead    = NULL;
    CMDBLK      *prlCmdblk    = NULL;
  char      *pclSelKey    = NULL;
  char      *pclFldPtr    = NULL;
  char      *pclDatPtr    = NULL;
  int tmpDbgLevel;
  char pclTmpIpAdr[32];
  char pclTmpTWS[64];


    /* Convert from network to host order */
    bchead_ntoh ((BC_HEAD*)data);

    prlBCHead    = (BC_HEAD*)((char*)data->data);
    prlCmdblk    = (CMDBLK*)((char*)prlBCHead->data);
    strcpy(pclUPRCommand, prlCmdblk->command);

    if (igFirst && !strcmp(prgSecurOption, SSECUR_DEFAULT))
    {
        /* the first time */    
        /* we have to check the commands */

        /* first set local pointers */
        pclSelection = (char*)prlCmdblk->data;
        pclFields    = (char*)pclSelection + strlen(pclSelection) + 1;

        /* is PASS in list? */
        /* first clear memory */
        memset((void*)pclUPRCommand, 0x00, iMIN_SIZE);
        memset((void*)pclUPRFields, 0x00, iMAX_SIZE);

        /* copy to temporary buffer */
        strcpy(pclUPRFields, pclFields);

        /* convert to uppercase */
        dbg(DEBUG,"Found Fields: %s", pclFields);
        StringUPR((UCHAR*)pclUPRFields);
        StringUPR((UCHAR*)pclUPRCommand);
        dbg(DEBUG,"Found toupper(Fields): %s", pclFields);

        if (strstr(pclUPRFields, SPASS_DEFAULT) != NULL)
        {
            ilPassNotInList = 0;
        }else{
            ilPassNotInList = 1;
        }

        /* dbg(DEBUG,"Found pass not in list: %s", ilPassNotInList ? "pass is net drinn" : "pass is drinn"); */

        /* compare command */
        if (
            (strcmp(pclUPRCommand, SGET_PRIVILEGES)      &&
             strcmp(pclUPRCommand, SCHECK_PERIOD)        &&
             strcmp(pclUPRCommand, SCHECK_PASSWORD))     ||
            (!strcmp(pclUPRCommand, SGET_PRIVILEGES)     &&
             ilPassNotInList)
           )
        {
            /* not a valid command */
            igCounter++;

            /* how may tries? */
            if (igCounter >= IMAX_COUNT)
            {
                /* to many trys */
                /* shutdown connection */
                igFirst   = 1;
                igCounter = 0;

                packet = (COMMIF*)calloc(PACKET_LEN,1);
                packet->command = NET_SHUTDOWN;
                packet->length  = 200;
                bchd_out        = (BC_HEAD*)packet->data;
                bchd_out->rc    = RC_FAIL;
                sprintf(bchd_out->data, "Authority error - too many tries");

                dbg(DEBUG,">>>>>>>>>  Start of Secur-Transmition <<<<<<<<");
                ilRC = tcp_send_data(packet->data, packet->length, sock);
                dbg(DEBUG,">>>>>>>>>  End of Secur-Transmition   <<<<<<<<");
                free((void*)packet);

                terminate_child (sock, -1);
            }
            else
            {
                /* only a message */
                packet = (COMMIF*)calloc(PACKET_LEN,1);
                packet->command = NET_DATA;
                packet->length  = 200;
                bchd_out        = (BC_HEAD*)packet->data;
                bchd_out->rc    = RC_FAIL;
                sprintf(bchd_out->data, "Authority error - not a valid command (%s)", prlCmdblk->command);  

                dbg(DEBUG,">>>>>>>>>  Start of Secur-Transmition <<<<<<<<");
                ilRC = tcp_send_data(packet->data, packet->length, sock);
                dbg(DEBUG,">>>>>>>>>  End of Secur-Transmition   <<<<<<<<");
                free((void*)packet);
            }
            /* return */
            return RC_FAIL;
        }
    }


    len = data->length+sizeof(EVENT);

    if ((out_event = (EVENT*)calloc(len,1)) == NULL)
    {
        dbg(TRACE,"forward_data: Malloc Error"); 
        return RC_FAIL;
    } 

    dbg(DEBUG,"Sending now from mod_id= %d",mod_id);

    out_event->command      = EVENT_DATA;
    out_event->originator   = (short) mod_id;
    out_event->data_length  = data->length;
    out_event->data_offset  = sizeof(EVENT);
    offs = (char *)out_event + sizeof(EVENT);
    memcpy (offs,data->data,data->length);

        /* dbg(TRACE,"OUT EVENT DATA_LENGTH=%d", out_event->data_length); */

    bchd  = (BC_HEAD *) ((char *)out_event + out_event->data_offset);

    if ((bchd->orig_name[0] >= '0') && (bchd->orig_name[0] <= '9'))
    {
        ilTimOut = atoi(bchd->orig_name);
        if ((ilTimOut < 0) || (ilTimOut > 3600))
        {
            ilTimOut = QUE_TIMEOUT / 4;
            } /* end if */
        ilNapOut = ilTimOut * 4;
        } /* end if */
    ilTist = time(0L) % 10000;
    sprintf(bchd->orig_name,"%d,%d",ilTist,ilTimOut);

/* 20050429 JIM: this line is moved up only :*/
  cmdblk = (CMDBLK *) ((char *)bchd->data);
  dbg(TRACE,"command <%s>",cmdblk->command);

  pclSelKey = (char *) cmdblk->data;

  if (strcmp(prlCmdblk->command, "CCO") == 0)
  {
      /* Check Connection Heart Beat */
      /* return only a message */
      pclDatPtr = pclSelKey;
      strcpy(pclDatPtr,"");
      pclDatPtr++;
      strcpy(pclDatPtr,"");
      pclDatPtr++;
      strcpy(pclDatPtr,"CCA");
      len =sizeof(COMMIF)+ out_event->data_length;
      packet = (COMMIF *) calloc(len,1);
      packet->command = NET_DATA;
      packet->length = out_event->data_length;
      /* Convert from host to network byte order */
      bchead_hton (bchd);
      memcpy (packet->data,bchd,out_event->data_length);
      dbg(TRACE,">>>>>>>>> RCVD HEART BEAT CCO  <<<<<<<<");
      rc = tcp_send_data(packet->data, packet->length, sock);
      dbg(TRACE,">>>>>>>>> SENT HEART BEAT CCA  <<<<<<<<");
      free((void*)packet);
      free(out_event);
      /* return */
      return RC_SUCCESS;
    }



/* 20050429 JIM: inserted ths to patch network adress 00000000: */
#ifdef ODT5
  sprintf(pclTmpIpAdr,"%08x",ccs_ntohl(my_client.sin_addr.s_addr));
#else
  sprintf(pclTmpIpAdr,"%08x",ntohl(my_client.sin_addr.s_addr));
#endif

  if (strcmp(cmdblk->command,"RBS")==0)
  {
     dbg(TRACE,"RBS detected address: <%s>",pclSelKey);
     dbg(TRACE,"addresss from struct: <%s>",pclTmpIpAdr);
     if (strlen(pclSelKey)==8) 
     {
        if (strcasecmp(pclSelKey,pclTmpIpAdr)!=0)
        {
          debug_level = igStartUpMode;
          dbg(TRACE,"RBS patching address from <%s> to <%s>",pclSelKey,pclTmpIpAdr);
          strcpy(pclSelKey,pclTmpIpAdr);
          debug_level = tmpDbgLevel;
        }
     }
  }
  
  strncpy(pclTmpTWS,cmdblk->tw_start,sizeof(cmdblk->tw_start));
  pclTmpTWS[sizeof(cmdblk->tw_start)] = 0x00;
  dbg(TRACE,"TWS <%s> STAMP", pclTmpTWS);
  if (strstr(pclTmpTWS,".NW.") != NULL)
  {
    dbg(TRACE,"SETTING NETOUT_NO_ACK");
    bchd->rc = NETOUT_NO_ACK;
    rc = que(QUE_PUT,parent_mod_id+1,mod_id,1,len,(char *)out_event);

    dbg(TRACE,"NOT WAITING FOR ANSWER");
    /* return only a message */
    bchd->rc = RC_SUCCESS;
    pclDatPtr = pclSelKey;
    strcpy(pclDatPtr,"");
    pclDatPtr += strlen(pclDatPtr) + 1;
    strcpy(pclDatPtr,"");
    pclDatPtr += strlen(pclDatPtr) + 1;
    strcpy(pclDatPtr,".NW.");
    len =sizeof(COMMIF)+ out_event->data_length;
    packet = (COMMIF *) calloc(len,1);
    packet->command = NET_DATA;
    packet->length = out_event->data_length;
    /* Convert from host to network byte order */
    bchead_hton (bchd);
    memcpy (packet->data,bchd,out_event->data_length);
    rc = tcp_send_data(packet->data, packet->length, sock);
    free((void*)packet);
    free(out_event);
    /* return */
    return RC_SUCCESS;
  }

    rc = que(QUE_PUT,parent_mod_id+1,mod_id,1,len,(char *)out_event);

    if (bchd->rc == NETOUT_NO_ACK)
    {
        do_ack = FALSE;
    }

    DebugPrintBchead(DEBUG,bchd);
    DebugPrintCmdblk(DEBUG,cmdblk);
  if (debug_level == DEBUG)
  {
        snap ((char *)bchd->data,512,outp);
    }
    strcpy(cgUserName,bchd->dest_name);
    cgUserName[sizeof(bchd->dest_name)] = 0x00;
    strcpy(cgWksName,bchd->recv_name);
    cgWksName[sizeof(bchd->recv_name)] = 0x00;
    get_real_item(cgApplName,cmdblk->tw_end,3);

    /* terminate flag */
    igTerminate = 0;

    /* Shudd there B an ACK ?? ********************************************/
    if (do_ack == TRUE)
        {
            int ilCount = 0;

        dbg(DEBUG,"Waiting for answer");
        reqtim = time(NULL);

        /* t = QUE_TIMEOUT+1; */
/* 20040826 JIM: instead of loop:
*      t = ilNapOut+15;
*      do 
*      {
*        if (ilCount++ < 15)
*        {
*          nap (20);
*        }
*        else
*        {
*dbg(DEBUG,"waiting,  %d tries remaining from max %d",t,ilNapOut+15);
*          nap (250);
*        }
*           rc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,0,(char *) &prlItem);
*           t--;
*       } while (t > 0 && rc == QUE_E_NOMSG);
*
* 20040826 JIM: use alarm() with macro ALARM:
*               using the alarm will result in lower load, because netin is
*               stopped in que().
*               The macro is requested by MCU for WMQ compatibility
*/
      igTcpTimeOutAlarmFlag = FALSE;
      ALARM(ilTimOut);
        rc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char *) &prlItem);
      ALARM(0);
      if(igTcpTimeOutAlarmFlag==TRUE)
      {
         rc = RC_FAIL;
      }
      
        if (rc == RC_SUCCESS) 
        {
#ifdef SUPPORT_CEDAMO
            /* *************** CEDAMON lines **********************/
            reqtim = time(NULL) - reqtim;
            dbg(DEBUG,"Received answer");

            if (reqtim > 1)
            {
                (void) MonutlSendEvent(5800, "PER", "NRT", reqtim, 0, NULL);
            }
            /* *************** CEDAMON lines end ******************/
#endif

            /* Acknowledge the item */
            rc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if ( rc != RC_SUCCESS ) 
            {
                handle_qerr(rc);
            }
  
            prlEvent = (EVENT *) prlItem->text;

            if ( prlEvent->command != EVENT_DATA ) 
            {
                if (debug_level == DEBUG)
                {
                snap((char *)prlItem,128,outp); fflush(outp);
            }
                dbg(TRACE,"WRONG EVENT COMMAND !!!!(%d)",prlEvent->command);
                free ((char *)prlItem);
                free(out_event);
                return -1;
            }

            /* set BCHead-Pointer */
            bchd  = (BC_HEAD *) ((char *)prlEvent + prlEvent->data_offset);

            /* returncode = RC_SUCCESS? */
            dbg(DEBUG,"Status first: %d, Counter: %d", igFirst, igCounter);
            dbg(DEBUG,"BCHead-returncode: %d", bchd->rc);

            cmdblk = (CMDBLK *) ((char *)bchd->data);

            if(igLoginOK == 0)
            {
                int tmpDbgLevel;

                tmpDbgLevel = debug_level;
        /*
         * do a "qput 1800 7" to set following childs into DBG mode 
         * netin will reread "STARTUP_MODE"
         */
        debug_level = igStartUpMode;
                if((bchd->rc != RC_SUCCESS) && strcmp(pclUPRCommand, SGET_PRIVILEGES))
                {
                    sprintf(cgMsgBuf,"%s;%s;%s",cgUserName,cgMyIpAddr,cgApplName);
                    dbg(TRACE,"LOGIN: ERR user <%s> wks <%s> ip <%s> appl <%s> queue <%d> pid: <%d>",cgUserName,cgWksName,cgMyIpAddr,cgApplName,mod_id,getpid());
                    send_message(IPRIO_ASCII,NETIN_USER_LOGIN_ERR,0,0,cgMsgBuf);
                } else {
                    sprintf(cgMsgBuf,"%s;%s;%s",cgUserName,cgMyIpAddr,cgApplName);
                    dbg(TRACE,"LOGIN: OK  user <%s> wks <%s> ip <%s> appl <%s> queue <%d> pid: <%d>",cgUserName,cgWksName,cgMyIpAddr,cgApplName,mod_id,getpid());
                    send_message(IPRIO_ASCII,NETIN_USER_LOGIN_OK,0,0,cgMsgBuf);
                    igLoginOK = 1;
                }
                debug_level = tmpDbgLevel;
            }

            /* test returncode */
            if ((igFirst == 1) && (bchd->rc != RC_SUCCESS) && (!strcmp(prgSecurOption, SSECUR_DEFAULT)))
            {
                igCounter++;
                if (igCounter >= IMAX_COUNT)
                {
                    /* to many trys */
                    /* shutdown connection */
                    igFirst     = 1;
                    igCounter   = 0;
                    igTerminate = 1;

                    packet = (COMMIF*)calloc(PACKET_LEN,1);
                    packet->command = NET_SHUTDOWN;
                    packet->length  = 200;
                    bchd_out        = (BC_HEAD*)packet->data;
                    bchd_out->rc    = RC_FAIL;
                    sprintf(bchd_out->data, "Authority error - too many tries (RC)");
                } else {
                    /* only a message */
                    packet = (COMMIF*)calloc(PACKET_LEN,1);
                    packet->command = NET_DATA;
                    packet->length  = 200;
                    bchd_out        = (BC_HEAD*)packet->data;
                    bchd_out->rc    = RC_FAIL;
                    strcpy(bchd_out->data, bchd->data);
                    /*********
                    sprintf(bchd_out->data, "Authority error - returncode(%d)", bchd->rc);  
                    *********/
                }
            } else {
                igFirst   = 0;
                igCounter = 0;

                len =sizeof(COMMIF)+ prlEvent->data_length;

                dbg(DEBUG,"Now malloc %d bytes for transfer", prlEvent->data_length);
        
                packet = (COMMIF *) calloc(len,1);
                packet->command = NET_DATA;
                packet->length = prlEvent->data_length;

                /* Convert from host to network byte order */
                bchead_hton (bchd);
                memcpy (packet->data,bchd,prlEvent->data_length);
                dbg(DEBUG,"sending %d bytes",prlEvent->data_length);
            }
        } else {

#ifdef SUPPORT_CEDAMO
            /* *************** CEDAMON line **********************/
            (void) MonutlSendEvent(5800, "PER", "OUT", 0, 0, NULL);
            /* *************** CEDAMON line end ******************/
#endif

            dbg(TRACE,"Error or Timeout ! Now malloc %d bytes for error msg", PACKET_LEN); 

            packet = (COMMIF *) calloc(PACKET_LEN,1);
            packet->command = NET_DATA;
            packet->length = 200;
            bchd_out  = (BC_HEAD *) packet->data;
            bchd_out->rc = RC_FAIL;
            strcpy (bchd_out->data, "QCP Error or Timeout");
        } /* end else */

        dbg(DEBUG,">>>>>>>>>>>>>>  Start of Transmition <<<<<<<<<<");
/*snap ((char *)packet->data,packet->length,outp);*/
        rc = tcp_send_data(packet->data, packet->length, sock);
        dbg(DEBUG,">>>>>>>>>>>>>>  End of Transmition   <<<<<<<<<<");

        free ((char *)prlItem);
        prlItem= NULL;
        free(packet);

        if (igTerminate)
        {
            dbg(DEBUG,"now terminating child");
            terminate_child (sock, -1);
        } else {
            dbg(DEBUG,"child is going on");
        }
    } 

    /* und tschuess */
    free(out_event);
    return rc == RC_SUCCESS ? RC_SUCCESS : RC_FAIL;
} 


/* ******************************************************************** */
/* Following the poll_sock function             */
/* Waits for input on the socket          */
/* The reason of the loop with TCP_TIMEOUT is, that a disconnect of the */
/* client application may be detected without loop and timeout after 30 */
/* minutes, in the loop it will be detected at the next call of         */
/* tcp_wait_timeout */
/* ******************************************************************** */
static void poll_sock (int sock)
{
  int rc = RC_SUCCESS;
    int len;
  do {
    rc = tcp_wait_timeout (sock, TCP_TIMEOUT);
  } while (rc == RC_FAIL);
} /* poll_sock */


/* ******************************************************************** */
/* Following the poll_q_and_sock function               */
/* Waits for input on the socket and polls the QCP for shutdown messages*/
/* Other QCP msg are discarded                      */
/* ******************************************************************** */
static void poll_q_and_sock (int sock)
{
  int rc = RC_SUCCESS;
    int len;
    int tmpDbgLevel;

  do {
    rc = tcp_wait_timeout (sock, TCP_TIMEOUT);
    if (rc == RC_FAIL || rc == RC_NOT_FOUND)
    {
    len=0;
/* 20020628 JIM: test for memory leak: allocation should be done in que */
/*  item=NULL; */
      /*rc = que(QUE_GETNW,0,mod_id,PRIORITY_3,item_len,(char *)item);*/
      rc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,len,(char *)&item);
      if (rc == RC_SUCCESS)
      {
    event=(EVENT*) item->text;
    rc = que(QUE_ACK,0,mod_id,0,0,NULL);
    switch (event->command) 
    {
      case TRACE_ON:  /* 7 */
      case TRACE_OFF: /* 8 */
        dbg_handle_debug(event->command);
      tmpDbgLevel = debug_level; /* 20050207 JIM: TRACE config reread */
            debug_level = TRACE;
            (void) GetLogFileMode (&igStartUpMode, "STARTUP_MODE", "TRACE");
            (void) GetLogFileMode (&igRuntimeMode, "RUNTIME_MODE", "TRACE");
      dbg (TRACE,"NETIN: checked debug config [SYSTEM]: STARTUP_MODE = %s ",
           (igStartUpMode == TRACE ? "TRACE" : 
           (igStartUpMode == DEBUG ? "DEBUG" :
           (igStartUpMode == 0 ? "OFF" :
           "UNKNOWN" ))) 
          );
      dbg (TRACE,"NETIN: checked debug config [SYSTEM]: RUNTIME_MODE = %s ",
           (igRuntimeMode == TRACE ? "TRACE" : 
           (igRuntimeMode == DEBUG ? "DEBUG" :
           (igRuntimeMode == 0 ? "OFF" : 
           "UNKNOWN" ))) 
          );
      debug_level = tmpDbgLevel; 
        break;
      case  HSB_ACT_TO_SBY  :
        dbg(TRACE,"poll_q_and_sock: ACT_TO_SBY received");
        
        terminate_child (sock, -1);
        break;
        
          case  SHUTDOWN    :
        dbg(TRACE,"poll_q_and_sock: Shutdown received(2) ");
        
        terminate_child (sock, -1);
        break;
    
      default           :
        break;
    } /* end switch */
      } /* fi RC_SUCCESS*/
      rc = RC_FAIL; /* dont return */
    } /* fi */
  } while (rc == RC_FAIL);
} /* poll_q_and_sock */


/* ******************************************************************** */
/* Following the bchead_ntoh function                   */
/* Converts from network to host order                  */
/* ******************************************************************** */
static void bchead_ntoh  (BC_HEAD *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

#ifdef ODT5
  Pbc_head->bc_num = ccs_ntohs (Pbc_head->bc_num);
  Pbc_head->tot_buf = ccs_ntohs (Pbc_head->tot_buf);
  Pbc_head->act_buf = ccs_ntohs (Pbc_head->act_buf);
  Pbc_head->rc = ccs_ntohs (Pbc_head->rc);
  Pbc_head->tot_size = ccs_ntohs (Pbc_head->tot_size);
  Pbc_head->cmd_size = ccs_ntohs (Pbc_head->cmd_size);
  Pbc_head->data_size = ccs_ntohs (Pbc_head->data_size);
#else
  Pbc_head->bc_num = ntohs (Pbc_head->bc_num);
  Pbc_head->tot_buf = ntohs (Pbc_head->tot_buf);
  Pbc_head->act_buf = ntohs (Pbc_head->act_buf);
  Pbc_head->rc = ntohs (Pbc_head->rc);
  Pbc_head->tot_size = ntohs (Pbc_head->tot_size);
  Pbc_head->cmd_size = ntohs (Pbc_head->cmd_size);
  Pbc_head->data_size = ntohs (Pbc_head->data_size);
#endif

#endif
}  /* bchead_ntohh */


/* ******************************************************************** */
/* Following the bchead_hton function                   */
/* Converts from network to host order                  */
/* ******************************************************************** */
static void bchead_hton  (BC_HEAD *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

  Pbc_head->bc_num = htons (Pbc_head->bc_num);
  Pbc_head->tot_buf = htons (Pbc_head->tot_buf);
  Pbc_head->act_buf = htons (Pbc_head->act_buf);
  Pbc_head->rc = htons (Pbc_head->rc);
  Pbc_head->tot_size = htons (Pbc_head->tot_size);
  Pbc_head->cmd_size = htons (Pbc_head->cmd_size);
  Pbc_head->data_size = htons (Pbc_head->data_size);

#endif
}  /* bchead_hton */

/* ******************************************************************** */
/* Following the handle queues function. This function waits for an     */
/* ctrl_sta status change.                      */
/* ******************************************************************** */

static void handle_queues()
{
    int rc  = RC_SUCCESS;

    for(;;)
    {
        dbg(TRACE,"handle_queues: QUE_GET");

        rc = que(QUE_GET,0,mod_id,PRIORITY_3,item_len,(char *)item);
        if(rc == RC_SUCCESS)
        {
            rc = que(QUE_ACK,0,mod_id,0,0,NULL);
        } /* end if */

        switch( event->command )
        {
        case    HSB_ACT_TO_SBY  :
            dbg(TRACE,"got HSB_ACT_TO_SBY");
            ctrl_sta = event->command;
            break;

        case    HSB_STANDBY :
            dbg(TRACE,"got HSB_STANDBY");
            ctrl_sta = event->command;
            break;

        case    HSB_COMING_UP   :
            dbg(TRACE,"got HSB_COMING_UP");
            ctrl_sta = event->command;
            break;

        case    HSB_ACTIVE  :
            dbg(TRACE,"got HSB_ACTIVE");
            ctrl_sta = event->command;
            terminate(-9);
            break;

        case    HSB_STANDALONE  :
            dbg(TRACE,"got HSB_STANDALONE");
            ctrl_sta = event->command;
            terminate(-9);
            break;

        case    HSB_DOWN    :
            dbg(TRACE,"got HSB_DOWN");
            ctrl_sta = event->command;
            terminate(-9);
            break;

        case    SHUTDOWN    :
            dbg(TRACE,"got SHUTDOWN");
            terminate(RC_SHUTDOWN);
            break;

        default         :
            dbg(TRACE,"handle_queues: UNKNOWN CMD");
            break;

        } /* end switch */

    } /* end for */

} /* end of handle_queues */

void SpecialLog(char *fmt, ...)
{
    struct tm rlTm;
    time_t tlNow;
    FILE *logptr;
    va_list args;
    char *s; 
    char pclError[1000];
    char pclFname[1024];
    static int ilDay = -1;

    tlNow = time(0);
    localtime_r( &tlNow, &rlTm );

    sprintf( pclFname, "%s/%s_%d.speclog", getenv("DBG_PATH"), mod_name, rlTm.tm_wday );
    if( ilDay != rlTm.tm_wday )
        logptr = fopen( pclFname, "w" );
    else
        logptr = fopen( pclFname, "a" );
    ilDay = rlTm.tm_wday;
    if( logptr == NULL )
        return;    
    
    va_start(args, fmt);
    (void) sprintf(pclError, "%s %2.2d-%2.2d-%4.4d %2.2d:%2.2d:%2.2d ",mod_name, 
                                 rlTm.tm_mday, rlTm.tm_mon+1, rlTm.tm_year+1900,
                                 rlTm.tm_hour, rlTm.tm_min, rlTm.tm_sec);
    s = pclError + strlen(pclError);
    strcat (s, fmt);
    strcat (s, "\n");
    vfprintf(logptr,pclError, args); fflush(logptr);
    va_end(args);                        
}

/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)               */
/* ******************************************************************** */
/* ******************************************************************** */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

