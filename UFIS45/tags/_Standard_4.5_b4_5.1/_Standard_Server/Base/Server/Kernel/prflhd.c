#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/prflhd.c 1.1 2005/04/08 15:41:06SGT akl Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* UFIS AS PRFLHD.C                                                           */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : April 2005                                                */
/* Description    : Process to set Field PRFL of AFTTAB                       */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_prflhd[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 



/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern int GetNoOfElements(char *s, char c);
extern long nap(long);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char      pcgHostName[XS_BUFF];
static char      pcgHostIp[XS_BUFF];

static int    igQueCounter=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static EVENT *prgOutEvent = NULL;
static char pcgCurrentTime[32];
static int igDiffUtcToLocal;

static char pcgLastDay[16];
static char pcgWaitTill[16];
static int igTriggerAction = FALSE;
static int igTriggerBchdl = FALSE;
static int igBcOutMode = 0;

static int    Init_prflhd();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static int GetConfig();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction);
static long GetSecondsFromCEDATime(char *pcpDateTime);
static void TrimRight(char *pcpBuffer);
static void TrimLeftRight(char *pcpBuffer);
static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP);
static int HandleCPRF();
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_prflhd);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_prflhd();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
	{
	  memset(prgItem,0x00,igItemLen);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  dbg(DEBUG,"QUE Counter %d",++igQueCounter);
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS ) 
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;    
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;    
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;    
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;    
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(1);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }
		  else
		    {
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break; 
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
                case  111 :
                  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */



	  /**************************************************************/
	  /* time parameter for cyclic actions                          */
	  /**************************************************************/
    
	  now = time(NULL);

	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_prflhd() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_prflhd()
{
  int    ilRc = RC_SUCCESS;            /* Return code */

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_prflhd : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_prflhd : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_prflhd : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_prflhd : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_prflhd : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_prflhd: use HOPO-field!");
	}
    }

  ilRc = TimeToStr(pcgCurrentTime,time(NULL));

  ilRc = GetConfig();

  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_prflhd();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_prflhd() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_prflhd() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclNewData[XXL_BUFF];
  char pclOldData[XXL_BUFF];
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  char pclTmpInitFog[32];
  char pclRegn[32];
  char pclStartTime[32];
  int ilLen;

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  memset(pclOldData,0x00,L_BUFF);
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  strcpy(pclNewData,pclData);
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  dbg(DEBUG,"Command:   <%s>",cmdblk->command);
/*
  dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
  dbg(DEBUG,"Fields:    <%s>",pclFields);
  dbg(DEBUG,"Data:      \n<%s>",pclNewData);
  dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
  dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
*/
  if (strcmp(cmdblk->command,"CPRF") == 0)
  {
     if (strcmp(pcgCurrentTime,pcgWaitTill) < 0)
        dbg(DEBUG,"HandleInternalData: CPRF Command not yet allowed");
     else
        ilRC = HandleCPRF();
  }
  else if (strcmp(cmdblk->command,"SPRF") == 0)
  {
     ilRC = TimeToStr(pcgWaitTill,time(NULL));
     dbg(DEBUG,"HandleInternalData: Now Handle CPRF Command");
  }

  dbg(DEBUG,"========================= START / END =========================");
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}

/*
	Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetConfig:";
  char pclTmpBuf[128];
  char pclDebugLevel[128];

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"%s Config File is <%s>",pclFunc,pcgConfigFile);

  ilRC = TimeToStr(pcgWaitTill,time(NULL));
  ilRC = iGetConfigEntry(pcgConfigFile,"PRFLHD","WAIT_AFTER_RESTART",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS || strcmp(pclTmpBuf,"-1") != 0)
     ilRC = AddSecondsToCEDATime(pcgWaitTill,atoi(pclTmpBuf)*60,1);

  ilRC = iGetConfigEntry(pcgConfigFile,"PRFLHD","LAST_DAY",CFG_STRING,pcgLastDay);
  if (ilRC != RC_SUCCESS)
     strcpy(pcgLastDay,"20050101");

  ilRC = iGetConfigEntry(pcgConfigFile,"PRFLHD","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
  if (strcmp(pclDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pclDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pclDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }

  ilRC = iGetConfigEntry(pcgConfigFile,"PRFLHD","TRIGGER_ACTION",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS && strcmp(pclTmpBuf,"YES") == 0)
     igTriggerAction = TRUE;
  ilRC = iGetConfigEntry(pcgConfigFile,"PRFLHD","TRIGGER_BCHDL",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS && strcmp(pclTmpBuf,"YES") == 0)
     igTriggerBchdl = TRUE;

  ilRC = iGetConfigEntry(pcgConfigFile,"PRFLHD","BC_OUT_MODE",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     igBcOutMode = atoi(pclTmpBuf);

  dbg(TRACE,"%s Wait after restart till %s UTC",pclFunc,pcgWaitTill);
  dbg(TRACE,"%s Last Day in History = %s",pclFunc,pcgLastDay);
  dbg(TRACE,"%s DEBUG_LEVEL = %s",pclFunc,pclDebugLevel);
  dbg(TRACE,"%s TRIGGER_ACTION = %d",pclFunc,igTriggerAction);
  dbg(TRACE,"%s TRIGGER_BCHDL = %d",pclFunc,igTriggerBchdl);
  dbg(TRACE,"%s BC_OUT_MODE = %d",pclFunc,igBcOutMode);

  return RC_SUCCESS;
} /* Enf of GetConfig */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;

  if (ipBchdl == TRUE && igTriggerBchdl == TRUE)
  {
     (void) tools_send_info_flag(1900,0,"prflhd","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send Broadcast: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }
  if (ipAction == TRUE && igTriggerAction == TRUE)
  {
     (void) tools_send_info_flag(7400,0,"prflhd","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send to ACTION: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }

  return ilRC;
} /* End of TriggerBchdlAction */


/*******************************************++++++++++*************************/
/*   GetSecondsFromCEDATime                                                   */
/*                                                                            */
/*   Serviceroutine, rechnet einen Datumseintrag im Cedaformat in Seconds um  */
/*   Input:  char *  Zeiger auf CEDA-Zeitpuffer                               */
/*   Output: long    Zeitwert                                                 */
/******************************************************************************/

static long GetSecondsFromCEDATime(char *pcpDateTime)
{
  long rc = 0;
  int year;
  char ch_help[5];
  struct tm *tstr1 = NULL, t1;
  struct tm *CurTime, t2;
  time_t llTime;
  time_t llUtcTime;
  time_t llLocalTime;

  CurTime = &t2;
  llTime = time(NULL);
  CurTime = (struct tm *)gmtime(&llTime);
  CurTime->tm_isdst = 0;
  llUtcTime = mktime(CurTime);
  CurTime = (struct tm *)localtime(&llTime);
  CurTime->tm_isdst = 0;
  llLocalTime = mktime(CurTime);
  igDiffUtcToLocal = llLocalTime - llUtcTime;
  /* dbg(TRACE,"UTC: <%ld> , Local: <%ld> , Diff: <%ld>",llUtcTime,llLocalTime,igDiffUtcToLocal); */

  memset(&t1,0x00,sizeof(struct tm));

  tstr1 = &t1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,pcpDateTime,4);
  year = atoi(ch_help);
  tstr1->tm_year = year - 1900;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[4],2);
  tstr1->tm_mon = atoi(ch_help) -1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[6],2);
  tstr1->tm_mday = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[8],2);
  tstr1->tm_hour = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[10],2);
  tstr1->tm_min = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[12],2);
  tstr1->tm_sec = atoi(ch_help);

  tstr1->tm_wday = 0;
  tstr1->tm_yday = 0;
  tstr1->tm_isdst = 0;
  rc = mktime(tstr1);

  rc = rc + igDiffUtcToLocal; /* add difference between local and utc */

  return(rc);
} /* end of GetSecondsFromCEDATime() */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static void TrimLeftRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[0];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && *pclBlank != '\0')
     {
        pclBlank++;
     }
     strcpy(pcpBuffer,pclBlank);
     TrimRight(pcpBuffer);
  }
} /* End of TrimRight */


static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpTime) == 12)
     strcat(pcpTime,"00");
  ilRC = AddSecondsToCEDATime(pcpTime,lpValue,ipP);
  return ilRC;
} /* End of MyAddSecondsToCEDATime */


static int HandleCPRF()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleCPRF:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[2048];
  char pclDataBuf[8000];
  char pclSelection[1024];
  char pclFieldList[1024];
  int ilRCdbWr = DB_SUCCESS;
  short slFktWr;
  short slCursorWr;
  char pclSqlBufWr[2048];
  char pclDataBufWr[8000];
  char pclSelectionWr[1024];
  int ilI;
  int ilNoEle;
  char pclParamList[1024];
  char pclCurPar[64];
  char pclAdid[16];
  char pclFtyp[64];
  char pclField[16];
  char pclTime[16];
  int ilConfigError;
  char *pclTmpPtr;
  char pclTmpBuf[1024];
  char pclLastTime[16];
  int ilCount;
  char pclUrno[16];

  ilRC = iGetConfigEntry(pcgConfigFile,"PRFLHD","PARAM_LIST",CFG_STRING,pclParamList);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%s No Parameter List defined",pclFunc);
     return ilRC;
  }
  ilNoEle = GetNoOfElements(pclParamList,',');
  for (ilI = 1; ilI <= ilNoEle; ilI++)
  {
     ilConfigError = FALSE;
     get_real_item(pclCurPar,pclParamList,ilI);
     ilRC = iGetConfigEntry(pcgConfigFile,pclCurPar,"ADID",CFG_STRING,pclAdid);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%s Parameter <ADID> not defined in Section <%s>",pclFunc,pclCurPar);
        ilConfigError = TRUE;
     }
     ilRC = iGetConfigEntry(pcgConfigFile,pclCurPar,"FTYP",CFG_STRING,pclFtyp);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%s Parameter <FTYP> not defined in Section <%s>",pclFunc,pclCurPar);
        ilConfigError = TRUE;
     }
     ilRC = iGetConfigEntry(pcgConfigFile,pclCurPar,"FLD",CFG_STRING,pclField);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%s Parameter <FLD> not defined in Section <%s>",pclFunc,pclCurPar);
        ilConfigError = TRUE;
     }
     ilRC = iGetConfigEntry(pcgConfigFile,pclCurPar,"TIME",CFG_STRING,pclTime);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%s Parameter <TIME> not defined in Section <%s>",pclFunc,pclCurPar);
        ilConfigError = TRUE;
     }
     if (ilConfigError == FALSE)
     {
        pclTmpPtr = strstr(pclFtyp,"P");
        if (pclTmpPtr != NULL)
           *pclTmpPtr = ' ';
        memset(pclTmpBuf,0x00,64);
        pclTmpPtr = pclFtyp;
        while (*pclTmpPtr != '\0')
        {
           strcat(pclTmpBuf,"'");
           strncat(pclTmpBuf,pclTmpPtr,1);
           strcat(pclTmpBuf,"',");
           pclTmpPtr++;
           if (*pclTmpPtr != '\0')
              pclTmpPtr++;
        }
        if (strlen(pclTmpBuf) > 0)
        {
           pclTmpBuf[strlen(pclTmpBuf)-1] = '\0';
           strcpy(pclFtyp,pclTmpBuf);
        }
        ilCount = 0;
        ilRC = TimeToStr(pclLastTime,time(NULL));
        ilRC = AddSecondsToCEDATime(pclLastTime,atoi(pclTime)*60*(-1),1);
        strcpy(pclFieldList,"URNO");
        sprintf(pclSelection,"WHERE %s BETWEEN '%s000000' AND '%s' AND ADID = '%s' AND FTYP IN (%s)",
                pclField,pcgLastDay,pclLastTime,pclAdid,pclFtyp);
        strcat(pclSelection," AND PRFL NOT IN ('C','U')");
        sprintf(pclSqlBuf,"SELECT %s FROM AFTTAB %s",pclFieldList,pclSelection);
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclUrno);
        while (ilRCdb == DB_SUCCESS)
        {
           ilCount++;
           sprintf(pclSqlBufWr,"UPDATE AFTTAB SET PRFL = 'C' WHERE URNO = %s",pclUrno);
           slCursorWr = 0;
           slFktWr = START;
           ilRCdbWr = sql_if(slFktWr,&slCursorWr,pclSqlBufWr,pclDataBufWr);
           if (ilRCdbWr == DB_SUCCESS)
           {
              commit_work();
              sprintf(pclSelectionWr,"WHERE URNO = %s",pclUrno);
              if (igBcOutMode == 0)
                 ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelectionWr,"PRFL","C",TRUE,TRUE);
              else if (igBcOutMode == 1)
                 ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelectionWr,"PRFL,#PRFL","C, ",TRUE,TRUE);
              else if (igBcOutMode == 2)
                 ilRC = TriggerBchdlAction("UFR","AFTTAB",pclSelectionWr,"PRFL,[CF],PRFL","C,[CF],C",TRUE,TRUE);
           }
           close_my_cursor(&slCursorWr);
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclUrno);
        }
        close_my_cursor(&slCursor);
        dbg(DEBUG,"%s Section: %s , %d Records updated",pclFunc,pclCurPar,ilCount);
     }
  }

  return ilRC;
} /* End of HandleCPRF */


