
#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h"  /* sets UFIS_VERSION, must be done defore mks_version */
/* static char mks_version[] = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/fdihdl.c 1.117 2011/07/22 13:09:07SGT ble Exp  $"; */
/*  static char mks_version[] = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/fdihdl.c 1.117 2011/07/12 13:09:07SGT dka Exp  $"; */
  static char mks_version[] = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/fdihdl.c 1.156 2012/06/01 13:09:07SGT dka Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  SMI                                                      */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/* 20060828 JIM: added trigger for GHSHDL/Billing WAW                         */
/* 20060828 JIM: send one event per AFT URNO when load was changed            */
/* 20060828 JIM: PRF 7954: Ignore signals                                     */
/* 20100301 MEI: Generic SQL Function which close cursor after it was called  */
/* 20100906 MEI: Continue the code development on multiple delay code with the*/
/*               addition of sub-delay code                                   */
/* 20101021 MEI: ATH report some delay code are generic without any airline info */
/* 20101027 MEI: Store archived delay code in DCMTAB instead of DCFTAB        */
/* 20101209 GFO: Allow FDIHDl to update USR Entries JIRA UFIS-91              */
/* 20110105 MEI: For delay code which is using generic basic data(exclude ALC3)*/
/*               fill ALC3 as blank in DCFTAB                                 */
/* 20110421 MEI: to fill in either DECN or DECA if these entries are found in basic data */
/* 20110412 DKA: Circular flights (UFIS-302) mix up of STOD as STOA (fdiaftn.src) */
/* 20110415 DKA: VIAL timings of AFTN updates origin instead of VIA's       */
/* 20110519 MEI: To allow empty entries for delay subcode info line         */
/* 20110628 DKA: Changes in fdiaftn.src					    */
/* 20110722 DKA: Add flag for fdiaftn.src - igAftnCopyOrgStdToAtdWhenViaDep */
/* 20110712 DKA: ver.1.117 - UFIS-678 Flight Movement Log for LDM. Changes in */
/*		 fdildm.src, fdihdl.c and fdikriskom.src                      */
/* 20110731 DKA: v.1.119: Change in fdikriscom.src                            */
/* 20110802 DKA: v.1.120: Extend args for CopyAftFieldToAftField.             */
/*               Change in fdiaftn.                                           */
/* 20110808 DKA: v.1.121 : Add update STAB and small delay to just after      */
/*               sending CEDA msg to FLIGHT. Related to UFIS-333.             */
/*               Perform similar copying of STOD to AIRD for MVT-VIA.         */
/* 20110822 DKA: v.1.122 : check in UFIS-678 changes for Flight Movement      */
/*               Log and added more safety checks for backwards compatibility */
/* 20110925 DKA: v.1.123 : UFIS-782 (Altea)                                   */
/*               SaveCompleteTlx() returns after SendAlteaLoaRes so as not to */
/*               insert or update TLXTAB for Altea.                           */
/*                                                                            */
/* 		 AFTN: Removed delay and manual update of STAB. To use        */
/*               the feature igAftnCopyOrgStdToAtdWhenViaDep, a FLIGHT with   */
/*		 ENABLE_HARDCODE_STAB is required. Currently only flight.1.85 */
/*		 special for BKK-AOT has this feature. 			      */
/*                                                                            */
/*               UFIS-975 Configurable exclusion of MVT infants.              */
/* 20110926 DKA: Configurable saving of Altea ATPIN to TLXTAB. Altea global   */
/*               flags for saving to TLXTAB corrected to be int.              */
/* 20110927 DKA: v.1.125 - fdialtea @I only one UNDER_LOAD per telex.         */
/* 20111024 DKA: v.1.126 - fdipal configurable fixed value of DPXTAB.PRID.    */
/*               Change described in UFIS-898.                                */
/* 20111029 DKA: v.1.127 - fdildm - configurable extraction of crew and       */
/*               technical staff from LDM flight line.                        */
/* 20111101 DKA: v.1.128 - fdihdl - SaveCmpTlx for text > 8000 char - modify  */
/*               where text is split for TelexPool display fix.               */
/* 20111117 DKA: v.1.129 - fdihdl - add property SCORE_CONTINUE_ON_ERROR      */
/* 20111118 CST: v.1.130 - fdiscore - add SCORE:DELETE_BY_STATUSCHANGE UFIS-1178*/
/* 20111208 DKA: v.1.131 - fdihdl - ALTEA TLXTAB.TIME to be system time. This */
/*               is already performed in existing code provided ALTEA messages*/
/*               remain as HeaderType7.                                       */
/*               fdihxx - Check ALTEA messages for more than one command.     */
/* 20111219 DKA: v.1.132 - fdiptm - send PTM alerts to COPHDL. Define target  */
/*               mod_id in fdihdl.c.                                          */
/* 20120201 DKA: v.1.133 - fdipsm - prevent duplicate DPXTAB entries if there */
/*               is existing CAL entries with new config item.                */
/* 20120207 DKA: v.1.134 - UFIS-1385: if loahdl is in systab, then send a LOA */
/*               msg to LOAHDL for those telex types that save to LOATAB.     */
/* 20120211 DKA: c.1.135 - Altea add checkin pax for @I. (fdialtea.1.09)      */
/* 20120214 GFO: Added DB_LoadUrnoList UFIS-1484                              */
/* 20120305 DKA: v.1.137 - AFTN to convert callsign number 0nnn to nnn        */
/*               UFIS-1441.                                                   */
/* 20120306 DKA: v.1.138 - UFIS-1491 MVT copy ONBD/OFBD to CHON/CHOF.         */
/*               Requires fdihdl.h ver 1.43, fdihxx v.1.29.                   */
/*               Note: fdipsc change to stop processing if pattern mismatch.  */
/*               (UFIS-1341). fdipsc v.1.5                                    */
/* 20120307 DKA: v.1.139          - test macro DB_LOAD_URNO_LIST for use of   */
/*               DB_LoadUrnoList per UFIS-1485 changes.                       */
/* 20120308 DKA: v.1.140 - UFIS-1491 copy ONBD/OFBD to CHON/CHOF in           */
/*               CheckTimeValues.                                             */
/*               UFIS-1582 - do not update actual times from MVT with future  */
/*               values, with a configurable check period into the future,    */
/*               [TELEX].[MVT_MAX_FUTURE_SECONDS].                            */
/*               UFIS-1592 - use CHO1/CHF1 instead, like ONDB/OFDB.           */
/*		 Remove actual times (LNDD,AIRD,etc) for MVT if they are more */
/*               than a configured period into the future.                    */
/*               To-do: create function "GetConfiguredFieldName()" in fdihxx  */
/*               so that given a telex type and fdihdl.cfg Dname, it returns  */
/*               the Fname. This avoids hard-coded LNDD, etc.                 */
/* 20120312 DKA: v.1.141 - Introduce flags to prevent ONBD/OFBD updates for   */
/*               arr/dep. For cases where on/off-chock is reqd but not on/off */
/*               -blocks. Add function in fdihxx to get configured fieldnames */
/*               RemoveActualFutureTimes also removes estimates in the past.  */
/* 20120315 DKA: v.1.142 - fdiaftn - pre-search for an existing proposed      */
/*               insertion before calling InsertFlight to avoid duplicates.   */
/* 20120316 DKA: v.1.143 - fdiaftn - v.1.38 - check STO[AD] are not in the    */
/*               past for FPL inserts.                                        */
/*               fdiaftn - skip inserts for configured suffixes, if in config.*/
/*               - UFIS-1621 - allow use of REGI instead of REGN.             */
/*               - MVT to allow use of flight line date for AFTTAB search.    */
/*               - UFIS-1600 - send STYP with AFTN ISF.                       */
/* 20120323 DKA: v.1.144 - UFIS-1613 : Add DPI telex type (AUH).              */
/*		 - UFIS-1671 - allow rejection of depature PTM.		      */
/*               - Fix bug in SendtoLoahdl for MVT.    			 	*/
/*		 - Fix non-standard PTM from DOH.				*/
/* 20120410 DKA: v.1.145 - UFIS-1754: Transfer bags excluded for configured	*/
/*		   airlines/destinations.					*/
/*		 - Fill up LOATAB.COFL if configured. Did not use LOATAB.CURN	*/
/*		   because that appeared to be half-implemented in FDI/src	*/
/*		   apart from fdiptm.src.					*/
/* 20120412 DKA: v.1.146							*/
/*                 UFIS-872 remove regn from MVT for sectors not directly of	*/
/*                 last station from/to HOPO.					*/
/*		   UFIS-1416 add possibility for LDM flight search to use 	*/
/*                 flight line date (similar to MVT). 				*/
/* 20120416 DKA: v.1.147 Specification for UFIS-1754 clarified to UFIS-1783	*/
/* 20120509 DKA: v.1.148 Skip evaluation to remove REGN in SearchFlight if   */
/*		   telex does not have complete info.                        */
/* 20120514 DKA: v.1.149 Bugfix fdihxx (v.1.32):GetFlightDate()              */
/* 20120515 DKA: v.1.150 Bugfix fdihxx (v.1.32):SetAkftab()                  */
/* 20120518 DKA: v.1.151 Bugfix fdipal (v.1.11) and corresponding change to  */
/*		   to fdipsm (1.18). Add flag for DPXTAB.ABSR to allow 'CAL' */
/* 20120521 DKA: v.1.152 fdiptm : UFIS-1754/1783 - add ground handled pax.   */
/* 20120522 DKA: v.1.153 Add flag to post-check telex date for LDM only.     */
/*		   UFIS-1416.						     */
/* 20120523 DKA: v.1.154 Configurable future days to search for day match in */
/*		   fdihxx:GetFlightDate. For cases where PAL connecting flt  */
/*		   is more than current hard-coded 2 days ahead.   	     */
/* 20120524 DKA: v.1.155 fdipal - ABSR 'CAL' for IRT with flag of v.1.151.   */
/* 20120601 DKA: v.1.156 reset igCurPsmValues in fdihdl.c to avoid an empty  */
/*		 PSM when igIgnoreDepPSMTelexesFromHomeAddr is true from     */
/*		 having the last valid PSM's data being saved to DPXTAB.     */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_fdihdl[]="@(#) UFIS 4.4 (c) ABB AAT/I fdihdl.c VS Version 4.4.1.16 07/01/2003 / AKL";


/* be careful with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <regex.h>
#include <unistd.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "syslib.h"
#include "send.h"
#include "db_if.h"

#include "ccsarray.h"
#include "fdihdl.h"
#include "fditools.h"
#include "itrek_fn.inc"   /* XML functions for ALTEA */

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   mod_id2 = 0;
static char  mod_name2[32];
static int   igSendToLoa = FALSE;
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static int   igNewReRead   = FALSE;
static int   igReReadRun   = FALSE;
static int   igScanPTM = FALSE;                   /* PTMS returned from table */
static char  cgConfigFile[512];
static int   igQueOut      = 0;
static int   igGlNoAck      = 0;
static char  pcgTwStart[64];
static char  pcgTwEnd[64];
static char  pcgTwHopo[64];
static char  pcgRecvName[64];
static char  pcgDestName[64];
static int  igCommandEnd=0;
static char pcgHomeAP[10];
static char pcgHome4AP[10];
static char pcgTABEnd[10];
static char pcgTwEndInfo[32];
static char pcgTwEndNew[32];
static char  pcgPTMFirst[64];
static char  pcgPTMBus[64];
static char  pcgPTMEco[64];
static char  pcgPTMPrem[64];
static char pcgCurLdmLine[512];
static int igSaveCompTlx=TRUE;
static int igTraceTlx=FALSE;
static int igFileSize=0;
static int igMultiTlx=FALSE;
static int igTraceTlxRes=FALSE;
static int igutc2local=FALSE;
static char pcgCfgFile[100];
static char pcgCfgFileKriscom[100];
static char pcgCfgFileAltea[100];
static char pcgUnique[20];
static char pcgAftnAftUrno[64];
static time_t lgGarbTime;
T_TLXINFO rgTlxInfo;
static T_RESLIST prgPTMResult;
static CCS_KEY *prgCmpTlxKey = NULL;
static CCS_KEY *prgMasterTlx = NULL;
static CCS_KEY *prgMasterCfg = NULL;
static CCS_KEY *prgMasterTlxArray = NULL;
static CCS_KEY *prgSlaveTlxArray = NULL;
static CCS_KEY *prgMasterCfgDoc = NULL;
static CCS_KEY *prgMasterCmdCfgArray = NULL;
static CCS_KEY *prgMasterCmpCfgArray = NULL;

static APX_VALUES *prgApxValues = NULL;
static int MaxApxRecs = MAX_APX_RECS;

static char pcgSubType[8];
static char pcgPTMPartNo[8];

static int igScheduleAction = FALSE;
static int igTlxTextFieldSize = 2000;
static int igStopTelexInterpretation = FALSE;
static int igTotalWeightCalForSin = FALSE;
static int igLdmAddInfants = TRUE;
static int igAllowCallSign = FALSE;
static int igLdmIncludeTransit = FALSE;
static int igSetAAADTimesForHomeApt = TRUE;
static int igSetAAADTimesForHomeAptAFTN = TRUE;
static int igSetETAIfONBOrATA = FALSE;
static int igUpdateFromAATelex = TRUE;
static int igSetAAOnBlockForHopo = TRUE;  /* UFIS-1491/1582 */
static int igSetADOffBlockForHopo = TRUE; /* UFIS-1491/1582 */
static int igMaxTimeDiff = 180;
static int igIgnoreOldTelexes = TRUE;
static int igBroadcastOldTelexes = TRUE;
static int igBroadcastTelexes = TRUE;
static int igTlxTooOld = FALSE;
static int igTlxTimeFound = FALSE;
static int igGetULDNumbersFromSISectionForCPM = FALSE;
static int igStoreCPMDetails = FALSE;
static int igStoreLDMDetails = FALSE;
static int igGetViaInfoFromLDM = FALSE;
static int igProcessRotTlx = FALSE;
static int igProcessArrLdmTlxAft = TRUE;
static int igProcessArrLdmTlxLoa = TRUE;
static int igProcessDepLdmTlxAft = TRUE;
static int igProcessDepLdmTlxLoa = TRUE;
static int igHandleMVTVIA = FALSE;
static int igClearETOnNXI = FALSE;
static int igIgnoreETIfADMissing = FALSE;
static int igNewLoatab = FALSE;
static int igNewLoatabUkey = FALSE;
static int igNewLoatabCofl = FALSE;
static int igArrDtlForPin = FALSE;
static int igArrayInsert = FALSE;
static int igCheckFlightDate = FALSE;
static int igUseStimForCheck = TRUE;
static int igHandleDLMTAB = FALSE;

/* for DCFTAB 15-SEP-2010 */
/* for DCMTAB 27-OCT-2010 */
static int igHandleDCFTAB = FALSE;
static int igMaxDelayInDCF = 0;
static int igMaxDelayInAFT = 2;
static int igInsInvalidDelayInDCF = TRUE;
static int igHandleMultipleDelay = FALSE;
static int igHandleDelaySubCode = FALSE;
static int igArchiveDCF = FALSE;
static int igNoDelCodes = 0;
static int igEraseDCF = FALSE;
static int igUpdateUSERRecords = FALSE;
static int igSyncDECADECN = FALSE;


static char pcgDLMSubTypes[64] = "AD,EA";
static char pcgDelFromDLM[64] = "'MVT'";
static int igFlightIsTooOld = -1;
static int igUpdateClosedFlight = TRUE;
static int igCalculateETAFromDEPMsg = FALSE;
static int igAccPTMFromOriginVia = FALSE;
static int igPSMInterpretation = FALSE;
static int igIgnoreDepPSMTelexesFromHomeAddr = FALSE;
static int igPALInterpretation = FALSE;
static int igCreate2DPXRecords = FALSE;
static char pcgPSMKeyWords[128] = "WCHR,DEAF,BLIND";
static char pcgPRMKeyWords[128] = "WCHR,DEAF,BLIND";
static int igPRMFromLDM = FALSE;
static char pcgHandlingTypeForPRM[128] = "";
static int igSendMsgToAlerterForPAL_PSM = FALSE;
static char pcgRalUrnoPal[16];
static char pcgRalTextPal[2048];
static char pcgRalUrnoPsm[16];
static char pcgRalTextPsm[2048];
static int igArrWindowStart = -60;
static int igArrWindowEnd = 600;
static int igDepWindowStart = -120;
static int igDepWindowEnd = 60;
static char pcgTimeArray1[24][25];
static char pcgTimeArray2[24][25];
static char pcgTimeArray3[24][25];
static char *pcgDataPtr;
static char pcgTlxOriginator[16] = "";
static int igCheckPscTelex = FALSE;
static char pcgUseRegnForFlightSearch[128] = "";
static char pcgRegnForFlightSearch[16] = "";
static int igScoreInterpretation = FALSE;
static char pcgScoreAppend[8192] = "";
static char pcgScoreAppendFieldList[8192] = "";

static int igProcessREGNifNotFound = FALSE;

/* DKA v.1.117 UFIS-209 */
static int igFlightMovementLog = 0;
static char pcgArrAlcToLdmtab [256];
static char pcgDepAlcToLdmtab [256];
static char pcgAlcTransitCalc [256];
static char pcgAlcDisembarkCalc [256];
static int  pcgFlightMovementLogTransitPeriod = 4320; /* minutes of 3 days */
static char pcgLdmTabBuf[1000*512];
static int igArrayPtrLdmTab;
static int igMaxDaysAheadForLdmTransit = 3; /* default */
static int igLdmCircularFlight = 0;

REC_DESC rgRecDesc;
static char pcgLoaBuf[1000*512];
static int igArrayPtr;
static char pcgNewUrnos[1000*16];
static int igLastUrno = 0;
static int igLastUrnoIdx = -1;

static char pcgAftnTelexType[8] = "ATC";
static char pcgDfsTelexType[8] = "DFS";
static int igAftnInsert = FALSE;
static int igAftnUpdate = FALSE;
static int igAftnConfigurableFields = FALSE;
static char pcgAftnTtyp[8] = "";
static char pcgAftnStyp[8] = "";
static char pcgUpdateAftnTypeList[1024] = "ALL";
static int igAftnInsertPLANOWANA = FALSE;
static int igAftnArrP1 = 30;
static int igAftnArrP2 = 30;
static int igAftnArrP3 = 180;
static int igAftnDepP1 = 15;
static int igAftnDepP2 = 15;
static int igAftnDepP3 = 180;

static int igAdfInsert = TRUE;
static char pcgAdfTtyp[8] = "";
static int igAdfUpdate = TRUE;
static int igAdfDelete = TRUE;
static int igAdfArrP1 = 30;
static int igAdfArrP2 = 30;
static int igAdfDepP1 = 15;
static int igAdfDepP2 = 15;
static int igAdfArrP3 = 30;
static int igAdfArrP4 = 30;
static int igAdfDepP3 = 15;
static int igAdfDepP4 = 15;

static int igAsdiArrP1 = 30;
static int igAsdiArrP2 = 30;
static int igAsdiDepP1 = 15;
static int igAsdiDepP2 = 15;

static int igDfsArrP1 = 30;
static int igDfsArrP2 = 30;
static int igDfsDepP1 = 15;
static int igDfsDepP2 = 15;

/* 20060828 JIM: added for GHSHDL/Billing WAW: */
static int igToBilling = 0;
static int igAftOrLoaChanged = 0;

/* Global Variables for current telex */
static char pcgAftUrno[32];
static int igAftReceiver = 7800;
static char pcgAftDestName[64];
static char pcgAftRecvName[64];
static char pcgAftTwStart[64];
static char pcgAftTwEnd[64];
static char pcgAftCommand[16];
static char pcgAftTable[16];
static char pcgAftSelection[100];
static char pcgAftFieldList[2000];
static char pcgAftValueList[COMPLETE_TELEX_LEN];
static char pcgTlxMstx[64];
static char cgTlxStatus;
static char pcgNextTlxUrno[32];
static char pcgLastTlxUrno[32];
static char pcgFlightNumber[32];
static char pcgFlightSchedTime[32];
static char pcgAftnSchedTime[32];
static int igHeaderTyp = 0;
static char pcgPTMUrnos[20][16];
static int igNoPTMTelexes;
static char pcgTlxUrnoList[1000][16];
static int igMaxTlxUrnoIdx;
static int igCurTlxUrnoIdx;
static char pcgAftPax1[32];
static char pcgAftPax2[32];
static char pcgAftPax3[32];
static char pcgAftBagw[32];
static char pcgAftAdid[8];
static char pcgAftOnbl[32];
static char pcgAftLand[32];
static char pcgAftAirb[32];
static int igNoDestinations;
static char pcgDestList[25][8];
static int igNoRoutes;
static char pcgRouteList[25][8];
static char pcgLdmFromApt[8];
static char pcgCsgn[16];
static char pcgTimeLine[2048];
static char pcgAlc3ForPRM[16];
static char pcgTlxTimeForPal[128];

static char pcgFltDate[16];
static char pcgFltDay[4];
static char pcgFltMonth[4];
static char pcgFltYear[8];
static int igTrustTlxDate = FALSE;

static UCM_VALUES prgUcmValues[100];
static int igCurUcmValues = 0;

static DLS_VALUES prgDlsValues[100];
static int igCurDlsValues = 0;

static CPM_VALUES prgCpmValues[100];
static int igCurCpmValues = 0;

static LDM_VALUES prgLdmValues[25];
static int igCurLdmValues = 0;

static FFM_VALUES prgFfmValues[100];
static int igCurFfmValues = 0;

static PSM_VALUES prgPsmValues[200];
static int igCurPsmValues = 0;

static PAL_VALUES prgPalValues[200];
static int igCurPalValues = 0;

static PSC_CONFIG_DATA prgPscConfigData[200];
static int igNoPscConfigData = 0;

static DPI_VALUES prgDpiValues;
static int igCurDpiCfgValues = 0;
static int igCurDpiBklValues = 0;
static int igCurDpiTrsValues = 0;
static int igCurDpiCkiValues = 0;
static int igCurDpiTrfValues = 0;
static int igCurDpiDisValues = 0;

static char pcgAftUrnoFromFDIT[32];

static TELEX_INPUT prgTelexInput[200];
static int igNoTelexInput = 0;

static KRISCOM_MAIN prgKrisMain;
static CLASS_CODES prgFirstClass[25];
static int igCntFirstClass = 0;
static CLASS_CODES prgBusinessClass[25];
static int igCntBusinessClass = 0;
static CLASS_CODES prgEconomyClass[25];
static int igCntEconomyClass = 0;
static KRISCOM_ATI prgKrisATI[200];
static ATI_VALUES prgAtiValues[500];
static int igCurAtiValues = 0;
static KRISCOM_DH prgKrisDH[200];
static DH_VALUES prgDhValues[500];
static int igCurDhValues = 0;
static KRISCOM_ATPI prgKrisATPI[200];
static ATPI_VALUES prgAtpiValues[500];
static int igCurAtpiValues = 0;
static KRISCOM_ATPINS prgKrisATPINS[200];
static ATPINS_VALUES prgAtpinsValues[500];
static int igCurAtpinsValues = 0;
static KRISCOM_ATPIN prgKrisATPIN[200];
static ATPIN_VALUES prgAtpinValues[500];
static ATPIN_VALUES_ARR prgAtpinValuesArr[500];
static ATPIN_VALUES_DEP prgAtpinValuesDep[500];
static int igCurAtpinValues = 0;
static int igCurAtpinValuesArr = 0;
static int igCurAtpinValuesDep = 0;
static KRISCOM_ATG prgKrisATG[200];
static ATG_VALUES prgAtgValues[500];
static int igCurAtgValues = 0;
static KRISCOM_LP prgKrisLP[200];
static KRISCOM_DE prgKrisDE[200];
static DE_VALUES prgDeValues[500];
static int igCurDeValues = 0;
static KRISCOM_PW prgKrisPW[200];
static PW_VALUES prgPwValues[500];
static int igCurPwValues = 0;

/* Altea equivalents */
static KRISCOM_MAIN prgAlteaMain;
static CLASS_CODES prgFirstClassAlt[25];
static int igCntFirstClassAlt = 0;
static CLASS_CODES prgBusinessClassAlt[25];
static int igCntBusinessClassAlt = 0;
static CLASS_CODES prgEconomyClassAlt[25];
static int igCntEconomyClassAlt = 0;
static KRISCOM_ATI prgAlteaATI[200];
static ATI_VALUES prgAtiValuesAlt[500];
static int igCurAtiValuesAlt = 0;
static KRISCOM_DH prgAlteaDH[200];
static DH_VALUES prgDhValuesAlt[500];
static int igCurDhValuesAlt = 0;
static KRISCOM_ATPI prgAlteaATPI[200];
static ATPI_VALUES prgAtpiValuesAlt[500];
static int igCurAtpiValuesAlt = 0;
static KRISCOM_ATPINS prgAlteaATPINS[200];
static ATPINS_VALUES prgAtpinsValuesAlt[500];
static int igCurAtpinsValuesAlt = 0;
static KRISCOM_ATPIN prgAlteaATPIN[200];
static ATPIN_VALUES prgAtpinValuesAlt[500];
static ATPIN_VALUES_ARR prgAtpinValuesArrAlt[500];
static ATPIN_VALUES_DEP prgAtpinValuesDepAlt[500];
static int igCurAtpinValuesAlt = 0;
static int igCurAtpinValuesArrAlt = 0;
static int igCurAtpinValuesDepAlt = 0;
static KRISCOM_ATG prgAlteaATG[200];
static ATG_VALUES prgAtgValuesAlt[500];
static int igCurAtgValuesAlt = 0;
static KRISCOM_LP prgAlteaLP[200];
static KRISCOM_DE prgAlteaDE[200];
static DE_VALUES prgDeValuesAlt[500];
static int igCurDeValuesAlt = 0;
static KRISCOM_PW prgAlteaPW[200];
static PW_VALUES prgPwValuesAlt[500];
static int igCurPwValuesAlt = 0;


static int igTimeToStopBookedPaxForATI = 0;
static int igTimeToStopBookedPaxForATPI = 0;

/* MEI 23-FEB-2010 */
static int igCalcTotOnboard = FALSE;

/* MEI 07-SEP-2010 */
DELAY_REAS_STRUCT rgDelayReas[20];

/* DKA 20110722 v.1.117 */
static int igAftnCopyOrgStdToAtdWhenViaDep = FALSE;
static int igAftnCopyOrgStdToAtdWhenViaDepFlag = FALSE;

/* DKA 20110712 - ver 1.117 */
static int igLoaInsertUrno  = -1;

#define MAX_IN_URNO_LIST 10000
static int igCurInUrnoList = 0;
static char pcgUrnoList[MAX_IN_URNO_LIST][16];

/* DKA 20110823 - Altea */
static int  igUseAkftab = 0;
static char pcgModnameAkftabTest [32];
static char pcgModnameAkftabSet [32];
static char pcgTagnameAlteaMessage [64];
static char pcgKriscomErrorMessage [128];
static char pcgAcceptedAlteaFlightStatus [64];
static char pcgAlteaAtpinStatus [64];
static int igAlteaSaveToTlxTab = 0;
static int igAlteaAtpinSaveToTlxTab = 0;

/* DKA 20110922 - UFIS-975 */
static int igMvtExcludeInfants = 0;

/* DKA 20111024 - ver.1.126 */
static char pcgPalPridFixedValue [64];

/* DKA 20111029 - ver1.127 */
static int igLdmExtractCrewTech = 0;

/* DKA 20111117 - ver.1.129 */
static int igScoreContinueOnError = 0;
static int igDeleteIsStatusChange = 0;

/* DKA 20111219 - ver.1.132 */
static int igPtmMsgDest = 0;

/* DKA 20120201 - ver.1.133 */
static int igPsmDelCAL = 0;

/* DKA 20120207 - ver.1.134 */
/* AFT URNO's from telexes that save to LOATAB - UFIS-1385.
   For most telexes this is only the main flight URNO. Exceptions
   are PTM,KRIS,ALTEA which will have URNO's of related arrivals or
   departures.
*/
static char pcgLoahdlFltUrnos [512];
static int  igLoahdlModid = 0;

/* UFIS-1485 */
static int igHandleUrnoRanges = FALSE;
static char pcgTableFullName[10] = "TLXTAB";
static char pcgTableKey[10] = "TLXTAB";

/* v.1.140 */
/* Allow actual times to be up to this number of seconds into the future */
static int igMvtMaxFutureSecs = 900; /* 15 minutes */

/* v.1.143 - Suffixes in callsign to flag not to insert for AFTN */
static char pcgAftnNoInsertForSuffix [32];
/* MVT - if flight line date should be used for AFTTAB search */
static int igMVTTrustTlxDateArr = 0;
static int igMVTTrustTlxDateDep = 0;
/* LDM - v.1.146 */
static int igLDMTrustTlxDateArr = 0;
static int igLDMTrustTlxDateDep = 0;

/* v.1.144 */
static int igApfiCkiAccept = 0;
static int igPtmAcceptDepFromHopo = 0;

/*
  v.1.145 - Thought keep this in [TELEX] instead of [PTM] because it might
  later be used for Altea/Kriscom where Transfer info is available
*/
static char pcgExcludeAlcApc3InTotalTransfer [256];
static char pcgPtmBaggageExcludedAirlines [32];
static char pcgPtmBaggageExcludedDests [64];

/* v.1.151 - used by fdipal and fdipsm - UFIS-1693 */
static int igPalDpxAbsrAllowCal = FALSE;

/* v.1.153 - UFIS-1416 */
static int igLDMCheckFlightDateArr = 0;
static int igLDMCheckFlightDateDep = 0; 

/* v.1.154 */
/* Default is the pre-existing hard-coded value */
static int igGetFlightDate_FutureDays = 2;

/******************************************************************************/
/* FDI Function prototypes                                                    */
/******************************************************************************/
static int AddCfgElement(char * pcpPtr, char *pcpCfgFile, T_CFG_ARRAY_ELEMENT *prpCfgArrayEPtr);
static int ChangeMonth(char *pcpResult, char *pcpInput);
static int CheckAlc(char *pcpResult,char *pcpInput,int ipCount);
static int CheckChar(char *pcpInput, char cpTyp, int ipCount);
/*static int ClearArray(CCS_KEY *prpKey,char cpInput);*/
static int Compare(void *pvpUserData,T_TLX_ELEMENT_KEY *a, T_TLX_ELEMENT_KEY *b); /* Compare Data func for CCSArrayfuncs */
static int CompareCfgArray(void *pvpUserData,T_FIELDLIST_KEY *a, T_FIELDLIST_KEY *b);
static int CompareFieldList(void *pvpUserData,T_FIELDLIST_KEY *a, T_FIELDLIST_KEY *b);
static int CompareTlxArray(void *pvpUserData,T_TLX_ARRAY_KEY *a, T_TLX_ARRAY_KEY *b);
static int DeleteTlxRow(char *pcpTlxKey);
static void DelTlxEntry(void);
static void DelArrEntry();
static int FindTlxCmd(char *pcpData, int *ipCmdCnt, int *ipGoOn, int ipHeaderTyp);
static int GetAlc(char **pcpInput,CCS_KEY *prpKey);
static int GetFltn(char **pcpInput,CCS_KEY *prpKey);
static void StoreApxValue(char *pcpTlxKey, char *pcpApxType, char *pcpApxValu);
static void DeployApxValue(char *pcpTlxKey, char *pcpAftUrno);
static void CopyLine(char *pcpDest, char *pcpSource);
static void GetUldInfo(char *pcpResult, char *pcpLine,int ipItem);
static char *GetLine(char *pcpLine,int ipLineNo);
static int GetFldNo(char *pcpFldBuf, char *pcpFldNam);
static int GetFldDat(char *pcpDatBuf, int ipFldNo, char *pcpResult);
static int GetField(char *pcpFldVal, char *pcpTlxTyp, char *pcpTlxStyp, char *pcpFldNam);

/*------------ written in .FDI/fdildm.src -------------------------*/
static void MakeLDMSum(char *pcpHomeAirport);
static int  HandleLDM(char *pcpData);
static int StoreLDMData(char *pcpIdent,char *pcpFlnuRurn,char *pcpType,
                        char *pcpDest,char *pcpValue,char *pcpFieldList,
                        char *pcpRecCont, int ipLdmTab);
static int StoreLDMDataTot(char *pcpIdent,char *pcpFlnuRurn,char *pcpType,
                           int ipValue,char *pcpFieldList,char *pcpRecCont,
                           int ipLdmTab);
static int GetCargoDataFromSI(char *pcpBeginSI);
static void GetLoadValueFromSI(char *pcpResult, char *pcpInput);
static int StoreLDMTAB (char *pcpIdent,char *pcpFlnuRurn,char *pcpType,
                        char *pcpDest,char *pcpValue,char *pcpFieldList);
static int SearchDepartureTransit (char *pcpArrUrno, char *pcppDepUrno);
static void EvaluateFlightMovementFlags (void);
static void GetLastOrNextStation (char *pcgAftUrno,
                         char *pcgLastOrNextStationAlc3, char pcpAdid);
static int StoreLDMDepTransit (char *pcpIdent,char *pcpUpdateType, int ipValue);
static int EvaluateCircular (char *pcpUrno);
static int LdmExtractCrewTech (char *pcpFlightLine);
static int LDM_SetTrustTlxDate(void);
/*------------ written in .FDI/fdicpm.src -------------------------*/
static int HandleCPM(char *pcpData);

/*------------ written in .FDI/fdiffm.src -------------------------*/
static int HandleFFM(char *pcpData);

/*------------ written in .FDI/fdihxx.src -------------------------*/
static int HandleIBT (char *pcpTable, char *pcpFields, char *pcpData,
                      char *pcpSelect);
static int HandleRTA (char *pcpTable, char *pcpFields, char *pcpData,
                      char *pcpSelect) ;
static int HandleFDI (char *pcpTable, char *pcpFields, char **pcpData,
                      char *pcpSelect, int ipSaveTlx) ;
static int HandleEXC (char *pcpTable, char *pcpFields, char *pcpData,
                      char *pcpSelect) ;
static int HandleRC (char *pcpLine) ;
static int HandleHdr1(char **pcpData);
static int HandleHdr2(char **pcpData);
static int HandleHdr3(char **pcpData);
static int HandleHdr4(char **pcpData);
static int HandleHdr5(char **pcpData);
static int HandleHdr6(char **pcpData);
static int HandleHdr7(char **pcpData);
static int HandleHdr8(char **pcpData);
static int HandleHdr9(char **pcpData);
static int HandleHdr10(char **pcpData);
static int HandleHdr11(char **pcpData);
static int HandleHdr12(char **pcpData);
static int HandleHdr13(char **pcpData);
static int HandleAA(char *pcpLine) ;
static int HandleAD(char *pcpLine) ;
static int HandleDL(char *pcpLine) ;
static int HandleEA(char *pcpLine) ;
static int HandleEB(char *pcpLine) ;
static int HandlePX(char *pcpLine) ;
static int HandleSI(char *pcpData) ;
static int HandleNI(char *pcpLine) ;
static int HandleED(char *pcpLine) ;
static int HandleEO(char *pcpLine) ;
static int HandleRR(char *pcpLine) ;
static void TrimRight(char *pcpBuffer);
static void AppendBlanks(char *pcpBuffer, int ipSize);
static void RemoveBlanks(char *pcpBuffer);
static void ReplaceChar(char *pcpBuffer, char clOrgChar, char clDestChar);
static void GetFlightDate(char *pcpLine, int ipWord, int ipItem);
static int CheckAkftab (char *pcpData);
static int SetAkftab (char *pcpData);
static int NormalizeFlno (char *pcpAltFlno, char *ufis_flno);
static int HandleAlteaMultiple (char *pcpResult, char **pcpData,
  int ipSaveTlx);
static int RemoveFutureActualTimes (char *pcpTlxType,
            char *pcpAftFieldList, char *pcpAftValueList);
static int GetCfgFieldName (int ipTlxCmd, char *pcpDName, char *pcpFName);

/*------------ written in .FDI/fdimvt.src -------------------------*/
static int HandleMVT(char *pcpData);
static int MVT_SetTrustTlxDate (void);

/*------------ written in .FDI/fdiptm.src -------------------------*/
static int HandlePTM(char *pcpData, char *pcpFlag);
static int ScanPTM(int ipEndTxtLine, char *pcpData, char *pcpFltKey, char *pcpBoard, char *pcpTrans);
static int SendPTMRes();
static int GetPTM(char *pcpFields, char *pcpData, char *pcpBuffer, int *ipNoLines,
                  char *pcpBoard, int ipPartNo);
static int PtmBaggageParseAirlineDestExclusion (void);

/*------------ written in .FDI/fdisxm.src -------------------------*/
static int HandleSXM(char *pcpData,int ipTyp);

/*------------ written in .FDI/fditdf.src -------------------------*/
static int CheckGarbTime();
static int UtcToLocal(char* pcpTime,char* pcpSeparator,char* pcpFormat);
static int Time2Date(char *pcpResult,char *pcpInput);
static int CheckTimeLine( void);
static int CheckTimeRow(char *pcpResult, char *pcpInput, int ipCount);
static int StrToTime(char *pcpTime,time_t *plpTime);
static int TimeToStr(char *pcpTime,time_t lpTime);

/*------------ written in .FDI/fdiasg.src -------------------------*/
static int HandleRQM(char *pcpData);
static int HandleDIV(char *pcpData);
static int HandleTPM(char *pcpData);
static int HandleETM(char *pcpData);
static int HandleFWD(char *pcpData);
static int HandleAFTN_PVG(char *pcpData, char *pcpAftUrno);
static int HandleUBM(char *pcpData);
static int HandleCCM(char *pcpData);

/*------------ written in .FDI/fdisto.src -------------------------*/
static int HandleSCR(char *pcpData);
static int HandleBTM(char *pcpData);
static int HandleBAY(char *pcpData);

/*------------ written in .FDI/fdiucm.src -------------------------*/
static int HandleUCM(char *pcpData);

/*------------ written in .FDI/fdidls.src -------------------------*/
static int HandleDLS(char *pcpData);

/*------------ written in .FDI/fdiaftn.src -------------------------*/
static int HandleAFTN(char *pcpData);
static int HandleFPL(char *pcpData);
static int HandleCHG(char *pcpData);
static int HandleCNL(char *pcpData);
static int HandleDLA(char *pcpData);
static int HandleARRDEP(char *pcpData);
static int CopyAftFieldToAftField (char *pcpAftUrno,
    char *pcpFieldList, char *pcpValueList, char *pcpFieldFrom, char *pcpFieldTo,
    char *pcpExtraFields, char *pcpExtraValues);

/*------------ written in .FDI/fdikriscom.src -------------------------*/
static int HandleKRISCOM(char *pcpData, char *pcpUrno, char *pcpCommand);
static int ReadKriscomConfig();
static int CheckTelexInputForKriscom(char *pcpData);
static int CalcTotOnboard(); /* MEI 01-MAR-2010 */

/*------------ written in .FDI/fdialtea.src -------------------------*/
static int HandleALTEA(char *pcpData, char *pcpUrno, char *pcpCommand);
static int ReadAlteaConfig();
static int CheckTelexInputForAltea(char *pcpData);
static int CalcTotOnboardAltea(); /* MEI 01-MAR-2010 */

/*------------ written in .FDI/fdiadf.src -------------------------*/
static int HandleADF(char *pcpData);

/*------------ written in .FDI/fdiasdi.src -------------------------*/
static int HandleASDI(char *pcpData);

/*------------ written in .FDI/fdiman.src -------------------------*/
static int HandleMAN(char *pcpData);

/*------------ written in .FDI/fdidfs.src -------------------------*/
static int HandleDFS(char *pcpData);

/*------------ written in .FDI/fdipsm.src -------------------------*/
static int HandlePSM(char *pcpData);

/*------------ written in .FDI/fdipal.src -------------------------*/
static int HandlePAL(char *pcpData);

/*------------ written in .FDI/fdipsc.src -------------------------*/
static int HandlePSC(char *pcpData, int ipIdx);
static int ReadPSCConfig();
static int CheckTelexInputForPSCData(char *pcpData);
static int ConvertDateToCEDA(char *pcpDate, char *pcpFormat);

/*------------ written in .FDI/fdiscore.src -------------------------*/
static int HandleSCORE(char *pcpData);

/*------------ written in .FDI/fdidpi.src -------------------------*/
static int HandleDPI(char *pcpData);
static int StoreDPIData(char *pcpIdent,char *pcpFlnuRurn,char *pcpType,
                        char *pcpValue,char *pcpFieldList, char *pcpRecCont);
void       InitDpiValues (DPI_VALUES *s_dpi_val);
static int  ExtractDPIvalues (char *pcpLine, char *pcpF, char *pcpJ, char *pcpY,
              int *piSum);

static int HandleTelex(int ipHeaderTyp,int ipSaveCompTlx,char **pcpData,int ipSaveTlx);
/*static int MakeArrEntry( char *pcpInput,char *pcpFlag);*/
static int MakeCfgFile();
static char *MakeFltKey(void);
static int PrintArrayStructure();
static int PrintCfgArrayStructure();
static int PruneFlight(char *prpInput,CCS_KEY *prpKey,int ipEnd);
static int RereadTlxtab();
static int ReadConfig(char *pcpCfgFile);
static int SaveCompleteTlx(char cpError, char *pcpData, int ipHeaderTyp,char *);
/*static int SearchCharAndReplaceAll(char *pcpData,char cpOldChar,char cpNewChar);*/
static int SearchInfoCmds();
static int SendTlxRes(int ipTlxCmd);
static void TraceTlx(char *pcpData);
static void *GetCfgArrayKeyData(void *pvpUserData,void *pvpData);
static void *GetFieldListKeyData(void *pvpUserData,void *pvpData);
static void *GetKeyData(void *pvpUserData,void *pvpData); /* Get KeyData from Data for CCSArrayfuncs*/
static void *GetTlxArrayKeyData(void *pvpUserData,void *pvpData);
static void *SlvGetTlxArrayKeyData(void *pvpUserData,void *pvpData);
static void get_unique(char *s);
static void SetSubtypeTo(char *SubType);
static int ChangeCharFromTo(char *pcpData,char *pcpFrom,char *pcpTo);
static int SendMVTLOARes();
static int SendLDMLOARes();
static int SendCPMLOARes();
static int SendUCMLOARes();
static int SendDLSLOARes();
static int SendFFMLOARes();
static int SendPSMDPXRes();
static int SendPALDPXRes();
static int SendDPILOARes();
static int GetLOAData(T_CFG_ARRAY_ELEMENT *pcpCfgArrElem, char *pcpField, T_TLXRESULT **prlTlxPtr);
/*static void PrintPM(regmatch_t *pm);*/
static void CheckCrLf(char *pcpText);

static int SearchFlight(char *pcpFields, char *pcpData, char *pcpUrno, char *pcpNewFields,
                        char *pcpNewData, int ipTlxCmd);
static int FdiHandleSql(char *pcpCommand, char *pcpTable, char *pcpUrno, char *pcpFields,
                        char *pcpData, char *pcpTwStart, char *pcpTwEnd, int ipSendToAction,
                        int ipSendToBchdl);
static int FdiSendSql(char *pcpSqlBuf);
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, char *pcpTwStart,
                              char *pcpTwEnd, int ipBchdl, int ipAction);
static int SaveTelexes(int ipHeaderTyp, char *pcpData);
static int RemoveItemFromList(char *pcpFields, char *pcpValues, char *pcpFieldName);
static int UpdateVial(char *pcpFields, char *pcpData, char *pcpFld, char *pcpVia);
static int CompressVial(char *pcpFields, char *pcpData);
static int CheckTimeValues(char *pcpFields, char *pcpData, char *pcpAdid);
static int CalcNewDay(int ipArray, char *pcpFieldName, char *pcpFieldValue, int ipSchHour);
static int CheckSequence(char *pcpField1, char *pcpField2, int *ipResult, char cpDir);
static int HandleFDIU(char *pcpSelection);
static int ProcessOldTelexes();
static int CheckTelexTime(char *pcpData, int ipHeaderType);
static void ConvertFlightLine(char *pcpLine);
static int GetMSTX();
static int HandleFTX();
static int SqlArrayInsert(char *pcpFldList, char *pcpValList, char *pcpDatList);
static int SqlArrayInsertTab(char *pcpTabName, char *pcpFldList, char *pcpValList, char *pcpDatList);
static int FillArray(char *pcpValueList);
static int HandleDCFTAB();
static int HandleDLMTAB();
static int SecondTimeCheck(char *pcpData, int ipPos, int ipHeaderTyp);

/* MEI 23-FEB-2010 */
static int RunSQL(char *pcpSelection, char *pcpData );

/* DKA 20120208 */
static int SendToLoahdl (void);

/******************************************************************************/
/* Extern Function prototypes                                                         */
/******************************************************************************/
extern int GetNoOfElements(char *s, char c);
extern int GetNextDataItem(char *pcpResult,char **pcpInput,char *pcpDel,char *pcpDef,char *pcpTrim);
extern int CountToDelim(char *pcpInput,int ipNum,char cpDelim);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int FindItemInList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
extern int GetWord(char *pcpResult, char *pcpInput,int ipNum);
extern int CedaDateToTimeStruct(struct tm *prpTimeStruct, char *pcpCedaDate);
extern int TimeStructToCedaDate( char *pcpCedaDate,struct tm *prpTimeStruct);
extern int AddSecondsToCEDATime(char *,long,int);
extern int BuildItemBuffer(char *, char *, int, char *);
extern int get_real_item(char *, char *, int);
extern int get_item_no(char *, char *, short);

extern int sql_if(short, short*, char*, char*);
extern int close_my_cursor(short *cursor);
extern int init_db(void);
extern int StrgPutStrg(char *, int *, char *, int, int, char *);
/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int Init_fdihdl();
static int  Reset(void);                       /* Reset program          */
static void Terminate(void);                   /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static int  HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

/******************************************************************************/
/* external SRC FDI handles function                                          */
/******************************************************************************/
#include "./FDI/fdildm.src"
#include "./FDI/fdihxx.src"
#include "./FDI/fdimvt.src"
#include "./FDI/fdicpm.src"
#include "./FDI/fdiptm.src"
#include "./FDI/fditdf.src"
#include "./FDI/fdisxm.src"
#include "./FDI/fdiasg.src"
#include "./FDI/fdisto.src"
#include "./FDI/fdiucm.src"
#include "./FDI/fdidls.src"
#include "./FDI/fdiaftn.src"
#include "./FDI/fdikriscom.src"
#include "./FDI/fdialtea.src"
#include "./FDI/fdiadf.src"
#include "./FDI/fdiasdi.src"
#include "./FDI/fdiman.src"
#include "./FDI/fdiffm.src"
#include "./FDI/fdidfs.src"
#include "./FDI/fdipsm.src"
#include "./FDI/fdipal.src"
#include "./FDI/fdipsc.src"
#include "./FDI/fdiscore.src"
#include "./FDI/fdidpi.src"

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int   ilRC = RC_SUCCESS;          /* Return code          */
  int   ilCnt = 0;

  INITIALIZE;           /* General initialization   */
  (void)SetSignals(HandleSignal);
  (void) UnsetSignals(); /* 20060828 JIM: who bothers about SIGCHLD? */
  dbg(TRACE,"MAIN: version <%s>",sccs_fdihdl);
  /* Attach to the MIKE queues */
  do
    {
      ilRC = init_que();
      if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

  sprintf(cgConfigFile,"%s/fdihdl",getenv("BIN_PATH"));
  ilRC = TransferFile(cgConfigFile);
  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
  /* uncomment if necessary */
  /* sprintf(cgConfigFile,"%s/fdihdl.cfg",getenv("CFG_PATH")); */
  /* ilRC = TransferFile(cgConfigFile); */
  /* if(ilRC != RC_SUCCESS) */
  /* { */
  /*    dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
  /* } */ /* end of if */
  ilRC = SendRemoteShutdown(mod_id);
  if(ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(DEBUG,"MAIN: waiting for status switch ...");
      HandleQueues();
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      if(igInitOK == FALSE)
    {
      ilRC = Init_fdihdl();
      if(ilRC != RC_SUCCESS)
        {
          dbg(TRACE,"Init_fdihdl: init failed!");
        } /* end of if */
    }/* end of if */
    } else {
      Terminate();
    }/* end of if */
  dbg(TRACE,"MAIN: initializing OK");

#ifdef DB_LOAD_URNO_LIST
  dbg(TRACE,"Init_fdihdl: ****** Using DB_LoadUrnoList *****");
#else
  dbg(TRACE,"Init_fdihdl: ****** Using GetNextValues *****");
#endif
  ilRC = ProcessOldTelexes();
  for(;;)
    {
      dbg(TRACE,"====================== START/END ====================");

      ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);

      /* depending on the size of the received item  */
      /* a realloc could be made by the que function */
      /* so do never forget to set event pointer !!! */
      prgEvent = (EVENT *) prgItem->text;

      if( ilRC == RC_SUCCESS )
    {

      /* Acknowledge the item */
      ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
      if( ilRC != RC_SUCCESS )
        {
                /* handle que_ack error */
          HandleQueErr(ilRC);
        } /* fi */

      switch( prgEvent->command )
        {
        case    HSB_STANDBY :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;
        case    HSB_COMING_UP   :
          ctrl_sta = prgEvent->command;
          HandleQueues();
          break;
        case    HSB_ACTIVE  :
          ctrl_sta = prgEvent->command;
          break;
        case    HSB_ACT_TO_SBY  :
          ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
          HandleQueues();
          break;
        case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
          ctrl_sta = prgEvent->command;
          Terminate();
          break;
        case    HSB_STANDALONE  :
          ctrl_sta = prgEvent->command;

          break;

        case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
          Terminate();
          break;
        case    RESET       :
          ilRC = Reset();
          break;

        case 555:
              ilRC = HandleFDIU("40548098,40557276");
          break;

        case 666 :
          /* Create new Config file in CFG_PATH  */
          ilRC = MakeCfgFile();

          break;

        case 667 :

          PrintArrayStructure();

          break;
        case 668 :
          igReReadRun = TRUE;
          igNewReRead = TRUE;
          RereadTlxtab();
          break;

        case 777 :
          PrintCfgArrayStructure();
          break;


        case    EVENT_DATA  :
          if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
        {
          ilRC = HandleData();
/*            CheckGarbTime(); */
          if(ilRC != RC_SUCCESS)
            {
              HandleErr(ilRC);
            }/* end of if */
        }else{
          dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
          DebugPrintItem(TRACE,prgItem);
          DebugPrintEvent(TRACE,prgEvent);
        }/* end of if */
          break;

        case    TRACE_ON :
          dbg_handle_debug(prgEvent->command);
          break;
        case    TRACE_OFF :
          dbg_handle_debug(prgEvent->command);
          break;
        default         :
          dbg(TRACE,"MAIN: unknown event");
          DebugPrintItem(TRACE,prgItem);
          DebugPrintEvent(TRACE,prgEvent);
          break;
        } /* end switch */
    } else {
      /* Handle queuing errors */
      HandleQueErr(ilRC);
    } /* end else */

    } /* end for */

  /* exit(0); */

} /* end of MAIN */


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_fdihdl()
{
  char pclFunc[]="Init_fdihdl:";
  int   ilRC = RC_SUCCESS;          /* Return code */
  char *pclPtr;
  char pclResult[32];
  int ilCount;

  T_TLXRESULT MVTLine;
  T_TLXRESULT *prlMVTLine;
  T_TLXHEAD   TLXLine;



  /* Init global Vars for HOPO  */
  /* get data from sgs.tab */
  if ((ilRC = tool_search_exco_data("ALL", "TABEND", pcgTABEnd)) != RC_SUCCESS)
    {
      dbg(TRACE,"%s Error cannot find entry <TABEND> in sgs.tab",pclFunc);
      Terminate();
    }
  dbg(DEBUG,"%s found TABEND: <%s>",pclFunc,pcgTABEnd);

  /* read HomeAirPort from SGS.TAB */
  if ((ilRC = tool_search_exco_data("SYS", "HOMEAP", pcgHomeAP)) != RC_SUCCESS)
    {
      dbg(TRACE,"%s Error cannot find entry <HOMEAP> in SGS.TAB",pclFunc);
      Terminate();
    }
  ilCount = 1;
  ilRC = syslibSearchDbData("APTTAB","APC3",pcgHomeAP,"APC4",pcgHome4AP,&ilCount,"\n") ;
  dbg(DEBUG,"%s found HOME-AIRPORT: <%s>/<%s>",pclFunc,pcgHomeAP,pcgHome4AP);

  sprintf(pcgTwEndInfo,"%s,%s,%s",pcgHomeAP,pcgTABEnd,mod_name);

  /* If Array exist, remove it  */
  if (prgMasterTlxArray != NULL){
    dbg(TRACE,"%s Destroy old MasterTlxArray",pclFunc);
    CCSArrayArrDestroy(&prgMasterTlxArray);
  }

  /* Init Main Telex Array   */
  prgMasterTlxArray = CCSArrayInit(prgMasterTlxArray,NULL,sizeof(T_TLX_ARRAY_KEY)
                   ,(CCS_COMPARE*) &CompareTlxArray
                   ,(CCS_KEY_PROC*) &GetTlxArrayKeyData
                   ,10,5,&ilRC);
  dbg(DEBUG,"%s CCSArrayInit TlxArray returned <%d>",pclFunc,ilRC);

  /* Init Slave key on Main Telex Array */
  prgSlaveTlxArray = CCSArrayInit(prgMasterTlxArray,NULL,sizeof(T_TLX_ARRAY_KEY)
                  ,(CCS_COMPARE*) &CompareTlxArray
                  ,(CCS_KEY_PROC*) &SlvGetTlxArrayKeyData
                  ,10,5,&ilRC);


  /* Init Config Array  */
  prgMasterCmdCfgArray = CCSArrayInit(prgMasterCmdCfgArray,NULL,sizeof(T_CFG_ARRAY_KEY)
                      ,(CCS_COMPARE*) &CompareCfgArray
                      ,(CCS_KEY_PROC*) &GetCfgArrayKeyData
                      ,10,5,&ilRC);
  dbg(DEBUG,"%s CCSCfgArrayInit CfgArray  returned <%d>",pclFunc,ilRC);

  prgMasterCmpCfgArray = CCSArrayInit(prgMasterCmpCfgArray,NULL,sizeof(T_CFG_ARRAY_KEY)
                      ,(CCS_COMPARE*) &CompareCfgArray
                      ,(CCS_KEY_PROC*) &GetCfgArrayKeyData
                      ,10,5,&ilRC);
  dbg(DEBUG,"%s CCSCfgArrayInit CfgArray  returned <%d>",pclFunc,ilRC);

  prgApxValues = (APX_VALUES *)malloc(sizeof(APX_VALUES) * (MaxApxRecs + 1));
  if (prgApxValues != NULL)
  {
    prgApxValues[0].Used = 0;
     dbg(TRACE,"ALLOCATED %d APX_VALUES", MaxApxRecs);
  } /* end if */
  else
  {
     dbg(TRACE,"CAN'T ALLOC APX_VALUES");
  } /* end else */

  /* Read config File*/
  sprintf(pcgCfgFile,"%s/%s.cfg", getenv("CFG_PATH"),mod_name);
  sprintf(pcgCfgFileKriscom,"%s/%s_kriscom.cfg", getenv("CFG_PATH"),mod_name);
  sprintf(pcgCfgFileAltea,"%s/%s_altea.cfg", getenv("CFG_PATH"),mod_name);

  ReadConfig(pcgCfgFile);

  CheckGarbTime();

  ilRC = init_db();
  if (ilRC == RC_SUCCESS)
  {
     dbg(TRACE,"Init DB successful");
  }
  else
  {
     dbg(TRACE,"Init DB failed , rc = %d",ilRC);
  }

  /*
    UFIS-1385 : check each installation for the existense of LOAHDL
  */
  igLoahdlModid = tool_get_q_id ("loahdl");
  if ((igLoahdlModid == RC_NOT_FOUND) || (igLoahdlModid == RC_FAIL))
  {
    dbg(TRACE,"%s Module Id of loahdl not found", pclFunc);
    igLoahdlModid = -1;
  }
  else
  {
    dbg(TRACE,"%s Module Id of loahdl is <%d>", pclFunc, igLoahdlModid);
  }

#ifdef DB_LOAD_URNO_LIST
  igHandleUrnoRanges = DB_GetUrnoKeyCode(pcgTableFullName,pcgTableKey);
  dbg(TRACE, "===============================================================");
  if (igHandleUrnoRanges != TRUE)
  {
    dbg(TRACE,"URNO POOL: GET URNO VALUES BY <GetNextValues> SNOTAB.ACNU");
  }
  else
  {
    dbg(TRACE,"URNO POOL: GET URNO NUMBERS BY <GetNextNumbers> NUMTAB.KEYS");
  }
  dbg(TRACE, "===============================================================");
#endif


  igCurInUrnoList = 0;
  igInitOK = TRUE;
  return(ilRC);

} /* end of initialize */


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRC = RC_SUCCESS;              /* Return code */


    dbg(TRACE,"Reset: now resetting");

    return ilRC;

} /* end of Reset */


/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{
  dbg(TRACE,"Terminate: beginn free resources");



  dbg(TRACE,"Terminate: now leaving ...");

  exit(0);

} /* end of Terminate */


/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int ilRC = RC_SUCCESS;          /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    case SIGCHLD:
      dbg(DEBUG,"Got a SIGCHLD signal");
      break;
    default :
        Terminate();
        break;
    } /* end of switch */
    exit(pipSig);

} /* end of HandleSignal */


/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */


/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int ilRC = RC_SUCCESS;

    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */

    return;
} /* end of HandleQueErr */


/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRC = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;

    do
    {

      ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS )
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */

            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;

            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;

            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;

            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;

            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;

            case    SHUTDOWN    :
                Terminate();
                break;

            case    RESET       :
                ilRC = Reset();
                break;

            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;

            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_fdihdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_fdihdl: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int  ilRC = RC_SUCCESS;
  int ilCount;
  char pclTable[100];
  char pclFunc[]="HandleData:";
  char pclActCmd[100];
  BC_HEAD *prlBchd;
  CMDBLK  *prlCmdblk;
  char    *pclSelect;
  char    *pclFields;
  char    *pclData;
  char    *pclPtr;
  int ilLen;
  char *pclTmpPtr;

  igQueOut  = prgEvent->originator;

  prlBchd   = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK *)((char *)prlBchd->data);
  pclSelect = (char *)prlCmdblk->data;
  pclFields = (char *)pclSelect + strlen(pclSelect) + 1;
  pclData   = (char *)pclFields + strlen(pclFields) + 1;

  if (prlBchd->rc == NETOUT_NO_ACK)
  {
     igGlNoAck = TRUE;
     dbg(DEBUG,"NO ANSWER EXPECTED (NO_ACK IS TRUE)");
  } /* end if */
  else
  {
     igGlNoAck = FALSE;
     dbg(DEBUG,"SENDER EXPECTS ANSWER (NO_ACK IS FALSE)");
  } /* end else */


  if (prlBchd->rc != RC_SUCCESS && prlBchd->rc != NETOUT_NO_ACK)
  {
     /* DebugPrintBchead(TRACE,prlBchd); */
     /* DebugPrintCmdblk(TRACE,prlCmdblk); */
     dbg(TRACE,"%s Originator %d",pclFunc,igQueOut);
     dbg(TRACE,"%s Error prlBchd->rc <%d>",pclFunc,prlBchd->rc);
     dbg(TRACE,"%s Error Description: <%s>",pclFunc,prlBchd->data);
     if (igReReadRun == TRUE)
     {
        RereadTlxtab();
     }
  }
  else
  {
     strcpy(pclTable,prlCmdblk->obj_name);
     strcpy(pclActCmd,prlCmdblk->command);

     /* Save Global Elements of CMDBLK */
     strcpy(pcgTwStart,prlCmdblk->tw_start);
     strcpy(pcgTwEnd,prlCmdblk->tw_end);

     /* Change sender to mod_name for reuse of twe  */
     strcpy(pcgTwEndNew,pcgTwEnd);
     ilCount = CountToDelim(pcgTwEnd,2,',');
     strcpy(&pcgTwEndNew[ilCount],mod_name);
     pclPtr= &pcgTwEndNew[ilCount];
     StringUPR((UCHAR*)pclPtr);

     dbg(DEBUG,"%s pcgTwEndNew <%s>",pclFunc,pcgTwEndNew);
     /* Save Global Elements of BC_HEAD */
     /* WorkStation */
     memset(pcgRecvName, '\0', (sizeof(prlBchd->recv_name) + 1));
     strncpy(pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name));
     /* User Login Name */
     memset(pcgDestName, '\0', (sizeof(prlBchd->dest_name) + 1));
     strncpy(pcgDestName, prlBchd->dest_name, sizeof(prlBchd->dest_name));
     dbg(DEBUG,"COMMAND <%s> FROM (%d) WKS <%s> USR <%s>",
         pclActCmd,igQueOut,pcgRecvName,pcgDestName);
     dbg(DEBUG,"SPECIAL INFO TWS <%s> TWE <%s>", pcgTwStart,pcgTwEnd);
     dbg(DEBUG,"--------- DATA ELEMENTS FROM SENDER --------");
     dbg(DEBUG,"TAB <%s>", pclTable);
     dbg(DEBUG,"SEL <%s>", pclSelect);
     dbg(DEBUG,"FLD <%s>", pclFields);
     dbg(DEBUG,"DAT\n<%.2000s>", pclData);
     dbg(DEBUG,"--------------------------------------------");

     if (strcmp(pclActCmd,"FDI") == 0 || strcmp(pclActCmd,"AFTN") == 0 ||
         strcmp(pclActCmd,"ADF") == 0 || strcmp(pclActCmd,"ASDI") == 0)
     {
        dbg(DEBUG,"COMMAND IDENTIFIED AS 'FDI'");
        strcpy(pcgAftUrnoFromFDIT,"");
        if (*pclData != '\0')
        {
           if (strcmp(pclActCmd,"FDI") == 0)
           {
              ilRC = HandleFDI(pclTable, pclFields, &pclData, pclSelect, TRUE);
           }
           else
           {
              if (strcmp(pclActCmd,"AFTN") == 0)
              {
                 ilRC = HandleFDI(pclTable, pclFields, &pclData, "TELEX,6", TRUE);
              }
              else
              {
                 if (strcmp(pclActCmd,"ADF") == 0)
                 {
                    pclTmpPtr = strstr(pclFields,"MSGI");
                    if (pclTmpPtr != NULL)
                    {
                       pclFields[strlen(pclFields)] = '\n';
                       ilRC = HandleFDI(pclTable, pclFields, &pclFields, "TELEX,9", TRUE);
                    }
                    else
                    {
                       ilRC = HandleFDI(pclTable, pclFields, &pclData, "TELEX,9", TRUE);
                    }
                 }
                 else
                 {
                    pclTmpPtr = strstr(pclFields,"MSGT");
                    if (pclTmpPtr != NULL)
                    {
                       pclFields[strlen(pclFields)] = '\n';
                       ilRC = HandleFDI(pclTable, pclFields, &pclFields, "TELEX,10", TRUE);
                    }
                    else
                    {
                       ilRC = HandleFDI(pclTable, pclFields, &pclData, "TELEX,10", TRUE);
                    }
                 }
              }
           }
        }
        else
        {
           dbg(TRACE,"HandleData: Error No Data attached");
           ilRC = RC_FAIL;
        }
     } /* end if FDI */
     else if (strcmp(pclActCmd,"FDIT") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS 'FDIT'");
        ilLen = get_real_item(pcgAftUrnoFromFDIT,pclSelect,3);
        if (*pclData != '\0' && strlen(pcgAftUrnoFromFDIT) > 0)
        {
           ilRC = HandleFDI(pclTable, pclFields, &pclData, pclSelect, TRUE);
        }
        else
        {
           dbg(TRACE,"HandleData: Error No Data or Urno attached");
           ilRC = RC_FAIL;
        }
     } /* end else if FDIT*/
     else if (strcmp(pclActCmd,"FDIU") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS 'FDIU'");
        ilRC = HandleFDIU(pclSelect);
     } /* end else if FDIU*/
     else if (strcmp(pclActCmd,"FTX") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS 'FTX'");
        ilRC = HandleFTX();
     } /* end else if FTX*/
     else if (strcmp(pclActCmd,"IBT") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS 'IBT'");
        ilRC = HandleIBT(pclTable, pclFields, pclData, pclSelect);
     } /* end else if IBT*/
     else if (strcmp(pclActCmd,"IRT") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS 'IBT'");
        ilRC = HandleIBT(pclTable, pclFields, pclData, pclSelect);
     } /* end else if IBT*/
     else if (strcmp(pclActCmd,"RTA") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS 'RTA'");
        ilRC = HandleRTA(pclTable, pclFields, pclData, pclSelect);
     } /* end else if RTA*/
     else if (strcmp(pclActCmd,"EXC") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS 'EXC'");
        ilRC = HandleEXC(pclTable, pclFields, pclData, pclSelect);
     } /* end else if EXC*/
     else if (strcmp(pclActCmd,"...") == 0)
     {
        dbg(TRACE,"COMMAND IDENTIFIED AS '...'");
        /* ilRC = Handle...(pclTable, pclFields, pclData, pclSelect); */
     } /* end else if */
     else
     {
        dbg(TRACE,"UNEXPECTED COMMAND <%s> RECEIVED", pclActCmd);
     } /* end else */

     if (ilRC != RC_SUCCESS)
     {
        /* dbg(TRACE,"--------- DATA ELEMENTS FROM SENDER --------"); */
        /* dbg(TRACE,"SEL <%s>", pclSelect); */
        /* dbg(TRACE,"FLD <%s>", pclFields); */
        /* dbg(TRACE,"DAT\n<%s>", pclData); */
        /* dbg(TRACE,"--------------------------------------------"); */
        ilRC = RC_SUCCESS;
     }
  }

  /*   PrintArrayStructure(); */

  return ilRC;

} /* end of HandleData */


/***************************************************
 * DelTlxEntry routine
 ****************************************************/
static void DelTlxEntry(void)
{
  int ilRC=RC_SUCCESS;
  char pclFunc[]="DelTlxEntry:";
  int ilLine,ilCol,ilPos;
  T_TLX_ARRAY_ELEMENT *prlResult;
  CCS_KEY *prlPtr;

  /*   PrintArrayStructure(); */

  dbg(DEBUG,"%s pcgTwStart <%s>",pclFunc,pcgTwStart);

  prlResult = CCSArrayGetData(prgSlaveTlxArray,pcgTwStart);

  if(prlResult != NULL)
    {
      if (prlResult->Count == 0)
    {
      dbg(DEBUG,"%s Found <%s> Key<%s>",pclFunc,prlResult->TlxType,prlResult->Unique);
      dbg(DEBUG,"%s Masterkey <%d>",pclFunc,prlResult->MasterKey);
      prlPtr = prlResult->MasterKey;

      if(prlPtr != NULL)
        {
          CCSArrayArrDestroy(&prlPtr);
          prlResult->MasterKey = NULL;
        }
      else
        dbg(DEBUG,"%s Masterkey is NULL",pclFunc);
      ilRC = CCSArrayDelete(prgSlaveTlxArray,pcgTwStart);
      dbg(DEBUG,"%s Delete returned <%d>",pclFunc,ilRC);

      ilRC = RC_SUCCESS;
    }
      else
    {
      prlResult->Count--;
    }
    }
  /*   PrintArrayStructure(); */

} /* End of DelTlxEntry*/


/************************************************************/
/* PrintArrayStructure                                      */
/*                                                          */
/* Print the contents of the major CCSArray                 */
/************************************************************/

static int PrintArrayStructure()
{
  T_TLX_ARRAY_ELEMENT *prlPtr1;
  T_TLXRESULT *prlPtr2;
  int ilRC=RC_FAIL;
  int ilCount=1;

  if(prgMasterTlxArray != NULL)
    {
      prlPtr1=CCSArrayGetFirst(prgMasterTlxArray);
      while( prlPtr1 != NULL)
    {
      dbg(DEBUG,"PAS %d: Array key <%s>",ilCount,prlPtr1->TlxType);
      dbg(DEBUG,"PAS %d: Array Unique <%s>",ilCount,prlPtr1->Unique);
      dbg(DEBUG,"PAS %d: Array Count <%d>",ilCount,prlPtr1->Count);
      dbg(DEBUG,"PAS %d: Array Masterkey <%d> ",ilCount++,prlPtr1->MasterKey);
      ilRC = RC_SUCCESS;
      /* #ifdef wurst */
      if (prlPtr1->MasterKey != NULL)
        {
          prlPtr2=CCSArrayGetFirst(prlPtr1->MasterKey);
        while(prlPtr2 != NULL)
          {
            if(strlen(prlPtr2->DValue) != 0 )
              dbg(DEBUG,"PAS:  Element  <%s> \t <%s>", prlPtr2->DName,prlPtr2->DValue);
            else
              dbg(DEBUG,"PAS:  Element  <%s> \t <%s>", prlPtr2->DName,prlPtr2->FValue);
            prlPtr2=CCSArrayGetNext(prlPtr1->MasterKey);
          }
        }
      /* #endif */
      prlPtr1=CCSArrayGetNext(prgMasterTlxArray);
    }
    }

  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"PAS: prgMasterTlxArray is empty");
  }



  return RC_SUCCESS;
} /* end of PrintArrayStructure  */

static int PrintCfgArrayStructure()
{
  char pclFunc[]="PrintCfgStruct:";
  T_CFG_ARRAY_ELEMENT *prlPtr1,*prlPtr2;
  T_FIELDLIST *prlPtr3;
  int ilRC = 1;
  int ilCount1 = 1;
  int ilCount2 = 1;

  dbg(DEBUG,"%s Print Cfg",pclFunc);

  CCSArrayKeySort(prgMasterCmdCfgArray);

  if(prgMasterCmdCfgArray != NULL)
    {
      prlPtr1=CCSArrayGetFirst(prgMasterCmdCfgArray);
      while(prlPtr1 != NULL)
    {
      dbg(DEBUG,"%s Cfg Cmd%d  <%s>",pclFunc,ilRC++,prlPtr1->CfgType);
      if(prlPtr1->MasterKey != NULL)
        {
          prlPtr3=CCSArrayGetFirst(prlPtr1->MasterKey);

          while(prlPtr3 != NULL)
        {
          dbg(DEBUG,"%s Telexcmd <%s> Entry <%s> ",pclFunc,prlPtr1->CfgType,
              prlPtr3->DName);
          if(prlPtr3->FName != NULL)
            dbg(DEBUG,"%s Tablefield is <%s>",pclFunc,prlPtr3->FName);
          else
            dbg(DEBUG,"%s Tablefield not set",pclFunc);

          prlPtr3=CCSArrayGetNext(prlPtr1->MasterKey);
        }
        }
      prlPtr1 = CCSArrayGetNext(prgMasterCmdCfgArray);
    }
    }
  return RC_SUCCESS;
} /* end of PrintCfgArrayStructure  */

/***************************************************
 * HandleTelex
 ****************************************************/

static int HandleTelex(int ipHeaderTyp,int ipSaveCompTlx,char **pcpData,int ipSaveTlx)
{
  int ilRC = RC_SUCCESS;
  int ilBreakOut;
  int ilCmdCount = 0;
  int ilGoOn = RC_SUCCESS;
  int clTlxStat = 'I';
  char pclFlag[32];
  char pclFunc[]="HandleTelex:";
  char *pclData=NULL;
  char pclChar[2],clSave;
  char pclGetPTM[4];
  T_TLXRESULT rlResult;
  T_TLXRESULT *prlResult;
  char *pclTrash;
  char pclResult[100];
  char pclFieldList[16];
  char pclValueList[16];
  int ilItemNo;
  int ilCount;
  char pclFldVal[128];
  char pclTmpBuf[128], pclTmpBuf2[128];

  /* Initialize structure for PTM*/
  memset(&prgPTMResult,0x00,RESLIST);
  prgPTMResult.AlloNum = 5;
  prgPTMResult.DstArray = calloc(prgPTMResult.AlloNum, DSTITEM);
  prgPTMResult.PosNum = 0;

  prgMasterTlx = NULL;

  igCommandEnd=0;
  igHeaderTyp = ipHeaderTyp;
  dbg(DEBUG,"%s Received Headertyp <%d>",pclFunc,ipHeaderTyp);

  strcpy(pcgSubType," ");
  strcpy(pcgPTMPartNo," ");

  memset(&rgTlxInfo,0,TLXINFO);
  pclData = &(**pcpData);
  CheckCrLf(pclData);
  pclTrash = pcgTrash;

  /*    Remove all characters listed in pcgTrash e.g. "\r\t" */
  while (*pclTrash != '\0')
  {
     dbg(DEBUG,"%s Delete Trash character <%d>",pclFunc,*pclTrash);
     /* SearchCharAndReplaceAll(pclData,*pclTrash,' '); */
     ChangeCharFromTo(pclData,pcgTrash,"     ");
     pclTrash++;
  }
  ChangeCharFromTo(pclData,",",".");

  /* Save all received telexes */
  if (ipSaveTlx == TRUE)
  {
     ilRC = SaveTelexes(ipHeaderTyp,pclData);
  }
  /* End of saving telexes */

  if (igStopTelexInterpretation == TRUE)
  {
     free(prgPTMResult.DstArray);
     dbg(TRACE,"%s Interpretation of telexes ist stopped !!!",pclFunc);
     return ilRC;
  }

  /* Clear Telexinformation struct */
  memset(&rgTlxInfo,0x00,TLXINFO);

  igAftnCopyOrgStdToAtdWhenViaDepFlag = FALSE;
  igLdmCircularFlight = FALSE;
  memset (pcgLoahdlFltUrnos, '\0', sizeof (pcgLoahdlFltUrnos));
  igCurPsmValues = 0; /* v.1.156 */

  while ((igCommandEnd != END_TELEX_STRING) && (ilRC == RC_SUCCESS))
  {
     ilCmdCount = 0;
     memset(pclGetPTM,0x00,sizeof(pclGetPTM));
     /* Set serial number for recognition  */
     memset(&pcgUnique,0,sizeof(pcgUnique));
     get_unique(pcgUnique);
     dbg(TRACE,"--------------- TELEX START ---------------");
     strcpy(pcgTimeLine,"");
     if (ipHeaderTyp != 9 && ipHeaderTyp != 10 && ipHeaderTyp != 11)
     {
        ilRC = CheckTelexTime(pclData,ipHeaderTyp);
     }
     switch (ipHeaderTyp)
     {
        case 1:
           ilRC = HandleHdr1(&pclData);
           break;
        case 2:
           ilRC = HandleHdr2(&pclData);
           break;
        case 3:
           ilRC = HandleHdr3(&pclData);
           break;
        case 4:
           ilRC = HandleHdr4(&pclData);
           break;
        case 5:
           ilRC = HandleHdr5(&pclData);
           break;
        case 6:
           ilRC = HandleHdr6(&pclData);
           break;
        case 7:
           ilRC = HandleHdr7(&pclData);
           break;
        case 8:
           ilRC = HandleHdr8(&pclData);
           break;
        case 9:
           ilRC = HandleHdr9(&pclData);
           break;
        case 10:
           ilRC = HandleHdr10(&pclData);
           break;
        case 11:
           ilRC = HandleHdr11(&pclData);
           break;
        case 12:
           ilRC = HandleHdr12(&pclData);
           break;
        case 13:
           ilRC = HandleHdr13(&pclData);
           break;
        default:
           dbg(TRACE,"%s Error Unknown Header Typ <%d>",pclFunc,ipHeaderTyp);
           ilRC = RC_FAIL;
     } /* end of switch */

     if (ilRC ==  RC_SUCCESS)
     {
        /*  Set Pointer to beginning of Telex */
        pclData += rgTlxInfo.TlxStart;
        if (prgMasterTlx != NULL)
        {
           dbg(DEBUG,"%s prg is not null",pclFunc);
           CCSArrayArrDestroy(&prgMasterTlx);
           dbg(DEBUG,"%s prg is now null",pclFunc);
        }
        prgMasterTlx = NULL;
        prgMasterTlx = CCSArrayInit(prgMasterTlx,NULL,TLX_ELEMENT_KEY,
                                    (CCS_COMPARE*) &Compare,
                                    (CCS_KEY_PROC*) &GetKeyData,
                                    MAX_TLX_ARR,ADD_TLX_ARR,&ilRC);
        /* Set String end (\0) to end of Text and save the former character  */
        clSave = pclData[rgTlxInfo.TxtEnd];
        pclData[rgTlxInfo.TxtEnd]= '\0';
        ilGoOn = RC_SUCCESS;
        ilBreakOut = FindTlxCmd(pclData,&ilCmdCount,&ilGoOn,ipHeaderTyp);
        do
        {
           dbg(DEBUG,"%s CCSArrayInit returned <%d>",pclFunc,ilRC);
           if (strlen(rgTlxInfo.TlxNumber) > 0)
           {
              memset(&rlResult,0,TLXRESULT);
              strcpy(rlResult.DName,"Seque");
              strcpy(rlResult.FValue,rgTlxInfo.TlxNumber);
              CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
           }
           cgTlxStatus = ' ';
           igAftOrLoaChanged = 0;
           strcpy(pcgAftUrno,"");
           strcpy(pcgFlightNumber,"");
           strcpy(pcgFlightSchedTime,"");
           strcpy(pcgAftnSchedTime,"");
           strcpy(pcgAlc3ForPRM,"");
           strcpy(pcgNextTlxUrno,"");
           strcpy(pcgLastTlxUrno,"");
           strcpy(pcgSubType," ");
           strcpy(pcgPTMPartNo," ");
           strcpy(pcgTlxMstx," ");
           pcgDataPtr = NULL;
           igNoDestinations = 0;
           igNoRoutes = 0;
           igNoDelCodes = 0;
           strcpy(pcgFltDate,"");
           strcpy(pcgFltDay,"");
           strcpy(pcgFltMonth,"");
           strcpy(pcgFltYear,"");
           igTrustTlxDate = FALSE;
           igCheckPscTelex = FALSE;
           strcpy(pcgRegnForFlightSearch,"");
           memset( rgDelayReas, 0, sizeof( rgDelayReas ) );
           if (igCurTlxUrnoIdx < igMaxTlxUrnoIdx)
           {
              strcpy(pclFieldList,"STAT");
              sprintf(pclValueList,"%c",FDI_STAT_CURRENT);
              if (igBroadcastOldTelexes == TRUE)
                 ilRC = FdiHandleSql("URT","TLXTAB",&pcgTlxUrnoList[igCurTlxUrnoIdx][0],
                                     pclFieldList,pclValueList,
                                     pcgTwStart,pcgTwEndNew,igScheduleAction,TRUE);
              else
                 ilRC = FdiHandleSql("URT","TLXTAB",&pcgTlxUrnoList[igCurTlxUrnoIdx][0],
                                     pclFieldList,pclValueList,
                                     pcgTwStart,pcgTwEndNew,igScheduleAction,FALSE);
           }
/* Used for Crash Test
if (igCurTlxUrnoIdx >= 1)
{
   exit(0);
}
*/
           switch(rgTlxInfo.TlxCmd)
           {
              case FDI_MVT:
             /* MakeArrEntry("MVT",""); */
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleMVT(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_LDM:
                 /* MakeArrEntry("LDM",""); */
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleLDM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_CPM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleCPM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_PTM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandlePTM(pclData,pclFlag);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 /* !!! MakeArrEntry MUST be behind HandlePTM not before  */
                 if (strlen(pclFlag) > 0)
                    sprintf(pclGetPTM,"%s,%d",pclFlag,ipHeaderTyp);
                 /*  MakeArrEntry("PTM",pclResult); */
                 break;
              case FDI_SAM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 /* MakeArrEntry("SAM",""); */
                 ilRC = HandleSXM(pclData,FDI_SAM);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 break;
              case FDI_SLC:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 /* MakeArrEntry("SLC",""); */
                 ilRC = HandleSXM(pclData,FDI_SLC);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 break;
              case FDI_SRM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 /* MakeArrEntry("SRM",""); */
                 ilRC = HandleSXM(pclData,FDI_SRM);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 break;
              case FDI_RQM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleRQM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_DIV:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleDIV(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_TPM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleTPM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_PSM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandlePSM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_ETM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleETM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_FWD:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleFWD(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_SCR:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleSCR(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_BTM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleBTM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_UCM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleUCM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_DLS:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleDLS(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_AFTN:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleAFTN(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_KRISCOM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleKRISCOM(pclData,NULL,NULL);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_ALTEA:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleALTEA(pclData,NULL,NULL);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_ADF:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleADF(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_ASDI:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleASDI(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_MAN:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleMAN(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_SCORE:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleSCORE(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_FFM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleFFM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_DFS:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleDFS(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC=RC_SUCCESS;
                 break;
              case FDI_UBM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleUBM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_CCM:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleCCM(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_BAY:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleBAY(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_PAL:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandlePAL(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_CAL:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandlePAL(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_PSC:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandlePSC(pclData,1);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_DPFI:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleDPI(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_APFI:
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 ilRC = HandleDPI(pclData);
                 clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 ilRC = RC_SUCCESS;
                 break;
              case FDI_OTHERS:
                 clTlxStat = FDI_STAT_TYPE_RECOGNIZED;
                 cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                 break;
              default:
                 clTlxStat = FDI_STAT_TYPE_UNKNOWN;
                 cgTlxStatus = FDI_STAT_TYPE_UNKNOWN;
                 if (ipHeaderTyp == 13)
                 {
                    cgTlxStatus = FDI_STAT_TYPE_RECOGNIZED;
                    ilRC = HandleAFTN_PVG(pclData,pcgAftnAftUrno);
                    clTlxStat = ilRC == RC_SUCCESS ? FDI_STAT_INTERPRETED : FDI_STAT_ERROR ;
                 }
                 else if (igNoTelexInput > 0 || igNoPscConfigData > 0)
                 {
                    ilRC = RC_FAIL;
                    if (igNoTelexInput > 0)
                       ilRC = CheckTelexInputForKriscom(pclData);
                    if (ilRC != RC_SUCCESS && igNoPscConfigData > 0)
                       ilRC = CheckTelexInputForPSCData(pclData);
                    if (ilRC != RC_SUCCESS)
                       dbg(TRACE,"%s No known Telex Command found",pclFunc);
                 }
                 else
                    dbg(TRACE,"%s No known Telex Command found",pclFunc);
                 ilRC = RC_SUCCESS;
           } /* end of switch */

           /* Restore character at TelexEnd position*/
           pclData[rgTlxInfo.TxtEnd] = clSave;
           if (ipSaveCompTlx == TRUE)
           {
              if (strcmp(pcgSubType,"PLA") != 0)
              {
                 ilRC = SaveCompleteTlx(clTlxStat,pclData,ipHeaderTyp,pclGetPTM);
              }
           }
           if (strlen(pcgAftUrno) > 0)
           {
              switch(rgTlxInfo.TlxCmd)
              {
                 case FDI_MVT:
                 /* Remove unwanted fields per configuration or logic */

                    if (strlen(pcgAftPax1) != 0 || strlen(pcgAftPax2) != 0 ||
                        strlen(pcgAftPax3) != 0)
                    {
                       ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"PAXT");
                    }
                    if (igSetAAADTimesForHomeApt == FALSE)
                    {
                       if (strcmp(pcgAftAdid,"A") == 0)
                       {
                          ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"LNDD");
                          ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"ONBD");
                       }
                       else
                       {
                          ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"OFBD");
                          ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"AIRD");
                       }
                    }
                    if (igSetETAIfONBOrATA == FALSE)
                    {
                       if (strlen(pcgAftOnbl) > 0 || strlen(pcgAftLand) > 0)
                       {
                          ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"ETAE");
                       }
                    }
                    if (igUpdateFromAATelex == FALSE)
                    {
                       if (strcmp(pcgSubType,"AA") == 0)
                       {
                          strcpy(pcgAftFieldList,"");
                          strcpy(pcgAftValueList,"");
                       }
                    }
                    if (igClearETOnNXI == TRUE)
                    {
                       if (strcmp(pcgSubType,"NI") == 0)
                       {
                          if (strcmp(pcgAftAdid,"A") == 0)
                          {
                             ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,
                                                       "ETAE");
                             strcat(pcgAftFieldList,",ETAE,ETOA");
                             strcat(pcgAftValueList,", , ");
                          }
                          else
                          {
                             ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,
                                                       "ETDE");
                             strcat(pcgAftFieldList,",ETDE,ETOD");
                             strcat(pcgAftValueList,", , ");
                          }
                       }
                    }
                    if (igIgnoreETIfADMissing == TRUE)
                    {
                       if (strcmp(pcgSubType,"EA") == 0 && strcmp(pcgAftAdid,"A") == 0)
                       {
                          if (strstr(pcgAftFieldList,"AIRD") == 0 &&
                              strstr(pcgAftFieldList,"OFBD") == 0)
                          {
                             ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,
                                                       "ETAE");
                          }
                       }
                    }
                    if (igHandleDCFTAB == TRUE)
                       ilRC = HandleDCFTAB();
                    if (igHandleDLMTAB == TRUE)
                       ilRC = HandleDLMTAB();
                    if (strstr(pcgAftFieldList,"DCD1") != NULL && strstr(pcgAftFieldList,"DCD2") == NULL)
                    {
                       strcat(pcgAftFieldList,",DCD2,DTD2");
                       strcat(pcgAftValueList,", , ");
                    }

                    if (igSetAAOnBlockForHopo == FALSE) /* UFIS-1491/1582 */
                    {
                      if ((strcmp(pcgSubType,"AA") == 0) && (strcmp(pcgAftAdid,"A") == 0))
                      {
                        ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"ONBD");
                      }
                    }

                    if (igSetADOffBlockForHopo == FALSE) /* UFIS-1491/1582 */
                    {
                      if ((strcmp(pcgSubType,"AD") == 0) && (strcmp(pcgAftAdid,"D") == 0))
                      {
                        ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"OFBD");
                      }
                    }

                    /* This also removes estimates in the past */
                    (void) RemoveFutureActualTimes ("MVT",pcgAftFieldList,pcgAftValueList);
                    
                    break;
                 case FDI_CPM:
                    if (strlen(pcgAftBagw) != 0)
                    {
                       ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"CGOT");
                    }
                    break;
                 case FDI_LDM:
                    if (strcmp(pcgAftAdid,"A") == 0 && igProcessArrLdmTlxAft == FALSE)
                    {
                       strcpy(pcgAftFieldList,"");
                       dbg(DEBUG,"%s Flight not updated due to configuration parameter",pclFunc);
                    }
                    else if (strcmp(pcgAftAdid,"D") == 0 && igProcessDepLdmTlxAft == FALSE)
                    {
                       strcpy(pcgAftFieldList,"");
                       dbg(DEBUG,"%s Flight not updated due to configuration parameter",pclFunc);
                    }
                    break;
              }
              if (strcmp(pcgAftAdid,"D") == 0 && strlen(pcgAftAirb) > 0)
              {
                 ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"REGN");
                 ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"REGI");
                 dbg( TRACE, "%s Dep flight has been AIRB <%s>, ignored REGN/REGI update", pclFunc, pcgAftAirb );
              }
         if (igProcessREGNifNotFound == FALSE)  {
              ilItemNo = get_item_no(pcgAftFieldList,"REGN",5) + 1;
              if (ilItemNo <= 0) /* Might be REGI for this telex type */
                ilItemNo = get_item_no(pcgAftFieldList,"REGI",5) + 1;
              if (ilItemNo > 0)
              {
                 get_real_item(pclFldVal,pcgAftValueList,ilItemNo);
                 TrimRight(pclFldVal);
                 ilCount = 1;
                 ilRC = syslibSearchDbData("ACRTAB","REGN",pclFldVal,"ACT3",pclTmpBuf,&ilCount,"\n");
                 if (ilRC != RC_SUCCESS || ilCount == 0)
                 {
                    dbg(TRACE,"%s REGN <%s> not found in ACRTAB ==> REGN ignored",pclFunc,pclFldVal);
                    ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"REGN");
                    ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"REGI");
                 }
              }
          }
        /*
              ilItemNo = get_item_no(pcgAftFieldList,"REGN",5) + 1;
              if (ilItemNo > 0)
              {
                 get_real_item(pclFldVal,pcgAftValueList,ilItemNo);
                 TrimRight(pclFldVal);
                 ilCount = 1;
                 ilRC = syslibSearchDbData("ACRTAB","REGN",pclFldVal,"ACT3",pclTmpBuf,&ilCount,"\n");
                 if (ilRC != RC_SUCCESS || ilCount == 0)
                 {
                    dbg(TRACE,"%s REGN <%s> not found in ACRTAB ==> REGN ignored",pclFunc,pclFldVal);
                    ilRC = RemoveItemFromList(pcgAftFieldList,pcgAftValueList,"REGN");
                 }
              }
`       */
              if (strlen(pcgAftFieldList) > 0)
              {
                 ilRC = SendCedaEvent(igAftReceiver,0,pcgAftDestName,pcgAftRecvName,
                                      pcgAftTwStart,pcgAftTwEnd,
                                      pcgAftCommand,pcgAftTable,pcgAftSelection,
                                      pcgAftFieldList,pcgAftValueList,"",4,NETOUT_NO_ACK);
                 dbg(DEBUG,"%s Selection <%s>",pclFunc,pcgAftSelection);
                 dbg(DEBUG,"%s FieldList <%s>",pclFunc,pcgAftFieldList);
                 dbg(DEBUG,"%s ValueList <%s>",pclFunc,pcgAftValueList);
                 dbg(DEBUG,"%s Sent <%s>. Returned <%d>",pclFunc,pcgAftCommand,ilRC);

                 /* UFIS-333 */
                 if ((igAftnCopyOrgStdToAtdWhenViaDep == TRUE)
                   && (igAftnCopyOrgStdToAtdWhenViaDepFlag == TRUE))
                 { 
                   igAftnCopyOrgStdToAtdWhenViaDepFlag = FALSE;
                 }
              }

              /* UFIS-1385 */
              /* Have to be outside of above block because e.g. PSC does not
                 send anything to FLIGHT
              */
              if (igLoahdlModid > 0)
                SendToLoahdl ();

              /* 20060828 JIM: send one event per AFT URNO when load was changed: */
              /* 20060828 JIM: added for GHSHDL/Billing WAW: */
              if ((igToBilling != 0) && (igAftOrLoaChanged == 1 ))
              {
                 ilRC = SendCedaEvent(igToBilling,0,"fdihdl","CEDA",pcgAftTwStart,pcgAftTwEnd,"URT","AFTTAB",
                                      pcgAftUrno,"URNO",pcgAftUrno,"",3,NETOUT_NO_ACK);
                 dbg(DEBUG,"Send to Billing: URT,AFTTAB,URNO,<%s>",pcgAftUrno);
              }
           }
           if (prgMasterTlx != NULL)
           {
              CCSArrayArrDestroy(&prgMasterTlx);
           }
           /* Set String end (\0) to end of Text and save the former character  */
           clSave=pclData[rgTlxInfo.TxtEnd];
           pclData[rgTlxInfo.TxtEnd]= '\0';
           prgMasterTlx = NULL;
           prgMasterTlx = CCSArrayInit(prgMasterTlx,NULL,TLX_ELEMENT_KEY,
                                       (CCS_COMPARE*) &Compare,
                                       (CCS_KEY_PROC*) &GetKeyData,
                                       MAX_TLX_ARR,ADD_TLX_ARR,&ilRC);
           if (igMultiTlx == FALSE || ilGoOn == RC_FAIL || ilBreakOut == RC_FAIL)
           {
              ilBreakOut = RC_FAIL;
           }
           else
           {
              ilBreakOut = FindTlxCmd(pclData,&ilCmdCount,&ilGoOn,ipHeaderTyp);
           }
        } while (ilBreakOut != RC_FAIL );
        /* Restore character at TelexEnd position*/
        pclData[rgTlxInfo.TxtEnd] = clSave;
     } /* end of if  */
     /* Set Pointer to end of Telex */
     pclData += rgTlxInfo.TlxEnd-1;
     dbg(TRACE,"--------------- TELEX END ---------------");

     /* Delete Entry for space reason only if */
     if (rgTlxInfo.TlxCmd != FDI_PTM || igScanPTM != TRUE)
     {
        DelArrEntry();
     }
  } /* end of while  */

  if (igCommandEnd == END_TELEX_STRING)
  {
     ilRC = RC_SUCCESS;
  }

  free(prgPTMResult.DstArray);

  return (ilRC);
}/* end of HandleTelex */

/*********************************************************/

/*********************************************************/

static void DelArrEntry()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="DelArrEntry:";
  T_TLX_ARRAY_ELEMENT *prlTlxArrEle=NULL;
  CCS_KEY *prlKeyPtr;

  prlKeyPtr = prgMasterTlx;

  if (prgMasterTlx != NULL){
    dbg(DEBUG,"%s prgMasterTlx is not NULL",pclFunc);
    CCSArrayArrDestroy(&prlKeyPtr);
    prgMasterTlx = NULL;
  } else {
    dbg(DEBUG,"%s prgMasterTlx is NULL",pclFunc);
  }


  if (prlKeyPtr != NULL)
    dbg(DEBUG,"%s prlKeyPtr is not NULL",pclFunc);
  else
    dbg(DEBUG,"%s prlKeyPtr is NULL",pclFunc);

  /*
    prlTlxArrEle = CCSArrayGetData(prgSlaveTlxArray,pcgUnique);

    prlKeyPtr = prlTlxArrEle->MasterKey;
    if(prlKeyPtr != NULL) {
    CCSArrayArrDestroy(&prlKeyPtr);
    prlTlxArrEle->MasterKey = NULL;
    } else {
    dbg(DEBUG,"%s Masterkey is NULL",pclFunc);
    }
  */
}/* end of DelArrEntry */

/*********************************************************/

/*********************************************************/

static void TraceTlx(char *pcpData)
{
  int ilRC=RC_SUCCESS;
  static int ilStart=TRUE;
  char pclFunc[]="TraceTlx:";
  char pclFilename[200];
  static FILE *pflTraceFile=NULL;
  static time_t rlOldTime;
  time_t rlNewTime;
  struct stat rlstbuf;

  rlNewTime  = time(0L);

  if (ilStart == TRUE){
   rlOldTime = rlNewTime;
   ilStart = FALSE;
  }

  if(rlOldTime+86400 < rlNewTime){
    rlOldTime = rlNewTime;
    if(pflTraceFile != NULL)
    fclose(pflTraceFile);
    pflTraceFile = NULL;
  }

  if(pflTraceFile == NULL){
    sprintf(pclFilename,"%s/%s.%s.trc",getenv("DBG_PATH"),mod_name,GetTimeStamp());
    pflTraceFile = fopen(pclFilename,"a");
    rlOldTime = time(0L);
  }

    fprintf(pflTraceFile,"<<%s: Timestamp %s  >>\n",mod_name,GetTimeStamp());
    fprintf(pflTraceFile,"%s",pcpData);
    fflush(pflTraceFile);

  if (fstat(fileno(pflTraceFile),&rlstbuf) !=  RC_FAIL){
    if(rlstbuf.st_size > igFileSize){
      dbg(DEBUG,"%s Trace file > %d bytes. Open new one. ",pclFunc,igFileSize);
      fclose(pflTraceFile);
      pflTraceFile = NULL;
    }
  }


}

/*********************************************************/
static int SaveCompleteTlx( char cpError, char *pcpData, int ipHeaderTyp, char *pcpFlag)
{
  int ilRC=RC_SUCCESS;
  int ilCol,ilPos,ilLine;
  int ilArr;
  char pclFunc[]="SaveCmpTlx:";
  int ilYear=0;
  int ilReceiver;
  int ilTlxSplit=FALSE;
  int ilSplitC = 0;
  char pclTable[10];
  char pclTwEndNew[32];
  char pclCommand[10];
  int  ilCount=0;
  int pclFlag = RC_SUCCESS;
  int ilPosT1 = RC_SUCCESS;
  int ilPosT2 = RC_SUCCESS;
  int ilPosP  = RC_SUCCESS;
  int ilPosB  = RC_SUCCESS;
  int ilPartNo = 0;
  int ilNoParts = 1;
  char pclTwStart[30];
  char pclSelection[100];
  char pclFieldList[2000];
  char pclValueList[COMPLETE_TELEX_LEN];
  char pclSaveList[COMPLETE_TELEX_LEN];
  char pclResult[100];
  char *pclSplitPtr=NULL;
  char pclTlxTxt1[10];
  char pclTlxTxt2[10];
  char pclPartNo[10];
  char pclNoParts[10];
  T_TLXRESULT *prlTlxPtr;
  T_TLXRESULT rlResult;
  T_CFG_ARRAY_ELEMENT *prlCfgArrElem;
  T_FIELDLIST *prlCfgField;
  char pclText[32000];
  int ilTextSize;
  int ilTextSize2;
  char pclSendCommand[16];
  char pclSendModId[16];
  char pclSendPrio[16];
  char pclSendData[16];
  int ilSendModId;
  int ilSendPrio;
  char pclSendSelection[128];

  /* Dummy Selection*/
  strcpy(pclSelection,"           ");

  memset(&rlResult,0,TLXRESULT);
  memset(pclValueList,0,COMPLETE_TELEX_LEN);
  memset(pclSaveList,0,COMPLETE_TELEX_LEN);
  memset(pclFieldList,0,2000);

/*
  if (ipHeaderTyp == 1)
  {
     prlTlxPtr = CCSArrayGetData(prgMasterTlx,"FDate");
      Build receive time from Telex time fields
     if (rgTlxInfo.TlxMonth[0] != '\0' && rgTlxInfo.TlxYear[0] != '\0' &&
         rgTlxInfo.TlxTime[0] != '\0' && prlTlxPtr != NULL)
     {
        ilYear = atoi(rgTlxInfo.TlxYear);
        if (ilYear > FDI_BORDER_YEAR)
           ilYear += 1900;
        else
           ilYear += 2000;
           sprintf(rlResult.FValue,"%""d%""s%""s%""s00",ilYear,rgTlxInfo.TlxMonth,
                   prlTlxPtr->DValue,rgTlxInfo.TlxTime);
     }
     else
     {  Use actual time as Telex receive Time
        strcpy(rlResult.FValue,GetTimeStamp());
     }
  }
  else
  {  Build receive time from Telex time fields
     if (rgTlxInfo.TlxTime[0] != '\0' && rgTlxInfo.TlxDay[0] != '\0' &&
         rgTlxInfo.TlxMonth[0]!= '\0' && rgTlxInfo.TlxYear[0] != '\0')
     {
        ilYear=atoi(rgTlxInfo.TlxYear);
        if (ilYear > FDI_BORDER_YEAR)
           ilYear += 1900;
        else
           ilYear += 2000;
        sprintf(rlResult.FValue,"%d""%s""%s""%s00",ilYear,rgTlxInfo.TlxMonth,
                rgTlxInfo.TlxDay,rgTlxInfo.TlxTime);
     }
     else
     {  Use actual time as Telex receive Time
        strcpy(rlResult.FValue,GetTimeStamp());
     }
  }
  strcpy(rlResult.DName,"RcvTime");
  ilArr = CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
  dbg(DEBUG,"%s Tlx receive time <%s>",pclFunc,rlResult.FValue);
*/

  /* Compose entry for complete Telex in DB  */
  memset(&rlResult,0,TLXRESULT);

  if (igCurTlxUrnoIdx >= igMaxTlxUrnoIdx)
  {
     strcpy(rlResult.DName,"CreateDate");
     strcpy(rlResult.FValue,GetTimeStamp());
     ilArr = CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
  }

  switch (rgTlxInfo.TlxCmd)
  {
     case FDI_PAL:
     case FDI_CAL:
        break;
     default:
        /* Set Status in telegram*/
        memset(&rlResult,0,TLXRESULT);
        strcpy(rlResult.DName,"Status");
        sprintf(rlResult.FValue,"%c",cgTlxStatus);
        ilArr = CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
        break;
  }

  MakeFltKey();

  switch (rgTlxInfo.TlxCmd)
  {
     case FDI_MVT:
        SendMVTLOARes();
        break;
     case FDI_LDM:
        SendLDMLOARes();
        break;
     case FDI_CPM:
        SendCPMLOARes();
        break;
     case FDI_UCM:
        SendUCMLOARes();
        break;
     case FDI_DLS:
        SendDLSLOARes();
        break;
     case FDI_KRISCOM:
        SendKRISLOARes();
        break;
     case FDI_ALTEA:
        SendAlteaLoaRes();
	/*
            Would have preferred not to return in the middle of a function,
	    but SendCompleteTlx() unfortunately performs two unrelated actions,
	    viz, LOATAB insert, and TLXTAB update.
	*/
        if ((strstr (pcpData,"@P/IN") != '\0' &&
          (igAlteaAtpinSaveToTlxTab == FALSE)))
	{
	  dbg(TRACE,"%s Return without update of TLXTAB for ALTEA ATPIN",pclFunc);
	  return;
	}
	if ((igAlteaSaveToTlxTab == FALSE) &&
          (strstr (pcpData,"@P/IN") == '\0')) /* non ATPIN msg */
	{
	  dbg(TRACE,"%s Return without update of TLXTAB for ALTEA",pclFunc);
	  return;
	}
        break;
     case FDI_FFM:
        SendFFMLOARes();
        break;
     case FDI_PSM:
        SendPSMDPXRes();
        break;
     case FDI_PAL:
        SendPALDPXRes();
        memset(&rlResult,0,TLXRESULT);
        strcpy(rlResult.DName,"Status");
        sprintf(rlResult.FValue,"%c",cgTlxStatus);
        ilArr = CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
        break;
     case FDI_CAL:
        SendPALDPXRes();
        memset(&rlResult,0,TLXRESULT);
        strcpy(rlResult.DName,"Status");
        sprintf(rlResult.FValue,"%c",cgTlxStatus);
        ilArr = CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
        break;
     case FDI_PSC:
        /*SendPSCDPXRes();*/
        break;
     case FDI_DPFI:
     case FDI_APFI:
        SendDPILOARes();
        break;
  }

  /* Set complete Telex entries in FDI_MAX_TLX_TEXT char pieces */
  memset(&rlResult,0,TLXRESULT);

  strcpy(rlResult.DName,"TlxTxt1");

  ilTextSize = igTlxTextFieldSize;
  ilTextSize2 = igTlxTextFieldSize;
  if (rgTlxInfo.TlxCmd == FDI_SCORE && strlen(pcgScoreAppend) > 0)
  {
     sprintf(pclText,"%s\n",pcgScoreAppend);
     strncat(pclText,pcpData,rgTlxInfo.TlxEnd);
     pclText[rgTlxInfo.TlxEnd+strlen(pcgScoreAppend)+1] = '\0';
  }
  else
  {
     if (rgTlxInfo.TlxEnd <= igTlxTextFieldSize) /* Text < 4000, no special handling */
     {
        strncpy(pclText,pcpData,rgTlxInfo.TlxEnd);
        pclText[rgTlxInfo.TlxEnd] = '\0';
     }
     else
     {
        /*
	  If Text>4000, TXT1 of first telex row to save up to first LF
          before position 4000
        */
        strncpy(pclText,pcpData,igTlxTextFieldSize);
        pclText[igTlxTextFieldSize] = '\0';
        ilTextSize = igTlxTextFieldSize;
        while (ilTextSize > 0 && pclText[ilTextSize] != '\n')
           ilTextSize--;
        ilTextSize++;
        pclText[ilTextSize] = '\0';
     }
  }
  strcpy(rlResult.FValue,pclText);
  ChangeCharFromTo(rlResult.FValue,pcgClientChars,pcgServerChars);

  ilArr = CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);

  memset(&rlResult,0,TLXRESULT);

  if (rgTlxInfo.TlxEnd > igTlxTextFieldSize)
  {
    /*
      Balance text after the LF position of TXT1. Could be that length of TXT1
      was less than 4000 due to position of the last LF in TXT1.
    */

     strcpy(rlResult.DName,"TlxTxt2");
     if (rgTlxInfo.TlxEnd <= ilTextSize+igTlxTextFieldSize)
     {
        /*
	   Balance text is less than 4000. Save w/o special handling.
        */
        strncpy(pclText,&pcpData[ilTextSize],rgTlxInfo.TlxEnd-ilTextSize);
        pclText[rgTlxInfo.TlxEnd-ilTextSize] = '\0';
        ilTextSize2 = rgTlxInfo.TlxEnd - ilTextSize;
     }
     else
     {
        /*
           Balance text greater than 4000. Save up to the last LF before 4000.
        */
        strncpy(pclText,&pcpData[ilTextSize],igTlxTextFieldSize);
        pclText[igTlxTextFieldSize] = '\0';
        ilTextSize2 = igTlxTextFieldSize;
        while (ilTextSize2 > 0 && pclText[ilTextSize2] != '\n')
           ilTextSize2--;
        ilTextSize2++;
        pclText[ilTextSize2] = '\0';
     }
     strcpy(rlResult.FValue,pclText);
     ChangeCharFromTo(rlResult.FValue,pcgClientChars,pcgServerChars);
     /*      dbg(DEBUG,"%s Jetzt: <%s>",pclFunc,rlResult.FValue); */
     CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
  }

  /*  Split Telex  ? */
  /* ilTextSize and ilTextSize2 are actual saved lengths in TXT1, TXT2 */
  if (rgTlxInfo.TlxEnd > ilTextSize+ilTextSize2)
     {
     /*
        ilSplitC is the current position up to which the original text has
        been split up to.
     */
     ilSplitC = ilTextSize+ilTextSize2;

     dbg(TRACE,"%s Saved so far <%d> + <%d> of total <%d> chars",
       pclFunc,ilTextSize,ilTextSize2,strlen (pcpData));

     prlCfgArrElem = CCSArrayGetFirst(prgMasterCmpCfgArray);
     if (prlCfgArrElem != NULL)
     {
        if (prlCfgArrElem->MasterKey != NULL)
        {
       prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"TlxTxt1");
       if (prlCfgField != NULL)
           {
          ilTlxSplit = TRUE;
          ilPartNo = 1;
          strcpy(pclTlxTxt1,prlCfgField->FName);
          prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"TlxTxt2");
          if (prlCfgField != NULL)
              {
             strcpy(pclTlxTxt2,prlCfgField->FName);
          }
       }
       prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"PartNo");
       if (prlCfgField != NULL)
           {
          strcpy(pclPartNo,prlCfgField->FName);
       }
        }
     }
  } /* end if > igTlxTextFieldSize*2 */
  ilNoParts = (rgTlxInfo.TlxEnd / (igTlxTextFieldSize * 2)) + 1;
  sprintf(pclNoParts,"%d",ilNoParts);

  /* Set Telex PartNo */
  memset(&rlResult,0,TLXRESULT);
  strcpy(rlResult.DName,"PartNo");
  sprintf(rlResult.FValue,"%d",ilPartNo);
  CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);

  /* Set Unique telex key (not URNO)  */
  memset(&rlResult,0,TLXRESULT);
  strcpy(rlResult.DName,"TlxKey");
  strcpy(rlResult.FValue,pcgUnique);
  CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);

  /* Set SendReceive flag of telex  */
  memset(&rlResult,0,TLXRESULT);
  strcpy(rlResult.DName,"SendRec");
  if (ipHeaderTyp == 11)
     strcpy(rlResult.FValue,"I");
  else
     strcpy(rlResult.FValue,"R");
  CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);


  /* Set contributer of telex  */
  memset(&rlResult,0,TLXRESULT);
  strcpy(rlResult.DName,"Contrib");
  strcpy(rlResult.FValue,pcgDestName);
  CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);

  /* Set subtype of telex  */
  memset(&rlResult,0,TLXRESULT);
  strcpy(rlResult.DName,"SubType");
  strcpy(rlResult.FValue,pcgSubType);
  CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);

  /* Grep first complete Telex command*/
  prlCfgArrElem = CCSArrayGetFirst(prgMasterCmpCfgArray);

  while (prlCfgArrElem != NULL)
  {
     dbg(DEBUG,"%s ---- Start <%s> cmd----",pclFunc,prlCfgArrElem->CfgType);
     if (prlCfgArrElem->MasterKey == NULL)
     {
        dbg(DEBUG,"%s <%s> no values specified (see cfg file)",pclFunc,
            prlCfgArrElem->CfgType);
     }
     else
     {
        /* Get Pointer to first Cfg Field of command  */
        prlCfgField = CCSArrayGetFirst(prlCfgArrElem->MasterKey);
        while (prlCfgField != NULL)
        {
           if ((prlTlxPtr = CCSArrayGetData(prgMasterTlx,prlCfgField->DName)) != NULL )
           {
              dbg(DEBUG,"%s Found <%s> with <%s>",pclFunc,prlCfgField->DName,
                  prlCfgField->FName);
              strcat(pclFieldList,prlCfgField->FName);
              if (strcmp(prlCfgField->DName,"RcvTime") == 0)
              {
                 strcat(pclValueList,pcgFlightSchedTime);
              }
              else
              {
                 strcat(pclValueList,prlTlxPtr->FValue);
              }
              strcat(pclFieldList,",");
              strcat(pclValueList,",");
              if (rgTlxInfo.TlxCmd == FDI_SCORE && strcmp(prlCfgField->DName,"PartNo") == 0)
              {
                 strcat(pclFieldList,"BEME,");
                 strcat(pclValueList,"Part 1 of ");
                 strcat(pclValueList,pclNoParts);
                 strcat(pclValueList,",");
              }
           }
           else
           {
              /* dbg(DEBUG,"%s <%s> not found in result",pclFunc);  */
           }
           prlCfgField = CCSArrayGetNext(prlCfgArrElem->MasterKey);
        } /* end of while  */
     } /* end of if  */
     if ((ilCount=strlen(pclValueList)) != 0)
     {
        /* strcat(pclFieldList,"HOPO"); */
        /* strcat(pclValueList,pcgHomeAP); */
        pclFieldList[strlen(pclFieldList)-1] ='\0';
        pclValueList[ilCount-1] = '\0';
        prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Recv");
        if (prlCfgField != NULL)
        {
           ilReceiver = atoi(prlCfgField->FName);
        }
        else
        {
           dbg(TRACE,"%s Error no Receiver found",pclFunc);
           ilRC = RC_FAIL;
        }
        prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Command");
        if (prlCfgField != NULL)
        {
           strcpy(pclCommand,prlCfgField->FName);
        }
        else
        {
           dbg(TRACE,"%s Error no Command found",pclFunc);
           ilRC = RC_FAIL;
        }
        prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Table");
        if (prlCfgField != NULL)
        {
           strcpy(pclTable,prlCfgField->FName);
           strcat(pclTable,pcgTABEnd);
        }
        else
        {
           strcpy(pclTable," ");
        }
        if (strlen(pcpFlag))
        {
           sprintf(pclTwStart,"%s,%s",pcgUnique,pcpFlag);
           pclFlag = RC_SUCCESS;
        }
        else
        {
           sprintf(pclTwStart,"%s",pcgUnique);
           pclFlag = NETOUT_NO_ACK;
          /* pclFlag = RC_SUCCESS; */
        }

        if (ilRC == RC_SUCCESS)
        {
           if (strlen(pcgAftUrno) > 0)
           {
              strcat(pclFieldList,",FLNU");
              strcat(pclValueList,",");
              strcat(pclValueList,pcgAftUrno);
              if (strlen(pcgTlxMstx) < 2)
                 ilRC = GetMSTX();
           }
           strcat(pclFieldList,",MSTX");
           strcat(pclValueList,",");
           strcat(pclValueList,pcgTlxMstx);
           strcat(pclFieldList,",MSNO");
           strcat(pclValueList,",");
           strcat(pclValueList,pcgPTMPartNo);
/*
           ilRC = tools_send_info_flag(ilReceiver,0,pcgDestName,"",pcgRecvName,"","",
                                       pclTwStart,pcgTwEndNew,
                                       pclCommand,pclTable,pclSelection,
                                       pclFieldList,pclValueList,pclFlag);
*/
/*
           ilRC = SendCedaEvent(ilReceiver,0,pcgDestName,pcgRecvName,pclTwStart,pcgTwEndNew,
                                pclCommand,pclTable,pclSelection,
                                pclFieldList,pclValueList,"",4,pclFlag);
           dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
           dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
           dbg(DEBUG,"%s ValueList show max.100 <%.100s>",pclFunc,pclValueList);
           dbg(DEBUG,"%s Sent IBT. Returned <%d>",pclFunc, ilRC);
*/
           if (igCurTlxUrnoIdx < igMaxTlxUrnoIdx)
           {
              if (igBroadcastOldTelexes == TRUE)
                 ilRC = FdiHandleSql("URT",pclTable,&pcgTlxUrnoList[igCurTlxUrnoIdx][0],
                                     pclFieldList,pclValueList,
                                     pclTwStart,pcgTwEndNew,igScheduleAction,TRUE);
              else if (igTlxTooOld == TRUE)
                 ilRC = FdiHandleSql("URT",pclTable,&pcgTlxUrnoList[igCurTlxUrnoIdx][0],
                                     pclFieldList,pclValueList,
                                     pclTwStart,pcgTwEndNew,igScheduleAction,FALSE);
              else
                 ilRC = FdiHandleSql("URT",pclTable,&pcgTlxUrnoList[igCurTlxUrnoIdx][0],
                                     pclFieldList,pclValueList,
                                     pclTwStart,pcgTwEndNew,igScheduleAction,TRUE);
              igCurTlxUrnoIdx++;
           }
           else
           {
              if (strstr(pclFieldList,"CDAT") == NULL)
              {
                 strcat(pclFieldList,",CDAT");
                 strcat(pclValueList,",");
                 strcat(pclValueList,GetTimeStamp());
              }
              if (strstr(pclFieldList,"TIME") == NULL)
              {
                 strcat(pclFieldList,",TIME");
                 strcat(pclValueList,",");
                 strcat(pclValueList,GetTimeStamp());
              }
              if (igBroadcastOldTelexes == TRUE)
                 ilRC = FdiHandleSql(pclCommand,pclTable,"",pclFieldList,pclValueList,
                                     pclTwStart,pcgTwEndNew,igScheduleAction,TRUE);
              else if (igTlxTooOld == TRUE)
                 ilRC = FdiHandleSql(pclCommand,pclTable,"",pclFieldList,pclValueList,
                                     pclTwStart,pcgTwEndNew,igScheduleAction,FALSE);
              else
                 ilRC = FdiHandleSql(pclCommand,pclTable,"",pclFieldList,pclValueList,
                                     pclTwStart,pcgTwEndNew,igScheduleAction,TRUE);
           }
           if (ilTlxSplit == TRUE)
           {
              /*
		Get delimiter count of TXT1 and TXT2 in pclFieldList, which will
                be the same delimiter count in pclValueList.
              */
              strcpy(pcgNextTlxUrno,"");
              strcpy(pcgLastTlxUrno,"");
              ilPosT1 = FindItemInList(pclFieldList,pclTlxTxt1,',',&ilLine,&ilCol,&ilPos);
              if (ilPosT1 == RC_SUCCESS)
                 ilPosT1 = ilLine;
              else
                 ilPosT1 = -1;
              if (strlen(pclTlxTxt2) > 0)
              {
                 ilPosT2 = FindItemInList(pclFieldList,pclTlxTxt2,',',&ilLine,&ilCol,&ilPos);
                 if (ilPosT2 == RC_SUCCESS)
                 {
                    ilPosT2 = ilLine;
                 }
                 else
                 {
                    ilPosT2 = -1;
                 }
              }
              else
              {
                 ilPosT2 = -1;
              }
              ilPosP = FindItemInList(pclFieldList,pclPartNo,',',&ilLine,&ilCol,&ilPos);
              if (ilPosP == RC_SUCCESS)
                 ilPosP = ilLine;
              else
                 ilPosP = -1;
           }

           /*
             If there is balance text to save beyond first TLXTAB row's TXT1 and TXT2,
             update pclValueList with the TXT1 and possibly TXT2 and BEME,MORE with the balance.
             v.1.128 - Save up to furthest LF only in each cycle of TXT1,TXT2.
           */
           while (ilTlxSplit == TRUE)
           {
              if (ilPosT1 > 0)
              {
                 ilPartNo++;
                 /* Change PartNo in ValueList*/
                 if (ilPosP > 0)
                 {
                    ilRC = CountToDelim(pclValueList,ilPosP,',');
                    strcpy(pclSaveList,&pclValueList[ilRC-1]);
                    ilRC = CountToDelim(pclValueList,ilPosP-1,',');
                    sprintf(&pclValueList[ilRC],"%d",ilPartNo);
                    strcat(pclValueList,pclSaveList);
                    if (rgTlxInfo.TlxCmd == FDI_SCORE)
                    {
                       ilPosB = FindItemInList(pclFieldList,"BEME",',',&ilLine,&ilCol,&ilPos);
                       if (ilPosB == RC_SUCCESS)
                          ilPosB = ilLine;
                       else
                          ilPosB = -1;
                       if (ilPosB > 0)
                       {
                          ilRC = CountToDelim(pclValueList,ilPosB,',');
                          strcpy(pclSaveList,&pclValueList[ilRC-1]);
                          ilRC = CountToDelim(pclValueList,ilPosB-1,',');
                          sprintf(&pclValueList[ilRC],"Part %d of %s",ilPartNo,pclNoParts);
                          strcat(pclValueList,pclSaveList);
                       }
                    }
                 }
                 ilRC = CountToDelim(pclValueList,ilPosT1,',');
                 strcpy(pclSaveList,&pclValueList[ilRC-1]); /* temp save everything after last TXT1 */
                 ilRC = CountToDelim(pclValueList,ilPosT1-1,','); /* find start of TXT1 in pclValueList */
                 if (ilSplitC+igTlxTextFieldSize > rgTlxInfo.TlxEnd )
                 {
                    /*
                       Balance text to save is less than 4000 char, so only TXT1
                       needed. Update TXT1 and remove TXT2. No need to look for LF.
                    */
                    strncpy(&pclValueList[ilRC],&pcpData[ilSplitC],
                            rgTlxInfo.TlxEnd - ilSplitC);
                    pclValueList[ilRC+rgTlxInfo.TlxEnd - ilSplitC] = 0x00;
                    ChangeCharFromTo(&pclValueList[ilRC],pcgClientChars,pcgServerChars);
                    strcat(pclValueList,pclSaveList);
                    ilTlxSplit = FALSE; /* Flag to break out of while() */
		    dbg(TRACE,"%s Saved last <%d> chars to TXT1",pclFunc,(rgTlxInfo.TlxEnd - ilSplitC));
                    /* Remove TlxTxt2 */
                    if (ilPosT2 > 0) /* Last save to TLXTAB included TXT2 */
                    {
                       /* First remove entry from Fieldlist*/
                       ilRC = CountToDelim(pclFieldList,ilPosT2,',');
                       if (strlen(pclFieldList) == ilRC)
                       {
                          strcpy(pclSaveList,&pclFieldList[ilRC]);
                       }
                       else
                       {
                          strcpy(pclSaveList,&pclFieldList[ilRC-1]);
                       }
                       ilRC = CountToDelim(pclFieldList,ilPosT2-1,',');
                       pclFieldList[ilRC-1] = 0x00;
                       strcat(pclFieldList,pclSaveList);
                       /* Than remove entry from Valuelist*/
                       ilRC = CountToDelim(pclValueList,ilPosT2,',');
                       if (strlen(pclFieldList) == ilRC)
                       {
                          strcpy(pclSaveList,&pclValueList[ilRC]);
                       }
                       else
                       {
                          strcpy(pclSaveList,&pclValueList[ilRC-1]);
                       }
                       ilRC = CountToDelim(pclValueList,ilPosT2-1,',');
                       pclValueList[ilRC-1] = 0x00;
                       strcat(pclValueList,pclSaveList);
                    } /* end of if ilRC2 > 0*/
                 }
                 else
                 {
                    /*
                       More than 4000 text still to be saved. v.1.128 - save in TXT1
                       only up to furthest LF within 4000.
		       First, save into temp space pclText up the last LF before 4000.
                    */
                    strncpy(pclText,&pcpData[ilSplitC],igTlxTextFieldSize);
                    pclText[igTlxTextFieldSize] = '\0';
                    ilTextSize = igTlxTextFieldSize;
                    while (ilTextSize > 0 && pclText[ilTextSize] != '\n')
                      ilTextSize--;
                    ilTextSize++;
                    pclText[ilTextSize] = '\0';

		    /* Then append to pclValueList from where last TXT1 was */
                    /* Original code was:
                       strncpy(&pclValueList[ilRC],&pcpData[ilSplitC],igTlxTextFieldSize);
                       pclValueList[ilRC+igTlxTextFieldSize] = 0x00;
                    */
                    strncpy(&pclValueList[ilRC],pclText,ilTextSize);
                    pclValueList[ilRC+ilTextSize] = 0x00;
                    ChangeCharFromTo(&pclValueList[ilRC],pcgClientChars,pcgServerChars);
                    strcat(pclValueList,pclSaveList);
                    /* ilSplitC += igTlxTextFieldSize; -- original code */
                    ilSplitC += ilTextSize;
                    dbg(TRACE,"%s Saved next <%d> chars to TXT1",pclFunc,ilTextSize);
                    /*
                      Last save to TLXTAB had TXT2. Logic appears to be that if
		      there were no TXT2, then we would not be here since no further TXT1
		      would be required after the last save to TLXTAB.
		    */
                    if (ilPosT2 > 0)
                    {
                       ilRC = CountToDelim(pclValueList,ilPosT2,',');
		       /*
                           Think it should be check length of pclValueList instead?
                        */
                       if (strlen(pclFieldList) == ilRC)  /* original code */
                       {
                          /* end of valuelist reached*/
                          strcpy(pclSaveList,&pclValueList[ilRC]);
                       }
                       else
                       {
			 /*
                            Save a copy of everything after last TXT2 in pclValueList
                         */
                          strcpy(pclSaveList,&pclValueList[ilRC-1]);
                       }
                       ilRC = CountToDelim(pclValueList,ilPosT2-1,',');
                       if (ilSplitC+igTlxTextFieldSize > rgTlxInfo.TlxEnd )
                       {
                          /*
                             Balance is less than 4000, so save w/o special handling.
                          */
                          strncpy(&pclValueList[ilRC],&pcpData[ilSplitC],
                                  rgTlxInfo.TlxEnd - ilSplitC);
                          pclValueList[ilRC+rgTlxInfo.TlxEnd - ilSplitC] = 0x00;
                          ChangeCharFromTo(&pclValueList[ilRC],pcgClientChars,pcgServerChars);
                          strcat(pclValueList,pclSaveList);
                          ilTlxSplit = FALSE;   /* flag to end while () */
                          dbg(TRACE,"%s Saved final <%d> chars to TXT2",pclFunc, (rgTlxInfo.TlxEnd - ilSplitC));
                       }
                       else
                       {
                         /*
                           More than 4000 text still to be saved. v.1.128 - save in TXT2
                           only up to furthest LF within 4000.
                         */
                          /* -- original code --
                          strncpy(&pclValueList[ilRC],&pcpData[ilSplitC],igTlxTextFieldSize);
                          pclValueList[ilRC+igTlxTextFieldSize] = 0x00;
			  */

			  /* Save in temp space pclText up to last LF before next 4000 */
                          strncpy(pclText,&pcpData[ilSplitC],igTlxTextFieldSize);
                          pclText[igTlxTextFieldSize] = '\0';
                          ilTextSize2 = igTlxTextFieldSize;
                          while (ilTextSize2 > 0 && pclText[ilTextSize2] != '\n')
                            ilTextSize2--;
                          ilTextSize2++;
                          pclText[ilTextSize2] = '\0';

                          /* Then append to pclValueList where TXT2 used to be */
                          strncpy(&pclValueList[ilRC],pclText,ilTextSize2);
                          pclValueList[ilRC+ilTextSize2] = 0x00;
                          ChangeCharFromTo(&pclValueList[ilRC],pcgClientChars,pcgServerChars);
                          strcat(pclValueList,pclSaveList);
                          ilSplitC += ilTextSize2;
                          dbg(TRACE,"%s Saved next <%d> chars to TXT2",pclFunc, ilTextSize2);

                       } /* end of else */
                    } /* end of if ilRC ==RC_SUCCESS */
                 } /* end of else*/
/*
                 ilRC = tools_send_info_flag(ilReceiver,0,pcgDestName,"",pcgRecvName,"","",
                                             pcgUnique,pcgTwEndNew,
                                             pclCommand,pclTable,pclSelection,
                                             pclFieldList,pclValueList,RC_SUCCESS);
*/
/*
                 ilRC = SendCedaEvent(ilReceiver,0,pcgDestName,pcgRecvName,
                                      pcgUnique,pcgTwEndNew,
                                      pclCommand,pclTable,pclSelection,
                                      pclFieldList,pclValueList,"",4,RC_SUCCESS);
                 dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
                 dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
                 dbg(DEBUG,"%s ValueList show max.200 <%.200s>",pclFunc,pclValueList);
                 dbg(DEBUG,"%s Sent IBT. Returned <%d>",pclFunc, ilRC);
*/
                 if (strstr(pclFieldList,"CDAT") == NULL)
                 {
                    strcat(pclFieldList,",CDAT");
                    strcat(pclValueList,",");
                    strcat(pclValueList,GetTimeStamp());
                 }
                 if (strstr(pclFieldList,"TIME") == NULL)
                 {
                    strcat(pclFieldList,",TIME");
                    strcat(pclValueList,",");
                    strcat(pclValueList,GetTimeStamp());
                 }
                 if (igBroadcastOldTelexes == TRUE)
                    ilRC = FdiHandleSql(pclCommand,pclTable,"",pclFieldList,pclValueList,
                                        pcgUnique,pcgTwEndNew,igScheduleAction,TRUE);
                 else if (igTlxTooOld == TRUE)
                    ilRC = FdiHandleSql(pclCommand,pclTable,"",pclFieldList,pclValueList,
                                        pcgUnique,pcgTwEndNew,igScheduleAction,FALSE);
                 else
                    ilRC = FdiHandleSql(pclCommand,pclTable,"",pclFieldList,pclValueList,
                                        pcgUnique,pcgTwEndNew,igScheduleAction,TRUE);
              } /* end of if PosT > 0*/
              else
              {
                 dbg(TRACE,"%s Error no TlxTxt field found",pclFunc);
                 ilTlxSplit = FALSE;
              }
           } /* end of while */
        } /* end of if*/
     }
     else
     {
        dbg(TRACE,"%s Error no values specified to send",pclFunc);
     }

/*AKL*/
     prlTlxPtr = CCSArrayGetData(prgMasterTlx,"TlxTyp");
     if (prlTlxPtr != NULL && strlen(pcgAftUrno) > 0)
     {
        ilRC = iGetConfigEntry(pcgCfgFile,prlTlxPtr->FValue,"SendCmd",CFG_STRING,pclSendCommand);
        if (ilRC == RC_SUCCESS)
        {
           ilRC = iGetConfigEntry(pcgCfgFile,prlTlxPtr->FValue,"SendModId",CFG_STRING,pclSendModId);
           if (ilRC == RC_SUCCESS)
           {
              ilSendModId = atoi(pclSendModId);
              ilRC = iGetConfigEntry(pcgCfgFile,prlTlxPtr->FValue,"SendPrio",CFG_STRING,pclSendPrio);
              if (ilRC == RC_SUCCESS)
                 ilSendPrio = atoi(pclSendPrio);
              else
                 ilSendPrio = 3;
              ilRC = iGetConfigEntry(pcgCfgFile,prlTlxPtr->FValue,"SendData",CFG_STRING,pclSendData);
              if (ilRC != RC_SUCCESS)
                 strcpy(pclSendData,"NO");
              if (strlen(pcgNextTlxUrno) > 0)
                 sprintf(pclSendSelection,"AFTURNO=%s,TLXURNO=%s",pcgAftUrno,pcgNextTlxUrno);
              else
                 sprintf(pclSendSelection,"AFTURNO=%s,TLXURNO=%s",pcgAftUrno,pcgLastTlxUrno);
              if (strcmp(pclSendData,"YES") == 0 && pcgDataPtr != NULL)
                 ilRC = SendCedaEvent(ilSendModId,0,pcgAftDestName,pcgAftRecvName,
                                      pcgAftTwStart,pcgAftTwEnd,
                                      pclSendCommand,"",pclSendSelection,
                                      prlTlxPtr->FValue,pcgDataPtr,"",ilSendPrio,NETOUT_NO_ACK);
              else
                 ilRC = SendCedaEvent(ilSendModId,0,pcgAftDestName,pcgAftRecvName,
                                      pcgAftTwStart,pcgAftTwEnd,
                                      pclSendCommand,"",pclSendSelection,
                                      prlTlxPtr->FValue,"","",ilSendPrio,NETOUT_NO_ACK);
                 dbg(DEBUG,"%s Sent <%s> to <%d>. Returned <%d>",
                     pclFunc,pclSendCommand,ilSendModId,ilRC);
                 dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSendSelection);
                 dbg(DEBUG,"%s FieldList <%s>",pclFunc,prlTlxPtr->FValue);
                 if (strcmp(pclSendData,"YES") == 0 && pcgDataPtr != NULL)
                    dbg(DEBUG,"%s ValueList <%s>",pclFunc,pcgDataPtr);
                 else
                    dbg(DEBUG,"%s ValueList <>",pclFunc);
           }
        }
     }

     dbg(DEBUG,"%s ---- End <%s> cmd----",pclFunc,prlCfgArrElem->CfgType);

     prlCfgArrElem = CCSArrayGetNext(prgMasterCmpCfgArray);
  } /* end of while  */

  return ilRC;
} /* end of SaveCompleteTlx */

#if 0
static int ClearArray(CCS_KEY *prpKey, char cpInput)
{
  char pclFunc[]="ClearArray";
  T_TLXRESULT *prlTlxResult;
  T_TLXHEAD   *prlTlxHead;

  switch(cpInput)
    {
    case 'R':
      prlTlxResult = CCSArrayGetFirst(prpKey);
      while( prlTlxResult != NULL)
    {
      prlTlxResult->Flag = 0;
      prlTlxResult->DValue[0] = '\0';
      prlTlxResult->FValue[0] = '\0';
      prlTlxResult = CCSArrayGetNext(prpKey);
    }
      break;
    case 'H':
      prlTlxHead = CCSArrayGetFirst(prpKey);
      while( prlTlxHead != NULL)
    {
      prlTlxHead->Flag = 0;
      prlTlxHead->DValue[0] = '\0';
      prlTlxHead->FValue[0] = '\0';
      prlTlxHead = CCSArrayGetNext(prpKey);
    }
      break;
    default:
      dbg(TRACE,"%s Error unknown option <%c>",pclFunc,cpInput);

    }

  return RC_SUCCESS;
} /* end of ClearArray */
#endif

#if 0
static int MakeArrEntry( char *pcpInput,char *pcpFlag)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="MakeArrEntry:";
  T_TLX_ARRAY_ELEMENT rlTlxArrayElement;
  T_TLXRESULT *prlResult;

  memset(&rlTlxArrayElement,0,TLX_ARRAY_ELEMENT);

  strcpy(rlTlxArrayElement.Flag,pcpFlag);
  strcpy(rlTlxArrayElement.TlxType,pcpInput);
  rlTlxArrayElement.MasterKey = prgMasterTlx;
  strcpy(rlTlxArrayElement.Unique,pcgUnique);

  CCSArrayAddUnsort(prgMasterTlxArray,&rlTlxArrayElement,TLX_ARRAY_ELEMENT);


  return ilRC;
} /* end of  MakeArrEntry  */
#endif






static int SendTlxRes(int ipTlxCmd)
{
  int ilRC =RC_SUCCESS;
  int ilCount=0;
  int ilExit = RC_SUCCESS;
  int ilReceiver;
  static FILE *pflTlxRes=NULL;
  char pclTable[10];
  char pclCommand[10];
  char pclFunc[]="SendTlxRes:";
  char pclFieldList[2000]="";
  char pclSelection[100];
  char pclDummy[10];
  char pclUnique[30];
  char pclResult[32];
  char pclValueList[COMPLETE_TELEX_LEN]="";
  T_FIELDLIST *prlCfgField=NULL;
  T_TLXRESULT *prlTlxPtr=NULL;
  T_TLXRESULT prlTlxResult;
  T_TLXRESULT prlResult;
  T_CFG_ARRAY_ELEMENT *prlCfgArrElem=NULL;
  T_TLX_ARRAY_ELEMENT *prlTlxArrayElement=NULL;
  char pclUrno[16];
  char pclNewFieldList[2000]="";
  char pclNewValueList[COMPLETE_TELEX_LEN]="";
  int ilDebugLevel;
  int ilYear = 0;

  /* Create FDay entry*/
  memset(&prlTlxResult,0x00,TLXRESULT);
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"FDate");
  if (prlTlxPtr != NULL)
  {
     Time2Date(prlTlxResult.DValue,"0000");
     sprintf(prlTlxResult.FValue,"%8.8s",prlTlxResult.DValue);
     sprintf(prlTlxResult.DName,"FDay");
     CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
  }
  memset(&prlTlxResult,0x00,TLXRESULT);
  sprintf(pclDummy,"%s%s",rgTlxInfo.TlxDay,rgTlxInfo.TlxTime);
  if (Time2Date(prlTlxResult.DValue,pclDummy) == RC_FAIL)
     return RC_FAIL;
  sprintf(prlTlxResult.FValue,"%14.14s",prlTlxResult.DValue);
  sprintf(prlTlxResult.DName,"TDay");
  CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);

/****************************************/
  memset(&prlTlxResult,0x00,TLXRESULT);
  if (igHeaderTyp == 1)
  {
     prlTlxPtr = CCSArrayGetData(prgMasterTlx,"FDate");
     /* Build receive time from Telex time fields*/
     if (rgTlxInfo.TlxMonth[0] != '\0' && rgTlxInfo.TlxYear[0] != '\0' &&
         rgTlxInfo.TlxTime[0] != '\0' && prlTlxPtr != NULL)
     {
        ilYear = atoi(rgTlxInfo.TlxYear);
        if (ilYear > FDI_BORDER_YEAR)
           ilYear += 1900;
        else
           ilYear += 2000;
           sprintf(prlResult.FValue,"%""d%""s%""s%""s00",ilYear,rgTlxInfo.TlxMonth,
                   prlTlxPtr->DValue,rgTlxInfo.TlxTime);
     }
     else
     { /* Use actual time as Telex receive Time */
        strcpy(prlResult.FValue,GetTimeStamp());
     }
  }
  else
  { /* Build receive time from Telex time fields*/
     if (rgTlxInfo.TlxTime[0] != '\0' && rgTlxInfo.TlxDay[0] != '\0' &&
         rgTlxInfo.TlxMonth[0]!= '\0' && rgTlxInfo.TlxYear[0] != '\0')
     {
        ilYear=atoi(rgTlxInfo.TlxYear);
        if (ilYear > FDI_BORDER_YEAR)
           ilYear += 1900;
        else
           ilYear += 2000;
        sprintf(prlResult.FValue,"%d""%s""%s""%s00",ilYear,rgTlxInfo.TlxMonth,
                rgTlxInfo.TlxDay,rgTlxInfo.TlxTime);
     }
     else
     { /* Use actual time as Telex receive Time */
        strcpy(prlResult.FValue,GetTimeStamp());
     }
  }
  strcpy(prlResult.DName,"RcvTime");
  ilRC = CCSArrayAddUnsort(prgMasterTlx,&prlResult,TLXRESULT);
  if (ipTlxCmd != FDI_MAN)
     strcpy(pcgFlightSchedTime,prlResult.FValue);
  dbg(DEBUG,"%s Tlx receive time <%s>",pclFunc,prlResult.FValue);
/****************************************/

  strcpy(pclSelection,"FLIGHT,TELEX,");
  switch (ipTlxCmd)
  {
     case FDI_MVT:
        /*  Check e.g. if ADOffBlock < ADAir < EATime */
        CheckTimeLine();
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"MVT");
        strcat(pclSelection,"MVT");
        break;
     case FDI_LDM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"LDM");
        strcat(pclSelection,"LDM");
        break;
     case FDI_CPM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"CPM");
        strcat(pclSelection,"CPM");
        break;
     case FDI_PTM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"PTM");
        strcat(pclSelection,"PTM");
        break;
     case FDI_SRM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"SRM");
        strcat(pclSelection,"SRM");
        break;
     case FDI_SAM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"SAM");
        strcat(pclSelection,"SAM");
        break;
     case FDI_SLC:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"SLC");
        strcat(pclSelection,"SLC");
        break;
     case FDI_RQM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"RQM");
        strcat(pclSelection,"RQM");
        break;
     case FDI_DIV:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"DIV");
        strcat(pclSelection,"DIV");
        break;
     case FDI_TPM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"TPM");
        strcat(pclSelection,"TPM");
        break;
     case FDI_PSM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"PSM");
        strcat(pclSelection,"PSM");
        break;
     case FDI_ETM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"ETM");
        strcat(pclSelection,"ETM");
        break;
     case FDI_FWD:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"FWD");
        strcat(pclSelection,"FWD");
        break;
     case FDI_SCR:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"SCR");
        strcat(pclSelection,"SCR");
        break;
     case FDI_BTM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"BTM");
        strcat(pclSelection,"BTM");
        break;
     case FDI_UCM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"UCM");
        strcat(pclSelection,"UCM");
        break;
     case FDI_DLS:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"DLS");
        strcat(pclSelection,"DLS");
        break;
     case FDI_AFTN:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"AFTN");
        strcat(pclSelection,"AFTN");
        if (strlen(pcgAftnSchedTime) > 0)
           strcpy(pcgFlightSchedTime,pcgAftnSchedTime);
        break;
     case FDI_KRISCOM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"KRISCOM");
        strcat(pclSelection,"KRISCOM");
        if (strlen(pcgKrisSTOD) > 0)
        {
           strcpy(pcgFlightSchedTime,pcgKrisSTOD);
        }
        break;
     case FDI_ALTEA:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"ALTEA");
        strcat(pclSelection,"ALTEA");
        if (strlen(pcgAlteaSTOD) > 0)
        {
           strcpy(pcgFlightSchedTime,pcgAlteaSTOD);
        }
        break;

     case FDI_ADF:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"ADF");
        strcat(pclSelection,"ADF");
        break;
     case FDI_ASDI:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"ASDI");
        strcat(pclSelection,"ASDI");
        break;
     case FDI_MAN:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"MAN");
        strcat(pclSelection,"MAN");
        break;
     case FDI_SCORE:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"SCORE");
        strcat(pclSelection,"SCORE");
        break;
     case FDI_FFM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"FFM");
        strcat(pclSelection,"FFM");
        break;
     case FDI_DFS:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"DFS");
        strcat(pclSelection,"DFS");
        break;
     case FDI_UBM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"UBM");
        strcat(pclSelection,"UBM");
        break;
     case FDI_CCM:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"CCM");
        strcat(pclSelection,"CCM");
        break;
     case FDI_BAY:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"BAY");
        strcat(pclSelection,"BAY");
        break;
     case FDI_PAL:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"PAL");
        strcat(pclSelection,"PAL");
        break;
     case FDI_CAL:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"CAL");
        strcat(pclSelection,"CAL");
        break;
     case FDI_PSC:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"PSC");
        strcat(pclSelection,"PSC");
        break; 
     case FDI_DPFI:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"DPFI");
        strcat(pclSelection,"DPFI");
        break;
     case FDI_APFI:
        prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"APFI");
        strcat(pclSelection,"APFI");
        break;
     default :
        dbg(DEBUG,"%s No Command provided",pclFunc);
        break;
  }

  if (prlCfgArrElem != NULL)
  {
     dbg(DEBUG,"%s ---- Start <%s> cmd----",pclFunc,prlCfgArrElem->CfgType);
     if (prlCfgArrElem->MasterKey == NULL)
     {
        dbg(DEBUG,"%s <%s> no values specified (see cfg file)",
            pclFunc,prlCfgArrElem->CfgType);
     }
     else
     {
        /* Get Pointer to first Cfg Field of command  */
        dbg(TRACE,"COLLECT VALUES FOR TKEY <%s>", pcgUnique);
        prlCfgField = CCSArrayGetFirst(prlCfgArrElem->MasterKey);
        while (prlCfgField != NULL)
        {
           if ((prlTlxPtr = CCSArrayGetData(prgMasterTlx,prlCfgField->DName)) != NULL)
           {
              dbg(DEBUG,"%s Found <%s> with <%s>",pclFunc,prlCfgField->DName,
                  prlCfgField->FName);
              if (strstr(prlCfgField->FName,"APX.") == NULL &&
                  strstr(prlCfgField->FName,"LOA.") == NULL)
              {
                 strcat(pclFieldList,prlCfgField->FName);
                 strcat(pclValueList,prlTlxPtr->FValue);
                 strcat(pclFieldList,",");
                 strcat(pclValueList,",");
              } /* end if */
              else
              {
                 if (strstr(prlCfgField->FName,"APX.") != NULL)
                 {
                    StoreApxValue(pcgUnique, prlCfgField->FName, prlTlxPtr->FValue);
                 }
              } /* end else */
           }
           else
           {
              /* dbg(DEBUG,"%s <%s> not found in result",pclFunc,prlCfgField->DName); */
           }
           prlCfgField = CCSArrayGetNext(prlCfgArrElem->MasterKey);
        } /* end of while  */
     }/* end of if  */
     if ((ilCount=strlen(pclValueList)) != 0)
     {
        /* strcat(pclFieldList,"HOPO"); */
        /* strcat(pclValueList,pcgHomeAP); */
        pclFieldList[strlen(pclFieldList)-1] ='\0';
        pclValueList[ilCount-1] = '\0';
        prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Recv");
        if (prlCfgField != NULL)
           ilReceiver = atoi(prlCfgField->FName);
        else
           ilExit = RC_FAIL;

        prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Command");
        if (prlCfgField != NULL)
           strcpy(pclCommand,prlCfgField->FName);
        else
           ilExit = RC_FAIL;

        prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Table");
        if (prlCfgField != NULL)
        {
           strcpy(pclTable,prlCfgField->FName);
           strcat(pclTable,pcgTABEnd);
        }
        else
           strcpy(pclTable," ");

        if (igScanPTM == TRUE)
        {
           GetDataItem(pclUnique,pcgTwStart,3,',',"","  ");
        }
        else
        {
           strcpy(pclUnique,pcgUnique);
        }

        if (ipTlxCmd == FDI_MVT)  /* v.1.143 - set igTrustTlxDate for MVT */
          (void) MVT_SetTrustTlxDate (); 
        else if (ipTlxCmd == FDI_LDM)  /* v.1.146 */
          (void) LDM_SetTrustTlxDate (); 

        if (ilExit != RC_FAIL)
        {
/*
           ilRC = tools_send_info_flag(ilReceiver,0,pcgDestName,"",pcgRecvName,"",
                                       "",pclUnique,pcgTwEndNew,
                                       pclCommand,pclTable,pclSelection,
                                          pclFieldList,pclValueList,RC_SUCCESS);
*/
/*
           ilRC = SendCedaEvent(ilReceiver,0,pcgDestName,pcgRecvName,pclUnique,pcgTwEndNew,
                                pclCommand,pclTable,pclSelection,
                                pclFieldList,pclValueList,"",4,RC_SUCCESS);
*/
           if (ipTlxCmd == FDI_AFTN || ipTlxCmd == FDI_KRISCOM || ipTlxCmd == FDI_ADF ||
               ipTlxCmd == FDI_ASDI || ipTlxCmd == FDI_MAN || ipTlxCmd == FDI_SCORE ||
               ipTlxCmd == FDI_DFS || ipTlxCmd == FDI_ALTEA)
           {
              strcpy(pclUrno,pcgAftUrno);
              strcpy(pclNewFieldList,pcgAftFieldList);
              strcpy(pclNewValueList,pcgAftValueList);
           }
           else
           {
              ilRC = SearchFlight(pclFieldList,pclValueList,pclUrno,pclNewFieldList,
                                  pclNewValueList,ipTlxCmd);
           }
           if (igCurTlxUrnoIdx < igMaxTlxUrnoIdx)
           {
              strcpy(pcgNextTlxUrno,&pcgTlxUrnoList[igCurTlxUrnoIdx][0]);
           }
           else
           {
              ilDebugLevel = debug_level;
              debug_level = 0;
#ifdef DB_LOAD_URNO_LIST
              /* UFIS-1485 */
              DB_LoadUrnoList(1,pcgNextTlxUrno,pcgTableKey);
#else
              GetNextValues(pcgNextTlxUrno,1);
#endif
              debug_level = ilDebugLevel;
           }
           strcpy(pclCommand,"UFR");
           sprintf(pclSelection,"WHERE URNO = %s",pclUrno);
           if (strlen(pclUrno) > 0)
           {
              strcpy(pcgAftUrno,pclUrno);
              igAftReceiver = ilReceiver;
              if (ipTlxCmd != FDI_ADF && ipTlxCmd != FDI_ASDI)
                 strcpy(pcgAftDestName,pcgDestName);
              if (ipTlxCmd != FDI_ADF && ipTlxCmd != FDI_ASDI)
                 strcpy(pcgAftRecvName,pcgRecvName);
              strcpy(pcgAftTwStart,pclUnique);
              strcpy(pcgAftTwEnd,pcgTwEndNew);
              strcpy(pcgAftCommand,pclCommand);
              strcpy(pcgAftTable,pclTable);
              strcpy(pcgAftSelection,pclSelection);
              strcpy(pcgAftFieldList,pclNewFieldList);
              strcpy(pcgAftValueList,pclNewValueList);
              DeployApxValue(pclUnique,pclUrno);
              switch (ipTlxCmd)
              {
                 case FDI_DIV:
                 case FDI_RQM:
                 case FDI_TPM:
                 case FDI_PSM:
                 case FDI_ETM:
                 case FDI_FWD:
                 case FDI_SCR:
                 case FDI_BTM:
                 case FDI_UBM:
                 case FDI_CCM:
                 case FDI_BAY:
                    cgTlxStatus = FDI_STAT_ASSIGNED;
                    break;
              }
           }
           else
           {
              strcpy(pcgAftUrno,"");
              igAftOrLoaChanged = 0;
              DeployApxValue(pclUnique,"0");
           }

           memset(&prlTlxResult,0x00,TLXRESULT);
           sprintf(prlTlxResult.FValue,"%s/%s",pclNewFieldList,pclNewValueList);
           MakeTokenList(prlTlxResult.DValue,prlTlxResult.FValue,",#",'#');
           strcpy(prlTlxResult.FValue,prlTlxResult.DValue);
           sprintf(prlTlxResult.DName,"saveflist");
           CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);

           /* Trace Telex Results */
           if (igTraceTlxRes == TRUE)
           {
              if (pflTlxRes == NULL)
              {
                 sprintf(pclResult,"%s/tlxres%d.trc",getenv("DBG_PATH"),getpid());
                 pflTlxRes = fopen(pclResult,"a");
              }/* end of if*/
              fprintf(pflTlxRes,"Receiver  <%d>\n",ilReceiver);
              fprintf(pflTlxRes,"Selection <%s>\n",pclSelection);
              fprintf(pflTlxRes,"FieldList <%s>\n",pclFieldList);
              fprintf(pflTlxRes,"ValueList <%s>\n",pclValueList);
              fflush(pflTlxRes);
           }/* end of if*/
        }/* end of if*/
     }
     else
     {
        dbg(TRACE,"%s Error no values specified to send",pclFunc);
     }
     dbg(DEBUG,"%s ---- End <%s> cmd----",pclFunc,prlCfgArrElem->CfgType);
  } /* end of if */
  else
  {
     dbg(TRACE,"%s No command configured",pclFunc);
     /* PrintCfgArrayStructure(); */
  }

  return ilRC;

} /* end of SendTlxRes*/


static int SearchInfoCmds(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilLine,ilCol,ilPos;
  int ilMarkPos,ilMarkCol,ilMarkLine;
  int ilRC2,ilCount;
  int i,k,j,ilHit=0;
  char pclResult[100];
  char pclLine[100];
  char pclFunc[]="SearchInfoCmds:";
  int ilSaveSIPos=0;
  char clSaveChar;
  /* Test for SI Information
     and move Textendposition to SI pos*/

  ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),"\nSI"
              ,'\n',&ilLine,&ilCol,&ilPos);
  if(ilRC==RC_SUCCESS)
    {
      dbg(DEBUG,"%s Found Keyword <SI>",pclFunc);

      ilRC2=HandleSI(&(pcpData[rgTlxInfo.TxtStart+ilPos]));

      ilSaveSIPos = rgTlxInfo.TxtStart+ilPos;
      clSaveChar = pcpData[ilSaveSIPos];
      pcpData[ilSaveSIPos] = '\0';
    }
  else
    {
      ilRC=RC_SUCCESS;
    }

  dbg(DEBUG,"%s Show Info Body max.200 <%s>",pclFunc,&(pcpData[rgTlxInfo.TxtStart]));

  /* Search for Telegram Keywords like AA  */
  if(ilRC==RC_SUCCESS)
    {
      ilRC=RC_FAIL;
      /* Find Primary Keyword like AA*/
      for(i=0; (prgInfoCmd[i][0].Function != NULL) && (ilRC == RC_FAIL);i++)
    {
      /* Look for Synonyms  */
      ilCount=GetNoOfElements(prgInfoCmd[i][0].TlxInfo,',');
      for(k=1; k <= ilCount && ilRC == RC_FAIL ; k++)
        {
          GetDataItem(pclResult, prgInfoCmd[i][0].TlxInfo,k,',',"","\0\0");
          ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),pclResult
                  ,'\n',&ilLine,&ilCol,&ilPos);
        }
      ilHit=i;
    }/* end for i  */

      if (ilRC == RC_SUCCESS)
      {
         GetDataItem(pclLine,&(pcpData[rgTlxInfo.TxtStart+ilPos]),1,'\n',"","  ");
         if (strcmp(pclResult,"\nDL") != 0 && strcmp(pclResult,"\nDLA") != 0 && strcmp(pclResult,"\nEDL") != 0 )
         {
            if (pclLine[strlen(pclResult)-1] == ' ' || isdigit(pclLine[strlen(pclResult)-1]))
               ilRC = RC_SUCCESS;
            else
               ilRC = RC_FAIL;
         }
      }

      if(ilRC==RC_SUCCESS)
    { /* Handle primary keyword  */

      /* Only for debugging info start */
      if(pclResult[0] == '\n')
        pclResult[0]=' ';
      dbg(DEBUG,"%s Found Keyword <%s>",pclFunc,pclResult);
      /* Only for debugging info end*/

      /*      ilRC2=HandleInfoCmd(&pcpData[rgTlxInfo.TxtStart+ilPos], */
      /*                  prgInfoCmd[ilHit][0].function); */

      /* Extract Line with Hit */
      GetDataItem(pclLine,&(pcpData[rgTlxInfo.TxtStart+ilPos]),
              1,'\n',"","  ");
      /* Call function for extracted Line e.g. HandleAA  */
      ilRC2=(prgInfoCmd[ilHit][0].Function)(pclLine);

      for(j=1; prgInfoCmd[ilHit][j].Function != NULL ;j++)
      {   /* Test for secondary keywords like EA*/
          /* and look for Synonyms  */
          ilCount=GetNoOfElements(prgInfoCmd[ilHit][j].TlxInfo,',');
          ilRC = RC_FAIL;
          for(k=1; (k <= ilCount) && (ilRC == RC_FAIL); k++)
          {
              GetDataItem(pclResult, prgInfoCmd[ilHit][j].TlxInfo,k,',',"","\0\0");
              ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart]),pclResult
                                  ,'\n',&ilLine,&ilCol,&ilPos);
              /* For DLA is different, since DL line can also contain A for delay code */
              if( ilRC == RC_SUCCESS && strcmp(prgInfoCmd[ilHit][j].TlxInfo, "\nDLA") == 0 )
              {
                  GetDataItem(pclLine,&(pcpData[rgTlxInfo.TxtStart+ilPos]),1,'\n',"","  ");
                  ilMarkPos = ilPos;
                  ilMarkLine = ilLine;
                  ilMarkCol = ilCol;
                  /* Check whether this is a DL line with code Ax */
                  if( pclLine[4] == '\n' || pclLine[4] == '/' )
                  {
                      ilRC=FindItemInList(&(pcpData[rgTlxInfo.TxtStart+ilMarkPos]),pclResult
                                          ,'\n',&ilLine,&ilCol,&ilPos);
                      ilCol += ilMarkCol;
                      ilLine += ilMarkLine;
                      ilPos += ilMarkPos;
                  }
              }
          }

          if (ilRC == RC_SUCCESS)
          {
             GetDataItem(pclLine,&(pcpData[rgTlxInfo.TxtStart+ilPos]),1,'\n',"","  ");
            if (strcmp(pclResult,"\nDL") != 0 && strcmp(pclResult,"\nEDL") != 0 && strcmp(pclResult,"\nDLA") != 0)
            {
               if (pclLine[strlen(pclResult)-1] == ' ' || isdigit(pclLine[strlen(pclResult)-1]))
                  ilRC = RC_SUCCESS;
               else
                  ilRC = RC_FAIL;
            }
          }

          if(ilRC==RC_SUCCESS)
        {
          /* Only for debugging. start */
          if(pclResult[0] == '\n')
            pclResult[0]=' ';
          dbg(DEBUG,"%s Found Keyword <%s>",pclFunc,pclResult);
          /* Only for debugging. end*/
          /*          ilRC2=HandleInfoCmd(&pcpData[rgTlxInfo.TxtStart+ilPos], */
          /*                      prgInfoCmd[ilHit][j].function); */
          /* Extract Line with Hit */
          GetDataItem(pclLine,&(pcpData[rgTlxInfo.TxtStart+ilPos]),
                  1,'\n',"","  ");
          /* Call function for extracted Line e.g. HandleAA  */
          ilRC2=(prgInfoCmd[ilHit][j].Function)(pclLine);
        }
        } /* end for j*/
      ilRC=RC_SUCCESS;
    } /* end of if */
      else
    {
      dbg(TRACE,"%s Error no primary keyword found in Telex",pclFunc);
     /*   ilRC=RC_SUCCESS; */
    }
    } /* end of if "search Keywords"   */

  if(ilSaveSIPos != 0)
    pcpData[ilSaveSIPos] = clSaveChar;

  return ilRC;

} /* end of SearchInfoCmds  */

static int FindTlxCmd(char *pcpData, int *ipCmdCnt, int *ipGoOn, int ipHeaderTyp)
{
  int ilRC = RC_SUCCESS;
  int i,j,ilLen,ilPos,ilCol;
  int ilDummy;
  static int ilCounter=1;
  char *pclData,*pclPtr;
  T_SYNTLX *prlPtr;
  char pclFunc[]="TlxCmd:";
  char pclResult[100];
  char pclResult2[100];
  T_TLXRESULT prlResult;
  int ilCommandOK;
  int ilRes;
  int ilRC1;

  /* actual cmd counter*/
  i = *ipCmdCnt;
  pclData = pcpData;

  /* prpTlxInfo.TxtStart */
  if (i == 0)
  {
     ilCounter = 1;
     dbg(DEBUG,"%s Show Telex max. 200 <%.200s>",pclFunc,pclData);
  }
  else
  {
     ++ilCounter;
  }
  dbg(DEBUG,"%s %d. Telex command search",pclFunc,ilCounter);

  rgTlxInfo.TlxCmd = 0;
  prlPtr = &prgTlxCmds[i];
  ilRC = RC_FAIL;
  while ((prlPtr->function != -1) && (ilRC == RC_FAIL))
  {
     /* dbg(DEBUG,"%s List of Commands <%s>",pclFunc,prlPtr->SynTlx); */
     pclPtr = prlPtr->SynTlx;
     while ((GetNextDataItem(pclResult,&pclPtr,",","","  ") != 0) && (ilRC == RC_FAIL))
     {
        if (ipHeaderTyp == 11)
        {
           ilPos = 0;
           ilRes = RC_FAIL;
           if (strcmp(pclResult,"\nSCORE") == 0)
              ilRes = RC_SUCCESS;
        }
        else
        {
           ilRes = FindItemInList(&pclData[rgTlxInfo.TxtStart],pclResult,'\n',
                                  &ilLen,&ilCol,&ilPos);
           if (ilRes != RC_SUCCESS && ipHeaderTyp == 6)
           {
              sprintf(pclResult2,"%c %s",pclResult[0],&pclResult[1]);
              ilRes = FindItemInList(&pclData[rgTlxInfo.TxtStart],pclResult2,'\n',
                                     &ilLen,&ilCol,&ilPos);
           }
        }
        if (ilRes == RC_SUCCESS)
        {
           dbg(DEBUG,"%s Found Command <%s> Pos: <%d>",pclFunc,pclResult,ilPos);
           ilRC1 = SecondTimeCheck(&pclData[rgTlxInfo.TxtStart],ilPos,ipHeaderTyp);
           ilPos += rgTlxInfo.TxtStart;
           if (prlPtr->Flag == FALSE)
           {
              *ipGoOn = RC_FAIL;
              dbg(DEBUG,"%s Stop search: multiple search for cmd <%s> is <FALSE>"
                  ,pclFunc,pclResult);
           }
           else
           {
              *ipGoOn = RC_SUCCESS;
           }
           ilCommandOK = TRUE;
           if (strstr(pclResult,"FWD") != NULL)
           {
              if (pclData[rgTlxInfo.TxtStart+ilPos+3] != ' ' && pclData[rgTlxInfo.TxtStart+ilPos+3] != '\0')
              {
                 ilCommandOK = FALSE;
              }
              if (pclData[rgTlxInfo.TxtStart+ilPos-1] == '/')
              {
                 if (!isdigit(pclData[rgTlxInfo.TxtStart+ilPos-2]))
                 {
                    ilCommandOK = FALSE;
                 }
              }
              else
              {
                 if (pclData[rgTlxInfo.TxtStart+ilPos-1] != '\n')
                 {
                    ilCommandOK = FALSE;
                 }
              }
           }
           if (ilCommandOK == TRUE)
           {
              /* Save orignal Telextyp*/
              strcpy(prlResult.DName,"TlxTyp");
              switch (prlPtr->function)
              {
                 case FDI_MVT:
                    sprintf(prlResult.FValue,"MVT");
                    break;
                 case FDI_SRM:
                    sprintf(prlResult.FValue,"SRM");
                    break;
                 case FDI_SAM:
                    sprintf(prlResult.FValue,"SAM");
                    break;
                 case FDI_SLC:
                    sprintf(prlResult.FValue,"SLC");
                    break;
                 case FDI_DLS:
                    sprintf(prlResult.FValue,"DLS");
                    break;
                 case FDI_FWD:
                    sprintf(prlResult.FValue,"FWD");
                    break;
                 case FDI_AFTN:
                    /*sprintf(prlResult.FValue,"ATC");*/
                    sprintf(prlResult.FValue,pcgAftnTelexType);
                    break;
                 case FDI_KRISCOM:
                    sprintf(prlResult.FValue,"KRIS");
                    break;
                 case FDI_ALTEA:
                    sprintf(prlResult.FValue,"ALT");
                    break;
		 case FDI_DPFI:
                    sprintf(prlResult.FValue,"DPFI");
                    break;
		 case FDI_APFI:
                    sprintf(prlResult.FValue,"APFI");
                    break;
                 case FDI_ADF:
                    sprintf(prlResult.FValue,"ADF");
                    break;
                 case FDI_ASDI:
                    sprintf(prlResult.FValue,"ASD");
                    break;
                 case FDI_MAN:
                    sprintf(prlResult.FValue,"MAN");
                    break;
                 case FDI_SCORE:
                    sprintf(prlResult.FValue,"SCOR");
                    break;
                 case FDI_FFM:
                    sprintf(prlResult.FValue,"FFM");
                    break;
                 case FDI_DFS:
                    /*sprintf(prlResult.FValue,"DFS");*/
                    sprintf(prlResult.FValue,pcgDfsTelexType);
                    break;
                 case FDI_UBM:
                    sprintf(prlResult.FValue,"UBM");
                    break;
                 case FDI_CCM:
                    sprintf(prlResult.FValue,"CCM");
                    break;
                 default:
                    if (pclResult[0] == '\n')
                       strcpy(prlResult.FValue,&pclResult[1]);
                    else
                       strcpy(prlResult.FValue,pclResult);
                    break;
              }
              CCSArrayAddUnsort(prgMasterTlx,&prlResult,TLXRESULT);
              rgTlxInfo.TlxCmd = prlPtr->function;
              ilDummy=ilPos;
              /* Set Pointer behind Command */
              FindItemInList(&pclData[ilDummy],"\n",'\n',&ilLen,&ilCol,&ilPos);
              rgTlxInfo.TxtStart = ilDummy+ilPos;
              dbg(DEBUG,"%s Set Text start to <%d>",pclFunc,rgTlxInfo.TxtStart);
              ilRC = RC_SUCCESS;
           } /* end if */
        }/* end of if  */
     }/* end of while  */
     i++;
     prlPtr = &prgTlxCmds[i];
  }/*  end of while */

  *ipCmdCnt = i;

  return ilRC;

} /* end of FindTlxCmd */




static void *GetTlxArrayKeyData(void *pvpUserData,void *pvpData)
{
  static T_TLX_ARRAY_KEY rlResult;
  T_TLX_ARRAY_ELEMENT *prlTemp;

  prlTemp = (T_TLX_ARRAY_ELEMENT*) pvpData;
  strcpy(rlResult.key,prlTemp->TlxType);

  return (&rlResult);
} /* end of GetTlxArrayKeyData  */

static void *SlvGetTlxArrayKeyData(void *pvpUserData,void *pvpData)
{
  static T_TLX_ARRAY_KEY rlResult;
  T_TLX_ARRAY_ELEMENT *prlTemp;

  prlTemp = (T_TLX_ARRAY_ELEMENT*) pvpData;
  strcpy(rlResult.key,prlTemp->Unique);

  return (&rlResult);
} /* end of SlvGetTlxArrayKeyData  */

static void *GetFieldListKeyData(void *pvpUserData,void *pvpData)
{
  static T_FIELDLIST_KEY rlResult;
  T_FIELDLIST *prlTemp;

  prlTemp = (T_FIELDLIST*) pvpData;
  strcpy(rlResult.key,prlTemp->DName);

  return (&rlResult);
} /* end of GetFieldListKeyData  */


static int CompareFieldList(void *pvpUserData,T_FIELDLIST_KEY *a, T_FIELDLIST_KEY *b)
{
    int ilRC=0;

    ilRC = strcmp(a->key,b->key);

    if(ilRC > 0)
        ilRC = 1;
    else if (ilRC == 0)
        ilRC = 0;
    else if (ilRC < 0)
        ilRC = -1;


    return (ilRC);
}

static void *GetCfgArrayKeyData(void *pvpUserData,void *pvpData)
{
  static T_FIELDLIST_KEY rlResult;
  T_CFG_ARRAY_ELEMENT *prlTemp;

  prlTemp = (T_CFG_ARRAY_ELEMENT*) pvpData;
  strcpy(rlResult.key,prlTemp->CfgType);

  return (&rlResult);
} /* end of GetCfgArrayKeyData  */

static int CompareCfgArray(void *pvpUserData,T_FIELDLIST_KEY *a, T_FIELDLIST_KEY *b)
{
    int ilRC=0;

    ilRC = strcmp(a->key,b->key);

    if(ilRC > 0)
        ilRC = 1;
    else if (ilRC == 0)
        ilRC = 0;
    else if (ilRC < 0)
        ilRC = -1;

  return (ilRC);
}


/***************************************************
 * Compare-Function for CCSARRAY library calls
 ****************************************************/
static int CompareTlxArray(void *pvpUserData,T_TLX_ARRAY_KEY *a, T_TLX_ARRAY_KEY *b)
{
    int ilRC=0;

    ilRC = strcmp(a->key,b->key);

    if(ilRC > 0)
        ilRC = 1;
    else if (ilRC == 0)
        ilRC = 0;
    else if (ilRC < 0)
        ilRC = -1;

  return (ilRC);
}


/******************************************************
 * Funtion for CCSArray library to extract Keydata
 *******************************************************/
static void *GetKeyData(void *pvpUserData,void *pvpData)
{
  static T_TLX_ELEMENT_KEY rlResult;
  T_TLXRESULT *prlTemp;

  prlTemp = (T_TLXRESULT*) pvpData;
  strcpy(rlResult.key,prlTemp->DName);

  return (&rlResult);
}

/***************************************************
 * Compare-Function for CCSARRAY library calls
 ****************************************************/
static int Compare(void *pvpUserData,T_TLX_ELEMENT_KEY *a, T_TLX_ELEMENT_KEY *b)
{
    int ilRC=0;

    ilRC = strcmp(a->key,b->key);

    if(ilRC > 0)
        ilRC = 1;
    else if (ilRC == 0)
        ilRC = 0;
    else if (ilRC < 0)
        ilRC = -1;

    return (ilRC);
}



static int CheckChar(char *pcpInput, char cpTyp, int ipCount)
{
  int i=0;
  int ilRC=RC_SUCCESS;
  int (*func)(int);
  int ilLen;
  int ilUnlimit = FALSE;


  if (pcpInput == NULL || *pcpInput == '\0') {
    ilRC = -1;
  }
  else {

    if (ipCount == 0){
      ipCount = 1000;
      ilUnlimit = TRUE;
    }
    ilLen = strlen(pcpInput);

    switch (cpTyp)
      {
      case 'A':  func = *isalpha;
    break;
      case 'N':  func = *isdigit;
    break;
      case 'O':  func = *isalnum;
    break;
      default: ilRC = -2;
      }
    while ( ilRC == RC_SUCCESS && pcpInput[i] != '\0'
        && i < ipCount )
      {
    if((*func)(pcpInput[i]) == 0)
      {
        ilRC = -3;
      }
    i++;
      } /* end of while*/
    if ( ilRC == RC_SUCCESS && pcpInput[i] == 0x00 && (i < ipCount && ilUnlimit == FALSE)){
      ilRC = -4;
    }

  }/* end of else */
  return ilRC;
}/* end of CheckChar*/


static int ChangeMonth(char *pcpResult, char *pcpInput)
{
  int ilSum=0;
  int ilRC=RC_SUCCESS;
  char clResult;
  char *pclSave;
  int ilLine,ilPos,ilCol;
  char clMonth[]= "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC";

  pclSave = pcpInput;

  while (*pclSave != '\0')
    {
      clResult = *pclSave;
      *pclSave  = (char)toupper(clResult);
      pclSave++;
    }

  ilRC = FindItemInList(clMonth, pcpInput,',',&ilLine,&ilPos,&ilCol);
  if(ilRC == RC_SUCCESS)
    {
      sprintf(pcpResult,"%02d",ilLine);
    }
  else
    {
      pcpResult='\0';
    }
  return ilRC;
} /* end of ChangeMonth*/

#if 0
static int SearchCharAndReplaceAll(char *pcpData,char cpOldChar,char cpNewChar)
{
  char *pclData;

  if(pcpData == NULL)
    return -1;
  else
    {
      pclData = pcpData;
      while((*pclData != '\0') && ((pcpData=strchr(pclData,cpOldChar))!=NULL ))
    {
      *pcpData = cpNewChar;
      pclData = pcpData;
    }
    }
  return RC_SUCCESS;
}/* SearchCharAndReplaceAll */
#endif

static int ChangeCharFromTo(char *pcpData,char *pcpFrom,char *pcpTo)
{
  int ilRC = RC_SUCCESS;
  int i;
  char pclFunc[]="ChangeCharFromTo:";
  char *pclData;

  if(pcpData == NULL)
    return -1;
  else if(pcpFrom == NULL)
    return -2;
  else if(pcpTo == NULL)
    return -3;
  else
    {
      pclData = pcpData;

      while(*pclData != 0x00)
    {
      for(i=0; pcpFrom[i] != 0x00 ; i++ )
        {
          if(pcpFrom[i] == *pclData)
        *pclData = pcpTo[i];
        }
      pclData++;
    }
    }

  return RC_SUCCESS;
} /* ChangeCharFromTo */


/************************************************************
 * Function:   PruneFlight
 * Parameter:  IN:   char ** pcpinput
 *
 * Return Code: RC_SUCCESS
 *              RC_FAIL
 *
 * Result:
 *
 * Description: Parts input Strings like
 *              LH1234D/12.DCAHD.FRA
 ************************************************************ */
static int PruneFlight(char *pcpInput, CCS_KEY *prpKey, int ipEnd)
{
  int ilRC=RC_SUCCESS;
  int i,j,k;
  int ilCount=0;
  int ilList = 0;
  int ilHit,ilMax=0;
  char clSeparator;
  char pclFunc[]="PruneFlt:";
  char pclResult[100];
  char pclList[100];
  char pclTmpValue[16];
  char pclOrig[16];
  T_TLXRESULT rlTlxResult;
  T_TLXRESULT *prlTlxResult=NULL;

  T_TLXHEAD *prlLine;

  dbg(DEBUG,"FLIGHT LINE <%s>",pcpInput);

  ilMax = strlen(pcpInput);

  if (ilMax == 0)
  {
     ilRC = RC_FAIL;
     dbg (TRACE,"%s Error empty Input string",pclFunc);
  }

  if (ilRC == RC_SUCCESS)
  {
     ilRC = GetAlc(&pcpInput,prpKey);
  }
  if (ilRC == RC_SUCCESS)
  {
     ilRC = GetFltn(&pcpInput,prpKey);
  }
  if (ilRC == RC_SUCCESS)
  {
     ilMax = strlen(pcpInput);
     while(pcpInput[--ilMax] == ' ');
     /* Search first alphanumeric character */
     while (isalnum(pcpInput[ilCount]) == 0)
     {
        ilCount++;
     }
     if (ilCount < ilMax)
     {
        /* Is alpha character, possible Suffix */
        if (isalpha(pcpInput[ilCount]) != 0)
        {
       /* is next character not alphanumeric, then char. is Suffix */
           if (isalnum(pcpInput[ilCount+1]) == 0)
           {
              sprintf(rlTlxResult.DValue,"%.1s",&pcpInput[ilCount]);
              sprintf(rlTlxResult.FValue,"%.1s",&pcpInput[ilCount]);
              sprintf(rlTlxResult.DName,"Suffix");
              ilRC = CCSArrayAddUnsort(prpKey,&rlTlxResult,TLXRESULT);
              if (ilRC != CCS_SUCCESS)
                 dbg(DEBUG,"%s Error Suffix not add to array",pclFunc);
              dbg(DEBUG,"%s Flight number suffix <%s>",pclFunc,rlTlxResult.DValue );
              ilCount += 2;
           }
        }
     }
     if (ilCount < ilMax)
     {
        /* When digit than Flight Date possible, but only 2 characters    */
        if ((isdigit(pcpInput[ilCount]) != 0) &&
            ((pcpInput[ilCount-1] == '/') || (pcpInput[ilCount-1] == '.')))
        {
           sprintf(rlTlxResult.DValue,"%.2d",atoi(&pcpInput[ilCount]));
           rlTlxResult.DValue[2] = '\0';
           sprintf(rlTlxResult.DName,"FDate");
           ilRC = CCSArrayAddUnsort(prpKey,&rlTlxResult,TLXRESULT);
           if (ilRC != RC_SUCCESS)
              dbg(DEBUG,"%s Error Date not add to array",pclFunc);
           dbg(DEBUG,"%s Flight date <%s>",pclFunc,rlTlxResult.DValue);
           /* Skip trash characters until first nonalphanumeric character */
           while(isalnum(pcpInput[ilCount]) != 0)
           {
              ilCount++;
           }
        }
     }

     /* Search first alphanumeric character as beginning of Registration */
     while(pcpInput[ilCount] != 0x00 && isalnum(pcpInput[ilCount]) == 0)
     {
        ilCount++;
     }
     ilRC = RC_SUCCESS;

     memset(&rlTlxResult,0x00,TLXRESULT);
     ilList = MakeTokenList(pclList,&pcpInput[ilCount],".",',');
     dbg(DEBUG,"%s \".\" pclList <%s> No. <%d>",pclFunc,pclList,ilList);
     if (ilList > 1)
     {
        clSeparator = ',';
     }
     else
     {
        /* ilList = MakeTokenList(pclList,&pcpInput[ilCount]," ",'#'); */
        ilList = MakeTokenList(pclList,&pcpInput[ilCount]," ",',');
        dbg(DEBUG,"%s \" \" pclList <%s> No. <%d>",pclFunc,pclList,ilList);
        /* clSeparator = '#'; */
        clSeparator = ',';
     }

     pclOrig[0] = '\0';
     if (ipEnd != FDI_END_WITH_REGIS)
     {
        ilRC = GetDataItem(pclResult,pclList,ilList--,clSeparator,"","  ");
        if (ilRC > 0 && ilRC == 3 )
        {
           dbg(DEBUG,"%s a.Origin <%s>",pclFunc,pclResult);
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pclResult);
           strcpy(rlTlxResult.FValue,pclResult);
           strcpy(rlTlxResult.DName,"Orig");
           CCSArrayAddUnsort(prpKey,&rlTlxResult,TLXRESULT);
           strcpy(pclOrig,pclResult);
           ilRC = RC_SUCCESS;
        }
        else
        {
           if (ilList > 1)
           {
              ilRC = GetDataItem(pclResult,pclList,ilList--,clSeparator,"","  ");
              if (ilRC > 0 && ilRC == 3 )
              {
                 dbg(DEBUG,"%s b.Origin <%s>",pclFunc,pclResult);
                 memset(&rlTlxResult,0x00,TLXRESULT);
                 strcpy(rlTlxResult.DValue,pclResult);
                 strcpy(rlTlxResult.FValue,pclResult);
                 strcpy(rlTlxResult.DName,"Orig");
                 CCSArrayAddUnsort(prpKey,&rlTlxResult,TLXRESULT);
                 strcpy(pclOrig,pclResult);
                 ilRC = RC_SUCCESS;
              }
           }
        }
     }

     if (ilRC == RC_SUCCESS)
     {
        memset(&rlTlxResult,0x00,TLXRESULT);
        if (clSeparator == ',')
        {
           ilRC = GetDataItem(pclResult,pclList,1,clSeparator,"","  ");
           if (ilRC > 0)
           {
              for (i=0,j=0; pclResult[i] != 0x00; i++)
              {
                 if (isalnum(pclResult[i]) != 0)
                 {
                    rlTlxResult.DValue[j] = pclResult[i];
                    j++;
                 }
              } /* end of for*/
           }
        }
        else
        {
           j=0;
           k = 0;
           while (k != ilList)
           {
              ilRC = GetDataItem(pclResult,pclList,k+1,clSeparator,"","  ");
              if (ilRC > 0)
              {
                 for (i=0; pclResult[i] != 0x00; i++)
                 {
                    if (isalnum(pclResult[i]) != 0)
                    {
                       rlTlxResult.DValue[j] = pclResult[i];
                       j++;
                    }
                 } /* end of for*/
              }/* end of if*/
              ++k;
           } /* end of while*/
        } /* end else separator */
        if (j > 0)
        {
           if (strcmp(pclOrig,rlTlxResult.DValue) != 0)
           {
              strcpy(rlTlxResult.FValue,rlTlxResult.DValue);
              strcpy(rlTlxResult.DName,"Regis");
              ilRC = CCSArrayAddUnsort(prpKey,&rlTlxResult,TLXRESULT);
              if (ilRC != CCS_SUCCESS)
                 dbg(DEBUG,"%s Error Regis not add to array",pclFunc);
              dbg(DEBUG,"%s Registration <%s>",pclFunc,rlTlxResult.FValue);
           }
           else
           {
              dbg(DEBUG,"%s No registration found",pclFunc);
           }
        }
        else
        {
           dbg(DEBUG,"%s No registration found",pclFunc);
        }
        ilRC = RC_SUCCESS;
     }/* end of succ for regis begin*/
  }

  return ilRC;
}/* end of PruneFlight */


/************************************************************
 * Function:   GetFltn
 * Parameter:  IN/OUT:  char ** pcpinput
 *
 * Return Code: RC_SUCCESS   pcpinput points behind hit
 *              RC_FAIL      pcpinput not changed
 *
 * Result:      Flightnumber is written in
 *              "Fltn" in prgMasterTlx
 *
 * Description: Search the first appearance of 1-5 digits.
 *              Max. 5 digits are allowed. Output is padded
 *              up to min. 3 digits. Input String Pointer is
 *              manipulated and points behind the flight number
 *              in the string.
 *
 ************************************************************/
static int GetFltn(char **pcpInput,CCS_KEY *prpKey)
{
  char *pclHit,*pclSave;
  char clNumber[10];
  int ildum,ilCount=0;
  int ilRC=RC_SUCCESS;
  char pclResult[100];
  char pclFunc[]="GetFltn:";
  T_TLXHEAD *prlLine;
  T_TLXRESULT prlTlxResult;

  pclSave=*pcpInput;
  memset(clNumber,0,sizeof(clNumber));
  memset(pclResult,0,sizeof(pclResult));

  /* Search first numeric, as begin of Flight Number */
  while (isdigit(**pcpInput) == 0)
    {
      (*pcpInput)++;
    }
  pclHit=*pcpInput;

  /* Search first not numeric, end of number*/
  while (isdigit(**pcpInput) != 0)
    {
      (*pcpInput)++;
      ilCount++;
    }

  ildum = atoi(strncpy(pclResult,pclHit,ilCount));
  sprintf(pclResult,"%d",ildum);
  switch (strlen(pclResult))
    {
    case 1:
    case 2:
    case 3:
      sprintf(prlTlxResult.FValue,"%03d",ildum);
      strcpy(prlTlxResult.DName,"Fltn");
      strcpy(prlTlxResult.DValue,prlTlxResult.FValue);
      ilRC = CCSArrayAddUnsort(prpKey,&prlTlxResult,TLXRESULT);
      if (ilRC != CCS_SUCCESS)
    dbg(DEBUG,"%s Error Fltn not add to array",pclFunc);
      dbg(DEBUG,"%s FlightNumber <%s>",pclFunc,prlTlxResult.FValue);
      break ;
    case 4:
    case 5:
      sprintf(prlTlxResult.FValue,"%d",ildum);
      strcpy(prlTlxResult.DValue,prlTlxResult.FValue);
      strcpy(prlTlxResult.DName,"Fltn");
      ilRC = CCSArrayAddUnsort(prpKey,&prlTlxResult,TLXRESULT);
      if (ilRC != CCS_SUCCESS)
    dbg(DEBUG,"%s Error Fltn not in array",pclFunc);
      dbg(DEBUG,"%s FlightNumber <%s>",pclFunc,prlTlxResult.FValue);
      break;
    default:
      ilRC = RC_FAIL;
      /* Restore input pointer */
      *pcpInput = pclSave;
      memcpy(clNumber,pclHit,ilCount);
      dbg (TRACE,"%s Error Flight number %s\n",pclFunc,clNumber);
    }
  return ilRC;
}/* end of FDIPruneFlightNumber */




/* ************************************************************
 * Function:   GetAlc
 * Parameter:  IN/OUT:  char ** pcpInput
 *
 *
 * Return Code: RC_SUCCESS   pcpInput points behind hit
 *              RC_FAIL      pcpInput not changed
 *
 * Result:      ALC is written in
 *              prpFlight->cAlc2 or
 *              prpFlight->cAlc3
 *
 * Description: Search the first appearance of 1-2 Alphanumeric
 *              followed by 1 Alphabetic character.
 *              Max. 3 characters are allowed
 *
 ************************************************************ */

static int GetAlc(char **pcpInput,CCS_KEY *prpKey)
{
  char *pclHit,*pclSave;
  char clAirline[10];
  int ilCount=0;
  int ilRC=RC_SUCCESS;
  char pclResult[100];
  char pclFunc[]="GetAlc:";
  T_TLXRESULT prlTlxResult;

  pclSave=*pcpInput;

  memset(clAirline,0,sizeof(clAirline));

  /* Search first alphanumeric */
  while (isalnum(**pcpInput) == 0)
    {
      (*pcpInput)++;
    }
  pclHit=*pcpInput;

  (*pcpInput) += 2;
  ilCount = 2;

  if (isalpha(**pcpInput) != 0){
    (*pcpInput)++;
    ilCount++;
  }

  if(ilCount == 3 || ilCount == 2)
    {
      strncpy(prlTlxResult.DValue,pclHit,ilCount);
      prlTlxResult.DValue[ilCount]='\0';

      ilRC = CheckAlc(pclResult,prlTlxResult.DValue,ilCount);

      if (ilRC == RC_SUCCESS)
    {
      if(ilCount==2)
        dbg(DEBUG,"%s ALC2 <%s> changed to ALC3 <%s>",pclFunc,prlTlxResult.DValue,pclResult);
      else
        dbg(DEBUG,"%s ALC3 Letter <%s>",pclFunc,pclResult);
      strcpy(prlTlxResult.DValue,pclResult);
      strcpy(prlTlxResult.FValue,pclResult);
      strcpy(prlTlxResult.DName,"Alc");
      ilRC = CCSArrayAddUnsort(prpKey,&prlTlxResult,TLXRESULT);

      if (ilRC != CCS_SUCCESS)
        dbg(DEBUG,"%s Error Alc not add to array",pclFunc);
    }
      else
    {
      dbg(TRACE,"%s Error Airline code <%s> not in db ",pclFunc,prlTlxResult.DValue);
    }
    }
  else
    {
      ilRC = RC_FAIL;
      *pcpInput = pclSave;
      memcpy(clAirline,pclHit,ilCount);

      dbg (TRACE,"%s Error Airline code %s\n",pclFunc,clAirline);
    }


  return ilRC;
} /* end of GetAlc */

static int CheckAlc(char *pcpResult,char *pcpInput,int ipCount)
{
  int ilRC=RC_SUCCESS;
  char pclString[4];
  char pclTable[10];
  char pclALC[32];
  char pclValue[32];
  int ilCount=1;

  sprintf(pclTable,"ALT%s",pcgTABEnd);
  sprintf(pclALC,"ALC%d",ipCount);
  sprintf(pclValue,"%s",pcpInput);


  ilRC = syslibSearchDbData(pclTable,pclALC,pclValue,"ALC3",pcpResult,&ilCount,"\n") ;

  return ilRC;
}



static void get_unique(char *s)
{
   struct timeb tp;
   time_t   _CurTime;
   struct tm    *CurTime;
   struct tm    rlTm;
   long     secVal,minVal,hourVal;
   char     tmp[10];
   static int   serial=0;

   _CurTime = time(0L);

   ftime(&tp);

   if(serial++ < 900);
   else
     serial = 0;

   sprintf(s,"%d%3.3d%3.3d",(int)_CurTime,tp.millitm,serial);

}

static char *MakeFltKey(void)
{
  char pclFunc[]="MakeFltKey:";
  char pclResult[100];
  char pclSuffix[2];
  char clDirect ='#';
  T_TLXRESULT *prlTlxPtr=NULL;
  T_TLXRESULT *prlSufPtr=NULL;
  T_TLXRESULT rlResult;

  /* Build FKEY  */
  memset(&rlResult,0,TLXRESULT);
  memset(&pclResult,0x00,sizeof(pclResult));

  /* Get Flight date*/
  if (strlen(pcgFlightSchedTime) == 0)
  {
     Time2Date(pclResult,"0000");
  }
  else
  {
     strcpy(pclResult,pcgFlightSchedTime);
  }

  /* if PTM -> depart or arrival*/
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"Board");
  if (prlTlxPtr != NULL)
  {
     if (strcmp(prlTlxPtr->FValue,pcgHomeAP) == 0)
     {
        clDirect = 'D';
     }
     else
     {
        prlTlxPtr = CCSArrayGetData(prgMasterTlx,"Trans");
        if (prlTlxPtr != NULL)
        {
       if (strcmp(prlTlxPtr->FValue,pcgHomeAP) == 0)
           {
          clDirect = 'A';
       }/* end if */
        } /* end if */
     } /* end else */
  }/* end if */

  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"FltKey");
  if (prlTlxPtr == NULL)
  {
     prlTlxPtr = CCSArrayGetData(prgMasterTlx,"Suffix");
     if (prlTlxPtr != NULL)
     {
        sprintf(pclSuffix,"%1.1s",prlTlxPtr->FValue);
     }
     else
     {
        strcpy(pclSuffix,"#");
     }
     prlTlxPtr = CCSArrayGetData(prgMasterTlx,"Fltn");
     if (prlTlxPtr != NULL)
     {
    sprintf(rlResult.DValue,"%05d",atoi(prlTlxPtr->FValue));
    prlTlxPtr = CCSArrayGetData(prgMasterTlx,"Alc");
    if (prlTlxPtr != NULL)
    {
       sprintf(rlResult.FValue,"%s%s%s%.8s%c",rlResult.DValue,
               prlTlxPtr->FValue,pclSuffix,pclResult,clDirect);
       strcpy(rlResult.DValue,rlResult.FValue);
       strcpy(rlResult.DName,"FltKey");
       CCSArrayAddUnsort(prgMasterTlx,&rlResult,TLXRESULT);
    }
    dbg(DEBUG,"%s FltKey <%s>",pclFunc,rlResult.FValue);
     }
  }
  return rlResult.FValue;
}/* end of MakeFltKey */




static int RereadTlxtab()
{
  static  int ilRC = RC_SUCCESS;
  static  char pclFunc[]="RereadTlxtab";
  static  char pclTxt1[8];
  static  char pclTxt2[8];
  static  char pclTimeField[8];
  static  char pclStartTime[FDI_CEDA_DATELEN+1];
  static  char pclEndTime[FDI_CEDA_DATELEN+1];
  static  char pclIntStartTime[FDI_CEDA_DATELEN+1];
  static  char pclIntEndTime[FDI_CEDA_DATELEN+1];
  static  char pclHdrTyp[3];
  static  char pclHopo[100];
  static  char pclTlxKey[100];
  static  char pclTwStart[12];
  static  int  ilRecv;
  static  int  ilBreakOut = FALSE;
  static  long ilInterval;
  static  char pclTable[16];
  static  char pclTwEnd[32];
  static  char pclResult[100];
  static  char pclFieldList[256];
  static  char pclSelection[1000];
  static  char pclCfgSelection[1000];

  if(igNewReRead == TRUE){
    ilRC = RC_SUCCESS;
    igNewReRead = FALSE;
    memset(pclTable,0x00,sizeof(pclTable));

    ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","table",CFG_STRING,pclTable);
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found table <%s>",pclFunc,pclTable);
      strcat(pclTable,pcgTABEnd);

      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","recv",CFG_STRING,pclResult);
      if(ilRC == RC_SUCCESS){
    ilRecv = atoi(pclResult);
    dbg(DEBUG,"%s Cfg Found recv <%d>",pclFunc,ilRecv);
      }
    }
    ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","interval",CFG_STRING,pclResult);
    if(ilRC == RC_SUCCESS){
      ilInterval = atoi(pclResult)*SECONDS_PER_DAY;
      dbg(DEBUG,"%s Cfg Found interval <%s>",pclFunc,pclResult);
    } else {
      ilInterval = 2*SECONDS_PER_DAY;
      dbg(DEBUG,"%s Cfg interval not found use <2>",pclFunc);
      ilRC = RC_SUCCESS;
    }
    if(ilRC == RC_SUCCESS){
      ilRC = iGetConfigRow(pcgCfgFile,"REREAD","selection",CFG_STRING,pclCfgSelection);
    }
    if(ilRC == RC_SUCCESS){
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","tlxkey",CFG_STRING,pclTlxKey);
    }
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found tkey <%s>",pclFunc,pclTxt1,pclTlxKey);
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","txt1",CFG_STRING,pclTxt1);
    }
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found txt1 <%s>",pclFunc,pclTxt1);
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","txt2",CFG_STRING,pclTxt2);
    }
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found txt2 <%s>",pclFunc,pclTxt2);
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","time_field",CFG_STRING,pclTimeField);
    }
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found time_field <%s>",pclFunc,pclTimeField);
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","hopo",CFG_STRING,pclHopo);
      if(ilRC == RC_SUCCESS && strlen(pclHopo) == 3){
    sprintf(pclTwEnd,"%s%s",pclHopo,&pcgTwEndInfo[3]);
    dbg(DEBUG,"%s Found <%s> combined to <%s>",pclFunc,pclHopo,pclTwEnd);
      } else {
    strcpy(pclTwEnd,pcgTwEndInfo);
    dbg(DEBUG,"%s No hopo found take <%s>",pclFunc,pclTwEnd);
    ilRC = RC_SUCCESS;
      }
    }
    if(ilRC == RC_SUCCESS){
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","start_time",CFG_STRING,pclStartTime);
      if(ilRC == RC_SUCCESS){
    if(strlen(pclStartTime) != FDI_CEDA_DATELEN){
      ilRC = RC_FAIL;
      dbg(TRACE,"%s Error: Wrong CEDATIME Format (start_time)",pclFunc);
    }
      }
    }
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found start_time <%s>",pclFunc,pclStartTime);
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","end_time",CFG_STRING,pclEndTime);
      if(ilRC == RC_SUCCESS){
    if(strlen(pclEndTime) != FDI_CEDA_DATELEN){
      ilRC = RC_FAIL;
      dbg(TRACE,"%s Error: Wrong CEDATIME Format (end_time)",pclFunc);
    }
      }
    }
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found end_time <%s>",pclFunc,pclEndTime);
      ilRC = iGetConfigEntry(pcgCfgFile,"REREAD","header_typ",CFG_STRING,pclHdrTyp);
    }
    if(ilRC == RC_SUCCESS){
      dbg(DEBUG,"%s Cfg Found header_typ <%s>",pclFunc,pclHdrTyp);

      ilRC = strcmp(pclStartTime,pclEndTime);
      if (ilRC > 0){
    strcpy(pclResult,pclStartTime);
    strcpy(pclStartTime,pclEndTime);
    strcpy(pclEndTime,pclResult);
      }

      strcpy(pclIntStartTime,pclStartTime);
      AddSecondsToCEDATime(pclIntStartTime,ilInterval,1);

      ilRC = strcmp(pclIntStartTime,pclEndTime);
      if(ilRC >= 0){
    strcpy(pclIntStartTime,pclStartTime);
    strcpy(pclIntEndTime,pclEndTime);
      } else {
    strcpy(pclIntEndTime,pclIntStartTime);
    strcpy(pclIntStartTime,pclStartTime);
      }

      sprintf(pclFieldList,"%s,%s,%s",pclTlxKey,pclTxt1,pclTxt2);
      sprintf(pclTwStart,"XXX,%s",pclHdrTyp);
      ilRC = RC_SUCCESS;
    }
  }/* end if newreread == TRUE*/

  if (ilRC == RC_SUCCESS){
    if( strcmp(pclIntStartTime,pclIntEndTime) != 0){
      if (strlen(pclCfgSelection) == 0){
    sprintf(pclSelection,"WHERE %s BETWEEN '%s' AND '%s'",
        pclTimeField,pclIntStartTime,pclIntEndTime);

      } else {
    sprintf(pclSelection,"WHERE %s BETWEEN '%s' AND '%s' AND (%s)",
        pclTimeField,pclIntStartTime,pclIntEndTime,pclCfgSelection);
      }
      /*        ilRC = tools_send_info_flag(ilRecv,0,mod_name,"","EXCO","","",pclTwStart,pclTwEnd */
      /*                  ,"RTA",pclTable,pclSelection, */
      /*                  pclFieldList,"",RC_SUCCESS); */

      ilRC = SendCedaEvent(ilRecv,0,mod_name,"EXCO",pclTwStart,pclTwEnd
               ,"RTA",pclTable,pclSelection,
               pclFieldList,"","",4,RC_SUCCESS);

      dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
      dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);

      dbg(DEBUG,"%s RTA sent ",pclFunc);


      strcpy(pclIntStartTime,pclIntEndTime);
      AddSecondsToCEDATime(pclIntEndTime,ilInterval,1);
      /*        dbg(DEBUG,"%s after add pclIntEndTime <%s>",pclFunc,pclIntEndTime);  */

      ilRC = strcmp(pclIntEndTime,pclEndTime);
      /*        dbg(DEBUG,"%s pclEndTime <%s>",pclFunc,pclEndTime);  */
      if (ilRC > 0){
    strcpy(pclIntEndTime,pclEndTime);
      }
      /*        dbg(DEBUG,"%s pclIntStartTime <%s>",pclFunc,pclIntStartTime); */
      /*        dbg(DEBUG,"%s pclIntEndTime <%s>",pclFunc, pclIntEndTime);  */
      ilRC = RC_SUCCESS;
    } else {
      dbg(DEBUG,"%s End of Reread reached",pclFunc);
      igReReadRun = FALSE;
    }
  } else {
    dbg(TRACE,"%s Error in configuration ",pclFunc);
    igReReadRun = FALSE;
  }

  return RC_SUCCESS;

}/*  end of RereadTlxtab */


static int DeleteTlxRow(char *pcpTlxKey)
{
  int ilRC = RC_SUCCESS;
  int ilReceiver;
  char pclFunc[]="DeleteTlxRow";
  char pclSelection[100];
  char pclTlxKey[10];
  char pclTable[10];
  T_FIELDLIST *prlCfgField;
  T_CFG_ARRAY_ELEMENT *prlCfgArrElem;


  prlCfgArrElem = CCSArrayGetFirst(prgMasterCmpCfgArray);
  if(prlCfgArrElem != NULL){
    if(prlCfgArrElem->MasterKey != NULL){
      prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Recv");
      if(prlCfgField != NULL){
    ilReceiver = atoi(prlCfgField->FName);
      } else {
    dbg(DEBUG,"%s <%s> no value for recv (see cfg file)",pclFunc);
    ilRC = RC_FAIL;
      }
      if(ilRC != RC_FAIL)
    {
      prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Table");
      if(prlCfgField != NULL){
        strcpy(pclTable,prlCfgField->FName);
      } else {
        dbg(DEBUG,"%s <%s> no value for Table (see cfg file)",pclFunc);
        ilRC = RC_FAIL;
      }
    }/* end of if */
      if(ilRC != RC_FAIL){
    prlCfgField = CCSArrayGetData(prlCfgArrElem->MasterKey,"Unique");
    if(prlCfgField != NULL){
      strcpy(pclTlxKey,prlCfgField->FName);
    } else {
      dbg(DEBUG,"%s <%s> no value for TlxKey (see cfg file)",pclFunc);
      ilRC = RC_FAIL;
    }
      }/* end of if */
    }
  }
  if(atoi(pcpTlxKey) == 0){
    dbg(DEBUG,"%s %s empty can not delete row",pclFunc,pclTlxKey,pclSelection);
    ilRC = RC_FAIL;
  }
  if(ilRC == RC_SUCCESS){
    dbg(DEBUG,"%s Sent DRT with Selection <%s>",pclFunc,pclSelection);
    sprintf(pclSelection,"WHERE %s = '%s'",pclTlxKey,pcpTlxKey);

    /*      ilRC = tools_send_info_flag(ilReceiver,0,pcgDestName,"",pcgRecvName,"" */
    /*                  ,""," ",pcgTwEndNew */
    /*                  ,"DRT",pclTable,pclSelection, */
    /*                  "","",NETOUT_NO_ACK); */

    ilRC = SendCedaEvent(ilReceiver,0,pcgDestName,pcgRecvName," ",pcgTwEndNew
             ,"DRT",pclTable,pclSelection,
             "","","",4,NETOUT_NO_ACK);

    dbg(DEBUG,"%s Sent DRT with Selection <%s>",pclFunc,pclSelection);
  }


  return RC_SUCCESS;
}/*end of  DeleteTlxRow */

static void SetSubtypeTo(char * pclSubType)
{
  T_TLXRESULT prlTlxResult;
  T_TLXRESULT *prlPtrResult;

  prlPtrResult = CCSArrayGetData(prgMasterTlx,"Subtype");

  if ( prlPtrResult == NULL){

    memset(&prlTlxResult,0x00,TLXRESULT);
    strcpy(prlTlxResult.FValue,pclSubType);
    strcpy(prlTlxResult.DName,"Subtype");

    CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
  }

  strcpy(pcgSubType,pclSubType);

return;
}


static int GetLOAData(T_CFG_ARRAY_ELEMENT *pcpCfgArrElem, char *pcpField, T_TLXRESULT **prlTlxPtr)
{
  int ilRC = 0;
  T_FIELDLIST *prlCfgField = NULL;

  prlCfgField = CCSArrayGetData(pcpCfgArrElem->MasterKey,pcpField);
  if (prlCfgField != NULL)
  {
     dbg(TRACE,"Field <%s> found",pcpField);
     *prlTlxPtr = CCSArrayGetData(prgMasterTlx,pcpField);
  }
  else
  {
     dbg(TRACE,"Field <%s> not found",pcpField);
     *prlTlxPtr = NULL;
  }
  return ilRC;
}

#if 0
static void PrintPM(regmatch_t *pm)
{
  char pclFunc[]="PrintPM:";
    dbg(DEBUG,"%s pm[0].rm_so <%d> pm[0].rm_eo <%d> ",pclFunc,pm[0].rm_so,pm[0].rm_eo);
    dbg(DEBUG,"%s pm[1].rm_so <%d> pm[1].rm_eo <%d> ",pclFunc,pm[1].rm_so,pm[1].rm_eo);
    dbg(DEBUG,"%s pm[2].rm_so <%d> pm[2].rm_eo <%d> ",pclFunc,pm[2].rm_so,pm[2].rm_eo);
    dbg(DEBUG,"%s pm[3].rm_so <%d> pm[3].rm_eo <%d> ",pclFunc,pm[3].rm_so,pm[3].rm_eo);
    dbg(DEBUG,"%s pm[4].rm_so <%d> pm[4].rm_eo <%d> ",pclFunc,pm[4].rm_so,pm[4].rm_eo);
    dbg(DEBUG,"%s pm[5].rm_so <%d> pm[5].rm_eo <%d> ",pclFunc,pm[5].rm_so,pm[5].rm_eo);
    dbg(DEBUG,"%s pm[6].rm_so <%d> pm[6].rm_eo <%d> ",pclFunc,pm[6].rm_so,pm[6].rm_eo);
}
#endif

/* -------------------------------------------------------------------- */
/* Config manipulate functions                                          */
/* -------------------------------------------------------------------- */

static int ReadConfig(char *pcpCfgFile)
{
  int  ilRC = RC_SUCCESS;
  char pclFunc[]="ReadCfg:";
  char clSave;
  char pclResult[1000];
  char pclResult2[1000];
  char pclResult3[1000];
  char pclValue[32];
  char *pclPtr;
  char *pclPTMPtr = NULL;
  int  p,k,i,j,q,ilCount = 0;
  int  ilNumber = 0;
  int  ilCmdCounter=0;
  char aclBuffer[10] = "";
  char aclClieBuffer[100];
  char aclServBuffer[100];
  char cgUfisConfigFile[100];
  T_FIELDLIST prlField;
  T_FIELDLIST *prlElement;
  T_CFG_ARRAY_ELEMENT prlCmdElement,prlCfgDocElement;
  CCS_KEY *prlMasterCfgDoc,*prlMasterCfg;
  CCS_KEY *prlSlaveCfg;
  T_CFG_ARRAY_ELEMENT *prlCfgArrayEPtr;
  T_CFG_ARRAY_ELEMENT *prlCfgDocEPtr;
  int ilI;
  char *pclAddr;
  char *pclKey;
  char *pclKey2;

  /* Read debug_level*/
  ilRC=iGetConfigEntry(pcpCfgFile,"GLOBAL","debug_level",CFG_STRING,pclResult);
  if(ilRC == RC_SUCCESS)
  {
   dbg(DEBUG,"%s debug_level = <%s>",pclFunc,pclResult);
   debug_level = TRACE ;
   if(strcmp(pclResult,"DEBUG") == 0)
   {
    debug_level = DEBUG;
   }
   else if(strcmp(pclResult,"OFF") == 0) debug_level = 0 ;
  }

  /* Read utc to local time flag    */
  igutc2local = FALSE;
  ilRC = iGetConfigEntry(pcpCfgFile,"GLOBAL","utc2local",CFG_STRING,pclResult);
  if(ilRC == RC_SUCCESS)
  {
   dbg(DEBUG,"%s utc2local = <%s>",pclFunc,pclResult);
   if(strcmp(pclResult,"ON") == 0)
   {
    igutc2local = TRUE;
   }
  }

  /* Read debug_level*/
  igSendToLoa = FALSE;
  ilRC = iGetConfigEntry(pcpCfgFile,"LDM","SendToLoa",CFG_STRING,pclResult);
  if(ilRC == RC_SUCCESS)
  {
   dbg(DEBUG,"%s SendToLoa = <%s>",pclFunc,pclResult);
   if (strcmp(pclResult,"YES") == 0)
   {
    igSendToLoa = TRUE;
   }
  }

  /* Get Client and Server chars */
  sprintf(cgUfisConfigFile,"%s/ufis_ceda.cfg",getenv("CFG_PATH"));

  ilRC = iGetConfigEntry(cgUfisConfigFile, "CHAR_PATCH", "CLIENT_CHARS",
             CFG_STRING, aclClieBuffer);
  if (ilRC == RC_SUCCESS)
  {
   ilRC = iGetConfigEntry(cgUfisConfigFile, "CHAR_PATCH", "SERVER_CHARS",
               CFG_STRING, aclServBuffer);
   if (ilRC == RC_SUCCESS)
   {
    i = GetNoOfElements(aclClieBuffer,',');
    if ( i == GetNoOfElements(aclServBuffer,','))
    {
     memset(pcgServerChars,0x00,100*sizeof(char));
     memset(pcgClientChars,0x00,100*sizeof(char));
     for ( j=0; j < i; j++)
     {
    GetDataItem(aclBuffer,aclServBuffer,j+1,',',"","\0\0");
    pcgServerChars[j*2] = atoi(aclBuffer);
      pcgServerChars[j*2+1] = ',';
     }
     pcgServerChars[(j-1)*2+1] = 0x00;
     for ( j=0; j < i; j++)
     {
    GetDataItem(aclBuffer,aclClieBuffer,j+1,',',"","\0\0");
    pcgClientChars[j*2] = atoi(aclBuffer);
      pcgClientChars[j*2+1] = ',';
     }
     pcgClientChars[(j-1)*2+1] = 0x00;
     dbg (DEBUG,"New Clientchars <%s> dec <%s>",pcgClientChars,aclClieBuffer);
     dbg (DEBUG,"New Serverchars <%s> dec <%s>",pcgServerChars,aclServBuffer);
     dbg (DEBUG," Serverchars <%d> ",strlen(pcgServerChars));
     dbg (DEBUG,"%s  und count <%d> ",aclBuffer,strlen(pcgServerChars));
     dbg (DEBUG,"i <%d>",i);
    }
   } else {
           ilRC = RC_SUCCESS;
           dbg(DEBUG,"Use standard (old) serverchars");
          }
  } else {
          dbg(DEBUG,"Use standard (old) serverchars");
          ilRC = RC_SUCCESS;
         }
   if (ilRC != RC_SUCCESS)
   {
      strcpy(pcgClientChars,"\042\047\054\012\015");
      strcpy(pcgServerChars,"\260\261\262\263\263");
      ilRC = RC_SUCCESS;
   }


  /* Read trace Telex results*/
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Tlxres",CFG_STRING,pclResult);

  if(ilRC == RC_SUCCESS)
  {
   igTraceTlxRes = FALSE;
   if(strcmp(pclResult,"ON") == 0)
   {
    igTraceTlxRes = TRUE;
   }
  }
  else {
        igTraceTlxRes = FALSE;
        strcpy(pclResult,"OFF");
       }
  dbg(DEBUG,"%s Tlxres = <%s>",pclFunc,pclResult);

  /* Read first class identifiers*/
  ilRC = iGetConfigEntry(pcpCfgFile,"PTM","First",CFG_STRING,pcgPTMFirst);
  if(ilRC != RC_SUCCESS)
  {
   pcgPTMFirst[0] = 'H';
   pcgPTMFirst[1] = 0x00;
  }
  dbg(DEBUG,"%s PTM First class <%s>",pclFunc,pcgPTMFirst);

  /* Read business class identifiers*/
  ilRC = iGetConfigEntry(pcpCfgFile,"PTM","Bus",CFG_STRING,pcgPTMBus);
  if(ilRC != RC_SUCCESS)
  {
    pcgPTMBus[0]= 'C';
    pcgPTMBus[1]= 0x00;
  }
  dbg(DEBUG,"%s PTM Business class <%s>",pclFunc,pcgPTMBus);

  /* Read economy class identifiers*/
  ilRC = iGetConfigEntry(pcpCfgFile,"PTM","Eco",CFG_STRING,pcgPTMEco);
  if(ilRC != RC_SUCCESS)
  {
    pcgPTMEco[0]= 'Y';
    pcgPTMEco[1]= 0x00;
  }
  dbg(DEBUG,"%s PTM Economy class <%s>",pclFunc,pcgPTMEco);

  /* Read premium class identifiers*/
  ilRC = iGetConfigEntry(pcpCfgFile,"PTM","Prem",CFG_STRING,pcgPTMPrem);
  if(ilRC != RC_SUCCESS)
  {
    pcgPTMPrem[0]= 'U';
    pcgPTMPrem[1]= 0x00;
  }
  dbg(DEBUG,"%s PTM Premium class <%s>",pclFunc,pcgPTMPrem);

  /* Enable Multiple Telexs in one */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","MultiTlx",CFG_STRING,pclResult);
  if(ilRC == RC_SUCCESS)
  {
   igMultiTlx = FALSE;
   if(strcmp(pclResult,"ON") == 0)
   {
    igMultiTlx = TRUE;
   }
  }
  else
  {
   igMultiTlx = FALSE;
   strcpy(pclResult,"OFF");
  }
  dbg(DEBUG,"%s MultiTlx = <%s>",pclFunc,pclResult);

  /* Read config file for TELEX "complete"*/
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","TTrace",CFG_STRING,pclResult);
  if(ilRC == RC_SUCCESS)
  {
    dbg(DEBUG,"%s TTrace = <%s>",pclFunc,pclResult);
    if(strcmp(pclResult,"ON") == 0)
    {
      igTraceTlx = TRUE;
      dbg(DEBUG,"%s Telex Trace is ON",pclFunc);
      ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","TraceSize",CFG_STRING,pclResult);
      if (ilRC != RC_SUCCESS)
    igFileSize = 1000000;
      else if ((igFileSize = atoi(pclResult)) == 0)
    igFileSize = 1000000;
      dbg(DEBUG,"%s TraceSize <%d>",pclFunc,igFileSize);
    }
    else
    {
      igTraceTlx = FALSE;
      dbg(DEBUG,"%s Telex Trace is OFF",pclFunc);
    }
  }
  else
  {
    igTraceTlx = FALSE;
    dbg(DEBUG,"%s Telex Trace is OFF",pclFunc);
  }

  memset(pclResult,0x00,sizeof(pclResult));
  /* Read config file for: exclude TELEX cmd from multiple  search*/
  ilRC = iGetConfigRow(pcpCfgFile,"TELEX","Extlxcmd",CFG_STRING,pclResult);
  dbg(DEBUG,"%s Exclude TELEX cmd returned <%s>",pclFunc,pclResult);

  if(ilRC == RC_SUCCESS)
  {
    memset(pclResult2,0,sizeof(pclResult2));

    ilNumber = MakeTokenList(pclResult2,pclResult,",",',');
    /* Insert an array for every found command  */
    for (k=1; k <= ilNumber ; k++)
    {
      GetDataItem(pclResult,pclResult2,k,',',"","  ");
      if (strlen(pclResult) > 0){
    dbg(DEBUG,"%s Insert <%s> in Cfg array",pclFunc,pclResult);
    for (j=0; pclResult[j] != '\0'; j++)
      prlCmdElement.CfgType[j]  = (char)toupper(pclResult[j]);
    prlCmdElement.CfgType[j]  = '\0';
    prlCmdElement.Flag  = FALSE; /* No Multiple Tlx Search allowed*/
    dbg(DEBUG,"%s Stop search after command is found",pclFunc);
    prlCmdElement.MasterKey = NULL;
    ilRC=CCSArrayAddUnsort(prgMasterCmdCfgArray,&prlCmdElement,CFG_ARRAY_ELEMENT);
      } /* end of if */
    } /* end of for k */
  } /* end of if  */

  memset(&prlCmdElement,0x00, sizeof(T_CFG_ARRAY_ELEMENT));
  memset(pclResult,0x00,sizeof(pclResult));
  /* Read config file for TELEX "complete"*/
  ilRC = iGetConfigRow(pcpCfgFile,"TELEX","commands",CFG_STRING,pclResult);
  dbg(DEBUG,"%s TELEX returned <%s>",pclFunc,pclResult);

  if(ilRC == RC_SUCCESS)
  {
    memset(pclResult2,0,sizeof(pclResult2));

    ilNumber = MakeTokenList(pclResult2,pclResult,",",',');
    /* Insert an array for every found command  */
    for (k=1; k <= ilNumber ; k++)
      {
    GetDataItem(pclResult,pclResult2,k,',',"","  ");
    dbg(DEBUG,"%s Insert <%s> in Cfg array",pclFunc,pclResult);
    for (j=0; pclResult[j] != '\0'; j++)
      prlCmdElement.CfgType[j]  = (char)toupper(pclResult[j]);
    prlCmdElement.CfgType[j]  = '\0';
    prlCmdElement.Flag  = TRUE; /* Multiple Tlx Search allowed*/
    prlCmdElement.MasterKey = NULL;
    CCSArrayAddUnsort(prgMasterCmdCfgArray,&prlCmdElement,CFG_ARRAY_ELEMENT);
      } /* end of for k */
  } /* end of if  */


  while ((prlCfgArrayEPtr = CCSArrayArrNext(prgMasterCmdCfgArray)) != NULL )
    {

      /* Init search array for Telegram typs  */
      if(strcmp(prlCfgArrayEPtr->CfgType,"MVT") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_MVT;
      pclPtr = pcgMVTVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"LDM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_LDM;
      pclPtr = pcgLDMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"CPM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_CPM;
      pclPtr = pcgCPMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"PTM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_PTM;
      pclPTMPtr = malloc((strlen(pcgPTMVars)+strlen(pcgPTMTVars)+2)*sizeof(char));
      sprintf(pclPTMPtr,"%s,%s",pcgPTMVars,pcgPTMTVars);
      pclPtr = pclPTMPtr;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"SAM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_SAM;
      pclPtr = pcgSAMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n-TITLE %s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"SLC") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_SLC;
      pclPtr = pcgSLCVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n-TITLE %s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"SRM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_SRM;
      pclPtr = pcgSRMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n-TITLE %s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"RQM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_RQM;
      pclPtr = pcgRQMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"DIV") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_DIV;
      pclPtr = pcgDIVVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"TPM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_TPM;
      pclPtr = pcgTPMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"PSM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_PSM;
      pclPtr = pcgPSMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"ETM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_ETM;
      pclPtr = pcgETMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"FWD") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_FWD;
      pclPtr = pcgFWDVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"SCR") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_SCR;
      pclPtr = pcgSCRVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"BTM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_BTM;
      pclPtr = pcgBTMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"UCM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_UCM;
      pclPtr = pcgUCMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"DLS") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_DLS;
      pclPtr = pcgDLSVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"AFTN") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_AFTN;
      pclPtr = pcgAFTNVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"KRISCOM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_KRISCOM;
      pclPtr = pcgKRISCOMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"ALTEA") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_ALTEA;
      pclPtr = pcgALTEAVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"DPFI") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_DPFI;
      pclPtr = pcgDPIVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"APFI") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_APFI;
      pclPtr = pcgDPIVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }

      else if (strcmp(prlCfgArrayEPtr->CfgType,"ADF") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_ADF;
      pclPtr = pcgADFVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"ASDI") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_ASDI;
      pclPtr = pcgASDIVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"MAN") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_MAN;
      pclPtr = pcgMANVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"SCORE") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_SCORE;
      pclPtr = pcgSCOREVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"FFM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_FFM;
      pclPtr = pcgFFMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"DFS") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_DFS;
      pclPtr = pcgDFSVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"UBM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_UBM;
      pclPtr = pcgUBMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"CCM") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_CCM;
      pclPtr = pcgCCMVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"BAY") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_BAY;
      pclPtr = pcgBAYVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"PAL") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_PAL;
      pclPtr = pcgPALVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"CAL") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_CAL;
      pclPtr = pcgCALVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else if (strcmp(prlCfgArrayEPtr->CfgType,"PSC") == 0)
    {
      prgTlxCmds[ilCmdCounter].function = FDI_PSC;
      pclPtr = pcgPSCVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }
      else
    {
      prgTlxCmds[ilCmdCounter].function = FDI_OTHERS;
      pclPtr = pcgOthersVars;
      sprintf(prgTlxCmds[ilCmdCounter].SynTlx,"\n%s",prlCfgArrayEPtr->CfgType);
    }

      dbg(DEBUG,"%s Read %s config section",pclFunc,prlCfgArrayEPtr->CfgType);

      AddCfgElement(pclPtr,pcpCfgFile,prlCfgArrayEPtr);

      /* Set Flag for Multiple telex search*/
      prgTlxCmds[ilCmdCounter].Flag = prlCfgArrayEPtr->Flag;

      if(prgTlxCmds[ilCmdCounter].Flag == TRUE){
    dbg(DEBUG,"%s Flag is TRUE",pclFunc);
      } else {
    dbg(DEBUG,"%s Flag is FALSE",pclFunc);
      }

      /* Add synonymes to telex typ search list  */
      if(prlCfgArrayEPtr->MasterKey != NULL) {
    if((prlElement = CCSArrayGetData(prlCfgArrayEPtr->MasterKey,"Synonyms")) != NULL)
      {
        ilCount = MakeTokenList(pclResult,prlElement->FName,",",',');
        for(i=1; i <= ilCount;i++)
          {
        GetDataItem(pclResult2,pclResult,i,',',"","  ");
            if (prgTlxCmds[ilCmdCounter].function == FDI_FWD ||
                prgTlxCmds[ilCmdCounter].function == FDI_DLS ||
                prgTlxCmds[ilCmdCounter].function == FDI_KRISCOM ||
                prgTlxCmds[ilCmdCounter].function == FDI_ALTEA ||
                prgTlxCmds[ilCmdCounter].function == FDI_ADF ||
                prgTlxCmds[ilCmdCounter].function == FDI_ASDI)
                {
           strcat(prgTlxCmds[ilCmdCounter].SynTlx,",");
                }
                else
                {
           strcat(prgTlxCmds[ilCmdCounter].SynTlx,",\n");
                }
        strcat(prgTlxCmds[ilCmdCounter].SynTlx,pclResult2);
          }
      }
    dbg(DEBUG,"%s Complete Command list <%s>",pclFunc,prgTlxCmds[ilCmdCounter].SynTlx);
      }
      if (strcmp(prlCfgArrayEPtr->CfgType,"KRISCOM") == 0)
      {
         ilRC = ReadKriscomConfig();
      }
      if (strcmp(prlCfgArrayEPtr->CfgType,"ALTEA") == 0)
      {
         ilRC = ReadAlteaConfig();
      }
      if (strcmp(prlCfgArrayEPtr->CfgType,"PSC") == 0)
      {
         ilRC = ReadPSCConfig();
      }
      ilCmdCounter++;
    } /* end of while */

  /* Insert end flag to array  */
  strcpy(prgTlxCmds[ilCmdCounter].SynTlx,"");
  prgTlxCmds[ilCmdCounter].function = -1;


  /* Read config file for TELEX  "complete"*/
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","complete",CFG_STRING,pclResult);

  if(ilRC == RC_SUCCESS)
    {
      dbg(DEBUG,"%s TELEX \"complete\" returned <%s>",pclFunc,pclResult);

      memset(pclResult2,0,sizeof(pclResult2));

      ilNumber = MakeTokenList(pclResult2,pclResult," ,",',');
      /* Insert an array for every found command  */
      for (k=1; k <= ilNumber ; k++)
    {
      GetDataItem(pclResult,pclResult2,k,',',"","  ");
      for (j=0; pclResult[j] != '\0'; j++)
        prlCmdElement.CfgType[j]  = (char)toupper(pclResult[j]);
      prlCmdElement.CfgType[j]  = '\0';
      dbg(DEBUG,"%s prlCmdElement.CfgType <%s>",pclFunc,
          prlCmdElement.CfgType);
      prlCmdElement.MasterKey = NULL;
      CCSArrayAddUnsort(prgMasterCmpCfgArray,&prlCmdElement,CFG_ARRAY_ELEMENT);
    } /* end of for k */
    } /* end of if  */

  while ((prlCfgArrayEPtr = CCSArrayGetNext(prgMasterCmpCfgArray)) != NULL )
    {
      pclPtr = pcgCmpTlx;
      dbg(DEBUG,"%s Read %s config section",pclFunc,&prlCfgArrayEPtr->CfgType);

      AddCfgElement(pclPtr,pcpCfgFile,prlCfgArrayEPtr);

    } /* end of while */

  if(pclPTMPtr != NULL){
    free(pclPTMPtr);
  }

  /* Schedule ACTION (YES/NO) */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ScheduleAction",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
        igScheduleAction = TRUE;
  }
  dbg(TRACE,"Schedule ACTION = %d",igScheduleAction);

  /* Get text field size of TLXTAB  */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","TlxTextFieldSize",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igTlxTextFieldSize = atoi(pclResult);
  }
  dbg(TRACE,"Text Field Size in TLXTAB = %d",igTlxTextFieldSize);

  /* Read config file for stopping telex interpretation */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","StopTelexInterpretation",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igStopTelexInterpretation = TRUE;
     }
  }
  dbg(TRACE,"Stop Telex Interpretation = %d",igStopTelexInterpretation);

  /* Read config file for calculation of total weight */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","TotalWeightCalForSin",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igTotalWeightCalForSin = TRUE;
     }
  }
  dbg(TRACE,"Total Weight Calculation For SIN = %d",igTotalWeightCalForSin);

  /* Read config file for adding infants to total pax */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","LdmAddInfants",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igLdmAddInfants = FALSE;
     }
  }
  dbg(TRACE,"Add Infants = %d",igLdmAddInfants);

  /* Read config file for adding transit pax to classes */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","LdmIncludeTransit",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igLdmIncludeTransit = TRUE;
     }
  }
  dbg(TRACE,"Include Transit Pax = %d",igLdmIncludeTransit);

  /* Read config file to allow call sign in flight line */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AllowCallSign",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igAllowCallSign = TRUE;
     }
  }
  dbg(TRACE,"Allow Call Sign in Flight Line = %d",igAllowCallSign);

  /* Read config file setting AA/AD Times for Home Airport (MVT) */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","SetAAADTimesForHomeApt",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igSetAAADTimesForHomeApt = FALSE;
     }
  }
  dbg(TRACE,"Set AA/AD Times for Home Airport (MVT) = %d",igSetAAADTimesForHomeApt);

  /* Read config file setting AA/AD Times for Home Airport (AFTN) */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","SetAAADTimesForHomeAptAFTN",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igSetAAADTimesForHomeAptAFTN = FALSE;
     }
  }
  dbg(TRACE,"Set AA/AD Times for Home Airport (AFTN) = %d",igSetAAADTimesForHomeAptAFTN);

  /* Read config file setting ETA if ONBL or ATA is set */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","SetETAIfONBOrATA",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igSetETAIfONBOrATA = TRUE;
     }
  }
  dbg(TRACE,"Set ETA, if ONBL or ATA is set = %d",igSetETAIfONBOrATA);

  /* Update from AA MVT Telex */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","UpdateFromAATelex",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igUpdateFromAATelex = FALSE;
     }
  }
  dbg(TRACE,"Update from AA MVT Telex = %d",igUpdateFromAATelex);

  /* Get max Time Difference between Current Time and Telex Time */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","MaxTimeDiff",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igMaxTimeDiff = atoi(pclResult);
  }
  dbg(TRACE,"Max Time Difference between Current Time and Telex Time = %d min",igMaxTimeDiff);

  /* Ignore old Telexes */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","IgnoreOldTelexes",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igIgnoreOldTelexes = FALSE;
     }
  }
  dbg(TRACE,"Ignore old Telexes = %d",igIgnoreOldTelexes);

  /* Broadcast old Telexes */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","BroadcastOldTelexes",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igBroadcastOldTelexes = FALSE;
     }
  }
  dbg(TRACE,"Broadcast old Telexes = %d",igBroadcastOldTelexes);

  /* Broadcast Telexes */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","BroadcastTelexes",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igBroadcastTelexes = FALSE;
     }
  }
  dbg(TRACE,"Broadcast Telexes = %d",igBroadcastTelexes);

  /* Store CPM Deatils */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","StoreCPMDetails",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igStoreCPMDetails = TRUE;
     }
  }
  dbg(TRACE,"Store CPM Details = %d",igStoreCPMDetails);

  /* Get ULD Data from SI Section for CPM Telexes */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","GetULDNumbersFromSISectionForCPM",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igGetULDNumbersFromSISectionForCPM = TRUE;
     }
  }
  dbg(TRACE,"Get ULD Data from SI Section for CPM Telexes = %d",igGetULDNumbersFromSISectionForCPM);

  /* Store LDM Deatils */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","StoreLDMDetails",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igStoreLDMDetails = TRUE;
     }
  }
  dbg(TRACE,"Store LDM Details = %d",igStoreLDMDetails);

  /* Get VIA Information from LDM for Arrival Flight */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","GetViaInfoFromLDM",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igGetViaInfoFromLDM = TRUE;
     }
  }
  dbg(TRACE,"Get VIA Information from LDM for Arrival Flight = %d",igGetViaInfoFromLDM);

  /* Process Rotation Telexes automatically */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ProcessRotTlx",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igProcessRotTlx = TRUE;
     }
  }
  dbg(TRACE,"Process Rotation Telexes automatically = %d",igProcessRotTlx);

  /* Process Arr/Dep LDM Telexes */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PROCESS_ARR_LDM_TLX_AFT",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igProcessArrLdmTlxAft = FALSE;
     }
  }
  dbg(TRACE,"Process Arrival LDM Telexes (AFT) = %d",igProcessArrLdmTlxAft);
/* Process REGN in case is not found in Basic Data
     This will trigger the Conflict "not found in basic data"
     The following must be also configured in flight
     1.) Section [AFT_FIELDCONFIG]
    <REGN> = SEND,KEEP,WARN
    (KEEP: To Update the flight with the received value anyway)
    (WARN: To send a message to Slhdl to insert a conflict in CFLTAB)
    2.) Section [CONFLICT_HANDLING]
    ENABLE_HCC = YES
  */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PROCESS_REGN_IF_NOTIN_BD",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igProcessREGNifNotFound = TRUE;
     }
  }
  dbg(TRACE,"Process REGN in case is not found in Basic Data  = %d",igProcessArrLdmTlxAft);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PROCESS_ARR_LDM_TLX_LOA",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igProcessArrLdmTlxLoa = FALSE;
     }
  }
  dbg(TRACE,"Process Arrival LDM Telexes (LOA) = %d",igProcessArrLdmTlxLoa);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PROCESS_DEP_LDM_TLX_AFT",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igProcessDepLdmTlxAft = FALSE;
     }
  }
  dbg(TRACE,"Process Departure LDM Telexes (AFT) = %d",igProcessDepLdmTlxAft);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PROCESS_DEP_LDM_TLX_LOA",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igProcessDepLdmTlxLoa = FALSE;
     }
  }
  dbg(TRACE,"Process Departure LDM Telexes (LOA) = %d",igProcessDepLdmTlxLoa);

  /* Handle VIAs of MVT Telex */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","HandleMVTVIA",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igHandleMVTVIA = TRUE;
     }
  }
  dbg(TRACE,"Handle VIAs of MVT Telex = %d",igHandleMVTVIA);

  /* Clear Estimated Time if Next Info Time is sent */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ClearETOnNXI",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igClearETOnNXI = TRUE;
     }
  }
  dbg(TRACE,"Clear Estimated Time if Next Info Time is sent = %d",igClearETOnNXI);

  /* Ignore Estimated Time if AD is missing */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","IgnoreETIfADMissing",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igIgnoreETIfADMissing = TRUE;
     }
  }
  dbg(TRACE,"Ignore Estimated Time if AD is missing = %d",igIgnoreETIfADMissing);

  /* Store Delay Codes in DLMTAB */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","HandleDLMTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igHandleDLMTAB = TRUE;
     }
  }
  dbg(TRACE,"Store Delay Codes in DLMTAB = %d",igHandleDLMTAB);

  /**** MEI 27-OCT-2010 ****/
  /* Store Delay Codes in DCFTAB */
  igHandleDCFTAB = FALSE;
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","HandleDCFTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS && strcmp(pclResult,"YES") == 0)
      igHandleDCFTAB = TRUE;
  dbg(TRACE,"Store Delay Codes in DCFTAB = %d",igHandleDCFTAB);


  if( igHandleDCFTAB == TRUE )
  {
      /* New feature to handle delay sub-code, requires new columns DECS in DENTAB */
      igHandleDelaySubCode = FALSE;
      ilRC = iGetConfigEntry(pcpCfgFile,"DCF_SECTION","HandleDelaySubCode",CFG_STRING,pclResult);
      if (ilRC == RC_SUCCESS && strcmp(pclResult,"YES") == 0)
          igHandleDelaySubCode = TRUE;
      dbg( TRACE,"Handle Delay Sub-Codes = %d",igHandleDelaySubCode );

      /* Originally only 2 delay codes are stored in DCFTAB, this parameter determine how many to be stored */
      ilRC = iGetConfigEntry(pcpCfgFile,"DCF_SECTION","MaxDelayInDCF",CFG_STRING,pclResult);
      if( ilRC == RC_SUCCESS )
      {
          igMaxDelayInDCF = MAX_DL_REASON;
          if( strcmp(pclResult,"ALL") )
            igMaxDelayInDCF = atoi(pclResult);
      }
      if( igMaxDelayInDCF <= 0 || igMaxDelayInDCF > MAX_DL_REASON )
          igMaxDelayInDCF = igMaxDelayInAFT;
      dbg( TRACE,"Max No. of Delay Codes to be stored in DCF = %d",igMaxDelayInDCF );

      igHandleMultipleDelay = FALSE;
      if( igMaxDelayInDCF > igMaxDelayInAFT )
          igHandleMultipleDelay = TRUE;
      dbg( TRACE,"Handle Multiple Delay Code = %d", igHandleMultipleDelay );

      igInsInvalidDelayInDCF = TRUE;
      ilRC = iGetConfigEntry(pcpCfgFile,"DCF_SECTION","InsInvalidDelayInDCF",CFG_STRING,pclResult);
      if (ilRC == RC_SUCCESS && strcmp(pclResult,"NO") == 0)
          igInsInvalidDelayInDCF = FALSE;
      dbg( TRACE,"Insert Invalid Delay Code In DCFTAB = %d",igInsInvalidDelayInDCF );

      /* This required a new table DCMTAB */
      igArchiveDCF = FALSE;
      ilRC = iGetConfigEntry(pcpCfgFile,"DCF_SECTION","ArchiveDCF",CFG_STRING,pclResult);
      if (ilRC == RC_SUCCESS && strcmp(pclResult,"YES") == 0)
          igArchiveDCF = TRUE;
      dbg( TRACE,"Archive DCF records = %d", igArchiveDCF );

      /* This will delete all existing DCF records including USR before reloading new telex delay codes */
      igEraseDCF = FALSE;
      ilRC = iGetConfigEntry(pcpCfgFile,"DCF_SECTION","EraseDCF",CFG_STRING,pclResult);
      if (ilRC == RC_SUCCESS && strcmp(pclResult,"YES") == 0)
          igEraseDCF = TRUE;
      dbg( TRACE,"Erase all DCF records = %d", igEraseDCF );

      /* This will allow fdihdl to update USER Entries  */
      igUpdateUSERRecords = FALSE;
      ilRC = iGetConfigEntry(pcpCfgFile,"DCF_SECTION","UpdateUSERRecords",CFG_STRING,pclResult);
      if (ilRC == RC_SUCCESS && strcmp(pclResult,"YES") == 0)
          igUpdateUSERRecords = TRUE;
      dbg( TRACE,"Update User Records From FDIHDL = %d", igUpdateUSERRecords );

      /* To fill in either DECA/DECN when the pair is found from the basic data  */
      igSyncDECADECN = FALSE;
      ilRC = iGetConfigEntry(pcpCfgFile,"DCF_SECTION","SyncDECADECN",CFG_STRING,pclResult);
      if (ilRC == RC_SUCCESS && strcmp(pclResult,"YES") == 0)
          igSyncDECADECN = TRUE;
      dbg( TRACE,"Sync DECA/DECN field base on basic data = %d", igSyncDECADECN );
}
  /*****************************/

  /* MVT Subtypes for DLMTAB */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","MVT_SubTypes_for_DLMTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDLMSubTypes,pclResult);
  }
  dbg(TRACE,"MVT Subtypes for DLMTAB = <%s>",pcgDLMSubTypes);

  /* Data Source Types for Delations from DLMTAB */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DataSourceForDeletesFromDLMTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDelFromDLM,"");
     if (strlen(pclResult) > 0)
     {
    ilCount = GetNoOfElements(pclResult,',');
    for (k=1; k <= ilCount; k++)
        {
           GetDataItem(pclResult2,pclResult,k,',',"","  ");
           strcat(pcgDelFromDLM,"'");
           strcat(pcgDelFromDLM,pclResult2);
           strcat(pcgDelFromDLM,"',");
        }
        pcgDelFromDLM[strlen(pcgDelFromDLM)-1] = '\0';
     }
  }
  dbg(TRACE,"Data Source Types for Deletions from DLMTAB = <%s>",pcgDelFromDLM);

  /* Use Array Insert */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ArrayInsert",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igArrayInsert = TRUE;
     }
  }
  dbg(TRACE,"Use Array Insert = %d",igArrayInsert);

  /* Check Flight Date */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","CheckFlightDate",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igCheckFlightDate = TRUE;
     }
  }
  dbg(TRACE,"Check Flight Date = %d",igCheckFlightDate);

  if (igCheckFlightDate == TRUE)
  {
     /* Use Scheduled Time of Flight Date for Check */
     ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","UseSchedTime",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        if (strcmp(pclResult,"NO") == 0)
        {
           igUseStimForCheck = FALSE;
        }
     }
     if (igUseStimForCheck == TRUE)
     {
        dbg(TRACE,"Use Scheduled Time for Check = %d",TRUE);
        dbg(TRACE,"Use Flight Date for Check = %d",FALSE);
     }
     else
     {
        dbg(TRACE,"Use Scheduled Time for Check = %d",FALSE);
        dbg(TRACE,"Use Flight Date for Check = %d",TRUE);
     }
  }

  /* No of minutes to specify a too old flight */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","FlightIsTooOld",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igFlightIsTooOld = atoi(pclResult);
  }
  dbg(TRACE,"No of minutes to specify a too old flight = %d",igFlightIsTooOld);

  /* Update Closed Flight (for MVT or AFTN only) */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","UpdateClosedFlight",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igUpdateClosedFlight = FALSE;
     }
  }
  dbg(TRACE,"Update Closed Flight (for MVT or AFTN only) = %d",igUpdateClosedFlight);

  /* Use Registration for Flight Search config file to allow call sign in flight line */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","UseRegnForFlightSearch",CFG_STRING,pcgUseRegnForFlightSearch);
  if (ilRC != RC_SUCCESS)
     strcpy(pcgUseRegnForFlightSearch,"");
  dbg(TRACE,"Use Registration for Flight Search = <%s>",pcgUseRegnForFlightSearch);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnTelexType",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgAftnTelexType,pclResult);
  }
  dbg(TRACE,"Telex Type for AFTN Messages = <%s>",pcgAftnTelexType);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DfsTelexType",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgDfsTelexType,pclResult);
  }
  dbg(TRACE,"Telex Type for DFS Messages = <%s>",pcgDfsTelexType);

  /* Insert AFTN flights */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnInsert",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igAftnInsert = TRUE;
     }
  }
  dbg(TRACE,"Insert AFTN flights = %d",igAftnInsert);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnTtyp",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgAftnTtyp,pclResult);
  }
  dbg(TRACE,"TTYP for Insert AFTN flights = <%s>",pcgAftnTtyp);

  /* AFTN STYP during ISF */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnStyp",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgAftnStyp,pclResult);
  }
  dbg(TRACE,"STYP for Insert AFTN flights = <%s>",pcgAftnStyp);

  /* Insert AFTN PLANOWANA flights */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnPLANOWANAInsert",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igAftnInsertPLANOWANA = TRUE;
     }
  }
  dbg(TRACE,"Insert AFTN PLANOWANA flights = %d",igAftnInsertPLANOWANA);

  /* Update AFTN flights */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnUpdate",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igAftnUpdate = TRUE;
     }
  }
  dbg(TRACE,"Update AFTN flights = %d",igAftnUpdate);

  /* Update AFTN type list */
  if (igAftnUpdate == TRUE)
  {
     ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","UpdateAftnTypeList",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        strcpy(pcgUpdateAftnTypeList,pclResult);
     }
     dbg(TRACE,"Update AFTN types = <%s>",pcgUpdateAftnTypeList);
  }

  /* Configurable AFTN fields */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnConfigurableFields",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igAftnConfigurableFields = TRUE;
     }
  }
  dbg(TRACE,"Configurable AFTN fields = %d",igAftnConfigurableFields);

  /* Calculate ETA From DEP AFTN Message */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","CalculateETAFromDEPMsg",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igCalculateETAFromDEPMsg = TRUE;
     }
  }
  dbg(TRACE,"Calculate ETA From DEP AFTN Message = %d",igCalculateETAFromDEPMsg);

  /* Accept and Accumulate PTM Telexes from Origin and VIA */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AccPTMFromOriginVia",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igAccPTMFromOriginVia = TRUE;
     }
  }
  dbg(TRACE,"Accept and Accumulate PTM Telexes from Origin and VIA = %d",igAccPTMFromOriginVia);

  /* PSM Interpretation */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PSMInterpretation",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igPSMInterpretation = TRUE;
     }
  }
  dbg(TRACE,"PSM Interpretation = %d",igPSMInterpretation);

  /* Ignore Departure PSM Telexes from Home Address */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","IgnoreDepPSMTelexesFromHomeAddr",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igIgnoreDepPSMTelexesFromHomeAddr = TRUE;
     }
  }
  dbg(TRACE,"Ignore Departure PSM Telexes from Home Address = %d",igIgnoreDepPSMTelexesFromHomeAddr);

  /* PRM From LDM */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PRMFromLDM",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igPRMFromLDM = TRUE;
     }
  }
  dbg(TRACE,"PRM From LDM = %d",igPRMFromLDM);

  /* PAL/CAL Interpretation */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PALInterpretation",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igPALInterpretation = TRUE;
     }
  }
  dbg(TRACE,"PAL/CAL Interpretation = %d",igPALInterpretation);

  if (igPSMInterpretation == TRUE || igPRMFromLDM == TRUE)
  {
     /* PSM Key Words */
     ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PSMKeyWords",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        strcpy(pcgPSMKeyWords,pclResult);
     }
     dbg(TRACE,"PSM Key Words = <%s>",pcgPSMKeyWords);
  }

  if (igPALInterpretation == TRUE)
  {
     /* PAL/CAL Key Words */
     ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PRMKeyWords",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        strcpy(pcgPRMKeyWords,pclResult);
     }
     dbg(TRACE,"PAL/CAL Key Words = <%s>",pcgPRMKeyWords);
  }

  /* Create 2 DPX Records */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Create2DPXRecords",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igCreate2DPXRecords = TRUE;
     }
  }
  dbg(TRACE,"Create 2 DPX Records = %d",igCreate2DPXRecords);

  if (igPALInterpretation == TRUE || igPSMInterpretation == TRUE || igPRMFromLDM == TRUE)
  {
     /* Handling Type for PRM */
     ilRC = iGetConfigRow(pcpCfgFile,"TELEX","HandlingTypeForPRM",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        strcpy(pcgHandlingTypeForPRM,pclResult);
     }
     dbg(TRACE,"Handling Type for PRM = <%s>",pcgHandlingTypeForPRM);

     /* Send Message to Alerter for PAL/CAL or PSM Telexes */
     ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","SendMsgToAlerterForPAL_PSM",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
     {
        if (strcmp(pclResult,"YES") == 0)
        {
           igSendMsgToAlerterForPAL_PSM = TRUE;
        }
     }
     dbg(TRACE,"Send Message to Alerter for PAL/CAL or PSM Telexes = %d",igSendMsgToAlerterForPAL_PSM);
  }

  /* Append Fields to SCORE Message */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ScoreAppend",CFG_STRING,pcgScoreAppend);
  if (ilRC != RC_SUCCESS)
     strcpy(pcgScoreAppend,"");
  dbg(TRACE,"ScoreAppend = <%s>",pcgScoreAppend);

  /* Automatically interpret SCORE Message */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ScoreInterpretation",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igScoreInterpretation = TRUE;
     }
  }
  dbg(TRACE,"ScoreInterpretation = %d",igScoreInterpretation);
  if (igScoreInterpretation == TRUE)
  {
     /* Read Position Values */
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosAction",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosAction = atoi(pclResult);
     dbg(TRACE,"PosAction = %d",igPosAction);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosHopo",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosHopo = atoi(pclResult);
     dbg(TRACE,"PosHopo = %d",igPosHopo);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosFlnoArr",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosFlnoArr = atoi(pclResult);
     dbg(TRACE,"PosFlnoArr = %d",igPosFlnoArr);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosFlnoDep",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosFlnoDep = atoi(pclResult);
     dbg(TRACE,"PosFlnoDep = %d",igPosFlnoDep);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosValidFrom",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosValidFrom = atoi(pclResult);
     dbg(TRACE,"PosValidFrom = %d",igPosValidFrom);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosValidTo",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosValidTo = atoi(pclResult);
     dbg(TRACE,"PosValidTo = %d",igPosValidTo);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosDoop",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosDoop = atoi(pclResult);
     dbg(TRACE,"PosDoop = %d",igPosDoop);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosAct3",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosAct3 = atoi(pclResult);
     dbg(TRACE,"PosAct3 = %d",igPosAct3);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosAct5",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosAct5 = atoi(pclResult);
     dbg(TRACE,"PosAct5 = %d",igPosAct5);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosOrig",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosOrig = atoi(pclResult);
     dbg(TRACE,"PosOrig = %d",igPosOrig);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosViaArr",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosViaArr = atoi(pclResult);
     dbg(TRACE,"PosViaArr = %d",igPosViaArr);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosOrig4",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosOrig4 = atoi(pclResult);
     dbg(TRACE,"PosOrig4 = %d",igPosOrig4);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosVia4Arr",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosVia4Arr = atoi(pclResult);
     dbg(TRACE,"PosVia4Arr = %d",igPosVia4Arr);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosDest",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosDest = atoi(pclResult);
     dbg(TRACE,"PosDest = %d",igPosDest);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosViaDep",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosViaDep = atoi(pclResult);
     dbg(TRACE,"PosViaDep = %d",igPosViaDep);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosDest4",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosDest4 = atoi(pclResult);
     dbg(TRACE,"PosDest4 = %d",igPosDest4);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosVia4Dep",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosVia4Dep = atoi(pclResult);
     dbg(TRACE,"PosVia4Dep = %d",igPosVia4Dep);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosStoa",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosStoa = atoi(pclResult);
     dbg(TRACE,"PosStoa = %d",igPosStoa);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosStod",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosStod = atoi(pclResult);
     dbg(TRACE,"PosStod = %d",igPosStod);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosOverInd",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosOverInd = atoi(pclResult);
     dbg(TRACE,"PosOverInd = %d",igPosOverInd);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosStypArr",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosStypArr = atoi(pclResult);
     dbg(TRACE,"PosStypArr = %d",igPosStypArr);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosStypDep",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosStypDep = atoi(pclResult);
     dbg(TRACE,"PosStypDep = %d",igPosStypDep);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosTermArr",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosTermArr = atoi(pclResult);
     dbg(TRACE,"PosTermArr = %d",igPosTermArr);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","PosTermDep",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igPosTermDep = atoi(pclResult);
     dbg(TRACE,"PosTermDep = %d",igPosTermDep);
     /* Append Field List for SCORE Message */
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","ScoreAppendFieldList",CFG_STRING,pcgScoreAppendFieldList);
     if (ilRC != RC_SUCCESS)
        strcpy(pcgScoreAppendFieldList,"");
     dbg(TRACE,"ScoreAppendFieldList = <%s>",pcgScoreAppendFieldList);
     /* Handle Past Flights */
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","HandlePastFlights",CFG_STRING,pcgHandlePastFlights);
     dbg(TRACE,"Handle Past Flights = %d",atoi(pcgHandlePastFlights));
     /* Read flight mod_id */
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","MODID_INS",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igModIdIns = atoi(pclResult);
     dbg(TRACE,"MODID_INS = %d",igModIdIns);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","MODID_UPD",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igModIdUpd = atoi(pclResult);
     dbg(TRACE,"MODID_UPD = %d",igModIdUpd);
     ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","MODID_DEL",CFG_STRING,pclResult);
     if (ilRC == RC_SUCCESS)
        igModIdDel = atoi(pclResult);
     dbg(TRACE,"MODID_DEL = %d",igModIdDel);
  }

  /* MEI 25-FEB-2010 Whether to calculate onboard values */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","CalcTotOnboard",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
        igCalcTotOnboard = TRUE;
  }
  dbg( TRACE,"Calc total onboard = <%d>",igCalcTotOnboard );

  /* AFTN time frame parameters */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnArrP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAftnArrP1 = atoi(pclResult);
  }
  dbg(TRACE,"Arr Time Frame Start = %d",igAftnArrP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnArrP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAftnArrP2 = atoi(pclResult);
  }
  dbg(TRACE,"Arr Time Frame End = %d",igAftnArrP2);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnArrP3",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAftnArrP3 = atoi(pclResult);
  }
  dbg(TRACE,"Arr Time Frame for FPL = %d",igAftnArrP3);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnDepP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAftnDepP1 = atoi(pclResult);
  }
  dbg(TRACE,"Dep Time Frame Start = %d",igAftnDepP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnDepP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAftnDepP2 = atoi(pclResult);
  }
  dbg(TRACE,"Dep Time Frame End = %d",igAftnDepP2);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnDepP3",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAftnDepP3 = atoi(pclResult);
  }
  dbg(TRACE,"Dep Time Frame for FPL = %d",igAftnDepP3);

  /* Insert ADF flights */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfInsert",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igAdfInsert = FALSE;
     }
  }
  dbg(TRACE,"Insert ADF flights = %d",igAdfInsert);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfTtyp",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy(pcgAdfTtyp,pclResult);
  }
  dbg(TRACE,"TTYP for Insert ADF flights = <%s>",pcgAdfTtyp);
  /* Update ADF flights */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfUpdate",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igAdfUpdate = FALSE;
     }
  }
  dbg(TRACE,"Update ADF flights = %d",igAdfUpdate);
  /* Delete ADF flights */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfDelete",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igAdfDelete = FALSE;
     }
  }
  dbg(TRACE,"Delete ADF flights = %d",igAdfDelete);
  /* ADF time frame parameters */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfArrP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfArrP1 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Arr Time Frame Start = %d",igAdfArrP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfArrP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfArrP2 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Arr Time Frame End = %d",igAdfArrP2);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfDepP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfDepP1 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Dep Time Frame Start = %d",igAdfDepP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfDepP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfDepP2 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Dep Time Frame End = %d",igAdfDepP2);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfArrP3",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfArrP3 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Arr Time Frame Start (Sector) = %d",igAdfArrP3);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfArrP4",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfArrP4 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Arr Time Frame End (Sector) = %d",igAdfArrP4);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfDepP3",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfDepP3 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Dep Time Frame Start (Sector) = %d",igAdfDepP3);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AdfDepP4",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAdfDepP4 = atoi(pclResult);
  }
  dbg(TRACE,"ADF Dep Time Frame End (Sector) = %d",igAdfDepP4);

  /* ASDI time frame parameters */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AsdiArrP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAsdiArrP1 = atoi(pclResult);
  }
  dbg(TRACE,"ASDI Arr Time Frame Start = %d",igAsdiArrP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AsdiArrP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAsdiArrP2 = atoi(pclResult);
  }
  dbg(TRACE,"ASDI Arr Time Frame End = %d",igAsdiArrP2);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AsdiDepP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAsdiDepP1 = atoi(pclResult);
  }
  dbg(TRACE,"ASDI Dep Time Frame Start = %d",igAsdiDepP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AsdiDepP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igAsdiDepP2 = atoi(pclResult);
  }
  dbg(TRACE,"ASDI Dep Time Frame End = %d",igAsdiDepP2);

  /* DFS time frame parameters */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DfsArrP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igDfsArrP1 = atoi(pclResult);
  }
  dbg(TRACE,"DFS Arr Time Frame Start = %d",igDfsArrP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DfsArrP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igDfsArrP2 = atoi(pclResult);
  }
  dbg(TRACE,"DFS Arr Time Frame End = %d",igDfsArrP2);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DfsDepP1",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igDfsDepP1 = atoi(pclResult);
  }
  dbg(TRACE,"DFS Dep Time Frame Start = %d",igDfsDepP1);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DfsDepP2",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igDfsDepP2 = atoi(pclResult);
  }
  dbg(TRACE,"DFS Dep Time Frame End = %d",igDfsDepP2);

  /* Read config file for time windows */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ArrWindowStart",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igArrWindowStart = atoi(pclResult);
  }
  dbg(TRACE,"Arr Window Start = %d",igArrWindowStart);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ArrWindowEnd",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igArrWindowEnd = atoi(pclResult);
  }
  dbg(TRACE,"Arr Window End = %d",igArrWindowEnd);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DepWindowStart",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igDepWindowStart = atoi(pclResult);
  }
  dbg(TRACE,"Dep Window Start = %d",igDepWindowStart);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","DepWindowEnd",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igDepWindowEnd = atoi(pclResult);
  }
  dbg(TRACE,"Dep Window End = %d",igDepWindowEnd);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_00",CFG_STRING,&pcgTimeArray1[0][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_01",CFG_STRING,&pcgTimeArray1[1][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_02",CFG_STRING,&pcgTimeArray1[2][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_03",CFG_STRING,&pcgTimeArray1[3][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_04",CFG_STRING,&pcgTimeArray1[4][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_05",CFG_STRING,&pcgTimeArray1[5][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_06",CFG_STRING,&pcgTimeArray1[6][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_07",CFG_STRING,&pcgTimeArray1[7][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_08",CFG_STRING,&pcgTimeArray1[8][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_09",CFG_STRING,&pcgTimeArray1[9][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_10",CFG_STRING,&pcgTimeArray1[10][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_11",CFG_STRING,&pcgTimeArray1[11][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_12",CFG_STRING,&pcgTimeArray1[12][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_13",CFG_STRING,&pcgTimeArray1[13][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_14",CFG_STRING,&pcgTimeArray1[14][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_15",CFG_STRING,&pcgTimeArray1[15][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_16",CFG_STRING,&pcgTimeArray1[16][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_17",CFG_STRING,&pcgTimeArray1[17][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_18",CFG_STRING,&pcgTimeArray1[18][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_19",CFG_STRING,&pcgTimeArray1[19][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_20",CFG_STRING,&pcgTimeArray1[20][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_21",CFG_STRING,&pcgTimeArray1[21][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_22",CFG_STRING,&pcgTimeArray1[22][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour1_23",CFG_STRING,&pcgTimeArray1[23][0]);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_00",CFG_STRING,&pcgTimeArray2[0][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_01",CFG_STRING,&pcgTimeArray2[1][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_02",CFG_STRING,&pcgTimeArray2[2][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_03",CFG_STRING,&pcgTimeArray2[3][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_04",CFG_STRING,&pcgTimeArray2[4][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_05",CFG_STRING,&pcgTimeArray2[5][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_06",CFG_STRING,&pcgTimeArray2[6][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_07",CFG_STRING,&pcgTimeArray2[7][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_08",CFG_STRING,&pcgTimeArray2[8][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_09",CFG_STRING,&pcgTimeArray2[9][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_10",CFG_STRING,&pcgTimeArray2[10][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_11",CFG_STRING,&pcgTimeArray2[11][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_12",CFG_STRING,&pcgTimeArray2[12][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_13",CFG_STRING,&pcgTimeArray2[13][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_14",CFG_STRING,&pcgTimeArray2[14][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_15",CFG_STRING,&pcgTimeArray2[15][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_16",CFG_STRING,&pcgTimeArray2[16][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_17",CFG_STRING,&pcgTimeArray2[17][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_18",CFG_STRING,&pcgTimeArray2[18][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_19",CFG_STRING,&pcgTimeArray2[19][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_20",CFG_STRING,&pcgTimeArray2[20][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_21",CFG_STRING,&pcgTimeArray2[21][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_22",CFG_STRING,&pcgTimeArray2[22][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour2_23",CFG_STRING,&pcgTimeArray2[23][0]);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_00",CFG_STRING,&pcgTimeArray3[0][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_01",CFG_STRING,&pcgTimeArray3[1][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_02",CFG_STRING,&pcgTimeArray3[2][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_03",CFG_STRING,&pcgTimeArray3[3][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_04",CFG_STRING,&pcgTimeArray3[4][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_05",CFG_STRING,&pcgTimeArray3[5][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_06",CFG_STRING,&pcgTimeArray3[6][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_07",CFG_STRING,&pcgTimeArray3[7][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_08",CFG_STRING,&pcgTimeArray3[8][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_09",CFG_STRING,&pcgTimeArray3[9][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_10",CFG_STRING,&pcgTimeArray3[10][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_11",CFG_STRING,&pcgTimeArray3[11][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_12",CFG_STRING,&pcgTimeArray3[12][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_13",CFG_STRING,&pcgTimeArray3[13][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_14",CFG_STRING,&pcgTimeArray3[14][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_15",CFG_STRING,&pcgTimeArray3[15][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_16",CFG_STRING,&pcgTimeArray3[16][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_17",CFG_STRING,&pcgTimeArray3[17][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_18",CFG_STRING,&pcgTimeArray3[18][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_19",CFG_STRING,&pcgTimeArray3[19][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_20",CFG_STRING,&pcgTimeArray3[20][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_21",CFG_STRING,&pcgTimeArray3[21][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_22",CFG_STRING,&pcgTimeArray3[22][0]);
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","Hour3_23",CFG_STRING,&pcgTimeArray3[23][0]);

  /* Store Details for Arrival Flights of @PIN Message */
  ilRC = iGetConfigEntry(pcpCfgFile,"LOATAB","ARR_DTL_FOR_PIN",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igArrDtlForPin = TRUE;
     }
  }
  dbg(TRACE,"Store Details for Arrival Flights of @PIN Message = %d",igArrDtlForPin);

  /* Read config file for new LOATAB structure */
  ilRC = iGetConfigEntry(pcpCfgFile,"LOATAB","NEW_LOATAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igNewLoatab = TRUE;
     }
  }
  dbg(TRACE,"Handle New LOATAB Layout = %d",igNewLoatab);
  ilRC = iGetConfigEntry(pcpCfgFile,"LOATAB","NEW_LOATAB_UKEY",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igNewLoatabUkey = TRUE;
     }
  }
  dbg(TRACE,"Handle New LOATAB Layout with UKEY = %d",igNewLoatabUkey);
  if (igNewLoatab == TRUE)
  {
     ilI = 0;
     pclAddr = pcgLoatabKeys[ilI].pclAddr;
     while (pclAddr != NULL)
     {
        pclKey = pcgLoatabKeys[ilI].pclKey;
        ilRC = iGetConfigRow(pcpCfgFile,"LOATAB",pclKey,CFG_STRING,pclResult);
        if (ilRC == RC_SUCCESS)
        {
           strcpy(pclAddr,pclResult);
        }
        else
        {
           strcpy(pclAddr,pcgLoatabKeys[ilI].pclText);
        }
        if (igNewLoatabUkey == TRUE)
        {
           strcat(pclAddr,",");
           pclKey2 = strstr(pclKey,"_");
           if (pclKey2 != NULL)
           {
              pclKey2++;
              strcat(pclAddr,pclKey2);
           }
           else
              strcat(pclAddr," ");
        }
        ilI++;
        pclAddr = pcgLoatabKeys[ilI].pclAddr;
     }
     ilI = 0;
     pclAddr = pcgLoatabKeys[ilI].pclAddr;
     while (pclAddr != NULL)
     {
        pclKey = pcgLoatabKeys[ilI].pclKey;
        dbg(TRACE,"LOATAB KEY <%s> = <%s>",pclKey,pclAddr);
        ilI++;
        pclAddr = pcgLoatabKeys[ilI].pclAddr;
     }
  }

  igNewLoatabCofl = FALSE;
  ilRC = iGetConfigEntry(pcpCfgFile,"LOATAB","NEW_LOATAB_COFL",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igNewLoatabCofl = TRUE;
     }
  }
  dbg(TRACE,"LOATAB:NEW_LOATAB_COFL :<%d>", igNewLoatabCofl);

  /*    PrintCfgArrayStructure(); */

  /* 20060828 JIM: added for GHSHDL/Billing WAW: */
  ilRC = iGetConfigEntry(pcpCfgFile,"BILLING","BillingModId",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igToBilling = atoi(pclResult);
  }
  dbg(TRACE,"ModId of Billing process = %d",igToBilling);

    /*
	Read config file setting if should copy Origin's STOD to Origin's ATOD
	when the ATD of the last VIA is received (AFTN). This is a workaround
	to make FIPS Daily Schedule highlight the flight.
  */
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnCopyOrgStdToOrgAtdWhenViaDep",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"YES") == 0)
     {
        igAftnCopyOrgStdToAtdWhenViaDep = TRUE;
     }
     else
        igAftnCopyOrgStdToAtdWhenViaDep = FALSE;
  }
  dbg(TRACE,"Copy Org Std to Org Atd if Via Departs (AFTN) = %d",igAftnCopyOrgStdToAtdWhenViaDep);

  ilRC = iGetConfigEntry(pcpCfgFile,"LDM","FLIGHT_MOVEMENT_LOG",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igFlightMovementLog = TRUE;
     }
     else
       igFlightMovementLog = FALSE;
  else
       igFlightMovementLog = FALSE;
  dbg(TRACE,"LDM:Handle FLIGHT_MOVEMENT_LOG :<%d>", igFlightMovementLog);

  ilRC = iGetConfigEntry(pcpCfgFile,"FLIGHT_MOVEMENT_LOG","ARR_ALC_TO_LDMTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy (pcgArrAlcToLdmtab, pclResult);
  else
     strcpy (pcgArrAlcToLdmtab, "");
  dbg(TRACE,"FLIGHT_MOVEMENT_LOG:ARR_ALC_TO_LDMTAB:<%s>", pcgArrAlcToLdmtab);

  ilRC = iGetConfigEntry(pcpCfgFile,"FLIGHT_MOVEMENT_LOG","DEP_ALC_TO_LDMTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy (pcgDepAlcToLdmtab, pclResult);
  else
     strcpy (pcgDepAlcToLdmtab, "");
  dbg(TRACE,"FLIGHT_MOVEMENT_LOG:DEP_ALC_TO_LDMTAB:<%s>", pcgDepAlcToLdmtab);

  ilRC = iGetConfigEntry(pcpCfgFile,"FLIGHT_MOVEMENT_LOG","ALC_TRANSIT_CALC",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy (pcgAlcTransitCalc, pclResult);
  else
     strcpy (pcgAlcTransitCalc, "");
  dbg(TRACE,"FLIGHT_MOVEMENT_LOG:ALC_TRANSIT_CALC:<%s>", pcgAlcTransitCalc);

  ilRC = iGetConfigEntry(pcpCfgFile,"FLIGHT_MOVEMENT_LOG","ALC_DISEMBARK_CALC",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy (pcgAlcDisembarkCalc, pclResult);
  else
     strcpy (pcgAlcDisembarkCalc, "");
  dbg(TRACE,"FLIGHT_MOVEMENT_LOG:ALC_DISEMBARK_CALC:<%s>", pcgAlcDisembarkCalc);

  ilRC = iGetConfigEntry(pcpCfgFile,"FLIGHT_MOVEMENT_LOG","MAX_DAYS_AHEAD_FOR_LDM_TRANSIT",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     igMaxDaysAheadForLdmTransit = atoi (pclResult);
  else
     igMaxDaysAheadForLdmTransit = 3;
  dbg(TRACE,"FLIGHT_MOVEMENT_LOG:MAX_DAYS_AHEAD_FOR_LDM_TRANSIT:<%d>",igMaxDaysAheadForLdmTransit);


  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","UseAkftab",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igUseAkftab = TRUE;
     }
     else
       igUseAkftab = FALSE;
  else
       igUseAkftab = FALSE;
  dbg(TRACE,"TELEX:UseAkftab :<%d>", igUseAkftab);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AKFTAB_TEST",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy (pcgModnameAkftabTest, pclResult);
  else
     strcpy (pcgModnameAkftabTest, "undefined");
  dbg(TRACE,"TELEX:AKFTAB_TEST:<%s>", pcgModnameAkftabTest);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AKFTAB_SET",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy (pcgModnameAkftabSet, pclResult);
  else
     strcpy (pcgModnameAkftabSet, "undefined");
  dbg(TRACE,"TELEX:AKFTAB_SET:<%s>", pcgModnameAkftabSet);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","TAGNAME_ALTEA_MESSAGE",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     strcpy (pcgTagnameAlteaMessage, pclResult);
  else
     strcpy (pcgTagnameAlteaMessage, "undefined");
  dbg(TRACE,"TELEX:TAGNAME_ALTEA_MESSAGE:<%s>", pcgTagnameAlteaMessage);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","KRISCOM_ERROR_MESSAGE",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy (pcgKriscomErrorMessage, pclResult);
     ReplaceChar (pcgKriscomErrorMessage,'_',' ');
  }
  else
     strcpy (pcgKriscomErrorMessage, "undefined");
  dbg(TRACE,"TELEX:KRISCOM_ERROR_MESSAGE:<%s>", pcgKriscomErrorMessage);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ALTEA_ACCEPTED_FLIGHT_STATUS",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy (pcgAcceptedAlteaFlightStatus, pclResult);
  }
  else
     strcpy (pcgAcceptedAlteaFlightStatus , "");
  dbg(TRACE,"TELEX:ALTEA_ACCEPTED_FLIGHT_STATUS:<%s>", pcgAcceptedAlteaFlightStatus);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ALTEA_ATPIN_STATUS",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy (pcgAlteaAtpinStatus, pclResult);
  }
  else
     strcpy (pcgAlteaAtpinStatus, "");
  dbg(TRACE,"TELEX:ALTEA_ATPIN_STATUS:<%s>", pcgAlteaAtpinStatus);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ALTEA_SAVE_TO_TLXTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igAlteaSaveToTlxTab = TRUE;
     }
     else
       igAlteaSaveToTlxTab = FALSE;
  else
       igAlteaSaveToTlxTab = FALSE;
  dbg(TRACE,"TELEX:ALTEA_SAVE_TO_TLXTAB :<%d>", igAlteaSaveToTlxTab);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","ALTEA_ATPIN_SAVE_TO_TLXTAB",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igAlteaAtpinSaveToTlxTab = TRUE;
     }
     else
       igAlteaAtpinSaveToTlxTab = FALSE;
  else
       igAlteaAtpinSaveToTlxTab = FALSE;
  dbg(TRACE,"TELEX:ALTEA_ATPIN_SAVE_TO_TLXTAB :<%d>", igAlteaAtpinSaveToTlxTab);


  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","MVT_EXCLUDE_INFANTS",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igMvtExcludeInfants = TRUE;
     }
     else
       igMvtExcludeInfants = FALSE;
  else
       igMvtExcludeInfants = FALSE;
  dbg(TRACE,"TELEX:MVT_EXCLUDE_INFANTS :<%d>", igMvtExcludeInfants);


  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PAL_PRID_FIXED_VALUE",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy (pcgPalPridFixedValue, pclResult);
  }
  else
     strcpy (pcgPalPridFixedValue, "undefined");
  dbg(TRACE,"TELEX:PAL_PRID_FIXED_VALUE:<%s>", pcgPalPridFixedValue);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","LDM_EXTRACT_CREW_TECH",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igLdmExtractCrewTech = TRUE;
     }
     else
       igLdmExtractCrewTech = FALSE;
  else
       igLdmExtractCrewTech = FALSE;
  dbg(TRACE,"TELEX:LDM_EXTRACT_CREW_TECH :<%d>", igLdmExtractCrewTech);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","SCORE_CONTINUE_ON_ERROR",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igScoreContinueOnError = TRUE;
     }
     else
      igScoreContinueOnError = FALSE;
  else
    igScoreContinueOnError = FALSE;
  dbg(TRACE,"TELEX:SCORE_CONTINUE_ON_ERROR :<%d>", igScoreContinueOnError);

  ilRC = iGetConfigEntry(pcpCfgFile,"SCORE","DELETE_BY_STATUSCHANGE",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igDeleteIsStatusChange = TRUE;
     }
     else
      igDeleteIsStatusChange = FALSE;
  else
    igDeleteIsStatusChange = FALSE;
  dbg(TRACE,"SCORE:DELETE_BY_STATUSCHANGE :<%d>", igDeleteIsStatusChange);

  igPtmMsgDest = 0;
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PTM_MSG_DEST",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     igPtmMsgDest = atoi (pclResult);
  }
  else
     igPtmMsgDest = 0;
  dbg(TRACE,"TELEX:PTM_MSG_DEST:<%d>", igPtmMsgDest);

  igPsmDelCAL = 0;
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PSM_DELETE_CAL",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igPsmDelCAL = TRUE;
     }
     else
      igPsmDelCAL = FALSE;
  else
    igPsmDelCAL = FALSE;
  dbg(TRACE,"TELEX:PSM_DELETE_CAL :<%d>", igPsmDelCAL);


  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","MVT_MAX_FUTURE_SECONDS",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
    igMvtMaxFutureSecs = atoi (pclResult);
  else
    igMvtMaxFutureSecs = 900; /* 15 minutes */
  dbg(TRACE,"TELEX:MVT_MAX_FUTURE_SECONDS:<%d>",igMvtMaxFutureSecs);

  /* Read config file setting On Block Times for Home Airport (MVT) */
  igSetAAOnBlockForHopo = TRUE;
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","SetAAOnBlockForHopo",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igSetAAOnBlockForHopo = FALSE;
     }
  }
  dbg(TRACE,"Set OnBlock for HOPO TELEX:SetAAOnBlockForHopo (MVT) = %d",igSetAAOnBlockForHopo);

  /* Read config file setting Off Block Times for Home Airport (MVT) */
  igSetADOffBlockForHopo = TRUE;
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","SetADOffBlockForHopo",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     if (strcmp(pclResult,"NO") == 0)
     {
        igSetADOffBlockForHopo = FALSE;
     }
  }
  dbg(TRACE,"Set OffBlock for HOPO TELEX:SetADOffBlockForHopo (MVT) = %d",igSetADOffBlockForHopo);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","AftnNoInsertForSuffix",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy (pcgAftnNoInsertForSuffix, pclResult); 
  }
  else
     strcpy (pcgAftnNoInsertForSuffix, "");
  dbg(TRACE,"TELEX:AftnNoInsertForSuffix:<%s>", pcgAftnNoInsertForSuffix);

  ilRC = iGetConfigEntry(pcpCfgFile,"MVT","TrustTlxDateArr",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igMVTTrustTlxDateArr = TRUE;
     }
     else
       igMVTTrustTlxDateArr = FALSE;
  else
     igMVTTrustTlxDateArr = FALSE;   
  dbg(TRACE,"MVT:TrustTlxDateArr :<%d>", igMVTTrustTlxDateArr);

  ilRC = iGetConfigEntry(pcpCfgFile,"MVT","TrustTlxDateDep",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igMVTTrustTlxDateDep = TRUE;
     }
     else
       igMVTTrustTlxDateDep = FALSE;
  else
     igMVTTrustTlxDateDep = FALSE;   
  dbg(TRACE,"MVT:TrustTlxDateDep :<%d>", igMVTTrustTlxDateDep);

  ilRC = iGetConfigEntry(pcpCfgFile,"APFI","CheckinAccept",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igApfiCkiAccept = TRUE;
     }
     else
       igApfiCkiAccept = FALSE;
  else
     igApfiCkiAccept = FALSE;   
  dbg(TRACE,"APFI:CheckinAccept :<%d>", igApfiCkiAccept);

  ilRC = iGetConfigEntry(pcpCfgFile,"PTM","AcceptDepFromHopo",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igPtmAcceptDepFromHopo = TRUE;
     }
     else
       igPtmAcceptDepFromHopo = FALSE;
  else
     igPtmAcceptDepFromHopo = FALSE;
  dbg(TRACE,"PTM:AcceptDepFromHopo :<%d>", igPtmAcceptDepFromHopo);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX",
    "EXCLUDE_ALC_APC3_IN_TOTAL_TRANSFER",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
     strcpy (pcgExcludeAlcApc3InTotalTransfer,pclResult);
     PtmBaggageParseAirlineDestExclusion ();
  }
  else
     strcpy (pcgExcludeAlcApc3InTotalTransfer, "");
  dbg(TRACE,"TELEX:EXCLUDE_ALC_APC3_IN_TOTAL_TRANSFER:<%s>",
    pcgExcludeAlcApc3InTotalTransfer);

  ilRC = iGetConfigEntry(pcpCfgFile,"LDM","TrustTlxDateArr",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igLDMTrustTlxDateArr = TRUE;
     }
     else
       igLDMTrustTlxDateArr = FALSE;
  else
     igLDMTrustTlxDateArr = FALSE;   
  dbg(TRACE,"LDM:TrustTlxDateArr :<%d>", igLDMTrustTlxDateArr);

  ilRC = iGetConfigEntry(pcpCfgFile,"LDM","TrustTlxDateDep",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igLDMTrustTlxDateDep = TRUE;
     }
     else
       igLDMTrustTlxDateDep = FALSE;
  else
     igLDMTrustTlxDateDep = FALSE;   
  dbg(TRACE,"LDM:TrustTlxDateDep :<%d>", igLDMTrustTlxDateDep);

  igPalDpxAbsrAllowCal = 0;
  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","PAL_DPX_ABSR_ALLOW_CAL",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igPalDpxAbsrAllowCal = TRUE;
     }
     else
       igPalDpxAbsrAllowCal = FALSE;
  else
     igPalDpxAbsrAllowCal = FALSE;
  dbg(TRACE,"TELEX:PAL_DPX_ABSR_ALLOW_CAL :<%d>", igPalDpxAbsrAllowCal); 

  igLDMCheckFlightDateArr = FALSE;
  ilRC = iGetConfigEntry(pcpCfgFile,"LDM","CheckFlightDateArr",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igLDMCheckFlightDateArr = TRUE;
     }
     else
       igLDMCheckFlightDateArr = FALSE;
  else
     igLDMCheckFlightDateArr = FALSE;   
  dbg(TRACE,"LDM:CheckFlightDateArr :<%d>", igLDMCheckFlightDateArr);

  igLDMCheckFlightDateDep = FALSE;
  ilRC = iGetConfigEntry(pcpCfgFile,"LDM","CheckFlightDateDep",CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
     if (strcmp(pclResult,"YES") == 0)
     {
       igLDMCheckFlightDateDep = TRUE;
     }
     else
       igLDMCheckFlightDateDep = FALSE;
  else
     igLDMCheckFlightDateDep = FALSE;   
  dbg(TRACE,"LDM:CheckFlightDateDep :<%d>", igLDMCheckFlightDateDep);

  ilRC = iGetConfigEntry(pcpCfgFile,"TELEX","GET_FLIGHT_DATE_FUTURE_DAYS",
    CFG_STRING,pclResult);
  if (ilRC == RC_SUCCESS)
  {
    igGetFlightDate_FutureDays = atoi (pclResult);
    if ((igGetFlightDate_FutureDays < 2) || (igGetFlightDate_FutureDays > 15))
    {
      dbg(TRACE,"TELEX:GET_FLIGHT_DATE_FUTURE_DAYS corrected from <%d> to 2",
        atoi (pclResult));
      igGetFlightDate_FutureDays = 2; /* original hard coded value */
    }
  }
  else
    igGetFlightDate_FutureDays = 2;
  dbg(TRACE,"TELEX:GET_FLIGHT_DATE_FUTURE_DAYS <%d>", igGetFlightDate_FutureDays);

  return ilRC; 
  /* Make sure any new configuration is added ABOVE the return statement ! */

} /* end of ReadConfig  */





static int AddCfgElement(char *pcpPtr, char *pcpCfgFile, T_CFG_ARRAY_ELEMENT *prpCfgArrayEPtr)
{
  int ilRC,ilHit;
  char pclFunc[]="AddCfgElement:";
  char  *pclPtr;
  char pclResult[100];
  char pclResult2[100];
  CCS_KEY *prlMasterCfg;
  T_FIELDLIST prlField;

  pclPtr = pcpPtr;

  prlMasterCfg = NULL;
  prlMasterCfg = CCSArrayInit(prlMasterCfg,NULL,FIELDLIST_KEY
                  ,(CCS_COMPARE*) &CompareFieldList
                  ,(CCS_KEY_PROC*) &GetFieldListKeyData
                  ,MAX_TLX_ARR,ADD_TLX_ARR,&ilRC);

  ilHit = RC_FAIL;
  while (*pclPtr != '\0')
    {
      GetNextDataItem(pclResult,&pclPtr,",","","  ");
      ilRC = iGetConfigRow(pcpCfgFile,prpCfgArrayEPtr->CfgType,pclResult,CFG_STRING
                 ,pclResult2);
      if(ilRC == RC_SUCCESS )
    {
      if (strlen(pclResult2) > 0)
        {
          strcpy(prlField.FName,pclResult2);
          strcpy(prlField.DName,pclResult);
          dbg(DEBUG,"%s Cfg Field <%s> is <%s>",pclFunc,
          pclResult,pclResult2);
          CCSArrayAddUnsort(prlMasterCfg,&prlField,FIELDLIST);
          ilHit = RC_SUCCESS;
        } else {
          /* No field added to cfg array */
          dbg(DEBUG,"%s Cfg Field <%s> empty",pclFunc,pclResult);

          /* If there is no recv no message could be sent*/
          if(strcmp(pclResult,"Recv") == 0)
        {
          dbg(TRACE,"%s Error: No receiver defined (see cfg file) ",pclFunc);
          CCSArrayArrDestroy(&prlMasterCfg);
          ilHit = RC_FAIL;
          break;
        }
          else if(strcmp(pclResult,"Command") == 0)
        {
          dbg(TRACE,"%s Error: No Command defined (see cfg file) ",pclFunc);
          CCSArrayArrDestroy(&prlMasterCfg);
          ilHit = RC_FAIL;
        }
        } /* end of if */
    } /* end of if */
      else
    {
      dbg(TRACE,"%s Cfg Field <%s> not found in cfg file",pclFunc,pclResult);

      /* If there is no recv no message could be sent*/
      if(strcmp(pclResult,"Recv") == 0)
        {
          dbg(TRACE,"%s Error: No receiver defined (see cfg file) ",pclFunc);
          CCSArrayArrDestroy(&prlMasterCfg);
          ilHit = RC_FAIL;
          break;
        }
    }
    } /* end of while */

  if(ilHit == RC_SUCCESS)
    prpCfgArrayEPtr->MasterKey = prlMasterCfg;

  return RC_SUCCESS;

}/* end of AddCfgElement */




static int MakeCfgFile()
{
  int i,ilCount;
  char pclFunc[]="MakeCfgFile:";
  char pclList[100];
  char pclCfgFile[100];
  char pclResult[100];
  char pclCmd[100];
  char *pclPtr;
  FILE *flFilehdl;

  memset(pclList,0,sizeof(pclList));

  sprintf(pclCfgFile,"%s/%s.cfg.%s", getenv("CFG_PATH"),mod_name,GetTimeStamp());
  dbg(DEBUG,"%s Create new cfg file <%s>",pclFunc,pclCfgFile);

  flFilehdl = fopen(pclCfgFile,"w");

  if(flFilehdl != NULL)
    {
      fprintf(flFilehdl,"[GLOBAL]\n");
      fprintf(flFilehdl,"* Debug Level [OFF|TRACE|DEBUG] default TRACE\n");
      fprintf(flFilehdl,"debug_level = OFF\n");
      fprintf(flFilehdl,"[GLOBAL_END]\n");

      fprintf(flFilehdl,"[TELEX]\n");

      /* Get all Telex commands  */
      for(i=0;prgTlxCmds[i].function != -1;i++)
    {
      GetDataItem(pclResult,prgTlxCmds[i].SynTlx,1,',',"","  ");
      if(i>0)
        strcat(pclList,",");
      strcat(pclList,&pclResult[1]);
    }/*  end of while */
      ilCount = i;
      strcat(pclList,"\0");

      dbg(DEBUG,"%s Telex command list <%s> ",pclFunc,pclList);
      fprintf(flFilehdl,"* Telex commands in search order \n");
      fprintf(flFilehdl,"commands = %s\n",pclList);
      fprintf(flFilehdl,"* Entries for complete a Telex\n");
      fprintf(flFilehdl,"complete = TLX\n");
      fprintf(flFilehdl,"* Garbage collection time span\n");
      fprintf(flFilehdl,"garbage = 1\n");
      fprintf(flFilehdl,"* Save interpr. Results [ON|OFF] default OFF\n");
      fprintf(flFilehdl,"* in $DBG_PATH/tlxres'pid'.trc\n");
      fprintf(flFilehdl,"Tlxres = OFF\n");
      fprintf(flFilehdl,"* Save all Telex in $DBG_PATH/modname`date`.trc\n");
      fprintf(flFilehdl,"TTrace = OFF\n");
      fprintf(flFilehdl,"* Size of Tracefile in byte\n");
      fprintf(flFilehdl,"TraceSize = 1000000\n");

      fprintf(flFilehdl,"* More Telexes (e.g. MVT and LDM) in one [ON|OFF]\n");
      fprintf(flFilehdl,"MultiTlx = OFF\n");

      fprintf(flFilehdl,"[TELEX_END]\n\n");


      /* Insert internal variable names for commands  */
      for(i=1;  i <= ilCount ; i++ )
    {
      GetDataItem(pclCmd,pclList,i,',',"","  ");
      fprintf(flFilehdl,"[%s]\n",pclCmd);

      switch(prgTlxCmds[i-1].function)
        {
        case FDI_MVT:
          pclPtr = pcgMVTVars;
          break;
        case FDI_PTM:
          pclPtr = pcgPTMVars;
          break;
        case FDI_LDM:
          pclPtr = pcgLDMVars;
          break;
        case FDI_CPM:
          pclPtr = pcgCPMVars;
          break;
        case FDI_RQM:
          pclPtr = pcgRQMVars;
          break;
        case FDI_DIV:
          pclPtr = pcgDIVVars;
          break;
        case FDI_TPM:
          pclPtr = pcgTPMVars;
          break;
        case FDI_PSM:
          pclPtr = pcgPSMVars;
          break;
        case FDI_ETM:
          pclPtr = pcgETMVars;
          break;
        case FDI_FWD:
          pclPtr = pcgFWDVars;
          break;
        case FDI_SCR:
          pclPtr = pcgSCRVars;
          break;
        case FDI_BTM:
          pclPtr = pcgBTMVars;
          break;
        case FDI_UCM:
          pclPtr = pcgUCMVars;
          break;
        case FDI_DLS:
          pclPtr = pcgDLSVars;
          break;
        case FDI_AFTN:
          pclPtr = pcgAFTNVars;
          break;
        case FDI_KRISCOM:
          pclPtr = pcgKRISCOMVars;
          break;
        case FDI_ALTEA:
          pclPtr = pcgALTEAVars;
          break;
        case FDI_ADF:
          pclPtr = pcgADFVars;
          break;
        case FDI_ASDI:
          pclPtr = pcgASDIVars;
          break;
        case FDI_MAN:
          pclPtr = pcgMANVars;
          break;
        case FDI_SCORE:
          pclPtr = pcgSCOREVars;
          break;
        case FDI_FFM:
          pclPtr = pcgFFMVars;
          break;
        case FDI_DFS:
          pclPtr = pcgDFSVars;
          break;
        case FDI_UBM:
          pclPtr = pcgUBMVars;
          break;
        case FDI_CCM:
          pclPtr = pcgCCMVars;
          break;
        case FDI_BAY:
          pclPtr = pcgBAYVars;
          break;
        case FDI_PAL:
          pclPtr = pcgPALVars;
          break;
        case FDI_CAL:
          pclPtr = pcgCALVars;
          break;
        case FDI_PSC:
          pclPtr = pcgPSCVars;
          break;
        case FDI_DPFI:
        case FDI_APFI:
          pclPtr = pcgDPIVars;
          break;
        default:
          pclPtr = pcgOthersVars;
        }
      if(pclPtr != NULL)
        {
          while(GetNextDataItem(pclResult,&pclPtr,",","","  ") > 0)
        fprintf(flFilehdl,"%s = \n",pclResult);
        }
      fprintf(flFilehdl,"[%s_END]\n\n",pclCmd);
    }
      /*  Insert internal variable names for complete*/
      fprintf(flFilehdl,"[TLX]\n");
      pclPtr = pcgCmpTlx;
      while(GetNextDataItem(pclResult,&pclPtr,",","","  ") > 0)
    fprintf(flFilehdl,"%s = \n",pclResult);
      fprintf(flFilehdl,"[TLX_END]\n\n");

      fprintf(flFilehdl,"[REREAD]\n");
      fprintf(flFilehdl,"table = \n");
      fprintf(flFilehdl,"txt1 = \n");
      fprintf(flFilehdl,"txt2 = \n");
      fprintf(flFilehdl,"recv = \n");
      fprintf(flFilehdl,"time_field = \n");
      fprintf(flFilehdl,"selection = \n");
      fprintf(flFilehdl,"start_time = \n");
      fprintf(flFilehdl,"end_time = \n");
      fprintf(flFilehdl,"header_typ = \n");
      fprintf(flFilehdl,"hopo = \n");
      fprintf(flFilehdl,"[REREAD_END]\n");


      fflush(flFilehdl);
      fclose(flFilehdl);
    }
  else
    {
      dbg(TRACE,"%s Error: ",pclFunc,strerror(errno));
    }

  return RC_SUCCESS;
} /* end of MakeCfgFile */



/******************************************************************************/
/******************************************************************************/
static void StoreApxValue(char *pcpTlxKey, char *pcpApxType, char *pcpApxValu)
{
  int ilRec = 0;
  int ilLoop = 0;
  int ilLen = 0;
  char pclApxCode[32];
  char *pclApxPtr = NULL;
  pclApxCode[0] = 0x00;
  pclApxPtr = strstr(pcpApxType, "APX.");
  if (pclApxPtr != NULL)
  {
    pclApxPtr += 4;
    ilLen = GetNextDataItem(pclApxCode,&pclApxPtr,",","","  ");
    if (ilLen > 0)
    {
      while ((ilLoop < prgApxValues[0].Used) && (ilRec < 1))
      {
        ilLoop++;
        if (prgApxValues[ilLoop].Used == FALSE)
        {
          ilRec = ilLoop;
        } /* end if */
      } /* end while */
      if ((ilRec < 1) && (prgApxValues[0].Used < MaxApxRecs))
      {
        prgApxValues[0].Used++;
        ilRec = prgApxValues[0].Used;
      } /* end if */
      if (ilRec > 0)
      {
        strcpy(prgApxValues[ilRec].TlxKey,pcpTlxKey);
        strcpy(prgApxValues[ilRec].ApxType,pclApxCode);
        strcpy(prgApxValues[ilRec].ApxValue,pcpApxValu);
        prgApxValues[ilRec].Used = TRUE;
      } /* end if */
    } /* end if */
  } /* end if */
  dbg(TRACE,"GOT APX REC %d <%s> <%s> <%s>", ilRec,pcpTlxKey,pclApxCode,pcpApxValu);
  return;
} /* end StoreApxValue */

/******************************************************************************/
/******************************************************************************/
static void DeployApxValue(char *pcpTlxKey, char *pcpAftUrno)
{
  int ilRC = RC_SUCCESS;
  int ilRec = 0;
  int ilCmp = 0;
  char pclApxFields[] = "FLNU,TYPE,PAXC,USEC,CDAT,USEU,LSTU,RURN";
  char pclApxData[1024];
  char pclSqlBuf[1024];
  int ilDeleteDone;

  ilDeleteDone = FALSE;

  if (igLdmCircularFlight == TRUE)
  {
    dbg(TRACE,"DeployApxValue: do not save to APXTAB for FML circular LDM telex");
    return;
  }

  while (ilRec < prgApxValues[0].Used)
  {
     ilRec++;
     if (prgApxValues[ilRec].Used == TRUE)
     {
        ilCmp = strcmp(pcpTlxKey,prgApxValues[ilRec].TlxKey);
        dbg(TRACE,"CMP KEY %d <%s> <%s> %d",ilRec,pcpTlxKey,prgApxValues[ilRec].TlxKey,ilCmp);
        if (ilCmp == 0)
        {
           dbg(TRACE,"FOUND APX ENTRY");
           sprintf(pclApxData,"%s,%s,%s,FDIHDL, ,FDIHDL, ,",pcpAftUrno,
                   prgApxValues[ilRec].ApxType,prgApxValues[ilRec].ApxValue);
           if (strlen(pcgNextTlxUrno) > 0)
           {
              strcat(pclApxData,pcgNextTlxUrno);
           }
           else
           {
              strcat(pclApxData,"0");
           }
/*
       ilRC = SendCedaEvent(7150,0,pcgDestName,pcgRecvName," ",pcgTwEndNew,
                                "WRD","APXTAB","FLNU,TYPE",
                                pclApxFields,pclApxData,"",4,NETOUT_NO_ACK);
*/
           if (ilDeleteDone == FALSE)
           {
              if (strlen(pcpAftUrno) > 2)
              {
                 sprintf(pclSqlBuf,"DELETE FROM APXTAB WHERE FLNU = %s",pcpAftUrno);
                 ilRC = FdiSendSql(pclSqlBuf);
              }
              ilDeleteDone = TRUE;
           }
           ilRC = FdiHandleSql("IBT","APXTAB","",pclApxFields,pclApxData,
                               pcgTwStart,pcgTwEndNew,FALSE,FALSE);
           prgApxValues[ilRec].Used = FALSE;
        } /* end if */
        else
        {
           if (ilCmp > 0)
           {
              dbg(TRACE,"APX ENTRY IS TOO OLD");
              prgApxValues[ilRec].Used = FALSE;
           } /* end else if */
           if (ilRec == prgApxValues[0].Used)
           {
              prgApxValues[0].Used--;
           }
        } /* end if */
     } /* end if */
  } /* end while */
  return;
} /* end DeployApxValue */

/******************************************************************************/
/******************************************************************************/
static void CheckCrLf(char *pcpText)
{
  char *pclPtr1 = NULL;
  char *pclPtr2 = NULL;
  pclPtr1 = pcpText;
  pclPtr2 = pclPtr1 + 1;
  while (*pclPtr1 != '\0')
  {
    if ((*pclPtr1 == '\r') && (*pclPtr2 != '\n'))
    {
      *pclPtr1 = '\n';
    } /* end if */
    pclPtr1++;
    pclPtr2++;
  } /* end while */
  return;
} /* end CheckCrLf */



static int SearchFlight(char *pcpFields, char *pcpData, char *pcpUrno, char *pcpNewFields,
                        char *pcpNewData, int ipTlxCmd)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[1024];
  char pclTmpBuf[1024];
  char pclSqlBufPal[1024];
  char pclSelectBufPal[1024];
  int ilCount;
  int ilI;
  int ilJ;
  char pclFieldName[8];
  char pclDataItem[128];
  char pclNewFieldList[1024];
  char pclNewDataList[1024];
  char pclAlc3[8] = "";
  char pclFltn[16] = "";
  char pclFlns[8] = "";
  char pclOrg3[8] = "";
  char pclDes3[8] = "";
  char pclFlda[16] = "";
  int ilLen;
  char pclAftUrno[32];
  char pclAftFlno[32];
  char pclAftTifa[32];
  char pclAftTifd[32];
  char pclAftStoa[32];
  char pclAftStod[32];
  char pclAftAdid[32];
  char pclAftFtyp[32];
  char pclAftVian[32];
  char pclAftVial[1024];
  char pclAftOrg3[32];
  char pclAftDes3[32];
  char pclAftVia3[32];
  char pclAftFlda[32];
  char pclAftRegn[32];
  int ilSendRequest;
  int ilIgnoreItem;
  char pclArrStart[32];
  char pclArrEnd[32];
  char pclDepStart[32];
  char pclDepEnd[32];
  int ilHit;
  int ilNoDest;
  char pclDestList[10][8];
  int ilRemTlx;
  int ilRemFlt;
  int ilRejected;
  T_TLXRESULT prlTlxResult;
  char pclAftAlc3[32];
  char pclAftFltn[32];
  char pclAftFlns[32];
  char pclTmpTime[32];
  char pclCurTime[32];

  cgTlxStatus = FDI_STAT_INTERPRETED;

  strcpy(pcpUrno,"");
  strcpy(pcpNewFields,"");
  strcpy(pcpNewData,"");
  /* strcpy(pcgFlightSchedTime,""); */

  strcpy(pclArrStart,GetTimeStamp());
  strcpy(pclArrEnd,pclArrStart);
  strcpy(pclDepStart,pclArrStart);
  strcpy(pclDepEnd,pclArrStart);
  ilRC = AddSecondsToCEDATime(pclArrStart,igArrWindowStart*60,1);
  ilRC = AddSecondsToCEDATime(pclArrEnd,igArrWindowEnd*60,1);
  ilRC = AddSecondsToCEDATime(pclDepStart,igDepWindowStart*60,1);
  ilRC = AddSecondsToCEDATime(pclDepEnd,igDepWindowEnd*60,1);

  strcpy(pclNewFieldList,"");
  strcpy(pclNewDataList,"");
  ilCount = GetNoOfElements(pcpFields,',');
  for (ilI = 1; ilI <= ilCount; ilI++)
  {
     ilIgnoreItem = FALSE;
     GetDataItem(pclFieldName,pcpFields,ilI,',',""," ");
     if (strcmp(pclFieldName,"ALC3") == 0)
     {
        GetDataItem(pclAlc3,pcpData,ilI,',',""," ");
        strcpy(pcgAlc3ForPRM,pclAlc3);
        ilIgnoreItem = TRUE;
     }
     if (strcmp(pclFieldName,"FLTN") == 0)
     {
        GetDataItem(pclFltn,pcpData,ilI,',',""," ");
        ilIgnoreItem = TRUE;
     }
     if (strcmp(pclFieldName,"FLNS") == 0)
     {
        GetDataItem(pclFlns,pcpData,ilI,',',""," ");
        ilIgnoreItem = TRUE;
     }
     if (strcmp(pclFieldName,"ORG3") == 0)
     {
        GetDataItem(pclOrg3,pcpData,ilI,',',""," ");
        ilIgnoreItem = TRUE;
     }
     if (strcmp(pclFieldName,"DES3") == 0)
     {
        GetDataItem(pclDes3,pcpData,ilI,',',""," ");
        ilIgnoreItem = TRUE;
     }
     if (strcmp(pclFieldName,"#FDAY") == 0)
     {
        GetDataItem(pclFlda,pcpData,ilI,',',""," ");
        ilIgnoreItem = TRUE;
     }
     if (ilIgnoreItem == FALSE)
     {
        GetDataItem(pclDataItem,pcpData,ilI,',',""," ");
        strcat(pclNewFieldList,pclFieldName);
        strcat(pclNewFieldList,",");
        strcat(pclNewDataList,pclDataItem);
        strcat(pclNewDataList,",");
     }
  }
  dbg(DEBUG,"SearchFlight: Found data ALC3=<%s> FLTN=<%s> FLNS=<%s> ORG3=<%s> DES3=<%s>",
      pclAlc3,pclFltn,pclFlns,pclOrg3,pclDes3);
  if (strlen(pclNewFieldList) > 0)
  {
     pclNewFieldList[strlen(pclNewFieldList)-1] = '\0';
     pclNewDataList[strlen(pclNewDataList)-1] = '\0';
  }
  dbg(DEBUG,"SearchFlight: New Field List <%s>",pclNewFieldList);
  dbg(DEBUG,"SearchFlight: New Data List <%s>",pclNewDataList);
  strcpy(pcpNewFields,pclNewFieldList);
  strcpy(pcpNewData,pclNewDataList);

  for (ilI = 0; ilI < igNoDestinations; ilI++)
  {
     dbg(DEBUG,"SearchFlight: Tlx Destination %d: <%s>",ilI+1,&pcgDestList[ilI][0]);
  }

  ilSendRequest = TRUE;
  strcpy(pclSqlBuf,"SELECT URNO,FLNO,TIFA,TIFD,STOA,STOD,ADID,FTYP,PAX1,PAX2,PAX3,BAGW,DES3,VIAN,VIAL,ONBL,LAND,AIRB,ALC3,FLTN,FLNS,ORG3,VIA3,FLDA,REGN FROM AFTTAB WHERE ");
  strcpy(pclSqlBufPal,pclSqlBuf);
  strcpy(pclSelectBufPal,"");
  if (strlen(pcgAftUrnoFromFDIT) > 0)
  {
     sprintf(pclSelectBuf,"URNO = %s",pcgAftUrnoFromFDIT);
     strcat(pclSqlBuf,pclSelectBuf);
  }
  else
  {
     strcpy(pclTmpBuf,"");
     if (strlen(pclOrg3) > 0 && strlen(pclDes3) > 0)
     {
        if (strcmp(pclOrg3,pcgHomeAP) == 0 && strcmp(pclDes3,pcgHomeAP) == 0)
        {
           if (igTrustTlxDate == FALSE)
              sprintf(pclSelectBuf,"(TIFA BETWEEN '%s' AND '%s' OR TIFD BETWEEN '%s' AND '%s')",
                      pclArrStart,pclArrEnd,pclDepStart,pclDepEnd);
           else
           {
              sprintf(pclSelectBuf,"(TIFA LIKE '%s%%'OR TIFD LIKE '%s%%')",
                      pcgFltDate,pcgFltDate);
              sprintf(pclSelectBufPal,"(STOA LIKE '%s%%'OR STOD LIKE '%s%%')",
                      pcgFltDate,pcgFltDate);
           }
           sprintf(pclTmpBuf," AND ORG3 = '%s' AND DES3 = '%s'",pclOrg3,pclDes3);
        }
        else
        {
           if (strcmp(pclOrg3,pcgHomeAP) == 0)
           {
              if (strstr(pclNewFieldList,"LNDD") != NULL ||
                  strstr(pclNewFieldList,"ONBD") != NULL)
              {
                 if (igTrustTlxDate == FALSE)
                    sprintf(pclSelectBuf,"TIFA BETWEEN '%s' AND '%s'",
                            pclDepStart,pclDepEnd);
                 else /* shouldn't this be instead "TIFD LIKE" ? */
                    sprintf(pclSelectBuf,"TIFA LIKE '%s%%'",pcgFltDate);
              }
              else
              {
                 if (igTrustTlxDate == FALSE)
                    sprintf(pclSelectBuf,"TIFD BETWEEN '%s' AND '%s'",
                            pclDepStart,pclDepEnd);
                 else
                    sprintf(pclSelectBuf,"TIFD LIKE '%s%%'",pcgFltDate);
              }
              sprintf(pclTmpBuf," AND ORG3 = '%s' AND (DES3 = '%s' OR VIA3 = '%s')",
                      pclOrg3,pclDes3,pclDes3);
           }
           else
           {
              if (strcmp(pclDes3,pcgHomeAP) == 0)
              {
                 if (igTrustTlxDate == FALSE)
                    sprintf(pclSelectBuf,"TIFA BETWEEN '%s' AND '%s'",
                            pclArrStart,pclArrEnd);
                 else
                 {
                    sprintf(pclSelectBuf,"TIFA LIKE '%s%%'",pcgFltDate);
                    sprintf(pclSelectBufPal,"STOD LIKE '%s%%'",pcgFltDate);
                 }
                 if (ipTlxCmd == FDI_PTM && igAccPTMFromOriginVia == TRUE)
                    sprintf(pclTmpBuf," AND DES3 = '%s'",pclDes3);
                 else
                    sprintf(pclTmpBuf," AND (ORG3 = '%s' OR VIA3 = '%s') AND DES3 = '%s'",
                            pclOrg3,pclOrg3,pclDes3);
              }
              else
              {
                 cgTlxStatus = FDI_STAT_OTHER_AIRPORT;
                 ilSendRequest = FALSE;
              }
           }
        }
     }
     else
     {
        if (strlen(pclOrg3) > 0)
        {
           if (strcmp(pclOrg3,pcgHomeAP) == 0)
           {
              if (strstr(pclNewFieldList,"LNDD") != NULL ||
                  strstr(pclNewFieldList,"ONBD") != NULL)
              {
                 if (igTrustTlxDate == FALSE)
                    sprintf(pclSelectBuf,"TIFA BETWEEN '%s' AND '%s'",
                            pclDepStart,pclDepEnd);
                 else
                    sprintf(pclSelectBuf,"TIFA LIKE '%s%%'",pcgFltDate);
              }
              else
              {
                 if (igTrustTlxDate == FALSE)
                    sprintf(pclSelectBuf,"TIFD BETWEEN '%s' AND '%s'",
                            pclDepStart,pclDepEnd);
                 else
                    sprintf(pclSelectBuf,"TIFD LIKE '%s%%'",pcgFltDate);
              }
              sprintf(pclTmpBuf," AND ORG3 = '%s'",pclOrg3);
           }
           else
           {
              if (igTrustTlxDate == FALSE)
                 sprintf(pclSelectBuf,"TIFA BETWEEN '%s' AND '%s'",
                         pclArrStart,pclArrEnd);
              else
              {
                 sprintf(pclSelectBuf,"TIFA LIKE '%s%%'",pcgFltDate);
                 sprintf(pclSelectBufPal,"STOD LIKE '%s%%'",pcgFltDate);
              }
              sprintf(pclTmpBuf," AND (ORG3 = '%s' OR VIA3 = '%s') AND DES3 = '%s'",
                      pclOrg3,pclOrg3,pcgHomeAP);
           }
        }
        else
        {
           if (strlen(pclDes3) > 0)
           {
              if (strcmp(pclDes3,pcgHomeAP) == 0)
              {
                 if (igTrustTlxDate == FALSE)
                    sprintf(pclSelectBuf,"TIFA BETWEEN '%s' AND '%s'",
                            pclArrStart,pclArrEnd);
                 else
                 {
                    sprintf(pclSelectBuf,"TIFA LIKE '%s%%'",pcgFltDate);
                    sprintf(pclSelectBufPal,"STOD LIKE '%s%%'",pcgFltDate);
                 }
                 sprintf(pclTmpBuf," AND DES3 = '%s'",pclDes3);
              }
              else
              {
                 if (strstr(pclNewFieldList,"LNDD") != NULL ||
                     strstr(pclNewFieldList,"ONBD") != NULL)
                 {
                    if (igTrustTlxDate == FALSE)
                       sprintf(pclSelectBuf,"TIFA BETWEEN '%s' AND '%s'",
                               pclDepStart,pclDepEnd);
                    else
                       sprintf(pclSelectBuf,"TIFA LIKE '%s%%'",pcgFltDate);
                 }
                 else
                 {
                    if (igTrustTlxDate == FALSE)
                       sprintf(pclSelectBuf,"TIFD BETWEEN '%s' AND '%s'",
                               pclDepStart,pclDepEnd);
                    else
                       sprintf(pclSelectBuf,"TIFD LIKE '%s%%'",pcgFltDate);
                 }
                 sprintf(pclTmpBuf," AND ORG3 = '%s' AND (DES3 = '%s' OR VIA3 = '%s')",
                         pcgHomeAP,pclDes3,pclDes3);
              }
           }
           else
           {
              if (rgTlxInfo.TlxCmd == FDI_CPM)
              {
                 if (igTrustTlxDate == FALSE)
                    sprintf(pclSelectBuf,"(TIFA BETWEEN '%s' AND '%s' OR TIFD BETWEEN '%s' AND '%s')",
                            pclArrStart,pclArrEnd,pclDepStart,pclDepEnd);
                 else
                    sprintf(pclSelectBuf,"(TIFA LIKE '%s%%' OR TIFD LIKE '%s%%')",
                            pcgFltDate,pcgFltDate);
                 strcpy(pclTmpBuf,"");
              }
              else
              {
                 cgTlxStatus = FDI_STAT_ERROR;
                 ilSendRequest = FALSE;
                 dbg(DEBUG,"SearchFlight: ORG3 and DES3 missing --> No Search");
              }
           }
        }
     }
     strcat(pclSelectBuf,pclTmpBuf);
     if (strlen(pclSelectBufPal) > 0)
        strcat(pclSelectBufPal,pclTmpBuf);
     if ((ipTlxCmd == FDI_SAM || ipTlxCmd == FDI_SLC || ipTlxCmd == FDI_SRM) &&
         strlen(pcgCsgn) > 0)
     {
        sprintf(pclTmpBuf," AND CSGN = '%s'",pcgCsgn);
        strcat(pclSelectBuf,pclTmpBuf);
        if (strlen(pclSelectBufPal) > 0)
           strcat(pclSelectBufPal,pclTmpBuf);
     }
     else
     {
        strcpy(pclTmpBuf,"");
        if (strlen(pclAlc3) > 0)
        {
           sprintf(pclTmpBuf," AND ALC3 = '%s'",pclAlc3);
        }
        else
        {
           cgTlxStatus = FDI_STAT_ERROR;
           ilSendRequest = FALSE;
           dbg(DEBUG,"SearchFlight: ALC3 missing --> No Search");
        }
        strcat(pclSelectBuf,pclTmpBuf);
        if (strlen(pclSelectBufPal) > 0)
           strcat(pclSelectBufPal,pclTmpBuf);
        strcpy(pclTmpBuf,"");
        if (strlen(pclFltn) > 0)
        {
           sprintf(pclTmpBuf," AND FLTN = '%s'",pclFltn);
        }
        else
        {
           cgTlxStatus = FDI_STAT_ERROR;
           ilSendRequest = FALSE;
           dbg(DEBUG,"SearchFlight: FLTN missing --> No Search");
        }
        strcat(pclSelectBuf,pclTmpBuf);
        if (strlen(pclSelectBufPal) > 0)
           strcat(pclSelectBufPal,pclTmpBuf);
        strcpy(pclTmpBuf,"");
        if (strlen(pclFlns) > 0)
        {
           sprintf(pclTmpBuf," AND FLNS = '%s'",pclFlns);
        }
        else
        {
           sprintf(pclTmpBuf," AND FLNS = ' '");
        }
        strcat(pclSelectBuf,pclTmpBuf);
        if (strlen(pclSelectBufPal) > 0)
           strcat(pclSelectBufPal,pclTmpBuf);
     }
     strcat(pclSelectBuf," AND FTYP NOT IN ('T','G')");
     if (strlen(pcgRegnForFlightSearch) > 0)
     {
        sprintf(pclTmpBuf," AND (REGN = '%s' OR REGN = ' ') ORDER BY REGN DESC",pcgRegnForFlightSearch);
        strcat(pclSelectBuf,pclTmpBuf);
     }
     if (strlen(pclSelectBufPal) > 0)
        strcat(pclSelectBufPal," AND FTYP NOT IN ('T','G')");
     strcat(pclSqlBuf,pclSelectBuf);
     if (strlen(pclSelectBufPal) > 0)
        strcat(pclSqlBufPal,pclSelectBufPal);
     if (igIgnoreOldTelexes == TRUE && igTlxTooOld == TRUE)
     {
        cgTlxStatus = FDI_STAT_TLX_TOO_OLD;
        ilSendRequest = FALSE;
        dbg(DEBUG,"SearchFlight: Telex is too old");
     }
  }
  if (ilSendRequest == TRUE)
  {
     if (strlen(pclSelectBufPal) > 0)
     {
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"SearchFlight: <%s>",pclSqlBufPal);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBufPal,pclDataBuf);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
           strcpy(pclSqlBuf,pclSqlBufPal);
     }
     ilHit = 0;
     ilRejected = FALSE;
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"SearchFlight: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",25,",");
        ilLen = get_real_item(pclAftUrno,pclDataBuf,1);
        ilLen = get_real_item(pclAftFlno,pclDataBuf,2);
        ilLen = get_real_item(pclAftTifa,pclDataBuf,3);
        ilLen = get_real_item(pclAftTifd,pclDataBuf,4);
        ilLen = get_real_item(pclAftStoa,pclDataBuf,5);
        ilLen = get_real_item(pclAftStod,pclDataBuf,6);
        ilLen = get_real_item(pclAftAdid,pclDataBuf,7);
        ilLen = get_real_item(pclAftFtyp,pclDataBuf,8);
        ilLen = get_real_item(pcgAftPax1,pclDataBuf,9);
        ilLen = get_real_item(pcgAftPax2,pclDataBuf,10);
        ilLen = get_real_item(pcgAftPax3,pclDataBuf,11);
        ilLen = get_real_item(pcgAftBagw,pclDataBuf,12);
        ilLen = get_real_item(&pclDestList[0][0],pclDataBuf,13);
        ilLen = get_real_item(pclAftVian,pclDataBuf,14);
        ilLen = get_real_item(pclAftVial,pclDataBuf,15);
        ilLen = get_real_item(pcgAftOnbl,pclDataBuf,16);
        ilLen = get_real_item(pcgAftLand,pclDataBuf,17);
        ilLen = get_real_item(pcgAftAirb,pclDataBuf,18);
        ilLen = get_real_item(pclAftAlc3,pclDataBuf,19);
        ilLen = get_real_item(pclAftFltn,pclDataBuf,20);
        ilLen = get_real_item(pclAftFlns,pclDataBuf,21);
        ilLen = get_real_item(pclAftOrg3,pclDataBuf,22);
        ilLen = get_real_item(pclAftVia3,pclDataBuf,23);
        ilLen = get_real_item(pclAftFlda,pclDataBuf,24);
        ilLen = get_real_item(pclAftRegn,pclDataBuf,25);
        strcpy(pclAftDes3,&pclDestList[0][0]);
        dbg(DEBUG,"SearchFlight: Found Flight \nADID = <%s>\nURNO = <%s>\nFLNO = <%s>\nTIFA/TIFD = <%s>/<%s>\nSTOA/STOD = <%s>/<%s>\nFLDA = <%s>",
            pclAftAdid,pclAftUrno,pclAftFlno,pclAftTifa,pclAftTifd,pclAftStoa,pclAftStod,pclAftFlda);
        strcpy(pcgAftAdid,pclAftAdid);
        strcpy(pcpUrno,pclAftUrno);
        strcpy(pcpNewFields,pclNewFieldList);
        strcpy(pcpNewData,pclNewDataList);
        strcpy(pcgFlightNumber,pclAftFlno);
        if (strcmp(pclAftAdid,"A") == 0)
        {
           strcpy(pcgFlightSchedTime,pclAftStoa);
        }
        else
        {
           if (strcmp(pclAftAdid,"D") == 0)
           {
              strcpy(pcgFlightSchedTime,pclAftStod);
           }
           else
           {
              strcpy(pcgFlightSchedTime,pclAftStoa);
           }
        }
        if (ipTlxCmd == FDI_LDM)
        {
           strcpy(pcgLdmFromApt,pclAftOrg3);
           if (strcmp(pclAftAdid,"A") == 0)
           {
              if (strlen(pclAftVian) > 0)
              {
                 strcpy(&pcgRouteList[0][0],pclAftOrg3);
                 igNoRoutes = atoi(pclAftVian) + 1;
                 for (ilI = 1; ilI < igNoRoutes; ilI++)
                 {
                    strncpy(&pcgRouteList[ilI][0],&pclAftVial[(ilI-1)*120+1],3);
                    pcgRouteList[ilI][3] = '\0';
                 }
                 for (ilI = 0; ilI < igNoRoutes; ilI++)
                 {
                    dbg(DEBUG,"SearchFlight: Flight Route %d: <%s>",
                        ilI+1,&pcgRouteList[ilI][0]);
                 }
                 if (igNoDestinations > 0)
                 {
                    for (ilI = 1; ilI < igNoRoutes; ilI++)
                    {
                       if (strcmp(&pcgRouteList[ilI][0],&pcgDestList[0][0]) == 0)
                       {
                          strcpy(pcgLdmFromApt,&pcgRouteList[ilI-1][0]);
                       }
                    }
                 }
                 else
                    strcpy(pcgLdmFromApt,&pcgRouteList[igNoRoutes-1][0]);
              }
              else
              {
                 strcpy(pcgLdmFromApt,pclAftOrg3);
              }
              dbg(DEBUG,"SearchFlight: Telex send from Airport: <%s>",pcgLdmFromApt);
           }
        }
        if (strcmp(pclAftAdid,"D") == 0 && igNoDestinations > 0 &&
            strlen(pcgAftUrnoFromFDIT) == 0)
        {
           if (strlen(pclAftVian) > 0)
           {
              ilNoDest = atoi(pclAftVian) + 1;
           }
           else
           {
              ilNoDest = 1;
           }
           for (ilI = 1; ilI < ilNoDest; ilI++)
           {
              strncpy(&pclDestList[ilI][0],&pclAftVial[(ilI-1)*120+1],3);
              pclDestList[ilI][3] = '\0';
           }
           for (ilI = 0; ilI < ilNoDest; ilI++)
           {
              dbg(DEBUG,"SearchFlight: Flight Destination %d: <%s>",
                  ilI+1,&pclDestList[ilI][0]);
           }
           ilRemTlx = igNoDestinations;
           ilRemFlt = ilNoDest;
           for (ilI = 0; ilI < igNoDestinations; ilI++)
           {
              for (ilJ = 0; ilJ < ilNoDest; ilJ++)
              {
                 if (strcmp(&pcgDestList[ilI][0],&pclDestList[ilJ][0]) == 0)
                 {
                    ilRemTlx--;
                    ilRemFlt--;
                 }
              }
           }
           if (ilRemTlx == 0 && ilRemFlt == 0)
           { /* accept flight */
              ilHit++;
              cgTlxStatus = FDI_STAT_FLT_UPDATED;
           }
           else
           {
              ilRejected = TRUE;
              dbg(DEBUG,"SearchFlight: Flight rejected due to different destinations in Tlx and Flight");
           }
        }
        else
        {
           ilHit++;
           cgTlxStatus = FDI_STAT_FLT_UPDATED;
        }
        if (strlen(pcgRegnForFlightSearch) > 0)
        {
           if (strcmp(pcgRegnForFlightSearch,pclAftRegn) == 0)
              ilRCdb = DB_ERROR;
           else
           {
              slFkt = NEXT;
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           }
        }
        else
        {
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        }
     }
     close_my_cursor(&slCursor);
     if (ilHit != 1)
     {
        strcpy(pcpUrno,"");
        /* strcpy(pcgFlightSchedTime,""); */
        if (ilHit == 0)
        {
           if (ilRejected == TRUE)
           {
              cgTlxStatus = FDI_STAT_OTHER_AIRPORT;
           }
           else
           {
              cgTlxStatus = FDI_STAT_FLT_NOT_FOUND;
           }
           dbg(DEBUG,"SearchFlight: No Flight Found");
        }
        else
        {
           cgTlxStatus = FDI_STAT_TOO_MANY_FLTS;
           dbg(DEBUG,"SearchFlight: More Than One Flight Found");
        }
     }
     else
     {
        if (strlen(pclOrg3) > 0 && strlen(pclDes3) > 0)
        {
           if (strcmp(pclOrg3,pclAftOrg3) == 0 &&
               strcmp(pclDes3,pclAftDes3) == 0 &&
               strlen(pclAftVia3) > 0)
           {
              if (ipTlxCmd != FDI_PTM)
                 cgTlxStatus = FDI_STAT_INVALID_LEG;
              else
              {
                 if (igAccPTMFromOriginVia == FALSE)
                    cgTlxStatus = FDI_STAT_INVALID_LEG;
              }
              if (cgTlxStatus == FDI_STAT_INVALID_LEG)
              {
                 dbg(TRACE,"SearchFlight: Wrong Leg in Telex <%s>-<%s>",pclOrg3,pclDes3);
                 strcpy(pcpUrno,"");
              }
           }
        }
        if (cgTlxStatus != FDI_STAT_FLT_NOT_FOUND && cgTlxStatus != FDI_STAT_INVALID_LEG &&
            ipTlxCmd == FDI_MVT && igUpdateClosedFlight == FALSE)
        {
           if (strcmp(pclAftAdid,"A") == 0 && strlen(pcgAftLand) > 1)
           {
              dbg(DEBUG,"SearchFlight: Flight is already closed");
              strcpy(pcpUrno,"");
              cgTlxStatus = FDI_STAT_FLIGHT_CLOSED;
           }
           if (strcmp(pclAftAdid,"D") == 0 && strlen(pcgAftAirb) > 1)
           {
              dbg(DEBUG,"SearchFlight: Flight is already closed");
              strcpy(pcpUrno,"");
              cgTlxStatus = FDI_STAT_FLIGHT_CLOSED;
           }
        }
        if (cgTlxStatus != FDI_STAT_FLT_NOT_FOUND && cgTlxStatus != FDI_STAT_INVALID_LEG &&
            cgTlxStatus != FDI_STAT_FLIGHT_CLOSED && igFlightIsTooOld > 0)
        {
           if (strcmp(pclAftAdid,"A") == 0)
              strcpy(pclTmpTime,pclAftTifa);
           else
              strcpy(pclTmpTime,pclAftTifd);
           if (strlen(pclTmpTime) == 12)
              strcat(pclTmpTime,"00");
           AddSecondsToCEDATime(pclTmpTime,igFlightIsTooOld*60,1);
           TimeToStr(pclCurTime,time(NULL));
           if (strcmp(pclTmpTime,pclCurTime) < 0)
           {
              if (strcmp(pclAftAdid,"A") == 0)
                 dbg(DEBUG,"SearchFlight: Flight is too old. TIFA = <%s>",pclAftTifa);
              else
                 dbg(DEBUG,"SearchFlight: Flight is too old. TIFD = <%s>",pclAftTifd);
              strcpy(pcpUrno,"");
              cgTlxStatus = FDI_STAT_FLIGHT_TOO_OLD;
           }
        }
        if (cgTlxStatus != FDI_STAT_FLT_NOT_FOUND && cgTlxStatus != FDI_STAT_INVALID_LEG &&
            cgTlxStatus != FDI_STAT_FLIGHT_CLOSED && cgTlxStatus != FDI_STAT_FLIGHT_TOO_OLD)
        {
           memset(pcgTlxMstx,0x00,40);
           strcpy(pcgTlxMstx,pclAftFlno);
           strcat(pcgTlxMstx," ");
           strncat(pcgTlxMstx,pcgFlightSchedTime,4);
           strcat(pcgTlxMstx,".");
           strncat(pcgTlxMstx,&pcgFlightSchedTime[4],2);
           strcat(pcgTlxMstx,".");
           strncat(pcgTlxMstx,&pcgFlightSchedTime[6],2);
           strcat(pcgTlxMstx," ");
           strncat(pcgTlxMstx,&pcgFlightSchedTime[8],2);
           strcat(pcgTlxMstx,":");
           strncat(pcgTlxMstx,&pcgFlightSchedTime[10],2);
           strcat(pcgTlxMstx," ");
           strcat(pcgTlxMstx,pclAftAdid);
           strcat(pcgTlxMstx," ");
           strcat(pcgTlxMstx,pclAftFtyp);
           ilRC = CheckTimeValues(pcpNewFields,pcpNewData,pclAftAdid);

           if ((ipTlxCmd == FDI_SAM || ipTlxCmd == FDI_SLC || ipTlxCmd == FDI_SRM) &&
               strlen(pcgCsgn) > 0)
           {
              memset(&prlTlxResult,0x00,TLXRESULT);
              strcpy(prlTlxResult.DValue,pclAftAlc3);
              strcpy(prlTlxResult.FValue,pclAftAlc3);
              strcpy(prlTlxResult.DName,"Alc");
              CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
              memset(&prlTlxResult,0x00,TLXRESULT);
              strcpy(prlTlxResult.DValue,pclAftFltn);
              strcpy(prlTlxResult.FValue,pclAftFltn);
              strcpy(prlTlxResult.DName,"Fltn");
              CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
              if (*pclAftFlns != '\0' && *pclAftFlns != ' ')
              {
                 memset(&prlTlxResult,0x00,TLXRESULT);
                 strcpy(prlTlxResult.DValue,pclAftFlns);
                 strcpy(prlTlxResult.FValue,pclAftFlns);
                 strcpy(prlTlxResult.DName,"Suffix");
                 CCSArrayAddUnsort(prgMasterTlx,&prlTlxResult,TLXRESULT);
              }
           }
        }
        if (igCheckFlightDate == TRUE)
        {
           if (igUseStimForCheck == TRUE)
           {
              if (*pclAftAdid == 'A')
              {
                 if (strlen(pclAftStod) > 1)
                    strcpy(pclAftFlda,pclAftStod); /* bcos sender uses departure date */
                 else
                    strcpy(pclAftFlda,pclAftStoa); /* better than nothing */
              }
              else
                 strcpy(pclAftFlda,pclAftStod);
              pclAftFlda[8] = '\0';
           }
           dbg(DEBUG,"SearchFlight: Flight Date in Telex = <%s>, in Flight Record = <%s>",
               pcgFltDate,pclAftFlda);
           if (strlen(pclAftFlda) > 0 && strlen(pcgFltDate) > 0 && strcmp(pcgFltDate,pclAftFlda) != 0)
           {
              strcpy(pcpUrno,"");
              cgTlxStatus = FDI_STAT_FLIGHT_DATE_CONFLICT;
              dbg(DEBUG,"SearchFlight: Flight Date Mismatch!!!");
           }
        }
        /*
		v.1.153 - Separate telex date post-check for LDM. Always use
		Stime. Did not piggy back igCheckFlightDate section to cater
		for possible move of this block to different src file in the future.
        */ 
        
        if (((ipTlxCmd == FDI_LDM) && (igLDMCheckFlightDateArr == TRUE) && (pclAftAdid [0] == 'A'))
            ||
            ((ipTlxCmd == FDI_LDM) && (igLDMCheckFlightDateDep == TRUE) && (pclAftAdid [0] == 'D')))
        {
          if (pclAftAdid [0] == 'A')
          { 
            if (strlen(pclAftStod) > 1)
              strcpy(pclAftFlda,pclAftStod); /* bcos sender uses dep. date */
            else
              strcpy(pclAftFlda,pclAftStoa); /* better than nothing */
          }
          else /* ADID has to be 'A'/'D' because FTYP T/G had been excluded */
          {
             strcpy(pclAftFlda,pclAftStod);
          }
          pclAftFlda[8] = '\0'; 
          dbg(TRACE,"SearchFlight: LDM Flight Date Telex <%s> AFTTAB STOA/D <%s>",
                pcgFltDate,pclAftFlda);
          if (strlen(pclAftFlda) > 0 && strlen(pcgFltDate) > 0 && strcmp(pcgFltDate,pclAftFlda) != 0)
          {
              strcpy(pcpUrno,"");
              cgTlxStatus = FDI_STAT_FLIGHT_DATE_CONFLICT;
              dbg(TRACE,"SearchFlight: LDM Flight Date Mismatch");
          }
          else
              dbg(TRACE,"SearchFlight: LDM Flight Date Accepted");
        } /* end LDM post-check */
     }
  }

  if (ipTlxCmd == FDI_MVT && igHandleMVTVIA == TRUE)
  {
     if (cgTlxStatus == FDI_STAT_FLT_NOT_FOUND ||
         cgTlxStatus == FDI_STAT_OTHER_AIRPORT ||
         cgTlxStatus == FDI_STAT_FLT_UPDATED ||
         cgTlxStatus == FDI_STAT_FLIGHT_CLOSED ||
         cgTlxStatus == FDI_STAT_FLIGHT_TOO_OLD)
     {
        dbg(TRACE,"SearchFlight: Start 2nd search <%s><%s>",pclOrg3,pclDes3);
        strcpy(pclSelectBuf,"");
        if (strlen(pclOrg3) > 0)
        {
           if (strcmp(pclOrg3,pcgHomeAP) == 0)
           {
              sprintf(pclSelectBuf,"TIFD BETWEEN '%s' AND '%s'",
                      pclDepStart,pclDepEnd);
              if (strlen(pclDes3) > 0)
                 sprintf(pclTmpBuf," AND ORG3 = '%s' AND VIAL LIKE '%%%s%%'",
                         pclOrg3,pclDes3);
              else
                 sprintf(pclTmpBuf," AND ORG3 = '%s'",pclOrg3);
              strcat(pclSelectBuf,pclTmpBuf);
              strcat(pclSelectBuf," AND ADID = 'D'");
           }
           else
           {
              if (strlen(pclDes3) > 0)
              {
                 if (strcmp(pclDes3,pcgHomeAP) == 0)
                 {
                    sprintf(pclSelectBuf,"TIFA BETWEEN '%s' AND '%s'",
                            pclArrStart,pclArrEnd);
                    sprintf(pclTmpBuf," AND DES3 = '%s' AND VIAL LIKE '%%%s%%'",
                            pclDes3,pclOrg3);
                    strcat(pclSelectBuf,pclTmpBuf);
                    strcat(pclSelectBuf," AND ADID = 'A'");
                 }
                 else
                 {
                    sprintf(pclSelectBuf,"(TIFA BETWEEN '%s' AND '%s' OR TIFD BETWEEN '%s' AND '%s')",
                            pclArrStart,pclArrEnd,pclDepStart,pclDepEnd);
                    sprintf(pclTmpBuf," AND (VIAL LIKE '%%%s%%' OR VIAL LIKE '%%%s%%')",
                            pclOrg3,pclDes3);
                    strcat(pclSelectBuf,pclTmpBuf);
                    strcat(pclSelectBuf," AND (ADID = 'A' OR ADID = 'D')");
                 }
              }
              else
              {
                 sprintf(pclSelectBuf,"(TIFA BETWEEN '%s' AND '%s' OR TIFD BETWEEN '%s' AND '%s')",
                         pclArrStart,pclArrEnd,pclDepStart,pclDepEnd);
                 sprintf(pclTmpBuf," AND VIAL LIKE '%%%s%%'",
                         pclOrg3);
                 strcat(pclSelectBuf,pclTmpBuf);
                 strcat(pclSelectBuf," AND (ADID = 'A' OR ADID = 'D')");
              }
           }
        }
        else
        {
           sprintf(pclSelectBuf,"(TIFA BETWEEN '%s' AND '%s' OR TIFD BETWEEN '%s' AND '%s')",
                   pclArrStart,pclArrEnd,pclDepStart,pclDepEnd);
           sprintf(pclTmpBuf," AND VIAL LIKE '%%%s%%'",
                   pclDes3);
           strcat(pclSelectBuf,pclTmpBuf);
           strcat(pclSelectBuf," AND (ADID = 'A' OR ADID = 'D')");
        }
        if (strlen(pclSelectBuf) > 0)
        {
           sprintf(pclTmpBuf," AND ALC3 = '%s'",pclAlc3);
           strcat(pclSelectBuf,pclTmpBuf);
           sprintf(pclTmpBuf," AND FLTN = '%s'",pclFltn);
           strcat(pclSelectBuf,pclTmpBuf);
           if (strlen(pclFlns) > 0)
           {
              sprintf(pclTmpBuf," AND FLNS = '%s'",pclFlns);
           }
           else
           {
              sprintf(pclTmpBuf," AND FLNS = ' '");
           }
           strcat(pclSelectBuf,pclTmpBuf);
           strcat(pclSelectBuf," AND FTYP NOT IN ('T','G')");
           if (cgTlxStatus == FDI_STAT_FLT_UPDATED)
              sprintf(pclSelectBuf,"URNO = %s",pclAftUrno);
           strcpy(pclSqlBuf,"SELECT URNO,FLNO,TIFA,TIFD,STOA,STOD,ADID,FTYP,PAX1,PAX2,PAX3,BAGW,ORG3,DES3,VIA3,VIAN,VIAL,ONBL,LAND,AIRB FROM AFTTAB WHERE ");
           strcat(pclSqlBuf,pclSelectBuf);
           ilHit = 0;
           ilRejected = FALSE;
           slCursor = 0;
           slFkt = START;
           dbg(DEBUG,"SearchFlight: <%s>",pclSqlBuf);
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           while (ilRCdb == DB_SUCCESS)
           {
              BuildItemBuffer(pclDataBuf,"",20,",");
              ilLen = get_real_item(pclAftUrno,pclDataBuf,1);
              ilLen = get_real_item(pclAftFlno,pclDataBuf,2);
              ilLen = get_real_item(pclAftTifa,pclDataBuf,3);
              ilLen = get_real_item(pclAftTifd,pclDataBuf,4);
              ilLen = get_real_item(pclAftStoa,pclDataBuf,5);
              ilLen = get_real_item(pclAftStod,pclDataBuf,6);
              ilLen = get_real_item(pclAftAdid,pclDataBuf,7);
              ilLen = get_real_item(pclAftFtyp,pclDataBuf,8);
              ilLen = get_real_item(pcgAftPax1,pclDataBuf,9);
              ilLen = get_real_item(pcgAftPax2,pclDataBuf,10);
              ilLen = get_real_item(pcgAftPax3,pclDataBuf,11);
              ilLen = get_real_item(pcgAftBagw,pclDataBuf,12);
              ilLen = get_real_item(pclAftOrg3,pclDataBuf,13);
              ilLen = get_real_item(pclAftDes3,pclDataBuf,14);
              ilLen = get_real_item(pclAftVia3,pclDataBuf,15);
              ilLen = get_real_item(pclAftVian,pclDataBuf,16);
              ilLen = get_real_item(pclAftVial,pclDataBuf,17);
              ilLen = get_real_item(pcgAftOnbl,pclDataBuf,18);
              ilLen = get_real_item(pcgAftLand,pclDataBuf,19);
              ilLen = get_real_item(pcgAftAirb,pclDataBuf,20);
              dbg(DEBUG,"SearchFlight: Found flight <%s><%s><%s><%s><%s><%s><%s>",
                  pclAftUrno,pclAftFlno,pclAftTifa,pclAftTifd,pclAftStoa,pclAftStod,pclAftAdid);
              strcpy(pcgAftAdid,pclAftAdid);
              strcpy(pcpUrno,pclAftUrno);
              strcpy(pcpNewFields,pclNewFieldList);
              strcpy(pcpNewData,pclNewDataList);
              if (strcmp(pclAftAdid,"A") == 0)
              {
                 strcpy(pcgFlightSchedTime,pclAftStoa);
              }
              else
              {
                 if (strcmp(pclAftAdid,"D") == 0)
                 {
                    strcpy(pcgFlightSchedTime,pclAftStod);
                 }
                 else
                 {
                    strcpy(pcgFlightSchedTime,pclAftStoa);
                 }
              }
              ilHit++;
              cgTlxStatus = FDI_STAT_FLT_UPDATED;
              slFkt = NEXT;
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           }
           close_my_cursor(&slCursor);
           if (ilHit != 1)
           {
              strcpy(pcpUrno,"");
              if (ilHit == 0)
              {
                 cgTlxStatus = FDI_STAT_FLT_NOT_FOUND;
                 dbg(DEBUG,"SearchFlight: No Flight Found");
              }
              else
              {
                 cgTlxStatus = FDI_STAT_TOO_MANY_FLTS;
                 dbg(DEBUG,"SearchFlight: More Than One Flight Found");
              }
           }
           else
           {
              memset(pcgTlxMstx,0x00,40);
              strcpy(pcgTlxMstx,pclAftFlno);
              strcat(pcgTlxMstx," ");
              strncat(pcgTlxMstx,pcgFlightSchedTime,4);
              strcat(pcgTlxMstx,".");
              strncat(pcgTlxMstx,&pcgFlightSchedTime[4],2);
              strcat(pcgTlxMstx,".");
              strncat(pcgTlxMstx,&pcgFlightSchedTime[6],2);
              strcat(pcgTlxMstx," ");
              strncat(pcgTlxMstx,&pcgFlightSchedTime[8],2);
              strcat(pcgTlxMstx,":");
              strncat(pcgTlxMstx,&pcgFlightSchedTime[10],2);
              strcat(pcgTlxMstx," ");
              strcat(pcgTlxMstx,pclAftAdid);
              strcat(pcgTlxMstx," ");
              strcat(pcgTlxMstx,pclAftFtyp);
              ilRC = CheckTimeValues(pcpNewFields,pcpNewData,pclAftAdid);
              if (strcmp(pclAftAdid,"A") == 0 && strlen(pclAftVia3) > 0 &&
                  strcmp(pclOrg3,pclAftVia3) != 0)
              {
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DCD1");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DTD1");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DCD2");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DTD2");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"NXTI");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"PAXT");
              }
              if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclOrg3,pcgHomeAP) != 0)
              {
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DCD1");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DTD1");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DCD2");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"DTD2");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"NXTI");
                 ilRC = RemoveItemFromList(pcpNewFields,pcpNewData,"PAXT");
              }
              if (strstr(pcpNewFields,"LNDD") != 0)
              {
                 if (strcmp(pclAftAdid,"A") == 0 && strcmp(pclDes3,pcgHomeAP) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"LNDD",pclDes3);
                 }
                 if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclDes3,pclAftDes3) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"LNDD",pclDes3);
                 }
              }
              if (strstr(pcpNewFields,"ONBD") != 0)
              {
                 if (strcmp(pclAftAdid,"A") == 0 && strcmp(pclDes3,pcgHomeAP) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ONBD",pclDes3);
                 }
                 if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclDes3,pclAftDes3) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ONBD",pclDes3);
                 }
              }
              if (strstr(pcpNewFields,"ONBE") != 0)
              {
                 if (strcmp(pclAftAdid,"A") == 0 && strcmp(pclDes3,pcgHomeAP) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ONBE",pclDes3);
                 }
                 if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclDes3,pclAftDes3) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ONBE",pclDes3);
                 }
              }
              if (strstr(pcpNewFields,"OFBD") != 0)
              {
                 if (strcmp(pclAftAdid,"A") == 0 && strcmp(pclOrg3,pclAftOrg3) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"OFBD",pclOrg3);
                 }
                 if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclOrg3,pcgHomeAP) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"OFBD",pclOrg3);
                 }
              }
              if (strstr(pcpNewFields,"AIRD") != 0)
              {
                 if (strcmp(pclAftAdid,"A") == 0 && strcmp(pclOrg3,pclAftOrg3) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"AIRD",pclOrg3);
                    if (igAftnCopyOrgStdToAtdWhenViaDep == TRUE)
                    {
                      ilRC = CopyAftFieldToAftField (pclAftUrno,pcpNewFields,
                        pcpNewData,"STOD","AIRD","","");
                      igAftnCopyOrgStdToAtdWhenViaDepFlag = TRUE;
                    }
                 }
                 if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclOrg3,pcgHomeAP) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"AIRD",pclOrg3);
                 }
              }
              if (strstr(pcpNewFields,"ETAE") != 0)
              {
                 if (strcmp(pclAftAdid,"A") == 0 && strcmp(pclDes3,pcgHomeAP) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ETAE",pclDes3);
                 }
                 if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclDes3,pclAftDes3) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ETAE",pclDes3);
                 }
              }
              if (strstr(pcpNewFields,"ETDE") != 0)
              {
                 if (strcmp(pclAftAdid,"A") == 0 && strcmp(pclOrg3,pclAftOrg3) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ETDE",pclOrg3);
                 }
                 if (strcmp(pclAftAdid,"D") == 0 && strcmp(pclOrg3,pcgHomeAP) != 0)
                 {
                    ilRC = UpdateVial(pcpNewFields,pcpNewData,"ETDE",pclOrg3);
                 }
              }
              if (strstr(pcpNewFields,"VSUP") != 0)
              {
                 ilRC = CompressVial(pcpNewFields,pcpNewData);
              }
           }
        }
     }
  }

/*
 *      UFIS-872
 *      At this point, whether or not the 2nd search for MVTVIA was performed, cgTlxStatus
 *      would be 'U' if a message is to be sent to FLIGHT after return from SearchFlight.
 *      Make use of the latest pclAftVian and pclAftVia3 even if it is from the 2nd search.
 *
 *	v.1.148 - skip evaluation if telex pclOrg3/pclDes3 is empty. This can
 *	happen if AA/AD is received from HOPO.
 */
  if ((ipTlxCmd == FDI_MVT) && (cgTlxStatus == FDI_STAT_FLT_UPDATED))
  {
    if (atoi (pclAftVian) > 0) /* VIAN is never a space */
    {
      if (((pclAftAdid [0] == 'A') && (strcmp (pclOrg3,pclAftVia3) != 0) &&
          (strlen(pclOrg3) > 0))
        ||
         ((pclAftAdid [0] == 'D') && (strcmp (pclDes3,pclAftVia3) != 0) &&
          (strlen(pclDes3) > 0)))
      {
        dbg(TRACE,
          "SearchFlight: REGN removed because not for 1st leg in/out of HOPO");
        if (pclAftAdid [0] == 'A')
          dbg(TRACE,"ADID <%c> Telex Origin <%s> AFT VIA3 <%s>",
            pclAftAdid [0],pclOrg3,pclAftVia3);
        else if (pclAftAdid [0] == 'D')
          dbg(TRACE,"ADID <%c> Telex Destination <%s> AFT VIA3 <%s>",
            pclAftAdid [0],pclDes3,pclAftVia3);

        if ((strlen (pcpNewFields) > 0) && (strlen (pcpNewData) > 0))
          (void) RemoveItemFromList(pcpNewFields,pcpNewData,"REGN");
      }
    }
  }

  return ilRC;
} /* End of SearchFlight */


static int FdiHandleSql(char *pcpCommand, char *pcpTable, char *pcpUrno, char *pcpFields,
                        char *pcpData, char *pcpTwStart, char *pcpTwEnd, int ipSendToAction,
                        int ipSendToBchdl)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[32000];
  char pclSelectBuf[1024];
  char pclDataBuf[1024];
  int ilDebugLevel;
  char pclNewUrno[32];
  char pclTable[32];
  char pclFields[2048];
  char pclData[32000];
  char pclTmpFields[2048];
  char pclTmpData[32000];
  char pclSqlBufFields[2048];
  char pclSqlBufValues[32000];
  int ilCount;
  int ilI;
  char pclFieldName[32];
  char pclFieldValue[32000];

  dbg(DEBUG,"FdiHandleSql: Command =        <%s>",pcpCommand);
  dbg(DEBUG,"FdiHandleSql: Table =          <%s>",pcpTable);
  dbg(DEBUG,"FdiHandleSql: Urno =           <%s>",pcpUrno);
  dbg(DEBUG,"FdiHandleSql: Fields =         <%s>",pcpFields);
  dbg(DEBUG,"FdiHandleSql: Data (max 100) = <%.100s>",pcpData);

  if (strcmp(pcpCommand,"IBT") != 0 && strcmp(pcpCommand,"URT") != 0 &&
      strcmp(pcpCommand,"DRT") != 0 && strcmp(pcpCommand,"IRT") != 0)
  {
     dbg(TRACE,"FdiHandleSql: Invalid command <%s>",pcpCommand);
     return RC_FAIL;
  }

  strcpy(pclTable,pcpTable);
  if (strlen(pclTable) == 3)
  {
     strcat(pclTable,pcgTABEnd);
  }

  strcpy(pclSelectBuf,"");

  if (strcmp(pcpCommand,"IBT") == 0 || strcmp(pcpCommand,"IRT") == 0)
  {
     if (strcmp(pclTable,"TLXTAB") == 0 && strlen(pcgNextTlxUrno) > 0)
     {
        strcpy(pclNewUrno,pcgNextTlxUrno);
        strcpy(pcgLastTlxUrno,pcgNextTlxUrno);
        strcpy(pcgNextTlxUrno,"");
     }
     else
     {
        ilDebugLevel = debug_level;
        debug_level = 0;
#ifdef DB_LOAD_URNO_LIST
	/* UFIS-1485 */
        DB_LoadUrnoList(1,pclNewUrno,pcgTableKey);
#else
        GetNextValues(pclNewUrno,1);
#endif        
       
        debug_level = ilDebugLevel;
        if (strcmp(pclTable,"TLXTAB") == 0)
           strcpy(pcgLastTlxUrno,pclNewUrno);
        if (strcmp(pclTable,"LOATAB") == 0)
          igLoaInsertUrno = atoi(pclNewUrno); /* save for lookup in calling function */
     }
     strcpy(pclFields,pcpFields);
     strcpy(pclData,pcpData);
     if (strcmp(pclTable,"ICSTAB") != 0)
     {
        if (strstr(pclFields,"HOPO") == NULL)
        {
           strcat(pclFields,",HOPO");
           strcat(pclData,",");
           strcat(pclData,pcgHomeAP);
        }
     }
     strcpy(pclSqlBufFields,"URNO,");
     strcpy(pclSqlBufValues,pclNewUrno);
     strcat(pclSqlBufValues,",");
     ilCount = GetNoOfElements(pclFields,',');
     for (ilI = 1; ilI <= ilCount; ilI++)
     {
        GetDataItem(pclFieldName,pclFields,ilI,',',""," ");
        GetDataItem(pclFieldValue,pclData,ilI,',',""," ");
        if (strlen(pclFieldValue) == 0)
        {
           if (strcmp(pclFieldName,"FLNU") == 0 || strcmp(pclFieldName,"RURN") == 0)
           {
              strcpy(pclFieldValue,"0");
           }
           else
           {
              strcpy(pclFieldValue," ");
           }
        }
        else
        {
           if ((strcmp(pclFieldName,"FLNU") == 0 || strcmp(pclFieldName,"RURN") == 0) &&
               pclFieldValue[0] == ' ')
           {
              strcpy(pclFieldValue,"0");
           }
        }
        strcat(pclSqlBufFields,pclFieldName);
        strcat(pclSqlBufFields,",");
        if (strcmp(pclFieldName,"URNO") != 0 && strcmp(pclFieldName,"FLNU") != 0 &&
            strcmp(pclFieldName,"RURN") != 0)
        {
           strcat(pclSqlBufValues,"'");
           strcat(pclSqlBufValues,pclFieldValue);
           strcat(pclSqlBufValues,"',");
        }
        else
        {
           strcat(pclSqlBufValues,pclFieldValue);
           strcat(pclSqlBufValues,",");
        }
     }
     pclSqlBufFields[strlen(pclSqlBufFields)-1] = '\0';
     pclSqlBufValues[strlen(pclSqlBufValues)-1] = '\0';
     sprintf(pclSqlBuf,"INSERT INTO %s FIELDS(%s) VALUES(%s)",
             pclTable,pclSqlBufFields,pclSqlBufValues);
     strcpy(pclTmpFields,"URNO,");
     strcpy(pclTmpData,pclNewUrno);
     strcat(pclTmpData,",");
     strcat(pclTmpFields,pclFields);
     strcat(pclTmpData,pclData);
     strcpy(pclFields,pclTmpFields);
     strcpy(pclData,pclTmpData);
  }

  if (strcmp(pcpCommand,"URT") == 0)
  {
     strcpy(pclSqlBufValues,"");
     ilCount = GetNoOfElements(pcpFields,',');
     for (ilI = 1; ilI <= ilCount; ilI++)
     {
        GetDataItem(pclFieldName,pcpFields,ilI,',',""," ");
        GetDataItem(pclFieldValue,pcpData,ilI,',',""," ");
        if (strlen(pclFieldValue) == 0)
        {
           if (strcmp(pclFieldName,"FLNU") == 0 || strcmp(pclFieldName,"RURN") == 0)
           {
              strcpy(pclFieldValue,"0");
           }
           else
           {
              strcpy(pclFieldValue," ");
           }
        }
        else
        {
           if ((strcmp(pclFieldName,"FLNU") == 0 || strcmp(pclFieldName,"RURN") == 0) &&
               pclFieldValue[0] == ' ')
           {
              strcpy(pclFieldValue,"0");
           }
        }
        strcat(pclSqlBufValues,pclFieldName);
        if (strcmp(pclFieldName,"URNO") != 0 && strcmp(pclFieldName,"FLNU") != 0 &&
            strcmp(pclFieldName,"RURN") != 0)
        {
           strcat(pclSqlBufValues,"='");
           strcat(pclSqlBufValues,pclFieldValue);
           strcat(pclSqlBufValues,"',");
        }
        else
        {
           strcat(pclSqlBufValues,"=");
           strcat(pclSqlBufValues,pclFieldValue);
           strcat(pclSqlBufValues,",");
        }
     }
     pclSqlBufValues[strlen(pclSqlBufValues)-1] = '\0';
     sprintf(pclSqlBuf,"UPDATE %s SET %s ",pclTable,pclSqlBufValues);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     strcpy(pclFields,pcpFields);
     strcpy(pclData,pcpData);
     if (strstr(pclFields,"URNO") == NULL)
     {
        strcat(pclFields,",URNO");
        strcat(pclData,",");
        strcat(pclData,pcpUrno);
     }
  }

  if (strcmp(pcpCommand,"DRT") == 0)
  {
     sprintf(pclSqlBuf,"DELETE FROM %s ",pclTable);
     sprintf(pclSelectBuf,"WHERE URNO = %s",pcpUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     strcpy(pclFields,pcpFields);
     strcpy(pclData,pcpData);
     if (strstr(pclFields,"URNO") == NULL)
     {
        strcat(pclFields,",URNO");
        strcat(pclData,",");
        strcat(pclData,pcpUrno);
     }
  }

  dbg(DEBUG,"FdiHandleSql: <%.150s>",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
     ilRC = TriggerBchdlAction(pcpCommand,pclTable,pcpUrno,pclFields,pclData,
                               pcpTwStart,pcpTwEnd,ipSendToBchdl,ipSendToAction);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of FdiHandleSql */


static int FdiSendSql(char *pcpSqlBuf)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "FdiSendSql:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclDataBuf[32000];
  char *pclTmpPtr;
  char pclSqlBuf[2048];

  if (strstr(pcpSqlBuf,"DELETE") != NULL && strstr(pcpSqlBuf,"LOATAB") != NULL)
  {
     dbg(TRACE,"%s Now collect URNOs first",pclFunc);
     strcpy(pclSqlBuf,"SELECT URNO ");
     pclTmpPtr = strstr(pcpSqlBuf,"FROM");
     if (pclTmpPtr != NULL)
     {
        strcat(pclSqlBuf,pclTmpPtr);
        dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        while (ilRCdb == DB_SUCCESS)
        {
           if (igCurInUrnoList < MAX_IN_URNO_LIST)
           {
              strcpy(&pcgUrnoList[igCurInUrnoList][0],pclDataBuf);
              igCurInUrnoList++;
           }
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        }
        close_my_cursor(&slCursor);
     }
  }
  dbg(DEBUG,"%s <%.200s>",pclFunc,pcpSqlBuf);
  slCursor = 0;
  slFkt = START;
  ilRCdb = sql_if(slFkt,&slCursor,pcpSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     commit_work();
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of FdiSendSql */


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, char *pcpTwStart,
                              char *pcpTwEnd, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;
  int ilCount;
  int ilI;
  char pclFieldName[16];
  char pclFieldValue[1024];
  char pclNewFields[1024] = "";
  char pclNewData[1024] = "";

  if (ipBchdl == TRUE)
  {
     if (strcmp(pcpCmd,"URT") == 0 && strcmp(pcpTable,"TLXTAB") == 0)
     {
        ilCount = GetNoOfElements(pcpFields,',');
        for (ilI = 1; ilI <= ilCount; ilI++)
        {
           GetDataItem(pclFieldName,pcpFields,ilI,',',""," ");
           if (strcmp(pclFieldName,"BEME") != 0 && strcmp(pclFieldName,"TXT1") != 0 &&
               strcmp(pclFieldName,"TXT2") != 0)
           {
              GetDataItem(pclFieldValue,pcpData,ilI,',',""," ");
              if (strlen(pclNewFields) == 0)
              {
                 strcpy(pclNewFields,pclFieldName);
                 strcpy(pclNewData,pclFieldValue);
              }
              else
              {
                 strcat(pclNewFields,",");
                 strcat(pclNewFields,pclFieldName);
                 strcat(pclNewData,",");
                 strcat(pclNewData,pclFieldValue);
              }
           }
        }
        if (igBroadcastTelexes == TRUE)
        {
           ilRC = SendCedaEvent(1900,0,"fdihdl","CEDA",pcpTwStart,pcpTwEnd,pcpCmd,pcpTable,
                                pcpSelection,pclNewFields,pclNewData,"",3,NETOUT_NO_ACK);
           dbg(DEBUG,"Send Broadcast: <%s><%s><%s><%s><%.100s>",
               pcpCmd,pcpTable,pcpSelection,pclNewFields,pclNewData);
        }
     }
     else
     {
        if (igBroadcastTelexes == TRUE)
        {
           ilRC = SendCedaEvent(1900,0,"fdihdl","CEDA",pcpTwStart,pcpTwEnd,pcpCmd,pcpTable,
                                pcpSelection,pcpFields,pcpData,"",3,NETOUT_NO_ACK);
           dbg(DEBUG,"Send Broadcast: <%s><%s><%s><%s><%.100s>",
               pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
        }
     }
  }

  if (ipAction == TRUE)
  {
     ilRC = SendCedaEvent(7400,0,"fdihdl","CEDA",pcpTwStart,pcpTwEnd,pcpCmd,pcpTable,
                          pcpSelection,pcpFields,pcpData,"",3,NETOUT_NO_ACK);
     dbg(DEBUG,"Send to ACTION: <%s><%s><%s><%s><%.100s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }

  return ilRC;
} /* End of TriggerBchdlAction */


static int SaveTelexes(int ipHeaderTyp, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilRCEnd = RC_SUCCESS;
  char *pclTlxStart = NULL;
  char *pclTlxEnd = NULL;
  int ilPosStart = 0;
  int ilPosEnd = 0;
  int ilLen = 0;
  int ilCol = 0;
  char clSaveChar;
  int ilCnt;
  int ilDebugLevel;
  char pclFieldList[1024];
  char pclValueList[8192];
  char pclText[32000];
  int ilI;
  char pclCdat[32];
  char pclNewData[32000];
  char *pclTmpPtr;
  char *pclTmpPtr2;
  char *pclTmpPtr3;
  char pclPageBegin[16];
  char *pclPtrBegin;
  char pclPrevPage[32000];
  char pclLastPage[32000];
  int ilLineNo;
  char pclLine1[1024];
  char pclLine2[1024];
  int ilBegin;
  char pclFirstLine[1024];
  char pclNextFirstLine[1024];
  int ilInsLF;
  int ilTextSize;

  strcpy(pclCdat,GetTimeStamp());
  igMaxTlxUrnoIdx = 0;
  igCurTlxUrnoIdx = 0;
  ilCnt = 0;
  ilRC = RC_FAIL;
  ilRCEnd = RC_SUCCESS;
  pclTlxStart = pcpData;
  pclTlxEnd = pcpData;
  while (ilRCEnd == RC_SUCCESS)
  {
     switch (ipHeaderTyp)
     {
        case 1:
        case 6:
           ilRC = FindItemInList(pclTlxStart,"ZCZC",'\n',&ilLen,&ilCol,&ilPosStart);
           if (ilRC == RC_SUCCESS)
           {
              ilPosStart--;
              pclTlxStart = pclTlxStart + ilPosStart;
              pclTlxEnd = pclTlxStart + 5;
              ilRCEnd = FindItemInList(pclTlxEnd,"ZCZC",'\n',&ilLen,&ilCol,&ilPosEnd);
              if (ilRCEnd == RC_SUCCESS)
              {
                 ilPosEnd--;
                 pclTlxEnd = pclTlxEnd + ilPosEnd;
              }
              else
              {
                 pclTlxEnd = pclTlxEnd + strlen(pclTlxEnd);
              }
           }
           else
           {
              pclTlxStart = pcpData;
              pclTlxEnd = pclTlxStart + strlen(pclTlxStart);
              ilRCEnd = RC_FAIL;
              ilRC = RC_SUCCESS;
           }
           break;
        case 2:
           ilRC = FindItemInList(pclTlxStart,"Message RCV",'\n',&ilLen,&ilCol,&ilPosStart);
           if (ilRC == RC_SUCCESS)
           {
              ilPosStart--;
              pclTlxStart = pclTlxStart + ilPosStart;
              pclTlxEnd = pclTlxStart + 12;
              ilRCEnd = FindItemInList(pclTlxEnd,"Message RCV",'\n',&ilLen,&ilCol,&ilPosEnd);
              if (ilRCEnd == RC_SUCCESS)
              {
                 ilPosEnd--;
                 pclTlxEnd = pclTlxEnd + ilPosEnd;
              }
              else
              {
                 pclTlxEnd = pclTlxEnd + strlen(pclTlxEnd);
              }
           }
           break;
        case 3:
           ilRC = FindItemInList(pclTlxStart,"\177RCV",'\n',&ilLen,&ilCol,&ilPosStart);
           if (ilRC == RC_SUCCESS)
           {
              pclTlxStart = pclTlxStart + ilPosStart;
              pclTlxEnd = pclTlxStart + 4;
              ilRCEnd = FindItemInList(pclTlxEnd,"\177RCV",'\n',&ilLen,&ilCol,&ilPosEnd);
              if (ilRCEnd == RC_SUCCESS)
              {
                 pclTlxEnd = pclTlxEnd + ilPosEnd;
              }
              else
              {
                 pclTlxEnd = pclTlxEnd + strlen(pclTlxEnd);
              }
           }
           break;
        case 4:
           ilRC = FindItemInList(pclTlxStart,"RCVD",'\n',&ilLen,&ilCol,&ilPosStart);
           if (ilRC == RC_SUCCESS)
           {
              ilPosStart -= 8;
              pclTlxStart = pclTlxStart + ilPosStart;
              pclTlxEnd = pclTlxStart + 12;
              ilRCEnd = FindItemInList(pclTlxEnd,"RCVD",'\n',&ilLen,&ilCol,&ilPosEnd);
              if (ilRCEnd == RC_SUCCESS)
              {
                 ilPosEnd -= 8;
                 pclTlxEnd = pclTlxEnd + ilPosEnd;
              }
              else
              {
                 pclTlxEnd = pclTlxEnd + strlen(pclTlxEnd);
              }
           }
           break;
        case 5:
/*
           if (pclTlxStart != pclTlxEnd)
           {
              pclTlxStart = pclTlxEnd;
           }
           ilRC = FindItemInList(pclTlxEnd,"@NNNN",'\n',&ilLen,&ilCol,&ilPosEnd);
           if (ilRC == RC_SUCCESS)
           {
              pclTlxEnd = pclTlxEnd + ilPosEnd + 5;
           }
*/
           pclTlxEnd = pclTlxEnd + strlen(pclTlxStart);
           ilRC = RC_SUCCESS;
           ilRCEnd = RC_FAIL;
           break;
        case 7:
           ilInsLF = FALSE;
           pclTmpPtr = strstr(pclTlxStart,"<=SCREENCONTENT=>");
           if (pclTmpPtr != NULL)
           {
              pclTmpPtr = strstr(pclTmpPtr,"\n");
              if (pclTmpPtr != NULL && strlen(pclTmpPtr) > 23)
              {
                 /* get 1st screen */
                 memset(pclNewData,0x00,32000);
                 pclTmpPtr++;
                 strncpy(pclPageBegin,pclTmpPtr,5);
                 pclPageBegin[5] = '\0';
                 ilBegin = 5;
                 if (*pclTmpPtr == 0x1b)
                 {
                    if (pclTmpPtr[4] == '?')
                       ilBegin = 4;
                    else
                       ilBegin = 7;
                    strncpy(pclPageBegin,pclTmpPtr,ilBegin);
                    pclPageBegin[ilBegin] = '\0';
                 }
                 if (pclPageBegin[0] == '/' || pclPageBegin[0] == 0x1b)
                 {
                    if (pclPageBegin[0] == '/')
                    {
                       ilInsLF = TRUE;
                    }
                    pclPtrBegin = pclTlxStart;
                    memset(pclPrevPage,0x00,32000);
                    strncpy(pclPrevPage,pclPtrBegin,pclTmpPtr-pclPtrBegin);
                    strncpy(pclNewData,pclPtrBegin,pclTmpPtr-pclPtrBegin);
                    pclPtrBegin = pclTmpPtr + ilBegin;
                    CopyLine(pclFirstLine,pclPtrBegin);
                    pclTmpPtr = strstr(pclPtrBegin,pclPageBegin);
                    while (pclTmpPtr != NULL)
                    {
                       /* keep current screen */
                       memset(pclPrevPage,0x00,32000);
                       strncpy(pclPrevPage,pclPtrBegin,pclTmpPtr-pclPtrBegin);
                       strncat(pclNewData,pclPtrBegin,pclTmpPtr-pclPtrBegin);
                       pclPtrBegin = pclTmpPtr + ilBegin;
                       CopyLine(pclNextFirstLine,pclPtrBegin);
                       pclTmpPtr = strstr(pclPtrBegin,pclPageBegin);
                       if (pclTmpPtr != NULL && strcmp(pclFirstLine,pclNextFirstLine) == 0)
                       {
                          pclPtrBegin += strlen(pclNextFirstLine) + 1;
                       }
                    }
                    /* get last screen */
                    if (strcmp(pclFirstLine,pclNextFirstLine) == 0)
                    {
                       pclPtrBegin += strlen(pclNextFirstLine) + 1;
                    }
                    memset(pclLastPage,0x00,32000);
                    strcpy(pclLastPage,pclPtrBegin);
                    pclTmpPtr = strstr(pclLastPage,"<=/SCREENCONTENT=>");
                    if (pclTmpPtr != NULL)
                    {
                       *pclTmpPtr = '\0';
                    }
                    /* check overlapping parts */
                    ilLineNo = 1;
                    pclTmpPtr = GetLine(pclPrevPage,ilLineNo);
                    while (pclTmpPtr != NULL)
                    {
                       CopyLine(pclLine1,pclTmpPtr);
                       if (strncmp(pclTmpPtr,pclLastPage,strlen(pclTmpPtr)) == 0)
                       {
                          pclPtrBegin += strlen(pclTmpPtr);
                          pclTmpPtr = NULL;
                       }
                       else
                       {
                          ilLineNo++;
                          pclTmpPtr = GetLine(pclPrevPage,ilLineNo);
                       }
                    }
                    strcat(pclNewData,pclPtrBegin);
                    strcpy(pcpData,pclNewData);
                 }
              }
           }
/*  64 chars begin */
           if (ilInsLF == TRUE)
           {
              memset(pclNewData,0x00,32000);
              pclTmpPtr = pcpData;
              pclTmpPtr2 = strstr(pclTmpPtr,"\n");
              while (pclTmpPtr2 != NULL)
              {
                 strncpy(pclText,pclTmpPtr,pclTmpPtr2-pclTmpPtr+1);
                 pclText[pclTmpPtr2-pclTmpPtr+1] = '\0';
                 pclTmpPtr3 = pclText;
                 while (strlen(pclTmpPtr3) > 65)
                 {
                    strncat(pclNewData,pclTmpPtr3,64);
                    strcat(pclNewData,"\n");
                    pclTmpPtr3 += 64;
                 }
                 strcat(pclNewData,pclTmpPtr3);
                 pclTmpPtr = pclTmpPtr2 + 1;
                 pclTmpPtr2 = strstr(pclTmpPtr,"\n");
              }
              while (strlen(pclTmpPtr) > 65)
              {
                 strncat(pclNewData,pclTmpPtr,64);
                 strcat(pclNewData,"\n");
                 pclTmpPtr += 64;
              }
              strcat(pclNewData,pclTmpPtr);
              strcpy(pcpData,pclNewData);
           }
/*  64 chars end */
           pclTlxEnd = pclTlxEnd + strlen(pclTlxStart);
           ilRC = RC_SUCCESS;
           ilRCEnd = RC_FAIL;
           break;
        case 9:
           pclTlxEnd = pclTlxEnd + strlen(pclTlxStart);
           ilRC = RC_SUCCESS;
           ilRCEnd = RC_FAIL;
           break;
        case 10:
           pclTlxEnd = pclTlxEnd + strlen(pclTlxStart);
           ilRC = RC_SUCCESS;
           ilRCEnd = RC_FAIL;
           break;
        case 11:
           pclTlxEnd = pclTlxEnd + strlen(pclTlxStart);
           ilRC = RC_SUCCESS;
           ilRCEnd = RC_FAIL;
           break;
        case 12:
           pclTlxEnd = pclTlxEnd + strlen(pclTlxStart);
           ilRC = RC_SUCCESS;
           ilRCEnd = RC_FAIL;
           break;
        case 13:
           pclTlxEnd = pclTlxEnd + strlen(pclTlxStart);
           ilRC = RC_SUCCESS;
           ilRCEnd = RC_FAIL;
           break;
     }
     if (ilRC == RC_SUCCESS)
     {
        clSaveChar = *pclTlxEnd;
        *pclTlxEnd = '\0';
        ilCnt++;
        dbg(TRACE,"SaveTelexes: Found Tlx No. %d\n<%.200s>",ilCnt,pclTlxStart);
        strcpy(pclFieldList,"TIME,CDAT,SERE,STAT,TXT1,TXT2");
        if (ipHeaderTyp == 11)
           sprintf(pclValueList,"%s,%s,I,%c,",pclCdat,pclCdat,FDI_STAT_WRITTEN);
        else
           sprintf(pclValueList,"%s,%s,R,%c,",pclCdat,pclCdat,FDI_STAT_WRITTEN);
        if (strlen(pclTlxStart) <= igTlxTextFieldSize)
        {
           strcpy(pclText,pclTlxStart);
           ChangeCharFromTo(pclText,pcgClientChars,pcgServerChars);
           strcat(pclValueList,pclText);
           strcat(pclValueList,", ");
        }
        else
        {
           strncpy(pclText,pclTlxStart,igTlxTextFieldSize);
           pclText[igTlxTextFieldSize] = '\0';
           ilTextSize = igTlxTextFieldSize;
           while (ilTextSize > 0 && pclText[ilTextSize] != '\n')
              ilTextSize--;
           ilTextSize++;
           pclText[ilTextSize] = '\0';
           ChangeCharFromTo(pclText,pcgClientChars,pcgServerChars);
           strcat(pclValueList,pclText);
           strcat(pclValueList,",");
           if (strlen(pclTlxStart) > ilTextSize+igTlxTextFieldSize)
           {
              strncpy(pclText,&pclTlxStart[ilTextSize],igTlxTextFieldSize);
              pclText[igTlxTextFieldSize] = '\0';
              ilTextSize = igTlxTextFieldSize;
              while (ilTextSize > 0 && pclText[ilTextSize] != '\n')
                 ilTextSize--;
              ilTextSize++;
              pclText[ilTextSize] = '\0';
           }
           else
           {
              strcpy(pclText,&pclTlxStart[ilTextSize]);
           }
           ChangeCharFromTo(pclText,pcgClientChars,pcgServerChars);
           strcat(pclValueList,pclText);
        }
        ilDebugLevel = debug_level;
        debug_level = 0;
#ifdef DB_LOAD_URNO_LIST
        /* UFIS-1485 */
        DB_LoadUrnoList(1,pcgNextTlxUrno,pcgTableKey);
#else
        GetNextValues(pcgNextTlxUrno,1);
#endif

        debug_level = ilDebugLevel;
        if (igMaxTlxUrnoIdx < 1000)
        {
           strcpy(&pcgTlxUrnoList[igMaxTlxUrnoIdx][0],pcgNextTlxUrno);
           igMaxTlxUrnoIdx++;
        }
        ilRC = FdiHandleSql("IBT","TLXTAB","",pclFieldList,pclValueList,
                            pcgTwStart,pcgTwEndNew,igScheduleAction,TRUE);
        strcpy(pcgNextTlxUrno,"");
        strcpy(pcgLastTlxUrno,"");
        *pclTlxEnd = clSaveChar;
        pclTlxStart = pclTlxEnd;
     }
     else
     {
        if (igMaxTlxUrnoIdx == 0)
        {
           dbg(TRACE,"SaveTelexes: No Tlx found");
        }
        else
        {
           ilRC = RC_SUCCESS;
        }
        ilRCEnd = RC_FAIL;
     }
  } /* End of while */

  for (ilI = 0; ilI < igMaxTlxUrnoIdx; ilI++)
  {
     dbg(DEBUG,"SaveTelexes: Tlx URNO No. %d = <%s>",ilI+1,&pcgTlxUrnoList[ilI][0]);
  }

  return ilRC;
} /* End of SaveTelexes */



static int RemoveItemFromList(char *pcpFields, char *pcpValues, char *pcpFieldName)
{
  int ilRC = RC_SUCCESS;
  char pclFields[2000];
  char pclValues[COMPLETE_TELEX_LEN];
  char *pclPtrFieldsS;
  char *pclPtrFieldsE;
  char *pclPtrValuesS;
  char *pclPtrValuesE;

  if (strstr(pcpFields,pcpFieldName) != NULL)
  {
     strcpy(pclFields,pcpFields);
     strcpy(pclValues,pcpValues);
     strcpy(pcpFields,"");
     strcpy(pcpValues,"");
     pclPtrFieldsS = pclFields;
     pclPtrValuesS = pclValues;
     pclPtrFieldsE = strstr(pclFields,",");
     pclPtrValuesE = strstr(pclValues,",");
     while (pclPtrFieldsE != NULL)
     {
        *pclPtrFieldsE = '\0';
        *pclPtrValuesE = '\0';
        if (strcmp(pclPtrFieldsS,pcpFieldName) != 0)
        {
           strcat(pcpFields,pclPtrFieldsS);
           strcat(pcpFields,",");
           strcat(pcpValues,pclPtrValuesS);
           strcat(pcpValues,",");
        }
        pclPtrFieldsS = pclPtrFieldsE + 1;
        pclPtrValuesS = pclPtrValuesE + 1;
        pclPtrFieldsE = strstr(pclPtrFieldsS,",");
        pclPtrValuesE = strstr(pclPtrValuesS,",");
     }
     if (strcmp(pclPtrFieldsS,pcpFieldName) != 0)
     {
        strcat(pcpFields,pclPtrFieldsS);
        strcat(pcpValues,pclPtrValuesS);
     }
     if (pcpFields[strlen(pcpFields)-1] == ',')
     {
        pcpFields[strlen(pcpFields)-1] = '\0';
     }
     if (pcpValues[strlen(pcpValues)-1] == ',')
     {
        pcpValues[strlen(pcpValues)-1] = '\0';
     }
  }

  return ilRC;
} /* End of RemoveItemFromList */


static int UpdateVial(char *pcpFields, char *pcpData, char *pcpFld, char *pcpVia)
{
  int ilRC = RC_SUCCESS;
  char pclFld[128];
  char pclFldVal[128];
  int ilItemNo;
  char pclTmpBuf[128];
  int ilLen;

/*dbg(TRACE,"Before:\n<%s>\n<%s>\n<%s><%s>",pcpFields,pcpData,pcpFld,pcpVia);*/
  dbg(TRACE,"UpdateVial: Field <%s> Via Station <%s>", pcpFld, pcpVia);
  ilItemNo = get_item_no(pcpFields,pcpFld,5) + 1;
  ilLen = get_real_item(pclFldVal,pcpData,ilItemNo);
  ilRC = RemoveItemFromList(pcpFields,pcpData,pcpFld);
  if (strlen(pcpFields) > 0)
  {
     strcat(pcpFields,",");
     strcat(pcpData,",");
  }
  if (strcmp(pcpFld,"LNDD") == 0)
     strcpy(pclFld,"LAND");
  else if (strcmp(pcpFld,"ONBD") == 0)
          strcpy(pclFld,"ONBL");
  else if (strcmp(pcpFld,"ONBE") == 0)
          strcpy(pclFld,"ONBL");
  else if (strcmp(pcpFld,"OFBD") == 0)
          strcpy(pclFld,"OFBL");
  else if (strcmp(pcpFld,"AIRD") == 0)
          strcpy(pclFld,"AIRB");
  else if (strcmp(pcpFld,"ETAE") == 0)
          strcpy(pclFld,"ETOA");
  else if (strcmp(pcpFld,"ETDE") == 0)
          strcpy(pclFld,"ETOD");
  strcat(pcpFields,"VSUP");
  sprintf(pclTmpBuf,"[APC3;%s]%s;%s",pclFld,pcpVia,pclFldVal);
  strcat(pcpData,pclTmpBuf);
/*dbg(TRACE,"After:\n<%s>\n<%s>",pcpFields,pcpData);*/

  return ilRC;
} /* End of UpdateVial */


static int CompressVial(char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char *pclTmpPtr;
  int ilLen;
  int ilCount;
  int ilI;
  char pclCurFld[8];
  char pclCurDat[1024];
  char pclNewFields[4096];
  char pclNewData[4096];
  int ilCnt;
  char pclApc[10][8];
  char pclFld[10][8];
  char pclTim[10][16];
  char pclNewVSUP1[4096];
  char pclNewVSUP2[4096];
  char pclNewVSUP3[4096];

  pclTmpPtr = strstr(pcpFields,"VSUP");
  if (pclTmpPtr != NULL)
  {
     pclTmpPtr += 4;
     pclTmpPtr = strstr(pclTmpPtr,"VSUP");
     if (pclTmpPtr != NULL)
     {
        strcpy(pclNewFields,"");
        strcpy(pclNewData,"");
        ilCnt = 0;
    ilCount=GetNoOfElements(pcpFields,',');
        for (ilI = 1; ilI <= ilCount; ilI++)
        {
           ilLen = get_real_item(pclCurFld,pcpFields,ilI);
           ilLen = get_real_item(pclCurDat,pcpData,ilI);
           if (strcmp(pclCurFld,"VSUP") != 0)
           {
              if (strlen(pclNewFields) > 0)
              {
                 strcat(pclNewFields,",");
                 strcat(pclNewData,",");
              }
              strcat(pclNewFields,pclCurFld);
              strcat(pclNewData,pclCurDat);
           }
           else
           {
              strncpy(&pclApc[ilCnt][0],&pclCurDat[11],3);
              pclApc[ilCnt][3] = '\0';
              strncpy(&pclFld[ilCnt][0],&pclCurDat[6],4);
              pclFld[ilCnt][4] = '\0';
              strncpy(&pclTim[ilCnt][0],&pclCurDat[15],14);
              pclTim[ilCnt][14] = '\0';
              ilCnt++;
           }
        }
        strcpy(pclNewVSUP1,"[APC3;");
        for (ilI = 0; ilI < ilCnt; ilI++)
        {
           strcat(pclNewVSUP1,&pclFld[ilI][0]);
           strcat(pclNewVSUP1,";");
        }
        pclNewVSUP1[strlen(pclNewVSUP1)-1] = '\0';
        strcat(pclNewVSUP1,"]");
        strcpy(pclNewVSUP2,"");
        strcpy(pclNewVSUP3,"");
        if (ilCnt == 3)
        {
           if (strcmp(&pclApc[0][0],&pclApc[1][0]) == 0 &&
               strcmp(&pclApc[1][0],&pclApc[2][0]) != 0)
           {
              sprintf(pclNewVSUP2,"%s;%s;%s;",
                      &pclApc[0][0],&pclTim[0][0],&pclTim[1][0]);
              sprintf(pclNewVSUP3,"|%s;;;%s",
                      &pclApc[2][0],&pclTim[2][0]);
           }
           else
           {
              if (strcmp(&pclApc[0][0],&pclApc[1][0]) != 0 &&
                  strcmp(&pclApc[1][0],&pclApc[2][0]) == 0)
              {
                 sprintf(pclNewVSUP2,"%s;%s;;",
                         &pclApc[0][0],&pclTim[0][0]);
                 sprintf(pclNewVSUP3,"|%s;;%s;%s",
                         &pclApc[1][0],&pclTim[1][0],&pclTim[2][0]);
              }
           }
        }
        else
        {
           if (strcmp(&pclApc[0][0],&pclApc[1][0]) == 0)
           {
              sprintf(pclNewVSUP2,"%s;%s;%s",
                      &pclApc[0][0],&pclTim[0][0],&pclTim[1][0]);
           }
           else
           {
              sprintf(pclNewVSUP2,"%s;%s;",
                      &pclApc[0][0],&pclTim[0][0]);
              sprintf(pclNewVSUP3,"|%s;;%s",
                      &pclApc[1][0],&pclTim[1][0]);
           }
        }
        if (strlen(pclNewFields) > 0)
        {
           strcat(pclNewFields,",");
           strcat(pclNewData,",");
        }
        strcat(pclNewFields,"VSUP");
        strcat(pclNewData,pclNewVSUP1);
        strcat(pclNewData,pclNewVSUP2);
        strcat(pclNewData,pclNewVSUP3);
        strcpy(pcpFields,pclNewFields);
        strcpy(pcpData,pclNewData);
     }
  }

  return ilRC;
} /* End of CompressVial */

/**********************************************************************
  v.1.140 - UFIS-1491 - add CHON, CHOF, They are copies of ONBD, OFBD.
  We are assumming that there will never be CHON/CHOF without
  ONBD/OFBD. This will simplify the changes in CheckTimeValues to
  that of simply copying the final resulting ONBD/OFBD (if any)
  to CHON/CHOF (if present).
  UFIS-1592 - Test CHO1/CHF1 primarily, and CHON/CHOF as well. This
  function can be called by HandleFDIU where rgTlxInfo is outdated, hence
  not possible to call GetCfgFieldName. 
 **********************************************************************
*/

static int CheckTimeValues(char *pcpFields, char *pcpData, char *pcpAdid)
{
  int ilRC = RC_SUCCESS;
  int ilCount;
  int ilI;
  char pclNewDataList[1024];
  char pclFieldName[16];
  char pclFieldValue[128];
  char pclSchHour[4];
  int ilSchHour;
  char pclActHour[4];
  int ilActHour;
  char pclActDay[16];
  char pclNewOFBD[32];
  char pclNewAIRD[32];
  char pclNewETDE[32];
  char pclNewETAE[32];
  char pclNewONBE[32];
  char pclNewONBD[32];
  char pclNewLNDD[32];
  char pclNewCHON[32];
  char pclNewCHOF[32];
  int ilCorrectSequence;

  dbg(DEBUG,"CheckTimeValues: Fields: <%s> <%s>",pcpFields,pcpAdid);
  dbg(DEBUG,"CheckTimeValues: Data  : <%s> <%s>",pcpData,pcpAdid);
  strcpy(pclNewOFBD,"");
  strcpy(pclNewAIRD,"");
  strcpy(pclNewETDE,"");
  strcpy(pclNewETAE,"");
  strcpy(pclNewONBE,"");
  strcpy(pclNewONBD,"");
  strcpy(pclNewLNDD,"");
  strcpy(pclNewCHON,"");
  strcpy(pclNewCHOF,"");

  strcpy(pclNewDataList,"");
  memset(pclSchHour,0x00,sizeof(pclSchHour));
  strncpy(pclSchHour,&pcgFlightSchedTime[8],2);
  ilSchHour = atoi(pclSchHour);
  ilCount = GetNoOfElements(pcpFields,',');
  for (ilI = 1; ilI <= ilCount; ilI++)
  {
     GetDataItem(pclFieldName,pcpFields,ilI,',',""," ");
     GetDataItem(pclFieldValue,pcpData,ilI,',',""," ");
     if (strcmp(pcpAdid,"A") == 0 || strcmp(pcpAdid,"B") == 0)
     {
        if (strcmp(pclFieldName,"LNDD") == 0 || strcmp(pclFieldName,"ONBD") == 0 ||
            strcmp(pclFieldName,"ETAE") == 0 || strcmp(pclFieldName,"ONBE") == 0 ||
            strcmp(pclFieldName,"NXTI") == 0 || strcmp(pclFieldName,"CHO1") == 0 ||
            strcmp(pclFieldName,"CHON") == 0)
        {
           ilRC = CalcNewDay(1,pclFieldName,pclFieldValue,ilSchHour);
        }
        else
        {
           if (strcmp(pclFieldName,"OFBD") == 0 || strcmp(pclFieldName,"AIRD") == 0 ||
               strcmp(pclFieldName,"ETDE") == 0 || strcmp(pclFieldName,"CTOT") == 0 ||
               strcmp(pclFieldName,"CHF1") == 0 || strcmp(pclFieldName,"CHOF") == 0)
           {
              ilRC = CalcNewDay(2,pclFieldName,pclFieldValue,ilSchHour);
           }
        }
     }
     else
     {
        if (strcmp(pclFieldName,"LNDD") == 0 || strcmp(pclFieldName,"ONBD") == 0 ||
            strcmp(pclFieldName,"ETAE") == 0 || strcmp(pclFieldName,"ONBE") == 0 ||
            strcmp(pclFieldName,"CHO1") == 0 || strcmp(pclFieldName,"CHON") == 0)
        {
           ilRC = CalcNewDay(3,pclFieldName,pclFieldValue,ilSchHour);
        }
        else
        {
           if (strcmp(pclFieldName,"OFBD") == 0 || strcmp(pclFieldName,"AIRD") == 0 ||
               strcmp(pclFieldName,"ETDE") == 0 || strcmp(pclFieldName,"CTOT") == 0 ||
               strcmp(pclFieldName,"NXTI") == 0 || strcmp(pclFieldName,"CHF1") == 0 ||
               strcmp(pclFieldName,"CHOF") == 0)
           {
              ilRC = CalcNewDay(1,pclFieldName,pclFieldValue,ilSchHour);
           }
        }
     }
     strcat(pclNewDataList,pclFieldValue);
     strcat(pclNewDataList,",");
     if (strcmp(pclFieldName,"OFBD") == 0)
     {
        strcpy(pclNewOFBD,pclFieldValue);
     }
     if (strcmp(pclFieldName,"AIRD") == 0)
     {
        strcpy(pclNewAIRD,pclFieldValue);
     }
     if (strcmp(pclFieldName,"ETDE") == 0)
     {
        strcpy(pclNewETDE,pclFieldValue);
     }
     if (strcmp(pclFieldName,"ETAE") == 0)
     {
        strcpy(pclNewETAE,pclFieldValue);
     }
     if (strcmp(pclFieldName,"ONBE") == 0)
     {
        strcpy(pclNewONBE,pclFieldValue);
     }
     if (strcmp(pclFieldName,"ONBD") == 0)
     {
        strcpy(pclNewONBD,pclFieldValue);
     }
     if (strcmp(pclFieldName,"LNDD") == 0)
     {
        strcpy(pclNewLNDD,pclFieldValue);
     }
  }
  pclNewDataList[strlen(pclNewDataList)-1] = '\0';
  strcpy(pcpData,pclNewDataList);

  ilCorrectSequence = TRUE;
  if (strcmp(pcpAdid,"A") == 0 || strcmp(pcpAdid,"B") == 0)
  {
     ilRC = CheckSequence(pclNewONBD,pclNewLNDD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewLNDD,pclNewETDE,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewLNDD,pclNewAIRD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewLNDD,pclNewOFBD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewONBD,pclNewETDE,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewONBD,pclNewAIRD,&ilCorrectSequence,'+'); 
     ilRC = CheckSequence(pclNewONBD,pclNewOFBD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewONBE,pclNewETDE,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewONBE,pclNewAIRD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewONBE,pclNewOFBD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewETAE,pclNewETDE,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewETAE,pclNewAIRD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewETAE,pclNewOFBD,&ilCorrectSequence,'+');
     ilRC = CheckSequence(pclNewAIRD,pclNewOFBD,&ilCorrectSequence,'+');
  }
  else
  {
     ilRC = CheckSequence(pclNewOFBD,pclNewAIRD,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewOFBD,pclNewETAE,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewOFBD,pclNewONBE,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewOFBD,pclNewONBD,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewOFBD,pclNewLNDD,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewAIRD,pclNewETAE,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewAIRD,pclNewONBE,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewAIRD,pclNewONBD,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewAIRD,pclNewLNDD,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewETDE,pclNewETAE,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewETDE,pclNewONBE,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewETDE,pclNewONBD,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewETDE,pclNewLNDD,&ilCorrectSequence,'-');
     ilRC = CheckSequence(pclNewLNDD,pclNewONBD,&ilCorrectSequence,'-');
  }
  if (ilCorrectSequence == FALSE)
  {
     strcpy(pclNewDataList,"");
     ilCount = GetNoOfElements(pcpFields,',');
     for (ilI = 1; ilI <= ilCount; ilI++)
     {
        GetDataItem(pclFieldName,pcpFields,ilI,',',""," ");
        GetDataItem(pclFieldValue,pcpData,ilI,',',""," ");
        if (strcmp(pclFieldName,"OFBD") == 0)
        {
           strcpy(pclFieldValue,pclNewOFBD);
        }
        if (strcmp(pclFieldName,"AIRD") == 0)
        {
           strcpy(pclFieldValue,pclNewAIRD);
        }
        if (strcmp(pclFieldName,"ETDE") == 0)
        {
           strcpy(pclFieldValue,pclNewETDE);
        }
        if (strcmp(pclFieldName,"ETAE") == 0)
        {
           strcpy(pclFieldValue,pclNewETAE);
        }
        if (strcmp(pclFieldName,"ONBE") == 0)
        {
           strcpy(pclFieldValue,pclNewONBE);
        }
        if (strcmp(pclFieldName,"ONBD") == 0)
        {
           strcpy(pclFieldValue,pclNewONBD);
        }
        if (strcmp(pclFieldName,"LNDD") == 0)
        {
           strcpy(pclFieldValue,pclNewLNDD);
        } 
        /* Replace CHON/CHOF (or CHO1/CHF1) only if ONBD/OFBD were changed */
        if ((strcmp(pclFieldName,"CHO1") == 0) || (strcmp(pclFieldName,"CHON") == 0))
        {
          if (strlen (pclNewONBD) != 0)
           strcpy(pclFieldValue,pclNewONBD);
        }
        if ((strcmp(pclFieldName,"CHF1") == 0) || (strcmp(pclFieldName,"CHOF") == 0))
        {
          if (strlen (pclNewOFBD) != 0)
           strcpy(pclFieldValue,pclNewOFBD);
        }

        strcat(pclNewDataList,pclFieldValue);
        strcat(pclNewDataList,",");
     }
  
     pclNewDataList[strlen(pclNewDataList)-1] = '\0';
     strcpy(pcpData,pclNewDataList);
  }

  return ilRC;
} /* End of CheckTimeValues */



static int CalcNewDay(int ipArray, char *pcpFieldName, char *pcpFieldValue, int ipSchHour)
{
  int ilRC = RC_SUCCESS;
  char pclActHour[4];
  int ilActHour;
  char pclActDay[16];
  char pclOldFieldValue[32];
  char clDayType;

  if (strlen(pcpFieldValue) == 0)
  {
     return ilRC;
  }

  memset(pclActHour,0x00,sizeof(pclActHour));
  strncpy(pclActHour,&pcpFieldValue[8],2);
  if (strcmp(pclActHour,"24") == 0)
  {
     strcpy(pclOldFieldValue,pcpFieldValue);
     strncpy(pcpFieldValue,pclOldFieldValue,8);
     pcpFieldValue[8] = '\0';
     strcat(pcpFieldValue,"00");
     strcat(pcpFieldValue,&pclOldFieldValue[10]);
  }
  strcpy(pclOldFieldValue,pcpFieldValue);
  memset(pclActDay,0x00,sizeof(pclActDay));
  memset(pclActHour,0x00,sizeof(pclActHour));
  strncpy(pclActHour,&pcpFieldValue[8],2);
  ilActHour = atoi(pclActHour);
  strncpy(pclActDay,pcgFlightSchedTime,8);
  strcat(pclActDay,"120000");
  if (ipArray == 1)
  {
     clDayType = pcgTimeArray1[ipSchHour][ilActHour];
  }
  else
  {
     if (ipArray == 2)
     {
        clDayType = pcgTimeArray2[ipSchHour][ilActHour];
     }
     else
     {
        clDayType = pcgTimeArray3[ipSchHour][ilActHour];
     }
  }
  if (clDayType == 'P')
  {
     ilRC = AddSecondsToCEDATime(pclActDay,86400*(-1),1);
  }
  else
  {
     if (clDayType == 'N')
     {
        ilRC = AddSecondsToCEDATime(pclActDay,86400,1);
     }
  }
  pclActDay[8] = '\0';
  strcat(pclActDay,&pcpFieldValue[8]);
  strcpy(pcpFieldValue,pclActDay);
  dbg(TRACE,"<%s>: Sch=%d, Act=%d, Old/New=<%s>/<%s>",
      pcpFieldName,ipSchHour,ilActHour,pclOldFieldValue,pcpFieldValue);

  return ilRC;
} /* End of CalcNewDay */



static int CheckSequence(char *pcpField1, char *pcpField2, int *ipResult, char cpDir)
{
  int ilRC = RC_SUCCESS;
  char pclActDay[32];
  char pclSaveTime[32];

  if (strlen(pcpField1) == 0 || strlen(pcpField2) == 0)
  {
     return ilRC;
  }

  if (cpDir == '+')
  {
     if (strcmp(pcpField1,pcpField2) < 0)
     {
        dbg(DEBUG,"Wrong Sequence: <%s> <%s>",pcpField1,pcpField2);
        strcpy(pclSaveTime,pcpField2);
        strcpy(pclActDay,pcpField2);
        pclActDay[8] = '\0';
        strcat(pclActDay,"120000");
        ilRC = AddSecondsToCEDATime(pclActDay,86400*(-1),1);
        pclActDay[8] = '\0';
        strcat(pclActDay,&pclSaveTime[8]);
        strcpy(pcpField2,pclActDay);
        *ipResult = FALSE;
     }
     else
     {
        if (strncmp(&pcpField1[6],&pcpField2[6],2) != 0 &&
            strcmp(&pcpField1[8],&pcpField2[8]) > 0)
        {
           dbg(DEBUG,"Wrong Sequence: <%s> <%s>",pcpField1,pcpField2);
           strcpy(pclSaveTime,pcpField2);
           strcpy(pclActDay,pcpField2);
           pclActDay[8] = '\0';
           strcat(pclActDay,"120000");
           ilRC = AddSecondsToCEDATime(pclActDay,86400,1);
           pclActDay[8] = '\0';
           strcat(pclActDay,&pclSaveTime[8]);
           strcpy(pcpField2,pclActDay);
           *ipResult = FALSE;
        }
     }
  }
  else
  {
     if (strcmp(pcpField1,pcpField2) > 0)
     {
        dbg(DEBUG,"Wrong Sequence: <%s> <%s>",pcpField1,pcpField2);
        strcpy(pclSaveTime,pcpField2);
        strcpy(pclActDay,pcpField2);
        pclActDay[8] = '\0';
        strcat(pclActDay,"120000");
        ilRC = AddSecondsToCEDATime(pclActDay,86400,1);
        pclActDay[8] = '\0';
        strcat(pclActDay,&pclSaveTime[8]);
        strcpy(pcpField2,pclActDay);
        *ipResult = FALSE;
     }
     else
     {
        if (strncmp(&pcpField1[6],&pcpField2[6],2) != 0 &&
            strcmp(&pcpField1[8],&pcpField2[8]) < 0)
        {
           dbg(DEBUG,"Wrong Sequence: <%s> <%s>",pcpField1,pcpField2);
           strcpy(pclSaveTime,pcpField2);
           strcpy(pclActDay,pcpField2);
           pclActDay[8] = '\0';
           strcat(pclActDay,"120000");
           ilRC = AddSecondsToCEDATime(pclActDay,86400*(-1),1);
           pclActDay[8] = '\0';
           strcat(pclActDay,&pclSaveTime[8]);
           strcpy(pcpField2,pclActDay);
           *ipResult = FALSE;
        }
     }
  }

  return ilRC;
} /* End of CheckSequence */



static int HandleFDIU(char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[32000];
  char pclTlxUrno[16];
  char pclAftUrno[16];
  char pclAftFlno[16];
  char pclAftStoa[16];
  char pclAftStod[16];
  char pclAftAdid[16];
  char pclAftFtyp[16];
  char pclAftPax1[16];
  char pclAftPax2[16];
  char pclAftPax3[16];
  char pclAftBagw[16];
  int ilLen;
  char pclTable[16];
  char pclFieldList[1024];
  char pclValueList[32000];
  char pclAftSelection[64];
  char pclAftFieldList[2048];
  char pclAftValueList[2048];
  char *pclTmpPtr;

  ilLen = get_real_item(pclAftUrno,pcpSelection,1);
  ilLen = get_real_item(pclTlxUrno,pcpSelection,2);
  if (strlen(pclAftUrno) == 0 || strlen(pclTlxUrno) == 0)
  {
     dbg(TRACE,"HandleFDIU: Invalid parameter: Selection = <%s>",pcpSelection);
     return RC_FAIL;
  }

  strcpy(pclSqlBuf,"SELECT FLNO,STOA,STOD,ADID,FTYP,PAX1,PAX2,PAX3,BAGW FROM AFTTAB WHERE ");
  sprintf(pclSelectBuf,"URNO = %s",pclAftUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"HandleFDIU: <%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",9,",");
     ilLen = get_real_item(pclAftFlno,pclDataBuf,1);
     ilLen = get_real_item(pclAftStoa,pclDataBuf,2);
     ilLen = get_real_item(pclAftStod,pclDataBuf,3);
     ilLen = get_real_item(pclAftAdid,pclDataBuf,4);
     ilLen = get_real_item(pclAftFtyp,pclDataBuf,5);
     ilLen = get_real_item(pclAftPax1,pclDataBuf,6);
     ilLen = get_real_item(pclAftPax2,pclDataBuf,7);
     ilLen = get_real_item(pclAftPax3,pclDataBuf,8);
     ilLen = get_real_item(pclAftBagw,pclDataBuf,9);
     if (strcmp(pclAftAdid,"A") == 0 || strcmp(pclAftAdid,"B") == 0)
     {
        strcpy(pcgFlightSchedTime,pclAftStoa);
     }
     else
     {
        strcpy(pcgFlightSchedTime,pclAftStod);
     }
     sprintf(pclSqlBuf,"UPDATE APXTAB SET FLNU = %s ",pclAftUrno);
     sprintf(pclSelectBuf,"WHERE RURN = %s",pclTlxUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"HandleFDIU: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);

     sprintf(pclSqlBuf,"UPDATE LOATAB SET FLNU = %s,TIME = '%s' ",
             pclAftUrno,pcgFlightSchedTime);
     sprintf(pclSelectBuf,"WHERE RURN = %s",pclTlxUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"HandleFDIU: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     commit_work();
     close_my_cursor(&slCursor);

     memset(pcgTlxMstx,0x00,40);
     strcpy(pcgTlxMstx,pclAftFlno);
     strcat(pcgTlxMstx," ");
     strncat(pcgTlxMstx,pcgFlightSchedTime,4);
     strcat(pcgTlxMstx,".");
     strncat(pcgTlxMstx,&pcgFlightSchedTime[4],2);
     strcat(pcgTlxMstx,".");
     strncat(pcgTlxMstx,&pcgFlightSchedTime[6],2);
     strcat(pcgTlxMstx," ");
     strncat(pcgTlxMstx,&pcgFlightSchedTime[8],2);
     strcat(pcgTlxMstx,":");
     strncat(pcgTlxMstx,&pcgFlightSchedTime[10],2);
     strcat(pcgTlxMstx," ");
     strcat(pcgTlxMstx,pclAftAdid);
     strcat(pcgTlxMstx," ");
     strcat(pcgTlxMstx,pclAftFtyp);
     strcat(pcgTlxMstx," USER");
     strcpy(pclTable,"TLXTAB");
     strcpy(pclFieldList,"FLNU,TIME,STAT,MSTX");
     sprintf(pclValueList,"%s,%s,%c,%s",
             pclAftUrno,pcgFlightSchedTime,FDI_STAT_FLT_UPDATED,pcgTlxMstx);
     ilRC = FdiHandleSql("URT",pclTable,pclTlxUrno,
                         pclFieldList,pclValueList,
                         pcgTwStart,pcgTwEndNew,igScheduleAction,TRUE);
     strcpy(pcgTlxMstx," ");

     strcpy(pclSqlBuf,"SELECT BEME FROM TLXTAB WHERE ");
     sprintf(pclSelectBuf,"URNO = %s",pclTlxUrno);
     strcat(pclSqlBuf,pclSelectBuf);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"HandleFDIU: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
     {
        if (strlen(pclDataBuf) > 4)
        {
           dbg(DEBUG,"HandleFDIU: BEME = <%s>",pclDataBuf);
           MakeTokenList(pclAftFieldList,pclDataBuf,",#",',');
           pclTmpPtr = strstr(pclAftFieldList,"/");
           if (pclTmpPtr != NULL)
           {
              *pclTmpPtr = '\0';
              pclTmpPtr++;
              strcpy(pclAftValueList,pclTmpPtr);
              if (strlen(pclAftPax1) != 0 || strlen(pclAftPax2) != 0 ||
                  strlen(pclAftPax3) != 0)
              {
                 ilRC = RemoveItemFromList(pclAftFieldList,pclAftValueList,"PAXT");
              }
              if (strlen(pclAftBagw) != 0)
              {
                 ilRC = RemoveItemFromList(pclAftFieldList,pclAftValueList,"CGOT");
              }
              ilRC = CheckTimeValues(pclAftFieldList,pclAftValueList,pclAftAdid);
              sprintf(pclAftSelection,"WHERE URNO = %s",pclAftUrno);
              ilRC = SendCedaEvent(igAftReceiver,0,pcgDestName,pcgRecvName,
                                   pcgTwStart,pcgTwEndNew,
                                   "UFR","AFTTAB",pclAftSelection,
                                   pclAftFieldList,pclAftValueList,"",4,NETOUT_NO_ACK);
              dbg(DEBUG,"HandleFDIU: Selection <%s>",pclAftSelection);
              dbg(DEBUG,"HandleFDIU: FieldList <%s>",pclAftFieldList);
              dbg(DEBUG,"HandleFDIU: ValueList <%s>",pclAftValueList);
              dbg(DEBUG,"HandleFDIU: Sent <UFR>. Returned <%d>",ilRC);
           }
           else
           {
              dbg(TRACE,"HandleFDIU: Invalid data in BEME field of TLX:\n<%s>",
                  pclAftFieldList);
           }
        }
     }
  }
  else
  {
     dbg(TRACE,"HandleFDIU: Flight not found");
     ilRC = RC_FAIL;
  }

  (void) tools_send_info_flag(igQueOut,0,"fdihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                              "FDIU","","","","",0);

  return ilRC;
} /* End of HandleFDIU */



static int ProcessOldTelexes()
{
  int ilRC = RC_SUCCESS;
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[32000];
  char pclUrno[32];
  char pclTxt1[4048];
  char pclTxt2[4048];
  int ilLen;
  char pclData[32000];
  char pclFields[128];
  char pclSelect[128];
  int ilHeaderTyp;
  int ilCount;
  char *pclDataPar;
  char pclTime[16];
  char pclRalFields[128];
  char pclRalData[1024];
  char pclRalNmsg[16];

  if (igStopTelexInterpretation == TRUE)
  {
     dbg(DEBUG,"ProcessOldTelexes: No search for old telexes, because telex interpretation is stopped");
     return ilRC;
  }

  if (igSendMsgToAlerterForPAL_PSM == TRUE)
  {
     strcpy(pclSqlBuf,"SELECT URNO FROM RALTAB WHERE PROC = 'FDIHDL'");
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"ProcessOldTelexes: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     close_my_cursor(&slCursor);
     if (ilRCdb != DB_SUCCESS)
     {
        TimeToStr(pclTime,time(NULL));
        strcpy(pclRalFields,"PROC,NMSG,TMSG,STAT,CDAT,USEC");
        sprintf(pclRalData,"FDIHDL,PROC_NAME,Handler to process PAL/CAL and PSM messages,U,%s,FDIHDL",
                pclTime);
        ilRC = FdiHandleSql("IBT","RALTAB","",pclRalFields,pclRalData,
                            pcgTwStart,pcgTwEndNew,FALSE,TRUE);
        strcpy(pclRalFields,"PROC,NMSG,TMSG,STAT,PRIO,CDAT,USEC");
        strcpy(pclRalData,"FDIHDL,PAL_PAX_UPD,PAL/CAL update for passenger %s on flight %s at %s,U,2,");
        strcat(pclRalData,pclTime);
        strcat(pclRalData,",FDIHDL");
        ilRC = FdiHandleSql("IBT","RALTAB","",pclRalFields,pclRalData,
                            pcgTwStart,pcgTwEndNew,FALSE,TRUE);
        strcpy(pclRalData,"FDIHDL,PSM_PAX_CFIRM,PSM confirmation for passenger %s on flight %s at %s,U,2,");
        strcat(pclRalData,pclTime);
        strcat(pclRalData,",FDIHDL");
        ilRC = FdiHandleSql("IBT","RALTAB","",pclRalFields,pclRalData,
                            pcgTwStart,pcgTwEndNew,FALSE,TRUE);
     }
     strcpy(pclSqlBuf,"SELECT URNO,NMSG,TMSG FROM RALTAB WHERE PROC = 'FDIHDL'");
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"ProcessOldTelexes: <%s>",pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     while (ilRCdb == DB_SUCCESS)
     {
        BuildItemBuffer(pclDataBuf,"",3,",");
        get_real_item(pclRalNmsg,pclDataBuf,2);
        TrimRight(pclRalNmsg);
        if (strcmp(pclRalNmsg,"PAL_PAX_UPD") == 0)
        {
           get_real_item(pcgRalUrnoPal,pclDataBuf,1);
           TrimRight(pcgRalUrnoPal);
           get_real_item(pcgRalTextPal,pclDataBuf,3);
           TrimRight(pcgRalTextPal);
        }
        else if (strcmp(pclRalNmsg,"PSM_PAX_CFIRM") == 0)
        {
           get_real_item(pcgRalUrnoPsm,pclDataBuf,1);
           TrimRight(pcgRalUrnoPsm);
           get_real_item(pcgRalTextPsm,pclDataBuf,3);
           TrimRight(pcgRalTextPsm);
        }
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
     }
     close_my_cursor(&slCursor);
  }

  dbg(DEBUG,"ProcessOldTelexes: Now search not yet processed telexes");
  ilCount = 0;
  TimeToStr(pclTime,time(NULL));
  AddSecondsToCEDATime(pclTime,-3600,1);
  strcpy(pcgTwStart,"TW,1,2,3");
  sprintf(pcgTwEnd,"%s,%s,STXHDL",pcgHomeAP,pcgTABEnd);
  sprintf(pcgTwEndNew,"%s,%s,FDIHDL",pcgHomeAP,pcgTABEnd);
  strcpy(pcgRecvName,"EXCO1");
  strcpy(pcgDestName,"SITA1");
  igQueOut = 9999;
  strcpy(pclFields,"WHAT EVER YOU NEED,1,2,3,4");
  strcpy(pclSqlBuf,"SELECT URNO,TXT1,TXT2 FROM TLXTAB WHERE ");
  sprintf(pclSelectBuf,"TIME > '%s' AND STAT = '%c' AND SERE = 'R'",
          pclTime,FDI_STAT_WRITTEN);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"ProcessOldTelexes: <%s>",pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",3,",");
     ilLen = get_real_item(pclUrno,pclDataBuf,1);
     ilLen = get_real_item(pclTxt1,pclDataBuf,2);
     ilLen = get_real_item(pclTxt2,pclDataBuf,3);
     strcpy(pclData,pclTxt1);
     strcat(pclData,pclTxt2);
     ChangeCharFromTo(pclData,pcgServerChars,pcgClientChars);
     ilHeaderTyp = 0;
     if (strstr(pclData,"ZCZC") != 0)
     {
        ilHeaderTyp = 1;
        strcpy(pclSelect,"TELEX,1");
     }
     else
     {
        if (strstr(pclData,"Message RCV") != 0)
        {
           ilHeaderTyp = 2;
           strcpy(pclSelect,"TELEX,2");
        }
        else
        {
           if (strstr(pclData,"\177RCV") != 0)
           {
              ilHeaderTyp = 3;
              strcpy(pclSelect,"TELEX,3");
           }
           else
           {
              if (strstr(pclData,"RCVD") != 0)
              {
                 ilHeaderTyp = 4;
                 strcpy(pclSelect,"TELEX,4");
              }
           }
        }
     }
     if (ilHeaderTyp != 0)
     {
        dbg(DEBUG,"ProcessOldTelexes: Process old Telex\n<%s>",pclData);
        pclDataPar = pclData;
        igMaxTlxUrnoIdx = 1;
        igCurTlxUrnoIdx = 0;
        strcpy(&pcgTlxUrnoList[0][0],pclUrno);
        ilRC = HandleFDI("TLXTAB",pclFields,&pclDataPar,pclSelect,FALSE);
        igMaxTlxUrnoIdx = 0;
        ilCount++;
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  if (ilCount > 0)
  {
     dbg(DEBUG,"ProcessOldTelexes: %d old telexes processed",ilCount);
  }
  else
  {
     dbg(DEBUG,"ProcessOldTelexes: No old telexes found");
  }

  return ilRC;
} /* End of ProcessOldTelexes */


static int CheckTelexTime(char *pcpData, int ipHeaderType)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "CheckTelexTime:";
  char pclCurrentTime[32];
  char pclCurrentTimePrevDay[32];
  char pclCurrentTimeNextDay[32];
  char pclCurrDay[4];
  char pclPrevDay[4];
  char pclNextDay[4];
  char pclCurrHour[4];
  char pclCurrMin[4];
  int ilCurrDay;
  int ilNextDay;
  int ilPrevDay;
  int ilCurrHour;
  int ilCurrMin;
  int ilLen;
  int ilPos;
  int ilCol;
  char pclLine[256];
  char pclLine2[256];
  char pclTlxTime[256];
  char pclTlxDay[4];
  char pclTlxHour[4];
  char pclTlxMin[4];
  int ilTlxDay;
  int ilTlxHour;
  int ilTlxMin;
  char *pclTmpPtr;
  int ilTimeDiff;
  int ilFound;
  char *pclData;

  strcpy(pcgTlxOriginator,"");
  strcpy(pclCurrentTime,GetTimeStamp());
  strcpy(pclCurrentTimePrevDay,pclCurrentTime);
  AddSecondsToCEDATime(pclCurrentTimePrevDay,-86400,1);
  strcpy(pclCurrentTimeNextDay,pclCurrentTime);
  AddSecondsToCEDATime(pclCurrentTimeNextDay,86400,1);
  memset(pclCurrDay,0,4);
  memset(pclPrevDay,0,4);
  memset(pclNextDay,0,4);
  memset(pclCurrHour,0,4);
  memset(pclCurrMin,0,4);
  strncpy(pclCurrDay,&pclCurrentTime[6],2);
  strncpy(pclPrevDay,&pclCurrentTimePrevDay[6],2);
  strncpy(pclNextDay,&pclCurrentTimeNextDay[6],2);
  strncpy(pclCurrHour,&pclCurrentTime[8],2);
  strncpy(pclCurrMin,&pclCurrentTime[10],2);
  ilCurrDay = atoi(pclCurrDay);
  ilPrevDay = atoi(pclPrevDay);
  ilNextDay = atoi(pclNextDay);
  ilCurrHour = atoi(pclCurrHour);
  ilCurrMin = atoi(pclCurrMin);
  dbg(TRACE,"%s Current Day = <%d><%d><%d> , Hour = <%d> , Min = <%d>",
      pclFunc,ilCurrDay,ilPrevDay,ilNextDay,ilCurrHour,ilCurrMin);
  igTlxTimeFound = FALSE;
  strcpy(pclTlxTime,"");
  ilFound = FALSE;
  pclData = pcpData;
  if (ipHeaderType == 12)
  {
     ilRC = RC_FAIL;
     pclTmpPtr = strstr(pclData,"=MSGID");
     if (pclTmpPtr != NULL)
     {
        pclTmpPtr = strstr(pclTmpPtr,"\n");
        if (pclTmpPtr != NULL)
        {
           pclTmpPtr++;
           CopyLine(pclLine,pclTmpPtr);
           ilRC = RC_SUCCESS;
        }
     }
     pclTmpPtr = strstr(pclData,"=ORIGIN");
     if (pclTmpPtr != NULL)
     {
        pclTmpPtr = strstr(pclTmpPtr,"\n");
        if (pclTmpPtr != NULL)
        {
           pclTmpPtr++;
           CopyLine(pcgTlxOriginator,pclTmpPtr);
        }
     }
  }
  else if (ipHeaderType == 6)
  {
     ilRC = RC_FAIL;
     pclTmpPtr = strstr(pclData,"(");
     if (pclTmpPtr != NULL)
     {
        pclTmpPtr -= 5;
        while (pclTmpPtr > pclData && *pclTmpPtr != '\n')
           pclTmpPtr--;
        if (*pclTmpPtr == '\n')
        {
           pclTmpPtr++;
           CopyLine(pclLine,pclTmpPtr);
           ilRC = RC_SUCCESS;
        }
     }
  }
  else
  {
     ilRC = FindItemInList(pclData,"\n.",'\n',&ilLen,&ilCol,&ilPos);
     while (ilRC == RC_SUCCESS && ilFound == FALSE)
     {
        if (*(pclData+ilPos+2) != ' ')
        {
           ilFound = TRUE;
        }
        else
        {
           pclData = pclData + ilPos + 2;
           ilRC = FindItemInList(pclData,"\n.",'\n',&ilLen,&ilCol,&ilPos);
        }
     }
     if (ilRC != RC_SUCCESS)
     {
        if (*pcpData == '.')
        {
           ilPos = 0;
           ilRC = RC_SUCCESS;
        }
     }
  }
  strcpy(pcgTlxTimeForPal," ");
  if (ilRC == RC_SUCCESS)
  {
     if (ipHeaderType == 12 || ipHeaderType == 6)
        ilRC = strlen(pclLine);
     else
        ilRC = GetDataItem(pclLine,pclData+ilPos+1,1,'\n',"","  ");
     strcpy(pcgTimeLine,pclLine);
     TrimRight(pcgTimeLine);
     dbg(TRACE,"%s Time Line = <%s>",pclFunc,pcgTimeLine);
     if (ilRC > 1)
     {
        if (ipHeaderType != 12 && ipHeaderType != 6)
        {
           ilRC = MakeTokenList(pclLine2,pclLine," ",',');
           ilRC = GetDataItem(pcgTlxOriginator,pclLine2,1,',',"","\0\0");
           ilRC = GetDataItem(pclLine,pclLine2,2,',',"","\0\0");
           if (strlen(pclLine) < 6)
           {
              ilRC = GetDataItem(pclLine,pclLine2,3,',',"","\0\0");
           }
        }
        if (strlen(pclLine) > 0)
        {
           if (ipHeaderType == 12 || ipHeaderType == 6)
           {
              strncpy(pclTlxTime,pclLine,6);
              pclTlxTime[6] = '\0';
           }
           else
           {
              if (pclLine[0] == 'T')
              {
                 strcpy(pclTlxTime,&pclLine[1]);
              }
              else
              {
                 strcpy(pclTlxTime,pclLine);
              }
              if (CheckChar(pclTlxTime,'N',6) == RC_SUCCESS)
              {
                 pclTmpPtr = pclTlxTime;
              }
              else
              {
                 pclTmpPtr = strstr(pclTlxTime,"/");
                 if (pclTmpPtr != NULL)
                 {
                    pclTmpPtr++;
                    if (pclTmpPtr[0] == 'T')
                    {
                       pclTmpPtr++;
                    }
                 }
                 else
                 {
                    if (strlen(pclTlxTime) == 8)
                       pclTmpPtr = pclTlxTime + 2;
                    else
                       pclTmpPtr = pclTlxTime;
                 }
              }
              strcpy(pclTlxTime,pclTmpPtr);
              pclTlxTime[6] = '\0';
           }
           if (strlen(pclTlxTime) == 6 && CheckChar(pclTlxTime,'N',6) == RC_SUCCESS)
           {
              strcpy(pcgTlxTimeForPal,pclTlxTime);
              memset(pclTlxDay,0,4);
              memset(pclTlxHour,0,4);
              memset(pclTlxMin,0,4);
              strncpy(pclTlxDay,&pclTlxTime[0],2);
              strncpy(pclTlxHour,&pclTlxTime[2],2);
              strncpy(pclTlxMin,&pclTlxTime[4],2);
              ilTlxDay = atoi(pclTlxDay);
              ilTlxHour = atoi(pclTlxHour);
              ilTlxMin = atoi(pclTlxMin);
              dbg(TRACE,"%s Telex Day = <%d> , Hour = <%d> , Min = <%d>",pclFunc,ilTlxDay,ilTlxHour,ilTlxMin);
              igTlxTimeFound = TRUE;
              igTlxTooOld = FALSE;
              if (ilCurrDay == ilTlxDay)
              {
                 ilTimeDiff = (ilCurrHour * 60 + ilCurrMin) - (ilTlxHour * 60 + ilTlxMin);
              }
              else
              {
                 if (ilPrevDay == ilTlxDay)
                 {
                    ilTimeDiff = (1440 + ilCurrHour * 60 + ilCurrMin) - (ilTlxHour * 60 + ilTlxMin);
                 }
                 else
                 {
                    if (ilNextDay == ilTlxDay)
                    {
                       ilTimeDiff = (ilCurrHour * 60 + ilCurrMin) - (1440 + ilTlxHour * 60 + ilTlxMin);
                    }
                    else
                    {
                       igTlxTooOld = TRUE;
                       dbg(TRACE,"%s Telex time is out of range!!!",pclFunc);
                    }
                 }
              }
              if (igTlxTooOld == FALSE)
              {
                 if (ilTimeDiff >= 0)
                 {
                    if (ilTimeDiff > igMaxTimeDiff)
                    {
                       igTlxTooOld = TRUE;
                       dbg(TRACE,"%s Telex time is too old!!! Diff = %d min",pclFunc,ilTimeDiff);
                    }
                 }
                 else
                 {
                    ilTimeDiff = ilTimeDiff * (-1);
                    if (ilTimeDiff > igMaxTimeDiff)
                    {
                       igTlxTooOld = TRUE;
                       dbg(TRACE,"%s Telex time is too much in future!!! Diff = -%d min",pclFunc,ilTimeDiff);
                    }
                    else
                    {
                       dbg(TRACE,"%s Telex time is negative but accepted!!!",pclFunc);
                    }
                 }
              }
           }
        }
     }
  }
  if (igTlxTimeFound == FALSE)
  {
     igTlxTooOld = FALSE;
     dbg(TRACE,"%s Telex time (%s) not found!!!",pclFunc,pclTlxTime);
  }
  if (igTlxTooOld == FALSE)
  {
     dbg(TRACE,"%s Telex Time accepted",pclFunc);
  }
  dbg(TRACE,"%s Telex Originator Address = <%s>",pclFunc,pcgTlxOriginator);

  return ilRC;
} /* End of CheckTelexTime */



static void ConvertFlightLine(char *pcpLine)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "ConvertFlightLine:";
  char pclFlight[256];
  char pclLine[256];
  int ilI,ilJ;
  int ilPos;
  char pclTable[8];
  int ilCount;
  char pclFlightNo[16];
  char pclTime[16];
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[1024];
  char pclStart[16];
  char pclEnd[16];
  int ilLen;
  char pclAlc2[8];
  char pclAlc3[8];
  char pclFltn[8];
  char pclFlns[8];

  if (igAllowCallSign == FALSE)
     return;

  memset(pclFlight,0x00,256);
  memset(pclLine,0x00,256);
  ilPos = -1;
  for (ilI = 0; ilI < strlen(pcpLine) && ilPos < 0; ilI++)
  {
     if (isalnum(pcpLine[ilI]))
        pclFlight[ilI] = pcpLine[ilI];
     else
        ilPos = ilI;
  }
  /* Check if a flight with this CallSign exists */
  ilRC = RC_FAIL;
  ilCount = 0;
  strcpy(pclStart,GetTimeStamp());
  strcpy(pclEnd,pclStart);
  ilRC = AddSecondsToCEDATime(pclStart,240*60*(-1),1);
  ilRC = AddSecondsToCEDATime(pclEnd,1080*60,1);
  strcpy(pclSqlBuf,"SELECT ALC2,ALC3,FLTN,FLNS FROM AFTTAB WHERE ");
  sprintf(pclSelectBuf,"((TIFA BETWEEN '%s' AND '%s' AND DES3 = '%s' AND ADID = 'A') OR (TIFD BETWEEN '%s' AND '%s' AND ORG3 = '%s' AND ADID = 'D')) AND FTYP NOT IN ('T','G') AND CSGN = '%s'",
          pclStart,pclEnd,pcgHomeAP,pclStart,pclEnd,pcgHomeAP,pclFlight);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",4,",");
     ilLen = get_real_item(pclAlc2,pclDataBuf,1);
     ilLen = get_real_item(pclAlc3,pclDataBuf,2);
     ilLen = get_real_item(pclFltn,pclDataBuf,3);
     ilLen = get_real_item(pclFlns,pclDataBuf,4);
     if (isdigit(pclFlight[2]))
        strcpy(pclFlightNo,pclAlc2);
     else
        strcpy(pclFlightNo,pclAlc3);
     strcat(pclFlightNo,pclFltn);
     strcat(pclFlightNo,pclFlns);
     dbg(DEBUG,"%s Found Flight: <%s,%s,%s,%s,%s>",
         pclFunc,pclAlc2,pclAlc3,pclFltn,pclFlns,pclFlightNo);
     ilRC = RC_SUCCESS;
     ilCount = 1;
  }
  else
  {
     AppendBlanks(pclFlight,9);
     TimeToStr(pclTime,time(NULL));
     sprintf(pclTable,"CLS%s",pcgTABEnd);
     ilCount = 0;
     ilRC = syslibSearchValidDbData(pclTable,"CALL",pclFlight,"FLNO",pclFlightNo,
                                    &ilCount,"\n","VPFR,VPTO",pclTime);
  }
  if (ilRC == RC_SUCCESS && ilCount == 1)
  {
     ilJ = 0;
     for (ilI = 0; ilI < strlen(pclFlightNo); ilI++)
     {
        if (pclFlightNo[ilI] != ' ')
        {
           pclLine[ilJ] = pclFlightNo[ilI];
           ilJ++;
        }
     }
     if (ilPos >= 0)
        strcat(pclLine,&pcpLine[ilPos]);
     strcpy(pcpLine,pclLine);
     dbg(DEBUG,"%s Converted to: <%s>",pclFunc,pcpLine);
  }

  return;
} /* End of ConvertFlightLine */



static int GetMSTX()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetMSTX:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelectBuf[1024];
  char pclDataBuf[1024];
  int ilLen;
  char pclFlno[16];
  char pclTime[16];
  char pclAdid[4];
  char pclFtyp[4];

  strcpy(pclSqlBuf,"SELECT FLNO,STOA,STOD,ADID,FTYP FROM AFTTAB WHERE ");
  sprintf(pclSelectBuf,"URNO = %s",pcgAftUrno);
  strcat(pclSqlBuf,pclSelectBuf);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",5,",");
     ilLen = get_real_item(pclFlno,pclDataBuf,1);
     ilLen = get_real_item(pclAdid,pclDataBuf,4);
     ilLen = get_real_item(pclFtyp,pclDataBuf,5);
     if (*pclAdid == 'A')
        ilLen = get_real_item(pclTime,pclDataBuf,2);
     else
        ilLen = get_real_item(pclTime,pclDataBuf,3);
     memset(pcgTlxMstx,0x00,40);
     strcpy(pcgTlxMstx,pclFlno);
     strcat(pcgTlxMstx," ");
     strncat(pcgTlxMstx,pclTime,4);
     strcat(pcgTlxMstx,".");
     strncat(pcgTlxMstx,&pclTime[4],2);
     strcat(pcgTlxMstx,".");
     strncat(pcgTlxMstx,&pclTime[6],2);
     strcat(pcgTlxMstx," ");
     strncat(pcgTlxMstx,&pclTime[8],2);
     strcat(pcgTlxMstx,":");
     strncat(pcgTlxMstx,&pclTime[10],2);
     strcat(pcgTlxMstx," ");
     strcat(pcgTlxMstx,pclAdid);
     strcat(pcgTlxMstx," ");
     strcat(pcgTlxMstx,pclFtyp);
  }
  close_my_cursor(&slCursor);

  return ilRC;
} /* End of GetMSTX */



static int HandleFTX(char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleFTX:";
  char pclTlxTypes[1024];

  ilRC = iGetConfigRow(pcgCfgFile,"TELEX","TlxTypes",CFG_STRING,pclTlxTypes);
  if (ilRC == RC_SUCCESS)
  {
     dbg(DEBUG,"%s TlxTypes returned <%s>",pclFunc,pclTlxTypes);
  }
  else
  {
     ilRC = iGetConfigRow(pcgCfgFile,"TELEX","commands",CFG_STRING,pclTlxTypes);
     dbg(DEBUG,"%s commands returned <%s>",pclFunc,pclTlxTypes);
  }
  (void) tools_send_info_flag(igQueOut,0,"fdihdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                              "FTX","","","",pclTlxTypes,0);

  return ilRC;
} /* End of HandleFTX */



static int HandleDCFTAB()
{
    T_CFG_ARRAY_ELEMENT *prlCfgArrElem;
    T_FIELDLIST *prlCfgField;
    T_TLXRESULT *prlTlxPtr;
    int ilDcd1No;
    int ilDcd2No;
    int ilDtd1No;
    int ilDtd2No;
    int ilRC = RC_SUCCESS;
    int ilRCdb = DB_SUCCESS;
    int ilNoRecs;
    int i,j;
    int ilCount;
    int ilSearchStage = 0;
    int ilNumDCFFields;
    short slFkt;
    short slCursor;
    char pclDcd1[16];
    char pclDcd2[16];
    char pclDtd1[8];
    char pclDtd2[8];
    char pclTlxUrno[12] = "\0";
    char pclSqlBuf[1024];
    char pclSqlWhere[1024];
    char pclDataBuf[1024];
    char pclFieldList[1024];
    char pclDataList[2048];
    char pclALC3[4];
    char pclURNO[MAX_DL_REASON][12];    /* field of DCFTAB - URNO */
    char pclDURN[MAX_DL_REASON][12];    /* field of DCFTAB - DURN */
    char pclDURA[MAX_DL_REASON][8];     /* field of DCFTAB - DURA */
    char pclSTAT[MAX_DL_REASON];
    char pclFillALC3[MAX_DL_REASON+1];
    char pclUDEN[12];
    char pclTmpIn[512];
    char pclTmpOut[512];
    char pclCurrentTime[16];
    char pclFunc[] = "HandleDCFTAB:";

    /* Get current timestamp */
    TimeToStr(pclCurrentTime,time(NULL));

    /* Not sure why, but just follow as per original code */
    strcpy(pclDcd1,"");
    strcpy(pclDcd2,"");
    strcpy(pclDtd1,"");
    strcpy(pclDtd2,"");
    ilDcd1No = get_item_no(pcgAftFieldList,"DCD1",5) + 1;
    ilDcd2No = get_item_no(pcgAftFieldList,"DCD2",5) + 1;
    ilDtd1No = get_item_no(pcgAftFieldList,"DTD1",5) + 1;
    ilDtd2No = get_item_no(pcgAftFieldList,"DTD2",5) + 1;
    if (ilDcd1No <= 0 && ilDcd2No <= 0 && ilDtd1No <= 0 && ilDtd2No <= 0)
        return RC_SUCCESS;

    if( igHandleDelaySubCode == TRUE )
    {
        strcpy( pclTlxUrno, "0" );
        if( igCurTlxUrnoIdx > 0 )
            sprintf( pclTlxUrno, "%s", pcgTlxUrnoList[igCurTlxUrnoIdx-1] );

        /* Get ALC3 from telex */
        igArrayPtr = 0;
        strcpy(pclALC3,"");
        prlCfgArrElem = CCSArrayGetFirst(prgMasterCmpCfgArray);
        while (prlCfgArrElem != NULL && strlen(pclALC3) == 0)
        {
            if (prlCfgArrElem->MasterKey != NULL)
            {
                prlCfgField = CCSArrayGetFirst(prlCfgArrElem->MasterKey);
                while (prlCfgField != NULL && strlen(pclALC3) == 0)
                {
                    if ((prlTlxPtr = CCSArrayGetData(prgMasterTlx,prlCfgField->DName)) != NULL )
                    {
                        if (strcmp(prlCfgField->DName,"Alc") == 0)
                            strcpy(pclALC3,prlTlxPtr->FValue);
                    }
                    prlCfgField = CCSArrayGetNext(prlCfgArrElem->MasterKey);
                } /* end of while  */
            } /* end of if  */
            prlCfgArrElem = CCSArrayGetNext(prgMasterCmpCfgArray);
        } /* end of while  */
        dbg( DEBUG, "%s ALC3 from telex <%s>", pclFunc, pclALC3 );
    }

    memset( pclFillALC3, 'Y', sizeof(pclFillALC3) );
    pclFillALC3[MAX_DL_REASON] = '\0';      /* jus to line up the memory */
    /* Filled up the DENTAB URNO for the delay code received */
    for( i = 0; i < igNoDelCodes; i++ )
    {
        memset( pclTmpOut, 0, sizeof(pclTmpOut) );
        ilCount = 1;
        ilRC = RC_FAIL;

        if( strlen(rgDelayReas[i].pclDelCode) <= 0 )
            continue;

        if( igHandleDelaySubCode == TRUE )
        {
            if( strlen(rgDelayReas[i].pclSubCode) <= 0 )
                strcpy( rgDelayReas[i].pclSubCode, " " );

            /* Search based on Sub-delay code only */
            sprintf(pclTmpIn,"%s,%s,%s",rgDelayReas[i].pclDelCode, rgDelayReas[i].pclSubCode, pclALC3 );
            ilRC = syslibSearchDbData("DENTAB","DECA,DECS,ALC3",pclTmpIn,
                                      "URNO,DENA,DECN,DECA",pclTmpOut,&ilCount,"\n") ;
            ilSearchStage = 1;
            if( ilRC != RC_SUCCESS )
            {
                ilRC = syslibSearchDbData("DENTAB","DECN,DECS,ALC3",pclTmpIn,
                                          "URNO,DENA,DECN,DECA",pclTmpOut,&ilCount,"\n") ;
                ilSearchStage = 2;
            }
            if( ilRC != RC_SUCCESS )
            {
                sprintf(pclTmpIn,"%s,%s, ",rgDelayReas[i].pclDelCode, rgDelayReas[i].pclSubCode );
                ilRC = syslibSearchDbData("DENTAB","DECA,DECS,ALC3",pclTmpIn,
                                          "URNO,DENA,DECN,DECA",pclTmpOut,&ilCount,"\n") ;
                ilSearchStage = 3;
                if( ilRC != RC_SUCCESS )
                {
                    ilRC = syslibSearchDbData("DENTAB","DECN,DECS,ALC3",pclTmpIn,
                                              "URNO,DENA,DECN,DECA",pclTmpOut,&ilCount,"\n") ;
                    ilSearchStage = 4;
                }
                if( ilRC == RC_SUCCESS )
                    pclFillALC3[i] = 'N';
            }
        }
        else
        {
            /* Main delay code only */
            ilRC = syslibSearchDbData("DENTAB","DECA",rgDelayReas[i].pclDelCode,
                                      "URNO,DENA,DECN,DECA",pclTmpOut,&ilCount,"\n") ;
            ilSearchStage = 5;
            if( ilRC != RC_SUCCESS )
            {
                ilRC = syslibSearchDbData("DENTAB","DECN",rgDelayReas[i].pclDelCode,
                                          "URNO,DENA,DECN,DECA",pclTmpOut,&ilCount,"\n") ;
                ilSearchStage = 6;
            }
        }
        if( ilRC == RC_SUCCESS && strlen(pclTmpOut) > 0 )
        {
            get_real_item(rgDelayReas[i].pclDenUrno,pclTmpOut,1); TrimRight(rgDelayReas[i].pclDenUrno);
            get_real_item(rgDelayReas[i].pclDelReason,pclTmpOut,2); TrimRight(rgDelayReas[i].pclDelReason);
            get_real_item(rgDelayReas[i].pclDelNumeric,pclTmpOut,3); TrimRight(rgDelayReas[i].pclDelNumeric);
            get_real_item(rgDelayReas[i].pclDelAlpha,pclTmpOut,4); TrimRight(rgDelayReas[i].pclDelAlpha);
        }
	else
	{
            strcpy(rgDelayReas[i].pclDenUrno,"0");
            strcpy(rgDelayReas[i].pclDelReason,"Error! Delay code not found in basic data");
            pclFillALC3[i] = 'N';
	}
        dbg( DEBUG, "%s SearchStage <%d> DELAY CODE received <%s> subcode <%s> DURA <%s> UDEN <%s> REAS <%s>",
                    pclFunc, ilSearchStage, rgDelayReas[i].pclDelCode,
                    rgDelayReas[i].pclSubCode, rgDelayReas[i].pclDelValue,
                    rgDelayReas[i].pclDenUrno,rgDelayReas[i].pclDelReason );
        if( igSyncDECADECN == TRUE )
            dbg( DEBUG, "%s BASIC DATA DECA <%s> DECN <%s>", pclFunc, rgDelayReas[i].pclDelAlpha, rgDelayReas[i].pclDelNumeric );
    } /* for loop */

    memset( pclDataList, 0, sizeof(pclDataList) );
    memset( pclSTAT, '-', sizeof(pclSTAT) );
    memset( pclURNO, 0, sizeof(pclURNO) );
    memset( pclDURN, 0, sizeof(pclDURN) );
    memset( pclDURA, 0, sizeof(pclDURA) );

    sprintf( pclSqlWhere,"FURN = %s ", pcgAftUrno );
    if( igEraseDCF == FALSE && igUpdateUSERRecords == FALSE )
        strcat( pclSqlWhere," AND USEC = 'TELEX'" );

    strcpy( pclFieldList,"URNO,DURN,DURA,FURN,READ,REMA,CDAT,HOPO,LSTU,USEC,USEU" );
    if( igHandleDelaySubCode == TRUE )
        strcat( pclFieldList, ",ALC3,DECN,DECA,DECS,MEAN,TURN,DSRC,DSEQ,USEQ,TLXC" );
    ilNumDCFFields = get_no_of_items(pclFieldList);
    dbg(DEBUG,"%s Number of DCFTAB Field <%d>",pclFunc,ilNumDCFFields);

    /* Load all existing delays from DCFTAB */
    ilNoRecs = 0;
    sprintf(pclSqlBuf,"SELECT %s FROM DCFTAB WHERE %s",pclFieldList,pclSqlWhere);
    dbg(DEBUG,"%s GetDCF <%s>",pclFunc,pclSqlBuf);

    /*i = 0;
    while( pclFieldList[i++] != ',' );*/ /* skip urno */

    igArrayPtr = 0;  /* Initialise the pcgLoaBuf Array */
    pcgLoaBuf[igArrayPtr] = '\0';
    InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);

    slCursor = 0;
    slFkt = START;
    ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
    while (ilRCdb == DB_SUCCESS)
    {
        BuildItemBuffer(pclDataBuf,pclFieldList,ilNumDCFFields,",");
        if( igArchiveDCF == TRUE )
        {
            j = 0;
            while( pclDataBuf[j++] != ',' ); /* skip urno */

            /*ilRC = FdiHandleSql( "IRT","DCMTAB","", &pclFieldList[i], &pclDataBuf[j],
                                 pcgTwStart, pcgTwEndNew, FALSE, FALSE);
            dbg( TRACE, "%s Archived to DCMTAB ilRC <%d>", pclFunc, ilRC ); */
            FillArray(&pclDataBuf[j]);
        }

        get_real_item(pclURNO[ilNoRecs],pclDataBuf,1); TrimRight(pclURNO[ilNoRecs]);
        get_real_item(pclDURN[ilNoRecs],pclDataBuf,2); TrimRight(pclDURN[ilNoRecs]);
        get_real_item(pclDURA[ilNoRecs],pclDataBuf,3); TrimRight(pclDURA[ilNoRecs]);

        if( igEraseDCF == TRUE )
            ilRC = FdiHandleSql("DRT","DCFTAB",pclURNO[ilNoRecs],"","",pcgTwStart,pcgTwEndNew,TRUE,TRUE);

        ilNoRecs++;
        slFkt = NEXT;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
    }

    close_my_cursor(&slCursor);
    dbg( DEBUG, "%s Loaded <%d> delay records from DCFTAB", pclFunc, ilNoRecs );

    if( strlen(pcgLoaBuf) > 0  && igArchiveDCF == TRUE )
    {
        igArrayPtr--;
        pcgLoaBuf[igArrayPtr] = '\0';
        strcpy( pclDataBuf,":VURNO,:VDURN,:VDURA,:VFURN,:VREAD,:VREMA,:VCDAT,:VHOPO,:VLSTU,:VUSEC,:VUSEU" );
        if( igHandleDelaySubCode == TRUE )
            strcat( pclDataBuf, ",:VALC3,:VDECN,:VDECA,:VDECS,:VMEAN,:VTURN,:VDSRC,:VDSEQ,:VUSEQ,:VTLXC" );
        ilRC = SqlArrayInsertTab("DCMTAB",pclFieldList,pclDataBuf,pcgLoaBuf);
        dbg( TRACE, "%s Archived to DCMTAB ilRC <%d>", pclFunc, ilRC );
    }

    if( igEraseDCF == FALSE )
    {
        /* Compare between existing delay code and new delay code received */
        ilCount = 0;
        for( i = 0; i < ilNoRecs; i++ )  /* Existing delay code */
        {
            for( j = 0; j < igNoDelCodes; j++ )   /* Received telex delay code */
            {
                if( !strcmp( pclDURN[i], "0" ) || j >= igMaxDelayInDCF )
                {
                    j = igNoDelCodes;
                    break;
                }
                if( !strcmp(pclDURN[i], rgDelayReas[j].pclDenUrno) && pclSTAT[j] == '-' )
                {
                    pclSTAT[j] = 'S';
                    ilCount++;
                    if( igHandleDelaySubCode == TRUE || strcmp(pclDURA[i], rgDelayReas[j].pclDelValue) )
                    {
                        /* Do update */
                        strcpy(pclFieldList,"USEU,LSTU,DURA");
                        sprintf(pclDataBuf,"TELEX,%s,%s", pclCurrentTime,rgDelayReas[j].pclDelValue);
                        if( igHandleDelaySubCode == TRUE )
                        {
                            strcat( pclFieldList, ",TURN,USEQ,DSEQ" );
                            sprintf( pclTmpIn, ",%s, ,%2.2d", pclTlxUrno,rgDelayReas[j].ilDseq );
                            strcat( pclDataBuf, pclTmpIn );
                        }
                        ilRC = FdiHandleSql("URT","DCFTAB",pclURNO[i],pclFieldList,pclDataBuf,
                                                       pcgTwStart,pcgTwEndNew,TRUE,TRUE);
                        pclSTAT[j] = 'U';
                    }
                    break;
                }
            } /* for loop */

            if( j >= igNoDelCodes )
            {
                /* Code not found in received telex. Delete */
                ilRC = FdiHandleSql("DRT","DCFTAB",pclURNO[i],"","",pcgTwStart,pcgTwEndNew,TRUE,TRUE);
                continue;
            }
        } /* for loop */
    } /* EraseDCF == FALSE */

    strcpy(pclFieldList,"HOPO,USEC,CDAT,DURN,DURA,FURN");
    if( igHandleDelaySubCode == TRUE )
        strcat(pclFieldList,",ALC3,DECS,MEAN,TURN,DSRC,DSEQ,USEQ,TLXC,DECA,DECN");
    /* remaining delay code do insert */
    for( i = 0; i < igNoDelCodes; i++ )
    {
        if( pclSTAT[i] == '-' )
        {
            if( rgDelayReas[i].pclDenUrno[0] == '0' && igInsInvalidDelayInDCF == FALSE )
                continue;

            sprintf(pclDataBuf, "%s,TELEX,%s,%s,%s,%s",
                                 pcgHomeAP,pclCurrentTime,rgDelayReas[i].pclDenUrno,
                                 rgDelayReas[i].pclDelValue,pcgAftUrno);
            if( igHandleDelaySubCode == TRUE )
            {
                sprintf( pclTmpIn, "%s,%s", rgDelayReas[i].pclDelCode, igSyncDECADECN == TRUE ? rgDelayReas[i].pclDelNumeric : " " );
                if (isdigit(rgDelayReas[i].pclDelCode[0]))
                    sprintf( pclTmpIn, "%s,%s", igSyncDECADECN == TRUE ? rgDelayReas[i].pclDelAlpha : " ", rgDelayReas[i].pclDelCode );
                sprintf(pclTmpOut,",%3.3s,%s,%s,%s,MVT,%2.2d, ,%s,%s",
                                   pclFillALC3[i] == 'Y' ? pclALC3 : " ",rgDelayReas[i].pclSubCode,rgDelayReas[i].pclDelReason,
                                   pclTlxUrno,rgDelayReas[i].ilDseq, rgDelayReas[i].pclTelexCode, pclTmpIn);
                strcat(pclDataBuf, pclTmpOut);
            }
            if( ilCount >= igMaxDelayInDCF )
            {
                if( igArchiveDCF == TRUE )
                    ilRC = FdiHandleSql("IRT","DCMTAB","",pclFieldList,pclDataBuf,
                                        pcgTwStart,pcgTwEndNew,TRUE,TRUE);
                continue;
            }

            ilRC = FdiHandleSql("IRT","DCFTAB","",pclFieldList,pclDataBuf,
                                pcgTwStart,pcgTwEndNew,TRUE,TRUE);
            ilCount++;
        }
    } /* for loop for recv code */

    if( igHandleDelaySubCode == TRUE && ilCount > 0 )
    {
        ilRC = TriggerBchdlAction( "SBC", "DCFTAB/SBC", pcgAftUrno, " "," ",
                                   pcgTwStart, pcgTwEndNew, TRUE, FALSE );
        dbg( TRACE, "%s Send special cmd <SBC> to CDID client", pclFunc );
    }
/*static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, char *pcpTwStart,
                              char *pcpTwEnd, int ipBchdl, int ipAction);*/
    return ilRC;
} /* End of HandleDCFTAB */


static int SecondTimeCheck(char *pcpData, int ipPos, int ipHeaderTyp)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SecondTimeCheck:";
  int ilI;
  char *pclTmpPtr;
  char *pclBegin;
  int ilFound;
  char pclTimeLine[2048];

  if (ipHeaderTyp == 6 || ipHeaderTyp == 9 || ipHeaderTyp == 10 || ipHeaderTyp == 11)
     return ilRC;
  if (igTlxTooOld == TRUE)
     return ilRC;

  strcpy(pclTimeLine,"");
  ilFound = FALSE;
  ilI = ipPos;
  pclTmpPtr = &pcpData[ipPos];
  if (ipHeaderTyp == 12)
  {
     while (ilI > 0 && ilFound == FALSE)
     {
        if (strncmp(pclTmpPtr,"=MSGID",6) == 0)
        {
           ilFound = TRUE;
        }
        else
        {
           ilI--;
           pclTmpPtr--;
        }
     }
     if (ilFound == TRUE)
     {
        pclBegin = pclTmpPtr;
        while (*pclTmpPtr != '\0' && *pclTmpPtr != '\n')
           pclTmpPtr++;
     }
  }
  else
  {
     while (ilI > 0 && ilFound == FALSE)
     {
        if (*pclTmpPtr == '.' && *(pclTmpPtr-1) == '\n')
        {
           ilFound = TRUE;
        }
        else
        {
           ilI--;
           pclTmpPtr--;
        }
     }
     if (ilFound == FALSE)
     {
        if (*pclTmpPtr == '.')
        {
           ilFound = TRUE;
        }
     }
  }
  if (ilFound == TRUE)
  {
     pclTmpPtr++;
     CopyLine(pclTimeLine,pclTmpPtr);
     TrimRight(pclTimeLine);
     if (strcmp(pcgTimeLine,pclTimeLine) != 0)
     {
        dbg(TRACE,"%s Additonal Time Line <%s> found",pclFunc,pclTimeLine);
        if (ipHeaderTyp == 12)
        {
           ilRC = CheckTelexTime(pclBegin,ipHeaderTyp);
        }
        else
        {
           pclTmpPtr--;
           ilFound = 0;
           while (ilI > 0 && ilFound < 2)
           {
              if (*pclTmpPtr == '\n')
                 ilFound++;
              if (ilFound < 2)
              {
                 ilI--;
                 pclTmpPtr--;
              }
           }
           if (*pclTmpPtr == '\n')
              pclTmpPtr++;
           ilRC = CheckTelexTime(pclTmpPtr,ipHeaderTyp);
        }
     }
  }

  return ilRC;
} /* End of SecondTimeCheck */



static int HandleDLMTAB()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleDLMTAB:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclFields[1024];
  char pclData[2048];
  int ilI;
  T_CFG_ARRAY_ELEMENT *prlCfgArrElem;
  T_FIELDLIST *prlCfgField;
  T_TLXRESULT *prlTlxPtr;
  char pclAlc3[8];
  int ilCount;
  char pclTmpIn[1024];
  char pclTmpOut[1024];
  char pclOutArray[20][1024];
  char pclFurnTurn[32];
  char pclTime[32];

  if (strlen(pcgAftUrno) == 0)
     return ilRC;

  if (strstr(pcgDLMSubTypes,pcgSubType) == NULL)
     return ilRC;

  if (strlen(pcgDelFromDLM) == 0)
     sprintf(pclSqlBuf,"DELETE FROM DLMTAB WHERE FURN = %s",pcgAftUrno);
  else
     sprintf(pclSqlBuf,"DELETE FROM DLMTAB WHERE FURN = %s AND DSRC IN (%s)",pcgAftUrno,pcgDelFromDLM);
  ilRC = FdiSendSql(pclSqlBuf);

  if (igNoDelCodes == 0)
     return ilRC;

  InitRecordDescriptor(&rgRecDesc,1024,0,FALSE);
  memset(pcgLoaBuf,0x00,1000*512);
  igArrayPtr = 0;
  strcpy(pclAlc3,"");
  prlCfgArrElem = CCSArrayGetFirst(prgMasterCmpCfgArray);
  while (prlCfgArrElem != NULL && strlen(pclAlc3) == 0)
  {
     if (prlCfgArrElem->MasterKey != NULL)
     {
        prlCfgField = CCSArrayGetFirst(prlCfgArrElem->MasterKey);
        while (prlCfgField != NULL && strlen(pclAlc3) == 0)
        {
           if ((prlTlxPtr = CCSArrayGetData(prgMasterTlx,prlCfgField->DName)) != NULL )
           {
              if (strcmp(prlCfgField->DName,"Alc") == 0)
                 strcpy(pclAlc3,prlTlxPtr->FValue);
           }
           prlCfgField = CCSArrayGetNext(prlCfgArrElem->MasterKey);
        } /* end of while  */
     } /* end of if  */
     prlCfgArrElem = CCSArrayGetNext(prgMasterCmpCfgArray);
  } /* end of while  */
  for (ilI = 0; ilI < igNoDelCodes; ilI++)
  {
     ilCount = 1;
     sprintf(pclTmpIn,"%s,%s",rgDelayReas[ilI].pclDelCode,pclAlc3);
     if (!isdigit(rgDelayReas[ilI].pclDelCode[0]))
        ilRC = syslibSearchDbData("DENTAB","DECA,ALC3",pclTmpIn,
                                  "DECN,DECA,ALC3,DENA",pclTmpOut,&ilCount,"\n") ;
     else
        ilRC = syslibSearchDbData("DENTAB","DECN,ALC3",pclTmpIn,
                                  "DECN,DECA,ALC3,DENA",pclTmpOut,&ilCount,"\n") ;
     if (ilRC != RC_SUCCESS)
     {
        ilCount = 1;
        sprintf(pclTmpIn,"%s, ",rgDelayReas[ilI].pclDelCode);
        if (!isdigit(rgDelayReas[ilI].pclDelCode[0]))
           ilRC = syslibSearchDbData("DENTAB","DECA,ALC3",pclTmpIn,
                                     "DECN,DECA,ALC3,DENA",pclTmpOut,&ilCount,"\n") ;
        else
           ilRC = syslibSearchDbData("DENTAB","DECN,ALC3",pclTmpIn,
                                     "DECN,DECA,ALC3,DENA",pclTmpOut,&ilCount,"\n") ;
     }
     if (ilRC != RC_SUCCESS)
     {
        if (!isdigit(rgDelayReas[ilI].pclDelCode[0]))
           sprintf(pclTmpOut," ,%s, ,Missing in Basic Data",rgDelayReas[ilI].pclDelCode);
        else
           sprintf(pclTmpOut,"%s, , ,Missing in Basic Data",rgDelayReas[ilI].pclDelCode);
     }
     strcpy(&pclOutArray[ilI][0],pclTmpOut);
  }
  strcpy(pclFields,"HOPO,USEC,CDAT,FURN,TURN,DECN,DECA,ALC3,DENA,DURA,DSRC");
  if (strlen(pcgAftUrno) > 0)
     sprintf(pclFurnTurn,"%s,",pcgAftUrno);
  else
     strcpy(pclFurnTurn,"0,");
  if (strlen(pcgNextTlxUrno) > 0)
     strcat(pclFurnTurn,pcgNextTlxUrno);
  else
     strcat(pclFurnTurn,"0");
  TimeToStr(pclTime,time(NULL));
  for (ilI = 0; ilI < igNoDelCodes; ilI++)
  {
     sprintf(pclData,"%s,%s,%s,%s,%s,%s,MVT",
             pcgHomeAP,mod_name,pclTime,pclFurnTurn,&pclOutArray[ilI][0],rgDelayReas[ilI].pclDelValue);
     if (igArrayInsert == TRUE)
        ilRC = FillArray(pclData);
     else
        ilRC = FdiHandleSql("IBT","DLMTAB","",pclFields,pclData,pcgTwStart,pcgTwEndNew,FALSE,FALSE);
  }
  if (strlen(pcgLoaBuf) > 0 && igArrayInsert == TRUE)
  {
     igArrayPtr--;
     pcgLoaBuf[igArrayPtr] = '\0';
     strcpy(pclFields,"URNO,HOPO,USEC,CDAT,FURN,TURN,DECN,DECA,ALC3,DENA,DURA,DSRC");
     strcpy(pclData,":VURNO,:VHOPO,:VUSEC,:VCDAT,:VFURN,:VTURN,:VDECN,:VDECA,:VALC3,:VDENA,:VDURA,:VDSRC");
     sprintf(pclSqlBuf,"INSERT INTO DLMTAB (%s) VALUES (%s)",pclFields,pclData);
     slCursor = 0;
     slFkt = 0;
     dbg(DEBUG,"%s <%s>\n%s",pclFunc,pclSqlBuf,pcgLoaBuf);
     ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcgLoaBuf,0,&rgRecDesc);
     if (ilRCdb == DB_SUCCESS)
        commit_work();
     else
     {
        rollback();
        dbg(TRACE,"%s Error = %d",pclFunc,ilRCdb);
     }
     close_my_cursor(&slCursor);
  }

  return ilRC;
} /* End of HandleDLMTAB */

/** MEI 01-MAR-2010 **/
static int RunSQL(char *pcpSelection, char *pcpData )
{
    int ilRc = DB_SUCCESS;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclErrBuff[128];
    char *pclFunc = "RunSQL";

    ilRc = sql_if ( START, &slSqlCursor, pcpSelection, pcpData );
    close_my_cursor ( &slSqlCursor );

    if( ilRc == DB_ERROR )
    {
        get_ora_err( ilRc, &pclErrBuff[0] );
        dbg( TRACE, "%s OraError! <%s>", pclFunc, &pclErrBuff[0] );
    }
    return ilRc;
}

/** MEI 10-NOV-2010 **/
static int SqlArrayInsertTab(char *pcpTabName, char *pcpFldList, char *pcpValList, char *pcpDatList)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="SqlArrayInsertTab:";
  int ilRCdb = DB_SUCCESS;
  char pclSqlBuf[1024];
  short slFkt = 0;
  short slCursor = 0;

  sprintf(pclSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",pcpTabName,pcpFldList,pcpValList);
  slCursor = 0;
  slFkt = 0;
  dbg(DEBUG,"%s <%s>\n%s",pclFunc,pclSqlBuf,pcpDatList);
  ilRCdb = SqlIfArray(slFkt,&slCursor,pclSqlBuf,pcpDatList,0,&rgRecDesc);
  if (ilRCdb == DB_SUCCESS)
     commit_work();
  else
  {
     rollback();
     dbg(TRACE,"%s Error = %d",pclFunc,ilRCdb);
  }
  close_my_cursor(&slCursor);
  return ilRC;
} /* End of SqlArrayInsertTab */

/********************************************************************
 *
 * SendToLoahdl - UFIS-1385
 *
 * Send a message to LOAHDL only if
 * (a) flight is found
 * (b) data saved to LOATAB
 * (c) LOAHDL has been configured for the particular project.
 *
 * Message consists of at least one set of DSSN,FLNU
 * There may be more if the telex type has related flights, e.g. PTM.
 * For most telex types, it is possible to deduce (b) by testing certain
 * global variables. But certain telexes require additional flag(s) in
 * FDI/src to enable testing for (b) here.
 * particular site.
 *
********************************************************************
 */

static int SendToLoahdl (void)
{
  int ilRC = RC_SUCCESS, ilRC2;
  char pclFunc[] = "SendToLoahdl:";
  char pclFields [64], pclSelection [32], pclCmd [32];
  char pclTemp [64], pclDssn [64], pclValues [64];
  int iNoUrnos, jj, iFlagSend;
  T_TLXRESULT *prlTlxPtr = NULL;
  T_TLXRESULT *prlTlxPtrMvt = NULL;
  T_CFG_ARRAY_ELEMENT *prlCfgArrElem = NULL;

  dbg(TRACE,"%s --------------------------",pclFunc);

  if (strlen(pcgAftUrno) == 0)
  {
    dbg(TRACE,"%s No flight found, so no msg to LOAHDL",pclFunc);
    dbg(TRACE,"%s --------------------------",pclFunc);
    return ilRC;
  }

  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"TlxTyp");
  if (prlTlxPtr == '\0')
  {
    dbg(TRACE,"%s Unexpected: cannot get pointer to Master Telex",pclFunc);
    return RC_FAIL;
  }

  iFlagSend = 0;
  switch (rgTlxInfo.TlxCmd)
  {
    /*
      Flwg copied from fdimvt.src so that file does not need an
      additional flag to indicate whether LOATAB values were saved.
    */
    case FDI_MVT:
      prlCfgArrElem = CCSArrayGetData(prgMasterCmdCfgArray,"MVT");
      if (prlCfgArrElem != '\0')
      {
        ilRC2 = GetLOAData(prlCfgArrElem,"PXT",&prlTlxPtrMvt);
        if (prlTlxPtrMvt != '\0')
        {
          if ((strlen(prlTlxPtrMvt->FValue) > 0))
            iFlagSend = 1;
        }
      }
      break;

    case FDI_LDM:
      if (igCurLdmValues >= 0) /* this was init to -1 */
        iFlagSend = 1;
      break;

    case FDI_DPFI:
    case FDI_APFI:
      /* These were init to -1 */
      if ((igCurDpiCfgValues >= 0) || (igCurDpiBklValues >= 0) ||
          (igCurDpiTrsValues >= 0) || (igCurDpiCkiValues >= 0) ||
          (igCurDpiTrfValues >= 0) || (igCurDpiDisValues >= 0)) 
        iFlagSend = 1;
      break;

    case FDI_CPM:
      /* if (igCurCpmValues > 0) */
      if (cgTlxStatus == FDI_STAT_FLT_UPDATED)
        iFlagSend = 1;
      break;

    case FDI_DLS:
      /* if (igCurDlsValues > 0) */
      if (cgTlxStatus == FDI_STAT_FLT_UPDATED)
        iFlagSend = 1;
      break;

    case FDI_FFM:
      /* if (igCurFfmValues > 0) */
      if (cgTlxStatus == FDI_STAT_FLT_UPDATED)
        iFlagSend = 1;
      break;

    case FDI_UCM:
      /* This wont work because initialization is to zero */
      /* if (igCurUcmValues > 0) */
      if (cgTlxStatus == FDI_STAT_FLT_UPDATED)
        iFlagSend = 1;
      break;

    case FDI_PTM:
      if (strlen (pcgLoahdlFltUrnos) > 0)
        iFlagSend = 1;
      break;

    case FDI_PSC:
      /*
        PSC global status would remain 'U' unless non-matching 'AA' line
        was encountered in fdipsc.src. If 'U' it implies values were saved.
      */
      if (cgTlxStatus == FDI_STAT_FLT_UPDATED)
        iFlagSend = 1;
      break;

  default:
      dbg(TRACE,"%s No msg coded for Telex Type <%s>",
        pclFunc,prlTlxPtr->FValue);
      iFlagSend = 0;
    break;
  }


/*
    These telex types do not save to LOATAB

    FDI_RQM, FDI_DIV, FDI_BTM, FDI_TPM,FDI_PSM,FDI_ETM,FDI_FWD,
    FDI_AFTN,FDI_DFS,FDI_ADF,FDI_ASDI,FDI_MAN,FDI_SCORE,FDI_UBM,
    FDI_CCM,FDI_BAY,FDI_PAL,FDI_CAL, so we are not sending
    message to LOAHDL for them.

    To-do: FDI_PTM,FDI_ALTEA,FDI_KRISCOM

*/

  if (igLoahdlModid <= 0)
  {
    dbg(TRACE,"%s No LOAHDL in sgs.tab",pclFunc);
  }
  else if (iFlagSend == 0)
  {
    dbg(TRACE,"%s Flagged not to send to LOAHDL",pclFunc);
  }
  else
  {
    strcpy (pclFields,"DSSN,FLNU");
    strcpy (pclCmd,"LOA");
    strcpy (pclSelection,"");
    /*
      At least ftiptm would leave a trailing comma.
    */
    if (strlen (pcgLoahdlFltUrnos) == 0)
      strcpy (pcgLoahdlFltUrnos,pcgAftUrno);
    else
    {
      if (pcgLoahdlFltUrnos [((strlen (pcgLoahdlFltUrnos)))-1] == ',')
        strcat (pcgLoahdlFltUrnos,pcgAftUrno);
      else
      {
        strcat (pcgLoahdlFltUrnos,",");
        strcat (pcgLoahdlFltUrnos,pcgAftUrno);
      }
    }

    iNoUrnos = get_no_of_items (pcgLoahdlFltUrnos);
    for (jj = 1; jj <= iNoUrnos; jj++)
    {
      (void) get_real_item (pclTemp,pcgLoahdlFltUrnos,jj);
      if ((strcmp (prlTlxPtr->FValue,"DPFI") == 0) ||
        (strcmp (prlTlxPtr->FValue,"APFI") == 0))
        sprintf (pclValues,"PFI,%s",pclTemp);
      else
        sprintf (pclValues,"%s,%s",prlTlxPtr->FValue,pclTemp);
      ilRC = SendCedaEvent(igLoahdlModid,0,pcgAftDestName,pcgAftRecvName,
                pcgAftTwStart,pcgAftTwEnd,pclCmd,
                "LOATAB",pclSelection,pclFields,pclValues,
                "",3,NETOUT_NO_ACK);

      dbg(TRACE,"%s Sent CMD<%s>FLD<%s>VAL<%s> to <%d> ret.<%d>",
        pclFunc,pclCmd,pclFields,pclValues,igLoahdlModid,ilRC);
    }
  }
  dbg(TRACE,"%s --------------------------",pclFunc);
  return ilRC;
}

