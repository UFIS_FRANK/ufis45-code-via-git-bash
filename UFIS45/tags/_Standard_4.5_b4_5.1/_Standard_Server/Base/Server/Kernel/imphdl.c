#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
/*  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/imphdl.c 4.7 2008/01/30 16:35:25SGT fei Exp  $"; */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/imphdl.c 4.12b 2012/04/27 16:35:25SGT fya Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  USC                                                      */
/* Date           :  17.02.2003                                               */
/* Description    :  unified import manager for flight data                   */
/*                                                                            */
/* Update history :  17.02.2003 module creation from skeleton                 */
/*                                                                            */
/*20030403 JIM: checkDataValid: no length check for ADID                      */
/*20030403 JIM: line 3168: SELECT UAFT FROM FSLTAB ==> SELECT URNO FROM FSLTAB*/
/*20030403 JIM: dbg(...  ,sccs_version) ==> dbg(...  ,mks_version)            */
/*20030403 JIM: siehe 'pcgNoCheckForLen'                                      */
/*20030403 JIM: checkDateSeq: 'fday < sday' ==> 'fday <= sday'                */
/*20030624 JIM: added ONBS and LNDA to cgFSLAdminFields (for CSKED)           */
/*20030625 JIM: added ETDA,OFBS and AIRA to cgFSLAdminFields (for CSKED)      */
/*              avoid update of ORG/DES while processing CSKED                */
/*                                                                            */
/*20030822 USC: send FLTS to FLIGHT (if relevant)                             */
/*20040810 JIM: mark mks_version as possibly unused                           */
/*										*/
/*20110317 DKA: Non-Scheduled flights for SATS-SOCC.				*/
/*		Generally tried to implement relevant code from imphdl.c of	*/
/*		CGMS. It appeared that imphdl was modified in parallel in	*/
/*		standard as well as CAG, so cgms has changes not found in	*/
/*		standard. Tests global flag igIsNonSched that is defined & set	*/
/*		in wrapElseSrc.c (similiar to wrapFcsSrc.c in CGMS).		*/
/*		Skip checkFLNO for ELSE since it is already performed in 	*/
/*		wrapElse.							*/
/*20110721 DKA: UFIS-743: bug fix - identifyFlightRecord failed for CSKED after	*/
/* v.4.9	an ELSE non-sched because flag igIsNonSched was not reset. 	*/
/*		Reset it in prepareData().					*/
/*20120416 v4.11 FYA: Fix the bug on "IN WORK" items after normal processing  */
/*20120523 v4.12a FYA: Fix the bug on HandleDelete insert records in IFTTAB,  */
/* the FLDU is too long								 																				*/
/*20120524 v4.12b FYA: Assign the interface name to pcgDestName which 				*/
/*originally is null in HandleTimerRequest SendCedaEvent											*/
/*                                                                            */
/******************************************************************************/
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include "ntisch.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#define ulong long 
#else
	extern int _daylight;
	extern long _timezone; 
	extern char *_tzname[2];
#endif

int debug_level = 0;
//Frank@Test
long lgDataID;
//Frank@Test
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char  cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static char  cgActCmd[16];
static int   igETA_FRAME = 0;
static char  cgETA_FRAME[20] = "\0";
static long  lgEvtCnt = 0;
//Frank@cannot find the definition of igIsNonSched
static int   igIsNonSched;
static char cgRUN_MODE[20] = "\0";
static int igRUN_MODE = 0;
//Frank@cannot find the definition of igIsNonSched

/* Frank v4.12a*/
char clFLDAFromWrapCsked[16] = "\0";   /* Frank v4.12b */
/******************************************************************************/
/* interface specific stuff						      */
/******************************************************************************/

enum TimeType {LOCAL=1,UTC};
enum FieldStatus {INVALID=0, VALID};
enum CmdList {INSCMD,UPDCMD,DELCMD};

#define MAX_INT_PARAMETER 32000
#define MAX_NUM_LEGS 32
#define RC_INVALID -128
#define RC_NODATA -129
#define RC_NOTFOUND -130
#define RC_ISPENDING -131
#define RC_DELAY -132
#define RC_HASTEMPLATE -133
#define RC_NOTUNIQUE -134
#define RC_ONDELETE -135
#define RC_HASSUFFIX -136
#define RC_NOTIMPL -137 
#define PeriodTimeField 9
#define CedaTimeField 15
#define FREQPeriodField 16
#define DATABLK_SIZE (256*1024)
#define MAX_RETRIEVE_TRY 2        /* number of times we will retry a failed retrieve */
                                  /* of event data, before an alert is raised        */ 
#define CEDATIMLEN 14

//#define TEST

/*
** Data status bits
*/
#define IS_VALID   0x01
#define IS_AFT     0x02           /* those fields will be send to the flight handler */ 
#define IS_INVALID 0x04
#define IS_UTCTIME 0x08           /* those fields have time values to be converted to UTC */
#define IS_DATEONLY 0x10          /* those fields don't have time values and need to be completed (update only) */
#define IS_AF1     0x20           /* those fields will go to AF1TAB */
/*
** URNO pool
*/
#define URNO_POOL_SIZE 100        /* number of URNOS fetched with each refill of pool */
#define URNO_SIZE 12              /* maximum size of URNO in string representation    */ 
/*
** possible values for boolean type values
** in impman.cfg
** specifying DEFAULT means: fetch value from
** the related default sectionx
*/ 
enum ConfigEntryVal {NO=0,YES,DEFAULT};

enum ADIDvalues {U=0,A,D,B,V};

static char *RouteType []      = { "Unknown", "Arrival", "Departure", "Both", "Via" };

typedef struct {
  char *FieldName;
  uint isUTC;
  uint isMandatory;
} FSLFields;

static FSLFields cgFSLFields [] = { 
  {"ORG3", FALSE, FALSE},
  {"ORG4", FALSE, FALSE},
  {"DES3", FALSE, FALSE},
  {"DES4", FALSE, FALSE},
  {"STOA", TRUE,  FALSE},
  {"STOD", TRUE,  FALSE},
  {"ADID", FALSE, FALSE},
  {"ETOA", TRUE,  FALSE}, 
  {"ETOD", TRUE,  FALSE},
  {"ACT3", FALSE, FALSE}, 
  {"ACT5", FALSE, FALSE}, 
  {"FLDA", FALSE, FALSE},
  {"FLDU", FALSE, FALSE},
  {"REGN", FALSE, FALSE},
  {"SECN", FALSE, FALSE},
  {"NUMS", FALSE, FALSE},
  {"FTYP", FALSE, FALSE},
  {"OFBL", FALSE, FALSE},
  {"ONBL", FALSE, FALSE},
  {"ETDC", TRUE,  FALSE},
  {"ETAC", TRUE,  FALSE},
  /*  {"AIRA", TRUE,  FALSE}, */
  {"LAND", TRUE,  FALSE},
  {"TIFA", TRUE, FALSE},
  {"TIFD", TRUE, FALSE},
  {NULL,   FALSE, FALSE}
}; 

/*
** the fields that are copied from
** the A/D sector to admin record
** these fields will be send to flight 
*/
static FSLFields cgFSLAdminFields [] = {
  { "FLNO", FALSE, FALSE},
  { "FLDA", FALSE, FALSE},
  { "STOA", FALSE, FALSE},
  { "STOD", FALSE, FALSE},
  /*  { "ACTI", FALSE, FALSE}, */
  { "ACT3", FALSE, FALSE},
  { "ACT5", FALSE, FALSE},
  { "FTYP", FALSE, FALSE},
  { "REGN", FALSE, FALSE},
  { "ADID", FALSE, FALSE},
  { "ORG3", FALSE, FALSE},
  { "ORG4", FALSE, FALSE},
  { "DES3", FALSE, FALSE},
  { "DES4", FALSE, FALSE},
  { "ETOA", TRUE,  FALSE},
  { "ETDA", TRUE,  FALSE},  /* reactivated by JIM */
  /* { "ETAA", TRUE,  FALSE},*/
  /*  { "ETDD", TRUE,  FALSE},*/
  { "OFBS", FALSE, FALSE},  /* reactivated by JIM */
  { "ONBS", TRUE,  FALSE},  /* reactivated by JIM */
  { "ETDC", TRUE,  FALSE},
  { "ETAC", TRUE,  FALSE},
  { "AIRA", TRUE,  FALSE}, /* reactivated by JIM */
  { "LNDA", TRUE,  FALSE}, /* reactivated by JIM */
  { "TTYP", FALSE, FALSE}, /*added by HEB*/
  { NULL,   FALSE, FALSE}
}; 

typedef struct {
  char *FieldName;
  uint isShortField;
  uint isDateOnly;
  uint DateOnlyAllowed;
  uint ADID;
} ShortFields;

/*
** a list of fields that contain cedatime
** and will be converted to UTC
*/
static ShortFields cgUTCTIMEField [] = {
  { "STOD",FALSE,FALSE,FALSE,D },
  { "NXTI",FALSE,FALSE,FALSE,D },
  { "ETOD",FALSE,FALSE,TRUE,D },
  { "OFBL",FALSE,FALSE,FALSE,D }, 
  { "AIRB",FALSE,FALSE,FALSE,D },
  { "GD1X",FALSE,FALSE,FALSE,D },
  { "GD1Y",FALSE,FALSE,FALSE,D },
  { "FCAL",FALSE,FALSE,FALSE,D },
  { "FCA1",FALSE,FALSE,FALSE,D },
  { "FCA2",FALSE,FALSE,FALSE,D },
  { "BOAO",FALSE,FALSE,FALSE,D },
  { "STOA",FALSE,FALSE,FALSE,A },
  { "ETOA",FALSE,FALSE,TRUE,A },
  { "TMOA",FALSE,FALSE,FALSE,A }, 
  { "LAND",FALSE,FALSE,FALSE,A }, 
  { "ONBL",FALSE,FALSE,FALSE,A },
  { NULL,  FALSE,FALSE,FALSE,A }
}; /* not yet completed */

/*
** a list of mandatory FSLTAB fields
*/
static char *cgMandatoryFSLFields [] = { "URNO", "FLNO", NULL };

/*
** a list of fields that will be send 
** to the flight
*/
/* 20110318 DKA: Add CSGN, DSSF for ELSE UFIS-244 (Non-scheduled flights) */
static char *cgAFTField []     = { "FLNO", "ACT3", "ACT5", "REMP", "GTA1", "GTA2", "GTD1", "GTD2", "TTYP", \
			           "DES3", "DES4", "REGN", "STOD", "STOA", "ORG3", "ORG4", "TGA1", "TGA2", \
				   "TGD1", "TGD2", "BLT1", "BLT2", "ETAA", "ETDA", "LNDA", "URNO", "PSTA", \
				   "PSTD", "RWYA", "RWYD", "GATE", "VIAL", "GD1X", "GD1Y", "BOAO", "FCAL", \
				   "ACTI", "ALC2", "FTYP", "ACTI", "ONBS", "OFBS", "ETDD", "ETAC", "ETOA",\
				   "ETAD", "ETDC", "AIRA", "ETOD", "JFNO", "LAND",  "AIRB", "BAZ1", "FLNS",
				   "ALC3", "FLTN", "CSGN", "DSSF", NULL };

/*
** fields that will be put into 
** the AFTTAB extension table AF1TAB
*/
static char *cgAF1Field []     = { "FCA1", "FCA2", NULL };

/*
** fields used to transport some special
** information to flight
*/
static char *cgAFTPseudoField [] = { "COSH", "VSUP", NULL };
/*
** fields with gate times
*/
static char *cgGateTimeField [] = { "GD1X", "GD1Y", "BOAO", "FCAL", NULL };
/*
** mandatory fields for config
*/
static char *cgConfigParam [] = { "IFNAME", "TIME", "PRIO", NULL };
/*
** optional fields for config
*/
static char *cgConfig [] = { "LOCAL_TIMES", "FIRST_SECTOR_DATE", "ALLOW_INSERTS", "ALLOW_DELETES", "MIXED_UPDATES", "DELETION_FRAME", \
			     "PRIORITY", "UPDATE_EMPTY", "BROADCAST", "INPUT_FIELDS", "FLD1_PROPERTIES", NULL };

static char  cgEmptyDate [] = "00000000";

static uint igDayTab []	= { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
/*
** the name of the interface related section in impman.cfg
** new interfaces have to be added here and
** below to the default section
*/
enum InterfaceType {ELSE=0,RMS,CSK,SQL,Z,UNKNOWN};

static char *Section [] = { "ELSE", "RMS", "CSKED", "SQL", "Z" };

#define NUM_INTF Z
/*
** the default values for each 
** new interface have to be added here
*/
/*                                       ELSE RMS  CSKED Z     */ 
static uint LOCAL_TIMES_DEF 	  [] = { NO,  NO,  NO,   NO };
static uint FIRST_SECTOR_DATE_DEF [] = { NO,  NO,  NO,   NO };
static uint ALLOW_INSERTS_DEF 	  [] = { NO,  NO,  NO,   NO }; 
static uint ALLOW_DELETES_DEF 	  [] = { NO,  NO,  NO,   NO }; 
static uint MIXED_UPDATES_DEF 	  [] = { NO,  NO,  NO,   NO };
static uint DELETION_FRAME_DEF	  [] = { 30,  30,  30,   30 };
static uint PRIORITY_DEF	  [] = { 4,   4,   4,    4 };
static uint UPDATE_EMPTY_DEF	  [] = { NO,  NO,  NO,   NO };
static uint BROADCAST_DEF	  [] = { YES, YES, YES,  YES };
static uint TOGGLE_OVERRULE_DEF   [] = { NO,  NO,  NO,   NO };
/*
** act config values
*/
typedef struct {
  char table[64];
  char field[64];
  uint len;
  char type[64];
} FLD1PROPTYPE;

static uint  igFirstSectorDate[NUM_INTF+1] = {FALSE,FALSE,FALSE,FALSE};
static uint  igAllowInserts[NUM_INTF+1]    = {FALSE,FALSE,FALSE,FALSE};
static uint  igAllowDeletes[NUM_INTF+1]    = {FALSE,FALSE,FALSE,FALSE};
static uint  igMixedUpdates[NUM_INTF+1]    = {FALSE,FALSE,FALSE,FALSE};
static uint  igDelFram[NUM_INTF+1]         = {30,30,30,30};
static uint  igUpdateEmpty[NUM_INTF+1]     = {FALSE,FALSE,FALSE,FALSE};
static uint  igBroadcast[NUM_INTF+1]       = {FALSE,FALSE,FALSE,FALSE};
static uint  igPrio[NUM_INTF+1]            = {4,4,4,4};
static uint  igToggleOverrule[NUM_INTF+1]  = {FALSE,FALSE,FALSE,FALSE};
static FLD1PROPTYPE sgFld1Prop[NUM_INTF+1];
static char  clInpFields[1024];

static uint  igPrioFromIf;
static uint  igTimeFromIf;
static uint  igIfType;
static uint  igTimerID;
static uint  igHasSql;
/*
** The required field configuration
*/
static char *mandatoryGroupOfFlightIdentifiers [] = { "CSGN", "FLNO", "FLCA" };
static char *mandatoryGroupOfRoutingInformation [] = { "ORIG", "DEST", "ADID" };
static char *mandatoryGroupOfPeriodInformation [] = { "VPFR", "VPTO", "FREQ" };
static char *mandatoryGroupOfTimeFieldValues [] = { "STOD", "STOA" };
/*
** available cfg items
*/ 
typedef struct {
        uint section_configured;        /* if section entry in .cfg */
	uint local_time;		/* boolean type */
	uint first_sector_date;		/* boolean type */
	uint allow_inserts;		/* boolean type */
	uint allow_deletes;		/* boolean type */
	uint mixed_updates;		/* boolean type */
	uint update_empty;		/* boolean type */
	uint broadcast;			/* boolean type */
	uint toggle_overrule; 		/* boolean type */
	int deletion_frame;			/* -1 -> default */
	int priority;				/* -1 -> default */
	char *input_fields;			/* string, no default */
	char *fld1_properties;			/* string, no default */
	} interface_config_type;

interface_config_type config[NUM_INTF+1];

typedef struct {
  char *data;
  uint status;      /* various status information (bit-mask)    */ 
  uint len;         /* size of currently allocated data element */
  uint timeType;    /* time is UTC or timezone */
  char timeZone[3]; /* the 2 letter code of the timezone, if time is local */
} DataElementType;

typedef struct {
  char stod[CedaTimeField];
  char stoa[CedaTimeField];
  char etod[CedaTimeField];
  char etoa[CedaTimeField];
  char ofbl[CedaTimeField];
  char onbl[CedaTimeField];
  char airb[CedaTimeField];
  char land[CedaTimeField];
  char tmoa[CedaTimeField];
  char nxti[CedaTimeField];
  uint HaveTimeInfo;
} TimeInfo;

typedef struct {
  char vpfr[PeriodTimeField];
  char vpto[PeriodTimeField];
  char freq[FREQPeriodField];
  uint frqw;
  uint HavePeriodInfo;
} PeriodInfo;

typedef struct {
  uint sendCount;
  uint rcvAnswerCount;
  uint lastElSent;
  uint numOfEntries;
  uint haveAF1Data;
  uint isAF1Update;
} MessageStateType;

typedef MessageStateType *MessageStateTypePtr;

struct List {
  char *field;
  struct List *next;
  struct List *prev;
  MessageStateTypePtr request;           /* pointer to a list of outstanding requests */
  DataElementType dobj[MAX_NUM_LEGS];    /* points to a DataElement   */
  PeriodInfo periodInfo[MAX_NUM_LEGS];   /* points to a PeriodElement */
  TimeInfo timeInfo[MAX_NUM_LEGS];       /* points to a TimeElement   */
  uint RouteType[MAX_NUM_LEGS];          /* the routing type (ADID)   */
};

typedef struct {
  char *firstElement;
  char *lastElement;
  uint numEntries;
} ListHead;

typedef struct List ListElem;
typedef ListElem *ListPtr;

struct InDataType {
  struct InDataType *next;
  struct InDataType *prev;
  ListPtr ifData;
  uint serialNumber;
  uint stage;
};

typedef struct InDataType InDataElem;
typedef struct InDataType *InDataPtr;

typedef  struct {
    char display[1];
    char apc3[3];
    char apc4[4];
    char stoa[14];
    char etoa[14];
    char land[14];
    char onbl[14];
    char stod[14];
    char etod[14];
    char ofbl[14];
    char airb[14];
  } VialType;

static InDataPtr InData; /* Head to list of inputdata */
/*static PeriodInfo periodInfo[MAX_NUM_LEGS];*/
/*static TimeInfo timeInfo[MAX_NUM_LEGS];*/
/*static igRouteType;*/  /* A|D|B */
static char pcgDataArea[DATABLK_SIZE];
static char pcgSqlBuf[12 * 1024];
static InDataPtr pcgCurrentData;

static char  pcgRecvName[128] ;  /* BC-Head used as WKS-Name */
static char  pcgDestName[128] ;  /* BC-Head used as USR-Name */
static char  pcgTwStart[128] ;   /* BC-Head used as Stamp */
static char  pcgTwEnd[128] ;     /* BC-Head used as Stamp */
static char  pcgChkWks[128] ; 
static char  pcgChkUsr[128] ; 
static char  pcgActCmd[16];
static char  pcgAftFields[1024];
static char  pcgAftValues[1024];
static char  pcgSelection[1024];
static char  pcgTmpSelection[1024];
static char  cgSelection[1024];
static char  cgFields[2048];
static char  cgData[2048];
char *pcgSel;
static char  pcgRetrieveFields[2048];
static char  pcgRetrieveData[2048];
static uint  igELSEconfigured=FALSE;
static uint  igRMSconfigured=FALSE;
static uint  igYconfigured=FALSE;
static uint  igZconfigured=FALSE;
static long  lgUrno;
static long  lgOnDelUrno;
static ListPtr pcgItemList=NULL;
static ListPtr pcgFlightResponse=NULL;
static ListPtr pcgFlightResponseUpd=NULL;
static ListPtr pcgAdminList=NULL;
static ListPtr pcgConfigList=NULL;
static ListPtr pcgAFTLenInfo=NULL;
static ListPtr pcgPendResp=NULL;
static int   igOriginator;
static uint  igNumOfLegs=0;
static uint  igHaveSecData = FALSE;
static uint  igReset = FALSE;
static int  igQueToElse = -1;
static int  igPrioToElse = 0;
static uint igElseAdHoc = FALSE;
static char pcgIfName[32];
static int  igFlightId = 0;
static int  igElseId = 0;
static uint igMaxMinAfterSched = 360;
static uint igMaxMinBeforeSched = 120;
static uint igNBCInterval = 5;
static uint igNBC = FALSE;
static uint igAlerted = FALSE;
/*HEB*/
static char pcgOrigData[4002] = "\0";
static uint igStopOnError = FALSE;
static uint igAllowRepair = FALSE;
static uint igTestRepair = FALSE;
static uint igHaveToUpdTime = FALSE;
static uint igHaveAF1Data = FALSE;
static uint igNewSearchAlg  = FALSE;
static uint igSchedInterval=0;  /* amount of days in future to set flihgt to FTYP='S' */
static uint igHavePendingDeletes = FALSE; /* remember if we are working on pending deletes */
static uint igIsRTFlight = FALSE;
static char *cgLogAdditionalFields = NULL;
static uint igHavePeriodData = FALSE;
static uint igEnableRecover = TRUE;
static char cgSqlString[2048];  /* Buffer for SQL interface clause */
static uint ilRecoverySleep = 0;
static char cgRecoverySleep[20] = "\0";

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   	HandleQueErr(int);                 /* Handles queuing errors */
static void 	HandleQueues(void);                /* Waiting for Sts.-switch*/


static int  HandleData(EVENT *prpEvent);       /* Handles event data     */

static void config_init();

static int  HandleData_User(BC_HEAD *prlBchead,
			    CMDBLK *prlCmdblk,
			    char *pclSelection,
			    char *pclFields,
			    char *pclData);

static int HandleInterfaceData(char *pclSelection,
			       char *pclFields,
			       char *pclData);

static int HandleFlightData(BC_HEAD *prlBchead,
			    CMDBLK *prlCmdblk,
			    char *pclSelection,
			    char *pclFields,
			    char *pclData);

static int HandleFlightResponseData(BC_HEAD *prlBchead,
				    CMDBLK *prlCmdblk,
				    char *pclSelection,
				    char *pclFields,
				    char *pclData);

ListPtr getFirstFree(ListPtr hook);
static int getItemByName(ListPtr hook, char *name, ListPtr *entry);
static InDataPtr createNewDataEntry(InDataPtr *InData);
static int checkGroupOfPeriodInfo(uint legLine);
static int checkDateSeq(char *firstDate, char *secDate);
static int checkGroupOfRoutingInfo(uint legLine);
static int checkGroupOfFlightIdentifiers(uint legLine, uint *gofidEntry);
static int searchFieldValid(char *field, uint legLine);
static int createItemList(char *list, ListPtr *hook);
static void addDataItemToList(char *list, ListPtr *hook, uint entry, char delimiter, uint defaultStatus);
static int addItemToList(char *element, ListPtr *hook);
static void removeItemFromList (ListPtr hook, ListPtr *pcpGlobal);
static int checkDataValid(char *field, char *data, uint legLine);
static int checkCedaTime(char *data);
static int checkLocation(char *field, char *data);
static int checkFLNO(char *data);
static int checkADID(char *data);
static int getSelection(char *SelectionString, uint  *interface, uint *timeType, uint *quePrio);
static int checkPeriod(char *data);
static char *getItemDataByName(ListPtr hook, char *name, uint legLine);
static int checkGroupOfTimeFieldValues(uint legLine);
static char *getNextItem(char *cpSource, char cpDelim, uint *ipLen, uint *ipLast);
static int checkDBField(char *pcpField, char *pcpData) ;
static void alert(char *cpMessage);
static MessageStateTypePtr getRequestDataByName(ListPtr hook, char *name);
static int catList(ListPtr *first, ListPtr *next);
static void HandleIfRecover(char *);
static void setup_config();
static void deleteItemList(ListPtr hook);
static int initAFTLenTable(ListPtr *cpTableList);
static int prepareData(long ipDataId);
static int getOrPutDBData(uint ipMode);
static int HandleTimerRequest();
static int isAFTField(char *pcField);
static int isAF1Field(char *pcField);
static int isToDeleteField(char *pcField, uint legLine);
static int isUTCTimeField(char *pcField);
static int HandleMultiSectorUpd (uint);
static int HandleMultiSectorData ();
static int HandlePeriodRecords(uint legLine);
static int HandleSingleRecord(char *pcTimeInfo, uint legLine);
static int putItemDataByName(ListPtr hook, char *name, char *newData, uint legLine, uint ilStatus);
static int HandleInsOrUpd(uint legLine);
static int identifyFlightRecord(char *pcpActor, uint legLine);
static int onDelete(long ipUrno);
static int FormatDayFreq (char *pcpFreq, char *pcpFreqChr);
static int isValidDay(char *pclMask, uint day);
static int getNextDay(int *thisDay, int lastDay, uint *ActDay, char *pclMask, uint proceed, char *ppDate, uint ipFrqw);
static int nextValidDay(char *pclMask, uint day);
static int createAFTSelection(char *FieldBuffer, char *DataBuffer, ListPtr fieldList, uint Both, uint legLine);
static int checkUpdateRequired(char *pcgAftFields, char *pcgAftValues, long lgUrno);
static int getFreeUrno(char *cpUrno);
static int checkGroupOfRoutingInfo(uint legLine);
static int checkMaxDataLenOk(char *cpField, char *cpData);
static void setSelConf(char *cpKey, char *cpValue);
static int get_bool(char *Section, char *field, uint entry, int *data);
static int get_int(char *Section, char *field, uint entry, int *data);
static char *to_upper(char *pcgIfName);
static void sendJoinFlight(long llLeftUrno, long llRightUrno);
static char *createSectorsFromRoute(long lpUrno, uint ipADID, char *pclRoute, uint ipInsert);
static void repairDB();
static void setupFLDA();
static int  updateTimeField(char *pcpField, long lpUrno, uint ipLegNo);
static void setupTIFx(uint ipDebug);
static int checkFlightState(long lpUrno, char *pclItem, char *pclValue);
static int setFtyp();
static void insertAF1Data(long plAF1Urno, long plAFTUrno);
static void updateAF1Data(long plAFTUrno);
static void confirmAf1Insert(long llURNO, uint ipUpdate);
static void setPendingDeletes();
static void checkDBConsistency(char *cgSelection, char *cgFields, char *cgData);
static int checkTimeConsistency(char *cpFLDU,char *cpFLDA,char *cpASTOD,char *cpFSTOD,
				char *cpASTOA, char *cpFSTOA, char *cpFLNO, char *cpCdat, 
				char *cpADID, long lpURNO, uint ipRepairFLDA,
				uint ipRepairREFLDU);

extern  char *getItem(char *,char *,char *);
static int fetchFlightData(char *,uint);
static int writeFKEYcomp(uint legLine);
static int getStringValue(char *Section, char *field, uint entry, char **dest);
static void checkFieldValid(char *pclFieldList);
static void CheckEstimatedTime(char *pcpFields, char *pcpData, char *pcpSelection);


/*xxx*/
#include "IMP/wrapElse.src.c"
#include "IMP/wrapCsked.src.c"

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int	ilRc = RC_SUCCESS;			/* Return code			*/
  int	ilCnt = 0;

  INITIALIZE;			/* General initialization	*/


  /*  debug_level = TRACE;*/
  debug_level = DEBUG;

  strcpy(cgProcessName,argv[0]);
	
  dbg(TRACE,"MAIN: version <%s>",mks_version);

  
  /* Attach to the MIKE queues */
  do
    {
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Process: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */



	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	
	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				

		if( ilRc == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			  {
			    /* handle que_ack error */
			    HandleQueErr(ilRc);
			  } /* fi */
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
			  /* process shutdown - maybe from uutil */
			  ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			  if( ilRc != RC_SUCCESS ) {
			    /* handle que_ack error */
			    HandleQueErr(ilRc);
			  } /* fi */
			  Terminate(1);
			  break;
					
			case	RESET		:
			  ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			  if( ilRc != RC_SUCCESS ) {
			    /* handle que_ack error */
			    HandleQueErr(ilRc);
			  } /* fi */
			  ilRc = Reset();
			  break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}

int stopflg = 1;

static int Init_Process()
{
  int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64] = "\0";
  char clKeyword[64] = "\0";
  int ilOldDebugLevel = 0;
  long pclAddFieldLens[12];
  char pclAddFields[256] = "\0";
  char pclSqlBuf[2560] = "\0";
  char pclSelection[1024] = "\0";
  short slCursor = 0;
  short slSqlFunc = 0;
  char  clBreak[24] = "\0";

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
		}
	}

	if(ilRc == RC_SUCCESS)
	{

	  ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
	  igFlightId = tool_get_q_id("flight");
	  igElseId = tool_get_q_id("else");
	}

	if(ilRc == RC_SUCCESS)
	{
	  deleteItemList(pcgAFTLenInfo);
	  pcgAFTLenInfo = NULL;
	  initAFTLenTable(&pcgAFTLenInfo);
	  config_init();
	  setup_config();
	}

	if(ilRc == RC_SUCCESS && igReset == FALSE)
	{
	  HandleIfRecover("IN WORK");
	}

	/* 
	** check for pending deletes
	*/
	if(igReset == FALSE) setPendingDeletes();

	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
	}
	
	return(ilRc);
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");

	igReset = TRUE;

	Init_Process();

	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
  int	   ilRc           = RC_SUCCESS;			/* Return code */
  int      ilCmd          = 0;

  BC_HEAD *prlBchead       = NULL;
  CMDBLK  *prlCmdblk       = NULL;
  char    *pclSelection    = NULL;
  char    *pclFields       = NULL;
  char    *pclData         = NULL;
  char    *pclRow          = NULL;
  char 	  clUrnoList[2400];
  char 	  clTable[34];
  int	  ilUpdPoolJob = TRUE;

  prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
  prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
  pclSelection = prlCmdblk->data;
  pclFields    = pclSelection + strlen(pclSelection) + 1;
  pclData      = pclFields + strlen(pclFields) + 1;
  
  
  strcpy(clTable,prlCmdblk->obj_name);
  
  if (strcmp(prlCmdblk->command,"TIM")) { /* supress info for timer message */ 
    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
    dbg(TRACE, "Cmd <%s> Que (%d) WKS <%s> Usr <%s>", prlCmdblk->command, prgEvent->originator, prlBchead->recv_name, prlBchead->dest_name);
    dbg(TRACE, "Special Info Prio (%d) TWS <%s> TWE <%s>", prgItem->priority, prlCmdblk->tw_start, prlCmdblk->tw_end);
  }
  
  HandleData_User(prlBchead,prlCmdblk,pclSelection,pclFields,pclData);

  if (strcmp(prlCmdblk->command,"TIM")) { /* supress info for timer message */ 
    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
  }
  return(RC_SUCCESS);
	
} /* end of HandleData */

/******************************************************************************/
/* HandleData_User	                                                      */
/*									      */
/* Handle process specific data						      */
/*									      */
/******************************************************************************/
static int HandleData_User(BC_HEAD *prlBchead,
			   CMDBLK *prlCmdblk,
			   char *pclSelection,
			   char *pclFields,
			   char *pclData)
{
  long llDataID = 0;
  int  ilRC;
  char *clIdent;
  char *pclPtr;
  char pclTwStart[64];
  char pclTwStartTmp[64];

  igAlerted = FALSE;
  igHavePeriodData = FALSE;
  strcpy  (pcgActCmd,prlCmdblk->command);
  memset  (pcgRecvName, '\0', (sizeof(prlBchead->recv_name) + 1)); 
  /*  strncpy (pcgRecvName, prlBchead->recv_name, sizeof(prlBchead->recv_name)); */
  strcpy (pcgRecvName, "TOOL");
    
  /* User */
  memset  (pcgDestName, '\0', sizeof(prlBchead->recv_name)) ; 
  strcpy  (pcgDestName, prlBchead->dest_name) ;   /* UserLoginName */
  
  //dbg(TRACE,"$$$$$$$$$$$$$$$$$$$$$line<%d>pcgDestName<%s>",__LINE__,pcgDestName);
  
  sprintf (pcgChkWks, ",%s,", pcgRecvName) ; 
  sprintf (pcgChkUsr, ",%s,", pcgDestName) ; 
    
  /* SaveGlobal Info of CMDBLK */
  strcpy (pcgTwStart, prlCmdblk->tw_start) ; 
  strcpy (pclTwStart, prlCmdblk->tw_start) ; 
  strcpy (pcgTwEnd, prlCmdblk->tw_end) ; 
    
  strcpy (pcgActCmd,prlCmdblk->command);
 
  /*  if (prgEvent->originator == FlightID) {
    HandleFlightData(prlBchead,prlCmdblk,pclSelection,pclFields,pclData);
    }*/
  if (!strcmp(pcgActCmd, "UFR") || !strcmp(pcgActCmd, "IFR") || !strcmp(pcgActCmd, "DFR") || !strcmp(pcgActCmd, "ERR")) {
    /*
    ** Receives 2 kind of messages:
    ** 1.) response to a requested IFR/UFR/DFR
    ** 2.) notification for external IFR/UFR/DFR actions
    **
    ** response messages allways have interface name, urno designators in
    ** tw_start and we only take care of the status record.
    */
    clIdent = strtok(pclTwStart, ",");
    if (clIdent != NULL) {
      if (!strcmp(clIdent, ".NBC.")) {
	clIdent = strtok(NULL, ","); /* skip .NBC. */
	pclPtr = strchr(pcgTwStart, ','); /* remove it from global */
	if (pclPtr != NULL) {
	  pclPtr++;
	  strcpy(pclTwStartTmp, pclPtr);
	  strcpy(pcgTwStart, pclTwStartTmp);
	}
      }
      if (clIdent != NULL) {
	if (!strcmp(clIdent, "else") || !strcmp(clIdent, "csk")) {
	  HandleFlightResponseData(prlBchead,prlCmdblk,pclSelection,pclFields,pclData);
	}
	else {
	  /* obsoleted */
	  /* HandleFlightData(prlBchead,prlCmdblk,pclSelection,pclFields,pclData); */
	}
      }
    }
  }
  else if (!strcmp(pcgActCmd, "IDA")) { /* Interface Data */
    llDataID = atol(pclData);
//Frank@TEST
	lgDataID = llDataID;
//Frank@TEST    

	ilRC = prepareData(llDataID);

//Frank@Test
 	if(igRUN_MODE == 1)
 	{
		dbg(DEBUG,"line<%d><HandleData_User>the end of prepareData",__LINE__);
		#ifdef TEST
			ilRC = RC_SUCCESS;
			dbg(DEBUG,"line<%d>for convience, set ilRC = RC_SUCCESS which will call HandleInterfaceData below",__LINE__);
		#endif
	}
//Frank@Test

    if (ilRC == RC_DELAY) return RC_SUCCESS; /* skipped data in ADHOC mode */
    if (ilRC == RC_IGNORE) {
//Frank@Test
				if(igRUN_MODE == 1)
				{
      		dbg(DEBUG,"<HandleData_User>RC_IGNORE: UPDATE MQRTAB SET STAT='OK' WHERE URNO=%ld",llDataID);
				}
//Frank@Test
	sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='OK' WHERE URNO=%ld", llDataID);
	ilRC = getOrPutDBData(START|COMMIT);
        if (ilRC != RC_SUCCESS) {
          dbg(TRACE,"<HandleData_User> Unable to acknowledge MQR data URNO <%ld>", llDataID);
        }
	return RC_IGNORE; /* skip data */
    }
    if (ilRC == RC_FAIL) {
      alert("Invalid data received from interface");
//Frank@Test
			if(igRUN_MODE == 1)
			{
      	dbg(DEBUG,"<HandleData_User>RC_FAIL: UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld",llDataID);
			}
//Frank@Test
      sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld", llDataID);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC != RC_SUCCESS) {
	dbg(TRACE,"<HandleData_User> Unable to acknowledge MQR data URNO <%ld>", llDataID);
      }
      return ilRC;
    }
	
    dbg(DEBUG, "Fields:<%s> Data:<%s> Selection: <%s>", cgFields, cgData, cgSelection);
    ilRC = HandleInterfaceData(cgSelection,cgFields,cgData);
    if (ilRC != RC_SUCCESS) {
      if (igAlerted == FALSE) alert("Error while handling interface data");
//Frank@Test
			if(igRUN_MODE == 1)
				{
      		dbg(DEBUG,"++++++Frank@Test<HandleData_User>: UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld",llDataID);
				}
//Frank@Test
      sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld", llDataID);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC != RC_SUCCESS) {
	dbg(TRACE,"<HandleData_User> Unable to acknowledge MQR data URNO <%ld>", llDataID);
      }
      return ilRC;
    }
  }
  else if (!strcmp(pcgActCmd, "TIM")) {
    ilRC = HandleTimerRequest();
  }
  else if (!strcmp(pcgActCmd, "REQ")) {
    igElseAdHoc = TRUE;
    ilRC = sendReqToElse(pclData);
  }
  else if (!strcmp(pcgActCmd, "UPS")) {
    /* nothing to do -- ignore */
  }
  else if (!strcmp(pcgActCmd,"UFB") || !strcmp(pcgActCmd, "DFB")) {
    /*	
    ** those requests will bypass the ELSE/CSKED wrappers
    ** data are expected in fieldlist/datalist presentation
    ** the interfaces will be faked as ELSE
    */
    if (!strcmp(pcgActCmd,"UFB")) strcpy(pcgActCmd,"UFR");
    else if (!strcmp(pcgActCmd,"DFB")) strcpy(pcgActCmd,"DFR");
    ilRC=HandleInterfaceData(pclSelection,pclFields,pclData);
  }
  else {
    dbg(TRACE,"<HandleData_User> Unknown Command <%s>", pcgActCmd);
    return RC_FAIL;
  }
  return ilRC;
}
/******************************************************************************/
/* HandleIfRecover                                                            */
/*                                                                            */
/* Retrieve outstanding requests from MQRTAB and try to work on it. This func */
/* may only be called after a cold start!                                     */ 
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static void HandleIfRecover(char *pcpMode)
{
  short slFkt, slCursor;
  int    ilGetRc;
  long   llUrno;
  int    ilRc;
  long   llEvent=0;
  char   pclSqlBuffer[256];
  char   pclDataArea[256];


  ilRc = ReadConfigEntry("MAIN","RECOVERY_SLEEP",cgRecoverySleep);
  ilRecoverySleep = atol(cgRecoverySleep);
  dbg(TRACE,"RECOVERY_SLEEP: <%d>",ilRecoverySleep);
  if (igEnableRecover == FALSE) return;

  sprintf(pclSqlBuffer,"SELECT URNO FROM MQRTAB WHERE STAT='%s' AND (INAM='else' OR INAM='csk') ORDER BY TPUT", pcpMode);
  slFkt = START;
  slCursor = 1;

  strcpy (pcgRecvName, "TOOL");
  sprintf (pcgChkWks, ",%s,", pcgRecvName) ; 

  dbg(DEBUG,"<HandleIfRecover> Search statement used <%s>",pclSqlBuffer);

  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuffer, pclDataArea),dbg(DEBUG,"++++++Frank@Test ilGetRc<%d>",ilGetRc),ilGetRc) == DB_SUCCESS) {
//Frank@test
			if(igRUN_MODE == 1)
			{
    		dbg(DEBUG,"the start of HandleIfRecovery-while loop");
    		dbg(DEBUG,"ilGetRc<%d> igAllowDeletes[igIfType]<%d>",ilGetRc,igAllowDeletes[igIfType]);
			}
//Frank@test    
	if (ilGetRc == DB_SUCCESS && igAllowDeletes[igIfType] == TRUE ) {
      llUrno = atol(pclDataArea);
      prgEvent = NULL; /* flag recover mode */
      /*
      ** mark this event 
      */
//Frank@Test
	if(igRUN_MODE == 1)
			{
      	dbg(DEBUG,"<HandldIfRecover>: UPDATE MQRTAB SET STAT='IMPMAN' WHERE URNO=%ld",llUrno);
			}
//Frank@Test
      sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='IMPMAN' WHERE URNO=%ld", llUrno);
      ilRc = getOrPutDBData(START|COMMIT);
      if (ilRc != RC_SUCCESS) {
	dbg(TRACE,"<HandleIfRecover> Unable to mark MQRTAB entry <%ld>", llUrno);
      }
      strcpy(pcgTwStart, "");
      llEvent++;
      dbg(TRACE,"========== START Recover <%10.10d> ==========",llEvent);
      ilRc = prepareData(llUrno);
      if (ilRc == RC_IGNORE) { /* Data will be ignored but the request will be acknowledged 'OK' */
//Frank@Test
			if(igRUN_MODE == 1)
			{
      	dbg(DEBUG,"<HandldIfRecover> UPDATE MQRTAB SET STAT='OK' WHERE URNO=%ld",llUrno);
			}
//Frank@Test
	sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='OK' WHERE URNO=%ld", llUrno);
	ilRc = getOrPutDBData(START|COMMIT);
        if (ilRc != RC_SUCCESS) {
          dbg(TRACE,"<HandleIfRecover> Unable to acknowledge MQR data URNO <%ld>", llUrno);
        }
      }
      else if (ilRc == RC_FAIL) {
	alert("Invalid data received from interface");
//Frank@Test
			if(igRUN_MODE == 1)
      {	
      	dbg(DEBUG,"<HandldIfRecover_RC_FAIL>: UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld",llUrno);
			}
//Frank@Test
	sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld", llUrno);
	ilRc = getOrPutDBData(START|COMMIT);
	if (ilRc != RC_SUCCESS) {
	  dbg(TRACE,"<HandleIfRecover> Unable to acknowledge MQR data URNO <%ld>", llUrno);
	}
	if (igStopOnError) {
	  dbg(TRACE,"<HandleIfRecover> **** Stopping recovery mode due to STOP_RECOVERY_ON_ERROR request in cfg");
	  dbg(TRACE,"<HandleIfRecover> **** MQRTAB entry of this error has URNO <%ld>", llUrno);
	  return;
	}
      }
      else {
	if (igIfType == CSK) strcpy(pcgDestName,"csk");
	else if (igIfType == ELSE)  strcpy(pcgDestName,"else");
	else {
	  dbg(TRACE,"<HandleIfRecover> Warning -- invalid interface type <%d>");
	  strcpy(pcgDestName,"");
	}
	sprintf (pcgChkUsr, ",%s,", pcgDestName) ; 
	ilRc = HandleInterfaceData(cgSelection,cgFields,cgData);
	if (ilRc != RC_SUCCESS) {
	  if (igAlerted == FALSE) alert("Error while handling interface data");
//Frank@Test
				if(igRUN_MODE == 1)
      	{
      		dbg(DEBUG,"<HandldIfRecover>: UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld",llUrno);
      	}
//Frank@Test
	  sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%ld", llUrno);
	  ilRc = getOrPutDBData(START|COMMIT);
	  if (ilRc != RC_SUCCESS) {
	    dbg(TRACE,"<HandleIfRecover> Unable to acknowledge MQR data URNO <%ld>", llUrno);
	  }
	}
      }
      dbg(TRACE,"========== END Recover <%10.10d> ==========",llEvent);
    }
    slFkt = NEXT;
	  dbg(TRACE,"Now sleep for <%d> milliseconds",ilRecoverySleep);
	nap(ilRecoverySleep);
  }
  close_my_cursor (&slCursor);
}
/******************************************************************************/
/* HandleTimerRequest                                                         */
/*                                                                            */
/* This function is called once per minute. FSLTAB is scanned for records     */
/* status 'ONDELETE' and timer value 0. Those records are expired for delete  */
/* and will be removed from both IFTTAB and AFTTAB. All records with timer    */
/* values <> 0 will be decremented.                                           */
/*                                                                            */
/* RFC6470 -- handle else timer different, i.e. set flight to 'X' if timer    */
/* expires                                                                    */
/******************************************************************************/
static int HandleTimerRequest()
{
  int ilRC;
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  uint ilReceiver = igFlightId;
  uint llURNO;
  uint ilPrio;
  char *pclData;
  char pclTwStart[64];
  char pclSqlBuf[1024];
  char pclDataArea[256];
  char clActor[33];
  MessageStateTypePtr pclRequestData;

  if (igHavePendingDeletes) {
    dbg(TRACE,"========== START pending delete handling ==========");
    sprintf(pclSqlBuf,"SELECT UAFT,RQID,USEC FROM IFTTAB WHERE DELF=0 AND STAT='ONDELETE'");
    slFkt = START;
    slCursor = 0;

    while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS) {
      if (ilGetRc == DB_SUCCESS) {
	BuildItemBuffer(pclDataArea, NULL, 3, "/");
	pclData = strtok(pclDataArea, "/");
	if (pclData != NULL) llURNO = atol(pclData);
	pclData = strtok(NULL, "/");
	if (pclData != NULL) {
	  strcpy(pclTwStart, pclData);
	  ConvertDbStringToClient(pclTwStart);
	  /* cut unused blanks from string */
	  pclData = strchr(pclTwStart, ' ');
	  *pclData = '\0';
	}
	pclData = strtok(NULL, "/");
	if (pclData != NULL) {
	  strcpy(clActor, pclData);
	}
	else {
	  dbg(TRACE,"<HandleTimerRequest> No actor found -- assuming csked");
	  strcpy(clActor,"csked");
	}
	
	/* Frank v4.12b */
	memset(pcgDestName,0,sizeof(pcgDestName));
	strcpy(pcgDestName,clActor);
	dbg(TRACE,"<HandleTimerRequest>pcgDestName<%d>",pcgDestName);
	dbg(TRACE,"<HandleTimerRequest>pcgRecvName<%d>",pcgRecvName);
	dbg(TRACE,"<HandleTimerRequest>pclTwStart<%d>",pclTwStart);
	dbg(TRACE,"<HandleTimerRequest>pcgTwEnd<%d>",pcgTwEnd);
	dbg(TRACE,"<HandleTimerRequest>pcgSelection<%d>",pcgSelection);
	dbg(TRACE,"<HandleTimerRequest>pcgAftFields<%d>",pcgAftFields);
	dbg(TRACE,"<HandleTimerRequest>pcgAftValues<%d>",pcgAftValues);
	
	if (!strncmp(clActor,"csked",5) && igAllowDeletes[igIfType] == TRUE) {
	  pcgAftFields[0] = '\0';
	  pcgAftValues[0] = '\0';
	  memset (pcgSelection, '\0', 1024) ;
	  strcpy(pcgActCmd,"DFR");
	  sprintf(pcgSelection, "WHERE URNO=%d", llURNO);
	  if (igPrioFromIf != 0) {
	    ilPrio = igPrioFromIf;
	  }
	  else ilPrio = igPrio[igIfType];
	  
	  CheckEstimatedTime(pcgAftFields, pcgAftValues, pcgSelection);
	  ilRC = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
			       pcgActCmd, "AFTTAB", pcgSelection,
			       pcgAftFields, pcgAftValues, "", ilPrio, NETOUT_NO_ACK) ;
	  if (ilRC != RC_SUCCESS && ilRC != QUE_E_INACTIVE) {
	    dbg(TRACE,"<HandleTimerRequest> Unable to send ceda event <%d>", ilRC);
	    dbg(TRACE,"========== END pending delete handling ==========");
	    return RC_FAIL;
	  }
	  /* This entry does exist. Set it to 'DELETING' only if the DFR was succesfully sent! */
	  sprintf(pcgSqlBuf,"UPDATE IFTTAB SET STAT='DELETING' WHERE UAFT='%ld'",llURNO);
	  ilRC = getOrPutDBData(START|COMMIT);
	  fixRequest(pclTwStart);
	}
	else if (!strncmp(clActor,"csked",5) && igAllowDeletes[igIfType] == FALSE) {
	  alert("<HandleTimerRequest> Expired deletions have not been forwarded due to a ALLOW_DELETES=NO configuration");
	}
	else { /* this should be ELSE */
	  /* we have to update its FTYP to 'X' */
	  strcpy(pcgAftFields,"FTYP");
	  strcpy(pcgAftValues, "X");
	  memset (pcgSelection, '\0', 1024) ;
	  strcpy(pcgActCmd,"UFR");
	  sprintf(pcgSelection, "WHERE URNO=%d", llURNO);
	  if (igPrioFromIf != 0) {
	    ilPrio = igPrioFromIf;
	  }
	  else ilPrio = igPrio[igIfType];
	  
	  CheckEstimatedTime(pcgAftFields, pcgAftValues, pcgSelection);
	  ilRC = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
			       pcgActCmd, "AFTTAB", pcgSelection,
			       pcgAftFields, pcgAftValues, "", ilPrio, NETOUT_NO_ACK) ;
	  if (ilRC != RC_SUCCESS && ilRC != QUE_E_INACTIVE) {
	    dbg(TRACE,"<HandleTimerRequest> Unable to send ceda event <%d>", ilRC);
	    return RC_FAIL;
	  }
	  /* This entry does exist. Delete it if the DFR was succesfully sent! */
	  sprintf(pcgSqlBuf,"DELETE FROM IFTTAB WHERE UAFT='%ld'",llURNO);
	  ilRC = getOrPutDBData(START|COMMIT);
	  fixRequest(pclTwStart);
	}
      }
      slFkt = NEXT;
    }
    close_my_cursor (&slCursor);
    
    /*
    ** usage of STAT='ONDELETE' is used to preselect the set of DB entries
    ** to be worked on
    */
    sprintf(pcgSqlBuf,"UPDATE IFTTAB SET DELF=DELF-1 WHERE STAT='ONDELETE' AND DELF<>0");
    ilRC = getOrPutDBData(START|COMMIT);
    /*
    ** check for remaining requests
    */
    setPendingDeletes();
    dbg(TRACE,"========== END pending delete handling ==========");
    return ilRC;
  } /* end igHavePendingDeletes */
}
/******************************************************************************/
/* setPendingDeletes                                                          */
/*                                                                            */
/* Set the count of remaining pending deletes                                 */
/*                                                                            */
/******************************************************************************/
static void setPendingDeletes()
{
  int ilRC;

  sprintf(pcgSqlBuf,"SELECT count(UAFT) FROM IFTTAB WHERE STAT='ONDELETE'");
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_NODATA) igHavePendingDeletes=0;
  else if (ilRC == RC_SUCCESS) {
    igHavePendingDeletes = atoi(pcgDataArea);
    dbg(DEBUG, "<setPendingDeletes> New count of pending deletes is <%d>",igHavePendingDeletes);
  }
}
/******************************************************************************/
/* alert                                                                      */
/*                                                                            */
/* This is a preliminary alerter function, which simply prints a message into */
/* the log file.                                                              */
/******************************************************************************/
static void alert(char *cpText)
{
  int ilRC;
  char clTimeStamp[15];
  /*
  GetServerTimeStamp("UTC", 1, 0, clTimeStamp);
  UtcToLocalTimeFixTZ(clTimeStamp);
  */
  if ((strcmp(pcgIfName,"else")==0) || (strcmp(pcgIfName,"elseah")==0))
  {
      strcpy(pcgIfName,"ELSE");
  }
  if (strcmp(pcgIfName,"csk")==0)
  {
      strcpy(pcgIfName,"CSKED");
  }

  ilRC = AddAlert2(to_upper(pcgIfName), "R", " ", "U", cpText,pcgOrigData, TRUE, FALSE, " ", " ", " ", " ");  
}

static char *to_upper(char *pcgIfName)
{
  char *ptr;

  ptr = pcgIfName;
  while (*ptr != '\0') {
    *ptr = toupper(*ptr);
    ptr++;
  }
  return pcgIfName;
}
/******************************************************************************/
/* HandleInterfaceData  	                                              */
/*									      */
/* Handle new instance of process specific data				      */
/*									      */
/******************************************************************************/
static int HandleInterfaceData(char *pclSelection,
			       char *pclFields,
			       char *pclData)
{
  uint numOfLegs=0;     /* Number of legs in data stream */
  uint legLine;
  uint gofidEntry;      /* First available entry in the list of group of flight identifiers */
  uint ilFirstValidLeg;
  uint ilLegNo = 0;
  char *pclDataLines[MAX_NUM_LEGS];  /* maximum number of legs we can handle */
  char pclTwStart[64];
  char *Item;
  char *Data;
  char *pclLegEnd;
  char clMessage[132];
  char clRoute[2];
  ListPtr pclItemList;
  int ilRc;
  uint ilStatus;
  uint ilFlnoLen;
  /*
  ** start with clean Field/Data/Selection
  */
  pcgAftFields[0] = '\0';
  pcgAftValues[0] = '\0';
  pcgSelection[0] = '\0';
  igHaveSecData = FALSE;
  igNBC = FALSE; /* supress broadcasts flag */
  igHaveToUpdTime = FALSE;
  igHaveAF1Data = FALSE;
  igIsRTFlight = FALSE;
  lgUrno = 0;
  lgOnDelUrno = 0;
  strcpy(pclTwStart,pcgTwStart);

  if (pclSelection == NULL || pclFields == NULL || pclData == NULL) {
    dbg(TRACE,"<HandleInterfaceData> Fatal -- received corrupted data");
    return RC_FAIL;
  }

  ilRc = getSelection(pclSelection,&igIfType, &igTimeFromIf, &igPrioFromIf);
  if (ilRc != RC_SUCCESS) {
    return RC_FAIL;
  }

  sprintf(pcgTwEnd, "%s,%s,%s", cgHopo, cgTabEnd, cgProcessName);

  if (igIfType == CSK) igHaveSecData = TRUE;

  dbg(TRACE,"<HandleInterfaceData> Working on %s data", Section[igIfType]);
  
  if(config[igIfType].section_configured == FALSE) {
      /* sprintf(clMessage,"<HandleInterfaceData> Requested interface <%s> not configured -- data not accepted", Section[igIfType]);
	 alert(clMessage);*/
    return RC_FAIL;
  }
  /*
  ** separate data lines to strings
  */
  if ((pclDataLines[numOfLegs] = strtok(pclData, "\n")) == NULL) {
    numOfLegs++;
    pclDataLines[numOfLegs] = pclData;
  }
  else {
    numOfLegs++;
    while (( pclLegEnd = strtok(NULL, "\n")) != NULL) {
      /* *pclLegEnd++ = '\0'; */
      pclDataLines[numOfLegs] = pclLegEnd;
      numOfLegs++;
      if (numOfLegs>=MAX_NUM_LEGS) {
	/* error message */
	dbg(TRACE,"<HandleInterfaceData> No space for additional Leg %d, increase MAX_NUM_LEGS!",numOfLegs);
	break;
      }
    }
  }
  igNumOfLegs = numOfLegs;
  if (igHaveSecData == TRUE) ilFirstValidLeg = 1;
  else ilFirstValidLeg = 0;
  /*  pcgCurrentData = createNewDataEntry(&InData); */
  createItemList(pclFields, &pcgItemList);
  for (legLine = ilFirstValidLeg; legLine < numOfLegs+ilFirstValidLeg; legLine++) {
    dbg(DEBUG,"<HandleInterfaceData> Leg data: <%s>",pclDataLines[legLine-ilFirstValidLeg]); 
    addDataItemToList(pclDataLines[legLine-ilFirstValidLeg], &pcgItemList, legLine, ',', INVALID);
  }
  /*  pcgItemList = pcgCurrentData->ifData; */
  for (legLine = ilFirstValidLeg; legLine < numOfLegs+ilFirstValidLeg; legLine++) {
    pclItemList = pcgItemList;
    /*
    ** check field data validity
    ** for each field
    */
    while (pclItemList != NULL) {
      Item = pclItemList->field;
      Data = pclItemList->dobj[legLine].data;

      /* dbg(TRACE,"%05d: ***HEB*** Item: <%s>, Data <%s>, legline: <%d>", __LINE__,Item,Data,legLine); */
      if (Data != NULL) {
	ilRc = checkDataValid(Item, Data,legLine);
	if (ilRc == RC_SUCCESS || ilRc == RC_HASTEMPLATE) {
	  ilStatus = IS_VALID;
	  if (!strcmp(Item,"FLNO")) {
	    /*
	    ** check suffix at position 9
	    */
	    ilFlnoLen = strlen(Data);
	    if (ilFlnoLen > 8) {
	      if (*(Data+8) != ' ') igIsRTFlight = TRUE;
	    }
	  }
	  if (isAFTField(Item)) {
	    ilStatus = ilStatus | IS_AFT;
	  }
	  if (isAF1Field(Item)) {
	    igHaveAF1Data = TRUE;
	    ilStatus = ilStatus | IS_AF1;
	  }
	  if (isUTCTimeField(Item)) {
	    ilStatus = ilStatus | IS_UTCTIME;
	  }
	  if (isToDeleteField(Item, legLine)) {
	    if (igUpdateEmpty == FALSE) { /* those fields get invisible for getAFTSelection */
	      ilStatus = ilStatus & ~IS_AFT; /* clear IS_AFT */
	    }
	  }
	  if (ilRc == RC_HASTEMPLATE) {
	    ilStatus = ilStatus | IS_DATEONLY;
	    igHaveToUpdTime = TRUE;
	  }
	  pclItemList->dobj[legLine].status = ilStatus;	
	}
	/* Has invalid data -> mark as invalid */ 
	/*      else pclItemList->dobj[legLine].status = IS_INVALID;*/
	/* cleanup and skip the rest */
	else {
	    /*sprintf(clMessage, "Record has invalid data Field: <%s> Data: <%s>", Item, Data);
	      alert(clMessage);*/
	  deleteItemList(pcgItemList);
	  pcgItemList = NULL;
	  return ilRc;
	}
      }
      pclItemList = pclItemList->next;
    }
    if (igIfType == CSK) { /* provide FLDU instead of FLDA */
      pclData =  getItemDataByName(pcgItemList, "FLDA", legLine);
      if (pclData != NULL) {
	putItemDataByName(pcgItemList, "FLDU", pclData, legLine, IS_VALID);
	putItemDataByName(pcgItemList, "FLDA", "", legLine, IS_VALID);
      }
    }
    /*
    ** At this point all fields of the current 
    ** legLine are checked
    ** fields are marked as valid if data is ok
    ** Now we need to check if all required fields 
    ** are availabe with valid data
    */

    /*
    ** If consistency check of this group
    ** fails, the current leg will be ignored
    */
    ilRc = checkGroupOfFlightIdentifiers(legLine, &gofidEntry);
    if (ilRc == RC_FAIL) {
      dbg(TRACE,"<HandleInterfaceData> Group of Flight Identifiers inconsistency leg %d data skipped",legLine);
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return RC_FAIL;
    }
    /*
    ** If consistency check of this group
    ** fails, the current leg will be ignored
    */
    ilRc = checkGroupOfRoutingInfo(legLine);
    if (ilRc == RC_FAIL) {
      dbg(TRACE,"<HandleInterfaceData> Group of Routing Info inconsistency leg %d data skipped",legLine);
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return RC_FAIL;
    }
    if (igIfType == CSK) { /* check for overall routing */
      if (pcgItemList->RouteType[legLine] == A) sprintf(clRoute,"A");
      else if (pcgItemList->RouteType[legLine] == D) sprintf(clRoute,"D");
    }
    /*
    ** If period info is not valid, we will not
    ** insert that info, but the rest of data 
    ** will be inserted
    */
//Frank@Test
		if(igRUN_MODE == 1)
		{
	  	dbg(DEBUG,"<HandleInterfaceData>Calling checkGroupOfPeriodInfo");
	  }
//Frank@Test
    ilRc = checkGroupOfPeriodInfo(legLine);
    if (ilRc == RC_FAIL) {
      dbg(TRACE,"<HandleInterfaceData> Group of Period Info inconsistency leg %d data skipped",legLine);
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return RC_FAIL;
    }
   ilRc = checkGroupOfTimeFieldValues(legLine);
   if (ilRc == RC_FAIL) {
     dbg(TRACE,"<HandleInterfaceData> Group of Time Field Info inconsistency leg %d data skipped",legLine);
     deleteItemList(pcgItemList);
     pcgItemList = NULL;
     return RC_FAIL;
   }
  } /* end for legLines.... */

//Frank@Test
  if(igRUN_MODE == 1)
  {
		dbg(DEBUG,"<HandleInterfaceData>end for legLines....");
		#ifdef TEST
			pcgItemList->periodInfo[0].HavePeriodInfo = TRUE;
			dbg(DEBUG,"line<%d><HandleInterfaceData>pcgItemList->periodInfo[0].HavePeriodInfo == TRUE",__LINE__);
		#endif
	}
//Frank@Test
  /*
  ** check if we need to handle a single 
  ** flight record or a set of seasonal
  ** flight records
  */
  
  if (igHaveSecData) ilLegNo = 1;

  if (igHaveSecData && strcmp(pcgActCmd, "DFR")) {
    if (!strcmp(pcgActCmd, "IFR"))
      ilRc = HandleMultiSectorData ();  
    else if (!strcmp(pcgActCmd, "UFR")) {
      if (igIfType == CSK) { /* only DFR will arrive here */
	if (!strcmp(clRoute,"A")) ilLegNo = igNumOfLegs;
	else if (!strcmp(clRoute,"D")) ilLegNo = 1;
	else {
	  dbg(TRACE,"<HandleInterfaceData> Unknown CSKED routing <%s> detected",clRoute);
	  deleteItemList(pcgItemList);
	  pcgItemList = NULL;
	  return RC_FAIL;
	}
      }
      ilRc = HandleMultiSectorUpd (ilLegNo);
    }
  }
  else if (pcgItemList->periodInfo[0].HavePeriodInfo == TRUE) {
//Frank@Test
	if(igRUN_MODE == 1)
	{
		dbg(DEBUG,"<HandleInterfaceData>calling HandlePeriodRecords");
	}
//Frank@Test
	ilRc = HandlePeriodRecords(0);
  }
  else {
    if (igIfType == CSK) { /* only DFR will arrive here */
      if (!strcmp(clRoute,"A")) ilLegNo = igNumOfLegs;
      else if (!strcmp(clRoute,"D")) ilLegNo = 1;
      else {
	dbg(TRACE,"<HandleInterfaceData> Unknown CSKED routing <%s> detected",clRoute);
	deleteItemList(pcgItemList);
	pcgItemList = NULL;
	return RC_FAIL;
      }
      dbg(DEBUG,"<HandleInterfaceData>Using leg <%d> for CSKED",ilLegNo);
    }
//Frank@Test
	if(igRUN_MODE == 1)
	{
		dbg(DEBUG,"<HandleInterfaceData>calling HandleSingleRecord");
	}
//Frank@Test    
	ilRc = HandleSingleRecord(NULL, ilLegNo);
  }
  if (ilRc == RC_SUCCESS) { 
    performJoinAction(ilLegNo);
    fixRequest(pclTwStart);
  }
  /*
  ** only remove if request completed 
  */
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
  return ilRc;
}
/******************************************************************************/
/* performJoinAction                                                          */
/*                                                                            */
/* check for join flights                                                     */
/* JFLN the flight number of the flight to join to is always mandatory        */
/* JDAY may be missing and is tried to be computed                            */
/* The main flight with routing                                               */
/*   Arrival will be joint to a corresponding departure                       */
/*   Departure will be joint to a corresponding arrival                       */
/*										*/
/* UFIS-244 : handle non-sched flights using CSGN. We expect linked flight's	*/
/* CSGN to be sent to us as JFLN from wrapElse.src.c.				*/
/*                                                                            */
/******************************************************************************/
int performJoinAction(uint ipLegNo)
{
  uint ilHaveJDAY = FALSE;
  uint ilDataOk = FALSE;
  uint ilHaveJUrno = FALSE;
  uint ilADID;
  int  ilRc;
  long llLeftUrno;    /* arrival URNO for join flights */
  long llRightUrno;   /* departure URNO for join flights */
  char *pclJFLN,*pclJDAY;  /* join flight fields */
  char *pclSTOD,*pclSTOA;  /* related sched times */
  char clADID[2];
  char clJDAY[9];

  sprintf(clADID," ");
  pclJFLN = getItemDataByName(pcgItemList, "JFLN", 0);
  pclJDAY = getItemDataByName(pcgItemList, "JDAY", 0);
  sprintf(clJDAY," "); /* initialize */
  if (pclJDAY != NULL) {
    sprintf(clJDAY,"%8s", pclJDAY);
    ilHaveJDAY = TRUE;
  }
  if (pclJFLN != NULL) {
    ilADID = pcgItemList->RouteType[ipLegNo];
    if (ilADID == D) {
      /* handle departure i.e. search for arrival join */
      llRightUrno = lgUrno;
      sprintf(clADID, "A");
      pcgItemList->RouteType[1] = A;
      pclSTOD = getItemDataByName(pcgItemList, "STOD", 0); /* fetch STOD from ELSE, which is LOCAL */
      LocalTimeToUtcFixTZ(pclSTOD); /* search is in AFT -> UTC */
      if (pclSTOD == NULL) {
	/* fetch it from DB */
	sprintf(pcgSqlBuf, "SELECT STOD FROM AFTTAB WHERE URNO=%ld", lgUrno);
	ilRc = getOrPutDBData(START);
	if (ilRc == RC_SUCCESS) pclSTOD = pcgDataArea;
	else pclSTOD = NULL;
      }
      if ((pclJDAY == NULL) && (pclSTOD != NULL)) {
	sprintf(clJDAY, "%8.8s", pclSTOD);  /* only for logging purposes - not a good idea to use for searching, unable to link to flights on same day */

      if (igIsNonSched == 0)
      {
	sprintf(pcgSqlBuf, "SELECT URNO FROM AFTTAB WHERE STOA<'%s' AND FLNO='%s' AND ADID='A' ORDER BY STOA DESC", pclSTOD, pclJFLN);
      }
      else  /* Non Sched UFIS-244 */
      {
	sprintf(pcgSqlBuf,
         "SELECT URNO FROM AFTTAB WHERE STOA<'%s' AND CSGN='%s' AND ADID='A' ORDER BY STOA DESC",
         pclSTOD, pclJFLN);
      }
	ilDataOk = TRUE;
     }
      /* 
      ** If JDAY was specified -> have to link exactly this one
      ** This is departure, so arrival STOA has to be before departure STOD
      ** This is not checked here!
      */
      else if (pclJDAY != NULL) {
	putItemDataByName(pcgItemList, "FLDA", pclJDAY, 1, IS_VALID);
	putItemDataByName(pcgItemList, "ADID", clADID, 1, IS_VALID);

        if (igIsNonSched == 0)
          putItemDataByName(pcgItemList, "FLNO", pclJFLN, 1, IS_VALID);
        else
          putItemDataByName(pcgItemList, "CSGN", pclJFLN, 1, IS_VALID);

	ilRc = identifyFlightRecord(NULL,1);
	/* needs modification *********** ilRc = fetchFlightByFLDA(pclJDAY, pclJFLN, clADID); */
	if (ilRc == RC_SUCCESS) ilHaveJUrno = TRUE;
      }
    }
    else if (ilADID == A) {
      llLeftUrno = lgUrno;
      sprintf(clADID, "D");
      pcgItemList->RouteType[1] = D; 
      pclSTOA = getItemDataByName(pcgItemList, "STOA", 0);
      LocalTimeToUtcFixTZ(pclSTOA);
      if (pclSTOA == NULL) {
	/* fetch it from DB */
	sprintf(pcgSqlBuf, "SELECT STOA FROM AFTTAB WHERE URNO=%ld", lgUrno);
	ilRc = getOrPutDBData(START);
	pclSTOA = pcgDataArea;
      }
      if ((pclJDAY == NULL) && (pclSTOA != NULL)) {
	sprintf(clJDAY, "%8.8s", pclSTOA); /* for logging puposes not a good idea for searching, unable to link to flights on same day */
	/*
	** To be more precise it should be STOD>STOA+reasonable offset
	*/
        if (igIsNonSched == 0)
        {
	  sprintf(pcgSqlBuf, "SELECT URNO FROM AFTTAB WHERE STOD>'%s' AND FLNO='%s' AND ADID='D' ORDER BY STOD", pclSTOA, pclJFLN);
        }
        else
        {
          sprintf(pcgSqlBuf,
            "SELECT URNO FROM AFTTAB WHERE STOD>'%s' AND CSGN='%s' AND ADID='D' ORDER BY STOD",
            pclSTOA, pclJFLN);
        }
	ilDataOk = TRUE;
      }
      /* 
      ** If JDAY was specified -> have to link exactly this one
      ** This is arrival, so deaprture STOD has to be after before arrival STOA
      ** This is not checked here!
      */
      else if (pclJDAY != NULL) {
	putItemDataByName(pcgItemList, "FLDA", pclJDAY, 1, IS_VALID);
	putItemDataByName(pcgItemList, "ADID", clADID, 1, IS_VALID);

        if (igIsNonSched == 0)
          putItemDataByName(pcgItemList, "FLNO", pclJFLN, 1, IS_VALID);
        else
          putItemDataByName(pcgItemList, "CSGN", pclJFLN, 1, IS_VALID);

	ilRc = identifyFlightRecord(NULL,1);
	if (ilRc == RC_SUCCESS) ilHaveJUrno = TRUE;
      }
    }
    if (ilDataOk) { /* we have a valid SELECT try to find a flight */
      ilRc = getOrPutDBData(START);
      if (ilRc == RC_SUCCESS) dbg(DEBUG,"<performJoinAction>SQL Performed: <%s> Result: Flight found",pcgSqlBuf);
      else dbg(DEBUG,"<performJoinAction>SQL Performed: <%s> Result: Flight not found",pcgSqlBuf);
      lgUrno = 0;
      if (ilRc != RC_SUCCESS) {
	if (ilDataOk) 
	dbg(TRACE, "<performJoinAction> Unable to join flights due to missing flight <%s>/<%s> on <%s>",pclJFLN, clADID,clJDAY);
	return RC_FAIL;
      }
      else {
	lgUrno = atol(pcgDataArea);
      }
    }
    else if (!ilHaveJUrno) { /* search for a specific JDAY failed -> no join possible */
      dbg(TRACE, "<performJoinAction> Unable to join flights due to missing flight <%s>/<%s> on <%s>",pclJFLN, clADID,clJDAY);
      return RC_FAIL;
    }
    
    /*
    ** At this point we have both URNOs
    */
    if (ilADID == D) llLeftUrno = lgUrno;
    else llRightUrno = lgUrno;
    dbg(DEBUG, "<performJoinAction> Joining Left <%ld> to Right <%ld>", llLeftUrno, llRightUrno);
    /*
    ** Send a request to flight and assume everything 
    ** will work well
    */
    sendJoinFlight(llLeftUrno, llRightUrno);
    ilRc = RC_SUCCESS;
  }
}
/******************************************************************************/
/* HandleMultiSectorUpd                                                       */
/*                                                                            */
/* This function will only update the HOPO connected sector of a flight       */
/*                                                                            */
/* Input:                                                                     */
/*        the global item list                                                */
/*                                                                            */
/* Return:                                                                    */
/*         RC_FAIL/RC_SUCCESS                                                 */
/*                                                                            */
/******************************************************************************/
static int HandleMultiSectorUpd (uint ipLegNo)
{
  int  ilRC;
  uint ilNUMS=0;
  uint ilMaxSecNum = 0;
  uint ilMinSecNum = 0;
  uint ilFirstSector = 0;
  uint ilLastSector = 0;
  uint i;
  uint ilNum = 0;
  uint entry = 0;
  uint ilStatus = 0;
  ListPtr pclTemp = NULL;
  char *pclData;
  char *pclADID;
  char *pclFLDU;
  char clADID[2];
  char clFLDA[15];
  char clFLNO[10];
  char clURNO[32];
  char clOrg3[5];
  char clDes3[5];
  char clMessage[128];
  char clSelect[256];
  char clRoute[2048];
  char *clRouteList [] = { "ORG3", "ORG4", "DES3", "DES4", NULL };
  /*
  ** first we need to have the number of sectors in data set
  */
  pclADID = getItemDataByName(pcgItemList,"ADID",ipLegNo);
  if (pclADID == NULL) {
    dbg(TRACE, "<HandleMultiSectorUpd> Unable to perform sector update ADID missing");
    return RC_FAIL;
  }
  if (!strcmp(pclADID,"A")) pcgItemList->RouteType[ipLegNo] = A;
  else if (!strcmp(pclADID,"D")) pcgItemList->RouteType[ipLegNo] = D;
  pclData = getItemDataByName(pcgItemList, "FLDA", ipLegNo);
  if (pclData == NULL) {
    dbg(TRACE, "<HandleMultiSectorUpd> Unable to perform sector update FLDA missing");
    return RC_FAIL;
  }
  else {
    sprintf(clFLDA, "%s", pclData);
    /* make FLDA invalid to prevent illegal update */
    putItemDataByName(pcgItemList, "FLDA", "", ipLegNo, IS_INVALID);
    /* used for flight identification, so write to internal data */
    putItemDataByName(pcgItemList, "FLDU", clFLDA, ipLegNo, IS_VALID);
  }
  pclData = getItemDataByName(pcgItemList, "FLNO", ipLegNo);
  if (pclData == NULL) {
    dbg(TRACE, "<HandleMultiSectorUpd> Unable to perform sector update FLNO missing");
    return RC_FAIL;
  }
  else {
    sprintf(clFLNO, "%s", pclData);
  }
  pclData = getItemDataByName(pcgItemList, "ORG3", ipLegNo);
  if (pclData == NULL) {
    dbg(TRACE, "<HandleMultiSectorUpd> Unable to perform sector update ORG3 missing");
    return RC_FAIL;
  }
  else {
    sprintf(clOrg3, "%s", pclData);
  }
  pclData = getItemDataByName(pcgItemList, "DES3", ipLegNo);
  if (pclData == NULL) {
    dbg(TRACE, "<HandleMultiSectorUpd> Unable to perform sector update DES3 missing");
    return RC_FAIL;
  }
  else {
    sprintf(clDes3, "%s", pclData);
  }
  /*
  ** Check ORG3/DES3 and disable update to via
  */
  if (pcgItemList->RouteType[ipLegNo] == A) {
    putItemDataByName(pcgItemList,"ORG3","",ipLegNo,IS_INVALID);
  }
  else if (pcgItemList->RouteType[ipLegNo] == D) {
    putItemDataByName(pcgItemList,"DES3","",ipLegNo,IS_INVALID);
  }
  if ((pcgItemList->RouteType[ipLegNo] == A && strcmp(clDes3,cgHopo)) ||
      (pcgItemList->RouteType[ipLegNo] == D && strcmp(clOrg3,cgHopo))||
      (pcgItemList->RouteType[ipLegNo] != D && pcgItemList->RouteType[ipLegNo] != A)) {
	 sprintf(clMessage, "Unable to update CSKED data FLNO: <%s> ADID: <%s> ORG3: <%s> DES3: <%s>",
	    clFLNO,pclADID,clOrg3,clDes3);
    alert(clMessage);
    return RC_FAIL;
  }
  ilRC = HandleSingleRecord(NULL,ipLegNo);
  /*  ilRC = identifyFlightRecord(NULL,ipLegNo);*/
  if (ilRC == RC_SUCCESS) {
    return ilRC; /*HandleInsOrUpd(ipLegNo);*/
  }
  else {
    dbg(TRACE, "<HandleMultiSectorUpd>Unable to update non existing flight FLNO: <%s> FLDU: <%s>",clFLNO,clFLDA);
    return ilRC;
  }
}

/******************************************************************************/
/* HandleMultiSectorData                                                      */
/*                                                                            */
/* This function will construct an 'admin record', containing the resulting   */
/* flight data.                                                               */
/*                                                                            */
/* Input:                                                                     */
/*        the global item list                                                */
/*                                                                            */
/* Return:                                                                    */
/*         RC_FAIL/RC_SUCCESS                                                 */
/*                                                                            */
/******************************************************************************/
static int HandleMultiSectorData ()
{
  int  ilRC;
  uint ilNumEntry;
  uint i;
  uint ilGenRouteType;
  uint ilFirstSector=0;
  uint ilLastSector=0;
  uint ilNum;
  uint ilStatus;
  uint entry = 0;
  uint ilFirst = FALSE;
  char *pclData;
  char *pclFKey;
  char clFKey[32];
  char clADID[2];
  char clSto[5];
  char clSTODat[15];
  char clLFLDA[15];
  char clFLDU[15];
  char clRoute[2048];

  for (ilNumEntry = 1; ilNumEntry<igNumOfLegs+1; ilNumEntry++) {
    ilGenRouteType = pcgItemList->RouteType[ilNumEntry];
    if (ilGenRouteType == A || ilGenRouteType == D) break;
  }
  if (ilGenRouteType == A) {
    strcpy(clADID, "A");
    strcpy(clSto, "STOA");
  }
  else if (ilGenRouteType == D) {
    strcpy(clADID, "D");
    strcpy(clSto, "STOD");
  }
  else if (!strcmp(pcgActCmd, "IFR")) { /* Via or similar */
    dbg(TRACE, "<HandleMultiSectorData> Fatal -- this record is not connected to <%s>", cgHopo);
    return RC_FAIL;
  }

  /*
  ** update the list of legs
  ** at this point we have:
  ** ilGenRouteType -- the type of record (A/D)
  ** ilNumEntry     -- the entry, which determines the type
  ** from this we will try to create an FKEY
  */
  switch (ilGenRouteType) {
  case A: 
    /* fetch time from last record */
    pclData = getItemDataByName(pcgItemList, "STOA", ilNumEntry);
    if (pclData != NULL) {
      sprintf(clSTODat,"%s",pclData);
    }
    else {
      dbg(TRACE, "<HandleMultiSectorData> Fatal -- no time info");
      return RC_FAIL;
    }
    /* FLDU is FLDA from CSKED */
    pclData = getItemDataByName(pcgItemList, "FLDA", 1);
    if (pclData != NULL) {
      sprintf(clFLDU, "%s",pclData);
    }
    else {
      dbg(TRACE, "<HandleMultiSectorData> Fatal -- no FLDA info");
      return RC_FAIL;
    }
    break;
  case D:
    /* fetch time from first record */
    pclData = getItemDataByName(pcgItemList, "STOD", 1);
    if (pclData != NULL) {
      sprintf(clSTODat,"%s",pclData);
    }
    else {
      dbg(TRACE, "<HandleMultiSectorData> Fatal -- no time info");
      return RC_FAIL;
    }
    /* FLDU is FLDA from CSKED */
    pclData = getItemDataByName(pcgItemList, "FLDA", 1);
    if (pclData != NULL) {
      sprintf(clFLDU, "%s",pclData);
    }
    else {
      dbg(TRACE, "<HandleMultiSectorData> Fatal -- no FLDA info");
      return RC_FAIL;
    }
    break;
  default:
    break;
  }

  UtcToLocalTimeFixTZ(clSTODat);
  sprintf(clLFLDA, "%.8s", clSTODat);

  putItemDataByName(pcgItemList, "LFLDA", clLFLDA, 0, IS_VALID);

  /*
  ** At this point we have an FKEY to search AODB
  ** if this is an insert, 
  /*
  ** compute first/last leg
  */
  for (i=1; i<igNumOfLegs+1; i++) {
    pclData = getItemDataByName(pcgItemList, "SECN", i);
    if (pclData != NULL) {
      if (!strcmp(pclData, "1")) {
	ilFirstSector = i;
      }
      else {
	ilNum = atoi(pclData);
	if (ilNum == igNumOfLegs) {
	  ilLastSector = i;
	}
      }
    }
  }
  if (ilLastSector == 0) ilLastSector = 1; /* special case for one leg request */
  
  if (!strcmp(clADID, "A")) {
    /*
    ** Arrival: take 
    **           -- origin from first sector
    **           -- destination from hopo
    **  from last sector:
    **           -- STOA
    **           -- FLNO
    **           -- REGN
    **           
    */
    pcgItemList->RouteType[0] = A;
    /*
    ** put all required fields into
    ** admin records
    */
    while (cgFSLAdminFields[entry].FieldName != NULL) {
      pclData = getItemDataByName(pcgItemList, cgFSLAdminFields[entry].FieldName, ilLastSector);
      if (cgFSLAdminFields[entry].isUTC == TRUE) {
	ilStatus = IS_VALID|IS_AFT|IS_UTCTIME;
	if (pclData != NULL && igTimeFromIf == LOCAL)  LocalTimeToUtcFixTZ(pclData);
      }
      else ilStatus = IS_VALID|IS_AFT;
      if (pclData != NULL) putItemDataByName(pcgItemList, cgFSLAdminFields[entry].FieldName, pclData, 0, IS_VALID|IS_AFT);
      entry++;
    }
    /*
    ** some special handling
    */
    putItemDataByName(pcgItemList, "DES3", cgHopo, 0, IS_VALID|IS_AFT);
    pclData = getItemDataByName(pcgItemList, "ORG3", ilFirstSector);
    if (pclData != NULL) putItemDataByName(pcgItemList, "ORG3", pclData, 0, IS_VALID|IS_AFT);
    pclData = getItemDataByName(pcgItemList, "STOA", ilLastSector);
    if (pclData != NULL) {
      if (igTimeFromIf == LOCAL) {
	LocalTimeToUtcFixTZ(pclData);
      }
      putItemDataByName(pcgItemList, "STOA", pclData, 0, IS_VALID|IS_AFT|IS_UTCTIME);
    }
    pclData = getItemDataByName(pcgItemList, "STOD", ilFirstSector);
    if (pclData != NULL) {
      if (igTimeFromIf == LOCAL) {
	LocalTimeToUtcFixTZ(pclData);
      }
      putItemDataByName(pcgItemList, "STOD", pclData, 0, IS_VALID|IS_AFT|IS_UTCTIME);
    }    
  }
  else if (!strcmp(clADID, "D")) {
    /*
    ** Departure: take 
    **           -- origin from hopo
    **           -- destination from last sector
    **           -- STOD from first sector
    **           -- FLNO from first sector
    */
    pcgItemList->RouteType[0] = D;
    /*
    ** put all required fields into
    ** admin records
    */
    while (cgFSLAdminFields[entry].FieldName != NULL) {
      pclData = getItemDataByName(pcgItemList, cgFSLAdminFields[entry].FieldName, ilFirstSector);
      if (cgFSLAdminFields[entry].isUTC == TRUE) {
	ilStatus = IS_VALID|IS_AFT|IS_UTCTIME;
	if (pclData != NULL && igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(pclData);
      }
      else ilStatus = IS_VALID|IS_AFT;
      if (pclData != NULL) putItemDataByName(pcgItemList, cgFSLAdminFields[entry].FieldName, pclData, 0, IS_VALID|IS_AFT);
      entry++;
    }
    /*
    ** some special handling
    */
    putItemDataByName(pcgItemList, "ORG3", cgHopo, 0, IS_VALID|IS_AFT);
    pclData = getItemDataByName(pcgItemList, "DES3", ilLastSector);
    if (pclData != NULL) putItemDataByName(pcgItemList, "DES3", pclData, 0, IS_VALID|IS_AFT);
    pclData = getItemDataByName(pcgItemList, "STOD", ilFirstSector);
    if (pclData != NULL) {
      if (igTimeFromIf == LOCAL) {
	LocalTimeToUtcFixTZ(pclData);
      }
      putItemDataByName(pcgItemList, "STOD", pclData, 0, IS_VALID|IS_AFT|IS_UTCTIME);
    }

    pclData = getItemDataByName(pcgItemList, "STOA", ilLastSector);
    if (pclData != NULL) {
      if (igTimeFromIf == LOCAL) {
	LocalTimeToUtcFixTZ(pclData);
      }
      putItemDataByName(pcgItemList, "STOA", pclData, 0, IS_VALID|IS_AFT|IS_UTCTIME);
    }
  }
  /*
  ** fill in ADID and LFKEY
  */
  pclFKey = getItemDataByName(pcgItemList, "LFKEY", ilNumEntry);
  if (pclFKey != NULL) strcpy(clFKey, pclFKey);
  else strcpy(clFKey, " ");
  putItemDataByName(pcgItemList, "FLDU", clFLDU, 0, IS_VALID);
  putItemDataByName(pcgItemList, "FLDA", clLFLDA, 0, IS_VALID);
  pclData = getItemDataByName(pcgItemList, "FTYP", ilNumEntry);
  if (pclData != NULL) putItemDataByName(pcgItemList, "FTYP", pclData, 0, IS_VALID);
  for (ilNumEntry = 0; ilNumEntry<=igNumOfLegs; ilNumEntry++) {
    pclData = getItemDataByName(pcgItemList, "ADID", ilNumEntry);
    putItemDataByName(pcgItemList, "ADID", clADID, ilNumEntry, IS_VALID);
    if (ilNumEntry>0) {/* only for SECTOR data */
      putItemDataByName(pcgItemList, "LFKEY", clFKey, ilNumEntry, IS_VALID);
      putItemDataByName(pcgItemList, "FLDU", clFLDU, ilNumEntry, IS_VALID);
      putItemDataByName(pcgItemList, "FLDA", clLFLDA, ilNumEntry, IS_VALID);
    }
  }
  /*
  ** write NUMS to admin record 
  */
  pclData = getItemDataByName(pcgItemList, "NUMS", ilFirstSector);
  if (pclData != NULL) putItemDataByName(pcgItemList, "NUMS", pclData, 0, IS_VALID); 
  createVialString(igNumOfLegs, ilLastSector, ilGenRouteType, pcgItemList, clRoute);
  /* If clRoute == "" -> no ROUT needs to be send */
  if (strcmp(clRoute, "")) putItemDataByName(pcgItemList, "ROUT", clRoute, 0, IS_VALID|IS_AFT);
  return HandleSingleRecord(NULL, 0);
}
/******************************************************************************/
/* createVialString                                                           */
/*                                                                            */
/* Create a string for the ROUT/VSUP handling.                                */
/*                                                                            */
/* Input:                                                                     */
/*       ipNumEntry     -- number of sectors                                  */
/*       ipLastSector   -- number of last sector                              */
/*       ipGenRouteType -- type of routing                                    */
/*       pcpItemList    -- item list of data objects                          */
/*                                                                            */
/* Return:                                                                    */
/*       pcpRoute       -- filled VIAL string buffer                          */
/*                                                                            */
/******************************************************************************/
int createVialString(uint ipNumEntry, uint ipLastSector, uint ipGenRouteType, ListPtr pcpItemList, char *pcpRoute)
{
  uint ilFirst = TRUE;
  uint ilNumEntry;
  char *pclData;
  /*
  ** create ROUTE field (which is associated to AFT's VIAL)
  ** Arrival: DES3(1);STOD(1);STOA(2)|DES3(2);STOD(2);STOA(3)|...|DES3(last-1);STOD(last-1);STOA(n)
  ** Departure: DES3(1);STOA(1);STOD(2)|DES3(2);STOA(2);STOD(3)|...|DES3(last-1);STOA(last-1);STOD(n)
  */
  sprintf(pcpRoute, "[APC3;STOA;STOD]");
  ilFirst = TRUE;
  for (ilNumEntry = 1; ilNumEntry<ipLastSector; ilNumEntry++) {
    if (ilFirst == FALSE) strcat(pcpRoute, ";|");
    pclData = getItemDataByName(pcgItemList, "DES3", ilNumEntry);
    if (pclData != NULL) strcat(pcpRoute, pclData);
    strcat(pcpRoute, ";");
    if (ipGenRouteType == A) {
      pclData = getItemDataByName(pcgItemList, "STOA", ilNumEntry);
      if (pclData != NULL) strcat(pcpRoute, pclData);
      strcat(pcpRoute, ";");
      pclData = getItemDataByName(pcgItemList, "STOD", ilNumEntry+1);
      if (pclData != NULL) strcat(pcpRoute, pclData);
    }
    else if (ipGenRouteType == D) {
      pclData = getItemDataByName(pcgItemList, "STOA", ilNumEntry);
      if (pclData != NULL) strcat(pcpRoute, pclData);
      strcat(pcpRoute, ";");
      pclData = getItemDataByName(pcgItemList, "STOD", ilNumEntry+1);
      if (pclData != NULL) strcat(pcpRoute, pclData);
    }
    ilFirst = FALSE;
  }
  if (!strcmp(pcpRoute, "[APC3;STOA;STOD]")) strcpy(pcpRoute, ""); /* empty ROUT shouldn't be send to flight */
}
/******************************************************************************/
/* HandleFlightResponseData                                                   */
/*                                                                            */
/* Handle responses to a requested IFR/UFR/DFR. UFR/IFR wil be marked by a    */
/* status '<OK>' or error '<OK>', DFR won't send a status, so we will asume   */
/* alway ok. If all responses of a request are received with success, this    */
/* request is acknowledged as 'OK' in MQRTAB. If one of the responses was     */
/* in error, this request is acknowledged as 'DATA ERR' in MQRTAB.            */
/* Update of FSLTAB data is done in HandleFlightRecord                        */ 
/*                                                                            */
/******************************************************************************/
static int HandleFlightResponseData(BC_HEAD *prlBchead,
			    CMDBLK *prlCmdblk,
			    char *pclSelection,
			    char *pclFields,
			    char *pclData)
{
  ListPtr pclLis;
  char clCommand[16];
  char *clURNO;
  char *pclDataTemp;
  uint ilRC = RC_SUCCESS;
  uint ilMQURNO = 0;
  uint ilErrResponse = FALSE;
  uint ilAckMQR = FALSE;
  uint ilIsDelete = FALSE;
  uint ilNumOfLines = 0;
  uint ilHaveAF1Data = FALSE;
  uint ilIsAF1Upd = FALSE;
  uint ilSndCount = FALSE;
  uint ilRcvCount = FALSE;
  uint ilLastElSent = FALSE;
  long llURNO = 0;
  char pclTwStart[64];
  char clStat[32];
  char clRqId[64];

  /* SaveGlobal Info of CMDBLK */
  strcpy (pclTwStart, pcgTwStart); 
  strcpy (pcgTwEnd, prlCmdblk->tw_end); 
  strcpy (clCommand, prlCmdblk->command);

  if (!strcmp(clCommand, "DFR")) {
    strcpy(pclSelection, "OK"); /* fake OK reply (flight won't send one) */
    ilIsDelete = TRUE;
  }
  /*
  ** search for this request in open 
  ** request list.
  ** If request wasn't found, nothing else to do
  ** i.e. return RC_SUCCESS.
  ** If we find that request, we need to figure out,
  ** if this flight was handled successfully or not.
  ** In case of successfully handling, we will set MQRTAB's
  ** STAT to 'OK' and to 'DATA ERR' otherwise.
  */
  clURNO = strchr(pcgTwStart,',');
  if (clURNO == NULL) {
    dbg(TRACE,"<HandleFlightResponseData> Unable to acknowledge MQRTAB, URNO missing from response");
    return RC_FAIL;
  }
  clURNO++;


  ilRC = fetchRequest(clURNO,&ilHaveAF1Data,&ilIsAF1Upd,&ilSndCount,&ilRcvCount,&ilLastElSent);
  if (ilRC == RC_FAIL) {
    dbg(TRACE, "<HandleFlightResponseData> Invalid response request id <%s> cmd <%s> fields <%s> data <%s>", 
	pclTwStart,clCommand,pclFields,pclData);
    return RC_FAIL;
  }
  if (!strcmp(clCommand,"ERR")) ilErrResponse = TRUE;

  if (ilHaveAF1Data == TRUE) igHaveAF1Data = TRUE;
  else igHaveAF1Data = FALSE;

  ilRcvCount++;
  if (ilLastElSent == TRUE) {
    if (ilRcvCount == ilSndCount) {
      dbg(DEBUG, "<HandleFlightResponseData> all <%d> items of request <%s> on interface <%s> have completed", 
	  ilRcvCount , pclTwStart, Section[igIfType]);
      ilAckMQR = TRUE;
      remRequest(clURNO);
    }
    else {
      updRcvResp(clURNO,ilRcvCount);
      dbg(DEBUG, "<HandleFlightResponseData>Received <%d> of <%d> items of request <%s>", 
	  ilRcvCount, ilSndCount, pclTwStart);
    }
  }

  /*
  ** Only first line of response is of interest
  ** so we will replace '\n'  by '\0' using strtok
  */
  pclDataTemp = strtok(pclData, "\n");
  dbg(DEBUG, "<HandleFlightResponseData> Received response: <%s>", pclData);

  createItemList(pclFields, &pcgFlightResponse);
  addDataItemToList(pclData, &pcgFlightResponse, 0, ',', IS_VALID);
  pclData = getItemDataByName(pcgFlightResponse, "URNO", 0);
  if (pclData != NULL) {
    llURNO = atol(pclData);
    if (!ilIsDelete && !ilErrResponse && igHaveAF1Data) confirmAf1Insert(llURNO, ilIsAF1Upd);
    /*
    ** pending flight is always deleted after ack'ed
    ** (only exists for IFR/DFR)
    */
    sprintf(clStat," ");
    /*    if (strcmp(clCommand, "UFR")) {*/
    if (!strcmp(clCommand, "IFR")||!strcmp(clCommand, "UFR")) sprintf(clStat,"INSERTING");
    else if (!strcmp(clCommand, "DFR")) sprintf(clStat,"DELETING");
    sprintf(clRqId,"%s",pcgTwStart);
    ConvertClientStringToDb(clRqId);
    sprintf(pcgSqlBuf, "DELETE FROM IFTTAB WHERE UAFT=%ld AND RQID='%s'",llURNO,clRqId); 
    ilRC = getOrPutDBData(START|COMMIT);
    if (ilRC != RC_SUCCESS && ilRC != RC_NODATA) { 
      dbg(TRACE,"<HandleFlightResponseData> Unable to perform <%s>", pcgSqlBuf);
    }
    if (!strcmp(clCommand, "DFR")) { /* remove corresponding AF1TAB entry */
      sprintf(pcgSqlBuf, "DELETE FROM AF1TAB WHERE FLNU='%ld'",llURNO);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC != RC_SUCCESS) {
	dbg(TRACE,"<HandleFlightResponseData> Unable to perform <%s>", pcgSqlBuf);
      }
    }
  }
  else {
    dbg(TRACE, "<HandleFlightResponseData> Missing required URNO");
    ilRC = RC_FAIL;
  } /* completed request */

  deleteItemList(pcgFlightResponse);
  pcgFlightResponse = NULL;
  
  if (ilAckMQR == FALSE) return RC_SUCCESS; /* not yet completed */
    
  ilMQURNO = atoi(clURNO);

  if (ilMQURNO != 0) {
    dbg(DEBUG, "<HandleFlightResponseData> Ack'ing MQRTAB urno <%d>", ilMQURNO);
    if(ilErrResponse == FALSE) { 
//Frank@Test
		if(igRUN_MODE == 1)
			{
      	dbg(DEBUG,"<HandleFlightResponseData>: UPDATE MQRTAB SET STAT='OK' WHERE URNO=%d",ilMQURNO);
			}
//Frank@Test
      sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='OK' WHERE URNO=%d", ilMQURNO);
    }
    else {
//Frank@Test
		if(igRUN_MODE == 1)
    {  
    	dbg(DEBUG,"<HandleFlightResponseData> UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%d",ilMQURNO);
		}
//Frank@Test
      sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='DATA ERR' WHERE URNO=%d", ilMQURNO);
    }
    ilRC = getOrPutDBData(START|COMMIT);
    if (ilRC != RC_SUCCESS) {
      dbg(TRACE,"<HandleFlightResponseData> Unable to acknowledge MQR data URNO <%d>", ilMQURNO);
    }
  }
  return ilRC;
}
/******************************************************************************/
/* confirmAf1Insert                                                           */
/*                                                                            */
/* Set the status of this entry to 'OK' and notify ACTION about insertion     */ 
/*                                                                            */
/******************************************************************************/
static void confirmAf1Insert(long llURNO, uint ipUpdate)
{
  int ilRC;
  char clFieldList[1024];

  if (ipUpdate == FALSE) {
    sprintf(pcgSqlBuf, "UPDATE AF1TAB SET STAT='OK' WHERE FLNU='%ld'", llURNO);
    
    ilRC = getOrPutDBData(START|COMMIT);
    if (ilRC != RC_SUCCESS) {
      dbg(TRACE,"<confirmAf1Insert> Unable to perform <%s>", pcgSqlBuf);
    }
  }

  sprintf(clFieldList, "FCA1,FCA2,URNO,FLNU");
  sprintf(pcgSqlBuf, "SELECT %s FROM AF1TAB WHERE FLNU='%ld'", clFieldList, llURNO);
  ilRC = getOrPutDBData(START);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE,"<confirmAf1Insert> Unable to perform <%s>", pcgSqlBuf);
  }
  else {
    BuildItemBuffer(pcgDataArea, NULL, 4, ",");
    tools_send_info_flag(7400,0,"","","","","", pcgTwStart, pcgTwEnd, "IFR", "AF1TAB", "", clFieldList, pcgDataArea, 0);
  }
}

/******************************************************************************/
/* User Tools		                                                      */
/******************************************************************************/


/******************************************************************************/
/* insIFTEntry                                                                */
/*                                                                            */
/* Insert an FSLTAB record, check the existance of mandatory fields.          */
/*                                                                            */
/* Input:                                                                     */
/*        uses the global pointer pcgFlightResponse to the list of items      */
/*                                                                            */
/* Return:                                                                    */
/*        RC_SUCCESS/RC_FAIL (if mandatory fields are missing)                */
/*                                                                            */
/******************************************************************************/
int insIFTEntry(ListPtr pclFlightData, uint legLine, char *pcpStatus)
{
  char *pclData;
  char clFLDA[9];
  char clFLDU[9];
  char clADID[2];
  char clFTYP[2];
  char clFLNO[10];
  char clSTOA[15];
  char clSTOD[15];
  char clETOA[15];
  char clETOD[15];
  char clONBL[15];
  char clOFBL[15];
  char clAIRB[15];
  char clLAND[15];
  char clStatus[33];
  char clFieldList[2048];
  char clDataList[2048];
  char clUrno[URNO_SIZE];
  char clTimeStamp[15];
  char clActor[16];
  char clRqId[64];
  ulong llUAFT=0;
  ulong llUrno=0;
  int  ilRC;
  uint ilHaveStod = FALSE;
  uint ilHaveStoa = FALSE;
  uint ilHaveEtoa = FALSE;
  uint ilHaveEtod = FALSE;
  uint ilHaveOnbl = FALSE;
  uint ilHaveOfbl = FALSE;
  uint ilHaveAirb = FALSE;
  uint ilHaveLand = FALSE;

  memset (clFieldList, '\0', 2048) ;
  memset (clDataList, '\0', 2048) ;

  /*
  ** first check, if a record is already
  ** under construction search by 
  ** FLNO,FLDX,ADID
  */
  pclData = getItemDataByName(pclFlightData,"FLNO",legLine);
  if (pclData == NULL) {
    dbg(TRACE, "<insIFTEntry>Fatal -- missing FLNO");
    return RC_FAIL;
  }
  else sprintf(clFLNO,"%s",pclData);

  pclData = getItemDataByName(pclFlightData,"ADID",legLine);
  if (pclData == NULL) {
    dbg(TRACE, "<insIFTEntry>Fatal -- missing ADID");
    return RC_FAIL;
  }
  else sprintf(clADID,"%s",pclData);

  pclData = getItemDataByName(pclFlightData,"STOD",legLine);
  if (pclData != NULL) {
    sprintf(clSTOD,"%s",pclData);
    ilHaveStod = TRUE;
  }
  pclData = getItemDataByName(pclFlightData,"STOA",legLine);
  if (pclData != NULL) {
    sprintf(clSTOA,"%s",pclData);
    ilHaveStoa = TRUE;
  }
  pclData = getItemDataByName(pclFlightData,"ETOA",legLine);
  if (pclData != NULL) {
    sprintf(clETOA,"%s",pclData);
    ilHaveEtoa = TRUE;
  }
  pclData = getItemDataByName(pclFlightData,"ETOD",legLine);
  if (pclData != NULL) {
    sprintf(clETOD,"%s",pclData);
    ilHaveEtod = TRUE;
  }
  pclData = getItemDataByName(pclFlightData,"ONBL",legLine);
  if (pclData != NULL) {
    sprintf(clONBL,"%s",pclData);
    ilHaveOnbl = TRUE;
  }
  pclData = getItemDataByName(pclFlightData,"OFBL",legLine);
  if (pclData != NULL) {
    sprintf(clOFBL,"%s",pclData);
    ilHaveOfbl = TRUE;
  }
  pclData = getItemDataByName(pclFlightData,"AIRB",legLine);
  if (pclData != NULL) {
    sprintf(clAIRB,"%s",pclData);
    ilHaveAirb = TRUE;
  }
  pclData = getItemDataByName(pclFlightData,"LAND",legLine);
  if (pclData != NULL) {
    sprintf(clLAND,"%s",pclData);
    ilHaveLand = TRUE;
  }
    

  pclData = getItemDataByName(pclFlightData,"FTYP",legLine);
  if (pclData != NULL) sprintf(clFTYP,"%s",pclData);
  else sprintf(clFTYP," ");

  if (igIfType == CSK) {
    pclData = getItemDataByName(pclFlightData,"FLDU",legLine);
    if (pclData == NULL) {
      dbg(TRACE, "<insIFTEntry>Fatal -- FLDU missing");
      return RC_FAIL;
    }
    else sprintf(clFLDU,"%s",pclData);
    /*
    ** try to get FLDA
    */
    pclData = getItemDataByName(pclFlightData,"FLDA",legLine);
    if (pclData == NULL) {
      sprintf(clFLDA,"");
    }
    else sprintf(clFLDA,pclData);
  }
  else if (igIfType == ELSE) {
    pclData = getItemDataByName(pclFlightData,"FLDA",legLine);
    if (pclData == NULL) {
      dbg(TRACE, "<insIFTEntry>Fatal -- FLDA missing");
      return RC_FAIL;
    }
    else sprintf(clFLDA,"%s",pclData);
    /*
    ** try to get FLDU
    */
    pclData = getItemDataByName(pclFlightData,"FLDU",legLine);
    if (pclData == NULL) {
      sprintf(clFLDU,"");
    }
    else sprintf(clFLDU,pclData);
  }
  else {
    /* error */
    dbg(TRACE, "<insIFTEntry>Fatal -- unknown Interface");
    return RC_FAIL;
  }

  /* fetch the URNO now */
  pclData = getItemDataByName(pclFlightData, "URNO", legLine);
  if (pclData == NULL) {
    dbg(TRACE, "<insIFTEntry>Fatal -- missing URNO");
    return RC_FAIL;
  }
  else llUAFT = atol(pclData);
  ilRC = getFreeUrno(clUrno);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE, "<insIFTEntry>Fatal -- unable to get new URNO");
    return RC_FAIL;
  }
  llUrno = atol(clUrno);
  sprintf(clFieldList,"FLNO,FLDA,FLDU,ADID,DELF,UAFT,URNO,FTYP");
  sprintf(clDataList,"'%s','%s','%s','%s','0','%ld','%ld','%s'",clFLNO,clFLDA,clFLDU,clADID,llUAFT,llUrno,clFTYP,clSTOD,clSTOA);
  /*
  ** append optional fields if available 
  */
  if (ilHaveStod) {
    strcat(clFieldList, ",STOD");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clSTOD);
    strcat(clDataList, clSTOD);
    strcat(clDataList, "'");
  }
  if (ilHaveStoa) {
    strcat(clFieldList, ",STOA");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clSTOA);
    strcat(clDataList, clSTOA);
    strcat(clDataList, "'");
  }
  if (ilHaveEtoa) {
    strcat(clFieldList, ",ETOA");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clETOA);
    strcat(clDataList, clETOA);
    strcat(clDataList, "'");
  }
  if (ilHaveEtod) {
    strcat(clFieldList, ",ETOD");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clETOD);
    strcat(clDataList, clETOD);
    strcat(clDataList, "'");
  }
  if (ilHaveOnbl) {
    strcat(clFieldList, ",ONBL");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clONBL);
    strcat(clDataList, clONBL);
    strcat(clDataList, "'");
  }
  if (ilHaveOfbl) {
    strcat(clFieldList, ",OFBL");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clOFBL);
    strcat(clDataList, clOFBL);
    strcat(clDataList, "'");
  }
  if (ilHaveAirb) {
    strcat(clFieldList, ",AIRB");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clAIRB);
    strcat(clDataList, clAIRB);
    strcat(clDataList, "'");
  }
  if (ilHaveLand) {
    strcat(clFieldList, ",LAND");
    strcat(clDataList, ",'");
    if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clLAND);
    strcat(clDataList, clLAND);
    strcat(clDataList, "'");
  }

  strcat(clFieldList, ",RQID");
  strcat(clDataList, ",'");
  sprintf(clRqId,"%s",pcgTwStart);
  ConvertClientStringToDb(clRqId);
  strcat(clDataList, clRqId); 
  strcat(clDataList, "'");
  GetServerTimeStamp("UTC", 1, 0, clTimeStamp); 
  strcat(clFieldList, ",CDAT");
  strcat(clDataList, ",'");
  strcat(clDataList, clTimeStamp); 
  strcat(clDataList, "'");
  strcat(clFieldList, ",USEC");
  if (igIfType == CSK) {
    strcat(clDataList, ",'csked'");
    sprintf(clActor,"csked");
  }
  else if (igIfType == ELSE) {
    strcat(clDataList, ",'else'");
    sprintf(clActor,"else");
  }
  else {
    strcat(clDataList, ",'impman'");
    sprintf(clActor,"impman");
  }
  strcat(clFieldList, ",STAT");
  sprintf(clStatus,",'%s'",pcpStatus);
  strcat(clDataList, clStatus);
  if (!strcmp(pcpStatus,"DELETING")) {
    /* if we already have an existing entry -> update STAT */
    sprintf (pcgSqlBuf, "SELECT UAFT FROM IFTTAB WHERE UAFT='%ld'",llUAFT);
    ilRC = getOrPutDBData (START);
    if (ilRC == RC_SUCCESS) {
      
      sprintf(clRqId,"%s",pcgTwStart);
      ConvertClientStringToDb(clRqId);
      sprintf (pcgSqlBuf, "UPDATE IFTTAB SET STAT='%s',LSTU='%s',USEU='%s',RQID='%s' WHERE UAFT='%ld'"
	       ,pcpStatus,clTimeStamp,clActor,clRqId,llUAFT);
      ilRC = getOrPutDBData (START|COMMIT);
      if (ilRC != RC_NODATA) {
	dbg(TRACE,"<insIFTEntry> SQL didn't succeed <%s>", pcgSqlBuf);
	return RC_FAIL;
      }
      else return ilRC;
    }
    else {
      if (ilRC != RC_SUCCESS) dbg(TRACE,"<insIFTEntry> SQL didn't succeed <%s>", pcgSqlBuf);
      return RC_FAIL;
    }
  }
  sprintf (pcgSqlBuf, "INSERT INTO IFTTAB (%s) VALUES(%s)", clFieldList, clDataList);
  ilRC = getOrPutDBData (START|COMMIT);
  if (ilRC != RC_SUCCESS) dbg(TRACE,"<insIFTEntry> SQL didn't succeed <%s>", pcgSqlBuf);
  return ilRC;
}
/******************************************************************************/
/* isUTCTimeField                                                             */
/*                                                                            */
/* Check if a field is in a list of UTC time  fields. Those fields are        */
/* automatically converted to UTC time.                                       */
/*                                                                            */
/* Input:                                                                     */
/*       pcField -- points to the name field to check against the list        */
/*                                                                            */
/* Return:                                                                    */
/*        TRUE if field is contained in the list, FALSE else                  */
/*                                                                            */
/******************************************************************************/
static int isUTCTimeField(char *pcField)
{
  uint ilCount=0;

  while (cgUTCTIMEField[ilCount].FieldName != NULL) {
      if (!strcmp(pcField, cgUTCTIMEField[ilCount].FieldName)) return TRUE;
    ilCount++;
  }
  return FALSE;
}
/******************************************************************************/
/* isAFTField                                                                 */
/*                                                                            */
/* Check if a field is in a list of AFT field. Those fields are automatically */
/* added to a Field/Data list by createAFTSelection.                          */
/*                                                                            */
/* Input:                                                                     */
/*       pcField -- points to the name field to check against the list        */
/*                                                                            */
/* Return:                                                                    */
/*        TRUE if field is contained in the list, FALSE else                  */
/*                                                                            */
/******************************************************************************/
static int isAFTField(char *pcField)
{
  uint ilCount=0;

  while (cgAFTField[ilCount] != NULL) {
    if (!strcmp(pcField, cgAFTField[ilCount])) return TRUE;
    ilCount++;
  }
  ilCount=0;
  while (cgAFTPseudoField[ilCount] != NULL) {
    if (!strcmp(pcField, cgAFTPseudoField[ilCount])) return TRUE;
    ilCount++;
  }
  dbg(DEBUG,"<isAFTField> Info: field <%s> will not be send to AFTTAB", pcField);
  return FALSE;
}
/******************************************************************************/
/* isAF1Field                                                                 */
/*                                                                            */
/* Check if a field is in a list of AF1 field. Those fields are automatically */
/* added to AF1TAB.                                                           */
/*                                                                            */
/* Input:                                                                     */
/*       pcField -- points to the name field to check against the list        */
/*                                                                            */
/* Return:                                                                    */
/*        TRUE if field is contained in the list, FALSE else                  */
/*                                                                            */
/******************************************************************************/
static int isAF1Field(char *pcField)
{
  uint ilCount=0;

  while (cgAF1Field[ilCount] != NULL) {
    if (!strcmp(pcField, cgAF1Field[ilCount])) return TRUE;
    ilCount++;
  }
  return FALSE;
}
/******************************************************************************/
/* isToDeleteField                                                            */
/*                                                                            */
/* Check if the field contains a single ' ', which means this field is to be  */
/* deleted.                                                                   */
/*                                                                            */
/* Input:                                                                     */
/*       pcField -- pointer to field name                                     */
/*       legLine -- data entry                                                */
/*                                                                            */
/* Return:                                                                    */
/*       TRUE  -- field is 'to delete' field                                  */
/*       FALSE else                                                           */
/*                                                                            */
/******************************************************************************/
static int isToDeleteField(char *pcField, uint legLine)
{
  ListPtr clElement;
  int ilRC;

  ilRC = getItemByName(pcgItemList, pcField, &clElement);
  if (ilRC == RC_SUCCESS) {
    if (!strcmp(clElement->dobj[legLine].data, " ")) {
      dbg(DEBUG,"<isToDeleteField> Field <%s> is a 'to delete' field", pcField); 
      return TRUE;
    }
  }
  return FALSE;
}
/******************************************************************************/
/* HandleDelete                                                               */
/*                                                                            */
/* Checks if the required field data is available. Tries to fetch the related */
/* URNO from FSLTAB. Removes all leg entries for that URNO from FSLTAB and    */
/* removes the flight record from AFTTAB.                                     */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- data entry                                                */
/*                                                                            */
/* Return:                                                                    */
/*       RC_FAIL/RC_SUCCESS                                                   */
/*                                                                            */
/******************************************************************************/
int HandleDelete(uint legLine) 
{
  char clFLDA[9];
  char clADID[2];
  char clFLNO[16];
  char clTime[15];
  char clFLDU[9];
  char clUrno[16];
  char clUSEC[33];
  char clCDAT[15];
  char clLSTU[15];
  static char  pclTwStart[64] ;
  char *clSTOA;
  char *clSTOD;
  char *pclData;
  uint ilReceiver = igFlightId;
  uint ilHaveFLDA = FALSE;
  uint ilHaveSTOA = FALSE;
  uint ilHaveSTOD = FALSE;
  uint ilHaveADID = FALSE;
  uint ilPrio;
  uint ilADID;
  uint i;
  uint ilLegLine=0;
  char clRqId[64];

  int ilRC = RC_SUCCESS;
  
  //Frank20120523 v1.42a
  memset(clFLDU,0,sizeof(clFLDU));
  //

  if (igAllowDeletes[igIfType] == YES) {
    strcpy(pcgActCmd,"DFR");
  }
  else {
    dbg(TRACE, "<HandleDelete> Deletes are not allowed -- skipping");
    return RC_FAIL;
  }

  /*
  ** At this point we have found an URNO
  ** to remove all legs and the related AFTTAB entry
  ** If we have mixed updates, we have to create an 
  ** IFTTAB entry with status 'ONDELETE' and setup a timer 
  ** for expiring.
  */
  if (igMixedUpdates[igIfType] == TRUE && igHavePeriodData == FALSE) {
    ilLegLine=legLine;
    sprintf(clRqId,"%s",pcgTwStart);
    ConvertClientStringToDb(clRqId);
    GetServerTimeStamp("UTC", 1, 0, clLSTU);
    UtcToLocalTimeFixTZ(clLSTU);
    strcpy(clCDAT,clLSTU);
    sprintf(pcgSqlBuf,"UPDATE IFTTAB SET STAT='ONDELETE',RQID='%s',LSTU='%s' WHERE UAFT='%ld'",clRqId,clLSTU,lgUrno);
    ilRC = getOrPutDBData(START|COMMIT);
    if (ilRC != RC_SUCCESS) {
      /*
      ** insert a temporary flight into FSLTAB
      ** FLNO,FLDU,STAT,DELF,UAFT
      */
      pclData = getItemDataByName(pcgItemList, "FLNO", ilLegLine);
      if (pclData != NULL) sprintf(clFLNO,"%s",pclData);
      else {
	dbg(TRACE, "<HandleDelete> Unable to create temporary flight -- missing FLNO");
	return RC_FAIL;
      }
      pclData = getItemDataByName(pcgItemList, "ADID", ilLegLine);
      if (pclData != NULL) sprintf(clADID,"%s",pclData);
      else {
	dbg(TRACE, "<HandleDelete> Unable to create temporary flight -- missing ADID");
	return RC_FAIL;
      }
      pclData = getItemDataByName(pcgItemList, "FLDA", ilLegLine);
//Frank20120523 v1.42a
      if (pclData != NULL) sprintf(clFLDU,"%s",pclData);//Frank20120523 v1.42a
      //if (pclData != NULL) strncpy(clFLDU,pclData,8);
      else {
	dbg(TRACE, "<HandleDelete> Unable to create temporary flight -- missing FLDU");
	return RC_FAIL;
      }
      if (igIfType==CSK) {
	if (!strcmp(clADID,"A")) pclData = getItemDataByName(pcgItemList, "STOA", ilLegLine);
	else if (!strcmp(clADID,"D")) pclData = getItemDataByName(pcgItemList, "STOD", ilLegLine);
	if (pclData != NULL) {
	  strcpy(clTime,pclData);
	  UtcToLocalTimeFixTZ(clTime);
	  sprintf(clFLDA,"%8.8s",clTime);
	  strcpy(clUSEC,"csked");
	}
	else {
	  dbg(TRACE, "<HandleDelete> Unable to create temporary flight -- missing FLDA");
	  return RC_FAIL;
	}
      }
      else if (igIfType==ELSE) {
	strcpy(clUSEC,"else");
	sprintf(clFLDA,"%s",clFLDU);
      }
      else strcpy(clUSEC,"unknown");
      getFreeUrno(clUrno);

      sprintf(clRqId,"%s",pcgTwStart);
      ConvertClientStringToDb(clRqId);
      sprintf (pcgSqlBuf,"INSERT INTO IFTTAB (STAT,DELF,FLNO,UAFT,ADID,FLDU,FLDA,CDAT,RQID,URNO,USEC) VALUES('ONDELETE','%d','%s','%ld','%s','%s','%s','%s','%s','%s','%s')",
	       igDelFram[igIfType],clFLNO,lgUrno,clADID,clFLDU,clFLDA,clCDAT,clRqId,clUrno,clUSEC);
      
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC != RC_SUCCESS) {
	dbg(TRACE, "<HandleDelete> Unable to perform <%s>",pcgSqlBuf);
	return RC_FAIL;
      }
    }
    updRequest(pcgTwStart,FALSE);
    igHavePendingDeletes++;
    dbg(DEBUG, "<HandleDelete> Performing delayed delete request <%s>, count of pending flights <%d>", pcgTwStart, igHavePendingDeletes);
  }

  if (igMixedUpdates[igIfType] == FALSE || igHavePeriodData == TRUE) {
    pcgAftFields[0] = '\0';
    pcgAftValues[0] = '\0';
    memset (pcgSelection, '\0', 1024) ;
    strcpy(pcgActCmd,"DFR");
    sprintf(pcgSelection, "WHERE URNO=%ld", lgUrno);
    if (igPrioFromIf != 0) {
      ilPrio = igPrioFromIf;
    }
    else ilPrio = igPrio[igIfType];
    insIFTEntry(pcgItemList,legLine,"DELETING");

    if (igNBC == TRUE) { /* supress broadcasts */
      sprintf(pclTwStart, ".NBC.,");
      strcat(pclTwStart, pcgTwStart);
    }
    else strcpy(pclTwStart, pcgTwStart);

	CheckEstimatedTime(pcgAftFields, pcgAftValues, pcgSelection);
    ilRC = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
			 pcgActCmd, "AFTTAB", pcgSelection,
			 pcgAftFields, pcgAftValues, "", ilPrio, NETOUT_NO_ACK) ;
    if (ilRC != RC_SUCCESS && ilRC != QUE_E_INACTIVE) {
      /* remove pending delete request if send failed */
      sprintf(pcgSqlBuf,"DELETE FROM IFTTAB WHERE UAFT='%ld'",lgUrno);
      ilRC = getOrPutDBData(START|COMMIT);
      dbg(TRACE,"<HandleDelete> Unable to send ceda event <%d>", ilRC);
      return RC_FAIL;
    }
    updRequest(pcgTwStart,FALSE);
  }
  return RC_SUCCESS;
}
/******************************************************************************/
/* Some functions used to maintain the request statistics in table RQETAB     */
/*                                                                            */
/* updRequest   -- update or insert request entry                             */
/* remRequest   -- removes an request entry                                   */
/* updRcvResp   -- increments count of received responses                     */
/* fetchRequest -- fetch all available data                                   */
/* fixRequest   -- flag the request has completed all work                    */
/*                                                                            */
/******************************************************************************/
int remRequest(char *pclIdent)
{
  int ilRC;

  sprintf(pcgSqlBuf,"DELETE FROM RQETAB WHERE UMQR='%s'",pclIdent);
  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC != RC_SUCCESS) dbg(TRACE,"<remRequest> Unable to remove request <%s>",pclIdent);
}
int updRcvResp(char *pclIdent, uint ipRcvCount)
{
  int ilRC;
  char clTime[15];

  GetServerTimeStamp("UTC", 1, 0, clTime);
  UtcToLocalTimeFixTZ(clTime);

  sprintf(pcgSqlBuf,"UPDATE RQETAB SET RCVC='%d',LSTU='%s' WHERE UMQR='%s'",ipRcvCount,clTime,pclIdent);
  ilRC = getOrPutDBData(START|COMMIT);

}
int fetchRequest(char *pclIdent,uint *ipHaveAF1Data,uint *ipIsAF1Upd,uint *ipSndCount,uint *ipRcvCount,uint *ipLastElSent)
{
  int ilRC;
  char *pclData;

  sprintf(pcgSqlBuf,"SELECT SNDC,RCVC,ALDS,AF1D,AF1U FROM RQETAB WHERE UMQR='%s'",pclIdent);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_NODATA) {
    dbg(TRACE,"<fetchRequest> Fatal no request found for id <%s>", pclIdent);
    return RC_FAIL;
  }
  BuildItemBuffer(pcgDataArea, NULL, 5, ",");
  pclData = strtok(pcgDataArea, ",");
  if (pclData != NULL) {
    *ipSndCount = atoi(pclData);
  }
  pclData = strtok(NULL, ",");
  if (pclData != NULL) {
    *ipRcvCount = atoi(pclData);
  }
  pclData = strtok(NULL, ",");
  if (pclData != NULL) {
    if (!strcmp(pclData,"T")) *ipLastElSent = TRUE;
    else *ipLastElSent = FALSE;
  }
  pclData = strtok(NULL, ",");
  if (pclData != NULL) {
    if (!strcmp(pclData,"T")) *ipHaveAF1Data = TRUE;
    else *ipHaveAF1Data = FALSE;
  }
  pclData = strtok(NULL, ",");
  if (pclData != NULL) {
    if (!strcmp(pclData,"T")) *ipIsAF1Upd = TRUE;
    else *ipIsAF1Upd = FALSE;
  }
}

int fixRequest(char *pclIdent)
{
  int ilRC;

  char *clUrno;
  ulong llUrno;
  char clTime[15];

  GetServerTimeStamp("UTC", 1, 0, clTime);
  UtcToLocalTimeFixTZ(clTime);
 
  clUrno = strchr(pclIdent,',');
  if (clUrno == NULL) {
    dbg(TRACE,"<fixRequest> Unable to update request");
    return RC_FAIL;
  }
  clUrno++;
  llUrno = atol(clUrno);
  sprintf(pcgSqlBuf,"UPDATE RQETAB SET ALDS='T',LSTU='%s' WHERE UMQR='%ld'",clTime,llUrno);
  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE,"<fixRequest> Unable to finalize request <%s>",pcgSqlBuf);
  }
}
  
int updRequest(char *pclIdent,uint ipIsAf1Upd)
{
  int ilRC;
  char *clUrno;
  ulong llUrno;
  char clHaveAF1[2];
  char clIsAF1Upd[2];
  char clTime[15];
  char clNewUrno[URNO_SIZE];

  GetServerTimeStamp("UTC", 1, 0, clTime);
  UtcToLocalTimeFixTZ(clTime);
  clUrno = strchr(pclIdent,',');
  if (clUrno == NULL) {
    dbg(TRACE,"<updRequest> Unable to update request");
    return RC_FAIL;
  }
  clUrno++;
  llUrno = atol(clUrno);
  sprintf(pcgSqlBuf,"SELECT UMQR FROM RQETAB WHERE UMQR='%ld'",llUrno);
  ilRC = getOrPutDBData(START);
  if (igHaveAF1Data) sprintf(clHaveAF1,"T");
  else sprintf(clHaveAF1,"F");
  if (ipIsAf1Upd) sprintf(clIsAF1Upd,"T");
  else sprintf(clIsAF1Upd,"F");
  if (ilRC == RC_NODATA) {
    ilRC = getFreeUrno(clNewUrno);
    sprintf(pcgSqlBuf,"INSERT INTO RQETAB (UMQR,SNDC,AF1D,AF1U,ALDS,CDAT,URNO) VALUES('%ld','1','%s','%s','F','%s','%s')",llUrno,clHaveAF1,clIsAF1Upd,clTime,clNewUrno);
  }
  else {
    sprintf(pcgSqlBuf,"UPDATE RQETAB SET SNDC=SNDC+1,AF1D='%s',AF1U='%s',LSTU='%s' WHERE UMQR='%ld'",clHaveAF1,clIsAF1Upd,clTime,llUrno);
  }
  ilRC = getOrPutDBData(START|COMMIT);
}
/******************************************************************************/
/* sendJoinFlight                                                             */
/*                                                                            */
/* Join 2 flights using flights 'JOF' command.                                */
/*                                                                            */
/* Input:                                                                     */
/*       lpLeftUrno -- Urno of arrival                                        */ 
/*       lpRightUrno -- Urno of departure                                     */ 
/*                                                                            */
/* Return:                                                                    */
/*                                                                            */
/******************************************************************************/
static void sendJoinFlight(long lpLeftUrno, long lpRightUrno)
{
  int ilRC;
  int ilPrio;
  MessageStateTypePtr pclRequestData;

  if (igPrioFromIf != 0) {
    ilPrio = igPrioFromIf;
  }
  else ilPrio = igPrio[igIfType];

  sprintf(pcgSelection, "%ld,%ld,%ld", lpLeftUrno, lpLeftUrno, lpRightUrno);
  sprintf(pcgActCmd, "JOF");

  CheckEstimatedTime(pcgAftFields, pcgAftValues, pcgSelection);
  ilRC = SendCedaEvent(igFlightId, 0, pcgDestName, pcgRecvName, pcgTwStart, pcgTwEnd,
			 pcgActCmd, "AFTTAB", pcgSelection,
			 pcgAftFields, pcgAftValues, "", ilPrio, NETOUT_NO_ACK) ;
  if (ilRC != RC_SUCCESS && ilRC != QUE_E_INACTIVE) {
      dbg(TRACE,"<sendJoinFlight> Unable to send ceda event <%d>", ilRC);
  }
  /* no response expected!! */
  /* updRequest(pcgTwStart,FALSE);*/
  dbg(DEBUG,"<sendJoinFlight> Sent ID <%s>", pcgTwStart);
}
  
/******************************************************************************/
/* prepareData                                                                */
/*                                                                            */
/* Fetch data (identified by ipDataId) from interface. Determines the name    */
/* of the interfaces and calls the dedicated wrapper function. If the data    */
/* conversion was successful, the HandleInterfaceData function is called.     */
/*                                                                            */
/* Must reset igIsNonSched otherwise previous non-sched ELSE will prevent     */
/* identifyRecord for non-ELSE (e.g. CSKED).				      */
/* Input:                                                                     */
/*       ipDataId -- the unique data ID                                       */
/*                                                                            */
/* Return:                                                                    */
/*       RC_SUCCESS/RC_FAIL                                                   */
/*                                                                            */
/******************************************************************************/
static int prepareData(long ipDataId)
{
  int ilRC;
  char *pclData;
  char *pclIfNam;
  char *pclIfNamEnd;
  char *pclNewLine;
  char clDelim[2] = "\01";
  char clAlertMessage[128];
  uint ilElseMsg = FALSE;
  uint ilElseInsertDelayed = FALSE;
  uint ilElseMsgAdHoc = FALSE;

  igIsNonSched = 0;

  sprintf (pcgSqlBuf,"SELECT INAM, DATA FROM MQRTAB WHERE URNO=%ld", ipDataId);
  ilRC = getOrPutDBData (START);
  if (ilRC != RC_SUCCESS) {
    return ilRC;
  }
  BuildItemBuffer(pcgDataArea, NULL, 2, clDelim);
  pclIfNam = strtok(pcgDataArea, clDelim);
  if (pclIfNam == NULL) {
    dbg(TRACE, "<prepareData> Fatal -- unable to fetch interface name");
    return RC_FAIL;
  }
  else if (strlen(pclIfNam) == 0) {
    dbg(TRACE, "<prepareData> Fatal -- unable to fetch interface name");
    return RC_FAIL;
  }
  else {
    pclIfNamEnd = strstr(pclIfNam, " ");
    if (pclIfNamEnd != NULL) *pclIfNamEnd = '\0';
  }
  pclData = strtok(NULL, clDelim);
  if (pclData == NULL) {
    dbg(TRACE, "<prepareData> Fatal -- unable to fetch interface data");
    return RC_FAIL;
  }

  ConvertDbStringToClient(pclData);
  /* remove trailing \n */
  pclNewLine = strrchr(pclData, '\n');
  if (pclNewLine != NULL) {
    if (!strcmp(pclNewLine+1, "")) *pclNewLine = '\0'; /* only a terminating \n may be removed */
  }
  strcpy(pcgOrigData,pclData);
  dbg(TRACE,"pcgOrigData: <%s>",pcgOrigData);

  /*
  ** recover mode
  */
  if (!strcmp(pcgTwStart, "")) sprintf(pcgTwStart, "%s,%ld", pclIfNam, ipDataId);  
  sprintf(pcgIfName, "%s", pclIfNam);

  dbg(DEBUG,"<prepareData> Data received <%s>", pclData);
  if (!strncmp(pclIfNam,"elseah",6)) {
    if (igElseAdHoc == TRUE) {
      ilElseMsgAdHoc = TRUE;
      if (!strncmp(pclData, "RQEND", 5)) ilElseInsertDelayed = TRUE;
    }
  }
  else if (!strncmp(pclIfNam,"else",4)) ilElseMsg = TRUE; 
  if (igElseAdHoc == TRUE && ilElseMsg == TRUE) {
//Frank@Test
			if(igRUN_MODE == 1)
			{
      	dbg(DEBUG,"<prepareData>: UPDATE MQRTAB SET STAT='DELAYED' WHERE URNO=%ld",ipDataId);
			}
//Frank@Test
    sprintf (pcgSqlBuf,"UPDATE MQRTAB SET STAT='DELAYED' WHERE URNO=%ld", ipDataId);
    ilRC = getOrPutDBData (START|COMMIT);
    if (ilRC != RC_SUCCESS) {
      dbg(TRACE, "<prepareData> Unable to mark delayed ELSE record with URNO <%ld>", ipDataId);
      return RC_FAIL;
    }
    else return RC_DELAY;
  }
  else if (ilElseInsertDelayed == TRUE) {
      HandleIfRecover("DELAYED");
      ilElseInsertDelayed = FALSE;
      igElseAdHoc = FALSE;
  }
  else if (ilElseMsgAdHoc == TRUE || ilElseMsg == TRUE) {
    ilRC = wrapElseData(pclData, pcgActCmd, cgSelection, cgFields, cgData, igPrioToElse, igQueToElse);
	igIfType = ELSE;

    if (ilRC == RC_IGNORE) return ilRC;
    if (ilRC != RC_SUCCESS) {
	/*sprintf(clAlertMessage, "ELSE: %s Command: <%s> Fields: <%s> Data: <%s>", cgSelection, pcgActCmd, cgFields, cgData);
	  aler(clAlertMessage);*/
    }
	dbg(TRACE,"%05d: #################### result of wrapElseData:\n",__LINE__);
	dbg(TRACE,"%05d: pclCmd:        <%s>",__LINE__,pcgActCmd);
	dbg(TRACE,"%05d: pclSelOut:     <%s>",__LINE__,cgSelection);
	dbg(TRACE,"%05d: pclFldOut:     <%s>",__LINE__,cgFields);
	dbg(TRACE,"%05d: pclDatOut:     <%s>",__LINE__,cgData);
	dbg(TRACE,"%05d: ipPrioToElse:  <%d>",__LINE__,igPrioToElse);
	dbg(TRACE,"%05d: ipQueueToElse: <%d>\n                           ############################################",__LINE__,igQueToElse);
    return ilRC;
  }
  else if (!strncmp(pclIfNam,"csk",3)) {
    ilRC = wrapCskedData(pclData, pcgActCmd, cgSelection, cgFields, cgData);
	igIfType = CSK;
    if (ilRC == RC_IGNORE) return ilRC;
    if (ilRC != RC_SUCCESS) {
	/*sprintf(clAlertMessage, "CSKED: %s Command: <%s> Fields: <%s> Data: <%s>", cgSelection, pcgActCmd, cgFields, cgData);
	  alert(clAlertMessage);*/
    }
	dbg(TRACE,"%05d: #################### result of wrapCskedData:\n",__LINE__);
	dbg(TRACE,"%05d: pclCmd:        <%s>",__LINE__,pcgActCmd);
	dbg(TRACE,"%05d: pclSelOut:     <%s>",__LINE__,cgSelection);
	dbg(TRACE,"%05d: pclFldOut:     <%s>",__LINE__,cgFields);
	dbg(TRACE,"%05d: pclDatOut:     <%s>\n                           ############################################",__LINE__,cgData);

	return ilRC;
  }
  else {
    dbg(TRACE, "<prepareData> Interface <%s> not yet implemented", pclIfNam);
    return RC_FAIL;
  }
}
/******************************************************************************/
/* getOrPutDBData                                                             */
/*                                                                            */
/* Fetch data as described in the global pcgSqlBuf and return the result in   */
/* global buffer pcgDataArea.                                                 */
/*                                                                            */
/* Input:                                                                     */
/*        ipMode -- function mode mask like START|COMMIT                      */
/*                                                                            */
/******************************************************************************/
static int getOrPutDBData(uint ipMode)
{
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];
  /*
  dbg (TRACE, "<getOrPutDBData> calling sql_if pcgSqlBuf <%s>", pcgSqlBuf);
  dbg (TRACE, "<getOrPutDBData> calling sql_if pcgSqlBuf <%s>", pcgDataArea);
  */
  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return RC_NODATA;
  }
}

int remOnDelReq(ulong lpUrno)
{
  int ilRc;
  char *clRqId;

  sprintf(pcgSqlBuf,"SELECT RQID FROM IFTTAB WHERE UAFT='%ld'",lpUrno);
  ilRc = getOrPutDBData(START);
  if (ilRc != RC_SUCCESS) {
    dbg(TRACE,"<remOnDelReq> Unable to fetch request block");
    return RC_FAIL;
  }
  ConvertDbStringToClient(pcgDataArea);
  clRqId = strchr(pcgDataArea,',');
  if (clRqId == NULL) {
    dbg(TRACE,"<remOnDelReq> Unable to fetch request block -- no request ID");
    return RC_FAIL;
  }
  clRqId++;

  /*
  ** remove pending request block
  ** set MQRTAB status 'OK'
  ** remove IFTTAB entry
  */
  remRequest(clRqId);
  ConvertClientStringToDb(clRqId);
//Frank@Test
	if(igRUN_MODE == 1)
	{
  	dbg(DEBUG,"<remOnDelReq>: UPDATE MQRTAB SET STAT='OK' WHERE URNO='%s'",clRqId);
	}
//Frank@Test
  sprintf(pcgSqlBuf,"UPDATE MQRTAB SET STAT='OK' WHERE URNO='%s'",clRqId);
  ilRc = getOrPutDBData(START|COMMIT);
  if (ilRc != RC_SUCCESS) dbg(TRACE,"<remOnDelReq> Unable to perform <%s>", pcgSqlBuf);  
}

/******************************************************************************/
/* HandleSingleRecord                                                         */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- the leg entry number                                      */
/*                                                                            */
/* Returns:                                                                   */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static int HandleSingleRecord(char *pcTimeInfo, uint legLine)
{
  int ilRc;
  uint ilUnknownCmd = FALSE;
  uint ilHandlePeriodRecords = FALSE;
  uint i;
  ulong llUrno;
  char clUrno[32];
  char *pclData;
  char clMessage[128];
  char clRqId[64];
  char clActor[33];

  strcpy(clActor,"");
  if (pcTimeInfo != NULL) 
  {
//Frank@Test
		if(igRUN_MODE == 1)
		{
			dbg(DEBUG,"<HandleSingleRecord>new loop");
		}
//Frank@Test
		#ifdef TEST
			ilHandlePeriodRecords = TRUE;
			dbg(DEBUG,"<HandleSingleRecord>ilHandlePeriodRecords = TRUE");
  	#endif
  }  
 
  ilRc = identifyFlightRecord(&clActor[0], legLine);

//Frank@Test
	if(igRUN_MODE == 1)
  {
  	dbg(DEBUG,"++++++Frank@Test<HandleSingleRecord>end of identifyFlightRecord");
		if(igIfType == CSK)
		{
			dbg(DEBUG,"++++++Frank@Test<HandleSingleRecord>igIfType<CSK>");
		}
		else if(igIfType == ELSE)
		{
			dbg(DEBUG,"++++++Frank@Test<HandleSingleRecord>igIfType<ELSE>");
		}
		else if(igIfType == SQL)
		{
			dbg(DEBUG,"++++++Frank@Test<HandleSingleRecord>igIfType<SQL>");
		}
		#ifdef TEST
  		dbg(DEBUG,"++++++Frank@Test<HandleSingleRecord>original pcgActCmd<%s>",pcgActCmd);
			dbg(DEBUG,"++++++Frank@Test<HandleSingleRecord>for test change <%s> to DFR",pcgActCmd);
			memset(pcgActCmd,0,sizeof(pcgActCmd));
			strncpy(pcgActCmd,"DFR",sizeof("DFR"));
			dbg(DEBUG,"++++++Frank@Test<HandleSingleRecord>modified pcgActCmd<%s>",pcgActCmd);
		#endif
 	}
 //Frank@Test
  sprintf(clUrno,"%ld",lgUrno);
  /*
  ####    ####   #    #  ######  #####
 #    #  #       #   #   #       #    #
 #        ####   ####    #####   #    #
 #            #  #  #    #       #    #
 #    #  #    #  #   #   #       #    #
  ####    ####   #    #  ######  #####

  */  
  if (igIfType == CSK) {
    if (!strcmp(pcgActCmd,"UFR")) {
      switch (ilRc) {
      case RC_NODATA:
	sprintf(clMessage, "Unable to update nonexistant flight");
	alert(clMessage);
	return RC_FAIL;
	break;
      case RC_ONDELETE:
	if (!strncmp(clActor,"csked",5)) {
	  remOnDelReq(lgUrno);
	  sprintf(clRqId,"%s",pcgTwStart);
	  ConvertClientStringToDb(clRqId);
	  sprintf(pcgSqlBuf,"UPDATE IFTTAB SET DELF='%d',STAT='INSERTING',RQID='%s' WHERE UAFT='%ld'", igDelFram[igIfType],clRqId,lgUrno);
	  ilRc = getOrPutDBData(START|COMMIT);
	  if (ilRc != RC_SUCCESS) dbg(TRACE,"<HandleSingleRecord> Unable to perform <%s>", pcgSqlBuf);
	}
	else if (!strncmp(clActor,"else",4)) {
	  /* shouldn't happen, no updates allowed while in mixed update sequence */
	  sprintf(clMessage, "CSKED update not allowed while ELSE mixed update is pending");
	  alert(clMessage);
	  return RC_FAIL;
	}
	break;
      case RC_SUCCESS:
	/* ok for update */
	break;
      case RC_NOTUNIQUE:
	sprintf(clMessage, "Unable to update nonunique flight");
	alert(clMessage);
	return RC_FAIL;
	break;
      default:
	dbg(TRACE,"<HandleSingleRecord> Ooops -- unknown rtc <%d>",ilRc);
	return RC_FAIL;
      }
    }
    else if (!strcmp(pcgActCmd,"IFR")) {
      switch (ilRc) {
      case RC_NODATA:
	/* ok for insert */
	if (igAllowInserts[igIfType] == NO) {
	  sprintf(clMessage, "Insert of flights not allowed for CSKED");
	  alert(clMessage);
	  return RC_FAIL;
	}
	break;
      case RC_ONDELETE:
	if (!strncmp(clActor,"csked",5)) {
	  remOnDelReq(lgUrno);
	  sprintf(clRqId,"%s",pcgTwStart);
	  ConvertClientStringToDb(clRqId);
	  sprintf(pcgSqlBuf,"UPDATE IFTTAB SET DELF='%d',STAT='INSERTING',RQID='%s' WHERE UAFT='%ld'", igDelFram[igIfType],clRqId,lgUrno);
	  ilRc = getOrPutDBData(START|COMMIT);
	  if (ilRc != RC_SUCCESS) dbg(TRACE,"<HandleSingleRecord> Unable to perform <%s>", pcgSqlBuf);
	  strcpy(pcgActCmd,"UFR"); /* Ok allow update */
	}
	else if (!strncmp(clActor,"else",4)) {
	  /* shouldn't happen, no updates allowed while in mixed update sequence */
	  sprintf(clMessage, "CSKED update not allowed while ELSE mixed update is pending");
	  alert(clMessage);
	  return RC_FAIL;
	}
	break;
      case RC_SUCCESS:
	/*
	** Special handling to reinstantiate flights
	** with FTYPE='X'
	** If we will find a flight with state 'X'
	** this is treated as UFR
	*/
	ilRc = checkFlightState(lgUrno, "FTYP", "X");
	if (ilRc == RC_SUCCESS) {
	  strcpy(pcgActCmd,"UFR"); /* IFR -> UFR */
	  /*
	  ** FTYP is set in depedence of FLDA (see imphdl.cfg)
	  */
	  setFtyp();
	  /*
	  ** provide URNO
	  */
	  for (i=1;i<=igNumOfLegs;i++) {
	    putItemDataByName(pcgItemList,"URNO", clUrno, i, IS_VALID|IS_AFT);
	  }
	}
	else {
	  dbg(TRACE,"<HandleSingleRecord> Unable to insert existing flight");
	  return RC_FAIL;
	}
	break;
      case RC_NOTUNIQUE:
	sprintf(clMessage, "Unable to update nonunique flight");
	alert(clMessage);
	return RC_FAIL;
	break;
      default:
	dbg(TRACE,"<HandleSingleRecord> Ooops -- unknown rtc <%d>",ilRc);
	return RC_FAIL;
      }
    }
    else if (!strcmp(pcgActCmd,"DFR")) {
      switch (ilRc) {
      case RC_NODATA:
	alert(clMessage);
	return RC_FAIL;
	break;
      case RC_ONDELETE:
	/* 
	** An already existing mixed update is pending
	** update timer to max and remove request block
	*/
	if (!strncmp(clActor,"csked",5)) {
	  remOnDelReq(lgUrno);
	  sprintf(clRqId,"%s",pcgTwStart);
	  ConvertClientStringToDb(clRqId);
	  sprintf(pcgSqlBuf,"UPDATE IFTTAB SET DELF='%d',STAT='ONDELETE',RQID='%s' WHERE UAFT='%ld'", igDelFram[igIfType],clRqId,lgUrno);
	  ilRc = getOrPutDBData(START|COMMIT);
	  if (ilRc != RC_SUCCESS) dbg(TRACE,"<HandleSingleRecord> Unable to perform <%s>", pcgSqlBuf);
	  return RC_SUCCESS;
	}
	else if (!strncmp(clActor,"else",4)) {
	  /* shouldn't happen, no updates allowed while in mixed update sequence */
	  sprintf(clMessage, "CSKED delete not allowed while ELSE mixed update is pending");
	  alert(clMessage);
	  return RC_FAIL;
	}
	break;
      case RC_SUCCESS:
	/* need to insert a record to IFTTAB and set it to 'ONDELETE' */
	break;
      case RC_NOTUNIQUE:
	sprintf(clMessage, "Unable to update nonunique flight");
	alert(clMessage);
	return RC_FAIL;
	break;
      default:
	dbg(TRACE,"<HandleSingleRecord> Ooops -- unknown rtc <%d>",ilRc);
	return RC_FAIL;
      }
    }
  }
  /*
  #####    ##    #    #   ####
    #     #  #   ##  ##  #
    #    #    #  # ## #   ####
    #    ######  #    #       #
    #    #    #  #    #  #    #
    #    #    #  #    #   ####
   */
  else if (igIfType == ELSE || igIfType == SQL) {
    if (!strcmp(pcgActCmd,"UFR")) {
      switch (ilRc) {
      case RC_NODATA:
        if (igIsNonSched != 0)
        {
	  /*
		identifyFlightRecord() did not find this CSGN in AFT, but this was already
		known in wrapElse for non-sched. Command was already changed to IFR there
		hence we should not be here.  
	  */
	  dbg (TRACE, "HandleSingleRecord: Warnnig: Bug - Non Sched IFR should not be here");
	  break;
        }
        /* Scheduled flights from here on */
	pclData = getItemDataByName(pcgItemList, "FLNO", 0);
	if (pclData != NULL) {
	  /* 
	  ** Handling of restricted airlines
	  ** should go to the config file 
	  */
	  if (strncmp(pclData,"SQ",2) || strncmp(pclData,"MI",2)) {
	    /*
	    ** check if this is a return/taxi flight
	    ** removed the check in 2.9:
	    ** We are not able to decide if we may insert an arbitrary
	    ** suffix for an unknown flight, so we will allow any suffix for now.
	    ** The sender is responsible for the correct choice 
	    */
	    /*if (igIsRTFlight) {
	    ** dbg(TRACE,"<HandleSingleRecord> Refusing to insert a flight with suffix");
	    **  return RC_FAIL;
	    **}
	    */
	    /*
	    ** check if this is a code share (flagged by the existance of COSH field
	    */
	    pclData = getItemDataByName(pcgItemList, "COSH", 0);
	    if (pclData != NULL) {
	      /* we have found a code share request to a non existant flight! */
	      sprintf(clMessage, "Insert of code share <%s> not allowed, master flight has to exist!", pclData);
	      alert(clMessage);
	      return RC_FAIL;
	    }
	    /*
	    ** we have to recalculate routing here!!!
	    */
	    strcpy(pcgActCmd,"IFR");
	    ilRc = checkGroupOfRoutingInfo(legLine);
	    if (ilRc == RC_FAIL) {
	      dbg(TRACE,"<HandleSingleRecord> Group of Routing Info inconsistency leg %d data skipped",legLine);
	      return RC_FAIL;
	    }
	  }
	  else {
	    sprintf(clMessage, "Insert of flight <%s> not allowed for SQ/MI", pclData);
	    alert(clMessage);
	    return RC_FAIL;
	  }
	}
	break;
      case RC_ONDELETE:
	if (!strncmp(clActor,"csked",5)) {
	      /* shouldn't happen, no updates allowed while in mixed update sequence */
	      sprintf(clMessage, "ELSE update not allowed while mixed update is pending");
	      alert(clMessage);
	      return RC_FAIL;
	}
	else if (!strncmp(clActor,"else",4)) {
	  /*
	  ** we have a pending ELSE "DELETE"
	  ** remove the pending request and 
	  ** perform the requested update
	  */
	  remOnDelReq(lgUrno);
	  sprintf(clRqId,"%s",pcgTwStart);
	  ConvertClientStringToDb(clRqId);
	  sprintf(pcgSqlBuf,"DELETE FROM IFTTAB WHERE UAFT='%ld'",lgUrno);
	  ilRc = getOrPutDBData(START|COMMIT);
	  /*
	  ** FTYP of the according flight hasn't yet been touched
	  ** so it will be reactivated with its current FTYP
	  */
	  /*putItemDataByName(pcgItemList,"FTYP", "O", legLine, IS_VALID|IS_AFT);*/
	  if (ilRc != RC_SUCCESS) dbg(TRACE,"<HandleSingleRecord> Unable to perform <%s>", pcgSqlBuf);
	}
	break;
      case RC_SUCCESS:
	/* ok for update */
	/*
	** Additional requirements for 'X' flights(from RFC6470):
	** First we have to check if FTYP is within new data
	** If so -> this FTYP will be used
	** if not, we have to check if this flight is cancelled
	** If so  -> we assume a reinstantiate to this flight
	** and the flight's FTYP turns to 'O'
	** if not -> we will keep the current FTYP
	*/
	pclData = getItemDataByName(pcgItemList, "FTYP", 0);
	if (pclData == NULL) {
	  ilRc = checkFlightState(lgUrno, "FTYP", "X");
	  if (ilRc == RC_SUCCESS){
	    /*
	    ** New data doesn't specify FTYP
	    ** so force this flight to 'O'
	    */
	    putItemDataByName(pcgItemList,"FTYP", "O", legLine, IS_VALID|IS_AFT);
	  }
	}
	break;
      case RC_NOTUNIQUE:
	sprintf(clMessage, "Unable to update nonunique flight");
	alert(clMessage);
	return RC_FAIL;
	break;
      case RC_HASSUFFIX:
	sprintf(clMessage, "Corresponding flight with suffix found -- refusing to insert");
	alert(clMessage);
	return RC_FAIL;
	break;
      default:
	dbg(TRACE,"<HandleSingleRecord> Ooops -- unknown rtc <%d>",ilRc);
	return RC_FAIL;
      }
    }
    else if (!strcmp(pcgActCmd,"DFR")) {
      switch (ilRc) {
      case RC_NODATA:
	if (ilHandlePeriodRecords == FALSE) {
	  sprintf(clMessage, "Unable to delete nonexistant flight");
	  alert(clMessage);
	  return RC_FAIL;
	}
	else 
	{	
//Frank@Test 
		//Frank@Test the modification
		/*
		dbg(DEBUG,"@@@@@@Frank@Test<HandleSingleRecord>calling remOnDelReq");
		dbg(DEBUG,"Frank<remOnDelReq>: UPDATE MQRTAB SET STAT='OK' WHERE URNO='%s'",clRqId);
		sprintf(pcgSqlBuf,"UPDATE MQRTAB SET STAT='OK' WHERE URNO='%s'",clRqId);
		ilRc = getOrPutDBData(START|COMMIT);
		if (ilRc != RC_SUCCESS) dbg(TRACE,"<remOnDelReq> Unable to perform <%s>", pcgSqlBuf);
		
		remOnDelReq(lgUrno);
		*/
		dbg(DEBUG,"line<%d>-----------<HandleSingleRecord>lgUrno<%d>-------------",__LINE__,lgUrno);
		dbg(DEBUG,"<HandleSingleRecord>llDataID<%d>",lgDataID);
		dbg(DEBUG,"<HandleFlightResponseData>: UPDATE MQRTAB SET STAT='OK' WHERE URNO=%d",lgDataID);
		sprintf(pcgSqlBuf, "UPDATE MQRTAB SET STAT='OK' WHERE URNO=%d", lgDataID);
		ilRc = getOrPutDBData(START|COMMIT);
		if (ilRc != RC_SUCCESS) 
		{
			dbg(TRACE,"<HandleFlightResponseData> Unable to UPDATE MQRTAB SET STAT='OK' WHERE URNO=%d", lgDataID);
		}
		lgDataID = 0;
		
		if(igRUN_MODE == 1)
		{
			dbg(DEBUG,"<HandleSingleRecord>interface:ELSE");
			dbg(DEBUG,"<HandleSingleRecord>command:DFR");
			dbg(DEBUG,"<HandleSingleRecord>finally it comes to this point which return RC_SUCCESS");
		}
//Frank@Test
		return RC_SUCCESS;
	}
	break;
      case RC_ONDELETE:
	if (!strncmp(clActor,"csked",5)) {
	  sprintf(clMessage, "ELSE delete not allowed while mixed update is pending");
	  alert(clMessage);
	  return RC_FAIL;
	}
	else if (!strncmp(clActor,"else",4)) {
	  /* 
	  ** An already existing mixed update is pending
	  ** update timer to max and remove request block
	  */
	  remOnDelReq(lgUrno);
	  sprintf(clRqId,"%s",pcgTwStart);
	  ConvertClientStringToDb(clRqId);
	  sprintf(pcgSqlBuf,"UPDATE IFTTAB SET DELF='%d',STAT='ONDELETE',RQID='%s' WHERE UAFT='%ld'", igDelFram[igIfType],clRqId,lgUrno);
	  ilRc = getOrPutDBData(START|COMMIT);
	  if (ilRc != RC_SUCCESS) dbg(TRACE,"<HandleSingleRecord> Unable to perform <%s>", pcgSqlBuf);
	  return RC_SUCCESS; 
	}
	break;
      case RC_SUCCESS:
	/* ok for delete */
	break;
      case RC_NOTUNIQUE:
	sprintf(clMessage, "Unable to update nonunique flight");
	alert(clMessage);
	return RC_FAIL;
	break;
      case RC_HASSUFFIX:
	sprintf(clMessage, "Corresponding flight with suffix found -- refusing to delete");
	alert(clMessage);
	return RC_FAIL;
	break;
      default:
	dbg(TRACE,"<HandleSingleRecord> Ooops -- unknown rtc <%d>",ilRc);
	return RC_FAIL;
      }
    }
  }
  if (!strcmp(pcgActCmd,"DFR")) ilRc = HandleDelete(legLine);
  else ilRc = HandleInsOrUpd(legLine);
  return ilRc;
}	
/******************************************************************************/
/* checkFlightState                                                           */
/*                                                                            */
/* Lookup pclItem in IFT/AFTTAB and check if it has pclValue. If true return  */
/* RC_SUCCESS, RC_FAIL else                                                   */
/*                                                                            */
/******************************************************************************/
static int checkFlightState(long lpUrno , char *pclItem, char *pclValue)
{
  int ilRc;

  sprintf(pcgSqlBuf, "SELECT UAFT FROM IFTTAB WHERE %s='%s' AND UAFT=%ld", pclItem, pclValue, lpUrno);
  ilRc = getOrPutDBData(START);
  if (ilRc == RC_SUCCESS) return ilRc;
  dbg(DEBUG, "<checkFlightState> Statement wasn't succesfull <%s>",pcgSqlBuf); 
  sprintf(pcgSqlBuf, "SELECT URNO FROM AFTTAB WHERE %s='%s' AND URNO=%ld", pclItem, pclValue, lpUrno);
  ilRc = getOrPutDBData(START);
  if (ilRc != RC_SUCCESS) dbg(DEBUG, "<checkFlightState> Statement wasn't succesfull <%s>",pcgSqlBuf);
  return ilRc;
}
/******************************************************************************/
/* setFtype                                                                   */
/*                                                                            */
/* Fetch flight day and compare against actual day. If igSchedInterval is     */
/* lower/equal difference, FTYP='O' else FTYP='S'                             */
/*                                                                            */
/* Result is written to the itemlist.                                         */
/*                                                                            */
/******************************************************************************/
static int setFtyp()
{
  int  ilRc;
  uint ilActDay, ilActMin, ilActWkDy;
  uint ilFldaDay, ilFldaMin, ilFldaWkDy;
  int  ilDiff;
  char clActTime[15];
  char clFldu[15];
  char *pclData;
  char clFtyp[2];

  GetServerTimeStamp("UTC", 1, 0, clActTime);
  pclData = getItemDataByName(pcgItemList, "FLDU", 0);
  if (pclData == NULL) {
    dbg(TRACE,"<setFtyp> Unable to fetch CSKED FLDA -- FATAL");
    return RC_FAIL;
  }
  strcpy(clFldu, pclData);
  strcat(clFldu, "000000"); /* add time part not quite exact but should be sufficient */
  ilRc = GetFullDay(clActTime,(int *)&ilActDay, (int *)&ilActMin, (int *)&ilActWkDy);
  ilRc = GetFullDay(clFldu,(int *)&ilFldaDay, (int *)&ilFldaMin, (int *)&ilFldaWkDy);
  ilDiff = ilFldaDay - ilActDay;
  dbg(DEBUG,"<setFtyp> ActTime: %s FLDU: %s Difference: %d Limit: %d", clActTime, clFldu, ilDiff, igSchedInterval);
  if (ilDiff < 0 || ilDiff > igSchedInterval) {
    strcpy(clFtyp, "S");
  }
  else {
    strcpy(clFtyp, "O");
  }
  /*
  ** Update internal FTYP
  */
  sprintf(pcgSqlBuf,"UPDATE IFTTAB SET FTYP='%s' WHERE UAFT='%ld'",clFtyp,lgUrno);
  getOrPutDBData(START);
  putItemDataByName(pcgItemList,"FTYP", &clFtyp[0], 0, IS_VALID|IS_AFT);
  return RC_SUCCESS;
}
/******************************************************************************/
/* onDelete                                                                   */
/*                                                                            */
/* Check if the specified record has status 'ONDELETE'.                       */
/*                                                                            */
/* Input:                                                                     */
/*       ipUrno -- UAFT of requested record                                   */
/*                                                                            */
/* Return:                                                                    */
/*       TRUE/FALSE                                                           */
/*                                                                            */
/******************************************************************************/
static int onDelete(long ipUrno)
{
  int ilRc;

  sprintf(pcgSqlBuf, "SELECT UAFT FROM FSLTAB WHERE UAFT=%ld AND STAT='ONDELETE'", ipUrno);
  ilRc = getOrPutDBData(START);
  if (ilRc == RC_SUCCESS) {
    if (igHavePendingDeletes = 0) { 
      dbg(TRACE, "<onDelete> Inconsistent internal state of mixed update deletes -- updating");
      setPendingDeletes();
    }
    return TRUE;
  }
  else return FALSE;
}
/******************************************************************************/
/* HandlePeriodRecords                                                        */
/*                                                                            */
/* From the period info create all required flight records for this period.   */
/* The GetFullDay function is used to transform the VPFR and VPFO dates into  */
/* a period. The flight day is calculated considering the Period-Mask. The    */
/* calculated period is then transformed back to a CEDA time using the        */
/* FullDayDate function.                                                      */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- the leg enry number                                       */
/*                                                                            */
/* Returns:                                                                   */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static int HandlePeriodRecords(uint legLine)
{
  char *pclVpfr;
  char *pclVpto;
  char *pclFreq;
  char *pclSTOD;
  uint frqw;
  int ilGetRc;
  int ilRc = RC_SUCCESS;
  int ilVpfrVal,ilMinVal,ilVpfrDay;
  int ilVptoVal,ilVptoDay;
  uint ilDayCount;
  uint ilActDay, ilActMin, ilWkDy;
  int ilThisDay;
  uint ilADID;
  uint ilHaveSched = FALSE;
  uint ilIsDfr = FALSE;
  char pclMask[32];
  char clNextDate[9];
  char clFlightDate[13];
  char clSTOData[13];
  char *pclSTOData;
  char clFieldName[32];
  char clFlightTime[7];
  char clFLDA[15];
  char clOrigCmd[32];

  /*
  ** prepare buffer 
  */
  memset (clFlightDate, '\0', 13) ;
  igHavePeriodData = TRUE;

  if (!strcmp(pcgActCmd,"DFR")) ilIsDfr = TRUE;

  if (ilIsDfr == FALSE) {
    /*
    ** get all the needed stuff first
    */
    ilADID = pcgItemList->RouteType[legLine];
    
    if (ilADID == A) {
      /* STOA */
      strcpy (clFieldName, "STOA");
    }
    else if (ilADID == D) {
      /* STOD */
      strcpy (clFieldName, "STOD");
    }
    else {
      dbg(TRACE, "<HandlePeriodRecords> Invalid ADID code <%d>", ilADID);
      return RC_FAIL;
    }
    pclSTOData = getItemDataByName(pcgItemList, &clFieldName[0], legLine);
    if (pclSTOData == NULL) {
      strcpy (clFieldName, "FLDA");
      /*
      **dbg(TRACE,"<HandlePeriodRecords> No data for required field <%s>", clFieldName);
      **return RC_FAIL;
      */
    }
    else ilHaveSched=TRUE;
  }
  else {
    strcpy (clFieldName, "FLDA");
  }
 
  if (ilHaveSched) {
    /* STOD/STOA */
    strcpy(&clSTOData[0], pclSTOData);
  }
  else {
    /* FLDA */
    strcpy(&clSTOData[0], "0000");
    addItemToList("FLDA", &pcgItemList);
  }
  /*
  ** add seconds 
  */
  strcpy(&clFlightTime[0], clSTOData);
  if (strlen(&clFlightTime[0]) == 4) {
    strcat(&clFlightTime[0], "00");
  }
  pclVpfr = pcgItemList->periodInfo[legLine].vpfr;
  pclVpto = pcgItemList->periodInfo[legLine].vpto;
  pclFreq = pcgItemList->periodInfo[legLine].freq;
  frqw = pcgItemList->periodInfo[legLine].frqw;
  /*
  ** transform CEDA time to period
  */
  ilGetRc = GetFullDay (pclVpfr, &ilVpfrVal, &ilMinVal, &ilVpfrDay);
  ilGetRc = GetFullDay (pclVpto, &ilVptoVal, &ilMinVal, &ilVptoDay);
  /*
  ** check if day is in NBC distance
  */
  if (ilVpfrVal - ilActDay >= igNBCInterval) {
    dbg(DEBUG,"<HandlePeriodRecords> ilVpfrVal: <%u>  ilActDay: <%u>", ilVpfrVal, ilActDay);
    igNBC = TRUE;
  }
  else igNBC = FALSE;
  /*
  ** format the period day mask 
  ** initialize working variables
  */
  ilDayCount = FormatDayFreq (pclFreq,pclMask);
  ilActDay = ilVpfrDay;
  ilThisDay = ilVpfrVal;
  /*
  ** if the first day is in the mask nothing else to do
  */
  if (isValidDay(pclMask, ilActDay)) {
    getNextDay(&ilThisDay, ilVptoVal, &ilActDay, pclMask, FALSE, &clNextDate[0], frqw);
  }
  /*
  ** tell the function to proceed to the next valid day
  */
  else {
    getNextDay(&ilThisDay, ilVptoVal, &ilActDay, pclMask, TRUE, &clNextDate[0], frqw);
  }
  strcpy(clFlightDate, clNextDate); 
  strcat(clFlightDate, &clSTOData[0]);
  dbg(DEBUG,"<HandlePeriodRecords> Create flight entry STOA/D <%s>", clFlightDate);
  if (!strcmp(clFieldName,"FLDA")) {
    clFlightDate[8] = '\0';
    putItemDataByName(pcgItemList,&clFieldName[0], &clFlightDate[0], legLine, IS_VALID|IS_AFT);
  }
  else {
    putItemDataByName(pcgItemList,&clFieldName[0],&clFlightDate[0],legLine,IS_VALID|IS_AFT);
    clFlightDate[8] = '\0';
    putItemDataByName(pcgItemList,"FLDA",clFlightDate,legLine,IS_VALID|IS_AFT);
  }
  strcpy(clOrigCmd,pcgActCmd); /* remember in case of changes */
//Frank@Test
	if(igRUN_MODE == 1)
	{
		dbg(DEBUG,"Frank@Test<HandlePeriodRecords>calling HandleSingleRecord-A");
	}
//Frank@Test  
	ilRc = HandleSingleRecord(clFlightDate, legLine);
  strcpy(pcgActCmd,clOrigCmd);
  if (strcmp(pcgActCmd,"DFR")) {  /* ignore errors while deleting */
    if (ilRc != RC_SUCCESS) return ilRc; 
  }
  /*
  ** now do the remaining records in this period
  */
  while (getNextDay(&ilThisDay, ilVptoVal, &ilActDay, pclMask, TRUE, clNextDate, frqw) == TRUE) {
    strcpy(clFlightDate, clNextDate); 
    strcat(clFlightDate, &clSTOData[0]);
    dbg(DEBUG,"<HandlePeriodRecords> Create flight entry STOA/D <%s>", clFlightDate);
    if (!strcmp(clFieldName,"FLDA")) {
      clFlightDate[8] = '\0';
      putItemDataByName(pcgItemList,&clFieldName[0], &clFlightDate[0], legLine, IS_VALID|IS_AFT);
    }
    else {
      putItemDataByName(pcgItemList,&clFieldName[0], &clFlightDate[0], legLine, IS_VALID|IS_AFT);
      clFlightDate[8] = '\0';
      putItemDataByName(pcgItemList,"FLDA",clFlightDate,legLine,IS_VALID|IS_AFT);
    }
    strcpy(clOrigCmd,pcgActCmd);
//Frank@Test
	if(igRUN_MODE == 1)
	{
		dbg(DEBUG,"++++++Frank@Test<HandlePeriodRecords>calling HandleSingleRecord-B");
	}
//Frank@Test
    ilRc = HandleSingleRecord(clFlightDate, legLine);
    strcpy(pcgActCmd,clOrigCmd);
    if (strcmp(pcgActCmd,"DFR")) { /* ignore errors while deleting */
      if (ilRc != RC_SUCCESS) return ilRc;
    }
  }
  igNBC = FALSE;
  putItemDataByName(pcgItemList, &clFieldName[0], &clFlightTime[0], legLine, IS_VALID|IS_AFT);
  return ilRc;
}
/******************************************************************************/
/* completeCedaTime                                                           */
/*                                                                            */
/* Completes a cedatime. If we have a gate time that has no date, we will try */
/* to calculate a reasonable day using departure time.                        */
/*                                                                            */
/* Input:                                                                     */
/*       pcpRefDate -- a string containing the departure date(time)           */
/*       pcpTime    -- a string containing the gate time                      */
/*                                                                            */
/* Return:                                                                    */
/*       RC_SUCCESS and pcpRefDate containing the calculated day/time pair    */
/*       RC_FAIL on error                                                     */
/*                                                                            */
/******************************************************************************/
int completeCedaTime(char *pcpRefDate, char *pcpTime, uint ipGateMode, uint ipLocal, uint ipDeparture)
{
  char clRefDate[15];
  char clTestDate[15];
  char clTestTime[7];
  int  ilCntMinRef;
  int  ilCntDayRef;
  int  ilCntMinTest;
  int  ilCntDayTest;
  int  ilWkDy;
  int  ilRC;
  int  ilMinDiff;

  strcpy(clRefDate, pcpRefDate);
  strncpy(clTestDate, clRefDate, 8);
  clTestDate[8] = '\0';
  strcat(clTestDate, pcpTime);

  /*  if (ipLocal == LOCAL) LocalTimeToUtcFixTZ(clRefDate); */
  if (igTimeFromIf == LOCAL) {
    LocalTimeToUtcFixTZ(clRefDate);
    LocalTimeToUtcFixTZ(clTestDate);
  }

  strncpy(clTestTime, &clTestDate[8], 6);
  clTestTime[6] = '\0';

  ilRC = GetFullDay(clRefDate, &ilCntDayRef, &ilCntMinRef, &ilWkDy);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE, "<completeCedaTime> Error getting full day from reference <%s>", clRefDate);
    return RC_FAIL;
  }
  ilRC = GetFullDay(clTestDate, &ilCntDayTest, &ilCntMinTest, &ilWkDy);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE, "<completeCedaTime> Error getting full day from test <%s>", clTestDate);
    return RC_FAIL;
  }

  /*
  if (ipGateMode == TRUE) {
    if (ilCntMinTest > ilCntMinRef) {
      ilCntDayTest = ilCntDayTest - 1;
      FullDayDate(clTestDate, ilCntDayTest);
      strcat(clTestDate, clTestTime);
    }
    dbg(DEBUG, "<completeCedaTime> Refdate <%s> test time <%s> calculated gate time <%s>", pcpRefDate, pcpTime, clTestDate); 
    strcpy(pcpRefDate, clTestDate);
    return RC_SUCCESS;
  }
  */
  ilMinDiff = ilCntMinTest - ilCntMinRef;
  if (ipDeparture) ilMinDiff = -ilMinDiff;
  if (ilMinDiff >= 0) { /* new date after schedule and beyond max delay */
    if (ilMinDiff <= igMaxMinAfterSched) {
      dbg(DEBUG, "<completeCedaTime> Refdate <%s> test time <%s> calculated ceda time <%s>", pcpRefDate, pcpTime, clTestDate);
      strcpy(pcpRefDate, clTestDate);
      return RC_SUCCESS;
    }
  }
  if (ilMinDiff < 0) { /* new date before schedule and in max before limit */ 
    if (abs(ilMinDiff) <= igMaxMinBeforeSched) {
      dbg(DEBUG, "<completeCedaTime> Refdate <%s> test time <%s> calculated ceda time <%s>", pcpRefDate, pcpTime, clTestDate);
      strcpy(pcpRefDate, clTestDate);
      return RC_SUCCESS;
    }
  }
  /*
  ** Not yet succeeded, try next day
  */
  if (ipDeparture) ilCntDayTest = ilCntDayTest - 1;
  else ilCntDayTest = ilCntDayTest + 1;
  FullDayDate(clTestDate, ilCntDayTest);
  strcat(clTestDate, clTestTime);
  ilRC = GetFullDay(clTestDate, &ilCntDayTest, &ilCntMinTest, &ilWkDy);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE, "<completeCedaTime> Error getting full day from test <%s>", clTestDate);
    return RC_FAIL;
  }
  ilMinDiff = ilCntMinTest - ilCntMinRef;
  if (ipDeparture) ilMinDiff = -ilMinDiff;
  if (ilMinDiff <= igMaxMinAfterSched) {
    dbg(DEBUG, "<completeCedaTime> Refdate <%s> test time <%s> calculated gate time <%s>", pcpRefDate, pcpTime, clTestDate);
    strcpy(pcpRefDate, clTestDate);
    return RC_SUCCESS;
  }
  dbg(TRACE, "<completeCedaTime> Unable to build: Refdate <%s> test time <%s> calculated gate time <%s>", clRefDate, clTestTime, clTestDate);
  return RC_FAIL;
}
/******************************************************************************/
/* getNextDay                                                                 */
/*                                                                            */
/* This function creates and returns the next valid CEDA date in this period. */
/*                                                                            */
/* Input:                                                                     */
/*       thisDay -- points to the last day we have completed                  */
/*       lastDay -- the last day in this period                               */
/*       ActDay  -- the last mask entry (week day) we worked on               */
/*       plcMask -- the period (week day) mask                                */
/*       proceed -- TRUE, we will proceed to the next valid day               */
/*                  FALSE, we will calculate the current date w/o proceeding  */
/*                                                                            */
/* Returns:                                                                   */
/*       thisDay -- updated to the next valid date (if proceed = TRUE)        */
/*       ActDay  -- updated to the next valid week day (if proceed = TRUE)    */
/*       ppDate  -- receives the CEDA date                                    */  
/*                                                                            */
/******************************************************************************/
static int getNextDay(int *thisDay, int lastDay, uint *ActDay, char *pclMask, uint proceed, char *ppDate, uint ipFrqw)
{
  uint ilDay = 0;
  uint ilOldDay;
  char date[9];

  if (proceed == TRUE) {
    ilOldDay = *ActDay;
    ilDay = nextValidDay(pclMask, *ActDay);
    if (ilDay == 0) return FALSE;
    *ActDay = ilDay;
    if (ilDay <= ilOldDay) ilDay = ilDay + (7*ipFrqw); /* possibly wrap to next valid week */
    *thisDay = *thisDay + (ilDay - ilOldDay); 
  }
  strcpy(date, cgEmptyDate);
  if (*thisDay <= lastDay) {
    FullDayDate(&date[0], *thisDay);
  }
  else {
    return FALSE;
  }
  strcpy(ppDate, date);
  return TRUE;
}
/******************************************************************************/
/* nextValidDay                                                               */
/*                                                                            */
/* returns the next valid day, starting from the current day                  */
/*                                                                            */
/* Input:                                                                     */
/*       pclMask -- the period (week day) mask                                */
/*       day     -- the current day                                           */
/*                                                                            */
/* Returns:                                                                   */
/*       the next valid day                                                   */
/*                                                                            */
/******************************************************************************/
static int nextValidDay(char *pclMask, uint day)
{
  char clActDay[2];
  uint i = 0;
  uint ilDay = 0;

  if (day >= 1 && day <=7) {
    /* Try to find the next possible day */
    while (pclMask[i] != '\0') {
      clActDay[0]= pclMask[i];
      clActDay[1] = '\0';
      ilDay = atoi(&clActDay[0]);
      if (ilDay > day) {
	return ilDay;
      }
      i++;
    }
    /* No day found, wrap day list */
    i = 0;
    while (pclMask[i] != '\0') {
      if (strncmp (&pclMask[i], " ", 1)) {
	clActDay[0] = pclMask[i];
	clActDay[1] = '\0';
	return atoi(&clActDay[0]);
      }
      i++;
    }
  }
  dbg(TRACE, "<nextValidDay> Invalid day %ud", day);
  return 0;
}
/******************************************************************************/
/* getFreeUrno                                                                */
/*                                                                            */
/* Maintains a pool of URNOS. If no more URNOS are available, the pool is     */
/* refilled with a set of URNO_POOL_SIZE.                                     */
/*                                                                            */
/* Input:                                                                     */
/*       Nothing                                                              */
/*                                                                            */
/* Return:                                                                    */
/*       cpUrno -- the buffer pointed to is filled with a new URNO            */
/*       RC_SUCCESS/RC_FAIL                                                   */
/*                                                                            */
/******************************************************************************/
static int getFreeUrno(char *cpUrno)
{
  static uint ilNumUrnos = 0;
  static uint ilFirstUrno = 0;
  static uint ilNextUrno = 0;
  char clNewUrno[URNO_SIZE];
  int ilRc;

  if (ilNumUrnos == 0) {
    ilRc = GetNextValues (clNewUrno, URNO_POOL_SIZE);
    if (ilRc == RC_SUCCESS) {
      ilFirstUrno = atoi(clNewUrno);
      ilNextUrno = ilFirstUrno;
      ilNumUrnos = 100;
    }
    else return RC_FAIL;
  }
  sprintf(cpUrno, "%d", ilNextUrno);
  ilNextUrno++;
  ilNumUrnos--;
  dbg(DEBUG,"<getFreeUrno> URNO First:<%d> Next:<%d> Num:<%d>", 
      ilFirstUrno, ilNextUrno, ilNumUrnos);
  return RC_SUCCESS;
}
/******************************************************************************/
/* isValidDay                                                                 */
/*                                                                            */
/* checks if a day is valid.                                                  */
/*                                                                            */
/* Input:                                                                     */
/*       pclMask -- the period (week day) mask                                */
/*       day     -- the current day                                           */
/*                                                                            */
/* Returns:                                                                   */
/*       TRUE/FALSE -- valid/invalid                                          */
/*                                                                            */
/******************************************************************************/
static int isValidDay(char *pclMask, uint day)
{
  char clActDay[2];
  uint i = 0;

  if (day >= 1 && day <=7) {
    sprintf (clActDay, "%d", day);
    while (pclMask[i] != '\0') {
      if (!strncmp(clActDay,&pclMask[i],1)) {
	/* have it ! */
	return TRUE; 
      }
      i++;
    }
  }
  return FALSE;
}
/******************************************************************************/
/* FormatDayFreq                                                              */
/*                                                                            */
/* This function is borrowed from flight ( )                                  */
/*                                                                            */
/* Input:                                                                     */
/*       pcpFreq     -- pointer to the period mask                            */
/*       pcpFreqChr  -- receives the new formatted output                     */
/*                                                                            */
/* Returns:                                                                   */
/*                                                                            */
/******************************************************************************/
static int FormatDayFreq (char *pcpFreq, char *pcpFreqChr)
{
  int ilIdx;
  int ilChrPos;
  int ilStrLen;
  int ilDayCnt = 0;
  char pclTmpChr[] = "X";
  strcpy (pcpFreqChr, "       ");

  ilStrLen = strlen (pcpFreq);
  for (ilChrPos = 0; ilChrPos < ilStrLen; ilChrPos++)
  {
    if (((pcpFreq[ilChrPos] >= '1') && (pcpFreq[ilChrPos] <= '7')) || (pcpFreq[ilChrPos] == 'X'))
    {
      if (pcpFreq[ilChrPos] == 'X')
      {
        pcpFreqChr[ilChrPos] = ilChrPos + '1';
      }                         /* end if */
      else
      {
        pclTmpChr[0] = pcpFreq[ilChrPos];
        ilIdx = atoi (pclTmpChr) - 1;
        pcpFreqChr[ilIdx] = pcpFreq[ilChrPos];
      }                         /* end else */
      ilDayCnt++;
    }                           /* end if */
  }                             /* end for */
  return ilDayCnt;
}                               /* end FormatDayFreq */

/******************************************************************************/
/* HandleInsOrUpd                                                             */
/*                                                                            */
/* Send data to flight. Creates a list of fields and a List of data to        */
/* perfrom an IFR/UFR transaction. Time values a changed to UTC if required.  */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- data entry                                                */

/*                                                                            */
/* Return:                                                                    */
/*        RC_SUCCESS/RC_FAIL                                                  */
/*                                                                            */
/******************************************************************************/
static int HandleInsOrUpd(uint legLine)
{
  uint ilReceiver=igFlightId;
  uint ilPrio;
  uint ilADID;
  uint ilMaxLegs = 1;
  uint ilIsAf1Upd = FALSE;
  int  ilRc = RC_SUCCESS;
  char *pcSTO;
  char *pcFKEY;
  char *pclRoute;
  char *pclData;
  char clFieldName[32];
  char clADID[2];
  char clFKey[19];
  char clFLTN[6];
  char clALC3[4];
  char clSTO[15];
  char clFTYP[2];
  char clUrno[URNO_SIZE];
  char clAF1Urno[URNO_SIZE];
  char clFLDA[15];
  static char  pclTwStart[64] ;
  char clRqId[64];
  uint i,j,k;

  if (igPrioFromIf != 0) {
    ilPrio = igPrioFromIf;
  }
  else ilPrio = igPrio[igIfType];

  pcgAftFields[0] = '\0';
  pcgAftValues[0] = '\0';
  memset (pcgSelection, '\0', 1024) ;

  if (ilRc == RC_FAIL) return RC_FAIL;
  ilADID = pcgItemList->RouteType[legLine];
  if (ilADID == A) {
    strcpy(&clFieldName[0], "STOA");
    strcpy(&clADID[0], "A");
  }
  else if (ilADID == D) {
    strcpy(&clFieldName[0], "STOD");
    strcpy(&clADID[0], "D");
  }
  else {
    dbg (TRACE, "<HandleInsOrUpd> Invalid ADID code <%d>", ilADID);
    return RC_FAIL;
  }
  /*
  ** now fetch one of STOA/STOD
  ** if we are updating allow FLDA too
  */
  pcSTO = getItemDataByName(pcgItemList, &clFieldName[0], legLine);
  if (pcSTO == NULL && !strcmp(pcgActCmd, "IFR")) {
    dbg(TRACE,"<HandleInsOrUpd> No data for required field <%s> for %s-command", clFieldName, pcgActCmd);
    return RC_FAIL;
  }

  if (!strcmp(pcgActCmd, "IFR")) {
    strcpy(&clSTO[0], pcSTO);
    if (igTimeFromIf == LOCAL) {
      LocalTimeToUtcFixTZ(clSTO);
    }
    if (igIsNonSched == 0)
    {
      ilRc = writeFKEYcomp(legLine); /* create ALC3/FLTN */
      if (ilRc != RC_SUCCESS) return RC_FAIL;
    }
  }
  else if (!strcmp(pcgActCmd, "UFR") || !strcmp(pcgActCmd, "DFR")) {
    /* add required WHERE clause */
    sprintf(pcgSelection, "WHERE URNO=%ld", lgUrno);
  }

  /*
  ** fetch a new URNO and construct 
  ** an IFTTAB entry
  ** check if we have a pending delete, i.e we would
  ** find a URNO in internal data
  */
  if (!strcmp(pcgActCmd, "IFR")) {
    pclData = getItemDataByName(pcgItemList, "URNO", legLine);
    if (pclData != NULL) lgOnDelUrno = atol(pclData);
    ilRc = getFreeUrno(clUrno);
    if (ilRc == RC_SUCCESS) {
      putItemDataByName(pcgItemList,"URNO", clUrno, legLine, IS_VALID|IS_AFT);
      ilRc = createAFTSelection(pcgAftFields,pcgAftValues, pcgItemList, TRUE, legLine);
      if (ilRc != RC_SUCCESS) return RC_FAIL;
      lgUrno=atol(clUrno);
      /* putItemDataByName(pcgItemList,"URNO", clUrno, 0, IS_VALID); */
      lgUrno = atol(clUrno);
      /*
      ** Special NFL/NOP/NFL sequence, which reinstantiates a
      ** cancelled flight
      ** Need to have a reminder until flight is in AFT
      */
      pclData = getItemDataByName(pcgItemList, "FTYP", legLine);
      if (pclData != NULL) sprintf(clFTYP,"%s",pclData);
      else sprintf(clFTYP," ");
      if (lgOnDelUrno) {
	sprintf(pcgSqlBuf, "DELETE FROM IFTTAB WHERE UAFT='%ld'",lgOnDelUrno);
	ilRc = getOrPutDBData(START|COMMIT);
	ilRc = insIFTEntry(pcgItemList,legLine,"INSERTING");
      }
      else ilRc = RC_NODATA;
      if (ilRc == RC_NODATA) {
	/* insert record */
	ilRc = insIFTEntry(pcgItemList,legLine,"INSERTING");
      }

      if (igHaveAF1Data) { 
	ilRc = getFreeUrno(clAF1Urno);
	if (ilRc == RC_SUCCESS) insertAF1Data(atol(clAF1Urno), atol(clUrno));
	else {
	  dbg(TRACE,"<HandleInsOrUpd> Fatal -- unable to get new AF1TAB URNO");
	  return RC_FAIL;
	}
      }
    }
    else {
      dbg(TRACE,"<HandleInsOrUpd> Fatal -- unable to get new URNO");
      return RC_FAIL;
    }
    if (igIfType == ELSE) {
      pclRoute = getItemDataByName(pcgItemList, "ROUT", 0);
      lgUrno = atol(clUrno);
      if (pclRoute != NULL) {
	strcat(pcgAftFields, ",ROUT");
	strcat(pcgAftValues, ",");
	strcat(pcgAftValues, pclRoute);
      }
    }
  }
  else if(!strcmp(pcgActCmd, "UFR")) {
    sprintf(clUrno,"0");
    putItemDataByName(pcgItemList,"URNO", clUrno, legLine, IS_INVALID);
    ilRc = createAFTSelection(pcgAftFields,pcgAftValues, pcgItemList, TRUE, legLine);
    if (ilRc != RC_SUCCESS) return RC_FAIL;
    if (igIfType == ELSE) {
      pclRoute = getItemDataByName(pcgItemList, "ROUT", 0);
      if (pclRoute != NULL) {
	strcat(pcgAftFields, ",ROUT");
	strcat(pcgAftValues, ",");
	strcat(pcgAftValues, pclRoute);
      }
    }
    if (igHaveAF1Data) {
    /*
    ** Check if AF1TAB entry exists 
    ** insert if not
    */
      sprintf(pcgSqlBuf, "SELECT URNO FROM AF1TAB WHERE FLNU=%ld", lgUrno);
      ilRc = getOrPutDBData(START);
      if (ilRc == RC_NODATA) {
	ilRc = getFreeUrno(clAF1Urno);
	if (ilRc == RC_SUCCESS) insertAF1Data(atol(clAF1Urno), lgUrno);      }
      else {
	ilIsAf1Upd = TRUE;
	updateAF1Data(lgUrno);
      }
    }
    /*
    ** Special NFL/NOP/NFL sequence, which reinstantiates a
    ** cancelled flight
    ** Need to have a reminder until flight is in AFT
    */
    pclData = getItemDataByName(pcgItemList, "FTYP", legLine);
    if (pclData != NULL) {
      sprintf(clRqId,"%s",pcgTwStart);
      ConvertClientStringToDb(clRqId);
      sprintf(pcgSqlBuf, "UPDATE IFTTAB SET FTYP='%s',RQID='%s',STAT='INSERTING' WHERE UAFT=%ld",pclData,clRqId,lgUrno);
      ilRc = getOrPutDBData(START|COMMIT);
      /*
      ** This is update so we already have an entry in AFT
      ** If this flight was created during recover and the update above didn't work,
      ** the flight has to be in AFT!
      ** This is however not sufficient, because the AFT flight doesn't have 
      ** the new FTYP until the request was performered.
      ** So we have to remember the X state in IFTTAB until the request was ackknowlegded.
      */
      if ((ilRc == RC_NODATA) && (!strcmp(pclData,"X"))) { /*** insert if missing and FTYP='X' ***/
	dbg(DEBUG,"<HandleInsOrUpd> No pending record for update of FTYP <%s>",pclData);
	/** insert record **/
	sprintf(clUrno,"%ld",lgUrno);
	putItemDataByName(pcgItemList,"URNO", clUrno, legLine, IS_VALID);
	ilRc = insIFTEntry(pcgItemList,legLine,"INSERTING");
      }
      else if ((ilRc != RC_SUCCESS) && (!strcmp(pclData,"X"))) { /* this is nasty because I am unable to remember a pending 'X' */
	dbg(TRACE,"<HandleInsOrUpd> There was a serious problem when trying to perform <%s>",pcgSqlBuf);
      }
    }
  }
  
  dbg(DEBUG,"<HandleInsOrUpd> Sending command: <%s> Fields: <%s> Data: <%s>", pcgActCmd, pcgAftFields, pcgAftValues); 

  if (igNBC == TRUE) { /* supress broadcasts */
    sprintf(pclTwStart, ".NBC.,");
    strcat(pclTwStart, pcgTwStart);
  }
  else strcpy(pclTwStart, pcgTwStart);

  CheckEstimatedTime(pcgAftFields, pcgAftValues, pcgSelection);
  if (igIsNonSched == 0)
  {
    ilRc = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
		       pcgActCmd, "AFTTAB", pcgSelection,
		       /*		       pcgAftFields, pcgAftValues, "", ilPrio, ACK_ON_ERR) ;*/
		       pcgAftFields, pcgAftValues, "", ilPrio, NETOUT_NO_ACK) ;
  }
  else
  {
    /*
        Hack to user and workstation name to allow special ELSE handling of
        non-scheduled flights by avoiding the use of a "user" username. 
    */
    ilRc = SendCedaEvent(ilReceiver, 0, "ElseAdmin", "ElseUser", pclTwStart, pcgTwEnd,
                       pcgActCmd, "AFTTAB", pcgSelection,
                       pcgAftFields, pcgAftValues, "", ilPrio, NETOUT_NO_ACK) ; 

  }

  dbg(DEBUG,"<HandleInsOrUpd> SendCedaEvent rtc <%d>",ilRc);
  if (ilRc != RC_SUCCESS && ilRc != QUE_E_INACTIVE) {
    if (!strcmp(pcgActCmd, "IFR") || !strcmp(pcgActCmd, "DFR")) {
      /* remove pending request if send failed */
      sprintf(clRqId,"%s",pcgTwStart);
      ConvertClientStringToDb(clRqId);
      sprintf(pcgSqlBuf,"DELETE FROM IFTTAB WHERE RQID='%s'",clRqId);
      ilRc = getOrPutDBData(START|COMMIT);
    }
    dbg(TRACE,"<HandleInsOrUpd> Unable to send ceda event <%d>", ilRc);
    return RC_FAIL;
  }
  updRequest(pcgTwStart,ilIsAf1Upd);
  dbg(DEBUG,"<HandleInsOrUpd> Sent ID: <%s>",pcgTwStart);
  return ilRc;
}
/******************************************************************************/
/* insertAF1Data                                                              */
/*                                                                            */
/* Insert a new record into AF1TAB                                            */
/*                                                                            */
/* Input:                                                                     */
/*        plAF1Urno -- AF1TAB Urno                                            */
/*        plAFTUrno -- AFTTAB Urno                                            */
/*                                                                            */
/* Record is inserted with status 'INSERTING' and will be set to 'OK' after   */
/* successful insertion from flight is confirmed.                             */
/*                                                                            */
/******************************************************************************/
static void insertAF1Data(long plAF1Urno, long plAFTUrno)
{
  int  ilRc;
  char *pclData;
  char clFCA1[15];
  char clFCA2[15];
  char clCDAT[15];
  char clUser[33];
  char clSTAT[17];

  pclData = getItemDataByName(pcgItemList, "FCA1", 0);
  if (pclData == NULL) sprintf(clFCA1, " ");
  else sprintf(clFCA1, "%s", pclData);

  pclData = getItemDataByName(pcgItemList, "FCA2", 0);
  if (pclData == NULL) sprintf(clFCA2, " ");
  else sprintf(clFCA2, "%s", pclData);

  GetServerTimeStamp("UTC", 1, 0, clCDAT);

  if (igIfType == CSK) sprintf(clUser, "imphdl_csked");
  else if (igIfType == ELSE) sprintf(clUser, "imphdl_else");
  else sprintf(clUser, "imphdl");

  sprintf(clSTAT, "INSERTING");

  sprintf(pcgSqlBuf, "INSERT INTO AF1TAB (URNO,FLNU,FCA1,FCA2,CDAT,USEC,STAT) VALUES('%ld','%ld','%s','%s','%s','%s','%s')",
	  plAF1Urno, plAFTUrno, clFCA1, clFCA2, clCDAT, clUser, clSTAT);

  ilRc = getOrPutDBData(START|COMMIT);
  if (ilRc != RC_SUCCESS) dbg(TRACE,"<insertAF1Data> Unable to perform insert <%s>", pcgSqlBuf);
}
/******************************************************************************/
/* updateAF1Data                                                              */
/*                                                                            */
/* Update a record in AF1TAB                                                  */
/*                                                                            */
/* Input:                                                                     */
/*        plAFTUrno -- AFTTAB Urno                                            */
/*                                                                            */
/* Record is updated with status 'INSERTING' and will be set to 'OK' after    */
/* successful insertion from flight is confirmed.                             */
/*                                                                            */
/******************************************************************************/
static void updateAF1Data(long plAFTUrno)
{
  int  ilRc;
  uint ilHaveFCA1 = FALSE;
  uint ilHaveFCA2 = FALSE;
  char *pclData;
  char clFCA1[32];
  char clFCA2[32];
  char clCDAT[15];
  char clConstText[256];
  char clUser[33];

  sprintf(pcgSqlBuf, "UPDATE AF1TAB SET ");
  pclData = getItemDataByName(pcgItemList, "FCA1", 0);
  if (pclData != NULL) {
    sprintf(clFCA1, "FCA1='%s',", pclData);
    ilHaveFCA1 = TRUE;
  }

  pclData = getItemDataByName(pcgItemList, "FCA2", 0);
  if (pclData != NULL) {
    sprintf(clFCA2, "FCA2='%s',", pclData);
    ilHaveFCA2 = TRUE;
  }
    
  GetServerTimeStamp("UTC", 1, 0, clCDAT);

  if (igIfType == CSK) sprintf(clUser, "imphdl_csked");
  else if (igIfType == ELSE) sprintf(clUser, "imphdl_else");
  else sprintf(clUser, "imphdl");

  sprintf(clConstText, "LSTU='%s',USEU='%s' WHERE FLNU='%ld'", clCDAT, clUser, plAFTUrno);

  if (ilHaveFCA1 || ilHaveFCA2) {
    if (ilHaveFCA1) strcat(pcgSqlBuf, clFCA1);
    if (ilHaveFCA2) strcat(pcgSqlBuf, clFCA2);
    strcat(pcgSqlBuf, clConstText);
    ilRc = getOrPutDBData(START|COMMIT);
    if (ilRc != RC_SUCCESS) dbg(TRACE,"<updateAF1Data> Unable to perform update <%s>", pcgSqlBuf);
  }
  else dbg(DEBUG,"<updateAF1Data> No update required");
}

/******************************************************************************/
/* sendReqToElse                                                              */
/*                                                                            */
/* Notify sender about an AdHoc sequence start.                               */
/*                                                                            */
/******************************************************************************/
int sendReqToElse(char *cpStartTime)
{
  int  ilRc;
  uint ilReceiver=igElseId;
  uint ilPrio=0;
  char pclActCmd[16];
  char pclFields[32];
  char pclData[64];
  char clActTime[15];

  if (igPrioFromIf != 0) {
    ilPrio = igPrioFromIf;
  }
  else ilPrio = igPrio[igIfType];

  GetServerTimeStamp("UTC", 1, 0, clActTime);
  UtcToLocalTimeFixTZ(clActTime);

  sprintf(pclActCmd, "OUT");
  sprintf(pclFields, "");
  sprintf(pclData, "RQ%s%s\n", cpStartTime, clActTime);

  CheckEstimatedTime(pcgAftFields, pcgAftValues, pcgSelection);
  ilRc = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pcgTwStart, pcgTwEnd,
		       pclActCmd, "ELSE", pcgSelection,
		       pclFields, pclData, "", ilPrio, NETOUT_NO_ACK) ;
  return ilRc;
}
/******************************************************************************/
/* writeFKEYcomp                                                              */
/*                                                                            */
/* Try to create the items ALC3 and FLTN (used by flighthandler to create     */
/* its FKEY).                                                                 */ 
/*                                                                            */
/******************************************************************************/
static int writeFKEYcomp(uint legLine)
{
  char *pclFLNO;
  char *pclALC3;
  char *pclFLNS;
  char *ptr=NULL;
  char clALC3[4];
  char clFLTN[4];
  char clFlno[10];
  char clMessage[128];
  uint ilFlnoLen = 0;
  uint ilHaveALC3 = FALSE;
  uint i=0;
  uint lenALC3 = 0;
  uint lenFLTN = 0;
  uint ilALC = FALSE;
  int  ilRC = RC_SUCCESS;
  

  pclFLNO = getItemDataByName(pcgItemList, "FLNO", legLine);
  if (pclFLNO == NULL) {
    dbg (TRACE,"<writeFKEYComp> Fatal -- no valid field data found for FLNO");
    return RC_FAIL;
  }
  else {
    strcpy(clFlno, pclFLNO);
    ilFlnoLen = strlen(clFlno);
  }
  pclALC3 = getItemDataByName(pcgItemList, "ALC3", legLine);
  if (pclALC3 != NULL) {
    ilHaveALC3 = TRUE;
    strncpy(clALC3,pclALC3, 3);
    clALC3[3] = '\0';
  }
  /*
  ** If no ALC3 available - try to eval from FLNO
  */
  if (ilHaveALC3 == FALSE) {
    ptr = pclFLNO;
    while (*ptr != ' ' && *ptr != '\0' && i<3){
      clALC3[i] = *ptr;
      ptr++;i++;
    }
    lenALC3 = i;
    clALC3[lenALC3] = '\0';
    /*
    ** validate ALC from database
    */
    if (lenALC3 == 2) {
      sprintf (pcgSqlBuf, "SELECT DISTINCT ALC3 FROM ALTTAB WHERE ALC2='%s'", clALC3);
      ilALC = TRUE;
    }
    else if (lenALC3 == 3) {
      sprintf (pcgSqlBuf, "SELECT DISTINCT ALC3 FROM ALTTAB WHERE ALC3='%s'", clALC3);
      ilALC = TRUE;
    }
    if (ilALC == TRUE) {
     ilRC = getOrPutDBData(START); 
      if (ilRC == RC_SUCCESS) {
	/* have it! */
	strcpy(clALC3, pcgDataArea);
      }
      else
      {
	dbg (TRACE,"<writeFKEYcomp> Fatal -- Unable to find Airlinecode <%s> in Basic Data", clALC3);
	sprintf(clMessage, "Unable to find Airlinecode <%s> in Basic Data", clALC3);
	alert(clMessage);
	igAlerted = TRUE;
	return RC_FAIL;
      }
    }
    else {
      dbg (TRACE,"<writeFKEYcomp> Fatal -- unable to create ALC3 from FLNO <%s>", pclFLNO);
      return RC_FAIL;
    }
    putItemDataByName(pcgItemList,"ALC3", clALC3, legLine, IS_VALID|IS_AFT);
  }
  /*
  ** fetch FLTN
  */
  i = 0;
  if (ptr == NULL) ptr = pclFLNO + 3; /* if ptr wasn't used to scan ALC -- initialize */
  while (*ptr == ' ' && *ptr != '\0') ptr++; /* skip over possible ' ' */
  while (isdigit((int) *ptr) && *ptr != '\0'){
    clFLTN[i] = *ptr;
    ptr++;i++;
  }
  lenFLTN = i;
  clFLTN[lenFLTN] = '\0';
  putItemDataByName(pcgItemList,"FLTN", clFLTN, legLine, IS_VALID|IS_AFT);
  return ilRC;
}
/******************************************************************************/
/* fetchIFTData                                                               */
/*                                                                            */
/* Input is a SQL statement to fetch URNO,TIFA,TIFD from DB                   */
/* Resulting data is written to global and internal data                      */
/*                                                                            */
/* Input:                                                                     */
/*        pcpSqlBuffer -- Buffer pointing to the SQL statement to perform     */
/*                                                                            */
/* Return:                                                                    */
/*         RC_SUCCESS                                                         */
/*         RC_NOTFOUND     No matches                                         */
/*         RC_NOTUNIQUE    Multiple matches                                   */
/*                                                                            */
/******************************************************************************/
static int fetchIFTData(char *pcpSqlBuffer,char *pcpStatus,char *pcpActor,uint legLine)
{
  int ilGetRc;
  int ilRc = RC_FAIL;
  short slFkt, slCursor;
  char clDataArea[256];
  char *pclData;

  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcpSqlBuffer, clDataArea);
  if (ilGetRc == DB_SUCCESS) {
    BuildItemBuffer(clDataArea, NULL, 3, ",");
      pclData = strtok(clDataArea, ",");
      if (pclData == NULL) {
	dbg(TRACE,"<fetchIFTData> fatal -- unable to fetch UAFT");
	close_my_cursor (&slCursor);
	return RC_FAIL;
      }
      lgUrno = atol(pclData);
      putItemDataByName(pcgItemList,"URNO",pclData, legLine, IS_VALID|IS_AFT);
      pclData = strtok(NULL, ",");
      if (pclData == NULL) {
	dbg(TRACE,"<fetchIFTData> fatal -- unable to fetch STAT");
	close_my_cursor (&slCursor);
	return RC_FAIL;
      }
      sprintf(pcpStatus,"%s",pclData);
      pclData = strtok(NULL, ",");
      if (pclData == NULL) {
	dbg(TRACE,"<fetchIFTData> fatal -- unable to fetch Actor");
	close_my_cursor (&slCursor);
	return RC_FAIL;
      }
      sprintf(pcpActor,"%s",pclData);
      dbg(DEBUG,"<fetchIFTData>SQL Performed: <%s> Result: Flight found with status <%s>",pcpSqlBuffer,pcpStatus);
      ilRc = RC_SUCCESS;
  }
  else {
    dbg(DEBUG,"<fetchIFTData>SQL Performed: <%s> Result: No flight found",pcpSqlBuffer);
    putItemDataByName(pcgItemList,"URNO","0",legLine, IS_VALID|IS_AFT);
    ilRc = RC_NOTFOUND;
  }
  close_my_cursor (&slCursor);
  return ilRc;
}
int writeReport(ulong lpUrno, uint ipPrintHeader, uint ipFlushBuffer)
{
  static FILE   *reportfd = NULL;
  int  ilRC;
  char *pclData;
  char clTimeStamp[15];
  char clFLNO[10];
  char clFLDA[9];
  char clSTOA[15];
  char clSTOD[15];
  char clORG3[4];
  char clDES3[4];
  char clADID[2];
  char clCDAT[15];
  char clLSTU[15];
  char clUSEC[33];
  char clUSEU[33];
  char clFileName[128];

  GetServerTimeStamp("UTC", 1, 0, clTimeStamp);
  UtcToLocalTimeFixTZ(clTimeStamp);

  if (reportfd == NULL) {
    sprintf(clFileName,"/ceda/debug/duplicate_flights_%s.rep",clTimeStamp);
    reportfd = fopen(clFileName, "w");
    fprintf(reportfd, "Report of duplicate flights\n");
  }
  sprintf(pcgSqlBuf,"SELECT FLNO,FLDA,STOD,STOA,ORG3,DES3,ADID,CDAT,LSTU,USEC,USEU FROM AFTTAB WHERE URNO='%ld'",lpUrno);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_SUCCESS) {
    BuildItemBuffer(pcgDataArea,NULL,12,",");
    pclData = strtok(pcgDataArea, ",");
    if (pclData == 0) {
      dbg(TRACE,"<writeReport> No FLNO for URNO <%ld> -- fatal",lpUrno);
      return RC_FAIL;
    }
    else sprintf(clFLNO,pclData);

    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clFLDA,"");
    else sprintf(clFLDA,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clSTOD,"");
    else sprintf(clSTOD,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clSTOA,"");
    else sprintf(clSTOA,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clORG3,"");
    else sprintf(clORG3,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clDES3,"");
    else sprintf(clDES3,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clADID,"");
    else sprintf(clADID,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clCDAT,"");
    else sprintf(clCDAT,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clLSTU,"");
    else sprintf(clLSTU,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clUSEC,"");
    else sprintf(clUSEC,pclData);
    pclData = strtok(NULL, ",");
    if (pclData == 0) sprintf(clUSEU,"");
    else sprintf(clUSEU,pclData);

    if (ipPrintHeader) fprintf(reportfd,"\n                FLNO      FLDA     STOD           STOA           ORG3  DES3  ADID  CDAT           LSTU           USEC             USEU             URNO");
    /*                                   1234567890123456123456789 12345678 12345678901234 12345678901234 12345 12345 12345 12345678901234 12345678901234 1234567890123456 12345678901234567890*/
    fprintf(reportfd, "\n%s: %-9.9s %-8.8s %-14.14s %-14.14s %-5.5s %-5.5s %-5.5s %-14.14s %-14.14s %-16.16s %-16.16s %ld",
	    clTimeStamp,clFLNO,clFLDA,clSTOD,clSTOA,clORG3,clDES3,clADID,clCDAT,clLSTU,clUSEC,clUSEU,lpUrno);
    if (ipFlushBuffer) fflush(reportfd);
  }    
} 
/******************************************************************************/
/* fetchFlightData                                                            */
/*                                                                            */
/* Input is a SQL statement to fetch URNO,TIFA,TIFD from DB                   */
/* Resulting data is written to global and internal data                      */
/*                                                                            */
/* Input:                                                                     */
/*        pcpSqlBuffer -- Buffer pointing to the SQL statement to perform     */
/*                                                                            */
/* Return:                                                                    */
/*         RC_SUCCESS                                                         */
/*         RC_NOTFOUND     No matches                                         */
/*         RC_NOTUNIQUE    Multiple matches                                   */
/*                                                                            */
/******************************************************************************/
static int fetchFlightData(char *pcpSqlBuffer,uint legLine)
{
  int ilGetRc;
  static FILE   *reportfd = NULL;
  short slFkt, slCursor;
  char clDataArea[4096];
  char clSqlBuffer[4096];
  char clReportBuffer[4096];
  char clUrno[32];
  char *pclData;
  uint ilFirstMatch = FALSE;
  uint ilIsNotUnique = FALSE;
  uint ilItemCount = 0;
  long llUrno[32];
  uint ilCount=0;
  uint ilPrintHeader=FALSE;
  uint ilFlushBuffer=FALSE;
  uint i;

  strcpy(clSqlBuffer,pcpSqlBuffer);
  ilItemCount = 1;
  if (strtok(clSqlBuffer, ",") != NULL) {
    while(strtok(NULL, ",") != NULL) 
      ilItemCount++;
  }

  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pcpSqlBuffer, clDataArea)) == DB_SUCCESS) {
    if (ilGetRc == DB_SUCCESS) {
      BuildItemBuffer(clDataArea, NULL, ilItemCount, ",");
      strcpy(clReportBuffer,clDataArea);
      pclData = strtok(clDataArea, ",");
      sprintf(clUrno,"%s",pclData);
      if (ilFirstMatch == FALSE) {
	ilFirstMatch = TRUE;
	lgUrno = atol(clUrno);
	putItemDataByName(pcgItemList, "URNO", clUrno, legLine, IS_VALID|IS_AFT);
	llUrno[ilCount] = lgUrno;
	ilCount++;
      }
      else {
	llUrno[ilCount] = atol(clUrno);
	ilCount++;
	ilIsNotUnique = TRUE;
      }
      dbg(DEBUG,"<fetchFlightData> SQL statement <%s> found data <%s>",pcpSqlBuffer,clReportBuffer);
      slFkt = NEXT;
    }
  }
  close_my_cursor (&slCursor);
  
  if (ilIsNotUnique) {
    for (i=0;i<ilCount;i++) {
      if (i==0) ilPrintHeader = TRUE;
      else ilPrintHeader = FALSE;
      if (i==ilCount-1) ilFlushBuffer =TRUE;
      writeReport(llUrno[i],ilPrintHeader,ilFlushBuffer);
    }
    dbg(TRACE,"<fetchFlightData>SQL Performed: <%s> Result: Flight not unique",pcpSqlBuffer);
    return RC_NOTUNIQUE;
  }
  if (ilFirstMatch == FALSE) {
    dbg(TRACE,"<fetchFlightData>SQL Performed: <%s> Result: No flight found",pcpSqlBuffer);
    putItemDataByName(pcgItemList,"URNO","0",legLine, IS_VALID|IS_AFT);
    return RC_NOTFOUND;
  }
  else {
    dbg(TRACE,"<fetchFlightData>SQL Performed: <%s> Result: Unique flight found",pcpSqlBuffer);
    return RC_SUCCESS;
  }
}
/******************************************************************************/
/* identifyFlightRecord                                                       */
/*                                                                            */
/* Identify the corresponding flight record that would fit our input data.    */
/*                                                                            */
/* Input:                                                                     */
/*        pcTimeInfo -- contains time info if period record is handled        */
/*                      NULL else                                             */
/*        legLine    -- the data entry                                        */ 
/*                                                                            */
/* Return:                                                                    */
/*        RC_SUCCES if record exists                                          */
/*        RC_FAIL if record doesn't exist                                     */
/*                                                                            */
/******************************************************************************/
static int identifyFlightRecord(char *pcpActor, uint legLine)
{
  int ilRC = RC_SUCCESS;
  uint ilIsDeleting = FALSE;
  uint ilIsInserting = FALSE;
  uint ilIsOnDelete = FALSE;
  char clADID[2];
  char clFLNO[10], clCSGN [10];
  char clFLDA[9];
  char clFLDU[9];
  char clSTOD[15];
  char clSTOA[15];
  char clActor[33];
  char clStatus[17];
  char clSqlBuf[256];
  char clMessage[128];
  char clRouteSelection[128];
  char *pclData;
  uint ilFirstMatch = FALSE;

  /*
  ** try to retrieve FLNO,FLDA,ADID 
  */
  if (pcgItemList->RouteType[legLine] == A) {
    strcpy(clADID, "A");
  }
  else if (pcgItemList->RouteType[legLine] == D) {
    strcpy(clADID, "D");
  }

  if (igIsNonSched == 0)
  {
    pclData = getItemDataByName(pcgItemList, "FLNO", legLine);
    if (pclData != NULL) { strcpy(clFLNO,pclData);
    }
    else {
      dbg(TRACE,"<identifyFlightRecord> Fatal -- no FLNO available");
      return RC_FAIL;
    }
  }
  else
  {
    pclData = getItemDataByName(pcgItemList, "CSGN", legLine);
    if (pclData != NULL)
    {
      strcpy (clCSGN, pclData);
    }
    else
    {    
      dbg(TRACE,"<identifyFlightRecord> Fatal -- no CSGN available");
      return RC_FAIL;
    }
  }
 
  if (igIsRTFlight) sprintf(clRouteSelection,"(ADID='%s' OR ADID='B')",clADID);
  else  sprintf(clRouteSelection,"ADID='%s'",clADID);
  /*
  ** Interface specific handling now 
  */
  if (igIfType == ELSE) {
    /*
    ** Use local FLDA from LFLDA
    ** find flight identified by FLNO/FLDA in AFTTAB
    ** SC0002 handle ELSE search
    */
    pclData = getItemDataByName(pcgItemList, "FLDA", legLine);
    if (pclData == NULL) {
      dbg(TRACE,"<identifyFlightRecord> Fatal -- no FLDA for ELSE data");
      return RC_FAIL;
    }
    else strcpy(clFLDA,pclData);
    
    /*
    ** First fetch flight from IFTTAB and retrieve status
	UFIS-244 - skip this for non-sched because IFTTAB has no CSGN field.
    */
    if (igIsNonSched == 0)
    {
      sprintf(clSqlBuf, "SELECT UAFT,STAT,USEC FROM IFTTAB WHERE FLNO='%s' AND %s AND FLDA='%s'", 
	    clFLNO,clRouteSelection,clFLDA);  
      ilRC = fetchIFTData(clSqlBuf,clStatus,clActor,legLine);
      if (ilRC == RC_SUCCESS) {
        if (pcpActor != NULL) strcpy(pcpActor,clActor);
        if (!strncmp(clStatus,"DELETING",8)) ilIsDeleting = TRUE;
        else if (!strncmp(clStatus,"INSERTING",9)) ilIsInserting = TRUE;
        else if (!strncmp(clStatus,"ONDELETE",8)) ilIsOnDelete = TRUE;
      }
      if (ilRC == RC_SUCCESS && ilIsDeleting) return RC_NODATA; /* DFR request was sent */
      if (ilRC == RC_SUCCESS && ilIsOnDelete) { /* This would only happen for SQ/MI flights */
        return RC_ONDELETE;
      }
    if (ilRC == RC_SUCCESS && ilIsInserting) return RC_SUCCESS;
    }

    /*
    ** No pending flights were found, second stage
    ** search AFT
    */

    /*
	UFIS-244 Non Sched from ELSE. Also added ADID to WHERE clause because an arrival
	& departure might have same FLNO or CSGN. (Per CGMS imphdl).
    */
    if (igIsNonSched == 0)
    {
      if (cgLogAdditionalFields == NULL) 
        sprintf(clSqlBuf,
	  "SELECT URNO FROM AFTTAB WHERE FLNO='%s' AND %s AND FLDA='%s' AND ADID = '%s'", 
	  clFLNO,clRouteSelection,clFLDA, clADID);  
      else 
        sprintf(clSqlBuf,
	  "SELECT URNO,%s FROM AFTTAB WHERE FLNO='%s' AND %s AND FLDA='%s' AND ADID = '%s'",
	  cgLogAdditionalFields,clFLNO,clRouteSelection,clFLDA, clADID);
    }
    else
    {
      pclData = getItemDataByName(pcgItemList, "CSGN", legLine);
      if (pclData != NULL)
      {
        strcpy(clCSGN, pclData);
      }

      if (cgLogAdditionalFields == NULL) 
        sprintf(clSqlBuf,
	    "SELECT URNO FROM AFTTAB WHERE CSGN='%s' AND %s AND FLDA='%s' AND ADID = '%s'", 
	      clCSGN,clRouteSelection,clFLDA, clADID);  
    else 
        sprintf(clSqlBuf,
	    "SELECT URNO,%s FROM AFTTAB WHERE CSGN='%s' AND %s AND FLDA='%s' AND ADID = '%s'",
	      cgLogAdditionalFields,clCSGN,clRouteSelection,clFLDA, clADID); 
    }

    ilRC = fetchFlightData(clSqlBuf,legLine);
    if (ilRC == RC_SUCCESS) return ilRC;
    if (ilRC == RC_NOTUNIQUE) {
      if (igIsNonSched == 0)
        sprintf(clMessage, "Flight not unique in AFTTAB for FLNO: <%s> FLDA: <%s> ADID: <%s>",clFLNO,clFLDA,clADID);
      else
        sprintf(clMessage, "Flight not unique in AFTTAB for CSGN: <%s> FLDA: <%s> ADID: <%s>",clCSGN,clFLDA,clADID);
      alert(clMessage);
      return RC_FAIL;
    }
    /*
    ** maybe pending insert
    */
    if (ilIsInserting) return RC_SUCCESS;
    /*
    ** check if we have a related suffix flight to
    ** this (non suffix) flight
    */
    /* UFIS-244 - can skip this for non-sched flights */
    if (igIsNonSched == 0)
    {
      if (cgLogAdditionalFields == NULL) 
        sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE FLNO like '%s%%' AND ADID='B' AND FLDA='%s'", 
	      clFLNO,clFLDA);
      else 
        sprintf(clSqlBuf, "SELECT URNO,%s FROM AFTTAB WHERE FLNO like '%s%%' AND ADID='B' AND FLDA='%s'",
	      cgLogAdditionalFields,clFLNO,clFLDA);
      ilRC = fetchFlightData(clSqlBuf,legLine);
      if (ilRC == RC_SUCCESS) return RC_HASSUFFIX;
      else return RC_NODATA;
    }
    return RC_NODATA; /* UFIS-244 : assume this is the last possible outcome. Copied from final
			 return code from last test for suffixed flight */
  }
  /*
  ** Handle CSKED
  ** Use UTC FLDA
  ** SC0001
  */
  else if (igIfType == CSK) {
    pclData = getItemDataByName(pcgItemList, "FLDU", legLine);
    if (pclData == NULL) {
      dbg(TRACE,"<identifyFlightRecord> Fatal -- no FLDU for CSKED data");
      return RC_FAIL;
    }
    else strcpy(clFLDU,pclData);
    /*
    ** Now first check for MIXED UPDATE pendings
    ** next search will succeed, the flight has mixed update pending
    */
    sprintf(clSqlBuf, "SELECT UAFT,STAT,USEC FROM IFTTAB WHERE FLNO='%s' AND %s AND FLDU='%8.8s'", 
	    clFLNO,clRouteSelection,clFLDU);  
    ilRC = fetchIFTData(clSqlBuf,clStatus,clActor,legLine);
    if (ilRC == RC_SUCCESS) {
      if (pcpActor != NULL) strcpy(pcpActor,clActor);
      if (!strncmp(clStatus,"DELETING",8)) ilIsDeleting = TRUE;
      else if (!strncmp(clStatus,"INSERTING",9)) ilIsInserting = TRUE;
      else if (!strncmp(clStatus,"ONDELETE",8)) ilIsOnDelete = TRUE;
    }
    if (ilRC == RC_SUCCESS && ilIsOnDelete) return RC_ONDELETE;
    /*
    ** Now check for pending deletes if the 
    ** status is 'DELETING' handle flight as deleted!
    */
    if (ilRC == RC_SUCCESS && ilIsDeleting) return RC_NODATA;
    /*
    ** check for pending inserts if status is 'INSERTING'
    ** handle flight as found!
    */
    if (ilRC == RC_SUCCESS && ilIsInserting) return RC_SUCCESS;
    /*
    ** No pending flights have been found, stage 2:
    ** Search in AFTTAB
    */
    
   dbg(TRACE,"<identifyFlightRecord> Fatal -- no CSGN available");
    
    if (cgLogAdditionalFields == NULL)
      sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE FLNO='%s' AND %s AND STOD like '%8.8s%%'", 
	      clFLNO,clRouteSelection,clFLDU);  
    else 
      sprintf(clSqlBuf, "SELECT URNO,%s FROM AFTTAB WHERE FLNO='%s' AND %s AND STOD like '8.8%s%%'",
	      cgLogAdditionalFields,clFLNO,clRouteSelection,clFLDU);
    ilRC = fetchFlightData(clSqlBuf,legLine);
    if (ilRC == RC_SUCCESS) return ilRC;
    else if (ilRC == RC_NOTUNIQUE) {
/* Frank v4.12*/
/* Frank v4.12a*/
			memset(clSqlBuf,0,sizeof(clSqlBuf));
			
			dbg(TRACE,"<identifyFlightRecord> clFLDAFromWrapCsked<%d>",clFLDAFromWrapCsked);
			
			/*
    	if (cgLogAdditionalFields == NULL)
      	sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE FLNO='%s' AND %s AND STOD like '%10.10s%%'", 
	      	clFLNO,clRouteSelection,clFLDU);  
    	else 
      	sprintf(clSqlBuf, "SELECT URNO,%s FROM AFTTAB WHERE FLNO='%s' AND %s AND STOD like '%10.10s%%'",
	      	cgLogAdditionalFields,clFLNO,clRouteSelection,clFLDU);
	    */
	    if (cgLogAdditionalFields == NULL)
      	sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE FLNO='%s' AND %s AND STOD like '%10.10s%%'", 
	      	clFLNO,clRouteSelection,clFLDAFromWrapCsked);  
    	else 
      	sprintf(clSqlBuf, "SELECT URNO,%s FROM AFTTAB WHERE FLNO='%s' AND %s AND STOD like '%10.10s%%'",
	      	cgLogAdditionalFields,clFLNO,clRouteSelection,clFLDAFromWrapCsked);
	    
	    memset(clFLDAFromWrapCsked,0,sizeof(clFLDAFromWrapCsked));
	    ilRC = fetchFlightData(clSqlBuf,legLine); 
			if (ilRC == RC_SUCCESS) 
				return ilRC;
			else if (ilRC == RC_NOTUNIQUE) 
			{
      	sprintf(clMessage, "Flight not unique in AFTTAB for FLNO: <%s> FLDU: <%s> ADID: <%s>",clFLNO,clFLDU,clADID);
      	alert(clMessage);
      	return RC_FAIL;
			}
/* Frank v4.12a*/
/* Frank v4.12*/
    }
    /*
    ** Return SUCCESS on pending insert
    */
    if (ilIsInserting) return RC_SUCCESS;
    /*
    ** If arrival try this 
    */
    if (pcgItemList->RouteType[legLine] == A) {
      pclData = getItemDataByName(pcgItemList, "STOA", legLine);
      if (pclData != NULL) {
	if (cgLogAdditionalFields == NULL)
	  sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE FLNO='%s' AND %s AND STOA LIKE '%8.8s%%'",
		  clFLNO,clRouteSelection,pclData);
	else
	  sprintf(clSqlBuf, "SELECT URNO,%s FROM AFTTAB WHERE FLNO='%s' AND %s AND STOA LIKE '%8.8s%%'",
		  cgLogAdditionalFields,clFLNO,clRouteSelection,pclData);

	ilRC = fetchFlightData(clSqlBuf,legLine);
	if (ilRC == RC_SUCCESS) return ilRC;
	else return RC_NODATA;
      }
      else {
	dbg(TRACE,"Search for appropriate flights not yet implemented");
	return RC_NOTIMPL;
      }
    }
    else if (ilRC == RC_NOTUNIQUE) {
/* Frank v1.42 */
/* Frank v1.42a */
			/*
			if (cgLogAdditionalFields == NULL)
	  		sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE FLNO='%s' AND %s AND STOA LIKE '%10.10s%%'",
		  		clFLNO,clRouteSelection,pclData);
			else
	  		sprintf(clSqlBuf, "SELECT URNO,%s FROM AFTTAB WHERE FLNO='%s' AND %s AND STOA LIKE '%10.10s%%'",
		  		cgLogAdditionalFields,clFLNO,clRouteSelection,pclData);
		  */
		  dbg(TRACE,"<identifyFlightRecord> clFLDAFromWrapCsked<%d>",clFLDAFromWrapCsked);
		  
		  if (cgLogAdditionalFields == NULL)
	  		sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE FLNO='%s' AND %s AND STOA LIKE '%10.10s%%'",
		  		clFLNO,clRouteSelection,clFLDAFromWrapCsked);
			else
	  		sprintf(clSqlBuf, "SELECT URNO,%s FROM AFTTAB WHERE FLNO='%s' AND %s AND STOA LIKE '%10.10s%%'",
		  		cgLogAdditionalFields,clFLNO,clRouteSelection,clFLDAFromWrapCsked);
		  
		  memset(clFLDAFromWrapCsked,0,sizeof(clFLDAFromWrapCsked));
		  ilRC = fetchFlightData(clSqlBuf,legLine);
		  if (ilRC == RC_SUCCESS) 
				return ilRC;
			else if (ilRC == RC_NOTUNIQUE) 
			{
      	sprintf(clMessage, "Flight not unique in AFTTAB for FLNO: <%s> FLDU: <%s> ADID: <%s>",clFLNO,clFLDU,clADID);
      	alert(clMessage);
      	return RC_FAIL;
			}
/* Frank v1.42a */
/* Frank v1.42 */
    }
    else {
      return RC_NODATA;
    }
  }//csk ends
  else if (igIfType == SQL) {
    /*
    ** Use local FLDA from LFLDA
    ** find flight identified by FLNO/FLDA in AFTTAB
    */
    
    /*
    ** First fetch flight from IFTTAB and retrieve status
    */
    if (!strcmp(clADID,"D")) {
      pclData = getItemDataByName(pcgItemList, "STOD", legLine);
      if (pclData != NULL) {
	strcpy(clSTOD,pclData);
      }
      else {
	dbg(TRACE,"<identifyFlightRecord> IF:SQL No STOD available -- unable to find flight");
	return RC_FAIL;
      }
      sprintf(cgSqlString,"FLNO='%s' AND %s AND STOD='%s'", 
	      clFLNO,clRouteSelection,clSTOD); 
    }
    else {
      pclData = getItemDataByName(pcgItemList, "STOA", legLine);
      if (pclData != NULL) {
	strcpy(clSTOA,pclData);
      }
      else {
	dbg(TRACE,"<identifyFlightRecord> IF:SQL No STOD available -- unable to find flight");
	return RC_FAIL;
      }
      sprintf(cgSqlString, "FLNO='%s' AND %s AND STOA='%s'", 
	      clFLNO,clRouteSelection,clSTOA);  
    }

    sprintf(clSqlBuf, "SELECT UAFT,STAT FROM IFTTAB WHERE %s", 
	    cgSqlString);

    ilRC = fetchIFTData(clSqlBuf,clStatus,clActor,legLine);
    if (ilRC == RC_SUCCESS) {
      if (!strncmp(clStatus,"DELETING",8)) ilIsDeleting = TRUE;
      else if (!strncmp(clStatus,"INSERTING",9)) ilIsInserting = TRUE;
      else if (!strncmp(clStatus,"ONDELETE",8)) ilIsOnDelete = TRUE;
    }
    if (ilRC == RC_SUCCESS && ilIsDeleting) return RC_NODATA; /* DFR request was sent */
    if (ilRC == RC_SUCCESS && ilIsOnDelete) { /* This would only happen for SQ/MI flights */
      return RC_ONDELETE;
    }
    if (ilRC == RC_SUCCESS && ilIsInserting) return RC_SUCCESS;
    /*
    ** No pending flights were found, second stage
    ** search AFT
    */
    sprintf(clSqlBuf, "SELECT URNO FROM AFTTAB WHERE %s", 
	    cgSqlString);
    ilRC = fetchFlightData(clSqlBuf,legLine);
    if (ilRC == RC_SUCCESS) return ilRC;
    if (ilRC == RC_NOTUNIQUE) {
      sprintf(clMessage, "Flight not unique in AFTTAB for Selection: <%s>",cgSqlString);
      alert(clMessage);
      return RC_FAIL;
    }
    /*
    ** maybe pending insert
    */
    if (ilIsInserting) return RC_SUCCESS;
  }
}


typedef struct {
  char *from;
  char *to;
} TransField;

static TransField TransFieldList[] = { 
  {"ETOA", "ETAA"},
  {"ETOD", "ETDA"},
  {"ONBL", "ONBS"},
  {"OFBL", "OFBS"},
  {"AIRB", "AIRA"},
  {"LAND", "LNDA"},
  { NULL,   NULL }
};

char *translateFieldName(char *pcpFromField)
{
  uint entry=0;

  while(TransFieldList[entry].from != NULL) {
    if (!strcmp(TransFieldList[entry].from, pcpFromField)) return TransFieldList[entry].to;
    entry++;
  }
  return pcpFromField;
}

/******************************************************************************/
/* createAFTSelection                                                         */
/*                                                                            */
/* Creates a ',' separated list of fieldnames. An enhanced version would      */
/* check if the field is a valid AFTTAB field.                                */
/*                                                                            */
/* Input:                                                                     */
/*       FieldBuffer -- where fields will be written to                       */
/*       DataBuffer  -- where data will be written to                         */
/*       fieldList -- the list to retrieve the fieldnames from                */
/*                                                                            */
/* Returns:                                                                   */
/*         a nicely filled buffer of AFTTAB items ready for building a SELECT */
/*         if Both is TRUE an additional DataBuffer is provided.              */
/*                                                                            */
/******************************************************************************/
static int createAFTSelection(char *FieldBuffer, char *DataBuffer, ListPtr fieldList, uint Both, uint legLine)
{
  ListPtr pclField;
  uint ilStatus = 0;
  uint ilORG3 = FALSE;
  uint ilORG4 = FALSE;
  uint ilDES3 = FALSE;
  uint ilDES4 = FALSE;
  uint ilFirst = TRUE;
  uint ilHaveSTOD = FALSE;
  uint ilHaveFLDA = FALSE;
  uint ilADID;
  uint ilEntry = 0;
  uint ilHaveSto = FALSE;
  uint ilTimeZone = UTC;
  uint ilDeparture = FALSE;
  int  ilRC;
  char *pclFieldEl;
  char clTimeBuf[15];
  char *clUtcTime;
  char *clField;
  char *pclSTOD;
  char *pclGate;
  char *pclFLDA;
  char *pclData;
  char clRefTime[9];
  char clTimField[12];
  char clAdidApp[2];
  char clSTO[15];
  char clSTOG[15];
  char clFLDA[15];
  char clMessage[128];

  ilADID = pcgItemList->RouteType[legLine];

  strcpy(clTimField, "");

  if (ilADID == D) {
    sprintf(clTimField, "STOD");
    sprintf(clAdidApp, "D");
    ilDeparture = TRUE;
  }
  else if (ilADID == A) {
    sprintf(clTimField, "STOA");
    sprintf(clAdidApp, "A");
  }
  pclData = getItemDataByName(pcgItemList, clTimField, legLine);
  if (pclData != NULL) {
    if (strlen(pclData) >= 12) {
     sprintf(clSTO, "%s", pclData);
     ilHaveSto = TRUE;
    }
  }
  if (ilHaveSto == FALSE) {
    if (!strcmp(pcgActCmd, "UFR") && strcmp(clTimField, "")) {
      sprintf(pcgSqlBuf, "SELECT %s FROM AFTTAB WHERE URNO=%ld", clTimField, lgUrno);
      ilRC = getOrPutDBData(START);
      if (ilRC == RC_NODATA) { /* pending insert? */
	sprintf(pcgSqlBuf, "SELECT %s FROM IFTTAB WHERE UAFT=%ld AND STAT='INSERTING'", clTimField, lgUrno);
	ilRC = getOrPutDBData(START);
      }
      if (ilRC == RC_SUCCESS) {
        strcpy(clSTO, pcgDataArea);
	if (igTimeFromIf == LOCAL) UtcToLocalTimeFixTZ(clSTO); /* unique representation */
	ilTimeZone = UTC;
        ilHaveSto = TRUE;
      }
      else  dbg(TRACE, "<createAFTSelection> Statement failed <%s>", pcgSqlBuf);
    }
    else if (!strcmp(pcgActCmd, "IFR")) { /* fatal for inserts! */
      sprintf(clMessage, "Unable to insert flight missing <%s> in Basic Data", clTimField);
      alert(clMessage);
      igAlerted = TRUE;
      return RC_FAIL;
    }
  }
  
  if (ilHaveSto == FALSE) dbg(TRACE, "<createAFTSelection> Unable to complete time data due to missing STO%s", clAdidApp);
  /*
  ** Expand time fields to full cedatime
  ** if required
  */
  strcpy(clSTOG, clSTO);
  ilEntry = 0;
  while (cgUTCTIMEField[ilEntry].FieldName != NULL) {
    if (cgUTCTIMEField[ilEntry].isDateOnly == TRUE && cgUTCTIMEField[ilEntry].ADID == ilADID) {
      if (!strcmp(pcgActCmd, "UFR")) {
	if (ilHaveSto) {
	  ilRC =updateTimeField(cgUTCTIMEField[ilEntry].FieldName,lgUrno,legLine);
	  if (ilRC == RC_FAIL) return RC_FAIL;
	}
      }
      else {
	dbg(TRACE, "<createAFTSelection> Unable to update field <%s> while handling INSERTS", cgUTCTIMEField[ilEntry].FieldName);
      }
    }
    else if (cgUTCTIMEField[ilEntry].isShortField == TRUE && cgUTCTIMEField[ilEntry].ADID == ilADID) {
      pclData = getItemDataByName(pcgItemList, cgUTCTIMEField[ilEntry].FieldName,legLine);
      if (pclData != NULL) {
	if (strcmp(pclData, " ")) { /* no blank fields */
	  if (ilHaveSto == FALSE) {
	    /*
	    ** unable to complete cedatime due to missing STOx
	    */
	    sprintf(clMessage, "Unable to complete ceda time missing <%s> in Basic Data", clTimField);
	    alert(clMessage);
	    igAlerted = TRUE;
	    return RC_FAIL;
	  }
	  else {
	    strcpy(clSTO, clSTOG);
	    ilRC = completeCedaTime(clSTO, pclData, FALSE, ilTimeZone, ilDeparture);
	    if (igTimeFromIf == LOCAL && ilTimeZone == UTC) UtcToLocalTimeFixTZ(clSTO); 
	    if (ilRC != RC_SUCCESS) {
	      /*
	      **putItemDataByName(pcgItemList, cgUTCTIMEField[ilEntry].FieldName, clSTO, legLine, IS_INVALID);
	      **dbg(TRACE, "<createAFTSelection> Invalid calculated time, disabled <%s>", clSTO);
	      */
	      sprintf(clMessage, "Unable to complete ceda time missing <%s> in Basic Data", clTimField);
	      alert(clMessage);
	      igAlerted = TRUE;
	      return RC_FAIL;
	    }
	    else {
	      putItemDataByName(pcgItemList, cgUTCTIMEField[ilEntry].FieldName, clSTO, legLine, 0);
	    }
	  }
	}
      }
    }
    ilEntry++;
  }
  /*
  ** Now fill in appropriate field/data 
  */
  pclField = fieldList;
  while (pclField != NULL) { 
    ilStatus = pclField->dobj[legLine].status;
    clField = translateFieldName(pclField->field);
    if (!strcmp(clField, "FLDA")) ilStatus = 0; /* FLDA field will not be send! */
    else if (!strcmp(clField, "ADID")) ilStatus = 0; /* ADID field will not be send! */
    if (ilStatus & IS_VALID && ilStatus & IS_AFT) {
      if (ilFirst) {
	if (Both == FALSE) ilFirst = FALSE;
	strcpy(FieldBuffer, clField);
      }
      else {
	strcat(FieldBuffer,",");
	strcat(FieldBuffer, clField);
      }
      if (Both == TRUE) {
	if (!ilFirst) {
	  strcat(DataBuffer, ",");
	}
	if (ilStatus & IS_UTCTIME) {
	  strcpy(&clTimeBuf[0],pclField->dobj[legLine].data);
	  if (igTimeFromIf == LOCAL) LocalTimeToUtcFixTZ(clTimeBuf);
	  clUtcTime = clTimeBuf;
	  if (ilFirst) {
	    ilFirst = FALSE;
	    strcpy (DataBuffer, clUtcTime);
	  }
	  else strcat(DataBuffer, clUtcTime);
	}
	else {
	  if (ilFirst) {
	    ilFirst = FALSE;
	    strcpy(DataBuffer, pclField->dobj[legLine].data);
	  }
	  else strcat(DataBuffer, pclField->dobj[legLine].data);
	}
      }
    }
    pclField = pclField->next;
  }

  if (Both == FALSE) return RC_SUCCESS;

  if (!strcmp(pcgActCmd, "IFR")) { /* please modify inserts only  */
    pclFieldEl = getItemDataByName(pcgItemList, "ORG3", legLine);
    if (pclFieldEl != NULL) {
      ilORG3 = TRUE;
    }
    pclFieldEl = getItemDataByName(pcgItemList, "ORG4", legLine);
    if (pclFieldEl != NULL) {
      ilORG4 = TRUE;
    }
    pclFieldEl = getItemDataByName(pcgItemList, "DES3", legLine);
    if (pclFieldEl != NULL) {
      ilDES3 = TRUE;
    }
    pclFieldEl = getItemDataByName(pcgItemList, "DES4", legLine);
    if (pclFieldEl != NULL) {
      ilDES4 = TRUE;
    }
    
    /*  ilADID = pcgItemList->RouteType[legLine]; */
    if (ilORG3 != TRUE && ilORG4 != TRUE) {
      /*
      ** we will assume a local flight 
      */
      if (strlen(cgHopo) == 3) {
	strcat(FieldBuffer, ",ORG3");
      }
      else if (strlen(cgHopo) == 4) {
	strcat(FieldBuffer, ",ORG4");
      }
      strcat(DataBuffer, ",");
      strcat(DataBuffer, cgHopo);
    }
    if (ilDES3 != TRUE && ilDES4 != TRUE) {
      if (strlen(cgHopo) == 3) {
	strcat(FieldBuffer, ",DES3");
      }
      else if (strlen(cgHopo) == 4) {
	strcat(FieldBuffer, ",DES4");
      }
      strcat(DataBuffer, ",");
      strcat(DataBuffer, cgHopo);
    }
  }
  /*
  ** check if FTYP is missing
  ** for an update
  */
  if (!strcmp(pcgActCmd, "IFR")) {
    pclFieldEl = getItemDataByName(pcgItemList, "FTYP", legLine);
    if (pclFieldEl == NULL) {
      /*
      ** the default depends on the interface
      ** at this time we always assume 'S'
      ** should be improved later
      */
      if (strlen(FieldBuffer) == NULL) {
	strcpy(FieldBuffer, "FTYP");
	strcpy(DataBuffer, "S");
      }
      else {
	strcat(FieldBuffer, ",FTYP");
	strcat(DataBuffer, ",S");
      }    
    }
  }
  return RC_SUCCESS;
}
/******************************************************************************/
/* updateTimeField                                                            */
/*                                                                            */
/* Update time part of a field from the DB. If it fails the field will be     */
/* disabled.                                                                  */
/*                                                                            */
/* Input:                                                                     */
/*       pcpField -- pointer to fieldname                                     */
/*       lgUrno   -- URNO of the related flight                               */
/*       ipLegNo  -- entry to internal data array                             */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
static int updateTimeField(char *pcpField, long lpUrno, uint ipLegNo)
{
  char *pclData;
  char clTimeField[15];
  char clDBTimeField[15];
  char clFieldName[16];
  int  ilRC;
  uint ilLen;
  char *pclFieldName;

  pclData = getItemDataByName(pcgItemList,pcpField,ipLegNo);
  if (pclData == NULL) {
    return; /* nothing to do here */
    /*
    dbg(TRACE, "<updateTimeField> Unable to get field <%s> from internal data", pcpField);
    putItemDataByName(pcgItemList, pcpField, "", ipLegNo, IS_INVALID);
    */
  }
  strncpy(clTimeField,pclData,8);
  clTimeField[8] = '\0';
  strcpy(clFieldName,pcpField); /* save for IFTTAB search */
  pclFieldName = translateFieldName(pcpField);
  sprintf(pcgSqlBuf, "SELECT %s FROM AFTTAB WHERE URNO=%ld", pclFieldName, lgUrno);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_NODATA) { /* pending insert? */
    sprintf(pcgSqlBuf, "SELECT %s FROM IFTTAB WHERE UAFT=%ld", clFieldName, lgUrno);
    ilRC = getOrPutDBData(START);
  }
  if (ilRC == RC_SUCCESS) {
    strcpy(clDBTimeField, pcgDataArea);
    /* time from AFTTAB should be valid i.e. */
    ilLen = strlen(clDBTimeField);
    if (ilLen == 14 && clDBTimeField[0] != ' ') {
      strcat(clTimeField,&clDBTimeField[8]);
      if (igTimeFromIf == LOCAL) UtcToLocalTimeFixTZ(clTimeField);
      putItemDataByName(pcgItemList, pcpField, clTimeField, ipLegNo, 0);
    }
    else {
      dbg(TRACE, "<updateTimeField> invalid time for field <%s> <%s>", pcpField, clDBTimeField);
      return RC_FAIL;
    }
  }
  else {
    dbg(TRACE, "<updateTimeField> Unable to get field <%s> from DB", pcpField);
    return RC_FAIL;
    /*putItemDataByName(pcgItemList, pcpField, clTimeField, ipLegNo, IS_INVALID);*/
  }
}
/******************************************************************************/
/* checkGroupOfTimeFieldValues                                                */
/*                                                                            */
/* STOD or STOA are mandatory for insert commands. Time info might be sent in */
/* hhmm format if a valid period info is available.                           */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- the line of data to check                                 */
/*                                                                            */
/* Returns:                                                                   */
/*         nothing (valid data is flagged by HaveTimeInfo)                    */
/*                                                                            */
/******************************************************************************/
static int checkGroupOfTimeFieldValues(uint legLine)
{
  int rtc;
  int ilRC = RC_SUCCESS;
  int ilSumRC = RC_SUCCESS;
  uint ilPeriodValid = FALSE;
  uint ilStod        = FALSE;
  uint ilStoa        = FALSE; 
  uint ilEntry       = 0;
  char *pclStoa;
  char *pclStod;
  char *pclData;
  ListPtr pclLis;

  pcgItemList->timeInfo[legLine].HaveTimeInfo = FALSE;

  if (!strcmp(cgActCmd,"IFR")) {
    rtc = getItemByName(pcgItemList, "STOD", &pclLis);
    if (rtc == RC_SUCCESS) {
      if (pclLis->dobj[legLine].status & IS_VALID) {
	pclStod=pclLis->dobj[legLine].data;
	ilStod=TRUE;
      }
    }
    rtc = getItemByName(pcgItemList, "STOA", &pclLis);
    if (rtc == RC_SUCCESS) {
      if (pclLis->dobj[legLine].status & IS_VALID) {
	pclStoa=pclLis->dobj[legLine].data;
	ilStoa=TRUE;
      }
    }
  }

  if (pcgItemList->periodInfo[legLine].HavePeriodInfo == TRUE) ilPeriodValid = TRUE;
  while (cgUTCTIMEField[ilEntry].FieldName != NULL) {
    pclData = getItemDataByName(pcgItemList, cgUTCTIMEField[ilEntry].FieldName,legLine);
    if (pclData != NULL) {
      rtc = checkCedaTime(pclData);
      if (rtc == RC_HASTEMPLATE) { /* Handle fields with missing time info */
	if (cgUTCTIMEField[ilEntry].DateOnlyAllowed) cgUTCTIMEField[ilEntry].isDateOnly = TRUE;
	else { 
	  dbg(TRACE,"<checkGroupOfTimeFieldValues> Date only not allowed for field <%s>",cgUTCTIMEField[ilEntry]);
	  ilSumRC = RC_FAIL;
	}
      }
      else if (rtc == RC_SUCCESS) {
	if (cgUTCTIMEField[ilEntry].DateOnlyAllowed) cgUTCTIMEField[ilEntry].isDateOnly = FALSE;
 
	/* only period records may have HHMM time */
	if (ilPeriodValid == FALSE && strlen(pclData) != CEDATIMLEN && strcmp(pclData, " ")) {
	  cgUTCTIMEField[ilEntry].isShortField = TRUE;
	}
	else cgUTCTIMEField[ilEntry].isShortField = FALSE;
      }
      else if (rtc != RC_SUCCESS) {
	dbg(TRACE,"<checkGroupOfTimeFieldValues> Field <%s> has invalid time <%s>",cgUTCTIMEField[ilEntry], pclData);
	ilSumRC = RC_FAIL;
      }
    }
    ilEntry++;
  }
  return ilSumRC;
}

/******************************************************************************/
/* checkGroupOfPeriodInfo                                                     */
/*                                                                            */
/* Check if we have some period info. We need to have at least VPFR, VPTO and */
/* FREQ. If FREQW is ommited FREQW=1 is assumed. If data is consistent the    */
/* global flag HavePeriodInfo is set to TRUE. The information will be stored  */
/* to global variables, for later access.                                     */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- the line of data to check                                 */
/*                                                                            */
/* Returns:                                                                   */
/*         nothing (valid data is flagged by HavePeriodInfo)                  */
/*                                                                            */
/******************************************************************************/
static int checkGroupOfPeriodInfo(uint legLine)
{
  int rtc;
  ListPtr pclLis;
  uint lrVPFR = FALSE;
  uint lrVPTO = FALSE;
  uint lrFREQ = FALSE;
  uint lrFRQW = FALSE;
  char *pclVPFR;
  char *pclVPTO;
  char *pclFREQ;
  char *pclFRQW;

  pcgItemList->periodInfo[legLine].HavePeriodInfo = FALSE;

  rtc = getItemByName(pcgItemList, "VPFR", &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) {
      pclVPFR=pclLis->dobj[legLine].data;
      lrVPFR=TRUE;
    }
  }
  rtc = getItemByName(pcgItemList, "VPTO", &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) {
      pclVPTO=pclLis->dobj[legLine].data;
      lrVPTO=TRUE;
    }
  }
  rtc = getItemByName(pcgItemList, "FREQ", &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) {
      pclFREQ=pclLis->dobj[legLine].data;
      lrFREQ=TRUE;
    }
  }
  rtc = getItemByName(pcgItemList, "FRQW", &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) {
      pclFRQW=pclLis->dobj[legLine].data;
      lrFRQW=TRUE;
    }
  }

  if (lrVPFR == FALSE && lrVPTO == FALSE && lrFREQ == FALSE) 
	{
//Frank@Test
	if(igRUN_MODE == 1)
	{
	  dbg(DEBUG,"<checkGroupOfPeriodInfo> return RC_SUCCESS");
	}
//Frank@Test	  
		return RC_SUCCESS; /* there is no period info */
	}

  if (lrVPFR == FALSE || lrVPTO == FALSE || lrFREQ == FALSE) {
    dbg(DEBUG,"<checkGroupOfPeriodInfo> At least one of VPFR,VPTO or FREQ is missing, period info will not be inserted");
    return RC_FAIL;
  }
  if (checkDateSeq(pclVPFR, pclVPTO) == RC_FAIL) {
    dbg(TRACE,"<checkGroupOfPeriodInfo> VPFR <%s> has to be before VPTO <%s> , period info will not be inserted", pclVPFR,pclVPTO);
    return RC_FAIL;
  }
  /*
  ** If we reach this point period information is ok.
  ** Put it to the global storage now and set the valid flag 
  ** for this leg
  */
  
//Frank@Test
	//dbg(DEBUG,"++++++Frank@Test<checkGroupOfPeriodInfo> pcgItemList->periodInfo[legLine].HavePeriodInfo = TRUE");
//Frank@Test
  pcgItemList->periodInfo[legLine].HavePeriodInfo = TRUE;
  strcpy(pcgItemList->periodInfo[legLine].vpfr, pclVPFR);
  strcpy(pcgItemList->periodInfo[legLine].vpto, pclVPTO);
  strcpy(pcgItemList->periodInfo[legLine].freq, pclFREQ);
  if (lrFRQW == FALSE)
    pcgItemList->periodInfo[legLine].frqw = 1;
  else
    pcgItemList->periodInfo[legLine].frqw = atoi(pclFRQW);
  return RC_SUCCESS;
}

/******************************************************************************/
/* checkDateSeq                                                               */
/*                                                                            */
/* Check if the date firstDate is before secDate                              */
/*                                                                            */
/* Input:                                                                     */
/*       firstDate -- points to first date format is yyyymmdd                 */
/*       secData   -- points to second date format is yyyymmdd                */
/*                                                                            */
/* Returns:                                                                   */
/*          RC_SUCCESS if firstDate is before secDate                         */
/*          RC_FAIL    if firstDate is after secDate                          */ 
/*                                                                            */
/******************************************************************************/
static int checkDateSeq(char *firstDate, char *secDate)
{
  uint fyear, fmonth, fday; /* first date items */
  uint syear, smonth, sday; /* second date items */
  char year[5];
  char day[3], month[3];
  char *ptr;

  strncpy(year,firstDate,4);
  fyear = atoi(year);

  strncpy(year,secDate,4);
  syear = atoi(year);

  ptr = firstDate+4;
  strncpy(month,ptr,2);
  fmonth = atoi(month);

  ptr = secDate+4;
  strncpy(month,ptr,2);
  smonth = atoi(month);

  ptr = firstDate+6;
  strncpy(day,ptr,2);
  fday = atoi(day);

  ptr = secDate+6;
  strncpy(day,ptr,2);
  sday = atoi(day);
 
  if (fyear < syear) return RC_SUCCESS;
  if (fyear == syear && fmonth < smonth) return RC_SUCCESS;
/*  if (fyear == syear && fmonth == smonth && fday < sday) return RC_SUCCESS; */
  if (fyear == syear && fmonth == smonth && fday <= sday) return RC_SUCCESS;
  return RC_FAIL;
}
/******************************************************************************/
/* checkGroupOfRoutingInfo                                                    */
/*                                                                            */
/* Checks for the existance of one of the fields from the list                */
/* mandatoryGroupOfRoutingInfo which is a mandatory condition.                */
/* If the first entry is found, check of field consistency will start:        */
/* In the absence of ADID field we will not assume HOPO for any of ORIG or    */
/* DEST. If ADID is present, an empty DEST implies DEST=<HOPO> for ADID=<A>   */
/* and an empty ORIG implies ORIG=<HOPO> for ADID=<D>.                        */
/* If ADID=<B> (local flights) both ORIG and DEST are set to HOPO.            */
/* If ADID is used in combination with ORIG/DEST, consistency is mandatory    */
/* for all fields!                                                            */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- the line of data to check                                 */
/*                                                                            */
/* Returns:                                                                   */
/*         rtc SUCCESS: mandatory fields found and consistent                 */
/*                                                                            */
/******************************************************************************/
static int checkGroupOfRoutingInfo(uint legLine)
{
  int  rtc;
  char *pclORIG;  
  char *pclDEST;
  char *pclADID;
  uint clORIG=FALSE;             /* field ORIG has valid data if TRUE */
  uint clORIGHasHopo = FALSE;    /* field ORIG has HOPO if TRUE */
  uint clDEST=FALSE;             /* field DEST has valid data if TRUE */
  uint clDESTHasHopo = FALSE;    /* field DEST has HOPO if TRUE */
  uint clADID=FALSE;             /* field ADID has valid data */
  uint clADIDType;
  ListPtr pclLis;
  char clADIDChar[2];

  rtc = getItemByName(pcgItemList, "ORIG", &pclLis);
  if (rtc == RC_FAIL) rtc = getItemByName(pcgItemList, "ORG3", &pclLis);
  if (rtc == RC_FAIL) rtc = getItemByName(pcgItemList, "ORG5", &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) {
      pclORIG=pclLis->dobj[legLine].data;
      clORIG=TRUE;
      if (!strcmp(pclORIG,cgHopo)) clORIGHasHopo = TRUE;
    }
  }
  rtc = getItemByName(pcgItemList, "DEST", &pclLis);
  if (rtc == RC_FAIL) rtc = getItemByName(pcgItemList, "DES3", &pclLis);
  if (rtc == RC_FAIL) rtc = getItemByName(pcgItemList, "DES5", &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) {
      pclDEST=pclLis->dobj[legLine].data;
      clDEST=TRUE;
      if (!strcmp(pclDEST,cgHopo)) clDESTHasHopo = TRUE;
    }
  }
  rtc = getItemByName(pcgItemList, "ADID", &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) {
      pclADID=pclLis->dobj[legLine].data;
      clADID=TRUE;
      /*
      ** note that data validity has been checked already
      */
      if (!strcmp(pclADID,"A")) clADIDType = A;
      else if (!strcmp(pclADID,"D")) clADIDType = D;
      else clADIDType = B;
      pcgItemList->RouteType[legLine] = clADIDType;
    }
  }


  if (clORIG==TRUE) dbg(TRACE,"<checkGroupOfRoutingInfo> ORIG <%s>",pclORIG); 
  if (clDEST==TRUE) dbg(TRACE,"<checkGroupOfRoutingInfo> DEST <%s>",pclDEST); 
  if (clADID==TRUE) dbg(TRACE,"<checkGroupOfRoutingInfo> ADID <%s>",pclADID);

  if (!strcmp(pcgActCmd, "IFR") || !strcmp(pcgActCmd, "DFR")) { /* asumptions not for UFR commands */
    if (clADID==TRUE) {
      if (clORIG==TRUE && clDEST==FALSE) {
	/*
	** ORIG = HOPO -> ADID = D/B
	** ORIG != HOPO -> ADID = A
	*/
	if ((clORIGHasHopo == TRUE && clADIDType == A ) || (clORIGHasHopo == FALSE && clADIDType != A)) {
	  dbg(TRACE,"<checkGroupOfRoutingInfo> Illegal combination: ORIG <%s> ADID <%s>",pclORIG,pclADID);
	  return RC_FAIL;
	}
      }
      else if (clORIG == FALSE && clDEST == TRUE) {
	/*
	** DEST = HOPO -> ADID = A
	** DEST != HOPO -> ADID = D
	*/
	if ((clDESTHasHopo == TRUE && clADIDType != A) || (clDESTHasHopo == FALSE && clADIDType != D)) {
	  dbg(TRACE,"<checkGroupOfRoutingInfo> Illegal combination: DEST <%s> ADID <%s>",pclDEST,pclADID);
	  return RC_FAIL;
	}
      }
      else if (clORIG == TRUE && clDEST == TRUE) {
	/*
	** DEST = HOPO, ORIG = HOPO  -> ADID = B
	** DEST = HOPO, ORIG != HOPO -> ADID = A
	** DEST != HOPO, ORIG= HOPO  -> ADID = D
	*/
	if ((clDESTHasHopo == TRUE && clORIGHasHopo == TRUE && clADIDType != B) ||
	    (clDESTHasHopo == TRUE && clORIGHasHopo == FALSE && clADIDType != A) ||
	    (clDESTHasHopo == FALSE && clORIGHasHopo == TRUE && clADIDType != D)) {
	  dbg(TRACE,"<checkGroupOfRoutingInfo> Illegal combination: ORIG <%s> DEST <%s> ADID <%s>",pclORIG,pclDEST,pclADID);
	  return RC_FAIL;
	}
      }
      else if (clORIG == FALSE && clADIDType == A && !strcmp(pcgActCmd, "IFR")) {
	dbg(TRACE,"<checkGroupOfRoutingInfo> Unable to insert arrival due to missing origin");
	return RC_FAIL;
      }
      else if (clDEST == FALSE && clADIDType == D && !strcmp(pcgActCmd, "IFR")) {
	dbg(TRACE,"<checkGroupOfRoutingInfo> Unable to handle departure due to missing destination");
	return RC_FAIL;
      }
      pcgItemList->RouteType[legLine] = clADIDType;
    }
  }
  if (clADID == FALSE) {
    /*
    ** DEST = HOPO, ORIG = HOPO  local     -> B
    ** DEST = HOPO, ORIG != HOPO arrival   -> A
    ** DEST != HOPO ORIG = HOPO  departure -> D
    ** DEST != HOPO ORIG != HOPO via       -> V
    */
    if (clDESTHasHopo == TRUE && clORIGHasHopo == TRUE) {
      pcgItemList->RouteType[legLine] = B;
      strcpy(clADIDChar, "B");
    }
    else if (clDESTHasHopo == TRUE && clORIGHasHopo == FALSE) {
      pcgItemList->RouteType[legLine] = A;
      strcpy(clADIDChar, "A");
    }
    else if (clDESTHasHopo == FALSE && clORIGHasHopo == TRUE) {
      pcgItemList->RouteType[legLine] = D;
      strcpy(clADIDChar, "D");
    }
    else if (clDESTHasHopo == FALSE && clORIGHasHopo == FALSE) {
      pcgItemList->RouteType[legLine] = V;
      strcpy(clADIDChar, "V");
    }
    if (strcmp(clADIDChar, "")) putItemDataByName(pcgItemList, "ADID", clADIDChar, legLine, IS_VALID|IS_AFT);
  }
  dbg(DEBUG,"<checkGroupOfRoutingInfo> Routing info is %s ",RouteType[pcgItemList->RouteType[legLine]]);

  return RC_SUCCESS;
}

/******************************************************************************/
/* checkGroupOfFlightIdentifiers                                              */
/*                                                                            */
/* Checks for the existance of one of the fields from the list                */
/* mandatoryGroupOfFlightIdentifiers which is a mandatory condition           */
/* If the first entry is found, the check was succesful                       */
/*                                                                            */
/* Input:                                                                     */
/*       legLine -- the line of data to check                                 */
/*                                                                            */
/* Returns:                                                                   */
/*         rtc SUCCESS: mandatory field found                                 */
/*                                                                            */
/******************************************************************************/
static int checkGroupOfFlightIdentifiers(uint legLine, uint *gofidEntry)
{
  char *pclField;
  uint entry = 0;
  int  rtc = RC_FAIL;

  while ((pclField = mandatoryGroupOfFlightIdentifiers[entry]) != NULL) {
    rtc = searchFieldValid(pclField, legLine);
    *gofidEntry = entry;
    if (rtc == RC_SUCCESS) return rtc;
    entry++;
  }
  return rtc;
}
/******************************************************************************/
/* searchFieldValid	                                                      */
/*                                                                            */
/* check if a field exists and has valid data                                 */
/*                                                                            */
/* Input:                                                                     */
/*        field    -- name of the field to examine                            */
/*        legLine  -- data entry to be used                                   */
/*                                                                            */
/* Returns:                                                                   */
/*        RC_SUCCESS if field exists and has valid data for corresponding leg */
/*                                                                            */
/******************************************************************************/
static int searchFieldValid(char *field, uint legLine)
{
  int rtc = RC_INVALID;
  ListPtr pclLis;

  rtc = getItemByName(pcgItemList, field, &pclLis);
  if (rtc == RC_SUCCESS) {
    if (pclLis->dobj[legLine].status & IS_VALID) return RC_SUCCESS;
    else return RC_INVALID;
  }
  else return rtc;
}
/******************************************************************************/
/* createNewDataEntry  	                                                      */
/*                                                                            */
/* Add a new data entry to the list of data. Each data entry is the storage   */
/* for one request from the wrapper process. So we are able to work on new    */
/* data, while a flight request is pending.                                   */
/*                                                                            */
/* Input:                                                                     */
/*        InData     -- pointer to a list of data entries.                    */
/*                                                                            */
/* returns:                                                                   */
/*        a pointer to the newly created data entry                           */
/*                                                                            */
/******************************************************************************/
static InDataPtr createNewDataEntry(InDataPtr *InData)
{
  InDataPtr pclFreeEl;

  if (*InData == NULL) {
    *InData = malloc(sizeof(InDataElem));
    (*InData)->next = NULL;
    (*InData)->prev = NULL;
    (*InData)->ifData = NULL;
    (*InData)->serialNumber = 0;
    (*InData)->stage = 0;
    pclFreeEl = *InData;
  }
  else {
    pclFreeEl = *InData;
    pclFreeEl = (InDataPtr) getFirstFree ((ListPtr)pclFreeEl);
    pclFreeEl->next = malloc(sizeof(InDataElem));
    pclFreeEl->next->next = NULL;
    pclFreeEl->next->ifData = NULL;
    pclFreeEl->next->serialNumber = 0;
    pclFreeEl->next->stage = 0;
    pclFreeEl->next->prev = pclFreeEl;
    
  }
  return pclFreeEl;
}

/******************************************************************************/
/* createItemList  	                                                      */
/*                                                                            */
/* Add new field items to the list, scan the item list and create a new entry */
/* for each item.                                                             */
/*                                                                            */
/* Input:                                                                     */
/*        list       -- pointer to data list containing the field items       */
/*        hook       -- pointer to list where the element is to add           */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static int createItemList(char *list, ListPtr *hook)
{
  char *pclElement;
  
  if (strlen(list) == 0) return RC_FAIL;
  if ((pclElement=strtok(list, ","))==NULL) {
    addItemToList(pclElement, hook);
    return RC_SUCCESS;
  }
  addItemToList(pclElement, hook);
  while ((pclElement = strtok(NULL, ",")) != NULL) {
    addItemToList(pclElement, hook);
  }
  return RC_SUCCESS;
}

/******************************************************************************/
/* addDataItemToList  	                                                      */
/*                                                                            */
/* Add new items to the list, allocate required memory, the field list needs  */
/* already exist. The list of data has to correspond to the field list        */
/*                                                                            */
/* Input:                                                                     */
/*        list       -- pointer to data list containing the data items        */
/*        hook       -- pointer to list where the element is to add           */
/*        entry      -- the number of the related leg                         */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static void addDataItemToList(char *list, ListPtr *hook, uint entry, char delimiter, uint defaultStatus)
{
  char *pclElement;      /* pointer to start of element */
  char *pclActElement;   /* pointer to actual element   */
  uint ilLenOfStr;
  uint ilMaxLen;
  uint ilLastElem = FALSE;
  uint ilNumItems = 0;
  uint i;
  uint ilDelete = TRUE;
  ListPtr pclPtr;
  ListPtr pclRemPtr;

  pclElement = list;
  pclPtr = *hook;
  while (*pclElement != NULL && pclPtr != NULL) {
    pclActElement = getNextItem(pclElement, delimiter, &ilLenOfStr, &ilLastElem);
    if (ilLastElem == FALSE) {
      pclElement = pclElement + ilLenOfStr + 1;
    }
    else {
      pclElement = pclElement + ilLenOfStr;
    }
    if (ilLenOfStr != 0) {
      ilMaxLen = ilLenOfStr + 1;
      pclPtr->dobj[entry].data = malloc(ilMaxLen);
      pclPtr->dobj[entry].len = ilMaxLen;
      pclPtr->dobj[entry].status = defaultStatus;
      strncpy(pclPtr->dobj[entry].data,pclActElement,ilMaxLen-1);
      pclPtr->dobj[entry].data[ilLenOfStr] = '\0';
      ilNumItems++;
    }
    else {
      for (i=0;i<MAX_NUM_LEGS;i++) {
	if (i!=entry) {
	  if (pclPtr->dobj[i].data != NULL) {
	    ilDelete = FALSE;
	    break;
	  }
        }
      }
      if (ilDelete == TRUE) {
        pclRemPtr = pclPtr;
        removeItemFromList(pclRemPtr, hook);
      }
    }
    pclPtr = pclPtr->next;
  }
  dbg(DEBUG,"<addDataItemToList> Added <%d> items to list", ilNumItems);
}  

/******************************************************************************/
/* getNextItem                                                                */
/*                                                                            */
/* Helper function to fetch the next item from a ',' separated list. If the   */
/* last item was fetched, ipLast will be set to TRUE.                         */
/*                                                                            */
/* Input:                                                                     */
/*      cpSource -- pointer to the item list                                  */
/*      cpDelim  -- separator character (something like ',')                  */
/*                                                                            */
/* Return:                                                                    */
/*      ipLen    -- len of the found item                                     */
/*      ipLast   -- TRUE, if the last item was fetched                        */
/*      returns a pointer to the item (isn't '\0' terminated, so use ipLen)   */
/*                                                                            */
/******************************************************************************/
static char *getNextItem(char *cpSource, char cpDelim, uint *ipLen, uint *ipLast)
{
  char *clStart;

  if (cpSource == NULL) {
    *ipLen = 0;
    return NULL;
  }

  clStart = cpSource;
  while (*cpSource != NULL && *cpSource != cpDelim) {
    cpSource++;
  }
  *ipLen = cpSource-clStart;
  if (*cpSource == NULL) *ipLast = TRUE;
  else *ipLast = FALSE;
  return clStart;
}

static int catList(ListPtr *first, ListPtr *next)
{
  ListPtr pclFreeEl;

  if (*first == NULL) return RC_FAIL;
  pclFreeEl = *first;
  pclFreeEl = getFirstFree (pclFreeEl);
  pclFreeEl->next = *next;
  return RC_SUCCESS;
}


/******************************************************************************/
/* addItemToList  	                                                      */
/*                                                                            */
/* add a new item to the list, allocate required memory, initialize pointers  */
/*                                                                            */
/* Input:                                                                     */
/*        element    -- pointer to field name                                 */
/*        hook       -- pointer to list where the element is to add           */
/*        numOfLegs  -- number of legs to initialize                          */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- pointer to the first object with free next entry           */
/*                                                                            */
/******************************************************************************/
static int addItemToList(char *element, ListPtr *hook)
{
  ListPtr pclFreeEl;
  ListPtr pclInitEl;
  uint i;

  if (*hook == NULL) {
    *hook = malloc(sizeof(ListElem));
    pclInitEl = *hook;
    pclFreeEl = NULL;
  }
  else {
    pclFreeEl = *hook;
    pclFreeEl = getFirstFree (pclFreeEl);
    pclFreeEl->next = malloc(sizeof(ListElem));
    pclInitEl = pclFreeEl->next;
  }
  pclInitEl->next = NULL;  
  pclInitEl->prev = pclFreeEl;
  pclInitEl->request = NULL;
  for (i=0; i<MAX_NUM_LEGS; i++) {
    pclInitEl->dobj[i].data = NULL;
    pclInitEl->dobj[i].status = 0;
    pclInitEl->dobj[i].len = 0;
    pclInitEl->dobj[i].timeType = 0;
    pclInitEl->dobj[i].timeZone[0] = '\0';
    pclInitEl->dobj[i].timeZone[1] = '\0';
    pclInitEl->dobj[i].timeZone[2] = '\0';

    pclInitEl->periodInfo[i].vpfr[0] = '\0'; 
    pclInitEl->periodInfo[i].vpto[0] = '\0'; 
    pclInitEl->periodInfo[i].freq[0] = '\0';
    pclInitEl->periodInfo[i].frqw = 0;
    pclInitEl->periodInfo[i].HavePeriodInfo = FALSE;

    pclInitEl->timeInfo[i].HaveTimeInfo = FALSE;
    pclInitEl->timeInfo[i].stod[0] = '\0';
    pclInitEl->timeInfo[i].stoa[0] = '\0';
    pclInitEl->timeInfo[i].etod[0] = '\0';
    pclInitEl->timeInfo[i].etoa[0] = '\0';
    pclInitEl->timeInfo[i].ofbl[0] = '\0';
    pclInitEl->timeInfo[i].onbl[0] = '\0';
    pclInitEl->timeInfo[i].airb[0] = '\0';
    pclInitEl->timeInfo[i].land[0] = '\0';
    pclInitEl->timeInfo[i].tmoa[0] = '\0';
    pclInitEl->timeInfo[i].nxti[0] = '\0';

    pclInitEl->RouteType[i] = 0;
    }

    pclInitEl->field = malloc(strlen(element)+1);
    strcpy(pclInitEl->field, element);

    return RC_SUCCESS;
}
/******************************************************************************/
/* getFirstFree  	                                                      */
/*                                                                            */
/* return a pointer to a 'field' object with name 'name'                      */
/* note the recursive usage of that function!                                 */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field list                                      */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- pointer to the first object with free next entry           */
/*                                                                            */
/******************************************************************************/
ListPtr getFirstFree(ListPtr hook)
{
  ListPtr pclLis;

  if (hook->next == NULL) {
    pclLis = hook;
    return pclLis;
  }
  pclLis = getFirstFree(hook->next);
  return pclLis;
}
/******************************************************************************/
/* findItem           	                                                      */
/*                                                                            */
/* returns RC_SUCCES, if item was found in list.                              */
/* note the recursive usage of that function!                                 */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*        name  -- the name of the field                                      */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- RC_SUCCESS: element was found                              */
/*                 RC_FAIL:    element was not found                          */
/*                                                                            */
/******************************************************************************/
int findItem(ListPtr hook, char *name)                   
{
  int rtc;

  if (!strcmp(name,hook->field)) {
    rtc = RC_SUCCESS;
    return rtc;
  }
  if (hook->next == NULL) return RC_FAIL;
  rtc = findItem(hook->next, name);
  return rtc;
}

/******************************************************************************/
/* getItemByName	                                                      */
/*                                                                            */
/* return a pointer to a 'field' object with name 'name'                      */
/* note the recursive usage of that function!                                 */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*        name  -- the name of the field                                      */
/*                                                                            */
/* returns:                                                                   */
/*        entry -- pointer to the element if rtc = RC_SUCCES                  */
/*        rtc   -- RC_SUCCESS: element was found                              */
/*                 RC_FAIL:    element was not found                          */
/*                                                                            */
/******************************************************************************/
static int getItemByName(ListPtr hook, char *name, ListPtr *entry)                   
{
  int rtc;

  if (!strcmp(name,hook->field)) {
    *entry = hook;
    rtc = RC_SUCCESS;
    return rtc;
  }
  if (hook->next == NULL) return RC_FAIL;
  rtc = getItemByName(hook->next, name, entry);
  return rtc;
}
/******************************************************************************/
/* putItemDataByName	                                                      */
/*                                                                            */
/* Rewrite data of an item. If the data element is to short, the required     */
/* additional space is allocated.                                             */
/*                                                                            */
/* Input:                                                                     */
/*        hook     -- pointer to the start of the itemlist                    */
/*        name     -- the name of the field to get data from                  */
/*        newData  -- pointer to the new data to write                        */               
/*        legLine  -- the data entry to use                                   */
/*                                                                            */
/* Return:                                                                    */
/*        returns  SUCCESS or RC_FAIL                                         */
/*                                                                            */
/******************************************************************************/

static int putItemDataByName(ListPtr hook, char *name, char *newData, uint legLine, uint ilStatus)
{
  uint ilnewDataLen = 0;
  uint iloldDataLen = 0;
  uint ilRC;
  ListPtr pclLis;

  ilnewDataLen = strlen(newData);

  if (ilStatus != IS_INVALID)
    if (checkDataValid(name, newData, legLine) != RC_SUCCESS) return RC_FAIL;
  
  ilRC = getItemByName(hook, name, &pclLis);
  if (ilRC != RC_SUCCESS) {
    addItemToList(name, &hook);
    dbg(DEBUG,"<putItemDataByName> Added field <%s> to list", name);
    ilRC = getItemByName(hook, name, &pclLis);
    if (ilRC != RC_SUCCESS) {
      dbg(TRACE, "<putItemDataByName> Unable to add field <%s>", name);
      return RC_FAIL;
    }
  }
  if (pclLis->dobj[legLine].data == NULL) iloldDataLen = 0;
  else iloldDataLen = strlen(pclLis->dobj[legLine].data);
  if ((ilnewDataLen > iloldDataLen) && (pclLis->dobj[legLine].len < ilnewDataLen)) {
    if (iloldDataLen == 0) {
      pclLis->dobj[legLine].data = malloc(ilnewDataLen+1);
    }
    else {
      pclLis->dobj[legLine].data = realloc(pclLis->dobj[legLine].data, ilnewDataLen+1);
    }
    pclLis->dobj[legLine].len  = ilnewDataLen;
  }
  if (ilStatus != 0) {
    if (ilStatus == IS_INVALID) {
      pclLis->dobj[legLine].status = IS_INVALID;
      return RC_SUCCESS;
    }
    else {
      pclLis->dobj[legLine].status = pclLis->dobj[legLine].status | ilStatus;
    }
  }
  dbg(DEBUG,"<putItemDataByName> writing <%s> to field <%s> leg <%d>", newData, name, legLine);
  strcpy(pclLis->dobj[legLine].data, newData);
  return RC_SUCCESS;
}
/******************************************************************************/
/* getRequestDataByName	                                                      */
/*                                                                            */
/* Retrieve data from an item.                                                */
/*                                                                            */
/* Input:                                                                     */
/*        hook     -- pointer to the start of the itemlist                    */
/*        name     -- the name of the field to get data from                  */
/*                                                                            */
/* Return:                                                                    */
/*        returns a pointer to the data string or NULL if not found           */
/*        or invalid                                                          */
/*                                                                            */
/******************************************************************************/
static MessageStateTypePtr getRequestDataByName(ListPtr hook, char *name)
{
  MessageStateTypePtr data;

  if (!strcmp(name,hook->field)) {
    if (hook->request == NULL) {
      hook->request = malloc(sizeof(MessageStateType));
      hook->request->sendCount = 0;
      hook->request->rcvAnswerCount = 0;
      hook->request->lastElSent = FALSE;
      hook->request->numOfEntries = 0;
    }
    return hook->request;
  }
  if (hook->next == NULL) return NULL;
  data = getRequestDataByName(hook->next, name);
  return data;
}
/******************************************************************************/
/* getItemDataByName	                                                      */
/*                                                                            */
/* Retrieve data from an item.                                                */
/*                                                                            */
/* Input:                                                                     */
/*        hook     -- pointer to the start of the itemlist                    */
/*        name     -- the name of the field to get data from                  */
/*        legLine  -- the data entry to use                                   */
/*                                                                            */
/* Return:                                                                    */
/*        returns a pointer to the data string or NULL if not found           */
/*        or invalid                                                          */
/*                                                                            */
/******************************************************************************/
static char *getItemDataByName(ListPtr hook, char *name, uint legLine)
{
  char *data;

  if (!strcmp(name,hook->field)) {
    if (hook->dobj[legLine].status & IS_VALID) return hook->dobj[legLine].data;
    else return NULL;
  }
  if (hook->next == NULL) return NULL;
  data = getItemDataByName(hook->next, name, legLine);
  return data;
}
/******************************************************************************/
/* removeItemFromList	                                                      */
/*                                                                            */
/* Remove an element from the list and free the allocated storage             */
/*                                                                            */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static void removeItemFromList (ListPtr hook, ListPtr *pcpGlobal)
{
  int i;

  if (hook->prev != NULL) { 
    if (hook->next != NULL) {
      hook->prev->next = hook->next;
    }
    else {
      hook->prev->next = NULL;
    }
    if (hook->next != NULL) {
      hook->next->prev = hook->prev;
    }
  }
  else if (hook->next != NULL) {
    hook->next->prev = NULL;
    *pcpGlobal = hook->next;
  }
  else {
    *pcpGlobal = NULL; 
  }
  dbg(TRACE,"<removeItemFromList> removing %s",hook->field);
  free(hook->field);
  hook->field = NULL;
  free(hook->request);
  hook->request = NULL;
  for (i=0; i<MAX_NUM_LEGS; i++) {
    if (hook->dobj[i].data != NULL) {
      free(hook->dobj[i].data);
      hook->dobj[i].data = NULL;
    }
  }
  free(hook);
}
/******************************************************************************/
/* deleteItemList	                                                      */
/*                                                                            */
/* Delete the entire item list and free all storage.                          */
/* This function is called recursively!                                       */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                              */
/*                                                                            */
/******************************************************************************/
static void deleteItemList(ListPtr hook)
{
  int i;
  if (hook != NULL)
    {
      deleteItemList(hook->next);
      free(hook->field);
      for (i=0; i<MAX_NUM_LEGS; i++) {
	if (hook->dobj[i].data != NULL) {
	  free(hook->dobj[i].data);
	}
      }
      free(hook);
    }
}

static char pcgNoCheckForLen[] = ",ADID,VPFR,VPTO,FREQ,COSH,ROUT,VSUP,UAFT,JFLN,JDAY,LFLDA,FLDU,FCA1,FCA2,DSSF,";
static char pcgNoCheckForValid[] = ",URNO,FKEY,LFKEY,FLDU,GATE,ONWD,REMP,GTA1,GTA2,GTD1,GTD2,TGA1,TGA2,TGD1,TGD2,REGN,BLT1,BLT2,VIAL,PSTA,PSTD,VPFR,VPTO,NUMS,SECN,RWYA,RWYD,JFNO,COSH,ROUT,VSUP,UAFT,BAZ1,JFLN,JDAY,FLNS,FLTN,CSGN,DSSF,";
static char pcgCheckForLoc[] = ",ORIG,ORG3,ORG4,DEST,DES3,DES4,";
static char pcgCheckForCTime[] = ",ETAA,ETDA,ETDC,AIRA,AIRB,FLDA,LFLDA,GD1X,GD1Y,BOAO,FCAL,FCA1,FCA2,LNDA,STOA,STOD,ONBS,ETAC,ETAD,OFBS,ETOA,ETOD,LAND,";
static char pcgCheckDBField[] = ",ACT3,ACT5,TTYP,ALC2,ALC3,";
/******************************************************************************/
/* checkDataValid	                                                      */
/*                                                                            */
/* validate data on a per field basis                                         */
/*                                                                            */
/* Input:                                                                     */
/*        field   -- the field to examine                                     */
/*        data    -- the data to examine                                      */
/*        legLine -- data entry                                               */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- RC_SUCCESS: element was validated                          */
/*                 RC_FAIL:    element was not validated                      */
/*                                                                            */
/******************************************************************************/
static int checkDataValid(char *field, char *data, uint legLine)
{
  int rtc = RC_SUCCESS;

  if (!strcmp(data, " ")) return RC_SUCCESS; /* This is for toDelete fields */

  if (!strcmp(field, "NUMS") || !strcmp(field, "SECN") || !strcmp(field, "FKEY") || 
      !strcmp(field, "LFKEY") || !strcmp(field, "ACTI") || !strcmp(field, "FTYP")) {
    return RC_SUCCESS;
  }
  else if (!strcmp(field, "ADID")) {
    return checkADID(data);
  }
  /*
  ** this checks the max len limit of AFT fields 
  */
  if (strstr(pcgNoCheckForLen,field) == NULL)
  { /* richtiger waere sprintf(xy,",%s,",field) ; 
                       if (strstr(pcgNoCheckForLen,xy) != NULL) ...
    */
    if (checkMaxDataLenOk(field, data) == FALSE) return RC_FAIL;
  }

  if (!strcmp(field, "FLNO")) {
    if (igIfType == ELSE)
    {
      dbg (TRACE, "checkDataValid: skip checkFLNO for ELSE");
    }
    else
      rtc = checkFLNO(data);
  }
  else if (strstr(pcgNoCheckForValid, field) != NULL) {
  }
  else if (strstr(pcgCheckForLoc, field) != NULL) {
    rtc = checkLocation(field, data);
  }
  else if (strstr(pcgCheckForCTime, field) != NULL) {
    rtc = checkCedaTime(data);
  }
  else if (strstr(pcgCheckDBField, field) != NULL) {
    rtc = checkDBField(field, data);
  }
  else if (!strcmp(field,"FREQ")) {
    rtc = checkPeriod(data);
  }
  else {
    dbg(TRACE,"<checkDataValid> Field handling for field %s not yet implemented", field);
    return RC_FAIL;
  }
  return rtc;
}
/******************************************************************************/
/* Field value validations                                                    */
/******************************************************************************/


/******************************************************************************/
/* checkCedaTime   	                                                      */
/*                                                                            */
/* Check if the provided time pattern is valid. It has to look like:          */
/* YYYYMMDDHHMM, DDHH.                                                        */
/*                                                                            */
/* Input:                                                                     */
/*        pcpData -- pointer to data we have to check                         */
/*                                                                            */
/* Returns:                                                                   */
/*        RC_SUCCESS if pattern is valid                                      */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkCedaTime(char *pcpData) 
{
  uint ilTimStrLen;
  uint ilHour, ilMin, ilSec;
  uint ilYear, ilMonth, ilDay;
  uint ilTime = FALSE;
  uint ilDate = FALSE;
  uint ilTimeTempl = FALSE;
  char clHour[3], clMin[3], clSec[3];
  char clYear[5], clMonth[3], clDay[3];
  char *pclData;
  char *pclTemplStr;

  if (!strcmp(pcpData, " ")) return RC_SUCCESS;

  ilTimStrLen = strlen(pcpData);

  if ((pclTemplStr = strstr(pcpData, "xxxx"))!= NULL) ilTimeTempl = TRUE;

  if (ilTimStrLen == 4 || ilTimStrLen ==6) ilTime = TRUE;
  else if (ilTimStrLen == 8) ilDate = TRUE;
  else if (ilTimStrLen == 12 || ilTimStrLen == 14) {
    ilTime = TRUE;
    ilDate = TRUE;
  }
  else {
    dbg(TRACE,"<checkCedaTime> Illegal time format <%s> valid is YYYYMMDD DDHH",pcpData); 
    return RC_FAIL;
  }

  if (ilTimeTempl == TRUE) ilTime = FALSE; /* don't check template string */

  pclData = pcpData;

  if (ilDate == TRUE) {
    strncpy(&clYear[0],pclData,4);
    clYear[4] = '\0';
    ilYear    = atoi(clYear);
    clMonth[0]= *(pclData+4);
    clMonth[1]= *(pclData+5);
    clMonth[2]= '\0';
    ilMonth   = atoi(clMonth);
    clDay[0]  = *(pclData+6);
    clDay[1]  = *(pclData+7);
    clDay[2]  = '\0';
    ilDay     = atoi(clDay);
    if (ilYear < 1970 || ilYear > 2100) {
      dbg(TRACE,"<checkCedaTime> Illegal year <%d>", ilYear);
      return RC_FAIL;
    }
    if (ilMonth < 1 || ilMonth > 12) {
      dbg(TRACE,"<checkCedaTime> Illegal month <%d>", ilMonth);
      return RC_FAIL;
    }
    /* check special case of 29. FEB in leap years! */
    if (LeapYear(ilYear) == 1 && ilMonth == 2) {
      if (ilDay < 1 || ilDay > 29) {
	dbg(TRACE,"<checkCedaTime> Illegal day <%d> in month <%d>", ilDay, ilMonth);
	return RC_FAIL;
      }
    }
    else if (ilDay < 1 || ilDay > igDayTab[ilMonth]) {
      dbg(TRACE,"<checkCedaTime> Illegal day <%d> in month <%d>", ilDay, ilMonth);
      return RC_FAIL;
    } 
    /* proceed to date entry */
    pclData = pcpData+8;
  }
  if (ilTime == TRUE) {
    clHour[0] = *pclData;
    clHour[1] = *(pclData+1);
    clHour[2] = '\0';
    ilHour    = atoi(clHour);
    clMin[0]  = *(pclData+2);
    clMin[1]  = *(pclData+3);
    clMin[2]  = '\0';
    ilMin     = atoi(clMin);
    if (ilTimStrLen == 6 || ilTimStrLen== 14) {
      clSec[0] = *(pclData+4);
      clSec[1] = *(pclData+5);
      clSec[2] = '\0';
      ilSec    = atoi(clSec);
      if (ilSec > 59) {
	dbg(TRACE,"<checkCedaTime> Illegal Second <%d>", ilMin);
      return RC_FAIL;
      }
    }
    if(ilHour > 23) {
      dbg(TRACE,"<checkCedaTime> Illegal Hour <%d>", ilHour);
      return RC_FAIL;
    }
    if (ilMin > 59) {
      dbg(TRACE,"<checkCedaTime> Illegal Minute <%d>", ilMin);
      return RC_FAIL;
    }
  }
  if (ilTimeTempl == TRUE) {
    return RC_HASTEMPLATE;
  }
  return RC_SUCCESS;
}

/******************************************************************************/
/* checkPeriod   	                                                      */
/*                                                                            */
/* Check if the provided period pattern is valid. It has to be one of:        */
/* (..3.5..), (3,5)                                                           */
/*                                                                            */
/* Input:                                                                     */
/*        field   -- field to fetch data from (FREQ)                          */
/*        legLine -- entry to leg                                             */
/*                                                                            */
/* Returns:                                                                   */
/*        RC_SUCCESS if pattern is valid                                      */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkPeriod(char *pcpData)
{
  uint ilPerStrLen;
  uint ilCount;
  uint ilVal = 0;
  char clPer[2];

  ilPerStrLen = strlen(pcpData);

  for (ilCount=0;ilCount<ilPerStrLen;ilCount++) {
    clPer[0] = *(pcpData+ilCount);
    clPer[1] = '\0';
    if(isdigit((int) clPer[0])) {
      ilVal = atoi(clPer);
      if ((ilVal < 1) || (ilVal > 7)) {
	dbg(TRACE,"<checkPeriod> invalid digit in period string <%s> at position <%d>", pcpData,ilCount+1); 
	return RC_FAIL;
      }
    }
    else if(clPer[0] != '.' && clPer[0] != ',') {
      dbg(TRACE,"<checkPeriod> invalid character in period string <%s> at position <%d>", pcpData,ilCount+1); 
      return RC_FAIL;
    }
  }
  return RC_SUCCESS;
}
/******************************************************************************/
/* checkLocation	                                                      */
/*                                                                            */
/* Read from APC3 or APC4 of APTTAB and compare the location code with that,  */
/* i.e. read APC3 if code length is 3  and APC4 if code length is 4.          */
/*                                                                            */
/* Input:                                                                     */
/*        field   -- field to fetch data from (ORIG,DEST...)                  */
/*        legLine -- entry to leg                                             */
/*                                                                            */
/* Returns:                                                                   */
/*        RC_SUCCESS if code was found (i.e. valid)                           */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkLocation(char *pcpField,char *pcpData)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  if (!strcmp(pcpField, "ORIG")) {
    if (strlen(pcpData) == 3) strcpy(pcpField, "ORG3"); 
    else if (strlen(pcpData) == 4)strcpy(pcpField, "ORG4"); 
  }
  else if (!strcmp(pcpField, "DEST")) {
    if (strlen(pcpData) == 3) strcpy(pcpField, "DES3");
    else if (strlen(pcpData) == 4)strcpy(pcpField, "DES4"); 
  }
  if (strlen(pcpData) == 3) {
    sprintf (pcgSqlBuf, "SELECT APC3 FROM APTTAB WHERE APC3='%s'", pcpData);
  }
  else {
    sprintf (pcgSqlBuf, "SELECT APC3 FROM APTTAB WHERE APC4='%s'", pcpData);
  }
  /* REMOVE dbg(TRACE,"checkLocation: Command <%s>", pcgSqlBuf); */
  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  if (ilGetRc == DB_SUCCESS) {
    ilRC = RC_SUCCESS;
  }                             /* end if */
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<checkLocation> Error: %s is invalid <%s> Oracle error <%s>",pcpField, pcpData, errBuff);
      ilRC = RC_FAIL;
    }                           /* end if */
  }
  close_my_cursor (&slCursor);
  
  return ilRC;
}

/******************************************************************************/
/* checkFLNO     	                                                      */
/*                                                                            */
/* Check if FLNO is valid. A valid FLNO looks like:                           */
/* XXXYYYY                                                                    */
/* where XXX is a valid ALC3/5 code, YYYY is a valid number.                  */
/*                                                                            */
/* Input:                                                                     */
/*       pointer to a FLNO string                                             */
/*                                                                            */
/* Return:                                                                    */
/*        RC_SUCCESS if FLNO is valid                                         */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkFLNO(char *pcpData) 
{
  uint ilFlnoLen;
  uint ilStrPos=0;
  uint ilBlanks=0;
  uint ilNumPos=0;
  int  ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char *clPtr;
  char clALC[10];
  char clNum[10];
  char clMessage[128];

  ilFlnoLen = strlen(pcpData);
  if (ilFlnoLen > 9 ) {
    dbg(TRACE,"<checkFLNO> Error -- FLNO must not be longer than 9 <%d>", ilFlnoLen);
    return RC_FAIL;
  }
  clPtr = pcpData;
  /*
  ** The first 2 letters are always accepted
  */
  strncpy(clALC, clPtr, 2);
  clALC[2] = '\0';
  clPtr++;clPtr++; /* advance to next */
  ilStrPos++;ilStrPos++;
  if (isalpha((int) *clPtr) || isdigit((int) *clPtr)) {
    strcat(clALC, clPtr);
    clPtr++;
    ilStrPos++;
  }
  /*
  ** validate ALC from database
  */
  if (ilStrPos == 2) {
      sprintf (pcgSqlBuf, "SELECT DISTINCT ALC3 FROM ALTTAB WHERE ALC2='%s'", clALC);
  }
  else if (ilStrPos == 3) {
      sprintf (pcgSqlBuf, "SELECT DISTINCT ALC3 FROM ALTTAB WHERE ALC3='%s'", clALC);
  }
  slFkt = START;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  close_my_cursor (&slCursor);
  if (ilGetRc != DB_SUCCESS) {
    if (ilGetRc == NOTFOUND) {
      dbg (TRACE,"<checkFLNO> Fatal -- unable to find ALC3 in Database <%s>", clALC);
      sprintf(clMessage, "Unable to find Airline Code <%s> in Basic Data", clALC);
      alert(clMessage);
      igAlerted = TRUE;
      return RC_FAIL;
    }
  }
  while (*clPtr == ' ' && *clPtr != '\0') {
    ilBlanks++;
    ilStrPos++;
    clPtr++; /* skip over possible ' ' */
  }
  if (ilStrPos == 2 && ilBlanks == 0) { /* has to be 3 char 3 num */
    dbg (TRACE,"<checkFLNO> Fatal illegal FLNO format <%s>", pcpData);
    return RC_FAIL;
  }
  while (isdigit((int) *clPtr)) {
    clNum[ilNumPos] = *clPtr;
    clPtr++; ilStrPos++; ilNumPos++;
  }
  clNum[ilNumPos] = '\0';
  if (ilStrPos == 8 || *clPtr == '\0') return RC_SUCCESS;
  else {
    dbg(TRACE,"<checkFLNO> WARNING -- unchecked item in FLNO <%s>",clPtr);
    return RC_SUCCESS;
  }
}

/******************************************************************************/
/* checkADID     	                                                      */
/*                                                                            */
/* Check if ADID is valid. A valid ADID is one of A|D|B|V                     */
/*                                                                            */
/* Input:                                                                     */
/*       pointer to a ADID string                                             */
/*                                                                            */
/* Return:                                                                    */
/*        RC_SUCCESS if ADID is valid                                         */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkADID(char *pcpData) {
  if (*pcpData == 'A' || *pcpData == 'D' || *pcpData == 'B' || *pcpData == 'V') {
    return RC_SUCCESS;
  }
  else {
    dbg(TRACE,"<checkADID> Invalid ADID received <%s>", pcpData);
    return RC_FAIL;
  }
}

/******************************************************************************/
/* checkDBField              	                                              */
/*                                                                            */
/* Check some DB fields against AODB contents                                 */
/*                                                                            */
/* Input:                                                                     */
/*       pcpField -- DB field                                                 */
/*       pcpData  -- DB data to check                                         */
/*                                                                            */
/* Return:                                                                    */
/*        RC_SUCCESS if ADID is valid                                         */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkDBField(char *pcpField, char *pcpData) 
{
  int  ilRC;
  char clObjName[64];
  static char clMessage[128];

  if (!strcmp(pcpField, "ACT3") || !strcmp(pcpField, "ACT5")) {
    if (!strcmp(pcpData, " ")) return RC_SUCCESS; /* field will be deleted, accept */
    sprintf(pcgSqlBuf, "SELECT %s FROM ACTTAB WHERE %s='%s'", pcpField, pcpField, pcpData);
    sprintf(clObjName, "Aircraft Type");
  }
  else if (!strcmp(pcpField, "TTYP")) {
    if (!strcmp(pcpData, " ")) return RC_SUCCESS; /* field will be deleted, accept */
    sprintf(pcgSqlBuf, "SELECT %s FROM NATTAB where %s='%s'", pcpField, pcpField, pcpData);
    sprintf(clObjName, "Flight Nature");
  }
  else if (!strcmp(pcpField,"ALC2")  || !strcmp(pcpField,"ALC3")) {
    if (!strcmp(pcpData, " ")) return RC_SUCCESS; /* field will be deleted, accept */
    sprintf(pcgSqlBuf, "SELECT %s FROM ALTTAB WHERE %s='%s'", pcpField, pcpField, pcpData);
    sprintf(clObjName, "Airline Code");
  }
  ilRC = getOrPutDBData(START);
  if (ilRC != RC_SUCCESS) {
    sprintf(clMessage,"Field <%s> has invalid %s <%s>", pcpField, clObjName, pcpData);
    alert(clMessage);
    igAlerted = TRUE;
    return RC_FAIL;
  }
  return RC_SUCCESS;
}
/******************************************************************************/
/* getSelection                                                               */
/*                                                                            */
/* retrieve and check values of IFNAME, TIME, PRIO			      */
/*                                                                            */
/******************************************************************************/
static int getSelection(char *SelectionString, uint  *interface, uint *timeType, uint *quePrio)
{
  char *pclSelptr;
  char *pclData;
  char *pclRef;
  int entry;
  uint ilEntry;
  uint ilNumItems = 0;
  char clOptFields[256];
  char clOptData[256];

  if (pcgConfigList != NULL) {
    deleteItemList(pcgConfigList);
    pcgConfigList = NULL;
  }
  /*
  ** try to fetch additional
  ** runtime configuration
  */
  clOptFields[0] = '\0';
  clOptData[0] = '\0';
  pclSelptr = strtok(SelectionString,":");
  if (pclSelptr != NULL) strcpy(clOptFields,pclSelptr);
  else {
    dbg(TRACE,"<getSelection> Fatal -- empty selection not allowed");
    return RC_FAIL;
  }
  pclData = strtok(NULL,",");
  if (pclData != NULL) {
    strcpy(clOptData, pclData);
    ilNumItems++;
  }
  pclSelptr = strtok(NULL, ":");
  while (pclSelptr != NULL) {
    if (pclSelptr == NULL) {
      break;
    }
    else {
      strcat(clOptFields, ",");
      strcat(clOptFields,pclSelptr);
      pclData = strtok(NULL,",");
      if (pclData == NULL) {
	break;
      }
      else {
	strcat(clOptData, ",");
	strcat(clOptData, pclData);
	ilNumItems++;
	pclSelptr = strtok(NULL, ":");
      }
    }
  }

  createItemList(clOptFields, &pcgConfigList);
  addDataItemToList(clOptData, &pcgConfigList, 0, ',', IS_VALID);
  dbg(TRACE,"<getSelection> Fields <%s>", clOptFields); 
  dbg(TRACE,"<getSelection> Data <%s>", clOptData); 

  /*
  ** fetch mandatory fields
  */
  ilEntry = 0;
  while (cgConfigParam[ilEntry] != NULL) {
    pclData = getItemDataByName(pcgConfigList,cgConfigParam[ilEntry],0);
    if (pclData == NULL) {
      if (!strcmp(cgConfigParam[ilEntry],"IFNAME") || !strcmp(cgConfigParam[ilEntry],"TIME")) {
	dbg(TRACE, "<getSelection> Fatal -- unable to fetch mandatory data <%s>",cgConfigParam[ilEntry]);
	return RC_FAIL;
      }
    }
    else {
      if (!strcmp(cgConfigParam[ilEntry],"IFNAME")) {
	for (entry=0; entry <= NUM_INTF; entry++) {
	  pclRef = Section[entry];
	  if (!strcmp(pclData, pclRef)) *interface = entry;
	} 
      }
      else if (!strcmp(cgConfigParam[ilEntry],"TIME")) {
	if (!strcmp(pclData,"LOCAL")) {
	  *timeType = LOCAL;

	}
	else if (!strcmp(pclData,"UTC")) {
	  *timeType = UTC;
	}
	else if (!strcmp(pclData,"SQLCLS")) {
	  if (pclData != NULL) strcpy(cgSqlString,pclData);
	  else {
	    dbg(TRACE, "<getSelection> Fatal -- empty SQL clause not allowed");
	    return RC_FAIL;
	  }
	}
	else {
	  /* error -- illegal time type */
	}
      }
      else if (!strcmp(cgConfigParam[ilEntry],"PRIO")) {
	*quePrio = atoi(pclData);
      }
    }
    ilEntry++;
  }
  ilEntry = 0;
  if (config[igIfType].toggle_overrule == DEFAULT) {
    igToggleOverrule[igIfType] = TOGGLE_OVERRULE_DEF[igIfType];
  }
  else if (config[igIfType].toggle_overrule == YES) {
    igToggleOverrule[igIfType] = TRUE;
  }
  else if (config[igIfType].toggle_overrule == NO) {
    igToggleOverrule[igIfType] = FALSE;
  }
  if (igToggleOverrule[igIfType] == TRUE) {
    while (cgConfig[ilEntry] != NULL) {
      pclData = getItemDataByName(pcgConfigList,cgConfig[ilEntry],0);
      if (pclData != NULL) {
	/* validate  this key/item pair */
	setSelConf(cgConfig[ilEntry], pclData);
      }
      ilEntry++;
    }
  }
  return RC_SUCCESS;
}
/******************************************************************************/
/* Initialization							      */
/******************************************************************************/
static void setSelConf(char *cpKey, char *cpValue)
{
  if (!strcmp(cpKey, "FIRST_SECTOR_DATE")) {
    igFirstSectorDate[igIfType] = FALSE;
  }
  else if (!strcmp(cpKey, "ALLOW_INSERTS")) {
    igAllowInserts[igIfType] = FALSE;
  }
  else if (!strcmp(cpKey, "ALLOW_DELETES")) {
    igAllowDeletes[igIfType] = FALSE;
  }
  else if (!strcmp(cpKey, "MIXED_UPDATES")) {
    igMixedUpdates[igIfType] = FALSE;
  }
  else if (!strcmp(cpKey, "DELETION_FRAME")) {
    igDelFram[igIfType] = 30;
  }
  else if (!strcmp(cpKey, "UPDATE_EMPTY")) {
   igUpdateEmpty[igIfType] = FALSE; 
  }
  else if (!strcmp(cpKey, "BROADCAST")) {
    igBroadcast[igIfType] = FALSE;
  }
  else if (!strcmp(cpKey, "INPUT_FIELDS")) {
    /* clInpFields */
  }
  else if (!strcmp(cpKey, "FLD1_PROPERTIES")) {
    /*
    sgFld1Prop.table;
    sgFld1Prop.field;
    sgFld1Prop.len;
    sgFld1Prop.type;
    */
  }
}  
/******************************************************************************/
/* Setup initial values before reading the config                             */
/******************************************************************************/
static void config_init()
{
  int i;

  for (i=0; i<NUM_INTF; i++) {
    config[i].local_time = DEFAULT;
    config[i].first_sector_date = DEFAULT;
    config[i].allow_inserts = DEFAULT;
    config[i].allow_deletes = DEFAULT;
    config[i].mixed_updates = DEFAULT;
    config[i].update_empty = DEFAULT;
    config[i].broadcast = DEFAULT;
    config[i].toggle_overrule = DEFAULT;
    config[i].input_fields = NULL;
    config[i].fld1_properties = NULL;
    config[i].deletion_frame = -1;
    config[i].priority = -1;
  }
}

/******************************************************************************/
/* Now read available entries and update config                            */
/******************************************************************************/

static void setup_config()
{
  int entry;
  int value;
  int rtc;

  for (entry=0; entry<=NUM_INTF; entry++) {
    rtc = get_bool(Section[entry], "LOCAL_TIMES", entry, &value);
    if (rtc == E_SECTION) {
      config[entry].section_configured = FALSE;
      continue; /* skip the rest for this interface if section is missing */
    }
    else if (rtc == RC_SUCCESS) config[entry].local_time = value;
    
    config[entry].section_configured = TRUE;
    
    rtc = get_bool(Section[entry], "FIRST_SECTOR_DATE", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].first_sector_date = value;
      if (value == YES) igFirstSectorDate[entry] = TRUE;
      else if (value == NO) igFirstSectorDate[entry] = FALSE;
    }
    rtc = get_bool(Section[entry], "ALLOW_INSERTS", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].allow_inserts = value;
      if (value == YES) igAllowInserts[entry] = TRUE;
      else if (value == NO) igAllowInserts[entry] = FALSE;
    }
    rtc = get_bool(Section[entry], "ALLOW_DELETES", entry, &value);

    if (rtc == RC_SUCCESS) {
      config[entry].allow_deletes = value;
      if (value == YES) igAllowDeletes[entry] = TRUE;
      else if (value == NO) igAllowDeletes[entry] = FALSE;
    }
    rtc = get_bool(Section[entry], "MIXED_UPDATES", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].mixed_updates = value;
      if (value == YES) igMixedUpdates[entry] = TRUE;
      else if (value == NO) igMixedUpdates[entry] = FALSE;
    }
    rtc = get_int(Section[entry],  "DELETION_FRAME", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].deletion_frame = value;
      igDelFram[entry] = value;
    }
    rtc = get_int(Section[entry],  "PRIORITY", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].priority = value;
      igPrio[entry] = value;
      igPrioToElse = value;
    }
    rtc = get_bool(Section[entry], "UPDATE_EMPTY", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].update_empty = value;
      if (value == YES) igUpdateEmpty[entry] = TRUE;
      else if (value == NO) igUpdateEmpty[entry] = FALSE;
    }
    rtc = get_bool(Section[entry], "BROADCAST", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].broadcast = value;
      if (value == YES) igBroadcast[entry] = TRUE;
      else if (value == NO) igBroadcast[entry] = FALSE;
    }
    rtc = get_bool(Section[entry], "TOGGLE_OVERRULE", entry, &value);
    if (rtc == RC_SUCCESS) {
      config[entry].toggle_overrule = value;
      if (value == YES) igToggleOverrule[entry] = TRUE;
      else if (value == NO) igToggleOverrule[entry] = FALSE;
    }
    rtc =  get_int(Section[entry],  "WRAPPER_MODID", entry, &value);
    if (rtc == RC_SUCCESS) {
      igQueToElse = value;
    }
  }
  rtc =  get_int("MAIN", "MAX_DELAY_SCHED", 0, &value);
  if (rtc == RC_SUCCESS) {
    igMaxMinAfterSched = value;
  }
  rtc =  get_int("MAIN", "MAX_BEFORE_SCHED", 0, &value);
  if (rtc == RC_SUCCESS) {
    igMaxMinBeforeSched = value;
  }
  rtc =  get_int("MAIN", "SUP_NBC_DAYS", 0, &value);
  if (rtc == RC_SUCCESS) {
    igNBCInterval = value;
  }
  rtc =  get_int("MAIN", "SET_FTYP_OPERATIONAL_DAYS", 0, &value);
  if (rtc == RC_SUCCESS) {
    igSchedInterval = value;
  }
  rtc = get_bool("MAIN", "STOP_RECOVERY_ON_ERROR", 0, &value);
  if (rtc == RC_SUCCESS) {
    if (value == YES) igStopOnError = TRUE;
    else igStopOnError = FALSE;
  }
  rtc = get_bool("MAIN", "ALLOW_REPAIR", 0, &value);
  if (rtc == RC_SUCCESS) {
    if (value == YES) igAllowRepair = TRUE;
    else igAllowRepair = FALSE;
  }
  rtc = get_bool("MAIN", "TEST_REPAIR", 0, &value);
  if (rtc == RC_SUCCESS) {
    if (value == YES) igTestRepair = TRUE;
    else igTestRepair = FALSE;
  }
  rtc = get_bool("MAIN", "ENABLE_RECOVERY", 0, &value);
  if (rtc == RC_SUCCESS) {
    if (value == YES) igEnableRecover = TRUE;
    else if (value == NO) igEnableRecover = FALSE;
  }
  rtc = get_bool("MAIN", "USE_NEW_SEARCH_ALG", 0, &value);
  if (rtc == RC_SUCCESS) {
    if (value == YES) igNewSearchAlg = TRUE;
    else igNewSearchAlg = FALSE;
  }
  rtc = getStringValue("LOGOPTIONS", "LOG_FOUND_FLIGHT_FIELDS",0,&cgLogAdditionalFields);

  rtc = ReadConfigEntry("ELSE", "ETA_FRAME",cgETA_FRAME);
  if(rtc != RC_SUCCESS) strcpy(cgETA_FRAME,"0");
  igETA_FRAME = atoi(cgETA_FRAME);

  //FRANK
  rtc = ReadConfigEntry("MAIN", "RUN_MODE",cgRUN_MODE);
  if(rtc != RC_SUCCESS)
  {
	strcpy(cgETA_FRAME,"0");
	igRUN_MODE = 0;
	dbg(DEBUG,"line<%d>the run mode is <REAL>igRUN_MODE<0>",__LINE__);
  }
  else if(strncmp(cgRUN_MODE,"TEST",4)==0)
  {
	igRUN_MODE = 1;
	dbg(DEBUG,"line<%d>the run mode is <%s>igRUN_MODE<1>",__LINE__,cgRUN_MODE);
  }
}

/*
** Fetch a boolean value from cfg
**
** Input:	Section -- name of the related section 
**		field	-- name of the entry
**		entry	-- entry to a specific interface table
**
** Output:	None
**
** Returns:	value contains the fetched value if rtc == RC_SUCCESS
**		rtc contains the message provided from the ReadConfigEntry
**		function, i.e E_SECTION or E_PARAM in case of error
**
** Description: 
**	Reads a value from impman.cfg. 
** 	If the requested section was not found an error message is printed. 
** 	If the requested field was not found, this is silently ignored.
**	If a value of 'YES' or 'NO' is returned, an appropriate value is 
**	written to the config (the value is no longer treated as default,
**	even if we have read a value that equals the default value!).
**	If the value is not 'YES' or 'NO', an error message is printed
*/  
static int get_bool(char *Section, char *field, uint entry, int *data)
{
  int rtc=RC_SUCCESS;
  char value[1024];

     rtc = ReadConfigEntry(Section, field, value);
     if (rtc<0) {
	/*
	** no entry or missing section
	*/
	if (rtc == E_PARAM) return rtc;	/* this parameter wasn't found in cfg -- keep default */
	else if (rtc == E_SECTION) {
		dbg(TRACE,"<get_bool> Section %s not found in impman.cfg !", Section);
		return rtc;
	}
     }
     if (!strcmp(value,"YES")) {
	*data = YES;
	}
     else if (!strcmp(value,"NO")) {
	*data = NO;
	}
     else {
	   dbg(TRACE,"<get_bool> Illegal Value %s for field %s in Section %s !", value, field, Section);
	   *data = -1;
	   return RC_FAIL;
	}
     return RC_SUCCESS;
}

/*
** Fetch an integer value from cfg
**
** Input:	Section -- name of the related section 
**		field	-- name of the entry
**		entry	-- entry to a specific interface table
**
** Output:	None
**
** Returns:	value contains the fetched value if rtc == RC_SUCCESS
**		rtc contains the message provided from the ReadConfigEntry
**		function, i.e E_SECTION or E_PARAM in case of error
**
** Description: 
**	Reads a value from impman.cfg. 
** 	If the requested section was not found an error message is printed. 
** 	If the requested field was not found, this is silently ignored.
**	If a value between 0 and 32000 is returned, an appropriate value is 
**	written to the config (the value is no longer treated as default,
**	even if we have read a value that equals the default value!).
**	If the value is not in range, an error message is printed
*/
static int get_int(char *Section, char *field, uint entry, int *data)
{
  int rtc = RC_SUCCESS;
  char value[1024];
  int intval;

  rtc = ReadConfigEntry(Section, field, value);
  if (rtc<0) {
    /*
    ** no entry or missing section
    */
    if (rtc == E_PARAM) return rtc;	/* this parameter wasn't found in cfg -- keep default */
    else if (rtc == E_SECTION) {
      dbg(TRACE,"<get_int> Section %s not found in impman.cfg !", Section);
      return rtc;
    }
  }
  intval = atoi(value); /* Note: using atoi without prechecking the string maybe dangeroes! */
  if (intval > 0 && intval <MAX_INT_PARAMETER) { 
    *data = intval;
    return RC_SUCCESS;
  }
  else {
    *data = -1;
    dbg(TRACE,"<get_int> Illegal Value %s for field %s in Section %s !", value, field, Section);
    return RC_FAIL;
  }
}
/** getStringValue                                                            
** Input:	Section -- name of the related section 
**		field	-- name of the entry
**		entry	-- entry to a specific interface table
**
** Output:	None
**
** Returns:	value contains the fetched value if rtc == RC_SUCCESS
**		rtc contains the message provided from the ReadConfigEntry
**		function, i.e E_SECTION or E_PARAM in case of error
**/
static int getStringValue(char *Section, char *field, uint entry, char **dest)
{
  int rtc = RC_SUCCESS;
  char value[1024];
  int len;

  rtc = ReadConfigEntry(Section, field, value);
  if (rtc<0) {
    /*
    ** no entry or missing section
    */
    if (*dest != NULL) {
      free (*dest);
      *dest = NULL;
    }
    if (rtc == E_PARAM) return rtc;	/* this parameter wasn't found in cfg -- keep default */
    else if (rtc == E_SECTION) {
      dbg(TRACE,"<getStringValue> Section %s not found in impman.cfg !", Section);
      return rtc;
    }
  }

  checkFieldValid(value);
  len = strlen(value) + 1;
  *dest = (char *)malloc(len);
  strcpy (*dest,value);
  dbg(TRACE,"<getStringValue> Getting <%s> with len <%d>", value,len);
  return RC_SUCCESS;
}
/******************************************************************************/
/* checkFieldValid                                                            */
/*                                                                            */
/* Input: Fieldlist to be checked                                             */
/*                                                                            */
/* scan the list of fields and check if they are valid i.e.contained in AFT   */
/*                                                                            */
/******************************************************************************/
static void checkFieldValid(char *pclFieldList)
{
  char clFieldList[1024];
  char *pclField;
  char *pclFieldRef;

  strcpy(clFieldList,pclFieldList);
  pclField = strtok(clFieldList,",");
  if (pclField == NULL) return;
  strcpy(pclFieldList,"");
  pclFieldRef = getItemDataByName(pcgAFTLenInfo,pclField,0);
  if (pclFieldRef!=NULL) strcpy(pclFieldList,pclField);
  while ((pclField = strtok(NULL,",")) != NULL) {
    pclFieldRef = getItemDataByName(pcgAFTLenInfo,pclField,0);
    if (pclFieldRef != NULL || !strcmp(pclField,"FLDA")) {
      strcat(pclFieldList,",");
      strcat(pclFieldList,pclField);
    }
    else dbg(TRACE,"<checkFieldValid> WARNING invalid field <%s> specified, removed from list",pclField);
  }
  dbg(DEBUG,"<checkFieldValid> Resulting field list: <%s>",pclFieldList);
}
/******************************************************************************/
/* checkMaxDataLenOk                                                          */
/*                                                                            */
/* The fielddata is  checked against the possible max len in database         */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static int checkMaxDataLenOk(char *cpField, char *cpData)
{
  char *pclFieldData;
  uint ilMaxLen;
  uint ilStrLen;

  pclFieldData = getItemDataByName(pcgAFTLenInfo, cpField, 0);
  if (pclFieldData != NULL) {
    ilMaxLen = atoi(pclFieldData);
    ilStrLen = strlen(cpData);
    if (ilStrLen <= ilMaxLen) {
      return TRUE;
    }
    else {
      dbg(TRACE, "<checkMaxDataLenOk> Invalid data length <%d> in field <%s> max is <%d>",
	  ilStrLen, cpField, ilMaxLen);
      return FALSE;
    }
  }
  else {
    dbg(TRACE, "<checkMaxDataLenOk> Unknown field <%s>", cpField);
    return FALSE;
  }
}
/******************************************************************************/
/* initAFTLenTable                                                            */
/*                                                                            */
/* Reads the column length of each relevant field in AFTTAB. Puts the         */
/* key/item pairs into a list                                                 */
/*                                                                            */
/* Input:                                                                     */
/*       cpTableList -- list to be filled                                     */
/*                                                                            */
/* Return:                                                                    */
/*       RC_FAIL/RC_SUCCESS                                                   */
/*                                                                            */
/******************************************************************************/
static int initAFTLenTable(ListPtr *cpTableList)
{
  char clTableName[7];
  char clFieldList[2048];
  char clDataList[1024];
  char *clField;
  char *clData;
  uint ilEntry = 0;
  int  ilGetRc;
  short slFkt,slCursor;
  uint ilFirst = TRUE;

  strcpy(clTableName, "AFTTAB");
  while (cgAFTField[ilEntry] != NULL) {
    if (ilEntry == 0) {
      strcpy(clFieldList, "'");
      strcat(clFieldList, cgAFTField[ilEntry]);
      strcat(clFieldList, "'");
    }
    else {
      strcat(clFieldList, ",'");
      strcat(clFieldList, cgAFTField[ilEntry]);
      strcat(clFieldList, "'");
    }
    ilEntry++;
  }

  sprintf(pcgSqlBuf, "SELECT COLUMN_NAME,DATA_LENGTH FROM USER_TAB_COLUMNS WHERE TABLE_NAME='%s' AND COLUMN_NAME IN (%s)", clTableName, clFieldList); 

  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea)) == DB_SUCCESS) {
    if (ilGetRc == DB_SUCCESS) {
      BuildItemBuffer(pcgDataArea, NULL, ilEntry, ",");
      clField = strtok(pcgDataArea, ",");
      clData = strtok(NULL, ","); 
     if (ilFirst) {
       strcpy(clFieldList, clField);
       strcpy(clDataList, clData);
       ilFirst = FALSE;
     }
     else {
       strcat(clFieldList, ",");
       strcat(clFieldList, clField);
       strcat(clDataList, ",");
       strcat(clDataList, clData);
     }
    }
    else {
      dbg(TRACE, "<initAFTLenTable> Error -- Statement was: <%s>", pcgSqlBuf);
    return RC_FAIL;
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);

  createItemList(clFieldList, cpTableList);
  addDataItemToList(clDataList, cpTableList, 0, ',', IS_VALID);
  return RC_SUCCESS;
}


static void CheckEstimatedTime(char *pcpFields, char *pcpData, char *pcpSelection)
{
	int ilItemNumber = 0;
	int ilItemNumber1 = 0;
	int i = 0;
	char clTemp[50] = "\0";
	char clTemp1[500] = "\0";
	char clTemp2[500] = "\0";
	char *pclPtr1 = NULL;
	char *pclPtr2 = NULL;
	char clUrno[20] = "\0";
	char clSTOA[20] = "\0";
	char *pclDataArea;
	int ilRC = RC_SUCCESS;
	char clETA[20] = "0";
	char clADID[4] = "\0";
	short slSqlFunc = 0;
	short slCursor = 0;
	char clSqlSelection[200] = "\0";
	char clFLNO[20] = "\0";
	char clTimeStamp[20];
	char clETA_DB[20] = "\0";

	if((strcmp(pcgIfName,"ELSE") == 0) || strcmp(pcgIfName,"else") == 0)
	{
		dbg(TRACE,"%05d: <CheckEstimatedTime> No ETA-Check for ELSE-Data necessary",__LINE__);
		return;
	}

	if((ilItemNumber = get_item_no(pcpFields,"ETOA",5)+1) > 0)
	{
		get_real_item(clETA,pcpData,ilItemNumber);
	}
	else if((ilItemNumber = get_item_no(pcpFields,"ETAA",5)+1) > 0)
	{
		get_real_item(clETA,pcpData,ilItemNumber);
	}
	else
	{
		dbg(TRACE,"%05d: NO estimated Time of arrival was found! No edtimated-check necessary",__LINE__);
		return;
	}

	if((ilItemNumber = get_item_no(pcpFields,"URNO",5)+1) > 0)
	{
		get_real_item(clUrno,pcpData,ilItemNumber);
	}
	else
	{
		pclPtr1 = strstr(pcpSelection,"=")+1 ;
		strcpy(clUrno,pclPtr1);
	}

	dbg(TRACE,"%05d: <CheckEstimatedTime> URNO: <%s>",__LINE__,clUrno);
	if(strlen(clUrno) <= 0)
	{
		dbg(TRACE,"%05d: <CheckEstimatedTime> NO URNO FOUND... nothing to do...",__LINE__);
		return;
	}


	slSqlFunc = START;
	slCursor = 0;
	
	sprintf(clSqlSelection,"SELECT STOA,ADID,FLNO,ETAA FROM AFTTAB WHERE URNO = %s",clUrno);
	if( (ilRC = sql_if(slSqlFunc,&slCursor,clSqlSelection ,clTemp1)) != DB_SUCCESS)
	{
		dbg(TRACE,"%05d: Record not found: <%s>",__LINE__,clSqlSelection);
		close_my_cursor(&slCursor);
		commit_work();
		return;
	}
	else
	{
		pclDataArea = clTemp1;
		for(i =  1; i < 4; i++) /* !!!!!!!!!!!!!!!!! apply number to number of fields in the selection !!!!!!!!*/
		{
			while(*pclDataArea != '\0')
			{
					pclDataArea++;
			}
			*pclDataArea = ',';
		}
		get_real_item(clSTOA, clTemp1, 1);
		get_real_item(clADID, clTemp1, 2);
		get_real_item(clFLNO, clTemp1, 3);
		get_real_item(clETA_DB, clTemp1, 4);
	}
	
	close_my_cursor(&slCursor);
	commit_work();

	
	dbg(TRACE,"%05d: clSTOA: <%s>, clADID: <%s>, clFLNO <%s>",__LINE__,clSTOA,clADID,clFLNO);
	if((strstr(clFLNO,"SQ") == NULL) && (strstr(clFLNO,"MI") == NULL))
	{
		dbg(TRACE,"%05d: <CheckEstimatedTime> This is no SQ/MI-Flight. No Check for estimated Times will be done!!!!!!",__LINE__);
		return;
	}
	
	if(clADID[0] == 'A') /*in the case STOA was retrieved by reading the DB its not know when reading if it is an arrival oder departure*/
	{	

		GetServerTimeStamp("UTC", 1, 0, clTimeStamp);
		/*UtcToLocalTimeFixTZ(clTimeStamp);*/
		ilRC = AddSecondsToCEDATime(clTimeStamp, igETA_FRAME*60 ,1);

		dbg(TRACE,"%05d: pcgIfName: <%s> clTimeStamp(now + %d Min): <%s>, clSTOA:<%s> clETA_DB: <%s>",__LINE__,pcgIfName,igETA_FRAME,clTimeStamp,clSTOA,clETA_DB);
		if((strcmp(clTimeStamp, clSTOA) <= 0) || (strlen(clETA_DB) == 0))/*now-time less than 45 min to stoa or no ETA is set*/
		{
			dbg(TRACE,"%05d: ETA <%s> from CSKED accepted",__LINE__,clETA);
		}
		else
		{
			dbg(TRACE,"%05d: ETA: <%s> from CSKED NOT accepted",__LINE__,clETA);
			clTemp1[0] = '\0';
			clTemp2[0] = '\0';
			i = 1;
			while (get_real_item(clTemp,pcpFields,i) != -1)
			{
				if(strcmp(clTemp,"ETAA") != 0)
				{
					strcat(clTemp1,clTemp);
					strcat(clTemp1,",");
					get_real_item(clTemp,pcpData,i);
					strcat(clTemp2,clTemp);
					strcat(clTemp2,",");
				}
				i++;
			}
			strcpy(pcpFields,clTemp1);
			strcpy(pcpData,clTemp2);
			
		}
	}
		
}

