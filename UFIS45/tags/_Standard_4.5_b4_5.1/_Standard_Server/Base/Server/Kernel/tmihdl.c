#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/tmihdl.c 1.1 2003/03/19 16:52:06SGT fei Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  GRM                                                      */
/* Date           :  28.6.01                                                  */
/* Description    :  Testserver quelink handler                               */
/*                   1.) logging of data coming from operativsystem by TCP/IP */
/*					 2.) direct send of specified data to testsystem coming   */
/*                       from operativsystem by TCP/IP                        */
/* Update history :  opserver auf xxx f�r kein connect gepr�ft 20.07.01       */
/* 20020613 JIM: usage of strsignal only on _SOLARIS                          */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="%Z% UFIS44 (c) ABB AAT/I %M% %I% / %E% %U% / VBL";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define __EXTENSIONS__

#undef MORE_DEBUG_TMI
/* #define MORE_DEBUG_TMI */

/* #define USAGE OF SEMAPHORE */
#undef TSM_USE_SEMAPHORE

/* Includes the whole bunch ... */
#include "tsmhdl.h"


/*
 * External variables                                                         
 */
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;

/*
 * Global variables 
 */
static ITEM  	*prgItem      	= NULL;                /* The queue item pointer  */
static ITEM  	*prgItem1      	= NULL;                /* The queue item pointer  */
static EVENT 	*prgEvent     	= NULL;                /* The event pointer       */
static EVENT 	*prgEvent1     	= NULL;                /* The event pointer       */
static ITEM		*prglItem ;		/* Local, extraction of Netto Tel + Mod_Id (Testsystem).. */

static int   	igItemLen     	= 0;                   /* length of incoming item */
static int   	igSocket	= -1;
static int   	igSocket1	= -1 ;
static int   	igInitOK      	= FALSE;

static int   	igConnectRequest= TRUE;

static CONF	prgConfig;
static int   	igConnected   	= 0;
static int   	igmaxfd	   	= 0 ;
static int 	igtsmstart 	= 0 ;
static fd_set   fdgallset;
static fd_set   fdgreadset;

	  					/* Get these Parameters from */
						/* Config File (strings only)*/
static t_cfgparam rgCfgTable[] = {
	{ "STATUS",	"TSMHDL",			CFG_STRING,	NULL },  	/* SENDER if Operativsystem, RECEIVER if Testsystem */
	{ "MAIN",	"OPERATIVSERVER",	CFG_STRING,	NULL },      	/* Name of Operativserver */
	{ "MAIN",	"TESTSERVER",		CFG_STRING,	NULL }, 	/* Name of Testserver (client) */
	{ "MAIN",	"TESTSERVICE",		CFG_STRING,	NULL }, 	/* Testservice */
	{ "MAIN",	"QUETOUT",			CFG_INT,	NULL },		/* QUEUE Timeout */
	{ "MAIN",	"RECONNTOUT",		CFG_INT,	NULL },   	/* Reconnect Timeout */
	{ "MAIN",	"MAINCONNTOUT",		CFG_INT,	NULL },  	/* First Connect Timeout */
	{ "MAIN",	"EVTPRINTFILE",		CFG_STRING,	NULL },        	 
	{ "MAIN",	"EVTPRINTCOUNT",	CFG_INT,	NULL },        	 
	{ "MAIN",	"EVTPRINTRECORDS",	CFG_INT,	NULL },        	 
	{ "MAIN",	"EVTPRINTCLOSE",	CFG_INT,	NULL },        	 
/* End of Records - hands off */
	{ NULL,		NULL,			0,		NULL }
} ;

/*
 * Function prototypes
 */

static int TMIWriteEvent(BC_HEAD *prpHead, CMDBLK *prpCmd,int ilkind);

static int 	Init_TSM();
static int	Reset(void);                       /* Reset program          */
static void	Terminate(void);                   /* Terminate program      */
static int	InstallConnection(void);            /* Installs TCP Connection*/
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(int);                  /* Handles event data     */
static void 	AckItem();				   /* Send ACK into queue    */
static void 	FreeCfgValues();			   /* free value ptrs in Cfg */
static void 	*vpGetConfValue( char *pcpConfName );  /* Return Value for Cfg   */
static void 	InitSignals( void );		   /* Initialize signals     */
static void 	remove_file();
static void 	create_file();
static void 	ini_client();
static int  	connect_to_prodserver();
static int  	ifmin(int i,int j);
static 	void delay (long msec);

static int 	TcpConnect_tsm(  char *servicename, char *servername );

extern int 	que(int func,int route,int module,int priority,int len,char *msg);
extern int 	queGB(int func,int route,int module,int priority,int *len,void *msg);
extern void GetServerTimeStamp(char *pcpType,int ipFormat,long lpTimediff,char *pcpTimeStamp);
extern int 	init_que( void );
extern int 	snap(char *pmem,long lim,FILE *outp);
#ifdef TSM_USE_SEMAPHORE
extern int	setsem(int id);
extern int	tstsem(int id);
extern int	clrsem(int id);
#endif

static	struct servent		*servent ;	/* ptr service db */
static	struct hostent		*hostent ;	/* ptr host db */
static	char	*servername ,*opservername;			/* TCP Comp. Name */
static	char	*servicename ;			/* Service name (remote) */

static	struct sockaddr_in	cliaddr,servaddr;	/* Listener, client addr structs */
static	int			igservlen ;	/* Length addr struct */
static	struct in_addr		*inaddr;	/* yet another address ptr */

static	int	igSendEvent = FALSE;
static	int	igSendQueue = FALSE;

/*--------------------------------------------------------------------------------
 *
 * The MAIN program
 *
 *--------------------------------------------------------------------------------*/
void main( int argc, char **argv )
{
	int	ilRC = RC_SUCCESS;			/* Return code		*/
	int	ilCnt = 0;
	char	*pclMod ;


	/*
	 *  init:  Log, Module, Cmd line processing
	 */
# ifdef USE_MACRO_INIT			/* Achtung: Macro ist fehlerhaft */
	INITIALIZE;			/* General initialization	 */
# else
	mod_name = argv[0];
	pclMod = strrchr( mod_name, '/' );
	if( pclMod )
		strcpy( mod_name, pclMod + 1 ) ;

# ifdef PID_LOGFILE
# error "PID_LOGFILE defined"
        sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"), mod_name ,getpid());
# else
        sprintf(__CedaLogFile,"%s/%s%05d.log",getenv("DBG_PATH"), mod_name ,getpid());
# endif

        outp = fopen(__CedaLogFile,"w");
        if (argc == 3) 
        {
                mod_id   = atoi(argv[1]);
                ctrl_sta = atoi(argv[2]);
        }
		else
		{
			fprintf( stderr, "syntax: %s mod_id status\n", mod_name );
			sleep(30);
			exit(1);
		}
        glbrc = init();
        if(glbrc)
        {
                fprintf(outp,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc);\
                exit(1);
        }
# endif

	InitSignals();				

	dbg(TRACE,"MAIN: version <%s>",sccs_version);

	/*----------------------------------------------------------------------
	 * Attach to the MIKE queues
	 *----------------------------------------------------------------------*/
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(60);
			ilCnt++;
		}/* end of if */
	} while((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if( ilRC != RC_SUCCESS )
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else 
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	} /* end of if */


	/*----------------------------------------------------------------------
	 *  configuration
	 *----------------------------------------------------------------------*/

	dbg(TRACE,"MAIN: initializing ...");
	
if( Init_TSM() != RC_SUCCESS)
	{
		dbg(TRACE,"Init_TSM: init failed!");
		Terminate();
	} /* end of if */

	dbg(TRACE,"MAIN: initializing OK");

	/*----------------------------------------------------------------------
	 * main application loop
	 * not expected to be leaved but terminated inside sub-functions
	 *----------------------------------------------------------------------*/

				
	ini_client();
	for(;;)
	{
		ilRC = queGB(QUE_GETBIGNW,0,mod_id,PRIORITY_3,&igItemLen,(void *)&prgItem);

		/*
		 * depending on the size of the received item
		 * a realloc could be made by the que function
		 * so do never forget to set event pointer !!!
		 */

		if( ilRC == RC_SUCCESS )
		{
			prgEvent = (EVENT *) prgItem->text;
			prgItem1 = (ITEM *) prgItem->text;
			prgEvent1 = (EVENT *) prgItem1->text;
#ifdef MORE_DEBUG_TMI
			dbg(DEBUG,"Main: got %d : i-func = %d,if = %d",prgEvent->command ,prgItem->function ,prgItem1->function);
#endif
			/* check kind of request */

			igSendEvent = FALSE;
			igSendQueue = FALSE;
			if (prgItem1->function >= 3*TSMACTIONOFFSET)
			{
				igSendEvent = TRUE;
				igSendQueue = TRUE;
				prgItem1->function -= 3*TSMACTIONOFFSET;
			}
			else
				if (prgItem1->function >= 2*TSMACTIONOFFSET)
			{
				igSendEvent = TRUE;
				prgItem1->function -= 2*TSMACTIONOFFSET;
			}
			else
				if (prgItem1->function >= 1*TSMACTIONOFFSET)
			{
				igSendQueue = TRUE;
				prgItem1->function -= 1*TSMACTIONOFFSET;
			}
				
			/* Acknowledge the item - error processing inside */
						
			AckItem();

			switch( prgEvent->command )
			{

				case	SHUTDOWN	: /* process shutdown - maybe from uutil */
					Terminate();
					break;
					
				case	RESET		: /* reload configuration */
					ilRC = Reset();
					break;
					
				case	EVENT_DATA : /* from CEDA queue events */
				case    QUE_GET	:
				case	QUE_CREATE:
				case	QUE_ACK:
#ifdef MORE_DEBUG_TMI
					dbg(DEBUG,"Main: vor Handle Data (1) queue Eintrag angekommen");
#endif
					ilRC = HandleData(1);
					if(ilRC != RC_SUCCESS)
					{
						dbg(TRACE,"Main: nach Handle Data RC_FAIL");
						HandleErr(ilRC);
					}
					else
					{
#ifdef MORE_DEBUG_TMI
						dbg(TRACE,"Main: nach Handle Data RC_SUCCESS");
#endif
					}
					break;
					
				case	TRACE_ON :
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;

				default	:	/* ignore */
					delay(200);
					break;
			} /* end switch */
		}
		else
			if (igConnectRequest == TRUE && igConnected == 0)
		{
#ifdef MORE_DEBUG_TMI
			dbg(TRACE,"Main: Clientpart: Try to connect");
#endif
			if (connect_to_prodserver() == RC_SUCCESS)
			{
							
				/* create files to show connect and to be a client*/
					
				create_file ();	/* create both file */
					
#ifdef MORE_DEBUG_TMI
				dbg(TRACE,"Main: Clientpart: connect done ok, igConnectRequest = %d",igConnectRequest);
#endif
			}
			else
			{
				dbg(DEBUG,"Main: Clientpart: connect not done, igConnectRequest = %d",igConnectRequest);
				delay(200);
			}
		}
		else
		{
			igSendEvent = FALSE;
			igSendQueue = FALSE;
			ilRC = HandleData(0);
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Main: nach Handle Data RC_FAIL");
				HandleErr(ilRC);
			}
			delay(200);
		}
	}
/* not reached
 *	exit(0);
 */
	
} /* end of MAIN */

/******************************************************************************/
/* signal initialization routine                                              */
/******************************************************************************/
static void InitSignals()
{
	struct	sigaction	sa ;		/* sigaction ctl record */
						/* list of signals to be set */
	int			signo[] = { SIGTERM, SIGPIPE, SIGALRM, SIGCHLD };
						/* number of signals */
	int			nsigno  = sizeof( signo ) / sizeof( signo[0] ) ;
	int			i ;		/* index */
	int			rc ;		/* local retcode	*/
	static int		initialized = 0 ;

	if( initialized )			/* called only once 	*/
		return ;

	sigemptyset( &sa.sa_mask );
	sa.sa_flags   = 0 ;			/* no specials */
	sa.sa_handler = HandleSignal ;		/* handler     */

	/*
	 * block delivered signals
	 */
	for( i = 0 ; i < nsigno ; i++ )
	{
		sigaddset( &sa.sa_mask, signo[i] );
	}

	/*
	 * set handler
	 */
	for( i = 0 ; i < nsigno; i++ )
	{
		rc = sigaction( signo[i], &sa, NULL );
		if( rc == RC_SUCCESS )
		{
			dbg( TRACE, "InitSignals: Signal <%d> OK", signo[i] );
		}
		else {
			dbg( TRACE, "InitSignals: Signal <%d> failed (reason=%s)",
					signo[i], strerror(errno) );
		}
	}

	initialized = 1 ;

	return ;
}

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_TSM()
{
	int		ilRC = RC_SUCCESS;			/* Return code */
	char  		clConfigFile[512];			/* config file name */
	t_cfgparam	*prlCfgEntry ;				/* entry single parm */
	char		clCfgString[200] ;			/* temp. conf entry */
	char		*pclServerMode ;			/* Server Mode: SERVER/TEST */

	if( igInitOK )
		return( ilRC );
	/*
	 * now reading from configfile or from database
	 * config entries are controlled and stored in global table rgCfgTable
	 */
	/* don't remove that         * r because tsmhdl.cfg is used by another program */
	sprintf( clConfigFile, "%s/%sr.cfg", getenv("CFG_PATH"), mod_name );
	for( prlCfgEntry = rgCfgTable ; prlCfgEntry->section && ilRC == RC_SUCCESS ; prlCfgEntry++ )
	{
                ilRC = iGetConfigEntry( clConfigFile, 

					prlCfgEntry->section, prlCfgEntry->name, prlCfgEntry->type,
					clCfgString  );
		if( ilRC == RC_SUCCESS )
		{
			switch( prlCfgEntry->type )
			{
			case CFG_STRING:
				prlCfgEntry->value.ptr = strdup( clCfgString );
				if( prlCfgEntry )
				{
					dbg( TRACE, "Init_TSM: %s='%s'", prlCfgEntry->name, prlCfgEntry->value.ptr );
				}
				else
				{
					dbg( TRACE, "Init_TSM: No more space - Need a bigger machine." );
					ilRC = RC_FAIL ;
				}
				break ;

			case CFG_INT:
				prlCfgEntry->value.num = *((int *) clCfgString );
				dbg( TRACE, "Init_TSM: %s=%d", prlCfgEntry->name, prlCfgEntry->value.num );
				break ;
					
			default:
				dbg( TRACE, "Init_TSM: bad config param type %d", prlCfgEntry->type );
				ilRC = RC_FAIL ;
				break ;
			}
		}
		else
		{
			dbg( TRACE, "Init_TSM: Param '%s' not found in file '%s' section '%s'.",
					prlCfgEntry->name, clConfigFile, prlCfgEntry->section );
			ilRC = RC_FAIL ;
		}
	}

	/*
	 * Determine and set servermode
	 * - After that:  Install TCP connection
	 */

	if( ilRC == RC_SUCCESS )
	{
		igInitOK = TRUE;
		pclServerMode = vpGetConfValue( "TSMHDL" );
		prgConfig.pcFilename	= (char *) vpGetConfValue( "EVTPRINTFILE");
		prgConfig.iCount		= (int) vpGetConfValue( "EVTPRINTCOUNT");
		prgConfig.iRecords		= (int) vpGetConfValue( "EVTPRINTRECORDS");
		prgConfig.iCloseCount	= (int) vpGetConfValue( "EVTPRINTCLOSE");
	}

	dbg( TRACE, "Init_TSM: Is it TestServer conf file = %s",clConfigFile);
 	/*
	ilRC = InstallConnection();     ****** will be done on another place ******
        */
	return(ilRC);
	
} /* end of initialize */

/*********************************************************************************
 *  Returns stored Conf Value (void *) for a given Conf Name	
 *  Call is useless without former call of Init_Tsm
 *  Parameter	Conf Parameter Name String
 *  Return	Success
 *			Ptr	(char *)
 *			Integer number (int)
 *			Real number    (float)
 *		Error
 *			NULL	(char *)
 *			-1	numbers 
 *********************************************************************************/
static void *vpGetConfValue( char *pcpConfName )
{
	t_cfgparam	*prlPtr	= rgCfgTable ;

	/*
	 *  loop while name is found and names differ 
	 *  name field and value field of last record are null ptrs
	 */
	while( prlPtr->name && strcmp( prlPtr->name, pcpConfName ) )
		prlPtr++ ;

	switch( prlPtr->type )
	{
	case CFG_STRING:
		return( (void *) prlPtr->value.ptr );
	case CFG_INT:
		return( (void *) prlPtr->value.num );
	default:
		break ;
	}

	dbg( TRACE, "vpGetConfValue: '%s' not found", pcpConfName );
	return( (void *) -1 );
}


/******************************************************************************
 * Reset Program
 * - Empty Queue
 * - Reread Conf File
 ******************************************************************************/
static int Reset()
{
	int	ilRC = RC_SUCCESS;				/* Return code */
	int	ilTmo ;						/* Timeout queue read */

	
	dbg(TRACE,"Reset: now resetting");

	igInitOK = FALSE ;

	/*
	 * Empty queue (w timeout)
	 */
	ilTmo	= (int) vpGetConfValue( "QUETOUT" );
	if( ilTmo == -1 )
		ilTmo = 60 ;	/* not defined or error */

	for (;;)
	{
		alarm( ilTmo );
		ilRC = queGB(QUE_GETBIGNW,0,mod_id,PRIORITY_3,&igItemLen,(char *)&prgItem);
		AckItem();
		if (ilRC != RC_SUCCESS )  break;
	}
	alarm(0);

	/*
	 * close connection
	 */
	if (igSocket >= 0)
	{
		shutdown (igSocket,2);
		close( igSocket );
		igSocket = -1 ;
	}
	if (igSocket1 >= 0)
	{
		shutdown (igSocket1,2);
		close( igSocket1 );
		igSocket1 = -1 ;
	}

	/*
	 * free configuration and restore
	 * - includes re-opening the tcp connection
	 */
	FreeCfgValues();
	Init_TSM() ;

	return ilRC;
	
} /* end of Reset */

/*********************************************************************************
 * Free Ptr Components of Config Table
 *********************************************************************************/
static void FreeCfgValues()
{
	t_cfgparam	*prlPtr	= rgCfgTable ;

	while( prlPtr->name )
	{
		if( prlPtr->type == CFG_STRING )
		{
			if( prlPtr->value.ptr )
				free( prlPtr->value.ptr ) ;
			prlPtr->value.ptr = NULL ;
		}
		prlPtr++ ;
	}
}
	

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{

	dbg(TRACE,"Terminate: now leaving ...");
	

	if( igSocket >= 0 )
	{
		shutdown (igSocket,2);
		dbg( TRACE, "Terminate: close socket %d", igSocket );
		close( igSocket );
		igSocket = -1;
	}
	if( igSocket1 >= 0 )
	{
		shutdown (igSocket1,2);
		dbg( TRACE, "Terminate: close socket1 %d", igSocket1 );
		close( igSocket1 );
		igSocket1 = -1;
	}

	FreeCfgValues();
	sleep (2 /*30*/);
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
/* 20020613 JIM: */
#if defined(_SOLARIS)
	dbg(TRACE,"HandleSignal: signal <%s> received", strsignal( pipSig ) );
#else
	dbg(TRACE,"HandleSignal: signal <%d> received", pipSig );
#endif
	switch(pipSig)
	{
	case SIGALRM:		/* ignore */
	case SIGCHLD:
		break ;

	case SIGPIPE:		/* reconnect	*/
		if( igSocket >= 0 )
		{
			shutdown (igSocket,2);
			dbg( TRACE, "HandleSignal: close socket %d", igSocket );
			close( igSocket );
			igSocket = -1;
		}
		if( igSocket1 >= 0 )
		{
			shutdown (igSocket1,2);
			dbg( TRACE, "HandleSignal: close socket1 %d", igSocket1 );
			close( igSocket1 );
			igSocket1 = -1;
		}
		break ;

	case SIGTERM:		/* disconnect + exit	*/
	default	:
		Terminate();
		break;
	} /* end of switch */

	return ;
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{

	dbg( TRACE, "HandleErr: internal error <%d>", pipErr );

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		/* dbg(TRACE,"<%d> : no messages on queue",pipErr); */	/* is very often */
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(int ilnum)
{
	int	ilRC = RC_SUCCESS;	/* Return code */
	char	*msg = NULL,*msg0,msg1[2049];			/* Msg buffer que() (Server), TcpRecv (Testsystem) */
	int	msglen,flag,mslen ,nready=0;		/* Msg Len received from que() (Server), via TCP (Testsystem) */
 	int	ilKeepalive = 0,ilRecLen = 0;
 	struct linger ling;
 	int	opt_bool = 1;

	struct timeval		tmo  ;
	int			tmosecs;
	
	/* used for send data to disk file GRM evt*/
	BC_HEAD 	*prlBCHead;
	CMDBLK  	*prlCmdBlk;
	char		*pclSelectionKey = NULL;
	char		*pclFields       = NULL;
	char		*pclData         = NULL;


	/*****************************GRM end evt*/
	
	/*
	 *  With Timeout ?
	 */
	

	tmosecs	= (int) vpGetConfValue( "RECONNTOUT" );
	if( tmosecs == -1 )
			tmosecs = 10 ;	/**** not defined or error ****/

	if( tmosecs > 0 )
	{
		tmo.tv_sec  = tmosecs ;
		tmo.tv_usec = 0 ;
	}

	/* first check if we have to send data to queue  part out of evthdl.c  */



	/*************** send Data to Disk ****************** GRM ***/

#ifdef MORE_DEBUG_TMI_2
	dbg(DEBUG,"HandleData: Testserver mode event = %d queue = %d",igSendEvent,igSendQueue);
#endif

	if (igSendEvent == TRUE && ilnum == 1 && igConnected == 0)
	{
		/* send data to file internal call*/

		/* INITIALIZATIONS */

		prlBCHead = (BC_HEAD *)((char *)prgEvent1 + sizeof(EVENT)); /* Get EVENT head */
 
		if (prlBCHead->rc == RC_FAIL)  			/* Get EVENT cmdblk with errormsg*/
		{
    		 prlCmdBlk = (CMDBLK *)( ((char *)(prlBCHead->data))+strlen(prlBCHead->data)+1);

		}
		else 							/* Get EVENT cmdblk without errormsg*/
  		{
     		prlCmdBlk = (CMDBLK *)((char *)prlBCHead->data);
  		}

		/* JUST FOR DEBUG REMOVE IF FINISHED!!! */
#ifdef MORE_DEBUG_TMI
		dbg(TRACE," ===== NOW PRINTING BC_HEAD STRUCT CONTENT ===== rc = %d RC FAIL = %d",prlBCHead->rc,RC_FAIL);
		dbg(TRACE,"prlBCHead->dest_name	<%s>",prlBCHead->dest_name);
		dbg(TRACE,"prlBCHead->orig_name <%s>",prlBCHead->orig_name);
		dbg(DEBUG,"prlBCHead->recv_name <%s>",prlBCHead->recv_name);
		dbg(DEBUG,"prlBCHead->bc_num 	<%d>",prlBCHead->bc_num);
		dbg(DEBUG,"prlBCHead->seq_id 	<%s>",prlBCHead->seq_id);
		dbg(DEBUG,"prlBCHead->tot_buf 	<%d>",prlBCHead->tot_buf);
		dbg(DEBUG,"prlBCHead->act_buf 	<%d>",prlBCHead->act_buf);
		dbg(DEBUG,"prlBCHead->ref_seq_id <%c>",prlBCHead->ref_seq_id);
		dbg(DEBUG,"prlBCHead->rc 		<%d>",prlBCHead->rc);
		dbg(DEBUG,"prlBCHead->tot_size 	<%d>",prlBCHead->tot_size);
		dbg(DEBUG,"prlBCHead->cmd_size 	<%d>",prlBCHead->cmd_size);
		dbg(DEBUG,"prlBCHead->data_size <%d>",prlBCHead->data_size);
		dbg(DEBUG,"prlBCHead->data 		<%s>",prlBCHead->data);
		dbg(DEBUG," ======================================");

snap((char*) prlCmdBlk,sizeof(CMDBLK),outp);

		dbg(DEBUG," ===== NOW PRINTING Command Block STRUCT CONTENT =====");
		dbg(DEBUG,"prlCmdBlk->command 	<%s>",prlCmdBlk->command );
		dbg(DEBUG,"prlCmdBlk->obj_name 	<%s>",prlCmdBlk->obj_name );
		dbg(DEBUG,"prlCmdBlk->order 	<%s>",prlCmdBlk->order);
		dbg(DEBUG,"prlCmdBlk->tw_start	<%s>",prlCmdBlk->tw_start);
		dbg(DEBUG,"prlCmdBlk->tw_end	<%s>",prlCmdBlk->tw_end);
		dbg(DEBUG,"prlCmdBlk->data 		<%s>",prlCmdBlk->data);
		dbg(DEBUG," ======================================");
#endif
		if(prlBCHead->bc_num < 0)
		{
			dbg(DEBUG,"Skipping already send headder!");
		}
		else
		{
			ilRC = TMIWriteEvent(prlBCHead, prlCmdBlk,0);
		}

	
	} /* end of write Data to disk GRM*/

	/*
        DebugPrintItem( TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        snap( msg, msglen, stdout );    */
	servicename = (char *) vpGetConfValue( "TESTSERVICE" );

    servername  = (char *) vpGetConfValue( "TESTSERVER");

	opservername  = (char *) vpGetConfValue( "OPERATIVSERVER" );
	if (!strcmp (opservername,"xxx")) igConnectRequest = FALSE;

			else igConnectRequest = TRUE;

	ilRC = RC_SUCCESS;

#ifdef MORE_DEBUG_TMI
	dbg(DEBUG,"HandleData: before reconnect ilRC = %d",ilRC);
#endif
	if (igConnectRequest == FALSE) return(ilRC);


	/*
	 *  Receiver: receive queue data via tcp, extract original module id from 
	 *  event data and send message to receiver of original message
	 *  buffer and number of bytes from program
	  */

	/* prglItem->msg_lengthprglItem->msg_lengthErweitertes Telegramm :  Doppelte ITEM - struktur !!! */

	igmaxfd = igSocket;
	FD_ZERO (&fdgallset);
	FD_SET (igSocket,&fdgallset);
	fdgreadset = fdgallset ;

  	nready = -1;
	nready = select( igmaxfd + 1, &fdgreadset, NULL, NULL, tmosecs ? &tmo : NULL );
	if( nready < 0 )
	{
  		dbg(TRACE, "HD:Select.. tmo error nready = %d %d",nready,errno);
		goto errorhandling ;
	}		

	msglen = sizeof(msg1-1);
	mslen = 0;
	for (;;)
	{
		ilRC = recv( igSocket, msg1, msglen,0 ); /* hier muesste msglen die Maxlaenge sein */
		if (ilRC > 0)
		{
			if (mslen == 0)
			{
				if (msg) free (msg);
				msg = malloc (ilRC);
			}
			else
			{
				msg = realloc(msg,ilRC);
			}
			memcpy (msg+mslen,msg1,ilRC);
			mslen+=ilRC;
			if (ilRC < msglen) break;
		}
		else break;
	}
		
	ilRC = 0;
	prglItem  = (ITEM *) msg;		/* Sender Info que() -> TSM	*/
	if( mslen > 4 && prglItem->route < 100000 && prglItem->originator < 100000)   	/* we got data and not ok or test */
	{
			prglItem  = (ITEM *) msg;		/* Sender Info que() -> TSM	*/
#ifdef MORE_DEBUG_TMI_2
			dbg(DEBUG, "HandleData: vor write event oder que(send) func=%d put %d mod_id=%d route=%d len=%d prio = %d",
				prglItem->function,QUE_PUT,prglItem->originator,
				prglItem->route, prglItem->msg_length, prglItem->priority );	
#endif
			dbg(DEBUG, "HandleData: que(send) func=%d put %d mod_id=%d route=%d len=%d prio = %d",
				prglItem->function,QUE_PUT,prglItem->originator,
				prglItem->route, prglItem->msg_length, prglItem->priority );	
			if (prglItem->priority < 0 || prglItem->priority > 10) prglItem->priority = 3;	

			igSendEvent = FALSE;
			igSendQueue = FALSE;
			if (prglItem->function >= 3*TSMACTIONOFFSET)
			{
				igSendEvent = TRUE;
				igSendQueue = TRUE;
				prglItem->function -= 3*TSMACTIONOFFSET;
			}
			else
				if (prglItem->function >= 2*TSMACTIONOFFSET)
			{
				igSendEvent = TRUE;
				prglItem->function -= 2*TSMACTIONOFFSET;
			}
			else
				if (prglItem->function >= 1*TSMACTIONOFFSET)
			{
				igSendQueue = TRUE;
				prglItem->function -= 1*TSMACTIONOFFSET;
			}

			if (igSendEvent == TRUE)
			{
				/* send data to file external call*/

				/* INITIALIZATIONS */

				prlBCHead = (BC_HEAD *)((char *)prglItem + sizeof(EVENT)); /* Get EVENT head */
 
				if (prlBCHead->rc == RC_FAIL)  			/* Get EVENT cmdblk with errormsg*/
				{
    				 prlCmdBlk = (CMDBLK *)( ((char *)(prlBCHead->data))+strlen(prlBCHead->data)+1);

				}
				else 							/* Get EVENT cmdblk without errormsg*/
  				{
     				prlCmdBlk = (CMDBLK *)((char *)prlBCHead->data);
  				}

				/* JUST FOR DEBUG REMOVE IF FINISHED!!! */
#ifdef MORE_DEBUG_TMI_2
				dbg(TRACE," ===== NOW PRINTING BC_HEAD STRUCT CONTENT ===== rc = %d RC FAIL = %d",prlBCHead->rc,RC_FAIL);
				dbg(TRACE,"prlBCHead->dest_name	<%s>",prlBCHead->dest_name);
				dbg(TRACE,"prlBCHead->orig_name <%s>",prlBCHead->orig_name);
				dbg(DEBUG,"prlBCHead->recv_name <%s>",prlBCHead->recv_name);
				dbg(DEBUG,"prlBCHead->bc_num 	<%d>",prlBCHead->bc_num);
				dbg(DEBUG,"prlBCHead->seq_id 	<%s>",prlBCHead->seq_id);
				dbg(DEBUG,"prlBCHead->tot_buf 	<%d>",prlBCHead->tot_buf);
				dbg(DEBUG,"prlBCHead->act_buf 	<%d>",prlBCHead->act_buf);
				dbg(DEBUG,"prlBCHead->ref_seq_id <%c>",prlBCHead->ref_seq_id);
				dbg(DEBUG,"prlBCHead->rc 		<%d>",prlBCHead->rc);
				dbg(DEBUG,"prlBCHead->tot_size 	<%d>",prlBCHead->tot_size);
				dbg(DEBUG,"prlBCHead->cmd_size 	<%d>",prlBCHead->cmd_size);
				dbg(DEBUG,"prlBCHead->data_size <%d>",prlBCHead->data_size);
				dbg(DEBUG,"prlBCHead->data 		<%s>",prlBCHead->data);
				dbg(DEBUG," ======================================");

snap((char*) prlCmdBlk,sizeof(CMDBLK),outp);

				dbg(DEBUG," ===== NOW PRINTING Command Block STRUCT CONTENT =====");
				dbg(DEBUG,"prlCmdBlk->command 	<%s>",prlCmdBlk->command );
				dbg(DEBUG,"prlCmdBlk->obj_name 	<%s>",prlCmdBlk->obj_name );
				dbg(DEBUG,"prlCmdBlk->order 	<%s>",prlCmdBlk->order);
				dbg(DEBUG,"prlCmdBlk->tw_start	<%s>",prlCmdBlk->tw_start);
				dbg(DEBUG,"prlCmdBlk->tw_end	<%s>",prlCmdBlk->tw_end);
				dbg(DEBUG,"prlCmdBlk->data 		<%s>",prlCmdBlk->data);
				dbg(DEBUG," ======================================");
#endif
				if(prlBCHead->bc_num < 0)
				{
					dbg(DEBUG,"Skipping already send headder!");
				}
				else
				{
					ilRC = TMIWriteEvent(prlBCHead, prlCmdBlk,1);
				}

	
			} /* end of write Data to disk GRM*/
			if (igSendQueue == TRUE)
			{

				/*
 				ilRC = que( prglItem->function, prglItem->route,  prglItem->originator,
					prglItem->priority, prglItem->msg_length-sizeof(ITEM)+sizeof(int), (char *) (&prglItem->text[0]));   */
				ilRC = que( prglItem->function, prglItem->route,  prglItem->originator,
					prglItem->priority, prglItem->msg_length, (char *) (prglItem->text));
				
				if (ilRC != RC_SUCCESS) dbg(TRACE, "HandleData: que(send) %s fkt %d route %d prio %d ori %d len %d", "error",
					prglItem->function,prglItem->route,prglItem->priority,prglItem->originator,prglItem->msg_length);
			}
		ilRC = RC_SUCCESS;
	}
	else if (mslen < 0)  	/* we got disconnect */
	{
		dbg(TRACE, "HandleData: TcpRecv rc = %d",ilRC);

		goto errorhandling;
	}
	else if (mslen == 2)
	{
			/* we got ok as handshake */
	}
 	if( msg )	/* free allocated message area */
		free( msg );

	return ilRC;

	/*----------------------------------------------------------------------
	 *  errorhandling
	 *  - save error code
	 *  - close socket
	 *  - socket and connection to 0
	 *----------------------------------------------------------------------*/
errorhandling:
	if( igSocket >= 0 )
	{
		shutdown (igSocket,2);
		close( igSocket );
		igSocket = -1 ;
	}
	if( igSocket1 >= 0 )
	{
		shutdown (igSocket1,2);
		close( igSocket1 );
		igSocket1 = -1;
	}
	igConnected = 0 ;

	dbg( TRACE, "Error Handlig : Connect %s,%s ... %s", servicename, servername, strerror( errno ) );
	return (RC_FAIL);

	
} /* end of HandleData */


/*--------------------------------------------------------------------------------
 *  Acknowledge Item
 *  incl. error processing
 *  no termination, no reaction
 *--------------------------------------------------------------------------------*/
static void AckItem()
{
	int	rc ;

	rc = que(QUE_ACK,0,mod_id,0,0,NULL);
	if( rc != RC_SUCCESS ) 
	{
		/* handle que_ack error */
		HandleQueErr(rc);
	} /* fi */

	return ;
}


static void create_file ()
{
	/* end of send_to_TSM */

	FILE 	*fp;
  	static char *divtmp = NULL;
	static char *divnam = NULL ;
	int	divnamlen = 0 ,ilrc;
	
#ifdef TSM_USE_SEMAPHORE
        ilrc = clrsem(2);
#else

	
	/* at  startup get TSM semaphore filename */
	
	
	if( divtmp == NULL )
 	{
  		divtmp = getenv( "TMP_PATH" );
    		if( divtmp == NULL )
      		{       dbg( TRACE, "TMP_PATH not set" );
        		return ;
        	}
        	divnamlen = strlen( divtmp ) + strlen( TSMACTFILENAME ) + 2 ;
        	divnam = malloc( divnamlen );
        	if( !divnam )
        	{
        		dbg( TRACE, "no more space available" );
        		return ;
        	}
        	
        	sprintf( divnam, "%s/%s", divtmp, TSMACTFILENAME );
        	
       	}
#ifdef MORE_DEBUG_TMI
       	dbg(TRACE, "divnam = %s ",divnam);
#endif
     	fp = fopen(divnam,"w");
     	fclose(fp);
#endif
 	return;	/* CREATE done */
}

static void remove_file ()
{
	/* end of send_to_TSM */
 	
 	static char *divtmp = NULL;
	static char *divnam = NULL ;
	int	divnamlen = 0 ,ilrc;

#ifdef TSM_USE_SEMAPHORE
        ilrc = clrsem(2);
#else
	
	/* at  startup get TSM semaphore filename */
	
	
	if( divtmp == NULL )
 	{
  		divtmp = getenv( "TMP_PATH" );
    		if( divtmp == NULL )
      		{       dbg( TRACE, "TMP_PATH not set" );
        		return ;
        	}
        	divnamlen = strlen( divtmp ) + strlen( TSMACTFILENAME ) + 2 ;
        	divnam = malloc( divnamlen );
        	if( !divnam )
        	{
        		dbg( TRACE, "no more space available" );
        		return ;
        	}
        	
        	sprintf( divnam, "%s/%s", divtmp, TSMACTFILENAME );
        	
       	}
       	remove(divnam);
#endif
 	return;	/* CREATE done */
}
static void ini_client()
{
	int	ilRC = RC_SUCCESS;				/* Return code */
	int	ilTmo;	
	/* Timeout queue read */

	remove_file();
	alarm( ilTmo );
	while( (ilRC = queGB(QUE_GETBIGNW,0,mod_id,PRIORITY_3,&igItemLen,(char *)&prgItem) ) == RC_SUCCESS )
		alarm( ilTmo ) ;
	alarm(0);
	igSocket  = -1;
	igSocket1 = -1;
	return;
}
static int connect_to_prodserver()
{
	int ilRC;
	ilRC = InstallConnection();
#ifdef MORE_DEBUG_TMI
 	dbg( TRACE, "connect to prod ilRC = %d", ilRC);
#endif
	return(ilRC);
 }
/******************************************************************************
 * Installs TCP Connection SERVER -> TESTsystem
 ******************************************************************************/
static int InstallConnection()
{
	int	ilRC = RC_SUCCESS ;		/* Retcode */
	int	rc = RC_SUCCESS ;		/* Retcode */
	int	tmo ;				/* Connection timeout (secs) */
	int	ntry = 3 ;			/* Accept: number of tries */

    servername  = (char *) vpGetConfValue( "TESTSERVER");

	if (!strcmp (servername,"xxx")) igConnectRequest = FALSE;

			else igConnectRequest = TRUE;
    ilRC = RC_FAIL;
	if (igConnectRequest == FALSE) return(ilRC);
	ilRC = RC_SUCCESS;
	if( igSocket < 0 )
	{
		servicename = (char *) vpGetConfValue( "TESTSERVICE" );
		if( !servicename )
		{
			dbg( TRACE, "InstallConnection: Missing Cfg Param '%s'", "TESTSERVICE" );
			return( RC_FAIL );
		}

		tmo	= (int) vpGetConfValue( "MAINCONNTOUT" );
		if( tmo == -1 )
			tmo = 60 ;	/* not defined or error */

		/* Server test connecting */

		servername  = (char *) vpGetConfValue( "OPERATIVSERVER" );
		if( servername )
		{
			rc = TcpConnect_tsm( servicename, servername );
			if (rc == RC_SUCCESS)
			{
#ifdef MORE_DEBUG_TMI
				dbg( TRACE, "InstallConnection: TCPconnect '%s' - ok",
					servername);
#endif
				rc = RC_SUCCESS;
			}
			else
			{
				dbg( TRACE, "InstallConnection: TCPconnect '%s' - not ok error",
						servername);
				rc = RC_FAIL;
			}
		}
		else
		{
			dbg( TRACE, "InstallConnection: Missing Cfg Param '%s'", "OPERATIVSERVER" );
			rc = RC_FAIL ;
		}
	}
	return( rc );
}
	


/*********************************************************************************
 *
 *  TCP interface connect to Produktionserver
 *
 *********************************************************************************/


static int TcpConnect_tsm( char *servicename, char *servername )
{
	int			len ,len1 = 3;		/* data struct len */

	struct in_addr		*inaddr ;	/* ptr addr struct name conversions */
	int			ilRC ,flag;	/* number of written bytes to socket */
 	char 			clbuf[4];
 	char 			clok[] = "ok";

	/*----------------------------------------------------------------------
	 *  establish connection for first call or after
	 *  broken pipe
	 *----------------------------------------------------------------------*/
	if( !igConnected )
	{
		/*
		 * name to structures (one time for connect)
		 */
		hostent = gethostbyname( servername );
		if( !hostent )
		{
			dbg( TRACE, "CONN:get host by name fault");
			goto errorhandling ;
		}
		
		servent = getservbyname( servicename, 0 );
		if( !servent )
		{
			dbg( TRACE, "CONN:get serv by name fault" );
			goto errorhandling ;
		}
		if (servent->s_port == 0) servent->s_port = 3699;
		
#ifdef MORE_DEBUG_TMI
		dbg( TRACE, "Connect %d", servent->s_port );
#endif
 		/*----------------------------------------------------------------------
		 *  initialze
		 *----------------------------------------------------------------------*/
		memset( (char *) &servaddr, 0x00, sizeof( servaddr ) );
		servaddr.sin_family	= AF_INET ;
		servaddr.sin_port	= htons( servent->s_port ) ;
		memcpy( &servaddr.sin_addr, hostent->h_addr, hostent->h_length );

		/*
		 * connect to socket
		 */
		if (igSocket < 0)
		{
			igSocket = socket( servaddr.sin_family, SOCK_STREAM,0 );
			if( igSocket < 0 )
				goto errorhandling ;

			if( connect( igSocket , (struct sockaddr *) &servaddr, sizeof( servaddr ) ) < 0 )
			{
				goto errorhandling ;
			}

			igConnected = 1 ;

			inaddr = (struct in_addr *) hostent->h_addr ;
#ifdef MORE_DEBUG_TMI
			dbg( TRACE, "Connect vor recv %s,%d,fd=%d",    inet_ntoa( *inaddr ),
                                                          servaddr.sin_port,
                                                          igSocket);
#endif
			/* ilRC = TcpRecv(igSocket,&clbuf,&len1);  */
  			flag = 0;
  			ilRC = recv(igSocket,clbuf,len1,flag);
#ifdef MORE_DEBUG_TMI
  			dbg (TRACE, "nach recv ack !!");
#endif
			if ((ilRC > 0) && (clbuf[0] == 'a'))
  			{
  				/*ilRC = TcpSend(igSocket,clok,2);*/
 				ilRC = send(igSocket,clok,2,flag);
 				if (ilRC > 0)
  				{
#ifdef MORE_DEBUG_TMI
					dbg( TRACE, "Connect ... handshake ok");
#endif
    				return (RC_SUCCESS);
  				}
 				else
 				{
					dbg( TRACE, "Connect ... send fault",ilRC);
  					goto errorhandling;
				}
  			}
  			else
  			{
 				dbg( TRACE, "Connect ... receive/handshake fault %d buf = %c errno = %s",ilRC,clbuf,strerror( errno ));
  				goto errorhandling;
 			}
 		}
 		if (igSocket >= 0)
 		{
 			close (igSocket);  	/* line closed */
  			igSocket = -1;
  		}
	}
	else
	{
		dbg( TRACE, "Already Connected ... %s,%d,fd=%d",    inet_ntoa( *inaddr ),
                                                          servaddr.sin_port,
                                                          igSocket );
	}
        return (RC_SUCCESS);
	/*----------------------------------------------------------------------
	 *  errorhandling
	 *  - save error code
	 *  - close socket
	 *  - socket and connection to 0
	 *----------------------------------------------------------------------*/
errorhandling:
	if( igSocket >= 0 )
	{
	 	shutdown (igSocket,2);
		close( igSocket );
		igSocket = -1 ;
	}
	if( igSocket1 >= 0 )
	{
		shutdown (igSocket1,2);
		close( igSocket1 );
		igSocket1 = -1 ;
	}
	igConnected = 0 ;

	dbg( TRACE, "Error Handlig : Connect %s,%s ... %s", servicename, servername, strerror( errno ) );

	return(RC_FAIL);
}


/******************************************************************************/
/* The write event data routine                                               */
/******************************************************************************/
static int TMIWriteEvent(BC_HEAD *prpHead, CMDBLK *prpCmd, int ilkind)
{
	FILE		*prlFile=NULL;
	int			ilRC = RC_FAIL;			/* Return code */
	char		aclAbsoluteFilename[512];
	char		aclTime[32];
	char		*prlData = NULL;
	struct stat	rlOpenstat;
	struct stat	rlClosestat;

	/* insert into the filename the actual number */
	sprintf(aclAbsoluteFilename, "%s/%s.%d", getenv("DBG_PATH"), prgConfig.pcFilename, prgConfig.iCount);

#ifdef MORE_DEBUG_TMI
		dbg(DEBUG, "Filename <%32s>", aclAbsoluteFilename);
#endif
	prlFile = fopen(aclAbsoluteFilename, "a");
	if(prlFile != NULL)
	{
			/* ready to write */
			/*print record start sign*/
			ilRC = fprintf(prlFile, "%c", RECORDSTART);
#ifdef MORE_DEBUG_TMI
			dbg(DEBUG, "ilRC <%d>", ilRC);
#endif
			/* FIXME get servertimestap */
			GetServerTimeStamp("UTC", 1, 0, &aclTime[0]);

			ilRC = fprintf(prlFile, "%.14s", aclTime);

#ifdef MORE_DEBUG_TMI
			dbg(DEBUG, "ilRC <%d> %14s", ilRC,aclTime);
			fflush(0);
#endif
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC FD <%d>", ilRC);
			fflush(0); */

			/*mod_id of event sender (uevent.h) */
			if (ilkind == 0)
			{
				ilRC = fprintf(prlFile, "%d", prgItem1->originator);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> origin %d", ilRC,prgItem1->originator);
				fflush(0);
#endif
				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> route %d", ilRC,prgItem1->route);
				fflush(0);
#endif
				ilRC = fprintf(prlFile, "%d", prgItem1->route);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);


#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> function %d", ilRC,prgItem1->function);
				fflush(0);
#endif
				ilRC = fprintf(prlFile, "%d", prgItem1->function);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				ilRC = fprintf(prlFile, "%d", prgItem1->priority);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> priority %d", ilRC,prgItem1->priority);
				fflush(0);
#endif
				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				ilRC = fprintf(prlFile, "%d", prgItem1->msg_length);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> msg-length %d", ilRC,prgItem1->msg_length);
				fflush(0);
#endif
				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			}
			else
			{
				ilRC = fprintf(prlFile, "%d", prglItem->originator);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> origin %d", ilRC,prglItem->originator);
				fflush(0);
#endif
				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> route %d", ilRC,prglItem->route);
				fflush(0);
#endif
				ilRC = fprintf(prlFile, "%d", prglItem->route);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);


#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> function %d", ilRC,prglItem->function);
				fflush(0);
#endif
				ilRC = fprintf(prlFile, "%d", prglItem->function);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				ilRC = fprintf(prlFile, "%d", prglItem->priority);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> priority %d", ilRC,prglItem->priority);
				fflush(0);
#endif
				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/

				ilRC = fprintf(prlFile, "%d", prglItem->msg_length);

#ifdef MORE_DEBUG_TMI
				dbg(DEBUG, "ilRC <%d> msg-length %d", ilRC,prglItem->msg_length);
				fflush(0);
#endif
				/*field delimiter */

				ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);

				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			}


			/*Headstruct (glbdef.h) */
			ilRC = fprintf(prlFile, "%.10s", prpHead->dest_name);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.10s", prpHead->orig_name);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.10s", prpHead->recv_name);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%d", prpHead->bc_num);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.10s", prpHead->seq_id);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%d", prpHead->tot_buf);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%d", prpHead->act_buf);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.10s", prpHead->ref_seq_id);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%d", prpHead->rc);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%d", prpHead->tot_size);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%d", prpHead->cmd_size);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%d", prpHead->data_size);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*commandstruct */
			/*dbg(DEBUG, "+-------------------------------------------------+");*/
			ilRC = fprintf(prlFile, "%.6s", prpCmd->command);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.33s", prpCmd->obj_name);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.2s", prpCmd->order);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.33s", prpCmd->tw_start);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%.33s", prpCmd->tw_end);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			ilRC = fprintf(prlFile, "%c", FIELDDELIMITER);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			/*dbg(DEBUG, "+------------------------------------------------+");*/
			/*datastruct */
			prlData = prpCmd->data;
			if(strlen(prlData) != 0)
			{
				ilRC = fprintf(prlFile, "%s", prlData);
				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			}
			fprintf(prlFile, "%c", FIELDDELIMITER);
			prlData += (strlen(prlData) +1);
			if(strlen(prlData) != 0)
			{
				ilRC = fprintf(prlFile, "%s", prlData);
				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			}
			fprintf(prlFile, "%c", FIELDDELIMITER);
			prlData += (strlen(prlData) +1);
			if(strlen(prlData) != 0)
			{
				ilRC = fprintf(prlFile, "%s", prlData);
				/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			}
			
			/* record end sign */
			ilRC = fprintf(prlFile, "%c\n", RECORDEND);
			/*dbg(DEBUG, "ilRC <%d>", ilRC);*/
			/*dbg(DEBUG, "+-------------------------------------------------+");*/


			/*close file*/
			fclose(prlFile);

#ifdef MORE_DEBUG_TMI
			dbg(DEBUG, "records: <%d> count: <%d>", prgConfig.iRecords, prgConfig.iCloseCount);
#endif
			/* check if record limit reached*/
			if(prgConfig.iRecords >= prgConfig.iCloseCount)
			{
				/* reset record counter and increase Filecounter*/
				prgConfig.iRecords = 0;
				prgConfig.iCount++;
			}
			else
			{
				prgConfig.iRecords++;
			}

			ilRC = RC_SUCCESS;
	}
	else
	{
		dbg(DEBUG, "unable to open File: <%s>", aclAbsoluteFilename);
	}
#ifdef MORE_DEBUG_TMI
	dbg(DEBUG, "return from TMI: ilRC <%d>", ilRC);
#endif
	return ilRC;
}
void delay (long msec)
{
	struct timeval timer;
	long	usec;
	static char timestamp[32];
	struct tm *ptm;
	time_t secs;
	static long ilstart = 0;
	if (ilstart == 0)
	{
			time(&secs);
			ptm = localtime(&secs);
			sprintf(timestamp,"%2d-%2d-%04d %02d:%02d:%02d\0",
				ptm->tm_mday,
				ptm->tm_mon,
				ptm->tm_year,
				ptm->tm_hour,
				ptm->tm_min,
				ptm->tm_sec);
			dbg( TRACE,"startdelay = %s",timestamp);
	}
	usec = msec*1000;
	timer.tv_sec  = usec/1000000;
	timer.tv_usec = usec - timer.tv_sec*1000000; /* micro sec */
	select (0,NULL,NULL,NULL,&timer);
	if (ilstart == 0)
	{
			time(&secs);
			ptm = localtime(&secs);
			sprintf(timestamp,"%2d-%2d-%04d %02d:%02d:%02d\0",
				ptm->tm_mday,
				ptm->tm_mon,
				ptm->tm_year,
				ptm->tm_hour,
				ptm->tm_min,
				ptm->tm_sec);
			dbg( TRACE,"stopdelay = %s",timestamp);
			ilstart = 1;
	}
	return;
}/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
