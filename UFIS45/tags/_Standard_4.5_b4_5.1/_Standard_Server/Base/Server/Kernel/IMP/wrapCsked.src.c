#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/IMP/wrapCsked.src.c 1.1 2004/12/13 17:51:05SGT usc Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : Uwe Schmeling                                             */
/* Date           : 13.12.2004                                                */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string 

/* Function Prototypes */

static int wrapCskedData(char *pcpData,char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpNewData);

static int wrapCskedData(char *pcpData,char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpNewData)
/* pcpData: The original Data that has to be transformed*/
/* Command, Selection,Fields,Data: the return-values of the function.... input for impman*/
{
  dbg(TRACE,"<wrapCskedData> This is a dummy wrapper module -- CSKED functionality is in project specific implementation");
  return RC_FAIL;
}



