#!/bin/ksh
###
### $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/UfisStart 1.8 2005/06/15 18:47:07SGT jim Exp  $
###
###     #    ######  ######
###    # #   #     # #     #
###   #   #  #     # #     #
###  #     # ######  ######
###  ####### #     # #     #
###  #     # #     # #     #
###  #     # ######  ######  GmbH (c) 1995-2004
###
### D-63150 Heusenstamm
###
### 20021121 JIM: corrected the check for still running sysmon and sysqcp
### 20021121 JIM: included UfisScan and DelIpc
### 20021121 JIM: exit UfisStart when last PNTAB process shows up (and not
###               hard coded ntisch or dbtsrv shows up)
### 20021202 JIM: readded: check if 'echo' needs flag for special chars
### 20030225 JWE: added function to show upcoming processes
### 20030324 JIM: proofed showing upcoming processes
### 20040129 JIM: catch stdout and stderr to xxxout_sysmon.log files and limit 
###               file size
### 20040129 JIM: save 4 versions of sysmon.log, sysqcp.log and ceda.log
### 20040129 JIM: collect summary of startup errors from sysmon.log
### 20040129 JIM: replace /ceda/debug by ${DBG_PATH}
### 20040203 JIM: removed output to tst.log, stamp BAD MAGIC NUMBER in ???out*
### 20040715 JIM: copying SYSMON to $RUN_PATH and starting from there
### 20040715 JIM: renaming debug to debug_YYYYmmddHHMM, rebuilding debug tree,
###               starting background job for compress of debug_YYYYmmddHHMM, 
###               starting UFIS, removing old debug_YYYYmmddHHMM
### 20041011 JIM: oracle check depends on "@" in login name
### 20041015 JIM: Start MQ manager, if config file exists
### 20050214 JIM: added output "wrong user" (PRF 7020)

. /ceda/etc/UfisEnv
clear

# check if 'echo' needs flag for special chars
ECHOFLAG=`echo -e .\c`
case ${ECHOFLAG} in
  -e*) ECHOFLAG=""
       ;;
  *)   ECHOFLAG="-e "
       ;;
esac

if [ -z "$CCSPRODUCT" ] ; then
   CCSPRODUCT=UFIS
fi

ERRLOG=$CFG_PATH/hsbadm.log
FILE1=sysqcp
FILE2=sysmon
# 20021120 JIM: set FILE3 to the last active process in sgs.tab:
FILE3=`grep "^PNTAB" $CFG_PATH/sgs.tab | tail -1 | cut -f2 -d' ' | cut -f1 -d','`
# FILE3 should be the last PNTAB entry !!!

FILE5=ora_dbw0
case "${CEDADBUSER}" in 
   *@*) # oracle assumed as remote:
        FILE5=
   ;;
esac
# unset FILE5, if no local oracle should be checked

TEMPFILE=/tmp/UfisStart.tmp

if [ -t 1 ]
then
	echo ${ECHOFLAG}"UfisStart: Now entering \c"
	date
else
	echo ${ECHOFLAG}"UfisStart: Now entering UfisStart \c">>$ERRLOG
	date>>$ERRLOG
fi

USERNAME=`id|cut -f2 -d'('|cut -f1 -d')'`
if [ $USERNAME != $CEDAUSER ]
then
  if [ -t 1 ]
  then
    # 20050214 JIM: 
    echo "UfisStart: You have to be user $CEDAUSER to startup $CCSPRODUCT "
  else
    echo "UfisStart: You have to be user $CEDAUSER to startup $CCSPRODUCT ">>$ERRLOG
  fi
  exit 23
fi

cnt=`ps -e | grep " $FILE1[ ]*$" | grep -v grep | grep -c " $FILE1"`
if [ $cnt -ne 0 ]
then
	if [ -t 1 ]
	then
        	echo "UfisStart: $FILE1 is allready running on this system!"
		echo "UfisStart: first run UfisStop!"
	else	
        	echo "UfisStart: $FILE1 is allready running on this system!">>$ERRLOG
		echo "UfisStart: first run UfisStop!">>$ERRLOG
	fi
	exit 11
fi

cnt=`ps -e | grep " $FILE2[ ]*$" | grep -v grep | grep -c " $FILE2"`
if [ $cnt -ne 0 ]
then
	if [ -t 1 ]
	then
        	echo "UfisStart: $FILE2 is allready running on this system!"
		echo "UfisStart: first run UfisStop!"
	else
        	echo "UfisStart: $FILE2 is allready running on this system!">>$ERRLOG
		echo "UfisStart: first run UfisStop!">>$ERRLOG
	fi
	exit 12
fi

if [ ! -z "${FILE5}" ] ; then
   # oracle connection without "@", so oracle must run local:
cnt=`ps -ef | grep " $FILE5" | grep -v grep | grep -c " $FILE5"`
if [ $cnt -eq 0 ]
then
	if [ -t 1 ]
	then
		echo "UfisStart: $FILE5 is NOT running!"
		echo "UfisStart: $CCSPRODUCT can not start!"
		echo "UfisStart: first start ORACLE!"
	else
		echo "UfisStart: $FILE5 is NOT running!">>$ERRLOG
		echo "UfisStart: $CCSPRODUCT can not start!">>$ERRLOG
		echo "UfisStart: first start ORACLE!">>$ERRLOG
	fi
	cp $CFG_PATH/hsb.down $CFG_PATH/hsb.dat 1>>$ERRLOG 2>>$ERRLOG
	exit 13
fi
fi

# 20041015 JIM: Start MQ manager, if config file exists:
if [ -f ${CFG_PATH}/wmqlib.cfg ] ; then
   if [ -x ${ETC_PATH}/UfisQMGRCtl.ksh ] ; then
      ${ETC_PATH}/UfisQMGRCtl.ksh start || exit 14
   fi
fi

cd $ETC_PATH

. UfisClean

if [ -d ${DBG_PATH} ] ; then
   OLD_DEBUG=${DBG_PATH}_`date +%Y%m%d%H%M`
   if [ -t 1 ]
   then
        echo "UfisStart: Renaming directory ${DBG_PATH} to ${OLD_DEBUG} "
   else
        echo "UfisStart: Renaming directory ${DBG_PATH} to ${OLD_DEBUG} ">>$ERRLOG
   fi
   TREE=`find ${DBG_PATH} -type d`
   mv ${DBG_PATH} ${OLD_DEBUG}
   if [ -t 1 ]
   then
        echo "UfisStart: Moving /ceda/conf/ceda.log to ${OLD_DEBUG} "
   else
        echo "UfisStart: Moving /ceda/conf/ceda.log to ${OLD_DEBUG} ">>$ERRLOG
   fi
   mv /ceda/conf/ceda.log ${OLD_DEBUG}/.
   # rebuilding old tree
   mkdir -p ${TREE}
   # and leave a hint
   if [ -d ${DBG_PATH}/old_prot ] ; then
      touch ${DBG_PATH}/old_prot/Old_files_are_in_`basename ${OLD_DEBUG}`
   fi
   if [ -t 1 ] ; then
     echo "(UfisStart: starting compress of ${OLD_DEBUG}/* in background job...)"
   else
     echo "(UfisStart: starting compress of ${OLD_DEBUG}/* in background job...)">>$ERRLOG
   fi
   ( cd ${OLD_DEBUG} ; for CFILE in * ; do nohup compress ${CFILE} 2>&1 ; done 2>&1 >$DBG_PATH/Compress_Old_Dbg.log )  &

else
   # rebuilding standard tree
   mkdir -p ${DBG_PATH} ${DBG_PATH}/old_prot
fi

# 20040129 JIM: create default UfisAlarm script, if not exist
if [ ! -f "${ETC_PATH}/UfisAlarm" ] ; then
  # create script UfisAlarm
  (
   cat <<EOF
     echo \`date\`"!!! BAD MAGIC NUMBER detected!!! " >>${DBG_PATH}/BAD_MAGIG_NUMBER.log
EOF
  ) > ${ETC_PATH}/UfisAlarm
  chmod u+x ${ETC_PATH}/UfisAlarm
fi

# script to allow switching of stdout and stderr log files:
# (this script need nawk or GNU awk)
out_filter()
{
  FLT=flt
  if [ ! -z "$1" ] ; then
     FLT=$1
  fi
  test -f tst.log && rm tst.log
  nawk 'BEGIN {CNT_STDOUT=0
               name="'${FLT}'out"
               LOG_STDOUT=ENVIRON["DBG_PATH"]"/"name"_sysmon.log"
               STOP_CALLED=0
              }
        {
          CNT_STDOUT++
          print name" :"$0 >LOG_STDOUT
          if (index($0,"BAD MAGI") > 0)
          {
	          if (STOP_CALLED==0)
	          {
              STOP_CALLED=1
              "date +%Y%m%d%H%M%S" | getline datum
              print name" "datum" ==== detected BAD MAGIC NUMBER ====" > LOG_STDOUT
              print name" "datum" ==== calling UfisAlarm ====" > LOG_STDOUT
              cmd="ksh "ENVIRON["ETC_PATH"]"/UfisAlarm"
              print cmd                                 > LOG_STDOUT
              cmd | getline
            }
          }
          if (CNT_STDOUT > 1000)
          {
            close(LOG_STDOUT)
            cmd="test -f "LOG_STDOUT".bak && rm "LOG_STDOUT".bak"
            cmd | getline
            cmd="mv "LOG_STDOUT" "LOG_STDOUT".bak"
            cmd | getline
            CNT_STDOUT=0
           }
        }'
}

# this script can be use also, but is extremely slow:
bash_stdout_filter()
{
  XIT=0
  CNT_STDOUT=0
  LOG_STDOUT=$DBG_PATH/stdout_sysmon.log
  while [ "${XIT}" = "0" ] ; do 
  read line
  RC=$?
  CNT_STDOUT=`expr ${CNT_STDOUT} + 1`
  if [ "`expr ${CNT_STDOUT} \> 1000`" = "1"  ] ; then
    mv ${LOG_STDOUT} ${LOG_STDOUT}.bak
    CNT_STDOUT=0
  fi
  STAMP="sysmon `date +%Y%m%m%d%H%M%S`"
  if [ ! "${RC}" = "0" ] ; then
    # read returns error (i.e EOF)
    echo "${STAMP}: sysmon finished" >>${LOG_STDOUT}
    XIT=1
  else
    echo "${STAMP}: ${CNT_STDOUT} ${line}"                           >>${LOG_STDOUT}
    case "${line}" in
      *"BAD"*)  
       echo "${STAMP}: got bad magick number"                        >>${LOG_STDOUT}
       # please note: the wildcard in grep avoids pid of grep:
       echo "${STAMP}: ==== sollte UfisScan rufen ===="              >>${LOG_STDOUT}
       #XIT=1
       ;;
    esac
  fi      
done
  
}

cd $RUN_PATH
# 20040714 JIM: added to start sysmon from RUNPATH:
cp $BIN_PATH/$FILE2 .

# create new sysmon log file to allow 'PROC=.tail -20...' (see later)
echo >${DBG_PATH}/sysmon.log

# start sysmon, inside inner brackets filter stdout, next brackets filter stderr
# (the first  '3>&1 1>&2 2>&3' exchange stderr and stdout to allow filter stderr
#  the second '3>&1 1>&2 2>&3' restore stderr and stdout i.e to allow use of 
#  stdout inside of filter)

# 20040714 JIM: modified to start sysmon from RUNPATH:
( ( ./$FILE2 $BIN_PATH $CFG_PATH $RUN_PATH d | out_filter std ) 3>&1 1>&2 2>&3 ) | out_filter err  3>&1 1>&2 2>&3 &

if [ -f "$TEMPFILE" ] ; then
   rm $TEMPFILE 1>>$ERRLOG 2>>$ERRLOG
fi

if [ -t 1 ]
then
	echo 
	echo ${ECHOFLAG}"UfisStart: Now starting $CCSPRODUCT! \c"
	date
else
	echo ${ECHOFLAG}"UfisStart: Now starting $CCSPRODUCT! \c">>$ERRLOG
	date>>$ERRLOG
fi

cnt=`ps -e | grep " $FILE3$" | grep -v grep | grep -c " $FILE3$"`
if [ $cnt -eq 0 ]
then
	echo ${ECHOFLAG}"UfisStart: <$FILE2>\tbuild SHM!"
	APROC=":"
	while [ $cnt -eq 0 ]
	do
		PROC=""
		if [ -t 1 ]
		then
			PROC=`tail -20 ${DBG_PATH}/sysmon.log|grep "return from syslibBuildSMS"|cut -d' ' -f7`
			if [ ! -z $PROC ]
			then
				echo ${ECHOFLAG}"UfisStart: <$FILE2>\tSHM-build ready!"
				echo ${ECHOFLAG}"UfisStart: <$FILE2>\tstarting processes!"
			fi
      # 20030324 JIM: proofed showing upcoming processes
			MPROC=`tail -80 ${DBG_PATH}/sysmon.log|grep "xecuting"|cut -d' ' -f5`
      for PROC in $MPROC ; do  
         case $APROC in    
            *:$PROC:*) ;; # already printed, do nothing
                    *)    # else print and add to list
                       echo ${ECHOFLAG} "UfisStart: <$PROC>\tstarted!"
                       APROC=$APROC"$PROC:"
                       ;; 
         esac 
      done
		fi
		sleep 1
		cnt=`ps -e | grep " $FILE3$"|grep -v grep | grep -c " $FILE3$"`
	done
fi

if [ -t 1 ]
then
	echo ${ECHOFLAG} "UfisStart: <$FILE3>\tstarted!"
  # 20040129 JIM: collect summary of startup errors from sysmon.log
  FAILED_PROCS=`awk '/ERROR stating child:/ {print $4}' ${DBG_PATH}/sysmon.log`
  if [ ! -z "${FAILED_PROCS}" ] ; then
     echo "UfisStart: processes failed to startup: ${FAILED_PROCS}"
     echo "UfisStart: error occured, SHUTDOWN in progress!"
  fi
	echo ${ECHOFLAG}"\nUfisStart: Now leaving UfisStart \c"
	date
else
	echo ${ECHOFLAG} "UfisStart: <$FILE3>\tstarted!">>$ERRLOG
  # 20040129 JIM: collect summary of startup errors from sysmon.log
  FAILED_PROCS=`awk '/ERROR stating child:/ {print $4}' ${DBG_PATH}/sysmon.log`
  if [ ! -z "${FAILED_PROCS}" ] ; then
     echo "UfisStart: processes failed to startup: ${FAILED_PROCS}" >>$ERRLOG
  fi
  echo "UfisStart: error occured, SHUTDOWN in progress!">>$ERRLOG
	echo ${ECHOFLAG}"\nUfisStart: Now leaving UfisStart \c">>$ERRLOG
	date>>$ERRLOG
fi

# 20040714 JIM: create default UfisClrOldDbg, if not exist
if [ ! -f "${ETC_PATH}/UfisClrOldDbg" ] ; then
  # create script UfisClrOldDbg
  (
   cat <<EOF
###     #    ######  ######
###    # #   #     # #     #
###   #   #  #     # #     #
###  #     # ######  ######
###  ####### #     # #     #
###  #     # #     # #     #
###  #     # ######  ######  GmbH (c) 2004
### 
### D-63150 Heusenstamm
###
### Script to remove entire old debug directories
###
### On UfisStart the directory \$DBG_PATH (normally /ceda/debug) is renamed
### to \$DBG_PATH_<date in YYYYmmddHHMM>, i.e. /ceda/debug_20040714. To avoid
### overfilling of the hard disk, this script checks for more than 4 
### \$DBG_PATH_<date in YYYYmmddHHMM> and deletes them. In Cron-jobs the
### deleting is done without user interaction, in terminal sessions the user
### is asked to confirm.

DBG_PARENT=\`dirname \$DBG_PATH\`
# don't use DBG_PATH for rm -rf, to dangerous if settings are missing: 
OLDDBG_BASE=debug_20
cd \${DBG_PARENT}
for RMFILE in \`ls -t1 | grep \${OLDDBG_BASE} | awk 'NR>4 {print \$0}'\` ; do 
    if [ -d \${RMFILE} ] ; then
       if [ -t 1 ] ; then
          echo
          echo "\`basename \$0\`: delete old debug directory \${RMFILE}? y/n [default yes]"
          read yesno
          if [ -z "\${yesno}" -o "\${yesno}" = "y" -o "\${yesno}" = "yes" ] ; then
             echo "Deleting \${RMFILE} ..."
             rm -rf \${RMFILE}
          fi
       else
          # Watchdog or Cronjob, nobody to ask
          echo "Deleting \${RMFILE} ..."
          rm -rf \${RMFILE}
       fi
    fi
done
EOF
  ) > ${ETC_PATH}/UfisClrOldDbg
  chmod u+x ${ETC_PATH}/UfisClrOldDbg
fi

if [ -t 1 ] ; then
   UfisClrOldDbg 
else
   UfisClrOldDbg 2>&1 >>$ERRLOG
fi

exit 0
#---END-OF-FILE----------------------------------------------------------------
