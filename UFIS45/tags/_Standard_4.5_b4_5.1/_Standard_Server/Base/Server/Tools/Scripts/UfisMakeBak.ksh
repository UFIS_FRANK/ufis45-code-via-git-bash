
:
###
### $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/UfisMakeBak.ksh 1.3 2005/11/30 17:34:07SGT jim Exp  $
###
###
### #     # #######   ###    #####             #     #####
### #     # #          #    #     #           # #   #     #
### #     # #          #    #                #   #  #
### #     # #####      #     #####   #####  #     #  #####
### #     # #          #          #         #######       #
### #     # #          #    #     #         #     # #     #
###  #####  #         ###    #####          #     #  ##### GmbH (c) 2005
###
### D-63150 Heusenstamm
###
### 20031208 JIM: using $CEDADBUSER/$CEDADBPW instead of fix settings
### 20051129 JIM: PRF 7999: adding awk exression for remote DB
### 20051129 JIM: also FROMUSER and TOUSER may not contain @host for remote DB
#

. /ceda/etc/UfisEnv
EXPUSER=`echo $CEDADBUSER $CEDADBPW | awk '
   {
     nn=split($1,usr,"@")
     if (nn>1)
     {
        print usr[1]"/"$2"@"usr[2]
     }
     else
     {
        print $1"/"$2
     }
   }'`
EXPOWNER=`echo $CEDADBUSER | awk '
   {
     nn=split($1,usr,"@")
     print usr[1]
   }'`

# exp ${EXPUSER} owner=${EXPOWNER} 

exp ${EXPUSER} owner=${EXPOWNER} buffer=9999999 feedback=10

rm -f expdat.dmp.Z

compress /ceda/expdat.dmp

echo "export done at `date`"
