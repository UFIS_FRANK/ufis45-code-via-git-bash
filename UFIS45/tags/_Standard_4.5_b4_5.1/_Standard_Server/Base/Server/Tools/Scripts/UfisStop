#!/bin/ksh
### $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/UfisStop 1.3 2006/09/28 16:47:05SGT jim Exp  $
###
###    #    ######  ######
###   # #   #     # #     #
###  #   #  #     # #     #
### #     # ######  ######
### ####### #     # #     #
### #     # #     # #     #
### #     # ######  ######    GmbH (c)
###
### D-63150 Heusenstamm
###
###
### mcu: 20020214: use of 'ps -e' according to 'uname -s'
### jim: 20020215: added 'sysmon' to kill at end
### jim: 20020215: added wait loop for 'sysmon' to delete SHM
### jim: 20021202: readded: check if 'echo' needs flag for special chars
### jim: 20050215: added call of UfisExcoStop (PRF 7014) 
###                added err msg, if user is not CEDAUSER
### jim: 20060928: qouted $STDTIMEOUT in "if"

. /ceda/etc/UfisEnv
clear

# check if 'echo' needs flag for special chars
ECHOFLAG=`echo -e .\c`
case ${ECHOFLAG} in
  -e*) ECHOFLAG=""
       ;;
  *)   ECHOFLAG="-e "
       ;;
esac

ERRLOG=$CFG_PATH/hsbadm.log
PRIO=15
FILE1=sysmon
FILE2=sysqcp
TAB=$CFG_PATH/sgs.tab
TMP01=$TMP_PATH/UfisStop.tmp01
TMP02=$TMP_PATH/UfisStop.tmp02
EXCOSTOP=${ETC_PATH}/UfisExcoStop
if [ -z "$CCSPRODUCT" ] ; then
   CCSPRODUCT=UFIS
fi

if [ -z "${SDTIMOUT}" ]
then
	echo ${ECHOFLAG}"UfisStop : SHUTDOWN-TIMEOUT NOT SET! USING STD. <90> seconds until hard-stop."
else
	echo ${ECHOFLAG}"UfisStop : SHUTDOWN-TIMEOUT = <$SDTIMOUT> seconds until hard-stop."
fi

if [ -t 1 ]
then
	echo ${ECHOFLAG}"UfisStop : Now entering \c"
	date
else
	echo ${ECHOFLAG}"UfisStop : Now entering \c">>$ERRLOG
	date>>$ERRLOG
fi

USERNAME=`id|cut -f2 -d'('|cut -f1 -d')'`
if [ $USERNAME != $CEDAUSER ]
then
  if [ -t 1 ]
  then
     echo "UfisStop : You have to be user $CEDAUSER to shut down $CCSPRODUCT "
  else
     echo "UfisStop : You have to be user $CEDAUSER to shut down $CCSPRODUCT ">>$ERRLOG
  fi
  exit 23
fi

if [ -x "${EXCOSTOP}" ] ; then
  "${EXCOSTOP}"
fi

ps -e | grep " $FILE1$" | grep -v grep > $TMP01
read PID1 DUMMY < $TMP01
if [ -z "$PID1" ]
then
	if [ -t 1 ]
	then
		echo "UfisStop : Proccess $FILE1 not found  "
		echo "UfisStop : Implication : $CCSPRODUCT is down."
	else
		echo "UfisStop : Proccess $FILE1 not found  ">>$ERRLOG
		echo "UfisStop : Implication : $CCSPRODUCT is down.">>$ERRLOG
	fi
	exit 21
fi

ps -e |grep " $FILE2$" | grep -v grep > $TMP01
read PID2 DUMMY < $TMP01
if [ -z "$PID2" ]
then
	if [ -t 1 ]
	then
		echo "UfisStop : Proccess $FILE2 not found  "
		echo "UfisStop : Implication : $CCSPRODUCT is down."
	else
		echo "UfisStop : Proccess $FILE2 not found  ">>$ERRLOG
		echo "UfisStop : Implication : $CCSPRODUCT is down.">>$ERRLOG
	fi
	exit 22
fi

kill -$PRIO $PID1 1>>$ERRLOG 2>>$ERRLOG
RC=$?
if [ $RC -ne 0 ]
then
	if [ -t 1 ]
	then
		echo "UfisStop : \"kill -$PRIO $PID1\" failed                          "
		echo "UfisStop : You have to be user $CEDAUSER to shutdown $CCSPRODUCT "
	else
		echo "UfisStop : \"kill -$PRIO $PID1\" failed                          ">>$ERRLOG
		echo "UfisStop : You have to be user $CEDAUSER to shutdown $CCSPRODUCT ">>$ERRLOG
	fi
	exit 23
fi

echo ${ECHOFLAG}"UfisStop : $CCSPRODUCT will shutdown now\c">>$ERRLOG
date>>$ERRLOG

lcnt=0
cnt=`ps -e | grep " $FILE2$" | grep -v grep | grep -c " $FILE2$"`
echo ${ECHOFLAG}"UfisStop : \c"
while [ $cnt -ne 0 ]
do
	if [ -t 1 ]
	then
		echo ${ECHOFLAG}".\c"
	fi
	sleep 1
	cnt=`ps -e | grep " $FILE2$" | grep -v grep | grep -c " $FILE2$"`
  lcnt=`expr $lcnt + 1`
  if [ $lcnt -gt 60 ]
  then
    cnt=0
	fi
done

if [ $lcnt -le 60 ] # so FILE2 terminated
then
  # now look for FILE1 to remove SHM and terminate
  lcnt=0
  cnt=`ps -e | grep " $FILE1$" | grep -v grep | grep -c " $FILE1$"`
  while [ $cnt -ne 0 ]
  do
  	if [ -t 1 ]
	  then
		  echo ${ECHOFLAG}".\c"
  	fi
	  sleep 3
  	cnt=`ps -e | grep " $FILE1$" | grep -v grep | grep -c " $FILE1$"`
    lcnt=`expr $lcnt + 1`
    if [ $lcnt -gt 60 ]
    then
      cnt=0
  	fi
  done
fi

echo ${ECHOFLAG}"\n"

OSNAME=`uname -s`
awk 'length>0 && $1=="PNTAB" {print $2;next}' $TAB|cut -f1 -d',' > $TMP01
echo sysmon >>$TMP01

for proc in `cat $TMP01`
do
 	ps -e | grep " $proc$" | grep -v grep > $TMP02
	if [ "`uname -s`" = "UnixWare" ]
	then
		read PID DUMMY1 DUMMY2 DUMMY3 DUMMY4 PNAME < $TMP02
	else
		read PID DUMMY1 DUMMY2 PNAME < $TMP02
	fi
	if [ -n "$PID" ]
	then
		if [ $proc = $PNAME ]
		then
			kill -9 $PID 1>>$ERRLOG 2>>$ERRLOG
			echo ${ECHOFLAG}"UfisStop : $proc $PID killed! \c" >> $ERRLOG
			date >> $ERRLOG
		fi
	fi
done

. UfisClean

if [ -t 1 ]
then
	echo ${ECHOFLAG}"UfisStop : Now leaving UfisStop \c"
	date
else
	echo ${ECHOFLAG}"UfisStop : Now leaving UfisStop \c">>$ERRLOG
	date>>$ERRLOG
fi
exit 0
#---END-OF-FILE----------------------------------------------------------------
