#!/bin/ksh

#USI = Ufis System Info
D_USI_CEDA="CEDA"
D_USI_DB="DB"
D_USI_SYSTEM="SYSTEM"
D_USI_CLIENT="CLIENT"

F_SCRIPT_LOG="capture.log"
S_MY_OS=`uname -s`

function FN_LOG 
{
	DT_NOW=`date +"%d/%m/%Y %H:%M:%S"`;
	STR_LOG="$DT_NOW $1"
	echo $STR_LOG;
	echo "$STR_LOG" >> $F_SCRIPT_LOG;
}

function FN_UFIS_VER
{
	F_UFIS_VER="$D_USI_CEDA/ufis_server_versions.txt";

	echo > $F_UFIS_VER
	echo "VERSION RETRIEVAL DATE: `date`" >> $F_UFIS_VER
	echo >> $F_UFIS_VER

	echo "============================================================================================================================" >> $F_UFIS_VER
	echo "PROCESS: SYSMON" >> $F_UFIS_VER
	echo "============================================================================================================================" >> $F_UFIS_VER
	strings /ceda/bin/sysmon | grep UFIS | grep -v grep  >> $F_UFIS_VER
	strings /ceda/bin/sysmon | grep compile | grep -v grep  >> $F_UFIS_VER
	echo >> $F_UFIS_VER
	echo >> $F_UFIS_VER
	for p in `grep "PNTAB " /ceda/conf/sgs.tab | awk '{print $2}' | awk -F"," '{print $1}'`
	do
        	echo "======================================================================================================" >> $F_UFIS_VER
        	echo "PROCESS: $p" >> $F_UFIS_VER
        	echo "======================================================================================================" >> $F_UFIS_VER
		P_NAME="/ceda/bin/$p"
		if [[ -f $P_NAME ]]
		then
        		strings $P_NAME | grep UFIS | grep -v grep  >> $F_UFIS_VER
        		strings $P_NAME | grep compile | grep -v grep  >> $F_UFIS_VER
		fi;
        	echo >> $F_UFIS_VER
        	echo >> $F_UFIS_VER
	done
}

function FN_DEATH
{
        date > $1
        printf "\n\n" >> $1 
        printf "\t%-10s %-8s %10s\n" "PROCESS" "DEATH COUNT" "TIME" >> $1
        printf "=========================================================\n" >> $1
        grep -h Death /ceda/debug/sysmon* | \
        awk '{printf "%-10s \t %6.6d \t %16s\n",$5,$9,substr($2,1,14)}' | \
        sort -r -k1,2 | \
        awk '$1 != prev {printf "%-10s %6d \t %s-%s-%s %s:%s:%s\n",$1,$2,substr($3,1,4),substr($3,5,2),substr($3,7,2),substr($3,9,2),substr($3,11,2),substr($3,13,2); prev = $1}' | sort -r -k3,4 | \
        awk '{ printf "%3s. \t %s\n", i+1, $0; i++ }' >> $1
        printf "\n\n" >> $1
}

function FN_CEDA_INFO
{
	FN_LOG "=============== In Ceda Information ==============="

	if [[ ! -d $D_USI_CEDA ]]
	then
		FN_LOG "Create CEDA directory..."
		mkdir $D_USI_CEDA 2>>$F_SCRIPT_LOG 2>>$F_SCRIPT_LOG;
	else
		FN_LOG "Remove old CEDA files..."
		rm -rf $D_USI_CEDA/* 2>>$F_SCRIPT_LOG 2>>$F_SCRIPT_LOG;
	fi;

	sleep 2;
	FN_UFIS_VER

	FN_LOG "Create CEDA/CONF directory..."
	D_USI_CEDA_CONF="$D_USI_CEDA/CONF"
	mkdir $D_USI_CEDA_CONF 2>>$F_SCRIPT_LOG; 
	FN_LOG "Copying config files..."
	cp -p /ceda/conf/*  $D_USI_CEDA_CONF/ 2>>$F_SCRIPT_LOG;

	FN_LOG "Create CEDA/ETC directory..."
	D_USI_CEDA_ETC="$D_USI_CEDA/ETC"
	mkdir $D_USI_CEDA_ETC  2>>$F_SCRIPT_LOG;
	FN_LOG "Copying etc(scripts) files..."
	cp -p /ceda/etc/*  $D_USI_CEDA_ETC/ 2>>$F_SCRIPT_LOG;

	FN_LOG "Getting files timing..."
	ls -lrt /ceda/bin/* > $D_USI_CEDA/ls_bin.txt
	ls -lrt /ceda/etc/* > $D_USI_CEDA/ls_etc.txt
	ls -lrt /ceda/runtime/* > $D_USI_CEDA/ls_runtime.txt

	FN_LOG "Getting death records..."
	FN_DEATH $D_USI_CEDA/death.txt 2>>$F_SCRIPT_LOG;

	FN_LOG "Getting user info..."
	whoami > $D_USI_CEDA/whoami.txt 2>>$F_SCRIPT_LOG; 
	crontab -l > $D_USI_CEDA/crontab.txt 2>>$F_SCRIPT_LOG;

	sleep 2;
	FN_LOG "=============== Completed Ceda Info Capturing ================"
	echo
}

function FN_EXEC_SQL
{
	F_USI_DB_QUERY="$D_USI_DB/db_info.sql"

	FN_LOG "Querying $1..."

	echo "SPOOL $3;"> $F_USI_DB_QUERY
	echo "SELECT TO_CHAR(SYSDATE,'DD-MM-YY HH24:MI:SS') FROM DUAL;" >> $F_USI_DB_QUERY
	echo "SET NEWPAGE NONE;" >> $F_USI_DB_QUERY 
	echo "SET PAGESIZE 0;" >> $F_USI_DB_QUERY 
	echo "SET SPACE 0;" >> $F_USI_DB_QUERY 
	echo "SET LINESIZE 16000;" >> $F_USI_DB_QUERY 
	echo "SET ECHO OFF;" >> $F_USI_DB_QUERY 
	echo "SET FEEDBACK OFF;" >> $F_USI_DB_QUERY 
	echo "SET VERIFY OFF;" >> $F_USI_DB_QUERY 
	echo "SET TERMOUT OFF;" >> $F_USI_DB_QUERY 
	echo "SET TRIMOUT ON;" >> $F_USI_DB_QUERY 
	echo "SET TRIMSPOOL ON;" >> $F_USI_DB_QUERY 
	echo "SET COLSEP ,;" >> $F_USI_DB_QUERY 
	if [[ $2 == " " ]]
	then
		S_QUERY="SELECT * FROM $1;"
	else
		S_QUERY="SELECT * FROM $1 ORDER BY $2;"
	fi;
	FN_LOG "$S_QUERY";
	echo "$S_QUERY" >> $F_USI_DB_QUERY 
	echo "ROLLBACK;" >> $F_USI_DB_QUERY 
	echo "QUIT;" >> $F_USI_DB_QUERY 
	sqlplus $CEDADBUSER/$CEDADBPW @$F_USI_DB_QUERY;
}
		
function FN_DB_INFO
{
	FN_LOG "=============== In Database Information ==============="

	if [[ -z $CEDADBUSER || -z $CEDADBPW ]]
	then
		FN_LOG "ERROR! DB env variable CEDADBUSER and CEDADBPW were not set"
		return;
	fi;

	if [[ ! -d $D_USI_DB ]]
	then
		FN_LOG "Create DB directory..."
		mkdir $D_USI_DB 2>>$F_SCRIPT_LOG
	else
		FN_LOG "Remove old DB files..."
		rm -f $D_USI_DB/* 2>>$F_SCRIPT_LOG
	fi;
	sleep 2;

	# UFIS tables
	FN_EXEC_SQL "PARTAB" "APPL,NAME" "$D_USI_DB/partab.txt"
	FN_EXEC_SQL "TABTAB" "LTNA" "$D_USI_DB/tabtab.txt"
	FN_EXEC_SQL "SYSTAB" "TANA,FINA" "$D_USI_DB/systab.txt"
	FN_EXEC_SQL "NUMTAB" "KEYS" "$D_USI_DB/numtab.txt"

	# Oracle tables
	FN_EXEC_SQL "V\$PARAMETER" "NAME" "$D_USI_DB/parameter.txt"
	FN_EXEC_SQL "USER_OBJECTS" "OBJECT_TYPE" "$D_USI_DB/user_objects.txt"
	FN_EXEC_SQL "DBA_PROFILES" "PROFILE" "$D_USI_DB/dba_profiles.txt"
	FN_EXEC_SQL "DBA_USERS" "USERNAME" "$D_USI_DB/dba_users.txt"
	FN_EXEC_SQL "ALL_USERS" "USERNAME" "$D_USI_DB/all_users.txt"
	FN_EXEC_SQL "NLS_DATABASE_PARAMETERS" "PARAMETER" "$D_USI_DB/nls_database_parameters.txt"
	FN_EXEC_SQL "GLOBAL_NAME" " " "$D_USI_DB/global_name.txt"

	sleep 2;
	FN_LOG "=============== Completed Database Info Capturing ================"
	echo
}
		
function FN_SYS_INFO
{
	FN_LOG "=============== In UNIX System Information ==============="
	if [[ ! -d $D_USI_SYSTEM ]]
	then
		FN_LOG "Create SYSTEM directory..."
		mkdir $D_USI_SYSTEM 2>>$F_SCRIPT_LOG
	else
		FN_LOG "Remove old SYSTEM files..."
		rm -f $D_USI_SYSTEM/* 2>>$F_SCRIPT_LOG
	fi;
	sleep 2;
	FN_LOG "Capturing environment info....."
	env > $D_USI_SYSTEM/env_env.txt 2>>$F_SCRIPT_LOG
	echo $PATH > $D_USI_SYSTEM/env_path.txt 2>>$F_SCRIPT_LOG
	cp $HOME/.profile $D_USI_SYSTEM/env_profile.txt 2>>$F_SCRIPT_LOG
	cp $HOME/.bashrc $D_USI_SYSTEM/env_bashrc.txt 2>>$F_SCRIPT_LOG
	cp $HOME/.bash_profile $D_USI_SYSTEM/env_bash_profile.txt 2>>$F_SCRIPT_LOG
	alias > $D_USI_SYSTEM/alias.txt 2>>$F_SCRIPT_LOG
	FN_LOG "Capturing network info....."
	cp /etc/hosts $D_USI_SYSTEM/net_hosts.txt 2>>$F_SCRIPT_LOG
	cp /etc/hosts.allow $D_USI_SYSTEM/net_hosts.allow.txt 2>>$F_SCRIPT_LOG
	cp /etc/hosts.deny $D_USI_SYSTEM/net_hosts.deny.txt 2>>$F_SCRIPT_LOG
	cp /etc/services $D_USI_SYSTEM/net_services.txt 2>>$F_SCRIPT_LOG
	netstat -a > $D_USI_SYSTEM/net_netstat.txt 2>>$F_SCRIPT_LOG
	FN_LOG "Capturing account info....."
	cp /etc/passwd $D_USI_SYSTEM/acct_passwd.txt 2>>$F_SCRIPT_LOG
	cp /etc/group $D_USI_SYSTEM/acct_group.txt 2>>$F_SCRIPT_LOG
	id ceda > $D_USI_SYSTEM/acct_ceda.id 2>>$F_SCRIPT_LOG
	FN_LOG "Capturing storage info....."
	df -h > $D_USI_SYSTEM/space_disk.txt 2>>$F_SCRIPT_LOG
	if [[ $? != 0 ]]	
	then
		df -k > $D_USI_SYSTEM/space_disk.txt 2>>$F_SCRIPT_LOG
		if [[ $? != 0 ]]
		then
			bdf > $D_USI_SYSTEM/space_disk.txt 2>>$F_SCRIPT_LOG
		fi;
	fi;
	FN_LOG "Capturing directory structure....."
	ls -lR /ceda | grep -v '^-' | grep -v '^l' > $D_USI_SYSTEM/dir_structure.txt 2>>$F_SCRIPT_LOG
	FN_LOG "Capturing system resource....."
	top -n 1 > $D_USI_SYSTEM/res_top.txt 2>>$F_SCRIPT_LOG
	ipcs -a > $D_USI_SYSTEM/res_ipcs.txt 2>>$F_SCRIPT_LOG
	FN_LOG "Capturing system spec....."
	uname -a > $D_USI_SYSTEM/spec_os.txt 2>>$F_SCRIPT_LOG

	if [[ "$S_MY_OS" == "Linux" ]]
	then
		lshw > $D_USI_SYSTEM/spec_hardware.txt 2>>$F_SCRIPT_LOG
		lshal > $D_USI_SYSTEM/spec_allhardware.txt 2>>$F_SCRIPT_LOG
		lspci > $D_USI_SYSTEM/spec_pci.txt 2>>$F_SCRIPT_LOG
		lsusb > $D_USI_SYSTEM/spec_usb.txt 2>>$F_SCRIPT_LOG
		cat /proc/cpuinfo > $D_USI_SYSTEM/spec_cpuinfo.txt 2>>$F_SCRIPT_LOG
		cat /proc/meminfo > $D_USI_SYSTEM/spec_meminfo.txt 2>>$F_SCRIPT_LOG
	elif [[ "$S_MY_OS" == "SunOS" ]]
	then
		psrinfo -pv > $D_USI_SYSTEM/spec_processor.txt 2>>$F_SCRIPT_LOG
		psrinfo -p > $D_USI_SYSTEM/spec_num_processor.txt 2>>$F_SCRIPT_LOG
		psrinfo -v > $D_USI_SYSTEM/spec_status_processor.txt 2>>$F_SCRIPT_LOG
		prtdiag -v > $D_USI_SYSTEM/spec_diagnostic.txt 2>>$F_SCRIPT_LOG
	elif [[ "$S_MY_OS" == "HP-UX" ]]
	then
		FN_LOG "Do not think we have permission to capture system info from HP-UX Server";
	fi;
	
	sleep 2;
	FN_LOG "=============== Completed UNIX System Info Capturing ================"
}
		
function FN_CLIENT_INFO
{
	FN_LOG "=============== In Client Information ==============="
	if [[ ! -d $D_USI_CLIENT ]]
	then
		FN_LOG "Create CLIENT directory..."
		mkdir $D_USI_CLIENT 2>>$F_SCRIPT_LOG
	else
		FN_LOG "Remove old CLIENT files..."
		rm -f $D_USI_CLIENT/* 2>>$F_SCRIPT_LOG
	fi;
	sleep 2;
	FN_LOG "Capturing Client version info (overview only, not exact figure)....."
	grep -w TWE /ceda/debug/authdl* | awk -F"<" '{print $3}' | sort | uniq > $D_USI_CLIENT/client_version.txt 2>>$F_SCRIPT_LOG

	D_USI_CLIENT_CONF=$D_USI_CLIENT/CONF
	if [[ ! -d $D_USI_CLIENT_CONF ]]
	then
		FN_LOG "Create CLIENT directory..."
		mkdir $D_USI_CLIENT_CONF 2>>$F_SCRIPT_LOG
	else
		FN_LOG "Remove old CLIENT files..."
		rm -f $D_USI_CLIENT_CONF/* 2>>$F_SCRIPT_LOG
	fi;

	echo
	FN_LOG "Please copy all Client SYSTEM files from the PC to the directory [$D_USI_CLIENT_CONF]"
	echo
	
	sleep 2;
	FN_LOG "=============== Completed Client Info Capturing ================"
	echo;
}
		
function FN_MENU
{
	echo $DT_NOW;
	echo
	echo "You like to capture information for:"
	echo
	echo "    1. Ceda "
	echo "    2. Database "
	echo "    3. System "
	echo "    4. Client"
	echo
	echo "    9. ALL"
	echo "    (Required during 1st gathering of information)"
	echo
	echo "    P. Print version of script" 
	echo "   =================================== "
	echo
	echo "    M. Menu"
	echo "    X. Exit"
	echo

	while [[ "$choice" != "X" && "$choice" != "x" ]]
	do
		print -n "Your choice ==> "
		read choice
		case "$choice" in
			1)echo
			  FN_CEDA_INFO;
			  continue;;
			2)echo
			  FN_DB_INFO;
			  continue;;
			3)echo
			  FN_SYS_INFO;
			  continue;;
			4)echo
			  FN_CLIENT_INFO;
			  continue;;
			9)echo
			  FN_CEDA_INFO;
			  FN_DB_INFO;
			  FN_SYS_INFO;
			  FN_CLIENT_INFO;
			  continue;;
			p|P)echo;
			  echo "Version: 1.0    \t    Date: 14-March-2011    \t    Author: ble"
			  echo;
			  continue;;
			x|X) exit;;
			m|M) clear
			     FN_MENU;;
			*) echo "    Invalid selection"; echo;;
		esac
	done
}


clear
echo "\t\t\tUFIS SYSTEM INFO CAPTURING MENU"
echo
echo

rm -f $F_SCRIPT_LOG;
FN_LOG "Menu Started" 
FN_LOG "My OS <$S_MY_OS>"
FN_MENU
