#!/usr/bin/ksh
# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/UfisImp 1.4 2005/11/29 21:19:16SGT jim Exp  $
#
# 20021212 JIM: (every ps is different) using IMP_ID of forked process
# 20031208 JIM: using ${CEDADBUSER}/${CEDADBPW} instead of fix settings
# 20051129 JIM: adding ${MKNOD} and awk exression for remote DB
# 20051129 JIM: also FROMUSER and TOUSER may not contain @host for remote DB
#

function display_help
{
   cat <<EOF

 This shell script `basename $0` will make an import into the 
 ORACLE DB from a compressed export dump file. The import dump 
 file will be uncompressed at instance by a named pipe, so there 
 is no uncompressed file at any time this script is running.
 
 The parameter "file=<expdat.dmp>" is mandantory, only replace
 "<expdat.dmp>" by the name of the real file name. 
 The export file must be compressed!

EOF
}

unset EXPUSER
unset EXPFROM
unset EXPTO
unset EXPTABLES
unset EXPFILE
unset EXPBUFFER
unset EXPIGNORE
unset EXPCOMMIT
unset EXPOTHER
unset HELP_NEEDED

# 20051129 JIM: adding ${MKNOD}:
MKNOD=/usr/sbin/mknod
if [ ! -f ${MKNOD} ] ; then
   MKNOD=/sbin/mknod
fi
if [ ! -f ${MKNOD} ] ; then
   MKNOD=mknod
fi

# At first evaluate the parameters the user has given:
case "$1" in
   *=*) ;;           # if first parameter has '=', no user given
   */*) EXPUSER=$1   # else a user/passwd should be given and we shift
        shift;;
   *)
esac

# now setting some defaults, if the user did not set them
# 20051129 JIM: adding awk exression for remote DB (sqlplus
# allows i.e. user@host/password, but 'exp' and 'imp' only
# allow  i.e. user/password@host:)
if [ -z "${EXPUSER}" ] ; then
EXPUSER=`echo $CEDADBUSER $CEDADBPW | awk '
   {
     nn=split($1,usr,"@")
     if (nn>1)
     {
        print usr[1]"/"$2"@"usr[2]
     }
     else
     {
        print $1"/"$2
     }
   }'`
fi
while [ ! -z "$1" ] ; do
   case "$1" in
      file=*)         EXPFILE=$1;;
      buffer=*)       EXPBUFFER=$1;;
      ignore=*)       EXPIGNORE=$1;;
      commit=*)       EXPCOMMIT=$1;;
      fromuser=*)     EXPFROM=$1;;
      touser=*)       EXPTO=$1;;
      tables=*)       EXPTABLES=$1;;
      ?|-?|-h|--help) HELP_NEEDED=yes ;;
      *)              EXPOTHER="${EXPOTHER} $1";;
   esac
   shift
done

if [ -z "${HELP_NEEDED}" ] ; then
   LOGFILE=/ceda/tmp/UfisImp.`date +"%Y%m%d_%H%M"`.log
   LOGCMD=" | tee ${LOGFILE} "

   # now setting some defaults, if the user did not set them
   if [ -z "${EXPFROM}" -a -z "${EXPTABLES}" ] ; then
      # 20051129 JIM: also FROMUSER and TOUSER may not contain @host for remote DB
      DBUSER="`echo ${CEDADBUSER} | awk '
        {
          nn=split($1,usr,\"@\")
          print usr[1]
        }'`"
      EXPFROM="fromuser=${DBUSER}"
      EXPTO="touser=${DBUSER}"
   fi
   if [ -z "${EXPBUFFER}" ] ; then
      EXPBUFFER="buffer=131072"
   fi
   if [ -z "${EXPFILE}" ] ; then
      echo
      echo 'Error: missing parameter "file=<expdat.dmp>"'
      echo
      HELP_NEEDED=yes
   else
      # we need the filename without 'file=' and '.Z'
      EXPFNAME=`echo "${EXPFILE}" | sed -e 's/.Z$//;s/^file=//'`
      EXPFILE="file=${EXPFNAME}"
      EXPFNAME="${EXPFNAME}"
   fi
fi

if [ -z "${HELP_NEEDED}" ] ; then
   # +++ BEGIN FUNCTION do_it +++
   function do_it
   {
      # create a named pipe:
      # 20051129 JIM: adding ${MKNOD}:
      echo ${MKNOD} "${EXPFNAME}" p
      ${MKNOD} "${EXPFNAME}" p  2>&1
      if [ $? != 0 ] ; then
          echo $0: unable to create pipe "${EXPFNAME}"
      else
         STARTDATE=`date +"%Y%m%d %H:%M:%S"`
         STARTSECS=`date +"%y %j %H %M" | awk '{print ((($1*365)+$2*24)+$3*60)+$4}'`
         # start a import reading from the named pipe, the uncompress will write
         # the data to the the pipe
         # start a import in background, which reads from the named pipe:
         echo imp "${EXPUSER}" "${EXPFILE}" "${EXPBUFFER}" "${EXPIGNORE}" \
             "${EXPCOMMIT}" "${EXPFROM}" "${EXPTO}" "${EXPTABLES}" "${EXPOTHER}" 2\>\&1 \&
         imp "${EXPUSER}" "${EXPFILE}" "${EXPBUFFER}" "${EXPIGNORE}" \
             "${EXPCOMMIT}" "${EXPFROM}" "${EXPTO}" "${EXPTABLES}" "${EXPOTHER}" 2>&1 &
         IMP_ID=$! 
         cnt=`ps -ef | grep "${IMP_ID} .* imp " | grep -v grep | grep -c "^"`
         if [ ${cnt} \> 0 ] ; then
            echo uncompress \-c "${EXPFNAME}".Z \>"${EXPFNAME}"
            uncompress -c "${EXPFNAME}".Z >"${EXPFNAME}"        2>&1
         fi

         ENDDATE=`date +"%Y%m%d %H:%M:%S"`
         ENDSECS=`date +"%y %j %H %M" | awk '{print ((($1*365)+$2*24)+$3*60)+$4}'`
 
         # remove the named pipe
         rm "${EXPFNAME}"  2>&1
 
         echo Import has run from "${STARTDATE}" to "${ENDDATE}"
         echo Import needed `expr "${ENDSECS}" - "${STARTSECS}"` minutes
      fi
   }
   # +++ END  FUNCTION do_it +++

   echo >${LOGFILE} 2>/dev/null
   if [ $? != 0 ] ; then
      # perhaps /ceda or /ceda/tmp don't exist yet, so write here:
      LOGFILE=UfisImp.`date +"%Y%m%d_%H%M"`.log
   fi
   echo >${LOGFILE} 2>/dev/null
   if [ $? != 0 ] ; then
      echo perhaps no write permissions to create log file, lets have a look,
      echo how far it goes
      echo
      LOGFILE=""
   fi
   if [ -z ${LOGFILE} ] ; then
      do_it 2>&1 
   else
      echo Logging to ${LOGFILE}
      do_it 2>&1 | tee ${LOGFILE}
      echo
      echo Log file ${LOGFILE} written
   fi
else
   display_help
fi

unset EXPUSER
unset EXPOWNER
unset EXPTABLES
unset EXPFILE
unset EXPBUFFER
unset EXPCONSISTENT
unset EXPIGNORE
unset EXPCOMMIT
unset EXPOTHER
unset HELP_NEEDED
unset LOGFILE
unset LOGCMD

