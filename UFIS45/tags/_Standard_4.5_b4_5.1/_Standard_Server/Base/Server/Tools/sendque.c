#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/sendque.c 1.1 2003/03/19 17:05:12SGT fei Exp  $";
#endif /* _DEF_mks_version */
/* 20020409 jim         : INIT_OHNE_LOG: init access to shared memory   */
/*                        without opening debug file (new queprim needs */
/*                        SHM to check queue id of stmhdl)              */
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp    */
/*               to avoid core dump by null pointer in dbg                  */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
#include "uevent.h"
#include "tools.h"

static char sccs_sendque[] ="@(#) UFIS 4.4 (c) ABB AAT/I sendque.c 44.3 / 00/01/11 13:48:54  / VBL";

int mod_id = 9999;
extern int debug_level=0;
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/
EVENT	*out_event;
char	*mod_name;
char	__CedaLogFile[128];

MAIN
{
  int rc = RC_SUCCESS;
  int len=0;
  long anz=0;
  int i = 0;
  FILE *dat_fp;
  char my_txt[4096];
  char fields[4096];
  char buf[500];
  int que_id_out;
  BC_HEAD rlBchd;
  CMDBLK *prlCmdblk = (CMDBLK *)buf;
  char *sel = prlCmdblk->data;

	/* 20020409 jim: */
 	/* init();			 Init access to ST */	
        /*INIT_OHNE_LOG*/

  if ( argc != 6 ) 
  {
    printf("Usage: sendque queue command filename no print\n        sendque 7800 UFR dat 1000 1\n First line of file contains field list !!!\n");
    exit (0);
  } /* end if */

  mod_name = argv[0];
  /* if argv[0] contains path (i.e. started by ddd), skip path */
  if (strrchr(mod_name,'/')!= NULL)
  {
     mod_name= strrchr(mod_name,'/');
     mod_name++;
  }
	sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
  memset ((char *) &rlBchd, 0, sizeof(BC_HEAD));
  memset ((char *) prlCmdblk, 0, sizeof(CMDBLK));
  strcpy (rlBchd.orig_name, "Sendque");
  strcpy (prlCmdblk->obj_name,"HAJ");
  strcpy (prlCmdblk->command,argv[2]);


  que_id_out=atoi(argv[1]);
  init_que();
  if( (dat_fp = fopen( argv[3], "r" )) != NULL )
  {                
    if( fgets( fields, 4096, dat_fp) != NULL) 
    {
      while ( (fgets( my_txt, 4096, dat_fp)) != NULL) 
      {
	memset(sel, 0, 60);
	strncpy (sel,my_txt,14);
	strcat (sel,",");
	strncpy (sel+15,my_txt,14);
	strcat (sel,",");
	strcat (sel,"1234567");
	strcat (sel,",");
	strcat (sel,"1");
/*	printf ("SEL=%s\n",sel);*/

	len = strlen(my_txt);
	len--;
	my_txt[len] = 0x00;
	anz++;
	if (anz>atol(argv[4]))
	{
	  exit(0);
	}
	if(atoi(argv[5])!=0)
	{
	  printf("%d <%s> (%d)\n",anz,my_txt,len);
	}
	rc = new_tools_send_sql (que_id_out, &rlBchd, prlCmdblk, fields, my_txt);
      } /* while */
    } 
    fclose(dat_fp);
  }/* end if */
  exit(0);
} /* end */





