#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/toupper.c 1.1 2003/03/19 17:05:21SGT fei Exp  $";
#endif /* _DEF_mks_version */

#include <stdio.h>


main(int argc,char *argv[])
{

	if (argc > 1)
	{
		int ilLc = 0;
		while(argv[1][ilLc++] != '\0')
			argv[1][ilLc-1] =  toupper( argv[1][ilLc-1] );
		printf("%s",argv[1]);
	}
	return  0;
}
