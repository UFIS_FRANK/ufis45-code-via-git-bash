#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/restart.c 1.1 2003/03/19 17:03:46SGT fei Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/* UGCCS Program Skeleton						*/
/*									*/
/* Author		: Claudia Jeckel				*/
/* Date			: 24.09.1993					*/
/* Description		:						*/
/*									*/
/* Update history	:						*/
/* 20020924 JIM: added global variables mod_name, __CedaLogFile and outp    */
/*               to avoid core dump by null pointer in dbg                  */
/* ******************************************************************** */
/* This program is a UGCCS main program */

static char sccs_restart[]="@(#) UFIS 4.4 (c) ABB AAT/I restart.c 44.2 / 00/01/07 15:29:52 / VBL"; 

#define U_MAIN
#define CEDA_PRG	
#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
#include "uevent.h"

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */

/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */
static	EVENT	event;			/* The event        		*/
char	*mod_name;
char	__CedaLogFile[128];
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/

/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */
extern	int	init_que();		/* Attach to UGCCS queues	*/
extern	int	que(int,int,int,int,int,char*); /* UGCCS queuing routine*/
    
extern int debug_level=DEBUG;

/* ******************************************************************** */
/* Function prototypes							*/
/* ******************************************************************** */

/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */
MAIN
{
	int	rc=0;			/* Return code			*/
	int	cnt = 0;
	
#ifdef UFIS43
  mod_name = argv[0];
  /* if argv[0] contains path (i.e. started by ddd), skip path */
  if (strrchr(mod_name,'/')!= NULL)
  {
     mod_name= strrchr(mod_name,'/');
     mod_name++;
  }
	sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
#else
	INITIALIZE;			/* General initialization	*/
#endif
	
	while( (init_que() !=0) && (cnt<10) ){
		cnt++;
		sleep(6);
	} /* end while */
	if(cnt>=10){
		printf("PUTQUE: ERROR: unable to init que!\n");
		exit(1);
	}/* end of if */

	event.type = SYS_EVENT;
	event.command = EVENT_DATA;
	event.originator = 10;
	event.retry_count = 0;
	event.data_offset = 0;
	event.data_length = 0;
	
	if((rc=que(QUE_RESTART,1,10,1,sizeof(EVENT),(char *)&event))!=RC_SUCCESS)
	{
		exit(2);
	} /* end if */
			
	exit(0);
	
} /* end of MAIN */

/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */
/* ******************************************************************** */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
