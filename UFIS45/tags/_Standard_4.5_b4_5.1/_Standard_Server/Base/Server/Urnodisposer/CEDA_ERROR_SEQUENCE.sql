-- WEBFIDS
-- Create sequence for WEBFIDS
-- also used for writing error messages
-- run as user ceda
create sequence CEDA_ERROR_SEQ
minvalue 1
maxvalue 999999
start with 1
increment by 1
nocache
cycle;
