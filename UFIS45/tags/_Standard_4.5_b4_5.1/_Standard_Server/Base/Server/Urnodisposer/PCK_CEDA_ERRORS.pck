CREATE OR REPLACE PACKAGE PCK_CEDA_ERRORS
AS
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Package              - PACKAGE PCK_CEDA_ERRORS
--
--      Version               - 1.0.0
--
--      Written By            - Jurgen Hammerschmid
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
--      Change Log
--
--      Name           Date               Version    Change
--      ----           ----------         -------    ------
--      Hammerschmid   15.11.2007         1.0.0      new
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

  -- Author  : JHA
  -- Created : 15.11.2007 13:05:12
  -- Purpose : Errorhandling

  -- Public type declarations
  --TYPE <TypeName> is <Datatype>;

  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --<VariableName> <Datatype>;

  -- Public procedure declarations
	PROCEDURE PRC_WRITE_ERROR			(p_Process			IN CEDA_ERRORS.PROCESS%TYPE,
										 p_ErrorCode		IN CEDA_ERRORS.ERROR_CODE%TYPE,
										 p_ErrorMsg			IN CEDA_ERRORS.ERROR_MSG%TYPE,
										 p_ErrorCategory	IN CEDA_ERRORS.ERROR_REF%TYPE,
                                         p_Commit			IN CHAR DEFAULT 'Y');
  -- Public function declarations

END PCK_CEDA_ERRORS;
/
CREATE OR REPLACE PACKAGE BODY PCK_CEDA_ERRORS
AS

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Package Body      - PCK_CEDA_ERRORS
--
--  Version           - 1.0
--
--  Written By        - Jurgen Hammerschmid
--
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Change Log
--
--  Name           Date               Version    Change
--  ----           ----------         -------    ------
--  Hammerschmid   15.11.2007         1.0        new
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--      Procedure              - PRC_WRITE_ERROR
-------------------------------------------------------------------------------------------
	PROCEDURE PRC_WRITE_ERROR			(p_Process			IN CEDA_ERRORS.PROCESS%TYPE,
										 p_ErrorCode		IN CEDA_ERRORS.ERROR_CODE%TYPE,
										 p_ErrorMsg			IN CEDA_ERRORS.ERROR_MSG%TYPE,
										 p_ErrorCategory	IN CEDA_ERRORS.ERROR_REF%TYPE,
                                         p_Commit			IN CHAR DEFAULT 'Y')
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Procedure              - PRC_WRITE_ERROR
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Jurgen Hammerschmid
    --
    --      Errors                - Errors will be written into the table CEDA_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
    --      Hammerschmid   15.11.2007         1.0.0      new
    --	    Hammerschmid   10.09.2008		  1.1.0		 Default parameter p_Commit inserted
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

    error_code			NUMBER;
    error_message		VARCHAR2(512);
	v_Seq_ID			NUMBER(9);

	BEGIN
		-- get next Sequence ID for Error number
		SELECT CEDA_ERROR_SEQ.Nextval
			INTO v_Seq_ID
		FROM dual;

		INSERT INTO CEDA_ERRORS (ERROR_NO, PROCESS, ERROR_CODE, ERROR_MSG,  DATE_TIME, ERROR_REF)
			   VALUES 			(v_Seq_ID, p_Process, p_ErrorCode, p_ErrorMsg, sysdate,  p_ErrorCategory);
--		IF p_Commit = 'Y' THEN
        	COMMIT;
--        END IF;

	EXCEPTION
		WHEN OTHERS THEN
            error_code := SQLCODE;
            error_message := SUBSTR(SQLERRM, 1, 150);
            DBMS_OUTPUT.PUT_LINE('PROC_WRITE_ERROR: ' || error_code || ' : ' || error_message);
          	RAISE;
	END PRC_WRITE_ERROR;

END PCK_CEDA_ERRORS;
/
