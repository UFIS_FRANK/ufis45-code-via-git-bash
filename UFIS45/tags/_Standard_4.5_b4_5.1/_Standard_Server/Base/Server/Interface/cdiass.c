#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Interface/cdiass.c 1.2 2009/06/03 14:46:27SGT akl Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* UFIS AS CDIASS.C                                                           */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : March 2006                                                */
/* Description    : Process to handle GPU, PCA and PLB Usage Data             */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_cdiass[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 


/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern int GetNoOfElements(char *s, char c);
extern long nap(long);
extern int SendCedaEvent(int,int,char*,char*,char*,char*,char*,char*,char*,char*,char*,char*,int,int);
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char  pcgHostName[XS_BUFF];
static char  pcgHostIp[XS_BUFF];

static int   igQueCounter=0;
static int   que_out=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static EVENT *prgOutEvent = NULL;
static char pcgCurrentTime[32];
static int igDiffUtcToLocal;
static char pcgServerChars[110];
static char pcgClientChars[110];
static char pcgUfisConfigFile[512];
static int igTolerance;
static char cgSeperator;

static char pcgRtypForCounters[16];
static char pcgRtypForGates[16];
static char pcgRtypForBelts[16];

static char pcgFieldList[2048];
static char pcgDataBuf[1000*16000];

static int igLastUrno = 0;
static int igLastUrnoIdx = -1;
static char pcgNewUrnos[1000*16];

static int igSendResponse = FALSE;

static int    Init_cdiass();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static int GetConfig();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int TimeToStrLocal(char *pcpTime,time_t lpTime);
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction);
static long GetSecondsFromCEDATime(char *pcpDateTime);
static void TrimRight(char *pcpBuffer);
static void TrimLeftRight(char *pcpBuffer);
static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP);
static char *GetLine(char *pcpLine, int ipLineNo);
static void CopyLine(char *pcpDest, char *pcpSource);
static int GetNextUrno();

static int HandleInventory(char *pcpCommand, char *pcpSelection);
static int HandleFlightCounter(char *pcpCommand, char *pcpType, char *pcpFields, char *pcpData);
static int HandleLogoText(char *pcpCommand, char *pcpFields, char *pcpData);
static int HandleCCA(char *pcpUrno, char *pcpType, char *pcpFldLst, char *pcpDataBuf);
static int HandleAFT(char *pcpUrno, char *pcpAdid, char *pcpType, char *pcpFldLst, char *pcpDataBuf);
static int HandleFLZ_FXT(char *pcpFldUrno, char *pcpFldLst, char *pcpDataBuf);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_cdiass);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_cdiass();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
    {
      now = time(NULL);
      while(TRUE)
	{
	  memset(prgItem,0x00,igItemLen);
	  ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	  dbg(DEBUG,"QUE Counter %d",++igQueCounter);
	  /* depending on the size of the received item  */
	  /* a realloc could be made by the que function */
	  /* so do never forget to set event pointer !!! */
	  prgEvent = (EVENT *) prgItem->text;
	  if( ilRc == RC_SUCCESS )
	    {
	      /* Acknowledge the item */
	      ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
	      if( ilRc != RC_SUCCESS ) 
		{
		  /* handle que_ack error */
		  HandleQueErr(ilRc);
		} /* fi */
	      switch( prgEvent->command )
		{
		case    HSB_STANDBY    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_COMING_UP    :
		  ctrl_sta = prgEvent->command;
		  HandleQueues();
		  break;    
		case    HSB_ACTIVE    :
		  ctrl_sta = prgEvent->command;
		  break;    
		case    HSB_ACT_TO_SBY    :
		  ctrl_sta = prgEvent->command;
		  /* CloseConnection(); */
		  HandleQueues();
		  break;    
		case    HSB_DOWN    :
		  /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
		  ctrl_sta = prgEvent->command;
		  Terminate(1);
		  break;    
		case    HSB_STANDALONE    :
		  ctrl_sta = prgEvent->command;
		  ResetDBCounter();
		  break;    
		case    REMOTE_DB :
		  /* ctrl_sta is checked inside */
		  HandleRemoteDB(prgEvent);
		  break;
		case    SHUTDOWN    :
		  /* process shutdown - maybe from uutil */
		  Terminate(1);
		  break;
		case    RESET        :
		  ilRc = Reset();
		  break;
		case    EVENT_DATA    :
		  if((ctrl_sta == HSB_STANDALONE) ||
		     (ctrl_sta == HSB_ACTIVE) ||
		     (ctrl_sta == HSB_ACT_TO_SBY))
		    {
		      ilItemFlag=TRUE;
		      ilRc = HandleInternalData();
		      if(ilRc != RC_SUCCESS)
			{
			  HandleErr(ilRc);
			}/* end of if */
		    }
		  else
		    {
		      dbg(TRACE,"MAIN: wrong HSB-status <%d>",ctrl_sta);
		      DebugPrintItem(TRACE,prgItem);
		      DebugPrintEvent(TRACE,prgEvent);
		    }/* end of if */
		  break; 
		case    TRACE_ON :
		  dbg_handle_debug(prgEvent->command);
		  break;
		case    TRACE_OFF :
		  dbg_handle_debug(prgEvent->command);
		  break;
                case  111 :
                  break;
		default            :
		  dbg(TRACE,"MAIN: unknown event");
		  DebugPrintItem(TRACE,prgItem);
		  DebugPrintEvent(TRACE,prgEvent);
		  break;
		} /* end switch */
	    }else{
	      /* Handle queuing errors */
	      HandleQueErr(ilRc);
	    } /* end else */



	  /**************************************************************/
	  /* time parameter for cyclic actions                          */
	  /**************************************************************/
    
	  now = time(NULL);

	} /* end while */
    }else{
      dbg(TRACE,"MAIN: Init_cdiass() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
      sleep(30);
    }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_cdiass()
{
  int    ilRc = RC_SUCCESS;            /* Return code */

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_cdiass : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_cdiass : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_cdiass : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_cdiass : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_cdiass : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_cdiass: use HOPO-field!");
	}
    }

  ilRc = GetConfig();

  ilRc = TimeToStr(pcgCurrentTime,time(NULL));
  
  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");
 
  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      break;
    case SIGPIPE:
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_cdiass();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_cdiass() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_cdiass() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclNewData[512000];
  char pclOldData[512000];
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  char pclTmpInitFog[32];
  char pclRegn[32];
  char pclStartTime[32];
  int ilLen;

  que_out = prgEvent->originator;
  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);
  if (bchd->rc == NETOUT_NO_ACK)
     igSendResponse = FALSE;
  else
     igSendResponse = TRUE;

  strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  memset(pclOldData,0x00,L_BUFF);
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  strcpy(pclNewData,pclData);
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  if (strcmp(cmdblk->command,"CCAU") == 0 || strcmp(cmdblk->command,"AFTU") == 0)
  {
     dbg(DEBUG,"Command:   <%s>",cmdblk->command);
     dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
     dbg(DEBUG,"Fields:    <%s>",pclFields);
     dbg(DEBUG,"Data:      <%s>",pclNewData);
     dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
     dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
     ilRC = HandleFlightCounter(cmdblk->command,"A",pclFields,pclNewData);
  }
  else if (strcmp(cmdblk->command,"FLZU") == 0 || strcmp(cmdblk->command,"FXTU") == 0)
  {
     dbg(DEBUG,"Command:   <%s>",cmdblk->command);
     dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
     dbg(DEBUG,"Fields:    <%s>",pclFields);
     dbg(DEBUG,"Data:      <%s>",pclNewData);
     dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
     dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
     ilRC = HandleLogoText(cmdblk->command,pclFields,pclNewData);
  }
  else if (strcmp(cmdblk->command,"CCAI") == 0 || strcmp(cmdblk->command,"AFTI") == 0 ||
           strcmp(cmdblk->command,"AFTIA") == 0 || strcmp(cmdblk->command,"AFTID") == 0)
  {
     dbg(DEBUG,"Command:   <%s>",cmdblk->command);
     dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
     dbg(DEBUG,"Fields:    <%s>",pclFields);
     dbg(DEBUG,"Data:      <%s>",pclNewData);
     dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
     dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
     ilRC = HandleInventory(cmdblk->command,pclSelection);
  }

  dbg(DEBUG,"========================= START / END =========================");
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}

/*
	Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetConfig:";
  int ilI;
  int ilJ;
  char pclTmpBuf[16000];
  char pclResult[512];
  char pclDebugLevel[128];
  char pclFldNam[10];
  char pclFldDat[16];
  char pclItem[256];
  char pclSelDssn[256];
  char pclSelType[256];
  char pclSelStyp[256];
  char pclSelSstp[256];
  char pclSelSsst[256];
  char pclBuffer[10];
  char pclClieBuffer[100];
  char pclServBuffer[100];

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"%s Config File is <%s>",pclFunc,pcgConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"CCATAB","RtypForCounters",CFG_STRING,pcgRtypForCounters);
  if (ilRC != RC_SUCCESS)
     strcpy(pcgRtypForCounters,"ACIC");
  ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","RtypForGates",CFG_STRING,pcgRtypForGates);
  if (ilRC != RC_SUCCESS)
     strcpy(pcgRtypForGates,"AGTD");
  ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","RtypForBelts",CFG_STRING,pcgRtypForBelts);
  if (ilRC != RC_SUCCESS)
     strcpy(pcgRtypForBelts,"BLT1");

  ilRC = iGetConfigEntry(pcgConfigFile,"CDIASS","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
  if (strcmp(pclDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pclDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pclDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }

  /* Get Client and Server chars */
  sprintf(pcgUfisConfigFile,"%s/ufis_ceda.cfg",getenv("CFG_PATH")); 
  ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","CLIENT_CHARS",
                         CFG_STRING, pclClieBuffer);
  if (ilRC == RC_SUCCESS)
  {
     ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","SERVER_CHARS",
                            CFG_STRING, pclServBuffer);
     if (ilRC == RC_SUCCESS)
     {
        ilI = GetNoOfElements(pclClieBuffer,',');
        if (ilI == GetNoOfElements(pclServBuffer,','))	
        {
           memset(pcgServerChars,0x00,100*sizeof(char));
           memset(pcgClientChars,0x00,100*sizeof(char));
           for (ilJ=0; ilJ < ilI; ilJ++) 
           {
              GetDataItem(pclBuffer,pclServBuffer,ilJ+1,',',"","\0\0");
              pcgServerChars[ilJ*2] = atoi(pclBuffer);	
              pcgServerChars[ilJ*2+1] = ',';
           }
           pcgServerChars[(ilJ-1)*2+1] = 0x00;
           for (ilJ=0; ilJ < ilI; ilJ++) 
           {
              GetDataItem(pclBuffer,pclClieBuffer,ilJ+1,',',"","\0\0");
              pcgClientChars[ilJ*2] = atoi(pclBuffer);	
              pcgClientChars[ilJ*2+1] = ',';
           }
           pcgClientChars[(ilJ-1)*2+1] = 0x00;
           dbg(DEBUG,"%s New Clientchars <%s> dec <%s>",pclFunc,pcgClientChars,pclClieBuffer);
           dbg(DEBUG,"%s New Serverchars <%s> dec <%s>",pclFunc,pcgServerChars,pclServBuffer);
        }
     }
     else
     {
        ilRC = RC_SUCCESS;
        dbg(DEBUG,"%s Use standard (old) serverchars",pclFunc);	
     }
  }
  else
  {
     dbg(DEBUG,"%s Use standard (old) serverchars",pclFunc);	
     ilRC = RC_SUCCESS;
  }
  if (ilRC != RC_SUCCESS)
  {
     strcpy(pcgClientChars,"\042\047\054\012\015");
     strcpy(pcgServerChars,"\260\261\262\263\263");
     ilRC = RC_SUCCESS;
  }

  dbg(TRACE,"%s DEBUG_LEVEL = %s",pclFunc,pclDebugLevel);

  return RC_SUCCESS;
} /* Enf of GetConfig */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */

/******************************************************************************/
/* The TimeToStrLocal routine                                                 */
/******************************************************************************/
static int TimeToStrLocal(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)localtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStrLocal */


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;

  if (ipBchdl == TRUE)
  {
     (void) tools_send_info_flag(1900,0,"cdiass","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send Broadcast: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }
  if (ipAction == TRUE)
  {
     (void) tools_send_info_flag(7400,0,"cdiass","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send to ACTION: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }

  return ilRC;
} /* End of TriggerBchdlAction */


/*******************************************++++++++++*************************/
/*   GetSecondsFromCEDATime                                                   */
/*                                                                            */
/*   Serviceroutine, rechnet einen Datumseintrag im Cedaformat in Seconds um  */
/*   Input:  char *  Zeiger auf CEDA-Zeitpuffer                               */
/*   Output: long    Zeitwert                                                 */
/******************************************************************************/

static long GetSecondsFromCEDATime(char *pcpDateTime)
{
  long rc = 0;
  int year;
  char ch_help[5];
  struct tm *tstr1 = NULL, t1;
  struct tm *CurTime, t2;
  time_t llTime;
  time_t llUtcTime;
  time_t llLocalTime;

  CurTime = &t2;
  llTime = time(NULL);
  CurTime = (struct tm *)gmtime(&llTime);
  CurTime->tm_isdst = 0;
  llUtcTime = mktime(CurTime);
  CurTime = (struct tm *)localtime(&llTime);
  CurTime->tm_isdst = 0;
  llLocalTime = mktime(CurTime);
  igDiffUtcToLocal = llLocalTime - llUtcTime;
  /* dbg(TRACE,"UTC: <%ld> , Local: <%ld> , Diff: <%ld>",llUtcTime,llLocalTime,igDiffUtcToLocal); */

  memset(&t1,0x00,sizeof(struct tm));

  tstr1 = &t1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,pcpDateTime,4);
  year = atoi(ch_help);
  tstr1->tm_year = year - 1900;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[4],2);
  tstr1->tm_mon = atoi(ch_help) -1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[6],2);
  tstr1->tm_mday = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[8],2);
  tstr1->tm_hour = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[10],2);
  tstr1->tm_min = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[12],2);
  tstr1->tm_sec = atoi(ch_help);

  tstr1->tm_wday = 0;
  tstr1->tm_yday = 0;
  tstr1->tm_isdst = 0;
  rc = mktime(tstr1);

  rc = rc + igDiffUtcToLocal; /* add difference between local and utc */

  return(rc);
} /* end of GetSecondsFromCEDATime() */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static void TrimLeftRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[0];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && *pclBlank != '\0')
     {
        pclBlank++;
     }
     strcpy(pcpBuffer,pclBlank);
     TrimRight(pcpBuffer);
  }
} /* End of TrimRight */


static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpTime) == 12)
     strcat(pcpTime,"00");
  ilRC = AddSecondsToCEDATime(pcpTime,lpValue,ipP);
  return ilRC;
} /* End of MyAddSecondsToCEDATime */


static char *GetLine(char *pcpLine, int ipLineNo)
{
  char *pclResult = NULL;
  char *pclP = pcpLine;
  int ilCurLine;

  ilCurLine = 1;
  while (*pclP != '\0' && ilCurLine < ipLineNo)
  {
     if (*pclP == '\n')
        ilCurLine++;
     pclP++;
  }
  if (*pclP != '\0')
     pclResult = pclP;

  return pclResult;
} /* End of GetLine */


static void CopyLine(char *pcpDest, char *pcpSource)
{
  char *pclP1 = pcpDest;
  char *pclP2 = pcpSource;

  while (*pclP2 != '\n' && *pclP2 != '\0')
  {
     *pclP1 = *pclP2;
     pclP1++;
     pclP2++;
  }
  *pclP1 = '\0';
  TrimRight(pcpDest);
} /* End of CopyLine */


static int GetNextUrno()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetNextUrno:";
  int ilDebugLevel;

  if (igLastUrnoIdx == 1000 || igLastUrnoIdx < 0)
  {
     ilDebugLevel = debug_level;
     debug_level = 0;
     GetNextValues(pcgNewUrnos,1010);
     debug_level = ilDebugLevel;
     igLastUrnoIdx = 0;
     igLastUrno = atoi(pcgNewUrnos);
  }
  else
  {
     igLastUrnoIdx++;
     igLastUrno++;
  }

  return ilRC;
} /* End of GetNextUrno */


static int HandleInventory(char *pcpCommand, char *pcpSelection)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleInventory:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[1024];
  char pclUrno[16];
  char pclAdid[16];
  char pclSection[16];
  char pclParam[16];
  int ilCount1;
  int ilCount2;
  char pclModId[16];
  char pclCommand[16];
  char pclTable[16];
  char pclPriority[16];
  int ilModId;
  int ilPriority;

  strcpy(pcgDataBuf,"");
  ilCount1 = 0;
  ilCount2 = 0;
  if (strcmp(pcpCommand,"CCAI") == 0)
  {
     strcpy(pclSection,"CCATAB");
     strcpy(pclParam,"SELECT");
  }
  else
  {
     strcpy(pclSection,"AFTTAB");
     strcpy(pclParam,"SELECT_A");
  }
  if (strlen(pcpSelection) == 0)
  {
     ilRC = iGetConfigRow(pcgConfigFile,pclSection,pclParam,CFG_STRING,pclSelection);
     if (ilRC != RC_SUCCESS)
     {
        dbg(TRACE,"%s Missing Configuration for Section <%s> and Parameter <%s>",pclFunc,pclSection,pclParam);
        return ilRC;
     }
  }
  else
     strcpy(pclSelection,pcpSelection);
  if (strcmp(pcpCommand,"CCAI") == 0)
     sprintf(pclSqlBuf,"SELECT URNO,CTYP FROM %s %s",pclSection,pclSelection);
  else
     sprintf(pclSqlBuf,"SELECT URNO,ADID FROM %s %s",pclSection,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",2,",");
     get_real_item(pclUrno,pclDataBuf,1);
     TrimRight(pclUrno);
     get_real_item(pclAdid,pclDataBuf,2);
     TrimRight(pclAdid);
     ilCount1++;
     if (strcmp(pcpCommand,"CCAI") == 0)
        ilRC = HandleFlightCounter("CCAU","I","URNO",pclUrno);
     else
     {
        strcat(pclUrno,",");
        strcat(pclUrno,pclAdid);
        ilRC = HandleFlightCounter("AFTU","I","URNO,ADID",pclUrno);
     }
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  if (strcmp(pcpCommand,"CCAI") == 0)
  {
     ilRC = iGetConfigEntry(pcgConfigFile,"CCATAB","MODID",CFG_STRING,pclModId);
     if (ilRC != RC_SUCCESS)
        strcpy(pclModId,"7922");
     ilRC = iGetConfigEntry(pcgConfigFile,"CCATAB","COMMAND_INV",CFG_STRING,pclCommand);
     if (ilRC != RC_SUCCESS)
        strcpy(pclCommand,"URT");
     ilRC = iGetConfigEntry(pcgConfigFile,"CCATAB","PRIORITY",CFG_STRING,pclPriority);
     if (ilRC != RC_SUCCESS)
        strcpy(pclPriority,"3");
     strcpy(pclTable,"CCATAB");
  }
  else
  {
     ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","MODID",CFG_STRING,pclModId);
     if (ilRC != RC_SUCCESS)
        strcpy(pclModId,"7922");
     if (*pclAdid == 'A')
        ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","COMMAND_INV_A",CFG_STRING,pclCommand);
     else
        ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","COMMAND_INV_D",CFG_STRING,pclCommand);
     if (ilRC != RC_SUCCESS)
        strcpy(pclCommand,"UFR");
     ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","PRIORITY",CFG_STRING,pclPriority);
     if (ilRC != RC_SUCCESS)
        strcpy(pclPriority,"3");
     strcpy(pclTable,"AFTTAB");
  }
  ilPriority = atoi(pclPriority);
  ilModId = atoi(pclModId);
  dbg(TRACE,"%s Send <%s> for <%s> to <%d> , Prio = %d",pclFunc,pclCommand,pclTable,ilModId,ilPriority);
  dbg(TRACE,"%s Selection: <%s>",pclFunc,pclSelection);
  dbg(TRACE,"%s Fields:    <%s>",pclFunc,pcgFieldList);
  dbg(TRACE,"%s Data:      <%s>",pclFunc,pcgDataBuf);
  ilRC = SendCedaEvent(ilModId,0,mod_name,"CEDA",pcgTwStart,pcgTwEnd,
                       pclCommand,pclTable,pclSelection,pcgFieldList,pcgDataBuf,"",
                       ilPriority,NETOUT_NO_ACK);
  if (strcmp(pcpCommand,"CCAI") == 0 || strlen(pcpSelection) > 0)
  {
     dbg(DEBUG,"%s %d Records found",pclFunc,ilCount1);
     return ilRC;
  }
  strcpy(pcgDataBuf,"");
  strcpy(pclParam,"SELECT_D");
  ilRC = iGetConfigRow(pcgConfigFile,pclSection,pclParam,CFG_STRING,pclSelection);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%s Missing Configuration for Section <%s> and Parameter <%s>",pclFunc,pclSection,pclParam);
     return ilRC;
  }
  sprintf(pclSqlBuf,"SELECT URNO FROM %s %s",pclSection,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclUrno);
  while (ilRCdb == DB_SUCCESS)
  {
     TrimRight(pclUrno);
     ilCount2++;
     strcat(pclUrno,",D");
     ilRC = HandleFlightCounter("AFTU","I","URNO,ADID",pclUrno);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclUrno);
  }
  close_my_cursor(&slCursor);
  ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","MODID",CFG_STRING,pclModId);
  if (ilRC != RC_SUCCESS)
     strcpy(pclModId,"7922");
  ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","COMMAND_INV_D",CFG_STRING,pclCommand);
  if (ilRC != RC_SUCCESS)
     strcpy(pclCommand,"UFR");
  ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","PRIORITY",CFG_STRING,pclPriority);
  if (ilRC != RC_SUCCESS)
     strcpy(pclPriority,"3");
  strcpy(pclTable,"AFTTAB");
  ilPriority = atoi(pclPriority);
  ilModId = atoi(pclModId);
  dbg(TRACE,"%s Send <%s> for <%s> to <%d> , Prio = %d",pclFunc,pclCommand,pclTable,ilModId,ilPriority);
  dbg(TRACE,"%s Selection: <%s>",pclFunc,pclSelection);
  dbg(TRACE,"%s Fields:    <%s>",pclFunc,pcgFieldList);
  dbg(TRACE,"%s Data:      <%s>",pclFunc,pcgDataBuf);
  ilRC = SendCedaEvent(ilModId,0,mod_name,"CEDA",pcgTwStart,pcgTwEnd,
                       pclCommand,pclTable,pclSelection,pcgFieldList,pcgDataBuf,"",
                       ilPriority,NETOUT_NO_ACK);
  dbg(DEBUG,"%s %d Arrival Records found",pclFunc,ilCount1);
  dbg(DEBUG,"%s %d Departure Records found",pclFunc,ilCount2);

  return ilRC;
} /* End of HandleInventory */


static int HandleFlightCounter(char *pcpCommand, char *pcpType, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleFlightCounter:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclFldLst[2048];
  char pclDataBuf[16000];
  int ilItemNo;
  char pclUrno[16];
  char pclAdid[16];
  char pclSection[16];
  char pclParam[16];
  int ilNoEle;

  ilItemNo = get_item_no(pcpFields,"URNO",5) + 1;
  if (ilItemNo <= 0)
  {
     dbg(TRACE,"%s No URNO found!",pclFunc);
     return ilRC;
  }
  get_real_item(pclUrno,pcpData,ilItemNo);
  if (strcmp(pcpCommand,"CCAU") == 0)
  {
     strcpy(pclSection,"CCATAB");
     strcpy(pclParam,"FIELDS");
  }
  else
  {
     strcpy(pclSection,"AFTTAB");
     ilItemNo = get_item_no(pcpFields,"ADID",5) + 1;
     if (ilItemNo <= 0)
     {
        dbg(TRACE,"%s No ADID found!",pclFunc);
        return ilRC;
     }
     get_real_item(pclAdid,pcpData,ilItemNo);
     if (*pclAdid == 'A')
        strcpy(pclParam,"FIELDS_A");
     else if (*pclAdid == 'D')
        strcpy(pclParam,"FIELDS_D");
     else
     {
        dbg(TRACE,"%s ADID is <%s>, will not be processed!",pclFunc,pclAdid);
        return ilRC;
     }
  }
  ilRC = iGetConfigEntry(pcgConfigFile,pclSection,pclParam,CFG_STRING,pclFldLst);
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%s Missing Configuration for Section <%s> and Parameter <%s>",pclFunc,pclSection,pclParam);
     return ilRC;
  }
  ilNoEle = GetNoOfElements(pclFldLst,',');
  sprintf(pclSelection,"WHERE URNO = %s",pclUrno);
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s",pclFldLst,pclSection,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     BuildItemBuffer(pclDataBuf,"",ilNoEle,",");
     if (strcmp(pcpCommand,"CCAU") == 0)
        ilRC = HandleCCA(pclUrno,pcpType,pclFldLst,pclDataBuf);
     else
        ilRC = HandleAFT(pclUrno,pclAdid,pcpType,pclFldLst,pclDataBuf);
  }

  return ilRC;
} /* End of HandleFlightCounter */


static int HandleLogoText(char *pcpCommand, char *pcpFields, char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleLogoText:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclDataBuf[1024];
  char pclFldu[16];
  char pclUrno[16];
  int ilItemNo;
  char pclRurn[16];
  char pclAurn[16];
  char pclRtyp[16];

  ilItemNo = get_item_no(pcpFields,"FLDU",5) + 1;
  if (ilItemNo <= 0)
  {
     ilItemNo = get_item_no(pcpFields,"URNO",5) + 1;
     if (ilItemNo <= 0)
     {
        dbg(TRACE,"%s No URNO and no FLDU found!",pclFunc);
        return ilRC;
     }
     get_real_item(pclUrno,pcpData,ilItemNo);
     if (strcmp(pcpCommand,"FLZU") == 0)
        sprintf(pclSqlBuf,"SELECT FLGU FROM FLZTAB WHERE URNO = %s",pclUrno);
     else
        sprintf(pclSqlBuf,"SELECT FLGU FROM FXTTAB WHERE URNO = %s",pclUrno);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFldu);
     close_my_cursor(&slCursor);
     if (ilRCdb != DB_SUCCESS)
     {
        dbg(TRACE,"%s Record with URNO does not exist!",pclFunc);
        return ilRC;
     }
     TrimRight(pclFldu);
  }
  get_real_item(pclFldu,pcpData,ilItemNo);
  sprintf(pclSqlBuf,"SELECT RURN,AURN,RTYP FROM FLDTAB WHERE URNO = %s",pclFldu);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  close_my_cursor(&slCursor);
  if (ilRCdb != DB_SUCCESS)
  {
     dbg(TRACE,"%s FLDTAB Record with URNO <%s> does not exist!",pclFunc,pclFldu);
     return ilRC;
  }
  BuildItemBuffer(pclDataBuf,"",3,",");
  get_real_item(pclRurn,pclDataBuf,1);
  TrimRight(pclRurn);
  get_real_item(pclAurn,pclDataBuf,2);
  TrimRight(pclAurn);
  get_real_item(pclRtyp,pclDataBuf,3);
  TrimRight(pclRtyp);
  if (strcmp(pclRtyp,pcgRtypForCounters) == 0)
     ilRC = HandleFlightCounter("CCAU","A","URNO",pclRurn);
  else if (strcmp(pclRtyp,pcgRtypForGates) == 0)
  {
     strcat(pclAurn,",D");
     ilRC = HandleFlightCounter("AFTU","A","URNO,ADID",pclAurn);
  }
  else if (strcmp(pclRtyp,pcgRtypForBelts) == 0)
  {
     strcat(pclAurn,",A");
     ilRC = HandleFlightCounter("AFTU","A","URNO,ADID",pclAurn);
  }
  else

  return ilRC;
} /* End of HandleLogoText */


static int HandleAFT(char *pcpUrno, char *pcpAdid, char *pcpType, char *pcpFldLst, char *pcpDataBuf)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleAFT:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclFldUrno[16];
  int ilItemNo;
  char pclLocation[16];
  char pclRtypForLocation[16];
  char pclModId[16];
  char pclCommand[16];
  char pclPriority[16];
  int ilModId;
  int ilPriority;

  if (*pcpAdid == 'A')
  {
     strcat(pcpFldLst,",B1LN,B1T1,B1T2,B2LN,B2T1,B2T2");
     ilItemNo = get_item_no(pcpFldLst,"BLT1",5) + 1;
  }
  else
  {
     strcat(pcpFldLst,",G1LN,G1T1,G1T2,G2LN,G2T1,G2T2");
     ilItemNo = get_item_no(pcpFldLst,"GTD1",5) + 1;
  }
  if (ilItemNo > 0)
  {
     get_real_item(pclLocation,pcpDataBuf,ilItemNo);
     TrimRight(pclLocation);
     if (*pclLocation != ' ')
     {
        if (*pcpAdid == 'A')
           strcpy(pclRtypForLocation,pcgRtypForBelts);
        else
           strcpy(pclRtypForLocation,pcgRtypForGates);
        sprintf(pclSqlBuf,"SELECT URNO FROM FLDTAB WHERE AURN = %s AND RNAM = '%s' AND RTYP = '%s'",
                pcpUrno,pclLocation,pclRtypForLocation);
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFldUrno);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
        {
           TrimRight(pclFldUrno);
           ilRC = HandleFLZ_FXT(pclFldUrno,pcpFldLst,pcpDataBuf);
        }
        else
           strcat(pcpDataBuf,", , , ");
        if (*pcpAdid == 'A')
           ilItemNo = get_item_no(pcpFldLst,"BLT2",5) + 1;
        else
           ilItemNo = get_item_no(pcpFldLst,"GTD2",5) + 1;
        if (ilItemNo > 0)
        {
           get_real_item(pclLocation,pcpDataBuf,ilItemNo);
           TrimRight(pclLocation);
           if (*pclLocation != ' ')
           {
              sprintf(pclSqlBuf,"SELECT URNO FROM FLDTAB WHERE AURN = %s AND RNAM = '%s' AND RTYP = '%s'",
                      pcpUrno,pclLocation,pclRtypForLocation);
              slCursor = 0;
              slFkt = START;
              dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFldUrno);
              close_my_cursor(&slCursor);
              if (ilRCdb == DB_SUCCESS)
              {
                 TrimRight(pclFldUrno);
                 ilRC = HandleFLZ_FXT(pclFldUrno,pcpFldLst,pcpDataBuf);
              }
              else
                 strcat(pcpDataBuf,", , , ");
           }
           else
              strcat(pcpDataBuf,", , , ");
        }
        else
           strcat(pcpDataBuf,", , , ");
     }
     else
        strcat(pcpDataBuf,", , , , , , ");
  }
  else
     strcat(pcpDataBuf,", , , , , , ");

  if (*pcpType == 'A')
  {
     ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","MODID",CFG_STRING,pclModId);
     if (ilRC != RC_SUCCESS)
        strcpy(pclModId,"7922");
     if (*pcpAdid == 'A')
        ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","COMMAND_ACT_A",CFG_STRING,pclCommand);
     else
        ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","COMMAND_ACT_D",CFG_STRING,pclCommand);
     if (ilRC != RC_SUCCESS)
        strcpy(pclCommand,"UFR");
     ilRC = iGetConfigEntry(pcgConfigFile,"AFTTAB","PRIORITY",CFG_STRING,pclPriority);
     if (ilRC != RC_SUCCESS)
        strcpy(pclPriority,"3");
     ilPriority = atoi(pclPriority);
     ilModId = atoi(pclModId);
     dbg(TRACE,"%s Send <%s> for <AFTTAB> to <%d> , Prio = %d",pclFunc,pclCommand,ilModId,ilPriority);
     dbg(TRACE,"%s Selection: <%s>",pclFunc,pcpUrno);
     dbg(TRACE,"%s Fields:    <%s>",pclFunc,pcpFldLst);
     dbg(TRACE,"%s Data:      <%s>",pclFunc,pcpDataBuf);
     ilRC = SendCedaEvent(ilModId,0,mod_name,"CEDA",pcgTwStart,pcgTwEnd,
                          pclCommand,"AFTTAB",pcpUrno,pcpFldLst,pcpDataBuf,"",
                          ilPriority,NETOUT_NO_ACK);
  }
  else
  {
     strcpy(pcgFieldList,pcpFldLst);
     strcat(pcgDataBuf,pcpDataBuf);
     strcat(pcgDataBuf,"\r\n");
  }

  return ilRC;
} /* End of HandleAFT */


static int HandleCCA(char *pcpUrno, char *pcpType, char *pcpFldLst, char *pcpDataBuf)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleCCA:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclFldUrno[16];
  char pclModId[16];
  char pclCommand[16];
  char pclPriority[16];
  int ilModId;
  int ilPriority;

  strcat(pcpFldLst,",LGSN,TXT1,TXT2");
  sprintf(pclSqlBuf,"SELECT URNO FROM FLDTAB WHERE RURN = %s AND RTYP = '%s'",pcpUrno,pcgRtypForCounters);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFldUrno);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     TrimRight(pclFldUrno);
     ilRC = HandleFLZ_FXT(pclFldUrno,pcpFldLst,pcpDataBuf);
  }
  else
     strcat(pcpDataBuf,", , , ");

  if (*pcpType == 'A')
  {
     ilRC = iGetConfigEntry(pcgConfigFile,"CCATAB","MODID",CFG_STRING,pclModId);
     if (ilRC != RC_SUCCESS)
        strcpy(pclModId,"7922");
     ilRC = iGetConfigEntry(pcgConfigFile,"CCATAB","COMMAND_ACT",CFG_STRING,pclCommand);
     if (ilRC != RC_SUCCESS)
        strcpy(pclCommand,"URT");
     ilRC = iGetConfigEntry(pcgConfigFile,"CCATAB","PRIORITY",CFG_STRING,pclPriority);
     if (ilRC != RC_SUCCESS)
        strcpy(pclPriority,"3");
     ilPriority = atoi(pclPriority);
     ilModId = atoi(pclModId);
     dbg(TRACE,"%s Send <%s> for <CCATAB> to <%d> , Prio = %d",pclFunc,pclCommand,ilModId,ilPriority);
     dbg(TRACE,"%s Selection: <%s>",pclFunc,pcpUrno);
     dbg(TRACE,"%s Fields:    <%s>",pclFunc,pcpFldLst);
     dbg(TRACE,"%s Data:      <%s>",pclFunc,pcpDataBuf);
     ilRC = SendCedaEvent(ilModId,0,mod_name,"CEDA",pcgTwStart,pcgTwEnd,
                          pclCommand,"CCATAB",pcpUrno,pcpFldLst,pcpDataBuf,"",
                          ilPriority,NETOUT_NO_ACK);
  }
  else
  {
     strcpy(pcgFieldList,pcpFldLst);
     strcat(pcgDataBuf,pcpDataBuf);
     strcat(pcgDataBuf,"\r\n");
  }

  return ilRC;
} /* End of HandleCCA */


static int HandleFLZ_FXT(char *pcpFldUrno, char *pcpFldLst, char *pcpDataBuf)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleFLZ_FXT:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSelection[1024];
  char pclSqlBuf[1024];
  char pclFlgUrno[16];
  char pclFlgLgsn[64];
  char pclFxtText[512];
  int ilCount;

  sprintf(pclSqlBuf,"SELECT FLGU FROM FLZTAB WHERE FLDU = %s",pcpFldUrno);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFlgUrno);
  close_my_cursor(&slCursor);
  if (ilRCdb == DB_SUCCESS)
  {
     TrimRight(pclFlgUrno);
     sprintf(pclSqlBuf,"SELECT LGSN FROM FLGTAB WHERE URNO = %s",pclFlgUrno);
     slCursor = 0;
     slFkt = START;
     dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFlgLgsn);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
     {
        TrimRight(pclFlgLgsn);
        strcat(pcpDataBuf,",");
        strcat(pcpDataBuf,pclFlgLgsn);
     }
     else
        strcat(pcpDataBuf,", ");
  }
  else
     strcat(pcpDataBuf,", ");
  ilCount = 0;
  sprintf(pclSqlBuf,"SELECT TEXT FROM FXTTAB WHERE FLDU = %s ORDER BY SORT",pcpFldUrno);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFxtText);
  while (ilRCdb == DB_SUCCESS)
  {
     ilCount++;
     TrimRight(pclFxtText);
     strcat(pcpDataBuf,",");
     strcat(pcpDataBuf,pclFxtText);
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFxtText);
  }
  close_my_cursor(&slCursor);
  if (ilCount == 0)
     strcat(pcpDataBuf,", , ");
  else if (ilCount == 1)
     strcat(pcpDataBuf,", ");

  return ilRC;
} /* End of HandleFLZ_FXT */

