# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Make/GlobalDefines 1.8 2006/01/20 17:19:28CET jim Exp  $
#
# 20020524 JIM:  added DSPPRJ for dsphdl (defined in $FS/Makefile)
# 20020711 JIM:  __FILESTAT__
# 20021119 JIM:  Test for malformed archives removed
# 20030318 JIM:  avoid "cc: Warning: -g conflicts with auto-inlining,..."
# 20030408 JIM: added "-lmqic -lmqmcs" for Webspere MQ (only to DEFAULT)
# 20030410 HEB: removed "-lmqic -lmqmcs" for Webspere MQ (only to DEFAULT)
# 20041018 JIM: step 1: if WALL is set in env, gcc is used and value of WALL
#                       is appended to COMPILE
# 20041018 JIM: step 2: if GCC is set in env, gcc is used
# 20041018 JIM: step 3: if gcc is used, "misalign" is replaced by other usefull
#                       values of GCC
# 20050304 JIM: checking $ORACLE_HOME/lib against $LD_LIBRARY_PATH to avoid
#               using of 64 bit lib
# 20050901 JIM: set WMQFLAG=-DWMQ for i.e PVG (uses WMQ instead of SYSQCP))
# 20060120 JIM: added settings for AIX

ifneq "$(WALL)" ""
CC=gcc
endif

ifeq "$(CC)" ""
#CC=gcc
CC=cc
endif

RCC=$(CC)
#MORELAGS=-DMORE_DEBUG

MY_OS=$(shell uname -s)
MY_USER=$(LOGNAME)@$(shell uname -n)/$(shell uname -s)
#Get Version Nymber remove the B and . in order to find the version Number 
# Specific for the HPUX version in case the version is Bigger 1129
# then expr should return 0 othrwise 1 
MY_OS_V := $(shell expr 1129 \> `uname -r |tr -d "B'."`)

# switches for i.e debuger ddd
#SYMBOLS= -g -v -d y
#SYMBOLS=-g -fast
SYMBOLS=-g

# 20050901 JIM:
# set WMQFLAG=-DWMQ for i.e PVG (uses WMQ instead of SYSQCP))
# the WMQFLAG then should be used in CFLAGS also...
WMQFLAG=
ifneq "$(WMQFLAG)" ""
MQMLIBS=-lmqm -lmqmcs 
endif

# 20050902 JIM: for multithreaded BCCOM settings are OS-depending:
MTSWITCH=-mt
MTLIB=-lthread

# where to find Librarys
# 20050304 JIM: may be 64 bit first: LIBFLAGS= -L$(ORACLE_HOME)/lib -L$(LD_LIBRARY_PATH)
ORALIBPATH=$(findstring $(ORACLE_HOME)/lib32,$(LD_LIBRARY_PATH))
ifeq "$(ORALIBPATH)" ""
   ORALIBPATH=$(findstring $(ORACLE_HOME)/lib,$(LD_LIBRARY_PATH))
endif
ifeq "$(ORALIBPATH)" ""
   $(warning "+++ ORACLE_HOME/lib is not in LD_LIBRARY_PATH +++")
   ORALIBPATH=$(ORACLE_HOME)/lib
endif
LIBFLAGS= -L$(ORALIBPATH) -L$(LD_LIBRARY_PATH)

# which Librarys
# UNIX_LIBS= -lnsl -lcurses -lmqic -lmqmcs -lsocket -lxnet -lm -lmalloc
UNIX_LIBS= -lnsl -lcurses -lsocket -lxnet -lm -lmalloc 
CEDA_LIB= $(L)/CEDAlib.a 
DB_LIB= $(L)/DBLIB.a 
ADD_LIBS=$(UNIX_LIBS) $(CEDA_LIB)

############ SUN SOLARIS ##################
ifeq "$(MY_OS)" "SunOS"
	ORA_LIBS= -lclntsh 
	CCS_OS=-D_SOLARIS   
	# -xtarget=ultra3 -m32	-xpagesize=64K 
	SCRIBA_OS=-D_SUN_UNIX
  # 20040503 JIM: avoid "cc1: Invalid option 'isalign'" with gcc
  ifneq "$(CC)" "gcc"
  	# switches for optimization or Word-Alignment
    # 20030318 JIM:  avoid "cc: Warning: -g conflicts with auto-inlining,..."
    ifeq "$(SYMBOLS)" "-g"
	    COMPILE= -misalign $(WALL)
    else
	    COMPILE= -fast -misalign $(WALL)
    endif
  else
      COMPILE= -D__USE_SVID -D__USE_GNU -fwritable-strings $(WALL)
  endif
############ SCO UNIXWARE #################
else 
	ifeq "$(MY_OS)"  "UnixWare"
	# switches for optimization or Word-Alignment
	ORA_LIBS= -lclntsh -Kthread
	COMPILE=$(WALL)
	CCS_OS=-D_UNIXWARE
	SCRIBA_OS=-D_SCO_UNIX
	# SCRIBA_OS kann noch wegoptimiert werden, wenn in scbhdl _SOLARIS, _UNIXWARE
	# etc... benutzt werden.
else
############ LINUX #################
	ifeq "$(MY_OS)"  "Linux"
	ORA_LIBS= -lclntsh 
	CCS_OS=-D_LINUX
	SCRIBA_OS=-D_LX_UNIX
	# switches for optimization or Word-Alignment
	COMPILE= -D__USE_SVID -D__USE_GNU  $(WALL)
	# -fwritable-strings
	# which Librarys
	#UNIX_LIBS= -lm -lrpcsvc -lncurses 
	UNIX_LIBS= -lm  -lrpcsvc -lncurses $(MQMLIBS) 
	# 20050902 JIM: for multithreaded BCCOM settings are OS-depending:
	MTSWITCH=-D__USE_UNIX98 -D_GNU_SOURCE -D_POSIX_PTHREAD_SEMANTICS
	MTLIB=-lpthread
	else
############ HP-UX #################
	ifeq "$(MY_OS)" "HP-UX"
	#UNIX_LIBS= -lnsl -lcurses -lxnet -lm -lmalloc -l:libcma.sl -lcl -l:libcl.a
	# libcma is obsolete (depreciated) under HP-UX 11i
	# libmalloc libnet is obsolete under 11.23 change it to -lm
	UNIX_LIBS= -lnsl -lcurses -lxnet -lm  -lcl 
	ORA_LIBS= -lclntsh 
	CCS_OS=-D_HPUX_SOURCE
	# Aa=ansi conform; u1=assume single byte alignment; DD32=compile 32bit compliant
	
	ifeq  "$(MY_OS_V)" "0" 
		CCS_OS=-D_HPUX_SOURCE -D_HPUX113_SOURCE	
	endif	 
	COMPILE= -Ae +u1 +DD32 $(WALL)
else
############ AIX #################
	ifeq "$(MY_OS)" "AIX"
	# on AIX the linker is easier:
	UNIX_LIBS=
	ORA_LIBS= -lclntsh -lcurses -lxnet -lm
	CCS_OS=-D_AIX
	# Aa=ansi conform; u1=assume single byte alignment; DD32=compile 32bit compliant
	COMPILE=$(WALL)
else
############ unknown ###############
	$(warning "+++ WARNING: 'uname -s' liefert ein nicht bekanntes OS: $(MY_OS)")
	$(warning "+++ MY_OS bitte in $$(PB)/GlobalDefines nachtragen")
	$(warning "+++ SCRIBA_OS bitte in $(RS)/scbhdl pruefen")
	CCS_OS=
	SCRIBA_OS=
endif
endif
endif
endif
endif



# Include-Dirs and some defines
CFLAGS=$(DSPPRJ) -DUFIS43 -I$(I) -DCCSDB_ORACLE $(CCS_OS) $(SCRIBA_OS) -D_CDICLIENT \
  -D__FILESTAT__="\"`filestat $<` $(MY_USER) $(PRJNAME) compiled at: \" \
   __DATE__\" \"__TIME__"

# oracle proc compiler:
PCCINC=$(ORACLE_HOME)/precomp/public
PCCINCLUDE= INCLUDE=$(PCCINC) INCLUDE=$(I) 
#PCCOPT= DBMS=V7 ORACA=YES HOLD_CURSOR=YES RELEASE_CURSOR=NO MAXOPENCURSORS=20 \
# SQLCHECK=SEMANTICS MODE=ORACLE
# For PA-RISC (BLR1) we have to add the following 
# DEFINE=__STDC__ DEFINE=_PA_RISC2_0
#
PCCOPT= DBMS=NATIVE ORACA=YES HOLD_CURSOR=YES RELEASE_CURSOR=NO MAXOPENCURSORS=20 \
        SQLCHECK=SEMANTICS MODE=ORACLE
PCCRECL= IRECLEN=247 ORECLEN=247 LRECLEN=255 
#PCCRECL= IRECLEN=1024 ORECLEN=1024 LRECLEN=1024

PROC=$(ORACLE_HOME)/bin/proc
PCCFLAGS= $(PCCINCLUDE) $(PCCRECL) $(PCCOPT)
