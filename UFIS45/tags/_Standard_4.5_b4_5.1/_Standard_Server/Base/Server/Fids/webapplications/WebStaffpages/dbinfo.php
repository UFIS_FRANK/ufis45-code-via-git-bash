<?php
// 20060621 JIM: PRF 8203: correct sorting via "ORDER BY" instead
//                         of sortColumn(). Note: To avoid sorting
//                         by tooltips ("New Dehli" instead of "DEL")
//                         before the Tooltip a HTML Comment has to be
//                         added ('<!--'||APC3||'--><font tooltip=...')




include("include/manager.php");
include("$ADODB_RootPath/list.php");
//include("include/template.php");

//include("include/class.overlib/class.overlib.php3");
include("include/class.LoginInit.php");


/* Login Check */
/* Set Login Instance*/
$login = new  LoginInit;
//$tpl = new Template($tmpl_path);

$smarty = new Smarty;

$smarty->caching=0;
$smarty->compile_check =true;
$smarty->debugging = false;

//20060710 JIM: PRF 8204: excel export: set default
if (!isset($Export)) {
    $Export="No";
}




//20070221 GFO: WAW Project : Added $LangTR for the Second Language Support^M
if (!isset($Lang)) {
//                      $LangTR = "";
    if (!(tep_session_is_registered("Lang_TR"))) {
        set_session("Lang_TR", "");
        //$_Session["Lang_TR"]="";
    }
} else {
    if ($Lang != "ENG" && len($Lang)>0) {
        set_session("Lang_TR", "_TR");
        //$_Session["Lang_TR"]="_TR";
    }       else {
        set_session("Lang_TR", "");
        //$_Session["Lang_TR"]="";
    }
}

$LangTR = get_session("Lang_TR");

$apfn = "apfn";
$beme = "beme";

if ($LangTR=="_TR") {
    $apfn = "apsn";
    $beme = "bemd";
}


if ((tep_session_is_registered("userid"))) {
    // The user has already been Logged
//20060710 JIM: PRF 8204: excel export: added $Export
//		$Query=$login->Tmpl($Session["userid"]);
    $Query=$login->Tmpl(get_session("userid"),$Export);
    $smarty->assign("DateTime", strftime("created at %d.%m.%Y, %H:%M",time()) );
} else {

    //20060710 JIM: PRF 8204: excel export: added $Export
    //$Query=	$login->Login($UserName,$Password);
    $UserName = $_REQUEST['UserName'];
    $Password = $_REQUEST['Password'];
    $Export = $_REQUEST['Export'];
    $Query=	$login->Login($UserName,$Password,$Export);
}

$debug = $_REQUEST['$debug'];
$TMPL_MENUID = $_REQUEST['TMPL_MENUID'];


//$debug = false;
/* Start Debug  if requested */
if ($debug=="true") {
    $tmpdebug="?debug=true";
    $db->debug=$debug;
}
else {
    $debug="false";
}
//$debug=true;
//$db->debug=$debug;





/* Init the time-diff. variables */
FindTDI();

// Expires the Page
$Cexpires=date("D, d M Y H:i:s ",AddDiff("S",$Refresh,time()))." GMT";

/* setting a default of Main and codeshare search if not set using the search mask */
if (empty($SearchType))
    $SearchType="MAC";

$SearchFlno="";
// Building SearchFlno variable if main-carrier shall be searched
if ($SearchType=="MC" || $SearchType=="MAC") {
    if  (len($airline)>0) {
        $airline=strtoupper($airline);
        $SearchFlno=$airline;
    }
    if (len($flightnumber)>0) {
        if (len($airline)>2) {
            $SearchFlno=$airline."%".$flightnumber;
        }
        if (len($airline)<=2) {
            $SearchFlno=$airline." "."%".$flightnumber;
        }
        if (len($airline)==0) {
            $SearchFlno=$flightnumber;
        }
    }
}
else {
    // on empty SearchType the user requests everything
    if (!empty($SearchType))
        $SearchFlno="<not used>";
}

// Building SearchJfno variable if codeshares shall be searched
$SearchJfno="";
if ($SearchType=="CS" || $SearchType=="MAC") {
    if (len($airline)>0) {
        $airline=strtoupper($airline);
        $SearchJfno=$airline;
    }
    if (len($flightnumber)>0) {
        if (len($airline)>2) {
            $SearchJfno=$airline."%".$flightnumber;
        }
        if (len($airline)<=2) {
            $SearchJfno=$airline." "."%".$flightnumber;
        }
        if (len($airline)==0) {
            $SearchJfno=$flightnumber;
        }
    }
}
else {
    // on empty SearchType the user requests everything
    if (!empty($SearchType))
        $SearchJfno="<not used>";
}
// ##################################################################
// ### starting to check if times have been set using the search mask
// ##################################################################


        //################################
        //# FROM DATE TIMEFRAME SETTING ##
        //################################
        // ### here we check for fromdateMonth,etc. ###
        // add or substract your value here for the default timeframe start

        if (checkdate ($TfVal1Month, $TfVal1Day, $TfVal1Year)) {
           $fromdate=mktime($TfVal1Hour ,$TfVal1Min, 0,$TfVal1Month ,$TfVal1Day, $TfVal1Year);
        }
        //##############################
        //# TO DATE TIMEFRAME SETTING ##
        //##############################
        // ### depending on the search-template either todateplus or todateMonth, etc. will have been set ###
        // add or substract your value here for the default timeframe end
        if ($todateplus && isset($fromdate))
        {
           $TfVal1=$fromdate ;
           $TfVal2=AddDiff("H",$todateplus,$fromdate);

        }


        //echo "Fromdate:($fromdateYear$fromdateMonth$fromdateDay$fromdateHour$fromdateMin)<br>";



// 20060307 JIM: PRF 8201: BEGIN Step 1 Timeframe settings
if (! $TfVal1_curr) {
    $TfVal1_curr=$TfVal1 / 60 ;
}
if (! $TfVal2_curr) {
    $TfVal2_curr=$TfVal2 / 60 ;
}
if ($TfVal1_curr) {
    $TfVal1=$TfVal1_curr * 60;
}
if ($TfVal2_curr) {
    $TfVal2=$TfVal2_curr * 60;
}
// 20060307 JIM: PRF 8201: END Step 1 Timeframe settings

if (!isset($fromdate)) {
    $TfVal1=time(0) + ($TfVal1*60);
    $TfVal2=time(0) + ($TfVal2*60);
}



//	$TfVal1=date("YmdHis",$TfVal1);

// 20070314 GFO: Solving the UTC Problem for the select statement
$TfVal1=gmdate("YmdHis",$TfVal1);
//	$TfVal2=date("YmdHis",$TfVal2);
// 20070314 GFO: Solving the UTC Problem for the select statement
$TfVal2=gmdate("YmdHis",$TfVal2);



$TfVal1Month=date("m",$TfVal1);
$TfVal1Year=date("Y",$TfVal1);
$TfVal1Day=date("d",$TfVal1);
$TfVal1Hour=date("H",$TfVal1);
$TfVal1Min=date("i",$TfVal1);

$TfVal2Month=date("m",$TfVal2);
$TfVal2Year=date("Y",$TfVal2);
$TfVal2Day=date("d",$TfVal2);
$TfVal2Hour=date("H",$TfVal2);
$TfVal2Min=date("i",$TfVal2);



// Setting search mask defaults on todays date at 00:00
$SearchstartdateMonth=date("m",time(0));
$SearchstartdateYear=date("Y",time(0));
$SearchstartdateDay=date("d",time(0));
$SearchstartdateHour="00";
$SearchstartdateMin="00";


// values for HTTP header
if ($Export=="Export") {
    // GFO 20070313: Remove the Images from the datavalue
    $PageTitle=eregi_replace('<[^>]*img*\"?[^>]*>', "",$PageTitle);
}

// values for HTTP header


$smarty->assign("PageTitle", $PageTitle );
$smarty->assign("STYLESHEET", $StyleSheet );
$smarty->assign("Refresh",$RefreshRate * 1000);
$smarty->assign("RefreshRate",$RefreshRate * 1000);
$smarty->assign("Cexpires",$Cexpires );

// values for div search mask
$smarty->assign("SearchUsed",$SearchUsed);
$smarty->assign("SearchstartdateMonth",$SearchstartdateMonth);
$smarty->assign("SearchstartdateYear",$SearchstartdateYear);
$smarty->assign("SearchstartdateDay",$SearchstartdateDay);
$smarty->assign("SearchstartdateHour",$SearchstartdateHour);
$smarty->assign("SearchstartdateMin",$SearchstartdateMin);
$smarty->assign("SearchenddateMonth",$SearchenddateMonth);
$smarty->assign("SearchenddateYear",$SearchenddateYear);
$smarty->assign("SearchenddateDay",$SearchenddateDay);
$smarty->assign("SearchenddateHour",$SearchenddateHour);
$smarty->assign("SearchenddateMin",$SearchenddateMin);
// values for div search mask
$smarty->assign("overlib",$overlib);
$smarty->assign("alfn",$alfn );
$smarty->assign("alfnspaces",$alfnspaces );
$smarty->assign("apfn",$apfn );
$smarty->assign("apfnspaces",$apfnspaces );
//$smarty->assign("FromDateHeader",$FromDateHeader );
//$smarty->assign("ToDateHeader",$ToDateHeader );
$smarty->assign("dateerror",$dateerror );
$smarty->assign("type",$type );
$smarty->assign("TMPL_MENUID",$TMPL_MENUID );
$smarty->assign("TfVal1",$TfVal1);
// 20060307 JIM: PRF 8201: Step 2 Timeframe settings
$smarty->assign("TfVal1_curr",$TfVal1_curr);
$smarty->assign("TfVal1Month",$TfVal1Month );
$smarty->assign("TfVal1Year",$TfVal1Year );
$smarty->assign("TfVal1Day",$TfVal1Day );
$smarty->assign("TfVal1Hour",$TfVal1Hour );
$smarty->assign("TfVal1Min",$TfVal1Min );
$smarty->assign("TfVal2",$TfVal2);
// 20060307 JIM: PRF 8201: Step 3 Timeframe settings
$smarty->assign("TfVal2_curr",$TfVal2_curr);
$smarty->assign("TfVal2Month",$TfVal2Month );
$smarty->assign("TfVal2Year",$TfVal2Year );
$smarty->assign("TfVal2Day",$TfVal2Day );
$smarty->assign("TfVal2Hour",$TfVal2Hour );
$smarty->assign("TfVal2Min",$TfVal2Min );
$smarty->assign("OrgDes",$OrgDes );

$airline=strtoupper($airline);
$smarty->assign("airline",$airline );
//$smarty->assign("flno",$flno );
$flightnumber=strtoupper($flightnumber);
$smarty->assign("flightnumber",$flightnumber);
$smarty->assign("SearchFlno",$SearchFlno);
$smarty->assign("SearchJfno",$SearchJfno);
$city=strtoupper($city);
$smarty->assign("city",$city );
$smarty->assign("PHP_SELF",$PHP_SELF );
$smarty->assign("debug",$debug);
// 20060621 JIM: PRF 8203: Step No 1: correct sorting
// 20080312 VIC:		 : Commented, this is causing two hidden variable for order by and creating confusion for the dbinfo.php.
//$smarty->assign("OrderBy",$OrderBy);
//$smarty->assign("DynamicSearch","");



// End Header
//20100210 GFo: Change in order not to display the first Menu on the time




if (strlen ($TMPL_MENUID)>0 || $DisplayFirstMenu) {
   $smarty->assign("SearchButton","<button class=\"ui-button\" id=\"openSearch\">Search </button>");
   //$smarty->assign("ajaxLoad","<script type=\"text/javascript\">flightData.fnReloadAjax('dbinfoAjax.php?TMPL_MENUID=$TMPL_MENUID');flightData.fnDraw();;</script>");
// We assign the values to the TDplus Select
    $i = -36;
    $todatePlus = array();
    while ($i <=36) {
        $tmps="";
        // if ($i == $todateplus) {
        // default selected entry of the std_search-window
        if ($i == 24) {
            $tmps= " selected";
        }
        if ($i<0)
            array_push($todatePlus, "<option value=\"$i\" $tmps>-$i");
        else
            array_push($todatePlus, "<option value=\"$i\" $tmps>+$i");
        //$tpl->parse("TDplus", true);
        $i++;
    }
     $smarty->assign("TDplus",$todatePlus);


// 20060621 JIM: PRF 8203: END Step 2: correct sorting
    $FldArray = $login->FldDSrows ($FldArray);
    if ($debug == "true" ) {
        echo "inf1.php: QUERY:$Query<br>";
        echo "<br>";
        echo "inf1.php: EQUERY:$ExtraQuery<br>";
        echo "inf1.php: PHP_SELF:$PHP_SELF<br>";
        echo "inf1.php: OrderBy:$OrderBy<br>";
        echo "inf1.php: TfVal1: $TfVal1<br>";
        echo "inf1.php: TfVal2: $TfVal2<br>";
    }
    $GLOBALS["COLUMN_NAMES"]="";
    $smarty->assign( "TableHeader",$login->FldHeader ($FldArray,$Export) );
    $smarty->assign( "TableColumns",$GLOBALS["COLUMN_NAMES"]);


} else {

    $smarty->assign("NoRows","<script>flightData.fnDestroy();</script>");
    $smarty->assign("PageTitle","Flight Information");
    $smarty->assign("SearchButton","");

}

$smarty->display($TemplateName);

exit;
?>
