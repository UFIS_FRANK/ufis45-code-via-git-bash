<?php 
$DEBUG = true;
include("includes/config.php");
include("includes/functions.php");

date_default_timezone_set ("Asia/Calcutta");


$Javascriptzeit = date("Y, n, j, G, i, s");
$refreshtime = 10000;

$display = $_SERVER['REMOTE_ADDR'];
if($display == "127.0.0.1") $display="172.17.9.168";

$display="172.17.9.168";

//Open Oracle Connection
$conn = OCILogon($webfids["OraUser"], $webfids["OraPass"], $webfids["OraDb"]);

//if (empty($_REQUEST["pointer"])) $pointer = 0;


if($page_array = getPagesByIp($display))
{
  if ($DEBUG)
  {
    echo "<pre>DEBUG: Size of \$page_array = ".sizeof($page_array)."<br>";
    print_r($page_array);
    echo "</pre>";
  }


  if (is_null($_REQUEST["pointer"]))
  {
    $pointer = 0;
    if ($DEBUG) echo "<hr>empty pointer = $pointer<hr>";
  }
  elseif ($_REQUEST["pointer"] == sizeof($page_array)-1)
  {
    $pointer = 0;
    if ($DEBUG) echo "<hr>sizeof pointer reached = $pointer <hr>";
  }
  else
  {
    $pointer = $_REQUEST["pointer"] + 1;
    if ($DEBUG) echo "<hr>pointer was added by 1 = $pointer<hr>";
  }

  $page_data = getDataByIpAndPage($display, $page_array[$pointer]["PAGE_ID"]);

  if ($DEBUG)
  {
    echo "<pre>\$page_data: ";
    print_r($page_data);
    echo "</pre>";
  }
  $template = trim($page_array[$pointer]["PAGE_ID"]).".html";
  $css = trim($page_array[$pointer]["PAGE_ID"]).".css";
}
else 
{
  $template = "systemdefault.html";
  $css = "fids.css";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>UFIS Webfids <?php echo $template; ?></title>
	<link rel="STYLESHEET" type="text/css" href="css/<?php echo $css; ?>">
	<script language="JavaScript">
	<!--
	var Serverzeit = new Date(<?php echo $Javascriptzeit; ?>);
	//-->
  </script>
  <script src="js/scripts.js" type="text/javascript"></script>
  <meta http-equiv="refresh" content="<?php echo $refreshtime; ?>; URL=webfids.php?pointer=<?php echo $pointer; ?>">
</head>
<body onLoad="UhrzeitAnzeigen();">

<?php 
include("templates/".$template);
//include("templates/BTT852W.html");
?>

</body>
</html>
<?php 

//close Oracle Connection
OCILogoff($conn);
?>