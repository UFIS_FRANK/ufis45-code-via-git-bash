CREATE OR REPLACE FUNCTION FUN_CALCULATE_TIME (p_Source IN CHAR,
								 			   p_Format IN CHAR,
								               p_Offset IN CHAR,
								               p_TimeIndex IN CHAR)
	RETURN CHAR
	IS
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Function              - FUN_CALCULATE_TIME
    --
    --      Version               - 1.0.0
    --
    --      Written By            - Juergen Hammerschmid
    --
    --      Return                -1 = OK
	--
	--		Error				  - Errors will be written into the table FIDS_ERRORS
    --
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------
    --      Change Log
    --
    --      Name           Date               Version    Change
    --      ----           ----------         -------    ------
	--      Hammerschmid   31.03.2008         1.0        new
    ---------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------

	-- For DateTime functions an offset of 1 is 1 day = 1440 minutes = 86400 sec.
	-- Example: v_NewDateTime + 60 minutes -> v_NewDateTime := v_NewDateTime + (60/1440);

	v_NewDateTime		DATE;
	v_DateOut			DATE;
	v_DayIn				NUMBER;
	v_DayOut			NUMBER;
	v_NewTime			APTTAB.tich%TYPE;
	v_Offset			FLOAT;
	v_Convert			CHAR(100);

	BEGIN
		-- Change string in date
		v_NewTime := '';
		v_Convert := '''' || p_Source || ''',''' || p_Format || '''';

		-- Change string in date
		v_NewDateTime := to_date(p_Source, '' || p_Format || '');

		CASE
			WHEN upper(p_TimeIndex) = 'MONTH' or upper(p_TimeIndex) = 'MONAT'		THEN
				-- handle last day of month e.g. 28 of Feb, 30, 31
				-- if add 1 month to 31 of Jan you will get 28 of Feb
				-- (last day of month because 31 of Jan is also last day of month)
				-- otherwise the day will be calculated correct.
				v_DateOut := add_months(v_NewDateTime, to_number(p_Offset));
				v_DayIn := to_number(to_char(v_newDateTime, 'DD'));
				v_DayOut := to_number(to_char(v_DateOut, 'DD'));
				IF v_DayIn > v_DayOut THEN
					v_NewTime := to_char(v_DateOut - (v_DayOut - v_DayIn), '' || p_Format || '');
				ELSE
					v_NewTime := to_char(add_months(v_NewDateTime, to_number(p_Offset)), '' || p_Format || '');
				END IF;
			WHEN upper(p_TimeIndex) = 'DAY' or upper(p_TimeIndex) = 'TAG'			THEN
				v_Offset := p_Offset;
				v_newTime := to_char(v_NewDateTime + v_Offset, '' || p_Format || '');
			WHEN upper(p_TimeIndex) = 'HOUR' or upper(p_TimeIndex) = 'STUNDE'		THEN
				v_Offset := p_Offset / 24;
				v_newTime := to_char(v_NewDateTime + v_Offset, '' || p_Format || '');
			WHEN upper(p_TimeIndex) = 'MINUTE' or upper(p_TimeIndex) = 'MINUTE'		THEN
				v_Offset := p_Offset / 1440;
				v_newTime := to_char(v_NewDateTime + v_Offset, '' || p_Format || '');
			WHEN upper(p_TimeIndex) = 'SECOND' or upper(p_TimeIndex) = 'SEKUNDE'	THEN
				v_Offset := p_Offset / 86400;
				v_newTime := to_char(v_NewDateTime + v_Offset, '' || p_Format || '');
			ELSE
				v_Offset := 0;
				v_newTime := to_char(v_NewDateTime + v_Offset, '' || p_Format || '');
		END CASE;
		RETURN v_NewTime;

	EXCEPTION
		WHEN OTHERS THEN
			v_NewTime := '';
		RETURN v_NewTime;
	END FUN_CALCULATE_TIME;
/
