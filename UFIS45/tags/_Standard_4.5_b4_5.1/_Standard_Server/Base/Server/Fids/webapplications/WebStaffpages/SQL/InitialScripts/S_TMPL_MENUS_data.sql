SET DEFINE OFF;
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Towings (En)', 10, 'select
urno,flno,rkey,regn,stoa,psta,ofbl,psta,psta,psta,onbl,pstd,stod,psta
from ceda.afttab
WHERE ((TIFA BETWEEN ''$TfVal1'' AND ''$TfVal2'') AND (FTYP =''T'')) ORDER BY FLNO,STOD', 6, 'Towings (En)', 
    NULL, 60, 15, 0, 1, 
    60, 1440, 'Towings (2nd Lang)');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Departures ', 2, 'SELECT   afttab.urno, flno, jfno, apttab.apsn des3, via3apttab.apsn via3, stod, ofbl, airb, etod, gtd1, wro1,
         pstd, act3, regn, REPLACE (fidtab.beme,
                  ''HH:mm'',
                  (case when trim(stod) <>'''' then
                     SUBSTR (get_localtime (stod, ''AUH''), 9, 2)
                  || '':''
                  || SUBSTR (get_localtime (stod, ''AUH''), 11, 2)
                  end )
                 ) remp
     FROM afttab, apttab, apttab via3apttab, fidtab
   WHERE afttab.des3 = apttab.apc3
     AND LTRIM (afttab.via3) = via3apttab.apc3(+)
     AND LTRIM (afttab.remp) = fidtab.code(+)
  AND (    (tifd BETWEEN ''$TfVal1'' AND ''$TfVal2'')
          AND (adid = ''D'' OR adid = ''B'')
          AND (flno <> '' '')
          AND (ftyp IN (''S'' ,''O'', ''X'', ''D'', ''R''))
        )
           ORDER BY stod, flno', 1, 'DEPARTURE', 
    'Dest.', 60, 100, 0, 1, 
    -1440, 1440, 'Departures ');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Arrivals ', 1, 'SELECT   afttab.urno, flno, jfno, apttab.apsn org3, via3apttab.apsn via3, stoa, afttab.land,
         onbl, etoa, blt1, psta, act3, regn,
         REPLACE (fidtab.beme,
                  ''HH:mm'',
                  (case when trim(stoa) <>'''' then
                     SUBSTR (get_localtime (stoa, ''AUH''), 9, 2)
                  || '':''
                  || SUBSTR (get_localtime (stoa, ''AUH''), 11, 2)
                  end )
                 ) remp
    FROM afttab, apttab, apttab via3apttab, fidtab
   WHERE afttab.des3 = apttab.apc3
     AND LTRIM (afttab.via3) = via3apttab.apc3(+)
     AND LTRIM (afttab.remp) = fidtab.code(+)
     AND (    (tifa BETWEEN ''$TfVal1''  AND ''$TfVal2'' )
          AND (adid = ''A'' OR adid = ''B'')
         )
     AND ftyp IN (''S'',''O'', ''X'', ''D'', ''R'')
      ORDER BY stoa, flno', 2, 'ARRIVAL', 
    'Orig.', 60, 40, 0, 1, 
    -1440, 1440, 'Arrivals ');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Arrival Baggage Conveyors ', 3, 'SELECT   afttab.urno, jfno,blt1, flno,  apttab.apsn org3, via3apttab.apsn via3, stoa, onbl, b1ba, b1ea, paxt,
         REPLACE(fidtab.beme, ''HH:mm'', substr(get_localtime(stoa,''AUH''),9,2)||'':'' ||substr(get_localtime(stoa,''AUH''),11,2)) remp FROM afttab, apttab, apttab via3apttab, fidtab WHERE afttab.des3 = apttab.apc3
     AND LTRIM (afttab.via3) = via3apttab.apc3(+)
     AND LTRIM (afttab.remp) = fidtab.code(+)
     AND
    (    (    (tifa BETWEEN get_utctime (-60, ''AUH'')
                             AND get_utctime (180, ''AUH'')
                   )
               AND (adid = ''A'')
              )
          AND (onbl <> '' '')
          AND (blt1 != '' '')
          AND (   ((b1ea > ''$TfVal1'') )
               OR ((b1ea = '' '') AND (onbl > ''$TfVal2'') )
              )
         ) ORDER BY blt1, onbl', 9, 'Arrival Baggage Conveyors', 
    NULL, 60, 50, 0, 1, 
    -5, 40, 'Arrival Baggage Conveyors ');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Check-in desks  ', 6, 'SELECT   /*+ ALL_ROWS */
         ccatab.urno, cictab.cnam ckic, ccatab.flno, flnu, ctyp, ckbs, ckes,
         ccatab.hopo, disp rema,'' '' des3,'' '' via3,'' '' stod,'' '' etod
    FROM cictab, ceda.ccatab
   WHERE cictab.cnam = ccatab.ckic
     AND (    ckbs < ''$TfVal1''
          AND ckes > ''$TfVal2''
          AND ckbs <> '' ''
          AND ckea = '' ''
          AND ckic <> '' ''
          AND ctyp = ''C''
         )
AND (cictab.cnam like ''%$DynamicCKIC%'') 

UNION 
SELECT   /*+ ALL_ROWS */
         ccatab.urno, ckic, afttab.flno, flnu, ctyp, ckbs, ckes,
         ccatab.hopo, disp rema,afttab.des3,afttab.via3,afttab.stod,afttab.etod
    FROM afttab, ceda.ccatab
   WHERE 
     ccatab.flnu = afttab.urno
     AND (    ckbs < ''$TfVal1''
          AND ckes > ''$TfVal2''
          AND ckbs <> '' ''
          AND ckea = '' ''
          AND ckic <> '' ''
          AND ctyp = '' ''
         ) ORDER BY  ckic, ckbs
', 3, 'Check-in desks ', 
    NULL, 60, 15, 0, 1, 
    0, 0, 'Check-in desks  ');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Gates ', 7, 'SELECT  /*+ ALL_ROWS */  gattab.gnam, afttab.urno, afttab.gtd1, afttab.wro1, afttab.flno,
         afttab.jfno, afttab.des3, afttab.via3, afttab.stod, afttab.etod,
         afttab.paxt, afttab.remp
    FROM gattab, afttab
   WHERE gattab.gnam = afttab.gtd1
     AND (    (afttab.tifd BETWEEN ''$TfVal1'' AND ''$TfVal2'')
          AND (afttab.adid = ''D'')
          AND afttab.flno <> '' ''
          AND afttab.gtd1 <> '' ''         
          AND ((afttab.remp <> ''GCL'') AND (afttab.remp <> ''DEP''))
         ) ORDER BY gattab.gnam', 4, 'Gates', 
    NULL, 60, 15, 0, 1, 
    -120, 120, 'Gates ');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Parking Stands (En)', 9, 'SELECT  /*+ ALL_ROWS */  afttab.urno, psttab.pnam, psta, regn, act3, flno, onbl, via3, via3,
         via3, via3
    FROM psttab, ceda.afttab
   WHERE psttab.pnam = afttab.psta(+)
     AND (    (tifa(+) BETWEEN ''$TfVal1'' AND ''$TfVal2'')
          AND (psta(+) <> '' '')
          AND ftyp(+) = ''O''
          AND (onbl(+) <> '' '')
         ) ORDER BY psttab.pnam,flno', 5, 'Parking Stands', 
    NULL, 60, 15, 0, 1, 
    -4320, 300, 'Parking Stands (2nd Lang)');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Airport Category', 11, 'select obj,stat,tdat from tmstab order by obj', 7, NULL, 
    NULL, NULL, NULL, NULL, NULL, 
    NULL, NULL, 'Airport Category');
Insert into SPA.S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2, MENUNAME_TR)
 Values
   ('Telephone numbers', 12, 'Select '''' from dual', 8, 'Telephone numbers', 
    NULL, NULL, NULL, NULL, NULL, 
    NULL, NULL, 'Telephone numbers');
COMMIT;
