<?php

include 'db_if_fblb.php';
include 'cedaphpif_qput.php';

class ArrBlt {

    function getDeviceInfo($ipaddr) {
        Global $oraconn, $db, $DBExecTime, $tdi;
        $qry_dev = "select dffd,dfco,dter  from devtab where dadr='" . $ipaddr . "'";
        $rs_dev = GetDBDataDirect(TRACE, $qry_dev);
        $dev = array($rs_dev->fields);
//print "ipaddr >".$ipaddr."  Belt >".trim($dev[0][1])."<br>";
        return $dev;
    }

    function getTimeConf() {
        Global $oraconn, $db, $DBExecTime, $tdi;
        $hrFac = 1 / 24;
        $tpCol = array(20 => 30, 21 => $hrFac, 22 => $hrFac, 23 => $hrFac, 24 => $hrFac, 25 => $hrFac);
        $qry_tp = "select TIPC, TIPU, TIPV from tiptab where tipc in ('20','21','22','23','24','25') order by tipc";
        $rs_tp = GetDBDataDirect(TRACE, $qry_tp);

        while (!$rs_tp->EOF) {
            for ($r = 1; $r <= $rs_tp->RecordCount(); $r++) {
                switch (trim($rs_tp->fields[0])) {
                    case 20:
                        if (trim($rs_tp->fields[1]) == 'H') {
                            $tpCol[20] = trim($rs_tp->fields[2]) * 60 * 60;
                        } elseif (trim($rs_tp->fields[1]) == 'M') {
                            $tpCol[20] = trim($rs_tp->fields[2]) * 60;
                        } elseif (trim($rs_tp->fields[1]) == 'D') {
                            $tpCol[20] = 30;
                        } else {
                            $tpCol[20] = trim($rs_tp->fields[2]);
                        }
//print "refTime >".$tpCol[20]." <br> ";
                        break;

                    case 21:
                        if (trim($rs_tp->fields[1]) == 'H') {
                            $tpCol[21] = trim($rs_tp->fields[2]) / 24;
                        } elseif (trim($rs_tp->fields[1]) == 'M') {
                            $tpCol[21] = trim($rs_tp->fields[2]) / (60 * 24);
                        } elseif (trim($rs_tp->fields[1]) == 'S') {
                            $tpCol[21] = trim($rs_tp->fields[2]) / (60 * 60 * 24);
                        } else {
                            $tpCol[21] = 1 / 24;
                        }
//print "tp_future >".$tpCol[21]." <br> ";
                        break;

                    case 22:
                        if (trim($rs_tp->fields[1]) == 'H') {
                            $tpCol[22] = trim($rs_tp->fields[2]) / 24;
                        } elseif (trim($rs_tp->fields[1]) == 'M') {
                            $tpCol[22] = trim($rs_tp->fields[2]) / (60 * 24);
                        } elseif (trim($rs_tp->fields[1]) == 'S') {
                            $tpCol[22] = trim($rs_tp->fields[2]) / (60 * 60 * 24);
                        } else {
                            $tpCol[22] = 1 / 24;
                        }
//print "tp_lbg >".$tpCol[22]." <br> ";
                        break;

                    case 23:
                        if (trim($rs_tp->fields[1]) == 'H') {
                            $tpCol[23] = trim($rs_tp->fields[2]) / 24;
                        } elseif (trim($rs_tp->fields[1]) == 'M') {
                            $tpCol[23] = trim($rs_tp->fields[2]) / (60 * 24);
                        } elseif (trim($rs_tp->fields[1]) == 'S') {
                            $tpCol[23] = trim($rs_tp->fields[2]) / (60 * 60 * 24);
                        } else {
                            $tpCol[23] = 1 / 24;
                        }
//print "tp_fbg >".$tpCol[23]." <br> ";
                        break;

                    case 24:
                        if (trim($rs_tp->fields[1]) == 'H') {
                            $tpCol[24] = trim($rs_tp->fields[2]) / 24;
                        } elseif (trim($rs_tp->fields[1]) == 'M') {
                            $tpCol[24] = trim($rs_tp->fields[2]) / (60 * 24);
                        } elseif (trim($rs_tp->fields[1]) == 'S') {
                            $tpCol[24] = trim($rs_tp->fields[2]) / (60 * 60 * 24);
                        } else {
                            $tpCol[24] = 1 / 24;
                        }
//print "tp_lnd >".$tpCol[24]." <br> ";
                        break;

                    case 25:
                        if (trim($rs_tp->fields[1]) == 'H') {
                            $tpCol[25] = trim($rs_tp->fields[2]) / 24;
                        } elseif (trim($rs_tp->fields[1]) == 'M') {
                            $tpCol[25] = trim($rs_tp->fields[2]) / (60 * 24);
                        } elseif (trim($rs_tp->fields[1]) == 'S') {
                            $tpCol[25] = trim($rs_tp->fields[2]) / (60 * 60 * 24);
                        } else {
                            $tpCol[25] = 1 / 24;
                        }
//print "tp_exp >".$tpCol[25]." <br> ";
                        break;
                }
                $rs_tp->MoveNext();
            }
        }
        $rs_tp->close();
        return $tpCol;
    }

    function getFlights($tp_future, $tp_lbg, $tp_fbg, $tp_lnd, $tp_exp, $devid) {
        Global $oraconn, $db, $DBExecTime, $tdi, $beginBelt1, $endBelt1, $beginBelt2, $endBelt2, $ActualSamewithFB, $ActualBeginBelt1, $ActualEndBelt1, $ActualBeginBelt2, $ActualEndBelt2;
        Global $ActualBeltOnly;

//putenv("TZ=EET");
        $utcDate = gmdate("YmdHis");
        $UtcNow = mktime();
        $LocNow = mktime();
//echo "UTC:". gmdate("YmdHis",$UtcNow);
//echo "LOC:". date("YmdHis",$LocNow);
        $tl_timeframe_FROM = gmdate("YmdHis", ($UtcNow - ($tp_exp * 86400)));
        $tl_timeframe_TO = gmdate("YmdHis", ($UtcNow + ($tp_future * 86400)));

        $timeframe = gmdate("YmdHis", ($UtcNow));

        /*
         * 
          print_r("tp_future:[".$tp_future."]</br>");
          print_r("tp_lbg:[".$tp_lbg."]</br>");
          print_r("tp_fbg:[".$tp_fbg."]</br>");
          print_r("tp_lnd:[".$tp_lnd."]</br>");
          print_r("tp_exp:[".$tp_exp."]</br>");
          print_r("devid:[".$devid."]</br>");
          print_r("tl_timeframe_FROM:[".$tl_timeframe_FROM."]</br>");
          print_r("tl_timeframe_TO:  [".$tl_timeframe_TO."]</br>");
         */

        // JWE 20050921: Below statement is much to slow because it converts each an every
        //							stoa value using the oracle to_date()-function.
        //							Now the timeframe is calculated inside PHP prior to the select.
        //							As well the field names of the db-fields to be used needed to be changed.
        //							B1BA --> BAS1; B1EA --> BAE1.
        //							B2BA --> BAS2; B2EA --> BAE2.
        //							Instead of STOA now TIFA is used for the SQL-query to rely on the best quality time.

        /*         * ****************************************************************************************************
          $qry_fl="select * from (select urno, flno, blt1, blt2, b1ea, b1ba, b2ea, b2ba, onbl, land, etai, stoa from afttab where ((( blt1='".$devid."' and ((b1ea=' ' or to_date(b1ea,'yyyymmddhh24miss')+".$tp_lbg." >= to_date('".$utcDate."','yyyymmddhh24miss') ) and (b1ba=' ' or to_date(b1ba,'yyyymmddhh24miss')+".$tp_fbg." >= to_date('".$utcDate."','yyyymmddhh24miss') ) )) or ( blt2='".$devid."' and ((b2ba=' ' or to_date(b2ba,'yyyymmddhh24miss')+".$tp_fbg." >= to_date('".$utcDate."','yyyymmddhh24miss') ) and (b2ea=' ' or to_date(b2ea,'yyyymmddhh24miss')+".$tp_lbg." >= to_date('".$utcDate."','yyyymmddhh24miss') ) ))) and (onbl=' ' or (to_date(onbl,'yyyymmddhh24miss')+".$tp_lnd." >= to_date('".$utcDate."','yyyymmddhh24miss') )) and (land=' ' or (to_date(land,'yyyymmddhh24miss')+".$tp_lnd." >= to_date('".$utcDate."','yyyymmddhh24miss') )) and (etai=' ' or (to_date(etai,'yyyymmddhh24miss')+".$tp_exp." >= to_date('".$utcDate."','yyyymmddhh24miss') )) and (to_date(stoa,'yyyymmddhh24miss') between to_date('".$utcDate."','yyyymmddhh24miss')-".$tp_exp." and to_date('".$utcDate."','yyyymmddhh24miss')+".$tp_future.")) and (blt1='".$devid."' or blt2 ='".$devid."') and stoa!=' ' and adid = 'A' order by stoa) where rownum < 6 ";
         * ********************************************************************************************************* */
        //Fields
        // Special for ATH BAS1,BAE1
        //In ATH the Actual Times for the Belt are different than the FB LB times
        $extrafields = "";
        if ($ActualSamewithFB == false) {
            $extrafields = "," . $ActualBeginBelt1 . "," . $ActualEndBelt1 . "," . $ActualBeginBelt2 . "," . $ActualEndBelt2;
        }

        if ($ActualBeltOnly == true ) {
            $qry_fl = "select * from (select urno, flno, blt1, blt2, $endBelt1, $beginBelt1, $endBelt2, $beginBelt2, onbl, land, etai, stoa " . $extrafields . " from afttab ";
            $qry_fl = $qry_fl . " WHERE (   (blt1 = '" . $devid . "' AND ".$ActualBeginBelt1." != ' ' AND ( ".$ActualEndBelt1." > '".$timeframe."' or ".$ActualEndBelt1." = ' '))";
            $qry_fl = $qry_fl . "         OR (blt2 = '" . $devid . "' AND ".$ActualBeginBelt2." != ' ' AND ( ".$ActualEndBelt2." > '".$timeframe."' or ".$ActualEndBelt2." = ' ')  ) ) ";
            $qry_fl = $qry_fl . " AND tifa BETWEEN '$tl_timeframe_FROM' and '$tl_timeframe_TO'";
            $qry_fl = $qry_fl . " AND tifa != ' ' and stoa != ' ' and adid = 'A' and  ftyp = 'O' order by tifa ) where rownum < 6";
        } else {
            $qry_fl = "select * from (select urno, flno, blt1, blt2, $endBelt1, $beginBelt1, $endBelt2, $beginBelt2, onbl, land, etai, stoa " . $extrafields . " from afttab ";
            $qry_fl = $qry_fl . " where ((( blt1='" . $devid . "' and (( ($endBelt1=' ' or $endBelt1 is null) or to_date(trim($endBelt1),'yyyymmddhh24miss')+" . $tp_lbg . " >= to_date('" . $utcDate . "','yyyymmddhh24miss') ) ";
            $qry_fl = $qry_fl . " and (($beginBelt1=' ' or $beginBelt1 is null) or to_date(trim($beginBelt1),'yyyymmddhh24miss')+" . $tp_fbg . " >= to_date('" . $utcDate . "','yyyymmddhh24miss') ) )) ";
            $qry_fl = $qry_fl . " or ( blt2='" . $devid . "' and ((($beginBelt2=' ' or $beginBelt2 is null) or to_date(trim($beginBelt2),'yyyymmddhh24miss')+" . $tp_fbg . " >= to_date('" . $utcDate . "','yyyymmddhh24miss') ) ";
            $qry_fl = $qry_fl . " and (($endBelt2=' ' or $endBelt2 is null) or to_date(trim($endBelt2),'yyyymmddhh24miss')+" . $tp_lbg . " >= to_date('" . $utcDate . "','yyyymmddhh24miss') ) )) ";
            $qry_fl = $qry_fl . " ) ";
            $qry_fl = $qry_fl . " and (onbl=' ' or (to_date(trim(onbl),'yyyymmddhh24miss')+" . $tp_lnd . " >= to_date('" . $utcDate . "','yyyymmddhh24miss') )) ";
            $qry_fl = $qry_fl . " and (land=' ' or (to_date(trim(land),'yyyymmddhh24miss')+" . $tp_lnd . " >= to_date('" . $utcDate . "','yyyymmddhh24miss') )) ";
            $qry_fl = $qry_fl . " and ((etai=' ' or etai is null) or (to_date(trim(etai),'yyyymmddhh24miss')+" . $tp_exp . " >= to_date('" . $utcDate . "','yyyymmddhh24miss') )) ";
            $qry_fl = $qry_fl . " and tifa between '$tl_timeframe_FROM' and '$tl_timeframe_TO') and (blt1='" . $devid . "' or blt2 ='" . $devid . "') ";
            $qry_fl = $qry_fl . " and tifa!=' ' and stoa != ' '  and adid = 'A' and ftyp = 'O' order by tifa) where rownum < 6 ";
        }
        //echo $qry_fl;
        $rs_fl = GetDBDataDirect(TRACE, $qry_fl);
        return $rs_fl;
    }

    function updateFlight($usrp) {

        Global $HOPO, $beginBelt1, $endBelt1, $beginBelt2, $endBelt2;
        $Data = '';
        $Fields = '';
        $uvar = explode("#", $usrp);
        $Dest = "flight";
        $Prio = "3";
        $Command = "UFR";
        $Table = "AFTTAB";
        $Selection = "WHERE URNO = " . $uvar[0];
        $Timeout = "10";
        $SubCmd = "";
        $curTime = gmdate("YmdHis");
        if ($uvar[1] == 'F') {
            if ($uvar[2] == '1') {
                $Fields = $beginBelt1;
            } else {
                $Fields = $beginBelt2;
            }
            $Data = $curTime;
        } elseif ($uvar[1] == 'L') {
            if ($uvar[2] == '1') {
                if ($uvar[3] == 'ONB') {
                    $Fields = "$endBelt1,$beginBelt1";
                    $Data = $curTime . "," . $curTime;
                } else {
                    $Fields = $endBelt1;
                    $Data = $curTime;
                }
            } else {
                if ($uvar[3] == 'ONB') {
                    $Fields = "$endBelt2,$beginBelt2";
                    $Data = $curTime . "," . $curTime;
                } else {
                    $Fields = $endBelt2;
                    $Data = $curTime;
                }
            }
        } elseif ($uvar[1] == 'C') {
            if ($uvar[2] == '1') {
                if ($uvar[3] == 'LBG') {
                    $Fields = $endBelt1;
                } else {
                    $Fields = $beginBelt1;
                }
            } else {
                if ($uvar[3] == 'LBG') {
                    $Fields = $endBelt2;
                } else {
                    $Fields = $beginBelt2;
                }
            }
            $Data = ' ';
        }
        $tmp_tws = "";
        $tmp_twe = $HOPO . ",TAB,FBLB";
        $result = CallQput(TRACE, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $tmp_tws, $tmp_twe, $SubCmd);
//echo "updateFlight-Result:[".$result."]<br>";
        return $result;
    }

    function updateTime($ipaddr, $devid) {
        Global $HOPO;
        $Data = "" . date("YmdHis") . ",U";
        $Fields = "\"DLCT,STAT\"";
        $Dest = "1200";
        $Prio = "3";
        $Command = "URT";
        $Table = "DEVTAB";
        $Selection = "WHERE dadr = \'" . $ipaddr . " \'";
        $Timeout = "-1";
        $SubCmd = "";
        $tmp_tws = "BELT" . $devid;
        $tmp_twe = $HOPO . ",TAB,FBLB";
        $result = CallQput(TRACE, $Dest, $Prio, $Command, $Table, $Fields, $Data, $Selection, $Timeout, $tmp_tws, $tmp_twe, $SubCmd);
//echo "updateTime-Result:[".$result."]<br>";
        return $result;
    }

}

?>
