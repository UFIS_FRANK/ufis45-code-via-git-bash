<?php

// 20060621 JIM: PRF 8203: correct sorting via "ORDER BY" instead
//                         of sortColumn(). Note: To avoid sorting
//                         by tooltips ("New Dehli" instead of "DEL")
//                         before the Tooltip a HTML Comment has to be
//                         added ('<!--'||APC3||'--><font tooltip=...')




include("include/manager.php");
include("$ADODB_RootPath/list.php");
//include("include/template.php");
//include("include/class.overlib/class.overlib.php3");
include("include/class.LoginInit.php");


/* Login Check */
/* Set Login Instance */
$login = new LoginInit;
//$tpl = new Template($tmpl_path);

$smarty = new Smarty;

$smarty->caching = 0;
$smarty->compile_check = true;
$smarty->debugging = false;


// Over lib
//$ol = new Overlib;
//if (isset($maskbgcolor)) {
//    $ol->ol_backcolor = $maskbgcolor;
//}
//$overlib=$ol->init();
//20060710 JIM: PRF 8204: excel export: set default
if (!isset($Export)) {
    $Export = "No";
}


//20070221 GFO: WAW Project : Added $LangTR for the Second Language Support
/*
  if (!isset($Lang)) {
  if (!isset($Session["Lang"])) {
  $Session["Lang"] = "";
  }
  } else {
  if ($Lang != "ENG" && len($Lang)>0) {
  $Session["Lang"] =  "_TR";
  }
  if ($Lang == "ENG") {
  $Session["Lang"] = "";
  }
  }

  $LangTR = $Session["Lang"];
 */


//20070221 GFO: WAW Project : Added $LangTR for the Second Language Support^M
if (!isset($Lang)) {
//                      $LangTR = "";
    if (!(tep_session_is_registered("Lang_TR"))) {
        set_session("Lang_TR", "");
        //$_Session["Lang_TR"]="";
    }
} else {
    if ($Lang != "ENG" && len($Lang) > 0) {
        set_session("Lang_TR", "_TR");
        //$_Session["Lang_TR"]="_TR";
    } else {
        set_session("Lang_TR", "");
        //$_Session["Lang_TR"]="";
    }
}

$LangTR = get_session("Lang_TR");

$apfn = "apfn";
$beme = "beme";

if ($LangTR == "_TR") {
    $apfn = "apsn";
    $beme = "bemd";
}


if ((tep_session_is_registered("userid"))) {
    // The user has already been Logged
//20060710 JIM: PRF 8204: excel export: added $Export
//		$Query=$login->Tmpl($Session["userid"]);
    $Query = $login->Tmpl(get_session("userid"), $Export);
    $smarty->assign("DateTime", strftime("created at %d.%m.%Y, %H:%M", time()));
} else {

    //20060710 JIM: PRF 8204: excel export: added $Export
    //$Query=	$login->Login($UserName,$Password);
    $Query = $login->Login($UserName, $Password, $Export);
}

//$debug = false;
/* Start Debug  if requested */
if ($debug == "true") {
    $tmpdebug = "?debug=true";
    $db->debug = $debug;
} else {
    $debug = "false";
}
//$debug=true;
//$db->debug=$debug;





/* Init the time-diff. variables */
FindTDI();

// Expires the Page
$Cexpires = date("D, d M Y H:i:s ", AddDiff("S", $Refresh, time())) . " GMT";

/* setting a default of Main and codeshare search if not set using the search mask */
if (empty($SearchType))
    $SearchType = "MAC";

$SearchFlno = "";
// Building SearchFlno variable if main-carrier shall be searched
if ($SearchType == "MC" || $SearchType == "MAC") {
    if (len($airline) > 0) {
        $airline = strtoupper($airline);
        $SearchFlno = $airline;
    }
    if (len($flightnumber) > 0) {
        if (len($airline) > 2) {
            $SearchFlno = $airline . "%" . $flightnumber;
        }
        if (len($airline) <= 2) {
            $SearchFlno = $airline . " " . "%" . $flightnumber;
        }
        if (len($airline) == 0) {
            $SearchFlno = $flightnumber;
        }
    }
} else {
    // on empty SearchType the user requests everything
    if (!empty($SearchType))
        $SearchFlno = "<not used>";
}

// Building SearchJfno variable if codeshares shall be searched
$SearchJfno = "";
if ($SearchType == "CS" || $SearchType == "MAC") {
    if (len($airline) > 0) {
        $airline = strtoupper($airline);
        $SearchJfno = $airline;
    }
    if (len($flightnumber) > 0) {
        if (len($airline) > 2) {
            $SearchJfno = $airline . "%" . $flightnumber;
        }
        if (len($airline) <= 2) {
            $SearchJfno = $airline . " " . "%" . $flightnumber;
        }
        if (len($airline) == 0) {
            $SearchJfno = $flightnumber;
        }
    }
} else {
    // on empty SearchType the user requests everything
    if (!empty($SearchType))
        $SearchJfno = "<not used>";
}
// ##################################################################
// ### starting to check if times have been set using the search mask
// ##################################################################


        //################################
        //# FROM DATE TIMEFRAME SETTING ##
        //################################
        // ### here we check for fromdateMonth,etc. ###
        // add or substract your value here for the default timeframe start

        if (checkdate ($TfVal1Month, $TfVal1Day, $TfVal1Year)) {
           $fromdate=mktime($TfVal1Hour ,$TfVal1Min, 0,$TfVal1Month ,$TfVal1Day, $TfVal1Year);
        }
        //##############################
        //# TO DATE TIMEFRAME SETTING ##
        //##############################
        // ### depending on the search-template either todateplus or todateMonth, etc. will have been set ###
        // add or substract your value here for the default timeframe end
        if ($todateplus && isset($fromdate))
        {
           $TfVal1=$fromdate ;
           $TfVal2=AddDiff("H",$todateplus,$fromdate);

        }


        //echo "Fromdate:($fromdateYear$fromdateMonth$fromdateDay$fromdateHour$fromdateMin)<br>";



// 20060307 JIM: PRF 8201: BEGIN Step 1 Timeframe settings
if (! $TfVal1_curr) {
    $TfVal1_curr=$TfVal1 / 60 ;
}
if (! $TfVal2_curr) {
    $TfVal2_curr=$TfVal2 / 60 ;
}
if ($TfVal1_curr) {
    $TfVal1=$TfVal1_curr * 60;
}
if ($TfVal2_curr) {
    $TfVal2=$TfVal2_curr * 60;
}
// 20060307 JIM: PRF 8201: END Step 1 Timeframe settings

if (!isset($fromdate)) {
    $TfVal1=time(0) + ($TfVal1*60);
    $TfVal2=time(0) + ($TfVal2*60);
}


$TfVal1Month=date("m",$TfVal1);
$TfVal1Year=date("Y",$TfVal1);
$TfVal1Day=date("d",$TfVal1);
$TfVal1Hour=date("H",$TfVal1);
$TfVal1Min=date("i",$TfVal1);

//	$TfVal1=date("YmdHis",$TfVal1);

// 20070314 GFO: Solving the UTC Problem for the select statement
$TfVal1=gmdate("YmdHis",$TfVal1);


$TfVal2Month=date("m",$TfVal2);
$TfVal2Year=date("Y",$TfVal2);
$TfVal2Day=date("d",$TfVal2);
$TfVal2Hour=date("H",$TfVal2);
$TfVal2Min=date("i",$TfVal2);

//	$TfVal2=date("YmdHis",$TfVal2);
// 20070314 GFO: Solving the UTC Problem for the select statement
$TfVal2=gmdate("YmdHis",$TfVal2);

// Setting search mask defaults on todays date at 00:00
$SearchstartdateMonth=date("m",time(0));
$SearchstartdateYear=date("Y",time(0));
$SearchstartdateDay=date("d",time(0));
$SearchstartdateHour="00";
$SearchstartdateMin="00";

//echo "Search:(".$SEARCHUSED.")<br>";
//echo "TfVal1:(".$TfVal1.")<br>";
//echo "TfVal2:(".$TfVal2.")<br>";
// Start Header
// $start = $tpl->utime();

/* We Must reverse the values of the array in order to pass  the results
  to the parse functions in the correct order */

//$tmpl_array=array_reverse ($tmpl_array);
//
// values for HTTP header
if ($Export == "Export") {
    // GFO 20070313: Remove the Images from the datavalue
    $PageTitle = eregi_replace('<[^>]*img*\"?[^>]*>', "", $PageTitle);
}

// values for HTTP header


$smarty->assign("PageTitle", $PageTitle);
$smarty->assign("STYLESHEET", $StyleSheet);
$smarty->assign("Refresh", $RefreshRate);
$smarty->assign("RefreshRate", $RefreshRate);
$smarty->assign("Cexpires", $Cexpires);

// values for div search mask
$smarty->assign("SearchUsed", $SearchUsed);
$smarty->assign("SearchstartdateMonth", $SearchstartdateMonth);
$smarty->assign("SearchstartdateYear", $SearchstartdateYear);
$smarty->assign("SearchstartdateDay", $SearchstartdateDay);
$smarty->assign("SearchstartdateHour", $SearchstartdateHour);
$smarty->assign("SearchstartdateMin", $SearchstartdateMin);
$smarty->assign("SearchenddateMonth", $SearchenddateMonth);
$smarty->assign("SearchenddateYear", $SearchenddateYear);
$smarty->assign("SearchenddateDay", $SearchenddateDay);
$smarty->assign("SearchenddateHour", $SearchenddateHour);
$smarty->assign("SearchenddateMin", $SearchenddateMin);
// values for div search mask
$smarty->assign("overlib", $overlib);
$smarty->assign("alfn", $alfn);
$smarty->assign("alfnspaces", $alfnspaces);
$smarty->assign("apfn", $apfn);
$smarty->assign("apfnspaces", $apfnspaces);
//$smarty->assign("FromDateHeader",$FromDateHeader );
//$smarty->assign("ToDateHeader",$ToDateHeader );
$smarty->assign("dateerror", $dateerror);
$smarty->assign("type", $type);
$smarty->assign("TMPL_MENUID", $TMPL_MENUID);
$smarty->assign("TfVal1", $TfVal1);
// 20060307 JIM: PRF 8201: Step 2 Timeframe settings
$smarty->assign("TfVal1_curr", $TfVal1_curr);
$smarty->assign("TfVal1Month", $TfVal1Month);
$smarty->assign("TfVal1Year", $TfVal1Year);
$smarty->assign("TfVal1Day", $TfVal1Day);
$smarty->assign("TfVal1Hour", $TfVal1Hour);
$smarty->assign("TfVal1Min", $TfVal1Min);
$smarty->assign("TfVal2", $TfVal2);
// 20060307 JIM: PRF 8201: Step 3 Timeframe settings
$smarty->assign("TfVal2_curr", $TfVal2_curr);
$smarty->assign("TfVal2Month", $TfVal2Month);
$smarty->assign("TfVal2Year", $TfVal2Year);
$smarty->assign("TfVal2Day", $TfVal2Day);
$smarty->assign("TfVal2Hour", $TfVal2Hour);
$smarty->assign("TfVal2Min", $TfVal2Min);
$smarty->assign("OrgDes", $OrgDes);

$airline = strtoupper($airline);
$smarty->assign("airline", $airline);
//$smarty->assign("flno",$flno );
$flightnumber = strtoupper($flightnumber);
$smarty->assign("flightnumber", $flightnumber);
$smarty->assign("SearchFlno", $SearchFlno);
$smarty->assign("SearchJfno", $SearchJfno);
$city = strtoupper($city);
$smarty->assign("city", $city);
$smarty->assign("PHP_SELF", $PHP_SELF);
$smarty->assign("debug", $debug);
// 20060621 JIM: PRF 8203: Step No 1: correct sorting
// 20080312 VIC:		 : Commented, this is causing two hidden variable for order by and creating confusion for the dbinfo.php.
//$smarty->assign("OrderBy",$OrderBy);
$smarty->assign("DynamicSearch", "");



// End Header
//20100210 GFo: Change in order not to display the first Menu on the time




if (strlen($TMPL_MENUID) > 0 || $DisplayFirstMenu) {
    $smarty->assign("SearchButton", "<button class=\"ui-button\" id=\"openSearch\">Search </button>");

// We assign the values to the TDplus Select
    $i = -36;
    while ($i <= 36) {
        $tmps = "";
        // if ($i == $todateplus) {
        // default selected entry of the std_search-window
        if ($i == 24) {
            $tmps = " selected";
        }
        if ($i < 0)
            $smarty->assign("todateplus", "<option value=\"$i\" $tmps>-$i");
        else
            $smarty->assign("todateplus", "<option value=\"$i\" $tmps>+$i");
        //$tpl->parse("TDplus", true);
        $i++;
    }

// 20060621 JIM: PRF 8203: BEGIN Step 2: correct sorting
    if ($NewOrderBy) {

        if ($OrderBy) {
            if ($OrderBy == $NewOrderBy) {
                $OrderBy = $OrderBy . "_desc";
            } else {
                $OrderBy = $NewOrderBy;
            }
        } else {
            $OrderBy = $NewOrderBy;
        }

        //20060813 VIC: A new parameter PageOrderBy is created for keeping the secondary sort column
        //when user scrolls from page to page. If request is from any table header column URL,
        //$PageOrderBy will not be set, if so, we can assign a new value of $OrderBy or the
        //secondary sort column to it
        //echo "A PageOrderBy = ".$PageOrderBy."<br>";
        if (!isset($PageOrderBy) && $PageOrderBy <> $OrderBy) {
            $PageOrderBy = $OrderBy;
        }
    }


//20060813 VIC: if $PageOrderBy isset or sent from the web form using the '>' button, the hidden value of PageOrderBy set previously will still be used.
//echo "B PageOrderBy = ".$PageOrderBy."<br>";
    $smarty->assign("PageOrderBy", $PageOrderBy);


// 20060621 JIM: PRF 8203: END Step 2: correct sorting
    $FldArray = $login->FldDSrows($FldArray);
    if ($debug == "true") {
        echo "inf1.php: QUERY:$Query<br>";
        echo "<br>";
        echo "inf1.php: EQUERY:$ExtraQuery<br>";
        echo "inf1.php: PHP_SELF:$PHP_SELF<br>";
        echo "inf1.php: OrderBy:$OrderBy<br>";
        echo "inf1.php: TfVal1: $TfVal1<br>";
        echo "inf1.php: TfVal2: $TfVal2<br>";
    }

// Start Results
//if ($debug == "true" )
//{
//	echo "inf1.php: QUERY:$Query<br>";
//	echo "<br>";
//	echo "inf1.php: EQUERY:$ExtraQuery<br>";
//}
// 20060621 JIM: PRF 8203: moved 'strupper' down to avoid uppercase PHP fieldnames
//$Query=strtoupper($Query);

    $Query = ereg_replace("\"", "'", $Query);
// 20080709 GFO: replace CR LF  with one space
    $Query = preg_replace("/\r\n|\n|\r/", " ", $Query, -1);

// GFO 20070313: the Query may contain lower letter , search for the ORDER BY in UPPER mode
    $pos = strpos(strtoupper($Query), " ORDER BY");

// 20060621 JIM: PRF 8203: BEGIN Step 3: correct sorting
    $tmpOrder = "";
    if ($OrderBy && $OrderBy != "") {
        //20060813 VIC: if $PageOrderBy isset or sent from the web form using the '>' button, we can concatenate to $tmpOrder the value of PageOrderBy as a secondary sort item.
        if (!isset($PageOrderBy) && $PageOrderBy <> $OrderBy)
            $tmpOrder = $OrderBy . "," . $PageOrderBy . ",";
        //$a="1";
        else
            $tmpOrder = $OrderBy . ",";

        if (($ppp = strpos($OrderBy, "_desc")) > 0) {
            $tmpOrder = substr($OrderBy, 0, $ppp) . " desc,";
        }
    }
// 20060621 JIM: PRF 8203: END Step 3: correct sorting
//		$Query=substr ($Query,0,$pos).$ExtraQuery.substr ($Query,$pos, len($Query));
// 20100210: GFO adding the AirlineFilter Per User
    $af = get_session("airlinefilter");
    $AirlineFilter = "";
    if (strlen($af) > 0) {
        //Check if in the query we have the AFTTAB or the APTTAB
        if (strpos(strtoupper($Query), "ALTTAB") > 0) {
            $af = " and (ALTTAB.ALC3='$af' or ALTTAB.ALC2='$af') ";
        }
        if (strpos(strtoupper($Query), "AFTTAB") > 0) {
            $af = " and (AFTTAB.ALC3='$af' or AFTTAB.ALC2='$af') ";
        }
        $AirlineFilter = $af;
    }
    //echo $AirlineFilter;

    if ($pos > 0) {
// 20060621 JIM: PRF 8203: Step 4: correct sorting:

        $Query = substr($Query, 0, $pos) . $ExtraQuery . substr($Query, $pos, 10) . $tmpOrder . substr
                        ($Query, $pos + 10, len($Query));
    } else {
        $Query.=$ExtraQuery;
        if ($tmpOrder != "") {
// 20060621 JIM: PRF 8203: Step 5: correct sorting:
            $Query.=$ExtraQuery . " ORDER BY " . substr($tmpOrder, 0, len($tmpOrder) - 1);
        }
    }

//		echo "inf1.php: QUERY:$Query<br>";
//		echo "<br>";
//	    echo $Query;

    $smarty->assign("TableHeader", $login->FldHeader($FldArray, $Export));

    if (len($Query) > 0) {
        eval("\$Query = \"$Query\";");
        // 20060621 JIM: PRF 8203: moved 'strupper' to here to avoid uppercase PHP fieldnames
        // 20060727 JIM: PRF 8207: use 'strupper' only behind FROM to avoid uppercase JPG file names
        $Query = ereg_replace(" from ", " FROM ", $Query);
        $Wpos = strpos($Query, " FROM ");
        // 20070314 GFO:  strupper was causing wrong slect statements
        $Query = substr($Query, 0, $Wpos) . substr($Query, $Wpos);

        $str_value = "{ \n \"aaData\": \n [ \n";
        if (len($TMPL_MENUID) > 0) {
            $tmp_arr = array();

            $tmp_arr = $login->QFld($Query, stristr($Query, " FROM "), $Export);

            //Rows
            $srow = 1;
            for ($i = 0; $i < count($tmp_arr); $i++) {
                //Rows Columns
                $xrow = 1;
                $str_value.= "{\n";
                for ($x = 0; $x < count($tmp_arr[$i]); $x++) {
                    //Field , Value pair
                    //$str_value.= '"DT_RowId": "' . $column['value'] . '"';
                    while ($column = each($tmp_arr[$i][$x])) {
                        //print_r($column);
                        //echo $column['key'] . "=" . $column['value'] . "<br>";\
                        $str_value.= '"' .$column['key'].'": "' . $column['value'] . '"';
                        if ($xrow < count($tmp_arr[$i])) {
                            $str_value.= ",";
                        }
                        $str_value.= "\n";
                        $xrow++;
                    }
                }
                $str_value.= "}";
                if ($srow < count($tmp_arr)) {
                    $str_value.= ",";
                }
                $str_value.= "\n";
                $srow++;
                // echo "<br><br>";
            }
        }

        $str_value.="] \n }";

        echo $str_value;

        // Show the Results
//		$login->QFld($Query,$WhereP);
//20060710 JIM: PRF 8204: excel export: added $Export
//		$login->QFld($Query,stristr($Query," FROM "));
//        if (len($TMPL_MENUID) > 0 ) {
//            print_r($login->QFld($Query,stristr($Query," FROM "),$Export));
//        }
        // $tpl->parse("NoRows",true);
    }
// End  Results
}


// 20060727 JIM: PRF 8202: Begin: set focus to first search field of Dynamic Search Box
//$XXX="";
//if (strlen($FirstSearchField)>0) {
//    $XXX="<script language=\"JavaScript\"><!--
//function SetMyFocus() {
//      document.action_form.".$FirstSearchField.".focus();
//}
//-->
//</script>
//";
//}
//$smarty->assign("SetFocus", $XXX);
// 20060727 JIM: PRF 8202: End: set focus to first search field Dynamic Search Box
//$smarty->display($TemplateName);
// Start Footer
//$tpl->pparse($menuname,false);
// End Footer
exit;
?>
