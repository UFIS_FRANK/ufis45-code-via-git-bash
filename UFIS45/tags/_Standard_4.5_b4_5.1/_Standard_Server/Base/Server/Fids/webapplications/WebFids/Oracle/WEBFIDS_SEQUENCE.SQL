-- WEBFIDS
-- Create sequence for WEBFIDS_JOBS
-- run as user ceda
create sequence WEBFIDS_SEQ
minvalue 1
maxvalue 999999
start with 1
increment by 1
nocache
cycle;
