CREATE OR REPLACE VIEW "CEDA"."FDVTAB" ("ADID","AIRB","APC3",
    "AURN","BLT1","BLT2","CDAT","CKIF","CKIT","DACO","DNAT","DSEQ",
    "ETOF","FLNO","FTYP","GTD1","GTD2","HOPO","INDO","LAND","LSTU",
    "OFBL","ONBL","REMP","RMTI","STOF","TIFF","URNO","USEC","USEU",
    "VIA3","WRO1","ACT3","ETAI","ETDI","REGN","PSTA","PSTD","TMOA",
    "TTYP") AS 
    SELECT 
fddtab.adid adid,
fddtab.airb airb,
fddtab.apc3 apc3,
fddtab.aurn aurn,
fddtab.blt1 blt1,
fddtab.blt2 blt2,
fddtab.cdat cdat,
fddtab.ckif ckif,
fddtab.ckit ckit,
fddtab.daco daco,
fddtab.dnat dnat,
fddtab.dseq dseq,
fddtab.etof etof,
fddtab.flno flno,
fddtab.ftyp ftyp,
fddtab.gtd1 gtd1,
fddtab.gtd2 gtd2,
fddtab.hopo hopo,
fddtab.indo indo,
fddtab.land land,
fddtab.lstu lstu,
fddtab.ofbl ofbl,
fddtab.onbl onbl,
fddtab.remp remp,
fddtab.rmti rmti,
fddtab.stof stof,
fddtab.tiff tiff,
fddtab.urno urno,
fddtab.usec usec,
fddtab.useu useu,
fddtab.via3 via3,
fddtab.wro1 wro1,
afttab.act3 act3,
afttab.etai etai,
afttab.etdi etdi,
afttab.regn regn,
afttab.psta psta,
afttab.pstd pstd,
afttab.tmoa tmoa,
afttab.ttyp ttyp
FROM afttab,fddtab
WHERE fddtab.aurn = afttab.urno WITH READ ONLY;
/

CREATE OR REPLACE  FUNCTION "CEDA"."GETCCAFLDVAL"   
(v_urno in ccatab.urno%TYPE,v_fld in systab.fina%TYPE,v_tab in fldtab.rtab%TYPE)
return char
is
rc_txt char(128);
CURSOR ccatab_curs (p_urno in ccatab.urno%TYPE) IS
	SELECT * FROM ccatab WHERE urno = p_urno;
BEGIN
  if v_tab <> 'CCA' and v_tab <> 'CCATAB' then
		rc_txt := ' ';
  else
	for cca_rec in ccatab_curs(v_urno) loop
		IF ccatab_curs%NOTFOUND THEN
			rc_txt := ' ';
		ELSE
			if upper(v_fld) = 'CTYP' THEN
				rc_txt := cca_rec.ctyp;
			elsif upper(v_fld) = 'CKBA' THEN
				rc_txt := cca_rec.ckba;
			elsif upper(v_fld) = 'CKBS' THEN
				rc_txt := cca_rec.ckbs;
			elsif upper(v_fld) = 'CKEA' THEN
				rc_txt := cca_rec.ckea;
			elsif upper(v_fld) = 'CKES' THEN
				rc_txt := cca_rec.ckes;
			elsif upper(v_fld) = 'DISP' THEN
				rc_txt := cca_rec.disp;
      elsif upper(v_fld) = 'REMA' THEN
				rc_txt := cca_rec.rema;				
			else
				rc_txt := ' ';
			end if;
		END IF;
		EXIT WHEN ccatab_curs%NOTFOUND OR ccatab_curs%FOUND;
	END LOOP;
	end if;
    RETURN (rtrim(rc_txt,' '));
END;
/


CREATE OR REPLACE VIEW "CEDA"."FLVTAB" ("GTD1","GTD2","GD1Y",
    "GD2Y","CKIF","CKIT","FLNO","REMP","JFNO","JCNT","STOD","ETOD",
    "STOA","TIFA","TIFD","PSTD","PSTA","ORG3","ORG4","DES3","DES4",
    "VIA3","VIA4","BLT1","B1BA","B1EA","URNO","ABTI","AURN","ACTI",
    "AFTI","AOTI","CDAT","CREC","CTYP","DCTI","DOTI","DSEQ","HOPO",
    "LSTU","RNAM","RTAB","RURN","RTYP","SCTI","SOTI","STAT","USEC",
    "USEU","FLG1","LXT1","LXT2","FXT1","FXT2","CTY1","CKBA","CKBS",
    "CKEA","CKES","FLG2","DISP") AS 
    SELECT /*+ FIRST_ROWS */ 
rtrim(afttab.gtd1,' ') gtd1,
rtrim(afttab.gtd2,' ') gtd2,
afttab.gd1y gd1y,
afttab.gd2y gd2y,
rtrim(afttab.ckif,' ') ckif,
afttab.ckit ckit,
rtrim(afttab.flno,' ') flno,
rtrim(afttab.remp,' ') remp,
afttab.jfno jfno,
afttab.jcnt jcnt,
afttab.stod stod,
afttab.etod etod,
afttab.stoa stoa,
afttab.tifa tifa,
afttab.tifd tifd,
afttab.pstd pstd,
afttab.psta psta,
rtrim(afttab.org3,' ') org3,
rtrim(afttab.org4,' ') org4,
rtrim(afttab.des3,' ') des3,
rtrim(afttab.des4,' ') des4,
rtrim(afttab.via3,' ') via3,
rtrim(afttab.via4,' ') via4,
rtrim(afttab.blt1,' ') blt1,
afttab.b1ba b1ba,
afttab.b1ea b1ea,
fldtab.urno urno,
fldtab.abti abti,
fldtab.aurn aurn,
fldtab.acti acti,
fldtab.afti afti,
fldtab.aoti aoti,
fldtab.cdat cdat,
rtrim(fldtab.crec,' ') crec,
rtrim(fldtab.ctyp,' ') ctyp,
fldtab.dcti dcti,
fldtab.doti doti,
fldtab.dseq dseq,
fldtab.hopo hopo,
fldtab.lstu lstu,
rtrim(fldtab.rnam,' ') rnam,
fldtab.rtab rtab,
fldtab.rurn rurn,
rtrim(fldtab.rtyp,' ') rtyp,
fldtab.scti scti,
fldtab.soti soti,
rtrim(fldtab.stat,' ') stat,
fldtab.usec usec,
fldtab.useu useu,
' ' flg1,
' ' lxt1,
' ' lxt2,
' ' fxt1,
' ' fxt2,
rtrim(GetCCAFldVal(fldtab.rurn,'CTYP',fldtab.rtab),' ') cty1,
GetCCAFldVal(fldtab.rurn,'CKBA',fldtab.rtab) ckba,
GetCCAFldVal(fldtab.rurn,'CKBS',fldtab.rtab) ckbs,
GetCCAFldVal(fldtab.rurn,'CKEA',fldtab.rtab) ckea,
GetCCAFldVal(fldtab.rurn,'CKES',fldtab.rtab) ckes,
GetCCAFldVal(fldtab.rurn,'REMA',fldtab.rtab) flg2,
rtrim(GetCCAFldVal(fldtab.rurn,'DISP',fldtab.rtab),' ') disp
FROM afttab,fldtab
WHERE fldtab.aurn = afttab.urno
UNION ALL
SELECT /*+ FIRST_ROWS */ 
' ' gtd1,
' ' gtd2,
' ' gd1y,
' ' gd2y,
' ' ckif,
' ' ckit,
' ' flno,
' ' remp,
' ' jfno,
' ' jcnt,
' ' stod,
' ' etod,
' ' stoa,
' ' tifa,
' ' tifd,
' ' pstd,
' ' psta,
' ' org3,
' ' org4,
' ' des3,
' ' des4,
' ' via3,
' ' via4,
' ' blt1,
' ' b1ba,
' ' b1ea,
fldtab.urno urno,
fldtab.abti abti,
fldtab.aurn aurn,
fldtab.acti acti,
fldtab.afti afti,
fldtab.aoti aoti,
fldtab.cdat cdat,
rtrim(fldtab.crec,' ') crec,
rtrim(fldtab.ctyp,' ') ctyp,
fldtab.dcti dcti,
fldtab.doti doti,
fldtab.dseq dseq,
fldtab.hopo hopo,
fldtab.lstu lstu,
rtrim(fldtab.rnam,' ') rnam,
fldtab.rtab rtab,
fldtab.rurn rurn,
rtrim(fldtab.rtyp,' ') rtyp,
fldtab.scti scti,
fldtab.soti soti,
rtrim(fldtab.stat,' ') stat,
fldtab.usec usec,
fldtab.useu useu,
alttab.alc3 flg1,
' ' lxt1,
' ' lxt2,
' ' fxt1,
' ' fxt2,
rtrim(GetCCAFldVal(fldtab.rurn,'CTYP',fldtab.rtab),' ') cty1,
GetCCAFldVal(fldtab.rurn,'CKBA',fldtab.rtab) ckba,
GetCCAFldVal(fldtab.rurn,'CKBS',fldtab.rtab) ckbs,
GetCCAFldVal(fldtab.rurn,'CKEA',fldtab.rtab) ckea,
GetCCAFldVal(fldtab.rurn,'CKES',fldtab.rtab) ckes,
GetCCAFldVal(fldtab.rurn,'REMA',fldtab.rtab) flg2,
rtrim(GetCCAFldVal(fldtab.rurn,'DISP',fldtab.rtab),' ') disp
FROM fldtab,ccatab,alttab
WHERE fldtab.aurn=0 and fldtab.rurn = ccatab.urno and ccatab.flnu=alttab.urno WITH READ ONLY;
/
commit;
exit;
