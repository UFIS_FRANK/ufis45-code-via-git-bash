#ifndef _DEF_mks_version_buffer_h
  #define _DEF_mks_version_buffer_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_buffer_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/buffer.h 1.2 2004/07/27 16:46:47SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/* Includefile buffer.h                                                      */
/*****************************************************************************/

#ifndef __BUFFER_INC
#define __BUFFER_INC


#include <stdio.h>
#include "glbdef.h"
#include "tools.h"

/* Prototypes */
int  HandleBuffer(char**, int*, int, int, int);
int  FreeBuffer  (char**, int*, int*);

#endif 
