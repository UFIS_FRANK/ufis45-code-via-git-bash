#ifndef _DEF_mks_version_ct_h
  #define _DEF_mks_version_ct_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ct_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ct.h 1.7 2011/08/09 16:26:44SGT bst Exp $";
#endif /* _DEF_mks_version */
#ifndef __CEDATAB_H__
#define __CEDATAB_H__

/*---------------------------*/
/* THIS IS A MAGIC46 VERSION */
/*---------------------------*/

/*------------------------------*
* Datatypes
*
*-------------------------------*/
/* Data Record Line Descriptor */

/*******************************************************
String descriptor for reusage of strings. This struct 
contains a string and the byte counter for used bytes and 
allocated bytes. To be use, e.g. for GetLinesByColumnsValue 
or GetLinesByIndex als return parameter. 
So the functions can decide of how to reallocate the Value 
from inside and from the caller. The called function must 
reallocate in case of insufficient space
*******************************************************/
struct _StringDescriptor
{
	char* Value;		/* string with values */
	long  AllocatedSize;	/* byte count from malloc or realloc */
	long  UsedLen;		/* calulated value from strlen */
};
typedef struct _StringDescriptor STR_DESC;

struct _KeyItemDescriptor
{
	char *ItemPtr;
	long  ItemLen;
	char  Help[4];
	long  AllocSize;	/* byte count from malloc or realloc */
	char *Value;		/* string with values */
};
typedef struct _KeyItemDescriptor KEY_DESC;

struct _UrnoDescriptor
{
	char UrnoValue[16];    /* String with actual URNO */
	char RangeCode[16];    /* KEYS code of NUMTAB */
	long UrnoCount;        /* Valid number of URNOs */
};
typedef struct _UrnoDescriptor URNO_DESC;

/* ---------------------------- */
/* Public Function Prototypes   */
/* ---------------------------- */

/*---------------------------------
MWO: 11.09.2002
Initialise the MainArray
----------------------------------*/
extern int CT_InitCedaTabBasic();

extern long CT_ActivateSubArray(char *MainName, char *ArrayName);
extern long CT_InsertTextLine(char* ArrayName, char *LineData);
extern long CT_CreateArray(char *ArrayName);
extern long CT_CountPattern(char *Buffer, char *Pattern);
extern int CT_GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel);
extern int CT_TrimLeft(char *pcpString, char cpTrim, int ipSize);
extern int CT_TrimRight(char *pcpString, char cpTrim, int ipSize);

extern long CT_Sort(char* ArrayName, char* pcpColumns, int ipSortMethod);
extern long CT_SortByColumnNames(char* ArrayName, char* pcpColumns, int ipSortMethod);
extern long CT_InsertTextLineAt(char* ArrayName, long LineNo, char *LineData);
extern long CT_InsertBuffer(char* ArrayName, char *BufferData, char *LineSeparator);
/* extern long InsertBufferAt(long LineNo, char *BufferData, char *LineSeparator); */
extern long CT_GetLineValues(char* ArrayName, long LineNo, STR_DESC *prpString);
extern long CT_SeekDataPool(char* ArrayName, long FirstLine);
extern long CT_GetNextDataLine(char* ArrayName, char *LineData);
extern void CT_DeleteDataLine(char* ArrayName, long LineNo);

extern long CT_SetFieldList(char* ArrayName, char *FieldList);
extern long CT_SetLengthList(char* ArrayName, char *LengthList);
extern long CT_SetProperties(char* ArrayName, char *Properties);
/* extern long SetStatusValues(long LineNo, char *Main, char *Fore, char *Back, char *Time); */
extern void CT_SetDataPoolAllocSize(char *ArrayName, long lSize);
/* get data functions */
/* MWO: 11.09.2002 */
extern long CT_GetColumnValue(char* ArrayName, long LineNo, long ColNo, STR_DESC *prpString);
extern long CT_GetColumnValueByName(char* ArrayName, long LineNo, char* ColName, STR_DESC *prpString);
extern long CT_SetColumnValue(char* ArrayName, long LineNo, long ColNo, char* pcpColumnValue);
extern long CT_SetColumnValueByName(char* ArrayName, long LineNo, char* ColName, char* pcpColumnValue);
extern long CT_CreateIndex(char* ArrayName, char* IndexName, char* Columns);
extern long CT_GetFieldValues(char* ArrayName, long LineNo, char* FldNames, STR_DESC *prpString);
extern long CT_SetFieldValues(char *ArrayName, long LineNo, char *NameList, char *DataList);
extern long CT_GetMultiValues(char* ArrayName, long LineNo, char* FldNames, char *prpString);
extern long CT_SetMultiValues(char *ArrayName, long LineNo, char *NameList, char *DataList);


extern void CT_DeleteIndex(char* ArrayName, char* IndexName);
extern long CT_GetLinesByIndexValue(char* ArrayName, char* IndexName, char* pcpSearchValues, STR_DESC* prpString, char* pcpCompareMethod);

extern long CT_GetLineCount(char* ArrayName);
extern long CT_GetLinesByColumnValue(char* ArrayName, char* ColName, char* SearchValue, STR_DESC* prpString, short CompareMethod);
extern void CT_SetLineStatusValue(char* ArrayName, long LineNo, long Value);
extern long CT_GetLineStatusValue(char* ArrayName, long LineNo);
extern long CT_GetLinesByStatusValue(char* ArrayName, long Value, STR_DESC* prpString, short CompareMethod);
extern void CT_SetLineDbInsFlag(char* ArrayName, long LineNo, long Value);
extern void CT_SetLineDbUpdFlag(char* ArrayName, long LineNo, long Value);
extern void CT_SetLineDbDelFlag(char* ArrayName, long LineNo, long Value);
extern void CT_ResetLineDbFlags(char* ArrayName, long LineNo);
extern long CT_GetLineDbInsFlag(char* ArrayName, long LineNo);
extern long CT_GetLineDbUpdFlag(char* ArrayName, long LineNo);
extern long CT_GetLineDbDelFlag(char* ArrayName, long LineNo);

extern long CT_GetCurrentDataLine(char* ArrayName, char *LineData);
extern long CT_GetFieldList(char* ArrayName, char *FieldList);
extern long CT_GetLengthList(char* ArrayName, char *LengthList);
extern long CT_GetProperties(char* ArrayName, char *Properties);
/* extern long GetStatusValues(long LineNo, long *Main, long *Fore, long *Back, long *Time); */

extern long CT_SetLineTag(char* ArrayName, long LineNo, char* pcpTag);
extern long CT_GetLineTag(char* ArrayName, long LineNo, STR_DESC* prpString);
extern long CT_SetAnyList(char* ArrayName, long LineNo, char* pcpTag);
extern long CT_GetAnyList(char* ArrayName, long LineNo, STR_DESC* prpString);
extern long CT_SetAnyData(char* ArrayName, long LineNo, char* pcpTag);
extern long CT_GetAnyData(char* ArrayName, long LineNo, STR_DESC* prpString);
extern long CT_SetOldData(char* ArrayName, long LineNo, char* pcpTag);
extern long CT_GetOldData(char* ArrayName, long LineNo, STR_DESC* prpString);


extern long CT_ResetContent(char* ArrayName);

/* Helper functions */
extern int CT_GetItemNo(char* pcpLine, char* pcpItem, char* pcpSepa);
extern long CT_GetColumnNameIndex(char* ArrayName, char* ColName);
extern int CT_GetItemsFromTo(char* pcpResult, char* pcpLine, long lpFrom, long lpTo, char* pcpSepa);
extern char* CT_GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
						char *pcpTextBuff, char *pcpKeyWord, char *pcpItemEnd, int bpCopyData);
extern long CT_SetAlignmentList(char* ArrayName, char* pcpAlignmentList);
extern void CT_SetFieldSeparator(char* ArrayName, char NewSeparator);
extern long CT_GetDataItemSize(char *pcpInput, long ipNum, char cpDel, long *ipItemBegin);
extern void CT_InitStringDescriptor(STR_DESC* prpString);

/* URNO POOL Handling */
extern int CT_SetUrnoPool(char *AreaName, char *PoolName, char *FirstValue, int UrnoCount);
extern int CT_GetNextUrno(char *AreaName, char *PoolName, char *UrnoValue, int ipTrimLeft);
extern int CT_GetUrnoStrg(char *AreaName, char *PoolName, char *UrnoValue, int ipTrimLeft);
extern void CT_ResetUrnoPool(char *AreaName, char *PoolName);
extern int CT_GetUrnoCount(char *AreaName, char *PoolName);

extern int CT_SetTrimMethod(char *ArrayName, char *TrimMethod);
extern int CT_CheckTrim(char *pcpStr, char *pcpHow, char *pcpWhat);
extern long CT_GetChangedValues(char *ArrayName, char *GetFldLst, long LineNo, char *OutFldLst, char *NewDatLst, char *OldDatLst);

extern void CT_SetLineCheckedFlag(char *ArrayName, long LineNo, long Value);
extern long CT_GetLineCheckedFlag(char *ArrayName, long LineNo);

extern long CT_GetDistinctList(char *ArrayName, char *ColName, char *StrPrefix, char *StrSuffix, char *StrDelimiter, STR_DESC *prpString);
extern void CT_CheckEmptyValue(char *pcpData, char *SetValue);
extern void CT_CheckNullValues(char *pcpDataIn, char *pcpDataOut);
extern int CT_GetArrayTableName(char *ArrayName, char *TableName);
extern int CT_SetArrayDataTracking(char *ArrayName, char *TableName, int TrackChanges, int DataCheckMethod);
extern int CT_SetArrayNullValueCheck(char *ArrayName, int DataCheckMethod);

#endif
/*
	======= Methodes of TAB.OCX to be implemented =================

DONE MWO: afx_msg void SetHeaderText(LPCTSTR HeaderText);
	afx_msg void ResetContent();
DONE MWO: afx_msg void InsertTextLine(LPCTSTR LineText, BOOL bpRedraw);
	afx_msg long GetLineCount();
	afx_msg void SetLineColor(long LineNo, long TextColor, long BackColor);
	afx_msg void InsertTextLineAt(long LineNo, LPCTSTR LineText, BOOL bpRedraw);
	afx_msg short DeleteLine(long LineNo);
	afx_msg long GetCurrentSelected();
DONE MWO: afx_msg BSTR GetLineValues(long LineNo);
	afx_msg BSTR GetLinesByTextColor(long Color);
	afx_msg BSTR GetLinesByBackColor(long Color);
	afx_msg void Init();
DONE MWO: afx_msg BSTR GetColumnValue(long LineNo, long ColNo);
DONE MWO: afx_msg void SetColumnValue(long LineNo, long ColNo, LPCTSTR Value);
	afx_msg BSTR GetLinesByColor(long BackColor, long TextColor);
	afx_msg void UpdateTextLine(long LineNo, LPCTSTR Values, BOOL Redraw);
	afx_msg void SetCurrentSelection(long LineNo);
DONE BST: afx_msg void SetFieldSeparator(LPCTSTR NewSeparator);
DONE MWO: afx_msg BSTR GetLinesByColumnValue(long ColNo, LPCTSTR Value, short CompareMethod);
	afx_msg void GetLineColor(long LineNo, long FAR* txtColor, long FAR* bkColor);
	afx_msg long InsertBuffer(LPCTSTR StrBuffer, LPCTSTR LineSeparator);
	afx_msg long GetColumnCount();
	afx_msg BSTR GetHeaderText();
DONE MWO: afx_msg void Sort(LPCTSTR ColNo, BOOL bAscending, BOOL bCaseSensitive);
	afx_msg BSTR SelectDistinct(LPCTSTR Cols, LPCTSTR StrPrefix, LPCTSTR StrAppendix, LPCTSTR StrDelimiter, BOOL Distinct);
	afx_msg long GetLineStatusValue(long LineNo);
DONE MWO: afx_msg BSTR GetLinesByStatusValue(long Value, short CompareMethod);
DONE MWO: afx_msg void SetLineStatusValue(long LineNo, long Value);
	afx_msg void SetMainHeaderValues(LPCTSTR Ranges, LPCTSTR Values, LPCTSTR Colors);
	afx_msg BSTR GetMainHeaderRanges();
	afx_msg BSTR GetMainHeaderValues();
	afx_msg BSTR GetNextLineByColumnValue(long ColNo, LPCTSTR Value, short CompareMethod);
	afx_msg BSTR GetMainHeaderColors();
	afx_msg BSTR GetLinesByColumnValues(long ColNo, LPCTSTR Values, short CompareMethod);
	afx_msg BSTR GetLinesByMultipleColumnValue(LPCTSTR Columns, LPCTSTR Values, short CompareMethod);
	afx_msg BOOL SetHeaderColors(long BackColor, long FontColor);
	afx_msg void TimerSetValue(long LineNo, short Value);
	afx_msg void TimerCheck();
	afx_msg BSTR GetBuffer(long lFromLine, long lToLine, LPCTSTR LineSeparator);
	afx_msg BOOL WriteToFile(LPCTSTR FileName, BOOL WithBasicInfo);
	afx_msg BOOL ReadFromFile(LPCTSTR FileName);
	afx_msg BOOL WriteToFileWithProperties(LPCTSTR FileName);
	afx_msg BOOL ReadFromFileWithProperties(LPCTSTR FileName);
	afx_msg void SetLineTag(long LineNo, LPCTSTR Values);
	afx_msg BSTR GetLineTag(long LineNo);
	afx_msg BSTR GetLineTagKeyItem(long LineNo, LPCTSTR KeyItem, LPCTSTR StartSeparator, LPCTSTR EndSeparator);


	======= Other Methodes of TAB.OCX =================

	afx_msg BSTR GetNoFocusColumns();
	afx_msg void SetNoFocusColumns(LPCTSTR lpszNewValue);
	afx_msg void SetLineCount(long ipCount);
	afx_msg void SetColumnCount(long ipCount);
	afx_msg void RedrawTab();
	afx_msg void LockScroll(BOOL Locked);
	afx_msg BSTR GetSelectedColumns();
	afx_msg void EnableHeaderSelection(BOOL enable);
	afx_msg void EnableInlineEdit(BOOL IE_Enable);
	afx_msg void EnableInlineMoveInternal(BOOL IE_Move);
	afx_msg void ShowHorzScroller(BOOL Show);
	afx_msg void EnableHeaderSizing(BOOL enable);
	afx_msg void SetTabFontBold(BOOL bold);
	afx_msg void ColSelectionRemoveAll();
	afx_msg void ColSelectionRemove(long ColNo);
	afx_msg void ColSelectionAdd(long ColNo);
	afx_msg void OnVScrollTo(long LineNo);
	afx_msg void ShowVertScroller(BOOL Show);
	afx_msg void OnHScrollTo(long ColNo);
	afx_msg void SetRegExFormatString(LPCTSTR FormatString);
	afx_msg BOOL CheckRegExMatchCell(long LineNo, long ColNo);
	afx_msg BSTR CheckRegExMatchLine(long LineNo, BOOL TrueFalse);
	afx_msg void SetInplaceEdit(long LineNo, long ColNo, BOOL bAssumeValue);
	afx_msg long GetVScrollPos();
	afx_msg long GetHScrollPos();
	afx_msg void ResetCellProperty(long LineNo, long ColNo);
	afx_msg BOOL SetCellProperty(long LineNo, long ColNo, LPCTSTR ObjID);
	afx_msg void CreateCellObj(LPCTSTR ObjID, long BackColor, long TextColor, short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg void CreateCellBitmpapObj(LPCTSTR ObjID, LPCTSTR BitmapFile);
	afx_msg BOOL SetCellBitmapProperty(long LineNo, long ColNo, LPCTSTR ObjID);
	afx_msg void ResetCellBitmapProperty(long LineNo, long ColNo);
	afx_msg BSTR GetBitmapFileName(long LineNo, long ColNo);
	afx_msg void ResetCellProperties(long LineNo);
	afx_msg void ResetBitmapProperties(long LineNo);
	afx_msg BOOL SetColumnProperty(long ColNo, LPCTSTR ObjID);
	afx_msg void ResetColumnProperty(long ColNo);
	afx_msg BOOL SetHeaderFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg BOOL SetMainHeaderFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg void PrintTabShowDialog(LPCTSTR sDocTitle, LPCTSTR sDate);
	afx_msg void PrintTab(long lhDC, LPCTSTR sDocTitle, LPCTSTR sDate);
	afx_msg void PrintSetPageHeaderHeight(long lHeight);
	afx_msg long PrintGetPageHeaderHeight();
	afx_msg void PrintSetPageFooterHeight(long lHeight);
	afx_msg long PrintGetPageFooterHeight();
	afx_msg void PrintSetDatePosition(LPCTSTR sLocation, LPCTSTR sAlignment);
	afx_msg void PrintGetDatePosition(BSTR FAR* psLocation, BSTR FAR* psAlignment);
	afx_msg void PrintSetTitlePosition(LPCTSTR sLocation, LPCTSTR sAlignment);
	afx_msg void PrintGetTitlePosition(BSTR FAR* psLocation, BSTR FAR* psAlignment);
	afx_msg void PrintSetPageEnumeration(LPCTSTR sLoaction, LPCTSTR sAlignment, long lStyle);
	afx_msg void PrintGetPageEnumeration(BSTR FAR* psLocation, BSTR FAR* psAlignment, long FAR* plStyle);
	afx_msg void PrintSetLogo(LPCTSTR sLocation, LPCTSTR sAlignment, LPCTSTR sPath);
	afx_msg void PrintGetLogo(BSTR FAR* psLocation, BSTR FAR* psAlignment, BSTR FAR* psPath);
	afx_msg void PrintSetSeparatorLineTop(long lDistance, long lWidth);
	afx_msg void PrintGetSeparatorLineTop(long FAR* plDistance, long FAR* plWidth);
	afx_msg void PrintSetSeparatorLineBottom(long lDistance, long lWidth);
	afx_msg void PrintGetSeparatorLineBottom(long FAR* plDistance, long FAR* plWidth);
	afx_msg void PrintSetFitToPage(BOOL bFitToPage);
	afx_msg BOOL PrintGetFitToPage();
	afx_msg void PrintSetColOrderString(LPCTSTR sColString);
	afx_msg BSTR PrintGetColOrderString();
	afx_msg void PrintSetMargins(long lTop, long lBottom, long lLeft, long lRight);
	afx_msg void PrintGetMargins(long FAR* plTop, long FAR* plBottom, long FAR* plLeft, long FAR* plRight);
	afx_msg BOOL PrintSetPageTitleFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg void PrintRepeatStatus(short sRepeatStatus);
	afx_msg BOOL CedaAction(LPCTSTR Cmd, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere);
	afx_msg BSTR GetLastCedaError();
	afx_msg void DateTimeSetColumn(long ColNo);
	afx_msg void DateTimeResetColumn(long ColNo);
	afx_msg void DateTimeSetInputFormatString(long ColNo, LPCTSTR FormatString);
	afx_msg BSTR DateTimeGetInputFormatString(long ColNo);
	afx_msg void DateTimeSetOutputFormatString(long ColNo, LPCTSTR FormatString);
	afx_msg BSTR DateTimeGetOutputFormatString(long ColNo);
	afx_msg void DateTimeSetUTCOffsetMinutes(long ColNo, long OffsetMinutes);
	afx_msg long DateTimeGetUTCOffsetMinutes(long ColNo);
	afx_msg void DateTimeSetMonthNames(LPCTSTR MonthNames);
	afx_msg BSTR DateTimeGetMonthNames();
*/
