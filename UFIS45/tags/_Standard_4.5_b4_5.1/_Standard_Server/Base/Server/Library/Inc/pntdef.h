#ifndef _DEF_mks_version_pntdef_h
  #define _DEF_mks_version_pntdef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_pntdef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/pntdef.h 1.2 2004/07/27 16:48:15SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************
*                                                                           *
*	R E C O R D  S T R U C T U R E  F O R  P N T A B  (10)              *
*                                                                           *
*****************************************************************************/

#ifndef __PNTDEF_INC
#define __PNTDEF_INC


struct _pntrec
	{
        char taskname[8];	/* process name */
	short process_no;	/* process no. */
	char pad[2];		/* Two Pad Bytes */ 
        long pid;               /* pid of the process */
	char dummy[10];		/* for extension */
        } ;
	
typedef struct _pntrec PNTREC;	

#endif

/****************************************************************************/
/****************************************************************************/
