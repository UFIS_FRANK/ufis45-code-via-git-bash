#ifndef _DEF_mks_version_chkdef_h
  #define _DEF_mks_version_chkdef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkdef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/chkdef.h 1.2 2004/07/27 16:47:00SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*	**********************************************************************	*/
/*	This include file is used by all Lib-Sources belonging to chkcfg 		*/
/*	(chkospar.c, chkceda.c and chkdb.c) and the the Configuration Check Tool	*/
/*	chkcfg itself.													*/
/*	Name			:	chkdef										*/
/*	Revision date	:												*/
/*	Author		:	jmu											*/
/*																*/
/*	NOTE			:	This should be the only include file for your program	*/
/*	**********************************************************************	*/

#ifndef __CHKDEF_INC
#define __CHKDEF_INC


#include	<stdio.h>
#include	<malloc.h>
#include	<errno.h>
#include	<signal.h>
#include	<time.h>
#include	<stdlib.h>
#include	<ctype.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<unistd.h>
#include	<pwd.h>

/*	These are the only CEDA-Header-Files that are neccessary	*/

#include	"glbdef.h"
#include	"db_if.h"
#include	"flddef.h"

#define	CHKCFG_NON_FATAL	FAILURE
#define	CHKCFG_FATAL		FAILURE - 1
#define	CFG_PATH	"CFG_PATH"

#define	SINIX
/*	#define	HP_UX	*/
/*	#define	SCO		*/
/*	#define	_SOLARIS	*/
#ifdef	SINIX
#define	MTUNE	"/etc/conf/cf.d/mtune"
#define	SERVICES	"/etc/services"
#endif
#ifdef	HP_UX
#define	MTUNE	"/etc/conf/cf.d/mtune"
#define	SERVICES	"/etc/services"
#endif
#ifdef	SCO
#define	MTUNE	"/etc/conf/cf.d/mtune"
#define	SERVICES	"/etc/services"
#endif
#ifdef	_SOLARIS
#define	MTUNE	"/etc/conf/cf.d/mtune"
#define	SERVICES	"/etc/services"
#endif


#define	TRUE			1
#define	FALSE		0

#define	CHECK_PRIVILEG	0x0001
#define	CHECK_PATH	0x0002

#define	MAX_PATH_LEN	200
#define	MAX_BUFFER_LEN	10000
#define	MAX_VALUE_LEN	20
#define	MAX_FIELD_LEN	100
#define	MAX_PARAMS	100
#define	MAX_ENV_VAR_NO	MAX_PARAMS
#define	THE_WORD_LEN	100
#define	MAX_TAB_COUNT	1200
#define	MAX_RIGHT_NO	12
#define	SUCCESS		0
#define	FAILURE		-1

struct	_confpar {
	char	name[100];
	int	path;
	int	value;
};
typedef	struct	_confpar	CONFPAR;
#define	CONFPAR_SIZE		sizeof(CONFPAR)

#endif

