#ifndef _DEF_mks_version_sthdef_h
  #define _DEF_mks_version_sthdef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_sthdef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/sthdef.h 1.2 2004/07/27 16:48:30SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/*-----------------------------------------------------------*/

/*      Definitions for 'STH' (System Table Handler)         */

/*-----------------------------------------------------------*/

#ifndef _STHDEF_H
#define _STHDEF_H


#ifndef OUTRECSZ
#include <stbdef.h>         /* include stb-definitions       */
#endif

/*      Function codes for 'STH'                             */

#define STTABLE  BITV00     /* Table access                  */
#define STRECORD BITV01     /* record access                 */
#define STPART   BITV02     /* partial access                */
#define STUNLOCK BITV03     /* unlock                        */
#define STLOCK   BITV04     /* lock                          */
#define STWRITE  BITV05     /* write                         */
#define STSEARCH BITV06     /* search                        */
#define STLOCATE BITV07     /* locate                        */
#define STREAD   BITV08     /* read                          */
#define STDELETE BITV09     /* delete a record               */
#define STINSERT BITV10     /* insert a new record           */
#define STSORT   BITV11     /* insert a new record sorted    */
                            /* NOT YET IMPLEMENTED !         */

/*      Return codes                                         */

#define STNORMAL    0       /* normal return                 */
#define STINVFCT    1       /* invalid function              */
#define STTNLKD     2       /* table not locked              */
#define STRNLKD     3       /* record not locked             */
#define STTALKD     4       /* you have locked the table     */
                            /* already                       */
#define STRALKD     5       /* you have locked the record    */
                            /* already                       */
#define STINVTAB    6       /* invalid table #               */
#define STINVREC    7       /* invalid record #              */
#define STINVACT    8       /* invalic access type           */
#define STNOACTY    9       /* no access type                */
#define STNOTFND   10       /* entry not found               */
#define STTABLKD   11       /* locked by someone else        */
#define STRECLKD   12       /* locked by someone else        */
#define STBYTCNT   13       /* invalid byte count            */
#define STFULL     14       /* table is full (INSERT)        */
#define STFATAL    15       /* fatal error                   */
#define STDUPL     16       /* duplicate record (INSERT)     */
#define STTSET     17       /* table resource occupied       */
#define STRSET     18       /* record resource occupied      */
#define STNOREC    19       /* no record no. for unlock/del. */
#define STNOPTR    20       /* no buffer pointer             */
#define STINVSRC   21       /* invalid search buffer         */
#define STINVSRT   22       /* invalid sort field            */

/*      Offsets and other constants                          */
/*      (See also <stbdef.h>)                                */

#define TNLEN  sizeof(int)    /* skip max. table no.         */
#define IDXLEN sizeof(TABHDR) /* size of a table header      */
#define DESLEN sizeof(RECHDR) /* size of a record header     */


/*      Structures                                           */
  
struct _offdes                /* used for partial access     */
  {
  unsigned long stinc;        /* offset to partial data      */
  unsigned long stnb;         /* # of bytes                  */
  };

typedef struct _offdes OFFDES;

struct _srcdes                /* search buffer               */
  {
  unsigned long nobytes;      /* # of bytes to search        */
  unsigned long bitmask;      /* bit mask                    */
  unsigned long increment;    /* offset                      */
  char data[20];              /* data                        */
  };

typedef struct _srcdes SRCDES;

#define SDLEN sizeof(SRCDES)-20

struct _sthbcb                /* input buffer to 'STH'       */
  {
  unsigned long function;     /* function code               */
  char *inputbuffer;          /* address of input buffer     */
  char *outputbuffer;         /* address of output buffer    */
  unsigned long tabno;        /* table #                     */
  unsigned long recno;        /* record #                    */
  };

typedef struct _sthbcb STHBCB;

struct _pointers              /* internal structures (STH)   */
  {
  TABHDR *ptabhdr;            /* pointer to current table hdr*/
  SPTREC *psptrec;            /* pointer to SPTREC           */
  GBLADR *pgbladr;            /* pointer to GBLADR           */
  char *base;                 /* systab base address         */
  char *mend;                 /* systab end address          */
  char *table;                /* current table address       */
  char *srcptr;               /* search work pointer         */
  char *locptr;               /* locate work pointer         */
  int curr_task;              /* current task no.            */
  STHBCB *params;             /* STH argument buffer         */
  } ;

typedef struct _pointers POINT;


int sth(STHBCB *bcb,GBLADR *pgbladr);

/* _STHDEF_H */
#endif

/****************************************************************************/
/****************************************************************************/
