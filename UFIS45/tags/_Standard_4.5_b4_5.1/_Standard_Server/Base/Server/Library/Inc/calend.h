#ifndef _DEF_mks_version_calend_h
  #define _DEF_mks_version_calend_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_calend_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/calend.h 1.2 2004/07/27 16:46:49SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* Header File for Calendar Utilities */

#ifndef __CALEND_INC
#define __CALEND_INC



struct _CalTblYears
{
  char cNam4Lc[5]; /* four digits as text         */
  char cNam2Lc[3]; /* two digits as text          */
  int  iNbr4Lc;    /* full  number (4 digits)     */
  int  iNbr2Lc;    /* short number (2 digits)     */
  int  iCntDay;    /* Count of Days before 01.01. */
  int  iCntMin;    /* Count of Minutes bef.01.01. */
  int  iWekDay;    /* Day Of Week of 01.01.       */
  int  iLpYear;    /* LeapYears = 1 else 0        */
  int  iMaxDay;    /* Counter of Days in Year     */
  int  iNoWeek;    /* Week of 01.01.              */
};/* end of struct _CalTblYears */
typedef struct _CalTblYears CALTBLYEARS;
#define CALTBLYEARS_SIZE sizeof(CALTABLYEARS)

struct _CalTblMonth
{
  char cNam2Lc[3];  /* two digits as text       */
  char cNamFul[10]; /* Full Name of the Month   */
  char cNam3Lc[4];  /* Short Name of the Month  */
  int  iCntDay;     /* Count of Days before 01. */
  int  iCntMin;     /* Count of Minutes bef.01. */
  int  iWekDay;     /* Day Of Week of 01.       */
  int  iMaxDay;     /* Counter of Days in Month */
  int  iNoWeek;     /* Week of 01.              */
};/* end of struct _CalTblMonth */
typedef struct _CalTblMonth CALTBLMONTH;
#define CALTBLMONTH_SIZE sizeof(CALTABLMONTH)


/* Define all Tables as static and global */

#define MAX_CAL_LIN 50
static struct _CalTblYears CalTblYears[MAX_CAL_LIN + 1] = {
	{ "0000", "00", 0, 0, 0, 0, 0, 0, 0, 0 }
};/* end of table CalTblYears */


static struct _CalTblMonth CalTblMonth[14] = {
	{ "00", "Nothing",   "NOT", 0, 0, 0, 0, 0 },
	{ "01", "January",   "JAN", 0, 0, 0,31, 0 },
	{ "02", "February",  "FEB", 0, 0, 0,28, 0 },
	{ "03", "March",     "MAR", 0, 0, 0,31, 0 },
	{ "04", "April",     "APR", 0, 0, 0,30, 0 },
	{ "05", "May",       "MAY", 0, 0, 0,31, 0 },
	{ "06", "June",      "JUN", 0, 0, 0,30, 0 },
	{ "07", "July",      "JUL", 0, 0, 0,31, 0 },
	{ "08", "August",    "AUG", 0, 0, 0,31, 0 },
	{ "09", "September", "SEP", 0, 0, 0,30, 0 },
	{ "10", "October",   "OCT", 0, 0, 0,31, 0 },
	{ "11", "November",  "NOV", 0, 0, 0,30, 0 },
	{ "12", "December",  "DEC", 0, 0, 0,31, 0 },
	{ "00", "Nothing",   "NOT", 0, 0, 0, 0, 0 }
};/* end of table CalTblMonth */

#endif

