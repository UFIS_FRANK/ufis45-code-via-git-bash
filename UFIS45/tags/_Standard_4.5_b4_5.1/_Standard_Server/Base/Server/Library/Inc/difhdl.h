#ifndef _DEF_mks_version_difhdl_h
  #define _DEF_mks_version_difhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_difhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/difhdl.h 1.2 2004/07/27 16:47:28SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */


#ifndef _DIFHDL_H_
	#define _DIFHDL_H_
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include<string.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "timdef.h"
#include "sthdef.h"
#include "router.h" 
/* #include "netout.h"   */ 
#include <time.h>

#define OFF 0
#define DOUBLED 1

struct Tables{
	char *table_section;
	char *table_name;
	int ilKeyCnt;
	char *table_keys;
	char *table_fields;
	struct Rules **table_rule;
};

struct Command{
	char *command;
	char *print_cmd;
	char *system;
	char *date_fields;
	char type[2];
	char *tables;
};

struct Rules{
	char *rule_type;
	char *rule_name;
	char *firing_flds;
	char *local_flds;
	char *sys_tab;
	char *sys_flds;
	char *rule;
	int rule_id;
	int fired;
	struct	Rules *next_rule;
};


struct DBSTRUCT {
	char 	table_name[32];	/* Table name */
	int 	ilTotLines;		 	/* Total no. of lines */
	int 	ilFldCnt;				/* Field count */	
	int	ilLineLen; 
};

struct FLDSTRUCT{
	char  field_name[16];	/* Field name	*/
	char  field_descr[1024]; /* field description */
	char field_type[4];		/* Field type */
	int ilFldLen;				/* Field length 	*/
	int ilFldOffset;			/* Offset of field in record */
};

struct DBMIRROR{
	struct DBSTRUCT Table;
	struct FLDSTRUCT *Field; 	/* Array of Field structures 	*/
	char *Data;
};

/* Default values */
#define ARRAY_SIZE 1024


struct _QM{
	int 	flag;
	char 	surn[11];
	char 	name[41];
	char 	finm[21];
	char 	rms_peno[21];
	char 	sch_peno[21];
	char 	new_code[128];
	char 	old_code[128];
	struct 	_QM *prev;
	struct 	_QM *next; 
};

#endif
