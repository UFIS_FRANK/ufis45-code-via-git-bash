#ifndef _DEF_mks_version_remhdl_h
  #define _DEF_mks_version_remhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_remhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/remhdl.h 1.2 2004/07/27 16:48:19SGT jim Exp  $";
#endif /* _DEF_mks_version */
/************************************************************************/
/* The Master include file.                                             */
/*                                                                      */
/*  Program       :                                                     */
/*                                                                      */
/*  Revision date :                                                     */
/*                                                                      */
/*  Author    	  :                                                     */
/*                                                                      */
/*                                                                      */
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/

#ifndef __REMHDL_H
#define __REMHDL_H

/* source-code-control-system version string */

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192  
#define FIELDS "URNO,REMP,STOA,ETOA,ETAI,TIFA,STOD,ETOD,ETDI,TIFD,FTYP,GD1X,GD1Y,AIRB,LAND,OFBL,ONBL,TMOA"
#endif /* __REMHDL_H */































































