#ifndef _DEF_mks_version_initdef_h
  #define _DEF_mks_version_initdef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_initdef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/initdef.h 1.2 2004/07/27 16:47:55SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*	<initdef.h> Global includes for main programms */


#ifndef _INITDEF_H
#define _INITDEF_H


#ifndef NBBY
#include <sys/types.h>
#endif

#ifndef IPC_ALLOC
#include <sys/ipc.h>
#endif

#include <sys/shm.h>

#ifndef EPERM
#include <errno.h>
#endif

#ifndef BITV00
#include <glbdef.h>
#endif

#include <sptdef.h>
#include <sthdef.h>

/* for shared memory access */
/****************************
TWE kommentiert am 06012000

#ifndef FAILURE
#define FAILURE (char *) -1
#endif
*****************************/

/* No variable declaration in library functions !!! (made 4 bdutil JB) */
/*#ifndef STH_IS_USED_IN_LIB*/

#ifdef NEVER

SPTREC *psptrec;		/* SPTAB structure */
GBLADR gbladr, *pgbladr = &gbladr; /* global addresses */
STHBCB bcb, *pbcb = &bcb;	/* system table handler bcb */

long shrd_mem_id = FATAL;	/* shared memory id */
long shrd_mem_size;		/* shared memory size */

long glbrc;			/* global return code */

int shm_ass(), init();
char *shm_acc();

#endif

extern SPTREC *psptrec;		/* SPTAB structure */
extern GBLADR gbladr, *pgbladr; /* global addresses */
extern STHBCB bcb, *pbcb ;	/* system table handler bcb */

extern long shrd_mem_id ;	/* shared memory id */
extern long shrd_mem_size;		/* shared memory size */

extern long glbrc;			/* global return code */

int shm_ass(), init();
char *shm_acc();



/*	STH_INIT macro assigns the Shared memory and locates
	SPTAB.

	It returns the glbrc as follows:
		
	0	Normal return
       -1	Shared memory assign failed
       -2	Shared memory access failed
	n	where n is the return code from STH  */

#define STH_INIT() 

/***
        {glbrc = init(); } \

        { if(glbrc) {fprintf(stderr,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc); \
          exit(1); }}
***/

/* Only for non CEDA progs like namstat or so */
#define STH_INIT_OLD() \
        {glbrc = init(); } \
        { if(glbrc) {fprintf(stderr,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc); \
          exit(1); }}


/* _INITDEF_H */
#endif 

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
