#ifndef _DEF_mks_version_list_h
  #define _DEF_mks_version_list_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_list_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/list.h 1.2 2004/07/27 16:48:01SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*******************************************************************************
*   Name              : list.h header                                          *
*   Author            : MCU                                                    *
*   Date              : 04.12.96                                               *
*   Description       : Library module, provides funktions to create           *
*                       and handle                                             *
*                       doubly-linked lists and a quicksort method to sort 
*                       these lists.                                           *
*   Update history    :                                                        *
*******************************************************************************/

#ifndef _LIST_H
#define _LIST_H


#define DATA_UNCHANGED 0
#define DATA_CHANGED 	 1
#define DATA_NEW			 2
#define DATA_DELETED   3

#define iITEM_NAME		0
#define iITEM_NUMBER		1
#define iFIRST_ITEM		0
#define iNEXT_ITEM		1

struct lelement
{
	struct lelement *Next;
	struct lelement *Prev;
	void  *Data;
};
typedef struct lelement  LISTELEMENT;
typedef LISTELEMENT *LPLISTELEMENT;


typedef struct
{
	LPLISTELEMENT First, Last, Current;
	int      Size, Count;
	char		pcItemNames[2048+1];	
} LISTHEADER;

typedef LISTHEADER  *LPLISTHEADER;

#ifndef BOOL
#define BOOL int
#endif
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (1)
#endif


/* enum used to define types of sort criteria */
extern enum KeyType { LKEY_INT, LKEY_LONG, LKEY_DATE, LKEY_CHAR };

/* external functions */
extern int GetIndex(char *, char *, char);
extern char *GetDataField(char *, unsigned int, char);

/* function prototypes **/

/************* initializing and destroying the list ******************/
/* ListInit allocate memory to hold the list header data and 
   initialize the list header */
LPLISTHEADER ListInit(LPLISTHEADER prpList,int ipSize);
/* ListDestroy: Destroys the list by freeing all allocated memory */
LPLISTHEADER ListDestroy(LPLISTHEADER prpList);

/************* inserting and deleting list elements ******************/
/* ListAppend: Appends an element at the end of the list */
LPLISTELEMENT ListAppend(LPLISTHEADER prpList,void  *pvpData);
/* ListInsertFirst: Inserts an element at the begin of the list */
LPLISTELEMENT ListInsertFirst(LPLISTHEADER prpList,void *pvpData);
/* ListInsert: Inserts an element  before list.current */
LPLISTELEMENT ListInsert(LPLISTHEADER prpList, void  *pvpData);
/* ListDeleteLast: Deletes the last element of the list */
LPLISTELEMENT ListDeleteLast(LPLISTHEADER prpList);
/* ListDeleteFirst: Deletes the first element of the list */
LPLISTELEMENT ListDeleteFirst(LPLISTHEADER prpList);
/* ListDelete: Deletes the current element of the list */
LPLISTELEMENT ListDelete(LPLISTHEADER prpList);

/************* moving in the list  ******************/
/* ListFindPrevious: If the current element is not the first element, 
	moves the current pointer to the previous element and 
   returns a pointer to this element.
   returns NULL if there is no previous element */
LPLISTELEMENT ListFindPrevious(LPLISTHEADER prpList);
/* ListFindNext: If the current element is not the last element, 
	moves the current pointer to the next element and 
   returns a pointer to this element.
   returns NULL if there is no next element */
LPLISTELEMENT ListFindNext(LPLISTHEADER prpList);
/* ListFindLast: Moves the current pointer to the last element and 
   returns a pointer to this element.
   returns NULL if the list is empty */
LPLISTELEMENT ListFindLast(LPLISTHEADER prpList);
/* ListFindFirst: Moves the current pointer to the first element and 
   returns a pointer to this element.
   returns NULL if the list is empty */
LPLISTELEMENT ListFindFirst(LPLISTHEADER prpList);
/* ListFindKey: Searches the list for matching key and returns the first
   found element 
	pvpBase: Pointer to the struct to be searched in
	pvpOffset: Pointer to the element in the struct to be searched for */
LPLISTELEMENT ListFindKey(LPLISTHEADER prpList,void *pvpBase, void *pvpOffset,int ipKeyType,void *pvpKey);

/* -------------------------------------------------------------------------*/
/* ListSetItemNames: Set the item names of this list... */
void ListSetItemNames(LPLISTHEADER prpList, char *pcpItemNames);

/* ListFindItem: Searches the list for matching key in item list (it doesn't work with stuctures...
*/
LPLISTELEMENT ListFindItem(LPLISTHEADER prpList, char *pvpItem, int ipItemType, char *pvpKey, int ipFirstNext);

/* ListFindIndexElement: return the index-element if it exist, otherwise null
	It works like a normal index based array access 
*/
LPLISTELEMENT ListFindIndexElement(LPLISTHEADER prpList, int ipIdx);
/* -------------------------------------------------------------------------*/

#endif 


