#ifndef _DEF_mks_version_glbdef_h
  #define _DEF_mks_version_glbdef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_glbdef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/glbdef.h 1.3 2005/06/03 20:48:51SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/* 20050531 JIM: PRF 7443: do not move event in WMQ Environment */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
#ifndef GLBDEF_INC
#define GLBDEF_INC

/* 20020710: JIM: added __FILESTAT__: (defined by make on cc-Line) */
#ifdef __FILESTAT__
static char sccs_glbdef_fstat[]   MARK_UNUSED ="@(#) FileStat: "__FILESTAT__;
#endif
    
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<memory.h>
#include<malloc.h>
#include<signal.h>
#include<ctype.h>
#include<signal.h>
#include <sys/types.h>
#include <sys/timeb.h>



#include "catchal.h"

/******************************************************************************

                             G L B D E F

*******************************************************************************

         T H I S  I N C L U D E  F I L E  C O N T A I N S  A L L
               G L O B A L L Y  U S E D  C O N S T A N T S

******************************************************************************/

/******************************************************************************

                      B I T  C O N S T A N T S

******************************************************************************/

#define BIT00		 0				/* bit  0 */
#define BIT01		 1				/* bit  1 */
#define BIT02		 2				/* bit  2 */
#define BIT03		 3				/* bit  3 */
#define BIT04		 4				/* bit  4 */
#define BIT05		 5				/* bit  5 */
#define BIT06		 6				/* bit  6 */
#define BIT07		 7				/* bit  7 */
#define BIT08		 8				/* bit  8 */
#define BIT09		 9				/* bit  9 */
#define BIT10		10				/* bit 10 */
#define BIT11		11				/* bit 11 */
#define BIT12		12				/* bit 12 */
#define BIT13		13				/* bit 13 */
#define BIT14		14				/* bit 14 */
#define BIT15		15				/* bit 15 */
#define BIT16		16				/* bit 16 */
#define BIT17		17				/* bit 17 */
#define BIT18		18				/* bit 18 */
#define BIT19		19				/* bit 19 */
#define BIT20		20				/* bit 20 */
#define BIT21		21				/* bit 21 */
#define BIT22		22				/* bit 22 */
#define BIT23		23				/* bit 23 */
#define BIT24		24				/* bit 24 */
#define BIT25		25				/* bit 25 */
#define BIT26		26				/* bit 26 */
#define BIT27		27				/* bit 27 */
#define BIT28		28				/* bit 28 */
#define BIT29		29				/* bit 29 */
#define BITHO     (sizeof(unsigned long) * 8) - 1 /* high-order bit */


#define BITV00		(1 << BIT00)			/* value bit  0 */
#define BITV01		(1 << BIT01)			/* value bit  1 */
#define BITV02		(1 << BIT02)			/* value bit  2 */
#define BITV03		(1 << BIT03)			/* value bit  3 */
#define BITV04		(1 << BIT04)			/* value bit  4 */
#define BITV05		(1 << BIT05)			/* value bit  5 */
#define BITV06		(1 << BIT06)			/* value bit  6 */
#define BITV07		(1 << BIT07)			/* value bit  7 */
#define BITV08		(1 << BIT08)			/* value bit  8 */
#define BITV09		(1 << BIT09)			/* value bit  9 */
#define BITV10		(1 << BIT10)			/* value bit 10 */
#define BITV11		(1 << BIT11)			/* value bit 11 */
#define BITV12		(1 << BIT12)			/* value bit 12 */
#define BITV13		(1 << BIT13)			/* value bit 13 */
#define BITV14		(1 << BIT14)			/* value bit 14 */
#define BITV15		(1 << BIT15)			/* value bit 15 */
#define BITV16		(1 << BIT16)			/* value bit 16 */
#define BITV17		(1 << BIT17)			/* value bit 17 */
#define BITV18		(1 << BIT18)			/* value bit 18 */
#define BITV19		(1 << BIT19)			/* value bit 19 */
#define BITV20		(1 << BIT20)			/* value bit 20 */
#define BITV21		(1 << BIT21)			/* value bit 21 */
#define BITV22		(1 << BIT22)			/* value bit 22 */
#define BITV23		(1 << BIT23)			/* value bit 23 */
#define BITV24		(1 << BIT24)			/* value bit 24 */
#define BITV25		(1 << BIT25)			/* value bit 25 */
#define BITV26		(1 << BIT26)			/* value bit 26 */
#define BITV27		(1 << BIT27)			/* value bit 27 */
#define BITV28		(1 << BIT28)			/* value bit 28 */
#define BITV29		(1 << BIT29)			/* value bit 29 */
#define BITVHO    ((unsigned long)1 << BITHO)        /* high-order bit */

/**************************************************************************

	Shared memory key

***************************************************************************/

#define KEY (key_t)90	/* shared memory key */


/*****************************************************************************

    	Global error definitions

*****************************************************************************/

/*	UGCCS error codes */

/* return from subroutine successful */
/*** #define SUCCESS		0	   ***/

#define NONFATAL	1	/* Non-fatal errors are pos. numbers */
#define FATAL	       -1	/* Fatal errors are negative numbers */

/* More different error codes */

#ifndef RC_SUCCESS
#define RC_SUCCESS 0 
#endif
#ifndef RC_FAIL
#define RC_FAIL    		-1
#endif
#ifndef RC_COMM_FAIL
#define RC_COMM_FAIL    -2
#define RC_INIT_FAIL    -3
#define RC_CEDA_FAIL    -4
#define RC_SHUTDOWN     -5
#define RC_ALREADY_INIT	-6
#define RC_NOT_FOUND	-7
#define RC_DATA_CUT	-8
#define RC_TIMEOUT	-9
#define RC_OVERFLOW	-10
#define RC_RESET -11
#define RC_HOST_ERROR -12
#define RC_END_OF_LINE -13
#define RC_END_OF_FILE -14
#define RC_IGNORE -15
#endif
#ifndef RC_WMQ_FAIL
#define RC_WMQ_FAIL		-18  /* 20050531 JIM: PRF 7443 */
#endif

                      
/* Useful defines */
                      
#define EOS '\0'
#define SECONDS_PER_HOUR	3600
#define SECONDS_PER_DAY		(3600 * 24)
#define SECONDS_PER_MINUTE	60

/*	Global error codes */

#define NOT_FOUND	0


/*****************************************************************************

    	Global definitions

*****************************************************************************/

#define NUL		0	/* Makes the source code more readable */
#define MAX_PRG_NAME	8	/* size of processname */
#define SYSID_SIZE	8	/* sizeof system id in bytes */
#define DEVICEID_SIZE   10	/* sizeof device id in bytes */
#define SYSDEV_SIZE     SYSID_SIZE+DEVICEID_SIZE
#define M_LEV		16      /* Maximum level count */
#define M_FIL		10	/* Maximum filter count */

#define	TABLE_NAME_LEN	6	/* Length of the name of a table in oracle */
#define	FIELD_NAME_LEN	4	/* Length of the name of a field in oracle */
#define	ALIAS_NAME_LEN	5	/* Length of an Alias Definition in SGS	   */

#define OBJECT_NAME_LEN  TABLE_NAME_LEN  +  FIELD_NAME_LEN + 1
#define	MAX_GROUND_TIME	 (240 * 60)
#define STD_DUTY_LEN	 (30)


#ifdef TRUE
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif

#define TRUE		1
#define FALSE		0
#define MAX_EVENT_SIZE	0x8000

#define	TRACE		0x1000
#define	DEBUG		0x1001

/*************************** Machine modes *********************************/
/***** only for internal use in programms of JEL ***********************/
/***** actual mode definitions see "uevent.h" ******************/


/*****************************************************************************

                   S Y S T E M  T A B L E  N U M B E R S

******************************************************************************/

#define SPTAB 1            /* System parameter configuration table  	*/
#define COTAB 2            /* Command configuration table  		*/
#define TETAB 3            /* Terminal configuration table */
#define FITAB 4            /* Files configuration table  		*/
#define ROTAB 5            /* Routing control configuration table  	*/
#define CNTAB 6            /* Connection configuration table  	*/
#define CATAB 7            /* Closed actual flight record config. table  */
#define ACTAS 8            /* Action scheduler configuration table    */
#define FHTAB 9            /* File handler configuration table    	*/
#define PNTAB 10	   /* Process Name Table                        */
#define ACTAB 11	   /* Aircraft Table */
#define APTAB 12	   /* Airport Table */
#define CKTAB 13	   /* Checker Tabel FLTCHK */
#define BCTAB 14	   /* Broadcast Table */
#define BDTAB 15	   /* Base Data Table */
#define ALTAB 16	   /* Airline Tabel */
#define DETAB 17	   /* Delay Code Table */
#define ARTAB 18	   /* Aircraft Table */
#define GATAB 19	   /* Gate Table */
#define NATAB 20	   /* Natures Table */
#define PSTAB 21	   /* Positions Table */
#define HGTAB 22	   /* Handling agent Table */
#define SETAB 23	   /* Season Table */
#define ASTAB 24	   /* Alias Table */
#define TTTAB 25	   /* Time Scheduler Configuration Table */
#define ATTAB 26	   /* Action Scheduler Configuration Table */
#define IXTAB 27	   /* Status Index Table */
#define CTTAB 28	   /* Conflict Checker Configuration Table */
#define C1TAB 29	   /* Command table 1 for router */
#define C2TAB 30	   /* Command table 2 for router */
#define NMTAB 31	   /* Name Server Table */
#define MTTAB 32	   /* MTSHDL Table */
#define EXTAB 33	   /* EXCO Table */
#define HSTAB 34	   /* HSB Table */
#define C3TAB 35	   /* Command table 1 for router */
#define C4TAB 36	   /* Command table 2 for router */

#define MAXTAB 53	   /* max. table number                         */

#define LUTTAB (1 << BIT15) /* lookup table idicator                    */

/******************************************************************************

                         F I L E  N U M B E R S

******************************************************************************/


/******************************************************************************

       S T R U C T U R E  F O R  G L O B A L  A D D R E S S E S

******************************************************************************/
struct _gbladr 	{
    char *stiadr[2];		/* sth glob sect. addr		*/
    		};
typedef struct _gbladr GBLADR;

/*****************************************************************************

                    I M A G E  N U M B E R S

*****************************************************************************/
/*
All ccs_nto.. makros are transferred to tcputil.h
and renamed to aat_nto...
BST 21.07.99
*/

/* ********************************************************************** */
/* UFIS Comm structs				 									  */
/* ********************************************************************** */


/* 
 *
 * Following the Broadcast Header  BC_HEAD 
 *
 */
typedef struct {
	char		dest_name[10];
	char		orig_name[10];
	char		recv_name[10];
	short		bc_num;
	char		seq_id[10];
	short		tot_buf;
	short		act_buf;
	char		ref_seq_id[10];
	short		rc;
	short		tot_size;
	short		cmd_size;
	short		data_size;
	char		data[1];
}BC_HEAD;


/* 
 *
 * Following the Command Block  CMDBLK
 *
 */
typedef struct {
	char		command[6];	
	char		obj_name[33];
	char		order[2];
	char		tw_start[33];
	char		tw_end[33];
	char		data[1];
}CMDBLK;


/* 
 *
 * Following the Command Block  CMDBLK
 *
 */
typedef struct {
	char		data[1];
}DATABLK;


#define NETOUT_NO_ACK   (-42)        


#endif
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
