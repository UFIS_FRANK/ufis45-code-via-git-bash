#ifndef _DEF_mks_version_buffer2_h
  #define _DEF_mks_version_buffer2_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_buffer2_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/buffer2.h 1.2 2004/07/27 16:46:48SGT jim Exp  $";
#endif /* _DEF_mks_version */
/***************************************************************************/
/*  Master includefile for Bufferhandling                                  */
/*  Author: rkl   26.01.00                                                 */
/***************************************************************************/

#ifndef _BUFFER2_H
#define _BUFFER2_H

#include <stdio.h>



typedef struct buffer {

  int size;        /* size of buffer               */
  int stepsize;    /* stepsize in Byte for realloc */
  int used;        /* used Buffersize              */
  char *data;      /* Pointer to Data              */
  
} BUFFER;

#endif  /* _BUFFER2_H */

