#ifndef _DEF_mks_version_bhsif_h
  #define _DEF_mks_version_bhsif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_bhsif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/dgshdl.h 1.1 2007/05/03 20:42:40SGT akl Exp  $";
#endif /* _DEF_mks_version */
/**************************************************************/
/* DOCKING GUIDANCE HANDLING Interface header file            */
/**************************************************************/
#ifndef BOOL
#define BOOL int
#endif

#define SOM  (char)0x02 /* start of message */
#define EOT  (char)0x03 /* end of message */
#define MAX_TELEGRAM_LEN 400
#define CONNECT_TIMEOUT 5
#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2

#define CFG_ALPHA       1200
#define CFG_NUM         1201
#define CFG_ALPHANUM    1202
#define CFG_IGNORE      1203
#define CFG_PRINT       1204


#define ALIVE_LEN 4 		/* means 4 bytes for the length of alive-cmd */
#define ALIVE_ID "Uxxx"	/* string for alive telegram */

/************************/
/* CFG-File - STRUCTURE */
/************************/
typedef struct 
{
	/* MAIN section in .cfg file*/    
	char *debug_level;		/* start debug-level*/
	char *mode;						/* type of running mode (REAL or TEST)*/
	char *db_write;				/* should recv. data be written to DB or not */
	char *fn_onbl;			/* Field Name for OnBlock */
	char *fn_ofbl;			/* Field Name for OfFBlock */
	char *dgs_host1;			/* IP-address or hostname of the 1. BHS-server */ 
	char *dgs_host2;			/* IP-address or hostname of the 2. BHS-server */ 
	char *service_port; 	/* service name for receiving data */
	char *wait_for_ack; 	/* wait-time in sec. for acknowledge */
	char *recv_timeout; 	/* wait-time in sec. for receiving a valid telegram */
	char *max_sends;			/* max. number of sends(resends) for a telegram */
	char *recv_log; 			/* log-file for receiving telegrams */
	char *send_log; 			/* log-file for sended telegrams */
	char *try_reconnect; 	/* try to connect every xy sec. */
	char *keep_alive; 	/* keep alive interval */
	char *ftp_client_os; 	/* operating system of the client */
	char *ftp_user; 			/* username for FTP-connection */
	char *ftp_pass; 			/* password for FTP-connection */
	char *ftp_timeout; 		/* */
}CFG;

/* AKL Last Version */

