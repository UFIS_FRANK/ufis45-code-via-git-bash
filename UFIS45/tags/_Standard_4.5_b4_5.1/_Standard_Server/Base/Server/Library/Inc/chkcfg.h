#ifndef _DEF_mks_version_chkcfg_h
  #define _DEF_mks_version_chkcfg_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkcfg_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/chkcfg.h 1.2 2004/07/27 16:46:58SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*	**********************************************************************	*/
/*	The Master include file.											*/
/*																*/
/*	Program		:	chkcfg										*/
/*	Revision date	:												*/
/*	Author		:	jmu											*/
/*																*/
/*	NOTE			:	This should be the only include file for your program	*/
/*	**********************************************************************	*/

#include	"chkdef.h"

#define	MAX_CHECK_OPTIONS	18
#define	A_SWITCH	0x0001
#define	B_SWITCH	0x0002
#define	C_SWITCH	0x0004
#define	D_SWITCH	0x0008
#define	E_SWITCH	0x0010
#define	F_SWITCH	0x0020
#define	G_SWITCH	0x0040
#define	H_SWITCH	0x0080
#define	I_SWITCH	0x0100
#define	J_SWITCH	0x0200
#define	K_SWITCH	0x0400
#define	L_SWITCH	0x0800
#define	M_SWITCH	0x1000
#define	N_SWITCH	0x2000
#define	O_SWITCH	0x4000
#define	P_SWITCH	0x8000
#define	ORA_SWITCH	0x0001
#define	UNIX_SWITCH	0x0002
#define	CEDA_SWITCH	0x0004

int	iGFlag[MAX_CHECK_OPTIONS] = {
		A_SWITCH,
		B_SWITCH,
		C_SWITCH,
		D_SWITCH,
		E_SWITCH,
		F_SWITCH,
		G_SWITCH,
		H_SWITCH,
		I_SWITCH,
		J_SWITCH,
		K_SWITCH,
		L_SWITCH,
		M_SWITCH,
		N_SWITCH,
		O_SWITCH,
		P_SWITCH,
	} ;
long	lGCheckFlag = 0;

#define	OPTIONSTRING	=	"cdutC:D:"
#define	USAGE_STRING_0	"Program 'chkcfg':\nChecks Parts of the Configuration of a Computer for a CEDA-based Product.\n"
#define	USAGE_STRING_1	"Correct Usage of Options for Program 'chkcfg':\n"
#define	USAGE_STRING_2	"\tchkcfg [-t] [-c] [-d] [-u] [-C<filename>] [-D<filename>]\n"
#define	USAGE_STRING_3	"Where the meaning of the Option is as follows\n"
#define	USAGE_STRING_4	"\t\t-c:\tCheck CEDA-Configuration\n"
#define	USAGE_STRING_5	"\t\t-d:\tCheck Database-Configuration\n"
#define	USAGE_STRING_6	"\t\t-u:\tCheck UNIX-Configuration\n"
#define	USAGE_STRING_7	"\t\t-C:\tAlternate Configuration-File, needs a filename\n"
#define	USAGE_STRING_8	"\t\t-D:\tAlternate Output-File, needs a filename\n"
#define	USAGE_STRING_9	"\t\t-p:\tProgram runs in Trace-Mode\n"
#define	USAGE_STRING_10	"\t\t-P:\tProgram runs in Debug-Mode\n"
#define	USAGE_STRING_11	"\t\t-h:\tHelp-Option, prints these lines\n"
#define	CONFIGFILE	"CheckConfig.cfg"
#define	DEBUGFILE		"CheckConfig.log"
char	*KernelString[] = { "ULIMIT","SEMMNI","SEMMNS","SHMMAX","SHMMIN","SHMMNI","SHMSEG","SHMALL",
					"NPROC","MAXUP","MAXUMEM","MSGMAP","MSGMAX","MSGMNB","MSGMNI","MSGSSZ",
					"MSGSEG","MSGTQL","NCALL","END_PARAMETER" };
char	*ParString[] = {	"CCSDBUSER","CCSDBPW","CEDADBUSER","CEDADBPW","CCSPRODUCT","CCSUSER","LOG_PATH","MSG_PATH",
					"DBG_PATH","CFG_PATH","ETC_PATH","BIN_PATH","TMP_PATH","RUN_PATH","END_PARAMETER" };
char	*PathString[] = {	"LOG_PATH","MSG_PATH","DBG_PATH","CFG_PATH","ETC_PATH","BIN_PATH","TMP_PATH","RUN_PATH","END_PARAMETER" };
char	*FileString[] = {	"hsb.down","hsb.dat","sgs.tab","systab.db1","END_PARAMETER" };
char	*ServString[] = {	"UFIS_WATCH","UFIS_NETIF","UFIS_APS","UFIS_OUT","UFIS_BC","END_PARAMETER" };

