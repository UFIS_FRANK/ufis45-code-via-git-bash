package jcedalib;
import java.util.*;


public class CedaXML 
{
      private static String mks_version = "@(#) UFIS_VERSION $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/jcedalib/CedaXML.java 1.3 2005/11/01 21:41:48SGT heb Exp  $";
      private String[][] array = new String[52][2];
    	private Bchead bch;	
    	
	public void CedaXML() 
      {
	}
	

	public Bchead getXML(String msg) 
      {
            int iBegin = 0;
            int iEnd = 0;
            this.bch = new Bchead();

            StringBuffer XmlStringBuffer = new StringBuffer(msg);
            iBegin = XmlStringBuffer.indexOf("<DEST>");
            iBegin += 6;
            iEnd = XmlStringBuffer.indexOf("</DEST>",iBegin);
            this.bch.dest = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<BCORIGINATOR>");
            iBegin += 14;
            iEnd = XmlStringBuffer.indexOf("</BCORIGINATOR>",iBegin);
            this.bch.bcoriginator = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<WKS>");
            iBegin += 5;
            iEnd = XmlStringBuffer.indexOf("</WKS>",iBegin);
            this.bch.wks = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<BCNUM>");
            iBegin += 7;
            iEnd = XmlStringBuffer.indexOf("</BCNUM>",iBegin);
            this.bch.bcnum = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<SEQID>");
            iBegin += 7;
            iEnd = XmlStringBuffer.indexOf("</SEQID>",iBegin);
            this.bch.seqid = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<TOTALBUF>");
            iBegin += 10;
            iEnd = XmlStringBuffer.indexOf("</TOTALBUF>",iBegin);
            this.bch.totalbuf = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<ACTBUF>");
            iBegin += 8;
            iEnd = XmlStringBuffer.indexOf("</ACTBUF>",iBegin);
            this.bch.actbuf = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<RSEQID>");
            iBegin += 8;
            iEnd = XmlStringBuffer.indexOf("</RSEQID>",iBegin);
            this.bch.rseqid = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<RC>");
            iBegin += 4;
            iEnd = XmlStringBuffer.indexOf("</RC>",iBegin);
            this.bch.rc = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<TOTALSIZE>");
            iBegin += 11;
            iEnd = XmlStringBuffer.indexOf("</TOTALSIZE>",iBegin);
            this.bch.totalsize = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<CMDSIZE>");
            iBegin += 9;
            iEnd = XmlStringBuffer.indexOf("</CMDSIZE>",iBegin);
            this.bch.cmdsize = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<DATASIZE>");
            iBegin += 10;
            iEnd = XmlStringBuffer.indexOf("</DATASIZE>",iBegin);
            this.bch.datasize = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<CMD>");
            iBegin += 5;
            iEnd = XmlStringBuffer.indexOf("</CMD>",iBegin);
            this.bch.cmd = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<OBJECT>");
            iBegin += 8;
            iEnd = XmlStringBuffer.indexOf("</OBJECT>",iBegin);
            this.bch.object = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<ORDER>");
            iBegin += 7;
            iEnd = XmlStringBuffer.indexOf("</ORDER>",iBegin);
            this.bch.order = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<TWSTART>");



            iBegin += 9;
            iEnd = XmlStringBuffer.indexOf("</TWSTART>",iBegin);
            this.bch.twstart = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<TWEND>");
            iBegin += 7;
            iEnd = XmlStringBuffer.indexOf("</TWEND>",iBegin);
            this.bch.twend = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<DATA>");
            iBegin += 6;
            iEnd = XmlStringBuffer.indexOf("</DATA>",iBegin);
            this.bch.data = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<FIELDS>");
            iBegin += 8;
            iEnd = XmlStringBuffer.indexOf("</FIELDS>",iBegin);
            this.bch.fields = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<SELECTION>");
            iBegin += 11;
            iEnd = XmlStringBuffer.indexOf("</SELECTION>",iBegin);
            this.bch.selection = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<ROUTE>");
            iBegin += 7;
            iEnd = XmlStringBuffer.indexOf("</ROUTE>",iBegin);
            this.bch.route = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<MSGLENGTH>");
            iBegin += 11;
            iEnd = XmlStringBuffer.indexOf("</MSGLENGTH>",iBegin);
            this.bch.msglength = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<ORIGINATOR>");
            iBegin += 12;
            iEnd = XmlStringBuffer.indexOf("</ORIGINATOR>",iBegin);
            this.bch.originator = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<BLOCKNR>");
            iBegin += 9;
            iEnd = XmlStringBuffer.indexOf("</BLOCKNR>",iBegin);
            this.bch.blocknr = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<EVENT_COMMAND>");
            iBegin += 15;
            iEnd = XmlStringBuffer.indexOf("</EVENT_COMMAND>",iBegin);
            this.bch.event_command = XmlStringBuffer.substring(iBegin,iEnd);

            iBegin = XmlStringBuffer.indexOf("<EVENT_TYPE>");
            iBegin += 12;
            iEnd = XmlStringBuffer.indexOf("</EVENT_TYPE>",iBegin);
            this.bch.event_type = XmlStringBuffer.substring(iBegin,iEnd);

            return this.bch;
}
      
	// Erstellt einen XML String
	public String setXML(Bchead bch, boolean expandData) 
      {
            String delimiter = "\n";
            String tmpString ="";
            StringBuffer dataStringBuffer = new StringBuffer(bch.data);
            int i = 0;
            int myIndex = 0;
            tmpString =
            "<DEST>" + bch.dest + "</DEST>"+
            "<BCORIGINATOR>" + bch.bcoriginator + "</BCORIGINATOR>"+
            "<WKS>" + bch.wks + "</WKS>"+
            "<BCNUM>" + bch.bcnum + "</BCNUM>"+
            "<SEQID>" + bch.seqid + "</SEQID>"+
            "<TOTALBUF>" + bch.totalbuf + "</TOTALBUF>"+
            "<ACTBUF>" + bch.actbuf + "</ACTBUF>"+
            "<RSEQID>" + bch.rseqid + "</RSEQID>"+
            "<RC>" + bch.rc + "</RC>"+
            "<TOTALSIZE>" + bch.totalsize + "</TOTALSIZE>"+
            "<CMDSIZE>" + bch.cmdsize + "</CMDSIZE>"+
            "<DATASIZE>" + bch.datasize + "</DATASIZE>"+
            "<CMD>" + bch.cmd + "</CMD>"+
            "<OBJECT>" + bch.object + "</OBJECT>"+
            "<ORDER>" + bch.order + "</ORDER>"+
            "<TWSTART>" + bch.twstart + "</TWSTART>"+
            "<TWEND>" + bch.twend + "</TWEND>";
            
            myIndex = dataStringBuffer.indexOf(delimiter,i);
            if((myIndex == -1) || (expandData == false))
            {
                  tmpString = tmpString + "<DATA>" + bch.data + "</DATA>";
            }
            else
            {
                  while (myIndex != -1)
                  {
                        if(dataStringBuffer.substring(i,myIndex).length() > 0)
                        {
                              tmpString = tmpString + "<DATA>" + dataStringBuffer.substring(i,myIndex) + "</DATA>";      
                        }
                        
                        i= myIndex+1;
                        myIndex=dataStringBuffer.indexOf(delimiter,i);
                  } 
                  if(dataStringBuffer.substring(i).length() > 0)
                  {
                        tmpString = tmpString + "<DATA>" + dataStringBuffer.substring(i) + "</DATA>";
                  }
            }



 

            tmpString = tmpString + 
            "<FIELDS>" + bch.fields + "</FIELDS>"+
            "<SELECTION>" + bch.selection + "</SELECTION>"+
            "<ROUTE>" + bch.route + "</ROUTE>"+
            "<MSGLENGTH>" + bch.msglength + "</MSGLENGTH>"+
            "<ORIGINATOR>" + bch.originator + "</ORIGINATOR>"+
            "<BLOCKNR>" + bch.blocknr + "</BLOCKNR>"+
            "<EVENT_COMMAND>" + bch.event_command + "</EVENT_COMMAND>"+
            "<EVENT_TYPE>" + bch.event_type + "</EVENT_TYPE>";
            return tmpString;
	}
	
}
