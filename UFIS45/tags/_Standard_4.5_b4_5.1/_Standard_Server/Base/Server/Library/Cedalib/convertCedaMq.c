#ifndef _DEF_mks_version_convertCedaMq_c
  #define _DEF_mks_version_convertCedaMq_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_convertCedaMq_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/convertCedaMq.c 1.9 2005/10/06 16:02:06SGT mcu Exp  $";
#endif /* _DEF_mks_convertCedaMq_c */

/******************************************************************************
 *
 * History:
 * 20050530 JIM: seems to be no problem in SYSQCP environment, but in
                 WMQ environment we need to copy complete event, see caller 
 * 20050531 JIM: PRF 7443: do not move event in WMQ Environment 
 ******************************************************************************
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>

#include <ugccsma.h>
#include <uevent.h>
#include <glbdef.h>
#include <quedef.h>

static CMDBLK rgCmdBlk;
static BC_HEAD rgBcHead;
static ITEM rgItem;
static EVENT rgEvent;

static char *pcgFields;
static char *pcgSelection;
static char *pcgData;

#define TYP_INT 0
#define TYP_SHORT 1
#define TYP_CHAR 2
#define TYP_DATA 3
#define TYP_FIELDS 4
#define TYP_SELECTION 5

static struct CedaTagTableStruct {
	char tag[24];
	char *value;
	short type;
	} CedaTagTable[32] = {
	{ "DEST", rgBcHead.dest_name ,TYP_CHAR},
	{ "BCORIGINATOR", rgBcHead.orig_name ,TYP_CHAR},
	{ "WKS", rgBcHead.recv_name ,TYP_CHAR},
	{ "BCNUM", (char *) &rgBcHead.bc_num ,TYP_SHORT},
	{ "SEQID", rgBcHead.seq_id ,TYP_CHAR},
	{ "TOTALBUF",(char *)  &rgBcHead.tot_buf ,TYP_SHORT},
	{ "ACTBUF",(char *)  &rgBcHead.act_buf ,TYP_SHORT},
	{ "RSEQID", rgBcHead.ref_seq_id ,TYP_CHAR},
	{ "RC", (char *) &rgBcHead.rc ,TYP_SHORT},
	{ "TOTALSIZE",(char *) &rgBcHead.tot_size ,TYP_SHORT},
	{ "CMDSIZE",(char *) &rgBcHead.cmd_size ,TYP_SHORT},
	{ "DATASIZE",(char *) &rgBcHead.data_size ,TYP_SHORT},
	{ "CMD", rgCmdBlk.command,TYP_CHAR},
	{ "OBJECT", rgCmdBlk.obj_name,TYP_CHAR},
	{ "ORDER", rgCmdBlk.order,TYP_CHAR},
	{ "TWSTART", rgCmdBlk.tw_start,TYP_CHAR},
	{ "TWEND", rgCmdBlk.tw_end,TYP_CHAR},
	{ "DATA",(char *) &pcgData,TYP_DATA},
	{ "FIELDS", (char *)&pcgFields,TYP_FIELDS},
	{ "SELECTION", (char *)&pcgSelection,TYP_SELECTION},
	{ "FUNCTION",(char *) &rgItem.function,TYP_INT},
	{ "ITORIGINATOR",(char *) &rgItem.originator,TYP_INT},
	{ "ITPRIORITY",(char *) &rgItem.priority,TYP_INT},
	{ "ROUTE",(char *) &rgItem.route,TYP_INT},
	{ "MSGLENGTH",(char *) &rgItem.msg_length,TYP_INT},
	{ "ORIGINATOR",(char *) &rgEvent.originator,TYP_SHORT},
	{ "BLOCKNR",(char *) &rgItem.block_nr,TYP_INT},
	{ "EVENT_COMMAND",(char *) &rgEvent.command,TYP_SHORT},
	{ "EVENT_TYPE",(char *) &rgEvent.type,TYP_SHORT},
	{ "", NULL, TYP_INT }
	};

static int GetCedaField(char *pcpToken)
{
	int ilRc = RC_FAIL;

	int ilLc = 0;

	while (*CedaTagTable[ilLc].tag != '\0')
	{
		if (strcmp(CedaTagTable[ilLc].tag,pcpToken) == 0)
		{
			ilRc = ilLc;
			break;
		}
		ilLc++;
	}

	return ilRc;
}


static int GetCedaToken(char **pcpSrc)
{
	int ilRc = RC_FAIL;
	char clStartToken[124];
	char clEndToken[124];
	char *pclEnd;
	char *pclValue;
	char *pclStart;
	int ilField;
	int *pilTmp;
	short *pslTmp;
	char *pclTmp;

	if ((pclStart = strchr(*pcpSrc,'<')) != NULL)
	{
		if ((pclEnd = strchr(pclStart,'>')) != NULL)
		{

			*pclEnd = '\0';
			strncpy(clStartToken,pclStart+1,120);
			clStartToken[120] = '\0';
			if ((ilField = GetCedaField(clStartToken)) != RC_FAIL)
			{
				ilRc = RC_SUCCESS;
				pclValue = pclEnd + 1;
				*pcpSrc = pclEnd + 1;
				sprintf(clEndToken,"</%s>",clStartToken);
				if ((pclEnd = strstr(*pcpSrc,clEndToken)) != NULL)
				{
						*pcpSrc = pclEnd + strlen(clEndToken);
						*pclEnd = '\0';

						switch(CedaTagTable[ilField].type)
						{
						case TYP_CHAR:
							strcpy(CedaTagTable[ilField].value,pclValue);
							break;
						case TYP_INT:
							pilTmp = (int *) CedaTagTable[ilField].value;
							*pilTmp = atoi(pclValue);
dbg(DEBUG,"<%s> <%s> %d",clStartToken,pclValue,*pilTmp);
							break;
						case TYP_SHORT:
							pslTmp = (short *) CedaTagTable[ilField].value;				
							*pslTmp = atoi(pclValue);
							break;
						case TYP_DATA:
							/**************
							*CedaTagTable[ilField].value = pclValue;
							{
								char *pclTmp;

								pclTmp = CedaTagTable[ilField].value ;
								}
								*************/
								pcgData = pclValue;
							break;
						case TYP_FIELDS:
								pcgFields = pclValue;
							break;
						case TYP_SELECTION:
								pcgSelection = pclValue;
							break;
						}
				}
			}
		}
	}

	return ilRc;

}


int MqToCeda2(ITEM **prpItem, char *pcpData,long *lpItemSize)
{
	int ilRc = RC_SUCCESS;
	char *pclData = NULL;
	char *pclValue = NULL;
	short slField;
	long llSize;
	char *pclTmp;
	EVENT *prlEvent;
	BC_HEAD *prlBchead;
  CMDBLK *prlCmdblk;

	pclData = pcpData;

		/* initialize global structs and pointers */
		memset(&rgEvent,0,sizeof(rgEvent));
		memset(&rgItem,0,sizeof(rgItem));
		memset(&rgBcHead,0,sizeof(rgBcHead));
		memset(&rgCmdBlk,0,sizeof(rgCmdBlk));
		pcgFields = NULL;
		pcgSelection = NULL;
		pcgData = NULL;

    /* initialize some default values */
		rgEvent.command = EVENT_DATA;
		rgEvent.type = USR_EVENT;
		rgEvent.retry_count = 0;
		rgEvent.data_offset = sizeof(EVENT);
		/* this while loop needs no block, 
			all work is done inside GetCedaToken */
		while(GetCedaToken(&pclData) != RC_FAIL);

		llSize = sizeof(ITEM) + sizeof(CMDBLK) + 
			+ sizeof(EVENT) + 	sizeof(BC_HEAD);
		if (pcgSelection != NULL)
		{
			llSize += strlen(pcgSelection) + 1;
		}
		else
		{
			llSize++;
		}
		if (pcgFields != NULL)
		{
			llSize += strlen(pcgFields) + 1;
		}
		else
		{
			llSize++;
		}
		if (pcgData != NULL)
		{
			llSize += strlen(pcgData) + 1;
		}
		else
		{
			llSize++;
		}

		if(llSize > (1024*1024*50))
		{
			dbg(TRACE,"MqToCeda2: ERROR: Message to large!");
			return RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"MqToCeda2: allocating <%d> Bytes for Item.",llSize);
			*prpItem = realloc(*prpItem,llSize);
		}

		rgEvent.data_length = llSize - sizeof(ITEM) - sizeof(EVENT);

		if (rgItem.msg_length == 0)
		{  /* 20050530 JIM: seems to be no problem in SYSQCP environment, but in
			    WMQ environment we need to copy complete event, see caller */
			dbg(TRACE,"MqToCeda2: patching rgItem.msg_length to <%d>",llSize - sizeof(ITEM));
			rgItem.msg_length = llSize - sizeof(ITEM);
		}

		memcpy(*prpItem,&rgItem,sizeof(ITEM));
		prlEvent = (EVENT *) (*prpItem)->text;
		memcpy(prlEvent,&rgEvent,sizeof(EVENT));
		prlBchead     = (BC_HEAD *) ((char *) prlEvent + sizeof(EVENT)) ;
		memcpy(prlBchead,&rgBcHead,sizeof(BC_HEAD));

/* 20050531 JIM: PRF 7443: do not move event in WMQ Environment */
/* Do i need "#ifndef WMQ" as in tools.c ??? */
		if (prlBchead->rc == RC_WMQ_FAIL)
		{
			/* FROM UNKNOWN FROM tools.c: 
			  if there was an error, then the data which contains
				the error description, gets copied to the BCHD->data
				area. This is because the dll uses this when the RC is
				non zero ! The command block will then point to the
				address after the BCHD address plus the length of the data */
			dbg(TRACE,"MqToCeda2: Copying data to BC Header <%s>",pcgData);
			strcpy(prlBchead->data,pcgData);
			prlCmdblk = (CMDBLK *)  (((char *) prlBchead->data)+strlen(pcgData)+1);
			pcgData[0]=0;
			prlBchead->rc = RC_FAIL;
		}
		else
		{
			dbg(TRACE,"MqToCeda2: prlBchead->rc != RC_WMQ_FAIL <%d>",prlBchead->rc);
			prlCmdblk = (CMDBLK *)  ((char *) prlBchead->data) ;
		}
		memcpy(prlCmdblk,&rgCmdBlk,sizeof(CMDBLK));
		/**********
   	pclSelection  = prlCmdblk->data ;
   	pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   	pclData       = pclFields + strlen (pclFields) + 1 ;
		*************/

		pclTmp = prlCmdblk->data;

		if (pcgSelection != NULL)
		{
			strcpy(pclTmp,pcgSelection);
		}
		else 
		{
			strcpy(pclTmp,"");
		}
		pclTmp += strlen(pclTmp) + 1;

		if (pcgFields != NULL)
		{
			strcpy(pclTmp,pcgFields);
		}
		else 
		{
			strcpy(pclTmp,"");
		}
		pclTmp += strlen(pclTmp) + 1;
		if (pcgData != NULL)
		{
			strcpy(pclTmp,pcgData);
		}
		else 
		{
			strcpy(pclTmp,"");
		}

		return ilRc;
}
int MqToCeda(ITEM **prpItem, char *pcpData)
{
	int ilRc = RC_SUCCESS;
	char *pclData = NULL;
	char *pclValue = NULL;
	short slField;
	long llSize;
	char *pclTmp;
	EVENT *prlEvent;
	BC_HEAD *prlBchead;
  CMDBLK *prlCmdblk;

	pclData = pcpData;

		/* initialize global structs and pointers */
		memset(&rgEvent,0,sizeof(rgEvent));
		memset(&rgItem,0,sizeof(rgItem));
		memset(&rgBcHead,0,sizeof(rgBcHead));
		memset(&rgCmdBlk,0,sizeof(rgCmdBlk));
		pcgFields = NULL;
		pcgSelection = NULL;
		pcgData = NULL;

    /* initialize some default values */
		rgEvent.command = EVENT_DATA;
		rgEvent.type = USR_EVENT;
		rgEvent.retry_count = 0;
		rgEvent.data_offset = sizeof(EVENT);
		/* this while loop needs no block, 
			all work is done inside GetCedaToken */
		while(GetCedaToken(&pclData) != RC_FAIL);

		llSize = sizeof(ITEM) + sizeof(CMDBLK) + 
			+ sizeof(EVENT) + 	sizeof(BC_HEAD);
		if (pcgSelection != NULL)
		{
			llSize += strlen(pcgSelection) + 1;
		}
		else
		{
			llSize++;
		}
		if (pcgFields != NULL)
		{
			llSize += strlen(pcgFields) + 1;
		}
		else
		{
			llSize++;
		}
		if (pcgData != NULL)
		{
			llSize += strlen(pcgData) + 1;
		}
		else
		{
			llSize++;
		}

		if(llSize > (1024*1024*50))
		{
			dbg(TRACE,"MqToCeda: ERROR: Message to large!");
			return RC_FAIL;
		}
		else
		{
			dbg(TRACE,"MqToCeda: allocating <%d> Bytes for Item.",llSize);
			if (llSize < 411 + sizeof(ITEM) + sizeof(EVENT))
			{
			/*** moved from netin, due to compatibility mit buggy sysqcp version **/
					llSize = 411 + sizeof(ITEM) + sizeof(EVENT);
			}
			*prpItem = realloc(*prpItem,llSize);
		}

		rgEvent.data_length = llSize - sizeof(ITEM) - sizeof(EVENT);

		if (rgItem.msg_length == 0)
		{  /* 20050530 JIM: seems to be no problem in SYSQCP environment, but in
			    WMQ environment we need to copy complete event, see caller */
			dbg(TRACE,"MqToCeda: patching rgItem.msg_length to <%d>",llSize - sizeof(ITEM));
			rgItem.msg_length = llSize - sizeof(ITEM);
		}

		memcpy(*prpItem,&rgItem,sizeof(ITEM));
		prlEvent = (EVENT *) (*prpItem)->text;
		memcpy(prlEvent,&rgEvent,sizeof(EVENT));
		prlBchead     = (BC_HEAD *) ((char *) prlEvent + sizeof(EVENT)) ;
		memcpy(prlBchead,&rgBcHead,sizeof(BC_HEAD));

/* 20050531 JIM: PRF 7443: do not move event in WMQ Environment */
/* Do i need "#ifndef WMQ" as in tools.c ??? */
		if (prlBchead->rc == RC_WMQ_FAIL)
		{
			/* FROM UNKNOWN FROM tools.c: 
			  if there was an error, then the data which contains
				the error description, gets copied to the BCHD->data
				area. This is because the dll uses this when the RC is
				non zero ! The command block will then point to the
				address after the BCHD address plus the length of the data */
			dbg(DEBUG,"MqToCeda: Copying data to BC Header <%s>",pcgData);
			strcpy(prlBchead->data,pcgData);
			prlCmdblk = (CMDBLK *)  (((char *) prlBchead->data)+strlen(pcgData)+1);
			pcgData[0]=0;
			prlBchead->rc = RC_FAIL;
		}
		else
		{
			dbg(TRACE,"MqToCeda: prlBchead->rc != RC_WMQ_FAIL <%d>",prlBchead->rc);
			prlCmdblk = (CMDBLK *)  ((char *) prlBchead->data) ;
		}

		memcpy(prlCmdblk,&rgCmdBlk,sizeof(CMDBLK));
		/**********
   	pclSelection  = prlCmdblk->data ;
   	pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   	pclData       = pclFields + strlen (pclFields) + 1 ;
		*************/

		pclTmp = prlCmdblk->data;

		if (pcgSelection != NULL)
		{
			strcpy(pclTmp,pcgSelection);
		}
		else 
		{
			strcpy(pclTmp,"");
		}
		pclTmp += strlen(pclTmp) + 1;

		if (pcgFields != NULL)
		{
			strcpy(pclTmp,pcgFields);
		}
		else 
		{
			strcpy(pclTmp,"");
		}
		pclTmp += strlen(pclTmp) + 1;
		if (pcgData != NULL)
		{
			strcpy(pclTmp,pcgData);
		}
		else 
		{
			strcpy(pclTmp,"");
		}

		return ilRc;
}

int	CedaToMqUsingEvent(EVENT *prpEvent,char **pcpDest,long *lpSendTextLen,int ipPriority)
{
	int ilRc = RC_SUCCESS;
	int ilLc;
	long llSize;
  BC_HEAD *prlBchd       = NULL;
  CMDBLK  *prlCmdBlk       = NULL;
  char    *pclSelection    = NULL;
  char    *pclFields       = NULL;
  char    *pclData         = NULL;
	char 		*pclTmp;
	int *pilTmp;
	short *pslTmp;


 	prlBchd    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
  prlCmdBlk    = (CMDBLK *)  ((char *)prlBchd->data);
  pclSelection = prlCmdBlk->data;
  pclFields    = pclSelection + strlen(pclSelection) + 1;
  pclData      = pclFields + strlen(pclFields) + 1;

  /****************************************/

	llSize = sizeof(ITEM) + sizeof(CMDBLK) + 
			+ sizeof(EVENT) + 	sizeof(BC_HEAD) +
			strlen(pclSelection) + strlen(pclFields) +
			strlen(pclData) + (sizeof(CedaTagTable)*2);

	if (*lpSendTextLen < (llSize + 100))
	{
		*lpSendTextLen = llSize + 100;
		*pcpDest = realloc(*pcpDest,llSize+100);
		if (*pcpDest != NULL)
		{
			dbg(DEBUG,"CedaToMqUsingEvent: Reallocated TextBuffer to <%ld> Bytes.",*lpSendTextLen);
		}
	}

	if (*pcpDest == NULL)
	{
		ilRc = RC_FAIL;
		dbg(TRACE,"CedaToMqUsingEvent: ERROR: Not enough memory to create XML from Ceda!");
	}
	else
	{
		pclTmp = *pcpDest;
		memset(&rgItem,0,sizeof(ITEM));
		memcpy(&rgEvent,prpEvent,sizeof(EVENT));
		memcpy(&rgBcHead,prlBchd,sizeof(BC_HEAD));
		memcpy(&rgCmdBlk,prlCmdBlk,sizeof(CMDBLK));
	
		/** set priority from argument **/
		rgItem.priority = ipPriority;
		dbg(DEBUG,"priority=%d",rgItem.priority);
		for(ilLc = 0; *CedaTagTable[ilLc].tag != '\0';ilLc++)
		{
			switch(CedaTagTable[ilLc].type)
			{
				case TYP_CHAR:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						CedaTagTable[ilLc].value,CedaTagTable[ilLc].tag);
					break;
				case TYP_INT:
					pilTmp = (int *) CedaTagTable[ilLc].value;				
					sprintf(pclTmp,"<%s>%d</%s>\n",CedaTagTable[ilLc].tag,*pilTmp,
					CedaTagTable[ilLc].tag);
					/*dbg(DEBUG,"<%s> %d",pclTmp,*pilTmp);*/
				break;
				case TYP_SHORT:
					pslTmp = (short *) CedaTagTable[ilLc].value;				
					sprintf(pclTmp,"<%s>%d</%s>\n",CedaTagTable[ilLc].tag,*pslTmp,
					CedaTagTable[ilLc].tag);
				break;
				case TYP_DATA:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						pclData,CedaTagTable[ilLc].tag);
					break;
				case TYP_FIELDS:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						pclFields,CedaTagTable[ilLc].tag);
					break;
				case TYP_SELECTION:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						pclSelection,CedaTagTable[ilLc].tag);
					break;
				}
				pclTmp += strlen(pclTmp);
		}
	}
	return ilRc;
}

int	CedaToMq(ITEM *prpItem,char **pcpDest)
{
	int ilRc = RC_SUCCESS;
	int ilLc;
	long llSize;
	EVENT *prlEvent;
  BC_HEAD *prlBchd       = NULL;
  CMDBLK  *prlCmdBlk       = NULL;
  char    *pclSelection    = NULL;
  char    *pclFields       = NULL;
  char    *pclData         = NULL;
	char 		*pclTmp;
	int *pilTmp;
	short *pslTmp;


	
  prlEvent = (EVENT *) prpItem->text;
 	prlBchd    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT));
  prlCmdBlk    = (CMDBLK *)  ((char *)prlBchd->data);
  pclSelection = prlCmdBlk->data;
  pclFields    = pclSelection + strlen(pclSelection) + 1;
  pclData      = pclFields + strlen(pclFields) + 1;


  /****************************************/

	llSize = sizeof(ITEM) + sizeof(CMDBLK) + 
			+ sizeof(EVENT) + 	sizeof(BC_HEAD) +
			strlen(pclSelection) + strlen(pclFields) +
			strlen(pclData) + (sizeof(CedaTagTable)*2);


	*pcpDest = realloc(*pcpDest,llSize);
	if (*pcpDest == NULL)
	{
		ilRc = RC_FAIL;
		dbg(TRACE,"CedaToMq: ERROR: Not enough memory to create XML from Ceda!");
	}
	else
	{
		dbg(DEBUG,"CedaToMq: Reallocated Textbuffer to <%ld> Bytes.",llSize);
		pclTmp = *pcpDest;
		memcpy(&rgItem,prpItem,sizeof(ITEM));
		memcpy(&rgEvent,prlEvent,sizeof(EVENT));
		memcpy(&rgBcHead,prlBchd,sizeof(BC_HEAD));
		memcpy(&rgCmdBlk,prlCmdBlk,sizeof(CMDBLK));

		for(ilLc = 0; *CedaTagTable[ilLc].tag != '\0';ilLc++)
		{
			switch(CedaTagTable[ilLc].type)
			{
				case TYP_CHAR:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						CedaTagTable[ilLc].value,CedaTagTable[ilLc].tag);
					break;
				case TYP_INT:
					pilTmp = (int *) CedaTagTable[ilLc].value;				
					sprintf(pclTmp,"<%s>%d</%s>\n",CedaTagTable[ilLc].tag,*pilTmp,
					CedaTagTable[ilLc].tag);
				break;
				case TYP_SHORT:
					pslTmp = (short *) CedaTagTable[ilLc].value;				
					sprintf(pclTmp,"<%s>%d</%s>\n",CedaTagTable[ilLc].tag,*pslTmp,
					CedaTagTable[ilLc].tag);
				break;
				case TYP_DATA:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						pclData,CedaTagTable[ilLc].tag);
					break;
				case TYP_FIELDS:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						pclFields,CedaTagTable[ilLc].tag);
					break;
				case TYP_SELECTION:
					sprintf(pclTmp,"<%s>%s</%s>\n",CedaTagTable[ilLc].tag,
						pclSelection,CedaTagTable[ilLc].tag);
					break;
				}
				pclTmp += strlen(pclTmp);
		}
	}
	return ilRc;
}
