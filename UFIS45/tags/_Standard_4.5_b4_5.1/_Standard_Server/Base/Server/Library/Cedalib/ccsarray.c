#ifndef _DEF_mks_version_ccsarray_c
  #define _DEF_mks_version_ccsarray_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ccsarray_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/ccsarray.c 1.2 2004/08/10 20:10:52SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB AAT/I ccsarray.c					                                          */
/*                                                                            */
/* Author         : Jibbo Mueller / JIM                                       */
/* Date           : 14 May 1999                                               */
/* Description    : library functions for arrays                              */
/*                  this module implements an unsorted array with sorted keys */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/

#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include "ccsarray.h"
#ifdef _MSC_VER
   #include "jim.h"
#else
   #include "glbdef.h"
   #include "tools.h"
   extern int itemCount(char *);
   #include "fditools.h"
#endif
#define MAX_TMPBUF1_SIZE 4096




#ifndef min
#define min(a,b)    (((a) < (b)) ? (a) : (b))
#endif
#ifndef max
#define max(a,b)    (((a) > (b)) ? (a) : (b))
#endif
/*
extern void *my_malloc(long size);
extern void my_free(void *ptr);
extern void *my_realloc(void * ptr, long size);
#define malloc(size) my_malloc(size)
#define free(size) my_free(size)
#define realloc(ptr,size) my_realloc(ptr,size)
*/
#define TEST_ARRAY
#undef TEST_ARRAY
#ifdef TEST_ARRAY
long xxx_prgTestArray[100];
int xxx_igNumOfKeys= 0;
#endif /*TEST_ARRAY*/

CCS_KEY *prgCedaTABArrBase=NULL; /* the Base of th Ceda arrays */
typedef struct T_CEDA_REC   /* == the CEDA array records structure == */
{
  char TabName[4];
  CEDA_ARRAY *CedaArr;
} CEDA_REC;

typedef char** KEY_TYPE;  /* for CCSTab... Routines */
/* =================================================================== 
   recursive procedure to sort an unsorted key array with quick sort
*/
KEY_RECORD prgTmpKeyRec2; /* for CCSArrayQKeySort, not needed for recursion, so global */
void *pvgTmpKeyPtr;       /* for CCSArrayQKeySort, not needed for recursion, so global */
long lgIndex1;            /* for CCSArrayQKeySort, not needed for recursion, so global */

void CCSArrayQKeySort(const CCS_KEY *prpKeyBase, const long lpLeft, const long lpRight)
{
   long llIndex2;

   if ( lpRight >  lpLeft)
   {
      pvgTmpKeyPtr= prpKeyBase->Keys[lpRight].Key; 
      llIndex2= lpLeft-1; 
      lgIndex1= lpRight;
      for (;;)
      {
         while ( (*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->Keys[++llIndex2].Key, pvgTmpKeyPtr) == -1) ;
         while ( (lgIndex1>llIndex2) && 
             ((*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->Keys[--lgIndex1].Key, pvgTmpKeyPtr) ==  1)) ;
         if ( llIndex2 >= lgIndex1 ) break;
         prgTmpKeyRec2= prpKeyBase->Keys[llIndex2]; 
         prpKeyBase->Keys[llIndex2]= prpKeyBase->Keys[lgIndex1]; 
         prpKeyBase->Keys[lgIndex1]= prgTmpKeyRec2;
     }
      prgTmpKeyRec2= prpKeyBase->Keys[llIndex2]; 
      prpKeyBase->Keys[llIndex2]= prpKeyBase->Keys[lpRight]; 
      prpKeyBase->Keys[lpRight]= prgTmpKeyRec2;

      CCSArrayQKeySort(prpKeyBase, lpLeft, llIndex2-1);
      CCSArrayQKeySort(prpKeyBase, llIndex2+1, lpRight);
   }
} /* CCSArrayQKeySort */


/* =================================================================== 
   Main procedure to sort an unsorted key array. The sort itself is done
   with a recursiv kind of quick sort.
*/
void CCSArrayOneKeySort(CCS_KEY *prpKeyBase)
{
   CCSArrayQKeySort( prpKeyBase, 0, prpKeyBase->ArrBase->Count-1);
   prpKeyBase->NextIndex=-1;
   prpKeyBase->ArrBase->MustSortKey = 0;
} /* CCSArrayOneKeySort */


/* =================================================================== 
   Main procedure to sort an unsorted key array. The sort itself is done
   with a recursiv kind of quick sort.
*/
extern void CCSArrayKeySort(CCS_KEY *prpKeyBase)
{
CCS_KEY *prlTmpKey;

   prlTmpKey= prpKeyBase->ArrBase->FirstKey;
   while (prlTmpKey!=NULL)
   {
      CCSArrayOneKeySort(prlTmpKey);
      prlTmpKey= prlTmpKey->NextKey;
   }
}

/* =============================================================== 
   Init procedure for slave keys. When a slave key is created, this
   procedure creates the key records for all data records already 
   stored with the master key.
*/
int CCSArrayMakeSubKeys(CCS_ARR *prpArr, CCS_KEY *prpNewKey)
{
long llLoop;
int ilRC= CCS_SUCCESS;

   for (llLoop=0; llLoop < prpArr->Count; llLoop++)
   {
      prpNewKey->Keys[llLoop].Key= malloc(prpNewKey->KeyLen);
      if (prpNewKey->Keys[llLoop].Key==NULL)
         return (CCS_ERR_NO_MEM);
      memcpy((prpNewKey->Keys[llLoop].Key),
            (*prpNewKey->KeyOfData)(prpNewKey->UserInfo,prpArr->CcsBase[llLoop].DataPtr), 
              prpNewKey->KeyLen);
      prpNewKey->Keys[llLoop].ArrayIndex = llLoop;
   };
   CCSArrayOneKeySort(prpNewKey);

   return(CCS_SUCCESS);
} /* CCSArrayMakeSubKeys */


/* =============================================================== 
   Init procedure for the Array. 
*/
CCS_ARR *CCSArrayArrInit( const long lpMaxCount,    
                          const long lpIncCount)
{
CCS_ARR     *prlTmpBase;

  prlTmpBase= malloc( sizeof(CCS_ARR));
  if (prlTmpBase==NULL)
  {
     return (NULL);
  }

  prlTmpBase->Count        = 0;          /* Number of elements in array */
  prlTmpBase->MaxCount     = lpMaxCount; /* Number of elements memory allocated */
  prlTmpBase->IncCount     = lpIncCount; /* Number of element increments when full*/
  prlTmpBase->FirstKey= NULL;

  prlTmpBase->CcsBase     = malloc( prlTmpBase->MaxCount * sizeof(DATA_RECORD)); 
  if (prlTmpBase->CcsBase==NULL)
  {  
    free(prlTmpBase);
     return (NULL);
  }

  prlTmpBase->MustSortKey  = 1;
  prlTmpBase->NextIndexA   = -1;         /* ArrFirst no called yet */

  return (prlTmpBase);
} /* CCSArrayArrInit */

/* =============================================================== 
   Init procedure for the Keys. If the parameter prpArr is a NULL
   pointer, the new key is handled as master key, otherwise prpArr
   is treated as master key for a new slave key. In the first case the
   space for the data elements is allocated, in the second case the
   slave pointer to the data is set to master pointer of the data.
*/
extern CCS_KEY *CCSArrayInit(CCS_KEY      *prpKeyBase, 
								     void         *pvpUserInfo,
                             const int     ipKeyLen,
                             CCS_COMPARE  *pppCompareFkt,  
                             CCS_KEY_PROC *pppKeyOfData,
                             const long    lpMaxCount,    
                             const long    lpIncCount,
                             int          *pipRC)
{
CCS_KEY *prlTmpBase;
CCS_KEY *prlTmpKey;
CCS_ARR    *prlArr;

  if ((*pppCompareFkt==NULL) || (*pppKeyOfData==NULL) )
  {
     *pipRC=CCS_ERR_NULL_POINTER;
     return (NULL);
  }
  if (prpKeyBase!=NULL)
     prlArr= prpKeyBase->ArrBase;
  else
  {  /* no key defined for this array ==> have to define Array */
     prlArr= CCSArrayArrInit(lpMaxCount,lpIncCount);
     if (prlArr==NULL)
     {
        *pipRC=CCS_ERR_NO_MEM;
        return (NULL);
     }
  }

  prlTmpBase= malloc( sizeof(CCS_KEY));
  if (prlTmpBase==NULL)
  {
     *pipRC=CCS_ERR_NO_MEM;
     return (NULL);
  }

  if (prlArr->FirstKey == NULL)
  {
    prlArr->FirstKey = prlTmpBase;
  }
  else
  {
     prlTmpKey= prlArr->FirstKey;
     while (prlTmpKey->NextKey!=NULL)
       prlTmpKey= prlTmpKey->NextKey;
     prlTmpKey->NextKey= prlTmpBase;
  }
#ifdef TEST_ARRAY
   prlTmpBase->Keys        = (KEY_RECORD*)&(xxx_prgTestArray[0])+xxx_igNumOfKeys*12;
   xxx_igNumOfKeys++;
#else /*TEST_ARRAY*/
  prlTmpBase->Keys         = malloc( prlArr->MaxCount * sizeof(KEY_RECORD)); 
  if (prlTmpBase->Keys==NULL)
  {
    *pipRC=CCS_ERR_NO_MEM;
    return (NULL);
  }
#endif /*TEST_ARRAY*/
  prlTmpBase->ArrBase      =  prlArr;
  prlTmpBase->UserInfo     =  pvpUserInfo;
  prlTmpBase->Compare      = *pppCompareFkt;
  prlTmpBase->KeyOfData    = *pppKeyOfData;
  prlTmpBase->KeyLen       = ipKeyLen;
  prlTmpBase->RangeBeg1=-1;
  prlTmpBase->RangeEnd1=-2;
  prlTmpBase->RangeBeg2=-1;
  prlTmpBase->RangeEnd2=-2;
  prlTmpBase->RangeKey1    = malloc(ipKeyLen);
  prlTmpBase->RangeKey2    = malloc(ipKeyLen);
  prlTmpBase->TmpKey       = malloc(ipKeyLen);
  if ((prlTmpBase->TmpKey==NULL) || (prlTmpBase->RangeKey1==NULL) ||
      (prlTmpBase->RangeKey2==NULL) )
  {
    *pipRC=CCS_ERR_NO_MEM;
     return (NULL);
  }

  prlTmpBase->NextIndex = -1;         /* KeyFirst no called yet */
  memset(prlTmpBase->RangeKey1,0,ipKeyLen);
  memset(prlTmpBase->RangeKey2,0,ipKeyLen);
  memset(prlTmpBase->TmpKey,0,ipKeyLen);
  prlTmpBase->NextKey= NULL;

 *pipRC= CCSArrayMakeSubKeys(prlArr,prlTmpBase);

  return (prlTmpBase);
} /* CCSKeyInit */

  
/* =================================================================== 
   search key element with key equal or NextKey-to prpKey and return 
   the index. The search is done with binary search in a sorted array.
*/   
long CCSArraySearchNextTo(CCS_KEY   *prpKeyBase,
                          const void      *prpKey, 
                          int             *ipSearchRC)
{
long llMiddle,llOldMiddle,llBegin,llEnd;
CCS_COMPARE  *pflCompareFkt= prpKeyBase->Compare; /* local copy of compare function,
                                                    for (little) more perfomance */
   if (prpKeyBase->ArrBase->MustSortKey == 1) 
       CCSArrayOneKeySort(prpKeyBase);

   llBegin = 0;
   llEnd   = prpKeyBase->ArrBase->Count-1;
   llOldMiddle= -1;
   llMiddle= (llEnd - llBegin) / 2;
   while (llOldMiddle!=llMiddle)
   {
      llOldMiddle=llMiddle;
      *ipSearchRC= (*pflCompareFkt)(prpKeyBase->UserInfo,prpKey,prpKeyBase->Keys[llMiddle].Key);
      if        (*ipSearchRC == 1)
      {
         llBegin= llMiddle;
         llMiddle= llEnd - ((llEnd - llBegin) / 2);
         
         if ((llMiddle==llOldMiddle) && (llEnd != llBegin))
         {   /* Difference between llBegin and llEnd is 1, 
               only one more compare to do */
            if (llMiddle==llEnd)
                llMiddle= llBegin;
            else
                llMiddle= llEnd;
            llOldMiddle=llMiddle;
            *ipSearchRC= (*pflCompareFkt)(prpKeyBase->UserInfo,prpKey,prpKeyBase->Keys[llMiddle].Key);
         }
      } else if (*ipSearchRC == -1)
      {
         llEnd= llMiddle;
         llMiddle= (llEnd - llBegin) / 2;
         llMiddle= llEnd - ((llEnd - llBegin) / 2);
         if ((llMiddle==llOldMiddle) && (llEnd != llBegin))
         {   /* Difference between llBegin and llEnd is 1, 
               only one more compare to do */
            if (llMiddle==llEnd)
                llMiddle= llBegin;
            else
                llMiddle= llEnd;
            llOldMiddle=llMiddle;
            *ipSearchRC= (*pflCompareFkt)(prpKeyBase->UserInfo,prpKey,prpKeyBase->Keys[llMiddle].Key);
         }
      } else /* found */
      {
         llOldMiddle= llMiddle; /* leave loop */
      }
   }
   return llMiddle;
} /* CCSArraySearchNextTo */


/* =============================================================== 
   find the place where to insert the key into a sorted array of keys 
*/
long CCSArrayPosToInsert(CCS_KEY *prpKeyBase, const void *pvpData)
{
  long llPos;
  int ilSearchRC;

  /* generate key for data: */
  memcpy(prpKeyBase->TmpKey,(*prpKeyBase->KeyOfData)(prpKeyBase->UserInfo,pvpData),prpKeyBase->KeyLen);
  if (prpKeyBase->ArrBase->Count!=0)
  {  /* find place to insert with binary search: */
    llPos= CCSArraySearchNextTo(prpKeyBase,prpKeyBase->TmpKey,&ilSearchRC);
    if (ilSearchRC==0)
    {   /* if key is already in array, search last entry with same key
          to insert the new key behind that key. Search is done by
          stepping trough the array starting at found entry
       */
       while ((llPos<prpKeyBase->ArrBase->Count-1) && (ilSearchRC==0))
       {
          llPos++;
          ilSearchRC= (*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->TmpKey,prpKeyBase->Keys[llPos].Key);
       }
       if ((ilSearchRC==0) && (llPos<prpKeyBase->ArrBase->Count-1))
          llPos++;
       ilSearchRC=0; /* to mark as found */
    }
    else if (ilSearchRC==1)
    {   /* last testet entry was lower than key, so step behind it: */
       llPos++;
    }
    if (llPos<prpKeyBase->ArrBase->Count)
    {   /* if there are entrys behind this position, move them one
          position up
       */
         memmove(&prpKeyBase->Keys[llPos+1], 
                &prpKeyBase->Keys[llPos],  
               (prpKeyBase->ArrBase->Count - llPos) * sizeof (KEY_RECORD));
    }
  }     /*     prpKeyBase->Count!=0 */ 
  else  /* ==> prpKeyBase->Count==0 */ 
  { 
     llPos=0;
  }
  return(llPos);
} /* CCSArrayPosToInsert */


/* =============================================================== 
   Slave procedure to locate the position for a new element
   of the array and insert the slave key
*/
int CCSArrayInsertSlave(CCS_KEY    *prpKeyBase, 
                        const void *pvpData)
{
  long llPos;

  /* find the place where to insert the key into a sorted array of keys */
  llPos= CCSArrayPosToInsert(prpKeyBase, pvpData);

  prpKeyBase->Keys[llPos].Key= malloc(prpKeyBase->KeyLen);
  if (prpKeyBase->Keys[llPos].Key==NULL)
         return (CCS_ERR_NO_MEM);
  memcpy((prpKeyBase->Keys[llPos].Key),prpKeyBase->TmpKey, prpKeyBase->KeyLen);
  prpKeyBase->Keys[llPos].ArrayIndex = prpKeyBase->ArrBase->Count;
  if ((llPos<=prpKeyBase->NextIndex) && (prpKeyBase->NextIndex>-1))
     prpKeyBase->NextIndex++;     /* adjust next index to process */
  if ((llPos<=prpKeyBase->SearchRange1) && (prpKeyBase->SearchRange1>-1))
     prpKeyBase->SearchRange1++;  /* adjust first index to process */
  if ((llPos<=prpKeyBase->SearchRange2) && (prpKeyBase->SearchRange2>-1))
     prpKeyBase->SearchRange2++;  /* adjust last index to process */
  else if ((llPos==prpKeyBase->SearchRange2+1) && (prpKeyBase->SearchRange2>-1))
     if ((*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->TmpKey,prpKeyBase->RangeKey2)<=0)
        prpKeyBase->SearchRange2++;  /* adjust last index to process */

  /* any more slaves: */
  if (prpKeyBase->NextKey!=NULL)
     return CCSArrayInsertSlave( prpKeyBase->NextKey, pvpData);

  return ( CCS_SUCCESS );
} /* CCSArrayInsertSlave */


/* =============================================================== 
   Master procedure to locate the position for a new element of the
   array and insert the element. In case it is called with a slave
   key, this routine will reenter with the master key.
*/
int CCSArrayArrInsert(CCS_ARR *prpArr, 
                      const void *pvpData, 
                      const long lpDataLen)
{
  void *prlTmp;
  CCS_KEY *rlKeyPtr;
  int ilRC= CCS_SUCCESS;

  if (prpArr->Count==prpArr->MaxCount)
  {  /* We are at the initial (or reallocated) array bound, so reallocate 
       (again)
     */
     prlTmp = realloc( prpArr->CcsBase, 
                    (prpArr->MaxCount+prpArr->IncCount)*sizeof(DATA_RECORD)); 
    if (prlTmp==NULL)
       return (CCS_ERR_NO_MEM);
    prpArr->CcsBase= prlTmp;
    rlKeyPtr = prpArr->FirstKey;
#ifndef TEST_ARRAY
    while (rlKeyPtr != NULL)
    {
        prlTmp = realloc( rlKeyPtr->Keys, 
                         (prpArr->MaxCount+prpArr->IncCount)*sizeof(KEY_RECORD)); 
       if (prlTmp==NULL)
          return (CCS_ERR_NO_MEM);

       rlKeyPtr->Keys= prlTmp;
        rlKeyPtr = rlKeyPtr->NextKey;
    }
#endif /*TEST_ARRAY*/

    prpArr->MaxCount+= prpArr->IncCount;
  }

  prpArr->CcsBase[prpArr->Count].DataLen = lpDataLen; 
  prpArr->CcsBase[prpArr->Count].DataPtr = malloc(lpDataLen); 
  if (prpArr->CcsBase[prpArr->Count].DataPtr==NULL)
    return (CCS_ERR_NO_MEM);
  prlTmp= prpArr->CcsBase[prpArr->Count].DataPtr;
  memcpy(prpArr->CcsBase[prpArr->Count].DataPtr, pvpData, lpDataLen); 

  /* if there are slave keys, create a key entry in the slave keys also */
  if (prpArr->FirstKey!=NULL)
     ilRC= CCSArrayInsertSlave( prpArr->FirstKey, pvpData);

  if (ilRC == CCS_SUCCESS)
     prpArr->Count++;

  return ( CCS_SUCCESS );
} /* CCSArrayArrInsert */


/* =============================================================== 
   Master procedure to locate the position for a new element of the
   array and insert the element. In case it is called with a slave
   key, this routine will reenter with the master key.
*/
extern int CCSArrayInsert(CCS_KEY *prpKeyBase, 
                          const void *pvpData, 
                          const long lpDataLen)
{
  return ( CCSArrayArrInsert(prpKeyBase->ArrBase, pvpData, lpDataLen) );
} /* CCSArrayInsert */


/* =============================================================== 
   Slave procedure to insert a new element into the array and into
   the unsorted key array
*/
int CCSArrayAddUnsortSlave(CCS_ARR  *prpArr, 
                           CCS_KEY    *prpKeyBase, 
                           const void *pvpData)
{
  /* generate key for data: */
  prpKeyBase->Keys[prpArr->Count].Key= malloc(prpKeyBase->KeyLen);
  if (prpKeyBase->Keys[prpArr->Count].Key==NULL)
         return (CCS_ERR_NO_MEM);
  memcpy((prpKeyBase->Keys[prpArr->Count].Key),(*prpKeyBase->KeyOfData)(prpKeyBase->UserInfo,pvpData), 
        prpKeyBase->KeyLen);
  prpKeyBase->Keys[prpArr->Count].ArrayIndex = prpArr->Count;

  /* any more slaves: */
  if (prpKeyBase->NextKey!=NULL)
     return CCSArrayAddUnsortSlave( prpArr, prpKeyBase->NextKey, pvpData);

  return ( CCS_SUCCESS );
} /* CCSArrayAddUnsortSlave */


/* =============================================================== 
   Main procedure to insert a new element into the array and into
   the unsorted key array. First Parameter is from type CCS_ARRAY
*/
int CCSArrayArrAddUnsort(CCS_ARR  *prpArr, 
                         const void *pvpData, 
                         const long  lpDataLen)
{
  void *prlTmp;
  CCS_KEY *rlKeyPtr;
  int ilRC= CCS_SUCCESS;

  if (prpArr->Count==prpArr->MaxCount)
  {  /* We are at the initial (or reallocated) array bound, so reallocate 
       (again)
     */
     prlTmp = realloc( prpArr->CcsBase, 
                    (prpArr->MaxCount+prpArr->IncCount)*sizeof(DATA_RECORD)); 
    if (prlTmp==NULL)
       return (CCS_ERR_NO_MEM);

    prpArr->CcsBase= prlTmp;

    rlKeyPtr = prpArr->FirstKey;
#ifndef TEST_ARRAY
    while (rlKeyPtr != NULL)
    {
        prlTmp = realloc( rlKeyPtr->Keys, 
                         (prpArr->MaxCount+prpArr->IncCount)*sizeof(KEY_RECORD)); 
       if (prlTmp==NULL)
          return (CCS_ERR_NO_MEM);

       rlKeyPtr->Keys= prlTmp;
        rlKeyPtr = rlKeyPtr->NextKey;
    }
#endif /*TEST_ARRAY*/

    prpArr->MaxCount+= prpArr->IncCount;
  }

  prpArr->CcsBase[prpArr->Count].DataLen = lpDataLen; 
  prpArr->CcsBase[prpArr->Count].DataPtr = malloc(lpDataLen); 
  if (prpArr->CcsBase[prpArr->Count].DataPtr==NULL)
    return (CCS_ERR_NO_MEM);

  prlTmp= prpArr->CcsBase[prpArr->Count].DataPtr;
  memcpy(prpArr->CcsBase[prpArr->Count].DataPtr, pvpData, lpDataLen); 

  prpArr->MustSortKey = 1;

  /* if there are slave keys, create a key entry in the slave keys also */
  if (prpArr->FirstKey!=NULL)
     ilRC= CCSArrayAddUnsortSlave( prpArr, prpArr->FirstKey, pvpData);

  if (ilRC == CCS_SUCCESS)
     prpArr->Count++;

  return ( CCS_SUCCESS );
} /* CCSArrayArrAddUnsort */

  
/* =============================================================== 
   Main procedure to insert a new element into the array and into
   the unsorted key array. First Parameter is from type CCS_KEY.
*/
extern int CCSArrayAddUnsort(CCS_KEY    *prpKeyBase, 
                             const void *pvpData, 
                             const long  lpDataLen)
{
   return(CCSArrayArrAddUnsort(prpKeyBase->ArrBase, pvpData, lpDataLen));
}


/* =================================================================== 
   procedure to locate and delete an key element of the array
*/
void CCSArrayNdxDelete(CCS_KEY   *prpKeyBase, 
                       const long lpArrayIndex)
{
long llLoop;

   for (llLoop=0 ; llLoop < prpKeyBase->ArrBase->Count; llLoop++)
   {
      if (prpKeyBase->Keys[llLoop].ArrayIndex == lpArrayIndex)
      {
         free(prpKeyBase->Keys[llLoop].Key);
         if (prpKeyBase->ArrBase->Count-1 > llLoop)
         {
           memmove(&prpKeyBase->Keys[llLoop], 
                 &prpKeyBase->Keys[llLoop+1],  
                 (prpKeyBase->ArrBase->Count - llLoop -1) * sizeof (KEY_RECORD));
         }
         prpKeyBase->Keys[prpKeyBase->ArrBase->Count-1].ArrayIndex= -1;
         prpKeyBase->Keys[prpKeyBase->ArrBase->Count-1].Key= NULL;
         if ((llLoop<=prpKeyBase->NextIndex) && (prpKeyBase->NextIndex>-1))
            prpKeyBase->NextIndex--;     /* adjust next index to process */
         if ((llLoop<=prpKeyBase->SearchRange1) && (prpKeyBase->SearchRange1>-1))
            prpKeyBase->SearchRange1--;  /* adjust first index to process */
         if ((llLoop<=prpKeyBase->SearchRange2) && (prpKeyBase->SearchRange2>-1))
            prpKeyBase->SearchRange2--;  /* adjust last index to process */
      }
      if (prpKeyBase->Keys[llLoop].ArrayIndex > lpArrayIndex)
         prpKeyBase->Keys[llLoop].ArrayIndex--;
   }

   if (prpKeyBase->NextKey != NULL)
         CCSArrayNdxDelete(prpKeyBase->NextKey, lpArrayIndex);
} /* CCSArrayNdxDelete */ 


/* =================================================================== 
   Procedure to locate and delete an element from the array
*/
void CCSArrayKeyDelDat(CCS_KEY *prpKeyBase,
                       long lpArrayIndex)
{
void* pvlData;

      pvlData= prpKeyBase->ArrBase->CcsBase[lpArrayIndex].DataPtr;
      if (prpKeyBase->ArrBase->Count-1 > lpArrayIndex)
      {
         memmove(&prpKeyBase->ArrBase->CcsBase[lpArrayIndex], 
                &prpKeyBase->ArrBase->CcsBase[lpArrayIndex+1],  
               (prpKeyBase->ArrBase->Count - lpArrayIndex -1) * sizeof (DATA_RECORD));
      }
      prpKeyBase->ArrBase->CcsBase[prpKeyBase->ArrBase->Count-1].DataLen= 0;
      prpKeyBase->ArrBase->CcsBase[prpKeyBase->ArrBase->Count-1].DataPtr= NULL;
      free(pvlData);

      /* now delete the key entry in the keys: */
      CCSArrayNdxDelete(prpKeyBase->ArrBase->FirstKey,lpArrayIndex);
      prpKeyBase->ArrBase->Count--;

}/* CCSArrayKeyDelDat */


/* =================================================================== 
   Procedure to locate and delete an element from the array
*/
extern int CCSArrayDelete(CCS_KEY    *prpKeyBase,
                          const void *prpDelkey)
{
long llKeyIndex;
int  ilSearchRC;
long llArrayIndex;

   if (prpKeyBase->ArrBase->Count==0)
      return (CCS_NOT_FOUND);

   llKeyIndex= CCSArraySearchNextTo(prpKeyBase,prpDelkey,&ilSearchRC);
   if (ilSearchRC==0)
   {
      llArrayIndex= prpKeyBase->Keys[llKeyIndex].ArrayIndex;
	  CCSArrayKeyDelDat(prpKeyBase,llArrayIndex);
      return (CCS_SUCCESS);
   }
   else
   return (CCS_NOT_FOUND);
}/* CCSArrayDelete */


/* =================================================================== 
   Find the key element key==rpSearchKey and return the data pointer 
*/
extern void *CCSArrayGetData(CCS_KEY    *prpKeyBase,
                             const void *prpSearchKey)
{
long llKeyIndex;
int  ilSearchRC;

   if (prpKeyBase->ArrBase->Count==0)
      return (NULL);

   llKeyIndex= CCSArraySearchNextTo(prpKeyBase,prpSearchKey,&ilSearchRC);
   if (ilSearchRC==0)
   {
      return (prpKeyBase->ArrBase->CcsBase[prpKeyBase->Keys[llKeyIndex].ArrayIndex
                            ].DataPtr);
   }
   else
    return (NULL);
} /* CCSArrayGetData */


/* =================================================================== 
   Get first data element sorted by the key prpKeyBase.
*/
extern void *CCSArrayGetFirst(CCS_KEY *prpKeyBase)
{
 if (prpKeyBase->ArrBase->MustSortKey == 1) 
    CCSArrayOneKeySort(prpKeyBase);

 prpKeyBase->NextIndex= 0;
  if (prpKeyBase->NextIndex<prpKeyBase->ArrBase->Count)
  {
    return (prpKeyBase->ArrBase->CcsBase[prpKeyBase->Keys[prpKeyBase->NextIndex
                                              ].ArrayIndex
                        ].DataPtr);
  } else
  {
     return (NULL);
  }
} /* CCSArrayGetFirst */


/* =================================================================== 
   Get NextKey data element sorted by the key prpKeyBase.
*/
extern void *CCSArrayGetNext(CCS_KEY *prpKeyBase)
{

  if (prpKeyBase->ArrBase->MustSortKey == 1) 
     CCSArrayOneKeySort(prpKeyBase);

  if (prpKeyBase->NextIndex!=-1)
     prpKeyBase->NextIndex++;
  else
     prpKeyBase->NextIndex=0;

  if (prpKeyBase->NextIndex<prpKeyBase->ArrBase->Count)
  {
    return (prpKeyBase->ArrBase->CcsBase[prpKeyBase->Keys[prpKeyBase->NextIndex
                                              ].ArrayIndex
                        ].DataPtr);
  } else
  {
     return (NULL);
  }
} /* CCSArrayGetNext */


/* =================================================================== 
   Get first data element sorted by the key prpKeyBase within a range 
   defined with the two parameter "vpKey1" and "vpKey2".
*/
extern void *CCSArrayGetFirstIn(CCS_KEY    *prpKeyBase,
                                const void *pvpKey1,
                                const void *pvpKey2)
{
int ipSearchRC;
long llNdx;

  prpKeyBase->SearchRange1= -1;
  prpKeyBase->SearchRange2= -1;
  memcpy(prpKeyBase->RangeKey1,pvpKey1,prpKeyBase->KeyLen);
  memcpy(prpKeyBase->RangeKey2,pvpKey2,prpKeyBase->KeyLen);
  
  if (prpKeyBase->ArrBase->Count==0)
     return (NULL);

  /* find one occurence of key 1 with binary search: */
  llNdx= CCSArraySearchNextTo(prpKeyBase,pvpKey1,&ipSearchRC);
  /* test if  found and unique */
  if (ipSearchRC==1)
  {   /* the last entry tested is less than the left range key */
     llNdx++; /* we don't want to see this entry */
  } else if (ipSearchRC==0)
  {   /* the last entry tested is equal the left range key */
     while ((llNdx>0) && (ipSearchRC==0))
     {  /* if there are equal keys, walk to the first key entry */
       llNdx--;
       ipSearchRC= (*prpKeyBase->Compare)(prpKeyBase->UserInfo,pvpKey1,prpKeyBase->Keys[llNdx].Key);
     }
     if (ipSearchRC!=0)
       llNdx++; /* if we are not at first entry .... */
  } else {
     ;
  }
  prpKeyBase->SearchRange1= llNdx;

  /* find one occurence of key 2 with binary search: */
  llNdx= CCSArraySearchNextTo(prpKeyBase,pvpKey2,&ipSearchRC);
  /* test if  found and unique */
  if (ipSearchRC==-1)
  {   /* the last entry tested is less than the left range key */
     llNdx--; /* we don't want to see this entry */
  } else if (ipSearchRC==0)
  {   /* the last entry tested is equal the left range key */
     while ((llNdx<prpKeyBase->ArrBase->Count-1) && (ipSearchRC==0))
     {  /* if there are equal keys, walk to the last key entry */
       llNdx++;
       ipSearchRC= (*prpKeyBase->Compare)(prpKeyBase->UserInfo,pvpKey2,prpKeyBase->Keys[llNdx].Key);
     }
     if (ipSearchRC!=0)
       llNdx--; /* if we are not at last entry .... */
  } else {
     ;
  }
  prpKeyBase->SearchRange2= llNdx;

  prpKeyBase->NextIndex= prpKeyBase->SearchRange1;
  if (prpKeyBase->NextIndex <= prpKeyBase->SearchRange2)
    return (prpKeyBase->ArrBase->CcsBase[prpKeyBase->Keys[prpKeyBase->NextIndex
                                        ].ArrayIndex
                     ].DataPtr);
  else
    return (NULL);
} /* CCSArrayGetFirstIn */


/* =================================================================== 
   Get first data element sorted by the key prpKeyBase within a range 
   defined with the two parameter "vpKey1" and "vpKey2".
*/
extern void *CCSArrayGetNextIn(CCS_KEY    *prpKeyBase,
                               const void *pvpKey1,
                               const void *pvpKey2)
{


  if (((*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->RangeKey1,pvpKey1)!=0)
   || ((*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->RangeKey2,pvpKey2)!=0))
  {  
      return(CCSArrayGetFirstIn(prpKeyBase, pvpKey1, pvpKey2));
  }

  prpKeyBase->NextIndex++;
  if (prpKeyBase->NextIndex <= prpKeyBase->SearchRange2)
    return (prpKeyBase->ArrBase->CcsBase[prpKeyBase->Keys[prpKeyBase->NextIndex
                                          ].ArrayIndex
                       ].DataPtr);
  else
    return (NULL);
} /* CCSArrayGetNextIn */


/* =================================================================== 
   Destroy one key of the array
*/
void CCSArrayKeyDestroy(CCS_KEY **prpKeyBase)
{
CCS_KEY *prlTmpBase1,*prlTmpBase2,*prlTmpBase3;
long llLoop;

   for (llLoop= 0; llLoop < (*prpKeyBase)->ArrBase->Count; llLoop++)
      free((*prpKeyBase)->Keys[llLoop].Key);
#ifdef TEST_ARRAY
   xxx_igNumOfKeys--;
#else /*TEST_ARRAY*/
   free((*prpKeyBase)->Keys);         (*prpKeyBase)->Keys=       NULL;
#endif /*TEST_ARRAY*/
   free((*prpKeyBase)->TmpKey);       (*prpKeyBase)->TmpKey=     NULL;
   free((*prpKeyBase)->RangeKey1);    (*prpKeyBase)->RangeKey1=  NULL;
   free((*prpKeyBase)->RangeKey2);    (*prpKeyBase)->RangeKey2=  NULL;
   prlTmpBase1= (*prpKeyBase)->NextKey;  /* now remove key pointer from list */
   prlTmpBase2= (*prpKeyBase)->ArrBase->FirstKey;  
   prlTmpBase3= *prpKeyBase;             /* to free the key list */
   if ((*prpKeyBase)->ArrBase->FirstKey==*prpKeyBase)
      (*prpKeyBase)->ArrBase->FirstKey= prlTmpBase1;
   else
   {
      while ((prlTmpBase2->NextKey!=NULL) && (prlTmpBase2->NextKey!=*prpKeyBase))
         prlTmpBase2= prlTmpBase2->NextKey;
      if     (prlTmpBase2->NextKey!=NULL)
         prlTmpBase2->NextKey= prlTmpBase1;
   }

   free(prlTmpBase3);          /* ->NextKey will be NULL and then the */
   *prpKeyBase= NULL;          /* master key will be set to NULL*/

} /* CCSArrayKeyDestroy */


/* =================================================================== 
   Destroy the complete array
*/
extern void CCSArrayArrDestroy(CCS_KEY **prpKeyBase)
{
CCS_KEY *prlTmpBase;
long llLoop;
CCS_ARR    *prlArr;
  prlArr= (*prpKeyBase)->ArrBase;

      for (llLoop= 0; llLoop < prlArr->Count; llLoop++)
         free(prlArr->CcsBase[llLoop].DataPtr);
      free(prlArr->CcsBase);        prlArr->CcsBase=      NULL;
      while (prlArr->FirstKey!=NULL)
      {  prlTmpBase= prlArr->FirstKey;
          CCSArrayKeyDestroy(&prlTmpBase);
      }
	  free(prlArr);
      *prpKeyBase= NULL;

} /* CCSArrayDestroy */


/* ===================================================================  
   Routine to get the number of elements in this array
*/
extern long CCSArrayGetCount(CCS_KEY *prpKeyBase)
{
   return prpKeyBase->ArrBase->Count; 
} /* CCSArrayGetCount */

/* =================================================================== */

/* =================================================================== 
   Get first data element of Array, unsorted
*/
extern void *CCSArrayArrFirst(CCS_KEY *prpKeyBase)
{
CCS_ARR    *prlArr;
  prlArr= prpKeyBase->ArrBase;

  prlArr->NextIndexA= 0;
  if (prlArr->NextIndexA<prlArr->Count)
  {
    return (prlArr->CcsBase[prlArr->NextIndexA].DataPtr);
  } else
  {
     return (NULL);
  }
} /* CCSArrayArrFirst */


/* =================================================================== 
   Get next data element of Array, unsorted
*/
extern void *CCSArrayArrNext(CCS_KEY *prpKeyBase)
{
CCS_ARR    *prlArr;
  prlArr= prpKeyBase->ArrBase;

  if (prlArr->NextIndexA!=-1)
     prlArr->NextIndexA++;
  else
     prlArr->NextIndexA=0;

  if (prlArr->NextIndexA<prlArr->Count)
  {
    return (prlArr->CcsBase[prlArr->NextIndexA].DataPtr);
  } else
  {
     return (NULL);
  }
} /* CCSArrayArrNext */


/* ===================================================================  
   Test the order of the array elements with the user defined compare 
   operation 
*/
extern int CCSArrayTestOrder(CCS_KEY    *prpKeyBase)
{
long llLoop;

  if (prpKeyBase->ArrBase->Count<2)
     return (CCS_SUCCESS); /* no errors in array */

  for (llLoop= 0; llLoop < prpKeyBase->ArrBase->Count-1; llLoop++)
  {
     if ((*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->Keys[llLoop].Key,
                            prpKeyBase->Keys[llLoop+1].Key)>0)
         return (CCS_ERR_ORDER); /* disordered */
  }

  return (CCS_SUCCESS); /* no errors in array */
} /* CCSArrayTestOrder */


/* ===================================================================  
   Test if the keys have lost the info. It uses the user defined compare 
   and key-of-data operation 
*/
extern int CCSArrayTestKeys(CCS_KEY    *prpKeyBase)
{
long llLoop;

  if (prpKeyBase->ArrBase->Count==0)
     return (CCS_SUCCESS); /* no errors in array */

  for (llLoop= 0; llLoop < prpKeyBase->ArrBase->Count; llLoop++)
  {
     if ((*prpKeyBase->Compare)(prpKeyBase->UserInfo,prpKeyBase->Keys[llLoop].Key,
         (*prpKeyBase->KeyOfData)(prpKeyBase->UserInfo,prpKeyBase->ArrBase->CcsBase[prpKeyBase->Keys[llLoop
                                                 ].ArrayIndex
                                                ].DataPtr))!=0)
         return (CCS_ERR_ORDER); /* disordered */
  }

  return (CCS_SUCCESS); /* no errors in array */
} /* CCSArrayTestKeys */


/* =================================================================== 
   The routines "CCSTabKeyOfData/A/B/C" are a routines to generate a key 
   from the data *DataPtr. 
   The routine "CCSTabKeyOfDataA" extracts the field number for this key 
   from prpKeyDescr, moves a pointer to the field with this field number,
   adds the beginning of the key offset in the field to the pointer and
   stores this pointer in the static pointer pcgMyKeyOfDataRC. 
   !!! This pointer must be read immediatly, because a following call to
   !!! this routine will destroy the info in this pointer !!!
   The routine returns a void pointer. All library functions, which are 
   using this routine, will copy the information stored in this area to 
   a save place (and perhaps allocate memory to store this data).
   The routine "CCSTabKeyOfDataA" does all this completely, 
   the routine "CCSTabKeyOfDataB" skips the step "adding the offset"
   in the key. (see "CCSTabInitKey")

   The library doesn't know the size of the structure KEY_TYPE, so, to
   copy the right amount of data, the size of the key must be set by
   calling "CCSTabInitKey".

   If you write your own "KeyOfData" routine, the address of the routines 
   must be provided to the library function "CCSTabInitKey" as shown 
   below.
*/
extern void* CCSTabKeyOfDataA(CEDA_KDESC *prpKeyDescr, char *prpDataPtr) 
{ 
static void *pcgMyKeyOfDataRC;
char *pclTmp;
unsigned int ilNum;

   pclTmp= prpDataPtr; /* set to start of data */ 
   for (ilNum= 0; ilNum < prpKeyDescr->FieldNum; ilNum++)
      pclTmp=  &(pclTmp)[strlen(pclTmp)+1];
   pcgMyKeyOfDataRC= &pclTmp[prpKeyDescr->Begin];
/*        dbg(DEBUG,"CCSTabKeyOfData: %s",pcgMyKeyOfDataRC); */
   return (&pcgMyKeyOfDataRC);
}

extern void* CCSTabKeyOfDataB(CEDA_KDESC *prpKeyDescr, char *prpDataPtr) 
{ 
static void *pcgMyKeyOfDataRC;
char *pclTmp;
unsigned int ilNum;

   pclTmp= prpDataPtr; /* set to start of data */ 
   for (ilNum= 0; ilNum < prpKeyDescr->FieldNum; ilNum++)
      pclTmp=  &(pclTmp)[strlen(pclTmp)+1];
   pcgMyKeyOfDataRC= pclTmp;
/*  //   dbg(DEBUG,"CCSTabKeyOfData: %s",pcgMyKeyOfDataRC); */
   return (&pcgMyKeyOfDataRC);
}

extern void* CCSTabKeyOfDataC(CEDA_KDESC *prpKeyDescr, char *prpDataPtr) 
{ 
static long llMyKeyOfDataRC;
char *pclTmp;
unsigned int ilNum;

   pclTmp= prpDataPtr; /* set to start of data */ 
   for (ilNum= 0; ilNum < prpKeyDescr->FieldNum; ilNum++)
      pclTmp=  &(pclTmp)[strlen(pclTmp)+1];
   llMyKeyOfDataRC= atol(pclTmp);
/*  //   dbg(DEBUG,"CCSTabKeyOfData: %ld",llMyKeyOfDataRC); */
   return (&llMyKeyOfDataRC);
}

void* CCSCedaKeyOfData(CEDA_KDESC *prpKeyDescr, char *prpDataPtr) 
{ 
static void *pcgMyKeyOfDataRC;

   pcgMyKeyOfDataRC= prpDataPtr;
/*  //   dbg(DEBUG,"CCSTabKeyOfData: %s",pcgMyKeyOfDataRC); */
   return (pcgMyKeyOfDataRC);
}

/* ================================================================== 
   The routines "CCSTabCompare/A/B" are routines to make the CEDA data 
   sortable by the key. 
   The routine "CCSTabCompareA" does a strncmp() with the keys,
   the routine "CCSTabCompareB" does a strcmp() with the keys.
   In case of the routine "CCSTabCompareA", the length of the
   compare will not exceed the length defined in prpKeyDescr->Len.

   The last two arguments to this routine are pointers to structures 
   of KEY_TYPE, which are casted to 'void*'. So the library doesn't 
   need to know the structure of KEY_TYPE.

   If you write your own "Compare" routine, the address of the routine 
   must be provided to the library function "CCSTabInitKey" as 
   shown below. 
*/
int CCSTabCompareA(CEDA_KDESC *prpKeyDescr, const KEY_TYPE a, const KEY_TYPE b)
{ 
/* this is a simple definition of the compare operator. It compares
   the strings a and b character by character. The number of 
   characters which are compared is given by the minimum of the key
   length and the length of one of the strings. The pointer u may 
   contain further info than the length of the key.
*/
/*  //   dbg(DEBUG,"Cmp: %s / %s: %d",*a,*b, */
/*  //       strncmp(*a,*b,prpKeyDescr->Len)); */
   return (strncmp(*a,*b,prpKeyDescr->Len));
}

int CCSTabCompareB(CEDA_KDESC *prpKeyDescr, const KEY_TYPE a, const KEY_TYPE b)
{ 
/* this is a simple definition of the compare operator. It compares
   the strings a and b character by character. The number of 
   characters which are compared is given by the minimum of the key
   length and the length of one of the strings. The pointer u may 
   contain further info than the length of the key.
*/
/*  //   dbg(DEBUG,"Cmp: %s / %s: %d",*a,*b,strcmp(*a,*b); */
   return (strcmp(*a,*b));
}

int CCSTabCompareC(CEDA_KDESC *prpKeyDescr, long *a, long *b)
{ 
   int ilRC= 0;
/* this is a simple definition of the compare operator. It compares
   the strings a and b character by character. The number of 
   characters which are compared is given by the minimum of the key
   length and the length of one of the strings. The pointer u may 
   contain further info than the length of the key.
*/
   if (*a>*b)
      ilRC= 1; 
   else if (*a<*b)
      ilRC= -1; 

/*  //   dbg(DEBUG,"Cmp: %ld / %ld: %d",*a,*b,ilRC); */
   return (ilRC); 

}

int CCSCedaCompare(CEDA_KDESC *prpKeyDescr, const char* a, const char* b)
{ 
/* this is a simple definition of the compare operator. It compares
   the strings a and b character by character. The number of 
   characters which are compared is given by the minimum of the key
   length and the length of one of the strings. The pointer u may 
   contain further info than the length of the key.
*/
/*  //   dbg(DEBUG,"Cmp: %s / %s: %d",*a,*b,strcmp(*a,*b); */
   return (strcmp(a,b));
}

/* ================================================================== 
   The routine "CCSTabRecLen" returns the total length of a record,
   which is seperated in strings by a '\0'.
   The number of strings mut be provided as ipNumOfFields
*/
int CCSTabRecLen(const char *prpDataPtr, unsigned int ipNumOfFields)
{
char *pclTmp;
unsigned int ilNum;
unsigned int ilCount= 0;

   pclTmp= (char*) prpDataPtr; /* set to start of data */ 
   for (ilNum= 0; ilNum < ipNumOfFields; ilNum++)
   {
      ilCount+= strlen(pclTmp)+1;
      pclTmp=  &(pclTmp)[strlen(pclTmp)+1];
   }

   return (ilCount);
}

/* ================================================================== 
   The routine "CCSTabAddToList" manages a list of index numbers to
   the array records of prpKeyBase. The list is build to suffer a
   comma ',' seperated list of key expressions like 
      "URNO_1>       0,TIFA_1<199901030000"
   The comma ',' is used as a AND relation. Other relations are not defined. 
   In the first call, when the list doesn't exists, it is created and 
   filled with the index numbers from RangeBeg1 to RangeEnd1 and from 
   RangeBeg2 to RangeEnd2, i.e. of "URNO_1...".
   In the following calls, the members of this list, which are not
   in the (new) range RangeBeg1 to RangeEnd1 and RangeBeg2 to 
   RangeEnd2 (i.e. "TIFA_1..."), are deleted from the list.
   (The calling routine must delete the resulting list.)
*/
int CCSTabAddToList(long **plpList,CCS_KEY *prpKeyBase, long *plpCount)
{
   long llCountNew, llLoop1, llLoop2;
   int ilFound;
   llCountNew= prpKeyBase->RangeEnd1-prpKeyBase->RangeBeg1+1;
   if (prpKeyBase->RangeEnd2>=prpKeyBase->RangeBeg2)
      llCountNew+= prpKeyBase->RangeEnd2-prpKeyBase->RangeBeg2+1;
   if (llCountNew<=0)
   {
      plpCount= 0;
      return (CCS_SUCCESS);
   }
   if (*plpList==NULL)
   {  /* create a new list and insert the array indices of the first range definition */
      (*plpCount)= 0;
      (*plpList)= malloc(llCountNew*sizeof(long));
      if ((*plpList)==NULL)
         return (CCS_FAIL);
      for (llLoop1=prpKeyBase->RangeBeg1; llLoop1<=prpKeyBase->RangeEnd1;llLoop1++)
      {
         (*plpList)[(*plpCount)++]= prpKeyBase->Keys[llLoop1].ArrayIndex;
      }
      for (llLoop1=prpKeyBase->RangeBeg2; llLoop1<=prpKeyBase->RangeEnd2;llLoop1++)
      {
         (*plpList)[(*plpCount)++]= prpKeyBase->Keys[llLoop1].ArrayIndex;
      }
      return (CCS_SUCCESS);
   } else
   {  /* delete members from list, if not in new range */
      for (llLoop2=0; llLoop2<*plpCount;llLoop2++)
      {
         ilFound= 0;
         for (llLoop1=prpKeyBase->RangeBeg1; llLoop1<=prpKeyBase->RangeEnd1;llLoop1++)
         {
            if ((*plpList)[llLoop2]== prpKeyBase->Keys[llLoop1].ArrayIndex)
            {
               ilFound= 1;
               llLoop1= prpKeyBase->RangeEnd1+1; /* to leave inner loop */
            }
         }
         for (llLoop1=prpKeyBase->RangeBeg2; llLoop1<=prpKeyBase->RangeEnd2;llLoop1++)
         {
            if ((*plpList)[llLoop2]== prpKeyBase->Keys[llLoop1].ArrayIndex)
            {
               ilFound= 1;
               llLoop1= prpKeyBase->RangeEnd2+1; /* to leave inner loop */
            }
         }
         if (ilFound==0)
         {  /* member not in new range definitions, so remove it */
            memmove(&(*plpList)[llLoop2],&(*plpList)[llLoop2+1],(*plpCount-llLoop2+1)*sizeof(long));
            (*plpCount)--;
            llLoop2--;
         }
      }
      return (CCS_SUCCESS);
   }
}

/* ================================================================== 
   The routine "CCSTabSubIndexList" evaluates the range of array records,
   which are covered by the value of pcpSubKey, and sets the variables
   RangeBeg1 and RangeEnd1 according to the operator stored in
   pcpSubKey[CEDA_SYMBOL_LEN]. 
   In case the operator equals to '#', the variables RangeBeg2 to RangeEnd2 
   are also set. 
*/
int CCSTabSubIndexList(CEDA_COMBI_KEY *prpCedaKey, 
                        char    *pcpSubKey,
                        long   **plpList,
                        long    *plpCount)
{
CCS_KEY *prlKeyBase;
void *pvlDummy;
void *pvlDummy2;
int ilRC= CCS_SUCCESS;
long llNumVal=0;

   prlKeyBase= prpCedaKey->Key;
   prlKeyBase->RangeBeg1= 0;
   prlKeyBase->RangeEnd1= prlKeyBase->ArrBase->Count-1;
   prlKeyBase->RangeBeg2= -1;
   prlKeyBase->RangeEnd2= -2;
   pvlDummy2= &pcpSubKey[CEDA_SYMBOL_LEN+1];  /* default: compare &str */
   if (prpCedaKey->KeyDescr.FieldType==3)      /* compare &long */
   {
      llNumVal= atoi(&pcpSubKey[CEDA_SYMBOL_LEN+1]);
      pvlDummy2= (void*) llNumVal;
   }

   pvlDummy= CCSArrayGetFirstIn(prlKeyBase,
                              &pvlDummy2,
                              &pvlDummy2);
   switch (pcpSubKey[CEDA_SYMBOL_LEN])
   {
   case '<': 
      prlKeyBase->RangeBeg1= 0;
      prlKeyBase->RangeEnd1= prlKeyBase->SearchRange1;
      if (prlKeyBase->SearchRange1>=prlKeyBase->SearchRange2)
      prlKeyBase->RangeEnd1--;
      ilRC= CCSTabAddToList(plpList,prlKeyBase,plpCount);
      break;
   case '>':
      prlKeyBase->RangeBeg1= prlKeyBase->SearchRange2;
      if (prlKeyBase->SearchRange2<=prlKeyBase->SearchRange1)
      prlKeyBase->RangeBeg1++;
      prlKeyBase->RangeEnd1= prlKeyBase->ArrBase->Count-1;
      ilRC= CCSTabAddToList(plpList,prlKeyBase,plpCount);
      break;
   case '=':
      prlKeyBase->RangeBeg1= prlKeyBase->SearchRange1;
      prlKeyBase->RangeEnd1= prlKeyBase->SearchRange2;
      ilRC= CCSTabAddToList(plpList,prlKeyBase,plpCount);
      break;
   case '#':
      if (prlKeyBase->SearchRange2>=prlKeyBase->SearchRange1)
      {  /* indeed there are records with the subkey */
         prlKeyBase->RangeBeg1= 0;
         prlKeyBase->RangeEnd1= prlKeyBase->SearchRange1-1;
         prlKeyBase->RangeBeg2= prlKeyBase->SearchRange2+1;
         prlKeyBase->RangeEnd2= prlKeyBase->ArrBase->Count-1;
         ilRC= CCSTabAddToList(plpList,prlKeyBase,plpCount);
      }
      else
         ; /* nothing to do */
      break;
   default:
      ilRC= CCS_ERR_OP_UNKNOWN;
      break;
   }
   prlKeyBase->RangeBeg1= 0;
   prlKeyBase->RangeEnd1= prlKeyBase->ArrBase->Count-1;

   return(ilRC);
}


/* ================================================================== 
   The routine "CCSTabGetIndexList" builds a list of array indices
   defined by a list of key expressions like 
      "URNO_1>       0,TIFA_1<199901030000"
   The ',' is is used as a AND relation. Other relations are not defined. 
*/
int CCSTabGetIndexList(CEDA_ARRAY *prpCedaArr, 
                        char       *pcpCombiKey,
                        long      **plpList,
                        long       *plpCount)
{
CEDA_COMBI_KEY   *prlTmpKey;
char *pclStr;
int   ilTotalKeyLen,ilOffsetInKey;
long llLoop= 0;
int ilRC= CCS_SUCCESS;

   pclStr= pcpCombiKey;
   ilTotalKeyLen=strlen(pcpCombiKey);
   ilOffsetInKey= 0;
   while (pclStr!=NULL)
   {  /* locate key record for key expression */
      prlTmpKey= prpCedaArr->FirstCombiKey;
      while ((prlTmpKey!=NULL) &&
            (strncmp(prlTmpKey->KeySym,pclStr,CEDA_SYMBOL_LEN)!=0))
      {
         prlTmpKey= prlTmpKey->Next;
      }
      if (prlTmpKey!=NULL)
      {  /* ==> strncmp()!=0 */
         /* build list or check, if elements remain in list */
         pclStr= strtok(&pcpCombiKey[ilOffsetInKey],",");
         ilOffsetInKey+= strlen(pclStr) + 1;
         ilRC= CCSTabSubIndexList(prlTmpKey,pclStr,plpList, plpCount);
         if (ilRC!=CCS_SUCCESS)
            return(ilRC);
         if (ilOffsetInKey<ilTotalKeyLen)
         {  /* reinsert ',' deleted by strtok() */
            pcpCombiKey[ilOffsetInKey-1]=',';
         }
      } else
      {   /* ==> prlTmpKey!=NULL   */
/*  //    Hier muss man den Schl�ssel aufloesen und das Array Satz f�r Satz bearbeiten */
         dbg(TRACE,"+++ CCSArray: key not found: '%6s'",&pcpCombiKey[ilOffsetInKey]);
      }
      /* continue with next key */
      pclStr= strchr(pclStr,',');
      if (pclStr!=NULL)
          pclStr++;
   }/* while (pclStr!=NULL) */
    
}


/* ================================================================== 
   The routine "CCSTabGetData" buidls a list of the elements from the 
   data array. The elements to insert to the list are defined by a list 
   of key expressions like 
      "URNO_1>       0,TIFA_1<199901030000"
   The ',' is is used as a AND relation. Other relations Symbols are not defined. 
   The first parameter is a pointer to the array base.
*/
extern int CCSTabGetData(CEDA_ARRAY *prpCedaArr, 
                         char       *pcpCombiKey,
                         USR_ARRAY  *prpDataPtr)
{
char NameOfSub[]="CCSTabGetData";
long *pllList=NULL;
long llLoop= 0;
int ilRC= CCS_SUCCESS;
long ilLen;
char *pclChr;
/*  //int dbgswitch= 0; */

   dbg(DEBUG,"<%s>: start",NameOfSub);

   (*prpDataPtr).Count= 0;
   (*prpDataPtr).Type=  1;  /* do not allocate / free for each record */
                            /* ==> copy pointer, not data */
   (*prpDataPtr).UsrBase= NULL;

   ilRC= CCSTabGetIndexList(prpCedaArr,pcpCombiKey,&pllList,&(*prpDataPtr).Count);
   if (ilRC!=CCS_SUCCESS)
      return(ilRC);
   if ((*prpDataPtr).Count<=0)
      return(CCS_SUCCESS);

   (*prpDataPtr).UsrBase= malloc((*prpDataPtr).Count* sizeof(void*));
   if ((*prpDataPtr).UsrBase==NULL)
   {
      free (pllList);
      pllList= NULL;
      (*prpDataPtr).Count= 0;
      return (CCS_ERR_NO_MEM);
   }
   else
   {
      if ((*prpDataPtr).Type==0)  /* do not allocate / free for each record */
         for (llLoop= 0; llLoop<(*prpDataPtr).Count; llLoop++)
         {
            (*prpDataPtr).UsrBase[llLoop]= prpCedaArr->CedaBase->CcsBase[pllList[llLoop]].DataPtr;
         }
      else
      {
/*  //         dbg(DEBUG,"<%s>: allocating....",NameOfSub); */
         for (llLoop= 0; llLoop<(*prpDataPtr).Count; llLoop++)
         {
            ilLen= prpCedaArr->CedaBase->CcsBase[pllList[llLoop]].DataLen;
/*  //            dbg(DEBUG,"<%s>: record %ld, Len: %d",NameOfSub,llLoop,ilLen); */
            (*prpDataPtr).UsrBase[llLoop]= malloc(ilLen);
            if ((*prpDataPtr).UsrBase[llLoop]==NULL)
            {
               dbg(TRACE,"<%s>: memory exceeded, record %ld",NameOfSub,llLoop);
               while (llLoop>= 0)
               {
                  free((*prpDataPtr).UsrBase[llLoop]);
                  llLoop--;
               }
               ilRC= CCS_ERR_NO_MEM;
               break; /* for llLoop*/
            }
            else
            {
/*  //               dbg(DEBUG,"<%s>: copying....",NameOfSub); */
/*  //               memcpy(&(*prpDataPtr).UsrBase[llLoop],prpCedaArr->CedaBase->CcsBase[pllList[llLoop]].DataPtr,ilLen); */
               memcpy((*prpDataPtr).UsrBase[llLoop],prpCedaArr->CedaBase->CcsBase[pllList[llLoop]].DataPtr,ilLen);
/*             dbg(DEBUG,"<%s>: building debug string....",NameOfSub);
               pclChr= (char*) &(*prpDataPtr).UsrBase[llLoop];
               strcpy(pclLine,"");
               for (ilNum=0; ilNum<prpCedaArr->NumOfFields; ilNum++)
               {
                  dbg(DEBUG,"<%s>: ..'%s'..",NameOfSub,pclChr);
                  strcpy(pclCurrFld,pclChr);
                  pclChr= &pclChr[strlen(pclChr)+1];
                  dbg(DEBUG,"<%s>: strcpy returned '%s'",NameOfSub,pclCurrFld);
                  strcat(pclLine,pclCurrFld);
                  strcat(pclLine,"#");
               }
               dbg(DEBUG,"<%s>: pclLine: %s",NameOfSub,pclLine);
*/
               pclChr= (char*) &(*prpDataPtr).UsrBase[llLoop];
/* +++***+++ * while (ilLen>1)  */
               {
/*  //                  if (dbgswitch==0) */
/*  //                   dbg(DEBUG,"<%s>: chr: %c, %d",NameOfSub,*pclChr,(int)pclChr[0]); */
                  if (*pclChr==0)
                     *pclChr= ',';
                  pclChr++;
                  ilLen--;
               }
/*  //               dbgswitch= 1; */
/*  //               dbg(DEBUG,"<%s>: converted....",NameOfSub); */
               dbg(DEBUG,"<%s>: UsrData: '%s'",NameOfSub,(char*) &(*prpDataPtr).UsrBase[llLoop]);
            }
         }
      }
      free (pllList);
      pllList= NULL;
      dbg(DEBUG,"<%s>: finished",NameOfSub);
      return (CCS_SUCCESS);
   }
}


/* ================================================================== 
   The routine "CCSCedaDelUsr" deletes a CEDA user data array.
   This includes the data, when the data was stored as copy. 
   If the user made copies to the data pointers, this copies 
   get invalid!
*/
extern void CCSCedaDelUsr(USR_ARRAY *prpDataPtr)
{
long llLoop= 0;

   if ((*prpDataPtr).Type!=0)
   {  /* The data pointers are pointers to a (sub) copy of the array data. */
      /* So, every data record must be freed: */
      for (llLoop= 0; llLoop<(*prpDataPtr).Count; llLoop++)
      {
         free((*prpDataPtr).UsrBase[llLoop]);
         (*prpDataPtr).UsrBase[llLoop]= NULL;
      }
   }
   else 
      ; /* do not allocate / free for each record */

   /* And now free the array of pointers: */
   free ((*prpDataPtr).UsrBase);
   (*prpDataPtr).UsrBase= NULL;
   (*prpDataPtr).Count= 0;
}


/* ================================================================== 
   The routine "CCSTabDelete" deletes elements from the array. The elements to 
   delete are defined by a list of key expressions like 
      "URNO_1>       0,TIFA_1<199901030000"
   The ',' is is used as a AND relation. Other relations are not defined. 
   The first parameter is a pointer to the array base.
*/
extern int CCSTabDelete(CEDA_ARRAY *prpCedaArr, 
                         char *pcpCombiKey)
{
long *pllList=NULL;
long llCount= 0;
long llLoop1, llLoop2;
int ilRC= CCS_SUCCESS;

   ilRC= CCSTabGetIndexList(prpCedaArr,pcpCombiKey,&pllList,&llCount);
   if (ilRC!=CCS_SUCCESS)
      return(ilRC);
   if (llCount<=0)
      return (CCS_NOT_FOUND);

   for (llLoop1= 0; llLoop1<llCount; llLoop1++)
   {
      CCSArrayKeyDelDat(prpCedaArr->CedaBase->FirstKey,pllList[llLoop1]);
      for (llLoop2= llLoop1+1; llLoop2<llCount; llLoop2++)
         if(pllList[llLoop2]>pllList[llLoop1])
            pllList[llLoop2]--;
   }
   free (pllList);
   return (CCS_SUCCESS);
}


/* ================================================================== 
   The routine "CCSTabTestKeys" tests, if the keys have lost the info.
   By using the the routine ...TestKeys, it uses the user defined compare 
   and key-of-data operation. 
   The first parameter is a pointer to the array base.
*/
extern int CCSTabTestKeys(CEDA_ARRAY *prpCedaArr)
{
CEDA_COMBI_KEY   *prlTmpKey;
int ilRC= CCS_SUCCESS;

   prlTmpKey= prpCedaArr->FirstCombiKey;
   while (prlTmpKey!=NULL)
   {
      if ((ilRC=CCSArrayTestKeys(prlTmpKey->Key))!=CCS_SUCCESS)
         return (ilRC);
      prlTmpKey= prlTmpKey->Next;
   }
   return(CCS_SUCCESS);
}


/* ================================================================== 
   The routine "CCSTabTestOrder" tests, if the sort order of the data
   is compliing to compare function of the key.
   By using the the routine ...TestOrder, it uses the user defined compare 
   operation. 
   The first parameter is a pointer to the array base.
*/
extern int CCSTabTestOrder(CEDA_ARRAY *prpCedaArr)
{
CEDA_COMBI_KEY   *prlTmpKey;
int ilRC= CCS_SUCCESS;

   prlTmpKey= prpCedaArr->FirstCombiKey;
   while (prlTmpKey!=NULL)
   {
      if ((ilRC=CCSArrayTestOrder(prlTmpKey->Key))!=CCS_SUCCESS)
         return (ilRC);
      prlTmpKey= prlTmpKey->Next;
   }
   return(CCS_SUCCESS);
}


/* ================================================================== 
   The routine "CCSTabKeySort" sorts all keys connected to the CEDA array
   pointer 'prpCedaArr'
   The first parameter is a pointer to the array base.
*/
extern void CCSTabKeySort(CEDA_ARRAY *prpCedaArr)
{
CEDA_COMBI_KEY *prlTmpKey;

   prlTmpKey= prpCedaArr->FirstCombiKey;
   while (prlTmpKey!=NULL)
   {
      CCSArrayOneKeySort(prlTmpKey->Key);
      prlTmpKey= prlTmpKey->Next;
   }
}

/* ================================================================== 
   The routine "CCSTabGetCount" returns the number of elements of the
   CEDA array 'prpCedaArr'
   The first parameter is a pointer to the array base.
*/
extern long CCSTabGetCount(const CEDA_ARRAY *prpCedaArr)
{
   return(prpCedaArr->CedaBase->Count);
}


/* =================================================================== 
   The routine "CCSTabCreateFDesc" creates a record with field descriptions
*/
int CCSTabCreateFDesc(CEDA_FDESC **prpFieldDescr,
                      int         *ipNumOfFields,
                      char        *pcpFNames,
                      char        *pcpFiTyS, 
                      char        *pcpFeLeS, 
                      char        *pcpSystS, 
                      char        *pcpTypeS)
{
char NameOfSub[]="CCSTabCreateFDesc";

int   ilNum;
int   ilRC= RC_SUCCESS;
char  *pclFldPtr, *pclFiTyPtr, *pclFeLePtr, *pclSystPtr, *pclTypePtr;
char  pclFldBuf[20], 
      pclFiTyBuf[4], 
      pclFeLeBuf[9], 
      pclSystBuf[2], 
      pclTypeBuf[5];

   /* evaluate number of ',' seperated parts: */
   *ipNumOfFields= 0;
   *ipNumOfFields= itemCount(pcpFNames);
   if (*ipNumOfFields==0)
   {
      dbg(TRACE,"<%s>: itemCount() returned 0 args!",NameOfSub);
      ilRC= RC_FAIL;
      return(ilRC);
   }
   else if (*ipNumOfFields<0)
   {
      dbg(TRACE,"<%s>: itemCount() returned error #%d!",NameOfSub,*ipNumOfFields);
      ilRC= RC_FAIL;
      return(ilRC);
   }
   if ( (*ipNumOfFields != itemCount(pcpFNames)) ||
        (*ipNumOfFields != itemCount(pcpFiTyS))  ||
        (*ipNumOfFields != itemCount(pcpFeLeS))  || 
        (*ipNumOfFields != itemCount(pcpSystS))  ||
        (*ipNumOfFields != itemCount(pcpTypeS))
      )
   {
      dbg(TRACE,"<%s>: different number of items passed!",NameOfSub);
      ilRC= RC_FAIL;
      return(ilRC);
   }

   
   /* now we know, how many field descriptors we need.*/
   /* Allocate space for field descriptions: */
   *prpFieldDescr= malloc(sizeof(CEDA_FDESC)*(*ipNumOfFields));
   if (*prpFieldDescr==NULL)
   {   
      *ipNumOfFields=0;
      return(CCS_ERR_NO_MEM);
   }

   /* copy the field values: */
   pclFldPtr= pcpFNames;
   pclFiTyPtr= pcpFiTyS;
   pclFeLePtr= pcpFeLeS;
   pclSystPtr= pcpSystS;
   pclTypePtr= pcpTypeS;
   ilNum= 0;
   while ((pclFldPtr!=NULL) && (strlen(pclFldPtr)>0))
   {
      GetNextDataItem(pclFldBuf,&pclFldPtr,",","","  ");
      GetNextDataItem(pclFiTyBuf,&pclFiTyPtr,",","","  ");
      GetNextDataItem(pclFeLeBuf,&pclFeLePtr,",","","  ");
      GetNextDataItem(pclSystBuf,&pclSystPtr,",","","  ");
      GetNextDataItem(pclTypeBuf,&pclTypePtr,",","","  ");
      if (strlen(pclFldBuf)!=4)
      {
         dbg(TRACE,"+++ CCSArray: Length of field name '%s' is not 4 byte!",pclFldBuf);
         *ipNumOfFields=0;
         free(*prpFieldDescr);
         *prpFieldDescr= NULL;
         return(CCS_ERR_FIELD_DESCR);
      }
      else
      {
         strcpy((*prpFieldDescr)[ilNum].FINA,pclFldBuf);
         if (strcmp(pclTypeBuf,"TRIM")==0)
            (*prpFieldDescr)[ilNum].TYPE= 1;
         else if (strcmp(pclTypeBuf,"DATE")==0)
            (*prpFieldDescr)[ilNum].TYPE= 2;
         if (strcmp(pclTypeBuf,"LONG")==0)
            (*prpFieldDescr)[ilNum].TYPE= 3;
         else
            (*prpFieldDescr)[ilNum].TYPE= 0;

         if (strcmp(pclFiTyBuf,"C")==0)
            (*prpFieldDescr)[ilNum].FITY= 1;
         else if (strcmp(pclFiTyBuf,"VC2")==0)
            (*prpFieldDescr)[ilNum].FITY= 2;
         else if (strcmp(pclFiTyBuf,"N")==0)
            (*prpFieldDescr)[ilNum].FITY= 3;
         else
            (*prpFieldDescr)[ilNum].FITY= 0;

         (*prpFieldDescr)[ilNum].FELE= atoi(pclFeLeBuf);

         if (strcmp(pclSystBuf,"Y")==0)
            (*prpFieldDescr)[ilNum].SYST= 1;
         else
            (*prpFieldDescr)[ilNum].SYST= 0;
      }
      ilNum++;
   }

   /* set all len of field to zero: 
   for (ilNum=0; ilNum<(*ipNumOfFields) ; ilNum++)
       (*prpFieldDescr)[ilNum].Len= 0;

   /* set all offsets in field to zero: 
   for (ilNum=0; ilNum<(*ipNumOfFields) ; ilNum++)
       (*prpFieldDescr)[ilNum].Begin= 0;
*/
   return (CCS_SUCCESS);
}


/* =================================================================== 
   The routine "CCSTabDestroy" deletes a complete CEDA data array.
   This includes the data array and all keys pointing to this data.
   If the user made copies to key pointers or data pointers, this
   copies get invalid!
   The first parameter is a pointer to the array base.
*/
extern void CCSTabDestroy(CEDA_ARRAY **prgCedaArr)
{
CEDA_COMBI_KEY *prlTmpKey;

   CCSArrayArrDestroy(&(*prgCedaArr)->CedaBase->FirstKey);
   free((*prgCedaArr)->FieldNames);
   free((*prgCedaArr)->FieldDescr);
   while ((*prgCedaArr)->FirstCombiKey!=NULL)
   {
      prlTmpKey= (*prgCedaArr)->FirstCombiKey->Next;
      free ((*prgCedaArr)->FirstCombiKey);
      (*prgCedaArr)->FirstCombiKey= prlTmpKey;
   }
   free((*prgCedaArr));
   (*prgCedaArr)=NULL;

}


/* =================================================================== 
   The routine "CCSTabInit" initializes a CCSCeda Array. It creates the
   data array and assoziates field names and field lengths with the data
   records.
   The return value is a pointer to the array base.
*/
CEDA_ARRAY *CCSTabInit(char *pcpFields, 
                       char *pcpFiTyS, 
                       char *pcpFeLeS, 
                       char *pcpSystS, 
                       char *pcpTypeS, 
                       long  lpStartCount, 
                       long  lpIncCount)
{
CEDA_ARRAY *prlTmp;
int ilRC= CCS_SUCCESS;

  	dbg(DEBUG,"+++ CCSTabInit: Init!");
   /* allocate space for base structure */
   prlTmp= malloc(sizeof(CEDA_ARRAY));
   if (prlTmp==NULL)
   {
    	dbg(TRACE,"+++ CCSTabInit: malloc of CEDA_ARRAY failed, no memory");
      return(NULL);
   }

   /* create array structure */
   prlTmp->CedaBase=CCSArrayArrInit(lpStartCount,lpIncCount);
   if (prlTmp->CedaBase==NULL)
   {   
    	dbg(TRACE,"+++ CCSTabInit: malloc of CCSArrayArrInit failed, no memory");
      free(prlTmp);
      return(NULL);
   }

   /* allocate space for CEDA array name */
   prlTmp->FieldNames= malloc(strlen(pcpFields)+1);
   if (prlTmp->FieldNames==NULL)
   {   
    	dbg(TRACE,"+++ CCSTabInit: malloc of FieldNames failed, no memory");
      free(prlTmp->CedaBase);
      free(prlTmp);
      return(NULL);
   }
   strcpy(prlTmp->FieldNames,pcpFields);

   ilRC= CCSTabCreateFDesc(&prlTmp->FieldDescr,&prlTmp->NumOfFields,
                   pcpFields, pcpFiTyS, pcpFeLeS, pcpSystS, pcpTypeS);
   if (ilRC!=0)
   {   
    	dbg(TRACE,"+++ CCSTabInit: Error in field description list!");
      free(prlTmp->FieldNames);
      free(prlTmp->CedaBase);
      free(prlTmp);
      return(NULL);
   }
   prlTmp->FirstCombiKey= NULL;
  	dbg(DEBUG,"+++ CCSTabInit: done (!)");

   return (prlTmp);
}

/* =================================================================== 
   The routine "CCSTabAddUnsort" adds records to the CCSCeda array in 
   unsorted manner.
   The first parameter is a pointer to the array base.
*/
extern int CCSTabAddUnsort(CEDA_ARRAY *prpCedaArr,
                            const void *pvpData) 
{
int llDataLen;
   llDataLen= CCSTabRecLen(pvpData, prpCedaArr->NumOfFields);
   return (CCSArrayArrAddUnsort( prpCedaArr->CedaBase, pvpData, llDataLen));
}

/* =================================================================== 
   The routine "CCSTabInsert" adds records to the CCSCeda array in 
   sorted manner.
   The first parameter is a pointer to the array base.
*/
extern int CCSTabInsert(CEDA_ARRAY *prpCedaArr,
                         const void *pvpData)
{
int llDataLen;
   llDataLen= CCSTabRecLen(pvpData, prpCedaArr->NumOfFields);
  return ( CCSArrayArrInsert(prpCedaArr->CedaBase, pvpData, llDataLen) );
}


/* =================================================================== 
   The routine "CCSTabInitKey" initializes one ore more keys to the
   CCSCeda Array, depending on the string 'pcpKeyList'. The ',' seperated 
   elements of 'pcpKeyList' are compared with the field names
   of the CEDA array to get the offset in the data records for te key.
   Every key gets a symbolic name from 'pcpSymbols', to allow keys with
   different Key-Of-Data functions with the same field name. The symbolic
   name must have 6 chars!
   The first parameter is a pointer to the array base.
*/
extern int CCSTabInitKey(CEDA_ARRAY   *prpCedaArr,
                          char         *pcpSymbols,
                          char         *pcpKeyList, 
                          char         *pcpKeyStart,
                          char         *pcpKeyLen)
{
  char NameOfSub[]="CCSTabInitKey";
CEDA_COMBI_KEY *prlTmpKey1, *prlTmpKey2;
int ilRC= CCS_SUCCESS;
int ilNum1, ilNum2, ilNumKeys, ilTmpFieldNum;
char *pclSymPtr, *pclFldPtr, *pclBegPtr, *pclLenPtr;
CCS_COMPARE  *pppCmpFkt;
CCS_KEY_PROC *pppKoD;
CCS_KEY rlHlpKey;
char pclTmpSym[100];
char pclTmpFld[100];
char pclTmpBeg[100];
char pclTmpLen[100];
int ilTmpTyp;

   if ((pcpSymbols==NULL) || (pcpSymbols[0]=='\0'))
      return(CCS_ERR_NO_KEYS);

   ilNumKeys= MakeTokenList(pcpSymbols, pcpSymbols, ",", ',' );
   if (ilNumKeys<0)
      return(CCS_ERR_NO_KEYS);
   if (ilNumKeys!=MakeTokenList(pcpKeyList, pcpKeyList, ",", ',' ))
      return(CCS_ERR_NO_KEYS);

	dbg(DEBUG,"<%s>, ilNumKeys: %d",NameOfSub,ilNumKeys);
/*   if (pclTmpSym || pclTmpFld || pclTmpBeg || pclTmpLen ) */
   if ((pcpSymbols==NULL) || (strlen(pcpSymbols)==0) || 
       (pcpKeyList==NULL) || (strlen(pcpKeyList)==0) )
   {
      return(CCS_ERR_KEY_SYNTAX);
   }

   pclSymPtr    = pcpSymbols;
   pclFldPtr    = pcpKeyList;
   pclBegPtr    = pcpKeyStart;
   pclLenPtr    = pcpKeyLen;
/*  //   pclTypPtr    = pcpKeyType; */
   for (ilNum1= 0; ilNum1<ilNumKeys; ilNum1++)
   {
    	dbg(DEBUG,"<%s>, begin of 'for'", NameOfSub);
      GetNextDataItem(pclTmpSym,&pclSymPtr,",","","  ");
      GetNextDataItem(pclTmpFld,&pclFldPtr,",","","  ");
      GetNextDataItem(pclTmpBeg,&pclBegPtr,",","","  ");
      GetNextDataItem(pclTmpLen,&pclLenPtr,",","","  ");

    	dbg(DEBUG,"<%s>, key symbol: %s, key field: %s",
             NameOfSub,pclTmpSym,pclTmpFld);

   	dbg(DEBUG,"<%s>, +++ calculating the pos of key field in array list",NameOfSub);
      /* Calculate the begin of the key in the data record    */
      /* -> sum the length of all fields before the key field */
      /*    and add the offset of the key in the key field    */
      ilTmpFieldNum= -1;
      for (ilNum2= 0; ilNum2<prpCedaArr->NumOfFields; ilNum2++)
      {
         if (strcmp(pclTmpFld,prpCedaArr->FieldDescr[ilNum2].FINA)==0)
         {
            ilTmpFieldNum= ilNum2;
            ilTmpTyp= prpCedaArr->FieldDescr[ilNum2].TYPE;
         }
      }
      if (ilTmpFieldNum==-1)
      {
       	dbg(TRACE,"<%s>, +++ Key '%s': field not loaded!",NameOfSub,pclTmpFld);
         ilRC= CCS_ERR_KEY_UNKNOWN;
      }
      if (strlen(pclTmpSym)!=CEDA_SYMBOL_LEN)
      {   
       	dbg(TRACE,"<%s>, +++ Key symbol len exceeded: '%s' !",NameOfSub,pclTmpSym);
         ilRC= CCS_ERR_SYM_NAME;
      }
      if (strlen(pclTmpFld)!=4)
      {   
       	dbg(TRACE,"<%s>, +++ Key field len exceeded: '%s' !",NameOfSub,pclTmpFld);
         ilRC= CCS_ERR_KEY_NAME;
      }
      if (ilRC==CCS_SUCCESS)
      {

    	   dbg(DEBUG,"<%s>, +++ pos of key field in array list found",NameOfSub);
 
 
         prlTmpKey1= malloc(sizeof(CEDA_COMBI_KEY));
         prlTmpKey1->Next=NULL;
 
         /* append new CombiKey to List */
         if (prpCedaArr->FirstCombiKey==NULL)
         {
            prpCedaArr->FirstCombiKey= prlTmpKey1;
         }
         else
         {
            prlTmpKey2= prpCedaArr->FirstCombiKey;
            while (prlTmpKey2->Next!=NULL)
               prlTmpKey2= prlTmpKey2->Next;
            prlTmpKey2->Next= prlTmpKey1;
         }
         prlTmpKey1->KeyDescr.FieldNum = ilTmpFieldNum;
 
    	   dbg(DEBUG,"<%s>, +++ pos to insert combi key found",NameOfSub);
         /* Store names of fields of CombiKey */
         strcpy(prlTmpKey1->KeySym,pclTmpSym);
         strcpy(prlTmpKey1->KeyDescr.KeyName,pclTmpFld);
         if ((pclTmpBeg) && strlen(pclTmpBeg))
            prlTmpKey1->KeyDescr.Begin= atoi(pclTmpBeg);
         else
            prlTmpKey1->KeyDescr.Begin= 0;
         if ((pclTmpLen) && strlen(pclTmpLen))
            prlTmpKey1->KeyDescr.Len= atoi(pclTmpLen);
         else
            prlTmpKey1->KeyDescr.Len= 0;
         prlTmpKey1->KeyDescr.FieldType= ilTmpTyp;
         if (prlTmpKey1->KeyDescr.Begin<0)
         {   
            free(prlTmpKey1);
            return(CCS_ERR_KEY_BEG);
         }
         if (prlTmpKey1->KeyDescr.Len<0)
         {   
            free(prlTmpKey1);
            return(CCS_ERR_KEY_LEN);
         }
         /* set the Key-Of-Data routine and Compare routine 
            to default values, if not set. The default ist a pointer
            to a char* and strcmp()
         */
    	   dbg(DEBUG,"<%s>, +++ looking for cmp functions",NameOfSub);
         pppCmpFkt= (CCS_COMPARE*) &CCSTabCompareB;
         if (ilTmpTyp!=3)
            if (prlTmpKey1->KeyDescr.Len!=0)  /* use strncmp() */
               pppCmpFkt= (CCS_COMPARE*) &CCSTabCompareA;
            else                              /* use strcmp() */
               pppCmpFkt= (CCS_COMPARE*) &CCSTabCompareB;
         else                                  /* *long cmp */
               pppCmpFkt= (CCS_COMPARE*) &CCSTabCompareC;
 
         pppKoD= (CCS_KEY_PROC*) &CCSTabKeyOfDataB;
         if (ilTmpTyp!=3)
            if (prlTmpKey1->KeyDescr.Begin!=0) /* use offset */
               pppKoD= (CCS_KEY_PROC*) &CCSTabKeyOfDataA;
            else                               /* no  offset */
               pppKoD= (CCS_KEY_PROC*) &CCSTabKeyOfDataB;
         else                                  /* get *long  */
               pppKoD= (CCS_KEY_PROC*) &CCSTabKeyOfDataC;
 
         /* CCSArrayInit needs a Key record with a pointer to the Array: */
         rlHlpKey.ArrBase= prpCedaArr->CedaBase;
         /* now we do have all infos to create a CCSArrayKey: */
         prlTmpKey1->Key=CCSArrayInit(&rlHlpKey,
            &prlTmpKey1->KeyDescr,sizeof(char*),  /*  //prlTmpKey1->KeyDescr.Len,  */
            pppCmpFkt, pppKoD, 0,0, &ilRC);
 
    	   dbg(DEBUG,"<%s>, +++ CCSArrayInit done",NameOfSub);
      } /* ilRC==CCS_SUCCESS (Sym valid, field valid and found )*/
    	dbg(DEBUG,"<%s>, +++ skipped to next field",NameOfSub);
   }    /* for */
  	dbg(DEBUG,"<%s>, +++ leaving...",NameOfSub);

   return(ilRC);
} /* CCSTabInitKey */

/* =================================================================== 
   The routine "GetPtrOfCedaArr" locates a record from the CCSCeda Table 
   array
*/
CEDA_ARRAY *GetPtrOfCedaArr(    char         *pcpTabName)
{
char pclFctName[]="GetPtrOfCedaArr";
CEDA_REC *prlTabPointer;

   prlTabPointer= CCSArrayGetData(prgCedaTABArrBase,pcpTabName);
   if (prlTabPointer==NULL)
   {
      dbg(TRACE,"<%s>: Table '%s' not found!",pclFctName,pcpTabName);
      return(NULL);
   }
   else
      return(prlTabPointer->CedaArr);
}


/* =================================================================== 
   The routine "CedaInit" initializes a CCSCeda Array. It creates the
   data array and assoziates field names and field lengths with the data
   records.
   The first parameter is the Name of the array.
*/
extern int CCSCedaInit(char *pcpTabName, 
                       char *pcpFields, 
                       char *pcpFiTyS, 
                       char *pcpFeLeS, 
                       char *pcpSystS, 
                       char *pcpTypeS, 
                       long  lpStartCount, 
                       long  lpIncCount)
{
char pclFctName[]="CCSCedaInit";
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;
CEDA_REC *prlTabPointer;

   if (strlen(pcpTabName)!=3)
   {
      dbg(TRACE,"<%s>: Table name '%s' exceeds 3 chars!",pclFctName,pcpTabName);
      ilRC= CCS_ERR_NAM_LEN;
      return(ilRC);
   }
   if (prgCedaTABArrBase==NULL)
   {
       prgCedaTABArrBase=CCSArrayInit(NULL,NULL,sizeof(char*),  
            (CCS_COMPARE*)CCSCedaCompare, (CCS_KEY_PROC*)CCSCedaKeyOfData, 10, 10, &ilRC);
       if (prgCedaTABArrBase==NULL)
       {
          dbg(TRACE,"<%s>: memory exceeded!",pclFctName);
          ilRC= CCS_ERR_NO_MEM;
       }
   }       
   if (ilRC==CCS_SUCCESS)
   {
      prlTabPointer= CCSArrayGetData(prgCedaTABArrBase,pcpTabName);
      if (prlTabPointer!=NULL)
      {
         dbg(TRACE,"<%s>: Table name '%s' already exists!",pclFctName,pcpTabName);
         ilRC= CCS_ERR_TAB_EXISTS;
      }
   }
   if (ilRC==CCS_SUCCESS)
   {
      prlTabPointer=malloc(sizeof(CEDA_REC));
      if (prlTabPointer==NULL)
      {
         dbg(TRACE,"<%s>: memory exceeded!",pclFctName);
         ilRC= CCS_ERR_NO_MEM;
      }
      else
      {
         strcpy(prlTabPointer->TabName,pcpTabName);
         prlCedaTmpArr= CCSTabInit(pcpFields, pcpFiTyS, pcpFeLeS, pcpSystS, 
                                   pcpTypeS, lpStartCount, lpIncCount);
         if (prlCedaTmpArr==NULL)
         {  /* array not created */
            dbg(TRACE,"<%s>: memory exceeded!",pclFctName);
            ilRC= CCS_ERR_NO_MEM;
         }
         else
         {  /* array created */
            memcpy(&prlTabPointer->CedaArr,&prlCedaTmpArr,sizeof(void*));
            ilRC= CCSArrayInsert(prgCedaTABArrBase,prlTabPointer,
                  sizeof(void*)+strlen(pcpTabName)+1);
         }
         free(prlTabPointer);
      }
   }  /* CCS_SUCCESS */
   if (ilRC!=CCS_SUCCESS)
       dbg(TRACE,"<%s>: init of %s failed!",pclFctName,pcpTabName);

   return (ilRC);
}

/* =================================================================== 
   The routine "CCSCedaDestroy" deletes a complete CEDA data array.
   This includes the data array and all keys pointing to this data.
   If the user made copies to key pointers or data pointers, this
   copies get invalid!
   The first parameter is the Name of the array.
*/
extern int CCSCedaDestroy(char *pcpTabName)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
   {
      CCSTabDestroy(&prlCedaTmpArr);
      CCSArrayDelete(prgCedaTABArrBase,pcpTabName);
      if (prgCedaTABArrBase->ArrBase->Count==0)
         CCSArrayArrDestroy(&prgCedaTABArrBase);
   }
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


/* =================================================================== 
   The routine "CCSCedaAddUnsort" adds records to the CCSCeda array in 
   unsorted manner.
   The first parameter is the Name of the array.
*/
extern int           CCSCedaAddUnsort(  char         *pcpTabName,
                                     const void   *pvpData)

{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      ilRC= CCSTabAddUnsort(prlCedaTmpArr,pvpData);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}

/* =================================================================== 
   The routine "CCSCedaDelete" deletes elements from the array. The elements to 
   delete are defined by a list of key expressions like 
      "URNO_1>       0,TIFA_1<199901030000"
   The ',' is is used as a AND relation. Other relations are not defined. 
   The first parameter is the Name of the array.
*/
extern int           CCSCedaDelete(     char         *pcpTabName, 
                                     char         *pcpCombiKey)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      ilRC= CCSTabDelete(prlCedaTmpArr,pcpCombiKey);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


/* =================================================================== 
   The routine "CCSCedaGetCount" returns the number of elements of the
   CEDA array 'pcpTabName'
   The first parameter is the Name of the array.
*/
long          CCSCedaGetCount(   char         *pcpTabName)
{
CEDA_ARRAY *prlCedaTmpArr;
long llCount= 0;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      llCount= CCSTabGetCount(prlCedaTmpArr);
   return (llCount);
}


/* =================================================================== 
   The first parameter is the Name of the array.
*/
extern int           CCSCedaGetData(    char         *pcpTabName, 
                                     char         *pcpCombiKey,
                                     USR_ARRAY    *prpDataPtr)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;
   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      ilRC= CCSTabGetData(prlCedaTmpArr,pcpCombiKey,prpDataPtr);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


/* =================================================================== 
   The routine "CCSCedaInitKey" initializes one ore more keys to the
   CCSCeda Array, depending on the string 'pcpKeyList'. The ',' seperated 
   elements of 'pcpKeyList' are compared with the field names
   of the CEDA array to get the offset in the data records for te key.
   Every key gets a symbolic name from 'pcpSymbols', to allow keys with
   different Key-Of-Data functions with the same field name. The symbolic
   name must have 6 chars!
   The first parameter is the Name of the array.
*/
extern int           CCSCedaInitKey( char         *pcpTabName,
                                     char         *pcpSymbols,
                                     char         *pcpKeyList, 
                                     char         *pcpKeyStart,
                                     char         *pcpKeyLen)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      ilRC= CCSTabInitKey(prlCedaTmpArr,pcpSymbols,pcpKeyList, 
                           pcpKeyStart,pcpKeyLen);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


/* =================================================================== 
   The routine "CCSCedaInsert" adds records to the CCSCeda array in 
   sorted manner.
   The first parameter is the Name of the array.
*/
extern int           CCSCedaInsert(     char         *pcpTabName,
                                     const void   *pvpData)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      ilRC= CCSTabInsert(prlCedaTmpArr,pvpData);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


/* =================================================================== 
   The routine "CCSCedaKeySort" sorts all keys connected to the CEDA array
   pointer 'prpCedaArr'
   The first parameter is the Name of the array.
*/
extern int           CCSCedaKeySort(    char         *pcpTabName)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      CCSTabKeySort(prlCedaTmpArr);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


/* =================================================================== 
   The routine "CCSCedaTestKeys" tests, if the keys have lost the info.
   By using the the routine ...TestKeys, it uses the user defined compare 
   and key-of-data operation. 
   The first parameter is the Name of the array.
*/
extern int           CCSCedaTestKeys(   char         *pcpTabName)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      ilRC= CCSTabTestKeys(prlCedaTmpArr);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


/* =================================================================== 
   The routine "CCSCedaTestOrder" tests, if the sort order of the data
   is compliing to compare function of the key.
   By using the the routine ...TestOrder, it uses the user defined compare 
   operation. 
   The first parameter is the Name of the array.
*/
extern int           CCSCedaTestOrder(  char         *pcpTabName)
{
CEDA_ARRAY *prlCedaTmpArr;
int ilRC= CCS_SUCCESS;

   if ((prlCedaTmpArr= GetPtrOfCedaArr(pcpTabName))!=NULL)
      ilRC= CCSTabTestOrder(prlCedaTmpArr);
   else
      ilRC= CCS_ERR_TAB_NOT_FOUND;
   return (ilRC);
}


