#ifndef _DEF_mks_version_chkospar_c
  #define _DEF_mks_version_chkospar_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkospar_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/chkospar.c 1.2 2004/08/10 20:09:34SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*	#define	CHKOSPAR	*/
#include	"chkospar.h"


extern	int	debug_level;

int	GetKernelParamValue(char *,FILE *,long *);
void	PrintToSpezifiedFile(char *,FILE *);
int	SubstTokOfLine(char *,char,char);
int	GetFieldValByNo(char *,char *,int,char);
int	GetFirstWord(char *,char *,int);
int	GetIWord(char *,char *,int,int);
int	CompareFirstWord(char *,char *);
int	ExpressToVal(char *,long *);
int	GetValueFromFile(char *,char *,int,int,char *,int,FILE *);
int	GetConfigSection(char *,CONFPAR *,FILE *,FILE *);

/*	*****************************************************************	*/
/*	Funktion GetKernelParamValue()								*/
/*	Die Funktion sucht in der UNIX-Systemdatei mtune nach dem �bergebenen	*/
/*	Kernel-Parameter. Der Pfad, in dem sich mtune befindet, wird beim	*/
/*	�bersetzen je nach Betriebssystem festgelegt.Wird der Parameter in	*/
/*	mtune gefunden, gibt die Funktion den entsprechenden Wert zur�ck, wie	*/
/*	er in mtune angegeben ist.									*/
/*	int	GetKernelParameter(char *pcpParam,FILE *pFpDbgFile,long plpVal)	*/
/*	IN/OUT	char *pcpParam, der zu suchende Parameter				*/
/*	IN		FILE	*pFpDbgFile, Debug-File f�r Fehlermeldungen			*/
/*	OUT		long	*plpVal, Puffer f�r Wert des gesuchten Parameters		*/
/*	Returns	SUCCESS	falls Parameterwert gefunden					*/
/*			RC_FAIL	falls kein Parameterwert errechnet wurde		*/
/*	*****************************************************************	*/

int	GetKernelParamValue(char *pcpParam,FILE *pFpDbgFile,long *plpVal)
{
	int	ilRC = RC_SUCCESS;
	int	ili,i;
	int	rc;
	int	ilFound;
	FILE	*FlKernel;
	char	clFileString[100];
	char	pclFieldVal[100];
	char	*pclWorkPtr;
	char	clLine[200];
	char	pclErrorText[200];

	if ( (pcpParam == (char *) NULL) || (plpVal == (long *) NULL) ) {
		sprintf(pclErrorText,"\nGetKernelParamValue:\nNo Parameter to Check or no Buffer for Value\n");
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		ilRC = RC_FAIL;
	}
	else {
		memset(clFileString,0x00,100);
		sprintf(clFileString,MTUNE);
		if (clFileString[0] == 0) {
			sprintf(pclErrorText,"\nGetKernelParamValue:\nThese Operating System are supported:\nHP_UX\nSINIX\nSCO\nSOLARIS\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = RC_FAIL;
		}
		else {
			FlKernel = fopen(clFileString,"r");
			if (FlKernel == (FILE *) NULL) {
				sprintf(pclErrorText,"\nGetKernelParamValue:\nUnable to open File '%s'\n",clFileString);
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
				ilRC = RC_FAIL;
			}
			else {
				for (ili = 0, ilFound = FALSE; (ilRC == RC_SUCCESS) && (ilFound == FALSE); ili++) {
					memset(clLine,0x00,200);
					if (fgets(clLine,200,FlKernel) == (char *) NULL) {
						if (ili <= 0) {
							sprintf(pclErrorText,"\nGetKernelParamValue:\nReading File '%s' Failed \n",clFileString);
							PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
						}
						ilRC = RC_FAIL;
					}
					else {
if(debug_level == DEBUG) {
sprintf(pclErrorText,"GetKernelParamValue:\nReading File line '%s'\n",clLine);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
for (i = 0; i < strlen(clLine); i++)
fprintf(pFpDbgFile,"%d(%x)",clLine[i]);
fprintf(pFpDbgFile,"\n");
fflush(pFpDbgFile);
}
						if ( (CompareFirstWord(clLine,pcpParam) == TRUE) /*&&
							(clLine[0] != '*')*/ ) {
							ilRC = SubstTokOfLine(clLine,9,',');
							if (ilRC != RC_FAIL) {
								ilRC = GetFieldValByNo(clLine,pclFieldVal,2,',');
								if (ilRC != RC_FAIL) {
									ilRC = ExpressToVal(pclFieldVal,plpVal);
									ilFound = TRUE;
								}
								else {
									sprintf(pclErrorText,"Couldn't find Field Value for Field '%s'\n'",clFileString);
									PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
								}
							}
							else {
								sprintf(pclErrorText,"Error preparing Field String for Field '%s'\n'",clFileString);
								PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
							}
						}
						ilRC = RC_SUCCESS;
					}
				}	/*	end for	*/
				if (ilFound == TRUE) {
if (debug_level == DEBUG) {
sprintf(pclErrorText,"\nValue for Field '%s' is %ld\n'",pclFieldVal,*plpVal);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
					;
				}
				else {
					sprintf(pclErrorText,"\nField '%s' not found\n'",pcpParam);
					PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
				}
			}	/*	end else	*/
		}	/*	end else	*/
	}	/*	end else	*/
	fclose(FlKernel);
	return ilRC;
}	/*	end of GetKernelParamValue()	*/


void	PrintToSpezifiedFile(char *pcpText,FILE *pFpFile)
{
	if (pcpText == (char *) NULL)
		;
	else if (pFpFile == (FILE *) NULL)
		printf(pcpText);
	else
		fprintf(pFpFile,pcpText);
	return;
}	/*	end of DebugToSpezifiedFile	*/

		

/*	*****************************************************************	*/
/*	Funktion SubstBlankOfLine()									*/
/*	Die Funktion sucht einen Nul-terminierten Character-String nach einem	*/
/*	Zeichen ab. Wird es zwischen erlaubten Zeichen erstmals gefunden wird	*/
/*	es durch ein Ersatzzeichen ersetzt. Alle weiteren Delinquenten werden	*/
/*	gel�scht. Nach Auftreten des n�chsten erlaubten Zeichens beginnt		*/
/*	diese Prozedur von Neuem.									*/
/*	Erlaubte Zeichen sind alle Zeichen au�er dem gesuchten Zeichen, dem 	*/
/*	Linefed-Zeichen und dem Carrige-return-Zeichen.					*/
/*	Die Funktion bricht ab, wenn der String in voller L�nge bearbeitet	*/
/*	wurde, das Linefed-Zeichen, das Carrige-return-Zeichen oder ein NUL-	*/
/*	Zeichen gefunden wird.										*/
/*	Der String hat jetzt eventuell nicht mehr seine urspr�ngliche L�nge.	*/
/*	Er ist selbsverst�ndlich NUL-terminiert.						*/
/*	int	SubstTokOfLine(char *pcpInline,char cpDel,char cpSubst)		*/
/*	IN/OUT	char *pcpInline, der zu bearbeitende String				*/
/*	IN		char	cpDel, das zu ersetzende Zeichen					*/
/*	IN		char	cpSubst, das Ersatzzeichen						*/
/*	Returns	int	L�nge des bearbeiteten Strings					*/
/*			RC_FAIL	falls calloc() fehlschlug					*/
/*	*****************************************************************	*/
int	SubstTokOfLine(char *pcpInline,char cpDel,char cpSubst)
{
	int	ili;
	int	ilFirst;
	int	ilLen;    
	int	ilNewLen;
	char	*pclOutline;
	char	*pclHelp;

	if (pcpInline == NULL)
		return RC_FAIL;
	ilLen = strlen(pcpInline);
	if ( (pclOutline = calloc(1,ilLen)) == (char *) NULL )
		return RC_FAIL;
	strncpy(pclOutline,pcpInline,ilLen);
	pclHelp = pclOutline;
	memset(pcpInline,0x00,ilLen);
	for (ili = 0,ilNewLen = 0, ilFirst = TRUE; ili < ilLen; ili++, pclHelp++) {
		if ( (*pclHelp == 10) ||
			(*pclHelp == 13) ||
			(*pclHelp == 0) ) {
			break;
		}
		else if (*pclHelp == cpDel) {
			if (ilFirst == TRUE) {
				ilFirst = FALSE;
				pcpInline[ilNewLen] = cpSubst;
				ilNewLen++;
			}
		}
		else {
			if (ilFirst == FALSE) {
				ilFirst = TRUE;
			}
			pcpInline[ilNewLen] = *pclHelp;
			ilNewLen++;
		}
	}
	return ilNewLen;
}	/*	end of SubstBlankOffLine()	*/

/*	*****************************************************************	*/
/*	Funktion GetFieldValByNo()									*/
/*	Die Funktion liest aus einem Nul-terminierten String von Feldern		*/
/*	das �ber die Nummer adressierte Feld. Erlaubte Zeichen innerhalb		*/
/*	eines Feldes sind alle Zeichen au�er dem �bergebenen Feldtrenn-		*/
/*	Zeichen, dem Linefed-Zeichen und dem Carrige-return-Zeichen.		*/
/*	Die Funktion bricht ab, wenn das Feld gelesen wurde, das Linefed-	*/
/*	Zeichen, das Carrige-return-Zeichen oder ein NUL-Zeichen gefunden	*/
/*	wird.													*/
/*	Als Ergebnis-Buffer erwartet die Funktion eine Character-Puffer der	*/
/*	L�nge MAX_FIELD_LEN (definiert in der Headerdatei chkospar.h. Er wird	*/
/*	selbsverst�ndlich NUL-terminiert zur�ckgegeben.					*/
/*	int	GetFieldValByNo										*/
/*	(char *pcpFieldString,char *pcpFieldVal,int ipNo,char cpDelimiter)	*/
/*	IN		char *pcpFieldString, der zu bearbeitende String			*/
/*	IN/OUT	char	*pcpFieldVal, der Ergebnispuffer					*/
/*	IN		int	ipNo, Index des Feldes							*/
/*	IN		char	cpDelimiter, Trennzeichen zwischen Feldern			*/
/*	Returns	int	L�nge des gelesenen Feldes						*/
/*			RC_FAIL	falls Feld nicht gefunden					*/
/*	*****************************************************************	*/

int	GetFieldValByNo(char *pcpFieldString,char *pcpFieldVal,int ipNo,char cpDelimiter)
{
	int	ilLen;
	int	ili,ilj;
	int	ilReadytoWrite = FALSE;
	int	ilNewLen;
	int	ilRC = RC_SUCCESS;

	if ( (pcpFieldString == NULL) || (pcpFieldVal == NULL) || (ipNo == 0) )
		ilRC = FALSE;
	else {
		ipNo--;
		ilLen = strlen(pcpFieldString);
		for (ilj = 0,ili = 0, ilNewLen = 0;
			(ili <= ipNo) && (ilj < ilLen) && (ilNewLen < MAX_FIELD_LEN);
			ilj++) {
			if (pcpFieldString[ilj] == cpDelimiter) {
				if (ili < ipNo) {
					ili++;
					if (ili == ipNo)
						ilReadytoWrite = TRUE;
					else if (ili > ipNo)
						break;
				}
				else {
					break;
				}
			}
			else if (	(pcpFieldString[ilj] == 10) ||
					(pcpFieldString[ilj] == 13) ||
					(pcpFieldString[ilj] == 0) )
				break;
			else {
				if (ilReadytoWrite == TRUE) {
					pcpFieldVal[ilNewLen] = pcpFieldString[ilj];
					ilNewLen++;
				}
			}
		}
		if (ilReadytoWrite == TRUE) {
			pcpFieldVal[ilNewLen] = 0;
			ilRC = ilNewLen;
		}
		else
			ilRC = RC_FAIL;
	}
	return ilRC;
}	/*	end of GetFieldValByNo()	*/
	
/*	*****************************************************************	*/
/*	Funktion GetFirstWord()										*/
/*	Die Funktion liest aus einem Nul-terminierten Character-String das 	*/
/*	erste Wort. Ein Wort kann alle Zeichen enthalten au�er den allge-	*/
/*	meinen Begrenzern Nul, Blank, CR, LF, TAB.						*/
/*	Das Ergebnis wird in den Ergebnispuffer geschrieben. Die L�nge des	*/
/*	Ergebnispuffers wird als Parameter �bergeben.					*/
/*	Die Funktion bricht ab, wenn ein Begrenzer gefunden wurde, oder die	*/
/*	Anzahl analysierter Zeichen die L�nge des Ergebnispuffers bzw. die	*/
/*	L�nge des Eingabepuffers �berschreitet.							*/
/*	int	GetFirstWord(char *pcpSource,char *pcpDest,int ipLen)			*/
/*	IN		char *pcpSource, der zu bearbeitende String				*/
/*	IN/OUT	char	*pcpDest, der Ergebnispuffer						*/
/*	IN		int	ipLen, Gr��e des Ergebnispuffers					*/
/*	Returns	int	L�nge des gelesenen Wortes						*/
/*			RC_FAIL	falls ein Parameter ung�ltig ist				*/
/*	*****************************************************************	*/

int	GetFirstWord(char *pcpSource,char *pcpDest,int ipLen)
{
	int	ilLen;
	int	ili;
	char	*pclHelp;

	if ( (pcpSource == NULL) || (pcpDest == NULL) || (ipLen == 0) )
		ili = 0;
	else {	
		ilLen = strlen(pcpSource);
		for (ili = 0, pclHelp = pcpSource; (ili < ipLen) && (ili < ilLen); ili++, pclHelp++) {
			if ( (*pclHelp == 0) ||
				(*pclHelp == 10) ||
				(*pclHelp == 13) ||
				(*pclHelp == '	') ||
				(*pclHelp == 32) ) {
				pcpDest[ili] = 0;
/*	Nur f�r Test	
fprintf(pardbg,"Delimiter of First Word is %d\n",(int) *pclHelp);
	Nur f�r Test	*/
				break;
			}
			else {
				pcpDest[ili] = *pclHelp;
			}
		}
		if ( (ili == ilLen) || (ili == ipLen) )
			pcpDest[ili - 1] = 0;
	}
/*	Nur f�r Test	
if (ili > 0)
	fprintf(pardbg,"Word is '%s' of length %d\n",pcpDest,ili);
else
	fprintf(pardbg,"1. Word not found, rc = %d\n",ili);
fflush(pardbg);
	Nur f�r Test	*/
	return ili;
}	/*	end of GetFirstWord()	*/
	
/*	*****************************************************************	*/
/*	Funktion GetIWord()											*/
/*	Die Funktion liest aus einem Nul-terminierten Character-String das 	*/
/*	Wort mit Index ipIndex.Ein Wort kann alle Zeichen enthalten au�er den	*/
/*	allgemeinen Begrenzern Nul, Blank, CR, LF, TAB.					*/
/*	Das Ergebnis wird in den Ergebnispuffer geschrieben. Die L�nge des	*/
/*	Ergebnispuffers wird als Parameter �bergeben.					*/
/*	Die Funktion bricht ab, wenn ein Begrenzer gefunden wurde, oder die	*/
/*	Anzahl analysierter Zeichen die L�nge des Ergebnispuffers bzw. die	*/
/*	L�nge des Eingabepuffers �berschreitet.							*/
/*	int	GetFirstWord(char *pcpSource,char *pcpDest,int ipLen,int ipIndex)*/
/*	IN		char *pcpSource, der zu bearbeitende String				*/
/*	IN/OUT	char	*pcpDest, der Ergebnispuffer						*/
/*	IN		int	ipLen, Gr��e des Ergebnispuffers					*/
/*	IN		int	ipIndex, Index des zu lesenden Wortes				*/
/*	Returns	int	L�nge des gelesenen Wortes						*/
/*			RC_FAIL	falls ein Parameter ung�ltig ist				*/
/*	*****************************************************************	*/

int	GetIWord(char *pcpSource,char *pcpDest,int ipLen,int ipIndex)
{
	int	ilLen;
	int	ili;
	int	ilj;
	int	ilk;
	char	*pclHelp;

/*	Nur f�r Test	
fprintf(pardbg,"String: '%s'\n",pcpSource);
	Nur f�r Test	*/
	if ( (pcpSource == NULL) || (pcpDest == NULL) || (ipLen == 0) || (ipIndex == 0) )
		ili = 0;
	else {	
/*	Nur f�r Test	
fprintf(pardbg,"In else now\n");
	Nur f�r Test	*/
		ilLen = strlen(pcpSource);
		for 	(ili = 0,ilk = 0, pclHelp = pcpSource, ilj = 1;
			(ilj <= ipIndex) && (ilk < ilLen) && (ili < ipLen);
			ilj++) {
/*	Nur f�r Test	
fprintf(pardbg,"Char %d = %d\n",ili,(int) *pclHelp);
	Nur f�r Test	*/
			for (ili = 0; (ilk < ilLen) && (ili < ipLen); ilk++, pclHelp++) {
/*	Nur f�r Test	
fprintf(pardbg,"Char %d = %d\n",ili,(int) *pclHelp);
	Nur f�r Test	*/
				if ( (*pclHelp == 0) ||
					(*pclHelp == 10) ||
					(*pclHelp == 13) ||
					(*pclHelp == '	') ||
					(*pclHelp == 32) ) {
					if (ili > 0) {
						pcpDest[ili] = 0;
						ili++;
/*	Nur f�r Test	
fprintf(pardbg,"Delimiter of Word %d is %d\n",ilj,(int) *pclHelp);
	Nur f�r Test	*/
						break;
					}
				}
				else if (ilj == ipIndex) {
					pcpDest[ili] = *pclHelp;
					ili++;
				}
				else {
					ili++;
				}
			}
		}
		if ( (ilk == ilLen) || (ili == ipLen) )
			pcpDest[ili - 1] = 0;
	}
/*	Nur f�r Test	
if (ili > 0)
	fprintf(pardbg,"Word %d is '%s' of length %d\n",(ilj - 1),pcpDest,ili);
else
	fprintf(pardbg,"Word %d not found, rc = %d\n",ipIndex,ili);
	Nur f�r Test	*/
	return ili;
}	/*	end of GetIWord()	*/
		
/*	*****************************************************************	*/
/*	Funktion CompareFirstWord()									*/
/*	Die Funktion vergleicht das erste Wort eines Character-Strings mit 	*/
/*	Vergleichswort. Die Funktion benutzt die Routine GetFirstWord().		*/
/*	Ist einer der beiden Parameter ung�ltig, bricht die Funktion ab und	*/
/*	liefert den Fehlerwert.										*/
/*	int	CompareFirstWord(char *pcpString,char *pcpComp)				*/
/*	IN		char *pcpString, der zu bearbeitende String				*/
/*	IN/OUT	char	*pcpComp, das Vergleichswort						*/
/*	Returns	int	TRUE,	wenn 1. Wort == Vergleichswort			*/
/*				FALSE,	 falls ein Parameter ung�ltig ist oder		*/
/*						1. Wort != Vergleichswort				*/
/*	*****************************************************************	*/

int	CompareFirstWord(char *pcpString,char *pcpComp)
{
	int	ilRC;
	int	ili;
	int	ilLen;
	char	clWord[THE_WORD_LEN];

	if ( (pcpString == NULL) || (pcpComp == NULL) )
		ilRC = FALSE;
	else {
		ilLen = strlen(pcpComp);
		if ( (ilRC = GetFirstWord(pcpString,clWord,THE_WORD_LEN)) <= 0)
			ilRC = FALSE;
		else {
			ilRC = TRUE;
			for (ili = 0; (ili < ilRC) || (ili < ilLen); ili++) {
				if (clWord[ili] != pcpComp[ili]) {
					ilRC = FALSE;
					break;
				}
			}
		}
	}
/*	Nur f�r Test	
fprintf(pardbg,"CompareFirstWord returns %d for Word '%s'\n",ilRC,pcpComp);
	Nur f�r Test	*/
	return ilRC;
}	/*	end of CompareFirstWord()	*/

int	ExpressToVal(char *pcpSource,long *plpValue)
{
	int		ilRC = RC_SUCCESS;				/*	return code	*/
	int		ili;							/*	counter	*/
	int		ilLen = 0;					/*	string length	*/
	int		ilFlag = STATUS_INIT;			/*	status flag	*/
	long		llVal1 = 0,llVal2 = 0,llVal3 = 0;	/*	value buffer	*/
	int		ilValueCount;					/*	value counter	*/
	char		pclBuffer[MAX_VALUE_LEN];		/*	collection buffer	*/
	int		ilBufCount = 0;				/*	collection counter	*/
	long		llValue = 0;
	int		ilBracketFlag = FALSE;
	char		*pclErrorPointer;
	char		clCalc1 = 0,clCalc2 = 0;
	int		ilCalcCount = 0;

	memset(pclBuffer,0x00,MAX_VALUE_LEN);

	*plpValue = 0;
/*	nur test
fprintf(pardbg,"Point 1\n");
fflush(pardbg);
	nur test	*/
	if (pcpSource == NULL) {
		return RC_FAIL;
	}
/*	nur test
fprintf(pardbg,"Point 2 '%s'\n",pcpSource);
fflush(pardbg);
	nur test	*/
	ilLen = strlen(pcpSource);
/*	nur test	
fprintf(pardbg,"ExpressToVal: String '%s' with strlen = %d\n",pcpSource,ilLen);
fflush(pardbg);
	nur test	*/
	if ((pclErrorPointer = calloc(1,ilLen)) == NULL)
		return RC_FAIL;
/*	nur test
fprintf(pardbg,"Point 4\n");
fflush(pardbg);
	nur test	*/
	for (ili = 0, ilValueCount = 0;
		(ilBufCount < MAX_VALUE_LEN) && (ili <= ilLen) && (ilValueCount < 3) && 
		(ilFlag != STATUS_EXIT) && (ilRC != RC_FAIL);
		ili++) {
/*	nur test
fprintf(pardbg,"Point 5.%d\n",ili);
fflush(pardbg);
	nur test	*/
		if (	(pcpSource[ili] == 9) ||
			(pcpSource[ili] == 10) ||
			(pcpSource[ili] == 13) ||
			(pcpSource[ili] == 32) ) {
/*	nur test
fprintf(pardbg,"Point 6\n");
fflush(pardbg);
	nur test	*/
			continue;
		}
		else {
/*	nur test
fprintf(pardbg,"Point 7.%d -> %c Status = '%s', ValCount = %d\n",
ilBufCount,pcpSource[ili],pcGStatString[ilFlag],ilValueCount);
fflush(pardbg);
	nur test	*/
			switch (ilFlag) {
				case STATUS_INIT:
					if 	(pcpSource[ili] == 0) {
						ilRC = RC_FAIL;
					}	/*	Keine Zeichen im String	*/
					else if ( (pcpSource[ili] >= '0') && (pcpSource[ili] <= '9') ) {
						pclBuffer[0] = pcpSource[ili];
						ilBufCount = 1;
						ilFlag = STATUS_NUMBER;
					}	/*	Erstes Zeichen ist Zahl	*/
					else if ( ( (pcpSource[ili] >= 'a') && (pcpSource[ili] <= 'z') ) ||
							( (pcpSource[ili] >= 'A') && (pcpSource[ili] <= 'Z') ) ) {
						pclBuffer[0] = pcpSource[ili];
						ilBufCount = 1;
						ilFlag = STATUS_LETTER;
					}	/*	Erstes Zeichen ist Buchstabe	*/
					else if 	(pcpSource[ili] == '(') {
						ilFlag = OPEN_BRACKET;
						ilBracketFlag = TRUE;
					}	/*	Erstes Zeichen ist 'offene' Klammer	*/
					else {
						pclErrorPointer[ili] = '*';
						ilRC = RC_FAIL;
					}	/*	Erstes Zeichen ist ung�ltig	*/
					break;
				case	STATUS_NUMBER:
/*	nur test
fprintf(pardbg,"Status = '%s', Char = %c, ValCount = %d\n",
pcGStatString[ilFlag],pcpSource[ili],ilValueCount);
fflush(pardbg);
	nur test	*/
					if ( (pcpSource[ili] >= '0') && (pcpSource[ili] <= '9') ) {
						pclBuffer[ilBufCount] = pcpSource[ili];
						ilBufCount++;
					}	/*	n�chstes Zeichen ist Nummer, Status erhalten	*/
					else if ( ( (pcpSource[ili] >= 'a') && (pcpSource[ili] <= 'z') ) ||
							( (pcpSource[ili] >= 'A') && (pcpSource[ili] <= 'Z') ) ) {
						pclBuffer[ilBufCount] = pcpSource[ili];
						ilBufCount++;
						ilFlag = STATUS_LETTER;
					}	/*	n�chstes Zeichen ist Buchstabe, Status �ndern	*/
					else if ( (pcpSource[ili] == '*') ||
							(pcpSource[ili] == '-') ||
							(pcpSource[ili] == '/') ||
							(pcpSource[ili] == '+') ) {
						if (ilCalcCount == 0) {
							clCalc1 = pcpSource[ili];
							ilCalcCount++;
						}
						else {
							clCalc2 = pcpSource[ili];
							ilCalcCount++;
						}
						if (ilValueCount == 0) {
							llVal1 = atol(pclBuffer);
							ilValueCount++;
							memset(pclBuffer,0x00,MAX_VALUE_LEN);
							ilFlag = STATUS_CALC;
						}
						else if (ilValueCount == 1) {
							llVal2 = atol(pclBuffer);
							ilValueCount++;
							memset(pclBuffer,0x00,MAX_VALUE_LEN);
							ilFlag = STATUS_CALC;
						}
						else {
							pclErrorPointer[ili] = '*';
							ilRC = RC_FAIL;
						}
					}	/*	n�chstes Zeichen ist Rechenzeichen, Status �ndern,	*/
						/*	Wert errechnen, mu� erster Wert sein! sonst -> Fehler	*/
					else if (pcpSource[ili] == ')') {
						if (ilBracketFlag == TRUE) {
							if (ilValueCount == 0) {
								llVal1 = atol(pclBuffer); 
								ilValueCount++;
								memset(pclBuffer,0x00,MAX_VALUE_LEN);
								ilFlag = CLOSE_BRACKET;
								ilBracketFlag = FALSE;
							}	/*	1. Wert eintragen und Statuswechsel	*/
							else if (ilValueCount == 1) {
								llVal2 = atol(pclBuffer);
								ilValueCount++;
								ilFlag = CLOSE_BRACKET;
								memset(pclBuffer,0x00,MAX_VALUE_LEN);
								ilBracketFlag = FALSE;
							}	/*	2. Wert eintragen und Statuswechsel -> Ende	*/
							else if (ilValueCount == 2) {
								llVal3 = atol(pclBuffer);
								ilValueCount++;
								ilFlag = STATUS_EXIT;
							}	/*	2. Wert eintragen und Statuswechsel -> Ende	*/
							else {
								pclErrorPointer[ili] = '*';
								ilRC = RC_FAIL;
							}
						}	/*	Klammer ge�ffnet -> OK	*/
						else {
							pclErrorPointer[ili] = '*';
							ilRC = RC_FAIL;
						}	/*	Keine Klammer ge�ffnet -> Fehler	*/
					}	/*	Klammer schlie�en	*/
					else if 	(pcpSource[ili] == 0) {
						if (ilValueCount == 0) {
							llVal1 = atol(pclBuffer); 
							ilValueCount++;
						}
						else if (ilValueCount == 1) {
							llVal2 = atol(pclBuffer);
							ilValueCount++;
						}
						else if (ilValueCount == 2) {
							llVal3 = atol(pclBuffer);
							ilValueCount++;
						}
						else {
							pclErrorPointer[ili] = '*';
							ilRC = RC_FAIL;
						}
						ilFlag = STATUS_EXIT;
					}	/*	N�chstes Zeichen ist Nul ggfl. Wert errechnen und Ende	*/
					else {
						pclErrorPointer[ili] = '*';
						ilRC = RC_FAIL;
					}	/*	Zeichen ist ung�ltig	*/
					break;
				case	STATUS_LETTER:
					if ( ( (pcpSource[ili] >= '0') && (pcpSource[ili] <= '9') ) ||
							( (pcpSource[ili] >= 'a') && (pcpSource[ili] <= 'z') ) ||
							( (pcpSource[ili] >= 'A') && (pcpSource[ili] <= 'Z') ) ) {
						pclBuffer[ilBufCount] = pcpSource[ili];
						ilBufCount++;
					}	/*	Zeichen ist Stringbestandteil -> weiter	*/
					else if ( (pcpSource[ili] == '*') ||
							(pcpSource[ili] == '-') ||
							(pcpSource[ili] == '/') ||
							(pcpSource[ili] == '+') ) {
						if (ilCalcCount == 0) {
							clCalc1 = pcpSource[ili];
							ilCalcCount++;
						}
						else {
							clCalc2 = pcpSource[ili];
							ilCalcCount++;
						}
						if ( (ilRC = GetKernelParamValue(pclBuffer,NULL,&llValue)) != RC_SUCCESS) {
							pclErrorPointer[ili] = '*';
							ilRC = RC_FAIL;
							ilFlag = STATUS_EXIT;
						}	/*	Wert des Strings nicht gefunden -> Fehler	*/
						else {
							if (ilValueCount == 0) {
								llVal1 = llValue; 
								ilValueCount++;
								ilFlag = STATUS_CALC;
							}
							else if (ilValueCount == 1) {
								llVal2 = llValue; 
								ilValueCount++;
								ilFlag = STATUS_CALC;
							}
							else {
								pclErrorPointer[ili] = '*';
								ilRC = RC_FAIL;
							}
						}	/*	n�chstes Zeichen ist Rechenzeichen, Status �ndern,	*/
							/*	Wert errechnen, mu� erster Wert sein! sonst -> Fehler	*/
					}	/*	Zeichen ist Rechenzeichen, Wert errechnen und Statuswechsel	*/
					else if (pcpSource[ili] == ')') {
						if (ilBracketFlag == TRUE) {
							if ( (ilRC = GetKernelParamValue(pclBuffer,NULL,&llValue)) != RC_SUCCESS) {
								pclErrorPointer[ili] = '*';
								ilRC = RC_FAIL;
								ilFlag = STATUS_EXIT;
							}	/*	Wert des Strings nicht gefunden -> Fehler	*/
							else {
								if (ilValueCount == 0) {
									llVal1 = llValue; 
									ilValueCount++;
									memset(pclBuffer,0x00,MAX_VALUE_LEN);
									ilFlag = CLOSE_BRACKET;
									ilBracketFlag = FALSE;
								}	/*	1. Wert eintragen und Statuswechsel	*/
								else if (ilValueCount == 1) {
									llVal2 = llValue; 
									ilValueCount++;
									memset(pclBuffer,0x00,MAX_VALUE_LEN);
									ilFlag = CLOSE_BRACKET;
									ilBracketFlag = FALSE;
								}	/*	1. Wert eintragen und Statuswechsel	*/
								else if (ilValueCount == 2) {
									llVal3 = atol(pclBuffer);
									ilValueCount++;
									ilFlag = STATUS_EXIT;
								}	/*	2. Wert eintragen und Statuswechsel -> Ende	*/
								else {
									pclErrorPointer[ili] = '*';
									ilRC = RC_FAIL;
								}
							}
						}	/*	Klammer ge�ffnet -> OK	*/
						else {
							pclErrorPointer[ili] = '*';
							ilRC = RC_FAIL;
						}	/*	Keine Klammer ge�ffnet -> Fehler	*/
					}
					else if 	(pcpSource[ili] == 0) {
						if ( (ilRC = GetKernelParamValue(pclBuffer,NULL,&llValue)) != RC_SUCCESS) {
							pclErrorPointer[ili] = '*';
							ilRC = RC_FAIL;
							ilFlag = STATUS_EXIT;
						}	/*	Wert des Strings nicht gefunden -> Fehler	*/
						else {
							if (ilValueCount == 0) {
								llVal1 = llValue; 
								ilValueCount++;
								memset(pclBuffer,0x00,MAX_VALUE_LEN);
								ilFlag = STATUS_EXIT;
							}	/*	1. Wert eintragen -> Ende	*/
							else if (ilValueCount == 1) {
								llVal2 = llValue; 
								ilValueCount++;
								memset(pclBuffer,0x00,MAX_VALUE_LEN);
								ilFlag = STATUS_EXIT;
							}	/*	1. Wert eintragen -> Ende	*/
							else if (ilValueCount == 2) {
								llVal3 = atol(pclBuffer);
								ilValueCount++;
								ilFlag = STATUS_EXIT;
							}	/*	1. Wert eintragen -> Ende	*/
							else {
								pclErrorPointer[ili] = '*';
								ilRC = RC_FAIL;
							}
						}
					}
					else {
						pclErrorPointer[ili] = '*';
						ilRC = RC_FAIL;
					}	/*	Zeichen ist ung�ltig	*/
					break;
				case	STATUS_CALC:
					if ( (pcpSource[ili] >= '0') && (pcpSource[ili] <= '9') ) {
						pclBuffer[0] = pcpSource[ili];
						ilBufCount = 1;
						ilFlag = STATUS_NUMBER;
					}	/*	Statuswechsel	*/
					else if ( ( (pcpSource[ili] >= 'a') && (pcpSource[ili] <= 'z') ) ||
							( (pcpSource[ili] >= 'A') && (pcpSource[ili] <= 'Z') ) ) {
						pclBuffer[0] = pcpSource[ili];
						ilBufCount = 1;
						ilFlag = STATUS_LETTER;
					}	/*	Statuswechsel	*/
					else if (pcpSource[ili] == '(') {
						ilFlag = OPEN_BRACKET;
					}	/*	Statuswechsel	*/
					else {
						pclErrorPointer[ili] = '*';
						ilRC = RC_FAIL;
					}
					break;
				case	OPEN_BRACKET:
					if ( (pcpSource[ili] >= '0') && (pcpSource[ili] <= '9') ) {
						pclBuffer[0] = pcpSource[ili];
						ilBufCount = 1;
						ilFlag = STATUS_NUMBER;
					}
					else if ( ( (pcpSource[ili] >= 'a') && (pcpSource[ili] <= 'z') ) ||
							( (pcpSource[ili] >= 'A') && (pcpSource[ili] <= 'Z') ) ) {
						pclBuffer[0] = pcpSource[ili];
						ilBufCount = 1;
						ilFlag = STATUS_LETTER;
					}
					else {
						pclErrorPointer[ili] = '*';
						ilRC = RC_FAIL;
					}
					break;
				case	CLOSE_BRACKET:
					if ( (pcpSource[ili] == '*') ||
						(pcpSource[ili] == '-') ||
						(pcpSource[ili] == '/') ||
						(pcpSource[ili] == '+') ) {
						if (ilCalcCount == 0) {
							clCalc1 = pcpSource[ili];
							ilCalcCount++;
						}
						else {
							clCalc2 = pcpSource[ili];
							ilCalcCount++;
						}
						ilFlag = STATUS_CALC;
					}
					break;
				default:
					break;
     		}	/*	end switch	*/
     	}	/*	end else	*/
     	if (ilFlag == STATUS_EXIT)
     		break;
     }	/*	end for	*/
    	if (ilRC == RC_SUCCESS) {
/*	nur test
fprintf(pardbg,"Point 8 -> Val1 = %ld, Val2 = %ld, Val3 = %ld\n",llVal1,llVal2,llVal3);
fflush(pardbg);
	nur test	*/
    		if (ilValueCount == 0)
    			ilRC = RC_FAIL;
    		else if (ilValueCount == 1) {
    			*plpValue = llVal1;
/*	nur test
fprintf(pardbg,"Point 8.1 -> Total = %ld\n",*plpValue);
fflush(pardbg);
	nur test	*/
    		}
    		else if (ilValueCount == 2) {
    			if (clCalc1 == '-')
    				*plpValue = llVal1 - llVal2;
    			else if (clCalc1 == '+')
    				*plpValue = llVal1 + llVal2;
    			else if (clCalc1 == '*')
    				*plpValue = llVal1 * llVal2;                                       
    			else if (clCalc1 == '/') {
    				if (llVal2 != 0)
    					*plpValue = llVal1 / llVal2;
    				else
    					ilRC = RC_FAIL;
    			}
			else
				ilRC = RC_FAIL;
/*	nur test
fprintf(pardbg,"Point 8.2 -> Total = %ld\n",*plpValue);
fflush(pardbg);
	nur test	*/
    		}
    		else if (ilValueCount == 3) {
    			if (clCalc1 == '*') {
    				llVal2 = llVal1 * llVal2;
	    			if (clCalc2 == '-')
    					*plpValue = llVal2 - llVal3;
    				else if (clCalc2 == '+')
    					*plpValue = llVal2 + llVal3;
    				else if (clCalc2 == '*')
    					*plpValue = llVal2 * llVal3;                                       
    				else if (clCalc2 == '/') {
    					if (llVal3 != 0)
    						*plpValue = llVal2 / llVal3;
    					else
    						ilRC = RC_FAIL;
    				}
    			}
    			else if (clCalc2 == '*') {
    				llVal2 = llVal3 * llVal2;
	    			if (clCalc1 == '-')
    					*plpValue = llVal1 - llVal2;
    				else if (clCalc1 == '+')
    					*plpValue = llVal1 + llVal2;
    				else if (clCalc1 == '/') {
    					if (llVal2 != 0)
    						*plpValue = llVal1 / llVal2;
    					else
    						ilRC = RC_FAIL;
    				}
    			}
    			else if (clCalc1 == '/') {
    				if (llVal2 != 0) {
    					llVal2 = llVal1 / llVal2;
	    				if (clCalc2 == '-')
    						*plpValue = llVal2 - llVal3;
    					else if (clCalc2 == '+')
    						*plpValue = llVal2 + llVal3;
    					else if (clCalc2 == '/') {
    						if (llVal3 != 0)
    							*plpValue = llVal2 / llVal3;
    						else
    							ilRC = RC_FAIL;
	    				}
    				}
				else
					ilRC = RC_FAIL;
    			}
    			else if (clCalc2 == '/') {
    				if (llVal3 != 0) {
	    				llVal2 = llVal2 / llVal3;
	    				if (clCalc1 == '-')
    						*plpValue = llVal1 - llVal2;
    					else if (clCalc1 == '+')
    						*plpValue = llVal1 + llVal2;
				}
				else
					ilRC = RC_FAIL;
    			}
    			if (clCalc1 == '+') {
    				llVal2 = llVal1 + llVal2;
	    			if (clCalc2 == '-')
    					*plpValue = llVal2 - llVal3;
    				else if (clCalc2 == '+')
    					*plpValue = llVal2 + llVal3;
    			}
    			else if (clCalc2 == '+') {
    				llVal2 = llVal3 + llVal2;
	    			if (clCalc1 == '-')
    					*plpValue = llVal1 - llVal2;
    				else if (clCalc1 == '+')
    					*plpValue = llVal1 + llVal2;
    			}
    			else {
    				*plpValue = llVal1 - llVal2 - llVal3;
    			}
    		}
	}
/*	nur test
fprintf(pardbg,"Point 9 -> return = %d\n",ilRC);
fflush(pardbg);
fprintf(pardbg,"10'%s'\n",pcpSource);
fprintf(pardbg,"10'%s'\n",pclErrorPointer);
fflush(pardbg);
fprintf(pardbg,"ExpressToVal: Return with value = %ld, rc = %d\n",*plpValue,ilRC);
fflush(pardbg);
	nur test	*/
	return ilRC;
}	/*	end of ExpressToVal()	*/
					
int	GetValueFromFile(char *pcpFile,char *pcpCrit,int ipCritPos,int ipDestPos,char *pcpDest,int ipDestLen,FILE *pFpDbgFile)
{
	int	ilRC = RC_SUCCESS;
	int	ili;
	int	rc;
	int	ilActLen = 0;
	int	ilFieldLen;
	int	ilCritLen;
	FILE	*FlFile;
	char	pclWord[THE_WORD_LEN];
	char	pclFieldVal[THE_WORD_LEN];
	char	*pclActDest;
	char	clLine[200];
	char	pclErrorText[200];

/*	Check for invalid Parameter	*/

	if ( (pcpFile == (char *) NULL) ||
		(pcpCrit == (char *) NULL) ||
		(pcpDest == (char *) NULL) ||
		(ipCritPos <= 0) ||
		(ipDestPos <= 0) ) {
		if (pcpFile == (char *) NULL) {
			sprintf(pclErrorText,"\nGetValueFromFile:\nFile Buffer is Pointer to NULL\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = RC_FAIL;
		}
		else if (pcpCrit == (char *) NULL) {
			sprintf(pclErrorText,"\nGetValueFromFile:\nCriterium Buffer is Pointer to NULL\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = RC_FAIL;
		}
		else if (pcpDest == (char *) NULL) {
			sprintf(pclErrorText,"\nGetValueFromFile:\nDestination Buffer is Pointer to NULL\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = RC_FAIL;
		}
		else if (ipCritPos <= 0) {
			sprintf(pclErrorText,"\nGetValueFromFile:\nInvalid Value for Criterium Word Position -> %d\n",ipCritPos);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = RC_FAIL;
		}
		else {
			sprintf(pclErrorText,"\nGetValueFromFile:\nInvalid Value for Destination Word Position -> %d\n",ipDestPos);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = RC_FAIL;
		}
	}
	else {
		ilCritLen = strlen(pcpCrit);
		if ( (pcpFile[0] == 0) || (ilCritLen == 0) ) {
			sprintf(pclErrorText,"\nGetValueFromFile:\nInvalid File Name, Starts with 0\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			ilRC = RC_FAIL;
		}	/*	Filename starts invalid -> Char 0	*/
		else {
			ilRC = access(pcpFile,R_OK);
               if (ilRC != RC_SUCCESS) {
				sprintf(pclErrorText,"GetValueFromFile:\nDidn't find File '%s' (ErrorValue is %d)\n",pcpFile,errno);
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
				ilRC = RC_FAIL;
			}	/*	File not found	*/
			else {
				FlFile = fopen(pcpFile,"r");
				if (FlFile == (FILE *) NULL) {
					sprintf(pclErrorText,"\nGetValueFromFile:\nUnable to open File '%s' (ErrorValue is %d)\n",pcpFile,errno);
					PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
					ilRC = RC_FAIL;
				}	/*	Couldn't open file	*/
				else {
					memset(pcpDest,0x00,ipDestLen);
					for (pclActDest = pcpDest,ili = 0; ilRC == RC_SUCCESS; ili++) {
						memset(clLine,0x00,200);
						if (fgets(clLine,200,FlFile) == (char *) NULL) {
							if (ili <= 0) {
								sprintf(pclErrorText,"\nGetValueFromFile:\nReading File '%s' Failed \n",pcpFile);
								PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
							}	/*	Couldn't even read one fucking line	*/
							ilRC = RC_FAIL;
						}	/*	no more lines to read	*/
						else {
if (debug_level == DEBUG) {
sprintf(pclErrorText,"GetKernelParamValue:\nReading File line '%s'\n",clLine);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
							/*	Prepare buffers with ' ' as delimiter between Words	*/
							ilRC = SubstTokOfLine(clLine,9,32);
							if (ilRC == RC_FAIL)
								break;
							ilRC = SubstTokOfLine(clLine,',',32);
							if (ilRC == RC_FAIL)
								break;
							if ( (ilRC = GetIWord(clLine,pclWord,THE_WORD_LEN,ipCritPos)) != RC_FAIL) {
if (debug_level == DEBUG) {
sprintf(pclErrorText,"Word on Pos %d is %s, searched word is '%s' (rc = %d, len = %d)\n",ipCritPos,pclWord,pcpCrit,ilRC,ilCritLen);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
								if (strncmp(pclWord,pcpCrit,ilCritLen) == 0) {
if (debug_level == DEBUG) {
sprintf(pclErrorText,"strncmp worked ok\n");
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
									ilRC = SubstTokOfLine(clLine,32,',');
									if (ilRC != RC_FAIL) {
										ilRC = GetFieldValByNo(clLine,pclFieldVal,ipDestPos,',');
										if (ilRC != RC_FAIL) {
if (debug_level == DEBUG) {
sprintf(pclErrorText,"GetFieldVal worked ok, Val ='%s'\n",pclFieldVal);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
sprintf(pclErrorText,"ActLen: %d,FieldVal: %d, sum + 1: %d, DestLen: %d\n",ilActLen,strlen(pclFieldVal),(ilActLen + strlen(pclFieldVal) + 1),ipDestLen);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
											if ( (ilFieldLen = strlen(pclFieldVal)) < ipDestLen ) {
												memcpy(pclActDest,pclFieldVal,ilFieldLen);
												ilRC = RC_SUCCESS;
												break;
											}	/*	Write Word to Destination-Buffer	*/
											else {
if (debug_level == DEBUG) {
sprintf(pclErrorText,"GetFieldVal Faild\n");
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
												ilRC = RC_FAIL;
											}	/*	Buffer already full	*/
										}	/*	Copy Field to Destination-Buffer	*/
										else {
											sprintf(pclErrorText,"Couldn't find Field Value for Field '%s'\n'",pcpCrit);
											PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
										}	/*	Couldn't read indicated Word	*/
									}	/*	Third Preparation for Buffer workd ok, Read field-Value	*/
									else {
										sprintf(pclErrorText,"Error preparing Field String for Field '%s'\n'",pcpFile);
										PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
									}	/*	Third Preparation for Buffer failed	*/
								}	/*	Found a correct line, get field-value	*/
								else {
if (debug_level == DEBUG) {
sprintf(pclErrorText,"strncmp Faild\n");
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
									;
								}	/*	This wasn't the expected Word on this position, go on	*/
							}	/*	GetIWord() failed, go on	*/
							ilRC = RC_SUCCESS;
						}	/*	reading a line from file succeeded	*/
					}	/*	end for	*/
				}	/*	end else	*/
			}	/*	end else	*/
		}	/*	end else	*/
	}	/*	end else	*/
	fclose(FlFile);
	return ilRC;
}	/*	end of GetKernelParamValue()	*/

/*	********************************************************************************	*/
/*	Funktion GetConfigSection()												*/
/*	Die Funktion sucht die gew�nschte Sektion in der �bergebenen Datei und schreibt alle	*/
/*	gefunden Werte in den ebenfalls �bergebenen Puffer.							*/
/*	Kann der gew�nschte Bereich nicht gefunden werden, oder hat ein Parameter einen un-	*/
/*	g�ltigen Wert, gibt die Funktion den Fehlercode RC_FAIL zur�ck. Im Erfolgsfall gibt	*/
/*	die funktion die Anzahl der gufunden Eintr�ge zur gesuchten Sektion zur�ck			*/
/*	int	GetConfigSection(char *pcpKeyWord,CONFPAR *pSpBuffer,FILE *pFConf,FILE *pFDbg)	*/
/*	IN	char		*pcpKeyWord, Namen der gesuchten Sektion						*/
/*	OUT	CONFPAR	*pSpBuffer, Ergebnis-Puffer f�r gefundene Eintr�ge der Sektion		*/
/*	IN	FILE		*pFConf, Konfigurationsdatei									*/
/*	IN	FILE		*pFDbg, Debugdatei											*/
/*	Returns	int	Anzahl der gefundenen Eintr�ge								*/
/*				RC_FAIL													*/
/*	********************************************************************************	*/

int	GetConfigSection(char *pcpKeyWord,CONFPAR *pSpBuffer,FILE *pFConf,FILE *pFDbg)
{
	int		ilRC = RC_FAIL;
	FILE		*pFlConfigFile;
	char		clLine[200];
	char		clWord[20];
	int		ilWordLen = 0;
	int		ilKeyWordLen = 0;
	int		ili;


	if (	(pcpKeyWord == (char *) NULL) ||
		(pSpBuffer == (CONFPAR *) NULL) ||
		(pFConf == (FILE *) NULL) ) {
		if (pFDbg != (FILE *) NULL) {
			fprintf(pFDbg,"Routine GetConfigSection failed with Parameter Problems\n");
			fflush(pFDbg);
		}
		return ilRC;
	}
	pFlConfigFile = pFConf;
	ilKeyWordLen = strlen(pcpKeyWord);
	while (fgets(clLine,200,pFlConfigFile) != (char *) NULL) {
		if ( (ilWordLen = GetFirstWord(clLine,clWord,20)) != RC_FAIL) {
			if (strncmp(clWord,pcpKeyWord,ilKeyWordLen) == 0) {
				for (ili = 0; ili < MAX_PARAMS; ili++) {
					if (fgets(clLine,200,pFlConfigFile) != (char *) NULL) {
						if ( (ilWordLen = GetFirstWord(clLine,clWord,20)) != RC_FAIL) {
							if (strcmp(clWord,"[END_PARAMETER]") == 0) {
								break;
							}
							else if (	(clWord[0] != 0) &&
									(clWord[0] != 13) &&
									(clWord[0] != 32) &&
									(clWord[0] != 9) &&
									(clWord[0] != 10) &&
									(clWord[0] != EOF) ) {
								strncpy(pSpBuffer[ili].name,clWord,ilWordLen);
							}	/*	end if strcmp()	*/
							else {
								if (pFDbg != (FILE *) NULL) {
									fprintf(pFDbg,"Section %s not properly ended, didn' find '[END_PARAMETER]\n",pcpKeyWord);
									fflush(pFDbg);
								}
								else {
									printf("Section %s not properly ended, didn' find '[END_PARAMETER]\n",pcpKeyWord);
									fflush(stdout);
								}
								ili = RC_FAIL;
								break;
							}
						}	/*	end if GetFirstWord()	*/
						else {
							if (pFDbg != (FILE *) NULL) {
								fprintf(pFDbg,"Section %s not properly ended, didn' find '[END_PARAMETER]\n",pcpKeyWord);
								fflush(pFDbg);
							}
							else {
								printf("Section %s not properly ended, didn' find '[END_PARAMETER]\n",pcpKeyWord);
								fflush(stdout);
							}
							ili = RC_FAIL;
							break;
						}
					}	/*	end if fgets()	*/
					else {
						if (pFDbg != (FILE *) NULL) {
							fprintf(pFDbg,"Section %s not properly ended, didn' find '[END_PARAMETER]\n",pcpKeyWord);
							fflush(pFDbg);
						}
						else {
							printf("Section %s not properly ended, didn' find '[END_PARAMETER]\n",pcpKeyWord);
							fflush(stdout);
						}
						ili = RC_FAIL;
						break;
					}
				}	/*	end for	*/
				ilRC = ili;
				break;
			}	/*	end if strcmp()	*/
		}	/*	end if GetFirstWord()	*/
	}	/*	end if fgets()	*/
	return ilRC;
}	/*	end of GetConfigSection()	*/

