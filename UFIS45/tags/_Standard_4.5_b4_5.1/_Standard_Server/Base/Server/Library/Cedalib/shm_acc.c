#ifndef _DEF_mks_version_shm_acc_c
  #define _DEF_mks_version_shm_acc_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_shm_acc_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/shm_acc.c 1.2 2004/08/10 20:29:43SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#define		UGCCS_FKT
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <glbdef.h>

#define FAILURE (char *) -1
#define TRUE  1
#define FAILED -1

/**************************  Function prototype header **********************/
extern  int shm_ass(long size,long func_perm);
extern  char *shm_acc(long shmid,long function,long size,struct _gbladr *pgbladr);
extern  int clean(char *base,long shmid,struct shmid_ds *pshmds);
/****************************************************************************/


/*extern char *shmat();   shared memory access routine */



shm_dyn(register long size,register long func_perm)
{
	int created = TRUE;
	register long shmid;

	errno = 0;
	if ((shmid = shmget(IPC_PRIVATE,size,func_perm)) == FAILED)
	{
      created = FAILED; /* failed */
	    dbg(TRACE,"shm_dyn: shmid=(%d),errno=(%d),strerror=(%s)"
				,shmid,errno,strerror(errno));

			/* NEVER use this, because you will overwrite the sgs.tab inside the shared memory */
			/* reason: the KEY define is used by SYSMON to create the SHM-sgs.tab */
			/**************************************************************************/
			#ifdef NULL
			errno = 0;
	    if ((shmid = shmget(KEY,size,0)) == FAILED)
	    {
				dbg(TRACE,"shm_dyn(key): shmid=(%d),errno=(%d),strerror=(%s)"
					,shmid,errno,strerror(errno));
         created = FAILED; /* failed */
				/* perror("SHMGET:"); */
			}
			else
			{
				#ifdef MORE_DEBUG
				dbg(TRACE,"shm_dyn(key): return SHM-ID(%d)",shmid);
				#endif
			}
			#endif
			/**************************************************************************/
			/* perror("SHMGET1:"); */
	}
	return((created == TRUE) ? shmid : created);
}

shm_ass(register long size,register long func_perm)

{
	int created = TRUE;
	register long shmid;

	if ((shmid = shmget(KEY,size,func_perm)) == FAILED) {
	    if ((shmid = shmget(KEY,size,0)) == FAILED) {
                created = FAILED; /* failed */
		/* perror("SHMGET:"); */
		}
		/* perror("SHMGET1:"); */
	}

	return((created == TRUE) ? shmid : created);
}


char *shm_acc(register long shmid,register long function,long size,GBLADR *pgbladr)

{
	register char *base = FAILURE;

        if ((base = (char *)shmat(shmid,NULL,function)) == FAILURE)
		{
            /* perror("\n shmat failed"); */
	} else {
            pgbladr->stiadr[0] = base;
	    pgbladr->stiadr[1] = base + size;
            }
	return(base);
}


/* Detach and remove the shared memory segment */

clean(register char *base,register long shmid,register struct shmid_ds *pshmds)

{
	
	shmdt(base);
	shmctl(shmid,IPC_RMID,pshmds);
	
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
