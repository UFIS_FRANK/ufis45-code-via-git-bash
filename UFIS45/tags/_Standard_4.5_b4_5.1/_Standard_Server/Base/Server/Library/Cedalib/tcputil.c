#ifndef _DEF_mks_version_tcputil_c
  #define _DEF_mks_version_tcputil_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tcputil_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/tcputil.c 1.5 2006/01/21 00:42:22SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*	                      						*/
/* CEDA Program Skeleton				      		*/
/*						            		*/
/* Author		: Jochen Beese				       	*/
/* Date			: 18/07/94	               			*/
/* Description		: TCP/IP Utility Functions 	       	      	*/
/*						             		*/
/* Update history	:				     		*/
/* 20060117 JIM: h_errno not defined on AIX                             */
/* Functions:				                            	*/
/*				                                    	*/
/* int tcp_create_socket (int type, char *Pservice);	        	*/
/* int tcp_open_connection (int socket, char *Pservice,char *Phostname);*/
/* int tcp_send_data (char *Pdata, int len, int socket);	       	*/
/* int tcp_forward_data (COMMIF *data, int que_out);	        	*/
/* int tcp_send_shutdown (int sock);				      	*/
/*					                   		*/
/* ******************************************************************** */


/* The master header file */
 
#include  <sys/types.h>
#include  <sys/times.h>
#include  <sys/socket.h>

#if defined(_UNIXWARE) || defined(_LINUX)
#include  <sys/time.h>
#endif

#ifndef _HPUX_SOURCE
	#include  <sys/select.h>
#else
	#include  <time.h>
#endif


#include  "glbdef.h"
#include  "netin.h"
#include  "tcputil.h"


#ifdef MAX_EVENT_SIZE
#undef MAX_EVENT_SIZE
#endif
#define MAX_EVENT_SIZE (sizeof(EVENT) + PACKET_LEN)

/* has ONLY to be used if we need a OS-bug workaround for the functions */
/* ntohs,htons,ntohl,htonl - for byteorder conversion                   */
/* #define _BYTEORDERBUG */

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */

/* ******************************************************************** */
/* Global variables							*/
/* ******************************************************************** */

static	int	CfgSegSize = PACKET_LEN-12; /* is 1024 */

static int igHostByteOrder = 0; /* global flag for the used host byte order */
                                /* 1=LITTLE ENDIAN ; 2=BIG ENDIAN           */
                                /* (only used if OS-bug is present)         */
 
/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */

extern  int	send_message(int,int,int,int,char*);
extern void snap (char *buf, int len);
#ifndef _HPUX_SOURCE
#ifndef _WINNT
#ifndef _AIX
extern int  h_errno;
#endif
#endif
#endif
    
/* ******************************************************************** */
/* Static Function prototypes						*/
/* ******************************************************************** */

void SetHostByteOrder(int *ipHbo);


/* ******************************************************************** */
/* Following the tcp_create_socket function				*/
/* Creates socket and binds a service address to it, if Pservice is not */
/* NULL									*/
/* ******************************************************************** */
int tcp_create_socket (int type, char *Pservice)
{
  int	ilRC;
  int	rc = RC_SUCCESS;	/* Return code */
  int	sock = 0;
  int	ilKeepalive = 0;
  size_t	ilRecLen = 0;
  struct 	servent 	*sp;
  struct	sockaddr_in	name;

#if defined(_WINNT)
  int bool_value=1;	/* Arg for setsockopt */
  int *opt_bool=&bool_value;
  struct linger lingstruct;	/* Arg for setsockopt */
  struct linger *ling=&lingstruct;	/* Arg for setsockopt */
#else
  int opt_bool=1;	/* Arg for setsockopt */
  struct linger ling;	/* Arg for setsockopt */
#endif

  sock = socket(AF_INET, type, 0); /* create socket */

  if (sock < 0 )
  {
    dbg(TRACE,"tcp_create_socket: Error can't open socket: %s", strerror( errno));
    rc = RC_FAIL;
  }
  else
  {
    dbg(DEBUG,"tcp_create_socket: Socket %d opened", sock);
    if (Pservice != NULL)
    {
      if((sp = getservbyname(Pservice, NULL) ) == NULL )
      {
	dbg(TRACE,"tcp_create_socket: unknown service %s", Pservice); 
	rc = RC_FAIL;
      }
    }
  }


  if(rc == RC_SUCCESS)
  {
    ilKeepalive = 0;

#if defined(_UNIXWARE) || defined(_SOLARIS) || defined(_LINUX) || defined(_HPUX113_SOURCE)
    ilRecLen = sizeof(int);
#endif

  /* int getsockopt(int s, int level, int optname, void *optval, size_t *optlen); */

    rc = getsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, &ilRecLen);
    if (rc == -1 )
    {
      rc = RC_FAIL;
      dbg(TRACE,"tcp_create_socket: getsockopt <%s>", strerror(errno));
    }
    else
    {
      if(ilKeepalive == 1)
      {
        dbg(DEBUG,"tcp_create_socket: SO_KEEPALIVE already set");
      }
      else
      {
        ilKeepalive = 1;
        rc = setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, sizeof(ilKeepalive));
        if (rc == -1 )
        {
          rc = RC_FAIL;
          dbg(TRACE,"tcp_create_socket: setsockopt <%s>", strerror(errno));
        }
        else
        {
          dbg(DEBUG,"tcp_create_socket: SO_KEEPALIVE set");
        }/* end of if */
      }/* end of if */
    }/* end of if */
  }/* end of if */


  if (rc == RC_SUCCESS)
  {
    rc = setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, (char *) &opt_bool, sizeof (opt_bool));
    if (rc == -1 )
    {	
      dbg(TRACE,"tcp_create_socket: Error Setsockopt REUSEADDR: %s", strerror(errno));
      rc = RC_FAIL;
    }
  }


  if (rc == RC_SUCCESS)
  {
#if defined(_WINNT)
    lingstruct.l_onoff = 0;
    lingstruct.l_linger = 0;
#else
    ling.l_onoff = 0;
    ling.l_linger = 0;
#endif
    rc = setsockopt (sock, SOL_SOCKET, SO_LINGER, (char *) &ling, sizeof (ling));
    if (rc == -1 )
    {	
      dbg(TRACE,"tcp_create_socket: Error Setsockopt LINGER: %s", strerror(errno));
      rc = RC_FAIL;
    }
  }


#if defined(_WINNT)
      rc = RC_SUCCESS;
#endif

  if (Pservice != NULL && rc == RC_SUCCESS)
  {
    dbg(DEBUG,"tcp_create_socket: got services %s", Pservice); 
  
    name.sin_addr.s_addr = INADDR_ANY;
    name.sin_family = AF_INET;
    name.sin_port   = (u_short) sp->s_port;
  
#ifdef _WINNT
    while ((rc = bind(sock, &name, sizeof(struct sockaddr_in))) < 0)
#else
	 errno = 0;
    while ((rc = bind(sock, (struct sockaddr *) &name, sizeof(struct sockaddr_in))) < 0)
#endif
    {
      dbg(DEBUG,"tcp_create_socket: bind returns: %d - sleep 10 seconds now -> <%s>", rc, strerror(errno));
      sleep(10);
    }
  }

  if (rc < 0) /* check if there was a recent error */
    {
      close(sock); /* close the socket to prevent an unlimited file opening! */
    }

  if (rc == RC_SUCCESS)
  {
    dbg(DEBUG,"tcp_create_socket: return sock %d",sock);
    return sock;
  }
  else
  {
    dbg(DEBUG,"tcp_create_socket: return RC_FAIL");
    return RC_FAIL;
  }

} /* end of create_socket */



/*******************************************************************
    NAME:       tcp_open_connection
    SYNOPSIS:   Opens a connection to the specified host & port (service)  
                on the specified socket.
    RETURNS:    RC_SUCCESS if successful, RC_FAIL if not.
********************************************************************/
int tcp_open_connection (int socket, char *Pservice, char *Phostname)
{
  int rc=RC_FAIL;
  int i = 0;
  struct hostent *hp;
  struct sockaddr_in	dummy;
  int serr=0;
  struct servent *sp;

  if (( hp = gethostbyname( Phostname)) == NULL )
  {
    dbg(TRACE,"tcp_open_connection: ERROR gethostbyname(%s): %s",
	       Phostname, strerror(errno));
    return RC_FAIL;
  }

  dbg(DEBUG,"tcp_open_connection: got hostname %s", Phostname);

  if (( sp = getservbyname(Pservice, NULL) ) == NULL )
  {
    dbg(TRACE,"tcp_open_connection: ERROR getservbyname(%s): %s",
               Pservice, strerror(errno) );
    return RC_FAIL;
  } 

  dbg(DEBUG,"tcp_open_connection: got service %s", Pservice);

  dummy.sin_port = sp->s_port;
  memcpy((char *) &dummy.sin_addr, *hp->h_addr_list, hp->h_length);
  dummy.sin_family = AF_INET;

  rc = connect(socket, (struct sockaddr *) &dummy, sizeof(dummy));
  if (rc < 0)
  {
    dbg(TRACE,"tcp_open_connection: ERROR while connect: %s",
               strerror(errno));
    return RC_FAIL;
  }

  dbg(DEBUG,"tcp_open_connection: got connection"); 
  return RC_SUCCESS;                                         

} /* end of tcp_open_connection */                                         


/* ******************************************************************** */
/* Following the tcp_send_data function					*/
/* Sends the data (len bytes) on the (connected) socket	and receives	*/
/* the ack								*/
/* ******************************************************************** */
int tcp_send_data (char *Pdata, int len, int socket)
{
  int	  remain;
  int	  send_len;
  int	  rc;
  COMMIF *ldata;
  COMMIF  ack;
  char	 *offs;

  ldata  = (COMMIF *)malloc (CfgSegSize + sizeof(COMMIF));
  offs	= Pdata;
  remain = len;
  do {
    if ( remain > CfgSegSize ) {
      ldata->command = htons (PACKET_DATA);
      ldata->length = htonl (CfgSegSize);
      memcpy (ldata->data, offs, CfgSegSize);
      remain -= CfgSegSize;
      offs+=CfgSegSize;
    } /* end if */
    else {
      ldata->length = htonl (remain);
      ldata->command = htons (PACKET_END);
      memcpy (ldata->data, offs, remain);
      remain = 0;
    } /* end else */
    send_len = ntohl (ldata->length) + sizeof(COMMIF);
    rc = write(socket,(char *)ldata, send_len);

    if ( rc <0 ) {
      dbg(TRACE,"tcp_send_data: ERROR %d : %s ",rc,
	      strerror(errno));
      remain = 0;
      sleep(1);
    } /* end if */
    else 
    {
      /* dbg(DEBUG,"tcp_send_data: (write: rc= %d) send %d bytes\t\t",rc,send_len);
      */
      rc = read (socket, (char *)&ack, sizeof(COMMIF));
      /* dbg(DEBUG,"tcp_send_data: (rc=%d) received ACK",rc);  */
      if ( rc != sizeof(COMMIF)) {
	sleep(5);
	dbg(TRACE,"tcp_send_data: READ ACK: %s ",strerror(errno));
	
	rc = RC_FAIL;
	remain = 0;
      } /* end if */
    } /* end else */
  } while (remain >0 );		

  free(ldata);
  if (rc >= 0)
    rc = RC_SUCCESS;

  return rc;
} /* tcp_send_data */


/* ******************************************************************** */
/* Following the tcp_send_shutdown function				*/
/* Sends the NET_SHUTDOWN-Command on the connected socket and closes	*/
/* the connection (not the socket !)					*/
/* ******************************************************************** */
int tcp_send_shutdown (int sock)
{
  int rc = RC_SUCCESS;
  COMMIF *ldata;
  COMMIF  ack;

  ldata  = (COMMIF *) malloc (CfgSegSize + sizeof(COMMIF));
  ldata->command = htons (NET_SHUTDOWN);
  ldata->length = htonl (CfgSegSize);
  rc = write (sock, (char *)ldata, CfgSegSize + sizeof(COMMIF) );

  if ( rc < 0) {
    dbg(TRACE,"tcp_send_shutdown: ERROR %d : %s ",rc,strerror(errno));
    sleep(1);
  } /* end if */
  else 
  {
    rc = read (sock, (char *)&ack, sizeof(COMMIF));
    dbg(DEBUG,"tcp_send_shutdown: (%d) received ACK",rc); 
    
    shutdown (sock, 2);
    dbg(DEBUG,"tcp_send_shutdown: Send_shutdown successful");
  }
  free(ldata);
} /* send_shutdown */

/* ******************************************************************** */
/* Following the tcp_forward_data function				*/
/* Puts the data on the qcp queue					*/
/* ******************************************************************** */
int tcp_forward_data (COMMIF *data, int que_out)
{
  int	len;
  int	rc = RC_SUCCESS;
  EVENT	*out_event;
  char *offs;

  len = data->length+sizeof(EVENT);
  dbg(DEBUG,"tcp_forward_data: Forwarding %d bytes to %d",len, que_out); 
  
  
  out_event = (EVENT *)calloc(len, 1);
  if( out_event == (EVENT *)NULL) {
    dbg(TRACE,"tcp_forward_data: Malloc Error"); 
    return RC_FAIL;
  } /* end if */

  out_event->command = EVENT_DATA;
  out_event->originator = (short) mod_id;
  out_event->data_length = (short) data->length;
  out_event->data_offset = sizeof(EVENT);
  memcpy ((char *) out_event + sizeof(EVENT), data->data, data->length);

  /*snap(offs,data->length); */

  rc = que(QUE_PUT,que_out,mod_id,3,len,(char *)out_event);
  free(out_event);

  if (rc != RC_SUCCESS)
  {
    dbg(DEBUG,"tcp_forward_data: returned %d:",rc);
  } /* fi */
  return rc;
} /* end of forward_data */


/* ******************************************************************** */
/* Following the tcp_wait_timeout function				*/
/* waits t seconds for received data on the socket/file descriptor sock	*/
/* returnd RC_FAIL, if error or RC_NOT_FOUND if timeout			*/
/* ******************************************************************** */
int tcp_wait_timeout (int sock, int t)
{
  int rc = RC_SUCCESS;
  int maxfdp1 = 0; /* added by eth */
  fd_set readfds;    
  struct timeval timeval;
  int cnt = 0;
	
  FD_ZERO(&readfds) ;
  FD_SET(sock,&readfds) ;
  maxfdp1 = sock+1;
  timeval.tv_sec = t;
  timeval.tv_usec = 0;

#ifndef _HPUX_SOURCE
  cnt = select (maxfdp1, &readfds, NULL, NULL, &timeval);
#else
  cnt = select (maxfdp1, (int *) &readfds, NULL, NULL, &timeval);
#endif
        
  if (cnt == -1)
  {
    dbg(TRACE, "tcp_wait_timeout: select err:sock=%d rc=%d : %s"
	    ,sock,rc,strerror(errno));  	
    rc = RC_FAIL;
  } /* end if */
    
  if (cnt == 0 )
  {
    /* dbg(TRACE, "tcp_wait_timeout: select timeout"); */
    rc = RC_NOT_FOUND;
  } /* fi */

  return rc ;
} /* tcp_wait_timeout */

/* ******************************************************************** */
/* The tcp_send_datagram routine					*/
/* ******************************************************************** */
int tcp_send_datagram (int sock, char *Phostadr, char *Phostname, 
		       char *Pservice, char *Pdata, int len)
{
  int rc = RC_SUCCESS;
  struct servent *sp;
  struct sockaddr_in target;
  struct hostent *remote_adr;

  memset ((char *) &target, 0, sizeof (struct sockaddr_in));

  if (Phostadr == NULL)
  {	/* A host name is given */
    dbg (DEBUG,"tcp_send_datagram: addr=<%s> name=<%s> serv=<%s> len=<%d>",Phostadr, 
			 Phostname,Pservice, len);

		errno = 0;
#if !(defined(_HPUX_SOURCE) || defined(_WINNT))
		h_errno = 0;
#endif
    remote_adr = gethostbyname (Phostname);
    if (remote_adr == NULL)
    {
#if defined(_HPUX_SOURCE) || defined(_WINNT)
      dbg(DEBUG,"tcp_send_datagram: gethostbyname(%s) errno<%d>=<%s>", 
	  		Phostname, errno,strerror(errno));
#else
      dbg(DEBUG,"tcp_send_datagram: gethostbyname(%s) h_errno<%d>=<%s>", 
	  		Phostname,h_errno,strerror(h_errno));
#endif
      rc = RC_NOT_FOUND;
    }
    else
    {
      dbg (DEBUG,"tcp_send_datagram: Host info: name=<%s> len=<%d> adr1=<%x>"
	   		,remote_adr->h_name, remote_adr->h_length,remote_adr->h_addr);
      dbg (DEBUG,"tcp_send_datagram: Send answer to name=<%s>",
	   		remote_adr->h_name);
      memcpy((char *) &target.sin_addr, *remote_adr->h_addr_list, 
	     remote_adr->h_length);
    } /* fi */
  } 
  else
  {	/* The address is given */
    dbg (DEBUG,"tcp_send_datagram: name=<%s> addr=<%s> serv=<%s> len=<%d>",Phostname, 
			 Phostadr,Pservice, len);
    sscanf(Phostadr,"%8x",(char *)&(target.sin_addr));
  } /* fi */ 

  if (rc == RC_SUCCESS)
  {
    if ((sp=getservbyname(Pservice,NULL) ) == NULL )
		{
      dbg (TRACE,"tcp_send_datagram: unknown service <%s>",Pservice);
      rc = RC_FAIL;
    } /* end if */
  } /* end if */

  if (rc == RC_SUCCESS)
  {
    target.sin_addr.s_addr = htonl(target.sin_addr.s_addr);
    target.sin_family = AF_INET;
    target.sin_port   = (u_short) sp->s_port;
    dbg (DEBUG,"tcp_send_datagram: target address<%8x> port<%x>",(int) (target.sin_addr.s_addr),
					target.sin_port);

    errno=0;
    rc = sendto(sock, Pdata, len, 0, (struct sockaddr *) &target, 
		 						sizeof (struct sockaddr_in));

    if (rc < 0)
    {
      rc = RC_FAIL;
      dbg(TRACE,"tcp_send_datagram: sendto() errno<%d>=<%s>",errno,strerror(errno));
    }
    else
    {
      rc = RC_SUCCESS;
    } /* fi */
  } /* fi */

  dbg(DEBUG,"tcp_send_datagram: returns <%d>",rc);
} /* tcp_send_datagram */


/* ******************************************************************** 
 * Function:  	TcpGetHostname
 * Parameter:	OUT:	pcpMyName	Host name buffer
 *		IN:	prpRecCmdblk	Length of buffer
 * Return:	RC_SUCCESS	OK
 *		RC_FAIL		TCPIP-Error
 * Description:	Returns the short host name (without domain and country code)
 *		of the own host
 * ******************************************************************** */
int TcpGetHostname (char *pcpMyName, int ipLen)
{
  int	ilRc = RC_SUCCESS;	/* Return code 	       */
  int ilI=0;

  ilRc = gethostname(pcpMyName, ipLen);
  for (ilI=0; pcpMyName[ilI] != EOS &&  pcpMyName[ilI] != '.';  ilI++)
    ;
  pcpMyName[ilI] = EOS;

  dbg(DEBUG,"TcpGetHostname: returns %s rc=%d", pcpMyName, ilRc);
  return ilRc;
} /* TcpGetHostname */
/* ******************************************************************** 
 * Function:  	SetHostByteOrder
 * Parameter:	IN:	ipHbo = variable which has to be set to 1 or 2
 * Return:	none                     
 * Description:	sets the variable to 1(LITTLE ENDIAN) or 2(BIG ENDIAN)
 *             	byteorder
 * ********************************************************************/
void SetHostByteOrder(int *ipHbo)
{
  short slTest = 65;
  char pclStrg[16];
  memcpy(pclStrg,&slTest,sizeof(short));
	if (pclStrg[0] == 65)
	{
	  *ipHbo = 1;
	  dbg(TRACE,"SetHostByteOrder: SYSTEM-BYTEORDER = <LITTLE ENDIAN>",*ipHbo);
	}
	else
	{
	  *ipHbo = 2;
	  dbg(TRACE,"SetHostByteOrder: SYSTEM-BYTEORDER = <BIG ENDIAN>",*ipHbo);
	}
} 
/* ******************************************************************** 
 * Function:  	aat_ntohs
 * Parameter:	IN:	spNet	Value in Network Byte order
 * Return:	value in Host byte order
 * Description:	CCS/AAT's version of ntohs for possible OS-bugs in the future
 * ******************************************************************** */
 short aat_ntohs(unsigned short spNet)
 {
   #ifndef _BYTEORDERBUG
    return ntohs(spNet);
   #else
    if (igHostByteOrder == 0)
	    {SetHostByteOrder(&igHostByteOrder);}

		if (igHostByteOrder == 1)
		  {return ((((short)(spNet) & 0x00ff) << 8) | (((short)(spNet) & 0xff00) >> 8));}
		else
		  if (igHostByteOrder == 2)
    		{return spNet;}
     	else
    		{dbg(TRACE,"aat_ntohs: ILLEGAL HOST-BYTEORDER SET!");}
  #endif
}
/* ******************************************************************** 
 * Function:  	aat_htons
 * Parameter:	IN:	spHost	Value in Host Byte order
 * Return:	value in Network byte order
 * Description:	CCS/AAT's version of htons for possible OS-bugs in the future
 * ******************************************************************** */
short aat_htons(unsigned short spHost)
{
   #ifndef _BYTEORDERBUG
	   return htons(spHost);
	 #else
	   if (igHostByteOrder == 0)
	      {SetHostByteOrder(&igHostByteOrder);}

	   if (igHostByteOrder == 1)
		    {return ((((short)(spHost) & 0x00ff) << 8) | (((short)(spHost) & 0xff00) >> 8));}
		 else
		    if (igHostByteOrder == 2)
		       {return spHost;}
			  else
				   {dbg(TRACE,"aat_htons: ILLEGAL HOST-BYTEORDER SET!");}
		#endif
}
/* ******************************************************************** 
 * Function:  	aat_ntohl
 * Parameter:	IN:	lpNet	Value in Network Byte order
 * Return:	value in Host byte order
 * Description:	CCS/AAT's version of ntohl for possible OS-bugs in the future
 * ******************************************************************** */
long aat_ntohl(unsigned long lpNet)
{
  #ifndef _BYTEORDERBUG
	  return ntohl(lpNet);
  #else
    if (igHostByteOrder == 0)
      {SetHostByteOrder(&igHostByteOrder);}

    if (igHostByteOrder == 1)
	    {return ( (((lpNet) & 0x000000ff) << 24) | (((lpNet) & 0x0000ff00) << 8) |
      (((lpNet) & 0x00ff0000) >> 8) | (((lpNet) & 0xff000000) >> 24));}
	  else
      if (igHostByteOrder == 2)
        {return lpNet;}
      else
        {dbg(TRACE,"aat_ntohl: ILLEGAL HOST-BYTEORDER SET!");}
	  #endif
}
/* ******************************************************************** 
 * Function:  	aat_htonl
 * Parameter:	IN:	lpHost	Value in host Byte order
 * Return:	value in network byte order
 * Description:	CCS/AAT's version of htonl for possible OS-bugs in the future
 * ******************************************************************** */
long aat_htonl(unsigned long lpHost)
{

	#ifndef _BYTEORDERBUG
     return htonl(lpHost);
  #else
    if (igHostByteOrder == 0)
       {SetHostByteOrder(&igHostByteOrder);}

    if (igHostByteOrder == 1)
	     {return ( (((lpHost) & 0x000000ff) << 24) | (((lpHost) & 0x0000ff00) << 8) |
       (((lpHost) & 0x00ff0000) >> 8) | (((lpHost) & 0xff000000) >> 24));}
    else
       if (igHostByteOrder == 2)
         {return lpHost;}
      else
        {dbg(TRACE,"aat_ntohl: ILLEGAL HOST-BYTEORDER SET!");}
   #endif
}  
/* ******************************************************************** */
/* Space for Appendings (Source-Code Control)				*/
/* ******************************************************************** */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
