#ifndef _DEF_mks_version_sth_c
  #define _DEF_mks_version_sth_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_sth_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/sth.c 1.2 2004/08/10 20:29:47SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/




#define UGCCS_FKT
/*$:
     ------------------------------------------------------------


        S  Y  S  T  E  M   T  A  B  L  E   H  A  N  D  L  E  R


     ------------------------------------------------------------

        Version   :  0.0       May 1989
        Programmer:  KJK
        Name      :  STH

        This module is an interface between the application tasks
        and the system tables.

        There are two types of system tables:

        -       record access tables
        -       normal tables

        Record access tables have fixed size records which may be
        accessed separately.
        Normal tables have no record structure.

	The following function codes are implemented:
	---------------------------------------------

	   Mnemonic:    Explanation 
	   --------     -----------	
	1. STTABLE	a single system table, may be combined with
			all function codes except 2,10 and 11
	2. STRECORD	a single record within a record table, may
			be combined with all function codes except
			1 and 3 
	3. STPART	only a part of a system table,
			may be combined with all function codes except
			3, 7, 8, 10 and 11
	4. STUNLOCK	unlock either a system table or a record, may
                  	be combined with all function codes except 5, 7,
			8, 9, 10 and 11 
	5. STLOCK	lock either a system table or a record, may be
			combined with all function codes except 4, 6,
			7, 8, 10 and 11 
	6. STWRITE      write a part of system table, a system table or
			a record, may be combined with all function codes
			except 5, 7, 8, 9, 10 and 11
	7. STSEARCH	search for a definite pattern in a system table,
			may be combined with all function codes except
			3, 4, 5, 6, 10 and 11
	8. STLOCATE	locate a system table or a record, may be combined
			with all function codes except 4, 5, 6, 10 and 11 
	9. STREAD	read a part of a system table, a table or a record,
			may be combined with all function codes except 4, 6,
			9, 10 and 11
       10. STDELETE	delete a record, must be combined with 2
     
       11. STINSERT	insert a new record, must be combined with 2, 
	                may be combined with 12

       12. STSORT       insert a new record, must be combined with 2 and 11   

	R E T U R N  C O D E S
	----------------------

	Mnemonic	Explanation
	--------	-----------

	STNORMAL	normal return

	STINVFCT	either an unknown function code or an invalid com-
			bination of function codes

	STTNLKD		from a function code STUNLOCK, the system table was
			not locked by you

	STRNLKD		from a function code STUNLOCK, the record was not
			locked by you

	STTALKD		from a function code STLOCK, the table was already
			locked by you

	STRALKD		from a function code STLOCK, the record was already
			locked by you

	STINVTAB	the system table no. in 'bcb->tabno' was smaller than
			1, greater than current maximum or not configured

	STINVREC	the record no. was either smaller than 0 or greater
			than the maximum record no. of the table

	STINVACT	both STTABLE and STRECORD function codes defined,
		        or table-/access type conflict

	STNOACTY	no access type defined (no STTABLE and no STRECORD)

	STNOTFND	search was unsuccessful

	Mnemonic	Explanation
	--------	-----------

	STTABLKD	the table was already
			locked by someone else

	STRECLKD	the record was already
			locked by someone else

	STBYTCNT	from STSEARCH/STPART function codes,
		        the offset plus byte
			count in search/offset buffer exceeds the table/record
			end

	STFULL  	from STINSERT function code, no room for a new
			record in a record table

	STFATAL		a fatal system error was encountered

	STDUPL		from STINSERT function code, the record already
			exists in that record table

	STTSET		the table resource in use time exceeded

	STRSET		the record resource in use time exceeded

	STNOREC		no record no. for unlock or delete 

	STNOPTR         no input and/or output buffer pointer

	STINVSRC	invalid search buffer

        STINVSRT        invalid sort field

	T H E  S E A R C H  F U N C T I O N
	-----------------------------------

	STSEARCH

	With this function code a wanted pattern will be searched for in
	a system table. If found the pointer given in 'bcb->outputbuffer'
	will be set to the start of the table/record. The pattern to be
	searched must be contiguous.

	The format of search buffer (SRCDES, defined in <sthdef.h>):
                                    (typedef SRCDES)

	unsigned long nobytes;   	length of the pattern in characters
        unsigned long bitmask;          bitmask, where each bit set, start-
		                        ing with the most significant bit,
					defines a character position within
					the pattern that will be considered
	unsigned long increment;        offset of the pattern within the table
					or the record
	char data[20];                  pattern to be searched


	Example:

	#include <gbldef.h>
	#include <sthdef.h>
	.
	.
	GBLADR gbladr, *pgbladr = &gbladr;    global addresses
	STHBCB bcb, *pbcb = &bcb;             allocate sth parameter buffer
	SRCDES srcdes, *psrcdes = &srcdes;    allocate search buffer
	char *pattern = "ABCDEF";             search pattern
	char *found;                          pointer set if pattern found
	int x;                                loop index
	unsigned long ret;                    return code from 'STH'

	.
	.
	                                      fill the search buffer
		psrcdes->nobytes = strlen(pattern); length of the pattern
		psrcdes->increment = 0;       offset

		for (x = 0; x < psrcdes->nobytes; x++) generate the bit mask
                    {                                  and move pattern into
                    psrcdes->bitmask |= (BITVHO >> x); the search buffer 
                    psrcdes->data[x] = pattern[x];
                    }

		pbcb->function = (STRECORD|STSEARCH|STLOCATE); function code 
		pbcb->inputbuffer = psrcdes;    address of search buffer
		pbcb->outputbuffer = &found;    set to address of record
		pbcb->tabno = COTAB;            command table
		pbcb->recno = 0;                set to correct record no. by
						'STH' if pattern found

		ret = sth(pbcb,pgbladr);

		...

	P A R T I A L  R E A D  /  W R I T E
	------------------------------------

	With this function code a part of a normal system table is read
	(STTABLE|STPART|STREAD) into a user's buffer, or written back
	(STTABLE|STPART|STWRITE) after a read.

	The offset within the table and the no. of characters to be read
	or written must be defined in the offset descriptor OFFDES that
	is defined in <sthdef.h>.

	The format of OFFDES:

	unsigned long stinc;            offset to partial data in the table
	unsigned long stnb;             no. of characters

	typedef is OFFDES

	Example:

	#include <gbldef.h>
	#include <sthdef.h>

	GBLADR gbladr, *pgbladr = &gbladr;      global addresses
	STHBCB bcb, *pbcb = &bcb;               sth parameter buffer

	struct {                                buffer = OFFDES + r/w bfr 
	       OFFDES offdes;                   for STPART
	       char bfr[20];                    read/write buffer  
	       } buffer, *pbuffer = &buffer;

	unsigned long ret;			return code from 'STH'
	.
	.
	pbuffer->offdes.stinc = 0;              from start of the table
	pbuffer->offdes.stnb = sizeof(pbuffer->bfr); no. of characters 

	read the part of the table

	pbcb->function = (STTABLE|STLOCK|STPART|STREAD);
	pbcb->inputbuffer = pbuffer;		OFFDES & read buffer
	pbcb->outputbuffer = pbuffer->bfr;	read buffer
	pbcb->tabno = STTAB;			status table

	ret = sth(pbcb,pgbladr);
	.					modify the buffer 
	.
	write back the modified part of the table

	pbcb->function = (STTABLE|STUNLOCK|STPART|STWRITE);
	pbcb->inputbuffer = pbuffer;		OFFDES & write buffer 
	pbcb->outputbuffer = NULL;

	ret = sth(pbcb,pgbladr);

	STH internal pointer-structure
	------------------------------

	System Table Handler uses an internal pointer structure for
	communication between STH-main and STH-subfunctions.

	The structure is defined in <sthdef.h> and has a typedef POINT.

	The format of the pointer structure:

	struct _pointers		internal pointer structure
	  {
          TABHDR *ptabhdr;		pointer to current table header
          SPTREC *psptrec;		pointer to SPTREC (SPTAB)
          GBLADR *pgbladr;              pointer to GBLADR
	  char *base;			addr. of the 1st system table index
          char *mend;			end address of the system tables
          char *table;			start address of the current table
          char *srcptr;			working pointer for STSEARCH
          char *locptr;			working pointer for STLOCATE
          long curr_task;		current task number
          STHBCB *params;		STH arguments
          } ;

	typedef struct _pointers POINT;
	
	STH arguments:
	---------------------
	
	Defined in <sthdef.h>

        - unsigned long function;    function code
        - char *inputbuffer;         addr. of input bfr
        - char *outputbuffer;        address of output bfr
        - unsigned long tabno;       table #
        - unsigned long recno;       record #

	typedef is STHBCB
                                                                          $:*/

/*2+*/

#include <stdio.h>
#include <ctype.h>
#include <glbdef.h>
#include <sptdef.h>
#include <sthdef.h>


/* FROM: INITDEF.h ****************************/
SPTREC *psptrec;		/* SPTAB structure */
GBLADR gbladr, *pgbladr = &gbladr; /* global addresses */
STHBCB bcb, *pbcb = &bcb;	/* system table handler bcb */

long shrd_mem_id = FATAL;	/* shared memory id */
long shrd_mem_size;		/* shared memory size */

long glbrc;			/* global return code */

int shm_ass(), init();
char *shm_acc();

/* That's it **************************/

/*      Define a bitpattern for all known function codes        */

#define FCTMASK (STTABLE |\
                 STRECORD |\
                 STPART |\
                 STUNLOCK |\
                 STLOCK |\
                 STWRITE |\
                 STSEARCH |\
                 STLOCATE |\
                 STREAD |\
                 STDELETE |\
                 STINSERT |\
	         STSORT)


/*1:



                                                                         1:*/


/**************************  Function prototype header **********************/
extern  int sth(struct _sthbcb *bcb,GBLADR *pgbladr);
static  int stback(struct _pointers *pointers,int x);
static  int stcomp(struct _srcdes *psrcdes,char *help);
static  char *stcopy(char *p,struct _stdfld *pstd);
static  int stdelete(struct _pointers *pointers);
static  struct _stddesc *stdesc(struct _pointers *pointers);
static  int stinit(struct _pointers *pointers);
static  int stinsert(struct _pointers *pointers);
static  int stlocate(struct _pointers *pointers);
static  int stlock(struct _pointers *pointers);
extern  int blkmove(char *,char *,int);	/* move a memory block      */
static  int stread(struct _pointers *pointers);
static  int stsearch(struct _pointers *pointers);
static  int stsyncheck(struct _pointers *pointers);
static  int stunlock(struct _pointers *pointers);
static  int stwrite(struct _pointers *pointers);
/****************************************************************************/


/*-------------------------------------------------------------------------*/
/*                                                                         */
/*                        S  T  H      M  A  I  N                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

sth(STHBCB *bcb,GBLADR *pgbladr)

/* bcb : parameter buffer                  */
/* pgbladr : global addresses                  */
                                      /* (see <glbdef.h>)                  */

{

	POINT point;                  /* allocate the internal struct.     */
        register POINT *pointers = &point; /* init the pointer             */ 
        register char *p;             /* working pointer                   */
        register unsigned long ret;   /* return code from functions        */

        /*      Make a few syntax checks to catch illegal function
                combinations.                                              */

	pointers->pgbladr = pgbladr;  /* save global pointer               */
        p = pgbladr->stiadr[0];       /* start of system table section     */
	pointers->base = p;           /* init base pointer                 */
        pointers->mend = pgbladr->stiadr[1]; /* end of system table section */

        pointers->params = bcb;       /* copy structure                    */

	if ((ret = stinit(pointers)) !=0)   /* init                        */
            return(ret);              /* if error, return                  */

	if ((ret = stsyncheck(pointers)) !=0) /* some syntax check         */ 
            return(ret);              /* syntax error                      */
  
	if (pointers->params->function & STLOCK) /* LOCK                   */ 
           {
           if (pointers->params->function & STSEARCH) /* + SEARCH          */
               if ((ret = stsearch(pointers)) !=0) 
                   return(ret);       /* if error, return                  */

           if ((ret = stlock(pointers)) !=0 ) /* lock                      */ 
               return(ret);           /* if error, return                  */
           }

	else if (pointers->params->function & STSEARCH) /* SEARCH          */
                 if ((ret = stsearch(pointers)) !=0)
                     return(ret);      /* if error, return                 */

	if (pointers->params->function & STLOCATE) /* LOCATE               */
           {
           if ((ret = stlocate(pointers)) !=0)
              return(ret);           /* if error, return                  */
           }
        else if (pointers->params->function & STREAD) /* READ              */
                 if ((ret = stread(pointers)) != 0) 
                     return(ret);     /* if error, return                  */

	if (pointers->params->function & STDELETE) /* DELETE a record      */
            if ((ret = stdelete(pointers)) !=0 )
                return(ret);          /* if error, return                  */

	if (pointers->params->function & STINSERT) /* INSERT a record      */
            if ((ret = stinsert(pointers)) !=0)
                return(ret);          /* if error, return                  */

	if (pointers->params->function & STWRITE) /* WRITE                 */
            if ((ret = stwrite(pointers)) !=0)
                return(ret);          /* if error, return                  */

	if (pointers->params->function & STUNLOCK) /* UNLOCK               */
            if ((ret = stunlock(pointers)) !=0)
                return(ret);          /* if error, return                  */
 
        return(STNORMAL);             /* return                            */ 
}

/*1:
        S T B A C K  -  subfunction is called by S T L O C K - subfunction.
        It unlocks all records in a record table that were so far locked
        by the current task if a record within the table was found
        locked by some other task.

	Error returns:

	- none
                                                                         1:*/

/*2+*/

static int stback(POINT *pointers,register int x)

/* pointers : internal structure               */
/* x : no. of locked records            */

{
        register RECHDR *prechdr;     /* record header structure           */
        char *s;                      /* working pointer                   */
        register int i;               /* working variable                  */

        if (!(x - 1))                 /* no records locked                 */
            return(STNORMAL);         /* return                            */

        s = (char *) pointers->table; /* get table address           */

        prechdr = (RECHDR *) s;       /* 1st record header                 */

/*      TABLE RESOURCE IS STILL SET !!! */

        for (i = 0; i < x; i++)       /* all locked records                */
            {
            prechdr->r_lktask = prechdr->r_lktime = 0; /* unlock           */

            s += pointers->psptrec->recsiz[pointers->params->tabno] + 
                 DESLEN;              /* next header                       */ 

            prechdr = (RECHDR *) s;
            }

}

/*1:
	S T C O M P - subfunction is a helpfunction for STSEARCH

	Returns:

	0 	string not found
	1	string found
                                                                        1:*/

/*2+*/

static int stcomp(register SRCDES *psrcdes,register char *help)

/* psrcdes : search buffer                   */
/* help    : offset                          */

{
	register int i;                 /* work variable                   */

	for (i = 0; i < psrcdes->nobytes; i++)
             if (psrcdes->bitmask & (BITVHO >> i))
                 if (psrcdes->data[i] != help[i])
                     return(0); 
	return(1);
}

/*1:
	S T C O P Y - subfunction is used by 'stdelete'
	function to generate an empty
	record (after delete).

                                                                     1:*/

/*2+*/

/*	External subroutines (stbsubr)	*/

extern sb_move();			/* move + pad with space */

static char *stcopy(register char *p,register STDFLD *pstd)

/* p    : record address */
/* pstd : field descriptor */


{
	register char *s = p;
	long i;
	char *pblanks = "                    ";

	switch (pstd->std_type)         /* only 1st field type */
               {
               case FLDI:               /* 1 or 2 byte integer */
               case FLDX:               /* 1 or 2 byte hex-integer */
	
               if (pstd->std_size == 1)
		   *(s + pstd->std_offs) = 0;
               else
	       	   *((short *) (s + pstd->std_offs)) = 0;
	          
               s += pstd->std_size;

               break;

               case FLDL:              /* long integer */
		       
	       *((long *) (s + pstd->std_offs)) = 0;   

               s += pstd->std_size;
	       
	       break;
	       
               case FLDS:              /* ASCII-string */

               for (i = 0; i < pstd->std_size; i++) /* binary zeroes */
		   *(s + pstd->std_offs + i) = 0; 

               s += pstd->std_size;

               break;

	       case FLDSB:             /* ASCII plus blanks */	       
	       	       
               sb_move(s + pstd->std_offs,pblanks,pstd->std_size);
	       
               s += pstd->std_size;

	       break;
               }
	return(s);
}

/*1:
	S T D E L E T E - subfunction deletes a given record from a
        record table

        Error returns:
                                                                       1:*/

/*2+*/

#define INTERVAL 500                    /* sleep 1 second */
#define WAIT_COUNT 5                  /* no. of retries for 'tset' */

extern nap();			      /* sleep for n seconds */
extern tset(long *);                        /* reserve a resource */

STDDESC *stdesc();                    /* find the table descriptor */

static int stdelete(pointers)

register POINT *pointers;             /* internal structure              */

{
        RECHDR *prechdr;               /* record header structure         */ 
	char *s;                       /* working character pointer       */
	char *help;                    /* working character pointer       */
	register int i, x;             /* work variable                   */
	STDDESC *pdsc;                 /* table descriptor                */
	STDFLD *pfld;		       /* field descriptor                */

        if (!pointers->params->recno) /* if record 0, return error */
             return(STNOREC);

	if ((pointers->ptabhdr->t_lktask) &&
            (pointers->ptabhdr->t_lktask != pointers->curr_task))
             return(STTABLKD); /* table locked by someone else */

        i = WAIT_COUNT; /* init wait count */

        while ((tset(&(pointers->ptabhdr->t_tset))) && (--i))
                nap(INTERVAL); /* wait a while if resource in use */

        if (!i) /* resource in use */
	    return(STTSET); /* return error */

        pointers->ptabhdr->t_lktask = pointers->curr_task; /* lock */

        s = (char *) pointers->table; /* table base */

        s += ((pointers->params->recno - 1) *
              (pointers->psptrec->recsiz[pointers->params->tabno] +
               DESLEN)); /* start record */

       prechdr = (RECHDR *) s; /* record header */

       if (prechdr->r_lktask)
           if (prechdr->r_lktask != pointers->curr_task)
              {
              pointers->ptabhdr->t_tset = pointers->ptabhdr->t_lktask = 0;
              return(STRECLKD); /* locked by someone else */
              }

        x = pointers->psptrec->recsiz[pointers->params->tabno] + DESLEN;

        help = s + x; /* next record */

        for (i = pointers->params->recno;
             i < pointers->psptrec->mrecno[pointers->params->tabno];
             i++)
                {
                blkmove(help,s,x); /* delete */

                help += x; /* next records */ 

                s += x;
                }

	if ((pdsc = (STDDESC *) stdesc(pointers)) == NULL)
            {
            pointers->ptabhdr->t_tset =
            pointers->ptabhdr->t_lktask = 0;
            return(STINVTAB); /* no descriptor, error */
            }
     
        pfld = (STDFLD *) pdsc->d_std; /* 1st field descriptor */
	
        x -= DESLEN;

	help -= x; /* keep the record header */

	help = stcopy(help,pfld); /* generate a dummy 1st field */

        x -= pfld->std_size; /* record size minus 1st field */

	while (x--) /* binary zeroes */
	       *help++ = 0;

        pointers->ptabhdr->t_tset = pointers->ptabhdr->t_lktask = 0;

	return(STNORMAL);
}

/*1:
	S T D E S C - subfunction is used by 'stinsert' and 'stdelete'
	functions to locate the table descriptor.

	Error returns:

	- NULL-pointer, if the descriptor not found	
                                                                      1:*/

/*2+*/

static STDDESC *stdesc(register POINT *pointers)

/* pointers : internal structure */

{
	register STDDESC *pdsc;        /* table descriptor */
	
	for (pdsc = d_tabs; pdsc->d_tabno; pdsc++) /* all descriptors */
	     if (pdsc->d_tabno == pointers->ptabhdr->t_tabno) /* found */
		 break;

	return((pdsc->d_tabno) ? pdsc : (STDDESC *) NULL);
}

/*1:
	S T I N I T - subfunction initializes the necessary pointers for
        the System Table Handler

	External subroutines:
		
	- getpid	get process id

        Error returns:

	- STINVACT	invalid access type
        - STINVTAB     	invalid table no.
        - STFATAL       fatal error
                                                                        1:*/

/*2+*/

static int stinit(register POINT *pointers)

/* pointers : internal structure               */

{
	register TABHDR *s;           /* working variable                 */
	int i;                        /* work variable                    */

	s = (TABHDR *) pointers->base; /* get header of SPTAB             */

	if (!s->t_tabno)              /* no SPTAB                         */
            return(STFATAL);          /* return error                     */

                                      /* address of SPTREC                */

	pointers->psptrec = (SPTREC *) (pointers->base + s->t_offset);

	pointers->ptabhdr = (TABHDR *) (pointers->base +
                            ((pointers->params->tabno - 1) * IDXLEN));

	if (!pointers->ptabhdr->t_tabno) /* table not configured          */
            return(STINVTAB);         /* return error                     */

	pointers->table = (char *) pointers->base +
                           pointers->ptabhdr->t_offset; /* table base     */

	if (pointers->params->function & STRECORD) /* record access */
            if (!pointers->ptabhdr->t_recsz) /* but not a record table */
                return(STINVACT);     /* return error                     */

	if ((i = getpid()) == -1)     /* no current task no.              */
             return(STFATAL);         /* return error                     */

	pointers->curr_task = i;      /* set current task no.             */

	pointers->srcptr = pointers->locptr = NULL;

	return(STNORMAL);             /* OK                               */
}

/*1:
	S T I N S E R T - subfunction inserts a given record into
	a record table, if table not full. STSORT combined with STINSERT
	will insert the given record sorted into the table.

	Error returns:
                                                                        1:*/

/*2+*/

static int stinsert(register POINT *pointers)

/* pointers : internal structure               */

{
        RECHDR *prechdr;               /* record header structure         */ 
	char *s;                       /* working character pointer       */
	char *help;                    /* working character pointer       */
	register int i, x;             /* work variable                   */
	STDDESC *pdsc;                 /* table descriptor                */
	STDFLD *pfld;		       /* field descriptor                */
	unsigned char dummy = 0;       /* for empty record                */
	long size;                     /* record size                     */

	if ((pointers->ptabhdr->t_lktask) &&
            (pointers->ptabhdr->t_lktask != pointers->curr_task))
             return(STTABLKD); /* table locked by someone else */

        i = WAIT_COUNT; /* init wait count */

        while ((tset(&(pointers->ptabhdr->t_tset))) && (--i))
                nap(INTERVAL); /* wait a while if resource in use */

        if (!i) /* resource in use */
	    return(STTSET); /* return error */

        pointers->ptabhdr->t_lktask = pointers->curr_task; /* lock */

        s = (char *) pointers->table; /* table base */

	if ((pdsc = (STDDESC *) stdesc(pointers)) == NULL)
            {
            pointers->ptabhdr->t_tset =
            pointers->ptabhdr->t_lktask = 0;
            return(STINVTAB); /* no descriptor, error */
            }

        help = s + DESLEN; /* 1st field of the 1st record */

        size = pointers->psptrec->recsiz[pointers->params->tabno] + DESLEN;

        pfld = (STDFLD *) pdsc->d_std; /* 1st field descriptor */

	if (pfld->std_type == FLDSB) /* dummy not binary zeroes */
	    dummy = ' ';             /* change to a blank */

        /* Check if the record already exists */

        for (x = 1; 
	     x <= pointers->psptrec->mrecno[pointers->params->tabno]; x++)
             {
             for (i = 0; i < pfld->std_size; i++)
	      	  if (help[i] != pointers->params->inputbuffer[i])
                      break;
	    
             if (i == pfld->std_size) /* record already exists */
                {
                pointers->ptabhdr->t_tset = pointers->ptabhdr->t_lktask = 0;
	        return(STDUPL); /* return error */
                }

             for (i = 0; i < pfld->std_size; i++) /* is it a dummy */
                  if (help[i] != dummy)
		      break;

             if (i == pfld->std_size) /* is a dummy record */
                 break; /* quit the main loop */

             help += size; /* next record */
             }

        if (x > pointers->psptrec->mrecno[pointers->params->tabno])
           {
           pointers->ptabhdr->t_tset =
           pointers->ptabhdr->t_lktask = 0;
           return(STFULL);  /* return - table full */
           }
        
        /* help is still pointing to the 1st dummy record */

        blkmove(pointers->params->inputbuffer,
		help,
		size - DESLEN); /* overwrite the dummy with the new record */

        pointers->ptabhdr->t_tset =
	pointers->ptabhdr->t_lktask = 0; /* unlock table */

	return(STNORMAL);             /* return */
}

/*1:
        S T L O C A T E - subfunction locates a table or a record if no
        search wanted 
        If successful:

        - sets pointers->params->outputbuffer to the address of
          the record or the table

        Error returns:

        - STINVREC       invalid record no.
	- STTABLKD	 table locked by someone else
	- STRECLKD       record locked by someone else
                                                                          1:*/

/*2+*/

static int stlocate(register POINT *pointers)

/* pointers : internal structure                     */

{
        register char *s;         /* working pointer                        */
	register RECHDR *prechdr; /* record header structure                */

	if (pointers->params->function & STSEARCH) /* search done already   */
            return(STNORMAL);            /* return                          */

	if ((pointers->ptabhdr->t_lktask) &&
            (pointers->ptabhdr->t_lktask != pointers->curr_task))
             return(STTABLKD); /* locked by someone else */

        if (pointers->params->function & STTABLE) /* locate a table         */
           {
           if (pointers->params->function & STREAD) /* called by stread     */
               pointers->locptr = (char *) pointers->table; 
           else
               *(char **) pointers->params->outputbuffer = 
	       pointers->table; 
           }
        else if (pointers->params->function & STRECORD) /* locate a record  */
           {
           if (pointers->params->recno < 0)  /* invalid record no.          */
               return(STINVREC);   /* error                                 */
           else if (!pointers->params->recno) /* record no. 0               */
               pointers->params->recno = 1;  /* set to 1                    */

           s = (char *) pointers->table; /* table address */ 

           s += ((pointers->params->recno - 1) *
                 (pointers->psptrec->recsiz[pointers->params->tabno] + 
                  DESLEN));        /* record address                        */ 

           prechdr = (RECHDR *) s;   /* get record header                   */

	   if (pointers->params->function & STREAD) /* called by stread     */
               pointers->locptr = s + DESLEN; /* set the pointer for read   */
           else
               *(char **) pointers->params->outputbuffer = s + DESLEN; 
           }

	return(STNORMAL);
}

/*1:
        S T L O C K - subfunction to lock a table or a record of a table.

        Called when STLOCK set in bcb->function.

        Calls the following subfunctions:

        Internal:
        - stback

        External:
        - time
        - sleep
        - tset

	Error returns:

	- STTSET                  table resource occupied too long
	- STTALKD                 table already locked by you
	- STTABLKD                table already locked by some other task
	- STRECLKD                record already locked by another task
	- STRSET                  record resource occupied too long
	- STRALKD                 record already locked by you
                                                                         1:*/

/*2+*/


/*      External functions                                                 */



static int stlock(register POINT *pointers)

/* pointers : internal structure                */

{
        RECHDR *prechdr;              /* record header structure           */
                                      /* (see 'stgdef.h')                  */
        char *s;                      /* working pointer                   */
        register int i = WAIT_COUNT;  /* work variable                     */
        register int x;               /* work variable                     */
        unsigned long ret;            /* return from 'locate' and 'search' */
	int	 returnv=STNORMAL;      /* return value			   */
	int	 cnt;		      /* retry count			   */
        while ((tset(&(pointers->ptabhdr->t_tset))) && (--i))
                nap(INTERVAL);       /* wait a while if locked           */

        if (!i)                       /* resource occupied                 */
            return(STTSET);           /* return error                      */

        if (pointers->ptabhdr->t_lktask) /* table already locked           */
           if (pointers->ptabhdr->t_lktask == pointers->curr_task)
              {
              pointers->ptabhdr->t_tset = 0; /* release table resource     */
              return(STTALKD);        /* you locked this table already     */ 
              }
           else
              {
              pointers->ptabhdr->t_tset = 0; /* release table resourse     */
              return(STTABLKD);          /* locked by someone else */
              }

        if (pointers->params->function & STTABLE) /* a whole table         */
           {
           (long ) time(&pointers->ptabhdr->t_lktime); /* time stamp */

           pointers->ptabhdr->t_lktask = pointers->curr_task; /* task no.  */

           if (pointers->ptabhdr->t_recsz) /* a record table */
              {                       /* offset to 1st record header       */
              s = (char *) pointers->table; 

              prechdr = (RECHDR *) s;

              for (x = 1;
                   x <= pointers->psptrec->mrecno[pointers->params->tabno]; 
                   x++)
                  {                   /* lock all records                  */
                  i = WAIT_COUNT;     /* init wait count                   */

                  while ((tset(&(prechdr->r_tset))) && (--i))
                          nap(INTERVAL); /* wait if locked               */

                  if (!i)             /* resource occupied                 */
                     {
                     if (x > 1)        /* locked some already              */
                        stback(pointers,x); /* unlock      */

                     pointers->ptabhdr->t_lktime =
                     pointers->ptabhdr->t_lktask = 0;
                     pointers->ptabhdr->t_tset = 0; /* rel. table res. */
                     return(STRSET);  /* return error                      */
                     }

                  if (prechdr->r_lktask) /* record already locked          */
                     if (prechdr->r_lktask != pointers->curr_task) 
                        {
                        if (x > 1)         /* locked some already          */
                            stback(pointers,x); /* unlock  */

                        pointers->ptabhdr->t_lktime =
                        pointers->ptabhdr->t_lktask = 0;
                        pointers->ptabhdr->t_tset = 0;  /* rel. table res. */
                        return(STRECLKD);    /* locked by someone else     */
                        }

                  prechdr->r_lktime = pointers->ptabhdr->t_lktime; /* time */ 
                  prechdr->r_lktask = pointers->curr_task; /* task no. */
                  prechdr->r_tset = 0;    /* release record resource       */

                  s += pointers->psptrec->recsiz[pointers->params->tabno] + 
                       DESLEN;

                  prechdr = (RECHDR *) s; /* next record                   */
                  }
              }
           pointers->ptabhdr->t_tset = 0; /* release the table resource */
           }

        else if (pointers->params->function & STRECORD) /* only one record */
           {
           if (!pointers->params->recno) /* record # 0, */
              {
              pointers->ptabhdr->t_tset = 0; /* release table resource */
              return(STNOREC);                  /* return error             */
              }

           s = (char *) pointers->table; /* table address */ 
           s += ((pointers->params->recno - 1) * 
                 (pointers->psptrec->recsiz[pointers->params->tabno] + 
                  DESLEN)); /* record address */ 

           prechdr = (RECHDR *) s; /* init rec. header pointer */

	   cnt = 0;

	   do {
		   i = WAIT_COUNT;              /* init wait count          */

		   while ((tset(&(prechdr->r_tset))) && (--i)) {
			   nap(INTERVAL);
		   }

		   if (!i)                    /* resource occupied        */
                   {
			   pointers->ptabhdr->t_tset = 0;
			   /* release table resource */
			   return(STRSET);    /* return error             */
		   }

		   if (prechdr->r_lktask)      /* record already locked    */
			   if (prechdr->r_lktask == pointers->curr_task)
			   {
					   prechdr->r_tset = 0;
					   /* release record resource */
					   pointers->ptabhdr->t_tset = 0;
					   /* release table resource */
					   returnv = STRALKD;
					   /* you already locked the record */ 
			   }
			   else 
				   /* locked by someone else   */
			   {
				   prechdr->r_tset = 0;
				   /* release record resource */
				   pointers->ptabhdr->t_tset = 0;
				   /* release table resource */
				   nap(500);
				   returnv = STRECLKD;
				   cnt++;
			   }
		   } while ( cnt < 10 && returnv == STRECLKD );

		   if ( returnv != STNORMAL ) {
			   return(returnv);
		   } /* end if */

           prechdr->r_lktask = pointers->curr_task; /* lock */
           (long) time(&prechdr->r_lktime); /* time stamp */
           prechdr->r_tset = 0; /* release record resource */
           pointers->ptabhdr->t_tset = 0; /* release table resource */
           }

	return(STNORMAL);
}

/*1:
	S T R E A D - subfunction moves the table, a part of the table or
	a record into the user's buffer

	External subroutines:

	- blkmove	moves a memory block (stgsubr)

	Internal subroutines:

	- stlocate	locate a table/record
	
	Error returns:

	- STINVACT	reading a whole record table
	- STNOPTR	no input buffer (STPART)
	- STTABLKD	table locked by someone else
                                                                          1:*/

/*2+*/


static int stread(register POINT *pointers)

/* pointers : internal                 */

{
	register char *start;			/* start of table/record   */ 
	register char *end;                     /* end of table/record     */ 
	unsigned long ret;			/* return from other func. */ 
	long bytes;				/* no. of characters       */ 
	OFFDES *poffdes;                        /* for STPART              */

	if ((pointers->ptabhdr->t_lktask) &&
            (pointers->ptabhdr->t_lktask != pointers->curr_task))
             return(STTABLKD); /* locked by someone else */

	if (pointers->params->function & STTABLE) /* a table */
           {
           if (pointers->ptabhdr->t_recsz) /* a whole record table */
               return(STINVACT); /* return error */

	   start = pointers->table; /* start */
           end = start + pointers->ptabhdr->t_tabsz; /* end */
           bytes = pointers->ptabhdr->t_tabsz; /* size */

           if (pointers->params->function & STPART) /* only a part */
              {
              if (pointers->params->inputbuffer == NULL) /* no descriptor */
                  return(STNOPTR); /* return error */

              poffdes = (OFFDES *) pointers->params->inputbuffer;

              if ((start + poffdes->stinc + poffdes->stnb - 1) > end)
                   return(STBYTCNT); /* access violation */

              start += poffdes->stinc; /* adjust start address */

              bytes = poffdes->stnb; /* size */
              }
	   }
	else
	   {     				/* a record                 */
           bytes = pointers->ptabhdr->t_recsz; /* no. of characters */

           if ((start = pointers->srcptr) == NULL) /* search not done */
              {
              if ((ret = stlocate(pointers)) !=0) /* locate */
                  return(ret); /* if error, return */
              start = pointers->locptr;
              }
	   }
	blkmove(start,pointers->params->outputbuffer,bytes); /* read */

	return(STNORMAL);			/* return                   */
}

/*1:
	S T S E A R C H - Subfunction 

	Internal subroutines:

	- stcomp	string compare with the bit-mask

	Error returns:

	- STNOPTR	no search buffer
	- STINVSRC	invalid search buffer
	- STTABLKD	table locked by someone else
	- STBYTCNT	access violation
	- STNOTFND	string not found
	- STRECLKD	record was locked by someone else
                                                                        1:*/

/*2+*/

static int stsearch(register POINT *pointers)

/* pointers : internal               */

{
        RECHDR *prechdr;               /* record header structure         */ 
	char *s;                       /* working character pointer       */
	SRCDES *psrcdes;               /* working search buffer           */
	char *help;                    /* working character pointer       */
        char *end;                     /* end of table/record             */
	register int i, x;             /* work variable                   */

	if (pointers->params->inputbuffer == NULL) /* no search buffer    */
            return(STNOPTR);           /* error */

	psrcdes = (SRCDES *) pointers->params->inputbuffer; /* search bfr */

	if ((psrcdes->nobytes < 1) ||
            (psrcdes->bitmask == (unsigned long)0) ||
            (psrcdes->increment < 0)) 
             return(STINVSRC); /* invalid search buffer */

	if ((pointers->ptabhdr->t_lktask) &&
            (pointers->ptabhdr->t_lktask != pointers->curr_task))
             return(STTABLKD); /* table locked by someone else */

	if (pointers->params->function & STTABLE) /* a table */
           {
           if ((psrcdes->increment + psrcdes->nobytes) >
                pointers->ptabhdr->t_tabsz)
                return(STBYTCNT); /* access violation */

           help = (char *) pointers->table + psrcdes->increment; /* position */

           if (!stcomp(psrcdes,help)) /* not found */
               return(STNOTFND);
           
           pointers->srcptr = pointers->table; /* set search pointer */

           if (pointers->params->function & STLOCATE) /* search + locate */
               *(char **) pointers->params->outputbuffer = pointers->srcptr;

           }
	else if (pointers->params->function & STRECORD) /* a record */
           {
           if ((psrcdes->increment + psrcdes->nobytes) >
                pointers->ptabhdr->t_recsz)
                return(STBYTCNT); /* access violation */

           if (!pointers->params->recno) /* if record 0, set to 1 */
               pointers->params->recno = 1;

           s = (char *) pointers->table; /* table base */

           s += ((pointers->params->recno - 1) *
                 (pointers->psptrec->recsiz[pointers->params->tabno] +
                  DESLEN)); /* start record */

           help = s + DESLEN + psrcdes->increment; /* plus increment */ 

           for (i = x = pointers->params->recno;
                i <= pointers->psptrec->mrecno[pointers->params->tabno];
                i++, x++)
                {
                if (stcomp(psrcdes,help)) /* found the string */
                   {
                   i = 0;   /* indicate found */
                   break;  /* yes */
                   }

                help += (DESLEN + 
                         pointers->psptrec->recsiz[pointers->params->tabno]); 

                s += (DESLEN +
                      pointers->psptrec->recsiz[pointers->params->tabno]);
                }

           if (i) /* not found */
               return(STNOTFND); /* return */

           prechdr = (RECHDR *) s; /* record header */

           /* location of the record */
           pointers->srcptr = s + DESLEN;
           pointers->params->recno = x; /* record no. */
           
           if (pointers->params->function & STLOCATE) /* search + locate */
               *(char **) pointers->params->outputbuffer = pointers->srcptr;
           }

	return(STNORMAL);
}

/*1:
        S T S Y N C H E C K -  subfunction makes a few syntax checks for the
        'function'-code, table no. and record no. in the
        'STHBCB'.


	Error returns:

	- STINVFCT               invalid function/function combination
	- STINVTAB               invalid table no. (0 or > max.)
	- STINVREC               invalid record no. (0 < or > max.)
	- STNOACTY		 no access type
	- STNOPTR                no input/output buffer pointer

                                                                         1:*/

/*2+*/

static int stsyncheck(register POINT *pointers)

/* pointers : internal                          */

{
	register unsigned long mask = ~FCTMASK;
	register unsigned long function = pointers->params->function;

        mask <<= 1;

        if ((!function) || (function & mask))
             return(STINVFCT);        /* unknown function                  */

        if ((pointers->params->tabno <= 0) ||
            (pointers->params->tabno > pointers->psptrec->maxtab))
             return(STINVTAB);        /* invalid table no.                 */

        if ((pointers->params->recno < 0) ||
           ((pointers->params->recno) &&
            (pointers->params->recno > 
             pointers->psptrec->mrecno[pointers->params->tabno])))
             return(STINVREC);        /* invalid record no.                */

        if (function & STREAD)
	    if ((function & STUNLOCK) ||
		(function & STWRITE)  ||
		(function & STLOCATE))
		 return(STINVFCT); /* invalid read */
	 
	if (function & STTABLE)
	    if ((function & STINSERT) ||
		(function & STDELETE) ||
		(function & STRECORD))
		 return(STINVFCT); /* invalid for table */
	 
	if (function & STPART)
	    if ((function & STRECORD) ||
		(function & STINSERT) ||
		(function & STDELETE) ||
		(function & STLOCATE) ||
		(function & STSEARCH))
		 return(STINVFCT); /* invalid partial */
	 
	if (function & STWRITE)
	    if (function & STLOCK)
		return(STINVFCT); /* invalid combination */
	
	if (function & STLOCK)
	    if ((function & STUNLOCK) ||
		(function & STLOCATE))
		 return(STINVFCT); /* invalid combination */
	 
	if (function & STSEARCH)
	    if ((function & STWRITE) ||
		(function & STINSERT))
		 return(STINVFCT); /* invalid search */
	 
	if (function & STLOCATE)
	    if ((function & STINSERT) ||
		(function & STWRITE))
		 return(STINVFCT); /* invalid locate */

        if (function & STSEARCH)
            if((!(function & STLOCATE)) &&
               (!(function & STREAD)))
                  return(STINVFCT); /* invalid search */ 

        if (function & STPART)
            if((!(function & STREAD)) &&
               (!(function & STWRITE))) /* invalid partial*/
                  return(STINVFCT);

	if (!(function & STTABLE))
              if (!(function & STRECORD))
                    return(STNOACTY);            /* no access type         */

	if (pointers->params->function & (STWRITE|STINSERT|STSEARCH))
            if (pointers->params->inputbuffer == NULL) /* no record ptr */
                return(STNOPTR);                 /* error                  */

	if (pointers->params->function & (STSEARCH|STLOCATE|STREAD))
            if (pointers->params->outputbuffer == NULL) /* no record ptr */
                return(STNOPTR);                 /* error                  */

        return(STNORMAL);                        /* no syntax errors       */
}

/*1:
        S T U N L O C K - subfunction to unlock a table or a record of a table.

        Called by 'sth' when STUNLOCK set in bcb->function.

        Calls the following subfunctions:

        External:
        - sleep
        - tset

        Error returns:

        - STRNLKD                   record was not locked
        - STRSET                    record resource occupied too long
        - STTNLKD                   table was not locked
        - STTSET                    table resource occupied too long
	- STNOREC                   no record no. for UNLOCK
                                                                          1:*/

/*2+*/

static int stunlock(register POINT *pointers) 

/* pointers : internal                             */

{
        RECHDR *prechdr;            /* record header structure              */
        register int i = WAIT_COUNT; /* work variable                       */
        register int x;             /* work variable                        */
        char *s;                    /* working pointer                      */

        while ((tset(&(pointers->ptabhdr->t_tset))) && (--i))
                nap(INTERVAL);    /* wait a while if in use               */

        if (!i)                     /* table resource set too long          */
            return(STTSET);         /* return error                         */


        if (pointers->params->function & STTABLE) /* a whole table */
           {
           if (!pointers->ptabhdr->t_lktask)  /* table not locked */
              {
              pointers->ptabhdr->t_tset = 0; /* release resource */
              return(STTNLKD);      /* return error                         */
              }

           if (pointers->ptabhdr->t_lktask != pointers->curr_task) 
              {
              pointers->ptabhdr->t_tset = 0; /* release resource */
              return(STTNLKD);       /* return error                        */
              }

           pointers->ptabhdr->t_lktime = pointers->ptabhdr->t_lktask = 0;

           if (pointers->ptabhdr->t_recsz) /* a record table */
              {
              s = (char *) pointers->table; /* table address */ 
              prechdr = (RECHDR *) s;

              for (x = 1;
                   x <= pointers->psptrec->mrecno[pointers->params->tabno];
                   x++)
                  {                 /* unlock all records                   */
                  i = WAIT_COUNT;   /* init wait count                      */

                  while ((tset(&(prechdr->r_tset))) && (--i))
                          nap(INTERVAL); /* wait if resource in use       */

                  if (!i)           /* resource in use too long             */
                     {
                     pointers->ptabhdr->t_tset = 0; /* release  resource */
                     return(STRSET);      /* return error                   */
                     }

                  if (!prechdr->r_lktask) /* record was not locked          */
                     {
                     prechdr->r_tset = pointers->ptabhdr->t_tset = 0; 
                     return(STRNLKD);     /* return error                   */
                     }

                  if (prechdr->r_lktask != pointers->curr_task) /* not by you */
                     {
                     prechdr->r_tset = pointers->ptabhdr->t_tset = 0;
                     return(STRNLKD); /* return error                       */
                     }

                  prechdr->r_lktime = prechdr->r_tset = prechdr->r_lktask = 0;

                  s += pointers->psptrec->recsiz[pointers->params->tabno] + 
                       DESLEN; /* next rec */
                  prechdr = (RECHDR *) s;
                  }
              }
           pointers->ptabhdr->t_tset = 0; /* release table resource */
           }

        else if (pointers->params->function & STRECORD) /* only one record */
           {
           if (!pointers->params->recno)  /* no record number              */
               return(STNOREC);           /* error */

           s = (char *) pointers->table; /* table address             */ 

           s += ((pointers->params->recno - 1) *
                (pointers->psptrec->recsiz[pointers->params->tabno] + 
                 DESLEN)); /* position     */

           prechdr = (RECHDR *) s;  /* init record header pointer           */

           i = WAIT_COUNT;          /* init wait count                      */

           while ((tset(&(prechdr->r_tset))) && (--i))
                   nap(INTERVAL); /* wait if in use                       */

           if (!i)                  /* resource in use too long             */
              {
              pointers->ptabhdr->t_tset = 0; /* release table resource */
              return(STRSET);       /* return error                         */
              }

           if (!prechdr->r_lktask)  /* not locked                           */
              {
              prechdr->r_tset = pointers->ptabhdr->t_tset = 0;
              return(STRNLKD);      /* return error                         */
              }

           if (prechdr->r_lktask != pointers->curr_task)
              {
              prechdr->r_tset = pointers->ptabhdr->t_tset = 0; /* release */
              return(STRNLKD);      /* return error                         */
              }

           prechdr->r_lktask = prechdr->r_lktime = prechdr->r_tset = 0;
           pointers->ptabhdr->t_tset = 0;     /* release table resource */
           }

	return(STNORMAL);
}

/*1:
	S T W R I T E - subfunction writes a table/part of a table or a record
	back after a read

	External subroutines:

	- blkmove	moves a memory block

	Error returns:

	- STTABLKD	table locked by someone else
	- STINVACT	invalid access type
	- STNOPTR	no buffer for STPART
	- STNOREC	no record no. 
	- STRECLKD	record locked by someone else
                                                                         1:*/

/*2+*/

static int stwrite(register POINT *pointers)

/* pointers : internal                 */

{
	OFFDES *poffdes;		      /* buffer for STPART        */ 
	register char *start;                 /* working pointer          */ 
	char *end;                            /* working pointer          */ 
	long bytes;                           /* no. of characters to wr. */ 
	register RECHDR *prechdr;             /* record header            */ 

	if ((pointers->ptabhdr->t_lktask) &&
            (pointers->ptabhdr->t_lktask != pointers->curr_task))
             return(STTABLKD); /* locked by someone else */

	if (pointers->params->function & STTABLE)	/* a table */
           {
           if (pointers->ptabhdr->t_recsz) /* a whole record table */
               return(STINVACT);                /* return error            */

           if (pointers->params->function & STUNLOCK) /* write + unlock */
 	       if (!pointers->ptabhdr->t_lktask) /* not locked */
		   return(STTNLKD);

           start = (char *) pointers->table; /* start address */

	   end = (char *) start + pointers->ptabhdr->t_tabsz; /* end address */

           bytes = pointers->ptabhdr->t_tabsz; /* size */

           if (pointers->params->function & STPART) /* only a part */
              {
              if (pointers->params->inputbuffer == NULL) /* no descriptor */
                  return(STNOPTR);               /* return error           */

              poffdes = (OFFDES *) pointers->params->inputbuffer;

              if ((start + poffdes->stinc + poffdes->stnb - 1) > end)
                   return(STBYTCNT); /* access violation */

              start += poffdes->stinc; /* start address */

              bytes = poffdes->stnb; /* size */
              }
           }
        else    				/* a record                */
           {
           if (!pointers->params->recno)  /* no record no. */
               return(STNOREC);                 /* return error            */

           bytes = pointers->ptabhdr->t_recsz; /* size */

           start = (char *) pointers->table; /* table addr. */

	   start += ((pointers->params->recno - 1) * 
                     (pointers->psptrec->recsiz[pointers->params->tabno] + 
                      DESLEN));

           prechdr = (RECHDR *) start;    /* record header                 */

           if ((prechdr->r_lktask) &&     /* locked                        */
               (prechdr->r_lktask != pointers->curr_task))
                return(STRECLKD); /* locked by someone else */ 

           if (pointers->params->function & STUNLOCK) /* write + unlock */
 	       if (!prechdr->r_lktask) /* not locked */
		   return(STRNLKD);

           start += DESLEN;                     /* past the header         */
           }

	if (pointers->params->function & STPART) /* only a part */
	    blkmove(pointers->params->inputbuffer + sizeof(*poffdes),
                    start,bytes);
        else
            blkmove(pointers->params->inputbuffer,start,bytes);

	return(STNORMAL);                       /* write and return        */
}
	
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
