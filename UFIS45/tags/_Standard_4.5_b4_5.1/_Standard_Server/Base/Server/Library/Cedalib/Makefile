# 
#   ##    #####   #####
#  #  #   #    #  #    #
# #    #  #####   #####
# ######  #    #  #    #
# #    #  #    #  #    #
# #    #  #####   #####  Airport Technologies GmbH
#
# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/Makefile 1.5 2004/10/04 20:54:15SGT jim Exp  $
# Makefile for Cedalib
#
# 
# 20020527 JIM:  added rule for dsplex.l
# 20020625 JIM:  avoid 'rm *' in clean
# 20021008 JIM: improved 'clean' of non existing targets
# 20021008 JIM: do not include .depends for target 'clean' targets
# 20021107 JIM: call $(MKDEP) instead of mkdep
# 20021119 JIM:  Test for malformed archives added for CEDAlib only
# 20021125 JIM: improved 'clean' of non existing targets
# 20021125 JIM: binary files are stored in /ceda/SCM_Bins/<user>/<prj>/Obj | Lib
#               to avoid trouble with HP/NFS and improve performance
# 20041004 JIM: clean: remove temporary files "*.used"

# globale Definitionen fuer alle Makefiles
include $(PB)/Make/GlobalDefines
ifeq "$(MY_OS)" "HP-UX"
  # test fuer "malformed archive"
  #SYMBOLS=
endif

.y.c:
	@#echo $?

# Definition aller .c Dateien als Quellen fuer Lib-Objects:
# 20030523 JIM: may be, rmschkrul.c is not there. So name especially
STD_SOURCES=$(wildcard *.c) 

# Definition aller .pc Dateien als Quellen fuer Oracle-Lib-Objects:
SPEC_SOURCES1=$(wildcard *.pc)

# Definition aller .l und .y Dateien als Quellen fuer Lib-Objects:
SPEC_SOURCES2=$(wildcard *.y) 
SPEC_SOURCES3=$(wildcard dsp*.l) 


# hier sollen die Binaries der Lib-Objects abgelegt werden:
# hier liegt mkdep:
ifeq "$(O)" ""
OBJDIR=tmp_$(MY_OS)
MKDEP= $(TS)/Bin_$(MY_OS)/mkdep
else
OBJDIR=$(O)/Cedalib
MKDEP= $(L)/mkdep
endif
VPATH= $(OBJDIR)
vpath %.o $(OBJDIR)
vpath %.a $(L)

ifdef OBJDIR
OBJS=$(wildcard $(OBJDIR)/*)
endif
DEPENDS=$(wildcard .depends $(L)/CEDAlib.a)
TMP_USED=$(wildcard *.used)

# Die Namen der Lib-Objects leiten sich aus den Namen der .c-Dateien ab.
# Hier wird eine Liste der Lib-Objects aufgebaut, indem von allen .c-Dateien
# das .c abgeschnitten wird und der Pfad '$(OBJDIR)/' hinzugefuegt wird:
DBLIB_OBJS= $(patsubst %,$(OBJDIR)/%,$(STD_SOURCES:.c=.o)) 
SPEC_OBJS1= $(patsubst %,$(OBJDIR)/%,$(SPEC_SOURCES1:.pc=.o))
SPEC_OBJS2= $(patsubst %,$(OBJDIR)/%,$(SPEC_SOURCES2:.y=.o))
SPEC_OBJS3= $(patsubst %,$(OBJDIR)/%,$(SPEC_SOURCES3:.l=.o))


# 1. Ziel und seine Abhaengigkeit
$L/CEDAlib.a: $(OBJDIR) $(L) $(DBLIB_OBJS) $(SPEC_OBJS1) $(SPEC_OBJS2) $(SPEC_OBJS3)

# 20030523 JIM: added dependency rmschkrul.c
$L/CEDAlib.a: rmschkrul.c

#include .depends

# Standard-Regel zum Make der DB Library-Objects
$(DBLIB_OBJS): $(OBJDIR)/%.o: %.c
	@echo "\n\n     $* ... \n"
	$(RCC) $(SYMBOLS) $(COMPILE) $(CFLAGS) -c $< -o $@ 2>&1
	@echo "\n     ... $* \n\n"
	@ar rv $L/CEDAlib.a $@


	
# Standard-Regel zum Make der DB Library-Objects von Oracle
$(SPEC_OBJS1): $(OBJDIR)/%.o: %.pc
	@echo "\n\n     @< ... \n"
	$(PROC) $(PCCFLAGS) iname=$(LS)/db_if.pc  2>&1
	$(RCC)  $(CFLAGS) $(COMPILE) $(SYMBOLS) -I$(PCCINC) -o $(OBJDIR)/@<.o -c $(LS)/db_if.c 2>&1
	@echo "\n     ... @< \n\n"
	@ar rv $L/CEDAlib.a $(OBJDIR)/db_if.o 2>&1
	@rm $(LS)/db_if.c 2>&1

# Standard-Regel zum Make Lex/Yacc Objecte
# $(SPEC_OBJS2): $(OBJDIR)/%.o: %.l %.y
#	@echo "\n\n     $* ... \n"
#	flex -P$* $*.l
#	   yacc -d $*.y
#	   mv y.tab.c $*.c
#	$(RCC) $(SYMBOLS) $(COMPILE) $(CFLAGS) -c $*.c -o $@ 2>&1 
#	@echo "\n     ... $* \n\n"
#	@ar rv $L/CEDAlib.a $@
#	rm y.tab.h
#	rm *$*.c
                            

# hier soll die Library erzeugt werden:
$(L):
	mkdir -p $(L)

clean:
ifneq "$(OBJS)" ""
	  @rm $(OBJDIR)/* 2>&1 >/dev/null 
endif   
ifneq "$(DEPENDS)" ""
	@rm $(DEPENDS) 2>&1 >/dev/null 
endif
ifneq "$(TMP_USED)" ""
	@rm $(TMP_USED) 2>&1 >/dev/null 
endif

dep:
	cd $(TS)/Mkdep ; $(MAKE) 
	$(MKDEP) *.c >.depends

.depends:
	$(MKDEP) *.c >.depends
          

$(OBJDIR)/rmschkrul.o: 
ifeq "$(MY_OS)" "HP-UX"
	@echo "Achtung: Wegen Fehler im YACC kann $< nicht hier erzeugt werden!"
	@echo "Bitte auf einer beliebigen Plattform im \$LS folgende Befehle von"
	@echo "Hand aufrufen:"
	@echo "  flex -Prmschkrul rmschkrul.l"
	@echo "  mv lex.rmschkrul.c lex.rmschkrul.h"
	@echo "  yacc -d rmschkrul.y"
	@echo "  mv y.tab.c rmschkrul.c"
	@echo "  rm y.tab.h"
	@echo "Die erzeugte Datei $< muss dann hieher kopiert und dort geloescht werden
else
	@echo "\n\n     $* ... \n"
	flex -Prmschkrul rmschkrul.l
	mv lex.rmschkrul.c lex.rmschkrul.h
	yacc -d rmschkrul.y
	mv y.tab.c rmschkrul.c
	rm y.tab.h
	$(RCC) $(SYMBOLS) $(COMPILE) $(CFLAGS) -c rmschkrul.c -o $@ 2>&1 
	mv rmschkrul.c rmschkrul.c.used 
	mv lex.rmschkrul.h lex.rmschkrul.h.used 
	@ar rv $L/CEDAlib.a $@
	@echo "\n     ... $* \n\n"
endif   

$(OBJDIR)/rmschkrul.o: rmschkrul.y

$(OBJDIR)/rmschkrul.o: rmschkrul.l


$(OBJDIR)/dsplex.o:
	@echo "\n\n     $* ... \n"
	flex -Pdsplex -t dsplex.l > $(OBJDIR)/dsplex.c
	$(RCC) $(SYMBOLS) $(COMPILE) $(CFLAGS) -c $(OBJDIR)/dsplex.c -o $@ 2>&1 
	@echo "\n     ... $* \n\n"
	@ar rv $L/CEDAlib.a $@
	rm $(OBJDIR)/dsplex.c
                            
$(OBJDIR)/dsplex.o: dsplex.l

ifneq ($(MAKECMDGOALS),clean)
  include .depends
endif

$(OBJDIR): /ceda/SCM_Bins
	@-mkdir -p $(OBJDIR) 2>&1

/ceda/SCM_Bins:
ifneq "$(LOGNAME)" "ceda"
	@echo == Das Verzeichnis /ceda/SCM_Bins existiert nicht. Diese Verzeichnis muss ==
	@echo == der User CEDA anlegen und fuer die Gruppe CEDA beschreibbar machen     ==
	@echo == Bitte Passwort fuer den User CEDA angeben:                             ==
	su - ceda -c "mkdir /ceda/SCM_Bins && chmod g+rwx /ceda/SCM_Bins"
else
	mkdir /ceda/SCM_Bins && chmod g+rwx /ceda/SCM_Bins
endif
