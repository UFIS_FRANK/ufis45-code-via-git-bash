#ifndef _DEF_mks_version_libccstr_c
  #define _DEF_mks_version_libccstr_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_libccstr_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/libccstr.c 1.7 2005/03/08 17:19:17SGT heb Exp  $";
#endif /* _DEF_mks_version */
/*
#ident  __DATE__ __TIME__ "CCS String Library"
*/

/* ==================================================================== */
/*									*/
/* CCS String Library   :  libccstr.c	                                */
/*									*/
/* Author		:  Bernhard Straesser				*/
/* Date			:  01/1995					*/
/* Description		:  Look online help LibStr                      */
/*									*/
/* Update history	: New Features by RHU / BST in 1996 		*/
/*									*/
/* ==================================================================== */


#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#ifndef GLBDEF_INC
#include "glbdef.h"		/* global definitions for ugccs prgs	*/
#endif

#include "libccstr.h"


/* ******************************************************************** */
/* defines                                                              */
/* ******************************************************************** */



/* char values */
#define CHR0_VAL    '\0'
#define BLNK_VAL    ' '
#define EQUL_VAL    '='
#define ZERO_VAL    '0'
#define NINE_VAL    '9'


#define NEWL_VAL    '\n'
#define TABS_VAL    '\t'
#define CMMA_VAL    ','


/* char strings */
#define CHR0_CHR   "\0"
#define NULL_CHR   ""
#define BLNK_CHR   " "
#define EQUL_CHR   "="


/*
#ifndef TRUE
        #define TRUE 1
        #define FALSE 0
#endif
*/








/*==============================================*/
/* FUNCTION str_new_chr				*/
/* (string new filled with characters)		*/
/* allocates a new string of lenght 'len'	*/
/* and sets end-of-string ('\0' in 'len + 1')	*/
/* The new string is continuosly filled	with 	*/
/* the characters given in 'c' (char or words).	*/
/* If 'c' is empty, string is filled with '\0'.	*/
/* Returns the pointer on the string or NULL	*/
/*==============================================*/
char *str_new_chr (int len, char *c) {
	char *str = NULL;
	int len_pat;
	int i;
	int j = NUL;
        if (len > NUL){
		str = malloc((len + 1));
		if( str != NULL ){
			str[len] = CHR0_VAL;
			len_pat = strlen(c);
			if (len_pat < 1) {
				len_pat = 1;
			}/* end if */
			while (len > NUL) {
				for (i = NUL; (i < len_pat) && (len > NUL); i++){
					str[j] = c[i];
					j++;
					len--;
				}/* end for */
			}/* end while */			
		}/* end if */
	}/* end if */
	return (str);
}/* END OF FUNCTION */


/*==============================================*/
/* FUNCTION str_cpy_chg				*/
/* It's like 'strcpy()' but the copied char's	*/
/* can be converted by defined methods.		*/
/* Copies a string into a 2nd of equal size	*/
/*==============================================*/
void str_cpy_chg ( char *s, char *t, int meth) {
	int i = NUL;
	while ((t[i]) != CHR0_VAL) {
		switch (meth) {  
			case NUL:
				s[i] = t[i];
				break;
			case 1:
				s[i] = toupper(t[i]);
				break;
			case -1:
				s[i] = tolower(t[i]);
				break;   
			default:
				s[i] = t[i];
		}/* end switch */ 
		i++;
	}/* end while */
	return;
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_cpy_mid				*/
/* copies a partition of string 't'             */
/* into a defined position of string 's'	*/
/* If len is omitted, all char's are copied.	*/
/* Sets a mark in 'd' behind the copied char's.	*/
/* Returns the position of the marker 'c_pos'	*/
/*==============================================*/
int str_cpy_mid ( char *s, int s_pos, int s_len, char *t, int t_pos, int t_len, char *d) {
	int rc = -10;
	
	if (s_len < 1) {
		s_len = strlen(s);
	}/* end if */
	
	if ((s_pos >= NUL) && (s_pos < s_len)) {
			
		if (t_len < 1){
			t_len = strlen(t) - t_pos;
		}/* end if */

		if ( (s_pos + t_len) > s_len ) {
			t_len = s_len - s_pos;
		}/* end if */
			
		while (t_len > NUL) {
			s[s_pos] = t[t_pos];
			t_pos++;
			s_pos++;
			t_len--;
		}/* end while */

		if (s_pos < s_len) {
			s[s_pos] = d[NUL];
		}/* end if */
		
		rc = s_pos;
			
	}/* end if */

	return (rc);
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_chg_upc				*/
/* changes the content to upper case		*/
/*==============================================*/
void str_chg_upc (char *s) {
	int i=NUL;
	while ((s[i]) != CHR0_VAL) {
		s[i] = toupper(s[i]);
		i++;
	}/* end while */
	return;
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_fnd_str				*/
/* searches a string 't' in string 's'		*/
/* beginning at position 'c_pos'		*/
/* Returns the start position of matching case	*/
/*==============================================*/
int str_fnd_str (int c_pos, int c_end, char *s, char *t) {
	int rc = -1;
	int si = NUL;
	int ti = NUL;             
	int sl;
	sl = strlen(s);
	if (c_end < NUL) {
		c_end = --sl;
	}/* end if */
	if ( (c_pos >= NUL) && (sl > c_pos) ) {
		for (si = c_pos; (s[si] != CHR0_VAL) && (rc == -1) && (si <= c_end); si++) {
			if (s[si] == t[NUL]) {
				rc = si;
				for (ti = NUL; (t[ti] != CHR0_VAL) && (rc >= NUL); ti++) {
					if (s[si] != t[ti]) {
						rc = -1;
						si--;
					}/* end if */
					si++;
				}/* end for */
			}/* end if */
		}/* end for */
	}/* end if */
	return (rc);
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_fnd_fst				*/
/* find first occurance of a char in a string	*/
/* Returns the position of either:		*/
/* the first     matching char (flg = TRUE) or	*/
/* the first not matching char (flg = FALSE) 	*/
/*==============================================*/
int str_fnd_fst(int c_pos, int lst_p, char *t, char *s, int *c_rck, int flg) { 
	int s_len;
	int i;

	s_len = strlen(s);

	if (lst_p < NUL) {
		lst_p = strlen(t) - 1;
	}/* end if */
	if (c_pos > lst_p){
		c_pos = lst_p + 1;
	}/* end if */

	/* search for just one character */
	if (s_len == 1) {
		while ( (c_pos <= lst_p) &&
			( ((flg == FALSE) && (t[c_pos] == *s)) ||
			  ((flg == TRUE)  && (t[c_pos] != *s)) ) ) {
			c_pos++;
		}/* end while */
	}/* end if */
	else {
		/* search for a set of characters */
		while (c_pos <= lst_p) {
			for ( i = NUL; i < s_len; i++) {
				if ( ((flg == TRUE) && (t[c_pos] == s[i])) ||
			  	     ((flg == FALSE)  && (t[c_pos] != s[i])) ) {
					*c_rck = i;
					c_pos--;
					/* stop while */
					lst_p = c_pos -1;
					break;
				}/* end if */
		 	}/* end for */
			c_pos++;
		}/* end while */
	}/* end else */

	return (c_pos);
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_fnd_lst				*/
/* Returns the position of either:		*/
/* the last     matching char (flg = TRUE) or	*/
/* the last not matching char (flg = FALSE) 	*/
/* Found position can be overwritten with *d    */
/*==============================================*/
int str_fnd_lst(int c_pos, int fst_p, char *t, char *s, char *d, int flg) { 
	int s_len;
	int i;
	s_len = strlen(s);
	
	if (c_pos < NUL) {
		c_pos = strlen(t);
		c_pos--;
	}/* end if */

	switch (flg) {
	
		/* search matching position */
		case TRUE:
			if (s_len == 1) {
				/* for just one character */
				while ( (c_pos >= fst_p) &&
				 	(t[c_pos] != *s) ) {
					c_pos--;
				}/* end while */
			} /* end if */
			else {
				/* for a group of characters */
				c_pos++;
				while (c_pos >= fst_p) {
					c_pos--;
					for ( i = NUL; i < s_len; i++) {
						if (t[c_pos] == s[i]) {
							/* stop while */
							fst_p = c_pos + 1;
							break;
						}/* end if */
				 	}/* end for */
				}/* end while */
			}/* end else */           
			break;
		
		/* search not matching position */
		case FALSE:
			if (s_len == 1) {
				while ( (c_pos >= fst_p) &&
				 	(t[c_pos] == *s) ) {
					c_pos--;
				}/* end while */           
			}/* end if */           
			break;
		default:
			;
	}/* end switch */
	
	if ((c_pos >= NUL) && (*d != CHR0_VAL)) {
		t[c_pos] = *d;
	}/* end if */
	
	return (c_pos);
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_trm_lft				*/
/* (trim left side of string)			*/
/* deletes leading characters (normally blanks)	*/
/*==============================================*/
void str_trm_lft(char *t, char *d) {
	int fst_c;
	int d_len;
	int j;
	int dmmy;
	char ch[] = BLNK_CHR;

	d_len = strlen(d);
	for (j = NUL; j < d_len; j++) {
		ch[NUL] = d[j];
		fst_c = str_fnd_fst(NUL, -1, t, ch, &dmmy, FALSE);
		str_mov_lft(NUL, fst_c, t);
	}/* end for */	
	return;
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_trm_rgt				*/
/* (trim right side of string)			*/
/* deletes trailing blancs			*/
/*==============================================*/
void str_trm_rgt(char *t, char *d, int meth) {
	int lst_c;
	int d_len;
	int t_len;
	int j;                          
	char ch[] = BLNK_CHR;
	
	lst_c = strlen(t);
	d_len = strlen(d);
	do {
		t_len = lst_c;
		for (j = NUL; j < d_len; j++) {
			ch[NUL] = d[j];
			lst_c = str_fnd_lst(-1, NUL, t, ch, NULL_CHR, FALSE);
			lst_c++;
			t[lst_c] = CHR0_VAL;
		}/* end for */
		if (meth == FALSE) {
			t_len = lst_c;
		}
	} while (lst_c < t_len);
	return;
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_trm_all				*/
/* (trim both sides of string)			*/
/* deletes leading and trailing blancs		*/
/*==============================================*/
void str_trm_all(char *t, char *d, int meth) {
	str_trm_lft(t, d);
	str_trm_rgt(t, d, meth);
	return;
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_mov_lft				*/
/* (move content of string from right to left)	*/
/* source in 'lst_i' / target in 'fst_i'	*/
/*==============================================*/
void str_mov_lft(int fst_i, int lst_i, char *t) {
	lst_i--;
	do {
		lst_i++;
		t[fst_i] = t[lst_i];
		fst_i++;
	} while (t[lst_i] != CHR0_VAL);
	return;
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION chk_num_val				*/
/* Returns error code -1			*/
/*==============================================*/
int chk_num_val(char *n) {								
	int rc = NUL;
	int fst_i;
	int lst_i;
	int i;
	int dmmy;
	
	/* skip leading blanks */
	fst_i = str_fnd_fst(NUL, -1, n, BLNK_CHR, &dmmy, FALSE);

	/* skip trailing blanks */
	lst_i = str_fnd_lst(-1, NUL, n, BLNK_CHR, NULL_CHR, FALSE);

	for (i = fst_i; i <= lst_i; i++) {
		if ((n[i] < ZERO_VAL) || (n[i] > NINE_VAL)) {
			rc = -1;       /* error */
			i += lst_i; /* leave for */
		} /* end if */
	}/* end for */

	return (rc);
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_cut_int	(contents are changed)	*/
/* gets the first int-value in the string.	*/
/* The remaining part of the string is shifted	*/
/* to leftmost position and a new '\0' is set.  */
/* So strlen() is NUL when all values are token	*/
/* Returns the integer value of first word.	*/
/*==============================================*/
int str_cut_int(char *t) {
	int n;
	int fst_i;
	int lst_i;
	int dmmy;
	lst_i = str_fnd_fst(NUL, -1, t, BLNK_CHR, &dmmy, FALSE);
	fst_i = lst_i;
	n = str_get_int(&lst_i, t, FALSE);	
	if (lst_i > fst_i) {
		lst_i = str_fnd_fst(lst_i, -1, t, BLNK_CHR, &dmmy, FALSE);
		str_mov_lft(NUL, lst_i, t);
	}/* end if */
	/* else */
	/* something else must be found */
	
	return (n);
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION str_get_int				*/
/* gets the int_value following 'fst_i'		*/
/* Returns the value and 1st non_num_chr 'fst_i'*/
/*==============================================*/
int str_get_int(int *fst_i, char *t, int skp) {
	int i;
	int n = NUL;
	int dmmy;
	if (skp == TRUE) {
		*fst_i = str_fnd_fst(*fst_i, -1, t, BLNK_CHR, &dmmy, FALSE);
	}/* end if */
	for (i = *fst_i; ((t[i] >= ZERO_VAL) && (t[i] <= NINE_VAL)); i++) {
		n = (n * 10) + (t[i] - ZERO_VAL);
	}/* end for */    
	(*fst_i) = i;
        return (n);
}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION get_item				*/
/* nbr   = number of item to get                */
/* ftx   = input buffer (contents of the items) */
/* buf   = output buffer for text of found item */
/* i_sep = (one) character as item separator    */
/* s_lim = character-set (start) of item-region */
/* e_lim = character-set (end)   of item-region */
/* returns TRUE for valid items (or FALSE)      */
/*==============================================*/

int get_item(int nbr, char *ftx, char *buf, int b_len, char *i_sep, char *s_lim, char *e_lim) {

	int  rc = TRUE;
	
	int  gt_rc;     
	int i;

	int  f_pos = NUL;
	int  fst_i = NUL;

	int  nxt_i;
	int  nxt_k;
	int  fnd_k = NUL;
	
	int  lst_i;     

	int  t_len;
	int  d_len;
	int  dmmy = NUL;
	
	d_len = strlen(s_lim);
	
	lst_i = strlen(ftx) - 1;
	nxt_i = lst_i + 1;

	/* get begin of next item-region */
	/* (fnd_k gets the index of found char in s_lim) */
	nxt_k = str_fnd_fst(fst_i, lst_i, ftx, s_lim, &fnd_k, TRUE);

	for (i=NUL; i<nbr; i++) {

		/* store first position */
		/* of current item      */
		f_pos = fst_i;
		
		/* position of next separator */
		nxt_i = str_fnd_fst(fst_i, lst_i, ftx, &i_sep[NUL], &dmmy, TRUE);
		
		while (nxt_i > nxt_k) {
			/* position in an item-region ?   */
			/* skip to the end of that region */
			/* (fnd_k is the char-index of the region) */
			fst_i = nxt_k + 1;
			nxt_k = str_fnd_fst(fst_i, lst_i, ftx, &e_lim[fnd_k], &dmmy, TRUE);

			/* get next separator position */
			fst_i = nxt_k + 1;
			nxt_i = str_fnd_fst(fst_i, lst_i, ftx, &i_sep[NUL], &dmmy, TRUE);
			
			/* get begin of next item-region */
			nxt_k = str_fnd_fst(fst_i, lst_i, ftx, s_lim, &fnd_k, TRUE);
		}/* end while */


		fst_i = nxt_i + 1;
		
	}/* end for */
	
	t_len = nxt_i - f_pos;
	if (b_len == 0){
		b_len = t_len + 1;
	}/* end if */
	if (t_len > 0){
		gt_rc = str_cpy_mid(buf, NUL, b_len, ftx, f_pos, t_len, CHR0_CHR);
	}/* end if */	
	else {
		buf[0] = 0x00;
	}/* end else */

	if ((f_pos > lst_i) || (nbr < 1)) {
		rc = FALSE;
	}/* end if */

	return (rc);

}/* END OF FUNCTION */

/*==============================================*/
/* FUNCTION chck_range				*/
/* checks a value to fit between to limits	*/
/* returns the checked value or variable st_rc	*/
/*==============================================*/
int chck_range (int low_cnt, int act_cnt, int max_cnt, int st_rc) {
	int rc;
	rc = st_rc;
	if ((act_cnt >= low_cnt) && (act_cnt <= max_cnt)) {
		rc = act_cnt;
	}/* end if */
	return (rc);
}/* END OF FUNCTION */

/* **********************************************************************/
/* following the get item no function 					*/
/* this function can be used to determin the item number of a field 	*/
/* in a buffer with e regular field width				*/
/* Example get_item_no("AAA,BBB,CCC","BBB",3) returns 1			*/
/*         get_item_no("AAA,BBB,CCC","AAA",3) returns 0			*/
/*         get_item_no("AAA,BBB,CCC","ZZZ",3) returns -1		*/
/* **********************************************************************/
int get_item_no(char *s,char *f,short elem_len)
{
  int	rc;
  
  rc = str_fnd_str(0,strlen(s),s,f);
  if ( rc >= 0 ) { /* means we found the item */
    rc = (rc / elem_len);
  } /* end if */

  return rc;
} /* end of get_item_no */


/* **********************************************************************/
/* following the get number of items function				*/
/* this function can be used to determin the number of items in a buffer*/
/* Example get_no_of_items("AAA,BBB,CCC") returns 3			*/
/*         get_item_no("AAA") 		  returns 1			*/
/* **********************************************************************/
int get_no_of_items(char *s)
{
  int	rc=0;

  if ( strlen(s) && s) {
    rc =1;
    while (*s) {
      if (*s++ ==',') {
	    rc++;
      } /* end if */
    } /* end while */
  } /* end if */

  return rc;
} /* end of get no of items */

int getWordFrame(char *s, char *t, int wrdNbr, int *wrdBgn, int *wrdEnd)
{
  int	rc = RC_FAIL;
  int   wrdCnt;
  int   cp;

  if (s != NULL)
  {
    if ( strlen(s) > 0)
    {
      wrdCnt = 0;
      cp = 0;
      while ((s[cp] != 0x00) && (rc != RC_SUCCESS))
      {
        if (s[cp] != t[0])
        {
          wrdCnt++;
          if (wrdCnt == wrdNbr)
          {
            (*wrdBgn) = cp;
          } /* end if */
          cp++;
		  while((s[cp] != t[0]) && (s[cp] != 0x00))
          {
			cp++;
		  } /* end while */
          cp--;
          if (wrdCnt == wrdNbr)
          {
            (*wrdEnd) = cp;
            rc = RC_SUCCESS;
          } /* end if */
        } /* end if */
		  cp++;
      } /* end while */
    } /* end if */
  } /* end if */

  return rc;
} /* end of get wordFrame */


int WordCount(char *s)
{
  int	rc = 0;
  int   cp;

  if (s != NULL)
  {
    if ( strlen(s) > 0)
    {
      rc = 0;
      cp = 0;
      while (s[cp] != 0x00)
      {
        if (s[cp] != ' ')
        {
		  rc++;
		  while((s[cp] != ' ') && (s[cp] != 0x00))
          {
			cp++;
		  } /* end while */
        } /* end if */
        else {
	      cp++;
        } /* end else */
      } /* end while */

    } /* end if */
  } /* end if */

  return rc;
} /* end of get no of words */


int  get_real_item(char *dest, char *src, int ItemNbr){
	int rc = -1;
	int gt_rc;
	int b_len = 0;
	int trim = TRUE;
	if (ItemNbr < 0){
		trim = FALSE;
		ItemNbr = - ItemNbr;
	} /* end if */
	gt_rc = get_item(ItemNbr, src, dest, b_len, ",", "\0", "\0");
	if (gt_rc == TRUE){
		if (trim == TRUE){
			str_trm_rgt(dest, " ", TRUE);
		} /* end if */
		rc = strlen(dest);
	}/* end if */
	return rc;
}/* end of get_real_item */





int  GetWord(int ipWord,char *pcpBase, char *pcpDest)
{

	return GetWords(ipWord,ipWord,pcpBase,pcpDest);

} /* end for GetWord */


int  GetWords(int ipWordFrom,int ipWordTo,char *pcpBase, char *pcpDest)
{
	char	pclTemp[128];
	int		ilRetCode;
	int		ilLoopi;
    int		wrdBgn=0;
    int		wrdEnd=0;

	if (ipWordTo == -1 )  {
		ipWordTo = WordCount(pcpBase);
	} /* end for loopi */

	pcpDest[0] = 0x00;
	for (ilLoopi=ipWordFrom; ilLoopi <= ipWordTo ; ilLoopi++) {
        ilRetCode = getWordFrame(pcpBase, " ", ilLoopi, &wrdBgn, &wrdEnd);
		if (ilRetCode != RC_SUCCESS )  {
			dbg(DEBUG,"getWordFrame: word(%d) not found.",ilLoopi);
		} /* end for loopi */
        else {
          ilRetCode = StrgCopyBytes(pclTemp, 0, pcpBase, wrdBgn, wrdEnd);
          pclTemp[wrdEnd - wrdBgn + 1] = 0x00;
		  strcat(pcpDest,pclTemp);
		  strcat(pcpDest," ");
        } /* end else */
 	} /* end for loopi */

	return RC_SUCCESS;

}/* end of GetWords */

int StrgCopyBytes(char *dest, int destPos, char *src, int srcPos, int srcTo)
{
  int rc = RC_SUCCESS;
  if (srcTo < 0)
  {
    srcTo = strlen(src) - 1;
  } /* end if */
  while (srcPos <= srcTo)
  {
    dest[destPos] = src[srcPos];
    destPos++;
    srcPos++;
  } /* end while */
  return rc;
} /* end of StrgCopyBytes */

int StrgPutStrg(char *dest, int *destPos, char *src, int srcPos, int srcTo, char *deli)
{
  int rc = RC_SUCCESS;
  if (srcTo < 0)
  {
    srcTo = strlen(src) - 1;
  } /* end if */
  while (srcPos <= srcTo)
  {
    dest[*destPos] = src[srcPos];
    (*destPos)++;
    srcPos++;
  } /* end while */
  srcPos = 0;
  srcTo = strlen(deli) - 1;
  while (srcPos <= srcTo)
  {
    dest[*destPos] = deli[srcPos];
    (*destPos)++;
    srcPos++;
  } /* end while */
  return rc;
} /* end of StrgPutStrg */

 
int str_fnd_buf(int c_pos, int c_end, char *s, char *t, int *cnt, int *cp, char *v) {
  int rc = -1;
  int si = 0;
  int ti = 0;
  int sl;
  sl = strlen(s);
  if (c_end < 0) {
    c_end = --sl;
  } /* end if */
  *cnt = 0;
  *cp = -1;  
  if ((c_pos >= 0) && (sl > c_pos)){
    for (si = c_pos; (s[si]!=0x00) && (rc < 0) && (si<=c_end); si++){
      if ((*cp!=si) && (s[si]==v[0])){
        (*cnt)++;
        *cp = si;
      } /* end if */
      if (s[si]==t[0]){
        rc = si;
        for (ti=0;(t[ti]!=0x00) && (rc>=0);ti++){
          if (s[si]!=t[ti]){
            rc = -1;
            si--;
            si--;
          } /* end if */
          si++;
          if ((*cp != si) && (s[si] == v[0]) && (rc < 0)) {
            (*cnt)++;
            *cp = si;
          } /* end if */
        } /* end for */
      } /* end if */
    } /* end for */
  } /* end if */
  return rc;
} /* end of str_fnd_buf */

/* ================================= */
/* get data of items by fieldname    */
/* Attention: Caller should COPY     */
/* the resultString because returned */
/* Value is a Pointer to LocalData ! */
/* ================================= */
char *mysGet_data(char *nam, char *field, char *data)
{
  static char fld_val[2048];
  int len;
  int fld_pos;
  int buf_pos;
  int UnUsed = 0;
  buf_pos = str_fnd_buf(0, -1, field, nam, &fld_pos, &UnUsed, ",");
  fld_pos++;
  len = get_real_item(fld_val, data, -fld_pos);
  if (len < 1)
  {
    fld_val[0] = 0x20;
    fld_val[1] = 0x00;
  } /* end if */
  return fld_val;
} /* end of mysGet_data */



/*==============================================*/
/* FUNCTION StrgShiftRight				*/
/* (move part of string from right to left)	*/
/* source in 'lft_i' / target in 'rgt_i'	*/
/*==============================================*/
void StrgShiftRight(int src_i, int tgt_i, char *t)
{
	int lft_i;
	int rgt_i;
	
	lft_i = strlen(t);
	rgt_i = lft_i + (tgt_i - src_i);

	do
	{
		t[rgt_i] = t[lft_i];
		rgt_i--;
		lft_i--;
	} while (lft_i >= src_i);
	return;
}/* END OF StrgShiftRight */


/*==============================================*/
/* FUNCTION StrgShiftLeft				*/
/* (move content of string from right to left)	*/
/* source in 'lst_i' / target in 'fst_i'	*/
/*==============================================*/
void StrgShiftLeft(int fst_i, int lst_i, char *t)
{
	lst_i--;
	do {
		lst_i++;
		t[fst_i] = t[lst_i];
		fst_i++;
	} while (t[lst_i] != CHR0_VAL);
	return;
}/* END OF StrgShiftLeft */

int GetItemBounds(char *pcpData,
		   int ipItemNbr, int *pipBegin, int *pipEnd, int *pipLen)
{
  int ilRetValue = RC_SUCCESS;
  int ilFirstPos = 0;
  int ilCurrPos = 0;
  int ilLastPos = 0;

  ilLastPos = strlen(pcpData);

  /* find first char of item */
  while ((ipItemNbr > 1) && (ilCurrPos <= ilLastPos))
  {
    if (pcpData[ilCurrPos] == ',')
    {
      ilFirstPos = ilCurrPos + 1;
      ipItemNbr--;
    } /* end if */
    ilCurrPos++;
  } /* end while */

  /* find last char of item */
  while ((ipItemNbr > 0) && (ilCurrPos < ilLastPos))
  {
    if (pcpData[ilCurrPos] == ',')
    {
      ipItemNbr--;
      ilCurrPos--;
    } /* end if */
    ilCurrPos++;
  } /* end while */

  ilCurrPos--;
  (*pipBegin) = ilFirstPos;
  (*pipLen) = ilCurrPos - ilFirstPos + 1;
  if (ilCurrPos < ilFirstPos)
  {
    ilCurrPos = ilFirstPos;
  } /* end if */
  (*pipEnd) = ilCurrPos;
  if (ipItemNbr != 0)
  {
    ilRetValue = RC_FAIL;
  } /* end if */
} /* end of GetItemBounds */


/* ******************************************************************** */
/* change NUL-Separator in an databuffer (form an Item-List)   */
/* ******************************************************************** */
void BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields,
                            char *pcpSepChr)
{
  int ilChrPos = 0;
  if (ipNoOfFields < 1)
  {
    ipNoOfFields = field_count(pcpFieldList);
  } /* end if */
  ipNoOfFields--;
  while (ipNoOfFields > 0)
  {
    if (pcpData[ilChrPos] == 0x00)
    {
      pcpData[ilChrPos] = pcpSepChr[0];
      ipNoOfFields--;
    } /* end if */
    ilChrPos++;
  } /* end while */
  return;
}/* end of BuildItemBuffer */


/***
char cgClientString[] = "\042\047\012\015\054\072\173\175\133\135\174\136\077\050\051\073\100\000";
char cgServerString[] = "\260\261\264\263\262\277\223\224\225\226\227\230\231\233\234\235\236\000";
*************/

char cgClientString[] = "\042\047\054\012\015\000";
char cgServerString[] = "\260\261\262\264\263\000";


int ConvertDbStringToClient(char *pcpString)
{
     char *pclPos;
		int ilPos;

			while(*pcpString != '\0')
			{
				pclPos = strchr(cgServerString,*pcpString);
				if (pclPos != NULL)
				{
					ilPos = pclPos - cgServerString;
					*pcpString = cgClientString[ilPos];
				}
			pcpString++;
			}
		return RC_SUCCESS;
}

int ConvertClientStringToDb(char *pcpString)
{
	char *pclPos;
	int ilPos;
	char *pclTmpString;

	while(*pcpString != '\0')
	{
		pclPos = strchr(cgClientString,*pcpString);
		if (pclPos != NULL)
		{
			ilPos = pclPos - cgClientString;
			*pcpString = cgServerString[ilPos];
		}
	pcpString++;
	}
	return RC_SUCCESS;
}






/*
 =================================================================================================
 Function CedaGetKeyItem
 -------------------------------------------------------------------------------------------------
 Strips data from a textbuffer using a keyword and an indicator of the key's 'end of data'.

 Purpose and mainly use: find a text anchor by any pattern and determine the end of data
 by another pattern. This function will be used by the Full Document Interpreter (fdihdl)
 and by the Ceda Data Request Handler (cdrhdl) as well. It will be incorporated into the
 new 'accelerated communication' layer of the (roostering) client applications also.
 -------------------------------------------------------------------------------------------------
 There are 4 IN parameters:
 pcpTextBuff: The text stream that has to be analysed
 pcpKeyCode:  The keyword (pattern of anchor) of the text that is wanted 
 pcpItemEnd:  The pattern that defines the end of the wanted text
 bpCopyData:  A flag that indicates if the identified data will be copied into the resultbuffer.
              This feature enables the caller to get the pointer to the data and the size of data
              regardless of the size of the given resultbuffer.
 -------------------------------------------------------------------------------------------------
 And 2 OUT parameters:
 pcpResultBuff: will contain the extracted data stream (if bpCopyData == true)
 pcpResultSize: always contains the size of the identified data.
 -------------------------------------------------------------------------------------------------
 Return Value:  The (char) pointer to the identified item data in the text or NULL.
 =================================================================================================
 
 Normal usage:
 -------------------------------------------------------------------------------------------------
 Let's assume that we got a text stream (pclTextBuff) like:

 "{=CMD=}RTA{=HOP=}ATH{=MODUL=}{=DATA=}Here is a lot of data\nwith several rows ....";

 and we want to extract the 'HOPO' section: We use the keyword {=HOP=} and as indicator for
 EndOfData we use "{=" from the beginning of the next keyword. Finally we decide to get the
 data back into the 'pclResult' buffer and set the flag CopyData to true.
 
 pclDataBegin = CedaGetKeyItem(pclResult, &llSize, pclTextBuff, "{=HOP=}", "{=", true);
 if (pclDataBegin != NULL)
 {
   printf("{=HOP=} :\t<%s>\n",pclResult);
 } 

 Now we receive the data in pclResult and llSize contains the length.
 Additionally the returned value in pclDataBegin points to the original position of the
 data inside the text stream of pclTextBuff for any purposes. 
 -------------------------------------------------------------------------------------------------
 We have different possibilities to determine what we found:
 Case 1: 
		CopyData set to true ...
		... and pclDataBegin != NULL:	The keyword exists
		... and llSize > 0 :			pclResult contains valid data
		... (or llSize == 0:			The keyword section is empty, pclResult is set to '\0')
		CopyData set to true ...
		... and pclDataBegin == NULL:	The keyword does not exist
		... then llSize = 0 :			No data available
		... and							pclResult is set to '\0'
 Case 2: 
		CopyData set to false ...		The result buffer remains unchanged
		... and pclDataBegin != NULL:	The keyword exists
		... and llSize > 0 :			pclDataBegin points to valid data
		... (or llSize == 0:			The keyword section is empty)
		CopyData set to false ...		
		... and pclDataBegin == NULL:	The keyword does not exist
		... then llSize == 0 and it makes no sense to proceed.
 -------------------------------------------------------------------------------------------------
 Note: (Special Usage)
 When you expect a very big data stream of several Mb you surely won't copy it into your local
 pclResult buffer. In such cases it is always the best way to set the CopyFlag to 'false': You
 will get the DataPointer and DataLength and you proceed working directly on the received data.
 =================================================================================================
*/

/* ======================================================================================== */
char *CedaGetKeyItem(char *pcpResultBuff, long *plpResultSize, 
				 char *pcpTextBuff, char *pcpKeyWord, char *pcpItemEnd, int bpCopyData)
{
	long llDataSize    = 0L;
	char *pclDataBegin = NULL;
	char *pclDataEnd   = NULL;
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord);				/* Search the keyword		*/
	if (pclDataBegin != NULL)
	{															/* Did we find it? Yes.		*/
		pclDataBegin += strlen(pcpKeyWord);						/* Skip behind the keyword	*/
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd);			/* Search end of data		*/
		if (pclDataEnd == NULL)
		{														/* End not found?			*/
			pclDataEnd = pclDataBegin + strlen(pclDataBegin);	/* Take the whole string	*/
		} /* end if */
		llDataSize = pclDataEnd - pclDataBegin;					/* Now calculate the length	*/
		if (bpCopyData == TRUE)
		{														/* Shall we copy?			*/
			strncpy(pcpResultBuff, pclDataBegin, llDataSize);	/* Yes, strip out the data	*/
		} /* end if */
	} /* end if */
	if (bpCopyData == TRUE)
	{															/* Allowed to set EOS?		*/
		pcpResultBuff[llDataSize] = 0x00;						/* Yes, terminate string	*/
	} /* end if */
	*plpResultSize = llDataSize;								/* Pass the length back		*/
	return pclDataBegin;										/* Return the data's begin	*/
} /* end CedaGetKeyItem */
/* ======================================================================================== */




