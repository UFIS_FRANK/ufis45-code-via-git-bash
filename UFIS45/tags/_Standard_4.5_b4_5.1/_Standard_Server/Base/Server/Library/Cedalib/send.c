#ifndef _DEF_mks_version_send_c
  #define _DEF_mks_version_send_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_send_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/send.c 1.2 2004/07/20 15:30:49SGT bst Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/ 
/* Contains Function:   SendCedaEvent()                                      */
/*                      GetCedaTimestamp()                                   */
/*                      SendStatus()                                         */
/*****************************************************************************/ 
#define _SEND_C

static char sccs_send_lib[] ="@(#) UFIS 4.4 (c) ABB AAT/I send.c 44.3 / 00/01/20 13:35:59 / RKL";

#include "send.h"

extern int   mod_id;
extern char *mod_name;

extern int   debug_level;

/*****************************************************************************/
/* Function:    SendCedaEvent()                                              */
/*****************************************************************************/ 
/* Parameter:   IN : ipRouteId    address to send the answer
 *                   ipOrgId      Set to mod_id if < 1
 *                   pcpDstNam    User Name
 *                   pcpRcvNam    Workstation name
 *                   pcpStart     used by FLIGHT as BC count
 *                   pcpEnd       used by FLIGHT as StateStamp
 *                   pcpCommand   Command send
 *                   pcpTabName   Table name
 *                   pcpSelection Selection Block
 *                   pcpFields    Field Block
 *                   pcpData      Data Block
 *                   pcpErrDscr   Error description string
 *                   ipPrior      Priority of send
 *                   ipRetCode    RC_FAIL,RC_SUCCESS,NETOUT_NO_ACK
 * Return:      RC_SUCCESS, RC_FAIL
 *
 * Description: This function is handles all functionalities from
 *              older functions:
 *                       tools_send_sql (),
 *                       tools_send_err (),
 *                       tools_send_sql_rc (),
 *                       tools_send_sql_prio (),
 *                       tools_send_sql_flag (),
 *                       tools_send_info_flag (),
 *                       new_tools_send_sql ().
 *
 *              It sends an Event/Statement to the specified mod_id.
 *              The result may be sent to the passed originator mod_id.
 *
 *              If ipRetCode parameter is set to RC_FAIL (Error occured)
 *              the error description (pcpErrDscr) will be written to
 *              BCHD->data. Here Clients look for the error. So the
 *              Command block will lay behind error description.
 *              If ipRetCode parameter is set to NETOUT_NO_ACK there
 *              will be send no answer to the sender.
 * 
 * History:
 *           ??.05.99 twe Written
 *           20.08.99 rkl remove Terminate()
 *           20.01.00 rkl commented debug
 * 
 *****************************************************************************/
int SendCedaEvent (int  ipRouteId,     /* adress to send the answer   */
                   int  ipOrgId,       /* Set to mod_id if < 1        */
                   char *pcpDstNam,    /* BC_HEAD.dest_name           */
                   char *pcpRcvNam,    /* BC_HEAD.recv_name           */
                   char *pcpStart,     /* CMDBLK.tw_start             */
                   char *pcpEnd,       /* CMDBLK.tw_end               */
                   char *pcpCommand,   /* CMDBLK.command              */
                   char *pcpTabName,   /* CMDBLK.obj_name             */
                   char *pcpSelection, /* Selection Block             */
                   char *pcpFields,    /* Field Block                 */
                   char *pcpData,      /* Data Block                  */
                   char *pcpErrDscr,   /* Error description           */
                   int  ipPrio,        /* 0 or Priority               */
                   int  ipRetCode)     /* BC_HEAD.rc: RC_SUCCESS or   */
                                       /*             RC_FAIL or      */
                                       /*             NETOUT_NO_ACK   */
{
  char *pclFct = "SendCedaEvent";
  static EVENT   *prlOutEvent = NULL;           /* only once allocated   */
  static int     ilCurLen     = 0;              /* old size for realloc  */
  int            ilRC         = RC_SUCCESS;     /* return value          */
  int            ilLength     = 0;              /* current needed length */
  int            ilPrio       = 0;              /* Priority              */
  BC_HEAD        *prlBchd     = NULL;           /* Broadcast header      */
  CMDBLK         *prlCmdBlk   = NULL;           /* Command Block         */
  char           *pclDataBlk  = NULL;           /* Data Block            */
  char           *pclSelBlk   = NULL;           /* Selection Block       */
  char           *pclFieldBlk = NULL;           /* Field Block           */
  char           *pclPtr      = NULL;


  if (ipRouteId <= 0)        /* no sendings to not existing processes  */
  {
     return ilRC;
  } /* end if */

  if (ipRetCode == RC_FAIL)            /* ACHTUNG: Zuvor != RC_SUCCESS */
  {
     /* if there was an error then the data area gets copied to
        the area BCDH->data
     */
     ilLength = strlen(pcpFields) + strlen(pcpErrDscr) + strlen(pcpData) + 
                strlen(pcpSelection) + sizeof(BC_HEAD)  + sizeof(CMDBLK) +
                sizeof(EVENT) + 128;
  } /* end if RC_FAIL */
  else
  {
     ilLength = strlen(pcpFields) + strlen(pcpData) + 
                strlen(pcpSelection) + sizeof(BC_HEAD)  + sizeof(CMDBLK) +
                sizeof(EVENT) + 128;  
     pclPtr = strstr(pcpErrDscr,"{-TIM-}");
     if (pclPtr != NULL)
     {
       pclPtr += 7;
     }
  } /* end else */

  /*
      The memory for EVENT becomes only once allocated and reallocated
      only if the new data size is bigger than from call before.
  */
  if ((prlOutEvent == NULL) && (ilCurLen == 0))
  {
     prlOutEvent = (EVENT *) malloc (ilLength);
     if (prlOutEvent == NULL)
     {
        ilRC= RC_FAIL;
	    debug_level = TRACE;
        dbg(TRACE,"%s: Malloc %d for EVENT failed <%s>",
                   pclFct, ilLength, strerror(errno));
        /*rkl 20.08.99
  	     * Terminate (); 
	     */
     } /* end if malloc fail */
     else
     {
        dbg(DEBUG, "%s: Allocated %d memory for EVENT", pclFct, ilLength);
        ilCurLen = ilLength;
        memset (((char *) prlOutEvent), 0, ilLength); 
     } /* end else */
  } /* end if first call */
  else
  {
     if (ilLength > ilCurLen)   /* realloc, data size larger than before */
     {
        prlOutEvent = (EVENT *) realloc (prlOutEvent, ilLength);
        if (prlOutEvent == NULL)
        {
           ilRC= RC_FAIL;
           debug_level = TRACE;
           dbg(TRACE,"%s: Realloc %d for EVENT failed <%s>",
                     pclFct, ilLength, strerror(errno));
           /*rkl 20.08.99
	        * Terminate ();
	        */
        } /* end if realloc fail */
        else
        {
           dbg(DEBUG, "%s: Reallocated to %d memory for EVENT",
                       pclFct, ilLength);
           ilCurLen = ilLength;
           memset (((char *) prlOutEvent), 0, ilLength); 
        } /* end else */
     } /* end if realloc */
  } /* end else */

  /* fill allocated memory (EVENT) */

  prlOutEvent->data_offset = sizeof(EVENT);
  if (ipOrgId > 0)
  {
     prlOutEvent->originator = ipOrgId;
  } /* end if */
  else
  {
     prlOutEvent->originator = mod_id;              /* should be global */
  } /* end else */
  prlOutEvent->data_length =  ilLength;
  prlOutEvent->command = EVENT_DATA;

  prlBchd = (BC_HEAD *) (((char *)prlOutEvent) + sizeof(EVENT));
  prlBchd->rc = ipRetCode;
  strcpy (prlBchd->dest_name, pcpDstNam);
  strcpy (prlBchd->orig_name, "");
  if (pclPtr != NULL)
  {
    strcpy (prlBchd->orig_name, pclPtr);
  }
  strcpy (prlBchd->recv_name, pcpRcvNam);
  strcpy (prlBchd->seq_id, "");
  strcpy (prlBchd->ref_seq_id, "");

  /*
     If Error message is send, the Event structure has
     another form, because dll use BCHD->data for error
     description in case of RC != RC_SUCCESS.
     So data containing the error description is written
     into BCHD->data (else no data). So the pointer to command
     block lays behind the error description
     (prlBchd->data + strlen(pcpErrDscr)).
  */
  
  if( ipRetCode == RC_FAIL)    /* ERROR!  ACHTUNG: Zuvor != RC_SUCCESS */
  {
     strcpy(prlBchd->data, pcpErrDscr);
     /*  set command block behind  */
     prlCmdBlk = (CMDBLK  *) ( ((char *)prlBchd->data) + strlen(pcpErrDscr) +1);
  } /* end if RC_FAIL */
  else
  {
      /*  set command block normal  */ 
      prlCmdBlk= (CMDBLK  *) ((char *)prlBchd->data);
  } /* end else */
 
  strcpy (prlCmdBlk->command, pcpCommand);
  strcpy (prlCmdBlk->tw_start, pcpStart);
  strcpy (prlCmdBlk->tw_end, pcpEnd);
 
  pclSelBlk = prlCmdBlk->data;
  strcpy (pclSelBlk, pcpSelection);
  strcpy (prlCmdBlk->obj_name, pcpTabName);
  pclFieldBlk = (char *) pclSelBlk + strlen (pclSelBlk) + 1;
  strcpy (pclFieldBlk, pcpFields);
  pclDataBlk   = pclFieldBlk + strlen(pclFieldBlk) + 1;
  strcpy (pclDataBlk, pcpData);

  /*rkl 20.01.00
   * dbg(DEBUG,"%s: rc<%d> Cmd<%s> Tab<%s>",
   *           pclFct, prlBchd->rc,prlCmdBlk->command,prlCmdBlk->obj_name); 
   * dbg(DEBUG,"%s: Sel<%s>", pclFct, prlCmdBlk->data);
   * dbg(DEBUG,"%s: Field<%s>", pclFct, pclFieldBlk);
   *
   * if (strlen(pclDataBlk) < 2048 )
   * {
   *   dbg(DEBUG,"%s: DataSize= <%d>", pclFct, strlen(pclDataBlk));
   *   dbg(DEBUG,"%s: Data<%s>", pclFct, pclDataBlk);
   * } 
   * else
   * {
   *   dbg(DEBUG,"%s: DataSize= <%d>", pclFct, strlen(pclDataBlk));
   * } 
   */
  if (ipRetCode == RC_FAIL)
  {
     dbg(DEBUG,"%s: ERROR<%s>", pclFct, prlBchd->data);
  } /* end if */

  if ((ipPrio >= PRIORITY_1) && (ipPrio <= PRIORITY_5))
  {
     ilPrio = ipPrio;
  }
  else    
  {
     ilPrio = PRIORITY_3;
  }
  
  if(ilRC == RC_SUCCESS)
  {
    ilRC = que(QUE_PUT, ipRouteId, mod_id, ilPrio, ilLength, (char *)prlOutEvent);
    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"%s: function que() failed rc=%d", pclFct, ilRC); 
      dbg(TRACE,"%s: No answer to %d from %d send",
                pclFct, ipRouteId, mod_id);
      ilRC = RC_FAIL;
    }
  } 

  return ilRC;

} /* end of SendCedaEvent */

/*****************************************************************************/
int GetCedaTimestamp(char *pcpResultBuf,
                     int    ipResultLen,
                     long   lpTimeOffset)
{
  int	    ilRC = RC_SUCCESS;		/* Return code */
  int       ilTimeLen = 0;
  time_t    llCurTime = 0;
  struct tm *prlLocalTimeResult;
  struct tm rlTimeStruct;
  char      pclTimeStamp[16];

  if(ilRC == RC_SUCCESS)
  {
	if(pcpResultBuf == NULL)
	{
		dbg(TRACE,"GetCedaTimestamp: invalid 1.st argument <0x%8.8x>",pcpResultBuf);
		ilRC = RC_FAIL;
	}/* end of if */
  }/* end of if */

  if(ilRC == RC_SUCCESS)
  {
	if(ipResultLen < 15)
	{
		dbg(TRACE,"GetCedaTimestamp: invalid 2.st argument <%d>",ipResultLen);
		dbg(TRACE,"GetCedaTimestamp: minimum of 15 characters required!");
		ilRC = RC_FAIL;
	}/* end of if */
  }/* end of if */

  if(ilRC == RC_SUCCESS)
  {
	memset((void*)&pclTimeStamp[0], 0x00, 16);
	llCurTime = time(0L);
	llCurTime += lpTimeOffset;
	prlLocalTimeResult = (struct tm *) localtime(&llCurTime);
	memcpy((char *)&rlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm));
	ilTimeLen = strftime(pclTimeStamp,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTimeStruct);
	strcpy(pcpResultBuf,pclTimeStamp);
  }/* end of if */

  return ilRC;

} /* end of GetCedaTimestamp */



/*****************************************************************************/
/* Function:   SendStatus()                                                  */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In    : char  *pcpCommand    - Command                                    */
/* In    : char  *pcpIFType     - Interfacetyp                               */
/* In    : char  *pcpIFName     - Interfacename                              */
/* In    : char  *pcpMsgTyp     - Messagetyp                                 */
/* In    : int     ipMsgNumber  - Messagenumber                              */
/* In    : char  *pcpData1      - Data1                                      */
/* In    : char  *pcpData1      - Data2                                      */
/* In    : char  *pcpData1      - Data3                                      */
/*                                                                           */
/* Global Variables:                                                         */
/*                                                                           */
/* Returncode:  RC_SUCCESS - all OK                                          */
/*           :  RC_FAIL    - Failure                                         */
/*                                                                           */
/*                                                                           */
/* Description: Send Status to stahdl                                        */
/*                                                                           */
/* History:                                                                  */
/*           02.07.99  rkl  Written                                          */
/*                                                                           */
/*****************************************************************************/
int SendStatus( char *pcpIfTyp, char *pcpIfName, char *pcpMsgTyp, 
                int    ipMsgNumber, 
                char *pcpData1, char *pcpData2,  char *pcpData3)
{
  char *pclFct   = "SendStatus";
  char *pclTable = "STATAB";
  char *pclCommand = "STA";
  int    ilRC     = RC_SUCCESS;
  int    ilRCSend = RC_SUCCESS;
  int    ilToStaHdl = 0;
  int    ilMyOwnPid = 0;
  char  pclDestName[10]    = "";
  char  pclRecvName[10]    = "";
  char  pclTimestamp[16]   = "";
  char pclErrorBuffer[512] = "";
  int    ilStrgLen = 0;
  static char *pclSelBuf       = NULL;
  static int    ilSelBufSize   = 0;
  static char *pclFieldBuf     = NULL;
  static int    ilFieldBufSize = 0;
  static char *pclDataBuf      = NULL;
  static int    ilDataBufSize  = 0;

  ilToStaHdl = tool_get_q_id("stahdl"); 
  ilMyOwnPid = getpid();  /* Get My own Pid */

  if(ilToStaHdl > 0)
  {
    strcpy(pclDestName,mod_name);
    strcpy(pclRecvName,"CEDA");
    ilRC = GetCedaTimestamp(&pclTimestamp[0],15,0);

    /* Selection Buffer */
    ilStrgLen = strlen(mod_name) + 8 + strlen(pcpIfName)+16;
    ilRC = HandleBuffer(&pclSelBuf,&ilSelBufSize,0,ilStrgLen+1,64);
    /* mod_name,mod_id,Interfacetyp,Interfacename,MsgTyp */
    sprintf(pclSelBuf,"%s,%d,%s,%s,%s",mod_name,mod_id,
                                       pcpIfTyp,pcpIfName,pcpMsgTyp);

    /* Field Buffer */
    ilStrgLen = strlen(pclRecvName) + 6 + strlen(pclTimestamp) + 4;
    ilRC = HandleBuffer(&pclFieldBuf,&ilFieldBufSize,0,ilStrgLen+1,64);
    sprintf(pclFieldBuf,"%s,%d,%s,%d",pclRecvName,ilMyOwnPid,pclTimestamp,
                                      ipMsgNumber);

    /* Data Buffer */
    ilStrgLen = strlen(pcpData1) + strlen(pcpData2) + strlen(pcpData3);
    ilRC = HandleBuffer(&pclDataBuf,&ilDataBufSize,0,ilStrgLen+1,64);
    sprintf(pclDataBuf,"%s,%s,%s",pcpData1,pcpData2,pcpData3);

  
    ilRCSend = SendCedaEvent (ilToStaHdl, 0, pclDestName, pclRecvName, 
                              pclTimestamp, "", pclCommand,
                              pclTable, pclSelBuf, pclFieldBuf,
                              pclDataBuf, pclErrorBuffer, 0, ilRC);
    if(ilRCSend == RC_FAIL)
    {
      ilRC = RC_FAIL;
      dbg(TRACE,"%s: SendCedaEvent failed <%d>",pclFct,ilRCSend);
    } /* end if */
  } /* end if */
    
  return ilRC;

} /* end of SendStatus() */

