#ifndef _DEF_mks_version_list_c
  #define _DEF_mks_version_list_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_list_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/list.c 1.2 2004/08/10 20:22:06SGT jim Exp  $";
#endif /* _DEF_mks_version */

/******************************************************************
*   Name        : list.c                                           *
*   Author      : MCU                                              *
*   Date       	: 04.12.96                                         *
*   Description : Library module, provides functions to create and *
*                handle doubly-linked lists and a quicksort method *
*                to sort these lists.                              *
*   Update history    :                                            *
*********************************************************************/

#include <stdlib.h>
#include <memory.h>
#include "glbdef.h"
#include "list.h"


/* function prototypes **/

LPLISTELEMENT ListMakeFirst(LPLISTHEADER prpList,void *pvpData);
LPLISTELEMENT ListDeleteOne(LPLISTHEADER prpList);
int ListCalculateCount(LPLISTHEADER prpList);
LPLISTELEMENT ListFreeElement(LPLISTELEMENT prpListElement);
LPLISTELEMENT ListAllocateElement(int ipSize);
LPLISTELEMENT ListMoveToLast(LPLISTHEADER prpList,LPLISTELEMENT prpListElement);
LPLISTHEADER ListFreeHeader(LPLISTHEADER prpListHeader);
LPLISTHEADER ListAllocateHeader(void);
static int (*CCSCompare)(void *,void *);
int CCSSort(LPLISTHEADER lpListe,int (*LocalCompare)(void *,void *));
void QuickSort(LPLISTELEMENT a[],int left,int right,
				 int (*LocalCompare)(void *,void *));


LPLISTHEADER ListAllocateHeader(void)
{
   LPLISTHEADER prlListHeader;
   
   prlListHeader = malloc(sizeof(LISTHEADER)+10);
	 if (prlListHeader == NULL)
   {
     /*dbg(0,"Not enough memory to allocate list header");*/
   }
   return prlListHeader;
}

LPLISTHEADER ListFreeHeader(LPLISTHEADER prpListHeader)
{
   free(prpListHeader);
   return NULL;
}

LPLISTELEMENT ListAllocateElement(int ipSize)
{
   LPLISTELEMENT prlListElement = NULL;

	if ((prlListElement = malloc(sizeof(LISTELEMENT)+200)) == NULL)
	{
		 /*dbg(TRACE,"LIST: Not enough memory to allocate list element");*/
	}
	else
	{
		if ((prlListElement->Data = malloc(ipSize+20)) == NULL)
		{
		/*dbg(TRACE,"LIST: Not enough memory to allocate memory for list element data");*/
		free(prlListElement);
		prlListElement = NULL;
		}
	}
	return prlListElement;
}

LPLISTELEMENT ListFreeElement(LPLISTELEMENT prpListElement)
{
   free(prpListElement->Data);
   free(prpListElement);
   return NULL;
}


int ListCalculateCount(LPLISTHEADER prpList)
{
   LPLISTELEMENT prlListElement;
   int ilCount = 0;

   for (prlListElement = prpList->First; prlListElement != NULL; prlListElement = prlListElement->Next)
      {
      ilCount++;
      }
   prpList->Count = ilCount;
   return ilCount;
}


LPLISTELEMENT ListAppend(LPLISTHEADER prpList,void  *pvpData)
{
   LPLISTELEMENT prlListElement;

   if (prpList->First == NULL)
      {
      return ( ListMakeFirst(prpList,pvpData));
      }

   prlListElement = ListAllocateElement(prpList->Size);
   prlListElement->Prev = prpList->Last;
   prlListElement->Next = NULL;
   prlListElement->Prev->Next = prlListElement;
   prpList->Last = prlListElement;
   prpList->Current = prlListElement;
   prpList->Count++;
   memcpy(prlListElement->Data,pvpData,prpList->Size);
	 /*dbg(DEBUG,"ListAppend: copied data to pointer <%x>",prlListElement->Data);*/
   return (prlListElement);
}


LPLISTELEMENT ListDelete(LPLISTHEADER prpList)
{
	LPLISTELEMENT prlListElement;

	if (prpList->First == NULL)
	{
		return ( NULL );
	}

	if (prpList->First == prpList->Last)
	{
		return ( ListDeleteOne(prpList));
	}

	if (prpList->Current == prpList->First)
	{
		return ( ListDeleteFirst(prpList));
	}

	if (prpList->Current == prpList->Last)
	{
		return ( ListDeleteLast(prpList));
	}

	prpList->Current->Prev->Next = prpList->Current->Next;
	prpList->Current->Next->Prev = prpList->Current->Prev;
	prpList->Count--;
	prlListElement = prpList->Current;

	prpList->Current   = prpList->Current->Next;
	if (prpList->Current == NULL)
	{
		prpList->Current = prpList->Last;
	}
	prlListElement = ListFreeElement(prlListElement);
	return (prpList->Current);
}


LPLISTELEMENT ListMoveToLast(LPLISTHEADER prpList,LPLISTELEMENT prpListElement)
{
	/*dbg(DEBUG,"ListMoveToLast %p first %p last %p prev %p next %p",
		prpList,prpList->Last,prpListElement->Prev,prpListElement->Next);*/
	if (prpListElement != prpList->Last)
	{
		if (prpListElement == prpList->First)
		{
		prpList->First = prpListElement->Next;
		prpListElement->Next->Prev = NULL;
		prpListElement->Prev = prpList->Last;
		prpList->Last->Next = prpListElement;
		prpList->Last = prpListElement;
		prpList->Last->Next = NULL;
		}
		else
		{
		prpListElement->Prev->Next = prpListElement->Next;
		prpListElement->Next->Prev = prpListElement->Prev;
		prpListElement->Prev = prpList->Last;
		prpList->Last->Next = prpListElement;
		prpList->Last = prpListElement;
		prpList->Last->Next = NULL;
		}
	}
	return prpList->Current;
}

LPLISTELEMENT ListDeleteOne(LPLISTHEADER prpList)
{
	prpList->First = ListFreeElement(prpList->First);

	prpList->Count--;
	prpList->Current = NULL;
	prpList->First = NULL;
	prpList->Last = NULL;
	return (NULL);
}

LPLISTELEMENT ListDeleteFirst(LPLISTHEADER prpList)
{
	if (prpList->First == NULL)
	{
		return (NULL);
	}
	if (prpList->First->Next == NULL)
	{
		return(ListDeleteOne(prpList));
	}
	prpList->Current = prpList->First->Next;
	prpList->First = prpList->Current;
	prpList->First->Prev = ListFreeElement(prpList->First->Prev);
	prpList->Count--;

	return (prpList->Current);
}

LPLISTELEMENT ListDeleteLast(LPLISTHEADER prpList)
{
	if (prpList->Last == NULL)
	{
		return (NULL);
	}
	if (prpList->Last->Prev == NULL)
	{
		return(ListDeleteOne(prpList));
	}
	prpList->Current = prpList->Last->Prev;
	prpList->Last = prpList->Current;
	prpList->Last->Next = ListFreeElement(prpList->Last->Next);
	prpList->Last->Next = NULL;
	prpList->Count--;

	return (prpList->Current);
}

LPLISTHEADER ListDestroy(LPLISTHEADER prpList)
{
	if (prpList == NULL)
	{
		return (NULL);
	}
	while (ListDeleteFirst(prpList) != NULL)
	{
	;
	}
	prpList = ListFreeHeader(prpList);
	return (NULL);
}

LPLISTELEMENT ListFindFirst(LPLISTHEADER prpList)
{
   prpList->Current = prpList->First;
   return (prpList->Current);
}

LPLISTELEMENT ListFindLast(LPLISTHEADER prpList)
{
   prpList->Current = prpList->Last;
   return (prpList->Last);
}

LPLISTELEMENT ListFindNext(LPLISTHEADER prpList)
{
   if (prpList->Current == NULL)
      {
      return (NULL);
      }
   if (prpList->Current->Next == NULL)
      {
      return (NULL);
      }
   prpList->Current = prpList->Current->Next;
   return (prpList->Current);
}

LPLISTELEMENT ListFindPrevious(LPLISTHEADER prpList)
{
   if (prpList->Current == NULL)
      {
      return (NULL);
      }
   if (prpList->Current->Prev == NULL)
      {
      return (NULL);
      }
   prpList->Current = prpList->Current->Prev;
   return (prpList->Current);
}

LPLISTHEADER ListInit(LPLISTHEADER prpList,int ipSize)
{
	if (prpList != NULL)
	{
		prpList = ListDestroy(prpList);
	}
	prpList = ListAllocateHeader();
	prpList->First = NULL;
	prpList->Last = NULL;
	prpList->Current = NULL;
	prpList->Size = ipSize;
	prpList->Count = 0;
	return (prpList);
}

LPLISTELEMENT ListInsert(LPLISTHEADER prpList, void  *pvpData)
{
	LPLISTELEMENT prlListElement;
	if (prpList->First == NULL)
	{
		return ( ListMakeFirst(prpList,pvpData));
	}
	if (prpList->Current == prpList->First)
	{
		return ( ListInsertFirst(prpList,pvpData));
	}
	prlListElement = ListAllocateElement(prpList->Size);
	prlListElement->Prev = prpList->Current->Prev;
	prlListElement->Next = prpList->Current;
	prpList->Current->Prev->Next = prlListElement;
	prpList->Current->Prev = prlListElement;
	prpList->Current = prlListElement;
	prpList->Count++;

	memcpy(prlListElement->Data,pvpData,prpList->Size);
	return (prlListElement);
}

LPLISTELEMENT ListInsertFirst(LPLISTHEADER prpList,void *pvpData)
{
	LPLISTELEMENT prlListElement;

	prlListElement = ListAllocateElement(prpList->Size);
	prlListElement->Prev = NULL;
	prlListElement->Next = prpList->First;
	prpList->First->Prev = prlListElement;
	prpList->Current = prlListElement;
	prpList->First = prlListElement;
	prpList->Count++;
	memcpy(prlListElement->Data,pvpData,prpList->Size);
	return (prlListElement);
}

LPLISTELEMENT ListMakeFirst(LPLISTHEADER prpList,void *pvpData)
{
	LPLISTELEMENT prlListElement;

	prlListElement = NULL;
	prlListElement = ListAllocateElement(prpList->Size);
	prlListElement->Prev = NULL;
	prlListElement->Next = NULL;
	prpList->Last = prlListElement;
	prpList->Current = prlListElement;
	prpList->First = prlListElement;
	prpList->Count++;

	if (pvpData != NULL)
	{
		memcpy(prlListElement->Data,pvpData,prpList->Size);
	 	/*dbg(DEBUG,"ListMakeFirst: copied data to pointer <%x>",prlListElement->Data);*/
	}
	return (prlListElement);
}

LPLISTELEMENT ListFindKey(LPLISTHEADER prpList,void *pvpBase, void *pvpOffset,int ipKeyType,void *pvpKey)
{

	LPLISTELEMENT prlEle;
	int ilOffset;
	
	ilOffset = (char *)pvpOffset - (char *)pvpBase;

	/*dbg(TRACE,"ListFindKey Count %d, KeyType %d,Offset %d Base %d Diff. %d",prpList->Count,
		ipKeyType,pvpOffset,pvpBase,ilOffset);*/

	for(prlEle = prpList->First; prlEle != NULL; prlEle = prlEle->Next)
	{
		switch (ipKeyType)
		{
		case LKEY_INT: if (*((int *)pvpKey) == *((int *)((char *)prlEle->Data+ilOffset)))
			   {
						prpList->Current = prlEle;
					   return prlEle;
			   }
				 break;
		case LKEY_LONG: 
		/*dbg(TRACE,"%ld %ld",
		*((long *)pvpKey),*((long *)((char *)prlEle->Data+ilOffset)));*/
		

		if(*((long *)pvpKey)==*((long *)((char *)prlEle->Data+ilOffset)))
			   {
					prpList->Current = prlEle;
					return prlEle;
			   }
			break;
	   case LKEY_CHAR:
			{
			char *pclKeyToSearchFor;
			char *pclKeyToCompare;
			pclKeyToSearchFor =  (char *)pvpKey;
			pclKeyToCompare = (char *)((char *)prlEle->Data+ilOffset);
			/*dbg(DEBUG,"Key=<%s> Liste=<%s>",pclKeyToSearchFor,
					pclKeyToCompare);*/
			if (strcmp(pclKeyToSearchFor,pclKeyToCompare) == 0)
			{
				prpList->Current = prlEle;
				return prlEle;
			}
			}
					break;
		}
	}
	return NULL;
}

int CCSSort(LPLISTHEADER lpListe,int (*LocalCompare)(void *,void *))
{
	LPLISTELEMENT *a;
	int    nLen = 0, i = 0;
 	LPLISTELEMENT pL;

	if (lpListe == NULL)
	{
		return RC_SUCCESS;
	}
	if (lpListe->First == lpListe->Last)
	{
		return RC_SUCCESS;
	}
	pL = ListFindFirst(lpListe);
	if (pL == NULL)
	{
		return (RC_FAIL);
	}
	/*
	while (pL != NULL)
	{
		nLen++;
		pL = ListFindNext(lpListe);
	}
	*/
	nLen = lpListe->Count;

	if ((a = malloc((nLen+2)*sizeof(int *))) == NULL)
	{
		return RC_FAIL;
	}
	pL = ListFindFirst(lpListe);
	while (pL != NULL)
	{
		a[  i++ ] = pL;
		pL = ListFindNext(lpListe);
	}
	/*dbg(0,"In sort, before QuickSort");*/
	QuickSort(a,0,i-1,LocalCompare);
  free(a); 
	return (RC_SUCCESS);
}


void QuickSort(LPLISTELEMENT a[],int left,int right,
				 int (*LocalCompare)(void *,void *))
{
	int l,r;
	void *pSave;
	static ilDepth = 0;

	l = left;
	r = right;

	ilDepth++;
	/*dbg(0,"in QuickSort, depth = %d",ilDepth);*/

	CCSCompare = LocalCompare;
	pSave = (*(a + l))->Data;
	while (l != r)
	{
		while (((*CCSCompare)((*(a + r))->Data,pSave) >= 0) && (l < r))
		{
			--r;
		}
		(*(a + l))->Data = (*(a + r))->Data;
		while (((*CCSCompare)((*(a+l))->Data,pSave) <= 0) && (l < r))
		{
			++l;
		}
		(*(a + r))->Data = (*(a + l))->Data;
	}
	(*(a + l))->Data = pSave;
	if (left < (l -1))
	{
		QuickSort(a,left,l-1,CCSCompare);
	}
	if ((l+1) < right)
	{
		QuickSort(a,(l+1),right,CCSCompare);
	}
	ilDepth--;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
LPLISTELEMENT ListFindIndexElement(LPLISTHEADER prpList, int ipIdx)
{
	int				ilInternalIdx;
	LPLISTELEMENT	prlEle;

	if (prpList == NULL)
		return NULL;
	if (ipIdx < 0)
		return NULL;
	if (ipIdx > prpList->Count-1)
		return NULL;
	
	for (ilInternalIdx=0, prlEle=prpList->First; ilInternalIdx<ipIdx && prlEle != NULL; prlEle=prlEle->Next, ilInternalIdx++)
		; /* nothing to do here */

	/* bye bye */
	if (prlEle)
	{
		prpList->Current = prlEle;
		return prlEle;
	}
	else
		return NULL;
}

void ListSetItemNames(LPLISTHEADER prpList, char *pcpItemNames)
{
	if (prpList == NULL)
		return;
	if (pcpItemNames == NULL)
		return;
	strncpy(prpList->pcItemNames, pcpItemNames, 2048);
	return;
}

LPLISTELEMENT ListFindItem(LPLISTHEADER prpList, char *pcpItem, int ipItemType, char *pcpKey, int ipFirstNext)
{
	int				ilItemPos;
	char				*pclItemToCompare;
	char				pclInternalItem[1024+1];
	char				pclInternalKey[1024+1];
	LPLISTELEMENT	prlEle;

	if (prpList == NULL)
		return NULL;
	if (pcpItem == NULL)
		return NULL;
	if (ipItemType != iITEM_NAME && ipItemType != iITEM_NUMBER)
		return NULL;
	if (pcpKey == NULL)
		return NULL;

	if (strlen(pcpItem) > 1024)
	{
		dbg(TRACE,"<ListFindIten> length of item > 1024 byte, can't work...");
		return NULL;
	}
	if (strlen(pcpKey) > 1024)
	{
		dbg(TRACE,"<ListFindIten> length of key > 1024 byte, can't work...");
		return NULL;
	}
	strcpy(pclInternalKey, pcpKey);
	strcpy(pclInternalItem, pcpItem);

	switch (ipItemType)
	{
		case iITEM_NAME:
			/* get position of this item in list... */
			ilItemPos = GetIndex(prpList->pcItemNames, pclInternalItem, ',');
			break;

		case iITEM_NUMBER:
			ilItemPos = atoi(pclInternalItem);
			break;
	}

	/* check index */
	if (ilItemPos < 0)
		return NULL;

	if (ipFirstNext == iFIRST_ITEM)
	{
		prlEle = prpList->First;
	}
	else
	{
		prlEle = prpList->Current;
		prlEle = prlEle->Next;
	}

	for (; prlEle != NULL; prlEle = prlEle->Next)
	{
		pclItemToCompare = GetDataField(prlEle->Data, (unsigned int)ilItemPos, ',');	
		if (!strcmp(pclItemToCompare, (char*)pclInternalKey))
		{
			/* found key in list... */
			prpList->Current = prlEle;
			return prlEle;
		}
	}
	return NULL;
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/


