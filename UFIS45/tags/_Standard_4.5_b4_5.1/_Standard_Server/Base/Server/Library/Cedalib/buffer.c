#ifndef _DEF_mks_version_buffer_c
  #define _DEF_mks_version_buffer_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_buffer_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/buffer.c 1.2 2004/08/10 20:10:55SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/ 
/* Contains Function:   HandleBuffer()                                       */
/*                      FreeBuffer()                                         */
/*****************************************************************************/ 
#define _BUFFER_C


#include "buffer.h"

extern int  debug_level;


/*****************************************************************************/ 
/* Function:   HandleBuffer                                                  */
/*****************************************************************************/ 
/* Parameter:                                                                */
/* In/Out:  char **pcpBuffer         - Pointer to Pointer to Buffer          */
/* In/Out:  int   *pipAlcBufSize     - Allocated Bufferlen                   */
/* In    :  int    pipUsedBufSize    - currently Used Bufferlen              */
/* In    :  int    ipFreeBufSizeNeed - Needed FreeBufferSize in Byte         */
/* In    :  int    ipBufStpSize      - Stepsize in Byte which added to Buffer*/
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*              RC_FAIL    - Wrong Parameter                                 */
/*                                                                           */
/* Description: Handles all Allocations made to Buffer (pcpBuffer)           */
/*              Depening on needed size and existing size                    */
/*                                                                           */
/* Note:        Parameter ipFreeBufSizeNeed must be != 0                     */
/*              If calloc() or realloc() fails  -> Terminate Programm        */
/*                                                                           */
/* History:                                                                  */
/*           28.04.99  rkl  Written                                          */
/*           12.05.99  rkl  Modified Struktur (smaller)                      */
/*           20.08.99  rkl  remove Terminate()                               */
/*                                                                           */
/*****************************************************************************/
int HandleBuffer( char **pcpBuffer,
                  int   *pipAlcBufSize     , int ipUsedBufSize,
                  int    ipFreeBufSizeNeed , int ipBufStpSize   )
{
  char *pclFct="HandleBuffer";
  int  ilRC = RC_SUCCESS; /* Return code */
  char pclErrData[256]="";
  int  ilNofStp     = 0;
  int  ilNewAlcBufSize = 0;

  /* Calculate new Sizes of Buffer */  
  ilNewAlcBufSize = ipUsedBufSize + ipFreeBufSizeNeed;
  if (*pipAlcBufSize >= ilNewAlcBufSize)      /* no alloc needed */
  {
    return ilRC;
  }

  /* Buffer needs malloc or realloc */
  if( ipBufStpSize > 0)             /* alloc in steps of BufStpSize */
  { 
    ilNofStp = ilNewAlcBufSize / ipBufStpSize;
    if ((ilNewAlcBufSize % ipBufStpSize) > 0)
    {
      ilNofStp++;
    }
    ilNewAlcBufSize = ilNofStp*ipBufStpSize;
    
  } /* end if */

  if ((*pcpBuffer == NULL) && (*pipAlcBufSize == 0))            /* No Buffer */
  {  
    *pcpBuffer = (char *) calloc(1,ilNewAlcBufSize);
  } /* end if */		        
  else if ((*pcpBuffer != NULL) && (*pipAlcBufSize > 0)) /*There is an Buffer */
  {
    *pcpBuffer = (char *) realloc(*pcpBuffer,ilNewAlcBufSize);
  } /* end else if */
  else
  {
    dbg(TRACE,"%s: Wrong Parameters send !",pclFct);
    ilRC = RC_FAIL;
    return ilRC;
  }


  if(*pcpBuffer == NULL)
  {
    debug_level = TRACE;
    dbg(TRACE,"%s: ALLOC %d FAILED %s",pclFct, 
              ilNewAlcBufSize, strerror(errno));
    sprintf(pclErrData,"Allocating %d Bytes  %s",
                      ilNewAlcBufSize, strerror(errno));
    ilRC = RC_FAIL;
    /*rkl 20.08.99
     * Terminate();
     */
  } /* end if */
  else
  {
    dbg(TRACE,"%s: Buffersize Changed from %d -> %d",
              pclFct, *pipAlcBufSize, ilNewAlcBufSize);
    *pipAlcBufSize = ilNewAlcBufSize;
  } /* end else */   

  return ilRC;
   
} /* end of HandleBuffer() */

/* ***************************************************************************/ 
/* Function:   FreeBuffer                                                    */
/* ***************************************************************************/ 
/* Parameter:                                                                */
/* In/Out:  char **pcpBuffer      - Pointer to Pointer to Buffer             */
/* In/Out:  int   *pipAlcBufSize  - Allocated Bufferlen                      */
/* In/Out:  int   *pipUsedBufSize - currently Used Bufferlen                 */
/*                                                                           */
/* Returncode:  RC_SUCCESS - alles OK                                        */
/*                                                                           */
/* Description:  Free Buffer                                                 */
/*                                                                           */
/* History:                                                                  */
/*           28.04.99  rkl  Written                                          */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
int FreeBuffer( char **pcpBuffer,
                int   *pipAlcBufSize, int *pipUsedBufSize )
{
  char *pclFct="FreeBuffer";
  int   ilRC = RC_SUCCESS;
  
  if( *pcpBuffer != NULL)
  {
    dbg(TRACE,"%s: FREE BUFFER",pclFct);
    free(*pcpBuffer);
    *pcpBuffer = NULL;
    *pipAlcBufSize = 0;
    *pipUsedBufSize = 0;
  } /* end if */ 
   
  return ilRC;
} /* end of FreeBuffer() */

