#ifndef _DEF_mks_version_debugrec_c
  #define _DEF_mks_version_debugrec_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_debugrec_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/debugrec.c 1.8 2006/02/21 21:38:32SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** *
 * 20041103 JIM: added DebugPrintEventLine and DebugPrintItemLine for compressed
 *               debug of item and event 
 * 20041117 JIM: added cpCaller: string of calling routine in
 *               DebugPrintEventLine and DebugPrintItemLine
 * 20041123 JIM: added fcUnknownFunc for CMD_FUNC to dbg unknown number
 * 20050313 JIM: added fcUnknownEvent for EVT_FUNC to dbg unknown event
 *               added DebugPrintItemEvent for better output of event dbg by sysqcp
 * 20060216 JIM: declared clDummy as static in fcUnknownFunc and fcUnknownEvent
 *               to avoid compiler warning ( - no error happened till now )
 * 20060221 JIM: moved SwitchDebugFile and SwitchDebugFileExt to dbg.c
 * 20060221 JIM: removed sccs_debugrec_lib
 * ******************************************************************** 
*/

#ifndef __DEBUGREC_INC
#define __DEBUGREC_INC

#include "ugccsma.h"
#include "debugrec.h"

/* ******************************************************************** */
/* print ITEM members                                                   */
/* ******************************************************************** */
int	DebugPrintItem( int ipLevel, ITEM *prpitmItem )
{
	int	ilRC	= RC_SUCCESS;
	
	if( prpitmItem == NULL)
	{
		dbg(TRACE,"DebugPrintItem: NULL-pointer not valid");
		ilRC = RC_FAIL;
	}else{
		dbg(ipLevel,"item->function     (%d)",prpitmItem->function);
		dbg(ipLevel,"item->route        (%d)",prpitmItem->route);
		dbg(ipLevel,"item->priority     (%d)",prpitmItem->priority);
		dbg(ipLevel,"item->msg_length   (%d)",prpitmItem->msg_length);
		dbg(ipLevel,"item->originator   (%d)",prpitmItem->originator);
	}/* end of if */

	return(ilRC);
	
}/* end of DebugPrintItem */

/* The function fcUnknownFunc is used by macro CMD_FUNC if string macro of 
   func is not configured
*/
char *fcUnknownFunc(int func)
{
static char clDummyStr[32];
   sprintf(clDummyStr,"%-15d",func);
   return clDummyStr;
}

/* The function fcUnknownEvent is used by macro EVT_FUNC if string macro of 
   func is not configured
*/
char *fcUnknownEvent(int func)
{
static char clDummyStr[32];
   sprintf(clDummyStr,"%-15d",func);
   return clDummyStr;
}

/* 20050313 JIM: better Event-Logging in SYSQCP: */
int	DebugPrintItemEvent( int ipLevel, char *cpCaller, ITEM *prpitmItem )
{
EVENT *prlEvent;
BC_HEAD *prlBchd;
CMDBLK *prlCmdblk;

	int	ilRC	= RC_SUCCESS;
	
	if( prpitmItem == NULL)
	{
		dbg(TRACE,"%-10s: DebugPrintItemEvent: NULL-pointer not valid",cpCaller);
		ilRC = RC_FAIL;
	}
  else
  {
    if ((prpitmItem->msg_length==0) || 
        (prpitmItem->function==QUE_CREATE) ||
        (prpitmItem->function==QUE_SHM))
    {
      dbg(ipLevel,"%-10s: %5d > %5d: %15s,p=%1d,l=%8d:     -,-",
           cpCaller,prpitmItem->originator,prpitmItem->route,
           CMD_FUNC(prpitmItem->function),
           prpitmItem->priority,prpitmItem->msg_length);
    }
    else
    {
      prlEvent=(EVENT *) prpitmItem->text;
      if (prlEvent->command == EVENT_DATA)
      {
         prlBchd = (BC_HEAD *) ((char *) prlEvent + sizeof (EVENT));
         prlCmdblk = (CMDBLK *) ((char *) prlBchd->data);
         dbg(ipLevel,"%-10s: %5d > %5d: %15s,p=%1d,l=%8d: %5d,%s/%s(%d)",
           cpCaller,prpitmItem->originator,prpitmItem->route,
           CMD_FUNC(prpitmItem->function),
           prpitmItem->priority,prpitmItem->msg_length,
           prlEvent->type,EVT_FUNC(prlEvent->command),
           prlCmdblk->command,prlEvent->originator);
      }
      else
      {
         dbg(ipLevel,"%-10s: %5d > %5d: %15s,p=%1d,l=%8d: %5d,%s(%d)",
           cpCaller,prpitmItem->originator,prpitmItem->route,
           CMD_FUNC(prpitmItem->function),
           prpitmItem->priority,prpitmItem->msg_length,
           prlEvent->type,EVT_FUNC(prlEvent->command),prlEvent->originator);
      }
    }
	}/* end of if */

	return(ilRC);
	
}/* end of DebugPrintItemLine */


/* 20041117 JIM: added cpCaller: string of calling routine */
int	DebugPrintItemLine( int ipLevel, char *cpCaller, ITEM *prpitmItem )
{
	int	ilRC	= RC_SUCCESS;
	
	if( prpitmItem == NULL)
	{
		dbg(TRACE,"%-10s: DebugPrintItem: NULL-pointer not valid",cpCaller);
		ilRC = RC_FAIL;
	}else{
		dbg(ipLevel,"%-10s: item: funct,route,prio,len,orig:       (%15s,%5d,%1d,%8d,%5d)",
           cpCaller,CMD_FUNC(prpitmItem->function),prpitmItem->route,
           prpitmItem->priority,prpitmItem->msg_length,prpitmItem->originator);
	}/* end of if */

	return(ilRC);
	
}/* end of DebugPrintItemLine */


/* ******************************************************************** */
/* print event members                                                  */
/* ******************************************************************** */
int	DebugPrintEvent( int ipLevel, EVENT *prpevnEvent )
{
	int	ilRC	= RC_SUCCESS;

	if( prpevnEvent == NULL)
	{
		dbg(TRACE,"DebugPrintEvent: NULL-pointer not valid");
		ilRC = RC_FAIL;
	}else{
		dbg(ipLevel,"event->type        (%d)",prpevnEvent->type);
		dbg(ipLevel,"event->command     (%d)",prpevnEvent->command);
		dbg(ipLevel,"event->originator  (%d)",prpevnEvent->originator);
		dbg(ipLevel,"event->sys_ID      (%s)",prpevnEvent->sys_ID);
		dbg(ipLevel,"event->retry_count (%d)",prpevnEvent->retry_count);
		dbg(ipLevel,"event->data_offset (%d)",prpevnEvent->data_offset);
		dbg(ipLevel,"event->data_length (%d)",prpevnEvent->data_length);
	}/* end of if */

	return(ilRC);

}/* end of DebugPrintEvent */


/* ******************************************************************** */
/* print event members                                                  */
/* ******************************************************************** */
/* 20041117 JIM: added cpCaller: string of calling routine */
int	DebugPrintEventLine( int ipLevel, char *cpCaller, EVENT *prpEvnt )
{
	int	ilRC	= RC_SUCCESS;

	if( prpEvnt == NULL)
	{
		dbg(TRACE,"%-10s: DebugPrintEvent: NULL-pointer not valid");
		ilRC = RC_FAIL;
	}else{
		dbg(ipLevel,"%-10s: event: typ,cmd,orig,ID,retry,offs,len: (%5d,%5d,%5d,%2d,%2d,%8d,%8d)",
           cpCaller,prpEvnt->type,prpEvnt->command,prpEvnt->originator,prpEvnt->sys_ID,
           prpEvnt->retry_count,prpEvnt->data_offset,prpEvnt->data_length);
	}/* end of if */

	return(ilRC);

}/* end of DebugPrintEventLine */

/* ******************************************************************** */
/* print BCHEAD members                                                 */
/* ******************************************************************** */
int	DebugPrintBchead( int ipLevel, BC_HEAD *prpbchBchead)
{
	int	ilRC	= RC_SUCCESS;

	if( prpbchBchead == NULL)
	{
		dbg(TRACE,"DebugPrintBchead: NULL-pointer not valid");
		ilRC = RC_FAIL;
	}else{
		dbg(ipLevel,"bchead->dest_name  (%s)",prpbchBchead->dest_name);
		dbg(ipLevel,"bchead->orig_name  (%s)",prpbchBchead->orig_name);
		dbg(ipLevel,"bchead->recv_name  (%s)",prpbchBchead->recv_name);
		dbg(ipLevel,"bchead->bc_num     (%d)",prpbchBchead->bc_num);
		dbg(ipLevel,"bchead->seq_id     (%s)",prpbchBchead->seq_id);
		dbg(ipLevel,"bchead->tot_buf    (%d)",prpbchBchead->tot_buf);
		dbg(ipLevel,"bchead->act_buf    (%d)",prpbchBchead->act_buf);
		dbg(ipLevel,"bchead->ref_seq_id (%s)",prpbchBchead->ref_seq_id);
		dbg(ipLevel,"bchead->rc         (%d)",prpbchBchead->rc);
		dbg(ipLevel,"bchead->tot_size   (%d)",prpbchBchead->tot_size);
		dbg(ipLevel,"bchead->cmd_size   (%d)",prpbchBchead->cmd_size);
		dbg(ipLevel,"bchead->data_size  (%d)",prpbchBchead->data_size);
	}/* end of if */

	return(ilRC);

}/* end of DebugPrintBchead */


/* ******************************************************************** */
/* print CMDBLK members                                                 */
/* ******************************************************************** */
int	DebugPrintCmdblk(int ipLevel, CMDBLK *prpcblCmdBlk )
{
	int	ilRC	= RC_SUCCESS;

	if( prpcblCmdBlk == NULL)
	{
		dbg(TRACE,"DebugPrintCmdblk: NULL-pointer not valid");
		ilRC = RC_FAIL;
	}else{
		dbg(ipLevel,"cmdblk->command    (%s)",prpcblCmdBlk->command);
		dbg(ipLevel,"cmdblk->obj_name   (%s)",prpcblCmdBlk->obj_name);
		dbg(ipLevel,"cmdblk->order      (%s)",prpcblCmdBlk->order);
		dbg(ipLevel,"cmdblk->tw_start   (%s)",prpcblCmdBlk->tw_start);
		dbg(ipLevel,"cmdblk->tw_end     (%s)",prpcblCmdBlk->tw_end);
	}/* end of if */

	return(ilRC);

}/* end of DebugPrintCmdblk */


/* ******************************************************************** */
/* print FTPConfig members                                              */
/* ******************************************************************** */
int	DebugPrintFTPConfig(int ipLevel, FTPConfig *prpcblFTPConfig)
{
	int	ilRC	= RC_SUCCESS;

	if(prpcblFTPConfig == NULL)
	{
		dbg(TRACE,"DebugPrintFTPConfig: NULL-pointer not valid");
		ilRC = RC_FAIL;
	}else{
		dbg(ipLevel,"ftpcfg->iModID                  (%d)",prpcblFTPConfig->iModID);
		dbg(ipLevel,"ftpcfg->iFtpRC                  (%d)",prpcblFTPConfig->iFtpRC);
		dbg(ipLevel,"ftpcfg->pcCmd                   (%s)",prpcblFTPConfig->pcCmd);
		dbg(ipLevel,"ftpcfg->pcHostName              (%s)",prpcblFTPConfig->pcHostName);
		dbg(ipLevel,"ftpcfg->pcUser                  (%s)",prpcblFTPConfig->pcUser);
		dbg(ipLevel,"ftpcfg->pcPasswd                (%s)",prpcblFTPConfig->pcPasswd);
		dbg(ipLevel,"ftpcfg->cTransferType           (%c)",prpcblFTPConfig->cTransferType);
		dbg(ipLevel,"ftpcfg->iTimeout                (%d)",prpcblFTPConfig->iTimeout);
		dbg(ipLevel,"ftpcfg->lGetFileTimer           (%ld)",prpcblFTPConfig->lGetFileTimer);
		dbg(ipLevel,"ftpcfg->lReceiveTimer           (%ld)",prpcblFTPConfig->lReceiveTimer);
		dbg(ipLevel,"ftpcfg->iDebugLevel             (%d)",prpcblFTPConfig->iDebugLevel);
		dbg(ipLevel,"ftpcfg->iClientOS               (%d)",prpcblFTPConfig->iClientOS);
		dbg(ipLevel,"ftpcfg->iServerOS               (%d)",prpcblFTPConfig->iServerOS);
		dbg(ipLevel,"ftpcfg->iRetryCounter           (%d)",prpcblFTPConfig->iRetryCounter);
		dbg(ipLevel,"ftpcfg->iDeleteRemoteSourceFile (%d)",prpcblFTPConfig->iDeleteRemoteSourceFile);
		dbg(ipLevel,"ftpcfg->iDeleteLocalSourceFile  (%d)",prpcblFTPConfig->iDeleteLocalSourceFile);
		dbg(ipLevel,"ftpcfg->iSectionType            (%d)",prpcblFTPConfig->iSectionType);
		dbg(ipLevel,"ftpcfg->iInvalidOffset          (%d)",prpcblFTPConfig->iInvalidOffset);
		dbg(ipLevel,"ftpcfg->iTryAgainOffset         (%d)",prpcblFTPConfig->iTryAgainOffset);
		dbg(ipLevel,"ftpcfg->pcHomeAirport           (%s)",prpcblFTPConfig->pcHomeAirport);
		dbg(ipLevel,"ftpcfg->pcTableExtension        (%s)",prpcblFTPConfig->pcTableExtension);
		dbg(ipLevel,"ftpcfg->pcLocalFilePath         (%s)",prpcblFTPConfig->pcLocalFilePath);
		dbg(ipLevel,"ftpcfg->pcLocalFileName         (%s)",prpcblFTPConfig->pcLocalFileName);
		dbg(ipLevel,"ftpcfg->pcRemoteFileName        (%s)",prpcblFTPConfig->pcRemoteFileName);
		dbg(ipLevel,"ftpcfg->pcRemoteFilePath        (%s)",prpcblFTPConfig->pcRemoteFilePath);
		dbg(ipLevel,"ftpcfg->iSendAnswer             (%d)",prpcblFTPConfig->iSendAnswer);
		dbg(ipLevel,"ftpcfg->cStructureCode          (%c)",prpcblFTPConfig->cStructureCode);
		dbg(ipLevel,"ftpcfg->cTransferMode           (%c)",prpcblFTPConfig->cTransferMode);
		dbg(ipLevel,"ftpcfg->iStoreType              (%d)",prpcblFTPConfig->iStoreType);
	}/* end of if */

	return(ilRC);

}/* end of DebugPrintCmdblk */

/* ******************************************************************** */
/* print ACTIONConfig members                                              */
/* ******************************************************************** */
int	DebugPrintACTIONConfig(int ipLevel, ACTIONConfig *prpAConfig)
{
	int	ilRC	= RC_SUCCESS;

	if(prpAConfig == NULL)
	{
		dbg(TRACE,"DebugPrintACTIONConfig: NULL-pointer not valid");
		ilRC = RC_FAIL;
	}
	else
	{
		dbg(ipLevel,"actioncfg->iADFlag            (%d)",prpAConfig->iADFlag);
		dbg(ipLevel,"actioncfg->iEmptyFieldHanding (%d)",prpAConfig->iEmptyFieldHandling);
		dbg(ipLevel,"actioncfg->iIgnoreEmptyFields (%d)",prpAConfig->iIgnoreEmptyFields);
		dbg(ipLevel,"actioncfg->pcSndCmd           (%s)",prpAConfig->pcSndCmd);
		dbg(ipLevel,"actioncfg->pcSectionName      (%s)",prpAConfig->pcSectionName);
		dbg(ipLevel,"actioncfg->pcTableName        (%s)",prpAConfig->pcTableName);
		dbg(ipLevel,"actioncfg->pcFields           (%s)",prpAConfig->pcFields);
		dbg(ipLevel,"actioncfg->pcSectionCommands  (%s)",prpAConfig->pcSectionCommands);
		dbg(ipLevel,"actioncfg->iModID             (%d)",prpAConfig->iModID);
		dbg(ipLevel,"actioncfg->iSuspendOwn        (%d)",prpAConfig->iSuspendOwn);
		dbg(ipLevel,"actioncfg->pcSuspendOriginator(%s)",prpAConfig->pcSuspendOriginator);
	}
	return(ilRC);
}

#endif

/* ******************************************************************** */
/*                      E N D   O F   F I L E                           */
/* ******************************************************************** */
