#ifndef _DEF_mks_version_chkval_c
  #define _DEF_mks_version_chkval_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkval_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/chkval.c 1.8 2004/08/10 20:41:23SGT jim Exp  $";
#endif /* _DEF_mks_version */
/***************************************************************************/
/*                                                                         */
/* ABB ACE/FC Lib Functions <REGELWERK>                                    */
/*                                                                         */
/* Author         :  GHE                                                   */
/* Date           :                                                        */
/* Description    :                                                        */ 
/*                : original source, with testcases & description can you  */
/*                  find in ChkVal.tst.c                                   */
/* Update history : 11.11.1999 GHE                                         */
/*                                                                         */
/***************************************************************************/ 
/*                                                                         */
/* source-code-control-system version string                               */


/* be carefule with strftime or similar functions !!!                      */
/*                                                                         */
/***************************************************************************/ 
/*                                                                         */
/*  filename of this module's               : Chkval.c  (in DIR $LS/DB)    */
/*  lib-name                                : DBLIB.a                      */
/*                                                                         */
/*                                                                         */
/***************************************************************************/ 
/* FUNCTIONS ARE:                                                          */
/* ==============                                                          */
/*                                                                         */
/* InitValidityModule (int ipMode, char *pcpTable )                        */
/*                                                                         */
/*     where : - ipMode is the define for array or sql direct call         */
/*               .CHKVAL_ARRAY : use array function for DB table handle    */
/*               .CHKVAL_DB    : use DB (SQL) direct                       */
/*             - pcpTable the name for the used table                      */
/*                                                                         */
/*     return values are:  0 -- RC_SUCCESS        Init ok !                */
/*                        -1 -- RC_FAIL           -.-  nok                 */
/*                                                                         */
/*     indepence: the calling function use access for the DB               */
/*     attention: in case of CHKVAL_DB the function returns with an open   */
/*                SQL cursor and a defined global COMMON data range        */
/*                                                                         */
/*     intr. tables  : SYSTAB, VALTAB                                      */
/*     intr. sgs.tab : ---                                                 */ 
/***************************************************************************/ 
/*                                                                         */
/* UpdateValidityModule (EVENT *prpEvent )                                 */
/*                                                                         */
/*     where : - prpEvent is the first par. that the proc. rec. via queue  */
/*               If this par. has no information an error occured. Normaly */
/*               this EVENT includes an IRT, URT, DRT command, which       */
/*               access is for the VATTAB (eq. pcpTable name) table        */
/*                                                                         */
/*     rem:    for CHKVAL_DB mode no statement is important. The function  */
/*             returns in this case RC_SUCCESS                             */
/*                                                                         */ 
/*     return values are:  0 -- RC_SUCCESS  Init ok !  (always 4 CHKVAL_DB)*/  
/*                        -1 -- RC_FAIL           -.-  nok                 */
/*                                                                         */
/*     indepences: - the calling function use access for the DB            */
/*                 - prev. the InitValidate() must succesfully called      */ 
/*                                                                         */
/*     intr. tables  : SYSTAB, VALTAB                                      */
/*     intr. sgs.tab : ---                                                 */ 
/*                                                                         */
/***************************************************************************/ 
/*                                                                         */
/* CheckValidity (char *pcpAppl, char *pcpUrno, char *pcpDate,             */
/*                int *piplsValid)                                         */
/*                                                                         */
/*     where : - pcpAppl is a script from the calling function, which must */
/*               be equal with VALTAB.APPL. This parameter must exist and  */
/*               does'nt never changed!                                    */
/*             - pcpUrno is a Urno, which must be equal with VALTAB.URUE   */
/*               This parameter must exist and does'nt never changed!      */
/*             - pcpDate is a CEDA date, like this: YYYYMMDDhhmmss.        */
/*               This parameter must exist and does'nt never changed!      */
/*             - piplsValid is the return value for the check result       */
/*                . 0    - not successfully (no rec. found)  FALSE         */
/*                         CHKVAL_INVALID                                  */ 
/*                . 1    - successfully (found a rec. for this cases) TRUE */
/*                         CHKVAL_VALID                                    */
/*                                                                         */ 
/*     return values are:  0 -- RC_SUCCESS                                 */  
/*                        -1 -- RC_FAIL                                    */
/*                                                                         */
/*     indepences: - the calling function use access for the DB            */
/*                 - prev. the InitValidate() must succesfully called      */ 
/*                                                                         */
/*     intr. tables  : SYSTAB, VALTAB                                      */
/*     intr. sgs.tab : ---                                                 */ 
/*                                                                         */
/***************************************************************************/ 
/*                                                                         */
/* CleanupValidityModule ( void )                                          */
/*                                                                         */
/*     frees allocated memory and resets all status information            */
/*                                                                         */
/*     return values are:  0 -- RC_SUCCESS                                 */  
/*                        -1 -- RC_FAIL                                    */
/*                                                                         */
/*     indepences: - the calling function use access for the DB            */
/*                                                                         */
/*     attention: in case of CHKVAL_DB the function must close the open    */
/*                SQL cursor and frees the defined global COMMON data      */ 
/*                area range                                               */
/*                                                                         */
/*     intr. tables  : ---                                                 */
/*     intr. sgs.tab : ---                                                 */ 
/*                                                                         */
/***************************************************************************/


/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "infofkt.h"
#include "AATArray.h"     /* using TWE array functions  */
#include "chkval.h"       /* always the last header !!! */ 



/*****************************************************************************/
/* External variables                                                        */
/*****************************************************************************/
extern int debug_level;
/*****************************************************************************/
/* Global variables                                                          */
/*****************************************************************************/
/* static ITEM    *prgItem            = NULL ;       / * The queue item pointer  */
/* static EVENT   *prgEvent           = NULL ;       / * The event pointer       */
static int     igItemLen           = 0 ;          /* length of incoming item */
static int     igInitOK            = FALSE ;
static char    cgConfigFile[512] ; 
static char    pcgArrName[64] ;                   /* name of created array   */
static int     igArrLoaded ;                      /* arr init cntrl.         */
static int     igSQLLoaded ;                      /* SQL init cntrl.         */

static char    cgSqlDeclareBuffer [DECL_MAX_BUFFER_SIZE] ; /* declare buf */  

static char    cpgSQLStatement [1024] ;   /* insert xy,xx from tab ..     */
static char    cgSqlFieldBuffer [1024] ;  /* HOPO;AAAX;.....              */
static char    cgSqlDKFieldBuffer [1024] ;/* HOPO,AAAX,.....              */
static char    cgSqlDecVarFlds [1024] ;   /* INTO VALUES :HOPO,:AAAX,...  */
static char    cgAddFlds [1024] ;         /* additional field list        */
static short   sgSQL_cursor ;
static short   sgSQL_fkt ;
static short   slgArr ;                   /* 1 -> arr.fkt. is useable */ 
static char    cgArrName[64] ;            /* name of the used array   */
static char    cgIdxName[64] ;            /* name of the used index   */
char    cgSelStr[512] ="";            /* incl. where def.         */ 
static long    lgAddFldLens[260] ;
static long    lgFldOff[260] ;
static long    lgFldLens[260] ;
static HANDLE  hlgArrHdl ;                /* name of the used handle */ 
static HANDLE  hlgIdxHdl ;                /* name of the used index handle */ 


   
static char    cpgTable[32] ;       /* global mark for used table name */
static int     igChkValWorkMode ;   /* array or DB, by init            */ 
static int igVafrIdx = -1;
static int igVatoIdx = -1;
static int igFreqIdx = -1;
static int igTimfIdx = -1;
static int igTimtIdx = -1;


/* set fields in brackets for not used !!! */
static tFIELD_DEF asgFldScpt[] =  { "APPL",   8, 0, "", "", 0, 1 , 
                                    "FREQ",   7, 0, "", "", 0, 1 , 
                                    "HOPO",   3, 0, "", "", 0, 1 , 
                                    "URNO",  10, 0, "", "", 0, 1 ,    
                                    "VAFR",  14, 0, "", "", 0, 1 , 
                                    "VATO",  14, 0, "", "", 0, 1 , 
                                    "UVAL",  10, 0, "", "", 0, 1 ,   /* add fields */ 
                                    "TABN",   6, 0, "", "", 0, 1 ,   
                                    "CDAT",  14, 0, "", "", 0, 1 ,   
                                    "LSTU",  14, 0, "", "", 0, 1 ,                          
                                    "TIMF",   4, 0, "", "", 0, 1 ,   
                                    "TIMT",   4, 0, "", "", 0, 1 ,  
                                    "URUE",  10, 0, "", "", 0, 1 ,     
                                    "USEC",  32, 0, "", "", 0, 1 ,  
                                    "USEU",  32, 0, "", "", 0, 1 ,
                                    "\0"  ,   0, 0, "", "", 0, 1 
                                  } ; 

  /*---------------------------------------------------------------------------------*/
  /* the next line gets additional fields, while a change exist at date 11.04.2000,  */
  /* An update of fields, between the executable time, doesnt work correctly,        */ 
  /* without the additional fields     GHe                                           */  
  /* I saved the original struct below                                               */
  /*---------------------------------------------------------------------------------*/ 

/* ------ START ORGINAL ---------
static tFIELD_DEF asgFldScpt[] =  { "APPL",   8, 0, "", "", 0, 1 , 
                        / *         "CDAT",  14, 0, "", "", 0, 1 ,   * / 
                                    "FREQ",   7, 0, "", "", 0, 1 , 
                                    "HOPO",   3, 0, "", "", 0, 1 , 
                        / *         "LSTU",  14, 0, "", "", 0, 1 ,   * /
                        / *         "TIMF",   4, 0, "", "", 0, 1 ,   * /
                        / *         "TIMT",   4, 0, "", "", 0, 1 ,   * /
                                    "URNO",  10, 0, "", "", 0, 1 ,    
                        / *         "URUE",  10, 0, "", "", 0, 1 ,   * /   
                        / *         "USEC",  32, 0, "", "", 0, 1 ,   * /
                        / *         "USEU",  32, 0, "", "", 0, 1 ,   * /
                                    "VAFR",  14, 0, "", "", 0, 1 , 
                                    "VATO",  14, 0, "", "", 0, 1 , 
                                    "UVAL",  10, 0, "", "", 0, 1 ,   
                        / *         "TABN",   6, 0, "", "", 0, 1 ,   * / 
                                    "\0"  ,   0, 0, "", "", 0, 1 
                                  } ; 
----- END ORGINAL ---- */


/* following table is only for test ! */ 
/* ------------------------------------------------------------------------- */
   /* in moment, use is the following sequenz from:   #define SEL_DB_FIELDS  
   buffer [start_x]   |   length  | end [\0 position]   |  name
   -------------------+-----------+---------------------+------ 
            0         |    8+1    |   8                  |  APPL
            9         |   14+1    |  23                  |  CDAT 
           24         |    7+1    |  31                  |  FREQ   
           32         |    3+1    |  35                  |  HOPO      
           36         |   14+1    |  50                  |  LSTU      
           51         |    4+1    |  55                  |  TIMF      
           56         |    4+1    |  60                  |  TIMT      
           61         |   10+1    |  71                  |  URNO      
           72         |   10+1    |  82                  |  URUE      
           83         |   32+1    | 115                  |  USEC      
          116         |   32+1    | 148                  |  USEU      
          149         |   14+1    | 163                  |  VAFR      
          164         |   14+1    | 178                  |  VATO      
          179         |   10+1    | 189                  |  UVAL      
          190         |    6+1    |  35                  |  TABN             
*/



/*****************************************************************************/
/* Function prototypes	                                                     */
/*****************************************************************************/
/* static int      Init_regchk() ;                                               */
/* static int      Reset(void) ;                  / * Reset program              */
/* static void     Terminate(void) ;              / * Terminate program          */
/* static void     HandleSignal(int) ;            / * Handles signals            */
/* static void     HandleErr(int) ;               / * Handles general errors     */
/* static void     HandleQueErr(int) ;            / * Handles queuing errors     */
/* static int      HandleData(void) ;             / * Handles event data         */
/* static void     HandleQueues(void) ;           / * Waiting for Sts.-switch    */

static int      FLdDefAndTest (char *ipTabName, 
                        char *pcpSqlBuffer) ;  /* compl. & test tFIELD_DEF[] */
static int      InitializeSQLVarBuffer ( char *pcSQLVarBuffer ) ;  
static int      testCaseRegChk ( void ) ; 
static int      ChkDateTimeRange (char *pcpFrom, char *pcpTo, char *pcpAct) ;
static int      SyntaxDateChkStamp (char *pcpAct) ; 
static int      CmpStrgs (char *cmpPar1, char *cmpPar2, int ipCmpLen) ;
static int		IsDateInRange (char *pcpFrom, char *pcpTo, char *pcpAct) ;
static int		IsTimeInRange (char *pcpTimf, char *pcpTimt, char *pcpAct );

/*--tested 18.11.99 GHe ------------------------------------------*/
/* ADDS ADDITIONAL INFORMATION TO THE tFIELD_DEF asgFldScpt[]     */ 
/* AND FILLS THE FOLLOWING STRINGS WITH INFORMATION :             */ 
/* -> cgSqlDecVarFlds, " INTO values(.. :HOPO,FERD, ... ) "       */
/* -> cgSqlFieldBuffer, " HOPO;AAAX;..... "                       */
/* -> cgSqlDKFieldBuffer, " HOPO,AAAX,..... "                     */
/*----------------------------------------------------------------*/
static int  FLdDefAndTest (char *ipTabName, char *pcpSqlBuffer) 
{  
 int  ilRC, ilArrNdx, ilActPos, ilI ;
 int  iTestOutput ;   

 iTestOutput = 0 ;      /* 1 -> testoutput is on */ 
 ilRC = RC_SUCCESS ;

 /* test the FieldNo for correct increment { prg. define errs } */ 
 ilArrNdx = 0 ;
 ilActPos = 0 ;  /* start position into the DB buffer */   
 strcpy ( cgSqlDecVarFlds, " INTO values(" ) ; /* .. :HOPO,FERD, ... */
 strcpy ( cgSqlFieldBuffer, " " ) ;            /* HOPO;AAAX;.....    */
 strcpy ( cgSqlDKFieldBuffer, "" ) ;           /* HOPO,AAAX,.....    */

 while (asgFldScpt[ilArrNdx].FieldName[0] != '\0')
 {
  /* set tabname */ 
  strcpy (asgFldScpt[ilArrNdx].TabName, ipTabName) ;    

  /* set and compute next position */
  asgFldScpt[ilArrNdx].StartPos = ilActPos ; 
  ilActPos += asgFldScpt[ilArrNdx].DefFieldLength 
              + asgFldScpt[ilArrNdx].Terminated ;

  /* fill rec. string with blanks and terminated */
  for (ilI=0; ilI < asgFldScpt[ilArrNdx].DefFieldLength; ilI++)
  { 
   asgFldScpt[ilArrNdx].Str[ilI] =  ' ' ;                     
  }
  asgFldScpt[ilArrNdx].Str[asgFldScpt[ilArrNdx].DefFieldLength] = '\0' ;   

  /* set tabname */ 
  /* create SQL and other field strings */    
  strcat (cgSqlDecVarFlds, ":") ;
  strcat (cgSqlDecVarFlds, asgFldScpt[ilArrNdx].FieldName) ;
  strcat (cgSqlFieldBuffer, asgFldScpt[ilArrNdx].FieldName) ;    
  strcat (cgSqlDKFieldBuffer, asgFldScpt[ilArrNdx].FieldName) ;    

  if (asgFldScpt[ilArrNdx+1].FieldName[0] != '\0')  /* exit next arr inp. ?*/
  {
   strcat ( cgSqlDecVarFlds,  "," ) ;    /* divide for next */ 
   strcat ( cgSqlFieldBuffer, ";" ) ;    /* -.- */  
   strcat ( cgSqlDKFieldBuffer, "," ) ;  /* -.- */  
  }

  /* create test output */ 
  if (iTestOutput == 1)
  { 
   dbg(DEBUG, "FLdDefAndTest : Fieldname is = %s ", 
                               asgFldScpt[ilArrNdx].FieldName ) ;

   dbg(DEBUG, "FLdDefAndTest : FieldLength is = %d ", asgFldScpt[ilArrNdx].DefFieldLength ) ;
   dbg(DEBUG, "FLdDefAndTest : FieldIndexNo is = %d ", asgFldScpt[ilArrNdx].FieldIndexNo ) ;
   dbg(DEBUG, "FLdDefAndTest : Field Str. is = <%s> ", asgFldScpt[ilArrNdx].Str ) ;
   dbg(DEBUG, "FLdDefAndTest : Field Start pos. is = %d ", asgFldScpt[ilArrNdx].StartPos ) ; 
   dbg(DEBUG, "FLdDefAndTest : ------------------------------------------------") ;
  }
  ilArrNdx++ ;      /* next arr field position */  
 } /* while */ 
 strcat (cgSqlDecVarFlds, ")") ;     
 if (iTestOutput == 1)
 { 
  dbg(DEBUG, "FLdDefAndTest : ================================================") ;
  dbg(DEBUG, "FLdDefAndTest : cgSqlDecVarFlds is = %s", cgSqlDecVarFlds ) ;
  dbg(DEBUG, "FLdDefAndTest : cgSqlFieldBuffer is = %s", cgSqlFieldBuffer ) ;
  dbg(DEBUG, "FLdDefAndTest : cgSqlDKFieldBuffer is = %s", cgSqlDKFieldBuffer ) ;
  dbg(DEBUG, "FLdDefAndTest : ------------------------------------------------") ;
 }

 return ilRC ; 

} /* end of function FLdDefAndTest () */ 







/*--tested 18.11.99 GHe -----------------------------------*/
/* sets the terminate chars into the SQL DECLARE Buffer    */ 
static int  InitializeSQLVarBuffer (char *pcSQLVarBuffer) 
{                      
 int    ilRC, ilArrNdx, ilI ; 
 int    iTestOutput ;   

 iTestOutput = 0 ;                 /* 1 -> testoutput is on */ 
 ilRC = RC_SUCCESS ;
 ilArrNdx = 0 ;   
 while (asgFldScpt[ilArrNdx].FieldName[0] != '\0')
 {
  for (ilI = asgFldScpt[ilArrNdx].StartPos; 
             ilI < (asgFldScpt[ilArrNdx].StartPos 
                 + asgFldScpt[ilArrNdx].DefFieldLength); ilI++)
  { 
   pcSQLVarBuffer[ilI] =  ' ' ;
   if (iTestOutput == 1)
   { 
    dbg(DEBUG, "InitializeSQLVarBuffer : Sets Term. FOR FLD=%s write Blank on pos. %d",
                asgFldScpt[ilArrNdx].FieldName, ilI) ;  
   }
  }

  /* set terminator for string, in the declare buffer */ 
  pcSQLVarBuffer[asgFldScpt[ilArrNdx].StartPos
      + asgFldScpt[ilArrNdx].DefFieldLength] = '\0' ;      

  if (iTestOutput == 1)
  { 
   dbg(DEBUG, "================================================") ;
   dbg(DEBUG, "Sets Term. FOR FLD=%s into Declare Buffer on pos. %d", 
                   asgFldScpt[ilArrNdx].FieldName, 
                   asgFldScpt[ilArrNdx].StartPos
                 + asgFldScpt[ilArrNdx].DefFieldLength) ;
  }

  ilArrNdx++ ;      /* next arr field position */  
 } /* while */ 

 return ilRC ; 

} /* end of function InitializeSQLVarBuffer () */  








/*--tested 18.11.99 GHe ------------------------------------------*/
/* LOADS SQL DECL. BUF. STRING TO THE tFIELD_DEF asgFldScpt[]     */ 
/*----------------------------------------------------------------*/
static int  LoadActDeclareBufferRecord(char *pcpSqlBuffer) 
{
 int    ilRC, ilArrNdx ;
 int  iTestOutput ;

 iTestOutput = 0 ;      /* 1 -> testoutput is on */ 
 ilRC = RC_SUCCESS ;
 ilArrNdx = 0 ;
 while (asgFldScpt[ilArrNdx].FieldName[0] != '\0') 
 {
  /* copy (field ) to str */  
  strncpy (asgFldScpt[ilArrNdx].Str, 
           &pcpSqlBuffer[asgFldScpt[ilArrNdx].StartPos], 
           asgFldScpt[ilArrNdx].DefFieldLength )  ;

  if (iTestOutput == 1)
  { 
   dbg(DEBUG, "LoadActDeclareBufferRecord : LOAD STR.FLD(%s) with := >%s< ",
               asgFldScpt[ilArrNdx].FieldName, asgFldScpt[ilArrNdx].Str ) ; 
  }

  ilArrNdx++ ;            /* next arr field position */
  
 } /* while */   

 return ilRC ; 

} /* end of function LoadActDeclareBufferRecord () */ 







/*--tested 18.11.99 GHe --------------------------------------------*/
/* Test act. Date (Par.3) for between from (Par.1) and to (Par.2)   */
/* Time and Date is given as a string, like "yyyymmddhhmmss"        */
/* return is 0 for success [ date/time are in the timerange ]       */ 
/* else 1 (not in day range [from]) or 2 (not in time range [from]) */  
/*      3 (not in day range [to]) or 4 (not in time range [to])     */  
/* ATTENTION: this function is much faster as the same function     */
/* ==========  _ChkDateTimeRange ()                                 */ 
/*------------------------------------------------------------------*/
static int  ChkDateTimeRange (char *pcpFrom, char *pcpTo, char *pcpAct) 
{
 int ilCmpI ; 

 ilCmpI = CmpStrgs (pcpAct, pcpFrom, 8) ;     
 if  (ilCmpI == -1 ) return 1 ;   /* less actDate */     

 dbg(DEBUG, "ChkDateTimeRange : ActDate=%s >= FromDate=%s",
             pcpAct, pcpFrom ) ;                            
 if  (ilCmpI == 0 )               /* equal Act,Form Date */     
 {
  /* special case !! date are identical -> look for times */ 
  ilCmpI = CmpStrgs (&pcpAct[8], &pcpFrom[8], 6) ; /* time check */      
  if  (ilCmpI == -1 ) return 2 ;   /* less actTime */      
  dbg(DEBUG, "ChkDateTimeRange : ActTime=%s >= FromTime=%s", &pcpAct[8],&pcpFrom[8]) ; 
 }

/* dbg(DEBUG, "ChkDateTimeRange : ActDate & Time is ok for FROM !!! " ) ; */

 /* now check the <To> date and time */ 
/* dbg(DEBUG, "ChkDateTimeRange : TEST ActDate=%s <= ToDate=%s", pcpAct, pcpTo) ; */

 ilCmpI = CmpStrgs (pcpAct, pcpTo, 8) ;     
 if  (ilCmpI == 1 ) return 3 ;   /* actDate is greater */     

 /* dbg(DEBUG, "ChkDateTimeRange : ActDate=%s <= ToDate=%s", pcpAct, pcpTo) ;*/

 if  (ilCmpI == 0) 
 {
 /* dbg(DEBUG, "ChkDateTimeRange : TEST ActDate=%s == ToDate=%s",pcpAct,pcpTo) ; */

  /* special case !! date are identical -> look for times */ 
  ilCmpI = CmpStrgs (&pcpAct[8], &pcpTo[8], 6) ; /* time check */      
  if  (ilCmpI == 1 ) return 4 ;   /* greater actTime */      

 /*  dbg(DEBUG, "ChkDateTimeRange : ActTime=%s >= ToTime=%s",      */
 /*            &pcpAct[8], &pcpTo[8]) ;                            */
 }

 /* dbg(DEBUG, "ChkDateTimeRange : Date and time are in the range !" ) ;   */ 

 return 0 ;   

}  /* ChkDateTimeRange ( )  */ 




/*--tested 18.11.99 GHe --------------------------------------------*/
/* Test act. Date (Par.3) for between from (Par.1) and to (Par.2)   */
/* Time and Date is given as a string, like "yyyymmddhhmmss"        */
/* return is 0 for success [ date/time are in the timerange ]       */ 
/* else 1 (not in day range [from]) or 2 (not in time range [from]) */  
/*      3 (not in day range [to]) or 4 (not in time range [to])     */  
/* ATTENTION :   USE SIMILAR FUNCTION ChkDateTimeRange (). THE      */  
/*               FUNCTION IS MUCH FASTER                            */  
/*------------------------------------------------------------------*/
static int  _ChkDateTimeRange (char *pcpFrom, char *pcpTo, char *pcpAct) 
{
 char  cpFromStr[36], cpToStr[36], cpActStr[36] ;  
 long  llcmpFromTimeValue, llcmpFromDateValue ; 
 long  llcmpToTimeValue, llcmpToDateValue ; 
 long  llGetActTimeValue, llGetActDateValue ; 


 strncpy (cpFromStr, pcpFrom, 14) ;              /* get from date */
 cpFromStr[8] = '\0' ; 
 llcmpFromDateValue = atol (cpFromStr) ;

 strncpy (cpActStr, pcpAct, 14) ;                /* get to date */ 
 cpActStr[8] = '\0' ; 
 llGetActDateValue = atol (cpActStr) ;   

 if  (llGetActDateValue < llcmpFromDateValue) return 1 ; 

/* dbg(TRACE, "_ChkDateTimeRange : ActDate=%ld >= FromDate=%ld",  */
/*     llGetActDateValue, llcmpFromDateValue) ;                   */

 if  (llGetActDateValue == llcmpFromDateValue)
 {
  /* special case !! date are identical -> look for times */ 
  llcmpFromTimeValue = atol (&pcpFrom[8]) ; 
  llGetActTimeValue  = atol (&pcpAct[8]) ; 
  if (llGetActTimeValue < llcmpFromTimeValue) return 2 ; 
/* dbg(TRACE, "_ChkDateTimeRange : ActTime=%ld >= FromTime=%ld",  */
/*    llGetActTimeValue, llcmpFromTimeValue) ;                    */
 }

/* dbg(TRACE, "_ChkDateTimeRange : ActDate & Time is ok for FROM !!! " ) ; */

 /* now check the <To> date and time */ 

 strcpy (cpToStr, pcpTo) ;               /* get to date */ 
 cpToStr[8] = '\0' ; 
 llcmpToDateValue = atol (cpToStr) ;   

 if  (llGetActDateValue > llcmpToDateValue) return 3 ;
 
/* dbg(TRACE, "_ChkDateTimeRange : ActDate=%ld <= ToDate=%ld",   */
/*    llGetActDateValue, llcmpToDateValue) ;                     */

 if  (llGetActDateValue == llcmpToDateValue)
 {
  /* special case !! date are identical -> look for times */ 
  llcmpToTimeValue = atol (&pcpTo[8]) ; 
  llGetActTimeValue  = atol (&pcpAct[8]) ;  /* not sure, for done prev. */ 
  if (llGetActTimeValue > llcmpToTimeValue) return 4 ; 
/*  dbg(TRACE, "_ChkDateTimeRange : ActTime=%ld >= ToTime=%ld",  */
/*     llGetActTimeValue, llcmpToTimeValue) ;                    */
 }

/* dbg(TRACE, "_ChkDateTimeRange : Date and time are in the range !" ) ;  */

 return 0 ;   

}  /* ChkDateTimeRange ( )  */ 





/*--tested 18.11.99 GHe --------------------------------------------*/
/* Test act. Date (Par.1) for correct and valid syntax              */ 
/* If so return is RC_SUCCESS, otherwise FALSE                      */ 
/* the Datestring has the form "JJJJMMDD......."                    */ 
/*------------------------------------------------------------------*/
static int SyntaxDateChkStamp (char *pcpAct)
{ 
  char    clActStr[36] ;    
  int     ilRC ;
  int     ilyear, ilmonth, ilday ;   

  ilRC = RC_FAILURE ; 
  strncpy (clActStr, pcpAct, 8 ) ; /* only date */ 
 
  ilday = atoi (&clActStr[6]) ;
  clActStr[6] = '\0' ;    
  ilmonth = atoi (&clActStr[4])  ; 
  clActStr[4] = '\0' ; 

  if ((ilmonth > 12) || (ilmonth < 1)) 
  {
/* dbg(DEBUG, "SyntaxDateChkStamp : Input of month NOK = <%d>", ilmonth ) ;  */
   return (ilRC) ;
  } 
  if ((ilday < 1) || (ilday > 31)) 
  {
/* dbg(DEBUG, "SyntaxDateChkStamp : Input of day NOK = <%d>, Month = <%d>", ilday, ilmonth ) ; */
   return (ilRC) ;
  } 
  
  switch (ilmonth) 
  { 
   case 2 : ilyear = (atoi(clActStr) - 1900) % 4 ;
            if ((ilyear != 0) && (ilday > 28)) 
            {
  /*         dbg(DEBUG, "SyntaxDateChkStamp : Input of day NOK = <%d>, Month <02>", ilday ) ;  */
             return (ilRC) ;
            }
            if ((ilyear == 0) && (ilday > 29)) 
            {
  /*         dbg(DEBUG, "SyntaxDateChkStamp : Input of day NOK = <%d>, Month <02>", ilday ) ;  */
             return (ilRC) ;
            }
           break ;

   case  4 : /* months with 30 days */ 
   case  6 :
   case  9 :
   case 11 : if (ilday > 30) 
             {
  /*          dbg(DEBUG, "SyntaxDateChkStamp : Input of day NOK = <%d>", ilday ) ;   */  
              return (ilRC) ;
             } 
           break ; 

   default : /* months with 31 days ! check is done above */

           break ;    
  }  /* switch */ 

  return ( RC_SUCCESS ) ; 

} /* end of function SyntaxDateChkStamp () */ 



/*--tested 18.11.99 GHe --------------------------------------------*/
/* Compare 2 strings with the SAME length !!                        */ 
/* returns are :   -1 parString_1 is less then parString_2          */ 
/* -------------    0 both strings are equal                        */ 
/*                  1 parString_1 is bigger as parString_2          */ 
/*------------------------------------------------------------------*/ 
static int CmpStrgs (char *cmpPar1, char *cmpPar2, int ipCmpLen)    
{     
  int     ilI ;
  
  ilI = 0 ; 
  while (ilI < ipCmpLen) 
  {
   if ( (int) cmpPar1[ilI] != (int) cmpPar2[ilI] )
   {
    if ( (int) cmpPar1[ilI] < (int) cmpPar2[ilI] ) return -1 ; 
    return 1 ; 
   }    
   ilI++ ; 
  }
  return 0 ; 
 
} /* end of function CmpStrgs () */ 






/*--tested 18.11.99 GHe --------------------------------------------*/
/* Test act. Date (Par.1) for a day frequenz. The meaning of the    */ 
/* day frequenz is defined in a 7 char string with existing chars   */ 
/* like '0' and '1'. '1' means : access for this day is ok (eq. '0' */ 
/* -> nok). The frequenz string (Par.2) starts with the first char  */
/* sequenz = monday, tuesday,...                                    */ 
/* RC_SUCCESS as return means, that the day is accept !             */ 
/*------------------------------------------------------------------*/
int ChkDayFrequenz (char *pcpAct, char *pcpFrequenz)
{ 
  char    clActStr[36] ;    
  int     ilCmpChr, ilRC ;
  int     ilyear, ilmonth, ilday ;   
  long    Julians19DayIs ;  
  int     alMonthDayArr [] = { 0,31,59,90,120,151,181,212,243,273,304,334 } ;  

  char    helpStr [32] ;

	if ( strcmp (pcpFrequenz, "1111111" ) == 0 )
		return RC_SUCCESS;

  ilRC = RC_FAILURE ; 

  /* -----  no add. syn. check for date -------------------------- 
  if ( SyntaxDateChkStamp (pcpAct) != RC_SUCCESS ) return (ilRC) ;
  -------------------------------------------------------------- */ 

  strncpy (clActStr, pcpAct, 8 ) ; /* only date */ 
  clActStr[8] = '\0' ;    

  ilday = atoi (&clActStr[6]) ;
  clActStr[6] = '\0' ;    
  ilmonth = atoi (&clActStr[4])  ; 
  clActStr[4] = '\0' ; 
  ilyear = atoi(clActStr) - 1900 ;
  if ((ilmonth > 12) || (ilmonth < 1)) 
  {
   dbg(DEBUG, "ChkDayFrequenz : Input of month NOK = <%d>", ilmonth ) ;  
   return (ilRC) ;
  } 
  if ((ilday < 1) || (ilday > 31)) 
  {
   dbg(DEBUG, "ChkDayFrequenz : Input of day NOK = <%d>", ilday ) ;  
   return (ilRC) ;
  } 

  Julians19DayIs = (365 * ilyear ) + (ilyear >> 2) ; 
  Julians19DayIs += alMonthDayArr[ilmonth-1] + ilday ; 
                
  if ((ilyear % 4) == 0) 
  {
   if ( ilmonth < 3 ) Julians19DayIs-- ;
  }
  ilCmpChr = (Julians19DayIs % 7) - 1 ;
  
  if (ilCmpChr < 0) ilCmpChr = 6 ;       /* shift sunday */
/*
  if (debug_level>TRACE)
  {
   switch (ilCmpChr)
   {
    case 0:     strcpy  (helpStr, "MONDAY") ;     break ;  
    case 1:     strcpy  (helpStr, "TUESDAY") ;    break ;  
    case 2:     strcpy  (helpStr, "WEDNESDAY") ;  break ;  
    case 3:     strcpy  (helpStr, "THURSDAY") ;   break ;  
    case 4:     strcpy  (helpStr, "FRIDAY") ;     break ;  
    case 5:     strcpy  (helpStr, "SATURDAY") ;   break ;  
    case 6:     strcpy  (helpStr, "SUNDAY") ;     break ;  
    default:    sprintf (helpStr, "ERROR ndx=%d", ilCmpChr) ;   break ;  
   } 

	dbg(DEBUG, "ChkDayFrequenz : Day for %2d.%2d.%4d is = %s",  
             ilday,ilmonth,ilyear+1900,helpStr) ;            
	dbg(TRACE, "ChkDayFrequenz : Frequenz is = <%s>", pcpFrequenz ) ;    
  }  */  

  if (pcpFrequenz[ilCmpChr] == '1')
  {   
	/* dbg(DEBUG, "ChkDayFrequenz : *** DAY IS OK !!! ***" ) ;      */
	ilRC = RC_SUCCESS ;  
  }
       
  return ( ilRC ) ; 
 
} /* end of function ChkDayFrequenz () */ 





/* ------------------------------------------------------------------------- */
static int  GetActFieldString(char *SearchFieldName, char *FieldStr) 
{
 int    ilRC, ilArrNdx, ilStrFound ;   

 ilRC = RC_SUCCESS ;
 ilArrNdx   = 0 ;  
 ilStrFound = 0 ;  
 while ((asgFldScpt[ilArrNdx].FieldName[0] != '\0') && (ilStrFound == 0))  
 {
  if (strcmp (asgFldScpt[ilArrNdx].FieldName, SearchFieldName) == 0) 
  {
   strcpy (FieldStr, asgFldScpt[ilArrNdx].Str) ;
   ilStrFound = 1 ; 
  } 
  if (asgFldScpt[ilArrNdx].FieldName[0] == '\0') 
  {
   /* for searched fieldname, string not found !"!!!! */ 
/*   dbg(DEBUG, "GetActFieldString : Fieldname %s ERR !STRING NOT FOUND!",  */
/*        SearchFieldName ) ;                                               */

   return (RC_FAILURE) ; 
  } 
  ilArrNdx++ ;            /* next arr field position */    
 } /* while */   

 return ilRC ; 

} /* end of function LoadActDeclareBufferRecord () */ 






/****************************************************************************/
int  InitValidityModule (int ipMode, char *pcpTable ) 
{
  int     ilDbMode  = TRACE ;                    /* -.- */
  int     ilRC ; 
  int     lArrIndexes = ARR_DEFAULT_INDIZES + 0 ;
  long    llRow = 0; 
	int ilCol,ilPos;

  ilRC = RC_SUCCESS ; 

  /*------------------------------------*/
  /* set all important global variables */
  /*------------------------------------*/

  igChkValWorkMode = ipMode ;                      /* W O R K M O D E */ 

  /* define tablenames, etc... */
  if (cpgTable[0] == '\0') 
  {
   strcpy (cpgTable, DEFAULT_CGP_TABLE) ;                   
  }
  strcpy (cpgTable, pcpTable) ;
  slgArr = 0 ;                     /* array functions are not useable */  
  hlgArrHdl = -1 ; 
  igArrLoaded = ARR_LOADING_NOK ; 
  igSQLLoaded = SQL_LOADING_NOK ; 

  /* define sql buffer, field defs. .. */
  ilRC = FLdDefAndTest (cpgTable, cgSqlFieldBuffer ) ;
  ilRC = InitializeSQLVarBuffer (cgSqlDeclareBuffer ) ; 


  if (igChkValWorkMode == CHKVAL_DB)              /*-------------------*/
  {                                               /* SQL INIT WORKMODE */
                                                  /*-------------------*/   
/*   dbg(ilDbMode, "InitValidityModule : WORKMODE is for DB/SQL") ;   */ 

   /* where have i done a logon ??? */ 

   /* open sql cursor */ 
   sgSQL_cursor = 0 ;
   sgSQL_fkt = START | REL_CURSOR ;
   sprintf (cpgSQLStatement, "SELECT %s FROM %s ",cgSqlDKFieldBuffer,cpgTable);
   ilRC = sql_if(sgSQL_fkt,&sgSQL_cursor,cpgSQLStatement,cgSqlDeclareBuffer);
   if (ilRC != DB_SUCCESS )    
   {                                                 /* SQL ERROR */     
 /*   dbg(ilDbMode, "InitValidityModule : SQL INIT ERR: FKT=%d; CALL=%s RC=%d",  */
 /*                  sgSQL_fkt, cpgSQLStatement, ilRC) ;                         */
    igSQLLoaded = SQL_LOADING_NOK ; 
    return (ilRC) ;     /* failed */ 
   }   

   /* define sql statement */ 
   igSQLLoaded = SQL_LOADING_OK ; 
   commit_work () ; 
  }
  else  /* default always ARRAY  */             /*---------------------*/
  {                                             /* ARRAY INIT WORKMODE */
                                                /*---------------------*/
   dbg(TRACE, "InitValidityModule : WORKMODE is for ARRAY") ;      
   /*  hag 20030903: CEDAArrayInitialize has to be done in Main program 
   INITIALIZE POOL ARRAY 
   dbg(TRACE, "InitValidityModule : Recnos for ARR = <%d>,<%d>", ARR_DEFAULT_RECNOS, lArrIndexes) ;      
   iRC = CEDAArrayInitialize( ARR_DEFAULT_RECNOS, lArrIndexes ) ;
   if (iRC != RC_SUCCESS) 
   {
    dbg(ilDbMode, "InitValidityModule : ERR CEDAArrayInitialize rc = %d", iRC) ; 
    return (RC_FAIL) ; 
   }
   dbg(TRACE,"InitValidityModule : DONE !! CEDAArrayInitialize Handle") ;   */ 

   igArrLoaded = ARR_LOADING_OK ; 
   sprintf (cgArrName, "%s", cpgTable) ;
   strcpy  (cgAddFlds, "" ) ;   
  
   /*strcpy (cgSelStr, "") ;  */  /* strcpy (cgSelStr, "WHERE .......") ; */

   ilRC = CEDAArrayCreate (&hlgArrHdl, cgArrName, cpgTable,
                           cgSelStr, cgAddFlds, lgAddFldLens,
                           cgSqlDKFieldBuffer, lgFldLens, lgFldOff) ;

   if (ilRC != RC_SUCCESS) 
   {
    dbg(DEBUG,"InitValidityModule : ERR CEDAArrayCreate Handle rc = %d", ilRC) ;
    return (RC_FAIL) ; 
   }
	/*  calculate field indices */
	FindItemInList(cgSqlDKFieldBuffer,"VAFR",',',&igVafrIdx,&ilCol,&ilPos);
	FindItemInList(cgSqlDKFieldBuffer,"VATO",',',&igVatoIdx,&ilCol,&ilPos);
	FindItemInList(cgSqlDKFieldBuffer,"FREQ",',',&igFreqIdx,&ilCol,&ilPos);
	FindItemInList(cgSqlDKFieldBuffer,"TIMF",',',&igTimfIdx,&ilCol,&ilPos);
	FindItemInList(cgSqlDKFieldBuffer,"TIMT",',',&igTimtIdx,&ilCol,&ilPos);

 	strcpy(cgIdxName,"UVALIDX1");

	ilRC = CEDAArrayCreateSimpleIndexDown(&hlgArrHdl,cgArrName,&hlgIdxHdl,
			cgIdxName,"UVAL");

	if (ilRC != RC_SUCCESS) 
	{
		dbg(DEBUG,
			"InitValidityModule : ERR CEDAArrayCreateSimpleIndexDown rc = %d",
			ilRC) ;
		return (RC_FAIL) ; 
	}
 
/*   dbg(TRACE,"InitValidityModule : DONE !! CEDAArrayCreate Handle") ;   */ 

   /* fill array */
   ilRC = CEDAArrayFill (&hlgArrHdl, cgArrName, cgAddFlds) ;
   if (ilRC != RC_SUCCESS) 
   {
    dbg(TRACE,"InitValidityModule : ERR CEDAArrayFill rc = %d", ilRC) ;
    return (RC_FAIL) ; 
   }
	CEDAArrayGetRowCount(&hlgArrHdl,cgArrName,&llRow);

    dbg(DEBUG, "InitValidityModule: VALTAB-Selection <%s> -> %d records loaded", 
		cgSelStr, llRow ) ;

/*   dbg(TRACE,"InitValidityModule : CEDAArrayFill SUCCESSFUL" ) ;    */ 
   slgArr = 1 ; 
  }

  return (ilRC) ; 

} /* end of InitValidityModule () */




/****************************************************************************/
int    UpdateValidityModule (EVENT *prpEvent )
{
  int     ilDbMode  = DEBUG ;                     /* -.- */
  int     ilRC ;

  ilRC = RC_SUCCESS ;

  if (igChkValWorkMode == CHKVAL_ARRAY)     /*----------------*/
  {                                         /* ARRAY WORKMODE */
                                            /*----------------*/
/*   dbg(TRACE, "UpdateValidityModule : WORKMODE is for ARRAY") ;  */ 
   if ( slgArr == NOT_IN_USE )              /* undef. arr. handle */ 
   {
/*  dbg(ilDbMode, "UpdateValidityModule : CEDAArrayFill DB not init") ;   */
    return (RC_FAIL) ; 
   }
   ilRC = CEDAArrayEventUpdate ( prpEvent ) ;
   if (ilRC != RC_SUCCESS)
   {
/*  dbg(ilDbMode, "UpdateValidityModule : ERROR CEDAArrayFill rc = %d") ; */
    return (RC_FAIL) ;
   }
  }

  if (igChkValWorkMode == CHKVAL_DB)        /*--------------*/
  {                                         /* SQL WORKMODE */
                                            /*--------------*/
   /* DO NOTHING */
/*   dbg(ilDbMode, "UpdateValidityModule : CEDAArrayFill WORKMODE is for SQL") ;  */
   return ( RC_SUCCESS ) ;
  }

  return (ilRC) ;
 
} /* end of UpdateValidityModule () */







/****************************************************************************/
int    CheckValidity (char *pcpAppl, char *pcpUrue, char *pcpDate,
                      int *piplsValid)
{
  int     ilDbMode  = DEBUG ;                /* -.- */
  int     ilRC, ilDbRC, ilFound ;
  char    clFreq[10] ; 
  char    clDateFrom[16], clDateTo[16], clUrue[12], clTimt[16], clTimf[16] ;  
  long    llRow ; 
  int     ilCmpChr, iTestCnt ; 
	char	*pclResult = 0;

  ilRC = RC_SUCCESS ; 
  *piplsValid = 0 ;        /* not found eg. FALSE */ 
 

  if (igChkValWorkMode == CHKVAL_DB)              /*-----------------*/
  {                                               /* DB SQL WORKMODE */
                                                  /*-----------------*/
   dbg(ilDbMode, "CheckValidity : DB/SQL Workmode") ;
   /* is init done ? */ 
   if (igSQLLoaded != SQL_LOADING_OK) 
   {
    dbg(ilDbMode, "CheckValidity : SQL INIT ERR: NO INITIALIZE INFO") ;     
    return (RC_FAIL) ;
   }

   /* search all records */ 
   sgSQL_fkt = START | REL_CURSOR ; 
   sprintf (cpgSQLStatement, "SELECT %s from %s ",cgSqlFieldBuffer,cpgTable) ; 
   ilRC =sql_if(sgSQL_fkt,&sgSQL_cursor,cpgSQLStatement,cgSqlDeclareBuffer) ;
   if (ilRC != DB_SUCCESS ) 
   {                                                /* SQL ERROR */ 
    dbg(ilDbMode, "CheckValidity : SQL INIT ERR: FKT=%d; CALL=%s RC=%d", 
                  sgSQL_fkt, cpgSQLStatement, ilRC) ;
    return (RC_FAILURE) ;                              /* failed */ 
   }

   ilDbRC = ilRC ;
   sgSQL_fkt = START ;
   sprintf (cpgSQLStatement, "SELECT DISTINCT %s from %s",
                             cgSqlFieldBuffer, cpgTable) ;

   while (ilDbRC == DB_SUCCESS)  
   {
    ilRC = LoadActDeclareBufferRecord(cgSqlDeclareBuffer) ;

    /* ---------------------------------------------------------*/
    GetActFieldString("FREQ", clFreq ) ;
    GetActFieldString("UVAL", clUrue ) ;/*  chg. to uval !!! */
    GetActFieldString("VAFR", clDateFrom ) ;
    GetActFieldString("VATO", clDateTo ) ; 

    dbg(DEBUG, "CheckValidity : SQL: READ FREQ = %s ", clFreq) ;  
    dbg(DEBUG, "CheckValidity : SQL: READ UVAL = %s ", clUrue) ; /*  chg. to UVAL !!!*/
    dbg(DEBUG, "CheckValidity : SQL: READ VAFR = %s ", clDateFrom) ;
    dbg(DEBUG, "CheckValidity : SQL: READ VATO = %s ", clDateTo ) ;
     
   /* ------------------------------------------------------------*/  

     GetActFieldString("UVAL", clUrue) ;   /* chg. to UVAL !!! */ 
     if (strcmp (pcpUrue, clUrue) == 0)      
     { 
      /* test date & time */
      GetActFieldString("VAFR", clDateFrom ) ;
      GetActFieldString("VATO", clDateTo ) ;

      if ((clDateTo[0] == ' ') || (clDateTo[0] == '\0') )  /* work with endless date */
      {
	   ilRC = IsDateInRange (clDateFrom, "99999999999999", pcpDate) ;
      }
      else     /* work with given <to> date */ 
      {
	   ilRC = IsDateInRange (clDateFrom, clDateTo, pcpDate) ;
      }

      if (ilRC == 0) 
      {
       /* date and time are correct */
       GetActFieldString("FREQ", clFreq ) ;
       ilRC = ChkDayFrequenz ( pcpDate, clFreq ) ; 
	  
       if (ilRC == 0) 
       {
        /* Frequency is correct */
        GetActFieldString("TIMF", clTimf ) ;
        GetActFieldString("TIMT", clTimt ) ;
        ilRC = IsTimeInRange (clTimf, clTimt, pcpDate) ;
	   }
       if (ilRC == RC_SUCCESS) 
       {
        *piplsValid = 1 ;
        commit_work() ;   
        return (RC_SUCCESS) ;
       } ;  /* if frequenz of day */ 
      } ;  /* if date&time */ 
     } ;  /* if pcpUrue */ 


    /* read next record */ 
    ilDbRC = sql_if(sgSQL_fkt,&sgSQL_cursor,cpgSQLStatement,
                    cgSqlDeclareBuffer) ;  

   } /* while next record */ 

   commit_work() ; 
   /* falling down, no record found !!! */ 
   dbg(ilDbMode, "CheckValidity : SQL: NO VALIDITY RECORD FOUND") ;     
   return (RC_FAIL) ;                              /* failed */

  }
  else                                   /*----------------*/
  {                                      /* ARRAY WORKMODE */
                                         /*----------------*/
	llRow = ARR_FIRST ; 
	ilFound = 0 ; 
	iTestCnt = 0 ;
	
	while ( ilFound == 0 )     /* CASE -> NO RECORDS !!! */ 
	{ 
		iTestCnt++ ; 
		dbg(ilDbMode, "CheckValidity : Looking for RUEURNO <%s> %d. time", pcpUrue, iTestCnt ) ; 

		clDateFrom[0] = '\0';
		clDateTo[0] = '\0';
		clFreq[0] = '\0';
		clTimf[0] = '\0';
		clTimt[0] = '\0';
		
		ilRC = CEDAArrayFindRowPointer(&hlgArrHdl,cgArrName,&hlgIdxHdl,cgIdxName,
										pcpUrue,&llRow,(void**)&pclResult);
		if (ilRC != RC_SUCCESS) 
		{
			dbg(ilDbMode, "CheckValidity(FindRowPointer) with rc = %d",ilRC) ;  
			return (RC_FAILURE) ;                        
		}
		
		if ( ( igVafrIdx > 0 ) && ( igVatoIdx > 0 ) )
		{
			strcpy ( clDateFrom,pclResult + lgFldOff[igVafrIdx-1] );
			strcpy ( clDateTo,	pclResult + lgFldOff[igVatoIdx-1] );
	
			/* test date & time */    
			if ((clDateTo[0] == ' ') || (clDateTo[0] == '\0') )  /* work with endless date */
			{
				ilRC = IsDateInRange (clDateFrom, "99999999999999", pcpDate) ;
			}
			else     /* work with given <to> date */ 
			{
				ilRC = IsDateInRange (clDateFrom, clDateTo, pcpDate) ;
			}
			if ((pcpDate[0] == ' ') || (pcpDate[0] == '\0')) 
			{
				ilRC = RC_FAIL ;             
			}       
			dbg(DEBUG, "CheckValidity : VAFR <%s> VATO <%s> RC <%d>", 
					clDateFrom, clDateTo, ilRC ) ; 
		}
		else
		{
			dbg(TRACE, "CheckValidity : FieldIndices not valid igVafrIdx <%d> igVatoIdx <%d>", 
				igVafrIdx, igVatoIdx );
			ilRC = RC_FAIL ;             
		}
		
		if (ilRC == 0) 
		{			 /* date and time are correct */
			if ( igFreqIdx > 0 )
			{
				strcpy ( clFreq, pclResult + lgFldOff[igFreqIdx-1] );
				ilRC = ChkDayFrequenz ( pcpDate, clFreq ) ; 
				dbg(DEBUG, "CheckValidity : Frequence <%s> RC <%d>", 
					clFreq, ilRC ) ;
			}
			else
			{
				dbg(TRACE, "CheckValidity : FieldIndex not valid igFreqIdx <%d>", 
					igFreqIdx );
				ilRC = RC_FAIL ;             
			}
		}
		if (ilRC == 0) 
		{  /* Frequency is correct */
			if ( ( igTimfIdx > 0 ) && ( igTimtIdx > 0 ) )
			{
				strcpy ( clTimf, pclResult + lgFldOff[igTimfIdx-1] );
				strcpy ( clTimt, pclResult + lgFldOff[igTimtIdx-1] );
				ilRC = IsTimeInRange (clTimf, clTimt, pcpDate) ;
				if (ilRC == RC_SUCCESS) 
				{
					*piplsValid = 1 ;
					return (RC_SUCCESS) ;
				}   /* if frequenz of day */ 
			}
			else
			{
				dbg(TRACE, "CheckValidity : FieldIndices not valid igTimfIdx <%d> igTimtIdx <%d>", 
					igTimfIdx, igTimtIdx );
				ilRC = RC_FAIL ;             
			}
		}   /* if date&time to */  
		
		llRow = ARR_NEXT ; 
	} ; /* end while */

  } ; /* else work with  array */ 

  return (RC_FAILURE) ;                     /* failed */

} /* end of CheckValidity () */















/****************************************************************************/
int    CleanupValidityModule ( void ) 
{
  int     ilDbMode  = TRACE ;                     /* -.- */
  int     ilRC ;

  ilRC = RC_SUCCESS ;

  if (igChkValWorkMode == CHKVAL_DB)    /*-----------------*/
  {                                     /* DB SQL WORKMODE */
                                        /*-----------------*/
   dbg(ilDbMode, "CleanupValidityModule : DB/SQL Workmode") ;
   sprintf ( cpgSQLStatement, "close cursor" ) ;
   sgSQL_fkt = NOACTION ;   
   ilRC = sql_if (sgSQL_fkt,&sgSQL_cursor,cpgSQLStatement,cgSqlDeclareBuffer) ;
   if (ilRC != DB_SUCCESS )
   {                                                    /* SQL ERROR */ 
    dbg(ilDbMode, "CleanupValidityModule : SQL CLOSE CURSOR ERR: FKT=%d; CALL=%s RC=%d",
                   sgSQL_fkt, cpgSQLStatement, ilRC) ;
    return (ilRC) ;     /* failed */
   }
  }
  else                                  /*----------------*/
  {                                     /* ARRAY WORKMODE */
                                        /*----------------*/
/* dbg(ilDbMode, "CleanupValidityModule : ARRAY Workmode") ;   */ 
   ilRC = CEDAArrayDelete (&hlgArrHdl, cgArrName) ;
   if (ilRC != RC_SUCCESS)
   {
    dbg(ilDbMode, "CleanupValidityModule : ARRAY DELETE ERR: RC = %d", ilRC) ;
    return (RC_FAIL) ;     /* failed */ 
   }
   ilRC = CEDAArrayDestroy (&hlgArrHdl, cgArrName) ;
   if (ilRC != RC_SUCCESS)
   {
    dbg(ilDbMode, "CleanupValidityModule : ARRAY DESTROY ERR: RC = %d", ilRC) ;
    return (RC_FAIL) ;     /* failed */
   }
  }

  return (ilRC) ;

} /* end of CleanupValidityModule () */





static int IsDateInRange (char *pcpFrom, char *pcpTo, char *pcpAct) 
{

	if ( strcmp(pcpAct,pcpFrom) < 0 )
	{
		dbg(DEBUG, "IsDateInRange: TestDate <%s> not between <%s> and <%s> ", pcpAct, pcpFrom, pcpTo ) ;                            
		return 1;
	}
	if ( strcmp(pcpTo,pcpAct) < 0 )
	{
		dbg(DEBUG, "IsDateInRange: TestDate <%s> not between <%s> and <%s> ", pcpAct, pcpFrom, pcpTo ) ;                            
		return 2;
	}
	return 0 ;   

}  /* ChkDateTimeRange ( )  */ 


/*  check against VALTAB.TIMF and TIMT if filled */
static int IsTimeInRange (char *pcpTimf, char *pcpTimt, char *pcpAct ) 
{
	int ilTimf, ilTimt;
	int ilMinutesAct, ilHoursAct;
	char clActStd[3], clActMin[3];
	BOOL blOk;

	if ( !pcpTimf || !pcpTimt || !pcpAct || ( strlen (pcpAct)<12 ) )
	{
		dbg ( DEBUG, "IsTimeInRange: Invalid input parameter(s). Rule will not be applied" );
		return RC_INVALID;
	}
	if ( sscanf ( pcpTimf, "%d", &ilTimf ) < 1 )
	{
		/*dbg ( DEBUG, "IsTimeInRange: TIMF not filled. Rule can be applied" );*/
		return RC_SUCCESS;
	}
	if ( sscanf ( pcpTimt, "%d", &ilTimt ) < 1 )
	{
		/*dbg ( DEBUG, "IsTimeInRange: TIMT not filled. Rule can be applied" );*/
		return RC_SUCCESS;
	}
	/* TIMT and TIMF are filled with minutes */
	/* convert Time in pcpAct into minutes */
	clActStd[0] = pcpAct[8];
	clActStd[1] = pcpAct[9];
	clActMin[0] = pcpAct[10];
	clActMin[1] = pcpAct[11];
	clActStd[2] = clActMin[2] = '\0';
	if ( ( sscanf ( clActStd, "%d", &ilHoursAct ) < 1 ) ||
		 ( sscanf ( clActMin, "%d", &ilMinutesAct ) < 1 ) )
	{
		dbg ( DEBUG, "IsTimeInRange: No correct Time in testdate <%s>", pcpAct );
		return RC_INVALID;
	}
	ilMinutesAct += 60*ilHoursAct;
	if ( ilTimf <= ilTimt )
	{
		blOk = ( (ilTimf <= ilMinutesAct) && (ilMinutesAct<=ilTimt) );
	}		
	else
	{
		blOk = ( (ilTimf <= ilMinutesAct) || (ilMinutesAct<=ilTimt) );
	}	
	dbg ( DEBUG, "IsTimeInRange: TIMF <%s> TIMT <%s> TestDate <%s> in Min <%d> blOk <%d>", 
		  pcpTimf, pcpTimt, pcpAct, ilMinutesAct, blOk );
	return ( blOk ? RC_SUCCESS : RC_FAIL );
}
	

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


int    CheckValidity2 (char *pcpAppl, char *pcpUrue, char *pcpDate,
                      int *piplsValid)
{
  int     ilDbMode  = DEBUG ;                /* -.- */
  int     ilRC, ilDbRC, ilFound ;
  char    clHelpStr[1024] ; 
  char    clHopo[64], clAppl[64], clFreq[64] ; 
  char    clDateFrom[64], clDateTo[64], clUrue[64], clTimt[16], clTimf[16] ;  
  long    llRow ; 
  int     ilCmpChr, iTestCnt ; 
	char	*pclResult = 0;

  ilRC = RC_SUCCESS ; 
  *piplsValid = 0 ;        /* not found eg. FALSE */ 
 

  if (igChkValWorkMode == CHKVAL_DB)              /*-----------------*/
  {                                               /* DB SQL WORKMODE */
                                                  /*-----------------*/
   dbg(ilDbMode, "CheckValidity : DB/SQL Workmode") ;
   /* is init done ? */ 
   if (igSQLLoaded != SQL_LOADING_OK) 
   {
    dbg(ilDbMode, "CheckValidity : SQL INIT ERR: NO INITIALIZE INFO") ;     
    return (RC_FAIL) ;
   }

   /* search all records */ 
   sgSQL_fkt = START | REL_CURSOR ; 
   sprintf (cpgSQLStatement, "SELECT %s from %s ",cgSqlFieldBuffer,cpgTable) ; 
   ilRC =sql_if(sgSQL_fkt,&sgSQL_cursor,cpgSQLStatement,cgSqlDeclareBuffer) ;
   if (ilRC != DB_SUCCESS ) 
   {                                                /* SQL ERROR */ 
    dbg(ilDbMode, "CheckValidity : SQL INIT ERR: FKT=%d; CALL=%s RC=%d", 
                  sgSQL_fkt, cpgSQLStatement, ilRC) ;
    return (RC_FAILURE) ;                              /* failed */ 
   }

   ilDbRC = ilRC ;
   sgSQL_fkt = START ;
   sprintf (cpgSQLStatement, "SELECT DISTINCT %s from %s",
                             cgSqlFieldBuffer, cpgTable) ;

   while (ilDbRC == DB_SUCCESS)  
   {
    ilRC = LoadActDeclareBufferRecord(cgSqlDeclareBuffer) ;

    /* ---------------------------------------------------------*/
    GetActFieldString("APPL", clAppl ) ; 
    GetActFieldString("FREQ", clFreq ) ;
    GetActFieldString("HOPO", clHopo ) ;
    GetActFieldString("UVAL", clUrue ) ;/*  chg. to uval !!! */
    GetActFieldString("VAFR", clDateFrom ) ;
    GetActFieldString("VATO", clDateTo ) ; 

    dbg(DEBUG, "CheckValidity : SQL: READ APPL = %s ", clAppl) ;  
    dbg(DEBUG, "CheckValidity : SQL: READ FREQ = %s ", clFreq) ;  
    dbg(DEBUG, "CheckValidity : SQL: READ HOPO = %s ", clHopo) ;  
    dbg(DEBUG, "CheckValidity : SQL: READ UVAL = %s ", clUrue) ; /*  chg. to UVAL !!!*/
    dbg(DEBUG, "CheckValidity : SQL: READ VAFR = %s ", clDateFrom) ;
    dbg(DEBUG, "CheckValidity : SQL: READ VATO = %s ", clDateTo ) ;
     
   /* ------------------------------------------------------------*/  

 /*-- APPL NOT INTERPRETED ------------------------------------*/
 /* TSE meaning is, that APPL is not important for the Check-  */
 /* validate module !!!       chg. 23.12.1999                  */ 
 /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
 /*    GetActFieldString( "APPL", clAppl ) ;                   */
 /*    if (strcmp ( pcpAppl, clAppl ) == 0)                    */ 
 /*    {                                                       */
 /*     dbg(DEBUG, "CheckValidity : APPL IS OK !!!") ;         */       
 /*------------------------------------------------------------*/  
     GetActFieldString("UVAL", clUrue) ;   /* chg. to UVAL !!! */ 
     if (strcmp (pcpUrue, clUrue) == 0)      
     { 
/*    dbg(DEBUG, "CheckValidity : URUE IS OK !!!") ;     */ 

      /* test date & time */
      GetActFieldString("VAFR", clDateFrom ) ;
      GetActFieldString("VATO", clDateTo ) ;

      if ((clDateTo[0] == ' ') || (clDateTo[0] == '\0') )  /* work with endless date */
      {
       /* ilRC = ChkDateTimeRange (clDateFrom, "99999999999999", pcpDate) ;   */
	   ilRC = IsDateInRange (clDateFrom, "99999999999999", pcpDate) ;
      }
      else     /* work with given <to> date */ 
      {
       /* ilRC = ChkDateTimeRange (clDateFrom, clDateTo, pcpDate) ;   */
	   ilRC = IsDateInRange (clDateFrom, clDateTo, pcpDate) ;
      }

      if (ilRC == 0) 
      {
       /* date and time are correct */
       GetActFieldString("FREQ", clFreq ) ;
       ilRC = ChkDayFrequenz ( pcpDate, clFreq ) ; 
	  
       if (ilRC == 0) 
       {
        /* Frequency is correct */
        GetActFieldString("TIMF", clTimf ) ;
        GetActFieldString("TIMT", clTimt ) ;
        ilRC = IsTimeInRange (clTimf, clTimt, pcpDate) ;
	   }
       if (ilRC == RC_SUCCESS) 
       {
        *piplsValid = 1 ;
        commit_work() ;   
        return (RC_SUCCESS) ;
       } ;  /* if frequenz of day */ 
      } ;  /* if date&time */ 
     } ;  /* if pcpUrue */ 

 /*-- APPL NOT INTERPRETED ------------------------------------*/
 /* } ;  / * if pcpAppl * /                                    */ 
 /*------------------------------------------------------------*/

    /* read next record */ 
    ilDbRC = sql_if(sgSQL_fkt,&sgSQL_cursor,cpgSQLStatement,
                    cgSqlDeclareBuffer) ;  

   } /* while next record */ 

   commit_work() ; 
   /* falling down, no record found !!! */ 
   dbg(ilDbMode, "CheckValidity : SQL: NO VALIDITY RECORD FOUND") ;     
   return (RC_FAIL) ;                              /* failed */

  }
  else                                   /*----------------*/
  {                                      /* ARRAY WORKMODE */
                                         /*----------------*/
 /*  dbg(ilDbMode, "CheckValidity : ARRAY Workmode") ;   */

   llRow = ARR_FIRST ; 
   ilFound = 0 ; 
   iTestCnt = 0 ;
 
   while ( ilFound == 0 )     /* CASE -> NO RECORDS !!! */ 
   { 
    iTestCnt++ ; 
    dbg(ilDbMode, "CheckValidity : | START WHILE LOOP = >>%d<< ", iTestCnt) ; 
    dbg(ilDbMode, "CheckValidity : Looking for RUEURNO >>%s<< ", pcpUrue) ; 
  
    ilRC = CEDAArrayFindRowPointer(&hlgArrHdl,cgArrName,&hlgIdxHdl,cgIdxName,
									pcpUrue,&llRow,(void**)&pclResult);
    if (ilRC != RC_SUCCESS) 
    {
  	 dbg(ilDbMode, "CheckValidity(FindRowPointer) with rc = %d",ilRC) ;  
     return (RC_FAILURE) ;                        /* failed */
    }

	ilRC = CEDAArrayGetRow(&hlgArrHdl, cgArrName, llRow, ',', 1024,
                           cgSqlDeclareBuffer) ;     
    if (ilRC != RC_SUCCESS) 
    {
     /* falling down, no record found !!! */ 
  	 dbg(TRACE, "CheckValidity : ARR: NO VALIDITY RECORD FOUND") ;  
     return (RC_FAILURE) ;                        /* failed */
    }

 /* dbg(DEBUG, "CheckValidity : -> <%s | %s | %s>", pcpAppl, pcpUrue, pcpDate) ; */        
 /* dbg(DEBUG, "CheckValidity : <- <%s>", cgSqlDeclareBuffer) ;                  */
    LoadActDeclareBufferRecord(cgSqlDeclareBuffer) ;  /* rec. 2 struct. */   

    /* ---------------------------------------------------------
    GetActFieldString("APPL", clAppl ) ;
    GetActFieldString("FREQ", clFreq ) ;
    GetActFieldString("HOPO", clHopo ) ;
    GetActFieldString("UVAL", clUrue ) ;    chg. to UVAL !!! 
    GetActFieldString("VAFR", clDateFrom ) ;
    GetActFieldString("VATO", clDateTo ) ;

    dbg(DEBUG, "CheckValidity : ARR: READ APPL = %s ", clAppl) ;  
    dbg(DEBUG, "CheckValidity : ARR: READ FREQ = %s ", clFreq) ;  
    dbg(DEBUG, "CheckValidity : ARR: READ HOPO = %s ", clHopo) ;  
    dbg(DEBUG, "CheckValidity : ARR: READ UVAL = %s ", clUrue) ;    chg. to UVAL !!!     
    dbg(DEBUG, "CheckValidity : ARR: READ VAFR = %s ", clDateFrom) ;  
    dbg(DEBUG, "CheckValidity : ARR: READ VATO = %s ", clDateTo ) ; 
    ------------------------------------------------------------*/  

 
/*    dbg(TRACE, "---------------------------------------" ) ;   */

    /*-- APPL NOT INTERPRETED ------------------------------------*/
    /* TSE meaning is, that APPL is not important for the Check-  */
    /* validate module !!!       chg. 23.12.1999                  */ 
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/ 
    /* GetActFieldString("APPL", clAppl ) ;                       */
    /* dbg(DEBUG, "CheckValidity : APPL read = <%s>", clAppl) ;   */
    /* if (strcmp (pcpAppl, clAppl ) == 0)                        */ 
    /* {                                                          */ 
    /* dbg(DEBUG, "CheckValidity : APPL IS OK !!!") ;             */ 
    /*------------------------------------------------------------*/

     GetActFieldString("UVAL", clUrue ) ;
 /*    dbg(DEBUG, "CheckValidity : UVAL read = <%s>", clUrue) ;    */
 /*    dbg(DEBUG, "CheckValidity : UVAL  cmp = <%s>", pcpUrue) ;   */
     if (strcmp (pcpUrue, clUrue) == 0)
     { 
    dbg(DEBUG, "CheckValidity : UVAL IS OK !!!") ;

      /* test date & time */    
      GetActFieldString("VAFR", clDateFrom ) ;
      GetActFieldString("VATO", clDateTo ) ;
      if ((clDateTo[0] == ' ') || (clDateTo[0] == '\0') )  /* work with endless date */
      {
       /* ilRC = ChkDateTimeRange (clDateFrom, "99999999999999", pcpDate) ;	*/
	   ilRC = IsDateInRange (clDateFrom, "99999999999999", pcpDate) ;
      }
      else     /* work with given <to> date */ 
      {
       /*  ilRC = ChkDateTimeRange (clDateFrom, clDateTo, pcpDate) ;  */
	   ilRC = IsDateInRange (clDateFrom, clDateTo, pcpDate) ;
      }
      if ((pcpDate[0] == ' ') || (pcpDate[0] == '\0')) 
      {
       ilRC = RC_FAIL ;             /* !!!! FALSE !!!! chg 11.05.2000 GH for TSC */ 
      }       


      if (ilRC == 0) 
      {
       /* date and time are correct */
		dbg(DEBUG, "CheckValidity : DATE & TIME ARE CORRECT !!!") ;
		GetActFieldString("FREQ", clFreq ) ;
		dbg(DEBUG, "CheckValidity : %s <-- this is the given frequenz", clFreq) ;

		ilRC = ChkDayFrequenz ( pcpDate, clFreq ) ; 
		if (ilRC == 0) 
		{
		  /* Frequency is correct */
		  GetActFieldString("TIMF", clTimf ) ;
		  GetActFieldString("TIMT", clTimt ) ;
		  ilRC = IsTimeInRange (clTimf, clTimt, pcpDate) ;
		}
		if (ilRC == RC_SUCCESS) 
		{
			*piplsValid = 1 ;
			return (RC_SUCCESS) ;
		}   /* if frequenz of day */ 
      } ;  /* if date&time to */  

     }   /* if pcpUrue */ 
	else
	{
		dbg(ilDbMode, "pcpUrue <%s> differs from clUrue <%s>", pcpUrue, clUrue);
	}
    /*-- APPL NOT INTERPRETED ------------------------------------*/
    /*  } ;  / * if pcpAppl * /                                   */
    /*------------------------------------------------------------*/

    llRow = ARR_NEXT ; 
   } ; /* end while */

  } ; /* else work with  array */ 

  return (RC_FAILURE) ;                     /* failed */

} /* end of CheckValidity () */

