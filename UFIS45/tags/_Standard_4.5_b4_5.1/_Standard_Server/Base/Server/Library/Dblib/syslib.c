#ifndef _DEF_mks_version_syslib_c
  #define _DEF_mks_version_syslib_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_syslib_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/syslib.c 1.5 2008/12/15 16:28:57SGT cst Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/*	S Y S L I B   Functions                                                  */
/*                                                                           */
/*****************************************************************************/

#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

#include "glbdef.h"
#include "db_if.h"
#include "syslib.h"
#include "msgno.h"
#include "nmghdl.h"


#define SYSLIB_VERSION 1
#define FAILED -1
#define SYSLIB_CREATE 0
#define SYSLIB_REORG 1
#define MAX_WAIT 60

extern FILE *outp;

extern int errno;

extern void snap(char *buf, int len, FILE *outp);

static int syslibBuildSMS(char *,int,char **,char **,long *);
static int syslibAttachSMS(char *,char **,char **);
static int syslibDetachSMS(char *,char *);
/*static int syslibGetFieldList(SYSH_DBSTRUCT *,SYSH_FIELDSTRUCT *,
							  char *,char *,SYSH_FIELDLIST2 *);
static int syslibGetSystabFieldList(char *,char *,SYSH_FIELDLIST *);*/
static int syslibReorgSMSInt(char *,char **,char *);
static int syslibSetSemaphore(int,int);
static int syslibClearSemaphore(int,int);
static int syslibCompare(char *,char *,int);

/*	Systab Description */
SYSH_FIELDLIST pSystabDesc[] =
{
	{"URNO",11,0,0,NULL},		/* unique reord number */
	{"TANA",33,11,0,NULL},		/* table name */
	{"FINA",17,44,0,NULL},		/* field name */
	{"FITY",4,61,0,NULL},		/* field type */
	{"FELE",9,65,0,NULL},		/* field length */
	{"MSGT",81,74,0,NULL},		/* message text, number */
	{"ADDI",1025,155,0,NULL},	/* additional information */
	{"TYPE",5,1180,0,NULL},		/* field contents type */
	{"REFE",49,1185,0,NULL},	/* basic data */
	{"DATR",49,1234,0,NULL},	/* source of field */
	{"INDX",2,1283,0,NULL},		/* index type */
	{"DEFA",65,1285,0,NULL},	/* default value */
	{"TATY",11,1350,0,NULL},	/* table type */
	{"SYST",2,1361,0,NULL},		/* shared memory flag */
	{"PROJ",129,1363,0,NULL},	/* project list */
	{"STAT",33,1492,0,NULL},	/* mandatory, optional */
	{"DEPE",65,1525,0,NULL},	/* reference to other fields */
	{"GUID",21,1590,0,NULL},	/* GUI information */
	{"SORT",5,1611,0,NULL},		/* sort number */
	{"CONT",33,1616,0,NULL},	/* container */
	{"LABL",65,1649,0,NULL},	/* GUI label */
	{"FMTS",21,1714,0,NULL},	/* format string */
	{"GTYP",3,1735,0,NULL},		/* GUI control type */
	{"GSTY",21,1738,0,NULL},	/* GUI window style */
	{"GLFL",65,1759,0,NULL},	/* GUI filter list */
	{"GDSR",33,1824,0,NULL},	/* GUI data source */
	{"GCFL",33,1857,0,NULL},	/* GUI control constraint fields */
	{"ORIE",2,1890,0,NULL},		/* orientation */
	{"REQF",2,1892,0,NULL},		/* required field flag */
	{"KONT",129,1894,0,NULL},	/* kontext sensitive help */
	{"LOGD",33,2023,0,NULL}		/* table name for logging */
};


/*****************************************************************************/
/*                                                                           */
/*	C R E A T E   S H A R E D   M E M O R Y                                  */
/*                                                                           */
/*****************************************************************************/

int syslibCreateSMS(char *pclTableP)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	char  *pclControl;
	char  *pclSMS;
	long  llSSMId;
	char	pclTable[33];

	/* Quoted strings in parameters causes runtime crash (read-only) */
	
	sprintf(pclTable,"%s",pclTableP);

	ilRc = iStrup(pclTable);

	ilRc = syslibBuildSMS(pclTable,SYSLIB_CREATE,&pclControl,&pclSMS,&llSSMId);
	dbg(DEBUG,"syslibCreateSMS: return from syslibBuildSMS = %d",ilRc);
	if (ilRc == RC_SUCCESS)
	{
		ilRc = syslibDetachSMS(pclControl,pclSMS);
		ilRc1 = send_message(IPRIO_ADMIN,SYSLIB_SSM_CREATED,0,0,pclTable);
	}
	else
	{
		ilRc1 = send_message(IPRIO_ADMIN,SYSLIB_SSM_CREATE_FAILED,0,0,pclTable);
	}

	return ilRc;
}	/* end of syslibCreateSMS */


/*****************************************************************************/
/*                                                                           */
/*	C R E A T E   B U I L D   M E M O R Y                                    */
/*                                                                           */
/*****************************************************************************/

int syslibBuildSMS(char *pclTable,int ilFlag,char **ppclControl,
				   char **ppclSMS,long *pllSSMId)
{
	int   ilRc = RC_SUCCESS;
	int   ilDb = RC_SUCCESS;
	short slFkt;
	short slCursor;
	int	  ilCnt;
	char  pclSqlBuf[512];
	char  pclDataArea[4096];
	int   ilSystabRecords;
	int   ilSSMSizeControl;
	int   ilSSMSize;
	char  *pclSSM;
	char  *pclSSMTmp;
	long  llSSMIdControl;	/* Control shared memory id */
	long  llSSMId;	/* shared memory id */
	long  llSEMId;	/* semaphore id */
	char  *pclControl;	/* Control shared memory address */
	char  *pclSMS;	/* shared memory address */
	SYSH_CONTROL psysControl;
	SYSH_GLOBAL *psysGlobal;
	SYSH_SYSTAB *psysSystab;
	SYSH_SYSTAB *psysSystabTmp;
	SYSH_DBSTRUCT *psysDbStruct;
	SYSH_DBSTRUCT *psysDbStructTmp;
	SYSH_FIELDSTRUCT *psysFieldStruct;
	SYSH_FIELDSTRUCT *psysFieldStructTmp;
	char  *pclDbData;
	char  *pclDbDataTmp;
	union
	{
		char pclProject[4];
		long llShmkey;
	} puShmkey;
	char  pclTableNames[512][33];
	char  pclFieldNames[1024][17];
	int   ilNoTables;
	int   ilNoTabs;
	int   ilNoFields;
	char  pclFieldLength[10];
	long  llOffStart;
	int   ilDbDataSize;
	char  pclFieldList[1024];
	unsigned short pslSemValues[4];
	union semun ulSemHelp;
	char  pclTabName[33];
	int   ilBlank;
	int   ilI;
	int   ilJ;
	int   ilK;
	char pclBuf[256];
	long  ilInc;

	ulSemHelp.array = pslSemValues;

/*		get number of SYSTAB reords		*/

	dbg(DEBUG,"syslibBuildSMS: Size of SYSH_GLOBAL = %d",SYSH_GLOBAL_SIZE);
	dbg(DEBUG,"syslibBuildSMS: Size of SYSH_SYSTAB = %d",SYSH_SYSTAB_SIZE);
	dbg(DEBUG,"syslibBuildSMS: Size of SYSH_DBSTRUCT = %d",SYSH_DBSTRUCT_SIZE);
	dbg(DEBUG,"syslibBuildSMS: Size of SYSH_FIELDSTRUCT = %d",
		SYSH_FIELDSTRUCT_SIZE);
	slCursor = 0;
	slFkt = START;
	sprintf(pclSqlBuf,"SELECT COUNT(*) FROM SYS%s",pclTable);
/*
	dbg(DEBUG,"SQL=%s",pclSqlBuf);
*/
	ilDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
	close_my_cursor(&slCursor);
	if (ilDb == RC_SUCCESS)
	{
		ilSystabRecords = atoi(pclDataArea);
		dbg(DEBUG,"syslibBuildSMS: No of SYSTAB Records = %d",
			ilSystabRecords);
	}
	else
	{
		dbg(TRACE,"syslibBuildSMS: Error from SELECT COUNT = %d",ilDb);
		return RC_FAIL;
	}

/*		allocate memory for SYSTAB		*/

	ilSSMSize = SYSH_GLOBAL_SIZE +
				(ilSystabRecords * SYSH_SYSTAB_SIZE) +
				SYSH_DBSTRUCT_SIZE +
				SYSH_FIELDSTRUCT_SIZE;
	dbg(DEBUG,"syslibBuildSMS: Size of SSM = %d",ilSSMSize);

	pclSSM = (char *)malloc(ilSSMSize);
	memset(pclSSM,'\0',ilSSMSize);
	pclSSMTmp = pclSSM;
	psysGlobal = (SYSH_GLOBAL *)pclSSM;
	pclSSMTmp += SYSH_GLOBAL_SIZE;
	psysSystab = (SYSH_SYSTAB *)pclSSMTmp;

	strncpy(psysGlobal->cProject,pclTable,4);
	psysGlobal->lVersion = SYSLIB_VERSION;
	psysGlobal->lStarted = time(0L);
	psysGlobal->lUpdated = psysGlobal->lStarted;
	psysGlobal->lUsedLenSSM = ilSSMSize;
	psysGlobal->lOffSystab = SYSH_GLOBAL_SIZE;
	psysGlobal->lOffDbStruct = psysGlobal->lOffSystab + 
						       (ilSystabRecords * SYSH_SYSTAB_SIZE);
	psysGlobal->lOffFieldStruct = psysGlobal->lOffDbStruct + SYSH_DBSTRUCT_SIZE;
	psysGlobal->lOffDbData = psysGlobal->lOffFieldStruct + 
							 SYSH_FIELDSTRUCT_SIZE;
	psysGlobal->lCntSystab = ilSystabRecords;

/*		write SYSTAB into memory		*/

	slCursor = 0;
	slFkt = START;
	sprintf(pclSqlBuf,"SELECT URNO,TANA,FINA,FITY,FELE,MSGT,ADDI,TYPE,REFE,DATR,INDX,DEFA,TATY,SYST,PROJ,STAT,DEPE,GUID,SORT,CONT,LABL,FMTS,GTYP,GSTY,GLFL,GDSR,GCFL,ORIE,REQF,KONT,LOGD FROM SYS%s ORDER BY TANA,FINA",pclTable);
	ilDb = RC_SUCCESS;
	ilCnt = 0;
	psysSystabTmp = psysSystab;
	while (ilDb == RC_SUCCESS)
	{
		ilDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
		if (ilDb != RC_SUCCESS)
		{
			if (ilDb != SQL_NOTFOUND)
			{
				get_ora_err (ilDb, pclBuf);
				dbg (TRACE, "syslibBuildSMS: %s",pclBuf);

			}
			if (ilCnt < ilSystabRecords)
			{
				dbg(TRACE,"syslibBuildSMS: Could read only %d records from SYSTAB, RC = %d",ilCnt,ilDb);
				return RC_FAIL;
			}
		}
		else
		{
			get_fld(pclDataArea,FIELD_1,STR,10,psysSystabTmp->urno);
			get_fld(pclDataArea,FIELD_2,STR,32,psysSystabTmp->tana);
			get_fld(pclDataArea,FIELD_3,STR,16,psysSystabTmp->fina);
			get_fld(pclDataArea,FIELD_4,STR,3,psysSystabTmp->fity);
			get_fld(pclDataArea,FIELD_5,STR,8,psysSystabTmp->fele);
			get_fld(pclDataArea,FIELD_6,STR,80,psysSystabTmp->msgt);
			get_fld(pclDataArea,FIELD_7,STR,1024,psysSystabTmp->addi);
			get_fld(pclDataArea,FIELD_8,STR,4,psysSystabTmp->type);
			get_fld(pclDataArea,FIELD_9,STR,48,psysSystabTmp->refe);
			get_fld(pclDataArea,FIELD_10,STR,48,psysSystabTmp->datr);
			get_fld(pclDataArea,FIELD_11,STR,1,psysSystabTmp->indx);
			get_fld(pclDataArea,FIELD_12,STR,64,psysSystabTmp->defa);
			get_fld(pclDataArea,FIELD_13,STR,10,psysSystabTmp->taty);
			get_fld(pclDataArea,FIELD_14,STR,1,psysSystabTmp->syst);
			get_fld(pclDataArea,FIELD_15,STR,128,psysSystabTmp->proj);
			get_fld(pclDataArea,FIELD_16,STR,32,psysSystabTmp->stat);
			get_fld(pclDataArea,FIELD_17,STR,64,psysSystabTmp->depe);
			get_fld(pclDataArea,FIELD_18,STR,20,psysSystabTmp->guid);
			get_fld(pclDataArea,FIELD_19,STR,4,psysSystabTmp->sort);
			get_fld(pclDataArea,FIELD_20,STR,32,psysSystabTmp->cont);
			get_fld(pclDataArea,FIELD_21,STR,64,psysSystabTmp->labl);
			get_fld(pclDataArea,FIELD_22,STR,20,psysSystabTmp->fmts);
			get_fld(pclDataArea,FIELD_23,STR,2,psysSystabTmp->gtyp);
			get_fld(pclDataArea,FIELD_24,STR,20,psysSystabTmp->gsty);
			get_fld(pclDataArea,FIELD_25,STR,64,psysSystabTmp->glfl);
			get_fld(pclDataArea,FIELD_26,STR,32,psysSystabTmp->gdsr);
			get_fld(pclDataArea,FIELD_27,STR,32,psysSystabTmp->gcfl);
			get_fld(pclDataArea,FIELD_28,STR,1,psysSystabTmp->orie);
			get_fld(pclDataArea,FIELD_29,STR,1,psysSystabTmp->reqf);
			get_fld(pclDataArea,FIELD_30,STR,128,psysSystabTmp->kont);
			get_fld(pclDataArea,FIELD_31,STR,32,psysSystabTmp->logd);
			ilBlank = 0;
			while (ilBlank < (int)strlen(psysSystabTmp->tana))
			{
				if (psysSystabTmp->tana[ilBlank] == ' ')
				{
					psysSystabTmp->tana[ilBlank] = '\0';
				}
				ilBlank++;
			}
			strcat(psysSystabTmp->tana,pclTable);
			ilBlank = 0;
			while (ilBlank < (int)strlen(psysSystabTmp->fina))
			{
				if (psysSystabTmp->fina[ilBlank] == ' ')
				{
					psysSystabTmp->fina[ilBlank] = '\0';
				}
				ilBlank++;
			}
			ilCnt++;
			pclSSMTmp = (char *)psysSystabTmp;
			pclSSMTmp += SYSH_SYSTAB_SIZE;
			psysSystabTmp = (SYSH_SYSTAB *)pclSSMTmp;
			slFkt = NEXT;
		}
	}
	close_my_cursor(&slCursor);
	dbg(DEBUG,"syslibBuildSMS: %d records read from SYSTAB",ilCnt);
/*	snap(pclSSM,2080,outp);	*/
	if (ilCnt != ilSystabRecords)
	{
		dbg(TRACE,"syslibBuildSMS: No of SYSTAB Records has changed: %d %d",
			ilCnt,ilSystabRecords);
		return RC_FAIL;
	}

/*		get tables for DB area		*/

	slCursor = 0;
	slFkt = START;
	sprintf(pclSqlBuf,"SELECT DISTINCT TANA FROM SYS%s",pclTable);
/*
sprintf(pclSqlBuf,"SELECT DISTINCT TANA FROM TABTAB");
*/
	ilDb = RC_SUCCESS;
	ilCnt = 0;
	while (ilDb == RC_SUCCESS)
	{
		ilDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
		if (ilDb == RC_SUCCESS)
		{
			ilBlank = 0;
			while (ilBlank < (int)strlen(pclDataArea))
			{
				if (pclDataArea[ilBlank] == ' ')
				{
					pclDataArea[ilBlank] = '\0';
				}
				ilBlank++;
			}
			strcat(pclDataArea,pclTable);
			strcpy(&pclTableNames[ilCnt][0],pclDataArea);
/*
			dbg(DEBUG,"syslibBuildSMS: Table <%s> selected",
				&pclTableNames[ilCnt][0]);
*/
			ilCnt++;
			slFkt = NEXT;
		}
	}
	close_my_cursor(&slCursor);
	ilNoTables = ilCnt;
	dbg(DEBUG,"syslibBuildSMS: %d tables selected from SYSTAB",ilNoTables);
/*
strcpy(&pclTableNames[0][0],"ACRTAB");
strcpy(&pclTableNames[1][0],"ACTTAB");
strcpy(&pclTableNames[2][0],"ALTTAB");
strcpy(&pclTableNames[3][0],"APTTAB");
strcpy(&pclTableNames[4][0],"DENTAB");
strcpy(&pclTableNames[5][0],"GATTAB");
strcpy(&pclTableNames[6][0],"NATTAB");
strcpy(&pclTableNames[7][0],"PSTTAB");
strcpy(&pclTableNames[8][0],"HAGTAB");
strcpy(&pclTableNames[9][0],"SEATAB");
ilNoTables = 10;
*/

/*		allocate DbStruct and FieldStruct	*/

	psysDbStruct = (SYSH_DBSTRUCT *)malloc(ilNoTables * SYSH_DBSTRUCT_SIZE);
	memset((char *)psysDbStruct,'\0',ilNoTables * SYSH_DBSTRUCT_SIZE);
	psysDbStructTmp = psysDbStruct;
	psysFieldStruct = (SYSH_FIELDSTRUCT *)malloc(ilNoTables * 20 * SYSH_FIELDSTRUCT_SIZE);
	memset((char *)psysFieldStruct,'\0',ilNoTables * 20 * SYSH_FIELDSTRUCT_SIZE);
	psysFieldStructTmp = psysFieldStruct;

/*		get fields for DB area		*/

	ilNoFields = 0;
	ilNoTabs = 0;
	for (ilI = 0; ilI < ilNoTables; ilI++)
	{
		slCursor = 0;
		slFkt = START;
		strcpy(pclTabName,&pclTableNames[ilI][0]);
		pclTabName[strlen(pclTabName)-3] = '\0';
		sprintf(pclSqlBuf,"SELECT FINA,FELE FROM SYS%s WHERE TANA = '%s' AND (FINA = 'URNO' OR SYST = 'Y') ORDER BY FINA",pclTable,pclTabName);
		sprintf(pclSqlBuf,"SELECT FINA,FELE FROM SYS%s WHERE TANA = '%s' AND SYST = 'Y' ORDER BY FINA",pclTable,pclTabName);
/*
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FETY = 'N') ORDER BY FINA",&pclTableNames[ilI][0]);
switch (ilI)
{
case 0:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'REGN' OR FINA = 'REGI' OR FINA = 'ACT3' OR FINA = 'MING' OR FINA = 'MAXG') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 1:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'ACT3' OR FINA = 'ACT4') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 2:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'ALC2' OR FINA = 'ALC3') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 3:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'APC3') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 4:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'DECN' OR FINA = 'DECA') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 5:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'GNAM') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 6:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'FNAT') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 7:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'PNAM' OR FINA = 'PDPO' OR FINA = 'PCAT') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 8:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'HAC3') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
case 9:
sprintf(pclSqlBuf,"SELECT FINA,FELE FROM TABTAB WHERE TANA = '%s' AND (FINA = 'URNO' OR FINA = 'SEAS') ORDER BY FINA",&pclTableNames[ilI][0]);
break;
}
*/
		ilDb = RC_SUCCESS;
		ilCnt = 0;
		while (ilDb == RC_SUCCESS)
		{
			ilDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
			if (ilDb == RC_SUCCESS)
			{
				get_fld(pclDataArea,FIELD_1,STR,16,&pclFieldNames[ilCnt][0]);
				get_fld(pclDataArea,FIELD_2,STR,8,pclFieldLength);
				ilBlank = 0;
				while (ilBlank < (int)strlen(&pclFieldNames[ilCnt][0]))
				{
					if (pclFieldNames[ilCnt][ilBlank] == ' ')
					{
						pclFieldNames[ilCnt][ilBlank] = '\0';
					}
					ilBlank++;
				}
				dbg(DEBUG,"syslibBuildSMS: Field <%s> selected",
					&pclFieldNames[ilCnt][0]);
				if (ilCnt == 0)
				{
					strcpy(psysDbStructTmp->pcTabName,&pclTableNames[ilI][0]);
					psysDbStructTmp->lCntTotLines = 0;
					psysDbStructTmp->lCntUsedLines = 0;
					psysDbStructTmp->lCntFields = 0;
					psysDbStructTmp->lOffStart = 0;
					psysDbStructTmp->lOffFieldStruct = ilNoFields;
					psysDbStructTmp->lLenLine = 0;
					llOffStart = 0;
					ilNoTabs++;
				}
				psysDbStructTmp->lCntFields++;
				psysDbStructTmp->lLenLine += atoi(pclFieldLength);
				strcpy(psysFieldStructTmp->pcFieldName,&pclFieldNames[ilCnt][0]);
				psysFieldStructTmp->lLenField = atoi(pclFieldLength);
				psysFieldStructTmp->lOffStart = llOffStart;
				psysFieldStructTmp->lIndexSystab = 0;
				llOffStart += atoi(pclFieldLength);
				pclSSMTmp = (char *)psysFieldStructTmp;
				pclSSMTmp += SYSH_FIELDSTRUCT_SIZE;
				psysFieldStructTmp = (SYSH_FIELDSTRUCT *)pclSSMTmp;
				ilCnt++;
				ilNoFields++;
				slFkt = NEXT;
			}
		}
		close_my_cursor(&slCursor);
		if (ilCnt > 0)
		{
			dbg(DEBUG,"syslibBuildSMS: %d fields selected from <%s>",ilCnt,&pclTableNames[ilI][0]);
			pclSSMTmp = (char *)psysDbStructTmp;
			pclSSMTmp += SYSH_DBSTRUCT_SIZE;
			psysDbStructTmp = (SYSH_DBSTRUCT *)pclSSMTmp;
		}
	}
	dbg(DEBUG,"syslibBuildSMS: %d real tables selected from SYSTAB",
		ilNoTabs);
	dbg(DEBUG,"syslibBuildSMS: DbStruct length = %d",
		ilNoTabs * SYSH_DBSTRUCT_SIZE);
	dbg(DEBUG,"syslibBuildSMS: %d fields total selected from SYSTAB",
		ilNoFields);
	dbg(DEBUG,"syslibBuildSMS: FieldStruct length = %d",
		ilNoFields * SYSH_FIELDSTRUCT_SIZE);
	psysGlobal->lOffFieldStruct = psysGlobal->lOffDbStruct + 
								  (ilNoTabs * SYSH_DBSTRUCT_SIZE);
	psysGlobal->lOffDbData = psysGlobal->lOffFieldStruct + 
							 (ilNoFields * SYSH_FIELDSTRUCT_SIZE);
	psysGlobal->lCntTables = ilNoTabs; 

/*		calculate DB data area size		*/

	psysDbStructTmp = psysDbStruct;
	ilDbDataSize = 0;
	for (ilI = 0; ilI < ilNoTabs; ilI++)
	{
		slCursor = 0;
		slFkt = START;
		sprintf(pclSqlBuf,"SELECT COUNT(*) FROM %s",psysDbStructTmp->pcTabName);
		ilDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
		close_my_cursor(&slCursor);
		if (ilDb != RC_SUCCESS)
		{
			dbg(DEBUG,"syslibBuildSMS: Error from SELECT COUNT %s is %d",
				psysDbStructTmp->pcTabName,ilDb);
			strcpy(pclDataArea,"0");
		}
		psysDbStructTmp->lOffStart = ilDbDataSize;
		psysDbStructTmp->lCntUsedLines = atoi(pclDataArea);
		dbg(DEBUG,"syslibBuildSMS: No of %s Records = %d",
			psysDbStructTmp->pcTabName,psysDbStructTmp->lCntUsedLines);
		if ((psysDbStructTmp->lCntUsedLines/10) < 200)
		{
			ilInc = 200;
		}
		else
		{
			ilInc = psysDbStructTmp->lCntUsedLines / 10;
		}
		psysDbStructTmp->lCntTotLines = psysDbStructTmp->lCntUsedLines + 1 + ilInc;
		ilDbDataSize += (psysDbStructTmp->lCntTotLines * psysDbStructTmp->lLenLine);
		pclSSMTmp = (char *)psysDbStructTmp;
		pclSSMTmp += SYSH_DBSTRUCT_SIZE;
		psysDbStructTmp = (SYSH_DBSTRUCT *)pclSSMTmp;
	}
	dbg(DEBUG,"syslibBuildSMS: DB data size = %d",ilDbDataSize);
	psysGlobal->lUsedLenSSM = psysGlobal->lOffDbData + ilDbDataSize;
	psysGlobal->lTotLenSSM = psysGlobal->lUsedLenSSM + 1 +
							 (psysGlobal->lUsedLenSSM / 10);

/*		fill DB data area		*/

	pclDbData = (char *)malloc(ilDbDataSize + 100);
	memset(pclDbData,'\0',ilDbDataSize);
	pclDbDataTmp = pclDbData;
	psysDbStructTmp = psysDbStruct;
	llOffStart = 0;
	for (ilJ = 0; ilJ < ilNoTabs; ilJ++)
	{
		psysDbStructTmp->lOffStart = llOffStart;
		if (psysDbStructTmp->lCntUsedLines > 0)
		{
			pclSSMTmp = (char *)psysFieldStruct +
						(psysDbStructTmp->lOffFieldStruct * SYSH_FIELDSTRUCT_SIZE);
			psysFieldStructTmp = (SYSH_FIELDSTRUCT *)pclSSMTmp;
			pclFieldList[0] = '\0';
			for (ilI = 0; ilI < psysDbStructTmp->lCntFields; ilI++)
			{
				strcat(pclFieldList,",");
				strcat(pclFieldList,psysFieldStructTmp->pcFieldName);
				pclSSMTmp = (char *)psysFieldStructTmp;
				pclSSMTmp += SYSH_FIELDSTRUCT_SIZE;
				psysFieldStructTmp = (SYSH_FIELDSTRUCT *)pclSSMTmp;
			}
			pclFieldList[0] = ' ';
			sprintf(pclSqlBuf,"SELECT%s FROM %s",
					pclFieldList,psysDbStructTmp->pcTabName);
/*
			dbg(DEBUG,"syslibBuildSMS: <%s> %d",
				pclSqlBuf,psysDbStructTmp->lCntUsedLines);
*/
			ilCnt = 0;
			slCursor = 0;
			slFkt = START;
			for (ilI = 0; ilI < psysDbStructTmp->lCntUsedLines; ilI++)
			{
				ilDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
				if (ilDb == RC_SUCCESS)
				{
					pclSSMTmp = (char *)psysFieldStruct +
								(psysDbStructTmp->lOffFieldStruct * SYSH_FIELDSTRUCT_SIZE);
					psysFieldStructTmp = (SYSH_FIELDSTRUCT *)pclSSMTmp;
					for (ilK = 0; ilK < psysDbStructTmp->lCntFields; ilK++)
					{
						get_fld(pclDataArea,(short)ilK,STR,
								(short)psysFieldStructTmp->lLenField,
								pclDbDataTmp);
						pclDbDataTmp += psysFieldStructTmp->lLenField;
						pclSSMTmp = (char *)psysFieldStructTmp;
						pclSSMTmp += SYSH_FIELDSTRUCT_SIZE;
						psysFieldStructTmp = (SYSH_FIELDSTRUCT *)pclSSMTmp;
					}
					llOffStart += psysDbStructTmp->lLenLine;
					ilCnt++;
				}
				slFkt = NEXT;
			}
			close_my_cursor(&slCursor);
			psysDbStructTmp->lCntUsedLines = ilCnt;
			ilCnt = psysDbStructTmp->lCntTotLines - ilCnt;
			llOffStart += (ilCnt * psysDbStructTmp->lLenLine);
			pclDbDataTmp += (ilCnt * psysDbStructTmp->lLenLine);
		}
		else
		{
			llOffStart += (psysDbStructTmp->lCntTotLines * psysDbStructTmp->lLenLine);
			pclDbDataTmp += (psysDbStructTmp->lCntTotLines * psysDbStructTmp->lLenLine);
		}
		pclSSMTmp = (char *)psysDbStructTmp;
		pclSSMTmp += SYSH_DBSTRUCT_SIZE;
		psysDbStructTmp = (SYSH_DBSTRUCT *)pclSSMTmp;
	}
/*	snap((char *)psysDbStruct,ilNoTabs * SYSH_DBSTRUCT_SIZE,outp);
	snap((char *)psysFieldStruct,ilNoFields * SYSH_FIELDSTRUCT_SIZE,outp);
	snap(pclDbData,ilDbDataSize,outp);	*/

/*		create and attach shared memory		*/

	strncpy(puShmkey.pclProject,pclTable,4);
	ilSSMSize = psysGlobal->lTotLenSSM;
	dbg(DEBUG,"syslibBuildSMS: SHMKEY = %x , SSM size = %d",
		puShmkey.llShmkey,ilSSMSize);
	llSSMId = shmget(puShmkey.llShmkey,ilSSMSize,IPC_CREAT|0666);
	if (llSSMId == RC_FAIL)
	{
		dbg(TRACE,"syslibBuildSMS: SHM creation failed");
		return RC_FAIL;
	}
	else
	{
		dbg(DEBUG,"syslibBuildSMS: SHM created , id = %d",llSSMId);
	}
	pclSMS = shmat(llSSMId,NULL,0);
	if (pclSMS == (char *)RC_FAIL)
	{
		dbg(TRACE,"syslibBuildSMS: SHM attachment failed");
		return RC_FAIL;
	}
	else
	{
		dbg(DEBUG,"syslibBuildSMS: SHM attached , Addr = %p",pclSMS);
	}
	*pllSSMId = llSSMId;

/*		create control shared memory	*/

	if (ilFlag == SYSLIB_CREATE)
	{
		memcpy((char *)&psysControl,(char *)psysGlobal,SYSH_CONTROL_SIZE);
		psysControl.cProject[3] = 'C';
		psysControl.lSSMId = llSSMId;
		psysControl.lCurrentUser = 1;
		psysControl.lReorg = SYSLIB_CREATE;
		puShmkey.pclProject[3] = 'C';
		ilSSMSizeControl = SYSH_CONTROL_SIZE;
		dbg(DEBUG,"syslibBuildSMS: SHMKEY = %x , SSM size = %d",
			puShmkey.llShmkey,ilSSMSizeControl);
		llSSMIdControl = shmget(puShmkey.llShmkey,ilSSMSizeControl,
								IPC_CREAT|0666);
		if (llSSMIdControl == RC_FAIL)
		{
			dbg(TRACE,"syslibBuildSMS: Control SHM creation failed");
			return RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"syslibBuildSMS: Control SHM created , id = %d",
				llSSMIdControl);
		}
		pclControl = shmat(llSSMIdControl,NULL,0);
		if (pclControl == (char *)RC_FAIL)
		{
			dbg(TRACE,"syslibBuildSMS: Control SHM attachment failed");
			return RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"syslibBuildSMS: Control SHM attached , Addr = %p",
				pclControl);
		}

		llSEMId = semget(puShmkey.llShmkey,2,IPC_CREAT|0660);
		if (llSEMId == RC_FAIL)
		{
			dbg(TRACE,"syslibBuildSMS: Semaphore creation failed");
			return RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"syslibBuildSMS: Semaphore created , id = %d",llSEMId);
			psysControl.lSEMId = llSEMId;
			pslSemValues[0] = 1;
			pslSemValues[1] = 1;
			ilRc = semctl(llSEMId,0,SETALL,ulSemHelp);
			if (ilRc == RC_FAIL)
			{
				dbg(TRACE,"syslibBuildSMS: Setting of semaphore values failed");
				dbg(TRACE,"syslibBuildSMS: ERRNO = %d , <%s>",errno,strerror(errno));
			}
			pslSemValues[0] = -1;
			pslSemValues[1] = -1;
			ilRc = semctl(llSEMId,0,GETALL,ulSemHelp);
			if (ilRc == RC_FAIL)
			{
				dbg(TRACE,"syslibBuildSMS: Reading of semaphore values failed");
				dbg(TRACE,"syslibBuildSMS: ERRNO = %d , <%s>",errno,strerror(errno));
			}
		}
		memcpy(pclControl,(char *)&psysControl,SYSH_CONTROL_SIZE);
/*		snap(pclControl,SYSH_CONTROL_SIZE,outp);	*/
	}
	else
	{
		pclControl = NULL;
	}

/*		copy all data into shared memory	*/

	memcpy(pclSMS,pclSSM,psysGlobal->lOffDbStruct);
	ilI = psysGlobal->lOffDbStruct;
	memcpy(&pclSMS[ilI],(char *)psysDbStruct,
	       psysGlobal->lOffFieldStruct - psysGlobal->lOffDbStruct);
	free((char *)psysDbStruct);
	ilI += (psysGlobal->lOffFieldStruct - psysGlobal->lOffDbStruct);
	memcpy(&pclSMS[ilI],(char *)psysFieldStruct,
	       psysGlobal->lOffDbData - psysGlobal->lOffFieldStruct);
	free((char *)psysFieldStruct);
	ilI += (psysGlobal->lOffDbData - psysGlobal->lOffFieldStruct);
	memcpy(&pclSMS[ilI],pclDbData,ilDbDataSize);
	free(pclDbData);
	free(pclSSM);
/*	snap(pclSMS,SYSH_GLOBAL_SIZE,outp);	*/
/*	snap(pclSMS,ilSSMSize,outp);	*/

	*ppclControl = pclControl;
	*ppclSMS = pclSMS;
/*
	dbg(DEBUG,"syslibBuildSMS: SHM Addrs = %p,%p",*ppclControl,*ppclSMS);
*/
	return ilRc;
}	/* end of syslibBuildSMS */


/*****************************************************************************/
/*                                                                           */
/*	D E L E T E   S H A R E D   M E M O R Y                                  */
/*                                                                           */
/*****************************************************************************/

int syslibDestroySMS(char *pclTableP)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	long  llSSMId;
	long  llSEMId;
	union semun ulSemHelp;
	union
	{
		char pclProject[4];
		long llShmkey;
	} puShmkey;
	char pclTable[33];

	/* Quoted strings in parameters causes runtime crash (read-only) */
	
	sprintf(pclTable,"%s",pclTableP);
	
	ilRc = iStrup(pclTable);
	ilRc = RC_SUCCESS;

	strncpy(puShmkey.pclProject,pclTable,4);
	llSSMId = shmget(puShmkey.llShmkey,10,IPC_CREAT|0666);
	if (llSSMId == RC_FAIL)
	{
		dbg(TRACE,"syslibDestroySMS: SHM mapping failed");
	}
	else
	{
		ilRc = shmctl(llSSMId,IPC_RMID,NULL);
		if (ilRc == RC_FAIL)
		{
			dbg(TRACE,"syslibDestroySMS: SHM deletion failed");
		}
		else
		{
			dbg(DEBUG,"syslibDestroySMS: SHM deleted");
		}
	}

	puShmkey.pclProject[3] = 'C';
	llSSMId = shmget(puShmkey.llShmkey,10,IPC_CREAT|0666);
	if (llSSMId == RC_FAIL)
	{
		dbg(TRACE,"syslibDestroySMS: Control SHM mapping failed");
	}
	else
	{
		ilRc = shmctl(llSSMId,IPC_RMID,NULL);
		if (ilRc == RC_FAIL)
		{
			dbg(TRACE,"syslibDestroySMS: Control SHM deletion failed");
		}
		else
		{
			dbg(DEBUG,"syslibDestroySMS: Control SHM deleted");
		}
	}

	llSEMId = semget(puShmkey.llShmkey,2,IPC_CREAT|0660);
	if (llSEMId == RC_FAIL)
	{
		dbg(TRACE,"syslibDestroySMS: Semaphore mapping failed");
	}
	else
	{
		ulSemHelp.array = NULL;
		ilRc = semctl(llSEMId,0,IPC_RMID,ulSemHelp);
		if (ilRc == RC_FAIL)
		{
			dbg(TRACE,"syslibDestroySMS: Semaphore deletion failed");
		}
		else
		{
			dbg(DEBUG,"syslibDestroySMS: Semaphore deleted");
		}
	}

/* Dont use QCP after shutdown sysqcp in sysmon */
/*	ilRc1 = send_message(IPRIO_ADMIN,SYSLIB_SSM_DELETED,0,0,pclTable);*/

	return RC_SUCCESS;
}	/* end of syslibDestroySMS */


/*****************************************************************************/
/*                                                                           */
/*	A T T A C H   S H A R E D   M E M O R Y                                  */
/*                                                                           */
/*****************************************************************************/

static int syslibAttachSMS(char *pclTable,char **ppclControl,char **ppclSMS)
{
	int   ilRc = RC_SUCCESS;
	int   ilCnt;
	char  *pclControl = NULL;
	char  *pclSMS = NULL;
	SYSH_CONTROL *psysControl;
	long  llSSMId;
	union
	{
		char pclProject[4];
		long llShmkey;
	} puShmkey;

	strncpy(puShmkey.pclProject,pclTable,4);
	puShmkey.pclProject[3] = 'C';
	llSSMId = shmget(puShmkey.llShmkey,10,IPC_CREAT|0666);
	if (llSSMId == RC_FAIL)
	{
		dbg(TRACE,"syslibAttachSMS: Control SHM mapping failed");
		return RC_FAIL;
	}
/*
	else
	{
		dbg(DEBUG,"syslibAttachSMS: Control SHM mapped , id = %d",llSSMId);
	}
*/

	pclControl = shmat(llSSMId,NULL,0);
	if (pclControl == (char *)RC_FAIL)
	{
		dbg(TRACE,"syslibAttachSMS: Control SHM attachment failed");
		dbg(TRACE,"syslibAttachSMS: ERRNO = %d , <%s>",errno,strerror(errno));
		return RC_FAIL;
	}
/*
	else
	{
		dbg(DEBUG,"syslibAttachSMS: Control SHM attached , Addr = %p",
			pclControl);
	}
*/
	if (strncmp(pclControl,pclTable,3) != 0 || *pclControl == '\0')
	{
		dbg(TRACE,"syslibAttachSMS: Control SHM is wrong");
		ilRc = syslibDetachSMS(pclControl,pclSMS);
		ilRc = syslibDestroySMS(pclTable);
		return RC_FAIL;
	}
/*
	else
	{
		dbg(DEBUG,"syslibAttachSMS: Control SHM is OK");
	}
*/
	psysControl = (SYSH_CONTROL *)pclControl;
	ilCnt = 0;
	ilRc = syslibSetSemaphore(psysControl->lSEMId,0);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibAttachSMS: Set semaphore has failed");
		return RC_FAIL;
	}
	while (psysControl->lReorg == SYSLIB_REORG && ilCnt < MAX_WAIT)
	{
		sleep(1);
		ilCnt++;
	}
	if (ilCnt == MAX_WAIT)
	{
		dbg(TRACE,"syslibAttachSMS: SHM reorg flag is permanently set");
		ilRc = syslibClearSemaphore(psysControl->lSEMId,0);
		ilRc = send_message(IPRIO_ADMIN,SYSLIB_SSM_REORG_PERM,0,0,pclTable);
		return RC_FAIL;
	}
	psysControl->lCurrentUser++;
	ilRc = syslibClearSemaphore(psysControl->lSEMId,0);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibAttachSMS: Clear semaphore has failed");
		return RC_FAIL;
	}
	llSSMId = psysControl->lSSMId;
	pclSMS = shmat(llSSMId,NULL,0);
	if (pclSMS == (char *)RC_FAIL)
	{
		dbg(TRACE,"syslibAttachSMS: SHM attachment failed");
		dbg(TRACE,"syslibAttachSMS: ERRNO = %d , <%s>",errno,strerror(errno));
		psysControl->lCurrentUser--;
		return RC_FAIL;
	}
/*
	else
	{
		dbg(DEBUG,"syslibAttachSMS: SHM attached , Addr = %p",pclSMS);
	}
*/
	if (strncmp(pclSMS,pclTable,3) != 0 || *pclSMS == '\0')
	{
		dbg(TRACE,"syslibAttachSMS: SHM is wrong");
		psysControl->lCurrentUser--;
		ilRc = syslibDetachSMS(pclControl,pclSMS);
		ilRc = syslibDestroySMS(pclTable);
		return RC_FAIL;
	}
/*
	else
	{
		dbg(DEBUG,"syslibAttachSMS: SHM is OK");
	}
*/
	*ppclControl = pclControl;
	*ppclSMS = pclSMS;
/*	snap(pclControl,SYSH_CONTROL_SIZE,outp);	*/
/*	snap(pclSMS,64,outp);	*/

	return ilRc;
}	/* end of syslibAttachSMS */


/*****************************************************************************/
/*                                                                           */
/*	D E T A C H   S H A R E D   M E M O R Y                              */
/*                                                                           */
/*****************************************************************************/

static int syslibDetachSMS(char *pclControl,char *pclSMS)
{
	int   ilRc = RC_SUCCESS;
	SYSH_CONTROL *psysControl;

/*	snap(pclControl,SYSH_CONTROL_SIZE,outp);	*/
/*	snap(pclSMS,SYSH_GLOBAL_SIZE,outp);	*/
	psysControl = (SYSH_CONTROL *)pclControl;
	ilRc = syslibSetSemaphore(psysControl->lSEMId,0);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibDetachSMS: Set semaphore has failed");
	}
	if (psysControl->lCurrentUser > 0)
	{
		psysControl->lCurrentUser--;
	}
	ilRc = syslibClearSemaphore(psysControl->lSEMId,0);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibDetachSMS: Clear semaphore has failed");
	}
	ilRc = shmdt(pclControl);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibDetachSMS: Control SHM detachment failed");
	}
/*
	else
	{
		dbg(DEBUG,"syslibDetachSMS: Control SHM detached");
	}
*/

	ilRc = shmdt(pclSMS);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibDetachSMS: SHM detachment failed");
		return RC_FAIL;
	}
/*
	else
	{
		dbg(DEBUG,"syslibDetachSMS: SHM detached");
	}
*/
	return ilRc;
}	/* end of syslibDetachSMS */


/*****************************************************************************/
/*                                                                           */
/*	U P D A T E   D B   D A T A                                              */
/*                                                                           */
/*****************************************************************************/

int syslibUpdateDbData(char clFkt,char *pclTableNameP,char *pclKeyP,
					   char *pclKeyValue)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	int   ilDb = RC_SUCCESS;
	char  *pclControl;
	char  *pclControlTmp;
	char  *pclSMS;
	char  pclProject[32];
	SYSH_CONTROL *psysControl;
	SYSH_GLOBAL *psysGlobal;
	SYSH_DBSTRUCT *psysDbStruct;
	SYSH_FIELDSTRUCT *psysFieldStruct;
	char  *pclDbData;
	char  *pclDbDataTmp;
	char  *pclTmp;
	char  pclFieldList[1024];
	long  llOffStart;
	long  llLenField;
	char  pclSqlBuf[512];
	char  pclDataArea[4096];
	short slCursor;
	int   ilFieldSize[256];
	char  pclDbLine[4096];
	char  *pclDbLineTmp;
	int   ilSize;
	int   ilCnt;
	int   ilI;
	int   ilJ;
	char	pclTableName[33];
	char	pclKey[129];				/* Size not fixed. 129 bytes to be sure */

	/* Quoted strings in parameters causes runtime crash (read-only) */
	
	sprintf(pclTableName,"%s",pclTableNameP);
	sprintf(pclKey,"%s",pclKeyP);

	ilRc = iStrup(pclTableName);
	ilRc = iStrup(pclKey);

	strcpy(pclProject,&pclTableName[strlen(pclTableName)-3]);
	ilRc = syslibAttachSMS(pclProject,&pclControl,&pclSMS);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"syslibUpdateDbData: SHM attachment failed");
		return RC_FAIL;
	}

/*
dbg(TRACE,"SYSLIB: FKT <%c>",clFkt);
dbg(TRACE,"SYSLIB: TBL <%s>",pclTableName);
dbg(TRACE,"SYSLIB: KEY <%s> <%s>",pclKey,pclKeyValue);
*/

	psysControl = (SYSH_CONTROL *)pclControl;
	psysGlobal = (SYSH_GLOBAL *)pclSMS;
	pclTmp = pclSMS + psysGlobal->lOffDbStruct;
	psysDbStruct = (SYSH_DBSTRUCT *)pclTmp;
	pclTmp = pclSMS + psysGlobal->lOffFieldStruct;
	psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;

	ilI = 0;
	ilRc = RC_FAIL;
	while (ilRc == RC_FAIL && ilI < psysGlobal->lCntTables)
	{
		if (strcmp(psysDbStruct->pcTabName,pclTableName) == 0)
		{
			ilRc = RC_SUCCESS;
		}
		else
		{
			pclTmp = (char *)psysDbStruct;
			pclTmp += SYSH_DBSTRUCT_SIZE;
			psysDbStruct = (SYSH_DBSTRUCT *)pclTmp;
		}
		ilI++;
	}
	if (ilRc == RC_FAIL)
	{
		dbg(DEBUG,"syslibUpdateDbData: Table not found");
		ilRc1 = syslibDetachSMS(pclControl,pclSMS);
		if (ilRc1 == RC_FAIL)
		{
			dbg(TRACE,"syslibReloadDbData: SHM detachment failed");
		}
		return RC_SUCCESS;
	}
	else
	{
		pclTmp = (char *)psysFieldStruct;
		pclTmp += (SYSH_FIELDSTRUCT_SIZE * psysDbStruct->lOffFieldStruct);
		psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;
		pclFieldList[0] = '\0';
		llOffStart = -1;
		for (ilI = 0; ilI < psysDbStruct->lCntFields; ilI++)
		{
			strcat(pclFieldList,",");
			strcat(pclFieldList,psysFieldStruct->pcFieldName);
			if (strcmp(psysFieldStruct->pcFieldName,pclKey) == 0)
			{
				llOffStart = psysFieldStruct->lOffStart;
				llLenField = psysFieldStruct->lLenField;
			}
			ilFieldSize[ilI] = psysFieldStruct->lLenField;
			pclTmp = (char *)psysFieldStruct;
			pclTmp += SYSH_FIELDSTRUCT_SIZE;
			psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;
		}
		pclFieldList[0] = ' ';
		sprintf(pclSqlBuf,"SELECT%s FROM %s WHERE %s='%s'",
				pclFieldList,pclTableName,pclKey,pclKeyValue);
		/*
		dbg(TRACE,"syslibUpdateDbData: Sql Buffer <%s>",pclSqlBuf);
		*/
		if (llOffStart < 0)
		{
			dbg(TRACE,"syslibUpdateDbData: Key does not exist");
			ilRc = RC_FAIL;
		}
	}

	if (ilRc == RC_SUCCESS)
	{
		switch (clFkt)
		{
			case 'U':
				/*
				dbg(TRACE,"syslibUpdateDbData: Update DB data");
				*/
				pclDbData = pclSMS + psysGlobal->lOffDbData +
							psysDbStruct->lOffStart;
				ilRc = RC_FAIL;
				ilI = 0;
				while (ilRc == RC_FAIL && ilI < psysDbStruct->lCntUsedLines)
				{
/*
dbg(TRACE,"COMP <%10.10s> <%s> LEN=%d",
	  &pclDbData[llOffStart],pclKeyValue,llLenField);
*/
					if (strncmp(&pclDbData[llOffStart],pclKeyValue,llLenField) == 0)
					{
						slCursor = 0;
						ilDb = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
						close_my_cursor(&slCursor);
						if (ilDb == RC_SUCCESS)
						{
							pclDbLineTmp = pclDbLine;
/*							snap(pclDbData,psysDbStruct->lLenLine+4,outp);	*/
							for (ilJ = 0; ilJ < psysDbStruct->lCntFields; ilJ++)
							{
								get_fld(pclDataArea,(short)ilJ,STR,
										ilFieldSize[ilJ],pclDbLineTmp);
								pclDbLineTmp += ilFieldSize[ilJ];
							}
							memcpy(pclDbData,pclDbLine,psysDbStruct->lLenLine);
							psysGlobal->lUpdated = time(0L);
/*							snap(pclDbData,psysDbStruct->lLenLine+4,outp);	*/
							ilRc = RC_SUCCESS;
						}
						else
						{
							dbg(TRACE,"syslibUpdateDbData: Could not read data");
							ilRc = RC_FAIL;
						}
					}
					else
					{
						pclDbData += psysDbStruct->lLenLine;
					}
					ilI++;
				}
				break;
			case 'I':
				/*
				dbg(TRACE,"syslibUpdateDbData: Insert DB data");
				*/
				if (psysDbStruct->lCntTotLines - psysDbStruct->lCntUsedLines > 0)
				{
					ilRc = syslibSetSemaphore(psysControl->lSEMId,1);
					if (ilRc == RC_FAIL)
					{
						dbg(TRACE,"syslibUpdateDbData: Set semaphore has failed");
					}
					pclDbData = pclSMS + psysGlobal->lOffDbData +
								psysDbStruct->lOffStart;
					ilRc = RC_FAIL;
					ilI = 0;
					while (ilRc == RC_FAIL && ilI < psysDbStruct->lCntUsedLines + 1)
					{
/*
dbg(TRACE,"COMP <%10.10s> <%s> LEN=%d",
	  &pclDbData[llOffStart],pclKeyValue,llLenField);
*/
						if (pclDbData[llOffStart] == '\0' ||
							strncmp(&pclDbData[llOffStart],pclKeyValue,llLenField) == 0)
						{
							slCursor = 0;
							ilDb = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
							close_my_cursor(&slCursor);
							if (ilDb == RC_SUCCESS)
							{
								pclDbLineTmp = pclDbLine;
/*								snap(pclDbData-psysDbStruct->lLenLine,psysDbStruct->lLenLine*2+4,outp);	*/
								for (ilJ = 0; ilJ < psysDbStruct->lCntFields; ilJ++)
								{
									get_fld(pclDataArea,(short)ilJ,STR,
											ilFieldSize[ilJ],pclDbLineTmp);
									pclDbLineTmp += ilFieldSize[ilJ];
								}
								memcpy(pclDbData,pclDbLine,psysDbStruct->lLenLine);
								psysGlobal->lUpdated = time(0L);
/*								snap(pclDbData-psysDbStruct->lLenLine,psysDbStruct->lLenLine*2+4,outp);	*/
/*								snap((char *)psysDbStruct,SYSH_DBSTRUCT_SIZE,outp);	*/
								if (ilI == psysDbStruct->lCntUsedLines)
								{
									psysDbStruct->lCntUsedLines++;
								}
/*								snap((char *)psysDbStruct,SYSH_DBSTRUCT_SIZE,outp);	*/
								ilRc = RC_SUCCESS;
							}
							else
							{
								dbg(TRACE,"syslibUpdateDbData: Could not read data");
								ilRc = RC_FAIL;
							}
						}
						else
						{
							pclDbData += psysDbStruct->lLenLine;
						}
						ilI++;
					}
					ilRc = syslibClearSemaphore(psysControl->lSEMId,1);
					if (ilRc == RC_FAIL)
					{
						dbg(TRACE,"syslibUpdateDbData: Clear semaphore has failed");
					}
				}
				else
				{	/* reorg shared memory */
					dbg(TRACE,"syslibUpdateDbData: Reorg shared memory");
					ilRc = syslibReorgSMSInt(pclControl,&pclSMS,pclProject);
				}
				break;
			case 'D':
/*
				dbg(DEBUG,"syslibUpdateDbData: Delete DB data");
*/
				ilRc = syslibSetSemaphore(psysControl->lSEMId,1);
				if (ilRc == RC_FAIL)
				{
					dbg(TRACE,"syslibUpdateDbData: Set semaphore has failed");
				}
				pclDbData = pclSMS + psysGlobal->lOffDbData +
							psysDbStruct->lOffStart;
				ilRc = RC_FAIL;
				ilI = 0;
				while (ilRc == RC_FAIL && ilI < psysDbStruct->lCntUsedLines)
				{
					if (strncmp(&pclDbData[llOffStart],pclKeyValue,llLenField) == 0)
					{
						ilRc = RC_SUCCESS;
					}
					else
					{
						pclDbData += psysDbStruct->lLenLine;
					}
					ilI++;
				}
				if (ilRc == RC_SUCCESS)
				{
					pclDbDataTmp = pclSMS + psysGlobal->lOffDbData +
								   psysDbStruct->lOffStart;
/*					snap(pclDbDataTmp,psysDbStruct->lCntTotLines*psysDbStruct->lLenLine,outp);	*/
					pclDbDataTmp = pclDbData + psysDbStruct->lLenLine;
					ilSize = ((psysDbStruct->lCntUsedLines - ilI) * 
							 psysDbStruct->lLenLine);
					memcpy(pclDbData,pclDbDataTmp,ilSize);
					pclDbData = pclSMS + psysGlobal->lOffDbData +
								psysDbStruct->lOffStart;
					pclDbData += ((psysDbStruct->lCntUsedLines - 1) *
								 psysDbStruct->lLenLine);
					memset(pclDbData,'\0',psysDbStruct->lLenLine);
					pclDbDataTmp = pclSMS + psysGlobal->lOffDbData +
								   psysDbStruct->lOffStart;
/*					snap(pclDbDataTmp,psysDbStruct->lCntTotLines*psysDbStruct->lLenLine,outp);	*/
/*					snap((char *)psysDbStruct,SYSH_DBSTRUCT_SIZE,outp);	*/
					psysDbStruct->lCntUsedLines--;
/*					snap((char *)psysDbStruct,SYSH_DBSTRUCT_SIZE,outp);	*/
					psysGlobal->lUpdated = time(0L);
				}
				else
				{
					dbg(TRACE,"syslibUpdateDbData: Could not read data");
					ilRc = RC_FAIL;
				}
				ilRc = syslibClearSemaphore(psysControl->lSEMId,1);
				if (ilRc == RC_FAIL)
				{
					dbg(TRACE,"syslibUpdateDbData: Clear semaphore has failed");
				}
				break;
			default :
				dbg(TRACE,"syslibUpdateDbData: Invalid function %c",clFkt);
				ilRc = RC_FAIL;
				break;
		}
	}

	ilRc1 = syslibDetachSMS(pclControl,pclSMS);
	if (ilRc1 == RC_FAIL)
	{
		dbg(TRACE,"syslibUpdateDbData: SHM detachment failed");
	}
/*
	else
	{
		dbg(DEBUG,"syslibUpdateDbData: SHM detached");
	}
*/
	return ilRc;
}	/* end of syslibUpdateDbData */


/*****************************************************************************/
/*                                                                           */
/*	S E A R C H   V A L I D   D B   D A T A                              */
/*                                                                           */
/*****************************************************************************/

int syslibSearchValidDbData(char *pcpTableName, char *pcpInFieldList,
                            char *pcpInFieldValues, char *pcpOutFieldList,
                            char *pcpOutFieldValues, int *pipCount,
                            char *pcpLineSeperator, char *pcpDateFields,
                            char *pcpTime)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "syslibSearchValidDbData:";
  char pclNewFieldList[2048];
  char pclNewFieldValues[32000];
  char pclTmpOut[32000];
  int ilLen;
  char pclFrom[8];
  char pclTo[8];
  int ilFound;
  char *pclTmpPtr;
  int ilItemNoFrom;
  int ilItemNoTo;
  char pclFromVal[16];
  char pclToVal[16];
  int ilOrgCount;
  int ilI;
  char *pclBegin;
  char *pclEnd;
  char pclLine[2048];

  memset(pclNewFieldValues,0x00,32000);
  ilLen = get_real_item(pclFrom,pcpDateFields,1);
  if (ilLen <= 0)
  {
     dbg(TRACE,"%s Valid from <%s> is missing!!!",pclFunc,pcpDateFields);
     strcpy(pcpOutFieldValues,"");
     *pipCount = 0;
     return RC_FAIL;
  }
  ilLen = get_real_item(pclTo,pcpDateFields,2);
  if (ilLen <= 0)
  {
     dbg(TRACE,"%s Valid to <%s> is missing!!!",pclFunc,pcpDateFields);
     strcpy(pcpOutFieldValues,"");
     *pipCount = 0;
     return RC_FAIL;
  }
  if (strlen(pcpTime) != 14)
  {
     dbg(TRACE,"%s Invalid date/time format <%s> !!!",pclFunc,pcpTime);
     strcpy(pcpOutFieldValues,"");
     *pipCount = 0;
     return RC_FAIL;
  }
  sprintf(pclNewFieldList,"%s,%s,%s",pcpOutFieldList,pclFrom,pclTo);
  strcpy(pclNewFieldList,pcpOutFieldList);
  if (strstr(pclNewFieldList,pclFrom) == NULL)
  {
     strcat(pclNewFieldList,",");
     strcat(pclNewFieldList,pclFrom);
  }
  if (strstr(pclNewFieldList,pclTo) == NULL)
  {
     strcat(pclNewFieldList,",");
     strcat(pclNewFieldList,pclTo);
  }
  ilRC = syslibSearchDbData(pcpTableName,pcpInFieldList,pcpInFieldValues,
                            pclNewFieldList,pclNewFieldValues,pipCount,
                            pcpLineSeperator);
  if (ilRC == RC_SUCCESS)
  {
     if (*pipCount > 0)
     {
        ilOrgCount = *pipCount;
        strcpy(pcpOutFieldValues,"");
        *pipCount = 0;
        ilRC = RC_NOT_FOUND;
        ilItemNoFrom = get_item_no(pclNewFieldList,pclFrom,4) + 1;
        ilItemNoTo = get_item_no(pclNewFieldList,pclTo,4) + 1;
        if (ilItemNoFrom > 0 && ilItemNoTo > 0)
        {
           ilFound = FALSE;
           pclBegin = pclNewFieldValues;
           pclEnd = pclBegin;
           for (ilI = 0; ilI < ilOrgCount && ilFound == FALSE; ilI++)
           {
              pclEnd = strstr(pclBegin,pcpLineSeperator);
              if (pclEnd == NULL)
                 pclEnd = pclBegin + strlen(pclBegin);
              strncpy(pclLine,pclBegin,pclEnd-pclBegin);
              pclLine[pclEnd-pclBegin] = '\0';
              ilLen = get_real_item(pclFromVal,pclLine,ilItemNoFrom);
              if (ilLen <= 0)
                 strcpy(pclFromVal,"20000101000000");
              ilLen = get_real_item(pclToVal,pclLine,ilItemNoTo);
              if (ilLen <= 0)
                 strcpy(pclToVal,"20201231235959");
              if (strcmp(pclFromVal,pcpTime) <= 0 && strcmp(pclToVal,pcpTime) >= 0)
              {
                 strcpy(pclTmpOut,pclLine);
                 *pipCount = 1;
                 ilRC = RC_SUCCESS;
                 ilFound = TRUE;
              }
              pclBegin = pclEnd + strlen(pcpLineSeperator);
              pclEnd = pclBegin;
           }
        }
     }
     else
     {
        strcpy(pcpOutFieldValues,"");
     }
     if (*pipCount == 1)
     {
        if (strstr(pcpOutFieldList,pclTo) == NULL)
        {
           ilFound = FALSE;
           pclTmpPtr = pclTmpOut + strlen(pclTmpOut);
           while (pclTmpPtr > pclTmpOut && ilFound == FALSE)
           {
              if (*pclTmpPtr == ',')
                 ilFound = TRUE;
              *pclTmpPtr = '\0';
              pclTmpPtr--;
           }
        }
        if (strstr(pcpOutFieldList,pclFrom) == NULL)
        {
           ilFound = FALSE;
           pclTmpPtr = pclTmpOut + strlen(pclTmpOut);
           while (pclTmpPtr > pclTmpOut && ilFound == FALSE)
           {
              if (*pclTmpPtr == ',')
                 ilFound = TRUE;
              *pclTmpPtr = '\0';
              pclTmpPtr--;
           }
        }
        strcpy(pcpOutFieldValues,pclTmpOut);
     }
  }
  else
  {
     strcpy(pcpOutFieldValues,"");
  }

  return ilRC;
} /* End of syslibSearchValidDbData */


/*****************************************************************************/
/*                                                                           */
/*	S E A R C H   D B   D A T A                                              */
/*                                                                           */
/*****************************************************************************/

int syslibSearchDbData(char *pclTableNameP,
		   char *pclInFieldList,char *pclInFieldValues,
		   char *pclOutFieldList,char *pclOutFieldValues,
		   int *pilCount,char *pclLineSeperator)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	char  *pclControl;
	char  *pclSMS;
	char  pclProject[32];
	SYSH_CONTROL *psysControl;
	SYSH_GLOBAL *psysGlobal;
	SYSH_DBSTRUCT *psysDbStruct;
	SYSH_FIELDSTRUCT *psysFieldStruct;
	SYSH_FIELDSTRUCT *psysFieldStructTmp;
	char  *pclDbData;
	char  *pclDbDataTmp;
	char  *pclTmp;
	char  pclTmpData[4096];
	SYSH_FIELDLIST2 pFieldDesc[32];
	SYSH_FIELDLIST2 pFieldDescV[32];
	int   ilCnt;
	int   ilCntV;
	int   ilCount;
	int   ilFound;
	int   ilI;
	int   ilJ;
	int   ilK;
	char  *pclTmpInFieldList;
	char  *pclTmpInFieldValues;
	char  *pclTmpOutFieldList;
	char  *pclTmpOutFieldValues;
	int   ilMaxCount;
	char	pclTableName[33];

	/* Quoted strings in parameters causes runtime crash (read-only) */
	
	sprintf(pclTableName,"%s",pclTableNameP);

	if (pclTableName != NULL)
	{
		ilRc = iStrup(pclTableName);
	}
	
	/*
	if(pclInFieldList != NULL)
	{
		ilRc = iStrup(pclInFieldList);
	}
	if(pclOutFieldList != NULL)
	{
		ilRc = iStrup(pclOutFieldList);
	} */

	pclTmpInFieldList = pclInFieldList;
	pclTmpInFieldValues = pclInFieldValues;
	pclTmpOutFieldList = pclOutFieldList;
	pclTmpOutFieldValues = pclOutFieldValues;
	strcpy(pclProject,&pclTableName[strlen(pclTableName)-3]);
	ilRc = syslibAttachSMS(pclProject,&pclControl,&pclSMS);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"syslibSearchDbData: SHM attachment failed");
		return RC_FAIL;
	}
/*
	else
	{
		dbg(TRACE,"syslibSearchDbData: SHM attached");
	}
*/
	psysGlobal = (SYSH_GLOBAL *)pclSMS;
	pclTmp = pclSMS + psysGlobal->lOffDbStruct;
	psysDbStruct = (SYSH_DBSTRUCT *)pclTmp;
	pclTmp = pclSMS + psysGlobal->lOffFieldStruct;
	psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;

	ilI = 0;
	ilRc = RC_FAIL;
	while (ilRc == RC_FAIL && ilI < psysGlobal->lCntTables)
	{
		if (strcmp(psysDbStruct->pcTabName,pclTableName) == 0)
		{
			ilRc = RC_SUCCESS;
		}
		else
		{
			pclTmp = (char *)psysDbStruct;
			pclTmp += SYSH_DBSTRUCT_SIZE;
			psysDbStruct = (SYSH_DBSTRUCT *)pclTmp;
		}
		ilI++;
	}
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibSearchDbData: Table not found");
	}
	else
	{
		pclTmp = (char *)psysFieldStruct;
		pclTmp += (SYSH_FIELDSTRUCT_SIZE * psysDbStruct->lOffFieldStruct);
		psysFieldStructTmp = (SYSH_FIELDSTRUCT *)pclTmp;
		if (pclTmpInFieldList == NULL || pclTmpInFieldValues == NULL)
		{
			ilCnt = 0;
		}
		else
		{
			ilCnt = syslibGetFieldList(psysDbStruct,
						   psysFieldStructTmp,
						   pclTmpInFieldList,
						   pclTmpInFieldValues,
						   pFieldDesc);
		}
		if (ilCnt < 0)
		{
			dbg(TRACE,"syslibSearchDbData: InFieldList not found");
			ilRc = RC_FAIL;
		}
		else
		{
			pclTmp = (char *)psysFieldStruct;
			pclTmp += (SYSH_FIELDSTRUCT_SIZE * psysDbStruct->lOffFieldStruct);
			psysFieldStructTmp = (SYSH_FIELDSTRUCT *)pclTmp;
			ilCntV = syslibGetFieldList(psysDbStruct,psysFieldStructTmp,
										   pclTmpOutFieldList,"",
										   pFieldDescV);
			if (ilCntV < 0)
			{
				dbg(TRACE,"syslibSearchDbData: OutFieldList not found");
				ilRc = RC_FAIL;
			}
		}
	}

	pclTmpOutFieldValues[0] = '\0';
	if (*pilCount == 0)
	{
		ilMaxCount = psysDbStruct->lCntUsedLines;
	}
	else
	{
		ilMaxCount = *pilCount;
	}
	ilCount = 0;
	if (ilRc == RC_SUCCESS)
	{
		pclDbData = pclSMS + psysGlobal->lOffDbData +
					psysDbStruct->lOffStart;
		ilRc = RC_NOT_FOUND;
		ilI = 0;
		while (ilRc == RC_NOT_FOUND && ilI < psysDbStruct->lCntUsedLines)
		{
			ilFound = 0;
			for (ilJ = 0; ilJ < ilCnt; ilJ++)
			{
			   if (ilMaxCount == 1)
			   {
			      if (syslibCompare(&pclDbData[pFieldDesc[ilJ].ilOffStart],
				                pFieldDesc[ilJ].pclFieldValue,
						pFieldDesc[ilJ].ilLenField) == 0)
			      {
			         ilFound++;
			      }
			   }
			   else
			   {
			      if (syslibCompare(&pclDbData[pFieldDesc[ilJ].ilOffStart],
				                pFieldDesc[ilJ].pclFieldValue,
						pFieldDesc[ilJ].ilLenValue) == 0)
			      {
			         ilFound++;
			      }
			   }
/*
dbg(DEBUG,"%s,%s,%d,%d",&pclDbData[pFieldDesc[ilJ].ilOffStart],
		pFieldDesc[ilJ].pclFieldValue,
		pFieldDesc[ilJ].ilLenField,
		ilFound);
*/
			}
			if (ilFound == ilCnt)
			{
/*
				dbg(DEBUG,"syslibSearchDbData: DB record found");
*/
/*				snap(pclDbData,psysDbStruct->lLenLine,outp);	*/
				if (ilCntV > 0)
				{
					if (ilCount > 0)
					{
						if (strlen(pclLineSeperator) > 0)
						{
							strcat(pclTmpOutFieldValues,pclLineSeperator);
							pclTmpOutFieldValues += strlen(pclTmpOutFieldValues);
						}
						else
						{
							pclTmpOutFieldValues += strlen(pclTmpOutFieldValues) + 1;
							pclTmpOutFieldValues[0] = '\0';
						}
					}
					for (ilK = 0; ilK < ilCntV; ilK++)
					{
						memcpy(pclTmpData,
							   &pclDbData[pFieldDescV[ilK].ilOffStart],
							   pFieldDescV[ilK].ilLenField);
					    pclTmpData[pFieldDescV[ilK].ilLenField] = '\0';
/*						dbg(DEBUG,"syslibSearchDbData: <%s>",pclTmpData);	*/
						strcat(pclTmpOutFieldValues,pclTmpData);
						if (ilK < ilCntV-1)
						{
							strcat(pclTmpOutFieldValues,",");
						}
					}
				}
/*
				dbg(DEBUG,"syslibSearchDbData: Output <%s>",pclTmpOutFieldValues);
*/
				ilCount++;
				if (ilCount < ilMaxCount)
				{
					pclTmpOutFieldValues += strlen(pclTmpOutFieldValues);
					pclDbData += psysDbStruct->lLenLine;
				}
				else
				{
					ilRc = RC_SUCCESS;
				}
			}
			else
			{
				pclDbData += psysDbStruct->lLenLine;
			}
			ilI++;
		}
	}
	
	if (ilCount > 0)
	{
		ilRc = RC_SUCCESS;
	}
	*pilCount = ilCount;
/*
	dbg(DEBUG,"syslibSearchDbData: First record <%s>",pclOutFieldValues);
*/
	ilRc1 = syslibDetachSMS(pclControl,pclSMS);
	if (ilRc1 == RC_FAIL)
	{
		dbg(TRACE,"syslibSearchDbData: SHM detachment failed");
	}
/*
	else
	{
		dbg(DEBUG,"syslibSearchDbData: SHM detached");
	}
*/

	return ilRc;
}	/* end of syslibSearchDbData */


/*****************************************************************************/
/*                                                                           */
/*	G E T   F I E L D   L I S T                                              */
/*                                                                           */
/*****************************************************************************/

int syslibGetFieldList(SYSH_DBSTRUCT *psysDbStruct,
					   SYSH_FIELDSTRUCT *psysFieldStruct,
					   char *pclFieldList,char *pclFieldValues,
					   SYSH_FIELDLIST2 *pFieldDesc)
{
	int   ilRc = RC_SUCCESS;
	char  *pclTmp;
	int   ilSizeField;
	int   ilSizeValue;
	int   ilCnt;
	int   ilFound;
	int   ilI;
	int   ilJ;
	int   ilK;
	int   ilL;
	int   ilLen;

	ilCnt = 0;
	ilSizeField = strlen(pclFieldList);
	ilSizeValue = strlen(pclFieldValues);
	ilCnt = 0;
	ilI = 0;
	ilJ = 0;
	ilK = 0;
	ilL = 0;
	while (ilI < ilSizeField)
	{
		if (pclFieldList[ilI] == ',')
		{
			memset(pFieldDesc[ilCnt].pclFieldValue,' ',MAXFIELDSIZE);
			ilLen = ilI - ilJ;
			strncpy(pFieldDesc[ilCnt].pclFieldName,&pclFieldList[ilJ],ilLen);
			pFieldDesc[ilCnt].pclFieldName[ilLen] = '\0';
			pFieldDesc[ilCnt].ilLenField = -1;
			pFieldDesc[ilCnt].ilOffStart = -1;
			pFieldDesc[ilCnt].pclFieldValue[0] = '\0';
			pFieldDesc[ilCnt].ilLenValue = 0;
			ilJ = ilI + 1;
			ilFound = FALSE;
			if (strlen(pclFieldValues) > 0)
			{
				while (ilK < ilSizeValue && ilFound == FALSE)
				{
					if (pclFieldValues[ilK] == ',')
					{
						ilLen = ilK - ilL;
						strncpy(pFieldDesc[ilCnt].pclFieldValue,&pclFieldValues[ilL],ilLen);
						pFieldDesc[ilCnt].ilLenValue = ilLen;
						ilL = ilK + 1;
						ilFound = TRUE;
					}
					ilK++;
				}
			}
			ilCnt++;
		}
		ilI++;
	}
	memset(pFieldDesc[ilCnt].pclFieldValue,' ',MAXFIELDSIZE);
	ilLen = ilI - ilJ;
	pFieldDesc[ilCnt].pclFieldName[ilLen] = '\0';
	strncpy(pFieldDesc[ilCnt].pclFieldName,&pclFieldList[ilJ],ilLen);
	pFieldDesc[ilCnt].ilLenField = -1;
	pFieldDesc[ilCnt].ilOffStart = -1;
	if (strlen(pclFieldValues) > 0)
	{
		ilLen = ilSizeValue - ilL;
		strncpy(pFieldDesc[ilCnt].pclFieldValue,&pclFieldValues[ilL],ilLen);
		pFieldDesc[ilCnt].ilLenValue = ilLen;
	}
	else
	{
		pFieldDesc[ilCnt].pclFieldValue[0] = '\0';
		pFieldDesc[ilCnt].ilLenValue = 0;
	}
	ilCnt++;
	for (ilI = 0; ilI < psysDbStruct->lCntFields; ilI++)
	{
		for (ilJ = 0; ilJ < ilCnt; ilJ++)
		{
			if (strcmp(psysFieldStruct->pcFieldName,pFieldDesc[ilJ].pclFieldName) == 0)
			{
				pFieldDesc[ilJ].ilOffStart = psysFieldStruct->lOffStart;
				pFieldDesc[ilJ].ilLenField = psysFieldStruct->lLenField;
				pFieldDesc[ilJ].pclFieldValue[pFieldDesc[ilJ].ilLenValue] = ' ';
			}
		}
		pclTmp = (char *)psysFieldStruct;
		pclTmp += SYSH_FIELDSTRUCT_SIZE;
		psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;
	}
	for (ilI = 0; ilI < ilCnt; ilI++)
	{
/*
dbg(TRACE,"syslibGetFieldList: Field %d: <%s> <%d> <%d> <%d> <%16.16s>",
ilI+1,pFieldDesc[ilI].pclFieldName,
pFieldDesc[ilI].ilLenField,
pFieldDesc[ilI].ilLenValue,
pFieldDesc[ilI].ilOffStart,
pFieldDesc[ilI].pclFieldValue);
*/
		if (pFieldDesc[ilI].ilOffStart < 0)
		{
			dbg(TRACE,"syslibGetFieldList: Key does not exist <%s>",
				pFieldDesc[ilI].pclFieldName);
			ilCnt = -1;
			ilRc = RC_FAIL;
		}
	}

	return ilCnt;
}	/* end of syslibGetFieldList */


/*****************************************************************************/
/*                                                                           */
/*	R E L O A D   D B   D A T A                                              */
/*                                                                           */
/*****************************************************************************/

int syslibReloadDbData(char *pclTableNameP)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	int   ilDb = RC_SUCCESS;
	char  *pclControl;
	char  *pclControlTmp;
	char  *pclSMS;
	char  pclProject[32];
	SYSH_CONTROL *psysControl;
	SYSH_GLOBAL *psysGlobal;
	SYSH_DBSTRUCT *psysDbStruct;
	SYSH_FIELDSTRUCT *psysFieldStruct;
	char  *pclDbData;
	char  *pclDbDataTmp;
	char  *pclTmp;
	char  pclFieldList[1024];
	long  llOffStart;
	long  llLenField;
	char  pclSqlBuf[512];
	char  pclDataArea[4096];
	short slCursor;
	short slFkt;
	int   ilFieldSize[256];
	char  pclDbLine[4096];
	char  *pclDbLineTmp;
	int   ilSize;
	int   ilCnt;
	int   ilI;
	int   ilJ;
	char	pclTableName[33];

	/* Quoted strings in parameters causes runtime crash (read-only) */
	
	sprintf(pclTableName,"%s",pclTableNameP);

	ilRc = iStrup(pclTableName);

	strcpy(pclProject,&pclTableName[strlen(pclTableName)-3]);
	ilRc = syslibAttachSMS(pclProject,&pclControl,&pclSMS);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"syslibReloadDbData: SHM attachment failed");
		return RC_FAIL;
	}

	psysControl = (SYSH_CONTROL *)pclControl;
	psysGlobal = (SYSH_GLOBAL *)pclSMS;
	pclTmp = pclSMS + psysGlobal->lOffDbStruct;
	psysDbStruct = (SYSH_DBSTRUCT *)pclTmp;
	pclTmp = pclSMS + psysGlobal->lOffFieldStruct;
	psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;

	ilI = 0;
	ilRc = RC_FAIL;
	while (ilRc == RC_FAIL && ilI < psysGlobal->lCntTables)
	{
		if (strcmp(psysDbStruct->pcTabName,pclTableName) == 0)
		{
			ilRc = RC_SUCCESS;
		}
		else
		{
			pclTmp = (char *)psysDbStruct;
			pclTmp += SYSH_DBSTRUCT_SIZE;
			psysDbStruct = (SYSH_DBSTRUCT *)pclTmp;
		}
		ilI++;
	}
	if (ilRc == RC_FAIL)
	{
		dbg(DEBUG,"syslibReloadDbData: Table not found");
		ilRc1 = syslibDetachSMS(pclControl,pclSMS);
		if (ilRc1 == RC_FAIL)
		{
			dbg(TRACE,"syslibReloadDbData: SHM detachment failed");
		}
		return RC_SUCCESS;
	}
	else
	{
		sprintf(pclSqlBuf,"SELECT COUNT(*) FROM %s",pclTableName);
		slCursor = 0;
		ilDb = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
		close_my_cursor(&slCursor);
		if (ilDb == RC_SUCCESS)
		{
			ilCnt = atoi(pclDataArea);
/*
			dbg(DEBUG,"syslibReloadDbData: %d Records in Table %s",
				ilCnt,pclTableName);
*/
		}
		else
		{
			dbg(TRACE,"syslibReloadDbData: Error reading %s : %d",
				pclTableName,ilDb);
			ilRc = RC_FAIL;
		}
		pclTmp = (char *)psysFieldStruct;
		pclTmp += (SYSH_FIELDSTRUCT_SIZE * psysDbStruct->lOffFieldStruct);
		psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;
		pclFieldList[0] = '\0';
		for (ilI = 0; ilI < psysDbStruct->lCntFields; ilI++)
		{
			strcat(pclFieldList,",");
			strcat(pclFieldList,psysFieldStruct->pcFieldName);
			ilFieldSize[ilI] = psysFieldStruct->lLenField;
			pclTmp = (char *)psysFieldStruct;
			pclTmp += SYSH_FIELDSTRUCT_SIZE;
			psysFieldStruct = (SYSH_FIELDSTRUCT *)pclTmp;
		}
		pclFieldList[0] = ' ';
		sprintf(pclSqlBuf,"SELECT%s FROM %s",
				pclFieldList,pclTableName);
	}

	if (ilRc == RC_SUCCESS)
	{
		if (ilCnt <= psysDbStruct->lCntTotLines)
		{
			ilRc = syslibSetSemaphore(psysControl->lSEMId,1);
			if (ilRc == RC_FAIL)
			{
				dbg(TRACE,"syslibReloadDbData: Set semaphore has failed");
			}
			pclDbData = pclSMS + psysGlobal->lOffDbData +
						psysDbStruct->lOffStart;
			pclDbDataTmp = pclDbData;
			psysDbStruct->lCntUsedLines = 0;
			memset(pclDbData,'\0',psysDbStruct->lCntTotLines * psysDbStruct->lLenLine);
			ilDb = RC_SUCCESS;
			slCursor = 0;
			slFkt = START;
			while (ilDb == RC_SUCCESS)
			{
				ilDb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataArea);
				if (ilDb == RC_SUCCESS)
				{
					pclDbLineTmp = pclDbLine;
					for (ilJ = 0; ilJ < psysDbStruct->lCntFields; ilJ++)
					{
						get_fld(pclDataArea,(short)ilJ,STR,
								ilFieldSize[ilJ],pclDbLineTmp);
						pclDbLineTmp += ilFieldSize[ilJ];
					}
					memcpy(pclDbData,pclDbLine,psysDbStruct->lLenLine);
					psysGlobal->lUpdated = time(0L);
					psysDbStruct->lCntUsedLines++;
					pclDbData += psysDbStruct->lLenLine;
					slFkt = NEXT;
				}
			}
			close_my_cursor(&slCursor);
			ilRc = syslibClearSemaphore(psysControl->lSEMId,1);
			if (ilRc == RC_FAIL)
			{
				dbg(TRACE,"syslibReloadDbData: Clear semaphore has failed");
			}
/*			snap((char *)psysDbStruct,SYSH_DBSTRUCT_SIZE,outp);	*/
/*			snap(pclDbDataTmp,psysDbStruct->lCntUsedLines*psysDbStruct->lLenLine+4,outp);	*/
		}
		else
		{	/* reorg shared memory */
			dbg(DEBUG,"syslibReloadDbData: Reorg shared memory");
			ilRc = syslibReorgSMSInt(pclControl,&pclSMS,pclProject);
/*			snap(pclControl,SYSH_CONTROL_SIZE,outp);	*/
/*			snap(pclSMS,SYSH_GLOBAL_SIZE,outp);	*/
		}
	}	

	ilRc1 = syslibDetachSMS(pclControl,pclSMS);
	if (ilRc1 == RC_FAIL)
	{
		dbg(TRACE,"syslibReloadDbData: SHM detachment failed");
	}

	return ilRc;
}	/* end of syslibReloadDbData */


/*****************************************************************************/
/*                                                                           */
/*	I N T E R N A L   R E O R G   O F   S H M                                */
/*                                                                           */
/*****************************************************************************/

int syslibReorgSMSInt(char *pclControl,char **pclSMS,char *pclProject)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	char  *pclControlTmp;
	int   ilCnt;
	char  *pclSMSReorg;
	SYSH_CONTROL *psysControl;

	dbg(DEBUG,"syslibReorgSMSInt: Reorg shared memory");
	pclSMSReorg = *pclSMS;
	psysControl = (SYSH_CONTROL *)pclControl;
/*	snap(pclControl,SYSH_CONTROL_SIZE,outp);	*/
/*	snap(pclSMSReorg,SYSH_GLOBAL_SIZE,outp);	*/
	ilRc = syslibSetSemaphore(psysControl->lSEMId,0);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibReorgSMSInt: Set semaphore has failed");
		return RC_FAIL;
	}
	if (psysControl->lReorg > SYSLIB_CREATE)
	{
		dbg(DEBUG,"syslibReorgSMSInt: Reorg already in progress");
		ilRc = syslibClearSemaphore(psysControl->lSEMId,0);
		return ilRc;
	}
	psysControl->lReorg = SYSLIB_REORG;
	ilRc = syslibClearSemaphore(psysControl->lSEMId,0);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibReorgSMSInt: Clear semaphore has failed");
		return RC_FAIL;
	}
	ilCnt = 0;
	while (psysControl->lCurrentUser > 1 && ilCnt < MAX_WAIT)
	{
		sleep(1);
		ilCnt++;
	}
	if (ilCnt == MAX_WAIT)
	{
		dbg(TRACE,"syslibReorgSMSInt: SHM is permanently used");
		ilRc1 = send_message(IPRIO_ADMIN,SYSLIB_SSM_USED_PERM,0,0,pclProject);
		psysControl->lReorg = SYSLIB_CREATE;
		ilRc = RC_FAIL;
	}
	if (ilRc != RC_FAIL)
	{
		ilRc = shmdt(pclSMSReorg);
		if (ilRc == RC_FAIL)
		{
			dbg(TRACE,"syslibReorgSMSInt: SHM detachment failed");
			ilRc = RC_FAIL;
		}
		ilRc = shmctl(psysControl->lSSMId,IPC_RMID,NULL);
		if (ilRc == RC_FAIL)
		{
			dbg(TRACE,"syslibReorgSMSInt: SHM deletion failed");
			psysControl->lReorg = SYSLIB_CREATE;
			ilRc = RC_FAIL;
		}
	}
	if (ilRc != RC_FAIL)
	{
		ilRc = syslibBuildSMS(pclProject,SYSLIB_REORG,
							  &pclControlTmp,&pclSMSReorg,
							  &psysControl->lSSMId);
		dbg(DEBUG,"syslibCreateSMS: return from syslibBuildSMS = %d",ilRc);
		psysControl->lReorg = SYSLIB_CREATE;
		*pclSMS = pclSMSReorg;
		if (ilRc == RC_SUCCESS)
		{
			ilRc1 = send_message(IPRIO_ADMIN,SYSLIB_SSM_REORG_OK,0,0,pclProject);
		}
		else
		{
			ilRc1 = send_message(IPRIO_ADMIN,SYSLIB_SSM_REORG_FAILED,0,0,pclProject);
		}
	}
/*	snap(pclControl,SYSH_CONTROL_SIZE,outp);	*/
/*	snap(pclSMSReorg,SYSH_GLOBAL_SIZE,outp);	*/

	return ilRc;
}	/* end of syslibReorgSMSInt */


/*****************************************************************************/
/*                                                                           */
/*	R E O R G   S H A R E D   M E M O R Y                                    */
/*                                                                           */
/*****************************************************************************/

int syslibReorgSMS(char *pclTableP)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	char  *pclControl;
	char  *pclSMS;
	char	pclTable[33];

	/* Quoted strings in parameters causes runtime crash (read-only) */
	
	sprintf(pclTable,"%s",pclTableP);

	ilRc = iStrup(pclTable);

	ilRc = syslibAttachSMS(pclTable,&pclControl,&pclSMS);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"syslibReorgSMS: SHM attachment failed");
		return RC_FAIL;
	}

	ilRc = syslibReorgSMSInt(pclControl,&pclSMS,pclTable);
/*	snap(pclControl,SYSH_CONTROL_SIZE,outp);	*/
/*	snap(pclSMS,SYSH_GLOBAL_SIZE,outp);	*/
	
	ilRc1 = syslibDetachSMS(pclControl,pclSMS);
	if (ilRc1 == RC_FAIL)
	{
		dbg(TRACE,"syslibReorgSMS: SHM detachment failed");
	}

	return ilRc;
}	/* end of syslibReorgSMS */


/*****************************************************************************/
/*                                                                           */
/*	S E T   S E M A P H O R E                                                */
/*                                                                           */
/*****************************************************************************/

int syslibSetSemaphore(int ilSemId, int ilSemNum)
{
	int   ilRc = RC_SUCCESS;
	SYSH_SEMBUF psysSembuf;
	unsigned short sems[2];
	union semun ulSemHelp;

	ulSemHelp.array = sems;
	ilRc = semctl(ilSemId,0,GETALL,ulSemHelp);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibSetSemaphore: Reading of semaphore values failed");
		dbg(TRACE,"syslibSetSemaphore: ERRNO = %d , <%s>",errno,strerror(errno));
	}

	psysSembuf.sem_num = ilSemNum;
	psysSembuf.sem_op = -1;
	psysSembuf.sem_flg = SEM_UNDO;
	ilRc = semop(ilSemId,(struct sembuf *)&psysSembuf,1);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibSetSemaphore: Set semaphore has failed");
		return RC_FAIL;
	}

	ilRc = semctl(ilSemId,0,GETALL,ulSemHelp);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibSetSemaphore: Reading of semaphore values failed");
		dbg(TRACE,"syslibSetSemaphore: ERRNO = %d , <%s>",errno,strerror(errno));
	}

	return ilRc;
}	/* end of syslibSetSemaphore */


/*****************************************************************************/
/*                                                                           */
/*	C L E A R   S E M A P H O R E                                            */
/*                                                                           */
/*****************************************************************************/

int syslibClearSemaphore(int ilSemId, int ilSemNum)
{
	int   ilRc = RC_SUCCESS;
	SYSH_SEMBUF psysSembuf;
	unsigned short sems[2];
	union semun ulSemHelp;

	ulSemHelp.array = sems;
	ilRc = semctl(ilSemId,0,GETALL,ulSemHelp);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibClearSemaphore: Reading of semaphore values failed");
		dbg(TRACE,"syslibClearSemaphore: ERRNO = %d , <%s>",errno,strerror(errno));
	}
	psysSembuf.sem_num = ilSemNum;
	psysSembuf.sem_op = 1;
	psysSembuf.sem_flg = SEM_UNDO;
	ilRc = semop(ilSemId,(struct sembuf *)&psysSembuf,1);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibClearSemaphore: Clear semaphore has failed");
		return RC_FAIL;
	}

	ilRc = semctl(ilSemId,0,GETALL,ulSemHelp);
	if (ilRc == RC_FAIL)
	{
		dbg(TRACE,"syslibClearSemaphore: Reading of semaphore values failed");
		dbg(TRACE,"syslibClearSemaphore: ERRNO = %d , <%s>",errno,strerror(errno));
	}

	return ilRc;
}	/* end of syslibClearSemaphore */



/*****************************************************************************/
/*                                                                           */
/*	S E A R C H   S Y S T A B   D A T A                                      */
/*                                                                           */
/*****************************************************************************/

int syslibSearchSystabData(char *pclProjectP,
		   char *pclInFieldList,char *pclInFieldValues,
		   char *pclOutFieldList,char *pclOutFieldValues,
		   int *pilCount,char *pclLineSeperator)
{
	int   ilRc = RC_SUCCESS;
	int   ilRc1 = RC_SUCCESS;
	char  *pclControl;
	char  *pclSMS;
	SYSH_CONTROL *psysControl;
	SYSH_GLOBAL *psysGlobal;
	SYSH_SYSTAB *psysSystab;
	char  *pclSystabData;
	char  *pclTmp;
	char  pclTmpData[4096];
	SYSH_FIELDLIST pFieldDesc[SYSH_NO_SYSTAB_FIELDS];
	SYSH_FIELDLIST pFieldDescV[SYSH_NO_SYSTAB_FIELDS];
	int   ilCnt;
	int   ilCntV;
	int   ilCount;
	int   ilFound;
	int   ilI;
	int   ilJ;
	int   ilK;
	char  *pclTmpInFieldList;
	char  *pclTmpInFieldValues;
	char  *pclTmpOutFieldList;
	char  *pclTmpOutFieldValues;
	char	pclProject[33];

	/* Quoted strings in parameters causes runtime crash (read-only) */

	sprintf(pclProject,"%s",pclProjectP);

	ilRc = iStrup(pclProject);
	
	/* ilRc = iStrup(pclInFieldList);
	ilRc = iStrup(pclOutFieldList); */

	pclTmpInFieldList = pclInFieldList;
	pclTmpInFieldValues = pclInFieldValues;
	pclTmpOutFieldList = pclOutFieldList;
	pclTmpOutFieldValues = pclOutFieldValues;
	ilRc = syslibAttachSMS(pclProject,&pclControl,&pclSMS);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"syslibSearchSystabData: SHM attachment failed");
		return RC_FAIL;
	}

	psysGlobal = (SYSH_GLOBAL *)pclSMS;
	pclTmp = pclSMS + psysGlobal->lOffSystab;
	psysSystab = (SYSH_SYSTAB *)pclTmp;

	if (pclTmpInFieldList == NULL || pclTmpInFieldValues == NULL)
	{
		ilCnt = 0;
	}
	else
	{
		ilCnt = syslibGetSystabFieldList(pclTmpInFieldList,
				   		 pclTmpInFieldValues,pFieldDesc);
	}
	ilCntV = syslibGetSystabFieldList(pclTmpOutFieldList,"",
					  pFieldDescV);
	if (ilCnt < 0)
	{
		ilRc = RC_FAIL;
	}
	if (ilCntV < 0)
	{
		ilRc = RC_FAIL;
	}

	pclTmpOutFieldValues[0] = '\0';
	ilCount = 0;
	if (ilRc == RC_SUCCESS)
	{
		pclSystabData = pclSMS + psysGlobal->lOffSystab;
		ilRc = RC_NOT_FOUND;
		ilI = 0;
		while (ilRc == RC_NOT_FOUND && ilI < psysGlobal->lCntSystab)
		{
			ilFound = 0;
			for (ilJ = 0; ilJ < ilCnt; ilJ++)
			{
				if (strncmp(&pclSystabData[pFieldDesc[ilJ].ilOffStart],
							pFieldDesc[ilJ].pclFieldValue,
							strlen(pFieldDesc[ilJ].pclFieldValue)) == 0)
				{
					ilFound++;
				}
			}
			if (ilFound == ilCnt)
			{
/*
				dbg(DEBUG,"syslibSearchSystabData: SYSTAB record found");
*/
/*				snap(pclSystabData,SYSH_SYSTAB_SIZE,outp);	*/
				if (ilCntV > 0)
				{
					if (ilCount > 0)
					{
						if (strlen(pclLineSeperator) > 0)
						{
							strcat(pclTmpOutFieldValues,pclLineSeperator);
							pclTmpOutFieldValues += strlen(pclTmpOutFieldValues);
						}
						else
						{
							pclTmpOutFieldValues += strlen(pclTmpOutFieldValues) + 1;
							pclTmpOutFieldValues[0] = '\0';
						}
					}
					for (ilK = 0; ilK < ilCntV; ilK++)
					{
						memcpy(pclTmpData,
							   &pclSystabData[pFieldDescV[ilK].ilOffStart],
							   pFieldDescV[ilK].ilLenField);
					    pclTmpData[pFieldDescV[ilK].ilLenField] = '\0';
/*						dbg(DEBUG,"syslibSearchSystabData: <%s>",pclTmpData); */
						strcat(pclTmpOutFieldValues,pclTmpData);
						if (ilK < ilCntV-1)
						{
							strcat(pclTmpOutFieldValues,",");
						}
					}
				}
/*				dbg(DEBUG,"syslibSearchSystabData: Output <%s>",pclTmpOutFieldValues);	*/
				ilCount++;
				if (*pilCount == 0)
				{
					pclTmpOutFieldValues += strlen(pclTmpOutFieldValues);
					pclSystabData += SYSH_SYSTAB_SIZE;
				}
				else
				{
					ilRc = RC_SUCCESS;
				}
			}
			else
			{
				pclSystabData += SYSH_SYSTAB_SIZE;
			}
			ilI++;
		}
	}
	
	if (ilCount > 0)
	{
		ilRc = RC_SUCCESS;
	}
	*pilCount = ilCount;
/*
	dbg(DEBUG,"syslibSearchSystabData: First record <%s>",pclOutFieldValues);
*/
	ilRc1 = syslibDetachSMS(pclControl,pclSMS);
	if (ilRc1 == RC_FAIL)
	{
		dbg(TRACE,"syslibSearchSystabData: SHM detachment failed");
	}

	return ilRc;
}	/* end of syslibSearchSystabData */


/*****************************************************************************/
/*                                                                           */
/*	G E T   S Y S T A B   F I E L D   L I S T                                */
/*                                                                           */
/*****************************************************************************/

int syslibGetSystabFieldList(char *pclFieldList,char *pclFieldValues,
						     SYSH_FIELDLIST *pFieldDesc)
{
	int   ilRc = RC_SUCCESS;
	char  *pclTmp;
	int   ilSizeField;
	int   ilSizeValue;
	int   ilCnt;
	int   ilFound;
	int   ilI;
	int   ilJ;
	int   ilK;
	int   ilL;

	ilCnt = 0;
	ilSizeField = strlen(pclFieldList);
	ilSizeValue = strlen(pclFieldValues);
	ilCnt = 0;
	ilI = 0;
	ilJ = 0;
	ilK = 0;
	ilL = 0;
	while (ilI < ilSizeField)
	{
		if (pclFieldList[ilI] == ',')
		{
			pclFieldList[ilI] = '\0';
			strcpy(pFieldDesc[ilCnt].pclFieldName,&pclFieldList[ilJ]);
			pFieldDesc[ilCnt].ilLenField = -1;
			pFieldDesc[ilCnt].ilOffStart = -1;
			pFieldDesc[ilCnt].pclFieldValue = NULL;
			ilJ = ilI + 1;
			ilFound = FALSE;
			if (strlen(pclFieldValues) > 0)
			{
				while (ilK < ilSizeValue && ilFound == FALSE)
				{
					if (pclFieldValues[ilK] == ',')
					{
						pclFieldValues[ilK] = '\0';
						pFieldDesc[ilCnt].pclFieldValue = &pclFieldValues[ilL];
						ilL = ilK + 1;
						ilFound = TRUE;
					}
					ilK++;
				}
			}
			ilCnt++;
		}
		ilI++;
	}
	strcpy(pFieldDesc[ilCnt].pclFieldName,&pclFieldList[ilJ]);
	pFieldDesc[ilCnt].ilLenField = -1;
	pFieldDesc[ilCnt].ilOffStart = -1;
	if (strlen(pclFieldValues) > 0)
	{
		pFieldDesc[ilCnt].pclFieldValue = &pclFieldValues[ilL];
	}
	else
	{
		pFieldDesc[ilCnt].pclFieldValue = NULL;
	}
	ilCnt++;
	for (ilI = 0; ilI < SYSH_NO_SYSTAB_FIELDS; ilI++)
	{
		for (ilJ = 0; ilJ < ilCnt; ilJ++)
		{
			if (strcmp(pSystabDesc[ilI].pclFieldName,pFieldDesc[ilJ].pclFieldName) == 0)
			{
				pFieldDesc[ilJ].ilOffStart = pSystabDesc[ilI].ilOffStart;
				pFieldDesc[ilJ].ilLenField = pSystabDesc[ilI].ilLenField;
			}
		}
	}
	for (ilI = 0; ilI < ilCnt; ilI++)
	{
/*
		dbg(TRACE,"syslibGetSystabFieldList: Field %d: <%s> <%d> <%d> <%s>",
			ilI+1,pFieldDesc[ilI].pclFieldName,
			pFieldDesc[ilI].ilLenField,
			pFieldDesc[ilI].ilOffStart,
			pFieldDesc[ilI].pclFieldValue);
*/
		if (pFieldDesc[ilI].ilOffStart < 0)
		{
			dbg(TRACE,"syslibGetSystabFieldList: Key does not exist <%s>",
				pFieldDesc[ilI].pclFieldName);
			ilCnt = -1;
			ilRc = RC_FAIL;
		}
	}

	return ilCnt;
}	/* end of syslibGetSystabFieldList */


/*****************************************************************************/
/*                                                                           */
/*	C O M P A R E   S T R I N G S                                        */	
/*                                                                           */
/*****************************************************************************/

int syslibCompare(char *pclVal1,char *pclVal2,int ilLen)
{
	int   ilI;

	for (ilI = 0; ilI < ilLen; ilI++)
	{
		if (pclVal1[ilI] != pclVal2[ilI])
		{
			return 1;
		}
	}
	return 0;
}	/* end of syslibCompare */

