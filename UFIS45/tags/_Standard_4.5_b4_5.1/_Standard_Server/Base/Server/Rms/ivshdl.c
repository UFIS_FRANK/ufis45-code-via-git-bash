#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Rms/ivshdl.c 1.1 2003/03/19 16:55:25SGT fei Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : Frank Scheibel                                            */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS 4.4 (c) ABB AAT/I ivshdl.c 44.1.6/ 01/06/05 / FSC";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "fditools.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>
#include <ctype.h>


#ifndef min
#define min(a,b) (a < b ? a : b)
#endif

#define CMD_IVS     (3)
#define CMD_IRT     (4)
#define CMD_URT     (5)
#define CMD_DRT     (6)

#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))
#define max(val1, val2)	((val1 > val2)?val1:val2)


#define JOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define POOLJOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPoolJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define DRRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgDrrArray.plrArrayFieldOfs[ipFieldNo-1]])
#define ALOFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAloArray.plrArrayFieldOfs[ipFieldNo-1]])
#define JTYFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgJtyArray.plrArrayFieldOfs[ipFieldNo-1]])
#define AFTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAftArray.plrArrayFieldOfs[ipFieldNo-1]])
#define STFFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgStfArray.plrArrayFieldOfs[ipFieldNo-1]])
#define IVSFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgIvsArray.plrArrayFieldOfs[ipFieldNo-1]])
#define POLFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPolArray.plrArrayFieldOfs[ipFieldNo-1]])

#define CGVFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgCgvArray.plrArrayFieldOfs[ipFieldNo-1]])

#define LIDSTATFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgLidStatArray.plrArrayFieldOfs[ipFieldNo-1]])
#define LIDMSGFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgLidMsgArray.plrArrayFieldOfs[ipFieldNo-1]])
#define MENUFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgMenuArray.plrArrayFieldOfs[ipFieldNo-1]])
#define INFJOBFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgInfJobArray.plrArrayFieldOfs[ipFieldNo-1]])
#define PATTERNFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPatternArray.plrArrayFieldOfs[ipFieldNo-1]])
#define PATTERNTYPEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgPatternTypeArray.plrArrayFieldOfs[ipFieldNo-1]])
#define FIELDSFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgFieldsArray.plrArrayFieldOfs[ipFieldNo-1]])

extern int get_no_of_items(char *s);

#define ARR_NAME_LEN           (28)
#define ARR_FLDLST_LEN         (512)
#define JTYPLEN			8
#define	ALOCLEN			10

static char *prgCmdTxt[] = {    "IVS",    "IRT",    "URT",    "DRT", NULL };
static int    rgCmdDef[] = { CMD_IVS , CMD_IRT , CMD_URT , CMD_DRT ,    0 };


/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/



#define JOB_SORTFIELDS "JOUR,ACFR,ACTO"
#define JOB_SORTDIR "D,A,A"
#define JOB_SORTFIELDS2 "URNO,ACFR,ACTO"
#define JOB_SORTDIR2 "D,D,D"
#define JOB_SORTFIELDS3 "USTF,ACFR,ACTO"
#define JOB_SORTDIR3 "D,A,A"
#define JOB_FIELDS "ACFR,ACTO,DETY,HOPO,JOUR,PLFR,PLTO,STAT,TEXT,UAFT,UAID,UALO,UJTY,URNO,USTF"
#define JOB_FIELDS2 "ACFR,ACTO,DETY,HOPO,JOUR,PLFR,PLTO,STAT,TEXT,UAFT,UAID,UALO,UJTY,URNO,USTF,ALID,ALOC,ALCX,FLTN,JTYP"
#define JOB_ADDFIELDS "ALID,ALOC,ALCX,FLTN,JTYP"

#define POOLJOB_SORTFIELDS "URNO"
#define POOLJOB_SORTDIR "D"
#define POOLJOB_SORTFIELDS2 "UDSR,ACFR,ACTO"
#define POOLJOB_SORTDIR2 "D,A,A"
#define POOLJOB_SORTFIELDS3 "UDSR,ACFR,CDAT"
#define POOLJOB_SORTDIR3 "D,D,D"

#define POOLJOB_FIELDS "ACFR,ACT3,ACTO,CDAT,DETY,GATE,HOPO,JOUR,LSTU,PLFR,PLTO,POSI,REGN,STAT,TEXT,UAFT,UAID,UALO,UDEL,UDRD,UDSR,UJTY,URNO,USEC,USEU,USTF,TTGF,TTGT"

#define ALO_SORTFIELDS "URNO"
#define ALO_SORTDIR "D"
#define ALO_SORTFIELDS2 "ALOC"
#define ALO_SORTDIR2 "D"
#define ALO_FIELDS "URNO,REFT,ALOT,ALOC,HOPO"

#define JTY_SORTFIELDS "NAME"
#define JTY_SORTDIR "D"
#define JTY_SORTFIELDS2 "URNO"
#define JTY_SORTDIR2 "D"
#define JTY_FIELDS "NAME,URNO,HOPO"

#define DRR_SORTFIELDS "URNO"
#define DRR_SORTDIR "D"
#define DRR_SORTFIELDS2 "STFU,AVFR"
#define DRR_SORTDIR2 "D,A"
#define DRR_SORTFIELDS3 "PENO,AVFR"
#define DRR_SORTDIR3 "D,D"
#define DRR_FIELDS "BSDU,URNO,DRSF,DRRN,SDAY,STFU,AVFR,AVTO,ROSS,ROSL,HOPO"
#define DRR_FIELDS2 "BSDU,URNO,DRSF,DRRN,SDAY,STFU,AVFR,AVTO,ROSS,ROSL,HOPO,PENO"
#define DRR_ADDFIELDS "PENO"

#define AFT_SORTFIELDS "URNO"
#define AFT_SORTDIR "D"
#define AFT_FIELDS "FLTN,URNO,ALC2,ALC3,HOPO"

#define STF_SORTFIELDS "PENO"
#define STF_SORTDIR "D"
#define STF_FIELDS "FINM,URNO,LANM,PENO,HOPO"

#define IVS_SORTFIELDS "TKEY"
#define IVS_SORTDIR "D"
#define IVS_FIELDS "TKEY,CODE,TEXT,URNO,HOPO"

#define POL_SORTFIELDS "URNO"
#define POL_SORTDIR "D"
#define POL_FIELDS "NAME,URNO,HOPO,DTEL"

#define CGV_SORTFIELDS "URNO"
#define CGV_SORTDIR "D"
#define CGV_FIELDS "NAME,URNO,HOPO"

#define LIDSTAT_SORTFIELDS "LIDN"
#define LIDSTAT_SORTDIR "D"
#define LIDSTAT_FIELDS "PENO,USTF,UDSR,UJOB,LIDN,STAT,MSTA,UJBT"

#define LIDMSG_SORTFIELDS "LIDN"
#define LIDMSG_SORTDIR "D"
#define LIDMSG_FIELDS "MSGT,LIDN"

#define MENU_SORTFIELDS "LIDN"
#define MENU_SORTDIR "D"
#define	MENU_FIELDS "MTXT,LIDN,PVAL"

#define INFJOB_SORTFIELDS "UJOB,CFLG"
#define INFJOB_SORTDIR "D,D"
#define	INFJOB_FIELDS "UJOB,CFLG"

#define PATTERN_SORTFIELDS "JTYP,ALOC"
#define PATTERN_SORTDIR "D,D"
#define	PATTERN_FIELDS "JTYP,ALOC,TEXT"

#define PATTERNTYPE_SORTFIELDS "JTYP,ALOC"
#define PATTERNTYPE_SORTDIR "D,D"
#define	PATTERNTYPE_FIELDS "JTYP,ALOC,TEXT"

#define FIELDS_SORTFIELDS "FNAM"
#define FIELDS_SORTDIR "D"
#define FIELDS_FIELDS "FNAM"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static EVENT  *prgOutEvent      = NULL;
struct _arrayinfo
{
  HANDLE   rrArrayHandle;
  char     crArrayName[ARR_NAME_LEN+1];
  char     crArrayFieldList[ARR_FLDLST_LEN+1];
  long     lrArrayFieldCnt;
  char    *pcrArrayRowBuf;
  long     lrArrayRowLen;
  long    *plrArrayFieldOfs;
  long    *plrArrayFieldLen;
  HANDLE   rrIdx01Handle;
  char     crIdx01Name[ARR_NAME_LEN+1];
  char     crIdx01FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx01FieldCnt;
  char    *pcrIdx01RowBuf;
  long     lrIdx01RowLen;
  long    *plrIdx01FieldPos;
  long    *plrIdx01FieldOrd;
   HANDLE   rrIdx02Handle;
  char     crIdx02Name[ARR_NAME_LEN+1];
  char     crIdx02FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx02FieldCnt;
  char    *pcrIdx02RowBuf;
  long     lrIdx02RowLen;
  long    *plrIdx02FieldPos;
  long    *plrIdx02FieldOrd;
   HANDLE   rrIdx03Handle;
  char     crIdx03Name[ARR_NAME_LEN+1];
  char     crIdx03FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx03FieldCnt;
  char    *pcrIdx03RowBuf;
  long     lrIdx03RowLen;
  long    *plrIdx03FieldPos;
  long    *plrIdx03FieldOrd;
   HANDLE   rrIdx04Handle;
  char     crIdx04Name[ARR_NAME_LEN+1];
  char     crIdx04FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx04FieldCnt;
  char    *pcrIdx04RowBuf;
  long     lrIdx04RowLen;
  long    *plrIdx04FieldPos;
  long    *plrIdx04FieldOrd;
   HANDLE   rrIdx05Handle;
  char     crIdx05Name[ARR_NAME_LEN+1];
  char     crIdx05FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx05FieldCnt;
  char    *pcrIdx05RowBuf;
  long     lrIdx05RowLen;
  long    *plrIdx05FieldPos;
  long    *plrIdx05FieldOrd;
   HANDLE   rrIdx06Handle;
  char     crIdx06Name[ARR_NAME_LEN+1];
  char     crIdx06FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx06FieldCnt;
  char    *pcrIdx06RowBuf;
  long     lrIdx06RowLen;
  long    *plrIdx06FieldPos;
  long    *plrIdx06FieldOrd;
   HANDLE   rrIdx07Handle;
  char     crIdx07Name[ARR_NAME_LEN+1];
  char     crIdx07FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx07FieldCnt;
  char    *pcrIdx07RowBuf;
  long     lrIdx07RowLen;
  long    *plrIdx07FieldPos;
  long    *plrIdx07FieldOrd;
   HANDLE   rrIdx08Handle;
  char     crIdx08Name[ARR_NAME_LEN+1];
  char     crIdx08FieldList[ARR_FLDLST_LEN+1];
  long     lrIdx08FieldCnt;
  char    *pcrIdx08RowBuf;
  long     lrIdx08RowLen;
  long    *plrIdx08FieldPos;
  long    *plrIdx08FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgJobArray;
static ARRAYINFO rgPoolJobArray;
static ARRAYINFO rgJtyArray;
static ARRAYINFO rgDrrArray;
static ARRAYINFO rgAloArray;
static ARRAYINFO rgAftArray;
static ARRAYINFO rgStfArray;
static ARRAYINFO rgIvsArray;
static ARRAYINFO rgPolArray;

static ARRAYINFO rgCgvArray;

static ARRAYINFO rgLidStatArray;
static ARRAYINFO rgLidMsgArray;
static ARRAYINFO rgMenuArray;
static ARRAYINFO rgInfJobArray;
static ARRAYINFO rgPatternArray;
static ARRAYINFO rgPatternTypeArray;
static ARRAYINFO rgFieldsArray;

static long  lgEvtCnt      = 0;


static int igPoolJobAcfr;
static int igPoolJobAct3;
static int igPoolJobActo;
static int igPoolJobCdat;
static int igPoolJobDety;
static int igPoolJobGate;
static int igPoolJobHopo;
static int igPoolJobJour;
static int igPoolJobLstu;
static int igPoolJobPlfr;
static int igPoolJobPlto;
static int igPoolJobPosi;
static int igPoolJobRegn;
static int igPoolJobStat;
static int igPoolJobText;
static int igPoolJobUaft;
static int igPoolJobUaid;
static int igPoolJobUalo;
static int igPoolJobUdel;
static int igPoolJobUdsr;
static int igPoolJobUdrd;
static int igPoolJobUjty;
static int igPoolJobUrno;
static int igPoolJobUsec;
static int igPoolJobUseu;
static int igPoolJobUstf;
static int igPoolJobTtgf;
static int igPoolJobTtgt;


static int igJobAcfr;
static int igJobActo;
static int igJobDety;
static int igJobHopo;
static int igJobJour;
static int igJobPlfr;
static int igJobPlto;
static int igJobStat;
static int igJobText;
static int igJobUaft;
static int igJobUaid;
static int igJobUalo;
static int igJobUjty;
static int igJobUrno;
static int igJobUstf;
static int igJobAlid;
static int igJobAloc;
static int igJobAlcx;
static int igJobFltn;
static int igJobJtyp;
	
static int igAloUrno;
static int igAloReft;
static int igAloAlot;
static int igAloAloc;

static int igJtyUrno;
static int igJtyName;

static int igIvsTkey;
static int igIvsText;
static int igIvsUrno;
static int igIvsCode;
static int igIvsHopo;

static int igPolDtel;
static int igPolUrno;
static int igPolName;
static int igPolHopo;
	
static int igCgvName;
static int igCgvUrno;
static int igCgvHopo;

static int igDrrBsdu;
static int igDrrDrsf;
static int igDrrUrno;
static int igDrrDrrn;
static int igDrrSday;
static int igDrrStfu;
static int igDrrAvfr;
static int igDrrAvto;
static int igDrrRosl;
static int igDrrRoss;
static int igDrrPeno;

static int igAftUrno;
static int igAftFltn;
static int igAftAlc2;
static int igAftAlc3;

static int igStfFinm;
static int igStfLanm;
static int igStfPeno;
static int igStfUrno;

static int igLidStatLidn;
static int igLidStatStat;
static int igLidStatMsta;
static int igLidStatPeno;
static int igLidStatUstf;
static int igLidStatUdsr;
static int igLidStatUjob;
static int igLidStatUjbt;

static int igLidMsgLidn;
static int igLidMsgMsgt;

static int igMenuLidn;
static int igMenuMtxt;
static int igMenuPval;

static int igInfJobUjob;
static int igInfJobCflg;

static int igPatternAloc;
static int igPatternJtyp;
static int igPatternText;

static int igPatternTypeAloc;
static int igPatternTypeJtyp;
static int igPatternTypeText;

static int igFieldsFnam;

static char * pcgFieldList = NULL;


static char  cgTabEnd[8];                       /* default table extension */

static char cgJobTab[14];
static char cgJtyTab[14];
static char cgDrrTab[14];
static char cgAloTab[14];
static char cgStfTab[14];
static char cgIvsTab[14];
static char cgCgvTab[14];
static char cgPolTab[14];
static char cgAftTab[14];

static char cgHopo[8]; /* default home airport    */
static char cgTdi1[24]; /* UTC-Loc7al Difference */
static char cgTdi2[24]; /* UTC-Local Difference */
static char cgTich[24]; /* UTC-Local Difference */

static int	igLoadOffSetStart;
static int	igLoadOffSetEnd;
static int	igOffSetEarlySBeg; /*Shiftbegin too early*/
static int	igOffSetLateSBeg;  /*Shiftbegin too late*/
static int	igOffSetLateBEnd;  /*Break End too late*/
static int	igLanguage;  /*Sprache*/

static BOOL bgSetPoolJobAcfr;
static BOOL bgSetJobActo;
static BOOL bgSetJobAcfr;

static time_t tgLoadStart;
static time_t tgLoadEnd;
static time_t tgCurrTime;

static char cgLidStatBuf[100];
static char cgLidMsgBuf[200];
static char cgMenuBuf[200];
static char cgInfJobBuf[50];
static char cgPatternBuf[200];
static char cgPatternTypeBuf[200];
static char cgFieldsBuf[20];

static char cgJobChangeFields[400];

long lgUjtyPol = 0;
long lgUjtyBrk = 0;
long lgUjtyDel = 0;
long lgUjtyDet = 0;
long lgUjtyFlt = 0;
	
/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int InitIvshdl();

void MyStrnCpy ( char *pcpDest, const char *pcpSource, size_t ipCount, int ipTrimRight );

static int GetTimeString(char *pcpTime,char *pcpDate);

static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpAddFieldLens, 
						 char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
						 char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
						 char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
						 ARRAYINFO *prpArrayInfo);

static int  SetArrayInfo(char *pcpArrayName,char *pcpTableName,  char *pcpArrayFieldList,char *pcpAddFields, long *plpAddFieldLens, 
						 char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
						 char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
						 char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
						 char *pcpIdx04Name, char *pcpIdx04FieldList, char *pcpIdx04Order, 
						 char *pcpIdx05Name, char *pcpIdx05FieldList, char *pcpIdx05Order, 
						 char *pcpIdx06Name, char *pcpIdx06FieldList, char *pcpIdx06Order, 
						 char *pcpIdx07Name, char *pcpIdx07FieldList, char *pcpIdx07Order, 
						 char *pcpIdx08Name, char *pcpIdx08FieldList, char *pcpIdx08Order, 
						 ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill,BOOL bpUseTrigger);

static int  SaveIndexInfo(ARRAYINFO *prpArrayInfo);

static int  TriggerAction(char *pcpTableName,char *pcpSendCommand,char *pcpCommands,char *pcpFields);


static int SendToIVSIF(char *pcpObject,char *pcpSelection,
			char *pcpFields,char *pcpData);

static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
				CMDBLK *prpCmdblk, char *pcpSelection, 
							char *pcpFields, char *pcpData);
 
static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo);



static int  GetFieldLength(char *pcpTana, char *pcpFina, long *pipLen);
static int  GetRowLength(char *pcpTana, char *pcpFieldList, long *pipLen);
static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen);
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);

static int TimeToStr(char *pcpTime,time_t lpTime);
static int StrToTime(char *pcpTime,time_t *plpTime);

static int GetItemNo(char *pcpFieldName,char *pcpFields,
		     int *pipItemNo);

static int GetItemData(char *pcpFieldName,char *pcpFields,char *pcpData,char *pcpDest);
static void TrimRight(char *pcpBuffer);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer);
static int  GetCommand(char *pcpCommand, int *pipCmd);
static void StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest);
static int UtcToLocal(char *pcpTime);

static void	Terminate(int ipSleep);            /* Terminate program      */

static int	Reset(void);                       /* Reset program          */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

/*Interne Funktionen*/
/*Init Functions*/
static int GetPatternTypeFromCfg(void);
static int GetPatternFromCfg(void);
/*Array Init Functions*/
static int InitializeLidStatRow(char *pcpLidStatBuf);
static int InitializeLidMsgRow(char *pcpLidMsgBuf);
static int InitializeMenuRow(char *pcpMenuBuf);
static int InitializeInfJobRow(char *pcpInfJobBuf);
static int InitializePatternRow(char *pcpPatternBuf);
static int InitializePatternTypeRow(char *pcpPatternTypeBuf);
static int InitializeFieldsRow(char *pcpFieldsBuf,char *pcpField);

/*Prepare Functions*/
static void SetFieldsArray();

static int PrepareJobData(char *pcpJobUrno);

/*Process Command Functions*/

static int ProcessIVS( char *pcpData);

static int Process115_TEXT( char *pcpLineID);
static int Process119_TEXT( char *pcpLineID);
static int Process116_TRANSFER( char *pcpLineID,char *pcpStat);
static int ProcessMenu( char *pcpLineID,int ipNumber);
static int ProcessInCall( char *pcpLineID);
static int ProcessGetSelection( char *pcpLineID,char *pcpPN);
static int Process101_DIAL(char *pcpLineID,int ipStat);
static int Process103_END( char *pcpLineID);
static int ProcessMenu1(char *pcpLineID);
static int ProcessMenu2(char *pcpLineID);
static int ProcessMenu3(char *pcpLineID);
static int ProcessMenu4(char *pcpLineID);
static int ProcessMenu5(char *pcpLineID);
static int ProcessInvalidMenu(char *pcpLineID);
static int ProcessConfirmJob(char *pcpLineID);
static int ProcessOutCall( char *pcpLineID);

static int ProcessCheckJobChanged( char *pcpUjob);
/*Data Handle Function*/
static int GetSelection6(char *pcpLineID);
static int RepeatGetSelection6(char *pcpLineID);
static int Menu_2(char *pcpLineID);
static int Menu_2_4(char *pcpLineID);
static int Menu_2_5(char *pcpLineID);
static int Menu_1_4(char *pcpLineID);
static int Menu_1_3_4Job(char *pcpLineID);
static int Menu_1_3_4Break(char *pcpLineID);
static int Menu_1_2_4_5(char *pcpLineID);
static int CheckPN(char *pcpLineID,char *pcpPeno);
static int CheckPoolJobStat(char *pcpLineID,int ipPos,char cpStat);
static int HandleConfirmPoolJob(char *pcpLineID);
static int HandleInformMa(char *pcpLineID);
static int HandleSetInformed(char *pcpLineID);
static int HandleSetConfirmed(char *pcpLineID);
static int HandleSetFinished(char *pcpLineID);
static int HandleConfirmJob(char *pcpLineID);
static int HandleFinishJob(char *pcpLineID);
static int Handle116_Transfer(char *pcpLineID,char *pcpStat);
static int HandleConnectDispo(char *pcpLineID);
static int HandleEndConnection(char *pcpLineID);
static int HandleGetNextJob(char *pcpLineID,BOOL blGetRealNextJob);
static int Handle101_DIAL(char *pcpLineID,int ipStat);
static int HandleSendJobText(char *pcpLineID);
static int HandleSpecialJob(char *pcpLineID);
static int ConfirmPoolJob(char *pcpLineID);
static int ConfirmJob(char *pcpLineID);
static int CheckJobChanged(char *pcpLineID);
static int CheckJobStatInformed(char *pcpLineID);
static int CheckJobStat(char *pcpLineID,int ipPos,char cpStat);
static int FindNextPlannedJob(char *pcpLineID);
static int FindNextInformedJob(char *pcpLineID);
static int FindNextJob(char *pcpLineID);
static int Find2ndNextJob(char *pcpLineID);
static int SetLidStat(char *pcpLineID, char *pcpStatus);
static int GetLidStat(char *pcpLineID, int *pipStatus);
static int SetLidMsg(char *pcpLineID, char *pcpMessage);
static int GetLidMsg(char *pcpLineID, char *pcpMessage);
static int CheckMenuKey(char *pcpLineID, char *pcpKey);
static int GetMenuText(char *pcpLineID, char *pcpMessage);
static int AddInfJob(char *pcpUjob);
static int RemoveInfJob(char *pcpUjob);
static int SetMenu(char *pcpLineID, char *pcpMessage,char *pcpPValues);
static int SetLidStatField(char *pcpLineID, char *pcpField,char *pcpValue);
static int GetLidStatField(char *pcpLineID, int ipFieldIdx,char *pcpValue);

static int GetJobText(char *pcpLineID,char *pcpJobText,BOOL bpStoreUjob);

static int GetDispoNumber(char *pcpLineID,char *pcpDispoNumber);

static int ConvertPattern(char *pcpPattern, char *pcpConverted);
static int SearchPatternInIvs(char *pcpUpperCase, char *pcpConverted);
static int ConvertNumber(int ipNumber, char *pcpConverted);
static int SetJobInformed(char *pcpLineID);
static int SetJobNotInformed(char *pcpLineID);

/**********Externe Funktionen*********/
extern void GetServerTimeStamp(char *pcpType, int ipFormat,long lpTimeDiff, char *pcpTimeStamp);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel;
	INITIALIZE;			/* General initialization	*/

	ilOldDebugLevel = debug_level;
	debug_level = TRACE;


	dbg(TRACE,"MAIN: version <%s>",sccs_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
		ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/demfnd",getenv("BIN_PATH"));
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	/* 20000619 bch start */
	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	}
	/* 20000619 bch end */


	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitIvshdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(0);
	}/* end of if */


	dbg(TRACE,"MAIN: initializing OK");

		
	debug_level = ilOldDebugLevel;


	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */

		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(0);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(0);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData();
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			case   667 :
				SaveIndexInfo(&rgJobArray) ;
				SaveIndexInfo(&rgPoolJobArray) ;
				break ;
			case	668 :
				SaveIndexInfo(&rgJtyArray) ;
				SaveIndexInfo(&rgDrrArray) ;
				SaveIndexInfo(&rgStfArray) ;
				SaveIndexInfo(&rgIvsArray) ;
				SaveIndexInfo(&rgAloArray) ;
				SaveIndexInfo(&rgCgvArray) ;
				SaveIndexInfo(&rgPolArray) ;
				break;
			case	669 :
				SaveIndexInfo(&rgLidStatArray) ;
				SaveIndexInfo(&rgLidMsgArray) ;
				SaveIndexInfo(&rgMenuArray) ;
				SaveIndexInfo(&rgInfJobArray) ;
				SaveIndexInfo(&rgPatternArray) ;
				SaveIndexInfo(&rgPatternTypeArray) ;
				SaveIndexInfo(&rgFieldsArray) ;
				break;
			case   670 :
				SaveIndexInfo(&rgAftArray) ;
				break ;

			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		
		} /* end else */
		
	} /* end for */
	
	exit(0);
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos;

	FindItemInList(POOLJOB_FIELDS,"ACFR",',',&igPoolJobAcfr,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"ACT3",',',&igPoolJobAct3,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"ACTO",',',&igPoolJobActo,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"CDAT",',',&igPoolJobCdat,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"DETY",',',&igPoolJobDety,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"GATE",',',&igPoolJobGate,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"HOPO",',',&igPoolJobHopo,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"JOUR",',',&igPoolJobJour,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"LSTU",',',&igPoolJobLstu,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"PLFR",',',&igPoolJobPlfr,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"PLTO",',',&igPoolJobPlto,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"POSI",',',&igPoolJobPosi,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"REGN",',',&igPoolJobRegn,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"STAT",',',&igPoolJobStat,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"TEXT",',',&igPoolJobText,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"UAFT",',',&igPoolJobUaft,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"UAID",',',&igPoolJobUaid,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"UALO",',',&igPoolJobUalo,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"UDEL",',',&igPoolJobUdel,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"UDRD",',',&igPoolJobUdrd,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"UDSR",',',&igPoolJobUdsr,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"UJTY",',',&igPoolJobUjty,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"URNO",',',&igPoolJobUrno,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"USEC",',',&igPoolJobUsec,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"USEU",',',&igPoolJobUseu,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"USTF",',',&igPoolJobUstf,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"TTGF",',',&igPoolJobTtgf,&ilCol,&ilPos);
	FindItemInList(POOLJOB_FIELDS,"TTGT",',',&igPoolJobTtgt,&ilCol,&ilPos);


	FindItemInList(JOB_FIELDS,"ACFR",',',&igJobAcfr,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"ACTO",',',&igJobActo,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"DETY",',',&igJobDety,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"HOPO",',',&igJobHopo,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"JOUR",',',&igJobJour,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"PLFR",',',&igJobPlfr,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"PLTO",',',&igJobPlto,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"STAT",',',&igJobStat,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"TEXT",',',&igJobText,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UAFT",',',&igJobUaft,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UAID",',',&igJobUaid,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UALO",',',&igJobUalo,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"UJTY",',',&igJobUjty,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"URNO",',',&igJobUrno,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS,"USTF",',',&igJobUstf,&ilCol,&ilPos);

	FindItemInList(JOB_FIELDS2,"ALID",',',&igJobAlid,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS2,"ALOC",',',&igJobAloc,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS2,"ALCX",',',&igJobAlcx,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS2,"FLTN",',',&igJobFltn,&ilCol,&ilPos);
	FindItemInList(JOB_FIELDS2,"JTYP",',',&igJobJtyp,&ilCol,&ilPos);
	
	FindItemInList(ALO_FIELDS,"URNO",',',&igAloUrno,&ilCol,&ilPos);
	FindItemInList(ALO_FIELDS,"REFT",',',&igAloReft,&ilCol,&ilPos);
	FindItemInList(ALO_FIELDS,"ALOT",',',&igAloAlot,&ilCol,&ilPos);
	FindItemInList(ALO_FIELDS,"ALOC",',',&igAloAloc,&ilCol,&ilPos);
	
	FindItemInList(AFT_FIELDS,"URNO",',',&igAftUrno,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"FLTN",',',&igAftFltn,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"ALC2",',',&igAftAlc2,&ilCol,&ilPos);
	FindItemInList(AFT_FIELDS,"ALC3",',',&igAftAlc3,&ilCol,&ilPos);

	FindItemInList(STF_FIELDS,"URNO",',',&igStfUrno,&ilCol,&ilPos);
	FindItemInList(STF_FIELDS,"FINM",',',&igStfFinm,&ilCol,&ilPos);
	FindItemInList(STF_FIELDS,"LANM",',',&igStfLanm,&ilCol,&ilPos);
	FindItemInList(STF_FIELDS,"PENO",',',&igStfPeno,&ilCol,&ilPos);

	FindItemInList(JTY_FIELDS,"URNO",',',&igJtyUrno,&ilCol,&ilPos);
	FindItemInList(JTY_FIELDS,"NAME",',',&igJtyName,&ilCol,&ilPos);

	FindItemInList(IVS_FIELDS,"TKEY",',',&igIvsTkey,&ilCol,&ilPos);
	FindItemInList(IVS_FIELDS,"CODE",',',&igIvsCode,&ilCol,&ilPos);
	FindItemInList(IVS_FIELDS,"TEXT",',',&igIvsText,&ilCol,&ilPos);
	FindItemInList(IVS_FIELDS,"URNO",',',&igIvsUrno,&ilCol,&ilPos);
	FindItemInList(IVS_FIELDS,"HOPO",',',&igIvsHopo,&ilCol,&ilPos);

	FindItemInList(POL_FIELDS,"HOPO",',',&igPolHopo,&ilCol,&ilPos);
	FindItemInList(POL_FIELDS,"URNO",',',&igPolUrno,&ilCol,&ilPos);
	FindItemInList(POL_FIELDS,"NAME",',',&igPolName,&ilCol,&ilPos);
	FindItemInList(POL_FIELDS,"DTEL",',',&igPolDtel,&ilCol,&ilPos);

	FindItemInList(CGV_FIELDS,"HOPO",',',&igCgvHopo,&ilCol,&ilPos);
	FindItemInList(CGV_FIELDS,"URNO",',',&igCgvUrno,&ilCol,&ilPos);
	FindItemInList(CGV_FIELDS,"NAME",',',&igCgvName,&ilCol,&ilPos);

	FindItemInList(DRR_FIELDS,"BSDU",',',&igDrrBsdu,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"DRSF",',',&igDrrDrsf,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"URNO",',',&igDrrUrno,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"DRRN",',',&igDrrDrrn,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"SDAY",',',&igDrrSday,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"STFU",',',&igDrrStfu,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"AVFR",',',&igDrrAvfr,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"AVTO",',',&igDrrAvto,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"ROSL",',',&igDrrRosl,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS,"ROSS",',',&igDrrRoss,&ilCol,&ilPos);
	FindItemInList(DRR_FIELDS2,"PENO",',',&igDrrPeno,&ilCol,&ilPos);

	/*AATArrays*/
	FindItemInList(LIDSTAT_FIELDS,"LIDN",',',&igLidStatLidn,&ilCol,&ilPos);
	FindItemInList(LIDSTAT_FIELDS,"PENO",',',&igLidStatPeno,&ilCol,&ilPos);
	FindItemInList(LIDSTAT_FIELDS,"USTF",',',&igLidStatUstf,&ilCol,&ilPos);
	FindItemInList(LIDSTAT_FIELDS,"UDSR",',',&igLidStatUdsr,&ilCol,&ilPos);
	FindItemInList(LIDSTAT_FIELDS,"UJOB",',',&igLidStatUjob,&ilCol,&ilPos);
	FindItemInList(LIDSTAT_FIELDS,"STAT",',',&igLidStatStat,&ilCol,&ilPos);
	FindItemInList(LIDSTAT_FIELDS,"MSTA",',',&igLidStatMsta,&ilCol,&ilPos);
	FindItemInList(LIDSTAT_FIELDS,"UJBT",',',&igLidStatUjbt,&ilCol,&ilPos);

	FindItemInList(LIDMSG_FIELDS,"LIDN",',',&igLidMsgLidn,&ilCol,&ilPos);
	FindItemInList(LIDMSG_FIELDS,"MSGT",',',&igLidMsgMsgt,&ilCol,&ilPos);

	FindItemInList(MENU_FIELDS,"LIDN",',',&igMenuLidn,&ilCol,&ilPos);
	FindItemInList(MENU_FIELDS,"MTXT",',',&igMenuMtxt,&ilCol,&ilPos);
	FindItemInList(MENU_FIELDS,"PVAL",',',&igMenuPval,&ilCol,&ilPos);

	FindItemInList(INFJOB_FIELDS,"UJOB",',',&igInfJobUjob,&ilCol,&ilPos);
	FindItemInList(INFJOB_FIELDS,"CFLG",',',&igInfJobCflg,&ilCol,&ilPos);

	FindItemInList(PATTERN_FIELDS,"JTYP",',',&igPatternJtyp,&ilCol,&ilPos);
	FindItemInList(PATTERN_FIELDS,"ALOC",',',&igPatternAloc,&ilCol,&ilPos);
	FindItemInList(PATTERN_FIELDS,"TEXT",',',&igPatternText,&ilCol,&ilPos);

	FindItemInList(PATTERNTYPE_FIELDS,"JTYP",',',&igPatternTypeJtyp,&ilCol,&ilPos);
	FindItemInList(PATTERNTYPE_FIELDS,"ALOC",',',&igPatternTypeAloc,&ilCol,&ilPos);
	FindItemInList(PATTERNTYPE_FIELDS,"TEXT",',',&igPatternTypeText,&ilCol,&ilPos);

	FindItemInList(FIELDS_FIELDS,"FNAM",',',&igFieldsFnam,&ilCol,&ilPos);

	return ilRc;
}

static int InitIvshdl()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	char pclSection[64];
	char pclKeyword[64];
	char pclSelection[264];
	char pclSqlBuf[2560];
	char pclDataArea[2560];
	char pclErr[2560];
 	short slCursor;
	short slSqlFunc;
	char pclJtytab[10];
	char pclApttab[10];
	long pllAddFieldLens[20];
	
	char clKey[12];
	long llFieldLength;
	long llRowNum = ARR_FIRST;
	char *pclJtyRow = NULL;
	char clLoadStart[20];
	char clLoadEnd[20];
	char clSdayStart[20];
	char clSdayEnd[20];

	
	/* read TableExtension from SGS.TAB */
	InitFieldIndex();
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSection,"ALL");
		sprintf(pclKeyword,"TABEND");

		ilRc = tool_search_exco_data(pclSection, pclKeyword, cgTabEnd);
		if (ilRc != RC_SUCCESS || strlen(cgTabEnd) <= 0)
		{
			dbg(TRACE,"InitIvshdl() Error reading TableExtension: [%s] %s not found in ceda/conf/sgs.tab",pclSection,pclKeyword);
			ilRc = RC_FAIL;
			Terminate(30);
		}
		else
		{
			sprintf(pclJtytab,"JTY%s",cgTabEnd);
			sprintf(pclApttab,"APT%s",cgTabEnd);
			dbg(TRACE,"InitIvshdl() TableExtension = <%s>",cgTabEnd);
		}
	}

	
	/* read HomeAirport from SGS.TAB */
	if(ilRc == RC_SUCCESS)
	{
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(pclSection,"SYS");
		sprintf(pclKeyword,"HOMEAP");

		ilRc = tool_search_exco_data(pclSection, pclKeyword, cgHopo);
		if (ilRc != RC_SUCCESS || strlen(cgHopo) <= 0)
		{
			dbg(TRACE,"InitDemfnd() Error reading HomeAirport: [%s] %s not found in ceda/conf/sgs.tab",pclSection,pclKeyword);
			ilRc = RC_FAIL;
			Terminate(30);
		}
		else
		{
			dbg(TRACE,"InitDemfnd() HomeAirport = <%s>",cgHopo);
		}
	}


	
	if(ilRc == RC_SUCCESS)
	{
		slSqlFunc = START;
		slCursor = 0;
		sprintf(pclSqlBuf,"SELECT TICH,TDI1,TDI2 FROM %s WHERE APC3='%s'",pclApttab,cgHopo);
		ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea);
		if (ilRc != DB_SUCCESS)
		{
			get_ora_err(ilRc,pclErr);
			dbg(TRACE,"InitDemfnd() Error reading UTC-Local difference in APTTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,pclSqlBuf);
			ilRc = RC_FAIL;
		}
		else
		{
			strcpy(cgTdi1,"60");
			strcpy(cgTdi2,"60");
			get_fld(pclDataArea,0,STR,14,cgTich);
			get_fld(pclDataArea,1,STR,14,cgTdi1);
			get_fld(pclDataArea,2,STR,14,cgTdi2);
			TrimRight(cgTich);
			if (*cgTich != '\0')
			{
				TrimRight(cgTdi1);
				TrimRight(cgTdi2);
				sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
				sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
			}
			dbg(TRACE,"InitIvshdl() Read %s -> TICH: <%s> TDI1: <%s> TDI2 <%s>",pclApttab,cgTich,cgTdi1,cgTdi2);
		}
		close_my_cursor(&slCursor);
		commit_work();
		slCursor = 0;
	}
	
	if(ilRc == RC_SUCCESS)
	{
		char pclTmpText[100];
		int  ilOffSet = 0;
		char clCurrTime[20];

		GetServerTimeStamp("UTC", 1, 0, clCurrTime);

		StrToTime(clCurrTime,&tgCurrTime);

		sprintf(pclSection,"MAIN");	
		sprintf(pclKeyword,"LOAD_OFFSET_START");
		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			ilOffSet = atol(pclTmpText);
			igLoadOffSetStart = (int) ilOffSet*60*60;
		}
		else
		{
			igLoadOffSetStart = -(2*60*60);
		}
		sprintf(pclKeyword,"LOAD_OFFSET_END");
		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			ilOffSet = atol(pclTmpText);
			igLoadOffSetEnd = (int) ilOffSet*60*60;
		}
		else
		{
			igLoadOffSetEnd = 24*60*60;
		}
	
		sprintf(pclKeyword,"OFFSET_EARLY_S_BEG");
		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			ilOffSet = atol(pclTmpText);
			igOffSetEarlySBeg = (int) ilOffSet*60;
		}
		else
		{
			igOffSetEarlySBeg = 20*60;
		}
		sprintf(pclKeyword,"OFFSET_LATE_S_BEG");
		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			ilOffSet = atol(pclTmpText);
			igOffSetLateSBeg = (int) ilOffSet*60;
		}
		else
		{
			igOffSetLateSBeg = 10*60;
		}
		sprintf(pclKeyword,"OFFSET_LATE_B_END");
		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			ilOffSet = atol(pclTmpText);
			igOffSetLateBEnd = (int) ilOffSet*60;
		}
		else
		{
			igOffSetLateBEnd = 10*60;
		}

		sprintf(pclKeyword,"LANGUAGE");

		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			igLanguage = atol(pclTmpText);
		}
		else
		{
			igLanguage = 1;
		}

		sprintf(pclKeyword,"SETPOOLJOBACFR");

		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			if(!strcmp(pclTmpText,"YES"))
			{
				bgSetPoolJobAcfr = TRUE;
			}
			else
			{
				bgSetPoolJobAcfr = FALSE;
			}
		}
		else
		{
			bgSetPoolJobAcfr = FALSE;
		}

		sprintf(pclKeyword,"SETJOBACFR");

		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			if(!strcmp(pclTmpText,"YES"))
			{
				bgSetJobAcfr = TRUE;
			}
			else
			{
				bgSetJobAcfr = FALSE;
			}
		}
		else
		{
			bgSetJobAcfr = FALSE;
		}

		sprintf(pclKeyword,"SETJOBACTO");

		if(ReadConfigEntry(pclSection,pclKeyword,pclTmpText) == RC_SUCCESS)
		{
			if(!strcmp(pclTmpText,"YES"))
			{
				bgSetJobActo = TRUE;
			}
			else
			{
				bgSetJobActo = FALSE;
			}
		}
		else
		{
			bgSetJobActo = FALSE;
		}

		sprintf(pclKeyword,"JOBCHANGEFIELDS");
		if(ReadConfigEntry(pclSection,pclKeyword,cgJobChangeFields) != RC_SUCCESS)
		{
			strcpy(cgJobChangeFields,"USTF");
		}


	}
	dbg(TRACE,"InitIvshdl() igOffSetEarlySEnd = %d",igOffSetLateBEnd);
	dbg(TRACE,"InitIvshdl() igOffSetLateSEnd = %d",igOffSetLateBEnd);
	dbg(TRACE,"InitIvshdl() igOffSetLateBEnd = %d",igOffSetLateBEnd);
	dbg(TRACE,"InitIvshdl() igLoadOffSetStart = %d",igLoadOffSetStart);
	dbg(TRACE,"InitIvshdl() igLoadOffSetEnd = %d",igLoadOffSetEnd);
	
	
	if(ilRc == RC_SUCCESS)
	{
	  sprintf(cgJobTab,"JOB%s",cgTabEnd);
	  sprintf(cgJtyTab,"JTY%s",cgTabEnd);
	  sprintf(cgAloTab,"ALO%s",cgTabEnd);
	  sprintf(cgStfTab,"STF%s",cgTabEnd);
	  sprintf(cgCgvTab,"CGV%s",cgTabEnd);
	  sprintf(cgIvsTab,"IVS%s",cgTabEnd);
	  sprintf(cgPolTab,"POL%s",cgTabEnd);
	  sprintf(cgAftTab,"AFT%s",cgTabEnd);
	  sprintf(cgDrrTab,"DRR%s",cgTabEnd);
	}
	
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(20,20);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */
	if(ilRc == RC_SUCCESS)
	{

		tgLoadStart = tgCurrTime + igLoadOffSetStart;
		tgLoadEnd = tgCurrTime + igLoadOffSetEnd;
		TimeToStr(clLoadStart,tgLoadStart);
		TimeToStr(clLoadEnd,tgLoadEnd);
		
		strncpy(clSdayStart,clLoadStart,8);
		strncpy(clSdayEnd,clLoadEnd,8);

		


		ilRc = SetArrayInfo(cgJtyTab,cgJtyTab,JTY_FIELDS,NULL,NULL,
							"JTYIDX1",JTY_SORTFIELDS,JTY_SORTDIR,
							"JTYIDX2",JTY_SORTFIELDS2,JTY_SORTDIR2,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							NULL,NULL,NULL,
							&rgJtyArray,NULL,TRUE,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: SetArrayInfo JTYTAB failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgJtyArray);
			dbg(TRACE,"InitIvshdl: SetArrayInfo JTYTAB OK");
		}/* end of if */


/*Hier wird die Urno f�r Jobtype == POL gestezt*/


		strcpy(clKey,"POL");
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
														&(rgJtyArray.crArrayName[0]),
														&(rgJtyArray.rrIdx01Handle),
														&(rgJtyArray.crIdx01Name[0]),
														clKey,&llRowNum,
														(void *) &pclJtyRow ) == RC_SUCCESS)
		{
			lgUjtyPol = atol(JTYFIELD(pclJtyRow,igJtyUrno));
		}
		else
		{
			dbg(TRACE,"InitIvshdl: error lgUjtyPol not initialised");
		}
		
		strcpy(clKey,"BRK");

		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
														&(rgJtyArray.crArrayName[0]),
														&(rgJtyArray.rrIdx01Handle),
														&(rgJtyArray.crIdx01Name[0]),
														clKey,&llRowNum,
														(void *) &pclJtyRow ) == RC_SUCCESS)
		{
			lgUjtyBrk = atol(JTYFIELD(pclJtyRow,igJtyUrno));
		}
		else
		{
			dbg(TRACE,"InitIvshdl: error lgUjtyBrk not initialised");
		}

		strcpy(clKey,"DET");

		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
														&(rgJtyArray.crArrayName[0]),
														&(rgJtyArray.rrIdx01Handle),
														&(rgJtyArray.crIdx01Name[0]),
														clKey,&llRowNum,
														(void *) &pclJtyRow ) == RC_SUCCESS)
		{
			lgUjtyDet = atol(JTYFIELD(pclJtyRow,igJtyUrno));
		}
		else
		{
			dbg(TRACE,"InitIvshdl: error lgUjtyDet not initialised");
		}

		strcpy(clKey,"DEL");

		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
														&(rgJtyArray.crArrayName[0]),
														&(rgJtyArray.rrIdx01Handle),
														&(rgJtyArray.crIdx01Name[0]),
														clKey,&llRowNum,
														(void *) &pclJtyRow ) == RC_SUCCESS)
		{
			lgUjtyDel = atol(JTYFIELD(pclJtyRow,igJtyUrno));
		}
		else
		{
			dbg(TRACE,"InitIvshdl: error lgUjtyDel not initialised");
		}


		strcpy(clKey,"FLT");

		llRowNum = ARR_FIRST;

		if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
														&(rgJtyArray.crArrayName[0]),
														&(rgJtyArray.rrIdx01Handle),
														&(rgJtyArray.crIdx01Name[0]),
														clKey,&llRowNum,
														(void *) &pclJtyRow ) == RC_SUCCESS)
		{
			lgUjtyFlt = atol(JTYFIELD(pclJtyRow,igJtyUrno));
		}
		else
		{
			dbg(TRACE,"InitIvshdl: error lgUjtyFlt not initialised");
		}

		dbg(TRACE,"InitIvshdl: SetArrayInfo JOBTAB start");
		sprintf(pclSelection,"WHERE UJTY != '%ld' AND UJTY != '%ld' AND UJTY != '%ld' AND ACTO > '%s' AND ACFR < '%s' AND HOPO = '%s'",lgUjtyPol,lgUjtyDel,lgUjtyDet,clLoadStart,clLoadEnd,cgHopo);
		
		dbg(TRACE,"InitIvshdl: JOB Selection <%s>",pclSelection);

		GetFieldLength("DEM","ALID",&(pllAddFieldLens[0]) );
		GetFieldLength("DEM","ALOC",&(pllAddFieldLens[1]) );
		pllAddFieldLens[2] = 4;
		GetFieldLength("AFT","FLTN",&(pllAddFieldLens[3]) );
		GetFieldLength("JTY","NAME",&(pllAddFieldLens[4]) );
		pllAddFieldLens[5] = 0;
	

		if(ilRc == RC_SUCCESS)
		{
			ilRc = SetArrayInfo(cgJobTab,cgJobTab,JOB_FIELDS,JOB_ADDFIELDS,pllAddFieldLens,
								"JOBIDX1",JOB_SORTFIELDS,JOB_SORTDIR,
								"JOBIDX2",JOB_SORTFIELDS2,JOB_SORTDIR2,
								"JOBIDX3",JOB_SORTFIELDS3,JOB_SORTDIR3,
								NULL,NULL,NULL,
								NULL,NULL,NULL,
								NULL,NULL,NULL,
								NULL,NULL,NULL,
								NULL,NULL,NULL,
								&rgJobArray,pclSelection,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo JOBTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgJobArray);                
				CEDAArraySendChanges2BCHDL(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,TRUE);
				CEDAArraySendChanges2ACTION(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,TRUE);

				dbg(TRACE,"InitIvshdl: SetArrayInfo JOBTAB OK");
			}/* end of if */

			sprintf(pclSelection,
						"((UJTY != %ld)  && (UJTY != %ld)  && (UJTY != %ld)  && (HOPO == %s)) ",lgUjtyPol,lgUjtyDel,lgUjtyDet,cgHopo);

			ilRc = CEDAArraySetFilter(&rgJobArray.rrArrayHandle,
						rgJobArray.crArrayName,pclSelection);

			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: CEDAArraySetFilter JOBTAB failed <%d>",ilRc);
			}
		}	

		if(ilRc == RC_SUCCESS)
		{
			sprintf(pclSelection,"WHERE UJTY = '%ld' AND ACTO > '%s' AND ACFR < '%s' AND HOPO = '%s'",lgUjtyPol,clLoadStart,clLoadEnd,cgHopo);
			dbg(TRACE,"InitIvshdl: POOLJOB Selection <%s>",pclSelection);
			ilRc = SetArrayInfo("JOBPOOL",cgJobTab,POOLJOB_FIELDS,NULL,NULL,
									"POOLJOBIDX1",POOLJOB_SORTFIELDS,POOLJOB_SORTDIR,
									"POOLJOBIDX2",POOLJOB_SORTFIELDS2,POOLJOB_SORTDIR2,
									"POOLJOBIDX3",POOLJOB_SORTFIELDS3,POOLJOB_SORTDIR3,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									&rgPoolJobArray,pclSelection,TRUE,FALSE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo POOLJOB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgPoolJobArray);
				CEDAArraySendChanges2BCHDL(&rgPoolJobArray.rrArrayHandle,rgPoolJobArray.crArrayName,TRUE);
				CEDAArraySendChanges2ACTION(&rgPoolJobArray.rrArrayHandle,rgPoolJobArray.crArrayName,TRUE);

				dbg(TRACE,"InitIvshdl: SetArrayInfo POOLJOB OK");
							sprintf(pclSelection,
						"((UJTY == %ld)  && (HOPO == %s)) ",lgUjtyPol,cgHopo);

				ilRc = CEDAArraySetFilter(&rgPoolJobArray.rrArrayHandle,
							rgPoolJobArray.crArrayName,pclSelection);
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitIvshdl: CEDAArraySetFilter POOLJOBTAB failed <%d>",ilRc);
				}


			}/* end of if */

		}

		if(ilRc == RC_SUCCESS)
		{
			sprintf(pclSelection,"WHERE AVTO >= '%s' AND AVFR <= '%s' AND HOPO = '%s'",clLoadStart,clLoadEnd,cgHopo);
			dbg(TRACE,"InitIvshdl: DRRTAB Selection <%s>",pclSelection);


			GetFieldLength("STF","PENO",&(pllAddFieldLens[0]) );			
			pllAddFieldLens[1] = 0;


			ilRc = SetArrayInfo(cgDrrTab,cgDrrTab,DRR_FIELDS,DRR_ADDFIELDS,pllAddFieldLens,
								"DRRIDX1",DRR_SORTFIELDS,DRR_SORTDIR,
								"DRRIDX2",DRR_SORTFIELDS2,DRR_SORTDIR2,							
								"DRRIDX3",DRR_SORTFIELDS3,DRR_SORTDIR3,							
									NULL,NULL,NULL,
								NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
								NULL,NULL,NULL,
								&rgDrrArray,pclSelection,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo DRRTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgDrrArray);
				dbg(TRACE,"InitIvshdl: SetArrayInfo DRRTAB OK");
			}/* end of if */

		}

		if(ilRc == RC_SUCCESS)
		{

			ilRc = SetArrayInfo(cgAloTab,cgAloTab,ALO_FIELDS,NULL,NULL,
									"ALOIDX1",ALO_SORTFIELDS,ALO_SORTDIR,
									"ALOIDX2",ALO_SORTFIELDS2,ALO_SORTDIR2,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									&rgAloArray,NULL,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo ALOTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgAloArray);
				dbg(TRACE,"InitIvshdl: SetArrayInfo ALOTAB OK");
			}/* end of if */
		}
		if(ilRc == RC_SUCCESS)
		{

			ilRc = SetArrayInfo(cgStfTab,cgStfTab,STF_FIELDS,NULL,NULL,
									"STFIDX1",STF_SORTFIELDS,STF_SORTDIR,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									&rgStfArray,NULL,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo STFTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgStfArray);
				dbg(TRACE,"InitIvshdl: SetArrayInfo STFTAB OK");
			}/* end of if */
		}
		if(ilRc == RC_SUCCESS)
		{

			ilRc = SetArrayInfo(cgIvsTab,cgIvsTab,IVS_FIELDS,NULL,NULL,
									"IVSIDX1",IVS_SORTFIELDS,IVS_SORTDIR,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									&rgIvsArray,NULL,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo IVSTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgIvsArray);
				dbg(TRACE,"InitIvshdl: SetArrayInfo IVSTAB OK");
			}/* end of if */
		}

		if(ilRc == RC_SUCCESS)
		{

			ilRc = SetArrayInfo(cgPolTab,cgPolTab,POL_FIELDS,NULL,NULL,
									"POLIDX1",POL_SORTFIELDS,POL_SORTDIR,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									&rgPolArray,NULL,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo POLTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgPolArray);
				dbg(TRACE,"InitIvshdl: SetArrayInfo POLTAB OK");
			}/* end of if */
		}

		if(ilRc == RC_SUCCESS)
		{

			ilRc = SetArrayInfo(cgCgvTab,cgCgvTab,CGV_FIELDS,NULL,NULL,
									"CGVIDX1",CGV_SORTFIELDS,CGV_SORTDIR,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									&rgCgvArray,NULL,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo CGVTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgCgvArray);
				dbg(TRACE,"InitIvshdl: SetArrayInfo CGVTAB OK");
			}/* end of if */
		}

		if(ilRc == RC_SUCCESS)
		{

			sprintf(pclSelection,"WHERE ((TIFA >= '%s' AND TIFA <= '%s' AND DES3 = '%s') OR (TIFD >= '%s' AND TIFD <= '%s' AND ORG3 = '%s'))",clLoadStart,clLoadEnd,cgHopo,clLoadStart,clLoadEnd,cgHopo);

			ilRc = SetArrayInfo(cgAftTab,cgAftTab,AFT_FIELDS,NULL,NULL,
									"AFTIDX1",AFT_SORTFIELDS,AFT_SORTDIR,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									NULL,NULL,NULL,
									&rgAftArray,pclSelection,TRUE,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: SetArrayInfo AFTTAB failed <%d>",ilRc);
			}else{
				DebugPrintArrayInfo(DEBUG,&rgAftArray);
				dbg(TRACE,"InitIvshdl: SetArrayInfo AFTTAB OK");
			}/* end of if */

			sprintf(pclSelection,
						"(((TIFA >= %s)  && (TIFA <= %s)  && (DES3 == %s)) || ((TIFD >= %s)  && (TIFD <= %s)  && (ORG3 == %s))) ",clLoadStart,clLoadEnd,cgHopo,clLoadStart,clLoadEnd,cgHopo);
			ilRc = CEDAArraySetFilter(&rgAftArray.rrArrayHandle,
						rgAftArray.crArrayName,pclSelection);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitIvshdl: CEDAArraySetFilter AFTTAB failed <%d>",ilRc);
			}
		}
		if(ilRc == RC_SUCCESS)
		{
			PrepareJobData("");
		}

		/*AATArrays*/

		GetFieldLength(cgStfTab,"PENO",&llFieldLength);
		pllAddFieldLens[0] = llFieldLength;
		GetFieldLength(cgStfTab,"URNO",&llFieldLength);
		pllAddFieldLens[1] = llFieldLength;
		pllAddFieldLens[2] = llFieldLength;
		pllAddFieldLens[3] = llFieldLength;
		pllAddFieldLens[4] = 10;
		pllAddFieldLens[5] = 10;
		pllAddFieldLens[6] = 10;
		pllAddFieldLens[7] = 10;
		pllAddFieldLens[8] = llFieldLength;
		pllAddFieldLens[9] = 0;

		ilRc = SetAATArrayInfo("LIDSTAT",LIDSTAT_FIELDS,pllAddFieldLens,"LIDSTATIDX1",LIDSTAT_SORTFIELDS,LIDSTAT_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgLidStatArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo LIDSTAT failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgLidStatArray);
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo LIDSTAT OK");
		}/* end of if */


		pllAddFieldLens[0] = 95;
		pllAddFieldLens[1] = 10;
		pllAddFieldLens[2] = 0;
		ilRc = SetAATArrayInfo("LIDMSG",LIDMSG_FIELDS,pllAddFieldLens,"LIDMSGIDX1",LIDMSG_SORTFIELDS,LIDMSG_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgLidMsgArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo LIDMSG failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgLidMsgArray);
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo LIDMSG OK");
		}/* end of if */

		pllAddFieldLens[0] = 95;
		pllAddFieldLens[1] = 10;
		pllAddFieldLens[2] = 20;
		pllAddFieldLens[3] = 0;
		ilRc = SetAATArrayInfo("MENU",MENU_FIELDS,pllAddFieldLens,"MENUIDX1",MENU_SORTFIELDS,MENU_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgMenuArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo MENU failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgMenuArray);
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo MENU OK");
		}/* end of if */

		
		GetFieldLength(cgStfTab,"URNO",&llFieldLength);
		pllAddFieldLens[0] = llFieldLength;
		pllAddFieldLens[1] = 4;
		pllAddFieldLens[2] = 0;
		ilRc = SetAATArrayInfo("INFJOB",INFJOB_FIELDS,pllAddFieldLens,"INFJOBIDX1",INFJOB_SORTFIELDS,INFJOB_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgInfJobArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo INFJOB failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgInfJobArray);
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo INFJOB OK");
		}/* end of if */
		
		GetFieldLength("JTY","NAME",&llFieldLength);
		pllAddFieldLens[0] = llFieldLength;
		GetFieldLength("ALO","ALOC",&llFieldLength);
		pllAddFieldLens[1] = llFieldLength;
		pllAddFieldLens[2] = 100;
		pllAddFieldLens[3] = 0;
		ilRc = SetAATArrayInfo("PATTERN",PATTERN_FIELDS,pllAddFieldLens,"PATTERNIDX1",PATTERN_SORTFIELDS,PATTERN_SORTDIR,
								NULL,NULL,NULL,NULL,NULL,NULL,&rgPatternArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo PATTERN failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgPatternArray);
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo PATTERN OK");
		}/* end of if */

		
		GetFieldLength("JTY","NAME",&llFieldLength);
		pllAddFieldLens[0] = llFieldLength;
		GetFieldLength("ALO","ALOC",&llFieldLength);
		pllAddFieldLens[1] = llFieldLength;
		pllAddFieldLens[2] = 100;
		pllAddFieldLens[3] = 0;
		ilRc = SetAATArrayInfo("PATTERNTYPE",PATTERNTYPE_FIELDS,pllAddFieldLens,"PATTERNTYPEIDX1",PATTERNTYPE_SORTFIELDS,PATTERNTYPE_SORTDIR,
								NULL,NULL,NULL,NULL,NULL,NULL,&rgPatternTypeArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo PATTERNTYPE failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgPatternTypeArray);
			dbg(TRACE,"InitIvshdl: SetAATArrayInfo PATTERNTYPE OK");
		}/* end of if */

		pllAddFieldLens[0] = 4;
		pllAddFieldLens[1] = 0;

		ilRc = SetAATArrayInfo("FIELDS",FIELDS_FIELDS,pllAddFieldLens,"FIELDSIDX1",FIELDS_SORTFIELDS,FIELDS_SORTDIR,NULL,NULL,NULL,NULL,NULL,NULL,&rgFieldsArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitJobhdl: SetAATArrayInfo FIELDSLIST failed <%d>",ilRc);
		}else{
			DebugPrintArrayInfo(DEBUG,&rgFieldsArray);
			dbg(TRACE,"InitJobhdl: SetAATArrayInfo FIELDSLIST OK");

			SetFieldsArray();
		}/* end of if */


		ilRc = GetPatternTypeFromCfg();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl() Error Reading PatternType");
		}

		ilRc = GetPatternFromCfg();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitIvshdl() Error Reading Pattern");
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
	}
	else
	{
		igInitOK = FALSE;
	}

	return ilRc;
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

 
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
	switch(pipSig)
	{
	default	:
		Terminate(0);
		break;
	} /* end of switch */
	exit(0);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	
	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(0);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				Terminate(0);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);
	if(igInitOK == FALSE)
	{
			ilRc = InitIvshdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemfnd: init failed!");
			} /* end of if */
	}/* end of if */
	/* OpenConnection(); */
} /* end of HandleQueues */
	
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int	ilRc = RC_SUCCESS;			/* Return code */
  int      ilCmd          = 0;

  BC_HEAD *prlBchead      = NULL;
  CMDBLK  *prlCmdblk      = NULL;
  char    *pclSelection   = NULL;
  char    *pclFields      = NULL;
  char    *pclData        = NULL;
  char     *pclNewline;
  char 		clUrnoList[2400];
  char 		clTable[24];


  prlBchead    = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
  pclSelection = prlCmdblk->data;
  pclFields    = pclSelection + strlen(pclSelection) + 1;
   
  pclData      = pclFields + strlen(pclFields) + 1;

 
  pclNewline = strchr(pclData,'\n');
  if (pclNewline != NULL)
  {
     *pclNewline = '\0';
  }
  pcgFieldList  = pclFields;

  dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

  DebugPrintBchead(DEBUG,prlBchead);
  DebugPrintCmdblk(DEBUG,prlCmdblk);
  dbg(DEBUG,"selection <%s>",pclSelection);
  dbg(DEBUG,"fields    <%s>",pclFields);


  ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
  if(ilRc == RC_SUCCESS)
    {
      switch (ilCmd)
		{
		case CMD_IRT :
			ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
			if((ilRc == RC_SUCCESS) && (strcmp(prlCmdblk->obj_name,cgJobTab) == 0))
			{
				int ilItemNoUrno = 0;
				char clUrno[20];
				if(GetItemNo("URNO",pclFields,&ilItemNoUrno) == RC_SUCCESS)
				{
				   get_real_item(clUrno,pclData,ilItemNoUrno);

			   	   dbg(TRACE,"HandleData URT JOBURNO <%s>",clUrno);
					if(atol(clUrno) > 0)
					{
						PrepareJobData(clUrno);
					}
			   }
			}
			break;
		case CMD_URT :
			ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
			if((ilRc == RC_SUCCESS) && (strcmp(prlCmdblk->obj_name,cgJobTab) == 0))
			{
			   void *pcvNxt = NULL;                                           
			   char *pclU = NULL;  
			   char *pclFieldsRow = NULL;
			   int ilItem = 0;
			   BOOL blCheckJobChanged = FALSE;
			   long llAction = ARR_FIRST;
			   pcvNxt = NULL;
			  

			  while((AATArrayGetRowPointer(&(rgFieldsArray.rrArrayHandle),
				   &(rgFieldsArray.crArrayName[0]),llAction,(void *)&pclFieldsRow) == RC_SUCCESS) && !blCheckJobChanged)
			  {
				  blCheckJobChanged = (GetItemNo(FIELDSFIELD(pclFieldsRow,igFieldsFnam),pclFields,&ilItem)
											== RC_SUCCESS)?TRUE:FALSE;	
				  llAction = ARR_NEXT;
				  
			  }

			  if(blCheckJobChanged)
			  {
				   while ((pclU=GetNextUrnoFromSelection(pclSelection,&pcvNxt))!= NULL)  
				   {
						dbg(TRACE,"HandleData URT JOBURNO <%s>",pclSelection);
						if(atol(pclU) > 0)
						{
							ProcessCheckJobChanged(pclU);
							PrepareJobData(pclU);
						}
				   }
			  }
			}

			break;
		case CMD_DRT :
			ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
			if((ilRc == RC_SUCCESS) && (strcmp(prlCmdblk->obj_name,cgJobTab) == 0))
			{
			   void *pcvNxt = NULL;                                           
			   char *pclU = NULL;  
			   pcvNxt = NULL;
			   while ((pclU=GetNextUrnoFromSelection(pclSelection,&pcvNxt))!= NULL)  
			   {
					dbg(TRACE,"HandleData DRT JOBURNO <%s>",pclSelection);
					if(atol(pclU) > 0)
					{
						ProcessCheckJobChanged(pclU);
					}
			   }
			}
			break;
		case CMD_IVS : /* Send Selected Demands  */
			ProcessIVS(pclData);
			break;
		default:
		  break;	
				 

		}
	
    }

  dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
  return ilRc;
	
} /* end of HandleData */

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/

/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc   = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[8];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */


/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int  SetArrayInfo(char *pcpArrayName,char *pcpTableName,  char *pcpArrayFieldList,char *pcpAddFields, long *plpAddFieldLens, 
						 char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
						 char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
						 char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
						 char *pcpIdx04Name, char *pcpIdx04FieldList, char *pcpIdx04Order, 
						 char *pcpIdx05Name, char *pcpIdx05FieldList, char *pcpIdx05Order, 
						 char *pcpIdx06Name, char *pcpIdx06FieldList, char *pcpIdx06Order, 
						 char *pcpIdx07Name, char *pcpIdx07FieldList, char *pcpIdx07Order, 
						 char *pcpIdx08Name, char *pcpIdx08FieldList, char *pcpIdx08Order, 
						 ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill,BOOL bpUseTrigger)
{
	int	ilRc   = RC_SUCCESS;				/* Return code */
	int ilLoop = 0;
	char clOrderTxt[4];
	long llTmpRowLength1 = 0;
	long llTmpRowLength2 = 0;
	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;
		prpArrayInfo->rrIdx02Handle = -1;
		prpArrayInfo->rrIdx03Handle = -1;
		prpArrayInfo->rrIdx04Handle = -1;
		prpArrayInfo->rrIdx05Handle = -1;
		prpArrayInfo->rrIdx06Handle = -1;
		prpArrayInfo->rrIdx07Handle = -1;
		prpArrayInfo->rrIdx08Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */

		if(pcpAddFields != NULL)
		{
			if(strlen(pcpAddFields) + 
				strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
			{
				strcat(prpArrayInfo->crArrayFieldList,",");
				strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
				dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
			}/* end of if */
		}/* end of if */


		if(ilRc == RC_SUCCESS)
		{
			prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
			if (pcpAddFields != NULL)
			{
				prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
			}

			prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			if(prpArrayInfo->plrArrayFieldOfs == NULL)
			{
				dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
				ilRc = RC_FAIL;
			}/* end of if */
		}/* end of if */


		if(pcpIdx01Name != NULL)
		{
			if(strlen(pcpIdx01Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx01FieldList != NULL)
		{
			if(strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);
			}/* end of if */
		}/* end of if */
		if(pcpIdx02Name != NULL)
		{
			if(strlen(pcpIdx02Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx02Name,pcpIdx02Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx02FieldList != NULL)
		{
			if(strlen(pcpIdx02FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx02FieldList,pcpIdx02FieldList);
			}/* end of if */
		}/* end of if */

		if(pcpIdx03Name != NULL)
		{
			if(strlen(pcpIdx03Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx03Name,pcpIdx03Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx03FieldList != NULL)
		{
			if(strlen(pcpIdx03FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx03FieldList,pcpIdx03FieldList);
			}/* end of if */
		}/* end of if */


		if(pcpIdx04Name != NULL)
		{
			if(strlen(pcpIdx04Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx04Name,pcpIdx04Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx04FieldList != NULL)
		{
			if(strlen(pcpIdx04FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx04FieldList,pcpIdx04FieldList);
			}/* end of if */
		}/* end of if */
		if(pcpIdx05Name != NULL)
		{
			if(strlen(pcpIdx05Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx05Name,pcpIdx05Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx05FieldList != NULL)
		{
			if(strlen(pcpIdx05FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx05FieldList,pcpIdx05FieldList);
			}/* end of if */
		}/* end of if */
		if(pcpIdx06Name != NULL)
		{
			if(strlen(pcpIdx06Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx06Name,pcpIdx06Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx06FieldList != NULL)
		{
			if(strlen(pcpIdx06FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx06FieldList,pcpIdx06FieldList);
			}/* end of if */
		}/* end of if */
		if(pcpIdx07Name != NULL)
		{
			if(strlen(pcpIdx07Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx07Name,pcpIdx07Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx07FieldList != NULL)
		{
			if(strlen(pcpIdx07FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx07FieldList,pcpIdx07FieldList);
			}/* end of if */
		}/* end of if */
		if(pcpIdx08Name != NULL)
		{
			if(strlen(pcpIdx08Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx08Name,pcpIdx08Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx08FieldList != NULL)
		{
			if(strlen(pcpIdx08FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx08FieldList,pcpIdx08FieldList);
			}/* end of if */
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen++;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx01FieldList != NULL)
   	{
/*       	ilRc = GetRowLength(pcpTableName,pcpIdx01FieldList,&(prpArrayInfo->lrIdx01RowLen));*/
       	ilRc = GetRowLength(pcpTableName,pcpIdx01FieldList,&llTmpRowLength1);
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}/* end of if */
		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx01FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx01RowLen = llTmpRowLength1 + llTmpRowLength2;
		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}/* end of if */

   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpIdx02FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx02Name,pcpIdx02FieldList);
       	}/* end of if */

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx02FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx02RowLen = llTmpRowLength1 + llTmpRowLength2;

		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx02Name,pcpIdx02FieldList);
       	}/* end of if */

   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpIdx03FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx03Name,pcpIdx03FieldList);
       	}/* end of if */

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx03FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx03RowLen = llTmpRowLength1 + llTmpRowLength2;
        
		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx03Name,pcpIdx03FieldList);
       	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpIdx04FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx04Name,pcpIdx04FieldList);
       	}/* end of if */

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx04FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx04RowLen = llTmpRowLength1 + llTmpRowLength2;
        
		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx04Name,pcpIdx04FieldList);
       	}/* end of if */
   	}/* end of if */

   	
   	if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpIdx05FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx05Name,pcpIdx05FieldList);
       	}/* end of if */

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx05FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx05RowLen = llTmpRowLength1 + llTmpRowLength2;
        
		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx05Name,pcpIdx05FieldList);
       	}/* end of if */
   	}/* end of if */

   	
   	if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpIdx06FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx06Name,pcpIdx06FieldList);
       	}/* end of if */

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx06FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx06RowLen = llTmpRowLength1 + llTmpRowLength2;
        
		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx06Name,pcpIdx06FieldList);
       	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpIdx07FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx07Name,pcpIdx07FieldList);
       	}/* end of if */

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx07FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx07RowLen = llTmpRowLength1 + llTmpRowLength2;
        
		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx07Name,pcpIdx07FieldList);
       	}/* end of if */
   	}/* end of if */


   	if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
   	{
       	ilRc = GetRowLength(pcpTableName,pcpIdx08FieldList,&llTmpRowLength1);
		
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx08Name,pcpIdx08FieldList);
       	}/* end of if */

		ilRc = GetLogicalRowLength(pcpAddFields,pcpIdx08FieldList,plpAddFieldLens,&llTmpRowLength2);
		prpArrayInfo->lrIdx08RowLen = llTmpRowLength1 + llTmpRowLength2;
        
		if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpIdx08Name,pcpIdx08FieldList);
       	}/* end of if */
   	}/* end of if */

   	
	if(ilRc == RC_SUCCESS  && pcpIdx01FieldList != NULL)
   	{
		prpArrayInfo->lrIdx01RowLen++;
		dbg(TRACE,"SetArrayInfo: lrIdx01RowLen <%ld>",prpArrayInfo->lrIdx01RowLen);
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	
 
	if(ilRc == RC_SUCCESS  && pcpIdx01FieldList != NULL)
	{
		prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);
		dbg(TRACE,"SetArrayInfo: lrIdx01FieldCnt <%ld>",prpArrayInfo->lrIdx01FieldCnt);

		prpArrayInfo->plrIdx01FieldPos = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx01FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx01FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx01FieldPos) = -1;
		}/* end of if */
	}/* end of if */

 
	if(ilRc == RC_SUCCESS  && pcpIdx01FieldList != NULL)
	{
		prpArrayInfo->plrIdx01FieldOrd = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx01FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx01FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
			{
				dbg(TRACE,"SetArrayInfo: ilLoop <%d>",ilLoop);
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx01Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx01FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */

	
	
	
 
   	if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
   	{
		prpArrayInfo->lrIdx02RowLen++;
		prpArrayInfo->pcrIdx02RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx02RowLen+1));
   		if(prpArrayInfo->pcrIdx02RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	
 	

	if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
	{
		prpArrayInfo->lrIdx02FieldCnt = get_no_of_items(pcpIdx02FieldList);

		prpArrayInfo->plrIdx02FieldPos = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx02FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx02FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx02FieldPos) = -1;
		}/* end of if */
	}/* end of if */


 
	if(ilRc == RC_SUCCESS && pcpIdx02FieldList != NULL)
	{
		prpArrayInfo->plrIdx02FieldOrd = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx02FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx02FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx02FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx02Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx02FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */
	

   	if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
   	{
		prpArrayInfo->lrIdx03RowLen++;
		prpArrayInfo->pcrIdx03RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx03RowLen+1));
   		if(prpArrayInfo->pcrIdx03RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	

	if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
	{
		prpArrayInfo->lrIdx03FieldCnt = get_no_of_items(pcpIdx03FieldList);

		prpArrayInfo->plrIdx03FieldPos = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx03FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx03FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx03FieldPos) = -1;
		}/* end of if */
	}/* end of if */


 	if(ilRc == RC_SUCCESS && pcpIdx03FieldList != NULL)
	{
		prpArrayInfo->plrIdx03FieldOrd = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx03FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx03FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx03FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx03Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx03FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */
		

   	if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
   	{
		prpArrayInfo->lrIdx04RowLen++;
		prpArrayInfo->pcrIdx04RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx04RowLen+1));
   		if(prpArrayInfo->pcrIdx04RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	

	if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
	{
		prpArrayInfo->lrIdx04FieldCnt = get_no_of_items(pcpIdx04FieldList);

		prpArrayInfo->plrIdx04FieldPos = (long *) calloc(prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx04FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx04FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx04FieldPos) = -1;
		}/* end of if */
	}/* end of if */


 	if(ilRc == RC_SUCCESS && pcpIdx04FieldList != NULL)
	{
		prpArrayInfo->plrIdx04FieldOrd = (long *) calloc(prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx04FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx04FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx04FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx04FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx04Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx04FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx04FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx04FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx04FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */
		

   	if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
   	{
		prpArrayInfo->lrIdx05RowLen++;
		prpArrayInfo->pcrIdx05RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx05RowLen+1));
   		if(prpArrayInfo->pcrIdx05RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	

	if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
	{
		prpArrayInfo->lrIdx05FieldCnt = get_no_of_items(pcpIdx05FieldList);

		prpArrayInfo->plrIdx05FieldPos = (long *) calloc(prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx05FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx05FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx05FieldPos) = -1;
		}/* end of if */
	}/* end of if */


 	if(ilRc == RC_SUCCESS && pcpIdx05FieldList != NULL)
	{
		prpArrayInfo->plrIdx05FieldOrd = (long *) calloc(prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx05FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx05FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx05FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx05FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx05Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx05FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx05FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx05FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx05FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */
	

   	if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
   	{
		prpArrayInfo->lrIdx06RowLen++;
		prpArrayInfo->pcrIdx06RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx06RowLen+1));
   		if(prpArrayInfo->pcrIdx06RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	

	if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
	{
		prpArrayInfo->lrIdx06FieldCnt = get_no_of_items(pcpIdx06FieldList);

		prpArrayInfo->plrIdx06FieldPos = (long *) calloc(prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx06FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx06FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx06FieldPos) = -1;
		}/* end of if */
	}/* end of if */


 	if(ilRc == RC_SUCCESS && pcpIdx06FieldList != NULL)
	{
		prpArrayInfo->plrIdx06FieldOrd = (long *) calloc(prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx06FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx06FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx06FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx06FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx06Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx06FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx06FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx06FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx06FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */
	
  	if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
   	{
		prpArrayInfo->lrIdx07RowLen++;
		prpArrayInfo->pcrIdx07RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx07RowLen+1));
   		if(prpArrayInfo->pcrIdx07RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	

	if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
	{
		prpArrayInfo->lrIdx07FieldCnt = get_no_of_items(pcpIdx07FieldList);

		prpArrayInfo->plrIdx07FieldPos = (long *) calloc(prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx07FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx07FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx07FieldPos) = -1;
		}/* end of if */
	}/* end of if */


 	if(ilRc == RC_SUCCESS && pcpIdx07FieldList != NULL)
	{
		prpArrayInfo->plrIdx07FieldOrd = (long *) calloc(prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx07FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx07FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx07FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx07FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx07Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx07FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx07FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx07FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx07FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */
	
  	if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
   	{
		prpArrayInfo->lrIdx08RowLen++;
		prpArrayInfo->pcrIdx08RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx08RowLen+1));
   		if(prpArrayInfo->pcrIdx08RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */
	

	if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
	{
		prpArrayInfo->lrIdx08FieldCnt = get_no_of_items(pcpIdx08FieldList);

		prpArrayInfo->plrIdx08FieldPos = (long *) calloc(prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx08FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx08FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx08FieldPos) = -1;
		}/* end of if */
	}/* end of if */


 	if(ilRc == RC_SUCCESS && pcpIdx08FieldList != NULL)
	{
		prpArrayInfo->plrIdx08FieldOrd = (long *) calloc(prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx08FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx08FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx08FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx08FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx08Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx08FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx08FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx08FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx08FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */
	

	if(ilRc == RC_SUCCESS)
	{
		if(plpAddFieldLens != NULL)
		{
			dbg(TRACE,"SetArrayInfo: plpAddFieldLens <%ld> arrayname <%s>",plpAddFieldLens[0],pcpArrayName);
		}

		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,plpAddFieldLens,pcpArrayFieldList,&(prpArrayInfo->plrArrayFieldLen[0]),&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

		

 
	if(ilRc == RC_SUCCESS && bpFill)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */
	

	if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx01Handle),pcpIdx01Name,pcpIdx01FieldList,&(prpArrayInfo->plrIdx01FieldOrd[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMultiIndex 1 failed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */
	

 	if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx02Handle),pcpIdx02Name,pcpIdx02FieldList,&(prpArrayInfo->plrIdx02FieldOrd[0]));

		if(ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMultiIndex 2 failed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */
	

 	if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx03Handle),pcpIdx03Name,pcpIdx03FieldList,&(prpArrayInfo->plrIdx03FieldOrd[0]));

		if(ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 3 Indexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS && pcpIdx04Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx04Handle),pcpIdx04Name,pcpIdx04FieldList,&(prpArrayInfo->plrIdx04FieldOrd[0]));

		if(ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 4 Indexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx05Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx05Handle),pcpIdx05Name,pcpIdx05FieldList,&(prpArrayInfo->plrIdx05FieldOrd[0]));

		if(ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 5 Indexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx06Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx06Handle),pcpIdx06Name,pcpIdx06FieldList,&(prpArrayInfo->plrIdx06FieldOrd[0]));

		if(ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 6 Indexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx07Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx07Handle),pcpIdx07Name,pcpIdx07FieldList,&(prpArrayInfo->plrIdx07FieldOrd[0]));

		if(ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 7 Indexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx08Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx08Handle),pcpIdx08Name,pcpIdx08FieldList,&(prpArrayInfo->plrIdx08FieldOrd[0]));

		if(ilRc != RC_SUCCESS )
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayCreateMulti 8 Indexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */

 	if(ilRc == RC_SUCCESS && bpUseTrigger) 
	{ 
	    ilRc = TriggerAction(pcpTableName,NULL,"IRT,URT,DRT",NULL); 
	    if(ilRc != RC_SUCCESS) 
	    { 
		dbg(TRACE,"SetArrayInfo: TriggerAction failed <%d>",ilRc); 
	    }/* end of if */ 
	}/* end of if */ 
 	
 
	return(ilRc);
	
} /* end of SetArrayInfo */
/******************************************************************************/
/*                                                                            */
/******************************************************************************/

static int  SetAATArrayInfo(char *pcpArrayName,char *pcpArrayFieldList, long *plpFieldLens, 
						 char *pcpIdx01Name, char *pcpIdx01FieldList, char *pcpIdx01Order, 
						 char *pcpIdx02Name, char *pcpIdx02FieldList, char *pcpIdx02Order, 
						 char *pcpIdx03Name, char *pcpIdx03FieldList, char *pcpIdx03Order, 
						 ARRAYINFO *prpArrayInfo)
{
	int	ilRc   = RC_SUCCESS;				/* Return code */
	int ilLoop = 0;
	char clOrderTxt[4];

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;
		prpArrayInfo->rrIdx02Handle = -1;
		prpArrayInfo->rrIdx03Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */

		if(ilRc == RC_SUCCESS)
		{
			prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);


			prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			if(prpArrayInfo->plrArrayFieldOfs == NULL)
			{
				dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
				ilRc = RC_FAIL;
			}/* end of if */
			else
			{
				prpArrayInfo->plrArrayFieldOfs[0] = 0;
				for(ilLoop = 1 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
				{
					prpArrayInfo->plrArrayFieldOfs[ilLoop] = prpArrayInfo->plrArrayFieldOfs[ilLoop -1] + (plpFieldLens[ilLoop-1] +1);
				}/* end of for */
			}
		}/* end of if */


		if(pcpIdx01Name != NULL)
		{
			if(strlen(pcpIdx01Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx01Name,pcpIdx01Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx01FieldList != NULL)
		{
			if(strlen(pcpIdx01FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx01FieldList,pcpIdx01FieldList);
			}/* end of if */
		}/* end of if */
		if(pcpIdx02Name != NULL)
		{
			if(strlen(pcpIdx02Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx02Name,pcpIdx02Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx02FieldList != NULL)
		{
			if(strlen(pcpIdx02FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx02FieldList,pcpIdx02FieldList);
			}/* end of if */
		}/* end of if */

		if(pcpIdx03Name != NULL)
		{
			if(strlen(pcpIdx03Name) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crIdx03Name,pcpIdx03Name);
			}/* end of if */
		}/* end of if */

		if(pcpIdx03FieldList != NULL)
		{
			if(strlen(pcpIdx03FieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crIdx03FieldList,pcpIdx03FieldList);
			}/* end of if */
		}/* end of if */


	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpArrayFieldList,plpFieldLens,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen++;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+1));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
   	{
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx01FieldList,plpFieldLens,&(prpArrayInfo->lrIdx01RowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx01Name,pcpIdx01FieldList);
       	}/* end of if */
   	}/* end of if */
   	if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
   	{
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx02FieldList,plpFieldLens,&(prpArrayInfo->lrIdx02RowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx02Name,pcpIdx02FieldList);
       	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
   	{
       	ilRc = GetLogicalRowLength(pcpArrayFieldList,pcpIdx03FieldList,plpFieldLens,&(prpArrayInfo->lrIdx03RowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetLogicalRowLength failed <%s> <%s>",pcpIdx03Name,pcpIdx03FieldList);
       	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
   	{
		prpArrayInfo->lrIdx01RowLen++;
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
	{
		prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpIdx01FieldList);

		prpArrayInfo->plrIdx01FieldPos = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx01FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx01FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx01FieldPos) = -1;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
	{
		prpArrayInfo->plrIdx01FieldOrd = (long *) calloc(prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx01FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx01FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx01Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx01FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx01FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
   	{
		prpArrayInfo->lrIdx02RowLen++;
		prpArrayInfo->pcrIdx02RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx02RowLen+1));
   		if(prpArrayInfo->pcrIdx02RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
	{
		prpArrayInfo->lrIdx02FieldCnt = get_no_of_items(pcpIdx02FieldList);

		prpArrayInfo->plrIdx02FieldPos = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx02FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx02FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx02FieldPos) = -1;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
	{
		prpArrayInfo->plrIdx02FieldOrd = (long *) calloc(prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx02FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx02FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx02FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx02Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx02FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx02FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */


   	if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
   	{
		prpArrayInfo->lrIdx03RowLen++;
		prpArrayInfo->pcrIdx03RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx03RowLen+1));
   		if(prpArrayInfo->pcrIdx03RowBuf == NULL)
   		{
			ilRc = RC_FAIL;
			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
	{
		prpArrayInfo->lrIdx03FieldCnt = get_no_of_items(pcpIdx03FieldList);

		prpArrayInfo->plrIdx03FieldPos = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx03FieldPos == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx03FieldPos calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrIdx03FieldPos) = -1;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
	{
		prpArrayInfo->plrIdx03FieldOrd = (long *) calloc(prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
		if(prpArrayInfo->plrIdx03FieldOrd == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrIdx03FieldOrd calloc(%d,%d) failed",prpArrayInfo->lrIdx03FieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx03FieldCnt ; ilLoop++)
			{
				ilRc = get_real_item(&clOrderTxt[0],pcpIdx03Order,ilLoop+1);
				if(ilRc > 0)
				{
					switch(clOrderTxt[0])
					{
					case 'A' :
						prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_ASC;
						break;
					case 'D' :
						prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
						break;
					default :
						prpArrayInfo->plrIdx03FieldOrd[ilLoop] = ARR_DESC;
						break;
					}/* end of switch */

					ilRc = RC_SUCCESS;
				}else{
					prpArrayInfo->plrIdx03FieldOrd[ilLoop] = -1;
				}/* end of for */
			}/* end of for */
		}/* end of if */
	}/* end of if */



	if(ilRc == RC_SUCCESS)
	{
		
		ilRc = AATArrayCreate(&(prpArrayInfo->rrArrayHandle),pcpArrayName,TRUE,1000,prpArrayInfo->lrArrayFieldCnt,plpFieldLens,pcpArrayFieldList,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */



	if(ilRc == RC_SUCCESS && pcpIdx01Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx01Handle),pcpIdx01Name,pcpIdx01FieldList,&(prpArrayInfo->plrIdx01FieldOrd[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */
 
	if(ilRc == RC_SUCCESS && pcpIdx02Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx02Handle),pcpIdx02Name,pcpIdx02FieldList,&(prpArrayInfo->plrIdx02FieldOrd[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS && pcpIdx03Name != NULL)
	{
		ilRc = CEDAArrayCreateMultiIndex(&(prpArrayInfo->rrArrayHandle),pcpArrayName,&(prpArrayInfo->rrIdx03Handle),pcpIdx03Name,pcpIdx03FieldList,&(prpArrayInfo->plrIdx03FieldOrd[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetAAtArrayInfo: CEDAArrayCreateMultiIndexfailed <%d>",ilRc);
 		}/* end of if */
	}/* end of if */

	return(ilRc);
	
} /* end of SetAAtArrayInfo */

/*************************************************************************************/
/*************************************************************************************/

static void DebugPrintArrayInfo(int ipDebugLevel, ARRAYINFO *prpArrayInfo)
{
	int ilLoop = 0;
	
	if(prpArrayInfo == NULL)
	{
		dbg(ipDebugLevel,"DebugPrintArrayInfo: nothing to print");
		return;
	}/* end of if */

	dbg(ipDebugLevel,"DebugPrintArrayInfo: rrArrayHandle         <%d>",prpArrayInfo->rrArrayHandle);
	dbg(ipDebugLevel,"DebugPrintArrayInfo: crArrayName           <%s>",&(prpArrayInfo->crArrayName[0]));
	dbg(ipDebugLevel,"DebugPrintArrayInfo: crArrayFieldList      <%s>",&(prpArrayInfo->crArrayFieldList[0]));
	dbg(ipDebugLevel,"DebugPrintArrayInfo: lrArrayFieldCnt       <%d>",prpArrayInfo->lrArrayFieldCnt);
	dbg(ipDebugLevel,"DebugPrintArrayInfo: lrArrayRowLen         <%d>",prpArrayInfo->lrArrayRowLen);
	dbg(ipDebugLevel,"DebugPrintArrayInfo: pcrArrayRowBuf        <%8.8x>",prpArrayInfo->pcrArrayRowBuf);
	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen      <%8.8x>",prpArrayInfo->plrArrayFieldLen);

	for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
	{
		dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldLen[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldLen[ilLoop]);
	}/* end of for */

	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs      <%8.8x>",prpArrayInfo->plrArrayFieldOfs);

	for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrArrayFieldCnt ; ilLoop++)
	{
		dbg(ipDebugLevel,"DebugPrintArrayInfo: plrArrayFieldOfs[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrArrayFieldOfs[ilLoop]);
	}/* end of for */

	dbg(ipDebugLevel,"DebugPrintArrayInfo: rrIdx01Handle         <%d>",prpArrayInfo->rrIdx01Handle);
	dbg(ipDebugLevel,"DebugPrintArrayInfo: crIdx01Name           <%s>",&(prpArrayInfo->crIdx01Name[0]));
	dbg(ipDebugLevel,"DebugPrintArrayInfo: crIdx01FieldList      <%s>",&(prpArrayInfo->crIdx01FieldList[0]));
	dbg(ipDebugLevel,"DebugPrintArrayInfo: lrIdx01FieldCnt       <%d>",prpArrayInfo->lrIdx01FieldCnt);
	dbg(ipDebugLevel,"DebugPrintArrayInfo: lrIdx01RowLen         <%ld>",prpArrayInfo->lrIdx01RowLen);
	dbg(ipDebugLevel,"DebugPrintArrayInfo: pcrIdx01RowBuf        <%8.8x>",prpArrayInfo->pcrIdx01RowBuf);

	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos      <%8.8x>",prpArrayInfo->plrIdx01FieldPos);

	for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
	{
		dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldPos[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldPos[ilLoop]);
	}/* end of for */

	dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd      <%8.8x>",prpArrayInfo->plrIdx01FieldOrd);

	for(ilLoop = 0 ; ilLoop < prpArrayInfo->lrIdx01FieldCnt ; ilLoop++)
	{
		dbg(ipDebugLevel,"DebugPrintArrayInfo: plrIdx01FieldOrd[%3.3d] <%ld>",ilLoop,prpArrayInfo->plrIdx01FieldOrd[ilLoop]);
	}/* end of for */

} /* end of DebugPrintArrayInfo */


	

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpTana, char *pcpFieldList, long *plpLen)
{
	int	 ilRc        = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop      = 0;
	long llFldLen    = 0;
	long llRowLen    = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	dbg(TRACE,"GetRowLength: Tana <%s> FieldList <%s> NoOfItems <%ld>",pcpTana,pcpFieldList,ilNoOfItems);
	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
	
		if(ilRc > 0)
		{
			ilRc = RC_SUCCESS;
			if(GetFieldLength(pcpTana,&clFina[0],&llFldLen) == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */

		}/* end of if */
		ilLoop++;
	}while(ilLoop <= ilNoOfItems);


	*plpLen = llRowLen;

	dbg(TRACE,"GetRowLength: llRowLen <%ld>",llRowLen);

	return(ilRc);
	
} /* end of GetRowLength */

static int GetLogicalRowLength(char *pcpTotalFieldList,char *pcpFieldList, long *plpFieldSizes,long *plpLen)
{
	int	 ilRc        = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilItemNo = 0;
	int  ilLoop      = 0;
	long llRowLen    = 0;
	char clFina[8];
	
	if (pcpTotalFieldList != NULL && pcpFieldList != NULL)
	{
		ilNoOfItems = get_no_of_items(pcpFieldList);

		
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);

			if(GetItemNo(clFina,pcpTotalFieldList,&ilItemNo) == RC_SUCCESS) 
			{
				llRowLen++;
				llRowLen += plpFieldSizes[ilItemNo-1];
				dbg(TRACE,"GetLogicalRowLength:  clFina <%s> plpFieldSizes[ilItemNo-1] <%ld>",clFina,plpFieldSizes[ilItemNo-1]);
			}

			ilLoop++;
		}while(ilLoop <= ilNoOfItems);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(TRACE,"GetLogicalRowLength:  FieldList <%s> plpLen <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} /* end of GetLogicalRowLength */



/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpTana, char *pcpFina, long *plpLen)
{
  int  ilRc  = RC_SUCCESS;			/* Return code */
  int  ilCnt = 0;
  char clTaFi[32];
  char clFele[16];
  char clFldLst[16];

  ilCnt = 1;
  sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
  sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt - konstante nicht m�glich */

 
  ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
  switch (ilRc)
    {
    case RC_SUCCESS :
      *plpLen = atoi(&clFele[0]);
      /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
      break;

    case RC_NOT_FOUND :

	  dbg(TRACE,"GetFieldLength: pcpTana <%s> pcpFina<%s>",pcpTana,pcpFina);
      dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
      break;

    case RC_FAIL :
      dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
      break;

    default :
      dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
      break;
    }/* end of switch */

  return(ilRc);
	
} /* end of GetFieldLength */



/******************************************************************************/
/******************************************************************************/


/* 20000619 bch start */
static int GetItemData(char *pcpFieldName,char *pcpFields,char *pcpData,char *pcpDest)
{
	int ilRc = RC_FAIL;
	int ilLc;
	int ilItemCount;
	char pclFieldName[1200];

	ilItemCount = get_no_of_items(pcpFields);

	for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
	{
		get_real_item(pclFieldName,pcpFields,ilLc);
		if(!strcmp(pclFieldName,pcpFieldName))
		{
				get_real_item(pcpDest,pcpData,ilLc);
				dbg(DEBUG,"%05d: %s <%s>",ilLc,pclFieldName,pcpDest);
				ilRc = RC_SUCCESS;
		}
	}

	return ilRc;
}
/* 20000619 bch end */

static int GetItemNo(char *pcpFieldName,char *pcpFields,
		int *pipItemNo)
{
	int ilRc = RC_FAIL;
	int ilLc;
	int ilItemCount;
	char pclFieldName[1200];

	*pipItemNo = 0;

	ilItemCount = get_no_of_items(pcpFields);

	dbg(DEBUG,"GetItemNo itemCount = %d",ilItemCount);	
	for(ilLc = 1; ilLc <= ilItemCount && ilRc != RC_SUCCESS; ilLc++)
	{
		get_real_item(pclFieldName,pcpFields,ilLc);
		if(!strcmp(pclFieldName,pcpFieldName))
		{
				*pipItemNo = ilLc;
				dbg(DEBUG,"%05d: %s",ilLc,pclFieldName);
				ilRc = RC_SUCCESS;
		}
	}

	return ilRc;
}


static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer)
{
	
	char pclSection[124];
	char pclKeyword[124];

	strcpy(pclSection,pcpSection);
	strcpy(pclKeyword,pcpKeyword);

	return iGetConfigEntry(cgConfigFile,pclSection,pclKeyword,CFG_STRING,pcpCfgBuffer);
}

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];

/*	dbg(DEBUG,"StrToTime, <%s>",pcpTime);*/
	
	if (strlen(pcpTime) < 12 /*|| (atol(pcpTime) <= 0)*/)
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */

	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
/*
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
*/
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(DEBUG,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
		/*now +=  + 3600;*/
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
	struct tm *_tm;
	
	_tm = (struct tm *)localtime(&lpTime);
	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
			_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
			_tm->tm_min,_tm->tm_sec);
	return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               




/******************************************************************************/ 
/******************************************************************************/ 
static int TriggerAction(char *pcpTableName,char *pcpSendCommand,char *pcpCommands,char *pcpFields) 
{ 
  int           ilRc          = RC_SUCCESS ;    /* Return code */ 
  EVENT        *prlEvent      = NULL ; 
  BC_HEAD      *prlBchead     = NULL ; 
  CMDBLK       *prlCmdblk     = NULL ; 
  ACTIONConfig *prlAction     = NULL ; 
  char         *pclSelection  = NULL ; 
  char         *pclFields     = NULL ; 
  char         *pclData       = NULL ; 
  long          llActSize     = 0 ; 
  int           ilAnswerQueue = 0 ; 
  char          clQueueName[16] ; 
	 
  if(ilRc == RC_SUCCESS) 
  { 
 
  }/* end of if */ 
	 
 
  if (ilRc == RC_SUCCESS) 
  { 
   sprintf (&clQueueName[0],"%s2", mod_name) ; 
 
   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]); 
   if (ilRc != RC_SUCCESS) 
   { 
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc); 
   } 
   else 
   { 
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]); 
   }/* end of if */ 
  }/* end of if */ 
	 
  if (ilRc == RC_SUCCESS) 
  { 
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666; 
   prgOutEvent = realloc(prgOutEvent,llActSize); 
   if(prgOutEvent == NULL) 
   { 
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize); 
    ilRc = RC_FAIL ; 
   } /* end of if */ 
  } /* end of if */ 
 
  if(ilRc == RC_SUCCESS) 
  { 
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ; 
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ; 
   pclSelection  = prlCmdblk->data ; 
   pclFields     = pclSelection + strlen (pclSelection) + 1 ; 
   pclData       = pclFields + strlen (pclFields) + 1 ; 
   strcpy (pclData, "DYN") ; 
   prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ; 
		 
   prgOutEvent->type        = USR_EVENT ; 
   prgOutEvent->command     = EVENT_DATA ; 
   prgOutEvent->originator  = ilAnswerQueue ; 
   prgOutEvent->retry_count = 0 ; 
   prgOutEvent->data_offset = sizeof(EVENT) ; 
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ; 
 
   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ; 
   prlAction->iIgnoreEmptyFields = 0 ; 
   if(pcpSendCommand != NULL)
   {
	strcpy(prlAction->pcSndCmd, pcpSendCommand) ; 
   }
   else
   {
	   strcpy(prlAction->pcSndCmd,"") ; 
   }
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ; 
   strcpy(prlAction->pcTableName, pcpTableName) ; 
	if(pcpFields != NULL)
	{
		strcpy(prlAction->pcFields, pcpFields) ; 
	}
	else
	{
		strcpy(prlAction->pcFields, "") ; 
	}
   strcpy(prlAction->pcSectionCommands, pcpCommands) ;    
    prlAction->iModID = mod_id ;   
   /**************************************** 
     DebugPrintEvent(DEBUG,prgOutEvent) ; 
     DebugPrintBchead(DEBUG,prlBchead) ; 
     DebugPrintCmdblk(DEBUG,prlCmdblk) ; 
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ; 
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ; 
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ; 
     DebugPrintACTIONConfig(DEBUG,prlAction) ; 
   ****************************************/ 
 
   if(ilRc == RC_SUCCESS) 
   { 
    prlAction->iADFlag = iDELETE_SECTION ; 
 
    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ;  
    if(ilRc != RC_SUCCESS) 
    { 
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ; 
    } 
    else 
    { 
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ; 
     if(ilRc != RC_SUCCESS) 
     { 
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ; 
     } 
     else 
     { 
      /* return value for delete does not matter !!! */ 
		 
      /**************************************** 
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ; 
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ; 
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ; 
      pclSelection = (char *)    prlCmdblk->data ; 
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1; 
      pclData      = (char *)    pclFields + strlen(pclFields) + 1; 
      DebugPrintItem(DEBUG, prgItem) ; 
      DebugPrintEvent(DEBUG, prlEvent) ; 
      DebugPrintBchead(DEBUG, prlBchead) ; 
      DebugPrintCmdblk(DEBUG, prlCmdblk) ; 
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ; 
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ; 
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ; 
      ****************************************/ 
    } /* end of if */ 
   }/* end of if */ 
  }/* end of if */ 
 
  if(ilRc == RC_SUCCESS) 
  { 
   prlAction->iADFlag = iADD_SECTION ; 
 
   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, 
                                              (char *) prgOutEvent ) ; 
   if(ilRc != RC_SUCCESS) 
   { 
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ; 
   } 
   else 
   { 
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ; 
    if(ilRc != RC_SUCCESS) 
    { 
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ; 
    } 
    else 
    { 
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ; 
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ; 
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ; 
     pclSelection = (char *)    prlCmdblk->data ; 
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ; 
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ; 
 
     if(strcmp(pclData,"SUCCESS") != 0) 
     { 
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ; 
      DebugPrintItem(DEBUG,prgItem) ; 
      DebugPrintEvent(DEBUG,prlEvent) ; 
      DebugPrintBchead(DEBUG,prlBchead) ; 
      DebugPrintCmdblk(DEBUG,prlCmdblk) ; 
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ; 
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ; 
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ; 
      ilRc = RC_FAIL ; 
     } 
     else 
     { 
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ; 
     }/* end of if */ 
    }/* end of if */ 
   }/* end of if */ 
  }/* end of if */ 
 
  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ; 
  if(ilRc != RC_SUCCESS) 
  { 
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ; 
  } 
  else 
  { 
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ; 
  }/* end of if */ 
 }/* end of if */ 
 
 return (ilRc) ; 
	 
} /* end of TriggerAction */ 
 


static int SendToIVSIF(char *pcpObject,char *pcpSelection,
			char *pcpFields,char *pcpData)
{
	int			ilRc;
	BC_HEAD		rlBCHead;
	CMDBLK		rlCmdblk;

	dbg(TRACE,"<SendToIVSIF> ----- START -----");
	dbg(TRACE,"<SendToIVSIF> DATA <%s>",pcpData);

	/* set BC-Head members */
	
	memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
	rlBCHead.rc = RC_SUCCESS;
	strncpy(rlBCHead.dest_name, "IVSHDL", 10);
	strncpy(rlBCHead.recv_name, "IVSHDL", 10);

	/* set CMDBLK members */
	memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
	strncpy(rlCmdblk.command, "IVS", 6);
	strncpy(rlCmdblk.obj_name, pcpObject, 10);
	sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, "IVSHDL");
	/* write request to QUE */

	if ((ilRc = SendToQue(prgEvent->originator, PRIORITY_3, &rlBCHead, &rlCmdblk,
			pcpSelection,pcpFields,pcpData)) != RC_SUCCESS)
	{
		dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRc);
		dbg(DEBUG," ----- END -----");
	}

	dbg(TRACE,"<SendToIVSIF> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
							CMDBLK *prpCmdblk, char *pcpSelection, 
							char *pcpFields, char *pcpData)
{
	int			ilRc;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

	dbg(DEBUG,"<SendToQue> ----- START -----");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
			 strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20; 

	dbg(DEBUG,"<SendToQue> ilLen <%ld>",ilLen);
	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
		dbg(DEBUG,"<SendToQue> ----- END -----");
		return RC_FAIL;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
	prlOutEvent->type 		 = SYS_EVENT;
	prlOutEvent->command 	 = EVENT_DATA;
	prlOutEvent->originator  = mod_id;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

	/* Selection */
	strcpy(prlOutCmdblk->data, pcpSelection);
	/* fields */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);


	/* data */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);


	/* send this message */
	dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
	if ((ilRc = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRc);
		return RC_FAIL;
	}

	/* delete memory */
	free((void*)prlOutEvent);

	dbg(DEBUG,"<SendToQue> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}



/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(min(ipSleep,1));
	
	exit(0);
	
} /* end of Terminate */
 

/******************************************************************************/
/******************************************************************************/
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
	int ilRc          = RC_SUCCESS;                      /* Return code */
	int ilWaitCounter = 0;

	do
	{
		sleep(1);
		ilWaitCounter += 1;

		ilRc = CheckQueue(ipModId,prpItem);
		if(ilRc != RC_SUCCESS)
		{
			if(ilRc != QUE_E_NOMSG)
			{
				dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
			}/* end of if */
		}/* end of if */
	}while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

	if(ilWaitCounter >= ipTimeout)
	{
		dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
		ilRc = RC_FAIL;
	}/* end of if */
	return ilRc;

}/* end of WaitAndCheckQueue */
 

/******************************************************************************/
static int CheckQueue(int ipModId, ITEM **prpItem)
{
	int     ilRc       = RC_SUCCESS;                      /* Return code */
	int     ilItemSize = 0;
	EVENT *prlEvent    = NULL;
	
	ilItemSize = I_SIZE;
	
	ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) prpItem);
	if(ilRc == RC_SUCCESS)
	{
		prlEvent = (EVENT*) ((*prpItem)->text);

		switch( prlEvent->command )
		{
			case    HSB_STANDBY :
				dbg(TRACE,"CheckQueue: HSB_STANDBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_COMING_UP   :
				dbg(TRACE,"CheckQueue: HSB_COMING_UP");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACTIVE  :
				dbg(TRACE,"CheckQueue: HSB_ACTIVE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACT_TO_SBY  :
				dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_DOWN    :
				dbg(TRACE,"CheckQueue: HSB_DOWN");
				ctrl_sta = prlEvent->command;
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;
		
		case    HSB_STANDALONE  :
				dbg(TRACE,"CheckQueue: HSB_STANDALONE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    SHUTDOWN    :
				dbg(TRACE,"CheckQueue: SHUTDOWN");
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;
	
			case    RESET       :
				ilRc = Reset();
				break;
	
			case    EVENT_DATA  :
				/* DebugPrintItem(DEBUG,prgItem); */
				/* DebugPrintEvent(DEBUG,prlEvent); */
				break;
	
			case    TRACE_ON :
				dbg_handle_debug(prlEvent->command);
				break;
	
			case    TRACE_OFF :
				dbg_handle_debug(prlEvent->command);
				break;


			default         :
				dbg(TRACE,"CheckQueue: unknown event");
				DebugPrintItem(TRACE,*prpItem);
				DebugPrintEvent(TRACE,prlEvent);
				break;
	
		} /* end switch */
	
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		if( ilRc != RC_SUCCESS )
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */
	}else{
		if(ilRc != QUE_E_NOMSG)
		{
			dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
			HandleQueErr(ilRc);
		}/* end of if */
	}/* end of if */
       
	return ilRc;
}/* end of CheckQueue */


static void TrimRight(char *pcpBuffer)
{
	char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
	while(isspace(*pclBlank) && pclBlank != pcpBuffer)
	{
		*pclBlank = '\0';
		pclBlank--;
	}
}

void StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

	time_t ilTime;

	StrToTime(pcpBase,&ilTime);
	ilTime += (time_t)(atoi(pcpOffs));
	TimeToStr(pcpDest,ilTime);

	return ;
} /* end of StrSubTime */

static int UtcToLocal(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clLocal[32];

	strcpy(clLocal,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
	StrAddTime(pcpTime,pclTdi,pcpTime);
	return ilRc;
}


/******************************************************************************/
/******************************************************************************/
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRC       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;
	long llRowCount;
	int ilCount = 0;
  char  *pclTestBuf = NULL ;


  dbg (DEBUG,"SaveIndexInfo: SetArrayInfo <%s>",prpArrayInfo->crArrayName) ;

  if (ilRC == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (DEBUG, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRC = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRC == RC_SUCCESS)
  {
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

	dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
	*prpArrayInfo->pcrIdx01RowBuf = '\0';
   llRow = ARR_FIRST ;
   do
   {
    ilRC = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRC == RC_SUCCESS)
    {
    
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
    

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRC = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRC == RC_SUCCESS)
      {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRC = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRC > 0)
      {
       ilRC = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (DEBUG, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC) ;
    } /* end of if */
   } while (ilRC == RC_SUCCESS) ;

   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;

	 CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),&(prpArrayInfo->crArrayName[0]),&llRowCount);

	llRow = ARR_FIRST;
	dbg(DEBUG,"Array  <%s> data follows Rows %ld",
			prpArrayInfo->crArrayName,llRowCount);
	do
	{
			ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
			&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
			if (ilRC == RC_SUCCESS)
			{
				for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt; ilCount++)
				{
				
					fprintf (prlFile,"<%s>",&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ; 
					fflush(prlFile) ;
					dbg (DEBUG, "SaveIndexInfo: ilCount <%ld> <%s>", ilCount,&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]) ;
				}
				fprintf (prlFile,"\n") ; 
				dbg (DEBUG, "\n") ;
				fflush(prlFile) ;
				llRow = ARR_NEXT;
			}
			else
			{
				dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
			}
	} while (ilRC == RC_SUCCESS);

   if (ilRC == RC_NOTFOUND)
   {
    ilRC = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
  
    dbg (DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
  
  } /* end of if */

  return (ilRC) ;

}/* end of SaveIndexInfo */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Interne Funktionen														  */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

static int InitializeLidStatRow(char *pcpLidStatBuf)
{
	int ilRc = RC_SUCCESS;
	
	memset(pcpLidStatBuf,0,rgLidStatArray.lrArrayRowLen);

	strcpy(LIDSTATFIELD(pcpLidStatBuf,igLidStatPeno)," ");
	strcpy(LIDSTATFIELD(pcpLidStatBuf,igLidStatUstf)," ");
	strcpy(LIDSTATFIELD(pcpLidStatBuf,igLidStatUjob)," ");
	strcpy(LIDSTATFIELD(pcpLidStatBuf,igLidStatLidn)," ");
	strcpy(LIDSTATFIELD(pcpLidStatBuf,igLidStatStat)," ");
	strcpy(LIDSTATFIELD(pcpLidStatBuf,igLidStatMsta)," ");
	return ilRc;
}

static int InitializeLidMsgRow(char *pcpLidMsgBuf)
{
	int ilRc = RC_SUCCESS;
	
	memset(pcpLidMsgBuf,0,rgLidStatArray.lrArrayRowLen);

	strcpy(LIDSTATFIELD(pcpLidMsgBuf,igLidMsgLidn)," ");
	strcpy(LIDSTATFIELD(pcpLidMsgBuf,igLidMsgMsgt)," ");
	return ilRc;
}

static int InitializeMenuRow(char *pcpMenuBuf)
{
	int ilRc = RC_SUCCESS;
	
	memset(pcpMenuBuf,0,rgMenuArray.lrArrayRowLen);

	strcpy(MENUFIELD(pcpMenuBuf,igMenuLidn)," ");
	strcpy(MENUFIELD(pcpMenuBuf,igMenuMtxt)," ");
	strcpy(MENUFIELD(pcpMenuBuf,igMenuPval)," ");
	return ilRc;
}

static int InitializeInfJobRow(char *pcpInfJobBuf)
{
	int ilRc = RC_SUCCESS;
	
	memset(pcpInfJobBuf,0,rgInfJobArray.lrArrayRowLen);

	strcpy(INFJOBFIELD(pcpInfJobBuf,igInfJobUjob)," ");
	strcpy(INFJOBFIELD(pcpInfJobBuf,igInfJobCflg),"0");
	return ilRc;
}

static int InitializePatternRow(char *pcpPatternBuf)
{
	int ilRc = RC_SUCCESS;
	
	memset(pcpPatternBuf,0,rgPatternArray.lrArrayRowLen);

	strcpy(PATTERNFIELD(pcpPatternBuf,igPatternJtyp)," ");
	strcpy(PATTERNFIELD(pcpPatternBuf,igPatternAloc)," ");
	strcpy(PATTERNFIELD(pcpPatternBuf,igPatternText)," ");
	return ilRc;
}

static int InitializePatternTypeRow(char *pcpPatternTypeBuf)
{
	int ilRc = RC_SUCCESS;
	
	memset(pcpPatternTypeBuf,0,rgPatternTypeArray.lrArrayRowLen);

	strcpy(PATTERNTYPEFIELD(pcpPatternTypeBuf,igPatternTypeJtyp)," ");
	strcpy(PATTERNTYPEFIELD(pcpPatternTypeBuf,igPatternTypeAloc)," ");
	strcpy(PATTERNTYPEFIELD(pcpPatternTypeBuf,igPatternTypeText)," ");
	return ilRc;
}

static int InitializeFieldsRow(char *pcpFieldsBuf,char *pcpField)
{
	int ilRc = RC_SUCCESS;
	
	memset(pcpFieldsBuf,' ',rgFieldsArray.lrArrayRowLen);

	strcpy(FIELDSFIELD(pcpFieldsBuf,igFieldsFnam),pcpField);
	
	return ilRc;
}

/******************************************************************************/
/******************************************************************************/
static void SetFieldsArray()
{
	char clTmpFieldVal[20];
	long llNext = ARR_NEXT;
	int ilCount;
	int ilNoOfFields = get_no_of_items(cgJobChangeFields);
	for(ilCount = 0; ilCount < ilNoOfFields; ilCount++)
	{
		
		if(GetDataItem(clTmpFieldVal,cgJobChangeFields,ilCount+1,',',""," \0") >= 0)
		{
			llNext = ARR_NEXT;
			dbg(TRACE,"SetFieldsArray clTmpFieldVal <%s>",clTmpFieldVal);
			InitializeFieldsRow(cgFieldsBuf,clTmpFieldVal);

			AATArrayAddRow(&(rgFieldsArray.rrArrayHandle),&(rgFieldsArray.crArrayName[0]),&llNext,(void *)cgJobChangeFields);
			AATArrayPutField(&(rgFieldsArray.rrArrayHandle),
									 &(rgFieldsArray.crArrayName[0]),NULL,"FNAM",llNext,TRUE,
									 clTmpFieldVal) ;

			
		}
	}

}

static int PrepareJobData(char *pcpJobUrno)
{
	int ilRc = RC_SUCCESS;
	long llAction = ARR_FIRST;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char *pclJtyRow = NULL;
	char *pclAloRow = NULL;
	char *pclAftRow = NULL;
	char *pclCgvRow = NULL;
	char clAlcx[4];

	while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
								&(rgJobArray.crArrayName[0]),
								&(rgJobArray.rrIdx02Handle),
								&(rgJobArray.crIdx02Name[0]),
								pcpJobUrno,&llAction,
									(void *) &pclJobRow ) == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJtyArray.rrArrayHandle),
								&(rgJtyArray.crArrayName[0]),
								&(rgJtyArray.rrIdx02Handle),
								&(rgJtyArray.crIdx02Name[0]),
								JOBFIELD(pclJobRow,igJobUjty),&llRowNum,
									(void *) &pclJtyRow ) == RC_SUCCESS)
		{
			CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"JTYP",
									llAction,JTYFIELD(pclJtyRow,igJtyName)) ;
		}

		llRowNum = ARR_FIRST;
		dbg(TRACE,"PrepareJobData Ualo <%s> ",JOBFIELD(pclJobRow,igJobUalo));

		if(CEDAArrayFindRowPointer(&(rgAloArray.rrArrayHandle),
								&(rgAloArray.crArrayName[0]),
								&(rgAloArray.rrIdx01Handle),
								&(rgAloArray.crIdx01Name[0]),
								JOBFIELD(pclJobRow,igJobUalo),&llRowNum,
									(void *) &pclAloRow ) == RC_SUCCESS)
		{
			dbg(TRACE,"PrepareJobData Aloc <%s> ",ALOFIELD(pclAloRow,igAloAloc));
			CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"ALOC",
									llAction,ALOFIELD(pclAloRow,igAloAloc)) ;
		}
		llRowNum = ARR_FIRST;

		dbg(TRACE,"PrepareJobData Uaid <%s> ",JOBFIELD(pclJobRow,igJobUaid));

		if(CEDAArrayFindRowPointer(&(rgCgvArray.rrArrayHandle),
								&(rgCgvArray.crArrayName[0]),
								&(rgCgvArray.rrIdx01Handle),
								&(rgCgvArray.crIdx01Name[0]),
								JOBFIELD(pclJobRow,igJobUaid),&llRowNum,
									(void *) &pclCgvRow ) == RC_SUCCESS)
		{
			dbg(TRACE,"PrepareJobData Name <%s> ",CGVFIELD(pclCgvRow,igCgvName));
			CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"ALID",
									llAction,CGVFIELD(pclCgvRow,igCgvName)) ;
		}
	
		llRowNum = ARR_FIRST;
		dbg(TRACE,"PrepareJobData Uaft <%s> ",JOBFIELD(pclJobRow,igJobUaft));

		if(atol(JOBFIELD(pclJobRow,igJobUaft)) > 0)
		{
			if(CEDAArrayFindRowPointer(&(rgAftArray.rrArrayHandle),
									&(rgAftArray.crArrayName[0]),
									&(rgAftArray.rrIdx01Handle),
									&(rgAftArray.crIdx01Name[0]),
									JOBFIELD(pclJobRow,igJobUaft),&llRowNum,
										(void *) &pclAftRow ) == RC_SUCCESS)
			{
				strcpy(clAlcx,AFTFIELD(pclAftRow,igAftAlc2));

				TrimRight(clAlcx);
				if (strlen(clAlcx) <= 1)
				{
					strcpy(clAlcx,AFTFIELD(pclAftRow,igAftAlc3));
					TrimRight(clAlcx);
				}

				dbg(TRACE,"PrepareJobData Alcx <%s> Fltn <%s>",clAlcx,AFTFIELD(pclAftRow,igAftFltn));


				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"ALCX",
										llAction,clAlcx) ;
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"FLTN",
										llAction,AFTFIELD(pclAftRow,igAftFltn));
			}
		}


		llAction = ARR_NEXT;
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayWriteDB(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),
											NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}


	return ilRc;
}


static int AddInfJob(char *pcpUjob)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	long llNext = ARR_NEXT;
	char *pclInfJobRow = NULL;

	ilRc = AATArrayFindRowPointer(&(rgInfJobArray.rrArrayHandle),
														&(rgInfJobArray.crArrayName[0]),
														&(rgInfJobArray.rrIdx01Handle),
														&(rgInfJobArray.crIdx01Name[0]),
														pcpUjob,&llRowNum,
														(void *) &pclInfJobRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayPutField(&(rgInfJobArray.rrArrayHandle),
										&(rgInfJobArray.crArrayName[0]),NULL,"CFLG",llRowNum,TRUE,"0");

	}
	if(ilRc == RC_NOTFOUND)
	{
		InitializeInfJobRow(cgInfJobBuf);
		ilRc = AATArrayAddRow(&(rgInfJobArray.rrArrayHandle),&(rgInfJobArray.crArrayName[0]),&llNext,(void *)cgInfJobBuf);
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgInfJobArray.rrArrayHandle),
										&(rgInfJobArray.crArrayName[0]),NULL,"UJOB",llNext,TRUE,pcpUjob) ;
			ilRc = AATArrayPutField(&(rgInfJobArray.rrArrayHandle),
										&(rgInfJobArray.crArrayName[0]),NULL,"CFLG",llNext,TRUE,"0") ;
		}
	}

	return ilRc;
}

static int RemoveInfJob(char *pcpUjob)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclInfJobRow = NULL;

	ilRc = AATArrayFindRowPointer(&(rgInfJobArray.rrArrayHandle),
														&(rgInfJobArray.crArrayName[0]),
														&(rgInfJobArray.rrIdx01Handle),
														&(rgInfJobArray.crIdx01Name[0]),
														pcpUjob,&llRowNum,
														(void *) &pclInfJobRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayDeleteRow(&(rgInfJobArray.rrArrayHandle),&(rgInfJobArray.crArrayName[0]),llRowNum);
	}

	return ilRc;
}

static int SetLidStat(char *pcpLineID, char *pcpStatus)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclLidStatRow = NULL;

	dbg(TRACE,"SetLidStat pcpLineID <%s> pcpStatus <%s>",pcpLineID,pcpStatus);
	ilRc = AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"STAT",llRowNum,TRUE,pcpStatus);
	}
	return ilRc;
}

static int GetLidStat(char *pcpLineID, int *pipStatus)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclLidStatRow = NULL;

	*pipStatus = -1;

	ilRc = AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow );
	if(ilRc == RC_SUCCESS)
	{
		*pipStatus = atoi(LIDSTATFIELD(pclLidStatRow,igLidStatStat));
	}
	dbg(TRACE,"GetLidStat pcpLineID <%s> pipStatus <%d>",pcpLineID,*pipStatus);
	return ilRc;
}

static int SetLidMsg(char *pcpLineID, char *pcpMessage)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	long llNext = ARR_NEXT;
	char *pclLidMsgRow = NULL;

	ilRc = AATArrayFindRowPointer(&(rgLidMsgArray.rrArrayHandle),
														&(rgLidMsgArray.crArrayName[0]),
														&(rgLidMsgArray.rrIdx01Handle),
														&(rgLidMsgArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidMsgRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayPutField(&(rgLidMsgArray.rrArrayHandle),
										&(rgLidMsgArray.crArrayName[0]),NULL,"MSGT",llRowNum,TRUE,pcpMessage);
	}
	if(ilRc == RC_NOTFOUND)
	{
		InitializeLidMsgRow(cgLidMsgBuf);
		ilRc = AATArrayAddRow(&(rgLidMsgArray.rrArrayHandle),&(rgLidMsgArray.crArrayName[0]),&llNext,(void *)cgLidMsgBuf);
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidMsgArray.rrArrayHandle),
										&(rgLidMsgArray.crArrayName[0]),NULL,"LIDN",llNext,TRUE,pcpLineID) ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidMsgArray.rrArrayHandle),
										&(rgLidMsgArray.crArrayName[0]),NULL,"MSGT",llNext,TRUE,pcpMessage) ;
		}
	}

	return ilRc;
}

static int GetLidMsg(char *pcpLineID, char *pcpMessage)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclLidMsgRow = NULL;

	ilRc = AATArrayFindRowPointer(&(rgLidMsgArray.rrArrayHandle),
														&(rgLidMsgArray.crArrayName[0]),
														&(rgLidMsgArray.rrIdx01Handle),
														&(rgLidMsgArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidMsgRow );
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcpMessage,LIDMSGFIELD(pclLidMsgRow,igLidMsgMsgt));
	}
	return ilRc;
}

static int SetLidStatField(char *pcpLineID, char *pcpField,char *pcpValue)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclLidStatRow = NULL;

	ilRc = AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,pcpField,llRowNum,TRUE,pcpValue);
	}

	dbg(TRACE,"SetLidStatField pcpLineID <%s> pcpField <%s> pcpValue <%s> ilRc <%d>",pcpLineID,pcpField,pcpValue,ilRc);
	return ilRc;
}

static int GetLidStatField(char *pcpLineID, int ipFieldIdx,char *pcpValue)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclLidStatRow = NULL;

	ilRc = AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow );
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcpValue,LIDSTATFIELD(pclLidStatRow,ipFieldIdx));
	}
	return ilRc;
}

static int SetMenu(char *pcpLineID, char *pcpMessage,char *pcpPValues)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	long llNext = ARR_NEXT;
	char *pclMenuRow = NULL;

	dbg(TRACE,"SetMenu pcpLineID <%s> pcpMessage <%s> pcpPValues <%s> ",pcpLineID,pcpMessage,pcpPValues);


	ilRc = AATArrayFindRowPointer(&(rgMenuArray.rrArrayHandle),
														&(rgMenuArray.crArrayName[0]),
														&(rgMenuArray.rrIdx01Handle),
														&(rgMenuArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclMenuRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayPutField(&(rgMenuArray.rrArrayHandle),
										&(rgMenuArray.crArrayName[0]),NULL,"MTXT",llRowNum,TRUE,pcpMessage);

		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgMenuArray.rrArrayHandle),
										&(rgMenuArray.crArrayName[0]),NULL,"PVAL",llRowNum,TRUE,pcpPValues);
		}

	}
	if(ilRc == RC_NOTFOUND)
	{
		InitializeMenuRow(cgMenuBuf);
		ilRc = AATArrayAddRow(&(rgMenuArray.rrArrayHandle),&(rgMenuArray.crArrayName[0]),&llNext,(void *)cgMenuBuf);
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgMenuArray.rrArrayHandle),
										&(rgMenuArray.crArrayName[0]),NULL,"LIDN",llNext,TRUE,pcpLineID) ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgMenuArray.rrArrayHandle),
										&(rgMenuArray.crArrayName[0]),NULL,"MTXT",llNext,TRUE,pcpMessage) ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgMenuArray.rrArrayHandle),
										&(rgMenuArray.crArrayName[0]),NULL,"PVAL",llNext,TRUE,pcpPValues);
		}

	}

	dbg(TRACE,"SetMenu ilRc <%d> ",ilRc);

	return ilRc;
}

static int GetMenuText(char *pcpLineID, char *pcpMessage)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclMenuRow = NULL;

	ilRc = AATArrayFindRowPointer(&(rgMenuArray.rrArrayHandle),
														&(rgMenuArray.crArrayName[0]),
														&(rgMenuArray.rrIdx01Handle),
														&(rgMenuArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclMenuRow );
	if(ilRc == RC_SUCCESS)
	{
		strcpy(pcpMessage,MENUFIELD(pclMenuRow,igMenuMtxt));
		dbg(TRACE,"GetMenuText pcpMessage <%s> ",pcpMessage);
	}
	return ilRc;
}

static int CheckMenuKey(char *pcpLineID, char *pcpKey)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclMenuRow = NULL;
	ilRc = AATArrayFindRowPointer(&(rgMenuArray.rrArrayHandle),
														&(rgMenuArray.crArrayName[0]),
														&(rgMenuArray.rrIdx01Handle),
														&(rgMenuArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclMenuRow );
	if(ilRc == RC_SUCCESS)
	{
		if(strstr( MENUFIELD(pclMenuRow,igMenuPval), pcpKey) == NULL)
		{
			ilRc = RC_FAIL;
		}
	}
	return ilRc;
}

static int ProcessIVS( char *pcpData)
{
	int	ilRc        = RC_SUCCESS;             /* Return code */
	int ilCmd = -1;
	char pclCommand[24];
	char pclRealData[1024];
	char pclLineId[10];
	char pclPN[20];
	char pclStat[10];
	char pclNumber[10];
	char pclErrorText[100];

	dbg(TRACE,"<ProcessIVS> DATA <%s>",pcpData);
	ilRc = GetDataItem (pclRealData,pcpData,1, '>',""," \0");
	if (ilRc > 0)
	{
		ilRc = GetDataItem (pclCommand,pclRealData,2, ',',""," \0");
	}
	if (ilRc > 0)
	{
		ilCmd = atoi(pclCommand);
			
		if (ilCmd > 0)
		{
			ilRc = GetDataItem (pclLineId,pclRealData,3, ',',""," \0");
			if (ilRc > 0)
			{
				switch(ilCmd)
				{
				case 101:
					ilRc = GetDataItem (pclStat,pclRealData,4, ',',""," \0");
					if(ilRc > 0)
					{
						ilRc = Process101_DIAL(pclLineId,atoi(pclStat));
					}
					break;
				case 107:
					ilRc = ProcessInCall(pclLineId);
					break;
				case 113:
					ilRc = ProcessOutCall(pclLineId);
					break;
				case 106:
					ilRc = GetDataItem (pclPN,pclRealData,4, ',',""," \0");
					if(ilRc > 0)
					{
						ilRc = ProcessGetSelection(pclLineId,pclPN);
					}
					else
					{
						ProcessInCall(pclLineId);
					}
					break;
				case 103:
					ilRc = Process103_END(pclLineId);
					break;
				case 115:
					ilRc = Process115_TEXT(pclLineId);
					break;
				case 119:
					ilRc = Process119_TEXT(pclLineId);
					break;
				case 116:
					ilRc = GetDataItem (pclStat,pclRealData,4, ',',""," \0");
					if(ilRc > 0)
					{
						ilRc = Process116_TRANSFER(pclLineId,pclStat);
					}
					break;
				case 112:
					ilRc = GetDataItem (pclNumber,pclRealData,4, ',',""," \0");
					if(ilRc > 0)
					{
						ilRc = ProcessMenu(pclLineId,atoi(pclNumber));
					}
					else
					{
						ProcessInvalidMenu(pclLineId);
					}
					break;
				}
			}
			else
			{
				dbg(TRACE,"ProcessIVS GetDataItem pclLineId failed");
				ilRc = RC_FAIL;
			}
		}
		else
		{
			dbg(TRACE,"ProcessIVS GetCommand failed pclCommand <%s>",pclCommand);
			ilRc = RC_FAIL;
		}
	} 
	else
	{
		dbg(TRACE,"ProcessIVS GetDataItem  for pclCommand failed");
		ilRc = RC_FAIL;
	}

	if(ilRc != RC_SUCCESS)
	{

		sprintf(pclErrorText,"<ADAPT,115_TEXT,%s,%d,w992>",pclLineId,igLanguage);
		if(atoi(pclLineId) > 0)
		{
			SetLidStat(pclLineId, "0");
		}
		SendToIVSIF("IVS","","",pclErrorText);
	}
		
	dbg(TRACE,"ProcessIVS End");

	return(ilRc) ;

} /* ProcessIVS */

static int Process115_TEXT( char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilStat = -1;
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if (ilRc == RC_SUCCESS)
	{
		switch(ilStat)
		{
		case 1:
			ilRc = GetSelection6(pcpLineID);
			break;
		case 41	:
			ilRc = ProcessInvalidMenu(pcpLineID);
			break;
		case 3:
			ilRc = GetSelection6(pcpLineID);
			break;
		case 5:
			ilRc = Menu_2(pcpLineID);
			break;
		case 9:
			ilRc = Menu_1_4(pcpLineID);
			break;
		case 10:
			ilRc = Menu_2_4(pcpLineID);
			break;
		case 21:
			ilRc = HandleEndConnection(pcpLineID);
			break;			
		case 26:
			ilRc = Menu_1_2_4_5(pcpLineID);
			break;
		case 25:
			ilRc = HandleSendJobText(pcpLineID);
			break;
		case 34:
			ilRc = HandleConnectDispo(pcpLineID);
			break;
		case 36:
			ilRc = HandleEndConnection(pcpLineID);
			break;
		case 32:
			ilRc = HandleEndConnection(pcpLineID);
			break;
		}
	}
	return ilRc;
}
static int Process119_TEXT( char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilStat = -1;
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if (ilRc == RC_SUCCESS)
	{
		switch(ilStat)
		{
		case 23:
			ilRc = Menu_2_5(pcpLineID);
			break;
		case 40:
			ilRc = Menu_2_4(pcpLineID);
			break;
		case 51:
			ilRc = Menu_2_4(pcpLineID);
			break;
		}
	}
	return ilRc;
}



static int Process116_TRANSFER( char *pcpLineID,char *pcpStat)
{
	int ilRc = RC_FAIL;
	int ilStat = -1;
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if (ilRc == RC_SUCCESS)
	{
		switch(ilStat)
		{
		case 35:
			ilRc = Handle116_Transfer(pcpLineID,pcpStat);
			break;
		}
	}
	return ilRc;
}



static int ProcessMenu( char *pcpLineID,int ipNumber)
{
	int ilRc = RC_FAIL;
	int ilKeyRc = RC_FAIL;
	
	dbg(TRACE,"ProcessMenu pcpLineID <%s> ipNumber <%d> ",pcpLineID,ipNumber);

	switch(ipNumber)
	{
	case 1:
		ilKeyRc = CheckMenuKey(pcpLineID,"1"); 
		if(ilKeyRc == RC_SUCCESS)
		{
			ilRc = ProcessMenu1(pcpLineID);
		}
		break;
	case 2:
		ilKeyRc = CheckMenuKey(pcpLineID,"2"); 
		if(ilKeyRc == RC_SUCCESS)
		{
			ilRc = ProcessMenu2(pcpLineID);
		}
		break;
	case 3:
		ilKeyRc = CheckMenuKey(pcpLineID,"3"); 
		if(ilKeyRc == RC_SUCCESS)
		{
			ilRc = ProcessMenu3(pcpLineID);
		}
		break;
	case 4:
		ilKeyRc = CheckMenuKey(pcpLineID,"4"); 
		if(ilKeyRc == RC_SUCCESS)
		{
			ilRc = ProcessMenu4(pcpLineID);
		}
		break;
	case 5:
		ilKeyRc = CheckMenuKey(pcpLineID,"5"); 
		if(ilKeyRc == RC_SUCCESS)
		{
			ilRc = ProcessMenu5(pcpLineID);
		}
		break;
	}

	if(ilKeyRc != RC_SUCCESS)
	{
		ilRc = ProcessInvalidMenu(pcpLineID);
	}

	return ilRc;
}
static int ProcessInCall( char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	long llNext = ARR_NEXT;
	char *pclLidStatRow = NULL;
	char pclAnswerText[100];

	ilRc = AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"STAT",llRowNum,TRUE,"1") ;
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"PENO",llRowNum,TRUE," ") ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"USTF",llRowNum,TRUE," ") ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"UDSR",llRowNum,TRUE," ") ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"UJOB",llRowNum,TRUE," ") ;
		}
	}
	else if (ilRc == RC_NOTFOUND)
	{
		InitializeLidStatRow(cgLidStatBuf);
		ilRc = AATArrayAddRow(&(rgLidStatArray.rrArrayHandle),&(rgLidStatArray.crArrayName[0]),&llNext,(void *)cgLidStatBuf);
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"LIDN",llNext,TRUE,pcpLineID) ;
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1021>",pcpLineID,igLanguage);
		SetLidStat(pcpLineID, "1");
		SendToIVSIF("IVS","","",pclAnswerText);
	}

	return ilRc;
}

static int ProcessOutCall(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	long llNext = ARR_NEXT;
	char *pclLidStatRow = NULL;
	char pclAnswerText[100];

	ilRc = AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow );
	if(ilRc == RC_SUCCESS)
	{
		ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"STAT",llRowNum,TRUE,"1") ;
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"PENO",llRowNum,TRUE," ") ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"USTF",llRowNum,TRUE," ") ;
		}
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"UDSR",llRowNum,TRUE," ") ;
		}
	}
	else if (ilRc == RC_NOTFOUND)
	{
		InitializeLidStatRow(cgLidStatBuf);
		ilRc = AATArrayAddRow(&(rgLidStatArray.rrArrayHandle),&(rgLidStatArray.crArrayName[0]),&llNext,(void *)cgLidStatBuf);
		if(ilRc == RC_SUCCESS)
		{
			ilRc = AATArrayPutField(&(rgLidStatArray.rrArrayHandle),
										&(rgLidStatArray.crArrayName[0]),NULL,"LIDN",llNext,TRUE,pcpLineID) ;
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		/*Hier Muss die nummer zusammengbastelt werden.*/
		sprintf(pclAnswerText,"<ADAPT,101_DIAL,%s,8336>",pcpLineID,igLanguage);
		SetLidStat(pcpLineID, "42");
	
		SendToIVSIF("IVS","","",pclAnswerText);
	}

	return ilRc;
}

static int Process101_DIAL(char *pcpLineID,int ipStat)
{
	int ilRc = RC_FAIL;
	int ilStat = -1;
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if (ilRc == RC_SUCCESS)
	{
		switch(ilStat)
		{
		case 42:
			ilRc = Handle101_DIAL(pcpLineID,ipStat);
			break;
		}
	}
	return ilRc;
}

static int ProcessGetSelection( char *pcpLineID,char *pcpPN)
{
	int ilRc = RC_FAIL;
	int ilStat = 0;
	char pclStat[20];
	char pclAnswerText[100];
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if(ilRc == RC_SUCCESS)
	{
		ilStat++;
		sprintf(pclStat,"%d",ilStat);
		ilRc = SetLidStat(pcpLineID, pclStat);
		if(ilRc == RC_SUCCESS)
		{
			if(CheckPN(pcpLineID,pcpPN) == RC_SUCCESS)
			{
				if(CheckPoolJobStat(pcpLineID,0,'C') == RC_SUCCESS)
				{
					/*Employee Pool Job is confirmed*/
					if(CheckJobStat(pcpLineID,0,'C') == RC_SUCCESS)
					{
							/*Employee job is confirmed */
						HandleFinishJob(pcpLineID);
					}
					else
					{
						if(CheckJobStatInformed(pcpLineID) == RC_SUCCESS)
						{
							/*Employee job is informed */
							HandleConfirmJob(pcpLineID);

						}
						else
						{
							/*Employee job is not informed */
							Menu_1_4(pcpLineID);
							ilRc = SetLidStat(pcpLineID, "9");
						}
					}
				}
				else
				{
					/*Employee Pool Job is not confirmed*/
					HandleConfirmPoolJob(pcpLineID);
				}
			}
			else
			{
				/*PN is invalid*/
				if(	ilStat == 3)
				{
					sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1022,w1023>",pcpLineID,igLanguage);
				}
				else
				{
					sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1022,w1024>",pcpLineID,igLanguage);
				}

				SetLidMsg(pcpLineID, pclAnswerText);
			
				ilRc = SendToIVSIF("IVS","","",pclAnswerText);

			}
		}
	}

	return ilRc;
}
static int Process103_END( char *pcpLineID)
{
	int ilRc = RC_FAIL;
	char pclAnswerText[100];

	ilRc = SetLidStat(pcpLineID, "0");
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclAnswerText,"<ADAPT,118_ACK,%s>",pcpLineID);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	}
	return ilRc;
}

static int ProcessCheckJobChanged( char *pcpUjob)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclInfJobRow = NULL;
	BOOL blCheckJobChanged = FALSE;
	int ilItem = 0;
	ilRc = AATArrayFindRowPointer(&(rgInfJobArray.rrArrayHandle),
														&(rgInfJobArray.crArrayName[0]),
														&(rgInfJobArray.rrIdx01Handle),
														&(rgInfJobArray.crIdx01Name[0]),
														pcpUjob,&llRowNum,
														(void *) &pclInfJobRow );
	if(ilRc == RC_SUCCESS)
	{
		blCheckJobChanged = (GetItemNo("STAT",cgJobChangeFields,&ilItem)
											== RC_SUCCESS)?TRUE:FALSE;
		if((atoi(INFJOBFIELD(pclInfJobRow,igInfJobCflg)) == 0) && blCheckJobChanged)
		{
			ilRc = AATArrayPutField(&(rgInfJobArray.rrArrayHandle),
										&(rgInfJobArray.crArrayName[0]),NULL,"CFLG",llRowNum,TRUE,"1");
		}
		else
		{
			ilRc = AATArrayPutField(&(rgInfJobArray.rrArrayHandle),
										&(rgInfJobArray.crArrayName[0]),NULL,"CFLG",llRowNum,TRUE,"2");
		}

	}

	return ilRc;
}





static int GetSelection6(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];
	int ilStat = 0;
	char pclStat[10];
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if(ilRc == RC_SUCCESS)
	{
		ilStat++;
		sprintf(pclStat,"%d",ilStat);
		dbg(TRACE,"GetSelection6 pcpLineID <%s> ilStat <%d> pclStat <%s>",pcpLineID,ilStat,pclStat);
		ilRc = SetLidStat(pcpLineID, pclStat);
		if(ilRc == RC_SUCCESS)
		{
			sprintf(pclAnswerText,"<ADAPT,106_GET_SELECTION,%s,6>",pcpLineID);
			ilRc = SendToIVSIF("IVS","","",pclAnswerText);
		}
	}

	return ilRc;
}

static int RepeatGetSelection6(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,106_GET_SELECTION,%s,6>",pcpLineID);
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}


static int Menu_2_5(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,112_MENU,%s,%d,w1010,-,w1002,-,-,w1007>",pcpLineID,igLanguage);
	
	SetMenu(pcpLineID,pclAnswerText,"2,5");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}

static int Menu_2(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,112_MENU,%s,%d,w1010,-,w1002>",pcpLineID,igLanguage);
	
	SetMenu(pcpLineID,pclAnswerText,"2");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}

static int Menu_2_4(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,112_MENU,%s,%d,w1010,-,w1002,-,w1005>",pcpLineID,igLanguage);
	
	SetMenu(pcpLineID,pclAnswerText,"2,4");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}

static int Menu_1_4(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,112_MENU,%s,%d,w1010,w1001,-,-,w1005>",pcpLineID,igLanguage);
	
	SetMenu(pcpLineID,pclAnswerText,"1,4");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}


static int Menu_1_2_4_5(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,112_MENU,%s,%d,w1010,w1006,w1002,-,w1005,w1007>",pcpLineID,igLanguage);
	
	SetMenu(pcpLineID,pclAnswerText,"1,2,4,5");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}

static int Menu_1_3_4Break(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,112_MENU,%s,%d,w1010,w1006,-,w1004,w1005>",pcpLineID,igLanguage);
	
	SetMenu(pcpLineID,pclAnswerText,"1,3,4");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}

static int Menu_1_3_4Job(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	sprintf(pclAnswerText,"<ADAPT,112_MENU,%s,%d,w1010,w1006,-,w1003,w1005>",pcpLineID,igLanguage);
	
	SetMenu(pcpLineID,pclAnswerText,"1,3,4");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	return ilRc;
}

static int ProcessMenu1(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilStat = -1;
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if (ilRc == RC_SUCCESS)
	{
		switch(ilStat)
		{
		case 9:
			ilRc = HandleInformMa(pcpLineID);
			break;
		case 26:
			ilRc = HandleGetNextJob(pcpLineID,FALSE);
			break;
		case 41:
			ilRc = HandleGetNextJob(pcpLineID,TRUE);
			break;
		}
	}
	return ilRc;
}
/*Menu2 Ausgabe der letzten Nachricht*/
static int ProcessMenu2(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	ilRc = GetLidMsg(pcpLineID, pclAnswerText);
	if(ilRc == RC_SUCCESS)
	{
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	}
	return ilRc;
}
static int ProcessMenu3(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilStat = -1;
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if (ilRc == RC_SUCCESS)
	{
		switch(ilStat)
		{
		case 41:
			ilRc = HandleSetFinished(pcpLineID);
			break;
		}
	}
	return ilRc;
}
/*Menu4 Verbindung zur Dispo*/
static int ProcessMenu4(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];	
	sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1042>",pcpLineID,igLanguage);
	
	SetLidStat(pcpLineID, "34");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);

	return ilRc;
}
static int ProcessMenu5(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilStat = -1;
	ilRc = GetLidStat (pcpLineID,&ilStat);
	if (ilRc == RC_SUCCESS)
	{
		switch(ilStat)
		{
		case 23:
			ilRc = HandleSetInformed(pcpLineID);
			break;
		case 26:
			ilRc = HandleSetConfirmed(pcpLineID);
			break;
		}
	}
	return ilRc;
}

static int ProcessInvalidMenu(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	ilRc = GetMenuText(pcpLineID, pclAnswerText);
	if(ilRc == RC_SUCCESS)
	{
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	}
	return ilRc;
}

static int CheckPN(char *pcpLineID,char *pcpPeno)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclStfRow = NULL;
	char *pclDrrRow = NULL;
	time_t tlAvto;
	time_t tlAvfr;
	char clCurrTime[20];

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);

	if(CEDAArrayFindRowPointer(&(rgStfArray.rrArrayHandle),
														&(rgStfArray.crArrayName[0]),
														&(rgStfArray.rrIdx01Handle),
														&(rgStfArray.crIdx01Name[0]),
														pcpPeno,&llRowNum,
														(void *) &pclStfRow ) == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		while((CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
														&(rgDrrArray.crArrayName[0]),
														&(rgDrrArray.rrIdx02Handle),
														&(rgDrrArray.crIdx02Name[0]),
														STFFIELD(pclStfRow,igStfUrno),&llRowNum,
														(void *) &pclDrrRow ) == RC_SUCCESS) && (ilRc == RC_FAIL))
		{

			if(StrToTime(DRRFIELD(pclDrrRow,igDrrAvto),&tlAvto) == RC_SUCCESS)
			{
				if(StrToTime(DRRFIELD(pclDrrRow,igDrrAvfr),&tlAvfr) == RC_SUCCESS)
				{
					/* Check ob die Schicht g�ltig ist*/
					if((tgCurrTime  < tlAvto) && (tgCurrTime + 10*60*60 > tlAvfr) &&
						( atoi(DRRFIELD(pclDrrRow,igDrrRosl)) == 3)) 
					{
						/*Mitarbeiter ist jetzt registriert*/
						ilRc = SetLidStatField(pcpLineID,"PENO",pcpPeno);
						if(ilRc == RC_SUCCESS)
						{
							ilRc = SetLidStatField(pcpLineID,"USTF",DRRFIELD(pclDrrRow,igDrrStfu));
						}
						if(ilRc == RC_SUCCESS)
						{
							ilRc = SetLidStatField(pcpLineID,"UDSR",DRRFIELD(pclDrrRow,igDrrUrno));
						}					
					}
				}
			}
			llRowNum = ARR_NEXT;
		}
	}
	return ilRc;
}

static int CheckPoolJobStat(char *pcpLineID,int ipPos,char cpStat)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclLidStatRow = NULL;
	char *pclPoolJobRow = NULL;
	char clCurrTime[20];
	char pclStat[15];

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	if(AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow ) == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		while(CEDAArrayFindRowPointer(&(rgPoolJobArray.rrArrayHandle),
														&(rgPoolJobArray.crArrayName[0]),
														&(rgPoolJobArray.rrIdx02Handle),
														&(rgPoolJobArray.crIdx02Name[0]),
														LIDSTATFIELD(pclLidStatRow,igLidStatUdsr),&llRowNum,
														(void *) &pclPoolJobRow ) == RC_SUCCESS)
		{
			strcpy(pclStat,POOLJOBFIELD(pclPoolJobRow,igPoolJobStat));

			if(pclStat[ipPos] == cpStat)
			{
				ilRc = RC_SUCCESS;
			}
			llRowNum = ARR_NEXT;
		}
	}

	dbg(TRACE,"CheckPoolJobStat pcpLineID <%s> ilRc <%d>",pcpLineID,ilRc);

	return ilRc;
}

static int CheckJobStat(char *pcpLineID,int ipPos,char cpStat)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[15];
	char clValue[20];

	ilRc =  GetLidStatField(pcpLineID, igLidStatUstf,clValue);

	if(ilRc == RC_SUCCESS)
	{
		ilRc = RC_FAIL;
		llRowNum = ARR_FIRST;
		while(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx03Handle),
														&(rgJobArray.crIdx03Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS)
		{
			strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));
			if(pclStat[ipPos] == cpStat)
			{
				ilRc =  SetLidStatField(pcpLineID,"UJOB",JOBFIELD(pclJobRow,igJobUrno));
			}
			llRowNum = ARR_NEXT;
		}
	}
	dbg(TRACE,"CheckJobStat pcpLineID <%s> ilRc <%d>",pcpLineID,ilRc);
	return ilRc;
}


static int CheckJobStatInformed(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[20];
	char clValue[20];
	BOOL blJobFound = FALSE;
	time_t tlAcfr;
	char clCurrTime[20];

	
	GetServerTimeStamp("UTC", 1, 0, clCurrTime);
	StrToTime(clCurrTime,&tgCurrTime);

	ilRc =  GetLidStatField(pcpLineID, igLidStatUstf,clValue);

	if(ilRc == RC_SUCCESS)
	{
		ilRc = RC_FAIL;
		while((AATArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
															&(rgJobArray.crArrayName[0]),
															&(rgJobArray.rrIdx03Handle),
															&(rgJobArray.crIdx03Name[0]),
															clValue,&llRowNum,
															(void *) &pclJobRow ) == RC_SUCCESS) && !blJobFound)
		{
			strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));

			StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlAcfr);

		    if((tlAcfr >= tgCurrTime) && (pclStat[0] == 'P' && pclStat[4] != '1'))
			{
				blJobFound = TRUE;
				ilRc = RC_FAIL;
			}
			else if(pclStat[0] == 'P' && pclStat[4] == '1')
			{
				ilRc =  SetLidStatField(pcpLineID,"UJOB",JOBFIELD(pclJobRow,igJobUrno));
				blJobFound = TRUE;
			}

			llRowNum = ARR_NEXT;

		}
	}		
	dbg(TRACE,"CheckJobStatInformed pcpLineID <%s> ilRc <%d>",pcpLineID,ilRc);

	return ilRc;

}

static int HandleConnectDispo(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];
	char pclDispoNumber[30];
	
	ilRc = GetDispoNumber(pcpLineID,pclDispoNumber);
	if(ilRc == RC_SUCCESS)
	{
		TrimRight(pclDispoNumber);
		sprintf(pclAnswerText,"<ADAPT,116_TRANSFER,%s,%d,%s>",pcpLineID,igLanguage,pclDispoNumber);
		SetLidStat(pcpLineID, "35");
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	}
	return ilRc;
}

static int HandleEndConnection(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];
	sprintf(pclAnswerText,"<ADAPT,103_END,%s>",pcpLineID);
	
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);

	return ilRc;
}

static int Handle101_DIAL(char *pcpLineID,int ipStat)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];

	switch(ipStat)
	{
	case 0:
		/*ilRc = SendNextJobMessage(pcpLineID);*/
		break;
	case 2:
		ilRc = HandleEndConnection(pcpLineID);
	}	
	
	sprintf(pclAnswerText,"<ADAPT,103_END,%s>",pcpLineID);
	
	SetLidStat(pcpLineID, "37");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);

	return ilRc;
}

static int HandleConfirmPoolJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclLidStatRow = NULL;
	char *pclPoolJobRow = NULL;
	time_t tlAcfr;
	char clCurrTime[20];
	char clTime[10];
	char clDate[20];
	char pclAnswerText[100];

	dbg(DEBUG,"HandleConfirmPoolJob pcpLineID <%s>",pcpLineID);
	
	GetServerTimeStamp("UTC", 1, 0, clCurrTime);
	StrToTime(clCurrTime,&tgCurrTime);
	if(AATArrayFindRowPointer(&(rgLidStatArray.rrArrayHandle),
														&(rgLidStatArray.crArrayName[0]),
														&(rgLidStatArray.rrIdx01Handle),
														&(rgLidStatArray.crIdx01Name[0]),
														pcpLineID,&llRowNum,
														(void *) &pclLidStatRow ) == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		dbg(DEBUG,"HandleConfirmPoolJob LidStatUdsr <%s>",LIDSTATFIELD(pclLidStatRow,igLidStatUdsr));

		if(CEDAArrayFindRowPointer(&(rgPoolJobArray.rrArrayHandle),
														&(rgPoolJobArray.crArrayName[0]),
														&(rgPoolJobArray.rrIdx02Handle),
														&(rgPoolJobArray.crIdx02Name[0]),
														LIDSTATFIELD(pclLidStatRow,igLidStatUdsr),&llRowNum,
														(void *) &pclPoolJobRow ) == RC_SUCCESS)
		{
			StrToTime(POOLJOBFIELD(pclPoolJobRow,igPoolJobAcfr),&tlAcfr);
			dbg(DEBUG,"HandleConfirmPoolJob ACFR <%s> tlAcfr <%d>",POOLJOBFIELD(pclPoolJobRow,igPoolJobAcfr),tlAcfr);

			if(tlAcfr  > tgCurrTime + igOffSetEarlySBeg)
			{
				strcpy(clDate,POOLJOBFIELD(pclPoolJobRow,igPoolJobAcfr));
				UtcToLocal(clDate);

				ilRc = GetTimeString(clTime,clDate);
				sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1051,w1052,t%s>",pcpLineID,igLanguage,clTime);
				
				SetLidStat(pcpLineID, "10");
				SetLidMsg(pcpLineID, pclAnswerText);
				SendToIVSIF("IVS","","",pclAnswerText);
			}
			else if(tlAcfr < tgCurrTime - igOffSetLateSBeg)
			{
				strcpy(clDate,POOLJOBFIELD(pclPoolJobRow,igPoolJobAcfr));
				UtcToLocal(clDate);
				ilRc = GetTimeString(clTime,clDate);
				sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1051,w1053,t%s>",pcpLineID,igLanguage,clTime);
				
				SetLidStat(pcpLineID, "10");
				SetLidMsg(pcpLineID, pclAnswerText);
				SendToIVSIF("IVS","","",pclAnswerText);
			}
			else
			{
				ilRc = ConfirmPoolJob(pcpLineID);
				if(ilRc == RC_SUCCESS)
				{
					sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1031>",pcpLineID,igLanguage);
					
					SetLidStat(pcpLineID, "9");
					SetLidMsg(pcpLineID, pclAnswerText);
					SendToIVSIF("IVS","","",pclAnswerText);
				}
			}
		}
	}
	return ilRc;
}


static int ConfirmPoolJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char clCurrTime[20];
	char pclStat[15];
	char clValue[20];
	BOOL blFirstJob = TRUE;

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	ilRc = GetLidStatField(pcpLineID,igLidStatUdsr,clValue);

	if(ilRc == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgPoolJobArray.rrArrayHandle),
														&(rgPoolJobArray.crArrayName[0]),
														&(rgPoolJobArray.rrIdx02Handle),
														&(rgPoolJobArray.crIdx02Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow) == RC_SUCCESS)
		{
			


			strcpy(pclStat,POOLJOBFIELD(pclJobRow,igPoolJobStat));

			if(pclStat[0] == 'P')
			{
				if(blFirstJob && bgSetPoolJobAcfr)
				{
					CEDAArrayPutField(&(rgPoolJobArray.rrArrayHandle),&(rgPoolJobArray.crArrayName[0]),NULL,"ACFR",
											llRowNum,clCurrTime) ;
				}
				blFirstJob = FALSE;	
				pclStat[0] = 'C';
				CEDAArrayPutField(&(rgPoolJobArray.rrArrayHandle),&(rgPoolJobArray.crArrayName[0]),NULL,"STAT",
										llRowNum,pclStat) ;
				ilRc = RC_SUCCESS;
			}
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayWriteDB(&(rgPoolJobArray.rrArrayHandle),&(rgPoolJobArray.crArrayName[0]),
											NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}
	return ilRc;
}


static int SetJobInformed(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[15];
	char clValue[20];

	ilRc = GetLidStatField(pcpLineID,igLidStatUjob,clValue);

	if(ilRc == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx02Handle),
														&(rgJobArray.crIdx02Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS)
		{
			strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));

			pclStat[4] = '1';
				
			CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"STAT",
										llRowNum,pclStat) ;
			ilRc = AddInfJob(JOBFIELD(pclJobRow,igJobUrno));

			ilRc = RC_SUCCESS;
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayWriteDB(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),
											NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}

	return ilRc;
}

static int SetJobNotInformed(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[15];
	char clValue[20];

	ilRc = GetLidStatField(pcpLineID,igLidStatUjob,clValue);

	if(ilRc == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx02Handle),
														&(rgJobArray.crIdx02Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS)
		{
			strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));

			pclStat[4] = '0';
				
			CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"STAT",
										llRowNum,pclStat);			
			ilRc = RemoveInfJob(JOBFIELD(pclJobRow,igJobUrno));

			ilRc = RC_SUCCESS;
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayWriteDB(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),
											NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}

	return ilRc;
}

static int HandleSpecialJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	char pclAnswerText[100];
	long llRowNum = ARR_FIRST; 
	char clValue[20];
	char *pclJobRow = NULL;
	char clAcfr[20],clActo[20];
	char clDate[20];


	ilRc = SetJobInformed(pcpLineID);
	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclAnswerText,"<ADAPT,119_TEXT,%s,%d,w1061,w1114,w1024>",pcpLineID,igLanguage);
		ilRc =  GetLidStatField(pcpLineID, igLidStatUjob,clValue);

		if(ilRc == RC_SUCCESS)
		{
			llRowNum = ARR_FIRST;
			if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
															&(rgJobArray.crArrayName[0]),
															&(rgJobArray.rrIdx02Handle),
															&(rgJobArray.crIdx02Name[0]),
															clValue,&llRowNum,
															(void *) &pclJobRow ) == RC_SUCCESS)
			{

								
				strcpy(clDate,JOBFIELD(pclJobRow,igJobAcfr));
				UtcToLocal(clDate);
				ilRc = GetTimeString(clAcfr,clDate);
												
				strcpy(clDate,JOBFIELD(pclJobRow,igJobActo));
				UtcToLocal(clDate);
				ilRc = GetTimeString(clActo,clDate);
												
				sprintf(pclAnswerText,"<ADAPT,119_TEXT,%s,%d,w1061,w1114,w1065,t%s,w1066,t%s,w1024>",pcpLineID,igLanguage,clAcfr,clActo);
			}
		}
		SetLidStat(pcpLineID, "51");
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	}

	return ilRc;
}


static int HandleSendJobText(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;
	char pclAnswerText[100];
	char clValue1[20];
	char clValue2[20];
	char pclJobText[100];

	memset((void*)&pclJobText[0], 0x00, 100);

	if(CheckJobChanged(pcpLineID) == RC_SUCCESS)
	{
		ilJobRc = FindNextJob(pcpLineID);
			
		if(ilJobRc == RC_SUCCESS)
		{
			GetLidStatField(pcpLineID, igLidStatUjob,clValue1);
			GetLidStatField(pcpLineID, igLidStatUjbt,clValue2);
			if((atol(clValue1) == atol(clValue2)) || atol(clValue2) == 0)
			{

				ilRc = GetJobText(pcpLineID,pclJobText,TRUE);
				if(ilRc == RC_SUCCESS)
				{
					sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1241,%s>",pcpLineID,igLanguage,pclJobText);
				
					SetLidStat(pcpLineID, "26");
					SetLidMsg(pcpLineID, pclAnswerText);
					ilRc = SendToIVSIF("IVS","","",pclAnswerText);
				}
				else
				{
					/*Nicht konfigurierter Jobtyp oder Spezial Job*/
					sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1241,w1213>",pcpLineID,igLanguage);
				
					SetLidStat(pcpLineID, "26");
					SetLidMsg(pcpLineID, pclAnswerText);

					ilRc = SendToIVSIF("IVS","","",pclAnswerText);
				}
			}
			else
			{
				/*Asugabe: Ihr Auftrag hat sich ge�ndert*/
				sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
				
				SetLidStat(pcpLineID, "9");
				SetLidMsg(pcpLineID, pclAnswerText);
				ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
			}

		}
	}	
	else
	{
		/* JOB wurde seit letztem abfragen ge�ndert*/
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
		SetJobNotInformed(pcpLineID);
		SetLidStat(pcpLineID, "9");
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
	}	


	return ilRc;
}

static int HandleInformMa(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;
	char pclAnswerText[100];

	char pclJobText[100];

	memset((void*)&pclJobText[0], 0x00, 100);

	ilJobRc = FindNextJob(pcpLineID);
	if(ilJobRc == RC_SUCCESS)
	{

		ilRc = GetJobText(pcpLineID,pclJobText,TRUE);
		if(ilRc == RC_SUCCESS)
		{
			sprintf(pclAnswerText,"<ADAPT,119_TEXT,%s,%d,w1103,%s>",pcpLineID,igLanguage,pclJobText);
		
			SetLidStat(pcpLineID, "23");
			SetLidMsg(pcpLineID, pclAnswerText);
			ilRc = SendToIVSIF("IVS","","",pclAnswerText);
		}
		else
		{
			/*Nicht konfigurierter Jobtyp oder Spezial Job*/
			ilRc = HandleSpecialJob(pcpLineID);
		}

	}
	else
	{
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1102>",pcpLineID,igLanguage);
		
		SetLidStat(pcpLineID, "21");
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);
	
	}

	return ilRc;
}

static int HandleSetInformed(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;
	char pclAnswerText[100];
	char clValue1[20];
	char clValue2[20];

	ilJobRc = FindNextJob(pcpLineID);
	if(ilJobRc == RC_SUCCESS)
	{
		GetLidStatField(pcpLineID, igLidStatUjob,clValue1);
		GetLidStatField(pcpLineID, igLidStatUjbt,clValue2);
		if(atol(clValue1) == atol(clValue2))
		{
			ilRc = SetJobInformed(pcpLineID);
			if(ilRc == RC_SUCCESS)
			{
				sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1032>",pcpLineID,igLanguage);
			
				SetLidStat(pcpLineID, "25");
				
				ilRc = SendToIVSIF("IVS","","",pclAnswerText);
			}
		}
		else
		{
			/*Asugabe: Ihr Auftrag hat sich ge�ndert*/
			sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
			
			SetLidStat(pcpLineID, "9");
			SetLidMsg(pcpLineID, pclAnswerText);
			ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
		}
	}
	else
	{
		/*Der Job wurde in der Zwischenzeit gel�scht*/
		/*Asugabe: Ihr Auftrag hat sich ge�ndert*/
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
		
		SetLidStat(pcpLineID, "9");
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
	}

	return ilRc;
}


static int HandleSetConfirmed(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;
	char pclAnswerText[100];
	char clValue1[20];
	char clValue2[20];

	ilJobRc = FindNextJob(pcpLineID);
	if(ilJobRc == RC_SUCCESS)
	{
		GetLidStatField(pcpLineID, igLidStatUjob,clValue1);
		GetLidStatField(pcpLineID, igLidStatUjbt,clValue2);
		if(atol(clValue1) == atol(clValue2))
		{
			ilRc = ConfirmJob(pcpLineID);
			
			if(ilRc == RC_SUCCESS)
			{
				sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1032>",pcpLineID,igLanguage);
			
				SetLidStat(pcpLineID, "32");
				
				ilRc = SendToIVSIF("IVS","","",pclAnswerText);
			}
		}
		else
		{
			/*Der Job wurde in der Zwischenzeit gel�scht*/
			/*Asugabe: Ihr Auftrag hat sich ge�ndert*/
			sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
			
			SetLidStat(pcpLineID, "9");
			SetLidMsg(pcpLineID, pclAnswerText);
			ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
		}
	}
	else
	{
		/*Der Job wurde in der Zwischenzeit gel�scht*/
		/*Asugabe: Ihr Auftrag hat sich ge�ndert*/
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
		
		SetLidStat(pcpLineID, "9");
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
	}

	return ilRc;
}


static int HandleSetFinished(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclAnswerText[100];
	char clValue[20];
	char pclStat[20];
	char clCurrTime[20];
	time_t tlAcfr;
	time_t tlCurrent;

	CheckJobStat(pcpLineID,0,'C');
	ilRc =  GetLidStatField(pcpLineID, igLidStatUjob,clValue);

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx02Handle),
														&(rgJobArray.crIdx02Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow );
		if(ilRc == RC_SUCCESS)
		{
			strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));

			if(pclStat[0] == 'C')
			{
				pclStat[0] = 'F';
				ilRc = CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"STAT",
										llRowNum,pclStat) ;
				
				if(ilRc == RC_SUCCESS)
				{
					if(bgSetJobActo)
					{
						GetServerTimeStamp("UTC", 1, 0, clCurrTime);
						StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlAcfr);		
						StrToTime(clCurrTime,&tlCurrent);		
						
						if(tlAcfr < tlCurrent)
						{
							ilRc = CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"ACTO",
											llRowNum,clCurrTime);
						}
					}

					sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1032>",pcpLineID,igLanguage);
				
					SetLidStat(pcpLineID, "9");
	
					ilRc = SendToIVSIF("IVS","","",pclAnswerText);
				}
			}
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayWriteDB(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),
											NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}

	return ilRc;
}

static int HandleGetNextJob(char *pcpLineID,BOOL blGetRealNextJob)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;
	char pclAnswerText[100];
	char pclJobText[100];

	memset((void*)&pclJobText[0], 0x00, 100);

	if(blGetRealNextJob)
	{
		ilJobRc = FindNextJob(pcpLineID);
	}
	else
	{
		ilJobRc = Find2ndNextJob(pcpLineID);
	}
	if(ilJobRc == RC_SUCCESS)
	{
		ilRc = GetJobText(pcpLineID,pclJobText,blGetRealNextJob);
		if(ilRc == RC_SUCCESS)
		{
			sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1104,%s>",pcpLineID,igLanguage,pclJobText);
		
			SetLidMsg(pcpLineID, pclAnswerText);

			ilRc = SendToIVSIF("IVS","","",pclAnswerText);
		}
		else
		{
			/*Nicht konfigurierter Jobtyp oder Spezial Job*/
			sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1104,w1213>",pcpLineID,igLanguage);
		
			SetLidMsg(pcpLineID, pclAnswerText);

			ilRc = SendToIVSIF("IVS","","",pclAnswerText);
		}

	}
	else
	{
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1102>",pcpLineID,igLanguage);
		
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
	}

	return ilRc;
}

static int ProcessConfirmJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;
	char pclAnswerText[100];
	char pclJobText[100];

	memset((void*)&pclJobText[0], 0x00, 100);

	ilJobRc = FindNextJob(pcpLineID);

	if(ilJobRc == RC_SUCCESS)
	{
		ilRc = GetJobText(pcpLineID,pclJobText,TRUE);
		if(ilRc == RC_SUCCESS)
		{		
			sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1241,%s>",pcpLineID,igLanguage,pclJobText);
	
			SetLidStat(pcpLineID, "26");
			SetLidMsg(pcpLineID, pclAnswerText);
		
			ilRc = SendToIVSIF("IVS","","",pclAnswerText);
		}
		else
		{
			/*Nicht konfigurierter Jobtyp oder Spezial Job*/
			sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1104,w1213>",pcpLineID,igLanguage);
		
			SetLidStat(pcpLineID, "26");

			SetLidMsg(pcpLineID, pclAnswerText);

			ilRc = SendToIVSIF("IVS","","",pclAnswerText);
		}

	}
	else
	{
		/*Der Job wurde in der Zwischenzeit gel�scht*/
		/*Asugabe: Ihr Auftrag hat sich ge�ndert*/
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
		
		SetLidStat(pcpLineID, "9");
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
	}
	return ilRc;
}

static int HandleConfirmJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	char pclAnswerText[100];

	if(CheckJobChanged(pcpLineID) == RC_SUCCESS)
	{
		ProcessConfirmJob(pcpLineID);
	}
	else
	{
		/* JOB wurde seit letztem abfragen ge�ndert*/
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1041>",pcpLineID,igLanguage);
		SetJobNotInformed(pcpLineID);
		SetLidStat(pcpLineID, "9");
		SetLidMsg(pcpLineID, pclAnswerText);
		ilRc = SendToIVSIF("IVS","","",pclAnswerText);	
	}
	return ilRc;
}

static int Handle116_Transfer(char *pcpLineID,char *pcpStat)
{
	int ilRc = RC_SUCCESS;
	char pclAnswerText[100];
	
	if(atol(pcpStat) == 2)
	{
		/*Status besetzt*/
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1044>",pcpLineID,igLanguage);
	}
	if(atol(pcpStat) == 3)
	{
		/*Status nicht abgenommen*/
		sprintf(pclAnswerText,"<ADAPT,115_TEXT,%s,%d,w1045>",pcpLineID,igLanguage);
	}
	SetLidStat(pcpLineID, "36");
	ilRc = SendToIVSIF("IVS","","",pclAnswerText);

	return ilRc;
}

static int HandleFinishJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	time_t tlActo;
	char clCurrTime[20];
	char clDate[20];
	char clTime[10];
	char clValue[20];
	char pclAnswerText[100];

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);
	StrToTime(clCurrTime,&tgCurrTime);
	ilRc =  GetLidStatField(pcpLineID, igLidStatUjob,clValue);
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx02Handle),
														&(rgJobArray.crIdx02Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow );
		if(ilRc == RC_SUCCESS)
		{
			if(atol(JOBFIELD(pclJobRow,igJobUjty)) == lgUjtyBrk)
			{

				StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlActo);
				if(tlActo + igOffSetLateBEnd < tgCurrTime)
				{
					strcpy(clDate,JOBFIELD(pclJobRow,igJobActo));
					UtcToLocal(clDate);

					ilRc = GetTimeString(clTime,clDate);

					sprintf(pclAnswerText,"<ADAPT,119_TEXT,%s,%d,w1067,t%s,w1043>",pcpLineID,igLanguage,clTime);
					
					SetLidStat(pcpLineID, "40");
					SetLidMsg(pcpLineID, pclAnswerText);
					ilRc = SendToIVSIF("IVS","","",pclAnswerText);
				}
				else
				{
					SetLidStat(pcpLineID, "41");
					ilRc = Menu_1_3_4Break(pcpLineID);
				}
			}
			else
			{
				SetLidStat(pcpLineID, "41");
				ilRc = Menu_1_3_4Job(pcpLineID);
			}
		}
	}
	return ilRc;
}


static int ConfirmJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[20];
	char clValue[20];
	char clCurrTime[20];
	time_t tlCurrTime;
	time_t tlAcfr;
	time_t tlActo;


	ilRc = GetLidStatField(pcpLineID,igLidStatUjob,clValue);
	if(ilRc == RC_SUCCESS)
	{
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx02Handle),
														&(rgJobArray.crIdx02Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS)
		{

			strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));

			if(pclStat[0] == 'P')
			{
				pclStat[0] = 'C';
				CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"STAT",
										llRowNum,pclStat) ;

				

			    if(bgSetJobAcfr)
				{
					GetServerTimeStamp("UTC", 1, 0, clCurrTime);
					StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlAcfr);		
			        StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlActo);
					ilRc = CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"ACFR",
									llRowNum,clCurrTime);
			        StrToTime(clCurrTime,&tlCurrTime);
                    if(tlActo < tlCurrTime + 5*60 && tlAcfr < tlCurrTime)
					{
						StrAddTime(clCurrTime,tlActo-tlAcfr,clCurrTime);

						ilRc = CEDAArrayPutField(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),NULL,"ACTO",
									llRowNum,clCurrTime);
					}			
				}


				RemoveInfJob(JOBFIELD(pclJobRow,igJobUrno));
				ilRc = RC_SUCCESS;
			}
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayWriteDB(&(rgJobArray.rrArrayHandle),&(rgJobArray.crArrayName[0]),
											NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
	}

	return ilRc;
}

static int FindNextPlannedJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[20];
	char clValue[20];
	BOOL blJobFound = FALSE;
	char clCurrTime[20];
	time_t tlAcfr,tlActo;

	ilRc =  GetLidStatField(pcpLineID, igLidStatUstf,clValue);

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);


	if(ilRc == RC_SUCCESS)
	{
		ilRc = RC_FAIL;
		while((AATArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
															&(rgJobArray.crArrayName[0]),
															&(rgJobArray.rrIdx03Handle),
															&(rgJobArray.crIdx03Name[0]),
															clValue,&llRowNum,
															(void *) &pclJobRow ) == RC_SUCCESS) && !blJobFound)
		{
			strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));
		
			StrToTime(JOBFIELD(pclJobRow,igJobAcfr),&tlAcfr);		
			StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlActo);
			if((tlAcfr < tgCurrTime + 10*60*60) && (tlActo > tgCurrTime))
			{
				if(pclStat[0] == 'P' && pclStat[4] != '1')
				{
					ilRc =  SetLidStatField(pcpLineID,"UJOB",JOBFIELD(pclJobRow,igJobUrno));
					blJobFound = TRUE;
				}
			}
			llRowNum = ARR_NEXT;
		}
	}
	return ilRc;

}

static int FindNextInformedJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[20];
	char clValue[20];
	char clCurrTime[20];
	BOOL blJobFound = FALSE;
	time_t tlActo;

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);
	ilRc =  GetLidStatField(pcpLineID, igLidStatUstf,clValue);

	while((AATArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx03Handle),
														&(rgJobArray.crIdx03Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS) && !blJobFound)
	{
		strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));
	
		if(pclStat[0] == 'P' && pclStat[4] == '1')
		{
			StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlActo);
				
			if(tlActo > tgCurrTime)
			{
				ilRc =  SetLidStatField(pcpLineID,"UJOB",JOBFIELD(pclJobRow,igJobUrno));
				blJobFound = TRUE;
			}
		}

		llRowNum = ARR_NEXT;

	}
	return ilRc;

}

static int FindNextJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[20];
	char clValue[20];
	char clCurrTime[20];
	BOOL blJobFound = FALSE;
	time_t tlActo;

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);
	ilRc =  GetLidStatField(pcpLineID, igLidStatUstf,clValue);

	while((AATArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx03Handle),
														&(rgJobArray.crIdx03Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS) && !blJobFound)
	{
		strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));
		dbg(TRACE,"FindNextJob pclStat <%s> ",pclStat);
		dbg(TRACE,"FindNextJob Ujob <%s> ",JOBFIELD(pclJobRow,igJobUrno));
	
		if(pclStat[0] == 'P')
		{
			StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlActo);
				
			if((tlActo > tgCurrTime) && (tlActo < tgCurrTime + 10*60*60))
			{
				ilJobRc =  SetLidStatField(pcpLineID,"UJOB",JOBFIELD(pclJobRow,igJobUrno));
				blJobFound = TRUE;
			}
		}

		llRowNum = ARR_NEXT;

	}
	return ilJobRc;

}

static int Find2ndNextJob(char *pcpLineID)
{
	int ilRc = RC_FAIL;
	int ilJobRc = RC_FAIL;

	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char pclStat[20];
	char clValue[20];
	char clCurrTime[20];
	BOOL blJobFound = FALSE;
	BOOL bl1stJobFound = FALSE;
	time_t tlActo;

	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);
	ilRc =  GetLidStatField(pcpLineID, igLidStatUstf,clValue);

	while((AATArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx03Handle),
														&(rgJobArray.crIdx03Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS) && !blJobFound)
	{
		strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));
	
		if(bl1stJobFound)
		{
			if(pclStat[0] == 'P')
			{
				StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlActo);
				
				if(tlActo > tgCurrTime)
				{
					ilJobRc =  SetLidStatField(pcpLineID,"UJOB",JOBFIELD(pclJobRow,igJobUrno));
					blJobFound = TRUE;
				}
			}
		}
		else
		{
			if(pclStat[0] == 'P' && pclStat[4] == '1')
			{
				StrToTime(JOBFIELD(pclJobRow,igJobActo),&tlActo);
					
				if(tlActo > tgCurrTime)
				{
					bl1stJobFound = TRUE;
				}
			}
		}


		llRowNum = ARR_NEXT;

	}
	return ilJobRc;

}


static int CheckJobChanged(char *pcpLineID)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char *pclInfJobRow = NULL;
	char clValue[20];
	char clKey[40];

	ilRc = GetLidStatField(pcpLineID,igLidStatUjob,clValue);
	if(ilRc == RC_SUCCESS)
	{
		sprintf(clKey,"%s,2",clValue);
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgInfJobArray.rrArrayHandle),
														&(rgInfJobArray.crArrayName[0]),
														&(rgInfJobArray.rrIdx01Handle),
														&(rgInfJobArray.crIdx01Name[0]),
														clKey,&llRowNum,
														(void *) &pclInfJobRow ) == RC_SUCCESS)
		{
			ilRc = RC_FAIL;
		}
	}
	return ilRc;
}

static int GetJobText(char *pcpLineID,char *pcpJobText,BOOL bpStoreUjob)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char *pclJobRow = NULL;
	char *pclPatternRow = NULL;
	char *pclPatternTypeRow = NULL;
	char *pclIvsRow = NULL;
	char pclStat[20];
	char clValue[20];
	char clKey[40];
	char clJtyp[20];
	char clAloc[20];
	char clText[70];
	char clPattern[110];
	char clPatternType[110];
	char clTime[20];
	char clDate[10];
	int ilNoOfItems = 0;
	int ilLoop = 0;
	char clField[10];
	char clType[3];
	int ilCol,ilPos;
	int ilJobField = 0;

	ilRc =  GetLidStatField(pcpLineID, igLidStatUjob,clValue);

	if(ilRc == RC_SUCCESS)
	{
		if(bpStoreUjob)
		{
			dbg(TRACE,"GetJobText Ujbt Value <%s> ",clValue);
			ilRc = SetLidStatField(pcpLineID, "UJBT",clValue);
			dbg(TRACE,"GetJobText Ujbt ilRc <%d> ",ilRc);
		}
		llRowNum = ARR_FIRST;
		if(CEDAArrayFindRowPointer(&(rgJobArray.rrArrayHandle),
														&(rgJobArray.crArrayName[0]),
														&(rgJobArray.rrIdx02Handle),
														&(rgJobArray.crIdx02Name[0]),
														clValue,&llRowNum,
														(void *) &pclJobRow ) == RC_SUCCESS)
		{
			strcpy(clJtyp,JOBFIELD(pclJobRow,igJobJtyp));
			strcpy(clAloc,JOBFIELD(pclJobRow,igJobAloc));
			TrimRight(clJtyp);
			TrimRight(clAloc);
			sprintf(clKey,"%s,%s",clJtyp,clAloc);
			llRowNum = ARR_FIRST;
			if(atol(JOBFIELD(pclJobRow,igJobUjty)) == lgUjtyBrk)
			{
				strcpy(pclStat,JOBFIELD(pclJobRow,igJobStat));
				
				if(pclStat[2] == '1')
				{
					strcat(pcpJobText,"w1061,w1063,w1065,");
				}
				else
				{
					strcat(pcpJobText,"w1061,w1062,w1065,");
				}
				strcpy(clDate,JOBFIELD(pclJobRow,igJobAcfr));
				UtcToLocal(clDate);
				ilRc = GetTimeString(clTime,clDate);
												
				strcat(pcpJobText,"t");
				strcat(pcpJobText,clTime);
				strcpy(clDate,JOBFIELD(pclJobRow,igJobActo));
				UtcToLocal(clDate);
				ilRc = GetTimeString(clTime,clDate);
												
				strcat(pcpJobText,",w1066,t");
				strcat(pcpJobText,clTime);

				
			}
			else
			{
				llRowNum = ARR_FIRST;
				if(CEDAArrayFindRowPointer(&(rgPatternArray.rrArrayHandle),
													&(rgPatternArray.crArrayName[0]),
													&(rgPatternArray.rrIdx01Handle),
													&(rgPatternArray.crIdx01Name[0]),
													clKey,&llRowNum,
													(void *) &pclPatternRow ) == RC_SUCCESS)
				{
					llRowNum = ARR_FIRST;
					dbg(TRACE,"GetJobText Pattern found clKey <%s>",clKey);
					if(CEDAArrayFindRowPointer(&(rgPatternTypeArray.rrArrayHandle),
														&(rgPatternTypeArray.crArrayName[0]),
														&(rgPatternTypeArray.rrIdx01Handle),
														&(rgPatternTypeArray.crIdx01Name[0]),
														clKey,&llRowNum,
														(void *) &pclPatternTypeRow ) == RC_SUCCESS)
					{

						dbg(TRACE,"GetJobText PatternType found ");

						strcpy(clPattern,PATTERNFIELD(pclPatternRow,igPatternText));
						strcpy(clPatternType,PATTERNFIELD(pclPatternTypeRow,igPatternTypeText));
						TrimRight(clPattern);
						TrimRight(clPatternType);
						ilNoOfItems = get_no_of_items(clPattern);
						if(ilNoOfItems == get_no_of_items(clPatternType))
						{
							ilLoop = 1;
							do
							{
								ilRc = get_real_item(&clField[0],clPattern,ilLoop);				
								if(ilRc > 0)
								{
									dbg(TRACE,"GetJobText clField <%s> ",clField);
									ilRc = get_real_item(&clType[0],clPatternType,ilLoop);				
									if(ilRc > 0)
									{
										dbg(TRACE,"GetJobText clType <%s> ",clType);
										TrimRight(pcpJobText);
										if(ilLoop > 1)
										{
											strcat(pcpJobText,",");
										}

										switch(clType[0])
										{
										case 'w' :
											if(FindItemInList(JOB_FIELDS2,clField,',',&ilJobField,&ilCol,&ilPos) == RC_SUCCESS)
											{
												dbg(TRACE,"GetJobText Type w , JobField <%s> ",JOBFIELD(pclJobRow,ilJobField));
												llRowNum = ARR_FIRST;
												if(CEDAArrayFindRowPointer(&(rgIvsArray.rrArrayHandle),
																					&(rgIvsArray.crArrayName[0]),
																					&(rgIvsArray.rrIdx01Handle),
																					&(rgIvsArray.crIdx01Name[0]),
																					JOBFIELD(pclJobRow,ilJobField),&llRowNum,
																					(void *) &pclIvsRow ) == RC_SUCCESS)
												{
													strcat(pcpJobText,IVSFIELD(pclIvsRow,igIvsCode));
													dbg(TRACE,"GetJobText IvsCode <%s> ",IVSFIELD(pclIvsRow,igIvsCode));
												}
											}										
											break;
										case 'c' :strcat(pcpJobText,clField);
												 dbg(TRACE,"GetJobText Type c , clField <%s> ",clField);
											break;
										case 't' :
											if(FindItemInList(JOB_FIELDS2,clField,',',&ilJobField,&ilCol,&ilPos) == RC_SUCCESS)
											{
												strcpy(clDate,JOBFIELD(pclJobRow,ilJobField));
												UtcToLocal(clDate);

												ilRc = GetTimeString(clTime,clDate);
												
												strcat(pcpJobText,"t");
												strcat(pcpJobText,clTime);
												 dbg(TRACE,"GetJobText Type t , clTime <%s> ",clTime);
												
											}																	
											break;
										case 's' :
											if(FindItemInList(JOB_FIELDS2,clField,',',&ilJobField,&ilCol,&ilPos) == RC_SUCCESS)
											{
												strcpy(clText,JOBFIELD(pclJobRow,ilJobField));
												dbg(TRACE,"GetJobText Type s , clText <%s> ",clText);
												dbg(TRACE,"GetJobText pcpJobText1 <%s> ",pcpJobText);
												ConvertPattern(clText,pcpJobText);
												dbg(TRACE,"GetJobText pcpJobText2 <%s> ",pcpJobText);
											}																	
											break;
										}
										
									}
								}/* end of if */
								ilLoop++;
							}while(ilLoop <= ilNoOfItems);
						}
					}
				}
				else
				{
					ilRc = RC_FAIL;
				}
			}
			llRowNum = ARR_NEXT;
		}
	}
	return ilRc;
}

static int GetDispoNumber(char *pcpLineID,char *pcpDispoNumber)
{
	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	long llRowNum2 = ARR_FIRST;
	char *pclPoolJobRow = NULL;
	char *pclPolRow = NULL;
	char clValue[20];
	char clCurrTime[20];
	time_t tlAcfr,tlActo;
	BOOL blPoolJobFound = FALSE;
	GetServerTimeStamp("UTC", 1, 0, clCurrTime);

	StrToTime(clCurrTime,&tgCurrTime);

	ilRc = GetLidStatField(pcpLineID, igLidStatUdsr,clValue);
	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"GetDispoNumber clValue <%s> ",clValue);
		while((CEDAArrayFindRowPointer(&(rgPoolJobArray.rrArrayHandle),
														&(rgPoolJobArray.crArrayName[0]),
														&(rgPoolJobArray.rrIdx03Handle),
														&(rgPoolJobArray.crIdx03Name[0]),
														clValue,&llRowNum,
														(void *) &pclPoolJobRow ) == RC_SUCCESS)
														&& !blPoolJobFound)
		{
			StrToTime(POOLJOBFIELD(pclPoolJobRow,igPoolJobAcfr),&tlAcfr);
			StrToTime(POOLJOBFIELD(pclPoolJobRow,igPoolJobActo),&tlActo);
			
			dbg(TRACE,"GetDispoNumber Acfr <%s> ",POOLJOBFIELD(pclPoolJobRow,igPoolJobAcfr));
			dbg(TRACE,"GetDispoNumber Acto <%s> ",POOLJOBFIELD(pclPoolJobRow,igPoolJobActo));
			blPoolJobFound = TRUE;
			llRowNum2 = ARR_FIRST;
			if(CEDAArrayFindRowPointer(&(rgPolArray.rrArrayHandle),
													&(rgPolArray.crArrayName[0]),
													&(rgPolArray.rrIdx01Handle),
													&(rgPolArray.crIdx01Name[0]),
													POOLJOBFIELD(pclPoolJobRow,igPoolJobUaid),&llRowNum2,
													(void *) &pclPolRow ) == RC_SUCCESS)
			{
				dbg(TRACE,"GetDispoNumber Dtel <%s> ",POLFIELD(pclPolRow,igPolDtel));
				strcpy(pcpDispoNumber,POLFIELD(pclPolRow,igPolDtel));
			}

			llRowNum = ARR_NEXT;
		}
	}
	return ilRc;
}

static int GetTimeString(char *pcpTime,char *pcpDate)
{
	int ilRc = RC_SUCCESS;
	if (strlen(pcpDate) < 12 )
	{
		return RC_FAIL;
	}
	MyStrnCpy (pcpTime, &pcpDate[8], 4, 1 );
	return ilRc;
}

void MyStrnCpy ( char *pcpDest, const char *pcpSource, size_t ipCount, int ipTrimRight )
{
	strncpy ( pcpDest, pcpSource, ipCount );
	pcpDest[ipCount]='\0';
	if ( ipTrimRight )
		TrimRight ( pcpDest );
}

static int ConvertPattern(char *pcpPattern, char *pcpConverted)
{
	int		ilRc = RC_SUCCESS;
	int		i;
	int		ilNumber;
	char	clDigits[8];

	memset(clDigits, 0x00, sizeof(clDigits));

	for ( i = 0; i < strlen(pcpPattern); i++)
	{
		if ( !isspace(pcpPattern[i]) )
		{
			if (i > 0)
				strcat(pcpConverted,",");

			if ( !isdigit(pcpPattern[i]) )
			{
				toupper(pcpPattern[i]);
				ilRc = SearchPatternInIvs(&pcpPattern[i], pcpConverted);
			}
			else
			{
				while ( isdigit(pcpPattern[i]) )
				{
					strncat(clDigits,&pcpPattern[i],1);
					i++;
				}
				ilNumber = atoi(clDigits);
				ilRc = ConvertNumber(ilNumber, pcpConverted);
				memset(clDigits, 0x00, sizeof(clDigits));
			}
		}
	}
	return ilRc;
}

static int SearchPatternInIvs(char *pcpUpperCase, char *pcpConverted)
{
	int		ilRc = RC_SUCCESS;
	char	clTkey[64+1];
	char	clHelp[64+1];
	long	llRowNum = ARR_FIRST;
	char	*pclIvsRow = NULL;

	memset(clTkey, 0x00, sizeof(clTkey));
	strncpy(clTkey,pcpUpperCase,1);

	if (ilRc = CEDAArrayFindRowPointer(&(rgIvsArray.rrArrayHandle),
										&(rgIvsArray.crArrayName[0]),
										&(rgIvsArray.rrIdx01Handle),
										&(rgIvsArray.crIdx01Name[0]),
										clTkey, &llRowNum, (void *) &pclIvsRow)
																 == RC_SUCCESS)
	{
		strcpy(clHelp,IVSFIELD(pclIvsRow,igIvsCode));
		TrimRight(clHelp);
		strcat(pcpConverted,clHelp);
	}

	return ilRc;
}

static int ConvertNumber(int ipNumber, char *pcpConverted)
{
	int		ilRc = RC_SUCCESS;
	char	clNumber[5];

	strcat(pcpConverted,"n");
	if ( ipNumber < 1001 )
	{
		sprintf(clNumber, "%d", ipNumber);
		strcat(pcpConverted, clNumber);
	}
	else
	{
		sprintf(clNumber, "%d", ipNumber/100);
		strcat(pcpConverted, clNumber);
		strcat(pcpConverted,",n");
		if ( ipNumber%100 < 10 )
		{
			strcat(pcpConverted,"0,");
			strcat(pcpConverted,"n");
			sprintf(clNumber, "%d", ipNumber%100);
			strcat(pcpConverted, clNumber);
		}
		else
		{
			sprintf(clNumber, "%d", ipNumber%100);
			strcat(pcpConverted, clNumber);
		}
	}
	return ilRc;
}

static int GetPatternFromCfg(void)
{
	int		ilRc = RC_SUCCESS;
	int 	ilNumOfPars;
	int		i;
	long	llRowNum;
	char	clPars[1024];
	char	clParN[16];
	char	clParContents[64];
	char	clJtyp[JTYPLEN+1];
	char	clAloc[ALOCLEN+1];
	char	clText[100];
	char	clDel = ';';

	ilRc = iGetConfigRow(cgConfigFile, "PATTERN", "PARS", CFG_STRING, clPars);
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"GetPatternFromCfg clPars <%s>",clPars);

		ilNumOfPars = get_no_of_items(clPars);
		for ( i = 1; i <= ilNumOfPars; i++ )
		{
			get_real_item(clParN, clPars, i);
			ilRc = iGetConfigRow(cgConfigFile, "PATTERN", clParN, CFG_STRING,
									clParContents);
    		if ( ilRc == RC_SUCCESS )
				ilRc = GetDataItem(clJtyp, clParContents, 1, clDel, "", "\0 ");
    		if ( ilRc > 0 )
   		 		ilRc = GetDataItem(clAloc, clParContents, 2, clDel, "", "\0 ");
    		if ( ilRc > 0 )
   		 		ilRc = GetDataItem(clText, clParContents, 3, clDel, "", "\0 ");
    		if ( ilRc > 0 )
				ilRc = InitializePatternRow(cgPatternBuf);
			if ( ilRc == RC_SUCCESS )
			{
				llRowNum = ARR_NEXT;
				ilRc = AATArrayAddRow(&(rgPatternArray.rrArrayHandle),
										&(rgPatternArray.crArrayName[0]),
										&llRowNum,(void *)cgPatternBuf);
				if(ilRc == RC_SUCCESS)
				{
					ilRc = AATArrayPutField(&(rgPatternArray.rrArrayHandle),
											&(rgPatternArray.crArrayName[0]),
											NULL,"JTYP",llRowNum,TRUE,clJtyp);
				}
				if(ilRc == RC_SUCCESS)
				{
					ilRc = AATArrayPutField(&(rgPatternArray.rrArrayHandle),
											&(rgPatternArray.crArrayName[0]),
											NULL,"ALOC",llRowNum,TRUE,clAloc);
				}
				if(ilRc == RC_SUCCESS)
				{
					ilRc = AATArrayPutField(&(rgPatternArray.rrArrayHandle),
											&(rgPatternArray.crArrayName[0]),
											NULL,"TEXT",llRowNum,TRUE,clText);
				}
			}
		}
	}
	
	return ilRc;
}

static int GetPatternTypeFromCfg(void)
{
	int		ilRc = RC_SUCCESS;
	int 	ilNumOfPars;
	int		i;
	long	llRowNum;
	char	clPars[1024];
	char	clParN[16];
	char	clParContents[64];
	char	clJtyp[JTYPLEN+1];
	char	clAloc[ALOCLEN+1];
	char	clText[100];
	char	clDel = ';';

	ilRc = iGetConfigRow(cgConfigFile, "PATTERNTYPE", "PARS", CFG_STRING,
							clPars);
	if (ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"GetPatternTypeFromCfg clPars <%s>",clPars);

		ilNumOfPars = get_no_of_items(clPars);
		dbg(TRACE,"GetPatternTypeFromCfg ilNumOfPars <%d>",ilNumOfPars);
		for ( i = 1; i <= ilNumOfPars; i++ )
		{
			get_real_item(clParN, clPars, i);
			ilRc = iGetConfigRow(cgConfigFile, "PATTERNTYPE", clParN,
								 CFG_STRING, clParContents);
    		if ( ilRc == RC_SUCCESS )
				ilRc = GetDataItem(clJtyp, clParContents, 1, clDel, "", "\0 ");
    		if ( ilRc > 0 )
   		 		ilRc = GetDataItem(clAloc, clParContents, 2, clDel, "", "\0 ");
    		if ( ilRc > 0 )
   		 		ilRc = GetDataItem(clText, clParContents, 3, clDel, "", "\0 ");
    		if ( ilRc > 0 )
				ilRc = InitializePatternTypeRow(cgPatternTypeBuf);
			if ( ilRc == RC_SUCCESS )
			{
				llRowNum = ARR_NEXT;
				ilRc = AATArrayAddRow(&(rgPatternTypeArray.rrArrayHandle),
										&(rgPatternTypeArray.crArrayName[0]),
										&llRowNum,(void *)cgPatternTypeBuf);
				if(ilRc == RC_SUCCESS)
				{
					ilRc = AATArrayPutField(&(rgPatternTypeArray.rrArrayHandle),
											&(rgPatternTypeArray.crArrayName[0]),
											NULL,"JTYP",llRowNum,TRUE,clJtyp);
				}
				if(ilRc == RC_SUCCESS)
				{
					ilRc = AATArrayPutField(&(rgPatternTypeArray.rrArrayHandle),
											&(rgPatternTypeArray.crArrayName[0]),
											NULL,"ALOC",llRowNum,TRUE,clAloc);
				}
				if(ilRc == RC_SUCCESS)
				{
					ilRc = AATArrayPutField(&(rgPatternTypeArray.rrArrayHandle),
											&(rgPatternTypeArray.crArrayName[0]),
											NULL,"TEXT",llRowNum,TRUE,clText);
				}
			}
		}
	}
	
	return ilRc;
}
