/************************************************/
/****** Account 27: compensation hours in month */
/************************************************/
void bas_27()
{
	char	clStaffUrno[12];
	char	clSday[16];
	char	clSourceMonth[4];
	char	clYear[6];
	char	clOldAccValue26[12];
	char	clOldAccValue23[12];
	char	clOldAccValue27[12];

	double	dlNewAccValue;

	dbg(DEBUG, "============================= ACCOUNT 27 START ===========================");

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 27: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 27: Stopped, because <clSday> = undef!");
		return;
	}

	/* get ACC record */
	clSourceMonth[0] = clSday[4];
	clSourceMonth[1] = clSday[5];
	clSourceMonth[2] = '\0';

	strncpy(clYear,clSday,4);
	clYear[4] = '\0';

	dbg(DEBUG, "Account 27: Year  <%s>",clYear);
	dbg(DEBUG, "Account 27: Month <%s>",clSourceMonth);

	GetAccValueHandle("26",clSourceMonth,clYear,clStaffUrno,"CL",clOldAccValue26);
	GetAccValueHandle("23",clSourceMonth,clYear,clStaffUrno,"CO",clOldAccValue23);
	GetAccValueHandle("27",clSourceMonth,clYear,clStaffUrno,"OP",clOldAccValue27);

	dlNewAccValue = atof(clOldAccValue26) - (atof(clOldAccValue23) - atof(clOldAccValue27)); 

	dbg(DEBUG,"Account 27: New value: <%lf>", dlNewAccValue);

	UpdateAccountHandle(dlNewAccValue,"27",clSourceMonth,clYear,clStaffUrno,"SCBHDL");
}
