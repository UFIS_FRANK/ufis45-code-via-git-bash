/*************************************/
/* Account 51: working hours balance */
/*************************************/
static void bas_51()
{
	char	clIsAushilfe[2];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clYear[6];
	char	clMonth[4];
	char	clOldAccValue26[128];
	char	clOldAccValue23[128];
	char	clOldAccValue51[128];
	double	dlResult;

	dbg(DEBUG, "============================= ACCOUNT 51 START ===========================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 1 && clIsAushilfe[0] == '1')
	{
		dbg(TRACE,"Account 51: Stopped, because of temporary staff");
		return;
	}

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 51: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 51: Stopped, because <clSday> = undef!");
		return;
	}

	/* get old ACC record */
	clMonth[0] = clSday[4];
	clMonth[1] = clSday[5];
	clMonth[2] = '\0';

	strncpy(clYear,clSday,4);
	clYear[4] = '\0';

	GetAccValueHandle("26",clMonth,clYear,clStaffUrno,"CL",clOldAccValue26);
	dbg(DEBUG, "Account 51: Old ACC close value of account 26 <clOldAccValue26> = <%s>",clOldAccValue26);

	GetAccValueHandle("23",clMonth,clYear,clStaffUrno,"CO",clOldAccValue23);
	dbg(DEBUG, "Account 51: Old ACC correction value of account 23 <clOldAccValue23> = <%s>",clOldAccValue23);

	GetAccValueHandle("51",clMonth,clYear,clStaffUrno,"OP",clOldAccValue51);
	dbg(DEBUG, "Account 51: Old ACC open value of account 51 <clOldAccValue51> = <%s>",clOldAccValue51);

	/* calculate new values and save it */
	dlResult = atof(clOldAccValue26) - atof(clOldAccValue23) + atof(clOldAccValue51);

	dbg(DEBUG, "Account 51: New ACC value <dlResult> = <%lf>", dlResult);
	dbg(DEBUG, "Account 51: Month <clMonth> = <%s>", clMonth);

	UpdateAccountHandle(dlResult,"51",clMonth,clYear,clStaffUrno,"SCBHDL");

	/* the open values of the next month(s) must be updated, but not overlapping the quarter */
	/*if (strcmp(clMonth,"03") == 0 ||
		strcmp(clMonth,"06") == 0 ||
		strcmp(clMonth,"09") == 0 ||
		strcmp(clMonth,"12") == 0)
	{
		dbg(DEBUG, "Account 51: calling 'UpdateAccountXYHandle' (NOT setting next month's open values)");
		UpdateAccountXYHandle(clStaffUrno,dlResult,"51",clSday,"SCBHDL","CL");
	}
	else
	{
		UpdateAccountHandle(dlResult,"51",clMonth,clYear,clStaffUrno,"SCBHDL");
	}*/
}
