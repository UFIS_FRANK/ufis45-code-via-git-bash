/*************************/
/* 5: free days in month */
/*************************/
void bas_5()
{
	char	clIsAushilfe[2];
	char	clLastCommand[4];
	char	clStaffUrno[12];
	char	clNewShiftUrno[12];
	char	clNewDrrRoss[2];
	char	clOldShiftUrno[12];
	char	clOldDrrStatus[2];
	char	clNewValue[2];
	char	clOldValue[2];
	char	clOldAccValue[128];
	char	clSday[16];
	char	clMonth[4];
	char	clYear[6];

	double	dlNewAccValue;
	int	ilResultValue;

	dbg(DEBUG, "============================= ACCOUNT 5 START ====================================");

	/* checking temporary staff */
	if (ReadTemporaryField("IsAushilfe",clIsAushilfe) == 1 && clIsAushilfe[0] == '1')
	{
		dbg(TRACE,"Account 5: Stopped, because of temporary staff");
		return;
	}

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 5: Command (%s)",clLastCommand);

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 5: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 5: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 5: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 5: pcgFields = <%s>",pcgFields);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 5: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 5: Stopped, because <clSday> = undef!");
		return;
	}

	/* BSDU of new DRR-record */
	if (ReadTemporaryField("NewBsdu",clNewShiftUrno) == 0)
	{
		dbg(TRACE,"Account 5: Stopped, because <clNewShiftUrno> = undef!");
		return;
	}

	/* Status des neuen Datensatzes	*/
	if (ReadTemporaryField("NewRoss",clNewDrrRoss) == 0)
	{
		dbg(TRACE,"Account 5: <clNewDrrRoss> = undef!");
		clOldShiftUrno[0] = '\0';
	}

	/************************* im Falle eines Updates alte Daten auslesen *****************************/
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* checking the old data */
		if (strlen(pcgOldData) == 0)
		{
			dbg(TRACE,"Account 5: <pcgOldData> = undef!");
		}
		else
		{
			dbg(DEBUG,"Account 5: <pcgOldData> = <%s>",pcgOldData);
		}

		/* BSDU of old DRR-record */
		if (ReadTemporaryField("OldBsdu",clOldShiftUrno) == 0)
		{
			dbg(TRACE, "Account 5: <clOldShiftUrno> = undef!");
			clOldShiftUrno[0] = '\0';
		}

		/* status of old record */
		if (ReadTemporaryField("OldRoss",clOldDrrStatus) == 0)
		{
			dbg(TRACE, "Account 5: <clOldDrrStatus> = undef!");
			clOldDrrStatus[0] = '\0';
		}
	}

	/* starting calculation */
	/* getting old value */
	strcpy(clOldValue,"0");
	if (strcmp(clLastCommand,"URT") == 0)
	{
		dbg(DEBUG, "Account 5: <clOldShiftUrno> = <%s>" , clOldShiftUrno);
	}
	if (clOldShiftUrno[0] != '\0')
	{
		/* use only active DRR-records */
		if (strcmp(clOldDrrStatus,"A") == 0)
		{
			ReadTemporaryField("OldIsRegFree",clOldValue);
			dbg(DEBUG, "Account 5: <clOldValue> = <%s>" , clOldValue);
		}
		else
		{
			dbg(TRACE, "Account 5: Wrong status of old DRR-record: <clOldDrrStatus> = <%s>" ,clOldDrrStatus);
		}
	}


	/* getting new value */
	dbg(DEBUG, "Account 5: <clNewShiftUrno> = <%s>" , clNewShiftUrno);
	strcpy(clNewValue,"0");
	if (strcmp(clNewDrrRoss,"A") == 0)
	{
		ReadTemporaryField("NewIsRegFree",clNewValue);
		dbg(DEBUG, "Account 5: <clNewValue> = <%s>" , clNewValue);
	}
	else
	{
		dbg(TRACE, "Account 5: Wrong status of new DRR-record:  <clNewDrrRoss> = <%s>" ,clNewDrrRoss);
	}

	/*  calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		ilResultValue = atoi(clNewValue) - atoi(clOldValue);
	}
	else
	{
		ilResultValue = - atoi(clNewValue); 
	}

	/* get ACC record */
	clMonth[0] = clSday[4];
	clMonth[1] = clSday[5];
	clMonth[2] = '\0';

	clYear[0] = clSday[0];
	clYear[1] = clSday[1];
	clYear[2] = clSday[2];
	clYear[3] = clSday[3];
	clYear[4] = '\0';

	GetAccValueHandle("5",clMonth,clYear,clStaffUrno,"CL",clOldAccValue);

	dbg(DEBUG, "Account 5: Old ACC value: <%s>", clOldAccValue);

	dlNewAccValue = atof(clOldAccValue) + ilResultValue;

	dbg(DEBUG, "Account 5: New ACC value: <%8.2lf>", dlNewAccValue);

	if (ilResultValue != 0)
	{
	
		UpdateAccountHandle(dlNewAccValue,"5",clMonth,clYear,clStaffUrno,"SCBHDL");

		SetNextScript("/ceda/bas/8.bbf");
		dbg(DEBUG, "Account 5: next account to be processed: account 8");
	}
	else
	{
		dbg(TRACE, "Account 5: Value not changed: <%s>", clOldAccValue);
	}
}
