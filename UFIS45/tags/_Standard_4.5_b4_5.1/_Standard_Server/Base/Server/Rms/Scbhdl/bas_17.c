/************************************/
/* IsAbsence(BOOL bpOld)            */
/* R�ckgabe: 1 -> Abwesenheit       */
/*           0 -> keine Abwesenheit */
/************************************/
static BOOL IsAbsence(BOOL bpOld)
{
	char	clIsBsd[2];
	char	clIsRegFree[2];
	char	clIsTraining[2];
	char	clIsHoliday[2];
	char	clIsShiftChangeoverDay[2];
	char	clIsCompensation[2];
	char	clIsIllness[2];

	if (bpOld)
	{
		if (ReadTemporaryField("OldIsBsd",clIsBsd) == 1 && clIsBsd[0] == '1')
			return FALSE;
		if (ReadTemporaryField("OldIsRegFree",clIsRegFree) == 1 && clIsRegFree[0] == '1')
			return FALSE;
		if (ReadTemporaryField("OldIsTraining",clIsTraining) == 1 && clIsTraining[0] == '1')
			return FALSE;
		if (ReadTemporaryField("OldIsHoliday",clIsHoliday) == 1 && clIsHoliday[0] == '1')
			return FALSE;
		if (ReadTemporaryField("OldIsShiftChangeoverDay",clIsShiftChangeoverDay) == 1 && clIsShiftChangeoverDay[0] == '1')
			return FALSE;
		if (ReadTemporaryField("OldIsCompensation",clIsCompensation) == 1 && clIsCompensation[0] == '1')
			return FALSE;
		if (ReadTemporaryField("OldIsIllness",clIsIllness) == 1 && clIsIllness[0] == '1')
			return FALSE;
	}
	else
	{
		if (ReadTemporaryField("NewIsBsd",clIsBsd) == 1 && clIsBsd[0] == '1')
			return FALSE;
		if (ReadTemporaryField("NewIsRegFree",clIsRegFree) == 1 && clIsRegFree[0] == '1')
			return FALSE;
		if (ReadTemporaryField("NewIsTraining",clIsTraining) == 1 && clIsTraining[0] == '1')
			return FALSE;
		if (ReadTemporaryField("NewIsHoliday",clIsHoliday) == 1 && clIsHoliday[0] == '1')
			return FALSE;
		if (ReadTemporaryField("NewIsShiftChangeoverDay",clIsShiftChangeoverDay) == 1 && clIsShiftChangeoverDay[0] == '1')
			return FALSE;
		if (ReadTemporaryField("NewIsCompensation",clIsCompensation) == 1 && clIsCompensation[0] == '1')
			return FALSE;
		if (ReadTemporaryField("NewIsIllness",clIsIllness) == 1 && clIsIllness[0] == '1')
			return FALSE;
	}

	return TRUE;
}

/*****************************************************/
/* ACCOUNT 17: number of (normal) absences per month */
/* ***************************************************/
static void bas_17()
{
	char 	clLastCommand[4];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clNewDrrRoss[2];
	char	clOldShiftUrno[12];
	char	clOldDrrRoss[2];
	char	clYear[6];
	char	clMonth[4];
	char	clOldAccValue[128];
	char	clNewShiftUrno[12];
	int	ilOldValue;
	int	ilNewValue;
	int	ilResultValue;
	double	dlNewAccValue;

	dbg(DEBUG, "============================= ACCOUNT 17 START ===========================");

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 17: Command: (%s)",clLastCommand);

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 17: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 17: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 17: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 17: pcgFields = <%s>",pcgFields);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 17: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 17: Stopped, because <clSday> = undef!");
		return;
	}

	/* BSDU of new DRR-record */
	if (ReadTemporaryField("NewBsdu",clNewShiftUrno) == 0)
	{
		dbg(TRACE,"Account 17: Stopped, because <clNewShiftUrno> = undef!");
		return;
	}

	/* Status des neuen Datensatzes	*/
	if (ReadTemporaryField("NewRoss",clNewDrrRoss) == 0)
	{
		dbg(TRACE,"Account 17: <clNewDrrRoss> = undef!");
		clOldShiftUrno[0] = '\0';
	}

	/* if URT get old account relevant values */
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* checking the old data */
		if (strlen(pcgOldData) == 0)
		{
			dbg(TRACE,"Account 17: <pcgOldData> = undef!");
		}
		else
		{
			dbg(DEBUG,"Account 17: <pcgOldData> = <%s>",pcgOldData);
		}

		/* BSDU of old DRR-record */
		if (ReadTemporaryField("OldBsdu",clOldShiftUrno) == 0)
		{
			dbg(TRACE, "Account 17: <clOldShiftUrno> = undef!");
			clOldShiftUrno[0] = '\0';
		}

		/* status of old record */
		if (ReadTemporaryField("OldRoss",clOldDrrRoss) == 0)
		{
			dbg(TRACE, "Account 17: <clOldDrrRoss> = undef!");
			clOldDrrRoss[0] = '\0';
		}
	}

	/* starting calculation */
	/* getting old value */
	ilOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
		if (strcmp(clOldShiftUrno,"0") != 0 &&  (clOldShiftUrno[0] != '\0'))
		{
			/* use only active DRR-records */
			if (strcmp(clOldDrrRoss,"A") == 0)
			{
				if (IsAbsence(TRUE))
				{
					ilOldValue = 1;
				}
				else
				{
					ilOldValue = 0;
				}
				dbg(DEBUG, "Account 17: <ilOldValue> = <%d>" , ilOldValue);
			}
			else
			{
				dbg(TRACE, "Account 17: Wrong status of old DRR-record: <clOldDrrRoss> = <%s>" ,clOldDrrRoss);
			}
		}
	}

	/* getting new value */
	ilNewValue = 0;

	/* use only active DRR-records */
	if (strcmp(clNewDrrRoss,"A") == 0)
	{
		if (strcmp(clNewShiftUrno,"0") != 0 &&  (clNewShiftUrno[0] != '\0'))
		{
			if (IsAbsence(FALSE))
			{
				ilNewValue = 1;
			}
			else
			{
				ilNewValue = 0;
			}
		}
		dbg(DEBUG, "Account 17: <ilNewValue> = <%d>",ilNewValue);
	}
	else
	{
		dbg(TRACE, "Account 17: Wrong status of new DRR-record: <clNewDrrRoss> = <%s>" ,clNewDrrRoss);
	}

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		ilResultValue = ilNewValue - ilOldValue;
	}
	else
	{
		ilResultValue = - ilNewValue; 
	}

	clMonth[0] = clSday[4];
	clMonth[1] = clSday[5];
	clMonth[2] = '\0';

	strncpy(clYear,clSday,4);
	clYear[4] = '\0';

	/* get ACC record */
	GetAccValueHandle("17",clMonth,clYear,clStaffUrno,"CL",clOldAccValue);

	dbg(DEBUG, "Account 17: Old ACC value: <%s>", clOldAccValue);

	dlNewAccValue = atof(clOldAccValue) + ilResultValue;

	dbg(DEBUG, "Account 17: New ACC value: <%lf>",dlNewAccValue);

	if (ilResultValue != 0)
	{
		UpdateAccountHandle(dlNewAccValue,"17",clMonth,clYear,clStaffUrno,"SCBHDL");
	}
	else
	{
		dbg(DEBUG, "Account 17: Value not changed: <%s>", clOldAccValue);
	}
}

