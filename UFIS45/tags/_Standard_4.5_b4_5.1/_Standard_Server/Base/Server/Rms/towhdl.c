#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id:  Ufis/_Standard/_Standard_Server/Base/Server/Rms/towhdl.c 1.9 2012/05/01 11:42:50EEST gfo Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/* JHA 23.06.2009                                                             */
/* PRF 9363: The tow demands created automatically by TOWHDL do not appear in */
/*           Flight Schedule. The leading comma in the selection part of      */
/*           the RAC BC is missing.                                           */
/* JHA 09.07.2009:                                                            */
/*           calculation of towdemands corrected.                             */
/* JHA 15.09.2009:                                                            */
/*           command IRT / URT included (New version = 1.7)                   */
/* GFO 09.12.2011:                                                            */
/*           Don't Update Towings when they have already a PSTA,PSTD          */
/*           change STOA,STOD to TIFA,TIFD                                    */
/* GFO 05.01.2012:                                                            */
/*           Don't Send BC when there is no updated on the record             */
/*           send UFR in case of update , RAC in case of insert  ,DFR in case */  
/*           of deleting unneeded tows                                        */
/*   !!!!!!! Update is perfromed by Fligt in case the PSTA,PSTD are filled    */  
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif
#ifndef max
#define max(a,b) (a > b ? a : b)
#endif

#define RUDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRudArray.plrArrayFieldOfs[ipFieldNo-1]])
#define AFTFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgAftArray.plrArrayFieldOfs[ipFieldNo-1]])

#define NEWTOWFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgNewTowArray.plrArrayFieldOfs[ipFieldNo-1]])
#define OLDTOWFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOldTowArray.plrArrayFieldOfs[ipFieldNo-1]])

#define RUEFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRueArray.plrArrayFieldOfs[ipFieldNo-1]])

#define RPQFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRpqArray.plrArrayFieldOfs[ipFieldNo-1]])
#define RLOFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgRloArray.plrArrayFieldOfs[ipFieldNo-1]])
#define SGRFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgSgrArray.plrArrayFieldOfs[ipFieldNo-1]])

BOOL IS_EMPTY (char *pcpBuf) 
{
	return	(!pcpBuf || *pcpBuf==' ' || *pcpBuf=='\0') ? TRUE : FALSE;
}

extern int  get_no_of_items(char *s);
extern int  get_item_no(char *, char *, short);
extern int	GetServerTimeStamp(char *, int, long, char *);
extern int	AddSecondsToCEDATime(char *, time_t, int);
extern long	StrDiffTime(char *, char *);

/*default values*/
#define MINDEDUDEFAULT	"1800"

/* fieldlength */
#define DATELEN		14
#define HOPOLEN		3
#define URNOLEN		10
#define DURATIONLEN	10

/*flags for new CCI demands */
#define	CUT			-3
#define	DELETE		-2
#define	OPENDEL		-1
#define	NOOP		0
#define	OPENNEW		1
#define	NEWDEM		2
#define	ADDED		3


#define RUL_FLD_LEN	(4)
#define MAXRUES (2000)
#define MAXALOCREFS (12)
#define MAXALOCREFENTRIES (8)

#define EVT_TURNAROUND	(0)
#define EVT_INBOUND			(1)
#define EVT_OUTBOUND		(2)
#define EVT_UNKNOWN     (3)

#define CMD_IFR     (1)
#define CMD_UFR     (2)
#define CMD_FIR     (3)
#define CMD_CFL     (4)

#define CMD_IAZ     (5)
#define CMD_UAZ     (6)
#define CMD_DAZ     (7)
#define CMD_UDR     (8)

#define CMD_IFI     (9)
#define CMD_UFI     (10)
#define CMD_DFI     (11)
#define CMD_UCD     (12)

#define CMD_DFR     (13)
#define CMD_IDR     (14)
#define CMD_IRT     (15)
#define CMD_URT     (16)

#define CMD_DRT     (17)
#define CMD_ANSWER	(18)
#define CMD_DLF		(19)

#define IDX_INBOUND (0)
#define IDX_OUTBOUND (1)

static char *prgCmdTxt[] = {    "IFR","UFR", "FIR", "CFL", 
								"IAZ", "UAZ", "DAZ", "UDR", 
								"IFI", "UFI", "DFI",
								"DFR", "IDR", "IRT", "URT",
								"DRT", "ANSWER", "DLF", NULL };
static int rgCmdDef[] = { CMD_IFR, CMD_UFR, CMD_FIR, CMD_CFL,
							CMD_IAZ, CMD_UAZ, CMD_DAZ, CMD_UDR, 
							CMD_IFI, CMD_UFI, CMD_DFI,
							CMD_DFR, CMD_IDR, CMD_IRT, CMD_URT,
							CMD_DRT, CMD_ANSWER, CMD_DLF, 0 };


#define CDT_SIMPLE  (0)
#define CDT_DYNAMIC (1)
#define CDT_STATIC  (2)

#define CMP_FALSE   (1)
#define CMP_SMALLER (2)
#define CMP_EQUAL   (3)
#define CMP_GREATER (4)

#define TPL_ACTIVE  (1)

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/

#define TOW_FIELDS "FKEY,URNO,RKEY,AURN,CSGN,FLNO,ALC2,ALC3,ORG3,DES3,ORG4,DES4,FLTN,FLNS,ACT3,ACT5,REGN,ADID,STOA,STOD,FTYP,PSTA,PSTD,OFBL,ONBL,TIFA,TIFD,TISA,TISD,USEU,LSTU,REM1,CDAT,USEC"  /* Used Fieldlist from AFTTAB */
#define ADD_TOW_FIELDS "JFND,SAVE,DELE"								  				 /* Additional internal fields */

#define RUD_FIELDS "ALOC,DBAR,DBFL,DEAR,DEBE,DEDU,DEEN,DEFL,DIDE,DRTY,EADB,FADD,FFPD,HOPO,LADE,LSTU,MAXD,MIND,REPD,RETY,RTDB,RTDE,SDTI,SUTI,TSDB,TSDE,TSPD,TTGF,TTGT,UDGR,UGHS,UPDE,URNO,URUE"
#define ADD_RUD_FIELDS "FLAG"
#define AFT_FIELDS "FKEY,URNO,RKEY,AURN,CSGN,FLNO,ALC2,ALC3,ORG3,DES3,ORG4,DES4,FLTN,FLNS,ACT3,ACT5,REGN,ADID,STOA,STOD,FTYP,PSTA,PSTD,OFBL,ONBL,TIFA,TIFD,TISA,TISD,USEU,LSTU,REM1,CDAT,USEC"

#define RUE_FIELDS "URNO,FISU,RUNA"
#define RPF_FIELDS "URNO,FCCO,GTAB,UPFC,URUD"
#define RPQ_FIELDS "URNO,QUCO,GTAB,UPER,URUD"
#define REQ_FIELDS "URNO,EQCO,GTAB,UEQU,URUD"
#define RLO_FIELDS "URNO,RLOC,GTAB,REFT,URUD"
#define SGR_FIELDS "URNO,GRPN"

#define URNOS_TO_FETCH 50

#define NO_BC	0
#define TCP_SBC	1
#define UDP_SBC	2
#define SINGLE_TO_BCHDL		4
#define SINGLE_TO_ACTION	8

#define IDX_IRTD 4	/* Index of List "INSERT" in prgTowInf->DataList */
#define IDX_URTD 5  /* Index of List "UPDATE" in prgTowInf->DataList */
#define IDX_DRTU 6  /* Index of List "DELETE" in prgTowInf->DataList */
#define IDX_IRTU 7	/* Index of List "IRT_URNO" in prgTowInf->DataList */
#define IDX_URTU 8	/* Index of List "URT_URNO" in prgTowInf->DataList */
#define IDX_DRTD 9	/* Index of List "DRT_DATA" in prgTowInf->DataList */

static int rgUIdx[3] = { IDX_IRTU, IDX_URTU, IDX_DRTU };

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = 0;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static EVENT *prgOutEvent      = NULL ;
static long  lgOutEventSize  = 0 ;
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igUrnoItemNo = 0;
static int   igRegnItemNo = 0;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgDataArea[8192];
static char  cgHopo[8];                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static char  cgProcessName[80];                  /* name of executable */
static int   igRouter = 1200;
static char  cgRegn[26];
static char  cgMinDedu[DURATIONLEN+1];
static int   lgEventType = 0;
static char  *pcgFieldList = NULL;
static char  *pcgInbound = NULL;
static char  *pcgOutbound = NULL;
static char  *pcgTemplates = 0;
static int	 igLenofTemplates = 0;

static time_t tgBeginStamp = 0;
static time_t tgEndStamp = 0;
static time_t tgQueDiff = 0;
static time_t tgMaxStampDiff = 0;
static BOOL   bgShowEvent = FALSE;

static char pcgTmpBuf[8192];
static char pcgNewTowingBuf[8192];

static char   cgCfgBuffer[1024];
static char   cgRegnAloc[1024];
static char	  pcgCCIDemBuf[8192];
static BOOL	  bgOldDemandsEx = FALSE;
static int	  igNwD = 0;
static int	  igMinDemand = 1;
static int	  igMaxDemand = 0;
static time_t tgIvStart,tgIvEnd;
static char	  *pcgIvStart = NULL;
static char	  *pcgIvEnd = NULL;
static char	  *pcgRudUrno = NULL;
static char	  *pcgStep = NULL;
static char   cgTdi1[24];
static char   cgTdi2[24];
static char   cgTich[24];
static int	  igStartUpMode = TRACE;
static int	  igRuntimeMode = 0;
static int	  igTOIgnore = 0;	/* don't leave gaps smaller than igToIgnore, if possible */
static BOOL	  bgSendSBC = FALSE;
static BOOL	  bgSendUpdDem = FALSE;
static BOOL	  bgUseFIDSTimes = FALSE;
static char	  cgMinDebe[15];
static char	  cgMaxDeen[15];

static char	  cgHopo3[5];	  /* used in init towings for orgx and desx */
static char   cgHopo4[5];

static char   pcgRudFields[401];
static char   pcgRudAddFields[41];

static char   pcgRueFields[401];

static char   pcgTowFields[401];
static char   pcgAftFields[401];

static long   lgActUrno =0 ;
static int    igReservedUrnoCnt = 0;
static int	  igActionId = 0;	
static char   cgIgnoreAloc [124]="";
static BOOL   bgDelInvalidDemands = FALSE;
static unsigned int  igLoadSel = 0;
static long   lgBCDays = -1;


struct _rgExDArray
{
	long	lrStartInSec;
	float	frSumOfDem;
	long	lrDiffOfDem;
};
typedef struct	_rgExDArray	EXDARRAY;

EXDARRAY	*prgExD = 0;

struct _rgNwDArray
{
	char	crStartDemand[DATELEN+1];
	char	crEndDemand[DATELEN+1];
	long	lrStartInSec;
	long	lrEndInSec;
	short	srState;
};
typedef	struct	_rgNwDArray	NWDARRAY;

NWDARRAY	*prgNwD = 0;

static struct RueUrnoStruct
{
	char Urno[11];
	char Fisu[2];
	char  IsUsed;
} prgRueUrnos[MAXRUES];

static struct AlocRefStruct
{
	char Name[24];
	char Table[MAXALOCREFENTRIES+1][14];
	char Field[MAXALOCREFENTRIES+1][12];
	int  IsUsed;
} prgAlocRefInbound[MAXALOCREFS+1],
		 prgAlocRefOutbound[MAXALOCREFS+1];

#define INBOUNDTIMES "TIFA,STOA,ETAI,ETOA,TMOA,LAND,ONBE,ONBL"
#define OUTBOUNDTIMES "TIFD,STOD,ETDI,ETOD,OFBL,AIRB"

static char   InboundTimes[12][16] = { 0 };
static char   OutboundTimes[12][16] = { 0 };
static char   cgSoh[2];
static char   cgStx[2];
static char   cgKomma[2];
static char   cgTplLoadExt[3][101] =  { " AND URUD IN (SELECT URNO FROM RUDTAB WHERE URUE IN (SELECT URNO FROM RUETAB WHERE UTPL IN (%s)))",	
									" AND URUD IN (SELECT URNO FROM RUDTAB WHERE UTPL IN (%s))", 
									" AND UTPL IN (%s)" };


/* static array informations */
#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (512)

struct _arrayinfo {
	HANDLE   rrArrayHandle;
	char     crArrayName[ARR_NAME_LEN+1];
	char     crTableName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char     *pcrArrayRowBuf;			/* no Buffer will be allocated, only used as row pointer */
	long     lrArrayRowLen;
	long     *plrArrayFieldOfs;
	long     *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char     *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long     *plrIdx01FieldPos;
	long     *plrIdx01FieldOrd;
	HANDLE	 rrIdx02Handle;
	char	 crIdx02Name[ARR_NAME_LEN+1];
	char	 crIdx02FieldList[ARR_FLDLST_LEN+1];
	long	 lrIdx02FieldCnt;
	char	 *pcrIdx02RowBuf;
	long	 lrIdx02RowLen;
	long	 *plrIdx02FieldPos;
	long	 *plrIdx02FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgRudArray;
static ARRAYINFO rgRueArray;

static ARRAYINFO rgNewTowArray;
static ARRAYINFO rgOldTowArray;

static ARRAYINFO rgAftArray;
static ARRAYINFO rgCCIRudArray;

static int igTowFKEY;
static int igTowURNO;
static int igTowRKEY;
static int igTowAURN;
static int igTowCSGN;
static int igTowFLNO;
static int igTowALC2;
static int igTowALC3;
static int igTowORG3;
static int igTowDES3;
static int igTowORG4;
static int igTowDES4;
static int igTowFLTN;
static int igTowFLNS;
static int igTowACT3;
static int igTowACT5;
static int igTowREGN;
static int igTowADID;
static int igTowSTOA;
static int igTowSTOD;
static int igTowFTYP;
static int igTowPSTA;
static int igTowPSTD;
static int igTowOFBL;
static int igTowONBL;
static int igTowTIFA;
static int igTowTIFD;
static int igTowTISA;
static int igTowTISD;
static int igTowUSEU;
static int igTowLSTU;
static int igTowREM1;
static int igTowCDAT;
static int igTowUSEC;

static int igTowJFND;
static int igTowSAVE;
static int igTowDELE;

static int igRudALOC;
static int igRudDBAR;
static int igRudDBFL;
static int igRudDEFL;
static int igRudDEAR;
static int igRudDEBE;
static int igRudDEDU;
static int igRudDEEN;
static int igRudDIDE;
static int igRudDRTY;
static int igRudEADB;
static int igRudFADD;
static int igRudFFPD;
static int igRudHOPO;
static int igRudLADE;
static int igRudLSTU;
static int igRudMAXD;
static int igRudMIND;
static int igRudREPD;
static int igRudRTDB;
static int igRudRTDE;
static int igRudSDTI;
static int igRudSUTI;
static int igRudTSDB;
static int igRudTSDE;
static int igRudTSPD;
static int igRudTTGF;
static int igRudTTGT;
static int igRudUDGR;
static int igRudUGHS;
static int igRudUPDE;
static int igRudURNO;
static int igRudURUE;
static int igRudRETY;
static int igRudFLAG;
static int igRudUSES;
static int igRudUTPL=-1;
static int igRudQUCO=-1;
static int igRudRECO=-1;

static int igRueURNO;
static int igRueFISU;
static int igRueRUNA;

static int igAftFKEY;
static int igAftURNO;
static int igAftRKEY;
static int igAftAURN;
static int igAftCSGN;
static int igAftFLNO;
static int igAftALC2;
static int igAftALC3;
static int igAftORG3;
static int igAftDES3;
static int igAftORG4;
static int igAftDES4;
static int igAftFLTN;
static int igAftFLNS;
static int igAftACT3;
static int igAftACT5;
static int igAftREGN;
static int igAftADID;
static int igAftSTOA;
static int igAftSTOD;
static int igAftFTYP;
static int igAftPSTA;
static int igAftPSTD;
static int igAftOFBL;
static int igAftONBL;
static int igAftTIFA;
static int igAftTIFD;
static int igAftTISA;
static int igAftTISD;
static int igAftUSEU;
static int igAftLSTU;
static int igAftREM1;
static int igAftCDAT;
static int igAftUSEC;

static AAT_PARAM *prgTowInf=0;
static BOOL bgUseJODTAB=TRUE;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  InitTowhdl();
static int  SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo);

static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static char *GetRef(char *pcpCfgBuffer,struct AlocRefStruct *pcpAlocBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *pipLen);

static int  GetEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData);
static int  SaveEventTimes(char *pcpInbound,char *pcpOutbound);
static int  GetCommand(char *pcpCommand, int *pipCmd);

static int  GetRueUrnos(char *pcpRueUrnos);
static int  ReadOldTowings(char *pcpOURI,char *pcpOURO, char *pcpTemplates);
static int  ReadOldFid(char *pcpDate, char *pcpTemplates);
static int  ReadRudTab();
static int  CreateTowings();
static int  CreateFid(char *pcpDate);

static int 	AddNewTowingRow();
static int  CompareTowings();
static int  UpdateTowing(ARRAYINFO *prpDest, long lpRowNum, ARRAYINFO *prpSource);

static int  DeleteOldTowings();
static int  SaveTowings(char *pcpRKey, int ipBcMode );
static int  SaveFisu();

static int SendRAC(char *pcpRKey);
static int SendBroadcast(char *pcpRKey);

static int  HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  TimeToStr(char *pcpTime,time_t lpTime); 

static int  StrToTime(char *pcpTime,time_t *pclTime);
static void SunCompStrAddTime(char *,char*,char *);
static int  SendToSqlHdl(char *pcpObject,char *pcpSelection,
						 char *pcpFields,char *pcpData);
static int  SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
					  CMDBLK *prpCmdblk, char *pcpSelection, 
					  char *pcpFields, char *pcpData);
static int  SendAnswer(EVENT *prpEvent,int ipRc);
static void TrimRight(char *pcpBuffer);

static int	SaveIndexInfo(ARRAYINFO *);
static int	CreateIdx1(ARRAYINFO *,char *,char *,char *);       
static int	CreateIdx2(ARRAYINFO *,char *,char *,char *);

static int	LocalToUtc(char *);
static int	UtcToLocal(char *);
static int	SetFisu(char *);
static int	GetDebugLevel(char *, int *);

static int  AddRowToRudArray( long lpRow );
static int  GetNextUrno(char *pcpUrno);
static BOOL SetFisuIfNecessary(char *pcpRueUrno);
static int  DoSendSBC( char *pcpStart, char *pcpEnd, char *pcpTpl, 
					   char *pcpDety, char *pcpWks );

static void CheckPerformance(int ipStart, char *pcpRtTime);

static int  GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen);
static int  ReadConfigEntries();
static BOOL ToBeBroadcasted( char *pcpDate );
static int  SetRudLogicalFields( ARRAYINFO *prpRudArr, char *pcpRudUrno );

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;

	INITIALIZE;						/* General initialization	*/

	if (debug_level < TRACE)
	{
		debug_level = TRACE;
	}

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	} while ((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}
	else
	{
		dbg(TRACE,"MAIN: init_db() OK!");
	}

	/*logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!!*/

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	}

	sprintf(cgConfigFile,"%s/towhdl.cfg",getenv("CFG_PATH"));
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	}

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	}

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitTowhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitTowhdl: init failed!");
			}
		}
	}
	else 
	{
		Terminate(30);
	}

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	
	debug_level = igRuntimeMode;

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			}
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					break;	
				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					/* CloseConnection(); */
					HandleQueues();
					break;	
				case	HSB_DOWN	:
					/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
						ctrl_sta = prgEvent->command;
					Terminate(1);
					break;	
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					ResetDBCounter();
					break;	
				case	REMOTE_DB :
					/* ctrl_sta is checked inside */
					HandleRemoteDB(prgEvent);
					break;
				case	SHUTDOWN	:
					/* process shutdown - maybe from uutil */
					Terminate(1);
					break;
					
				case	RESET		:
					ilRc = Reset();
					break;
					
				case	EVENT_DATA	:
					if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
					{
						ilRc = HandleData(prgEvent);
						if(ilRc != RC_SUCCESS)
						{
							HandleErr(ilRc);
						}
					}
					else
					{
						dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
					}
					break;
					
				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;
				case 100:
					SaveIndexInfo( &rgCCIRudArray );
					break;
				case 101:
					SaveIndexInfo( &rgOldTowArray );
					SaveIndexInfo( &rgNewTowArray );

					break;
				case 102:
/*					SaveIndexInfo( &rgCCITowArray );*/
					break;
				case 103:
/*					
					SaveIndexInfo( &rgRpfArray );
					SaveIndexInfo( &rgRpqArray );
					SaveIndexInfo( &rgReqArray );
					SaveIndexInfo( &rgRloArray );
*/
					break;
				default			:
					dbg(TRACE,"MAIN: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} /* end switch */
		} 
		else 
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		}
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos, ilDBFields;
	
	FindItemInList(pcgAftFields,"FKEY",',',&igAftFKEY,&ilCol,&ilPos);	
	FindItemInList(pcgAftFields,"URNO",',',&igAftURNO,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"RKEY",',',&igAftRKEY,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"AURN",',',&igAftAURN,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"CSGN",',',&igAftCSGN,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"FLNO",',',&igAftFLNO,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ALC2",',',&igAftALC2,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ALC3",',',&igAftALC3,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ORG3",',',&igAftORG3,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"DES3",',',&igAftDES3,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ORG4",',',&igAftORG4,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"DES4",',',&igAftDES4,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"FLTN",',',&igAftFLTN,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"FLNS",',',&igAftFLNS,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ACT3",',',&igAftACT3,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ACT5",',',&igAftACT5,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"REGN",',',&igAftREGN,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ADID",',',&igAftADID,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"STOA",',',&igAftSTOA,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"STOD",',',&igAftSTOD,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"FTYP",',',&igAftFTYP,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"PSTA",',',&igAftPSTA,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"PSTD",',',&igAftPSTD,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"OFBL",',',&igAftOFBL,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"ONBL",',',&igAftONBL,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"TIFA",',',&igAftTIFA,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"TIFD",',',&igAftTIFD,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"TISA",',',&igAftTISA,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"TISD",',',&igAftTISD,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"USEU",',',&igAftUSEU,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"LSTU",',',&igAftLSTU,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"REM1",',',&igAftREM1,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"CDAT",',',&igAftCDAT,&ilCol,&ilPos);
	FindItemInList(pcgAftFields,"USEC",',',&igAftUSEC,&ilCol,&ilPos);

	ilDBFields = get_no_of_items(pcgAftFields);

	FindItemInList(pcgTowFields,"FKEY",',',&igTowFKEY,&ilCol,&ilPos);	
	FindItemInList(pcgTowFields,"URNO",',',&igTowURNO,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"RKEY",',',&igTowRKEY,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"AURN",',',&igTowAURN,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"CSGN",',',&igTowCSGN,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"FLNO",',',&igTowFLNO,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ALC2",',',&igTowALC2,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ALC3",',',&igTowALC3,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ORG3",',',&igTowORG3,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"DES3",',',&igTowDES3,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ORG4",',',&igTowORG4,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"DES4",',',&igTowDES4,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"FLTN",',',&igTowFLTN,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"FLNS",',',&igTowFLNS,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ACT3",',',&igTowACT3,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ACT5",',',&igTowACT5,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"REGN",',',&igTowREGN,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ADID",',',&igTowADID,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"STOA",',',&igTowSTOA,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"STOD",',',&igTowSTOD,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"FTYP",',',&igTowFTYP,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"PSTA",',',&igTowPSTA,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"PSTD",',',&igTowPSTD,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"OFBL",',',&igTowOFBL,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"ONBL",',',&igTowONBL,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"TIFA",',',&igTowTIFA,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"TIFD",',',&igTowTIFD,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"TISA",',',&igTowTISA,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"TISD",',',&igTowTISD,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"USEU",',',&igTowUSEU,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"LSTU",',',&igTowLSTU,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"REM1",',',&igTowREM1,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"CDAT",',',&igTowCDAT,&ilCol,&ilPos);
	FindItemInList(pcgTowFields,"USEC",',',&igTowUSEC,&ilCol,&ilPos);
	
	ilDBFields = get_no_of_items(pcgTowFields);
	FindItemInList(ADD_TOW_FIELDS,"JFND",',',&igTowJFND,&ilCol,&ilPos);
	if ( igTowJFND > 0 )
	{
		igTowJFND += ilDBFields;
	}
	FindItemInList(ADD_TOW_FIELDS,"SAVE",',',&igTowSAVE,&ilCol,&ilPos);
	if ( igTowSAVE > 0 )
	{
		igTowSAVE += ilDBFields;
	}
	dbg(DEBUG,"igTowSave=%d",igTowSAVE);
	FindItemInList(ADD_TOW_FIELDS,"DELE",',',&igTowDELE,&ilCol,&ilPos);
	if ( igTowDELE > 0 )
	{
		igTowDELE += ilDBFields;
	}

	FindItemInList(pcgRudFields,"ALOC",',',&igRudALOC,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DBAR",',',&igRudDBAR,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DBFL",',',&igRudDBFL,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEFL",',',&igRudDEFL,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEAR",',',&igRudDEAR,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEBE",',',&igRudDEBE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEDU",',',&igRudDEDU,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DEEN",',',&igRudDEEN,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DIDE",',',&igRudDIDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"DRTY",',',&igRudDRTY,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"EADB",',',&igRudEADB,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"FADD",',',&igRudFADD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"FFPD",',',&igRudFFPD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"HOPO",',',&igRudHOPO,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"LADE",',',&igRudLADE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"LSTU",',',&igRudLSTU,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"MAXD",',',&igRudMAXD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"MIND",',',&igRudMIND,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"REPD",',',&igRudREPD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"RTDB",',',&igRudRTDB,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"RTDE",',',&igRudRTDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"SDTI",',',&igRudSDTI,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"SUTI",',',&igRudSUTI,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TSDB",',',&igRudTSDB,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TSDE",',',&igRudTSDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TSPD",',',&igRudTSPD,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TTGF",',',&igRudTTGF,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"TTGT",',',&igRudTTGT,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UDGR",',',&igRudUDGR,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UGHS",',',&igRudUGHS,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UPDE",',',&igRudUPDE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"URNO",',',&igRudURNO,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"URUE",',',&igRudURUE,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"RETY",',',&igRudRETY,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"USES",',',&igRudUSES,&ilCol,&ilPos);
	FindItemInList(pcgRudFields,"UTPL",',',&igRudUTPL,&ilCol,&ilPos);

	ilDBFields = get_no_of_items(pcgRudFields);
	FindItemInList(pcgRudAddFields,"FLAG",',',&igRudFLAG,&ilCol,&ilPos);
	if ( igRudFLAG >= 0 )
	{
		igRudFLAG += ilDBFields;
	}
	if ( (igRudUTPL<=0) )
	{
		dbg( TRACE, "InitFieldIndex: igRudUTPL <%d> => Reset load format!",
			  igRudUTPL );
	    igLoadSel = 0; 
	}
	FindItemInList(pcgRudAddFields,"QUCO",',',&igRudQUCO,&ilCol,&ilPos);
	if ( igRudQUCO >= 0 )
	{
		igRudQUCO += ilDBFields;
	}
	FindItemInList(pcgRudAddFields,"RECO",',',&igRudRECO,&ilCol,&ilPos);
	if ( igRudRECO >= 0 )
	{
		igRudRECO += ilDBFields;
	}
	
	FindItemInList(pcgRueFields,"URNO",',',&igRueURNO,&ilCol,&ilPos);
	FindItemInList(pcgRueFields,"FISU",',',&igRueFISU,&ilCol,&ilPos);
	FindItemInList(pcgRueFields,"RUNA",',',&igRueRUNA,&ilCol,&ilPos);

	ilDBFields = get_no_of_items(pcgRueFields);
	
	return ilRc;
}

static int InitTowhdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	char clSection[128];
	char clKeyword[64];
	char *pclCfgBuffer;
	int  ilIndex = 0, ilDummy;
	long pclAddFieldLens[12];
	long llRecoLen = -1, llQucoLen=-1;

	/* BST und SMI wollen das so */
	cgSoh[0] = 0x01;
	cgSoh[1] = 0x00;

	cgStx[0] = 0x02;
	cgStx[1] = 0x00;

	cgKomma[0] = ',';
	cgKomma[1] = 0x00;

	/* now reading from configfile or from database */

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE, "<InitTowhdl> EXTAB,%s,%s not found in SGS.TAB",
				&clSection[0], &clKeyword[0]);
			Terminate(30);
		}
		else 
		{
			dbg(TRACE,"<InitTowhdl> home airport    <%s>",&cgHopo[0]);
		}
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitTowhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		}
		else
		{
			dbg(TRACE,"<InitTowhdl> table extension <%s>",&cgTabEnd[0]);
		}
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	memset(prgAlocRefInbound,0,sizeof(prgAlocRefInbound));
	memset(prgAlocRefOutbound,0,sizeof(prgAlocRefOutbound));

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"regnaloc");

		ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s>",&clSection[0],&clKeyword[0],cgConfigFile);
		}
		else
		{
			dbg(TRACE,"InitTowhdl: <%s> <%s> <%s>",&clSection[0],&clKeyword[0],cgCfgBuffer);
			strcpy(cgRegnAloc,cgCfgBuffer);
		}
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"aloci");

		ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s>",&clSection[0],&clKeyword[0],cgConfigFile);
		}
		else
		{
			dbg(TRACE,"InitTowhdl: <%s> <%s> <%s>",&clSection[0],&clKeyword[0],cgCfgBuffer);
			ilIndex = 0;
			pclCfgBuffer = cgCfgBuffer;
			do
			{
				pclCfgBuffer = 
					GetRef(pclCfgBuffer,&prgAlocRefInbound[ilIndex++]);
			} while (*pclCfgBuffer != '\0');
			ilIndex = 0;
			while(*prgAlocRefInbound[ilIndex].Name != '\0')
			{
				int ilTableNo = 0;
				dbg(TRACE,"Name: <%s>",prgAlocRefInbound[ilIndex].Name);
				while (*prgAlocRefInbound[ilIndex].Table[ilTableNo] != '\0')
				{
					dbg(TRACE,"Table: <%s>, Field: <%s>",
						prgAlocRefInbound[ilIndex].Table[ilTableNo],
						prgAlocRefInbound[ilIndex].Field[ilTableNo]);
					ilTableNo++;
				}
				ilIndex++;
			}

		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"aloco");
		ilIndex = 0;

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s>",&clSection[0],&clKeyword[0],cgConfigFile);
		}
		else
		{
			dbg(TRACE,"InitTowhdl: <%s> <%s> <%s>",&clSection[0],&clKeyword[0],cgCfgBuffer);
			ilIndex = 0;
			pclCfgBuffer = cgCfgBuffer;
			do
			{
				pclCfgBuffer = 
					GetRef(pclCfgBuffer,&prgAlocRefOutbound[ilIndex++]);
			} while (*pclCfgBuffer != '\0');

			ilIndex = 0;
			while(*prgAlocRefOutbound[ilIndex].Name != '\0')
			{
				int ilTableNo = 0;
				dbg(TRACE,"Name: <%s>",prgAlocRefOutbound[ilIndex].Name);
				while (*prgAlocRefOutbound[ilIndex].Table[ilTableNo] != '\0')
				{
					dbg(TRACE,"Table: <%s>, Field: <%s>",
						prgAlocRefOutbound[ilIndex].Table[ilTableNo],
						prgAlocRefOutbound[ilIndex].Field[ilTableNo]);
					ilTableNo++;
				}
				ilIndex++;
			}

		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0], "DEMANDLEN");
		sprintf(&clKeyword[0], "MIN_DURATION");

		ilRc = iGetConfigRow(cgConfigFile, clSection, clKeyword, CFG_STRING,
								cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s>",
				&clSection[0], &clKeyword[0], cgConfigFile);
			strcpy(cgMinDedu, MINDEDUDEFAULT);
			ilRc = RC_SUCCESS;
		}
		else
		{
			dbg(TRACE, "InitTowhdl: <%s> <%s> <%s>", &clSection[0],
				&clKeyword[0], cgCfgBuffer);
			strcpy(cgMinDedu, cgCfgBuffer);
			if (!atoi(cgMinDedu))
				strcpy(cgMinDedu, MINDEDUDEFAULT);
		}
		dbg(TRACE,"Minimal length of demand: %s",cgMinDedu);

		ilRc = iGetConfigRow ( cgConfigFile, "CCI", "MIN_GAP_LENGTH", 
						  CFG_STRING, cgCfgBuffer );
		if ( ilRc == RC_SUCCESS )
		{
			igTOIgnore = atoi ( cgCfgBuffer );
		}
		else
		{
			dbg(TRACE,"InitTowhdl: not found: <CCI>, <MIN_GAP_LENGTH> in <%s>",
					   cgConfigFile);
			ilRc = RC_SUCCESS;
		}
		dbg(TRACE,"InitTowhdl: Gaps <= %d minutes will be ignored",igTOIgnore);
	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"SendSBC");

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{	/* parameter missing -> take default and go on */
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s> take default",&clSection[0],&clKeyword[0],cgConfigFile);
			ilRc = RC_SUCCESS; 
		}
		else
		{
			if ( strcmp(cgCfgBuffer,"YES") == 0)
			{
				bgSendSBC = TRUE;
			}
		}/* end of if */
		dbg(TRACE,"InitTowhdl: SendSBC <%d>", bgSendSBC );
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"HOPO3");

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{	/* parameter missing -> take default and go on */
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s> take default",&clSection[0],&clKeyword[0],cgConfigFile);
			strcpy(cgHopo3,cgHopo);
			ilRc = RC_SUCCESS; 
		}
		else
		{
			strcpy(cgHopo3,cgCfgBuffer);
			
		}/* end of if */
		dbg(TRACE,"InitTowhdl: HOPO3 <%s>", cgHopo3 );
	}/* end of if */
	

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"HOPO4");

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{	/* parameter missing -> take default and go on */
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s> take default",&clSection[0],&clKeyword[0],cgConfigFile);
			strcpy(cgHopo3,cgHopo);
			ilRc = RC_SUCCESS; 
		}
		else
		{
			strcpy(cgHopo4,cgCfgBuffer);
			
		}/* end of if */
		dbg(TRACE,"InitTowhdl: HOPO4 <%s>", cgHopo4 );
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"MAIN");
		sprintf(&clKeyword[0],"SendUPDDEM");

		ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{	/* parameter missing -> take default and go on */
			dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s> take default",clSection,clKeyword,cgConfigFile);
			ilRc = RC_SUCCESS; 
		}
		else
		{
			if ( strcmp(cgCfgBuffer,"YES") == 0)
			{
				bgSendUpdDem = TRUE;
			}
		}/* end of if */
		dbg(TRACE,"InitTowhdl: SendUPDDEM <%d>", bgSendUpdDem );
	}/* end of if */
	
	sprintf(&clSection[0],"MAIN");
	strcpy(clKeyword,"UseFidsTimes");

	ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,cgCfgBuffer);
	if(ilRc != RC_SUCCESS)
	{	/* parameter missing -> take default and go on */
		dbg(TRACE,"InitTowhdl: not found: <%s> <%s> in <%s> take default",&clSection[0],&clKeyword[0],cgConfigFile);
		ilRc = RC_SUCCESS; 
	}
	else
	{
		if ( strcmp(cgCfgBuffer,"YES") == 0)
		{
			bgUseFIDSTimes = TRUE;
		}
	}/* end of if */
	dbg(TRACE,"InitTowhdl: UseFIDSTimes <%d>", bgUseFIDSTimes );

	ilRc = iGetConfigRow ( cgConfigFile, "MAIN", "DelInvalidDemands", CFG_INT, (char *)&ilDummy);
	if ( ilRc == RC_SUCCESS )
	{
		dbg( TRACE,"InitTowhdl: Deletion of invalid demands is handled by MODID <%d>", ilDummy );
		if ( ilDummy == mod_id )
		{
			bgDelInvalidDemands = TRUE;
		}
	}
	dbg(TRACE,"InitTowhdl: bgDelInvalidDemands <%d>", bgDelInvalidDemands );

	ilRc = iGetConfigRow ( cgConfigFile, "SYSTEM", "TIME_TRACING", CFG_STRING, cgCfgBuffer );
	if ( ilRc == RC_SUCCESS )
	{
		tgMaxStampDiff = atol ( cgCfgBuffer );
		dbg(TRACE,"InitTowhdl: Warning if processing of event lasts longer than %d seconds (since flight)",
			tgMaxStampDiff );
	}
	else
	{
		dbg(TRACE,"InitTowhdl: not found: <SYSTEM>, <TIME_TRACING> in <%s>", cgConfigFile);
		ilRc = RC_SUCCESS;
	}

	ilRc = ReadConfigEntries();

	if ( pcgTemplates = malloc ( 512 ) )
	{
		pcgTemplates[0] = '\0';
		igLenofTemplates = 512;
		dbg( DEBUG, "InitTowhdl: Allocated 512 Bytes for Templatebuffer" );
	}
	else
	{
		dbg( TRACE, "InitTowhdl: Unable to allocated 512 Bytes for Templatebuffer" );
		ilRc = RC_NOMEM;
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(10,10);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
	/** read TICH ***********/
		char pclDataArea[2256];
		char pclSqlBuf[256];
		short slCursor = 0;
		sprintf(pclSqlBuf,
				"SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3 = '%s'",cgHopo);
		ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
		if (ilRc != DB_SUCCESS)
		{
			dbg(TRACE,"Home Airport <%s> not found in APTTAB %d",cgHopo,ilRc);
			check_ret(ilRc);
		} /* end while */
		else
		{
			strcpy(cgTdi1,"60");
			strcpy(cgTdi2,"60");
			get_fld(pclDataArea,0,STR,14,cgTich);
			get_fld(pclDataArea,1,STR,14,cgTdi1);
			get_fld(pclDataArea,2,STR,14,cgTdi2);
			TrimRight(cgTich);
			if (*cgTich != '\0')
			{
				TrimRight(cgTdi1);
				TrimRight(cgTdi2);
				sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
				sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
			}
			dbg(TRACE,"Tich: <%s> Tdi1: <%s> Tdi2 <%s>",
				cgTich,cgTdi1,cgTdi2);
		}
		close_my_cursor(&slCursor);
		slCursor = 0;
	}

    igActionId = tool_get_q_id("action");
    dbg(TRACE, "InitTowhdl: action mod_id <%d> found", igActionId );
    
	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitTowhdl: SetArrayInfo NEWTOWS (AFTTAB) start");
		strcpy ( pcgTowFields, TOW_FIELDS );

		pclAddFieldLens[0] = 1;
		pclAddFieldLens[1] = 1;
		pclAddFieldLens[2] = 1;
		pclAddFieldLens[3] = 0;
		pclAddFieldLens[4] = 0;
		/*ilRc = SetArrayInfo("NEWTOWS","AFTTAB",pcgTowFields, ADD_TOW_FIELDS,pclAddFieldLens,&rgNewTowArray);*/
		ilRc = SetArrayInfo("NEWTOWS","AFTTAB",pcgTowFields, ADD_TOW_FIELDS,pclAddFieldLens,&rgNewTowArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo NEWTWOS failed <%d>",ilRc);
		}
		else
		{
			dbg(TRACE, "InitTowhdl: SetArrayInfo NEWTOWS OK");
			dbg(TRACE, "<%s> %d", rgNewTowArray.crArrayName,
				rgNewTowArray.rrArrayHandle);
			CEDAArraySendChanges2ACTION(&rgNewTowArray.rrArrayHandle,
   										rgNewTowArray.crArrayName, TRUE);
			CEDAArraySendChanges2BCHDL(&rgNewTowArray.rrArrayHandle,
										rgNewTowArray.crArrayName, TRUE);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitTowhdl: SetArrayInfo TOWTAB (AFTTAB) start");

		pclAddFieldLens[0] = 1;
		pclAddFieldLens[1] = 1;
		pclAddFieldLens[2] = 1;
		pclAddFieldLens[3] = 0;
		pclAddFieldLens[4] = 0;
		ilRc = SetArrayInfo("TOWS","AFTTAB",pcgTowFields, ADD_TOW_FIELDS, pclAddFieldLens,&rgOldTowArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo TOWS failed <%d>",ilRc);
		}
		else
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo TOWS OK");
			CEDAArraySendChanges2ACTION(&rgOldTowArray.rrArrayHandle,
								        rgOldTowArray.crArrayName,TRUE);
			CEDAArraySendChanges2BCHDL( &rgOldTowArray.rrArrayHandle,
							            rgOldTowArray.crArrayName,TRUE);
		}/* end of if */
	}
	
	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"InitTowhdl: SetArrayInfo RUDTAB start");
		strcpy ( pcgRudFields, RUD_FIELDS );
		strcpy ( pcgRudAddFields, ADD_RUD_FIELDS );
		if ( GetFieldLength( &cgHopo[0], "RUDTAB", "USES", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgRudFields, ",USES" );
		}
		else
		{
			dbg( TRACE,"InitTowhdl: field RUDTAB.USES not in DB configuration" );
		}
		if ( GetFieldLength( &cgHopo[0], "RUDTAB", "UTPL", pclAddFieldLens ) 
			 == RC_SUCCESS )
		{
			strcat ( pcgRudFields, ",UTPL" );
		}
		else
		{
			dbg( TRACE,"InitTowhdl: field RUDTAB.UTPL not in DB configuration" );
		}
		dbg( DEBUG, "Actual RUDTAB-fields: %s", pcgRudFields );

		pclAddFieldLens[0] = 2;
		pclAddFieldLens[1] = pclAddFieldLens[2] = pclAddFieldLens[3] = 0;
		if ( llRecoLen > 0 )
		{
			pclAddFieldLens[1] = llRecoLen;
			strcat ( pcgRudAddFields, ",RECO" );
			if ( llQucoLen > 0 ) 
			{
				pclAddFieldLens[2] = llQucoLen;
				strcat ( pcgRudAddFields, ",QUCO" );
			}
		}
		dbg( TRACE,"InitTowhdl: Addiditonal RUD-Fields <%s>", pcgRudAddFields );

		ilRc = SetArrayInfo("RUDTAB", "RUDTAB",	pcgRudFields, pcgRudAddFields,
							pclAddFieldLens, &rgRudArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo RUDTAB failed <%d>",ilRc);
		}
		else
		{
			ilRc = CreateIdx1(&rgRudArray,"IDXRUD1","URNO","A");
			if ( ilRc != RC_SUCCESS )
			{
				dbg(TRACE,"Init_towhdl: CreateIdx1 for RUD failed <%d>", ilRc);
			}
			else
			{
				dbg(TRACE,"InitTowhdl: SetArrayInfo RUDTAB OK");
			}
		}
	}


	if(ilRc == RC_SUCCESS)
	{

		dbg(DEBUG,"InitTowhdl: SetArrayInfo CCIRUDTAB start");

		ilRc = SetArrayInfo("CCIRUD","RUDTAB",pcgRudFields,pcgRudAddFields,
							pclAddFieldLens, &rgCCIRudArray);

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo CCIRUDTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy ( clSection, "WHERE URUE in (select URNO from RUETAB where rust!='2')" );
			ilRc = CEDAArrayRefill (&rgCCIRudArray.rrArrayHandle,
									rgCCIRudArray.crArrayName, clSection, NULL, ARR_FIRST );
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"SetArrayInfo: CEDAArrayRefill failed <%d>",ilRc);
			}
			else
			{
				dbg(TRACE,"InitTowhdl: SetArrayInfo CCIRUDTAB OK");
				ilRc = CreateIdx1(&rgCCIRudArray,"IDXRUD1","URNO","A");
				if (ilRc!=RC_SUCCESS)
				{
					dbg(TRACE,"Init_towhdl: CreateIdx1 for RUD failed <%d>",
						ilRc);
				}
				else
				{
		 			ilRc = CreateIdx2 ( &rgCCIRudArray,"IDXRUD2","URUE,FADD,FFPD,UPDE",
										"A,A,A,A" );
					if (ilRc!=RC_SUCCESS)
					{
						dbg(TRACE,"Init_towhdl: CreateIdx2 for RUD failed <%d>",
							ilRc);
					}
				}
				ConfigureAction("RUDTAB", 0, 0 );
			}
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"InitTowhdl: SetArrayInfo AFTTAB start");
		strcpy ( pcgAftFields, AFT_FIELDS );

		ilRc = SetArrayInfo("AFTTAB","AFTTAB",pcgAftFields,NULL,NULL,&rgAftArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo AFTTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo AFTTAB OK");
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"InitTowhdl: SetArrayInfo RUETAB start");
		strcpy ( pcgRueFields, RUE_FIELDS );

		ilRc = SetArrayInfo("RUETAB","RUETAB",pcgRueFields,NULL,NULL,&rgRueArray);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo RUETAB failed <%d>",ilRc);
		}
		else
		{
			dbg(TRACE,"InitTowhdl: SetArrayInfo RUETAB OK");
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}
		else
		{
			dbg(TRACE,"InitTowhdl: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}

	if( (ilRc == RC_SUCCESS) && (igRudQUCO > 0) && (llRecoLen > 0) && (llQucoLen>0) )
	{
		dbg( DEBUG, "InitTowhdl: Length of field QUCU <%ld> index <%d>", 
			  rgCCIRudArray.plrArrayFieldLen[igRudQUCO-1], igRudQUCO );
		SetRudLogicalFields ( &rgCCIRudArray, "" );
	}

	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
	}/* end of if */

	{
		long llMaxDbgLines = 30000;
	    int ilMaxDbgFiles = 10;

		ilRc = iGetConfigRow (cgConfigFile, "SYSTEM", "MAXDBGLINES", 
						  CFG_STRING, cgCfgBuffer );
		if ( ilRc == RC_SUCCESS )
		{
			llMaxDbgLines = atol ( cgCfgBuffer );
		}
		else
		{
			dbg(TRACE,"InitTowhdl: not found: <SYSTEM>, <MAXDBGLINES> in <%s>",
					   cgConfigFile);
			ilRc = RC_SUCCESS;
		}
		ilRc = iGetConfigRow (cgConfigFile, "SYSTEM", "MAXDBGFILES", 
						  CFG_STRING, cgCfgBuffer );
		if ( ilRc == RC_SUCCESS )
		{
			ilMaxDbgFiles = atoi ( cgCfgBuffer );
		}
		else
		{
			dbg(TRACE,"InitTowhdl: not found: <SYSTEM>, <MAXDBGFILES> in <%s>",
					   cgConfigFile);
			ilRc = RC_SUCCESS;
		}
		SetDbgLimits(llMaxDbgLines,ilMaxDbgFiles);
	}
	
	return(ilRc);
	
} /* end of initialize */



char *GetRef(char *pcpCfgBuffer,struct AlocRefStruct *pcpAlocBuffer)
{
	char *pclResult = NULL;
	char *pclTmp;
	char *pclNext;
	char *pclTable;
	char *pclField;
	char pclTmpBuf[124];
	int ilIndex = 0;


	pclTmp = strchr(pcpCfgBuffer,';');
	if(pclTmp != NULL)
	{
		*pclTmp = '\0';
		pclResult = pclTmp + 1;
	}
	strcpy(pclTmpBuf,pcpCfgBuffer);
	dbg(TRACE,"%05d:GetRef <%s>",__LINE__,pclTmpBuf);
	pclTmp = pclTmpBuf;
	{
		if((pclTable = strchr(pclTmp,',')) != NULL)
		{
			*pclTable = '\0';
			strcpy(pcpAlocBuffer->Name,pclTmp);
			pclTable++;
			pclNext = pclTable;
			do
			{
				pclTable = pclNext;
				pclNext = strchr(pclTable,',');
				if(pclNext != NULL)
				{
					pclNext++;
				}
				pclField = strchr(pclTable,'.');
				if (pclField != NULL)
				{
					*pclField = '\0';
					pclField++;
					strcpy(pcpAlocBuffer->Table[ilIndex],pclTable);
					pclTable = strchr(pclField,',');
					pclField[4] = '\0';
					if(pclTable != NULL)
					{
						*pclTable = '\0';
					}
					strcpy(pcpAlocBuffer->Field[ilIndex++],pclField);
				}
			} while(pclNext != NULL);
		}
	}
	return(pclResult);
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	int ilStart, i;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}
		else
		{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

	if(pcpTableName == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid parameter TableName: null pointer not valid");
		ilRc = RC_FAIL;
	}
	else
	{
		strcpy ( prpArrayInfo->crTableName,pcpTableName );
       	dbg(DEBUG,"SetArrayInfo: crTableName=<%s>",prpArrayInfo->crTableName );
    }
   
	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = 0;
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = 0; 
   	}

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,pcpTableName,NULL,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
		else
		{
			if (pcpAddFields != NULL)
			{
				ilStart = get_no_of_items(pcpArrayFieldList);
				for ( i=ilStart; i<prpArrayInfo->lrArrayFieldCnt; i++ )
				{
					prpArrayInfo->plrArrayFieldLen[i] = pcpAddFieldLens[i-ilStart];
				}
			}
		}
	}/* end of if */

	if(pcpArrayFieldList != NULL)
	{
		if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
		{
			strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
		}/* end of if */
	}/* end of if */
	if(pcpAddFields != NULL)
	{
		if(strlen(pcpAddFields) + 
			strlen(prpArrayInfo->crArrayFieldList) <= 
					(size_t)ARR_FLDLST_LEN)
		{
			strcat(prpArrayInfo->crArrayFieldList,",");
			strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
			dbg(TRACE,"FieldList <%s>",prpArrayInfo->crArrayFieldList);
		}/* end of if */
	}/* end of if */

	return(ilRc);
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	ReadConfigEntries ();
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(min(ipSleep,1));
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
#if 0
static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */
#endif

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) 
	{
		case	QUE_E_FUNC	:	/* Unknown function */
			dbg(TRACE,"<%d> : unknown function",pipErr);
			break;
		case	QUE_E_MEMORY	:	/* Malloc reports no memory */
			dbg(TRACE,"<%d> : malloc failed",pipErr);
			break;
		case	QUE_E_SEND	:	/* Error using msgsnd */
			dbg(TRACE,"<%d> : msgsnd failed",pipErr);
			break;
		case	QUE_E_GET	:	/* Error using msgrcv */
			dbg(TRACE,"<%d> : msgrcv failed",pipErr);
			break;
		case	QUE_E_EXISTS	:
			dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
			break;
		case	QUE_E_NOFIND	:
			dbg(TRACE,"<%d> : route not found ",pipErr);
			break;
		case	QUE_E_ACKUNEX	:
			dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
			break;
		case	QUE_E_STATUS	:
			dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
			break;
		case	QUE_E_INACTIVE	:
			dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
			break;
		case	QUE_E_MISACK	:
			dbg(TRACE,"<%d> : missing ack ",pipErr);
			break;
		case	QUE_E_NOQUEUES	:
			dbg(TRACE,"<%d> : queue does not exist",pipErr);
			break;
		case	QUE_E_RESP	:	/* No response on CREATE */
			dbg(TRACE,"<%d> : no response on create",pipErr);
			break;
		case	QUE_E_FULL	:
			dbg(TRACE,"<%d> : too many route destinations",pipErr);
			break;
		case	QUE_E_NOMSG	:	/* No message on queue */
			dbg(TRACE,"<%d> : no messages on queue",pipErr);
			break;
		case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
			dbg(TRACE,"<%d> : invalid originator=0",pipErr);
			break;
		case	QUE_E_NOINIT	:	/* Queues is not initialized*/
			dbg(TRACE,"<%d> : queues are not initialized",pipErr);
			break;
		case	QUE_E_ITOBIG	:
			dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
			break;
		case	QUE_E_BUFSIZ	:
			dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
			break;
		default			:	/* Unknown queue error */
			dbg(TRACE,"<%d> : unknown error",pipErr);
			break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					break;	
		
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					break;	
		
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					ilBreakOut = TRUE;
					break;	
	
				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					break;	
		
				case	HSB_DOWN	:
					/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
					ctrl_sta = prgEvent->command;
					Terminate(1);
					break;	
		
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					ResetDBCounter();
					ilBreakOut = TRUE;
					break;	
	
				case	REMOTE_DB :
					/* ctrl_sta is checked inside */
					HandleRemoteDB(prgEvent);
					break;
	
				case	SHUTDOWN	:
					Terminate(1);
					break;
							
				case	RESET		:
					ilRc = Reset();
					break;
							
				case	EVENT_DATA	:
					dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
						
				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;
	
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;
	
				default			:
					dbg(TRACE,"HandleQueues: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} /* end switch */
		} 
		else 
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
		ilRc = InitTowhdl();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitTowhdl: init failed!");
		} /* end of if */
	}/* end of if */

} /* end of HandleQueues */
	

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList, long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina, long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerstφrt */

	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
		case RC_SUCCESS :
			*plpLen = atoi(&clFele[0]);
			 dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);
			break;
	
		case RC_NOT_FOUND :
			dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s,%s>",
							ilRc,pcpTana,pcpFina);
			break;
	
		case RC_FAIL :
			dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
			break;
	
		default :
			dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
			break;
	}/* end of switch */

	return(ilRc);
	
} /* end of GetFieldLength */


static int SaveEventTimes(char *pcpInbound,char *pcpOutbound)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int ilLen;
	int ilTimeNo = 1;
	int ilItemNo = 1;
	char *pclTimeStr;
	int ilCol,ilPos;

	memset(InboundTimes,0,sizeof(InboundTimes));
	memset(OutboundTimes,0,sizeof(InboundTimes));

	FindItemInList(pcgFieldList,"URNO",',',&igUrnoItemNo,&ilCol,&ilPos);
	pclTimeStr = INBOUNDTIMES;

	dbg(DEBUG,"SaveEventTimes: UrnoItemNo %d",igUrnoItemNo);
	do
	{
		ilLen = GetNextDataItem(pcgTmpBuf,&pclTimeStr,&cgKomma[0],
							"","\0\0");
		if (ilLen > 0)
		{
			if ( bgUseFIDSTimes || strcmp(pcgTmpBuf, "ETOA") )
			{
				ilRc = FindItemInList(pcgFieldList,pcgTmpBuf,',',&ilItemNo,&ilCol,&ilPos);
				if (ilRc == RC_SUCCESS)
				{
					GetDataItem(InboundTimes[ilTimeNo],pcpInbound,ilItemNo,',',""," \0");
				}
			}
			if (!*InboundTimes[ilTimeNo] && ilTimeNo > 1)
			{
				strcpy(InboundTimes[ilTimeNo],InboundTimes[ilTimeNo-1]);
			}
			dbg(DEBUG,"%s %d <%s>",pcgTmpBuf,ilTimeNo,InboundTimes[ilTimeNo]);
			ilTimeNo++;
		}
	} while (ilLen > 0);


	ilTimeNo = 1;
	pclTimeStr = OUTBOUNDTIMES;
	do
	{
		ilLen = GetNextDataItem(pcgTmpBuf,&pclTimeStr,&cgKomma[0],
							"","\0\0");
		if (ilLen > 0)
		{
			if ( bgUseFIDSTimes || strcmp(pcgTmpBuf, "ETOD") )
			{
				ilRc = FindItemInList(pcgFieldList,pcgTmpBuf,',',&ilItemNo,&ilCol,&ilPos);
				if (ilRc == RC_SUCCESS)
				{
					GetDataItem(OutboundTimes[ilTimeNo],pcpOutbound,ilItemNo,',',""," \0");
				}
			}
			if (!*OutboundTimes[ilTimeNo] && ilTimeNo > 1)
			{
				strcpy(OutboundTimes[ilTimeNo],OutboundTimes[ilTimeNo-1]);
			}
			dbg(DEBUG,"%s %d <%s>",pcgTmpBuf,ilTimeNo,OutboundTimes[ilTimeNo]);
			ilTimeNo++;
		}
	} while (ilLen > 0);
	/*** save registration, sometimes we need it later ***/
	*cgRegn = '\0';
	if (FindItemInList(pcgFieldList,"REGN",',',&igRegnItemNo,&ilCol,&ilPos)
				== RC_SUCCESS)
	{
		GetDataItem(cgRegn,pcpInbound,igRegnItemNo,',',""," \0");
		if (*cgRegn == '\0')
		{
			GetDataItem(cgRegn,pcpOutbound,igRegnItemNo,',',""," \0");
		}
		dbg(DEBUG,"REGN=<%s>",cgRegn);
	}
	else
	{
		dbg(TRACE,"REGN not found in Fieldlist");
	}
	
	return(ilRc);
}

/******************************************************************************/
/******************************************************************************/
static int GetEventType(long *plpEventType,char **pcpInbound, char **pcpOutbound,char *pcpData)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	char clDel = '\n';
	char *pclTmp1 = NULL;
	
	if(ilRc == RC_SUCCESS)
	{
		pclTmp1 = strchr(pcpData,clDel);
		if(pclTmp1!= NULL)
		{
			*pclTmp1 = 0x00;
	
			*pcpInbound = strdup(pcpData);
			if(*pcpInbound == NULL)
			{
				dbg(TRACE,"GetEventType: strdup - calloc failed");
				ilRc = RC_FAIL;
			}/* end of if*/

			*pclTmp1 = clDel;
		}
		else
		{
			dbg(TRACE, "GetEventType: strchr <0x%x> not found in <%s>", clDel,
				pcpData);
			ilRc = RC_FAIL;
		}/* end of if*/
	}/* end of if*/

	
	if(ilRc == RC_SUCCESS)
	{
		pclTmp1++;
		*pcpOutbound = strdup(pclTmp1);
		if(*pcpOutbound == NULL)
		{
			dbg(TRACE,"GetEventType: strdup - calloc failed");
			ilRc = RC_FAIL;
		}/* end of if*/
	}/* end of if*/

	if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"GetEventType:  inbound <%x><%s>",pcpInbound,*pcpInbound);
		dbg(DEBUG,"GetEventType: outbound <%x><%s>",pcpOutbound,*pcpOutbound);

		*plpEventType = EVT_UNKNOWN;

		if(strlen(*pcpInbound) == 0)
		{
			if(strlen(*pcpOutbound) == 0)
			{
				dbg(TRACE,"GetEventType: inbound and outbound empty");
				*plpEventType = EVT_UNKNOWN;
				ilRc = RC_FAIL;
			}
			else
			{
				*plpEventType = EVT_OUTBOUND;
				ilRc = RC_SUCCESS;
			}/* end of if */
		}
		else
		{
			if(strlen(*pcpOutbound) == 0)
			{
				*plpEventType = EVT_INBOUND;
				ilRc = RC_SUCCESS;
			}
			else
			{
				*plpEventType = EVT_TURNAROUND;
				ilRc = RC_SUCCESS;
			}/* end of if */
		}/* end of if */
	}

	return(ilRc);
	
} /* end of GetEventType */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	dbg(DEBUG,"%05d: rc=%d",__LINE__,ilRc);
	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	dbg(DEBUG,"%05d: rc=%d",__LINE__,ilRc);
	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}
	else
	{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */
	int       ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRueUrnos     = NULL;
	char    *pclTmp = NULL;
	char	clSelection[512], clUrno[21];
	int		ilSBCMode = SINGLE_TO_BCHDL |SINGLE_TO_ACTION;
	int		ilLen, ilOldDebugLevel;
	BOOL    blDoBc = TRUE;
	BOOL 	blRuleFound = FALSE;
        
        BOOL 	blInsertTow = FALSE;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;
	pclRueUrnos  = pclData + strlen(pclData) + 1;

	if (strlen(pclRueUrnos) != 0)
	{
		blRuleFound = TRUE;
	}
	
	pcgFieldList = pclFields;

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
	if ( prgItem )
		dbg(TRACE, "item->priority <%d>", prgItem->priority ) ;

	/****************************************/
	if ( debug_level < DEBUG )
	{
		dbg(TRACE, "bchead->orig_name  (%s)", prlBchead->orig_name );
	}
	else
	{
		DebugPrintBchead(DEBUG,prlBchead);
	}
	
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	dbg(TRACE,"RueUrnos  <%s>",pclRueUrnos);
	/****************************************/

	bgShowEvent = FALSE;
	CheckPerformance (TRUE, prlBchead->orig_name);

	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	if(ilRc == RC_SUCCESS)
	{
		
		if (strstr(prlCmdblk->tw_start,".NBC.") != NULL)
		{
			dbg(DEBUG,"SILENT TRANSACTION (NO BROADCASTS)");
			ilSBCMode = NO_BC; /* no BC at all */
		}

		cgMinDebe[0] = '\0';
		cgMaxDeen[0] = '\0';
		pcgTemplates[0] = '\0';

		/* JHA20090623 */
		dbg(DEBUG,"HandleData: command <%s> found",prlCmdblk->command);
			
		switch (ilCmd)
		{
			case CMD_IFR :
			case CMD_UFR :
				/* Do not continue if no rule is defined */
/*				if ( blRuleFound == TRUE)
				{
*/
					if ( pclTmp = strchr(pclSelection,'|') )
					{	/* hag20020529: remove possible flag from rule diagnosis */
						*pclTmp = '\0';
					}
	
					
					ilRc = GetRueUrnos(pclRueUrnos);
	
					ilRc = GetEventType((long *)&lgEventType, &pcgInbound, &pcgOutbound, pclData);
					if(ilRc == RC_SUCCESS)
					{
						char pclOURI[24];
						char pclOURO[24];
	
						SaveEventTimes(pcgInbound,pcgOutbound);
						
						ilRc = ReadRudTab();
						
						GetDataItem(pclOURI,pcgInbound,igUrnoItemNo,',',""," \0");
						GetDataItem(pclOURO,pcgOutbound,igUrnoItemNo,',',""," \0");
						
						if (!strcmp(prlBchead->dest_name, "FLICOL"))
						{
							dbg(DEBUG,"ReadOldTowings with pclSelection %s",pclSelection);
							ReadOldTowings(pclOURI,pclOURO,pclSelection);
							if ( bgSendSBC )
							{	/* Event from Coverage and CFG-Flag set -> one BC for all demands */
								if ( ilSBCMode != NO_BC )
									ilSBCMode = TCP_SBC|SINGLE_TO_ACTION;	/* old UDP-SBC (RELDEM) at the end of calc., TCP-SBC (UPDDEM) after each flight */
							}
						}
						else
						{
							dbg(DEBUG,"ReadOldTowings without pclSelection");
							ReadOldTowings(pclOURI,pclOURO,NULL);
	
							if ( pcgInbound )
								blDoBc = ToBeBroadcasted ( InboundTimes[1] );
							else
								blDoBc = ToBeBroadcasted ( OutboundTimes[1] );
								
							if ( !blDoBc )		
							{
								dbg( TRACE, "HandleData: Flight outside the configured time frmame for broadcasts -> NO BC!" );
								ilSBCMode = NO_BC;
							}
							else if ( bgSendUpdDem )
							{	/* Event from Coverage and CFG-Flag set -> one BC for all demands */
								if ( ilSBCMode != NO_BC )
									ilSBCMode = TCP_SBC | UDP_SBC |SINGLE_TO_ACTION;	/* UDPDEM as UDP-SBC and TCP-SBC after each flight */
							}				
						}
						if(strstr(pclSelection,"RUTY=3") == NULL)
						{
							CreateTowings();
                                                        blInsertTow = TRUE;
						}
						CompareTowings();						
						SaveTowings(pclOURI,ilSBCMode);
						SaveFisu();
						/*SendRAC(pclOURI,blInsertTow);*/
					}
					else
					{
						dbg(TRACE,"HandleData: GetEventType failed <%d>",ilRc);
					}/* end of if */
/*					
				}
				else
				{
					dbg(DEBUG,"HandleData: No rule defined for command IFR / UFR");
				}
*/
				break; /* CMD_UFR */
			case CMD_DFR :
				break; /* CMD_DFR */
			case CMD_FIR :
				break; /* CMD_FIR  */
			case CMD_CFL :
				break; /* CMD_CFL */
			case CMD_UDR :
				break; /* CMD_UDR */
			case CMD_IDR :
				break; /* CMD_IDR */
			case CMD_DRT:
				break; /* CMD_DRT */
			case CMD_IRT:
			case CMD_URT:
				/* JHA 20090916 */
				ilRc = CEDAArrayEventUpdate( prgEvent ); 
				if( (ilRc==RC_SUCCESS) && (ilCmd!=CMD_DRT) )
				{	
					clUrno[0] = '\0';
					if ( strcmp(prlCmdblk->obj_name,"RUDTAB") == 0) 
						ilRc = tool_get_field_data (pclFields, pclData, "URNO", clUrno );
					else
						if ( !strcmp(prlCmdblk->obj_name,"RPFTAB") ||
							 !strcmp(prlCmdblk->obj_name,"RPQTAB") ||
							 !strcmp(prlCmdblk->obj_name,"REQTAB") ||
							 !strcmp(prlCmdblk->obj_name,"RLOTAB") )
							ilRc = tool_get_field_data (pclFields, pclData, "URUD", clUrno );
					if ( (ilRc == RC_SUCCESS) && !IS_EMPTY (clUrno) )
						ilRc = SetRudLogicalFields ( &rgCCIRudArray, clUrno );
					else
						dbg ( TRACE, "HandleData (IRT/URT): Could not find RUD-URNO in event" );
				}
				dbg(DEBUG, "command IRT, URT or DRT processed, table<%s> ilRC <%ld>", 
					prlCmdblk->obj_name, ilRc);
				break; /* CMD_IRT / CMD_URT */
			case CMD_ANSWER:
				break; /* CMD_ANSWER */
			case CMD_DLF:
				/* Get Data Load Format */				
				break; /* CMD_DLF */
			default:
				dbg(TRACE, "HandleData: unknown command <%s>",
					&prlCmdblk->command[0]);
				SendAnswer(prpEvent,RC_FAIL);
				break; /* default */

		}/* end of switch */
	
	}
	else
	{
		dbg(TRACE,"HandleData: GetCommand failed");
	}/* end of if */

	if(pcgInbound != NULL)
	{
		free(pcgInbound);
		pcgInbound = NULL;
	}/* end of if */

	if(pcgOutbound != NULL)
	{
		free(pcgOutbound);
		pcgOutbound = NULL;
	}/* end of if */

	CheckPerformance (FALSE, "\0");
	ilOldDebugLevel = debug_level;
	if (bgShowEvent == TRUE)
	{
		debug_level = TRACE;
		dbg(TRACE, "item->priority <%d>", prgItem->priority ) ;
		dbg(TRACE, "bchead->orig_name  (%s)", prlBchead->orig_name );
		DebugPrintCmdblk(TRACE,prlCmdblk);
		dbg(TRACE,"selection <%s>",pclSelection);
		dbg(TRACE,"fields    <%s>",pclFields);
		dbg(TRACE,"data      <%s>",pclData);
	}                             /* end if */

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
	debug_level = ilOldDebugLevel ;

	return(RC_SUCCESS);
	
} /* end of HandleData */

static int GetRueUrnos(char *pcpRueUrnos)
{

	int ilRc = RC_SUCCESS;
	int ilLC           = 0; /* Loop Count for inner loop */
	int ilTemplateNo   = 0;
	int ilRueLen;
	int ilIdx = 0;  /* actual index in prgRueUrnos */

	/* initialize RUE-Urnos */
	memset(prgRueUrnos,0,sizeof(prgRueUrnos));

	dbg(TRACE,"RueUrnos: <%s>",pcpRueUrnos);
	do
	{
		ilRueLen = GetNextDataItem(pcgTmpBuf,
					&pcpRueUrnos,&cgStx[0],"","\0\0");
		if (ilRueLen  > 0)
		{	
			char *pclTmpBuf = pcgTmpBuf;
			for (ilLC = IDX_INBOUND; ilLC <= IDX_OUTBOUND; ilLC++)
			{
				/* we've got RUE-Urnos for one template */
				char pclLocalBuf[128];
				int ilDL;
				{
					ilDL=GetNextDataItem(pclLocalBuf,&pclTmpBuf,
								&cgSoh[0],"","  \0\0");
					if (ilDL >= 1 )
					{
						prgRueUrnos[ilIdx].IsUsed = TRUE;
						strcpy(prgRueUrnos[ilIdx].Urno, pclLocalBuf);
						ilDL = GetNextDataItem(pclLocalBuf,
										&pclTmpBuf,&cgSoh[0],""," \0\0");
						strcpy(prgRueUrnos[ilIdx].Fisu,pclLocalBuf);
						ilIdx++;
					}
				}
			}
			ilTemplateNo++;
		}
	} while (ilRueLen > 0);

	for( ilTemplateNo = 0; ilTemplateNo < MAXRUES && 
							prgRueUrnos[ilTemplateNo].IsUsed ; ilTemplateNo++)
	{
		dbg(DEBUG,"%d. Rule URNO <%s> FISU <%s>", ilTemplateNo+1, 
			prgRueUrnos[ilTemplateNo].Urno, prgRueUrnos[ilTemplateNo].Fisu );
	}

	return ilRc;
}

static int ReadOldTowings(char *pcpOURI,char *pcpOURO, char *pcpTemplates)
{
	int 	ilRc = RC_SUCCESS;
	char 	pclTmpBuf[512];
	char 	pclAddData[256];
	long 	llAction = ARR_FIRST;
	int		ilNoOfItems = 0;
	char	clField[URNOLEN+1];
	int		i;

	*pcgTmpBuf = '\0';

	CEDAArrayDelete(&rgOldTowArray.rrArrayHandle,
					rgOldTowArray.crArrayName);
	dbg(DEBUG,"ReadOldTowings: old tow-array deleted ");

	dbg(DEBUG,"ReadOldTowings: OURI = '%s', OURO = '%s'",pcpOURI,pcpOURO);
	dbg(DEBUG,"ReadOldTowings: Selection = '%s'",pcpTemplates);
	
	sprintf(pcgTmpBuf,"WHERE RKEY='%s' AND FTYP IN ('T','G') AND USEC = 'TOWHDL' ORDER BY STOD",pcpOURI);
	dbg(DEBUG,"ReadOldTowings Selection: <%s>",pcgTmpBuf);

	ilRc = CEDAArrayRefill(&rgOldTowArray.rrArrayHandle,
						   rgOldTowArray.crArrayName, pcgTmpBuf, NULL,
						   ARR_FIRST);

	dbg(DEBUG,"ReadOldTowings: RC=%d",ilRc);
	return ilRc;
}

static int DeleteOldTowings()
{
	int 	ilRc = RC_SUCCESS, ilRc1;
	char 	*pclTowRow;
	long 	llRowNum = ARR_LAST;
	char 	clFlgs[24], clUrno[12], clUrud[12];

	dbg(DEBUG,"DeleteOldTowingss: Start" );
	while(CEDAArrayGetRowPointer(&rgOldTowArray.rrArrayHandle,
				rgOldTowArray.crArrayName,llRowNum,(void *)&pclTowRow) == RC_SUCCESS)
	{

		strcpy ( clUrno, OLDTOWFIELD(pclTowRow,igTowURNO) );
		dbg(DEBUG,"DeleteOldTowing: Urno %s, DELE <%s>",clUrno,
			OLDTOWFIELD(pclTowRow,igTowDELE));
		
		if( strncmp(OLDTOWFIELD(pclTowRow,igTowDELE),"0",1) != 0 && strncmp(OLDTOWFIELD(pclTowRow,igTowSAVE),"2",1) != 0)
		{
			llRowNum = ARR_CURRENT;
			ilRc1 = CEDAArrayDeleteRow(&rgOldTowArray.rrArrayHandle,
											rgOldTowArray.crArrayName,llRowNum);
			if ( ilRc1 != RC_SUCCESS )
			{
				int ilOldDbgLevel = debug_level;
				debug_level = TRACE;
				dbg ( TRACE, "DeleteOldTowings: CEDAArrayDeleteRow failed RC <%d> URNO <%s>",
					  ilRc, clUrno);
				debug_level = ilOldDbgLevel;
				ilRc = ilRc1;
			}
			else
			{
				dbg ( TRACE, "DelOld: URNO <%s> deleted", clUrno );
			}
		}
		else
		{
			dbg ( TRACE, "DelOldTowings: Keep URNO <%s> DELE <%s>", 
				  clUrno, OLDTOWFIELD(pclTowRow,igTowDELE) );
		}
		llRowNum = ARR_PREV;
	}

	dbg(DEBUG,"DeleteOldTowings RC=%d",ilRc);
	return ilRc;
}

static int SaveTowings(char *pcpRKey, int ipBcMode )
{

	int ilRc = RC_SUCCESS;
	char *pclRow, *pclData = 0;
	int	 i, ilModified, ilTouched;
	long llRowNum ,llBytes;
	char clTwEnd[33], clLists[11];
	BOOL blActive;

	dbg(DEBUG,"SaveTowings: Start ipBcMode <%d>", ipBcMode );
	if ( ipBcMode < 0 )
	{
		ipBcMode = NO_BC;
	}

        ilRc = SendBroadcast(pcpRKey);
        DeleteOldTowings();

        blActive = (ipBcMode&SINGLE_TO_BCHDL) ? TRUE : FALSE;
	CEDAArraySendChanges2BCHDL( &rgNewTowArray.rrArrayHandle,
								rgNewTowArray.crArrayName, blActive );
	CEDAArraySendChanges2BCHDL( &rgOldTowArray.rrArrayHandle,
								rgOldTowArray.crArrayName, blActive );

	blActive = (ipBcMode&SINGLE_TO_ACTION) ? TRUE : FALSE;
	CEDAArraySendChanges2ACTION( &rgNewTowArray.rrArrayHandle,
								 rgNewTowArray.crArrayName, blActive );
	CEDAArraySendChanges2ACTION( &rgOldTowArray.rrArrayHandle,
								 rgOldTowArray.crArrayName, blActive );
        
	if ( prgTowInf )
	{
		prgTowInf->AppendData = FALSE;
	}
	ilRc = AATArraySaveChanges(&rgOldTowArray.rrArrayHandle, rgOldTowArray.crArrayName, 
								&prgTowInf, ARR_COMMIT_ALL_OK);
	dbg ( TRACE, "SaveTowings: AATArraySaveChanges <TOWINGS> RC <%d>", ilRc );
	if ( prgTowInf )
	{
		prgTowInf->AppendData = TRUE;
	}
	ilRc = AATArraySaveChanges( &rgNewTowArray.rrArrayHandle, rgNewTowArray.crArrayName, 
								&prgTowInf, ARR_COMMIT_ALL_OK);
	dbg ( TRACE, "SaveTowings: AATArraySaveChanges <NEWTOWINGS> RC <%d>", ilRc );

	ilTouched = prgTowInf->DataList[IDX_IRTU].ValueCount + 
				prgTowInf->DataList[IDX_URTU].ValueCount +
				prgTowInf->DataList[IDX_DRTU].ValueCount; 


	if ( ilTouched <= 0)
	{
		dbg ( TRACE, "SaveTowings: No data changes !!!" );
	}
	
	if ( pclData )
	{
		free ( pclData );
	}

	dbg(DEBUG,"SaveTowings: finished RC <%d>", ilRc );
	return ilRc;
}

static int ReadOldFid(char *pcpDate, char *pcpTemplates)
{
	int		ilRc = RC_SUCCESS;
	int		ilNoOfItems = 0;
	int		i;
	char	pclDate[24], clHint[41];
	char	pclAddData[256];
	char	clField[URNOLEN+1];
	long llAction = ARR_FIRST;

	strcpy(pclAddData,"");
	strcpy(pclDate,pcpDate);
	AddSecondsToCEDATime(pclDate, 86400, 1);
 
	if (pcpTemplates != NULL)
		dbg(DEBUG, "ReadOldFid Templates: <%s>", pcpTemplates);

	/*   *clRudBuf = '\0';*//*	 cgDataArea instead of clRudBuf */
	*cgDataArea = '\0';
	clHint[0] = '\0';

	if((pcpTemplates != NULL) && (*pcpTemplates != '\0'))
	{
		*pcgTmpBuf = '\0';	/* has been clTemplates before */
		ilNoOfItems = get_no_of_items(pcpTemplates);
		for ( i = 0; i < ilNoOfItems; i++ )
		{
			if (i == 0)
			{
				strcat(pcgTmpBuf, "'");
			}
			else
			{
				strcat(pcgTmpBuf, ",'");
			}
			get_real_item(clField, pcpTemplates, i+1);
			strcat(pcgTmpBuf, clField);
			strcat(pcgTmpBuf, "'");
		}
		if ( igLoadSel <= 2 )
		{
			sprintf( cgDataArea, cgTplLoadExt[igLoadSel], pcgTmpBuf);
		}
		if ( igLoadSel < 2 )
		{
			strcpy ( clHint, "/*+ INDEX(RUDTAB RUDTAB_UTPL) */" );
		}
	}

	*pcgTmpBuf = '\0';	/* pcgTmpBuf instead of pclSelection */

	sprintf(pcgTmpBuf,
			"WHERE DETY IN (' ','7') AND (DEBE BETWEEN '%s' AND '%s')", pcpDate,
			pclDate);

	if (*cgDataArea != '\0')
	{
		strcat(pcgTmpBuf,cgDataArea);
	}

	dbg(DEBUG,"ReadOldFid RC=%d",ilRc);
	return ilRc;
}

static int ReadRudTab()
{
	int ilRc = RC_SUCCESS;
	int ilTemplateNo = 0;
	char clKey[21];
	long llAction = ARR_FIRST;
	long llRow = ARR_FIRST;
	int  ilRefillCnt = 0;
	char *pclResult=0;
	long llWorkRows = 0;

	dbg( DEBUG, "ReadRudTab: Start");

	CEDAArrayDelete(&rgRudArray.rrArrayHandle,
					rgRudArray.crArrayName);
	CEDAArrayDisactivateIndex ( &(rgRudArray.rrArrayHandle),	
								rgRudArray.crArrayName,
								&(rgRudArray.rrIdx01Handle),
								rgRudArray.crIdx01Name );
	dbg( DEBUG, "ReadRudTab: CEDAArray cleared");
	for( ilTemplateNo = 0; ilTemplateNo < MAXRUES && 
							prgRueUrnos[ilTemplateNo].IsUsed; ilTemplateNo++)
	{	
		if ( *prgRueUrnos[ilTemplateNo].Urno &&
			 (atol(prgRueUrnos[ilTemplateNo].Urno)>0) )
		{
			if ( (ilTemplateNo == 0 ) ||  
				strcmp(prgRueUrnos[ilTemplateNo-1].Urno,
						   prgRueUrnos[ilTemplateNo].Urno ) )
			{	/* if same URNO has been inserted before (Turnaround rule) -> skip */
				sprintf ( clKey, "%s,1,0", prgRueUrnos[ilTemplateNo].Urno );
				llRow = ARR_FIRST;

				while ( CEDAArrayFindRowPointer(&rgCCIRudArray.rrArrayHandle,
					 							rgCCIRudArray.crArrayName,
												&rgCCIRudArray.rrIdx02Handle,
												rgCCIRudArray.crIdx02Name,clKey,
												&llRow,(void**)&pclResult) == RC_SUCCESS )
				{
					AddRowToRudArray ( llRow );
					llRow = ARR_NEXT;
				}

				dbg( DEBUG, "ReadRudTab: %d. rule added <%s>", ilTemplateNo+1,
					  prgRueUrnos[ilTemplateNo].Urno  );
			}
		}
	}
	CEDAArrayActivateIndex (&(rgRudArray.rrArrayHandle),	
							rgRudArray.crArrayName,
							&(rgRudArray.rrIdx01Handle),
							rgRudArray.crIdx01Name );
	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
							rgRudArray.crArrayName,&llWorkRows);
	dbg( TRACE, "ReadRudTab: End RudCount=%ld",llWorkRows);
	/*lgUsedRudUrnosLength = (llWorkRows*15)+100;
	pcgUsedRudUrnos = realloc(pcgUsedRudUrnos,lgUsedRudUrnosLength);
	memset(pcgUsedRudUrnos,0,lgUsedRudUrnosLength);*/
	if (llWorkRows == 0)
	{
		ilRc = RC_NODATA;
	}
	
	return ilRc;
}

static int UpdateTowing(ARRAYINFO *prpDest,long lpRowNum, ARRAYINFO *prpSource)
{
	int ilRc = RC_SUCCESS;
	char pclNow[DATELEN+1], clAloc[21], clFlgs[24];
	long llFieldNo;
	BOOL blModified = FALSE;

	dbg(DEBUG, "UpdateTowning: start RowNum %ld" ,lpRowNum);

	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowFKEY),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFKEY)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"FKEY",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFKEY));
		blModified = TRUE;
	}

/*
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowURNO),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowURNO)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"URNO",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowURNO));
		blModified = TRUE;
	}
*/
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowRKEY),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowRKEY)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"RKEY",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowRKEY));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowAURN),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowAURN)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"AURN",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowAURN));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowCSGN),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowCSGN)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"CSGN",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowCSGN));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowFLNO),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFLNO)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"FLNO",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFLNO));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowALC2),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowALC2)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ALC2",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowALC2));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowALC3),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowALC3)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ALC3",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowALC3));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowORG3),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowORG3)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ORG3",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowORG3));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowDES3),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowDES3)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DES3",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowDES3));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowORG4),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowORG4)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ORG4",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowORG4));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowDES4),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowDES4)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"DES4",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowDES4));
		blModified = TRUE;
	}
	
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowFLTN),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFLTN)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"FLTN",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFLTN));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowACT3),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowACT3)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ACT3",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowACT3));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowACT5),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowACT5)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ACT5",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowACT5));
		blModified = TRUE;
	}

	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowADID),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowADID)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ADID",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowADID));
		blModified = TRUE;
	}

	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowSTOA),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowSTOA)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"STOA",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowSTOA));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowSTOD),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowSTOD)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"STOD",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowSTOD));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowFTYP),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFTYP)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"FTYP",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowFTYP));
		blModified = TRUE;
	}
	
/* JHA20090811*/
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowPSTA),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowPSTA)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"PSTA",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowPSTA));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowPSTD),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowPSTD)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"PSTD",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowPSTD));
		blModified = TRUE;
	}
/*end JHA20080911*/
	

/*
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowOFBL),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowOFBL)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"OFBL",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowOFBL));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowONBL),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowONBL)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"ONBL",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowONBL));
		blModified = TRUE;
	}
	
*/
	
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowTIFA),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTIFA)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TIFA",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTIFA));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowTIFD),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTIFD)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TIFD",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTIFD));
		blModified = TRUE;
	}

	/* new request from 09.12.2008 RST */
/*	
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowTISA),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTISA)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TISA",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTISA));
		blModified = TRUE;
	}
	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowTISD),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTISD)))
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"TISD",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowTISD));
		blModified = TRUE;
	}
*/

	if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowREM1),
		NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowREM1)) != 0 )
	{
		llFieldNo = -1;

		CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
			&llFieldNo,"REM1",lpRowNum,
				NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowREM1));
		blModified = TRUE;
	}
	
	if ( blModified )
	{
            dbg(TRACE, "UpdateTowning: TOW URNO <%s> changed ",OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowURNO) );
            llFieldNo = -1;
            CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,&llFieldNo,"DELE",lpRowNum,"0");
            llFieldNo = -1;
            CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,&llFieldNo,"SAVE",lpRowNum,"1");
/*		
 * 
		if (strcmp(OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowUSEU),
			NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowUSEU)))
		{
*/
			llFieldNo = -1;
			GetServerTimeStamp("UTC", 1, 0, pclNow);
			CEDAArrayPutField(&prpDest->rrArrayHandle, prpDest->crArrayName,
							  &llFieldNo, "LSTU", lpRowNum, pclNow);
		
			llFieldNo = -1;
/*
			CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
				&llFieldNo,"USEU",lpRowNum,
					NEWTOWFIELD(prpSource->pcrArrayRowBuf,igTowUSEU));
*/
			CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,
				&llFieldNo,"USEU",lpRowNum,"TOWHDL");
                        
                         llFieldNo = -1;
/*
		}
*/
	} else {
            llFieldNo = -1;
            CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,&llFieldNo,"DELE",lpRowNum,"0");
            llFieldNo = -1;
            CEDAArrayPutField(&prpDest->rrArrayHandle,prpDest->crArrayName,&llFieldNo,"SAVE",lpRowNum,"0");
             dbg ( TRACE, "UpdateTowning: TOW URNO <%s> unchanged ",OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowURNO) );
            
        }
/*	
	else 
		dbg ( DEBUG, "UpdateDemand: demand URUD <%s> unchanged ", 
			 OLDTOWFIELD(prpDest->pcrArrayRowBuf,igTowURUD) );
*/			 
	return ilRc;
}

static int CreateFid(char *pcpDate)
{

	int ilRc = RC_SUCCESS;
	long llRowNum = ARR_FIRST;
	char *pclRudRow = NULL;
	char pclTime1[32];
	char pclTime2[32];
	char pclDate[32];
	char pclTmpStr[32];

	/* first delete all old demands **/
	
	dbg(DEBUG,"CreateFid");

	strcpy(pclDate,pcpDate);
	UtcToLocal(pclDate);
	pclDate[8] = '\0';

	/************ empty FISU list *******************/
	/*memset(pcgUsedRudUrnos,0,lgUsedRudUrnosLength); */


	dbg(DEBUG,"DB DEDU DEBE DEEN DRTY  TSDB TSDE SUTI SDTI TTGT TTGF");
	while(CEDAArrayGetRowPointer(&rgRudArray.rrArrayHandle,
				rgRudArray.crArrayName,llRowNum,(void *)&pclRudRow) == RC_SUCCESS)
	{
		llRowNum = ARR_NEXT;

		dbg(DEBUG,"%-s  %-6.6s %-14.14s %-14.14s <%-1.1s> %-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s",

					RUDFIELD(pclRudRow,igRudDBAR),
					RUDFIELD(pclRudRow,igRudDEDU),
					RUDFIELD(pclRudRow,igRudDEBE),
					RUDFIELD(pclRudRow,igRudDEEN),
					RUDFIELD(pclRudRow,igRudDRTY),
					RUDFIELD(pclRudRow,igRudTSDB),
					RUDFIELD(pclRudRow,igRudTSDE),
					RUDFIELD(pclRudRow,igRudSUTI),
					RUDFIELD(pclRudRow,igRudSDTI),
					RUDFIELD(pclRudRow,igRudRTDB),
					RUDFIELD(pclRudRow,igRudRTDE),
					RUDFIELD(pclRudRow,igRudTTGT),
					RUDFIELD(pclRudRow,igRudTTGF));


		if(strncmp(RUDFIELD(pclRudRow,igRudDBFL),"1",1) == 0)
		{
			/************ calculate demand begin ****/
			sprintf(pclTime1,"%s%s",pclDate,(char *)RUDFIELD(pclRudRow,igRudEADB)+8);
			SunCompStrAddTime(pclTime1,RUDFIELD(pclRudRow,igRudDEDU),pclTime2);
			/*********** + duty duration = duty end ******************/
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudSUTI));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** setup time substracted **********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudTTGT));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** time to go to time substracted ****************/ 
			LocalToUtc(pclTime1);
			/********* now calculate duty end, saved in pclTime2 ******/
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudTTGF),pclTime2);
			/*********** time to go to time added *********************/ 
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudSDTI),pclTime2);
			/*********** setdown time added **********************/ 
			LocalToUtc(pclTime2);
		}
		else if(strncmp(RUDFIELD(pclRudRow,igRudDEFL),"1",1) == 0)
		{
			/************ calculate demand end ****/
			sprintf(pclTime1,"%s%s",pclDate,(char *)RUDFIELD(pclRudRow,igRudLADE)+8);
			SunCompStrAddTime(pclTime1,RUDFIELD(pclRudRow,igRudTTGF),pclTime1);
			/*********** time to go from time added *********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudDEDU));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** - duty duration = duty begin ******************/
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudSUTI));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** setup time subtracted **********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudTTGT));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** time to go to time subtracted ****************/ 
			LocalToUtc(pclTime1);
			/********* now calculate duty end, saved in pclTime2 ******/
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudSDTI),pclTime2);
			/*********** setdown time added **********************/ 
			LocalToUtc(pclTime2);
		}
		else
		{
			/************ calculate demand begin ****/
			sprintf(pclTime1,"%s%s",pclDate,(char *)RUDFIELD(pclRudRow,igRudDEBE)+8);
			SunCompStrAddTime(pclTime1,RUDFIELD(pclRudRow,igRudDEDU),pclTime2);
			/*********** + duty duration = duty end ******************/
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudSUTI));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** setup time substracted **********************/ 
			sprintf(pclTmpStr,"-%s",RUDFIELD(pclRudRow,igRudTTGT));
			SunCompStrAddTime(pclTime1,pclTmpStr,pclTime1);
			/*********** time to go to time substracted ****************/ 
			LocalToUtc(pclTime1);
			/********* now calculate duty end, saved in pclTime2 ******/
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudTTGF),pclTime2);
			/*********** time to go to time added *********************/ 
			SunCompStrAddTime(pclTime2,RUDFIELD(pclRudRow,igRudSDTI),pclTime2);
			/*********** setdown time added **********************/ 
			LocalToUtc(pclTime2);
		}
	}
	return ilRc;
}

static int InitializeNewTowing(char *pcpNewTowingBuf)
{
		int ilRc = RC_SUCCESS;
		char pclUrno[32]="0";
		char pclNow[DATELEN+1];

		/* GetNextValues(pclUrno,1); */
		GetNextUrno(pclUrno);
		GetServerTimeStamp("UTC", 1, 0, pclNow);

		memset(pcpNewTowingBuf,0,rgNewTowArray.lrArrayRowLen);
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowFKEY)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowURNO),pclUrno);
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowRKEY)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowAURN)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowCSGN)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowFLNO)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowALC2)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowALC3)," ");

		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowORG3),cgHopo3);
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowDES3),cgHopo3);
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowORG4),cgHopo4);
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowDES4),cgHopo4);
		
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowFLTN)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowFLNS)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowACT3)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowACT5)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowREGN)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowADID),"B");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowSTOA)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowSTOD)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowFTYP),"T");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowPSTA)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowPSTD)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowOFBL)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowONBL)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowTIFA)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowTIFD)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowTISA)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowTISD)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowREM1)," ");
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowCDAT),pclNow);
		strcpy(NEWTOWFIELD(pcpNewTowingBuf,igTowUSEC),"TOWHDL");
		
		return ilRc;
}

/*  Adds a new demand from pcgNewDemandBuf to rgNewDemArray */
static int AddNewTowingRow ()
{
	int		i, ilRc;
	char	*pclTmpBuf;
	long	llAction = ARR_FIRST;
	char	clField[11];

	*pcgTmpBuf = '\0';
	pclTmpBuf = pcgTmpBuf;
	for(i = 1; i <= rgNewTowArray.lrArrayFieldCnt; i++)
	{
		*clField = '\0';
		GetDataItem(clField,rgNewTowArray.crArrayFieldList,i,',',"","\0\0");
		strcpy(pclTmpBuf,NEWTOWFIELD(pcgNewTowingBuf,i)); 
		pclTmpBuf += strlen(pclTmpBuf)+1;
	}
	ilRc = CEDAArrayAddRow(&rgNewTowArray.rrArrayHandle,
									rgNewTowArray.crArrayName,
									&llAction, (void *)pcgTmpBuf);
	if ( ilRc != RC_SUCCESS )
		dbg ( DEBUG, "AddNewTowRow: RC <%ld>", ilRc );
	return ilRc;
}

static int CreateTowings()
{

	int 	ilRc = RC_SUCCESS;
	int 	ilItemNo = 0;
	int 	ilCol,ilPos;
	long 	llRowNum = ARR_FIRST;
	char 	*pclRudRow = NULL;
	char    *pclAftRow = NULL;
	char    *pclRueRow = NULL;
	char 	pclOURI[32];
	char 	pclOURO[32];
	char 	pclTime1[32];
	char 	pclTime2[32];
	char 	pclTmpStr[32];
	long	llWorkRows;
	long	llWorkRowsOld;
	long	llCurrent;
	long	llDiff;
	long	llMinDedu;
	long 	llAction = ARR_FIRST;
	time_t	tlDebe;
	time_t	tlDeen;
	char   	clAlidsIn[201]="";
	char   	clAlidsOut[201]="";
	char   	clKey[21], *ps1, *ps2;
	long	llFieldNo = -1;

	char 	pclAddData[256];

	/* first delete all old towings **/
	
	dbg(DEBUG,"Create Towings");

	CEDAArrayDelete(&rgNewTowArray.rrArrayHandle,
					rgNewTowArray.crArrayName);

	dbg(DEBUG,"old new tow-array deleted ");

	/************ empty FISU list *******************/

	GetDataItem(pclOURI,pcgInbound,igUrnoItemNo,',',""," \0");
	GetDataItem(pclOURO,pcgOutbound,igUrnoItemNo,',',""," \0");

	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
							rgRudArray.crArrayName,&llWorkRows);
	llWorkRowsOld = llWorkRows + 1; /*that the condition is true at the begin*/

	dbg(DEBUG,"InboundUrno OURI = %s, OutboundUrno OURO = %s",pclOURI,pclOURO);
	dbg(DEBUG,"-------------------------------------------------------------");

	dbg(DEBUG,"DB DEDU DRTY  TSDB TSDE SUTI SDTI RTDB RTDE TTGT TTGF");

	while ((llWorkRows < llWorkRowsOld) && (llWorkRows > 0))
	{
		llRowNum = ARR_FIRST;
		llWorkRowsOld = llWorkRows;

		while(CEDAArrayGetRowPointer(&rgRudArray.rrArrayHandle,
										rgRudArray.crArrayName, llRowNum,
										(void *)&pclRudRow) == RC_SUCCESS)
		{
			llRowNum = ARR_NEXT;
			
			if (strncmp(RUDFIELD(pclRudRow,igRudFLAG),"1",1) != 0)
			{
				dbg(DEBUG,
					"%-s  %-6.6s <%-1.1s> %-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s%-5.5s",
					RUDFIELD(pclRudRow,igRudDBAR),
					RUDFIELD(pclRudRow,igRudDEDU),
					RUDFIELD(pclRudRow,igRudDRTY),
					RUDFIELD(pclRudRow,igRudTSDB),
					RUDFIELD(pclRudRow,igRudTSDE),
					RUDFIELD(pclRudRow,igRudSUTI),
					RUDFIELD(pclRudRow,igRudSDTI),
					RUDFIELD(pclRudRow,igRudRTDB),
					RUDFIELD(pclRudRow,igRudRTDE),
					RUDFIELD(pclRudRow,igRudTTGT),
					RUDFIELD(pclRudRow,igRudTTGF));

				dbg(DEBUG, "CreateTowings: Next->InitializeNewTowing");	
				InitializeNewTowing(pcgNewTowingBuf);

				CEDAArrayDelete(&rgAftArray.rrArrayHandle,
								rgAftArray.crArrayName);
				if (strcmp(RUDFIELD(pclRudRow,igRudDRTY), "1") == 0 )
				{
					/* Inbound */
					sprintf(pcgTmpBuf,"WHERE URNO='%s'",pclOURI);
					dbg(DEBUG, "CreateTowings: Inbound <%s>",pcgTmpBuf );	
					
				}
				else
				{
					/* Outbound */
					sprintf(pcgTmpBuf,"WHERE URNO='%s'",pclOURO);
					dbg(DEBUG, "CreateTowings: Outbound <%s>",pcgTmpBuf );	
				}
				
				CEDAArrayRefill(&rgAftArray.rrArrayHandle,
								rgAftArray.crArrayName, pcgTmpBuf, pclAddData,
								llAction);
				
				if (CEDAArrayGetRowPointer(&rgAftArray.rrArrayHandle,
											rgAftArray.crArrayName,
											ARR_FIRST,
											(void *)&pclAftRow) == RC_SUCCESS)
				{
					dbg(DEBUG, "CreateTowings: Flightnumber = %s", AFTFIELD(pclAftRow,igAftFLNO));
				}

				/* Get Rulename for remark */
				CEDAArrayDelete(&rgRueArray.rrArrayHandle,
								rgRueArray.crArrayName);
				sprintf(pcgTmpBuf,"WHERE URNO='%s'",RUDFIELD(pclRudRow,igRudURUE));

				CEDAArrayRefill(&rgRueArray.rrArrayHandle,
								rgRueArray.crArrayName, pcgTmpBuf, pclAddData,
								llAction);
				
				if (CEDAArrayGetRowPointer(&rgRueArray.rrArrayHandle,
											rgRueArray.crArrayName,
											ARR_FIRST,
											(void *)&pclRueRow) == RC_SUCCESS)
				{
					dbg(DEBUG, "CreateTowings: Rule = %s", RUEFIELD(pclRueRow,igRueRUNA));
				}
				
				/************* fill towing items *********************/
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowFKEY),AFTFIELD(pclAftRow,igAftFKEY));	
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowRKEY),AFTFIELD(pclAftRow,igAftRKEY));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowAURN),AFTFIELD(pclAftRow,igAftAURN));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowCSGN),AFTFIELD(pclAftRow,igAftCSGN));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowFLNO),AFTFIELD(pclAftRow,igAftFLNO));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowALC2),AFTFIELD(pclAftRow,igAftALC2));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowALC3),AFTFIELD(pclAftRow,igAftALC3));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowFLTN),AFTFIELD(pclAftRow,igAftFLTN));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowFLNS),AFTFIELD(pclAftRow,igAftFLNS));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowACT3),AFTFIELD(pclAftRow,igAftACT3));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowACT5),AFTFIELD(pclAftRow,igAftACT5));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowREGN),AFTFIELD(pclAftRow,igAftREGN));
				
				/* JHA20090623 */
				/* in case of inbound copy always URNO to AURN */
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowAURN),AFTFIELD(pclAftRow,igAftRKEY));

				dbg(DEBUG, "CreateTowings: Urno = %s", RUEFIELD(pclRueRow,igTowURNO));
				
				if (strcmp(RUDFIELD(pclRudRow,igRudDRTY), "1") == 0 )
				{
					/* Arrival (inbound) */
					/* SunCompStrAddTime(AFTFIELD(pclAftRow,igAftSTOA),RUDFIELD(pclRudRow,igRudTSDB),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOD));*/
                                        SunCompStrAddTime(AFTFIELD(pclAftRow,igAftTIFA),RUDFIELD(pclRudRow,igRudTSDB),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOD));
					SunCompStrAddTime(NEWTOWFIELD(pcgNewTowingBuf,igTowSTOD),RUDFIELD(pclRudRow,igRudDEDU),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOA));
					
					/* JHA20090810 */
					strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowPSTD),AFTFIELD(pclAftRow,igAftPSTA));
					dbg(DEBUG,"CreateTowings: PSTD = %s", NEWTOWFIELD(pcgNewTowingBuf,igTowPSTD));
					dbg(DEBUG,"CreateTowings: Inbound rule found");
				}
				else
				{
					/* Departure (outbound) */
					/* SunCompStrAddTime(AFTFIELD(pclAftRow,igAftSTOD),RUDFIELD(pclRudRow,igRudTSDE),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOA)); */
                                        SunCompStrAddTime(AFTFIELD(pclAftRow,igAftTIFD),RUDFIELD(pclRudRow,igRudTSDE),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOA));
					SunCompStrAddTime(NEWTOWFIELD(pcgNewTowingBuf,igTowSTOA),RUDFIELD(pclRudRow,igRudDEDU),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOD));

					sprintf(pclTime1,"-%s",RUDFIELD(pclRudRow,igRudDEDU));
					SunCompStrAddTime(NEWTOWFIELD(pcgNewTowingBuf,igTowSTOA),pclTime1,NEWTOWFIELD(pcgNewTowingBuf,igTowSTOD));
					
					strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowPSTA),AFTFIELD(pclAftRow,igAftPSTD));
					dbg(DEBUG,"CreateTowings: PSTA = %s", NEWTOWFIELD(pcgNewTowingBuf,igTowPSTA));
					dbg(DEBUG,"CreateTowings: Outbound rule found");
				}
				/* JHA 20090824 do not copy on / ofblock from original flight record
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowOFBL),AFTFIELD(pclAftRow,igAftOFBL));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowONBL),AFTFIELD(pclAftRow,igAftONBL));
				 end JHA 20090824*/
				 
				/* new requirenment from 08.12.2008 RST corrected 17.12.2008*/
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowTIFA),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOA));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowTIFD),NEWTOWFIELD(pcgNewTowingBuf,igTowSTOD));
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowTISA),"S");
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowTISD),"S");
				
				strcpy(NEWTOWFIELD(pcgNewTowingBuf,igTowREM1),RUEFIELD(pclRueRow,igRueRUNA));

				ilRc = AddNewTowingRow ();
			}
		}
	}

	CEDAArrayGetRowCount(&rgRudArray.rrArrayHandle,
						rgRudArray.crArrayName,&llWorkRowsOld);
	CEDAArrayGetRowCount(&rgNewTowArray.rrArrayHandle,
						rgNewTowArray.crArrayName,&llWorkRows);

	dbg(DEBUG,"CreateTowings: created <%ld> towings for <%ld> RUDs", llWorkRows, llWorkRowsOld );
	
	if ( llWorkRowsOld != llWorkRows ) /*that the condition is true at the begin*/
	{
		dbg(TRACE,"CreateTowings: created <%ld> towings for <%ld> RUDs", llWorkRows, llWorkRowsOld );
	}
	else
	{
		dbg(DEBUG,"CreateTowings finished");
	}
	return ilRc;
}


ShowSaveDebugOnly(int ipLine,long lpRowNum)
{
	char *pclTowRow;

	if (CEDAArrayGetRowPointer(&rgOldTowArray.rrArrayHandle,
				rgOldTowArray.crArrayName,lpRowNum,(void *)&pclTowRow) == RC_SUCCESS)
	{
		dbg(DEBUG,"%05d: Field %d RowNum %ld SAVE <%s>, URNO %s",ipLine,
			igTowSAVE,lpRowNum,
			OLDTOWFIELD(pclTowRow,igTowSAVE),
			OLDTOWFIELD(pclTowRow,igTowURNO));
	}
	else
	{
		dbg(DEBUG,"Error retrieving Old Tow Row");
	}
}

static int CompareTowings()
{

	int ilRc = RC_SUCCESS, ilRc1;
	long llNewTowRowNum;
	long llOldTowRowNum;
	
	char pclNewTowUrud[24];
	char pclOldTowUrud[24];
	char pclJfnd[24];
	char pclFlgs[24];
	char pclPsta[20];
	char pclPstd[20];
	
	long llRowCount1, llRowCount2;
	long llCurrent = ARR_CURRENT;
	long llFieldJFND = -1;
	long llFieldSAVE = -1;
	long llFieldFLGS = -1;
	BOOL blPsta;
	BOOL blPstd;
        long llFieldNo;

	dbg(DEBUG,"CompareTowings");
	
	/* PRF6094: Workaround, since the problem in AATArrays is not solved	*/
	/* Destroy index before following loop and rebuild it after loop		*/
	ilRc = CEDAArrayDestroyIndex( &rgOldTowArray.rrArrayHandle,
								   rgOldTowArray.crArrayName,
								   &rgOldTowArray.rrIdx01Handle,
							       rgOldTowArray.crIdx01Name );

	dbg ( DEBUG, "CompareTowings: CEDAArrayDestroyIndex old towings RC <%d>", ilRc );

	ilRc1 = CEDAArrayDestroyIndex( &rgNewTowArray.rrArrayHandle,
								   rgNewTowArray.crArrayName,
								   &rgNewTowArray.rrIdx01Handle,
							       rgNewTowArray.crIdx01Name );

	dbg ( DEBUG, "CompareTowings: CEDAArrayDestroyIndex new towings RC <%d>", ilRc1 );

	blPsta = FALSE;
	blPstd = FALSE;
	
	llNewTowRowNum = ARR_FIRST;
	llOldTowRowNum = ARR_FIRST;
	
	CEDAArrayGetRowCount(&rgOldTowArray.rrArrayHandle,rgOldTowArray.crArrayName,&llRowCount1);
	CEDAArrayGetRowCount(&rgNewTowArray.rrArrayHandle,rgNewTowArray.crArrayName,&llRowCount2);

	dbg(TRACE,"CompareTowings: %ld old towings, %ld new towings", llRowCount1, llRowCount2 );

	/* PRF6094: 2nd part of workaround	*/
	ilRc = CreateIdx1( &rgOldTowArray, "IDXOTOW", "STOD", "A" );
	dbg ( DEBUG, "CompareTowings: OldTowIdx1 RC <%d>", ilRc );
	
	ilRc1 = CreateIdx1( &rgNewTowArray, "IDXNTOW", "STOD", "A" );
	dbg ( DEBUG, "CompareTowings: NewTowIdx1 RC <%d>", ilRc1 );

	/* Compare rules
	   both Arrays OldTowArray and NewTowArray are sorted in identical order (here STOD).
	   1. Old array empty new array empty -> do nothing
	   2. Old array empty new array more than one item -> insert
       3. Old array not empty new array empty -> delete all old entries (delete towings)
	   4. Old array not empty new array not empty -> update record by record in old array and delete new record
	   5. Old array has more entries than new array -> update record by record and delete rest
	   6. Old array has entry but less than new array -> update record by record and insert new ones
	   
	   Masterloop is newArray
	*/
	
	while(CEDAArrayFindRowPointer(&rgNewTowArray.rrArrayHandle,
                    rgNewTowArray.crArrayName,
                    &rgNewTowArray.rrIdx01Handle,
                    rgNewTowArray.crIdx01Name, NULL,
                    &llNewTowRowNum,
                    (void *)&rgNewTowArray.pcrArrayRowBuf) == RC_SUCCESS)

	{
		llNewTowRowNum = ARR_NEXT;
		
		dbg(DEBUG,"CompareTowings: NewTow URNO <%s>", 
			OLDTOWFIELD(rgNewTowArray.pcrArrayRowBuf,igTowURNO));
		
		if (CEDAArrayFindRowPointer(&rgOldTowArray.rrArrayHandle,
                    rgOldTowArray.crArrayName,
                    &rgOldTowArray.rrIdx01Handle,
                    rgOldTowArray.crIdx01Name, NULL,
                    &llOldTowRowNum,
                    (void *)&rgOldTowArray.pcrArrayRowBuf) == RC_SUCCESS)
		{

			dbg(DEBUG,"CompareTowings: OldTow URNO <%s>", 
				OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowURNO));
			

/*
			strcpy(pclOnblock,OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowONBL));
			TrimRight(pclOnblock);
			
			strcpy(pclOfblock,OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowOFBL));
			TrimRight(pclOfblock);
*/

                        strcpy(pclPsta,OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowPSTA));
			TrimRight(pclPsta);
			
			strcpy(pclPstd,OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowPSTD));
			TrimRight(pclPstd);
                        
			
			if (strlen(pclPsta) > 0 )
			{
				dbg(DEBUG,"CompareTowings: igTowPSTA <%s> URNO <%s>", 
					OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowPSTA),
					OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowURNO));
				blPsta = TRUE;
			}
			else
			{
				blPsta = FALSE;
			}

			if (strlen(pclPstd) > 0 )
			{
				dbg(DEBUG,"CompareTowings: igTowPSTD <%s> URNO <%s>", 
					OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowPSTD),
					OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowURNO));
				blPstd = TRUE;
			}
			else
			{
				blPstd = FALSE;
			}
			
			dbg(DEBUG,"CompareTowings: Update follows OLD = %s, NEW = %s",
				OLDTOWFIELD(rgOldTowArray.pcrArrayRowBuf,igTowURNO),
				NEWTOWFIELD(rgNewTowArray.pcrArrayRowBuf,igTowURNO));
			

                        
                        

                        
                        if (blPsta == FALSE && blPstd == FALSE )
			{
				dbg(DEBUG,"CompareTowings: No onblock set -> Update towing");
				UpdateTowing(&rgOldTowArray, llOldTowRowNum, &rgNewTowArray);
				
			} 
                        strcpy(NEWTOWFIELD(rgNewTowArray.pcrArrayRowBuf,igTowDELE),"1");
                        llFieldNo = -1;
                        CEDAArrayPutField(&rgOldTowArray.rrArrayHandle,rgOldTowArray.crArrayName,&llFieldNo,"SAVE",llOldTowRowNum,"2");
                        
			llOldTowRowNum = ARR_NEXT;
		}
	}


	llNewTowRowNum = ARR_LAST;
	while(CEDAArrayGetRowPointer(&rgNewTowArray.rrArrayHandle,
								 rgNewTowArray.crArrayName,llNewTowRowNum,
								 (void *)&rgNewTowArray.pcrArrayRowBuf) == RC_SUCCESS)
	{
		char *pclDelete;

		pclDelete = NEWTOWFIELD(rgNewTowArray.pcrArrayRowBuf,igTowDELE);

		dbg(DEBUG,"CompareTowings: Delete follows NEW = %s <%s>",
			NEWTOWFIELD(rgNewTowArray.pcrArrayRowBuf,igTowURNO),
			NEWTOWFIELD(rgNewTowArray.pcrArrayRowBuf,igTowDELE));
			
		if (*pclDelete == '1')
		{	
			ilRc1 = CEDAArrayDeleteRow(&rgNewTowArray.rrArrayHandle,
								   rgNewTowArray.crArrayName,ARR_CURRENT);
			dbg(DEBUG,"CompareTowings: Delete flag set. New row deleted" );
		}
		llNewTowRowNum = ARR_PREV;
	}	
        
        
	dbg(DEBUG,"CompareTowings finished");

	return ilRc;
}


static int SaveFisu()
{

	int ilRc = RC_SUCCESS;
	/* char pclSelection[124]; */
	int ilTemplateNo = 0;
	BOOL blSet = FALSE;

	for( ilTemplateNo = 0; ilTemplateNo < MAXRUES && 
							prgRueUrnos[ilTemplateNo].IsUsed ; ilTemplateNo++)
	{
		if ((*prgRueUrnos[ilTemplateNo].Fisu != '1') &&
			(atol(prgRueUrnos[ilTemplateNo].Urno) != 0))
		{
			blSet |= SetFisuIfNecessary( prgRueUrnos[ilTemplateNo].Urno );
		}
	}
	if ( blSet )
	{
		ilRc = CEDAArrayWriteDB(&rgRueArray.rrArrayHandle, rgRueArray.crArrayName,
								NULL,NULL,NULL,NULL,ARR_COMMIT_ALL_OK);
		if ( ilRc == RC_SUCCESS )
			dbg(DEBUG,"SaveFisu: CEDAArrayWriteDB succeeded!" );
		else
			dbg(TRACE,"SaveFisu: CEDAArrayWriteDB failed RC<%d>", ilRc );
	}
	return ilRc;
}

static int SendBroadcast(char *pcpRKey) {
    long llTowRowNum;
    char pclFldUrno[20];
    char pclFldSave[5];
    char pclFldDele[5];
    char *pclTowRow = NULL;
    char pclUpdSel[50];
    BOOL blsendRac = FALSE;

    dbg(DEBUG, "SendBroadcast Start");
    llTowRowNum = ARR_FIRST;
    while (CEDAArrayGetRowPointer(&rgNewTowArray.rrArrayHandle, rgNewTowArray.crArrayName, llTowRowNum, (void *) &pclTowRow) ==
            RC_SUCCESS) {
        blsendRac = TRUE;
/*
        BuildItemBuffer(pclTowRow, "", rgNewTowArray.lrArrayFieldCnt, ",");
        get_real_item(pclFldUrno, pclTowRow, igTowURNO);
        sprintf(pclUpdSel, "WHERE URNO=%s", pclFldUrno);
        (void) tools_send_info_flag(1900, 0, "TOWHDL", "", "TOWHDL", "", "", "0", "0", "RAC", "AFTTAB", pclUpdSel, rgNewTowArray.crArrayFieldList, pclTowRow, 0);
        dbg(DEBUG, "SendBroadcast rgNewTowArray ArrayRowBuf IFR Fields %s", pclUpdSel);
        delton(pclTowRow);
*/
        llTowRowNum = ARR_NEXT;
    }
    if (blsendRac==TRUE) {
        SendRAC(pcpRKey);
    }
    

    llTowRowNum = ARR_FIRST;
    while (CEDAArrayGetRowPointer(&rgOldTowArray.rrArrayHandle, rgOldTowArray.crArrayName, llTowRowNum, (void *) &pclTowRow) ==
            RC_SUCCESS) {
        BuildItemBuffer(pclTowRow, "", rgOldTowArray.lrArrayFieldCnt, ",");
        get_real_item(pclFldUrno, pclTowRow, igTowURNO);
        get_real_item(pclFldSave, pclTowRow, igTowSAVE);
        get_real_item(pclFldDele, pclTowRow, igTowDELE);
        sprintf(pclUpdSel, "WHERE URNO=%s", pclFldUrno);
        if (strcmp(pclFldSave, "1") == 0) {            
            (void) tools_send_info_flag(1900, 0, "TOWHDL", "", "TOWHDL", "", "", "0", "0", "UFR", "AFTTAB", pclUpdSel, rgOldTowArray.crArrayFieldList, pclTowRow, 0);
            dbg(DEBUG, "SendBroadcast rgOldTowArray ArrayRowBuf UFR Fields %s", pclUpdSel);
        } else if (strcmp(pclFldDele, " ") != 0 && strcmp(pclFldSave, "2") != 0) {
            (void) tools_send_info_flag(1900, 0, "TOWHDL", "", "TOWHDL", "", "", "0", "0", "DFR", "AFTTAB", pclFldUrno, "", pclFldUrno, 0);
            dbg(DEBUG, "SendBroadcast rgOldTowArray ArrayRowBuf DFR Fields %s", pclUpdSel);

        }else {
            dbg(DEBUG, "SendBroadcast rgOldTowArray UFR (%s)  Will not be send  Save (%s) , Dele (%s) ", pclFldUrno, pclFldSave,pclFldDele);
        }
        delton(pclTowRow);
        llTowRowNum = ARR_NEXT;
    }
    dbg(DEBUG, "SendBroadcast End");
    return TRUE;

}

static int	SendRAC(char *pcpRKey)
{
  char	pclFldMin[20];
  char	pclFldMax[20];
  char	pclSqlBuf[128];
  char	pclFldData[250];
  char	pclFld[150];
  char  pclFldDataTmp[250];
  char  pclUpdSel[50];
 
  short slLocalCursor = 0;
  short slFkt = START;
  
  int 	ilRc = RC_SUCCESS;
  int 	ilGetRc = RC_SUCCESS;
  
  pclFldData[0] = 0x00;

  
  
  strcpy (pclFld, "MIN(TIFA),MAX(TIFD)");
  
  /*sprintf (pclSqlBuf, "SELECT MIN(TIFA),MAX(TIFD) FROM AFTTAB WHERE RKEY=%s ", pcpRKey);   */
  sprintf (pclUpdSel, "WHERE RKEY=%s ", pcpRKey);
  sprintf (pclSqlBuf, "SELECT %s FROM AFTTAB %s",pclFld, pclUpdSel);
  
  dbg (TRACE, "SendRAC: <%s>", pclSqlBuf);
  
  
  sprintf(pclFldDataTmp,",");
  slLocalCursor = 0;
  slFkt = START;
   
    while ((ilGetRc = sql_if (slFkt, &slLocalCursor, pclSqlBuf, pclFldData)) == DB_SUCCESS)
  {

    /* JHA20090623 PRF 9363 */
    BuildItemBuffer (pclFldData, "", 2, ",");
    strcat(pclFldDataTmp,pclFldData);   
    
    
    dbg (TRACE, "SendRAC pclFldData: <%s>", pclFldData);
    dbg (TRACE, "SendRAC pclFldDataTmp: <%s>", pclFldDataTmp);
/*
    get_real_item (pclFldMin, pclFldData, 1);
    get_real_item (pclFldMax, pclFldData, 2);
    */

    (void) tools_send_info_flag (1900, 0, "TOWHDL", "TOWHDL", "TOWHDL", "", "", "0", "0", "RAC", "AFTTAB", pclFldDataTmp, "RKEY", pcpRKey, 0);
    slFkt = NEXT;
  }                             /* end while */
  close_my_cursor (&slLocalCursor);

  return ilRc;	
}

static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];

	dbg(0,"StrToTime, <%s>",pcpTime);
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */

	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm -> tm_year = atoi(_tmpc)-1900;
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
		/*now +=  + 3600;*/
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}

static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                               
	struct tm *_tm;
	
	_tm = (struct tm *)localtime(&lpTime);
	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
			_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
			_tm->tm_min,_tm->tm_sec);
	return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';                          
}                                                                               


/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */


void	SunCompStrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{
	strcpy(pcpDest, pcpBase);
	AddSecondsToCEDATime(pcpDest,atoi(pcpOffs),1);
	return ;
}


static int SendToSqlHdl(char *pcpObject,char *pcpSelection,
			char *pcpFields,char *pcpData)
{
	int			ilRC;
	BC_HEAD		rlBCHead;
	CMDBLK		rlCmdblk;

	dbg(DEBUG,"<SCAFC> ----- START -----");

	/* set BC-Head members */
	memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
	rlBCHead.rc = RC_SUCCESS;
	strncpy(rlBCHead.dest_name, cgProcessName, 10);
	strncpy(rlBCHead.dest_name, "TOWHDL", 10);
	strncpy(rlBCHead.recv_name, "TOWHDL", 10);

	/* set CMDBLK members */
	memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
	strncpy(rlCmdblk.command, "URT", 6);
	strncpy(rlCmdblk.obj_name, pcpObject, 10);
	sprintf(rlCmdblk.tw_end, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);

	/* write request to QUE */
	if ((ilRC = SendToQue(igRouter, PRIORITY_4, &rlBCHead, &rlCmdblk,
			pcpSelection,pcpFields,pcpData)) != RC_SUCCESS)
	{
		dbg(TRACE,"%05d SendToQue returns <%d>", __LINE__, ilRC);
		dbg(DEBUG," ----- END -----");
		return ilRC;
	}
		
	dbg(DEBUG,"<SCAFC> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
						CMDBLK *prpCmdblk, char *pcpSelection, 
						char *pcpFields, char *pcpData)
{
	int			ilRC;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

	dbg(DEBUG,"<SendToQue> ----- START -----");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
			 strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 20; 

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
		dbg(DEBUG,"<SendToQue> ----- END -----");
		return RC_FAIL;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
	prlOutEvent->type 		 = SYS_EVENT;
	prlOutEvent->command 	 = EVENT_DATA;
	prlOutEvent->originator  = mod_id;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

	/* Selection */
	strcpy(prlOutCmdblk->data, pcpSelection);

	/* fields */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

	/* data */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

	/* send this message */
	dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
	if ((ilRC = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRC);
		return RC_FAIL;
	}

	/* delete memory */
	free((void*)prlOutEvent);

	dbg(DEBUG,"<SendToQue> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

static int SendAnswer(EVENT *prpEvent,int ipRc)
{
	int	      ilRc           = RC_SUCCESS;			/* Return code */

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	prlBchead->rc = ipRc;

	dbg(DEBUG,"SendAnswer to: %d",prpEvent->originator);
	if ((ilRc=SendToQue(prpEvent->originator,
				PRIORITY_4, prlBchead, prlCmdblk,
						pclSelection,pclFields,pclData)) != RC_SUCCESS)
	{
		dbg(DEBUG,"%05d SendToQue returns <%d>", __LINE__, ilRc);
		dbg(DEBUG," ----- END -----");
	}
	else
	{
		dbg(DEBUG,"%05d SendToQue (%d) returns <%d>", __LINE__,
					prpEvent->originator,ilRc);
	}

	return ilRc;
}

static void TrimRight(char *s)
{    /* search for last non-space character */
    int i = 0;
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--);
	s[++i] = '\0'; /* trim off right spaces */    
}

static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
	int		ilRC = RC_SUCCESS;
	long	llRow = 0;
	FILE	*prlFile = NULL;
	char	clDel = ',';
	char	clFile[512];
	char	*pclTrimBuf = NULL;
	long	llRowCount;
	int		ilCount = 0;
	char	*pclTestBuf = NULL;


	dbg(TRACE,"SaveIndexInfo: Array name <%s>",prpArrayInfo->crArrayName);

	if (ilRC == RC_SUCCESS)
	{
		sprintf(&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,
				prpArrayInfo->crArrayName);

		errno = 0;
		prlFile = fopen (&clFile[0], "w");
		if (prlFile == NULL)
		{
		    dbg(TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0],
				errno, strerror(errno));
			ilRC = RC_FAIL;
 		}
	}

	if (ilRC == RC_SUCCESS)
  	{
		dbg(DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName);
		dbg(DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList);
		dbg(DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList);

 		fprintf(prlFile,"ArrayName      <%s>\n",prpArrayInfo->crArrayName);
		fflush(prlFile);
		fprintf(prlFile,"ArrayFieldList <%s>\n",prpArrayInfo->crArrayFieldList);
		fflush(prlFile);
		fprintf(prlFile,"Idx01FieldList <%s>\n",prpArrayInfo->crIdx01FieldList);
		fflush(prlFile);

		dbg(DEBUG,"IndexData"); 
		fprintf(prlFile,"IndexData\n"); fflush(prlFile);

		dbg(DEBUG,"FindKey <%s>",prpArrayInfo->pcrIdx01RowBuf);
		*prpArrayInfo->pcrIdx01RowBuf = '\0';
		if ( prpArrayInfo->rrIdx01Handle >= 0 )
		{
			llRow = ARR_FIRST;
			do
   			{
				ilRC = CEDAArrayFindKey(&(prpArrayInfo->rrArrayHandle),
										&(prpArrayInfo->crArrayName[0]),
										&(prpArrayInfo->rrIdx01Handle),
										prpArrayInfo->crIdx01Name, &llRow,
										prpArrayInfo->lrIdx01RowLen,
										prpArrayInfo->pcrIdx01RowBuf);
				if (ilRC == RC_SUCCESS)
				{
    
   					dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,
						prpArrayInfo->pcrIdx01RowBuf);

		    		pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf);
		     		if (pclTrimBuf != NULL)
					{
						ilRC = GetDataItem(pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf,
											1, clDel, "", "  ");
		      			if (ilRC > 0)
		      			{
							ilRC = RC_SUCCESS;
							fprintf(prlFile,"Key            <%ld><%s>\n",llRow,
									pclTrimBuf);
							fflush(prlFile);
		     			}

						free (pclTrimBuf);
		   				pclTrimBuf = NULL;
		    		}

 		   			llRow = ARR_NEXT;
				}
				else
    			{
   					dbg(TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRC);
				}
			} while (ilRC == RC_SUCCESS) ;
		}
		else
		{
			dbg(DEBUG,"rrIdx01Handle not initialized");
		}
		dbg(DEBUG,"ArrayData");
		fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

  		llRow = ARR_FIRST;

		CEDAArrayGetRowCount(&(prpArrayInfo->rrArrayHandle),
								&(prpArrayInfo->crArrayName[0]),&llRowCount);

		llRow = ARR_FIRST;
		dbg(DEBUG,"Array  <%s> data follows Rows %ld",prpArrayInfo->crArrayName,
			llRowCount);
		do
		{
			ilRC = CEDAArrayGetRowPointer(&(prpArrayInfo->rrArrayHandle),
			&(prpArrayInfo->crArrayName[0]),llRow,(void *)&pclTestBuf);
			if (ilRC == RC_SUCCESS)
			{
				for( ilCount = 0; ilCount < prpArrayInfo->lrArrayFieldCnt;
					 ilCount++)
				{
					fprintf(prlFile,"<%s>",
						&pclTestBuf[prpArrayInfo->plrArrayFieldOfs[ilCount]]); 
					fflush(prlFile);
				}
				fprintf(prlFile,"\n"); 
				dbg(DEBUG,"\n");
				fflush(prlFile);
				llRow = ARR_NEXT;
			}
			else
			{
				dbg(DEBUG,"GetRowPointer failed RC=%d",ilRC);
			}
		} while (ilRC == RC_SUCCESS);

		if (ilRC == RC_NOTFOUND)
		{
			ilRC = RC_SUCCESS;
		}
	}

	if (prlFile != NULL)
	{
		fclose(prlFile);
		prlFile = NULL;
		dbg(DEBUG, "SaveIndexInfo: event saved - file <%s>", &clFile[0]);
	}

	return ilRC;
}

static int CreateIdx1 ( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
						char *pcpFields, char *pcpOrdering )
{
	int		ilRC = RC_FAIL, ilLoop;
	char clOrderTxt[4];
	long	*pllOrdering=0;

	if ( !pcpFields )
	{
		return ilRC;		/*  0-Pointer in parameter pcpFields */
	}

	strcpy ( prpArrayInfo->crIdx01Name, pcpIdxName );
	strcpy ( prpArrayInfo->crIdx01FieldList, pcpFields );

	ilRC = GetTotalRowLength( pcpFields, prpArrayInfo, &(prpArrayInfo->lrIdx01RowLen) )	;
    
	if(ilRC != RC_SUCCESS)
	{
        dbg(TRACE,"CreateIdx1: GetTotalRowLength failed <%s> <%s>",
			prpArrayInfo->crTableName, pcpFields);
	}
	else
    {
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,
									   prpArrayInfo->lrIdx01RowLen+1);
   		if( prpArrayInfo->pcrIdx01RowBuf == NULL )
   		{
      		ilRC = RC_FAIL;
      		dbg(TRACE,"CreateIdx1: calloc failed");
   		}
   	}

	if(ilRC == RC_SUCCESS)
	{
		prpArrayInfo->lrIdx01FieldCnt = get_no_of_items(pcpFields);
		pllOrdering = (long *)calloc(prpArrayInfo->lrIdx01FieldCnt,
										sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"SetArrayInfo: pllOrdering calloc(%d,%d) failed",
				prpArrayInfo->lrIdx01FieldCnt,sizeof(long));
			ilRC = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx01FieldCnt; ilLoop++)
			{
				if (!pcpOrdering ||
					 ( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1) <= 0 ) )
				{
					clOrderTxt[0] = 'D';
				}
				if ( clOrderTxt[0] == 'A' )
				{
					pllOrdering[ilLoop] = ARR_ASC;
				}
				else
				{
					pllOrdering[ilLoop] = ARR_DESC;
				}
			}
		}
	}

	ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName,
									  &(prpArrayInfo->rrIdx01Handle), 
									  prpArrayInfo->crIdx01Name,
									  prpArrayInfo->crIdx01FieldList, 
									  pllOrdering );
	if ( pllOrdering )
	{
		free (pllOrdering);
	}
	pllOrdering = 0;
	return ilRC;									
}

static int CreateIdx2( ARRAYINFO *prpArrayInfo, char *pcpIdxName,
						char *pcpFields, char *pcpOrdering )
{
	int		ilRC = RC_FAIL, ilLoop;
	char	clOrderTxt[4];
	long	*pllOrdering=0;

	if ( !pcpFields )
		return ilRC;		/*  0-Pointer in parameter pcpFields */

	strcpy ( prpArrayInfo->crIdx02Name, pcpIdxName );
	strcpy ( prpArrayInfo->crIdx02FieldList, pcpFields );
	
	ilRC = GetTotalRowLength( pcpFields, prpArrayInfo, &(prpArrayInfo->lrIdx02RowLen) )	;
    
	if(ilRC != RC_SUCCESS)
	{
        dbg(TRACE,"CreateIdx2 GetRowLength failed <%s> <%s>",
			prpArrayInfo->crTableName, pcpFields);
	}
	else
    {
		prpArrayInfo->pcrIdx02RowBuf = (char *) calloc(1,
									   prpArrayInfo->lrIdx02RowLen+1);
   		if( prpArrayInfo->pcrIdx02RowBuf == NULL )
   		{
      		ilRC = RC_FAIL;
      		dbg(TRACE,"CreateIdx2: calloc failed");
   		}
   	}

	if(ilRC == RC_SUCCESS)
	{
		prpArrayInfo->lrIdx02FieldCnt = get_no_of_items(pcpFields);
		pllOrdering = (long *)calloc(prpArrayInfo->lrIdx02FieldCnt,
										sizeof(long));
		if(!pllOrdering )
		{
			dbg(TRACE,"SetArrayInfo: pllOrdering calloc(%d,%d) failed",
				prpArrayInfo->lrIdx02FieldCnt,sizeof(long));
			ilRC = RC_FAIL;
		}
		else
		{
			for(ilLoop=0; ilLoop < prpArrayInfo->lrIdx02FieldCnt; ilLoop++)
			{
				if ( !pcpOrdering ||
					( get_real_item(clOrderTxt,pcpOrdering,ilLoop+1) <=0) )
				{
					clOrderTxt[0] = 'D';
				}
				if ( clOrderTxt[0] == 'A' )
				{
					pllOrdering[ilLoop] = ARR_ASC;
				}
				else
				{
					pllOrdering[ilLoop] = ARR_DESC;
				}
			}
		}
	}

	ilRC = CEDAArrayCreateMultiIndex( &(prpArrayInfo->rrArrayHandle), 
									  prpArrayInfo->crArrayName,
									  &(prpArrayInfo->rrIdx02Handle), 
									  prpArrayInfo->crIdx02Name,
									  prpArrayInfo->crIdx02FieldList, 
									  pllOrdering );
	if ( pllOrdering )
	{
		free (pllOrdering);
	}
	pllOrdering = 0;
	return ilRC;									
}

static int LocalToUtc(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clLocal[32];

	strcpy(clLocal,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

	AddSecondsToCEDATime(pcpTime,(time_t)0-atol(pclTdi),1);
	dbg(DEBUG,"2UTC  : <%s> ",pcpTime);

	return ilRc;
}

static int UtcToLocal(char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char *pclTdi;
	char clUtc[32];

	strcpy(clUtc,pcpTime);
	pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;

	AddSecondsToCEDATime(pcpTime,(time_t)0+atol(pclTdi),1);
	dbg(DEBUG,"2LOCAL  : <%s> ",pcpTime);

	return ilRc;
}

#if 0
static int SetFisu(char *pcpRudResult)
{/*not detailly tested*/
	int ilRc = RC_SUCCESS;
 	short	slCursor;
	short	slSqlFunc;
	char	pclSelection[124];
	char	pclSqlBuf[128];
	char	pclDataArea[256];

	slSqlFunc = START;
	slCursor = 0;

	sprintf(pclSqlBuf, "SELECT FISU FROM RUETAB WHERE URNO='%s'",
			RUDFIELD(pcpRudResult, igRudURUE));

	ilRc = sql_if(slSqlFunc, &slCursor, pclSqlBuf, pclDataArea);
	close_my_cursor(&slCursor);

	if ((ilRc == RC_SUCCESS) && (strncmp(pclDataArea, "1", 1) != 0))
	{
		sprintf(pclSelection, "WHERE URNO='%s'",
				RUDFIELD(pcpRudResult, igRudURUE));

		ilRc = SendToSqlHdl("RUETAB", pclSelection, "FISU", "1");
	}
	return ilRc;
}
#endif

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
		{
			*pipMode = DEBUG;
		}
		else if (!strcmp(clCfgValue, "TRACE"))
		{
			*pipMode = TRACE;
		}
		else
		{
			*pipMode = 0;
		}
	}

	return ilRC;
}

/********************************************************************/
static int GetNextUrno(char *pcpUrno)
{
	int ilRC = RC_SUCCESS ;
	char  pclDataArea[IDATA_AREA_SIZE] ;
	
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
		memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
		if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
		{
			dbg( TRACE,"GetNextValues failed RC <%d>", ilRC );
		}
		else
		{
			strcpy ( pcpUrno, pclDataArea );
			igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
			lgActUrno = atol(pcpUrno);
			dbg( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
				  URNOS_TO_FETCH, pcpUrno ) ;
		}
	}
	else
	{
		igReservedUrnoCnt-- ;
		lgActUrno++ ;
		sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
		dbg( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
			  pcpUrno, igReservedUrnoCnt ) ;
	}
	
	return (ilRC) ;
} /* getNextUrno () */

static BOOL SetFisuIfNecessary(char *pcpRueUrno)
{
	char	clRueUrno[24];
	int		ilFound, ilFisu = 0, ilRc = RC_SUCCESS;
	char	*pclResult = 0;
	long	llRow = ARR_FIRST;
	/* char	clSelection[124]; */
	static long	llFieldNo = -1;
	BOOL	blSet = FALSE;
		
	strcpy( clRueUrno, pcpRueUrno );
	TrimRight ( clRueUrno );
	if ( ilFisu == 1 )
	{
		dbg( TRACE, "SetFisuIfNecessary: Fisu already set for rule <%s>", clRueUrno );
	}
	else
	{
		if ( ilFound != RC_SUCCESS )
		{
			llRow = ARR_LAST;
			dbg( DEBUG, "SetFisuIfNecessary: CEDAArrayAddRow returned <%d>", ilRc );
		}
		if ( ilRc == RC_SUCCESS)
		{
			dbg( DEBUG, "SetFisuIfNecessary: Setting FISU=1 for rule <%s> RC <%d>", 
				  clRueUrno, ilRc );
			blSet = ( ilRc == RC_SUCCESS );
		}
	}
	return blSet;
}

static int DoSendSBC ( char *pcpStart, char *pcpEnd, char *pcpTpl, 
					   char *pcpDety, char *pcpWks )
{
	char	*pclData;
	int		ilRC = RC_SUCCESS, ilLen;
	char	clTwEnd[33];


	if ( !pcpStart || !pcpEnd || !pcpTpl || !pcpDety || !pcpWks )
		return RC_FAIL;

	ilLen = strlen(pcpStart) + strlen(pcpEnd) + strlen(pcpTpl) + 24;
	pclData = malloc( ilLen );

	if ( !pclData )
	{
		ilRC = RC_FAIL;
	}

	strncpy ( pclData, pcpStart, 14 );
	pclData[14]='\0';
	strcat ( pclData, "-" );
	ilLen = strlen ( pclData );
	strncpy ( &(pclData[ilLen]), pcpEnd, 14 );
	pclData[14+ilLen]='\0';
	strcat ( pclData, pcpTpl );
	strcat ( pclData, "\n{=NOTCP=}" );
	
	dbg( TRACE, "DoSendSBC: Data <%s>", pclData );
	sprintf (clTwEnd, "%s,%s,%s", cgHopo,cgTabEnd, cgProcessName);
	ilRC = tools_send_info_flag( 1900,0, "BCHDL", "TOWHDL", pcpWks, "", 
								 "", "", clTwEnd, "SBC","RELDEM",pcpDety,
								 "DEBE,DEEN|UTPL",pclData,0);          
	if ( pclData )
	{
		free ( pclData );
	}

	dbg( TRACE, "DoSendSBC: RC <%d>", ilRC );
	return ilRC;
}


static int AddRowToRudArray ( long lpRow )
{
	int ilRC;
	long llRowWrite = ARR_LAST;
	char *pclRudRow=0;

	dbg( DEBUG, "AddRowToRudArray: Start Row <%ld>", lpRow );


	ilRC = CEDAArrayGetRowPointer(&rgCCIRudArray.rrArrayHandle, rgCCIRudArray.crArrayName, 
			lpRow, (void*)&pclRudRow );

	if ( ilRC == RC_SUCCESS )
	{
		ilRC = CEDAArrayAddRow (&rgRudArray.rrArrayHandle, rgRudArray.crArrayName, 
								&llRowWrite, (void*)pclRudRow );
		if ( ilRC != RC_SUCCESS )
		{
			dbg( TRACE, "AddRowToRudArray: CEDAArrayAddRow failed" );
		}
	}
	else
	{
		dbg( TRACE, "AddRowToRudArray: CEDAArrayGetRowPointer failed for row <%d>", lpRow );
	}

	return ilRC;
}

/* *******************************************************/
static void CheckPerformance (int ipStart, char *pcpRtTime)
{
	time_t tlQueTime, tlStampDiff, tlNowTime;
	int ilOldDbgLevel = debug_level;

	if (tgMaxStampDiff > 0)
	{
		if (ipStart == TRUE)
		{
			tgBeginStamp = time (0L);
			tgQueDiff = 0;
			if (pcpRtTime && (pcpRtTime[0] >= '0') && (pcpRtTime[0] <= '9'))
			{
				tlNowTime = tgBeginStamp % 10000;
				tlQueTime = atol (pcpRtTime);
				tgQueDiff = tlNowTime - tlQueTime;
				if (tgQueDiff > tgMaxStampDiff)
				{
					if ( debug_level < TRACE )
					{
						debug_level = TRACE;
						bgShowEvent = TRUE;
					}
					dbg(TRACE, "===> PERF_CHECK: EVENT QUEUE_TIME (%d SEC) <===", tgQueDiff);
				}                       
			}                         
		}  
		else
		{
			tgEndStamp = time (0L);
			tlStampDiff = tgEndStamp - tgBeginStamp;
			if ( tlStampDiff > tgMaxStampDiff )
			{		
				if ( debug_level < TRACE )
				{
					debug_level = TRACE;
					bgShowEvent = TRUE;
				}
				dbg(TRACE, "===> PERF_CHECK: TOWHDL  EVENT PROCESSING (%d SEC) <===", tlStampDiff);
			}
			else
				dbg(TRACE, "----- TOWHDL  EVENT PROCESSING (%d SEC)", tlStampDiff);
			if ((tgQueDiff > 0) && (tlStampDiff > 0))
			{
				tlStampDiff += tgQueDiff;
			    if (tlStampDiff > tgMaxStampDiff)
			    {
					if ( debug_level < TRACE )
					{
						debug_level = TRACE;
						bgShowEvent = TRUE;
					}
					dbg(TRACE, "===> PERF_CHECK: TOTAL QUEUE_TIME (%d SEC) <===", tlStampDiff);
			    }
				else
					dbg(TRACE, "----- TOTAL QUEUE_TIME (%d SEC)", tlStampDiff);
			}    
		}      
		debug_level = ilOldDbgLevel;
	}        
	return;
}                               /* end CheckPerformance */

static int GetTotalRowLength( char *pcpFieldList, ARRAYINFO *prpArray, long *plpLen)
{
    int	 ilRc        = RC_SUCCESS;			/* Return code */
    int  ilNoOfItems = 0;
    int  ilItemNo = 0;
    int  ilLoop      = 0;
    long llRowLen    = 0;
    long llFldLen    = 0;
    char clFina[8];
	
    if (prpArray != NULL && pcpFieldList != NULL)
    {
		ilNoOfItems = get_no_of_items(pcpFieldList);
			
		ilLoop = 1;
		do
		{
			get_real_item(clFina,pcpFieldList,ilLoop);

			ilItemNo = get_item_no(prpArray->crArrayFieldList, clFina, 5);
			/*if(GetItemNo(clFina,prpArray->crArrayFieldList,&ilItemNo) == RC_SUCCESS) */
			if (ilItemNo >= 0)
			{
				llRowLen++;
				llRowLen += prpArray->plrArrayFieldLen[ilItemNo];
				dbg(DEBUG,"GetTotalRowLength: clFina <%s> Length <%ld>",clFina,prpArray->plrArrayFieldLen[ilItemNo]);
			}
			else
			{
				ilRc == RC_FAIL;
			}
			ilLoop++;
		} while(ilLoop <= ilNoOfItems);
	}
	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	dbg(DEBUG,"GetTotalRowLength:  FieldList <%s> Length <%ld>",pcpFieldList,*plpLen);
	return(ilRc);
	
} /* end of GetTotalRowLength */

	
static int ReadConfigEntries ()
{
	int ilRc, ilValue;

	ilRc = iGetConfigRow ( cgConfigFile, "MAIN", "LoadFormat", CFG_STRING, cgCfgBuffer );
	if ( ilRc == RC_SUCCESS )
	{
		if ( strstr ( cgCfgBuffer, "RUD" ) )
		{
			igLoadSel = 1;
		}
		else if ( strstr ( cgCfgBuffer, "DEM" ) )
		{
			igLoadSel = 2;
		}
	}
	dbg(TRACE,"ReadConfigEntries: Using format Nr. %d to load old demands", igLoadSel );
	lgBCDays = -1;
	if ( ( iGetConfigRow ( cgConfigFile, "MAIN", "BroadCastDays", CFG_STRING, cgCfgBuffer ) == RC_SUCCESS ) &&
		  ( sscanf ( cgCfgBuffer, "%d", &ilValue ) > 0 ) )
	{
		dbg(TRACE,"ReadConfigEntries: Broadcasting within %d days", ilValue );
		lgBCDays = ilValue * 24 * 3600;
	}
	else
	{
		dbg(TRACE,"ReadConfigEntries: No time frame for broadcasting defined !" );
	}

	return RC_SUCCESS;
}

/******************************************************************************/
static int GetCedaTimestamp(char *pcpResultBuf,int ipResultLen, long lpTimeOffset)
{
	int         ilRC      = RC_SUCCESS ;      /* Return code */
	int         ilTimeLen = 0 ;
	time_t      llCurTime = 0 ;
	struct tm  *prlLocalTimeResult ;
	struct tm   rlTimeStruct ;
	char        clTimeStamp[16];

	if (ilRC == RC_SUCCESS)
	{
		if (pcpResultBuf == NULL)
		{
			dbg(TRACE, "GetCedaTimestamp: Result <%s>",clTimeStamp) ; 
			dbg(TRACE, "GetCedaTimestamp: invalid 1.st argument <0x%8.8x>",pcpResultBuf) ; 
			ilRC = RC_FAIL ;
		}  
	} 

	if (ilRC == RC_SUCCESS)
	{
		if (ipResultLen < 15)
		{
			dbg( TRACE, "GetCedaTimestamp: invalid 2.st argument <%d>",ipResultLen);
			dbg( TRACE,"GetCedaTimestamp: minimum of 15 characters required!");
			ilRC = RC_FAIL ;
		} /* end of if */
	} /* end of if */

	if (ilRC == RC_SUCCESS)
	{
		memset ((void*) &clTimeStamp[0], 0x00, 16) ;
		llCurTime = time(0L) ;
		llCurTime += lpTimeOffset ;
		prlLocalTimeResult = (struct tm *) localtime (&llCurTime) ;
		memcpy ((char *) &rlTimeStruct,(char *)prlLocalTimeResult,sizeof(struct tm)) ;
		ilTimeLen = strftime(clTimeStamp,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTimeStruct) ;
		if (ilTimeLen <= ipResultLen)
		{
  			LocalToUtc ( clTimeStamp );
			strcpy (pcpResultBuf, clTimeStamp) ;
		} /* end of if */
	} /* end of if */

	return ilRC ;

} /* end of GetCedaTimestamp */


static BOOL ToBeBroadcasted ( char *pcpDate )
{
	BOOL blRet = TRUE;
	char clEndOfRange[16];

	if ( !pcpDate )
	{
		dbg( TRACE, "ToBeBroadcasted: Date is NULL, BCDays <%ld>", lgBCDays );
	}
	else
	{
		dbg( DEBUG, "ToBeBroadcasted: Date <%s>, BCDays <%ld>", pcpDate, lgBCDays );
	}
	if ( pcpDate && pcpDate[0] && ( lgBCDays >= 0 ) )
	{
		if ( GetCedaTimestamp( clEndOfRange, 16, lgBCDays ) == RC_SUCCESS )
		{
			if ( strcmp ( pcpDate, clEndOfRange ) > 0 )
			{
				dbg( DEBUG, "ToBeBroadcasted: <%s> later than end of Range <%s>",
							 pcpDate, clEndOfRange ); 
				blRet = FALSE;
			}
			else
			{
				dbg( DEBUG, "ToBeBroadcasted: <%s> before end of Range <%s>",
							 pcpDate, clEndOfRange );
			}
		}
	}
	return blRet;
}


static int SetRudLogicalFields ( ARRAYINFO *prpRudArr, char *pcpRudUrno )
{
	int ilRc = RC_SUCCESS, ilRet = RC_SUCCESS;
	long llRudRow = ARR_FIRST, llResRow, llFieldNo=-1, llQucoIdx=-1;
	char *pclRudResult=0, *pclResource=0, *pclRety, *pclRloc;
	char clReco[34], clQuco[211];
	char clResKey[12];

	if ( !pcpRudUrno )
		return RC_INVALID;

	while ( CEDAArrayFindRowPointer(&(prpRudArr->rrArrayHandle),
									prpRudArr->crArrayName,
									&(prpRudArr->rrIdx01Handle),
									prpRudArr->crIdx01Name,pcpRudUrno,
									&llRudRow, (void**)&pclRudResult) == RC_SUCCESS )
	{
		clReco[0] = clQuco[0] = '\0';

		/* dbg( DEBUG, "SetRudLogicalFields: found URNO <%s> RETY <%s>", clResKey, pclRety ); */

		if ( strcmp( pclRety, "100") == 0 )
		{
/*			ilRc |= GetQualifications ( clResKey, clQuco );*/
		}
		else
		{
			llResRow = ARR_FIRST;
		}
		dbg( DEBUG, "SetRudLogicalFields: URUD <%s> RECO <%s> QUCO <%s> RC <%d>",
			  clResKey, clReco, clQuco, ilRc );
		if ( ilRc == RC_SUCCESS )
		{
			ilRc = CEDAArrayPutField( &(prpRudArr->rrArrayHandle), 
									  prpRudArr->crArrayName, &llFieldNo,
									  "RECO",llRudRow, clReco );
		}
		if ( clQuco[0] )
		{
			ilRc = CEDAArrayPutField( &(prpRudArr->rrArrayHandle), 
									  prpRudArr->crArrayName,
									  &llQucoIdx,"QUCO",llRudRow, clQuco );
		}
		if ( ilRc != RC_SUCCESS )
		{
			ilRet = ilRc;
		}
		llRudRow = ARR_NEXT;	
	}/* endof while */
	return ilRet;
}

static int GetResourceName ( ARRAYINFO *prpArrayInfo, int ipGtabIdx, int ipUresIdx, 
							 int ipCodeIdx, char *pcpUrud, char *pcpName )
{
	long llRow = ARR_FIRST;
	char *pclResource=0, *pclSgrRow, *pclUres, *pclGtab, *pclCode;
	int ilRc = RC_NOTFOUND;

	if ( !pcpUrud || !pcpName )
	{
		return RC_INVALID;
	}
	
	/*dbg( DEBUG, "GetResourceName: ARRAY <%s> URUD <%s> UresIdx <%d> Codeidx <%d> GtabIdx <%d>", 
		  prpArrayInfo->crArrayName, pcpUrud, ipUresIdx, ipCodeIdx, ipGtabIdx );*/
	pcpName[0]= '\0';
	if ( CEDAArrayFindRowPointer(&(prpArrayInfo->rrArrayHandle),
								 prpArrayInfo->crArrayName,
								 &(prpArrayInfo->rrIdx01Handle),
								 prpArrayInfo->crIdx01Name,pcpUrud,
								 &llRow, (void**)&pclResource) == RC_SUCCESS )
	{
		pclUres = pclResource + prpArrayInfo->plrArrayFieldOfs[ipUresIdx-1];
		pclGtab = pclResource + prpArrayInfo->plrArrayFieldOfs[ipGtabIdx-1];
		pclCode = pclResource + prpArrayInfo->plrArrayFieldOfs[ipCodeIdx-1];
		
		if ( *pclGtab == 'S' )
		{
			llRow = ARR_FIRST;
		}
		else
		{
			strcpy ( pcpName, pclCode );
			ilRc = RC_SUCCESS;
		}
	}
	if ( ilRc != RC_SUCCESS )
	{
		dbg( TRACE, "GetResourceName failed: URUD <%s> ilRc <%d>", pcpUrud, ilRc );
	}

	return ilRc;
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/*	This table explains new OURI,OURO, DETY of splitted manual demands		  	
	(turnaround-> inbound + outbound) in function SplitManualDemand

	old Turnaround dem.  current Flight	->	old Demand          new Demand
		OURI  OURO			OURI  OURO		OURI  OURO DETY		OURI  OURO DETY
	----------------------------------------------------------------------------
	1.	1234  4567			1234  7890		1234  7890	1		empty 4567  2
	2.	1234  4567			7890  4567		7890  4567	2		1234  empty 1	
	3.	1234  4567			1234  empty		1234  empty	1		empty 4567  2		
	4.	1234  4567			empty 4567		empty 4567	2		1234  empty	1
*/
