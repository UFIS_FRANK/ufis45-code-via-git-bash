rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:


rem Copy ini- and config-files
copy c:\ufis_bin\*.ini %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.cfg %drive%\standard_build_msi_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\RosteringPrint.ulf %drive%\standard_build_msi\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\alerter.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\autocoverage.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\cuteif.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fidas.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\grp.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\initializeaccount.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\opsspm.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\opsscm.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\poolalloc.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\profileeditor.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\regelwerk.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\servicecatalog.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\staffbalance.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\staffterm.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\wgrtool.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\coco3.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rostering.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rosteringprint.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\absenceplanning.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\export.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\logtab_viewer.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\daco3tool.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\uld_manager.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\hub_manager.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\ufisdatabasetool.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\ruleschecker.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\infopc.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\OnlineChangesMonitor.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\FlightDataTool.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\FlightDataGenerator.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*

REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.ocx %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomclient.ocx %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomclient.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomserver.exe %drive%\standard_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomserver.tlb %drive%\standard_build_msi\ufis_client_appl\system\*.*


copy c:\ufis_bin\release\FlightPermits.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\CheckListProcessing.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\status_manager.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\data_changes.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\fips_reports.exe %drive%\standard_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\Ufis.Utils.dll %drive%\standard_build_msi\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\UserControls.dll %drive%\standard_build_msi\ufis_client_appl\applications\interopdll\*.*
copy c:\ufis_bin\release\ZedGraph.dll %drive%\standard_build_msi\ufis_client_appl\applications\interopdll\*.*


rem Copy ReleaseNotes
rem copy c:\ufis_bin\releasenotes.zip %drive%\standard_build_msi\ufis_client_appl\rel_notes\*.*


rem Copy Help-Files
copy c:\ufis_bin\Help\*.chm %drive%\standard_build_msi\ufis_client_appl\help\*.*
