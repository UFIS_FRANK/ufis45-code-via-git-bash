// CedaCotData.h

#ifndef __CEDAPCOTDATA__
#define __CEDAPCOTDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <CCSCedaData.h>

//---------------------------------------------------------------------------------------------------------

struct COTDATA 
{
	char 	 Ctrc[7]; 	// Contract-Code
	long 	 Urno;		// Unique record number
	char 	 Inbu[3]; 	// Indicator, if employee should be displayed in InitAccount
	char 	 Dptc[8+2]; // Organisationseinheit Code

	//DataCreated by this class
	int      IsChanged;

	COTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}
}; // end COTDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCotData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omKeyMap;
    CMapStringToPtr omCtrcMap;

    CCSPtrArray<COTDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// OCotations
public:
    CedaCotData();
	~CedaCotData();
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(COTDATA *prpCot);
	bool InsertInternal(COTDATA *prpCot);
	bool Update(COTDATA *prpCot);
	bool UpdateInternal(COTDATA *prpCot);
	bool Delete(long lpUrno);
	bool DeleteInternal(COTDATA *prpCot);
	bool Save(COTDATA *prpCot);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	COTDATA* GetCotByUrno(long lpUrno);
	COTDATA* GetCotByCtrc(CString opCtrc);
	COTDATA* GetCotByKey(CString opCtrc, CString opDptc);
	bool IsValidCot(COTDATA* prpCot);
	bool Initialize(CString opServerName);

	// Private methods
private:
    void PrepareCotData(COTDATA *prpCotData);

};

//---------------------------------------------------------------------------------------------------------

extern CedaCotData ogCotData;

#endif //__CEDAPCOTDATA__
