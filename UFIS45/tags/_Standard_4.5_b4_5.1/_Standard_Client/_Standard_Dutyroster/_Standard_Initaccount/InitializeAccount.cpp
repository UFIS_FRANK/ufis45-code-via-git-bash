// InitializeAccount.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <InitializeAccount.h>

#include <BasicData.h>
#include <CCSGlobl.h>
#include <PrivList.h>
#include <Ufis.h>
#include <CedaBasicData.h>
#include <CedaSystabData.h>
#include <CedaSorData.h>
#include <CedaScoData.h>
#include <GUILng.h>
#include <VersionInfo.h>

#include <LoginDlg.h>		// Login Dialog
#include <ListBoxDlg.h>		// Ausgabe neuer Parameter in diesem Dielog
#include <RegisterDlg.h>	// Dialog zum Registrieren der Anwendung
#include <InitialLoadDlg.h>	// Diealog zum auflisten der der geladenen Stammdeten
#include <InitializeAccountDlg.h>
#include <AatHelp.h>
#include <AatLogin.h>
#include <PrivList.h>
#include <CedaInitModuData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CStringArray  ogCmdLineStghArray;

/////////////////////////////////////////////////////////////////////////////
// CInitializeAccountApp

BEGIN_MESSAGE_MAP(CInitializeAccountApp, CWinApp)
	//{{AFX_MSG_MAP(CInitializeAccountApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitializeAccountApp construction

CInitializeAccountApp::CInitializeAccountApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CInitializeAccountApp object

CInitializeAccountApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CInitializeAccountApp initialization

BOOL CInitializeAccountApp::ExitInstance()
{
	AatHelp::ExitApp();
	return TRUE;
}

BOOL CInitializeAccountApp::InitInstance()
{
	// initialize handle of dialog box
	m_hwndDialog = NULL;

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	if (!AfxSocketInit())
	{
		AfxMessageBox("IDP_SOCKETS_INIT_FAILED");
		return FALSE;
	}

	// Standard initialization
	SetDialogBkColor();        // Set dialog background color to gray

	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// StingrayGrid initialisieren
	GXInit(); 

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	//***************************************************************************
	// Parameter�bergabe durch Aufruf aus einem anderen Programm
	//***************************************************************************
	ogCmdLineStghArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	//m_lpCmdLine => "AppName,UserID,Password"
	if(olCmdLine.GetLength() == 0)
	{
		ogCmdLineStghArray.Add(ogAppName);
		CTime olTime = CTime::GetCurrentTime();
		CString olTmp;
		olTmp = olTime.Format("%Y");
		olTmp.Insert(0, "31.12.");

		ogCmdLineStghArray.Add(olTmp);//CTime::GetCurrentTime().Format("%d.%m.%Y"));
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("");
	}
	else
	{
		if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
		{
			MessageBox(NULL,LoadStg(IDS_STRING31),"InitializeAccount",MB_ICONERROR);
			return FALSE;
		}
	}
	//---------------------------------------------------------------------------
	/*Beispiel zum aufrufen der Applikation mit Parameter
	  f�r das Programm aus dem heraus aufgerufen wird
	
		char *args[4];
		char slRunTxt[256];
		args[0] = "child";
		sprintf(slRunTxt,"%s,%s,%s,%s",Applikationsname,Username,Passwort,Free);
		args[1] = slRunTxt;
		args[2] = NULL;
		args[3] = NULL;
		int ilReturn = _spawnv(_P_NOWAIT,"InitializeAccount.exe",args);
	*/
	//---------------------------------------------------------------------------
	//***************************************************************************
	// Parameter�bergabe Ende
	//***************************************************************************

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Ufis"));

	//LoadStdProfileSettings();  // Load standard INI file options (including MRU)


	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}


    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pclConfigPath);

	// CedaBasicData Objekt initialisieren
	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogBCD.SetHomeAirport(CString(pcgHome));

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);


	// Standard Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	// INIT Tablenames and Homeairport
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	ogCommHandler.SetAppName(ogAppName);

    if(ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom ") + ogCommHandler.LastError());
        return FALSE;
    }

	//***********************************************************************************************
	// Initialisierung der GUI-Sprache
	//***********************************************************************************************
	// in ceda.ini einf�gen !!!!!!!!
	// ;'DE','US','IT' oder 'DE,Test' etc.
	// LANGUAGE=DE
	char lng[128];
	char dbparam[128];

	CGUILng* ogGUILng = CGUILng::TheOne();

    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", lng, sizeof lng, pclConfigPath);

    int ret = GetPrivateProfileString(ogAppName, "DBParam", "", dbparam, sizeof dbparam, pclConfigPath);
	CString Param = dbparam;
	if (Param.IsEmpty())
	{
		Param = "0";
	}

	CString Error	= "";
	CStringArray olApplArray;
	olApplArray.Add(ogAppl);
	bool rdr = ogGUILng->MemberInit(&Error, CString(pcgUser), &olApplArray, CString(pcgHome), CString(lng), Param);

	if (!rdr)
	{
		CString tmp = "Languagesupport failed!\n"+Error+"parameter empty!\n";
        AfxMessageBox(tmp);
		TRACE("\nMultiLng-Support failed!\n %sparameter empty!!!\n",Error);
	}
	//***********************************************************************************************
	// ENDE der Initialisierung der GUI-Sprache
	//***********************************************************************************************

	//--------------------------------------------------------------------------------------
	// Register your broadcasts here
	CString olTableName;
	CString olTableExt = CString(pcgTableExt);
	// RT : Read Table
	// IRT: Insert Record Table
	// URT: Update Record Table
	// DRT: Delete Record Table

	olTableName = CString("SBC") + olTableExt;
	ogBcHandle.AddTableCommand(olTableName,CString("XBS2"),BC_XBS2_READY,true);
	ogBcHandle.AddTableCommand(olTableName,CString("XBS"),BC_XBS2_READY,true);

	olTableName = CString("STF") + olTableExt;
	ogBcHandle.AddTableCommand (olTableName, CString("URT"), BC_STF_CHANGE, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("IRT"), BC_STF_NEW, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("DRT"), BC_STF_DELETE, true, pcgAppName);

	olTableName = CString("SOR") + olTableExt;
	ogBcHandle.AddTableCommand (olTableName, CString("URT"), BC_SOR_CHANGE, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("IRT"), BC_SOR_NEW, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("DRT"), BC_SOR_DELETE, true, pcgAppName);
	
	olTableName = CString("SCO") + olTableExt;
	ogBcHandle.AddTableCommand (olTableName, CString("URT"), BC_SCO_CHANGE, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("IRT"), BC_SCO_NEW, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("DRT"), BC_SCO_DELETE, true, pcgAppName);

	olTableName = CString("COT") + olTableExt;
	ogBcHandle.AddTableCommand (olTableName, CString("URT"), BC_COT_CHANGE, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("IRT"), BC_COT_NEW, true, pcgAppName);
	ogBcHandle.AddTableCommand (olTableName, CString("DRT"), BC_COT_DELETE, true, pcgAppName);

#if	0
	CLoginDialog olLoginDlg(pcgHome,ogAppName,NULL);
	if(ogCmdLineStghArray.GetAt(0) != ogAppName)
	{
		if(olLoginDlg.Login(ogCmdLineStghArray.GetAt(1),ogCmdLineStghArray.GetAt(2)) == false)
		{
			return FALSE;
		}
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK; 
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		} 
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_LISTBOX_DLG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());
	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}

	strcpy(pcgUser,olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());
	ogBasicData.omUserID = pcgUser;

	ogCommHandler.SetUser(pcgUser);
	strcpy(CCSCedaData::pcmUser, pcgUser);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif

	// Login Zeit setzen
	ogLoginTime = CTime::GetCurrentTime();

	// Ruft Lade-Dialog auf
    InitialLoad();

	// Pr�ft die geladenen Parameter
	CheckParameters();

	ogBasicData.SetLocalDiff();
	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich(); 

	// Anfangszeit der Nachtjobs f�r den xbshdl (Kompilieren von
	// Basic Skripten) aus ceda.ini lesen
	char pclStartNightJobAt[5];
	GetPrivateProfileString("InitializeAccount", "STARTNIGHTJOBAT", "0100", pclStartNightJobAt, sizeof pclStartNightJobAt, pclConfigPath);
	TRACE("\nZeitversetzte Jobs (STARTNIGHTJOBAT) starten um %sh.\n",pclStartNightJobAt);

	// read command for 'Lohnbestandteile' from ceda.ini
	char pclCmdLohnbestandteile[80];
	GetPrivateProfileString("InitializeAccount", "COMMAND-LOHNBESTANDTEILE", "INIT_LB", pclCmdLohnbestandteile, sizeof pclCmdLohnbestandteile, pclConfigPath);
	TRACE("\nKommando fuer Lohnbestandteile (COMMAND-LOHNBESTANDTEILE): '%s'.\n",pclCmdLohnbestandteile);
	
	// read command for recalculation
	char pclCmdRecalculation[80];
	GetPrivateProfileString("InitializeAccount", "COMMAND-RECALCULATION", "RECALC", pclCmdRecalculation, sizeof pclCmdRecalculation, pclConfigPath);
	TRACE("\nKommando fuer Konto-Neuberechnung (COMMAND-RECALCULATION): '%s'.\n",pclCmdRecalculation);
	
	// read time and date for monthly action
	char pclStartMonthlyTimeAndDate[7];
	GetPrivateProfileString("InitializeAccount", "MONTHLYTIMEANDDATE", "150230", pclStartMonthlyTimeAndDate, sizeof pclStartMonthlyTimeAndDate, pclConfigPath);
	TRACE("\nLohnbestandteile werden monatlich ausgef�hrt (TTSSMM): '%s'.\n",pclStartMonthlyTimeAndDate);
	
	// read maximum number of employee urnos coded in a single broadcast
	char pclMaxEmplUrnos[80];
	GetPrivateProfileString("InitializeAccount", "MAXEMPLURNO", "80", pclMaxEmplUrnos, sizeof pclMaxEmplUrnos, pclConfigPath);
	TRACE("\nMaximale Anzahl der Mitarbeiter Urnos pro Broadcast (MAXEMPLURNO): <%d>.\n",atoi(pclMaxEmplUrnos));

	// do main dialog
	CInitializeAccountDlg dlg(NULL,CString(pclStartNightJobAt),CString(pclCmdLohnbestandteile),CString(pclCmdRecalculation),CString(pclStartMonthlyTimeAndDate),atoi(pclMaxEmplUrnos));
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.

	if (ogGUILng != NULL)
	{
		delete ogGUILng;
	}
	ogGUILng = NULL;

	return FALSE;
}


//****************************************************************************
// Hier werden die Daten von der Datenbank in die Application geladen
//****************************************************************************

void CInitializeAccountApp::InitialLoad()
{
	pogInitialLoad = new CInitialLoadDlg();
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING1362));
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	bgIsInitialized = false;

	/////////////////////////////////////////////////////////////////////////////////////
	// add here your cedaXXXdata read methods
	/////////////////////////////////////////////////////////////////////////////////////
	//------------------------------------------------------------------------------------
	//ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
	ogStfData.SetTableName(CString(CString("STF") + CString(pcgTableExt)));
	ogSorData.SetTableName(CString(CString("SOR") + CString(pcgTableExt)));
	ogScoData.SetTableName(CString(CString("SCO") + CString(pcgTableExt)));
	ogCotData.SetTableName(CString(CString("COT") + CString(pcgTableExt)));

	ogStfData.Initialize(ogCommHandler.pcmRealHostName);
	ogSorData.Initialize(ogCommHandler.pcmRealHostName);
	ogScoData.Initialize(ogCommHandler.pcmRealHostName);
	ogCotData.Initialize(ogCommHandler.pcmRealHostName);

	//--------------------------------------------------------------------------------------
	//Update Sync because a crashed apllication could have set this state
	CTime olCurrentTime; 
	CTime olToTime;
	olCurrentTime = CTime::GetCurrentTime();
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	char pclData[1024]="";
	//SYNC END------------------------------------------------------------------------

	//---------------------------------------------------------------------------------
	// Start Reading

	CString olSizeText;
	int ilTmpLoadSize = 0;
	int ilPercent = 100 / 7; //Teiler ist gleich Anzahl der zu lesenden Tabellen

	// read parameters
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1844));
	int ilCnt;
	ilCnt = ogCCSParam.BufferParams(ogAppl);
	ilCnt += ogCCSParam.BufferParams("GLOBAL");
	olSizeText.Format(" ........ %d", ilCnt);
	LoadParameters();
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// read basic data into ogBCD
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1847));
	ogBCD.SetObject("ADE");
	ogBCD.SetObjectDesc("ADE", LoadStg(IDS_STRING1847));
	ogBCD.AddKeyMap("ADE", "URNO");
	ogBCD.AddKeyMap("ADE", "NAME");
	ogBCD.AddKeyMap("ADE", "CODE");
	ogBCD.AddKeyMap("ADE", "TYPE");
	ogBCD.AddKeyMap("ADE", "SCBI");
	ogBCD.AddKeyMap("ADE", "SCBR");
	ogBCD.AddKeyMap("ADE", "SCBS");
	ogBCD.Read(CString("ADE"));
	olSizeText.Format(" ........ %d",ogBCD.GetDataCount("ADE"));
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// OrgUnits (ORGTAB) - read basic data into ogBCD
	pogInitialLoad->SetMessage(LoadStg(IDS_Orgeinheit));
	ogBCD.SetObject("ORG");
	ogBCD.SetObjectDesc("ORG", LoadStg(IDS_Orgeinheit));
	ogBCD.Read(CString("ORG"));
	olSizeText.Format(" ........ %d",ogBCD.GetDataCount("ORG"));
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// Contracts (COTTAB)
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING13));
	ogCotData.Read();
	olSizeText.Format(" ........ %d",ogCotData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// read staff data
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1381));
	ogStfData.Read();
	olSizeText.Format(" ........ %d",ogStfData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// OrgUnit-Assignments (SORTAB)
	pogInitialLoad->SetMessage(LoadStg(IDS_Zuordnungen));
	ogSorData.Read();
	olSizeText.Format(" ........ %d",ogSorData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	// Contract assignments (SCOTAB)
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING12));
	ogScoData.Read("ORDER BY CODE");
	olSizeText.Format(" ........ %d",ogScoData.omData.GetSize());
	pogInitialLoad->SetMessage(olSizeText, false);
	pogInitialLoad->SetProgress(ilPercent);

	//-- End Reading
	pogInitialLoad->SetProgress(100);

	// Hochlade Dialog beenden
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		delete pogInitialLoad;
		pogInitialLoad = NULL;
	}
}


//****************************************************************************
// CAboutDlg dialog used for App About
//****************************************************************************

// App command to run the dialog
void CInitializeAccountApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_STATIC_USER, m_StcUser);
	DDX_Control(pDX, IDC_STATIC_SERVER, m_StcServer);
	DDX_Control(pDX, IDC_STATIC_LOGINTIME, m_StcLogintime);
	DDX_Control(pDX, IDC_COPYRIGHT4, m_Copyright4);
	DDX_Control(pDX, IDC_COPYRIGHT3, m_Copyright3);
	DDX_Control(pDX, IDC_COPYRIGHT2, m_Copyright2);
	DDX_Control(pDX, IDC_COPYRIGHT1, m_Copyright1);
	DDX_Control(pDX, IDC_SERVER,	 m_Server);
	DDX_Control(pDX, IDC_USER,		 m_User);
	DDX_Control(pDX, IDC_LOGINTIME,  m_Logintime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//****************************************************************************
// Daten aufbereiten und darstellen
//****************************************************************************

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olVersionString;
	VersionInfo rlInfo;

	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT

	olVersionString.Format("Initialize Accounts %s / %s", rlInfo.omFileVersion, __DATE__);

	// Statics einstellen
	m_Copyright1.SetWindowText(rlInfo.omProductName);
	m_Copyright2.SetWindowText(olVersionString);
	m_Copyright3.SetWindowText(rlInfo.omLegalCopyright);
	m_Copyright4.SetWindowText(LoadStg(IDS_COPYRIGHT4));
	m_StcServer.SetWindowText(LoadStg(IDS_STATIC_SERVER));
	m_StcUser.SetWindowText(LoadStg(IDS_STATIC_USER));
	m_StcLogintime.SetWindowText(LoadStg(IDS_STATIC_LOGINTIME));

	CString olServer = ogCommHandler.pcmRealHostName;
	olServer  += " / ";
	olServer  += ogCommHandler.pcmRealHostType;
	m_Server.SetWindowText(olServer);
	m_User.SetWindowText(CString(pcgUser));
	m_Logintime.SetWindowText(ogLoginTime.Format("%d.%m.%Y  %H:%M"));

	SetWindowText(LoadStg(IDS_APPL_ABOUTBOX));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CInitializeAccountApp::ProcessMessageFilter(int code, LPMSG lpMsg) 
{
	unsigned char chKey;
	if (m_hwndDialog != NULL){
		if ((lpMsg->hwnd == m_hwndDialog) || ::IsChild(m_hwndDialog, lpMsg->hwnd)){
			// Use ::IsChild to get messages that may be going
			// to the dialog's controls.  In the case of
			// WM_KEYDOWN this is required.
			if (lpMsg->message == WM_CHAR){
				chKey = (unsigned char)lpMsg->wParam;
				((CInitializeAccountDlg*)m_pMainWnd)->ProcessChar((UINT)chKey);
			}
		}
	}
	// default processing of message
	return CWinApp::ProcessMessageFilter(code, lpMsg);
}


void CInitializeAccountApp::LoadParameters()
{
	// aktuelle Zeit erhalten
	COleDateTime olTimeNow = COleDateTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	CString olDefaultValidFrom	= ("19901010101010");
	CString olDefaultValidTo	= "";

	ogCCSParam.GetParam(ogAppl,"ID_NORM_H_BASIS",olStringNow,"40",LoadStg(IDS_STRING14),"Accounting","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_NIGHT_START",olStringNow,"20:00",LoadStg(IDS_STRING15),"Accounting","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam(ogAppl,"ID_NIGHT_END",olStringNow,"06:00",LoadStg(IDS_STRING16),"Accounting","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
	ogCCSParam.GetParam("GLOBAL","ID_ENABLE_ACC",olStringNow,"Y",LoadStg(IDS_STRING17),"Accounting","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","1111111",true);
}

void CInitializeAccountApp::CheckParameters()
{
	// Sollten neue Parameter eingef�gt worden sein, Meldung ausgeben.
	CStringList* polMessageList = ogCCSParam.GetMessageList();
	if (polMessageList->GetCount() != 0)
	{
		CListBoxDlg olDlg(polMessageList, LoadStg(IDS_STRING1847),LoadStg(IDS_STRING1850));
		olDlg.DoModal();
	}
}