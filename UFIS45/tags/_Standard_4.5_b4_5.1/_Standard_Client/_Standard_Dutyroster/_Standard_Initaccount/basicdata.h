// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <InitialLoadDlg.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <ccscedaData.h>
#include <CcsLog.h>
#include <CCSTime.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>


CString LoadStg(UINT nID);

CTime COleDateTimeToCTime(COleDateTime opTime);
COleDateTime CTimeToCOleDateTime(CTime opTime);
// mit einem Datums-String ein COleDateTime-Objekt initialisieren
bool YYYYMMDDToOleDateTime(CString opTimeString, COleDateTime &opTime);

int GetItemCount(CString olList, char cpTrenner  = ',' );
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');
CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
CString DeleteListItem(CString &opList, CString olItem);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData);
CString SortItemList(CString opSubString, char cpTrenner);
int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner = ',');

/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CCSCedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;

	long GetNextUrno(void);
	int imNextOrder;


	int GetNextOrderNo();
	bool GetNurnos(int ipNrOfUrnos);

	char *GetCedaCommand(CString opCmdType);
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();
	int GetUtcDifference(void);

	void SetLocalDiff();

	CTimeSpan GetLocalDiff(CTime opDate)
	{
		if(opDate < omTich)
		{
			return omLocalDiff1;
		}
		else
		{
			return omLocalDiff2;
		}

		//return omLocalDiff;
	};
	CTimeSpan GetLocalDiff1() { return omLocalDiff1; };
	CTimeSpan GetLocalDiff2() { return omLocalDiff2; };
	CTime     GetTich() { return omTich; };

	void LocalToUtc(CTime &opTime);
	void UtcToLocal(CTime &opTime);
public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

private:
	CTimeSpan omLocalDiff1;
	CTimeSpan omLocalDiff2;
	CTime	  omTich;

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
private:
	CDWordArray omUrnos;
};


#endif
