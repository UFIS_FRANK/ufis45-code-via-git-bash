// CedaSorData.h

#ifndef __CEDASREDATA__
#define __CEDASREDATA__

#include <stdafx.h>
#include <CedaStfData.h>
#include <basicdata.h>
#include <afxdisp.h>

struct SREDATA 
{
	long			Urno;
	long			Surn;
	int				Regi;
	COleDateTime	Vpfr;
	COleDateTime	Vpto;

	//DataCreated by this class
	int				IsChanged;

	SREDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}
};

class CedaSreData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SREDATA> omDataSre;		// sorted by Surn & Time

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaSreData();
	~CedaSreData();
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SREDATA *prpSre);
	bool InsertInternal(SREDATA *prpSre);
	bool Update(SREDATA *prpSre);
	bool UpdateInternal(SREDATA *prpSre);
	bool Delete(long lpUrno);
	bool DeleteInternal(SREDATA *prpSre);
	bool ReadSpecialData(CCSPtrArray<SREDATA> *popSre,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SREDATA *prpSre);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SREDATA *GetSreByUrno(long lpUrno);
	int GetRegiBySurnWithTime(long lpSurn, COleDateTime opDate);
	bool IsRegularEmployee(long lpSurn, COleDateTime opDate);
	bool IsValidSre(SREDATA *prpSre);

// Private methods
private:
    void PrepareSreData(SREDATA *prpSreData);
	int FindFirstOfSurn(long lpSurn);
};

//---------------------------------------------------------------------------------------------------------
extern CedaSreData ogSreData;

#endif //__CEDASORDATA__