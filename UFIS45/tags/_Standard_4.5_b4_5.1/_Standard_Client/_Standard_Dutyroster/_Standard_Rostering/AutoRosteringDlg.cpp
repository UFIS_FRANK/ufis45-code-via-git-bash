// AutoRosteringDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rostering.h"
#include "AutoRosteringDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AutoRosteringDlg dialog


AutoRosteringDlg::AutoRosteringDlg(CWnd* pParent /*=NULL*/)
	: CDialog(AutoRosteringDlg::IDD, pParent)
{
	
	//{{AFX_DATA_INIT(AutoRosteringDlg)
	//m_RestDay->SelectedIndex = 1;
	//m_Week->SelectedIndex = 1;

	//}}AFX_DATA_INIT

}


void AutoRosteringDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AutoRosteringDlg)
	DDX_Control(pDX, IDC_WEEK, m_Week);
	DDX_Control(pDX, IDC_RESTDAY, m_RestDay);
	//}}AFX_DATA_MAP

	m_RestDay.SetCurSel(0);
	m_Week.SetCurSel(0);
}


BEGIN_MESSAGE_MAP(AutoRosteringDlg, CDialog)
	//{{AFX_MSG_MAP(AutoRosteringDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AutoRosteringDlg message handlers

void AutoRosteringDlg::OnOK() 
{
	// TODO: Add extra validation here
	int ilSel = m_RestDay.GetCurSel();
	CString olSelText = "";
	if(ilSel != CB_ERR)
	{
		m_RestDay.GetLBText(ilSel,olSelText);
	    imRestDay =	atoi(olSelText);
	}

	ilSel = m_Week.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_Week.GetLBText(ilSel,olSelText);
		imWeek = atoi(olSelText);
	}


   

	CDialog::OnOK();



}
