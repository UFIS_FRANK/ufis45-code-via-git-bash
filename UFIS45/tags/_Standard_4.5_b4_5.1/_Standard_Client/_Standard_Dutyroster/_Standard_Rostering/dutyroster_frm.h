//////////////////////////////////////////////////////////////////////////////////
//
//		HISTORY
//
//		rdr		12.01.2k	OnToolTipText added, overrides the CMDIChildWnd-fct
//
//////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DUTYROSTER_FRM_H__978497A5_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
#define AFX_DUTYROSTER_FRM_H__978497A5_5C56_11D3_8EF7_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DutyRoster_Frm.h : header file
//
// spezial ToolBar
#include <combobar.h>
#include <Plan_Info.h>
#include <CharacteristicValueDlg.h>

/////////////////////////////////////////////////////////////////////////////
// DutyRoster_Frm frame

class DutyRoster_Frm : public CMDIChildWnd
{
	DECLARE_DYNCREATE(DutyRoster_Frm)
protected:
	DutyRoster_Frm();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	// rdr 12.01.2000
	// for Multilanguage-ToolTop-Support added 
	bool OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult);
	// R�ckgabe der Toolbar
	CComboToolBar* GetToolBar() {return (&m_wndToolBar);}
	// Gibt einen Zeiger auf die Combobox zur�ck
	CComboBox* GetComboBox()	{return (&(m_wndToolBar.m_toolBarCombo));}
	// L�dt die beiden verschiedenen Toolbars
	void LoadToolBar(BOOL bIsAttention);
	// liefert den Zeiger auf den Kennzahl-Dialog
	CCharacteristicValueDlg* GetCValDlg(void) {return pomCharacteristicDlg;}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DutyRoster_Frm)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~DutyRoster_Frm();
	// Combobox in der ToolBar erstellen.
	BOOL CreateComboBox();

	// InfoBox mit allen Planungsstufen
	CPlan_Info* pmPlanInfoDlg;
	// 'Schwebedialog' mit Kennzahlen
	CCharacteristicValueDlg *pomCharacteristicDlg;
	
	// Generated message map functions
	//{{AFX_MSG(DutyRoster_Frm)
		afx_msg void OnSizing(UINT nSide, LPRECT lpRect );
		afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
		afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
		afx_msg void OnUpdateBView(CCmdUI* pCmdUI);
		afx_msg void OnUpdateBRelease(CCmdUI* pCmdUI);
		afx_msg void OnUpdateBPrint(CCmdUI* pCmdUI);
		afx_msg void OnUpdateBAttention(CCmdUI* pCmdUI);
		afx_msg void OnUpdateBActual(CCmdUI* pCmdUI);
		afx_msg void OnUpdateCombo(CCmdUI* pCmdUI);
		afx_msg void OnUpdateSelectMa(CCmdUI* pCmdUI);
		afx_msg void OnUpdateShowLegend(CCmdUI* pCmdUI);
		afx_msg void OnUpdateShowCharacteristicValues(CCmdUI* pCmdUI);
		afx_msg void OnUpdateExcel(CCmdUI* pCmdUI);
		afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
		afx_msg void OnBShowLegend();
		afx_msg void OnBShowCharacteristicValues();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// spezial Toolbar mit Combobox
	CComboToolBar	m_wndToolBar;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DUTYROSTER_FRM_H__978497A5_5C56_11D3_8EF7_00001C034EA0__INCLUDED_)
