#if !defined(AFX_GRIDDLG_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_)
#define AFX_GRIDDLG_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GridDlg.h : header file
//

class CGridControl;

/////////////////////////////////////////////////////////////////////////////
// CCommonGridDlg dialog

class CCommonGridDlg : public CDialog
{
// Construction
public:
	CCommonGridDlg(CString opCaption,CStringArray *popColumn1, 
		  CStringArray *popColumn2, 
		  CStringArray *popUrnos, 
		  CString opHeader1 = CString(""),
		  CString opHeader2 = CString(""),
		  CWnd* pParent = NULL,
		  bool bpShowSearch = false);   // standard constructor
	~CCommonGridDlg();

	// Ausgewählte Urno abholen
	CString GetReturnUrno() {return (omReturnString);}
// Dialog Data
	//{{AFX_DATA(CCommonGridDlg)
	enum { IDD = IDD_COMMON_GRID_DLG };
	CButton	m_OK;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommonGridDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Grid initialisieren
	void IniGrid ();
	void IniColWidths();
	void FillGrid();
	bool SortGrid(int ipRow);

	// Das Grid
	CGridControl	*pomGrid;

	// Grid initialisieren

	CStringArray	*pomColumn1; 
	CStringArray	*pomColumn2;
	CStringArray	*pomUrnos;
	CString			omHeader1;
	CString			omHeader2;

	CString			omCaption;
	CString			omReturnString;
	bool			bmShowSearch;
	
	// Generated message map functions
	//{{AFX_MSG(CCommonGridDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
    afx_msg LONG OnGridLButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBSearch();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AWDLG_H__435D8F21_784E_11D1_B434_0000B45A33F5__INCLUDED_)
