#ifndef _CEDADRSDATA_H_
#define _CEDADRSDATA_H_
 
#include <stdafx.h>
/*#include "CCSPtrArray.h"
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>*/

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define DRS_SEND_DDX	(true)
#define DRS_NO_SEND_DDX	(false)

// Felddimensionen
#define DRS_STAT_LEN	(1)
#define DRS_USES_LEN	(32)

// Struktur eines DRS-Datensatzes
struct DRSDATA {
	long			Ats1;					// Datensatznr. eines zugeordneten Mitarbeiters
	long			Drru;					// Datensatznr. des zugeordneten DRR
	char 			Sday[SDAY_LEN+2]; 	// Tages-Schl�ssel YYYYMMDD
	char 			Stat[DRS_STAT_LEN+2]; 	// Status des MAs (bei Zuordnungen)
	char 			Uses[DRS_USES_LEN+2]; 	// Verursacher des letzten Schicht tausches
	long 			Stfu;					// Mitarbeiter-Urno
	long 			Urno;					// Datensatz-Urno

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	DRSDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
	}
};	

// the broadcast CallBack function, has to be outside the CedaDrsData class
void ProcessDrsCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaDrsData: kapselt den Zugriff auf die Tabelle DRS (Daily Roster Supplements - untert�gige Erg�nzung)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaDrsData: public CedaData
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaDrsData(CString opTableName = "DRS", CString opExtName = "TAB");
	~CedaDrsData();

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_DRS_CHANGE,BC_DRS_DELETE und BC_DRS_NEW
	void ProcessDrsBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Lesen/Schreiben/Erzeugen/Kopieren von Datens�tzen
	// Datens�tze nach Datum gefiltert lesen
	bool ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
									CMapPtrToPtr *popLoadStfUrnoMap = NULL,
									bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadDrsByUrno(long lpUrno, DRSDATA *prpDrs);
	// Nutzinformationen kopieren (Schichtcode usw.)
	void CopyDrsValues(DRSDATA* popDrsDataSource,DRSDATA* popDrsDataTarget);
	// DRSDATA erzeugen, initialisieren und in die Datenhaltung mit aufnehmen
	DRSDATA* CreateDrsData(long lpStfUrno, CString opDay, long olDrru);
	// l�scht einen DRS, der dem DRR <lpDrrUrno> zugordnet ist
	bool RemoveInternalByDrrUrno(long lpDrrUrno,bool bpSendBC = false);
	// l�scht einen DRS, der dem DRR <popDrr> zugordnet ist
	bool DeleteDrsByDrrUrno(long lpDrrUrno);
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(DRSDATA *prpDrs, BOOL bpWithSave);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(DRSDATA *prpDrs, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(DRSDATA *prpDrs);

	// DRS mit Urno <lpUrno> suchen
	DRSDATA* GetDrsByUrno(long llUrno);
	// DRS suchen, der �ber die DRR-Urno <lpDrru> mit dem entsprechenden DRR verkn�pft ist
	DRSDATA* GetDrsByDrru(long lpDrru);
	// die DRR-Urno eines DRS ver�ndern
	bool ChangeDrrUrno(long lpSrcDrru, long lpNewDrru);

protected:	
// Funktionen
// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
// Datens�tze bearbeiten/speichern
	// einen Broadcast DRS_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(DRSDATA *prpDrs, bool bpSendDdx);
	// einen Broadcast DRS_CHANGE versenden
	bool UpdateInternal(DRSDATA *prpDrs, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(DRSDATA *prpDrs, bool bpSendDdx = true);
	// speichert einen einzelnen Datensatz
	bool Save(DRSDATA *prpDrs);
// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

// Daten
    // die geladenen Datens�tze
	CCSPtrArray<DRSDATA> omData;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
    // Map mit den DRR-Datensatz-Urnos der geladenen Datens�tze. Da es pro DRR nur
	// einen DRS geben kann, ist die MAP eindeutig
	CMapPtrToPtr omDrruMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten DRSs
	COleDateTime omMinDay;
	COleDateTime omMaxDay;
};

#endif	// _CEDADRSDATA_H_
