// ShiftPlanDlg.cpp: Implementierungsdatei
//---------------------------------------------------------------------------

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftPlanDlg 
//---------------------------------------------------------------------------

ShiftPlanDlg::ShiftPlanDlg(CString *popSPLName,CString *popOrgUnit,CString *popAction,CWnd* pParent /*=NULL*/)	: CDialog(ShiftPlanDlg::IDD, pParent)
{
	pomSPLName = popSPLName;
	pomOrgUnit = popOrgUnit;
	pomAction  = popAction;

	//{{AFX_DATA_INIT(ShiftPlanDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

void ShiftPlanDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftPlanDlg)
	DDX_Control(pDX, IDCANCEL,		m_B_Cancel);
	DDX_Control(pDX, IDC_CB_NAME,	m_CB_Name);
	DDX_Control(pDX, IDC_CB_SP_ORGUNIT,m_CB_OrgUnit);
	DDX_Control(pDX, IDC_B_NEW,		m_N_New);
	DDX_Control(pDX, IDC_B_DELETE,	m_B_Delete);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(ShiftPlanDlg, CDialog)
	//{{AFX_MSG_MAP(ShiftPlanDlg)
	ON_BN_CLICKED(IDC_B_DELETE, OnBDelete)
	ON_BN_CLICKED(IDC_B_NEW, OnBNew)
	ON_CBN_SELCHANGE(IDC_CB_NAME, OnSelchangeCbName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten ShiftPlanDlg 
//---------------------------------------------------------------------------

BOOL ShiftPlanDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(LoadStg(SHIFT_CAPTION));

	m_B_Cancel.SetWindowText(LoadStg(SHIFT_STF_B_CANCEL));
	m_N_New.SetWindowText(LoadStg(SHIFT_B_SAVEPLAN));
	m_B_Delete.SetWindowText(LoadStg(SHIFT_B_DELETE));

	SetDlgItemText(IDC_S_SHIFTROSTER,LoadStg(IDS_STRING392));
	SetDlgItemText(IDC_S_SP_ORGUNIT,LoadStg(IDS_STRING437));

	if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "N")
	{
		//we have to hide some controls and to do some resizings
		CRect olRect;
		int ilMoveUp = 70;

		GetDlgItem (IDC_S_SP_ORGUNIT)->ShowWindow(SW_HIDE);
		GetDlgItem (IDC_CB_SP_ORGUNIT)->ShowWindow(SW_HIDE);

		// --- resizing the form
		GetWindowRect (olRect);
		ClientToScreen (olRect);
		MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);
		CenterWindow();

		// --- resizing the frame
		GetDlgItem (IDC_SPL_STATIC)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDC_SPL_STATIC)->MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp + 10);

		// --- move bottom buttons up
		GetDlgItem (IDC_B_NEW)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDC_B_NEW)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem (IDC_B_DELETE)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDC_B_DELETE)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

		GetDlgItem (IDCANCEL)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem (IDCANCEL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
	}

	// fill CB with SPL-records ("Shift Roster")
	bool blAdd;
	CString olPriv;
	CString olOrgUnit;

	omObject = "SPL";
	pomRecord = new RecordSet(ogBCD.GetFieldCount(omObject));
	m_CB_Name.AddString("");

	int ilSnamIdx = ogBCD.GetFieldIndex(omObject,"SNAM");
	int ilDpt1Idx = ogBCD.GetFieldIndex(omObject,"DPT1");

	for (int i = 0; i < ogBCD.GetDataCount(omObject); i++)
	{
		blAdd = false;
		ogBCD.GetRecord(omObject,i, *pomRecord);

		if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "N")
		{
			blAdd = true;
		}
		else
		{
			if (ilDpt1Idx < 0)
			{
				blAdd = true;
			}
			else
			{
				olOrgUnit = pomRecord->Values[ilDpt1Idx];
				if (strlen(olOrgUnit) > 0)
				{
					olPriv = ogPrivList.GetStat(olOrgUnit);
					if (olPriv == "-" || olPriv == "0")
					{
						blAdd = false;
					}
					else
					{
						blAdd = true;
					}
				}
				else
				{
					blAdd = true;
				}
			}
		}

		if (blAdd == true)
		{
			m_CB_Name.AddString(pomRecord->Values[ilSnamIdx]);
		}
	}
	delete pomRecord;

	// fill the OrgUnitCombo
	m_CB_OrgUnit.ResetContent();
	m_CB_OrgUnit.AddString("");
	int ilOrgCount =  ogOrgData.omData.GetSize();
	ORGDATA *prlOrgData = NULL;
	CString olSec;
	for (i = 0; i < ilOrgCount; i++)
	{
		blAdd = false;
		prlOrgData = &ogOrgData.omData[i];

		if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
		{
			olSec = ogPrivList.GetStat(prlOrgData->Dpt1);
			if (olSec != "-" && olSec != "0")
			{
				blAdd = true;
			}
		}
		else
		{
			blAdd = true;
		}

		if (blAdd == true)
		{
			m_CB_OrgUnit.AddString(prlOrgData->Dpt1);
		}
	}

	return TRUE;
}

//---------------------------------------------------------------------------

void ShiftPlanDlg::OnBDelete() 
{
	CString olErrorText,olSPLName;	
	int ilSel = m_CB_Name.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_CB_Name.GetLBText(ilSel,olSPLName);
		olSPLName.TrimRight();
		olSPLName.TrimLeft();
		CString olTmp;
		bool blRet = ogBCD.GetField("SPL", "SNAM", olSPLName, "URNO", olTmp );
		if (blRet == true)
		{
			if (IDYES == MessageBox(LoadStg(ST_DATENSATZ_LOESCHEN),LoadStg(ST_FRAGE),(MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2)))
			{
				*pomAction = "DRT";
				*pomSPLName = olSPLName;
				CDialog::OnOK();
			}
		}
		else
		{
			Beep(440,70);
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(LoadStg(ST_DATENSATZ_WAEHLEN), LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
	}
}
//---------------------------------------------------------------------------

void ShiftPlanDlg::OnBNew() 
{
	OnOK();
}

//---------------------------------------------------------------------------

void ShiftPlanDlg::OnOK() 
{
	bool ilStatus = true;

	CString olSPLName;
	CString olErrorText;
	CString olOrgUnit;

	int ilSelItem = m_CB_Name.GetCurSel();
	if (ilSelItem != CB_ERR)
	{
		m_CB_Name.GetLBText(ilSelItem, olSPLName);
	}
	else
	{
		m_CB_Name.GetWindowText(olSPLName);
	}

	ilSelItem = m_CB_OrgUnit.GetCurSel();
	if (ilSelItem != CB_ERR)
	{
		m_CB_OrgUnit.GetLBText(ilSelItem, olOrgUnit);
	}
	else
	{
		m_CB_OrgUnit.GetWindowText(olOrgUnit);
	}

	olSPLName.TrimRight();
	olSPLName.TrimLeft();

	olOrgUnit.TrimRight();
	olOrgUnit.TrimLeft();

	if (olSPLName.GetLength() == 0)
	{
		ilStatus = false;
		olErrorText = LoadStg(IDS_STRING63475); //Schichtplan enth�lt keine Daten.\n*INTXT*
	}

	if (olSPLName.GetLength() > 32)
	{
		ilStatus = false;
		olErrorText = LoadStg(IDS_STRING63476); //Schichtplan entspricht nicht dem Eingabeformat.\n*INTXT*
	}

	if (ilStatus == true)
	{
		CString olTmp;
		bool blRet = ogBCD.GetField("SPL", "SNAM", olSPLName, "URNO", olTmp );
		if (blRet == true)
		{
			*pomAction = "URT";
		}
		else
		{
			*pomAction = "IRT";
		}
	}

	if (ilStatus == true)
	{
		*pomSPLName = olSPLName;
		*pomOrgUnit = olOrgUnit;
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		m_CB_Name.SetFocus();
	}	
}

void ShiftPlanDlg::OnSelchangeCbName() 
{
	int ilDpt1Idx = ogBCD.GetFieldIndex("SPL","DPT1");
	if (ilDpt1Idx > -1)
	{
		CString olSPLName;
		CString olOrgUnit;
		int ilSelItem = m_CB_Name.GetCurSel();
		if (ilSelItem != CB_ERR)
		{
			m_CB_Name.GetLBText(ilSelItem, olSPLName);
		}

		bool blRet = ogBCD.GetField("SPL", "SNAM", olSPLName, "DPT1", olOrgUnit );
		if (blRet == true)
		{
			int ilIdx = m_CB_OrgUnit.FindStringExact(-1,olOrgUnit);
			if (ilIdx != CB_ERR )
			{
				m_CB_OrgUnit.SetCurSel(ilIdx);
			}
			else
			{
				m_CB_OrgUnit.SetCurSel(-1);
			}
		}
	}
}
