// DutyPlanPs.cpp: Implementierungsdatei
// 

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int Compare_ODA( const ODADATA **e1, const ODADATA **e2);

///////////////////////////////////////////////////////////////////////////// 
// DutyPlanPs
//---------------------------------------------------------------------------

IMPLEMENT_DYNAMIC(DutyPlanPs, CPropertySheet)

DutyPlanPs::DutyPlanPs(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)	: CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
CCS_TRY;
	omDutyPlanAbsencePp.m_psp.dwFlags	&= ~(PSP_HASHELP);

	pomParent = (DutyRoster_View*)pParentWnd;

	AddPage(&omDutyPlanAbsencePp);

	pomCancelButton = NULL;
	pomOKButton     = NULL;
	pomSName		= NULL;
	pomSPersNr		= NULL;
	pomSDienstVon	= NULL;
	pomSDienstBis	= NULL;
	pomSPauseVon	= NULL;
	pomSPauseBis	= NULL;
	pomSDauer		= NULL;
	pomSRemark1		= NULL;
	pomSRemark2		= NULL;
	pomEName		= NULL;
	pomEPersNr		= NULL;
	pomEDienstVonD	= NULL;
	pomEDienstBisD	= NULL;
	pomEDienstVonT	= NULL;
	pomEDienstBisT	= NULL;
	pomEPauseVon	= NULL;
	pomEPauseBis	= NULL;
	pomEDauer		= NULL;
	pomERemark1		= NULL;
	pomERemark2		= NULL;
	pomDrrGrid		= NULL;

	omCurrentShift	= "";
	lmStfUrno		= 0;
	omPType			= "L";
CCS_CATCH_ALL;
}

DutyPlanPs::DutyPlanPs(LPCTSTR pszCaption, long lpEurn, CString opCurShift, COleDateTime opActDat, const CString& ropPType,CWnd* pParentWnd, UINT iSelectPage ) : CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
CCS_TRY;
	pomParent		= (DutyRoster_View*)pParentWnd;
	omCurrentShift	= opCurShift;					// Schicht-Nummer, f�r den der Dialog aufgerufen wurde
	omCurrentDate	= opActDat;						// Schicht-Tag, f�r den der Dialog aufgerufen wurde
	lmStfUrno		= lpEurn;						// Mitarbeiter-Urno
	omPType			= ropPType;						// Ausgew�hlte Planstufe des Anwenders

	omDutyPlanAbsencePp.m_psp.dwFlags &= ~(PSP_HASHELP);

	AddPage(&omDutyPlanAbsencePp);

	pomCancelButton = NULL;
	pomOKButton     = NULL;
	pomSName		= NULL;
	pomSPersNr		= NULL;
	pomSDienstVon	= NULL;
	pomSDienstBis	= NULL;
	pomSPauseVon	= NULL;
	pomSPauseBis	= NULL;
	pomSDauer		= NULL;
	pomSRemark1		= NULL;
	pomSRemark2		= NULL;
	pomEName		= NULL;
	pomEPersNr		= NULL;
	pomEDienstVonD	= NULL;
	pomEDienstBisD	= NULL;
	pomEDienstVonT	= NULL;
	pomEDienstBisT	= NULL;
	pomEPauseVon	= NULL;
	pomEPauseBis	= NULL;
	pomEDauer		= NULL;
	pomERemark1		= NULL;
	pomERemark2		= NULL;
	pomDrrGrid		= NULL;

	//////////////////////////////////////////////////////////////////////
	// Fill mask data ////////////////////////////////////////////////////

	omEDat = opActDat.Format("%d.%m.%Y");

	BSDDATA *prlBsd = ogBsdData.GetBsdByBsdc( opCurShift);
	if( prlBsd != NULL)
	{
		double dlSoll = atof(prlBsd->Sdu1)/60;
		omESollT.Format("%01.2f",dlSoll);
	}

	STFDATA *prlStf = ogStfData.GetStfByUrno(lmStfUrno);
	if( prlStf != NULL)
	{
		lmUrno	  = prlStf->Urno;
		VIEWINFO *polView = &pomParent->rmViewInfo;
		omEName = CBasicData::GetFormatedEmployeeName(polView->iEName, prlStf, "", "");
		omETel    = prlStf->Teld;
		omEPersNr = prlStf->Peno;
		omERem    = prlStf->Rema;
	}

	omDutyPlanAbsencePp.InitValues(lmStfUrno, pomParent);

CCS_CATCH_ALL;
}

DutyPlanPs::~DutyPlanPs()
{
CCS_TRY;
	delete pomDrrGrid;
	delete pomSName;
	delete pomSPersNr;
	delete pomSDienstVon;
	delete pomSDienstBis;
	delete pomSPauseVon;
	delete pomSPauseBis;
	delete pomSDauer;
	delete pomSRemark1;
	delete pomSRemark2;
	delete pomEName;
	delete pomEPersNr;
	delete pomEDienstVonD;
	delete pomEDienstBisD;
	delete pomEDienstVonT;
	delete pomEDienstBisT;
	delete pomEPauseVon;
	delete pomEPauseBis;
	delete pomEDauer;
	delete pomERemark1;
	delete pomERemark2;

CCS_CATCH_ALL;
}

BEGIN_MESSAGE_MAP(DutyPlanPs, CPropertySheet)
	//{{AFX_MSG_MAP(DutyPlanPs)
	ON_WM_PAINT()
	ON_COMMAND(ID_APP_EXIT,			OnAppExit)
	ON_BN_CLICKED(IDOK,				OnOK)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyPlanPs 
//---------------------------------------------------------------------------

void DutyPlanPs::OnPaint() 
{
	CPaintDC DC(this); // device context for painting
	
	COLORREF olHighlightColor	= GetSysColor(COLOR_3DHIGHLIGHT);
	COLORREF olLightColor		= GetSysColor(COLOR_3DLIGHT);
	COLORREF olDkShadowColor	= GetSysColor(COLOR_3DDKSHADOW);
	COLORREF olShadowColor		= GetSysColor(COLOR_3DSHADOW);
	COLORREF olWindowColor		= GetSysColor(COLOR_WINDOW);

	bool blPaint = false;
	BOOL blEnabled  = TRUE;
	BOOL blReadOnly = TRUE;
	CRect olRect;
	for(int i=0; i<omWndPtrArray.GetSize();i++)
	{
		CWnd *polWnd = (CWnd*)omWndPtrArray[i];
		blPaint = false;
		if(polWnd!=NULL)
		{
			blPaint = true;
			polWnd->GetWindowRect(&olRect);
			blEnabled = polWnd->IsWindowEnabled();
			if(polWnd->GetStyle() & ES_READONLY)
				blReadOnly = TRUE;
			else
				blReadOnly = FALSE;

		}
		if(blPaint)
		{
			ScreenToClient(&olRect);
  			olRect.left -= 3;olRect.top -= 3;olRect.right += 2;olRect.bottom += 2;
			DrawLine(&DC, olRect.left, olRect.top, olRect.right, olRect.top, olShadowColor);
			DrawLine(&DC, olRect.left, olRect.top, olRect.left, olRect.bottom, olShadowColor);
			DrawLine(&DC, olRect.left, olRect.bottom, olRect.right+1, olRect.bottom, olHighlightColor);
			DrawLine(&DC, olRect.right, olRect.top, olRect.right, olRect.bottom, olHighlightColor);
  			olRect.left += 1;olRect.top += 1;olRect.right -= 1;olRect.bottom -= 1;
			DrawLine(&DC, olRect.left, olRect.top, olRect.right, olRect.top, olDkShadowColor);
			DrawLine(&DC, olRect.left, olRect.top, olRect.left, olRect.bottom, olDkShadowColor);
			DrawLine(&DC, olRect.left, olRect.bottom, olRect.right+1, olRect.bottom, olLightColor);
			DrawLine(&DC, olRect.right, olRect.top, olRect.right, olRect.bottom, olLightColor);
			if(blEnabled && !blReadOnly)
			{
				olRect.left += 1;olRect.top += 1;olRect.right -= 1;olRect.bottom -= 1;
				DrawLine(&DC, olRect.left, olRect.top, olRect.right, olRect.top, olWindowColor);
				DrawLine(&DC, olRect.left, olRect.top, olRect.left, olRect.bottom, olWindowColor);
				DrawLine(&DC, olRect.left, olRect.bottom, olRect.right+1, olRect.bottom, olWindowColor);
				DrawLine(&DC, olRect.right, olRect.top, olRect.right, olRect.bottom, olWindowColor);
			}
		}
	}
}

//---------------------------------------------------------------------------

void DutyPlanPs::DrawLine(CDC *opDC, long left, long top, long right, long bottom, COLORREF color)
{ 
	CPen newPen;
	newPen.CreatePen(PS_SOLID, 1, color);
	CPen *oldPen = opDC->SelectObject(&newPen);
	opDC->MoveTo(left, top);
	opDC->LineTo(right, bottom);
	opDC->SelectObject(oldPen);
    newPen.DeleteObject();
}

//---------------------------------------------------------------------------

BOOL DutyPlanPs::OnInitDialog() 
{
CCS_TRY;
	BOOL bResult = CPropertySheet::OnInitDialog();

	// Titel setzen
	CString olWindowText;
	GetWindowText(olWindowText);
	olWindowText += CString(" [ ") + omEDat + CString(" ]");
	SetWindowText(olWindowText);

	CWnd *olHelpButton = GetDlgItem(IDHELP);
	if (olHelpButton != NULL)
	{
		olHelpButton->ShowWindow(SW_HIDE);
	}

	// Hardcodiert die H�he !
	int ilResizeHight = 130;

	// Sort ODA for AW-Dialog
	ogOdaData.omData.Sort(Compare_ODA);

	// ---resize the Sheet---
	CRect olSheetRect;
	GetWindowRect(&olSheetRect);
	olSheetRect.top -= ilResizeHight/2;
	olSheetRect.bottom += ilResizeHight/2 - 70;
	MoveWindow(&olSheetRect);

	// ---resize the Buttons---
	CRect olButtonRect;

	pomOKButton = (CButton*)GetDlgItem(IDOK);
	pomOKButton->GetWindowRect(&olButtonRect);
	ScreenToClient(&olButtonRect);
	olButtonRect.top += ilResizeHight - 75;
	olButtonRect.bottom += ilResizeHight - 75;

	int ilButtonWidth = olButtonRect.right - olButtonRect.left;
	int ilButtonLeft = ((olSheetRect.right - olSheetRect.left)/2) - ((ilButtonWidth*2 + 10 + 20)/2);
	olButtonRect.left = ilButtonLeft;
	olButtonRect.right = olButtonRect.left + ilButtonWidth;

	pomOKButton->MoveWindow(&olButtonRect);

	olButtonRect.left = olButtonRect.right + 5;
	olButtonRect.right = olButtonRect.left + ilButtonWidth;

	pomCancelButton = (CButton*)GetDlgItem(IDCANCEL);
	pomCancelButton->MoveWindow(&olButtonRect);

	SetDlgItemText(IDCANCEL,LoadStg(ID_CANCEL));
	SetDlgItemText(IDOK,LoadStg(ID_OK));

	// ---resize the CTabCtrl---
	int ilPageTop = ilResizeHight-132;
	CRect olCTabCtrlRect;
	CTabCtrl *plTab = GetTabControl();
	plTab->GetWindowRect(&olCTabCtrlRect);
	ScreenToClient(&olCTabCtrlRect);
	olCTabCtrlRect.top += ilPageTop + 50;
	olCTabCtrlRect.bottom += ilPageTop + 50;
	plTab->MoveWindow(&olCTabCtrlRect);

	// ---resize the Page---
	CPropertyPage *polPage = GetActivePage();
	CRect olPageRect;
	polPage->GetWindowRect(&olPageRect);
	ScreenToClient(&olPageRect);
	olPageRect.top += ilPageTop + 50;
	olPageRect.bottom += ilPageTop + 40;//- 10;
	polPage->MoveWindow(&olPageRect);

	// ---Create Fields---
	pomSName		= new CStatic();
	pomSPersNr		= new CStatic();
	pomSDienstVon	= new CStatic();
	pomSDienstBis	= new CStatic();
	pomSPauseVon	= new CStatic();
	pomSPauseBis	= new CStatic();
	pomSDauer		= new CStatic();
	pomSRemark1		= new CStatic();
	pomSRemark2		= new CStatic();
	pomEName		= new CCSEdit();
	pomEPersNr		= new CCSEdit();
	pomEDienstVonD	= new CCSEdit();
	pomEDienstBisD	= new CCSEdit();
	pomEDienstVonT	= new CCSEdit();
	pomEDienstBisT	= new CCSEdit();
	pomEPauseVon	= new CCSEdit();
	pomEPauseBis	= new CCSEdit();
	pomEDauer		= new CCSEdit();
	pomERemark1		= new CCSEdit();
	pomERemark2		= new CCSEdit();

/////////////////////////////////
	int ilHight = 17;
	int ilIntervall = ilHight + 10;
	CString olDP(":");

	//Create DutyRoster-Fields
	int ilLeft1		= 15;			
	int ilRight1	= ilLeft1	+ 75;
	int ilLeft2		= ilRight1	+ 10;	
	int ilRight2	= ilLeft2	+ 170;
	int ilLeft3		= ilRight2	+ 30;	
	int ilRight3	= ilLeft3	+ 90;
	int ilLeft4		= ilRight3	+ 10;	
	int ilRight4	= ilLeft4	+ 110;

	int ilTop1a = 15;					
	int ilBottom1a = ilTop1a+ilHight;
	int ilTop1b = ilTop1a+ilIntervall;	
	int ilTop1  = 0;					
	int ilTop2  = (ilTop1b+ilIntervall+8) -60 ;
	int ilBottom2  = (ilTop2+ilHight);
	int ilTop3  = ilTop2+ilIntervall;	
	int ilBottom3  = ilTop3+ilHight;

/////////////////////////////////
	pomSName->Create(LoadStg(IDS_STRING60)+olDP, WS_VISIBLE, CRect(ilLeft1,ilTop2,ilRight1,ilBottom2), this, IDC_STATIC);
	pomSName->SetFont(&ogMSSansSerif_Regular_8);

	pomEName->Create(WS_CHILD|WS_VISIBLE|ES_READONLY, CRect(ilLeft2,ilTop2,ilRight2,ilBottom2), this, 0);
	pomEName->SetFont(&ogMSSansSerif_Regular_8);
	omWndPtrArray.Add((void*)pomEName);
	pomEName->SetInitText(omEName);
	pomEName->SetBKColor(SILVER);
	/////////////////////////////
	pomSPersNr->Create(LoadStg(SHIFT_STF_PENO)+olDP, WS_VISIBLE, CRect(ilLeft3,ilTop2,ilRight3,ilBottom2), this, IDC_STATIC);
	pomSPersNr->SetFont(&ogMSSansSerif_Regular_8);

	pomEPersNr->Create(WS_CHILD|WS_VISIBLE|ES_READONLY, CRect(ilLeft4,ilTop2,ilRight4,ilBottom2), this, 0);
	pomEPersNr->SetFont(&ogMSSansSerif_Regular_8);
	omWndPtrArray.Add((void*)pomEPersNr);
	pomEPersNr->SetInitText(omEPersNr);
	pomEPersNr->SetBKColor(SILVER);
/////////////////////////////////

	//Create DRR-Fields
	ilLeft2		= ilRight1+10;		
	ilRight1	= ilLeft1+80;
	ilTop1a		= olCTabCtrlRect.bottom + 15;
	ilTop1		= ilTop1a+ilIntervall + 8;
	ilBottom1a	= ilTop1a+ilHight;
	ilBottom3	= ilTop1+(2*ilIntervall)+ilHight;

	SetWindowPos(&wndTop,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

	return bResult;
CCS_CATCH_ALL;
return FALSE;
}

//---------------------------------------------------------------------------

void DutyPlanPs::OnOK() 
{
CCS_TRY;
	CPropertySheet::EndDialog(1);
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void DutyPlanPs::InternalCancel()
{
CCS_TRY;
	CPropertySheet::EndDialog(0);
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyPlanPs::OnAppExit() 
{
CCS_TRY;
CCS_CATCH_ALL;
}

//**********************************************************************************
// InitDrrGrid: initialisiert das Grid f�r die DRRs.
// R�ckgabe:	keine
//**********************************************************************************

void DutyPlanPs::InitDrrGrid(CRect opRect)
{
CCS_TRY;
	// Grid erzeugen
	pomDrrGrid = new CGridControl();
	DWORD lWndStyle = WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL;
	if (pomDrrGrid->Create(lWndStyle, opRect, this, ID_DRR_GRID) == 0)
	{
		//RC(false);
		TRACE("DutyPlanPs::OnInitDialog(): Fehler beim Erzeugen des Grids.\n");
		return;
	}
	omWndPtrArray.Add((void*)pomDrrGrid);
	pomDrrGrid->SetParentWnd(this);
	// Grid initialisieren
	pomDrrGrid->Initialize();

	// Tooltips aktivieren
	pomDrrGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap = pomDrrGrid->GetParam()->GetStylesMap();
	olStylesmap->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	// Anzahl und Breite der Spalten/Zeilen einstellen
	InitDrrColWidths ();

	// �berschriften setzen
	pomDrrGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1890));
	pomDrrGrid->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1891));
	pomDrrGrid->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING1892));
	pomDrrGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING1893));
	pomDrrGrid->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING1894));
	pomDrrGrid->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING1895));
	// Tooltip-Texte der Spalten-Header einstellen
	pomDrrGrid->SetStyleRange(CGXRange(0,1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1896)));	
	pomDrrGrid->SetStyleRange(CGXRange(0,2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1897)));	
	pomDrrGrid->SetStyleRange(CGXRange(0,3), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1898)));	
	pomDrrGrid->SetStyleRange(CGXRange(0,4), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1899)));	
	pomDrrGrid->SetStyleRange(CGXRange(0,5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1900)));	
	pomDrrGrid->SetStyleRange(CGXRange(0,6), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1901)));	
	
	
	// Grid mit Daten f�llen.
	FillDrrGrid();

	pomDrrGrid->GetParam()->EnableUndo(FALSE);
	pomDrrGrid->LockUpdate(TRUE);
	pomDrrGrid->LockUpdate(FALSE);
	pomDrrGrid->GetParam()->EnableUndo(TRUE);

	
	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomDrrGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	pomDrrGrid->GetParam()->EnableSelection(GX_SELROW);

	pomDrrGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				);
	pomDrrGrid->EnableAutoGrow ( false );

CCS_CATCH_ALL;
}

//**********************************************************************************
// InitDrrColWidths: stellt die Spaltenbreiten des Grids ein.
// R�ckgabe:	keine
//**********************************************************************************

void DutyPlanPs::InitDrrColWidths()
{
CCS_TRY;	
	pomDrrGrid->SetColCount(6);
	pomDrrGrid->SetRowCount(0);
	pomDrrGrid->SetColWidth ( 0, 0, 25);
	pomDrrGrid->SetColWidth ( 1, 1, 55);
	pomDrrGrid->SetColWidth ( 2, 2, 55);
	pomDrrGrid->SetColWidth ( 3, 3, 55);
	pomDrrGrid->SetColWidth ( 4, 4, 55);
	pomDrrGrid->SetColWidth ( 5, 5, 55);
	pomDrrGrid->SetColWidth ( 6, 6, 192);
	// �berschriften setzen
	pomDrrGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1890));
	pomDrrGrid->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1891));
	pomDrrGrid->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING1892));
	pomDrrGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING1893));
	pomDrrGrid->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING1894));
	pomDrrGrid->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING1895));
CCS_CATCH_ALL;
}

//**********************************************************************************
// FillDrrGrid: Grid mit Daten f�llen.
// R�ckgabe:	keine
//**********************************************************************************

void DutyPlanPs::FillDrrGrid()
{
CCS_TRY;	
	ROWCOL ilRowCount;
	BSDDATA * polBsd;
	CCSPtrArray<DRRDATA> olDrrList;
	
	if (ogDrrData.GetDrrListByKeyWithoutDrrn(omCurrentDate.Format("%Y%m%d"),lmStfUrno,""/*omCurrentShift*/,&olDrrList) == 0)
	{
		// keine Datens�tze -> abbrechen
		return;
	}

	// Readonly ausschalten
	pomDrrGrid->GetParam()->SetLockReadOnly(false);

	// Anzahl der Rows ermitteln
	ilRowCount = pomDrrGrid->GetRowCount();
	if (ilRowCount > 0)
	{
		// Grid l�schen			
		pomDrrGrid->RemoveRows(1,ilRowCount);
	}

	for(int ilCount=0; ilCount<olDrrList.GetSize(); ilCount++)
	{
		// Neue Row einf�gen
		ilRowCount = pomDrrGrid->GetRowCount();
		pomDrrGrid->InsertRows(ilRowCount+1,1);
		// Feldwerte eintragen (IST)

		pomDrrGrid->SetValueRange(CGXRange(ilRowCount+1,0),LoadStg(IDS_STRING1905));

		if(olDrrList[ilCount].Avfr.GetStatus() == COleDateTime::valid)
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+1,1),(olDrrList[ilCount].Avfr).Format("%H:%M"));
		if(olDrrList[ilCount].Avto.GetStatus() == COleDateTime::valid)
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+1,2),(olDrrList[ilCount].Avto).Format("%H:%M"));
		if(olDrrList[ilCount].Sbfr.GetStatus() == COleDateTime::valid)
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+1,3),(olDrrList[ilCount].Sbfr).Format("%H:%M"));
		if(olDrrList[ilCount].Sbto.GetStatus() == COleDateTime::valid)
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+1,4),(olDrrList[ilCount].Sbto).Format("%H:%M"));
		pomDrrGrid->SetValueRange(CGXRange(ilRowCount+1,5),CString(olDrrList[ilCount].Sblu));
		pomDrrGrid->SetValueRange(CGXRange(ilRowCount+1,6),CString(olDrrList[ilCount].Rema));
		// Feldwerte eintragen (soll)
		// Schicht ermitteln
		polBsd = ogBsdData.GetBsdByBsdc(olDrrList[ilCount].Scod);
		if (polBsd != NULL)
		{
			// Neue Row einf�gen
			pomDrrGrid->InsertRows(ilRowCount+2,1);

			// Farbe setzen
			pomDrrGrid->SetStyleRange( CGXRange(ilRowCount+2,1,ilRowCount+2,5), CGXStyle().SetTextColor(COLORREF(GREEN)));

			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+2,0),LoadStg(IDS_STRING1906));

			CString olEsbg(polBsd->Esbg);
			olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+2,1),olEsbg);

			CString olLsen(polBsd->Lsen);
			olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+2,2),olLsen);

			CString olBkf1(polBsd->Bkf1);
			if (!olBkf1.IsEmpty())
			{
				olBkf1 = olBkf1.Left(2) + ":" + olBkf1.Right(2);
				pomDrrGrid->SetValueRange(CGXRange(ilRowCount+2,3),olBkf1);
			}

			CString olBkt1(polBsd->Bkt1);
			if (!olBkt1.IsEmpty())
			{
				olBkt1 = olBkt1.Left(2) + ":" + olBkt1.Right(2);
				pomDrrGrid->SetValueRange(CGXRange(ilRowCount+2,4),olBkt1);
			}
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+2,5),polBsd->Bkd1);
			pomDrrGrid->SetValueRange(CGXRange(ilRowCount+2,6),polBsd->Rema);
		}
		
//		ilCount++;
	}
	// Readonly einschalten
	pomDrrGrid->GetParam()->SetLockReadOnly(true);
CCS_CATCH_ALL;
}

//*******************************************************************************************************************
//*******************************************************************************************************************
// Globale Funktionen
//*******************************************************************************************************************
//*******************************************************************************************************************

static int Compare_ODA( const ODADATA **e1, const ODADATA **e2)
{
CCS_TRY;
	CString ole1,ole2;
	ole1 = (**e1).Sdac;
	ole2 = (**e2).Sdac;
	return (ole1.Collate(ole2));
CCS_CATCH_ALL;
return 0;
}

//*******************************************************************************************************************
//*******************************************************************************************************************
//	Klasse DutyInsert
//	Hilfsklasse zum Eintragen von Abwesenheitscodes in DRRs f�r mehrere Tage, die
//	auch ausserhalb des View-Zeitraums liegen k�nnen.
//*******************************************************************************************************************
//*******************************************************************************************************************

DutyInsert::DutyInsert()
{
CCS_TRY;
CCS_CATCH_ALL;
}

/*************************************************************************************************************************************
// WriteInDB: �ndert die aktiven und ggbfl. die Urlaubs-DRRs des Mitarbeiters 
//	<lpStfUrno> im Zeitraum <opFrom> bis <opTo>, der Abwesenheits-Code 
//	<opCode> wird eingetragen. Wenn die DRRs nicht existieren, werden sie erzeugt.
bool bpInsert - false - Daten l�schen und nicht einf�gen!!!
// R�ckgabe:	keine
*************************************************************************************************************************************/

void DutyInsert::WriteInDB(long lpStfUrno, COleDateTime opFrom, COleDateTime opTo, 
						   CString opCode, CString opPType, CWnd* pParentWnd, bool bpUpdateActiveDRR,
						   bool bpUpdateVacancyDRR, bool bpOverwriteWeekend,
						   bool bpOverwriteRegFree, bool bpInsert/*=true*/)
{
CCS_TRY;

	// Abwesenheitscode pr�fen
	ODADATA *prlOda = NULL;
	DRRDATA *prlDrr = NULL;
	prlOda = ogOdaData.GetOdaBySdac(opCode);
	if(prlOda == NULL)
	{
		// Abwesenheitscode nicht gefunden -> abbrechen
		return;
	}

	// Warte-Cursor
	AfxGetApp()->DoWaitCursor(1);

	// Zeiger auf Parent f�r Zugriff auf View-Info
	DutyRoster_View *polParent = (DutyRoster_View*)pParentWnd;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Pr�fen, ob Daten nachgeladen werden m�ssen
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	COleDateTime olLoadDateFrom		= polParent->rmViewInfo.oLoadDateFrom;
	COleDateTime olLoadDateTo		= polParent->rmViewInfo.oLoadDateTo;
	bool blConflictCheck			= polParent->rmViewInfo.bDoConflictCheck;
	COleDateTime olInsertFrom		= opFrom;
	COleDateTime olInsertTo			= opTo;

	// Zeitrahmen pr�fen
	int ilCheckVal = CedaDataHelper::GetMaxPeriod(olLoadDateFrom,olLoadDateTo,olInsertFrom,olInsertTo,&olLoadDateFrom,&olLoadDateTo);
	if ((ilCheckVal != CHECK_P2_INNER_P1) && (ilCheckVal != CHECK_INVALID_DATE))
	{
		// MA neu laden
		polParent->LoadStaffInData(lpStfUrno, COleDateTimeToCTime(olLoadDateFrom), COleDateTimeToCTime(olLoadDateTo));
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Ende Pr�fen, ob Daten nachgeladen werden m�ssen
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// DRR �ndern
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	COleDateTimeSpan olInsertPeriode = olInsertTo - olInsertFrom;
	COleDateTime olDay = olInsertFrom;
	// Anzahl der Tage, die bearbeitet werden
	int ilDaysInPeriode = (int)olInsertPeriode.GetDays() + 1;

	for(int i=0; i<ilDaysInPeriode; i++)
	{
		// 'reg.-arbeitsfrei'-Schicht ermitteln
		ODADATA *prlFreeOda = ogOdaData.GetDefaultFreeODA(olDay);

		// ADO: Pr�fen ob DefaultFreeODA g�ltig ist
		if (prlFreeOda != NULL)
		{
			//----------------------------------------------------
			// ODA-Code in aktiven DRR eintragen
			//----------------------------------------------------
			if (bpUpdateActiveDRR)
			{
				// Wochenenden mit 'reg.-arbeitsfrei'-Schicht �berschreiben?
				if (bpOverwriteWeekend)
				{
					// ja -> Sonntag oder Sonnabend?
					if (CedaDataHelper::IsWeekendOrHoliday(olDay, polParent->pomIsTabViewer->omHolidayKeyMap,lpStfUrno))
					{
						if(!ogDrrData.SetDrrDataScodByODA(prlFreeOda,lpStfUrno,olDay.Format("%Y%m%d"),opPType,true,"1",true,NULL,blConflictCheck))
						{
							goto lbl;
						}
						else
						{
							prlDrr = ogDrrData.GetDrrByKey(olDay.Format("%Y%m%d"),lpStfUrno,"2",opPType);
							if (prlDrr != NULL)
							{
								if(!ogDrrData.SetDrrDataScodByODA(prlOda,lpStfUrno,olDay.Format("%Y%m%d"),opPType,true,"2",true,NULL,blConflictCheck))
								{
									goto lbl;
								}
								//ogDrrData.Delete(prlDrr,TRUE);
							}
						}
					}
					else 
					{
						// nein -> Werktag, die vom Benutzer ausgew�hlte Abwesenheit benutzen
						if(!ogDrrData.SetDrrDataScodByODA(prlOda,lpStfUrno,olDay.Format("%Y%m%d"),opPType,true,"1",true,NULL,blConflictCheck))
						{
							goto lbl;
						}
						else
						{
							prlDrr = ogDrrData.GetDrrByKey(olDay.Format("%Y%m%d"),lpStfUrno,"2",opPType);
							if (prlDrr != NULL)
							{
								if(!ogDrrData.SetDrrDataScodByODA(prlOda,lpStfUrno,olDay.Format("%Y%m%d"),opPType,true,"2",true,NULL,blConflictCheck))
								{
									goto lbl;
								}
								//ogDrrData.Delete(prlDrr,TRUE);
							}
						}
					}
				}
				// Tage mit 'reg.-arbeitsfrei'-Schicht NICHT �berschreiben?
				else if (bpOverwriteRegFree)
				{
					if(!ogDrrData.SetDrrDataScodByODA(prlOda,lpStfUrno,olDay.Format("%Y%m%d"),opPType,true,"1",true,prlFreeOda,blConflictCheck))
					{
						goto lbl;
					}
					else
					{
						prlDrr = ogDrrData.GetDrrByKey(olDay.Format("%Y%m%d"),lpStfUrno,"2",opPType);
						if (prlDrr != NULL)
						{
							if(!ogDrrData.SetDrrDataScodByODA(prlOda,lpStfUrno,olDay.Format("%Y%m%d"),opPType,true,"2",true,prlFreeOda,blConflictCheck))
							{
								goto lbl;
							}
							//ogDrrData.Delete(prlDrr,TRUE);
						}
					}
				}
			}
			//----------------------------------------------------
			// ODA-Code in Urlaubs-DRR eintragen
			//----------------------------------------------------
			if (bpUpdateVacancyDRR) 
			{
				// Wochenenden mit 'reg.-arbeitsfrei'-Schicht �berschreiben?
				if (bpOverwriteWeekend)
				{
					// ja -> Sonntag oder Sonnabend?
					if (CedaDataHelper::IsWeekendOrHoliday(olDay, polParent->pomIsTabViewer->omHolidayKeyMap,lpStfUrno))
					{
						if(!ogDrrData.SetDrrDataScodByODA(prlFreeOda,lpStfUrno,olDay.Format("%Y%m%d"),"U",false,"1",true,NULL,blConflictCheck))
							goto lbl;
					}
					else // nein -> Werktag, die vom Benutzer ausgew�hlte Abwesenheit benutzen
					{
						if(!ogDrrData.SetDrrDataScodByODA(prlOda,lpStfUrno,olDay.Format("%Y%m%d"),"U",false,"1",true,NULL,blConflictCheck))
							goto lbl;
					}
				}
				// Tage mit 'reg.-arbeitsfrei'-Schicht NICHT �berschreiben?
				else if (bpOverwriteRegFree)
				{
					if(!ogDrrData.SetDrrDataScodByODA(prlOda,lpStfUrno,olDay.Format("%Y%m%d"),"U",false,"1",true,prlFreeOda,blConflictCheck))
					{
						goto lbl;
					}
					/*else
					{
						prlDrr = ogDrrData.GetDrrByKey(olDay.Format("%Y%m%d"),lpStfUrno,"2",opPType);
						if (prlDrr != NULL)
						{
							ogDrrData.Delete(prlDrr,TRUE);
						}
					}*/
				}
			}
		}
lbl:
		olDay += COleDateTimeSpan(1,0,0,0);
	}

	CStringArray* polWarningsArray = ogShiftCheck.GetWarningList();
	if (!polWarningsArray)
		return;

	for(int ilCount=0; ilCount<polWarningsArray->GetSize(); ilCount++)
	{
		polParent->InsertAttention (polWarningsArray->GetAt(ilCount), RED);
	}

	// aufr�umen
	delete polWarningsArray;

	RemoveWaitCursor();

CCS_CATCH_ALL;
}