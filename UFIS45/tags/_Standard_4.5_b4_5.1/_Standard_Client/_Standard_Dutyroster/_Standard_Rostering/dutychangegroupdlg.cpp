// DutyChangeGroupDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



static int ComparePfc( const PFCDATA **e1, const PFCDATA **e2);

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyChangeGroupDlg 
/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------

DutyChangeGroupDlg::DutyChangeGroupDlg(INLINEDATA *prpInlineUpdate1, INLINEDATA *prpInlineUpdate2, CWnd* pParent /*=NULL*/) : CDialog(DutyChangeGroupDlg::IDD, pParent)
{
	CCS_TRY;
	//{{AFX_DATA_INIT(DutyChangeGroupDlg)
	//}}AFX_DATA_INIT

	pomParent = (DutyRoster_View*)pParent;
	m_pToolTip = NULL;

	rmInlineUpdate1 = *prpInlineUpdate1; // 1 = Drop ( to Staff ), 2 = Drag ( from Staff )
	rmInlineUpdate2 = *prpInlineUpdate2;

	pomGroups = &pomParent->omGroups;
	pomStfData = &pomParent->omStfData;
	prmViewInfo = &pomParent->rmViewInfo;

	imGroupLine1 = rmInlineUpdate1.iRow + rmInlineUpdate1.iRowOffset;
	imGroupLine2 = rmInlineUpdate2.iRow + rmInlineUpdate2.iRowOffset;
	CCS_CATCH_ALL;
}


DutyChangeGroupDlg::~DutyChangeGroupDlg()
{
	delete m_pToolTip;
}


void DutyChangeGroupDlg::DoDataExchange(CDataExchange* pDX)
{
	CCS_TRY;
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyChangeGroupDlg)
	DDX_Control(pDX, IDC_C_TAUSCHEN1,	 m_C_Tauschen1);
	DDX_Control(pDX, IDC_C_TAUSCHEN2,	 m_C_Tauschen2);
	DDX_Control(pDX, IDC_C_ERSETZEN1,	 m_C_Ersetzen1);
	DDX_Control(pDX, IDC_C_ERSETZEN2,	 m_C_Ersetzen2);
	DDX_Control(pDX, IDC_C_UMSETZEN2,	 m_C_Umsetzen2);
	DDX_Control(pDX, IDC_B_UMSETZEN,	 m_B_Umsetzen);
	DDX_Control(pDX, IDC_B_TAUSCHEN,	 m_B_Tauschen);
	DDX_Control(pDX, IDC_B_INFO,		 m_B_Info);
	DDX_Control(pDX, IDC_B_ERSETZEN,	 m_B_Ersetzen);
	DDX_Control(pDX, IDC_G_Name2,		 m_G_Name2);
	DDX_Control(pDX, IDC_G_NAME1,		 m_G_Name1);
	DDX_Control(pDX, IDC_E_ERSETZEN1,	 m_E_Ersetzen1);
	DDX_Control(pDX, IDC_E_ERSETZEN2,	 m_E_Ersetzen2);
	DDX_Control(pDX, IDC_E_FUNCIS1,		 m_E_FuncIs1);
	DDX_Control(pDX, IDC_E_FUNCIS2,		 m_E_FuncIs2);
	DDX_Control(pDX, IDC_E_FUNCSTAMM1,	 m_E_FuncStamm1);
	DDX_Control(pDX, IDC_E_FUNCSTAMM2,	 m_E_FuncStamm2);
	DDX_Control(pDX, IDC_E_GRUPPIS1,	 m_E_GruppIs1);
	DDX_Control(pDX, IDC_E_GRUPPIS2,	 m_E_GruppIs2);
	DDX_Control(pDX, IDC_E_GRUPPSTAMM1,	 m_E_GruppStamm1);
	DDX_Control(pDX, IDC_E_GRUPPSTAMM2,	 m_E_GruppStamm2);
	DDX_Control(pDX, IDC_E_TAUSCHEN1,	 m_E_Tauschen1);
	DDX_Control(pDX, IDC_E_TAUSCHEN2,	 m_E_Tauschen2);
	DDX_Control(pDX, IDC_E_UMSETZEN2,	 m_E_Umsetzen2);
	//}}AFX_DATA_MAP
	CCS_CATCH_ALL;
}


BEGIN_MESSAGE_MAP(DutyChangeGroupDlg, CDialog)
	//{{AFX_MSG_MAP(DutyChangeGroupDlg)
	ON_BN_CLICKED(IDC_B_ERSETZEN, OnBErsetzen)
	ON_BN_CLICKED(IDC_B_TAUSCHEN, OnBTauschen)
	ON_BN_CLICKED(IDC_B_UMSETZEN, OnBUmsetzen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyChangeGroupDlg 
/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------

BOOL DutyChangeGroupDlg::OnInitDialog() 
{
	CCS_TRY;

	// >>>>>> 1 = Drop ( to Staff ), 2 = Drag ( from Staff ) <<<<<<<
	CDialog::OnInitDialog();

	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);
	
	m_B_Info.EnableWindow(FALSE);
	//Set Window and Cursor Pos.
	CRect olDlgRect, olParentRect, olSystemRect;
	olSystemRect.left	= 0;
	olSystemRect.top	= 0;
	olSystemRect.right  = ::GetSystemMetrics(SM_CXSCREEN);
	olSystemRect.bottom = ::GetSystemMetrics(SM_CYSCREEN);
	
	pomParent->GetWindowRect(&olParentRect);
	GetWindowRect(&olDlgRect);

	int ilLeft = (olParentRect.left + 200) - olDlgRect.left;
	olDlgRect.left += ilLeft;
	olDlgRect.right += ilLeft;
	if(olDlgRect.right>olSystemRect.right)
	{
		ilLeft = olDlgRect.right - olSystemRect.right;
		olDlgRect.left -= ilLeft;
		olDlgRect.right -= ilLeft;
	}

	olDlgRect.top += 100;
	olDlgRect.bottom += 100;
	if(olDlgRect.bottom>olSystemRect.bottom)
	{
		int ilTop = olDlgRect.bottom - olSystemRect.bottom;
		olDlgRect.top -= ilTop;
		olDlgRect.bottom -= ilTop;
	}

	MoveWindow(&olDlgRect);

	::SetCursorPos(olDlgRect.left+((olDlgRect.right-olDlgRect.left)/2), olDlgRect.top + 90);
	// End >> Set Window and Cursor Pos.

	m_E_Ersetzen1.SetBKColor(SILVER);
	m_E_Ersetzen2.SetBKColor(SILVER);
	m_E_FuncIs1.SetBKColor(SILVER);
	m_E_FuncIs2.SetBKColor(SILVER);
	m_E_FuncStamm1.SetBKColor(SILVER);
	m_E_FuncStamm2.SetBKColor(SILVER);
	m_E_GruppIs1.SetBKColor(SILVER);
	m_E_GruppIs2.SetBKColor(SILVER);
	m_E_GruppStamm1.SetBKColor(SILVER);
	m_E_GruppStamm2.SetBKColor(SILVER);
	m_E_Tauschen1.SetBKColor(SILVER);
	m_E_Tauschen2.SetBKColor(SILVER);
	m_E_Umsetzen2.SetBKColor(SILVER);

	// Windows Text f�llen in der Form: "[ alter MA1 ] <<< [ neuer MA2 ]".
	CString olVName, olNName, olText, olHeaderText;
	olHeaderText = LoadStg(IDS_STRING63493) + CString (" "); //Arbeitsgruppen-Zuweisung:*INTXT*
	if(imGroupLine1 < pomGroups->GetSize())
	{
		STFDATA *prlStfData = ogStfData.GetStfByUrno((*pomGroups)[imGroupLine1].lStfUrno);
		if (prlStfData != NULL)
		{
			olText = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, prlStfData, "", "");
		}
		if (olText.GetLength())
		{
			m_G_Name1.SetWindowText(olText);
		}
		CString olTmp;
		olTmp.Format("[ %s ]",olText);
		olHeaderText += olTmp;
		m_E_GruppIs1.SetInitText((*pomGroups)[imGroupLine1].oWgpCode);
		m_E_FuncIs1.SetInitText((*pomGroups)[imGroupLine1].oPfcCode);
	}
	olHeaderText += CString(" <<< ");
	if(imGroupLine2 < pomGroups->GetSize())
	{
		STFDATA *prlStfData = ogStfData.GetStfByUrno((*pomGroups)[imGroupLine2].lStfUrno);
		if (prlStfData != NULL)
		{
			olText = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, prlStfData, "", "");
		}
		if (olText.GetLength())
		{
			m_G_Name2.SetWindowText(olText);
		}
		CString olTmp;
		olTmp.Format("[ %s ]",olText);
		olHeaderText += olTmp;
		m_E_GruppIs2.SetInitText((*pomGroups)[imGroupLine2].oWgpCode);
		m_E_FuncIs2.SetInitText((*pomGroups)[imGroupLine2].oPfcCode);

	}
	SetWindowText(olHeaderText);

	// setting the statics
	SetDlgItemText(IDC_STATIC1, LoadStg(IDS_STRING63485));	//Gruppe Ist:*INTXT*
	SetDlgItemText(IDC_STATIC9, LoadStg(IDS_STRING63485));	//Gruppe Ist:*INTXT*
	SetDlgItemText(IDC_STATIC5, LoadStg(IDS_STRING63486));	//Gruppe Soll:*INTXT*
	SetDlgItemText(IDC_STATIC7, LoadStg(IDS_STRING63486));	//Gruppe Soll:*INTXT*
	SetDlgItemText(IDC_STATIC13, LoadStg(IDS_STRING63486));	//Gruppe Soll:*INTXT*
	SetDlgItemText(IDC_STATIC15, LoadStg(IDS_STRING63486));	//Gruppe Soll:*INTXT*
	SetDlgItemText(IDC_STATIC17, LoadStg(IDS_STRING63486));	//Gruppe Soll:*INTXT*
	SetDlgItemText(IDC_STATIC2, LoadStg(IDS_STRING63487));	//Funktion Ist:*INTXT*
	SetDlgItemText(IDC_STATIC10, LoadStg(IDS_STRING63487));	//Funktion Ist:*INTXT*
	SetDlgItemText(IDC_STATIC6, LoadStg(IDS_STRING63488));	//Funktion Soll:*INTXT*
	SetDlgItemText(IDC_STATIC8, LoadStg(IDS_STRING63488));	//Funktion Soll:*INTXT*
	SetDlgItemText(IDC_STATIC14, LoadStg(IDS_STRING63488));	//Funktion Soll:*INTXT*
	SetDlgItemText(IDC_STATIC16, LoadStg(IDS_STRING63488));	//Funktion Soll:*INTXT*
	SetDlgItemText(IDC_STATIC18, LoadStg(IDS_STRING63488));	//Funktion Soll:*INTXT*
	SetDlgItemText(IDC_STATIC3, LoadStg(IDS_STRING63489));	//Stammdaten:*INTXT*
	SetDlgItemText(IDC_STATIC4, LoadStg(IDS_STRING63489));	//Stammdaten:*INTXT*
	SetDlgItemText(IDC_STATIC11, LoadStg(IDS_STRING63489));	//Stammdaten:*INTXT*
	SetDlgItemText(IDC_STATIC12, LoadStg(IDS_STRING63489));	//Stammdaten:*INTXT*
	SetDlgItemText(IDC_B_TAUSCHEN, LoadStg(IDS_STRING63490));	//&Tauschen*INTXT*
	SetDlgItemText(IDC_B_ERSETZEN, LoadStg(IDS_STRING63491));	//&Ersetzen*INTXT*
	SetDlgItemText(IDC_B_UMSETZEN, LoadStg(IDS_STRING63492));	//&Umsetzen*INTXT*
	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));				//&Abbrechen*INTXT*
	SetDlgItemText(IDC_B_INFO, LoadStg(SHIFT_B_INFO));			//&Info*INTXT*

	// Pr�fe ob Staff2 die Aktuelle Planungsstufe hat
	bool blActual = false;
	CString olSday = prmViewInfo->oSelectDate.Format("%Y%m%d");

	DRRDATA *prlDrr = NULL;

	if(prmViewInfo->oPType == "2")
		blActual = true;
	
	if(!blActual)
	{
		m_C_Tauschen1.EnableWindow(FALSE);
		m_C_Tauschen2.EnableWindow(FALSE);
		m_E_Tauschen1.EnableWindow(FALSE);
		m_E_Tauschen2.EnableWindow(FALSE);
		m_B_Tauschen.EnableWindow(FALSE);

		m_C_Ersetzen1.EnableWindow(FALSE);
		m_C_Ersetzen2.EnableWindow(FALSE);
		m_E_Ersetzen1.EnableWindow(FALSE);
		m_E_Ersetzen2.EnableWindow(FALSE);
		m_B_Ersetzen.EnableWindow(FALSE);
	}
	// END >> Pr�fe ob Staff2 die Aktuelle Planungsstufe hat

	// Fill BasicData Group and Funktion
	CString olEfct1Stamm,olEfct2Stamm,olWgpc1Stamm,olWgpc2Stamm;
	if((*pomGroups)[imGroupLine1].lStfUrno != 0 && prmViewInfo->oSelectDate.GetStatus() == COleDateTime::valid)
	{
		olEfct1Stamm = ogSpfData.GetMainPfcBySurnWithTime((*pomGroups)[imGroupLine1].lStfUrno, prmViewInfo->oSelectDate);
		olWgpc1Stamm = ogSwgData.GetWgpBySurnWithTime((*pomGroups)[imGroupLine1].lStfUrno, prmViewInfo->oSelectDate);
	}
	else
	{
		m_C_Tauschen1.EnableWindow(FALSE);
		m_C_Tauschen2.EnableWindow(FALSE);
		m_E_Tauschen1.EnableWindow(FALSE);
		m_E_Tauschen2.EnableWindow(FALSE);
		m_B_Tauschen.EnableWindow(FALSE);

		m_C_Ersetzen1.EnableWindow(FALSE);
		m_C_Ersetzen2.EnableWindow(FALSE);
		m_E_Ersetzen1.EnableWindow(FALSE);
		m_E_Ersetzen2.EnableWindow(FALSE);
		m_B_Ersetzen.EnableWindow(FALSE);
	}
	if((*pomGroups)[imGroupLine2].lStfUrno != 0 && prmViewInfo->oSelectDate.GetStatus() == COleDateTime::valid)
	{
		olEfct2Stamm = ogSpfData.GetMainPfcBySurnWithTime((*pomGroups)[imGroupLine2].lStfUrno, prmViewInfo->oSelectDate);
		olWgpc2Stamm = ogSwgData.GetWgpBySurnWithTime((*pomGroups)[imGroupLine2].lStfUrno, prmViewInfo->oSelectDate);
	}
	m_E_FuncStamm1.SetInitText(olEfct1Stamm);
	m_E_GruppStamm1.SetInitText(olWgpc1Stamm);
	m_E_FuncStamm2.SetInitText(olEfct2Stamm);
	m_E_GruppStamm2.SetInitText(olWgpc2Stamm);
	//End >> Fill BasicData Group and Funktion

	//Hat 1 eine g�ltige Schicht
	bool blTauschenErsetzen = false;

	if((*pomGroups)[imGroupLine1].lStfUrno != 0 && prmViewInfo->oSelectDate.GetStatus() == COleDateTime::valid &&
		(prmViewInfo->oPType == "2" || prmViewInfo->oPType == "3" || prmViewInfo->oPType == "4"))
	{
		// olSday ist schon bereit initialisiert und bleibt wie vorher f�r den 2. MA
		prlDrr = ogDrrData.GetDrrByKey(olSday, (*pomGroups)[imGroupLine1].lStfUrno, (*pomGroups)[imGroupLine2].oDrrn, prmViewInfo->oPType); 

		if(prlDrr != NULL)
		{
			if(strlen(prlDrr->Scod) != NULL)
			{
				blTauschenErsetzen = true; 
			}
		}
	}
	if(!blTauschenErsetzen)
	{
		m_C_Tauschen1.EnableWindow(FALSE);
		m_C_Tauschen2.EnableWindow(FALSE);
		m_E_Tauschen1.EnableWindow(FALSE);
		m_E_Tauschen2.EnableWindow(FALSE);
		m_B_Tauschen.EnableWindow(FALSE);

		m_C_Ersetzen1.EnableWindow(FALSE);
		m_C_Ersetzen2.EnableWindow(FALSE);
		m_E_Ersetzen1.EnableWindow(FALSE);
		m_E_Ersetzen2.EnableWindow(FALSE);
		m_B_Ersetzen.EnableWindow(FALSE);
	}
	//End >> Hat 1 eine g�ltige Schicht

	// Funktionen und Gruppen f�r ComboBoxen ermitteln
	CString olPfcCode1Tauschen,olPfcCode2Tauschen;
	CString olPfcCode1Ersetzen,olPfcCode2Ersetzen;
	CString olPfcCode2Umsetzen;
	CString olWgpc1Tauschen,olWgpc1Ersetzen,olWgpc2Soll;

	if((*pomGroups)[imGroupLine1].oWgpCode != "")
	{
		olPfcCode2Tauschen = (*pomGroups)[imGroupLine1]. oPfcCode;
		olPfcCode2Ersetzen = (*pomGroups)[imGroupLine1]. oPfcCode;
		olPfcCode2Umsetzen = (*pomGroups)[imGroupLine1]. oPfcCode;
		olWgpc2Soll = (*pomGroups)[imGroupLine1].oWgpCode;
	}
	else
	{
		olPfcCode2Tauschen = olEfct2Stamm;
		olPfcCode2Ersetzen = "";
		olPfcCode2Umsetzen = olEfct2Stamm;
		olWgpc2Soll = "";

		m_C_Ersetzen1.EnableWindow(FALSE);
		m_C_Ersetzen2.EnableWindow(FALSE);
		m_E_Ersetzen1.EnableWindow(FALSE);
		m_E_Ersetzen2.EnableWindow(FALSE);
		m_B_Ersetzen.EnableWindow(FALSE);
	}
	if((*pomGroups)[imGroupLine2].oWgpCode != "")
	{
		olPfcCode1Tauschen = (*pomGroups)[imGroupLine2]. oPfcCode;
		olWgpc1Tauschen = (*pomGroups)[imGroupLine2].oWgpCode;

		olPfcCode1Ersetzen = olEfct1Stamm;
		olWgpc1Ersetzen = "";
	}
	else
	{
		olPfcCode1Tauschen = olEfct1Stamm;
		olWgpc1Tauschen = "";

		olPfcCode1Ersetzen = olEfct1Stamm;
		olWgpc1Ersetzen = "";
	}
	olPfcCode1Ersetzen = olEfct1Stamm;
	// Ende >> Funktion f�r ComboBoxes ermitteln

	//Fill Group fields
	m_E_Tauschen1.SetInitText(olWgpc1Tauschen);
	m_E_Ersetzen1.SetInitText(olWgpc1Ersetzen);
	m_E_Tauschen2.SetInitText(olWgpc2Soll);
	m_E_Ersetzen2.SetInitText(olWgpc2Soll);
	m_E_Umsetzen2.SetInitText(olWgpc2Soll);
	//End >> Fill Group fields

	// Fill ComboBoxes
	m_C_Tauschen1.SetFont(&ogCourier_Regular_9);
	m_C_Ersetzen1.SetFont(&ogCourier_Regular_9);
	m_C_Tauschen2.SetFont(&ogCourier_Regular_9);
	m_C_Ersetzen2.SetFont(&ogCourier_Regular_9);
	m_C_Umsetzen2.SetFont(&ogCourier_Regular_9);

	ogPfcData.omData.Sort(ComparePfc);
	int ilPfc = ogPfcData.omData.GetSize();
	for(int i = 0; i < ilPfc; i++)
	{
		PFCDATA *prlPfc = &ogPfcData.omData[i];
		char pclTmp[50];
		sprintf( pclTmp, "%-8.8s %-40.40s", prlPfc->Fctc, prlPfc->Fctn);

		m_C_Tauschen1.AddString(pclTmp);
		m_C_Ersetzen1.AddString(pclTmp);
		m_C_Tauschen2.AddString(pclTmp);
		m_C_Ersetzen2.AddString(pclTmp);
		m_C_Umsetzen2.AddString(pclTmp);

		if(olPfcCode1Tauschen == prlPfc->Fctc)
		{
			m_C_Tauschen1.SetCurSel(i);
		}
		if(olPfcCode1Ersetzen == prlPfc->Fctc)
		{
			m_C_Ersetzen1.SetCurSel(i);
		}
		if(olPfcCode2Tauschen == prlPfc->Fctc)
		{
			m_C_Tauschen2.SetCurSel(i);
		}
		if(olPfcCode2Ersetzen == prlPfc->Fctc)
		{
			m_C_Ersetzen2.SetCurSel(i);
		}
		if(olPfcCode2Umsetzen == prlPfc->Fctc)
		{
			m_C_Umsetzen2.SetCurSel(i);
		}
	}
	//End >> Fill ComboBoxes

	if(m_C_Tauschen1.IsWindowEnabled() == FALSE)
	{
		m_C_Tauschen1.SetCurSel(-1);
		m_E_Tauschen1.SetInitText("");
	}
	if(m_C_Tauschen2.IsWindowEnabled() == FALSE)
	{
		m_C_Tauschen2.SetCurSel(-1);
		m_E_Tauschen2.SetInitText("");
	}
	if(m_C_Ersetzen1.IsWindowEnabled() == FALSE)
	{
		m_C_Ersetzen1.SetCurSel(-1);
		m_E_Ersetzen1.SetInitText("");
	}
	if(m_C_Ersetzen2.IsWindowEnabled() == FALSE)
	{
		m_C_Ersetzen2.SetCurSel(-1);
		m_E_Ersetzen2.SetInitText("");
	}
	if(m_C_Umsetzen2.IsWindowEnabled() == FALSE)
	{
		m_C_Umsetzen2.SetCurSel(-1);
		m_E_Umsetzen2.SetInitText("");
	}

	//Set up the tooltip
	m_pToolTip = new CToolTipCtrl;
	if(!m_pToolTip->Create(this))
	{
	   TRACE("Unable To create ToolTip\n");
	   return TRUE;
	}

	if (m_pToolTip->AddTool(&m_B_Umsetzen, LoadStg(IDS_STRING63496)))
	{
	   TRACE("Unable to add tooltip to the 'Umsetzen'-button.\n");
	}

	if (m_pToolTip->AddTool(&m_B_Tauschen, LoadStg(IDS_STRING63494)))
	{
	   TRACE("Unable to add tooltip to the 'Tauschen'-button.\n");
	}

	if (m_pToolTip->AddTool(&m_B_Ersetzen, LoadStg(IDS_STRING63495)))
	{
	   TRACE("Unable to add tooltip to the 'Ersetzen'-button.\n");
	}

	m_pToolTip->Activate(TRUE);

	return TRUE;
	CCS_CATCH_ALL;
	return 0;
}

//---------------------------------------------------------------------------

void DutyChangeGroupDlg::OnOK()
{
	CCS_TRY;
	CDialog::OnOK();
	CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyChangeGroupDlg::InternalCancel()
{
	CCS_TRY;
	CDialog::OnCancel();
	CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyChangeGroupDlg::OnCancel() 
{
	CCS_TRY;
	CDialog::OnCancel();
	CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyChangeGroupDlg::OnBTauschen() 
{
	CCS_TRY;
	GROUPCHANGEINFO rlGroupChangeInfo1, rlGroupChangeInfo2;
	CString olTmp;

	m_C_Tauschen1.GetWindowText(olTmp);
	olTmp = olTmp.Left(5);
	olTmp.TrimLeft();
	olTmp.TrimRight();
	rlGroupChangeInfo1.oFctc = olTmp;

	m_E_Tauschen1.GetWindowText(olTmp);
	rlGroupChangeInfo1.oWgpc    = olTmp;

	// diese Felder beibehalten
	rlGroupChangeInfo1.lStfu  = (*pomGroups)[imGroupLine1].lStfUrno;
	rlGroupChangeInfo1.oDrrn     = (*pomGroups)[imGroupLine1].oDrrn;
	
	// richtig initialisieren
	rlGroupChangeInfo1.oDay      = prmViewInfo->oSelectDate;

	// Funktionen tauschen
	m_C_Tauschen2.GetWindowText(olTmp);
	olTmp = olTmp.Left(5);
	olTmp.TrimLeft();
	olTmp.TrimRight();
	rlGroupChangeInfo2.oFctc = olTmp;

	m_E_Tauschen2.GetWindowText(olTmp);
	rlGroupChangeInfo2.oWgpc    = olTmp;

	// diese beibehalten
	rlGroupChangeInfo2.lStfu  = (*pomGroups)[imGroupLine2].lStfUrno;
	rlGroupChangeInfo2.oDrrn     = (*pomGroups)[imGroupLine2].oDrrn;

	// richtig initialisieren
	rlGroupChangeInfo2.oDay      = prmViewInfo->oSelectDate;

	ChangeGroup(&rlGroupChangeInfo1, &rlGroupChangeInfo2, pomParent->rmViewInfo.bCheckMainFuncOnly);
	OnOK();
	CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyChangeGroupDlg::OnBErsetzen() 
{
CCS_TRY;
	GROUPCHANGEINFO rlGroupChangeInfo1, rlGroupChangeInfo2;
	CString olTmp;
	
	// dieser MA fliegt raus (nach unten)
	// seine Daten werden auf Standardwerte initialisiert
	m_C_Ersetzen1.GetWindowText(olTmp);
	olTmp = olTmp.Left(5);
	olTmp.TrimLeft();
	olTmp.TrimRight();
	rlGroupChangeInfo1.oFctc = olTmp;
	
	rlGroupChangeInfo1.oWgpc    = "";
	
	rlGroupChangeInfo1.lStfu  = (*pomGroups)[imGroupLine1].lStfUrno;
	rlGroupChangeInfo1.oDrrn     = (*pomGroups)[imGroupLine1].oDrrn;
	rlGroupChangeInfo1.oDay      = prmViewInfo->oSelectDate;
	
	// einzusetzender MA
	
	m_C_Ersetzen2.GetWindowText(olTmp);
	olTmp = olTmp.Left(5);
	olTmp.TrimLeft();
	olTmp.TrimRight();
	rlGroupChangeInfo2.oFctc = olTmp;
	
	m_E_Ersetzen2.GetWindowText(olTmp);
	rlGroupChangeInfo2.oWgpc    = olTmp;
	
	rlGroupChangeInfo2.lStfu  = (*pomGroups)[imGroupLine2].lStfUrno;
	rlGroupChangeInfo2.oDrrn     = (*pomGroups)[imGroupLine2].oDrrn;
	rlGroupChangeInfo2.oDay      = prmViewInfo->oSelectDate;
	
	ChangeGroup(&rlGroupChangeInfo1, &rlGroupChangeInfo2, pomParent->rmViewInfo.bCheckMainFuncOnly);
	OnOK();
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyChangeGroupDlg::OnBUmsetzen() 
{
	CCS_TRY;
	GROUPCHANGEINFO rlGroupChangeInfo2;
	CString olTmp;

	m_C_Umsetzen2.GetWindowText(olTmp);
	olTmp = olTmp.Left(5);
	olTmp.TrimLeft();
	olTmp.TrimRight();
	rlGroupChangeInfo2.oFctc = olTmp;

	m_E_Umsetzen2.GetWindowText(olTmp);
	rlGroupChangeInfo2.oWgpc    = olTmp;

	rlGroupChangeInfo2.lStfu  = (*pomGroups)[imGroupLine2].lStfUrno;
	rlGroupChangeInfo2.oDrrn     = (*pomGroups)[imGroupLine2].oDrrn;
	rlGroupChangeInfo2.oDay      = prmViewInfo->oSelectDate;

	ChangeGroup(NULL, &rlGroupChangeInfo2, pomParent->rmViewInfo.bCheckMainFuncOnly);
	OnOK();
	CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

static int ComparePfc( const PFCDATA **e1, const PFCDATA **e2)
{
	CString ole1,ole2;
	ole1 = (**e1).Fctc;
	ole2 = (**e2).Fctc;
	return (ole1.Collate(ole2));
}


BOOL DutyChangeGroupDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (m_pToolTip != NULL)
	{
		m_pToolTip->RelayEvent(pMsg);
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
