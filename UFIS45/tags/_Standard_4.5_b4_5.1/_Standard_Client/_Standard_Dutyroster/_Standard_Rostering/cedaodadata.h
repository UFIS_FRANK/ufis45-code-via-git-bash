// CedaOdaData.h

#ifndef __CEDAPODADATA__
#define __CEDAPODADATA__
 
#include <stdafx.h>
#include <ccsglobl.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct ODADATA 
{
	CTime	 Cdat;			// Erstellungsdatum
	//char	 Ctrc[5+2];		// Vertragsart.code
	char	 Dptc[8+2];		// Organisationseinheit,code
	char	 Dura[4+2];		// Dauer (Ersatzstunden)
	CTime	 Lstu;			// Datum letzte �nderung
	char	 Prfl[1+2];		// Protokollierungskennung
	char	 Rema[60+2];	// Bemerkungen
	char	 Sdac[5+2];		// Code
	char	 Sdak[12+2];	// Kurzbezeichnung
	char	 Sdan[40+2];	// Bezeichnung
	char	 Sdas[6+2];		// SAP-Code
	char	 Tatp[1+2];		// Auswirkung auf ZK
	char	 Tsap[1+2];		// Send to SAP
	char	 Type[2+2];		// Type Intern
	char	 Upln[1+2];		// in Urlaubsplanung
	long	 Urno;			// Eindeutige Datensatz-Nr.
	char	 Usec[32+2];	// Anwender (Ersteller)
	char	 Useu[32+2];	// Anwender (letzte �nderung)
	char	 Free[1+2];		// Regul�r Arbeitsfrei
	char	 Tbsd[1+2];		// Time Based: wenn "0" - an diesem Tag ist keine andere Schicht,
							// Anfangszeit ist 00:00 oder das Ende der vorherigen Schicht 
							// (was sp�ter vorkommt), Endzeit ist 00:00 n�chsten Tages
							// wenn "1" - an diesem Tag ist/war eine andere Schicht, 
							// die Anfangs- und Endzeit sind dieser Schicht entnommen
							// (k�nnen im Tagesdialog danach ge�ndert werden)
	char	Work[1+2];		// wenn "1" - diese Abwesenheit soll in ShiftCheck wie eine Schicht behandelt werden
	char	Abfr[6];		// abwesend von		HHMM
	char	Abto[6];		// abwesend bis		HHMM
	char 	Blen[6];	 	// Pausenl�nge	HHMM
	char    Rgbc[7]; 		// color code for print-out
							

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	ODADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}

}; // end ODADataStruct

//---------------------------------------------------------------------------------------------------------
// Class 

class CedaOdaData: public CedaData
{
// Variablen
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omSdacMap;
    CMapStringToPtr omKeyMap;

    CCSPtrArray<ODADATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Funktionen
public:
    CedaOdaData();
	~CedaOdaData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(ODADATA *prpOda);
	bool InsertInternal(ODADATA *prpOda);
	bool IsValidOda(ODADATA *prpOda);
	bool Update(ODADATA *prpOda);
	bool UpdateInternal(ODADATA *prpOda);
	bool Delete(long lpUrno);
	bool DeleteInternal(ODADATA *prpOda);
	bool ReadSpecialData(CCSPtrArray<ODADATA> *popOda,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(ODADATA *prpOda);
	// pr�fen, ob <opCode> eine Abwesenheit ist und das Flag 'regul�r arbeitsfrei' gesetzt ist
	int IsODAAndIsRegularFree(CString opCode, int* pipTbsd = 0, bool* pbpWork = 0, bool* pbpIsSleepDay = 0);
	// pr�fen, ob opCode zeitbasierend ist
	int IsODAAndIsTimeBased(CString opCode);
	// die am Tag <opDate> g�ltige Default-Abwesenheit mit Flag 'regul�r arbeitsfrei' ermitteln
	ODADATA* GetDefaultFreeODA(COleDateTime opDate);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	ODADATA* GetOdaByUrno(long lpUrno);
	ODADATA* GetOdaBySdac(CString opSdac);
	ODADATA* GetOdaByKey(CString opSdac, CString opCtrc);
};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPODADATA__
