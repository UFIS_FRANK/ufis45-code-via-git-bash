// CedaBsdData.h
 
#ifndef __CEDAPBSDDATA__
#define __CEDAPBSDDATA__
 
#include <stdafx.h>
#include <basicdata.h>

#define BSD_ESBG_LENGTH		4

//---------------------------------------------------------------------------------------------------------

struct BSDDATA 
{
	char 	 	 	 Bkd1[6]; 	// Pausenl�nge
	char 	 	 	 Bkf1[6]; 	// Pausenlage von
	//bda 29.09.00: not used char 	 	 	 Bkr1[3]; 	// Pausenlage relativ zu Schichtbeginn oder absolut
	char 	 	 	 Bkt1[6]; 	// Pausenlage bis
	char 	 	 	 Bsdc[10]; 	// Code
	//bda 29.09.00: not used char 	 	 	 Bsdk[14]; 	// Kurzbezeichnung
	//bda 29.09.00: not used char 	 	 	 Bsdn[42]; 	// Bezeichnung
	//bda 29.09.00: not used char 	 	 	 Bsds[5]; 	// SAP-Code
	CTime 	  	  	 Cdat; 		// Erstellungsdatum
	//bda 29.09.00: not used char 	 	 	 Ctrc[7]; 	// Vertragsart.code
	char 	 	 	 Esbg[BSD_ESBG_LENGTH+2]; 	// Fr�hester Schichtbeginn
	char 	 	 	 Lsen[6]; 	// Sp�testes Schichtende
	CTime 	  	  	 Lstu; 		// Datum letzte �nderung
	char 	 	 	 Prfl[3]; 	// Protokollierungskennung
	char 	 	 	 Rema[62]; 	// Bemerkungen
	char 	 	 	 Sdu1[6]; 	// Regul�re Schichtdauer
	//bda 29.09.00: not used char 	 	 	 Sex1[6]; 	// M�gliche Arbeitszeitver-l�ngerung
	//bda 29.09.00: not used char 	 	 	 Ssh1[6]; 	// M�gliche Arbeitszeitverk�rzung
	char 	 	 	 Type[3]; 	// Flag (statisch/dynamisch)
	long 	 	 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 	 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 	 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 	 	 Rgsb[16]; 	// Fr�hester Schichtbeginn dynamisch	- neu - z.Zt. nicht benutzt
	COleDateTime 	 Vafr; 		// G�ltig von
	COleDateTime 	 Vato; 		// G�ltig bis
	char 	 	 	 Fctc[FCTC_LEN+2]; 	// Reg.Funktions.code					- neu
	char 	 	 	 Dpt1[8+2]; // Code aus ORGTAB - Reg.OrgEinheit.code - testen
	char			 Fdel[1+2];	//Abordnungen existieren			- neu - z.Zt. nicht benutzt
	char 	 	 	 Bewc[7]; 	// Bewertungsfaktor.code
	char 	 	 	 Rgbc[7]; 	// color code for print-out



	//DataCreated by this class
	int      IsChanged;
	BOOL	 IsSelected;
	CTime    From;
	CTime	 To;
	CTime	 BreakFrom;
	CTime	 BreakTo;
	CTime	 BreakTimeFrameFrom;
	CTime	 BreakTimeFrameTo;
	//long, CTime
	BSDDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		IsSelected = FALSE;
	// Cdat=-1,Lstu=-1; //<zB.(FIELD_DATE Felder)
	}

}; // end BSDDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaBsdData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omBsdcMap;
	CUIntArray omSelectedData;
    CCSPtrArray<BSDDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// OBsdations
public:
    CedaBsdData();
	~CedaBsdData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(BSDDATA *prpBsd);
	bool InsertInternal(BSDDATA *prpBsd);
	bool Update(BSDDATA *prpBsd);
	bool UpdateInternal(BSDDATA *prpBsd);
	bool Delete(long lpUrno);
	bool DeleteInternal(BSDDATA *prpBsd);
	bool ReadSpecialData(CCSPtrArray<BSDDATA> *popBsd,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(BSDDATA *prpBsd);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void MakeBsdSelected(BSDDATA *prpBsd, BOOL bpSelect);
	bool GetSelectedBsd(CCSPtrArray<BSDDATA> *popSelectedBsd);
	void DeSelectAll();
	BSDDATA* GetBsdByUrno(long lpUrno);
	BSDDATA* GetBsdByBsdc(CString opBsdc);
	BSDDATA* GetBsdByBsdcAndDate(CString opBsdc,CString opDay);
	BSDDATA* GetBsdByBsdcAndDate(CString opBsdc,COleDateTime opDay);
	// Pr�fen ob diese Urno f�r diesen Zeitpunkt g�ltig ist.
	// Falls nicht, versucht die Funktion eine g�ltige Schicht zu finden,
	// Falls auch das missling, wird null zur�ckgegeben.
	long CheckAndGetValidBsdCode(long ilUrno,CString olNewText,int ilCodeType,COleDateTime olDay);
	bool IsBsdCode(CString opCode);
	bool GetBkf1Time(BSDDATA* popBsd, COleDateTimeSpan& opBkf1Time);
	bool GetBkt1Time(BSDDATA* popBsd, COleDateTimeSpan& opBkt1Time);
	bool IsValidBsd(BSDDATA* popBsd);
	
	// Private methods
private:
    void PrepareBsdData(BSDDATA *prpBsdData);
	

};

//---------------------------------------------------------------------------------------------------------

extern CedaBsdData ogBsdData;

#endif //__CEDAPBSDDATA__
