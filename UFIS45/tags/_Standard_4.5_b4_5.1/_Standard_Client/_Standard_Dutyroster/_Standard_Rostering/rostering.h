// Rostering.h : main header file for the ROSTERING application
//

#if !defined(AFX_ROSTERING_H__F66E6B64_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
#define AFX_ROSTERING_H__F66E6B64_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>       // main symbols


//***************************************************************************
// CRosteringApp:
// See Rostering.cpp for the implementation of this class
//***************************************************************************

class CRosteringApp : public CWinApp
{
public:
	CRosteringApp();
	~CRosteringApp();

private:
	void InitialLoad();
	void LoadParameters();
	void CheckParameters();
	void CheckCorruptData();

	// Men�s �bersetzen
	BOOL TranslateMainMenu(HMENU hMenu);
	BOOL TranslateDocMenu(HMENU hMenu);

public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRosteringApp)
	public:
	virtual BOOL InitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CRosteringApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:

	CMultiDocTemplate* pomShiftRosterTemplate;
	CMultiDocTemplate* pomDutyRosterTemplate;
	CMultiDocTemplate* pomBSDTemplate;
};

//***************************************************************************
// About Dialog
//***************************************************************************

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_ClassLibVersion;
	CStatic	m_StcUser;
	CStatic	m_StcServer;
	CStatic	m_StcLogintime;
	CStatic	m_Copyright4;
	CStatic	m_Copyright3;
	CStatic	m_Copyright2;
	CStatic	m_Copyright1;
	CStatic	m_Server;
	CStatic	m_User;
	CStatic	m_Logintime;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROSTERING_H__F66E6B64_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
