// CedaCotData.h

#ifndef __CEDAPCOTDATA__
#define __CEDAPCOTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct COTDATA 
{
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Ctrc[7]; 	// Code
	char 	 Ctrn[42]; 	// Bezeichnung
	char 	 Dptc[8+2]; // Organisationseinheit Code
	CTime 	 Lstu;		// Datum letzte Änderung
	char 	 Misl[6]; 	// Minimale Schichtlänge
	char 	 Mxsl[6]; 	// Maximale Schichtlänge
	char 	 Nowd[3]; 	// Arbeitstage pro Woche
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char	 Regi[3];	// 'Regelmässigkeits'-Indicator 
	char 	 Rema[62]; 	// Bemerkung
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte Änderung)
	char 	 Whpw[6]; 	// Arbeitstunden pro Wochen
	char 	 Wrkd[9]; 	// Wochenarbeitstage
	char 	 Sbpa[1+2]; // Pause bezahlt (J/N)

	//DataCreated by this class
	int      IsChanged;

	COTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}


}; // end COTDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCotData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapStringToPtr omKeyMap;
    CMapStringToPtr omCtrcMap;

    CCSPtrArray<COTDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// OCotations
public:
    CedaCotData();
	~CedaCotData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(COTDATA *prpCot);
	bool InsertInternal(COTDATA *prpCot);
	bool Update(COTDATA *prpCot);
	bool UpdateInternal(COTDATA *prpCot);
	bool Delete(long lpUrno);
	bool DeleteInternal(COTDATA *prpCot);
	bool ReadSpecialData(CCSPtrArray<COTDATA> *popCot,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(COTDATA *prpCot);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	COTDATA* GetCotByUrno(long lpUrno);
	COTDATA* GetCotByCtrc(CString opCtrc);
	COTDATA* GetCotByKey(CString opCtrc, CString opDptc);
	bool IsValidCot(COTDATA* prpCot);


	// Private methods
private:
    void PrepareCotData(COTDATA *prpCotData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPCOTDATA__
