// ShiftKonsistDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//---------------------------------------------------------------------------
// Dialogfeld ShiftKonsistDlg ///////////////////////////////////////////////
//---------------------------------------------------------------------------
ShiftKonsistDlg::ShiftKonsistDlg(CWnd* pParent /*=NULL*/) : CDialog(ShiftKonsistDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShiftKonsistDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	CDialog::Create(ShiftKonsistDlg::IDD, pParent);
	pomParent = (ShiftRoster_View*)pParent;

}
//---------------------------------------------------------------------------
ShiftKonsistDlg::~ShiftKonsistDlg(void)
{
	// ge�ndert ADO 28.10.99
	//omAssignd.DeleteAll();
}
//---------------------------------------------------------------------------
void ShiftKonsistDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftKonsistDlg)
	DDX_Control(pDX, IDC_S_HEADER1, m_S_H1);
	DDX_Control(pDX, IDC_S_HEADER2, m_S_H2);
	DDX_Control(pDX, IDC_S_HEADER3, m_S_H3);
	DDX_Control(pDX, IDC_S_HEADER4, m_S_H4);
	DDX_Control(pDX, IDC_S_HEADER5, m_S_H5);
	DDX_Control(pDX, IDC_LB_LIST, m_LB_List);
	//}}AFX_DATA_MAP
}
BEGIN_MESSAGE_MAP(ShiftKonsistDlg, CDialog)
	//{{AFX_MSG_MAP(ShiftKonsistDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//---------------------------------------------------------------------------
// Behandlungsroutinen f�r Nachrichten ShiftKonsistDlg //////////////////////
//---------------------------------------------------------------------------
BOOL ShiftKonsistDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_LB_List.SetFont(&ogCourier_Regular_10);
	CString olTmp;

	SetWindowText(LoadStg(SHIFT_KONSIST_CAPTION));
	olTmp = CString(" ") + LoadStg(SHIFT_STF_NAME);
	m_S_H1.SetWindowText(olTmp);
	olTmp = CString(" ") + LoadStg(SHIFT_CAPTION);
	m_S_H2.SetWindowText(olTmp);
	olTmp = CString(" ") + LoadStg(SHIFT_G_GSP);
	m_S_H3.SetWindowText(olTmp);
	olTmp = CString(" ") + LoadStg(SHIFT_S_WEEK);
	m_S_H4.SetWindowText(olTmp);
	olTmp = CString(" ") + LoadStg(SHIFT_S_VALID);
	m_S_H5.SetWindowText(olTmp);

	CRect olRectList;
	m_LB_List.GetWindowRect(&olRectList);
	ScreenToClient(&olRectList);

	int ilListLeft   = olRectList.left;
	//int ilListTop    = olRectList.top;
	//int ilListRight  = olRectList.right;
	//int ilListBottom = olRectList.bottom;

	int ilTop		= olRectList.top - 23;
	int ilBottom	= olRectList.top - 1;
	int ilFirstLeft = ilListLeft;
	int ilLastRight = olRectList.right;
	int ilFaktor	= 8;

	int ilLeft  = ilFirstLeft;
	int ilRight = 0;
	//Gesamt-Textl�nge ca. ? Char.

	ilRight = ilLeft + (31+1)*ilFaktor - 1;
	m_S_H1.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));//Name
	ilLeft = ilRight+1;
	ilRight = ilLeft + (20+1)*ilFaktor - 1;
	m_S_H2.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));//SPL-Name
	ilLeft = ilRight+1;
	ilRight = ilLeft + (20+1)*ilFaktor - 1;
	m_S_H3.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));//GPL-Name
	ilLeft = ilRight+1;
	ilRight = ilLeft + (2+2)*ilFaktor - 1;
	m_S_H4.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));//Week
	ilLeft = ilRight+1;
	ilRight = ilLastRight;
	m_S_H5.MoveWindow(CRect(ilLeft,ilTop,ilRight,ilBottom));//Valid from/to

	ShowWindow(SW_SHOWNORMAL);

	return TRUE;
}
//---------------------------------------------------------------------------
void ShiftKonsistDlg::SetValues()
{
	omAssignd.DeleteAll();
	ASSIGNDDEMPLOYEE *prlAssigndData;
	//*** Get all assignd employees ************

	CStringArray olUrnoArray;

	int ilGSPweekIdx = ogBCD.GetFieldIndex("GSP","WEEK");
	int ilGSPstfuIdx = ogBCD.GetFieldIndex("GSP","STFU");
	int ilGSPgpluIdx = ogBCD.GetFieldIndex("GSP","GPLU");
	int ilGSPurnoIdx = ogBCD.GetFieldIndex("GSP","URNO");

	RecordSet *polGSPRecord = new RecordSet(ogBCD.GetFieldCount("GSP"));
	int ilGSPCount = ogBCD.GetDataCount("GSP");
	for(int i = 0; i < ilGSPCount; i++)
	{
		CString olSPLurno,olSPLsnam;
		CString olGPLurno,olGPLgsnm,olGPLvafr,olGPLvato;
		CString olGSPurno,olGSPweek,olSTFurnos;

		ogBCD.GetRecord("GSP",i, *polGSPRecord);
		olSTFurnos = polGSPRecord->Values[ilGSPstfuIdx];
		int ilUrnos  = ExtractItemList(olSTFurnos,&olUrnoArray,'|');
		if(ilUrnos > 0)
		{
			olGSPurno = polGSPRecord->Values[ilGSPurnoIdx];
			olGSPweek = polGSPRecord->Values[ilGSPweekIdx];
			olGPLurno = polGSPRecord->Values[ilGSPgpluIdx];

			ogBCD.GetField("GPL", "URNO", olGPLurno, "VAFR", olGPLvafr);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "VATO", olGPLvato);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "SPLU", olSPLurno);
			ogBCD.GetField("GPL", "URNO", olGPLurno, "GSNM", olGPLgsnm);

			ogBCD.GetField("SPL", "URNO", olSPLurno, "SNAM", olSPLsnam);

			for(int m=0;m<ilUrnos;m++)
			{
				prlAssigndData = new ASSIGNDDEMPLOYEE;
				prlAssigndData->Stfu    = olUrnoArray[m];

				STFDATA *prlStf = ogStfData.GetStfByUrno(atol(olUrnoArray[m]));
				if(prlStf != NULL)
				{
					prlAssigndData->StfName = pomParent->GetFormatedEmployeeName(prlStf);
					//prlAssigndData->StfName.Format("%s, %s",prlStf->Lanm,prlStf->Finm);
				}

				prlAssigndData->Splu    = olSPLurno;
				prlAssigndData->SplName = olSPLsnam;
				prlAssigndData->Gplu    = olGPLurno;
				prlAssigndData->GplName = olGPLgsnm;
				prlAssigndData->AsFr    = olGPLvafr;
				prlAssigndData->AsTo    = olGPLvato;
				prlAssigndData->Gspu    = olGSPurno;
				prlAssigndData->Week    = atoi(olGSPweek);

				omAssignd.Add(prlAssigndData);
			}
		}
	}
	delete polGSPRecord;
	//***	
	//*** Find all multi assignd employees ************
	int ilSize = omAssignd.GetSize();
	for(int x=0; x<ilSize; x++)
	{
		if(omAssignd[x].IsDouble == false)
		{
			for(int y=x+1; y<ilSize; y++)
			{
				if(omAssignd[y].IsDouble == false)
				{
					if(omAssignd[x].Stfu == omAssignd[y].Stfu)
					{
						if(omAssignd[x].AsFr <= omAssignd[y].AsTo && omAssignd[x].AsTo >= omAssignd[y].AsFr)
						{
							omAssignd[x].IsDouble = true;
							omAssignd[y].IsDouble = true;
						}
					}
				}
			}
		}
	}
	//***	
	//*** Insert all multi assignd employees ************
	//m_LB_List.LockWindowUpdate();
	m_LB_List.ResetContent();
	CString olLine,olTmpName,olTempTime;
	for(i = 0; i < ilSize; i++)
	{
		if(omAssignd[i].IsDouble == true)
		{
			CString olFr = omAssignd[i].AsFr;
			CString olTo = omAssignd[i].AsTo;
			if(olFr.GetLength() >= 8 && olTo.GetLength() >= 8)
				olTempTime.Format("%s.%s.%s - %s.%s.%s",olFr.Mid(6,2),olFr.Mid(4,2),olFr.Mid(0,4),olTo.Mid(6,2),olTo.Mid(4,2),olTo.Mid(0,4));
			else
				olTempTime = CString(".. - ..");
			olLine.Format("%-31s %-20s %-20s %02d  %-23s",omAssignd[i].StfName.Left(31),omAssignd[i].SplName.Left(20),omAssignd[i].GplName.Left(20),omAssignd[i].Week,olTempTime);
			m_LB_List.AddString(olLine);
		}
	}
	if(m_LB_List.GetCount() == 0)
		m_LB_List.AddString(LoadStg(SHIFT_KONSIST_OK));
	
	m_LB_List.SetCurSel(0);

	//m_LB_List.UnlockWindowUpdate();
	//***	
}
//---------------------------------------------------------------------------
void ShiftKonsistDlg::OnCancel() 
{
	CDialog::OnCancel();

//  ge�ndert ADO 28.10.99
//	CDialog::DestroyWindow();
//	delete this;
//	pogShiftKonsistDlg = NULL;
}
//---------------------------------------------------------------------------
