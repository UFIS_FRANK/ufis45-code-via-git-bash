// cCfgd.cpp - Class for handling Cfgloyee data
//

#include <stdafx.h>

CedaCfgData::CedaCfgData()
{                  
    // Create an array of CEDARECINFO for CFGDATA
    BEGIN_CEDARECINFO(CFGDATA, CfgDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_CHAR_TRIM(Pkno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CfgDataRecInfo)/sizeof(CfgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CfgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
	strcpy(pcmTableName,"VCD");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields, "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT");
	pcmFieldList = pcmListOfFields;

	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,
								MON_DUTYROSTER_STRING,
								MON_SHIFTROSTER_STRING,
								MON_DUTYROSTER_STATICCOLUMNS,
								MON_DUTYROSTER_DEBITROWS,
								MON_DUTYROSTER_TOTWIDTH,
								MON_DUTYROSTER_RELATION);
}

CedaCfgData::~CedaCfgData()
{
	ogDdx.UnRegister(this,NOTUSED);
	omData.DeleteAll();
	//omViews.DeleteAll();
	omUrnoMap.RemoveAll();
	omCkeyMap.RemoveAll();
	omRecInfo.DeleteAll();
	ClearAllViews();
}

bool CedaCfgData::ReadCfgData()
{
    CString olWhere;
	bool ilRc = true;


// this section is responsible for the VIEWS in charts and lists
	ilRc = true;
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    // Select data from the database	and text like 'VIEW=test_rro_test%%' 
    olWhere.Format("WHERE APPN='CCS_%s' AND (CTYP = 'VIEW-DATA' AND PKNO = '%s') ORDER BY URNO", ogAppName, ogBasicData.omUserID);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}

	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	ClearAllViews();
    for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord2(ilCountRecord,&rlCfg)) == true)
		{
			PrepareViewData(&rlCfg);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, &rlCfg);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
	}
	ClearFastSocketBuffer();	

    return true;
}

bool CedaCfgData::ReadMonitorSetup()
{
	CString olWhere;
	bool ilRc = true;
    olWhere.Format("WHERE APPN='CCS_%s' AND (CTYP = 'MONITORS' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	rmMonitorSetup.Urno = ogBasicData.GetNextUrno();
	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,
								MON_DUTYROSTER_STRING,
								MON_SHIFTROSTER_STRING,
								MON_DUTYROSTER_STATICCOLUMNS,
								MON_DUTYROSTER_DEBITROWS,
								MON_DUTYROSTER_TOTWIDTH,
								MON_DUTYROSTER_RELATION);
	
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}

	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	CFGDATA *prlCfg = new CFGDATA;
	bool blFound = true;
    for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
    {
		if ((ilRc = GetBufferRecord2(ilCountRecord,prlCfg)) == false)
		{
			blFound = false;
		}
		else
		{
			rmMonitorSetup = *prlCfg;
			rmMonitorSetup.IsChanged = DATA_UNCHANGED;
		}
	}
	ClearFastSocketBuffer();	
	delete prlCfg;

    return true;
}

int CedaCfgData::GetMonitorForWindow(CString opWindow)
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmMonitorSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(opWindow) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				CString olString = olSetupList[i];

				CString test = olString.Mid(ilPos+1);

				ilRet = atoi(test);
			}
		}
	}
	return ilRet;
}

void CedaCfgData::DeleteViewFromDiagram(CString opDiagram, CString olView)
{
	bool olRc = true;
	char pclSelection[1024]="";
	char pclDiagram[100]="";
	char pclViewName[100]="";

	strcpy(pclDiagram, opDiagram);
	strcpy(pclViewName, olView);
	//MWO TO DO CCS_FPMS ==> APPL
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE '%%VIEW=%s%%')", ogAppName, ogBasicData.omUserID, pclDiagram, pclViewName);
	olRc = CedaAction("DRT",pclSelection);
}

//
// RRO 27.03.03
// correcting crash in FILTER-section
// now it is possible to save any view size, e.g. 1000 function-URNOs
//
void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[2024]="";
	char pclData[4096];
	char pclDiagram[100]="";
	char pclText[4096]="";
	char pclTmp[4096]="";

	strcpy(pclDiagram, opDiagram);

	// to make it easier we delete all rows of the user and his configuration for the specified diagram
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s%%')", 
		ogAppName, ogBasicData.omUserID, pclDiagram,opViewName);
	
	olRc = CedaAction("DRT",pclSelection);

	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for (int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{
			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for (int j = 0; j < ilC2; j++)
			{
				if (prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if (prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						rlCfg.Vafr = CTime::GetCurrentTime();
						rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);

						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							// general view information
							sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s#", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();

							// getting values of the array and putting them together
							CString olTEXT = "";
							CString olTmp;
							for (int l = 0; l < ilC4; l++)
							{
								olTmp.Format("%s@", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								olTEXT += olTmp;
							}

							// check how many packets we need (VCDTAB.TEXT is only VARCHAR(2000)!)
							int ilStringLength = olTEXT.GetLength();
							int ilTmp = 0;
							while (ilTmp < ilStringLength)
							{
								ilTmp += 1800;
								if (ilTmp < ilStringLength)
								{
									//int ilSep = olTEXT.Find("|",ilTmp);
									/*if (ilSep > ilTmp && ilSep < (ilTmp+50))
									{
										olTEXT.Delete(ilSep);
										olTEXT.Insert(ilSep, "\n");
									}
									else
									{*/
										olTEXT.Insert(ilTmp, "\n");
									//}
									ilStringLength++;
								}
							}

							// extract the packets into an array
							CStringArray olArr;
							int ilPackets = ExtractTextLineFast(olArr, olTEXT.GetBuffer(0), "\n");

							// save every packet in the DB
							for (l = 0; l < ilPackets; l++)
							{
								rlCfg.Urno = ogBasicData.GetNextUrno();
								olTEXT = olArr[l] + "#";
								strcpy(rlCfg.Text, pclText);
								strcat(rlCfg.Text,olTEXT.GetBuffer(0));
								MakeCedaData(olListOfData,&rlCfg);
								strcpy(pclData,olListOfData);
								olRc = CedaAction("IRT","","",pclData);
							}
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, "#");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);
						olRc = CedaAction("IRT","","",pclData);
						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';
					}
				}
			}
		i = ilC1;
		}
	}
}
////////////////////////////////////////////////////////////////
// Interprets the raw data for one propertypage
BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
	char pclOriginal[2048]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
	char pclTEXT[2048]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, "#");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, "#");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(!strcmp(prpRawData->Type, "FILTER"))
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}

////////////////////////////////////////////////////////////////
//MWO: extracts the values which are comma-separated and copies them
//     into CStringArray of VIEW_TEXTDATA

// FILTER: pcpSepa = "@"
// SONST   pcpSepa = "|"
void CedaCfgData::MakeViewValues(char *pspValues, CStringArray &ropValues, char *pcpSepa )
{
	char *psp = NULL;
	char pclOriginal[2048]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, pcpSepa/*"@ oder |"*/);
	if (psp == NULL)
	{
		// fine - there is only one value
		if (ropValues.GetSize())
		{
			ropValues[ropValues.GetSize()-1] += pclOriginal;
		}
		else
		{
			ropValues.Add(pclOriginal);
		}
	}
	else
	{
		// the things are getting complicated ... (several values)
		bool blFirst = true;
		char pclPart[2048];
		char pclRest[2048];
		pclPart[0] = '\0';
		pclRest[0] = '\0';

		while (psp != NULL)
		{
			strcpy(pclRest, psp+1);
			*psp = '\0';
			strcpy(pclPart, pclOriginal);
			strcpy(pclOriginal, pclRest);

			if (strcmp(pclPart, "") != 0)
			{
				if (ropValues.GetSize() && blFirst == true)
				{
					// continue last entry
					ropValues[ropValues.GetSize()-1] += pclPart;
					blFirst = false;
				}
				else
				{
					// add a new entry
					ropValues.Add(pclPart);
					blFirst = false;
				}
			}

			psp = strstr(pclOriginal, pcpSepa/*"@ oder |"*/);
		}
		
		// don't forget to add the rest ...
		if (strlen(pclRest) > 0)
		{
			ropValues.Add(pclRest);
		}
	}
}


////////////////////////////////////////////////////////////////
// MWO: prepares the nested arrays for the CFGDATA
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, rlTextData.omValues, "@");
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, rlTypeData.omValues, "|");
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);
	}
	else
	{

		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, rlTextData.omValues, "@");
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, rlTypeData.omValues, "|");
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, rlTextData.omValues, "@");
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, rlTypeData.omValues, "|");
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA* rlTextData = new VIEW_TEXTDATA;
						rlTextData->Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, rlTextData->omValues, "@");
						prlFoundTypeData->omTextData.Add(rlTextData);
					}
					else
					{
						MakeViewValues(rlRawData.Values, prlFoundTextData->omValues, "@");
					}
				}
				else
				{

				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a view, e.g. Staffdia
VIEWDATA * CedaCfgData::FindViewData(CFGDATA *prlCfg)
{
	int ilCount = omViews.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prlCfg->Ckey)
		{
			return &omViews[i];
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////
//MWO: Evaluates the existing of a viewname, e.g. Heute, Morgen
VIEW_VIEWNAMES * CedaCfgData::FindViewNameData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == prpRawData->Name)
				{	
					return &omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a viewtyp, e.g. FILTER, GROUP
VIEW_TYPEDATA * CedaCfgData::FindViewTypeData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							return  &omViews[i].omNameData[j].omTypeData[k];
						}
					}
				}
			}
		}
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of values for FILTER
VIEW_TEXTDATA * CedaCfgData::FindViewTextData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							int ilC4 = omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == CString(prpRawData->Page))
								{
									return &omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////
//MWO: Clear all buffers allocated in omViews
void CedaCfgData::ClearAllViews()
{
	while(omViews.GetSize() > 0)
	{
		while(omViews[0].omNameData.GetSize() > 0)
		{
			while(omViews[0].omNameData[0].omTypeData.GetSize() > 0)
			{
				while(omViews[0].omNameData[0].omTypeData[0].omTextData.GetSize() > 0)
				{
					omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.RemoveAll();
					omViews[0].omNameData[0].omTypeData[0].omTextData.DeleteAt(0);
				}
				omViews[0].omNameData[0].omTypeData[0].omValues.RemoveAll();
				omViews[0].omNameData[0].omTypeData.DeleteAt(0);
			}
			omViews[0].omNameData.DeleteAt(0);
		}
		omViews.DeleteAt(0);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class and StaffTable class)

bool CedaCfgData::InsertCfg(const CFGDATA *prpCfgData)
{

    if (CfgExist(prpCfgData->Urno))
	{
        return (UpdateCfgRecord(prpCfgData));
	}
    else
	{
        return (InsertCfgRecord(prpCfgData));
	}
}

bool CedaCfgData::UpdateCfg(const CFGDATA *prpCfgData)
{
    return(UpdateCfgRecord(prpCfgData));
}


BOOL CedaCfgData::CfgExist(long lpUrno)
{
	// don't read from database anymore, just check internal array
	CFGDATA *prpData;

	return(omUrnoMap.Lookup((void *) &lpUrno,(void *&)prpData) );
}

bool CedaCfgData::InsertCfgRecord(const CFGDATA *prpCfgData)
{
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "URNO,CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // CFGCKI ~ 430 Byte
    sprintf(CfgData, "%ld,%s,%s,%s,%s,%s,%s,%s",
        prpCfgData->Urno, 
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);

   return (CedaAction("IRT", "CFGCKI", CfgFields, "", "", CfgData));
}

bool CedaCfgData::UpdateCfgRecord(const CFGDATA *prpCfgData)
{
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s,%s",
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);
	sprintf(pclSelection," where URNO = '%ld%'",prpCfgData->Urno);
	bool olRc = CedaAction("URT", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	//ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}

// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	 ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaCfgData::ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	if ((ipDDXType == BC_CFG_CHANGE) || (ipDDXType == BC_CFG_INSERT))
	{
		CFGDATA *prpCfg;
		struct BcStruct *prlCfgData;

		prlCfgData = (struct BcStruct *) vpDataPointer;
		long llUrno = GetUrnoFromSelection(prlCfgData->Selection);

		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpCfg) == TRUE)
		{
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
		}
		else
		{
			prpCfg = new CFGDATA;
			GetRecordFromItemList(prpCfg, prlCfgData->Fields,prlCfgData->Data);
			omData.Add(prpCfg);
			omCkeyMap.SetAt(prpCfg->Ckey,prpCfg);
			omUrnoMap.SetAt((void *)prpCfg->Urno,prpCfg);
			ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prpCfg);
		}
	}
}

long  CedaCfgData::GetUrnoById(char *pclWiid)
{
	int ilWhatifCount = omData.GetSize();
	for ( int i = 0; i < ilWhatifCount; i++)
	{
		if (strcmp(omData[i].Ckey,pclWiid) == 0)
		{
			return omData[i].Urno;
		}
	}
	return 0L;
}

BOOL  CedaCfgData::GetIdByUrno(long lpUrno,char *pcpWiid)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		strcpy(pcpWiid,prlCfg->Ckey);
		return TRUE;
	}
	return FALSE;
}


CFGDATA  *CedaCfgData::GetCfgByUrno(long lpUrno)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		return prlCfg;
	}
	return NULL;
}

bool CedaCfgData::AddCfg(CFGDATA *prpCfg)
{
	CFGDATA *prlCfg = new CFGDATA;
	memcpy(prlCfg,prpCfg,sizeof(CFGDATA));
	prlCfg->IsChanged = DATA_NEW;

	omData.Add(prlCfg);
	omUrnoMap.SetAt((void *)prlCfg->Urno, prlCfg);
	omCkeyMap.SetAt(prlCfg->Ckey, prlCfg); 

	ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prlCfg);
	SaveCfg(prlCfg);
    return true;
}

bool CedaCfgData::ChangeCfgData(CFGDATA *prpCfg)
{
//	int ilCountRecord;

	if (prpCfg->IsChanged == DATA_UNCHANGED)
	{
		prpCfg->IsChanged = DATA_CHANGED;
	}
	SaveCfg(prpCfg);

    return true;
}

bool CedaCfgData::DeleteCfg(long lpUrno)
{

	CFGDATA *prpCfg = GetCfgByUrno(lpUrno);
	if (prpCfg != NULL)
	{
		prpCfg->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);
		omCkeyMap.RemoveKey(prpCfg->Ckey);

		SaveCfg(prpCfg);
	}
    return true;
}

bool CedaCfgData::SaveCfg(CFGDATA *prpCfg)
{

	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[824];

	if ((prpCfg->IsChanged == DATA_UNCHANGED) || (! bgOnline))
	{
		return true; // no change, nothing to do
	}
	switch(prpCfg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpCfg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		if (olRc != true)
		{
			prpCfg->IsChanged = DATA_NEW;
			SaveCfg(prpCfg);
		}
		else
		{
			prpCfg->IsChanged = DATA_UNCHANGED;
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}

    return true;
}
