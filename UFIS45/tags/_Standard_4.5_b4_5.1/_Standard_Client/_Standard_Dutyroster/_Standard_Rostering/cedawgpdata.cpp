// CedaWgpData.cpp
 
#include <stdafx.h>

CedaWgpData ogWgpData;

void ProcessWgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaWgpData::CedaWgpData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for WGPDataStruct
	BEGIN_CEDARECINFO(WGPDATA,WGPDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Wgpc,"WGPC")
		FIELD_CHAR_TRIM	(Wgpn,"WGPN")
		FIELD_LONG		(Pgpu,"PGPU")

	END_CEDARECINFO //(WGPDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(WGPDataRecInfo)/sizeof(WGPDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WGPDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"WGP");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"CDAT,LSTU,PRFL,REMA,URNO,USEC,USEU,WGPC,WGPN,PGPU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaWgpData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaWgpData::Register(void)
{
	DdxRegister((void *)this,BC_WGP_CHANGE,	"WGPDATA", "Wgp-changed",	ProcessWgpCf);
	DdxRegister((void *)this,BC_WGP_NEW,	"WGPDATA", "Wgp-new",		ProcessWgpCf);
	DdxRegister((void *)this,BC_WGP_DELETE,	"WGPDATA", "Wgp-deleted",	ProcessWgpCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaWgpData::~CedaWgpData(void)
{
	TRACE("CedaWgpData::~CedaWgpData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaWgpData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaWgpData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omWgpcMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaWgpData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omWgpcMap.RemoveAll();
    omData.DeleteAll();
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Wgp: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		WGPDATA *prlWgp = new WGPDATA;
		if ((ilRc = GetFirstBufferRecord2(prlWgp)) == true)
		{
			if(IsValidWgp(prlWgp))
			{
				omData.Add(prlWgp);//Update omData
				omUrnoMap.SetAt((void *)prlWgp->Urno,prlWgp);
				
				CString olTmp;
				olTmp.Format("%s",prlWgp->Wgpc);
				omWgpcMap.SetAt(olTmp,prlWgp);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlWgp);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				// defect
				delete prlWgp;
			}	
		}
		else
		{
			delete prlWgp;
		}	
	}
	TRACE("Read-Wgp: %d gelesen\n",ilCountRecord-1);
        		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaWgpData::Insert(WGPDATA *prpWgp)
{
	prpWgp->IsChanged = DATA_NEW;
	if(Save(prpWgp) == false) return false; //Update Database
	InsertInternal(prpWgp);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaWgpData::InsertInternal(WGPDATA *prpWgp)
{
	ogDdx.DataChanged((void *)this, WGP_NEW,(void *)prpWgp ); //Update Viewer
	omData.Add(prpWgp);//Update omData
	omUrnoMap.SetAt((void *)prpWgp->Urno,prpWgp);

	CString olTmp;
	olTmp.Format("%s",prpWgp->Wgpc);
	omWgpcMap.SetAt(olTmp,prpWgp);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaWgpData::Delete(long lpUrno)
{
	WGPDATA *prlWgp = GetWgpByUrno(lpUrno);
	if (prlWgp != NULL)
	{
		prlWgp->IsChanged = DATA_DELETED;
		if(Save(prlWgp) == false) return false; //Update Database
		DeleteInternal(prlWgp);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaWgpData::DeleteInternal(WGPDATA *prpWgp)
{
	ogDdx.DataChanged((void *)this,WGP_DELETE,(void *)prpWgp); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpWgp->Urno);

	CString olTmp;
	olTmp.Format("%s",prpWgp->Wgpc);
	omWgpcMap.RemoveKey(olTmp);

	int ilWgpCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilWgpCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpWgp->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaWgpData::Update(WGPDATA *prpWgp)
{
	if (GetWgpByUrno(prpWgp->Urno) != NULL)
	{
		if (prpWgp->IsChanged == DATA_UNCHANGED)
		{
			prpWgp->IsChanged = DATA_CHANGED;
		}
		if(Save(prpWgp) == false) return false; //Update Database
		UpdateInternal(prpWgp);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaWgpData::UpdateInternal(WGPDATA *prpWgp)
{
	WGPDATA *prlWgp = GetWgpByUrno(prpWgp->Urno);
	if (prlWgp != NULL)
	{
		*prlWgp = *prpWgp; //Update omData
		ogDdx.DataChanged((void *)this,WGP_CHANGE,(void *)prlWgp); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

WGPDATA *CedaWgpData::GetWgpByUrno(long lpUrno)
{
	WGPDATA  *prlWgp;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlWgp) == TRUE)
	{
		return prlWgp;
	}
	return NULL;
}

//--GET-BY-WGPC--------------------------------------------------------------------------------------------

WGPDATA *CedaWgpData::GetWgpByWgpc(CString opWgpc)
{
	WGPDATA  *prlWgp;
	if(omWgpcMap.Lookup(opWgpc,(void *&)prlWgp) == TRUE)
	{
		return prlWgp;
	}
	return NULL;
}

//--GET-PGPU-BY-WGPC--------------------------------------------------------------------------------------------

long CedaWgpData::GetPgpuByWgpc(CString opWgpc)
{
	WGPDATA  *prlWgp;
	if(omWgpcMap.Lookup(opWgpc,(void *&)prlWgp) == TRUE)
	{
		return prlWgp->Pgpu;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaWgpData::ReadSpecialData(CCSPtrArray<WGPDATA> *popWgp,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popWgp != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			WGPDATA *prpWgp = new WGPDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpWgp,CString(pclFieldList))) == true)
			{
				popWgp->Add(prpWgp);
			}
			else
			{
				delete prpWgp;
			}
		}
		if(popWgp->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaWgpData::Save(WGPDATA *prpWgp)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpWgp->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpWgp->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpWgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgp->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpWgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgp->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Wgp-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessWgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogWgpData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void CedaWgpData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlWgpData;
	prlWgpData = (struct BcStruct *) vpDataPointer;
	WGPDATA *prlWgp;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlWgpData->Cmd, prlWgpData->Object, prlWgpData->Twe, prlWgpData->Selection, prlWgpData->Fields, prlWgpData->Data,prlWgpData->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	default:
		break;
	case BC_WGP_CHANGE:
		prlWgp = GetWgpByUrno(GetUrnoFromSelection(prlWgpData->Selection));
		if(prlWgp != NULL)
		{
			GetRecordFromItemList(prlWgp,prlWgpData->Fields,prlWgpData->Data);
			UpdateInternal(prlWgp);
			break;
		}
	case BC_WGP_NEW:
		prlWgp = new WGPDATA;
		GetRecordFromItemList(prlWgp,prlWgpData->Fields,prlWgpData->Data);
		InsertInternal(prlWgp);
		break;
	case BC_WGP_DELETE:
		{
			long llUrno;
			CString olSelection = (CString)prlWgpData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlWgpData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlWgp = GetWgpByUrno(llUrno);
			if (prlWgp != NULL)
			{
				DeleteInternal(prlWgp);
			}
		}
		break;
	}
}

/**********************************************************************************
Pgpu;	 	// Urno der Planungsgruppe testen
**********************************************************************************/
bool CedaWgpData::IsValidWgp(WGPDATA* popWgp)
{
	CCS_TRY;
	
	if(!ogPgpData.GetPgpByUrno(popWgp->Pgpu))
	{
		// PGP nicht gefunden!
		CString olErr;
		olErr.Format("Work Group Table entry '%s' is defect: Group Type with URNO '%d' is not found in Work Group Types Table\n",
			popWgp->Wgpc, popWgp->Pgpu);
		ogErrlog += olErr;

		if(GetDefectDataString(ogRosteringLogText, (void *)popWgp), "WGPTAB.PGPU is not found in PGPTAB")
		{
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

void CedaWgpData::GetWgpList(CListBox& ropList)
{
	int ilIndex;
	ropList.ResetContent();
	for (int i = 0; i < this->omData.GetSize(); i++)
	{
		WGPDATA *prlData = &this->omData[i];
		ilIndex = ropList.AddString(prlData->Wgpc);
		ropList.SetItemData(ilIndex,prlData->Urno);
	}
}