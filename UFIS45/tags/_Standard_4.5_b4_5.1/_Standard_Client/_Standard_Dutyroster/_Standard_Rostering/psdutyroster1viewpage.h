#ifndef AFX_PSDUTYROSTER1VIEWPAGE_H__B6867891_2D08_11D2_8016_004095434A85__INCLUDED_
#define AFX_PSDUTYROSTER1VIEWPAGE_H__B6867891_2D08_11D2_8016_004095434A85__INCLUDED_

// PSDutyRoster1ViewPage.h : Header-Datei
//
#include <stdafx.h>
#include <Ansicht.h>
#include <RosterViewPage.h>
#include <DutyRoster_View.h>
//#include "CCSEdit.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PSDutyRoster1ViewPage 

class PSDutyRoster1ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSDutyRoster1ViewPage)

// Konstruktion
public:
	PSDutyRoster1ViewPage();
	~PSDutyRoster1ViewPage();

public:
// Funktionen
	// ???
	void SetCalledFrom(CString opCalledFrom);


// Dialogfelddaten
	//{{AFX_DATA(PSDutyRoster1ViewPage)
	enum { IDD = IDD_PSDUTYROSTER };
	CButton	m_SelectMainFuncOnly;
	CButton	m_SelectAllFunc;
	CListBox	m_LB_Orgs;
	CListBox	m_LB_Functions;
	CCSEdit	m_DayFrom;
	CCSEdit	m_DayTo;
	CCSEdit	m_E_PDays;
	CCSEdit	m_E_MDays;
	CButton	m_R_Fix;
	CButton	m_R_Var;
	//}}AFX_DATA
	
// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PSDutyRoster1ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PSDutyRoster1ViewPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnFix();
	afx_msg void OnVar();
	afx_msg void OnMarkAllFunctions();
	afx_msg void OnAllOrgs();
	afx_msg void OnNoFunctions();
	afx_msg void OnNoOrgs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
// Funktionen
	// Daten lesen
	BOOL GetData();
	// Daten schreiben
	void SetData();
	void SetPValues4ViewPage(CStringArray* popValues4ViewPage);

// Daten
	// 'Ansicht Dienstplan'
	CString omCalledFrom;
private:
	CString omSelACTURNO;
	CString omSelALTURNO;
	CString omTitleStrg;
	void SetButtonText();
	void SetStatic();
	CString* GetSelectedPlanGroupFunctions();
	void RestorePlanGroupFuncSelection();
	// Daten
	// Pointer zu dem omValues der DutyRosterPropertySheet.omDutyRoster4View -
	// wegen Zugriff auf Group-Selektion
	CStringArray* pomValues4ViewPage;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSDUTYROSTER1VIEWPAGE_H__B6867891_2D08_11D2_8016_004095434A85__INCLUDED_
