#ifndef _CEDADRRDATA_H_
#define _CEDADRRDATA_H_
 
#include <stdafx.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
#include <CedaBsdData.h>	// Basisschichten, ben�tigt in CedaDrrData::SetDrrDataScodByBSD()
#include <CedaOdaData.h>	// Abwesenheiten, ben�tigt in CedaDrrData::SetDrrDataScodByODA()

///////////////////////////////////////////////////////////////////////////// 

// COMMANDS FOR MEMBER-FUNCTIONS
#define DRR_SEND_DDX	(true)
#define DRR_NO_SEND_DDX	(false)

// max. Anzahl von Planungsstufen
#define DRR_MAX_ROSL	4

// Struktur eines DRR-Datensatzes
struct DRRDATA {
	COleDateTime	Avfr;				// anwesend von
	COleDateTime	Avto;				// anwesend bis
	char			Bufu[BUFU_LEN+2]; 	// Funknummer
	COleDateTime	Cdat;				// Erstellungsdatum
	char			Drrn[DRRN_LEN+2]; 	// Schichtnummer (1-n pro Tag)
	char 			Expf[EXPF_LEN+2]; 	// Export Flag ('A'= Archive, ' '= not Archive)
	COleDateTime	Lstu;				// Datum letzte �nderung
	char 			Rema[REMA_LEN+2]; 	// Bemerkung
	char 			Rosl[ROSL_LEN+2]; 	// Planungsstufe
	char 			Ross[ROSS_LEN+2]; 	// Status der Planungsstufe ('L'=last/vorherige, 'A'=active/aktuelle, 'N'=next/n�chste)
	COleDateTime	Sbfr;				// Pausenlage von
	COleDateTime	Sbto;				// Pausenlage bis
	char 			Sblp[SBLP_LEN+2]; 	// Pausenl�nge bezahlt
	char 			Sblu[SBLU_LEN+2]; 	// Pausenl�nge unbezahlt in Minuten, z.B. = "30"
	char 			Scod[SCOD_LEN+2]; 	// Schichtcode (Bezeichner)
	char			Scoo[SCOO_LEN+2]; 	// urspr�ngliche BSD-Schichtcode (Bezeichner)
	char 			Sday[SDAY_LEN+2]; 	// Tages-Schl�ssel YYYYMMDD
	char 			Drsf[DRSF_LEN+2]; 	// gibt es einen DRS diesem DRR? 0=Nein, 1=Ja
	long 			Stfu;				// Mitarbeiter-Urno
	long 			Urno;				// Datensatz-Urno
	char 			Usec[USEC_LEN+2]; 	// Anwender (Ersteller)
	char 			Useu[USEU_LEN+2]; 	// Anwender (letzte �nderung)
	long 			Bsdu;				// BSD Urno				
	char 			Bkdp[BKDP_LEN+2]; 	// 1. Pause bezahlt Ja = x
	char			Drs1[DRS1_LEN+2]; 	// Zusatzinfo zur Schicht / vor Schicht				
	char			Drs2[DRS2_LEN+2]; 	// Prozent der Arbeitsunf�higkeit (z.B. 050Z)			
	char			Drs3[DRS3_LEN+2]; 	// Zusatzinfo zur Schicht / nach Schicht				
	char			Drs4[DRS4_LEN+2]; 	// Zusatzinfo zur Schicht / gesamt		- in Fraport: "0" - Abwesend
	char 			Fctc[FCTC_LEN+2]; 	// Funktion des Mitarbeiters
	char			Prfl[PRFL_LEN+2];	// Protocol flag

	//	DataCreated by this class
	int	 IsChanged;	// Check whether Data has Changed f�r Relaese
	bool Redrawn;
	bool IsNew;

	//	Data repaired string
	char Repaired[512];

	DRRDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		Avfr.SetStatus(COleDateTime::invalid);		// anwesend von
		Avto.SetStatus(COleDateTime::invalid);		// anwesend bis
		Cdat.SetStatus(COleDateTime::invalid);		// Erstellungsdatum
		Lstu.SetStatus(COleDateTime::invalid);		// Datum letzte �nderung
		Sbfr.SetStatus(COleDateTime::invalid);		// Pausenlage von
		Sbto.SetStatus(COleDateTime::invalid);		// Pausenlage bis
		Redrawn = true;
		IsNew = false;
		Stfu = 0;
	}
};	

// the broadcast CallBack function, has to be outside the CedaDrrData class
void ProcessDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaDrrData: kapselt den Zugriff auf die Tabelle DRR
//************************************************************************************************************************
//************************************************************************************************************************

class CedaDrrData: public CedaData
{
// Funktionen
public:
	void ReleaseRecords(CCSPtrArray<DRRDATA> *popArr = NULL, CString opActionHint = "LATE,NOLOG");
	bool IsDrrChanged(DRRDATA *popDrr1, DRRDATA *popDrr2);
	void UnRelease(COleDateTime opFrom, COleDateTime opTo, CString opLevel, CList<long,long> *opStfUrnoList);
    // Konstruktor/Destruktor
	CedaDrrData(CString opTableName = "DRR", CString opExtName = "TAB");
	~CedaDrrData();

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	int	GetMaxFieldLength(const CString& ropField);

	// DRRDATA kopieren
	void CopyDrr(DRRDATA* popDst, DRRDATA* popSrc);
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);
	// sperrt den Zugriff auf die DRRTAB durch andere Module
	bool Synchronise(CString opUnknown, bool bpActive = false);

// Broadcasts empfangen und bearbeiten
	// BC_DRR_NEW, BC_DRR_CHANGE und BC_DRR_DELETE behandeln
	void ProcessDrrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	// behandelt die Broadcasts BC_RELDRR, BC_RELOAD_MULTI_DRR und BC_RELOAD_SINGLE_DRR
	void ProcessRelDrrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Kommunikation mit CEDA
	// Kommandos an CEDA senden
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze einlesen
	bool Read(char *pspWhere, CMapPtrToPtr *popLoadStfUrnoMap = NULL, bool bpRegisterBC = true, bool bpClearData = true,bool bpOverwrite = false);
	// Datensatz mit Urno <lpUrno> lesen
	bool ReadDrrByUrno(long lpUrno, DRRDATA *prpDrr);
	// Datens�tze nach Datum gefiltert lesen
	bool ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
									CMapPtrToPtr *popLoadStfUrnoMap = NULL,
									bool bpRegisterBC = true, bool bpClearData = true);
	// gibt manipulierte Datens�tze frei und veranlasst deren Speicherung
	bool Release(CString opFrom, CString opTo, bool bpAction = false);
	// alle DRRs eines Mitarbeiters aus der internen Datenhaltung entfernen (ohne aus der DB zu l�schen!!!)
	bool RemoveInternalByStaffUrno(long lpStfUrno, bool bpRemoveDRS = true, bool bpSendBC = false);
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(DRRDATA *prpDrr, BOOL bpWithSave);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(DRRDATA *prpDrr, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(DRRDATA *prpDrr, CString opDrs2 = "");
	// liefert den Zeiger auf die interne Datenhaltung <omData>
	DRRDATA*	GetInternalData (int ipIndex) {return (DRRDATA*)omData.CPtrArray::GetAt(ipIndex);}
	// liefert die Anzahl der Datens�tze in der internen Datenhaltung <omData>
	int	GetInternalSize() {return omData.GetSize();}

// Datenfelder behandeln
	// IsValidDrr: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten DRRs
	bool IsValidDrr(DRRDATA *popDrr, DRRDATA** ppopPrevDrr=0);
	// wenn spDrs == "8", "1" oder "2" sind, ist es Teilzeit+ oder �berzeit, return true
	bool IsExtraShift(CString opDrs);
	
// Datens�tze suchen
	// DRR nach Schl�ssel (DRRN/ROSL/SDAY/STFU) suchen
	DRRDATA* GetDrrByKey(CString opSday, long lpStfu, CString opDrrn = "1",
						 CString opRosl = "2");
	// den nach Drrn n�chsten DRR zu <opSourceDrr> suchen
	DRRDATA *GetNextDrrByKey(DRRDATA *popSourceDrr);
	// ermittelt den DRR mit der h�chsten DRR-Nummer
	DRRDATA* GetDrrWithMaxDrrn(DRRDATA *popLastDrr);
	// DRR mit Planungsstatus 'aktiv' suchen
	DRRDATA* GetDrrByRoss(CString opSday, long lpStfu, CString opDrrn = "1",
						  CString opRosl = "2", CString opRoss = "A");
	// DRR mit Urno <lpUrno> suchen
	DRRDATA* GetDrrByUrno(long llUrno);
	// ermittelt die Anzahl aller DRRs pro Mitarbeiter, Planungsstufe und Tag
	int GetDrrCountByKeyWithoutDrrn(DRRDATA *popDrr) {return GetDrrListByKeyWithoutDrrn(CString(popDrr->Sday),popDrr->Stfu,CString(popDrr->Rosl));}
	// ermittelt die Anzahl aller DRRs pro Mitarbeiter, Planungsstufe und Tag und erstellt eine Liste, wenn popDrrList!=0 ist
	int GetDrrListByKeyWithoutDrrn(CString opSday, long lpStfu, CString opRosl, CCSPtrArray<DRRDATA> *popDrrList=0);
	// Liefert DRRDATA mit ROSL == "A" nach dem angegebenen Tag/Stfu zur�ck
	DRRDATA* GetActiveDrr(CString opSday, long lpStfu);
	// R�ckgabe des Namens einer Planungstufe
	CString GetTypeName(CString opType);

	// Testen ob es eine Doppeltour gibt
	bool HasDoubleTour(DRRDATA* popDrr);
	// Doppeltour zwischen diesen beiden DRRs aufl�sen.
	bool CancelDoubleTour(DRRDATA* popDrr1,bool bpShowMessage = true);

// Datensatz-bezogene Funktionen
	// darf der Datensatz vom Dienstplan bearbeitet werden (pr�ft Planungstatus)?
	bool IsEditableByDutyRoster(DRRDATA *popDrr, CString opPType);
	// weichen die Angaben zu Schicht- und Pausenzeit von den Stammdaten ab?
	bool IsDrrDifferentFromShift(DRRDATA *popDrr);
	// pr�ft, ob das Datum innerhalb der Ein/Austrittszeit des MAs liegt und fragt den Benutzer bei Bedarf
	bool IsNewShiftWithinEmployerWorkTime(long lpStfu, long lpNewShiftUrno, DRRDATA* popDrr);
	//  Gibt die n�chste g�ltige Planungstufe zur�ck.
	// Noch hardcodiert, erst ein Prototyp.
	CString GetNextRosl(CString opOldRosl);
	// vertauscht die DRR-Nummern zweier DRRs und aller anh�ngiger DRDs und DRAs
	bool SwapDrrn(DRRDATA *popDrr, CString opNewDrrn);
// Datenfelder manipulieren
	// Anwender, Zeit und Flag der letzten �nderung setzen
	void SetDrrDataLastUpdate(DRRDATA *popDrrData);

// Hilfsfunktionen zur Manipualtion von Objekten vom Typ DRRDATA und Feldkonvertierung
	// Schichtinformation ermitteln und speichern
	bool SetDrrDataScod(DRRDATA *popDrrData, bool bpCodeIsBsd, long lpNewShiftUrno,CString opNewFctc,bool bpSetEditFlag, bool bpCheckMainFuncOnly);
	// aktiven und Urlaubs-DRR ermitteln und Abwesenheit speichern
	bool SetDrrDataScodByODA(ODADATA *popOda, long lpStfUrno, CString opSday,CString opPType,bool bpDoChangeActive, CString opDrrn = "1",bool bpDoUpdate = true, ODADATA *popExcludeODA = NULL, bool bpDoConflictCheck = true);
	// DRRDATA erzeugen, initialisieren und in die Datenhaltung mit aufnehmen
	DRRDATA* CreateDrrData(long lpStfUrno, CString opDay, CString opRosl, CString opRoss = "", CString opDrrn = "1", bool bpInsertInternal = true);

	bool ReadSpecialData(CCSPtrArray<DRRDATA> *popDrr,char *pspWhere,char *pspFieldList,bool ipSYS);

	// L�scht alle DRD�s, die an diesen DRR gekoppelt sind.
	void DeleteAllDrdByDrr(DRRDATA* popDrr);
	// L�scht alle DRA�s, die an diesen DRR gekoppelt sind.
	void DeleteAllDraByDrr(DRRDATA* popDrr);
	// �ndert einen Schichtcode, L�scht nach R�ckfrage DRA und DRD Datens�tze
	long CheckBSDCode(DRRDATA* popDrr,long lpNewShiftUrno,long lpOldShiftUrno);
	// pr�ft, ob die Basisschicht <popBsd> mit einer der Schichten, die den DRRs in <popDrrList> zugeordnet sind, konkurriert
	bool CheckBsd(BSDDATA *popBsd, CCSPtrArray<DRRDATA> &popDrrList, CString opOrgList,
				  CString opSday, bool bpCheckOrg = true);

	// Nutzinformationen kopieren (Schichtcode usw.) Mit Flag ob Schichcode �beschrieben werden darf
	void CopyDrrValues(DRRDATA* popDrrDataSource,DRRDATA* popDrrDataTarget,bool bpOverwrite = true);
	// Ver�ndert ein zugeh�rigen Schichtcode in einer Doppeltour.
	bool ChangeConnectedScod(DRRDATA* popDrr1,long lpNewShiftUrno,CString opNewFctc, bool bpCheckMainFuncOnly);
	// L�scht Rosl und dazugeh�rige Felder
	bool ClearRosl(DRRDATA* popDrr, bool bpSave = true);
	// Setzt alle Nutzdaten in einem DRR zur�ck
	bool ClearDrr(DRRDATA* popDrr, bool bpSave = true,bool bpCheckForDoubleTour = true);
	// Pr�ft ob der DRR DRDs hat
	bool HasDrdData(DRRDATA* popDrr);
	// Pr�ft ob der DRR DRAs hat
	bool HasDraData(DRRDATA* popDrr);
	// Pr�fen ob die Funktionen von Schicht und Mitarbeiter zusammenpassen
	bool CheckFuncOrgContract(DRRDATA* popDrr,long lpNewShiftUrno, CString opNewFctc, bool bpCheckMainFuncOnly);

	// Made by RDR
	DRRDATA* pomLastInternChangedDrr;

	bool InitializeShiftImport(COleDateTime opDateFrom, COleDateTime opDateTo);

	// Schichtinformation aus der Tabelle BSD (Basisschichten) ermitteln und speichern
	bool SetDrrDataScodByBSD(DRRDATA *popDrrData, long lpNewShiftUrno, CString opNewFctc, bool bpSetEditFlag);
	// Schichtinformation aus der Tabelle ODA (Abwesenheiten) ermitteln und speichern
	bool SetDrrDataScodByODA(DRRDATA *popDrrData, long lpNewShiftUrno, bool bpSetEditFlag);
	// erzeugt aus dem Feld SDAY ein Objekt vom Typ COleDateTime
	bool SdayToOleDateTime(DRRDATA *prpDrr, COleDateTime &opTime);
	// wenn neu einzuf�gende DRR-BSD mit DELs verbunden ist, m�ssen DRA-s erstellt werden
	bool CheckAndSetDraIfDrrBsdDelConnected(DRRDATA *prpDrr, long opBsdUrno, CMapPtrToPtr* popDraMap=0);

	// bda: makes a formatted string with all fields of a data structure for trace and debug output
	bool GetDataFormatted(CString &pcpListOfData, DRRDATA *prpDrr); 

	// Debug Funktion
	bool TraceDrr(DRRDATA *popDrr, LPCTSTR popStr);
    // Map mit den Prim�rschl�sseln (aus SDAY, DRRN, STFU und ROSL) der Datens�tze
	CMapStringToPtr omKeyMap;
protected:	
// Funktionen
// Lesen/Schreiben von Datens�tzen
	// speichert einen einzelnen Datensatz
	bool Save(DRRDATA *prpDrr);
	// einen Broadcast DRR_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(DRRDATA *prpDrr, bool bpSendDdx,bool bpOverwrite = false);
	// einen Broadcast DRR_CHANGE versenden
	bool UpdateInternal(DRRDATA *prpDrr, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(DRRDATA *prpDrr, bool bpSendDdx = true);
	
// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);
// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Start- und Endtag f�r zu ladende Datens�tze (DRR->SDAY) einstellen, Daten vorher pr�fen
	bool CheckAndSetLoadPeriod(COleDateTime *popLoadFrom,COleDateTime *popLoadTo);
	// ermittelt die zeitliche �berschneidung zum eingestellten Zeitraum der Datenhaltung
	bool GetReloadPeriod(COleDateTime *popLoadFrom, COleDateTime *popLoadTo);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
// Datens�tze lesen/nachladen
	// Datens�tze f�r bestimmte Mitarbeiter nachladen
	bool Reload(BcStruct *prpBcStruct);
	// die in <prpBcStruct> enthaltenen Datens�tze in die interne Datenhaltung einf�gen
	bool ReloadMultiDrr(BcStruct *prpBcStruct);
	// den in <prpBcStruct> enthaltenen Datensatz in die interne Datenhaltung einf�gen
	bool ReloadSingleDrr(BcStruct *prpBcStruct);
// Datenfelder manipulieren
	// Doppeltouren: gibt den zugeh�rigen DRR des 2. Mitarbeiters zur�ck
	DRRDATA* GetConnectedDrr(DRRDATA* popDrr1);
	// verringert die DRR-Nummer (Drrn) des Datensatzes um eins, DRDs und DRAs werden ebenfalls angepasst
	bool DecreaseDrrn(CString opSday, long lpStfu, CString opRosl, int ipDrrn);
	// Erstellt aus dem Selection-String den Urno-String und findet DRR
	DRRDATA* GetDrrFromSelectionStringUrno(CString opSelection);

	bool DeleteDRA_DRDbyChangeBsd(DRRDATA* popDrr);

// Daten
    // die geladenen Datens�tze
	CCSPtrArray<DRRDATA> omData;
	// Filter: wenn nicht NULL, enth�lt diese Map die Urnos der Mitarbeiter, f�r die 
	// Daten geladen werden 
	CMapPtrToPtr *pomLoadStfuUrnoMap;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// min. und max. Tag der Ansichts-relevanten DRRs
	COleDateTime omMinDay;
	COleDateTime omMaxDay;

private:
	bool bmRemovedPRFL;
};

#endif	// _CEDADRRDATA_H_
