// CedaSorData.h

#ifndef __CEDASORDATA__
#define __CEDASORDATA__
 
#include <stdafx.h>
#include <CedaStfData.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct SORDATA 
{
	long			Urno;
	long			Surn;		// Referenz auf STF.URNO
	char			Code[8+1];	// Codereferenz ORG.CODE
	COleDateTime	Vpfr;		// Gultig von
	COleDateTime	Vpto;		// Gultig Bis
	//char			Lead[1+1];	// Leader of group (yes = 'x', no = ' ')
	//char			Odgc[8+1];	// Group Code

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SORDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SORDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSorData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SORDATA> omDataSur;		// sorted by Surn & Time
    CCSPtrArray<SORDATA> omDataOrg;	// sorted by Code & Time

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaSorData();
	~CedaSorData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SORDATA *prpSor);
	bool InsertInternal(SORDATA *prpSor);
	bool Update(SORDATA *prpSor);
	bool UpdateInternal(SORDATA *prpSor);
	bool Delete(long lpUrno);
	bool DeleteInternal(SORDATA *prpSor);
	bool ReadSpecialData(CCSPtrArray<SORDATA> *popSor,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SORDATA *prpSor);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SORDATA *GetSorByUrno(long lpUrno);
	void GetSorBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SORDATA> *popSorData);
	CString GetOrgBySurnWithTime(long lpSurn, COleDateTime opDate);
	void GetSorArrayByOrgArrayWithTime(CStringArray *popOrg, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData);
	CString GetStfWithoutOrgWithTime(COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<STFDATA> *popStfData);
	bool IsValidSor(SORDATA *prpSor);

	// Private methods
private:
    void PrepareSorData(SORDATA *prpSorData);
	int FindFirstOfSurn(long lpSurn);
	int FindFirstOfCode(LPCTSTR popCode);
};

//---------------------------------------------------------------------------------------------------------

extern CedaSorData ogSorData;

#endif //__CEDASORDATA__
