// CedaWorkTime.cpp: implementation of the CedaWorkTime class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/********************************************************************
class CWorkTimeEntry 
********************************************************************/

CWorkTimeEntry::CWorkTimeEntry()
{
	cmState = S_REGULAR;	// 0
	omSb = 0;
	cmTimeType = TIME_NORMAL;
	omTime.SetStatus(COleDateTime::invalid);
	omTime.SetStatus(COleDateTime::invalid);
}

// rundet omTime auf volle Minuten
void CWorkTimeEntry::RoundTime()
{
	if(omTime.GetStatus() != COleDateTime::valid) return;

	omTime.SetDateTime(omTime.GetYear(), omTime.GetMonth(), omTime.GetDay(), omTime.GetHour(), omTime.GetMinute(),0);
}

// rundet omSb auf volle Minuten
void CWorkTimeEntry::RoundSb()
{
	if(omSb.GetStatus() != COleDateTimeSpan::valid) return;

	omSb.SetDateTimeSpan(0,0,(int)omSb.GetTotalMinutes(),0);
}

/****************************************************
berechnet absoluten Tag

****************************************************/
int CWorkTimeEntry::GetAbsDay()
{
	int ilRet = (int)floor(omTime.m_dt);
	return ilRet;
}

/********************************************************************
class CedaWorkTime
********************************************************************/

CedaWorkTime::CedaWorkTime(int ipEName)
{
	InitWorkTimeData();
	lmStfu = 0;
	bmShiftData = false;
	pomShiftCheck = 0;
	pomErrors = 0;
	pomWarnings = 0;
	bmSaveUndo = false;
	imEName = ipEName;
}

CedaWorkTime::CedaWorkTime(long lpStfu, CedaShiftCheck* popShiftCheck, int ipEName)
{
	InitWorkTimeData();
	lmStfu = lpStfu;
	bmShiftData = false;
	pomShiftCheck = popShiftCheck;
	pomErrors = 0;
	pomWarnings = 0;
	bmSaveUndo = false;
	imEName = ipEName;
}

CedaWorkTime::CedaWorkTime(CedaWorkTime &poSrc, int ipEName)
{
	CCS_TRY;
	lmStfu = poSrc.lmStfu;
	bmShiftData = poSrc.bmShiftData;
	
	pomShiftCheck = poSrc.pomShiftCheck;
	
	pomErrors = 0;
	pomWarnings = 0;
	bmSaveUndo = false;

	omWorkTimeData.Copy(poSrc.omWorkTimeData);
	imEName = ipEName;

	CCS_CATCH_ALL;
}

CedaWorkTime::~CedaWorkTime()
{
	if(pomErrors != 0)
		delete pomErrors;
	if(pomWarnings != 0)
		delete pomWarnings;
}

CedaWorkTime& CedaWorkTime::operator =(CedaWorkTime &poSrc)
{
	CCS_TRY;
	lmStfu = poSrc.lmStfu;
	bmShiftData = poSrc.bmShiftData;
	pomShiftCheck = poSrc.pomShiftCheck;
	omWorkTimeData.Copy(poSrc.omWorkTimeData);
	return *this;
	CCS_CATCH_ALL;
	return *this;
}

/********************************************************************************************
wir legen zwei dummy Punkte an: 0- heutiges Datum -10 Jahre, 1- heutiges Datum + 10 Jahre
dadurch wird gesichert, da� selbst wenn keine einzige Schicht gespeichert ist, trotzdem die 
paar S�tze da sind - d.h. alles frei
********************************************************************************************/
void CedaWorkTime::InitWorkTimeData()
{
	CWorkTimeEntry olWTEntry;
	olWTEntry.omTime = COleDateTime::GetCurrentTime() - COleDateTimeSpan(3650,0,0,0);
	olWTEntry.RoundTime();
	olWTEntry.omSb = 0;
	olWTEntry.cmType = WTE_END;		//allerfr�hester Punkt
	olWTEntry.cmState = S_REGULAR;
	olWTEntry.lmUrno = 0;

	omWorkTimeData.Add(olWTEntry);

	olWTEntry.omTime = COleDateTime::GetCurrentTime() + COleDateTimeSpan(3650,0,0,0);
	olWTEntry.RoundTime();
	//olWTEntry.omSb = 0;
	olWTEntry.cmType = WTE_FREE;	//allersp�tester Punkt
	olWTEntry.cmState = S_REGULAR;
	//olWTEntry.lmUrno = 0;

	omWorkTimeData.Add(olWTEntry);
}

/********************************************************************************************
UpdateData() speichert �nderungen der Arbeitszeit im Datenarray
return false hei�t, das es kein Update gemacht worden ist (nicht unbedingt ein Fehler)
********************************************************************************************/
int CedaWorkTime::UpdateData(DRRDATA* prpDrr,int ipDDXType)
{
	int ilRet = RET_NO_ERROR;

	if(	ipDDXType != DRR_DELETE && 
		(!strlen(prpDrr->Scod)							||
		prpDrr->Avfr.GetStatus() != COleDateTime::valid	|| 
		prpDrr->Avto.GetStatus() != COleDateTime::valid))
	{
		if(ipDDXType == DRR_CHANGE)
		{
			ipDDXType = DRR_DELETE;		// das ist ja ein DELETE!
		}
		else
		{
			return RET_SOFTWARE_ERROR;		// Fehler
		}
	}

	if(ipDDXType == DRR_DELETE)
	{
		ilRet = UpdateDataDrrDelete(prpDrr);
	}
	else
	{
ogBackTrace += ",40_4";
		ilRet = UpdateDataDrrNewChange(prpDrr, ipDDXType);
	}

	return ilRet;
}

/****************************************************************************************
f�hrt Delete-Befehl durch
****************************************************************************************/
int CedaWorkTime::UpdateDataDrrDelete(DRRDATA* prpDrr)
{
	CWorkTimeEntry polEntry[2];
	polEntry[0].cmType = WTE_NONE;
	
	// erstes Element
	polEntry[0].lmUrno = polEntry[1].lmUrno = prpDrr->Urno;		// Urno des urspr�nglichen DRRDATA

	return LinkWTArrays(polEntry, DRR_DELETE);
}

/****************************************************************************************
f�hrt New und Change durch
****************************************************************************************/
int CedaWorkTime::UpdateDataDrrNewChange(DRRDATA* prpDrr,int ipDDXType)
{
	// wir pr�fen, ob das eine aktive Planstufe ist
	// sonst haben wir kein Interesse daran
ogBackTrace = "1";
	if(strcmp(prpDrr->Ross, "A"))
	{
		// nein
		// es kann eine �nderung der Planstufe sein, so dass diese Schicht gel�scht werden soll
		if (ipDDXType == DRR_CHANGE)
		{
			return UpdateDataDrrDelete(prpDrr);
		}
		else 
		{
			return RET_CANCEL;	// sonst kein Interesse an einer nicht aktiven Planstufe
		}
	}
ogBackTrace += ",2";
	int ilRet = RET_NO_ERROR;
	CWorkTimeEntry polEntry[2];
	polEntry[0].cmType = WTE_NONE;

	// erstes Element
	polEntry[0].lmUrno = polEntry[1].lmUrno = prpDrr->Urno;		// Urno des urspr�nglichen DRRDATA

	bool bDontLink = false;

	// zweites Element - sp�ter wird es gebraucht
	polEntry[1].cmType = WTE_END;

	int ilTbsd;
	bool blWork = false;
	bool blIsSleepDay = false;
	int ilCode = ogOdaData.IsODAAndIsRegularFree(CString(prpDrr->Scod), &ilTbsd, &blWork, &blIsSleepDay);
ogBackTrace += ",3";
	if (ilCode == CODE_IS_BSD)
	{
		// es ist eine Schicht
		if(prpDrr->Avfr == prpDrr->Avto)
		{
			// Das ist ein Dummy-Eintrag
			// f�r einen nicht FREE-DRR gleiche Zeiten haben keinen Sinn, abbrechen
			return RET_CANCEL;
		}
ogBackTrace += ",4";
		if(prpDrr->Bsdu != 0)
		{
ogBackTrace += ",5";
			// Bsdu == 0 bei k�nstlichem DRR aus ShiftCheck, jedoch in diesem Fall sind Drs1 bis Drs4 immer leer
			BSDDATA* prlBsd = ogBsdData.GetBsdByUrno(prpDrr->Bsdu);
			if(!prlBsd) return RET_SOFTWARE_ERROR; // jede Schicht mu� einen g�ltigen BSD haben
ogBackTrace += ",6";			
			COleDateTime olSday;
			
			CedaDataHelper::DateStringToOleDateTime(prpDrr->Sday,olSday);
			// pr�fen, ob der untersuchte Tag im G�ltigkeitszeitraum der Schicht liegt
			if (prlBsd->Vafr.GetStatus() == COleDateTime::valid && olSday < prlBsd->Vafr || 
				prlBsd->Vato.GetStatus() == COleDateTime::valid && olSday > prlBsd->Vato)
				return RET_SOFTWARE_ERROR;		// der Zeitraum der Schicht ist ung�ltig
ogBackTrace += ",7";			
			// ist es TeilzeitPlus?
			// check EnableTeilzeitplusQuestion
			char pclConfigString[10];
			BOOL EnableTeilzeitplusQuestion = false;
			GetPrivateProfileString(pcgAppName, "EnableOT",  "FALSE",pclConfigString, sizeof pclConfigString, pcgConfigPath);
			if (stricmp(pclConfigString,"TRUE")==0)
				EnableTeilzeitplusQuestion = true;
			if(ogDrrData.IsExtraShift(CString(prpDrr->Drs4)) && EnableTeilzeitplusQuestion)
			{
				polEntry[0].cmType = WTE_SHIFT_PLUS;	// Die gesamte Schicht ist TeilzeitPlus oder �berzeit
			} 
			else
			{
				bool blPrev = ogDrrData.IsExtraShift(CString(prpDrr->Drs1));
				bool blPost = ogDrrData.IsExtraShift(CString(prpDrr->Drs3));
				
				if(blPrev || blPost)
				{
					// Es gibt vorherige oder sp�tere Sonderschichtzeiten
					// SDAY konvertieren
					COleDateTime olSday;
					if (!ogDrrData.SdayToOleDateTime(prpDrr,olSday)) return RET_SOFTWARE_ERROR;	// bei Fehler abbrechen
					// ESBG umwandeln in Zeit
					COleDateTimeSpan olEsbgSpan;
					if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(prlBsd->Esbg,olEsbgSpan)) return RET_SOFTWARE_ERROR;	// bei Fehler abbrechen
					// Anwesend von einstellen: SDAY + ESBG
					COleDateTime olBsdFr = olSday + olEsbgSpan;
					
					//---------------------------------------------------------------------------------------------------------------------
					// AVTO einstellen aus DRR->und BSD->LSEN (sp�testes Schichtende)
					//---------------------------------------------------------------------------------------------------------------------
					// LSEN umwandeln in Zeit
					COleDateTimeSpan olLsenSpan;
					if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(prlBsd->Lsen,olLsenSpan)) return RET_SOFTWARE_ERROR;	// bei Fehler abbrechen
					// Sp�testes Schichtende fr�her als fr�hester Schichtbeginn?
					if (olLsenSpan <= olEsbgSpan)
					{
						// ja -> einen Tag zus�tzlich addieren
						olLsenSpan += COleDateTimeSpan(1,0,0,0);
					}
					// Anwesend bis einstellen: SDAY + LSEN
					COleDateTime olBsdTo = olSday + olLsenSpan;
ogBackTrace += ",8";					
					// Bsd-Zeiten einbeziehen
					blPrev = blPrev && olBsdFr > prpDrr->Avfr;
					blPost = blPost && olBsdTo < prpDrr->Avto;
					
					if(!blPrev && olBsdFr > prpDrr->Avfr)
					{
						// wenn Anfangszeit keine Sonderschicht ist, aber gr�sser als Bsd ist, m�ssen wir sie einstellen
						olBsdFr = prpDrr->Avfr;
					}
					
					if(!blPost && olBsdTo < prpDrr->Avto)
					{
						// wenn Endzeit keine Sonderschicht ist, aber gr�sser als Bsd ist, m�ssen wir sie einstellen
						olBsdTo = prpDrr->Avto;
					}
ogBackTrace += ",9";					
					if(blPrev)
					{
						// vorherige Zeit
ogBackTrace += ",10";
						SetTimesFromTo(polEntry, prpDrr->Avfr, olBsdFr, 0);
ogBackTrace += ",11";
						polEntry[0].cmType = WTE_SHIFT_PLUS;	// Diese Schicht ist TeilzeitPlus oder �berzeit
ogBackTrace += ",12";
						ilRet = LinkWTArrays(polEntry, ipDDXType);
ogBackTrace += ",13";
						if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
					}
					
					// Hauptzeit
					SetTimesFromTo(polEntry, olBsdFr, olBsdTo, prpDrr->Sblu);
					polEntry[0].cmType = WTE_SHIFT;
ogBackTrace += ",14";					
					ilRet = LinkWTArrays(polEntry, blPrev ? DRR_NEW : ipDDXType);// DRR_NEW - LinkWTArrays darf nur einmal die alten Daten l�schen
					if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
ogBackTrace += ",15";					
					if(blPost)
					{
ogBackTrace += ",16";
						// sp�tere Zeit
						SetTimesFromTo(polEntry, olBsdTo, prpDrr->Avto, 0);
ogBackTrace += ",17";
						polEntry[0].cmType = WTE_SHIFT_PLUS;	// Diese Schicht ist TeilzeitPlus oder �berzeit
						ilRet = LinkWTArrays(polEntry, DRR_NEW); // DRR_NEW - LinkWTArrays darf nur einmal die alten Daten l�schen
ogBackTrace += ",18";
						if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
					}
					bDontLink = true;
				}
			}
		}

		if (polEntry[0].cmType == WTE_NONE)
		{
			polEntry[0].cmType = WTE_SHIFT;		// niemand hat initialisiert, bleibt als normale Schicht
		}

		if (!bDontLink)
		{
ogBackTrace += ",19";
			// Anfangs- und Endzeiten setzen
			SetTimesFromTo(polEntry, prpDrr->Avfr, prpDrr->Avto, prpDrr->Sblu);
		}
ogBackTrace += ",20";
	}
	else if (ilCode == CODE_IS_ODA_FREE || ilCode == CODE_IS_ODA_REGULARFREE)
	{
		if (blIsSleepDay)
		{
			polEntry[0].cmType = WTE_SLEEP_DAY;
		}
		else
		{
			if (blWork == true)
			{
				// es ist eine Abwesenheit, die wir als Schicht behandeln m�ssen (ODA.Work == 1)
				polEntry[0].cmType = WTE_SHIFT;		// SHIFT aus einer ODA
			}
			else if (ilCode == CODE_IS_ODA_FREE)
			{
				polEntry[0].cmType = WTE_FREE;		// FREE-Schicht
			}
			else if (ilCode == CODE_IS_ODA_REGULARFREE)
			{
				polEntry[0].cmType = WTE_R_FREE;	// R_FREE-Schicht
			}

			//-------------------------------------------------------------------
			// Anfangs- und Endzeiten setzen
			if((ilTbsd == CODE_IS_ODA_TIMEBASED || blWork == true) && prpDrr->Avfr != prpDrr->Avto)
			{
				// Diese Abwesenheit hat Schichtzeiten bekommen, wir m�ssen diese Zeiten �bernehmen
				SetTimesFromTo(polEntry, prpDrr->Avfr, prpDrr->Avto, prpDrr->Sblu);
			}
		}
	}
	else
	{
		// unbekannt
		_ASSERTE(false);
		return RET_SOFTWARE_ERROR;
	}

	// Anfangs- Endezeit anpassen, wenn erforderlich
	if(polEntry[0].omTime.GetStatus() == COleDateTime::invalid)
	{
		// Wir setzen k�nstliche Anfangs- und Endzeiten:
		// Anfangszeit auf Ende der vorherigen Schicht, wenn sie an diesem Tag endet, sonst auf 00:00 Uhr 
		// dieses Tages
		// Endzeit auf 00:00 des n�chsten Tages
		polEntry[0].cmTimeType = TIME_ZERO;
		polEntry[0].omTime.SetDate(prpDrr->Avfr.GetYear(), prpDrr->Avfr.GetMonth(), prpDrr->Avfr.GetDay());
		// der zweite Zeitpunkt ist immer 00:00 n�chsten Tages
		polEntry[1].omTime = polEntry[0].omTime + COleDateTimeSpan(0,24,0,0);

		int ilTimePoint = FindPrevTimePoint(0,LONG_MAX,polEntry[0].omTime,(WTE_END | WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE | WTE_R_FREE));
		if (ilTimePoint != -1)
		{
			// es gibt Punkte zuvor
			ilTimePoint = GetTimePoint(ilTimePoint, omWorkTimeData[ilTimePoint].lmUrno,WTE_END);
			_ASSERTE(ilTimePoint != -1);
			if(ilTimePoint == -1) 
				return RET_SOFTWARE_ERROR;		// SOFTWARE_ERROR

			// check on following shift+
			if (omWorkTimeData.GetSize() > ilTimePoint+1)
			{
				if (omWorkTimeData[ilTimePoint+1].cmType == WTE_SHIFT_PLUS && omWorkTimeData[ilTimePoint+1].lmUrno == omWorkTimeData[ilTimePoint].lmUrno)
				{
					ilTimePoint = GetTimePoint(ilTimePoint+1, omWorkTimeData[ilTimePoint+1].lmUrno,WTE_END);
					_ASSERTE(ilTimePoint != -1);
					if(ilTimePoint == -1) 
						return RET_SOFTWARE_ERROR;		// SOFTWARE_ERROR
				}
			}

			if(	omWorkTimeData[ilTimePoint].omTime > polEntry[0].omTime &&
				omWorkTimeData[ilTimePoint].omTime < polEntry[0].omTime + COleDateTimeSpan(0,24,0,0))
			{
				// einen Eintrag gefunden und seine Zeit liegt innerhalb dises Tages
				polEntry[0].omTime = omWorkTimeData[ilTimePoint].omTime;
			}
		}
		else 
		{
			return RET_SOFTWARE_ERROR;	// falsche Zeitangaben in Drr!
		}
	}

	if(!bDontLink)
	{
		ilRet = LinkWTArrays(polEntry, ipDDXType);
		if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
	}

	// DRAs d�rfen nur danach bearbeitet werden, weil wenn LinkWTArrays DRRs l�scht, l�scht sie
	// auch die dazugeh�rende DRAs, auch wenn sie erst jetzt eingef�gt waren
	if(polEntry[0].cmType & WTE_DRA_ENABLED_SHIFT)
	{
		// Existieren DRAs?
		if(ogDraData.HasDraOfDrr(prpDrr))
		{
			CMapPtrToPtr olDraMap;
			int nDra = ogDraData.GetDraMapOfDrr(&olDraMap, prpDrr);
			if(nDra > 0)
			{
				DRADATA* prlDra;
				long llKey;
			
				for(POSITION pos = olDraMap.GetStartPosition();pos!=0;)
				{
					olDraMap.GetNextAssoc(pos, (void*&)llKey, (void*&)prlDra);
					ilRet = UpdateData(prlDra, DRA_CHANGE);
					if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
				}
			}
		}
	}

	return ilRet;
}

/****************************************************************************************
Dienstfunktion: berechnet Anfang- und Endzeiten und initialist CWorkTimeEntry-s Felder
****************************************************************************************/
void CedaWorkTime::SetTimesFromTo(CWorkTimeEntry* popWorkTimeEntry, COleDateTime opFrom, COleDateTime opTo, char* opSblu)
{
	//-------------------------------------------------------------------
	// Anfangs- und Endzeiten setzen
	
	// Anfangszeit setzen
	popWorkTimeEntry[0].omTime = opFrom;
	popWorkTimeEntry[0].RoundTime();
	popWorkTimeEntry[0].omSb = opTo - opFrom;
	
	// Schichtdauer berechnen
	if(opSblu != 0 && strlen(opSblu))
	{
		int ilSblu = atoi(opSblu);
		if( ilSblu > 0 ) 
		{
			popWorkTimeEntry[0].omSb -= COleDateTimeSpan(0,0,ilSblu,0);	// Pause aus DRR einbezihen
		}
	}
	popWorkTimeEntry[0].RoundSb();
	
	// Endzeit setzen
	popWorkTimeEntry[1].omTime = opTo;
	popWorkTimeEntry[1].RoundTime();
}


/********************************************************************************************
UpdateData() speichert �nderungen der Arbeitszeit im Datenarray
return false hei�t, das es kein Update gemacht worden ist (nicht unbedingt ein Fehler)
********************************************************************************************/
int CedaWorkTime::UpdateData(DRADATA* prpDra,int ipDDXType)
{
	CWorkTimeEntry polEntry[2];
	polEntry[0].lmUrno = polEntry[1].lmUrno = prpDra->Urno;	


	if(ipDDXType != DRA_DELETE && 
		(prpDra->Abfr.GetStatus() != COleDateTime::valid || 
		prpDra->Abto.GetStatus() != COleDateTime::valid ||
		prpDra->Abfr == prpDra->Abto))
	{
		if(ipDDXType == DRA_CHANGE)
		{
			ipDDXType = DRA_DELETE;
		}
		else
		{
			return RET_SOFTWARE_ERROR;
		}
	}

	if( ipDDXType != DRA_DELETE )
	{
		// erstes Element
		polEntry[0].omTime = prpDra->Abfr;
		polEntry[0].RoundTime();
		polEntry[0].cmType = WTE_DRA_FREE;
		
		// zweites Element
		polEntry[1].omTime = prpDra->Abto;
		polEntry[1].RoundTime();
		polEntry[1].cmType = WTE_END;
	}

	return LinkWTArrays(polEntry, ipDDXType);
}

/**********************************************************************************************
Update omWorkTimeData durch die Elemente der popNewWorkTimeData
ipDDXType = DRR_CHANGE, DRR_NEW, DRR_DELETE, DRA_CHANGE, DRA_NEW, DRA_DELETE 
popWorkTimeEntry hat immer 2 Punkte

Hier gehen wir davon aus, da� die Punkte immer aufsteigend sortiert sind - 
wichtig f�r die richtige Updatereihenfolge. 
die Punkte m�ssen einzeln upgedatet werden, weil wir nicht wissen, ob in der omWorkTimeData 
die Punkte zusammen oder getrennt stehen werden
return false wenn kein Einf�gen stattgefunden hat
**********************************************************************************************/
int CedaWorkTime::LinkWTArrays(CWorkTimeEntry* popWorkTimeEntry, int ipDDXType)
{
	CCS_TRY;
	popWorkTimeEntry[0].RoundTime();
	popWorkTimeEntry[0].RoundSb();
	popWorkTimeEntry[1].RoundTime();

	int ilRet = RET_NO_ERROR;

#ifdef TRACE_ALL_CHECKFUNC
	bool bTraced = false;
#endif TRACE_ALL_CHECKFUNC
	switch(ipDDXType)
	{
	case DRR_DELETE:
	case DRA_DELETE:
		DeletePoints(popWorkTimeEntry, ipDDXType);
		break;
	case DRR_NEW:
	case DRA_NEW:
		ilRet = InsertPoints(popWorkTimeEntry, ipDDXType);
		break;
	case DRR_CHANGE:
	case DRA_CHANGE:
		// CHANGE = DELETE + NEW
		DeletePoints(popWorkTimeEntry, ipDDXType);
		ilRet = InsertPoints(popWorkTimeEntry, ipDDXType);

		if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR)
		{
#ifdef TRACE_ALL_CHECKFUNC
			// TRACE detaillierte Ausgabe
			if(lmStfu==SHOW_MA_URNO || SHOW_MA_URNO==0)
			{
				int iSize = omWorkTimeData.GetSize();
				TRACE("CedaWorkTime::CheckIt Ergebnis: lmStfu: %d, WorkTimeData-Size: %d, Auflistung:\n", lmStfu, iSize);
				for(int iTrace=0; iTrace<iSize; iTrace++)
				{
					CString olTime = omWorkTimeData[iTrace].omTime.Format("%d.%m.%y %H:%M");
					CString olSb = omWorkTimeData[iTrace].omSb.Format("%H:%M");
					CString olType;
					CString olLess = "";
					switch(omWorkTimeData[iTrace].cmType)
					{
					case WTE_NONE		:		olType = "NONE    ";	break;
					case WTE_END		:		olType = "END     ";	break;
					case WTE_SHIFT		:		olType = "SHIFT   ";	break;
					case WTE_SHIFT_PLUS	:		olType = "SHIFT+  ";	break;
					case WTE_FREE		:		olType = "FREE    ";	break;
					case WTE_R_FREE		:		olType = "R_FREE  ";	break;
					case WTE_DRA_FREE	:		olType = "DRA_FREE";	break;
					case WTE_SLEEP_DAY	:		olType = "SLEEPDAY";	break;
					default:
						break;
					}
					
					CString olState;
					switch(omWorkTimeData[iTrace].cmState)
					{
					case S_REGULAR :	olState = "reg";	break;
					case S_DELETED :	olState = "del";	break;
					case S_INSERTED:	olState = "ins";	break;
					}
					// Name
					CString olName = "";
					STFDATA *prlStf;
					prlStf = ogStfData.GetStfByUrno(lmStfu);
					
					if (prlStf > NULL)
					{
						olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
					}
					
					if(iTrace>0 && omWorkTimeData[iTrace].cmType == omWorkTimeData[iTrace-1].cmType && 
						omWorkTimeData[iTrace].cmState != S_DELETED && omWorkTimeData[iTrace-1].cmState != S_DELETED)
						olLess = "Der Typ ist gleich, wie vorheriger!!!";
					if(iTrace>0 && omWorkTimeData[iTrace].omTime < omWorkTimeData[iTrace-1].omTime && 
						omWorkTimeData[iTrace].cmState != S_DELETED && omWorkTimeData[iTrace-1].cmState != S_DELETED)
						olLess += " Zeit ist kleiner als vorherige!!!";
					
					TRACE("%s \t%d \t %s\t, %s, \t%s, \t%s \t%d \t%d \t%s \n", olName, iTrace, (LPCTSTR)olTime, (LPCTSTR)olSb, olType, olState, omWorkTimeData[iTrace].lmUrno, omWorkTimeData[iTrace].cmTimeType, olLess);
					//Sleep(5);
				}
			}
			bTraced = true;
#endif TRACE_ALL_CHECKFUNC
			
			RestoreAllRemovedAndInserted();
		}

		break;
	}

	// Fehlermeldungen ausgeben
	if(ilRet == RET_ERROR || ilRet == RET_WARNING)
	{
		bool blIsError =	(pomShiftCheck->imOutput & OUT_ERROR_AS_ERROR) && ilRet == RET_ERROR ||
					(pomShiftCheck->imOutput & OUT_WARNING_AS_ERROR) && ilRet == RET_WARNING;

		if(blIsError)
		{
			// MessageBox ausgeben
			ShowErrorBox();
		}
	}


#ifdef TRACE_ALL_CHECKFUNC
	// TRACE detaillierte Ausgabe
	if(!bTraced && (lmStfu==SHOW_MA_URNO || SHOW_MA_URNO==0))
	{
		int iSize = omWorkTimeData.GetSize();
		TRACE("CedaWorkTime::CheckIt Ergebnis: lmStfu: %d, WorkTimeData-Size: %d, Auflistung:\n", lmStfu, iSize);
		for(int iTrace=0; iTrace<iSize; iTrace++)
		{
			CString olTime = omWorkTimeData[iTrace].omTime.Format("%d.%m.%y %H:%M");
			CString olSb = omWorkTimeData[iTrace].omSb.Format("%H:%M");
			CString olType;
			CString olLess = "";
			switch(omWorkTimeData[iTrace].cmType)
			{
			case WTE_NONE		:		olType = "NONE    ";	break;
			case WTE_END		:		olType = "END     ";	break;
			case WTE_SHIFT		:		olType = "SHIFT   ";	break;
			case WTE_SHIFT_PLUS	:		olType = "SHIFT+  ";	break;
			case WTE_FREE		:		olType = "FREE    ";	break;
			case WTE_R_FREE		:		olType = "R_FREE  ";	break;
			case WTE_DRA_FREE	:		olType = "DRA_FREE";	break;
			case WTE_SLEEP_DAY	:		olType = "SLEEPDAY";	break;
			default:
				break;
			}
			
			CString olState;
			switch(omWorkTimeData[iTrace].cmState)
			{
			case S_REGULAR :	olState = "reg";	break;
			case S_DELETED :	olState = "del";	break;
			case S_INSERTED:	olState = "ins";	break;
			}
			// Name
			CString olName = "";
			STFDATA *prlStf;
			prlStf = ogStfData.GetStfByUrno(lmStfu);

			if (prlStf > NULL)
			{
				olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
			}

			if(iTrace>0 && omWorkTimeData[iTrace].cmType == omWorkTimeData[iTrace-1].cmType && 
				omWorkTimeData[iTrace].cmState != S_DELETED && omWorkTimeData[iTrace-1].cmState != S_DELETED)
				olLess = "Der Typ ist gleich, wie vorheriger!!!";
			if(iTrace>0 && omWorkTimeData[iTrace].omTime < omWorkTimeData[iTrace-1].omTime && 
				omWorkTimeData[iTrace].cmState != S_DELETED && omWorkTimeData[iTrace-1].cmState != S_DELETED)
				olLess += " Zeit ist kleiner als vorherige!!!";
			
			TRACE("%s \t%d \t %s\t, %s, \t%s, \t%s \t%d \t%d \t%s \n", olName, iTrace, (LPCTSTR)olTime, (LPCTSTR)olSb, olType, olState, omWorkTimeData[iTrace].lmUrno, omWorkTimeData[iTrace].cmTimeType, olLess);
			//Sleep(5);
		}
	}
#endif TRACE_ALL_CHECKFUNC

	return ilRet;

	CCS_CATCH_ALL;
	return RET_SOFTWARE_ERROR;
}

/*****************************************************************************
L�schfunktion f�r LinkWTArrays()
*****************************************************************************/
void CedaWorkTime::DeletePoints(CWorkTimeEntry* popWorkTimeEntry, int ipDDXType)
{
	int ilStart = -1;
	int ilSize = omWorkTimeData.GetSize();

	// L�SCHEN
	if(popWorkTimeEntry[0].lmUrno != 0)
	{
		for(ilStart=0; ilStart<ilSize; ilStart++)
		{
			if(omWorkTimeData[ilStart].cmState == S_DELETED) 
				continue;
			if(omWorkTimeData[ilStart].lmUrno == popWorkTimeEntry[0].lmUrno) 
				break;
		}

		// anything to do ?
		if( ilStart == ilSize)
		{
			return;
		}
	}
	else
	{
		ilStart = -1;
	}
	
	if (ilStart == -1 && (ipDDXType == DRR_CHANGE || ipDDXType == DRR_DELETE ))
	{
		ilStart = FindFirstTimePointOfThisDay(0, LONG_MAX, popWorkTimeEntry[0].omTime, WTE_ALL_START);
	}
	
	if (ilStart != -1)
	{
		int ilStartNextSameUrno = GetTimePoint(ilStart+1, omWorkTimeData[ilStart].lmUrno, WTE_ALL_START);
		if(ilStartNextSameUrno != -1)
		{
			// wenn von kombinierten Schicht mit TZ+-Teilen auf Normalschicht gewechselt wird, sind im omWorkTimeData
			// mehr als eine Schicht mit gleichem Urno gespeichert. Alle diese Schichten m�ssen gel�scht werden
			int ilStart2NextSameUrno = GetTimePoint(ilStartNextSameUrno+1, omWorkTimeData[ilStart].lmUrno, WTE_ALL_START);
			if(ilStart2NextSameUrno != -1)
			{
				// maximum 3 mal, doch l�schen darf nur r�ckw�rts
				RemovePoints(ilStart2NextSameUrno);
			}
			RemovePoints(ilStartNextSameUrno);
		}
		
		RemovePoints(ilStart);
	}
}

/*****************************************************************************
1. wenn bmSaveUndo = true, markieren wir den Punkt, als gel�scht - f�rs Testen
sonst l�schen wir den
2. wenn ein WTE_SHIFT oder WTE_FREE gel�scht wird, korrigieren wir die Anfangszeit der nachfolgenden WTE_FREE | WTE_R_FREE
und evtl. l�schen alle DRAs
*****************************************************************************/
void CedaWorkTime::RemovePoints(int ipStart)
{
	int ilEnd = GetTimePoint(ipStart+1, omWorkTimeData[ipStart].lmUrno, WTE_END);
	_ASSERTE(ipStart<ilEnd);
	
	if(omWorkTimeData[ipStart].cmType & WTE_DRA_ENABLED_SHIFT)
	{
		// DRA
		int ilDraStart;
		for(int iy=ipStart+1; iy<ilEnd; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].cmType == WTE_DRA_FREE)
			{
				ilDraStart = iy;
				for(iy++; iy<ilEnd; iy++)
				{
					if(omWorkTimeData[iy].cmState == S_DELETED) continue;
					if(omWorkTimeData[iy].lmUrno == omWorkTimeData[ilDraStart].lmUrno)
					{
						if(bmSaveUndo)
						{
							omWorkTimeData[iy].cmState = S_DELETED;
							omWorkTimeData[ilDraStart].cmState = S_DELETED;
						}
						else
						{
							omWorkTimeData.RemoveAt(iy);
							omWorkTimeData.RemoveAt(ilDraStart);
							ilEnd -= 2;	// die Punkte sind ganz rausgeflogen
							iy-=2;	// die inneren DRA-Punkte haben wir doch gel�scht!
						}
						
						break;
					}
				}
			}
		}		
	
		if (omWorkTimeData[ipStart].cmType == WTE_SHIFT || omWorkTimeData[ipStart].cmType == WTE_FREE)
		{
			// WTE_FREE/R_FREE Anpassung
			for(iy=ilEnd+1;iy<omWorkTimeData.GetSize()-1;iy++)
			{
				if(omWorkTimeData[iy].cmState == S_DELETED) continue;
				if(omWorkTimeData[iy].cmType & WTE_ALL_START)
				{
					if(omWorkTimeData[iy].cmTimeType == TIME_ZERO && 
						omWorkTimeData[ilEnd].omTime == omWorkTimeData[iy].omTime)
					{
						// das ist der k�nstlich angepasste Anfang der FREE/R_FREE, 
						// wir setzen omTime an den Anfang des Tages
						omWorkTimeData[iy].omTime.SetDate(omWorkTimeData[iy].omTime.GetYear(), omWorkTimeData[iy].omTime.GetMonth(), omWorkTimeData[iy].omTime.GetDay());
					}
					break;
				}
			}
		}
		
	}
	
	if(bmSaveUndo)
	{
		omWorkTimeData[ilEnd].cmState = S_DELETED;
		omWorkTimeData[ipStart].cmState = S_DELETED;
	}
	else
	{
		omWorkTimeData.RemoveAt(ilEnd);
		omWorkTimeData.RemoveAt(ipStart);
	}
}

/*****************************************************************************
1. wenn bmSaveUndo = true, marktieren wir den neuen Punkt, als INSERTED f�rs Testen
um sp�ter ihn l�schen zu k�nnen
2. wenn ein WTE_SHIFT eingef�gt wird, korrigieren wir die Anfangszeit der nachfolgenden WTE_FREE | WTE_R_FREE
3. Zusammenst�sse werden abgelehnt und Ruchgabe false

  // Neue Elemente werden nach Zeit eingef�gt: immer sch�n aufsteigend
	// ACHTUNG! Es wird nicht gepr�ft, ob die Elemente eine logische kosequente Reihe bilden, nur einf�gen
	// mit einer Ausnahme von nachfolgendem FREE/R_FREE, dessen Start an das Ende der vorherigen Schicht angepasst wird
*****************************************************************************/
int CedaWorkTime::InsertPoints(CWorkTimeEntry* popWorkTimeEntry, int ipDDXType)
{
	int ilSize = omWorkTimeData.GetSize();
	if(ilSize == 2)
	{
		// Zeitinizialisierung der beiden Grund-Punkte auf +- 9,5 Jahre
		omWorkTimeData[0].omTime = popWorkTimeEntry->omTime - COleDateTimeSpan(3467,0,0,0);
		omWorkTimeData[1].omTime = popWorkTimeEntry->omTime + COleDateTimeSpan(3467,0,0,0);
	}

	SetStartOfNextFreeShift(popWorkTimeEntry);
	
	int ilStart, ilEnd;
	// ilLastCheckPoint speichert letzten relevanten Startpunkt, um eine Kollision zu erkennen
	int ilLastCheckPoint = -1;
	int ilCollisionPossibleType = WTE_NONE;

	// WTE_DRA_FREE kollidiert nur mit WTE_DRA_FREE,
	// WTE_SHIFT_PLUS ist transparent mit WTE_R_FREE, alle anderen Kombinationen kollidieren
	switch(popWorkTimeEntry[0].cmType)
	{
	default:
		break;
	case WTE_DRA_FREE:
		ilCollisionPossibleType = WTE_DRA_FREE | WTE_SLEEP_DAY;
		break;
	case WTE_SHIFT:
		ilCollisionPossibleType = WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE;	// | WTE_SLEEP_DAY;// | WTE_R_FREE;
		break;
	case WTE_SHIFT_PLUS:
		ilCollisionPossibleType = WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE;	// | WTE_SLEEP_DAY;
		break;
	case WTE_FREE:
		ilCollisionPossibleType = WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE | WTE_R_FREE | WTE_SLEEP_DAY;
		break;
	case WTE_R_FREE:
		ilCollisionPossibleType = WTE_SHIFT | WTE_FREE | WTE_R_FREE | WTE_SLEEP_DAY;
	case WTE_SLEEP_DAY:
		ilCollisionPossibleType = WTE_FREE | WTE_R_FREE | WTE_SLEEP_DAY | WTE_DRA_FREE;
		break;
	}

	// 1. Punkt
	for(ilStart=0; ilStart<ilSize; ilStart++)
	{
		if(omWorkTimeData[ilStart].cmState == S_DELETED) continue;
		if(omWorkTimeData[ilStart].omTime <= popWorkTimeEntry[0].omTime) 
		{
			if(omWorkTimeData[ilStart].cmType & ilCollisionPossibleType)
			{
				ilLastCheckPoint = ilStart;
			}
			else if(ilLastCheckPoint != -1 && 
				omWorkTimeData[ilStart].cmType == WTE_END &&
				omWorkTimeData[ilLastCheckPoint].lmUrno == omWorkTimeData[ilStart].lmUrno)
			{
				// das Ende vom vorher gemerkten Startpunkt, l�schen
				ilLastCheckPoint = -1;
			}
			
			continue;
		}
		break;
	}
	
	// Pr�fen auf Zusammenstoss mit dem vorherigen Eintrag:
	// DRA kann mit DRA kollidieren
	// DRR: WTE_SHIFT_PLUS ist transparent mit WTE_R_FREE, alle anderen Kombinationen kollidieren
	if(ilSize > 2 && ilLastCheckPoint != -1)
	{
		return AddErrorMessage(ID_WOR_SPACE, ilLastCheckPoint, RET_ERROR, omWorkTimeData[ilLastCheckPoint].cmType);
	}
	
	// 2. Punkt
	for(ilEnd = ilStart; ilEnd<ilSize; ilEnd++)
	{
		if(omWorkTimeData[ilEnd].cmState == S_DELETED) continue;
		if(omWorkTimeData[ilEnd].omTime < popWorkTimeEntry[1].omTime) 
		{
			// Pr�fen auf Zusammenstoss mit dem nachfolgenden Eintrag
			if(	omWorkTimeData[ilEnd].cmType & ilCollisionPossibleType && 
				ilSize > 2)
			{
				// Das ist ein relevanter Start, ein Kollisions-Konflikt ist da
				return AddErrorMessage(ID_WOR_SPACE, ilEnd, RET_ERROR, ilCollisionPossibleType);
			}
			continue;
		}
		break;
	}

	if(bmSaveUndo)
	{
		popWorkTimeEntry[1].cmState = S_INSERTED;
		popWorkTimeEntry[0].cmState = S_INSERTED;
	}
	omWorkTimeData.InsertAt(ilEnd, popWorkTimeEntry[1]);
	omWorkTimeData.InsertAt(ilStart, popWorkTimeEntry[0]);

	return RET_NO_ERROR;
}

/**************************************************************************
Dienstfunktion: passt Start von nachfolgendem WTE_FREE/WRE_R_FREE/WTE_SHIFT mit TIME_ZERO an die Endzeit
der vorherigen Schicht
**************************************************************************/
void CedaWorkTime::SetStartOfNextFreeShift(CWorkTimeEntry* popWorkTimeEntry)
{
	// n�chsten WTE_FREE/R_FREE Anfang anpassen
	if(popWorkTimeEntry[0].cmType & (WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE))
	{
		// Ende dieses Tages
		COleDateTime olStartOfDay(popWorkTimeEntry[1].omTime.GetYear(), popWorkTimeEntry[1].omTime.GetMonth(), popWorkTimeEntry[1].omTime.GetDay(), 0,0,0);
		COleDateTime olEndOfDay;
		olEndOfDay = olStartOfDay + COleDateTimeSpan(1,0,0,0);
		
		for(int iy=0;iy<omWorkTimeData.GetSize()-1;iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(	(omWorkTimeData[iy].cmTimeType == TIME_ZERO) &&
				omWorkTimeData[iy].omTime >= olStartOfDay && 
				omWorkTimeData[iy].omTime < olEndOfDay)
			{
				// das ist der Anfang der FREE/R_FREE/SHIFT, den wir korrigieren sollen
				// wir setzen omTime an das Ende der neuen Schicht
				omWorkTimeData[iy].omTime = popWorkTimeEntry[1].omTime;
				break;
			}
		}
	}
}

/*****************************************************************************
l�scht alle Punkte, die als cmState == S_INSERTED markiert sind und
widerherstellt alle Punkte, die als cmState == S_DELETED markiert sind
*****************************************************************************/
void CedaWorkTime::RestoreAllRemovedAndInserted()
{
	bmSaveUndo = false;
	// Eckpunkte sind ausgeschlossen
	for(int ix=omWorkTimeData.GetSize()-2; ix>0;ix--)
	{
		switch(omWorkTimeData[ix].cmState)
		{
		default:
			continue;
		case S_INSERTED:
			omWorkTimeData.RemoveAt(ix);
			continue;
		case S_DELETED:
			omWorkTimeData[ix].cmState = S_REGULAR;
			continue;
		}
	}

	SetFreeStartTimes();
}

/********************************************************************************************
Passt alle evtl. nicht angepasste FREE-Startzeiten an die vorherige Schicht
********************************************************************************************/
void CedaWorkTime::SetFreeStartTimes()
{
	int ilLastFree = -1;
	for(int ix=omWorkTimeData.GetSize()-2; ix>0;ix--)
	{
		if(omWorkTimeData[ix].cmState == S_DELETED) continue;
		switch(omWorkTimeData[ix].cmType)
		{
		default: 
			continue;
		case WTE_FREE:
		case WTE_R_FREE:
			ilLastFree = ix;
			continue;
		case WTE_END:
			if(	ilLastFree != -1 && 
				omWorkTimeData[ix].GetAbsDay() == omWorkTimeData[ilLastFree].GetAbsDay())
			{
				omWorkTimeData[ilLastFree].omTime = omWorkTimeData[ix].omTime;
				ilLastFree = -1;
			}
			continue;
		}
	}
}

/********************************************************************************************
Findet Element mit dem n�chstkleineren oder gleichen Index, als ipEndPoint und angebenen Typ ipWTE
R�ckgabe: index zu omWorkTimeData
ipWTE = Kombination von WTE_xxx, nur diese Punkte werden analysiert
********************************************************************************************/
int CedaWorkTime::FindPrevTimePoint(int ipStartPoint, int ipEndPoint, int ipWTE)
{
	ipEndPoint = min(ipEndPoint, omWorkTimeData.GetUpperBound());
	if(ipEndPoint<0) return -1;	// leer
	if(ipStartPoint < 0) ipStartPoint = 0;

	for(int ilCount=ipEndPoint; ilCount>=ipStartPoint; ilCount--)
	{
		if(omWorkTimeData[ilCount].cmState == S_DELETED) continue;
		if(ipWTE & omWorkTimeData[ilCount].cmType) 
			return ilCount;
	}
	return -1;
}

/********************************************************************************************
Findet Element mit dem n�chstkleineren oder gleichen Zeitpunkt und angebenen Typ ipWTE
R�ckgabe: index zu omWorkTimeData
ipWTE = Kombination von WTE_xxx, nur diese Punkte werden analysiert
********************************************************************************************/
int CedaWorkTime::FindPrevTimePoint(int ipStartPoint, int ipEndPoint, COleDateTime opFind, int ipWTE)
{
	ipEndPoint = min(ipEndPoint, omWorkTimeData.GetUpperBound());
	if(ipEndPoint<0) return -1;	// leer
	if(ipStartPoint < 0) ipStartPoint = 0;

	for(int ilCount=ipEndPoint; ilCount>=ipStartPoint; ilCount--)
	{
		if(omWorkTimeData[ilCount].cmState == S_DELETED) 
			continue;
		if(omWorkTimeData[ilCount].omTime > opFind)		
			continue;
		if(ipWTE & omWorkTimeData[ilCount].cmType) 
			return ilCount;
	}
	return -1;
}

/********************************************************************************************
Sucht zwischen ipStartPoint und ipEndPoint an dem Tag opFind nach ipWTE-Punkt.
R�ckgabe: index zu omWorkTimeData
ipWTE = Kombination von WTE_xxx, nur diese Punkte werden analysiert
********************************************************************************************/
int CedaWorkTime::FindFirstTimePointOfThisDay(int ipStartPoint, int ipEndPoint, COleDateTime opFind, int ipWTE)
{
	if(opFind.GetStatus() != COleDateTime::valid)
		return -1;

	int ilAbsDay = 	(int)floor(opFind.m_dt);

	opFind.SetDate(opFind.GetYear(), opFind.GetMonth(), opFind.GetDay());

	ipEndPoint = min(ipEndPoint, omWorkTimeData.GetUpperBound());
	if(ipEndPoint<0) return -1;	// leer

	if(ipStartPoint < 0) ipStartPoint = 0;

	for(int iy=ipStartPoint; iy<=ipEndPoint; iy++)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) 
			continue;
		if(omWorkTimeData[iy].omTime < opFind)
			continue;
		if(ipWTE & omWorkTimeData[iy].cmType) 
			break;
	}

	if(iy > ipEndPoint) return -1;
	
	if(omWorkTimeData[iy].GetAbsDay() != ilAbsDay) return -1;	// nur am Stichtag ist der Fund relevant
	
	return iy;
}

/********************************************************************************************
Findet Element mit dem n�chstgr�sseren Zeitpunkt
ipStartPoint inklusive!
R�ckgabe: index zu omWorkTimeData
********************************************************************************************/
int CedaWorkTime::FindNextTimePoint(int ipStartPoint, int ipEndPoint, COleDateTime opFind, int ipWTE)
{
	ipEndPoint = min(ipEndPoint, omWorkTimeData.GetUpperBound());
	if(ipEndPoint<0) return -1;	// leer

	if(ipStartPoint < 0) ipStartPoint = 0;
	// Es darf nur linear von unten gesucht werden, alle Elemente sind aufsteigend sortiert
	for(int ilCount=ipStartPoint; ilCount<=ipEndPoint; ilCount++)
	{
		if(omWorkTimeData[ilCount].cmState == S_DELETED) 
			continue;
		if(opFind >= omWorkTimeData[ilCount].omTime)
			continue;
		if(ipWTE & omWorkTimeData[ilCount].cmType) 
			return ilCount;	
	}
	return -1;	// kein Element gefunden
}

/********************************************************************************************
Findet Element mit dem angegebenen lpUrno ab dem ipStartPoint aufw�rts
R�ckgabe: index zu omWorkTimeData oder -1, wenn kein gefunden
********************************************************************************************/
int CedaWorkTime::GetTimePoint(int ipStartPoint, long lpUrno, int ipWTE)
{
	int ilSize = omWorkTimeData.GetSize();
	if(!ilSize) return -1;	// leer
	for(int ilCount=ipStartPoint; ilCount<ilSize; ilCount++)
	{
		if(omWorkTimeData[ilCount].cmState == S_DELETED) continue;
		if(lpUrno == omWorkTimeData[ilCount].lmUrno && (ipWTE & omWorkTimeData[ilCount].cmType)) 
			return ilCount;
	}
	return -1;
}

/********************************************************************************************
die eingentliche Testfunktion
prpVoid ist ein von prpDrr oder prpDra - neuer Datensatz zum Testen,
ipDDXType - ein von Broadcasts:
DRR_CHANGE, DRR_MULTI_CHANGE, DRR_NEW, DRR_DELETE, (DRR_MULTI_CHANGE == DRR_CHANGE)
DRA_CHANGE, DRA_NEW, DRA_DELETE
DRR_CHECK - f�r "nur testen", keine �nderung
	in diesem Fall, wenn prpVoid != 0, dann ist es ein COleDateTime* und es wird an einem Tag getestet,
	sonst, wenn prpVoid == 0, testen wir alle Tage
R�ckgabe: false, wenn "verweigern"
true, wenn OK oder "Warnung"
Alle Mitteilungen werden direkt hier ausgegeben
********************************************************************************************/
bool CedaWorkTime::CheckIt(void* prpVoid, int ipDDXType, bool bpCheckWeek)
{
	int ilRet = RET_NO_ERROR;
	if(prpVoid != 0)
	{
		ilRet = CheckSingleData(prpVoid, ipDDXType, bpCheckWeek);

		// bei RET_ERROR meldet ShowErrorBox() bei Bedarf selbstst�ndig die Fehlermeldungen auf dem Schirm,
		// und durch return false verweigert die Annahme der �nderung.
		// bei RET_WARNING bleiben Fehlermeldungen im pomWarnings so lange liegen, bis ShiftCheck
		// alle Meldungen abholt.
	}
	else
	{
		CheckAllData();
	}

#ifdef TRACE_ALL_CHECKFUNC
	if(lmStfu==SHOW_MA_URNO || SHOW_MA_URNO==0)
	{
		int iSize = omWorkTimeData.GetSize();
		TRACE("CedaWorkTime::CheckIt Ergebnis: lmStfu: %d, WorkTimeData-Size: %d, Auflistung:\n", lmStfu, iSize);
		for(int iTrace=0; iTrace<iSize; iTrace++)
		{
			CString olTime = omWorkTimeData[iTrace].omTime.Format("%d.%m.%y %H:%M");
			CString olSb = omWorkTimeData[iTrace].omSb.Format("%H:%M");
			CString olType;
			CString olLess = "";
			switch(omWorkTimeData[iTrace].cmType)
			{
			case WTE_NONE		:		olType = "NONE    ";	break;
			case WTE_END		:		olType = "END     ";	break;
			case WTE_SHIFT		:		olType = "SHIFT   ";	break;
			case WTE_SHIFT_PLUS	:		olType = "SHIFT+  ";	break;
			case WTE_FREE		:		olType = "FREE    ";	break;
			case WTE_R_FREE		:		olType = "R_FREE  ";	break;
			case WTE_DRA_FREE	:		olType = "DRA_FREE";	break;
			case WTE_SLEEP_DAY	:		olType = "SLEEPDAY";	break;
			default:
				break;
			}
			
			CString olState;
			switch(omWorkTimeData[iTrace].cmState)
			{
			case S_REGULAR :	olState = "reg";	break;
			case S_DELETED :	olState = "del";	break;
			case S_INSERTED:	olState = "ins";	break;
			}
			// Name
			CString olName = "";
			STFDATA *prlStf;
			prlStf = ogStfData.GetStfByUrno(lmStfu);

			if (prlStf > NULL)
			{
				olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
			}

			if(iTrace>0 && omWorkTimeData[iTrace].cmType == omWorkTimeData[iTrace-1].cmType && 
				omWorkTimeData[iTrace].cmState != S_DELETED && omWorkTimeData[iTrace-1].cmState != S_DELETED)
				olLess = "Der Typ ist gleich, wie vorheriger!!!";
			if(iTrace>0 && omWorkTimeData[iTrace].omTime < omWorkTimeData[iTrace-1].omTime && 
				omWorkTimeData[iTrace].cmState != S_DELETED && omWorkTimeData[iTrace-1].cmState != S_DELETED)
				olLess += " Zeit ist kleiner als vorherige!!!";
			
			TRACE("%s \t%d \t %s\t, %s, \t%s, \t%s \t%d \t%d \t%s \n", olName, iTrace, (LPCTSTR)olTime, (LPCTSTR)olSb, olType, olState, omWorkTimeData[iTrace].lmUrno, omWorkTimeData[iTrace].cmTimeType, olLess);
			//Sleep(5);
		}
	}
#endif TRACE_ALL_CHECKFUNC

	return ilRet != RET_ERROR;
}

/*******************************************************************************************
Testet einen Datensatz DRRDATA oder DRADATA
prpVoid == DRRDATA*			wenn ipDDXType ist ein von DRR_NEW, _CHANGE, _DELETE
		== DRADATA*			f�r DRA_xxx
		== COleDateTime*	f�r DRR_CHECK
*******************************************************************************************/
int CedaWorkTime::CheckSingleData(void* prpVoid, int ipDDXType, bool bpCheckWeek)
{
	int ilRet = RET_NO_ERROR;
	
	if(ipDDXType != DRR_CHECK)
	{
		DRRDATA*		prlDrr = 0;
		DRADATA*		prlDra = 0;

		switch(ipDDXType)
		{
		default:
			prlDrr = (DRRDATA*)prpVoid;
			if(prlDrr == 0 || prlDrr->Urno == 0 || prlDrr->Avfr.GetStatus() != COleDateTime::valid || prlDrr->Avto.GetStatus() != COleDateTime::valid || strcmp(prlDrr->Ross, "A"))
			{
				return RET_NO_ERROR;
			}

			// prlDrr hat einen neuen Satz, den man pr�fen mu�
			// einen MA an einem Tag checken
			bmSaveUndo = true;
			// updaten, testen, zur�ck updaten
			ilRet = UpdateData(prlDrr, ipDDXType);	
			if(ilRet != RET_NO_ERROR && ilRet != RET_WARNING)
			{
				if(bmSaveUndo)
				{
					RestoreAllRemovedAndInserted();
				}
				return ilRet;
				// entweder kein Data upgedated, d.h. keine �nderungen sind notwendig, keine Pr�fung, oder ein Fehler passierte
			}
			break;
		case DRA_NEW   :
		case DRA_CHANGE:
		case DRA_DELETE:
			prlDra = (DRADATA*)prpVoid;
			if (prlDra != 0&& prlDra->Urno && prlDra->Abfr.GetStatus() == COleDateTime::valid && prlDra->Abto.GetStatus() == COleDateTime::valid)
			{
				// prlDra hat einen neuen Satz, den man pr�fen mu�
				// einzelnen MA am einzelnen Tag checken
				bmSaveUndo = true;
				// updaten, testen, zur�ck updaten
				ilRet = UpdateData(prlDra, ipDDXType);
				if(ilRet != RET_NO_ERROR && ilRet != RET_WARNING)
				{
					if(bmSaveUndo)
					{
						RestoreAllRemovedAndInserted();
					}
					return ilRet;
					// entweder kein Data upgedated, d.h. keine �nderungen sind notwendig, keine Pr�fung, oder ein Fehler passierte
				}
			}
			else 
			{
				return RET_NO_ERROR;
			}
			break;
		}
		
		// alle Tests durchlaufen
		ilRet = CheckOneDayRules(prlDrr, prlDra, GetDataChange(prlDrr, ipDDXType), bpCheckWeek);
		
		if(ilRet == RET_ERROR)
		{
			// MessageBox ausgeben
			if (ogOdaData.GetOdaBySdac(prlDrr->Scod) != NULL)
			{
				ShowErrorBox(true);
			}
			else
			{
				ShowErrorBox(false);
			}
		}
		else if(ilRet == RET_SOFTWARE_ERROR)
		{
			return ilRet;
		}

		if(bmSaveUndo)
		{
			RestoreAllRemovedAndInserted();
		}
	}
	else
	{
		DRRDATA			rlDrrData;
		int				ilTestDay;
		
		// bei DRR_CHECK suchen wir den Tag mit prlDayToCheck-Startzeit und testen ihn
		COleDateTime*	prlDayToCheck;
		prlDayToCheck = (COleDateTime*)prpVoid;

		// den Tag suchen
		ilTestDay = FindFirstTimePointOfThisDay(0, LONG_MAX, *prlDayToCheck, (WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE | WTE_R_FREE));
		if(ilTestDay == -1) return RET_CANCEL;	// Kein Eintrag, kein Fehler ;)

		int ilODType = 0;
		switch(omWorkTimeData[ilTestDay].cmType)
		{
		case WTE_SHIFT:
			ilODType = OT_DRR_SHIFT_CHANGE;
			break;
		case WTE_SHIFT_PLUS:
			ilODType = OT_DRR_SHIFT_PLUS_CHANGE;
			break;
		case WTE_FREE:
			ilODType = OT_DRR_FREE_CHANGE;
			break;
		case WTE_R_FREE:
			ilODType = OT_DRR_R_FREE_CHANGE;
			break;
		}
		
		rlDrrData.Urno = omWorkTimeData[ilTestDay].lmUrno;
		rlDrrData.Avfr = omWorkTimeData[ilTestDay].omTime;
				
		ilTestDay = GetTimePoint(ilTestDay, rlDrrData.Urno, WTE_END);
		if(ilTestDay == -1) return RET_SOFTWARE_ERROR;
		rlDrrData.Avto = omWorkTimeData[ilTestDay].omTime;
		
		ilRet = CheckOneDayRules(&rlDrrData, 0, ilODType, bpCheckWeek);
	}

	return ilRet;
}

/****************************************************************************
Wenn pomErrors nicht leer ist, werden alle gespeicherten Meldungen nacheinander
gezeigt, danach alle Meldungen gel�scht
****************************************************************************/
void CedaWorkTime::ShowErrorBox(bool bpIsAbsence)
{
	if(!pomErrors) return;
	int ilSize = pomErrors->GetSize();

	if(!pomShiftCheck->bmEnableTeilzeitplusQuestion || bpIsAbsence)
	{
		// Alle Meldungen als Fehlerwarnung mit 'OK' ausgeben
		for(int ilCount=0; ilCount<ilSize; ilCount++)
		{
			if(pomErrors->GetAt(ilCount).IsEmpty())
				continue;
			AfxMessageBox((LPCTSTR)pomErrors->GetAt(ilCount));
		}
	}
	else
	{
		// es d�rfte nur eine Fehlermeldung gespeichert werden
		// Statt einfacher Fehlermeldung fragen wir, ob man die Schicht als Teilzeit+ speischern soll
		if(ilSize >= 1)
		{
			CString olMsg;
			// in Genf kein TZ+ sondern Overtime
			if("ZRH" == ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
			{
				// ... + Soll die neue Schicht als Teilzeit Plus eingef�gt werden?*INTXT*
				olMsg = pomErrors->GetAt(0) + "\n\n" + LoadStg(IDS_STRING1656);
			}
			else
			{
				// ... + Soll die neue Schicht als �berzeit eingef�gt werden?*INTXT*
				olMsg = pomErrors->GetAt(0) + "\n\n" + LoadStg(IDS_STRING9161);
			}
			int ilRet = AfxMessageBox((LPCTSTR)olMsg, MB_YESNO | MB_ICONQUESTION);
			if(ilRet == IDYES)
			{
				pomShiftCheck->bmSaveAsTeilzeitplus = true;
			}
			else
			{
				pomShiftCheck->bmSaveAsTeilzeitplus = false;
			}
		}
	}
	pomErrors->RemoveAll();
} 

/****************************************************************************
true, wenn pomWarnings nicht leer ist (d.h. Warnungen vorliegen)
****************************************************************************/
bool CedaWorkTime::IsWarning()
{
	return (pomWarnings != 0 && pomWarnings->GetSize() > 0);
}

/*******************************************************************************************
Testet alle Datens�tze
*******************************************************************************************/
void CedaWorkTime::CheckAllData()
{
	_ASSERTE(!pomShiftCheck->bmEnableTeilzeitplusQuestion);	// Unsinn bei AllData
	
	// jeden Satz finden, testen	
	int ix[6];		
	// ix[0]	Start	vorheriger Schicht
	// ix[1]	Ende	vorheriger Schicht
	// ix[2]	Start	dieser Schicht
	// ix[3]	Ende	dieser Schicht
	// ix[4]	Start	n�chster Schicht
	// ix[5]	Ende	n�chster Schicht
	
	for(int indx=0;indx<5;indx++)
		ix[indx] = -1;
	
	int	ilSize = omWorkTimeData.GetSize();
	int ilStart, ilEnd;
	
	for(ilStart=1; ilStart<ilSize-1; ilStart++)
	{
		if(omWorkTimeData[ilStart].cmState == S_DELETED) continue;
		// wir suchen einen Start
		if(omWorkTimeData[ilStart].cmType & (WTE_SHIFT | WTE_SHIFT_PLUS | WTE_FREE | WTE_R_FREE | WTE_DRA_FREE | WTE_SLEEP_DAY))
		{
			// Ende finden
			
			for(ilEnd=ilStart+1;ilEnd<ilSize-1;ilEnd++)
			{
				if(omWorkTimeData[ilEnd].cmState == S_DELETED) continue;
				if(omWorkTimeData[ilEnd].lmUrno == omWorkTimeData[ilStart].lmUrno)
				{
					// Testen!
					ix[2] = ilStart;
					ix[3] = ilEnd;
					CheckAllRules(ix);
					break;	// zur�ck zur Start-Schleife
				}
			}
		}
	}
}

/********************************************************************************************
L�uft alle Tests f�r alle Tage durch
********************************************************************************************/
void CedaWorkTime::CheckAllRules(int* piX)
{
	DRRDATA		rlDrr;
	DRADATA		rlDra;

	if(omWorkTimeData[piX[2]].cmType == WTE_DRA_FREE)
	{
		rlDra.Urno = omWorkTimeData[piX[2]].lmUrno;
		rlDra.Abfr = omWorkTimeData[piX[2]].omTime;
		rlDra.Abto = omWorkTimeData[piX[3]].omTime;
	}
	else
	{
		rlDrr.Urno = omWorkTimeData[piX[2]].lmUrno;
		rlDrr.Avfr = omWorkTimeData[piX[2]].omTime;
		rlDrr.Avto = omWorkTimeData[piX[3]].omTime;
	}

	// Testen durchf�hren
	switch(omWorkTimeData[piX[2]].cmType)
	{
	default:
		return;
	case WTE_SHIFT:
		if(omWorkTimeData[piX[2]].cmTimeType != TIME_ZERO)
		{
			if(RET_ERROR == Check_ID_WOR_FREEHT(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_RESTH(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_RESTDW(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_M_WORHT(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_WORHWT(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_MAWORDT(&rlDrr, piX)) return;
		}
		else
		{
			if(RET_ERROR == Check_ID_WOR_RESTDW(&rlDrr, piX)) return;
		}
		break;
	case WTE_SHIFT_PLUS:
		if(RET_ERROR == Check_ID_WOR_FREEHT(&rlDrr, piX)) return;
		break;
	case WTE_FREE:
		if(omWorkTimeData[piX[2]].cmTimeType != TIME_ZERO)
		{
			if(RET_ERROR == Check_ID_WOR_FREEHT(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_RESTH(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_RESTDW(&rlDrr, piX)) return;
			if(RET_ERROR == Check_ID_WOR_MAFREEDT(&rlDrr, piX)) return;
		}
		else
		{
			if(RET_ERROR == Check_ID_WOR_RESTDW(&rlDrr, piX)) return;
		}
		break;
	case WTE_R_FREE:
		if(RET_ERROR == Check_ID_WOR_MAFREEDT(&rlDrr, piX)) return;
		break;
	case WTE_DRA_FREE:
		break;
	case WTE_SLEEP_DAY:
		break;
	}
}

/********************************************************************************************
alle Regeln nacheinander werden gepr�ft, 
R�ckgabe - ein von RET_i enum-s
BITTE: zu jedem Regel komplette Beschreibung hinzuf�gen! - Das ist die einzige M�glichkeit,
sp�ter irgendwas zu kapieren

Diese Funktion optimiert Aufrufe der Pr�ffunktionen:
- analysiert die Art der �nderung, ob sie zu einer Vergr��erung, Verkleinerung oder
�nderung der Arbeitszeit f�hrt, 
- dementsprechend werden nur dijenige Pr�ffunktionen aufgerufen, 
die f�r diesen Fall zutreffen.
********************************************************************************************/
int CedaWorkTime::CheckOneDayRules(DRRDATA* prpDrr, DRADATA* prpDra, int ipDataChange, bool bpCheckWeek)
{
	if(!prpDrr && !prpDra) return RET_NO_ERROR;

	int ilRet = RET_NO_ERROR;

	// Datenvorbereitung: Start- und Endpunkte dieses DRR/DRA-s finden
	long llUrno = 0;
	switch(ipDataChange)
	{
	default:
	//case OT_DRR_FREE_CHANGE:
	//case OT_DRR_R_FREE_NEW:
	//case OT_DRR_R_FREE_CHANGE:
	//case OT_DRR_FREE_NEW:
	//case OT_DRR_SHIFT_NEW:
	//case OT_DRR_SHIFT_CHANGE:
	//case OT_DRR_SHIFT_PLUS_NEW:
	//case OT_DRR_SHIFT_PLUS_CHANGE:
		llUrno = prpDrr->Urno;
		break;

	case OT_DRR_SHIFT_DELETE:
	case OT_DRR_SHIFT_PLUS_DELETE:
	case OT_DRR_R_FREE_DELETE:
	case OT_DRR_FREE_DELETE:
	case OT_DRA_DELETE:
		return RET_NO_ERROR;

	case OT_DRA_NEW:
	case OT_DRA_CHANGE:  
		llUrno = prpDra->Urno;
		break;
	}

	int iy;
	int	ilSize = omWorkTimeData.GetSize();

	int ix[6];		
	// ix[0]	Start	vorheriger Schicht
	// ix[1]	Ende	vorheriger Schicht
	// ix[2]	Start	dieser Schicht
	// ix[3]	Ende	dieser Schicht
	// ix[4]	Start	n�chster Schicht
	// ix[5]	Ende	n�chster Schicht

	// diese Runde kann bis drei mal gedreht werden, und zwar, wenn mehr als ein Eintrag f�r diese llUrno gespeichert ist
	iy=0;
	do
	{
		for(int indx=0;indx<5;indx++)
			ix[indx] = -1;
		
		// diese Schicht
		// Start
		for(; iy<ilSize; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].lmUrno == llUrno) break;
		}

		if(iy<ilSize)
		{
			ix[2] = iy;
		}
		else
		{
			// keine Eintr�ge f�r llUrno mehr, beenden
			break;
		}

		// Ende
		for(iy++; iy<ilSize; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].lmUrno == llUrno) break;
		}

		ix[3] = iy;
		iy=ix[3]+1;		// n�chstemal mu� ab hier f�r die llUrno gesucht werden 

		if(ix[2] == ilSize || ix[3] == ilSize) 
		{
			_ASSERTE(false);		
			return RET_SOFTWARE_ERROR;		// keine diese Schicht gefunden!
		}

		// Pr�fungen
		// Ein negatives Ergebnis f�hrt zum Abbruch der weiteren Pr�fung und return RET_xxx

		// Ausnahme
		if (bpCheckWeek)
		{
			if((omWorkTimeData[ix[2]].cmType & (WTE_SHIFT | WTE_SHIFT_PLUS)) && omWorkTimeData[ix[2]].cmTimeType == TIME_ZERO)
			{
				// min. freie Tage pro Woche
				ilRet = Check_ID_WOR_RESTDW(prpDrr, ix);
				if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
				continue;
			}
		}

		if(omWorkTimeData[ix[2]].cmType & (WTE_SHIFT | WTE_SHIFT_PLUS))
		{
			// t�gl. Ruhezeit (toleriert)
			ilRet = Check_ID_WOR_FREEHT(prpDrr, ix);
			if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
		}

		if(omWorkTimeData[ix[2]].cmType & (WTE_SHIFT | WTE_FREE | WTE_R_FREE))
		{
			// freier Tag: min. Ruhezeit unterschritten
			ilRet = Check_ID_WOR_RESTH(prpDrr, ix);
			if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
		}

		if(omWorkTimeData[ix[2]].cmType & WTE_SHIFT)
		{
			// max. t�gliche Arbeitszeit
			ilRet = Check_ID_WOR_M_WORHT(prpDrr, ix);
			if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
		}

		if(omWorkTimeData[ix[2]].cmType & WTE_R_FREE)
		{
			// freie Tage in Folge (toleriert)
			ilRet = Check_ID_WOR_MAFREEDT(prpDrr, ix);
			if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
		}

		if (bpCheckWeek)
		{
			// Wochenarbeitszeit
			ilRet = Check_ID_WOR_WORHWT(prpDrr, ix);
			if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;

			// Arbeitstage in Folge
			ilRet = Check_ID_WOR_MAWORDT(prpDrr, ix);
			if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
			// Min. freie Tage pro Woche
			ilRet = Check_ID_WOR_RESTDW(prpDrr, ix);
			if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
		}
	}
	while(true);

	return RET_NO_ERROR;
}


/**************************************************************************************
Pr�fung:
ID_WOR_FREEH
1. Minimale Anzahl freier Stunden
Die minimale Nachtruhe von n Stunden (=8) darf in keinem Fall unterschritten werden, 
Dies gilt f�r die Schichtzuteilung, Tourenabtausch, Anordnung von 
�berzeit, Leisten von Teilzeit Plus. Das System mu� solche Eingabeversuche mit dem 
entsprechenden Hinweis verweigern.
<Wenn Schichtdauer der vorherige und der n�chste Schicht zusammen l�nger als 
10 Stunden ist, soll wegen Teilzeit+ gefragt werden>

Parameter: Minimale Anzahl freier Stunden, ID_WOR_FREEH

ID_WOR_FREEHT
2. Minimale Anzahl freier Stunden toleriert
Betr�gt die Nachtruhe weniger als 10 Stunden mu� eine Hinweiswarnung gegeben werden,
Dies gilt f�r die Schichtzuteilung, Tourenabtausch, Anordnung von 
�berzeit, Leisten von Teilzeit Plus. Der Dienstplaner 
ist somit aufgefordert seine Eingaben zu �berpr�fen. Es obliegt nun der Verantwortung 
des Dienstplaners im Rahmen der gesetzlichen Vorgaben zu Agieren.
<Wenn Schichtdauer der vorherige und der n�chste Schicht zusammen l�nger als 
10 Stunden ist, soll wegen Teilzeit+ gefragt werden>

Parameter: Minimale Anzahl freier Stunden toleriert, ID_WOR_FREEHT
**************************************************************************************/
int CedaWorkTime::Check_ID_WOR_FREEHT(DRRDATA* prpDrr, int* piX)
{
	if(!pomShiftCheck->bmDoConfictCheck) return RET_NO_ERROR;	// alle Pr�fungen sind abgeschaltet
	bool blCheckID_WOR_FREEH = pomShiftCheck->omRules.GetAt(ID_WOR_FREEH) != -1;
	bool blCheckID_WOR_FREEHT = pomShiftCheck->omRules.GetAt(ID_WOR_FREEHT) != -1;
	if(!blCheckID_WOR_FREEH && !blCheckID_WOR_FREEHT) return RET_NO_ERROR;	// alle Pr�fungen k�nnen durch -1 ausgeschaltet werden
	if(prpDrr->Avto < pomShiftCheck->omCheckDateFrom || prpDrr->Avfr > pomShiftCheck->omCheckDateTo) return RET_NO_ERROR;
	
	int ilRet = RET_NO_ERROR;
	int	ilSize = omWorkTimeData.GetSize();
	int iy;
		
	/* 
	piX[0] - Start Vorheriger Schicht
	piX[1] - Ende  Vorheriger Schicht
	piX[2] - Start Dieser Schicht
	piX[3] - Ende  Dieser Schicht
	piX[4] - Start Nachfolgender Schicht
	piX[5] - Ende  Nachfolgender Schicht
	*/
	
	// vorherige Schicht (nicht beim ShiftRosterCheck pr�fen, nur bei DutyRoster)
	// Start
	if (!pomShiftCheck->bmShiftRosterCheck)
	{
		for(iy=piX[2]-1; iy>=0; iy--)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].cmType & WTE_SHIFT_ALL) 
			{
				if(omWorkTimeData[iy].lmUrno == omWorkTimeData[piX[2]].lmUrno)
				{
					// das ist eigene Schicht (Vorzeit), nichts zu pr�fen
					piX[0] = -1;
				}
				else
				{
					piX[0] = iy;
				}
				break;
			}
		}
	}

	if(piX[0] >= 0)
	{
		
		// Ende
		for(iy++; iy<piX[2]; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].lmUrno == omWorkTimeData[piX[0]].lmUrno) 
			{
				piX[1] = iy;
				break;
			}
		}
	}
	else
	{
		piX[1] = -1;
	}

	// nachfolgede Schicht
	// Start
	for(iy=piX[3]+1; iy<ilSize; iy++)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		if(omWorkTimeData[iy].cmType & WTE_SHIFT_ALL) 
		{
			if(omWorkTimeData[iy].lmUrno == omWorkTimeData[piX[2]].lmUrno)
			{
				// das ist eigene Schicht (Vorzeit), nichts zu pr�fen
				iy = ilSize;	// - Ausschalter
			}
			break;
		}
	}
	piX[4] = iy;

	if (piX[4] < ilSize)
	{
		// Ende
		for(iy++; iy<ilSize; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].lmUrno == omWorkTimeData[piX[4]].lmUrno) 
			{
				break;
			}
		}
		piX[5] = iy;
	}

	// Kontrolle 
	COleDateTimeSpan olFreeH;
	if(blCheckID_WOR_FREEH)
		olFreeH = COleDateTimeSpan(0,0,pomShiftCheck->omRules[ID_WOR_FREEH],-1);
	COleDateTimeSpan olFreeHT;
	if(blCheckID_WOR_FREEHT)
		olFreeHT = COleDateTimeSpan(0,0,pomShiftCheck->omRules[ID_WOR_FREEHT],-1);
	
	// Vorherige Schicht
	if(	piX[0] > -1 && piX[0] < ilSize && 
		piX[1] > -1 && piX[1] < ilSize && 
		piX[2] > -1 && piX[2] < ilSize && 
		piX[3] > -1 && piX[3] < ilSize )
//		omWorkTimeData[piX[2]].GetAbsDay() - omWorkTimeData[piX[0]].GetAbsDay() == 1)
{
		// alle Schichtzeiten sind korrekt berechnet und
		// die vorherige Schicht ist genau ein Tag vor der aktuellen
		if(	blCheckID_WOR_FREEH && 
			(omWorkTimeData[piX[2]].omTime - omWorkTimeData[piX[1]].omTime < olFreeH))
		{
			// Fehler: die Pause ist k�rzer, als erlaubt
			return AddErrorMessage(ID_WOR_FREEH, piX[2], RET_ERROR);
		}
		
		if(	blCheckID_WOR_FREEHT && 
			(omWorkTimeData[piX[2]].omTime - omWorkTimeData[piX[1]].omTime < olFreeHT))
		{
			// Warnung: die Pause ist zu kurz
			ilRet = AddErrorMessage(ID_WOR_FREEHT, piX[2], RET_WARNING);
		}
	}
	
	// Nachfolgende Schicht
	if(	piX[2] > -1 && piX[2] < ilSize && 
		piX[3] > -1 && piX[3] < ilSize && 
		piX[4] > -1 && piX[4] < ilSize && 
		piX[5] > -1 && piX[5] < ilSize )
//		omWorkTimeData[piX[4]].GetAbsDay() - omWorkTimeData[piX[2]].GetAbsDay() == 1 )
	{
		// alle Schichtzeiten sind korrekt berechnet und
		// die nachfolgende Schicht ist am gleichen oder n�chstem Tag
		if(	blCheckID_WOR_FREEH && 
			(omWorkTimeData[piX[4]].omTime - omWorkTimeData[piX[3]].omTime < olFreeH))
		{
			// Fehler: die Pause ist k�rzer, als erlaubt
			return AddErrorMessage(ID_WOR_FREEH, piX[3], RET_ERROR);
		}
		
		if(	blCheckID_WOR_FREEHT && 
			(omWorkTimeData[piX[4]].omTime - omWorkTimeData[piX[3]].omTime < olFreeHT))
		{
			// Warnung: die Pause ist zu kurz
			ilRet = AddErrorMessage(ID_WOR_FREEHT, piX[3], RET_WARNING);
		}
	}
	
	return ilRet;
}

/**************************************************************************************
Pr�fung:
ID_WOR_RESTH
3. Minimale Ruhezeit eines "Frei"-Tages
Die minimale Ruhezeit eines Freitages betr�gt 33 Stunden. Diese d�rfen weder in der 
Planung noch durch Tourenabtausch unterschritten werden. Das System mu� solche
Eingabeversuche mit dem entsprechenden Hinweis verweigern. 
Liegen mehr als ein Freie Tag vor, soll nur ein Hinweis ausgegeben werden. Die Verweigerung
ist nicht mehr erforderlich.
Eingaben von �berzeit oder Teilzeit Plus in die Ruhezeitperiode sind m�glich und der Ruhetag gilt weiterhin 
als gew�hrt. Es wird nur die regul�re Zeit der Schicht laut Stammdaten bewertet.
Die Ruhezeit von n Freitagen berechnet sich wie folgt: 24h * nTage + 9h

Eine 2. Schicht die als Teilzeit Plus gekennzeichnet ist, wird nicht bewertet

Anmerkung: Teilzeit Plus kann nur als zweite Schicht vergeben werden, 
d.h. wenn eine zweite Schicht zu einer FREI - Schicht vergeben wird, 
ist diese zweite Schicht automatisch eine Teilzeit Plus - Schicht.

Parameter: Minimale Ruhezeit eines Freitages, ID_WOR_RESTH
**************************************************************************************/
int CedaWorkTime::Check_ID_WOR_RESTH(DRRDATA* prpDrr, int* piX)
{
	if(!pomShiftCheck->bmDoConfictCheck) return RET_NO_ERROR;	// alle Pr�fungen sind abgeschaltet
	if(pomShiftCheck->omRules.GetAt(ID_WOR_RESTH) == -1) return RET_NO_ERROR;	// alle Pr�fungen k�nnen durch -1 ausgeschaltet werden
	if(prpDrr->Avto < pomShiftCheck->omCheckDateFrom || prpDrr->Avfr > pomShiftCheck->omCheckDateTo) return RET_NO_ERROR;
	
	int ilSize = omWorkTimeData.GetSize();
	int iy;
	int ilRFreeNumber=0;	// Anzahl der freien Tage
	
	int ilMinNumberOfHours = pomShiftCheck->omRules[ID_WOR_RESTH] / 60;
	
	if(omWorkTimeData[piX[2]].cmType == WTE_R_FREE)
	{
		// piX[0]	Start der vorherigen Schicht
		// piX[1]	Ende der vorherigen Schicht
		// piX[2]	Start	DRR_R_FREE diese
		// piX[3]	Ende	DRR_R_FREE diese
		// piX[4]	
		// piX[5]	Start DRR_SHIFT danach

		// zuerst nach unten eine Schicht suchen
		for(iy=piX[2]-1; iy>=0; iy--)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].cmType & WTE_SHIFT) break;
		}

		// Ende der Schicht finden
		if(iy < 0)
		{
			// offenes Ende nach unten - kein Problem
			return RET_NO_ERROR;
		}

		piX[0] = iy;
		// Ende der unteren Schicht suchen
		for(iy++; iy<piX[2]; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(	(omWorkTimeData[iy].cmType & WTE_END) && 
				omWorkTimeData[iy].lmUrno == omWorkTimeData[piX[0]].lmUrno)
				break;
		}

		piX[1] = iy;

		// bis n�chste Schicht iterieren und alle WTE_R_FREEs z�hlen (inklusive der neuen)
		ilRFreeNumber=0;
		for(iy=piX[1]+1; iy<ilSize; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].cmType & WTE_R_FREE) ilRFreeNumber++;
			if(omWorkTimeData[iy].cmType & WTE_SHIFT) break;
		}

		piX[5] = iy;
		if(piX[5] == ilSize)
		{
			// offenes Ende nach oben - kein Problem
			return RET_NO_ERROR;
		}

		// Anfang der Schicht gefunden, pr�fen
		COleDateTimeSpan olHours = omWorkTimeData[piX[5]].omTime - omWorkTimeData[piX[1]].omTime;
		int ilNumberOfHours = (int)(24.0*olHours.m_span);
		
		if(ilNumberOfHours - (ilRFreeNumber - 1) * 24 < ilMinNumberOfHours)
		{
			if(ilRFreeNumber > 1)
			{
				// Warning: die R_FREE ist k�rzer, als erlaubt
				return AddErrorMessage(ID_WOR_RESTH, piX[3], RET_WARNING);
			}
			else
			{
				// Fehler: die R_FREE ist k�rzer, als erlaubt
				pomShiftCheck->bmEnableTeilzeitplusQuestion = false;	// beim Einf�gen einer R_FREE hat kein Sinn, nach Teilzeit+ zu fragen
				return AddErrorMessage(ID_WOR_RESTH, piX[3], RET_ERROR);
			}
		}
	}
	else if(omWorkTimeData[piX[2]].cmType == WTE_SHIFT)
	{
		// wenn DRR eine Schicht ist, checken wir, ob die nicht die anliegenden R_FREE einschr�nkt
		
		// piX[0]	Start der vorherigen Schicht
		// piX[1]	Ende der vorherigen Schicht
		// piX[2]	Start	DRR_SHIFT diese
		// piX[3]	Ende	DRR_SHIFT diese
		// piX[4]	
		// piX[5]	Start DRR_SHIFT danach
		
		// unten
		
		// zuerst vorherige SHIFT suchen und dabei die R_FREEs z�hlen
		ilRFreeNumber=0;
		for(iy=piX[2]-1; iy>=0; iy--)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].cmType & WTE_R_FREE) ilRFreeNumber++;
			if(omWorkTimeData[iy].cmType & WTE_SHIFT) break;
		}

		// Ende der Schicht finden
		if(ilRFreeNumber > 0 && iy >=0)
		{
			piX[0] = iy;
			for(iy++; iy<piX[2]; iy++)
			{
				if(omWorkTimeData[iy].cmState == S_DELETED) continue;
				if(	(omWorkTimeData[iy].cmType & WTE_END) && 
					omWorkTimeData[iy].lmUrno == omWorkTimeData[piX[0]].lmUrno)
					break;
			}

			piX[1] = iy;
			if(piX[1] < piX[2])
			{
				// Ende der Schicht gefunden, pr�fen
				COleDateTimeSpan olTime = omWorkTimeData[piX[2]].omTime - omWorkTimeData[piX[1]].omTime;
				int ilNumberOfHours = (int)(24.0*olTime.m_span);
				
				if(ilNumberOfHours - (ilRFreeNumber - 1) * 24 < ilMinNumberOfHours)
				{
					if(ilRFreeNumber > 1)
					{
						// Warning: die R_FREE ist k�rzer, als erlaubt
						return AddErrorMessage(ID_WOR_RESTH, piX[2], RET_WARNING);
					}
					else
					{
						// Fehler: die R_FREE ist k�rzer, als erlaubt
						return AddErrorMessage(ID_WOR_RESTH, piX[2], RET_ERROR);
					}
				}
			}
		}
		
		// oben
		// zuerst n�chste SHIFT suchen und dabei die R_FREEs z�hlen
		ilRFreeNumber=0;
		for(iy=piX[3]+1; iy<ilSize; iy++)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].cmType & WTE_R_FREE) ilRFreeNumber++;
			if(omWorkTimeData[iy].cmType & WTE_SHIFT) break;
		}

		piX[5] = iy;
		if(ilRFreeNumber > 0 && piX[5] < ilSize)
		{
			// Anfang der Schicht gefunden, pr�fen
			COleDateTimeSpan olTime = omWorkTimeData[piX[5]].omTime - omWorkTimeData[piX[3]].omTime;
			int ilNumberOfHours = (int)(24*olTime.m_span);
			
			if(ilNumberOfHours - (ilRFreeNumber - 1) * 24 < ilMinNumberOfHours)
			{
				if(ilRFreeNumber > 1)
				{
					// Warning: die R_FREE ist k�rzer, als erlaubt
					return AddErrorMessage(ID_WOR_RESTH, piX[3], RET_WARNING);
				}
				else
				{
					// Fehler: die R_FREE ist k�rzer, als erlaubt
					return AddErrorMessage(ID_WOR_RESTH, piX[3], RET_ERROR);
				}
			}
		}
	}
	
	return RET_NO_ERROR;
}

/**************************************************************************************
Pr�fung:
ID_WOR_RESTDW
4. Minimale Anzahl der Freien Tage pro Woche [toleriert] - NICHT MEHR TOLERIERT
Pro Kalenderwoche soll mindestens ein Ruhetag gew�hrt werden. Ausnahmen 
k�nnen in ausserordentlichen Situationen nach Absprache und mit dem 
Einverst�ndnis der/des MitarbeiterIn gemacht werden. Versehentliche Eingaben 
werden durch eine entsprechende Hinweiswarnung ausgeschlossen. Bei Leistungen
von �berzeit oder Teilzeit Plus w�hrend Ruhetagen gelten diese weiterhin als 
gew�hrt.

  Parameter: Minimale Anzahl der Freien Tage pro Woche toleriert, ID_WOR_RESTDW  ------
**************************************************************************************/
int CedaWorkTime::Check_ID_WOR_RESTDW(DRRDATA* prpDrr, int* piX)
{
	if(!pomShiftCheck->bmDoConfictCheck) return RET_NO_ERROR;	// alle Pr�fungen sind abgeschaltet
	if(pomShiftCheck->omRules.GetAt(ID_WOR_RESTDW) == -1) return RET_NO_ERROR;	// alle Pr�fungen k�nnen durch -1 ausgeschaltet werden
	if(prpDrr->Avto < pomShiftCheck->omCheckDateFrom || prpDrr->Avfr > pomShiftCheck->omCheckDateTo) return RET_NO_ERROR;

	// Kalenderwoche feststellen: Wendepunkt ist 00:00 Montag
	int ilDayOfWeek = prpDrr->Avfr.GetDayOfWeek() - 2;	
	if(ilDayOfWeek == -1) ilDayOfWeek = 6;
	// jetzt 0-Mo,1-Di,...6-So

	//                                                   in Minuten 
	COleDateTimeSpan olRestH(0,0,pomShiftCheck->omRules[ID_WOR_RESTH],0); // Sollzeit
	if(olRestH < COleDateTimeSpan(0,24,0,0)) return RET_SOFTWARE_ERROR;
	
	// Startzeitpunkt
	COleDateTime olDayStart = prpDrr->Avfr - COleDateTimeSpan(ilDayOfWeek, prpDrr->Avfr.GetHour(), prpDrr->Avfr.GetMinute(), prpDrr->Avfr.GetSecond());

	int ilRet;
	// Wenn der Pr�ftag Montag ist, m�ssen wir die Woche zuvor auch pr�fen, ob diese Schicht nicht die Zeiten der
	// vorherigen Woche einschr�nkt
	// Das gleiche gilt f�r Sonntag und die Woche danach
	if(ilDayOfWeek == 0)
	{
		// Montag: die vorherige Woche pr�fen
		olDayStart -= COleDateTimeSpan(7,0,0,0);
		ilRet = CheckTheWeek_ID_WOR_RESTDW(olDayStart, olRestH, piX);
		if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;
		olDayStart += COleDateTimeSpan(7,0,0,0);
	}

	// Diese Woche pr�fen
	ilRet = CheckTheWeek_ID_WOR_RESTDW(olDayStart, olRestH, piX);
	if(ilRet == RET_ERROR || ilRet == RET_SOFTWARE_ERROR) return ilRet;

	if(ilDayOfWeek == 6)
	{
		// Sonntag: die Woche danach pr�fen
		olDayStart += COleDateTimeSpan(7,0,0,0);
		ilRet = CheckTheWeek_ID_WOR_RESTDW(olDayStart, olRestH, piX);
	}
	return ilRet;
}

/*************************************************************************************************
Unterfunktion
*************************************************************************************************/
int CedaWorkTime::CheckTheWeek_ID_WOR_RESTDW(COleDateTime opDayStart, COleDateTimeSpan opRestH, int* piX)
{
	// piX[0]	Zeitpunkt vor dem Wochenanfang
	// piX[1]
	// piX[2]	Start	DRR_ diese
	// piX[3]	Ende	DRR_ diese
	// piX[4]
	// piX[5]	Zeitpunkt nach dem Wochenende

	// mindestens N Tage pro Kalenderwoche m�ssen R_FREE haben oder f�r R_FREE taugen (insg. >= 33 Std frei haben)

	int nlRFreeCount=0;				// viewiele R_Free gez�hlt worden sind 
	int	ilSize = omWorkTimeData.GetSize();

	//----------------------------------------------------------------------------
	// 1. gibt es an einem der sieben Tage R_FREE oder einen leeren Tag �berhaupt?
	
	// 0 - zovor
	// 1 bis 7 - Mo bis So
	// 8 - danach
	int ilOccupiedDay[9];
	memset((void*)ilOccupiedDay, 0, sizeof(int)*9);
	ilOccupiedDay[0] = -1;	// Voreinstellung, um zu wissen, ob eine Initialisierung stattgefunden hat

	int ilThisDay;
	int ilAbsDayStart = (int)floor(opDayStart.m_dt);

	COleDateTime olThisDay = opDayStart;
	for(int iy=0; iy<ilSize; iy++)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		
		if(omWorkTimeData[iy].cmType & (WTE_SHIFT | WTE_FREE))
		{
			if(iy < ilSize-1)
			{
				ilThisDay = omWorkTimeData[iy].GetAbsDay() - ilAbsDayStart;
				if(ilThisDay < 0)
				{
					ilOccupiedDay[0] = iy;	// nur wenn zuvor eine WTE_SHIFT/FREE steht, sonst bleibt 0
				}
				else if(ilThisDay < 7)
				{
					if(ilOccupiedDay[ilThisDay+1] == 0)
					{
						ilOccupiedDay[ilThisDay+1] = iy;	// nur die erste Schicht des Tages speichern
					}
				}
				else
				{
					ilOccupiedDay[8] = iy;
					break;		// Ende
				}
			}
			else
			{
				ilOccupiedDay[8] = iy;
				break;		// Ende
			}
		}
	}

	// leere/R_FREE Tage z�hlen
	nlRFreeCount = 0;
	for(int ilDay=1; ilDay<8; ilDay++)
	{
		if(ilOccupiedDay[ilDay] == 0)
			nlRFreeCount++;
	}

	// Anzahl potientiell freier Tage pr�fen
	if(nlRFreeCount < (int)pomShiftCheck->omRules[ID_WOR_RESTDW])
	{
		// Fehler: die R_FREE-Anzahl ist weniger, als erlaubt (soll z.Zt. mindestens ein Frei-Tag pro Woche sein)
		return AddErrorMessage(ID_WOR_RESTDW, piX[3], RET_ERROR);
	}

	//----------------------------------------------------------------------------------
	// 2. genauer z�hlen
	nlRFreeCount = 0;	// weiter z�hlen wir hoch
	int ilStartOfFreeTime;
	int ilEndOfFreeTime;
	// Es gibt genug potentiell freien Tage in der Woche, die zul�ssige Zeit dieser Tage (33 Std.) mu� gepr�ft werden
	for(ilDay=1; ilDay<8; ilDay++)
	{
		if(ilOccupiedDay[ilDay] == 0)
		{
			// Dieser Tag ist leer/WTE_R_FREE, wir suchen die Zeitgrenzen (davor - danach)
			// Ende der Schicht zuvor
			if(ilDay == 0 && ilOccupiedDay[0] == -1)
			{
				// Montag gleich leer aber davor war keine WTE_SHIFT/FREE
				ilStartOfFreeTime = 0;
			}
			else
			{
				ilStartOfFreeTime = 0;
				// bis an eine Schicht nach unten laufen
				for(int iy=ilOccupiedDay[ilDay-1]; iy>=0; iy--)
				{
					if(omWorkTimeData[iy].cmState == S_DELETED) continue;
					if(omWorkTimeData[iy].cmType == WTE_SHIFT)
					{
						// Ende dieser Schicht finden
						long llThisUrno = omWorkTimeData[iy].lmUrno;
						for(++iy;iy<ilSize;iy++)
						{
							if(omWorkTimeData[iy].cmState == S_DELETED) continue;
							if(llThisUrno == omWorkTimeData[iy].lmUrno && omWorkTimeData[iy].cmType == WTE_END)
							{
								ilStartOfFreeTime = iy;
								iy=0;	// break f�r die h�here Schleife
								break;
							}
						}
					}
				}
			}
			
			// das Ende suchen (alle zusammenh�ngende leer/WTE_R_FREE/WTE_FREE Tage durchlaufen (wenn vorhanden))

			// Wenn kein Ende gefunden wird, d.h. bis Ende ist alles frei, bleibt diese Voreinstellung erhalten
			ilEndOfFreeTime = ilSize-1;
			for(;ilDay<8; ilDay++)
			{
				if(	ilOccupiedDay[ilDay+1] == ilSize-1 ||
					ilOccupiedDay[ilDay+1] != 0 && 
					omWorkTimeData[ilOccupiedDay[ilDay+1]].cmType != WTE_FREE)	// WTE_FREE wird ja zu der R_FREE zugez�hlt, so m�ssen wir sie ignorieren
				{
					ilEndOfFreeTime = ilOccupiedDay[ilDay+1];
					break;
				}
			}

			COleDateTimeSpan olTime = omWorkTimeData[ilEndOfFreeTime].omTime - omWorkTimeData[ilStartOfFreeTime].omTime;
			nlRFreeCount += GetNumberOfRegFreeTimes(olTime, opRestH);
			if(nlRFreeCount >= (int)pomShiftCheck->omRules[ID_WOR_RESTDW])
				return RET_NO_ERROR;
		}
	}
	// Fehler: die R_FREE-Anzahl ist weniger, als erlaubt (soll z.Zt. mindestens ein Frei-Tag pro Woche sein)
	return AddErrorMessage(ID_WOR_RESTDW, piX[2], RET_ERROR);
	
}

/**************************************************************************************
Dienstfunktion: stellt fest, ob der Zeitraum opTime ausreichend Zeit f�r mindestens ein
R_FREE hat, wenn ja, dann f�r wieviele.
R�ckgabe: Anzahl von R_FREEs in opTime
**************************************************************************************/
int CedaWorkTime::GetNumberOfRegFreeTimes(COleDateTimeSpan& opTime, COleDateTimeSpan& opRestH)
{
	// opRestH Sollzeit == 33 Std, opRestH mu� > als 24 Std. sein
	if(opTime < opRestH) return 0;

	int nlHours = opTime.GetHours() + opTime.GetDays()*24;
	int nlRegFree = nlHours/24;
	
	if((nlHours % 24) < opRestH.GetHours() - 24)
	{
		// z.B. wenn opRestH = 33, dann ist die Restzeit < als 33-24=9 Std.
		nlRegFree--;
	}
	
	return nlRegFree;
}

/**************************************************************************************
Pr�fung:
ID_WOR_MAWORH
5. Maximale Arbeitszeit
Alle Schichten eines Tages d�rfen in ihrer 
Gesamtl�nge 10 Stunden nicht �berschreiten. Das System mu� beim Anlegen einer 
Schicht die L�nge erkennen und Zeit�berschreitungen mit dem entsprechenden 
Hinweis verweigern k�nnen. Die Verl�ngerung durch Leistung von zus�tzlicher 
�berzeit oder Teilzeit Plus ist statthaft. Es wird nur die regul�re Zeit der 
Schicht laut Stammdaten bewertet.

Parameter: Maximale Arbeitszeit, ID_WOR_MAWORH

ID_WOR_MAWORHT
6. Maximale Arbeitszeit toleriert
Wenn die zu leistende Arbeitszeit aller Schichten eines Tages eine Gesamtl�nge von n Stunden 
�berschreiten, soll das System dies beim Anlegen einer Schicht mit einer 
entsprechenden Hinweiswarnung anzeigen. Die Verl�ngerung durch Leistung von 
zus�tzlicher �berzeit oder Teilzeit Plus ist statthaft. Es wird nur die regul�re
Zeit der Schicht laut Stammdaten bewertet.

Parameter: Maximale Arbeitszeit toleriert, ID_WOR_MAWORHT

ID_WOR_MIWORH
1. Minimale Arbeitszeit
Eine im Rahmen der normalen Arbeitszeit zu leistende Schicht darf in ihrer 
Gesamtl�nge n Stunden nicht unterschreiten. Das System mu� beim Anlegen einer 
Schicht die L�nge erkennen und Zeitunterschreitungen mit dem entsprechenden 
Hinweis verweigern k�nnen. Die Verk�rzung der Leistung ist statthaft. Es wird 
nur die regul�re Zeit der Schicht laut Stammdaten bewertet.

Parameter: Minimale Arbeitszeit, ID_WOR_MIWORH


ID_WOR_MIWORHT
2. Minimale Arbeitszeit toleriert
Wenn die zu leistende Arbeitszeit einer Schicht eine Gesamtl�nge von n Stunden 
unterschreitet, soll das System dies beim Anlegen einer Schicht mit einer 
entsprechenden Hinweiswarnung anzeigen. Die Verk�rzung der Leistung ist statthaft.
Es wird nur die regul�re Zeit der Schicht laut Stammdaten bewertet.

Parameter: Minimale Arbeitszeit toleriert, ID_WOR_MIWORHT
**************************************************************************************/
int CedaWorkTime::Check_ID_WOR_M_WORHT(DRRDATA* prpDrr, int* piX)
{
	if(!pomShiftCheck->bmDoConfictCheck) return RET_NO_ERROR;	// alle Pr�fungen sind abgeschaltet
	if(prpDrr->Avto < pomShiftCheck->omCheckDateFrom || prpDrr->Avfr > pomShiftCheck->omCheckDateTo) return RET_NO_ERROR;

	bool blID_WOR_MAWORH = pomShiftCheck->omRules.GetAt(ID_WOR_MAWORH) != -1;
	bool blID_WOR_MAWORHT = pomShiftCheck->omRules.GetAt(ID_WOR_MAWORHT) != -1;
	bool blID_WOR_MIWORH = pomShiftCheck->omRules.GetAt(ID_WOR_MIWORH) != -1;
	bool blID_WOR_MIWORHT = pomShiftCheck->omRules.GetAt(ID_WOR_MIWORHT) != -1;

	int iy;

	// Die Arbeitszeit dieses Tages z�hlen
	
	// zur ersten Schicht des Tagen laufen
	int ilLastShift = -1;
	int ilThisDay = omWorkTimeData[piX[2]].GetAbsDay();

	for(iy=piX[2]-1; iy>0; iy--)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		if(omWorkTimeData[iy].GetAbsDay() != ilThisDay)
			break;
		if(omWorkTimeData[iy].cmType == WTE_SHIFT)
			ilLastShift = iy;
	}

	COleDateTimeSpan olWorkTime = 0;
	
	// wenn ilLastShift == -1, wird iy=piX[2] gesetzt
	iy=(ilLastShift == -1 ? piX[2] : ilLastShift);
	for(; iy<omWorkTimeData.GetSize(); iy++)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		if(omWorkTimeData[iy].GetAbsDay() != ilThisDay)
			break;
		if(omWorkTimeData[iy].cmType == WTE_SHIFT)
			olWorkTime += omWorkTimeData[iy].omSb;
	}

	if(blID_WOR_MAWORH)
	{
		//                                                     in Minuten
		COleDateTimeSpan olMaworH(0,0,pomShiftCheck->omRules[ID_WOR_MAWORH],0); // Sollzeit
		
		// Check
		if(olWorkTime > olMaworH)
		{
			// Fehler: die Schichtenl�nge ist �berschritten!
			return AddErrorMessage(ID_WOR_MAWORH, piX[2], RET_ERROR);
		}
	}
	

	if(blID_WOR_MAWORHT)
	{
		COleDateTimeSpan olMaworHT(0,0,pomShiftCheck->omRules[ID_WOR_MAWORHT],0); // Sollzeit
		
		if(olWorkTime > olMaworHT)
		{
			// Warnung: die Schichten sind zu lang
			AddErrorMessage(ID_WOR_MAWORHT, piX[2], RET_WARNING);
		}
	}

	if(blID_WOR_MIWORH)
	{
		//                                                     in Minuten
		COleDateTimeSpan olMiworH(0,0,pomShiftCheck->omRules[ID_WOR_MIWORH],0); // Sollzeit
		
		// Check
		if(olWorkTime < olMiworH)
		{
			// Fehler: die Schichtl�nge ist unterschritten!
			return AddErrorMessage(ID_WOR_MIWORH, piX[2], RET_ERROR);
		}
	}
	
	if(blID_WOR_MIWORHT)
	{
		COleDateTimeSpan olMiworHT(0,0,pomShiftCheck->omRules[ID_WOR_MIWORHT],0); // Sollzeit
		
		if(olWorkTime < olMiworHT)
		{
			// Warnung: die Schicht ist zu kurz
			AddErrorMessage(ID_WOR_MIWORHT, piX[2], RET_WARNING);
		}
	}
	
	return RET_NO_ERROR;
}

/**************************************************************************************
Pr�fung:

ID_WOR_WORHWT
7. Wochenarbeitszeit toleriert
Die maximale Wochenarbeitszeit von 60 Stunden soll nicht �berschritten werden. 
Das System mu� eine entsprechende Hinweiswarnung geben. Leistungen von �berzeit 
oder Teilzeit Plus sind zus�tzlich m�glich. Es wird nur die regul�re Zeit der 
Schicht laut Stammdaten bewertet.

Parameter: Wochenarbeitszeit toleriert, ID_WOR_WORHWT
**************************************************************************************/
int CedaWorkTime::Check_ID_WOR_WORHWT(DRRDATA* prpDrr, int* piX)
{
	if(!pomShiftCheck->bmDoConfictCheck) return RET_NO_ERROR;	// alle Pr�fungen sind abgeschaltet
	if(pomShiftCheck->omRules.GetAt(ID_WOR_WORHWT) == -1) return RET_NO_ERROR;	// alle Pr�fungen k�nnen durch -1 ausgeschaltet werden
	if(prpDrr->Avto < pomShiftCheck->omCheckDateFrom || prpDrr->Avfr > pomShiftCheck->omCheckDateTo) return RET_NO_ERROR;

	//int ilRet = RET_NO_ERROR;
	int	ilSize = omWorkTimeData.GetSize();
	int iy;

	//                                                     in Minuten
	COleDateTimeSpan olWorhwT(0,0,pomShiftCheck->omRules[ID_WOR_WORHWT],0); // Sollzeit

	// Kalenderwoche feststellen: Wendepunkt ist 00:00 Montag
	int ilDayOfWeek = prpDrr->Avfr.GetDayOfWeek() - 2;	
	if(ilDayOfWeek == -1) ilDayOfWeek = 6;
	// jetzt 0-Mo,1-Di,...6-So
	
	// Startzeitpunkt
	COleDateTime olDayStart = prpDrr->Avfr - COleDateTimeSpan(ilDayOfWeek, prpDrr->Avfr.GetHour(), prpDrr->Avfr.GetMinute(), prpDrr->Avfr.GetSecond());
	COleDateTime olDayEnd = olDayStart + COleDateTimeSpan(7,0,0,0);

	COleDateTimeSpan olSumme = 0;

	for(iy=0; iy<ilSize; iy++)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		if(	omWorkTimeData[iy].cmType == WTE_SHIFT && 
			omWorkTimeData[iy].omTime >= olDayStart && 
			omWorkTimeData[iy].omTime < olDayEnd) 
		{
			olSumme += omWorkTimeData[iy].omSb;
		}
	}

	// Checken
	if((olWorhwT + COleDateTimeSpan(0,0,0,1)) < olSumme)
	{
		// Warnung: die Wochen-Arbeitszeit ist �berschritten
		return AddErrorMessage(ID_WOR_WORHWT, piX[2], RET_WARNING);
	}
	return RET_NO_ERROR;
}

/**************************************************************************************
Pr�fung:
ID_WOR_MAWORD
8. Maximale Arbeitstage in Folge
Die Anzahl der zu leistende Arbeitstage in Folge darf in ihrer Gesamtzahl n Tage
nicht �berschreiten. Das System mu� beim Anlegen einer Schicht die Anzahl der Tage
in Folge erkennen und �berschreitungen mit dem entsprechenden Hinweis verweigern 
k�nnen. Gez�hlt werden alle Schichtcodes. Teilzeit Plus - Schichten sind zus�tzlich 
m�glich und werden nicht bewertet

Parameter: Maximale Arbeitstage in Folge, ID_WOR_MAWORD

ID_WOR_MAWORDT
9. Maximale Arbeitstage in Folge toleriert
Wenn die zu leistenden Arbeitstage in Folge eine Gesamtzahl von n Tagen 
�berschreitet, soll das System dies beim Anlegen einer Schicht mit einer 
entsprechenden Hinweiswarnung anzeigen. Gez�hlt werden alle Schichtcodes. 
Teilzeit Plus - Schichten sind zus�tzlich m�glich und werden nicht bewertet

Parameter: Maximale Arbeitstage in Folge toleriert, ID_WOR_MAWORDT
**************************************************************************************/
int CedaWorkTime::Check_ID_WOR_MAWORDT(DRRDATA* prpDrr, int* piX)
{
	if(!pomShiftCheck->bmDoConfictCheck) return RET_NO_ERROR;	// alle Pr�fungen sind abgeschaltet
	bool blCheckID_WOR_MAWORD = pomShiftCheck->omRules.GetAt(ID_WOR_MAWORD) != -1;
	bool blCheckID_WOR_MAWORDT = pomShiftCheck->omRules.GetAt(ID_WOR_MAWORDT) != -1;
	if(!blCheckID_WOR_MAWORD && !blCheckID_WOR_MAWORDT) return RET_NO_ERROR;	// alle Pr�fungen k�nnen durch -1 ausgeschaltet werden
	if(prpDrr->Avto < pomShiftCheck->omCheckDateFrom || prpDrr->Avfr > pomShiftCheck->omCheckDateTo) return RET_NO_ERROR;

	int ilRet = RET_NO_ERROR;
	int	ilSize = omWorkTimeData.GetSize();
	int iy;

	int ilShifts;
	if (omWorkTimeData[piX[2]].cmType & WTE_SHIFT)
	{
		ilShifts = 1;	// diese Schicht inklusive
	}
	else
	{
		ilShifts = 0;
	}
	int ilLastShift;

	// nach unten z�hlen
	if (omWorkTimeData[piX[2]].cmType & WTE_SHIFT)
	{
		for(iy=piX[2]-1, ilLastShift=piX[2]; iy>=0; iy--)
		{
			if(omWorkTimeData[iy].cmState == S_DELETED) continue;
			if(omWorkTimeData[iy].cmType & (WTE_FREE | WTE_R_FREE | WTE_SLEEP_DAY)) break;
			if(omWorkTimeData[iy].cmType == WTE_SHIFT) 
			{
				if(omWorkTimeData[ilLastShift].GetAbsDay() - omWorkTimeData[iy].GetAbsDay() > 1)
				{
					// wenn diese Schicht mehr als ein Tag Abstand zur n�chsten Schicht hat, 
					// gen�gt es, um mit dem Z�hlen abzubrechen (dann ist genug Raum, um eine Pause einzuf�gen)
					break;
				}
				else
				{
					if(omWorkTimeData[iy].GetAbsDay() != omWorkTimeData[ilLastShift].GetAbsDay())
					{ 
						ilLastShift = iy;
						ilShifts++;
					}
				}
			}
		}
	}

	// nach oben z�hlen
	for(iy=piX[3]+1, ilLastShift=piX[2]; iy<ilSize; iy++)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		if(omWorkTimeData[iy].cmType & (WTE_FREE | WTE_R_FREE | WTE_SLEEP_DAY)) break;
		if(omWorkTimeData[iy].cmType == WTE_SHIFT) 
		{
			if(omWorkTimeData[iy].GetAbsDay() - omWorkTimeData[ilLastShift].GetAbsDay() > 1)
			{
				// wenn diese Schicht mehr als ein Tag Abstand zur n�chsten Schicht hat, 
				// gen�gt es, um mit dem Z�hlen abzubrechen (dann ist genug Raum, um eine Pause einzuf�gen)
				break;
			}
			else
			{
				if(omWorkTimeData[iy].GetAbsDay() != omWorkTimeData[ilLastShift].GetAbsDay())
				{ 
					ilLastShift = iy;
					ilShifts++;
				}
			}
		}
	}

	// Checken
	if(blCheckID_WOR_MAWORD && ilShifts > (int)pomShiftCheck->omRules[ID_WOR_MAWORD])
	{
		// Fehler: die Wochen-Arbeitszeit ist �berschritten
		return AddErrorMessage(ID_WOR_MAWORD, piX[2], RET_ERROR);
	}
	else if(blCheckID_WOR_MAWORDT && ilShifts > (int)pomShiftCheck->omRules[ID_WOR_MAWORDT])
	{
		// Warnung: die Wochen-Arbeitszeit ist �berschritten
		ilRet = AddErrorMessage(ID_WOR_MAWORDT, piX[2], RET_WARNING);
	}

	return ilRet;
}

/**************************************************************************************
Pr�fung:
ID_WOR_MAFREED
3. Freie Tage in Folge
Die Anzahl der Freien Tage in Folge darf in ihrer Gesamtzahl n Tage nicht 
�berschreiten. Das System mu� beim Anlegen einer Abwesenheit die Anzahl der 
Tage in Folge erkennen und �berschreitungen mit dem entsprechenden H k�nnen. 
Gez�hlt werden alle Abwesenheitscodes. Teilzeit Plus - Schichten sind zus�tzlich
m�glich und werden nicht bewertet

Parameter: Freie Tage in Folge, ID_WOR_MAFREED

ID_WOR_MAFREEDT
4. Freie Tage in Folge toleriert
Wenn die zu leistenden Freien Tage in Folge eine Gesamtzahl von n Tagen 
unterschreitet, soll das System dies beim Anlegen einer Abwesenheit mit 
einer entsprechenden Hinweiswarnung anzeigen. Gez�hlt werden alle Abwesenheitscodes. 
Teilzeit Plus - Schichten sind zus�tzlich m�glich und werden nicht bewertet

Parameter: Freie Tage in Folge toleriert, ID_WOR_MAFREEDT
**************************************************************************************/
int CedaWorkTime::Check_ID_WOR_MAFREEDT(DRRDATA* prpDrr, int* piX)
{
	if(!pomShiftCheck->bmDoConfictCheck) return RET_NO_ERROR;	// alle Pr�fungen sind abgeschaltet
	bool blCheckID_WOR_MAFREED = pomShiftCheck->omRules.GetAt(ID_WOR_MAFREED) != -1;
	bool blCheckID_WOR_MAFREEDT = pomShiftCheck->omRules.GetAt(ID_WOR_MAFREEDT) != -1;
	if(!blCheckID_WOR_MAFREED && !blCheckID_WOR_MAFREEDT) return RET_NO_ERROR;	// alle Pr�fungen k�nnen durch -1 ausgeschaltet werden
	if(prpDrr->Avto < pomShiftCheck->omCheckDateFrom || prpDrr->Avfr > pomShiftCheck->omCheckDateTo) return RET_NO_ERROR;

	int ilRet = RET_NO_ERROR;
	int	ilSize = omWorkTimeData.GetSize();

	int iy;

	int ilFreeDays;
	if (omWorkTimeData[piX[2]].cmType & WTE_R_FREE)
	{
		ilFreeDays = 1;	// diese Schicht inklusive
	}
	else
	{
		ilFreeDays = 0;
	}

	int ilLastFree;

	// nach unten z�hlen
	for(iy=piX[2]-1, ilLastFree=piX[2]; iy>=0; iy--)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		if(omWorkTimeData[iy].cmType == WTE_SHIFT) break;
		if(omWorkTimeData[iy].cmType & (/*WTE_FREE |*/ WTE_R_FREE)) 
		{
			if(omWorkTimeData[ilLastFree].GetAbsDay() > omWorkTimeData[iy].GetAbsDay() + 1)
			{
				// wenn zwischen diesem und vorherigem Free mindestens ein Tag Abstand ist, 
				// gen�gt es, um mit dem Z�hlen abzubrechen (dann ist genug Raum, um eine Schicht einzuf�gen)
				break;
			}
			else
			{
				ilLastFree = iy;
				ilFreeDays++;
			}
		}
	}

	// nach oben z�hlen
	for(iy=piX[3]+1, ilLastFree=piX[2]; iy<ilSize-1; iy++)
	{
		if(omWorkTimeData[iy].cmState == S_DELETED) continue;
		if(omWorkTimeData[iy].cmType == WTE_SHIFT) break;
		if(omWorkTimeData[iy].cmType & (/*WTE_FREE |*/ WTE_R_FREE)) 
		{
			if(omWorkTimeData[iy].GetAbsDay() > omWorkTimeData[ilLastFree].GetAbsDay() + 1)
			{
				// wenn zwischen diesem und n�chstem Free mindestens ein Tag Abstand ist, 
				// gen�gt es, um mit dem Z�hlen abzubrechen (dann ist genug Raum, um eine Schicht einzuf�gen)
				break;
			}
			else
			{
				ilLastFree = iy;
				ilFreeDays++;
			}
		}
	}

	// Checken
	if(blCheckID_WOR_MAFREED && ilFreeDays > (int)pomShiftCheck->omRules[ID_WOR_MAFREED])
	{
		// Fehler: die Anzahl der freien Tage ist �berschritten
		return AddErrorMessage(ID_WOR_MAFREED, piX[2], RET_ERROR);
	}
	else if(blCheckID_WOR_MAFREEDT && ilFreeDays > (int)pomShiftCheck->omRules[ID_WOR_MAFREEDT])
	{
		// Warnung: die Anzahl der freien Tage ist zu gro�
		ilRet = AddErrorMessage(ID_WOR_MAFREEDT, piX[2], RET_WARNING);
	}

	return ilRet;
}

/**************************************************************************************
Stellt die Art der �nderung fest
	int ShiftTime_xxx
**************************************************************************************/
int CedaWorkTime::GetDataChange(DRRDATA* prpDrr, int ipDDXType)
{
	int ilDataChange = OT_NONE;
	int ilTbsd = CODE_UNDEFINED;
	int ilCode = CODE_UNDEFINED;
	bool blWork = false;

	// 1. Feststellen, ob die �nderung zur Vergr��erung, Verkleinerung oder �nderung der Arbeitszeit f�hrt
	switch(ipDDXType)
	{
	default:
		break;
	case DRR_MULTI_CHANGE:	// sollte nicht vorkommen, wird ja fr�her durch DRR_CHANGE ersetzt
	case DRR_CHANGE:
	case DRR_NEW:
	case DRR_DELETE:
		if(!prpDrr) return ilDataChange;

		ilCode = ogOdaData.IsODAAndIsRegularFree(CString(prpDrr->Scod), &ilTbsd, &blWork);
		if(blWork)
		{
			ilCode = CODE_IS_BSD;	// k�nstlicher Griff: ODA mit ODA.Work == 1 soll als Schicht behandelt werden
		}


		
		switch(ilCode)
		{
		default:
			return OT_NONE;

		case CODE_IS_BSD:		// keine Abwesenheit
			// ist es Teilzeit Plus oder Dummy?
			if(!blWork)
			{
				if(prpDrr->Avfr == prpDrr->Avto)
				{
					// Das ist ein Dummy-Eintrag
					// f�r einen nicht FREE-DRR gleiche Zeiten haben keinen Sinn
					return OT_NONE;
				}
				
				if(	ogDrrData.IsExtraShift(CString(prpDrr->Drs4)) || 
					ogDrrData.IsExtraShift(CString(prpDrr->Drs1)) || 
					ogDrrData.IsExtraShift(CString(prpDrr->Drs3)))
				{
					// Das ist ganz oder zum Teil eine TeilzeitPlus-Schicht
					switch (ipDDXType)
					{
					default:
						break;
					case DRR_MULTI_CHANGE:	// sollte nicht vorkommen, wird ja fr�her durch DRR_CHANGE ersetzt
					case DRR_CHANGE:
						ilDataChange = OT_DRR_SHIFT_PLUS_CHANGE;
						break;
					case DRR_NEW:
						ilDataChange = OT_DRR_SHIFT_PLUS_NEW;
						break;
					case DRR_DELETE:
						ilDataChange = OT_DRR_SHIFT_PLUS_DELETE;
						break;
					}
				}
			}
			
			if(ilDataChange == OT_NONE)
			{
				// kein SHIFT_PLUS gesetzt
				switch (ipDDXType)
				{
				default:
					break;
				case DRR_MULTI_CHANGE:	// sollte nicht vorkommen, wird ja fr�her durch DRR_CHANGE ersetzt
				case DRR_CHANGE:
					ilDataChange = OT_DRR_SHIFT_CHANGE;
					break;
				case DRR_NEW:
					ilDataChange = OT_DRR_SHIFT_NEW;
					break;
				case DRR_DELETE:
					ilDataChange = OT_DRR_SHIFT_DELETE;
					break;
				}
			}
			break;
		case CODE_IS_ODA_FREE:	// <opCode> ist ODA-Datensatz, Flag ist NICHT gesetzt - nicht regul�r
			// FREE-Schicht
			switch (ipDDXType)
			{
			default:
				break;
			case DRR_MULTI_CHANGE:	// sollte nicht vorkommen, wird ja fr�her durch DRR_CHANGE ersetzt
			case DRR_CHANGE:
				ilDataChange = OT_DRR_FREE_CHANGE;
				break;
			case DRR_NEW:
				ilDataChange = OT_DRR_FREE_NEW;
				break;
			case DRR_DELETE:
				ilDataChange = OT_DRR_FREE_DELETE;
				break;
			}
			break;
		case CODE_IS_ODA_REGULARFREE:				// Code ist ODA, Flag ist gesetzt
			// R_FREE-Schicht
			switch (ipDDXType)
			{
			default:
				break;
			case DRR_MULTI_CHANGE:	// sollte nicht vorkommen, wird ja fr�her durch DRR_CHANGE ersetzt
			case DRR_CHANGE:
				ilDataChange = OT_DRR_R_FREE_CHANGE;
				break;
			case DRR_NEW:
				ilDataChange = OT_DRR_R_FREE_NEW;
				break;
			case DRR_DELETE:
				ilDataChange = OT_DRR_R_FREE_DELETE;
				break;
			}
			break;
		}

		break;
	case DRA_NEW:
		ilDataChange = OT_DRA_NEW;
		break;
	case DRA_CHANGE:
		ilDataChange = OT_DRA_CHANGE;
		break;
	case DRA_DELETE:
		ilDataChange = OT_DRA_DELETE;
		break;
	}
	return ilDataChange;
}



/**************************************************************************************
ipIndex zeigt auf die getroffene Position
R�ckgabe - ein von RET_i enum-s
abh�ngig vom ipWTE �ndern sich die Messages
**************************************************************************************/
int CedaWorkTime::AddErrorMessage(int ipTest, int ipIndex, int ipErr, int ipWTE/*=WTE_NONE*/)
{
	_ASSERTE(pomShiftCheck != 0);
	if(!pomShiftCheck) return RET_SOFTWARE_ERROR;
	
	// Wird es als Fehlermeldung oder Warnung ausgegeben? - kann alles kombiniert werden
	bool blIsError =	(pomShiftCheck->imOutput & OUT_ERROR_AS_ERROR) && ipErr == RET_ERROR ||
						(pomShiftCheck->imOutput & OUT_WARNING_AS_ERROR) && ipErr == RET_WARNING;
							
	bool blIsWarning =	(pomShiftCheck->imOutput & OUT_ERROR_AS_WARNING) && ipErr == RET_ERROR ||
						(pomShiftCheck->imOutput & OUT_WARNING_AS_WARNING) && ipErr == RET_WARNING;
		
	if(!blIsError && !blIsWarning) return ipErr;	// gar keine Meldung

	if(blIsError)
	{
		if(!pomErrors)
		{
			pomErrors = new CStringArray;
			if(!pomErrors)
				return RET_SOFTWARE_ERROR;
		}
	}

	if(blIsWarning)
	{
		if(!pomWarnings)
		{
			pomWarnings = new CStringArray;
			if(!pomWarnings)
				return RET_SOFTWARE_ERROR;
		}
	}

	int ilValue = 0;
	DWORD dlTestId;

	switch(ipTest)
	{
	default:
		_ASSERTE(FALSE);
		return RET_SOFTWARE_ERROR;
		break;
	case ID_WOR_SPACE:
		if(ipWTE & WTE_SHIFT)
		{
			// Arbeitszeiten �berschneiden sich!*INTXT*
			dlTestId = IDS_STRING1654;
		}
		else if(ipWTE & (WTE_FREE | WTE_R_FREE))
		{
			// Abwesenheiten �berschneiden sich!*INTXT*
			dlTestId = IDS_STRING186;
		}
		else if(ipWTE & WTE_DRA_FREE)
		{
			// Untert�gige Abwesenheiten �berschneiden sich!*INTXT*
			dlTestId = IDS_STRING187;
		}
		else
		{
			// Arbeitszeiten �berschneiden sich!*INTXT*
			dlTestId = IDS_STRING1654;
		}
		break;
	case ID_WOR_FREEH:
		// T�gliche Ruhezeit unterschritten! (%d h)*INTXT*
		dlTestId = IDS_STRING1626;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_FREEHT:
		// t�gliche Ruhezeit < %d h*INTXT*
		dlTestId = IDS_STRING1627;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_RESTH:
		// Freier Tag: min. Ruhezeit unterschritten! (%d h)*INTXT*
		if ("SIN" == ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER"))
			dlTestId = IDS_STRING11828;
		else
			dlTestId = IDS_STRING1628;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_RESTDW:
		// Min. Freie Tage unterschritten! (%d pro Woche)*INTXT*
		dlTestId = IDS_STRING1629;
		ilValue = pomShiftCheck->omRules[ipTest];
		break;
	case ID_WOR_MAWORH:
		// Max. t�gliche Arbeitszeit �berschritten! (%d h)*INTXT*
		dlTestId = IDS_STRING1640;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_MAWORHT:
		// max. t�gliche Arbeitszeit > %d h*INTXT*
		dlTestId = IDS_STRING1645;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_WORHWT:
		// Wochenarbeitszeit > %d h*INTXT*
		dlTestId = IDS_STRING1646;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_MIWORH:
		// Min. t�gliche Arbeitszeit unterschritten! (%d h)*INTXT*
		dlTestId = IDS_STRING1647;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_MIWORHT:
		// min. t�gliche Arbeitszeit < %d h*INTXT*
		dlTestId = IDS_STRING1648;
		ilValue = pomShiftCheck->omRules[ipTest]/60;
		break;
	case ID_WOR_MAWORD:
		// Max. Arbeitstage in Folge �berschritten! (%d)*INTXT*
		dlTestId = IDS_STRING1649;
		ilValue = pomShiftCheck->omRules[ipTest];
		break;
	case ID_WOR_MAWORDT:
		// Arbeitstage in Folge > %d*INTXT*
		dlTestId = IDS_STRING1650;
		ilValue = pomShiftCheck->omRules[ipTest];
		break;
	case ID_WOR_MAFREED:
		// Freie Tage in Folge �berschritten! (%d)*INTXT*
		dlTestId = IDS_STRING1651;
		ilValue = pomShiftCheck->omRules[ipTest];
		break;
	case ID_WOR_MAFREEDT:
		// freie Tage in Folge > %d*INTXT*
		dlTestId = IDS_STRING1652;
		ilValue = pomShiftCheck->omRules[ipTest];
		break;
	}

	// Bevor die Meldung ausgegeben wird, checken wir grunds�tzlich die M�glichkeit, eine Teilzeit+ anzubieten
	CheckTeilzeitPlus(ipTest, ipIndex, ipErr);

	CString olName;
	CString olTime;

	if(pomShiftCheck->bmShiftRosterCheck)
	{
		// ShiftRosterCheckIt()-Aufruf
		// getting the week-no. of the ShiftRoster
		// TAKE CARE: if the week is bigger than the ShiftRoster-periode, then it is one of the
		// generated weeks at the end of the ShiftRoster to detect violations of the ShiftRosterChange

		COleDateTimeSpan olDayDif = omWorkTimeData[ipIndex].omTime - pomShiftCheck->omCheckDateFrom;
		olName.Format("%s %d",LoadStg(SHIFT_S_WEEK), (int)(olDayDif.GetDays() / 7) + 1);

		if (ipTest == ID_WOR_SPACE  || ipTest == ID_WOR_RESTH   ||
			ipTest == ID_WOR_MAWORH || ipTest == ID_WOR_MAWORHT || 
			ipTest == ID_WOR_MIWORH || ipTest == ID_WOR_MIWORHT)
		{
			DWORD dwlDay = 0;
			switch(omWorkTimeData[ipIndex].omTime.GetDayOfWeek())
			{
			case 2:	dwlDay = SHIFT_S_MO;	break;	// Mo
			case 3:	dwlDay = SHIFT_S_TU;	break;	// Di
			case 4:	dwlDay = SHIFT_S_WE;	break;	// Mi
			case 5:	dwlDay = SHIFT_S_TH;	break;	// Do
			case 6:	dwlDay = SHIFT_S_FR;	break;	// Fr
			case 7:	dwlDay = SHIFT_S_SA;	break;	// Sa
			case 1:	dwlDay = SHIFT_S_SU;	break;	// So
			}

			olTime = LoadStg(dwlDay);

			// add shift times only for this error (PRF 3670)
			if (ipTest == ID_WOR_MAWORHT || ipTest == ID_WOR_MAWORH)
			{
				CString olFrom;
				CString olTo;
				olFrom = omWorkTimeData[ipIndex].omTime.Format("%H:%M");
				olTo = omWorkTimeData[ipIndex+1].omTime.Format("%H:%M");
				olTime += ", " + olFrom + " - " + olTo;
			}
		}
		else if (ipTest == ID_WOR_FREEH || ipTest == ID_WOR_FREEHT)
		{
			// gem. PRF 3670 Punkt 1 des Testprotokolls sollen beide Schichten
			// (d.h. Ende Schicht 1, Anfang Schicht 2) ausgegeben werden.
			DWORD dwlDay1 = 0;
			DWORD dwlDay2 = 0;

			switch(omWorkTimeData[ipIndex].omTime.GetDayOfWeek())
			{
			case 2:	dwlDay1 = SHIFT_S_MO;	break;	// Mo
			case 3:	dwlDay1 = SHIFT_S_TU;	break;	// Di
			case 4:	dwlDay1 = SHIFT_S_WE;	break;	// Mi
			case 5:	dwlDay1 = SHIFT_S_TH;	break;	// Do
			case 6:	dwlDay1 = SHIFT_S_FR;	break;	// Fr
			case 7:	dwlDay1 = SHIFT_S_SA;	break;	// Sa
			case 1:	dwlDay1 = SHIFT_S_SU;	break;	// So
			}

			switch(omWorkTimeData[ipIndex+1].omTime.GetDayOfWeek())
			{
			case 2:	dwlDay2 = SHIFT_S_MO;	break;	// Mo
			case 3:	dwlDay2 = SHIFT_S_TU;	break;	// Di
			case 4:	dwlDay2 = SHIFT_S_WE;	break;	// Mi
			case 5:	dwlDay2 = SHIFT_S_TH;	break;	// Do
			case 6:	dwlDay2 = SHIFT_S_FR;	break;	// Fr
			case 7:	dwlDay2 = SHIFT_S_SA;	break;	// Sa
			case 1:	dwlDay2 = SHIFT_S_SU;	break;	// So
			}

			CString olFrom;
			CString olTo;
			olFrom = omWorkTimeData[ipIndex].omTime.Format("%H:%M");
			olTo = omWorkTimeData[ipIndex+1].omTime.Format("%H:%M");
			olTime = LoadStg(dwlDay1) + " (" + olFrom + ") - " + LoadStg(dwlDay2) + " (" + olTo + ")";
		}
	}
	else
	{
		// DutyRosterCheckIt()-Aufruf
		olName = ogStfData.GetName(lmStfu, imEName);
		olTime = omWorkTimeData[ipIndex].omTime.Format("%d.%m.%y %H:%M");
	}

	CString olError;
	olError.Format(LoadStg(dlTestId), ilValue);
	CString olMessage;

	if(olTime.IsEmpty())
	{
		// Name: Fehlermeldung
		// <%s>: %s
		olMessage.Format("%s:  %s", olName, olError);
	}
	else
	{
		// Name / Datum: Fehlermeldung
		// <%s>/<%s>:  %s
		olMessage.Format("%s / %s:  %s", olName, olTime, olError);
	}

	if(blIsError)
	{
		pomErrors->Add(olMessage);
	}
	
	// wenn Fehler als Warnung ausgeht (in die Liste) f�gen wir "Fehler: " zuvor ein
	if((pomShiftCheck->imOutput & OUT_ERROR_AS_WARNING) && ipErr == RET_ERROR)
	{
		//                  "Fehler:*INTXT*"
		olMessage = LoadStg(IDS_STRING535) + " " + olMessage; 
	}

	if(blIsWarning)
	{
		pomWarnings->Add(olMessage);
		// ipErr = RET_WARNING;
	}
	//else if(blIsError)
	//{
		// ipErr = RET_ERROR;
	//}

#ifdef TRACE_ALL_CHECKFUNC
	TRACE("AddErrorMessage(): %s\n", olMessage);
#endif TRACE_ALL_CHECKFUNC

	return ipErr;
}

/*************************************************************************************
wenn Teilzeit+ nicht m�glich ist, setzt man bmEnableTeilzeitplusQuestion auf false
*************************************************************************************/
void CedaWorkTime::CheckTeilzeitPlus(int ipTest, int ipIndex, int ipErr)
{
	if(omWorkTimeData[ipIndex].cmType & WTE_SHIFT_ALL)
	{
		if(omWorkTimeData[ipIndex].cmTimeType == TIME_ZERO)
		{
			pomShiftCheck->bmEnableTeilzeitplusQuestion = false;
			return;
		}
	}
	else if(omWorkTimeData[ipIndex].cmType & WTE_END)
	{
		for(int ilCount=ipIndex-1; ilCount>=0; ilCount--)
		{
			if(omWorkTimeData[ilCount].cmState == S_DELETED) continue;
			if(omWorkTimeData[ipIndex].lmUrno == omWorkTimeData[ilCount].lmUrno)
			{
				if(omWorkTimeData[ilCount].cmTimeType == TIME_ZERO)
				{
					pomShiftCheck->bmEnableTeilzeitplusQuestion = false;
					return;
				}

				break;
			}
		}
	}

	switch(ipTest)
	{
	default:
		break;
		// alle Regeln, die TeilzeitPlus beachten
	case ID_WOR_SPACE:
		// Die Arbeitszeiten d�rfen sich nicht �berlappen
	case ID_WOR_FREEH:
		// Minimale Anzahl freier Stunden
		pomShiftCheck->bmEnableTeilzeitplusQuestion = false;
		break;
	}
}

#ifdef _DEBUG
#ifdef TRACE_ALL_CHECKFUNC
#undef TRACE_ALL_CHECKFUNC
#endif TRACE_ALL_CHECKFUNC
#endif _DEBUG










