#include <stdafx.h>

CedaSdtData::CedaSdtData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(SDTDATA, SdtDataRecInfo)
		FIELD_CHAR_TRIM(Bsdc, "BSDC")	
		FIELD_CHAR_TRIM(Dnam, "DNAM")	
		FIELD_CHAR_TRIM(Type, "TYPE")	
		FIELD_LONG	   (Urno, "URNO")	
		FIELD_DATE	   (Bkf1, "BKF1")	
		FIELD_DATE	   (Bkt1, "BKT1")	
		FIELD_DATE	   (Sbgi, "SBGI")	
		FIELD_DATE	   (Seni, "SENI")	
		FIELD_DATE	   (Esbg, "ESBG")	
		FIELD_DATE	   (Lsen, "LSEN")	
		FIELD_DATE	   (Brkf, "BRKF")
		FIELD_DATE	   (Brkt, "BRKT")	
		FIELD_LONG	   (Bsdu, "BSDU")	
		FIELD_LONG	   (Sdgu, "SDGU")	
		FIELD_CHAR_TRIM(Fctc, "FCTC")	
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SdtDataRecInfo)/sizeof(SdtDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SdtDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"SDT");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"BSDC,DNAM,TYPE,"
						   "URNO,BKF1,BKT1,SBGI,SENI,ESBG,LSEN,"
						   "BRKF,BRKT,BSDU,SDGU,FCTC");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	ClearAll();

}

//************************************************************************************
//
//************************************************************************************

void CedaSdtData::Register(void)
{
CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// STD-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_SDT_CHANGE, "CedaSdtData", "BC_SDT_CHANGE",        ProcessSdtCf);
	DdxRegister((void *)this,BC_SDT_NEW,    "CedaSdtData", "BC_SDT_NEW",		   ProcessSdtCf);
	DdxRegister((void *)this,BC_SDT_DELETE, "CedaSdtData", "BC_SDT_DELETE",        ProcessSdtCf);

	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL;
}

//************************************************************************************
//
//************************************************************************************

CedaSdtData::~CedaSdtData()
{
	ClearAll();
}	

//******************************************************************************************
// Wird in ShiftRoster_View benutzt, es werden offensichtlich nur bestimmte Daten eingelesen
//******************************************************************************************

bool CedaSdtData::ReadSpecialData(CCSPtrArray<SDTDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/, bool bpRegisterBC/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	// Ist die �bergebende Feldliste g�ltig
	if(strlen(pspFieldList) > 0)
	{
		// Hier wird angegeben welche Felder gelesen werden sollen
		// Bei * werden alle genommen
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			// �bergebene Feldliste wird benutzt
			strcpy(pclFieldList, pspFieldList);
		}
	}

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}

	// Wenn der Pointer Array g�ltig ist,
	int ilCountRecord = 0;
	if(popSdt != NULL)
	{
		// Solange wie der R�ckgabewert TRUE ist weitermachen
		for (ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			// Einen SDT Datensatz erzeugen.
			SDTDATA *prpSdt = new SDTDATA;
			// Datensatz f�llen
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpSdt,CString(pclFieldList))) == true)
			{
				// Datensatz ins Array einf�gen
				popSdt->Add(prpSdt);
			}
			else
			{
				// letzten Datensatz l�schen, falls ung�ltig
				delete prpSdt;
			}
		}

	// Wenn nichts eingetragen
		if(popSdt->GetSize() == 0) return false;
	}
	ClearFastSocketBuffer();	
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();
        
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//************************************************************************************
// Einlesen der gesamten Informationen (???) Wird bisher im Rostering nicht benutzt.
//************************************************************************************

bool CedaSdtData::Read(char *pspWhere /*NULL*/, bool bpRegisterBC/*=true*/, bool bpClearAll/*=true*/)
{
	bool blRc = true;

	// alle Daten l�schen
	if (bpClearAll)
	{
		ClearAll();
	}

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
			return false;
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
			return false;
	}

	for (int ilCountRecord = 0; blRc == true; ilCountRecord++)
    {
		// Datenhaltungs Struct
		SDTDATA *prlSdt = new SDTDATA;

		if ((blRc = GetFirstBufferRecord2(prlSdt))==true) 
		{
			prlSdt->bmIsChanged = DATA_UNCHANGED;
			if(AddSdtInternal(prlSdt)==false)
			{
				if(IsTraceLoggingEnabled())
				{
					GetDefectDataString(ogRosteringLogText, (void*)prlSdt);
					WriteInRosteringLog(LOGFILE_TRACE);
				}
				delete prlSdt;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlSdt);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlSdt;
			TRACE("Deleted extra prlSdt\n");
		}
	}
	ClearFastSocketBuffer();	

	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}


	return true;
}

//************************************************************************************
// l�schen aller Daten diese Klasse
//************************************************************************************

bool CedaSdtData::ClearAll()
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();

	// beim BC-Handler abmelden
	if(bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    
	return true;
}

//************************************************************************************
//
//************************************************************************************

bool CedaSdtData::AddSdtInternal(SDTDATA *prpSdt, bool bpWithDDX /*= false*/)
{
	SDTDATA *prlSdt = NULL;
	if(omUrnoMap.Lookup((void *)prpSdt->Urno,(void *&)prlSdt)==FALSE)
	{
		omData.Add(prpSdt);
		omUrnoMap.SetAt((void *)prpSdt->Urno,prpSdt);
		if(bpWithDDX == true)
		{
			ogDdx.DataChanged((void *)this,SDT_NEW,(void *)prpSdt);
		}
		return true;
	}
	else
	{
		return false;
	}
}

//************************************************************************************
// Update data methods (called from PrePlanTable class)
//************************************************************************************

bool CedaSdtData::DeleteSdt(SDTDATA *prpSdtData, BOOL bpWithSave)
{

	bool olRc = true;
	if(prpSdtData->bmIsChanged == DATA_UNCHANGED)
	{
		prpSdtData->bmIsChanged = DATA_DELETED;
	}
	ogDdx.DataChanged((void *)this,SDT_DELETE,(void *)prpSdtData);
	
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		olRc = SaveSdt(prpSdtData);
	}
	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdtData::InsertSdt(SDTDATA *prpSdtData, BOOL bpWithSave)
{
	bool olRc = true;

	if(prpSdtData->bmIsChanged == DATA_UNCHANGED)
	{
		prpSdtData->bmIsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		SaveSdt(prpSdtData);
		//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
	ogDdx.DataChanged((void *)this,/*SDT_NEW*/SDT_CHANGE,(void *)prpSdtData);
	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdtData::UpdateSdt(SDTDATA *prpSdtData, BOOL bpWithSave)
{
	bool olRc = true;

	ogDdx.DataChanged((void *)this,SDT_CHANGE,(void *)prpSdtData);
	if(prpSdtData->bmIsChanged == DATA_UNCHANGED)
	{
		prpSdtData->bmIsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olDummy;
		SaveSdt(prpSdtData);
	}
	return olRc;
}

//**********************************************************************************
//
//**********************************************************************************

bool CedaSdtData::SdtExist(long Urno)
{
	// don't read from database anymore, just check internal array
	SDTDATA *prlData;
	bool olRc = true;

	if(omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == FALSE)
	{
		olRc = false;
	}
	return olRc;
}


//**********************************************************************************
// the callback function for storing broadcasted fligthdata in FLIGHTDATA array
//**********************************************************************************

void  ProcessSdtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaSdtData *polSdtData = (CedaSdtData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polSdtData->ProcessSdtBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	RemoveWaitCursor();
}

//**********************************************************************************
// 
//**********************************************************************************

void  CedaSdtData::ProcessSdtBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	CCS_TRY;

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	SDTDATA *prlSdt = NULL;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	case BC_SDT_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlSdt = GetSdtByUrno(llUrno);
			if(prlSdt != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlSdt,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateSdtInternal(prlSdt);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_SDT_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlSdt = new SDTDATA;
			GetRecordFromItemList(prlSdt,prlBcStruct->Fields,prlBcStruct->Data);
			AddSdtInternal(prlSdt, SDT_SEND_DDX);
		}
		break;
	case BC_SDT_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz in der internen Datenhaltung enthalten ist
			prlSdt = GetSdtByUrno(llUrno);
			if (prlSdt != NULL)
			{
				ogDdx.DataChanged((void *)this,SDT_DELETE,(void *)prlSdt);
				// ja -> Datensatz l�schen
				DeleteSdtInternal(prlSdt);
			}
			else
			{
				DeleteSdtBySdgu(llUrno);
			}
		}
		break;
	default:
		break;
	}
CCS_CATCH_ALL;
}

//**********************************************************************************
// 
//**********************************************************************************

SDTDATA * CedaSdtData::GetSdtByUrno(long pcpUrno)
{
	SDTDATA *prpSdt;

	if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prpSdt) == TRUE)
	{
		return prpSdt;
	}
	return NULL;
}


//**********************************************************************************
// 
//**********************************************************************************

bool CedaSdtData::DeleteSdtInternal(SDTDATA *prpSdt)
{
	omUrnoMap.RemoveKey((void *)prpSdt->Urno);

	int ilSdtCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilSdtCount-1; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpSdt->Urno)
		{
			omData.DeleteAt(ilCountRecord);
		}
	}
    return true;
}

//**********************************************************************************
// 
//**********************************************************************************

bool CedaSdtData::SaveSdt(SDTDATA *prpSdt)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if (prpSdt->bmIsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpSdt->bmIsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSdt);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpSdt->bmIsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSdt->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSdt);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpSdt->bmIsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpSdt->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
		}
		break;
	}
   return olRc;
}

//**********************************************************************************
// 
//**********************************************************************************

void CedaSdtData::GetSdtByBsdu(long lpBsdu,CCSPtrArray<SDTDATA> *popSdtData)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Bsdu == lpBsdu)
		{
			popSdtData->Add(prlSdt);
		}
	}
}

//**********************************************************************************
// 
//**********************************************************************************

void CedaSdtData::GetSdtBySdgu(long lpSdgu,CCSPtrArray<SDTDATA> *popSdtData)
{
	POSITION rlPos;
	for (rlPos = omUrnoMap.GetStartPosition(); popSdtData!=NULL && rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Sdgu == lpSdgu)
		{
			popSdtData->Add(prlSdt);
		}
	}
}


//**********************************************************************************
// 
//**********************************************************************************

void CedaSdtData::DeleteSdtBySdgu(long lpSdgUrno)
{
	SDTDATA *prlSdt;

	long llSdtCount = omData.GetSize() -1;
	for (long l = llSdtCount; l > -1; l--)
	{
		prlSdt = &omData[l];
		if (prlSdt->Sdgu == lpSdgUrno)
		{
			omUrnoMap.RemoveKey((void *)prlSdt->Urno);
			omData.DeleteAt(l);
		}
	}
}

//**********************************************************************************
// 
//**********************************************************************************

bool CedaSdtData::UpdateSdtInternal(SDTDATA *prpSdt)
{
	bool blRet = true;
	if(prpSdt!=NULL)
	{
		SDTDATA* prlSdt = GetSdtByUrno(prpSdt->Urno);
		if(prlSdt==NULL)
		{
			blRet = false;
		}
		else
		{
			*prlSdt = *prpSdt;
			//uhi 6.9.00
			ogDdx.DataChanged((void *)this,SDT_CHANGE,(void *)prpSdt);
		}
	}
	else
	{
		blRet = false;
	}
    return blRet;
}


bool CedaSdtData::ReloadSdtBySdgu(long lpSdgu)
{
CCS_TRY
	// delete already loaded SDTs
	DeleteSdtBySdgu (lpSdgu);

	// getting the actual view for building the where-statement
	DutyRoster_View	*polDutyView = NULL;
	CWinApp *pApp = AfxGetApp();
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL && polDutyView == NULL)
	{
		CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
		POSITION p1 = pDocTempl->GetFirstDocPosition();
		while(p1 != NULL && polDutyView == NULL)
		{
			CDocument *pDoc = pDocTempl->GetNextDoc(p1);
			POSITION p2 = pDoc->GetFirstViewPosition();
			while (p2 != NULL && polDutyView == NULL)
			{
				polDutyView = DYNAMIC_DOWNCAST(DutyRoster_View,pDoc->GetNextView(p2));
								
			}
		}
	}

	if (polDutyView != NULL)
	{
		if (polDutyView->rmViewInfo.bCorrectViewInfo)
		{
			if (polDutyView->rmViewInfo.oPfcStatFctcs.IsEmpty() == FALSE)
			{
				CString	olSdtWhere;
				// Filterkriterium Start- und Enddatum f�r Datens�tze
				CString olDateHFrom  = polDutyView->rmViewInfo.oDateFrom.Format("%Y%m%d000000");
				CString olDateHTo    = polDutyView->rmViewInfo.oDateTo.Format("%Y%m%d235959");
				// Filterkriterium ausgew�hlte Funtionen
				CString omTmpFkts	 = polDutyView->rmViewInfo.oPfcStatFctcs;
				// WHERE-Clause generieren
				olSdtWhere.Format("WHERE SDGU = '%ld' AND FCTC IN (%s) AND ESBG BETWEEN '%s' AND '%s'", lpSdgu, omTmpFkts, olDateHFrom, olDateHTo);

				// Datens�tze abfragen
				if (ogSdtData.Read((char*)(LPCTSTR)olSdtWhere, true, false) == false)
				{
					// Fehler -> echter Fehler (anderer als 'Keine Datens�tze gefunden')?
					ogSdtData.omLastErrorMessage.MakeLower();
					if (ogSdtData.omLastErrorMessage.Find("no data found") == -1)
					{
						// ja -> ausgeben
						CString olErrorTxt;
						olErrorTxt.Format("%s %d\n%s",LoadStg(ST_READERR), ogSdtData.imLastReturnCode, ogSdtData.omLastErrorMessage);
						MessageBox (NULL, olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
					}
				}
			}
			polDutyView->pomDebitTabViewer->ChangeView(true);
		}
	}

	return true;
CCS_CATCH_ALL
	return false;
}