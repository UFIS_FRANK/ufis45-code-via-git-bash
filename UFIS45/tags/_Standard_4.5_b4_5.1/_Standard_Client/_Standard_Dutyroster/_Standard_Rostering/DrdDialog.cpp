// DrdDialog.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDrdDialog 


CDrdDialog::CDrdDialog(CWnd* pParent,DRDDATA* popDrdData,DRRDATA* popDrrData,CMapPtrToPtr* popDrdMap,bool bpCheckMainFuncOnly)
	: CDialog(CDrdDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDrdDialog)
	//}}AFX_DATA_INIT

	pomDrdData = popDrdData;
	pomDrrData = popDrrData;
	pomDrdMap  = popDrdMap;
	bmCheckMainFuncOnly = bpCheckMainFuncOnly;
}

//********************************************************************************
//
//********************************************************************************

CDrdDialog::~CDrdDialog()
{
}

//********************************************************************************
//
//********************************************************************************

void CDrdDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDrdDialog)
	DDX_Control(pDX, IDC_LB_LB, m_LB_Perc);
	DDX_Control(pDX, IDC_LB_LB_ADDITIONAL, m_LB_PercAdditional);
	DDX_Control(pDX, IDC_CB_ORG, m_CB_Org);
	DDX_Control(pDX, IDC_CB_FUNCTION, m_CB_Function);
	DDX_Control(pDX, IDC_AVFA_TIME3, m_TimeTo);
	DDX_Control(pDX, IDC_AVFA_TIME2, m_TimeFrom);
	DDX_Control(pDX, IDC_AVFA_DATE2, m_DateFrom);
	DDX_Control(pDX, IDC_AVFA_DATE3, m_DateTo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDrdDialog, CDialog)
	//{{AFX_MSG_MAP(CDrdDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//********************************************************************************
//
//********************************************************************************

BOOL CDrdDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// -------------------------------------------------------------------------------
	// Edit Felder setzen

	// Zeit und Datum setzen
	m_DateFrom.SetTypeToDate( true);
	m_DateFrom.SetBKColor(YELLOW);
	m_DateFrom.SetTextErrColor(RED);
	m_TimeFrom.SetTypeToTime( true);
	m_TimeFrom.SetBKColor(YELLOW);
	m_TimeFrom.SetTextErrColor(RED);

	if( pomDrdData->Drdf.GetStatus() != COleDateTime::valid)
	{
		m_DateFrom.SetInitText("");
		m_TimeFrom.SetInitText("");
	}
	else
	{
		m_DateFrom.SetInitText(pomDrdData->Drdf.Format("%d.%m.%Y"));
		m_TimeFrom.SetInitText(pomDrdData->Drdf.Format("%H:%M"));
	}
	//--------------------------------------------------------
	m_DateTo.SetTypeToDate( true);
	m_DateTo.SetBKColor(YELLOW);
	m_DateTo.SetTextErrColor(RED);
	m_TimeTo.SetTypeToTime( true);
	m_TimeTo.SetBKColor(YELLOW);
	m_TimeTo.SetTextErrColor(RED);
	if( pomDrdData->Drdt.GetStatus() != COleDateTime::valid)
	{
		m_DateTo.SetInitText("");
		m_TimeTo.SetInitText("");
	}
	else
	{
		m_DateTo.SetInitText(pomDrdData->Drdt.Format("%d.%m.%Y"));
		m_TimeTo.SetInitText(pomDrdData->Drdt.Format("%H:%M"));
	}

	FillComboBoxes();
	FillListBoxes();

	SetWindowText(LoadStg(IDC_S_Abweichung));
	SetStatic();
	SetButtonText();	

	return TRUE;  
}

//********************************************************************************
// Eingabe auf G�ltigkeit pr�fen
//********************************************************************************

bool CDrdDialog::IsInputValid(COleDateTime opTimeFrom,COleDateTime opTimeTo)
{
	bool blResult = true;
	CString olErrorString;
	// G�ltigkeit testen, und Fehlermeldung ausgeben
	if(	!m_DateTo.GetStatus()	|| 
		!m_TimeTo.GetStatus()	||
		!m_DateFrom.GetStatus()	|| 
		!m_TimeFrom.GetStatus())
	{
		olErrorString += LoadStg(IDS_STRING1855);
		blResult = false;
	}
	// Zeiteingabe ist g�ltig, weiter pr�fen
	else
	{
		// Pr�fen Ist von kleiner als bis ?
		if (opTimeFrom > opTimeTo)
		{
			olErrorString += LoadStg(IDS_STRING1856);
			blResult = false;
		}

		// Zeit in der unteren Grenze der Schicht ?
		if (opTimeFrom < pomDrrData->Avfr)
		{
			olErrorString += LoadStg(IDS_STRING1857);
			blResult = false;
		}

		// Zeit in der oberen Grenze der Schicht ?
		if (opTimeTo > pomDrrData->Avto)
		{
			olErrorString += LoadStg(IDS_STRING1858);
			blResult = false;
		}

		// Abwesenheiten d�rfen sich nicht �berschneiden
		if (!ogDrdData.IsValidDrd(opTimeFrom,opTimeTo,pomDrdData->Urno,pomDrdMap))
		{
			olErrorString += LoadStg(IDS_STRING1859);
			blResult = false;
		}

		
		// Nur max. 15 Qualifikationen passen in DRDTAB.PRMC (VARCHAR(90)) : (5 + 1) * 15 = 90
		// ==> deshalb nur zus�tzliche Qualifikationen abspeichern
		if (m_LB_PercAdditional.GetSelCount() > 15)
		{
			olErrorString += LoadStg(IDS_STRING1860);
			m_LB_Perc.SetFocus();
			blResult = false;
		}
		else
		{
			int *pnSelectedItems;
			int ilSelCount;
			ilSelCount = m_LB_PercAdditional.GetSelCount();
			pnSelectedItems = (int*)malloc((size_t)sizeof(int)*ilSelCount);
			if (m_LB_PercAdditional.GetSelItems(ilSelCount, pnSelectedItems) != LB_ERR)
			{
				CString olPrmc;
				CString olItem;
				void  *prlVoid = NULL;
				for (int i = 0; i < ilSelCount; i++)
				{
					m_LB_PercAdditional.GetText(pnSelectedItems[i], olItem);
					olItem = olItem.Left(5);
					olItem.TrimRight();
					olPrmc += "|" + olItem;
				}
				if (olPrmc.GetLength())
				{
					olPrmc = olPrmc.Mid(1);
					strcpy (pomDrdData->Prmc, olPrmc.GetBuffer(0));
				}
			}
			free (pnSelectedItems);
		}
	}
	
	// Fehlermeldung ausgeben
	if (!blResult)
	{
		m_DateFrom.SetFocus();
		m_DateFrom.SetSel(0,-1);
		MessageBox(olErrorString,"Rostering",MB_ICONWARNING);
	}

	return blResult;
}

//********************************************************************************
// OnOK()
//********************************************************************************

void CDrdDialog::OnOK() 
{
	// Zwischenspeicher f�r Werte
	DRDDATA olTempDrd;
	// ben�tigte Felder f�r Validierung kopieren
	olTempDrd.Stfu = pomDrdData->Stfu;
	strcpy(olTempDrd.Sday,pomDrdData->Sday);

	// Zeitwerte lesen
	GetTimeData(olTempDrd);

	// G�ltigkeit pr�fen
	if (!IsInputValid(olTempDrd.Drdf,olTempDrd.Drdt))
		return;

	// Funktion, Organisationseinheiten, Arbeitsgruppen und Qualifikationen lesen
	GetJobData(olTempDrd);

	// pr�fen, ob eine Abweichung vom Ausgangsdatensatz vorliegt
	CCSPtrArray<DELDATA> olDelList;
	ogDelData.GetDelListByBsdu(pomDrrData->Bsdu, false, &olDelList);

	if (!ogDrdData.CompareDrdToDrd(&olTempDrd,pomDrdData,false) || 
		!ogDrdData.CompareDrdToDelList(&olTempDrd, olDelList))
	{
		// Der gew�hlte Zeitraum �berschneidet sich mit einer anderen Abweichung.\n*INTXT*
		MessageBox(LoadStg(IDS_STRING1859),"Rostering",MB_ICONWARNING);
		return;
	}

	// pr�fen, ob eine Abweichung von den Stammdaten vorliegt
	if (!ogDrdData.CompareDrdToBasicData(&olTempDrd,bmCheckMainFuncOnly))
	{
		// nein -> Warnung
		MessageBox(LoadStg(IDS_STRING1864),"Rostering",MB_ICONWARNING);
	}

	// �nderungen �bernehmen
	ogDrdData.CopyDrd(&olTempDrd,pomDrdData);
		
	CDialog::OnOK();
}

//********************************************************************************
// GetJobData: liest die Werte f�r Funktion, Arbeitsgruppe, Organisationseinheit
//	und Qualifikationen aus den Controls und speichert sie in <opTargetDrd>.
// R�ckgabe:	keine
//********************************************************************************

void CDrdDialog::GetJobData(DRDDATA &opTargetDrd)
{
CCS_TRY;
	// Comboboxen auslesen----------------------------------------------------------
	// F�r die Bezeichnung werden aus der Map die Codes gesucht
	CString olStringValue,olStringResult;

	// Funktionen
	// String auslesen
	m_CB_Function.GetWindowText(olStringValue);
	if(omPfcMap.Lookup( olStringValue, olStringResult) )
		strcpy(opTargetDrd.Fctc, olStringResult.GetBuffer(0));
	// Organisationseinheiten
	// String auslesen
	m_CB_Org.GetWindowText(olStringValue);
	if (omOrgMap.Lookup( olStringValue, olStringResult) )
	{
		strcpy(opTargetDrd.Dpt1, olStringResult.GetBuffer(0));
	}

	// Listboxen auslesen----------------------------------------------------------
	CString olSelStr = "";	// single entry from Listbox
	CString olResStr = "";	// concatenated resultstring

	int ilItems = m_LB_PercAdditional.GetSelCount();
	if(ilItems != LB_ERR )
	{
		if (ilItems > 0)
		{
			int *pllItemlist = (int *) malloc( sizeof( int * ) * ilItems);
			m_LB_PercAdditional.GetSelItems( ilItems, pllItemlist );
			// Es d�rfen nur 15 Codes eingetragen werden
			for( int i=0; i<ilItems && i < 15; i++)
			{
				// Qualifikationsbezeichung lesen
				m_LB_PercAdditional.GetText( *(pllItemlist+i), olSelStr);
				// Code ermitteln und in DS speichern
				if(omPerMap.Lookup(olSelStr,olStringResult))
					olResStr += olStringResult + "|";
			}
			free( pllItemlist);
		}

		// String darf nur bestimmte L�nge haben !
		olResStr = olResStr.Left(88);

		// Qualifikationsstring in DS speichern
		strcpy(opTargetDrd.Prmc, olResStr.GetBuffer(0));
	}
CCS_CATCH_ALL;
}

//********************************************************************************
// OnCancel() 
//********************************************************************************

void CDrdDialog::OnCancel() 
{
	CDialog::OnCancel();
}

//********************************************************************************
// GetTimeData: liest die Datums- und Zeitwerte aus den Editcontrols und
//	speichert sie in <opTargetDrd>.
// R�ckgabe:	keine
//********************************************************************************

void CDrdDialog::GetTimeData(DRDDATA &opTargetDrd)
{
CCS_TRY;
	// Zeiten formatieren
	CString olDateFrom,olDateTo,olTimeFrom,olTimeTo,olFormatStringFrom,olFormatStringTo;
	COleDateTime olOleTimeTo,olOleTimeFrom;
	
	m_DateFrom.GetWindowText(olDateFrom);
	m_DateTo.GetWindowText(olDateTo);
	m_TimeFrom.GetWindowText(olTimeFrom);
	m_TimeTo.GetWindowText(olTimeTo);

	// Ver�ndert auf Format dd.mm.yyyy
	CheckDDMMYYValid(olDateFrom);
	CheckDDMMYYValid(olDateTo);

	// Alle Extra Zeichen (.:) l�schen.
	CedaDataHelper::DeleteExtraChars(olDateFrom);
	CedaDataHelper::DeleteExtraChars(olTimeFrom);
	CedaDataHelper::DeleteExtraChars(olDateTo);
	CedaDataHelper::DeleteExtraChars(olTimeTo);
	
	// Datum und Zeit "von" formatieren
	olFormatStringFrom = olDateFrom.Right(4) + olDateFrom.Mid(2,2) + olDateFrom.Left(2) + olTimeFrom.Left(2) + olTimeFrom.Right(2) +"00";

	// Datum und Zeit "bis" formatieren
	olFormatStringTo = olDateTo.Right(4) + olDateTo.Mid(2,2) + olDateTo.Left(2) + olTimeTo.Left(2) + olTimeTo.Right(2) +"00";

	// String in OleDateTime umwandeln, und bei Erfolg in die Struktur kopieren
	CedaDataHelper::DateTimeStringToOleDateTime(olFormatStringFrom,olOleTimeFrom);
	opTargetDrd.Drdf = olOleTimeFrom;
	
	CedaDataHelper::DateTimeStringToOleDateTime(olFormatStringTo,olOleTimeTo);
	opTargetDrd.Drdt = olOleTimeTo;
CCS_CATCH_ALL;
}

void CDrdDialog::SetStatic()
{
	SetDlgItemText(IDC_S_Abweichung,LoadStg(IDC_S_Abweichung));
	SetDlgItemText(IDC_S_von,LoadStg(IDC_S_von));
	SetDlgItemText(IDC_S_bis,LoadStg(IDC_S_bis));
	SetDlgItemText(IDC_S_Funktion,LoadStg(IDC_S_Funktion2));
	SetDlgItemText(IDC_S_OrgEinheit,LoadStg(IDC_S_OrgEinheit));
	SetDlgItemText(IDC_S_Arbeitsgruppe,LoadStg(IDC_S_Arbeitsgruppe));
	SetDlgItemText(IDC_S_Qualifikationen,LoadStg(IDC_S_Qualifikationen));
	SetDlgItemText(IDC_S_QualifikationenZusatz, LoadStg(IDS_STRING63497));
}

void CDrdDialog::SetButtonText()
{
	SetDlgItemText(IDOK,LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL,LoadStg(ID_CANCEL));
}

void CDrdDialog::FillListBoxes()
{
CCS_TRY;

	PERDATA* polPerData;
	m_LB_Perc.SetFont(&ogCourier_Regular_8);
	m_LB_PercAdditional.SetFont(&ogCourier_Regular_8);
	
	// -------------------------------------------------------------------------------
	// Listbox Qualifikationen f�llen
	// Qualifikationen die er aus den Stammdaten schon hat:

	int ilSpe;
	int i;
	ilSpe = ogSpeData.omData.GetSize();
	for (i = 0; i < ilSpe; i++)
	{
		SPEDATA *prlSpe = &ogSpeData.omData[i];
		if (prlSpe->Surn == pomDrdData->Stfu)
		{
			polPerData = ogPerData.GetPerByCode(prlSpe->Code);
			if (polPerData != NULL)
			{
				omSprPermits += "|" + CString(polPerData->Prmc);
				omSprPermitsMap.SetAt(CString(polPerData->Prmc),NULL);
				char pclTmp[50];
				sprintf (pclTmp, "%-5.5s %-40.40s", polPerData->Prmc, polPerData->Prmn);
				m_LB_Perc.AddString(pclTmp);
			}
		}
	}
	if (omSprPermits.GetLength())
	{
		omSprPermits = omSprPermits.Mid(1);	//removing first pipe
	}

	// -------------------------------------------------------------------------------
	// untere Listbox mit den zus�tzlich m�glichen Qualifikationen f�llen
	int ilPer = ogPerData.omData.GetSize();
	CString olItem;
	void  *prlVoid = NULL;
	for (i = 0; i < ilPer; i++)
	{
		PERDATA *prlPer = &ogPerData.omData[i];
		// Map f�llen
		char pclTmp[50];
		sprintf (pclTmp, "%-5.5s %-40.40s", prlPer->Prmc, prlPer->Prmn);
		omPerMap.SetAt(pclTmp,prlPer->Prmc);
		olItem = CString(prlPer->Prmc);

		// falls noch nicht in den Stammdaten, dann hinzuf�gen
		if (omSprPermitsMap.Lookup(olItem,(void *&)prlVoid) == FALSE)
		{
			m_LB_PercAdditional.AddString(pclTmp);
		}
	}

	// -------------------------------------------------------------------------------
	// highlight selected items in ListBox of the additional items
	CString olSubString = CString(pomDrdData->Prmc);
	if (olSubString.GetLength() > 0)
	{
		CString olItem;
		int ilCount = m_LB_PercAdditional.GetCount();

		for (i = 0; i < ilCount; i++) 
		{
			m_LB_PercAdditional.GetText(i, olItem);
			olItem = olItem.Left(5);
			olItem.TrimRight();
			olItem += "|";
			if (olSubString.Find(olItem) > -1)
			{
				m_LB_PercAdditional.SetSel(i);
			}
		}
	}

CCS_CATCH_ALL;
}

void CDrdDialog::FillComboBoxes()
{
CCS_TRY;
	int i;
	CString olFormat;
	m_CB_Function.SetFont(&ogCourier_Regular_8);
	int ilPfc = ogPfcData.omData.GetSize();
	for (i = 0; i < ilPfc; i++)
	{
		PFCDATA *prlPfc = &ogPfcData.omData[i];
		olFormat.Format("%-8.8s %-40.40s",prlPfc->Fctc,prlPfc->Fctn);
		m_CB_Function.AddString( olFormat);
		// Map f�llen
		omPfcMap.SetAt(olFormat,prlPfc->Fctc);
		// Eintrag in LB selektieren
		if (strcmp(pomDrdData->Fctc,prlPfc->Fctc) == 0)
		{
			m_CB_Function.SelectString(0,olFormat);
		}
	}

	// Organisationseinheiten
	m_CB_Org.SetFont(&ogCourier_Regular_8);
	int ilOrg = ogOrgData.omData.GetSize();
	for (i = 0; i < ilOrg; i++)
	{
		ORGDATA *prlOrg = &ogOrgData.omData[i];
		olFormat.Format("%-8.8s %-40.40s",prlOrg->Dpt1,prlOrg->Dptn);
		m_CB_Org.AddString(olFormat);
		// Map f�llen
		omOrgMap.SetAt(olFormat,prlOrg->Dpt1);

		// Eintrag in LB selektieren
		if (strcmp(pomDrdData->Dpt1,prlOrg->Dpt1) == 0)
		{
			m_CB_Org.SelectString(0,olFormat);
		}
	}
CCS_CATCH_ALL;
}
