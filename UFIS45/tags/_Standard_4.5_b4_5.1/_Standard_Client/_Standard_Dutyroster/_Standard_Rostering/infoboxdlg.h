#ifndef AFX_INFOBOXDLG_H__E9934EA1_89CB_11D2_8E38_0000C002916B__INCLUDED_
#define AFX_INFOBOXDLG_H__E9934EA1_89CB_11D2_8E38_0000C002916B__INCLUDED_

// InfoBoxDlg.h : Header-Datei
//
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld InfoBoxDlg 

class InfoBoxDlg : public CDialog
{
// Konstruktion
public:
	InfoBoxDlg::InfoBoxDlg(CString opHeaderText, CString opFieldText,
		       CWnd* pParent = NULL,
			   CRect opRect = CRect(0,0,::GetSystemMetrics(SM_CXSCREEN),::GetSystemMetrics(SM_CYSCREEN)));

	
		

	void InternalCancel() ;

// Dialogfelddaten
	//{{AFX_DATA(InfoBoxDlg)
	enum { IDD = IDD_INFOBOX_DLG };
	CStatic	m_Text;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(InfoBoxDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(InfoBoxDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CString omText;
	CString omHText;
	CRect omRect;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_INFOBOXDLG_H__E9934EA1_89CB_11D2_8E38_0000C002916B__INCLUDED_
