// cedadrrdata.cpp - Klasse f�r die Handhabung von SPR-Daten (Daily Roster Record)
//

#include <stdafx.h>
//************************************************************************************************************************************************
// ProcessSprCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_SPR_CHANGE, BC_SPR_NEW und BC_SPR_DELETE. Ruft zur eigentlichen Bearbeitung
//	der Nachrichten die Funktion CedaSprData::ProcessSprBc() der entsprechenden 
//	Instanz von CedaSprData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************
//STR_DESC omStrDesc;

void  ProcessSprCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaSprData *polSprData = (CedaSprData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polSprData->ProcessSprBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	RemoveWaitCursor();
}

//*********************************************************************************************
// ProcessSprBc: behandelt die Broadcasts BC_SPR_NEW, BC_SPR_CHANGE und BC_SPR_DELETE.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaSprData::ProcessSprBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY;

	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	SPRDATA *prlSpr = NULL;

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}
	
	switch(ipDDXType)
	{
	case BC_SPR_CHANGE:	// Datensatz �ndern
		{
			// Datenatz ermittlen
			prlSpr = GetSprFromSelectionStringUrno(CString(prlBcStruct->Selection));
			
			if(prlSpr != NULL)
			{
				// Datensatz vorhanden - aktualisieren
				GetRecordFromItemList(prlSpr,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlSpr);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_SPR_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlSpr = new SPRDATA;
			GetRecordFromItemList(prlSpr,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlSpr, SPR_SEND_DDX);
		}
		break;
	case BC_SPR_DELETE:	// Datensatz l�schen
		{
			// Datenatz ermittlen
			prlSpr = GetSprFromSelectionStringUrno(CString(prlBcStruct->Selection));
			
			if (prlSpr != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlSpr);
			}
		}
		break;
	default:
		break;
	}
	CCS_CATCH_ALL;
}

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaSprData::CedaSprData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r SPR-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(SPRDATA, SprDataRecInfo)
		FIELD_OLEDATE	(Acti,"ACTI")
		FIELD_CHAR_TRIM	(Fcol,"FCOL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Ustf,"USTF")
		FIELD_OLEDATE	(Lstu,"LSTU")
	END_CEDARECINFO;

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(SprDataRecInfo)/sizeof(SprDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SprDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
	
	// Tabellenname per Default auf SPR setzen
	SetTableNameAndExt(opTableName+opExtName);
	
    // Feldnamen initialisieren
	strcpy(pcmListOfFields,	"ACTI,FCOL,URNO,USTF,LSTU");
	
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
	
	// min. / max. Datum f�r SPRs initialisieren
	omMinDay.SetStatus(COleDateTime::invalid);
	omMaxDay.SetStatus(COleDateTime::invalid);
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaSprData::~CedaSprData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();

	
	// deleteing omUstfMap
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omUstfMap.GetStartPosition(); rlPos != NULL; )
	{
		omUstfMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUstfMap.RemoveAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaSprData::SetTableNameAndExt(CString opTableAndExtName)
{
	CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// CheckAndSetLoadPeriod: setzt den Zeitraum f�r zu ladende Daten, pr�ft vorher, ob
//	die Daten OK sind und das Enddatum nicht vor dem Startdatum liegt.
// R�ckgabe:	false	->	einer der �bergebenen Zeitwerte ist ung�ltig
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaSprData::CheckAndSetLoadPeriod(COleDateTime *popLoadFrom, COleDateTime *popLoadTo)
{
	CCS_TRY;
	TRACE("CedaSprData::CheckAndSetLoadPeriod(): popLoadFrom=%s * popLoadTo=%s\n",popLoadFrom->Format("%d.%m.%Y"),popLoadTo->Format("%d.%m.%Y"));
	if((popLoadFrom == NULL) || (popLoadFrom->GetStatus() == COleDateTime::invalid) || 
		(popLoadTo == NULL) || (popLoadTo->GetStatus() == COleDateTime::invalid) || 
		(*popLoadFrom > *popLoadTo)) 
		return false;	// einer der Werte ist NULL oder ung�ltig
	
	// Start- und Endtag f�r zu ladende Daten anpassen wenn n�tig
	if ((omMinDay.GetStatus() == COleDateTime::invalid) || 
		((popLoadFrom != NULL) && (*popLoadFrom < omMinDay))) 
		omMinDay = *popLoadFrom;
	if ((omMaxDay.GetStatus() == COleDateTime::invalid) || 
		((popLoadTo != NULL) && (*popLoadTo > omMaxDay))) 
		omMaxDay = *popLoadTo; 
	TRACE("\t neues Ladedatum: omMinDay=%s *** omMaxDay=%s\n",omMinDay.Format("%d.%m.%Y"),omMaxDay.Format("%d.%m.%Y"));
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaSprData::Register(void)
{
	CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren
	
	// SPR-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_SPR_CHANGE,        "CedaSprData", "BC_SPR_CHANGE",     ProcessSprCf);
	DdxRegister((void *)this,BC_SPR_NEW,           "CedaSprData", "BC_SPR_NEW",		   ProcessSprCf);
	DdxRegister((void *)this,BC_SPR_DELETE,        "CedaSprData", "BC_SPR_DELETE",     ProcessSprCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
	CCS_CATCH_ALL;
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaSprData::ClearAll(bool bpUnregister)
{
	CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
    //omKeyMap.RemoveAll();
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omUstfMap.GetStartPosition(); rlPos != NULL; )
	{
		omUstfMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUstfMap.RemoveAll();
	omData.DeleteAll();

	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// ReadSprByUrno: liest den SPR-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaSprData::ReadSprByUrno(long lpUrno, SPRDATA *prpSpr)
{
	CCS_TRY;
	CString olWhere;
	olWhere.Format("%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}
	
	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		ClearFastSocketBuffer();	
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	SPRDATA *prlSpr = new SPRDATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpSpr) == true)
	{  
		ClearFastSocketBuffer();	
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlSpr;
		ClearFastSocketBuffer();	
		return false;
	}
	
	return false;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// ReadFilteredByDateAndStaff: liest Datens�tze ein, deren Feld Acti
//	innerhalb des Zeitraums <opStart> bis <opEnd>
//	liegt. Die Parameter werden auf G�ltigkeit gepr�ft. Wenn <popLoadStfUrnoMap> nicht
//	NULL ist, werden nur die Datens�tze geladen, deren Feld STFU (Mitarbeiter-Urno)
//	im Array enthalten ist.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaSprData::ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/, bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
	CCS_TRY;
	// G�ltigkeit der Datumsangaben pr�fen und eventuelle Ladedatum anpassen
	if (!CheckAndSetLoadPeriod(&opStart, &opEnd))
	{
		// Fehler -> terminieren
		return false;
	}
	
	// Puffer f�r WHERE-Statement
	CString olWhere;
	
	// Anzahl der Urnos
	int ilCountStfUrnos = -1;
	if ((popLoadStfUrnoMap != NULL) && ((ilCountStfUrnos = popLoadStfUrnoMap->GetCount()) == 0))
	{
		// es gibt nichts zu tun, kein Fehler
		return true;	
	}
	
	// keine Urno-Liste oder Urno-Liste �bergeben aber Anzahl gr��er als max. in WHERE-Clause erlaubte MA-Urnos?
	if ((popLoadStfUrnoMap == NULL) || (ilCountStfUrnos > MAX_WHERE_STAFFURNO_IN))
	{
		// mehr Urnos als in WHERE-Clause passen -> alles laden und lokal filtern
		olWhere.Format("WHERE ACTI BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),popLoadStfUrnoMap,bpRegisterBC,bpClearData);
	}
	else
	{
		// MA-Urnos mit in WHERE-Clause aufnehmen
		POSITION pos;			// zum Iterieren
		void *plDummy;			// das Objekt, immer NULL und daher obsolet
		long llStfUrno;			// die Urno als long
		CString	olBuf;			// um die Urno in einen String zu konvertieren
		CString olStfUrnoList;	// die Urno-Liste
		// alle Urnos ermitteln
		for(pos = popLoadStfUrnoMap->GetStartPosition(); pos != NULL;)   
		{
			// n�chste Urno
			popLoadStfUrnoMap->GetNextAssoc(pos,(void*&)llStfUrno,plDummy);
			// umwandeln in String
			olBuf.Format("%ld",llStfUrno);
			// ab in die Liste
			olStfUrnoList += ",'" + olBuf + "'";
		}
		// f�hrendes Komma aus Urno-Liste entfernen, WHERE-Statement generieren
		olWhere.Format("WHERE USTF IN (%s) AND ACTI BETWEEN '%s' AND '%s'",olStfUrnoList.Mid(1),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),NULL,bpRegisterBC,bpClearData);
	}
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaSprData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/, bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
	CCS_TRY;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CedaData::CedaAction2("RT", "")) return false;
	}
	else
	{
		if (!CedaData::CedaAction2("RT", pspWhere)) return false;
	}
	
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		SPRDATA *prlSpr = new SPRDATA;
		// Datensatz lesen
 		blMoreRecords = GetBufferRecord2(ilCountRecord,prlSpr);
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		if(blMoreRecords && 
			((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlSpr->Ustf, (void *&)prlVoid) == TRUE)))
		{ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlSpr, SPR_NO_SEND_DDX))
			{
				// Fehler -> lokalen Datensatz l�schen
				delete prlSpr;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlSpr);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			// kein weiterer Datensatz
			delete prlSpr;
		}
	} while (blMoreRecords);

	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen SPR
	TRACE("Read-Spr: %d gelesen\n",ilCountRecord);

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}
	
	ClearFastSocketBuffer();	
	
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpSpr> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaSprData::Insert(SPRDATA *prpSpr, bool bpSave /*= true*/)
{
	CCS_TRY;
	
	// �nderungs-Flag setzen
	prpSpr->IsChanged = DATA_NEW;
	
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpSpr) == false) return false;
	
	pomLastInternChangedSpr = prpSpr;
	
	// Broadcast SPR_NEW abschicken
	InsertInternal(prpSpr,true);
	
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
// muss sp�ter oder vorher explizit durch Aufruf von Save() in der Datenbank 
// gespeichert werden. Wenn <bpSendDdx> true ist, wird der Broadcast-Handler
// aktiviert.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaSprData::InsertInternal(SPRDATA *prpSpr, bool bpSendDdx)
{
	CCS_TRY;
	if (!IsValidSpr(prpSpr))
	{
		return false;
	}

	// converting the UTC-times to local times
	ConvertDatesToLocal(prpSpr);

	// Datensatz intern anf�gen
	omData.Add(prpSpr);

	// Datensatz der Key-Map hinzuf�gen
	CMapPtrToPtr *polSingleMap;
	if(omUstfMap.Lookup((void *) prpSpr->Ustf, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *) prpSpr->Urno, prpSpr);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prpSpr->Urno, prpSpr);
		omUstfMap.SetAt((void *)prpSpr->Ustf,polSingleMap);
	}

	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpSpr->Urno,prpSpr);

	// Broadcast senden?
	if (bpSendDdx == true)
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,SPR_NEW,(void *)prpSpr);
	}

	return true;
	CCS_CATCH_ALL;
	return false;
}

/*********************************************************************************************
IsValidSpr: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten SPRs
Datenfelder pr�fen
*********************************************************************************************/

bool CedaSprData::IsValidSpr(SPRDATA *popSpr)
{
	CCS_TRY;
	if(!popSpr)
	{
		return TraceSpr(popSpr, (LPCTSTR)"SPR defect: ");
	}
	
	if(	ogStfData.GetStfByUrno(popSpr->Ustf) == 0)
	{
		ogRosteringLogText += "Ustf in STFTAB not found\n";
		return TraceSpr(popSpr, (LPCTSTR)"SPR defect: ");
	}
	
	if(	popSpr->Acti.GetStatus() != COleDateTime::valid )
	{
		ogRosteringLogText += "Acti defect\n";
		return TraceSpr(popSpr, (LPCTSTR)"SPR defect: ");
	}
	
	// nur diese Zeichen sind korrekt:
	switch(popSpr->Fcol[0])
	{
	default:
		ogRosteringLogText += "popSpr->Fco[0] defect\n";
		return TraceSpr(popSpr, (LPCTSTR)"SPR defect: ");
	case '0':
	case '1':
	case '2':
	case '3':
		break;
	}
	
	// SPR ist g�ltig!
	return true;
	
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpSpr->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaSprData::Update(SPRDATA *prpSpr)
{
	CCS_TRY;
	if (GetSprByUrno(prpSpr->Urno) != NULL)
	{
		
		// �nderungsflag setzen
		if (prpSpr->IsChanged == DATA_UNCHANGED)
		{
			prpSpr->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(Save(prpSpr) == false) return false; 
		
		pomLastInternChangedSpr = prpSpr;
		
		// Broadcast SPR_CHANGE versenden
		UpdateInternal(prpSpr);
	}
	
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaSprData::UpdateInternal(SPRDATA *prpSpr, bool bpSendDdx /*true*/)
{
	CCS_TRY;
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
		// ein Broadcast kann u.u. zum L�schen des MAs f�hren, dann knallts, wenn man
		// keine lokale Kopie des Datensatzes angelegt hat
		SPRDATA olSprData;
		CopySpr(&olSprData,prpSpr);
		
		if(pomLastInternChangedSpr == prpSpr)
		{
			pomLastInternChangedSpr = &olSprData;
		}
		
		ogDdx.DataChanged((void *)this,SPR_CHANGE,(void *)&olSprData);
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaSprData::DeleteInternal(SPRDATA *prpSpr, bool bpSendDdx /*true*/)
{
	CCS_TRY;
	// Broadcast senden BEVOR der Datensatz gel�scht wird
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,SPR_DELETE,(void *)prpSpr);
	}
	
	
	// Tages-Schl�ssel YYYYMMDD
	CString olDay = prpSpr->Acti.Format("%Y%m%d");
	// Puffer f�r Schl�ssel
	CString olPrimaryKey;
	olPrimaryKey.Format("%s-%ld-%s",olDay,prpSpr->Ustf,prpSpr->Fcol);

	// Prim�rschl�ssel l�schen
	//omKeyMap.RemoveKey(olPrimaryKey);
	CMapPtrToPtr *polSingleMap;
	if(omUstfMap.Lookup((void *) prpSpr->Ustf, (void *&) polSingleMap))
	{
		polSingleMap->RemoveKey((void *)prpSpr->Urno);
		if(polSingleMap->GetCount() <= 0)
		{
			omUstfMap.RemoveKey((void *)prpSpr->Ustf);
			delete polSingleMap;
		}
	}
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpSpr->Urno);
	
	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpSpr->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
	CCS_CATCH_ALL;
}

//*********************************************************************************************
// RemoveInternalByStaffUrno: entfernt alle SPRs aus der internen Datenhaltung,
//	die dem Mitarbeiter <lpStfUrno> zugeordnet sind. Die Datens�tze werden NICHT 
//	aus der Datenbank gel�scht.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaSprData::RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC /*= false*/)
{
	CCS_TRY;
	SPRDATA *prlSpr;
	// alle SPRs durchgehen
	for(int i=0; i<omData.GetSize(); i++)
	{
		// SPR dieses MAs entfernen?
		if(lpStfUrno == omData[i].Ustf)
		{
			// ja
			if ((prlSpr =  GetSprByUrno(omData[i].Urno)) == NULL) return false;
			// SPR entfernen
			DeleteInternal(prlSpr, bpSendBC);
			// neue Datensatz-Anzahl anpassen
			i--;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpSpr> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaSprData::Delete(SPRDATA *prpSpr, BOOL bpWithSave)
{
	CCS_TRY;
	// Flag setzen
	prpSpr->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpSpr)) return false;
	}
	
	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpSpr,true);
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

/********************************************************************************
Erstellt aus dem Selection-String den Urno-String und findet SPR
********************************************************************************/
SPRDATA* CedaSprData::GetSprFromSelectionStringUrno(CString opSelection)
{
	long llUrno;
	if (opSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(opSelection);
	}
	else
	{
		int ilLast;
		int ilFirst = opSelection.Find("=")+2;

		// der String kann in einer der zwei Formen angegeben werden:
		// "123456789 123456789" oder "WHERE URNO = 123456789"
		if(ilFirst < 2)
		{
			// kein gefunden, der String ist in der Form: "123456789 123456789"
			ilFirst = 0;
			for(ilLast=ilFirst; ilLast<opSelection.GetLength(); ilLast++)
			{
				//char ilTest = opSelection.GetAt(ilLast);
				if(!isdigit((int)opSelection.GetAt(ilLast)))
				{
					break;
				}
			}
		}
		else
		{
			// gefunden, die Form: "WHERE URNO = 123456789"
			ilLast  = opSelection.GetLength();
		}
		llUrno = atol(opSelection.Mid(ilFirst,ilLast-ilFirst));
	}
	return GetSprByUrno(llUrno);
}

//*********************************************************************************************
// GetSprByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

SPRDATA *CedaSprData::GetSprByUrno(long lpUrno)
{
	// der Datensatz
	SPRDATA *prpSpr;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpSpr) == TRUE)
	{
		return prpSpr;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
}

//*********************************************************************************************
// GetSprByKey: sucht den Datensatz mit dem Prim�rschl�ssel aus 
//	Schichttag (<opSday>), Schichtnummer (<lpSprn>) ,Mitarbeiter-
//	Urno (<lpUstf>) und Planungsstufe (<opRosl>).
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

SPRDATA *CedaSprData::GetSprByKey(long lpUstf, const char *pcpFcol, COleDateTime opShiftStart, COleDateTime opShiftEnd)
{
	if(opShiftStart.GetStatus() == COleDateTime::valid && opShiftEnd.GetStatus() == COleDateTime::valid)
	{
		COleDateTime olStart = opShiftStart - COleDateTimeSpan(0,2,0,0); // - ogBasicData.omShiftStartTolerance;
		COleDateTime olEnd = opShiftEnd + COleDateTimeSpan(0,2,0,0);// + ogBasicData.omShiftEndTolerance;

		CMapPtrToPtr *polSingleMap;
		SPRDATA *prlSpr = NULL;
		long llUrno;

		if (omUstfMap.Lookup((void *)lpUstf, (void *& )polSingleMap))
		{
			SPRDATA*			prlBestMatch=NULL;
			COleDateTimeSpan	olBestMatchDiff;

			for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos, (void *&) llUrno, (void *& ) prlSpr);
				if (prlSpr != NULL && !strcmp(prlSpr->Fcol,pcpFcol) && prlSpr->Acti >= olStart && prlSpr->Acti <= olEnd)
				{
					COleDateTimeSpan olDiff1 = opShiftStart - prlSpr->Acti;
					COleDateTimeSpan olDiff2 = opShiftEnd - prlSpr->Acti;
					if (prlBestMatch == NULL)
					{
						if (strcmp(pcpFcol,"1") == 0)
						{
							olBestMatchDiff = olDiff1;
							prlBestMatch = prlSpr;
						}
						else if (strcmp(pcpFcol,"0") == 0)
						{
							olBestMatchDiff = olDiff2;
							prlBestMatch = prlSpr;
						}
						else
						{
							prlBestMatch = prlSpr;
						}

					}
					else if (strcmp(pcpFcol,"1") == 0)
					{
						if (fabs(olDiff1.GetTotalMinutes()) < fabs(olBestMatchDiff.GetTotalMinutes()))
						{
							prlBestMatch = prlSpr;
							olBestMatchDiff = olDiff1;
						}
					}
					else if (strcmp(pcpFcol,"0") == 0)
					{
						if (fabs(olDiff2.GetTotalMinutes()) < fabs(olBestMatchDiff.GetTotalMinutes()))
						{
							prlBestMatch = prlSpr;
							olBestMatchDiff = olDiff2;
						}
					}
					else if (prlSpr->Lstu > prlBestMatch->Lstu)
					{
						prlBestMatch = prlSpr;
					}
				}
			}
			
			return prlBestMatch;
		}
	}

	return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpSpr>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaSprData::Save(SPRDATA *prpSpr)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048]; 

	if (prpSpr->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpSpr->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		if(prpSpr->Acti.GetStatus() != 0)
		{
			AfxMessageBox("DATA_NEW, prpSpr->Acti invalid");
		}
		MakeCedaData(&omRecInfo,olListOfData,prpSpr);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpSpr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpSpr->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSpr);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpSpr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpSpr->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSpr);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("DRT",pclSelection,"",pclData);
		break;
	}
	
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaSprData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList, char *pcpSelection, char *pcpSort, char *pcpData, char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, int ipFieldCount /*= 0*/)
{
	CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpTableName,pcpFieldList,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,pomData,ipFieldCount);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaSprData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort, char *pcpData, char *pcpDest /*"BUF1"*/, bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
	CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

/**********************************************************************
SPRDATA kopieren
**********************************************************************/
void CedaSprData::CopySpr(SPRDATA* popDst, SPRDATA* popSrc)
{
	if(!popDst || !popSrc) return;
	// da wir z.Zt. keine Pointer in der SPRDATA haben, kann memcpy benutzt werden
	memcpy((void*)popDst, (void*)popSrc, sizeof(SPRDATA));
}

/********************************************************************************
Debug Trace Funktion
********************************************************************************/
bool CedaSprData::TraceSpr(SPRDATA *popSpr, LPCTSTR popStr)
{
	CString olMsg = popStr;
	GetDataFormatted(olMsg, popSpr);

	ogRosteringLogText += olMsg;
	WriteInRosteringLog(LOGFILE_TRACE);

	ogErrlog += olMsg;
	WriteInErrlogFile();
	
	return false;
}

/****************************************************************************************
bda: makes a formatted string with all fields of a data structure for trace and debug output
****************************************************************************************/
bool CedaSprData::GetDataFormatted(CString &pcpListOfData, SPRDATA *prpSpr)
{
	// Name
	CString olName = "";
	STFDATA *prlStf;
	prlStf = ogStfData.GetStfByUrno(prpSpr->Ustf);
	
	if (prlStf > NULL)
	{
		olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
	}
	
	pcpListOfData += olName;
	pcpListOfData += " \t";
	CedaData::GetDataFormatted(pcpListOfData, (void *)prpSpr);
	return true;
}

/*******************************************************
Zeiten holen
*******************************************************/
void CedaSprData::GetShiftTimes(COleDateTime& opFrom, COleDateTime& opTo, COleDateTime& opSbfr, COleDateTime& opSbto, COleDateTime opAVFR, COleDateTime opAVTO, long lpUstf)
{
	SPRDATA* polSpr = NULL;

	opFrom.SetStatus(COleDateTime::invalid);
	opTo.SetStatus(COleDateTime::invalid);
	opSbfr.SetStatus(COleDateTime::invalid);
	opSbto.SetStatus(COleDateTime::invalid);

	if (opAVFR.GetStatus() != COleDateTime::valid) return;
	if (opAVTO.GetStatus() != COleDateTime::valid) return;

	// From
	polSpr = GetSprByKey(lpUstf, "1", opAVFR, opAVTO);
	if (polSpr)
	{
		opFrom = polSpr->Acti;
	}

	// To
	polSpr = GetSprByKey(lpUstf, "0", opAVFR, opAVTO);
	if (polSpr)
	{
		opTo = polSpr->Acti;
	}

	// Sbfr
	polSpr = GetSprByKey(lpUstf, "2", opAVFR, opAVTO);
	if (polSpr)
	{
		opSbfr = polSpr->Acti;
	}

	polSpr = GetSprByKey(lpUstf, "3", opAVFR, opAVTO);
	if (polSpr)
	{
		opSbto = polSpr->Acti;
	}
}

/*************************************************************************
Avfr & "1"-Time
Avto & "0"-Time
Sbfr & "2"-Time
Sbto & "3"-Time
true wenn eine der Zeiten sich um mindestens 5 Minuten unterscheidet
*************************************************************************/
bool CedaSprData::AreTimesDifferent(DRRDATA* popDrr)
{
	// min Differenzzeit in minuten 
	static int ilDiffTime = -1;
	if (ilDiffTime == -1)
	{
		ilDiffTime = atoi(ogCCSParam.GetParamValue(ogAppl,"ID_WOR_AZE_DIFF"));
		if (ilDiffTime < 0)
			ilDiffTime *= -1;

	}

	if (!popDrr) 
	{
		return false;
	}

	COleDateTime olFrom, olTo, olSbfr, olSbto;

	GetShiftTimes(olFrom, olTo, olSbfr, olSbto, popDrr->Avfr, popDrr->Avto, popDrr->Stfu);

	COleDateTimeSpan olDiff(0,0,ilDiffTime,0);

	COleDateTimeSpan olCurrDiff;
	if (popDrr->Avfr.GetStatus() == COleDateTime::valid && olFrom.GetStatus() == COleDateTime::valid)
	{
		olCurrDiff = popDrr->Avfr - olFrom;
		if (olCurrDiff.m_span < 0)
		{
			olCurrDiff.m_span *= -1;
		}
		if (olCurrDiff > olDiff)
		{
			return true;
		}
	}

	if (popDrr->Avto.GetStatus() == COleDateTime::valid && olTo.GetStatus() == COleDateTime::valid)
	{
		olCurrDiff = popDrr->Avto - olTo;
		if (olCurrDiff.m_span < 0)
		{
			olCurrDiff.m_span *= -1;
		}
		if (olCurrDiff > olDiff)
		{
			return true;
		}
	}

	if (popDrr->Sbfr.GetStatus() == COleDateTime::valid && olSbfr.GetStatus() == COleDateTime::valid)
	{
		olCurrDiff = popDrr->Sbfr - olSbfr;
		if (olCurrDiff.m_span < 0)
		{
			olCurrDiff.m_span *= -1;
		}
		if (olCurrDiff > olDiff)
		{
			return true;
		}
	}

	if (popDrr->Sbto.GetStatus() == COleDateTime::valid && olSbto.GetStatus() == COleDateTime::valid)
	{
		olCurrDiff = popDrr->Sbto - olSbto;
		if (olCurrDiff.m_span < 0)
		{
			olCurrDiff.m_span *= -1;
		}
		if (olCurrDiff > olDiff)
		{
			return true;
		}
	}

	return false;
}

void CedaSprData::ConvertDatesToLocal(SPRDATA *prpSpr)
{
	if (prpSpr != NULL)
	{
		ogBasicData.ConvertDateToLocal(prpSpr->Acti);
	}
}