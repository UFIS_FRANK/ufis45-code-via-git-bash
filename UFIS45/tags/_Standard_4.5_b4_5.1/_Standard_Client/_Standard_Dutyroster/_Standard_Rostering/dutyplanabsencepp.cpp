// DutyPlanAbsencePp.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <DutyAbsenceOverviewDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite DutyPlanAbsencePp 
//---------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(DutyPlanAbsencePp, CPropertyPage)

DutyPlanAbsencePp::DutyPlanAbsencePp() : CPropertyPage(DutyPlanAbsencePp::IDD)
{
	CCS_TRY;

	//{{AFX_DATA_INIT(DutyPlanAbsencePp)
	m_EfromDate		= _T("");
	m_Reas			= _T("");
	m_Arc1H			= _T("");
	m_EtoDate		= _T("");
	//}}AFX_DATA_INIT

	pomParent = NULL;

	m_psp.pszTitle = LoadStg(IDS_STRING1680);
	m_psp.dwFlags |= PSP_USETITLE;

	CCS_CATCH_ALL;
}

DutyPlanAbsencePp::~DutyPlanAbsencePp()
{
	CCS_TRY;

	CCS_CATCH_ALL;
}

void DutyPlanAbsencePp::DoDataExchange(CDataExchange* pDX)
{
CCS_TRY;
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyPlanAbsencePp)
	DDX_Control(pDX, IDC_RADIO_REGULAR_FREE,	omRBRegFree);
	DDX_Control(pDX, IDC_RADIO_WEEKEND_FREE,	omRBWeekendFree);
	DDX_Control(pDX, IDC_DELUPTYPE,				omDelUPType);
	DDX_Control(pDX, IDC_DELACTIVETYPE,			omDelActivePType);
	DDX_Control(pDX, IDC_CHECKACTIVEPTYPE,		omCheckActivePType);
	DDX_Control(pDX, IDC_CHECKUPTYPE,			omCheckUPType);
	DDX_Control(pDX, IDC_B_RMSVIEWH,			m_B_RmsViewH);
	DDX_Control(pDX, IDC_B_INSERT,				m_B_Insert);
	DDX_Control(pDX, IDC_E_FROM,				m_From);
	DDX_Control(pDX, IDC_E_TO,					m_To);
	DDX_Control(pDX, IDC_E_RMSCODEH,			m_RMSCodeH);
	DDX_Text(pDX,	 IDC_E_FROM,				m_EfromDate);
	DDX_Text(pDX,	 IDC_E_TO,					m_EtoDate);
	DDX_Text(pDX,	 IDC_E_RMSCODEH,			m_Arc1H);
	//}}AFX_DATA_MAP
CCS_CATCH_ALL;
}

BEGIN_MESSAGE_MAP(DutyPlanAbsencePp, CPropertyPage)
	//{{AFX_MSG_MAP(DutyPlanAbsencePp)
	ON_BN_CLICKED(IDC_B_RMSVIEWH,		OnBRmsviewH)
	ON_BN_CLICKED(IDC_B_INSERT,			OnBStart)
	ON_BN_CLICKED(IDC_DELUPTYPE,		OnDelUpType)
	ON_BN_CLICKED(IDC_DELACTIVETYPE,	OnDelActivePType)
	ON_BN_CLICKED(IDC_CHECKACTIVEPTYPE, OnCheckActivePType)
	ON_BN_CLICKED(IDC_CHECKUPTYPE,		OnCheckUpType)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyPlanAbsencePp 
//---------------------------------------------------------------------------

BOOL DutyPlanAbsencePp::OnInitDialog() 
{
CCS_TRY;
	CPropertyPage::OnInitDialog();

	omRBWeekendFree.SetCheck(false);
	omRBRegFree.SetCheck(true);

	m_From.SetTypeToDate();
	m_From.SetTextErrColor(RED);
	m_From.SetBKColor(LTYELLOW);
	m_From.SetInitText(m_EfromDate);

	m_To.SetTypeToDate();
	m_To.SetTextErrColor(RED);
	m_To.SetBKColor(LTYELLOW);
	m_To.SetInitText(m_EtoDate);

	m_RMSCodeH.SetFormat("x|#x|#x|#x|#x|#");
	m_RMSCodeH.SetTextLimit(0,5);
	m_RMSCodeH.SetTextErrColor(RED);
	m_RMSCodeH.SetBKColor(LTYELLOW);
	m_RMSCodeH.SetInitText(m_Arc1H);

	OnRFausgleich();

	SetLabels();

	omCheckActivePType.SetCheck(1);
	omCheckUPType.SetCheck(0);

	return TRUE;
CCS_CATCH_ALL;
return FALSE;
}

//---------------------------------------------------------------------------

void DutyPlanAbsencePp::OnCancel()
{
CCS_TRY;
	;//do nothing more
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyPlanAbsencePp::OnOK() 
{
CCS_TRY;
	;//do nothing more
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

void DutyPlanAbsencePp::InitValues(long lpStfUrno, CWnd* pParentWnd)
{
CCS_TRY;
	lmStfUrno		= lpStfUrno;
	pomParent		= (DutyRoster_View*)pParentWnd;
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------


//*******************************************************************************************************************
// EnableOtherAbsencesControls: aktiviert oder deaktiviert die Oberfl�chenelemente
//	zur Eingabe sonstiger Abwesenheiten.
// R�ckgabe:	keine
//*******************************************************************************************************************

void DutyPlanAbsencePp::EnableOtherAbsencesControls(bool bpActivate) 
{
CCS_TRY;
	// Radiobutton aktivieren
CCS_CATCH_ALL;
}

//*******************************************************************************************************************
// OnRFausgleich: Eventhandler RButton 'Abwesenheiten/Urlaub/Ferienausgleich' geklickt.
// R�ckgabe:	keine
//*******************************************************************************************************************

void DutyPlanAbsencePp::OnRFausgleich() 
{
CCS_TRY;
	EnableAbsenceControls(true);
	EnableOtherAbsencesControls(false);
CCS_CATCH_ALL;
}

//*******************************************************************************************************************
// EnableAbsenceControls: aktiviert oder deaktiviert die Oberfl�chenelemente
//	zur Eingabe von Abwesenheiten/Urlaub/Ferienausgleich.
// R�ckgabe:	keine
//*******************************************************************************************************************

void DutyPlanAbsencePp::EnableAbsenceControls(bool bpActivate)
{
CCS_TRY;
	// Radiobutton aktivieren
	m_RMSCodeH.EnableWindow(bpActivate);
	m_B_RmsViewH.EnableWindow(bpActivate);
	m_From.EnableWindow(bpActivate);
	m_To.EnableWindow(bpActivate);
	m_B_Insert.EnableWindow(bpActivate);

	// Radiobuttons 'reg. artbeitsfrei...' aktivieren / deaktivieren
	omRBRegFree.EnableWindow(bpActivate);
	omRBWeekendFree.EnableWindow(bpActivate);

	// Checkboxen 'Einf�gen in Planungsstufe...' aktivieren / deaktivieren
	omCheckActivePType.EnableWindow(bpActivate);
	omCheckUPType.EnableWindow(bpActivate);
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------

//*******************************************************************************************************************
// IsRegularFreeOrHoliday(): pr�fen, ob der Tag <opDay> dem MA als Arbeitstag
//	berechnet werden muss.
// R�ckgabe:	true	->	an diesem Tag ist ein Feiertag oder der
//							MA h�tte regul�r arbeitsfrei
//				false	->	der Tag wird als Arbeitstag gerechnet
//*******************************************************************************************************************

bool DutyPlanAbsencePp::IsRegularFreeOrHoliday(COleDateTime opDay, long lpStfUrno, ODADATA *popOda)
{
CCS_TRY;
	// Wochenenden �berschreiben?
	bool blOverwriteWeekend = omRBWeekendFree.GetCheck() != 0;

	// regul�r arbeitsfreie Tage �berschreiben
	bool blOverwriteRegFree = omRBRegFree.GetCheck() != 0;

	// aktueller Tag = gesetzlicher Feiertag?
	CString olTmpHolDay = opDay.Format("%Y%m%d000000");
	RecordSet *polRecord = new RecordSet(ogBCD.GetFieldCount("HOL"));
	if(ogBCD.GetRecord("HOL", "HDAY", olTmpHolDay, *polRecord) == true)
	{
		// Feiertag -> Arbeitstag wird nicht angerechnet
		delete polRecord;
		return true;
	}
	delete polRecord;

	// Sonntag oder Sonnabend und Wochenenden �berschreiben?
	if(pomParent == 0 || pomParent->pomIsTabViewer == 0)
		return false;
	
	if(blOverwriteWeekend && CedaDataHelper::IsWeekendOrHoliday(opDay, pomParent->pomIsTabViewer->omHolidayKeyMap, lpStfUrno))
	{
		// Tag w�rde auf regul�r frei gesetzt -> nicht anrechnen
		return true;
	}

	// DRR an diesem Tag ermitteln
	DRRDATA *prlDrr = ogDrrData.GetDrrByRoss(opDay.Format("%Y%m%d"),lpStfUrno);
	if (prlDrr == NULL) 
	{
		// keinen DRR gefunden -> Tag anrechnen
		return false;
	}
	// DRR gefunden -> pr�fen, ob reg. arbeitsfrei
	bool blCodeIsFree = (ogOdaData.IsODAAndIsRegularFree(CString(prlDrr->Scod)) == CODE_IS_ODA_REGULARFREE);
	
	// DRR hat reg. arbeitsfrei und reg. arbeitsfrei NICHT �berschreiben?
	if (blCodeIsFree && blOverwriteRegFree)
	{
		// Tag ist regul�r frei -> nicht anrechnen
		return true;
	}
	// Tag anrechnen
	return false;
CCS_CATCH_ALL;
return false;
}
		
//*******************************************************************************************************************
// IsRMSCodeOk(): pr�ft, ob es einen Abwesenheits-Datensatz mit dem Code <olCode>
//	gibt.
// R�ckgabe:	true	->	Datensatz gefunden
//				false	->	kein Datensatz mit Code <olCode> gefunden
//*******************************************************************************************************************

bool DutyPlanAbsencePp::IsRMSCodeOk(CString olCode)
{
CCS_TRY;
	if(olCode.IsEmpty())
		return true;

	ODADATA *prlOda = ogOdaData.GetOdaBySdac(olCode);
	if(prlOda != NULL)
		return true;

	return false;
CCS_CATCH_ALL;
return false;
}

//*******************************************************************************************************************
// OnBRmsviewH(): Event-Handler f�r den Button zur Abwesenheitsauswahl
//	(Abwesenheit / Urlaub / Ferienausgleich).
// R�ckgabe:	keine
//*******************************************************************************************************************
void DutyPlanAbsencePp::OnBRmsviewH() 
{
CCS_TRY;
	DoSelectOda(&m_RMSCodeH);
CCS_CATCH_ALL;
}

/********************************************************************************************
Checkt, ob ein von omDelActivePType, omDelUPType gecheckt ist und l�scht gegebenfalls
********************************************************************************************/
void DutyPlanAbsencePp::OnBStart() 
{
	CCS_TRY;

	CString olFrom, olTo, olCode, olTmp;
	bool blOK = true;
	// Abwesenheits-Code in Dienstplan Stufe 'Urlaub' �bertragen?
	bool blAddToUPType = omCheckUPType.GetCheck() != 0;
	// Abwesenheits-Code in Dienstplan aktuelle Stufe �bertragen?
	bool blAddToActivePType = omCheckActivePType.GetCheck() != 0;
	// Wochenende auf Regul�r-Frei-Default-Schicht (aus PARTAB) setzen?
	// (Hintergrund: wenn innerhalb des Zeitraums auch Sonnabende und Sonntage
	// liegen, bekommen diese nicht die einzuf�gende Schicht, sondern diejenige
	// Schicht, die per Default f�r regul�r arbeitsfreie Tage (also nicht
	// Urlaubskonto-relevant) gilt.)
	bool blOverwiteWeekend = omRBWeekendFree.GetCheck() != 0;
	// Sonst: alle Schichten, die nicht das Flag regul�r arbeitsfrei
	// haben, werden mit der einzuf�genden Schicht �berschrieben.
	// Da denkbar ist, dass irgendein Kunde dieses UND obiges Feature 
	// <blOverwiteWeekend> parallel zulassen m�chte (keine Radiobuttons
	// sondern CheckBoxen), werden beide BOOLeans getrennt ausgewertet und
	// weiter unten an DutyInsert::WriteInDB() �bergeben. F�r diesen Fall
	// muss dann nur noch DutyInsert::WriteInDB() angepasst werden.
	bool blOverwiteRegFree = omRBRegFree.GetCheck() != 0;
	
	// Abwesenheits-Code aus Dienstplan Stufe 'Urlaub' entfernen?
	bool blDelFromUPType = omDelUPType.GetCheck() != 0;
	// Abwesenheits-Code aus Dienstplan aktuelle Stufe entfernen?
	bool blDelFromActivePType = omDelActivePType.GetCheck() != 0;


	m_From.GetWindowText(olFrom);
	m_To.GetWindowText(olTo);
	m_RMSCodeH.GetWindowText(olCode);
	
	// Fehlerkontrolle

	CString olErrorText("");
	COleDateTime olTimeFrom, olTimeTo, olTimeNow;
	olTimeNow = COleDateTime::GetCurrentTime ();
	olTimeNow -= 1;

	if(m_From.GetStatus() && olFrom.GetLength() && m_To.GetStatus() && olTo.GetLength())
	{
		olTimeFrom = OleDateStringToDate(olFrom);
		olTimeTo = OleDateStringToDate(olTo);
		if(	olTimeFrom.GetStatus() == COleDateTime::valid && 
			olTimeTo.GetStatus() == COleDateTime::valid &&
			olTimeTo >= olTimeFrom)
		{
			if (olTimeFrom < olTimeNow || olTimeTo < olTimeNow)
			{
				// Fehler 
				blOK = false;
				// Sie k�nnen keine Schichten in der Vergangenheit planen.
				olErrorText = LoadStg(IDS_STRING63452);
			}
			//else: Zeitangaben korrekt
		}
		else
		{
			// Fehler 
			blOK = false;
			// Fehler bei der Eingabe von Zeit oder Datum.\n*INTXT*
			olErrorText = LoadStg(IDS_STRING1855);
		}
	}
	else
	{
		// Fehler 
		blOK = false;
		// Fehler bei der Eingabe von Zeit oder Datum.\n*INTXT*
		olErrorText = LoadStg(IDS_STRING1855);
	}

	if(!blOK)
	{
		MessageBox(olErrorText,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);

		return;
	}

	
	// sollen wir einf�gen oder l�schen?
	if(omCheckActivePType.GetCheck() == 1 || omCheckUPType.GetCheck() == 1)
	{
		// Abwesenheit einf�gen
		DutyInsert olDutyInsert;
		olDutyInsert.WriteInDB(lmStfUrno, olTimeFrom, olTimeTo, olCode, "2",pomParent,blAddToActivePType,blAddToUPType,blOverwiteWeekend,blOverwiteRegFree,true);
	}
	else if(omDelActivePType.GetCheck() == 1 || omDelUPType.GetCheck() == 1)
	{
		// l�schen
		DutyAbsenceOverviewDlg	olDlg(lmStfUrno, olTimeFrom, olTimeTo, olCode,this,blDelFromActivePType,blDelFromUPType);
		olDlg.DoModal();
	}


	CCS_CATCH_ALL;
}
//---------------------------------------------------------------------------


void DutyPlanAbsencePp::SetLabels()
{
	//radio-buttons
	SetDlgItemText(IDC_RADIO_REGULAR_FREE,LoadStg(IDC_RADIO_REGULAR_FREE));
	SetDlgItemText(IDC_RADIO_WEEKEND_FREE,LoadStg(IDC_RADIO_WEEKEND_FREE));

	//statics
	SetDlgItemText(IDS_STRING1742,LoadStg(IDS_STRING1742));
	SetDlgItemText(IDC_S_von,LoadStg(IDC_S_von));
	SetDlgItemText(IDC_S_bis,LoadStg(IDC_S_bis));

	//checkboxes
	SetDlgItemText(IDC_CHECKUPTYPE,LoadStg(IDC_CHECKUPTYPE));
	SetDlgItemText(IDC_CHECKACTIVEPTYPE,LoadStg(IDC_CHECKACTIVEPTYPE));
	SetDlgItemText(IDC_DELUPTYPE,LoadStg(IDC_DELUPTYPE));
	SetDlgItemText(IDC_DELACTIVETYPE,LoadStg(IDC_DELACTIVETYPE));

	//buttons
	SetDlgItemText(IDC_B_INSERT,LoadStg(IDC_B_INSERT));
}


//*******************************************************************************************************************
// DoSelectOda(): ruft den Dialog zur Auswahl einer Abwesenheit auf und 
//	tr�gt den Code im �bergebenen CCSEdit ein.
// R�ckgabe:	keine
//*******************************************************************************************************************

void DutyPlanAbsencePp::DoSelectOda(CCSEdit *popCCSEdit)
{
CCS_TRY;
	// sicherheitshalber
	if (popCCSEdit == NULL) return;

	// Arrays mit Daten f�llen
	CStringArray olCol1, olCol2, olCol3;
 	CString olUrno;
	for(int i = 0; i < ogOdaData.omData.GetSize(); i++)
	{
		olUrno.Format("%ld", ogOdaData.omData[i].Urno);
		olCol1.Add(CString(ogOdaData.omData[i].Sdac));
		olCol2.Add(CString(ogOdaData.omData[i].Sdan));
		olCol3.Add(olUrno);
	}
	
	// Auswahl-Dialog ausf�hren
	CCommonGridDlg olDlg(LoadStg(IDS_STRING401),&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING1839), LoadStg(IDS_STRING1840), this);
	if(olDlg.DoModal() == IDOK)
	{
		ODADATA *prlOda = ogOdaData.GetOdaByUrno(atol(olDlg.GetReturnUrno()));
		if(prlOda != NULL)
		{
			// g�ltige Abwesenheit eintragen
			popCCSEdit->SetInitText(prlOda->Sdac);
		}
	}
CCS_CATCH_ALL;
}

/********************************************************************************************
Checkt, ob ein von omCheckActivePType, omCheckUPType gecheckt ist und l�scht gegebenfalls
********************************************************************************************/
void DutyPlanAbsencePp::OnDelUpType() 
{
	omCheckActivePType.SetCheck(0);
	omCheckUPType.SetCheck(0);
}

/********************************************************************************************
Checkt, ob ein von omCheckActivePType, omCheckUPType gecheckt ist und l�scht gegebenfalls
********************************************************************************************/
void DutyPlanAbsencePp::OnDelActivePType() 
{
	omCheckActivePType.SetCheck(0);
	omCheckUPType.SetCheck(0);
}

/********************************************************************************************
Checkt, ob ein von omDelActivePType, omDelUPType gecheckt ist und l�scht gegebenfalls
********************************************************************************************/
void DutyPlanAbsencePp::OnCheckActivePType() 
{
	omDelUPType.SetCheck(0);
	omDelActivePType.SetCheck(0);
}

/********************************************************************************************
Checkt, ob ein von omDelActivePType, omDelUPType gecheckt ist und l�scht gegebenfalls
********************************************************************************************/
void DutyPlanAbsencePp::OnCheckUpType() 
{
	omDelUPType.SetCheck(0);
	omDelActivePType.SetCheck(0);
}