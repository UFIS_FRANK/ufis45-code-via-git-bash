#if !defined(AFX_DUTYSHOWJOBSDLG_H__AFFD2AD3_D758_11D4_AC8E_0050DA1CABCB__INCLUDED_)
#define AFX_DUTYSHOWJOBSDLG_H__AFFD2AD3_D758_11D4_AC8E_0050DA1CABCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DutyShowJobsDlg.h : header file
//
#include <stdafx.h>
#include <DutyRoster_View.h>
//#include "CCSEdit.h"
#include <CedaBsdData.h>
#include <CedaOdaData.h>
#include <CedaRelData.h>

class CGridControl;

#define DUTY_SHOW_JOBS_COLCOUNT 5

/////////////////////////////////////////////////////////////////////////////
// DutyShowJobsDlg dialog

class DutyShowJobsDlg : public CDialog
{
// Construction
public:
	DutyShowJobsDlg(CWnd* pParent = NULL);   // standard constructor
	~DutyShowJobsDlg();

	// behandelt die Broadcasts BC_REL_CHANGE,BC_REL_DELETE und BC_REL_NEW
	void ProcessDutyShowJobsDlgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Dialog Data
	//{{AFX_DATA(DutyShowJobsDlg)
	enum { IDD = IDD_DUTY_SHOWJOBS_DLG };
	CButton	m_b_Update;
	CButton	m_b_Delete;
	CButton	m_b_Find;
	CCSEdit	m_e_DayFrom;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DutyShowJobsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DutyShowJobsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBFind();
	afx_msg void OnUpdate();
	afx_msg void OnReleasedJobs();
	afx_msg void OnDeleteJobs();
	afx_msg void OnOpendJobs();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	DutyReleaseDlg*	pomParent;

	bool				bmShowOpendJobs;		// Modus der Tabelle: true - offene Jobs zeigen, false - abgearbeitete Jobs zeigen
	COleDateTime 		omDayFrom;			// Ab diesen Tag sollen abgearbeitete Jobs gezeigt werden

	CedaRelData			omRelData;			// aktuelle eingelesene RELDATA-s

	VIEWINFO*			prmViewInfo;

	// Beinhaltet alle ausgewählten Mitarbeiter
	CList<long,long>	omSelectedUrnoList;
	CString				omVpfr;
	CString				omVpto;

	int					imIDStatic;
	int					imIDProgress;

	// Das Grid
	CGridControl*		pomGrid;

	// Grid initialisieren
	void IniGrid ();
	void IniColWidths();
	void FillGrid(bool bpRegisterBC, bool bpShowMessageBox=true);	
	bool SortGrid(int ipRow);
	// Selection auslesen.
	void GetSelectedItems();
	void DeleteSelectedItems();		// Selektierte Jobs löschen

	// Broadcasts Register-Funktion
	void Register(void);

	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DUTYSHOWJOBSDLG_H__AFFD2AD3_D758_11D4_AC8E_0050DA1CABCB__INCLUDED_)
