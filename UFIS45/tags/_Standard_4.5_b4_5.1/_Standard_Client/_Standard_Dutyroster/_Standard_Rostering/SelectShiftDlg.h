#if !defined(AFX_SELECTSHIFTDLG_H__2D1386B5_D300_11D3_8FD8_0050DA1CABB6__INCLUDED_)
#define AFX_SELECTSHIFTDLG_H__2D1386B5_D300_11D3_8FD8_0050DA1CABB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectShiftDlg.h : Header-Datei
//

// DRR-Datens�tze
#include <CedaDrrData.h>
// Basisschichten
#include <CedaBsdData.h>
// Delegationen
#include <CedaDelData.h>
// VIEWINFO
#include <DutyRoster_View.h>

class CGridControl;

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CSelectShiftDlg 

class CSelectShiftDlg : public CDialog
{
// Konstruktion
public:
	CSelectShiftDlg(CWnd* pParent, DRRDATA *popDrr);   // Standardkonstruktor
	~CSelectShiftDlg();

	// R�ckgabe der Daten
	BSDDATA* GetBsdPtr(void) {return pomBsd;}
	ODADATA* GetOdaPtr(void) {return pomOda;}
	UINT GetCodeType(void) {return imCodeTypeSelected;}
	long GetCodeUrno(void) {return lmCodeUrnoSelected;}

// Dialogfelddaten
	//{{AFX_DATA(CSelectShiftDlg)
	enum { IDD = IDD_SELECT_SHIFT_DLG };
	CButton	m_But_Ok;
	CButton	m_But_Cancel;
	CButton	m_But_Search;
	CButton m_But_Search_Oda;
	CButton m_Combo_Fitting_Shifts;
	//}}AFX_DATA


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(CSelectShiftDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:
// Daten
	// Grid mit Basisschichten
	CGridControl		*pomBsdGrid;
	// Grid mit Abwesenheiten
	CGridControl		*pomOdaGrid;
	// Dialogtitel
	CString				omCaption;
	// der Basisschichtdatensatz, nachdem mit OK beendet wurde
	BSDDATA				*pomBsd;
	// der Abwesenheitsdatensatz, nachdem mit OK beendet wurde
	ODADATA				*pomOda;
	UINT				imCodeTypeSelected;
	long				lmCodeUrnoSelected;
	// der DRR mit den Filterwerten f�r anzuzeigende Schichten
	DRRDATA				*pomDrr;
	// Die Ansicht, die die Organisationscodes zum Ausfiltern
	// nicht relevanter Basisschichten h�lt. Die anzuzeigenden Organisationen
	// werden in der Registerkarte 'Statistik' definiert.
	VIEWINFO			*prmViewInfo;

// Funktionen	
	// Grid f�r Basisschichten initialisieren
	void InitGrids(void);
	// Spaltenattribute des Basisschichten-Grids einstellen
	void InitColWidths(void);
	// Grid f�r Basisschichten mit Daten f�llen
	void FillBsdGrid(void);
	void FillOdaGrid(void);
	// Grid auf- oder absteigend nach Spalte <ipRow> sortieren
	bool SortGrid(CGridControl* popGrid,int ipRow);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CSelectShiftDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnButtonFindScod();
	afx_msg void OnButtonFindOda();
	afx_msg void OnButtonFittingShifts();
	//}}AFX_MSG
    afx_msg LONG OnGridLButton(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_SELECTSHIFTDLG_H__2D1386B5_D300_11D3_8FD8_0050DA1CABB6__INCLUDED_
