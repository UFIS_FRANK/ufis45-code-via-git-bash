// cedadrrdata.cpp - Klasse f�r die Handhabung von DRR-Daten (Daily Roster Record)
//

#include <stdafx.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen 
//************************************************************************************************************************************************
//************************************************************************************************************************************************

// globale Callback-Funktion f�r den Broadcast-Handler
void  ProcessRelDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************************************
// ProcessDrrCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_DRR_CHANGE, BC_DRR_NEW und BC_DRR_DELETE. Ruft zur eigentlichen Bearbeitung
//	der Nachrichten die Funktion CedaDrrData::ProcessDrrBc() der entsprechenden 
//	Instanz von CedaDrrData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaDrrData *polDrrData = (CedaDrrData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	BC_TRY
	polDrrData->ProcessDrrBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
	// Sanduhr ausblenden
	RemoveWaitCursor();
}

//************************************************************************************************************************************************
// ProcessRelDrrCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_RELOAD_SINGLE_DRR, BC_RELOAD_MULTI_DRR und BC_RELDRR. Ruft zur eingentlichen 
//	Bearbeitung der Nachrichten die Funktion CedaDrrData::ProcessRelDrrBc() der 
//	entsprechenden Instanz von CedaDrrData auf.
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessRelDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{ 
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaDrrData *polDrrData = (CedaDrrData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polDrrData->ProcessRelDrrBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	RemoveWaitCursor();
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDrrData
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaDrrData::CedaDrrData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r DRR-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(DRRDATA, DrrDataRecInfo)
		FIELD_OLEDATE	(Avfr,"AVFR")	// anwwesend von
		FIELD_OLEDATE	(Avto,"AVTO")	// anwesend bis
		FIELD_CHAR_TRIM	(Bufu,"BUFU")	// Funknummer
		FIELD_OLEDATE	(Cdat,"CDAT")	// erzeugt am (Creation Date)
		FIELD_CHAR_TRIM	(Drrn,"DRRN")	// Schichtnummer (1-n)
		FIELD_CHAR_TRIM	(Drsf,"DRSF")	// Flag existiert ein Drs-Datensatz
		FIELD_CHAR_TRIM	(Expf,"EXPF")	// Export Fule ('A' = Archive oder '_' = no Archive)
		FIELD_OLEDATE	(Lstu,"LSTU")	// letzte �nderung (last update time)
		FIELD_CHAR_TRIM	(Rema,"REMA")	// Bemerkung (remark)
		FIELD_CHAR_TRIM	(Rosl,"ROSL")	// Planungsstufe (roster level)
		FIELD_CHAR_TRIM	(Ross,"ROSS")	// Status der Planungstufe ('L' = last, 'A' = active/current, 'N' = next)
		FIELD_OLEDATE	(Sbfr,"SBFR")	// Pausenlage von
		FIELD_CHAR_TRIM	(Sblp,"SBLP")	// Pausenl�nge bezahlt
		FIELD_CHAR_TRIM	(Sblu,"SBLU")	// Pausenl�ne unbezahlt
		FIELD_OLEDATE	(Sbto,"SBTO")	// Pausenlage bis
		FIELD_CHAR_TRIM	(Scod,"SCOD")	// Schichtcode
		FIELD_CHAR_TRIM	(Scoo,"SCOO")	// urspr�ngliche BSD-Schichtcode
		FIELD_CHAR_TRIM	(Sday,"SDAY")	// Schichttag
		FIELD_LONG		(Stfu,"STFU")	// MA-Urno
		FIELD_LONG		(Urno,"URNO")	// DS-Urno
		FIELD_CHAR_TRIM	(Usec,"USEC")	// Anwender - Ersteller
		FIELD_CHAR_TRIM	(Useu,"USEU")	// Anwender - letzte �nderung
		FIELD_LONG		(Bsdu,"BSDU")	// BSD-Urno
		FIELD_CHAR_TRIM	(Bkdp,"BKDP")	// 1. Pause bezahlt
		FIELD_CHAR_TRIM	(Drs1,"DRS1")	// Zusatzinfo zur Schicht / nach Schicht
		FIELD_CHAR_TRIM	(Drs2,"DRS2")	// Prozent der Arbeitsunf�higkeit
		FIELD_CHAR_TRIM	(Drs3,"DRS3")	// Zusatzinfo zur Schicht / vor Schicht
		FIELD_CHAR_TRIM	(Drs4,"DRS4")	// Zusatzinfo zur Schicht / gesammt
		FIELD_CHAR_TRIM	(Fctc,"FCTC") 	// Funktion des Mitarbeiters
		FIELD_CHAR_TRIM	(Prfl,"PRFL") 	// Protocol flag
		
		
		END_CEDARECINFO;
	// Infostruktur kopieren
    for (int i = 0; i < sizeof(DrrDataRecInfo)/sizeof(DrrDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DrrDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	
	// Tabellenname per Default auf DRR setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);
	
    // Feldnamen initialisieren
	strcpy(pcmListOfFields,	"AVFR,AVTO,BUFU,CDAT,DRRN,DRSF,EXPF,LSTU,REMA,ROSL,"
		"ROSS,SBFR,SBLP,SBLU,SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU,BSDU,BKDP,DRS1,DRS2,DRS3,DRS4,FCTC");
	bmRemovedPRFL = false;
	
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
	
	// min. / max. Datum f�r DRRs initialisieren
	omMinDay.SetStatus(COleDateTime::invalid);
	omMaxDay.SetStatus(COleDateTime::invalid);

}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaDrrData::~CedaDrrData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrrData::SetTableNameAndExt(CString opTableAndExtName)
{
	CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
	CCS_CATCH_ALL;
	return false;
}

/****************************************************************************************
wenn neu einzuf�gende DRR-BSD mit DELs verbunden ist, m�ssen DRA-s erstellt werden
Erl�uterung:
- manche Schichten haben Pflichtabwesenheiten, diese Tatschache ist als "x" in BSD.FDEL 
markiert
- wenn eine Schicht erzeugt wird, die solche BSD beinhaltet, m�ssen den Abwesenheiten entsprechende
DRAs ebenso kreiert werden
= wenn BSD.FDEL == "x", dann suchen wir in DELDATA alle Datens�tze mit BSDU == diese BSD.URNO raus.
f�r jeden DELDATA Datensatz erzeugen wir DRA mit DRA.ABFR = DEL.DELF, DRA.ABTO = DEL.DELT, DRA.SDAC = DEL.SDAC,
wobei dem DEL.SDAC ein ODA entsprechen mu� (ODA.SDAC == DEL.SDAC)
long opBsdUrno BSD.URNO mu� extra gegeben werden, mu� nicht unbedingt gleich prpDrr->Bsdu sein
R�ckgabe: false wenn Fehler
Wenn popDraMap != 0, die nimmt die neuen DRAs auf
****************************************************************************************/
bool CedaDrrData::CheckAndSetDraIfDrrBsdDelConnected(DRRDATA *prpDrr, long opBsdUrno, CMapPtrToPtr* popDraMap/*=0*/)
{
	CCS_TRY;
	
	CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
	CString olContinue = "ZRH,GVA,BSL";
	if (olContinue.Find (olCustomer) == -1) //nur f�r die Schweiz zus�tzliche records anlegen
		return true;

	if(!prpDrr) 
		return true;
	
	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(opBsdUrno);
	if(!polBsd)	
		return true;		// die Urno ist kein BSD-Urno (ODA oder falsch)
	
	if(strcmp(polBsd->Fdel, "x")) 
		return true;	// diese BSD hat keine verbundene DELs
	
	// verbundene DELs bekommen
	CCSPtrArray<DELDATA> olDelList;
	int ilDelNum = ogDelData.GetDelListByBsdu(polBsd->Urno, true, &olDelList);
	if(!ilDelNum) 
		return true; // die DELTAB ist f�r diese BSD leer, warum auch immer, wir haben nichts zu speichern
	
	DRADATA* prlDra;
	DELDATA* prlDel;
	COleDateTimeSpan olTime;
	
	for(int ilDelCnt=0;ilDelCnt < ilDelNum; ilDelCnt++)
	{
		prlDel = (DELDATA*)&olDelList[ilDelCnt];
		if(!prlDel) continue;	// wir wollen keine �berraschungen mit Assertion false
		prlDra = ogDraData.CreateDraData(prpDrr);
		if(!prlDra) break;	// wir wollen keine �berraschungen mit Assertion false
		
		// Zeit von
		CedaDataHelper::DateStringToOleDateTime(prpDrr->Sday,prlDra->Abfr);
		CedaDataHelper::HourMinStringToOleDateTimeSpan(CString(prlDel->Delf), olTime);
		prlDra->Abfr += olTime;
		
		// Zeit bis
		CedaDataHelper::DateStringToOleDateTime(prpDrr->Sday,prlDra->Abto);
		CedaDataHelper::HourMinStringToOleDateTimeSpan(CString(prlDel->Delt), olTime);
		prlDra->Abto += olTime;
		
		// Tag anpassen
		if(prlDra->Abto <= prlDra->Abfr)
		{
			prlDra->Abto += COleDateTimeSpan(1,0,0,0);
		}
		strcpy(prlDra->Sdac, prlDel->Sdac);
		
		// DRA speichern
		if(popDraMap != 0)
		{
			// in der Map speichern
			popDraMap->SetAt((void *)prlDra->Urno,prlDra);
		}
		else
		{
			// in ogDraData speichern
			if (!ogDraData.Insert(prlDra))
			{
				// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDraData.imLastReturnCode, ogDraData.omLastErrorMessage);
				AfxMessageBox(olErrorTxt, MB_ICONEXCLAMATION);
				return false;
			}
		}
	}
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// CheckAndSetLoadPeriod: setzt den Zeitraum f�r zu ladende Daten, pr�ft vorher, ob
//	die Daten OK sind und das Enddatum nicht vor dem Startdatum liegt.
// R�ckgabe:	false	->	einer der �bergebenen Zeitwerte ist ung�ltig
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrrData::CheckAndSetLoadPeriod(COleDateTime *popLoadFrom, COleDateTime *popLoadTo)
{
	CCS_TRY;
	TRACE("CedaDrrData::CheckAndSetLoadPeriod(): popLoadFrom=%s * popLoadTo=%s\n",popLoadFrom->Format("%d.%m.%Y"),popLoadTo->Format("%d.%m.%Y"));
	if((popLoadFrom == NULL) || (popLoadFrom->GetStatus() == COleDateTime::invalid) || 
		(popLoadTo == NULL) || (popLoadTo->GetStatus() == COleDateTime::invalid) || 
		(*popLoadFrom > *popLoadTo)) 
		return false;	// einer der Werte ist NULL oder ung�ltig
	
	// Start- und Endtag f�r zu ladende Daten anpassen wenn n�tig
	if ((omMinDay.GetStatus() == COleDateTime::invalid) || 
		((popLoadFrom != NULL) && (*popLoadFrom < omMinDay))) 
		omMinDay = *popLoadFrom;
	if ((omMaxDay.GetStatus() == COleDateTime::invalid) || 
		((popLoadTo != NULL) && (*popLoadTo > omMaxDay))) 
		omMaxDay = *popLoadTo; 
	TRACE("\t neues Ladedatum: omMinDay=%s *** omMaxDay=%s\n",omMinDay.Format("%d.%m.%Y"),omMaxDay.Format("%d.%m.%Y"));
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// GetReloadPeriod: pr�fen, ob entweder Nachlade-Start oder Nachlade-Ende innerhalb des 
//	Datenhaltungszeitraums <omMinDay> bis <omMaxDay> liegen und den sich 
//	�berschneidenden Zeitraum ermittlen
// R�ckgabe:	false	->	einer der �bergebenen Zeitwerte ist ung�ltig
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrrData::GetReloadPeriod(COleDateTime *popLoadFrom, COleDateTime *popLoadTo)
{
	CCS_TRY;
	TRACE("CedaDrrData::GetReloadPeriod(): popLoadFrom=%s * popLoadTo=%s *** omMinDay=%s * omMaxDay=%s\n",popLoadFrom->Format("%d.%m.%Y"),popLoadTo->Format("%d.%m.%Y"),omMinDay.Format("%d.%m.%Y"),omMaxDay.Format("%d.%m.%Y"));
	// Daten ok?
	if (*popLoadFrom > *popLoadTo)	return false;	// nein -> abbrechen
	
	// gibt es eine zeitliche �berschneidung?
	if (((*popLoadFrom < omMinDay) && (*popLoadTo < omMinDay)) ||
		((*popLoadFrom > omMaxDay) && (*popLoadTo > omMaxDay)))	
	{
		TRACE("CedaDrrData::GetReloadPeriod(): keine zeitliche �berschneidung, R�ckgabe ist FALSE\n");
		return false; // nein -> abbrechen
	}
	
	// Start- und Enddatum einstellen
	if (*popLoadFrom < omMinDay) *popLoadFrom = omMinDay;
	if (*popLoadTo > omMaxDay) *popLoadTo = omMaxDay;
	TRACE("\t*****�berschneidung***** popLoadFrom=%s * popLoadTo=%s *** omMinDay=%s * omMaxDay=%s\n",popLoadFrom->Format("%d.%m.%Y"),popLoadTo->Format("%d.%m.%Y"),omMinDay.Format("%d.%m.%Y"),omMaxDay.Format("%d.%m.%Y"));
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaDrrData::Register(void)
{
	CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren
	
	// DRR-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_DRR_CHANGE,        "CedaDrrData", "BC_DRR_CHANGE",        ProcessDrrCf);
	DdxRegister((void *)this,BC_DRR_NEW,           "CedaDrrData", "BC_DRR_NEW",		   ProcessDrrCf);
	DdxRegister((void *)this,BC_DRR_DELETE,        "CedaDrrData", "BC_DRR_DELETE",        ProcessDrrCf);
	DdxRegister((void *)this,BC_RELOAD_SINGLE_DRR, "CedaDrrData", "BC_RELOAD_SINGLE_DRR", ProcessRelDrrCf);
	DdxRegister((void *)this,BC_RELOAD_MULTI_DRR,  "CedaDrrData", "BC_RELOAD_MULTI_DRR",  ProcessRelDrrCf);
	DdxRegister((void *)this,BC_RELDRR,			   "CedaDrrData", "BC_RELDRR",            ProcessRelDrrCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
	CCS_CATCH_ALL;
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaDrrData::ClearAll(bool bpUnregister)
{
	CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
	CCS_CATCH_ALL;
	return false;
}

/*****************************************************************************************
L�scht alle Schichtdaten aus einem DRR
*****************************************************************************************/

bool CedaDrrData::ClearDrr(DRRDATA* popDrr, bool bpSave, bool bpCheckForDoubleTour)
{
	CCS_TRY;
	
	if (popDrr == NULL)
	{
		return false;
	}
	
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	
	if(!DeleteDRA_DRDbyChangeBsd(popDrr))
	{
		return RemoveWaitCursor(false);
	}
	//------------------------------------------------------------------------------------------
	// Bearbeiten von Doppeltouren
	//------------------------------------------------------------------------------------------
	if (bpCheckForDoubleTour && HasDoubleTour(popDrr))
	{
		// Den verbundenen DRR ermitteln
		DRRDATA* polDrr2 = GetConnectedDrr(popDrr);
		if (polDrr2 != NULL)
		{
			// Sch�ler oder Lehrer
			DRSDATA* polDrs2;
			polDrs2 = ogDrsData.GetDrsByDrru(polDrr2->Urno);
			
			if (polDrs2 != NULL) 
			{
				
				// Status sichern, DRS wird nach CancelDoubleTour zur�ckgesetzt.
				CString olStatus = CString(polDrs2->Stat);
				
				// Doppeltour trennen, ohne weitere Abfrage
				if (!CancelDoubleTour(popDrr,false))
				{
					return RemoveWaitCursor(false);
				}
				
				// Beim Lehrer nachfragen, beim Sch�ler nicht
				if (olStatus == "T")
				{
					if (AfxMessageBox(LoadStg(IDS_STRING982),MB_ICONQUESTION|MB_YESNO) == IDYES)
					{
						// Zugeh�rigen DRR der Doppeltour l�schen
						ClearDrr(polDrr2,true,false);
					}
				}
				else
					ClearDrr(polDrr2,true,false);
			}
			else
			{
				// corrupt data clean
				GetDefectDataString(ogRosteringLogText, polDrr2, " double tour flag DRSF is checked, but DRSDATA not found. data is corrupt. ");
				WriteInRosteringLog(LOGFILE_TRACE);
				strcpy (popDrr->Drsf,"");
			}
		}
		else
		{
			// corrupt data clean
			GetDefectDataString(ogRosteringLogText, popDrr, " double tour flag DRSF is checked, but connected employee not found. data is corrupt. ");
			WriteInRosteringLog(LOGFILE_TRACE);
			strcpy (popDrr->Drsf,"");
		}
	}
	
	
	// Sollte es mehere Schichten an diesem Tag geben wird diese Schicht gel�scht und
	// die anderen r�cken vor. Sonst werden nur die Nutz�daten des DRR gel�scht.
	
	// alle zus�tzlichen Schichten ermitteln
	CCSPtrArray<DRRDATA> olAdditionalDrrs;
	// mehr als einen DRR gefunden?
	if (ogDrrData.GetDrrListByKeyWithoutDrrn(CString(popDrr->Sday),popDrr->Stfu,CString(popDrr->Rosl),&olAdditionalDrrs) > 1)
	{
		// DRR und alle anh�ngigen DRDs, DRAs und DRS l�schen. Datens�tze mit h�heren
		// DRR-Nummern (Drrn) werden angepasst
		ogDrrData.Delete(popDrr,true);
		
		return RemoveWaitCursor(true);
	}
	else
	{
		
		// DRS l�schen, wenn vorhanden
		ogDrsData.DeleteDrsByDrrUrno(popDrr->Urno);
		
		// Drs-Felder l�schen
		popDrr->Drs1[0] = 0;
		popDrr->Drs3[0] = 0;
		popDrr->Drs4[0] = 0;
		
		//------------------------------------------------------------------------------------------
		// Nutzdaten l�schen
		//------------------------------------------------------------------------------------------
		popDrr->Avfr.SetStatus(COleDateTime::null);
		popDrr->Avto.SetStatus(COleDateTime::null);
		popDrr->Sbfr.SetStatus(COleDateTime::null);
		popDrr->Sbto.SetStatus(COleDateTime::null);
		
		strcpy (popDrr->Bkdp,"");
		strcpy (popDrr->Bufu,"");
		strcpy (popDrr->Expf,"");
		strcpy (popDrr->Rema,"");
		strcpy (popDrr->Sblp,"");
		strcpy (popDrr->Sblu,"");
		
		SetDrrDataLastUpdate(popDrr);
		// DefaultZeichen f�r gel�schte DRRs
		CString olText = ogCCSParam.GetParamValue(ogAppl,"ID_DRR_DELETED");
		strcpy (popDrr->Scoo,popDrr->Scod);
		strcpy (popDrr->Scod,olText);
		
		strcpy (popDrr->Drsf,"");
		
		popDrr->Bsdu = 0;
		
		// Abspeichern
		popDrr->IsChanged = DATA_CHANGED;
		if(bpSave)
		{
			RemoveWaitCursor(true);
			return Update(popDrr);
		}
	}
	
	RemoveWaitCursor(false);
	
	CCS_CATCH_ALL;
	return RemoveWaitCursor(false);
}

/*****************************************************************************************
L�scht Rosl und dazugeh�rige Felder
*****************************************************************************************/

bool CedaDrrData::ClearRosl(DRRDATA* popDrr, bool bpSave)
{
	CCS_TRY;
	*popDrr->Rosl = (char)0;
	popDrr->Bsdu = 0;
	popDrr->IsChanged = DATA_CHANGED;
	if(bpSave)
		return Save(popDrr);
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// ReadDrrByUrno: liest den DRR-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaDrrData::ReadDrrByUrno(long lpUrno, DRRDATA *prpDrr)
{
	CCS_TRY;
	CString olWhere;
	olWhere.Format("%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}
	
	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		ClearFastSocketBuffer();	
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	DRRDATA *prlDrr = new DRRDATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpDrr) == true)
	{  
		ClearFastSocketBuffer();	
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlDrr;
		ClearFastSocketBuffer();	
		return false;
	}
	
	return false;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// ReadFilteredByDateAndStaff: liest Datens�tze ein, deren Felder AVFR (Anwesend von)
//	und AVTO (anwesend bis) innerhalb des Zeitraums <opStart> bis <opEnd>
//	liegen. Die Parameter werden auf G�ltigkeit gepr�ft. Wenn <popLoadStfUrnoMap> nicht
//	NULL ist, werden nur die Datens�tze geladen, deren Feld STFU (Mitarbeiter-Urno)
//	im Array enthalten ist.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrrData::ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/, bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
	CCS_TRY;
	// G�ltigkeit der Datumsangaben pr�fen und eventuelle Ladedatum anpassen
	if (!CheckAndSetLoadPeriod(&opStart, &opEnd))
	{
		// Fehler -> terminieren
		return false;
	}

	// Anzahl der Urnos
	int ilCountStfUrnos = -1;
	if ((popLoadStfUrnoMap != NULL) && ((ilCountStfUrnos = popLoadStfUrnoMap->GetCount()) == 0))
	{
		// es gibt nichts zu tun, kein Fehler
		return true;	
	}

	// Puffer f�r WHERE-Statement
	CString olWhere;

	// keine Urno-Liste �bergeben
	if (popLoadStfUrnoMap == NULL)
	{
		// mehr Urnos als in WHERE-Clause passen -> alles laden und lokal filtern
		olWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s'",opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
		// Datens�tze einlesen und Ergebnis zur�ckgeben
		return Read(olWhere.GetBuffer(0),popLoadStfUrnoMap,bpRegisterBC,bpClearData);
	}
	else
	{
		// MA-Urnos mit in WHERE-Clause aufnehmen
		void *plDummy;			// das Objekt, immer NULL und daher obsolet
		long llStfUrno;			// die Urno als long
		CString olStfUrnoList;	// die Urno-Liste
		CString olStfUrno;		// einzelne URNO

		// alle Urnos ermitteln
		int ilUrnos		= 0;
		int ilMaxUrnos	= 100;

		//Daten l�schen
		if (bpClearData)
		{
			ClearAll(!bpRegisterBC);
		}
		else if (bpRegisterBC)
		{
			Register();
		}

		for(POSITION pos = popLoadStfUrnoMap->GetStartPosition(); pos != NULL;)   
		{
			ilUrnos++;
			popLoadStfUrnoMap->GetNextAssoc(pos,(void*&)llStfUrno,plDummy);
			olStfUrno.Format("'%ld'",llStfUrno);
			olStfUrnoList += ',' + olStfUrno;
			if (ilUrnos % ilMaxUrnos == 0)
			{
				// f�hrendes Komma aus Urno-Liste entfernen, WHERE-Statement generieren
				olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'",olStfUrnoList.Mid(1),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));

				if (!Read(olWhere.GetBuffer(0),NULL,bpRegisterBC,false)) return false;

				//URNO-Liste zur�cksetzen
				olStfUrnoList = "";
			}
		}
		// nun die restlichen laden (falls n�tig)
		if (olStfUrnoList.GetLength() > 0)
		{
			olWhere.Format("WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s'",olStfUrnoList.Mid(1),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"),opStart.Format("%Y%m%d"), opEnd.Format("%Y%m%d"));
			if (!Read(olWhere.GetBuffer(0),NULL,bpRegisterBC,false)) return false;
		}
	}
	CCS_CATCH_ALL;
	return true;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrrData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/, bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/,bool bpOverwrite /* = false */)
{
	CCS_TRY;

	if (CedaObject::IsFieldAvailable("DRR","PRFL"))
	{
		strcpy(pcmListOfFields,	"AVFR,AVTO,BUFU,CDAT,DRRN,DRSF,EXPF,LSTU,REMA,ROSL,"
								"ROSS,SBFR,SBLP,SBLU,SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU,BSDU,BKDP,DRS1,DRS2,DRS3,DRS4,FCTC,PRFL");
	}
	else
	{
		if (bmRemovedPRFL == false)
		{
			omRecInfo.DeleteAt(omRecInfo.GetSize() - 1);
			bmRemovedPRFL = true;
		}
	}



	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CedaData::CedaAction2("RT", "")) return false;
	}
	else
	{
//		if (!CCSCedaData::CedaAction2("RT", pspWhere,NULL,pcgDataBuf,"BUF1",false,0,true,NULL,NULL,NULL)) return false;
//		if (!CCSCedaData::CedaAction2("RT", pspWhere,NULL,pcgDataBuf,"BUF1",false,0,true,NULL,NULL,"/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */")) return false;
		if (!CCSCedaData::CedaAction2("RT", pspWhere,NULL,pcgDataBuf,"BUF1",false,0,true,NULL,NULL,"/*+ INDEX(DRRTAB DRRTAB_SDAY_STFU) */")) return false;
	}
	
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRRDATA *prlDrr = new DRRDATA;
		// Datensatz lesen
 		blMoreRecords = GetBufferRecord2(ilCountRecord,prlDrr);
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		if(blMoreRecords && 
			((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlDrr->Stfu, (void *&)prlVoid) == TRUE)))
		{ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlDrr, DRR_NO_SEND_DDX,bpOverwrite))
			{
				// Fehler -> lokalen Datensatz l�schen
				delete prlDrr;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlDrr);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			// kein weiterer Datensatz
			delete prlDrr;
		}
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();
	
	// Test: Anzahl der gelesenen DRR
	TRACE("Read-Drr: %d gelesen\n",ilCountRecord);
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}
	
	ClearFastSocketBuffer();	
	
    return true;
	CCS_CATCH_ALL;
	return false;
}

//---------------------------------------------------------------------------

bool CedaDrrData::ReadSpecialData(CCSPtrArray<DRRDATA> *popDrr,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
    return true;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpDrr> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::Insert(DRRDATA *prpDrr, bool bpSave /*= true*/)
{
	CCS_TRY;
	
	// �nderungs-Flag setzen
	prpDrr->IsChanged = DATA_NEW;
	
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpDrr) == false) return false;
	
	pomLastInternChangedDrr = prpDrr;
	
	// Broadcast DRR_NEW abschicken
	InsertInternal(prpDrr,true);
	
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird der Broadcast-Handler
//	aktiviert.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::InsertInternal(DRRDATA *prpDrr, bool bpSendDdx,bool bpOverwrite)
{
	CCS_TRY;
	DRRDATA* polPrevActDrr = 0;
	if(!IsValidDrr(prpDrr, &polPrevActDrr))
	{
		return false;
	}
	
	if(polPrevActDrr != 0)
	{
		// es gibt ein aktiver Datensatz drunter
		// zuerst m�ssen wir den auf "L" setzen und broadcasten
		strcpy(polPrevActDrr->Ross,"L");
		UpdateInternal(polPrevActDrr, true);
	}

	// Primary-Key dieses Datensatzes als String
	CString olPrimaryKey;
	olPrimaryKey.Format("%s-%ld-%s-%s",prpDrr->Sday,prpDrr->Stfu,prpDrr->Drrn,prpDrr->Rosl);
	DRRDATA *prlData;
	// �bergebener Datensatz NULL oder Datensatz mit diesem Schl�ssel
	// (Schichttag,Schichtnummer,MA-Urno) schon vorhanden?

	if (omKeyMap.Lookup(olPrimaryKey,(void *&)prlData)  == TRUE)
	{
		if (bpOverwrite)
		{
			// eventuell mit DRR verkn�pfte DRS entfernen
			ogDrsData.RemoveInternalByDrrUrno(prlData->Urno);
			// dann DRR intern l�schen
			DeleteInternal(prlData, DRR_NO_SEND_DDX);
		}
		else
		{
			return false;
		}
	}
	
	// Datensatz intern anf�gen
	omData.Add(prpDrr);
	//TRACE("Speichere DRR mit Schl�ssel = %s\n",olPrimaryKey);
	//TraceDrr(prpDrr, "DRR ");
	omKeyMap.SetAt(olPrimaryKey,prpDrr);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpDrr->Urno,prpDrr);
	
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,DRR_NEW,(void *)prpDrr);
	}
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpDrr->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::Update(DRRDATA *prpDrr,CString opDrs2/*=""*/)
{
	CCS_TRY;
	if (GetDrrByUrno(prpDrr->Urno) != NULL)
	{
		if (strcmp(opDrs2.GetBuffer(0),"2") == 0)
		{
			strcpy(prpDrr->Drs2,"2");
		}
		else
		{
			strcpy(prpDrr->Drs2,"1");
		}

		// �nderungsflag setzen
		if (prpDrr->IsChanged == DATA_UNCHANGED)
		{
			prpDrr->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(Save(prpDrr) == false) return false; 
		
		pomLastInternChangedDrr = prpDrr;
		
		// Broadcast DRR_CHANGE versenden
		UpdateInternal(prpDrr);
	}
	
    return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::UpdateInternal(DRRDATA *prpDrr, bool bpSendDdx /*true*/)
{
	CCS_TRY;
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
		// check validity of DRR record to get the repaired info
		DRRDATA* polPrevActDrr = 0;
		IsValidDrr(prpDrr, &polPrevActDrr);

		// ein Broadcast kann u.u. zum L�schen des MAs f�hren, dann knallts, wenn man
		// keine lokale Kopie des Datensatzes angelegt hat
		DRRDATA olDrrData;
		CopyDrr(&olDrrData,prpDrr);
		
		if(pomLastInternChangedDrr == prpDrr)
		{
			pomLastInternChangedDrr = &olDrrData;
		}
		
		ogDdx.DataChanged((void *)this,DRR_CHANGE,(void *)&olDrrData);
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaDrrData::DeleteInternal(DRRDATA *prpDrr, bool bpSendDdx /*true*/)
{
	CCS_TRY;

	// internen Broadcast senden BEVOR der Datensatz gel�scht wird
	if (bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,DRR_DELETE,(void *)prpDrr);
	}

	CString	olTmp;
	olTmp.Format ("%s-%ld-%s-%s",prpDrr->Sday,prpDrr->Stfu,prpDrr->Drrn,prpDrr->Rosl);
	omKeyMap.RemoveKey(olTmp);

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpDrr->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpDrr->Urno)
		{
			omData.DeleteAt(i);
			break;
		}
	}
	CCS_CATCH_ALL;
}

//*********************************************************************************************
// RemoveInternalByStaffUrno: entfernt alle DRRs aus der internen Datenhaltung,
//	die dem Mitarbeiter <lpStfUrno> zugeordnet sind. Die Datens�tze werden NICHT 
//	aus der Datenbank gel�scht.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::RemoveInternalByStaffUrno(long lpStfUrno, bool bpRemoveDRS/* = true*/, bool bpSendBC /*= false*/)
{
	CCS_TRY;
	DRRDATA *prlDrr;
	// alle DRRs durchgehen
	for(int i=0; i<omData.GetSize(); i++)
	{
		// DRR dieses MAs entfernen?
		if(lpStfUrno == omData[i].Stfu)
		{
			// ja
			if ((prlDrr =  GetDrrByUrno(omData[i].Urno)) == NULL) return false;
			// wenn gew�nscht, DRS entfernen
			if (bpRemoveDRS) ogDrsData.RemoveInternalByDrrUrno(omData[i].Urno,bpSendBC);
			// DRR entfernen
			DeleteInternal(prlDrr, bpSendBC);
			// neue Datensatz-Anzahl anpassen
			i--;
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpDrr> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::Delete(DRRDATA *prpDrr, BOOL bpWithSave)
{
	CCS_TRY;
	// Flag setzen
	prpDrr->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpDrr)) return false;
	}
	
	// Schl�sselfelder retten
	CString olSday = prpDrr->Sday;
	long llStfu = prpDrr->Stfu;
	CString olRosl = prpDrr->Rosl;
	// Wichtig!!!VOR dem l�schen die h�chste DRR-Nummer ermitteln und alle 
	// eventuell anzupassenden DRRs, DRDs und DRAs ermitteln
	CCSPtrArray<DRRDATA> olDrrList;
	int ilMaxDrrn = GetDrrListByKeyWithoutDrrn(olSday,llStfu,olRosl,&olDrrList);
	int ilDrrn = atoi(prpDrr->Drrn);
	
	// alle anh�ngigen DRA und DRD l�schen
	DeleteAllDraByDrr(prpDrr);
	DeleteAllDrdByDrr(prpDrr);
	// DRS l�schen, wenn vorhanden
	ogDrsData.DeleteDrsByDrrUrno(prpDrr->Urno);
	
	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpDrr,true);
	
	// wenn n�tig, alle anh�ngigen DRA und DRD mit h�heren Nummern
	// neu durchnummerieren
	if (ilDrrn < ilMaxDrrn)
	{
		// alle DRDs und DRAs �ndern
		for (int ilCount=ilDrrn+1; ilCount<=ilMaxDrrn; ilCount++)
		{
			DecreaseDrrn(olSday,llStfu,olRosl,ilCount);
		}
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// DecreaseDrrn: sucht den DRR mit den �bergebenen Schl�sselwerten und verringert
//	den Wert des Feldes Drrn um eins. Zus�tzlich wird das Feld Drrn aller anh�ngigen
//	DRAs uns DRDs angepasst.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	der Datensatz wurde nicht gefunden oder es gibt einen 
//							Datensatz mit den zu speichernden Schl�sselwerten
//*********************************************************************************************

bool CedaDrrData::DecreaseDrrn(CString opSday, long lpStfu, CString opRosl, int ipDrrn)
{
	CCS_TRY;
	// Debug: Drr-Nummer muss g�ltig sein
	ASSERT(ipDrrn > 0);
	
	// Puffer f�r Schl�ssel
	CString	olTmp;
	// Drr-Nummer in String umwandeln
	CString olDrrn, olNewDrrn;
	olDrrn.Format("%d",ipDrrn);
	olNewDrrn.Format("%d",ipDrrn-1);
	
	// Debug: der Zieldatensatz darf nicht existieren
	ASSERT(GetDrrByKey(opSday,lpStfu,olNewDrrn,opRosl) == NULL);
	
	// den zu �ndernden Datensatz ermitteln
	DRRDATA *prlDrr;
	if ((prlDrr = GetDrrByKey(opSday,lpStfu,olDrrn,opRosl)) == NULL) return false;
	
	// alle DRDs und DRAs anpassen
	ogDrdData.DecreaseDrrn(prlDrr);
	ogDraData.DecreaseDrrn(prlDrr);
	
	// aus den Datenhaltungsmaps eleminieren
	// Prim�rschl�ssel l�schen
	olTmp.Format("%s-%ld-%s-%s",prlDrr->Sday,prlDrr->Stfu,prlDrr->Drrn,prlDrr->Rosl);
	omKeyMap.RemoveKey(olTmp);
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prlDrr->Urno);
	
	// DRR-Nummer anpassen
	strcpy(prlDrr->Drrn,olNewDrrn.GetBuffer(0));
	// �nderungsflags setzen
	SetDrrDataLastUpdate(prlDrr);
	
	// und wieder ab in die Key-Maps
	// Key formatieren f�r Schichttag/MA-Urno-Map
	olTmp.Format("%s-%ld-%s-%s",prlDrr->Sday,prlDrr->Stfu,prlDrr->Drrn,prlDrr->Rosl);
	omKeyMap.SetAt(olTmp,prlDrr);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prlDrr->Urno,prlDrr);
	
	bool bRet = Save(prlDrr);
	
	if(bRet)
	{
		// Broadcast senden
		ogDdx.DataChanged((void *)this,DRR_CHANGE,(void *)prlDrr);
	}
	
	return bRet;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// SwapDrrn: vertauscht die DRR-Nummern zweier DRRs und aller anh�ngiger DRDs und DRAs.
//	Die Funktion dient dazu, einen Zusatz-DRR zum ersten DRR zu machen (siehe
//	CTageslisteDlg).
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	der Datensatz wurde nicht gefunden oder es gibt einen 
//							Datensatz mit den zu speichernden Schl�sselwerten
//*********************************************************************************************

bool CedaDrrData::SwapDrrn(DRRDATA *popDrr, CString opNewDrrn)
{
	CCS_TRY;
	// Debug: Drr-Nummer muss g�ltig sein
	ASSERT(!opNewDrrn.IsEmpty() && (opNewDrrn.SpanExcluding("123456789") == ""));
	
	// Puffer f�r Schl�ssel
	CString	olTmp;
	
	// den zu �ndernden Datensatz ermitteln
	DRRDATA *prlDrr;
	if ((prlDrr = GetDrrByKey(CString(popDrr->Sday),popDrr->Stfu,opNewDrrn,CString(popDrr->Rosl))) == NULL) return false;
	
	// alle DRDs und DRAs anpassen
	ogDrdData.SwapDrrn(popDrr,prlDrr);
	ogDraData.SwapDrrn(popDrr,prlDrr);
	
	//----------------------------------------------------------------------------------------------------
	// beide DRRs aus den Key-Maps entfernen, die DRR-Nummern vertauschen
	// und dann wieder in den Key-Maps speichern
	//----------------------------------------------------------------------------------------------------
	// Prim�rschl�ssel l�schen
	olTmp.Format("%s-%ld-%s-%s",prlDrr->Sday,prlDrr->Stfu,prlDrr->Drrn,prlDrr->Rosl);
	omKeyMap.RemoveKey(olTmp);
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prlDrr->Urno);
	
	// Prim�rschl�ssel l�schen
	olTmp.Format("%s-%ld-%s-%s",popDrr->Sday,popDrr->Stfu,popDrr->Drrn,popDrr->Rosl);
	omKeyMap.RemoveKey(olTmp);
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)popDrr->Urno);
	
	// DRR-Nummern vertauschen
	strcpy(prlDrr->Drrn,popDrr->Drrn);
	strcpy(popDrr->Drrn,opNewDrrn.GetBuffer(0));
	
	// �nderungsflags setzen
	SetDrrDataLastUpdate(prlDrr);
	SetDrrDataLastUpdate(popDrr);
	
	// Datens�tze speichern
	Save(prlDrr);
	Save(popDrr);
	
	// und wieder ab in die Key-Maps
	// Key formatieren f�r Schichttag/MA-Urno-Map
	olTmp.Format("%s-%ld-%s-%s",prlDrr->Sday,prlDrr->Stfu,prlDrr->Drrn,prlDrr->Rosl);
	omKeyMap.SetAt(olTmp,prlDrr);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prlDrr->Urno,prlDrr);
	
	// und wieder ab in die Key-Maps
	// Key formatieren f�r Schichttag/MA-Urno-Map
	olTmp.Format("%s-%ld-%s-%s",popDrr->Sday,popDrr->Stfu,popDrr->Drrn,popDrr->Rosl);
	omKeyMap.SetAt(olTmp,popDrr);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)popDrr->Urno,popDrr);
	
	// Broadcasts senden
	ogDdx.DataChanged((void *)this,DRR_CHANGE,(void *)prlDrr);
	ogDdx.DataChanged((void *)this,DRR_CHANGE,(void *)popDrr);
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// ProcessRelDrrBc: behandelt die Broadcasts BC_RELDRR, BC_RELOAD_MULTI_DRR und
//	BC_RELOAD_SINGLE_DRR.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDrrData::ProcessRelDrrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY;
	BcStruct *prlBcStruct = (BcStruct*)vpDataPointer;
	//bool blRet = false;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}
	
	switch (ipDDXType)
	{
	case BC_RELDRR: // Datens�tze f�r bestimmte (oder alle) MA nachladen
		Reload(prlBcStruct);
		break;
	case BC_RELOAD_MULTI_DRR: // alle in <prlBcStruct> enthaltenen Datens�tze nachladen
		ReloadMultiDrr(prlBcStruct);
		break;
	case BC_RELOAD_SINGLE_DRR: // den in <prlBcStruct> enthaltenen Datensatz nachladen
		ReloadSingleDrr(prlBcStruct);
		break;
	}// end switch
	CCS_CATCH_ALL;
}

//*********************************************************************************************
// Reload: l�dt alle Datens�tze mit den Urnos <opUrnos> neu.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::Reload(BcStruct *prpBcStruct)
{
	CCS_TRY;
	bool blOk = true;
	bool blErrorOccurred = false;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// BC-struct zerlegen
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	MakeClientString(prpBcStruct->Data);
	CString olData  = prpBcStruct->Data;
	CStringArray olDataArray; // Parameter-Liste
	if (ExtractItemList(olData, &olDataArray, '-') != 4)
	{
		return false;
	}
	// Startdatum
	CString olDateFrom = olDataArray[0];
	// Enddatum
	CString olDateTo = olDataArray[1];
	// Mitarbeiter-Urnos
	CString olUrnos = olDataArray[2];
	//Planungsstufen
	CStringArray olRoslArray;
	MakeClientString(olDataArray[3]);
	CString olRoslWhere = olDataArray[3];
	if (ExtractItemList(olRoslWhere, &olRoslArray, ',') != 2)
	{
		return false;
	}
	if (olRoslArray[0] == CString("0"))
	{
		olRoslWhere = CString ("ROSL = '1'");
	}
	else
	{
		olRoslWhere.Format ("ROSL IN ('%s','%s')", olRoslArray[0], olRoslArray[1]);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Ende Parameter zerlegen
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	DutyRoster_View *pView = NULL;

	CWinApp *pApp = AfxGetApp();
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while (pos != NULL && pView == NULL)
	{
		CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
		POSITION p1 = pDocTempl->GetFirstDocPosition();
		while (p1 != NULL && pView == NULL)
		{
			CDocument *pDoc = pDocTempl->GetNextDoc(p1);
			POSITION p2 = pDoc->GetFirstViewPosition();
			while (p2 != NULL && pView == NULL)
			{
				pView = DYNAMIC_DOWNCAST(DutyRoster_View,pDoc->GetNextView(p2));
			}
		}
	}

	if (pView == NULL)
		return false;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Ladezeitraum ermitteln: der Zeitraum (opDateFrom - opDateTo) muss
	// sich mit dem Zeitraum (omMinDay - omMaxDay) �berschneiden. Der Schnittzeitraum
	// ist der Zeitraum der nachzuladenden Daten.
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	COleDateTime olReloadFrom;
	COleDateTime olReloadTo;
	
	// Strings konvertieren
	if (!CedaDataHelper::DateStringToOleDateTime(olDateFrom,olReloadFrom) || 
		!CedaDataHelper::DateStringToOleDateTime(olDateTo,olReloadTo))	return false; // ung�ltige Daten

	// den relevanten Zeitraum ermitteln oder terminieren, wenn es keinen gibt
	if (!GetReloadPeriod(&olReloadFrom,&olReloadTo)) return false;

	if (strcmp(prpBcStruct->Selection,CCSCedaData::pcmReqId) != 0)
	{
		if (strcmp(prpBcStruct->Dest2,CCSCedaData::pcmReqId) == 0)
		{
			CString olFrom;
			CString olTo;
			bool blMessage = false;
			if (olRoslArray.GetSize() > 1)
			{
					if (olRoslArray[0] == CString("0"))
					{
						olFrom = LoadStg(IDS_STRING1586);
						olTo = LoadStg(IDS_STRING11827);
						blMessage = true;
					}
					else if (olRoslArray[0] == CString("1"))
					{
						olFrom = LoadStg(IDS_STRING11827);
						olTo = LoadStg(IDS_STRING1587);
						blMessage = true;
					}
					else if (olRoslArray[0] == CString("2"))
					{
						olFrom = LoadStg(IDS_STRING1587);
						olTo = LoadStg(IDS_STRING1588);
						blMessage = true;
					}
					else if (olRoslArray[0] == CString("3"))
					{
						olFrom = LoadStg(IDS_STRING1588);
						olTo = LoadStg(IDS_STRING1589);
						blMessage = true;
					}
					else if (olRoslArray[0] == CString("4"))
					{
						olFrom = LoadStg(IDS_STRING1589);
						olTo = LoadStg(IDS_STRING1593);
						blMessage = true;
					}
				if (blMessage == true)
				{
					olFrom.MakeUpper();
					olTo.MakeUpper();
					CString olMsg;
					olMsg.Format(LoadStg(IDS_STRING1577), olFrom, olTo);
					olMsg += LoadStg(IDS_STRING1579);
					::MessageBox(NULL, olMsg, LoadStg(IDS_STRING1578), MB_ICONINFORMATION | MB_OK);
				}
			}
		}

		DRRDATA *prlDrr = NULL; // zu l�schender DS
		CPtrArray olDrrArray; // speichert die zu l�schenden DRRs
		COleDateTime olSday;
		CString olStfu; 
		CStringArray olReloadUrnoArray;

		for (int ilC = 0; ilC < pView->rmViewInfo.oStfUrnos.GetSize();ilC++)
		{
			if (olUrnos.Find(pView->rmViewInfo.oStfUrnos[ilC]) != -1  || olUrnos == "ALL")
			{
				olReloadUrnoArray.Add(pView->rmViewInfo.oStfUrnos[ilC]);
			}
		}
	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//// Daten nachladen
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		CString olDrrWhere;
		CString olDrsWhere;
		int ilUrnos; // Urno-Z�hler
		CString olUrnoList; // Urno-Substring f�r WHERE-Statement
		// solange es noch MAs im Array gibt, f�r die Datens�tze nachgeladen werden m�ssen
		while (olReloadUrnoArray.GetSize() > 0)
		{
			// Urno-Z�hler zur�cksetzen
			ilUrnos = 0;
			// Urno-Substring zur�cksetzen
			olUrnoList = "";
			// max. f�r 100 MA nachladen
			while (ilUrnos < 99 && olReloadUrnoArray.GetSize() > 0)
			{
				ilUrnos++;

				// "alte" DRRs intern l�schen
				for (COleDateTime olDay = olReloadFrom; olDay <= olReloadTo; olDay += COleDateTimeSpan(1,0,0,0)) 
				{
					DRRDATA *prlDrr;
					prlDrr = ogDrrData.GetDrrByKey (olDay.Format("%Y%m%d"), atol(olReloadUrnoArray[0]), "2", olRoslArray[0]);
					if (prlDrr)
					{
						// "alten" DRSs intern l�schen
						ogDrsData.RemoveInternalByDrrUrno(prlDrr->Urno);
						// "alten" DRRs intern l�schen
						ogDrrData.DeleteInternal(prlDrr, false);
					}
					prlDrr = ogDrrData.GetDrrByKey (olDay.Format("%Y%m%d"), atol(olReloadUrnoArray[0]), "2", olRoslArray[1]);
					if (prlDrr)
					{
						// "alten" DRSs intern l�schen
						ogDrsData.RemoveInternalByDrrUrno(prlDrr->Urno);
						// "alten" DRRs intern l�schen
						ogDrrData.DeleteInternal(prlDrr, false);
					}
					prlDrr = ogDrrData.GetDrrByKey (olDay.Format("%Y%m%d"), atol(olReloadUrnoArray[0]), "1", olRoslArray[0]);
					if (prlDrr)
					{
						// "alten" DRSs intern l�schen
						ogDrsData.RemoveInternalByDrrUrno(prlDrr->Urno);
						// "alten" DRRs intern l�schen
						ogDrrData.DeleteInternal(prlDrr, false);
					}
					prlDrr = ogDrrData.GetDrrByKey (olDay.Format("%Y%m%d"), atol(olReloadUrnoArray[0]), "1", olRoslArray[1]);
					if (prlDrr)
					{
						// "alten" DRSs intern l�schen
						ogDrsData.RemoveInternalByDrrUrno(prlDrr->Urno);
						// "alten" DRRs intern l�schen
						ogDrrData.DeleteInternal(prlDrr, false);
					}
				}

				// n�chste Urno in Substring
				olUrnoList += CString(",'") + olReloadUrnoArray[0] + "'";
				// Urno aus Array l�schen
				olReloadUrnoArray.RemoveAt(0);
			}
			// f�hrendes Komma eleminieren
			olUrnoList = olUrnoList.Mid(1);
			// WHERE-Statements generieren
			olDrrWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s' AND STFU IN (%s) AND %s", olReloadFrom.Format("%Y%m%d"),olReloadTo.Format("%Y%m%d"), olUrnoList, olRoslWhere);
			olDrsWhere.Format("WHERE SDAY BETWEEN '%s' AND '%s' AND STFU IN (%s)", olReloadFrom.Format("%Y%m%d"),olReloadTo.Format("%Y%m%d"), olUrnoList);
			// DRR-Datens�tze und DRS-Datens�tze nachladen
			blOk = ((Read(olDrrWhere.GetBuffer(0),NULL,false,false,true)) &&
				(ogDrsData.Read(olDrsWhere.GetBuffer(0),NULL,false,false)));
			// im Fehlerfall merken
			if (!blOk) blErrorOccurred = true;
		}
	}
	else
	{
		CString olFrom;
		CString olTo;
		bool blMessage = false;
		if (olRoslArray.GetSize() > 1)
		{
			if (olRoslArray[0] != CString("0"))
			{
				if (olRoslArray[0] == CString("1"))
				{
					olFrom = LoadStg(IDS_STRING1586);
					olTo = LoadStg(IDS_STRING1587);
					blMessage = true;
				}
				else if (olRoslArray[0] == CString("2"))
				{
					olFrom = LoadStg(IDS_STRING1587);
					olTo = LoadStg(IDS_STRING1588);
					blMessage = true;
				}
				else if (olRoslArray[0] == CString("3"))
				{
					olFrom = LoadStg(IDS_STRING1588);
					olTo = LoadStg(IDS_STRING1589);
					blMessage = true;
				}
				else if (olRoslArray[0] == CString("4"))
				{
					olFrom = LoadStg(IDS_STRING1589);
					olTo = LoadStg(IDS_STRING1601);
					blMessage = true;
				}
			}
			if (blMessage == true)
			{
				CString olMsg;
				olMsg.Format(LoadStg(IDS_STRING1577), olFrom, olTo);
				::MessageBox(NULL, olMsg, LoadStg(IDS_STRING1578), MB_ICONINFORMATION | MB_OK);
			}
		}
	}

	// internen Broadcast an andere Module
	ogDdx.DataChanged((void *)this,RELDRR,NULL); //Update Viewer ect.
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Ende Daten nachladen
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return blErrorOccurred; // R�ckgabe: Fehler bei einer Lese-Operation aufgetreten?
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// ReloadSingleDrr: l�dt den in <prpBcStruct> enthaltenen Datensatz.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::ReloadSingleDrr(BcStruct *prpBcStruct)
{
	CCS_TRY;
	DRRDATA *prlNewDrr = NULL, *prlOldDrr = NULL;
	
	// neuen DRR erzeugen
	prlNewDrr = new DRRDATA;
	if (prlNewDrr == NULL){
		// ToDo: Fehlerbehandlung?
		return false;
	}
	
	// den n�chsten DRR extrahieren
	GetBufferRecord(&omRecInfo,prpBcStruct->Data,prlNewDrr);
	// den DRR in der internen Datenhaltung suchen
	prlOldDrr = GetDrrByUrno(prlNewDrr->Urno);
	
	// den neuen DRR einf�gen, aktualisieren oder l�schen?
	if(strchr("IU",prpBcStruct->Fields[0]) != NULL)
	{
		// DRR gefunden?
		if (prlOldDrr != NULL){
			// ja -> aus der internen Datenhaltung l�schen (ohne BC zu senden)
			DeleteInternal(prlOldDrr,DRR_NO_SEND_DDX);
			// Speicher freigeben
			delete prlOldDrr;
		}
		// den neuen DRR in die Datenhaltung aufnehmen, bzw. den ge�nderten 
		// wieder einf�gen (ohne BC zu senden)
		InsertInternal(prlNewDrr,DRR_SEND_DDX);
	}
	else if(prpBcStruct->Fields[0] == 'D')
	{
		// DRR gefunden?
		if (prlOldDrr != NULL){
			// ja -> aus der internen Datenhaltung l�schen, BC senden
			DeleteInternal(prlOldDrr);
			// Speicher freigeben
			delete prlOldDrr;
		}
		// Jetzt muss noch der Speicher des extrahierten DRRs freigegeben 
		// werden, da <prlNewDrr> nur dazu diente, die Urno zu ermitteln.
		delete prlNewDrr;
	} 
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// ReloadMultiDrr: l�dt alle Datens�tze, die im Parameter <prpBcStruct> kodiert sind.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::ReloadMultiDrr(BcStruct *prpBcStruct)
{
	CCS_TRY;
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Parameter zerlegen
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Art der �nderung (Insert, Update oder Delete)
	char *pclCmd = prpBcStruct->Fields;
	// Datens�tze in Stringform
	CStringArray olData;
	int ilCnt = ExtractItemList(CString(prpBcStruct->Data),&olData, '\n');
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Ende Parameter zerlegen
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Datens�tze nachladen
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Liste f�r alle DRRs, die von internen Modulen nachgeladen werden m�ssen,
	// die den DRR_MULTI_CHANGE Broadcast empfangen
	CCSPtrArray<DRRDATA> olDrrList;
	DRRDATA *prlNewDrr = NULL, *prlOldDrr = NULL;
	// alle DRRs durchgehen
	for(int i=0;i<ilCnt;i++)
	{
		// neuen DRR erzeugen
		prlNewDrr = new DRRDATA;
		if (prlNewDrr == NULL){
			// ToDo: Fehlerbehandlung?
			return false;
		}
		// den n�chsten DRR extrahieren
		GetBufferRecord(&omRecInfo,olData[i].GetBuffer(0),prlNewDrr);
		// den DRR in der internen Datenhaltung suchen
		prlOldDrr = GetDrrByUrno(prlNewDrr->Urno);
		// DRR gefunden?
		if (prlOldDrr != NULL){
			// ja -> aus der internen Datenhaltung l�schen (ohne BC zu senden)
			DeleteInternal(prlOldDrr,DRR_NO_SEND_DDX);
			// Speicher freigeben
			delete prlOldDrr;
		}
		// den neuen DRR einf�gen, aktualisieren oder l�schen?
		switch(*pclCmd)
		{
		case 'I': // Insert
		case 'U': // Update
			// den neuen DRR in die Datenhaltung aufnehmen, bzw. den ge�nderten 
			// wieder einf�gen (ohne BC zu senden)
			InsertInternal(prlNewDrr,DRR_NO_SEND_DDX);
			// den DRR in die Liste f�r die Module aufnehmen
			olDrrList.Add(prlNewDrr);
			break;
		case 'D': // Delete
			// DRR wurde oben bereits aus der internen Datenhaltung gel�scht.
			// Jetzt muss noch der Speicher des extrahierten DRRs freigegeben 
			// werden, da der DRR in diesem Fall nur dazu diente, die Urno
			// zu ermitteln.
			delete prlNewDrr;
			break;
		default: // Fehler - unbekanntes Kommando
			{
				break; 
			} 
		} // switch(*pclCmd)
	} // end for
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// Ende Datens�tze nachladen
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// ???
	ogBcHandle.GetBc();
	// internen Reload-Broadcast absetzen, wenn erforderlich
	ogDdx.DataChanged((void *)this,DRR_MULTI_CHANGE,(void *)&olDrrList);

	return true;
	CCS_CATCH_ALL;
	return false;
}

/********************************************************************************
Erstellt aus dem Selection-String den Urno-String und findet DRR
********************************************************************************/
DRRDATA* CedaDrrData::GetDrrFromSelectionStringUrno(CString opSelection)
{
	long llUrno;
	if (opSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(opSelection);
	}
	else
	{
		int ilLast;
		int ilFirst = opSelection.Find("=")+2;
		
		// der String kann in einer der zwei Formen angegeben werden:
		// "123456789 123456789" oder
		// "WHERE URNO = 123456789"
		if(ilFirst < 2)
		{
			// kein gefunden, RELDPL schickt den String in der Form: "123456789 123456789"
			ilFirst = 0;
			for(ilLast=ilFirst; ilLast<opSelection.GetLength(); ilLast++)
			{
				//char ilTest = opSelection.GetAt(ilLast);
				if(!isdigit((int)opSelection.GetAt(ilLast)))
				{
					break;
				}
			}
		}
		else
		{
			// gefunden, die Form: "WHERE URNO = 123456789"
			ilLast  = opSelection.GetLength();
		}
		llUrno = atol(opSelection.Mid(ilFirst,ilLast-ilFirst));
	}
	return GetDrrByUrno(llUrno);
}

//*********************************************************************************************
// ProcessDrrBc: behandelt die Broadcasts BC_DRR_NEW, BC_DRR_CHANGE und BC_DRR_DELETE.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDrrData::ProcessDrrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY;

	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	DRRDATA *prlDrr = NULL;

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	case BC_DRR_CHANGE:	// Datensatz �ndern
		{
			// Datenatz ermittlen
			prlDrr = GetDrrFromSelectionStringUrno(CString(prlBcStruct->Selection));

			if(prlDrr != NULL)
			{
				DRRDATA olDrrDataNew;
				GetRecordFromItemList(&olDrrDataNew,prlBcStruct->Fields,prlBcStruct->Data);
				if (olDrrDataNew.Stfu > 0 && prlDrr->Stfu != olDrrDataNew.Stfu)
				{
					// we have a "swap shift"!
					if (prlDrr->IsChanged == DATA_DELETED)
					{
						// it is the second BC of a swap shift operation
						DRRDATA *prlDrrSave = new DRRDATA;
						ogDrrData.CopyDrr(prlDrrSave,prlDrr);

						ogDrrData.Delete(prlDrr,FALSE);

						GetRecordFromItemList(prlDrrSave,prlBcStruct->Fields,prlBcStruct->Data);
						ogDrrData.Insert(prlDrrSave,false);
					}
					else
					{
						// it is the first BC of a swap shift operation
						DRRDATA *prlDrrData2 = GetDrrByKey(prlDrr->Sday,olDrrDataNew.Stfu,prlDrr->Drrn, prlDrr->Rosl);
						if (prlDrrData2 != NULL)
						{
							// both employees are in the view
							DRRDATA *prlDrrSave1 = new DRRDATA;
							DRRDATA* prlDrrSave2 = new DRRDATA;

							DRRDATA *prlDrrSave1_Drrn2 = NULL;
							DRRDATA *prlDrrSave2_Drrn2 = NULL;

							DRRDATA *prlDrrData1_Drrn2 = NULL;
							DRRDATA *prlDrrData2_Drrn2 = NULL;

							ogDrrData.CopyDrr(prlDrrSave1,prlDrr);
							ogDrrData.CopyDrr(prlDrrSave2,prlDrrData2);


							// 0. step: store 2nd shifts, if any
							prlDrrData1_Drrn2 = ogDrrData.GetDrrByKey(prlDrr->Sday,prlDrr->Stfu, "2",prlDrr->Rosl);
							prlDrrData2_Drrn2 = ogDrrData.GetDrrByKey(prlDrrData2->Sday,prlDrrData2->Stfu, "2",prlDrrData2->Rosl);

							if (prlDrrData1_Drrn2 != NULL)
							{
								prlDrrSave1_Drrn2 = new DRRDATA;
								ogDrrData.CopyDrr(prlDrrSave1_Drrn2,prlDrrData1_Drrn2);
							}

							if (prlDrrData2_Drrn2 != NULL)
							{
								prlDrrSave2_Drrn2 = new DRRDATA;
								ogDrrData.CopyDrr(prlDrrSave2_Drrn2,prlDrrData2_Drrn2);
							}


							// 1. step: deleting old shifts internal
							if (prlDrrData1_Drrn2 != NULL)
								ogDrrData.Delete(prlDrrData1_Drrn2,FALSE);

							if (prlDrrData2_Drrn2 != NULL)
								ogDrrData.Delete(prlDrrData2_Drrn2,FALSE);

							ogDrrData.Delete(prlDrr,FALSE);
							ogDrrData.Delete(prlDrrData2,FALSE);

							// 2. step: setting second record's STFU
							prlDrrSave2->Stfu = 1; //can't assign it to the correct employee here! --> dummy-STFU
							GetRecordFromItemList(prlDrrSave1,prlBcStruct->Fields,prlBcStruct->Data);

							// 3. step: inserting the records internal
							ogDrrData.Insert(prlDrrSave1,false);
							ogDrrData.Insert(prlDrrSave2,false);

							if (prlDrrSave1_Drrn2 != NULL)
								ogDrrData.Insert(prlDrrSave1_Drrn2,false);

							if (prlDrrSave2_Drrn2 != NULL)
								ogDrrData.Insert(prlDrrSave2_Drrn2,false);

							// 4. step: deleting second record internal
							prlDrrSave2->IsChanged = DATA_DELETED;
						}
						else
						{
							// only one employee is in the view
							DRRDATA *prlDrrSave = new DRRDATA;
							ogDrrData.CopyDrr(prlDrrSave,prlDrr);
							ogDrrData.Delete(prlDrr,FALSE);
							GetRecordFromItemList(prlDrrSave,prlBcStruct->Fields,prlBcStruct->Data);
							ogDrrData.Insert(prlDrrSave,false);
						}
					}
				}
				else
				{

					// "normal" change of the record
					GetRecordFromItemList(prlDrr,prlBcStruct->Fields,prlBcStruct->Data);
					UpdateInternal(prlDrr);
				}
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_DRR_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlDrr = new DRRDATA;
			GetRecordFromItemList(prlDrr,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlDrr, DRR_SEND_DDX);
		}
		break;
	case BC_DRR_DELETE:	// Datensatz l�schen
		{
			// Datenatz ermittlen
			prlDrr = GetDrrFromSelectionStringUrno(CString(prlBcStruct->Selection));
			
			if (prlDrr != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlDrr);
			}
		}
		break;
	default:
		break;
	}
	CCS_CATCH_ALL;
}

//*********************************************************************************************
// GetDrrByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRRDATA *CedaDrrData::GetDrrByUrno(long lpUrno)
{
	// der Datensatz
	DRRDATA *prpDrr;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpDrr) == TRUE)
	{
		return prpDrr;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
}

//*********************************************************************************************
// GetNextDrrByKey: sucht den n�chsten Datensatz mit den Schl�sselfeldern
//	der Vorlage mit inkrementierter DRR-Nummer Drrn.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRRDATA *CedaDrrData::GetNextDrrByKey(DRRDATA *popSourceDrr)
{
	CCS_TRY;
	// g�ltiger DRR?
	if (popSourceDrr == NULL) return NULL;
	// Drrn ermitteln und inkrementieren
	int ilDrrn = atoi(popSourceDrr->Drrn);
	CString olDrrn;
	olDrrn.Format("%d",++ilDrrn);
	// DRR ermitteln
	return GetDrrByKey(popSourceDrr->Sday,popSourceDrr->Stfu,olDrrn,popSourceDrr->Rosl);
	CCS_CATCH_ALL;
	return NULL;
}

//*********************************************************************************************
// GetDrrWithMaxDrrn: den DRR mit der h�chsten DRR-Nummer.
// R�ckgabe:	der DRR oder NULL, wenn es keinen gibt
//*********************************************************************************************

DRRDATA* CedaDrrData::GetDrrWithMaxDrrn(DRRDATA *popLastDrr)
{
	CCS_TRY;
	// n�chsten DRR ermitteln
	DRRDATA* prlNextDrr = GetNextDrrByKey(popLastDrr);
	// gibt es einen n�chsten DRR?
	if (prlNextDrr != NULL)
	{
		// ja -> rekursiv weitersuchen
		return GetDrrWithMaxDrrn(prlNextDrr);
	}
	else
	{
		// nein -> den letzten zur�ckgeben
		return popLastDrr;
	}
	CCS_CATCH_ALL;
	return NULL;
}

//*********************************************************************************************
// GetDrrByKey: sucht den Datensatz mit dem Prim�rschl�ssel aus 
// Schichttag (<opSday>), Schichtnummer (<lpDrrn>) ,Mitarbeiter-
// Urno (<lpStfu>) und Planungsstufe (<opRosl>).
// R�ckgabe: ein Zeiger auf den Datensatz oder NULL, wenn der
//           Datensatz nicht gefunden wurde
//*********************************************************************************************

DRRDATA *CedaDrrData::GetDrrByKey(CString opSday, long lpStfu, CString opDrrn /*="1"*/, CString opRosl /*= "2"*/)
{
	// Puffer f�r den Prim�rschl�ssel
	CString olKey;

	// Prim�rschl�ssel generieren
	olKey.Format("%s-%ld-%s-%s",opSday, lpStfu, opDrrn, opRosl);

	// der Datensatz
	DRRDATA *prlDrr = NULL;
	// Datensatz suchen
	if (omKeyMap.Lookup(olKey,(void *&)prlDrr) == TRUE)
	{
		// Datensatz gefunden -> Zeiger darauf zur�ckgeben
		return prlDrr;
	}
	// Datensatz nicht gefunden -> R�ckgabe NULL
	return NULL;
}

//*********************************************************************************************
// GetDrrListByKeyWithoutDrrn: sucht alle Datens�tze am Schichttag (<opSday>)
// f�r den MA <lpStfu> und mit der Planungsstufe <opRosl>. 
// !!! ACHTUNG !!! Funktioniert nur einwandfrei, solange die Schichtnummern (DRRN)
// fortlaufend bleiben. Wird eine Schicht gel�scht, m�ssen alle h�heren
// Schichtnummern dekrementiert werden.
// wenn popDrrList == 0, wird keine Liste ausgef�llt, nur die Anzahl Datens�tze zur�ckgegeben
// R�ckgabe:	Anzahl der gefundenen Datens�tze
//*********************************************************************************************

int CedaDrrData::GetDrrListByKeyWithoutDrrn(CString opSday, long lpStfu, CString opRosl, CCSPtrArray<DRRDATA> *popDrrList/*=0*/)
{
	CCS_TRY;
	// Schichtnummer(DRRN)-Z�hler
	int ilDrrn = 1;
	CString olDrrn = "1";
	DRRDATA  *prlDrr = NULL;

	// wenn keine Planungsstufe angegeben ist...
	if (opRosl == "")
	{
		// .. erst mal die aktive ermitteln
		if ((prlDrr = GetDrrByRoss(opSday,lpStfu,"1","","A")) == NULL)
		{
			// keine aktive Planungsstufe -> abbrechen
			return 0;
		}
		// sonst den gefundenen DRR hinzuf�gen...
		if(popDrrList != 0)
		{
			popDrrList->Add(prlDrr);
		}
		olDrrn.Format("%d",++ilDrrn);	// Nummer 1 gefunden -> n�chste Nummer
		// ...und mit der aktiven Planungsstufe den Rest suchen
		opRosl = CString(prlDrr->Rosl);
	}
	
	// alle DRRs suchen
	while ((prlDrr = GetDrrByKey(opSday,lpStfu,olDrrn,opRosl)) != NULL)
	{
		if(popDrrList != 0)
		{
			popDrrList->Add(prlDrr);
		}
		olDrrn.Format("%d",++ilDrrn);
	}

	// R�ckgabe: Anzahl der gefundenen DRRs
	return (ilDrrn-1);
	CCS_CATCH_ALL;
	return 0;
}

/**********************************************************************************************
Liefert DRRDATA mit ROSS == "A" nach dem angegebenen Tag/Stfu zur�ck
ROSL und DRRN sind von unten durchsucht, DRRN ab 1, ROSL = { 2, 3, 4, 5 }
**********************************************************************************************/
DRRDATA *CedaDrrData::GetActiveDrr(CString opSday, long lpStfu)
{
	CCS_TRY;
	DRRDATA* prlDrr=0;
	CString olDrrn;
	CString olRosl;
	
	bool bBreak = false;
	for(int ilDrrn=1; !bBreak; ilDrrn++)
	{
		olDrrn.Format("%i", ilDrrn);
		for(olRosl = "2";; olRosl = GetNextRosl(olRosl))
		{
			if(olRosl == "")
				break;
			
			prlDrr = GetDrrByKey(opSday, lpStfu, olDrrn, olRosl);
			
			if(!prlDrr || !strcmp(prlDrr->Ross, "A"))
			{
				bBreak = true;	// Ausgang aus doppelter Schleife
				break;
			}
		}
	}

	return prlDrr;		// entweder NULL, wenn kein gefunden, oder DRRDATA mit Ross == "A"

	CCS_CATCH_ALL;
	return 0;
}

//*********************************************************************************************
// GetDrrByRoss: sucht den Datensatz mit dem Schl�ssel <opSday>-<lpStfu>-
//	<opDrrn>-<opRosl> und pr�ft, ob die Planungsstufe gleich <opRoss> ist 
//	(m�gliche Werte: Aktiv = "A", n�chste = "N", Letzte = "L"). Wenn die
//	Planungsstufe <opRosl> nicht spezifiziert ist, wird aus allen vorhandenen
//	Planungsstufen (ausser Schichtplanstufe) der Datensatz herausgesucht, der
//	den Planungsstatus <opRoss> hat.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRRDATA *CedaDrrData::GetDrrByRoss(CString opSday, long lpStfu, CString opDrrn /*="1"*/, CString opRosl /*= "2"*/, CString opRoss /*= "A"*/)
{
	CCS_TRY;
	// der Datensatz
	DRRDATA *prlDrr = NULL;
	
	// alle Planungsstufen absuchen?
	if (opRosl == "")
	{
		// ja -> alle Planungsstufen h�her Schichtplan durchsuchen
		// Puffer f�r int-Konvertierung
		CString olRosl = "L";
		do
		{
			// Datensatz suchen und Status pr�fen
			if (((prlDrr = GetDrrByKey(opSday,lpStfu,opDrrn,olRosl)) != NULL) &&
				(prlDrr->Ross == opRoss))
			{
				// Datensatz gefunden, Status stimmt -> Zeiger zur�ckgeben
				return prlDrr;
			}
		} while ((olRosl = GetNextRosl(olRosl)) != "");
	}
	else
	{
		// Datensatz mit Planungsstufe <opRosl> suchen und Status pr�fen
		if (((prlDrr = GetDrrByKey(opSday,lpStfu,opDrrn,opRosl)) != NULL) &&
			(prlDrr->Ross == opRoss))
		{
			// Datensatz gefunden, Status stimmt -> Zeiger zur�ckgeben
			return prlDrr;
		}
	}
	// Datensatz nicht gefunden -> R�ckgabe NULL
	return NULL;
	CCS_CATCH_ALL;
	return NULL;
}

//*********************************************************************************************
// IsDrrDifferentFromShift: pr�ft, ob der Datensatz <popDrr> vom Stammdatensatz der 
//	Basisschicht oder Abwesenheit bei den Zeitwerten (Schichtbeginn, Schichtende, etc.) abweicht.
// die Funktion der Schicht wird nicht gepr�ft!
// R�ckgabe:	true	->	Zeitwerte weichen ab
//				false	->	Zeitwerte sind gleich
//*********************************************************************************************

bool CedaDrrData::IsDrrDifferentFromShift(DRRDATA *popDrr)
{
	CCS_TRY;
	// Parameter pr�fen
	if ((popDrr == NULL) || (strcmp(popDrr->Sday,"") == 0) || popDrr->Bsdu == 0)
	{
		return false;
	}
	
	// ADO:
	// Nur wenn es sich um einen "echten" Schichcode handelt darf verglichen werden
	// keine Abwesenheiten vergleichen
	
	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(popDrr->Bsdu);
	if (polBsd == NULL)
		// nicht weiter vergleichen.
		return false;
	
	// SDAY konvertieren
	COleDateTime olSday;
	if (!SdayToOleDateTime(popDrr,olSday)) return false;	// bei Fehler abbrechen
	
	// ESBG umwandeln in Zeit
	COleDateTimeSpan olEsbgSpan;
	if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Esbg,olEsbgSpan)) return false;	// bei Fehler abbrechen
	// Anwesend von - vergleichen
	if(popDrr->Avfr != olSday + olEsbgSpan) 
		return true;	// ungleich
	
	// LSEN umwandeln in Zeit
	COleDateTimeSpan olLsenSpan;
	if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Lsen,olLsenSpan)) return false;	// bei Fehler abbrechen
	// Sp�testes Schichtende fr�her als fr�hester Schichtbeginn?
	if (olLsenSpan <= olEsbgSpan)
	{
		// ja -> einen Tag zus�tzlich addieren
		olLsenSpan += COleDateTimeSpan(1,0,0,0);
	}
	// Anwesend bis - vergleichen
	if(popDrr->Avto != olSday + olLsenSpan) 
		return true;	// ungleich
	
	// BKF1 umwandeln in Zeit
	COleDateTimeSpan olBkf1Span;
	if(ogBsdData.GetBkf1Time(polBsd, olBkf1Span))
	{
		// Pause von - vergleichen
		if(popDrr->Sbfr != olSday + olBkf1Span) 
			return true;	// ungleich
	}
	else if(popDrr->Sbfr.GetStatus() == COleDateTime::valid && 
		popDrr->Sbto.GetStatus() == COleDateTime::valid &&
		popDrr->Sbfr != popDrr->Sbto)
	{
		// BSD hat keine Pausenzeit, DRR hat eine
		return true;
	}
	
	// BKT1 umwandeln in Zeit
	COleDateTimeSpan olBkt1Span;
	if(ogBsdData.GetBkt1Time(polBsd, olBkt1Span))
	{
		// Pause von - vergleichen
		if(popDrr->Sbto != olSday + olBkt1Span) 
			return true;	// ungleich
	}
	else if(popDrr->Sbfr.GetStatus() == COleDateTime::valid && 
		popDrr->Sbto.GetStatus() == COleDateTime::valid &&
		popDrr->Sbfr != popDrr->Sbto)
	{
		// BSD hat keine Pausenzeit, DRR hat eine
		return true;
	}
	
	//---------------------------------------------------------------------------------------------------------------------
	// Pausenl�nge vergleichen
	//---------------------------------------------------------------------------------------------------------------------
	if(strcmp(popDrr->Sblu,polBsd->Bkd1))
		return true;
	
	return false;
	
	CCS_CATCH_ALL;
	return false;
}

//*********************************************************************************************
// IsEditableByDutyRoster: pr�ft, ob der Datensatz <popDrr>, bzw. die 
//	Planungsstufe <opType> im Dienstplan bearbeitet werden darf. 
//	Wenn der Datensatz NULL ist, wird nur <opType> untersucht.
// R�ckgabe:	true	->	Datensatz darf bearbeitet werden
//				false	->	Datensatz darf nicht bearbeitet werden
//*********************************************************************************************

bool CedaDrrData::IsEditableByDutyRoster(DRRDATA *popDrr, CString opPType)
{
	CCS_TRY;
	//	TRACE("Pr�fe, ob DRR editierbar ist (Planungstyp: %s)\n",opPType);
	
	// der Datensatz ist im Dienstplan ver�nderbar wenn:
	// 1.) der Datensatz NULL ist und die Planungsstufe <opPType> 2 (= Dienstplanstufe) ist,
	//     (das bedeutet, dass der Datensatz erst noch angelegt werden muss)
	// 2.) die Planungsstufe <opPType> Wunsch ("W") oder Urlaub ("U") oder Langzeitdienstplan (L) ist,
	// 3.) der Datensatz den Planungsstatus aktiv hat.
	if (((popDrr == NULL) && (opPType == "2")) ||		/* 1. Fall */
		(opPType == "W") || (opPType == "U") || /*((opPType == "L")  && (popDrr == NULL)) ||*/			/* 2. Fall */
		((popDrr != NULL) && (CString(popDrr->Ross) == "A") && (opPType != "1")))	/* 3. Fall */
	{
		// ja -> Planungsstufe ist ver�nderbar
		return true;
	}
	// keiner der beschriebenen F�lle trifft zu -> Datensatz ist im Dienstplan
	// nicht ver�nderbar
	return false;
	CCS_CATCH_ALL;
	return false;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpDrr>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaDrrData::Save(DRRDATA *prpDrr)
{
	bool blRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048]; 
	
	if (prpDrr->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	
	switch(prpDrr->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		if(prpDrr->Avto.GetStatus() != 0)
		{
			AfxMessageBox("DATA_NEW, prpDrr->Avto invalid");
		}
		MakeCedaData(&omRecInfo,olListOfData,prpDrr);
		strcpy(pclData,olListOfData);
		blRc = CedaAction("IRT","","",pclData);
		prpDrr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrr->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrr);
		strcpy(pclData,olListOfData);
		blRc = CedaAction("URT",pclSelection,"",pclData);
		prpDrr->IsChanged = DATA_UNCHANGED;
		prpDrr->Repaired[0] = '\0';
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrr->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrr);
		strcpy(pclData,olListOfData);
		blRc = CedaAction("DRT",pclSelection,"",pclData);
		break;
	}
	
	// R�ckgabe: Ergebnis der CedaAction
	return blRc;
}

//*******************************************************************************
// Release: gibt alle internen Datens�tze frei und veranlasst die Speicherung.
//	<opFrom> und <opTo> geben an, von welcher Planungsstufe auf welche 
//	Planungsstufe freigegeben wurde (z.B. "1" Schichtplan auf "L" Lang-
//	zeitdienstplan).
// R�ckgabe:	Erfolg?
//*******************************************************************************

bool CedaDrrData::Release(CString opFrom, CString opTo, bool bpAction/*= false*/)
{
	CCS_TRY;
	
	// Kommandostrings f�r CEDA: INSERT-, UPDATE- und DELETE-Kommando
	CString olIRT, olURT, olDRT;
	// Feldliste: beschreibt die zu speichernden Felder der Tabelle
	CString olFieldList;
	// vollst�ndige Kommandos (Update, Insert und Delete) f�r CedaData::CedaAction()
	// Format:	1.Zeile:		Kommando, Feldliste
	//			1.-n. Zeile:	relevante Felder des Datensatzes (zum L�schen reicht die Urno, sonst alle Felder)
	CString olUpdateString, olInsertString, olDeleteString;
	// Zwischenspeicher
	CString olTmpText;
	// Z�hler f�r Anzahlen der ge�nderten/gel�schten/neuen Datens�tze
	int ilNoOfUpdates = 0, ilNoOfDeletes = 0, ilNoOfInserts = 0;
	// Anzahl der Felder pro Datensatz ermitteln
	CStringArray olFields;	// nur Hilfsarray, wird nicht weiter benutzt
	int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 
	
	// Kommandostrings (Header) generieren
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);
	
	CString olOrigFieldList;
	// Paketz�hler: alle hundert Datens�tze einen Schreibvorgang anstossen
	int ilCurrentCount = 0;
	// Gesamtz�hler, wird am Ende benutzt, um zu pr�fen, ob mind. 1 DS geschrieben wurde
	int ilTotalCount = 0;
	
	// Array, der die neu zu ladenden Datensatz-Urnos speichert (alle, die ge�ndert
	// wurden)
	CStringArray olStfUrnoArray;
	// Zeitraum f�r neu zu ladende Daten, initialisiert mit astronomischen Daten.
	// Die Daten werden mit den tats�chlichen min./max. Daten der gespeicherten
	// DRRs �berschrieben. Dazu werden beide Werte (von/bis) mit dem ersten 
	// auftretenden Datum initialisiert und dann die Grenzen mit jedem weiteren
	// Datum erweitert (siehe weiter unten)
	CString olRelFrom = CString("99991231");
	CString	olRelTo   = CString("19000101");
	CString olRelTmpDate;
	BOOL blStfFound = false;
	
	// alte Feldliste speichern
	olOrigFieldList = CString(pcmListOfFields);
	// Strings f�r CedaAction initialisieren
	olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
	olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
	olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
	
	// Datensynchronisation anstossen
	Synchronise("DUTYROSTER",true);
	
	// nein -> dann alle intern gecachten Datens�tze Speichern
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		////////////////////////////////////////////////////////////////////////
		//// �nderung pr�fen und Pakete mit Datens�tzen schn�ren
		////////////////////////////////////////////////////////////////////////
		// die Datensatz-Urno
		long llUrno;
		bool blDelete = false;
		// der Datensatz
		DRRDATA *prlDrr;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDrr);
		if(prlDrr->IsChanged != DATA_UNCHANGED )
		{
			CString olListOfData;
			CString olCurrentUrno;
			olCurrentUrno.Format("%d",prlDrr->Urno);
			// aus dem Datensatz einen String generieren (omResInfo ist Member von CCSCedaData)
			MakeCedaData(&omRecInfo,olListOfData,prlDrr);
			// was soll mit dem Datensatz geschehen
			switch(prlDrr->IsChanged)
			{
			case DATA_NEW:	// neuer DS
				olInsertString += olListOfData + CString("\n");
				ilNoOfInserts++;
				break;
			case DATA_CHANGED:	// ge�nderter DS
				olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
				ilNoOfUpdates++;
				break;
			case DATA_DELETED:	// DS l�schen
				olDeleteString += olCurrentUrno + CString("\n");
				blDelete = true;
				ilNoOfDeletes++;
				break;
			}
			// Z�hler inkrementieren
			ilCurrentCount++;
			ilTotalCount++;
			olTmpText.Format("%ld",prlDrr->Stfu);

			// Grenzen des Zeitraums f�r neu zu ladende Daten f�r 
			// Broadcast anpassen, wenn n�tig
			olRelTmpDate = prlDrr->Sday;
			if(olRelTmpDate < olRelFrom) olRelFrom = olRelTmpDate;
			if(olRelTmpDate > olRelTo)   olRelTo   = olRelTmpDate;

			// Mitarbeiter-Urno speichern, damit sp�ter ein Broadcast 
			// generiert werden kann. Nur die DRRs f�r betroffene 
			// MA von den Empf�ngern des Broadcasts geupdatet werden
			// m�ssen.
			blStfFound = false;
			for (int ilStfCount=0;(ilStfCount<olStfUrnoArray.GetSize()) && !blStfFound; ilStfCount++)
			{
				// MA im Array suchen
				if (olStfUrnoArray[ilStfCount] == olTmpText) 
				{
					// MA gefunden -> Suche abbrechen
					blStfFound = true;
				}
			}
			if (!blStfFound)
			{
				olStfUrnoArray.Add(olTmpText);	
			}
			if (blDelete == false)
			{
				// �nderungsflag zur�cksetzen
				prlDrr->IsChanged = DATA_UNCHANGED;
			}
			else
			{
				DeleteInternal(prlDrr, true);
				//Delete(prlDrr, false);
			}
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende �nderung pr�fen und Pakete mit Datens�tzen schn�ren
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		//// 500er Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
		// 500 Datens�tze im Paket?
		if(ilCurrentCount == 500)
		{
			// ja -> Paket speichern
			// neue Datens�tze erzeugt?
			if(ilNoOfInserts > 0)
			{
				// ja -> speichern
				if (bpAction)
				{
					CedaAction("REL","LATE,NOBC,NOLOG","",olInsertString.GetBuffer(0));
				}
				else
				{
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olInsertString.GetBuffer(0));
				}
			}
			// Datens�tze ge�ndert?
			if(ilNoOfUpdates > 0)
			{
				// ja -> speichern
				if (bpAction)
				{
					CedaAction("REL","LATE,NOBC,NOLOG","",olUpdateString.GetBuffer(0));
				}
				else
				{
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olUpdateString.GetBuffer(0));
				}
			}
			// Datens�tze gel�scht?
			if(ilNoOfDeletes > 0)
			{
				// ja -> aus Datenbank l�schen
				if (bpAction)
				{
					CedaAction("REL","LATE,NOBC,NOLOG","",olDeleteString.GetBuffer(0));
				}
				else
				{
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olDeleteString.GetBuffer(0));
				}
			}
			
			// Z�hler und Strings wieder auf Initialwerte setzen
			ilCurrentCount = 0;
			ilNoOfInserts = 0;
			ilNoOfUpdates = 0;
			ilNoOfDeletes = 0;
			olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
			olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
			olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
		}
		////////////////////////////////////////////////////////////////////////
		//// Ende 500er Paket in Datenbank speichern
		////////////////////////////////////////////////////////////////////////
	}//for(rlPos = ......
	
	
	////////////////////////////////////////////////////////////////////////
	//// Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
	////////////////////////////////////////////////////////////////////////
	// neue Datens�tze erzeugt?
	// ja -> Paket speichern
	// neue Datens�tze erzeugt?
	if(ilNoOfInserts > 0)
	{
		// ja -> speichern
		if (bpAction)
		{
			CedaAction("REL","LATE,NOBC,NOLOG","",olInsertString.GetBuffer(0));
		}
		else
		{
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olInsertString.GetBuffer(0));
		}
	}
	// Datens�tze ge�ndert?
	if(ilNoOfUpdates > 0)
	{
		// ja -> speichern
		if (bpAction)
		{
			CedaAction("REL","LATE,NOBC,NOLOG","",olUpdateString.GetBuffer(0));
		}
		else
		{
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olUpdateString.GetBuffer(0));
		}
	}
	// Datens�tze gel�scht?
	if(ilNoOfDeletes > 0)
	{
		// ja -> aus Datenbank l�schen
		if (bpAction)
		{
			CedaAction("REL","LATE,NOBC,NOLOG","",olDeleteString.GetBuffer(0));
		}
		else
		{
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG","",olDeleteString.GetBuffer(0));
		}
	}
	////////////////////////////////////////////////////////////////////////
	//// Ende Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
	////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////////
	//// Broadcast erzeugen
	////////////////////////////////////////////////////////////////////////
	
	// gab es neue/ge�nderte/gel�schte Datens�tze?
	if(ilTotalCount>0)
	{
		// ja -> alle betroffenen Mitarbeiter durchgehen
		while(olStfUrnoArray.GetSize() > 0)
		{
			// Mitarbeiter-Z�hler
			int ilUrnos = 0;	
			// String-Puffer f�r die Urnos aller Mitarbeiter
			CString olTmpUrnos;
			// maximal je 60 Mitarbeiter-Urnos im String speichern
			while(ilUrnos < 60 && olStfUrnoArray.GetSize() > 0)
			{
				ilUrnos++;
				olTmpUrnos += CString(",") + olStfUrnoArray[0];
				olStfUrnoArray.RemoveAt(0);
			}
			// f�hrendes Komma eleminieren
			olTmpUrnos = olTmpUrnos.Mid(1);
			// String f�r Broadcasts generieren
			CString olData;
			olData.Format("%s-%s-%s-%s,%s", olRelFrom, olRelTo, olTmpUrnos, opFrom /*von Planungsstufe*/, opTo /*auf Planungsstufe*/);
			MakeCedaString(olData);
			// CEDA veranlassen, einen Broadcast zu senden
			CedaAction("SBC","RELDRR","",CCSCedaData::pcmReqId,"",olData.GetBuffer(0));
		}
		// Broadcast f�r Coverage senden 
		CString olRange = olRelFrom + "-" + olRelTo;
		CedaAction("SBC","ENDRLR","","","",olRange.GetBuffer(0));
	}
	////////////////////////////////////////////////////////////////////////
	//// Ende Broadcast erzeugen
	////////////////////////////////////////////////////////////////////////
	
	// Datensynchronisation abschliessen
	Synchronise("DUTYROSTER");
	
	CCS_CATCH_ALL;
	return true;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrrData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList, char *pcpSelection, char *pcpSort, char *pcpData, char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, int ipFieldCount /*= 0*/)
{
	CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpTableName,pcpFieldList,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID,pomData,ipFieldCount);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaDrrData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort, char *pcpData, char *pcpDest /*"BUF1"*/, bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
	CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
	CCS_CATCH_ALL;
	return false;
}

//*************************************************************************************
// Synchronise: sperrt den Zugriff auf die DRRTAB f�r andere Module/Prozesse/
//	Whatevers oder gibt den Zugriff wieder frei, wenn <bpActive> == false.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler
//*************************************************************************************

bool CedaDrrData::Synchronise(CString opUnknown, bool bpActive)
{
	CCS_TRY;
	CTime olCurrentTime = CTime::GetCurrentTime(); 
	CTime olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	olToTime = olCurrentTime + CTimeSpan(0, 0, 2, 0);
	CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
	CString olStrTo = CTimeToDBString(olToTime,olToTime);
	char pclData[1024]="";
	char pclSelection[1024]="";
	if (bpActive){
		sprintf(pclData, "*CMD*,SYN%s,SYNC,%s,N,%s,%s, ", pcgTableExt, pcmTableName, olStrFrom.GetBuffer(0), olStrTo.GetBuffer(0));
		strcpy(pclSelection, "LATE");
		CedaAction("REL",pclSelection,"",pclData);
		// Synchronisieren OK?
		if(CString(pclSelection).Find("NOT OK") != -1)
		{
			// terminieren
			return false;
		}
	}
	else{ 
		// Synchronisation beenden
		sprintf(pclData, "*CMD*,SYN%s,SYNC,%s, ,%s,%s, ", pcgTableExt, pcmTableName, olStrFrom.GetBuffer(0), olStrTo.GetBuffer(0));
		CedaAction("REL","LATE","",pclData);
	}
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// CreateAndAddDrrData: initialisiert eine Struktur vom Typ DRRDATA. Der Datensatz muss
//	durch Aufruf von Insert() oder Release() gespeichert werden. Bei Speichern
//	durch Release() sollte <bpInsertInternal> gesetzt sein, damit der Datensatz
//	in die interne Datenhaltung aufgenommen wird.
// R�ckgabe:	ein Zeiger auf den ge�nderten / erzeugten Datensatz oder
//				NULL, wenn ein Fehler auftrat.
//************************************************************************************************************************************************

DRRDATA* CedaDrrData::CreateDrrData(long lpStfUrno, CString opDay, CString opRosl, CString opRoss /*= ""*/, CString opDrrn /*= "1"*/, bool bpInsertInternal /*= true*/)
{
	CCS_TRY;
	// Objekt erzeugen
	DRRDATA *prlDrrData = new DRRDATA;
	// �nderungsflag setzen
	prlDrrData->IsChanged = DATA_NEW;
	// Erzeugungsdatum einstellen
	prlDrrData->Cdat = COleDateTime::GetCurrentTime();
	ogBasicData.ConvertDateToUtc(prlDrrData->Cdat);
	// Anwender (Ersteller) einstellen
	strcpy(prlDrrData->Usec,pcgUser);
	// Schichttag einstellen
	strcpy(prlDrrData->Sday,opDay.GetBuffer(0));
	// Mitarbeiter-Urno
	prlDrrData->Stfu = lpStfUrno;
	// n�chste freie Datensatz-Urno ermitteln und speichern
	prlDrrData->Urno = ogBasicData.GetNextUrno();
	// Planungsstufe einstellen
	strcpy(prlDrrData->Rosl,opRosl.GetBuffer(0));
	// Pausenlage auf NULL setzen
	prlDrrData->Sbfr.SetStatus(COleDateTime::null);
	// Pausenlage auf NULL setzen
	prlDrrData->Sbto.SetStatus(COleDateTime::null);
	// Planungsstufenstatus einstellen
	if (opRoss != "") strcpy(prlDrrData->Ross,opRoss.GetBuffer(0));
	// DRRN darf nicht gr�sser in CCSGlobal definierten MAX_DRR_DRRN sein, 
	// da das Menue-Mapping in DutyRoster_View::TableRButtonDown() davon
	// abh�ngt
	if (atoi(opDrrn.GetBuffer(0)) > MAX_DRR_DRRN)
	{
		// unzul�ssiger Wert -> aufr�umen und abbrechen
		delete prlDrrData;
		return NULL;
	}
	// DRRN setzen (Nummer des DRR bei mehreren Schichten an einem Tag)
	strcpy(prlDrrData->Drrn,opDrrn.GetBuffer(0));
	// Exportflag einstellen
	strcpy(prlDrrData->Expf," ");
	// Fctc einstellen
	strcpy(prlDrrData->Fctc,"");
	// set Sub1/Sub2
	strcpy(prlDrrData->Drs2,"1");

	// initialize SAP export flag, if opRosl == '1' !
	if (opRosl == "1")
	{
		strcpy(prlDrrData->Prfl,"1");
	}

	// Datensatz in einf�gen, wenn gew�nscht (ohne BC!!!)
	if (bpInsertInternal) InsertInternal(prlDrrData,DRR_NO_SEND_DDX);
	// Zeiger auf Datensatz zur�ck
	return prlDrrData;
	CCS_CATCH_ALL;
	return NULL;
}

/************************************************************************************************************************************************
SetDrrDataScod: stellt die Felder AVTO, AVFR, SBFR, SBTO und SBLU ein. Der
Code <opCode> wird je nach <bpCodeIsBsd> in der Tabelle BSD (Basisschichten) oder in
der Tabelle ODA (Basisabwesenheiten) gesucht.
R�ckgabe:	true	->	Schichtcode ist Abwesenheit oder Basisschicht und wurde
eingetragen
false	->	ung�ltiger Parameter oder Code nicht gefunden
bpCheckMainFuncOnly - true - Schichtzuordnung nur f�r Stammfunktionen pr�fen, sonst f�r alle
- CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
************************************************************************************************************************************************/

bool CedaDrrData::SetDrrDataScod(DRRDATA *popDrrData, bool bpCodeIsBsd, long lpNewShiftUrno,CString opNewFctc, bool bpSetEditFlag, bool bpCheckMainFuncOnly)
{
	CCS_TRY;
	
	// ung�ltiges Objekt oder Schichturno oder Planungstyp? -> terminieren
	if ((popDrrData == NULL) || (lpNewShiftUrno == 0))	return false;	
	
	// Achtung: Wichtig falls ein neuer DRR erzeugt wird, bei vorhandenen DRRs l�uft 
	// die Pr�fung �ber ChangeBsdCode
	// Pr�fen ob die Funktionen zusammenpassen bei BSD oder ob die Abwesenheit zul�ssig ist bei ODA
	if (!CheckFuncOrgContract(popDrrData,lpNewShiftUrno,opNewFctc,bpCheckMainFuncOnly))
		return false;
	
	
	// je nach Schichttyp (Abwesenheit oder Basisschicht)
	if(bpCodeIsBsd)
	{
		
		// Eventuell zugeh�rige Doppeltour Schicht �ndern 
		if (ChangeConnectedScod(popDrrData,lpNewShiftUrno,opNewFctc, bpCheckMainFuncOnly))
		{
			return SetDrrDataScodByBSD(popDrrData,lpNewShiftUrno,opNewFctc,bpSetEditFlag);
		}
		else
			// Problem mit dem �ndern der zugeordneten Schicht.
			return false;
	}
	else
	{		
		// Testen ob Doppeltour vorhanden
		if (HasDoubleTour(popDrrData))
		{
			if (CancelDoubleTour(popDrrData))
			{
				return SetDrrDataScodByODA(popDrrData,lpNewShiftUrno,bpSetEditFlag);
			}
			else
				// Doppeltour soll NICHT getrennt werden
				return false;
		}
		else
			// Keine Doppeltour vorhanden.
			return SetDrrDataScodByODA(popDrrData,lpNewShiftUrno,bpSetEditFlag);
	}
	
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// SetDrrDataScodByODA: stellt die Felder AVTO, AVFR, SBFR, SBTO und SBLU ein. Der
//	Code <opCode> wird in der Tabelle ODA (Basisabwesenheiten) gesucht.
// R�ckgabe:	true	-> ODA-Datensatz gefunden und Info eingetragen
//				false	-> ODA-Datensatz nicht gefunden
//************************************************************************************************************************************************

bool CedaDrrData::SetDrrDataScodByODA(DRRDATA *popDrrData, long lpNewShiftUrno, bool bpSetEditFlag)
{
	CCS_TRY;

	// ung�ltiges Objekt? -> terminieren
	if (popDrrData == NULL)
		return false;

	// ODA-Datensatz suchen
	ODADATA *prlOdaData = ogOdaData.GetOdaByUrno(lpNewShiftUrno);

	// Datensatz gefunden?
	if (prlOdaData == NULL)
		return false;

	// ist dieSchicht REGULAR FREE?
	bool	blWork		= false;
	int		ilOdaFree	= ogOdaData.IsODAAndIsRegularFree(prlOdaData->Sdac, 0, &blWork);

	ODADATA *polOda = ogOdaData.GetOdaByUrno(popDrrData->Bsdu);
	if (polOda == NULL && strlen(popDrrData->Scod))
	{
		strcpy (popDrrData->Scoo, popDrrData->Scod);
	}

	// Schicht von .. bis.. Pause von .. bis.. ermitteln
	COleDateTime olTime;
	if (!SdayToOleDateTime(popDrrData, olTime))
		return false;

	// Felder setzen
	// SDAY konvertieren
	COleDateTime olSday;
	if (!SdayToOleDateTime(popDrrData, olSday))
		return false;	// bei Fehler abbrechen

	// wenn in ODA g�ltige Anfangs- und Endzeiten sind, kopieren wir diese Zeiten in DRR.
	COleDateTimeSpan olAbfr, olAbto;
	if (CedaDataHelper::HourMinStringToOleDateTimeSpan(prlOdaData->Abfr,olAbfr) &&
		CedaDataHelper::HourMinStringToOleDateTimeSpan(prlOdaData->Abto,olAbto))
	{
		popDrrData->Avfr = olSday + olAbfr;
		popDrrData->Avto = olSday + olAbto;

		if (popDrrData->Avto <= popDrrData->Avfr)
		{
			// ja -> einen Tag zus�tzlich addieren
			popDrrData->Avto += COleDateTimeSpan(1,0,0,0);
		}
		strcpy(popDrrData->Sblu, prlOdaData->Blen);
	}
	else
	{
		bool blGenerateTime = false;
		if (strcmp(prlOdaData->Tbsd, "1") == 0)
		{
			if (popDrrData->Avfr.GetStatus() != COleDateTime::valid || 
				popDrrData->Avto.GetStatus() != COleDateTime::valid)
			{
				blGenerateTime = true;
			}
		}
		else
		{
			blGenerateTime = true;
		}

		if (blGenerateTime)
		{
			// wenn in DRR KEINE g�ltige Anfangs- und Endzeiten sind oder keine Zeiten in ODATAB sind,
			// generieren wir die Zeiten als 1/5 der Wochenzeit
			// abh�ngig vom Vertrag des MAs, mit Anfangszeit 8:30.
			{
				popDrrData->Avfr = olSday + COleDateTimeSpan(0,8,30,0);
				CAccounts olAccount(&ogDrrData,&ogAccData,&ogStfData,&ogBsdData,&ogOdaData,&ogScoData,&ogCotData);
				double dCommonDayWorkingTime = olAccount.GetCommonDayWorkingTime(popDrrData);
				dCommonDayWorkingTime += 0.5;	// runden
				popDrrData->Avto = popDrrData->Avfr + COleDateTimeSpan(0,0,(int)dCommonDayWorkingTime,0);
				// Pausenl�nge 0
				strcpy(popDrrData->Sblu,"0000");
				popDrrData->Sbfr.SetTime(0,0,0);
				popDrrData->Sbto.SetTime(0,0,0);
			}
		}
	}

	// check if we must initialize the send to SAP export flag
	if (strcmp(popDrrData->Rosl,"1") == 0 && strcmp(popDrrData->Scod,prlOdaData->Sdac) != 0)
	{
		strcpy(popDrrData->Prfl,"1");
	}

	// Schichtcode speichern
	strcpy(popDrrData->Scoo,popDrrData->Scod);
	strcpy(popDrrData->Scod,prlOdaData->Sdac);

	// Schichturno speichern
	popDrrData->Bsdu = lpNewShiftUrno;

	// Drs-Felder l�schen
	popDrrData->Drs1[0] = 0;
	popDrrData->Drs3[0] = 0;
	popDrrData->Drs4[0] = 0;

	// Funktion l�schen
	strcpy (popDrrData->Fctc, " ");

	// Info und Flag f�r letzte �nderung setzen
	if (bpSetEditFlag)
	{
		SetDrrDataLastUpdate(popDrrData);
	}

	// R�ckgabe OK
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// SetDrrDataScodByODA: stellt die Felder AVTO, AVFR, SBFR, SBTO und SBLU ein. Der
//	aktive DRR und der Urlaubs-DRR des MAs <lpStfUrno> wird ermittelt. Wenn 
//	<bpDoUpdate> = false ist, wird der DRR nicht gesichert. Die aufrufende
//	Funktion muss in diesem Fall explizit sichern (z.B. durch Release).
// R�ckgabe:	der ge�nderte/erzeugte DRR
//************************************************************************************************************************************************

bool CedaDrrData::SetDrrDataScodByODA(ODADATA *popOda, long lpStfUrno, CString opSday, CString opPType, bool bpDoChangeActive, CString opDrrn /*="1"*/, bool bpDoUpdate /*=true*/, ODADATA *popExcludeODA /*=NULL*/, bool bpDoConflictCheck /*= true*/)
{
	CCS_TRY;
	// g�ltiger ODA-Datensatz?
	if (popOda == NULL) return false; // nein -> abbrechen
	
	// Planungsstatus und -stufe
	CString olRoss = "A", olRosl = opPType;
	
	// zu �ndernder oder neuer DRR
	DRRDATA *prlDrr = NULL;
	
	// aktiven oder Urlaubs-DRR aktualisieren?
	if (bpDoChangeActive)
	{
		// aktiven DRR �ndern
		prlDrr = GetDrrByRoss(opSday,lpStfUrno,opDrrn,olRosl);
		if (prlDrr == NULL)
			prlDrr = GetActiveDrr (opSday, lpStfUrno);
	}
	else
	{
		// Urlaubs-DRR �ndern -> nur �ndern, wenn die einzuf�gende Schicht das
		// 'regul�r arbeitsfrei' Flag hat
		//		if(!blIsRegFree) return false;
		// Urlaubs-DRR �ndern
		olRosl = "U";
		olRoss = "";
		prlDrr = GetDrrByKey(opSday,lpStfUrno,opDrrn,olRosl);
	}

	// DRR neu erzeugen, wenn nicht vorhanden
	bool blIsNew = false;
	if(prlDrr == NULL) 
	{
		// DRR existiert nicht -> neuen anlegen (bei Misserfolg terminieren)
		if ((prlDrr = CreateDrrData(lpStfUrno,opSday,olRosl/*Dienstplanstufe*/,olRoss/*Status*/,opDrrn,!bpDoUpdate)) == NULL) return false;
		// Datensatz wurde neu erzeugt
		blIsNew = true;
	}
	else if (popExcludeODA != NULL)
	{
		// DRR existiert -> Schichtcode �berpr�fen
		if (CString(prlDrr->Scod) == CString(popExcludeODA->Sdac))
			return false;
	}

	// wir erstellen eine Kopie des aktuellen pomDrr, um im Fehlerfall alles wiederherzustellen
	DRRDATA olDrrSave;
	ogDrrData.CopyDrr(&olDrrSave, prlDrr);
	
	if(!SetDrrDataScodByODA(prlDrr, popOda->Urno, false)) return false;

	// ShiftCheck durchf�hren
	int ilDDXType;
	if (blIsNew == true)
		ilDDXType = DRR_NEW;
	else
		ilDDXType = DRR_CHANGE;

	if (!ogShiftCheck.DutyRosterCheckIt(prlDrr, bpDoConflictCheck, ilDDXType, false))
	{
		ogDrrData.CopyDrr(prlDrr, &olDrrSave);
		ogShiftCheck.UpdateData(prlDrr, DRR_CHANGE);
		return false;
	}

	// Info und Flag f�r letzte �nderung setzen
	SetDrrDataLastUpdate(prlDrr);

	// Datensatz speichern und BC senden, falls gew�nscht
	if (bpDoUpdate)
	{
		// neuer Datensatz ?
		if (blIsNew)
		{
			// ja -> einf�gen
			if (!Insert(prlDrr))
			{
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_INSERTERR), imLastReturnCode, omLastErrorMessage);
				::MessageBox(NULL, olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				delete prlDrr;
				return false;
			}
		}
		else
		{
			// nein -> aktualisieren
			if (!Update(prlDrr))
			{
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_INSERTERR), imLastReturnCode, omLastErrorMessage);
				::MessageBox(NULL, olErrorTxt, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
				delete prlDrr;
				return false;
			}
		}
	}
	// erzeugten / ge�nderten Datensatz zur�ckgeben
	return true;		
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// SdayToOleDateTime: erzeugt aus dem Feld SDAY ein COleDateTime-Obkjekt.
// R�ckgabe:	true	-> SDAY g�ltig, <opTime> initialisiert
//				false	-> Fehler
//************************************************************************************************************************************************

bool CedaDrrData::SdayToOleDateTime(DRRDATA *prpDrr, COleDateTime &opTime)
{
	CCS_TRY;
	// DRR pr�fen
	if (prpDrr == NULL)
	{
		//ung�ltiger DS -> terminieren
		return false;
	}
	
	// SDAY lesen
	CString olSday(prpDrr->Sday);
	
	// konvertieren
	return CedaDataHelper::DateStringToOleDateTime(olSday,opTime);
	CCS_CATCH_ALL;
	return false;
}

/************************************************************************************************************************************************
// SetDrrDataScodByBSD: stellt die Felder AVTO, AVFR, SBFR, SBTO, SCOO und SBLU ein. Die
//	Urno <lpNewShiftUrno> wird in der Tabelle BSD (Basisschichten) gesucht.
// R�ckgabe:	true	-> BSD-Datensatz gefunden und Info eingetragen
//				false	-> BSD-Datensatz nicht gefunden
- CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
************************************************************************************************************************************************/

bool CedaDrrData::SetDrrDataScodByBSD(DRRDATA *popDrrData, long lpNewShiftUrno, CString opNewFctc, bool bpSetEditFlag)
{
	CCS_TRY;
	// ung�ltiges Objekt? -> terminieren
	if (popDrrData == NULL)	return false;	
	
	// ge�ndert von ADO 27.1.00
	// Nur Anwesenheitschichten sind erlaubt.
	// BSD-Datensatz suchen
	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(lpNewShiftUrno);
	
	// Datensatz gefunden?
	if (polBsd == NULL)	
	{
		// nein -> Fehler, terminieren
		return false;
	}
	
	// check if we must initialize the send to SAP export flag
	if (strcmp(popDrrData->Rosl,"1") == 0 && strcmp(popDrrData->Scod,polBsd->Bsdc) != 0)
	{
		strcpy(popDrrData->Prfl,"1");
	}

	//---------------------------------------------------------------------------------------------------------------------
	// set the shift code(s)
	//---------------------------------------------------------------------------------------------------------------------
	strcpy(popDrrData->Scoo,popDrrData->Scod); //copy old shift code into  DRRTAB.SCOO
	strcpy(popDrrData->Scod,polBsd->Bsdc);	   //copy new shift code into  DRRTAB.SCOD
	
	//---------------------------------------------------------------------------------------------------------------------
	// Fctc einstellen
	//---------------------------------------------------------------------------------------------------------------------
	if(opNewFctc.IsEmpty())
	{
		strcpy(popDrrData->Fctc,polBsd->Fctc);
	}
	else
	{
		strcpy(popDrrData->Fctc,opNewFctc);
	}
	//---------------------------------------------------------------------------------------------------------------------
	// Urno einstellen
	//---------------------------------------------------------------------------------------------------------------------
	popDrrData->Bsdu = polBsd->Urno;
	//---------------------------------------------------------------------------------------------------------------------
	// AVFR einstellen aus DRR->SDAY (g�ltig am) und BSD->ESBG (fr�hester Schichtbeginn)
	//---------------------------------------------------------------------------------------------------------------------
	// SDAY konvertieren
	COleDateTime olSday;
	if (!SdayToOleDateTime(popDrrData,olSday)) return false;	// bei Fehler abbrechen
	// ESBG umwandeln in Zeit
	COleDateTimeSpan olEsbgSpan;
	if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Esbg,olEsbgSpan)) return false;	// bei Fehler abbrechen
	// Anwesend von einstellen: SDAY + ESBG
	popDrrData->Avfr = olSday + olEsbgSpan;
	//	TRACE("*** Schichtbeginn: %s ***\n",popDrrData->Avfr.Format("%d.%m.%Y, %H:%M:%S"));
	
	//---------------------------------------------------------------------------------------------------------------------
	// AVTO einstellen aus DRR->und BSD->LSEN (sp�testes Schichtende)
	//---------------------------------------------------------------------------------------------------------------------
	// LSEN umwandeln in Zeit
	COleDateTimeSpan olLsenSpan;
	if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Lsen,olLsenSpan)) return false;	// bei Fehler abbrechen
	// Sp�testes Schichtende fr�her als fr�hester Schichtbeginn?
	if (olLsenSpan <= olEsbgSpan)
	{
		// ja -> einen Tag zus�tzlich addieren
		olLsenSpan += COleDateTimeSpan(1,0,0,0);
	}
	// Anwesend bis einstellen: SDAY + LSEN
	popDrrData->Avto = olSday + olLsenSpan;
	
	//---------------------------------------------------------------------------------------------------------------------
	// SBFR einstellen aus DRR->SDAY (g�ltig am) und BSD->BKF1 (Pausenbeginn)
	//---------------------------------------------------------------------------------------------------------------------
	// BKF1 umwandeln in Zeit
	COleDateTimeSpan olBkf1Span;
	if(ogBsdData.GetBkf1Time(polBsd, olBkf1Span))
	{
		// Pause von einstellen: SDAY + BKF1
		popDrrData->Sbfr = olSday + olBkf1Span;
	}
	else 
		popDrrData->Sbfr.SetStatus(COleDateTime::null);
	
	//---------------------------------------------------------------------------------------------------------------------
	// SBTO einstellen aus DRR->und BSD->BKT1 (Pausenende)
	//---------------------------------------------------------------------------------------------------------------------
	// BKT1 umwandeln in Zeit
	COleDateTimeSpan olBkt1Span;
	if(ogBsdData.GetBkt1Time(polBsd, olBkt1Span))
	{
		// Pause bis einstellen: SDAY + BKT1
		popDrrData->Sbto = olSday + olBkt1Span;
	}
	else 
		popDrrData->Sbto.SetStatus(COleDateTime::null);
	
	//---------------------------------------------------------------------------------------------------------------------
	// Pausenl�nge einstellen
	//---------------------------------------------------------------------------------------------------------------------
	strcpy(popDrrData->Sblu,polBsd->Bkd1);

	if(popDrrData->Avto.GetStatus() != COleDateTime::valid)
	{
		AfxMessageBox("popDrrData->Avto.GetStatus() != COleDateTime::valid");
	}
	
	// Info und Flag f�r letzte �nderung setzen
	if (bpSetEditFlag) SetDrrDataLastUpdate(popDrrData);
	// R�ckgabe OK
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************************************************
// SetDrrDataLastUpdate: stellt die Werte f�r die letzte �nderung ein. Das 
//	'Datensatz ge�ndert' Flag wird gesetzt, wenn n�tig.
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaDrrData::SetDrrDataLastUpdate(DRRDATA *popDrrData)
{
	CCS_TRY;
	// ung�ltiges Objekt? -> terminieren
	if (popDrrData == NULL)
		return;

	if (popDrrData->IsChanged != DATA_NEW && popDrrData->IsChanged != DATA_DELETED)
	{
		// Anwender-Name und Zeit der letzten �nderung speichern
		strcpy(popDrrData->Useu,pcgUser);
		popDrrData->Lstu = COleDateTime::GetCurrentTime();
		ogBasicData.ConvertDateToUtc(popDrrData->Lstu);

		// �nderungsflag setzen
		popDrrData->IsChanged = DATA_CHANGED;
	}

	CCS_CATCH_ALL;
}

//************************************************************************************************************************************************
// GetNextRosl : Gibt die n�chste g�ltige Planungsstufe zur�ck.
//************************************************************************************************************************************************

CString CedaDrrData::GetNextRosl(CString opOldRosl)
{
	CCS_TRY;
	CString olNewRosl;
	olNewRosl.Empty();
	
	if (opOldRosl.IsEmpty())
		return olNewRosl;
	
	// Pr�fen ob die Langzeitstufe angezeigt werden soll
	
	// Werte aus der Parameterklasse einlesen
	CString NextAfterLevel1;
	CString olStepList= ogCCSParam.GetParamValue(ogAppl,"ID_SHOW_STEPS");
	if (olStepList.Find("L") == -1)
		NextAfterLevel1 = "2";
	else
		NextAfterLevel1 = "L";
	
	// die Rosls sind nach H�ufigkeitsprinzip sortiert
	if		(opOldRosl == "2" )	olNewRosl = "3";
	else if (opOldRosl == "3" )	olNewRosl = "4";
	else if (opOldRosl == "4" )	olNewRosl = "5";
	else if	(opOldRosl == "1" )	olNewRosl = NextAfterLevel1;
	else if (opOldRosl == "L" )	olNewRosl = "2";
	else if (opOldRosl == "LU")	olNewRosl = "2";
	else if (opOldRosl == "U" )	olNewRosl = "2";
	else						olNewRosl = "";	// Ende
	
	return olNewRosl;
	CCS_CATCH_ALL;
	return CString("");
}

//************************************************************************************************************************************************
// CopyDrrValues : Kopiert die "Nutzdaten" (keine urnos usw.)
//************************************************************************************************************************************************

void CedaDrrData::CopyDrrValues(DRRDATA* popDrrDataSource,DRRDATA* popDrrDataTarget,bool bpOverwrite /* true*/)
{
	CCS_TRY;
	
	// Wenn �berschreiben erlaubt ist einfach kopieren. oder wenn das Ziel leer ist
	if (bpOverwrite || strcmp(popDrrDataTarget->Scod,"") == 0)
	{
		strcpy(popDrrDataTarget->Scod,popDrrDataSource->Scod);
		strcpy(popDrrDataTarget->Scoo,popDrrDataSource->Scoo);
		popDrrDataTarget->Bsdu = popDrrDataSource->Bsdu;
		// Daten kopieren
		popDrrDataTarget->Avfr = popDrrDataSource->Avfr;
		popDrrDataTarget->Avto = popDrrDataSource->Avto;
		strcpy(popDrrDataTarget->Bufu,popDrrDataSource->Bufu);
		strcpy(popDrrDataTarget->Drsf,popDrrDataSource->Drsf);
		strcpy(popDrrDataTarget->Expf,popDrrDataSource->Expf);
		strcpy(popDrrDataTarget->Rema,popDrrDataSource->Rema);
		popDrrDataTarget->Sbfr = popDrrDataSource->Sbfr;
		strcpy(popDrrDataTarget->Sblp,popDrrDataSource->Sblp);
		strcpy(popDrrDataTarget->Sblu,popDrrDataSource->Sblu);
		popDrrDataTarget->Sbto = popDrrDataSource->Sbto;
		strcpy(popDrrDataTarget->Drs1,popDrrDataSource->Drs1);
		strcpy(popDrrDataTarget->Drs2,popDrrDataSource->Drs2);
		strcpy(popDrrDataTarget->Drs3,popDrrDataSource->Drs3);
		strcpy(popDrrDataTarget->Drs4,popDrrDataSource->Drs4);
		strcpy(popDrrDataTarget->Fctc,popDrrDataSource->Fctc);
		// User und Zeit setzen
		SetDrrDataLastUpdate(popDrrDataTarget);
	}
	
	CCS_CATCH_ALL;
}

//************************************************************************************************************************************************
// GetTypeName: Gibt den Namen der Stufe als String zur�ck
//************************************************************************************************************************************************

CString CedaDrrData::GetTypeName(CString opType)
{
	CCS_TRY;
	CString olName;
	
	if(opType == "1")
	{
		olName = LoadStg(IDS_STRING1586);
	}
	else if(opType == "2")
	{
		olName = LoadStg(IDS_STRING1587);
	}
	else if(opType == "3")
	{
		olName = LoadStg(IDS_STRING1588);
	}
	else if(opType == "4")
	{
		olName = LoadStg(IDS_STRING1589);
	}
	else if(opType == "LU")
	{
		olName = LoadStg(IDS_STRING1876);
	}

	return olName;
	CCS_CATCH_ALL;
	return CString("");
}

//************************************************************************************************************************************************
// L�scht alle zum DRR geh�renden DRD�s
//************************************************************************************************************************************************

void CedaDrrData::DeleteAllDrdByDrr(DRRDATA* popDrr)
{
	CCS_TRY;
	if (popDrr == NULL)
		return;
	
	// Map mit allen relevanten DRDs
	CMapPtrToPtr	olDrdMap;
	POSITION		rlPos; 
	long			llUrno;
	DRDDATA*		prlDrd; 
	
	// alle relevanten DRDs ermitteln
	if (ogDrdData.GetDrdMapOfDrr(&olDrdMap,popDrr) == -1)
	{
		// Fehler -> terminieren
		return;
	}
	
	for(rlPos = olDrdMap.GetStartPosition(); rlPos != NULL; )
	{
		// DRD ermitteln
		olDrdMap.GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDrd);
		//DRD l�schen.
		if (!ogDrdData.Delete(prlDrd,true))
		{
			// Fehler ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrdData.imLastReturnCode, ogDrdData.omLastErrorMessage);
			AfxMessageBox(olErrorTxt,MB_ICONEXCLAMATION);
		}
	}
	CCS_CATCH_ALL;
}

//************************************************************************************************************************************************
// DeleteAllDraByDrr: L�scht alle zum DRR geh�renden DRA�s
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaDrrData::DeleteAllDraByDrr(DRRDATA* popDrr)
{
	CCS_TRY;
	if (popDrr == NULL)
		return;
	
	// Map mit allen relevanten DRAs
	CMapPtrToPtr	olDraMap;
	POSITION		rlPos; 
	long			llUrno;
	DRADATA*		prlDra; 
	
	// alle relevanten DRDs ermitteln
	if (ogDraData.GetDraMapOfDrr(&olDraMap,popDrr) == -1)
	{
		// Fehler -> terminieren
		return;
	}
	
	for(rlPos = olDraMap.GetStartPosition(); rlPos != NULL; )
	{
		// DRD ermitteln
		olDraMap.GetNextAssoc(rlPos,(void *&)llUrno, (void *&)prlDra);
		//DRD l�schen.
		// TODO: Fehlermeldung einbauen
		if (!ogDraData.Delete(prlDra,true))
		{
			// Fehler ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDraData.imLastReturnCode, ogDraData.omLastErrorMessage);
			AfxMessageBox(olErrorTxt, MB_ICONEXCLAMATION);
		}
	}
	CCS_CATCH_ALL;
}

//**********************************************************************************
// CheckBsd: pr�ft, ob die Basisschicht mit einer der Schichten in Konflikt
// steht, die den DRRs in <popDrrList> zugeordnet sind. Das Referenzdatum
// <opSday> muss mitgeliefert werden, falls keine DRRs im Array enthalten 
// sind.
// R�ckgabe:	true	-> Schicht ist OK
//				false	-> Schicht nicht OK
//**********************************************************************************

bool CedaDrrData::CheckBsd(BSDDATA *popBsd, CCSPtrArray<DRRDATA> &popDrrList, CString opOrgList, CString opSday, bool bpCheckOrg)
{
	CCS_TRY;
	// pr�fen, ob der untersuchte Tag im G�ltigkeitszeitraum der Schicht liegt
	COleDateTime olSday, olEsbg, olLsen;
	CedaDataHelper::DateStringToOleDateTime(opSday,olSday);
	if (((popBsd->Vafr.GetStatus() == COleDateTime::valid) && (olSday < popBsd->Vafr)) ||
		((popBsd->Vato.GetStatus() == COleDateTime::valid) && (olSday > popBsd->Vato)))
		return false;
	
	if (bpCheckOrg == true)
	{
		// pr�fen, ob der Organisationscode der Schicht im String <opOrgList> enthalten ist (kommt aus der Ansicht)
		if (!opOrgList.IsEmpty())
		{
			// opOrgList hat die Form: "'ABC','DDE','DDS',..."
			// in BSDTAB.DPT1 steht die OrgUnit der Basisschicht drin
			if(lstrlen(popBsd->Dpt1) == 0)
			{
				if (opOrgList.Find("NO_ORGCODE") == -1)
					return false;
			}
			else
			{
				CString olOrgCodeWithMark;
				olOrgCodeWithMark.Format("'%s'", LPCTSTR(popBsd->Dpt1));
				if (opOrgList.Find(olOrgCodeWithMark) == -1)
					return false;
			}
		}
		else
		{
			return true;
		}
	}

	// Alle bereits vorhandenen Schichten durchgehen und
	// pr�fen, ob es zeitliche �berschneidungen gibt. Der zu pr�fende
	// Zeitraum wird aus dem Referenzdatum und den Uhrzeiten der G�ltigkeit
	// der Basisschicht konstruiert.
	CedaDataHelper::DateTimeStringToOleDateTime(opSday+CString(popBsd->Esbg),olEsbg);
	CedaDataHelper::DateTimeStringToOleDateTime(opSday+CString(popBsd->Lsen),olLsen);
	// Enddatum um einen Tag korrigieren, falls n�tig (Start > Ende, z.B. 23:00h Start - 01:00h Ende)
	if (olLsen < olEsbg) 
		olLsen += COleDateTimeSpan(1,0,0,0);
	for (int ilCount=0; ilCount<popDrrList.GetSize(); ilCount++)
	{
		if (popDrrList[ilCount].Avfr.GetStatus() != COleDateTime::valid)
			return false;
		
		if (popDrrList[ilCount].Avto.GetStatus() != COleDateTime::valid)
			return false;
		
		// Zeit pr�fen
		if (!(((olEsbg < popDrrList[ilCount].Avfr) && 
			(olLsen <= popDrrList[ilCount].Avfr)) || 
			((olEsbg >= popDrrList[ilCount].Avto) && 
			(olLsen > popDrrList[ilCount].Avto))))
			return false;
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************
// Pr�ft ob dem �begebenen DRR DRAs zugeordnet sind
//************************************************************************************

bool CedaDrrData::HasDraData(DRRDATA* popDrr)
{
	CMapPtrToPtr	olDraMap;
	//----------------------------------------------------------------------------
	// alle relevanten DRDs ermitteln
	if (ogDraData.GetDraMapOfDrr(&olDraMap,popDrr) == -1)
	{
		// Fehler -> terminieren
		return (false);
	}
	
	if (olDraMap.IsEmpty())
		return false;
	else
		return true;
}

//************************************************************************************
// Pr�ft ob dem �begebenen DRR DRDs zugeordnet sind
//************************************************************************************

bool CedaDrrData::HasDrdData(DRRDATA* popDrr)
{
	CMapPtrToPtr	olDrdMap;
	//----------------------------------------------------------------------------
	// alle relevanten DRDs ermitteln
	if (ogDrdData.GetDrdMapOfDrr(&olDrdMap,popDrr) == -1)
	{
		// Fehler -> terminieren
		return (false);
	}
	
	if (olDrdMap.IsEmpty())
		return false;
	else
		return true;
}

/**************************************************************************************
f�r BSD wird gepr�ft ob die Funktionen von Schicht und Mitarbeiter zusammenpassen
f�r ODA - ob diese Abwesenheit f�r den Vertragscode des Mitarbeiters zul�ssig ist
bpCheckMainFuncOnly - true - Schichtzuordnung nur f�r Stammfunktionen pr�fen, sonst f�r alle
- CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
**************************************************************************************/
bool CedaDrrData::CheckFuncOrgContract(DRRDATA* popDrr,long lpNewShiftUrno, CString opNewFctc, bool bpCheckMainFuncOnly)
{
	// Werte aus der Parameterklasse einlesen
	COleDateTime olOleDate;
	CedaDataHelper::DateStringToOleDateTime(popDrr->Sday,olOleDate);

	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(lpNewShiftUrno);
	if (polBsd != NULL)
	{
		CString olTestFunction = ogCCSParam.GetParamValue(ogAppl,"ID_TEST_FUNCTION");
		if(olTestFunction == "Y")
		{
			if(opNewFctc.IsEmpty())
			{
				// nur wenn kein Funktionscode mitgegeben ist, setzen wir einen BSD-Code ein
				opNewFctc = polBsd->Fctc;
			}
			// Bei keinem Code ist es immer OK !
			if (!opNewFctc.IsEmpty())
			{
				// Codes vergleichen
				if (!ogSpfData.ComparePfcBySurnWithTime(opNewFctc,popDrr->Stfu,olOleDate,bpCheckMainFuncOnly))
				{
					// Funktionen sind nicht gleich: keine der Funktionen des Mitarbeiters stimmt mit opNewFctc �berein
					CString olName;
					CString olTime;
					STFDATA *prlStf;

					prlStf = ogStfData.GetStfByUrno(popDrr->Stfu);

					if (prlStf > NULL)
					{
						CString olVName(prlStf->Finm);
						olName.Format("%s.%s",olVName.Left(1),prlStf->Lanm);
					}
					olTime = olOleDate.Format("%d.%m.%y");
					
					CCSPtrArray<SPFDATA> olSpfData;
					int ilFuncNum = ogSpfData.GetSpfArrayBySurnWithTime(olSpfData,popDrr->Stfu,olOleDate,bpCheckMainFuncOnly);
					CString olFunctions = ogSpfData.FormatFuncs(olSpfData,1);
					
					CString olMessage;
					
					if(!ilFuncNum)
					{
						// Abfrage ob gew�nscht
						// %s  hat am  %s  keine regul�re Funktion.\n
						// Die Schicht  %s  hat die Funktion  %s\n
						// \n
						// Soll die Schicht dennoch zugewiesen werden?*INTXT*
						olMessage.Format(LoadStg(IDS_STRING1657),olName,olTime,polBsd->Bsdc,opNewFctc);
						if (AfxMessageBox(olMessage,MB_ICONQUESTION | MB_YESNO) != IDYES)
							// -> Nein, keine �nderung durchf�hren
							return false;
					}
					else if(ilFuncNum == 1)
					{
						// Abfrage ob gew�nscht
						// %s  hat am  %s  regul�re Funktion  %s.\n
						// Die Schicht  %s  hat die Funktion  %s\n
						// \n
						// Soll die Schicht dennoch zugewiesen werden?*INTXT*
						olMessage.Format(LoadStg(IDS_STRING1655),olName,olTime,olFunctions,polBsd->Bsdc,opNewFctc);
						if (AfxMessageBox(olMessage,MB_ICONQUESTION | MB_YESNO) != IDYES)
							// -> Nein, keine �nderung durchf�hren
							return false;
					}
					else
					{
						// Abfrage ob gew�nscht
						// %s  hat am  %s  regul�re Funktionen  %s.\n
						// Die Schicht  %s  hat die Funktion  %s\n
						// \n
						// Soll die Schicht dennoch zugewiesen werden?*INTXT*
						olMessage.Format(LoadStg(IDS_STRING1660),olName,olTime,olFunctions,polBsd->Bsdc,opNewFctc);
						if (AfxMessageBox(olMessage,MB_ICONQUESTION | MB_YESNO) != IDYES)
							// -> Nein, keine �nderung durchf�hren
							return false;
					}
				}
			}
		}
	}
	else
	{
		// Soll die Abwesenheit nach dem Vertragscode kontrolliert werden? Y/N
		CString olTestFunction = ogCCSParam.GetParamValue(ogAppl,"ID_TEST_ABSENCE");
		if(olTestFunction == "Y")
		{
			// ODA?
			ODADATA* polOda = ogOdaData.GetOdaByUrno(lpNewShiftUrno);
			if(polOda != 0)
			{
				// wir pr�fen, ob dieser MA diese Abwesenheit bekommen darf
				
				// Vertragsliste SCODATA holen
				CCSPtrArray<SCODATA> olScoData;
				ogScoData.GetScoListBySurnWithTime(popDrr->Stfu, olOleDate, &olScoData);
				
				bool blEnabled = false;
				OACDATA* polOac;
				SCODATA* polSco = 0;
				CString olCode;
				CString olSdac(polOda->Sdac);
				for(int index=0; index<olScoData.GetSize(); index++)
				{
					// alle Vertr�ge durchgehen
					polSco = &olScoData[index];
					if(!polSco) continue;
					
					olCode = polSco->Code;
					polOac = ogOacData.GetOacByKey(olCode, olSdac);
					if(polOac != 0)
					{
						// gefunden, weiter brauchen wir nicht zu suchen
						blEnabled = true;
						break;
					}
				}
				
				if(blEnabled == false)
				{
					// kein OAC gefunden oder der OAC-Code stimmt mit der neuen Abwesenheit nicht �berein
					// wir m�ssen den Benutzer informieren
					CString olName;
					CString olTimeString;
					STFDATA *prlStf;
					
					prlStf = ogStfData.GetStfByUrno(popDrr->Stfu);
					
					if (prlStf > NULL)
					{
						CString olVName(prlStf->Finm);
						olName.Format("%s.%s",olVName.Left(1),prlStf->Lanm);
					}
					olTimeString = olOleDate.Format("%d.%m.%y");
					
					CString olMessage;
					
					// %s am %s:\n\n
					// Die Abwesenheit %s ist nach dem aktuellen Vertragscode %s nicht zul�ssig!*INTXT*
					olMessage.Format(LoadStg(IDS_STRING1658),olName,olTimeString,polOda->Sdac,(polSco == 0 ? "" : polSco->Code));
					AfxMessageBox(olMessage,MB_ICONSTOP | MB_OK);
					return false;
				}
			}
		}
	}
	return true;
}

/************************************************************************************
// CheckBSDCode: Pr�fft einen Schichtcode, L�scht nach R�ckfrage DRA und DRD 
//	Datens�tze. Pr�ft ob Doppeltouren vorliegen.
// R�ckgabe:	die alte, �berschriebene Schichturno
- CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
************************************************************************************/

long CedaDrrData::CheckBSDCode(DRRDATA* popDrr,long lpNewShiftUrno,long lpOldShiftUrno) 
{
	CCS_TRY;
	if (popDrr == NULL)
		return lpOldShiftUrno;
	
	// Das Ein-/Austrittsdatum pr�fen
	if(!IsNewShiftWithinEmployerWorkTime(popDrr->Stfu, lpNewShiftUrno, popDrr)) 
		return lpOldShiftUrno;
	
	// Wenn beide Codes g�ltige ODA-Codes sind, brauchen wir keine weitere Pr�fung
	if(ogOdaData.GetOdaByUrno(lpNewShiftUrno) != NULL && ogOdaData.GetOdaByUrno(lpOldShiftUrno) != NULL)
		return lpNewShiftUrno;
	
	// Keine g�ltige neue Urno oder keine �nderung des Codes
	if (lpNewShiftUrno == 0 || lpNewShiftUrno == lpOldShiftUrno)
		return lpOldShiftUrno;
	
	//----------------------------------------------------------------------------
	//Pr�fen ob ein g�ltiger Schicht-Code oder Abwesenheits-Code eingegeben wurde
	if(ogBsdData.GetBsdByUrno(lpNewShiftUrno) == NULL  && ogOdaData.GetOdaByUrno(lpNewShiftUrno) == NULL)
	{
		// "Schichtcode ung�ltig.\n*INTXT*"
		AfxMessageBox(LoadStg(IDS_STRING1870),MB_ICONWARNING);
		// -> Fehler, alten Code wiederherstellen
		return lpOldShiftUrno;
	}
	
	//----------------------------------------------------------------------------
	// Pr�fen ob Funktionen vom Mitarbeiter und Schicht passen
	if(!DeleteDRA_DRDbyChangeBsd(popDrr))
	{
		return lpOldShiftUrno;
	}
	else
	{
		// -> Alles ok
		return lpNewShiftUrno;
	}
	
	CCS_CATCH_ALL;
	return 0;
}

/***********************************************************************************************
pr�ft, ob das Datum innerhalb der Ein/Austrittszeit des MAs liegt und fragt den Benutzer bei Bedarf
R�ckgabe: true, wenn ja oder wenn nein, aber Benutzer hat die Frage bejaat, false nein oder Fehler
***********************************************************************************************/
bool CedaDrrData::IsNewShiftWithinEmployerWorkTime(long lpStfu, long lpNewShiftUrno, DRRDATA* popDrr)
{
	
	// Das Ein-/Austrittsdatum pr�fen
	STFDATA* polStf = ogStfData.GetStfByUrno(lpStfu);
	if(!polStf) return false;
	
	COleDateTime olShiftTimeFrom, olShiftTimeTo, olSdayTime;
	
	// SDAY konvertieren
	COleDateTime olSday;
	if(!CedaDataHelper::DateStringToOleDateTime(CString(popDrr->Sday),olSdayTime)) return false;
	
	BSDDATA* polBsd = ogBsdData.GetBsdByUrno(lpNewShiftUrno);
	if(polBsd != 0)
	{
		
		// ESBG umwandeln in Zeit
		COleDateTimeSpan olEsbgSpan;
		if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Esbg,olEsbgSpan)) return false;	// bei Fehler abbrechen
		
		olShiftTimeFrom = olSdayTime + olEsbgSpan;
		
		// LSEN umwandeln in Zeit
		COleDateTimeSpan olLsenSpan;
		if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Lsen,olLsenSpan)) return false;	// bei Fehler abbrechen
		// Sp�testes Schichtende fr�her als fr�hester Schichtbeginn?
		if (olLsenSpan <= olEsbgSpan)
		{
			// ja -> einen Tag zus�tzlich addieren
			olLsenSpan += COleDateTimeSpan(1,0,0,0);
		}
		// Anwesend bis einstellen: SDAY + LSEN
		olShiftTimeTo = olSdayTime + olLsenSpan;
	}
	else
	{
		if(popDrr->Avfr.GetStatus() == COleDateTime::valid)
		{
			olShiftTimeFrom = popDrr->Avfr;
		}
		else
		{
			olShiftTimeFrom = olSdayTime;
		}
		
		if(popDrr->Avto.GetStatus() == COleDateTime::valid)
		{
			olShiftTimeTo = popDrr->Avto;
		}
		else
		{
			olShiftTimeTo = olSdayTime;
		}
	}
	
	bool blBefor = 
		polStf->Doem.GetStatus() == COleDateTime::valid && 
		GetAbsDay(olShiftTimeFrom) < GetAbsDay(polStf->Doem);
	bool blAfter = 
		polStf->Dodm.GetStatus() == COleDateTime::valid && 
		GetAbsDay(olShiftTimeFrom) > GetAbsDay(polStf->Dodm);
	
	if(	 blBefor || blAfter )
	{
		CString olName = ogStfData.GetName(polStf->Urno);
		CString olMsg;
		
		if(blBefor)
		{
			// Drr-Datum ist vor der Eintrittszeit, mu� Benutzer gefragt werden
			CString olShiftTime = olShiftTimeFrom.Format("%d.%m.%Y %H:%M");
			CString olStfTime = polStf->Doem.Format("%d.%m.%Y %H:%M");
			
			// "Mitarbeiter %s, Eintrittsdatum %s\n
			// Schicht/Abwesenheit %s am %s\n\n
			//
			// Die Schicht liegt vor dem g�ltigen Eintrittsdatum\n
			
			olMsg.Format(LoadStg(IDS_STRING1874), olName, olStfTime, popDrr->Scod, olShiftTime);
		}
		else if(blAfter)
		{
			// Drr-Datum ist nach der Austrittszeit, mu� Benutzer gefragt werden
			// %s
			CString olStfTime = polStf->Dodm.Format("%d.%m.%Y %H:%M");
			
			// "Mitarbeiter %s,  Austrittsdatum %s - Ende der Besch�ftigung*INTXT*"
			
			olMsg.Format(LoadStg(IDS_STRING1936), olName, olStfTime);
		}
		
		AfxMessageBox((LPCTSTR)olMsg, MB_ICONSTOP);
		
		return false;
	}
	return true;
}

//************************************************************************************
// HasDoubleTour: Testen, ob es DRS Informationen gibt.
// R�ckgabe:	true	-> Doppeltour gefunden
//				false	-> keine Doppeltour
//************************************************************************************

bool CedaDrrData::HasDoubleTour(DRRDATA* popDrr)
{
	CCS_TRY;
	if (popDrr == NULL) return false;
	
	if (CString(popDrr->Drsf) == "1")
		return true;
	else
		return false;
	CCS_CATCH_ALL;
	return false;
}

//************************************************************************************
// GetConnectedDrr: gibt den zugeh�rigen DRR des 2. Mitarbeiters der Doppeltour 
//	zur�ck.
// R�ckgabe:	der gefundene Doppeltour-DRR oder NULL
//************************************************************************************

DRRDATA* CedaDrrData::GetConnectedDrr(DRRDATA* popDrr1)
{
	CCS_TRY;
	// zugeh�rigen DRS erhalten
	DRSDATA* polDrs1;
	polDrs1 = ogDrsData.GetDrsByDrru(popDrr1->Urno);
	
	if (polDrs1 == NULL)
		return NULL;
	
	// DRR f�r Mitarbeiter 2 finden
	DRRDATA* polDrr2 = ogDrrData.GetDrrByRoss(polDrs1->Sday,polDrs1->Ats1,"1","","A");
	
	return (polDrr2);
	CCS_CATCH_ALL;
	return NULL;
}

/************************************************************************************
CancelDoubleTour: Hebt die Doppeltour zwischen zwei DRRs auf.
popDrr1 kann produktive oder nicht produktiver MA sein
R�ckgabe:	Aktion erfolgreich?
************************************************************************************/
bool CedaDrrData::CancelDoubleTour(DRRDATA* popDrr1, bool bpShowMessage /*true*/)
{
	CCS_TRY;
	if (popDrr1 == NULL )
		return false;
	
	// Zugeh�rigen DRR finden
	DRRDATA* polDrr2 = GetConnectedDrr(popDrr1);
	
	if (polDrr2 == NULL)
		return false;
	
	//-------------------------------------------------------------------------
	// Pr�fen, ob eine Doppeltour vorliegt
	if (!HasDoubleTour(popDrr1) || !HasDoubleTour(polDrr2))
		return false;
	//-------------------------------------------------------------------------
	
	DRSDATA* polDrs2;
	polDrs2 = ogDrsData.GetDrsByDrru(polDrr2->Urno);
	
	DRSDATA* polDrs1;
	polDrs1 = ogDrsData.GetDrsByDrru(popDrr1->Urno);
	
	
	// Beide DRS gefunden ?
	if (polDrs1 == NULL || polDrs2 == NULL)
		return false;
	
	// Verbindung der Touren Pr�fen
	if (polDrs1->Stfu != polDrs2->Ats1)
		return false;
	
	if (polDrs2->Stfu != polDrs1->Ats1)
		return false;
	
	// Namen berechnen
	CString olFullName1;
	STFDATA *prlStf = ogStfData.GetStfByUrno(popDrr1->Stfu);
	if( prlStf != NULL)
	{
		olFullName1 = "- ";
		olFullName1 += prlStf->Lanm;
		olFullName1 += ", ";
		olFullName1 += prlStf->Finm;
	}
	
	
	CString olProdMA  = "";				//rdr prod.  MA
	CString olNProdMA = "";				//rdr nprod. MA
	CString olDrs     = "";				//rdr 
	bool blClearDrr	  = false;			//rdr p MA Drr l�schen?
	
	// Produktiv nicht produktiv Zusatz setzen
	if (CString(polDrs1->Stat) == "T")
	{
		olProdMA += prlStf->Lanm;	// rdr
		olProdMA += ", ";			// rdr
		olProdMA += prlStf->Finm;	// rdr
		olDrs = "1";				// rdr
		olFullName1 += " (" + LoadStg(IDS_STRING344) +")";
	}
	if (CString(polDrs1->Stat) == "S")
	{
		olNProdMA += prlStf->Lanm;	// rdr
		olNProdMA += ", ";			// rdr
		olNProdMA += prlStf->Finm;	// rdr
		
		olFullName1 += " (" + LoadStg(IDS_STRING349) +")";
	}
	
	// Namen berechnen
	CString olFullName2;
	prlStf = ogStfData.GetStfByUrno(polDrr2->Stfu);
	if( prlStf != NULL)
	{
		olFullName2 = "- ";
		olFullName2 += prlStf->Lanm;
		olFullName2 += ", ";
		olFullName2 += prlStf->Finm;
	}
	
	// Produktiv nicht produktiv Zusatz setzen
	if (CString(polDrs2->Stat) == "T")
	{
		olProdMA += prlStf->Lanm;	// rdr
		olProdMA += ", ";			// rdr
		olProdMA += prlStf->Finm;	// rdr
		olDrs = "2";				// rdr

		olFullName2 += " (" + LoadStg(IDS_STRING344) +")";
	}
	if (CString(polDrs2->Stat) == "S")
	{
		olNProdMA += prlStf->Lanm;	// rdr
		olNProdMA += ", ";			// rdr
		olNProdMA += prlStf->Finm;	// rdr
		
		olFullName2 += " (" + LoadStg(IDS_STRING349) + ")";
	}
	
	CString olMsg = "";
	
	if (bpShowMessage)
	{
		//Die Schicht des nicht produktiven Mitarbeiters wird gel�scht.\n
		//Soll die Schicht des (produktiven) Mitarbeiters %s ebenfalls gel�scht werden?\n*INTXT*
		olMsg.Format (LPCTSTR(LoadStg(IDS_STRING63478)), olProdMA);
		int ilResult;
		ilResult = AfxMessageBox(olMsg,MB_ICONQUESTION | MB_YESNOCANCEL);
		if (ilResult == IDNO)
		{
			blClearDrr = false;
		}
		else if (ilResult == IDYES)
		{
			blClearDrr = true;
		}
		else
		{
			return false;
		}
	}
	
	DRRDATA* polDrrProdMA;
	DRRDATA* polDrrNoProdMA;
	
	if(olDrs.Find("1"))
	{
		polDrrProdMA = polDrr2;
		polDrrNoProdMA = popDrr1;
	}
	else
	{
		polDrrProdMA = popDrr1;
		polDrrNoProdMA = polDrr2;
	}
	
	if (blClearDrr)
	{
		if (!ogDrrData.ClearDrr(polDrrProdMA,true,false))
		{	
			if (ogDrrData.imLastReturnCode != 0)
			{	// Fehler ausgeben
				CString olErrorTxt;
				olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
				AfxMessageBox(olErrorTxt, MB_ICONEXCLAMATION);
				return false;
			}
			else
			{
				// das ist ein Abbruch des L�schens durch Benutzer
				ogDrsData.DeleteDrsByDrrUrno(polDrrProdMA->Urno);
			}
		}	
	}
	else
	{
		// DRS l�schen
		ogDrsData.DeleteDrsByDrrUrno(polDrrProdMA->Urno);
	}
	
	if (!ogDrrData.ClearDrr(polDrrNoProdMA,true,false))
	{	
		if (ogDrrData.imLastReturnCode != 0)
		{	// Fehler ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), ogDrrData.imLastReturnCode, ogDrrData.omLastErrorMessage);
			AfxMessageBox(olErrorTxt,  MB_ICONEXCLAMATION);
			return false;
		}
		else
		{
			// das ist ein Abbruch des L�schens durch Benutzer
			ogDrsData.DeleteDrsByDrrUrno(polDrrNoProdMA->Urno);
		}
	}	
	
	strcpy (polDrrProdMA->Drsf,"");
	polDrrProdMA->IsChanged = DATA_CHANGED;
	Update(polDrrProdMA);
	
	return true;
	CCS_CATCH_ALL;
	return false;
}

/************************************************************************************
// ChangeConnectedScod: �ndert die Schicht bei zugeh�rigen DRR die durch eine 
//	Doppeltour verbunden sind.
// R�ckgabe:	true	-> Aktion erfolgreich oder Umsetzen nicht notwendig.
//				false	-> Fehler oder keine Doppeltour
- CString opNewFctc - wenn nicht leer, dann mu� diese Funktion �bernommen werden 
(kommt aus Coverage/Statistik per DragDrop)
************************************************************************************/

bool CedaDrrData::ChangeConnectedScod(DRRDATA* popDrr1,long lpNewShiftUrno,CString opNewFctc, bool bpCheckMainFuncOnly)
{
	CCS_TRY;
	if (popDrr1 == NULL)
		return false;
	
	// Umsetzen unn�tig, keine Doppeltour vorhanden.
	if (!HasDoubleTour(popDrr1))
		return true;
	
	// Den verbundenen DRR ermitteln
	DRRDATA* polDrr2 = GetConnectedDrr(popDrr1);
	if (polDrr2 == NULL)
		return false;
	
	// Pr�fen ob eine Doppeltour vorliegt.
	if (!HasDoubleTour(polDrr2))
		return false;
	
	// Wenn sich die Schichtcodes oder Funktionen unterscheiden,Code umsetzen.
	
	bool blBsduDif = lpNewShiftUrno != polDrr2->Bsdu;
	bool blFctcDif = false;
	
	// opNewFctc sollte an der Stelle == popDrr1->Fctc sein, doch wir lassen die Pr�fung, weil sie hier irrelevant ist und
	// wegen Undo kann auch anders sein
	// wenn opNewFctc leer ist, kommt die Basisfunktion aus der Schicht lpNewSchiftUrno
	
	if(	!opNewFctc.IsEmpty())
	{
		blFctcDif = (opNewFctc != polDrr2->Fctc);
	}
	else
	{
		BSDDATA* polBsd = ogBsdData.GetBsdByUrno(lpNewShiftUrno);
		blFctcDif = (polBsd != 0 && strcmp(polBsd->Bsdc, polDrr2->Fctc) != 0);
	}
	
	if (blBsduDif || blFctcDif)
	{
		// Information ausgeben und Umsetzen best�tigen lassen
		if (AfxMessageBox(LoadStg(IDS_STRING432), MB_ICONQUESTION|MB_OKCANCEL) == IDCANCEL)
			return false;
	}
	
	if(blFctcDif)
	{
		// Pr�fen, ob der Mitarbeiter diese Funktion erhalten darf
		if(!CheckFuncOrgContract(polDrr2,lpNewShiftUrno,opNewFctc, bpCheckMainFuncOnly))
			return false;
	}
	
	if (blBsduDif || blFctcDif)
	{
		if(lpNewShiftUrno != polDrr2->Bsdu)
		{
			// Schichtcode f�r 2.ten Mitarbeiter pr�fen
			if(polDrr2->Bsdu == ogDrrData.CheckBSDCode(polDrr2,lpNewShiftUrno,polDrr2->Bsdu))
				return false;
		}
		
		// Schichtcode kopieren
		SetDrrDataScodByBSD(polDrr2,lpNewShiftUrno,opNewFctc,true);
		
		if (!Update(polDrr2))
		{	
			// Fehler ausgeben
			CString olErrorTxt;
			olErrorTxt.Format("%s %d\n%s",LoadStg(ST_WRITEERR), imLastReturnCode, omLastErrorMessage);
			AfxMessageBox(olErrorTxt, MB_ICONEXCLAMATION);
			return false;
		}	
		
	}
	return true;
	CCS_CATCH_ALL;
	return false;
}

/**********************************************************************
DRRDATA kopieren
**********************************************************************/
void CedaDrrData::CopyDrr(DRRDATA* popDst, DRRDATA* popSrc)
{
	if(!popDst || !popSrc) return;
	// da wir z.Zt. keine Pointer in der DRRDATA haben, kann memcpy benutzt werden
	memcpy((void*)popDst, (void*)popSrc, sizeof(DRRDATA));
}

/***********************************************************************************
wenn spDrs == "8", "1" oder "2" sind, ist es Teilzeit+ oder �berzeit, return true
***********************************************************************************/
bool CedaDrrData::IsExtraShift(CString opDrs)
{
	if(	opDrs.GetLength() && 
		(opDrs == "8" || opDrs == "2" || opDrs == "1"))
		return true;
	return false;
}

/*********************************************************************************************
IsValidDrr: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten DRRs
0. Die L�nge der Datenfelder pr�fen
1. Datenfelder pr�fen:
COleDateTime	Avfr;					- g�ltige Zeit							// anwesend von
COleDateTime	Avto;					- g�ltige Zeit, nicht kleiner, als Avfr	// anwesend bis
char			Bufu[BUFU_LEN+2]; 	- L�nge									// Funknummer
COleDateTime	Cdat;				 	keine Pr�fung							// Erstellungsdatum
char			Drrn[DRRN_LEN+2];	- L�nge, eine g�ltige Zahl			 	// Schichtnummer (1-n pro Tag)
char 			Expf[EXPF_LEN+2];	- L�nge	 								// Export Flag ('A'= Archive, ' '= not Archive)
COleDateTime	Lstu;					keine Pr�fung							// Datum letzte �nderung
char 			Rema[REMA_LEN+2];	- L�nge	 					 	// Bemerkung
char 			Rosl[ROSL_LEN+2];	- L�nge, eine von "" "2" "3" "4" "1" "L" "U" // Planungsstufe
char 			Ross[ROSS_LEN+2];	- L�nge, eine von "L" "A" "N"	 	// Status der Planungsstufe ('L'=last/vorherige, 'A'=active/aktuelle, 'N'=next/n�chste)
COleDateTime	Sbfr;					keine Pr�fung		// Pausenlage von
COleDateTime	Sbto;					wenn Sbfr und Sbto valid sind, soll Sbto >= Sbfr sein	// Pausenlage bis
char 			Sblp[SBLP_LEN+2];	- L�nge								 	// Pausenl�nge bezahlt
char 			Sblu[SBLU_LEN+2];	- L�nge								 	// Pausenl�nge unbezahlt in Minuten, z.B. = "30"
char 			Scod[SCOD_LEN+2];	- L�nge, ein von BSD/ODA-Codes		 	// Schichtcode (Bezeichner)
char			Scoo[SCOO_LEN+2];	- L�nge,							 	// urspr�ngliche BSD-Schichtcode (Bezeichner)
char 			Sday[SDAY_LEN+2];	- L�nge, Datumsangabe in der Form YYYYMMDD		 	// Tages-Schl�ssel YYYYMMDD
char 			Drsf[DRSF_LEN+2];	- L�nge, "", "0" oder "1"				 	// gibt es einen DRS diesem DRR? 0=Nein, 1=Ja
long 			Stfu;					- eine g�ltige Zahl !=0					// Mitarbeiter-Urno
long 			Urno;					- eine g�ltige Zahl !=0					// Datensatz-Urno
char 			Usec[USEC_LEN+2];	- L�nge	 					 	// Anwender (Ersteller)
char 			Useu[USEU_LEN+2];	- L�nge	 					 	// Anwender (letzte �nderung)
long 			Bsdu;					ein von BDS/ODA Urnos			// BSD Urno				ACHTUNG! bei �nderung DRA nicht vergessen
char 			Bkdp[BKDP_LEN+2];	- L�nge						 	// 1. Pause bezahlt Ja = x
char			Drs1[DRS1_LEN+2];	- L�nge	 					 	// Zusatzinfo zur Schicht / nach Schicht				ACHTUNG! bei �nderung DRA nicht vergessen
char			Drs2[DRS2_LEN+2];	- L�nge	 					 	// Prozent der Arbeitsunf�higkeit (z.B. 050Z)				ACHTUNG! bei �nderung DRA nicht vergessen
char			Drs3[DRS3_LEN+2];	- L�nge	 					 	// Zusatzinfo zur Schicht / vor Schicht				ACHTUNG! bei �nderung DRA nicht vergessen
char			Drs4[DRS4_LEN+2];	- L�nge	 					 	// Zusatzinfo zur Schicht / gesamt				ACHTUNG! bei �nderung DRA nicht vergessen
char 			Fctc[FCTC_LEN+2]; 	- L�nge, Funktion des Mitarbeiters, soll eine von bekannten Funktionen sein

  wenn Scod und Bsdu == ODA-Datensatz und ODA.Tbsd == "1" dann mu� Avto > Avfr sein, weil
  in diesem Fall diese freie Schicht von einer BSD-Schicht abgeleitet ist
  
	ROSS & ROSL:  wenn Rosl == "A", es wird auch gepr�ft, ob noch ein DRR-Datensatz mit Ross == "A" an gleicher Zeit liegt 
	(Freigabe-Problem vermeiden). 
	- Wenn ja und ppopPrevDrr == 0, return false, 
	- wenn ja und ppopPrevDrr != 0, return true und in ppopPrevDrr den Zeiger auf diesen anderen Datensatz
	
	  R�ckgabe:	true	->	DRD ist OK
	  false	->	DRD ist nicht OK
*********************************************************************************************/

bool CedaDrrData::IsValidDrr(DRRDATA *popDrr, DRRDATA** ppopPrevDrr/*=0*/)
{
	CCS_TRY;
	if(!popDrr)
	{
		ogRosteringLogText += "DRR-record: Invalid pointer!\n";
		return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
	}
	
	// es sind zwei legale Zust�nde:
	// initialisiert und gel�scht

	CString olRepaired;

	bool blDeleted = (popDrr->Bsdu == 0);
	
	if(	strlen(popDrr->Drrn) != DRRN_LEN			||
		strlen(popDrr->Rosl) != ROSL_LEN			||
		strlen(popDrr->Ross) > ROSS_LEN				||
		strlen(popDrr->Sday) != SDAY_LEN			||
		strlen(popDrr->Bufu) > BUFU_LEN 			||
		strlen(popDrr->Expf) > EXPF_LEN 			||
		strlen(popDrr->Rema) > REMA_LEN 			||
		strlen(popDrr->Sblp) > SBLP_LEN 			||
		strlen(popDrr->Sblu) > SBLU_LEN 			||
		strlen(popDrr->Scod) > SCOD_LEN 			||
		strlen(popDrr->Scoo) > SCOO_LEN 			||
		strlen(popDrr->Drsf) > DRSF_LEN 			||
		strlen(popDrr->Usec) > USEC_LEN 			||
		strlen(popDrr->Useu) > USEU_LEN 			||
		strlen(popDrr->Bkdp) > BKDP_LEN 			||
		strlen(popDrr->Drs1) > DRS1_LEN 			||
		strlen(popDrr->Drs2) > DRS2_LEN 			||
		strlen(popDrr->Drs3) > DRS3_LEN 			||
		strlen(popDrr->Drs4) > DRS4_LEN 			||
		strlen(popDrr->Fctc) > FCTC_LEN 			||
		!popDrr->Stfu									|| 
		!popDrr->Urno)
	{
#ifdef _DEBUG
		// detaillierte Ausgabe
		if( strlen(popDrr->Drrn) != DRRN_LEN			)		ogRosteringLogText += "strlen(Drrn) != DRRN_LEN			\n";
		if( strlen(popDrr->Rosl) != ROSL_LEN			)		ogRosteringLogText += "strlen(Rosl) != ROSL_LEN			\n";
		if( strlen(popDrr->Ross) >  ROSS_LEN			)		ogRosteringLogText += "strlen(Ross) >  ROSS_LEN			\n";
		if( strlen(popDrr->Sday) != SDAY_LEN			)		ogRosteringLogText += "strlen(Sday) != SDAY_LEN			\n";
		if( strlen(popDrr->Bufu) > BUFU_LEN 			)		ogRosteringLogText += "strlen(Bufu) > BUFU_LEN 			\n";
		if( strlen(popDrr->Expf) > EXPF_LEN 			)		ogRosteringLogText += "strlen(Expf) > EXPF_LEN 			\n";
		if( strlen(popDrr->Rema) > REMA_LEN 			)		ogRosteringLogText += "strlen(Rema) > REMA_LEN 			\n";
		if( strlen(popDrr->Sblp) > SBLP_LEN 			)		ogRosteringLogText += "strlen(Sblp) > SBLP_LEN 			\n";
		if( strlen(popDrr->Sblu) > SBLU_LEN 			)		ogRosteringLogText += "strlen(Sblu) > SBLU_LEN 			\n";
		if( strlen(popDrr->Scod) > SCOD_LEN 			)		ogRosteringLogText += "strlen(Scod) > SCOD_LEN 			\n";
		if( strlen(popDrr->Scoo) > SCOO_LEN 			)		ogRosteringLogText += "strlen(Scoo) > SCOO_LEN 			\n";
		if( strlen(popDrr->Drsf) > DRSF_LEN 			)		ogRosteringLogText += "strlen(Drsf) > DRSF_LEN 			\n";
		if( strlen(popDrr->Usec) > USEC_LEN 			)		ogRosteringLogText += "strlen(Usec) > USEC_LEN 			\n";
		if( strlen(popDrr->Useu) > USEU_LEN 			)		ogRosteringLogText += "strlen(Useu) > USEU_LEN 			\n";
		if( strlen(popDrr->Bkdp) > BKDP_LEN 			)		ogRosteringLogText += "strlen(Bkdp) > BKDP_LEN 			\n";
		if( strlen(popDrr->Drs1) > DRS1_LEN 			)		ogRosteringLogText += "strlen(Drs1) > DRS1_LEN 			\n";
		if( strlen(popDrr->Drs2) > DRS2_LEN 			)		ogRosteringLogText += "strlen(Drs2) > DRS2_LEN 			\n";
		if( strlen(popDrr->Drs3) > DRS3_LEN 			)		ogRosteringLogText += "strlen(Drs3) > DRS3_LEN 			\n";
		if( strlen(popDrr->Drs4) > DRS4_LEN 			)		ogRosteringLogText += "strlen(Drs4) > DRS4_LEN 			\n";
		if( strlen(popDrr->Fctc) > FCTC_LEN 			)		ogRosteringLogText += "strlen(Fctc) > FCTC_LEN 			\n";
		if( !popDrr->Stfu									)	ogRosteringLogText += "Stfu = 0\n";
		if( !popDrr->Urno									)	ogRosteringLogText += "Urno = 0\n";
#endif 
		ogRosteringLogText += "DRR-record: Wrong size!\n";
		return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
	} 
	
	if(	!blDeleted && (
		popDrr->Avfr.GetStatus() != COleDateTime::valid ||
		popDrr->Avto.GetStatus() != COleDateTime::valid ||
		popDrr->Avto < popDrr->Avfr))
	{
		ogRosteringLogText += "DRR-record: Start time < end timepop (AVTO < AVFR)!\n";
		olRepaired += "Start and end time repaired from basic data\n";
		TraceDrr(popDrr, (LPCTSTR)"DRR defect, repairing: ");

		// check, if BSD or ODA
		BSDDATA *polBsd = ogBsdData.GetBsdByUrno(popDrr->Bsdu);
		if (polBsd != NULL)
		{
			// SDAY konvertieren
			COleDateTime olSday;
			if (!SdayToOleDateTime(popDrr,olSday)) return false;	// bei Fehler abbrechen
			// ESBG umwandeln in Zeit
			COleDateTimeSpan olEsbgSpan;
			if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Esbg,olEsbgSpan)) return false;	// bei Fehler abbrechen
			// Anwesend von einstellen: SDAY + ESBG
			popDrr->Avfr = olSday + olEsbgSpan;
			//	TRACE("*** Schichtbeginn: %s ***\n",popDrrData->Avfr.Format("%d.%m.%Y, %H:%M:%S"));
			
			//---------------------------------------------------------------------------------------------------------------------
			// AVTO einstellen aus DRR->und BSD->LSEN (sp�testes Schichtende)
			//---------------------------------------------------------------------------------------------------------------------
			// LSEN umwandeln in Zeit
			COleDateTimeSpan olLsenSpan;
			if (!CedaDataHelper::HourMinStringToOleDateTimeSpan(polBsd->Lsen,olLsenSpan)) return false;	// bei Fehler abbrechen
			// Sp�testes Schichtende fr�her als fr�hester Schichtbeginn?
			if (olLsenSpan <= olEsbgSpan)
			{
				// ja -> einen Tag zus�tzlich addieren
				olLsenSpan += COleDateTimeSpan(1,0,0,0);
			}
			// Anwesend bis einstellen: SDAY + LSEN
			popDrr->Avto = olSday + olLsenSpan;
		}
		else
		{
			ODADATA *prlOdaData = ogOdaData.GetOdaByUrno(popDrr->Bsdu);
			if (prlOdaData != NULL)
			{
				// Felder setzen
				// SDAY konvertieren
				COleDateTime olSday;
				if (!SdayToOleDateTime(popDrr, olSday))
					return false;	// bei Fehler abbrechen

				// wenn in ODA g�ltige Anfangs- und Endzeiten sind, kopieren wir diese Zeiten in DRR.
				COleDateTimeSpan olAbfr, olAbto;
				if (CedaDataHelper::HourMinStringToOleDateTimeSpan(prlOdaData->Abfr,olAbfr) &&
					CedaDataHelper::HourMinStringToOleDateTimeSpan(prlOdaData->Abto,olAbto))
				{
					popDrr->Avfr = olSday + olAbfr;
					popDrr->Avto = olSday + olAbto;

					if (popDrr->Avto <= popDrr->Avfr)
					{
						// ja -> einen Tag zus�tzlich addieren
						popDrr->Avto += COleDateTimeSpan(1,0,0,0);
					}
					strcpy(popDrr->Sblu, prlOdaData->Blen);
				}
				else
				{
					bool blGenerateTime = false;
					if (strcmp(prlOdaData->Tbsd, "1") == 0)
					{
						if (popDrr->Avfr.GetStatus() != COleDateTime::valid || 
							popDrr->Avto.GetStatus() != COleDateTime::valid)
						{
							blGenerateTime = true;
						}
					}
					else
					{
						blGenerateTime = true;
					}

					if (blGenerateTime)
					{
						// wenn in DRR KEINE g�ltige Anfangs- und Endzeiten sind oder keine Zeiten in ODATAB sind,
						// generieren wir die Zeiten als 1/5 der Wochenzeit
						// abh�ngig vom Vertrag des MAs, mit Anfangszeit 8:30.
						{
							popDrr->Avfr = olSday + COleDateTimeSpan(0,8,30,0);
							CAccounts olAccount(&ogDrrData,&ogAccData,&ogStfData,&ogBsdData,&ogOdaData,&ogScoData,&ogCotData);
							double dCommonDayWorkingTime = olAccount.GetCommonDayWorkingTime(popDrr);
							dCommonDayWorkingTime += 0.5;	// runden
							popDrr->Avto = popDrr->Avfr + COleDateTimeSpan(0,0,(int)dCommonDayWorkingTime,0);
							// Pausenl�nge 0
							strcpy(popDrr->Sblu,"0000");
							popDrr->Sbfr.SetTime(0,0,0);
							popDrr->Sbto.SetTime(0,0,0);
						}
					}
				}
			}
			else
				return false;
		}

	}
	
	if(atoi(popDrr->Drrn) <= 0) 
	{
		ogRosteringLogText += "DRR-record: Wrong shift number (DRRN <= 0)!\n";
		return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
	}
	
	// nur diese Zeichen sind korrekt:
	switch(popDrr->Rosl[0])
	{
	default:
		ogRosteringLogText += "DRR-record: Wrong shift level (ROSL)!\n";
		return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
	case '2':
	case '3':
	case '4':
	case '5':
	case '1':
	case 'L':
	case 'U':
	case 'W':
		break;
	}
	
	// ROSS
	switch(popDrr->Rosl[0])
	{
	default: 
		break;
	case '2':
	case '3':
	case '4':
	case '5':
		// ROSS: nur diese Zeichen sind korrekt:
		switch(popDrr->Ross[0])
		{
		default:
			ogRosteringLogText += "DRR-record: Wrong shift status (ROSS)!\n";
			return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
		case 'L':
		case 'A':
		case 'N':
			break;
		}
	}

	if(	!blDeleted && 
		popDrr->Sbfr.GetStatus() == COleDateTime::valid && 
		popDrr->Sbto.GetStatus() == COleDateTime::valid && 
		popDrr->Sbto < popDrr->Sbfr)
	{
		// Pausenende liegt vor dem Pausenanfang
		ogRosteringLogText += "DRR-record: Break from < break to (SBTO < SBFR)!\n";
		olRepaired += "Break from and to repaired from basic data!\n";
		TraceDrr(popDrr, (LPCTSTR)"DRR defect, repairing: ");

		// check, if BSD or ODA
		BSDDATA *polBsd = ogBsdData.GetBsdByUrno(popDrr->Bsdu);
		if (polBsd != NULL)
		{
			// SDAY konvertieren
			COleDateTime olSday;
			if (!SdayToOleDateTime(popDrr,olSday)) return false;	// bei Fehler abbrechen

			// SBFR einstellen aus DRR->SDAY (g�ltig am) und BSD->BKF1 (Pausenbeginn)
			//---------------------------------------------------------------------------------------------------------------------
			// BKF1 umwandeln in Zeit
			COleDateTimeSpan olBkf1Span;
			if(ogBsdData.GetBkf1Time(polBsd, olBkf1Span))
			{
				// Pause von einstellen: SDAY + BKF1
				popDrr->Sbfr = olSday + olBkf1Span;
			}
			else 
				popDrr->Sbfr.SetStatus(COleDateTime::null);
			
			//---------------------------------------------------------------------------------------------------------------------
			// SBTO einstellen aus DRR->und BSD->BKT1 (Pausenende)
			//---------------------------------------------------------------------------------------------------------------------
			// BKT1 umwandeln in Zeit
			COleDateTimeSpan olBkt1Span;
			if(ogBsdData.GetBkt1Time(polBsd, olBkt1Span))
			{
				// Pause bis einstellen: SDAY + BKT1
				popDrr->Sbto = olSday + olBkt1Span;
			}
			else 
				popDrr->Sbto.SetStatus(COleDateTime::null);

		}
		else
		{
			ODADATA *prlOdaData = ogOdaData.GetOdaByUrno(popDrr->Bsdu);
			if (prlOdaData != NULL)
			{
				// Felder setzen
				// SDAY konvertieren
				COleDateTime olSday;
				if (!SdayToOleDateTime(popDrr, olSday))
					return false;	// bei Fehler abbrechen


			}
			else
				return false;
		}

	}
	
	if(!blDeleted)
	{
		// Scode-Check
		int ilCode = ogOdaData.IsODAAndIsRegularFree(CString(popDrr->Scod));
		switch(ilCode)
		{
		default:
			// kein g�ltiger Scod
			ogRosteringLogText += "DRR-record: Shift code (SCOD) is unknown!\n";
			olRepaired += "Shift code (SCOD) repaired from basic data!\n";
			TraceDrr(popDrr, (LPCTSTR)"DRR defect,repairing: ");
			strcpy(popDrr->Scod,"");				
		break;			
		case CODE_IS_BSD:		// keine Abwesenheit
			if(popDrr->Avfr == popDrr->Avto)
			{
				// f�r einen nicht FREE-DRR gleiche Zeiten haben keinen Sinn
				ogRosteringLogText += "DRR-record: No absence, but time from = time to (AVFR = AVTO)!\n";
				return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
			}
			if(popDrr->Bsdu != 0)
			{
				BSDDATA *polBsd = ogBsdData.GetBsdByUrno(popDrr->Bsdu);
				
				if(	!polBsd || strcmp(polBsd->Bsdc, popDrr->Scod) != 0)
				{
#ifdef _DEBUG
					if(!polBsd)
					{
						// kein BSDDATA f�r Bsdu gefunden
						ogRosteringLogText += "BSDDATA for DRR.BSDU not found\n"; 
					}
					else
					{
						// Bsdu falsch oder zeigt nicht auf den richtigen BSD-Datensatz
						ogRosteringLogText += "DRR.SCOD != BSD.BSDC\n"; 
					}
#endif _DEBUG
					if (polBsd != NULL)
					{
						TraceDrr(popDrr, (LPCTSTR)"DRR defect,repairing: ");
						olRepaired += "Shift code (SCOD) repaired from basic data!\n";
						strcpy(popDrr->Scod,polBsd->Bsdc);
					}
					else
					{
						// check if bsdu matches an absence instead 
						polBsd = ogBsdData.GetBsdByBsdc(popDrr->Scod);
						if (polBsd != NULL)
						{
							TraceDrr(popDrr, (LPCTSTR)"DRR defect,repairing: ");
							olRepaired += "Basic shift urno repaired from basic data\n"; 
							popDrr->Bsdu = polBsd->Urno;
						}
						else
						{
							return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
						}
					}
				}
			}
			else
			{
				ogRosteringLogText += "BSDU is empty\n";
				return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
			}
			
			break;
		case CODE_IS_ODA_FREE:	// <opCode> ist ODA-Datensatz, Flag ist NICHT gesetzt - nicht regul�r
		case CODE_IS_ODA_REGULARFREE:				// Code ist ODA, Flag ist gesetzt
			{
				ODADATA* polOda = ogOdaData.GetOdaByUrno(popDrr->Bsdu);
				if(	!polOda || strcmp(polOda->Sdac, popDrr->Scod) != 0)
				{
					// Bsdu falsch oder zeigt nicht auf den richtigen ODA-Datensatz
#ifdef _DEBUG
					if(!polOda)
					{
						ogRosteringLogText += "ODA for DRR.SCOD is not found\n"; 
					}
					else
					{
						ogRosteringLogText += "ODA.SDAC != DRR.SCOD\n"; 
					}
#endif
					if (polOda != NULL)
					{
						TraceDrr(popDrr, (LPCTSTR)"DRR defect,repairing: ");
						olRepaired += "Shift code repaired from basic data\n"; 
						strcpy(popDrr->Scod,polOda->Sdac);
					}
					else
					{
						polOda = ogOdaData.GetOdaBySdac(popDrr->Scod);
						if (polOda != NULL)
						{
							TraceDrr(popDrr, (LPCTSTR)"DRR defect,repairing: ");
							olRepaired += "Basic shift urno repaired from basic data\n"; 
							popDrr->Bsdu = polOda->Urno;
						}
						else
						{
							return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
						}
					}
				}

				if (strcmp(polOda->Work, "1") != 0 && (strlen(popDrr->Drs1) || strlen(popDrr->Drs3) || strlen(popDrr->Drs4)))
				{
					ogRosteringLogText += "DRR-record: Absence is not treated like a shift, but OT-flag(s) are set!\n";
					return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
				} 
				
				bool blWork;
				if(CODE_IS_ODA_FREE == ogOdaData.IsODAAndIsRegularFree(popDrr->Scod, 0, &blWork))
				{
					if(blWork && popDrr->Avto <= popDrr->Avfr)
					{
						// wenn Scod und Bsdu == ODA-Datensatz und ODA.Work == "1" dann mu� Avto > Avfr sein, weil
						// in diesem Fall diese freie Schicht in die Zeitkontrolle kommt
						ogRosteringLogText += "ODA.WORK = 1, but DRR.AVTO <= DRR.AVFR\n";
						TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
					}
				}
			}
			break;
		}
	}

	COleDateTime olDTime;
	if(!YYYYMMDDToOleDateTime(CString(popDrr->Sday), olDTime)) 
	{
		// Datumsformat ist falsch
		ogRosteringLogText += "SDAY is defect\n";
		return TraceDrr(popDrr, (LPCTSTR)"DRR defect: ");
	}

	if(!blDeleted)
	{
		switch(popDrr->Drsf[0])
		{
		default:
			// kein g�ltiges Zeichen
			ogRosteringLogText += "DRSF is wrong\n";
			TraceDrr(popDrr, (LPCTSTR)"DRR defect,repairing: ");
			olRepaired += "DRSF repaired, set to empty\n";
			strcpy(popDrr->Drsf,"");
		break;
		case 0:
		case '0':
		case '1':
			break;
		}
	}

	// Fctc pr�fen
	if (strlen(popDrr->Fctc) > 0 && CString(popDrr->Fctc) != CString(" ") && !ogPfcData.GetPfcByFctc(CString(popDrr->Fctc)))
	{
		ogRosteringLogText += "DRR.FCTC in PFCTAB not found\n";
		TraceDrr(popDrr, (LPCTSTR)"DRR defect,repairing: ");
		olRepaired += "Function code repaired, set to empty\n";
		strcpy(popDrr->Fctc,"");
	}

	// copy repair information to DRR record if not already filled
	strncpy(popDrr->Repaired,olRepaired,sizeof(popDrr->Repaired));
	popDrr->Repaired[sizeof(popDrr->Repaired)-1] = '\0';

	// DRR ist g�ltig!
	return true;

	CCS_CATCH_ALL;
	return false;
}

/********************************************************************************
Debug Trace Funktion
********************************************************************************/
bool CedaDrrData::TraceDrr(DRRDATA *popDrr, LPCTSTR popStr)
{
	CString olMsg = popStr;
	GetDataFormatted(olMsg, popDrr);

	ogRosteringLogText += olMsg;
	WriteInRosteringLog(LOGFILE_TRACE);

	ogErrlog += olMsg;
	WriteInErrlogFile();
	
	return false;
}

/*****************************************************************************
1. Message Box, die mitteilt, dass die anh�ngende Abwesenheiten und Abweichungen
gel�scht werden
2. wenn vom Benutzer best�tigt, alle DRDs und DRAs l�schen
R�ckgabe: true, wenn best�tigt, sonst false
*****************************************************************************/
bool CedaDrrData::DeleteDRA_DRDbyChangeBsd(DRRDATA* popDrr)
{
	CMapPtrToPtr	olDraMap;
	CMapPtrToPtr	olDrdMap;
	
	// alle relevanten DRAs & DRDs ermitteln
	if (ogDraData.GetDraMapOfDrr(&olDraMap,popDrr) == -1 ||
		ogDrdData.GetDrdMapOfDrr(&olDrdMap,popDrr) == -1)
	{
		// Fehler -> terminieren
		return false;
	}
	
	if (olDraMap.IsEmpty() && olDrdMap.IsEmpty())
	{
		// keine DRAs und DRDs, kein Problem
		return true;
	}
	
	// Liste der DRAs erstellen
	long llUrno;
	CString olThisEntry;
	
	CString olDraList;		
	CString olTimeFrom;
	CString olTimeTo;
	if (!olDraMap.IsEmpty())
	{
		DRADATA* polDra = 0;
		
		for(POSITION pos = olDraMap.GetStartPosition(); pos != 0;)
		{
			olDraMap.GetNextAssoc(pos, (void *&)llUrno, (void *&)polDra);
			if(!polDra)
				continue;
			
			olTimeFrom.Empty();
			olTimeTo.Empty();
			
			if(polDra->Abfr.GetStatus() == COleDateTime::valid)
			{
				olTimeFrom = polDra->Abfr.Format("%H:%M");
				
				if(polDra->Abto.GetStatus() == COleDateTime::valid)
				{
					olTimeTo = polDra->Abto.Format("%H:%M");
				}
			}
			
			// Sdac / Abfr - Abto
			olThisEntry.Format("%s\t%s - %s\n", 
				polDra->Sdac, 
				olTimeFrom,
				olTimeTo);
			
			olDraList += olThisEntry;
		}
	}
	
	// Liste der DRDs erstellen
	CString olDrdList;		
	if (!olDrdMap.IsEmpty())
	{
		DRDDATA* polDrd = 0;
		
		for(POSITION pos = olDrdMap.GetStartPosition(); pos != 0; )
		{
			olDrdMap.GetNextAssoc(pos, (void *&)llUrno, (void *&)polDrd);
			if(!polDrd)
				continue;
			
			olTimeFrom.Empty();
			olTimeTo.Empty();
			
			if(polDrd->Drdf.GetStatus() == COleDateTime::valid)
			{
				olTimeFrom = polDrd->Drdf.Format("%H:%M");
				
				if(polDrd->Drdt.GetStatus() == COleDateTime::valid)
				{
					olTimeTo = polDrd->Drdt.Format("%H:%M");
				}
			}
			
			// Fctc / Dpt1 / Drdf - Drdt
			olThisEntry.Format("%s / %s\t%s - %s\n", 
				polDrd->Fctc, 
				polDrd->Dpt1,
				olTimeFrom,
				olTimeTo);
			
			olDrdList += olThisEntry;
		}
	}
	
	// MessageBox -en 
	CString olMsg;
	if (!olDraMap.IsEmpty() && olDrdMap.IsEmpty())
	{
		// nur Dra
		
		// "Durch die �nderung des Schichtcodes %s werden\n
		// die zugeordneten Abwesenheiten ebenfalls gel�scht.\n\n
		// Zugeordneten Abwesenheiten:\n%s*INTXT*"
		olMsg.Format(LPCTSTR(LoadStg(IDS_STRING1872)),popDrr->Scod, olDraList);
	}
	else if(olDraMap.IsEmpty() && !olDrdMap.IsEmpty())
	{
		// nur Drd
		// "Durch die �nderung des Schichtcodes %s werden\n
		// die zugeordneten Abweichungen ebenfalls gel�scht.\n\n
		// Zugeordneten Abweichungen:\n%s*INTXT*"
		olMsg.Format(LPCTSTR(LoadStg(IDS_STRING1871)),popDrr->Scod, olDraList, olDrdList);
	}
	else
	{
		// Dra & Drd
		
		// "Durch die �nderung des Schichtcodes %s werden\n
		// die zugeordneten Abwesenheiten und die zugeordneten Abweichungen ebenfalls gel�scht.\n\n
		// Zugeordneten Abwesenheiten:\n%s\n
		// Zugeordneten Abweichungen:\n%s*INTXT*"
		olMsg.Format(LPCTSTR(LoadStg(IDS_STRING1873)),popDrr->Scod, olDraList, olDrdList);
	}
	
	
	// L�schen
	if (AfxMessageBox(olMsg,MB_ICONQUESTION | MB_OKCANCEL) == IDOK)
	{
		// -> DRA l�schen
		if (!olDraMap.IsEmpty())
			DeleteAllDraByDrr(popDrr);
		// -> DRD l�schen
		if (!olDrdMap.IsEmpty())
			DeleteAllDrdByDrr(popDrr);
		return true;
	}
	
	return false;
}

/****************************************************************************************
bda: makes a formatted string with all fields of a data structure for trace and debug output
****************************************************************************************/
bool CedaDrrData::GetDataFormatted(CString &pcpListOfData, DRRDATA *prpDrr)
{
	// Name
	CString olName = "";
	STFDATA *prlStf;
	prlStf = ogStfData.GetStfByUrno(prpDrr->Stfu);
	
	if (prlStf > NULL)
	{
		olName.Format("%s,%s",prlStf->Lanm,prlStf->Finm);
	}
	
	pcpListOfData += olName;
	pcpListOfData += " \t";
	CedaData::GetDataFormatted(pcpListOfData, (void *)prpDrr);
	return true;
}

/************************************************************************************
gives to the interface process a command to start a data import from SAP file
************************************************************************************/
bool CedaDrrData::InitializeShiftImport(COleDateTime opDateFrom, COleDateTime opDateTo)
{
	CString olSelection;

	olSelection.Format("%s,%s",opDateFrom.Format("%Y%m%d"),opDateTo.Format("%Y%m%d"));
	//MakeCedaString(olSelection);

	CedaAction("SAP",CCSCedaData::pcmTableName,"",olSelection.GetBuffer(0),"","");

	return true;
}

void CedaDrrData::UnRelease(COleDateTime opFrom, COleDateTime opTo, CString opLevel, CList<long,long> *opStfUrnoList)
{
	CWaitCursor olDummy;

	int ilLevel1 = atoi(opLevel.GetBuffer(0));
	int ilLevel2 = ilLevel1 - 1;

	if (ilLevel1 > 0)
	{
		COleDateTimeSpan olDay = COleDateTimeSpan(1, 0, 0, 0);
		POSITION olPos;

		CString olSDAY;
		CString olLevel1;
		CString olLevel2;
		CString olDRRN;
		olLevel1.Format("%d", ilLevel1);
		olLevel2.Format("%d", ilLevel2);

		long llStfUrno;

		DRRDATA *prlDrrData1 = NULL;
		DRRDATA *prlDrrData2 = NULL;
		DRSDATA *prlDrsData  = NULL;

		while (opFrom <= opTo)
		{
			olSDAY = opFrom.Format("%Y%m%d");
			for (olPos = opStfUrnoList->GetHeadPosition(); olPos != NULL;)
			{
				llStfUrno = opStfUrnoList->GetNext(olPos);

				//n-shifts
				for (int i = 1; i <= 2; i++)
				{
					olDRRN.Format("%d", i);
					prlDrrData1 = GetDrrByKey(olSDAY, llStfUrno, olDRRN, olLevel1);
					if (prlDrrData1 != NULL)
					{
						if (strcmp(prlDrrData1->Ross, "A") == NULL)
						{
							prlDrrData1->IsChanged = DATA_DELETED;
							prlDrsData = ogDrsData.GetDrsByDrru(prlDrrData1->Urno);
							if (ilLevel2 != 1)
							{
								prlDrrData2 = GetDrrByKey(olSDAY, llStfUrno, olDRRN, olLevel2);
								if (prlDrrData2 != NULL)
								{
									strcpy(prlDrrData2->Ross,"A");
									prlDrrData2->IsChanged = DATA_CHANGED;
									if (prlDrsData != NULL)
									{
										prlDrsData->Drru = prlDrrData2->Urno;
										prlDrsData->IsChanged = DATA_CHANGED;
										ogDrsData.Update(prlDrsData);
									}
								}
								else
								{
									if (prlDrsData != NULL)
									{
										ogDrsData.Delete(prlDrsData, TRUE);
									}
								}
							}
							else
							{
								if (prlDrsData != NULL)
								{
									ogDrsData.Delete(prlDrsData, TRUE);
								}
							}
						}
					}
				}
			}

			opFrom += olDay;
		}

		if (ilLevel1 == 3)
		{
			Release(olLevel2, olLevel1, true);
		}
		else
		{
			Release(olLevel2, olLevel1, false);
		}

		//refresh complete view
		DutyRoster_View *pView = NULL;

		CWinApp *pApp = AfxGetApp();
		POSITION pos = pApp->GetFirstDocTemplatePosition();
		while (pos != NULL && pView == NULL)
		{
			CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
			POSITION p1 = pDocTempl->GetFirstDocPosition();
			while (p1 != NULL && pView == NULL)
			{
				CDocument *pDoc = pDocTempl->GetNextDoc(p1);
				POSITION p2 = pDoc->GetFirstViewPosition();
				while (p2 != NULL && pView == NULL)
				{
					pView = DYNAMIC_DOWNCAST(DutyRoster_View,pDoc->GetNextView(p2));
				}
			}
		}

		if (pView != NULL)
		{
			pView->pomSolidTabViewer->ChangeView(false, false);
			pView->pomIsTabViewer->ChangeView(false, false);
			pView->pomDebitTabViewer->ChangeView(false, false);
			pView->pomStatTabViewer->ChangeView(false, false);
		}
	}
}

bool CedaDrrData::IsDrrChanged(DRRDATA *popDrr1, DRRDATA *popDrr2)
{
	if (popDrr1->Avfr != popDrr2->Avfr)
		return true;

	if (popDrr1->Avto != popDrr2->Avto)
		return true;

	if (strcmp(popDrr1->Bufu,popDrr2->Bufu) != 0)
		return true;

	if (strcmp(popDrr1->Drrn,popDrr2->Drrn) != 0)
		return true;

	if (strcmp(popDrr1->Drsf,popDrr2->Drsf) != 0)
		return true;

	if (strcmp(popDrr1->Rema,popDrr2->Rema) != 0)
		return true;

	if (strcmp(popDrr1->Ross,popDrr2->Ross) != 0)
		return true;

	if (strcmp(popDrr1->Rosl,popDrr2->Rosl) != 0)
		return true;

	if (popDrr1->Sbfr != popDrr2->Sbfr)
		return true;

	if (popDrr1->Sbto != popDrr2->Sbto)
		return true;

	if (strcmp(popDrr1->Scod,popDrr2->Scod) != 0)
		return true;

	if (strcmp(popDrr1->Scoo,popDrr2->Scoo) != 0)
		return true;

	if (strcmp(popDrr1->Sblp,popDrr2->Sblp) != 0)
		return true;

	if (strcmp(popDrr1->Sblu,popDrr2->Sblu) != 0)
		return true;

	if (strcmp(popDrr1->Sday,popDrr2->Sday) != 0)
		return true;

	if (popDrr1->Stfu != popDrr2->Stfu)
		return true;

	if (popDrr1->Bsdu != popDrr2->Bsdu)
		return true;

	if (strcmp(popDrr1->Bkdp,popDrr2->Bkdp) != 0)
		return true;

	if (strcmp(popDrr1->Drs1,popDrr2->Drs1) != 0)
		return true;

	if ((strcmp(popDrr1->Drs2," ") != 0) && (strcmp(popDrr2->Drs2," ") != 0))
	{
		if (strcmp(popDrr1->Drs2,popDrr2->Drs2) != 0)
			return true;
	}

	if (strcmp(popDrr1->Drs3,popDrr2->Drs3) != 0)
		return true;

	if (strcmp(popDrr1->Drs4,popDrr2->Drs4) != 0)
		return true;

	if (strcmp(popDrr1->Fctc,popDrr2->Fctc) != 0)
		return true;

	if (strcmp(popDrr1->Prfl,popDrr2->Prfl) != 0)
		return true;

	if (strlen(popDrr1->Repaired) > 0 || strlen(popDrr2->Repaired) > 0)
		return true;
	return false;
}

void CedaDrrData::ReleaseRecords(CCSPtrArray<DRRDATA> *popArr, CString opActionHint)
{
CCS_TRY;
	if (popArr == NULL)
		return;
	if (popArr->GetSize() == 0)
		return;

	CString olIRT, olURT, olDRT;
	CString olFieldList;
	CString olUpdateString, olInsertString, olDeleteString;
	// Zwischenspeicher
	CString olTmpText;
	// Z�hler f�r Anzahlen der ge�nderten/gel�schten/neuen Datens�tze
	int ilNoOfUpdates = 0, ilNoOfDeletes = 0, ilNoOfInserts = 0;

	// Anzahl der Felder pro Datensatz ermitteln
	CStringArray olFields;
	int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 

	// Kommandostrings (Header) generieren
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	int ilCurrentCount = 0;

	// alte Feldliste speichern
	olOrigFieldList = CString(pcmListOfFields);
	// Strings f�r CedaAction initialisieren
	olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
	olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
	olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

	DRRDATA *prlDrr;
	for (long l = 0; l < popArr->GetSize(); l++)
	{
		bool blDelete = false;

		prlDrr = &(*popArr)[l];

		if(prlDrr->IsChanged != DATA_UNCHANGED )
		{
			CString olListOfData;
			CString olCurrentUrno;
			olCurrentUrno.Format("%d",prlDrr->Urno);
			// aus dem Datensatz einen String generieren (omResInfo ist Member von CCSCedaData)
			MakeCedaData(&omRecInfo,olListOfData,prlDrr);
			// was soll mit dem Datensatz geschehen
			switch(prlDrr->IsChanged)
			{
			case DATA_NEW:	// neuer DS
				olInsertString += olListOfData + CString("\n");
				ilNoOfInserts++;
				break;
			case DATA_CHANGED:	// ge�nderter DS
				olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
				ilNoOfUpdates++;
				break;
			case DATA_DELETED:	// DS l�schen
				olDeleteString += olCurrentUrno + CString("\n");
				blDelete = true;
				ilNoOfDeletes++;
				break;
			}
			// Z�hler inkrementieren
			ilCurrentCount++;

			if (blDelete == false)
			{
				// �nderungsflag zur�cksetzen
				prlDrr->IsChanged = DATA_UNCHANGED;
			}
			else
			{
				DeleteInternal(prlDrr, true);
			}
		}

		// 500 Datens�tze im Paket
		if(ilCurrentCount == 500)
		{
			if(ilNoOfInserts > 0)
			{
				// ja -> speichern
				CedaAction("REL",opActionHint.GetBuffer(0),"",olInsertString.GetBuffer(0));
			}
			if(ilNoOfUpdates > 0)
			{
				// ja -> speichern
				CedaAction("REL",opActionHint.GetBuffer(0),"",olUpdateString.GetBuffer(0));
			}
			if(ilNoOfDeletes > 0)
			{
				// ja -> aus Datenbank l�schen
				CedaAction("REL",opActionHint.GetBuffer(0),"",olDeleteString.GetBuffer(0));
			}

			// Z�hler und Strings wieder auf Initialwerte setzen
			ilCurrentCount = 0;
			ilNoOfInserts = 0;
			ilNoOfUpdates = 0;
			ilNoOfDeletes = 0;
			olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
			olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
			olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
		}
	}

	// Rest(von unter Fuenfhundert)-Paket in Datenbank speichern
	if(ilNoOfInserts > 0)
	{
		CedaAction("REL",opActionHint.GetBuffer(0),"",olInsertString.GetBuffer(0));
	}
	if(ilNoOfUpdates > 0)
	{
		CedaAction("REL",opActionHint.GetBuffer(0),"",olUpdateString.GetBuffer(0));
	}
	if(ilNoOfDeletes > 0)
	{
		CedaAction("REL",opActionHint.GetBuffer(0),"",olDeleteString.GetBuffer(0));
	}

	CCS_CATCH_ALL;
}

int CedaDrrData::GetMaxFieldLength(const CString& opField)
{

	return CedaObject::GetMaxFieldLength("DRR",opField);
}
