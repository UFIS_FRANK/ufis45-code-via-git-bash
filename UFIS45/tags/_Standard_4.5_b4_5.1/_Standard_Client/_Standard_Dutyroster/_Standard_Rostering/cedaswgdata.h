// CedaSwgData.h

#ifndef __CEDAPSWGDATA__
#define __CEDAPSWGDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

// Felddimensionen
#define SWG_CODE_LEN	(7)		// CODE == WGPC


struct SWGDATA 
{
	long 			Surn;
	long 			Urno;
	char 			Code[SWG_CODE_LEN];	// = Wgpc
	COleDateTime 	Vpfr;
	COleDateTime 	Vpto;

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SWGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SWGDATA

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSwgData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SWGDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// OSwgations
public:
    CedaSwgData();
	~CedaSwgData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SWGDATA *prpSwg);
	bool InsertInternal(SWGDATA *prpSwg);
	bool Update(SWGDATA *prpSwg);
	bool UpdateInternal(SWGDATA *prpSwg);
	bool Delete(long lpUrno);
	bool DeleteInternal(SWGDATA *prpSwg);
	bool ReadSpecialData(CCSPtrArray<SWGDATA> *popSwg,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SWGDATA *prpSwg);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SWGDATA* GetSwgByUrno(long lpUrno);
	void GetSwgBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SWGDATA> *popSwgData);
	CString GetWgpBySurnWithTime(long lpSurn, COleDateTime opDate);
	SWGDATA* GetSwgBySurnWithTime(long lpSurn, COleDateTime opDate);
	CString GetSwgByWgpWithTime(CStringArray *popWgp,COleDateTime opStart,COleDateTime opEnd,CCSPtrArray<SWGDATA> *popSwgData);
	bool SetWgpBySurnWithTime(long lpSurn, COleDateTime opDate, LPCTSTR popWgpc);
	bool IsValidSwg(SWGDATA *prpSwg);


	// Private methods
private:
    void PrepareSwgData(SWGDATA *prpSwgData);

};	

//---------------------------------------------------------------------------------------------------------

extern CedaSwgData ogSwgData;

#endif //__CEDAPSWGDATA__
