// SelectShiftDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CSelectShiftDlg 


CSelectShiftDlg::CSelectShiftDlg(CWnd* pParent, DRRDATA *popDrr)
	: CDialog(CSelectShiftDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectShiftDlg)
	//}}AFX_DATA_INIT
	// save parameteres
	pomDrr			= popDrr;
	pomBsd			= 0;
	pomOda			= 0;
	pomBsdGrid		= NULL;
	pomOdaGrid		= NULL;
	imCodeTypeSelected = CODE_UNDEFINED;	
	lmCodeUrnoSelected = 0;

	// looking after the view of our parent
	DutyRoster_View *pView = NULL;

	CWinApp *pApp = AfxGetApp();
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while (pos != NULL && pView == NULL)
	{
		CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
		POSITION p1 = pDocTempl->GetFirstDocPosition();
		while (p1 != NULL && pView == NULL)
		{
			CDocument *pDoc = pDocTempl->GetNextDoc(p1);
			POSITION p2 = pDoc->GetFirstViewPosition();
			while (p2 != NULL && pView == NULL)
			{
				pView = DYNAMIC_DOWNCAST(DutyRoster_View,pDoc->GetNextView(p2));
			}
		}
	}

	if (pView != NULL)
	{
		prmViewInfo = &pView->rmViewInfo;
	}
	else
	{
		prmViewInfo = NULL;
	}
}

CSelectShiftDlg::~CSelectShiftDlg()
{
	if (pomBsdGrid != NULL)
		delete pomBsdGrid;

	if (pomOdaGrid != NULL)
		delete pomOdaGrid;
}

void CSelectShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectShiftDlg)
	DDX_Control(pDX, IDOK, m_But_Ok);
	DDX_Control(pDX, IDCANCEL, m_But_Cancel);
	DDX_Control(pDX, IDC_BUTTON_FIND_SCOD, m_But_Search);
	DDX_Control(pDX, IDC_BUTTON_FIND_ODA, m_But_Search_Oda);
	DDX_Control(pDX, IDC_C_FITTING_SHIFTS, m_Combo_Fitting_Shifts);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectShiftDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectShiftDlg)
	ON_BN_CLICKED(IDC_BUTTON_FIND_SCOD, OnButtonFindScod)
	ON_BN_CLICKED(IDC_BUTTON_FIND_ODA, OnButtonFindOda)
	ON_BN_CLICKED(IDC_C_FITTING_SHIFTS, OnButtonFittingShifts)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_LBUTTONDOWN,	OnGridLButton)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CSelectShiftDlg 

BOOL CSelectShiftDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// String-Resourcen setzen.
	// Titel
	SetWindowText(LoadStg(IDS_STRING971));
	
	// Beschriftungen der Buttons
	m_But_Search.SetWindowText(LoadStg(IDS_STRING1002));
	m_But_Search_Oda.SetWindowText(LoadStg(IDS_STRING1002));
	m_But_Ok.SetWindowText(LoadStg(IDS_STRING998));
	m_But_Cancel.SetWindowText(LoadStg(IDS_STRING999));
	m_Combo_Fitting_Shifts.SetWindowText(LoadStg(IDS_STRING63462));

	// default einstellen
	m_Combo_Fitting_Shifts.SetCheck(1);

	// Grid initialisieren
	InitGrids();
	SortGrid(pomBsdGrid,1);
	SortGrid(pomOdaGrid,1);

	SetDlgItemText(IDC_STATIC_TITLE_BSD_GRID,LoadStg(IDS_STRING972));
	SetDlgItemText(IDC_STATIC_TITLE_ODA_GRID,LoadStg(IDS_STRING401));

	return TRUE;  
}

//**********************************************************************************
//
//**********************************************************************************

void CSelectShiftDlg::OnOK() 
{
CCS_TRY;
	CString			olUrno;
	CRowColArray	olRowColArrayBsd,olRowColArrayOda;
	CGXStyle		olStyle;

	// Aktuelle Auswahl ermitteln und zur�ckgeben
	// Selektion ausw�hlen
	pomBsdGrid->GetSelectedRows(olRowColArrayBsd);
	pomOdaGrid->GetSelectedRows(olRowColArrayOda);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArrayOda.GetSize() == 0 && olRowColArrayBsd.GetSize() == 0)
	{
		// Message an den Benutzer
		MessageBox(LoadStg(IDS_STRING973),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}

	if (imCodeTypeSelected == CODE_IS_BSD)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomBsdGrid->ComposeStyleRowCol(olRowColArrayBsd[0],1, &olStyle );
		// Zeiger auf Datensatz erhalten
		pomBsd = (BSDDATA*)olStyle.GetItemDataPtr();

		// g�ltige Auswahl?
		if (pomBsd == NULL)
		{
			// Message an den Benutzer
			MessageBox(LoadStg(IDS_STRING973),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			return;
		}
		
		lmCodeUrnoSelected = pomBsd->Urno;
	}
	else if (imCodeTypeSelected == CODE_IS_ODA)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomOdaGrid->ComposeStyleRowCol(olRowColArrayOda[0],1, &olStyle );
		// Zeiger auf Datensatz erhalten
		pomOda = (ODADATA*)olStyle.GetItemDataPtr();

		// g�ltige Auswahl?
		if (pomOda == NULL)
		{
			// Message an den Benutzer
			MessageBox(LoadStg(IDS_STRING973),LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
			return;
		}

		lmCodeUrnoSelected = pomOda->Urno;
	}

	CDialog::OnOK();
CCS_CATCH_ALL;
}

#define SELSHIFT_MINROWS 20

//**********************************************************************************
// InitBsdGrid	: initialisiert das Grid mit den Basisschichten
// R�ckgabe		: keine
//**********************************************************************************

void CSelectShiftDlg::InitGrids()
{
CCS_TRY;
	// Warte Cursor einblenden
	CWaitCursor olDummy;

	// Grid erzeugen
	pomBsdGrid = new CGridControl(this,IDC_GRID_BSD,10,1);
	pomOdaGrid = new CGridControl(this,IDC_GRID_ODA,2,ogOdaData.omData.GetSize());

	// Grid initialisieren
	pomBsdGrid->Initialize();
	pomOdaGrid->Initialize();

	// vertikale Scrollbar aktivieren
	pomBsdGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);
	pomOdaGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);

	// Tooltips aktivieren
	pomBsdGrid->EnableGridToolTips();
	CGXStylesMap *olStylesmap = pomBsdGrid->GetParam()->GetStylesMap();
	olStylesmap->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	pomOdaGrid->EnableGridToolTips();
	olStylesmap = pomOdaGrid->GetParam()->GetStylesMap();
	olStylesmap->AddUserAttribute(GX_IDS_UA_TOOLTIPTEXT, CGXStyle().SetWrapText(TRUE).SetAutoSize(TRUE));

	// Anzahl und Breite der Spalten/Zeilen einstellen
	InitColWidths ();

	// Tooltip-Texte einstellen
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1915)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1931)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(3), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1913)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(4), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1914)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1932)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(6), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1898)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(7), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1899)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(8), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1900)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(9), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1933)));	
	pomBsdGrid->SetStyleRange(CGXRange().SetCols(10), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING1934)));	

	// Tooltip-Texte der Spalten-Header einstellen
	pomOdaGrid->SetStyleRange(CGXRange().SetCols(1), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING390)));	
	pomOdaGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, LoadStg(IDS_STRING395)));	
	
	// Grid mit Daten f�llen.
	FillBsdGrid();
	FillOdaGrid();

	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomBsdGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	pomBsdGrid->GetParam()->EnableSelection(GX_SELROW);

	pomBsdGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomBsdGrid->EnableAutoGrow ( false );

	// DoppelClick Acktion festlegen
	pomBsdGrid->SetLbDblClickAction ( WM_COMMAND, IDOK);
	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomOdaGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	pomOdaGrid->GetParam()->EnableSelection(GX_SELROW);

	pomOdaGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC)
				);
	pomOdaGrid->EnableAutoGrow ( false );
	// DoppelClick Acktion festlegen
	pomOdaGrid->SetLbDblClickAction ( WM_COMMAND, IDOK);

CCS_CATCH_ALL;
}

//**********************************************************************************
// InitBsdColWidths	: stellt die Breiten der Spalten des BSD-Grids ein.
// R�ckgabe			: keine
//**********************************************************************************

void CSelectShiftDlg::InitColWidths()
{
CCS_TRY;
	// Spaltenbreiten einstellen
	pomBsdGrid->SetColWidth ( 0, 0, 30);
	pomBsdGrid->SetColWidth ( 1, 1, 65);
	pomBsdGrid->SetColWidth ( 2, 2, 66);
	pomBsdGrid->SetColWidth ( 3, 3, 50);
	pomBsdGrid->SetColWidth ( 4, 4, 50);
	pomBsdGrid->SetColWidth ( 5, 5, 67);
	pomBsdGrid->SetColWidth ( 6, 6, 67);
	pomBsdGrid->SetColWidth ( 7, 7, 67);
	pomBsdGrid->SetColWidth ( 8, 8, 50);
	pomBsdGrid->SetColWidth ( 9, 9, 67);
	pomBsdGrid->SetColWidth ( 10, 10, 67);

	// Spaltenbreiten einstellen
	pomOdaGrid->SetColWidth ( 0, 0, 30);
	pomOdaGrid->SetColWidth ( 1, 1, 50);
	pomOdaGrid->SetColWidth ( 2, 2, 140);

	// �berschriften setzen
	pomBsdGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING974));
	pomBsdGrid->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1931));
	pomBsdGrid->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING975));
	pomBsdGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING976));
	pomBsdGrid->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING1932));
	pomBsdGrid->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING977));
	pomBsdGrid->SetValueRange(CGXRange(0,7), LoadStg(IDS_STRING978));
	pomBsdGrid->SetValueRange(CGXRange(0,8), LoadStg(IDS_STRING979));
	pomBsdGrid->SetValueRange(CGXRange(0,9), LoadStg(IDS_STRING1933));
	pomBsdGrid->SetValueRange(CGXRange(0,10), LoadStg(IDS_STRING1934));

	// �berschriften setzen
	pomOdaGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1620));
	pomOdaGrid->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING461));

CCS_CATCH_ALL;
}


//**********************************************************************************
// FillOdaGrid: f�llt das ODA-Grid mit Daten.
// R�ckgabe		: keine
//**********************************************************************************

void CSelectShiftDlg::FillOdaGrid()
{
	CWaitCursor olDummy;

	pomOdaGrid->LockUpdate(TRUE);

	int ilNotDisplayedAbsences = 0;

	for (int i = 0; i < ogOdaData.omData.GetSize(); i++)
	{
		ODADATA *prlOda = &ogOdaData.omData[i];

		// insert absence only if it is not 'regular free' and no 'sleepday'
		if (!strcmp(prlOda->Free, "x") || !strcmp(prlOda->Type, "S"))
		{
			ilNotDisplayedAbsences++;
			pomOdaGrid->RemoveRows(pomOdaGrid->GetRowCount(), pomOdaGrid->GetRowCount());
		}
		else
		{
			// Urno mit erstem Feld koppeln.						
			pomOdaGrid->SetStyleRange (CGXRange(i+1-ilNotDisplayedAbsences,1), CGXStyle().SetItemDataPtr((void*)prlOda));

			// Datenfelder f�llen
			pomOdaGrid->SetValueRange(CGXRange(i+1-ilNotDisplayedAbsences,1),	prlOda->Sdac);
			pomOdaGrid->SetValueRange(CGXRange(i+1-ilNotDisplayedAbsences,2),	prlOda->Sdan);
		}
	}

	// Anzeige wieder anstellen
	pomOdaGrid->LockUpdate(false);
	// Neu zeichnen.
	pomOdaGrid->Redraw();
}


//**********************************************************************************
// FillBsdGrid	: f�llt das BSD-Grid mit Daten. Wenn <pomDrr> nicht NULL ist, 
// werden nur die Basisschichten ins Grid mit aufgenommen, die f�r den Tag
// <pomDrr->Sday> g�ltig sind und sich nicht mit der Anwesenheitszeit im
// DRR �berschneiden (ausser der <pomDrr->Scod> ist ein Abwesenheitscode).
// R�ckgabe		: keine
//**********************************************************************************

void CSelectShiftDlg::FillBsdGrid()
{
CCS_TRY;
	CWaitCursor olDummy;

	ROWCOL ilRowCount;
	BSDDATA * polBsd;
	CCSPtrArray<DRRDATA> olDrrList;

	// soll nach Organisationseinheiten (eingestellt in Registerkarte Statistik)
	// gefiltert werden?
	CString olOrgString = "";
	bool	blAddShift	= false;
	if (prmViewInfo != NULL)
	{
		olOrgString = prmViewInfo->oOrgStatDpt1s;
	}

	// Neuzeichnen pro Row verhindern
	pomBsdGrid->LockUpdate(true);
	// Readonly ausschalten
	pomBsdGrid->GetParam()->SetLockReadOnly(false);

	// Anzahl der Rows ermitteln
	ilRowCount = pomBsdGrid->GetRowCount();
	if (ilRowCount > 0)
	{
		// Grid l�schen			
		pomBsdGrid->RemoveRows(1, ilRowCount);
	}

	// g�ltiger DRR?
	if (pomDrr != NULL)
	{
		// dann die Liste mit den DRRs f�r diesen MA/Planungstyp/Tag ermitteln
		ogDrrData.GetDrrListByKeyWithoutDrrn(CString(pomDrr->Sday),pomDrr->Stfu,CString(pomDrr->Rosl),&olDrrList);
	}

	// alle BSD-Datens�tze durchgehen
	for(int ilCount = 0; ilCount < ogBsdData.omData.GetSize(); ilCount++)
	{
		// Datensatz ermitteln
		polBsd = &ogBsdData.omData[ilCount];

		// Werte pr�fen, ob einf�gen
		if (m_Combo_Fitting_Shifts.GetCheck() == 0)
		{
			blAddShift = true;
		}
		else
		{
			blAddShift = false;
		}
		if (blAddShift == false)
		{
			if (ogDrrData.CheckBsd (polBsd, olDrrList, olOrgString, CString(pomDrr->Sday), !(prmViewInfo->bAllCodes)) == true)
			{
				blAddShift = true;
			}
		}

		// Schicht zur Auswahl stellen
		if (blAddShift == true)
		{
			// Neue Row einf�gen
			ilRowCount = pomBsdGrid->GetRowCount();
			pomBsdGrid->InsertRows(ilRowCount+1,1);
			// Urno mit erstem Feld koppeln.						
			pomBsdGrid->SetStyleRange (CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr((void*)polBsd));
			//----------------------------------------------------------------
			// 1.) Code
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,1),polBsd->Bsdc);
			//----------------------------------------------------------------
			// 2.) Funktion 1
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,2),polBsd->Fctc);
			//----------------------------------------------------------------
			// 3.) fr�hester Schichtbeginn
			CString olEsbg(polBsd->Esbg);
			olEsbg = olEsbg.Left(2) + ":" + olEsbg.Right(2);
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,3),olEsbg);
			//----------------------------------------------------------------
			// 4.) sp�testes Schichtende
			CString olLsen(polBsd->Lsen);
			olLsen = olLsen.Left(2) + ":" + olLsen.Right(2);
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,4),olLsen);
			//----------------------------------------------------------------
			// 5.) regul�re Dauer
			COleDateTimeSpan olMyTime(0,0, atoi(polBsd->Sdu1),0 );
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,5),olMyTime.Format("%H:%M"));
			//----------------------------------------------------------------
			// 6.) Pause von
			CString olBkf1(polBsd->Bkf1);
			if (!olBkf1.IsEmpty())
			{
				olBkf1 = olBkf1.Left(2) + ":" + olBkf1.Right(2);
				pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,6),olBkf1);
			}
			//----------------------------------------------------------------
			// 7.) Pause bis
			CString olBkt1(polBsd->Bkt1);
			if (!olBkt1.IsEmpty())
			{
				olBkt1 = olBkt1.Left(2) + ":" + olBkt1.Right(2);
				pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,7),olBkt1);
			}
			//----------------------------------------------------------------
			// 8.) Pausenl�nge
			pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,8),polBsd->Bkd1);
			//----------------------------------------------------------------
			// 9 und 10.) Funktionen 2 und 3
			// verbundene DELs bekommen
			CCSPtrArray<DELDATA> olDelList;
			int ilDelNum = ogDelData.GetDelListByBsdu(polBsd->Urno, false, &olDelList);
			ilDelNum = min(ilDelNum, 2);	// z.Zt. nur 2 Funktionen darstellbar

			for(int ilCount=0; ilCount<ilDelNum; ilCount++)
			{
				DELDATA* polDel = (DELDATA*)&olDelList[ilCount];
				pomBsdGrid->SetValueRange(CGXRange(ilRowCount+1,9+ilCount),polDel->Fctc);
			}
		}
	}
	// Readonly einschalten
	pomBsdGrid->GetParam()->SetLockReadOnly(true);
	// Neuzeichnen pro Row wieder einschalten
	pomBsdGrid->LockUpdate(false);
	// Neuzeichnen
	pomBsdGrid->Redraw();

CCS_CATCH_ALL;
}

//**********************************************************************************
// SortBsdGrid: sortiert die Eintr�ge im Basisschichten auf- oder absteigend.
// R�ckgabe:	immer true, es sei denn eine Exception wurde ausgel�st
//**********************************************************************************

bool CSelectShiftDlg::SortGrid(CGridControl* popGrid,int ipRow)
{
CCS_TRY;
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize( 1 );
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	popGrid->SortRows(CGXRange().SetRows(1,popGrid->GetRowCount()),sortInfo); 
	return (true);
CCS_CATCH_ALL;
return false;
}

//**********************************************************************************
// OnGridLButton: EventHandler Left Button Click in das Grid
// R�ckgabe		: immer 0L
//**********************************************************************************

LONG CSelectShiftDlg::OnGridLButton(WPARAM wParam, LPARAM lParam)
{
CCS_TRY;
	GRIDNOTIFY* rlNotify = (GRIDNOTIFY*) lParam;

	// Bisherige Auswahl l�schen.
	pomBsdGrid->SetSelection(NULL);
	pomOdaGrid->SetSelection(NULL);

	// In welches Grid wurde geklickt
	if (rlNotify->source == pomBsdGrid)
	{
		imCodeTypeSelected = CODE_IS_BSD;
		// neue Auswahl markieren
		POSITION area = pomBsdGrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
		pomBsdGrid->SetSelection(area,rlNotify->row,0,rlNotify->row,2);
	}
	else
	{
		imCodeTypeSelected = CODE_IS_ODA;
		// neue Auswahl markieren
		POSITION area = pomOdaGrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
		pomOdaGrid->SetSelection(area,rlNotify->row,0,rlNotify->row,2);
	}

	return 0L;

CCS_CATCH_ALL;
return 0L;
}

//*******************************************************************************************************************
// OnButtonFindScod	: Event-Handler f�r Button 'Suchen'. Sucht einen Text im Grid.
// R�ckgabe			: keine
//*******************************************************************************************************************

void CSelectShiftDlg::OnButtonFindScod() 
{
	if (pomBsdGrid)
	{
		pomBsdGrid->OnShowFindReplaceDialog(TRUE);
	}
}

void CSelectShiftDlg::OnButtonFindOda() 
{
	if (pomOdaGrid)
	{
		pomOdaGrid->OnShowFindReplaceDialog(TRUE);
	}
}

void CSelectShiftDlg::OnButtonFittingShifts()
{
	FillBsdGrid();
	SortGrid(pomBsdGrid,1);
}