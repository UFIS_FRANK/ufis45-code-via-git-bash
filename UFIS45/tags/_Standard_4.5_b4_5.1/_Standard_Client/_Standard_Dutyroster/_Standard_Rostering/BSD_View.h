#if !defined(AFX_BSD_VIEW_H__74D50E04_82DA_11D3_8F9D_00500454BF3F__INCLUDED_)
#define AFX_BSD_VIEW_H__74D50E04_82DA_11D3_8F9D_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BSD_View.h : Header-Datei


#include <stdafx.h>
#include <resource.h>
#include <DutyRoster_View.h>
#include <ShiftRoster_View.h>
#include <Table.h>
#include <CedaBsdData.h>
#include <CedaOdaData.h>
#include <basicdata.h>
//#include "CCSDragDropCtrl.h"

// Angabe, welche Klasse (Shift- oder DutyRosterView) den
// Dialog erzeugt hat
enum
{
	DUTYROSTER_BSD_DLG  = 101,
	SHIFTROSTER_BSD_DLG = 102
};

/////////////////////////////////////////////////////////////////////////////
// Formularansicht BSD_View 

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif


/////////////////////////////////////////////////////////////////////////////
// View BsdView
// Der View zeigt alle Schichten und Abwesenheiten an. Die Schicht-
// bezeichner lassen sich per Drag and Drop in die Fenster der aufrufenden
// Klassen ziehen (sofern diese Drag and Drop unterst�tzen).

class BSD_View : public CFormView
{
protected:
	BSD_View();           // Dynamische Erstellung verwendet gesch�tzten Konstruktor
	DECLARE_DYNCREATE(BSD_View)

// Formulardaten
public:
	//{{AFX_DATA(BSD_View)
	enum { IDD = IDD_BSD_VIEW };
		// HINWEIS: Der Klassen-Assistent f�gt hier Datenelemente ein
	//}}AFX_DATA

// Attribute
public:

private:
	// Typ der Klasse, die den Dialog erzeugt hat
	// (Shift- oder DutyRosterView)
	//UINT imOpenFrom;

// Operationen
public:

	CString Format_Table(CString opTable,void *prpData);
	void InternalCancel();

	// Parent Window (Shift- oder DutyRosterView)
	//CWnd *pomParent;
	// Arrays, die die Strings halten. Die Strings repr�sentieren
	// die verf�gbaren Datens�tze f�r Basisschichten und Abwesenheiten
	CCSPtrArray<BSDDATA>  omBsdLines;
	CCSPtrArray<ODADATA>  omOdaLines;
	// mit Hilfe der CTable-Objekte werden die Inhalte der Arrays f�r
	// Basisschichten und Abwesenheiten dargestellt und Events wie 
	// Mausklicks behandelt (CTable h�lt ein Datenobjekt CTableListBox).
	CTable *pomBsdTable;
	CTable *pomOdaTable;
	// Objekt f�r D'n'D-Funktionalit�t
	CCSDragDropCtrl omDragDropObject;


// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(BSD_View)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	virtual ~BSD_View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(BSD_View)
		afx_msg LONG OnDragBegin(UINT wParam, LONG lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_BSD_VIEW_H__74D50E04_82DA_11D3_8F9D_00500454BF3F__INCLUDED_
