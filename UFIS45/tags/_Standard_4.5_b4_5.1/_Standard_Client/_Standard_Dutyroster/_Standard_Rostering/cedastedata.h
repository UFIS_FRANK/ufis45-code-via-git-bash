// CedaSteData.h

#ifndef __CEDAPSTEDATA__
#define __CEDAPSTEDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct STEDATA 
{
	long			Urno;
	long			Surn;
	char			Code[7];
	COleDateTime	Vpfr;
	COleDateTime	Vpto;

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	STEDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end STEDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSteData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<STEDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// OSteations
public:
    CedaSteData();
	~CedaSteData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(STEDATA *prpSte);
	bool InsertInternal(STEDATA *prpSte);
	bool Update(STEDATA *prpSte);
	bool UpdateInternal(STEDATA *prpSte);
	bool Delete(long lpUrno);
	bool DeleteInternal(STEDATA *prpSte);
	bool ReadStecialData(CCSPtrArray<STEDATA> *popSte,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(STEDATA *prpSte);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	STEDATA  *GetSteByUrno(long lpUrno);
	void GetSteBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<STEDATA> *popSteData);
	CString GetTeaBySurnWithTime(long lpSurn, COleDateTime opDate);
	CString GetSteByTeaWithTime(CStringArray *popTea,COleDateTime opStart,COleDateTime opEnd,CCSPtrArray<STEDATA> *popSteData);


	// Private methods
private:
    void PrepareSteData(STEDATA *prpSteData);

};

//---------------------------------------------------------------------------------------------------------

extern CedaSteData ogSteData;

#endif //__CEDAPSTEDATA__
