// CedaSpfData.h

#ifndef __CEDAPSPFDATA__
#define __CEDAPSPFDATA__
 
#include <stdafx.h>
#include <CedaStfData.h>
#include <basicdata.h>
#include <afxdisp.h>

/***********************************************************************************
SPFDATA
verbindet einen Mitarbeiter mit einer Funktion:
f�r die angegebene Zeit (wenn angegeben) gilt f�r den Mitarbeiter 'Surn' 
die Funktion 'Fctc', mit dem Priorit�t 'Prio'
***********************************************************************************/

struct SPFDATA 
{
	long			Urno;				// URNO dieses Datensatzes
	long			Surn;				// URNO des zugeh�rigen Mitarbeiters
	char			Fctc[FCTC_LEN+2];	// FCTC - Funktionscode aus PFC.FCTC
	char			Prio[PRIO_LEN+2];	// Priorit�t der Funktion, = 1 - Stammfunktion
	COleDateTime	Vpfr;
	COleDateTime	Vpto;

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SPFDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SPFDataStruct

#ifndef _DEBUG
#define TraceSpf(popSpf, popStr)		false
#else
// globale Debug Funktion
bool TraceSpf(SPFDATA *popSpf, LPCTSTR popStr);
#endif _DEBUG

   /***********************************************************************************
***********************************************************************************/

class CedaSpfData: public CedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SPFDATA> omData;

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

public:
    CedaSpfData();
	~CedaSpfData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SPFDATA *prpSpf);
	bool InsertInternal(SPFDATA *prpSpf);
	bool Update(SPFDATA *prpSpf);
	bool UpdateInternal(SPFDATA *prpSpf);
	bool Delete(long lpUrno);
	bool DeleteInternal(SPFDATA *prpSpf);
	bool ReadSpecialData(CCSPtrArray<SPFDATA> *popSpf,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SPFDATA *prpSpf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SPFDATA *GetSpfByUrno(long lpUrno);
	CheckValidSpfData();
private:
	int FindFirstOfSurn(long lpSurn);
public:
	CString GetMainPfcBySurnWithTime(long lpSurn, COleDateTime opDate);
	CString FormatFuncs(CCSPtrArray<SPFDATA> &opSpfData, int ipStartPrio);
	bool ComparePfcBySurnWithTime(CString opPfc, long lpSurn, COleDateTime opDate, bool bpMainFuncOnly);
	int GetSpfArrayBySurnWithTime(CCSPtrArray<SPFDATA> &opSpfData, long lpSurn, COleDateTime opDate, bool bpMainFuncOnly);
	void GetStfuByPfcListWithTime(CMapStringToPtr& opPfcMap, COleDateTime opStart, COleDateTime opEnd, CMapPtrToPtr& opStfUrnoMap, bool bpSelectAllFunc, bool bpGetAllMAWithoutFunc);

	// Private methods
private:
    void PrepareSpfData(SPFDATA *prpSpfData);

};

//---------------------------------------------------------------------------------------------------------

extern CedaSpfData ogSpfData;

#endif //__CEDAPSPFDATA__
