#ifndef _CEDASPRDATA_H_
#define _CEDASPRDATA_H_
 
#include <stdafx.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>

///////////////////////////////////////////////////////////////////////////// 

// COMMANDS FOR MEMBER-FUNCTIONS
#define SPR_SEND_DDX	(true)
#define SPR_NO_SEND_DDX	(false)

// Struktur eines SPR-Datensatzes
struct SPRDATA 
{
	/*
	ACTI      CHAR(14)	Zeiteintrag aus dem BU-Telegramm (BU = Buchung, ST-Diagramme werden ignoriert)
	//CDAT      CHAR(14)	Aktuelles Datum/Uhrzeit
	FCOL      CHAR(1)	Kommen/Gehen-Flag (0 � gehen, 1 � kommen)evtl. 2 � PauseBeginn, 3� PauseEnde
	//HOPO      CHAR(3)	Home-Airport
	//LSTU      CHAR(14)	Aktuelles Datum/Uhrzeit
	//PKNO      CHAR(20)	PENO des Mitarbeiters
	URNO      CHAR(10)	Unique record number
	//USEC      CHAR(32)	azehdl
	//USEU      CHAR(32)	azehdl
	USTF      CHAR(10)	URNO des Mitarbeiters aus der STFTAB
	*/

	COleDateTime	Acti;			// Zeiteintrag
	char			Fcol[1+2];		// Kommen/Gehen-Flag 0 gehen, 1 kommen, 2 PauseBeginn, 3 PauseEnde
	long			Urno;			// Unique record number
	long			Ustf;			// URNO des Mitarbeiters aus der STFTAB
	COleDateTime	Lstu;			// last update


	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Release
	SPRDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		Acti.SetStatus(COleDateTime::invalid);
		Lstu.SetStatus(COleDateTime::invalid);
	}
};	

// the broadcast CallBack function, has to be outside the CedaSprData class
void ProcessSprCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaSprData: kapselt den Zugriff auf die Tabelle SPR
//************************************************************************************************************************
//************************************************************************************************************************

class CedaSprData: public CedaData
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaSprData(CString opTableName = "SPR", CString opExtName = "TAB");
	~CedaSprData();

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// Zeiten holen
	//void GetShiftTimes(COleDateTime& opFrom, COleDateTime& opTo, COleDateTime& opSbfr, COleDateTime& opSbto, CString opSday, long lpUstf);
	void GetShiftTimes(COleDateTime& opFrom, COleDateTime& opTo, COleDateTime& opSbfr, COleDateTime& opSbto, COleDateTime opAVFR, COleDateTime opAVTO, long lpUstf)	;
	// true wenn eine der Zeiten sich um mindestens 5 Minuten unterscheidet
	bool AreTimesDifferent(DRRDATA* popDrr);

	// SPRDATA kopieren
	void CopySpr(SPRDATA* popDst, SPRDATA* popSrc);
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

// Broadcasts empfangen und bearbeiten
	// BC_SPR_NEW, BC_SPR_CHANGE und BC_SPR_DELETE behandeln
	void ProcessSprBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Kommunikation mit CEDA
	// Kommandos an CEDA senden
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze einlesen
	bool Read(char *pspWhere, CMapPtrToPtr *popLoadStfUrnoMap = NULL, bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit Urno <lpUrno> lesen
	bool ReadSprByUrno(long lpUrno, SPRDATA *prpSpr);
	// Datens�tze nach Datum gefiltert lesen
	bool ReadFilteredByDateAndStaff(COleDateTime opStart, COleDateTime opEnd,
									CMapPtrToPtr *popLoadStfUrnoMap = NULL,
									bool bpRegisterBC = true, bool bpClearData = true);
	// alle SPRs eines Mitarbeiters aus der internen Datenhaltung entfernen (ohne aus der DB zu l�schen!!!)
	bool RemoveInternalByStaffUrno(long lpStfUrno, bool bpSendBC = false);
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(SPRDATA *prpSpr, BOOL bpWithSave);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(SPRDATA *prpSpr, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(SPRDATA *prpSpr);
	// liefert den Zeiger auf die interne Datenhaltung <omData>
	SPRDATA*	GetInternalData (int ipIndex) {return (SPRDATA*)omData.CPtrArray::GetAt(ipIndex);}
	// liefert die Anzahl der Datens�tze in der internen Datenhaltung <omData>
	int	GetInternalSize() {return omData.GetSize();}

// Datenfelder behandeln
	// IsValidSpr: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten SPRs
	bool IsValidSpr(SPRDATA *popSpr);
	
// Datens�tze suchen
	// SPR nach Schl�ssel (SPRN/ROSL/SDAY/STFU) suchen
	//SPRDATA *GetSprByKey(CString opSday, long lpUstf, CString opFcol);
	SPRDATA *GetSprByKey(long lpUstf, const char *pcpFcol, COleDateTime opShiftStart, COleDateTime opShiftEnd);
	// SPR mit Urno <lpUrno> suchen
	SPRDATA* GetSprByUrno(long llUrno);

// Datensatz-bezogene Funktionen
// Datenfelder manipulieren
	// Anwender, Zeit und Flag der letzten �nderung setzen
	void SetSprDataLastUpdate(SPRDATA *popSprData);

// Hilfsfunktionen zur Manipualtion von Objekten vom Typ SPRDATA und Feldkonvertierung

	// Made by RDR
	SPRDATA* pomLastInternChangedSpr;

	// bda: makes a formatted string with all fields of a data structure for trace and debug output
	bool GetDataFormatted(CString &pcpListOfData, SPRDATA *prpSpr); 

	// Debug Funktion
	bool TraceSpr(SPRDATA *popSpr, LPCTSTR popStr);

protected:	
// Funktionen
// Lesen/Schreiben von Datens�tzen
	// speichert einen einzelnen Datensatz
	bool Save(SPRDATA *prpSpr);
	// einen Broadcast SPR_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(SPRDATA *prpSpr, bool bpSendDdx);
	// einen Broadcast SPR_CHANGE versenden
	bool UpdateInternal(SPRDATA *prpSpr, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(SPRDATA *prpSpr, bool bpSendDdx = true);
	void ConvertDatesToLocal(SPRDATA *prpSpr);
	
// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);
// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Start- und Endtag f�r zu ladende Datens�tze (SPR->SDAY) einstellen, Daten vorher pr�fen
	bool CheckAndSetLoadPeriod(COleDateTime *popLoadFrom,COleDateTime *popLoadTo);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
// Datenfelder manipulieren
	// Erstellt aus dem Selection-String den Urno-String und findet SPR
	SPRDATA* GetSprFromSelectionStringUrno(CString opSelection);

// Daten
    // die geladenen Datens�tze
	CCSPtrArray<SPRDATA> omData;
	// Filter: wenn nicht NULL, enth�lt diese Map die Urnos der Mitarbeiter, f�r die 
	// Daten geladen werden 
	CMapPtrToPtr *pomLoadStfuUrnoMap;
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
    // Map mit den Prim�rschl�sseln (aus SDAY, SPRN, STFU und ROSL) der Datens�tze
	//CMapStringToPtr omKeyMap;
	// mapping the clocking system times of the employees
	CMapPtrToPtr omUstfMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten SPRs
	COleDateTime omMinDay;
	COleDateTime omMaxDay;
};

#endif	// _CEDASPRDATA_H_
