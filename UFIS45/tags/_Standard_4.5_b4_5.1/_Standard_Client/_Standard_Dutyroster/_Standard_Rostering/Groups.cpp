// Groups.cpp: implementation of the CGroups class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//static int CompareGroupFunc( const GROUPSTRUCT **e1, const GROUPSTRUCT **e2);
//static int CompareGroupByTimeFunc( const GROUPSTRUCT **e1, const GROUPSTRUCT **e2);

CGroups::~CGroups()
{
	DeleteAll();
}


/*****************************************************************************
Sieh CCSPtrArray::Sort
*****************************************************************************/
void CGroups::Sort(int ipCompareMethod)
{
	switch(ipCompareMethod)
	{
	default:
		break;
	case CompareGroupAbc:
		//CCSPtrArray::Sort(CompareGroupFunc);
		break;
	case CompareGroupByTime:
		//CCSPtrArray::Sort(CompareGroupByTimeFunc);
		break;
	}
}

//****************************************************************************
// CompareGroupFunc: vergleicht zwei Arbeitsgruppen nach Code, bei 
//	Gleichheit des Codes nach Priorit�t und schlie�lich nach
//	Schichtzeit.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

/*static int CompareGroupFunc( const GROUPSTRUCT **e1, const GROUPSTRUCT **e2)
{
	int ilCompareResult = 0;
	CString ole1,ole2;
	int ile1,ile2;
	CTime olet1,olet2;

	ole1 = (**e1).oWgpCode;
	ole2 = (**e2).oWgpCode;
	ilCompareResult = ole1.Collate(ole2);

	if(ilCompareResult == 0)
	{
		ile1 = (**e1).iGroupPrio;
		ile2 = (**e2).iGroupPrio;
		if(ile1 < ile2) return -1;
		else if(ile1 > ile2) return 1;
	}

	if(ilCompareResult == 0)
	{
		olet1 = (**e1).oShiftTime;
		olet2 = (**e2).oShiftTime;
			 if(olet1 < olet2) return -1;
		else if(olet1 > olet2) return 1;
	}

	return ilCompareResult;
}*/

//****************************************************************************
// CompareGroupByTimeFunc: vergleicht zwei Arbeitsgruppen nach Zeit, bei 
//	Gleichheit der Zeit nach Code.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************
/*
static int CompareGroupByTimeFunc( const GROUPSTRUCT **e1, const GROUPSTRUCT **e2)
{
	int ilCompareResult = 0;
	CTime olet1,olet2;
	CString ole1,ole2;

	olet1 = (**e1).oShiftTime;
	olet2 = (**e2).oShiftTime;
	if(olet1 < olet2) return -1;
	else if(olet1 > olet2) return 1;

	if(ilCompareResult == 0)
	{
		ole1 = (**e1).oWgpCode;
		ole2 = (**e2).oWgpCode;
		ilCompareResult = ole1.Collate(ole2);
	}

	return ilCompareResult;
}*/

/**********************************************************************************************
Parameter prpChangeInfo1, prpChangeInfo2 beinhalten einzusetzende Informationen f�r jeden
MA.
ChangeGroup() vergleicht diese Informationen mit vorhandenen und wenn keine gefunden, erstellt und
speichert neue DRGs

die Parameter k�nnen auch 0 sein
bool bpCheckMainFuncOnly - true - Schichtzuordnung nur f�r Stammfunktionen pr�fen, sonst f�r alle
**********************************************************************************************/
void ChangeGroup(GROUPCHANGEINFO *prpChangeInfo1, GROUPCHANGEINFO *prpChangeInfo2, bool bpCheckMainFuncOnly) 
{
CCS_TRY;
	AfxGetApp()->DoWaitCursor(1);

	// das Info1 wird sofort zugewiesen, das 2. kommt nach der ersten for-Runde
	GROUPCHANGEINFO *prlGroupChangeInfo = prpChangeInfo1;
	// Liste f�r die neue DRGs
	CCSPtrArray<DRGDATA>  olDrgList;
	
	DRGDATA *prlDrg;
	bool bEqualToStaff;

	for(int i=0; i<2; i++)
	{
		bEqualToStaff = true;
		if(prlGroupChangeInfo)
		{
			
			CTime olDay = COleDateTimeToCTime(prlGroupChangeInfo->oDay);
			//int ilDay = prlGroupChangeInfo->oDay.GetDay()-1;
			CString olSday = prlGroupChangeInfo->oDay.Format("%Y%m%d");
			
			// Stimmen die neuen Daten mit Stammdaten �berein?
			
			// Abweichung von Funktion laut Stammdaten pr�fen
			if (!ogSpfData.ComparePfcBySurnWithTime(prlGroupChangeInfo->oFctc,prlGroupChangeInfo->lStfu, prlGroupChangeInfo->oDay, bpCheckMainFuncOnly))
			{
				// Funktion weicht ab
				bEqualToStaff = false;
			}
			// Abweichung von Arbeitsgruppe laut Stammdaten pr�fen
			if (bEqualToStaff && ogSwgData.GetWgpBySurnWithTime(prlGroupChangeInfo->lStfu, prlGroupChangeInfo->oDay) != 
				prlGroupChangeInfo->oWgpc)
			{
				// Arbeitsgruppe weicht ab
				bEqualToStaff = false;
			}
			
			// Ob f�r diesen MA an dem Tag schon ein DRG vorhanden ist?
			prlDrg = ogDrgData.GetDrgBySdayAndStfu(olSday, prlGroupChangeInfo->lStfu);
			
			if(!prlDrg && !bEqualToStaff)
			{
				// kein DRG gefunden, neu erstellen
				prlDrg = ogDrgData.CreateDrgData(prlGroupChangeInfo->lStfu, olSday, prlGroupChangeInfo->oDrrn);
				if(!prlDrg)
					continue;	// immer noch kein prlDrg, hier ist etwas faul..;(
				
				strncpy(prlDrg->Wgpc, prlGroupChangeInfo->oWgpc, WGPC_LEN); // WGPC - Arbeitsgruppencode aus WGP
				strncpy(prlDrg->Fctc, prlGroupChangeInfo->oFctc, FCTC_LEN);	// FCTC - Funktionscode aus PFC
				strncpy(prlDrg->Drrn, prlGroupChangeInfo->oDrrn, DRRN_LEN);	
				// die folgende Felder stimmen bereits �berein
				// prlDrg->lStfUrno;		// STFU - Urno des MA
				// prlDrg->oDay;	// SDAY - Schicht-Tag (YYYYMMDD)
				
				TRACE("ChangeGroup(): kein DRG gefunden, neu erstellen, Stfu:%lu\n",prlGroupChangeInfo->lStfu);
				ogDrgData.Insert(prlDrg);
			}
			else if(!prlDrg && bEqualToStaff)
			{
				// kein DRG gefunden und die neue Daten gleichen den Stammdaten, kein Job
				TRACE("ChangeGroup(): kein DRG gefunden und die neue Daten gleichen den Stammdaten, kein Job, Stfu:%lu\n",prlGroupChangeInfo->lStfu);
			}
			else if(prlDrg && !bEqualToStaff)
			{
				// DRG gefunden, die neue Daten weichen von Stammdaten ab
				if(	!strcmp(prlDrg->Wgpc, prlGroupChangeInfo->oWgpc) && 
					!strcmp(prlDrg->Fctc, prlGroupChangeInfo->oFctc) &&
					!strcmp(prlDrg->Drrn, prlGroupChangeInfo->oDrrn))
				{
					TRACE("ChangeGroup(): DRG gefunden, die neuen Daten gleichen der DRG, keine �nderung, Stfu:%lu\n",prlGroupChangeInfo->lStfu);
					// die neuen Daten gleichen der DRG, kein Job
				}
				else
				{
					// die neuen Daten weichen von DRG ab
					strncpy(prlDrg->Wgpc, prlGroupChangeInfo->oWgpc, WGPC_LEN); // WGPC - Arbeitsgruppencode aus WGP
					strncpy(prlDrg->Fctc, prlGroupChangeInfo->oFctc, FCTC_LEN);	// FCTC - Funktionscode aus PFC
					strncpy(prlDrg->Drrn, prlGroupChangeInfo->oDrrn, DRRN_LEN);	
					// die folgende Felder stimmen bereits �berein
					// prlDrg->lStfUrno;		// STFU - Urno des MA
					// prlDrg->oDay;	// SDAY - Schicht-Tag (YYYYMMDD)
					
				TRACE("ChangeGroup(): DRG gefunden, die neue Daten weichen von von DRG ab, DRG �ndern, Stfu:%lu\n",prlGroupChangeInfo->lStfu);
					ogDrgData.Update(prlDrg);
				}
			}
			else if(prlDrg && bEqualToStaff)
			{
				// DRG gefunden, aber neue Daten stimmen mit Stammdaten �berein, DRG l�schen
				TRACE("ChangeGroup(): DRG gefunden, die neue Daten stimmen mit Stammdaten �berein, DRG l�schen, Stfu:%lu\n",prlGroupChangeInfo->lStfu);
				ogDrgData.Delete(prlDrg);
			}


			// Funktionen der Schicht auf die neue Funktion �ndern
			CCSPtrArray<DRRDATA> *polDrrList = new CCSPtrArray<DRRDATA>;
			ogDrrData.GetDrrListByKeyWithoutDrrn(olSday, prlGroupChangeInfo->lStfu, CString("2"), polDrrList);

			for (int ilDrrNo = 0; ilDrrNo < polDrrList->GetSize(); ilDrrNo++)
			{
				DRRDATA* polDrr = (DRRDATA*)polDrrList->CPtrArray::GetAt(ilDrrNo);
				if (polDrr != NULL)
				{
					if (CString(polDrr->Ross) == CString("A"))
					{
						if(CString (polDrr->Fctc) != CString (prlGroupChangeInfo->oFctc))
						{
							strcpy(polDrr->Fctc, prlGroupChangeInfo->oFctc);
							ogDrrData.SetDrrDataLastUpdate(polDrr);
							ogDrrData.Update(polDrr);
						}
					}
				}
			}
			polDrrList->RemoveAll();
			delete polDrrList;

		}
		// das gleiche noch mal f�r den 2. MA
		prlGroupChangeInfo = prpChangeInfo2;
	}

	RemoveWaitCursor();

CCS_CATCH_ALL;
}

