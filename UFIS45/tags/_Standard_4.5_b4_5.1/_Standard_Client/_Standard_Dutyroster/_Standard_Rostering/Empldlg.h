#ifndef AFX_EMPLDLG_H__7891F411_CEC0_11DF_BFCE_004095434A85__INCLUDED_
#define AFX_EMPLDLG_H__7891F411_CEC0_11DF_BFCE_004095434A85__INCLUDED_

// EmplDlg.h : Header-Datei
//

#include <resource.h>
#include <basicdata.h>
#include <ShiftRoster_View.h>




// Auszug aus STF
struct AWSTFVIEWDATA_EMPL
{
	CString	Urno;  	// Eindeutige Datensatz-Nr.
	CString Finm; 	// Vorname
	CString Lanm;	// Name
	CString Makr;	// Mitarbeiterkreis
	CString Perc; 	// Kürzel
	CString Prmc; 	// Funktion
	CString Fgmc; 	// Fahrgemeinschaften
	CString Wgpc; 	// Arbeitsgruppen
	CString Peno;	// Personalnummer
	COleDateTime Dodm; 	// Austrittsdatum
	COleDateTime Doem;	// Eintrittsdatum
	bool Select;	// MA ist in GSP enthalten

	AWSTFVIEWDATA_EMPL(void)
	{
		Select = false;
	}
};
// end AWSTFVIEWDATA_EMPL

// Auszug aus DBO
struct AWDBOVIEWDATA_EMPL
{
	CString Grpn; 	// Beaufschlagungs-Gruppenname (aus GRN)
	CString Valu; 	// Werte der Grupierung (BSD-Urnos aus GRM)
	CString	Urno;  	// Eindeutige Datensatz-Nr. (GRN)
};
// end AWDBOVIEWDATA_EMPL

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld EmplDlg 

class EmplDlg : public CDialog
{
// Konstruktion
public:
	EmplDlg(CStringList* popUrnoList, CString opSelectUrno,CWnd* pParent );
	~EmplDlg(void);
// Dialogfelddaten
	//{{AFX_DATA(EmplDlg)
	enum { IDD = IDD_EMP_DLG };
	CButton			m_B_Ok;
	CButton			m_B_Cancel;
	CButton			m_B_1;
	CButton			m_B_2;
	CButton			m_B_3;
	CButton			m_B_4;
	CButton			m_B_5;
	CButton			m_B_6;
	CButton			m_B_7;
	CButton			m_B_8;
	CButton			m_B_9;
	CButton			m_B_Insert;
	CButton			m_B_Delete;

	CColorListBox	m_LB_List;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(EmplDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL
	
// Implementierung
protected:

	void Sort(int ipSort);
	void DisplayData();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(EmplDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBInsert();
	afx_msg void OnBDelete();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnButton7();
	afx_msg void OnButton8();
	afx_msg void OnButton9();
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	//afx_msg void OnB_1();
	//afx_msg void OnB_2();
	//afx_msg void OnB_3();
	//afx_msg void OnB_4();
	//afx_msg void OnB_5();
	//afx_msg void OnB_6();
	//afx_msg void OnB_7();


public:
	CCSPtrArray<AWSTFVIEWDATA_EMPL>  omStfLines;
	CCSPtrArray<AWDBOVIEWDATA_EMPL>  omDboLines;
	CString omUp;
	CString omDown;
	int imB1;
	int imB2;
	int imB3;
	int imB4;
	int imB5;
	int imB6;
	int imB7;
	int imB8;
	int imB9;
	
protected:
	ShiftRoster_View*	pomParent;
	CString				omSelectUrno;
	CStringList*		pomUrnoList;
	CStringList*		pomLocalUrnoList;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SHIFTAWDLG_H__7891F411_CEC0_11D1_BFCE_004095434A85__INCLUDED_
