#if !defined(AFX_CHARACTERISTICVALUEDLG_H__7E913CC3_E842_11D3_8FF0_0050DA1CABB6__INCLUDED_)
#define AFX_CHARACTERISTICVALUEDLG_H__7E913CC3_E842_11D3_8FF0_0050DA1CABB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CharacteristicValueDlg.h : Header-Datei
//

class CGridControl;

// minimale Anzahl der Zeilen in den Grids
#define GRID_MINROWS	20

#include <dutyroster_view.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CCharacteristicValueDlg 

class CCharacteristicValueDlg : public CDialog
{
// Konstruktion
public:
	CCharacteristicValueDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~CCharacteristicValueDlg();
	
	//uhi 8.5.01
	VIEWINFO *pomViewInfo;
	CCSPtrArray<STFDATA> *pomStfData;
	CAccounts *pomAccounts;

	// nicht-modaler Aufruf
	bool Create(void) {return (CDialog::Create(CCharacteristicValueDlg::IDD,pomParent) != 0);}
	// Grids mit Daten füllen
	void FillGrids(void);
	// Grids leeren
	void ClearGrids(bool bpFillBlank = true);
	// Bearbeitung und Weitergabe der BC`s
	void HandleBc(int ipDDXType, void* vpDataPointer);

// Dialogfelddaten
	//{{AFX_DATA(CCharacteristicValueDlg)
	enum { IDD = IDD_CHARACTERISTIC_VALUES };
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CCharacteristicValueDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Grids
	CGridControl	*pomAccountGrid;
	CGridControl	*pomStatisticGrid;
	// Zeiger auf Parent-Window
	CWnd* pomParent;

	// Stringwert für Statistik Zeile 1 (restliche mögliche Abwesenheiten)
	CString omStatValue;
		
	// Konten-Grid initialisieren
	void InitAccountGrid ();
	// Spalten des Konten-Grids initialisieren
	void InitAccountColWidths();
	// Konten-Grid füllen
	void FillAccountGrid();
	// Statistik-Grid initialisieren
	void InitStatisticGrid ();
	// Spalten des Statistik-Grid initialisieren
	void InitStatisticColWidths();
	// Statistik-Grid füllen
	void FillStatisticGrid();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CCharacteristicValueDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_CHARACTERISTICVALUEDLG_H__7E913CC3_E842_11D3_8FF0_0050DA1CABB6__INCLUDED_
