#if !defined(_DUTYDRAGDROPTABVIEWER_H_INCLUDED_)
#define _DUTYDRAGDROPTABVIEWER_H_INCLUDED_

#include <stdafx.h>
/*#include "CViewer.h"
#include <CCSDynTable.h>
#include <CedaDrgData.h>
#include <CCSPtrArray.h>
#include <DutyDragDropDlg.h>
#include <Groups.h>*/

/////////////////////////////////////////////////////////////////////////////
// DutyDragDropTabViewer

/*struct SELECTEDFIELD
{
	int iRow;
	int iColumn;
	int iRowOffset;
	int iColumnOffset;
	CString oOldText;
	int iOldBkColor;

	SELECTEDFIELD(void)
	{
		iRow          = 0;
		iColumn       = 0;
		iRowOffset    = 0;
		iColumnOffset = 0;
		iOldBkColor	  = 0;
	}
};*/


/////////////////////////////////////////////////////////////////////////////
class DutyDragDropTabViewer : public CViewer
{
// Constructions
public:
    DutyDragDropTabViewer(CWnd* pParent = NULL);
    ~DutyDragDropTabViewer();
	DutyDragDropDlg *pomParent;

    void Attach(CCSDynTable *popAttachWnd);
    virtual void ChangeView(bool bpSetHNull = true, bool bpSetVNull = true);

// Internal data processing routines
private:
	void InitTableArrays(int ipColumnOffset = 0);
	void MakeTableData(int ipColumnOffset,int ipRowOffset);
	void AddRow(int ipRow, int ipColumns, int ipColumnOffset, int ipRowOffset);
	void DelRow(int ipRow);
	void AddColumn(int ipColumn, int ipColumnOffset, int ipRowOffset);
	void DelColumn(int ipColumn);
	void ChangeFieldText(int ipRow, int ipColumn, CString opText, int ipFieldType = _TABLE);
	void ChangeFieldTextColor(int ipRow, int ipColumn, COLORREF opColor, int ipFieldType = _TABLE);
	void ChangeFieldMarkerColor(int ipRow, int ipColumn, int ipColorNr, int ipMarker);
	int ChangeFieldBkColor(int ipRow, int ipColumn, int ipBkColorNr, int ipFieldType = _TABLE);
	int GetFieldBkColor(int ipRow, int ipColumn);
	void ChangeFieldDragDropStatus(int ipRow, int ipColumn, bool bpDrag, bool bpDrop, int ipFieldType = _TABLE);
	int GetDayStrg(int ipDayOffset, CString &opText);

// Operations
public:

// Window refreshing routines
public:
	void HorzTableScroll(MODIFYTAB *prpModiTab);
	void VertTableScroll(MODIFYTAB *prpModiTab);
	void TableSizing(/*UINT nSide, */LPRECT lpRect);
	void MoveTable(MODIFYTAB *prpModiTab);
	void SelectUpdateField(INLINEDATA *prpInlineUpdate);
	void InlineUpdate(INLINEDATA *prpInlineUpdate);
// APO 08.01.2000: Umstellung ROS/DSR auf DRR

// BDA 15.02.2000  Umstellung ROS/DSR auf DRR
	void ProcessDrgChange(DRGDATA *prpDrg);
	void ProcessDrgNew(DRGDATA *prpDrg);
	void ProcessDrrChange(DRRDATA* popDrr);
	void ProcessDrrDelete(DRRDATA* popDrr);
	void ProcessReload();

	bool GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType);


// Attributes
private:
    CCSDynTable *pomDynTab;
	TABLEDATA *prmTableData;
	
	VIEWINFO *prmViewInfo;
	CCSPtrArray<STFDATA> *pomStfData;
	//CCSPtrArray<GROUPSTRUCT> *pomGroups;
	CGroups *pomGroups;

	COleDateTime omFrom;
	COleDateTime omTo;
	COleDateTime omShowFrom;
	COleDateTime omShowTo;
	int imDays;

	CStringArray omPTypes;

	//CCSPtrArray<SELECTEDFIELD> omSelectedField; //for DragDrop

// Methods which handle changes (from Data Distributor)
public:

};

#endif //_DUTYDRAGDROPTABVIEWER_H_INCLUDED_
