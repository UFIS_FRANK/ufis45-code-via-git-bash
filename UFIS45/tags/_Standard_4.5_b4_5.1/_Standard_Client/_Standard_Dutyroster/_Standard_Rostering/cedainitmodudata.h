// CedaInitModuData.h

#ifndef __CEDAINITMODUDATA__
#define __CEDAINITMODUDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//--Class declaratino-------------------------------------------------------------------------------------------------------

class CedaInitModuData: public CedaData
{
public:

// Operations
	CedaInitModuData();
	~CedaInitModuData();

	bool SendInitModu();
	CString GetInitModuTxt();
	CString GetInitModuTxtG();

// Variaben
	char pcmInitModuFieldList[200];
private:
	CString GetOrgUnitRegisterString();
};

//---------------------------------------------------------------------------------------------------------

extern CedaInitModuData ogInitModuData;


#endif //__CEDAINITMODUDATA__
