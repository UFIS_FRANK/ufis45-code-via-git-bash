// MainFrm.h : interface of the CMainFrame class
//
//////////////////////////////////////////////////////////////////////////////////
//
//		HISTORY
//
//		rdr		12.01.2k	GetMessageString added, overrides the CMDIFrameWnd-fct
//
//////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__F66E6B68_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
#define AFX_MAINFRM_H__F66E6B68_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CodeDlg.h>
#include <CodeBigDlg.h>

// Spezial StatusBar
#include <XStatusBar.h>
#include <XPaneText.h>

//**************************************************************************
// Klasse Mainframe
//**************************************************************************

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:

	// Hier gehen alle BC f�r diese Anwendung rein
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 

	// �ndert die Zeitpunkte, Organisations- und Funktionslisten in den Code Dialogen
	void ChangeValidDatesFromDuty(bool bpFromDutyRoster,COleDateTime opDateFrom,COleDateTime opDateTo, CString opOrgList, CString opPfcList, bool bpAllCodes);

	// Der aktive MDI Frame wurde gewechselt
	void ActiveFrameChanged(int npSource, CString opOrgCodes, CString opPfcList);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	void GetMessageString(UINT nID, CString& rMessage) const;
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	XStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

	// gibt an, ob das geblinke f�r ankommende BC angezeigt werden soll
	BOOL		m_bActivateBlinker;

	// Zeiger auf den Code Dialog
	CCodeDlg*		pomCodeDlg;
	CCodeBigDlg*	pomBigCodeDlg;

	// Zustand des Code Dialogs
	bool bmIsSmall;

	// Men� initialisiert (sprachabh�ngig)?
	bool bmMenuLoaded;

	// letzt aktivierter Frame: DUTY_VIEW || SHIFT_VIEW
	int imSource;

	// G�ltige Ladezeitr�ume
	COleDateTime omDutyDateFrom,omDutyDateTo;
	bool bmShowDutyAllCodes;

	bool	 bmBroadcastCheckMsg;
	int		 imBroadcastState;		// the broadcast state of the watch dog
									// 0 = initialized, 1 = data send, 2 = data received
	long	imTimerValue;

// Generated message map functions
protected:

	// Message zum ver�ndern des Codedialogs wird bearbeitet
	LONG OnChangeCodeDlg(UINT wParam, LONG /*lParam*/); 

	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnViewDutyroster();
	afx_msg void OnViewShiftroster();
	afx_msg void OnFileSetup();
	afx_msg void OnProgrammStammdaten();
	afx_msg void OnProgrammGruppierung();
	afx_msg void OnClose();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnProgrammStaffBalance();
	afx_msg void OnProgrammInitAccount();
	afx_msg void OnProgrammAbsencePlanning();
	afx_msg void OnImportShifts();
	afx_msg void OnSendtosap();
	afx_msg void ReloadBasicData();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	static void  ProcessTrafficLight(long BcNum, int ipState);
	static void  ProcessCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
		   void	 UpdateTrafficLight(int ipState);

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__F66E6B68_5BB3_11D3_8EF6_00001C034EA0__INCLUDED_)
