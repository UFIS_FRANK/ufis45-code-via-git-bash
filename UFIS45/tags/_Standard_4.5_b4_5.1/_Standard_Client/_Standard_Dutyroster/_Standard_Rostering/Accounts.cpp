/////////////////////////////////////////////////////////////////////////////
// Accounts.cpp: implementation of the CAccounts class.
/////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// Construction/Destruction 
///////////////////////////////////////////////////////////////////////////// 

CAccounts::CAccounts(CedaDrrData *popDrrData, CedaAccData *popAccData,
					 CedaStfData *popStfData, 
					 CedaBsdData *popBsdData, CedaOdaData *popOdaData, 
					 CedaScoData *popScoData, CedaCotData *popCotData)
{
	// Zeiger auf Objekte zur Datenhaltung kopieren
	// Tagesschichtdaten
	pomDrrData = popDrrData;
	// Mitarbeiter Zeitkontodaten
	pomAccData = popAccData;
	// Mitarbeiterdaten
	pomStfData = popStfData;
	// Basisschichtdaten
	pomBsdData = popBsdData;
	// Basisabwesenheitsdaten
	pomOdaData = popOdaData;
	// Daten zu Vertragszuordnungen
	pomScoData = popScoData;
	// Vertragsdaten
	pomCotData = popCotData;
	// Die Beschreibung aller Konten Typen erstellen
	FillAccountTypeList();
}

CAccounts::~CAccounts()
{
CCS_TRY;
	DeleteRunningAccounts();
	DeleteAttentionText();
    omAccountTypeInfoList.DeleteAll();
	omAccountTypeInfoPtrMap.RemoveAll();
CCS_CATCH_ALL;
}

/////////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////////


//************************************************************************************************************
// GetCloseAccountByStaff: ermittelt den Abschlu�kontostand 
//	vom Typ <opType> f�r den Mitarbeiter <lpStaffUrno>.
// R�ckgabe:	BOOL	TRUE -> Konto gefunden
//						FALSE -> kein Konto gefunden
//************************************************************************************************************

bool CAccounts::GetCloseAccountByStaff(long lpStaffUrno, CString opYearFrom,
									  int ipMonthFrom, double &dpClose, CString opType /* = "A"*/)
{
CCS_TRY;
	// Er�ffnungswert initialisieren
	dpClose = 0;		
	// Arrays f�r den monatlichen Start-, Korrektur- und Abschlu�wert
	CStringArray olCloseArray;
	// Konto ermitteln
	ACCDATA *prlAcc = pomAccData->GetAccByKey(lpStaffUrno, opYearFrom, opType);

	// Konto gefunden?
	if(prlAcc != NULL)
	{
		// ja -> monatliche Start-, Korrektur- und Abschlu�werte ermitteln
		// (kann man auch �ber Pointerarithmetik machen:
		// 			int ilSize = sizeof(prlAcc->Op01);
		//			char *pclPtr = (char *)(prlAcc->Op01 + (ipMonthFrom-1) * ilSize);
		//			dpOpen = atoi(pclPtr);
		// ist aber unsicher)

		olCloseArray.Add(prlAcc->Cl01);
		olCloseArray.Add(prlAcc->Cl02);
		olCloseArray.Add(prlAcc->Cl03);
		olCloseArray.Add(prlAcc->Cl04);
		olCloseArray.Add(prlAcc->Cl05);
		olCloseArray.Add(prlAcc->Cl06);
		olCloseArray.Add(prlAcc->Cl07);
		olCloseArray.Add(prlAcc->Cl08);
		olCloseArray.Add(prlAcc->Cl09);
		olCloseArray.Add(prlAcc->Cl10);
		olCloseArray.Add(prlAcc->Cl11);
		olCloseArray.Add(prlAcc->Cl12);
		// Er�ffnungswert ermitteln, wenn vorhanden
		if(olCloseArray.GetSize() >= ipMonthFrom)
 			dpClose = atof(olCloseArray[ipMonthFrom-1]) * 60;
	}

	// Konto gefunden?
	return (prlAcc != NULL);
CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************
// GetOpenAccountByStaff: ermittelt den Er�ffnungskontostand 
//	vom Typ <opType> f�r den Mitarbeiter <lpStaffUrno>.
// R�ckgabe:	BOOL	TRUE -> Konto gefunden
//						FALSE -> kein Konto gefunden
//************************************************************************************************************

bool CAccounts::GetOpenAccountByStaff(long lpStaffUrno, CString opYearFrom,
									  int ipMonthFrom, double &dpOpen, CString opType /* = "A"*/)
{
CCS_TRY;
	// Er�ffnungswert initialisieren
	dpOpen = 0;		
	// Arrays f�r den monatlichen Start-, Korrektur- und Abschlu�wert
	CStringArray olOpenArray;
	// Konto ermitteln
	ACCDATA *prlAcc = pomAccData->GetAccByKey(lpStaffUrno, opYearFrom, opType);

	// Konto gefunden?
	if(prlAcc != NULL)
	{
		// ja -> monatliche Start-, Korrektur- und Abschlu�werte ermitteln
		// (kann man auch �ber Pointerarithmetik machen:
		// 			int ilSize = sizeof(prlAcc->Op01);
		//			char *pclPtr = (char *)(prlAcc->Op01 + (ipMonthFrom-1) * ilSize);
		//			dpOpen = atoi(pclPtr);
		// ist aber unsicher)

		olOpenArray.Add(prlAcc->Op01);
		olOpenArray.Add(prlAcc->Op02);
		olOpenArray.Add(prlAcc->Op03);
		olOpenArray.Add(prlAcc->Op04);
		olOpenArray.Add(prlAcc->Op05);
		olOpenArray.Add(prlAcc->Op06);
		olOpenArray.Add(prlAcc->Op07);
		olOpenArray.Add(prlAcc->Op08);
		olOpenArray.Add(prlAcc->Op09);
		olOpenArray.Add(prlAcc->Op10);
		olOpenArray.Add(prlAcc->Op11);
		olOpenArray.Add(prlAcc->Op12);
		// Er�ffnungswert ermitteln, wenn vorhanden
		if(olOpenArray.GetSize() >= ipMonthFrom)
 			dpOpen = atof(olOpenArray[ipMonthFrom-1]) * 60;
	}

	// Konto gefunden?
	return (prlAcc != NULL);
CCS_CATCH_ALL;
	return false;
}

//************************************************************************************************************
// SetCloseAccountByStaff: speichert den Abschlu�kontowert <dpCloseMin> des Monats
//	<ipMonth> im Datensatz <prpAcc> und setzt das DATA_CHANGED-Flag.
// R�ckgabe:	bool Erfolg?
//************************************************************************************************************

bool CAccounts::SetCloseAccountByStaff(ACCDATA *prpAcc, double dpCloseMin, int ipMonth, bool bpSetOpen /*true*/)
{
CCS_TRY;
	// Datensatz darf nicht null sein
	_ASSERT(prpAcc != NULL);
	
	// Abschlusswert in Stunden errechnen
	double dlCloseHour = (dpCloseMin/60);

	// Zeiger auf die Speicherorte f�r Abschlu�- und Er�ffnungswert des n�chsten Monats
	char *pclClose, *pclNextMonthOpen;

	// je nach Monat das entsprechende Datenfeld benutzen
	switch (ipMonth){
	case 1:	// Januar
		pclClose = (char *)prpAcc->Cl01;
		pclNextMonthOpen = (char *)prpAcc->Op02;
		break;
	case 2: // Februar ......
		pclClose = (char *)prpAcc->Cl02;
		pclNextMonthOpen = (char *)prpAcc->Op03;
		break;
	case 3:
		pclClose = (char *)prpAcc->Cl03;
		pclNextMonthOpen = (char *)prpAcc->Op04;
		break;
	case 4:
		pclClose = (char *)prpAcc->Cl04;
		pclNextMonthOpen = (char *)prpAcc->Op05;
		break;
	case 5:
		pclClose = (char *)prpAcc->Cl05;
		pclNextMonthOpen = (char *)prpAcc->Op06;
		break;
	case 6:
		pclClose = (char *)prpAcc->Cl06;
		pclNextMonthOpen = (char *)prpAcc->Op07;
		break;
	case 7:
		pclClose = (char *)prpAcc->Cl07;
		pclNextMonthOpen = (char *)prpAcc->Op08;
		break;
	case 8:
		pclClose = (char *)prpAcc->Cl08;
		pclNextMonthOpen = (char *)prpAcc->Op09;
		break;
	case 9:
		pclClose = (char *)prpAcc->Cl09;
		pclNextMonthOpen = (char *)prpAcc->Op10;
		break;
	case 10:
		pclClose = (char *)prpAcc->Cl10;
		pclNextMonthOpen = (char *)prpAcc->Op11;
		break;
	case 11:
		pclClose = (char *)prpAcc->Cl11;
		pclNextMonthOpen = (char *)prpAcc->Op12;
		break;
	case 12: // f�r Dezember kein Er�ffnungskonto
		pclClose = (char *)prpAcc->Cl12;
		pclNextMonthOpen = NULL;
		break;
	default :
		return false;
	}
	
	// Abschlusswert speichern
	sprintf(pclClose,"%0.2f",dlCloseHour);

	if (bpSetOpen)
	{
		// Er�ffnungswert des n�chsten Monats speichern (ausser f�r Dezember)
		if (pclNextMonthOpen != NULL)
	 		sprintf(pclNextMonthOpen,"%0.2f",dlCloseHour);
	}
	else
	{
		// Er�ffnungswert des n�chsten Monats auf 0 setzen (ausser f�r Dezember)
		if (pclNextMonthOpen != NULL)
	 		strcpy (pclNextMonthOpen,"0");
	}

	// �nderungsflag setzen, wenn Datensatz nicht DATA_NEW (dann wird sowieso gespeichert)
	if (prpAcc->IsChanged == DATA_UNCHANGED)
		prpAcc->IsChanged = DATA_CHANGED;

CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************
// GetAccDataByStaff: ermittelt den Kontodatensatz vom Typ <opType> f�r den Mitarbeiter 
//	<lpStaffUrno>. Wenn der Datensatz nicht existiert, wird versucht ihn zu erzeugen.
//	Der Datensatz wird mit allen bekannten Informationen initialisiert.
// R�ckgabe:	ein Zeiger auf das Konto, NULL, wenn ein Fehler auftrat
//************************************************************************************************************

ACCDATA* CAccounts::GetAccDataByStaff(long lpStaffUrno, CString opYearFrom, int ipMonthFrom, 
									  double &dpOpenMin, CString opType ,
									  bool bpUseCorrection /*= true*/)
{
CCS_TRY;
	// Wert initialisieren
	dpOpenMin = 0;
	// Arrays f�r den monatlichen Start-, Korrektur- und Abschlu�wert
	CStringArray olOpenArray, olCorrectionArray;
	// Konto ermitteln
	ACCDATA *prlAcc = pomAccData->GetAccByKey(lpStaffUrno, opYearFrom, opType);

	// Konto gefunden?
	if(prlAcc != NULL)
	{
		// ja -> monatliche Start- und Korrekturwerte ermitteln
		olOpenArray.Add(prlAcc->Op01); olCorrectionArray.Add(prlAcc->Co01);
		olOpenArray.Add(prlAcc->Op02); olCorrectionArray.Add(prlAcc->Co02);
		olOpenArray.Add(prlAcc->Op03); olCorrectionArray.Add(prlAcc->Co03);
		olOpenArray.Add(prlAcc->Op04); olCorrectionArray.Add(prlAcc->Co04);
		olOpenArray.Add(prlAcc->Op05); olCorrectionArray.Add(prlAcc->Co05);
		olOpenArray.Add(prlAcc->Op06); olCorrectionArray.Add(prlAcc->Co06);
		olOpenArray.Add(prlAcc->Op07); olCorrectionArray.Add(prlAcc->Co07);
		olOpenArray.Add(prlAcc->Op08); olCorrectionArray.Add(prlAcc->Co08);
		olOpenArray.Add(prlAcc->Op09); olCorrectionArray.Add(prlAcc->Co09);
		olOpenArray.Add(prlAcc->Op10); olCorrectionArray.Add(prlAcc->Co10);
		olOpenArray.Add(prlAcc->Op11); olCorrectionArray.Add(prlAcc->Co11);
		olOpenArray.Add(prlAcc->Op12); olCorrectionArray.Add(prlAcc->Co12);
		// Er�ffnungswert in Min. ermitteln
		if(olOpenArray.GetSize() >= ipMonthFrom){
			dpOpenMin = atof(olOpenArray[ipMonthFrom-1]) * 60;
			// Korrekturwert ber�cksichtigen?
			if (bpUseCorrection){
				dpOpenMin += atof(olCorrectionArray[ipMonthFrom-1]) * 60;
			}
		}
	}
	else
	{
		// nein -> neues Konto erzeugen
		prlAcc = new ACCDATA;
		// Konto erzeugt?
		if (prlAcc != NULL){
			// ja -> Datensatz initialisieren
			// Typ speichern
			strcpy(prlAcc->Type,opType);
			// Mitarbeiter-Urno
			prlAcc->Stfu = lpStaffUrno;
			// n�chste freie Universal-Datensatz-ID ermitteln und speichern
			prlAcc->Urno = ogBasicData.GetNextUrno();
			// Jahr, f�r das das Konto gilt speichern
			strcpy(prlAcc->Year,opYearFrom);
			// Datensatz-Erstellungsdatum ermitteln und speichern
			prlAcc->Cdat = CTime::GetCurrentTime();
			// Ersteller-Info ermitteln und speichern
			strcpy(prlAcc->Usec,ogBasicData.omUserID);				
			// Mitarbeiterstammdatensatz ermitteln
			STFDATA *prlStf = pomStfData->GetStfByUrno(lpStaffUrno);
			if(prlStf!=NULL)
			{
				// Mitarbeiter-Personalnummer ermitteln und speichern
				strcpy(prlAcc->Peno,prlStf->Peno);
			}
			// Datensatz-Status kennzeichnen
			prlAcc->IsChanged = DATA_NEW;
			// Datensatz dem Datenhaltungsobjekt hinzuf�gen
			// N�,ADO 31.3.00 
			//pomAccData->InsertInternal(prlAcc,false);
		}
	}
	
	// Konto gefunden?
	return prlAcc;
CCS_CATCH_ALL;
	return NULL;
}

//***************************************************************************************************
// DeleteRunningAccounts: l�scht alle Mitarbeiterkonten aus dem Kontenarray
//	<omRunningAccount>.
// R�ckgabe: keine
//***************************************************************************************************

void CAccounts::DeleteRunningAccounts()
{
CCS_TRY;
	// solange Mitarbeiter im Array sind
	while(omRunningAccount.GetSize()>0)
	{
		// Mitarbeiterkonto l�schen
		DeleteRunningAccountsByStaff(omRunningAccount[0].lStfUrno);
	}
CCS_CATCH_ALL;
}

//***************************************************************************************************
// DeleteRunningAccountsByStaff: l�scht das Mitarbeiterkonto des Mitarbeiters
//  <lpStaffUrno> aus dem Kontenarray <omRunningAccount>.
// R�ckgabe: keine
//***************************************************************************************************

void CAccounts::DeleteRunningAccountsByStaff(long lpStaffUrno)
{
CCS_TRY;
	//RUNNINGACCOUNT *prlRunningAccount = NULL;
	int ilSize = omRunningAccount.GetSize();
	for(int i=0; i<ilSize; i++)
	{
		if(omRunningAccount[i].lStfUrno == lpStaffUrno)
		{
			omRunningAccount[i].oAccountsMap.RemoveAll();
			omRunningAccount[i].oAccounts.DeleteAll();
			omRunningAccount.DeleteAt(i);
			omRunningAccountMap.RemoveKey((void*)lpStaffUrno);
			i = ilSize;
		}
	}
CCS_CATCH_ALL;
}

//************************************************************************************************************
// GetRunningfAccountPosByStaffUrno: ermittelt den Kontoindex (= Zeilennummer in
//	View).
// R�ckgabe:	der Index der Mitarbeiterkonten oder -1, wenn der Mitarbeiter
//				nicht gefunden wurde.
//************************************************************************************************************

int CAccounts::GetRunningfAccountPosByStaffUrno(long lpStaffUrno)
{
CCS_TRY;	
	// Anzahl der vorhandenen Mitarbeiter (gleich Anzahl der Mitarbeiterkonten) ermitteln
	int ilAccountSize = omRunningAccount.GetSize();

	// alle Mitarbeiterkonten untersuchen
	for(int i=0; i<ilAccountSize; i++)
	{
		// stimmt die StaffUrno �berein?
		if (omRunningAccount[i].lStfUrno == lpStaffUrno)
			return i;	// ja -> Index zur�ckliefern
	}
CCS_CATCH_ALL;
	// Konto nicht gefunden oder Exception -> ung�ltigen Index zur�ckliefern
	return -1;
}

//************************************************************************************************************
// GetRunningfAccountNoOfTypes: ermittelt die Anzahl der unterschiedlichen Konten.
// R�ckgabe:	Anzahl der Mitarbeiterkonten
//************************************************************************************************************

int CAccounts::GetNoOfRunningfAccountTypes()
{
CCS_TRY;	
	// Anzahl der vorhandenen unteschiedlichen Konten pro Mitarbeiter ermitteln
	return omRunningAccount[0].oAccounts.GetSize();
CCS_CATCH_ALL;
	return -1;
}

//************************************************************************************************************
// GetRunningAccountByStaff: ermittelt das Arbeitszeitkonto vom Typ 
//	<ipAccountType> des Mitarbeiters mit der URNO <lpStaffUrno> und speichert 
//	die Werte f�r Dauer und G�ltigkeit in einem Objekt vom Typ
//	ACCOUNTRETURNSTRUCT. Initialwerte in ACCOUNTRETURNSTRUCT zeigen an, da�
//	entweder das Konto nicht gefunden wurde oder eine Exception ausgel�st wurde.
// R�ckgabe:	ACCOUNTRETURNSTRUCT
//************************************************************************************************************

ACCOUNTRETURNSTRUCT CAccounts::GetRunningAccountByStaff(UINT ipAccountType, long pStaffUrno)
{
CCS_TRY;
	// R�ckgabe-Objekt
	ACCOUNTRETURNSTRUCT rlAccountReturnStruct;

	// Wie soll das Konto Formatiert werden (z.B. "%01.2f ")
	CString olAccount;
	olAccount.Format("%d",ipAccountType);
	rlAccountReturnStruct.oFormat = ogBCD.GetField("ADE","TYPE",olAccount,"FORM");

	// Objekte f�r Kontosuche
	RUNNINGACCOUNT *prlRunningAccount = NULL;
	RUNNINGACCOUNTTYPE *prlRunningAccountType = NULL;
	// Gesamtkonten suchen f�r diesen Mitarbeiter suchen
	if(omRunningAccountMap.Lookup((void *)pStaffUrno,(void *&)prlRunningAccount) == TRUE)
	{
		// gefunden -> Aus der Map f�r diesen Mitarbeiter diesen Kontotyp suchen
		if(prlRunningAccount->oAccountsMap.Lookup((void *)ipAccountType,(void *&)prlRunningAccountType) == TRUE)
		{
			// gefunden -> Werte kopieren
			rlAccountReturnStruct.bBool   = prlRunningAccountType->bOK;
			rlAccountReturnStruct.dDouble = prlRunningAccountType->dHour;
		}
	}
	// Info-Objekt zur�ckgeben
	return rlAccountReturnStruct;
CCS_CATCH_ALL;
// Default-initialisiertes Objekt zur�ckgeben (Werte siehe Header)
ACCOUNTRETURNSTRUCT rlError;
return rlError;
}

//***************************************************************************************************
// GetRunningAccountFormat: gibt das Formattag zur�ck, das dem benutzten
//	Zahlenformat entspricht, das zur Darstellung der Kontowerte vom Typ 
//	<ipAccountType> benutzt wird.
// R�ckgabe:	das Formattag oder ein Leerstring, wenn eine 
//				Exception ausgel�st wurde.
//***************************************************************************************************

CString CAccounts::GetRunningAccountFormat(UINT ipAccountType)
{
CCS_TRY;
	// Puffer f�r die R�ckgabe
	CString olHeader = "";
	// je nach gew�nschtem Kontotyp <ipAccountType> den entsprechenden Format String zur�ckgeben

	ACCOUNTTYPEINFO *prlAccountType = NULL;
	if ((prlAccountType = GetAccountTypeInfoByAccountType(ipAccountType)) != NULL)
	{
		olHeader = prlAccountType->oFormat;
	}
	return olHeader;
CCS_CATCH_ALL;
return CString("");
}

//***************************************************************************************************
// GetRunningAccountHeader: gibt die sprachabh�ngige Bezeichnung des Arbeitszeit-
//	kontos vom Typ <ipAccountType> zur�ck.
// R�ckgabe:	die Kontobezeichnung oder ein Leerstring, wenn eine 
//				Exception ausgel�st wurde oder der Kontotyp unbekannt ist.
//***************************************************************************************************

CString CAccounts::GetRunningAccountHeader(UINT ipAccountType, bool bpGetShortName /*=true*/)
{
CCS_TRY;
	// Puffer f�r die R�ckgabe
	CString olHeader = "";
	// je nach gew�nschtem Kontotyp <ipAccountType> den entsprechenden Text String zur�ckgeben
	ACCOUNTTYPEINFO *prlAccountType = NULL;
	if ((prlAccountType = GetAccountTypeInfoByAccountType(ipAccountType)) != NULL)
	{
		if (bpGetShortName)
			olHeader = prlAccountType->oShortName;
		else
			olHeader = prlAccountType->oName;
	}
	return olHeader;
CCS_CATCH_ALL;
return CString("");
}

//***************************************************************************************************
// GetAttentionText: f�llt den Array <popAttentionTextArray> mit den Eintr�gen
//	aus <omAttentionTextArray>. Dieser Array speichert alle generierten Warnungen
//	und sonstige Meldungen, die bei der Ermittlung der Arbeitszeitkonten
//	angefallen sind (z.B. 'das Konto f�r XY konnte nicht ermittelt werden' etc.).
// R�ckgabe:	TRUE -> es gibt Eintr�ge
//				FALSE -> es gibt keine Eintr�ge
//***************************************************************************************************

bool CAccounts::GetAttentionText(CStringArray *popAttentionTextArray)
{
CCS_TRY;
	// Annahme: keine Eintr�ge vorhanden
	bool blExistAttentionText = false;
	// �bergebenen Array leeren
	popAttentionTextArray->RemoveAll();
	// Klassenmember anh�ngen -> entspricht kopieren
	popAttentionTextArray->Append(omAttentionTextArray);
	// gibt es Eintr�ge?
	if(popAttentionTextArray->GetSize()>0)
	{
		// ja -> Erkenntnis speichern
		blExistAttentionText = true;
	}
	// R�ckgabe: gibt es Eintr�ge?
	return blExistAttentionText;
CCS_CATCH_ALL;
return false;
}

//***************************************************************************************************
// DeleteAttentionText: leert den Array <omAttentionTextArray>. Dieser Array 
//	speichert alle generierten Warnungen und sonstige Meldungen, die bei der 
//	Ermittlung der Arbeitszeitkonten angefallen sind (z.B. 'das Konto f�r XY 
//	konnte nicht ermittelt werden' etc.).
// R�ckgabe:	keine
//***************************************************************************************************

void CAccounts::DeleteAttentionText()
{
CCS_TRY;
	omAttentionTextArray.RemoveAll();
CCS_CATCH_ALL;
}

//***************************************************************************
// F�llen der Account-Typen-Liste
// R�ck: Anzahl der Elemente in omAccountTypeInfoList
//***************************************************************************
int CAccounts::FillAccountTypeList()
{
CCS_TRY;
	omAccountTypeInfoList.DeleteAll();
	omAccountTypeInfoPtrMap.RemoveAll();
	ACCOUNTTYPEINFO *prlAccountType =NULL;

	TRACE("\nFound %i Accounts",ogBCD.GetDataCount("ADE"));
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("\nFound %i Accounts",ogBCD.GetDataCount("ADE"));
		WriteInRosteringLog(LOGFILE_FULL);
	}

	// Alle Konten durchgehen
	for (int i=0;i < ogBCD.GetDataCount("ADE"); i++)
	{
		prlAccountType = new ACCOUNTTYPEINFO;

		prlAccountType->iInternNumber	= atol(ogBCD.GetField("ADE", i, "TYPE")); 
		prlAccountType->oExternKey		= ogBCD.GetField("ADE", i, "CODE"); 
		prlAccountType->oName			= ogBCD.GetField("ADE", i, "NAME"); 
		prlAccountType->oShortName		= ogBCD.GetField("ADE", i, "SNAM"); 
		prlAccountType->oFormat			= ogBCD.GetField("ADE", i, "FORM"); 

		if (ogBCD.GetField("ADE", i, "SHOW") == "x")
			prlAccountType->bShowAccount = true;
		else
			prlAccountType->bShowAccount = false;


		omAccountTypeInfoList.Add(prlAccountType);
		omAccountTypeInfoPtrMap.SetAt((void *)prlAccountType->iInternNumber,prlAccountType);
	}
	return omAccountTypeInfoList.GetSize();
CCS_CATCH_ALL;
return 0;
}

//***************************************************************************
//
//***************************************************************************

ACCOUNTTYPEINFO* CAccounts::GetAccountTypeInfoByAccountType(UINT ipInternNumber)
{
CCS_TRY;
	ACCOUNTTYPEINFO *prlAccountType = NULL;

	if (omAccountTypeInfoPtrMap.Lookup((void *)ipInternNumber, (void *&) prlAccountType) == TRUE)
	{
		return prlAccountType;
	}
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//***************************************************************************
// GetAccountTypeInfoByExternKey: sucht den Datensatz mit dem Feldwert
//	<opExternKey> und gibt ihn zur�ck
// R�ckgabe:	NULL (Datensatz nicht gefunden oder Exception), sonst der DS
//***************************************************************************

ACCOUNTTYPEINFO* CAccounts::GetAccountTypeInfoByExternKey(CString opExternKey)
{
CCS_TRY;
	// Anzahl der bekannten Konten
	int ilAccountSize = omAccountTypeInfoList.GetSize();
	// Datensatz suchen
	for (int ilCount=0; ilCount<ilAccountSize; ilCount++)
	{
		// gefunden?
		if (omAccountTypeInfoList[ilCount].oExternKey == opExternKey)
			return &omAccountTypeInfoList[ilCount]; // ja
	}
	// Datensatz nicht gefunden
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//************************************************************************************************************
// GetRunningAccountByTypeAndStaff: ermittelt das Arbeitszeitkonto vom Typ 
//	<ipAccountType> des Mitarbeiters mit der URNO <lpStaffUrno>. Wenn es nicht 
//	bereits ermittelt wurde, wird es erzeugt.
// R�ckgabe:	das ermittelte Konto oder NULL, wenn eine Exception ausgel�st
//				wurde
//************************************************************************************************************

RUNNINGACCOUNTTYPE* CAccounts::GetRunningAccountByTypeAndStaff(long lpStaffUrno, 
															   UINT ipAccountType,
															   long lpDays)
{
CCS_TRY;
	// zu ermittelndes Gesamtkonto...
	RUNNINGACCOUNT *prlRunningAccount = NULL;
	// ...und das geforderte Konto vom Typ <ipAccountType>
	RUNNINGACCOUNTTYPE *prlRunningAccountType = NULL;
	
	// Gesamtkonto f�r Mitarbeiter <lpStaffUrno> vorhanden?
	if(omRunningAccountMap.Lookup((void *)lpStaffUrno,(void *&)prlRunningAccount) == FALSE)
	{
		// nein -> neu anlegen...
		prlRunningAccount = new RUNNINGACCOUNT;
		// ...,dem Mitarbeiter zuweisen...
		prlRunningAccount->lStfUrno = lpStaffUrno;
		// ...und intern speichern
		omRunningAccount.Add(prlRunningAccount);
		// das neu erzeugte Konto mappen
		omRunningAccountMap.SetAt((void *)lpStaffUrno, prlRunningAccount);
	}
	
	// Konto vom Typ <ipAccountType> im Gesamtkonto vorhanden?
	if(prlRunningAccount->oAccountsMap.Lookup((void *)ipAccountType,(void *&)prlRunningAccountType) == FALSE)
	{
		// nein -> neus Konto anlegen...
		prlRunningAccountType = new RUNNINGACCOUNTTYPE;
		// ...und zwar vom Typ <ipAccountType>
		prlRunningAccountType->iType = ipAccountType;
		// Konto dem Gesamtkonto hinzuf�gen
		prlRunningAccount->oAccounts.Add(prlRunningAccountType);
		// ...und mappen
		prlRunningAccount->oAccountsMap.SetAt((void *)ipAccountType, prlRunningAccountType);
		// Element f�r <lpDays> Tage zur Verf�gung stellen
		prlRunningAccountType->oDailyElements.SetSize(lpDays);
		// jedes Element mit 0 initialisieren
		for(int i=0; i<lpDays; i++)
		{
			prlRunningAccountType->oDailyElements.SetAt(i, "0");
		}
	}
	else	// Konto vorhanden
	{
		// nicht genug Elemente (= Tage) vorhanden f�r den zu untersuchenden Zeitraum?
		while(prlRunningAccountType->oDailyElements.GetSize()<lpDays)
		{
			// Array vergr��ern und neue Elemente initialisieren,...
			prlRunningAccountType->oDailyElements.Add("0");
		}
		prlRunningAccountType->oDailyElements.SetSize(lpDays);	// ...bis die Gr��e stimmt
	}
	// gefundenes/erzeugtes Konto zur�ckliefern
	return prlRunningAccountType;
CCS_CATCH_ALL;
// Exception ausgel�st -> R�ckgabe ist NULL
return NULL;
}

//************************************************************************************************************
// GetRunningAccountByStaff: ermittelt das Arbeitszeitkonto vom Typ 
//	<ipAccountType> des Mitarbeiters mit der URNO <lpStaffUrno> und speichert 
//	die Werte f�r Dauer und G�ltigkeit in einem Objekt vom Typ
//	ACCOUNTRETURNSTRUCT. Initialwerte in ACCOUNTRETURNSTRUCT zeigen an, da�
//	entweder das Konto nicht gefunden wurde oder eine Exception ausgel�st wurde.
// Ermittlung erfolgt je nach Kontotyp 

// R�ckgabe:	ACCOUNTRETURNSTRUCT
//************************************************************************************************************

ACCOUNTRETURNSTRUCT CAccounts::GetAccountByStaffAndMonth(UINT ipAccountType, long pStaffUrno, COleDateTime opFromDate, COleDateTime opToDate)
{

	ACCOUNTRETURNSTRUCT rlError;
CCS_TRY;
	
	// Kontoart ermitteln
	CString olAccount;
	olAccount.Format("%d",ipAccountType);

	// Typ erhalten
	CString olType = ogBCD.GetField("ADE","TYPE",olAccount,"KTYP");

	if (olType == "S")
	{
		// Statisch
		return (GetStaticAccountByStaffAndMonth(ipAccountType,pStaffUrno,opFromDate,opToDate,0,"K"));
	}
	else if (olType == "D")
	{
		// Dynamisch, jetzt mit dem Handler wird dieser Fall wie Statisch behandelt
		return (GetStaticAccountByStaffAndMonth(ipAccountType,pStaffUrno,opFromDate,opToDate,0,"C"));
	}
	else if (olType == "T")
	{
		// Tempor�r
		return (GetTempAccountByStaffAndMonth(ipAccountType,pStaffUrno,opFromDate,opToDate));
	}

	return rlError;
CCS_CATCH_ALL;

return rlError;
}

//************************************************************************************************************
// R�ckgabe:	ACCOUNTRETURNSTRUCT
//************************************************************************************************************

ACCOUNTRETURNSTRUCT CAccounts::GetStaticAccountByStaffAndMonth(UINT ipAccount, long pStaffUrno,COleDateTime opFromDate, COleDateTime opToDate,int ipUsmoValue,CString opFieldToUse /*K*/,bool bpIsTempAccount /*=false*/)
{
	// Fehler-Objekt
	ACCOUNTRETURNSTRUCT rlError;
	rlError.bBool = false;

CCS_TRY;
	// R�ckgabe-Objekt
	ACCOUNTRETURNSTRUCT rlAccountReturnStruct;

	// G�ltigkeit des Datums pr�fen
	if (opFromDate.GetStatus() == COleDateTime::invalid || opToDate.GetStatus() == COleDateTime::invalid)
		return rlError;
	
	CString olYear;
	CString olAccount;
	CString olValue;
	ACCDATA* polAcc = NULL;
	COleDateTime olFrom = opFromDate;
	double dlResult = 0.0;
	bool blAdd = true;
	int ilLastQuarter = 0;

	olAccount.Format("%d",ipAccount);

	// Wie soll das Konto Formatiert werden (z.B. "%01.2f ")
	rlAccountReturnStruct.oFormat = ogBCD.GetField("ADE","TYPE",olAccount,"FORM");

	while (olFrom.GetMonth() <= opToDate.GetMonth() && olFrom.GetYear() <= opToDate.GetYear() && olFrom.GetStatus() == COleDateTime::valid)
	{
		olYear.Format("%d",olFrom.GetYear());
		olValue = "";
		polAcc = pomAccData->GetAccByKey(pStaffUrno,olYear,olAccount);
		blAdd = true;

		// Datensatz gefunden ?
		if (polAcc == NULL)
		{
			// Format und ValueDefinition mu� auch bei nicht gefundenem Datensatz gesetzt werden
			rlError.oFormat	= rlAccountReturnStruct.oFormat;
			if (ogBCD.GetField("ADE","TYPE",olAccount,"KPER") == "S")
			{
				rlError.oValueDefinition = "STRINGorSINGEL"; // STRINGorSINGEL sagt das oString angezeigt werden soll 
			}
			if (ogBCD.GetField("ADE","TYPE",olAccount,"FTYP") == "T")
			{
				rlError.oValueDefinition = "STRINGorSINGEL"; // STRINGorSINGEL sagt das oString angezeigt werden soll 
			}
			//return rlError;
		}
		else
		{
			//---------------------------------------------------------------
			// Ermittelung des Monatswertes
			//---------------------------------------------------------------
			rlAccountReturnStruct.bBool = true;

			// Berechnung des zu nutzenden Monats
			int ilMonthToUse = olFrom.GetMonth();
			// M�gliche Werte im USMO Feld ber�cksichtigen
			ilMonthToUse = ilMonthToUse + ipUsmoValue;

			// Darf nicht die Grenzen sprengen
			if (ilMonthToUse < 1)
				ilMonthToUse = 1;

			if (ilMonthToUse > 12)
				ilMonthToUse = 12;
			
			// Welches Feld soll genutzt werden (O = �ffnung , C = Close, K = Correction)
			if (opFieldToUse == "O")
			{
				// je nach Monat das entsprechende Datenfeld benutzen
				switch (ilMonthToUse)
				{
				case 1:	// Januar
					olValue = (char *)polAcc->Op01;
					break;
				case 2: // Februar ......
					olValue = (char *)polAcc->Op02;
					break;
				case 3:
					olValue = (char *)polAcc->Op03;
					break;
				case 4:
					olValue = (char *)polAcc->Op04;
					break;
				case 5:
					olValue = (char *)polAcc->Op05;
					break;
				case 6:
					olValue = (char *)polAcc->Op06;
					break;
				case 7:
					olValue = (char *)polAcc->Op07;
					break;
				case 8:
					olValue = (char *)polAcc->Op08;
					break;
				case 9:	
					olValue = (char *)polAcc->Op09;
					break;
				case 10:
					olValue = (char *)polAcc->Op10;
					break;
				case 11:
					olValue = (char *)polAcc->Op11;
					break;
				case 12: 
					olValue = (char *)polAcc->Op12;
					break;
				default :
					// Fehler
					olValue = "0";
					rlAccountReturnStruct.bBool = true;
				}
			}
			else if (opFieldToUse == "C")
			{
				// je nach Monat das entsprechende Datenfeld benutzen
				switch (ilMonthToUse)
				{
				case 1:	// Januar
					olValue = (char *)polAcc->Cl01;
					break;
				case 2: // Februar ......
					olValue = (char *)polAcc->Cl02;
					break;
				case 3:
					olValue = (char *)polAcc->Cl03;
					break;
				case 4:
					olValue = (char *)polAcc->Cl04;
					break;
				case 5:
					olValue = (char *)polAcc->Cl05;
					break;
				case 6:
					olValue = (char *)polAcc->Cl06;
					break;
				case 7:
					olValue = (char *)polAcc->Cl07;
					break;
				case 8:
					olValue = (char *)polAcc->Cl08;
					break;
				case 9:	
					olValue = (char *)polAcc->Cl09;
					break;
				case 10:
					olValue = (char *)polAcc->Cl10;
					break;
				case 11:
					olValue = (char *)polAcc->Cl11;
					break;
				case 12: 
					olValue = (char *)polAcc->Cl12;
					break;
				default :
					// Fehler
					olValue = "0";
					rlAccountReturnStruct.bBool = true;
				}
			}
			else
			{
				// Default:
					// je nach Monat das entsprechende Datenfeld benutzen
				switch (ilMonthToUse)
				{
				case 1:	// Januar
					olValue = (char *)polAcc->Co01;
					break;
				case 2: // Februar ......
					olValue = (char *)polAcc->Co02;
					break;
				case 3:
					olValue = (char *)polAcc->Co03;
					break;
				case 4:
					olValue = (char *)polAcc->Co04;
					break;
				case 5:
					olValue = (char *)polAcc->Co05;
					break;
				case 6:
					olValue = (char *)polAcc->Co06;
					break;
				case 7:
					olValue = (char *)polAcc->Co07;
					break;
				case 8:
					olValue = (char *)polAcc->Co08;
					break;
				case 9:	
					olValue = (char *)polAcc->Co09;
					break;
				case 10:
					olValue = (char *)polAcc->Co10;
					break;
				case 11:
					olValue = (char *)polAcc->Co11;
					break;
				case 12: 
					olValue = (char *)polAcc->Co12;
					break;
				default :
					// Fehler
					olValue = "0";
					rlAccountReturnStruct.bBool = true;
				}
			}

			// --------------------------- Spezialf�lle --------------------------------------------
			// Falls es sich um ein Single Wert handelt, immer den Wert in CO01 nehmen 
			CString olKPer, olKTyp, olFTyp;
			olKPer = ogBCD.GetField("ADE","TYPE",olAccount,"KPER");
			olKTyp = ogBCD.GetField("ADE","TYPE",olAccount,"KTYP");
			olFTyp = ogBCD.GetField("ADE","TYPE",olAccount,"FTYP");

			if (bpIsTempAccount == false || olFTyp != "H")
			{

				if(olKPer == "S" && olFTyp == "T")
				{
					rlAccountReturnStruct.oString = (char *)polAcc->Co01;
					rlAccountReturnStruct.oValueDefinition = "STRINGorSINGEL";// STRINGorSINGEL sagt das oString angezeigt werden soll 
				}
				// Falls es sich um ein Format Type = T handelt, immer als String bearbeiten
				if(olKPer == "M" && olFTyp == "T")
				{
					rlAccountReturnStruct.oString = olValue;
					rlAccountReturnStruct.oValueDefinition = "STRINGorSINGEL";// STRINGorSINGEL sagt das oString angezeigt werden soll 
				}
				if(olKPer == "Q")
				{
					switch (ilMonthToUse)
					{
						case 1:
						case 2:
						case 3:
							if (ilLastQuarter == 1)
							{
								blAdd = false;
							}
							else
							{
								ilLastQuarter = 1;
							}
							break;
						case 4:
						case 5:
						case 6:
							if (ilLastQuarter == 2)
							{
								blAdd = false;
							}
							else
							{
								ilLastQuarter = 2;
							}
							break;
						case 7:
						case 8:
						case 9:
							if (ilLastQuarter == 3)
							{
								blAdd = false;
							}
							else
							{
								ilLastQuarter = 3;
							}
							break;
						case 10:
						case 11:
						case 12:
							if (ilLastQuarter == 4)
							{
								blAdd = false;
							}
							else
							{
								ilLastQuarter = 4;
							}
							break;
					}
				}
				// Falls es sich um ein Jahres Wert handelt, immer den Wert in CL12 nehmen
				if(olKPer == "Y" && olKTyp == "D")
				{
					olValue = (char *)polAcc->Cl12;
					rlAccountReturnStruct.bBool = true;
					blAdd = false;
				}
				if(olKPer == "Y" && olKTyp == "S")
				{
					olValue = (char *)polAcc->Co12;
					rlAccountReturnStruct.bBool = true;
					blAdd = false;
				}
			}
		}

		if (blAdd == false)
		{
			dlResult = atof(olValue);
		}
		else
		{
			dlResult += atof(olValue);
		}

		if (olFrom.GetMonth() == 12)
		{
			// next year January
			olFrom.SetDate(olFrom.GetYear()+1,1,olFrom.GetDay());
			ilLastQuarter = 0;
		}
		else
		{
			// next month
			olFrom.SetDate(olFrom.GetYear(),olFrom.GetMonth()+1,olFrom.GetDay());
		}
	}

	// Auswertung
	rlAccountReturnStruct.dDouble = dlResult;//atof(olValue);

	return rlAccountReturnStruct;
CCS_CATCH_ALL;
// Default-initialisiertes Objekt zur�ckgeben (Werte siehe Header)
return rlError;
}

//************************************************************************************************************
// R�ckgabe:	ACCOUNTRETURNSTRUCT
//************************************************************************************************************

ACCOUNTRETURNSTRUCT CAccounts::GetTempAccountByStaffAndMonth(UINT ipAccountType, long pStaffUrno,COleDateTime opFromDate,COleDateTime opToDate)
{
	// Fehler-Objekt
	ACCOUNTRETURNSTRUCT rlError;

CCS_TRY;
	// R�ckgabe-Objekt
	ACCOUNTRETURNSTRUCT rlAccountReturnStruct;

	// G�ltigkeit des Datums pr�fen
	if (opFromDate.GetStatus() == COleDateTime::invalid || opToDate.GetStatus() == COleDateTime::invalid)
	{
		ACCOUNTRETURNSTRUCT rlError;
		return rlError;
	}

	// Acounttype umwandeln
	CString olAccountType;
	olAccountType.Format("%d",ipAccountType);

	// Wie soll das Konto Formatiert werden (z.B. "%01.2f ")
	rlAccountReturnStruct.oFormat = ogBCD.GetField("ADE","TYPE",olAccountType,"FORM");

	// Urno des ADE Datensatzes erhalten
	CString olAdeUrno = ogBCD.GetField("ADE","TYPE",olAccountType,"URNO");

	CString olAdeDper = ogBCD.GetField("ADE","TYPE",olAccountType,"DPER");
	if (olAdeDper == "Y")
	{
		opFromDate.SetDate(opFromDate.GetYear(), 12, 1);
		opToDate.SetDate(opFromDate.GetYear(), 12, 31);
	}
	else if (olAdeDper == "L")
	{
		opFromDate.SetDate(opToDate.GetYear(), opToDate.GetMonth(), opToDate.GetDay());
	}

	// Alle ADS Datens�tze durchgehen, die sich auf diese URNO beziehen.
	// F�r die Reihenfolge ist das Feld LNUM relevant.
	CString olCount;
	CString olAdeToUse,olAdsToUse;

	// Ergebnisswert
	double dlResult = 0;

	// Alles Ok ?
	bool blOk = true;
	int ilCount = 0;
	do
	{
		// Ermittlung des zu Bearbeitenden Accounts 
		ilCount++;
		olCount.Format("%d",ilCount);

		// Suche ADS mit der Urno des ADE und der fortlaufenden Nummer
		olAdsToUse = ogBCD.GetFieldExt("ADS", "ADEU", "LNUM",olAdeUrno, olCount, "URNO");

		if (olAdsToUse.IsEmpty())
			break;

		// Operator erhalten
		CString olOperatorToUse = ogBCD.GetField("ADS", "URNO", olAdsToUse, "OPER");

		// Open oder Close Feld benutzen
		CString olFieldToUse = ogBCD.GetField("ADS", "URNO", olAdsToUse, "OLCL");

		// Monats Differenz erhalten
		int ilUsmoToUse = atoi(ogBCD.GetField("ADS", "URNO", olAdsToUse, "USMO"));

		// Interne Nummes des zugeh�rigen ADE Datensatzes ermitteln
		olAdeToUse = ogBCD.GetField("ADS", "URNO", olAdsToUse, "TYPE");

		// -------- ADE Auswerten Datensatz --------------------------------------------------------------------
		// Typ erhalten (statisch oder dynamisch hier m�glich)
		CString olTypeToUse = ogBCD.GetField("ADE","TYPE",olAdeToUse,"KTYP");
		// Account als UINT
		UINT ilAdeToUse = atoi(olAdeToUse);

		rlAccountReturnStruct.dDouble = 0;

		// Eigendlichen Wert ermitteln.
		if (olTypeToUse == "S")
		{
			ACCOUNTRETURNSTRUCT olReturnStruct = GetStaticAccountByStaffAndMonth(ilAdeToUse, pStaffUrno,opFromDate,opToDate,ilUsmoToUse,olTypeToUse);

			// Pr�fen ob ein Fehler aufgetreten ist.
			if (!olReturnStruct.bBool)
				blOk = false;

			rlAccountReturnStruct.oString			= olReturnStruct.oString;
			rlAccountReturnStruct.dDouble			= olReturnStruct.dDouble;
			rlAccountReturnStruct.oValueDefinition  = olReturnStruct.oValueDefinition;

		}
		else if (olTypeToUse == "D")
		{
			//******************************************************************************
			//******************************************************************************
			// ACHTUNG HARDCODIERT !
			// Das Konto 26 wird hier als Statisches Konto interpretiert das aus der Datenbank
			// geholt wird (Er�ffnungswert f�r den Monat)
			//******************************************************************************
			//******************************************************************************
			ACCOUNTRETURNSTRUCT olReturnStruct = GetStaticAccountByStaffAndMonth(ilAdeToUse, pStaffUrno,opFromDate, opToDate,ilUsmoToUse,olFieldToUse,true);
			rlAccountReturnStruct.oString			= olReturnStruct.oString;
			rlAccountReturnStruct.dDouble			= olReturnStruct.dDouble;
			rlAccountReturnStruct.oValueDefinition  = olReturnStruct.oValueDefinition;

			// Pr�fen ob ein Fehler aufgetreten ist.
			if (!olReturnStruct.bBool)
				blOk = false;

			//******************************************************************************
			//******************************************************************************
			// HARDCODIERT ENDE
			//******************************************************************************
			//******************************************************************************

		}
		else if (olTypeToUse == "T")
		{
			// Fehler, darf nicht sein !
		}

		// Operator auf erhaltenen Wert anwenden.
		if (rlAccountReturnStruct.oValueDefinition == "STRINGorSINGEL")
		{
			rlAccountReturnStruct.oString = rlAccountReturnStruct.oString.Left(6);
			rlAccountReturnStruct.bBool = blOk;

		}
		else
		{
			dlResult = UseOperator(dlResult,olOperatorToUse, rlAccountReturnStruct.dDouble);
			rlAccountReturnStruct.bBool = blOk;
		}
	} while(!olAdsToUse.IsEmpty());

	// Endg�ltige Werte eintragen und zur�ckgeben.
	rlAccountReturnStruct.dDouble = dlResult;

	return rlAccountReturnStruct;

CCS_CATCH_ALL;
// Default-initialisiertes Objekt zur�ckgeben (Werte siehe Header)
return rlError;
}

//*****************************************************************************************
// Operator auf erhaltenen Wert anwenden.
//*****************************************************************************************

double CAccounts::UseOperator(double dlResult,CString olOperatorToUse,double dlValueToUse)
{
	if (olOperatorToUse == "+")
		return (dlResult + dlValueToUse);

	if (olOperatorToUse == "-")
		return (dlResult - dlValueToUse);

	if (olOperatorToUse == "/")
		return (dlResult / dlValueToUse);

	if (olOperatorToUse == "*")
		return (dlResult * dlValueToUse);

	// Fehler, keinen Operator anwenden
	return (dlResult);
}

//*****************************************************************************************
//*****************************************************************************************
// AZK Konto (alter Kram)
//*****************************************************************************************
//*****************************************************************************************

//*****************************************************************************************
// IsHolOrAbsencensCode: Pr�ft ob Urlaub oder ganzt�gige Abwesenheit vorliegt
//*****************************************************************************************


bool CAccounts::IsHolOrAbsencenCode(DRRDATA* popDrr)
{
	// Zugeordnete Abwesenheit ermitteln
	ODADATA* polOda = pomOdaData->GetOdaByUrno(popDrr->Bsdu);
	
	// Pr�fen ob eine Abwesenheit vorliegt
	if (polOda == NULL)
		return false;

	// Code umwandeln
	int ilCode = atoi(polOda->Sdac);

	//-------------------------------------------------------------------
	// Hardcodierte Regeln

	if (ilCode == 460) // Urlaub/Ferien
		return true;

	if ((ilCode <= 304) && (ilCode >= 301))
		return true;
	
	if (ilCode == 350)
		return true;

	if (ilCode == 470)
		return true;

	// -------------------------------------
	// Noch testen
	// -------------------------------------

	if (ilCode == 316 || ((ilCode <= 471) && (ilCode >= 493)))
	{
		// Pr�fen ob es eine Aushilfskraft ist.
		//Vertragliche Arbeitszeit ermittern
		CString olActualCot = ogScoData.GetCotAndCWEHBySurnWithTime(popDrr->Stfu,popDrr->Avfr);
		COTDATA *prlCot = ogCotData.GetCotByCtrc(olActualCot);
		if(prlCot != NULL)
		{
			// darf keine Aushilfe
			if (CString(prlCot->Ctrc) != "230" && CString(prlCot->Ctrc) != "231" && CString(prlCot->Ctrc) != "232")
				return true;
		}
	}
	return false;
}

//*****************************************************************************************
// IsIll: Pr�ft ob Krankheit vorliegt
//*****************************************************************************************

bool CAccounts::IsIllCode(DRRDATA* popDrr)
{
	// Zugeordnete Abwesenheit ermitteln
	ODADATA* polOda = pomOdaData->GetOdaByUrno(popDrr->Bsdu);
	
	// Pr�fen ob eine Abwesenheit vorliegt
	if (polOda == NULL)
		return false;

	// Code umwandeln
	int ilCode = atoi(polOda->Sdac);

	//-------------------------------------------------------------------
	// Hardcodierte Regeln

	if ((ilCode >= 401) && (ilCode <= 406))
		return true;

	if ((ilCode >= 410) && (ilCode <= 419))
		return true;

	return false;
}

//*****************************************************************************************
// IsIll: Pr�ft ob Schichtcode eingetragen wurde
//*****************************************************************************************

bool CAccounts::IsShiftCode(DRRDATA* popDrr)
{
	// ToDo Andorn:
	// Welche Planungsstufen sollen ber�cksichtig werden ?

	BSDDATA* polBsd = pomBsdData->GetBsdByUrno(popDrr->Bsdu);
	
	if (polBsd == NULL)
		return false;
	else
		return true;
}

//*****************************************************************************************
// Exitieren Informationen was er f�r Dienst gehabt h�tte w�re er 
// nicht krank gewesen.
//*****************************************************************************************

DRRDATA* CAccounts::HasShiftInfo(DRRDATA* prlDrr)
{
	if (prlDrr == NULL)
		return NULL;

	// Pr�fen ob es einen DRR mit Langzeitdienstplan gibt
	DRRDATA* polInfoDrr = pomDrrData->GetDrrByKey(prlDrr->Sday, prlDrr->Stfu, "1", "L");

	if (polInfoDrr == NULL)
		return NULL;

	return polInfoDrr;
}

//*****************************************************************************************
// Wochenarbeitzeit/5
//*****************************************************************************************


double CAccounts::GetCommonDayWorkingTime(DRRDATA* popDrr)
{
CCS_TRY;
	if (popDrr == NULL)
		return 0;

	long	llResult = 0;

	// Daten aus SCOTAB
	// f�r den evaluierten Zeitraum g�ltige Vertragsdaten ermitteln
	CCSPtrArray<SCODATA> olScoData;
	pomScoData->GetScoListBySurnWithTime(popDrr->Stfu, popDrr->Avfr, &olScoData);

	// Aktuell g�ltigen Datensatz ermitteln
	long llScoUrno = pomScoData->GetScoUrnoWithTime(popDrr->Avfr, &olScoData);
	SCODATA* polSco = pomScoData->GetScoByUrno(llScoUrno);
	
	if (polSco == NULL)
		return 0;

	CString olCweh(polSco->Cweh);

	if (olCweh.GetLength() == 4)
	{
		int ilHour = atoi(olCweh.Left(2));
		int ilMin = atoi(olCweh.Right(2));
		llResult = ilHour*60 + ilMin;
	}
	else
		llResult  = 0;

	return llResult/5;
CCS_CATCH_ALL;
	return 0;
}


//************************************************************************************************************
// GetDailyGavMinutesByStaff: ermittelt die t�gliche Pausenzeit die zur Arbeitszeit z�hlt
// R�ckgabe:	bool
//************************************************************************************************************

bool CAccounts::GetDailyGAVMinutesByStaff(DRRDATA* popDrr, double &dpDrrMin)
{
CCS_TRY;
	// Hat der Mitarbeiter einen GAV Vertrag
	bool blIsGav = false;

	COleDateTime olDay;
	olDay = popDrr->Avfr;

	//  Handelt es sich um einen Sonntag ?
	if (olDay.GetDayOfWeek() == 1)
	{
		blIsGav = true;
	}
	else
	{
		// Handelt es sich um einen Feiertag ? (HOLTAB)
		CString olTmp;
		olTmp = olDay.Format("%Y%m%d000000");
		CString olUrno = ogBCD.GetField("HOL","HDAY",olTmp,"URNO");

		if (!olUrno.IsEmpty())
			blIsGav = true;
	}

	// Nur wenn ein Sonntag oder ein Feiertag vorliegt den Arbeitsvertrag pr�fen
	if (blIsGav)
	{
		blIsGav = false;

		//Vertragliche Arbeitszeit ermittern
		CString olActualCot = pomScoData->GetCotAndCWEHBySurnWithTime(popDrr->Stfu, popDrr->Avfr);
		COTDATA *prlCot = pomCotData->GetCotByCtrc(olActualCot);
		if(prlCot != NULL)
		{
			// GAV Code im Vertrag
			if (CString (prlCot->Ctrc) == "103")
				blIsGav = true;
		}
	}
	
	// Wenn alle Vorrausetzungen erf�llt sind, die Pausenzeit addieren
	if (blIsGav)
	{
		// Pausenzeit addieren
		dpDrrMin += atoi(popDrr->Sblu);
	}

	return true;

CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************
// GetDailyDRRMinutesByStaff: ermittelt die t�gliche Planarbeitszeit laut DRR
// R�ckgabe:	bool	DRR-Datensatz gefunden OK?
//************************************************************************************************************


bool CAccounts::GetDailyDRRMinutesByStaff(DRRDATA* popDrr, double &dpDrrMin)
{
CCS_TRY;


	CString olTest = popDrr->Avfr.Format("%H.%M.%S ---") + popDrr->Avto.Format("%H.%M.%S") ;
	//TRACE(" Accounts DrrMinutes: %s\n",olTest);
	
	// Minuten zur�cksetzen
	dpDrrMin = 0;

	COleDateTimeSpan olShiftTime;

	// L�nge der Schicht inklusive Pause
	olShiftTime = popDrr->Avto - popDrr->Avfr;

	olTest = olShiftTime.Format("%H.%M.%S");
	//TRACE(" Accounts BreakMinutes: %s\n",olTest);

	// L�nge der Schicht in Minuten
	dpDrrMin = olShiftTime.GetHours() * 60;
	dpDrrMin += olShiftTime.GetMinutes();

	// unbezahlte Pausenl�nge abziehen
	dpDrrMin -= atoi(popDrr->Sblu);
	
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************
// GetDailyTotalMinutesByStaff: ermittelt die t�gliche Gesamtarbeitszeit. Wenn 
//	<bpReturnOnError> nicht gesetzt ist, werden Fehler beim Ermitteln von
//	Datens�tzen ignoriert und mit 0-Werten gerechnet. ACHTUNG: der �bergebene Wert wird
//	nicht mit 0 initialisiert, um eventuelle Er�ffnungswerte nicht zu �berschreiben.
// R�ckgabe:	bool	true	->	alle Werte konnten ermittelt werden
//						false	->	mindestens eine aufgerufene Fkt. konnte
//									ben�tigte Datens�tze nicht finden
//************************************************************************************************************



bool CAccounts::GetDailyTotalAZKMinutesByStaff(long lpStaffUrno, COleDateTime opActualDayOle,
											double &dpDrrMin,double &dpBreakMin, bool bpReturnOnError )
{
CCS_TRY;
	bool blAccountOK = true;
	dpDrrMin = 0;
	dpBreakMin = 0;
	
	// Werte f�r Schl�ssel
	CString olSday = opActualDayOle.Format("%Y%m%d");

	// DRR mit Planungsstufe aktiv ermitteln
	DRRDATA *prlDrr = pomDrrData->GetDrrByRoss(olSday,lpStaffUrno,"1","","A");

	//----------------------------------------------------------------
	// 	Pr�fen ob ein aktiver DRR gefunden wurde
	if (prlDrr == NULL)
	{
		// WAS SOLL DANN PASSIEREN ?
		dpDrrMin = dpBreakMin = 0;
	}
	else
	{
		// DRR G�ltigkeitspr�fung
		if ((prlDrr->Avfr.GetStatus() != COleDateTime::valid) || (prlDrr->Avto.GetStatus() != COleDateTime::valid))
		{
			// Fehler
			dpDrrMin = dpBreakMin = 0;
			return false;
		}

		if (IsShiftCode(prlDrr))
		{
			// -> Schicht wurde gefunden
			// t�gliche, geplante Soll-Arbeitszeit ermitteln
			if(!GetDailyDRRMinutesByStaff(prlDrr,dpDrrMin))
			{
				// Fehler -> in bool merken f�r sp�tere Auswertung (siehe Funktionsende)
				blAccountOK = false;
			}

			// Pausenzeit ber�cksichtigen (Wenn MA ein GAV Vertrag hat)
			if(!GetDailyGAVMinutesByStaff(prlDrr,dpBreakMin))
			{
				// Fehler -> in bool merken f�r sp�tere Auswertung (siehe Funktionsende)
				blAccountOK = false;
			}
		}

		// Urlaub oder ganzt�gige betriebliche Abwesenheit
		else if (IsHolOrAbsencenCode(prlDrr))
		{
			dpDrrMin += GetCommonDayWorkingTime(prlDrr);
		}
		// Krankheit
		else if (IsIllCode(prlDrr))
		{
			// Exitieren Informationen was er f�r Dienst gehabt h�tte w�re er 
			// nicht krank gewesen.
			DRRDATA* polInfoDrr = HasShiftInfo(prlDrr);
			if (polInfoDrr == NULL)
			{
				// -> regul�re Schicht
				dpDrrMin += GetCommonDayWorkingTime(prlDrr);
			}
			else
			{
				// Pr�fen ob es eine Abwesenheit ist
				if (pomOdaData->GetOdaByUrno(polInfoDrr->Bsdu) == NULL)
				{
					// -> normale Schicht
					// t�gliche, geplante Soll-Arbeitszeit ermitteln
					if(!GetDailyDRRMinutesByStaff(polInfoDrr,dpDrrMin))
					{
						// Fehler -> in bool merken f�r sp�tere Auswertung (siehe Funktionsende)
						blAccountOK = false;
					}
						// Pausenzeit ber�cksichtigen (Wenn MA ein GAV Vertrag hat)
					if(!GetDailyGAVMinutesByStaff(polInfoDrr,dpBreakMin))
					{
						// Fehler -> in bool merken f�r sp�tere Auswertung (siehe Funktionsende)
						blAccountOK = false;
					}
				}
				else
					// -> Abwesenheit
					dpDrrMin += 0;
			}
		}
	}

	return blAccountOK;
CCS_CATCH_ALL;
return false;

}

//************************************************************************************************************
//************************************************************************************************************
//************************************************************************************************************
// Vacation Kontos
//************************************************************************************************************
//************************************************************************************************************
//************************************************************************************************************


//************************************************************************************************************
// FillRunningAccount_Vacation_ByStaff: ermittelt die Arbeitszeitkonten des Mitarbeiters
//	mit der URNO <lpStaffUrno> innerhalb des Zeitraumes <opAccountDateFrom> 
//	bis <opAccountDateTo>.
//************************************************************************************************************

bool CAccounts::GetDailyTotalVACMinutesByStaff(int ipOffDayCount,int ipCourseDayCount,long lpStaffUrno, COleDateTime opDay, double &dpResult)

{
	dpResult = 0;
	return true;
}

//--------------------------------------------------------
// Hilfsfunktion zum Runden
// ACHTUNG: Was passiert mit negativen Zahlen ????
//--------------------------------------------------------

int CAccounts::Round(double dpValue)
{
CCS_TRY;

	int x = (int)floor(dpValue);
	double ilRest = dpValue - x;

	if (ilRest >= 0.5)
		return (int)ceil(dpValue);
	else
		return (int)floor(dpValue);

CCS_CATCH_ALL;
}

//************************************************************
// Z�hlt die OffTage und CourseTage an einem Tag
//************************************************************

void CAccounts::CountOffAndCourseDays(CString olSday,long lpStaffUrno,int& ilTempOff,int& ilTempCourse)
{
	ilTempOff = 0;
	ilTempCourse = 0;

	// DRR mit Planungsstufe aktiv ermitteln
	DRRDATA *prlDrr = pomDrrData->GetDrrByRoss(olSday,lpStaffUrno);

	if (prlDrr != NULL)
	{
		// Zugeordnete Abwesenheit ermitteln
		ODADATA* polOda = pomOdaData->GetOdaByUrno(prlDrr->Bsdu);

		// Pr�fen ob eine Abwesenheit vorliegt
		if (polOda != NULL)
		{
			// Code umwandeln
			int ilCode = atoi(polOda->Sdac);
				// Urlaub/Ferien
			if (ilCode == 460) 
				ilTempOff++;
				// Kurs
			if (ilCode == 301 || ilCode == 302) 
				ilTempCourse++;
		}
	}
}











	


		

