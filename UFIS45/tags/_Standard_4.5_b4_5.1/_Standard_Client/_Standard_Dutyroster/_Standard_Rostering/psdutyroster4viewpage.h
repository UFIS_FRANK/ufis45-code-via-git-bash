#ifndef AFX_PSDUTYROSTER4VIEWPAGE_H__AAB2A221_7A21_11D2_8061_004095434A85__INCLUDED_
#define AFX_PSDUTYROSTER4VIEWPAGE_H__AAB2A221_7A21_11D2_8061_004095434A85__INCLUDED_

// PSDutyRoster4ViewPage.h : Header-Datei
//
#include <Ansicht.h>
#include <RosterViewPage.h>
#include <DutyRoster_View.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld PSDutyRoster4ViewPage 

class PSDutyRoster4ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSDutyRoster4ViewPage)

// Konstruktion
public:
	PSDutyRoster4ViewPage();
	~PSDutyRoster4ViewPage();

// Dialogfelddaten
	//{{AFX_DATA(PSDutyRoster4ViewPage)
	enum { IDD = IDD_PSDUTYROSTER4 };
	CStatic		m_S4;
	CStatic		m_S3;
	CStatic		m_S2;
	CButton		m_R_SortPeriode;
	CButton		m_R_SortDay;
	CComboBox	m_C_Grupp;
	CButton		m_NoGrupp;
	CButton		m_Grupp;
	CButton		m_R_Perc;
	CButton		m_R_Shnm;
	CButton		m_R_Free;
	CButton		m_R_FLName;
	CButton		m_Plan4;
	CButton		m_Plan3;
	CButton		m_Plan2;
	CButton		m_Check_Sort;
	//}}AFX_DATA

	BOOL GetData();
	BOOL GetData(CStringArray &opValues);
	void SetData();
	void SetData(CStringArray &opValues);


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(PSDutyRoster4ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(PSDutyRoster4ViewPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnGrupp();
	afx_msg void OnNogrupp();
	afx_msg void OnBStartConfig();
	afx_msg void OnRFlname();
	afx_msg void OnRFree();
	afx_msg void OnRPerc();
	afx_msg void OnRShnm();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString omConfigString;
	CString omCalledFrom;
	bool bmPgpLoaded;
	void SetCalledFrom(CString opCalledFrom);
private:
	void HandleNameRadioButtons();
	CString omTitleStrg;
	void SetButtonText();
	void SetStatic();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSDUTYROSTER4VIEWPAGE_H__AAB2A221_7A21_11D2_8061_004095434A85__INCLUDED_
