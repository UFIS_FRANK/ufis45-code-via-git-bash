// PrintDailyRosterB.cpp : implementation file
//

#include <stdafx.h>
#include <PrintDailyRosterB.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

long CPrintDailyRosterB::lmViewGroupUrno = 0;
int imName;
BOOL bmSortFctc;

static	const char	*DayOfWeek[]	=	{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
};

//****************************************************************************
// CompareDrrByTimeFunc: vergleicht zwei Drr's nach Zeit, bei 
//	Gleichheit der Zeit nach Code.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

int CPrintDailyRosterB::CompareDrrByStfuAndTime (const DRRDATA **e1, const DRRDATA **e2)
{
	if (CPrintDailyRosterB::IsTeamLeader((*e1)) && CPrintDailyRosterB::IsTeamLeader((*e2)))
	{
		// both are team leader
		if ((bmSortFctc == TRUE) && (strcmp((**e1).Fctc,(**e2).Fctc) != 0))
			return strcmp((**e1).Fctc,(**e2).Fctc);
		else if ((**e1).Avfr > (**e2).Avfr)
			return 1;
		else if ((**e1).Avfr < (**e2).Avfr)
			return -1;
		else
		{
			if ((**e1).Avto > (**e2).Avto)
				return 1;
			else if ((**e1).Avto < (**e2).Avto)
				return -1;
			else
				return strcmp(ogStfData.GetName((**e1).Stfu, imName),
							  ogStfData.GetName((**e2).Stfu, imName));
		}
	}
	else if (CPrintDailyRosterB::IsTeamLeader((*e1)))
		return -1;
	else if (CPrintDailyRosterB::IsTeamLeader((*e2)))
		return 1;
	else if ((bmSortFctc == TRUE) && (strcmp((**e1).Fctc,(**e2).Fctc) != 0))
		return strcmp((**e1).Fctc,(**e2).Fctc);
	else if ((**e1).Avfr > (**e2).Avfr)
		return 1;
	else if ((**e1).Avfr < (**e2).Avfr)
		return -1;
	else
	{
		if ((**e1).Avto > (**e2).Avto)
			return 1;
		else if ((**e1).Avto < (**e2).Avto)
			return -1;
		else
			return strcmp(ogStfData.GetName((**e1).Stfu, imName),
						  ogStfData.GetName((**e2).Stfu, imName));
	}
}

//****************************************************************************
// CompareDrrByTimeFunc: vergleicht zwei Drr's nach Zeit, bei 
//	Gleichheit der Zeit nach Code.
// R�ckgabe:	<0	->	e1 < e2
//				==0	->	e1 == e2
//				>0	->	e1 > e2
//****************************************************************************

int CPrintDailyRosterB::CompareTeamsByTime (const Team **e1, const Team **e2)
{
	if ((**e1).omTeamLeaderStartTime > (**e2).omTeamLeaderStartTime)
		return 1;
	else if ((**e1).omTeamLeaderStartTime < (**e2).omTeamLeaderStartTime)
		return -1;
	else
	{
		if ((**e1).omTeamLeaderEndTime > (**e2).omTeamLeaderEndTime)
			return 1;
		else if ((**e1).omTeamLeaderEndTime < (**e2).omTeamLeaderEndTime)
			return -1;
		else
			return strcmp((**e1).omName,(**e2).omName);
	}
}

//****************************************************************************
// CompareShiftGroupsByGrpn: vergleicht zwei SGR's nach Namen 
//****************************************************************************

int CPrintDailyRosterB::CompareShiftGroupsByGrpn (const SGRDATA **e1, const SGRDATA **e2)
{
	return strcmp((**e1).Grpn,(**e2).Grpn);
}

/////////////////////////////////////////////////////////////////////////////
// CPrintDailyRosterB

IMPLEMENT_DYNCREATE(CPrintDailyRosterB, CPrintDailyRoster)

CPrintDailyRosterB::CPrintDailyRosterB()
{
	omType	  = "DAILYROSTERPLAN_B";
	omTitle   = LoadStg (IDS_STRING63463);  //"Tagesdienstplan Disponenten" 
	omCaption = omTitle;
	omView	  = "Default";
	omKeyItems = "";
	pomDutyRoster_View = GetViewInfo();
	ASSERT(pomDutyRoster_View);
	imName = pomDutyRoster_View->rmViewInfo.iEName;
}


CPrintDailyRosterB::CPrintDailyRosterB(const COleDateTime& ropDate,const CString& ropViewName,const CStringArray& ropStfUrnos,const CString& ropLevel,const CString& ropKeyItems)
{
	omDate	  = ropDate;
	omStfUrnos.Append(ropStfUrnos);
	omType	  = "DAILYROSTERPLAN_B";
	omTitle   = LoadStg (IDS_STRING63463);  //"Tagesdienstplan Disponenten"
	omCaption = omTitle;
	omView	  = ropViewName;	
	omLevel	  = ropLevel;
	pomDutyRoster_View = GetViewInfo();
	CPrintDailyRosterB::lmViewGroupUrno = pomDutyRoster_View->rmViewInfo.lPlanGroupUrno;
	imName = pomDutyRoster_View->rmViewInfo.iEName;
	ASSERT(pomDutyRoster_View);
	if(strstr(ropKeyItems,"SORTFUNCTION") == NULL)
		bmSortFctc = FALSE;
	else
		bmSortFctc = TRUE;
	omKeyItems = ropKeyItems;
}

CPrintDailyRosterB::~CPrintDailyRosterB()
{
	DeleteAll();
}


BEGIN_MESSAGE_MAP(CPrintDailyRosterB, CPrintDailyRoster)
	//{{AFX_MSG_MAP(CPrintDailyRosterB)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPrintDailyRosterB::DeleteAll()
{
	CString olName;
	OrgUnit*polOrgUnit;

	POSITION pos = omOrgUnits.GetStartPosition();
	while(pos != NULL)
	{
		omOrgUnits.GetNextAssoc(pos,olName,(void *&)polOrgUnit);
		delete polOrgUnit;
	}

	CPrintDailyRoster::DeleteAll();
}

BOOL CPrintDailyRosterB::BuildGroups()
{
	CString olOrgUnit;
	void*	polValue;
	for (int i = 0; i < omStfUrnos.GetSize(); i++)
	{
		olOrgUnit = ogSorData.GetOrgBySurnWithTime(atol(omStfUrnos[i]),omDate);	
		if (!omOrgUnits.Lookup(olOrgUnit,polValue))
		{
			OrgUnit *polOrgUnit  = new OrgUnit;
			if (BuildOrgUnit(polOrgUnit,olOrgUnit))
			{
				omOrgUnits.SetAt(olOrgUnit,polOrgUnit);
			}
			else
			{
				delete polOrgUnit;
			}
		}
	}

	return TRUE;
}

BOOL CPrintDailyRosterB::BuildOrgUnit(OrgUnit *popOrgUnit,const CString& ropOrgUnit)
{
	BOOL blRet = FALSE;
	CCSPtrArray<SGRDATA> olSgrList;
	ogSgrData.GetSgrsByTabn(olSgrList,"BSD","ROSTER");
	olSgrList.Sort(CompareShiftGroupsByGrpn);
	for (int i = 0; i < olSgrList.GetSize(); i++)
	{
		Group *polGroup  = new Group;
		polGroup->omName = olSgrList[i].Grpn;
		if (BuildGroup(polGroup,olSgrList[i],ropOrgUnit))
		{
			blRet = TRUE;
			if (polGroup->omTeams.GetCount())
			{
				popOrgUnit->omGroups.Add(polGroup);
			}
			else
			{
				delete polGroup;
			}
		}
		else
		{
			delete polGroup;
		}
	}

	olSgrList.DeleteAll();

	return blRet;
}

BOOL CPrintDailyRosterB::BuildGroup(Group *popGroup,SGRDATA& ropSgr,const CString& ropOrgUnit)
{
	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,ropSgr.Urno);
	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		BSDDATA *polBSD = ogBsdData.GetBsdByUrno(atoi(olSgmList[i].Uval));
		if (polBSD)
		{
			popGroup->omBasicShiftMap.SetAt(polBSD->Bsdc,polBSD);
		}
	}
	
	// prepare the allowed functions
	DutyRoster_View *polView = GetViewInfo();
	VIEWINFO *polViewInfo = &polView->rmViewInfo;
	CMapStringToPtr	omFunctionMap;
	CString olTmp = polViewInfo->oPfcStatUrnos;
	CStringArray olArr;
	CedaData::ExtractTextLineFast(olArr, olTmp.GetBuffer(0), ",");
	for (i = 0; i < olArr.GetSize(); i++)
	{
		PFCDATA *polPfc = ogPfcData.GetPfcByUrno (atol(olArr[i]));
		if (polPfc != NULL)
		{
			omFunctionMap.SetAt	(polPfc->Fctc, polPfc);
		}
	}

	// loop over all employees visible in this view
	for (i = 0; i < omStfUrnos.GetSize(); i++)
	{
		if (ogSorData.GetOrgBySurnWithTime(atol(omStfUrnos[i]),omDate) == ropOrgUnit)
		{
			CCSPtrArray<DRRDATA> olDrrList;
			ogDrrData.GetDrrListByKeyWithoutDrrn(omDate.Format("%Y%m%d"),atoi(omStfUrnos[i]),omLevel,&olDrrList);				
			for (int j = 0; j < olDrrList.GetSize(); j++)
			{
				void *ptr;
				if (popGroup->omBasicShiftMap.Lookup(olDrrList[j].Scod,ptr))
				{
					if (omFunctionMap.Lookup(olDrrList[j].Fctc,ptr))
					{
						DRGDATA *polDrg = ogDrgData.GetDrgBySdayAndStfu(omDate.Format("%Y%m%d"),atol(omStfUrnos[i]));
						if (polDrg)	// daily roster group ?
						{
							Team *polTeam = NULL;
							if (!popGroup->omTeams.Lookup(polDrg->Wgpc,(void *&)polTeam))
							{
																
								WGPDATA *polWgp = ogWgpData.GetWgpByWgpc(polDrg->Wgpc);
								if (polWgp)
								{
									if (pomDutyRoster_View->rmViewInfo.iGroup == 2)	// Daily roster group activated
									{
										if (polWgp->Pgpu == pomDutyRoster_View->rmViewInfo.lPlanGroupUrno)
										{
											polTeam			= new Team;
											polTeam->omName = polWgp->Wgpn;
											popGroup->omTeams.SetAt(polDrg->Wgpc,polTeam);
										}
									}
									else
									{
										polTeam			= new Team;
										polTeam->omName = polWgp->Wgpn;
										popGroup->omTeams.SetAt(polDrg->Wgpc,polTeam);
									}
								}
							}

							if (polTeam)
							{
								polTeam->omDailyRosterRecords.Add(&olDrrList[j]);
							}
							else
							{
								// add to functional map
								CString olFct = this->GetShiftFunction(&olDrrList[j]);
															
								if (!popGroup->omFunctions.Lookup(olFct,(void *&)polTeam))
								{
									polTeam = new Team;
									polTeam->omName = olFct;
									popGroup->omFunctions.SetAt(olFct,polTeam);
								}

								polTeam->omDailyRosterRecords.Add(&olDrrList[j]);
							}

							if (IsTeamLeader(&olDrrList[j]))
							{
								if (polTeam->omTeamLeaderStartTime.GetStatus() == COleDateTime::valid)
								{
									if (olDrrList[j].Avfr < polTeam->omTeamLeaderStartTime)
									{
										polTeam->omTeamLeaderStartTime = olDrrList[j].Avfr;
										polTeam->omTeamLeaderEndTime = olDrrList[j].Avto;
									}
								}
								else
								{
									polTeam->omTeamLeaderStartTime = olDrrList[j].Avfr;
									polTeam->omTeamLeaderEndTime = olDrrList[j].Avto;
								}
							}

							// check for absences
							BuildAbsences(&olDrrList[j],polTeam->omDailyRosterAbsences);
						}
						else	// static group
						{
							SWGDATA *polSwg = ogSwgData.GetSwgBySurnWithTime(atol(omStfUrnos[i]),omDate);
							if (polSwg)
							{
								Team *polTeam;
								if (!popGroup->omTeams.Lookup(polSwg->Code,(void *&)polTeam))
								{
									polTeam = new Team;

									WGPDATA *polWgp = ogWgpData.GetWgpByWgpc(polSwg->Code);
									if (polWgp)
										polTeam->omName = polWgp->Wgpn;
									else
										polTeam->omName = polSwg->Code;
									popGroup->omTeams.SetAt(polSwg->Code,polTeam);
								}

								if (IsTeamLeader(&olDrrList[j]))
								{
									if (polTeam->omTeamLeaderStartTime.GetStatus() == COleDateTime::valid)
									{
										if (olDrrList[j].Avfr < polTeam->omTeamLeaderStartTime)
										{
											polTeam->omTeamLeaderStartTime = olDrrList[j].Avfr;
											polTeam->omTeamLeaderEndTime = olDrrList[j].Avto;
										}
									}
									else
									{
										polTeam->omTeamLeaderStartTime = olDrrList[j].Avfr;
										polTeam->omTeamLeaderEndTime = olDrrList[j].Avto;
									}
								}

								polTeam->omDailyRosterRecords.Add(&olDrrList[j]);
								
								// check for absences
								BuildAbsences(&olDrrList[j],polTeam->omDailyRosterAbsences);
							}
							else
							{
								// add to functional map
								CString olFct = this->GetShiftFunction(&olDrrList[j]);
															
								Team *polTeam;
								if (!popGroup->omFunctions.Lookup(olFct,(void *&)polTeam))
								{
									polTeam = new Team;
									polTeam->omName = olFct;
									popGroup->omFunctions.SetAt(olFct,polTeam);
								}

								if (IsTeamLeader(&olDrrList[j]))
								{
									if (polTeam->omTeamLeaderStartTime.GetStatus() == COleDateTime::valid)
									{
										if (olDrrList[j].Avfr < polTeam->omTeamLeaderStartTime)
										{
											polTeam->omTeamLeaderStartTime = olDrrList[j].Avfr;
											polTeam->omTeamLeaderEndTime = olDrrList[j].Avto;
										}
									}
									else
									{
										polTeam->omTeamLeaderStartTime = olDrrList[j].Avfr;
										polTeam->omTeamLeaderEndTime = olDrrList[j].Avto;
									}
								}

								polTeam->omDailyRosterRecords.Add(&olDrrList[j]);				

								// check for absences
								BuildAbsences(&olDrrList[j],polTeam->omDailyRosterAbsences);
							}
						}
					}
				}
			}
		}
	}

	olSgmList.DeleteAll();
	return TRUE;
}

void CPrintDailyRosterB::GenerateSpecific(std::ofstream& of)
{
	CString olName;
	OrgUnit *polOrgUnit;

	for (POSITION pos = omOrgUnits.GetStartPosition(); pos;)
	{
		omOrgUnits.GetNextAssoc(pos,olName,(void *&)polOrgUnit);
		GenerateOrgUnit(of,olName,polOrgUnit);
	}
}

void CPrintDailyRosterB::GenerateOrgUnit(std::ofstream& of,const CString& ropOrgUnit,OrgUnit *popOrgUnit)
{
	if (popOrgUnit->omGroups.GetSize())
	{
		GenerateHeader(of,ropOrgUnit);
	}

	for (int i = 0; i < popOrgUnit->omGroups.GetSize(); i++)
	{
		Group *polGroup = &popOrgUnit->omGroups[i];		
		GenerateGroup(of,ropOrgUnit,polGroup);
	}
}

void CPrintDailyRosterB::GenerateGroup(std::ofstream& of,const CString& ropOrgUnit,Group *polGroup)
{

	CString olName;
	Team*	polTeam;
	int		i;

	omLine.Format("<=GROUP=>");
	of	<<	(const char *)omLine
		<<	(const char *)polGroup->omName
		<<	"<=\\=>" 
		<<	'\n'
		;

	if (pomDutyRoster_View->rmViewInfo.iGroup == 2)	// daily roster group
	{
		CCSPtrArray<Team> olTeamArray;
		for (i = 0; i < pomDutyRoster_View->rmViewInfo.oWorkGroupCodes.GetSize(); i++)
		{
			CString olWorkGroupCode = pomDutyRoster_View->rmViewInfo.oWorkGroupCodes[i];
			if (polGroup->omTeams.Lookup(olWorkGroupCode,(void *&)polTeam) && polTeam != NULL)
			{
				olTeamArray.Add(polTeam);
			}
		}
		olTeamArray.Sort(CompareTeamsByTime);
		for (i = 0; i < olTeamArray.GetSize(); i++)
		{
			GenerateTeam(of,&olTeamArray[i]);
		}
	}
	else
	{
		CCSPtrArray<Team> olTeamArray;
		for (POSITION pos = polGroup->omTeams.GetStartPosition(); pos != NULL;)
		{
			polGroup->omTeams.GetNextAssoc(pos,olName,(void *&)polTeam);
			olTeamArray.Add(polTeam);
			
		}
		olTeamArray.Sort(CompareTeamsByTime);
		for (i = 0; i < olTeamArray.GetSize(); i++)
		{
			GenerateTeam(of,&olTeamArray[i]);
		}
	}

	for (POSITION pos = polGroup->omFunctions.GetStartPosition(); pos != NULL;)
	{
		polGroup->omFunctions.GetNextAssoc(pos,olName,(void *&)polTeam);
		GenerateTeam(of,polTeam);
	}
}

void CPrintDailyRosterB::GenerateTeam(std::ofstream& of,Team *polTeam)
{
	polTeam->omDailyRosterRecords.Sort(CompareDrrByStfuAndTime);

	omLine.Format("<=SEPA=>");
	of	<<	(const char *)omLine
		<<	(const char *)polTeam->omName
		<<	"<=\\=>" 
		<<	'\n'
		;

	for (int i = 0; i < polTeam->omDailyRosterRecords.GetSize(); i++)
	{
		DRRDATA *polDrr = &polTeam->omDailyRosterRecords[i];
		STFDATA *polStf = ogStfData.GetStfByUrno(polDrr->Stfu);
		COleDateTimeSpan olDuration = polDrr->Avto - polDrr->Avfr;
		// Schichtdauer berechnen
		if(polDrr->Sblu != 0 && strlen(polDrr->Sblu))
		{
			int ilSblu = atoi(polDrr->Sblu);
			if (ilSblu > 0)
				olDuration -= COleDateTimeSpan(0,0,ilSblu,0);	// Pause aus DRR einbezihen
		}

		omLine.Format("");
		of	<<	(const char *)omLine
			<<	(const char *)polDrr->Avfr.Format("%H.%M") 
			<<	'|' 
			<<	'-' 
			<<	'|' 
			<<	(const char *)polDrr->Avto.Format("%H.%M") 
			<<	'|'
			<<	polDrr->Scod 
			<<	'|' 
			<<	(const char *)GetShiftFunction(polDrr) 
			<<	'|'
			<<	(const char *)ogStfData.GetName(polStf->Urno, imName)
			<<	'|'
			<<	(const char *)olDuration.Format("%H:%M")
			<<	'\n'
			;
	}

	polTeam->omDailyRosterAbsences.Sort(CompareAbsenceBySdac);

	CString olSdac;
	for (i = 0; i < polTeam->omDailyRosterAbsences.GetSize(); i++)
	{
		Absence *polAbs = &polTeam->omDailyRosterAbsences[i];
		if (polAbs->Sdac != olSdac)
		{
			omLine.Format("");
			of	<<	(const char *)omLine
				<<	(const char *)polAbs->Sdan
				<<	'\n'
				;

		}

		STFDATA *polStf = ogStfData.GetStfByUrno(polAbs->Stfu);
		COleDateTimeSpan olDuration = polAbs->Abto - polAbs->Abfr;

		omLine.Format("");
		of	<<	(const char *)omLine
			<<	(const char *)polAbs->Abfr.Format("%H.%M") 
			<<	'|' 
			<<	'-' 
			<<	'|' 
			<<	(const char *)polAbs->Abto.Format("%H.%M") 
			<<	'|'
			<<	(const char *)polAbs->Sdac
			<<	'|' 
			<<	'|'
			<<	(const char *)ogStfData.GetName(polStf->Urno, imName)
			<<	'|'
			<<	(const char *)olDuration.Format("%H:%M")
			<<	'\n'
			;
	}
}

BOOL CPrintDailyRosterB::IsTeamLeader(const DRRDATA *popDrr)
{
	COleDateTime olSday;
	CedaDataHelper::DateStringToOleDateTime(popDrr->Sday,olSday);

/*	SWGDATA *polSwg = ogSwgData.GetSwgBySurnWithTime(popDrr->Stfu,olSday);
	if (!polSwg)
		return FALSE;
	long lpUrno = ogWgpData.GetPgpuByWgpc(polSwg->Code);

	PGPDATA *polPgp = ogPgpData.GetPgpByUrno(lpUrno);
*/
	long lpUrno = lmViewGroupUrno;

	PGPDATA *polPgp = ogPgpData.GetPgpByUrno(lpUrno);

	if (polPgp)
	{
		CString olFct = popDrr->Fctc;
		if (!olFct.GetLength())
		{
			olFct = ogSpfData.GetMainPfcBySurnWithTime(popDrr->Stfu,olSday);
		}

		if (strstr(polPgp->Pgpm,olFct) == polPgp->Pgpm)
			return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CPrintDailyRosterB message handlers
