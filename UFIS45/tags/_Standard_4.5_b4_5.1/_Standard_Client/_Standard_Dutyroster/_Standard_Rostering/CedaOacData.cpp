// CedaOacData.cpp: - Klasse f�r die Handhabung von OAC-Daten (Daily Roster Groups - 
//  Tagesdienstplan Arbeitsgruppen)
//

// BDA 16.02.2000 erstellt aus CedaDrdData.cpp

#include <stdafx.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessOacCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_OAC_CHANGE, BC_OAC_NEW und BC_OAC_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaOacData::ProcessOacBc() der entsprechenden 
//	Instanz von CedaOacData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessOacCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaOacData *polOacData = (CedaOacData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polOacData->ProcessOacBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
// Implementation der Klasse CedaOacData
//************************************************************************************************************************************************


//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaOacData::CedaOacData(CString opTableName, CString opExtName) : CedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r OAC-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(OACDATA, OacDataRecInfo)
		FIELD_CHAR_TRIM (Ctrc,"CTRC")	//	Contract Code from COT
		FIELD_CHAR_TRIM (Hopo,"HOPO") 	//	Home Airport
		FIELD_CHAR_TRIM (Sdac,"SDAC") 	//	Absence Code from ODA
		FIELD_LONG  	(Urno,"URNO") 	//	Eindeutige Datensatz-Nr.
	END_CEDARECINFO
	
	// Infostruktur kopieren
	for (int i = 0; i < sizeof(OacDataRecInfo)/sizeof(OacDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&OacDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	}
	
	// Tabellenname per Default auf OAC setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);
	
	// Feldnamen initialisieren
	strcpy(pcmListOfFields,	"CTRC,HOPO,SDAC,URNO");

	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
	// Datenarrays initialisieren
	omUrnoMap.InitHashTable(256);
	Register();
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaOacData::~CedaOacData()
{
	TRACE("CedaOacData::~CedaOacData called\n");
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaOacData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY;
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaOacData::Register(void)
{
CCS_TRY;
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// OAC-Datensatz hat sich ge�ndert
	DdxRegister((void *)this,BC_OAC_CHANGE,   "CedaOacData", "BC_OAC_CHANGE",        ProcessOacCf);
	DdxRegister((void *)this,BC_OAC_NEW,      "CedaOacData", "BC_OAC_NEW",		   ProcessOacCf);
	DdxRegister((void *)this,BC_OAC_DELETE,   "CedaOacData", "BC_OAC_DELETE",        ProcessOacCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL;
}

//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaOacData::ClearAll(bool bpUnregister)
{
CCS_TRY;
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz abgemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// ReadOacByUrno: liest den OAC-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	true, wenn Datensatz erfolgreich gelesen, sonst false
//************************************************************************************************************************************************

bool CedaOacData::ReadOacByUrno(long lpUrno, OACDATA *prpOac)
{
CCS_TRY;
	CString olWhere;
	olWhere.Format("'%ld'", lpUrno);
	// Datensatz aus Datenbank lesen
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, olWhere);
		WriteInRosteringLog();
	}

	if (CedaAction2("RT", (char*)(LPCTSTR)olWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	OACDATA *prlOac = new OACDATA;
	// und initialisieren
	if (GetBufferRecord2(0,prpOac) == true)
	{  
		// umschreiben::: TRACE("ReadOacByUrno: Datensatz gefunden,\nDrrn: %s\nFctc: %s\nHopo: %s\nSday: %s\nStfu: %d\nUrno: %d\nWgpc: %s\n",
			//prlOac->Drrn, prlOac->Fctc, prlOac->Hopo, prlOac->Sday, prlOac->Stfu, prlOac->Urno, prlOac->Wgpc);
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Kein OAC gefunden - aufr�umen und terminieren
		delete prlOac;
		return false;
	}

	return false;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaOacData::Read(char *pspWhere, bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY;
	// Return-Code f�r Funktionsaufrufe
	//bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{
		//if (!CedaData::CedaAction("RT", "")) return false;
		if (!CedaAction2("RT", "")) return false;
	}
	else
	{
		//if (!CedaData::CedaAction("RT", pspWhere)) return false;
		if (!CedaAction2("RT", pspWhere)) return false;
	}

    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecord = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		OACDATA *prlOac = new OACDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord2(ilCountRecord,prlOac);
		if(!blMoreRecords)
		{
			// kein weiterer Datensatz
			delete prlOac;
			break;
		}
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecord++;

		if(IsValidOac(prlOac))
		{
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlOac, OAC_NO_SEND_DDX))
			{
				// Pointer l�schen
				delete prlOac;
			}
#ifdef TRACE_FULL_DATA
			else
			{
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlOac);
				WriteLogFull("");
			}
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlOac; // defect
		}
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen OAC
	TRACE("Read-Oac: %d gelesen\n",ilCountRecord);
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord, pcmTableName);
		WriteInRosteringLog();
	}

    return true;
CCS_CATCH_ALL;
return false;
}

//*******************************************************************************************************************
// ReadSpecialData: liest Datens�tze mit Hilfe eines �bergebenen SQL-Strings ein.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************************************************

bool CedaOacData::ReadSpecialData(CCSPtrArray<OACDATA> *popOacArray, char *pspWhere,
								  char *pspFieldList, char *pcpSort, bool ipSYS/*=true*/)
{
CCS_TRY;
	bool ilRc = true;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s ORDER BY %s>",pcmTableName, pspWhere,pcpSort);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,"",pspWhere,pcpSort,pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT",pcmTableName,"",pspWhere,pcpSort,pcgDataBuf) == false) return false;
	}
	if(popOacArray != NULL)	// Datens�tze in den �bergebenen Array
	{
		CString olFieldList(pspFieldList);
		if(olFieldList.IsEmpty())
			olFieldList = pcmListOfFields;	// GetBufferRecord kann leider mit "" nichts anfangen
			
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			OACDATA *prlOac = new OACDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prlOac,olFieldList)) == true)
			{
				// change::: TRACE("CedaOacData::ReadSpecialData(): OAC gelesen, Drrn: %s, Fctc: %s, Hopo: %s, Sday: %s, Stfu: %d, Urno: %d, Wgpc: %s\n",
					//prlOac->Drrn, prlOac->Fctc, prlOac->Hopo, prlOac->Sday, prlOac->Stfu, prlOac->Urno, prlOac->Wgpc);
				popOacArray->Add(prlOac);
			}
			else
			{
				delete prlOac;
			}
		}
		if(popOacArray->GetSize() == 0) return false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpOac> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaOacData::Insert(OACDATA *prpOac, bool bpSave /*= true*/)
{
CCS_TRY;
	// �nderungs-Flag setzen
	prpOac->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpOac) == false) return false;
	// Broadcast OAC_NEW abschicken
	InsertInternal(prpOac,true);
    return true;
CCS_CATCH_ALL;
return false;
}

/*********************************************************************************************
InsertInternal: 1. pr�fft, ob der neue Datensatz korrekt ist, u.a. auch ob schon ein Datensatz
mit gleichem prim�ren Schl�ssel "SDAY-DRRN-STFU" gespeichert ist, wenn ja - return FALSE 
(es kann nur einen geben), wenn nein
2. f�gt den internen Arrays einen neuen Datensatz hinzu. 
Der Datensatz muss fr�her oder sp�ter explizit durch Aufruf von Save() oder Release() in der Datenbank 
gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
R�ckgabe:	false	->	Fehler
			true	->	alles OK
*********************************************************************************************/

bool CedaOacData::InsertInternal(OACDATA *prpOac, bool bpSendDdx)
{
CCS_TRY;
	if(!IsValidOac(prpOac))
		return false;

	// Datensatz intern anf�gen
	omData.Add(prpOac);

	// Primary-Key dieses Datensatzes als String
	CString olPrimaryKey;
	olPrimaryKey.Format("%s-%s",prpOac->Ctrc, prpOac->Sdac);
//	TRACE("Speichere OAC mit Schl�ssel = %s\n",olPrimaryKey);
	omKeyMap.SetAt(olPrimaryKey,prpOac);

	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpOac->Urno,prpOac);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,OAC_NEW,(void *)prpOac);
	}
	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpOac->Urno> und speichert ihn
//	in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaOacData::Update(OACDATA *prpOac)
{
CCS_TRY;
	// Datensatz raussuchen
	if (GetOacByUrno(prpOac->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpOac->IsChanged == DATA_UNCHANGED)
		{
			prpOac->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(Save(prpOac) == false) return false; 
		// Broadcast OAC_CHANGE versenden
		UpdateInternal(prpOac);
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaOacData::UpdateInternal(OACDATA *prpOac, bool bpSendDdx /*true*/)
{
CCS_TRY
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
		ogDdx.DataChanged((void *)this,OAC_CHANGE,(void *)prpOac);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaOacData::DeleteInternal(OACDATA *prpOac, bool bpSendDdx /*true*/)
{
CCS_TRY;
	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpOac->Urno);

	// Primary-Key dieses Datensatzes als String
	CString olPrimaryKey;
	olPrimaryKey.Format("%s-%s",prpOac->Ctrc, prpOac->Sdac);
//	TRACE("L�sche OAC mit Schl�ssel = %s\n",olPrimaryKey);
	omKeyMap.RemoveKey(olPrimaryKey);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpOac->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}

	// Broadcast senden
	// Wir brauchen keinen OAC-Pointer mitzuschicken, viel sauberer und konsequenter ist, wenn
	// die Daten direkt aus dem Bestand gelesen werden. Gel�schter Datensatz wird damit nicht 
	// auftauchen.
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,OAC_DELETE,(void *)0);
	}

CCS_CATCH_ALL;
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpOac> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaOacData::Delete(OACDATA *prpOac, BOOL bpWithSave)
{
CCS_TRY;
	// Flag setzen
	prpOac->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpOac)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpOac,true);

	return true;
CCS_CATCH_ALL;
return false;
}

//*********************************************************************************************
// ProcessOacBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaOacData::ProcessOacBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;
#ifdef _DEBUG
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaOacData::ProcessOacBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif // _DEBUG

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	OACDATA *prlOac = NULL;
	
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlBcStruct->Cmd, prlBcStruct->Object, prlBcStruct->Twe, prlBcStruct->Selection, prlBcStruct->Fields, prlBcStruct->Data,prlBcStruct->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	case BC_OAC_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermitteln
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlOac = GetOacByUrno(llUrno);
			if(prlOac != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlOac,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlOac);
				break;
			}
			// nein -> gehe zum Insert, !!! weil es kann sein, dass diese �nderung den Mitarbeiter in View laden soll
		}
	case BC_OAC_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlOac = new OACDATA;
			GetRecordFromItemList(prlOac,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlOac, OAC_SEND_DDX);
		}
		break;
	case BC_OAC_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermitteln
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlOac = GetOacByUrno(llUrno);
			if (prlOac != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlOac);
			}
		}
		break;
	default:
		break;
	}
#ifdef _DEBUG
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaOacData::ProcessOacBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
#endif // _DEBUG
CCS_CATCH_ALL;
}

//*********************************************************************************************
// GetOacByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

OACDATA *CedaOacData::GetOacByUrno(long lpUrno)
{
CCS_TRY;
	// der Datensatz
	OACDATA *prpOac;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpOac) == TRUE)
	{
		return prpOac;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//*********************************************************************************************
// GetOacByKey: sucht den Datensatz mit dem Prim�rschl�ssel aus 
//	Contract Code (<opCtrc>) und Absence Code (<opSdac>).
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

OACDATA *CedaOacData::GetOacByKey(CString opCtrc, CString opSdac)
{
	// Prim�rschl�ssel generieren
	// Primary-Key als String
	CString olPrimaryKey;
	olPrimaryKey.Format("%s-%s",opCtrc, opSdac);
	//	TRACE("CedaOacData::GetOacByKey: Suche OAC mit Key = %s:\n",olPrimaryKey);
	// der Datensatz
	OACDATA *prlOac = NULL;
	// Datensatz suchen
	if (omKeyMap.Lookup(olPrimaryKey,(void *&)prlOac) == TRUE)
	{
		// Datensatz gefunden -> Zeiger darauf zur�ckgeben
//		TRACE("CedaOacData::GetOacByKey: OAC gefunden\n",olPrimaryKey);
		return prlOac;
	}
	// Datensatz nicht gefunden -> R�ckgabe NULL
	return NULL;
}

/*********************************************************************************************
IsValidOac: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten OACs
0. Die L�nge der Datenfelder pr�fen
1. Datenfelder pr�fen:
Ctrc 
*********************************************************************************************/

bool CedaOacData::IsValidOac(OACDATA *popOac)
{
CCS_TRY;
	//EmptyLogString();

	if(!popOac)
	{
		return false;
	}

	/*
	0. Die L�nge der Datenfelder pr�fen
	1. Datenfelder pr�fen:
	Dors	'D', 'S' oder ''
	Rosl	ein char
	Sjob	'N', 'L', ''
	Stfu	!= 0
	Urno	!= 0
	*/
	if(	strlen(popOac->Ctrc) > OAC_CTRC_LEN ||
		strlen(popOac->Hopo) > HOPO_LEN ||
		strlen(popOac->Sdac) > SDAC_LEN ||
		!popOac->Urno)
	{
		ogRosteringLogText += "OAC field length is wrong\n";
		TraceOacData(popOac);
		return false;
	}

	if(strcmp(popOac->Hopo,pcmHomeAirport))
	{
		ogRosteringLogText += "HOPO is wrong\n";
		TraceOacData(popOac);
		return false;
	}

	if(strlen(popOac->Sdac) > 0)
	{
		if(!ogOdaData.GetOdaBySdac(CString(popOac->Sdac)))
		{
			CString olErr;
			olErr.Format("Absence code '%s' in Absences Table not found, OACTAB data corrupt.\n",
				popOac->Sdac);
			ogErrlog += olErr;

			ogRosteringLogText += " ODADATA with OAC.SDAC not found \n";
			TraceOacData(popOac);
			return false;
		}
	}

	// char Ctrc[OAC_CTRC_LEN+2];	//	Contract Code from COT
	if(!ogCotData.GetCotByCtrc(CString(popOac->Ctrc)))
	{
		CString olErr;
		olErr.Format("Contract code '%s' in Contract Types Table not found, OACTAB data corrupt.\n",
			popOac->Ctrc);
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)popOac, "OACTAB.CTRC in COTTAB not found");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;	// 1. Defekter Datensatz
	}

	// OAC ist g�ltig
	return true;
CCS_CATCH_ALL;
return false;
}

/*********************************************************************************
*********************************************************************************/
void CedaOacData::TraceOacData(OACDATA* prpOac)
{
	GetDefectDataString(ogRosteringLogText, (void *)prpOac);

#ifdef _DEBUG
	TRACE("%s\n",(LPCTSTR)ogRosteringLogText);
	Sleep(5);
#endif _DEBUG
	WriteInRosteringLog(LOGFILE_TRACE);
}


//*******************************************************************************
// Save: speichert den Datensatz <prpOac>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaOacData::Save(OACDATA *prpOac)
{
CCS_TRY;
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpOac->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpOac->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOac);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpOac->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpOac->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOac);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpOac->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpOac->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaOacData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CedaData::CedaAction()
//*************************************************************************************

bool CedaOacData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY;
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************
//************************************************************************************************************************
// die folgenden Funktionen sind Hilfsfunktionen zum Manipulieren von 
// Objekten des Typs OACDATA. Die Funktionen wurden als Member der Klasse 
// CedaOacData angelegt, um den Zugriff und die Manipulation von OAC-Datens�tzen
// zu zentralisieren und in einer Klasse zu kapseln.
//************************************************************************************************************************
//************************************************************************************************************************

//************************************************************************************************************************************************
// CreateOacData: initialisiert eine Struktur vom Typ OACDATA.
// R�ckgabe:	ein Zeiger auf den ge�nderten / erzeugten Datensatz oder
//				NULL, wenn ein Fehler auftrat.
//************************************************************************************************************************************************

OACDATA* CedaOacData::CreateOacData()
{
CCS_TRY;
	// Objekt erzeugen
	OACDATA *prlOacData = new OACDATA;
	// �nderungsflag setzen
	prlOacData->IsChanged = DATA_NEW;
	// n�chste freie Datensatz-Urno ermitteln und speichern
	prlOacData->Urno = ogBasicData.GetNextUrno();
	// Hopo
	strcpy(prlOacData->Hopo,pcmHomeAirport);
	// Zeiger auf Datensatz zur�ck
	return prlOacData;
CCS_CATCH_ALL;
return NULL;
}

/**********************************************************************************************************
// OACDATA erzeugen und initialisieren mit angegebenen Parametern ohne Aufnahme in die interne Datenhaltung
beliebige Pointer k�nnen NULL sein
**********************************************************************************************************/
OACDATA* CedaOacData::CreateOacData(char* ppHopo)
{
	CCS_TRY;
	//Parameter testen
	if(	ppHopo != 0 && strlen(ppHopo) > HOPO_LEN) return 0;
	
	// Objekt erzeugen
	OACDATA *prlOacData = new OACDATA;
	if(!prlOacData) return 0;
	// �nderungsflag setzen
	prlOacData->IsChanged = DATA_NEW;
	if(	ppHopo != 0 )
		strcpy(prlOacData->Hopo,ppHopo);
	
	// Zeiger auf Datensatz zur�ck
	return prlOacData;
	CCS_CATCH_ALL;
	return NULL;
}


//************************************************************************************************************************************************
// CompareOacToOac: zwei OACs miteinander vergleichen. Wenn <bpCompareKey> gesetzt ist,
//	werden auch die Felder, die den Schl�ssel ergeben verglichen. 
// R�ckgabe:	false	->	die Werte der Felder beider OACs sind gleich
//				true	->	die Werte der Felder beider OACs sind unterschiedlich
//************************************************************************************************************************************************

bool CedaOacData::CompareOacToOac(OACDATA *popOac1, OACDATA *popOac2,bool bpCompareKey /*= false*/)
{
CCS_TRY;
	// Felder vergleichen
	if (strcmp(popOac1->Hopo,popOac2->Hopo) != 0)
	{
		return true;
	}
CCS_CATCH_ALL;
return false;
}

//************************************************************************************************************************************************
// CopyOac: kopiert alles, ausser Urno
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaOacData::CopyOac(OACDATA* popOacDataSource,OACDATA* popOacDataTarget)
{
	// Daten kopieren
	strcpy(popOacDataTarget->Hopo,popOacDataSource->Hopo);
}

