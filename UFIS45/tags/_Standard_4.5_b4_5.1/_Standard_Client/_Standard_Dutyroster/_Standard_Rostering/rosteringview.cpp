// RosteringView.cpp : implementation of the CRosteringView class
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//****************************************************************************
// CRosteringView
//****************************************************************************

IMPLEMENT_DYNCREATE(CRosteringView, CView)

BEGIN_MESSAGE_MAP(CRosteringView, CView)
	//{{AFX_MSG_MAP(CRosteringView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()


//****************************************************************************
// CRosteringView construction
//****************************************************************************

CRosteringView::CRosteringView()
{
}

//****************************************************************************
// CRosteringView destruction
//****************************************************************************

CRosteringView::~CRosteringView()
{
}

//****************************************************************************
// 
//****************************************************************************

BOOL CRosteringView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CView::PreCreateWindow(cs);
}

//****************************************************************************
// CRosteringView drawing
//****************************************************************************

void CRosteringView::OnDraw(CDC* pDC)
{
	CRosteringDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

//****************************************************************************
// CRosteringView printing
//****************************************************************************

BOOL CRosteringView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

//****************************************************************************
// CRosteringView printing
//****************************************************************************

void CRosteringView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

//****************************************************************************
// CRosteringView printing
//****************************************************************************

void CRosteringView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

//****************************************************************************
// CRosteringView diagnostics
//****************************************************************************


#ifdef _DEBUG
void CRosteringView::AssertValid() const
{
	CView::AssertValid();
}

void CRosteringView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

//****************************************************************************
// 
//****************************************************************************

CRosteringDoc* CRosteringView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRosteringDoc)));
	return (CRosteringDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRosteringView message handlers
