// CCSDynTable.cpp: Implementierungsdatei
//
#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCSDynTable
//---------------------------------------------------------------------------

CCSDynTable::CCSDynTable(CWnd* pParent /*=NULL*/, UINT ipID/*=0*/)
{
	// Test
	m_id = 0;

	pomParent = pParent;
	if(pomParent == NULL)
		pomParent = GetParent();
	imTType = TTYPE_FIX;
	imID = ipID;

	omOldToolTipRect = CRect(0,0,0,0);
	imOldParentWndWidth  = 0;
	imOldParentWndHeight = 0;

    omWhitePen.CreatePen(PS_SOLID, 1, COLORREF(DT_WHITE));
    omBlackPen.CreatePen(PS_SOLID, 1, COLORREF(DT_BLACK));
    omBlackPenFat.CreatePen(PS_SOLID, 2, COLORREF(DT_BLACK));
	omSilverPen.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVER));

	omDefTablePen.CreatePen(PS_SOLID, 1, COLORREF(DT_WHITE));
	omDefSolidPen.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVER));
	omDefHeaderPen.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVER));

	omDefTablePen0.CreatePen(PS_SOLID, 1, COLORREF(DT_BLACK				)); omDefPens.Add((void*)&omDefTablePen0);
	omDefTablePen1.CreatePen(PS_SOLID, 1, COLORREF(DT_WHITE				)); omDefPens.Add((void*)&omDefTablePen1);
	omDefTablePen2.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVER			)); omDefPens.Add((void*)&omDefTablePen2);
	omDefTablePen3.CreatePen(PS_SOLID, 1, COLORREF(DT_LIGHTSILVER1		)); omDefPens.Add((void*)&omDefTablePen3);
	omDefTablePen4.CreatePen(PS_SOLID, 1, COLORREF(DT_LIGHTSILVER2		)); omDefPens.Add((void*)&omDefTablePen4);
	omDefTablePen5.CreatePen(PS_SOLID, 1, COLORREF(DT_SELECTBLUE		)); omDefPens.Add((void*)&omDefTablePen5);
	omDefTablePen6.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERRED			)); omDefPens.Add((void*)&omDefTablePen6);
	omDefTablePen7.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERGREEN		)); omDefPens.Add((void*)&omDefTablePen7);
	omDefTablePen8.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERREDLIGHT	)); omDefPens.Add((void*)&omDefTablePen8);
	omDefTablePen9.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERGREENLIGHT	)); omDefPens.Add((void*)&omDefTablePen9);
	omDefTablePen10.CreatePen(PS_SOLID, 1, COLORREF(RED					)); omDefPens.Add((void*)&omDefTablePen10);
	omDefTablePen11.CreatePen(PS_SOLID, 1, COLORREF(BLUE				)); omDefPens.Add((void*)&omDefTablePen11);
	omDefTablePen12.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERREDLIGHT2	)); omDefPens.Add((void*)&omDefTablePen12);
	omDefTablePen13.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERBLUELIGHT	)); omDefPens.Add((void*)&omDefTablePen13);
	omDefTablePen14.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERORANGELIGHT)); omDefPens.Add((void*)&omDefTablePen14);
	omDefTablePen15.CreatePen(PS_SOLID, 1, COLORREF(DT_SILVERREDLIGHT3  )); omDefPens.Add((void*)&omDefTablePen15);
	omDefTablePen16.CreatePen(PS_SOLID, 1, COLORREF(GREEN				)); omDefPens.Add((void*)&omDefTablePen16);
	omDefTablePen17.CreatePen(PS_SOLID, 1, COLORREF(DT_LIGHTBLUE		)); omDefPens.Add((void*)&omDefTablePen17);

	omWhiteBrush.CreateSolidBrush(COLORREF(DT_WHITE));
    omBlackBrush.CreateSolidBrush(COLORREF(DT_BLACK));
    omSilverBrush.CreateSolidBrush(COLORREF(DT_SILVER));
	
	omDefTableBrush.CreateSolidBrush(COLORREF(DT_WHITE));
	omDefSolidBrush.CreateSolidBrush(COLORREF(DT_SILVER));
	omDefHeaderBrush.CreateSolidBrush(COLORREF(DT_SILVER));

	omDefTableBrush0.CreateSolidBrush(COLORREF(DT_BLACK				)); omDefBrushes.Add((void*)&omDefTableBrush0);
	omDefTableBrush1.CreateSolidBrush(COLORREF(DT_WHITE				)); omDefBrushes.Add((void*)&omDefTableBrush1);
	omDefTableBrush2.CreateSolidBrush(COLORREF(DT_SILVER			)); omDefBrushes.Add((void*)&omDefTableBrush2);
	omDefTableBrush3.CreateSolidBrush(COLORREF(DT_LIGHTSILVER1		)); omDefBrushes.Add((void*)&omDefTableBrush3);
	omDefTableBrush4.CreateSolidBrush(COLORREF(DT_LIGHTSILVER2		)); omDefBrushes.Add((void*)&omDefTableBrush4);
	omDefTableBrush5.CreateSolidBrush(COLORREF(DT_SELECTBLUE		)); omDefBrushes.Add((void*)&omDefTableBrush5);
	omDefTableBrush6.CreateSolidBrush(COLORREF(DT_SILVERRED			)); omDefBrushes.Add((void*)&omDefTableBrush6);
	omDefTableBrush7.CreateSolidBrush(COLORREF(DT_SILVERGREEN		)); omDefBrushes.Add((void*)&omDefTableBrush7);
	omDefTableBrush8.CreateSolidBrush(COLORREF(DT_SILVERREDLIGHT	)); omDefBrushes.Add((void*)&omDefTableBrush8);
	omDefTableBrush9.CreateSolidBrush(COLORREF(DT_SILVERGREENLIGHT	)); omDefBrushes.Add((void*)&omDefTableBrush9);
	omDefTableBrush10.CreateSolidBrush(COLORREF(RED					)); omDefBrushes.Add((void*)&omDefTableBrush10);
	omDefTableBrush11.CreateSolidBrush(COLORREF(BLUE				)); omDefBrushes.Add((void*)&omDefTableBrush11);
	omDefTableBrush12.CreateSolidBrush(COLORREF(DT_SILVERREDLIGHT2	)); omDefBrushes.Add((void*)&omDefTableBrush12);
	omDefTableBrush13.CreateSolidBrush(COLORREF(DT_SILVERBLUELIGHT	)); omDefBrushes.Add((void*)&omDefTableBrush13);
	omDefTableBrush14.CreateSolidBrush(COLORREF(DT_SILVERORANGELIGHT)); omDefBrushes.Add((void*)&omDefTableBrush14);
	omDefTableBrush15.CreateSolidBrush(COLORREF(DT_SILVERREDLIGHT3  )); omDefBrushes.Add((void*)&omDefTableBrush15);
	omDefTableBrush16.CreateSolidBrush(COLORREF(GREEN				)); omDefBrushes.Add((void*)&omDefTableBrush16);
	omDefTableBrush17.CreateSolidBrush(COLORREF(DT_LIGHTBLUE		)); omDefBrushes.Add((void*)&omDefTableBrush17);
	omDefTableBrush18.CreateSolidBrush(COLORREF(DT_YELLOW			)); omDefBrushes.Add((void*)&omDefTableBrush18);
	

	omDefTableColor      = COLORREF(DT_WHITE);
	omDefHeaderColor     = COLORREF(DT_SILVER);
	omDefSolidColor      = COLORREF(DT_SILVER);
	omDefTableTextColor  = COLORREF(DT_BLACK);
	omDefHeaderTextColor = COLORREF(DT_BLACK);
	omDefSolidTextColor  = COLORREF(DT_BLACK);

	rmTableData.oSolidHeader.oBkColorNr = -1;
	rmTableData.oSolidHeader.oTextColor = omDefHeaderTextColor;

	rmTableData.oSolidHeader.iFontStyle = FONT_STYLE_REGULAR;

	omDefTableFont.CreateFont( 15, 6, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS,PROOF_QUALITY, DEFAULT_PITCH, "Arial" );
	omItalicTableFont.CreateFont( 15, 6, 0, 0, FW_NORMAL, 1, 0, 0,DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS,PROOF_QUALITY, DEFAULT_PITCH, "Arial" );
	omDefHeaderFont.CreateFont( 15, 6, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS,PROOF_QUALITY, DEFAULT_PITCH, "Arial" );
	omDefSolidFont.CreateFont( 15, 6, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS,PROOF_QUALITY, DEFAULT_PITCH, "Arial" );

	pomDefTableFont  = &omDefTableFont;
	pomItalicTableFont  = &omItalicTableFont;
	pomDefHeaderFont = &omDefHeaderFont;
	pomDefSolidFont  = &omDefSolidFont;

	imDefTableFormat  = DT_SINGLELINE|DT_CENTER|DT_VCENTER;
	imDefHeaderFormat = DT_SINGLELINE|DT_CENTER|DT_VCENTER;
	imDefSolidFormat  = DT_SINGLELINE|DT_VCENTER;

	omEraseBkgndRect.SetRectEmpty();
	imEBkRegion = 0;

	imColumns     = 0;
	imRows		  = 0;
	imColumnWidth = 0;
	imRowHight    = 0;
	imHeaderHight = 0;
	imSolidWidth  = 0;

	omTableRect.SetRectEmpty();
	bmVScrollBar     = true;
	bmHScrollBar     = true;
	imScrollBarWidth = 0;
	imScrollBarHight = 0;
	imMaxDataHorz    = 0;
	imMinDataHorz    = 0;
	imMaxDataVert    = 0;
	imMinDataVert    = 0;
	imColumnStartPos   = 0;
	imRowStartPos      = 0;

	imHorzDiffToParentWnd = 0;	
	imVertDiffToParentWnd = 0;

	imEditColumn       = -1;
	imEditRow          = -1;
	bmInplaceEditExist = false;
	pomIPEdit          = NULL;
	prmDefIEditAttrib  = &rmDefIEditAttrib;

	rmNoIEditAttrib.Style |= ES_READONLY;


	bmIPEditCRLF = false;
	bmDestroyIPEditAction = false;


	pomVScroll = NULL;
	pomHScroll = NULL;
	prmModifyTab = new MODIFYTAB;
	prmModifyTab->iID = imID;

	imDragWord = 0; //0 = no Drag
	imDropWord = 0; //0 = no Drop
	bmIsDropFieldSelected = false;
}

CCSDynTable::~CCSDynTable()
{
	ResetContent();
	delete prmModifyTab;
	if(pomVScroll != NULL) delete pomVScroll;
	if(pomHScroll != NULL) delete pomHScroll;
	omDefBrushes.RemoveAll();
	omDefPens.RemoveAll();
	if(pomIPEdit != NULL) delete pomIPEdit;
}


BEGIN_MESSAGE_MAP(CCSDynTable, CWnd)
	//{{AFX_MSG_MAP(CCSDynTable)
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
    ON_MESSAGE(WM_EDIT_MOVE_IP_LEFT,	OnMoveInplaceLeft)  
	ON_MESSAGE(WM_EDIT_MOVE_IP_RIGHT,	OnMoveInplaceRight)  
    ON_MESSAGE(WM_EDIT_MOVE_IP_UP ,		OnMoveInplaceUp)  
    ON_MESSAGE(WM_EDIT_MOVE_IP_DOWN,	OnMoveInplaceDown)  
    ON_MESSAGE(WM_EDIT_IP_END,			OnInplaceReturn)  
	ON_WM_ERASEBKGND()
	ON_MESSAGE(WM_DRAGOVER,				OnDragOver)
	ON_MESSAGE(WM_DROP,					OnDrop)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_SIZE()
	ON_WM_SIZING()
	ON_WM_CLOSE()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


bool CCSDynTable::DeleteTable()
{
	delete this;
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CCSDynTable 
//---------------------------------------------------------------------------

BOOL CCSDynTable::Create()
{
	DWORD dwStyle = WS_CHILD | WS_VISIBLE;

	return Create(NULL, NULL, dwStyle, omTableRect, pomParent,imID,NULL);
}

//---------------------------------------------------------------------------

BOOL CCSDynTable::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

//---------------------------------------------------------------------------

int CCSDynTable::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect olWndRect;
	// ADO
	pomParent->GetClientRect(&olWndRect);
	imOldParentWndWidth  = olWndRect.right - olWndRect.left;
	imOldParentWndHeight = olWndRect.bottom - olWndRect.top;

	SCROLLINFO rlScrollInfo;
	if(bmVScrollBar)
	{
		CRect olRect;
		GetClientRect(&olRect);
		pomVScroll = new CScrollBar();
		olRect.left = olRect.right - imScrollBarWidth;
		if(bmHScrollBar)
		{
			olRect.bottom = olRect.bottom - imScrollBarHight;
		}
		pomVScroll->Create(SBS_VERT, olRect, this, imID+9998);//|WS_TABSTOP
		pomVScroll->SetScrollRange(0,0);
		pomVScroll->SetScrollPos(0);
		pomVScroll->GetScrollInfo(&rlScrollInfo,SIF_ALL);
		rlScrollInfo.nPage = imColumns;
		pomVScroll->SetScrollInfo(&rlScrollInfo);
		pomVScroll->ShowWindow(SW_SHOWNORMAL);
		pomVScroll->EnableScrollBar(ESB_DISABLE_BOTH);

	}
	if(bmHScrollBar)
	{
		pomHScroll = new CScrollBar();
		CRect olRect;
		GetClientRect(&olRect);
		olRect.top = olRect.bottom - imScrollBarHight;
		if(bmVScrollBar)
		{
			olRect.right = olRect.right - imScrollBarWidth;
		}
		pomHScroll->Create(SBS_HORZ, olRect, this, imID+9999);//|WS_TABSTOP
		pomHScroll->SetScrollRange( 0,0);
		pomHScroll->SetScrollPos(0);
		pomHScroll->GetScrollInfo(&rlScrollInfo,SIF_ALL);
		rlScrollInfo.nPage = imRows;
		pomHScroll->SetScrollInfo(&rlScrollInfo);
		pomHScroll->ShowWindow(SW_SHOWNORMAL);
		pomHScroll->EnableScrollBar(ESB_DISABLE_BOTH);
	}

	omToolTip.Create(this);
	omToolTip.AddTool(this, "");
	omToolTip.SetDelayTime(TTDT_INITIAL,500);  // Ansprechzeit
	omToolTip.SetDelayTime(TTDT_AUTOPOP,4000); // Anzeigedauer
	omToolTip.SetDelayTime(TTDT_RESHOW,500);
	omToolTip.Activate(FALSE);

	return 0;
}

//---------------------------------------------------------------------------

void CCSDynTable::SetTableData(CRect opRect,bool bpInplaceEdit /*= false*/,bool bpHSB /*= true*/, bool bpVSB /*= true*/)
{
	imTType = TTYPE_VAR;
	omTableRect = opRect;
	bmInplaceEdit    = bpInplaceEdit;
	bmHScrollBar = bpHSB;
	if(bmHScrollBar) imScrollBarHight = 16;
	bmVScrollBar = bpVSB;
	if(bmVScrollBar) imScrollBarWidth = 16;
}

//---------------------------------------------------------------------------

void CCSDynTable::SetTableData(CRect &opRect, UINT ipColumns, UINT ipColumnWidth, UINT ipHeaderHight, UINT ipRows, UINT ipRowHight, UINT ipSolidWidth,
							   bool bpInplaceEdit /*=fase*/,bool bpIPEditCRLF /*=false*/,bool bpHSB /*=true*/, bool bpVSB /*=true*/,
							   bool bpHSizing /*=true*/, bool bpVSizing/*=true*/)
{
	imTType = TTYPE_FIX;
	imColumns     = ipColumns;
	imRows		  = ipRows;
	imColumnWidth = ipColumnWidth;
	imRowHight    = ipRowHight;
	imHeaderHight = ipHeaderHight;
	imSolidWidth  = ipSolidWidth;
	bmInplaceEdit    = bpInplaceEdit;
	bmIPEditCRLF	 = bpIPEditCRLF;

	bmVSizing = bpVSizing;
	bmHSizing = bpHSizing;

	bmHScrollBar = bpHSB;
	if(bmHScrollBar) imScrollBarHight = 16;
	bmVScrollBar = bpVSB;
	if(bmVScrollBar) imScrollBarWidth = 16;

	omTableRect.left   = opRect.left;
	omTableRect.top    = opRect.top;
	omTableRect.right  = opRect.left+imColumnWidth*imColumns+imSolidWidth+imScrollBarWidth+1;
	omTableRect.bottom = opRect.top+imRowHight*imRows+imHeaderHight+imScrollBarHight+1;
	opRect = omTableRect;
}

//---------------------------------------------------------------------------

void CCSDynTable::SetDefaultColors(COLORREF opTableColor,COLORREF opTableTextColor, COLORREF opHeaderColor,COLORREF opHeaderTextColor, COLORREF opSolidColor, COLORREF opSolidTextColor)
{
	omDefTableColor      = opTableColor;
	omDefTableTextColor  = opTableTextColor;

	omDefHeaderColor     = opHeaderColor;
	omDefHeaderTextColor = opHeaderTextColor;

	omDefSolidColor      = opSolidColor;
	omDefSolidTextColor  = opSolidTextColor;

	rmTableData.oSolidHeader.oTextColor = omDefHeaderTextColor;

	omDefTablePen.DeleteObject();
	omDefSolidPen.DeleteObject();
	omDefHeaderPen.DeleteObject();

	omDefTableBrush.DeleteObject();
	omDefSolidBrush.DeleteObject();
	omDefHeaderBrush.DeleteObject();


	omDefTablePen.CreatePen(PS_SOLID, 1, omDefTableColor);
	omDefHeaderPen.CreatePen(PS_SOLID, 1, omDefHeaderColor);
	omDefSolidPen.CreatePen(PS_SOLID, 1, omDefSolidColor);

	omDefTableBrush.CreateSolidBrush(omDefTableColor);
	omDefHeaderBrush.CreateSolidBrush(omDefHeaderColor);
	omDefSolidBrush.CreateSolidBrush(omDefSolidColor);
}

//---------------------------------------------------------------------------

void CCSDynTable::SetDefaultColors(UINT ipIdx ,COLORREF opTableColor)
{
	if(ipIdx < (UINT)omDefBrushes.GetSize() && ipIdx >= 0)
	{
		CBrush *opBrush = (CBrush*)omDefBrushes[ipIdx];
		opBrush->DeleteObject();
		opBrush->CreateSolidBrush(opTableColor);
	}
	if(ipIdx < (UINT)omDefPens.GetSize() && ipIdx >= 0)
	{
		CPen *opPen = (CPen*)omDefPens[ipIdx];
		opPen->DeleteObject();
		opPen->CreatePen(PS_SOLID, 1, opTableColor);
	}
}


//---------------------------------------------------------------------------

void CCSDynTable::SetDefaultTextForm(CFont *popTableFont, CFont *popItalicTableFont, CFont *popHeaderFont, CFont *popSolidFont, UINT ipTableFormat, UINT ipHeaderFormat, UINT ipSolidFormat, CCSEDIT_ATTRIB * prpIEditAttrib)
{
	prmDefIEditAttrib  = prpIEditAttrib;
	pomDefTableFont    = popTableFont;
	pomItalicTableFont = popItalicTableFont;
	pomDefHeaderFont   = popHeaderFont;
	pomDefSolidFont    = popSolidFont;

	imDefTableFormat  = ipTableFormat;
	imDefHeaderFormat = ipHeaderFormat;
	imDefSolidFormat  = ipSolidFormat;

	rmNoIEditAttrib.Style = prmDefIEditAttrib->Style;
	rmNoIEditAttrib.Style |= ES_READONLY;

}

//---------------------------------------------------------------------------

void CCSDynTable::SetDifferenceToParentWndRect(int ipHorizontal /*= 0*/,int ipVertical /*= 0*/)
{
	//if(ipHorizontal>0)
	imHorzDiffToParentWnd = ipHorizontal;	
	//if(ipVertical>0)
	imVertDiffToParentWnd = ipVertical;
}

//---------------------------------------------------------------------------

void CCSDynTable::OnPaint() 
{
	CPaintDC olDC(this); // device context for painting

	if(rmIPEditPaintInfo.bExist == true)
	{
		rmIPEditPaintInfo.bExist = false;
		if(rmIPEditPaintInfo.iOldRow > -1)
		{
			PaintFieldText(&olDC, imColumnStartPos+rmIPEditPaintInfo.iOldColumn, imRowStartPos+rmIPEditPaintInfo.iOldRow);
			rmIPEditPaintInfo.iOldColumn = -1;
			rmIPEditPaintInfo.iOldRow    = -1;
		}
	}
	else
	{
		CRect olTableRect;
		GetClientRect(&olTableRect);
		if(omEraseBkgndRect.IsRectNull() || olTableRect == omEraseBkgndRect)
		{
			//TRACE("GOT ALL\n");
			//if (omEraseBkgndRect.IsRectNull()) TRACE("IsRectNull\n");
			//if (olTableRect == omEraseBkgndRect) TRACE("==\n");
			PaintTable(&olDC);
			PaintText(&olDC);
		}
		else
		{
			int ilFromRow    = -1;
			int ilFromColumn = -1;
			int ilToRow		 = -1;
			int ilToColumn   = -1;

			int ilXStart = olTableRect.left   + imSolidWidth;
			int ilYStart = olTableRect.top    + imHeaderHight;
			int ilXEnd   = olTableRect.right  - imScrollBarWidth;
			int ilYEnd   = olTableRect.bottom - imScrollBarHight;

			if(omEraseBkgndRect.left <= ilXStart)
				ilFromColumn = 0;
			if(omEraseBkgndRect.top <= ilYStart)
				ilFromRow = 0;
			if(omEraseBkgndRect.right >= ilXEnd)
				ilToColumn = imColumns;
			if(omEraseBkgndRect.bottom >= ilYEnd)
				ilToRow = imRows;

			if(ilFromColumn == -1)
				ilFromColumn = (int)((omEraseBkgndRect.left-ilXStart)/imColumnWidth);
			if(ilToColumn == -1)
				ilToColumn = (int)((omEraseBkgndRect.right-ilXStart)/imColumnWidth);
			if(ilFromRow == -1)
				ilFromRow = (int)((omEraseBkgndRect.top-ilYStart)/imRowHight);
			if(ilToRow == -1)
				ilToRow = (int)((omEraseBkgndRect.bottom-ilYStart)/imRowHight);

			if(ilFromColumn == -1)
				ilFromColumn = 0;
			if(ilToColumn == -1)
				ilToColumn = 0;
			if(ilFromRow == -1)
				ilFromRow = 0;
			if(ilToRow == -1)
				ilToRow = 0;

			if(ilFromRow!=-1 && ilToRow!=-1 && ilFromColumn!=-1 && ilToColumn!=-1)
			{
				PaintTable(&olDC, ilFromColumn, ilFromRow, ilToColumn, ilToRow);
				PaintText(&olDC, ilFromColumn, ilFromRow, ilToColumn, ilToRow);
			}
		}
	}
	omEraseBkgndRect.SetRectEmpty();
}

//---------------------------------------------------------------------------

BOOL CCSDynTable::OnEraseBkgnd(CDC *pDC)
{
// 2 SIMPLEREGION
// 3 COMPLEXREGION
// ERROR
// NULLREGION
	imEBkRegion =  pDC->GetClipBox(&omEraseBkgndRect);
	if(imEBkRegion != SIMPLEREGION && imEBkRegion != COMPLEXREGION)
		omEraseBkgndRect.SetRectEmpty();
	
	return CWnd::OnEraseBkgnd(pDC);
}

//---------------------------------------------------------------------------

void CCSDynTable::PaintTable(CDC *popDC, int ipFromColumn /*= -1*/, int ipFromRow /*= -1*/,int ipToColumn /*= -1*/, int ipToRow /*= -1*/)
{
	//CDC *polDC = GetDC();

	// Test
	//TRACE("%i Paint Table: fr%d  fc%d  tr%d  tc%d\n",m_id,ipFromRow,ipFromColumn,ipToRow,ipToColumn);
	m_id++;

	CDC *polDC = popDC;
	if(ipFromColumn == -1) ipFromColumn = 0;
	if(ipFromRow == -1)	   ipFromRow    = 0;
	if(ipToColumn == -1)   ipToColumn   = imColumns;
	if(ipToRow == -1)      ipToRow      = imRows;

	CRect olTableRect;
	olTableRect.left = 0;
	olTableRect.top = 0;
	olTableRect.right = omTableRect.right-omTableRect.left - imScrollBarWidth;
	olTableRect.bottom = omTableRect.bottom-omTableRect.top - imScrollBarHight;

	int ilXStart = olTableRect.left;
	int ilYStart = olTableRect.top;
	int ilXEnd   = olTableRect.right;
	int ilYEnd   = olTableRect.bottom;
	int ilSolid  = 0;
	int ilHeader = 0;
	//********* paint table **********
	// background 
	//polDC->FillRect(olTableRect,&omDefTableBrush);	
	//+----------------- header and solid backgruond-----------------
	if(imHeaderHight > 0)
	{
		ilHeader = 1;
		//polDC->FillRect(CRect(ilXStart,ilYStart,ilXEnd,ilYStart+imHeaderHight),&omDefHeaderBrush);	
	}

	if(imSolidWidth > 0)
	{
		ilSolid = 1;
		//polDC->FillRect(CRect(ilXStart,ilYStart,ilXStart+imSolidWidth,ilYEnd),&omDefHeaderBrush);	
	}

	int ilTmpHeaderHight = 0;
	int ilTmpSolidWidth = 0;
	//--- row and column lines ---
	polDC->SelectObject(&omBlackPen);
	polDC->SelectObject(&omDefTableBrush);

	for(int ilRow=0;ilRow<=(imRows+ilHeader);ilRow++)
	{
		if(!(ilRow < ipFromRow || ilRow > ipToRow+ilHeader))
		{
			polDC->MoveTo(ilXStart, ilYStart+ilRow*imRowHight+ilTmpHeaderHight);
			polDC->LineTo(ilXEnd,   ilYStart+ilRow*imRowHight+ilTmpHeaderHight);
		}
		if(ilRow == 0 && ilHeader == 1)
		{
			ilTmpHeaderHight = imHeaderHight-imRowHight;
		}
	}
	for(int ilCol=0;ilCol<=(imColumns+ilSolid);ilCol++)
	{
		if(!(ilCol < ipFromColumn || ilCol > ipToColumn+ilSolid))
		{
			polDC->MoveTo(ilXStart+ilCol*imColumnWidth+ilTmpSolidWidth, ilYStart);
			polDC->LineTo(ilXStart+ilCol*imColumnWidth+ilTmpSolidWidth, ilYEnd);
		}
		if(ilCol == 0 && ilSolid == 1)
		{
			ilTmpSolidWidth = imSolidWidth-imColumnWidth;
		}
	}
}

//---------------------------------------------------------------------------

void CCSDynTable::PaintText(CDC *popDC, int ipFromColumn /*= -1*/, int ipFromRow /*= -1*/,int ipToColumn /*= -1*/, int ipToRow /*= -1*/)
{
	//CDC *polDC = GetDC();
	CDC *polDC = popDC;

	if(ipFromColumn == -1) ipFromColumn = 0;
	if(ipFromRow == -1)	   ipFromRow    = 0;
	if(ipToColumn == -1)   ipToColumn   = imColumns;
	if(ipToRow == -1)      ipToRow      = imRows;

	CRect olTableRect;
	olTableRect.left = 0;
	olTableRect.top = 0;
	olTableRect.right = omTableRect.right-omTableRect.left - imScrollBarWidth;
	olTableRect.bottom = omTableRect.bottom-omTableRect.top - imScrollBarHight;

	int ilXStart = olTableRect.left;
	int ilYStart = olTableRect.top;
	//int ilXEnd   = olTableRect.right;
	//int ilYEnd   = olTableRect.bottom;
	//int ilSolid  = 0;
	//int ilHeader = 0;

	CRect olTextRect;
	CString olText;

	//----- fill header  -----
	polDC->SetBkMode(TRANSPARENT);// TRANSPARENT  OPAQUE
	polDC->SelectObject(pomDefHeaderFont);	
	if(imHeaderHight>0)
	{
		int ilHeaderCols = rmTableData.oHeader.GetSize();
		for(int ilCol=0;ilCol<imColumns;ilCol++)
		{
			if(!(ilCol < ipFromColumn || ilCol > ipToColumn))
			{
				olText.Empty();
				olTextRect.left   = ilXStart+imSolidWidth+ilCol*imColumnWidth+1;
				olTextRect.top    = ilYStart+1;
				olTextRect.right  = olTextRect.left+imColumnWidth-1;
				olTextRect.bottom = olTextRect.top+imHeaderHight-1;

				//***** Header BkColor
				if(ilHeaderCols <= ilCol)
				{
					polDC->FillRect(olTextRect,&omDefHeaderBrush);	
				}
				else if(((FIELDDATA*)rmTableData.oHeader[ilCol])->oBkColorNr < 0)
				{
					polDC->FillRect(olTextRect,&omDefHeaderBrush);	
				}
				else
				{
					int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->oBkColorNr;
					polDC->FillRect(olTextRect,(CBrush*)omDefBrushes[ilNr]);
				}
				//***** Header Lines
				if(ilHeaderCols > ilCol)
				{
					PaintExtraLines(polDC, olTextRect, (FIELDDATA*)rmTableData.oHeader[ilCol], ilCol, ilHeaderCols, 1, 1);
				}
				//***** Header Marker
				if(ilHeaderCols > ilCol)
				{
					if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBeamColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBeamColorNr;
						PaintMarker(polDC, MARKER_L_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBeamColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBeamColorNr;
						PaintMarker(polDC, MARKER_R_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iLTTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iLTTriangleColorNr;
						PaintMarker(polDC, MARKER_LT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iRTTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iRTTriangleColorNr;
						PaintMarker(polDC, MARKER_RT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBTriangleColorNr;
						PaintMarker(polDC, MARKER_RB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBTriangleColorNr;
						PaintMarker(polDC, MARKER_LB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
				}
				//***** Header Text Color
				if(ilHeaderCols <= ilCol)
				{
					polDC->SetTextColor(omDefHeaderTextColor);
				}
				else if(((FIELDDATA*)rmTableData.oHeader[ilCol])->oTextColor == -1)
				{
					polDC->SetTextColor(omDefHeaderTextColor);
				}
				else
				{
					polDC->SetTextColor(((FIELDDATA*)rmTableData.oHeader[ilCol])->oTextColor);
				}
				//***** Header Text
				if(ilHeaderCols > ilCol)
				{
					olText = ((FIELDDATA*)rmTableData.oHeader[ilCol])->oText;
				}
				polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefHeaderFormat);
			}
		}
	}
	//----- fill solid  -----
	polDC->SetBkMode(TRANSPARENT);// TRANSPARENT  OPAQUE
	polDC->SelectObject(pomDefSolidFont);	
	if(imSolidWidth>0)
	{
		int ilSolidRows = rmTableData.oSolid.GetSize();
		for(int ilRow=0;ilRow<imRows;ilRow++)
		{
			if(!(ilRow < ipFromRow || ilRow > ipToRow))
			{
				olText.Empty();
				olTextRect.left   = ilXStart+1;
				olTextRect.top    = ilYStart+imHeaderHight+imRowHight*ilRow+1;
				olTextRect.right  = olTextRect.left+imSolidWidth-1;
				olTextRect.bottom = olTextRect.top+imRowHight-1;

				//***** Solid BkColor
				if(ilSolidRows <= ilRow)
				{
					polDC->FillRect(olTextRect,&omDefSolidBrush);	
				}
				else if(((FIELDDATA*)rmTableData.oSolid[ilRow])->oBkColorNr < 0)
				{
					polDC->FillRect(olTextRect,&omDefSolidBrush);	
				}
				else
				{
					int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->oBkColorNr;
					polDC->FillRect(olTextRect,(CBrush*)omDefBrushes[ilNr]);
				}
				//***** Solid Lines
				if(ilSolidRows > ilRow)
				{
					PaintExtraLines(polDC, olTextRect, (FIELDDATA*)rmTableData.oSolid[ilRow], 1, 1, ilRow, ilSolidRows);
				}
				//***** Solid Marker
				if(ilSolidRows > ilRow)
				{
					if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBeamColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBeamColorNr;
						PaintMarker(polDC, MARKER_L_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBeamColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBeamColorNr;
						PaintMarker(polDC, MARKER_R_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iLTTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iLTTriangleColorNr;
						PaintMarker(polDC, MARKER_LT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iRTTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iRTTriangleColorNr;
						PaintMarker(polDC, MARKER_RT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBTriangleColorNr;
						PaintMarker(polDC, MARKER_RB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBTriangleColorNr;
						PaintMarker(polDC, MARKER_LB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
				}
				//***** Solid Text Color
				if(ilSolidRows <= ilRow)
				{
					polDC->SetTextColor(omDefSolidTextColor);
				}
				else if(((FIELDDATA*)rmTableData.oSolid[ilRow])->oTextColor == -1)
				{
					polDC->SetTextColor(omDefSolidTextColor);
				}
				else
				{
					polDC->SetTextColor(((FIELDDATA*)rmTableData.oSolid[ilRow])->oTextColor);
				}
				//***** Solid Text
				if(ilSolidRows > ilRow)
				{
					olText = ((FIELDDATA*)rmTableData.oSolid[ilRow])->oText;
				}
				polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefSolidFormat);
			}
		}
	}
	//----- fill solidheader  -----
	if(imSolidWidth>0 && imHeaderHight>0)
	{
		olText.Empty();
		polDC->SetBkMode(TRANSPARENT);// TRANSPARENT  OPAQUE
		polDC->SelectObject(pomDefHeaderFont);	

		olTextRect.left   = ilXStart+1;
		olTextRect.top    = ilYStart+1;
		olTextRect.right  = olTextRect.left+imSolidWidth-1;
		olTextRect.bottom = olTextRect.top+imHeaderHight-1;

		//***** SolidHeader BkColor
		if(rmTableData.oSolidHeader.oBkColorNr < 0)
		{
			polDC->FillRect(olTextRect,&omDefHeaderBrush);	
		}
		else
		{
			int ilNr = rmTableData.oSolidHeader.oBkColorNr;
			polDC->FillRect(olTextRect,(CBrush*)omDefBrushes[ilNr]);
		}
		//***** SolidHeader Lines
		PaintExtraLines(polDC, olTextRect, &(rmTableData.oSolidHeader), 1, 1, 1, 1);
		//***** SolidHeader Text Color
		if(rmTableData.oSolidHeader.oTextColor == -1)
		{
			polDC->SetTextColor(omDefHeaderTextColor);
		}
		else
		{
			polDC->SetTextColor(rmTableData.oSolidHeader.oTextColor);
		}
		//***** SolidHeader Text
		olText = rmTableData.oSolidHeader.oText;
		polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefSolidFormat);

	}
	//----- fill table  -----
	polDC->SetBkMode(TRANSPARENT);// TRANSPARENT  OPAQUE
	polDC->SelectObject(pomDefTableFont);	

	int ilTableRows = rmTableData.oTable.GetSize();
	for(int ilRow=0;ilRow<imRows;ilRow++)
	{
		if(!(ilRow < ipFromRow || ilRow > ipToRow))
		{
			int ilTableCols = 0;
			CPtrArray *polTableArray = NULL;
			if(ilTableRows > ilRow)
			{
				polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
				ilTableCols = polTableArray->GetSize();
			}
			for(int ilCol=0;ilCol<imColumns;ilCol++)
			{
				if(!(ilCol < ipFromColumn || ilCol > ipToColumn))
				{
					olText.Empty();
					olTextRect.left   = ilXStart+imSolidWidth+ilCol*imColumnWidth+1;
					olTextRect.top    = ilYStart+imHeaderHight+ilRow*imRowHight+1;
					olTextRect.right  = olTextRect.left+imColumnWidth-1;
					olTextRect.bottom = olTextRect.top+imRowHight-1;

					if(ilTableRows > ilRow)
					{
						//***** Table BkColor
						if(ilTableCols <= ilCol)
						{
							polDC->FillRect(olTextRect,&omDefTableBrush);	
						}
						else if(((FIELDDATA*)(*polTableArray)[ilCol])->oBkColorNr < 0)
						{
							polDC->FillRect(olTextRect,&omDefTableBrush);	
						}
						else
						{
							int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->oBkColorNr;
							polDC->FillRect(olTextRect,(CBrush*)omDefBrushes[ilNr]);
						}
						//***** Table Lines
						if(ilTableCols > ilCol)
						{
							PaintExtraLines(polDC, olTextRect, (FIELDDATA*)(*polTableArray)[ilCol], ilCol, ilTableCols, ilRow, ilTableRows);
						}
						//***** Table Marker
						if(ilTableCols > ilCol)
						{
							if(((FIELDDATA*)(*polTableArray)[ilCol])->iLBeamColorNr >= 0)
							{
								int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iLBeamColorNr;
								PaintMarker(polDC, MARKER_L_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
							}
							if(((FIELDDATA*)(*polTableArray)[ilCol])->iRBeamColorNr >= 0)
							{
								int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iRBeamColorNr;
								PaintMarker(polDC, MARKER_R_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
							}
							if(((FIELDDATA*)(*polTableArray)[ilCol])->iLTTriangleColorNr >= 0)
							{
								int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iLTTriangleColorNr;
								PaintMarker(polDC, MARKER_LT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
							}
							if(((FIELDDATA*)(*polTableArray)[ilCol])->iRTTriangleColorNr >= 0)
							{
								int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iRTTriangleColorNr;
								PaintMarker(polDC, MARKER_RT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
							}
							if(((FIELDDATA*)(*polTableArray)[ilCol])->iRBTriangleColorNr >= 0)
							{
								int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iRBTriangleColorNr;
								PaintMarker(polDC, MARKER_RB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
							}
							if(((FIELDDATA*)(*polTableArray)[ilCol])->iLBTriangleColorNr >= 0)
							{
								int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iLBTriangleColorNr;
								PaintMarker(polDC, MARKER_LB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
							}
						}
						//***** Table Text Color
						if(ilTableCols <= ilCol)
						{
							polDC->SetTextColor(omDefTableTextColor);
						}
						else if(((FIELDDATA*)(*polTableArray)[ilCol])->oTextColor == -1)
						{
							polDC->SetTextColor(omDefTableTextColor);
						}
						else
						{
							polDC->SetTextColor(((FIELDDATA*)(*polTableArray)[ilCol])->oTextColor);
						}
						//***** Table Text
						if(ilTableCols > ilCol)
						{
							olText = ((FIELDDATA*)(*polTableArray)[ilCol])->oText;
						}

						switch(((FIELDDATA*)(*polTableArray)[ilCol])->iFontStyle)
						{
						//case FONT_STYLE_REGULAR:
						//case FONT_STYLE_BOLD:
						default:
							polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefTableFormat);
							break;
						case FONT_STYLE_ITALIC:
							CFont* polOldFont = polDC->SelectObject(pomItalicTableFont);	
							polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefTableFormat);
							polDC->SelectObject(polOldFont);
							break;
						}

					}
					else
					{
						polDC->FillRect(olTextRect,&omDefTableBrush);	
						polDC->SetTextColor(omDefTableTextColor);
					}
				}
			}
		}
	}
	if(pomIPEdit != NULL)
	{
		pomIPEdit->InvalidateRect(NULL);
	}
}

//---------------------------------------------------------------------------

void CCSDynTable::PaintFieldText(CDC *popDC, int ipColumn, int ipRow) //Absolute Colum and Row (0-Max)
{
	//CDC *polDC = GetDC();
	CDC *polDC = popDC;

	CRect olTableRect;
	olTableRect.left = 0;
	olTableRect.top = 0;
	olTableRect.right = omTableRect.right-omTableRect.left - imScrollBarWidth;
	olTableRect.bottom = omTableRect.bottom-omTableRect.top - imScrollBarHight;

	int ilXStart = olTableRect.left;
	int ilYStart = olTableRect.top;
	//int ilXEnd   = olTableRect.right;
	//int ilYEnd   = olTableRect.bottom;
	//int ilSolid  = 0;
	//int ilHeader = 0;

	CRect olTextRect;
	CString olText;
	olText.Empty();

	int ilRow = ipRow - imRowStartPos;
	int ilCol = ipColumn - imColumnStartPos;

	if(ipRow == -1 && ipColumn >= 0 && imHeaderHight > 0)
	{
		//----- fill header  -----
		if(ilCol >= 0 && ilCol < imColumns)
		{
			polDC->SetBkMode(TRANSPARENT);// TRANSPARENT  OPAQUE
			polDC->SelectObject(pomDefHeaderFont);	

			olTextRect.left   = ilXStart+imSolidWidth+ilCol*imColumnWidth+1;
			olTextRect.top    = ilYStart+1;
			olTextRect.right  = olTextRect.left+imColumnWidth-1;
			olTextRect.bottom = olTextRect.top+imHeaderHight-1;

			int ilHeaderCols = rmTableData.oHeader.GetSize();
			//***** Header BkColor
			if(ilHeaderCols <= ilCol)
			{
				polDC->FillRect(olTextRect,&omDefHeaderBrush);	
			}
			else if(((FIELDDATA*)rmTableData.oHeader[ilCol])->oBkColorNr < 0)
			{
				polDC->FillRect(olTextRect,&omDefHeaderBrush);	
			}
			else
			{
				int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->oBkColorNr;
				polDC->FillRect(olTextRect,(CBrush*)omDefBrushes[ilNr]);
			}
			//***** Header Lines
			if(ilHeaderCols > ilCol)
			{
				PaintExtraLines(polDC, olTextRect, (FIELDDATA*)rmTableData.oHeader[ilCol], ilCol, ilHeaderCols, 1, 1);
			}
			//***** Header Marker
			if(ilHeaderCols > ilCol)
			{
				if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBeamColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBeamColorNr;
					PaintMarker(polDC, MARKER_L_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBeamColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBeamColorNr;
					PaintMarker(polDC, MARKER_R_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iLTTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iLTTriangleColorNr;
					PaintMarker(polDC, MARKER_LT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iRTTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iRTTriangleColorNr;
					PaintMarker(polDC, MARKER_RT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iRBTriangleColorNr;
					PaintMarker(polDC, MARKER_RB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oHeader[ilCol])->iLBTriangleColorNr;
					PaintMarker(polDC, MARKER_LB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
			}
			//***** Header Text Color
			if(ilHeaderCols <= ilCol)
			{
				polDC->SetTextColor(omDefHeaderTextColor);
			}
			else if(((FIELDDATA*)rmTableData.oHeader[ilCol])->oTextColor == -1)
			{
				polDC->SetTextColor(omDefHeaderTextColor);
			}
			else
			{
				polDC->SetTextColor(((FIELDDATA*)rmTableData.oHeader[ilCol])->oTextColor);
			}
			//***** Header Text
			if(ilHeaderCols > ilCol)
			{
				olText = ((FIELDDATA*)rmTableData.oHeader[ilCol])->oText;
			}
			polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefHeaderFormat);
		}
	}
	else if(ipRow >= 0 && ipColumn == -1 && imSolidWidth > 0)
	{
		if(ilRow >= 0 && ilRow < imRows)
		{
			//----- fill solid  -----
			polDC->SetBkMode(TRANSPARENT);// TRANSPARENT  OPAQUE
			polDC->SelectObject(pomDefSolidFont);

			olTextRect.left   = ilXStart+1;
			olTextRect.top    = ilYStart+imHeaderHight+imRowHight*ilRow+1;
			olTextRect.right  = olTextRect.left+imSolidWidth-1;
			olTextRect.bottom = olTextRect.top+imRowHight-1;

			int ilSolidRows = rmTableData.oSolid.GetSize();
			//***** Solid BkColor
			if(ilSolidRows <= ilRow)
			{
				polDC->FillRect(olTextRect,&omDefSolidBrush);	
			}
			else if(((FIELDDATA*)rmTableData.oSolid[ilRow])->oBkColorNr < 0)
			{
				polDC->FillRect(olTextRect,&omDefSolidBrush);	
			}
			else
			{
				int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->oBkColorNr;
				polDC->FillRect(olTextRect,(CBrush*)omDefBrushes[ilNr]);
			}
			//***** Solid Lines
			if(ilSolidRows > ilRow)
			{
				PaintExtraLines(polDC, olTextRect, (FIELDDATA*)rmTableData.oSolid[ilRow], 1, 1, ilRow, ilSolidRows);
			}
			//***** Solid Marker
			if(ilSolidRows > ilRow)
			{
				if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBeamColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBeamColorNr;
					PaintMarker(polDC, MARKER_L_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBeamColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBeamColorNr;
					PaintMarker(polDC, MARKER_R_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iLTTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iLTTriangleColorNr;
					PaintMarker(polDC, MARKER_LT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iRTTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iRTTriangleColorNr;
					PaintMarker(polDC, MARKER_RT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iRBTriangleColorNr;
					PaintMarker(polDC, MARKER_RB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
				if(((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBTriangleColorNr >= 0)
				{
					int ilNr = ((FIELDDATA*)rmTableData.oSolid[ilRow])->iLBTriangleColorNr;
					PaintMarker(polDC, MARKER_LB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
				}
			}
			//***** Solid Text Color
			if(ilSolidRows <= ilRow)
			{
				polDC->SetTextColor(omDefSolidTextColor);
			}
			else if(((FIELDDATA*)rmTableData.oSolid[ilRow])->oTextColor == -1)
			{
				polDC->SetTextColor(omDefSolidTextColor);
			}
			else
			{
				polDC->SetTextColor(((FIELDDATA*)rmTableData.oSolid[ilRow])->oTextColor);
			}
			//***** Solid Text
			if(ilSolidRows > ilRow)
			{
				olText = ((FIELDDATA*)rmTableData.oSolid[ilRow])->oText;
			}
			polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefSolidFormat);
		}
	}
	else if(ipRow >= 0 && ipColumn >= 0)
	{
		if(ilRow >= 0 && ilCol >= 0 && ilRow < imRows && ilCol < imColumns)
		{
			//----- fill table  -----
			polDC->SetBkMode(TRANSPARENT);// TRANSPARENT  OPAQUE
			polDC->SelectObject(pomDefTableFont);
			
			olTextRect.left   = ilXStart+imSolidWidth+ilCol*imColumnWidth+1;
			olTextRect.top    = ilYStart+imHeaderHight+ilRow*imRowHight+1;
			olTextRect.right  = olTextRect.left+imColumnWidth-1;
			olTextRect.bottom = olTextRect.top+imRowHight-1;

			int ilTableRows = rmTableData.oTable.GetSize();

			int ilTableCols = 0;
			CPtrArray *polTableArray = NULL;

			if(ilTableRows > ilRow)
			{
				polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
				ilTableCols = polTableArray->GetSize();
			}
			if(ilTableRows > ilRow)
			{
				//***** Table BkColor
				if(ilTableCols <= ilCol)
				{
					polDC->FillRect(olTextRect,&omDefTableBrush);	
				}
				else if(((FIELDDATA*)(*polTableArray)[ilCol])->oBkColorNr < 0)
				{
					polDC->FillRect(olTextRect,&omDefTableBrush);	
				}
				else
				{
					int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->oBkColorNr;
					polDC->FillRect(olTextRect,(CBrush*)omDefBrushes[ilNr]);
				}
				//***** Table Lines
				if(ilTableCols > ilCol)
				{
					PaintExtraLines(polDC, olTextRect, (FIELDDATA*)(*polTableArray)[ilCol], ilCol, ilTableCols, ilRow, ilTableRows);
				}
				//***** Table Marker
				if(ilTableCols > ilCol)
				{
					if(((FIELDDATA*)(*polTableArray)[ilCol])->iLBeamColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iLBeamColorNr;
						PaintMarker(polDC, MARKER_L_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)(*polTableArray)[ilCol])->iRBeamColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iRBeamColorNr;
						PaintMarker(polDC, MARKER_R_B, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)(*polTableArray)[ilCol])->iLTTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iLTTriangleColorNr;
						PaintMarker(polDC, MARKER_LT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)(*polTableArray)[ilCol])->iRTTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iRTTriangleColorNr;
						PaintMarker(polDC, MARKER_RT_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)(*polTableArray)[ilCol])->iRBTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iRBTriangleColorNr;
						PaintMarker(polDC, MARKER_RB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
					if(((FIELDDATA*)(*polTableArray)[ilCol])->iLBTriangleColorNr >= 0)
					{
						int ilNr = ((FIELDDATA*)(*polTableArray)[ilCol])->iLBTriangleColorNr;
						PaintMarker(polDC, MARKER_LB_T, olTextRect, (CBrush*)omDefBrushes[ilNr]);
					}
				}
				//***** Table Text Color
				if(ilTableCols <= ilCol)
				{
					polDC->SetTextColor(omDefTableTextColor);
				}
				else if(((FIELDDATA*)(*polTableArray)[ilCol])->oTextColor == -1)
				{
					polDC->SetTextColor(omDefTableTextColor);
				}
				else
				{
					polDC->SetTextColor(((FIELDDATA*)(*polTableArray)[ilCol])->oTextColor);
				}
				//***** Table Text
				if(ilTableCols > ilCol)
				{
					olText = ((FIELDDATA*)(*polTableArray)[ilCol])->oText;
				}
			
				switch(((FIELDDATA*)(*polTableArray)[ilCol])->iFontStyle)
				{
					//case FONT_STYLE_REGULAR:
					//case FONT_STYLE_BOLD:
				default:
					polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefTableFormat);
					break;
				case FONT_STYLE_ITALIC:
					CFont* polOldFont = polDC->SelectObject(pomItalicTableFont);	
					polDC->DrawText(olText,olText.GetLength(),olTextRect, imDefTableFormat);
					polDC->SelectObject(polOldFont);
					break;
				}
			}
			else
			{
				polDC->FillRect(olTextRect,&omDefTableBrush);	
				polDC->SetTextColor(omDefTableTextColor);
			}
		}
	}
}

//---------------------------------------------------------------------------

void CCSDynTable::PaintExtraLines(CDC *popDC, CRect opRect, FIELDDATA *popFieldData, int ipCol, int ipTableCols, int ipRow, int ipTableRows)
{

	if((popFieldData->iLLineWidth == NORMAL_LINE && ipCol != 0) || popFieldData->iLLineWidth == NORMAL_LINE_ALWAYS)
	{
		
		if(popFieldData->iLLineColorNr < 0)
		{
			popDC->SelectObject(&omBlackPen);
		}
		else
		{
			popDC->SelectObject((CPen*)omDefPens[popFieldData->iLLineColorNr]);
		}

		popDC->MoveTo(opRect.left, opRect.top);
		popDC->LineTo(opRect.left, opRect.bottom);
	}
	if((popFieldData->iTLineWidth == NORMAL_LINE && ipRow != 0) || popFieldData->iTLineWidth == NORMAL_LINE_ALWAYS)
	{
		if(popFieldData->iTLineColorNr < 0)
		{
			popDC->SelectObject(&omBlackPen);
		}
		else
		{
			popDC->SelectObject((CPen*)omDefPens[popFieldData->iLLineColorNr]);
		}
		popDC->MoveTo(opRect.left, opRect.top);
		popDC->LineTo(opRect.right, opRect.top);
	}
	if((popFieldData->iRLineWidth == NORMAL_LINE && ipCol+1 != ipTableCols) || popFieldData->iRLineWidth == NORMAL_LINE_ALWAYS)
	{
		if(popFieldData->iRLineColorNr < 0)
		{
			popDC->SelectObject(&omBlackPen);
		}
		else
		{
			popDC->SelectObject((CPen*)omDefPens[popFieldData->iLLineColorNr]);
		}
		popDC->MoveTo(opRect.right-1, opRect.top);
		popDC->LineTo(opRect.right-1, opRect.bottom);
	}
	if((popFieldData->iBLineWidth == NORMAL_LINE && ipRow+1 != ipTableRows) || popFieldData->iBLineWidth == NORMAL_LINE_ALWAYS)
	{
		if(popFieldData->iBLineColorNr < 0)
		{
			popDC->SelectObject(&omBlackPen);
		}
		else
		{
			popDC->SelectObject((CPen*)omDefPens[popFieldData->iLLineColorNr]);
		}
		popDC->MoveTo(opRect.left, opRect.bottom-1);
		popDC->LineTo(opRect.right, opRect.bottom-1);
	}

	popDC->SelectObject(&omBlackPen);
	//popDC->SelectObject(&omDefTableBrush);
}

//---------------------------------------------------------------------------
void CCSDynTable::PaintMarker(CDC *popDC, int ipMarker, CRect opRect, CBrush *popBrush)
{
	int ilHight = (opRect.bottom - opRect.top)/3;
	ilHight +=  1;

	if(ipMarker == MARKER_L_B || ipMarker == MARKER_R_B)
	{
		CRect olBeamRect;
		switch(ipMarker)
		{
			case MARKER_L_B:
				olBeamRect.left   = opRect.left;
				olBeamRect.top    = opRect.top;
				olBeamRect.right  = opRect.left + ilHight;
				olBeamRect.bottom = opRect.bottom;
				break;
			case MARKER_R_B:
				olBeamRect.left   = opRect.right - ilHight;
				olBeamRect.top    = opRect.top;
				olBeamRect.right  = opRect.right;
				olBeamRect.bottom = opRect.bottom;
				break;
		}
		popDC->FillRect(olBeamRect,popBrush);
	}
	if(ipMarker == MARKER_LT_T || ipMarker == MARKER_RT_T || ipMarker == MARKER_RB_T || ipMarker == MARKER_LB_T)
	{
		CPoint olTrianglePoints[3];
		switch(ipMarker)
		{
			case MARKER_LT_T:
				olTrianglePoints[0].x = opRect.left;
				olTrianglePoints[0].y = opRect.top;
				olTrianglePoints[1].x = opRect.left + ilHight;
				olTrianglePoints[1].y = opRect.top;
				olTrianglePoints[2].x = opRect.left;
				olTrianglePoints[2].y = opRect.top + ilHight;
				break;
			case MARKER_RT_T:
				olTrianglePoints[0].x = opRect.right;
				olTrianglePoints[0].y = opRect.top;
				olTrianglePoints[1].x = opRect.right;
				olTrianglePoints[1].y = opRect.top + ilHight;
				olTrianglePoints[2].x = opRect.right - ilHight;
				olTrianglePoints[2].y = opRect.top;
				break;
			case MARKER_RB_T:
				olTrianglePoints[0].x = opRect.right;
				olTrianglePoints[0].y = opRect.bottom;
				olTrianglePoints[1].x = opRect.right - (ilHight+1) ;
				olTrianglePoints[1].y = opRect.bottom;
				olTrianglePoints[2].x = opRect.right;
				olTrianglePoints[2].y = opRect.bottom - (ilHight+1);
				break;
			case MARKER_LB_T:
				olTrianglePoints[0].x = opRect.left;
				olTrianglePoints[0].y = opRect.bottom;
				olTrianglePoints[1].x = opRect.left;
				olTrianglePoints[1].y = opRect.bottom - (ilHight+1);
				olTrianglePoints[2].x = opRect.left + (ilHight+1);
				olTrianglePoints[2].y = opRect.bottom;
				break;
		}
		//popDC->SelectObject(opBrush);
		//popDC->SetPolyFillMode(WINDING);
		//popDC->Polygon(olTrianglePoints, 3);
		CRgn  olRgn;
		olRgn.CreatePolygonRgn(olTrianglePoints, 3, WINDING);
		popDC->FillRgn(&olRgn, popBrush);
	}
}

//---------------------------------------------------------------------------

void CCSDynTable::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	//nSBCode f�r HScroll:
	//SB_LINELEFT      0
	//SB_LINERIGHT     1
	//SB_PAGELEFT      2
	//SB_PAGERIGHT     3
	//SB_THUMBPOSITION 4
	//SB_THUMBTRACK	   5
	//SB_LEFT          6
	//SB_RIGHT         7
	//SB_ENDSCROLL	   8

	int ilPosTmp1,ilPosTmp2,ilOldPos;
	int ilRangeMax,ilRangeMin;

	ilPosTmp1 = pScrollBar->GetScrollPos();
	ilOldPos = ilPosTmp1;
	pScrollBar->GetScrollRange(&ilRangeMin,&ilRangeMax);

	if((void*)pScrollBar == (void*)pomHScroll)
	{
		ilPosTmp2 = imColumns-1;
		switch(nSBCode)
		{
			case SB_THUMBTRACK:
				ilPosTmp1 = nPos;
				break;
			case SB_THUMBPOSITION:
				ilPosTmp1 = nPos;
				break;
			case SB_PAGELEFT:
				if((ilPosTmp1 - ilPosTmp2) > ilRangeMin)
				{
					ilPosTmp1 -= ilPosTmp2;
				}
				else
				{
					ilPosTmp1 = ilRangeMin;
				}
				break;
			case SB_PAGERIGHT:
				if((ilPosTmp1 + ilPosTmp2) <= ilRangeMax - ilPosTmp2)
				{
					ilPosTmp1 += ilPosTmp2;
				}
				else
				{
					ilPosTmp1 = ilRangeMax-ilPosTmp2;
				}
				break;
			case SB_LINELEFT:
				if(ilPosTmp1 > ilRangeMin)
					ilPosTmp1--;
				break;
			case SB_LINERIGHT :
				if((ilPosTmp1 + 1) <= ilRangeMax - ilPosTmp2)
					ilPosTmp1++;
				break;
			case SB_LEFT:
				ilPosTmp1 = ilRangeMin;
				break;
			case SB_RIGHT:
				ilPosTmp1 = ilRangeMax-ilPosTmp2;
				if(ilPosTmp1<ilRangeMin)
					ilPosTmp1=ilRangeMin;
				break;
		}
		if(ilPosTmp1 != ilOldPos)
		{
			bool blIPEStatus = true;
			if(pomIPEdit != NULL)
				if(GetIPEditStatus() == false)
					blIPEStatus = false;

			if(blIPEStatus)
			{
				EndInplaceEditing();
				pScrollBar->SetScrollPos(ilPosTmp1);
				prmModifyTab->iOldHorzPos = ilOldPos;
				prmModifyTab->iNewHorzPos = ilPosTmp1;
				pomParent->SendMessage(WM_DYNTABLE_HSCROLL,imID,(long)prmModifyTab);
			}
		}
	}
}

//---------------------------------------------------------------------------

void CCSDynTable::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	//nSBCode f�r VScroll:
	//SB_LINEUP        0
	//SB_LINEDOWN      1
	//SB_PAGEUP        2
	//SB_PAGEDOWN      3
	//SB_THUMBPOSITION 4
	//SB_THUMBTRACK	   5
	//SB_TOP           6
	//SB_BOTTOM        7
	//SB_ENDSCROLL	   8

	int ilPosTmp1,ilPosTmp2,ilOldPos;
	int ilRangeMax,ilRangeMin;

	ilPosTmp1 = pScrollBar->GetScrollPos();
	ilOldPos = ilPosTmp1;
	pScrollBar->GetScrollRange(&ilRangeMin,&ilRangeMax);

	if((void*)pScrollBar == (void*)pomVScroll)
	{
		ilPosTmp2 = imRows-1;
		switch(nSBCode)
		{
			case SB_THUMBTRACK:
				ilPosTmp1 = nPos;
				break;
			case SB_THUMBPOSITION:
				ilPosTmp1 = nPos;
				break;
			case SB_PAGEUP:
				if((ilPosTmp1 - ilPosTmp2) > ilRangeMin)
				{
					ilPosTmp1 -= ilPosTmp2;
				}
				else
				{
					ilPosTmp1 = ilRangeMin;
				}
				break;
			case SB_PAGEDOWN:
				if((ilPosTmp1 + ilPosTmp2) <= ilRangeMax - ilPosTmp2)
				{
					ilPosTmp1 += ilPosTmp2;
				}
				else
				{
					ilPosTmp1 = ilRangeMax-ilPosTmp2;
				}
				break;
			case SB_LINEUP:
				if(ilPosTmp1 > ilRangeMin)
					ilPosTmp1--;
				break;
			case SB_LINEDOWN :
				if((ilPosTmp1 + 1) <= ilRangeMax - ilPosTmp2)
					ilPosTmp1++;
				break;
			case SB_TOP:
				ilPosTmp1 = ilRangeMin;
				break;
			case SB_BOTTOM:
				ilPosTmp1 = ilRangeMax-ilPosTmp2;
				if(ilPosTmp1<ilRangeMin)
					ilPosTmp1=ilRangeMin;
				break;
		}

		// Pr�fen ob Position ver�ndert wurde
		if(ilPosTmp1 != ilOldPos)
		{
			bool blIPEStatus = true;
			if(pomIPEdit != NULL)
				if(GetIPEditStatus() == false)
					blIPEStatus = false;

			if(blIPEStatus)
			{
				EndInplaceEditing();
				pScrollBar->SetScrollPos(ilPosTmp1);
				prmModifyTab->iOldVertPos = ilOldPos;
				prmModifyTab->iNewVertPos = ilPosTmp1;
				pomParent->SendMessage(WM_DYNTABLE_VSCROLL,imID,(long)prmModifyTab);
			}
		}
	}
}

//************************************************************************************
// ADO: Scrollt auf eine vorgegebende Position
//************************************************************************************

void CCSDynTable::ScrollToPosition(int ipPosition)
{
	int ilRangeMax,ilRangeMin;

	// Zuerst pr�fen ob die DynTable eine Scrollbar hat.
	if (pomVScroll != NULL)
	{
		EndInplaceEditing();
		pomVScroll->GetScrollRange(&ilRangeMin,&ilRangeMax);
		if(ilRangeMax > imRows)
		{
			// Pr�fen ob neue Position g�ltig
			if (ipPosition >= ilRangeMin && ipPosition <= ilRangeMax)
			{
				//Test auf Ende der Tabelle
				if (ipPosition + imRows > ilRangeMax)
					// es wurde "zu weit nach unten" gescrollt
				{
					// Maximalwert einstellen
					ipPosition = (ilRangeMax - imRows) + 1; 
				}
				
				prmModifyTab->iOldVertPos = pomVScroll->GetScrollPos();
				pomVScroll->SetScrollPos(ipPosition);
				prmModifyTab->iNewVertPos = ipPosition;
				pomParent->SendMessage(WM_DYNTABLE_VSCROLL,imID,(long)prmModifyTab);
			}
		}
	}
}

//---------------------------------------------------------------------------

void CCSDynTable::ResetContent()
{
	//****** Header
	for(int i = rmTableData.oHeader.GetSize()-1;i>=0;i--)
	{
		FIELDDATA *polValue = (FIELDDATA*)rmTableData.oHeader[i];
		delete polValue;
	}
	rmTableData.oHeader.RemoveAll();

	//****** Solid 
	for(i = rmTableData.oSolid.GetSize()-1;i>=0;i--)
	{
		FIELDDATA *polValue = (FIELDDATA*)rmTableData.oSolid[i];
		delete polValue;
	}
	rmTableData.oSolid.RemoveAll();

	//****** Table
	for(i = rmTableData.oTable.GetSize()-1;i>=0;i--)
	{
		CPtrArray *polValue = (CPtrArray*)rmTableData.oTable[i];

		for(int m = polValue->GetSize()-1;m>=0;m--)
		{
			FIELDDATA *polValueCol = (FIELDDATA*)(*polValue)[m];
			delete polValueCol;
		}
		polValue->RemoveAll();
		delete polValue;
	}
	rmTableData.oTable.RemoveAll();
}

//---------------------------------------------------------------------------

TABLEDATA* CCSDynTable::GetTableData()
{
	return &rmTableData;
}

//---------------------------------------------------------------------------

void CCSDynTable::GetTableInfo(TABLEINFO &rpTableInfo)
{
	rpTableInfo.iID = imID;
	rpTableInfo.iColumns = imColumns;
	rpTableInfo.iRows    = imRows;
	if(imHeaderHight > 0)
		rpTableInfo.bHeader  = true;
	if(imSolidWidth > 0)
		rpTableInfo.bSolid   = true;
	rpTableInfo.iMinDataHorz = imMinDataHorz;
	rpTableInfo.iMaxDataHorz = imMaxDataHorz;
	rpTableInfo.iMinDataVert = imMinDataVert;
	rpTableInfo.iMaxDataVert = imMaxDataVert;
	rpTableInfo.iColumnOffset = imColumnStartPos;
	rpTableInfo.iRowOffset    = imRowStartPos;
	rpTableInfo.iColumnWidth = imColumnWidth;
	rpTableInfo.iRowHight    = imRowHight;


}

//---------------------------------------------------------------------------

void CCSDynTable::SetHScrollData(UINT ipMin, UINT ipMax, UINT ipPos)
{
	bmIsDropFieldSelected = false;
	if(pomHScroll != NULL)
	{
		imMinDataHorz = ipMin;
		imMaxDataHorz = ipMax;
		SCROLLINFO rlScrollInfo;
		pomHScroll->SetScrollRange( ipMin,ipMax);
		pomHScroll->SetScrollPos(ipPos);
		pomHScroll->GetScrollInfo(&rlScrollInfo,SIF_ALL);
		rlScrollInfo.nPage = imColumns;
		pomHScroll->SetScrollInfo(&rlScrollInfo);
		if((int)(ipMax-ipMin) >= imColumns)
		{
			pomHScroll->EnableScrollBar(ESB_ENABLE_BOTH);
		}
		else
		{
			pomHScroll->EnableScrollBar(ESB_DISABLE_BOTH);
		}
	}
	imColumnStartPos = ipPos;
	prmModifyTab->iOldHorzPos = 0;
	prmModifyTab->iNewHorzPos = ipPos;
}

//---------------------------------------------------------------------------

void CCSDynTable::SetVScrollData(UINT ipMin, UINT ipMax, UINT ipPos)
{
	bmIsDropFieldSelected = false;
	if(pomVScroll != NULL)
	{
		imMinDataVert = ipMin;
		imMaxDataVert = ipMax;
		SCROLLINFO rlScrollInfo;
		pomVScroll->SetScrollRange(ipMin,ipMax);
		pomVScroll->SetScrollPos(ipPos);
		pomVScroll->GetScrollInfo(&rlScrollInfo,SIF_ALL);
		rlScrollInfo.nPage = imRows;
		pomVScroll->SetScrollInfo(&rlScrollInfo);
		if((int)(ipMax-ipMin) >= imRows)
		{
			pomVScroll->EnableScrollBar(ESB_ENABLE_BOTH);
		}
		else
		{
			pomVScroll->EnableScrollBar(ESB_DISABLE_BOTH);
		}
	}
	imRowStartPos = ipPos;
	prmModifyTab->iOldVertPos = 0;
	prmModifyTab->iNewVertPos = ipPos;
}

//---------------------------------------------------------------------------

void CCSDynTable::ChangedTableData(int ipNewColumnOffset, int ipNewRowOffset)
{
	prmModifyTab->iOldVertPos = imColumnStartPos;
	prmModifyTab->iOldHorzPos = imRowStartPos;

	imColumnStartPos = ipNewColumnOffset;
	imRowStartPos    = ipNewRowOffset;

	prmModifyTab->iNewHorzPos = imColumnStartPos;
	prmModifyTab->iNewVertPos = imRowStartPos;

	CDC *polDC = GetDC();
	PaintText(polDC);
	ReleaseDC(polDC);
}

//---------------------------------------------------------------------------

void CCSDynTable::ChangedFieldData(int ipRow ,int ipColumn)
{
	CDC *polDC = GetDC();
	PaintFieldText(polDC, ipColumn, ipRow);
	ReleaseDC(polDC);
}
//---------------------------------------------------------------------------

void CCSDynTable::SizeTable(/*UINT nSide, */LPRECT lpRect)
{
	CRect olNewParentWndRect = *lpRect;

	int ilWidthDif  = 0;
	int ilHeightDif = 0;

	bool blIsHSized = false;
	bool blIsVSized = false;
	bool blIsSized  = false;

	do
	{
		ilWidthDif  = (olNewParentWndRect.right - olNewParentWndRect.left)-imOldParentWndWidth + imHorzDiffToParentWnd;
		ilHeightDif = (olNewParentWndRect.bottom - olNewParentWndRect.top)-imOldParentWndHeight + imVertDiffToParentWnd;

		blIsSized = false;

		if(ilWidthDif < 0 && imColumns > 1 && bmHSizing)
		{
			blIsHSized = true;
			blIsSized = true;
			prmModifyTab->iOldColumns = imColumns;
			imColumns--;
			prmModifyTab->iNewColumns = imColumns;
			imOldParentWndWidth -= imColumnWidth;
		}
		else if(ilWidthDif >= imColumnWidth && bmHSizing)
		{
			blIsHSized = true;
			blIsSized = true;
			prmModifyTab->iOldColumns = imColumns;
			imColumns++;
			prmModifyTab->iNewColumns = imColumns;
			imOldParentWndWidth += imColumnWidth;
		}
		else if(ilHeightDif < 0 && imRows > 1 && bmVSizing)
		{
			blIsVSized = true;
			blIsSized = true;
			prmModifyTab->iOldRows = imRows;
			imRows--;
			prmModifyTab->iNewRows = imRows;
			imOldParentWndHeight -= imRowHight;
		}
		else if(ilHeightDif >= imRowHight && bmVSizing)
		{
			blIsVSized = true;
			blIsSized = true;
			prmModifyTab->iOldRows = imRows;
			imRows++;
			prmModifyTab->iNewRows = imRows;
			imOldParentWndHeight += imRowHight;
		}
	}
	while(blIsSized);

	if(blIsHSized || blIsVSized)
	{
		EndInplaceEditing();
		prmModifyTab->oOldRect = omTableRect;
		omTableRect.right  = omTableRect.left+imColumnWidth*imColumns+imSolidWidth+imScrollBarWidth+1;
		omTableRect.bottom = omTableRect.top+imRowHight*imRows+imHeaderHight+imScrollBarHight+1;
		prmModifyTab->oNewRect = omTableRect;

		if(blIsHSized)
		{
			/*if(imMaxDataHorz<=imColumns && imColumnStartPos > 0 && (prmModifyTab->iNewColumns - prmModifyTab->iOldColumns)>0)
			{
				prmModifyTab->iOldHorzPos = prmModifyTab->iNewHorzPos;
				imColumnStartPos--;
				prmModifyTab->iNewHorzPos = imColumnStartPos;
				if(pomHScroll != NULL)
				{
					pomHScroll->SetScrollPos(imColumnStartPos);
				}
			}*/
			pomParent->SendMessage(WM_DYNTABLE_HMOVE,imID,(long)prmModifyTab);
			if(imMaxDataHorz>=(imColumns-1) && imColumnStartPos > 0 && (prmModifyTab->iNewColumns - prmModifyTab->iOldColumns)>0)
			{
				if(pomHScroll != NULL)
				{
					OnHScroll(SB_LINELEFT,0, pomHScroll);
				}
			}
		}
		if(blIsVSized)
		{
			/*if(imMaxDataVert<=imRows && imRowStartPos > 0 && (prmModifyTab->iNewRows - prmModifyTab->iOldRows)>0)
			{
				prmModifyTab->iOldVertPos = prmModifyTab->iNewVertPos;
				imRowStartPos--;
				prmModifyTab->iNewVertPos = imRowStartPos;
				if(pomVScroll != NULL)
				{
					pomVScroll->SetScrollPos(imRowStartPos);
				}
			}*/
			pomParent->SendMessage(WM_DYNTABLE_VMOVE,imID,(long)prmModifyTab);
			if(imMaxDataVert>=(imRows-1) && imRowStartPos > 0 && (prmModifyTab->iNewRows - prmModifyTab->iOldRows)>0)
			{
				if(pomVScroll != NULL)
				{
					OnVScroll(SB_LINEUP, 0, pomVScroll);
				}
			}
		}

		MoveWindow(omTableRect);

		if(pomVScroll != NULL)
		{
			SCROLLINFO rlScrollInfo;
			CRect olRect;
			GetClientRect(&olRect);
			olRect.left = olRect.right - imScrollBarWidth;
			if(bmHScrollBar)
			{
				olRect.bottom = olRect.bottom - imScrollBarHight;
			}
			pomVScroll->MoveWindow(olRect);
			pomVScroll->GetScrollInfo(&rlScrollInfo,SIF_ALL);
			rlScrollInfo.nPage = imRows;
			pomVScroll->SetScrollInfo(&rlScrollInfo);
			if((rlScrollInfo.nMax-rlScrollInfo.nMin) >= imRows)
			{
				pomVScroll->EnableScrollBar(ESB_ENABLE_BOTH);
			}
			else
			{
				pomVScroll->EnableScrollBar(ESB_DISABLE_BOTH);
			}
		}
		if(pomHScroll != NULL)
		{
			SCROLLINFO rlScrollInfo;
			CRect olRect;
			GetClientRect(&olRect);
			olRect.top = olRect.bottom - imScrollBarHight;
			if(bmVScrollBar)
			{
				olRect.right = olRect.right - imScrollBarWidth;
			}
			pomHScroll->MoveWindow(olRect);
			pomHScroll->GetScrollInfo(&rlScrollInfo,SIF_ALL);
			rlScrollInfo.nPage = imColumns;
			pomHScroll->SetScrollInfo(&rlScrollInfo);
			if((rlScrollInfo.nMax-rlScrollInfo.nMin) >= imColumns)
			{
				pomHScroll->EnableScrollBar(ESB_ENABLE_BOTH);
			}
			else
			{
				pomHScroll->EnableScrollBar(ESB_DISABLE_BOTH);
			}
		}
	}
}

//---------------------------------------------------------------------------

void CCSDynTable::MoveTable(int ipX, int ipY)
{
	prmModifyTab->oOldRect = omTableRect;
	omTableRect.left   = omTableRect.left   + ipX;
	omTableRect.right  = omTableRect.right  + ipX;
	omTableRect.top    = omTableRect.top    + ipY;
	omTableRect.bottom = omTableRect.bottom + ipY;
	prmModifyTab->oNewRect = omTableRect;
	MoveWindow(omTableRect);
}

//---------------------------------------------------------------------------

void CCSDynTable::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CString olText;
	int ilColumn = 0;
	int ilRow    = 0;
	CRect olRect = CRect(0,0,0,0);
	if(GetTableFieldByPoint(point, ilRow, ilColumn, olRect))
	{
		bool blIPEStatus = true;
		if(ilRow > -1 || ilColumn > -1)
		{
			if(pomIPEdit != NULL)
				if(GetIPEditStatus() == false)
					blIPEStatus = false;
			if(blIPEStatus)
			{
				EndInplaceEditing();
			}
		}
		
		if(IsDrag(ilRow,ilColumn))
		{
			if(ilRow > -1 && ilColumn > -1) //Table
			{
				bmIsDropFieldSelected = true;

				rmInlineData.iFlag         = SELECT;
				rmInlineData.iRow          = ilRow;
				rmInlineData.iColumn       = ilColumn;
				rmInlineData.iRowOffset    = imRowStartPos;
				rmInlineData.iColumnOffset = imColumnStartPos;
				rmInlineData.oNewText      = "";
				rmInlineData.oOldText      = "";
				rmInlineData.oPoint        = point;
				pomParent->SendMessage(WM_TABLE_DRAGBEGIN,imID,(long)&rmInlineData);
			}
			else if(ilRow == -1 && ilColumn > -1) //Header
			{
				rmInlineData.iFlag         = 0;
				rmInlineData.iRow          = ilRow;
				rmInlineData.iColumn       = ilColumn;
				rmInlineData.iRowOffset    = imRowStartPos;
				rmInlineData.iColumnOffset = imColumnStartPos;
				rmInlineData.oOldText      = "";
				rmInlineData.oNewText      = "";
				rmInlineData.oPoint        = point;
				pomParent->SendMessage(WM_TABLE_DRAGBEGIN,imID,(long)&rmInlineData);
			}
			else if(ilRow > -1 && ilColumn == -1) //Solid
			{
				rmInlineData.iFlag         = 0;
				rmInlineData.iRow          = ilRow;
				rmInlineData.iColumn       = ilColumn;
				rmInlineData.iRowOffset    = imRowStartPos;
				rmInlineData.iColumnOffset = imColumnStartPos;
				rmInlineData.oOldText      = "";
				rmInlineData.oNewText      = "";
				rmInlineData.oPoint        = point;
				pomParent->SendMessage(WM_TABLE_DRAGBEGIN,imID,(long)&rmInlineData);
			}
		}
	}
	CWnd::OnLButtonDown(nFlags, point);
}

//*****************************************************************************
// We need to pass these messages to the tooltip for it to determine
// the position of the mouse
//*****************************************************************************

LRESULT CCSDynTable::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		{
			MSG msg;
			msg.hwnd = m_hWnd;
			msg.message = message;
			msg.wParam = wParam;
			msg.lParam = lParam;
			omToolTip.RelayEvent(&msg);
		}
	}

	return CWnd::WindowProc(message,wParam,lParam);
}

//*****************************************************************************
// Per Maus und Steuerungstaste sollen Felder Markiert werden
//*****************************************************************************

void CCSDynTable::OnMouseMove(UINT nFlags, CPoint point) 
{
	int ilColumn = 0;
	int ilRow    = 0;
	CRect olRect = CRect(0,0,0,0);
	if(GetTableFieldByPoint(point, ilRow, ilColumn, olRect))
	{
		pomParent->SendMessage(WM_DYNTABLE_MOUSEMOVE,imID,ilRow);
		
		if(ilRow > -1 && ilColumn > -1 && (nFlags & MK_CONTROL) && (nFlags & MK_LBUTTON))  //Table und STRG Taste
		{
			if(IsDrop(ilRow,ilColumn))
			{
				bmIsDropFieldSelected = true;

				rmInlineData.iFlag         = ONLYSELECT;
				rmInlineData.iRow          = ilRow;
				rmInlineData.iColumn       = ilColumn;
				rmInlineData.iRowOffset    = imRowStartPos;
				rmInlineData.iColumnOffset = imColumnStartPos;
				rmInlineData.oNewText      = "";
				if(ilRow<rmTableData.oTable.GetSize())
				{
					CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
					if(ilColumn<polTableArray->GetSize())
					{
						rmInlineData.oOldText = ((FIELDDATA*)(*polTableArray)[ilColumn])->oText;
					}
				}
				rmInlineData.oPoint        = point;
				pomParent->SendMessage(WM_DYNTABLE_LBUTTONUP,imID,(long)&rmInlineData);
				// Sichern des letzten Punktes
				omLastPoint = point;
			}
		}
		//********ToolTip************
		if(omOldToolTipRect != olRect)
		{
			//das Rect Hat sich ver�ndert
			//gibt es einen ToolTip Text 
			CString olToolTipText = GetToolTipText(ilRow,ilColumn);

			if(olToolTipText.IsEmpty())
				omToolTip.Activate(FALSE);
			else
				omToolTip.Activate(TRUE);

			omOldToolTipRect = olRect;
			omToolTip.UpdateTipText(olToolTipText, this);
		}
		//********ToolTip Ende************
	}

	CWnd::OnMouseMove(nFlags, point);
}

//*****************************************************************************
//
//*****************************************************************************

void CCSDynTable::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// Aktion ignorieren, wenn der gleiche Punkt wie bei MouseMove
	if (omLastPoint == point)
	{
		CWnd::OnLButtonUp(nFlags, point);
		return;
	}

	int ilColumn = 0;
	int ilRow    = 0;
	CRect olRect = CRect(0,0,0,0);
	if(GetTableFieldByPoint(point, ilRow, ilColumn, olRect))
	{
		bool blIPEStatus = true;
		if(ilRow > -1 || ilColumn > -1)
		{
			if(pomIPEdit != NULL)
				if(GetIPEditStatus() == false)
					blIPEStatus = false;
			if(blIPEStatus)
			{
				EndInplaceEditing();
			}
			if(!(nFlags & MK_CONTROL) && bmIsDropFieldSelected)
			{
					bmIsDropFieldSelected = false;

					rmInlineData.iFlag         = UNSELECT;
					rmInlineData.iRow          = 0;
					rmInlineData.iColumn       = 0;
					rmInlineData.iRowOffset    = 0;
					rmInlineData.iColumnOffset = 0;
					rmInlineData.oNewText      = "";
					rmInlineData.oOldText      = "";
					rmInlineData.oPoint        = point;

					pomParent->SendMessage(WM_DYNTABLE_LBUTTONUP,imID,(long)&rmInlineData);
			}
		}
		if(ilRow > -1 && ilColumn > -1) //Table
		{
			if(nFlags & MK_CONTROL)
			{
				if(IsDrop(ilRow,ilColumn))
				{
					bmIsDropFieldSelected = true;

					rmInlineData.iFlag         = SELECT;
					rmInlineData.iRow          = ilRow;
					rmInlineData.iColumn       = ilColumn;
					rmInlineData.iRowOffset    = imRowStartPos;
					rmInlineData.iColumnOffset = imColumnStartPos;
					rmInlineData.oNewText      = "";
					if(ilRow<rmTableData.oTable.GetSize())
					{
						CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
						if(ilColumn<polTableArray->GetSize())
						{
							rmInlineData.oOldText = ((FIELDDATA*)(*polTableArray)[ilColumn])->oText;
						}
					}
					rmInlineData.oPoint        = point;

					pomParent->SendMessage(WM_DYNTABLE_LBUTTONUP,imID,(long)&rmInlineData);
				}
			}
			else
			{
				if(blIPEStatus)
				{
					/*bool blEditCreated = */
					MakeInplaceEdit(ilRow,ilColumn);
				}
			}	
		}
		else if(ilRow == -1 && ilColumn > -1) //Header
		{
			rmInlineData.iFlag         = 0;
			rmInlineData.iRow          = ilRow;
			rmInlineData.iColumn       = ilColumn;
			rmInlineData.iRowOffset    = imRowStartPos;
			rmInlineData.iColumnOffset = imColumnStartPos;
			rmInlineData.oOldText      = "";
			rmInlineData.oNewText      = "";
			rmInlineData.oPoint        = point;
			pomParent->SendMessage(WM_DYNTABLE_LBUTTONUP,imID,(long)&rmInlineData);
		}
		else if(ilRow > -1 && ilColumn == -1) //Solid
		{
			rmInlineData.iFlag         = 0;
			rmInlineData.iRow          = ilRow;
			rmInlineData.iColumn       = ilColumn;
			rmInlineData.iRowOffset    = imRowStartPos;
			rmInlineData.iColumnOffset = imColumnStartPos;
			rmInlineData.oOldText      = "";
			rmInlineData.oNewText      = "";
			rmInlineData.oPoint        = point;
			pomParent->SendMessage(WM_DYNTABLE_LBUTTONUP,imID,(long)&rmInlineData);
		}
	}
	CWnd::OnLButtonUp(nFlags, point);
}

//---------------------------------------------------------------------------

void CCSDynTable::OnRButtonDown(UINT nFlags, CPoint point) 
{
	int ilColumn = 0;
	int ilRow    = 0;
	CRect olRect = CRect(0,0,0,0);
	if(GetTableFieldByPoint(point, ilRow, ilColumn, olRect))
	{
		if(ilRow > -1 || ilColumn > -1)
		{
			//not yet implemented
		}
		if(ilRow > -1 && ilColumn > -1) //Table
		{
			rmInlineData.iFlag				= 0;
			rmInlineData.iRow				= ilRow;
			rmInlineData.iColumn			= ilColumn;
			rmInlineData.iRowOffset			= imRowStartPos;
			rmInlineData.iColumnOffset		= imColumnStartPos;
			rmInlineData.oOldText			= "";
			rmInlineData.oNewText			= "";
			if(ilRow<rmTableData.oTable.GetSize())
			{
				CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
				if(ilColumn<polTableArray->GetSize())
				{
					rmInlineData.iRButtonDownStatus = ((FIELDDATA*)(*polTableArray)[ilColumn])->iRButtonDownStatus;
				}
			}
			CPoint olPoint = point;
			ClientToScreen(&olPoint);
			rmInlineData.oPoint				= olPoint;
			pomParent->SendMessage(WM_DYNTABLE_RBUTTONDOWN,imID,(long)&rmInlineData);
		}
		else if(ilRow == -1 && ilColumn > -1) //Header
		{
			rmInlineData.iFlag				= 0;
			rmInlineData.iRow				= ilRow;
			rmInlineData.iColumn			= ilColumn;
			rmInlineData.iRowOffset			= imRowStartPos;
			rmInlineData.iColumnOffset		= imColumnStartPos;
			rmInlineData.oOldText			= "";
			rmInlineData.oNewText			= "";
						
			CPoint olPoint = point;
			ClientToScreen(&olPoint);
			rmInlineData.oPoint				= olPoint;
			pomParent->SendMessage(WM_DYNTABLE_RBUTTONDOWN,imID,(long)&rmInlineData);
		}
		else if(ilRow > -1 && ilColumn == -1) //Solid
		{
			rmInlineData.iFlag				= 0;
			rmInlineData.iRow				= ilRow;
			rmInlineData.iColumn			= ilColumn;
			rmInlineData.iRowOffset			= imRowStartPos;
			rmInlineData.iColumnOffset		= imColumnStartPos;
			rmInlineData.oOldText			= "";
			rmInlineData.oNewText			= "";
			if(ilRow < rmTableData.oSolid.GetSize())
			{
				rmInlineData.iRButtonDownStatus = RBDS_ALL;//((FIELDDATA*)rmTableData.oSolid[ilRow])->iRButtonDownStatus;
			}
			CPoint olPoint = point;
			ClientToScreen(&olPoint);
			rmInlineData.oPoint				= olPoint;
			pomParent->SendMessage(WM_DYNTABLE_RBUTTONDOWN,imID,(long)&rmInlineData);
		}
	}
	CWnd::OnRButtonDown(nFlags, point);
}

//---------------------------------------------------------------------------

void CCSDynTable::OnRButtonUp(UINT nFlags, CPoint point) 
{
	int ilColumn = 0;
	int ilRow    = 0;
	CRect olRect = CRect(0,0,0,0);
	if(GetTableFieldByPoint(point, ilRow, ilColumn, olRect))
	{
		if(ilRow > -1 || ilColumn > -1)
		{
			//not yet implemented
		}
		if(ilRow > -1 && ilColumn > -1) //Table
		{
/*			rmInlineData.iFlag				= 0;
			rmInlineData.iRow				= ilRow;
			rmInlineData.iColumn			= ilColumn;
			rmInlineData.iRowOffset			= imRowStartPos;
			rmInlineData.iColumnOffset		= imColumnStartPos;
			rmInlineData.oOldText			= "";
			rmInlineData.oNewText			= "";
			if(ilRow<rmTableData.oTable.GetSize())
			{
				CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
				if(ilColumn<polTableArray->GetSize())
				{
					rmInlineData.iRButtonDownStatus = ((FIELDDATA*)(*polTableArray)[ilColumn])->iRButtonDownStatus;
				}
			}
			CPoint olPoint = point;
			ClientToScreen(&olPoint);
			rmInlineData.oPoint				= olPoint;
			pomParent->SendMessage(WM_DYNTABLE_RBUTTONUP,imID,(long)&rmInlineData);*/
		}
		else if(ilRow == -1 && ilColumn > -1) //Header
		{
			//not yet implemented
		}
		else if(ilRow > -1 && ilColumn == -1) //Solid
		{
			//not yet implemented
		}
	}
	CWnd::OnRButtonUp(nFlags, point);
}

//---------------------------------------------------------------------------

bool CCSDynTable::MakeInplaceEdit(int ipRow, int ipColumn)
{
	if(bmInplaceEdit == false)
		return false;

	if(IsIPEdit(ipRow, ipColumn))
	{
		CreateIPEdit(ipRow,ipColumn);
	}
	else
	{
		return false;
	}

	return true;
}

//---------------------------------------------------------------------------

void CCSDynTable::CreateIPEdit(int ipRow, int ipColumn)
{
	CRect olFieldRect;
	olFieldRect.left   = imSolidWidth+ipColumn*imColumnWidth+1;//0
	olFieldRect.top    = imHeaderHight+ipRow*imRowHight+1;//0
	olFieldRect.right  = olFieldRect.left+imColumnWidth-1;//+1
	olFieldRect.bottom = olFieldRect.top+imRowHight-1;//+1

	rmIPEditPaintInfo.bExist = true;
	rmIPEditPaintInfo.iNewRow    = ipRow;
	rmIPEditPaintInfo.iNewColumn = ipColumn;

	bool blNotReadonly = true;
	if(IsIPEdit(ipRow, ipColumn))
	{
		CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ipRow];
		pomIPEdit = new CCSEdit(olFieldRect, this, this, pomDefTableFont, true, &(((FIELDDATA*)(*polTableArray)[ipColumn])->rIPEAttrib)); 
	}
	else
	{
		pomIPEdit = new CCSEdit(olFieldRect, this, this, pomDefTableFont, true, &rmNoIEditAttrib);
		blNotReadonly = false;
	}

	CString olText;
	if(rmTableData.oTable.GetSize() > ipRow)
	{
		CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ipRow];
		if(polTableArray->GetSize() > ipColumn)
		{
			olText = ((FIELDDATA*)(*polTableArray)[ipColumn])->oText;
		}
	}

	// Sollte es sich um das Defaultzeichen handeln, dann LEER String �bergeben
	// DefaultZeichen f�r nicht geplante Tage
	CString olDefaultText = ogCCSParam.GetParamValue(ogAppl,"ID_BLANK_CHAR");

	if (olText == olDefaultText)
		olText.Empty();
	
	pomIPEdit->SetInitText(olText);
	

	if(blNotReadonly)
		pomIPEdit->SetSel(0,-1);

	bmInplaceEditExist = true;
	imEditColumn = ipColumn;
	imEditRow    = ipRow;
}

//---------------------------------------------------------------------------

void CCSDynTable::EndInplaceEditing()
{
	if(pomIPEdit != NULL && bmDestroyIPEditAction == false)
	{
		bmDestroyIPEditAction = true;
		CheckInlineUpdate();
		rmIPEditPaintInfo.iOldRow	 = rmIPEditPaintInfo.iNewRow;
		rmIPEditPaintInfo.iOldColumn = rmIPEditPaintInfo.iNewColumn;
		rmIPEditPaintInfo.iNewRow    = -1;
		rmIPEditPaintInfo.iNewColumn = -1;

		// BDA 18.02.2000 wenn Gruppensortierung aktiviert ist, l�scht
		// DutyRoster_View::FillGroupStruct() pomIPEdit. Es passiert
		// durch CheckInlineUpdate() und so muss pomIPEdit noch mal aufs
		// Null gepr�fft werden, sonst knallt's.
		if(pomIPEdit != NULL)
		{
			pomIPEdit->DestroyWindow();
			delete pomIPEdit;
			pomIPEdit = NULL;
		}
		bmDestroyIPEditAction = false;
		bmInplaceEditExist = false;
	}
}

//---------------------------------------------------------------------------

bool CCSDynTable::GetTableFieldByPoint(CPoint point,int &ipRow,int &ipColumn, CRect &opRect)
{
	CRect olTableRect;
	GetClientRect(&olTableRect);

	int ilXStart = 0;
	int ilYStart = 0;
	int ilXEnd   = 0;
	int ilYEnd   = 0;
	///////Is the point at the Header
	if(imHeaderHight > 0)
	{
		ilXStart = olTableRect.left   + imSolidWidth;
		ilYStart = olTableRect.top;
		ilXEnd   = olTableRect.right  - imScrollBarWidth;
		ilYEnd   = olTableRect.top    + imHeaderHight;

		if(point.x>=ilXStart && point.x<=ilXEnd && point.y>=ilYStart && point.y<=ilYEnd)
		{
			ipRow    = -1;
			ipColumn = (int)((point.x-ilXStart-2)/imColumnWidth);
			if(ipColumn<0||ipColumn>=imColumns)
			{
				ipColumn = -1;
				ipRow    = -1;
				opRect = CRect(0,0,0,0);
				return false;
			}
			CRect olFieldRect;
			olFieldRect.left   = imSolidWidth+ipColumn*imColumnWidth+1;//0
			olFieldRect.top    = 1;//0
			olFieldRect.right  = olFieldRect.left+imColumnWidth-1;//+1
			olFieldRect.bottom = olFieldRect.top+imHeaderHight-1;//+1
			opRect = olFieldRect;
			return true;
		}
	}
	///////Is the point at the Solid
	if(imSolidWidth > 0)
	{
		ilXStart = olTableRect.left;
		ilYStart = olTableRect.top    + imHeaderHight;
		ilXEnd   = olTableRect.left   + imSolidWidth;
		ilYEnd   = olTableRect.bottom - imScrollBarHight;
		if(point.x>=ilXStart && point.x<=ilXEnd && point.y>=ilYStart && point.y<=ilYEnd)
		{
			ipColumn = -1;
			ipRow = (int)((point.y-ilYStart-2)/imRowHight);
			if(ipRow<0||ipRow>=imRows)
			{
				ipColumn = -1;
				ipRow    = -1;
				opRect = CRect(0,0,0,0);
				return false;
			}
			CRect olFieldRect;
			olFieldRect.left   = 1;//0
			olFieldRect.top    = imHeaderHight+ipRow*imRowHight+1;//0
			olFieldRect.right  = olFieldRect.left+imSolidWidth-1;//+1
			olFieldRect.bottom = olFieldRect.top+imRowHight-1;//+1
			opRect = olFieldRect;
			return true;
		}
	}
	///////Is the point at the Table
	ilXStart = olTableRect.left   + imSolidWidth;
	ilYStart = olTableRect.top    + imHeaderHight;
	ilXEnd   = olTableRect.right  - imScrollBarWidth;
	ilYEnd   = olTableRect.bottom - imScrollBarHight;

	if(point.x<ilXStart||point.x>ilXEnd||point.y<ilYStart||point.y>ilYEnd)
	{
		ipColumn = -1;
		ipRow    = -1;
		opRect = CRect(0,0,0,0);
		return false;
	}

	ipColumn = (int)((point.x-ilXStart-2)/imColumnWidth);
	ipRow = (int)((point.y-ilYStart-2)/imRowHight);

	if(ipColumn<0||ipColumn>=imColumns||ipRow<0||ipRow>=imRows)
	{
		ipColumn = -1;
		ipRow    = -1;
		opRect = CRect(0,0,0,0);
		return false;
	}

	CRect olFieldRect;
	olFieldRect.left   = imSolidWidth+ipColumn*imColumnWidth+1;//0
	olFieldRect.top    = imHeaderHight+ipRow*imRowHight+1;//0
	olFieldRect.right  = olFieldRect.left+imColumnWidth-1;//+1
	olFieldRect.bottom = olFieldRect.top+imRowHight-1;//+1

	opRect = olFieldRect;
	return true;
}

//---------------------------------------------------------------------------

LONG CCSDynTable::OnMoveInplaceRight(UINT wParam, LONG lParam)
{
	if(pomIPEdit != NULL)
		if(GetIPEditStatus() == false)
				return 0L;
	
	if(imEditColumn < imColumns-1)
	{
		EndInplaceEditing();
		CreateIPEdit(imEditRow,imEditColumn+1);
	}
	else if(imEditColumn == imColumns-1)
	{
		if(imColumnStartPos<(imMaxDataHorz-imColumns+1))
		{
			OnHScroll(SB_LINERIGHT,0, pomHScroll);
			EndInplaceEditing();
			CreateIPEdit(imEditRow,imEditColumn);
		}
		else
		{
			if(bmIPEditCRLF == true)
			{
				if(imEditRow < imRows-1)
				{
					OnHScroll(SB_LEFT, 0, pomHScroll);
					EndInplaceEditing();
					CreateIPEdit(imEditRow+1,0);
				}
				else if(imEditRow == imRows-1)
				{
					if(imRowStartPos<(imMaxDataVert-imRows+1))
					{
						OnHScroll(SB_LEFT, 0, pomHScroll);
						OnVScroll(SB_LINEDOWN, 0, pomVScroll);
						EndInplaceEditing();
						CreateIPEdit(imEditRow,0);
					}
					else
					{
						OnHScroll(SB_LEFT, 0, pomHScroll);
						OnVScroll(SB_TOP, 0, pomVScroll);
						EndInplaceEditing();
						CreateIPEdit(0,0);
					}
				}
			}
		}
	}
	return 0L;
}

//---------------------------------------------------------------------------

LONG CCSDynTable::OnMoveInplaceLeft(UINT wParam, LONG lParam)
{
	if(pomIPEdit != NULL)
		if(GetIPEditStatus() == false)
				return 0L;

	if(imEditColumn > 0)
	{
		EndInplaceEditing();
		CreateIPEdit(imEditRow,imEditColumn-1);
	}
	else if(imEditColumn == 0)
	{
		if(imColumnStartPos > imMinDataHorz)
		{
			OnHScroll(SB_LINELEFT,0, pomHScroll);
			EndInplaceEditing();
			CreateIPEdit(imEditRow,imEditColumn);
		}
		else
		{
			if(bmIPEditCRLF == true)
			{
				if(imEditRow > 0)
				{
					OnHScroll(SB_RIGHT, 0, pomHScroll);
					EndInplaceEditing();
					CreateIPEdit(imEditRow-1,imColumns-1);
				}
				else if(imEditRow == 0)
				{
					if(imRowStartPos > 0)
					{
						OnHScroll(SB_RIGHT, 0, pomHScroll);
						OnVScroll(SB_LINEUP, 0, pomVScroll);
						EndInplaceEditing();
						CreateIPEdit(imEditRow,imColumns-1);
					}
					else
					{
						OnHScroll(SB_RIGHT, 0, pomHScroll);
						OnVScroll(SB_BOTTOM, 0, pomVScroll);
						EndInplaceEditing();
						CreateIPEdit(imRows-1,imColumns-1);
					}
				}
			}
		}
	}
	return 0L;
}
//---------------------------------------------------------------------------

LONG CCSDynTable::OnMoveInplaceUp(UINT wParam, LONG lParam)
{
	if(pomIPEdit != NULL)
		if(GetIPEditStatus() == false)
				return 0L;

		if(imEditRow > 0)
	{
		EndInplaceEditing();
		CreateIPEdit(imEditRow-1,imEditColumn);
	}
	else if(imEditRow == 0)
	{
		if(imRowStartPos > imMinDataVert)
		{
			OnVScroll(SB_LINEUP, 0, pomVScroll);
			EndInplaceEditing();
			CreateIPEdit(imEditRow,imEditColumn);
		}
	}
	return 0L;
}
//---------------------------------------------------------------------------

LONG CCSDynTable::OnMoveInplaceDown(UINT wParam, LONG lParam)
{
	if(pomIPEdit != NULL)
		if(GetIPEditStatus() == false)
				return 0L;

		if(imEditRow < imRows-1)
	{
		EndInplaceEditing();
		CreateIPEdit(imEditRow+1,imEditColumn);
	}
	else if(imEditRow == imRows-1)
	{
		if(imRowStartPos<(imMaxDataVert-imRows+1))
		{
			OnVScroll(SB_LINEDOWN, 0, pomVScroll);
			EndInplaceEditing();
			CreateIPEdit(imEditRow,imEditColumn);
		}
	}
	return 0L;
}

//---------------------------------------------------------------------------

LONG CCSDynTable::OnInplaceReturn(UINT wParam, LONG lParam)
{
	if(pomIPEdit != NULL)
	{
		if(GetIPEditStatus())
			EndInplaceEditing();
	}
	return 0L;
}

//---------------------------------------------------------------------------

bool CCSDynTable::CheckInlineUpdate()
{
	if(pomIPEdit != NULL)
	{
		if(IsIPEdit(imEditRow,imEditColumn))
		{
			if(GetIPEditStatus())
			{
				CString olNewText;
				CString olOldText;

				CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[imEditRow];
				olOldText = ((FIELDDATA*)(*polTableArray)[imEditColumn])->oText;
				olNewText = olOldText;

				pomIPEdit->GetWindowText(olNewText);

				if(olOldText != olNewText)
				{
					rmInlineData.iFlag         = IPEDIT_UPDATE;
					rmInlineData.iRow          = imEditRow;
					rmInlineData.iColumn       = imEditColumn;
					rmInlineData.iRowOffset    = imRowStartPos;
					rmInlineData.iColumnOffset = imColumnStartPos;
					rmInlineData.oOldText      = olOldText;
					rmInlineData.oNewText      = olNewText;
					rmInlineData.lNewUrno      = 0;

					pomParent->SendMessage(WM_DYNTABLE_INLINE_UPDATE,imID,(long)&rmInlineData);
				}
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

//---------------------------------------------------------------------------

bool CCSDynTable::GetIPEditStatus()
{
	if(pomIPEdit != NULL)
	{
		return pomIPEdit->GetStatus();
	}
	return false;
}

//---------------------------------------------------------------------------

bool CCSDynTable::GetDropStatus()
{
	// Future music
	return true;
}

//---------------------------------------------------------------------------

bool CCSDynTable::IsIPEdit(int ipRow,int ipColumn)
{
	bool blInplaceEdit = false;
	if(rmTableData.oTable.GetSize() > ipRow)
	{
		CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ipRow];
		if(polTableArray->GetSize() > ipColumn)
		{
			blInplaceEdit = ((FIELDDATA*)(*polTableArray)[ipColumn])->bInplaceEdit;
		}
	}
	return blInplaceEdit;
}

//---------------------------------------------------------------------------

bool CCSDynTable::IsDrag(int ipRow,int ipColumn)
{
	bool blDrag = false;
	if(imDragWord != 0)
	{
		if(ipRow > -1 && ipColumn > -1) //Table
		{
			if(rmTableData.oTable.GetSize() > ipRow)
			{
				CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ipRow];
				if(polTableArray->GetSize() > ipColumn)
				{
					blDrag = ((FIELDDATA*)(*polTableArray)[ipColumn])->bDrag;
				}
			}
		}
		else if(ipRow == -1 && ipColumn > -1) //Header
		{
			if(rmTableData.oHeader.GetSize() > ipColumn)
			{
				blDrag = ((FIELDDATA*)rmTableData.oHeader[ipColumn])->bDrag;
			}
		}
		else if(ipRow > -1 && ipColumn == -1) //Solid
		{
			if(rmTableData.oSolid.GetSize() > ipRow)
			{
				blDrag = ((FIELDDATA*)rmTableData.oSolid[ipRow])->bDrag;
			}
		}
	}
	return blDrag;
}

//---------------------------------------------------------------------------

bool CCSDynTable::IsDrop(int ipRow,int ipColumn)
{
	bool blDrop = false;
	if(imDropWord != 0)
	{
		if(ipRow > -1 && ipColumn > -1) //Table
		{
			if(rmTableData.oTable.GetSize() > ipRow)
			{
				CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ipRow];
				if(polTableArray->GetSize() > ipColumn)
				{
					blDrop = ((FIELDDATA*)(*polTableArray)[ipColumn])->bDrop;
				}
			}
		}
		else if(ipRow == -1 && ipColumn > -1) //Header
		{
			if(rmTableData.oHeader.GetSize() > ipColumn)
			{
				blDrop = ((FIELDDATA*)rmTableData.oHeader[ipColumn])->bDrop;
			}
		}
		else if(ipRow > -1 && ipColumn == -1) //Solid
		{
			if(rmTableData.oSolid.GetSize() > ipRow)
			{
				blDrop = ((FIELDDATA*)rmTableData.oSolid[ipRow])->bDrop;
			}
		}
	}
	return blDrop;
}

//---------------------------------------------------------------------------

void CCSDynTable::DragDropRevoke()
{
	omDragDrop.Revoke();
}

//---------------------------------------------------------------------------

void CCSDynTable::DropRegister(UINT ipDataClassWord)
{
	imDropWord = ipDataClassWord;
	omDragDrop.RegisterTarget(this, this);
}

//---------------------------------------------------------------------------

void CCSDynTable::DragRegister(UINT ipDataClassWord)
{
	imDragWord = ipDataClassWord;
	//omDragDrop.RegisterTarget(this, this);
}

//---------------------------------------------------------------------------

LONG CCSDynTable::OnDragOver(UINT wParam, LONG lParam)
{
	CCSDynTable *polTable = (CCSDynTable *)lParam;
	if(polTable == this && omDragDrop.GetDataClass() == imDropWord)
	{
		CPoint olDragOverPoint;
		::GetCursorPos(&olDragOverPoint);
		ScreenToClient(&olDragOverPoint);
		
		int ilColumn = 0;
		int ilRow    = 0;
		CRect olRect = CRect(0,0,0,0);
		
		if(GetTableFieldByPoint(olDragOverPoint, ilRow, ilColumn, olRect))
		{
			pomParent->SendMessage(WM_DRAGOVER,imID,ilRow);

			if(IsDrop(ilRow, ilColumn))
			{
				return 0L;	
			}
		}
	}
	return -1L;
}

//********************************************************************************************
// Beginn einer DnD Aktionen
//********************************************************************************************

void CCSDynTable::OnDrag(CString opText)
{

	if (opText.GetLength() > 0)
	{
		//omDragDrop
		// ACHTUN.G.: Ziel k�nnen beide Views sein !
		// D'n'D-Objekt initialisieren
		omDragDrop.CreateDWordData(DIT_DUTYROSTER_BSD_TABLE, 2);
		// Basisschichten-Datenobjekt hinzuf�gen
		omDragDrop.AddString(opText);
		// "Absenderkennung" mitgeben
		omDragDrop.AddString("BSD");
		// Aktion fortsetzen
		omDragDrop.BeginDrag();

		TRACE("CCSDynTable: Beginne DnD Aktion\n");
	}
	else
	{
		TRACE("CCSDynTable: DnD Aktion gestoppt\n");
	}
}

//********************************************************************************************
//
//********************************************************************************************

LONG CCSDynTable::OnDrop(UINT wParam, LONG lParam)
{
	CCSDynTable *polTable = (CCSDynTable *)lParam;
	if(polTable == this && omDragDrop.GetDataClass() == imDropWord)
	{
		CPoint olDragOverPoint;
		::GetCursorPos(&olDragOverPoint);
		ScreenToClient(&olDragOverPoint);

		int ilColumn = 0;
		int ilRow    = 0;
		CRect olRect = CRect(0,0,0,0);

		if(GetTableFieldByPoint(olDragOverPoint, ilRow, ilColumn, olRect))
		{
			if(IsDrop(ilRow, ilColumn))
			{
				if(GetDropStatus())
				{
					if(ilRow > -1 && ilColumn > -1) //Table
					{
						CString olOldText;
						CString olNewText;
						long	llNewUrno;

						CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
						olOldText	= ((FIELDDATA*)(*polTableArray)[ilColumn])->oText;

						olNewText = omDragDrop.GetDataString(0);
						llNewUrno = atol(omDragDrop.GetDataString(2));

						if(olOldText != olNewText || bmIsDropFieldSelected)
						{
							bmIsDropFieldSelected = false;
							rmInlineData.iFlag			= DRAGDROP_UPDATE;
							rmInlineData.iRow			= ilRow;
							rmInlineData.iColumn		= ilColumn;
							rmInlineData.iRowOffset		= imRowStartPos;
							rmInlineData.iColumnOffset	= imColumnStartPos;
							rmInlineData.oOldText		= olOldText;
							rmInlineData.oNewText		= olNewText;
							rmInlineData.lNewUrno		= llNewUrno;
							rmInlineData.iDropeffect	= (UINT)wParam;
							int ilPos1 = olNewText.Find("(");
							if (ilPos1 != -1)
							{
								// Das kommt aus DebitTab:
								//		" SHIFT1 (FUNC1, 08:00 - 16:00)"
								// oder " SHIFT1 (08:00 - 16:00)"
								int ilPos2 = olNewText.Find(',', ilPos1);
								if (ilPos2 != -1)
								{
									// Additiv ist nicht aktiviert, Funktion gespeichert
									rmInlineData.oFctc = olNewText.Mid (ilPos1 + 1, ilPos2 - ilPos1 - 1);
								}
							}

							pomParent->SendMessage(WM_DYNTABLE_INLINE_UPDATE,imID,(long)&rmInlineData);
						}
					}
					else if(ilRow == -1 && ilColumn > -1) //Header
					{
						//not yet implemented
					}
					else if(ilRow > -1 && ilColumn == -1) //Solid
					{
						CString olOldText;
						CString olNewText;
						long	llNewUrno;

						if(ilRow < rmTableData.oSolid.GetSize())
						{
							olOldText = ((FIELDDATA*)rmTableData.oSolid[ilRow])->oText;
						}

						olNewText = omDragDrop.GetDataString(0);
						llNewUrno = atol(omDragDrop.GetDataString(2));

						if(olOldText != olNewText || bmIsDropFieldSelected)
						{
							bmIsDropFieldSelected = false;
							rmInlineData.iFlag			= DRAGDROP_UPDATE;
							rmInlineData.iRow			= ilRow;
							rmInlineData.iColumn		= ilColumn;
							rmInlineData.iRowOffset		= imRowStartPos;
							rmInlineData.iColumnOffset	= imColumnStartPos;
							rmInlineData.oOldText		= olOldText;
							rmInlineData.oNewText		= olNewText;
							rmInlineData.lNewUrno		= llNewUrno;
							rmInlineData.iDropeffect	= (UINT)wParam;
							int ilPos1 = olNewText.Find("(");
							if (ilPos1 != -1)
							{
								int ilPos2 = olNewText.Find(',', ilPos1);
								if (ilPos2 != -1)
								{
									// Additiv ist nicht aktiviert, Funktion gespeichert
									rmInlineData.oFctc = olNewText.Mid (ilPos1 + 1, ilPos2 - ilPos1 - 1);
								}
							}

							pomParent->SendMessage(WM_DYNTABLE_INLINE_UPDATE,imID,(long)&rmInlineData);
						}
					}
				}
			}
		}
	}
	return 0L;
}

//---------------------------------------------------------------------------

void CCSDynTable::EndIPEditandDragDropAction()
{
	if(pomIPEdit != NULL)
	{
		bmDestroyIPEditAction = true;
		pomIPEdit->DestroyWindow();
		delete pomIPEdit;
		pomIPEdit = NULL;
		bmDestroyIPEditAction = false;
		bmInplaceEditExist = false;
	}
	if(bmIsDropFieldSelected)
	{
		bmIsDropFieldSelected = false;

		rmInlineData.iFlag         = UNSELECT;
		rmInlineData.iRow          = 0;
		rmInlineData.iColumn       = 0;
		rmInlineData.iRowOffset    = 0;
		rmInlineData.iColumnOffset = 0;
		rmInlineData.oNewText      = "";
		rmInlineData.oOldText      = "";

		pomParent->SendMessage(WM_DYNTABLE_LBUTTONUP,imID,(long)&rmInlineData);
	}
}

//---------------------------------------------------------------------------

CString CCSDynTable::GetToolTipText(int ipRow,int ipColumn)
{
	CString olToolTipText;
	if(ipRow > -1 && ipColumn > -1) //Table
	{
		if(rmTableData.oTable.GetSize() > ipRow)
		{
			CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ipRow];
			if(polTableArray->GetSize() > ipColumn)
			{
				olToolTipText = ((FIELDDATA*)(*polTableArray)[ipColumn])->oToolTipText;
			}
		}
	}
	else if(ipRow == -1 && ipColumn > -1) //Header
	{
		if(rmTableData.oHeader.GetSize() > ipColumn)
		{
			olToolTipText = ((FIELDDATA*)rmTableData.oHeader[ipColumn])->oToolTipText;
		}
	}
	else if(ipRow > -1 && ipColumn == -1) //Solid
	{
		if(rmTableData.oSolid.GetSize() > ipRow)
		{
			olToolTipText = ((FIELDDATA*)rmTableData.oSolid[ipRow])->oToolTipText;
		}
	}
	return olToolTipText;
}

//---------------------------------------------------------------------------

void CCSDynTable::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	int ilColumn = 0;
	int ilRow    = 0;
	CRect olRect = CRect(0,0,0,0);
	GetTableFieldByPoint(point, ilRow, ilColumn, olRect);
	if(ilRow > -1 && ilColumn > -1) //Table
	{
		rmInlineData.iFlag         = 0;
		rmInlineData.iRow          = ilRow;
		rmInlineData.iColumn       = ilColumn;
		rmInlineData.iRowOffset    = imRowStartPos;
		rmInlineData.iColumnOffset = imColumnStartPos;
		rmInlineData.oNewText      = "";
		if(ilRow<rmTableData.oTable.GetSize())
		{
			CPtrArray *polTableArray = (CPtrArray*)rmTableData.oTable[ilRow];
			if(ilColumn<polTableArray->GetSize())
			{
				rmInlineData.oOldText = ((FIELDDATA*)(*polTableArray)[ilColumn])->oText;
			}
		}
		rmInlineData.oPoint        = point;
		pomParent->SendMessage(WM_DYNTABLE_LBUTTONDBLCLK,imID,(long)&rmInlineData);
	}
	CWnd::OnLButtonDblClk(nFlags, point);
}

void CCSDynTable::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	
	//TRACE("DynTable:OnSize\n");
}

void CCSDynTable::OnSizing(UINT fwSide, LPRECT pRect) 
{
	CWnd::OnSizing(fwSide, pRect);
	
	//TRACE("DynTable:OnSizing\n");
	
}

//****************************************************************************
// Beenden des Windows
//****************************************************************************

void CCSDynTable::OnClose() 
{
	//Beenden �ber 'Esc' verbieten
	//CWnd::OnClose();
}


//*****************************************************************************
//*****************************************************************************
// Abgeleitete Klasse f�r den Debit Tab Viewer
// Ver�ndertes Drag Verhalten
//*****************************************************************************
//*****************************************************************************

CCSDebitDynTable::CCSDebitDynTable(CWnd* pParent /*=NULL*/, UINT ipID/*=0*/)
				 : CCSDynTable(pParent, ipID)
{
}

BEGIN_MESSAGE_MAP(CCSDebitDynTable, CCSDynTable)
	//{{AFX_MSG_MAP(CCSDebitDynTable)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//*****************************************************************************
// DnD einleiten
//*****************************************************************************

void CCSDebitDynTable::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CString olText;
	int ilColumn = 0;
	int ilRow    = 0;
	CRect olRect = CRect(0,0,0,0);
	if(GetTableFieldByPoint(point, ilRow, ilColumn, olRect))
	{
		bool blIPEStatus = true;
		if(ilRow > -1 || ilColumn > -1)
		{
			if(pomIPEdit != NULL)
				if(GetIPEditStatus() == false)
					blIPEStatus = false;
			if(blIPEStatus)
			{
				EndInplaceEditing();
			}
		}
		
		if(IsDrag(ilRow,ilColumn))
		{
			
			// from the statistic table (the shift demands from the coverage/shift planning)
			// on a shift (right columns)
			if(ilRow > -1 && ilColumn > -1) //Table
			{
				bmIsDropFieldSelected = true;

				rmInlineData.iFlag         = SELECT;
				rmInlineData.iRow          = ilRow;
				rmInlineData.iColumn       = ilColumn;
				rmInlineData.iRowOffset    = imRowStartPos;
				rmInlineData.iColumnOffset = imColumnStartPos;
				rmInlineData.oNewText      = "";
				rmInlineData.oOldText      = "";
				rmInlineData.oPoint        = point;

				// Text kommt aus Solid der gleichen Zeile
				olText = ((FIELDDATA*)rmTableData.oSolid[ilRow])->oText;

				int ilPos1 = olText.Find("(");
				if (ilPos1 != -1)
				{
					// Das kommt aus DebitTab:
					//		" SHIFT1 (FUNC1, 08:00 - 16:00)"
					// oder " SHIFT1 (08:00 - 16:00)"
					int ilPos2 = olText.Find(',', ilPos1);
					if (ilPos2 != -1)
					{
						// Additiv ist nicht aktiviert, Funktion gespeichert
						rmInlineData.oFctc = olText.Mid (ilPos1 + 1, ilPos2 - ilPos1 - 1);
					}
				}
				OnDrag(olText);
			}
			else if(ilRow == -1 && ilColumn > -1) //Header
			{
				rmInlineData.iFlag         = 0;
				rmInlineData.iRow          = ilRow;
				rmInlineData.iColumn       = ilColumn;
				rmInlineData.iRowOffset    = imRowStartPos;
				rmInlineData.iColumnOffset = imColumnStartPos;
				rmInlineData.oOldText      = "";
				rmInlineData.oNewText      = "";
				rmInlineData.oPoint        = point;
				int ilPos1 = olText.Find("(");
				if (ilPos1 != -1)
				{
					// Das kommt aus DebitTab:
					//		" SHIFT1 (FUNC1, 08:00 - 16:00)"
					// oder " SHIFT1 (08:00 - 16:00)"
					int ilPos2 = olText.Find(',', ilPos1);
					if (ilPos2 != -1)
					{
						// Additiv ist nicht aktiviert, Funktion gespeichert
						rmInlineData.oFctc = olText.Mid (ilPos1 + 1, ilPos2 - ilPos1 - 1);
					}
				}
				olText = "";
				OnDrag(olText);
			}
			
			// from the statistic table (the shift demands from the coverage/shift planning)
			// left column
			else if(ilRow > -1 && ilColumn == -1) //Solid
			{
				rmInlineData.iFlag         = 0;
				rmInlineData.iRow          = ilRow;
				rmInlineData.iColumn       = ilColumn;
				rmInlineData.iRowOffset    = imRowStartPos;
				rmInlineData.iColumnOffset = imColumnStartPos;
				rmInlineData.oOldText      = "";
				rmInlineData.oNewText      = "";
				rmInlineData.oPoint        = point;
				olText = ((FIELDDATA*)rmTableData.oSolid[ilRow])->oText;
				int ilPos1 = olText.Find("(");
				if (ilPos1 != -1)
				{
					// Das kommt aus DebitTab:
					//		" SHIFT1 (FUNC1, 08:00 - 16:00)"
					// oder " SHIFT1 (08:00 - 16:00)"
					int ilPos2 = olText.Find(',', ilPos1);
					if (ilPos2 != -1)
					{
						// Additiv ist nicht aktiviert, Funktion gespeichert
						rmInlineData.oFctc = olText.Mid (ilPos1 + 1, ilPos2 - ilPos1 - 1);
					}
				}
				OnDrag(olText);
			}
		}
	}
	CWnd::OnLButtonDown(nFlags, point);
}

