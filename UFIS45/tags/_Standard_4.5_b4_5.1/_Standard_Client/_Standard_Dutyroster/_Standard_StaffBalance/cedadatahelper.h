#ifndef _CEDADATAHELPER_H_
#define _CEDADATAHELPER_H_
 
#include <ccsglobl.h>

//----------------------------------------------------------------------------------------------------------------------------------------------------
// R�ckgabecodes f�r GetMaxPeriod()
#define CHECK_P2_OUTER_P1				0	// die erste Zeitspanne liegt innerhalb der zweiten Zeitspanne
#define CHECK_P2_INNER_ABOVE_P1			1	// die zweite Zeitspanne beginnt innerhalb der ersten Zeitspanne, �berschreitet diese aber
#define CHECK_P2_INNER_UNDERNEATH_P1	2	// die zweite Zeitspanne beginnt innerhalb der der ersten Zeitspanne, unterschreitet diese aber
#define CHECK_P2_INNER_P1				3	// die zweite Zeitspanne liegt innerhalb der ersten Zeitspanne
#define CHECK_P2_ABOVE_P1				5	// die zweite Zeitspanne liegt ausserhalb und �ber der ersten Zeitspanne										
#define CHECK_P2_UNDERNEATH_P1			10	// die zweite Zeitspanne liegt ausserhalb und unter der ersten Zeitspanne	
#define CHECK_INVALID_DATE				-1	// einer der Parameter ist ung�ltig									
//----------------------------------------------------------------------------------------------------------------------------------------------------

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaDataHelper: Hilfsfunktionen zum Konvertieren von Daten, etc.
//************************************************************************************************************************
//************************************************************************************************************************

class CedaDataHelper
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaDataHelper() {;}
	~CedaDataHelper() {;}

	// erzeugt aus einem String im Format 'YYYYMMDD' ein Objekt vom Typ COleDateTime
	static bool DateStringToOleDateTime(CString opDate, COleDateTime &opTime);
	// erzeugt aus einem Zeit-String im Format 'HHMM' oder 'HHMMSS' ein Objekt vom Typ COleDateTimeSpan
	static bool HourMinStringToOleDateTimeSpan(CString opHourMin, COleDateTimeSpan &opTimeSpan);
	// erzeugt aus einem Zeit-String im Format 'YYYYMMDDHHMMSS' ein Objekt vom Typ COleDateTimeSpan
	static bool DateTimeStringToOleDateTime(CString opDateTime, COleDateTime &opTime);
	// L�scht alle ":" und "." aus einen String
	static void DeleteExtraChars(CString &opString);
	// Anzahl der Tage eines Monats ermitteln
	static int GetDaysOfMonth(COleDateTime opDay);
	// Datums-String ('YYYYMMDD') umwandeln auf Wochende pr�fen
	static bool IsWeekend(CString opDateString);
	// pr�fen, ob der Tag in <opDate> ein Sonntag oder Sonnabend ist
	static bool IsWeekend(COleDateTime opDate);
	// pr�fen, wie zwei Zeitspannen zueinander liegen und den max. Zeitraum aus beiden ermitteln
	static int GetMaxPeriod(COleDateTime opP1From, COleDateTime opP1To, 
							COleDateTime opP2From, COleDateTime opP2To, 
							COleDateTime *popMaxPeriodFrom = NULL, 
							COleDateTime *popMaxPeriodTo = NULL);
};

#endif	// _CEDADATAHELPER_H_
