// Accounts.h: interface for the CAccounts class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ACCOUNTS_H__12926483_764A_11D3_8F0E_00001C034EA0__INCLUDED_)
#define AFX_ACCOUNTS_H__12926483_764A_11D3_8F0E_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CedaDrrData.h> // Dienstplan
#include <CedaEspData.h> // Arbeitszeitkonten bearbeitung (aus Dienstplan bearbeiten)
#include <CedaAccData.h> // Konten
#include <CedaCotData.h> // Arbeitsvertragsarten
#include <CedaScoData.h> // Zuordnung Arbeitsvertragsarten
#include <CedaStfData.h> // Mittarbeiterstammdaten
#include <CedaBsdData.h> // Basisschichten
#include <CedaOdaData.h> // Abwesenheiten
#include <CedaOrgData.h> // Organisationseinheiten

// Kontotypen
enum Kontotypen
{
	K_AZK = 10001,		//Arbeitszeitkonto
	K_VACATION,			//Urlaubskonto
	K_HOUERSNORM,		//Normstunden
	K_HOUERSPLAN,		//Planstunden
	K_HOUERSIS			//Geleistete Stunden


};

// Mit ACCOUNTTYPEINFO werden die einzelnen Kontentypen beschrieben
struct ACCOUNTTYPEINFO 
{
	UINT	iInternNumber;	// Rostering Interner KontoCode aus enum Kontotypen
	CString	oExternKey;		// Externer KontoSchl�ssel ist gleich dem Define ohne 'K_' aus enum Kontotypen 
	CString	oName;			// Name der Kontos
	CString	oShortName;		// Kurzname des Kontos
	CString	oFormat;		// Wie soll das Konto Formatiert werden (z.B. "%01.2f ")
	bool	bShowAccount;	// Soll das Konto �berhaupt zur Auswahl angezeigt werden

	ACCOUNTTYPEINFO(void)
	{
		iInternNumber	= 0;
		oExternKey		= "";
		oName			= "";
		oShortName		= "";
		oFormat			= "%f ";
		bShowAccount	= true;
	}
};

// Konto eines bestimmten Typs. Die Struktur h�lt die Info �ber
// Typ, Dauer und G�ltigkeit des Kontos und speichert die Konten
// jedes einzelnen Tages in Stringform???
struct RUNNINGACCOUNTTYPE
{
	UINT iType;		//AZK, Urlaub usw. (siehe enum ACCOUNT_TYPES)
	bool bOK;		//Is Account valid
	double dHour;	//Total Account Hour
	CStringArray oDailyElements;

	RUNNINGACCOUNTTYPE(void)
	{
		dHour	= 0;
		bOK		= false;
	}
};

// Konto eines Mitarbeiters. Die Struktur beinhaltet alle Infos
// �ber die einzelnen Arbeitszeitkonten eines bestimmten Mitarbeiters.
// Wenn z.B. die Konten f�r Urlaub und Arbeitszeit vom 1.1.99 bis 1.2.99
// ermittelt werden sollen, gibt es f�r jeden Mitarbeiter eine Struktur 
// vom Typ RUNNINGACCOUNT, die wiederum zwei Strukturen vom Typ
// RUNNINGACCOUNTTYPE in <oAccounts> h�lt (eine f�r Urlaub, eine f�r
// Arbeitszeit).
struct RUNNINGACCOUNT
{
	long lStfUrno;								//Mitarbeiter-Urno
	CMapPtrToPtr oAccountsMap;					//Map to CCSPtrArray<RUNNINGACCOUNTTYPE> oAccountElements
	CCSPtrArray<RUNNINGACCOUNTTYPE> oAccounts;	//CCSPtrArray><RUNNINGACCOUNTTYPE> mit den einzelnen Konten

	RUNNINGACCOUNT(void)
	{
		lStfUrno	= 0;
	}
};

// Info-Objekt f�r Funktionen, die spezielle Werte abfragen
// sollen (z.B. ob ein Konto g�ltig ist). Die Datentypen k�nnen
// je nach Bedarf benutzt werden.
struct ACCOUNTRETURNSTRUCT
{
	CString oValueDefinition; // Beschreibt die �bergebenen Werte in ACCOUNTRETURNSTRUCT
	int		iInt;
	long	lLong;
	double	dDouble;
	bool	bBool;
	CString	oString;
	CString	oFormat;		// Wie soll das Konto Formatiert werden (z.B. "%01.2f ")

	ACCOUNTRETURNSTRUCT(void)
	{
		oValueDefinition = "UNDEF";
		iInt	= 0;
		lLong	= 0;
		dDouble	= 0;
		bBool	= false;
	}
};

//---------------------------------------------------------------------------

class CAccounts  
{
public:
	// Konstruktor / Destruktor
	CAccounts(CedaDrrData *popDrrData, CedaAccData *popAccData, CedaEspData *popEspData,
			  CedaStfData *popStfData, CedaBsdData *popBsdData, CedaOdaData *popOdaData, 
			  CedaScoData *popScoData, CedaCotData *popCotData);
	virtual ~CAccounts();

// Funktionen
	// Mitarbeiterkonten aller Mitarbeiter in <popStaffUrnos> ermitteln
//	void FillRunningAccounts(CUIntArray *popAccountTypes, CStringArray *popStaffUrnos, 
//							 COleDateTime opAccountDateFrom, COleDateTime opAccountDateTo);
	// Mitarbeiterkonten des Mitarbeiters <lpStaffUrno> ermitteln
//	void FillRunningAccountsByStaff(CUIntArray *popAccountTypes, long lpStaffUrno, COleDateTime opAccountDateFrom, 
//									COleDateTime opAccountDateTo, ESPDATA *prpEsp = NULL);
	
	// Mitarbeiterkonto eines bestimmten Mitarbeiters l�schen
	void DeleteRunningAccountsByStaff(long lpStaffUrno);
	// alle Mitarbeiterkonten aus dem Kontenarray l�schen
	void DeleteRunningAccounts();

	// Info �ber ein Konto
	ACCOUNTRETURNSTRUCT GetRunningAccountByStaff(UINT ipAccountType, long pStaffUrno);
	// Zahlenformattag eines Kontotyps ermitteln
	CString GetRunningAccountFormat(UINT ipAccountType);
	// sprachabh�ngiger Bezeichner eines Kontotyps ermitteln
	CString GetRunningAccountHeader(UINT ipAccountType, bool bpGetShortName = true);
	// Index eines Mitarbeiterkontos ermitteln (= Zeile in View)
	int GetRunningfAccountPosByStaffUrno(long lpStaffUrno);
	// Anzahl der Konten pro Mitarbeiter (= Spalten in View) ermitteln
	int GetNoOfRunningfAccountTypes(void);

	// Array mit Warnungen und Meldungen kopieren
	bool GetAttentionText(CStringArray *popAttentionTextArray);
	// Array mit Warnungen und Meldungen leeren
	void DeleteAttentionText();
	// Abschlu�kontowert speichern
	bool SetCloseAccountByStaff(ACCDATA *prpAcc, double dpCloseMin, int ipMonth,bool bpSetOpen = true);
	// ermittelt die aktuellen Kontodaten vom Typ Arbeitszeitkonto f�r den Mitarbeiter <lpStaffUrno>
	ACCDATA* GetAccDataByStaff(long lpStaffUrno, CString opYearFrom, int ipMonthFrom, 
							   double &dpOpenMin, CString opType , bool bpUseCorrection = true);
	// t�gliche Gesamtarbeitszeit ermitteln
	bool GetDailyTotalAZKMinutesByStaff(long lpStaffUrno, COleDateTime olActualDayOle, 
									 double &pDrrMin,double &dpBreakMin, bool bpReturnOnError = false);

	// Berechnung der Freien-Tage
	bool GetDailyTotalVACMinutesByStaff(int ipOffDayCount,int ipCourseDayCount,long lpStaffUrno, COleDateTime opDay, double &dpResult);

	// Ermittelt Werte f�r alle Konten
	ACCOUNTRETURNSTRUCT GetAccountByStaffAndMonth(UINT ipAccountType, long pStaffUrno,COleDateTime opDate);

	// Ermittelt Werte f�r statische Konten
	ACCOUNTRETURNSTRUCT GetStaticAccountByStaffAndMonth(UINT ipAccountType, long pStaffUrno,COleDateTime opDate, int ipUsmoValue,CString opFieldToUse = "K");
	// Ermittelt Werte f�r tempor�re Konten
	ACCOUNTRETURNSTRUCT GetTempAccountByStaffAndMonth(UINT ipAccountType, long pStaffUrno,COleDateTime opDate);
	// Pr�ft das Konto freie Tage
	//void Check_VAC_ByStaffAndMonth(int ipValue,long lpStaffUrno,COleDateTime opDay);
	// Anzahl der Offtage und Course Tage am Tag
	void CountOffAndCourseDays(CString olSday,long lpStaffUrno,int& ilTempOff,int& ilTempCourse);

	// Ermittlung des Schlu�kontostandes
	bool GetCloseAccountByStaff(long lpStaffUrno, CString opYearFrom, int ipMonthFrom, 
							   double &dpClose, CString opType );
	// Ermittlung des Er�ffnungskontostandes
	bool GetOpenAccountByStaff(long lpStaffUrno, CString opYearFrom, int ipMonthFrom, 
							   double &dpOpen, CString opType);
protected:
// Funktionen
	// Konto vom Typ <ipAccountType> des Mitarbeiters <lpStaffUrno> abfragen / erzeugen
	RUNNINGACCOUNTTYPE* GetRunningAccountByTypeAndStaff(long lpStaffUrno, UINT ipAccountType,long lpDays);
	// Arbeitszeitkonto des Mitarbeiters <lpStaffUrno> ermitteln
//	void FillRunningAccount_AZK_ByStaff(long lpStaffUrno, COleDateTime opAccountDateFrom, 
//										COleDateTime opAccountDateTo, ESPDATA *prpEsp = NULL);
	// Freizeitkonto des Mitarbeiters <lpStaffUrno> ermitteln
	//void FillRunningAccount_VAC_ByStaff(long lpStaffUrno, COleDateTime opAccountDateFrom, 
	//										   COleDateTime opAccountDateTo);

	//uhi 19.01.01
	// Ermittlung des Er�ffnungskontostandes
	//bool GetOpenAccountByStaff(long lpStaffUrno, CString opYearFrom, int ipMonthFrom, 
	//						   double &dpOpen, CString opType);
	// t�gliche, vertragliche Sollarbeitszeit in Min. ermitteln
//	bool GetDailySCOMinutesByStaff(long lpStaffUrno, int lpDay, COleDateTime opAccountDateFrom,
//								   COleDateTime opAccountDateTo, double &dpMinutesPerDay);

	// t�gliche Arbeitszeit laut Dienstplan in Min. ermitteln
	bool GetDailyDRRMinutesByStaff(DRRDATA* popDrr, double &dpDrrMin);

	// relevante Pausenzeit berechnen (GAV - Arbeitsvertr�ge)
	bool GetDailyGAVMinutesByStaff(DRRDATA* popDrr, double &dpDrrMin);

	// Kernroutine zur Berechnung der Ferien-Tage
//	int UseVacAlg(	int ipAnzahltouren,double dpTourlaenge1,double dpTourlaenge2, 
//						long lpStfUrno,COleDateTime opDate,double dpVereinbarteWochenstunden,
//						double dpKursTourLaenge,int ipOffDayCount, int ipCourseDayCount);

	
	// Wendet einen Operator an.
	double UseOperator(double dlResult,CString olOperatorToUse,double dlValueToUse);

	// IsHolOrAbsencensCode: Pr�ft ob Urlaub oder ganzt�gige Abwesenheit vorliegt
	bool IsHolOrAbsencenCode(DRRDATA* popDrr);
	// IsIll: Pr�ft ob Krankheit vorliegt
	bool IsIllCode(DRRDATA* popDrr);
	// Ist eine Schicht eingetragen worden
	bool IsShiftCode(DRRDATA* popDrr);
	// Was h�tte er gehabt wenn er nicht krank gewesen w�re
	DRRDATA* HasShiftInfo(DRRDATA* prlDrr);
	// Wochenarbeitszeit / 5
	double  GetCommonDayWorkingTime(DRRDATA* prlDrr);
	// Hilsfunktion zum Runden
	int Round(double dpValue);
	
// Daten
	// Array mit den einzelnen Arbeitskonten der Mitarbeiter
	CCSPtrArray<RUNNINGACCOUNT> omRunningAccount;
    // Map der Mitarbeiterkonten mit der Mitarbeiter-URNO als Schl�ssel
	CMapPtrToPtr omRunningAccountMap;
	// Array mit allen Meldungen und Warnungen, die bei der Ermittlung der
	// Konten anfallen
	CStringArray omAttentionTextArray;
	// Datenhaltungsobjekte
	// Tagesschichtdaten
	CedaDrrData *pomDrrData;
	// Kontodaten
	CedaAccData *pomAccData;
	// ESP-Daten
	CedaEspData *pomEspData;
	// Mitarbeiterdaten
	CedaStfData *pomStfData;
	// Basisschichtdaten
	CedaBsdData *pomBsdData;
	// Abwesenheitendaten
	CedaOdaData *pomOdaData;
	// Vertragszuordnungsdaten
	CedaScoData *pomScoData;
	// Vertragsdaten
	CedaCotData *pomCotData;

/////////////////////////////////////////////////////////////////////////////
// Account List Funktionen und Daten

// Daten
public:
	// In omAccountTypeInfoList werden die einzelnen Account Typen gesammelt
    CCSPtrArray<ACCOUNTTYPEINFO> omAccountTypeInfoList;
	CMapPtrToPtr omAccountTypeInfoPtrMap;

// Funktionen
public:
	// f�llen der Account-Typen-Liste
	// r�ck: Anzahl der Elemente in omAccountTypeInfoList
	int FillAccountTypeList();
	// liefert den Ptr. auf den Datensatz mit dem Feldwert iInternNumber = <ipInternNumber> zur�ck
	ACCOUNTTYPEINFO* GetAccountTypeInfoByAccountType(UINT ipInternNumber);
	// liefert den Ptr. auf den Datensatz mit dem Feldwert oExternKey = <opExternKey> zur�ck
	ACCOUNTTYPEINFO* GetAccountTypeInfoByExternKey(CString opExternKey);
};

#endif // !defined(AFX_ACCOUNTS_H__12926483_764A_11D3_8F0E_00001C034EA0__INCLUDED_)
