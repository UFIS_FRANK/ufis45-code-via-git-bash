// CedaUmuData.cpp - Klasse f�r die Handhabung von Umu-Daten (Budgetberechnung) 
//
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaUmuData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessUmuCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_UMU_CHANGE, BC_UMU_NEW und BC_UMU_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaUmuData::ProcessUmuBc() der entsprechenden 
//	Instanz von CedaUmuData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessUmuCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaUmuData *polUmuDATA = (CedaUmuData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polUmuDATA->ProcessUmuBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaUmuData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaUmuData::CedaUmuData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Umu-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(UMUDATA, UmuDataRecInfo)
		FIELD_LONG		(Umut,"UMUT")	// Urno MUTTAB
		FIELD_CHAR_TRIM	(Netw,"NETW")	// Wochenstunden - Zus�tzliche Ferien
		FIELD_CHAR_TRIM	(Lanm,"LANM")	// Nachname
		FIELD_CHAR_TRIM	(Finm,"FINM")	// Vorname
		FIELD_CHAR_TRIM	(Doem,"DOEM")	// Entrance
		FIELD_CHAR_TRIM	(Dodm,"DODM")	// Leaving
		FIELD_CHAR_TRIM	(Mufr,"MUFR")	// Bisheriger Wert
		FIELD_CHAR_TRIM	(Sufr,"SUFR")	// Bisheriger Wert Zus�tzliche Ferien
		FIELD_CHAR_TRIM	(Muto,"MUTO")	// Neuer Wert
		FIELD_CHAR_TRIM	(Suto,"SUTO")	// Neuer Wert Zus�tzliche Ferien 
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Hopo
		FIELD_LONG   	(Urno,"URNO")	// Urno
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(UmuDataRecInfo)/sizeof(UmuDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&UmuDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Umu setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"UMUT,NETW,LANM,FINM,DOEM,DODM,MUFR,SUFR,MUTO,SUTO,HOPO,URNO");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Umus initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaUmuData::~CedaUmuData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaUmuData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaUmuData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Umu-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_UMU_CHANGE, CString("CedaUmuData"), CString("BC_UMU_CHANGE"),        ProcessUmuCf);
	ogDdx.Register((void *)this,BC_UMU_NEW, CString("CedaUmuData"), CString("BC_UMU_NEW"),		   ProcessUmuCf);
	ogDdx.Register((void *)this,BC_UMU_DELETE, CString("CedaUmuData"), CString("BC_UMU_DELETE"),        ProcessUmuCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaUmuData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadUmuByUrno: liest den Umu-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaUmuData::ReadUmuByUrno(long lpUrno, UMUDATA *prpUmu)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	UMUDATA *prlUmu = new UMUDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpUmu) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlUmu;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaUmuData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		UMUDATA *prlUmu = new UMUDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlUmu);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlUmu->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlUmu, UMU_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlUmu;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlUmu;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen Umu
	TRACE("Read-Umu: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpUmu> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUmuData::Insert(UMUDATA *prpUmu, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpUmu->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpUmu) == false) return false;
	// Broadcast UMU_NEW abschicken
	InsertInternal(prpUmu,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUmuData::InsertInternal(UMUDATA *prpUmu, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpUmu);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpUmu->Urno,prpUmu);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,UMU_NEW,(void *)prpUmu);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpUmu->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaUmuData::Update(UMUDATA *prpUmu,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetUmuByUrno(prpUmu->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpUmu->IsChanged == DATA_UNCHANGED)
		{
			prpUmu->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpUmu) == false) return false; 
		// Broadcast Bud_CHANGE versenden
		UpdateInternal(prpUmu);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaUmuData::UpdateInternal(UMUDATA *prpUmu, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// Umu in lokaler Datenhaltung suchen
	UMUDATA *prlUmu = GetUmuByKey(prpUmu->Sday,prpUmu->Stfu,prpUmu->Budn,prpUmu->Rosl);
	// Umu gefunden?
	if (prlUmu != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlUmu = *prpUmu;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",UMU_CHANGE);
		ogDdx.DataChanged((void *)this,UMU_CHANGE,(void *)prpUmu);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaUmuData::DeleteInternal(UMUDATA *prpUmu, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,UMU_DELETE,(void *)prpUmu);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpUmu->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpUmu->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpUmu> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaUmuData::Delete(UMUDATA *prpUmu, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpUmu->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpUmu)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpUmu,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessUmuBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaUmuData::ProcessUmuBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaUmuData::ProcessUmuBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	UMUDATA *prlUmu = NULL;

	switch(ipDDXType)
	{
	case BC_UMU_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlUmu = new UMUDATA;
			GetRecordFromItemList(prlUmu,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlUmu, UMU_SEND_DDX);
		}
		break;
	case BC_UMU_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlUmu = GetUmuByUrno(llUrno);
			if(prlUmu != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlUmu,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlUmu);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_UMU_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlUmu = GetUmuByUrno(llUrno);
			if (prlUmu != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlUmu);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaUmuData::ProcessUmuBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetUmuByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

UMUDATA *CedaUmuData::GetUmuByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	UMUDATA *prpUmu;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpUmu) == TRUE)
	{
		return prpUmu;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpUmu>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaUmuData::Save(UMUDATA *prpUmu)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpUmu->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpUmu->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpUmu);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpUmu->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpUmu->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpUmu);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpUmu->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpUmu->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaUmuData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaUmuData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyUmuValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaUmuData::CopyUmu(UMUDATA* popUmuDataSource,UMUDATA* popUmuDataTarget)
{
	// Daten kopieren
	/*strcpy(popUmuDataTarget->Rema,popUmuDataSource->Rema);
	strcpy(popUmuDataTarget->Sdac,popUmuDataSource->Sdac);
	popUmuDataTarget->Abfr = popUmuDataSource->Abfr;
	popUmuDataTarget->Abto = popUmuDataSource->Abto;*/
}

bool CedaUmuData::ReadUmuData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		UMUDATA *prlUmu = new UMUDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlUmu);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlUmu, UMU_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlUmu;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen Umu
	TRACE("Read-Umu: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

