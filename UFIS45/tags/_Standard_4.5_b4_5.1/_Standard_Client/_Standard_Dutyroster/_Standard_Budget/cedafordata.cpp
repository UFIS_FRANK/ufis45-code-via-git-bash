// CedaForData.cpp - Klasse f�r die Handhabung von For-Daten (Budgetberechnung) 
//
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaForData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessForCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_FOR_CHANGE, BC_FOR_NEW und BC_FOR_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaForData::ProcessForBc() der entsprechenden 
//	Instanz von CedaForData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessForCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaForData *polForDATA = (CedaForData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polForDATA->ProcessForBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaForData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaForData::CedaForData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r For-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(FORDATA, ForDataRecInfo)
		FIELD_LONG		(Ubud,"UBUD")	// Urno BUDTAB
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")	// Organisationseinheit
		FIELD_CHAR_TRIM	(Yemo,"YEMO")	// Jahr/Monat
		FIELD_CHAR_TRIM	(Actu,"ACTU")	// Umrechnung auf Personalmonate
		FIELD_CHAR_TRIM	(Budg,"BUDG")	// Wert aus Stammdaten Budget
		FIELD_CHAR_TRIM	(Diff,"DIFF")	// Differenz BUDG - ACTU
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Hopo
		FIELD_OLEDATE	(Cdat,"CDAT")	// Erstellungsdatum
		FIELD_LONG   	(Urno,"URNO")	// Urno
		FIELD_CHAR_TRIM	(Usec,"USEC")	// Ersteller
		FIELD_CHAR_TRIM	(Useu,"USEU")	// Anwender letzte �nderung
		FIELD_OLEDATE	(Lstu,"LSTU")	// Datum letzte �nderung
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(ForDataRecInfo)/sizeof(ForDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ForDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf For setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"UBUD,DPT1,YEMO,ACTU,BUDG,DIFF,HOPO,CDAT,URNO,USEC,USEU,LSTU");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Fors initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaForData::~CedaForData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaForData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaForData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// For-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_FOR_CHANGE, CString("CedaForData"), CString("BC_FOR_CHANGE"),        ProcessForCf);
	ogDdx.Register((void *)this,BC_FOR_NEW, CString("CedaForData"), CString("BC_FOR_NEW"),		   ProcessForCf);
	ogDdx.Register((void *)this,BC_FOR_DELETE, CString("CedaForData"), CString("BC_FOR_DELETE"),        ProcessForCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaForData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadForByUrno: liest den For-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaForData::ReadForByUrno(long lpUrno, FORDATA *prpFor)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	FORDATA *prlFor = new FORDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpFor) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlFor;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaForData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		FORDATA *prlFor = new FORDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlFor);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlFor->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlFor, FOR_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlFor;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlFor;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen For
	TRACE("Read-For: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpFor> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaForData::Insert(FORDATA *prpFor, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpFor->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpFor) == false) return false;
	// Broadcast FOR_NEW abschicken
	InsertInternal(prpFor,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaForData::InsertInternal(FORDATA *prpFor, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpFor);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpFor->Urno,prpFor);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,FOR_NEW,(void *)prpFor);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpFor->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaForData::Update(FORDATA *prpFor,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetForByUrno(prpFor->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpFor->IsChanged == DATA_UNCHANGED)
		{
			prpFor->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpFor) == false) return false; 
		// Broadcast Bud_CHANGE versenden
		UpdateInternal(prpFor);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaForData::UpdateInternal(FORDATA *prpFor, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// For in lokaler Datenhaltung suchen
	FORDATA *prlFor = GetForByKey(prpFor->Sday,prpFor->Stfu,prpFor->Budn,prpFor->Rosl);
	// For gefunden?
	if (prlFor != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlFor = *prpFor;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",FOR_CHANGE);
		ogDdx.DataChanged((void *)this,FOR_CHANGE,(void *)prpFor);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaForData::DeleteInternal(FORDATA *prpFor, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,FOR_DELETE,(void *)prpFor);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpFor->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpFor->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpFor> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaForData::Delete(FORDATA *prpFor, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpFor->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpFor)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpFor,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessForBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaForData::ProcessForBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaForData::ProcessForBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	FORDATA *prlFor = NULL;

	switch(ipDDXType)
	{
	case BC_FOR_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlFor = new FORDATA;
			GetRecordFromItemList(prlFor,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlFor, FOR_SEND_DDX);
		}
		break;
	case BC_FOR_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlFor = GetForByUrno(llUrno);
			if(prlFor != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlFor,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlFor);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_FOR_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlFor = GetForByUrno(llUrno);
			if (prlFor != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlFor);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaForData::ProcessForBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetForByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

FORDATA *CedaForData::GetForByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	FORDATA *prpFor;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpFor) == TRUE)
	{
		return prpFor;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpFor>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaForData::Save(FORDATA *prpFor)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpFor->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpFor->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpFor);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpFor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpFor->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpFor);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpFor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpFor->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaForData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaForData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyForValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaForData::CopyFor(FORDATA* popForDataSource,FORDATA* popForDataTarget)
{
	// Daten kopieren
	/*strcpy(popForDataTarget->Rema,popForDataSource->Rema);
	strcpy(popForDataTarget->Sdac,popForDataSource->Sdac);
	popForDataTarget->Abfr = popForDataSource->Abfr;
	popForDataTarget->Abto = popForDataSource->Abto;*/
}

bool CedaForData::ReadForData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		FORDATA *prlFor = new FORDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlFor);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlFor, FOR_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlFor;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen For
	TRACE("Read-For: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

