#ifndef _CedaForData_H_
#define _CedaForData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define FOR_SEND_DDX	(true)
#define FOR_NO_SEND_DDX	(false)

// Felddimensionen
#define FOR_DPT1_LEN	(8)
#define FOR_YEMO_LEN	(6)
#define FOR_ACTU_LEN	(7)
#define FOR_HOPO_LEN	(3)
#define FOR_USEC_LEN	(32)

// Struktur eines For-Datensatzes
struct FORDATA {
	long			Ubud;					//Urno BUDTAB
	char			Dpt1[FOR_DPT1_LEN+2];	//Organisationseinheit
	char			Yemo[FOR_YEMO_LEN+2];	//Jahr/Monat
	char			Budg[FOR_ACTU_LEN+2];	//Wert aus Stammdaten Budget
	char			Actu[FOR_ACTU_LEN+2];	//Umrechnung auf Personalmonate
	char			Diff[FOR_ACTU_LEN+2];	//BUDG - ACTU
	char			Hopo[FOR_HOPO_LEN+2];	//Hopo
	COleDateTime	Cdat;					//Erstellungsdatum
	long			Urno;					//Urno
	char			Usec[FOR_USEC_LEN+2];	//Ersteller
	char			Useu[FOR_USEC_LEN+2];	//Anwender letzte �nderung
	COleDateTime	Lstu;					//Datum letzte �nderung

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	FORDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaForData class
void ProcessForCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaForData: kapselt den Zugriff auf die Tabelle For (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaForData: public CCSCedaData
{
// Funktionen
public:
	bool ReadForData();
    // Konstruktor/Destruktor
	CedaForData(CString opTableName = "FOR", CString opExtName = "TAB");
	~CedaForData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<FORDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_FOR_CHANGE,BC_FOR_DELETE und BC_FOR_NEW
	void ProcessForBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Lesen/Schreiben von Datens�tzen
	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadForByUrno(long lpUrno, FORDATA *prpFor);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(FORDATA *prpFor, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(FORDATA *prpFor, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(FORDATA *prpFor, bool bpSave = true);

	// Datens�tze suchen
	// For nach Urno suchen
	FORDATA* GetForByUrno(long lpUrno);
	
	// Manipulation von Datens�tzen
	// kopiert die Feldwerte eines For-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyFor(FORDATA* popForDataSource,FORDATA* popForDataTarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(FORDATA *prpFor);
	// einen Broadcast FOR_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(FORDATA *prpFor, bool bpSendDdx);
	// einen Broadcast FOR_CHANGE versenden
	bool UpdateInternal(FORDATA *prpFor, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(FORDATA *prpFor, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Fors
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaForData_H_
