#if !defined(AFX_BUDGETDATADLG_H__CF256FE1_2BC1_11D4_8FD0_0050DADD7302__INCLUDED_)
#define AFX_BUDGETDATADLG_H__CF256FE1_2BC1_11D4_8FD0_0050DADD7302__INCLUDED_

#include <gridcontrol.h>
#include <CCSEdit.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BudgetDataDlg.h : header file
//

#define MINROWS1 1
/////////////////////////////////////////////////////////////////////////////
// CBudgetDataDlg dialog

class CBudgetDataDlg : public CDialog
{
// Construction
public:
	bool m_new;
	CBudgetDataDlg(long urno, CWnd* pParent = NULL);   // standard constructor
	CBudgetDataDlg(CWnd* pParent = NULL);	
	~CBudgetDataDlg();

// Dialog Data
	//{{AFX_DATA(CBudgetDataDlg)
	enum { IDD = IDD_BUDGETDATA_DLG };
	CButton	m_mutation;
	CButton	m_forecast;
	CButton	m_budget;
	CButton	m_headcount;
	CCSEdit	m_tomonth;
	CCSEdit	m_remark;
	CCSEdit	m_frommonth;
	CCSEdit	m_day;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBudgetDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_changedbyuser;
	CString m_changedonhour;
	CString m_changedonday;
	CString m_createdbyuser;
	CString m_createdonhour;
	CString m_createdonday;
	CGridControl *pomData;
	void IniGrid();
	HICON m_hIcon;
	long m_urno;


	// Generated message map functions
	//{{AFX_MSG(CBudgetDataDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	virtual void OnOK();
	afx_msg void OnInfo();
	afx_msg void OnOrgDelete();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BUDGETDATADLG_H__CF256FE1_2BC1_11D4_8FD0_0050DADD7302__INCLUDED_)
