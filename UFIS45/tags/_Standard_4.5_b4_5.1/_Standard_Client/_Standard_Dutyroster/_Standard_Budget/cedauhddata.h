#ifndef _CedaUhdData_H_
#define _CedaUhdData_H_
 
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>
//#include "CedaDrrData.h" // f�r G�ltigkeitspr�fungen

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define UHD_SEND_DDX	(true)
#define UHD_NO_SEND_DDX	(false)

// Felddimensionen
#define UHD_WHPW_LEN	(4)
#define UHD_SUPH_LEN	(1)
#define UHD_STSC_LEN	(2)
#define UHD_HEAD_LEN	(5)
#define UHD_ACTU_LEN	(7)
#define UHD_HOPO_LEN	(3)

// Struktur eines Uhd-Datensatzes
struct UHDDATA {
	long			Uhdc;					//Urno HDCTAB
	char			Whpw[UHD_WHPW_LEN+2];	//Wochenstunden
	char			Suph[UHD_SUPH_LEN+2];	//Indikator Zus�tzliche Ferien
	char			Netw[UHD_WHPW_LEN+2];	//WHPW - SUPH
	char			Stsc[UHD_STSC_LEN+2];	//Code Reduzierte Arbeitszeit
	char			Head[UHD_HEAD_LEN+2];	//Anzahl Vertr�ge
	char			Actu[UHD_ACTU_LEN+2];	//Umrechnung auf Personalmonate
	char			Budg[UHD_ACTU_LEN+2];	//Wert aus Stammdaten Budget
	char			Diff[UHD_ACTU_LEN+2];	//BUDG - ACTU
	char			Hopo[UHD_HOPO_LEN+2];	//Hopo
	long			Urno;					//Urno;

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	UHDDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		//Abfr.SetStatus(COleDateTime::invalid);		// Abweichung von
		//Abto.SetStatus(COleDateTime::invalid);		// Abweichung bis
	}
};	

// the broadcast CallBack function, has to be outside the CedaUhdData class
void ProcessUhdCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//************************************************************************************************************************
//************************************************************************************************************************
// Deklaration CedaUhdData: kapselt den Zugriff auf die Tabelle Uhd (Daily Roster
//	Absences - untert�gige Abwesenheit)
//************************************************************************************************************************
//************************************************************************************************************************

class CedaUhdData: public CCSCedaData
{
// Funktionen
public:
	bool ReadUhdData();
    // Konstruktor/Destruktor
	CedaUhdData(CString opTableName = "UHD", CString opExtName = "TAB");
	~CedaUhdData();

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

	// die geladenen Datens�tze
	CCSPtrArray<UHDDATA> omData;

	// allgemeine Funktionen
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_UHD_CHANGE,BC_UHD_DELETE und BC_UHD_NEW
	void ProcessUhdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	// Lesen/Schreiben von Datens�tzen
	// Datens�tze einlesen
	bool Read(char *pspWhere = NULL, CMapPtrToPtr *popLoadStfUrnoMap = NULL,
			  bool bpRegisterBC = true, bool bpClearData = true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadUhdByUrno(long lpUrno, UHDDATA *prpUhd);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(UHDDATA *prpUhd, bool bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(UHDDATA *prpUhd, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(UHDDATA *prpUhd, bool bpSave = true);

	// Datens�tze suchen
	// Uhd nach Urno suchen
	UHDDATA* GetUhdByUrno(long lpUrno);
	
	// Manipulation von Datens�tzen
	// kopiert die Feldwerte eines Uhd-Datensatzes ohne die Schl�sselfelder (Urno, etc.)
	void CopyUhd(UHDDATA* popUhdDataSource,UHDDATA* popUhdDataTarget);

protected:	
	// Funktionen
	// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister = true);

	// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

	// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
	
	// Datens�tze bearbeiten/speichern
	// speichert einen einzelnen Datensatz
	bool Save(UHDDATA *prpUhd);
	// einen Broadcast UHD_NEW abschicken und den Datensatz in die interne Datenhaltung aufnehmen
	bool InsertInternal(UHDDATA *prpUhd, bool bpSendDdx);
	// einen Broadcast UHD_CHANGE versenden
	bool UpdateInternal(UHDDATA *prpUhd, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(UHDDATA *prpUhd, bool bpSendDdx = true);

	// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;

	// ToDo: Minimale und maximales G�ltigkeitsdatum einbauen, damit nicht
	// unn�tig viele Datens�tze intern gehalten werden. Die min.-max.-Daten
	// m�ssen bei jeder Leseaktion (ReadXXX) gepr�ft und wenn n�tig erweitert
	// werden. Wenn dann BCs einlaufen (REL,NEW), kann gepr�ft werden ob
	// die Datens�tze in den BCs �berhaupt relevant sind.

	// min. und max. Tag der Ansichts-relevanten Uhds
	//	COleDateTime omMinDay;
	//	COleDateTime omMaxDay;
};

#endif	// _CedaUhdData_H_
