// CedaHdcData.cpp - Klasse f�r die Handhabung von Hdc-Daten (HDCgetberechnung) 
//
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaHdcData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessHdcCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_HDC_CHANGE, BC_HDC_NEW und BC_HDC_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaHdcData::ProcessHdcBc() der entsprechenden 
//	Instanz von CedaHdcData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessHdcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Empf�nger ermitteln
	CedaHdcData *polHdcData = (CedaHdcData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polHdcData->ProcessHdcBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaHdcData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaHdcData::CedaHdcData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Hdc-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(HDCDATA, HdcDataRecInfo)
		FIELD_LONG		(Ubud,"UBUD")	// Urno BUDTAB
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")	// Organisationseinheit
		FIELD_CHAR_TRIM (Ymdy,"YMDY")	// Jahr/Monat/Stichtag
		//FIELD_CHAR_TRIM	(Rema,"REMA")	// Remarks
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Hopo
		FIELD_OLEDATE	(Cdat,"CDAT")	// Erstellungsdatum
		FIELD_LONG   	(Urno,"URNO")	// Urno
		FIELD_CHAR_TRIM	(Usec,"USEC")	// Ersteller
		FIELD_CHAR_TRIM	(Useu,"USEU")	// Anwender letzte �nderung
		FIELD_OLEDATE	(Lstu,"LSTU")	// Datum letzte �nderung
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(HdcDataRecInfo)/sizeof(HdcDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&HdcDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Hdc setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"UBUD,DPT1,YMDY,HOPO,CDAT,URNO,USEC,USEU,LSTU");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Hdcs initialisieren
	//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaHdcData::~CedaHdcData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaHdcData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaHdcData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Hdc-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_HDC_CHANGE, CString("CedaHdcData"), CString("BC_HDC_CHANGE"),        ProcessHdcCf);
	ogDdx.Register((void *)this,BC_HDC_NEW, CString("CedaHdcData"), CString("BC_HDC_NEW"),		   ProcessHdcCf);
	ogDdx.Register((void *)this,BC_HDC_DELETE, CString("CedaHdcData"), CString("BC_HDC_DELETE"),        ProcessHdcCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaHdcData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadHdcByUrno: liest den Hdc-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaHdcData::ReadHdcByUrno(long lpUrno, HDCDATA *prpHdc)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	HDCDATA *prlHdc = new HDCDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpHdc) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlHdc;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaHdcData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		HDCDATA *prlHdc = new HDCDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlHdc);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlHdc->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlHdc, HDC_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlHdc;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlHdc;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen Hdc
	TRACE("Read-Hdc: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpHdc> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaHdcData::Insert(HDCDATA *prpHdc, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpHdc->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpHdc) == false) return false;
	// Broadcast HDC_NEW abschicken
	InsertInternal(prpHdc,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaHdcData::InsertInternal(HDCDATA *prpHdc, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpHdc);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpHdc->Urno,prpHdc);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,HDC_NEW,(void *)prpHdc);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpHdc->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaHdcData::Update(HDCDATA *prpHdc,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetHdcByUrno(prpHdc->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpHdc->IsChanged == DATA_UNCHANGED)
		{
			prpHdc->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpHdc) == false) return false; 
		// Broadcast HDC_CHANGE versenden
		UpdateInternal(prpHdc);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaHdcData::UpdateInternal(HDCDATA *prpHdc, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// Hdc in lokaler Datenhaltung suchen
	HDCDATA *prlHdc = GetHdcByKey(prpHdc->Sday,prpHdc->Stfu,prpHdc->Budn,prpHdc->Rosl);
	// HDC gefunden?
	if (prlHdc != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlHdc = *prpHDC;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",HDC_CHANGE);
		ogDdx.DataChanged((void *)this,HDC_CHANGE,(void *)prpHdc);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaHdcData::DeleteInternal(HDCDATA *prpHdc, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,HDC_DELETE,(void *)prpHdc);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpHdc->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpHdc->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpHdc> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaHdcData::Delete(HDCDATA *prpHdc, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpHdc->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpHdc)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpHdc,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessHdcBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaHdcData::ProcessHdcBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaHdcData::ProcessHdcBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	HDCDATA *prlHdc = NULL;

	switch(ipDDXType)
	{
	case BC_HDC_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlHdc = new HDCDATA;
			GetRecordFromItemList(prlHdc,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlHdc, HDC_SEND_DDX);
		}
		break;
	case BC_HDC_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlHdc = GetHdcByUrno(llUrno);
			if(prlHdc != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlHdc,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlHdc);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_HDC_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlHdc = GetHdcByUrno(llUrno);
			if (prlHdc != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlHdc);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaHdcData::ProcessHdcBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetHdcByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

HDCDATA *CedaHdcData::GetHdcByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	HDCDATA *prpHdc;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpHdc) == TRUE)
	{
		return prpHdc;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpHdc>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaHdcData::Save(HDCDATA *prpHdc)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpHdc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpHdc->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpHdc);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpHdc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpHdc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpHdc);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpHdc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpHdc->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaHdcData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaHdcData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}


//************************************************************************************************************************************************
// CopyHdcValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaHdcData::CopyHdc(HDCDATA* popHdcDataSource,HDCDATA* popHdcDataTarget)
{
	// Daten kopieren
	/*strcpy(popHdcDATATarget->Rema,popHdcDATASource->Rema);
	strcpy(popHdcDATATarget->Sdac,popHdcDATASource->Sdac);
	popHdcDATATarget->Abfr = popHdcDATASource->Abfr;
	popHdcDATATarget->Abto = popHdcDATASource->Abto;*/
}

bool CedaHdcData::ReadHdcData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		HDCDATA *prlHdc = new HDCDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlHdc);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlHdc, HDC_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlHdc;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen Hdc
	TRACE("Read-Hdc: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

