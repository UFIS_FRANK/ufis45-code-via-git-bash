VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.MDIForm frmMain 
   BackColor       =   &H8000000C&
   Caption         =   "AbsencePlanning"
   ClientHeight    =   3420
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   9375
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "ImageList1"
      DisabledImageList=   "ImageList1"
      HotImageList    =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "New"
            Object.ToolTipText     =   "1035"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "Open"
            Object.ToolTipText     =   "1036"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "Save"
            Object.ToolTipText     =   "1037"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Print"
            Object.ToolTipText     =   "1038"
            ImageKey        =   "Print"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ViewEditor"
            Object.ToolTipText     =   "1047"
            ImageKey        =   "DefiningViews"
         EndProperty
      EndProperty
      Begin TABLib.TAB PrintTab 
         Height          =   375
         Left            =   5160
         TabIndex        =   5
         Top             =   0
         Visible         =   0   'False
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   661
         _StockProps     =   64
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   255
         Left            =   4485
         TabIndex        =   4
         Top             =   60
         Visible         =   0   'False
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         _Version        =   393216
         BuddyControl    =   "txtMaxAbsWeek"
         BuddyDispid     =   196609
         OrigLeft        =   5040
         OrigTop         =   50
         OrigRight       =   5280
         OrigBottom      =   305
         Max             =   1000
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.TextBox txtMaxAbsWeek 
         Height          =   315
         Left            =   3960
         TabIndex        =   3
         Top             =   35
         Visible         =   0   'False
         Width           =   800
      End
      Begin VB.ComboBox cmbViews 
         Height          =   315
         Left            =   1440
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   35
         Width           =   2295
      End
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   3150
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6615
            Text            =   "Ready."
            TextSave        =   "Ready."
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   4234
            MinWidth        =   4234
            Text            =   "BC"
            TextSave        =   "BC"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "16.02.2006"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            TextSave        =   "14:10"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   1200
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Orientation     =   2
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   360
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":030A
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":041C
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":052E
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0640
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0752
            Key             =   "Cut"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0864
            Key             =   "Copy"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0976
            Key             =   "Paste"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0A88
            Key             =   "Bold"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0B9A
            Key             =   "Italic"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0CAC
            Key             =   "Underline"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0DBE
            Key             =   "Align Left"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0ED0
            Key             =   "Center"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0FE2
            Key             =   "Align Right"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":10F4
            Key             =   "DefiningViews"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "1000"
      Begin VB.Menu mnuFileNew 
         Caption         =   "1001"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "1002"
         Shortcut        =   ^O
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileClose 
         Caption         =   "1003"
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "1004"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileSaveAs 
         Caption         =   "1005"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileSaveAll 
         Caption         =   "1006"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "1007"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileBar2 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFilePageSetup 
         Caption         =   "1008"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFilePrintPreview 
         Caption         =   "1009"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "1010"
      End
      Begin VB.Menu mnuFileBar3 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileSend 
         Caption         =   "1011"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileBar4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileMRU 
         Caption         =   ""
         Index           =   1
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileMRU 
         Caption         =   ""
         Index           =   2
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileMRU 
         Caption         =   ""
         Index           =   3
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileBar5 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "1012"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "1013"
      Visible         =   0   'False
      Begin VB.Menu mnuEditUndo 
         Caption         =   "1014"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEditBar0 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEditCut 
         Caption         =   "1015"
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditCopy 
         Caption         =   "1016"
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "1017"
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEditPasteSpecial 
         Caption         =   "1018"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "1019"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "1020"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "1021"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "1022"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuViewRefreshAll 
         Caption         =   "1179"
         Shortcut        =   {F6}
         Visible         =   0   'False
      End
      Begin VB.Menu mnuViewOptions 
         Caption         =   "1023"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuViewEditor 
         Caption         =   "1024"
      End
   End
   Begin VB.Menu mnuWindow 
      Caption         =   "1025"
      WindowList      =   -1  'True
      Begin VB.Menu mnuWindowNewWindow 
         Caption         =   "1026"
      End
      Begin VB.Menu mnuWindowBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuWindowCascade 
         Caption         =   "1027"
      End
      Begin VB.Menu mnuWindowTileHorizontal 
         Caption         =   "1028"
      End
      Begin VB.Menu mnuWindowTileVertical 
         Caption         =   "1029"
      End
      Begin VB.Menu mnuWindowArrangeIcons 
         Caption         =   "1030"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "1031"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "1032"
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "1033"
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "1034"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const EM_UNDO = &HC7
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Any) As Long
Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hWnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)
Private imPrintCount As Integer
Private olDocument As frmDocument

Public BC As clsBroadcasts

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   ??.0?.2001
' .
' . description :   Look if the users chooses another view.
' .                 If he does, start the rebuild.
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmbViews_Click()
    Dim olDocument As frmDocument
    If Module1.fMainForm.ActiveForm Is Nothing Then
        LoadNewDoc
    End If

    Set olDocument = Module1.fMainForm.ActiveForm
    olDocument.TabMain.SetFocus

    'only refresh if he chose another view!
    If olDocument.omViewName <> cmbViews.Text Then
        olDocument.omViewName = cmbViews.Text
        olDocument.Form_Reload
        frmCalc.FillMainGrid
        'olDocument.Caption = olDocument.Caption & " - " & CStr(olDocument.TabMain.GetLineCount) & " employees."
    End If
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   ??.0?.2001
' .
' . description :   Loading the MDI-Form
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub MDIForm_Load()
    'init the BCProxy for receiving broadcasts

    Set BC = New clsBroadcasts
    BC.InitBroadcasts
    BC.RegisterBC

    'do some layout
    LoadResStrings Me
    Me.Top = GetSetting(App.Title, "Settings", "MainTop", 1000)
    Me.Left = GetSetting(App.Title, "Settings", "MainLeft", 1000)
    Me.Width = GetSetting(App.Title, "Settings", "MainWidth", 6500)
    Me.Height = GetSetting(App.Title, "Settings", "MainHeight", 6500)

    'load data from the DB
    Load frmHiddenServerConnection

    'load a new document
    LoadNewDoc

    InitViewCombo
    'MDIForm_Unload (vbNo)
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   ??.0?.2001
' .
' . description :   init the combo with the names of the loaded views
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub InitViewCombo()
    Dim i As Integer
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    cmbViews.Clear
    For i = 1 To gcViews.Count Step 1
        cmbViews.AddItem gcViews(i).ViewName
    Next i

    cmbViews.Text = olDocument.omViewName
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   ??.0?.2001
' .
' . description :   loading a new frmDocument and increment the doc-counter
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub LoadNewDoc()
    Static lDocumentCount As Long
    Dim frmD As frmDocument
    lDocumentCount = lDocumentCount + 1
    Set frmD = New frmDocument
    frmD.Caption = "Document " & lDocumentCount
    frmD.WindowState = vbMaximized
    frmD.Show
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   07.06.2001
' .
' . description :   ask before unloading anything
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim Msg, Response   ' Declare variables.
    Msg = LoadResString(1180)
    Response = MsgBox(Msg, vbQuestion + vbYesNo, "Closing Application")

    Select Case Response
        Case vbYes:
            Cancel = 0  'the MDI will be closed
        Case Else:
            Cancel = 1  'the MDI won't be closed
    End Select
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   07.06.2001
' .
' . description :   unloading the MDI-Form
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub MDIForm_Unload(Cancel As Integer)
    Select Case Cancel
        Case 0:
            BC.UnRegisterBC
            Unload frmAbout
            Unload frmCalc
            Unload frmDetailWindow
            Unload frmDocument
            Unload frmHidden
            Unload frmHiddenServerConnection
            Unload frmOptions
            Unload frmSplash

            If Me.WindowState <> vbMinimized Then
                SaveSetting App.Title, "Settings", "MainLeft", Me.Left
                SaveSetting App.Title, "Settings", "MainTop", Me.Top
                SaveSetting App.Title, "Settings", "MainWidth", Me.Width
                SaveSetting App.Title, "Settings", "MainHeight", Me.Height
            End If

        Case Else:
            'the MDI won't be closed!
    End Select
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   ??.0?.2001
' .
' . description :   clicking the menus of the MDI
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub mnuFileNew_Click()
    LoadNewDoc
End Sub

'RRO: the following is commented for RFC 165, project SWR, 2001-09-24
'
'Private Sub txtMaxAbsWeek_Change()
'    If IsNumeric(txtMaxAbsWeek.Text) = True Then
'        UpDown1.Value = txtMaxAbsWeek.Text
'    End If
'End Sub
'Private Sub UpDown1_Change()
'    txtMaxAbsWeek.Text = UpDown1.Value
'    Dim olDocument As frmDocument
'    Set olDocument = Module1.fMainForm.ActiveForm
'    olDocument.CheckMaxAbsences
'End Sub

Private Sub tbToolBar_ButtonClick(ByVal Button As MSComCtlLib.Button)
    On Error Resume Next
    Select Case Button.key
        Case "New"
            LoadNewDoc
        Case "ViewEditor"
            If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") = 1 Then
                mnuViewEditor_Click
            End If
        Case "Print"
            mnuFilePrint_Click
        Case Else:
            'do nothing
            'perhaps we need the items below in future
        End Select

'        Case "Open"
'            mnuFileOpen_Click
'        Case "Save"
'            mnuFileSave_Click
'        Case "Cut"
'            mnuEditCut_Click
'        Case "Copy"
'            mnuEditCopy_Click
'        Case "Paste"
'            mnuEditPaste_Click
'        Case "Bold"
'            ActiveForm.rtfText.SelBold = Not ActiveForm.rtfText.SelBold
'            Button.Value = IIf(ActiveForm.rtfText.SelBold, tbrPressed, tbrUnpressed)
'        Case "Italic"
'            ActiveForm.rtfText.SelItalic = Not ActiveForm.rtfText.SelItalic
'            Button.Value = IIf(ActiveForm.rtfText.SelItalic, tbrPressed, tbrUnpressed)
'        Case "Underline"
'            ActiveForm.rtfText.SelUnderline = Not ActiveForm.rtfText.SelUnderline
'            Button.Value = IIf(ActiveForm.rtfText.SelUnderline, tbrPressed, tbrUnpressed)
'        Case "Align Left"
'            ActiveForm.rtfText.SelAlignment = rtfLeft
'        Case "Center"
'            ActiveForm.rtfText.SelAlignment = rtfCenter
'        Case "Align Right"
'            ActiveForm.rtfText.SelAlignment = rtfRight
End Sub

Private Sub mnuHelpAbout_Click()
    frmAbout.Show vbModal, Me
End Sub

Private Sub mnuHelpSearchForHelpOn_Click()
    Dim nRet As Integer

    'if there is no helpfile for this project display a message to the user
    'you can set the HelpFile for your application in the
    'Project Properties dialog
    If Len(App.HelpFile) = 0 Then
        MsgBox "Unable to display Help Contents. There is no Help associated with this project.", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hWnd, App.HelpFile, 261, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If
End Sub

Private Sub mnuHelpContents_Click()
    DisplayHtmHelpFile
End Sub

Private Sub mnuWindowArrangeIcons_Click()
    Me.Arrange vbArrangeIcons
End Sub

Private Sub mnuWindowTileVertical_Click()
    Me.Arrange vbTileVertical
End Sub

Private Sub mnuWindowTileHorizontal_Click()
    Me.Arrange vbTileHorizontal
End Sub

Private Sub mnuWindowCascade_Click()
    Me.Arrange vbCascade
End Sub

Private Sub mnuWindowNewWindow_Click()
    LoadNewDoc
End Sub

Private Sub mnuViewEditor_Click()
    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") = 1 Then
        frmOptions.Show vbModal, Me
    End If
    InitViewCombo
End Sub

Private Sub mnuViewRefresh_Click()
    Module1.fMainForm.ActiveForm.Form_Reload
    frmCalc.FillMainGrid
End Sub

Private Sub mnuViewStatusBar_Click()
    mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
    sbStatusBar.Visible = mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
    mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
    tbToolBar.Visible = mnuViewToolbar.Checked
End Sub

Private Sub mnuEditPaste_Click()
    On Error Resume Next
    ActiveForm.rtfText.SelRTF = Clipboard.GetText
End Sub

Private Sub mnuEditCopy_Click()
    On Error Resume Next
    Clipboard.SetText ActiveForm.rtfText.SelRTF
End Sub

Private Sub mnuEditCut_Click()
    On Error Resume Next
    Clipboard.SetText ActiveForm.rtfText.SelRTF
    ActiveForm.rtfText.SelText = vbNullString
End Sub

Private Sub mnuFileExit_Click()
    'unload the form
    Unload Me
End Sub

Private Sub mnuFilePrint_Click()
    On Error GoTo ErrHdl

    With Module1.fMainForm
        If .ActiveForm Is Nothing Then
            Exit Sub
        Else
            Set olDocument = .ActiveForm
        End If

        Dim strPrintDate As String
        Dim strTitle As String

        strPrintDate = Now
        strTitle = Module1.fMainForm.ActiveForm.Caption

        With .dlgCommonDialog
            .DialogTitle = "Print"
            .CancelError = True
            .Flags = cdlPDPrintSetup Or cdlPDReturnDC Or cdlPDHidePrintToFile Or cdlPDNoSelection
            .ShowPrinter
            If Err <> MSComDlg.cdlCancel Then
                imPrintCount = 0
                InitPrintTab
                PrintTab.PrintTab .hDC, strTitle, strPrintDate
            End If
        End With
    End With

    'PrintTab.ResetContent 'free memory
    Exit Sub
ErrHdl:
    If Err.Number = cdlCancel Then
    Else
        'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmMain.mnuFilePrint_Click", Err
        WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmMain.mnuFilePrint_Click", Err
    End If
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   18.09.2001
' .
' . description :   preparing the PrinTab for printing
' .                 - the TabLeft
' .                 - the TabHeader
' .                 - the TabRight
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub InitPrintTab()
    Dim strBuffer As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strMainHeaderRanges As String
    Dim strMainHeaderValues As String
    Dim strColumnAlignmentString As String

    'Dim strColumnAlignmentStr As String
    Dim i As Integer

    PrintTab.ResetContent
    PrintTab.MainHeader = True
    PrintTab.MainHeaderOnly = True
    PrintTab.ShowVertScroller False
    PrintTab.PrintSetPageTitleFont 18, True, False, True, 0, "Times New Roman"
    SetFonts

    'build header- & format-strings
    strHeaderString = Right(olDocument.TabMain.HeaderString, Len(olDocument.TabMain.HeaderString) - 1)
    strHeaderLengthString = Right(olDocument.TabMain.HeaderLengthString, Len(olDocument.TabMain.HeaderLengthString) - 2)
    strMainHeaderRanges = "3," & olDocument.TabHeader.GetMainHeaderRanges & ",2"
    strMainHeaderValues = "             " & olDocument.TabLeft.GetColumnValue(0, 0) & olDocument.TabLeft.GetColumnValue(0, 1) & "," & olDocument.TabHeader.GetMainHeaderValues & ","
    strColumnAlignmentString = "L,L,L,"
    For i = 1 To 53
        strColumnAlignmentString = strColumnAlignmentString & "R,"
    Next i
    strColumnAlignmentString = strColumnAlignmentString & "C,C"

    'insert header- & format-strings
    PrintTab.HeaderString = strHeaderString
    PrintTab.HeaderLengthString = strHeaderLengthString
    PrintTab.SetMainHeaderValues strMainHeaderRanges, strMainHeaderValues, ""
    PrintTab.ColumnAlignmentString = strColumnAlignmentString

    'insert data
    strBuffer = "," & olDocument.TabLeft.GetColumnValue(1, 0) & "," & olDocument.TabLeft.GetColumnValue(1, 1) & "," & _
        olDocument.TabHeader.GetLineValues(0) & olDocument.TabRight.GetLineValues(0) & Chr(10) & _
        "," & olDocument.TabLeft.GetColumnValue(2, 0) & "," & olDocument.TabLeft.GetColumnValue(2, 1) & "," & _
        olDocument.TabHeader.GetLineValues(1) & olDocument.TabRight.GetLineValues(1) & Chr(10) & _
        "," & olDocument.TabLeft.GetColumnValue(3, 0) & "," & olDocument.TabLeft.GetColumnValue(3, 1) & "," & _
        olDocument.TabHeader.GetLineValues(2) & olDocument.TabRight.GetLineValues(2) & Chr(10)
    PrintTab.InsertBuffer strBuffer, Chr(10)

    'PrintTab.PrintSetMargins MarginTop.Text, MarginBottom.Text, MarginLeft.Text, MarginRight.Text
    PrintTab.PrintSetMargins "20", "20", "20", "15"
    PrintTab.PrintSetPageHeaderHeight "20"
    PrintTab.PrintSetPageFooterHeight "15"
    PrintTab.PrintSetSeparatorLineTop "18", "1"
    PrintTab.PrintSetSeparatorLineBottom "13", "1"
    PrintTab.PrintSetDatePosition "B", "R"
    PrintTab.PrintSetTitlePosition "T", "L"
    PrintTab.PrintSetFitToPage True
    'PrintTab.PrintSetLogo "T", "R", GetIniEntry("", "AbsPlan", "", "PRINTBMP", "c:\ufis\system\logo.bmp")
    PrintTab.PrintSetLogo "T", "R", GetIniEntry("", "AbsPlan", "", "PRINTBMP", UFIS_SYSTEM & "\logo.bmp")
End Sub
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.09.2001
' .
' . description :   preparing the fonts of the PrinTab for printing
' .                 - the TabLeft
' .                 - the TabHeader
' .                 - the TabRight
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub SetFonts()
    Select Case olDocument.imSrceenWidth
        Case 1152:
            PrintTab.FontName = "Arial"
            PrintTab.FontSize = 13
        Case 1280:
            PrintTab.FontName = "Arial"
            PrintTab.FontSize = 14
        Case Else: 'e.g. 1024
            PrintTab.FontName = "Arial"
            PrintTab.FontSize = 12
    End Select
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   19.09.2001
' .
' . description :   preparing the PrinTab for printing
' .                 - the TabMain
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ReInitPrintTab1()
    Dim strTmp As String
    Dim strLine As String
    Dim strBuffer As String
    Dim strFirstItem As String

    Dim llGetLineCount As Long
    Dim llPacketCount As Long
    Dim llPacketSize As Long
    Dim llRestLines As Long
    Dim l As Long
    Dim m As Long

    Dim i As Integer
    Dim ilItemLen As Integer
    Dim ilFirstKommaPos As String

    PrintTab.ResetContent
    SetFonts1

    ' doing the layout
    PrintTab.DateTimeSetColumn 2
    PrintTab.DateTimeSetInputFormatString 2, "YYYYMMDD"
    PrintTab.DateTimeSetOutputFormatString 2, "DDMMMYY"
'    PrintTab.DateTimeColumns = "2"
'    PrintTab.ColumnDateFormats = ",,YYYYMMDD"
    strTmp = "L,L,R,"
    For i = 1 To 53 Step 1
        strTmp = strTmp + "C,"
    Next i
    strTmp = strTmp + "R,R,"
    PrintTab.ColumnAlignmentString = strTmp

    'filling with data
    llPacketSize = 50
    llGetLineCount = olDocument.TabMain.GetLineCount - 1
    llPacketCount = (llGetLineCount \ llPacketSize)

    For l = 1 To llPacketCount
        For m = ((l - 1) * llPacketSize) To ((l * llPacketSize) - 1)
            strLine = olDocument.TabMain.GetLineValues(m)
            ilFirstKommaPos = InStr(1, strLine, ",")
            If ilFirstKommaPos > 0 Then
                strBuffer = strBuffer & Right(strLine, Len(strLine) - ilFirstKommaPos) & Chr(10)
            End If
        Next m
        PrintTab.InsertBuffer strBuffer, Chr(10)
        strBuffer = ""
    Next l
    llRestLines = llGetLineCount Mod llPacketSize
    If llRestLines > 0 Then
        For m = ((l - 1) * llPacketSize) To llGetLineCount
            strLine = olDocument.TabMain.GetLineValues(m)
            ilFirstKommaPos = InStr(1, strLine, ",")
            If ilFirstKommaPos > 0 Then
                strBuffer = strBuffer & Right(strLine, Len(strLine) - ilFirstKommaPos) & Chr(10)
            End If
        Next m
        PrintTab.InsertBuffer strBuffer, Chr(10)
        strBuffer = ""
    End If
End Sub
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.09.2001
' .
' . description :   preparing the fonts of the PrinTab for printing
' .                 - the TabMain
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub SetFonts1()
    Select Case olDocument.imSrceenWidth
        Case 1152:
            PrintTab.CreateCellObj "LastName", vbWhite, vbBlack, 13, False, False, False, 0, "CourierNew"
            PrintTab.CreateCellObj "FirstName", vbWhite, vbBlack, 13, False, False, False, 0, "CourierNew"
            PrintTab.CreateCellObj "DateCol", vbWhite, vbBlack, 11, False, False, False, 0, "CourierNew"
            PrintTab.FontName = "CourierNew"
            PrintTab.FontSize = 13
        Case 1280:
            PrintTab.CreateCellObj "LastName", vbWhite, vbBlack, 14, False, False, False, 0, "CourierNew"
            PrintTab.CreateCellObj "FirstName", vbWhite, vbBlack, 14, False, False, False, 0, "CourierNew"
            PrintTab.CreateCellObj "DateCol", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            PrintTab.FontName = "CourierNew"
            PrintTab.FontSize = 14
        Case Else: 'e.g. 1024
            PrintTab.CreateCellObj "LastName", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            PrintTab.CreateCellObj "FirstName", vbWhite, vbBlack, 12, False, False, False, 0, "CourierNew"
            PrintTab.CreateCellObj "DateCol", vbWhite, vbBlack, 10, False, False, False, 0, "CourierNew"
            PrintTab.FontName = "CourierNew"
            PrintTab.FontSize = 12
    End Select

    PrintTab.SetColumnProperty 0, "LastName"
    PrintTab.SetColumnProperty 1, "FirstName"
    PrintTab.SetColumnProperty 2, "DateCol"
End Sub
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.09.2001
' .
' . description :   preparing the PrinTab for printing
' .                 - the TabBlockedWeeks (line "Blocked weeks")
' .                 - the TabMaxAbsWeek (lines "Max. absent per week" and "TTL heads absent")
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ReInitPrintTab2()
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strMainHeaderRanges As String
    Dim strMainHeaderValues As String
    Dim strColumnAlignmentString As String
    Dim i As Integer

    PrintTab.ResetContent
    PrintTab.MainHeader = False
    PrintTab.MainHeaderOnly = True
    PrintTab.DateTimeResetColumn 2
    'PrintTab.ColumnDateFormats = ""

    ' building format- & layout-strings
    strHeaderString = "," & olDocument.TabBlockedWeeks.HeaderString & ","
    strHeaderLengthString = olDocument.imLeftTabWidth & "," & _
        olDocument.TabBlockedWeeks.HeaderLengthString & _
        olDocument.imRightTabWidth
    strMainHeaderRanges = "1," & olDocument.TabBlockedWeeks.GetMainHeaderRanges & ",1"
    strMainHeaderValues = "," & olDocument.TabBlockedWeeks.GetMainHeaderValues & ","
    For i = 1 To 55
        strColumnAlignmentString = strColumnAlignmentString & "C,"
    Next i

    'insert header- & format-strings
    PrintTab.HeaderString = strHeaderString
    PrintTab.HeaderLengthString = strHeaderLengthString
    PrintTab.SetMainHeaderValues strMainHeaderRanges, strMainHeaderValues, ""
    PrintTab.ColumnAlignmentString = strColumnAlignmentString

    ' fill in the values
    PrintTab.InsertTextLineAt 0, olDocument.Option1(0).Caption & "," & olDocument.TabBlockedWeeks.GetLineValues(0), False
    PrintTab.InsertTextLineAt 1, olDocument.Option1(1).Caption & "," & olDocument.TabMaxAbsWeek.GetLineValues(0), False
    PrintTab.InsertTextLineAt 2, olDocument.Option1(2).Caption & "," & olDocument.TabMaxAbsWeek.GetLineValues(1), False

    SetFonts2
End Sub
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.09.2001
' .
' . description :   preparing the fonts of the PrinTab for printing
' .                 - the TabBlockedWeeks
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub SetFonts2()
    PrintTab.ResetColumnProperty 0
    PrintTab.ResetColumnProperty 1
    PrintTab.ResetColumnProperty 2

    PrintTab.CreateCellObj "Blue", vbBlue, vbBlack, 12, False, False, False, 0, "Wingdings"
    PrintTab.CreateCellObj "Black", vbBlack, vbWhite, 12, False, False, False, 0, "Wingdings"
    PrintTab.CreateCellObj "Red", vbRed, vbBlack, 12, False, False, False, 0, "Wingdings"

    Dim i As Integer
    Dim ilCompareValue As Integer
    Dim strKey As String
    Dim strLine As String

    strLine = olDocument.TabMaxAbsWeek.GetLineValues(0)
    For i = 1 To 53 Step 1
        strKey = CStr(i)
        ilCompareValue = CInt(GetItem(strLine, i, ","))

        With olDocument
            If DoesKeyExist(.colTTLHeadsAbsent, strKey) = True Then
                If .colTTLHeadsAbsent(strKey) > ilCompareValue Then
                    PrintTab.SetCellProperty 0, i, "Red"
                ElseIf .colTTLHeadsAbsent(strKey) = ilCompareValue Then
                    PrintTab.SetCellProperty 0, i, "Black"
                Else
                    PrintTab.SetCellProperty 0, i, "Blue"
                End If
            Else
                If ilCompareValue <> 0 Then
                    PrintTab.SetCellProperty 0, i, "Blue"
                Else
                    PrintTab.SetCellProperty 0, i, "Black"
                End If
            End If
        End With
    Next i
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.09.2001
' .
' . description :   preparing the PrinTab for printing
' .                 - the TabHRSMonth (line "HRS absent/month")
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ReInitPrintTab3()
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strMainHeaderRanges As String
    Dim strMainHeaderValues As String
    Dim strColumnAlignmentString As String
    Dim strTmp As String
    Dim i As Integer

    PrintTab.ResetContent

    ' building format- & layout-strings
    strHeaderString = "," & olDocument.TabHRSMonth.HeaderString
    strHeaderLengthString = olDocument.TabHRSMonth.HeaderLengthString
    strMainHeaderRanges = "1," & olDocument.TabHRSMonth.GetMainHeaderRanges
    strMainHeaderValues = "," & olDocument.TabHRSMonth.GetMainHeaderValues
    strColumnAlignmentString = "C,"
    For i = 1 To 14
        strColumnAlignmentString = strColumnAlignmentString & "R,"
    Next i
    For i = 1 To ItemCount(strHeaderLengthString, ",") - 1
        If i = 1 Then
            strTmp = strTmp & CStr(GetItem(strHeaderLengthString, i, ",") - 1) & ","
        ElseIf i = 4 Then
            strTmp = strTmp & CStr(GetItem(strHeaderLengthString, i, ",") - 1) & ","
        ElseIf i = 7 Then
            strTmp = strTmp & CStr(GetItem(strHeaderLengthString, i, ",") - 1) & ","
        ElseIf i = 9 Then
            strTmp = strTmp & CStr(GetItem(strHeaderLengthString, i, ",") - 1) & ","
        ElseIf i = 12 Then
            strTmp = strTmp & CStr(GetItem(strHeaderLengthString, i, ",") - 1) & ","
        Else
            strTmp = strTmp & GetItem(strHeaderLengthString, i, ",") & ","
        End If
        
    Next i
    strHeaderLengthString = olDocument.imLeftTabWidth & "," & strTmp

    'insert header- & format-strings
    PrintTab.HeaderString = strHeaderString
    PrintTab.HeaderLengthString = strHeaderLengthString
    PrintTab.SetMainHeaderValues strMainHeaderRanges, strMainHeaderValues, ""
    PrintTab.ColumnAlignmentString = strColumnAlignmentString

    ' fill in the values
    PrintTab.InsertTextLine olDocument.Option1(3).Caption & "," & olDocument.TabHRSMonth.GetLineValues(0), False
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.09.2001
' .
' . description :   preparing the PrinTab for printing
' .                 - the TabHRSQuart (line "HRS absent/month")
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ReInitPrintTab4()
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strMainHeaderRanges As String
    Dim strMainHeaderValues As String
    Dim strColumnAlignmentString As String
    Dim strTmp As String
    Dim i As Integer

    PrintTab.ResetContent

    ' building format- & layout-strings
    strHeaderString = "," & olDocument.TabHRSQuart.HeaderString
    strHeaderLengthString = olDocument.TabHRSQuart.HeaderLengthString
    strMainHeaderRanges = "1," & olDocument.TabHRSQuart.GetMainHeaderRanges
    strMainHeaderValues = "," & olDocument.TabHRSQuart.GetMainHeaderValues
    
    For i = 1 To ItemCount(strHeaderLengthString, ",") - 1
        If i Mod 2 = 0 Then
            If i <> 6 Then
                strTmp = strTmp & CStr(GetItem(strHeaderLengthString, i, ",") - 1) & ","
            Else
                strTmp = strTmp & GetItem(strHeaderLengthString, i, ",") & ","
            End If
        Else
            strTmp = strTmp & GetItem(strHeaderLengthString, i, ",") & ","
        End If
    Next i
    strHeaderLengthString = olDocument.imLeftTabWidth & "," & strTmp

    'insert header- & format-strings
    PrintTab.HeaderString = strHeaderString
    PrintTab.HeaderLengthString = strHeaderLengthString
    PrintTab.SetMainHeaderValues strMainHeaderRanges, strMainHeaderValues, ""

    ' fill in the values
    PrintTab.InsertTextLine olDocument.Option1(4).Caption & "," & olDocument.TabHRSQuart.GetLineValues(0), False
End Sub

Private Sub PrintTab_PrintFinishedTab()
    If imPrintCount < 1 Then            'the first time - we've already printed the top-grids

        imPrintCount = 1
        ReInitPrintTab1
        PrintTab.PrintRepeatStatus 0    'distance to the next TAB: 0 lines

    ElseIf imPrintCount = 1 Then

        imPrintCount = 2
        ReInitPrintTab2
        PrintTab.PrintRepeatStatus 0    'distance to the next TAB: 0 lines

    ElseIf imPrintCount = 2 Then

        imPrintCount = 3
        ReInitPrintTab3
        PrintTab.PrintRepeatStatus 0    'distance to the next TAB: 0 lines

    ElseIf imPrintCount = 3 Then

        imPrintCount = 4
        ReInitPrintTab4
        PrintTab.PrintRepeatStatus 0    'distance to the next TAB: 0 lines

    Else

        PrintTab.PrintRepeatStatus -1   'stop printing
 
    End If
End Sub

'Private Sub mnuFilePrint_Click()
'    'On Error Resume Next
'    On Error GoTo ErrHdl
'
'    Dim strPrintDate As String
'    Dim olPrintTab As TABLib.Tab
'
'    If ActiveForm Is Nothing Then Exit Sub
'
'    strPrintDate = Now
'
'    With dlgCommonDialog
'        .DialogTitle = "Print"
'        .CancelError = True
'        '.Flags = cdlPDReturnDC + cdlPDNoPageNums
'        .Flags = cdlPDPrintSetup Or cdlPDReturnDC Or cdlPDHidePrintToFile Or cdlPDNoSelection
'
''        If ActiveForm.rtfText.SelLength = 0 Then
''            .Flags = .Flags + cdlPDAllPages
''        Else
''            .Flags = .Flags + cdlPDSelection
''        End If
'        .ShowPrinter
'        If Err <> MSComDlg.cdlCancel Then
'            olPrintTab.PrintTab .hDC, "Test", strPrintDate
'            'ActiveForm.rtfText.SelPrint .hDC
'        End If
'    End With
'
'    Exit Sub
'ErrHdl:
'    If Err.Number = cdlCancel Then
'    Else
'        WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.CheckAddRemoveEmployee", Err
'    End If
'    Resume Next
'End Sub

'Private Sub mnuEditUndo_Click()
'    'ToDo: Add 'mnuEditUndo_Click' code.
'    MsgBox "Add 'mnuEditUndo_Click' code."
'End Sub
'
'Private Sub mnuEditPasteSpecial_Click()
'    'ToDo: Add 'mnuEditPasteSpecial_Click' code.
'    MsgBox "Add 'mnuEditPasteSpecial_Click' code."
'End Sub
'
'Private Sub mnuViewOptions_Click()
'    'ToDo: Add 'mnuViewOptions_Click' code.
'    MsgBox "Add 'mnuViewOptions_Click' code."
'End Sub
'
'Private Sub mnuFileSend_Click()
'    'ToDo: Add 'mnuFileSend_Click' code.
'    MsgBox "Add 'mnuFileSend_Click' code."
'End Sub
'
'
'Private Sub mnuFilePrintPreview_Click()
'    'ToDo: Add 'mnuFilePrintPreview_Click' code.
'    MsgBox "Add 'mnuFilePrintPreview_Click' code."
'End Sub
'
'Private Sub mnuFilePageSetup_Click()
'    On Error Resume Next
'    With dlgCommonDialog
'        .DialogTitle = "Page Setup"
'        .CancelError = True
'        .ShowPrinter
'    End With
'
'End Sub

'Private Sub mnuFileProperties_Click()
'    'ToDo: Add 'mnuFileProperties_Click' code.
'    MsgBox "Add 'mnuFileProperties_Click' code."
'End Sub
'
'Private Sub mnuFileSaveAll_Click()
'    'ToDo: Add 'mnuFileSaveAll_Click' code.
'    MsgBox "Add 'mnuFileSaveAll_Click' code."
'End Sub
'
'Private Sub mnuFileSaveAs_Click()
'    Dim sFile As String
'
'
'    If ActiveForm Is Nothing Then Exit Sub
'
'
'    With dlgCommonDialog
'        .DialogTitle = "Save As"
'        .CancelError = False
'        'ToDo: set the flags and attributes of the common dialog control
'        .Filter = "All Files (*.*)|*.*"
'        .ShowSave
'        If Len(.FileName) = 0 Then
'            Exit Sub
'        End If
'        sFile = .FileName
'    End With
'    ActiveForm.Caption = sFile
'    ActiveForm.rtfText.SaveFile sFile
'
'End Sub
'
'Private Sub mnuFileSave_Click()
'    Dim sFile As String
'    If Left$(ActiveForm.Caption, 8) = "Document" Then
'        With dlgCommonDialog
'            .DialogTitle = "Save"
'            .CancelError = False
'            'ToDo: set the flags and attributes of the common dialog control
'            .Filter = "All Files (*.*)|*.*"
'            .ShowSave
'            If Len(.FileName) = 0 Then
'                Exit Sub
'            End If
'            sFile = .FileName
'        End With
'        ActiveForm.rtfText.SaveFile sFile
'    Else
'        sFile = ActiveForm.Caption
'        ActiveForm.rtfText.SaveFile sFile
'    End If
'
'End Sub
'
'Private Sub mnuFileClose_Click()
'    'ToDo: Add 'mnuFileClose_Click' code.
'    MsgBox "Add 'mnuFileClose_Click' code."
'End Sub
'
'Private Sub mnuFileOpen_Click()
'    Dim sFile As String
'
'
'    If ActiveForm Is Nothing Then LoadNewDoc
'
'
'    With dlgCommonDialog
'        .DialogTitle = "Open"
'        .CancelError = False
'        'ToDo: set the flags and attributes of the common dialog control
'        .Filter = "All Files (*.*)|*.*"
'        .ShowOpen
'        If Len(.FileName) = 0 Then
'            Exit Sub
'        End If
'        sFile = .FileName
'    End With
'    ActiveForm.rtfText.LoadFile sFile
'    ActiveForm.Caption = sFile
'
'End Sub
