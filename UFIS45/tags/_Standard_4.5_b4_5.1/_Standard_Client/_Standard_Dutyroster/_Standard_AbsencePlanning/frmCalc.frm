VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmCalc 
   Caption         =   "Form1"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10605
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   10605
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB tmpTab 
      Height          =   855
      Left            =   7200
      TabIndex        =   2
      Top             =   5400
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCalc 
      Height          =   1695
      Left            =   2400
      TabIndex        =   1
      Top             =   5760
      Width           =   3615
      _Version        =   65536
      _ExtentX        =   6376
      _ExtentY        =   2990
      _StockProps     =   64
   End
   Begin TABLib.TAB TabAbsences 
      Height          =   3375
      Left            =   2040
      TabIndex        =   0
      Top             =   720
      Width           =   5295
      _Version        =   65536
      _ExtentX        =   9340
      _ExtentY        =   5953
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmCalc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim colEmployeeUrnos As New Collection

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   31.05.2001
' .
' . description :   updates the balance-information in all documents
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub CheckAddRemoveEmployee(stftabUrno As String)
    On Error GoTo ErrHdl
    Dim olEmployee As clsEmployee
    Dim olDocument As Form
    Dim olElement
    Dim llStatusValue As Long
    Dim llReferenceDate As Long
    Dim ilYearNow As Integer
    Dim ilLineNo As Integer
    Dim i As Integer
    Dim j As Integer

    Dim strCompare As String
    Dim strLine As String

    Dim blBelongsToView As Boolean
    Dim blFound As Boolean

    If DoesKeyExist(gcEmployee, stftabUrno) <> False Then
        Set olEmployee = gcEmployee(stftabUrno)
        ilYearNow = Year(Now)
        For i = 0 To Forms.Count - 1
            Set olDocument = Forms(i)
            If olDocument.Name = "frmDocument" Then 'we've found a frmDocument!!!
                Dim olView As clsView
                For Each olElement In gcViews
                    If olDocument.omViewName = olElement.ViewName Then
                        Set olView = olElement
                        Exit For
                    End If
                Next olElement
                If olView Is Nothing Then Exit Sub

                'we have to look if the employee belongs to the view or not
                If olView.ViewYear < ilYearNow Then     'we take the 31.12. as reference-date for memberships
                    llReferenceDate = (CLng(olView.ViewYear) * 10000) + 1231
                ElseIf olView.ViewYear > ilYearNow Then 'we take the 1.1. as reference-date for memberships
                    llReferenceDate = (CLng(olView.ViewYear) * 10000) + 101
                Else                                    'we take the actual date as reference for memberships
                    llReferenceDate = (CLng(olView.ViewYear) * 10000) + (100 * Month(Now)) + Day(Now)
                End If

                blBelongsToView = True
                If olView.DontCareOrgUnit <> True Then
                    strCompare = GetBestFitOrgUnit(olEmployee.EmployeeURNO, llReferenceDate)
                    If DoesKeyExist(olDocument.colOrgUnit, strCompare) <> False Then
                        blBelongsToView = True
                    Else
                        blBelongsToView = False
                    End If
                End If
                If blBelongsToView <> False Then
                    If olView.DontCareDutyGroup <> True Then
                        strCompare = GetBestFitDutyGroup(olEmployee.EmployeeURNO, llReferenceDate)
                        If DoesKeyExist(olDocument.colDutyGroup, strCompare) <> False Then
                            blBelongsToView = True
                        Else
                            blBelongsToView = False
                        End If
                    End If
                End If
                If blBelongsToView <> False Then
                    If olView.DontCareContract <> True Then
                        strCompare = GetBestFitContractType(olEmployee.EmployeeURNO, llReferenceDate)
                        If DoesKeyExist(olDocument.colContractType, strCompare) <> False Then
                            blBelongsToView = True
                        Else
                            blBelongsToView = False
                        End If
                    End If
                End If
                If blBelongsToView <> False Then
                    If olView.DontCareFunction <> True Then
                        strCompare = GetBestFitFunction(olEmployee.EmployeeURNO, llReferenceDate)
                        If DoesKeyExist(olDocument.colFunction, strCompare) <> False Then
                            blBelongsToView = True
                        Else
                            blBelongsToView = False
                        End If
                    End If
                End If

                ' let's look after a possible line of the employee
                blFound = False
                j = 0
                While (j < olDocument.TabMain.GetLineCount) And (blFound <> True)
                    j = j + 1
                    strLine = olDocument.TabMain.GetLineValues(j)
                    If GetItem(strLine, 1, ",") = Trim(stftabUrno) Then 'the user belongs to the view
                        blFound = True
                        ilLineNo = j
                    End If
                Wend

                'if he belongs to the view, we have to take care that we'll see him
                If blBelongsToView <> False Then
                    Dim strInsert As String
                    'build the string
                    strInsert = stftabUrno + "," + _
                        olEmployee.EmployeeSecondName + "," + _
                        olEmployee.EmployeeFirstName + "," + _
                        olEmployee.EmployeeEntryDate + "," + _
                        GetLineString(stftabUrno, CStr(olView.ViewYear), False) + "," + _
                        GetItem(olEmployee.ItemBalanceHoliday(CStr(olView.ViewYear)), 2, "@") '+ "," + _
                        'GetItem(olEmployee.ItemBalanceZIF(CStr(olView.ViewYear)), 2, "@")

                    If blFound <> True Then
                        olDocument.TabMain.InsertTextLine strInsert, True
                        olDocument.TabMain.SetLineColor olDocument.TabMain.GetLineCount, vbBlack, 12640511
                        olDocument.TabMain.SetCellProperty olDocument.TabMain.GetLineCount, 1, "Marked_LastName"
                        olDocument.TabMain.SetCellProperty olDocument.TabMain.GetLineCount, 2, "Marked_FirstName"
                        olDocument.TabMain.SetCellProperty olDocument.TabMain.GetLineCount, 3, "Marked_DateCol"
                        llStatusValue = olDocument.TabMain.GetLineStatusValue(olDocument.TabMain.GetLineCount) Or 1
                        If olEmployee.EmployeeHasChildren = True Then
                            llStatusValue = llStatusValue Or 4
                        Else
                            llStatusValue = llStatusValue And (2147483647 - 4)
                        End If
                        olDocument.TabMain.SetLineStatusValue olDocument.TabMain.GetLineCount, llStatusValue
                        olDocument.TabMain.TimerSetValue olDocument.TabMain.GetLineCount, 5
                    Else
                        olDocument.TabMain.SetLineColor ilLineNo, vbBlack, 12640511
                        olDocument.TabMain.SetCellProperty ilLineNo, 1, "Marked_LastName"
                        olDocument.TabMain.SetCellProperty ilLineNo, 2, "Marked_FirstName"
                        olDocument.TabMain.SetCellProperty ilLineNo, 3, "Marked_DateCol"
                        olDocument.TabMain.UpdateTextLine ilLineNo, strInsert, True
                        llStatusValue = olDocument.TabMain.GetLineStatusValue(ilLineNo) Or 1
                        If olEmployee.EmployeeHasChildren = True Then
                            llStatusValue = llStatusValue Or 4
                        Else
                            llStatusValue = llStatusValue And (2147483647 - 4)
                        End If
                        olDocument.TabMain.SetLineStatusValue ilLineNo, llStatusValue
                        'olDocument.TabMain.SetLineStatusValue ilLineNo, 1
                        olDocument.TabMain.TimerSetValue ilLineNo, 5
                    End If
                Else
                'if he doesn't belong to the view, we have to take care that we don't see him
                    If blFound <> False Then
                        llStatusValue = olDocument.TabMain.GetLineStatusValue(ilLineNo) Or 2
                        olDocument.TabMain.SetLineStatusValue ilLineNo, llStatusValue
                        'olDocument.TabMain.SetLineStatusValue ilLineNo, 2
                        olDocument.TabMain.TimerSetValue ilLineNo, 5
                        olDocument.TabMain.SetLineColor ilLineNo, vbBlack, 12640511
                        olDocument.TabMain.SetCellProperty ilLineNo, 1, "Marked_LastName"
                        olDocument.TabMain.SetCellProperty ilLineNo, 2, "Marked_FirstName"
                        olDocument.TabMain.SetCellProperty ilLineNo, 3, "Marked_DateCol"
                        olDocument.TabMain.RedrawTab
                    End If
                End If

            End If
        Next i
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.CheckAddRemoveEmployee", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.CheckAddRemoveEmployee", Err
    Err.Clear
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   28.05.2001
' .
' . description :   updates the balance-information in all documents
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub UpdateBalanceInformation(stftabUrno As String, strYear As String)
    On Error GoTo ErrHdl
    Dim olDocument As Form
    Dim blFound As Boolean
    Dim strLine As String
    Dim olView As clsView
    Dim olElement
    Dim i As Integer
    Dim j As Integer
    Dim llStatusValue As Long

    For i = 0 To Forms.Count - 1
        Set olDocument = Forms(i)
        If olDocument.Name = "frmDocument" Then 'we've found a frmDocument!!!
            For Each olElement In gcViews
                If olDocument.omViewName = olElement.ViewName Then
                    If CStr(olElement.ViewYear) = strYear Then
                        j = 0
                        blFound = False
                        While (j < olDocument.TabMain.GetLineCount) And (blFound <> True)
                            strLine = olDocument.TabMain.GetLineValues(j)
                            If GetItem(strLine, 1, ",") = Trim(stftabUrno) Then 'the user belongs to the view
                                blFound = True
                                Dim olEmployee As clsEmployee
                                If DoesKeyExist(gcEmployee, Trim(stftabUrno)) = True Then
                                    Set olEmployee = gcEmployee(stftabUrno)
                                    olDocument.TabMain.SetColumnValue j, 57, GetItem(olEmployee.ItemBalanceHoliday(strYear), 2, "@")
                                    'olDocument.TabMain.SetColumnValue j, 58, GetItem(olEmployee.ItemBalanceZIF(strYear), 2, "@")
                                    'olDocument.TabMain.SetLineStatusValue j, 1
                                    llStatusValue = olDocument.TabMain.GetLineStatusValue(j) Or 1
                                    olDocument.TabMain.SetLineStatusValue j, llStatusValue
                                    olDocument.TabMain.TimerSetValue j, 5
                                    olDocument.TabMain.SetLineColor j, vbBlack, 12640511
                                    olDocument.TabMain.SetCellProperty j, 1, "Marked_LastName"
                                    olDocument.TabMain.SetCellProperty j, 2, "Marked_FirstName"
                                    olDocument.TabMain.SetCellProperty j, 3, "Marked_DateCol"
                                    olDocument.TabMain.RedrawTab
                                End If
                            End If
                            j = j + 1
                        Wend
                    End If
                    Exit For
                End If
            Next olElement
        End If
        If olDocument.Name = "frmDetailWindow" Then 'we've found a frmDetailWindow!!!
            If ogActualEmployeeURNO = stftabUrno Then
                For Each olElement In gcViews
                    If olElement.ViewName = Module1.fMainForm.ActiveForm.omViewName Then
                        Set olView = olElement
                        Exit For
                    End If
                Next olElement
                If olView Is Nothing Then Exit Sub

                frmDetailWindow.GridocxHeader.LockReadOnly = False
                Dim Range As Object
                Set Range = frmDetailWindow.GridocxDetail.CreateRangeObject()
                Range.InitRange 1, 1, 6, 2
                frmDetailWindow.GridocxHeader.SetValueRange Range, "", gxOverride
                FillTopGrid (olView.ViewYear)
                frmDetailWindow.GridocxHeader.LockReadOnly = True
            End If
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.UpdateBalanceInformation", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.UpdateBalanceInformation", Err
    Err.Clear
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   21.05.2001
' .
' .description  :   updates the HRS-information in all documents
' .                 bInsert:    TRUE  - insert (e.g. the employee's got a new absence
' .                             FALSE - delete (e.g. the employee looses an absence
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub InsertDeleteHRS(stftabUrno As String, strDate As String, bInsert As Boolean)
    On Error GoTo ErrHdl
    Dim olDocument As Form
    Dim blFound As Boolean
    Dim strLine As String
    Dim strHelp As String
    Dim i As Integer
    Dim j As Integer

    For i = 0 To Forms.Count - 1
        Set olDocument = Forms(i)
        ' Let's look for the the documents
        If olDocument.Name = "frmDocument" Then 'we've found a frmDocument!!!
            j = 0
            blFound = False

            If DoesKeyExist(olDocument.colDateColumn, strDate) = True Then ' it is in the year
                While (j < olDocument.TabMain.GetLineCount) And (blFound <> True)
                    strLine = olDocument.TabMain.GetLineValues(j)
                    If GetItem(strLine, 1, ",") = Trim(stftabUrno) Then 'the user belongs to the view
                        blFound = True
                        Dim olEmployee As clsEmployee

                        ' look for the employee
                        If DoesKeyExist(gcEmployee, Trim(stftabUrno)) = True Then
                            ' get the WeeklyHours of the employee for this week
                            Set olEmployee = gcEmployee(stftabUrno)
                            Dim ilHours As Integer
                            If olEmployee.CountEmployeeWeeklyWorkingHours > 0 Then
                                If olEmployee.CountEmployeeWeeklyWorkingHours > 1 Then
                                    ' get the valid value for that week
                                    Dim llThisColumnDate As Long
                                    Dim llHelp As Long
                                    Dim llBestFit As Long
                                    llThisColumnDate = olDocument.colColumnDate(olDocument.colDateColumn(strDate))
                                    For j = 1 To olEmployee.CountEmployeeWeeklyWorkingHours
                                        strHelp = GetItem(olEmployee.ItemEmployeeWeeklyWorkingHours(j), 1, "@")
                                        If IsNumeric(strHelp) = True Then
                                            llHelp = CLng(Left(strHelp, 8))
                                            If llHelp <= llThisColumnDate Then
                                                If llHelp > llBestFit Then
                                                    llBestFit = llHelp
                                                    ilHours = Left(GetItem(olEmployee.ItemEmployeeWeeklyWorkingHours(j), 2, "@"), 2)
                                                End If
                                            End If
                                        End If
                                    Next j
                                Else
                                    ilHours = Left(GetItem(olEmployee.ItemEmployeeWeeklyWorkingHours(1), 2, "@"), 2)
                                End If
                            Else
                                ilHours = 40
                            End If

                            ' update the information in the colHrsAbsCol
                            Dim sHours As Single
                            sHours = 0.2 * ilHours
                            If bInsert = True Then ' add the hours for the day
                                If DoesKeyExist(olDocument.colHrsAbsCol, olDocument.colDateColumn(strDate)) <> False Then
                                    sHours = sHours + olDocument.colHrsAbsCol(CStr(olDocument.colDateColumn(strDate)))
                                    olDocument.colHrsAbsCol.Remove CStr(olDocument.colDateColumn(strDate))
                                    olDocument.colHrsAbsCol.Add Item:=sHours, key:=CStr(olDocument.colDateColumn(strDate))
                                Else
                                    olDocument.colHrsAbsCol.Add Item:=sHours, key:=CStr(olDocument.colDateColumn(strDate))
                                End If
                            Else ' remove the hours for the day
                                If DoesKeyExist(olDocument.colHrsAbsCol, olDocument.colDateColumn(strDate)) <> False Then
                                    sHours = olDocument.colHrsAbsCol(CStr(olDocument.colDateColumn(strDate))) - sHours
                                    olDocument.colHrsAbsCol.Remove CStr(olDocument.colDateColumn(strDate))
                                    olDocument.colHrsAbsCol.Add Item:=sHours, key:=CStr(olDocument.colDateColumn(strDate))
                                'Else 'this will never happen
                                '    olDocument.colHrsAbsCol.Add Item:=sHours, key:=olDocument.colDateColumn(strDate)
                                End If
                            End If

                            ' now update the lines in the TabMain
                            FillHRSInformation olDocument
                        End If
                    End If
                    j = j + 1
                Wend
            End If
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.InsertDeleteHRS", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.InsertDeleteHRS", Err
    Err.Clear
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   16.05.2001
' .
' .description  :   updates one employee in all documents
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub UpdateEmployee(stftabUrno As String)
    On Error GoTo ErrHdl
    Dim olDocument As Form
    Dim blFound As Boolean
    Dim strNewLine As String
    Dim strOldLine As String
    Dim strNewItem As String
    Dim strOldItem As String
    Dim strLine As String
    Dim strKey As String
    Dim strTTL As String
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim ilCount As Integer
    Dim olElement
    Dim olView As clsView
    Dim llStatusValue As Long

    For i = 0 To Forms.Count - 1
        Set olDocument = Forms(i)
        ' Let's look for the the documents
        If olDocument.Name = "frmDocument" Then
            ' we've found a frmDocument!!!
            j = 0
            blFound = False
            While (j < olDocument.TabMain.GetLineCount) And (blFound <> True)
                strLine = olDocument.TabMain.GetLineValues(j)
                If GetItem(strLine, 1, ",") = Trim(stftabUrno) Then
                    ' the user belongs to the view we found
                    blFound = True
                    For Each olElement In gcViews
                        If olElement.ViewName = olDocument.omViewName Then
                            Set olView = olElement
                            Exit For
                        End If
                    Next olElement
                    If olView Is Nothing Then Exit Sub

                    strOldLine = olDocument.TabMain.GetLineValues(j)
                    strNewLine = Trim(stftabUrno) + "," + _
                        GetItem(strLine, 2, ",") + "," + _
                        GetItem(strLine, 3, ",") + "," + _
                        GetItem(strLine, 4, ",") + "," + _
                        GetLineString(stftabUrno, olView.ViewYear, False)
                    olDocument.TabMain.SetLineColor j, vbBlack, 12640511
                    olDocument.TabMain.SetCellProperty j, 1, "Marked_LastName"
                    olDocument.TabMain.SetCellProperty j, 2, "Marked_FirstName"
                    olDocument.TabMain.SetCellProperty j, 3, "Marked_DateCol"
                    olDocument.TabMain.UpdateTextLine j, strNewLine, True
                    'olDocument.TabMain.SetLineStatusValue j, 1
                    llStatusValue = olDocument.TabMain.GetLineStatusValue(j) Or 1
                    olDocument.TabMain.SetLineStatusValue j, llStatusValue
                    olDocument.TabMain.TimerSetValue j, 5

                    ' now we have to calculate the TTL abs. per week
                    For k = 5 To 58
                        strNewItem = GetItem(strNewLine, k, ",")
                        strOldItem = GetItem(strOldLine, k, ",")
                        strKey = CStr(k - 4)
                        If (strNewItem <> "") And (strOldItem = "") Then
                            If DoesKeyExist(olDocument.colTTLHeadsAbsent, strKey) = True Then
                                ilCount = olDocument.colTTLHeadsAbsent(strKey) + 1
                                olDocument.colTTLHeadsAbsent.Remove (strKey)
                                olDocument.colTTLHeadsAbsent.Add Item:=ilCount, key:=strKey
                            Else
                                olDocument.colTTLHeadsAbsent.Add Item:=1, key:=strKey
                            End If
                        ElseIf (strNewItem = "") And (strOldItem <> "") Then
                            If DoesKeyExist(olDocument.colTTLHeadsAbsent, strKey) = True Then
                                ilCount = olDocument.colTTLHeadsAbsent(strKey) - 1
                                olDocument.colTTLHeadsAbsent.Remove (strKey)
                                olDocument.colTTLHeadsAbsent.Add Item:=ilCount, key:=strKey
                            End If
                        End If
                    Next k

                    For k = 1 To 53 Step 1
                        If DoesKeyExist(olDocument.colTTLHeadsAbsent, CStr(k)) = True Then
                            strTTL = strTTL + CStr(olDocument.colTTLHeadsAbsent(CStr(k))) + ","
                        Else
                            strTTL = strTTL + "0,"
                        End If
                    Next k
                    olDocument.TabMaxAbsWeek.UpdateTextLine 1, strTTL, True
                    olDocument.CheckMaxAbsences
                    FillHRSInformation olDocument
                End If
                j = j + 1
            Wend
        End If

        If olDocument.Name = "frmDetailWindow" Then 'we've found a frmDetailWindow!!!
            If ogActualEmployeeURNO = stftabUrno Then
                For Each olElement In gcViews
                    If olElement.ViewName = Module1.fMainForm.ActiveForm.omViewName Then
                        Set olView = olElement
                        Exit For
                    End If
                Next olElement

                frmDetailWindow.GridocxDetail.LockReadOnly = False
                Dim Range As Object
                Set Range = frmDetailWindow.GridocxDetail.CreateRangeObject()
                Range.InitRange 1, 1, 31, 14
                frmDetailWindow.GridocxDetail.SetValueRange Range, "", gxOverride
                frmDetailWindow.GridocxDetail.LockReadOnly = True
                
                If olView Is Nothing Then
                    FillDetailWindow frmDetailWindow.imYear
                Else
                    FillDetailWindow olView.ViewYear
                End If
            End If
        End If
    Next i

    Set olDocument = Nothing
    Set olElement = Nothing
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.UpdateEmployee", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.UpdateEmployee", Err
    Err.Clear
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   17.04.2001
' .
' .description  :   fills the grid in the frmDocument depending on the
' .                 actual view
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub FillMainGrid()
    On Error GoTo ErrHdl
    Module1.fMainForm.MousePointer = vbHourglass
    DoEvents

    Dim olDocument As frmDocument
1    Set olDocument = Module1.fMainForm.ActiveForm

2    If Trim(olDocument.omViewName) <> "" Then
        Dim olView As clsView
        Dim olElement

        Dim strBuffer As String
        Dim strLineNo As String
        Dim strLine As String
        Dim strURNO As String
        Dim strTTL As String
        Dim strKey As String
        Dim strMaxAbs As String
        Dim strHlp As String

        Dim i As Integer
        Dim j As Integer
        Dim iIdx As Integer

        ' just look after the number of columns etc.
3        InitTabs

        ' get the actual view
        For Each olElement In gcViews
            If olElement.ViewName = olDocument.omViewName Then
                Set olView = olElement
                Exit For
            End If
        Next
        If olView Is Nothing Then Exit Sub
        olDocument.Caption = olView.ViewName

        'load DRR-data if necessary
4        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1157)
        If frmHiddenServerConnection.blLocal = True Then
            frmHiddenServerConnection.LoadDRR_Local olView.ViewYear
        Else
            frmHiddenServerConnection.LoadDRR olView.ViewYear
        End If

        'delete collections
5        While olDocument.colTTLHeadsAbsent.Count > 0
           olDocument.colTTLHeadsAbsent.Remove 1
        Wend
        While olDocument.colHrsAbsCol.Count > 0
           olDocument.colHrsAbsCol.Remove 1
        Wend
        While olDocument.colAbsType.Count > 0
           olDocument.colAbsType.Remove 1
        Wend
        While olDocument.colTotalAbsCountWeekly.Count > 0
           olDocument.colTotalAbsCountWeekly.Remove 1
        Wend

        'look after the absences belonging to the view
6        For i = 1 To olView.CountAbsenceType Step 1
            strKey = CStr(olView.ItemAbsenceType(i))
            If DoesKeyExist(olDocument.colAbsType, strKey) <> True Then
                olDocument.colAbsType.Add key:=strKey, Item:=0
            End If
        Next i

        'Text: "Looking for the people belonging to the view..."
7        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1150)
        FindPeople

        'get the line numbers of the people
        For i = 1 To colEmployeeUrnos.Count
            strBuffer = strBuffer + colEmployeeUrnos(i) + ","
        Next i
        If Len(strBuffer) > 0 Then
            strBuffer = Left(strBuffer, Len(strBuffer) - 1)
        End If
        iIdx = GetItemNo(frmHiddenServerConnection.TabSTF.HeaderString, "URNO") - 1
        strLineNo = frmHiddenServerConnection.TabSTF.GetLinesByColumnValues(CLng(iIdx), strBuffer, 0)

        ' for every employee belonging to the view...
8        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1151)

        Dim strKidsLines As String
        Dim olEmployee As clsEmployee
        Dim llLineNumber As Long

        'hier werden die gefundenen Zeilen-Nummern reingehauen -> sau schnell!
        tmpTab.ResetContent
        tmpTab.SetFieldSeparator "|"
        tmpTab.InsertBuffer strLineNo, ","
        strBuffer = ""

9        For i = 0 To tmpTab.GetLineCount - 1 Step 1
            ' ... add a line to the TabMain
            llLineNumber = tmpTab.GetColumnValue(i, 0)
            strURNO = frmHiddenServerConnection.TabSTF.GetColumnValue(llLineNumber, iIdx)
            strBuffer = strBuffer & strURNO & "," & _
                frmHiddenServerConnection.TabSTF.GetColumnValue(llLineNumber, 1) & "," & _
                frmHiddenServerConnection.TabSTF.GetColumnValue(llLineNumber, 0) & "," & _
                frmHiddenServerConnection.TabSTF.GetColumnValue(llLineNumber, 2) & "," & _
                GetLineString(strURNO, olView.ViewYear, True) & Chr(10)

            ' insert the buffer
            If (i > 0) And (i Mod 40) = 0 Then
                olDocument.TabMain.InsertBuffer strBuffer, Chr(10)
                strBuffer = ""
            End If

            ' keep the line in mind whether the MA's got children
            If frmHiddenServerConnection.TabSTF.GetColumnValue(llLineNumber, 7) = "1" Then
                strKidsLines = strKidsLines & (i) & ","
            End If
        Next i
        olDocument.TabMain.InsertBuffer strBuffer, Chr(10)

        Dim llLineStatus As Long
        tmpTab.ResetContent
        tmpTab.InsertBuffer strKidsLines, ","
        For i = 0 To tmpTab.GetLineCount - 1
            llLineNumber = tmpTab.GetColumnValue(i, 0)
            llLineStatus = olDocument.TabMain.GetLineStatusValue(llLineNumber)
            llLineStatus = llLineStatus Or 4
            olDocument.TabMain.SetLineStatusValue llLineNumber, llLineStatus
        Next i

        'now we are looking after the bottom-TABs...
10        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1155)
        For i = 1 To 53 Step 1
            strKey = CStr(i)
            If DoesKeyExist(olDocument.colTTLHeadsAbsent, strKey) = True Then
                strTTL = strTTL + CStr(olDocument.colTTLHeadsAbsent(strKey)) + ","
            Else
                strTTL = strTTL + "0,"
            End If
        Next i
        Dim strTotalAbsences As String
        For i = 1 To 53 Step 1
            strKey = CStr(i)
            If DoesKeyExist(olDocument.colTotalAbsCountWeekly, strKey) = True Then
                strTotalAbsences = strTotalAbsences + CStr(olDocument.colTotalAbsCountWeekly(strKey)) + ","
            Else
                strTotalAbsences = strTotalAbsences + "0,"
            End If
        Next i
        
11        olDocument.TabMaxAbsWeek.UpdateTextLine 0, strMaxAbs, False
        olDocument.TabMaxAbsWeek.UpdateTextLine 1, strTTL, False
        olDocument.TabMaxAbsWeek.UpdateTextLine 2, strTotalAbsences, True
        FillTabMaxAbsWeek olView, olDocument
        olDocument.CheckMaxAbsences
        olDocument.CheckLineColors

        ' Text: "Calculating the hours..."
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1156)
        FillHRSInformation olDocument
    End If

    'Text: "Ready."
    Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1153)
    Module1.fMainForm.MousePointer = vbDefault
    olDocument.TabMain.RedrawTab
    DoEvents
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.FillMainGrid at section " & CStr(Erl), Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.FillMainGrid at section " & CStr(Erl), Err
    Err.Clear
    Resume Next
End Sub

Private Function FillTabMaxAbsWeek(ByRef rView As clsView, ByRef rDocument As frmDocument)
    Dim strInsert As String
    Dim i As Integer
    Dim olMaxAbsentPerWeek As clsMaxAbsentPerWeek

    If DoesKeyExist(gcMaxAbsent, rView.ViewAbsentPattern) = True Then
        Set olMaxAbsentPerWeek = gcMaxAbsent.Item(rView.ViewAbsentPattern)
        Dim ilDay As Byte
        Dim ilWeek As Byte
        Dim ilYear As Integer
        Dim ilMonth As Byte
        Dim strValues As String
        Dim strDate As String

        strValues = rDocument.TabHeader.GetLineValues(0)
        For i = 1 To 53
            strDate = rDocument.colColumnDate(CStr(i))
            ilYear = Left(strDate, 4)
            ilMonth = Mid(strDate, 5, 2)
            ilDay = Mid(strDate, 7, 2)
            ilWeek = frmHidden.GetWeekOfDay(ilDay, ilMonth, ilYear)
            If i = 53 And ilWeek = 1 Then
                ilYear = ilYear + 1
            ElseIf i = 1 And ilMonth = 12 Then
                ilYear = ilYear + 1
            End If
            strInsert = strInsert & CStr(olMaxAbsentPerWeek.GetValue(ilYear, CInt(ilWeek))) & ","
        Next i
    Else
        For i = 1 To 53
            strInsert = strInsert & "0,"
        Next i
    End If
    rDocument.TabMaxAbsWeek.UpdateTextLine 0, strInsert, False
End Function

Private Function FillHRSInformation(ByRef pDocument As frmDocument)
    On Error GoTo ErrHdl
    Dim slMonth As Single
    Dim slQuart As Single
    Dim slYear As Single
    Dim strMonth As String
    Dim strQuart As String
    Dim olMonthString As String

    Dim ilColumns As Integer
    Dim ilActCol As Integer
    Dim i As Integer
    Dim j As Integer

    slQuart = 0
    slYear = 0
    If GetItem(ogMainHeaderRanges, 1, ",") = 1 Then
        strMonth = strMonth + ","
        strQuart = strQuart + ","
        ilActCol = 2
        For i = 2 To ItemCount(ogMainHeaderRanges, ",") - 1
            slMonth = 0
            For j = 1 To GetItem(ogMainHeaderRanges, i, ",")
                If DoesKeyExist(pDocument.colHrsAbsCol, (CStr(ilActCol))) = True Then
                    slMonth = slMonth + pDocument.colHrsAbsCol(CStr(ilActCol))
                End If
                ilActCol = ilActCol + 1
            Next j
            slQuart = slQuart + slMonth
            strMonth = strMonth + CStr(Round(slMonth, 0)) + ","
            If (i - 1) Mod 3 = 0 Then
                strQuart = strQuart + CStr(Round(slQuart, 0)) + ","
                slYear = slYear + slQuart
                slQuart = 0
            End If
        Next i
    ElseIf GetItem(ogMainHeaderRanges, (ItemCount(ogMainHeaderRanges, ",") - 1), ",") = 1 Then
        ilActCol = 1
        For i = 1 To ItemCount(ogMainHeaderRanges, ",") - 2
            slMonth = 0
            For j = 1 To GetItem(ogMainHeaderRanges, i, ",")
                If DoesKeyExist(pDocument.colHrsAbsCol, (CStr(ilActCol))) = True Then
                    slMonth = slMonth + pDocument.colHrsAbsCol(CStr(ilActCol))
                End If
                ilActCol = ilActCol + 1
            Next j
            slQuart = slQuart + slMonth
            strMonth = strMonth + CStr(Round(slMonth, 0)) + ","
            If i Mod 3 = 0 Then
                strQuart = strQuart + CStr(Round(slQuart, 0)) + ","
                slYear = slYear + slQuart
                slQuart = 0
            End If
        Next i
        strMonth = strMonth + ","
        strQuart = strQuart + ","
    Else
        ilActCol = 1
        For i = 1 To ItemCount(ogMainHeaderRanges, ",") - 1
            slMonth = 0
            For j = 1 To GetItem(ogMainHeaderRanges, i, ",")
                If DoesKeyExist(pDocument.colHrsAbsCol, (CStr(ilActCol))) = True Then
                    slMonth = slMonth + pDocument.colHrsAbsCol(CStr(ilActCol))
                End If
                ilActCol = ilActCol + 1
            Next j
            slQuart = slQuart + slMonth
            strMonth = strMonth + CStr(Round(slMonth, 0)) + ","
            If i Mod 3 = 0 Then
                strQuart = strQuart + CStr(Round(slQuart, 0)) + ","
                slYear = slYear + slQuart
                slQuart = 0
            End If
        Next i
    End If
    pDocument.TabHRSMonth.UpdateTextLine 0, strMonth, True
    pDocument.TabHRSQuart.UpdateTextLine 0, strQuart, True
    pDocument.TabHRSMonth.SetColumnValue 0, pDocument.TabHRSMonth.GetColumnCount - 1, LoadResString(1136)
    pDocument.TabHRSQuart.SetColumnValue 0, pDocument.TabHRSQuart.GetColumnCount - 1, CStr(Round(slYear, 0)) + "  "
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.FillHRSInformation", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.FillHRSInformation", Err
    Err.Clear
    Resume Next
End Function

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   19.04.2001
' .
' .description  :   fills the grid in the frmDetailWindow depending on the
' .                 actual employee.
' .                 This is the preparation for the properly filling.
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub FillDetailWindow(iYear As Integer)
    On Error GoTo ErrHdl
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    If (Trim(olDocument.omViewName) <> "") Or (frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1) Then
        Dim olElement

        Dim strBuffer As String
        Dim strLineNo As String
        Dim strLine As String
        Dim strBSDU As String
        Dim strDate As String
        Dim strHelp As String
        Dim strCol As String
        Dim strAbs As String

        Dim i As Integer
        Dim ilYear As Integer

        Dim ilDay As Byte
        Dim ilMonth As Byte
        Dim ilWeek As Byte

        TabAbsences.ResetContent

        ' Get all absences of the employee and put them in the TabAbsences
        strLineNo = frmHiddenServerConnection.TabDRR.GetLinesByColumnValue(1, ogActualEmployeeURNO, 0)

        For i = 1 To ItemCount(strLineNo, ",") Step 1

            ' Add all absences of the employee to the TabAbsences
            strLine = frmHiddenServerConnection.TabDRR.GetLineValues(GetItem(strLineNo, i, ","))
            strBSDU = GetItem(strLine, 4, ",")

            If strBSDU <> "0" Then

                ' look after the date of the absence
                strDate = GetItem(strLine, 1, ",")

                ' look after the kind of absence
                strHelp = frmHiddenServerConnection.TabODA.GetLinesByColumnValue(2, strBSDU, 0)
                If IsNumeric(strHelp) <> False Then

                    strAbs = Trim(frmHiddenServerConnection.TabODA.GetColumnValue(CInt(strHelp), 0))
                    If DoesKeyExist(gcAbsenceCodeLookup, strAbs) = True Then 'only then it is an absence!

                        ' look after the week the absence belongs to
                        ilYear = Left(strDate, 4)
                        ilMonth = Mid(strDate, 5, 2)
                        ilDay = Mid(strDate, 7, 2)
                        ilWeek = frmHidden.GetWeekOfDay(ilDay, ilMonth, ilYear)
        
                        ' THIS IS IMPORTANT! Building the absence-information to put into the TabAbsences.
                        ' So we can get the absences by week later (in "FillWeeksOfYear").
                        strCol = CStr(ilWeek) + "@" + CStr(ilYear) + "@"
                        strBuffer = strBuffer + Trim(strDate) + "," + strAbs + "," + strCol + Chr(10)
        
                        If (i > 1) And (i Mod 20) = 0 Then
                            TabAbsences.InsertBuffer strBuffer, Chr(10)
                            strBuffer = ""
                        End If
                    End If
                End If
            End If
        Next i
        TabAbsences.InsertBuffer strBuffer, Chr(10)
        strBuffer = ""
' Hooray!!! Now we've got all absences of the employee. So let's look after the weeks they are in

        FillWeeksOfYear 48, 53, iYear, 1, 1
        FillWeeksOfYear 1, 53, iYear, 2, 13
        FillWeeksOfYear 1, 6, iYear, 14, 14

' now we have to calculate the information  in the top-grid
        FillTopGrid (iYear)
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.FillDetailWindow", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.FillDetailWindow", Err
    Err.Clear
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   15.05.2001
' .
' .description  :   fills the top grid in the frmDetailWindow
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub FillTopGrid(iYear As Integer)
    On Error GoTo ErrHdl

    Dim llYELA As Single
    Dim llYECU As Single
    Dim llOP01 As Single
    Dim llCL12 As Single

    Dim strLineNo As String
    Dim strLine As String
    Dim strHlp As String
    Dim ilDecSep As Integer
    Dim iIdx As Integer
    Dim i As Integer

    Dim LCID As Long
    LCID = GetSystemDefaultLCID()
    Dim strDecSep As String
    strDecSep = GetUserLocaleInfo(LCID, LOCALE_SDECIMAL)
    Dim strFormat As String
    strFormat = "#####0" & "." & "00"

    Dim Range As Object
    Set Range = frmDetailWindow.GridocxHeader.CreateRangeObject()

    strLineNo = frmHiddenServerConnection.TabACC.GetLinesByColumnValues(1, ogActualEmployeeURNO, 0)
    For i = 1 To ItemCount(strLineNo, ",")
        strLine = frmHiddenServerConnection.TabACC.GetLineValues(GetItem(strLineNo, i, ","))
        
        strHlp = ""
        iIdx = GetItemNo(frmHiddenServerConnection.TabACC.HeaderString, "YECU")
        strHlp = Trim(GetItem(strLine, iIdx, ","))
        If (strHlp <> "") Then
            llYELA = 0
            llYECU = 0
            llOP01 = 0
            llCL12 = 0

            strHlp = RoundCutNumber(strHlp, 2)
            If IsNumeric(strHlp) = True Then llYECU = strHlp
    
            iIdx = GetItemNo(frmHiddenServerConnection.TabACC.HeaderString, "OP01")
            strHlp = RoundCutNumber(Trim(GetItem(strLine, iIdx, ",")), 2)
            If IsNumeric(strHlp) = True Then llOP01 = strHlp
    
            iIdx = GetItemNo(frmHiddenServerConnection.TabACC.HeaderString, "CL12")
            strHlp = RoundCutNumber(Trim(GetItem(strLine, iIdx, ",")), 2)
            If IsNumeric(strHlp) = True Then llCL12 = strHlp
            
            iIdx = GetItemNo(frmHiddenServerConnection.TabACC.HeaderString, "YELA")
            strHlp = RoundCutNumber(Trim(GetItem(strLine, iIdx, ",")), 2)
            If IsNumeric(strHlp) = True Then llYELA = strHlp
    
            iIdx = GetItemNo(frmHiddenServerConnection.TabACC.HeaderString, "TYPE")
            strHlp = GetItem(strLine, iIdx, ",")
            If IsNumeric(strHlp) <> False Then
                Select Case CInt(strHlp)
                    Case 30: 'we've found holiday-information
                        iIdx = GetItemNo(frmHiddenServerConnection.TabACC.HeaderString, "YEAR")
                        strHlp = GetItem(strLine, iIdx, ",")
                        If IsNumeric(strHlp) <> False Then
                            Select Case CInt(strHlp)
                                Case iYear:
                                    Range.InitRange 1, 1, 1, 1
                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llYELA, strFormat), gxOverride
                                    Range.InitRange 2, 1, 2, 1
                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llYECU, strFormat), gxOverride
                                    Range.InitRange 3, 1, 3, 1
                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(Round(((llYELA + llYECU) - llCL12), 2), strFormat), gxOverride
                                    Range.InitRange 4, 1, 4, 1
                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llCL12, strFormat), gxOverride
                                Case (iYear + 1):
                                    Range.InitRange 5, 1, 5, 1
                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llYECU, strFormat), gxOverride
                                    Range.InitRange 6, 1, 6, 1
                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(Round(((llYELA + llYECU) - llCL12), 2), strFormat), gxOverride
                                    Range.InitRange 7, 1, 7, 1
                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llCL12, strFormat), gxOverride
                            End Select
                        End If
'                    Case 31: 'we've found ZIF-information
'                        iIdx = GetItemNo(frmHiddenServerConnection.TabACC.HeaderString, "YEAR")
'                        strHlp = GetItem(strLine, iIdx, ",")
'                        If IsNumeric(strHlp) <> False Then
'                            Select Case CInt(strHlp)
'                                Case iYear:
'                                    Range.InitRange 1, 2, 1, 2
'                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llYELA, strFormat), gxOverride
'                                    Range.InitRange 2, 2, 2, 2
'                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llYECU, strFormat), gxOverride
'                                    Range.InitRange 3, 2, 3, 2
'                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(Round(((llYELA + llYECU) - llCL12), 2), strFormat), gxOverride
'                                    Range.InitRange 4, 2, 4, 2
'                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llCL12, strFormat), gxOverride
'                                Case (iYear + 1):
'                                    Range.InitRange 5, 2, 5, 2
'                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llYECU, strFormat), gxOverride
'                                    Range.InitRange 6, 2, 6, 2
'                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(Round(((llYELA + llYECU) - llCL12), 2), strFormat), gxOverride
'                                    Range.InitRange 7, 2, 7, 2
'                                    frmDetailWindow.GridocxHeader.SetValueRange Range, Format(llCL12, strFormat), gxOverride
'                            End Select
'                        End If
                End Select
            End If
        End If
    Next i

    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.FillTopGrid", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.FillTopGrid", Err
    Err.Clear
    Resume Next
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.04.2001
' .
' .description  :   fills the grid in the frmDetailWindow depending on the
' .                 actual employee. Depends on the values in the TabAbsences!!!
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub FillWeeksOfYear(StartWeek As Byte, EndWeek As Byte, iYear As Integer, FirstLine As Byte, LastLine As Byte)
    On Error GoTo ErrHdl
    Dim colWeeklyAbsenceCodes As New Collection

    Dim i As Integer
    Dim j As Integer
    Dim ilLineNo As Integer
    Dim ilCount As Integer

    Dim blLeft As Byte
    Dim blRight As Byte
    Dim blRow As Byte

    Dim strLineNo As String
    Dim strLine As String
    Dim strDate As String
    Dim strAbs As String
    Dim strItem As String

    Dim olElement

    DoEvents
    frmDetailWindow.GridocxDetail.LockReadOnly = False
    Dim Range As Object
    Set Range = frmDetailWindow.GridocxDetail.CreateRangeObject()

    For i = StartWeek To EndWeek Step 1

        ' Remove name from the collection.
        For j = 1 To colWeeklyAbsenceCodes.Count
           colWeeklyAbsenceCodes.Remove 1
        Next j

        ' fill the collection to lookup the number of the different weekly absences
        If FirstLine <> LastLine Then
            strLineNo = TabAbsences.GetLinesByColumnValue(2, CStr(i) & "@" & CStr(iYear) & "@", 0)
        Else
            If FirstLine = 1 Then
                strLineNo = TabAbsences.GetLinesByColumnValue(2, CStr(i) + "@" + CStr(iYear - 1) & "@", 0)
            Else
                strLineNo = TabAbsences.GetLinesByColumnValue(2, CStr(i) + "@" + CStr(iYear + 1) & "@", 0)
            End If
        End If
        
        For j = 1 To ItemCount(strLineNo, ",") Step 1
            ilLineNo = GetItem(strLineNo, j, ",")
            strAbs = TabAbsences.GetColumnValue(ilLineNo, 1)
            If DoesKeyExist(colWeeklyAbsenceCodes, strAbs) = True Then
                ilCount = GetItem(colWeeklyAbsenceCodes.Item(strAbs), 1, "@") + 1
                strItem = CStr(ilCount) + "@" + strAbs
                colWeeklyAbsenceCodes.Remove (strAbs)
                colWeeklyAbsenceCodes.Add Item:=strItem, key:=strAbs
            Else
                strItem = "1@" + strAbs
                colWeeklyAbsenceCodes.Add Item:=strItem, key:=strAbs
            End If
        Next j

        ' fill in the chars
        For j = 1 To ItemCount(strLineNo, ",") Step 1
            strLine = TabAbsences.GetLineValues(GetItem(strLineNo, j, ","))
            strDate = GetItem(strLine, 1, ",")
            strAbs = GetItem(strLine, 2, ",")

            If GetItem(colWeeklyAbsenceCodes(strAbs), 1, "@") > 4 Then
                If DoesKeyExist(gcAbsenceCodeLookup, strAbs) = True Then
                    strAbs = UCase(gcAbsenceCodeLookup.Item(strAbs))
                    If strAbs = "" Then strAbs = "x"
                End If
            Else
                If DoesKeyExist(gcAbsenceCodeLookup, strAbs) = True Then
                    strAbs = LCase(gcAbsenceCodeLookup.Item(strAbs))
                    If strAbs = "" Then strAbs = "x"
                End If
            End If

            If FirstLine <> LastLine Then
                blLeft = Mid(strDate, 7, 2)
                blRight = blLeft
                blRow = Mid(strDate, 5, 2) + 1
                Range.InitRange blLeft, blRow, blRight, blRow
            Else
                blLeft = Mid(strDate, 7, 2)
                blRight = blLeft
                blRow = FirstLine
                Range.InitRange blLeft, blRow, blRight, blRow
            End If
            frmDetailWindow.GridocxDetail.SetValueRange Range, strAbs, gxOverride
        Next j
    Next i

    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.FillWeeksOfYear", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.FillWeeksOfYear", Err
    Err.Clear
    Resume Next
End Sub

Private Function GetBestFitDutyGroup(stftabUrno As String, lReferenceDate As Long) As String
    On Error GoTo ErrHdl
    Dim llBestFit As Long
    Dim strHelp As String
    Dim llHelp As Long
    Dim llTo As Long
    Dim i As Integer
    Dim olEmployee As clsEmployee
    If stftabUrno = "" Then
        Exit Function
    End If
    Set olEmployee = gcEmployee(stftabUrno)
    llBestFit = 0
    For i = 1 To olEmployee.CountDutyGroup
        strHelp = GetItem(olEmployee.ItemDutyGroup(i), 1, "@")
        If Len(strHelp) > 8 Then
            llHelp = CLng(Left(strHelp, 8))
            If llHelp <= lReferenceDate Then
                'testing the VATO (valid to)
                strHelp = GetItem(olEmployee.ItemDutyGroup(i), 3, "@")
                If Len(strHelp) > 8 Then
                    llTo = CLng(Left(strHelp, 8))
                Else
                    llTo = 99999999
                End If
                If llHelp > llBestFit And llTo >= lReferenceDate Then
                    llBestFit = llHelp
                    GetBestFitDutyGroup = GetItem(olEmployee.ItemDutyGroup(i), 2, "@")
                End If
            End If
        End If
    Next i
    If GetBestFitDutyGroup = "" Then
        If olEmployee.CountDutyGroup > 0 Then 'we didn't find a fitting type in the past, so let's take the one in the future
            strHelp = GetItem(olEmployee.ItemDutyGroup(1), 3, "@")
            If Len(strHelp) > 8 Then
                llTo = CLng(Left(strHelp, 8))
            Else
                llTo = 99999999
            End If
            If llTo >= lReferenceDate Then
                GetBestFitDutyGroup = GetItem(olEmployee.ItemDutyGroup(1), 2, "@")
            End If
        End If
    End If
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.GetBestFitDutyGroup", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.GetBestFitDutyGroup", Err
    Err.Clear
End Function
Private Function GetBestFitOrgUnit(stftabUrno As String, lReferenceDate As Long) As String
    On Error GoTo ErrHdl
    Dim llBestFit As Long
    Dim llHelp As Long
    Dim llTo As Long
    Dim i As Integer
    Dim strHelp As String
    Dim olEmployee As clsEmployee
    If stftabUrno = "" Then
        Exit Function
    End If
    Set olEmployee = gcEmployee(stftabUrno)
    llBestFit = 0
    For i = 1 To olEmployee.CountOrgUnit
        'testing the VAFR (valid from)
        strHelp = GetItem(olEmployee.ItemOrgUnit(i), 1, "@")
        If Len(strHelp) > 8 Then
            llHelp = CLng(Left(strHelp, 8))
            If llHelp <= lReferenceDate Then
                'testing the VATO (valid to)
                strHelp = GetItem(olEmployee.ItemOrgUnit(i), 3, "@")
                If Len(strHelp) > 8 Then
                    llTo = CLng(Left(strHelp, 8))
                Else
                    llTo = 99999999
                End If
                If llHelp > llBestFit And llTo >= lReferenceDate Then
                    llBestFit = llHelp
                    GetBestFitOrgUnit = GetItem(olEmployee.ItemOrgUnit(i), 2, "@")
                End If
            End If
        End If
    Next i
    If GetBestFitOrgUnit = "" Then
        If olEmployee.CountOrgUnit > 0 Then 'we didn't find a fitting type in the past, so let's take the one in the future
            strHelp = GetItem(olEmployee.ItemOrgUnit(1), 3, "@")
            If Len(strHelp) > 8 Then
                llTo = CLng(Left(strHelp, 8))
            Else
                llTo = 99999999
            End If
            If llTo >= lReferenceDate Then
                GetBestFitOrgUnit = GetItem(olEmployee.ItemOrgUnit(1), 2, "@")
            End If
        End If
    End If
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.GetBestFitOrgUnit", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.GetBestFitOrgUnit", Err
    Err.Clear
End Function
Private Function GetBestFitContractType(stftabUrno As String, lReferenceDate As Long) As String
    On Error GoTo ErrHdl
    Dim llBestFit As Long
    Dim llHelp As Long
    Dim llTo As Long
    Dim i As Integer
    Dim strHelp As String
    Dim olEmployee As clsEmployee
    If stftabUrno = "" Then
        Exit Function
    End If
    Set olEmployee = gcEmployee(stftabUrno)
    llBestFit = 0
    For i = 1 To olEmployee.CountContractType
        'testing the VAFR (valid from)
        strHelp = GetItem(olEmployee.ItemContractType(i), 1, "@")
        If Len(strHelp) > 8 Then
            llHelp = CLng(Left(strHelp, 8))
            If llHelp <= lReferenceDate Then
                'testing the VATO (valid to)
                strHelp = GetItem(olEmployee.ItemContractType(i), 3, "@")
                If Len(strHelp) > 8 Then
                    llTo = CLng(Left(strHelp, 8))
                Else
                    llTo = 99999999
                End If
                If llHelp > llBestFit And llTo >= lReferenceDate Then
                    llBestFit = llHelp
                    GetBestFitContractType = GetItem(olEmployee.ItemContractType(i), 2, "@")
                End If
            End If
        End If
    Next i
    If GetBestFitContractType = "" Then
        If olEmployee.CountContractType > 0 Then 'we didn't find a fitting type in the past, so let's take the one in the future
            strHelp = GetItem(olEmployee.ItemContractType(1), 3, "@")
            If Len(strHelp) > 8 Then
                llTo = CLng(Left(strHelp, 8))
            Else
                llTo = 99999999
            End If
            If llTo >= lReferenceDate Then
                GetBestFitContractType = GetItem(olEmployee.ItemContractType(1), 2, "@")
            End If
        End If
    End If
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.GetBestFitContractType", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.GetBestFitContractType", Err
    Err.Clear
End Function
Private Function GetBestFitFunction(stftabUrno As String, lReferenceDate As Long) As String
    On Error GoTo ErrHdl
    Dim llBestFit As Long
    Dim llHelp As Long
    Dim llTo As Long
    Dim i As Integer
    Dim strHelp As String
    Dim olEmployee As clsEmployee
    If stftabUrno = "" Then
        Exit Function
    End If
    Set olEmployee = gcEmployee(stftabUrno)
    llBestFit = 0
    For i = 1 To olEmployee.CountFunction
        'testing the VAFR (valid from)
        strHelp = GetItem(olEmployee.ItemFunction(i), 1, "@")
        If Len(strHelp) > 8 Then
            llHelp = CLng(Left(strHelp, 8))
            If llHelp <= lReferenceDate Then
                'testing the VATO (valid to)
                strHelp = GetItem(olEmployee.ItemFunction(i), 3, "@")
                If Len(strHelp) > 8 Then
                    llTo = CLng(Left(strHelp, 8))
                Else
                    llTo = 99999999
                End If
                If llHelp > llBestFit And llTo >= lReferenceDate Then
                    llBestFit = llHelp
                    GetBestFitFunction = GetItem(olEmployee.ItemFunction(i), 2, "@")
                End If
            End If
        End If
    Next i
    If GetBestFitFunction = "" Then
        If olEmployee.CountFunction > 0 Then 'we didn't find a fitting type in the past, so let's take the one in the future
            strHelp = GetItem(olEmployee.ItemFunction(1), 3, "@")
            If Len(strHelp) > 8 Then
                llTo = CLng(Left(strHelp, 8))
            Else
                llTo = 99999999
            End If
            If llTo >= lReferenceDate Then
                GetBestFitFunction = GetItem(olEmployee.ItemFunction(1), 2, "@")
            End If
        End If
    End If
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.GetBestFitFunction", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.GetBestFitFunction", Err
    Err.Clear
End Function
Private Sub FindPeople()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim ilYearNow As Integer
    Dim olView As clsView
    Dim olEmployee As clsEmployee
    Dim strKey As String
    Dim strOrgUnit As String
    Dim strFunction As String
    Dim strDutyGroup As String
    Dim strContractType As String
    Dim olElement
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    For Each olElement In gcViews
        If olElement.ViewName = olDocument.omViewName Then
            Set olView = olElement
            Exit For
        End If
    Next
    If olView Is Nothing Then Exit Sub

    'reset all collection
    For i = 1 To colEmployeeUrnos.Count
        colEmployeeUrnos.Remove 1
    Next i
    For i = 1 To olDocument.colDutyGroup.Count
        olDocument.colDutyGroup.Remove 1
    Next i
    For i = 1 To olDocument.colOrgUnit.Count
        olDocument.colOrgUnit.Remove 1
    Next i
    For i = 1 To olDocument.colContractType.Count
        olDocument.colContractType.Remove 1
    Next i
    For i = 1 To olDocument.colFunction.Count
        olDocument.colFunction.Remove 1
    Next i

    ilYearNow = Year(Now)
    
    'init the collections
    FillDayColumnCollection (olView.ViewYear)   'inits the "lookup-collection"

    ' filling the collection with the view's information for fast lookup
    For i = 1 To olView.CountOrgUnit
        strKey = Trim(olView.ItemOrgUnit(i))
        If DoesKeyExist(olDocument.colOrgUnit, strKey) <> True Then
            olDocument.colOrgUnit.Add key:=strKey, Item:=strKey
        End If
    Next i

    For i = 1 To olView.CountDutyGroup
        strKey = Trim(GetItem(olView.ItemDutyGroup(i), 1, ","))
        If DoesKeyExist(olDocument.colDutyGroup, strKey) <> True Then
            olDocument.colDutyGroup.Add key:=strKey, Item:=strKey
        End If
    Next i

    For i = 1 To olView.CountFunctionEmployee
        strKey = Trim(olView.ItemFunctionEmployee(i))
        If DoesKeyExist(olDocument.colFunction, strKey) <> True Then
            olDocument.colFunction.Add key:=strKey, Item:=strKey
        End If
    Next i

    For i = 1 To olView.CountContractType
        strKey = Trim(olView.ItemContractType(i))
        If DoesKeyExist(olDocument.colContractType, strKey) <> True Then
            olDocument.colContractType.Add key:=strKey, Item:=strKey
        End If
    Next i

    ' now let's check all employees if they are belonging to the view
    Dim strExitDate As String
    Dim llExitDate As Long
    Dim llReferenceDate As Long
    Dim llBestFit As Long
    Dim llHelp As Long
    Dim blBelongsToView As Boolean
    Dim strCompare As String

    For i = 1 To gcEmployee.Count
        Set olEmployee = gcEmployee(i)

        If olView.ViewYear < ilYearNow Then     'we take the 31.12. as reference-date for memberships
            llReferenceDate = (CLng(olView.ViewYear) * 10000) + 1231
        ElseIf olView.ViewYear > ilYearNow Then 'we take the 1.1. as reference-date for memberships
            llReferenceDate = (CLng(olView.ViewYear) * 10000) + 101
        Else                                    'we take the actual date as reference for memberships
            llReferenceDate = (CLng(olView.ViewYear) * 10000) + (100 * Month(Now)) + Day(Now)
        End If
'If olEmployee.EmployeePENO = 744625 Then ' 209524443 Or olEmployee.EmployeeURNO = 29461793 Then
'    MsgBox "Hallo"
'End If
        'is he still employed?
        strExitDate = olEmployee.EmployeeExitDate
        If Len(strExitDate) < 8 Or IsNumeric(strExitDate) = False Then
            llExitDate = "99999999"
        Else
            llExitDate = CLng(Left(strExitDate, 8))
        End If
        
        If llReferenceDate <= llExitDate Then
            blBelongsToView = True
            If olView.DontCareOrgUnit <> True Then
                strCompare = GetBestFitOrgUnit(olEmployee.EmployeeURNO, llReferenceDate)
                If DoesKeyExist(olDocument.colOrgUnit, strCompare) <> False Then
                    blBelongsToView = True
                Else
                    blBelongsToView = False
                End If
            End If
            If blBelongsToView <> False Then
                If olView.DontCareDutyGroup <> True Then
                    strCompare = GetBestFitDutyGroup(olEmployee.EmployeeURNO, llReferenceDate)
                    If DoesKeyExist(olDocument.colDutyGroup, strCompare) <> False Then
                        blBelongsToView = True
                    Else
                        blBelongsToView = False
                    End If
                End If
            End If
            If blBelongsToView <> False Then
                If olView.DontCareContract <> True Then
                    strCompare = GetBestFitContractType(olEmployee.EmployeeURNO, llReferenceDate)
                    If DoesKeyExist(olDocument.colContractType, strCompare) <> False Then
                        blBelongsToView = True
                    Else
                        blBelongsToView = False
                    End If
                End If
            End If
            If blBelongsToView <> False Then
                If olView.DontCareFunction <> True Then
                    strCompare = GetBestFitFunction(olEmployee.EmployeeURNO, llReferenceDate)
                    If DoesKeyExist(olDocument.colFunction, strCompare) <> False Then
                        blBelongsToView = True
                    Else
                        blBelongsToView = False
                    End If
                End If
            End If
        Else
            blBelongsToView = False
        End If

        If blBelongsToView = True Then
            colEmployeeUrnos.Add Item:=olEmployee.EmployeeURNO, key:=CStr(olEmployee.EmployeeURNO)
        End If
    Next i

    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.FindPeople", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.FindPeople", Err
    Err.Clear
    Resume Next
End Sub


Private Sub InitTabs()
    On Error GoTo ErrHdl
    TabAbsences.HeaderLengthString = "80,80,80"
    TabAbsences.HeaderString = "DATE,ABSE,WEEK"
    TabAbsences.ColumnWidthString = "6,20,2"
    TabAbsences.EnableHeaderSizing True
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.InitTabs", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.InitTabs", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   20.04.2001
' .
' .description  :   + delivers the line-string of one employee for the main-TAB
' .                 + sets the collections for the inf.
' .                 if we want to get the line-string after a BC, we mustn't count
' .                 the TTL... and HRS... ==> bCount=false
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Function GetLineString(stftabUrno As String, strYear As String, bCount As Boolean) As String
    On Error GoTo ErrHdl
    Dim strLineNo As String
    Dim strLine As String

    Dim strHelp As String

    Dim strDate As String
    Dim strAbs As String
    Dim strCol As String
    Dim strKey As String

    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim ilCount As Integer
    Dim ilCountTTL As Integer

    Dim llMinDate As Long
    Dim llMaxDate As Long
    Dim llActDate As Long

    Dim sHours As Single

    Dim olEmployee As clsEmployee
    Set olEmployee = gcEmployee(stftabUrno)

    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    TabAbsences.ResetContent
'    If stftabUrno = "35858713" Then
'        MsgBox "hallo"
'    End If
'    If olEmployee.EmployeePENO = "739177" Then
'        MsgBox "hallo"
'    End If

    ' Get all absences of the employee
    For i = 1 To olEmployee.CountEmployeeAbsences
        strHelp = olEmployee.ItemEmployeeAbsences(i)
        strDate = Trim(GetItem(strHelp, 2, "@"))
        strAbs = Trim(GetItem(strHelp, 1, "@"))
        If DoesKeyExist(olDocument.colAbsType, strAbs) = True Then
            If DoesKeyExist(olDocument.colDateColumn, strDate) = True Then
                strCol = olDocument.colDateColumn.Item(strDate)
                TabAbsences.InsertTextLine strDate + "," + strAbs + "," + strCol, True
                'frmCalc.Show
            End If
        End If
    Next i

    ' Now we've got all absences of the employee. So let's look after the weeks they are in
    For i = 1 To 53 Step 1
        Dim colWeeklyAbsences As New Collection
        For j = 1 To colWeeklyAbsences.Count
            colWeeklyAbsences.Remove 1
        Next j

        strLineNo = TabAbsences.GetLinesByColumnValue(2, i, 0)
        If strLineNo <> "" Then
            ' get the WeeklyHours of the employee for this week
            Dim ilHours As Integer
            If bCount = True Then
                If olEmployee.CountEmployeeWeeklyWorkingHours > 0 Then
                    If olEmployee.CountEmployeeWeeklyWorkingHours > 1 Then
                        ' get the valid value for that week
                        Dim llThisColumnDate As Long
                        Dim llHelp As Long
                        Dim llBestFit As Long
                        llThisColumnDate = olDocument.colColumnDate(CStr(i))
                        For j = 1 To olEmployee.CountEmployeeWeeklyWorkingHours
                            strHelp = GetItem(olEmployee.ItemEmployeeWeeklyWorkingHours(j), 1, "@")
                            If IsNumeric(strHelp) <> False Then
                                llHelp = CLng(Left(strHelp, 8))
                                If llHelp <= llThisColumnDate Then
                                    If llHelp > llBestFit Then
                                        llBestFit = llHelp
                                        ilHours = Left(GetItem(olEmployee.ItemEmployeeWeeklyWorkingHours(j), 2, "@"), 2)
                                    End If
                                End If
                            End If
                        Next j
                    Else
                        strHelp = GetItem(olEmployee.ItemEmployeeWeeklyWorkingHours(1), 2, "@")
                        If Len(strHelp) > 1 Then
                            ilHours = Left(strHelp, 2)
                        Else
                            ilHours = 40
                        End If
                    End If
                Else
                    ilHours = 40
                End If
            End If

            ' look after the absences of the employee of this week
            For j = 1 To ItemCount(strLineNo, ",") Step 1
                strAbs = TabAbsences.GetColumnValue(GetItem(strLineNo, j, ","), 1)

                ' look after the weekly absences of this employee
                If DoesKeyExist(colWeeklyAbsences, strAbs) = True Then
                    ilCount = colWeeklyAbsences.Item(strAbs) + 1
                    colWeeklyAbsences.Remove (strAbs)
                    colWeeklyAbsences.Add Item:=ilCount, key:=strAbs
                Else
                    colWeeklyAbsences.Add Item:=1, key:=strAbs
                End If

                'look after the total weekly absences
                strKey = CStr(i)
                If DoesKeyExist(olDocument.colTotalAbsCountWeekly, strKey) <> False Then
                    sHours = CStr(1 + olDocument.colTotalAbsCountWeekly(strKey))
                    olDocument.colTotalAbsCountWeekly.Remove (strKey)
                    olDocument.colTotalAbsCountWeekly.Add Item:=sHours, key:=strKey
                Else
                    olDocument.colTotalAbsCountWeekly.Add Item:="1", key:=strKey
                End If

                ' count the hours
                If bCount = True Then
                    'don't count absences with "regul�r arbeitsfrei" (e.g. OFF)
                    If DoesKeyExist(gcNoHrsCalculationAbsences, strAbs) <> True Then
                        sHours = 0.2 * ilHours
                        If DoesKeyExist(olDocument.colHrsAbsCol, strKey) <> False Then
                            sHours = sHours + olDocument.colHrsAbsCol(strKey)
                            olDocument.colHrsAbsCol.Remove (strKey)
                            olDocument.colHrsAbsCol.Add Item:=sHours, key:=strKey
                        Else
                            olDocument.colHrsAbsCol.Add Item:=sHours, key:=strKey
                        End If
                    End If
                End If
                'End If
            Next j
            If colWeeklyAbsences.Count > 1 Or frmHiddenServerConnection.strALWAYS_NUMBERS = "TRUE" Then
                Dim ilWeeklyAbsences As Integer
                Dim ilCountResult As Integer
                ilCountResult = 0
                For ilWeeklyAbsences = 1 To colWeeklyAbsences.Count
                    ilCountResult = ilCountResult + colWeeklyAbsences.Item(ilWeeklyAbsences)
                Next
                GetLineString = GetLineString + CStr(ilCountResult) + ","
            ElseIf colWeeklyAbsences.Count > 0 Then
                If colWeeklyAbsences.Item(1) > 4 Then
                    GetLineString = GetLineString + UCase(gcAbsenceCodeLookup.Item(strAbs)) + ","
                Else
                    GetLineString = GetLineString + gcAbsenceCodeLookup.Item(strAbs) + ","
                End If
            Else
                GetLineString = GetLineString + ","
            End If

            ' look after the weekly absences of the VIEW
            If bCount = True Then
                strKey = CStr(i)
                If DoesKeyExist(olDocument.colTTLHeadsAbsent, strKey) = True Then
                    ilCountTTL = olDocument.colTTLHeadsAbsent.Item(strKey) + 1
                    olDocument.colTTLHeadsAbsent.Remove (strKey)
                    olDocument.colTTLHeadsAbsent.Add Item:=ilCountTTL, key:=strKey
                Else
                    olDocument.colTTLHeadsAbsent.Add Item:=1, key:=strKey
                End If
                'End If
            End If
        Else
            GetLineString = GetLineString + ","
        End If
    Next i

    ' last but not least we have to look after the balance information
    Dim strHlpHoliday As String
    'Dim strHlpZIF As String
    Dim ilInStr As Integer
    For i = 1 To olEmployee.CountBalanceHoliday
        strHelp = olEmployee.ItemBalanceHoliday(i)
        If GetItem(strHelp, 1, "@") = strYear Then
            strHlpHoliday = GetItem(strHelp, 2, "@")
            Exit For
        End If
    Next i
'    For i = 1 To olEmployee.CountBalanceZIF
'        strHelp = olEmployee.ItemBalanceZIF(i)
'        If GetItem(strHelp, 1, "@") = strYear Then
'            strHlpZIF = GetItem(strHelp, 2, "@")
'            Exit For
'        End If
'    Next i

    If strHlpHoliday <> "" Then
        ilInStr = InStr(1, strHlpHoliday, ",", vbBinaryCompare)
        If ilInStr > 0 Then
            strHlpHoliday = Left(strHlpHoliday, ilInStr - 1) & "." & Right(strHlpHoliday, Len(strHlpHoliday) - ilInStr)
        End If
        GetLineString = GetLineString + strHlpHoliday + ","
    Else
        GetLineString = GetLineString + ","
    End If
'    If strHlpZIF <> "" Then
'        ilInStr = InStr(1, strHlpZIF, ",", vbBinaryCompare)
'        If ilInStr > 0 Then
'            strHlpZIF = Left(strHlpZIF, ilInStr - 1) & "." & Right(strHlpZIF, Len(strHlpZIF) - ilInStr)
'        End If
'        GetLineString = GetLineString + strHlpZIF + ","
'    Else
        GetLineString = GetLineString + ","
'    End If
    Exit Function
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.GetLineString", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.GetLineString", Err
    Err.Clear
    Resume Next
End Function

Private Sub FillDayColumnCollection(iYear As Integer)
    On Error GoTo ErrHdl
    Dim strWeeks As String
    Dim strMondays As String
    Dim strSundays As String

    Dim i As Integer
    Dim j As Integer

    Dim ilStartDay As Integer
    Dim ilEndDay As Integer
    Dim strYear As String
    Dim strMonth As String
    Dim strMonthYear As String
    Dim ilColumns As Integer
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    While olDocument.colDateColumn.Count > 0
        olDocument.colDateColumn.Remove 1
    Wend
    While olDocument.colColumnDate.Count > 0
        olDocument.colColumnDate.Remove 1
    Wend

    strMondays = olDocument.TabHeader.GetLineValues(1)
    strSundays = olDocument.TabHeader.GetLineValues(2)

    ilColumns = ItemCount(strMondays, ",") - 1

    For i = 1 To ilColumns
        ilStartDay = CInt(GetItem(strMondays, i, ","))
        ilEndDay = CInt(GetItem(strSundays, i, ","))
        strMonthYear = GetItem(ogMonthString, i, ",")
        strMonth = GetItem(strMonthYear, 1, "/")
        strYear = GetItem(strMonthYear, 2, "/")

        If ilEndDay < ilStartDay Then
            ' look after the first column
            If i = 1 Then
                olDocument.colColumnDate.Add Item:=CStr(iYear - 1) + "12" + Format(ilStartDay, "00"), key:=CStr(i)
                While ilStartDay < 32
                    olDocument.colDateColumn.Add Item:=i, key:=CStr(iYear - 1) + "12" + Format(ilStartDay, "00")
                    ilStartDay = ilStartDay + 1
                Wend
                ilStartDay = 0
                While ilStartDay < ilEndDay
                    ilStartDay = ilStartDay + 1
                    olDocument.colDateColumn.Add Item:=i, key:=CStr(iYear) & "01" & Format(ilStartDay, "00")
                Wend

            ' look after the last column
            ElseIf i = ilColumns Then
                olDocument.colColumnDate.Add Item:=strYear + "12" + Format(ilStartDay, "00"), key:=CStr(i)
                While ilStartDay < 32
                    olDocument.colDateColumn.Add Item:=i, key:=strYear + "12" + Format(ilStartDay, "00")
                    ilStartDay = ilStartDay + 1
                Wend
                ilStartDay = 0
                While ilStartDay < ilEndDay
                    ilStartDay = ilStartDay + 1
                    olDocument.colDateColumn.Add Item:=i, key:=CStr(iYear + 1) + "01" + Format(ilStartDay, "00")
                Wend

            ' look after the columns in the middle
            Else
                If ilEndDay > 3 Then
                    olDocument.colColumnDate.Add Item:=strYear + Format(strMonth - 1, "00") + Format(ilStartDay, "00"), key:=CStr(i)
                    While ilStartDay < 32
                        olDocument.colDateColumn.Add Item:=i, key:=strYear + Format(strMonth - 1, "00") + Format(ilStartDay, "00")
                        ilStartDay = ilStartDay + 1
                    Wend
                Else
                    olDocument.colColumnDate.Add Item:=strYear + strMonth + Format(ilStartDay, "00"), key:=CStr(i)
                    While ilStartDay < 32
                        olDocument.colDateColumn.Add Item:=i, key:=strYear + strMonth + Format(ilStartDay, "00")
                        ilStartDay = ilStartDay + 1
                    Wend
                End If
                ilStartDay = 0
                If ilEndDay > 3 Then
                    While ilStartDay < ilEndDay
                        ilStartDay = ilStartDay + 1
                        olDocument.colDateColumn.Add Item:=i, key:=strYear + strMonth + Format(ilStartDay, "00")
                    Wend
                Else
                    While ilStartDay < ilEndDay
                        ilStartDay = ilStartDay + 1
                        olDocument.colDateColumn.Add Item:=i, key:=strYear + Format(strMonth + 1, "00") + Format(ilStartDay, "00")
                    Wend
                End If
            End If

        Else
            olDocument.colColumnDate.Add Item:=strYear + strMonth + Format(ilStartDay, "00"), key:=CStr(i)
            While ilStartDay <= ilEndDay
                olDocument.colDateColumn.Add Item:=i, key:=strYear + strMonth + Format(ilStartDay, "00")
                ilStartDay = ilStartDay + 1
            Wend
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmCalc.FillDayColumnCollection", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmCalc.FillDayColumnCollection", Err
    Err.Clear
End Sub
