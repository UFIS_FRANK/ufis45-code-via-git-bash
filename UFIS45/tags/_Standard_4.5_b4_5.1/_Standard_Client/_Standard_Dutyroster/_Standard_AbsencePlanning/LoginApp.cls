VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LoginApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private WithEvents LogApp As AATLoginControl
Attribute LogApp.VB_VarHelpID = -1

'- - - - - - - - - - - - - - - - - - - - - - -
'- methods
'- - - - - - - - - - - - - - - - - - - - - - -
Public Sub Init()
    Set LogApp = New ULoginInterface
End Sub

'- - - - - - - - - - - - - - - - - - - - - - -
'- events
'- - - - - - - - - - - - - - - - - - - - - - -
Private Sub LogApp_LoginResult()
    'Module1.strRetLogin = strLoginResult
End Sub
