VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmHiddenServerConnection 
   Caption         =   "Form1"
   ClientHeight    =   12450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13950
   LinkTopic       =   "Form1"
   ScaleHeight     =   12450
   ScaleWidth      =   13950
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin TABLib.TAB TabMAW 
      Height          =   1215
      Left            =   10200
      TabIndex        =   36
      Top             =   2880
      Width           =   2895
      _Version        =   65536
      _ExtentX        =   5106
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB TabMAA 
      Height          =   1215
      Left            =   10080
      TabIndex        =   34
      Top             =   1080
      Width           =   2895
      _Version        =   65536
      _ExtentX        =   5106
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB TabHOL 
      Height          =   735
      Left            =   5520
      TabIndex        =   32
      Top             =   11520
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   1296
      _StockProps     =   64
   End
   Begin TABLib.TAB TabACC 
      Height          =   1095
      Left            =   240
      TabIndex        =   30
      Top             =   10680
      Width           =   3615
      _Version        =   65536
      _ExtentX        =   6376
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB TabVCD 
      Height          =   1095
      Left            =   5520
      TabIndex        =   28
      Top             =   9960
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSCO 
      Height          =   1455
      Left            =   240
      TabIndex        =   27
      Top             =   8640
      Width           =   3615
      _Version        =   65536
      _ExtentX        =   6376
      _ExtentY        =   2566
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSOR 
      Height          =   1215
      Left            =   5520
      TabIndex        =   23
      Top             =   6480
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB TabODA 
      Height          =   1455
      Left            =   5520
      TabIndex        =   18
      Top             =   4560
      Width           =   3735
      _Version        =   65536
      _ExtentX        =   6588
      _ExtentY        =   2566
      _StockProps     =   64
   End
   Begin TABLib.TAB TabSTF 
      Height          =   1455
      Left            =   5520
      TabIndex        =   17
      Top             =   2760
      Width           =   3735
      _Version        =   65536
      _ExtentX        =   6588
      _ExtentY        =   2566
      _StockProps     =   64
   End
   Begin TABLib.TAB TabDRR 
      Height          =   1335
      Left            =   5520
      TabIndex        =   16
      Top             =   1080
      Width           =   3735
      _Version        =   65536
      _ExtentX        =   6588
      _ExtentY        =   2355
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCOT 
      Height          =   1695
      Left            =   240
      TabIndex        =   14
      Top             =   6600
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   2990
      _StockProps     =   64
   End
   Begin TABLib.TAB TabODG 
      Height          =   1455
      Left            =   240
      TabIndex        =   10
      Top             =   2760
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   2566
      _StockProps     =   64
   End
   Begin TABLib.TAB TabPFC 
      Height          =   1695
      Left            =   240
      TabIndex        =   9
      Top             =   4560
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   2990
      _StockProps     =   64
   End
   Begin TABLib.TAB TabORG 
      Height          =   1335
      Left            =   240
      TabIndex        =   8
      Top             =   1080
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   2355
      _StockProps     =   64
   End
   Begin VB.TextBox txtHostType 
      Height          =   375
      Left            =   5640
      TabIndex        =   6
      Text            =   "Text1"
      Top             =   360
      Width           =   1455
   End
   Begin VB.TextBox txtHostName 
      Height          =   375
      Left            =   3720
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   360
      Width           =   1695
   End
   Begin VB.TextBox txtTblExt 
      Height          =   375
      Left            =   2040
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   360
      Width           =   1455
   End
   Begin VB.TextBox txtHOPO 
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   360
      Width           =   1695
   End
   Begin TABLib.TAB TabSPF 
      Height          =   1215
      Left            =   5520
      TabIndex        =   25
      Top             =   8160
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin VB.Label Label19 
      Caption         =   "MAWTAB"
      Height          =   255
      Left            =   10200
      TabIndex        =   37
      Top             =   2520
      Width           =   735
   End
   Begin VB.Label Label18 
      Caption         =   "MAATAB"
      Height          =   255
      Left            =   10080
      TabIndex        =   35
      Top             =   720
      Width           =   735
   End
   Begin VB.Label Label17 
      Caption         =   "HOLTAB"
      Height          =   255
      Left            =   5520
      TabIndex        =   33
      Top             =   11160
      Width           =   735
   End
   Begin VB.Label Label16 
      Caption         =   "ACCTAB"
      Height          =   255
      Left            =   240
      TabIndex        =   31
      Top             =   10320
      Width           =   735
   End
   Begin VB.Label Label15 
      Caption         =   "VCDTAB"
      Height          =   255
      Left            =   5520
      TabIndex        =   29
      Top             =   9600
      Width           =   735
   End
   Begin VB.Label Label14 
      Caption         =   "SCOTAB"
      Height          =   255
      Left            =   240
      TabIndex        =   26
      Top             =   8400
      Width           =   735
   End
   Begin VB.Label Label13 
      Caption         =   "SPFTAB"
      Height          =   255
      Left            =   5520
      TabIndex        =   24
      Top             =   7920
      Width           =   735
   End
   Begin VB.Label Label12 
      Caption         =   "SORTAB"
      Height          =   255
      Left            =   5520
      TabIndex        =   22
      Top             =   6120
      Width           =   735
   End
   Begin VB.Label Label11 
      Caption         =   "ODATAB"
      Height          =   255
      Left            =   5520
      TabIndex        =   21
      Top             =   4320
      Width           =   735
   End
   Begin VB.Label Label10 
      Caption         =   "STFTAB"
      Height          =   255
      Left            =   5520
      TabIndex        =   20
      Top             =   2520
      Width           =   735
   End
   Begin VB.Label Label9 
      Caption         =   "DRRTAB"
      Height          =   255
      Left            =   5520
      TabIndex        =   19
      Top             =   840
      Width           =   735
   End
   Begin VB.Label Label8 
      Caption         =   "COTTAB"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   6360
      Width           =   1215
   End
   Begin VB.Label Label7 
      Caption         =   "PFCTAB"
      Height          =   255
      Left            =   360
      TabIndex        =   13
      Top             =   4320
      Width           =   615
   End
   Begin VB.Label Label6 
      Caption         =   "ODGTAB"
      Height          =   255
      Left            =   240
      TabIndex        =   12
      Top             =   2520
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "ORGTAB"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   840
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "Host Type:"
      Height          =   255
      Left            =   5640
      TabIndex        =   7
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "HostName:"
      Height          =   255
      Left            =   3720
      TabIndex        =   5
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Table Extension:"
      Height          =   255
      Left            =   2040
      TabIndex        =   3
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "HOPO:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frmHiddenServerConnection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private cmLoadedYears As New Collection
Private strOrgUnit As String
Private strUrnoList As String
Private bmInitTabDRR As Boolean
Private strAbsences As String
Public strALWAYS_NUMBERS As String
Public strSHOW_HEADS_PER_WEEK_LINE As String

Public blLocal As Boolean

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   08.06.2001
' .
' . description :   the entry-point for loading data at startup
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Form_Load()
    CedaIsConnected = False
    strOrgUnit = ""
    blLocal = False
    'blLocal = True
    bmInitTabDRR = False
    strAbsences = ""

    InitHiddenServer        'reading data from the ceda.ini

    ConnectToCeda           'connecting to ceda using frmsplash.UfisCom1

    If IsNumeric(frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup")) = False Then
        MsgBox "Please actualize the data in BDPS-SEC!" + vbCrLf + _
        "The application will terminate!", vbCritical, "BDPS-SEC not actualised"
        Module1.fMainForm.BC.UnRegisterBC
        Unload frmAbout
        Unload frmCalc
        Unload frmDetailWindow
        Unload frmDocument
        Unload frmHidden
        Unload frmOptions
        Unload frmSplash
        Unload frmHiddenServerConnection
        End
    End If

    LoadData                'load the necessary data from the DB

    InitViews               'fill the view-collection
    InitAbsenceCodes        'fill the gcAbsenceCodeLookup-collection
    InitEmployees           'fill the gcEmployees with data
    InitMaxAbsent           'fill the gcMaxAbsent with data
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   08.06.2001
' .
' . description :   load all employees, but remember: DRR-data is only being loaded
' .                 in dependancy of the selected view's year, not at startup
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub LoadData()
    If blLocal = True Then
        LoadLocal
    Else
        LoadSOR
        LoadSCO
        LoadORG
        LoadODG
        LoadPFC
        LoadCOT
        LoadSTF
        LoadODA
        LoadACC
        LoadSPF
        LoadHOL
        LoadMAW
        LoadMAA
    End If
    LoadVCD     'information only on the server yet
End Sub

Private Sub InitHiddenServer()
    txtHostName.Text = GetIniEntry("", "GLOBAL", "AbsPlan", "HOSTNAME", "")
    txtHostType.Text = GetIniEntry("", "GLOBAL", "AbsPlan", "HOSTTYPE", "UNKNOWN")
    txtTblExt.Text = GetIniEntry("", "GLOBAL", "AbsPlan", "TABLEEXTENSION", "TAB")
    txtHOPO.Text = GetIniEntry("", "GLOBAL", "AbsPlan", "HOMEAIRPORT", "")
    strALWAYS_NUMBERS = UCase(GetIniEntry("", "GLOBAL", "AbsPlan", "ALWAYS_NUMBERS", "FALSE"))
    strSHOW_HEADS_PER_WEEK_LINE = UCase(GetIniEntry("", "GLOBAL", "AbsPlan", "SHOW_HEADS_PER_WEEK_LINE", "FALSE"))
End Sub

Public Function ConnectToCeda() As Boolean
    Dim sServer As String
    Dim sHopo As String
    Dim sTableExt As String
    Dim sConnectType As String
    Dim ret As Integer
    If frmSplash.ProgressBar1.Value < frmSplash.ProgressBar1.Max Then
        frmSplash.List1.AddItem ("Connecting to CEDA ...")
        frmSplash.List1.Refresh
        frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    End If
    CedaIsConnected = False

    frmSplash.UfisCom1.CleanupCom

    sServer = txtHostName.Text
    sHopo = txtHOPO.Text
    sTableExt = txtTblExt.Text
    sConnectType = "CEDA"

    If ogActualUser = "" Then
        frmSplash.UfisCom1.SetCedaPerameters "AbsPlan", sHopo, sTableExt
    Else
        frmSplash.UfisCom1.SetCedaPerameters ogActualUser, sHopo, sTableExt
    End If

    If sServer <> "LOCAL" Then
        ret = frmSplash.UfisCom1.InitCom(sServer, sConnectType)
    End If
    If ret = 0 Then
        MsgBox "Connection to CEDA failed!", vbCritical, "No connection!"
        End
    Else
        CedaIsConnected = True
    End If

    ConnectToCeda = CedaIsConnected
End Function

Public Sub LoadSOR()
    frmSplash.List1.AddItem ("Loading SORTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1

    Dim strWhere As String
    Dim strTable As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llLoadedRecords As Long

    Dim strArr() As String

    ' init the TAB-control
    InitTabForCedaConnection TabSOR
    strTable = "SORTAB"
    strFieldlist = "SURN,CODE,ODGC,VPFR,VPTO,URNO"
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabSOR.HeaderLengthString = strHeaderLenStr
    TabSOR.HeaderString = strFieldlist
    TabSOR.ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L,L,L,L"
    TabSOR.ColumnWidthString = "14,14,14,14,14,14,14,14,14,14,14"

    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        ' load only the actual user
        strWhere = "WHERE SURN = '" & ogActualUserURNO & "'"
        TabSOR.CedaAction "RT", strTable, strFieldlist, "", strWhere
    Else
        'load the data year by year
        Dim ilYear As Integer
        ilYear = Year(Now)
        For i = 1 To 5
            Select Case i
                Case 1:
                    strWhere = "WHERE VPFR < '" & CStr(ilYear - 2) & "0101000000'"
                Case 2:
                    strWhere = "WHERE VPFR BETWEEN '" & CStr(ilYear - 2) & "0101000000' AND '" & CStr(ilYear - 2) & "1231235959'"
                Case 3:
                    strWhere = "WHERE VPFR BETWEEN '" & CStr(ilYear - 1) & "0101000000' AND '" & CStr(ilYear - 1) & "1231235959'"
                Case 4:
                    strWhere = "WHERE VPFR BETWEEN '" & CStr(ilYear) & "0101000000' AND '" & CStr(ilYear) & "1231235959'"
                Case Else '5
                    strWhere = "WHERE VPFR > '" & CStr(ilYear) & "1231235959'"
            End Select

            TabSOR.CedaAction "RT", strTable, strFieldlist, "", strWhere
        Next i
    End If
    llLoadedRecords = TabSOR.GetLineCount
    frmSplash.List1.AddItem (CStr(llLoadedRecords) & " records loaded.")
End Sub

Public Sub LoadORG()
    frmSplash.List1.AddItem ("Loading ORGTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim strArr() As String

    InitTabForCedaConnection TabORG
    strTable = "ORGTAB"
    strWhere = "WHERE URNO > '0'"
    strFieldlist = "DPT1,DPT2,DPTN,URNO"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabORG.HeaderLengthString = strHeaderLenStr
    TabORG.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabORG.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabORG.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadODG()
    frmSplash.List1.AddItem ("Loading ODGTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim strArr() As String

    InitTabForCedaConnection TabODG
    strTable = "ODGTAB"
    strWhere = "WHERE URNO > '0'"
    strFieldlist = "ODGC,ODGN,ORGU,URNO"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabODG.HeaderLengthString = strHeaderLenStr
    TabODG.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabODG.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabODG.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadPFC()
    frmSplash.List1.AddItem ("Loading PFCTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim strArr() As String

    InitTabForCedaConnection TabPFC
    strTable = "PFCTAB"
    strWhere = "WHERE URNO > '0'"
    strFieldlist = "DPTC,FCTC,FCTN,URNO"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabPFC.HeaderLengthString = strHeaderLenStr
    TabPFC.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabPFC.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabPFC.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadCOT()
    frmSplash.List1.AddItem ("Loading COTTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim strArr() As String

    InitTabForCedaConnection TabCOT
    strTable = "COTTAB"
    strWhere = "WHERE URNO > '0'"
    strFieldlist = "CTRC,CTRN,DPTC,REGI,URNO"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabCOT.HeaderLengthString = strHeaderLenStr
    TabCOT.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabCOT.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabCOT.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadDRR(iYear As Integer)
    ' we want to load the data only once!!!
    If DoesKeyExist(cmLoadedYears, CStr(iYear)) = False Then

        Dim strFieldlist As String ' the fields to load
        Dim strSdayMin As String ' the minimum day for the where-statement
        Dim strSdayMax As String ' the maximum day for the where-statement
        Dim strTable As String ' the table to load
        Dim strWhere As String ' the where-statement
        Dim tmpStr As String ' temporary string for building the buffer
        

        Dim llTabDrrLastLine As Long ' remember the last DRR-Line to init the new loaded absences
        Dim llRecords As Long ' getting the number of the delivered records
        Dim ilLoop As Long ' determing the loop-variables max-value
        Dim i As Long ' loop-variable
        Dim j As Long ' loop-variable

        Dim ilCountEmployees As Integer ' counting the employees we have to load
        Dim blLoad As Boolean ' indicator whether to load or not
        Dim iIdx As Integer

        cmLoadedYears.Add Item:=CStr(iYear), key:=CStr(iYear)
        strTable = "DRRTAB"
        strFieldlist = "SDAY,STFU,SCOD,BSDU,URNO,ROSS"

        ' init the TABDRR?
        If bmInitTabDRR = False Then
            Dim strHeaderLenStr As String
            Dim strArr() As String
            Dim ilItemCount As Integer
    
            TabDRR.ResetContent
            TabDRR.ShowHorzScroller True
            TabDRR.EnableHeaderSizing True
    
            'set headertext of the TAB
            strArr = Split(strFieldlist, ",")
            ilItemCount = UBound(strArr)
            For i = 0 To ilItemCount
                If i + 1 > ilItemCount Then
                    strHeaderLenStr = strHeaderLenStr & "120"
                Else
                    strHeaderLenStr = strHeaderLenStr & "120,"
                End If
            Next i
            TabDRR.HeaderLengthString = strHeaderLenStr
            TabDRR.HeaderString = strFieldlist
            
            'building a string containing all absences
            iIdx = GetItemNo(TabODA.HeaderString, "SDAC") - 1
            For i = 0 To TabODA.GetLineCount - 1
                strAbsences = strAbsences & "'" & TabODA.GetColumnValue(i, iIdx) & "',"
            Next i
            If Len(strAbsences) > 0 Then
                strAbsences = Left(strAbsences, Len(strAbsences) - 1)
            End If

            'init the tab for CADA-connection
            InitTabForCedaConnection TabDRR
            bmInitTabDRR = True
        End If

        ' remember the last DRR-Line to init the new loaded absences
        llTabDrrLastLine = TabDRR.GetLineCount

        If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
            ' load only the actual user
            cmLoadedYears.Add Item:=CStr(iYear + 1), key:=CStr(iYear + 1)
            strWhere = "WHERE STFU = '" & ogActualUserURNO & "' AND SDAY > '" & CStr(iYear - 1) & "1130' AND ROSS = 'A' AND SCOD IN (" & strAbsences & ")"
            TabDRR.CedaAction "RT", strTable, strFieldlist, "", strWhere
        Else
            ' we have to load 14 months, beginning with december of the last year,
            ' ending with the january of the following year
            ' first let's look how many employees there are
            ilCountEmployees = gcEmployee.Count
            Select Case ((ilCountEmployees - 500) \ 1000) 'DIV-operator
                Case 0: ' 0 - 1499 MAs
                    ilLoop = 14 ' load month by month
    
                Case 1: ' 1500 - 2499 MAs
                    ilLoop = 28 ' load "15 days"-packets
    
                Case 2: ' 2500 - 3499 MAs
                    ilLoop = 42 ' load "10 days"-packets
    
                Case Else: ' more than 3499 MAs
                    ilLoop = 84 ' load "5 days"-packets
            End Select
    
            'ConnectToCeda
            'frmSplash.UfisCom1.AccessMethod = "CEDA"
            For i = 1 To ilLoop Step 1
                ' we have to take care if we've loaded data from the year before or after iYear,
                ' because there are overlapping months
                Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1157) & CStr(ilLoop - i) & " parts left..."
                blLoad = True
    
                Select Case ilLoop
                    Case 14: ' load month by month
                        Select Case i
                            Case 1:
                                strSdayMin = CStr(iYear - 1) + "1201"
                                strSdayMax = CStr(iYear - 1) + "1231"
                                If DoesKeyExist(cmLoadedYears, CStr(iYear - 1)) = True Then
                                    blLoad = False
                                End If
                            Case 14:
                                strSdayMin = CStr(iYear + 1) + "0101"
                                strSdayMax = CStr(iYear + 1) + "0131"
                                If DoesKeyExist(cmLoadedYears, CStr(iYear + 1)) = True Then
                                    blLoad = False
                                End If
                            Case Else:
                                strSdayMin = CStr(iYear) + CStr(Format(i - 1, "00")) + "01"
                                strSdayMax = CStr(iYear) + CStr(Format(i - 1, "00")) + "31"
                        End Select
                    
                    Case 28: ' load "15 days"-packets
                        Select Case i
                            Case 1:
                                strSdayMin = CStr(iYear - 1) + "1201"
                                strSdayMax = CStr(iYear - 1) + "1215"
                            Case 2:
                                strSdayMin = CStr(iYear - 1) + "1216"
                                strSdayMax = CStr(iYear - 1) + "1231"
                            Case 27:
                                strSdayMin = CStr(iYear + 1) + "0101"
                                strSdayMax = CStr(iYear + 1) + "0115"
                            Case 28:
                                strSdayMin = CStr(iYear + 1) + "0116"
                                strSdayMax = CStr(iYear + 1) + "0131"
                            Case Else:
                                If ((i Mod 2) = 0) Then 'gerade => 2. Monatshälfte laden
                                    strSdayMin = CStr(iYear) + CStr(Format(((i \ 2) - 1), "00")) + "16"
                                    strSdayMax = CStr(iYear) + CStr(Format(((i \ 2) - 1), "00")) + "31"
                                Else ' ungerade => 1. Monatshälfte laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 2), "00")) + "01"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 2), "00")) + "15"
                                End If
                        End Select
                        If i < 5 Then 'did we load it already?
                            If DoesKeyExist(cmLoadedYears, CStr(iYear - 1)) = True Then
                                blLoad = False
                            End If
                        ElseIf i > 24 Then 'did we load it already?
                            If DoesKeyExist(cmLoadedYears, CStr(iYear + 1)) = True Then
                                blLoad = False
                            End If
                        End If
    
                    Case 42: ' load "10 days"-packets
                        Select Case i
                            Case 1:
                                strSdayMin = CStr(iYear - 1) + "1201"
                                strSdayMax = CStr(iYear - 1) + "1210"
                            Case 2:
                                strSdayMin = CStr(iYear - 1) + "1211"
                                strSdayMax = CStr(iYear - 1) + "1220"
                            Case 3:
                                strSdayMin = CStr(iYear - 1) + "1221"
                                strSdayMax = CStr(iYear - 1) + "1231"
                            Case 40:
                                strSdayMin = CStr(iYear + 1) + "0101"
                                strSdayMax = CStr(iYear + 1) + "0110"
                            Case 41:
                                strSdayMin = CStr(iYear + 1) + "0111"
                                strSdayMax = CStr(iYear + 1) + "0120"
                            Case 42:
                                strSdayMin = CStr(iYear + 1) + "0121"
                                strSdayMax = CStr(iYear + 1) + "0131"
                            Case Else:
                                If ((i Mod 3) = 0) Then ' 3. Monatsdrittel laden
                                    strSdayMin = CStr(iYear) + CStr(Format(((i \ 3) - 1), "00")) + "21"
                                    strSdayMax = CStr(iYear) + CStr(Format(((i \ 3) - 1), "00")) + "31"
                                ElseIf ((i Mod 3) = 2) Then ' 2. Monatsdrittel laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 3), "00")) + "11"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 3), "00")) + "20"
                                Else ' ((i Mod 3) = 1) => 1. Monatsdrittel laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 3), "00")) + "01"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 3), "00")) + "10"
                                End If
                        End Select
                        If i < 7 Then 'did we load it already?
                            If DoesKeyExist(cmLoadedYears, CStr(iYear - 1)) = True Then
                                blLoad = False
                            End If
                        ElseIf i > 36 Then 'did we load it already?
                            If DoesKeyExist(cmLoadedYears, CStr(iYear + 1)) = True Then
                                blLoad = False
                            End If
                        End If
    
                    Case 84: ' load "5 days"-packets
                        Select Case i
                            Case 1:
                                strSdayMin = CStr(iYear - 1) + "1201"
                                strSdayMax = CStr(iYear - 1) + "1205"
                            Case 2:
                                strSdayMin = CStr(iYear - 1) + "1206"
                                strSdayMax = CStr(iYear - 1) + "1210"
                            Case 3:
                                strSdayMin = CStr(iYear - 1) + "1211"
                                strSdayMax = CStr(iYear - 1) + "1215"
                            Case 4:
                                strSdayMin = CStr(iYear - 1) + "1216"
                                strSdayMax = CStr(iYear - 1) + "1220"
                            Case 5:
                                strSdayMin = CStr(iYear - 1) + "1221"
                                strSdayMax = CStr(iYear - 1) + "1225"
                            Case 6:
                                strSdayMin = CStr(iYear - 1) + "1226"
                                strSdayMax = CStr(iYear - 1) + "1231"
                            Case 79:
                                strSdayMin = CStr(iYear + 1) + "0101"
                                strSdayMax = CStr(iYear + 1) + "0105"
                            Case 80:
                                strSdayMin = CStr(iYear + 1) + "0106"
                                strSdayMax = CStr(iYear + 1) + "0110"
                            Case 81:
                                strSdayMin = CStr(iYear + 1) + "0111"
                                strSdayMax = CStr(iYear + 1) + "0115"
                            Case 82:
                                strSdayMin = CStr(iYear + 1) + "0116"
                                strSdayMax = CStr(iYear + 1) + "0120"
                            Case 83:
                                strSdayMin = CStr(iYear + 1) + "0121"
                                strSdayMax = CStr(iYear + 1) + "0125"
                            Case 84:
                                strSdayMin = CStr(iYear + 1) + "0126"
                                strSdayMax = CStr(iYear + 1) + "0131"
                            Case Else:
                                If ((i Mod 6) = 0) Then ' 6. Monatssechstel laden
                                    strSdayMin = CStr(iYear) + CStr(Format(((i \ 6) - 1), "00")) + "26"
                                    strSdayMax = CStr(iYear) + CStr(Format(((i \ 6) - 1), "00")) + "31"
                                ElseIf ((i Mod 6) = 5) Then ' 5. Monatssechstel laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 6), "00")) + "21"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 6), "00")) + "25"
                                ElseIf ((i Mod 6) = 4) Then ' 4. Monatssechstel laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 6), "00")) + "16"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 6), "00")) + "20"
                                ElseIf ((i Mod 6) = 3) Then ' 3. Monatssechstel laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 6), "00")) + "11"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 6), "00")) + "15"
                                ElseIf ((i Mod 6) = 2) Then ' 2. Monatssechstel laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 6), "00")) + "06"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 6), "00")) + "10"
                                Else ' ((i Mod 6) = 1) => 1. Monatssechstel laden
                                    strSdayMin = CStr(iYear) + CStr(Format((i \ 6), "00")) + "01"
                                    strSdayMax = CStr(iYear) + CStr(Format((i \ 6), "00")) + "05"
                                End If
                        End Select
                        If i < 13 Then 'did we load it already?
                            If DoesKeyExist(cmLoadedYears, CStr(iYear - 1)) = True Then
                                blLoad = False
                            End If
                        ElseIf i > 72 Then 'did we load it already?
                            If DoesKeyExist(cmLoadedYears, CStr(iYear + 1)) = True Then
                                blLoad = False
                            End If
                        End If
                    
                    Case Else:
                End Select

                strWhere = "WHERE SDAY BETWEEN '" + strSdayMin + "' AND '" + strSdayMax + "' AND SCOD IN (" & strAbsences & ") AND ROSS = 'A'"

                If blLoad = True Then
                    ' Read from ceda and insert the buffer directly into the grid
                    TabDRR.CedaAction "RT", strTable, strFieldlist, "", strWhere
                End If
            Next i
        End If

        'add the absences to the employees
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1158)
        Dim strLine As String
        Dim strAbs As String
        Dim strKey As String
        Dim strSDAY As String
        Dim strItem As String
        For i = llTabDrrLastLine To TabDRR.GetLineCount
            strAbs = TabDRR.GetColumnValue(i, 2)            ' the absence code
            If strAbs <> "" Then
                strKey = TabDRR.GetColumnValue(i, 1)        ' the STFU
                strSDAY = TabDRR.GetColumnValue(i, 0)       ' the ShiftDay
                strItem = strAbs + "@" + strSDAY
                If DoesKeyExist(gcEmployee, strKey) <> False Then
                    gcEmployee(strKey).AddEmployeeAbsences strSDAY, strItem
                End If
            End If
        Next i
    End If
End Sub

Public Sub LoadSTF()
    frmSplash.List1.AddItem ("Loading STFTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim strArr() As String

    InitTabForCedaConnection TabSTF
    strTable = "STFTAB"
    strFieldlist = "FINM,LANM,DOEM,DODM,REGI,PENO,URNO,KIDS,DODM"

    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        strWhere = "WHERE URNO = '" & ogActualUserURNO & "'"
    Else
        strWhere = "WHERE URNO > '0'"
    End If

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabSTF.HeaderLengthString = strHeaderLenStr
    TabSTF.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabSTF.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabSTF.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadODA()
    frmSplash.List1.AddItem ("Loading ODATAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = 2

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim strArr() As String

    InitTabForCedaConnection TabODA
    strTable = "ODATAB"
    strWhere = "WHERE URNO > '0'"
    strFieldlist = "SDAC,SDAE,URNO,SDAA,SDAN,FREE"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabODA.HeaderLengthString = strHeaderLenStr
    TabODA.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabODA.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabODA.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadSPF()
    frmSplash.List1.AddItem ("Loading SPFTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 2

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim strArr() As String

    InitTabForCedaConnection TabSPF
    strTable = "SPFTAB"
    strFieldlist = "SURN,CODE,VPFR,VPTO,URNO"
    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        strWhere = "WHERE SURN = '" & ogActualUserURNO & "' AND (PRIO = '1' OR PRIO = ' ')"
    Else
        strWhere = "WHERE PRIO = '1' OR PRIO = ' '"
    End If

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabSPF.HeaderLengthString = strHeaderLenStr
    TabSPF.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabSPF.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabSPF.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadSCO()
    frmSplash.List1.AddItem ("Loading SCOTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1

    Dim tmpStr As String
    Dim strArr() As String
    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String

    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long

    InitTabForCedaConnection TabSCO
    strTable = "SCOTAB"
    strFieldlist = "SURN,CODE,VPFR,VPTO,CWEH,CPFC,URNO"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabSCO.HeaderLengthString = strHeaderLenStr
    TabSCO.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        strWhere = "WHERE SURN = '" & ogActualUserURNO & "'"

        ' Read from ceda and insert the buffer directly into the grid
        TabSCO.CedaAction "RT", strTable, strFieldlist, "", strWhere
    Else
        Dim ilYear As Integer
        ilYear = Year(Now)
        For i = 1 To 5
            Select Case i
                Case 1:
                    strWhere = "WHERE VPFR < '" & CStr(ilYear - 2) & "0101000000'"
                Case 2:
                    strWhere = "WHERE VPFR BETWEEN '" & CStr(ilYear - 2) & "0101000000' AND '" & CStr(ilYear - 2) & "1231235959'"
                Case 3:
                    strWhere = "WHERE VPFR BETWEEN '" & CStr(ilYear - 1) & "0101000000' AND '" & CStr(ilYear - 1) & "1231235959'"
                Case 4:
                    strWhere = "WHERE VPFR BETWEEN '" & CStr(ilYear) & "0101000000' AND '" & CStr(ilYear) & "1231235959'"
                Case Else '5
                    strWhere = "WHERE VPFR > '" & CStr(ilYear) & "1231235959'"
            End Select

            ' Read from ceda and insert the buffer directly into the grid
            TabSCO.CedaAction "RT", strTable, strFieldlist, "", strWhere
        Next i
    End If

    llRecords = TabSCO.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadACC()
    frmSplash.List1.AddItem ("Loading ACCTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 2

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long

    Dim strArr() As String

    InitTabForCedaConnection TabACC
    strTable = "ACCTAB"
    strFieldlist = "TYPE,STFU,CL12,YEAR,OP01,URNO,YECU,YELA"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabACC.HeaderLengthString = strHeaderLenStr
    TabACC.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        strWhere = "WHERE STFU = '" & ogActualUserURNO & "' AND (TYPE = '30' OR TYPE = '31')"

        ' Read from ceda and insert the buffer directly into the grid
        TabACC.CedaAction "RT", strTable, strFieldlist, "", strWhere
    Else
        Dim ilYear As Integer
        ilYear = Year(Now)
        For i = 1 To 5
            Select Case i
                Case 1:
                    strWhere = "WHERE YEAR < '" & CStr(ilYear - 2) & "' AND (TYPE = '30' OR TYPE = '31')"
                Case 2:
                    strWhere = "WHERE YEAR = '" & CStr(ilYear - 2) & "' AND (TYPE = '30' OR TYPE = '31')"
                Case 3:
                    strWhere = "WHERE YEAR = '" & CStr(ilYear - 1) & "' AND (TYPE = '30' OR TYPE = '31')"
                Case 4:
                    strWhere = "WHERE YEAR = '" & CStr(ilYear) & "' AND (TYPE = '30' OR TYPE = '31')"
                Case Else '5
                    strWhere = "WHERE YEAR > '" & CStr(ilYear) & "' AND (TYPE = '30' OR TYPE = '31')"
            End Select

            ' Read from ceda and insert the buffer directly into the grid
            TabACC.CedaAction "RT", strTable, strFieldlist, "", strWhere
        Next i
    End If

    llRecords = TabACC.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadVCD()
    frmSplash.List1.AddItem ("Loading VCDTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 2

    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String
    Dim strHeaderLenStr As String
    Dim i As Integer
    Dim ilItemCount As Integer
    Dim llRecords As Long

    Dim strArr() As String

    InitTabForCedaConnection TabVCD
    strTable = "VCDTAB"
    strFieldlist = "CKEY,CTYP,TEXT"
    strWhere = "WHERE APPN = 'AbsPlan' AND PKNO = '" + ogActualUser + "'"
    TabVCD.ColumnWidthString = "32,32,2000"
    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabVCD.HeaderLengthString = strHeaderLenStr
    TabVCD.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") = 1 Then
        ' the user can see all
        ' Read from ceda and insert the buffer directly into the grid
        TabVCD.CedaAction "RT", strTable, strFieldlist, "", strWhere
    Else
        ' the user can see only his own group
    End If
    llRecords = TabVCD.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadHOL()
    frmSplash.List1.AddItem ("Loading HOLTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    'frmSplash.List1.TopIndex = 8
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 2

    Dim strTable, strFieldlist, strWhere, strHeaderLenStr As String
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim i As Long
    Dim tmpStr As String

    Dim strArr() As String

    InitTabForCedaConnection TabHOL
    strTable = "HOLTAB"
    strFieldlist = "HDAY,TYPE,REMA,URNO"
    strWhere = "WHERE URNO > '0'"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabHOL.HeaderLengthString = strHeaderLenStr
    TabHOL.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabHOL.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabHOL.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub
Public Sub LoadMAW()
    frmSplash.List1.AddItem ("Loading MAWTAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 2

    Dim strTable, strFieldlist, strWhere, strHeaderLenStr As String
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim i As Long

    Dim strArr() As String

    InitTabForCedaConnection TabMAW
    strTable = "MAWTAB"
    strWhere = "WHERE URNO > '0'"
    strFieldlist = "URNO,NAME,DFLT"

    TabMAW.ColumnWidthString = "10,32,5"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabMAW.HeaderLengthString = strHeaderLenStr
    TabMAW.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabMAW.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabMAW.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub LoadMAA()
    frmSplash.List1.AddItem ("Loading MAATAB ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 2

    Dim strTable, strFieldlist, strWhere, strHeaderLenStr As String
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim i As Long
    Dim tmpStr As String

    Dim strArr() As String

    InitTabForCedaConnection TabMAA
    strTable = "MAATAB"
    strWhere = "WHERE URNO > '0'"
    strFieldlist = "URNO,MAWU,VALU,WEEK,YEAR"

    TabMAA.ColumnWidthString = "10,10,5,2,4"

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabMAA.HeaderLengthString = strHeaderLenStr
    TabMAA.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    TabMAA.CedaAction "RT", strTable, strFieldlist, "", strWhere

    llRecords = TabMAA.GetLineCount
    frmSplash.List1.AddItem (CStr(llRecords) & " records loaded.")
End Sub

Public Sub SaveView(SaveString As String)
    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String

    Dim strCTYP As String   '0-ViewGeneral, 1-OrgUnit, 2-DutyGroup, 3-Function, 4-Contract
    Dim strCKEY As String   'name of the view
    Dim strURNO As String   'URNO
    Dim strAPPN As String   'application name
    Dim strTEXT As String   'the data to write into the db
    Dim strData As String   'the ´complete data-string to write into the db

    Dim llRecords As Long
    Dim i As Integer

    strCKEY = GetItem(SaveString, 1, ";")
    strCTYP = GetItem(SaveString, 2, ";")
    strTEXT = GetItem(SaveString, 3, ";")

    strAPPN = "AbsPlan"
    strTable = "VCDTAB"
    strFieldlist = "URNO,TEXT"
    strWhere = "WHERE URNO > '0' AND APPN = '" + strAPPN + "' AND PKNO = '" + ogActualUser + _
        "' AND CTYP = '" + strCTYP + "' AND CKEY = '" + strCKEY + "'"

    ConnectToCeda
    frmSplash.UfisCom1.AccessMethod = "CEDA"
    If frmSplash.UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
        ' the view already has got an URNO, so just update
        If frmSplash.UfisCom1.GetBufferLine(0) <> "" Then
            strURNO = Trim(GetItem(frmSplash.UfisCom1.GetBufferLine(0), 1, ","))
            strFieldlist = "CKEY,CTYP,TEXT"
            strWhere = "WHERE URNO = '" + strURNO + "'"
            strData = strCKEY + "," + strCTYP + "," + strTEXT
            frmSplash.UfisCom1.CallServer "URT", strTable, strFieldlist, strData, strWhere, "360"
        ' the view hasn't got an URNO => make new entry in the db
        Else
            ' do we have any reserved URNOs in "ogURNO"?
            If ogURNO.Count < 1 Then
                strWhere = ""
                If frmSplash.UfisCom1.CallServer("GMU", strTable, "*", "18", "", "360") = 0 Then
                    strURNO = frmSplash.UfisCom1.GetBufferLine(0)
                    llRecords = ItemCount(strURNO, ",")
                    For i = 1 To llRecords
                        ogURNO.Add GetItem(strURNO, i, ",")
                    Next i
                End If
            End If
            strURNO = ogURNO.Item(1)
            ogURNO.Remove 1
            strFieldlist = "APPN,CKEY,CTYP,HOPO,PKNO,TEXT,URNO,VAFR,VATO"
            strWhere = ""
            strData = "AbsPlan," + strCKEY + "," + strCTYP + "," + _
                frmHiddenServerConnection.txtHOPO.Text + "," + ogActualUser + "," + _
                strTEXT + "," + strURNO + ",199901010000,205012310000"
            frmSplash.UfisCom1.CallServer "IRT", strTable, strFieldlist, strData, strWhere, "360"
        End If
    Else
        ' do we have any reserved URNOs in "ogURNO"?
        If ogURNO.Count < 1 Then
            If frmSplash.UfisCom1.CallServer("GMU", strTable, "*", "18", "", "360") = 0 Then
                llRecords = frmSplash.UfisCom1.GetBufferCount
                If llRecords > 1 Then
                    For i = 0 To llRecords - 1
                        ogURNO.Add GetItem(frmSplash.UfisCom1.GetBufferLine(0), 1, ",")
                    Next i
                Else
                    strURNO = frmSplash.UfisCom1.GetBufferLine(0)
                    llRecords = ItemCount(strURNO, ",")
                    For i = 1 To llRecords
                        ogURNO.Add GetItem(strURNO, i, ",")
                    Next i
                End If
            End If
        End If
        strURNO = ogURNO.Item(1)
        ogURNO.Remove 1
        strFieldlist = "APPN,CKEY,CTYP,HOPO,PKNO,TEXT,URNO,VAFR,VATO"
        strWhere = ""
        strData = "AbsPlan," + strCKEY + "," + strCTYP + "," + _
            frmHiddenServerConnection.txtHOPO.Text + "," + ogActualUser + "," + _
            strTEXT + "," + strURNO + ",199901010000,205012310000"
        frmSplash.UfisCom1.CallServer "IRT", strTable, strFieldlist, strData, strWhere, "360"
    End If
End Sub

Public Sub DeleteView(ViewName As String)
    Dim strTable As String
    Dim strWhere As String
    Dim strFieldlist As String

    Dim strCKEY As String   'name of the view
    Dim strAPPN As String   'application name

    strCKEY = ViewName
    strAPPN = "AbsPlan"
    strTable = "VCDTAB"
    strFieldlist = ""
    strWhere = "WHERE URNO > '0' AND APPN = '" + strAPPN + "' AND PKNO = '" + ogActualUser + _
        "' AND CKEY = '" + strCKEY + "'"

    frmSplash.UfisCom1.CallServer "DRT", strTable, strFieldlist, "", strWhere, "360"
End Sub

Private Sub InitViews()
    frmSplash.List1.AddItem ("Initializing views ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 2

    On Error GoTo ErrHdl
    Dim strViewNames As String
    Dim strLineNo As String
    Dim strLine As String
    Dim strItems As String

    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim olView As New clsView

    Dim strItem As String
    strViewNames = TabVCD.SelectDistinct(0, "", "", ",", True)
    For i = 1 To ItemCount(strViewNames, ",") Step 1
        Set olView = Nothing
        olView.ViewName = Trim(GetItem(strViewNames, i, ","))
        strLineNo = TabVCD.GetLinesByColumnValue(0, olView.ViewName, 0)

        For j = 1 To ItemCount(strLineNo, ",") Step 1
            strLine = TabVCD.GetLineValues(GetItem(strLineNo, j, ","))
            If Trim(TabVCD.GetColumnValue(GetItem(strLineNo, j, ","), 0)) = olView.ViewName Then
                strItems = GetItem(strLine, 3, ",")
                If Len(strItems) > 0 Then
                    If Right(strItems, Len(strItems) - 1) = "|" Then
                        strItems = Left(strItems, Len(strItems) - 1)
                    End If
                End If
    
                Select Case GetItem(strLine, 2, ",")
                    '0-general
                    Case 0:
                        olView.ViewYear = CInt(GetItem(strItems, 1, "|"))
                        'olView.ViewMaxAbsentPerWeek = CInt(GetItem(strItems, 2, "|"))
                        olView.DontCareOrgUnit = GetItem(strItems, 3, "|")
                        olView.DontCareDutyGroup = GetItem(strItems, 4, "|")
                        olView.DontCareFunction = GetItem(strItems, 5, "|")
                        olView.DontCareContract = GetItem(strItems, 6, "|")
                        olView.ViewAbsentPattern = GetItem(strItems, 7, "|")
    
                    '1-OrgUnit
                    Case 1:
                        For k = 1 To ItemCount(strItems, "|") Step 1
                            strItem = GetItem(strItems, k, "|")
                            If strItem <> "" Then
                                olView.AddOrgUnit strItem
                            End If
                        Next k
    
                    '2-DutyGroup
                    Case 2:
                        For k = 1 To ItemCount(strItems, "|") Step 1
                            strItem = GetItem(strItems, k, "|")
                            If strItem <> "" Then
                                olView.AddDutyGroup strItem
                            End If
                        Next k
    
                    '3-Function
                    Case 3:
                        For k = 1 To ItemCount(strItems, "|") Step 1
                            strItem = GetItem(strItems, k, "|")
                            If strItem <> "" Then
                                olView.AddFunctionEmployee strItem
                            End If
                        Next k
    
                    '4-Contract
                    Case 4:
                        For k = 1 To ItemCount(strItems, "|") Step 1
                            strItem = GetItem(strItems, k, "|")
                            If strItem <> "" Then
                                olView.AddContractType strItem
                            End If
                        Next k
    
                    '5-AbsenceType
                    Case 5:
                        For k = 1 To ItemCount(strItems, "|") Step 1
                            strItem = GetItem(strItems, k, "|")
                            If strItem <> "" Then
                                olView.AddAbsenceType strItem
                            End If
                        Next k

                End Select
            End If
        Next j
        gcViews.Add olView
        Set olView = Nothing
    Next i

    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitViews", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitViews", Err
    Err.Clear
    Resume Next
End Sub

Private Sub InitAbsenceCodes()
    frmSplash.List1.AddItem ("Initializing absent codes ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 1

    On Error GoTo ErrHdl

    Dim i As Long
    Dim strSDAC As String
    Dim strSDAA As String
    Dim strFREE As String
    Dim iIdxSDAC As Integer
    Dim iIdxSDAA As Integer
    Dim iIdxFREE As Integer

    iIdxSDAC = GetItemNo(TabODA.HeaderString, "SDAC") - 1
    iIdxSDAA = GetItemNo(TabODA.HeaderString, "SDAA") - 1
    iIdxFREE = GetItemNo(TabODA.HeaderString, "FREE") - 1

    For i = 0 To (TabODA.GetLineCount - 1) Step 1
        strSDAC = TabODA.GetColumnValue(i, iIdxSDAC)
        If Len(strSDAC) > 0 Then
            If DoesKeyExist(gcAbsenceCodeLookup, strSDAC) = False Then
                strSDAA = TabODA.GetColumnValue(i, iIdxSDAA)
                strFREE = TabODA.GetColumnValue(i, iIdxFREE)
                gcAbsenceCodeLookup.Add Item:=strSDAA, key:=strSDAC
                If strFREE = "x" Then
                    gcNoHrsCalculationAbsences.Add Item:=strSDAC, key:=strSDAC
                End If
            End If
        End If
    Next i

    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitAbsenceCodes", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitAbsenceCodes", Err
    Err.Clear
    Resume Next
End Sub

Private Sub InitEmployees()
    frmSplash.List1.AddItem ("Initializing employees ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 1

    InitEmployeeSTF
    InitEmployeeSCO
    InitEmployeeDRR
    InitEmployeeACC
    InitEmployeeMemberInfo
End Sub

Private Sub InitEmployeeSTF()
    On Error GoTo ErrHdl
    Dim strLineNo As String
    Dim strLine As String
    Dim strKey As String
    Dim i As Long

    '"FINM,LANM,DOEM,DODM,REGI,PENO,URNO,KIDS,DODM"
    For i = 0 To TabSTF.GetLineCount
        strLine = TabSTF.GetLineValues(i)
        strKey = Trim(GetItem(strLine, 7, ","))
        If strKey <> "" Then
            If DoesKeyExist(gcEmployee, strKey) <> True Then
            Dim olEmployee As New clsEmployee
                olEmployee.EmployeeURNO = strKey
                olEmployee.EmployeeFirstName = GetItem(strLine, 1, ",")
                olEmployee.EmployeeSecondName = GetItem(strLine, 2, ",")
                olEmployee.EmployeeEntryDate = GetItem(strLine, 3, ",")
                olEmployee.EmployeePENO = GetItem(strLine, 6, ",")
                If GetItem(strLine, 8, ",") = "1" Then
                    olEmployee.EmployeeHasChildren = True
                Else
                    olEmployee.EmployeeHasChildren = False
                End If

                olEmployee.EmployeeExitDate = GetItem(strLine, 9, ",")

                gcEmployee.Add Item:=olEmployee, key:=strKey
                Set olEmployee = Nothing
            End If
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeSTF", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeSTF", Err
    Err.Clear
    Resume Next
End Sub


Private Sub InitEmployeeSCO()
    On Error GoTo ErrHdl
    Dim strSURN As String
    Dim strVPFR As String
    Dim strTYPE As String
    Dim strCEWH As String
    Dim strItem As String
    Dim i As Long

    ' init the weekly working hours
    For i = 0 To TabSCO.GetLineCount
        strSURN = TabSCO.GetColumnValue(i, 0)
        strVPFR = TabSCO.GetColumnValue(i, 2)
        strCEWH = TabSCO.GetColumnValue(i, 4)
        If strCEWH <> "" Then
            strItem = strVPFR + "@" + strCEWH
        Else
            strItem = strVPFR + "@40"
        End If
        If DoesKeyExist(gcEmployee, strSURN) <> False Then
            gcEmployee(strSURN).AddEmployeeWeeklyWorkingHours strVPFR, strItem
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeSCO", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeSCO", Err
    Err.Clear
    Resume Next
End Sub
Private Sub InitEmployeeDRR()
    On Error GoTo ErrHdl
    Dim strSURN As String
    Dim strLineNo As String
    Dim strLine As String
    Dim strKey As String
    Dim strItem As String
    Dim strVPFR As String
    Dim strSDAY As String
    Dim strAbs As String
    Dim strTYPE As String
    Dim strYear As String
    Dim strCEWH As String
    Dim strHours As String
    Dim i As Long
    Dim j As Long

    'init the absences
    For i = 0 To TabDRR.GetLineCount
        strLine = TabDRR.GetLineValues(i)
        strAbs = Trim(GetItem(strLine, 3, ","))         ' the absence code
        If strAbs <> "" Then
            strKey = Trim(GetItem(strLine, 2, ","))     ' the STFU
            strSDAY = Trim(GetItem(strLine, 1, ","))    ' the ShiftDay
            strItem = strAbs + "@" + strSDAY
            If DoesKeyExist(gcEmployee, strKey) <> False Then
                gcEmployee(strKey).AddEmployeeAbsences strSDAY, strItem
            End If
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeDRR", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeDRR", Err
    Err.Clear
    Resume Next
End Sub
Private Sub InitEmployeeACC()
    On Error GoTo ErrHdl
    Dim strSURN As String
    Dim strLineNo As String
    Dim strLine As String
    Dim strKey As String
    Dim strItem As String
    Dim strVPFR As String
    Dim strSDAY As String
    Dim strAbs As String
    Dim strTYPE As String
    Dim strYear As String
    Dim strCEWH As String
    Dim strHours As String
    Dim i As Long
    Dim j As Long

    'init the balance information
    For i = 0 To TabACC.GetLineCount
        strLine = TabACC.GetLineValues(i)
        strKey = Trim(GetItem(strLine, 2, ","))                         ' the STFU
        strTYPE = Trim(GetItem(strLine, 1, ","))                        ' the Type (30 or 31)
        strHours = RoundCutNumber(Trim(GetItem(strLine, 3, ",")), 2)    ' CL12 (the hours)
        strYear = Trim(GetItem(strLine, 4, ","))                        ' the year of the balance
        strItem = CStr(strYear) & "@" & CStr(strHours)
        If DoesKeyExist(gcEmployee, strKey) <> False Then
            If strTYPE = "30" Then
                gcEmployee(strKey).AddBalanceHoliday strYear, strItem
            ElseIf strTYPE = "31" Then
                gcEmployee(strKey).AddBalanceZIF strYear, strItem
            End If
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeACC", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeACC", Err
    Err.Clear
    Resume Next
End Sub

Private Sub InitEmployeeMemberInfo()
    On Error GoTo ErrHdl
    Dim strCPFC As String
    Dim strPKZU As String
    Dim strODGC As String
    Dim strCODE As String
    Dim strSURN As String
    Dim strItem As String
    Dim strVPFR As String
    Dim strVPTO As String
    Dim i As Long

    'init the membership-information
    '"SURN,CODE,VPFR,VPTO,CWEH,CPFC,URNO"
    For i = 0 To TabSCO.GetLineCount - 1
        strSURN = TabSCO.GetColumnValue(i, 0)
        strCODE = TabSCO.GetColumnValue(i, 1)
        strVPFR = TabSCO.GetColumnValue(i, 2)
        strVPTO = TabSCO.GetColumnValue(i, 3)
        If DoesKeyExist(gcEmployee, strSURN) <> False Then
            strItem = strVPFR & "@" & strCODE & "@" & strVPTO
            gcEmployee(strSURN).AddContractType strItem, strItem
        End If
    Next i
    '"SURN,CODE,ODGC,VPFR,VPTO,URNO"
    For i = 0 To TabSOR.GetLineCount - 1
        strSURN = TabSOR.GetColumnValue(i, 0)
        strCODE = TabSOR.GetColumnValue(i, 1)   ' OrgUnit
        strODGC = TabSOR.GetColumnValue(i, 2)   ' DutyGroup
        strVPFR = TabSOR.GetColumnValue(i, 3)
        strVPTO = TabSOR.GetColumnValue(i, 4)
        If DoesKeyExist(gcEmployee, strSURN) <> False Then
            strItem = strVPFR + "@" + strODGC & "@" & strVPTO
            gcEmployee(strSURN).AddDutyGroup strItem, strItem
            strItem = strVPFR + "@" + strCODE & "@" & strVPTO
            gcEmployee(strSURN).AddOrgUnit strItem, strItem
        End If
    Next i
    '"SURN,CODE,VPFR,VPTO,URNO"
    For i = 0 To TabSPF.GetLineCount - 1
        strSURN = TabSPF.GetColumnValue(i, 0)
        strCODE = TabSPF.GetColumnValue(i, 1)    ' function
        strVPFR = TabSPF.GetColumnValue(i, 2)
        strVPTO = TabSPF.GetColumnValue(i, 3)

        If DoesKeyExist(gcEmployee, strSURN) <> False Then
            strItem = strVPFR + "@" + strCODE & "@" & strVPTO
            gcEmployee(strSURN).AddFunction strItem, strItem
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeMemberInfo", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitEmployeeMemberInfo", Err
    Err.Clear
    Resume Next
End Sub

Private Sub InitMaxAbsent()
    On Error GoTo ErrHdl
    frmSplash.List1.AddItem ("Initializing the allowed maximum absent employees ...")
    frmSplash.List1.Refresh
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    frmSplash.List1.TopIndex = frmSplash.List1.TopIndex + 1

    Dim i As Long
    Dim j As Long
    Dim llLineNo As Long
    Dim strURNO As String
    Dim strNAME As String
    Dim strDFLT As String
    Dim strWeek As String
    Dim strYear As String
    Dim strVALU As String
    Dim strMAWU As String
    Dim strLineNo As String

    For i = 0 To TabMAW.GetLineCount - 1

        Dim strLine As String
        strLine = TabMAW.GetLineValues(i)
        strURNO = GetItem(strLine, 1, ",")
        strNAME = GetItem(strLine, 2, ",")
        strDFLT = GetItem(strLine, 3, ",")
        
        If DoesKeyExist(gcMaxAbsent, strNAME) <> True Then
            'we're building the unique objects
            Dim olMaxAbsent As New clsMaxAbsentPerWeek
            olMaxAbsent.PatternName = strNAME
            If IsNumeric(strDFLT) = True Then
                If CInt(strDFLT) > -1 Then
                    olMaxAbsent.PatternDefaultValue = strDFLT
                End If
            End If

            'now we look for the MAATAB-values
            strLineNo = TabMAA.GetLinesByColumnValue(1, strURNO, 0)
            For j = 1 To ItemCount(strLineNo, ",")
                llLineNo = CLng(GetItem(strLineNo, CInt(j), ","))

                strLine = TabMAA.GetLineValues(llLineNo)
                strVALU = GetItem(strLine, 3, ",")
                strWeek = GetItem(strLine, 4, ",")
                strYear = GetItem(strLine, 5, ",")
                olMaxAbsent.AddValue CInt(strYear), CInt(strWeek), CInt(strVALU)
            Next j

            gcMaxAbsent.Add Item:=olMaxAbsent, key:=strNAME
            Set olMaxAbsent = Nothing
        End If
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmHiddenServerConnection.InitMaxAbsent", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmHiddenServerConnection.InitMaxAbsent", Err
    Err.Clear
    Resume Next
End Sub

Public Sub LoadDRR_Local(iYear As Integer)
    ' we want to load the data only once!!!
    If DoesKeyExist(cmLoadedYears, CStr(iYear)) <> True Then
        Dim rec As New Recordset
        Dim conn As New Connection
        Dim sql As String
        Dim strWhere As String
        Dim strTable As String
        Dim strBuffer As String
        Dim strFieldlist As String
        Dim strHeaderLenStr As String
        Dim tmpStr As String
        Dim strSdayMin As String
        Dim strSdayMax As String
        Dim strArr() As String

        Dim llRecords As Long
        Dim llTabDrrLastLine As Long

        Dim ilItemCount As Integer
        Dim i As Long
        Dim j As Long
        Dim blLoad As Boolean

        cmLoadedYears.Add Item:=CStr(iYear), key:=CStr(iYear)
        conn.Open "Provider=MSDASQL.1;Persist Security Info=False;Data Source=NizzaLokal"
        i = 0
        strTable = "CEDA_DRRTAB"
        strFieldlist = "SDAY,STFU,SCOD,BSDU,URNO"

        'set headertext of the TAB
        strArr = Split(strFieldlist, ",")
        ilItemCount = UBound(strArr)
        For i = 0 To ilItemCount
            If i + 1 > ilItemCount Then
                strHeaderLenStr = strHeaderLenStr & "120"
            Else
                strHeaderLenStr = strHeaderLenStr & "120,"
            End If
        Next i
        TabDRR.HeaderLengthString = strHeaderLenStr
        TabDRR.HeaderString = strFieldlist
        llTabDrrLastLine = TabDRR.GetLineCount

        ' we have to load 14 months, beginning with december of the last year,
        ' ending with the january of the following year
        For i = 1 To 14 Step 1
            ' we have to take care if we've loaded data from the year before or after iYear,
            ' because there are overlapping months
            Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = Module1.fMainForm.sbStatusBar.Panels.Item(1).Text & "."
            blLoad = True
            Select Case i
                Case 1:
                    strSdayMin = CStr(iYear - 1) + "1130"
                    strSdayMax = CStr(iYear) + "0101"
                    If DoesKeyExist(cmLoadedYears, CStr(iYear - 1)) = True Then
                        blLoad = False
                    End If
                Case 2:
                    strSdayMin = CStr(iYear - 1) + "1231"
                    strSdayMax = CStr(iYear) + "0201"
                    If DoesKeyExist(cmLoadedYears, CStr(iYear - 1)) = True Then
                        blLoad = False
                    End If
                Case 13:
                    strSdayMin = CStr(iYear) + "1130"
                    strSdayMax = CStr(iYear + 1) + "0101"
                    If DoesKeyExist(cmLoadedYears, CStr(iYear + 1)) = True Then
                        blLoad = False
                    End If
                Case 14:
                    strSdayMin = CStr(iYear) + "1231"
                    strSdayMax = CStr(iYear + 1) + "0201"
                    If DoesKeyExist(cmLoadedYears, CStr(iYear + 1)) = True Then
                        blLoad = False
                    End If
                Case Else:
                    strSdayMin = CStr(iYear) + CStr(Format(i - 2, "00")) + "31"
                    strSdayMax = CStr(iYear) + CStr(Format(i, "00")) + "01"
            End Select

            strWhere = "WHERE ROSL = 'U' AND SDAY > '" + strSdayMin + "' AND SDAY < '" + strSdayMax + "'"
            If strOrgUnit <> "" Then
                strWhere = strWhere & " AND STFU IN (" & strUrnoList & ")"
            End If

            If blLoad = True Then
                sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
                rec.Open sql, conn
                j = 0
                While Not rec.EOF
                    strBuffer = strBuffer + rec!sday + "," + rec!stfu + "," + rec!scod + "," + rec!bsdu + Chr(10)
                    If (j > 0) And (j Mod 50) = 0 Then
                        TabDRR.InsertBuffer strBuffer, Chr(10)
                        strBuffer = ""
                    End If
                    rec.MoveNext
                    j = j + 1
                Wend
                TabDRR.InsertBuffer strBuffer, Chr(10)
                strBuffer = ""
                rec.Close
            End If
        Next i
        conn.Close
        Set rec = Nothing
        Set conn = Nothing
        
        'add the absences to the employees
        Dim strLine As String
        Dim strAbs As String
        Dim strKey As String
        Dim strSDAY As String
        Dim strItem As String
        For i = llTabDrrLastLine To TabDRR.GetLineCount
            strLine = TabDRR.GetLineValues(i)
            strAbs = Trim(GetItem(strLine, 3, ","))         ' the absence code
            If strAbs <> "" Then
                strKey = Trim(GetItem(strLine, 2, ","))     ' the STFU
                strSDAY = Trim(GetItem(strLine, 1, ","))    ' the ShiftDay
                strItem = strAbs + "@" + strSDAY
                If DoesKeyExist(gcEmployee, strKey) <> False Then
                    gcEmployee(strKey).AddEmployeeAbsences strSDAY, strItem
                End If
            End If
        Next i
    End If
End Sub

Private Sub LoadLocal()
    Dim rec As New Recordset
    Dim conn As New Connection

    Dim sql As String
    Dim strWhere As String
    Dim strTable As String
    Dim strBuffer As String
    Dim strFieldlist As String

    Dim i As Long

    TabSCO.ResetContent
    TabSCO.ShowHorzScroller True
    TabSCO.EnableHeaderSizing True
    TabSPF.ResetContent
    TabSPF.ShowHorzScroller True
    TabSPF.EnableHeaderSizing True
    TabSOR.ResetContent
    TabSOR.ShowHorzScroller True
    TabODA.ResetContent
    TabODA.ShowHorzScroller True
    TabODA.EnableHeaderSizing True
    TabSTF.ResetContent
    TabSTF.ShowHorzScroller True
    TabSTF.EnableHeaderSizing True
    TabCOT.ResetContent
    TabCOT.ShowHorzScroller True
    TabCOT.EnableHeaderSizing True
    TabPFC.ResetContent
    TabPFC.ShowHorzScroller True
    TabPFC.EnableHeaderSizing True
    TabODG.ResetContent
    TabODG.ShowHorzScroller True
    TabODG.EnableHeaderSizing True
    TabORG.ResetContent
    TabORG.ShowHorzScroller True
    TabORG.EnableHeaderSizing True
    TabDRR.ResetContent
    TabDRR.ShowHorzScroller True
    TabDRR.EnableHeaderSizing True
    TabACC.ResetContent
    TabACC.ShowHorzScroller True
    TabACC.EnableHeaderSizing True

    conn.Open "Provider=MSDASQL.1;Persist Security Info=False;Data Source=NizzaLokal"
    'conn.Open "Provider=MSDASQL.1;Persist Security Info=False;Data Source=FrankfurtLokal"

'frmSplash.lblStatus.Caption = "Loading SORTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_SORTAB"
    If strOrgUnit <> "" Then
        strFieldlist = "SURN"
        strWhere = "WHERE CODE = '" + strOrgUnit + "'"
    Else
        strFieldlist = "SURN,CODE,ODGC,VPFR,VPTO,URNO"
        strWhere = "WHERE URNO > '0'"
    End If

    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!surn + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabSOR.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabSOR.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

    If strOrgUnit <> "" Then
        strUrnoList = TabSOR.SelectDistinct(0, "'", "'", ",", True)
        TabSOR.ResetContent
        strFieldlist = "SURN,CODE,ODGC,VPFR,VPTO,URNO"
        strWhere = "WHERE SURN IN (" & strUrnoList & ")"
        sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
        rec.Open sql, conn
        While Not rec.EOF
            strBuffer = strBuffer + rec!surn + "," + rec!code + "," + rec!odgc + "," + rec!vpfr + "," + rec!vpto + "," + rec!URNO + Chr(10)
            If (i > 0) And (i Mod 50) = 0 Then
                TabSOR.InsertBuffer strBuffer, Chr(10)
                strBuffer = ""
            End If
            rec.MoveNext
            i = i + 1
        Wend
        TabSOR.InsertBuffer strBuffer, Chr(10)
        strBuffer = ""
        rec.Close
    End If

'frmSplash.lblStatus.Caption = "Loading SCOTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_SCOTAB"
    strFieldlist = "SURN,CODE,VPFR,VPTO,CWEH,CPFC,URNO"
    If strOrgUnit <> "" Then
        strWhere = "WHERE URNO > '0' AND SURN IN (" & strUrnoList & ")"
    Else
        strWhere = "WHERE URNO > '0'"
    End If
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!surn + "," + rec!code + "," + rec!vpfr + "," + rec!vpto + _
        "," + rec!cweh + "," + rec!cpfc + "," + rec!URNO + "," + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabSCO.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabSCO.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading SPFTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_SPFTAB"
    strFieldlist = "SURN,CODE,VPFR,VPTO,URNO"
    If strOrgUnit <> "" Then
        strWhere = "WHERE URNO > '0' AND SURN IN (" & strUrnoList & ") AND (PRIO = '1' OR PRIO = ' ')"
    Else
        strWhere = "WHERE URNO > '0' AND (PRIO = '1' OR PRIO = ' ')"
    End If
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!surn + "," + rec!code + "," + rec!vpfr + "," + rec!vpto + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabSPF.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabSPF.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading ODATAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_ODATAB"
    strFieldlist = "SDAC,SDAE,URNO,SDAS,SDAN"
    strWhere = "WHERE URNO > '0'"
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!sdac + "," + rec!sdae + "," + rec!URNO + "," + rec!sdas + "," + rec!sdan + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabODA.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabODA.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading STFTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_STFTAB"
    strFieldlist = "FINM,LANM,DOEM,DODM,REGI,PENO,URNO"
    If strOrgUnit <> "" Then
        strWhere = "WHERE URNO IN (" & strUrnoList & ")"
    Else
        strWhere = "WHERE URNO > '0'"
    End If
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!finm + "," + rec!lanm + "," + rec!doem + "," + rec!dodm + "," + _
                    rec!regi + "," + rec!PENO + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabSTF.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabSTF.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading COTTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_COTTAB"
    strFieldlist = "CTRC,CTRN,DPTC,REGI,URNO"
    strWhere = "WHERE URNO > '0'"
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!ctrc + "," + rec!ctrn + "," + rec!dptc + "," + rec!regi + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabCOT.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabCOT.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading PFCTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_PFCTAB"
    strFieldlist = "DPTC,FCTC,FCTN,URNO"
    strWhere = "WHERE URNO > '0'"
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!dptc + "," + rec!fctc + "," + rec!fctn + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabPFC.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabPFC.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading ODGTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_ODGTAB"
    strFieldlist = "ODGC,ODGN,ORGU,URNO"
    strWhere = "WHERE URNO > '0'"
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!odgc + "," + rec!odgn + "," + rec!orgu + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabODG.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabODG.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading ORGTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_ORGTAB"
    strFieldlist = "DPT1,DPT2,DPTN,URNO"
    strWhere = "WHERE URNO > '0'"
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!dpt1 + "," + rec!dpt2 + "," + rec!dptn + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabORG.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabORG.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading ACCTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_ACCTAB"
    strFieldlist = "TYPE,STFU,CL12,YEAR,OP01,URNO"
    strWhere = "WHERE TYPE = '30' OR TYPE = '31'"
    If strOrgUnit <> "" Then
        strWhere = strWhere & " AND STFU IN (" & strUrnoList & ")"
    End If
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!Type + "," + rec!stfu + "," + rec!cl12 + "," + rec!Year + "," + rec!op01 + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabACC.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabACC.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""
    rec.Close

'frmSplash.lblStatus.Caption = "Loading HOLTAB ..."
    DoEvents
    i = 0
    strTable = "CEDA_HOLTAB"
    strFieldlist = "HDAY,TYPE,REMA,URNO"
    strWhere = "WHERE URNO > '0'"
    sql = "SELECT " + strFieldlist + " FROM " + strTable + " " + strWhere
    rec.Open sql, conn
    While Not rec.EOF
        strBuffer = strBuffer + rec!hday + "," + rec!Type + "," + rec!rema + "," + rec!URNO + Chr(10)
        If (i > 0) And (i Mod 50) = 0 Then
            TabHOL.InsertBuffer strBuffer, Chr(10)
            strBuffer = ""
        End If
        rec.MoveNext
        i = i + 1
    Wend
    TabHOL.InsertBuffer strBuffer, Chr(10)
    strBuffer = ""

    rec.Close
    conn.Close
    Set rec = Nothing
    Set conn = Nothing
End Sub

Private Sub Form_Terminate()
    frmSplash.UfisCom1.CleanupCom
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmSplash.UfisCom1.CleanupCom
End Sub

Private Sub InitTabForCedaConnection(ByRef rTab As TABLib.Tab)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = txtHostName.Text
    rTab.CedaPort = "3357"
    rTab.CedaHopo = txtHOPO.Text
    rTab.CedaCurrentApplication = "AbsPlan"
    rTab.CedaTabext = "TAB"
    If ogActualUser = "" Then
        rTab.CedaUser = "AbsPlan"
    Else
        rTab.CedaUser = ogActualUser
    End If
    rTab.CedaWorkstation = GetWorkstationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = "Test_App"

    'do general initializing
    rTab.ResetContent
    rTab.ShowHorzScroller True
    rTab.EnableHeaderSizing True
End Sub
