VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Begin VB.Form frmHidden 
   Caption         =   "Form1"
   ClientHeight    =   11430
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12060
   LinkTopic       =   "Form1"
   ScaleHeight     =   11430
   ScaleWidth      =   12060
   StartUpPosition =   3  'Windows Default
   Begin MSComCtl2.MonthView MonthView1 
      Height          =   6660
      Left            =   600
      TabIndex        =   1
      Top             =   3480
      Width           =   12390
      _ExtentX        =   21855
      _ExtentY        =   11748
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      MonthColumns    =   4
      MonthRows       =   3
      ShowWeekNumbers =   -1  'True
      StartOfWeek     =   22806530
      CurrentDate     =   36955
   End
   Begin MSACAL.Calendar Calendar1 
      Height          =   2415
      Left            =   3360
      TabIndex        =   0
      Top             =   240
      Width           =   3855
      _Version        =   524288
      _ExtentX        =   6800
      _ExtentY        =   4260
      _StockProps     =   1
      BackColor       =   -2147483633
      Year            =   2001
      Month           =   3
      Day             =   5
      DayLength       =   1
      MonthLength     =   2
      DayFontColor    =   0
      FirstDay        =   1
      GridCellEffect  =   1
      GridFontColor   =   10485760
      GridLinesColor  =   -2147483632
      ShowDateSelectors=   -1  'True
      ShowDays        =   -1  'True
      ShowHorizontalGrid=   -1  'True
      ShowTitle       =   -1  'True
      ShowVerticalGrid=   -1  'True
      TitleFontColor  =   10485760
      ValueIsNull     =   0   'False
      BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmHidden"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Function GetMonthDays(MonthNo As Byte, YearNo As Integer) As Byte
    Calendar1.Year = YearNo
    Calendar1.Month = MonthNo
    Calendar1.Day = 31
    GetMonthDays = Calendar1.Day
End Function

Public Function GetMondaysOfMonth(MonthNo As Byte, YearNo As Integer) As String
    Dim i As Integer
    Dim strResult As String

    ' init the calendar for this case
    Calendar1.Year = YearNo
    Calendar1.Month = MonthNo

    ' build the return string
    For i = 1 To GetMonthDays(MonthNo, YearNo) Step 1
        Calendar1.Day = i
        If Weekday(Calendar1.Value) = vbMonday Then
            strResult = strResult + CStr(i) + ","
        End If
    Next i
    GetMondaysOfMonth = strResult
End Function

Public Function GetFridaysOfMonth(MonthNo As Byte, YearNo As Integer) As String
    Dim i As Integer
    Dim strResult As String

    ' init the calendar for this case
    Calendar1.Year = YearNo
    Calendar1.Month = MonthNo

    ' build the return string
    For i = 1 To GetMonthDays(MonthNo, YearNo) Step 1
        Calendar1.Day = i
        If Weekday(Calendar1.Value) = vbFriday Then
            strResult = strResult + CStr(i) + ","
        End If
    Next i
    GetFridaysOfMonth = strResult
End Function

Public Function GetSaturdaysOfMonth(MonthNo As Byte, YearNo As Integer) As String
    Dim i As Integer
    Dim strResult As String

    ' init the calendar for this case
    Calendar1.Year = YearNo
    Calendar1.Month = MonthNo

    ' build the return string
    For i = 1 To GetMonthDays(MonthNo, YearNo) Step 1
        Calendar1.Day = i
        If Weekday(Calendar1.Value) = vbSaturday Then
            strResult = strResult + CStr(i) + ","
        End If
    Next i
    GetSaturdaysOfMonth = strResult
End Function

Public Function GetSundaysOfMonth(MonthNo As Byte, YearNo As Integer) As String
    Dim i As Integer
    Dim strResult As String

    ' init the calendar for this case
    Calendar1.Year = YearNo
    Calendar1.Month = MonthNo

    ' build the return string
    For i = 1 To GetMonthDays(MonthNo, YearNo) Step 1
        Calendar1.Day = i
        If Weekday(Calendar1.Value) = vbSunday Then
            strResult = strResult + CStr(i) + ","
        End If
    Next i
    GetSundaysOfMonth = strResult
End Function

Public Function GetSaturdaysOfYear(MonthNo As Byte, YearNo As Integer) As String
    Dim i As Byte
    Dim strResult As String

    For i = 1 To 12 Step 1
        strResult = strResult + GetSaturdaysOfMonth(i, YearNo)
    Next i

    GetSaturdaysOfYear = strResult
End Function

Public Function GetSundaysOfYear(MonthNo As Byte, YearNo As Integer) As String
    Dim i As Byte
    Dim strResult As String

    For i = 1 To 12 Step 1
        strResult = strResult + GetSundaysOfMonth(i, YearNo)
    Next i

    GetSundaysOfYear = strResult
End Function

Public Function GetFirstSundayOfMonth(MonthNo As Byte, YearNo As Integer) As Byte
    Dim i As Integer
    Dim MonthDays As Byte
    MonthDays = GetMonthDays(MonthNo, YearNo)

    ' init the calendar for this case
    i = 1
    Calendar1.Year = YearNo
    Calendar1.Month = MonthNo
    Calendar1.Day = i

    ' build the return string
    If (Weekday(Calendar1.Value) = vbSunday) Then
        GetFirstSundayOfMonth = i
    Else
        While (i < MonthDays + 1) And (Weekday(Calendar1.Value) <> vbSunday)
            i = i + 1
            Calendar1.Day = i
        Wend
        GetFirstSundayOfMonth = i
    End If
End Function

Public Function GetNumberOfWeeks(MonthNo As Byte, YearNo As Integer) As Byte
    Dim MonthDays As Byte
    MonthDays = GetMonthDays(MonthNo, YearNo)

    ' init the calendar for this case
    Calendar1.Year = YearNo
    Calendar1.Month = MonthNo
    Calendar1.Day = 1

    GetNumberOfWeeks = 4

    Select Case (Weekday(Calendar1.Value))
        Case vbTuesday
            If MonthDays > 30 Then
                GetNumberOfWeeks = 5
            End If
        Case vbWednesday
            If MonthDays > 29 Then
                GetNumberOfWeeks = 5
            End If
        Case vbThursday
            If MonthDays > 28 Then
                GetNumberOfWeeks = 5
            End If
    End Select
End Function

Public Function GetWeekOfDay(DayNo As Byte, MonthNo As Byte, YearNo As Integer) As Byte
    MonthView1.Value = CDate(CStr(DayNo) + "/" + CStr(MonthNo) + "/" + CStr(YearNo))
    GetWeekOfDay = MonthView1.Week
End Function
