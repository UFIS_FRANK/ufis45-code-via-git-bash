VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{6937B440-6494-11CF-97D0-524153480000}#6.0#0"; "ogocxgrd.ocx"
Begin VB.Form frmDetailWindow 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DetailWindow"
   ClientHeight    =   6750
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11355
   Icon            =   "frmDetailWindow.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   450
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   757
   StartUpPosition =   3  'Windows Default
   Tag             =   "1125"
   Begin VB.CommandButton cmdLastYear 
      Caption         =   "< &Last year"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3240
      TabIndex        =   8
      Tag             =   "1200"
      Top             =   6120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdNextYear 
      Caption         =   "&Next year >"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   7
      Tag             =   "1201"
      Top             =   6120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin TABLib.TAB PrintTab 
      Height          =   375
      Left            =   8280
      TabIndex        =   6
      Top             =   5040
      Visible         =   0   'False
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   661
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5745
      TabIndex        =   5
      Tag             =   "1010"
      Top             =   6120
      Width           =   1095
   End
   Begin GRIDOCXLib.Gridocx GridocxHeader 
      Height          =   975
      Left            =   960
      TabIndex        =   3
      Top             =   960
      Width           =   8415
      _Version        =   393216
      _ExtentX        =   14843
      _ExtentY        =   1720
      _StockProps     =   0
      AllowEnter      =   0   'False
      AllowMoveRows   =   0   'False
      AllowMoveCols   =   0   'False
      TrackingRows    =   0   'False
      TrackingCols    =   0   'False
      MarkRowHeader   =   0   'False
      MarkColHeader   =   0   'False
   End
   Begin GRIDOCXLib.Gridocx GridocxMainHeader 
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   11055
      _Version        =   393216
      _ExtentX        =   19500
      _ExtentY        =   873
      _StockProps     =   0
      DisplayColHeaders=   0   'False
      DisplayRowHeaders=   0   'False
      DisplayVertLines=   0   'False
      DisplayHorzLines=   0   'False
      NumberedColHeader=   0   'False
      NumberedRowHeader=   0   'False
      MarkRowHeader   =   0   'False
      MarkColHeader   =   0   'False
      NumRows         =   1
      NumCols         =   1
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4515
      TabIndex        =   0
      Tag             =   "1003"
      Top             =   6120
      Width           =   1095
   End
   Begin GRIDOCXLib.Gridocx GridocxDetail 
      Height          =   2775
      Left            =   480
      TabIndex        =   1
      Top             =   2400
      Width           =   5895
      _Version        =   393216
      _ExtentX        =   10398
      _ExtentY        =   4895
      _StockProps     =   0
      LinkedFileName  =   "C:\Ufis\develop\AbsPlan\12429382lnk.lnk"
      BeginProperty DefaultFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      InteriorColor   =   16777215
      AllowEnter      =   0   'False
      NumRows         =   0
      NumCols         =   0
      SmartResize     =   -1  'True
      ExcelSelectionFrame=   0   'False
      ExcelCurrentCell=   0   'False
   End
   Begin VB.Label lblInfo 
      Height          =   975
      Left            =   9720
      TabIndex        =   4
      Top             =   960
      Width           =   1335
   End
End
Attribute VB_Name = "frmDetailWindow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private olView As clsView
Public imYear As Integer
Private imPrintCount As Integer

Private Sub cmdLastYear_Click()
    cmdLastYear.Enabled = False
    cmdNextYear.Enabled = True
    lblInfo.Caption = ""
    imYear = imYear - 1
    Init
End Sub

Private Sub cmdNextYear_Click()
    cmdLastYear.Enabled = True
    cmdNextYear.Enabled = False
    'lblInfo.Caption = LoadResString(1140)
    lblInfo.Caption = ""
    imYear = imYear + 1
    Init
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   06.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Private Sub Form_Load()
    Dim olElement

    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    LoadResStrings Me

    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        cmdLastYear.Visible = True
        cmdNextYear.Visible = True
        cmdLastYear.Enabled = False
        cmdNextYear.Enabled = True
    Else
        cmdLastYear.Visible = False
        cmdNextYear.Visible = False
    End If

    ' get the actual view
    For Each olElement In gcViews
        If olElement.ViewName = olDocument.omViewName Then
            Set olView = olElement              '"pointer" to the actual view
            imYear = olView.ViewYear
            Exit For
        End If
    Next
    If olView Is Nothing Then
        imYear = Year(Now)
    End If

    Init
End Sub
Private Sub Init()
    Dim StyleGridocxHeader As Object
    Dim StyleGridocxDetail As Object
    Dim RangeGridocxHeader As Object
    Dim RangeGridocxDetail As Object
    Set StyleGridocxHeader = GridocxHeader.CreateStyleObject()
    Set StyleGridocxDetail = GridocxDetail.CreateStyleObject()
    Set RangeGridocxHeader = GridocxHeader.CreateRangeObject()
    Set RangeGridocxDetail = GridocxDetail.CreateRangeObject()

    GridocxHeader.SetGridReadOnly False
    GridocxDetail.SetGridReadOnly False

    InitHeader
    InitGridocxHeader
    InitGridocxDetail
    frmCalc.FillDetailWindow imYear

    GridocxHeader.SetGridReadOnly True
    GridocxDetail.SetGridReadOnly True

''    StyleGridocxHeader.SetReadOnly True
''    StyleGridocxDetail.SetReadOnly True
'    RangeGridocxHeader.InitRange 1, 1, 6, 2
'    RangeGridocxDetail.InitRange 1, 1, 31, 14
'    GridocxHeader.SetStyleRange RangeGridocxHeader, StyleGridocxHeader, gxOverride
'    GridocxDetail.SetStyleRange RangeGridocxDetail, StyleGridocxDetail, gxOverride

    lblInfo.Height = GridocxHeader.Height
    lblInfo.Width = GridocxDetail.Width - GridocxHeader.Width - 10
    lblInfo.Move GridocxHeader.Left + GridocxHeader.Width + 10, GridocxHeader.Top, lblInfo.Width, lblInfo.Height
End Sub
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   19.04.2001
' .
' . description :   inits the labels at the header of the form
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub InitHeader()
    Dim str As String
    Dim style As Object
    Dim Range As Object
    Dim oFont As Object

    Set style = GridocxMainHeader.CreateStyleObject()
    Set Range = GridocxMainHeader.CreateRangeObject()
    Set oFont = GridocxMainHeader.CreateFontObject()

    ' look after the scrollbars
    GridocxMainHeader.SetScrollBarMode 0, gxnDisabled
    GridocxMainHeader.SetScrollBarMode 1, gxnDisabled
    GridocxMainHeader.SetColWidth 1, 1, GridocxMainHeader.Width
    GridocxMainHeader.SetRowHeight 1, 1, 31

    str = LoadResString(1135) + " " + _
        ogActualEmployeeNameSecond + ", " + _
        ogActualEmployeeNameFirst + ", " + _
        ogActualEmployeePENO

    Range.InitRange 1, 1, 1, 1
    oFont.SetBold True
    oFont.SetFaceName "Arial"
    oFont.SetSize 16
    style.SetHorizontalAlignment 1
    style.SetFont oFont, gxOverride
    style.SetValue str
    style.SetEnabled False
    GridocxMainHeader.SetStyleRange Range, style, gxOverride
    frmDetailWindow.Caption = ogActualEmployeeNameFirst + " " + ogActualEmployeeNameSecond + ", " + ogActualEmployeePENO
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   06.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub InitGridocxHeader()
    Dim i As Integer
    Dim style As Object
    Dim Range As Object
    Dim oFont As Object

    Set style = GridocxHeader.CreateStyleObject()
    Set Range = GridocxHeader.CreateRangeObject()
    Set oFont = GridocxHeader.CreateFontObject()

    ' look after the scrollbars
    GridocxHeader.SetScrollBarMode 0, gxnDisabled
    GridocxHeader.SetScrollBarMode 1, gxnDisabled

    ' look after the general width and height
    GridocxHeader.Width = 562
    GridocxHeader.Height = 62

    ' look after the number of rows and columns
    GridocxHeader.SetColCount 7
    GridocxHeader.SetRowCount 1

    ' look after the width of the columns
    GridocxHeader.SetColWidth 0, 0, 70
    GridocxHeader.SetColWidth 1, 7, 70
    GridocxHeader.Left = GridocxDetail.Left

    ' look after the height of the columns
    GridocxHeader.SetRowHeight 0, 0, 40
    GridocxHeader.SetRowHeight 1, 1, 20

    ' set the values of the cells (the headers of the rows and of the columns)
    For i = 1 To 1 Step 1
        Range.InitRange 0, i, 0, i
        GridocxHeader.SetValueRange Range, LoadResString(1126 + i), gxOverride
    Next i
    Range.InitRange 1, 0, 1, 0
    GridocxHeader.SetValueRange Range, LoadResString(1166), gxOverride
    For i = 2 To 7 Step 1
        Range.InitRange i, 0, i, 0
        GridocxHeader.SetValueRange Range, LoadResString(1127 + i), gxOverride
    Next i

    ' set the background color
    Range.InitRange 1, 1, 7, 1
    GridocxHeader.SetValueRange Range, "", gxOverride
    style.Free
    oFont.SetFaceName "Arial"
    oFont.SetSize 8.25
    style.SetFont oFont, gxOverride
    style.SetInterior (RGB(255, 255, 255))
    style.SetEnabled False

    GridocxHeader.SetStyleRange Range, style, gxOverride
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   06.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub InitGridocxDetail()
    Dim i As Integer
    Dim style As Object
    Dim Range As Object
    Dim oFont As Object

    Set style = GridocxDetail.CreateStyleObject()
    Set Range = GridocxDetail.CreateRangeObject()
    Set oFont = GridocxDetail.CreateFontObject()

    ' look after the scrollbars
    GridocxDetail.SetScrollBarMode 0, gxnDisabled
    GridocxDetail.SetScrollBarMode 1, gxnDisabled
    
    ' look after the general width and height
    GridocxDetail.Width = 702
    GridocxDetail.Height = 227

    ' look after the number of rows and columns
    GridocxDetail.SetColCount 31
    GridocxDetail.SetRowCount 14

    ' look after the width of the columns
    GridocxDetail.SetColWidth 0, 0, 80
    GridocxDetail.SetColWidth 1, 31, 20

    ' look after the height of the columns
    GridocxDetail.SetRowHeight 0, 14, 15

    GridocxDetail.SelectionMode = eGX_SELROW
    GridocxDetail.ActivateCellMode = eGX_CAFOCUS_DBLCLICKONCELL

    ' set the values of the cells (the headers of the rows and of the columns)
    Range.InitRange 0, 1, 0, 1
    'GridocxDetail.SetValueRange Range, LoadResString(1112) + " " + CStr(olView.ViewYear - 1), gxOverride
    GridocxDetail.SetValueRange Range, LoadResString(1112) + " " + CStr(imYear - 1), gxOverride
    For i = 2 To 13 Step 1
        Range.InitRange 0, i, 0, i
        'GridocxDetail.SetValueRange Range, LoadResString(1099 + i) + " " + CStr(olView.ViewYear), gxOverride
        GridocxDetail.SetValueRange Range, LoadResString(1099 + i) + " " + CStr(imYear), gxOverride
    Next i
    Range.InitRange 0, 14, 0, 14
    'GridocxDetail.SetValueRange Range, LoadResString(1101) + " " + CStr(olView.ViewYear + 1), gxOverride
    GridocxDetail.SetValueRange Range, LoadResString(1101) + " " + CStr(imYear + 1), gxOverride
    For i = 1 To 31 Step 1
        Range.InitRange i, 0, i, 0
        GridocxDetail.SetValueRange Range, i, gxOverride
    Next i

    ' set the font
    oFont.SetBold False
    oFont.SetFaceName "Arial"
    oFont.SetSize 10
    style.SetFont oFont, gxOverride
    Range.InitRange 0, 0, 31, 0
    GridocxDetail.SetStyleRange Range, style, gxOverride
    Range.InitRange 0, 1, 0, 14
    GridocxDetail.SetStyleRange Range, style, gxOverride

    ' set the background colors
    Range.InitRange 1, 1, 31, 14
    GridocxDetail.SetValueRange Range, "", gxOverride
    style.Free
    style.SetInterior (RGB(255, 255, 255))
    'Style.SetEnabled False
    GridocxDetail.SetStyleRange Range, style, gxOverride
    InitGridocxDetailColors
 End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   06.04.2001
' .
' . description :   start of coloring of the weekends and the days
' .                 not belonging to the month
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Public Sub InitGridocxDetailColors()
    Dim i As Byte
    'ColorMonth 12, olView.ViewYear - 1, 1
    ColorMonth 12, imYear - 1, 1
    For i = 1 To 12 Step 1
        'ColorMonth i, olView.ViewYear, i + 1
        ColorMonth i, imYear, i + 1
    Next i
    'ColorMonth 1, olView.ViewYear + 1, 14
    ColorMonth 1, imYear + 1, 14
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   06.04.2001
' .
' . description :   coloring the weekends and the days of one month
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ColorMonth(bMonth As Byte, iYear As Integer, bLine As Byte)
    Dim style As Object
    Dim Range As Object
'    Dim Brush As Object

    Set style = GridocxDetail.CreateStyleObject()
    Set Range = GridocxDetail.CreateRangeObject()
'    Set Brush = GridocxDetail.CreateBrushObject()

    Dim olArrSat As New Collection
    Dim olArrSun As New Collection
    Dim olArrHol As New Collection
    Dim strSat As String
    Dim strSun As String
    Dim strHol As String

    Dim i As Integer
    Dim ilFound As Integer
    Dim ilLen As Integer
    Dim olCollectionElement
    Dim ColorWeekend As Long
    Dim ColorHoliday As Long
    Dim ColorEndMonth As Long

'    ColorWeekend = 12640511 '16777152
'    ColorHoliday = 12648447 'vbRed
    ColorWeekend = 12648447
    ColorHoliday = 12640511
    ColorEndMonth = vbButtonFace

    ' get the saturdays and the sundays of the month in a string
    strSat = frmHidden.GetSaturdaysOfMonth(bMonth, iYear)
    strSun = frmHidden.GetSundaysOfMonth(bMonth, iYear)
    strHol = frmHiddenServerConnection.TabHOL.GetLinesByColumnValue(0, CStr(iYear) + CStr(Format(bMonth, "00")), 0)

    ' fill the collections with the saturdays and sundays
    ilLen = Len(strSat)
    ilFound = InStr(strSat, ",")
    While ilFound > 1 And ilLen > 0
        olArrSat.Add (Left(strSat, ilFound - 1))
        strSat = Right(strSat, ilLen - ilFound)
        ilFound = InStr(strSat, ",")
        ilLen = Len(strSat)
    Wend

    ilLen = Len(strSun)
    ilFound = InStr(strSun, ",")
    While ilFound > 1 And ilLen > 0
        olArrSun.Add (Left(strSun, ilFound - 1))
        strSun = Right(strSun, ilLen - ilFound)
        ilFound = InStr(strSun, ",")
        ilLen = Len(strSun)
    Wend

    For i = 1 To ItemCount(strHol, ",")
        olArrHol.Add Mid(frmHiddenServerConnection.TabHOL.GetLineValues(GetItem(strHol, i, ",")), 7, 2)
    Next i

    ' color the saturdays and sundays
    For Each olCollectionElement In olArrSat
        Range.InitRange olCollectionElement, bLine, olCollectionElement, bLine
        style.SetInterior (ColorWeekend)
        GridocxDetail.SetStyleRange Range, style, gxOverride
    Next
    For Each olCollectionElement In olArrSun
        Range.InitRange olCollectionElement, bLine, olCollectionElement, bLine
        style.SetInterior (ColorWeekend)
        GridocxDetail.SetStyleRange Range, style, gxOverride
    Next
    For Each olCollectionElement In olArrHol
        Range.InitRange olCollectionElement, bLine, olCollectionElement, bLine
        'Brush.SetHatch 0
        'style.SetInterior Brush ', gxOverride
        style.SetInterior (ColorHoliday)
        GridocxDetail.SetStyleRange Range, style, gxOverride
    Next

    ' color the days not belonging to the month
    Select Case frmHidden.GetMonthDays(bMonth, iYear)
        Case 28:
            Range.InitRange 29, bLine, 31, bLine
            style.SetInterior (ColorEndMonth)
            GridocxDetail.SetStyleRange Range, style, gxOverride
        Case 29:
            Range.InitRange 30, bLine, 31, bLine
            style.SetInterior (ColorEndMonth)
            GridocxDetail.SetStyleRange Range, style, gxOverride
        Case 30:
            Range.InitRange 31, bLine, 31, bLine
            style.SetInterior (ColorEndMonth)
            GridocxDetail.SetStyleRange Range, style, gxOverride
    End Select
End Sub

Private Sub cmdClose_Click()
    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
        Unload Module1.fMainForm
    Else
        Unload Me
    End If
End Sub

'Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'    If frmSplash.AATLoginControl1.GetPrivileges("m_OnlyOwnGroup") <> 1 Then
'        Cancel = 1
'        Unload Module1.fMainForm
'    End If
'End Sub

Private Sub GridocxDetail_OnLButtonClickedRowCol(ByVal nRow As Double, ByVal nCol As Double, ByVal nFlags As Double, ByVal point As Object, ByVal bProcessed As Object)
    GridocxDetail_OnMovedCurrentCell nRow, nCol
End Sub

Private Sub GridocxDetail_OnLButtonDblClkRowCol(ByVal nRow As Double, ByVal nCol As Double, ByVal nFlags As Double, ByVal point As Object, ByVal bProcessed As Object)
    GridocxDetail_OnMovedCurrentCell nRow, nCol
End Sub

Private Sub GridocxDetail_OnMovedCurrentCell(ByVal nRow As Double, ByVal nCol As Double)
    On Error GoTo ErrHdl

    If nRow > 0 And nCol > 0 Then
        Dim strDate As String
        Dim strLineNo As String
        Dim strAbsCode As String
        Dim strAbs As String
        Dim strTEXT As String

        Select Case nRow
            Case 1:
                'strDate = CStr(olView.ViewYear - 1) + "12" + CStr(Format(nCol, "00"))
                strDate = CStr(imYear - 1) + "12" + CStr(Format(nCol, "00"))
            Case 14:
                'strDate = CStr(olView.ViewYear + 1) + "01" + CStr(Format(nCol, "00"))
                strDate = CStr(imYear + 1) + "01" + CStr(Format(nCol, "00"))
            Case Else:
                'strDate = CStr(olView.ViewYear) + CStr(Format(nRow - 1, "00")) + CStr(Format(nCol, "00"))
                strDate = CStr(imYear) + CStr(Format(nRow - 1, "00")) + CStr(Format(nCol, "00"))
        End Select
        strLineNo = frmCalc.TabAbsences.GetLinesByColumnValue(0, strDate, 1)
        If strLineNo <> "" Then
            If IsNumeric(strLineNo) = False Then
                strLineNo = GetItem(strLineNo, 1, ",")
            End If
            If IsNumeric(strLineNo) = True Then
                strAbsCode = GetItem(frmCalc.TabAbsences.GetLineValues(strLineNo), 2, ",")
                strLineNo = frmHiddenServerConnection.TabODA.GetLinesByColumnValue(0, strAbsCode, 0)
                If strLineNo <> "" Then
                    If (IsNumeric(strLineNo) <> False) And (DoesKeyExist(gcAbsenceCodeLookup, strAbsCode) = True) Then
                        strAbs = GetItem(frmHiddenServerConnection.TabODA.GetLineValues(strLineNo), 5, ",")
                        strTEXT = LoadResString(1137) + " " + _
                            Mid(strDate, 5, 2) + "/" + Mid(strDate, 7, 2) + "/" + Left(strDate, 4) + _
                            vbCrLf + _
                            LoadResString(1138) + vbCrLf + strAbsCode + _
                            vbCrLf + _
                            LoadResString(1139) + vbCrLf + strAbs
                        lblInfo.Caption = strTEXT
                        'MsgBox strTEXT, vbInformation
                    Else
                        'lblInfo.Caption = "No absence code."
                        lblInfo.Caption = LoadResString(1140)
                    End If
                Else
                    'lblInfo.Caption = "No absence code."
                    lblInfo.Caption = LoadResString(1140)
                End If
            End If
        Else
            lblInfo.Caption = LoadResString(1140)
        End If
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDetailWindow.GridocxDetail_OnMovedCurrentCell", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDetailWindow.GridocxDetail_OnMovedCurrentCell", Err
    Err.Clear
End Sub

Private Sub cmdPrint_Click()
    On Error GoTo ErrHdl

    With Module1.fMainForm
        If .ActiveForm Is Nothing Then Exit Sub

        Dim strPrintDate As String
        Dim strTitle As String

        PrintTab.SetFieldSeparator "|"
        PrintTab.PrintSetPageTitleFont 18, True, False, True, 0, "Times New Roman"
        strPrintDate = Now
        InitPrintTab

        strTitle = LoadResString(1135) + " " + _
            ogActualEmployeeNameSecond + ", " + _
            ogActualEmployeeNameFirst + ", " + _
            ogActualEmployeePENO

        With .dlgCommonDialog
            .DialogTitle = "Print"
            .CancelError = True
            .Flags = cdlPDPrintSetup Or cdlPDReturnDC Or cdlPDHidePrintToFile Or cdlPDNoSelection
            .ShowPrinter
            If Err <> MSComDlg.cdlCancel Then
                PrintTab.PrintTab .hDC, strTitle, strPrintDate
            End If
        End With
    End With

    Exit Sub
ErrHdl:
    If Err.Number = cdlCancel Then
    Else
        'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmDetailWindow.cmdPrint_Click", Err
        WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmDetailWindow.cmdPrint_Click", Err
    End If
    Resume Next
End Sub

Private Sub InitPrintTab()
    Dim strBuffer As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strColumnAlignmentStr As String
    Dim i As Integer
    Dim j As Integer

    imPrintCount = 0
    PrintTab.ResetContent

    'header strings
    strHeaderString = "|" & LoadResString(1166) & "|"
    strHeaderLengthString = "82|90|"
    strColumnAlignmentStr = "L|R|"
    For i = 1 To 6 Step 1   'alt: 6
        strColumnAlignmentStr = strColumnAlignmentStr & "R|"
        strHeaderLengthString = strHeaderLengthString & "90|"   'alt: 103
        strHeaderString = strHeaderString & LoadResString(1128 + i) & "|"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    strHeaderString = Left(strHeaderString, Len(strHeaderString) - 1)
    PrintTab.ColumnAlignmentString = strColumnAlignmentStr
    PrintTab.HeaderLengthString = strHeaderLengthString
    PrintTab.HeaderString = strHeaderString

    'header font
    PrintTab.HeaderFontSize = 11

    'body of the upper TAB
    strBuffer = ""
    For i = 1 To 1
        strBuffer = strBuffer & LoadResString(1126 + i) & "|"
        For j = 1 To 8  'alt: 7
            strBuffer = strBuffer & GridocxHeader.GetValueRowCol(i, j) & "|"
        Next j
        strBuffer = strBuffer & Chr(10)
    Next i
    PrintTab.InsertBuffer strBuffer, Chr(10)

    'PrintTab.PrintSetMargins MarginTop.Text, MarginBottom.Text, MarginLeft.Text, MarginRight.Text
    PrintTab.PrintSetMargins "20", "20", "15", "15"
    PrintTab.PrintSetPageHeaderHeight "20"
    PrintTab.PrintSetPageFooterHeight "15"
    PrintTab.PrintSetSeparatorLineTop "18", "1"
    PrintTab.PrintSetSeparatorLineBottom "13", "1"
    PrintTab.PrintSetDatePosition "B", "R"
    PrintTab.PrintSetTitlePosition "T", "L"

    'PrintTab.PrintSetLogo "T", "R", GetIniEntry("", "AbsPlan", "", "PRINTBMP", "c:\ufis\system\logo.bmp")
    PrintTab.PrintSetLogo "T", "R", GetIniEntry("", "AbsPlan", "", "PRINTBMP", UFIS_SYSTEM & "\logo.bmp")
    'PrintTab.PrintSetFitToPage True
End Sub
Private Sub ReInitPrintTab()
    Dim strBuffer As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strColumnAlignmentStr As String

    Dim i As Integer
    Dim j As Integer

    PrintTab.ResetContent

    'header strings
    strHeaderString = "|"
    strHeaderLengthString = "80|"
    strColumnAlignmentStr = "L|"
    For i = 1 To 31
        strHeaderString = strHeaderString & CStr(i) & "|"
        strHeaderLengthString = strHeaderLengthString & "20|"
        strColumnAlignmentStr = strColumnAlignmentStr & "C|"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    strHeaderString = Left(strHeaderString, Len(strHeaderString) - 1)
    PrintTab.ColumnAlignmentString = strColumnAlignmentStr
    PrintTab.HeaderLengthString = strHeaderLengthString
    PrintTab.HeaderString = strHeaderString

    'header font
    PrintTab.HeaderFontSize = 14

    'cell objects
    PrintTab.CreateCellObj "WEEKEND", 14277081, vbBlack, 14, False, False, False, 1, "Courier"
    PrintTab.CreateCellObj "HOLIDAY", 8355711, vbBlack, 14, False, False, False, 1, "Courier"
    PrintTab.CreateCellObj "ENDMONTH", 12632256, vbBlack, 14, False, False, False, 1, "Courier"

    'fill the TAB with data of the stingray-grid
    For i = 1 To 14
        If i = 1 Then
            'strBuffer = strBuffer & LoadResString(1112) + " " + CStr(olView.ViewYear - 1) & "|"
            strBuffer = strBuffer & LoadResString(1112) + " " + CStr(imYear - 1) & "|"
        ElseIf i = 14 Then
            'strBuffer = strBuffer & LoadResString(1101) + " " + CStr(olView.ViewYear + 1) & "|"
            strBuffer = strBuffer & LoadResString(1101) + " " + CStr(imYear + 1) & "|"
        Else
            'strBuffer = strBuffer & LoadResString(1099 + i) + " " + CStr(olView.ViewYear) & "|"
            strBuffer = strBuffer & LoadResString(1099 + i) + " " + CStr(imYear) & "|"
        End If

        For j = 1 To 31 Step 1
            strBuffer = strBuffer & GridocxDetail.GetValueRowCol(i, j) & "|"
        Next j
        strBuffer = strBuffer & Chr(10)
    Next i
    PrintTab.InsertBuffer strBuffer, Chr(10)

    'setting the colors after filling the TAB with values
    For i = 0 To 13
        If i = 0 Then
            'ColorMonthPrintTab 12, (olView.ViewYear - 1), CByte(i)
            ColorMonthPrintTab 12, (imYear - 1), CByte(i)
        ElseIf i = 13 Then
            'ColorMonthPrintTab 1, (olView.ViewYear + 1), CByte(i)
            ColorMonthPrintTab 1, (imYear + 1), CByte(i)
        Else
            'ColorMonthPrintTab CByte(i), olView.ViewYear, CByte(i)
            ColorMonthPrintTab CByte(i), imYear, CByte(i)
        End If
    Next i
End Sub

Private Sub PrintTab_PrintFinishedTab()
    If imPrintCount < 1 Then
        imPrintCount = 1
        ReInitPrintTab
        PrintTab.PrintRepeatStatus 2    'distance to the next TAB: 2 lines
    Else
        PrintTab.PrintRepeatStatus -1   'stop printing
    End If
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   18.09.2001
' .
' . description :   coloring the PrintTab
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ColorMonthPrintTab(bMonth As Byte, iYear As Integer, bLine As Byte)

    Dim strSat As String
    Dim strSun As String
    Dim strHol As String

    Dim i As Integer

    ' get the saturdays and the sundays of the month in a string
    strSat = frmHidden.GetSaturdaysOfMonth(bMonth, iYear)
    strSun = frmHidden.GetSundaysOfMonth(bMonth, iYear)
    strHol = frmHiddenServerConnection.TabHOL.GetLinesByColumnValue(0, CStr(iYear) + CStr(Format(bMonth, "00")), 0)

    ' color the saturdays
    For i = 1 To ItemCount(strSat, ",") - 1
        PrintTab.SetCellProperty bLine, GetItem(strSat, i, ","), "WEEKEND"
    Next i

    ' color the sundays
    For i = 1 To ItemCount(strSun, ",") - 1
        PrintTab.SetCellProperty bLine, GetItem(strSun, i, ","), "WEEKEND"
    Next i

    ' color the holidays
    For i = 1 To ItemCount(strHol, ",")
        'PrintTab.SetCellProperty bLine, GetItem(strHol, i, ","), "HOLIDAY"
        PrintTab.SetCellProperty bLine, Mid(frmHiddenServerConnection.TabHOL.GetLineValues(GetItem(strHol, i, ",")), 7, 2), "HOLIDAY"
    Next i

    ' color the days not belonging to the month
    Select Case frmHidden.GetMonthDays(bMonth, iYear)
        Case 28:
            PrintTab.SetCellProperty bLine, 29, "ENDMONTH"
            PrintTab.SetCellProperty bLine, 30, "ENDMONTH"
            PrintTab.SetCellProperty bLine, 31, "ENDMONTH"
        Case 29:
            PrintTab.SetCellProperty bLine, 30, "ENDMONTH"
            PrintTab.SetCellProperty bLine, 31, "ENDMONTH"
        Case 30:
            PrintTab.SetCellProperty bLine, 31, "ENDMONTH"
    End Select
End Sub

