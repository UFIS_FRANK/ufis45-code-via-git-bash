VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmOptions 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   7080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9255
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7080
   ScaleWidth      =   9255
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "1061"
   Visible         =   0   'False
   Begin VB.ComboBox cmbAbsentPattern 
      Height          =   330
      Left            =   3360
      Sorted          =   -1  'True
      TabIndex        =   62
      Text            =   "<Default>"
      Top             =   5400
      Width           =   2175
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "1003"
      Height          =   375
      Left            =   4080
      TabIndex        =   37
      Tag             =   "1003"
      Top             =   6495
      Width           =   1095
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "1078"
      Height          =   375
      Left            =   6840
      TabIndex        =   3
      Tag             =   "1078"
      Top             =   5375
      Width           =   1000
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "1077"
      Height          =   375
      Left            =   5760
      TabIndex        =   2
      Tag             =   "1077"
      Top             =   5375
      Width           =   1000
   End
   Begin VB.ComboBox cmbViewName 
      Height          =   330
      Left            =   240
      Sorted          =   -1  'True
      TabIndex        =   0
      Text            =   "<Default>"
      Top             =   5400
      Width           =   2175
   End
   Begin TabDlg.SSTab tbsOptions 
      Height          =   4575
      Left            =   240
      TabIndex        =   5
      Tag             =   "1088"
      Top             =   240
      Width           =   8730
      _ExtentX        =   15399
      _ExtentY        =   8070
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "1086"
      TabPicture(0)   =   "frmOptions.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "1087"
      TabPicture(1)   =   "frmOptions.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "1088"
      TabPicture(2)   =   "frmOptions.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame3"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "1089"
      TabPicture(3)   =   "frmOptions.frx":035E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame4"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "1095"
      TabPicture(4)   =   "frmOptions.frx":037A
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame5"
      Tab(4).ControlCount=   1
      Begin VB.Frame Frame5 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3855
         Left            =   -74760
         TabIndex        =   41
         Top             =   480
         Width           =   8175
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   ">>"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   47
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   46
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   "<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   45
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   "<<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   44
            Top             =   2640
            Width           =   375
         End
         Begin VB.ListBox lbAbsenceTypeSelect 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   43
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbAbsenceType 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   42
            Top             =   480
            Width           =   3600
         End
         Begin VB.Label Label5 
            Caption         =   "1171"
            Height          =   255
            Index           =   4
            Left            =   4440
            TabIndex        =   61
            Tag             =   "1171"
            Top             =   240
            Width           =   2415
         End
         Begin VB.Label Label4 
            Caption         =   "1170"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   60
            Tag             =   "1170"
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.Frame Frame4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3855
         Left            =   -74760
         TabIndex        =   30
         Top             =   480
         Width           =   8175
         Begin VB.CheckBox ckbDontCare 
            Caption         =   "1099"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   51
            Tag             =   "1099"
            Top             =   3480
            Width           =   3615
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   ">>"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   36
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   35
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   "<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   34
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   "<<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   33
            Top             =   2640
            Width           =   375
         End
         Begin VB.ListBox lbContractFilterSelected 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   32
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbContractFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   31
            Top             =   480
            Width           =   3600
         End
         Begin VB.Label Label5 
            Caption         =   "1171"
            Height          =   255
            Index           =   3
            Left            =   4440
            TabIndex        =   59
            Tag             =   "1171"
            Top             =   240
            Width           =   2415
         End
         Begin VB.Label Label4 
            Caption         =   "1170"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   58
            Tag             =   "1170"
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.Frame Frame3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3855
         Left            =   -74760
         TabIndex        =   23
         Top             =   480
         Width           =   8175
         Begin VB.CheckBox ckbDontCare 
            Caption         =   "1098"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   50
            Tag             =   "1098"
            Top             =   3480
            Width           =   3615
         End
         Begin VB.ListBox lbFunctionFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   29
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbFunctionFilterSelected 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   28
            Top             =   480
            Width           =   3600
         End
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   "<<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   27
            Top             =   2640
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   "<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   26
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   25
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   ">>"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   24
            Top             =   840
            Width           =   375
         End
         Begin VB.Label Label5 
            Caption         =   "1171"
            Height          =   255
            Index           =   2
            Left            =   4440
            TabIndex        =   57
            Tag             =   "1171"
            Top             =   240
            Width           =   2415
         End
         Begin VB.Label Label4 
            Caption         =   "1170"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   56
            Tag             =   "1170"
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3855
         Left            =   -74760
         TabIndex        =   16
         Top             =   480
         Width           =   8175
         Begin VB.CheckBox ckbDontCare 
            Caption         =   "1097"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   49
            Tag             =   "1097"
            Top             =   3480
            Width           =   3615
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   ">>"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   22
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   21
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   "<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   20
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   "<<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   19
            Top             =   2640
            Width           =   375
         End
         Begin VB.ListBox lbDutyGroupFilterSelected 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   18
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbDutyGroupFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   17
            Top             =   480
            Width           =   3600
         End
         Begin VB.Label Label5 
            Caption         =   "1171"
            Height          =   255
            Index           =   1
            Left            =   4440
            TabIndex        =   55
            Tag             =   "1171"
            Top             =   240
            Width           =   2415
         End
         Begin VB.Label Label4 
            Caption         =   "1170"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   54
            Tag             =   "1170"
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3855
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   8175
         Begin VB.CheckBox ckbDontCare 
            Caption         =   "1096"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   48
            Tag             =   "1096"
            Top             =   3480
            Width           =   3615
         End
         Begin VB.ListBox lbOrgUnitFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            ItemData        =   "frmOptions.frx":0396
            Left            =   120
            List            =   "frmOptions.frx":0398
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   15
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbOrgUnitFilterSelect 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   10
            Top             =   480
            Width           =   3600
         End
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   "<<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   9
            Top             =   2640
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   "<"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   8
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   7
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   ">>"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   6
            Top             =   840
            Width           =   375
         End
         Begin VB.Label Label5 
            Caption         =   "1171"
            Height          =   255
            Index           =   0
            Left            =   4440
            TabIndex        =   53
            Tag             =   "1171"
            Top             =   240
            Width           =   2415
         End
         Begin VB.Label Label4 
            Caption         =   "1170"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   52
            Tag             =   "1170"
            Top             =   240
            Width           =   2415
         End
      End
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "1079"
      Height          =   375
      Left            =   7920
      TabIndex        =   4
      Tag             =   "1079"
      Top             =   5375
      Width           =   1000
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   255
      Left            =   3000
      TabIndex        =   1
      Top             =   5430
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   450
      _Version        =   393216
      Value           =   2000
      BuddyControl    =   "txtYear"
      BuddyDispid     =   196638
      OrigLeft        =   6855
      OrigTop         =   270
      OrigRight       =   7095
      OrigBottom      =   585
      Max             =   2050
      Min             =   2000
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtYear 
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   2520
      TabIndex        =   12
      Text            =   "2000"
      Top             =   5400
      Width           =   735
   End
   Begin MSComCtl2.UpDown UpDown2 
      Height          =   255
      Left            =   8760
      TabIndex        =   38
      Top             =   6510
      Visible         =   0   'False
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   450
      _Version        =   393216
      Value           =   3
      BuddyControl    =   "txtMaxAbsWeek"
      BuddyDispid     =   196639
      OrigLeft        =   6855
      OrigTop         =   270
      OrigRight       =   7095
      OrigBottom      =   585
      Max             =   99
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   0   'False
   End
   Begin VB.TextBox txtMaxAbsWeek 
      Enabled         =   0   'False
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   8160
      TabIndex        =   39
      Text            =   "3"
      Top             =   6480
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "1094"
      Height          =   255
      Left            =   3360
      TabIndex        =   40
      Tag             =   "1094"
      Top             =   5040
      Width           =   1815
   End
   Begin VB.Line Line1 
      BorderWidth     =   3
      X1              =   240
      X2              =   9000
      Y1              =   6120
      Y2              =   6120
   End
   Begin VB.Label Label3 
      Caption         =   "1081"
      Height          =   255
      Left            =   2520
      TabIndex        =   14
      Tag             =   "1081"
      Top             =   5040
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "1080"
      Height          =   255
      Left            =   240
      TabIndex        =   13
      Tag             =   "1080"
      Top             =   5040
      Width           =   2055
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim bmChanges As Boolean                ' did the user made any changes?
Dim bmRefresh As Boolean                ' do we need a refresh?
Dim imColIdx As Integer                 ' index of the view displayed in the options-window
Dim strSpace As String                  ' string displayed as separation between DPT1 and DPTN
Dim colDutyGroup As New Collection      ' this is for looking up the duty group;
                                        ' the DB-design is awfully bad, so we have to go
                                        ' from the STFTAB over the SORTAB over the ORGTAB to the ODGTAB
                                        ' to find the duty group

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub Form_Load()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    ' init the module variables
    bmChanges = False
    bmRefresh = False
    imColIdx = 0
    strSpace = "..."

    LoadResStrings Me       'load the strings

    ' look after the entries in the combobox "cmbViewName"
    cmbViewName.Clear
    For i = 1 To gcViews.Count Step 1
        cmbViewName.AddItem gcViews(i).ViewName
        If gcViews(i).ViewName = olDocument.omViewName Then 'cmbViewName.Text Then
            imColIdx = i
        End If
    Next i

    ' look after the entries in the combobox "cmbAbsentPattern"
    cmbAbsentPattern.Clear
    For i = 1 To gcMaxAbsent.Count Step 1
        cmbAbsentPattern.AddItem gcMaxAbsent(i).PatternName
    Next i


    ' init the combobox with the names of the views
    If olDocument.omViewName <> "" Then
        cmbViewName.Text = olDocument.omViewName
        txtYear.Text = gcViews(imColIdx).ViewYear
        cmbAbsentPattern.Text = gcViews(imColIdx).ViewAbsentPattern
        'txtMaxAbsWeek.Text = gcViews(imColIdx).ViewMaxAbsentPerWeek
        cmbAbsentPattern.Text = gcViews(imColIdx).ViewAbsentPattern
        ckbDontCare(0).Value = -1 * CInt(gcViews(imColIdx).DontCareOrgUnit)
        ckbDontCare(1).Value = -1 * CInt(gcViews(imColIdx).DontCareDutyGroup)
        ckbDontCare(2).Value = -1 * CInt(gcViews(imColIdx).DontCareFunction)
        ckbDontCare(3).Value = -1 * CInt(gcViews(imColIdx).DontCareContract)
'        If IsNumeric(txtYear.Text) = True Then
'            UpDown1.Value = txtYear.Text
'        End If
'        If IsNumeric(txtMaxAbsWeek.Text) = True Then
'            UpDown2.Value = txtMaxAbsWeek.Text
'        End If
    Else
        'cmbViewName.Text = "<Default>"
        txtYear.Text = Year(Now)
    End If

    ' init the registers (the riders) of the SSTab
    tbsOptions.TabCaption(0) = LoadResString(1086)
    tbsOptions.TabCaption(1) = LoadResString(1087)
    tbsOptions.TabCaption(2) = LoadResString(1088)
    tbsOptions.TabCaption(3) = LoadResString(1089)
    tbsOptions.TabCaption(4) = LoadResString(1095)
    tbsOptions.Tab = 0

    ' init the rest...
    HandleOrgFilter
    HandleDutyGroupFilter
    HandleFunctionFilter
    HandleContractFilter
    HandleAbsenceFilter
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.Form_Load", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.Form_Load", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleOrgFilter()
    On Error GoTo ErrHdl
    Dim i, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strDPT1 As String
    Dim strDPTN As String
    Dim str As String

    lbOrgUnitFilter.Clear
    lbOrgUnitFilterSelect.Clear

    With frmHiddenServerConnection
        For i = 0 To (.TabORG.GetLineCount - 1) Step 1
            blIsSelected = False
            strDPT1 = Trim(.TabORG.GetColumnValue(i, 0))
            strDPTN = Trim(.TabORG.GetColumnValue(i, 2))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 0 To olView.CountOrgUnit Step 1
                    If olView.ItemOrgUnit(j) = strDPT1 Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strDPT1
            While Len(str) < 8
                str = str + " "
            Wend
            If strDPTN <> "" Then
                str = str + strSpace + strDPTN
            End If

            'add the string
            If blIsSelected = False Then
                lbOrgUnitFilter.AddItem str
            Else
                lbOrgUnitFilterSelect.AddItem str
            End If
        Next i
    End With
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.HandleOrgFilter", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.HandleOrgFilter", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleDutyGroupFilter()
    On Error GoTo ErrHdl
    Dim i, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strODGC As String
    Dim strODGN As String
    Dim strORGU As String
    Dim str As String
    Dim strKey As String

    ' delete old entries in the collection
    For i = 1 To colDutyGroup.Count
        colDutyGroup.Remove 1
    Next i

    ' fill the collection
    With frmHiddenServerConnection
        For i = 0 To (.TabODG.GetLineCount - 1) Step 1
            strODGC = Trim(.TabODG.GetColumnValue(i, 0))
            strODGN = Trim(.TabODG.GetColumnValue(i, 1))
            strORGU = Trim(.TabODG.GetColumnValue(i, 2))

            'build the string
            str = strODGC
            While Len(str) < 8
                str = str + " "
            Wend
            If strODGN <> "" Then
                str = str + strSpace + strODGN
            End If
            colDutyGroup.Add Item:=str, key:=(strODGC + "@" + strORGU)
        Next i
    End With

    lbDutyGroupFilter.Clear
    lbDutyGroupFilterSelected.Clear

    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        Dim olView As New clsView
        Set olView = gcViews(imColIdx)

        ' add the selected ones
        For i = 1 To olView.CountDutyGroup Step 1
            strORGU = olView.ItemDutyGroup(i)
            If DoesKeyExist(colDutyGroup, strORGU) = True Then
                lbDutyGroupFilterSelected.AddItem colDutyGroup(strORGU)
                colDutyGroup.Remove (strORGU)
            End If
        Next i
    End If

    ' add the non-selected ones
    For i = 1 To colDutyGroup.Count
        lbDutyGroupFilter.AddItem colDutyGroup(i)
    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.HandleDutyGroupFilter", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.HandleDutyGroupFilter", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleFunctionFilter()
    On Error GoTo ErrHdl
    Dim i, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strFCTC As String
    Dim strFCTN As String
    Dim str As String

    lbFunctionFilter.Clear
    lbFunctionFilterSelected.Clear

    With frmHiddenServerConnection
        For i = 0 To (.TabPFC.GetLineCount - 1) Step 1
            blIsSelected = False
            strFCTC = Trim(.TabPFC.GetColumnValue(i, 1))
            strFCTN = Trim(.TabPFC.GetColumnValue(i, 2))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 1 To olView.CountFunctionEmployee Step 1
                    If olView.ItemFunctionEmployee(j) = strFCTC Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strFCTC
            While Len(str) < 8
                str = str + " "
            Wend
            If strFCTN <> "" Then
                str = str + strSpace + strFCTN
            End If

            'add the string
            If blIsSelected = False Then
                lbFunctionFilter.AddItem str
            Else
                lbFunctionFilterSelected.AddItem str
            End If
        Next i
    End With
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.HandleFunctionFilter", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.HandleFunctionFilter", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleContractFilter()
    On Error GoTo ErrHdl
    Dim i, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strCOTC As String
    Dim strCOTN As String
    Dim str As String

    lbContractFilter.Clear
    lbContractFilterSelected.Clear

    With frmHiddenServerConnection
        For i = 0 To (.TabCOT.GetLineCount - 1) Step 1
            blIsSelected = False
            strCOTC = Trim(.TabCOT.GetColumnValue(i, 0))
            strCOTN = Trim(.TabCOT.GetColumnValue(i, 1))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 1 To olView.CountContractType Step 1
                    If olView.ItemContractType(j) = strCOTC Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strCOTC
            While Len(str) < 8
                str = str + " "
            Wend
            If strCOTN <> "" Then
                str = str + strSpace + strCOTN
            End If

            'add the string
            If blIsSelected = False Then
                lbContractFilter.AddItem str
            Else
                lbContractFilterSelected.AddItem str
            End If
        Next i
    End With
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.HandleContractFilter", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.HandleContractFilter", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   14.05.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleAbsenceFilter()
    On Error GoTo ErrHdl
    Dim i, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strSDAC As String
    Dim strSDAN As String
    Dim str As String

    lbAbsenceType.Clear
    lbAbsenceTypeSelect.Clear

    With frmHiddenServerConnection
        For i = 0 To (.TabODA.GetLineCount - 1) Step 1
            blIsSelected = False
            strSDAC = Trim(.TabODA.GetColumnValue(i, 0))
            strSDAN = Trim(.TabODA.GetColumnValue(i, 4))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 0 To olView.CountAbsenceType Step 1
                    If olView.ItemAbsenceType(j) = strSDAC Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strSDAC
            While Len(str) < 8
                str = str + " "
            Wend
            If strSDAN <> "" Then
                str = str + strSpace + strSDAN
            End If

            'add the string
            If blIsSelected = False Then
                lbAbsenceType.AddItem str
            Else
                lbAbsenceTypeSelect.AddItem str
            End If
        Next i
    End With
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.HandleAbsenceFilter", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.HandleAbsenceFilter", Err
    Err.Clear
End Sub
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of bottom buttons (SAVE,DELETE,APPLY,CLOSE)
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' .
' . description :   saves the new view in the collection, not in the DB
' .
' . changes     :   05.04.2001  correcting of the logical sequence
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Private Sub cmdSave_Click()
    On Error GoTo ErrHdl
    If Trim(cmbViewName.Text) <> "" Then
        bmRefresh = True
        Dim blViewDoesAlreadyExist As Boolean

        Dim i As Integer

        Dim strOrgUnit As String
        Dim strDutyGroup As String
        Dim strFunction As String
        Dim strContract As String
        Dim strView As String
        Dim strAbs As String

        Dim olView As clsView

        'look if there is already a view with the name specified in the cmbViewName
        blViewDoesAlreadyExist = False
        Dim olElement
        For Each olElement In gcViews
            If olElement.ViewName = cmbViewName.Text Then
                blViewDoesAlreadyExist = True
                Exit For
            End If
        Next

        'Add a new view to the collection if there is no with the specified name
        If blViewDoesAlreadyExist = False Then
            MakeView
        End If

        ' ... and take the new settings ...
        ApplyAll

        Set olView = gcViews(imColIdx)

'        strView = olView.ViewName + ";0;" + CStr(olView.ViewYear) + "|" + CStr(olView.ViewMaxAbsentPerWeek) + _
'            "|" + CStr(ckbDontCare(0).Value) + "|" + CStr(ckbDontCare(1).Value) + _
'            "|" + CStr(ckbDontCare(2).Value) + "|" + CStr(ckbDontCare(3).Value)
        strView = olView.ViewName + ";0;" + CStr(olView.ViewYear) + "|" + "0" + _
            "|" + CStr(ckbDontCare(0).Value) + "|" + CStr(ckbDontCare(1).Value) + _
            "|" + CStr(ckbDontCare(2).Value) + "|" + CStr(ckbDontCare(3).Value) + _
            "|" + CStr(cmbAbsentPattern.Text)

        strOrgUnit = olView.ViewName + ";1;"
        For i = 1 To olView.CountOrgUnit
            strOrgUnit = strOrgUnit + olView.ItemOrgUnit(i) + "|"
        Next i

        strDutyGroup = olView.ViewName + ";2;"
        For i = 1 To olView.CountDutyGroup
            strDutyGroup = strDutyGroup + olView.ItemDutyGroup(i) + "|"
        Next i

        strFunction = olView.ViewName + ";3;"
        For i = 1 To olView.CountFunctionEmployee
            strFunction = strFunction + olView.ItemFunctionEmployee(i) + "|"
        Next i

        strContract = olView.ViewName + ";4;"
        For i = 1 To olView.CountContractType
            strContract = strContract + olView.ItemContractType(i) + "|"
        Next i

        strAbs = olView.ViewName + ";5;"
        For i = 1 To olView.CountAbsenceType
            strAbs = strAbs + olView.ItemAbsenceType(i) + "|"
        Next i

        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092)
        frmHiddenServerConnection.SaveView strView
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "."
        frmHiddenServerConnection.SaveView strOrgUnit
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + ".."
        frmHiddenServerConnection.SaveView strDutyGroup
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "..."
        frmHiddenServerConnection.SaveView strFunction
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "...."
        frmHiddenServerConnection.SaveView strContract
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "....."
        frmHiddenServerConnection.SaveView strAbs
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1093)

    Else
        MsgBox LoadResString(1084), vbCritical, LoadResString(1085)
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdSave_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdSave_Click", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdApply_Click()
    On Error GoTo ErrHdl
    If Trim(cmbViewName.Text) <> "" Then
        Dim i As Integer
        Dim blViewDoesAlreadyExist As Boolean
        bmRefresh = True

        'look if there is already a view with the name specified in the cmbViewName
        blViewDoesAlreadyExist = False
        For i = 1 To gcViews.Count Step 1
            If gcViews(i).ViewName = cmbViewName.Text Then
                blViewDoesAlreadyExist = True
                Exit For
            End If
        Next i

        'Add a new view to the collection if there is no with the specified name
        If blViewDoesAlreadyExist = False Then
            MakeView
        End If

        ' ... and take the new settings ...
        ApplyAll

    Else
        MsgBox LoadResString(1084), vbCritical, LoadResString(1085)
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdApply_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdApply_Click", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ApplyAll()
    On Error GoTo ErrHdl
    bmChanges = False   ' important!
    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        Dim i As Integer
        Dim ilPos As Integer
        Dim olDocument As frmDocument
        Set olDocument = Module1.fMainForm.ActiveForm

        ' look after the general things
        gcViews(imColIdx).ViewYear = txtYear.Text
        gcViews(imColIdx).ViewAbsentPattern = cmbAbsentPattern.Text
        'gcViews(imColIdx).ViewMaxAbsentPerWeek = txtMaxAbsWeek.Text
        gcViews(imColIdx).DontCareOrgUnit = ckbDontCare(0).Value
        gcViews(imColIdx).DontCareDutyGroup = ckbDontCare(1).Value
        gcViews(imColIdx).DontCareFunction = ckbDontCare(2).Value
        gcViews(imColIdx).DontCareContract = ckbDontCare(3).Value

        ' set the actual view to this one
        olDocument.omViewName = cmbViewName.Text

        ' look after the organisation units
        gcViews(imColIdx).RemoveAllOrgUnit
        For i = 0 To lbOrgUnitFilterSelect.ListCount - 1 Step 1
            ilPos = InStr(lbOrgUnitFilterSelect.List(i), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddOrgUnit Trim(Left(lbOrgUnitFilterSelect.List(i), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddOrgUnit Trim(lbOrgUnitFilterSelect.List(i))
            End If
        Next i

        ' look after the duty groups
        gcViews(imColIdx).RemoveAllDutyGroup
        Dim strODGC As String
        Dim strODGN As String
        Dim strLineNo As String
        Dim strLine As String
        For i = 0 To (lbDutyGroupFilterSelected.ListCount - 1)
            strODGC = GetItem(lbDutyGroupFilterSelected.List(i), 1, strSpace)
            strODGN = GetItem(lbDutyGroupFilterSelected.List(i), 2, strSpace)
            If strODGN = "" Then strODGN = " "
            strLineNo = frmHiddenServerConnection.TabODG.GetLinesByMultipleColumnValue("0,1", (strODGC + "," + strODGN), 0)
            If ItemCount(strLineNo, ",") = 1 Then
                strLine = frmHiddenServerConnection.TabODG.GetLineValues(GetItem(strLineNo, 1, ","))
                gcViews(imColIdx).AddDutyGroup (strODGC + "@" + GetItem(strLine, 3, ","))
            ElseIf ItemCount(strLineNo, ",") > 1 Then
                strLine = frmHiddenServerConnection.TabODG.GetLineValues(GetItem(strLineNo, 2, ","))
                gcViews(imColIdx).AddDutyGroup (strODGC + "@" + GetItem(strLine, 3, ","))
            End If
        Next i

        ' look after the function of the employees
        gcViews(imColIdx).RemoveAllFunctionEmployee
        For i = 0 To lbFunctionFilterSelected.ListCount - 1 Step 1
            ilPos = InStr(lbFunctionFilterSelected.List(i), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddFunctionEmployee Trim(Left(lbFunctionFilterSelected.List(i), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddFunctionEmployee Trim(lbFunctionFilterSelected.List(i))
            End If
        Next i

        ' look after the contract types
        gcViews(imColIdx).RemoveAllContractType
        For i = 0 To lbContractFilterSelected.ListCount - 1 Step 1
            ilPos = InStr(lbContractFilterSelected.List(i), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddContractType Trim(Left(lbContractFilterSelected.List(i), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddContractType Trim(lbContractFilterSelected.List(i))
            End If
        Next i

        ' look after the absence types
        gcViews(imColIdx).RemoveAllAbsenceType
        For i = 0 To lbAbsenceTypeSelect.ListCount - 1 Step 1
            ilPos = InStr(lbAbsenceTypeSelect.List(i), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddAbsenceType Trim(Left(lbAbsenceTypeSelect.List(i), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddAbsenceType Trim(lbAbsenceTypeSelect.List(i))
            End If
        Next i
        
        'look after the dont-care checkboxes
        gcViews(imColIdx).DontCareOrgUnit = ckbDontCare(0).Value
        gcViews(imColIdx).DontCareDutyGroup = ckbDontCare(1).Value
        gcViews(imColIdx).DontCareFunction = ckbDontCare(2).Value
        gcViews(imColIdx).DontCareContract = ckbDontCare(3).Value

    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.ApplyAll", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.ApplyAll", Err
    Err.Clear
End Sub

Private Sub MakeView()
    On Error GoTo ErrHdl
    Dim olView As New clsView
    Dim i As Integer

    'set the properties of the new view
    olView.ViewName = cmbViewName.Text
    gcViews.Add olView
    imColIdx = gcViews.Count
    Set olView = Nothing

    'add the new item to the combo-box
    cmbViewName.AddItem cmbViewName.Text

    ' look after the collection-index
'    For i = 1 To gcViews.Count Step 1
'        If gcViews(i).ViewName = cmbViewName.Text Then
'            imColIdx = i
'            Exit For
'        End If
'    Next i
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.MakeView", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.MakeView", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdDelete_Click()
    On Error GoTo ErrHdl
    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        bmRefresh = True
        Dim i As Integer
        frmHiddenServerConnection.DeleteView gcViews(imColIdx).ViewName
        gcViews(imColIdx).RemoveAllOrgUnit
        gcViews(imColIdx).RemoveAllDutyGroup
        gcViews(imColIdx).RemoveAllFunctionEmployee
        gcViews(imColIdx).RemoveAllContractType
        gcViews.Remove (imColIdx)

        If imColIdx > gcViews.Count Then
            imColIdx = gcViews.Count
        End If

        ' reinit the combo-box
        cmbViewName.Clear
        For i = 1 To gcViews.Count Step 1
            cmbViewName.AddItem gcViews(i).ViewName
        Next i
        If imColIdx > 0 Then
            cmbViewName.Text = gcViews(imColIdx).ViewName
        Else
            cmbViewName.Text = ""
        End If

        ' reinit the txtYear
        If Trim(cmbViewName.Text) <> "" Then
            txtYear.Text = gcViews(imColIdx).ViewYear
            'txtMaxAbsWeek.Text = gcViews(imColIdx).ViewMaxAbsentPerWeek
            cmbAbsentPattern.Text = gcViews(imColIdx).ViewAbsentPattern
            ckbDontCare(0).Value = -1 * CInt(gcViews(imColIdx).DontCareOrgUnit)
            ckbDontCare(1).Value = -1 * CInt(gcViews(imColIdx).DontCareDutyGroup)
            ckbDontCare(2).Value = -1 * CInt(gcViews(imColIdx).DontCareFunction)
            ckbDontCare(3).Value = -1 * CInt(gcViews(imColIdx).DontCareContract)
            If IsNumeric(txtYear.Text) = True Then
                UpDown1.Value = txtYear.Text
            End If
            If IsNumeric(txtMaxAbsWeek.Text) = True Then
                UpDown2.Value = txtMaxAbsWeek.Text
            End If
        End If

        HandleOrgFilter
        HandleDutyGroupFilter
        HandleFunctionFilter
        HandleContractFilter
        HandleAbsenceFilter
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdDelete_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdDelete_Click", Err
    Err.Clear
End Sub


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdClose_Click()
    On Error GoTo ErrHdl
    If bmChanges = False Then
        Unload Me
    Else
        Dim Response
        Response = MsgBox(LoadResString(1091), vbQuestion + vbYesNo, LoadResString(1090))

        Select Case Response
            Case vbYes
                cmdApply_Click
            Case vbNo
                bmChanges = False
        End Select
        Unload Me
    End If

    'Load the new view
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm
    If bmRefresh <> False Then
        olDocument.Form_Reload
        frmCalc.FillMainGrid
    End If
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdClose_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdClose_Click", Err
    Err.Clear
End Sub


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmbViewName_Click()
    On Error GoTo ErrHdl
    bmRefresh = True
    If bmChanges = True Then
        Dim Response
        Response = MsgBox(LoadResString(1091), vbQuestion + vbYesNo, LoadResString(1090))

        Select Case Response
            Case vbYes
                cmdApply_Click
            Case vbNo
                bmChanges = False
        End Select
    End If

    Dim i As Integer

    ' look for the correct collection index
    For i = 1 To gcViews.Count Step 1
        If gcViews(i).ViewName = cmbViewName.Text Then
            imColIdx = i
            Exit For
        End If
    Next i

    ' adapt all other controls
    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        txtYear.Text = gcViews(imColIdx).ViewYear
        'txtMaxAbsWeek.Text = gcViews(imColIdx).ViewMaxAbsentPerWeek
        cmbAbsentPattern.Text = gcViews(imColIdx).ViewAbsentPattern
        ckbDontCare(0).Value = -1 * CInt(gcViews(imColIdx).DontCareOrgUnit)
        ckbDontCare(1).Value = -1 * CInt(gcViews(imColIdx).DontCareDutyGroup)
        ckbDontCare(2).Value = -1 * CInt(gcViews(imColIdx).DontCareFunction)
        ckbDontCare(3).Value = -1 * CInt(gcViews(imColIdx).DontCareContract)
        If IsNumeric(txtYear.Text) = True Then
            UpDown1.Value = txtYear.Text
        End If
        If IsNumeric(txtMaxAbsWeek.Text) = True Then
            UpDown2.Value = txtMaxAbsWeek.Text
        End If
    End If
    HandleOrgFilter
    HandleDutyGroupFilter
    HandleFunctionFilter
    HandleContractFilter
    HandleAbsenceFilter
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmbViewName_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmbViewName_Click", Err
    Err.Clear
End Sub

'Private Sub txtMaxAbsWeek_Change()
'    'bmChanges = True
'    'bmRefresh = True
'End Sub
'Private Sub txtYear_Change()
'    'bmChanges = True
'    'bmRefresh = True
'End Sub
'Private Sub UpDown1_Change()
'    'bmChanges = True
'    'bmRefresh = True
'End Sub
'Private Sub UpDown2_Change()
'    'bmChanges = True
'    'bmRefresh = True
'End Sub


' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Organisation Unit
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapOrg_Click(Index As Integer)
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True

    Select Case Index
        Case 0:     'all to right
            While lbOrgUnitFilter.ListCount > 0
                lbOrgUnitFilterSelect.AddItem lbOrgUnitFilter.List(0)
                lbOrgUnitFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbOrgUnitFilter_DblClick

        Case 2:     'only selected to left
            lbOrgUnitFilterSelect_DblClick

        Case 3:     'all to left
            While lbOrgUnitFilterSelect.ListCount > 0
                lbOrgUnitFilter.AddItem lbOrgUnitFilterSelect.List(0)
                lbOrgUnitFilterSelect.RemoveItem (0)
            Wend
    End Select
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdSwapOrg_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdSwapOrg_Click", Err
    Err.Clear
End Sub


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub lbOrgUnitFilter_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    Dim j As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbOrgUnitFilter.ListCount
        If lbOrgUnitFilter.Selected(i) = True Then
            lbOrgUnitFilterSelect.AddItem lbOrgUnitFilter.List(i)
            lbOrgUnitFilter.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbOrgUnitFilter_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbOrgUnitFilter_DblClick", Err
    Err.Clear
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub lbOrgUnitFilterSelect_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbOrgUnitFilterSelect.ListCount
        If lbOrgUnitFilterSelect.Selected(i) = True Then
            lbOrgUnitFilter.AddItem lbOrgUnitFilterSelect.List(i)
            lbOrgUnitFilterSelect.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbOrgUnitFilter_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbOrgUnitFilter_DblClick", Err
    Err.Clear
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Duty Groups
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Private Sub cmdSwapDutyGroup_Click(Index As Integer)
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True

    Select Case Index
        Case 0:     'all to right
            While lbDutyGroupFilter.ListCount > 0
                lbDutyGroupFilterSelected.AddItem lbDutyGroupFilter.List(0)
                lbDutyGroupFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbDutyGroupFilter_DblClick

        Case 2:     'only selected to left
            lbDutyGroupFilterSelected_DblClick

        Case 3:     'all to left
            While lbDutyGroupFilterSelected.ListCount > 0
                lbDutyGroupFilter.AddItem lbDutyGroupFilterSelected.List(0)
                lbDutyGroupFilterSelected.RemoveItem (0)
            Wend
    End Select
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdSwapDutyGroup_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdSwapDutyGroup_Click", Err
    Err.Clear
End Sub

Private Sub lbDutyGroupFilter_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbDutyGroupFilter.ListCount
        If lbDutyGroupFilter.Selected(i) = True Then
            lbDutyGroupFilterSelected.AddItem lbDutyGroupFilter.List(i)
            lbDutyGroupFilter.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbDutyGroupFilter_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbDutyGroupFilter_DblClick", Err
    Err.Clear
End Sub

Private Sub lbDutyGroupFilterSelected_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbDutyGroupFilterSelected.ListCount
        If lbDutyGroupFilterSelected.Selected(i) = True Then
            lbDutyGroupFilter.AddItem lbDutyGroupFilterSelected.List(i)
            lbDutyGroupFilterSelected.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbDutyGroupFilterSelected_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbDutyGroupFilterSelected_DblClick", Err
    Err.Clear
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Function Employees
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapFunction_Click(Index As Integer)
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True

    Select Case Index
        Case 0:     'all to right
            While lbFunctionFilter.ListCount > 0
                lbFunctionFilterSelected.AddItem lbFunctionFilter.List(0)
                lbFunctionFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbFunctionFilter_DblClick

        Case 2:     'only selected to left
            lbFunctionFilterSelected_DblClick

        Case 3:     'all to left
            While lbFunctionFilterSelected.ListCount > 0
                lbFunctionFilter.AddItem lbFunctionFilterSelected.List(0)
                lbFunctionFilterSelected.RemoveItem (0)
            Wend
    End Select
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdSwapFunction_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdSwapFunction_Click", Err
    Err.Clear
End Sub

Private Sub lbFunctionFilter_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbFunctionFilter.ListCount
        If lbFunctionFilter.Selected(i) = True Then
            lbFunctionFilterSelected.AddItem lbFunctionFilter.List(i)
            lbFunctionFilter.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbFunctionFilter_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbFunctionFilter_DblClick", Err
    Err.Clear
End Sub

Private Sub lbFunctionFilterSelected_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbFunctionFilterSelected.ListCount
        If lbFunctionFilterSelected.Selected(i) = True Then
            lbFunctionFilter.AddItem lbFunctionFilterSelected.List(i)
            lbFunctionFilterSelected.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbFunctionFilterSelected_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbFunctionFilterSelected_DblClick", Err
    Err.Clear
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Contract type
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapContract_Click(Index As Integer)
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True

    Select Case Index
        Case 0:     'all to right
            While lbContractFilter.ListCount > 0
                lbContractFilterSelected.AddItem lbContractFilter.List(0)
                lbContractFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbContractFilter_DblClick

        Case 2:     'only selected to left
            lbContractFilterSelected_DblClick

        Case 3:     'all to left
            While lbContractFilterSelected.ListCount > 0
                lbContractFilter.AddItem lbContractFilterSelected.List(0)
                lbContractFilterSelected.RemoveItem (0)
            Wend
    End Select
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdSwapContract_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdSwapContract_Click", Err
    Err.Clear
End Sub

Private Sub lbContractFilter_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbContractFilter.ListCount
        If lbContractFilter.Selected(i) = True Then
            lbContractFilterSelected.AddItem lbContractFilter.List(i)
            lbContractFilter.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbContractFilter_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbContractFilter_DblClick", Err
    Err.Clear
End Sub

Private Sub lbContractFilterSelected_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbContractFilterSelected.ListCount
        If lbContractFilterSelected.Selected(i) = True Then
            lbContractFilter.AddItem lbContractFilterSelected.List(i)
            lbContractFilterSelected.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbContractFilterSelected_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbContractFilterSelected_DblClick", Err
    Err.Clear
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of absence type
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   14.05.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapAbsence_Click(Index As Integer)
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True

    Select Case Index
        Case 0:     'all to right
            While lbAbsenceType.ListCount > 0
                lbAbsenceTypeSelect.AddItem lbAbsenceType.List(0)
                lbAbsenceType.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbAbsenceType_DblClick

        Case 2:     'only selected to left
            lbAbsenceTypeSelect_DblClick

        Case 3:     'all to left
            While lbAbsenceTypeSelect.ListCount > 0
                lbAbsenceType.AddItem lbAbsenceTypeSelect.List(0)
                lbAbsenceTypeSelect.RemoveItem (0)
            Wend
    End Select
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.cmdSwapAbsence_Click", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.cmdSwapAbsence_Click", Err
    Err.Clear
End Sub

Private Sub lbAbsenceType_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbAbsenceType.ListCount
        If lbAbsenceType.Selected(i) = True Then
            lbAbsenceTypeSelect.AddItem lbAbsenceType.List(i)
            lbAbsenceType.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbAbsenceType_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbAbsenceType_DblClick", Err
    Err.Clear
End Sub

Private Sub lbAbsenceTypeSelect_DblClick()
    On Error GoTo ErrHdl
    Dim i As Integer
    bmChanges = True
    bmRefresh = True
    i = 0
    While i < lbAbsenceTypeSelect.ListCount
        If lbAbsenceTypeSelect.Selected(i) = True Then
            lbAbsenceType.AddItem lbAbsenceTypeSelect.List(i)
            lbAbsenceTypeSelect.RemoveItem (i)
        Else
            i = i + 1
        End If
    Wend
    Exit Sub
ErrHdl:
    'WriteErrToLog "c:\tmp\AbsPlanErr.txt", "frmOptions.lbAbsenceTypeSelect_DblClick", Err
    WriteErrToLog UFIS_TMP & "\AbsPlanErr.txt", "frmOptions.lbAbsenceTypeSelect_DblClick", Err
    Err.Clear
End Sub
