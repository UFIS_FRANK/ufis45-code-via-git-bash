// CoveragePfcSelectDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <CoveragePfcSelectDlg.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CoveragePfcSelectDlg dialog


CoveragePfcSelectDlg::CoveragePfcSelectDlg(CWnd* pParent /*=NULL*/, CUIntArray *popPfcUrnos)
	: CDialog(CoveragePfcSelectDlg::IDD, pParent)
{

	for(int i = 0; i < popPfcUrnos->GetSize(); i++)
	{
		omPfcUrnos.Add((*popPfcUrnos)[i]);
	}
	//{{AFX_DATA_INIT(CoveragePfcSelectDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

}


CoveragePfcSelectDlg::~CoveragePfcSelectDlg()
{

	omPfcData.DeleteAll();

	omPfcUrnos.RemoveAll();

}


void CoveragePfcSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CoveragePfcSelectDlg)
	DDX_Control(pDX, IDC_PFC_LIST, m_CL_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CoveragePfcSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CoveragePfcSelectDlg)
	ON_LBN_DBLCLK(IDC_PFC_LIST, OnDblclkPfcList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CoveragePfcSelectDlg message handlers

void CoveragePfcSelectDlg::OnOK() 
{
	int ilSel = m_CL_List.GetCurSel();

	if(ilSel ==  LB_ERR)
	{
		MessageBox(LoadStg(IDS_STRING1570), LoadStg(IDS_WARNING));
		return;
	}

	PFCDATA *prlPfc = (PFCDATA *)m_CL_List.GetItemDataPtr(ilSel);
	if(prlPfc != NULL)
	{
		omSelPfcCode = CString(prlPfc->Fctc);
	}
	else
	{
//		m_CL_List.GetText(ilSel,omSelPfcCode);
	}


	CDialog::OnOK();
}



BOOL CoveragePfcSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_CL_List.SetFont(&ogCourier_Regular_8);	
	
	PFCDATA *prlPfc;
	

	//ogPfcData.omData.Sort(ComparePfc);
	
	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING999));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING998));
	}

	SetWindowText(LoadStg(IDS_STRING61262));

	for(int  i = 0; i < omPfcUrnos.GetSize(); i++)
	{

		prlPfc = ogPfcData.GetPfcByUrno(omPfcUrnos[i]);

		if(prlPfc != NULL)
		{
			omPfcData.New(*prlPfc);

			char pclLine[250] = "";
			
			sprintf(pclLine, "%-7s  %s", prlPfc->Fctc, prlPfc->Fctn);
			int ilIndex = m_CL_List.AddString(pclLine);
			if (ilIndex != LB_ERR)
			{
				m_CL_List.SetItemDataPtr(ilIndex, (void *)prlPfc);
			}	
		}
		else
		{
/*********************
			CString olUrno;
			olUrno.Format("%ld",omPfcUrnos[i]);
			CString olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olUrno, "PFC", "GRPN");
			if(!olTmpFkt.IsEmpty())
			{
				char pclLine[250] = "";
				sprintf(pclLine, "%s", olTmpFkt);
				int ilIndex = m_CL_List.AddString(pclLine);
				if (ilIndex != LB_ERR)
				{
					m_CL_List.SetItemDataPtr(ilIndex, (void *)NULL);
				}	
			}
***********************/
		}

	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CoveragePfcSelectDlg::OnDblclkPfcList() 
{
	// TODO: Add your control notification handler code here
	int ilSel = m_CL_List.GetCurSel();

	if(ilSel ==  LB_ERR)
	{
		MessageBox(LoadStg(IDS_STRING1570), LoadStg(IDS_WARNING));
		return;
	}

	PFCDATA *prlPfc = (PFCDATA *)m_CL_List.GetItemDataPtr(ilSel);
	if(prlPfc != NULL)
	{
		omSelPfcCode = CString(prlPfc->Fctc);
	}
	else
	{
//		m_CL_List.GetText(ilSel,omSelPfcCode);
	}

	CDialog::OnOK();	
}
