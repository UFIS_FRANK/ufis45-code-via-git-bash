// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////


#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>



#include <CedaCfgData.h>
#include <CedaGegData.h>
#include <CedaPerData.h>
#include <CedaPfcData.h>
#include <CedaDemData.h>
#include <CedaBlkData.h>
#include <CedaBsdData.h>
#include <CedaMsdData.h>
#include <CedaSdgData.h>
#include <CedaSdtData.h>
#include <CedaSgmData.h>
#include <CedaSpfData.h>
#include <CedaStfData.h>
#include <CedaValData.h>
#include <CedaDrrData.h>
#include <ACProgressBar.h>

//#include "CedaDsrData.h"
#include <ConflictCheck.h>
#include <CCSClientWnd.h>

#include <DataSet.h>
#include <PrivList.h>
 
// start bch
#include <CCSBasic.h>
#include <CedaBasicData.h>

#include <CedaParData.h>
#include <CedaOrgData.h>
#include <CedaOdaData.h>
#include <CedaCotData.h>
#include <CedaCohData.h>
#include <CedaGrmData.h>
#include <CedaMfmData.h>
#include <CedaOacData.h>
#include <CedaPacData.h>
#include <CedaWgpData.h>
#include <CedaWisData.h>
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section
const char *pcgAppName = "Coverage";
CString ogCustomer;

CString ogAppName = "Coverage"; // Name of *.exe file of this
CString ogAppl = "COVERAGE"; //f�r die 8 Stelligen APPL und APPN Felder in der DB
CString ogGlobal = "GLOBAL";

char pcgUser[33];
char pcgPasswd[33];

char pcgHome[4] = "ZRH";
char pcgHelpPath[1024] = "C:\\UFIS\\SYSTEM";
char pcgHome4[5] = "EDDV";
char pcgTableExt[10] = "TAB";
bool bpUseGhdBc;
const CString ogAutoCoverageLog = CCSLog::GetTmpPath("\\AutoCoverage.log");

//uhi 11.10.00
CTime ogLoginTime		= -1;

extern bool bgCreateDemIsActive = false;
extern int bgCreateDemCount = 0;
extern int bgReleaseDemCount = 0;
extern bool bgRuleDiagnosticIsActive = false;

CCSLog          ogLog(pcgAppName);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, pcgAppName);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CBasicData      ogBasicData;
CCSBasic ogCCSBasic;
CedaBasicData	ogBCD("", &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);


DataSet			ogDataSet;
CBackGround		*pogBackGround;
CedaCfgData		ogCfgData;
CedaGegData		ogGegData;
CedaPerData		ogPerData;
CedaPfcData		ogPfcData;
CedaDemData		ogDemData;

CedaBlkData		ogBlkData;
CedaBsdData		ogBsdData;
CedaBsdData		ogAllBsdData;
CedaMsdData		ogMsdData;
CedaSdgData		ogSdgData;
CedaSdtData		ogSdtData;
CedaSdtData		ogSdtRosterData;
CedaSdtData		ogSdtShiftData;
CedaSgmData		ogSgmData;
CedaSpfData		ogSpfData;
CedaStfData		ogStfData;
CedaValData		ogValData;
CedaDrrData		ogDrrData;

CedaParData		ogParData;
CedaOrgData		ogOrgData;
CedaOdaData     ogOdaData;
CedaCotData		ogCotData;
CedaCohData		ogCohData;
CedaGrmData		ogGrmData;
CedaMfmData		ogMfmData;
CedaOacData		ogOacData;
CedaPacData		ogPacData;
CedaWgpData		ogWgpData;
CedaWisData		ogWisData;

ConflictCheck ogConflictData;
ConflictCheck ogCoverageConflictData;
bool bgIsModal = false;
bool bgKlebefunktion;
CJobDlg *pogJobDlg;

CedaFlightData ogFlightData;
CedaFlightData ogCovFlightData;
PrivList		ogPrivList;

//MWO 01.08.03 
CACProgressBar *pogProgressBarDlg;
//END MWO 01.08.03 
ofstream of_catch;
CString ogBackTrace = "";
WORD	ogBcRecursion = 0;

long  igWhatIfUrno;
CString ogWhatIfKey;
int   igDaysToRead;
int igFontIndex1;
int igFontIndex2;

bool bgNoScroll;
bool bgOnline = true;
bool bgIsButtonListMovable = false;
bool bgEinteilung = true;
bool bgEcChange;
bool bgAcChange;
bool bgNoAccept;
bool bgNoReturn;

CInitialLoadDlg *pogInitialLoad;

CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogSmallFonts_Regular_8;
CFont ogSmallFonts_Bold_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogCourier_Bold_10;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Bold_8;
CFont ogCourier_Regular_9;


CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;

CFont ogScalingFonts[30];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;


COLORREF ogBarColors[MAXCOLORS+1] = { BLACK,WHITE,GRAY,GREEN,RED,
	BLUE,SILVER,MAROON,OLIVE,NAVY,PURPLE,TEAL,LIME,YELLOW,FUCHSIA,AQUA,WHITE,
	BLACK,ORANGE,PGREEN,PBLUE,GREEN,
 GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,
 GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,
 GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,
 GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,GREEN,
 GREEN
	};

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;

CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

char cgYes = '+';
char cgNo = ' ';

COLORREF CCSClientWnd::lmBkColor = lgBkColor;

/////////////////////////////////////////////////////////////////////////////

void CreateBrushes()
{
	 int ilLc;

	ogColors[0] = BLACK;
	ogColors[1] = WHITE;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;
	ogColors[19] = GREEN;
	ogColors[20] = GREEN;

// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogBarColors[ilLc]);
	}

	// create a break job brush pattern
	delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK_RED);
	ogBrushs[21] = new CBrush(&omBitmap);

	delete ogBrushs[22];
	CBitmap omBitmap2;
	omBitmap2.LoadBitmap(IDB_BREAK_GRAY);
	ogBrushs[22] = new CBrush(&omBitmap2);

	delete ogBrushs[23];
	CBitmap omBitmap3;
	omBitmap3.LoadBitmap(IDB_BREAK_GREEN);
	ogBrushs[23] = new CBrush(&omBitmap3);

	delete ogBrushs[24];
	CBitmap omBitmap4;
	omBitmap4.LoadBitmap(IDB_BREAK_SILVER);
	ogBrushs[24] = new CBrush(&omBitmap4);

	ogColors[25] = LTGRAY;
	delete ogBrushs[25];
	ogBrushs[25] = new CBrush(ogColors[25]);
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}


void InitFont() 
{
    CDC dc;
    BOOL blRet;
	blRet = dc.CreateCompatibleDC(NULL);
//    ASSERT(dc.CreateCompatibleDC(NULL));

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
	blRet = ogSmallFonts_Regular_6.CreateFontIndirect(&logFont);
//    ASSERT(ogSmallFonts_Regular_6.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
	blRet = ogSmallFonts_Regular_7.CreateFontIndirect(&logFont);
//    ASSERT(ogSmallFonts_Regular_7.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, /*"Small Fonts"*/"MS LineDraw");
    blRet = ogSmallFonts_Regular_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Bold_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
	blRet = ogCourier_Regular_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Regular_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Bold_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    blRet = ogCourier_Regular_9.CreateFontIndirect(&logFont);

	for(int i = 0; i < 8; i++)
	{
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfQuality = PROOF_QUALITY;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Small Fonts"); 
		blRet = ogScalingFonts[i].CreateFontIndirect(&logFont);
	}
	for( i = 8; i < 20; i++)
	{ 
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		logFont.lfQuality = PROOF_QUALITY;

		lstrcpy(logFont.lfFaceName, "MS Sans Serif"); /*"MS Sans Serif""Arial"*/
		blRet = ogScalingFonts[i/*GANTT_S_FONT*/].CreateFontIndirect(&logFont);
	}

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    blRet = ogMSSansSerif_Regular_8.CreateFontIndirect(&logFont);

//Times	

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(30, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_30.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_16.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_12.CreateFontIndirect(&logFont);

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    blRet = ogTimesNewRoman_9.CreateFontIndirect(&logFont);

    dc.DeleteDC();
}


NoBroadcastSupport::NoBroadcastSupport()
{
	ogBcHandle.StopBc();
}

NoBroadcastSupport::~NoBroadcastSupport()
{
	ogBcHandle.StartBc();
}

