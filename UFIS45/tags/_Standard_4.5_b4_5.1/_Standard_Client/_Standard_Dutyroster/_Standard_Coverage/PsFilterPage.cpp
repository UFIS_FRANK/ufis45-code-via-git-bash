// PsAlcFilterPage.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <Coverage.h>
#include <PsAlcFilterPage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite CPsAlcFilterPage 

IMPLEMENT_DYNCREATE(CPsAlcFilterPage, CPropertyPage)

CPsAlcFilterPage::CPsAlcFilterPage() : CPropertyPage(CPsAlcFilterPage::IDD)
{
	//{{AFX_DATA_INIT(CPsAlcFilterPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}

CPsAlcFilterPage::~CPsAlcFilterPage()
{
}

void CPsAlcFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPsAlcFilterPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPsAlcFilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPsAlcFilterPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Zuordnungsmakros f�r Nachrichten ein
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPsAlcFilterPage 
