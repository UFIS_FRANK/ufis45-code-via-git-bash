#if !defined(AFX_SETUPSHEET_H__27A161D3_9D2F_11D6_820D_00010215BFDE__INCLUDED_)
#define AFX_SETUPSHEET_H__27A161D3_9D2F_11D6_820D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetupSheet.h : header file
//
#include <ColorSetupPage.h>
#include <OmmonSetupPage.h>

/////////////////////////////////////////////////////////////////////////////
// SetupSheet

class SetupSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(SetupSheet)

// Construction
public:
	SetupSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	SetupSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SetupSheet)
	public:
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~SetupSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(SetupSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	ColorSetupPage	omColorSetupPage;
	CommonSetupPage	omCommonSetupPage;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETUPSHEET_H__27A161D3_9D2F_11D6_820D_00010215BFDE__INCLUDED_)
