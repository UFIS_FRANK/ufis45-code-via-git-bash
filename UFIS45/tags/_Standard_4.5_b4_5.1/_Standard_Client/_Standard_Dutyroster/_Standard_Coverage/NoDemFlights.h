// Search.h: interface for the CSearch class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NODEMFLIGHTS_H__AA7520F3_4575_11D3_94FB_00001C018ACE__INCLUDED_)
#define AFX_NODEMFLIGHTS_H__AA7520F3_4575_11D3_94FB_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <CedaFlightData.h>
#include <CoverageDiaViewer.h>
#include <Search.h>

class CNoDemFlights 
{
public:
	CNoDemFlights();
	CNoDemFlights(CoverageDiagramViewer *popCallerView,const CStringArray * popTsrArray,const CMapPtrToPtr * popFlightDemMap,bool bpWithoutDems);
	virtual ~CNoDemFlights();

	bool FindFlightsWithoutDem(const CStringArray * popTsrArray,const CMapPtrToPtr * popFlightDemMap,bool bpWithoutDems = true );
private:
	CedaFlightData omFlights;
	CCSPtrArray<FLIGHTSEARCHDATA> omFoundFlight;
	CMapPtrToPtr omUrnoMap;
	CoverageDiagramViewer *pomViewer;
	CMapPtrToPtr omDemMap;


};

#endif // !defined(AFX_NODEMFLIGHTS_H__AA7520F3_4575_11D3_94FB_00001C018ACE__INCLUDED_)
