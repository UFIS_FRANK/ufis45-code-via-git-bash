#if !defined(AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_)
#define AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GxGridTab.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GxGridTab window
#ifndef _GXRESRC_H_
#include <gxresrc.h>
#endif

#ifndef _GXVW_H_
#include <gxvw.h>
#endif

#ifndef _GXEXT_H_
#include <gxext.h>
#endif

#ifndef _GXMSG_H_
#include <gxmsg.h>
#endif


#define WM_GRID_DBL_CKL (WM_USER+10050)

class GxGridTab : public CGXGridWnd
{
DECLARE_DYNCREATE(GxGridTab)

// Construction
public:
	GxGridTab(CWnd *popParent);
	GxGridTab();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GxGridTab)
	//}}AFX_VIRTUAL

	CString omGridName;
	CWnd *pomParent;
// Implementation
public:
	virtual ~GxGridTab();
	virtual void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	virtual BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	virtual BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);

	void SetGridName(CString opname)
	{
		omGridName = opname;
	};


	// Generated message map functions
protected:
	//{{AFX_MSG(GxGridTab)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_)
