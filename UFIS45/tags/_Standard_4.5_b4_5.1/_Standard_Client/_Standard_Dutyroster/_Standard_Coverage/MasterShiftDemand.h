#ifndef AFX_MASTERSHIFTDEMAND_H__A4C50921_461F_11D2_8DF0_0000C002916B__INCLUDED_
#define AFX_MASTERSHIFTDEMAND_H__A4C50921_461F_11D2_8DF0_0000C002916B__INCLUDED_

// MasterShiftDemand.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld MasterShiftDemand 

#include <resource.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <SdgGraphic.h>
#include <CedaSdgData.h>
#include <CedaBsdData.h>

struct MSDTIME
{
	CTime Time;
	bool  valid;
	MSDTIME()
	{
		valid = true;
	}
};

class MasterShiftDemand : public CDialog
{
// Konstruktion
public:
	MasterShiftDemand(CWnd* pParent = NULL, SDGDATA *prpSdg = NULL, CTime opStart = TIMENULL, CTime opEnd = TIMENULL);   // Standardkonstruktor
	~MasterShiftDemand();

	CTime omLoadStart;
	CTime omLoadEnd;
	//uhi 11.9.00 Stichtag
	CTime omValidInput;
	//CCSTable *pomTable;
	SdgGraphic *pomGraph;
	SDGDATA rmSdg;	
	SDGDATA rmOrgSdg;	
	CTime omStartTime;
	CTime omEndeTime;

	bool bmSdgChanged;
	//uhi 18.9.00
	//Nur noch Infofeld Display Begin
	//bool bmDispBegChanged;
	bool bmBegChanged;
	CTime omOrgDispBegi;
	CTime omBegi;
	CTime omEnde;
	CTime omDispBegi;
	int imDays;
	int imDura;
	int imPeri;
	int imWeeks;
	CString omDnam;
	CString omDvor;
	CString omAbge;
	CStringArray omRepi;
	bool bmReadOnly;
	//uhi 29.8.00
	bool bmCopy;

	bool bmCheckValidity;

	CString	omReplaceFunction;
	CString	omWithFunction;
	UINT	imCountOfCopies;

// Dialogfelddaten
	//{{AFX_DATA(MasterShiftDemand)
	enum { IDD = IDD_MASTER_SHIFT_DEMAND };
	CButton		m_CB_OK;
	//uhi 29.8.00
	CButton		m_Copy;
	CStatic		m_Helper;
	//CStatic		m_List;
	//CEdit		m_Days;
	CEdit		m_Begi;
	CEdit		m_DispBegi;
	CEdit		m_Dnam;
	CEdit		m_Dura;
	CEdit		m_Peri;
	CEdit		m_Ende;
	CComboBox	m_Vorg;
	CComboBox	m_Oper;
	CButton		m_BtnBegi;
	CButton		m_BtnEnde;
	CButton		m_BtnDispBegi;
	//}}AFX_DATA

	CCSPtrArray<MSDTIME> omDateArray;
	LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	bool CheckTime(CTime opTime, int *ipIdx = NULL);
	bool ListHasOverlapping(CUIntArray &ropIdxes);
	void ReCheckList();
// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(MasterShiftDemand)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	void CalcRepeatings();
	void CalcEndTime();
	void CalcBeginTime();	
	BOOL IsOperative();

	CString	ReplaceFunctionWith(const CString& ropFctc);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(MasterShiftDemand)
	virtual void OnOK();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	virtual BOOL OnInitDialog();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKillfocusBegi();
	afx_msg void OnKillfocusEnde();
	afx_msg void OnKillfocusDispBegi();
	afx_msg void OnKillfocusDura();
	afx_msg void OnKillfocusPeri();
	afx_msg void OnChangeDura();
	afx_msg void OnChangePeri();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnNew();
	afx_msg void OnCopy();
	afx_msg	BOOL OnTtnNeedText(UINT id,NMHDR * pTTTStruct,LRESULT * pResult);
	afx_msg void OnSelchangeOper();
	afx_msg void OnBegi();
	afx_msg void OnEnde();
	afx_msg void OnDispBegi();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	bool bmOperEnabled;
	int	 imLastInput;
	int	 imNextMonth;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_MASTERSHIFTDEMAND_H__A4C50921_461F_11D2_8DF0_0000C002916B__INCLUDED_
