#if !defined(AFX_CFLREADYDLG_H__B4F14F41_2AE8_11D4_9694_0050DAE32E8C__INCLUDED_)
#define AFX_CFLREADYDLG_H__B4F14F41_2AE8_11D4_9694_0050DAE32E8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CflReadyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCflReadyDlg dialog

class CCflReadyDlg : public CDialog
{
// Construction
public:
	CCflReadyDlg(CWnd* pParent = NULL);   // standard constructor
	CCflReadyDlg(UINT nID,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCflReadyDlg)
	enum { IDD = IDD_CFLREADYDLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCflReadyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCflReadyDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CFLREADYDLG_H__B4F14F41_2AE8_11D4_9694_0050DAE32E8C__INCLUDED_)
