 
// ComboBoxBar.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <ComboBoxBar.h>
#include <Basicdata.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CComboBoxBar

//--------------------------
CComboBoxBar::CComboBoxBar()
{
   pomComboBox = new CComboBox;
//   pomToggleButton = new CButton;
}


CComboBoxBar::~CComboBoxBar()
{
	delete pomComboBox;
//	delete pomToggleButton;
}


BOOL CComboBoxBar::CheckEnabled( int ipID )
{
	
	CToolBarCtrl&  bar = GetToolBarCtrl();

	BOOL blTest = bar.IsButtonEnabled(ipID);
	blTest = !bar.IsButtonIndeterminate(ipID);
	return blTest;
}
BOOL CComboBoxBar::DisableButton( int ipID )
{
	
	CToolBarCtrl&  bar = GetToolBarCtrl();
	bar.SetState( ipID,TBSTATE_INDETERMINATE);
	bar.EnableButton( ipID,FALSE);
	   
	BOOL blTest = bar.IsButtonEnabled(ipID);

	return TRUE;
}

BOOL CComboBoxBar::EnableButton( int ipID )
{
	
	CToolBarCtrl&  bar = GetToolBarCtrl();
	bar.SetState( ipID,TBSTATE_ENABLED);
	bar.EnableButton( ipID);
	   
	BOOL blTest = bar.IsButtonEnabled(ipID);

	return TRUE;
}

BOOL CComboBoxBar::HideButton( int ipID,BOOL bpHide)
{
	
	CToolBarCtrl&  bar = GetToolBarCtrl();
	if (bpHide)
	{
		bar.SetState( ipID, TBSTATE_HIDDEN);
	}
	else
	{
		bar.HideButton(ipID,FALSE);
	}
	 
	return TRUE;
}

BOOL CComboBoxBar::IsButtonHidden(int ipID)
{
	CToolBarCtrl&  bar = GetToolBarCtrl();
	return bar.IsButtonHidden(ipID);
}

BOOL  CComboBoxBar::LoadToolBar(UINT nIDResource) 
{
	BOOL err = CToolBar::LoadToolBar(nIDResource );

	if(!err)
		return err;

	//--- set style to flat
	ModifyStyle(0 , GetStyle() | TBSTYLE_FLAT | TBSTYLE_TOOLTIPS);


	//--- set text 
	CToolBarCtrl&  bar = GetToolBarCtrl();


	if(m_pStringMap)
	{
		delete m_pStringMap;
		       m_pStringMap = NULL;
	}

	TBBUTTON tb;
	for(int ilIndex = bar.GetButtonCount() - 1; ilIndex >= 0; ilIndex--)
	{
		 ZeroMemory(&tb , sizeof(TBBUTTON));
		 bar.GetButton(ilIndex , &tb);

		//--- if separator
		if((tb.fsStyle & TBSTYLE_SEP) == TBSTYLE_SEP)
		continue; 
		
		 //--- if ID invalid
		 if((tb.idCommand) == 0)
			 continue;
/*****************	
		 //--- if togglebutton
		 if(GetItemID(ilIndex) == IDC_TOGGLEBUTTON)
			 continue;
******************/
		 //--- if combobox
		 if(GetItemID(ilIndex) == IDP_PLACEHOLDER)
			 continue;

		 //--- create string
		 CString  strText;
		 LPCSTR   lpszButtonText = NULL;
		 CString  strButtonText(_T(""));
		 _TCHAR   seps[]= _T("\n");

		 
         strText = LoadStg(tb.idCommand);

		 if(!strText.IsEmpty())
		 {
             lpszButtonText = _tcstok((LPSTR) (LPCSTR) strText , seps); 

			 while(lpszButtonText)
			 {
                    strButtonText  = lpszButtonText;
					lpszButtonText = _tcstok(NULL , seps);
			 }
		 }

		 if(! strButtonText.IsEmpty())
		       BOOL err = SetButtonText(ilIndex , strButtonText);

		 //--- Resize  Buttons
		 CRect  rect(0,0,0,0);
		 CSize  sizeMax(80 , 45);

		 //--- Gr��te Ausdehnung finden 
		 /*
	     for(ilIndex = bar.GetButtonCount() -1; ilIndex >= 0; ilIndex --)
		 {
		      bar.GetItemRect(ilIndex , rect);
		      rect.NormalizeRect();

			  sizeMax.cx = max(rect.Size().cx , sizeMax.cx);
			  sizeMax.cy = max(rect.Size().cy , sizeMax.cy);
		 }
		 */

		 SetSizes(sizeMax , CSize(30 , 25));

	}

	return err;
}


/////////////////////////////////////////////////////////////////////////////
// CComboBoxBar message handlers

//---- Test eigene Message Map -------

 BEGIN_MESSAGE_MAP(CComboBoxBar , CToolBar)
   //{{AFX_MSG_MAP(CComboBoxBar) 
  //}}AFX_MSG_MAP  
 END_MESSAGE_MAP()


