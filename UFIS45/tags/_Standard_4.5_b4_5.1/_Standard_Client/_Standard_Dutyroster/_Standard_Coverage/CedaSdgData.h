#ifndef _CEDASDGDATA_H_
#define _CEDASDGDATA_H_

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
/////////////////////////////////////////////////////////////////////////////



struct SDGDATA {
	CTime 	 Begi;			// Beginn Schichtbedarf
	CTime	 Ende;			// Ende Schichtbedarf
	CTime	 Dbeg;			// Start Masterschicht
	char 	 Days[4]; 		// Zeitdauer in Tagen	
	char 	 Dnam[33];		// Bedarfsname	
	char 	 Dura[4]; 		// Wiederholungen 	
	char 	 Peri[4]; 		// Periodendistanz		
	char 	 Repi[801];		// Beginn der Wiederholungen	
	long	 Dvor;			// Urno des Vorg�ngers
	char	 Abge[2];		// Flag Vorg�nger abgel�st ('1' = Ja, '0' = Nein) 	
	long 	 Urno; 			// Urno 
	char	 Flags[4];		// Database status flags (contains Expd and Operation flags)

	bool    Expd;			// SDG released ? ('1' = Ja, '0' = Nein)
	char	Operation[2];	// Operational flag ('P' = Planned, 'O' = Operative, 'A' = Archived)	
	int		IsChanged;
	bool    IsSelected;
	bool	IsLoaded;		// Has it been loaded from DB ?
	SDGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Urno = 0;
		Begi = TIMENULL;
		Ende = TIMENULL;
		Operation[0] = 'O';

		IsChanged = 0;
		IsSelected = false;
		IsLoaded = false;
	}

	
 };	
//typedef struct SdtDataStruct SDGDATA;


// the broadcast CallBack function, has to be outside the CedaSdgData class
void ProcessSdgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


class CedaSdgData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<SDGDATA> omData;

    CMapPtrToPtr	omUrnoMap;
	CMapStringToPtr omDnamMap;
	CMapPtrToPtr	omOperativeMap;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// Operations
public:
    CedaSdgData();
	~CedaSdgData();
	void Register();

	void GetAllDnam(CStringArray &ropList, CTime opStartTime, CTime opEndTime);
	void GetAllDnam(CStringArray& ropList);
	
	long GetUrnoByDnam(CString opDnam);

	bool Read(char *pspWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<SDGDATA> *popSdg, char *pspWhere, char *pspFieldList, bool ipSYS);
	
	void PrepareSdgData(SDGDATA *prpSdt);
    bool InsertSdg(const SDGDATA *prpSdgData);    // used in PrePlanTable only
    bool UpdateSdg(const SDGDATA *prpSdgData);    // used in PrePlanTable only
	void ProcessSdgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void PrepareDataAfterRead(SDGDATA *prpSdg);
	void PrepareDataForWrite(SDGDATA *prpSdg);
	bool AddSdgInternal(SDGDATA *prpSdt);
	bool ClearAll();

	SDGDATA * GetSdgByUrno(long pcpUrno);
	SDGDATA * GetSdgByDnam(CString opDnam);
	bool ChangeSdgTime(long lpSdtUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave = false);
	bool SdtExist(long Urno);
	bool DnamExist(CString opDnam);
	bool InsertSdg(SDGDATA *prpSdgData, BOOL bpWithSave = FALSE);
	bool UpdateSdg(SDGDATA *prpSdgData, BOOL bpWithSave = FALSE);
	bool DeleteSdg(SDGDATA *prpSdgData, BOOL bpWithSave = FALSE);
	bool SaveSdg(SDGDATA *prpSdg);
	bool DeleteSdgInternal(SDGDATA *prpSdg);
	bool CheckSelected(long lpSdgu,bool blSetSelected);
	bool CheckLoaded(long lpSdgu,bool blSetLoaded);
	void ResetLoadFlag();	
	void Release(CCSPtrArray<SDGDATA> *popSDGList = NULL);

	void GetTimeFrames(long lpUrno, CCSPtrArray<TIMEFRAMEDATA> &opTimes);

private:
	bool bmOperEnabled;
};

extern CedaSdgData ogSdgData;

#endif
