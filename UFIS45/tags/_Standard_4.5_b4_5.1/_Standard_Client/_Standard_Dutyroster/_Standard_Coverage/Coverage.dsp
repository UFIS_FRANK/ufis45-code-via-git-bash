# Microsoft Developer Studio Project File - Name="Coverage" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Coverage - Win32 WatsonDebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Coverage.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Coverage.mak" CFG="Coverage - Win32 WatsonDebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Coverage - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 bchDebug" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 AutoCoverageDebug" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 AutoCoverageRelease" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 ADRDebug" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 ADRRelease" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 ADRAutoCoverageDebug" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 ADRAutoCoverageRelease" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 THODebug" (based on "Win32 (x86) Application")
!MESSAGE "Coverage - Win32 WatsonDebug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Coverage - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Coverage\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Coverage - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_bin\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Coverage\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "Coverage - Win32 bchDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Coverage___Win32_bchDebug"
# PROP BASE Intermediate_Dir "Coverage___Win32_bchDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Coverage___Win32_bchDebug"
# PROP Intermediate_Dir "Coverage___Win32_bchDebug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 C:\Ufis_Bin\ClassLib\Debug\CCSClass.lib C:\Ufis_Bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Ufis_Bin\ClassLib\Debug\CCSClass.lib C:\Ufis_Bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "Coverage - Win32 AutoCoverageDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Coverage___Win32_AutoCoverageDebug"
# PROP BASE Intermediate_Dir "Coverage___Win32_AutoCoverageDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\AutoCoverage\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\Ufis_bin\Debug\AutoCoverage.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "Coverage - Win32 AutoCoverageRelease"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Coverage___Win32_AutoCoverageRelease"
# PROP BASE Intermediate_Dir "Coverage___Win32_AutoCoverageRelease"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\AutoCoverage\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCSAUTOCOVERAGE" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386 /out:"c:\Ufis_bin\Release/AutoCoverage.exe"

!ELSEIF  "$(CFG)" == "Coverage - Win32 ADRDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Coverage___Win32_ADRDebug"
# PROP BASE Intermediate_Dir "Coverage___Win32_ADRDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_bin\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Coverage\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "USE_WINHELP" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "Coverage - Win32 ADRRelease"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Coverage___Win32_ADRRelease"
# PROP BASE Intermediate_Dir "Coverage___Win32_ADRRelease"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_bin\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Coverage\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "USE_WINHELP" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Coverage - Win32 ADRAutoCoverageDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Coverage___Win32_ADRAutoCoverageDebug"
# PROP BASE Intermediate_Dir "Coverage___Win32_ADRAutoCoverageDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\AutoCoverage\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /D "USE_WINHELP" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\Ufis_bin\Debug\AutoCoverage.exe" /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\Ufis_bin\Debug\AutoCoverage.exe" /pdbtype:sept

!ELSEIF  "$(CFG)" == "Coverage - Win32 ADRAutoCoverageRelease"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Coverage___Win32_ADRAutoCoverageRelease"
# PROP BASE Intermediate_Dir "Coverage___Win32_ADRAutoCoverageRelease"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\AutoCoverage\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCSAUTOCOVERAGE" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /D "USE_WINHELP" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386 /out:"c:\Ufis_bin\Release/AutoCoverage.exe"
# ADD LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386 /out:"c:\Ufis_bin\Release/AutoCoverage.exe"

!ELSEIF  "$(CFG)" == "Coverage - Win32 THODebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Coverage___Win32_THODebug"
# PROP BASE Intermediate_Dir "Coverage___Win32_THODebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "Coverage___Win32_THODebug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /GX /Z7 /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /pdb:none /debug /machine:I386

!ELSEIF  "$(CFG)" == "Coverage - Win32 WatsonDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Coverage___Win32_WatsonDebug"
# PROP BASE Intermediate_Dir "Coverage___Win32_WatsonDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\AutoCoverage\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "AUTOCOVERAGE" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\Ufis_bin\Debug\AutoCoverage.exe" /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"c:\Ufis_bin\Debug\AutoCoverage.exe"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "Coverage - Win32 Release"
# Name "Coverage - Win32 Debug"
# Name "Coverage - Win32 bchDebug"
# Name "Coverage - Win32 AutoCoverageDebug"
# Name "Coverage - Win32 AutoCoverageRelease"
# Name "Coverage - Win32 ADRDebug"
# Name "Coverage - Win32 ADRRelease"
# Name "Coverage - Win32 ADRAutoCoverageDebug"
# Name "Coverage - Win32 ADRAutoCoverageRelease"
# Name "Coverage - Win32 THODebug"
# Name "Coverage - Win32 WatsonDebug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\ACProgressBar.cpp
# End Source File
# Begin Source File

SOURCE=.\AutoCovDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AutoCoverage.cpp
# End Source File
# Begin Source File

SOURCE=.\AwDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\BaseShifts.cpp
# End Source File
# Begin Source File

SOURCE=.\BaseShiftsGroups.cpp
# End Source File
# Begin Source File

SOURCE=.\BaseShiftsPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\BasisschichtenDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BewertungPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSParam.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBlkData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaBsdData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCohData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCotData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDemData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDrrData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGegData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaGrmData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaMfmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaMsdData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaOacData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaOdaData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaOrgData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPacData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaParData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPerData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaPfcData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSdgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSdtData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaSpfData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaStfData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaValData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaWgpData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaWisData.cpp
# End Source File
# Begin Source File

SOURCE=.\CflReadyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CflReadyWithRuleDiagnosticsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CheckReferenz.Cpp
# End Source File
# Begin Source File

SOURCE=.\ChildView.cpp
# End Source File
# Begin Source File

SOURCE=.\Colors.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorSetupPage.cpp
# End Source File
# Begin Source File

SOURCE=.\ComboBoxBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ConflictCheck.cpp
# End Source File
# Begin Source File

SOURCE=.\CopyShiftRosterRequirementDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CovDiagram.cpp
# End Source File
# Begin Source File

SOURCE=.\Coverage.cpp
# End Source File
# Begin Source File

SOURCE=.\Coverage.rc
# End Source File
# Begin Source File

SOURCE=.\CoverageChart.cpp
# End Source File
# Begin Source File

SOURCE=.\CoverageDiaViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\CoverageGantt.cpp
# End Source File
# Begin Source File

SOURCE=.\CoverageGraphicWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\CoveragePfcSelectDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoverageStatistics.cpp
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.CPP
# End Source File
# Begin Source File

SOURCE=.\DataSet.cpp
# End Source File
# Begin Source File

SOURCE=.\DelSdgDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DispoDetailGantt.cpp
# End Source File
# Begin Source File

SOURCE=.\DispoDetailGanttViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightPage.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightTablePropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\GridFenster.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\GxGridTab.cpp
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InputData.cpp
# End Source File
# Begin Source File

SOURCE=.\JobDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoadTimeSpanDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MasstabDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MasterShiftDemand.cpp
# End Source File
# Begin Source File

SOURCE=.\NoDemFlights.cpp
# End Source File
# Begin Source File

SOURCE=.\OffsetSimulationPage.cpp
# End Source File
# Begin Source File

SOURCE=.\ommonSetupPage.cpp
# End Source File
# Begin Source File

SOURCE=.\onflictDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\PsAlcFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsAltFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsAptFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBewCicFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBewGatFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBewGhsFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBewPfcFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBewPstFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBewRegFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBewReqFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsBshFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsFlightPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsPosFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PSSearchFlightPage.cpp
# End Source File
# Begin Source File

SOURCE=.\PsTemplateFilterPage.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ResultTable.cpp
# End Source File
# Begin Source File

SOURCE=.\ScrollToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\Search.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchPropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchResultsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectDateDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SetupSheet.cpp
# End Source File
# Begin Source File

SOURCE=.\SimulationDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SimulationPage.cpp
# End Source File
# Begin Source File

SOURCE=.\SimulationSheet.cpp
# End Source File
# Begin Source File

SOURCE=.\SmoothSimulationPage.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\STGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\TABLE.CPP
# End Source File
# Begin Source File

SOURCE=.\UniEingabe.cpp
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\ACProgressBar.h
# End Source File
# Begin Source File

SOURCE=.\AutoCovDlg.h
# End Source File
# Begin Source File

SOURCE=.\AutoCoverage.h
# End Source File
# Begin Source File

SOURCE=.\AwDlg.h
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\BaseShifts.h
# End Source File
# Begin Source File

SOURCE=.\BaseShiftsGroups.h
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\BasisschichtenDlg.h
# End Source File
# Begin Source File

SOURCE=.\BewertungPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CCSParam.h
# End Source File
# Begin Source File

SOURCE=.\CedaBlkData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCohData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCotData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDemData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDrrData.h
# End Source File
# Begin Source File

SOURCE=.\CedaGegData.h
# End Source File
# Begin Source File

SOURCE=.\CedaGrmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaMfmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaMsdData.h
# End Source File
# Begin Source File

SOURCE=.\CedaOacData.h
# End Source File
# Begin Source File

SOURCE=.\CedaOdaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaOrgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaPacData.h
# End Source File
# Begin Source File

SOURCE=.\CedaParData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSdtData.h
# End Source File
# Begin Source File

SOURCE=.\CedaSgmData.h
# End Source File
# Begin Source File

SOURCE=.\CedaValData.h
# End Source File
# Begin Source File

SOURCE=.\CedaWgpData.h
# End Source File
# Begin Source File

SOURCE=.\CedaWisData.h
# End Source File
# Begin Source File

SOURCE=.\CflReadyDlg.h
# End Source File
# Begin Source File

SOURCE=.\CflReadyWithRuleDiagnosticsDlg.h
# End Source File
# Begin Source File

SOURCE=.\CheckReferenz.h
# End Source File
# Begin Source File

SOURCE=.\ChildView.h
# End Source File
# Begin Source File

SOURCE=.\Colors.h
# End Source File
# Begin Source File

SOURCE=.\ColorSetupPage.h
# End Source File
# Begin Source File

SOURCE=.\CopyShiftRosterRequirementDlg.h
# End Source File
# Begin Source File

SOURCE=.\CovDiagram.h
# End Source File
# Begin Source File

SOURCE=.\Coverage.h
# End Source File
# Begin Source File

SOURCE=.\CoverageChart.h
# End Source File
# Begin Source File

SOURCE=.\CoverageDiaViewer.h
# End Source File
# Begin Source File

SOURCE=.\CoverageGraphicWnd.h
# End Source File
# Begin Source File

SOURCE=.\CoverageStatistics.h
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.H
# End Source File
# Begin Source File

SOURCE=.\DelSdgDlg.h
# End Source File
# Begin Source File

SOURCE=.\DispoDetailGantt.h
# End Source File
# Begin Source File

SOURCE=.\FlightPage.h
# End Source File
# Begin Source File

SOURCE=.\FlightTablePropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.h
# End Source File
# Begin Source File

SOURCE=.\InputData.h
# End Source File
# Begin Source File

SOURCE=.\JobDlg.h
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoadTimeSpanDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MasstabDlg.h
# End Source File
# Begin Source File

SOURCE=.\OffsetSimulationPage.h
# End Source File
# Begin Source File

SOURCE=.\ommonSetupPage.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\PsAltFilterPage.h
# End Source File
# Begin Source File

SOURCE=.\PsBewPstFilterPage.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\Search.h
# End Source File
# Begin Source File

SOURCE=.\SearchPropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\SearchResultsDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelectDateDlg.h
# End Source File
# Begin Source File

SOURCE=.\SetupSheet.h
# End Source File
# Begin Source File

SOURCE=.\SimulationPage.h
# End Source File
# Begin Source File

SOURCE=.\SimulationSheet.h
# End Source File
# Begin Source File

SOURCE=.\SmoothSimulationPage.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\STGrid.h
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\abb.bmp
# End Source File
# Begin Source File

SOURCE=.\res\about.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00009.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00010.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00011.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00012.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00013.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00014.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00015.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00015Rot.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00016.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00017.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00018.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00019.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00020.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00022.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00024.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00025.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00026.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00027.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00028.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00030.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00031.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp3dlin.bmp
# End Source File
# Begin Source File

SOURCE=.\res\break_gr.bmp
# End Source File
# Begin Source File

SOURCE=.\res\break_re.bmp
# End Source File
# Begin Source File

SOURCE=.\res\break_si.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Coverage.ico
# End Source File
# Begin Source File

SOURCE=.\res\Coverage.rc2
# End Source File
# Begin Source File

SOURCE=.\res\exclamat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ganttcha.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\left_bar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\login.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\monohaj_de.bmp
# End Source File
# Begin Source File

SOURCE=.\res\monohaj_eng.bmp
# End Source File
# Begin Source File

SOURCE=.\res\monoufis.bmp
# End Source File
# Begin Source File

SOURCE=.\res\right_ba.bmp
# End Source File
# Begin Source File

SOURCE=.\res\rms.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Rms01.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbarNormal.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbarRot.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufis.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufis_sma.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UFISBITMAP.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufisintro256.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ufislogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\viewtemp.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\DataComp.avi
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
