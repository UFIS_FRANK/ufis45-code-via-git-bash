// BaseShiftsPropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <Coverage.h>
#include <CCSGlobl.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <BaseShiftsPropertySheet.h>
#include <StringConst.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNAMIC(BaseShiftsPropertySheet, BasePropertySheet)

static UINT WM_FINDREPLACE = ::RegisterWindowMessage(FINDMSGSTRING);

/////////////////////////////////////////////////////////////////////////////
// BaseShiftsPropertySheet
//

BEGIN_MESSAGE_MAP(BaseShiftsPropertySheet, BasePropertySheet)
	//{{AFX_MSG_MAP(BaseShiftsPropertySheet)
	ON_BN_CLICKED(IDC_SHEET_FIND, OnFind)
    ON_REGISTERED_MESSAGE( WM_FINDREPLACE, OnFindMsg )
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BaseShiftsPropertySheet::BaseShiftsPropertySheet(CString opCalledFrom,CString opCaption, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(opCaption, pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_PsBshFilterPage);
	pomViewer = popViewer; 
	pomFindDlg = NULL;


	m_PsBshFilterPage.SetCaption(LoadStg(COV_BTN_BASICSHIFTS));
} 

void BaseShiftsPropertySheet::LoadDataFromViewer()
{
	if (pomViewer != NULL)
	{
		pomViewer->GetFilter("BSH",m_PsBshFilterPage.omSelectedItems);

	}
}

void BaseShiftsPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{

	if (pomViewer != NULL)
	{
		pomViewer->SetFilter("BSH",m_PsBshFilterPage.omSelectedItems);
	}

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet message handlers

BOOL BaseShiftsPropertySheet::OnInitDialog() 
{
	BOOL bResult = BasePropertySheet::OnInitDialog();

	CRect rcApply;
	GetDlgItem(IDC_SHEET_APPLY)->GetWindowRect(rcApply);
	ScreenToClient(rcApply);

	CButton *polFind = new CButton;

	polFind->Create(LoadStg(IDS_STRING1965), WS_VISIBLE | BS_PUSHBUTTON,
		CRect(rcApply.right + 2, rcApply.top, rcApply.right + 2 + rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_FIND);
    polFind->SetFont(GetDlgItem(IDCANCEL)->GetFont());

	return bResult;
}

BOOL BaseShiftsPropertySheet::DestroyWindow() 
{
	delete (CButton *)GetDlgItem(IDC_SHEET_FIND);

	return BasePropertySheet::DestroyWindow();
}

void BaseShiftsPropertySheet::OnFind()
{
	if (pomFindDlg == NULL)
	{
		pomFindDlg = new CFindReplaceDialog();
		pomFindDlg->Create(TRUE,"",NULL,FR_DOWN,this);

		pomFindDlg->ShowWindow(SW_SHOWNORMAL);
	}
	else if (pomFindDlg->IsWindowVisible() == FALSE)
	{
		pomFindDlg->ShowWindow(SW_SHOWNORMAL);
	}
	else
	{
		pomFindDlg->ShowWindow(SW_HIDE);
	}

}

LRESULT BaseShiftsPropertySheet::OnFindMsg(WPARAM wParam, LPARAM lParam)
{
	CFindReplaceDialog *polFindDlg = CFindReplaceDialog::GetNotifier(lParam);

	if (polFindDlg != NULL)
	{
		if (polFindDlg->IsTerminating())
		{
//			delete pomFindDlg;
			pomFindDlg = NULL;
		}
		else if (polFindDlg->FindNext())
		{
			m_PsBshFilterPage.Find(polFindDlg->m_fr.lpstrFindWhat,polFindDlg->MatchCase(),polFindDlg->MatchWholeWord(),polFindDlg->SearchDown());
		}
	}

	return 0;
}

