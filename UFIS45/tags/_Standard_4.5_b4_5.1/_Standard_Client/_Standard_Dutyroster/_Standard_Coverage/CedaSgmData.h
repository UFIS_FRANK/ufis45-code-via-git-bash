// Class for Blocked Times
#ifndef _CEDASGMDATA_H_
#define _CEDASGMDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SgmDataStruct
{
	long	Urno;		// Unique Record Number
	char	Tabn[10];	// Table name
	long	Ugty;		// Urno of group type	
	long	Usgr;		// Urno of group name
	long	Uval;		// Urno of table value
	char	Valu[14];	// Value	

	SgmDataStruct(void)
	{
		Urno = 0L;
		strcpy(Tabn,"");
		Ugty = 0L;
		Usgr = 0L;
		Uval = 0L;
		strcpy(Valu,"");
	}
};

typedef struct SgmDataStruct SGMDATA;

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaSgmData: public CCSCedaData
{

// Attributes
public:
	CString GetTableName(void);

// Operations
public:
	CedaSgmData();
	~CedaSgmData();

	BOOL		ReadSgmData(const CTime& ropFrom,const CTime& ropTo);
	SGMDATA*	GetSgmByUrno(long lpUrno);
	int			GetSgmByUsgr(const CString& ropTable,long lpUrno,CCSPtrArray<SGMDATA>& ropSgm);
	int			GetCountOfRecords();
	void		ProcessSgmBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	SGMDATA*	AddSgmInternal(SGMDATA &rrpSgm);
	void		DeleteSgmInternal(SGMDATA *prpSgm);
	void		ClearAll();

private:
    CCSPtrArray<SGMDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapPtrToPtr			omUsgrMap;
};


extern CedaSgmData ogSgmData;
#endif _CEDASGMDATA_H_
