
// ComboBoxBar.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <ComboBoxBar.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CComboBoxBar

//--------------------------
CComboBoxBar::CComboBoxBar()
{
   pomComboBox = 0;
   pomToggleButton = 0;
   pomToolBar = 0;
}


CComboBoxBar::~CComboBoxBar()
{
}



BOOL  CComboBoxBar::LoadToolBar(UINT nIDResource) 
{
	pomToolBar = AddScrollToolBar ( nIDResource );

	if(!pomToolBar)
		return FALSE;

	//--- set style to flat
	pomToolBar->ModifyStyle(0 , TBSTYLE_TOOLTIPS);

	//--- set text 
	CToolBarCtrl&  bar = pomToolBar->GetToolBarCtrl();


	TBBUTTON tb;
	for(int ilIndex = bar.GetButtonCount() - 1; ilIndex >= 0; ilIndex--)
	{
		 ZeroMemory(&tb , sizeof(TBBUTTON));
		 bar.GetButton(ilIndex , &tb);

		//--- if separator
		if((tb.fsStyle & TBSTYLE_SEP) == TBSTYLE_SEP)
		continue; 
		
		 //--- if ID invalid
		 if((tb.idCommand) == 0)
			 continue;
	
		 //--- create string
		 CString  strText;
		 LPCSTR   lpszButtonText = NULL;
		 CString  strButtonText(_T(""));
		 _TCHAR   seps[]= _T("\n");

		 
		//strText.LoadString(tb.idCommand);
		strText.LoadString(tb.idCommand);

		if(!strText.IsEmpty())
		{
			lpszButtonText = _tcstok((LPSTR) (LPCSTR) strText , seps); 

			while(lpszButtonText)
			{
				strButtonText  = lpszButtonText;
				lpszButtonText = _tcstok(NULL , seps);
			}
		}

		if(! strButtonText.IsEmpty())
			pomToolBar->SetButtonText(ilIndex , strButtonText);

	}

	//--- Resize  Buttons
	CSize  sizeMax(80 , 45);

	pomToolBar->SetSizes(sizeMax , CSize(30 , 25));

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CComboBoxBar message handlers

//---- Test eigene Message Map -------

 BEGIN_MESSAGE_MAP(CComboBoxBar , CScrollRebar)
   //{{AFX_MSG_MAP(CComboBoxBar) 
  //}}AFX_MSG_MAP  
 END_MESSAGE_MAP()


