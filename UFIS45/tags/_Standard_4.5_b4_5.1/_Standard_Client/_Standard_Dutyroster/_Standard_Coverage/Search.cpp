// Search.cpp: implementation of the CSearch class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <Coverage.h>
#include <Search.h>
#include <CovDiagram.h>
#include <Jobdlg.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSearch::CSearch()
{
	pomResultDlg	= NULL;
	pomTsrArray		= NULL;
	pomFlightDemMap = NULL;

}

CSearch::~CSearch()
{
	omFlights.ClearAll();
	omUrnoMap.RemoveAll();
	omSearchFlight.DeleteAll();
	omDemMap.RemoveAll();

	if (pomResultDlg != NULL)
	{
		pomResultDlg->DestroyWindow();
		delete pomResultDlg;
		pomResultDlg = NULL;
	}


}

static int CompareFlights(const FLIGHTSEARCHDATA **e1, const FLIGHTSEARCHDATA **e2)
{

	return(strcmp((**e1).SortTifa,(**e2).SortTifa));
	
}



bool CSearch::SearchFlightData(CString opWhere)
{
	omFlights.ClearAll();
	omUrnoMap.RemoveAll();
	omSearchFlight.DeleteAll();
	CWaitCursor olWait;

	if (!omFlights.ReadRotations(opWhere.GetBuffer(0), true))
	{
		return false;
	}

	int ilFlightCount = omFlights.omData.GetSize();



	if (ilFlightCount == 0)
	{
		MessageBox(NULL, LoadStg(IDS_STRING61215),LoadStg(IDS_WARNING), MB_OK);
		olWait.Restore();
		return false;

	}

	for(int ilIndex = 0; ilIndex < ilFlightCount;ilIndex++)
	{
		FLIGHTDATA *prlDummyFlight= NULL;
		FLIGHTDATA *prlFlightA= NULL;
		FLIGHTDATA *prlFlightD= NULL;
		FLIGHTDATA *prlTmpFlight = &omFlights.omData[ilIndex];

		if(prlTmpFlight != NULL)
		{
			if (omUrnoMap.Lookup((void *)prlTmpFlight->Urno,(void *& )prlDummyFlight) == FALSE)
			{
				omUrnoMap.SetAt((void *)prlTmpFlight->Urno,prlTmpFlight);
			
				if(strcmp(prlTmpFlight->Adid, "D") == 0)
				{
					prlFlightD = prlTmpFlight;
					prlFlightA = omFlights.GetJoinFlight(prlTmpFlight);
					if(prlFlightA != NULL)
					{
						if (omUrnoMap.Lookup((void *)prlFlightA->Urno,(void *& )prlDummyFlight) == FALSE)
						{
							omUrnoMap.SetAt((void *)prlFlightA->Urno,prlFlightA);
						}
						else
						{
							prlFlightA = NULL;
						}
					}
				}
				else if(strcmp(prlTmpFlight->Adid, "A") == 0)
				{
					prlFlightA = prlTmpFlight;
					prlFlightD = omFlights.GetJoinFlight(prlTmpFlight);
					if(prlFlightD != NULL)
					{
						if (omUrnoMap.Lookup((void *)prlFlightD->Urno,(void *& )prlDummyFlight) == FALSE)
						{
							omUrnoMap.SetAt((void *)prlFlightD->Urno,prlFlightD);
						}
						else
						{
							prlFlightD = NULL;
						}
					}

				}
				else if(strcmp(prlTmpFlight->Adid, "B") == 0) //Rundfl�ge erstmal ignorieren!!
				{
					prlFlightA = NULL;
					prlFlightD = NULL;
				}
			}
		
			FLIGHTSEARCHDATA rlSearchFlightData;

			if(prlFlightD != NULL)
			{
				strcpy(rlSearchFlightData.Act3d,prlFlightD->Act3);
				rlSearchFlightData.Furn = prlFlightD->Urno;
				rlSearchFlightData.Urnd = prlFlightD->Urno;
				strcpy(rlSearchFlightData.Flnod,prlFlightD->Flno);
				strcpy(rlSearchFlightData.Regnd,prlFlightD->Regn);
				strcpy(rlSearchFlightData.Dest,prlFlightD->Des3);
				strcpy(rlSearchFlightData.PosD,prlFlightD->Pstd);
				CTime olStod = prlFlightD->Stod;

				CString olTime = olStod.Format("%d.%m.%y - %H%M");
				strcpy(rlSearchFlightData.Tifd,olTime);
				olTime = olStod.Format("%Y%m%d%H%M");
				strcpy(rlSearchFlightData.SortTifd,olTime);
				strcpy(rlSearchFlightData.SortTifa,olTime);

			}
			if(prlFlightA != NULL)
			{
				strcpy(rlSearchFlightData.Act3a,prlFlightA->Act3);
				if(prlFlightD == NULL)
				{
					rlSearchFlightData.Furn = prlFlightA->Urno;
				}
				rlSearchFlightData.Urna = prlFlightA->Urno;
				strcpy(rlSearchFlightData.Flnoa,prlFlightA->Flno);
				strcpy(rlSearchFlightData.Regna,prlFlightA->Regn);
				strcpy(rlSearchFlightData.Orig,prlFlightA->Org3);
				strcpy(rlSearchFlightData.PosA,prlFlightA->Psta);

				CTime olStoa = prlFlightA->Stoa;

				CString olTime = olStoa.Format("%d.%m.%y - %H%M");
				strcpy(rlSearchFlightData.Tifa,olTime);
				olTime = olStoa.Format("%Y%m%d%H%M");
				strcpy(rlSearchFlightData.SortTifa,olTime);
				if(prlFlightD == NULL)
				{
					strcpy(rlSearchFlightData.SortTifd,olTime);
				}


			}
			if((prlFlightD != NULL || prlFlightA != NULL))
			{
				bool blAIsLoaded = true;
				bool blDIsLoaded = true;
				if (rlSearchFlightData.Urna != 0L)
				{
					if(ogCovFlightData.GetFlightByUrno(rlSearchFlightData.Urna) == NULL)
					{
						blAIsLoaded = false;
					}
				}
				if (rlSearchFlightData.Urnd != 0L)
				{
					if(ogCovFlightData.GetFlightByUrno(rlSearchFlightData.Urnd) == NULL)
					{
						blDIsLoaded = false;
					}
				}

				rlSearchFlightData.IsLoaded = (blDIsLoaded && blAIsLoaded);
				omSearchFlight.NewAt(omSearchFlight.GetSize(),rlSearchFlightData);
			}
		}
	}

	if(omSearchFlight.GetSize() == 1)
	{
		long llUrna = omSearchFlight[0].Urna;
		long llUrnd = omSearchFlight[0].Urnd;
		if( pogJobDlg == NULL )
		{
			pogJobDlg = new CJobDlg(NULL );
		}
		else
		{
			pogJobDlg->SetActiveWindow();
		}

		
		FLIGHTDATA *prlFlightA = omFlights.GetFlightByUrno(llUrna);
		FLIGHTDATA *prlFlightD = omFlights.GetFlightByUrno(llUrnd);

		POSITION rlPos;

		DEMDATA * prlDem;
		omDemMap.RemoveAll();
		if(pomFlightDemMap != NULL)
		{
			CMapPtrToPtr *polDemMap;
			if(llUrna > 0)
			{
				if(pomFlightDemMap->Lookup((void *)llUrna,(void *&)polDemMap)  == TRUE)
				{	
					for ( rlPos =  polDemMap->GetStartPosition(); rlPos != NULL; )
					{
						long llUrno;
						polDemMap->GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) prlDem);
						if(prlDem != NULL)
						{
							omDemMap.SetAt((void *)prlDem->Urno,prlDem);
						}
					}
				}
			}
			if(llUrnd > 0)
			{
				if(pomFlightDemMap->Lookup((void *)llUrnd,(void *&)polDemMap)  == TRUE)
				{	
					for ( rlPos =  polDemMap->GetStartPosition(); rlPos != NULL; )
					{
						long llUrno;
						polDemMap->GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) prlDem);
						if(prlDem != NULL)
						{
							omDemMap.SetAt((void *)prlDem->Urno,prlDem);
						}
					}
				}
			}
		}

		

		pogJobDlg->InitFlightFields((CWnd*)pogCoverageDiagram,pomTsrArray, &omDemMap, prlFlightA,prlFlightD, llUrna, llUrnd,NULL);
		omSearchFlight.DeleteAll();
		return false;
	}
	else
	{
		omSearchFlight.Sort(CompareFlights);
		if(pomResultDlg != NULL)
		{
			pomResultDlg->DestroyWindow();
			delete pomResultDlg;
			pomResultDlg = NULL;
		}

		pomResultDlg = new SearchResultsDlg(omSearchFlight,NULL,opWhere);
	
		pomResultDlg->AttachTsrArray(pomTsrArray);
		pomResultDlg->AttachFlightDemMap(pomFlightDemMap);
		pomResultDlg->AttachFlightData(&omFlights);
		omSearchFlight.DeleteAll();
		pomResultDlg->DoCreate();
		return true;
	}

}


bool CSearch::SendCreateDemData(bool bpCreateFdep,CString opWhere,bool bpCreateFid,CString opFidStart,CString opFidEnd,bool bpIsSingleFlight,bool bpRuleDiagnosis)
{

	CString olTmp;
	CString olValidTpls = "|";
	for (int ilTpl = 0; ilTpl < ogBasicData.omValidTemplates.GetSize();ilTpl++)
	{
		if(ilTpl > 0)
		{
			olValidTpls += ("," + ogBasicData.omValidTemplates.GetAt(ilTpl));
		}
		else
		{
			olValidTpls += ogBasicData.omValidTemplates.GetAt(ilTpl);
		}
	}

	olTmp = opFidStart + "," + opFidEnd + "," + olValidTpls;


	bgCreateDemCount = 0;
	bgReleaseDemCount = 0;

	if(bpCreateFid)
	{
		bgCreateDemCount++;
		bgReleaseDemCount++;
	}
	if(bpCreateFdep)
	{
		bgCreateDemCount++;
		bgReleaseDemCount++;
	}
	if(bpCreateFid)
	{
		ogCovFlightData.CreateFidDemands(olTmp);
	}
	if(bpCreateFdep)
	{
		opWhere += "     [ROTATIONS]" + olValidTpls;
		/*
		if(bpIsSingleFlight)
		{
			opWhere += "|1";
		}
		*/
		ogCovFlightData.CreateDemands(opWhere,bpRuleDiagnosis);
	}
	return true;

}

void CSearch::AttachTsrArray(const CStringArray * popTsrArray)
{
	pomTsrArray = popTsrArray;
}

void CSearch::AttachFlightDemMap(const CMapPtrToPtr * popFlightDemMap)
{
	pomFlightDemMap = popFlightDemMap;
}

