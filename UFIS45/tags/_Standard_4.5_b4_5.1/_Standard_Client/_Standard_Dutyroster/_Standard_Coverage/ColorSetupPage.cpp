// ColorSetupPage.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <ColorSetupPage.h>
#include <BasicData.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ColorSetupPage property page

IMPLEMENT_DYNCREATE(ColorSetupPage, CPropertyPage)

ColorSetupPage::ColorSetupPage() : CPropertyPage(ColorSetupPage::IDD)
{
	//{{AFX_DATA_INIT(ColorSetupPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomColorList = new CGridFenster(this);
	ogBasicData.GetColorConfData(omColorConfData);

	// initialize array of lines changed
	for (int ilLc = 0; ilLc < NUMCONFCOLORS; ilLc++)
		omLinesChanged[ilLc] = 0;

	bmInit = false;
}

ColorSetupPage::~ColorSetupPage()
{
	delete pomColorList;
	omColorConfData.DeleteAll();

}

void ColorSetupPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ColorSetupPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate)
	{
		CWaitCursor wait;
		
		for (int ilLc = 0; ilLc < omColorConfData.GetSize(); ilLc++)
		{
			if (ilLc < NUMCONFCOLORS && omLinesChanged[ilLc])
			{
				// Save the data
				if (ogBasicData.SetColorConfData(ilLc,omColorConfData[ilLc]))
				{
					omLinesChanged[ilLc] = 0; // ok, line has been saved
				}
			}
		}

	}
	else if (bmInit)
	{
		pomColorList->LockUpdate(FALSE);

		CGXStyle olStyle;
		CString olText;
		CString olOrgText;
		int ilCount = omColorConfData.GetSize();

		CStringArray olItemList;
		pomColorList->SetRowCount(max(12,ilCount));

		// generate "-*- demand curve at first
		int ilLc = 0;
		COLORREF olColor = RGB(255,255,255);	// white
		pomColorList->SetStyleRange(CGXRange(ilLc+1, 1, ilLc+1, 1), olStyle.SetInterior( CGXBrush().SetColor(olColor)));

		olText.Format("%d",omColorConfData[FUNCTION_ALL].Index);
		pomColorList->SetStyleRange(CGXRange(ilLc+1, 1, ilLc+1, 1), olStyle.SetValue(olText));

		olText.Format("%d",omColorConfData[FUNCTION_ALL].Type);
		pomColorList->SetStyleRange(CGXRange(ilLc+1, 2, ilLc+1, 2), olStyle.SetValue(olText));

		olText = omColorConfData[FUNCTION_ALL].Name;
		pomColorList->SetStyleRange(CGXRange(ilLc+1, 3, ilLc+1, 3), olStyle.SetValue(olText));

		olText = "          ";
		olColor = omColorConfData[FUNCTION_ALL].Color;
		pomColorList->SetStyleRange(CGXRange(ilLc+1, 4, ilLc+1, 4), olStyle.SetInterior( CGXBrush().SetColor(olColor)));
		pomColorList->SetStyleRange(CGXRange(ilLc+1, 4, ilLc+1, 4), olStyle.SetValue(olText));

		for (int i = 0; i < ilCount; i++)
		{
			++ilLc;
			if (i != FUNCTION_ALL)
			{
				COLORREF olColor = RGB(255,255,255);	// white
				pomColorList->SetStyleRange(CGXRange(ilLc+1, 1, ilLc+1, 1), olStyle.SetInterior( CGXBrush().SetColor(olColor)));

				olText.Format("%d",omColorConfData[i].Index);
				pomColorList->SetStyleRange(CGXRange(ilLc+1, 1, ilLc+1, 1), olStyle.SetValue(olText));

				olText.Format("%d",omColorConfData[i].Type);
				pomColorList->SetStyleRange(CGXRange(ilLc+1, 2, ilLc+1, 2), olStyle.SetValue(olText));

				olText = omColorConfData[i].Name;
				pomColorList->SetStyleRange(CGXRange(ilLc+1, 3, ilLc+1, 3), olStyle.SetValue(olText));

				olText = "          ";
				olColor = omColorConfData[i].Color;
				pomColorList->SetStyleRange(CGXRange(ilLc+1, 4, ilLc+1, 4), olStyle.SetInterior( CGXBrush().SetColor(olColor)));
				pomColorList->SetStyleRange(CGXRange(ilLc+1, 4, ilLc+1, 4), olStyle.SetValue(olText));
			}
		}
			
		pomColorList->Redraw();

	}

}

void ColorSetupPage::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

BEGIN_MESSAGE_MAP(ColorSetupPage, CPropertyPage)
	//{{AFX_MSG_MAP(ColorSetupPage)
		// NOTE: the ClassWizard will add message map macros here
	ON_BN_CLICKED(IDC_BUTTON_LOAD, OnLoadFromDefault)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnSaveAsDefault)
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK, OnLButtonClickedRowCol)
	ON_MESSAGE(GRID_MESSAGE_DOUBLECLICK, OnLButtonDblClickedRowCol)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ColorSetupPage message handlers

BOOL ColorSetupPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	CWnd *polWnd = GetDlgItem(IDC_BUTTON_LOAD);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1836));
		ogBasicData.SetWindowStat("SETUP_DLG_LoadFromDefault",polWnd);
	}

	polWnd = GetDlgItem(IDC_BUTTON_SAVE);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1837));
		ogBasicData.SetWindowStat("SETUP_DLG_SaveAsDefault",polWnd);
	}

	pomColorList->SubclassDlgItem(IDC_COLOR_LIST, this);
	pomColorList->Initialize();

	CGXStyle olStyle;

	pomColorList->LockUpdate(TRUE);
	pomColorList->GetParam()->EnableUndo(FALSE);
	pomColorList->GetParam()->EnableTrackColWidth(FALSE);
	pomColorList->GetParam()->EnableTrackRowHeight(FALSE);
	pomColorList->GetParam()->EnableSelection(GX_SELROW);
	pomColorList->GetParam()->SetSpecialMode(GX_MODELBOX_SS);
	pomColorList->GetParam()->SetHideCurrentCell(GX_HIDE_ALWAYS); 
	pomColorList->GetParam()->SetNumberedColHeaders(FALSE);

	pomColorList->SetColCount(4);

	pomColorList->SetColWidth(4,4,30);
	pomColorList->SetColWidth(3,3,275);
	pomColorList->SetColWidth(2,2,30);
	pomColorList->SetColWidth(1,1,30);
	pomColorList->SetColWidth(0,0,30);


	pomColorList->SetRowHeight(0, 0, 12);
		
	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	pomColorList->HideCols(0,2); 

	bmInit = true;
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

BOOL  ColorSetupPage::OnLButtonDblClickedRowCol(UINT wParam, LONG lParam) 
{
	int ilRow = ((CELLPOS*)lParam)->Row;
	int ilCol = ((CELLPOS*)lParam)->Col;

	if (ilRow <= omColorConfData.GetSize())
	{
		int ilIndex = atoi(pomColorList->GetValueRowCol(ilRow, 1));
		
		CGXStyle olStyle;
		CColorDialog olDlg(omColorConfData[ilIndex].Color,0,this);
		if (olDlg.DoModal() == IDOK)
		{
   			COLORREF olColor = olDlg.GetColor();
			if (olColor != omColorConfData[ilIndex].Color)
			{
				pomColorList->LockUpdate(FALSE);
				pomColorList->GetParam()->SetLockReadOnly(FALSE);
				pomColorList->SetStyleRange(CGXRange(ilRow, 4, ilRow, 4), olStyle.SetInterior( CGXBrush().SetColor(olColor)));
//				pomColorList->SetStyleRange(CGXRange(ilRow, 4, ilRow, 4), olStyle.SetValue(" "));
				pomColorList->GetParam()->SetLockReadOnly(TRUE);
				pomColorList->Redraw();
				omColorConfData[ilIndex].Color = olColor;
				omLinesChanged[ilIndex] = 1;	// changed
			}
		}

		return TRUE;
	}
	else
		return FALSE;
}

BOOL  ColorSetupPage::OnLButtonClickedRowCol(UINT wParam, LONG lParam) 
{
	int ilRow = ((CELLPOS*)lParam)->Row;
	int ilCol = ((CELLPOS*)lParam)->Col;

	if (ilRow <= omColorConfData.GetSize())
	{
		int ilIndex = atoi(pomColorList->GetValueRowCol(ilRow, 1));
		
		CGXStyle olStyle;
		CColorDialog olDlg(omColorConfData[ilIndex].Color,0,this);
		if (olDlg.DoModal() == IDOK)
		{
   			COLORREF olColor = olDlg.GetColor();
			if (olColor != omColorConfData[ilIndex].Color)
			{
				pomColorList->LockUpdate(FALSE);
				pomColorList->GetParam()->SetLockReadOnly(FALSE);
				pomColorList->SetStyleRange(CGXRange(ilRow, 4, ilRow, 4), olStyle.SetInterior( CGXBrush().SetColor(olColor)));
//				pomColorList->SetStyleRange(CGXRange(ilRow, 4, ilRow, 4), olStyle.SetValue("Buddelberger"));
				pomColorList->GetParam()->SetLockReadOnly(TRUE);
				pomColorList->Redraw();
				omColorConfData[ilIndex].Color = olColor;
				omLinesChanged[ilIndex] = 1;	// changed
			}
		}

		return TRUE;
	}
	else
		return FALSE;
}

void ColorSetupPage::OnLoadFromDefault() 
{
	// TODO: Add your control notification handler code here
    CString olRecord;

	int ilRecCount = 0;

	CStringArray olLines;
	// read default values, if there are none, use hard coded values
	if (ogCfgData.ReadColors(ogCfgData.pomDefaultName,olLines) == false)
	{
		ogCfgData.GetDefaultColorSetup(olLines);
	}

	for(int i = 0; i < olLines.GetSize(); i++)
	{
		CString olRecord = olLines.GetAt(i);
		int ilMaxFields  = 4;
		int ilMaxBytes   = olRecord.GetLength();
		COLORCONFDATA *prlColorConfData = new COLORCONFDATA;

		for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < ilMaxFields; ilFieldCount++)
    	{
        // Extract next field in the specified text-line
			for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && olRecord[ilByteCount] != ';'; ilByteCount++)
				;
			if(ilByteCount <= ilMaxBytes)
			{
				CString olField = olRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
				ilByteCount++;
				switch(ilFieldCount)
				{
				case 0 : prlColorConfData->Index = atol((LPCSTR)olField);
					break;
				case 1 : prlColorConfData->Type  = atol((LPCSTR)olField);
					break;
				case 2 : prlColorConfData->Name  = olField;
					break;
				case 3 : prlColorConfData->Color = (COLORREF) atol((LPCSTR)olField);
					break;
				}
			}
		}

		// check, if an update is necessary
		if (i < omColorConfData.GetSize())
		{
			COLORCONFDATA *prlOldConfData = &omColorConfData[i];
			if (*prlOldConfData != *prlColorConfData)
			{
				delete prlOldConfData;
				omColorConfData.SetAt(i,prlColorConfData);
				ilRecCount++;

				omLinesChanged[i] = 1;	// changed
			}
		}
		else
		{
			omColorConfData.Add(prlColorConfData);
			ilRecCount++;

			omLinesChanged[i] = 1;	// changed
		}
	}

	UpdateData(FALSE);		
}

void ColorSetupPage::OnSaveAsDefault() 
{
	CWaitCursor wait;
	
    for (int ilLc = 0; ilLc < omColorConfData.GetSize(); ilLc++)
	{
		CFGDATA rlCfg;
		rlCfg.Urno = ogBasicData.GetNextUrno();
		sprintf(rlCfg.Ctyp, "COLOR%02d", ilLc);
		sprintf(rlCfg.Pkno, ogCfgData.pomDefaultName);
		sprintf(rlCfg.Text,"%d;%ld;%s;%ld",
		ilLc,
		omColorConfData[ilLc].Type,
		omColorConfData[ilLc].Name,
		omColorConfData[ilLc].Color
		);
		//Save the data
		ogCfgData.SaveColorData(ogCfgData.pomDefaultName,&rlCfg);

	}
}

void ColorSetupPage::OnOK() 
{
	// TODO: Add your specialized code here and/or call the base class
	if (UpdateData(TRUE))
	{
		CWaitCursor olWait;
		ogDdx.DataChanged(this,UPDATE_COLOR_SETUP,NULL);

	}
	CPropertyPage::OnOK();
}

void ColorSetupPage::OnCancel() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CPropertyPage::OnCancel();
}
