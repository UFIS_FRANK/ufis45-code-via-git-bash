// Class for Validity Times
#ifndef _CEDAVALDATA_H_
#define _CEDAVALDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct ValDataStruct
{
	long	Urno;		// Unique Record Number
	char	Tabn[4];	// valid record table name
	char	Freq[8];	// days of week
	long	Uval;		// valid record URNO
	CTime	Vafr;		// valid from...
	CTime	Vato;		// valid to ...
	char	Tifr[5];	// from o'clock
	char	Tito[5];	// to o'clock	
	CTime   Cdat;
	char    Usec[32+2];
	char    Useu[32+2];
	char    Appl[8+2];

	//DataCreated by this class
	int      IsChanged;
	
	ValDataStruct(void)
	{
		Urno = 0L;
		strcpy(Tabn,"");
		strcpy(Freq,"");
		Uval = 0L;
		Vafr = TIMENULL;
		Vato = TIMENULL;
		strcpy(Tifr,"");
		strcpy(Tito,"");
	}
};

typedef struct ValDataStruct VALDATA;

struct	ValidTimes
{
	CTime	Tifr;
	CTime	Tito;

	ValidTimes()
	{
		Tifr = TIMENULL;
		Tito = TIMENULL;
	}

	ValidTimes(const CTime& ropTifr,const CTime& ropTito)
	{
		Tifr = ropTifr;
		Tito = ropTito;
	}
};

typedef struct ValidTimes	VALIDTIMES;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaValData: public CCSCedaData
{

// Attributes
public:
	CString GetTableName(void);

// Operations
public:
	CedaValData();
	~CedaValData();

	BOOL		ReadValData(const CTime& ropFrom,const CTime& ropTo);
	VALDATA*	GetValByUrno(long lpUrno);
	int			GetValid(const CString& ropTable,long lpUrno,CCSPtrArray<VALDATA>& ropValid);
	int			GetCountOfRecords();
	int			GetValidTimes(VALDATA *prpValid,const CTime& ropStart,const CTime& ropEnd,CCSPtrArray<VALIDTIMES>& ropValid);
	int			GetValidTimes(const CString& ropTable,long lpUrno,const CTime& ropStart,const CTime& ropEnd,CCSPtrArray<VALIDTIMES>& ropValid);
	BOOL		IsValidAt(VALDATA *prpValid,const CTime& ropStart,const CTime& ropEnd);
	bool		IsCompletelyValid(const CString& ropTable, long lpUrno, CTime opFrom, CTime opTo);
	void		ProcessValBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool        IsValid(long lpUval,const CString& opTimestamp);
	CTime       COleDateTime2CTime(COleDateTime time);
	COleDateTime CTime2COleDateTime(CTime time);
	bool        Insert(VALDATA *prpVal);
	bool        Save(VALDATA *prpVal);
	int			GetYear(CString y){return atoi(y.Left(4));}
    int			GetMonth(CString y){return atoi(y.Mid(4,2));}
	int			GetDay(CString y){return atoi(y.Mid(6,2));}
	int			GetHour(CString y){return atoi(y.Mid(8,2));}
	int			GetMinute(CString y){return atoi(y.Mid(10,2));}
	int			GetSecond(CString y){return atoi(y.Mid(12,2));}
	bool		IsDayOfWeekSet(CString y, int dow){ return (y.Mid(dow,1) == "1");}	// 1=MON, 7=SUN

private:
	VALDATA*	AddValInternal(VALDATA &rrpVal);
	void		DeleteValInternal(VALDATA *prpVal);
	void		ConvertDatesToLocal(VALDATA &rrpVal);
	void		ClearAll();

private:
    CCSPtrArray <VALDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapStringToPtr			omTableMap;
};


extern CedaValData ogValData;
#endif _CEDAVALDATA_H_
