// stviewer.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
//#include "cviewer.h"
#include <CCSBar.h>
#include <ccsddx.h>
#include <DispoDetailGanttViewer.h>
//#include "PremisPage.h"
#include <CedaPfcData.h>
#include <CedaPerData.h>
#include <CedaBasicData.h>
#include <JobDlg.h>
//#include "StaffDiagram.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Adjusted number of overlapped level in the given line
#define VisualMaxOverlapLevel(lineno)	(max(1, GetMaxOverlapLevel(lineno)))

// Local function prototype
/*
static void DemDetailCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);*/



/////////////////////////////////////////////////////////////////////////////
// DispoDetailGanttViewer
//
DispoDetailGanttViewer::DispoDetailGanttViewer(long lpFkey)
{

	//omFkey = opFKey;
	lmFkey = lpFkey;
	pomAttachWnd = NULL;
	omBkBrush.CreateSolidBrush(RGB(235,235,235));
	omBarBrushGray.CreateSolidBrush(RGB(190,190,190));
	//omSortOrder.Add("Arbeitsbeginn");

	omGroundStart = TIMENULL;
	omGroundStart = TIMENULL;

	pomDemMap = NULL;
	prmFlightA = NULL;
	prmFlightD = NULL;
	pomStafDiagram = NULL;
	/*
	ogDdx.Register(this, INFO_DEM_NEW,
		CString("DDG_Viewer"), CString("DEM New"), DemDetailCf);
	ogDdx.Register(this, INFO_DEM_CHANGE,
		CString("DDG_Viewer"), CString("DEM Changed"), DemDetailCf);
	ogDdx.Register(this, INFO_DEM_DELETE,
		CString("DDG_Viewer"), CString("DEM Deleted"), DemDetailCf);
	*/
}

DispoDetailGanttViewer::~DispoDetailGanttViewer()
{
   ogDdx.UnRegister(this, NOTUSED);
	ClearAll();
}


long DispoDetailGanttViewer::GetCurrentFkey()
{
	return lmFkey;
}
void DispoDetailGanttViewer::SetCurrentFkey(long lpFkey)
{
	lmFkey = lpFkey;
}

void DispoDetailGanttViewer::SetCurrentFlights(FLIGHTDATA *prpFlightA, FLIGHTDATA *prpFlightD)
{
	prmFlightA = prpFlightA;
	prmFlightD = prpFlightD;
	if(prmFlightA != NULL)
	{
		CTime olStod = prmFlightA->Tifa;
		omGroundStart = olStod;
	}
	if(prmFlightD != NULL)
	{
		CTime olStod = prmFlightD->Tifd;
		omGroundEnd = olStod;
	}
}

void DispoDetailGanttViewer::ClearAll()
{
	int ilCount = 0, ilCount2 = 0;
	//omSortOrder.RemoveAll( );
	ilCount = omLines.GetSize();
	DEM_LINEDATA rlB;
	ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omLines[i].Bars.DeleteAll();
		omLines[i].BkBars.DeleteAll();
	}
	omLines.DeleteAll();
}
void DispoDetailGanttViewer::SetDemPointer(CMapPtrToPtr *popDemMap)
{
	pomDemMap = popDemMap;
}

void DispoDetailGanttViewer::Attach(CJobDlg *popAttachWnd)
{
    pomAttachWnd = popAttachWnd;
}

void DispoDetailGanttViewer::ChangeViewTo(long lpFkey, CTime opStartTime, CTime opEndTime)
{
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	ClearAll();
	MakeLines();
	MakeBars();
}

DEMDATA *DispoDetailGanttViewer::GetDemByUrno(long lpUrno)
{
	DEMDATA *prlDem = NULL;
	if(pomDemMap->Lookup((void *)lpUrno,(void *&)prlDem) == TRUE)
	{
		return prlDem;
	}
	return NULL;
}

BOOL DispoDetailGanttViewer::IsPassFilter(DEMDATA *prpDem)
{
	BOOL blRet = TRUE;

	if(prpDem->IsChanged == DATA_DELETED)
	{
		blRet = FALSE;
	}
	return blRet;
}

#if 0
int DispoDetailGanttViewer::CompareLine(DEM_LINEDATA *prpLine1, DEM_LINEDATA *prpLine2)
{
	// Compare in the sort order, from the outermost to the innermost
	int ilCompareResult = 0;

	//if(prpLine2->Urno == -1 || prpLine1->Urno == -1)
	//	return 1;
	if(prpLine2->RTyp < prpLine1->RTyp )
	{
		return -1;
	}
	if(prpLine2->RTyp > prpLine1->RTyp )
	{
		return 1;
	}
	if(prpLine2->Lknm == CString("") && prpLine1->Lknm == CString(""))
	{
		return -1;
	}
	else if(prpLine2->Lknm == CString("") && prpLine1->Lknm != CString(""))
	{
		return 1;
	}
	if(prpLine2->Lknm != CString("") && prpLine1->Lknm == CString(""))
	{
		return -1;
	}

	return (prpLine1->Dube == prpLine2->Dube)? 
							  ((prpLine1->Lknm == prpLine2->Lknm)? 0: 
							   (prpLine1->Lknm < prpLine2->Lknm)? 1: -1):
	(prpLine1->Dube > prpLine2->Dube)? 1: -1;

//	if(prpLine1->Dube == prpLine2->Dube)
//	{
//		if(prpLine2->Lknm == CString("") && prpLine1->Lknm == CString(""))
//		{
//			return -1;
//		}
//		else if(prpLine2->Lknm == CString("") && prpLine1->Lknm != CString(""))
//		{
//			return 1;
//		}
//		if(prpLine2->Lknm != CString("") && prpLine1->Lknm == CString(""))
//		{
//			return -1;
//		}
//		else
//		{
//			ilCompareResult = ((prpLine1->Lknm == prpLine2->Lknm)? 0:
//				(prpLine1->Lknm < prpLine2->Lknm)? 1: -1);
//		}	
//
//		return 0;
//	}
//	else
/*	{
		if(prpLine1->Dube < prpLine2->Dube)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
*/
	if (ilCompareResult != 0)
		return ilCompareResult;
	return 0;	// we can say that these two lines are equal
}
#endif



void DispoDetailGanttViewer::MakeLines()
{

	int ilAnz = 0;
	//ilCount = pomCedaDemData->omData.GetSize();
	
	POSITION rlPos;
	long llUrno;
	DEMDATA *prlDem = NULL;
	for ( rlPos = pomDemMap->GetStartPosition(); rlPos != NULL; )
	{	
		pomDemMap->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		if(prlDem != NULL)
		{
			MakeLine(prlDem);
			ilAnz++;		
		}
	}

	for(int i = omLines.GetSize(); i < 11; i++)
	{
		MakeEmptyLine();
	}

}

void DispoDetailGanttViewer::MakeBars()
{
	
	
	POSITION rlPos;
	long llUrno;
	DEMDATA *prlDem = NULL;
	for ( rlPos = pomDemMap->GetStartPosition(); rlPos != NULL; )
	{	
		pomDemMap->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		if(prlDem != NULL)
		{
			int ilLineno;
			if (FindDutyBar(prlDem->Urno, ilLineno))	// corresponding duty bar found?
			{
				MakeBar(ilLineno, prlDem);
			}	
		}
	}

}

void DispoDetailGanttViewer::MakeEmptyLine()
{
	DEM_LINEDATA rlLine;
	rlLine.Urno = -1;
	rlLine.Fkey = -1;
	rlLine.Disp = -1;
	rlLine.Dety = -1;
	rlLine.Urno = -1;
	rlLine.RTyp = 1000;
	rlLine.Lknm = CString("");
	rlLine.Tnam = CString("");
	CreateLine( &rlLine);
}

//void DispoDetailGanttViewer::MakeLineOrManager(DEMDATA *prpDem) -- pepper for Jobs
void DispoDetailGanttViewer::MakeLine(DEMDATA *prpDem)
{
	// Create a new line in found 

	if(IsPassFilter(prpDem))
	{
		
		DEM_BKBARDATA rlBkBar;
		rlBkBar.Urno = prpDem->Urno;
		rlBkBar.Text = "";
		
		if(omGroundStart != TIMENULL)
		{
			rlBkBar.StartTime = omGroundStart;
		}
		else
		{
			rlBkBar.StartTime = omStartTime;
		}
		if(omGroundEnd != TIMENULL)
		{
			rlBkBar.EndTime = omGroundEnd;
		}
		else
		{
			rlBkBar.EndTime = omEndTime;
		}
		CString olBS = rlBkBar.StartTime.Format("%d.%m.%Y - %H:%M");
		CString olBE = rlBkBar.EndTime.Format("%d.%m.%Y - %H:%M");

		rlBkBar.MarkerBrush = &omBkBrush;

		DEM_LINEDATA rlLine;
		rlLine.Urno = prpDem->Urno;
		if(prpDem->Ouro > 0L)
			rlLine.Fkey = prpDem->Ouro;
		else
			rlLine.Fkey = prpDem->Ouri;

	
		rlLine.BkBars.NewAt(0, rlBkBar);

		rlLine.Lknm = ogDemData.GetServiceName(prpDem->Urud);
		int ilIdx = 0;
		if(prpDem->HasPfc)
			ilIdx = 1;
		if(prpDem->HasPer)
			ilIdx = 2;
		if(prpDem->HasCic)
			ilIdx = 3;
		if(prpDem->HasPos)
			ilIdx = 4;
		if(prpDem->HasGat)
			ilIdx = 5;
		rlLine.RTyp = ilIdx;

		rlLine.Tnam = ogDemData.GetTemplateName ( prpDem->Urud );
		rlLine.Disp = ogDemData.GetDispNr ( prpDem->Urud );
		rlLine.Dety = atoi ( prpDem->Dety );

		CreateLine( &rlLine);
	}
}

void DispoDetailGanttViewer::MakeBar(int ipLineno, DEMDATA *prpDem)
{
	int ilCount = 0;
	DEM_BARDATA rlBar;

	rlBar.Urno = prpDem->Urno;
	if( prpDem->Ouro > 0L)
		rlBar.Fkey = prpDem->Ouro;
	else
		rlBar.Fkey = prpDem->Ouri;

	rlBar.Dube = prpDem->DispDebe;
	rlBar.Duen = prpDem->DispDeen;
	CString olB = rlBar.Dube.Format("%H:%M");
	CString olE = rlBar.Duen.Format("%H:%M");
	//TO DO fill text for the Bar Text
	rlBar.Text = BarTextAndValues(prpDem);
	rlBar.Text2 = " - " + olB + " -> " + olE + " / " + CString(prpDem->Rusn);

	rlBar.FrameType = FRAMERECT;
	rlBar.MarkerType = MARKFULL;
	

	switch(prpDem->Dety[0])
	{
	case '0' :	// Turnaround
		rlBar.MarkerBrush = ogBrushs[YELLOW_IDX];
		rlBar.TextColor = ogColors[BLACK_IDX];
	break;
	case '1' :	// Inbound
		rlBar.MarkerBrush = ogBrushs[PGREEN_IDX];
		rlBar.TextColor = ogColors[BLACK_IDX];
	break;
	case '2' :	// Outbound
		rlBar.MarkerBrush = ogBrushs[PBLUE_IDX];
		rlBar.TextColor = ogColors[BLACK_IDX];
	break;
	default:
		rlBar.MarkerBrush = ogBrushs[GREEN_IDX];
		rlBar.TextColor = ogColors[BLACK_IDX];
	}
	
	rlBar.FrameType = FRAMERECT;
	rlBar.MarkerType = MARKFULL;
	int ilBarno = CreateBar(ipLineno, &rlBar);
}


CString DispoDetailGanttViewer::LineText(DEMDATA *prpDem)
{
	char buf[512];
	return buf;
}

CString DispoDetailGanttViewer::BarTextAndValues(DEMDATA *prpDem)
{
	CString olText("");
	if(prpDem != NULL)
	{
		if(prpDem->HasPfc)
		{
			CString olPfc = ogDemData.GetPfcSingeUrnos(prpDem->Urud);
			CStringArray olPfcArray;
			ExtractItemList(olPfc,&olPfcArray,',');
			for(int ilCount = 0;ilCount < olPfcArray.GetSize(); ilCount++)
			{
				long llUrno = atol(olPfcArray[ilCount]);
				CString olTmpFkt = ogPfcData.GetFctcByUrno(llUrno) ;
				if(olTmpFkt.IsEmpty())
				{
					olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olPfcArray[ilCount], "PFC", "GRPN");
				}
				if(!olTmpFkt.IsEmpty())
				{
					olTmpFkt += "  ";
					olText += olTmpFkt;
				}
			}
			olPfcArray.RemoveAll();
		}
		if(prpDem->HasPer)
		{
			CString olPfc = ogDemData.GetPerSingeUrnos(prpDem->Urud);
			CStringArray olPfcArray;
			ExtractItemList(olPfc,&olPfcArray,',');
			for(int ilCount = 0;ilCount < olPfcArray.GetSize(); ilCount++)
			{
				long llUrno = atol(olPfcArray[ilCount]);
				CString olTmpFkt = ogPerData.GetPrmcByUrno(llUrno) ;
				if(olTmpFkt.IsEmpty())
				{
					olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olPfcArray[ilCount], "PER", "GRPN");
				}
				if(!olTmpFkt.IsEmpty())
				{
					// replace blanks fru '/'
					if (olText.GetLength() > 3)
					{
						olText = olText.Left(olText.GetLength() - strlen("  "));
						olText+= '/'; 
					}

					olTmpFkt += "  ";
					olText += olTmpFkt;
				}
			}
			olPfcArray.RemoveAll();
		}
		if(prpDem->HasCic)
		{
			CString olPfc = ogDemData.GetCicSingeUrnos(prpDem->Urud);
			CStringArray olPfcArray;
			ExtractItemList(olPfc,&olPfcArray,',');
			for(int ilCount = 0;ilCount < olPfcArray.GetSize(); ilCount++)
			{
				CString olTmpFkt = ogBCD.GetField("CIC", "URNO", olPfcArray[ilCount], "CNAM");
				if(olTmpFkt.IsEmpty())
				{
					olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olPfcArray[ilCount], "CIC", "GRPN");
				}	
				if(!olTmpFkt.IsEmpty())
				{
					olTmpFkt += "  ";
					olText += olTmpFkt;
				}
			}
			olPfcArray.RemoveAll();
		}		
		if(prpDem->HasPos)
		{
			CString olPfc = ogDemData.GetCicSingeUrnos(prpDem->Urud);
			CStringArray olPfcArray;
			ExtractItemList(olPfc,&olPfcArray,',');
			for(int ilCount = 0;ilCount < olPfcArray.GetSize(); ilCount++)
			{
				CString olTmpFkt = ogBCD.GetField("PST", "URNO", olPfcArray[ilCount], "PNAM");
				if(olTmpFkt.IsEmpty())
				{
					olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olPfcArray[ilCount], "PST", "GRPN");
				}	
				if(!olTmpFkt.IsEmpty())
				{
					olTmpFkt += "  ";
					olText += olTmpFkt;
				}
			}
			olPfcArray.RemoveAll();
		}
		if(prpDem->HasGat)
		{
			CString olPfc = ogDemData.GetCicSingeUrnos(prpDem->Urud);
			CStringArray olPfcArray;
			ExtractItemList(olPfc,&olPfcArray,',');
			for(int ilCount = 0;ilCount < olPfcArray.GetSize(); ilCount++)
			{
				CString olTmpFkt = ogBCD.GetField("GAT", "URNO", olPfcArray[ilCount], "GNAM");
				if(olTmpFkt.IsEmpty())
				{
					olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olPfcArray[ilCount], "GAT", "GRPN");
				}	
				if(!olTmpFkt.IsEmpty())
				{
					olTmpFkt += "  ";
					olText += olTmpFkt;
				}
			}
			olPfcArray.RemoveAll();
		}
		if(prpDem->HasReq)
		{
			CString olPfc = ogDemData.GetReqSingeUrnos(prpDem->Urud);
			CString olUrud;
			olUrud.Format("%d",prpDem->Urud);
			CString olEqco = ogBCD.GetField("REQ","URUD",olUrud,"EQCO");

			CStringArray olPfcArray;
			ExtractItemList(olPfc,&olPfcArray,',');
			for(int ilCount = 0;ilCount < olPfcArray.GetSize(); ilCount++)
			{
				CString olTmpFkt;
				if (!olEqco.IsEmpty())
				{
					olTmpFkt = ogBCD.GetField("EQU", "URNO", olPfcArray[ilCount], "GCDE");
				}
				else
				{
					olTmpFkt = ogBCD.GetFieldExt("SGR", "URNO", "TABN", olPfcArray[ilCount], "EQU", "GRPN");
				}	

				if(!olTmpFkt.IsEmpty())
				{
					olTmpFkt += "  ";
					olText += olTmpFkt;
				}
			}
			olPfcArray.RemoveAll();
		}
	}

	return olText;

}



BOOL DispoDetailGanttViewer::FindDutyBar(/*CTime opDube,*/ long lpUrno, int &ripLineno)
{
		for (ripLineno = 0; ripLineno < GetLineCount(); ripLineno++)
			if (GetLine(ripLineno)->Urno == lpUrno)
				return TRUE;

	return FALSE;
}

BOOL DispoDetailGanttViewer::FindBarGlobal(long lpUrno, int &ripLineno, int &ripBarno)
{
	int ilCount = omLines.GetSize();
//		for(ripLineno = 0; ripLineno < ilCount; ripLineno++)
		if(FindDutyBar(lpUrno, ripLineno))
		{
			for (ripBarno = 0; ripBarno < GetBarCount(ripLineno); ripBarno++)
			{
				if (GetBar( ripLineno, ripBarno)->Urno == lpUrno)
				{
					return TRUE;
				}
			}
		}
/*	int ilCount = omLines.GetSize();
		for(ripLineno = 0; ripLineno < ilCount; ripLineno++)
			for (ripBarno = 0; ripBarno < GetBarCount(ripLineno); ripBarno++)
				if (GetBar( ripLineno, ripBarno)->Urno == lpUrno)
					return TRUE;
*/

	return FALSE;
}

BOOL DispoDetailGanttViewer::FindBar(/*CTime opDube,*/long lpUrno, int &ripLineno, int &ripBarno)
{
			for (ripBarno = 0; ripBarno < GetBarCount(ripLineno); ripBarno++)
				if (GetBar( ripLineno, ripBarno)->Urno == lpUrno)
					return TRUE;

	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// DispoDetailGanttViewer -- Viewer basic operations
//

int DispoDetailGanttViewer::GetNextFreeLine()
{
	for(int i = 0; i < omLines.GetSize(); i++)
		if(omLines[i].Fkey == -1)
			return i;

	return -1;
}

int DispoDetailGanttViewer::GetRealLineCount()
{
	int ilBarCount=0;
	for(int i = 0; i < omLines.GetSize(); i++)
	{
		if(GetBarCount(i) > 0)
		{
			ilBarCount++;
		}
	}
    return ilBarCount;
}

int DispoDetailGanttViewer::GetLineCount()
{
	return omLines.GetSize(); 
}

DEM_LINEDATA *DispoDetailGanttViewer::GetLine(int ipLineno)
{
    return &omLines[ipLineno];
}

CString DispoDetailGanttViewer::GetLineText(int ipLineno, COLORREF *prpTextColor, COLORREF *prpBkColor)
{
	//*prpTextColor = BLACK;//ogColors[IDX_BLACK];
	//*prpBkColor = SILVER;//ogColors[IDX_SILVER];
	*prpTextColor = YELLOW;//ogColors[IDX_YELLOW];
	*prpBkColor = GRAY;//ogColors[IDX_GRAY];

	if((ipLineno >= 0) && (ipLineno <= omLines.GetSize()-1))
	{
		return omLines[ipLineno].Lknm;
	}
    return CString(""); 
}

int DispoDetailGanttViewer::GetVisualMaxOverlapLevel(int ipLineno)
{
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipLineno);//MWO  / 3 * 3 + 2;
}

int DispoDetailGanttViewer::GetMaxOverlapLevel( int ipLineno)
{
    return omLines[ipLineno].MaxOverlapLevel;
}

DEM_BKBARDATA *DispoDetailGanttViewer::GetBkBar(int ipLineno, int ipBkBarno)
{
    return &omLines[ipLineno].BkBars[ipBkBarno];
}

CString DispoDetailGanttViewer::GetBkBarText(int ipLineno, int ipBkBarno)
{
    return omLines[ipLineno].BkBars[ipBkBarno].Text;
}

int DispoDetailGanttViewer::GetBkBarCount(int ipLineno)
{
    return omLines[ipLineno].BkBars.GetSize();
}

int DispoDetailGanttViewer::GetBarCount(int ipLineno)
{
	 if(ipLineno < omLines.GetSize())
		return omLines[ipLineno].Bars.GetSize();
	 else
		 return 0;
}

DEM_BARDATA *DispoDetailGanttViewer::GetBar(int ipLineno, int ipBarno)
{
    return &omLines[ipLineno].Bars[ipBarno];
}

CString DispoDetailGanttViewer::GetBarText( int ipLineno, int ipBarno)
{
    return (omLines[ipLineno].Bars[ipBarno].Text + omLines[ipLineno].Bars[ipBarno].Text2);
}



/////////////////////////////////////////////////////////////////////////////
// DispoDetailGanttViewer - DEM_BARDATA array maintenance (overlapping version)
//



int DispoDetailGanttViewer::CreateLine( DEM_LINEDATA *prpLine)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareLine(prpLine, GetLine(ilLineno)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, *prpLine);
    DEM_LINEDATA *prlLine = &omLines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLineno;
}

void DispoDetailGanttViewer::DeleteLine( int ipLineno)
{
    while (GetBkBarCount(ipLineno) > 0)
        DeleteBkBar(ipLineno, 0);

    while (GetBarCount(ipLineno) > 0)
        DeleteBar(ipLineno, 0);

    omLines.DeleteAt(ipLineno);
}

void DispoDetailGanttViewer::DeleteBkBar(int ipLineno, int ipBkBarno)
{
    omLines[ipLineno].BkBars.DeleteAt(ipBkBarno);
}


// no overlapping version, data will be sorted by the field StartTime



void DispoDetailGanttViewer::GetOverlappedBarsFromTime( int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    DEM_LINEDATA *prlLine = GetLine(ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].Dube;
			CTime olEndTime = prlLine->Bars[ilBarno].Duen;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].Dube);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].Duen);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}



int DispoDetailGanttViewer::CreateBar(int ipLineno, DEM_BARDATA *prpBar, BOOL bpTopBar)
{
    DEM_LINEDATA *prlLine = GetLine( ipLineno);
    int ilBarCount = omLines[ipLineno].Bars.GetSize();

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipLineno,
		prpBar->Dube, prpBar->Duen, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			DEM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			DEM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void DispoDetailGanttViewer::DeleteBar(int ipLineno, int ipBarno)
{

    DEM_LINEDATA *prlLine = GetLine( ipLineno);
    DEM_BARDATA *prlBar = &prlLine->Bars[ipBarno];	// given bar is in painting order
	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipLineno,
		prlBar->Dube, prlBar->Duen, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<DEM_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		DEM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			DEM_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}



/////////////////////////////////////////////////////////////////////////////
// DispoDetailGanttViewer - DEM_BARDATA calculation routines

// Return the bar number of the list box based on the given period [time1, time2]
int DispoDetailGanttViewer::GetBarnoFromTime( int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipLineno)-1; i >= 0; i--)
    {
        DEM_BARDATA *prlBar = GetBar( ipLineno, i);
        if (opTime1 <= prlBar->Duen && prlBar->Dube<= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}



/////////////////////////////////////////////////////////////////////////////
// DispoDetailGanttViewer private methods

// Returns the first bar no (in the time order) of the given overlap grop.
// What's special to this method is we could get the same overlapping bar either
// give "ipBarno" in paint order or "ipBarno" in time order.
//
int DispoDetailGanttViewer::GetFirstOverlapBarno(DEM_LINEDATA *prpLine, int ipBarno)
{
    if (IsBetween(ipBarno, 0, prpLine->Bars.GetSize() - 1)) // valid element in array?
        return ipBarno - GetBarInTimeOrder(prpLine,ipBarno)->OverlapLevel;
    return -1;
}

// Return an pointer to the data of the specified bar (given bar is in time order)
DEM_BARDATA *DispoDetailGanttViewer::GetBarInTimeOrder(DEM_LINEDATA *prpLine, int ipBarno)
{
    //return &prpLine->Bars[ipBarno + prpLine->TimeOrder[ipBarno]];
  // MCU 07.07 I've splitted the line for testing purpose (to see the i value)
	int ilBarno = max(ipBarno, 0);
	int ilIndex ;
	if(ilBarno < prpLine->Bars.GetSize())
	{
		 ilIndex = min(ipBarno + prpLine->TimeOrder[ilBarno],max(prpLine->Bars.GetSize()-1,0));
	}
	else
	{
		ilIndex = prpLine->Bars.GetSize()-1;
	}
    return &prpLine->Bars[max(ilIndex,0)];
}

// Return the right most time of the bars which is displayed before this bar (given bar is in time order)
CTime DispoDetailGanttViewer::GetEndTimeOfBarsOnTheLeft(DEM_LINEDATA *prpLine, int ipBarno)
{
    CTime olEndTimeOfBarsOnTheLeft = TIMENULL;  // provide default value in case of error

    // Get the maximum ending time of the bars on the left of the given bar
    int ilFirstBar = GetFirstOverlapBarno(prpLine, ipBarno-1);
    if (ilFirstBar != -1)
        for (int i = ilFirstBar; GetFirstOverlapBarno(prpLine, i) == ilFirstBar; i++)
        {
            CTime olEndTimeOfThisBar = prpLine->Bars[i].Duen;
            if (olEndTimeOfThisBar > olEndTimeOfBarsOnTheLeft)
                olEndTimeOfBarsOnTheLeft = olEndTimeOfThisBar;
        }

    return olEndTimeOfBarsOnTheLeft;
}

// This routine will delete "ilTotalBars" bars start from the bar number
// "ilFirstBar" in the prlLine->Bars[]. Then all of these bars will be inserted into
// the array again, expected to be in the correct order.
void DispoDetailGanttViewer::ReorderBars(int ipLineno, int ilFirstBar, int ilTotalBars)
{
    CCSPtrArray<DEM_BARDATA> Bars;
    DEM_LINEDATA *prlLine = GetLine(ipLineno);
    int ilBarno;

    // copy bars data out of the array and save them into the local "Bars" array
    for (ilBarno = 0; ilBarno < ilTotalBars; ilBarno++)
    {
        Bars.Add(&prlLine->Bars[ilFirstBar]);
        prlLine->Bars.RemoveAt(ilFirstBar);
        prlLine->TimeOrder.DeleteAt(ilFirstBar);
    }

    // insert bar to those machine one by one, then clean up the memory allocated for bars
    for (ilBarno = 0; ilBarno < ilTotalBars; ilBarno++)
    {
        DEM_BARDATA *prlBar = &Bars[ilBarno];
        CreateBar(ipLineno, prlBar);
        delete prlBar;
    }
} 


/////////////////////////////////////////////////////////////////////////////
// DispoDetailGanttViewer Jobs creation methods
/*
static void DemDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    DispoDetailGanttViewer *polViewer = (DispoDetailGanttViewer *)popInstance;

	if (ipDDXType == INFO_DEM_NEW)
		polViewer->ProcessDemNew((DEMDATA *)vpDataPointer);
	if (ipDDXType == INFO_DEM_CHANGE)
		polViewer->ProcessDemChange((DEMDATA *)vpDataPointer);
//	if (ipDDXType == DEM_STATUS_CHANGE)
//		polViewer->ProcessDemChange((DEMDATA *)vpDataPointer);
	if (ipDDXType == INFO_DEM_DELETE)
		polViewer->ProcessDemDelete((DEMDATA *)vpDataPointer);
}
*/
//-----------------------------------------------------
// From DDX ==> there is a new Employee

void DispoDetailGanttViewer::ProcessDemNew(DEMDATA *prpDem)
{
	int ilLineno;  //GetLine(int ipLineno)
	//FindBarGlobal(prpDem->Urno, ilLineno, ilBarno);
	if(IsPassFilter(prpDem))
	{
		MakeLine(prpDem);
		if(FindDutyBar(prpDem->Urno, ilLineno) == TRUE)
		{
			MakeBar(ilLineno, prpDem);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = ilLineno;
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_DELETELINE, lParam);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
			}
		}
	}
}

void DispoDetailGanttViewer::ProcessDemChange(DEMDATA *prpDem)
{
	int ilLineno;
	if (FindDutyBar(prpDem->Urno,ilLineno))
	{
		// Update the Viewer's data
		int ilOldMaxOverlapLevel = VisualMaxOverlapLevel( ilLineno);

		// Notifies the attached window (if exist)
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			LONG lParam = ilLineno;
			DeleteLine(ilLineno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_DELETELINE, lParam);

			if (IsPassFilter(prpDem))
			{
				MakeLine(prpDem);
				if (FindDutyBar(prpDem->Urno, ilLineno) == TRUE)
				{
					MakeBar(ilLineno, prpDem);
					if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						LONG lParam = ilLineno;
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
					}
				}
			}
			else
			{
				MakeEmptyLine();
				int ilNewLine = omLines.GetSize()-1;
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = ilNewLine;
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
				}
			}

		}
		
	}
	else
		ProcessDemNew(prpDem);
}


void DispoDetailGanttViewer::ProcessPremisDelete(DEMDATA *prpDem)
{
	pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_RESETCONTENT, 0);
	ClearAll();
}

void DispoDetailGanttViewer::ProcessDemDelete(DEMDATA *prpDem)
{
	int ilLineno;

	if (FindDutyBar(prpDem->Urno, ilLineno))	// corresponding duty bar found
	{
		// Update the Viewer's data
		int ilOldMaxOverlapLevel = VisualMaxOverlapLevel( ilLineno);
		// Notifies the attached window (if exist)
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			LONG lParam = ilLineno;
			DeleteLine(ilLineno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_DELETELINE, lParam);
			MakeEmptyLine();
			int ilNewLine = omLines.GetSize()-1;
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = ilNewLine;
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
			}
		}
    }

}

int DispoDetailGanttViewer::CompareLine(DEM_LINEDATA *prpLine1, DEM_LINEDATA *prpLine2)
{
	if(prpLine2->Urno == -1 )
		return 1;
	if(prpLine1->Urno == -1 )
		return -1;
	if(prpLine2->Tnam < prpLine1->Tnam )
	{
		TRACE("CompareLine: <%ld> <%ld> Tnam <%s> <%s> result -1\n",
			  prpLine1->Urno, prpLine2->Urno, prpLine1->Tnam, prpLine2->Tnam );
		return -1;
	}
	else
		if (prpLine2->Tnam > prpLine1->Tnam )
		{
			TRACE("CompareLine: <%ld> <%ld> Tnam <%s> <%s> result 1\n",
				   prpLine1->Urno, prpLine2->Urno, prpLine1->Tnam, prpLine2->Tnam );
			return 1;
		}
	if(prpLine2->Dety < prpLine1->Dety )
	{
		TRACE("CompareLine: <%ld> <%ld> Dety <%d> <%d> result -1\n",
			  prpLine1->Urno, prpLine2->Urno, prpLine1->Dety, prpLine2->Dety );
		return -1;
	}
	else
		if(prpLine2->Dety > prpLine1->Dety )
		{
			TRACE("CompareLine: <%ld> <%ld> Dety <%d> <%d> result 1\n",
			      prpLine1->Urno, prpLine2->Urno, prpLine1->Dety, prpLine2->Dety );
			return 1;
		}
	if(prpLine2->Disp < prpLine1->Disp)
	{
		TRACE("CompareLine: <%ld> <%ld> Disp <%d> <%d> result -1\n",
			  prpLine1->Urno, prpLine2->Urno, prpLine1->Disp, prpLine2->Disp );
		return -1;
	}
	else
		if(prpLine2->Disp > prpLine1->Disp)
		{
			TRACE("CompareLine: <%ld> <%ld> Disp <%d> <%d> result 1\n",
				  prpLine1->Urno, prpLine2->Urno, prpLine1->Disp, prpLine2->Disp );
			return 1;
		}
	if ( prpLine2->Urno < prpLine1->Urno )
	{
		TRACE("CompareLine: URNO <%ld> <%ld> result -1\n",
				  prpLine1->Urno, prpLine2->Urno );
		return -1;
	}
	else
	{
		TRACE("CompareLine: URNO <%ld> <%ld> result -\n",
				  prpLine1->Urno, prpLine2->Urno );
		return 1;

	}
}
