#if !defined(AFX_SELECTDATEDLG_H__861CB808_5FF0_4A60_ACB2_7289A35AC941__INCLUDED_)
#define AFX_SELECTDATEDLG_H__861CB808_5FF0_4A60_ACB2_7289A35AC941__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectDateDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectDateDlg dialog

class CSelectDateDlg : public CDialog
{
// Construction
public:
	CSelectDateDlg(CWnd* pParent = NULL);   // standard constructor

	void		SetDate(const CTime& ropDate);
	void		SetRange(const CTime& ropFrom,const CTime ropTo);
	void		SetValidWeekDay(int ipWeekDay);
	CTime		GetDate() const;

// Dialog Data
	//{{AFX_DATA(CSelectDateDlg)
	enum { IDD = IDD_SELECT_DATE_DLG };
	CMonthCalCtrl	m_CalCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectDateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectDateDlg)
	afx_msg void OnGetdaystateMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelectMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureMonthcalendar1(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CTime	omDate;
	CTime	omFrom;
	CTime	omTo;
	int		imOleDateWeekDay;
	int		imCalCtrlWeekDay;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTDATEDLG_H__861CB808_5FF0_4A60_ACB2_7289A35AC941__INCLUDED_)
