#if !defined(AFX_ONFLICTDLG_H__81584801_9A3D_11D1_B471_0000B45A33F5__INCLUDED_)
#define AFX_ONFLICTDLG_H__81584801_9A3D_11D1_B471_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// onflictDlg.h : header file
//

#include <ConflictCheck.h>
#include <Table.h>
#include <CViewer.h>

struct CONFLICT_LINEDATA
{
	int  erroNo;
	long FlightUrno;
	long Fkey;	
	long GhdKey;
	long GhdUrno;
	long DsrUrno;
	CTime Time;
	int  Type;
	char Esnm[4]; 
	char Sfca[4];
	char Text[1024];
	CONFLICT_LINEDATA(void)
	{
		memset(this, '\0', sizeof(*this));
		Time = -1; Type = CFI_NOCONFLICT;
	}
};

/////////////////////////////////////////////////////////////////////////////
// ConflictDlg dialog

class ConflictDlg : public CDialog
{
// Construction
public:
	ConflictDlg(CWnd* pParent, 
				bool bpCxxAdd,
				CedaFlightData *popFlightData, ConflictCheck *popConflictData,CViewer *popCallerView = NULL);   // standard constructor
	~ConflictDlg();
 
	CCSPtrArray<CONFLICT_LINEDATA> omLines;

	ConflictCheck	*pomConflictData;
	CedaFlightData	*pomFlightData;
//	CedaGhdData		*pomGhdData;
	CWnd *pomParent;
	
	bool bmCxxAdd;
	int m_nDialogBarHeight;
	CTable *pomTable;
	CString Format(CONFLICT_LINEDATA *prpConfl);
	void CreateLine(CONFLICT_LINEDATA *prpConfl);
	void UpdateDisplay();
	void LoadList();
	void Clear();
// Dialog Data
	//{{AFX_DATA(ConflictDlg)
	enum { IDD = IDD_CONFLICT_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ConflictDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
   LONG OnTableLButtonDblClk(UINT wParam, LONG lParam);
   LONG OnTableRButtonDown(UINT wParam, LONG lParam);
   void OnMenuConflictDelete();
   LONG OnTableDragBegin(UINT wParam, LONG lParam);
	BOOL isCreated;
	// Generated message map functions
	//{{AFX_MSG(ConflictDlg)
	afx_msg void OnDeleteall();
	afx_msg void OnView();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CViewer *pomCallerView;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ONFLICTDLG_H__81584801_9A3D_11D1_B471_0000B45A33F5__INCLUDED_)
