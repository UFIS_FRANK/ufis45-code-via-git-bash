#ifndef _CSDTSD_H_
#define _CSDTSD_H_
 
//#include <afxwin.h>
#include <ccsglobl.h>
//#include "ccsobj.h"
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
/////////////////////////////////////////////////////////////////////////////



struct SDTDATA {
	char 	 Bewc[7]; 	// Bewertungsfaktor.code
	char 	 Bkr1[3]; 	// Pausenlage relativ zu Schichtbeginn oder absolut
	char 	 Bsdc[10]; 	// Code
	char 	 Bsdk[14]; 	// Kurzbezeichnung
	char 	 Bsdn[42]; 	// Bezeichnung
	char 	 Bsds[5]; 	// SAP-Code
	char 	 Ctrc[7]; 	// Vertragsart.code
	char 	 Days[9]; 	// Verkehrstage
	char 	 Dnam[34]; 	// Bedarfsname
	char 	 Type[3]; 	// Flag (statisch/dynamisch)
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	int 	 Sdu1/*[6]*/; 	// Regul�re Schichtdauer
	int 	 Sex1/*[6]*/; 	// M�gliche Arbeitszeitver-l�ngerung
	int 	 Ssh1/*[6]*/; 	// M�gliche Arbeitszeitverk�rzung
	int		 Bkd1/*[6]*/; 	// Pausenl�nge
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	long	 Bsdu;
	CTime	 Brkf;	//Pause von
	CTime	 Brkt;	//Pause bis
	CTime 	 Bkf1/*[6]*/; 	// Pausenlage von			====> char 	 Bkf1[6]
	CTime 	 Bkt1/*[6]*/; 	// Pausenlage bis			====> char 	 Bkt1
	CTime	 Sbgi;			//Schichtbeginn
	CTime	 Seni;			//Schichtende
	CTime 	 Cdat; 	// Erstellungsdatum
	CTime 	 Esbg; 	// Fr�hester Schichtbeginn		===> char 	 Esbg[6]
	CTime 	 Lsen; 	// Sp�testes Schichtende		===> char 	 Lsen[6]
	CTime 	 Lstu; 	// Datum letzte �nderung
	long	 Sdgu;
	char     Fctc[64]; // Extra bytes for 'multi-talented' employees, used in displaying roster plans - gsa

	//Fehlt noch
	//Additionals
	int  BarBkColorIdx;        // Index in global Color Table for Backgroundcolor of the Bar
	int  BarTextColorIdx;      // Index in global Color Table for Textcolor of the Bar
	int  TriangelColorLeft;	   // Dreiecksfarbe im Balken links
	int  TriangelColorRight;	   // Dreiecksfarbe im Balken rechts
	int  TblTextColorIdx;		// Index in global Color Table for TextColor in NoAllocTable
	int  IsChanged;	// Check whether Data has Changed f�r Relaese
	BOOL IsSelected;
	BOOL IsConflictAccepted;
	bool IsDetailMarked; 		
	bool IsVirtuel; 
	bool IsAutoSdt;
	int  Planungsstufe;
	char StatPlSt[2];
	long RosterStfu;
	long GspUrno;
	long SplUrno;
	long MsdUrno;
	int  Faktor;
	int  Drrn;
	char Sday[18];
    char Enames[125];
	CTime OrgBrkf;
	CTime OrgBrkt;


	SDTDATA(void)
	{ /*memset(&Key,'\0',sizeof(*this)-sizeof(Break));*/
		memset(this,'\0',sizeof(*this));

		IsSelected = FALSE;IsConflictAccepted=FALSE;
		Faktor = 1;
		Bkf1 = TIMENULL;
		Bkt1 = TIMENULL;
		Cdat = TIMENULL;
		Esbg = TIMENULL;
		Lsen = TIMENULL;
		Lstu = TIMENULL;
		Brkf = TIMENULL;
		Brkt = TIMENULL;
		OrgBrkf = TIMENULL;
		OrgBrkt = TIMENULL;
		Sbgi = TIMENULL;
		Seni = TIMENULL;

		IsAutoSdt = false;
		IsDetailMarked = false;
		BarBkColorIdx = SILVER_IDX;
		BarTextColorIdx = BLACK_IDX;
		TriangelColorLeft = -1;	   // Dreiecksfarbe im Balken links
		TriangelColorRight = -1;	   // Dreiecksfarbe im Balken rechts
	}

	friend bool operator==(const SDTDATA& rrp1,const SDTDATA& rrp2)
	{
#if	1
		if (rrp1.Bkd1 != 0)	// we do have a break length !
		{
			return	rrp1.Sdgu == rrp2.Sdgu			
				&&	rrp1.Bsdu == rrp2.Bsdu			

				&&	strcmp(rrp1.Bewc,rrp2.Bewc) == 0
				&&	strcmp(rrp1.Bkr1,rrp2.Bkr1) == 0
				&&	strcmp(rrp1.Bsdc,rrp2.Bsdc) == 0
				&&	strcmp(rrp1.Bsdk,rrp2.Bsdk) == 0
				&&	strcmp(rrp1.Bsdn,rrp2.Bsdn) == 0
				&&	strcmp(rrp1.Bsds,rrp2.Bsds) == 0
				&&	strcmp(rrp1.Ctrc,rrp2.Ctrc) == 0
				&&	strcmp(rrp1.Type,rrp2.Type) == 0
				&&	strcmp(rrp1.Fctc,rrp2.Fctc) == 0

				&&	rrp1.Sdu1 == rrp2.Sdu1
				&&	rrp1.Sex1 == rrp2.Sex1
				&&	rrp1.Ssh1 == rrp2.Ssh1
				&&	rrp1.Bkd1 == rrp2.Bkd1

				&&	rrp1.Esbg == rrp2.Esbg
				&&	rrp1.Lsen == rrp2.Lsen
				&&	rrp1.Bkf1 == rrp2.Bkf1
				&&	rrp1.Bkt1 == rrp2.Bkt1
				&&	rrp1.Brkf == rrp2.Brkf
				&&	rrp1.Brkt == rrp2.Brkt

				&&	rrp1.Sbgi == rrp2.Sbgi
				&&	rrp1.Seni == rrp2.Seni
				;
		}
		else
		{
			return	rrp1.Sdgu == rrp2.Sdgu			
				&&	rrp1.Bsdu == rrp2.Bsdu			

				&&	strcmp(rrp1.Bewc,rrp2.Bewc) == 0
				&&	strcmp(rrp1.Bkr1,rrp2.Bkr1) == 0
				&&	strcmp(rrp1.Bsdc,rrp2.Bsdc) == 0
				&&	strcmp(rrp1.Bsdk,rrp2.Bsdk) == 0
				&&	strcmp(rrp1.Bsdn,rrp2.Bsdn) == 0
				&&	strcmp(rrp1.Bsds,rrp2.Bsds) == 0
				&&	strcmp(rrp1.Ctrc,rrp2.Ctrc) == 0
				&&	strcmp(rrp1.Type,rrp2.Type) == 0
				&&	strcmp(rrp1.Fctc,rrp2.Fctc) == 0

				&&	rrp1.Sdu1 == rrp2.Sdu1
				&&	rrp1.Sex1 == rrp2.Sex1
				&&	rrp1.Ssh1 == rrp2.Ssh1
				&&	rrp1.Bkd1 == rrp2.Bkd1

				&&	rrp1.Esbg == rrp2.Esbg
				&&	rrp1.Lsen == rrp2.Lsen

				&&	rrp1.Sbgi == rrp2.Sbgi
				&&	rrp1.Seni == rrp2.Seni
				;
		}
#else
		bool blOK = true;
		blOK =	blOK && rrp1.Sdgu == rrp2.Sdgu;
		if (!blOK)
			TRACE("rrp1.Sdgu == rrp2.Sdgu");
		if (!blOK)
			return blOK;

		blOK =	blOK && rrp1.Bsdu == rrp2.Bsdu;			
		if (!blOK)
			TRACE("rrp1.Bsdu == rrp2.Bsdu");
		if (!blOK)
			return blOK;


		blOK =	blOK && strcmp(rrp1.Bewc,rrp2.Bewc) == 0;
		if (!blOK)
			TRACE("rrp1.Bewc == rrp2.Bewc");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Bkr1,rrp2.Bkr1) == 0;
		if (!blOK)
			TRACE("rrp1.Bkr1 == rrp2.Bkr1");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Bsdc,rrp2.Bsdc) == 0;
		if (!blOK)
			TRACE("rrp1.Bsdc == rrp2.Bsdc");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Bsdk,rrp2.Bsdk) == 0;
		if (!blOK)
			TRACE("rrp1.Bsdk == rrp2.Bsdk");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Bsdn,rrp2.Bsdn) == 0;
		if (!blOK)
			TRACE("rrp1.Bsdn == rrp2.Bsdn");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Bsds,rrp2.Bsds) == 0;
		if (!blOK)
			TRACE("rrp1.Bsds == rrp2.Bsds");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Ctrc,rrp2.Ctrc) == 0;
		if (!blOK)
			TRACE("rrp1.Ctrc == rrp2.Ctrc");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Type,rrp2.Type) == 0;
		if (!blOK)
			TRACE("rrp1.Type == rrp2.Type");
		if (!blOK)
			return blOK;

		blOK =	blOK && strcmp(rrp1.Fctc,rrp2.Fctc) == 0;
		if (!blOK)
			TRACE("rrp1.Fctc == rrp2.Fctc");
		if (!blOK)
			return blOK;

		blOK =	blOK && rrp1.Sdu1 == rrp2.Sdu1;
		if (!blOK)
			TRACE("rrp1.Sdu1 == rrp2.Sdu1");
		if (!blOK)
			return blOK;

		blOK =	blOK && rrp1.Sex1 == rrp2.Sex1;
		if (!blOK)
			TRACE("rrp1.Sex1 == rrp2.Sex1");
		if (!blOK)
			return blOK;

		blOK =	blOK && rrp1.Ssh1 == rrp2.Ssh1;
		if (!blOK)
			TRACE("rrp1.Ssh1 == rrp2.Ssh1");
		if (!blOK)
			return blOK;

		blOK =	blOK && rrp1.Bkd1 == rrp2.Bkd1;
		if (!blOK)
			TRACE("rrp1.Bkd1 == rrp2.Bkd1");
		if (!blOK)
			return blOK;



		blOK =	blOK && rrp1.Esbg == rrp2.Esbg;
		if (!blOK)
			TRACE("rrp1.Esbg == rrp2.Esbg");
		if (!blOK)
			return blOK;

		blOK =	blOK && rrp1.Lsen == rrp2.Lsen;
		if (!blOK)
			TRACE("rrp1.Lsen == rrp2.Lsen");
		if (!blOK)
			return blOK;

		if (rrp1.Bkd1 != 0)	// we do have a break length !
		{
			blOK =	blOK && rrp1.Bkf1 == rrp2.Bkf1;
			if (!blOK)
				TRACE("rrp1.Bkf1 == rrp2.Bkf1");
			if (!blOK)
				return blOK;

			blOK =	blOK && rrp1.Bkt1 == rrp2.Bkt1;
			if (!blOK)
				TRACE("rrp1.Bkt1 == rrp2.Bkt1");
			if (!blOK)
				return blOK;

			blOK =	blOK && rrp1.Brkf == rrp2.Brkf;
			if (!blOK)
				TRACE("rrp1.Brkf == rrp2.Brkf");
			if (!blOK)
				return blOK;

			blOK =	blOK && rrp1.Brkt == rrp2.Brkt;
			if (!blOK)
				TRACE("rrp1.Brkt == rrp2.Brkt");
			if (!blOK)
				return blOK;
		}


		blOK =	blOK && rrp1.Sbgi == rrp2.Sbgi;
		if (!blOK)
			TRACE("rrp1.Sbgi == rrp2.Sbgi");
		if (!blOK)
			return blOK;

		blOK =	blOK && rrp1.Seni == rrp2.Seni;
		if (!blOK)
			TRACE("rrp1.Seni == rrp2.Seni");
		if (!blOK)
			return blOK;

		return blOK;
#endif

	}

	friend bool operator!=(const SDTDATA& rrp1,const SDTDATA& rrp2)
	{
		return !operator==(rrp1,rrp2);
	}

	CString	GetKey()
	{
		CString olKey;
		olKey.Format("%d%s%s%s%s%ld%ld%s",Bsdu,Bkr1,Bsdc,Bsdn,Type,Esbg.GetTime(),Lsen.GetTime(),Fctc);
		return olKey;
	}
	
 };	
//typedef struct SdtDataStruct SDTDATA;


// the broadcast CallBack function, has to be outside the CedaSdtData class
void ProcessSdtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


class CedaSdtData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<SDTDATA> omData;
	CUIntArray			 omSelectedData;
	CCSPtrArray<SDTDATA> omRedrawDuties;

    CMapPtrToPtr		omUrnoMap;
	CMapStringToPtr		omDnamMap;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	bool ChangeTime(long lpUrno,CTime opStartTime,CTime opEndTime,GanttDynType bpDynType,BOOL bpSave = TRUE);
// Operations
public:
    CedaSdtData();
	~CedaSdtData();
	void Register(void);
	bool Read(char *pspWhere = NULL);
	bool ReadRealSdt(char *pspWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<SDTDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS);
	bool ReadSdtWithSdgu(long lpSdgu);	
	bool DeleteSdtWithRos(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx);

	bool DeleteSdtWithSplu(long lpSplUrno);

	bool DeleteVirtuellSdtsBySdg(long lpSdgu);
	bool DeleteVirtuellSdtsByMsdu(long lpMsdu);
	//bool DeleteAllSdtsWithRosu(long lpRosu,bool bpWithDdx = true);
	//bool DeleteSdtWithRosuBetweenTimes(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx = true);
	//void PrepareSdtData(SDTDATA *prpSdt);
    //bool InsertSdt(const SDTDATA *prpSdtData);    // used in PrePlanTable only
    //bool UpdateSdt(const SDTDATA *prpSdtData);    // used in PrePlanTable only
 	void ProcessSdtBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool AddSdtInternal(SDTDATA *prpSdt, bool bpWithDDX = false);
	bool UpdateSdtInternal(SDTDATA *prpSdt);
	bool ClearAll();


	CString GetTotalFctc(long lpUrno);
	CString GetTotalQuali(long lpUrno);

	SDTDATA * GetSdtByUrno(long pcpUrno);
	bool GetSdtsSdguAndType(long lpSdgu, char cpType,CCSPtrArray <SDTDATA> &opSdtData,bool bpAdd = false);

	bool GetSdtByGplUrno(long lpGplu, CCSPtrArray <SDTDATA> &opSdtData);
	bool GetSdtByGspUrno(long lpGspu, CCSPtrArray <SDTDATA> &opSdtData);
	bool GetSdtsBetweenTimes(CTime opStart,CTime opEnd,CCSPtrArray<SDTDATA> &opSdtArray);
	bool GetSdtsBetweenTimes(CTime opStart,CTime opEnd,CMapPtrToPtr &opSdtUrnoMap);
	bool GetSdtsBetweenTimesAndSdgu(CTime opStart,CTime opEnd,CMapPtrToPtr &ropSdgUrnoMap,CMapPtrToPtr &opSdtUrnoMap);
	bool GetSdtsBetweenTimesAndSdgu(CTime opStart,CTime opEnd,long lpSdgu,CMapPtrToPtr &opSdtUrnoMap);
	bool GetSdtsBetweenTimesAndDrrFilter(CTime opStart,CTime opEnd,CMapPtrToPtr &opSdtUrnoMap,bool bpShowActualDsr,int ipLevel);
	bool GetSdtsByGplus(CString opGplus,CMapPtrToPtr &opSdtUrnoMap);
	bool ChangeSdtTime(long lpSdtUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave = false);
	bool SdtExist(long Urno);
	bool DnamExist(CString opDnam);
	bool InsertSdt(SDTDATA *prpSdtData, BOOL bpWithSave = FALSE,bool bpSendDdx = true);
	bool UpdateSdt(SDTDATA *prpSdtData, BOOL bpWithSave = FALSE ,bool bpSendDdx = true);
	bool DeleteSdt(SDTDATA *prpSdtData, BOOL bpWithSave = FALSE,bool bpSendDdx = true);
	bool SaveSdt(SDTDATA *prpSdt);
	bool DeleteSdtInternal(SDTDATA *prpSdt);
	void MakeSdtSelected(SDTDATA *prlSdt, BOOL bpSelect);
	void DeSelectAll();
	void Release(CCSPtrArray<SDTDATA> *popSDTList = NULL,long lpSdgu = 0);
	void ReleaseVirtuellSdts(long lpSdgu);
	void GetAllDnam(CStringArray &ropList);
	bool GetSelectedSdt(CCSPtrArray<SDTDATA> *popSelectedSdt);
	void DeleteBySdgu(long lpSdgUrno);
	void GetSdtByBsdu(long lpBsdu,CCSPtrArray<SDTDATA> *popSdtData);
	void GetSdtByRosu(long lpSdgu,CCSPtrArray<SDTDATA> *popSdtData);
	bool DeleteSdtWithSdgu(SDTDATA *prpSdtData,bool bpWithSave = true);

	void GetAllAutoSdts(CCSPtrArray <SDTDATA> &opSdtData);
	void DeleteAutoSdts();
private:
	void PrepareSdtData(SDTDATA *prpSdt);

	
	CMapPtrToPtr omTotalFctcMap;
	CCSPtrArray<CString> omTotalFctcArray;

	CMapPtrToPtr omTotalQualiMap;
	CCSPtrArray<CString> omTotalQualiArray;

	CMapStringToPtr	omSdguAndTypeMap;


};

extern CedaSdtData ogSdtData;
extern CedaSdtData ogSdtRosterData;
extern CedaSdtData ogSdtShiftData;

#endif
