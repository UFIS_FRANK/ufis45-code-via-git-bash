// MasstabDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <MasstabDlg.h>
#include <CCSGlobl.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMasstabDlg dialog


CMasstabDlg::CMasstabDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMasstabDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMasstabDlg)
	m_4hVal = -1;
	m_100PercVal = -1;
	m_RBMinVal = -1;
	m_BaseShift = TRUE;
	m_Graphic = TRUE;
	m_Shifts = TRUE;
	m_LowerBound = 0;
	m_UpperBound = 0;
	m_FixValue = 10;
	m_BorderFix = 1;
	//}}AFX_DATA_INIT

	m_RBMinVal = 1;

	m_FontHeight = 8;
		
	m_4hVal =2;

	bmTimeLines = false;

}


void CMasstabDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMasstabDlg)
	DDX_Control(pDX, IDC_FONT, m_Font);
	DDX_Control(pDX, IDC_TEST, m_TestText);
	DDX_Control(pDX, IDC_STARTTIME, m_Time);
	DDX_Control(pDX, IDC_STARTDATE, m_Date);
	DDX_Control(pDX, IDC_RB_MIN, m_Min);
	DDX_Control(pDX, IDC_HEIGHT, m_Height);
	DDX_Control(pDX, IDC_FLT_TIMELINES, m_TimeLines);
	DDX_Control(pDX, IDC_4H, m_4h);
	DDX_Control(pDX, IDC_100PERCENT, m_100Proz);
	DDX_Radio(pDX, IDC_4H, m_4hVal);
	DDX_Radio(pDX, IDC_100PERCENT, m_100PercVal);
	DDX_Radio(pDX, IDC_RB_MIN, m_RBMinVal);
	DDX_Check(pDX, IDC_BASESHIFT, m_BaseShift);
	DDX_Check(pDX, IDC_GRAPHIC, m_Graphic);
	DDX_Check(pDX, IDC_SHIFTS, m_Shifts);
	DDX_Text(pDX, IDC_LOWERBOUND, m_LowerBound);
	DDX_Text(pDX, IDC_UPPERBOUND, m_UpperBound);
	DDX_Text(pDX, IDC_FIXVALUE, m_FixValue);
	DDX_Radio(pDX, IDC_BORDERFIX, m_BorderFix);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMasstabDlg, CDialog)
	//{{AFX_MSG_MAP(CMasstabDlg)
	ON_BN_CLICKED(IDC_4H, On4h)
	ON_BN_CLICKED(IDC_24H, On24h)
	ON_BN_CLICKED(IDC_12H, On12h)
	ON_BN_CLICKED(IDC_10H, On10h)
	ON_BN_CLICKED(IDC_6H, On6h)
	ON_BN_CLICKED(IDC_8H, On8h)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMasstabDlg message handlers

void CMasstabDlg::On4h() 
{
	m_Hour = 4;
	m_Height.SetPos(4 * ONE_HOUR_POINT);    
}	


void CMasstabDlg::On24h() 
{
	m_Hour = 24;
	m_Height.SetPos(24 * ONE_HOUR_POINT);    
}

void CMasstabDlg::On12h() 
{
	m_Hour = 12;
	m_Height.SetPos(12 * ONE_HOUR_POINT);    
}

void CMasstabDlg::On10h() 
{
	m_Hour = 10;
	m_Height.SetPos(10 * ONE_HOUR_POINT);    
}

void CMasstabDlg::On6h() 
{
	m_Hour = 6;
	m_Height.SetPos(6 * ONE_HOUR_POINT);    
}

void CMasstabDlg::On8h() 
{
	m_Hour = 8;	
	m_Height.SetPos(8 * ONE_HOUR_POINT);    
}

void CMasstabDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	
	    //CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
    //UpdateData();

    m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;
	m_4hVal = -1;
    if (m_Hour == 4)
	{
        m_4hVal = 0;
	}
    else if (m_Hour == 6)
	{
        m_4hVal = 1;
	}
    else if (m_Hour == 8)
	{
        m_4hVal = 2;
	}
    else if (m_Hour == 10)
	{
        m_4hVal = 3;
	}
    else if (m_Hour == 12)
	{
        m_4hVal = 4;
	}
    else if (m_Hour == 24)
	{
        m_4hVal = 5;
	}
    else if (m_Hour == 0)
    {
        m_Hour = 1;
    }

    
    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);


	int ilFontPos = m_Font.GetPos();
	m_TestText.SetFont(&ogScalingFonts[ilFontPos]);
	m_TestText.UpdateWindow();
    
	UpdateData(FALSE);

}

BOOL CMasstabDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	
	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING999));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING998));
	}

	polWnd = GetDlgItem(IDC_FENSTER);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61286));
	}

	polWnd = GetDlgItem(IDC_GRAPHICTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61287));
	}
	polWnd = GetDlgItem(IDC_SHIFTTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61288));
	}
	polWnd = GetDlgItem(IDC_BASETEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(COV_BTN_BASICSHIFTS));
	}
	polWnd = GetDlgItem(IDC_GPARAM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61289));
	}
	polWnd = GetDlgItem(IDC_MAXLINE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61290));
	}
	polWnd = GetDlgItem(IDC_MINLINE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61291));
	}
	polWnd = GetDlgItem(IDC_BOUND);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61292));
	}
	polWnd = GetDlgItem(IDC_BORDERFIX);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61293));
	}
	polWnd = GetDlgItem(IDC_RADIO2);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61294));
	}
	polWnd = GetDlgItem(IDC_DISPBEG);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61295));
	}
	polWnd = GetDlgItem(IDC_UHR);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61296));
	}
	polWnd = GetDlgItem(IDC_24H);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61297));
	}
	polWnd = GetDlgItem(IDC_12H);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61298));
	}
	polWnd = GetDlgItem(IDC_10H);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61299));
	}
	polWnd = GetDlgItem(IDC_8H);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61300));
	}
	polWnd = GetDlgItem(IDC_6H);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61301));
	}
	polWnd = GetDlgItem(IDC_4H);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61302));
	}
	polWnd = GetDlgItem(IDC_SCALE0);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61303));
	}
	polWnd = GetDlgItem(IDC_SCALE6);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61304));
	}
	polWnd = GetDlgItem(IDC_SCALE12);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61305));
	}
	polWnd = GetDlgItem(IDC_SCALE18);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61306));
	}
	polWnd = GetDlgItem(IDC_SCALE24);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61307));
	}
	polWnd = GetDlgItem(IDC_ZEITRAHMEN);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61308));
	}
	polWnd = GetDlgItem(IDC_BALKEN);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61309));
	}
	polWnd = GetDlgItem(IDC_SCALES);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61310));
	}
	polWnd = GetDlgItem(IDC_SCALEM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61311));
	}
	polWnd = GetDlgItem(IDC_SCALEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61312));
	}
	polWnd = GetDlgItem(IDC_SCALEXL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61313));
	}


	SetWindowText(LoadStg(IDS_STRING61314));


	m_4hVal = -1;
   if (m_Hour == 4)
	{
        m_4hVal = 0;
	}
    else if (m_Hour == 6)
	{
        m_4hVal = 1;
	}
    else if (m_Hour == 8)
	{
        m_4hVal = 2;
	}
    else if (m_Hour == 10)
	{
        m_4hVal = 3;
	}
    else if (m_Hour == 12)
	{
        m_4hVal = 4;
	}
    else if (m_Hour == 24)
	{
        m_4hVal = 5;
	}
    
    m_Height.SetRange(0, 120);
	m_Height.SetTicFreq(10);
    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);

	m_Font.SetRange(0,19);
	m_Font.SetTicFreq(1);
    m_Font.SetPos(m_FontHeight);

	m_TestText.SetFont(&ogScalingFonts[m_FontHeight]);

	if(bmTimeLines)
	{
		m_TimeLines.SetCheck(1);
	}
	else
	{
		m_TimeLines.SetCheck(0);
	}



	CTime olTime = CTime::GetCurrentTime();
	m_Date.SetWindowText(olTime.Format("%d.%m.%Y"));
	m_Time.SetWindowText(olTime.Format("%H:%M"));


	m_Date.SetWindowText(omDispStartTime.Format("%d.%m.%Y"));
	m_Time.SetWindowText(omDispStartTime.Format("%H:%M"));

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CMasstabDlg::SetDispStartTime(CTime opDispStartTime)
{
	omDispStartTime = opDispStartTime;	
}

void CMasstabDlg::SetMaxDispStartTime(CTime opMaxDispStartTime)
{
	omMaxDispStartTime = opMaxDispStartTime;	
}

void CMasstabDlg::SetMinDispStartTime(CTime opMinDispStartTime)
{
	omMinDispStartTime = opMinDispStartTime;	
}

CTime CMasstabDlg::GetDispStartTime()
{
	return omDispStartTime;
}

void CMasstabDlg::SetDispDuration(CTimeSpan opDuration)
{
	omDuration = opDuration	;
	m_Hour = opDuration.GetTotalHours();
	if (opDuration.GetMinutes() == 59)
		++m_Hour;

}

CTimeSpan CMasstabDlg::GetDispDuration()
{
	return omDuration;
}

void CMasstabDlg::OnOK() 
{
	CString olStrDate, olStrTime,olTmpBound;

	CWnd *polWnd = NULL;
   	polWnd = GetDlgItem(IDC_FIXVALUE);

	if(polWnd != NULL)
	{
		polWnd->GetWindowText(olTmpBound);
		if(olTmpBound.IsEmpty())
		{
			polWnd->SetWindowText("10");
		}
	}
   	polWnd = GetDlgItem(IDC_UPPERBOUND);

	if(polWnd != NULL)
	{
		polWnd->GetWindowText(olTmpBound);
		if(olTmpBound.IsEmpty())
		{
			polWnd->SetWindowText("0");
		}
	}
   	polWnd = GetDlgItem(IDC_LOWERBOUND);

	if(polWnd != NULL)
	{
		polWnd->GetWindowText(olTmpBound);
		if(olTmpBound.IsEmpty())
		{
			polWnd->SetWindowText("0");
		}
	}
 

    m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;

	omDuration = CTimeSpan(0,m_Hour,0,0);

	m_Date.GetWindowText(olStrDate);
	m_Time.GetWindowText(olStrTime);
	CTime olDate, olTime;
	olDate = DateStringToDate(olStrDate);
	if(olDate != TIMENULL)
	{
		olTime = HourStringToDate(olStrTime, olDate);
	}
	if(olDate == TIMENULL || olTime == TIMENULL )
	{
		CString olText, olAdd;
		olText = LoadStg(IDS_NO_DATE_TIME);
		olAdd = LoadStg(IDS_WARNING);
		MessageBox(olText, olAdd, MB_OK);
		return;

	}
	CTime olDispStartTime = CTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(),olTime.GetMinute(),0);
	if(olDispStartTime >= omMaxDispStartTime)
	{
		CString olText, olAdd;
		olText = LoadStg(IDS_STRING61210);
		olAdd = LoadStg(IDS_WARNING);
		MessageBox(olText, olAdd, MB_OK);
		return;

	}

	if(olDispStartTime < omMinDispStartTime)
	{
		CString olText, olAdd;
		olText = LoadStg(IDS_STRING61210);
		olAdd = LoadStg(IDS_WARNING);
		MessageBox(olText, olAdd, MB_OK);
		return;

	}

	omDispStartTime = olDispStartTime;

	m_FontHeight = m_Font.GetPos();
	bmTimeLines = (m_TimeLines.GetCheck() == 1);

	CDialog::OnOK();
}


bool CMasstabDlg::GetMin()
{
	return (m_RBMinVal == 0)?true:false;
}

bool CMasstabDlg::GetTimeLines()
{
	return bmTimeLines;
}

int CMasstabDlg::GetFont()
{
	return 	m_FontHeight;

}
bool CMasstabDlg::GetGrapicState()
{
	return 	(m_Graphic == TRUE)?true:false;

}
bool CMasstabDlg::GetShiftState()
{
	return 	(m_Shifts == TRUE)?true:false;

}
bool CMasstabDlg::GetBaseShiftState()
{
	return 	(m_BaseShift == TRUE)?true:false;

}
int CMasstabDlg::GetUpperBound()
{
	return 	m_UpperBound;
}

int CMasstabDlg::GetLowerBound()
{
	return 	m_LowerBound;
}

int CMasstabDlg::GetFixValue()
{
	return 	(m_BorderFix == 0)?m_FixValue:10;
}

bool CMasstabDlg::IsBorderFix()
{
	return 	(m_BorderFix == 0)?true:false;
}

	
void CMasstabDlg::SetGrapicState(bool bpState)
{
	m_Graphic = (bpState)? TRUE:FALSE;

}
void CMasstabDlg::SetShiftState(bool bpState)
{
	m_Shifts = (bpState)? TRUE:FALSE;

}
void CMasstabDlg::SetBaseShiftState(bool bpState)
{
	m_BaseShift = (bpState)? TRUE:FALSE;
}
