#if !defined(AFX_ACPROGRESSBAR_H__E20A35B1_3ACE_11D5_9797_0050DAE32E8C__INCLUDED_)
#define AFX_ACPROGRESSBAR_H__E20A35B1_3ACE_11D5_9797_0050DAE32E8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ACProgressBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CACProgressBar dialog

class CACProgressBar : public CDialog
{
// Construction
public:
	CACProgressBar(CWnd* pParent = NULL, int ipSize = 100);   // standard constructor

	void SetProgress(int ipLoopNumber,int ipProgress = 1);

	BOOL DestroyWindow(void);
	bool CheckContinue();
	int  imTotalLoops;

// Dialog Data
	//{{AFX_DATA(CACProgressBar)
	enum { IDD = IDD_ACPROGRESS };
	CProgressCtrl	m_ProgressCtrl;
	CStatic	m_LoopNumber;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CACProgressBar)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CACProgressBar)
	afx_msg void OnStop();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	bool bmStop;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACPROGRESSBAR_H__E20A35B1_3ACE_11D5_9797_0050DAE32E8C__INCLUDED_)
