// cCfgd.cpp - Class for handling Cfgloyee data
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <CCScedacom.h>
#include <CCScedadata.h>
#include <ccsddx.h>
#include <CCSbchandle.h>
#include <ccsddx.h>
#include <basicdata.h>
#include <CedaCfgData.h>
#include <cviewer.h>
//#include <cjobd.h>

const char* CedaCfgData::pomDefaultName = "#__DEFAULT__#";

CedaCfgData::CedaCfgData()
{                  
    // Create an array of CEDARECINFO for CFGDATA
    BEGIN_CEDARECINFO(CFGDATA, CfgDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Appn,"APPN")
        FIELD_CHAR_TRIM(Ctyp,"CTYP")
        FIELD_CHAR_TRIM(Ckey,"CKEY")
        FIELD_DATE(Vafr,"VAFR")
        FIELD_DATE(Vato,"VATO")
        FIELD_CHAR_TRIM(Pkno,"PKNO")
        FIELD_CHAR_TRIM(Text,"TEXT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CfgDataRecInfo)/sizeof(CfgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CfgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
	strcpy(pcmTableName,"VCD");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields, "URNO,APPN,CTYP,CKEY,VAFR,VATO,PKNO,TEXT");
	pcmFieldList = pcmListOfFields;

	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,		
								MON_STAFF_BOARD_STRING,	
								MON_NORESOURCE_STRING,	
								MON_FLIGHTCHANGES_STRING,
								MON_FLIGHTSCHEDULE_STRING,
								MON_COVERAGE_STRING,		
								MON_RULES_STRING,
								MON_DUTYROSTER_STRING);
}

CedaCfgData::~CedaCfgData()
{
	ogDdx.UnRegister(this,NOTUSED);
	omData.DeleteAll();
	//omViews.DeleteAll();
	omUrnoMap.RemoveAll();
	omCkeyMap.RemoveAll();
	omRecInfo.DeleteAll();
	ClearAllViews();
}

bool CedaCfgData::ReadCfgData()
{
    char pclWhere[512];
	bool ilRc = true;
	int ilLc = 0;
    // Select data from the database
    sprintf(pclWhere,"WHERE APPN='CCS_%s' AND (CTYP = 'CKI-SETUP')",ogAppName);
    omCkeyMap.RemoveAll();
	omUrnoMap.RemoveAll();
    omData.DeleteAll();

    if (CedaAction("RT", pclWhere) == true)
	{
		// this section is responsible for SETUP
		// Load data from CedaData into the dynamic array of record
		for (ilLc = 0; ilRc == true; ilLc++)
		{
			CFGDATA *prlCfg = new CFGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == true)
			{
				PrepareCfgData(prlCfg);
				omData.Add(prlCfg);
				omCkeyMap.SetAt(prlCfg->Ckey,prlCfg);
				omUrnoMap.SetAt((void *)prlCfg->Urno,prlCfg);
			}
			else
			{
				delete prlCfg;
			} 
		}
	}
	MakeCurrentUser(); 

// this section is responsible for the VIEWS in charts and lists
	ilRc = true;

    // Select data from the database
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP = 'VIEW-DATA' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	ClearAllViews();
    for (ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			PrepareViewData(&rlCfg);
		}
	}

    return true;
}

bool CedaCfgData::ReadMonitorSetup()
{
	char pclWhere[2048]="";
	bool ilRc = true;
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP = 'MONITORS' AND PKNO = '%s')", ogAppName, ogBasicData.omUserID);

	rmMonitorSetup.Urno = ogBasicData.GetNextUrno();
	sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
	strcpy(rmMonitorSetup.Ctyp, "MONITORS");
	strcpy(rmMonitorSetup.Ckey, "");
	rmMonitorSetup.Vafr = CTime::GetCurrentTime();
	rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
	strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
	rmMonitorSetup.IsChanged = DATA_NEW;
	sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
								MON_COUNT_STRING,		
								MON_STAFF_BOARD_STRING,	
								MON_NORESOURCE_STRING,	
								MON_FLIGHTCHANGES_STRING,
								MON_FLIGHTSCHEDULE_STRING,
								MON_COVERAGE_STRING,		
								MON_RULES_STRING,
								MON_DUTYROSTER_STRING);
	
	if (CedaAction("RT", pclWhere) == false)
        return false;

	//Initialize the datastructure of for the views
	CFGDATA *prlCfg = new CFGDATA;
	bool blFound = true;
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		if ((ilRc = GetBufferRecord(ilLc,prlCfg)) == false)
		{
			blFound = false;
/*			rmMonitorSetup.Urno = ogBasicData.GetNextUrno();
			sprintf(rmMonitorSetup.Appn, "CCS_%s", ogAppName);
			strcpy(rmMonitorSetup.Ctyp, "MONITORS");
			strcpy(rmMonitorSetup.Ckey, "");
			rmMonitorSetup.Vafr = CTime::GetCurrentTime();
			rmMonitorSetup.Vato = CTime(2020, 12, 31, 23, 59, 00);
			strcpy(rmMonitorSetup.Pkno, ogBasicData.omUserID);
			rmMonitorSetup.IsChanged = DATA_NEW;
			sprintf(rmMonitorSetup.Text, "%s=1#%s=1#%s=1#%s=1#%s=1#%s=1#%s=1",
										MON_COUNT_STRING,		
										MON_STAFF_BOARD_STRING,	
										MON_NORESOURCE_STRING,	
										MON_FLIGHTCHANGES_STRING,
										MON_FLIGHTSCHEDULE_STRING,
										MON_COVERAGE_STRING,		
										MON_RULES_STRING,
										MON_DUTYROSTER_STRING);
*/
		}
		else
		{
			rmMonitorSetup = *prlCfg;
			rmMonitorSetup.IsChanged = DATA_UNCHANGED;
		}
	}
	delete prlCfg;
    return true;
}
int CedaCfgData::GetMonitorForWindow(CString opWindow)
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmMonitorSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(opWindow) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				char pclTmp[100];
				char x[111];
				CString olString = olSetupList[i];
				strcpy(x, olString);
				sprintf(pclTmp, "%c", olString[ilPos+1]);
				ilRet = atoi(pclTmp);
			}
		}
	}
	return ilRet;
}

int CedaCfgData::GetMonitorCount()
{
	int ilRet = 1;
	CStringArray olSetupList;
	ExtractItemList(CString(rmMonitorSetup.Text), &olSetupList, '#');
	for(int i = 0; i < olSetupList.GetSize(); i++)
	{
		if(olSetupList[i].Find(MON_COUNT_STRING) != -1)
		{
			int ilPos = olSetupList[i].Find("=");
			if(ilPos != -1)
			{
				char pclTmp[100];
				CString olString = olSetupList[i];
				sprintf(pclTmp, "%c", olString[ilPos+1]);
				ilRet = atoi(pclTmp);
			}
		}
	}
	return ilRet;
}

bool CedaCfgData::ReadConflicts(CStringArray &opLines)
{
	char pclWhere[1024]="";
	int ilLc = 0;

	opLines.RemoveAll();
	bool ilRc = true;
	bool olRc = true;
//Now we read the conflict configuration
	ilRc = true;

    // Select data from the database
    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND (CTYP LIKE 'CONFLICT%%' AND PKNO = '%s') ORDER BY CTYP", ogAppName,ogBasicData.omUserID);
    olRc = CedaAction("RT", pclWhere);

    for (ilLc = 0; ilRc == true; ilLc++)
    {
		CFGDATA rlCfg;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			CString olStr = CString(rlCfg.Text);
			opLines.Add(olStr);
		}
	}
	if((ilLc < 15) || (olRc == false))
	{
		opLines.RemoveAll();
		opLines.Add(CString("0;0;Kein Konflikt;0;0;!;?;65280;8454016;Kein Konflikt"));
		opLines.Add(CString("1;1;Kein Einsatz;90;60;!;?;255;33023;Es ist noch kein Bedarf fuer diesem Flug abgedeckt"));
		opLines.Add(CString("2;2;Unterdeckung;80;0;!;?;33023;16711680;Es ist nur ein Bedarf fuer diesen Flug abgedeckt"));
		opLines.Add(CString("3;4;Unterdeckung;70;10;!;?;8388863;32896;Es sind noch nicht alle Bedarfe abgedeckt"));
		opLines.Add(CString("4;8;Delay;60;3;6;6;65535;65280;Neuer Text"));
		opLines.Add(CString("5;16;Cancellation;40;0;!;?;8388736;65280;Kein Konflikt"));
		opLines.Add(CString("6;32;Diversion;30;0;!;?;8421631;65280;Kein Konflikt"));
		opLines.Add(CString("7;64;Einsatzueberlappung;40;0;!;?;12615680;33023;Überlappung mit nachfolgendem Einsatz"));
		opLines.Add(CString("8;128;Bestätigung;30;0;!;?;4227327;255;Keine Einsatzbestätigung 15 Minuten vor Einsatzbeginn"));
		opLines.Add(CString("9;256;Gate Change;30;120;!;?;4210816;65280;Neues Gate"));
		opLines.Add(CString("10;512;Kein Bedarf;99;0;!;?;16777088;65280;Kein Bedarf"));
		opLines.Add(CString("11;1024;Equipment Change;30;0;!;?;4227327;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("12;2048;Überbuchung;30;0;!;?;16711680;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("13;4096;Bedarfsüberdeckung;30;0;!;?;8421376;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("14;8192;Keine Rückmeldung;30;0;!;?;65535;65280;Anderer Flugzeugtyp"));
		opLines.Add(CString("15;16384;Nicht im Pool;30;0;!;?;32896;65280;Mitarbeiter ist nicht dem Pool zugeteilt"));
	}
    return true;
}

bool CedaCfgData::SaveConflictData(CFGDATA *prpCfg)
{
	CString olListOfData;
	bool olRc = true;
	char pclData[824];
	char pclSelection[1024]="";
	//First we delete the entry
	//MWO TO DO CCS_FPMS durch APPN ersetzen
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND CTYP = '%s')", ogAppName, ogBasicData.omUserID, prpCfg->Ctyp);
	olRc = CedaAction("DRT",pclSelection);

	//and create it new
	MakeCedaData(olListOfData,prpCfg);
	strcpy(pclData,olListOfData);
	olRc = CedaAction("IRT","","",pclData);

	return olRc;
}

void CedaCfgData::MakeCurrentUser(void)
{
	// Fills the setup struct for the user with current login-USERID
	CCSPtrArray<CFGDATA> olData;
	int ilCountSets = 0;
	int ilCount = omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = omData[i];
		if((strcmp(rlC.Pkno, ogBasicData.omUserID) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			olData.NewAt(0, rlC);
			ilCountSets++;
		}
	}
	
	if(ilCountSets == 1)
	{
		CString olParameters;
		CString olTmp1, olTmp2;
		CFGDATA rlC1 = olData.GetAt(0);
		olTmp1 = rlC1.Text;
		olParameters = olTmp1;
		InterpretSetupString(olParameters, &rmUserSetup);
	}
	else	// default initialization
	{
		rmUserSetup.DET1 = "300";
		rmUserSetup.DET2 = "60";
		rmUserSetup.NAME = "<LANM><+>40<+>,<+><FINM><+>40<+>";
		rmUserSetup.NDFC.Format("%ld",GRAY);
		rmUserSetup.EVSO = "0";
	}
	olData.DeleteAll();
}

void CedaCfgData::DeleteViewFromDiagram(CString opDiagram, CString olView)
{
	bool olRc = true;
	char pclSelection[1024]="";
	char pclDiagram[100]="";
	char pclViewName[100]="";

	strcpy(pclDiagram, opDiagram);
	strcpy(pclViewName, olView);
	//MWO TO DO CCS_FPMS ==> APPL
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE '%%VIEW=%s%%')", ogAppName, ogBasicData.omUserID, pclDiagram, pclViewName);
	olRc = CedaAction("DRT",pclSelection);
}

void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	bool olRc = true; 
	CString olListOfData;
	char pclSelection[1024]="";
	char pclData[824];
	char pclDiagram[100]="";
	char pclText[401]="";
	char pclTmp[512]="";
	//

	strcpy(pclDiagram, opDiagram);

	MakeCedaString(opViewName);

	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s%%')", 
		ogAppName, ogBasicData.omUserID, pclDiagram,opViewName);

	
	olRc = CedaAction("DRT",pclSelection);
	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for(int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{

			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							sprintf(pclText, "VIEW=%s#;", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s;", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s;TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								char pclS[100]="";
								sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								if (strncmp(pclS,"STARTDATE=",10) && strncmp(pclS,"ENDDATE=",8))
								{
									strcat(pclTmp, pclS);
								}
								if(strlen(pclTmp)>300)
								{
									//we have to devide the row into two parts
									CFGDATA rlCfg2;
									rlCfg2.Urno = ogBasicData.GetNextUrno();
									strcpy(rlCfg2.Ckey, rlCfg.Ckey);
									strcpy(rlCfg2.Pkno, rlCfg.Pkno);
									strcpy(rlCfg2.Ctyp, rlCfg.Ctyp);
									strcpy(rlCfg2.Text, pclText);
									strcat(pclTmp, ";");
									strcat(rlCfg2.Text, pclTmp);
									pclTmp[0]='\0';
									rlCfg2.Vafr = CTime::GetCurrentTime();
									rlCfg2.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);									//And save the record
									MakeCedaData(olListOfData,&rlCfg2);
									strcpy(pclData,olListOfData);

								
									olRc = CedaAction("IRT","","",pclData);

								}
							}
							rlCfg.Urno = ogBasicData.GetNextUrno();
							strcat(pclTmp, ";");
							strcat(pclText, pclTmp);
							strcpy(rlCfg.Text, pclText);
							rlCfg.Vafr = CTime::GetCurrentTime();
							rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);							//and save to database
							MakeCedaData(olListOfData,&rlCfg);
							strcpy(pclData,olListOfData);

							olRc = CedaAction("IRT","","",pclData);

							rlCfg.IsChanged = DATA_UNCHANGED;
							pclText[0]='\0';
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#;", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s;TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, ";");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						rlCfg.Vafr = CTime::GetCurrentTime();
						rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);

						olRc = CedaAction("IRT","","",pclData);

						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';
					}
				}
			}
		i = ilC1;
		}
	}
}

/******************************
void CedaCfgData::UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[2024]="";
	char pclData[2048];
	char pclDiagram[100]="";
	char pclText[2024]="";
	char pclTmp[2024]="";
	//

	strcpy(pclDiagram, opDiagram);

	

	// to make it easier we delete all rows of the user and his configuration for the
	// specified diagram
	//MWO TO DO CCS_FPMS ==> APPL
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND "
		"CKEY = '%s' AND CTYP = 'VIEW-DATA' AND TEXT LIKE 'VIEW=%s%%')", 
		ogAppName, ogBasicData.omUserID, pclDiagram,opViewName);

	
	olRc = CedaAction("DRT",pclSelection);
	// after removing we create a completely new configuration
	int ilC1 = prpView.omNameData.GetSize();

	for(int i = 0; i < ilC1; i++)
	{
		if (opViewName == prpView.omNameData[i].ViewName) 
		{

			int ilC2 = prpView.omNameData[i].omTypeData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(prpView.omNameData[i].omTypeData[j].Type == "FILTER")
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						int ilC3 = prpView.omNameData[i].omTypeData[j].omTextData.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
							sprintf(pclTmp, "TYPE=%s#", prpView.omNameData[i].omTypeData[j].Type );
							strcat(pclText, pclTmp);
							sprintf(pclTmp, "PAGE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].omTextData[k].Page);
							strcat(pclText, pclTmp);
							pclTmp[0]='\0';
							int ilC4 = prpView.omNameData[i].omTypeData[j].omTextData[k].omValues.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								char pclS[1800]="";
								sprintf(pclS, "%s@", prpView.omNameData[i].omTypeData[j].omTextData[k].omValues[l]);
								strcat(pclTmp, pclS);
								if(strlen(pclTmp)>1800)
								{
									//we have to devide the row into two parts
									CFGDATA rlCfg2;
									rlCfg2.Urno = ogBasicData.GetNextUrno();
									strcpy(rlCfg2.Ckey, rlCfg.Ckey);
									strcpy(rlCfg2.Pkno, rlCfg.Pkno);
									strcpy(rlCfg2.Ctyp, rlCfg.Ctyp);
									strcpy(rlCfg2.Text, pclText);
									strcat(pclTmp, "#");
									strcat(rlCfg2.Text, pclTmp);
									pclTmp[0]='\0';
									//And save the record
									MakeCedaData(olListOfData,&rlCfg2);
									strcpy(pclData,olListOfData);
									olRc = CedaAction("IRT","","",pclData);
								}
							}
							rlCfg.Urno = ogBasicData.GetNextUrno();
							strcat(pclTmp, "#");
							strcat(pclText, pclTmp);
							strcpy(rlCfg.Text, pclText);
							rlCfg.Vafr = CTime::GetCurrentTime();
							rlCfg.Vato = rlCfg.Vafr + CTimeSpan(365*10, 0, 0, 0);
							//and save to database
							MakeCedaData(olListOfData,&rlCfg);
							strcpy(pclData,olListOfData);
							olRc = CedaAction("IRT","","",pclData);
							rlCfg.IsChanged = DATA_UNCHANGED;
							pclText[0]='\0';
						}
					}
				}
				else //not filter but ==> Group, Sort etc.
				{
					if(prpView.omNameData[i].ViewName != "<Default>")
					{
						CFGDATA rlCfg;
						rlCfg.Urno = ogBasicData.GetNextUrno();
						strcpy(rlCfg.Ckey, pclDiagram);
						strcpy(rlCfg.Pkno, ogBasicData.omUserID);
						strcpy(rlCfg.Ctyp, "VIEW-DATA");
						sprintf(pclText, "VIEW=%s#", prpView.omNameData[i].ViewName);
						sprintf(pclTmp, "TYPE=%s#TEXT=", prpView.omNameData[i].omTypeData[j].Type );
						strcat(pclText, pclTmp);
						pclTmp[0]='\0';
						int ilC3 = prpView.omNameData[i].omTypeData[j].omValues.GetSize();
						for(int k = 0; k < ilC3; k++)
						{
							char pclS[100]="";
							sprintf(pclS, "%s|", prpView.omNameData[i].omTypeData[j].omValues[k]);
							strcat(pclTmp, pclS);
						}
						strcat(pclTmp, "#");
						strcat(pclText, pclTmp);
						strcpy(rlCfg.Text, pclText);
						//and save to database
						MakeCedaData(olListOfData,&rlCfg);
						strcpy(pclData,olListOfData);
						olRc = CedaAction("IRT","","",pclData);
						rlCfg.IsChanged = DATA_UNCHANGED;
						pclText[0]='\0';
					}
				}
			}
		i = ilC1;
		}
	}
}

  *************************/
////////////////////////////////////////////////////////////////
// Interprets the raw data for one propertypage

BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
	char pclTEXT[1024]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, ";");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, ";");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal,psp);
		if (pclOriginal[strlen(pclOriginal)-1] == '#')
		{
			pclOriginal[strlen(pclOriginal)-1] = '\0';
		}
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(prpRawData->Type == "FILTER")
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}
/*****************************
BOOL CedaCfgData::MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA *prpRawData)
{
	strcpy(prpRawData->Ckey, prpCfg->Ckey);
	char *psp = NULL;
	char pclOriginal[2024]="";
	char pclVIEW[100]="";
	char pclTYPE[100]="";
	char pclPAGE[100]="";
	char pclTEXT[1024]="";

	strcpy(pclOriginal, prpCfg->Text);
	psp = strstr(pclOriginal, "#");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "VIEW=")) != NULL)
			strcpy(pclVIEW, pclPart);
		else if((pclTmp = strstr(pclPart, "TYPE=")) != NULL)
			strcpy(pclTYPE, pclPart);
		else if((pclTmp = strstr(pclPart, "PAGE=")) != NULL)
			strcpy(pclPAGE, pclPart);
		else if((pclTmp = strstr(pclPart, "TEXT=")) != NULL)
			strcpy(pclTEXT, pclPart);

		psp = strstr(pclOriginal, "#");
	}

	psp = strstr(pclTYPE, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Type, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclVIEW, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Name, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclPAGE, "=");
	if(psp == NULL)
	{
		if(prpRawData->Type == "FILTER")
		{
			return FALSE;
		}
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Page, pclOriginal);
		}
	}
	psp = NULL;
	psp = strstr(pclTEXT, "=");
	if(psp == NULL)
	{
		return FALSE;
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			return FALSE;
		}
		else
		{
			strcpy(prpRawData->Values, pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}
********************/
////////////////////////////////////////////////////////////////
//MWO: extracts the values which are comma-separated and copies them
//     into CStringArray of VIEW_TEXTDATA

// FILTER: pcpSepa = "@"
// SONST   pcpSepa = "|"
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues)
{
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclValue[100]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, "|");
	while(psp != NULL)
	{
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if(strcmp(pclPart, "") != 0)
			if (strncmp(pclPart,"STARTDATE=",10))
				if (strncmp(pclPart,"ENDDATE=",8))
					popValues->NewAt(popValues->GetSize(), pclPart);

		psp = strstr(pclOriginal, "|");
	}
}
/************************
void CedaCfgData::MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues, char *pcpSepa )
{
	char *psp = NULL;
	char pclOriginal[2024]="";
	char pclValue[100]="";

	strcpy(pclOriginal, pspValues);
	psp = strstr(pclOriginal, pcpSepa);
	while(psp != NULL)
	{
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if(strcmp(pclPart, "") != 0)
			 popValues->NewAt(popValues->GetSize(), pclPart);

		psp = strstr(pclOriginal, pcpSepa);
	}
}

************************/
////////////////////////////////////////////////////////////////
// MWO: prepares the nested arrays for the CFGDATA
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, &rlTextData.omValues);
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);
	}
	else
	{
		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, &rlTextData.omValues);
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, &rlTextData.omValues);
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, &rlTypeData.omValues);
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA rlTextData;
						rlTextData.Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, &rlTextData.omValues);
						prlFoundTypeData->omTextData.NewAt(prlFoundTypeData->omTextData.GetSize(), rlTextData);
					}
					else
					{ 
						MakeViewValues(rlRawData.Values, &prlFoundTextData->omValues);
					}
				}
				else
				{

				}
			}
		}
	}
}
/***********************
void CedaCfgData::PrepareViewData(CFGDATA *prpCfg)
{
	VIEWDATA *prlFoundViewData;
	VIEW_VIEWNAMES *prlFoundViewName;
	VIEW_TYPEDATA *prlFoundTypeData;
	VIEW_TEXTDATA *prlFoundTextData;
	RAW_VIEWDATA rlRawData;

	if(MakeRawViewData(prpCfg, &rlRawData) != TRUE)
	{
		return; //then the row is corrupt
	}
	prlFoundViewData = FindViewData(prpCfg);
	if(prlFoundViewData == NULL) 
	{
		//if the key does not exist we have generate an new on one from the root
		VIEW_TEXTDATA rlTextData;
		VIEW_TYPEDATA rlTypeData;
		VIEW_VIEWNAMES rlNameData;
		VIEWDATA rlViewData;
		rlViewData.Ckey = CString(rlRawData.Ckey);
		rlNameData.ViewName = CString(rlRawData.Name);
		rlTypeData.Type = CString(rlRawData.Type);
		if(strcmp(rlRawData.Type, "FILTER") == 0)
		{
			rlTextData.Page = CString(rlRawData.Page);
			MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
			rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
		}
		else
		{
			MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
		}
		rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
		rlViewData.omNameData.NewAt(rlViewData.omNameData.GetSize(), rlNameData);
		omViews.NewAt(omViews.GetSize(), rlViewData);

	}
	else
	{

		//the key exists, we have to look for the rest of the tree and
		//evaluate, whether parts of it do exist and merge them together
		//Now first let's look for existing of the view-name
		prlFoundViewName = FindViewNameData(&rlRawData);
		if(prlFoundViewName == NULL)
		{
			//the view itself exists but there are no entries

			VIEW_TEXTDATA rlTextData;
			VIEW_TYPEDATA rlTypeData;
			VIEW_VIEWNAMES rlNameData;
			rlNameData.ViewName = CString(rlRawData.Name);
			rlTypeData.Type = CString(rlRawData.Type);
			if(strcmp(rlRawData.Type, "FILTER") == 0)
			{
				rlTextData.Page = CString(rlRawData.Page);
				MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
				rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
			}
			else
			{
				MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
			}
			rlNameData.omTypeData.NewAt(rlNameData.omTypeData.GetSize(), rlTypeData);
			prlFoundViewData->omNameData.NewAt(prlFoundViewData->omNameData.GetSize(), rlNameData);
		}
		else
		{
			//there are name entries => let's have a look at typs and their data
			prlFoundTypeData = FindViewTypeData(&rlRawData);
			if(prlFoundTypeData == NULL) //***** OK
			{
				//View + name exist but no Type entries
				VIEW_TYPEDATA rlTypeData;
				VIEW_TEXTDATA rlTextData;
				rlTypeData.Type = CString(rlRawData.Type);
				if(strcmp(rlRawData.Type, "FILTER") == 0)
				{
					rlTextData.Page = CString(rlRawData.Page);
					MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
					rlTypeData.omTextData.NewAt(rlTypeData.omTextData.GetSize(), rlTextData);
				}
				else
				{
					MakeViewValues(rlRawData.Values, &rlTypeData.omValues, "|");
				}
				prlFoundViewName->omTypeData.NewAt(prlFoundViewName->omTypeData.GetSize(), rlTypeData);
			}
			else
			{
				if(rlRawData.Type == CString("FILTER"))//***
				{   
					prlFoundTextData = FindViewTextData(&rlRawData);
					if(prlFoundTextData == NULL) //*** OK
					{
						//View + Name + Type exist but no values for Filter
						VIEW_TEXTDATA rlTextData;
						rlTextData.Page = CString(rlRawData.Page);
						MakeViewValues(rlRawData.Values, &rlTextData.omValues, "@");
						prlFoundTypeData->omTextData.NewAt(prlFoundTypeData->omTextData.GetSize(), rlTextData);
					}
					else
					{ 
						MakeViewValues(rlRawData.Values, &prlFoundTextData->omValues, "@");
					}
				}
				else
				{

				}
			}
		}
	}
			
}

*********************/
/////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a view, e.g. Staffdia
VIEWDATA * CedaCfgData::FindViewData(CFGDATA *prlCfg)
{
	int ilCount = omViews.GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prlCfg->Ckey)
		{
			return &omViews[i];
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////
//MWO: Evaluates the existing of a viewname, e.g. Heute, Morgen
VIEW_VIEWNAMES * CedaCfgData::FindViewNameData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == prpRawData->Name)
				{	
					return &omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of a viewtyp, e.g. FILTER, GROUP
VIEW_TYPEDATA * CedaCfgData::FindViewTypeData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							return  &omViews[i].omNameData[j].omTypeData[k];
						}
					}
				}
			}
		}
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////////////
// MWO: Evaluates the existing of values for FILTER
VIEW_TEXTDATA * CedaCfgData::FindViewTextData(RAW_VIEWDATA *prpRawData)
{
	int ilCount = omViews.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omViews[i].Ckey == prpRawData->Ckey)
		{
			int ilC2 = omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(omViews[i].omNameData[j].ViewName == CString(prpRawData->Name))
				{	
					int ilC3 = omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(omViews[i].omNameData[j].omTypeData[k].Type == CString(prpRawData->Type))
						{
							int ilC4 = omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == CString(prpRawData->Page))
								{
									return &omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////
//MWO: Clear all buffers allocated in omViews
void CedaCfgData::ClearAllViews()
{
	while(omViews.GetSize() > 0)
	{
	//	VIEWDATA *prlView = &omViews[0];
		while(omViews[0].omNameData.GetSize() > 0)
		{
	//		VIEW_VIEWNAMES *prlViewName = &omViews[0].omNameData[0];
			while(omViews[0].omNameData[0].omTypeData.GetSize() > 0)
			{
	//			VIEW_TYPEDATA *prlViewType = &omViews[0].omNameData[0].omTypeData[0];
				while(omViews[0].omNameData[0].omTypeData[0].omTextData.GetSize() > 0)
				{
	//				VIEW_TEXTDATA *prlViewText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0];
					while(omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.GetSize() > 0)
					{
						CString *prlText = &omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues[0];
						omViews[0].omNameData[0].omTypeData[0].omTextData[0].omValues.DeleteAt(0);
					}
					omViews[0].omNameData[0].omTypeData[0].omTextData.DeleteAt(0);
				}
			//	omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAll();
			
				while(omViews[0].omNameData[0].omTypeData[0].omValues.GetSize() > 0)
				{
					omViews[0].omNameData[0].omTypeData[0].omValues.DeleteAt(0);
				}
			
				omViews[0].omNameData[0].omTypeData.DeleteAt(0);
			}
			omViews[0].omNameData.DeleteAt(0);
		}
		omViews.DeleteAt(0);
	}
}
// Prepare some whatif data, not read from database
void CedaCfgData::PrepareCfgData(CFGDATA *prpCfg)
{
}


/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class and StaffTable class)

bool CedaCfgData::InsertCfg(const CFGDATA *prpCfgData)
{

    if (CfgExist(prpCfgData->Urno))
	{
        return (UpdateCfgRecord(prpCfgData));
	}
    else
	{
        return (InsertCfgRecord(prpCfgData));
	}
}

bool CedaCfgData::UpdateCfg(const CFGDATA *prpCfgData)
{
    return(UpdateCfgRecord(prpCfgData));
}


BOOL CedaCfgData::CfgExist(long lpUrno)
{
	// don't read from database anymore, just check internal array
	CFGDATA *prpData;

	return(omUrnoMap.Lookup((void *) &lpUrno,(void *&)prpData) );
}

bool CedaCfgData::InsertCfgRecord(const CFGDATA *prpCfgData)
{
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "URNO,CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // CFGCKI ~ 430 Byte
    sprintf(CfgData, "%ld,%s,%s,%s,%s,%s,%s,%s",
        prpCfgData->Urno, 
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);

   return (CedaAction("IRT", "CFGCKI", CfgFields, "", "", CfgData));
}

bool CedaCfgData::UpdateCfgRecord(const CFGDATA *prpCfgData)
{
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s,%s",
		prpCfgData->Appn,
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Pkno,
		prpCfgData->Text);
	sprintf(pclSelection," where URNO = '%ld%'",prpCfgData->Urno);
	bool olRc = CedaAction("URT", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	//ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfgData);
	return olRc;
}


bool CedaCfgData::CreateCfgRequest(const CFGDATA *prpCfgData)
{
	CWaitCursor olWait;
    // update Cfgl data in table "SHFCKI"
    // insert new Cfg record into table "SHFCKI"
    char *CfgFields = "CTYP,CKEY,VAFR,VATO,TEXT";
    char CfgData[512]; // SHFCKI ~ 140 byte
	char pclSelection[64];
    sprintf(CfgData, "%s,%s,%s,%s,%s,%s",
		prpCfgData->Ctyp,
		prpCfgData->Ckey,
		prpCfgData->Vafr,
		prpCfgData->Vato,
		prpCfgData->Text,
		prpCfgData->Pkno);
	sprintf(pclSelection,"'%s'",prpCfgData->Ckey);
	bool olRc = CedaAction("CKO", "CFGCKI", CfgFields, pclSelection, "", CfgData);
	if (olRc == true)
	{
		MessageBox(NULL,CfgData,LoadStg(IDS_STRING465),MB_OK);
		olWait.Restore();
	}
	else
	{
		MessageBox(NULL,LoadStg(IDS_STRING466),LoadStg(IDS_STRING465),MB_OK);
		olWait.Restore();
	}
	return olRc;
}


// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
	 ogCfgData.ProcessCfgBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}


void  CedaCfgData::ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	if ((ipDDXType == BC_CFG_CHANGE) || (ipDDXType == BC_CFG_INSERT))
	{
		CFGDATA *prpCfg;
		struct BcStruct *prlCfgData;

		prlCfgData = (struct BcStruct *) vpDataPointer;
		long llUrno = GetUrnoFromSelection(prlCfgData->Selection);

		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpCfg) == TRUE)
		{
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			ogDdx.DataChanged((void *)this,CFG_CHANGE,(void *)prpCfg);
		}
		else
		{
			prpCfg = new CFGDATA;
			GetRecordFromItemList(prpCfg,
				prlCfgData->Fields,prlCfgData->Data);
			PrepareCfgData(prpCfg);
			omData.Add(prpCfg);
			omCkeyMap.SetAt(prpCfg->Ckey,prpCfg);
			omUrnoMap.SetAt((void *)prpCfg->Urno,prpCfg);
			ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prpCfg);
		}
	}
}

long  CedaCfgData::GetUrnoById(char *pclWiid)
{
	int ilWhatifCount = omData.GetSize();
	for ( int i = 0; i < ilWhatifCount; i++)
	{
		if (strcmp(omData[i].Ckey,pclWiid) == 0)
		{
			return omData[i].Urno;
		}
	}
	return 0L;
}

BOOL  CedaCfgData::GetIdByUrno(long lpUrno,char *pcpWiid)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		strcpy(pcpWiid,prlCfg->Ckey);
		return TRUE;
	}
	return FALSE;
}


CFGDATA  *CedaCfgData::GetCfgByUrno(long lpUrno)
{
	CFGDATA  *prlCfg;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfg) == TRUE)
	{
		return prlCfg;
	}
	return NULL;
}

bool CedaCfgData::AddCfg(CFGDATA *prpCfg)
{
	CFGDATA *prlCfg = new CFGDATA;
	memcpy(prlCfg,prpCfg,sizeof(CFGDATA));
	prlCfg->IsChanged = DATA_NEW;

	omData.Add(prlCfg);
	omUrnoMap.SetAt((void *)prlCfg->Urno, prlCfg);
	omCkeyMap.SetAt(prlCfg->Ckey, prlCfg); 

	ogDdx.DataChanged((void *)this,CFG_INSERT,(void *)prlCfg);
	SaveCfg(prlCfg);
    return true;
}

bool CedaCfgData::ChangeCfgData(CFGDATA *prpCfg)
{
//	int ilLc;

	if (prpCfg->IsChanged == DATA_UNCHANGED)
	{
		prpCfg->IsChanged = DATA_CHANGED;
	}
	SaveCfg(prpCfg);

    return true;
}

bool CedaCfgData::DeleteCfg(long lpUrno)
{

	CFGDATA *prpCfg = GetCfgByUrno(lpUrno);
	if (prpCfg != NULL)
	{
		prpCfg->IsChanged = DATA_DELETED;

		omUrnoMap.RemoveKey((void *)lpUrno);
		omCkeyMap.RemoveKey(prpCfg->Ckey);

		SaveCfg(prpCfg);
	}
    return true;
}

bool CedaCfgData::SaveCfg(CFGDATA *prpCfg)
{

	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[824];

	if ((prpCfg->IsChanged == DATA_UNCHANGED) || (! bgOnline))
	{
		return true; // no change, nothing to do
	}
	switch(prpCfg->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpCfg->IsChanged = DATA_UNCHANGED;
		if(strcmp(prpCfg->Pkno, ogBasicData.omUserID)==0)
		{
			MakeCurrentUser();
		}

		break;
	case DATA_CHANGED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		MakeCedaData(olListOfData,prpCfg);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		if (olRc != true)
		{
			prpCfg->IsChanged = DATA_NEW;
			SaveCfg(prpCfg);
		}
		else
		{
			prpCfg->IsChanged = DATA_UNCHANGED;
			if(strcmp(prpCfg->Pkno, ogBasicData.omUserID)==0)
			{
				MakeCurrentUser();
			}
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpCfg->Urno);
		olRc = CedaAction("DRT",pclSelection);
		MakeCurrentUser();
		break;
	}

	
    return true;

}

BOOL CedaCfgData::InterpretSetupString(CString popSetupString,USERSETUPDATA *prpSetupData)
{
	char *psp = NULL;
	char pclOriginal[1024]="";
	char pclStr[1024]="";
	char pclResult[256]="";

	char pclDET1[300]="";
	char pclDET2[300]="";
	char pclNAME[300]="";
	char pclEVSO[300]="";	
	char pclNDFC[300]="";	

	strcpy(pclOriginal, popSetupString);

	// First we have to devide the incomming string into valid Commands

	// the parts of incomming string
	

	psp = strstr(pclOriginal, ";");
	while(psp != NULL)
	{
		char *pclTmp;
		char pclPart[2048];
		char pclRest[2048];
		strcpy(pclRest, psp+1);
		*psp = '\0';
		strcpy(pclPart, pclOriginal);
		strcpy(pclOriginal, pclRest);
		if((pclTmp = strstr(pclPart, "DET1=")) != NULL)
			strcpy(pclDET1, pclPart);
		else if((pclTmp = strstr(pclPart, "DET2=")) != NULL)
			strcpy(pclDET2, pclPart);
		else if((pclTmp = strstr(pclPart, "NAME=")) != NULL)
			strcpy(pclNAME, pclPart);
		else if((pclTmp = strstr(pclPart, "EVSO=")) != NULL)
			strcpy(pclEVSO, pclPart);
		else if((pclTmp = strstr(pclPart, "NDFC=")) != NULL)
			strcpy(pclNDFC, pclPart);

		psp = strstr(pclOriginal, ";");

	}


	
	psp = strstr(pclDET1, "=");
	if(psp == NULL)
	{
			prpSetupData->DET1 = CString("300");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if (strlen(pclOriginal) == 0)
		{
			prpSetupData->DET1 = CString("300");
		}
		else
		{
			prpSetupData->DET1 =  CString(pclOriginal);
		}
	}
	psp = NULL;

	
	psp = strstr(pclDET2, "=");
	if(psp == NULL)
	{
			prpSetupData->DET2 = CString("60");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->DET2 = CString("60");
		}
		else
		{
			prpSetupData->DET2 =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclNAME, "=");
	if(psp == NULL)
	{
			prpSetupData->NAME = CString("<LANM><+>40<+>,<+><FINM><+>40<+>");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if(strlen(pclOriginal) == 0)
		{
			prpSetupData->NAME = CString("<LANM><+>40<+>,<+><FINM><+>40<+>");
		}
		else
		{
			CString olOriginal(pclOriginal);
			if (olOriginal.Find('+') == -1)
			{
				prpSetupData->NAME =  CString(pclOriginal);
			}
			else if (olOriginal.Find("<+>") == -1)	// old format
			{
				olOriginal.Replace("+","<+>");
				olOriginal.Replace("LANM","<LANM>");
				olOriginal.Replace("FINM","<FINM>");
				prpSetupData->NAME =  CString(olOriginal);

			}
			else	// new format !
			{
				prpSetupData->NAME =  CString(pclOriginal);
			}
		}
	}
	psp = NULL;

	psp = strstr(pclEVSO, "=");
	if(psp == NULL)
	{
			prpSetupData->EVSO = CString("0");
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if (strlen(pclOriginal) == 0)
		{
			prpSetupData->EVSO = CString("0");
		}
		else
		{
			prpSetupData->EVSO =  CString(pclOriginal);
		}
	}
	psp = NULL;

	psp = strstr(pclNDFC, "=");
	if(psp == NULL)
	{
			prpSetupData->NDFC.Format("%ld",GRAY);
	}
	else
	{
		psp = psp + 1;
		strcpy(pclOriginal, psp);
		if (strlen(pclOriginal) == 0)
		{
			prpSetupData->NDFC.Format("%ld",GRAY);
		}
		else
		{
			prpSetupData->NDFC = CString(pclOriginal);
		}
	}
	psp = NULL;

	return TRUE;
}

void CedaCfgData::SetCfgData(void)
{
	//ogCommHandler.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
	//ogCommHandler.bmTurnaroundBars = ogCfgData.rmUserSetup.TACK[0] == 'J';
	//ogCommHandler.bmDemandsOnly = ogCfgData.rmUserSetup.FBCK[0] == 'N';
	//ogCommHandler.bmShowShadowBars = ogCfgData.rmUserSetup.SBCK[0] == 'J';
	
/*	CViewer rlViewer;
	rlViewer.SetViewerKey("GateDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GACV);

	rlViewer.SetViewerKey("StaffDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STCV);

	rlViewer.SetViewerKey("CciDia");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCCV);
	rlViewer.SetViewerKey("StaffTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.STLV);
	rlViewer.SetViewerKey("FltSched");
	rlViewer.SelectView(ogCfgData.rmUserSetup.FPLV);
	rlViewer.SetViewerKey("GateTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.GBLV);
	rlViewer.SetViewerKey("CciTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CCBV);
	rlViewer.SetViewerKey("ReqTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.RQSV);
	rlViewer.SetViewerKey("ConfTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.CONV);
	rlViewer.SetViewerKey("PPTab");
	rlViewer.SelectView(ogCfgData.rmUserSetup.VPLV);
*/
}

void CedaCfgData::GetDefaultColorSetup(CStringArray &opLines)
{
	opLines.RemoveAll();
	CString olColour; 
	CString olText;

	// line 0	= Hellblau
	olText.Format(LoadStg(IDS_STRING1838),1);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 128, 255, 255));
	opLines.Add(CString("0;" + olColour));

	// line 1	= Orange
	olText.Format(LoadStg(IDS_STRING1838),2);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 255,128, 128));
	opLines.Add(CString("1;" + olColour));

	// line 2	= Gelb
	olText.Format(LoadStg(IDS_STRING1838),3);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 255, 255,   0));
	opLines.Add(CString("2;" + olColour + ";" + olColour));

	// line 3	= Braun
	olText.Format(LoadStg(IDS_STRING1838),4);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 128,  64,   0));
	opLines.Add(CString("3;" + olColour));

	// line 4	= Türkis
	olText.Format(LoadStg(IDS_STRING1838),5);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 255,   0, 128));
	opLines.Add(CString("4;" + olColour));

	// line 5	= Schwarz
	olText.Format(LoadStg(IDS_STRING1838),6);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB(   0,   0,   0));
	opLines.Add(CString("5;" + olColour));

	// line 6	= Orange
	olText.Format(LoadStg(IDS_STRING1838),7);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 255,128, 128));
	opLines.Add(CString("6;" + olColour));

	// line 7	= Gelb
	olText.Format(LoadStg(IDS_STRING1838),8);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 255, 255,   0));
	opLines.Add(CString("7;" + olColour));

	// line 8	= Braun
	olText.Format(LoadStg(IDS_STRING1838),9);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 128,  64,   0));
	opLines.Add(CString("8;" + olColour));

	// line 9	= Türkis
	olText.Format(LoadStg(IDS_STRING1838),10);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 255,   0, 128));
	opLines.Add(CString("9;" + olColour));

	// line 10	= Orange
	olText.Format(LoadStg(IDS_STRING1838),11);
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 255,128, 128));
	opLines.Add(CString("10;" + olColour));

	// -*- coverage	= green
	olText.Format(LoadStg(IDS_STRING1839));
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB( 0, 128,   0));
	opLines.Add(CString("11;" + olColour));

	// single coverage	= light green
	olText.Format(LoadStg(IDS_STRING1840));
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB(  0, 255,   0));
	opLines.Add(CString("12;" + olColour));

	// simulation visual = white
	olText.Format(LoadStg(IDS_STRING1841));
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB(255, 255, 255));
	opLines.Add(CString("13;" + olColour));

	// simulation calc   = red
	olText.Format(LoadStg(IDS_STRING1842));
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB(255, 0  ,   0));
	opLines.Add(CString("14;" + olColour));

	// min/max lines	 = yellow
	olText.Format(LoadStg(IDS_STRING1843));
	olColour.Format("%d;%s;%ld;",PS_DASHDOT,olText,RGB(255, 255,   0));
	opLines.Add(CString("15;" + olColour));

	// -*- demand	= blue
	olText.Format(LoadStg(IDS_STRING1849));
	olColour.Format("%d;%s;%ld;",PS_SOLID,olText,RGB(  0,   0, 255));
	opLines.Add(CString("16;" + olColour));

}

void CedaCfgData::CfgRecordFromColor(const CString& popUsername,const CString& popRecord,CFGDATA& popCfgData)
{
	int ilMaxFields = 4;
	int ilMaxBytes  = popRecord.GetLength();

	int ilIndex;
	long llType;
	CString olName;
	COLORREF olNCol;

	for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < ilMaxFields; ilFieldCount++)
   	{
        // Extract next field in the specified text-line
		for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && popRecord[ilByteCount] != ';'; ilByteCount++)
			;
		if(ilByteCount <= ilMaxBytes)
		{
			CString olField = popRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
			ilByteCount++;
			switch(ilFieldCount)
			{
			case 0 : ilIndex = atol((LPCSTR)olField);
				break;
			case 1 : llType  = atol((LPCSTR)olField);
				break;
			case 2 : olName  = olField;
				break;
			case 3 : olNCol = (COLORREF) atol((LPCSTR)olField);
				break;
			}
		}
	}

	popCfgData.Urno = ogBasicData.GetNextUrno();
	sprintf(popCfgData.Ctyp, "COLOR%02d", ilIndex);
	strncpy(popCfgData.Pkno,(LPCSTR)popUsername,sizeof(popCfgData.Pkno));
	popCfgData.Pkno[sizeof(popCfgData.Pkno)-1] = '\0';
	strncpy(popCfgData.Text,(LPCSTR)popRecord,sizeof(popCfgData.Text));
	popCfgData.Text[sizeof(popCfgData.Text)-1] = '\0';

}

// read color config data
bool CedaCfgData::ReadColors(const CString& opUserName,CStringArray &opLines)
{
	char pclWhere[1024]="";
	int ilLc = 0;

	opLines.RemoveAll();
	bool	ilRc = true;
	bool	olRc = true;

    sprintf(pclWhere, "WHERE APPN='CCS_%s' AND CTYP LIKE 'COLOR%%' AND PKNO = '%s' ORDER BY CTYP", ogAppName,opUserName);
    olRc = CedaAction("RT", pclWhere);

	ilLc = 0;
    while(olRc == true && ilRc == true)
    {
		CFGDATA rlCfg;
		CString olTxt;
		if ((ilRc = GetBufferRecord(ilLc,&rlCfg)) == true)
		{
			if(ilLc < NUMCONFCOLORS)
			{
				olTxt = CString(rlCfg.Text);
				opLines.Add(olTxt);
			}
			else
			{
				olTxt.Format("Color <%d> ignored because exceeds NUMCONFCOLORS <%d>\n%s",ilLc,NUMCONFCOLORS,rlCfg.Text);
			}
			TRACE("%s\n",olTxt);
			ilLc++;
		}
	}

	if(ilLc > NUMCONFCOLORS || olRc == false || ResetToDefault())
	{
		return false;
	}
    return true;
}

bool CedaCfgData::ResetToDefault(void)
{

	bool blResetToDefault = false;
	char pclTmpText[512];
	char pclConfigPath[512];
	char pclKeyword[521];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	strcpy(pclKeyword,"RESET_COLORS_TO_DEFAULT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!stricmp(pclTmpText,"YES"))
	{
		blResetToDefault = true;
	}
	return blResetToDefault;
}

bool CedaCfgData::SaveColorData(const CString& opUserName,CFGDATA *prpCfg)
{
	CString olListOfData;
	bool olRc = true;
	char pclData[824];
	char pclSelection[1024]="";
	//First we delete the entry
	sprintf(pclSelection,"WHERE APPN='CCS_%s' AND (PKNO = '%s' AND CTYP = '%s')", ogAppName, ogBasicData.omUserID, prpCfg->Ctyp);
	olRc = CedaAction("DRT",pclSelection);

	//and create it anew
	MakeCedaData(olListOfData,prpCfg);
	strcpy(pclData,olListOfData);
	olRc = CedaAction("IRT","","",pclData);

	return olRc;
}
