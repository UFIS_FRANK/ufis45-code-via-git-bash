// CedaWgpData.cpp
 
#include <stdafx.h>
#include <CedaWgpData.h>
#include <resource.h>


void ProcessWgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaWgpData::CedaWgpData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for WGPDataStruct
	BEGIN_CEDARECINFO(WGPDATA,WGPDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Wgpc,"WGPC")
		FIELD_CHAR_TRIM	(Wgpn,"WGPN")
		FIELD_LONG		(Pgpu,"PGPU")

	END_CEDARECINFO //(WGPDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(WGPDataRecInfo)/sizeof(WGPDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WGPDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"WGP");
	strcpy(pcmListOfFields,"CDAT,LSTU,PRFL,REMA,URNO,USEC,USEU,WGPC,WGPN,PGPU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaWgpData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("WGPC");
	ropFields.Add("WGPN");
	ropFields.Add("PGPU");

	ropDesription.Add(LoadStg(IDS_STRING61407));
	ropDesription.Add(LoadStg(IDS_STRING61408));
	ropDesription.Add(LoadStg(IDS_STRING61468));
	ropDesription.Add(LoadStg(IDS_STRING61469));
	ropDesription.Add(LoadStg(IDS_STRING61470));
	ropDesription.Add(LoadStg(IDS_STRING61471));
	ropDesription.Add(LoadStg(IDS_STRING61479));
	ropDesription.Add(LoadStg(IDS_STRING61503));
	ropDesription.Add(LoadStg(IDS_STRING61491));
	ropDesription.Add(LoadStg(IDS_STRING61545));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaWgpData::Register(void)
{
	ogDdx.Register((void *)this,BC_WGP_CHANGE,	CString("WGPDATA"), CString("Wgp-changed"),	ProcessWgpCf);
	ogDdx.Register((void *)this,BC_WGP_NEW,		CString("WGPDATA"), CString("Wgp-new"),		ProcessWgpCf);
	ogDdx.Register((void *)this,BC_WGP_DELETE,	CString("WGPDATA"), CString("Wgp-deleted"),	ProcessWgpCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaWgpData::~CedaWgpData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaWgpData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaWgpData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		WGPDATA *prlWgp = new WGPDATA;
		if ((ilRc = GetFirstBufferRecord(prlWgp)) == true)
		{
			omData.Add(prlWgp);//Update omData
			omUrnoMap.SetAt((void *)prlWgp->Urno,prlWgp);
		}
		else
		{
			delete prlWgp;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaWgpData::Insert(WGPDATA *prpWgp)
{
	prpWgp->IsChanged = DATA_NEW;
	if(Save(prpWgp) == false) return false; //Update Database
	InsertInternal(prpWgp);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaWgpData::InsertInternal(WGPDATA *prpWgp)
{
	ogDdx.DataChanged((void *)this, WGP_NEW,(void *)prpWgp ); //Update Viewer
	omData.Add(prpWgp);//Update omData
	omUrnoMap.SetAt((void *)prpWgp->Urno,prpWgp);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaWgpData::Delete(long lpUrno)
{
	WGPDATA *prlWgp = GetWgpByUrno(lpUrno);
	if (prlWgp != NULL)
	{
		prlWgp->IsChanged = DATA_DELETED;
		if(Save(prlWgp) == false) return false; //Update Database
		DeleteInternal(prlWgp);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaWgpData::DeleteInternal(WGPDATA *prpWgp)
{
	ogDdx.DataChanged((void *)this,WGP_DELETE,(void *)prpWgp); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpWgp->Urno);
	int ilWgpCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilWgpCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpWgp->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaWgpData::Update(WGPDATA *prpWgp)
{
	if (GetWgpByUrno(prpWgp->Urno) != NULL)
	{
		if (prpWgp->IsChanged == DATA_UNCHANGED)
		{
			prpWgp->IsChanged = DATA_CHANGED;
		}
		if(Save(prpWgp) == false) return false; //Update Database
		UpdateInternal(prpWgp);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaWgpData::UpdateInternal(WGPDATA *prpWgp)
{
	WGPDATA *prlWgp = GetWgpByUrno(prpWgp->Urno);
	if (prlWgp != NULL)
	{
		*prlWgp = *prpWgp; //Update omData
		ogDdx.DataChanged((void *)this,WGP_CHANGE,(void *)prlWgp); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

WGPDATA *CedaWgpData::GetWgpByUrno(long lpUrno)
{
	WGPDATA  *prlWgp;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlWgp) == TRUE)
	{
		return prlWgp;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaWgpData::ReadSpecialData(CCSPtrArray<WGPDATA> *popWgp,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","WGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","WGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popWgp != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			WGPDATA *prpWgp = new WGPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpWgp,CString(pclFieldList))) == true)
			{
				popWgp->Add(prpWgp);
			}
			else
			{
				delete prpWgp;
			}
		}
		if(popWgp->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaWgpData::Save(WGPDATA *prpWgp)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpWgp->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpWgp->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpWgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgp->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpWgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgp->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessWgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogWgpData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaWgpData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlWgpData;
	prlWgpData = (struct BcStruct *) vpDataPointer;
	WGPDATA *prlWgp;
	if(ipDDXType == BC_WGP_NEW)
	{
		prlWgp = new WGPDATA;
		GetRecordFromItemList(prlWgp,prlWgpData->Fields,prlWgpData->Data);
		if(ValidateWgpBcData(prlWgp->Urno)) //Prf: 8795
		{
			InsertInternal(prlWgp);
		}
		else
		{
			delete prlWgp;
		}
	}
	if(ipDDXType == BC_WGP_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlWgpData->Selection);
		prlWgp = GetWgpByUrno(llUrno);
		if(prlWgp != NULL)
		{
			GetRecordFromItemList(prlWgp,prlWgpData->Fields,prlWgpData->Data);
			if(ValidateWgpBcData(prlWgp->Urno)) //Prf: 8795
			{
				UpdateInternal(prlWgp);
			}
			else
			{
				DeleteInternal(prlWgp);
			}
		}
	}
	if(ipDDXType == BC_WGP_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlWgpData->Selection);

		prlWgp = GetWgpByUrno(llUrno);
		if (prlWgp != NULL)
		{
			DeleteInternal(prlWgp);
		}
	}
}

//Prf: 8795
//--ValidateWgpBcData--------------------------------------------------------------------------------------

bool CedaWgpData::ValidateWgpBcData(const long& lrpUrno)
{
	bool blValidateWgpBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<WGPDATA> olWgps;
		if(!ReadSpecialData(&olWgps,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateWgpBcData = false;
		}
	}
	return blValidateWgpBcData;
}

//---------------------------------------------------------------------------------------------------------
