// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__1316909A_35CE_11D3_94E7_00001C018ACE__INCLUDED_)
#define AFX_MAINFRM_H__1316909A_35CE_11D3_94E7_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ChildView.h>
#include <CovDiagram.h>

class CMainFrame : public CovDiagram
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 

	
	// for Multilanguage-ToolTop-Support added 
	bool OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult);

	void GetMessageString(UINT nID, CString& rMessage) const;

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CChildView    m_wndView;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnPaint();
	afx_msg void OnNextChart();
    afx_msg void OnPrevChart();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnDeactivateAutoCov(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnActivateAutoCov(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateGraphic(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateBaseShift(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateDemandGantt(WPARAM wParam, LPARAM lParam);
	afx_msg LONG UpdateDemandCombo(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnAnsicht();
	afx_msg void OnCombobox();
	afx_msg void OnSimulation();
	afx_msg void OnStatistic();
	afx_msg void OnKonflict();
	afx_msg void OnCreateDem();	
	afx_msg void OnAutoCov();	
	afx_msg void OnDrucken();
	afx_msg void OnBewertung();
	afx_msg void OnFlightNoRes();
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
	afx_msg void OnLoadtimeframe();
	afx_msg void OnMasstab();
	afx_msg void OnSearch();
	afx_msg void OnSearchExit();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnViewbutton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

    


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__1316909A_35CE_11D3_94E7_00001C018ACE__INCLUDED_)
