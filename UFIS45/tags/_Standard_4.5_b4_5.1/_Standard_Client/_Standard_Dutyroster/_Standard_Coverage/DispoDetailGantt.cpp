// stgantt.cpp : implementation file
//  

#include <stdafx.h>
#include <Coverage.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSTimeScale.h>
//#include "cviewer.h"
#include <CCSDragDropCtrl.h>
#include <CCSBar.h>
#include <ccsbchandle.h>
#include <ccsddx.h>
#include <DispoDetailGantt.h>
#include <ccsddx.h>
#include <DataSet.h>
#include <CedaDemData.h>
#include <CedaPfcData.h>
#include <JobDlg.h>
//#include "StaffDiagram.h"
#include <PrivList.h>

//#include "stgantt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

#define	FIX_TIMESCALE_ROUNDING_ERROR
#ifdef	FIX_TIMESCALE_ROUNDING_ERROR
#define GetX(time)	(pomTimeScale->GetXFromTime(time) + imVerticalScaleWidth)
#define GetCTime(x)	(pomTimeScale->GetTimeFromX((x) - imVerticalScaleWidth))
#else
////////////////////////////////////////////////////////////////////////

// Macro definition for get X-coordinate from the given time
#define GetX(time)  (omDisplayStart == omDisplayEnd? -1: \
            (imVerticalScaleWidth + \
            (((time) - omDisplayStart).GetTotalSeconds() * \
            (imWindowWidth - imVerticalScaleWidth) / \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds())))

// Macro definition for get time from the given X-coordinate
#define GetCTime(x)  ((imWindowWidth - imVerticalScaleWidth) == 0? TIMENULL: \
            (omDisplayStart + \
            (time_t)(((x) - imVerticalScaleWidth) * \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds() / \
            (imWindowWidth - imVerticalScaleWidth))))

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
#endif
////////////////////////////////////////////////////////////////////////

extern bool bgDoTimer;
/////////////////////////////////////////////////////////////////////////////
// DispoDetailGantt

DispoDetailGantt::DispoDetailGantt(DispoDetailGanttViewer *popViewer, 
    int ipVerticalScaleWidth, int ipVerticalScaleIndent,
    CFont *popVerticalScaleFont, CFont *popGanttChartFont,
    int ipGutterHeight, int ipOverlapHeight,
    COLORREF lpVerticalScaleTextColor, COLORREF lpVerticalScaleBackgroundColor,
    COLORREF lpHighlightVerticalScaleTextColor, COLORREF lpHighlightVerticalScaleBackgroundColor,
    COLORREF lpGanttChartTextColor, COLORREF lpGanttChartBackgroundColor,
    COLORREF lpHighlightGanttChartTextColor, COLORREF lpHighlightGanttChartBackgroundColor)
{
    SetViewer(popViewer);
    SetVerticalScaleWidth(ipVerticalScaleWidth);
    SetVerticalScaleIndent(ipVerticalScaleIndent);
//    SetFonts(popVerticalScaleFont, popGanttChartFont);
//MWO    SetFonts(igFontIndex1, igFontIndex2);
    SetFonts(MS_SANS6, MS_SANS6);

    SetGutters(ipGutterHeight, ipOverlapHeight);
    SetVerticalScaleColors(lpVerticalScaleTextColor, lpVerticalScaleBackgroundColor,
        lpHighlightVerticalScaleTextColor, lpHighlightVerticalScaleBackgroundColor);
    SetGanttChartColors(lpGanttChartTextColor, lpGanttChartBackgroundColor,
        lpHighlightGanttChartTextColor, lpHighlightGanttChartBackgroundColor);

    // Initialize default values
    omDisplayStart = TIMENULL;
    omDisplayEnd = TIMENULL;
    imWindowWidth = 0;          // unessential -- WM_SIZE will initialize this
    bmIsFixedScaling = TRUE;    // unessential -- will be set in method SetDisplayWindow()
    omCurrentTime = TIMENULL;
    omMarkTimeStart = TIMENULL;
    omMarkTimeEnd = TIMENULL;
    pomStatusBar = NULL;
	pomTimeScale = NULL;
    bmIsMouseInWindow = FALSE;
    bmIsControlKeyDown = FALSE;
    imHighlightLine = -1;
    imCurrentBar = -1;
	bmActiveBarSet = FALSE;
	bmContextBarSet = FALSE;
	bmActiveLineSet = FALSE;
	pomDialog = NULL;

	lmCurrentDemUrno = -1;
    // Required only if you allow moving/resizing
    SetBorderPrecision(15);
    umResizeMode = HTNOWHERE;
}

void DispoDetailGantt::SetViewer(DispoDetailGanttViewer *popViewer)
{
    pomViewer = popViewer;
}

void DispoDetailGantt::SetVerticalScaleWidth(int ipWidth)
{
    imVerticalScaleWidth = ipWidth;
}

void DispoDetailGantt::SetVerticalScaleIndent(int ipIndent)
{
    imVerticalScaleIndent = ipIndent;
}

//void DispoDetailGantt::SetFonts(CFont *popVerticalScaleFont, CFont *popGanttChartFont)
void DispoDetailGantt::SetFonts(int index1, int index2)
{
	//LOGFONT rlLogFont;
    pomVerticalScaleFont = &ogMSSansSerif_Bold_8;//&ogSmallFonts_Regular_7;
    pomGanttChartFont = &ogSmallFonts_Regular_7;//&ogScalingFonts[2/*index2*/];//&ogSmallFonts_Regular_4;//popGanttChartFont;
	//pomGanttChartFont->GetLogFont(&ogSmallFonts_Regular_7);LogFont*/
	
    // Calculate the normal height of a bar
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    if (pomGanttChartFont)
        dc.SelectObject(pomGanttChartFont);

	
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm); 
	//imBarHeight = 8;
    imBarHeight = tm.tmHeight + tm.tmExternalLeading + 2;
    //imBarHeight = -rlLogFont.lfHeight - 5;// + 4;
    imLeadingHeight = (tm.tmExternalLeading + 2) / 2;
    dc.DeleteDC();
}

void DispoDetailGantt::SetGutters(int ipGutterHeight, int ipOverlapHeight)
{
    imGutterHeight = ipGutterHeight;
    imOverlapHeight = ipOverlapHeight;
}

void DispoDetailGantt::SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmVerticalScaleTextColor = lpTextColor;
    lmVerticalScaleBackgroundColor = lpBackgroundColor;
    lmHighlightVerticalScaleTextColor = lpHighlightTextColor;
    lmHighlightVerticalScaleBackgroundColor = lpHighlightBackgroundColor;
}

void DispoDetailGantt::SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmGanttChartTextColor = lpTextColor;
    lmGanttChartBackgroundColor = lpBackgroundColor;
    lmHighlightGanttChartTextColor = lpHighlightTextColor;
    lmHighlightGanttChartBackgroundColor = lpHighlightBackgroundColor;
}

// Attentions:
// Return the height that it's expected for being displayed
// Be careful, this one may be called before the CListBox was created.
//
int DispoDetailGantt::GetGanttChartHeight()
{
    int ilHeight = 0;
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(); ilLineno++)
        ilHeight += GetLineHeight(ilLineno);

    return ilHeight;
}

int DispoDetailGantt::GetLineHeight(int ilLineno)
{
    int ilMaxOverlapLevel = pomViewer->GetLine(ilLineno)->MaxOverlapLevel;
    return /*(2 * imGutterHeight) + */imBarHeight + (max(1, ilMaxOverlapLevel) * imOverlapHeight);
}

BOOL DispoDetailGantt::Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
    // Make sure that the viewer is already given
    if (pomViewer == NULL)
        return FALSE;

    // Make sure that all essential style is defined
    // WS_CLIPSIBLINGS is very important for the ListBox so it does not paint outside its window
    dwStyle |= WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS;
    dwStyle |= LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE;
    dwStyle |= LBS_MULTIPLESEL;

    // Create the window
    if (CListBox::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
        return FALSE;

    // Create items according to what's in the Viewer
    ResetContent();
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(); ilLineno++)
    {
        AddString("");
        SetItemHeight(ilLineno, GetLineHeight(ilLineno));
    }
	//if(prmPreviousBar!=NULL)
	//	delete prmPreviousBar;
	prmPreviousBar = new DEM_BARDATA;
    return TRUE;
}

void DispoDetailGantt::SetStatusBar(CStatic *popStatusBar)
{
    pomStatusBar = popStatusBar;
}

void DispoDetailGantt::SetTimeScale(CCSTimeScale *popTimeScale)
{
    pomTimeScale = popTimeScale;
}

void DispoDetailGantt::SetBorderPrecision(int ipBorderPrecision)
{
    imBorderPreLeft = ipBorderPrecision / 2;
    imBorderPreRight = (ipBorderPrecision+1) / 2;
}

void DispoDetailGantt::SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling)
{
    omDisplayStart = opDisplayStart;
    omDisplayEnd = opDisplayEnd;
    bmIsFixedScaling = bpFixedScaling;

    if (m_hWnd == NULL)
        imWindowWidth = 0;  // window is still not opened
    else
    {
        CRect rect;
        GetWindowRect(&rect);   // can't use client rect (vertical scroll may less its width)
        imWindowWidth = rect.Width();   // however, this is correct only if window has no border
        RepaintGanttChart();    // redraw the entire screen
    }
}

void DispoDetailGantt::SetDisplayStart(CTime opDisplayStart)
{
    if (bmIsFixedScaling)   // must shift both start/end time while keeping the old time span
        omDisplayEnd = opDisplayStart + (omDisplayEnd - omDisplayStart).GetTotalSeconds();
    omDisplayStart = opDisplayStart;
    RepaintGanttChart();    // redraw the entire screen
}

void DispoDetailGantt::SetCurrentTime(CTime opCurrentTime)
{                               
    RepaintGanttChart(-1, omCurrentTime, opCurrentTime);
    omCurrentTime = opCurrentTime;
}

void DispoDetailGantt::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
    RepaintGanttChart(-1, omMarkTimeStart, opMarkTimeStart);
    omMarkTimeStart = opMarkTimeStart;
    RepaintGanttChart(-1, omMarkTimeEnd, opMarkTimeEnd);
    omMarkTimeEnd = opMarkTimeEnd;
}

void DispoDetailGantt::DrawDotHorizontalLine(CDC *pDC, int itemID, const CRect &rcItem)
{
    CBitmap Bitmap;
	CTime olTime, olTmpTime;
	int ilX;

	CRect olRect(rcItem);
	CPen SepPen(PS_DOT, 1, BLACK);
	COLORREF olOldBkColor = pDC->SetBkColor(SILVER);
	CPen *pOldPen = pDC->SelectObject(&SepPen);
	int ilHeight = 0;
	CRect olTmpRect = olRect;
//	for (int i = GetTopIndex(); i < 11; i++)
	{
		olRect.bottom = olRect.top + GetLineHeight(0);
        pDC->MoveTo(olRect.left, olRect.bottom - 1);
        pDC->LineTo(olRect.right, olRect.bottom - 1);
		olRect.top += GetLineHeight(0);
	}

	olRect = olTmpRect;
	CPen BlackPen(PS_DOT, 1, GRAY);
	pDC->SetBkColor(GRAY);
	pDC->SelectObject(&BlackPen);
	olTime = pomTimeScale->GetDisplayStartTime();
	olTime -= CTimeSpan(0, 0, olTime.GetMinute(), 0);
	for(int ili=0; olTime <= omDisplayEnd; ili++)
	{
		olTime += CTimeSpan(0,1,0,0);
		ilX = pomTimeScale->GetXFromTime(olTime)+imVerticalScaleIndent;
		pDC->MoveTo(ilX, olRect.top);
		pDC->LineTo(ilX, olRect.bottom);
	}
	pDC->SelectObject(pOldPen);	
	pDC->SetBkColor(olOldBkColor);
}
void DispoDetailGantt::RepaintVerticalScale(int ipLineno, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (GetItemRect(ipLineno, &rcPaint) != LB_ERR)  // valid item in list box?
    {
        rcPaint.right = imVerticalScaleWidth - 1;
        InvalidateRect(&rcPaint, bpErase);
    }
}

void DispoDetailGantt::RepaintGanttChart(int ipLineno, CTime opStartTime, CTime opEndTime, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
        GetClientRect(&rcPaint);
    else
        GetItemRect(ipLineno, &rcPaint);

    // Update the GanttChart body
    if (opStartTime <= omDisplayStart)
        rcPaint.left = imVerticalScaleWidth;
    else
        rcPaint.left = (int)GetX(opStartTime);

    // Use the client width if user want to repaint to the end of time
	CString x = omDisplayEnd.Format("%d.%m.%Y-%H:%M");
	CString y = opEndTime.Format("%d.%m.%Y-%H:%M");
    if (opEndTime != TIMENULL && opEndTime <= omDisplayEnd)
        rcPaint.right = (int)GetX(opEndTime) + 1;

    InvalidateRect(&rcPaint, bpErase);
}

void DispoDetailGantt::RepaintItemHeight(int ipLineno)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    // This repaint method will recalculate the item height
    SetItemHeight(ipLineno, GetLineHeight(ipLineno));

    CRect rcItem, rcPaint;
    GetClientRect(&rcPaint);
    GetItemRect(ipLineno, &rcItem);
    rcPaint.top = rcItem.top;
    InvalidateRect(&rcPaint, TRUE);
}


/////////////////////////////////////////////////////////////////////////////
// DispoDetailGantt implementation

void DispoDetailGantt::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    // We have to define MeasureItem() here since MFC places an ASSERT inside
    // the default MeasureItem() of an owner-drawn CListBox.
}

void DispoDetailGantt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// To let the GanttChart know automatically those changes in TimeScale.
// We had to reset the value of omDisplayStart and omDisplayEnd everytime.
	omDisplayStart = GetCTime(imVerticalScaleWidth);
	omDisplayEnd = GetCTime(imWindowWidth);
////////////////////////////////////////////////////////////////////////
    
	int itemID = lpDrawItemStruct->itemID;

    // Attention:
    // This optimization will the speed of displaying GanttChart very a lot.
    // However, this works because we hadn't display any selected or focus items.
    // (We use the "imCurrentItem" variable instead.)
    // So, be careful since this assumption may be changed in the future.
    //
    if (itemID == -1)
        return;
    if (!(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
        return; // nothing more to do if listbox just change its selection and/or focus item

    // Drawing routines start here
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    CRect rcItem(&lpDrawItemStruct->rcItem);
    CRect rcClip;
    dc.GetClipBox(&rcClip);

    // Draw vertical scale if necessary
    if (rcClip.left < imVerticalScaleWidth)
        DrawVerticalScale(&dc, itemID, rcItem);

	DrawDotHorizontalLine(&dc, itemID, rcItem);
    // Draw time lines and bars if necessary
    if (imVerticalScaleWidth <= rcClip.right)
    {
		//MWO
		//rcItem.bottom = rcItem.top - imBarHeight;
		// Ende MWO
        CRgn rgn;
        CRect rect(imVerticalScaleWidth, rcItem.top, rcItem.right, rcItem.bottom);
        rgn.CreateRectRgnIndirect(&rect);
        dc.SelectClipRgn(&rgn);
		DrawBackgroundBars(&dc, itemID, rcItem, rcClip);
        DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
        DrawGanttChart(&dc, itemID, rcItem, rcClip);
        if (umResizeMode != HTNOWHERE)
            DrawFocusRect(&dc, &omRectResize);
        dc.SelectClipRgn(NULL);
    }

    dc.Detach();
}


/////////////////////////////////////////////////////////////////////////////
// DispoDetailGantt implementation helper functions

void DispoDetailGantt::DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem)
{
	CPoint point;
	int	ilx;
    ::GetCursorPos(&point);
    ScreenToClient(&point);


    // Check if we need to change the highlight text in VerticalScale
    BOOL blHighlight;
    if (!bmIsMouseInWindow || !rcItem.PtInRect(point))
        blHighlight = FALSE;
    else
    {
        blHighlight = TRUE;
        if (imHighlightLine != itemID)
            RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = itemID;
    }

    // Drawing routines start here
	COLORREF rlTextColor, rlBkColor;
    CString olVertScaleText;
	olVertScaleText = pomViewer->GetLineText(itemID, &rlTextColor, &rlBkColor);
    CFont *pOldFont = pDC->SelectObject(pomVerticalScaleFont);
    COLORREF nOldTextColor = pDC->SetTextColor(blHighlight?
        lmHighlightVerticalScaleTextColor: rlTextColor);
    COLORREF nOldBackgroundColor = pDC->SetBkColor(/*blHighlight?
        lmHighlightVerticalScaleBackgroundColor: */rlBkColor);
    CRect rect(rcItem);
	CRect textRect = rect;

    int left = rect.left + imVerticalScaleIndent;
	rect.right = imVerticalScaleWidth - 2;
    
	ilx = 2;
	textRect.right = (int)(imVerticalScaleWidth-2);
	textRect.left = ilx;
	textRect.top += 2;
	textRect.bottom -= 2;//1;
	pDC->ExtTextOut(ilx, textRect.top, /*ETO_CLIPPED |*/ ETO_OPAQUE, &textRect, olVertScaleText, lstrlen(olVertScaleText), NULL);

	CRect olDotRect = textRect;
	olDotRect.top -= 2;
	olDotRect.bottom += 2;
	CPen BlackPen(PS_SOLID/*PS_DOT*/, 1, BLACK);
	CPen *pOldPen = pDC->SelectObject(&BlackPen);
	pDC->MoveTo(olDotRect.left, olDotRect.bottom-1);
	pDC->LineTo(olDotRect.right-1, olDotRect.bottom-1);

	pDC->SetTextColor(nOldTextColor);
	pDC->SetBkColor(nOldBackgroundColor);
	pDC->SelectObject(pOldPen);
}


void DispoDetailGantt::DrawBackgroundBars(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;
	CBrush olBlackBrush(COLORREF(BLACK));

    // Draw each bar if it in the range of the clipped box
    for (int i = 0; i < pomViewer->GetBkBarCount(itemID); i++)
    {
        DEM_BKBARDATA *prlBkBar = pomViewer->GetBkBar(itemID, i);

        if (!IsOverlapped(prlBkBar->StartTime, prlBkBar->EndTime, omDisplayStart, omDisplayEnd))
            continue;   // skip bar which is out of time range

        int left = (int)GetX(prlBkBar->StartTime);
        int right = (int)GetX(prlBkBar->EndTime);
        int top = rcItem.top;
        int bottom = rcItem.bottom;
		CCSPtrArray<CCSBarDecoStruct> olDecoData;
		CCSBarDecoStruct rlBarDeco;

        if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
            continue;   // skip bar which is out of clipping region

		rlBarDeco.Rect = CRect(left, top, right, bottom);
		rlBarDeco.Type = BD_CCSBAR;
		rlBarDeco.Text = prlBkBar->Text;
		rlBarDeco.HorizontalAlignment = BD_CENTER;
		rlBarDeco.VerticalAlignment = BD_CENTER;
		rlBarDeco.MarkerBrush = prlBkBar->MarkerBrush;
		rlBarDeco.MarkerType = MARKFULL;
		rlBarDeco.TextFont = pomGanttChartFont;
		olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
//Trennlinie
		rlBarDeco.Rect = CRect(left, bottom-1, right, bottom);
		rlBarDeco.Type = BD_LINE;
		rlBarDeco.Text = "";
		rlBarDeco.HorizontalAlignment = BD_CENTER;
		rlBarDeco.VerticalAlignment = BD_CENTER;
		rlBarDeco.MarkerBrush = &olBlackBrush;
		rlBarDeco.MarkerType = MARKFULL;
		CPen SepPen(PS_DOT, 1, COLORREF(BLACK));
		POINT rlFrom, rlTo;
		rlFrom.x = left; rlFrom.y = bottom-1;
		rlTo.x = right; rlTo.y = bottom-1;
		rlBarDeco.PointFrom = rlFrom;
		rlBarDeco.PointTo = rlTo;
		rlBarDeco.pPen = &SepPen;
		rlBarDeco.TextFont = pomGanttChartFont;
		olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
//Stundenstriche GetX(time)
		CTime olTime = prlBkBar->StartTime;
		CString olTimeS = prlBkBar->StartTime.Format("%d.%m.%Y - %H:%M");
		//CTime olTime = pomTimeScale->GetDisplayStartTime();
		olTime -= CTimeSpan(0, 0, olTime.GetMinute(), 0);
		for(int ili=0; olTime < prlBkBar->EndTime; ili++)
		{
			olTime += CTimeSpan(0,1,0,0);
			olTimeS = olTime.Format("%d.%m.%Y - %H:%M");
			int ilX = (int)GetX(olTime);//pomTimeScale->GetXFromTime(olTime);
			//pDC->PatBlt(ilX, ilHeight*5, olRect.bottom - olRect.top + 1, 1, PATCOPY);
			//pDC->MoveTo(ilX, olRect.top);
			//pDC->LineTo(ilX, olRect.bottom);
			left = ilX;
			right= ilX;
			rlBarDeco.Rect = CRect(ilX, top, ilX, bottom);
			rlBarDeco.Type = BD_LINE;
			rlBarDeco.Text = "";
			rlBarDeco.HorizontalAlignment = BD_CENTER;
			rlBarDeco.VerticalAlignment = BD_CENTER;
			rlBarDeco.MarkerBrush = &olBlackBrush;
			rlBarDeco.MarkerType = MARKFULL;
			CPen SepPen2(PS_SOLID, 1, COLORREF(GRAY));
			POINT rlFrom, rlTo;
			rlFrom.x = left; rlFrom.y = top-1;
			rlTo.x = right; rlTo.y = bottom-1;
			rlBarDeco.PointFrom = rlFrom;
			rlBarDeco.PointTo = rlTo;
			rlBarDeco.pPen = &SepPen2;
			rlBarDeco.TextFont = pomGanttChartFont;
			olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
		}
///----------- Und Malen
		CCSGanttBar olPaintBar(pDC,olDecoData);
		olDecoData.DeleteAll();

    }
}

void DispoDetailGantt::DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;

	CBrush olBlackBrush(COLORREF(BLACK));
	//CCSPtrArray<CCSBarDecoStruct> olDecoData;  //For additional decoration
	//olDecoData.RemoveAll();
    // Draw each bar if it in the range of the clipped box
	int ilBarCount = pomViewer->GetBarCount( itemID);
    for (int i = 0; i < pomViewer->GetBarCount( itemID); i++)
    {
        DEM_BARDATA *prlBar = pomViewer->GetBar(itemID, i);
		

        if (!IsOverlapped(prlBar->Dube, prlBar->Duen, omDisplayStart, omDisplayEnd))
            continue;   // skip bar which is out of time range

		DEMDATA *prlDem = pomViewer->GetDemByUrno(prlBar->Urno);
		if(prlDem != NULL)
		{
			CCSPtrArray<CCSBarDecoStruct> olDecoData;
			int left = (int)GetX(prlBar->Dube);
			int right = (int)GetX(prlBar->Duen);
			int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
			int bottom = top + imBarHeight;

			CCSBarDecoStruct rlBarDeco;
				rlBarDeco.Rect = CRect(left, top, right, bottom);
				if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
					continue;   // skip bar which is out of clipping region
				rlBarDeco.Type = BD_CCSBAR;
				rlBarDeco.Text = prlBar->Text;
				rlBarDeco.BkColor = prlBar->BkColor;
				rlBarDeco.TextColor = prlBar->TextColor;
				rlBarDeco.HorizontalAlignment = BD_CENTER;
				rlBarDeco.VerticalAlignment = BD_CENTER;
				rlBarDeco.MarkerBrush = prlBar->MarkerBrush;
				rlBarDeco.FrameType = prlBar->FrameType;
				rlBarDeco.FrameBrush = ogBrushs[BLACK_IDX];
				rlBarDeco.MarkerType = prlBar->MarkerType;
				rlBarDeco.TextFont = pomGanttChartFont;
				olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

				rlBarDeco.Rect = CRect(left, top, right, bottom);
/****
				CPoint olPoints[3];
				CRgn *polRgn;
				CRgn *polRgn2;
				olPoints[0].x = left;
				olPoints[0].y = top;

				olPoints[1].x = left; 
				olPoints[1].y = bottom;
				
				olPoints[2].x = left+(bottom-top);
				olPoints[2].y = top;
				rlBarDeco.Type = BD_REGION;
				rlBarDeco.Text = "";
				rlBarDeco.BkColor = prlBar->BkColor;
				rlBarDeco.TextColor = ogColors[ORANGE_IDX];
				rlBarDeco.HorizontalAlignment = BD_CENTER;
				rlBarDeco.VerticalAlignment = BD_CENTER;
				rlBarDeco.MarkerBrush = ogBrushs[ORANGE_IDX];
				rlBarDeco.FrameType = MARKNONE;
				rlBarDeco.FrameBrush = ogBrushs[BLACK_IDX];
				rlBarDeco.MarkerType = MARKNONE;
				rlBarDeco.TextFont = pomGanttChartFont;
				polRgn = new CRgn;
				polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
				rlBarDeco.Region = polRgn;
				olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
// 2, Dreieck
				olPoints[0].x = right-(bottom-top);
				olPoints[0].y = bottom;

				olPoints[1].x = right; 
				olPoints[1].y = bottom;
				
				olPoints[2].x = right;
				olPoints[2].y = top;
				rlBarDeco.Type = BD_REGION;
				rlBarDeco.Text = "";
				rlBarDeco.BkColor = prlBar->BkColor;
				rlBarDeco.TextColor = ogColors[ORANGE_IDX];
				rlBarDeco.HorizontalAlignment = BD_CENTER;
				rlBarDeco.VerticalAlignment = BD_CENTER;
				rlBarDeco.MarkerBrush = ogBrushs[ORANGE_IDX];
				rlBarDeco.FrameType = MARKNONE;
				rlBarDeco.FrameBrush = ogBrushs[BLACK_IDX];
				rlBarDeco.MarkerType = MARKNONE;
				rlBarDeco.TextFont = pomGanttChartFont;
				polRgn2 = new CRgn;
				polRgn2->CreatePolygonRgn( olPoints, 3,  WINDING);
				rlBarDeco.Region = polRgn2;
				olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

***/
				rlBarDeco.Type = BD_LINE;
				rlBarDeco.MarkerBrush = &olBlackBrush;
				rlBarDeco.MarkerType = MARKFULL;
				CPen SepPen(PS_SOLID, 1, COLORREF(BLACK));
				POINT rlFrom, rlTo;
				rlFrom.x = left; rlFrom.y = top;
				rlTo.x = left; rlTo.y = bottom;
				rlBarDeco.PointFrom = rlFrom;
				rlBarDeco.PointTo = rlTo;
				rlBarDeco.pPen = &SepPen;
				rlBarDeco.TextFont = pomGanttChartFont;
				olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

				rlBarDeco.Type = BD_LINE;
				rlBarDeco.MarkerBrush = &olBlackBrush;
				rlBarDeco.MarkerType = MARKFULL;
				rlFrom.x = left; rlFrom.y = bottom;
				rlTo.x = right; rlTo.y = bottom;
				rlBarDeco.PointFrom = rlFrom;
				rlBarDeco.PointTo = rlTo;
				rlBarDeco.pPen = &SepPen;
				rlBarDeco.TextFont = pomGanttChartFont;
				olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

				rlBarDeco.Type = BD_LINE;
				rlBarDeco.MarkerBrush = &olBlackBrush;
				rlBarDeco.MarkerType = MARKFULL;
				rlFrom.x = right; rlFrom.y = bottom;
				rlTo.x = right; rlTo.y = top;
				rlBarDeco.PointFrom = rlFrom;
				rlBarDeco.PointTo = rlTo;
				rlBarDeco.pPen = &SepPen;
				rlBarDeco.TextFont = pomGanttChartFont;
				olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

				rlBarDeco.Type = BD_LINE;
				rlBarDeco.MarkerBrush = &olBlackBrush;
				rlBarDeco.MarkerType = MARKFULL;
				rlFrom.x = right; rlFrom.y = top;
				rlTo.x = left; rlTo.y = top;
				rlBarDeco.PointFrom = rlFrom;
				rlBarDeco.PointTo = rlTo;
				rlBarDeco.pPen = &SepPen;
				rlBarDeco.TextFont = pomGanttChartFont;
				olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);


				CCSGanttBar olPaintBar(pDC,olDecoData);
//				delete polRgn;
//				delete polRgn2;
				//CCSGanttBar olBar;
				//olBar.Paint(pDC, olDecoData);
			olDecoData.DeleteAll();

		}
    }
}

void DispoDetailGantt::DrawTimeLines(CDC *pDC, int top, int bottom)
{
    // Draw current time
    if (omCurrentTime != TIMENULL && IsBetween(omCurrentTime, omDisplayStart, omDisplayEnd))
    {
        CPen penRed(PS_SOLID, 0, RGB(255, 0, 0));
        CPen *pOldPen = pDC->SelectObject(&penRed);
        int x = (int)GetX(omCurrentTime);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time start
    if (omMarkTimeStart != TIMENULL && IsBetween(omMarkTimeStart, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeStart);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time end
    if (omMarkTimeEnd != TIMENULL && IsBetween(omMarkTimeEnd, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeEnd);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }
}

void DispoDetailGantt::DrawFocusRect(CDC *pDC, const CRect &rect)
{
	CRect rcFocus = rect;
	rcFocus.right++;	// extra pixel to help DrawFocusRect() work correctly
	pDC->DrawFocusRect(&rcFocus);
}


/////////////////////////////////////////////////////////////////////////////
// DispoDetailGantt message handlers

BEGIN_MESSAGE_MAP(DispoDetailGantt, CWnd)
	//{{AFX_MSG_MAP(DispoDetailGantt)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_MOUSEMOVE()
    ON_WM_TIMER()
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int DispoDetailGantt::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    m_DragDropTarget.RegisterTarget(this, this);
	
	return 0;
}
DispoDetailGantt::~DispoDetailGantt()
{
	m_DragDropTarget.Revoke();
	if(prmPreviousBar!=NULL)
		delete prmPreviousBar;
	prmPreviousBar=NULL;
}
void DispoDetailGantt::OnDestroy() 
{
	m_DragDropTarget.Revoke();
	//CWnd::OnDestroy();
	CListBox::OnDestroy();
}

void DispoDetailGantt::OnSize(UINT nType, int cx, int cy)
{
    CListBox::OnSize(nType, cx, cy);

    CRect rect;
    GetWindowRect(&rect);   // don't use cx (vertical scroll may less the width of client rect)

    if (bmIsFixedScaling)   // fixed-scaling mode?
    {
        if (imWindowWidth == 0) // first time initialization?
            imWindowWidth = rect.Width();
        omDisplayEnd = GetCTime(rect.Width());   // has to recalculate the right most boundary?
    }
    else    // must be automatic variable-scaling mode
    {
        if (rect.Width() != imWindowWidth) // display window changed?
            InvalidateRect(NULL);   // repaint whole GanttChart
    }
    imWindowWidth = rect.Width();
}

// Attention:
// Be careful, this may not work if you try to develop horizontal-scrolling in GanttChart.
// Generally, if the ListBox need to be repainted, it will send the message WM_ERASEBKGND.
// But if you allow horizontal-scrolling, WM_ERASEBKGND will be called before the new
// horizontal position could be detected by dc.GetWindowOrg().x in DrawItem().
// However, this version work fine since there's no such scrolling.
// Then, we can assume that the beginning offset will be 0 all the time.
//
BOOL DispoDetailGantt::OnEraseBkgnd(CDC* pDC)
{
    CRect rectErase;
    pDC->GetClipBox(&rectErase);

    // Draw the VerticalScale if the user specify it to be displayed
    if (imVerticalScaleWidth > 0)
    {
//		int ilx;
        CBrush brush(lmVerticalScaleBackgroundColor);
        CRect rect(0, rectErase.top, imVerticalScaleWidth, rectErase.bottom);
        pDC->FillRect(&rect, &brush);

        // Draw seperator line between VerticalScale and the body of GanttChart
        CPen penBlack(PS_SOLID, 0, /*::GetSysColor(COLOR_WINDOWFRAME)*/GRAY);
        CPen *pOldPen = pDC->SelectObject(&penBlack);
		//At the End of VerticalScale
        pDC->MoveTo(imVerticalScaleWidth-2, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-2, rectErase.bottom);
    }

    // Draw the background of the body of GanttChart
    CBrush brush(lmGanttChartBackgroundColor);
    CRect rect(imVerticalScaleWidth, rectErase.top, rectErase.right, rectErase.bottom);
    pDC->FillRect(&rect, &brush);

    // Draw the vertical current time and marked time lines
    DrawTimeLines(pDC, rectErase.top, rectErase.bottom);
	//DrawDotHorizontalLine(pDC);

    // Tell Windows there's nothing more to do
    return TRUE;
}

void DispoDetailGantt::OnMouseMove(UINT nFlags, CPoint point)
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

	// Checking for dragging a job bar?
	// Notes: Here's the best place for checking for dragging from job bar.
	// We cannot simply regard OnLButtonDown() as the beginning of dragging,
	// since we defined OnLButtonDown() for bringing bar to front or to back.
	int itemID = GetItemFromPoint(point);

	if ((nFlags & MK_LBUTTON) &&
		itemID != -1 && imCurrentBar != -1)
	{
		DragJobBarBegin(/*imHighlightLine*/itemID, imCurrentBar);
	}

    CListBox::OnMouseMove(nFlags, point);
    UpdateGanttChart(point, nFlags & MK_CONTROL);
}

void DispoDetailGantt::OnTimer(UINT nIDEvent)
{
	if(	bgDoTimer == true)
	{
		if (umResizeMode != HTNOWHERE)  // in SetCapture()?
			return;

		if (nIDEvent == 0)  // the last timer message, clear everything
		{
			bmIsMouseInWindow = FALSE;
			RepaintVerticalScale(imHighlightLine, FALSE);
			imHighlightLine = -1;       // no more selected line
			UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
			return;
		}

		CPoint point;
		::GetCursorPos(&point);
		ScreenToClient(&point);
		UpdateGanttChart(point, ::GetKeyState(VK_CONTROL) & 0x8080);
	}
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
}

void DispoDetailGantt::OnLButtonDown(UINT nFlags, CPoint point)
{
    CListBox::OnLButtonDown(nFlags, point);
}

void DispoDetailGantt::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CListBox::OnLButtonUp(nFlags, point);
}

void DispoDetailGantt::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
}

void DispoDetailGantt::OnRButtonDown(UINT nFlags, CPoint point) 
{

	CWnd::OnRButtonDown(nFlags, point);
}

/////////////////////////////////////////////////////////////////////////////
// DispoDetailGantt context menus helper functions
/////////////////////////////////////////////////////////////////////////////
// DispoDetailGantt message handlers helper functions

// Update every necessary GUI elements for each mouse/timer event
void DispoDetailGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
    int itemID = GetItemFromPoint(point);

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            //((CButtonListDialog *)AfxGetMainWnd())->RegisterTimer(this);    // install the timer
        }

        if (itemID != imHighlightLine)  // move to new bar?
            RepaintVerticalScale(itemID, FALSE);
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
            UpdateBarStatus(itemID, -1);    // moving on vertical scale, not a bar
        else
        {
            int ilBarno;
            if (!bpIsControlKeyDown)
                ilBarno = GetBarnoFromPoint(itemID, point);
            else
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
            UpdateBarStatus(itemID, ilBarno);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
    if (bmIsControlKeyDown && !bpIsControlKeyDown)              // no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
//    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        //;   // do noting
/*    if(ogPrivList.GetStat("JOB_DLG_TIME")=='1')
	{
		if (HitTest(itemID, imCurrentBar, point) == HTCAPTION) // body?
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZE));
		else                                                        // left/right border?
		 //SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		 SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
*/
    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
}

// Update the frame window's status bar
void DispoDetailGantt::UpdateBarStatus(int ipLineno, int ipBarno)
{
    CString s;

    if (ipLineno == imHighlightLine && ipBarno == imCurrentBar)	// user still be on the old bar?
		return;

    if (ipLineno == -1)	// there is no more bar status?
        s = "";
    else if (ipBarno == -1)	// user move outside the bar?
        s = "";
	else
        s = pomViewer->GetBarText(ipLineno, ipBarno);

	// display status bar
    if (pomStatusBar != NULL)
        pomStatusBar->SetWindowText(s);

	// display top scale indicator
/*	if (pomTimeScale != NULL)
	{
		if (ipLineno == -1 ||	// mouse moved out of every lines?
			ipBarno == -1 && imCurrentBar != -1)	// mouse just leave a bar?
			pomTimeScale->RemoveAllTopScaleIndicator();
		else if (ipBarno != imCurrentBar)
		{
			pomTimeScale->RemoveAllTopScaleIndicator();
			int ilIndicatorCount = pomViewer->GetIndicatorCount(ipLineno, ipBarno);
			for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
			{
				JOB_INDICATORDATA *prlIndicator = pomViewer->GetIndicator(ipLineno, ipBarno, ilIndicatorno);
				pomTimeScale->AddTopScaleIndicator(prlIndicator->StartTime,
					prlIndicator->EndTime, prlIndicator->Color);
			}
			pomTimeScale->DisplayTopScaleIndicator();
		}
	}
*/
    // remember the bar and update the status bar (if exist)
    imCurrentBar = ipBarno;
}

// Start moving/resizing
BOOL DispoDetailGantt::BeginMovingOrResizing(CPoint point)
{
/***************

******************/
    return (umResizeMode != HTNOWHERE);
}

// Update the resizing/moving rectangle
BOOL DispoDetailGantt::OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown)
{
//FSCX

	return TRUE;
}
// return the item ID of the list box based on the given "point"
int DispoDetailGantt::GetItemFromPoint(CPoint point)
{
    CRect rcItem;
    for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}

// Return the bar number of the list box based on the given point
int DispoDetailGantt::GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    point.y -= rcItem.top + imGutterHeight;
    int level1 = ((point.y < imBarHeight) || (imOverlapHeight == 0))? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
    int level2 = ((point.y < 0) || (imOverlapHeight == 0))? -1: point.y / imOverlapHeight;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBarnoFromTime(ipLineno, time1, time2, level1, level2);
}


// Return the hit test area code (determine the location of the cursor).
// Possible codes are HTLEFT, HTRIGHT, HTNOWHERE (out-of-bar), HTCAPTION (bar body)
// 
UINT DispoDetailGantt::HitTest(int ipLineno, int ipBarno, CPoint point)
{
    if (ipLineno == -1 || ipBarno == -1)    // this checking was added after some bugs found
        return HTNOWHERE;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);

    // Check against each possible case of hit test areas.
    // Since most people like to drag things to the right, we check right border before left.
    // Be careful, if the bar is very small, the user may not be able to get HTCAPTION or HTLEFT.
    //
    DEM_BARDATA *prlBar = pomViewer->GetBar(ipLineno, ipBarno);
    if (IsBetween(prlBar->Duen, time1, time2))
        return HTRIGHT;
    else if (IsBetween(prlBar->Dube, time1, time2))
        return HTLEFT;
    else if (IsOverlapped(time1, time2, prlBar->Dube, prlBar->Duen))
        return HTCAPTION;

    return HTNOWHERE;
}

//-DTT Jul.25-----------------------------------------------------------
// Return true if the mouse position in on the vertical scale or a duty bar.
// Also remember the place where the drop happened in "imDropLineno".
BOOL DispoDetailGantt::IsDropOnDutyBar(CPoint point)
{
	int ilLineno;
	if ((ilLineno = GetItemFromPoint(point)) == -1)
		return FALSE;	// cannot find a line of this diagram
	if (point.x < imVerticalScaleWidth - 2)
		return TRUE;	// mouse is on the vertical scale

	// If it reaches here, mouse is on the background of the GanttChart.
	return FALSE;
}
//----------------------------------------------------------------------


/////////////////////////////////////////////////////////////////////////////
// DispoDetailGantt -- drag-and-drop functionalities
//
// The user may drag from:
//	- VerticalScale or the DutyBar for create non-duty jobs.
//	- the JobBar for reassignment a job.
//
// and the user will have opportunities to:
//	-- drop from PrePlanTable onto any places in DispoDetailGantt.
//		If the user also press Ctrl, we will open a dialog and confirm before.
//	-- drop from DutyBar from other pools for create job detach.
//		We will ask the user for the period of time and create one JobDetach and another
//		JobPool for the new pool for the specified period of time.
//	-- drop from JobBar in the same pool (reassign job)
//		(Just for JobFlight, JobCciDesk, and JobSpecial.)
//		We will change the field JOUR and the field PKNO to the new staff.
//	-- drop from FlightBar onto the Vertical Scale, the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job for that staff.
//	-- drop from the background area from FlightDetailWindow onto the Vertical Scale,
//		the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job (without demand) for that staff.
//	-- drop from DemandBar onto the Vertical Scale, the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job for that staff to resolve that demand.
//	-- drop from gate (vertical scale) in GateDiagram onto any places in the gantt chart.
//		We will ask the user for the period of time and create many flight jobs for
//		this employees for demands in that Gate.
//	-- drop from CCI-Desk (vertical scale) in CciDiagram (to create JobCci)
//		We will ask the user for the period of time and create a JobCciDesk for
//		this employee. (use the default time as the default).
//
void DispoDetailGantt::DragDutyBarBegin(int ipLineno)
{
/*	char slDube[20]="";
	m_DragDropSource.CreateDWordData(DIT_JOBBAR, 1);
	CTime Dube = pomViewer->GetLine(ipLineno)->Dube;
//	m_DragDropSource.AddDWord(Dube);
	strcpy(slDube, Dube.Format("%d%m%Y%H%M"));
	m_DragDropSource.AddString(slDube);
	m_DragDropSource.BeginDrag();
*/
}

void DispoDetailGantt::DragJobBarBegin(int ipLineno, int ipBarno)
{

}

LONG DispoDetailGantt::OnDragOver(UINT wParam, LONG lParam)
{
    ::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    

	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;
	int ilLineCnt=-1;
	int itemID = GetItemFromPoint(omDropPosition);
	int ilBarno = GetBarnoFromPoint(itemID, omDropPosition);
	long llRet =-1L;
	int ilClass = m_DragDropTarget.GetDataClass(); 
	CCSDragDropCtrl *polDragDropCtrl = &m_DragDropTarget;
	FLIGHTDATA *prlCurFlight=NULL,*prlDragFlight=NULL;
	DEMDATA *prlCurDem=NULL,*prlDragDem=NULL;
	if(ogPrivList.GetStat("DDG_DRAG_OVER")=='1')
	{

	}
	return llRet;
}

LONG DispoDetailGantt::OnDrop(UINT wParam, LONG lParam)
{
	//FSCX

    return -1L;
//----------------------------------------------------------------------
}


LONG DispoDetailGantt::ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}


void DispoDetailGantt::AttachWindow(CJobDlg *popWnd)
{
	pomDialog = popWnd;
}

