// CedaSphData.h

#ifndef __CEDASPHDATA__
#define __CEDASPHDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct SPHDATA 
{
	char 	 Act3[5]; 	// Flugzeugtyp
	long 	 Actm;		// Flugzeugtyp Gruppe (URNO)
	long 	 Alcm;		// Fluggesellschaft Gruppe (URNO)
	char 	 Apc3[5]; 	// Flughafen
	long 	 Apcm;		// Flughafen Gruppe (URNO)
	char 	 Arde[3]; 	// Ankunft oder Abflug
	CTime 	 Cdat; 		// Erstellungsdatum
	char 	 Days[9]; 	// Verkehrstage
	char 	 Flnc[5]; 	// Flugnummer Code
	char 	 Flnn[7]; 	// Flugnummer Nummer
	char 	 Flns[3]; 	// Flugnummer Suffix
	CTime 	 Lstu; 		// Datum letzte Änderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Regn[14]; 	// Flugzeug-Kennzeichen
	char 	 Styp[4]; 	// Servicetyp
	char 	 Ttyp[7]; 	// Verkehrsartenschlüssel
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte Änderung)
	char 	 Eart[3]; 	// Ergänzungsart


	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed für Relaese

	SPHDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

}; // end SphDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSphData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SPHDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaSphData();
	~CedaSphData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SPHDATA *prpSph);
	bool InsertInternal(SPHDATA *prpSph);
	bool Update(SPHDATA *prpSph);
	bool UpdateInternal(SPHDATA *prpSph);
	bool Delete(long lpUrno);
	bool DeleteInternal(SPHDATA *prpSph);
	bool ReadSpecialData(CCSPtrArray<SPHDATA> *popSph,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SPHDATA *prpSph);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SPHDATA  *GetSphByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareSphData(SPHDATA *prpSphData);
	CString omWhere; //Prf: 8795
	bool ValidateSphBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDASPHDATA__
