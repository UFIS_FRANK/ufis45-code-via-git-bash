
// cSdtd.cpp - Class for handling Sdt data
//

#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CCSGlobl.h>
#include <CedaSdtData.h>
#include <CedaSdgData.h>
#include <CedaBsdData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <CedaBasicData.h>
#include <algorithm>

#include <sys/timeb.h>
#include <time.h>
#define IsEnveloped(start1, end1, start2, end2)    ((start1) <= (end2) && (start1) >= (start2) && (end1) <= (end2) && (end1) >= (start2))

static int CompareByTime(const SDTDATA **e1, const SDTDATA **e2)
{
	return ((**e1).Esbg>(**e2).Esbg) ? 0 : -1;
}

CedaSdtData::CedaSdtData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(SDTDATA, SdtDataRecInfo)
		FIELD_CHAR_TRIM(Bewc, "BEWC")
		FIELD_CHAR_TRIM(Bkr1, "BKR1")	
		FIELD_CHAR_TRIM(Bsdc, "BSDC")	
		FIELD_CHAR_TRIM(Bsdk, "BSDK")	
		FIELD_CHAR_TRIM(Bsdn, "BSDN")	
		FIELD_CHAR_TRIM(Bsds, "BSDS")	
		FIELD_CHAR_TRIM(Ctrc, "CTRC")	
		FIELD_CHAR_TRIM(Days, "DAYS")	
		FIELD_CHAR_TRIM(Dnam, "DNAM")	
		FIELD_CHAR_TRIM(Type, "TYPE")	
		FIELD_CHAR_TRIM(Usec, "USEC")	
		FIELD_CHAR_TRIM(Useu, "USEU")	
		FIELD_INT	   (Sdu1, "SDU1")	
		FIELD_INT	   (Sex1, "SEX1")	
		FIELD_INT	   (Ssh1, "SSH1")	
		FIELD_INT	   (Bkd1, "BKD1")	
		FIELD_LONG	   (Urno, "URNO")	
		FIELD_DATE	   (Bkf1, "BKF1")	
		FIELD_DATE	   (Bkt1, "BKT1")	
		FIELD_DATE	   (Sbgi, "SBGI")	
		FIELD_DATE	   (Seni, "SENI")	
		FIELD_DATE	   (Cdat, "CDAT")	
		FIELD_DATE	   (Esbg, "ESBG")	
		FIELD_DATE	   (Lsen, "LSEN")	
		FIELD_DATE	   (Lstu, "LSTU")	
		FIELD_DATE	   (Brkf, "BRKF")
		FIELD_DATE	   (Brkt, "BRKT")	
		FIELD_LONG	   (Bsdu, "BSDU")	
		FIELD_LONG	   (Sdgu, "SDGU")	
		FIELD_CHAR_TRIM(Fctc, "FCTC")	
    END_CEDARECINFO
    // Copy the record structure
    for (int i = 0; i < sizeof(SdtDataRecInfo)/sizeof(SdtDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SdtDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"SDT");
	strcat(pcmTableName,pcgTableExt);

	strcpy(pcmListOfFields,"BEWC,BKR1,BSDC,BSDK,BSDN,BSDS,CTRC,DAYS,"
						   "DNAM,TYPE,USEC,USEU,SDU1,SEX1,"
						   "SSH1,BKD1,URNO,BKF1,BKT1,SBGI,SENI,CDAT,ESBG,LSEN,"
						   "LSTU,BRKF,BRKT,BSDU,SDGU,FCTC");

	 pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer


    //omData.SetSize(600);
	//omData.RemoveAll();
	ClearAll();
}

void CedaSdtData::Register(void)
{
	ogDdx.Register(this,BC_SDT_CHANGE,CString("BC_SDTCHANGE"), CString("SdtDataChange"),ProcessSdtCf);
	ogDdx.Register(this,BC_SDT_NEW,CString("BC_SDTNEW"), CString("SdtDataNew"),ProcessSdtCf);
	ogDdx.Register(this,BC_SDT_DELETE,CString("BC_SDTNEW"), CString("SdtDataDelete"),ProcessSdtCf);
	ogDdx.Register(this,BC_RELSDG,CString("BC_RELSDG"),CString("RELSDG"),ProcessSdtCf);
}

CedaSdtData::~CedaSdtData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
	
}	

bool CedaSdtData::ReadSpecialData(CCSPtrArray<SDTDATA> *popSdt, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";

	if(strlen(pspFieldList) > 0)
	{
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			strcpy(pclFieldList, pspFieldList);
		}
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSdt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SDTDATA *prpSdt = new SDTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSdt,CString(pclFieldList))) == true)
			{
				popSdt->Add(prpSdt);
			}
			else
			{
				delete prpSdt;
			}
		}
		if(popSdt->GetSize() == 0) return false;
	}
    return true;
}



bool CedaSdtData::Read(char *pspWhere /*NULL*/)
{
    //char where[512];
	//omData.DeleteAll();
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;
	SYSTEMTIME rlPerf;
	
	ClearAll();

	CTime omTime;

	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read DB Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
		{
			return false;
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read DB End %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Load data from CedaData into the dynamic array of record

	
    GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read <Building omData> Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
	for (int ilLc = 0; ilRc == true; ilLc++)
    {
		SDTDATA *prlSdt = new SDTDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSdt))==true) //GetBufferRecord(ilLc,prlSdt)) == true)
		{
			prlSdt->IsChanged = DATA_UNCHANGED;

			if(AddSdtInternal(prlSdt)==false)
			{
				delete prlSdt;
			}
		}
		else
		{
			delete prlSdt;
			TRACE("Deleted extra prlSdt\n");
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read <Building omData> End<Count %d> %d:%d:%d.%d\n",ilLc,rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    return true;
}

bool CedaSdtData::ReadRealSdt(char *pspWhere /*NULL*/)
{
    
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;
	SYSTEMTIME rlPerf;
	
	CCSPtrArray<SDTDATA> olDelList;

	POSITION rlPos;	
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(!prlSdt->IsVirtuel)
		{
			prlSdt->IsChanged = DATA_DELETED;
			olDelList.Add(prlSdt);
		}

	}

	for(int i = olDelList.GetSize() - 1; i >= 0; i--)
	{
		DeleteSdtInternal(&olDelList[i]);
	}

	olDelList.RemoveAll();

	
	CTime omTime;

	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read DB Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction2("RTA", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (CedaAction2("RT", pspWhere) == false)
		{
			return false;
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read DB End %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Load data from CedaData into the dynamic array of record

	
    GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read <Building omData> Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
	for (int ilLc = 0; ilRc == true; ilLc++)
    {
		SDTDATA *prlSdt = new SDTDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSdt))==true) //GetBufferRecord(ilLc,prlSdt)) == true)
		{
			prlSdt->IsChanged = DATA_UNCHANGED;

			if(AddSdtInternal(prlSdt)==false)
			{
				delete prlSdt;
			}
		}
		else
		{
			delete prlSdt;
			TRACE("Deleted extra prlSdt\n");
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::Read <Building omData> End<Count %d> %d:%d:%d.%d\n",ilLc,rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    return true;
}

bool CedaSdtData::ReadSdtWithSdgu(long lpSdgu)
{
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;
	SYSTEMTIME rlPerf;
	
	CTime omTime;

	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReadSdtWithSdgu Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	CString olWhere;
	olWhere.Format("WHERE SDGU = '%ld'",lpSdgu);

    // Select data from the database
	if (CedaAction2("RT", olWhere.GetBuffer(0)) == false)
	{
		return false;
	}

	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReadSdtWithSdgu End %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    // Load data from CedaData into the dynamic array of record

	
    GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReadSdtWithSdgu <Building omData> Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
	for (int ilLc = 0; ilRc == true; ilLc++)
    {
		SDTDATA *prlSdt = new SDTDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSdt))==true) 
		{
			prlSdt->IsChanged = DATA_UNCHANGED;

			if (AddSdtInternal(prlSdt) == false)
			{
				delete prlSdt;
			}
		}
		else
		{
			delete prlSdt;
			TRACE("Deleted extra prlSdt\n");
		}
	}
	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReadSdtWithSdgu <Building omData> End<Count %d> %d:%d:%d.%d\n",ilLc,rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
    return true;
		
}

bool CedaSdtData::ClearAll()
{
    omUrnoMap.RemoveAll();
	omDnamMap.RemoveAll();
	omSelectedData.RemoveAll();
	omRedrawDuties.RemoveAll();
	omTotalFctcMap.RemoveAll();
	omTotalQualiMap.RemoveAll();

	omTotalFctcArray.DeleteAll();
	omTotalQualiArray.DeleteAll();

	for (POSITION pos = omSdguAndTypeMap.GetStartPosition(); pos != NULL;)
	{
		CString olName;
		CCSPtrArray<SDTDATA> *polValue;
		omSdguAndTypeMap.GetNextAssoc(pos,olName,(void *&)polValue);
		delete polValue;
	}

	omSdguAndTypeMap.RemoveAll();


    omData.DeleteAll();

    return true;
}


bool CedaSdtData::AddSdtInternal(SDTDATA *prpSdt, bool bpWithDDX /*= false*/)
{
	SDTDATA *prlSdt = NULL;
	if(omUrnoMap.Lookup((void *)prpSdt->Urno,(void *&)prlSdt)==FALSE)
	{
		PrepareSdtData(prpSdt);
		omData.Add(prpSdt);
		omUrnoMap.SetAt((void *)prpSdt->Urno,prpSdt);
		omDnamMap.SetAt((CString)prpSdt->Dnam, prpSdt);

		CString olName;
		olName.Format("%ld,%s",prpSdt->Sdgu,prpSdt->Type);
		CCSPtrArray<SDTDATA> *polValue = NULL;
		if (omSdguAndTypeMap.Lookup(olName,(void *&)polValue) == FALSE)
		{
			polValue = new CCSPtrArray<SDTDATA>;
			omSdguAndTypeMap.SetAt(olName,polValue);
		}

		polValue->Add(prpSdt);

		if(bpWithDDX == true)
		{
			ogDdx.DataChanged((void *)this,SDT_NEW,(void *)prpSdt);
		}
		return true;
	}
	else
	{
		return false;
	}
}




/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class)

bool CedaSdtData::DeleteSdt(SDTDATA *prpSdtData, BOOL bpWithSave,bool bpSendDdx )
{

	bool olRc = true;
		
	prpSdtData->IsChanged = DATA_DELETED;

	if(bpSendDdx)
		ogDdx.DataChanged((void *)this,SDT_DELETE,(void *)prpSdtData);
	
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		olRc = SaveSdt(prpSdtData);
	}

	DeleteSdtInternal(prpSdtData);
	return olRc;
}

bool CedaSdtData::DeleteSdtWithSplu(long lpSplUrno)
{
	POSITION rlPos;
	long llUrno;

	SDTDATA *prlSdt = NULL;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->SplUrno == lpSplUrno)
		{
			DeleteSdtInternal(prlSdt);
		
		}
	}
	return true;
}

bool CedaSdtData::DeleteSdtWithSdgu(SDTDATA *prpSdtData,bool bpWithSave)
{
	bool olRc = false;
	
	if(GetSdtByUrno(prpSdtData->Urno)==NULL)
	{
		return olRc;
	}

	long llSdgu = prpSdtData->Sdgu;
	long llBsdu = prpSdtData->Bsdu;
	POSITION rlPos;
	CCSPtrArray<SDTDATA> olSdtList;

	CCSPtrArray<TIMEFRAMEDATA> olTimes;
	ogSdgData.GetTimeFrames(llSdgu, olTimes);
	CTimeSpan olSpan;
	
	int ilDayOffset = -999;
	for(int j = olTimes.GetSize()-1;j>=0;j--)
	{
		if(IsBetween(prpSdtData->Esbg,olTimes[j].StartTime,olTimes[j].EndTime))
		{
			olSpan = CTime(prpSdtData->Esbg.GetYear(), prpSdtData->Esbg.GetMonth(), prpSdtData->Esbg.GetDay(), 0, 0, 0) - olTimes[j].StartTime;
			ilDayOffset = olSpan.GetDays();
			break;
		}
	}

	if(ilDayOffset==-999)
	{
		// Error!!
		olTimes.DeleteAll();
		return false;
	}
	CTimeSpan olDayOffset(ilDayOffset,0,0,0);

	int ilDayOfWeek = prpSdtData->Esbg.GetDayOfWeek();

	// Collect all specified SDTs within SDG on this day
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Sdgu==llSdgu && prlSdt->Bsdu == llBsdu /*&& prlSdt->Esbg.GetDayOfWeek() == ilDayOfWeek*/ && strcmp(prlSdt->Fctc,prpSdtData->Fctc)==0 && prpSdtData->IsVirtuel)
		{
			olSdtList.Add(prlSdt);
			olRc = true;
		}
	}

	if(bpWithSave && olRc == true)
	{
		//olSdtList.Sort(CompareByTime);
		CCSPtrArray<SDTDATA> olList;
		CString olDeletedDates = "";
		for(j = olTimes.GetSize()-1;j>=0;j--)
		{
			// Determine date of shift in question
			//CTime olDate = /*prlSdt->Esbg =*/ HourStringToDate(prpSdtData->Esbg.Format("%H%M00"), olTimes[j].StartTime + olDayOffset);
			//TRACE("DATE %s\n",olDate.Format("%d.%m.%Y - %H:%M"));
			SDTDATA *prlDefaultSdt = NULL;
			for(int i = 0; i < olSdtList.GetSize(); i++)
			{
				SDTDATA *prlSdt = &olSdtList[i];
				
				if(IsBetween(prlSdt->Esbg,olTimes[j].StartTime,olTimes[j].EndTime))
				{
					if(prlSdt->Urno==prpSdtData->Urno)		
					{
						prlDefaultSdt = prlSdt;
						break;
					}
					else if(prlSdt->IsSelected == TRUE)
					{
						prlDefaultSdt = prlSdt;
					}
					else
					{
						if(prlDefaultSdt==NULL)
						{
							prlDefaultSdt = prlSdt;
						}
					}
				}
			}
			if(prlDefaultSdt!=NULL)
			{
				//TRACE("Preparing to delete URNO %ld => ESBG <%s>\n",prlDefaultSdt->Urno,prlDefaultSdt->Esbg.Format("%d.%m.%Y - %H:%M"));
				olList.Add(prlDefaultSdt);
			}
		}

		for(int i = olList.GetSize() - 1; i >= 0; i--)
		{
			DeleteSdtInternal(&olList[i]);
		}
		olList.RemoveAll();
		olSdtList.RemoveAll();
	}
	olTimes.DeleteAll();
	return olRc;
}

bool CedaSdtData::DeleteVirtuellSdtsBySdg(long lpSdgu)
{
	bool olRc = false;
	
	long llSdgu = lpSdgu;
	POSITION rlPos;
	CCSPtrArray<SDTDATA> olSdtList;

	// Collect all specified SDTs within SDG on this day
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Sdgu==llSdgu  &&  prlSdt->IsVirtuel)
		{
			olSdtList.Add(prlSdt);
			olRc = true;
		}
	}



	for(int i = olSdtList.GetSize() - 1; i >= 0; i--)
	{
		ogBasicData.PushBackUrno(olSdtList[i].Urno);
		DeleteSdtInternal(&olSdtList[i]);
	}
	
	return olRc;
}

bool CedaSdtData::DeleteVirtuellSdtsByMsdu(long lpMsdu)
{
	bool olRc = false;
	
	long llMsdu = lpMsdu;
	POSITION rlPos;
	CCSPtrArray<SDTDATA> olSdtList;

	// Collect all specified SDTs within SDG on this day
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->MsdUrno==llMsdu  &&  prlSdt->IsVirtuel)
		{
			olSdtList.Add(prlSdt);
			olRc = true;
		}
	}



	for(int i = olSdtList.GetSize() - 1; i >= 0; i--)
	{
		ogBasicData.PushBackUrno(olSdtList[i].Urno);
		DeleteSdtInternal(&olSdtList[i]);
	}
	
	return olRc;
}

/*
bool CedaSdtData::DeleteAllSdtsWithRosu(long lpRosu,bool bpWithDdx)
{
	bool olRc = false;
	POSITION rlPos;
	CCSPtrArray<SDTDATA> olSdtList;
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Sdgu==lpRosu)
		{
			olSdtList.Add(prlSdt);
			olRc = true;
		}
	}

	if(bpWithDdx)
	{
		for(int j = olSdtList.GetSize()-1;j>=0;j--)
		{
			SDTDATA *prlSdt = &olSdtList[j];
			prlSdt->IsChanged = DATA_DELETED;
			ogDdx.DataChanged((void *)this,SDT_DELETE,(void *)prlSdt);
			DeleteSdtInternal(&olSdtList[j]);	
		}
	}
	olSdtList.RemoveAll();
	return olRc;
}
*/

bool CedaSdtData::DeleteSdtWithRos(long lpRosu,CTime opStart,CTime opEnd,bool bpWithDdx)
{
	bool olRc = false;
	POSITION rlPos;
	CCSPtrArray<SDTDATA> olSdtList;
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Sdgu==lpRosu && IsBetween(prlSdt->Esbg,opStart,opEnd))
		{
			olSdtList.Add(prlSdt);
			olRc = true;
			//break;
		}
	}

	for(int j = olSdtList.GetSize()-1;j>=0;j--)
	{
		SDTDATA *prlSdt = &olSdtList[j];
		prlSdt->IsChanged = DATA_DELETED;
		if(bpWithDdx)
			ogDdx.DataChanged((void *)this,SDT_DELETE,(void *)prlSdt);
		DeleteSdtInternal(&olSdtList[j]);	
	}
	//olSdtList.RemoveAll();
	return olRc;
}


bool CedaSdtData::InsertSdt(SDTDATA *prpSdtData, BOOL bpWithSave,bool bpSendDdx)
{
	bool olRc = true;

	//prpSdtData->IsChanged = DATA_NEW;
	if(prpSdtData->IsChanged == DATA_UNCHANGED)
	{
		prpSdtData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		SaveSdt(prpSdtData);
	}
	if(bpSendDdx)
		ogDdx.DataChanged((void *)this,/*SDT_NEW*/SDT_CHANGE,(void *)prpSdtData);
	return olRc;
}

bool CedaSdtData::UpdateSdt(SDTDATA *prpSdtData, BOOL bpWithSave,bool bpSendDdx)
{
	bool olRc = true;

	if(bpSendDdx)
		ogDdx.DataChanged((void *)this,SDT_CHANGE,(void *)prpSdtData);

	if(prpSdtData->IsChanged == DATA_UNCHANGED)
	{
		prpSdtData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		SaveSdt(prpSdtData);
	}


	return olRc;

}


bool CedaSdtData::SdtExist(long Urno)
{
	// don't read from database anymore, just check internal array
	SDTDATA *prlData;
	bool olRc = true;

	if(omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == TRUE)
	{
		;//*prpData = prlData;
	}
	else
	{
		olRc = false;
	}
	return olRc;
}

bool CedaSdtData::DnamExist(CString opDnam)
{
	SDTDATA *prlData;
	if(omDnamMap.Lookup(opDnam,(void *&)prlData) == TRUE)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*void CedaSdtData::GetAllDnam(CStringArray &ropList)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		CString olDnam;
		omDnamMap.GetNextAssoc(rlPos, olDnam, (void *&)prlSdt);
		//ogDdx.DataChanged((void *)this,SDT_CHANGE,(void *)prlSdt);
		ropList.Add(CString(prlSdt->Dnam));
	}
}
*/
void CedaSdtData::GetAllDnam(CStringArray &ropList)
{
	bool ilRc = true;
	char pclFieldList[256] = "DNAM";

	if (CedaAction("RT",pcmTableName, " distinct(DNAM) ", "","",pcgDataBuf) == false)
	{
		return;
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SDTDATA *prlSdt = new SDTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlSdt,CString(pclFieldList))) == true)
		{
			ropList.Add(CString(prlSdt->Dnam));
			delete prlSdt;
		}
		else
		{
			delete prlSdt;
		}
	}
}
// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessSdtCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		ogSdtData.ProcessSdtBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}


void  CedaSdtData::ProcessSdtBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	if (ipDDXType == BC_SDT_CHANGE || ipDDXType == BC_SDT_NEW)
	{
		SDTDATA *prpSdt;
		struct BcStruct *prlSdtData;

		prlSdtData = (struct BcStruct *) vpDataPointer;

		long llUrno;

		if (strstr(prlSdtData->Selection,"WHERE") != NULL)
		{
			llUrno = GetUrnoFromSelection(prlSdtData->Selection);
		}
		else
		{
			llUrno = atol(prlSdtData->Selection);
		}
	
		if (omUrnoMap.Lookup((void*)llUrno,(void *& )prpSdt) == TRUE)
		{
			GetRecordFromItemList(prpSdt,prlSdtData->Fields,prlSdtData->Data);
			ogDdx.DataChanged((void *)this,SDT_CHANGE,(void *)prpSdt);
		}
		else
		{
			prpSdt = new SDTDATA;
			GetRecordFromItemList(prpSdt,prlSdtData->Fields,prlSdtData->Data);
			AddSdtInternal(prpSdt);
			ogDdx.DataChanged((void *)this,SDT_NEW,(void *)prpSdt);

		}
	}
	else if (ipDDXType == BC_SDT_DELETE)
	{
		SDTDATA *prpSdt;
		struct BcStruct *prlSdtData;

		prlSdtData = (struct BcStruct *) vpDataPointer;

		long llUrno;

		if (strstr(prlSdtData->Selection,"WHERE") != NULL)
		{
			llUrno = GetUrnoFromSelection(prlSdtData->Selection);
		}
		else
		{
			llUrno = atol(prlSdtData->Selection);
		}
	
		
		if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpSdt) == TRUE)
		{
			ogDdx.DataChanged((void *)this, SDT_DELETE,(void *)prpSdt );
			DeleteSdtInternal(prpSdt);
		}
		
	}
	else if (ipDDXType == BC_RELSDG)
	{
		struct BcStruct *prlBcStruct;
		prlBcStruct = (struct BcStruct *) vpDataPointer;
		SDGDATA *prlSdg = ogSdgData.GetSdgByUrno(atol(prlBcStruct->Data));					
		if (prlSdg)
		{
			SDTDATA *prlSdt;
			long llUrno;

			CCSPtrArray<SDTDATA> olList;
			POSITION rlPos;
			
			for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
				if(prlSdt->Sdgu == prlSdg->Urno)
				{
					olList.Add(prlSdt);
					prlSdt->IsChanged = DATA_DELETED;
				}
			}

			for(int i = olList.GetSize() - 1; i >= 0; i--)
			{
				DeleteSdtInternal(&olList[i]);
			}

			olList.RemoveAll();

			this->ReadSdtWithSdgu(prlSdg->Urno);
			
			ogDdx.DataChanged(this,RELSDG,(void *)prlSdg);
		}
	}
}



SDTDATA * CedaSdtData::GetSdtByUrno(long pcpUrno)
{
	SDTDATA *prpSdt;

	if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prpSdt) == TRUE)
	{
		return prpSdt;
	}
	return NULL;
}



bool CedaSdtData::ChangeSdtTime(long lpSdtUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave)
{
	SDTDATA *prlSdt = GetSdtByUrno(lpSdtUrno);
	if(prlSdt != NULL)
	{
		ogDdx.DataChanged((void *)this,SDT_CHANGE,(void *)prlSdt);
		if (prlSdt->IsChanged == DATA_UNCHANGED)
		{
			prlSdt->IsChanged = DATA_CHANGED;
		}
		if(bpWithSave == true)
		{
			UpdateSdt(prlSdt, TRUE);
		}
	}
    return true;
}


bool CedaSdtData::DeleteSdtInternal(SDTDATA *prpSdt)
{
	omUrnoMap.RemoveKey((void *)prpSdt->Urno);

	CString olName;
	olName.Format("%ld,%s",prpSdt->Sdgu,prpSdt->Type);

	CCSPtrArray<SDTDATA> *polValue = NULL;
	if (omSdguAndTypeMap.Lookup(olName,(void *&)polValue) == TRUE)
	{
		for (int ilLc = 0; ilLc < polValue->GetSize(); ilLc++)
		{
			if (polValue->GetAt(ilLc).Urno == prpSdt->Urno)
			{
				polValue->RemoveAt(ilLc);
				if (polValue->GetSize() == 0)
				{
					delete polValue;
					omSdguAndTypeMap.RemoveKey(olName);
				}
				break;
			}
		}
	}

	int ilSdtCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSdtCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSdt->Urno)
		{
			omData.DeleteAt(ilLc);
			break;
		}
	}

    return true;
}



bool CedaSdtData::SaveSdt(SDTDATA *prpSdt)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if(prpSdt->IsVirtuel)
	{
		prpSdt->IsChanged = DATA_UNCHANGED;
	}

	if (prpSdt->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpSdt->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSdt);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpSdt->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpSdt->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSdt);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpSdt->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		if(!prpSdt->IsVirtuel)
		{
			sprintf(pclSelection, "WHERE URNO = '%ld'", prpSdt->Urno);
			olRc = CedaAction("DRT",pclSelection);
			if(!omLastErrorMessage.IsEmpty())
			{
				::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
			}
		}
		break;
	}

   return olRc;
}




void CedaSdtData::MakeSdtSelected(SDTDATA *prpSdt, BOOL bpSelect)
{
	if(prpSdt != NULL)
	{
		int ilCount = omSelectedData.GetSize();
		long lSdtu = prpSdt->Urno;
		UINT *loc;
		prpSdt->IsSelected = bpSelect;

		if(bpSelect == TRUE)
		{			
			if(ilCount>0)
			{
				loc = std::find(&omSelectedData[0],&omSelectedData[0] + ilCount,lSdtu);
				if(loc== (&omSelectedData[0] + ilCount)) // not found
				{
					omSelectedData.Add(prpSdt->Urno);
					ogDdx.DataChanged((void *)this,SDT_SELCHANGE,(void *)prpSdt);
				}
			}
			else
			{
				omSelectedData.Add(prpSdt->Urno);
				ogDdx.DataChanged((void *)this,SDT_SELCHANGE,(void *)prpSdt);
			}
		}
		else
		{
			if(ilCount>0)
			{
				loc = std::find(&omSelectedData[0],&omSelectedData[0] + ilCount,lSdtu);
				if(loc!= (&omSelectedData[0] + ilCount)) // found
				{
					ogDdx.DataChanged((void *)this,SDT_SELCHANGE,(void *)prpSdt);
					omSelectedData.RemoveAt(loc-&omSelectedData[0]);
				}
			}
		}
	}
}

void CedaSdtData::GetSdtByBsdu(long lpBsdu,CCSPtrArray<SDTDATA> *popSdtData)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Bsdu == lpBsdu)
		{
			popSdtData->Add(prlSdt);
		}
	}
}
void CedaSdtData::GetSdtByRosu(long lpSdgu,CCSPtrArray<SDTDATA> *popSdtData)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); popSdtData!=NULL && rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Sdgu == lpSdgu)
		{
			popSdtData->Add(prlSdt);
		}
	}
}

void CedaSdtData::DeSelectAll()
{
	int ilCount = omSelectedData.GetSize();
	for(int i = ilCount-1; i >= 0; i--)
	{
		SDTDATA *prlSdt = GetSdtByUrno(omSelectedData[i]);
		if(prlSdt != NULL)
		{
			if(prlSdt->IsSelected == TRUE)
			{ 
				prlSdt->IsSelected = FALSE;
				ogDdx.DataChanged((void *)this,SDT_SELCHANGE,(void *)prlSdt);
			}
		}
	}
	omSelectedData.RemoveAll();
	
}


//------------------------------------------------------------------------------
// This function has two different behaviors:
// 1. If the parameter popGhdList is set and has data ==> only these Data are released
// 2. No Parameter ==> All SDT-Data are released
void CedaSdtData::Release(CCSPtrArray<SDTDATA> *popList,long lpSdgu)
{
	CString olIRT;
	CString olURT;
	CString olDRT;
	CString olFieldList;
	CString olDataList;
	CString olUpdateString;
	CString olInsertString;
	CString olDeleteString;
	//struct _timeb timebuffer;
	int ilNoUpdates = 0;
	int ilNoDeletes = 0;
	int ilNoInserts = 0;
	CStringArray olFields;
	int ilFields = ExtractItemList(CString(ogSdtData.pcmListOfFields),&olFields); 
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	int ilCurrentCount = 0;
	if(popList == NULL) //==> Dann alle Speichern
	{
		olOrigFieldList = CString(ogSdtData.pcmListOfFields);
		olInsertString = olIRT + CString(ogSdtData.pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(ogSdtData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;

		POSITION rlPos;
		for ( rlPos = ogSdtData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			SDTDATA *prlSdt;
			ogSdtData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			if(prlSdt->IsChanged != DATA_UNCHANGED && !prlSdt->IsVirtuel)
			{
				CString olListOfData;
				CString olCurrentUrno;
				olCurrentUrno.Format("%d",prlSdt->Urno);
				MakeCedaData(&omRecInfo,olListOfData,prlSdt);
				switch(prlSdt->IsChanged)
				{
				case DATA_NEW:
					olInsertString += olListOfData + CString("\n");
					ilCurrentCount++;
					ilNoInserts++;
					break;
				case DATA_CHANGED:
					olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
					ilNoUpdates++;
					ilCurrentCount++;
					break;
				case DATA_DELETED:
					olDeleteString += olCurrentUrno + CString("\n");
					ilCurrentCount++;
					ilNoDeletes++;
					break;
				}
				prlSdt->IsChanged = DATA_UNCHANGED;
			}
			if(ilCurrentCount == 500)
			{
				if(ilNoInserts > 0)
				{
					//_ftime( &timebuffer );
					//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olInsertString.GetBuffer(0));
					//_ftime( &timebuffer );
					//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
					strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				if(ilNoUpdates > 0)
				{
					//_ftime( &timebuffer );
					//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
					//_ftime( &timebuffer );
					//TRACE("Stopped time(UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
					strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				if(ilNoDeletes > 0)
				{
					//_ftime( &timebuffer );
					//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
					CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
					//_ftime( &timebuffer );
					//TRACE("Stopped time(DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
					strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
				ilCurrentCount = 0;
				ilNoInserts = 0;
				ilNoUpdates = 0;
				ilNoDeletes = 0;
				olInsertString = olIRT + CString(ogSdtData.pcmListOfFields) + CString("\n");
				olUpdateString = olURT + CString(ogSdtData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
				olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;
			}
		}//for(rlPos = ......
		if(ilNoInserts > 0)
		{
			//_ftime( &timebuffer );
			//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olInsertString.GetBuffer(0));
			//_ftime( &timebuffer );
			//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
			strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
		if(ilNoUpdates > 0)
		{
			//_ftime( &timebuffer );
			//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
			//_ftime( &timebuffer );
			//TRACE("Stopped time(UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
			strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}
		if(ilNoDeletes > 0)
		{
			//_ftime( &timebuffer );
			//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
			CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
			//_ftime( &timebuffer );
			//TRACE("Stopped time(DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
			strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
		}

	}
	else
	{
		olOrigFieldList = CString(ogSdtData.pcmListOfFields);
		olInsertString = olIRT + CString(ogSdtData.pcmListOfFields) + CString("\n");
		olUpdateString = olURT + CString(ogSdtData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;

		for ( int i = 0; i < popList->GetSize(); i++)
		{
			SDTDATA *prlSdt = GetSdtByUrno(popList->GetAt(i).Urno);
			if(prlSdt != NULL)
			{
				if(prlSdt->IsChanged != DATA_UNCHANGED)
				{
					CString olListOfData;
					CString olCurrentUrno;
					olCurrentUrno.Format("%d",prlSdt->Urno);
					MakeCedaData(&omRecInfo,olListOfData,prlSdt);
					switch(prlSdt->IsChanged)
					{
					case DATA_NEW:
						olInsertString += olListOfData + CString("\n");
						ilNoInserts++;
						ilCurrentCount++;
						break;
					case DATA_CHANGED:
						olUpdateString += olListOfData + CString(",") + olCurrentUrno + CString("\n");
						ilNoUpdates++;
						ilCurrentCount++;
						break;
					case DATA_DELETED:
						olDeleteString += olCurrentUrno + CString("\n");
						ilNoDeletes++;
						ilCurrentCount++;
						break;
					}
					prlSdt->IsChanged = DATA_UNCHANGED;
					if(ilCurrentCount == 500)
					{
						if(ilNoInserts > 0)
						{
							//_ftime( &timebuffer );
							//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
							CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olInsertString.GetBuffer(0));
							//_ftime( &timebuffer );
							//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
							strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoUpdates > 0)
						{
							//_ftime( &timebuffer );
							//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
							CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
							//_ftime( &timebuffer );
							//TRACE("Stopped time(UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
							strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoDeletes > 0)
						{
							//_ftime( &timebuffer );
							//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
							CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
							//_ftime( &timebuffer );
							//TRACE("Stopped time(DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
							strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						ilCurrentCount = 0;
						ilNoInserts = 0;
						ilNoUpdates = 0;
						ilNoDeletes = 0;
						olInsertString = olIRT + CString(ogSdtData.pcmListOfFields) + CString("\n");
						olUpdateString = olURT + CString(ogSdtData.pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
						olDeleteString = olDRT + CString("[URNO=:VURNO]")+ CString("\n");;
			
					}
				}
			}
		}//for(rlPos = ......
		if(ilCurrentCount > 0)
		{
			if(ilNoInserts > 0)
			{
				//_ftime( &timebuffer );
				//TRACE("Start time (INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
				CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olInsertString.GetBuffer(0));
				//_ftime( &timebuffer );
				//TRACE("Stopped time(INSERT)<%d>: %s %hu\n",ilNoInserts,ctime(&(timebuffer.time)), timebuffer.millitm);
				strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoUpdates > 0)
			{
				//_ftime( &timebuffer );
				//TRACE("Start time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
				CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olUpdateString.GetBuffer(0));
				//_ftime( &timebuffer );
				//TRACE("Stopped time (UPDATE)<%d>: %s %hu\n",ilNoUpdates,ctime(&(timebuffer.time)), timebuffer.millitm);
				strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoDeletes > 0)
			{
				//_ftime( &timebuffer );
				//TRACE("Start time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
				CedaAction("REL","LATE,NOBC,NOACTION,NOLOG"/*"LATE"*/,"",olDeleteString.GetBuffer(0));
				//_ftime( &timebuffer );
				//TRACE("Stopped time (DELETE)<%d>: %s %hu\n",ilNoDeletes,ctime(&(timebuffer.time)), timebuffer.millitm);
				strcpy(ogSdtData.pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
		}

		if (lpSdgu != 0)
		{
			CString olData;
			olData.Format("%ld",lpSdgu);
			CedaAction("SBC","RELSDG","",CCSCedaData::pcmReqId,"",olData.GetBuffer(0));
		}
	}
}


void CedaSdtData::DeleteBySdgu(long lpSdgUrno)
{
	SDTDATA *prlSdt;
	long llUrno;

	CCSPtrArray<SDTDATA> olList;
	POSITION rlPos;
	
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		if(prlSdt->Sdgu == lpSdgUrno)
		{
			olList.Add(prlSdt);
			prlSdt->IsChanged = DATA_DELETED;
		}
	}


	for(int i = olList.GetSize() - 1; i >= 0; i--)
	{
		DeleteSdtInternal(&olList[i]);
	}

	char pclSelection[512];
	sprintf(pclSelection, "WHERE SDGU = '%ld'", lpSdgUrno);
	bool olRc = CedaAction("DRT",pclSelection);

	olList.RemoveAll();
}


bool CedaSdtData::UpdateSdtInternal(SDTDATA *prpSdt)
{
	bool blRet = true;
	if (prpSdt != NULL)
	{
		SDTDATA* prlSdt = GetSdtByUrno(prpSdt->Urno);
		if(prlSdt == NULL)
		{
			blRet = false;
		}
		else
		{
			*prlSdt = *prpSdt;
		}
	}
	else
	{
		blRet = false;
	}
    return blRet;

}

bool CedaSdtData::GetSelectedSdt(CCSPtrArray<SDTDATA> *popSelectedSdt)
{
	int ilCount = omSelectedData.GetUpperBound();
	
	for(int k=0;k<= ilCount;k++)
	{
		long llUrno = omSelectedData.GetAt(k);
		SDTDATA *prlSdt = GetSdtByUrno(llUrno);
		if(prlSdt!=NULL)
		{
			popSelectedSdt->NewAt(0,*prlSdt);
		}
	}
	return (popSelectedSdt->GetSize()>0);
}


bool CedaSdtData::ChangeTime(long lpUrno,CTime opStartTime,CTime opEndTime,GanttDynType bpDynType,BOOL bpSave /* TRUE */)
{
	SDTDATA *prlSdt = GetSdtByUrno(lpUrno);
	CTimeSpan olTimeDiff,olBrkDura;
	CString olStartTime,olEndTime;
	bool blRet = false;
	olStartTime = opStartTime.Format("%Y.%m.%d - %H:%M");
	olEndTime = opEndTime.Format("%Y.%m.%d - %H:%M");

	if(prlSdt!=NULL)
	{
		switch(bpDynType)
		{
		case GANTT_BREAK:
			if(IsEnveloped(opStartTime, opEndTime, prlSdt->Bkf1,prlSdt->Bkt1))
			{
				if(prlSdt->Brkf != opStartTime || prlSdt->Brkt != opEndTime)
				{
					prlSdt->Brkf = opStartTime;
					prlSdt->Brkt = opEndTime;
					UpdateSdt(prlSdt,bpSave);
					blRet =true;
				}
			}
			break;
		case GANTT_BREAK_PERIOD:
			if((strcmp(prlSdt->Type,"D")!=0 && (prlSdt->Bkf1 != opStartTime || prlSdt->Bkt1 != opEndTime) && IsEnveloped(opStartTime,opEndTime,prlSdt->Esbg,prlSdt->Lsen)) || (strcmp(prlSdt->Type,"D")==0 && IsEnveloped(opStartTime,opEndTime,prlSdt->Sbgi,prlSdt->Seni)))
			{
				prlSdt->Bkf1 = opStartTime;
				prlSdt->Bkt1 = opEndTime;
				UpdateSdt(prlSdt,bpSave);
				blRet =true;
			}
			break;
		case GANTT_SHIFT:
			//(prlSdt->Sbgi != opStartTime || prlSdt->Seni == opEndTime)
			if(IsEnveloped(opStartTime, opEndTime, prlSdt->Esbg,prlSdt->Lsen) && IsEnveloped( prlSdt->Bkf1,prlSdt->Bkt1,opStartTime, opEndTime))
			{
				prlSdt->Sbgi = opStartTime;
				prlSdt->Seni = opEndTime;
				UpdateSdt(prlSdt,bpSave);
				blRet = true;
			}
		default:
			break;
		}
	}
	return blRet;
}

bool CedaSdtData::GetSdtsBetweenTimes(CTime opStart,CTime opEnd,CCSPtrArray<SDTDATA> &opSdtArray)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			//TRACE("Begin <%s> End <%s>\n",prlSdt->Esbg.Format("%Y.%m.%d-%H:%M"),prlSdt->Lsen.Format("%Y.%m.%d-%H:%M"));
			if(prlSdt->Esbg<=opEnd && prlSdt->Lsen>=opStart && prlSdt->IsChanged!=DATA_DELETED)
			{
				opSdtArray.Add(prlSdt);
				blRet = true;
			}
		}
	}
	return blRet;
}

bool CedaSdtData::GetSdtsBetweenTimes(CTime opStart,CTime opEnd,CMapPtrToPtr &opSdtUrnoMap)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			//TRACE("Begin <%s> End <%s>\n",prlSdt->Esbg.Format("%Y.%m.%d-%H:%M"),prlSdt->Lsen.Format("%Y.%m.%d-%H:%M"));
			CString olStr = CString(prlSdt->Fctc);
			
			if(prlSdt->Esbg<=opEnd && prlSdt->Lsen>=opStart && prlSdt->IsChanged!=DATA_DELETED && !prlSdt->IsVirtuel)
			{
				opSdtUrnoMap.SetAt((void *)prlSdt->Urno,prlSdt);
				blRet = true;
			}
		}
	}
	return blRet;
}

bool CedaSdtData::GetSdtsBetweenTimesAndSdgu(CTime opStart,CTime opEnd,CMapPtrToPtr& ropSdgUrnoMap,CMapPtrToPtr &opSdtUrnoMap)
{
	bool blRet =false;
	void *polValue = NULL;

	if (opEnd != TIMENULL && opStart != TIMENULL && opEnd > opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			if (prlSdt->Sdgu == 0 || ropSdgUrnoMap.Lookup((void *)prlSdt->Sdgu,polValue))
			{
				//TRACE("Begin <%s> End <%s>\n",prlSdt->Esbg.Format("%Y.%m.%d-%H:%M"),prlSdt->Lsen.Format("%Y.%m.%d-%H:%M"));
				if(prlSdt->Esbg <= opEnd && prlSdt->Lsen >= opStart && prlSdt->IsChanged != DATA_DELETED && !prlSdt->IsVirtuel)
				{
					opSdtUrnoMap.SetAt((void *)prlSdt->Urno,prlSdt);
					blRet = true;
				}
			}
		}
	}
	return blRet;
}

bool CedaSdtData::GetSdtsBetweenTimesAndDrrFilter(CTime opStart,CTime opEnd,CMapPtrToPtr &opSdtUrnoMap,bool bpShowActualDsr,int ipLevel)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			
			CString olStr = CString(prlSdt->Fctc);
			
			bool blPassFilter = true;
			if(bpShowActualDsr)
			{
				if(*prlSdt->StatPlSt != 'A')
				{
					blPassFilter = false;
				}
			}
			else
			{
				if(prlSdt->Planungsstufe != ipLevel)
				{
					blPassFilter = false;
				}
			}

			if(blPassFilter && prlSdt->Esbg<=opEnd && prlSdt->Lsen>=opStart && *prlSdt->Type == 'R' && prlSdt->IsChanged!=DATA_DELETED)
			{
				opSdtUrnoMap.SetAt((void *)prlSdt->Urno,prlSdt);
				blRet = true;
			}
		}
	}
	return blRet;
}


bool CedaSdtData::GetSdtByGplUrno(long lpGplu, CCSPtrArray <SDTDATA> &opSdtData)
{
	bool blRet =false;
	
	opSdtData.RemoveAll();

	POSITION rlPos;	
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
	
		if(prlSdt->Sdgu == lpGplu )
		{
			opSdtData.Add(prlSdt);
			blRet = true;
		}
	}
	return blRet;
}

bool CedaSdtData::GetSdtByGspUrno(long lpGspu, CCSPtrArray <SDTDATA> &opSdtData)
{
	bool blRet =false;
	
	opSdtData.RemoveAll();

	POSITION rlPos;	
	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
	
		if(prlSdt->GspUrno == lpGspu )
		{
			opSdtData.Add(prlSdt);
			blRet = true;
		}
	}
	return blRet;
}

bool CedaSdtData::GetSdtsSdguAndType(long lpSdgu, char cpType,CCSPtrArray <SDTDATA> &opSdtData,bool bpAdd)
{
	bool blRet = false;

	if (!bpAdd)
		opSdtData.RemoveAll();

	CString olName;
	olName.Format("%ld,%c",lpSdgu,cpType);
	CCSPtrArray<SDTDATA> *polValue;
	if (omSdguAndTypeMap.Lookup(olName,(void *&)polValue) == FALSE)
		return false;

	opSdtData.Append(*polValue);

	return true;
}

bool CedaSdtData::GetSdtsBetweenTimesAndSdgu(CTime opStart,CTime opEnd,long lpSdgu,CMapPtrToPtr &opSdtUrnoMap)
{
	bool blRet =false;
	if(opEnd!=TIMENULL && opStart!=TIMENULL && opEnd>opStart)
	{
		POSITION rlPos;	
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			//TRACE("Begin <%s> End <%s>\n",prlSdt->Esbg.Format("%Y.%m.%d-%H:%M"),prlSdt->Lsen.Format("%Y.%m.%d-%H:%M"));
			CString olStr = CString(prlSdt->Fctc);
			if(prlSdt->Esbg<=opEnd && prlSdt->Lsen>=opStart && prlSdt->IsChanged!=DATA_DELETED && prlSdt->Sdgu == lpSdgu && prlSdt->IsVirtuel)
			{
				opSdtUrnoMap.SetAt((void *)prlSdt->Urno,prlSdt);
				blRet = true;
			}
		}
	}
	return blRet;
}

bool CedaSdtData::GetSdtsByGplus(CString opGplus,CMapPtrToPtr &opSdtUrnoMap)
{
	bool blRet =false;

	POSITION rlPos;

	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		//TRACE("Begin <%s> End <%s>\n",prlSdt->Esbg.Format("%Y.%m.%d-%H:%M"),prlSdt->Lsen.Format("%Y.%m.%d-%H:%M"));
		CString olStr;
		olStr.Format(",%ld,",prlSdt->Sdgu);
		if(opGplus.Find(olStr) > -1)
		{
			opSdtUrnoMap.SetAt((void *)prlSdt->Urno,prlSdt);
			blRet = true;
		}
	}
	return blRet;
}

void CedaSdtData::ReleaseVirtuellSdts(long lpSdgu)
{
	CTime omTime;

	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReleaseVirtuellSdts Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	CCSPtrArray<SDTDATA> olSaveList;
	CCSPtrArray<SDTDATA> olDelList;
	POSITION rlPos;	
	long llSDTs = 0; 

	SDGDATA * prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
	if(prlSdg != NULL)
	{
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			CString olStr = CString(prlSdt->Fctc);

			if(!prlSdt->IsVirtuel && prlSdt->IsChanged!=DATA_DELETED && prlSdt->Sdgu == lpSdgu)
			{
				prlSdt->IsChanged = DATA_DELETED;
				olDelList.Add(prlSdt);
			}
			else if(prlSdt->IsVirtuel && prlSdt->IsChanged!=DATA_DELETED && prlSdt->Sdgu == lpSdgu)
			{
				prlSdt->IsVirtuel = false;
				prlSdt->IsChanged = DATA_NEW;
				olSaveList.Add(prlSdt);
			}
		}

		for(int i = olDelList.GetSize() - 1; i >= 0; i--)
		{
			DeleteSdtInternal(&olDelList[i]);
		}

		char pclSelection[512];
		sprintf(pclSelection, "WHERE SDGU = '%ld'", lpSdgu);
		bool olRc = CedaAction("DRT",pclSelection);

		Release(&olSaveList,lpSdgu);

		llSDTs = olDelList.GetSize() + olSaveList.GetSize();

		olDelList.RemoveAll();
		olSaveList.RemoveAll();
	}

	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReleaseVirtuellSdts End %d:%d:%d.%d for %ld SDT's\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds,llSDTs);

#if	0

	if (!ogSdgData.CheckLoaded(lpSdgu,true))
	{
		bool blLoaded = ogSdtData.ReadSdtWithSdgu(lpSdgu);			// to get the previously released SDT's
		ogSdgData.CheckLoaded(lpSdgu,blLoaded);
	}

	CTime omTime;

	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReleaseVirtuellSdts Start %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	CMapStringToPtr		 olOldMap;
	CCSPtrArray<SDTDATA> olNewList;
	CString				 olKey;
	CCSPtrArray<SDTDATA>*polArray = NULL;	
	POSITION rlPos;	

	olOldMap.InitHashTable(20000,TRUE);
	long llSDTs = 0; 

	SDGDATA * prlSdg = ogSdgData.GetSdgByUrno(lpSdgu);
	if(prlSdg != NULL)
	{
		CTimeSpan olDisplayOffset = 0;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			SDTDATA *prlSdt;
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
			CString olStr = CString(prlSdt->Fctc);

			// select the previously released SDT's
			if(!prlSdt->IsVirtuel && prlSdt->IsChanged!=DATA_DELETED && prlSdt->Sdgu == lpSdgu)
			{
				olKey = prlSdt->GetKey();
				if (!olOldMap.Lookup(olKey,(void *&)polArray))
				{
					polArray = new CCSPtrArray<SDTDATA>;
					olOldMap.SetAt(olKey,(void *)polArray);
				}

				polArray->Add(prlSdt);
				++llSDTs;
			}

			// select the SDT's to be released
			if(prlSdt->IsVirtuel && prlSdt->IsChanged!=DATA_DELETED && prlSdt->Sdgu == lpSdgu)
			{
				olNewList.Add(prlSdt);
				++llSDTs;
			}
		}

		// compare the list to figure out what is to be done

		SDTDATA *prlSdt;
		for(int i = olNewList.GetSize() - 1; i >= 0; i--)
		{
			bool blFound = false;
			olKey = olNewList[i].GetKey();
			if (olOldMap.Lookup(olKey,(void *&)polArray))
			{
				for (int j = polArray->GetSize() - 1; j >= 0; j--)
				{
					if (olNewList[i] == (*polArray)[j])		// identical	-> nothing to do
					{
						blFound = true;
						polArray->RemoveAt(j);
						if (polArray->GetSize() == 0)
						{
							olOldMap.RemoveKey(olKey);
							delete polArray;
						}

						DeleteSdtInternal(&olNewList[i]);
						olNewList.RemoveAt(i);
						break;
					}
				}
			}

			if (!blFound)	// -> release this SDT's
			{
				prlSdt = &olNewList[i];
				prlSdt->IsVirtuel = false;
				prlSdt->IsChanged = DATA_NEW;
			}
		}

		if (olOldMap.GetCount() < 100)
		{
			// delete previously released SDT's no longer valid
			POSITION pos = olOldMap.GetStartPosition();
			while (pos != NULL)
			{
				olOldMap.GetNextAssoc(pos,olKey,(void *&)polArray);
				for (int j = polArray->GetSize() - 1; j >= 0; j--)
				{
					DeleteSdt(&(*polArray)[j],true,false);
				}

				delete polArray;
			}

			olOldMap.RemoveAll();
		}
		else
		{
			// delete previously released SDT's no longer valid
			CCSPtrArray<SDTDATA> olOldList;

			POSITION pos = olOldMap.GetStartPosition();
			while (pos != NULL)
			{
				olOldMap.GetNextAssoc(pos,olKey,(void *&)polArray);
				for (int j = polArray->GetSize() - 1; j >= 0; j--)
				{
					prlSdt = &(*polArray)[j];
					prlSdt->IsChanged = DATA_DELETED;
					olOldList.Add(prlSdt);
				}

				delete polArray;
			}

			olOldMap.RemoveAll();

			// call the release handler to delete the SDT's no longer valid
			Release(&olOldList);

			for (int j = olOldList.GetSize() - 1; j >= 0; j--)
			{
				DeleteSdtInternal(&olOldList[j]);
			}
		}

		// call the release handler to release the newly created SDT's
		Release(&olNewList);

	}

	GetSystemTime(&rlPerf);
	TRACE("CedaSdtData::ReleaseVirtuellSdts End %d:%d:%d.%d for %ld SDT's\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds,llSDTs);

#endif
}


void CedaSdtData::PrepareSdtData(SDTDATA *prpSdt)
{

	CString olFctc = prpSdt->Fctc;

	CString olBsdUrno;
	olBsdUrno.Format("%ld",prpSdt->Bsdu);

	CString olAllFctc = ogBCD.GetValueList("DEL", "BSDU", olBsdUrno, "FCTC") ;
	if(!olAllFctc.IsEmpty())
	{
		olAllFctc += ",";
	}
	olAllFctc += olFctc;

	if(*prpSdt->Type == 'R')  //Roster record;
	{
		CCSPtrArray <SDTDATA> olSdtList;
		CString olDrdFct = "";
		CString olComplFct = "";
		GetSdtsSdguAndType(prpSdt->Urno, 'D',olSdtList);
		GetSdtsSdguAndType(prpSdt->Urno, 'A',olSdtList,true);
		for(int ilDrd = 0; ilDrd < olSdtList.GetSize(); ilDrd++)
		{
			olDrdFct = olSdtList[ilDrd].Fctc;
			if(!olDrdFct.IsEmpty())
			{
				olComplFct += ",";
				olComplFct += olDrdFct;
			}
		}
		if(!olComplFct.IsEmpty())
		{
			olAllFctc += olComplFct;
		}

		if(prpSdt->RosterStfu > 0)
		{
			CString olUrno;
			CStringArray olList;
			CString olFrom,olTo,olCode;
			CString olQualiUrnos = "";
			CTime olStart, olEnd;
			olUrno.Format("%ld",prpSdt->RosterStfu);
			CString olAllQuali = ogBCD.GetValueList("SPE", "SURN", olUrno, "URNO") ;
			ExtractItemList(olAllQuali,&olList);
			for(int ilLCount = 0; ilLCount < olList.GetSize();ilLCount++)
			{
				ogBCD.GetFields("SPE", "URNO", olList[ilLCount],"VPFR","VPTO", olFrom,olTo);
				ogBCD.GetField("SPE", "URNO", olList[ilLCount],"CODE", olCode);
				olStart= DBStringToDateTime(olFrom);
				olEnd = DBStringToDateTime(olTo);

				if(olStart != TIMENULL && olEnd != TIMENULL)
				{
					if(IsOverlapped(olStart,olEnd,prpSdt->Esbg,prpSdt->Lsen) == TRUE)
					{
						olQualiUrnos += olCode ;
						olQualiUrnos += ",";
					}
				}
				else if(olStart != TIMENULL)
				{
					if(olStart < prpSdt->Lsen)
					{
						olQualiUrnos += olCode ;
						olQualiUrnos += ",";
					}
				}
				else if(olEnd != TIMENULL)
				{
					if(olEnd > prpSdt->Esbg)
					{
						olQualiUrnos += olCode ;
						olQualiUrnos += ",";
					}
				}
				else
				{
						olQualiUrnos += olCode;
						olQualiUrnos += ",";
				}	
			}

			olList.RemoveAll();

			if(!olQualiUrnos.IsEmpty())
			{
				int ilPos = omTotalQualiArray.GetSize();
				omTotalQualiArray.NewAt(ilPos,olQualiUrnos);
				omTotalQualiMap.SetAt((void *)prpSdt->Urno,&omTotalQualiArray[ilPos]);
			}
		}
	}


	
	int ilPos = omTotalFctcArray.GetSize();
	omTotalFctcArray.NewAt(ilPos,olAllFctc);
	omTotalFctcMap.SetAt((void *)prpSdt->Urno,&omTotalFctcArray[ilPos]);



}


CString CedaSdtData::GetTotalFctc(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omTotalFctcMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}


CString CedaSdtData::GetTotalQuali(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omTotalQualiMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}


void CedaSdtData::DeleteAutoSdts()
{
	int ilSdtCount = omData.GetSize() -1;
	for (int ilLc = ilSdtCount ; ilLc >= 0; ilLc--)
	{
		if (omData[ilLc].IsAutoSdt)
		{
			omUrnoMap.RemoveKey((void *)omData[ilLc].Urno);

			CString olName;
			olName.Format("%ld,%s",omData[ilLc].Sdgu,omData[ilLc].Type);

			CCSPtrArray<SDTDATA> *polValue = NULL;
			if (omSdguAndTypeMap.Lookup(olName,(void *&)polValue) == TRUE)
			{
				for (int ilLc2 = 0; ilLc2 < polValue->GetSize(); ilLc2++)
				{
					if (polValue->GetAt(ilLc2).Urno == omData[ilLc].Urno)
					{
						polValue->RemoveAt(ilLc2);
						if (polValue->GetSize == 0)
						{
							delete polValue;
							omSdguAndTypeMap.RemoveKey(olName);
						}
						break;
					}
				}
			}

			omData.DeleteAt(ilLc);
		}
	}
}


void CedaSdtData::GetAllAutoSdts(CCSPtrArray <SDTDATA> &opSdtData)
{

	POSITION rlPos;

	for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		SDTDATA *prlSdt;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSdt);
		//TRACE("Begin <%s> End <%s>\n",prlSdt->Esbg.Format("%Y.%m.%d-%H:%M"),prlSdt->Lsen.Format("%Y.%m.%d-%H:%M"));
		
		if(prlSdt->IsAutoSdt)
		{
			opSdtData.Add(prlSdt);
	
		}
	}
}