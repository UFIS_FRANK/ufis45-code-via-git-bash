// Search.cpp: implementation of the CSearch class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <Coverage.h>
#include <NoDemFlights.h>
#include <CovDiagram.h>
#include <Jobdlg.h>
#include <SearchResultsDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
	
static int CompareFlights(const FLIGHTSEARCHDATA **e1, const FLIGHTSEARCHDATA **e2)
{

	return(strcmp((**e1).SortTifa,(**e2).SortTifa));
	
}





CNoDemFlights::CNoDemFlights()
{
	
}

CNoDemFlights::CNoDemFlights(CoverageDiagramViewer *popCallerView,const CStringArray * popTsrArray,const CMapPtrToPtr * popFlightDemMap,bool bpWithoutDems)
{
	pomViewer = popCallerView;
	FindFlightsWithoutDem( popTsrArray,popFlightDemMap,bpWithoutDems);
}

CNoDemFlights::~CNoDemFlights()
{
	omDemMap.RemoveAll();
	omUrnoMap.RemoveAll();
}

bool CNoDemFlights::FindFlightsWithoutDem(const CStringArray * popTsrArray,const CMapPtrToPtr * popFlightDemMap,bool bpWithoutDems )
{
	omUrnoMap.RemoveAll();
	CCSPtrArray<FLIGHTDATA> olFlights;

	CWaitCursor olWait;
	POSITION rlPos;
	for ( rlPos = pomViewer->GetFilterFlightUrnoMap()->GetStartPosition(); rlPos != NULL; )
	{
		FLIGHTDATA * prlFlight = NULL;
		long llUrno;
		pomViewer->GetFilterFlightUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlFlight);

		if(prlFlight != NULL)
		{
			prlFlight->IsTouched = false;
		}
	}

	
	for ( rlPos = pomViewer->GetDemUrnoMap()->GetStartPosition(); rlPos != NULL; )
	{
		DEMDATA *prlDem;
		long llUrno;
		pomViewer->GetDemUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);

		if(prlDem != NULL)
		{
			FLIGHTDATA * prlFlight = NULL;
			if((atoi(prlDem->Dety) == 0) || (atoi(prlDem->Dety) == 1))
			{
				if(pomViewer->GetFilterFlightUrnoMap()->Lookup((void *)prlDem->Ouri,(void *&)prlFlight)  == TRUE)
				{
					if(prlFlight != NULL)
					{
						if(prlFlight->Ftyp[0] != 'N')
						{
							prlFlight->IsTouched = true;
						}
					}
				}
			}
			if((atoi(prlDem->Dety) == 0) || (atoi(prlDem->Dety) == 2))
			{
				if(pomViewer->GetFilterFlightUrnoMap()->Lookup((void *)prlDem->Ouro,(void *&)prlFlight)  == TRUE)
				{
					if(prlFlight != NULL)
					{
						if(prlFlight->Ftyp[0] != 'N')
						{
							prlFlight->IsTouched = true;
						}
					}
				}
			}
		}
	}

	for ( rlPos = pomViewer->GetFilterFlightUrnoMap()->GetStartPosition(); rlPos != NULL; )
	{
		FLIGHTDATA * prlFlight = NULL;
		long llUrno;
		pomViewer->GetFilterFlightUrnoMap()->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlFlight);

		if(prlFlight != NULL)
		{
			if(!prlFlight->IsTouched)
			{
				if(bpWithoutDems)
				{
					olFlights.Add(prlFlight);
				}
			}
			else
			{
				if(!bpWithoutDems)
				{
					olFlights.Add(prlFlight);
				}
			}

		}
	}





	int ilFlightCount = olFlights.GetSize();

	if(ilFlightCount == 0)
	{
		if(bpWithoutDems)
		{
			MessageBox(NULL, LoadStg(IDS_STRING61229),LoadStg(IDS_WARNING), MB_OK);
		}
		else
		{
			MessageBox(NULL, LoadStg(IDS_STRING61230),LoadStg(IDS_WARNING), MB_OK);
		}
		return false;

	}

	for(int ilIndex = 0; ilIndex < ilFlightCount;ilIndex++)
	{
		FLIGHTDATA *prlDummyFlight= NULL;
		FLIGHTDATA *prlFlightA= NULL;
		FLIGHTDATA *prlFlightD= NULL;
		FLIGHTDATA *prlTmpFlight = &olFlights[ilIndex];

		if(prlTmpFlight != NULL )
		{
			if (omUrnoMap.Lookup((void *)prlTmpFlight->Urno,(void *& )prlDummyFlight) == FALSE)
			{
				omUrnoMap.SetAt((void *)prlTmpFlight->Urno,prlTmpFlight);
			
				if(strcmp(prlTmpFlight->Adid, "D") == 0)
				{
					prlFlightD = prlTmpFlight;
					prlFlightA = ogCovFlightData.GetJoinFlight(prlTmpFlight);
					if(prlFlightA != NULL)
					{
						if (omUrnoMap.Lookup((void *)prlFlightA->Urno,(void *& )prlDummyFlight) == FALSE)
						{
							omUrnoMap.SetAt((void *)prlFlightA->Urno,prlFlightA);
						}
						else
						{
							prlFlightA = NULL;
						}
					}
				}
				else if(strcmp(prlTmpFlight->Adid, "A") == 0)
				{
					prlFlightA = prlTmpFlight;
					prlFlightD = ogCovFlightData.GetJoinFlight(prlTmpFlight);
					if(prlFlightD != NULL)
					{
						if (omUrnoMap.Lookup((void *)prlFlightD->Urno,(void *& )prlDummyFlight) == FALSE)
						{
							omUrnoMap.SetAt((void *)prlFlightD->Urno,prlFlightD);
						}
						else
						{
							prlFlightD = NULL;
						}
					}

				}
				else if(strcmp(prlTmpFlight->Adid, "B") == 0) //Rundfl�ge erstmal ignorieren!!
				{
					prlFlightA = NULL;
					prlFlightD = NULL;
				}
			}
		
			FLIGHTSEARCHDATA rlSearchFlightData;

			if(prlFlightD != NULL)
			{
				strcpy(rlSearchFlightData.Act3d,prlFlightD->Act3);
				rlSearchFlightData.Furn = prlFlightD->Urno;
				rlSearchFlightData.Urnd = prlFlightD->Urno;
				strcpy(rlSearchFlightData.Flnod,prlFlightD->Flno);
				strcpy(rlSearchFlightData.Regnd,prlFlightD->Regn);
				strcpy(rlSearchFlightData.Dest,prlFlightD->Des3);
				strcpy(rlSearchFlightData.PosD,prlFlightD->Pstd);

				CTime olStod = prlFlightD->Stod;

				CString olTime = olStod.Format("%d.%m.%y - %H%M");
				strcpy(rlSearchFlightData.Tifd,olTime);
				olTime = olStod.Format("%Y%m%d%H%M");
				strcpy(rlSearchFlightData.SortTifd,olTime);
				strcpy(rlSearchFlightData.SortTifa,olTime);
				if(prlFlightD->IsTouched)
				{
					rlSearchFlightData.OutGrayed = true;
				}

			}
			if(prlFlightA != NULL)
			{
				strcpy(rlSearchFlightData.Act3a,prlFlightA->Act3);
				if(prlFlightD == NULL)
				{
					rlSearchFlightData.Furn = prlFlightA->Urno;
				}
				rlSearchFlightData.Urna = prlFlightA->Urno;
				strcpy(rlSearchFlightData.Flnoa,prlFlightA->Flno);
				strcpy(rlSearchFlightData.Regna,prlFlightA->Regn);
				strcpy(rlSearchFlightData.Orig,prlFlightA->Org3);
				strcpy(rlSearchFlightData.PosA,prlFlightA->Psta);

				CTime olStoa = prlFlightA->Stoa;

				CString olTime = olStoa.Format("%d.%m.%y - %H%M");
				strcpy(rlSearchFlightData.Tifa,olTime);
				olTime = olStoa.Format("%Y%m%d%H%M");
				strcpy(rlSearchFlightData.SortTifa,olTime);
				if(prlFlightD == NULL)
				{
					strcpy(rlSearchFlightData.SortTifd,olTime);
				}
				if(prlFlightA->IsTouched)
				{
					rlSearchFlightData.InGrayed = true;
				}


			}
			if((prlFlightD != NULL || prlFlightA != NULL))
			{
				bool blAIsLoaded = true;
				bool blDIsLoaded = true;
				if (rlSearchFlightData.Urna != 0L)
				{
					if(ogCovFlightData.GetFlightByUrno(rlSearchFlightData.Urna) == NULL)
					{
						blAIsLoaded = false;
					}
				}
				if (rlSearchFlightData.Urnd != 0L)
				{
					if(ogCovFlightData.GetFlightByUrno(rlSearchFlightData.Urnd) == NULL)
					{
						blDIsLoaded = false;
					}
				}

				rlSearchFlightData.IsLoaded = (blDIsLoaded && blAIsLoaded);
				omFoundFlight.NewAt(omFoundFlight.GetSize(),rlSearchFlightData);
			}
		}
	}

	if(omFoundFlight.GetSize() == 1)
	{
		long llUrna = omFoundFlight[0].Urna;
		long llUrnd = omFoundFlight[0].Urnd;
		if( pogJobDlg == NULL )
		{
			pogJobDlg = new CJobDlg(NULL );
		}
		else
		{
			pogJobDlg->SetActiveWindow();
		}

		FLIGHTDATA *prlFlightA = ogCovFlightData.GetFlightByUrno(llUrna);
		FLIGHTDATA *prlFlightD = ogCovFlightData.GetFlightByUrno(llUrnd);

		DEMDATA *prlDem;
		omDemMap.RemoveAll();
		if(popFlightDemMap != NULL)
		{
			CMapPtrToPtr *polDemMap;
			if(llUrna > 0)
			{
				if(popFlightDemMap->Lookup((void *)llUrna,(void *&)polDemMap)  == TRUE)
				{	
					for ( rlPos =  polDemMap->GetStartPosition(); rlPos != NULL; )
					{
						long llUrno;
						polDemMap->GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) prlDem);
						if(prlDem != NULL)
						{
							omDemMap.SetAt((void *)prlDem->Urno,prlDem);
						}
					}
				}
			}
			if(llUrnd > 0)
			{
				if(popFlightDemMap->Lookup((void *)llUrnd,(void *&)polDemMap)  == TRUE)
				{	
					for ( rlPos =  polDemMap->GetStartPosition(); rlPos != NULL; )
					{
						long llUrno;
						polDemMap->GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) prlDem);
						if(prlDem != NULL)
						{
							omDemMap.SetAt((void *)prlDem->Urno,prlDem);
						}
					}
				}
			}
		}


		pogJobDlg->InitFlightFields((CWnd*)pogCoverageDiagram, popTsrArray,&omDemMap,prlFlightA,prlFlightD,llUrna, llUrnd,NULL);
		return false;
	}
	else
	{
		omFoundFlight.Sort(CompareFlights);
		SearchResultsDlg olResultDlg(omFoundFlight,NULL,"");
		olResultDlg.SetCaptionText(LoadStg(IDS_STRING61345));
		olResultDlg.AttachTsrArray(popTsrArray);
		olResultDlg.AttachFlightDemMap(popFlightDemMap);

		olResultDlg.AttachFlightData(&ogCovFlightData);
		omFoundFlight.DeleteAll();
		olResultDlg.DoModal();
		return true;
	}

}
