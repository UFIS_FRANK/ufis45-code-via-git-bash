// stafchrt.h : header file
//

#ifndef __COVERAGECHART_
#define __COVERAGECHART_

#include <ccsdragdropctrl.h>
#include <CCSGlobl.h>
#include <CCS3dStatic.h>
#include <CoverageGantt.h>
#include <CoverageGraphicWnd.h>
//#include "CedaRosData.h"

#include <Resulttable.h>


/////////////////////////////////////////////////////////////////////////////
// CoverageChart frame
enum 
{
	COVERAGE_ZEROLINE = -1,
	COVERAGE_GRAPHIC = 0,
	COVERAGE_SHIFTDEMANDS = 1,
	COVERAGE_BASESHIFTS = 2
};

class CovDiagram;

//@Man:
//@Memo: Coverage Chart
/*@Doc: 
	Frame for the 3 chart types 
	COVERAGE_GRAPHIC = 0,
	COVERAGE_SHIFTDEMANDS = 1,
	COVERAGE_BASESHIFTS = 2
*/
class CoverageChart : public CFrameWnd
{
	friend CovDiagram;

    DECLARE_DYNCREATE(CoverageChart)
public:
	CoverageChart();
    CoverageChart(int ipChartType);           // protected constructor used by dynamic creation
    virtual ~CoverageChart();

	int		imChartType;
	CString omButtonText;
	CComboBox omComboBox;
	CCSButtonCtrl omButton2;
	CCSButtonCtrl omButton3;
	CCSButtonCtrl omButton4;
	CCSButtonCtrl omButton6;
	CCSButtonCtrl omAutoCovRel;
	CCSButtonCtrl omAutoCovDis;
	CCSButtonCtrl omDemUpdate;
	CCSButtonCtrl omBCStatus;	
	CButton		  omAdditiveCB;
	CButton		  omIgnoreBreakCB;
	CButton		  omShiftRoster;
	CButton		  omDummyShift;
	CButton		  omStaticShift;
	CButton		  omDynamicShift;
	CResultTable *pomResultTable;
	CCSButtonCtrl omExcel;

	

// Operations
public:
    int GetHeight();
    int GetState(void) { return imState; };
    void SetState(int ipState) { imState = ipState; };
    
    CCSTimeScale *GetTimeScale(void) { return pomTimeScale; };
    void SetTimeScale(CCSTimeScale *popTimeScale)
    {
        pomTimeScale = popTimeScale;
    };

    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };

    bool bmAdditive;
    CoverageDiagramViewer *GetViewer(void) { return pomViewer; };
    int GetGroupNo() { return imGroupNo; };
    void SetViewer(CoverageDiagramViewer *popViewer, int ipGroupNo)
    {
        pomViewer = popViewer;
        imGroupNo = ipGroupNo;
    };

    void SetMarkTime(CTime opStatTime, CTime opEndTime);
    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };
	void SetTimeFrame(CTime opStartTime, CTime opEndTime)
	{
		omStartTime = opStartTime;
		omEndTime = opEndTime;
	}

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };


	void ChangeToAutoCov(bool bpAutoCov);

	void CoverageChart::DeaktivateCombobox(bool blDeaktivate);

    CoverageGantt *GetGanttPtr(void) { return &omGantt; };
    CCSButtonCtrl  *GetChartButtonPtr(void) { return &omButton; };
    CCS3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };
	CCS3DStatic *GetCountTextPtr(void) { return pomCountText; };

//@ManMemo:SetPIdxFieldText 
 /*@Doc:
	sets the productivity index into the static field 
	*/
	void SetPIdxFieldText();

//@ManMemo:SetReleaseBtnRed 
 /*@Doc:
	sets the Release button red
	bpSetRed = true ->Set button Red
	bpSetRed = false ->Set button Gray
	*/
	void SetReleaseBtnRed(bool bpSetRed);
	void SetUpdateBtnRed(bool bpSetRed);

	bool IsBreakOptActive(void)	{ return bmOptBreakIsActive; };

	CString GetActiveViewName();
	CString GetActiveViewType();

	CTime SetDefaultVafr(CTime &ropVafr);

	void enableDisableUpdateDeleteButton(BOOL b);


// Overrides
public:
	//@ManMemo:ReloadComboView 
 /*@Doc:
	reloads the comobox in the chart
	*/
	void ReloadComboView();
// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CoverageChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnChartButton();
    afx_msg void OnChartButton2();
	afx_msg void OnButtonInsert();
	afx_msg void OnButtonUpdate();
	afx_msg void OnButtonDelete();
	afx_msg void OnChartButton4();
	afx_msg void OnChartButton5();
	afx_msg void OnChartButton3();
	afx_msg void OnChartButton6();
	afx_msg void OnAutoCovRelease();
	afx_msg void OnAutoCovDiscard();
	afx_msg void OnChartButton7();
    afx_msg LONG OnChartButtonRButtonDown(UINT, LONG);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg LONG OnReloadCombo(UINT wParam, LONG lParam);
	afx_msg LONG OnUpdateCombo(UINT wParam, LONG lParam);
	afx_msg void OnMenuAssign();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSelChangeCB();
	afx_msg void OnDemUpdate();
	afx_msg void OnExcelButton();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    
protected:
    int imState;
    int imHeight;
    
    CCSButtonCtrl omButton;
    CCS3DStatic *pomTopScaleText;
	CCS3DStatic *pomPIdxText;
	CCS3DStatic *pomCountText;
    
    CCSTimeScale *pomTimeScale;
    CStatusBar *pomStatusBar;
    CoverageDiagramViewer *pomViewer;
    int imGroupNo;
    CTime omStartTime;
	CTime omEndTime;
    CTimeSpan omInterval;
    CoverageGantt omGantt;
	CCoverageGraphicWnd omGraphic;
private:    
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
    int imStartTopScaleTextPos;
    int imStartVerticalScalePos;
	int CalculateMaxWidth();

	bool bmIsInit;

	bool bmNoReload;

	bool bmOptBreakIsActive;
	bool bmReleaseIsRed;
	bool bmUpdateIsRed;
	bool bmOperEnabled;
	void SetStaticFieldText();

	void Export(const CString& opExportName);
	CString CreateFileName();
	bool CreateExcelFile(const CString& ropExcelFileName,const CString& ropDelimiter);

// Drag-and-drop section
public:
    CCSDragDropCtrl m_ChartWindowDragDrop;
    CCSDragDropCtrl m_ChartButtonDragDrop;
    CCSDragDropCtrl m_CountTextDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;

//-DTT Jul.22-----------------------------------------------------------
// No more routine ProcessPartTimeAssignment(), we use a new dialog in
// file "dassignp.h" instead.
//	void ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//  void ProcessPartTimeAssignment(CCSDragDropCtrl *popDragDropCtrl);
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
	LONG ProcessDropFlightBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
//----------------------------------------------------------------------
};

/////////////////////////////////////////////////////////////////////////////

#endif // __COVERAGECHART_
