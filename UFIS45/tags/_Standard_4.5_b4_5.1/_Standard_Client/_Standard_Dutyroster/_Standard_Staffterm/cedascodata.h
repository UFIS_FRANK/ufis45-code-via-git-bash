// CedaScoData.h

#ifndef __CEDAPSCODATA__
#define __CEDAPSCODATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

// COMMANDS FOR MEMBER-FUNCTIONS
#define SCO_SEND_DDX	(true)
#define SCO_NO_SEND_DDX	(false)

//---------------------------------------------------------------------------------------------------------

struct SCODATA 
{
	long			Urno;
	long			Surn;
	char			Code[5+2];
	COleDateTime	Vpfr;
	COleDateTime	Vpto;
	char			Cweh[4+2];
	char			Stsc[2+2];
	char			Zifi[1+2];

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SCODATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Vpfr.SetStatus(COleDateTime::invalid);
		Vpto.SetStatus(COleDateTime::invalid);
	}

}; // end SCODataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaScoData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<SCODATA> omData;

	char pcmListOfFields[2048];
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaScoData();
	~CedaScoData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SCODATA *prpSco);
	bool InsertInternal(SCODATA *prpSco);
	bool Update(SCODATA *prpSco);
	bool UpdateInternal(SCODATA *prpSco);
	bool Delete(long lpUrno);
	bool DeleteInternal(SCODATA *prpSco);
	bool ReadSpecialData(CCSPtrArray<SCODATA> *popSco,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SCODATA *prpSco);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SCODATA *GetScoByUrno(long lpUrno);
	void GetScoBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SCODATA> *popScoData);
	void GetScoBySurnWithTime(long lpSurn,COleDateTime opStart,COleDateTime opEnd,CCSPtrArray<SCODATA> *popScoData);
	CString GetCotBySurnWithTime(long lpSurn, COleDateTime opDate);
	//CString GetCotBySurnWithTime(long lpSurn, COleDateTime opDate, CCSPtrArray<SCODATA> *popScoData);
	CString GetCotWithTime(COleDateTime opDate, CCSPtrArray<SCODATA> *popScoData);
	long GetScoUrnoWithTime(COleDateTime opDate, CCSPtrArray<SCODATA> *popScoData);
	CString GetScoByCotWithTime(CStringArray *popCot, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SCODATA> *popScoData);
	bool ReadScoData();

	// Private methods
private:
    void PrepareScoData(SCODATA *prpScoData);

protected:
	
};

//---------------------------------------------------------------------------------------------------------

//extern CedaScoData ogScoData;

#endif //__CEDAPSCODATA__
