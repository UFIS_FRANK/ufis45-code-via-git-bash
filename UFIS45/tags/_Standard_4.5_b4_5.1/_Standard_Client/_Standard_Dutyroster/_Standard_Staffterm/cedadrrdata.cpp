// CedaDrrData.cpp - Klasse f�r die Handhabung von Drr-Daten
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaDrrData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessDrrCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_Bud_CHANGE, BC_DRR_NEW und BC_DRR_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaDrrData::ProcessDrrBc() der entsprechenden 
//	Instanz von CedaDrrData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDrrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaDrrData *polDrrData = (CedaDrrData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polDrrData->ProcessDrrBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	AfxGetApp()->DoWaitCursor(-1);
}

static int CompareStfuAndTimes(const DRRDATA **e1, const DRRDATA **e2);
static int CompareTimes(const DRRDATA **e1, const DRRDATA **e2);

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDrrData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaDrrData::CedaDrrData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Drr-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(DRRDATA, DrrDataRecInfo)
		FIELD_CHAR_TRIM	(Avfr,"AVFR")	// Anwesend von
		FIELD_CHAR_TRIM	(Avto,"AVTO")	// Anwesend bis
		FIELD_CHAR_TRIM	(Bkdp,"BKDP")	// 1.Pause bezahlt
		FIELD_CHAR_TRIM	(Bsdu,"BSDU")	// Urno BSDTAB
		FIELD_CHAR_TRIM	(Bufu,"BUFU")	// Funknummer
		FIELD_OLEDATE	(Cdat,"CDAT")	// Erstellungsdatum
		FIELD_CHAR_TRIM	(Drrn,"DRRN")	// Schichtnummer
		FIELD_CHAR_TRIM	(Drsf,"DRSF")	// Flags f�r Zusatzinfo
		FIELD_CHAR_TRIM	(Expf,"EXPF")	// Export File
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Home Airport
		FIELD_OLEDATE	(Lstu,"LSTU")	// �nderungsdatum
		FIELD_CHAR_TRIM	(Rema,"REMA")	// Remarks
		FIELD_CHAR_TRIM	(Rosl,"ROSL")	// Planungsstufe
		FIELD_CHAR_TRIM	(Ross,"ROSS")	// Status der Planungsstufe
		FIELD_CHAR_TRIM	(Sbfr,"SBFR")	// Pausenlage von
		FIELD_CHAR_TRIM	(Sblp,"SBLP")	// 2.Pause
		FIELD_CHAR_TRIM	(Sblu,"SBLU")	// 1.Pause
		FIELD_CHAR_TRIM	(Sbto,"SBTO")	// Pausenlage bis
		FIELD_CHAR_TRIM	(Scod,"SCOD")	// Schichtcode
		FIELD_CHAR_TRIM	(Sday,"SDAY")	// Schichttag
		FIELD_CHAR_TRIM	(Stfu,"STFU")	// Urno STFTAB
		FIELD_LONG   	(Urno,"URNO")	// Urno
		FIELD_CHAR_TRIM	(Usec,"USEC")	// Ersteller
		FIELD_CHAR_TRIM	(Useu,"USEU")	// Anwender letzte �nderung
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(DrrDataRecInfo)/sizeof(DrrDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DrrDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Drr setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"AVFR,AVTO,BKDP,BSDU,BUFU,CDAT,DRRN,DRSF,EXPF,HOPO,LSTU,REMA,ROSL,ROSS,SBFR,SBLP,SBLU,SBTO,SCOD,SDAY,STFU,URNO,USEC,USEU");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Drrs initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaDrrData::~CedaDrrData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrrData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaDrrData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Drr-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_DRR_CHANGE, CString("CedaDrrData"), CString("BC_DRR_CHANGE"),        ProcessDrrCf);
	ogDdx.Register((void *)this,BC_DRR_NEW, CString("CedaDrrData"), CString("BC_DRR_NEW"),		   ProcessDrrCf);
	ogDdx.Register((void *)this,BC_DRR_DELETE, CString("CedaDrrData"), CString("BC_DRR_DELETE"),        ProcessDrrCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaDrrData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadDrrByUrno: liest den Drr-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaDrrData::ReadDrrByUrno(long lpUrno, DRRDATA *prpDrr)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	DRRDATA *prlDrr = new DRRDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpDrr) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlDrr;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrrData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRRDATA *prlDrr = new DRRDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlDrr);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlDrr->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlDrr, DRR_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlDrr;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlDrr;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen Drr
	TRACE("Read-Drr: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpDrr> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::Insert(DRRDATA *prpDrr, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpDrr->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpDrr) == false) return false;
	// Broadcast DRR_NEW abschicken
	InsertInternal(prpDrr,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::InsertInternal(DRRDATA *prpDrr, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpDrr);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpDrr->Urno,prpDrr);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,DRR_NEW,(void *)prpDrr);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpDrr->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::Update(DRRDATA *prpDrr,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetDrrByUrno(prpDrr->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpDrr->IsChanged == DATA_UNCHANGED)
		{
			prpDrr->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpDrr) == false) return false; 
		// Broadcast DRR_CHANGE versenden
		UpdateInternal(prpDrr);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::UpdateInternal(DRRDATA *prpDrr, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// Drr in lokaler Datenhaltung suchen
	DRRDATA *prlDrr = GetDrrByKey(prpDrr->Sday,prpDrr->Stfu,prpDrr->Budn,prpDrr->Rosl);
	// Drr gefunden?
	if (prlDrr != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlDrr = *prpDrr;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",DRR_CHANGE);
		ogDdx.DataChanged((void *)this,DRR_CHANGE,(void *)prpDrr);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaDrrData::DeleteInternal(DRRDATA *prpDrr, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,DRR_DELETE,(void *)prpDrr);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpDrr->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpDrr->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpDrr> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrrData::Delete(DRRDATA *prpDrr, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpDrr->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpDrr)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpDrr,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessDrrBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDrrData::ProcessDrrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaDrrData::ProcessDrrBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	DRRDATA *prlDrr = NULL;

	switch(ipDDXType)
	{
	case BC_DRR_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlDrr = new DRRDATA;
			GetRecordFromItemList(prlDrr,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlDrr, DRR_SEND_DDX);
		}
		break;
	case BC_DRR_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrr = GetDrrByUrno(llUrno);
			if(prlDrr != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlDrr,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlDrr);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_DRR_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrr = GetDrrByUrno(llUrno);
			if (prlDrr != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlDrr);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaDrrData::ProcessDrrBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetDrrByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRRDATA *CedaDrrData::GetDrrByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	DRRDATA *prpDrr;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpDrr) == TRUE)
	{
		return prpDrr;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpDrr>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaDrrData::Save(DRRDATA *prpDrr)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpDrr->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDrr->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDrr);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDrr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrr->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrr);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDrr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrr->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaDrrData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaDrrData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyDrrValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaDrrData::CopyDrr(DRRDATA* popDrrDataSource,DRRDATA* popDrrdataTarget)
{
	// Daten kopieren
	/*strcpy(popDrrDataTarget->Rema,popDrrDataSource->Rema);
	strcpy(popDrrDataTarget->Sdac,popDrrDataSource->Sdac);
	popDrrDataTarget->Abfr = popDrrDataSource->Abfr;
	popDrrDataTarget->Abto = popDrrDataSource->Abto;*/
}

bool CedaDrrData::ReadDrrData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRRDATA *prlDrr = new DRRDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlDrr);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlDrr, DRR_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlDrr;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen Drr
	TRACE("Read-Drr: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

DRRDATA* CedaDrrData::GetDrrByStfuWithTimeAndRosl(CString opStfu, CString opTime, CString opRosl)
{
CCS_TRY
	DRRDATA  *prlDrr;

	//CCSPtrArray<DRRDATA> olDrrData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDrr);
		if((prlDrr->Stfu == opStfu)){
			if(prlDrr->Sday == opTime && prlDrr->Rosl == opRosl)
				//olDrrData.Add(prlDrr); 
				return prlDrr;
		}
	}
	return NULL;
CCS_CATCH_ALL
	return NULL;
}

int CedaDrrData::GetDrrByStfuWithTimeAndRosl(CString opStfu, CString opTime, CString opRosl, CCSPtrArray<DRRDATA> *popDrrData)
{
CCS_TRY
	DRRDATA  *prlDrr;
	POSITION rlPos;

	popDrrData->RemoveAll();

	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDrr);
		if((prlDrr->Stfu == opStfu)){
			if(prlDrr->Sday == opTime && prlDrr->Rosl == opRosl)
				popDrrData->Add(prlDrr); 
		}
	}
	popDrrData->Sort(CompareTimes);

	return popDrrData->GetSize();
CCS_CATCH_ALL
	return 0;
}

static int CompareStfuAndTimes(const DRRDATA **e1, const DRRDATA **e2)
{
	int ilCompareResult = 0;

	     if((**e1).Stfu>(**e2).Stfu) ilCompareResult = 1;
	else if((**e1).Stfu<(**e2).Stfu) ilCompareResult = -1;

	if(ilCompareResult == 0)
	{
		     if((**e1).Sday>(**e2).Sday) ilCompareResult = 1;
		else if((**e1).Sday<(**e2).Sday) ilCompareResult = -1;
	}
	return ilCompareResult;
}

static int CompareTimes(const DRRDATA **e1, const DRRDATA **e2)
{
	/*if((**e1).Avfr>(**e2).Avfr)
		return -1;
	else
		return 1;*/

	int ilCompareResult = 0;

	     if((**e1).Avfr>(**e2).Avfr) ilCompareResult = -1;
	else if((**e1).Avfr<(**e2).Avfr) ilCompareResult = 1;

	/*if(ilCompareResult == 0)
	{
		     if((**e1).Avfr>(**e2).Avfr) ilCompareResult = -1;
		else if((**e1).Avfr<(**e2).Avfr) ilCompareResult = 1;
	}*/
	return ilCompareResult;
}
