// MultipleInput1.cpp : implementation file
//

#include <stdafx.h>
#include <staffterm.h>
#include <MultipleInput.h>
#include <CedaWisData.h>
#include <CedaDrwData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMultipleInput dialog


CMultipleInput::CMultipleInput(CWnd* pParent /*=NULL*/)
	: CDialog(CMultipleInput::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMultipleInput)
	//}}AFX_DATA_INIT
	StfUrno = 0;
}


void CMultipleInput::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMultipleInput)
	DDX_Control(pDX, IDC_STATIC_PRIO, m_Static_Prio);
	DDX_Control(pDX, IDC_EDIT_REMARKS, m_Edit_Remarks);
	DDX_Control(pDX, IDC_CHECK_WEDNESDAY, m_Check_Wednesday);
	DDX_Control(pDX, IDC_CHECK_TUESDAY, m_Check_Tuesday);
	DDX_Control(pDX, IDC_CHECK_THURSDAY, m_Check_Thursday);
	DDX_Control(pDX, IDC_CHECK_SUNDAY, m_Check_Sunday);
	DDX_Control(pDX, IDC_CHECK_SATURDAY, m_Check_Saturday);
	DDX_Control(pDX, IDC_CHECK_MONDAY, m_Check_Monday);
	DDX_Control(pDX, IDC_CHECK_FRIDAY, m_Check_Friday);
	DDX_Control(pDX, IDC_CHECK_EVERYDAY, m_Check_EveryDay);
	DDX_Control(pDX, IDC_COMBO_PRIO, m_Combo_Prio);
	DDX_Control(pDX, IDC_COMBO_CODE, m_Combo_Code);
	DDX_Control(pDX, IDC_EDIT_TO, m_Edit_To);
	DDX_Control(pDX, IDC_EDIT_FROM, m_Edit_From);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMultipleInput, CDialog)
	//{{AFX_MSG_MAP(CMultipleInput)
	ON_BN_CLICKED(IDC_CHECK_EVERYDAY, OnCheckEveryday)
	ON_BN_CLICKED(IDC_CHECK_FRIDAY, OnCheckFriday)
	ON_BN_CLICKED(IDC_CHECK_MONDAY, OnCheckMonday)
	ON_BN_CLICKED(IDC_CHECK_SATURDAY, OnCheckSaturday)
	ON_BN_CLICKED(IDC_CHECK_SUNDAY, OnCheckSunday)
	ON_BN_CLICKED(IDC_CHECK_THURSDAY, OnCheckThursday)
	ON_BN_CLICKED(IDC_CHECK_TUESDAY, OnCheckTuesday)
	ON_BN_CLICKED(IDC_CHECK_WEDNESDAY, OnCheckWednesday)
	ON_CBN_SELCHANGE(IDC_COMBO_CODE, OnSelchangeComboCode)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMultipleInput message handlers

BOOL CMultipleInput::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu = LoadStg(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
		pSysMenu->DeleteMenu(SC_CLOSE, MF_BYCOMMAND);
		pSysMenu->DeleteMenu(SC_MAXIMIZE, MF_BYCOMMAND);
	}
	
	// TODO: Add extra initialization here
	SetDlgItemText(IDC_STATIC_FROM, LoadStg(IDS_DATEFROM));
	SetDlgItemText(IDC_STATIC_TO, LoadStg(IDS_DATETO));
	SetDlgItemText(IDC_STATIC_CODE, LoadStg(IDS_CODE));
	SetDlgItemText(IDC_STATIC_PRIO, LoadStg(IDS_PRIO));
	SetDlgItemText(IDC_STATIC_REMARKS, LoadStg(IDS_REMARKS));
	SetDlgItemText(IDC_CHECK_MONDAY, LoadStg(IDS_MONDAY));
	SetDlgItemText(IDC_CHECK_TUESDAY, LoadStg(IDS_TUESDAY));
	SetDlgItemText(IDC_CHECK_WEDNESDAY, LoadStg(IDS_WEDNESDAY));
	SetDlgItemText(IDC_CHECK_THURSDAY, LoadStg(IDS_THURSDAY));
	SetDlgItemText(IDC_CHECK_FRIDAY, LoadStg(IDS_FRIDAY));
	SetDlgItemText(IDC_CHECK_SATURDAY, LoadStg(IDS_SATURDAY));
	SetDlgItemText(IDC_CHECK_SUNDAY, LoadStg(IDS_SUNDAY));
	SetDlgItemText(IDC_CHECK_EVERYDAY, LoadStg(IDS_EVERYDAY));
	SetDlgItemText(IDOK, LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));

	m_Edit_From.SetTextLimit(0, 10);
	m_Edit_From.SetTextErrColor(RED);
	m_Edit_From.SetTypeToDate(true);

	m_Edit_To.SetTextLimit(0, 10);
	m_Edit_To.SetTextErrColor(RED);
	m_Edit_To.SetTypeToDate(true);

	//m_Edit_Code.SetTextErrColor(RED);
	//m_Edit_Code.SetTypeToString(CString(""), 8, 0);

	//m_Edit_Prio.SetTextErrColor(RED);
	//m_Edit_Prio.SetTypeToInt(0, 4);

	m_Edit_Remarks.SetTextErrColor(RED);
	m_Edit_Remarks.SetTypeToString(CString(""), 60, 0);
	//m_Edit_Remarks.EnableWindow(false);

	CString csWish, csCode;
	
	//uhi 10.04.01

	/*for (int n=0; n<ogWisData.omData.GetSize(); n++){
		csCode = ogWisData.omData[n].Wisc;
		if (!csCode.IsEmpty()){
			m_Combo_Code.AddString(csCode);
		}
	}*/

	m_Combo_Code.SetFont(&ogCourier_Regular_10);
	FillCodeCombo();

	//uhi 23.7.01 Prio von 1 - 3
	for (int n=1; n<4; n++){
		csCode.Format("%i", n);
		m_Combo_Prio.AddString(csCode);
	}

	m_Check_EveryDay.SetCheck(1);

	//uhi 22.01.01
	if(bGav ==false){
		m_Combo_Prio.ShowWindow(SW_HIDE);
		m_Static_Prio.ShowWindow(SW_HIDE);
	}
	else{
		m_Combo_Prio.ShowWindow(SW_SHOWNORMAL);
		m_Static_Prio.ShowWindow(SW_SHOWNORMAL);
	}

	SetWindowText(LoadStg(IDS_MULTIPLE_INPUT));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMultipleInput::OnOK() 
{
	
	CString csWish, csPrio, csFrom, csTo;
	CString csRems, csSday, csStfu, csPrioAlt, csRemsAlt, csWishAlt;
	
	COleDateTime olDate, olFrom, olTo;
	COleDateTimeSpan olOneDay(1, 0, 0, 0);

	int iWeekday;
	
	bool bSave;

	DRWDATA *pDrw;
	WISDATA *pWis;

	CString MESSAGE_BOX_CAPTION	= LoadStg(IDS_STAFFTERM);


	// TODO: Add extra validation here
	if(m_Edit_From.GetStatus() == false || m_Edit_To.GetStatus() == false){ // || m_Edit_Prio.GetStatus() == false)
		MessageBox(LoadStg(IDS_INVALID_DATE),MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
		return;
	}

	//m_Edit_Code.GetWindowText(csWish);
	
	m_Combo_Code.GetWindowText(csWish);
	csWish = csWish.Left(8);
	csWish.TrimRight();
	//Wunschcode pr�fen
	if (!csWish.IsEmpty()){
		pWis = ogWisData.GetWisByKey(csWish);
		if (pWis == NULL){
			MessageBox(LoadStg(IDS_INVALID_CODE),MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
			return;
		}
	}
	else{
		MessageBox(LoadStg(IDS_INVALID_CODE),MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
		return;
	}

	m_Combo_Prio.GetWindowText(csPrio);
	//Priorit�t pr�fen
	if (!csPrio.IsEmpty()){
		if (csPrio != CString("0") && csPrio != CString("1") && csPrio != CString("2") && csPrio != CString("3")){
			MessageBox(LoadStg(IDS_INVALID_PRIO),MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
			return;
		}
	}
	//else
	//	return;

	//Kein Tag ausgew�hlt
	if(m_Check_Monday.GetCheck() == 0 && m_Check_Tuesday.GetCheck() == 0 && m_Check_Wednesday.GetCheck() == 0 &&
		m_Check_Thursday.GetCheck() == 0 && m_Check_Friday.GetCheck() == 0 && m_Check_Saturday.GetCheck() == 0 &&
		m_Check_Sunday.GetCheck() == 0 && m_Check_EveryDay.GetCheck() == 0)
		return;

	//Eingabe erlaubt?
	m_Edit_From.GetWindowText(csFrom);
	m_Edit_To.GetWindowText(csTo);

	olFrom = OleDateStringToDate(csFrom);
	olTo = OleDateStringToDate(csTo);

	CString olValidInput = ctValidInput.Format("%d.%m.%Y");
	if(ctValidInput>olFrom)
		//Keine Eingabe
		MessageBox(LoadStg(IDS_STRING1020) + olValidInput,MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
	
	//Speichern DRWTAB
	m_Edit_Remarks.GetWindowText(csRems); 
	csStfu.Format("%d", StfUrno);

	olDate = olFrom;
	for (; ; ){
		if(olDate > olTo)
			break;
		
		bSave = false;
		if(m_Check_EveryDay.GetCheck() == 0){
			iWeekday = olDate.GetDayOfWeek();
			switch(iWeekday){
			case 1: //Sunday
				if(m_Check_Sunday.GetCheck() == 1)
					bSave = true;
				break;
			case 2: //Monday
				if(m_Check_Monday.GetCheck() == 1)
					bSave = true;
				break;
			case 3: //Tuesday
				if(m_Check_Tuesday.GetCheck() == 1)
					bSave = true;
				break;
			case 4: //Wednesday
				if(m_Check_Wednesday.GetCheck() == 1)
					bSave = true;
				break;
			case 5: //Thursday
				if(m_Check_Thursday.GetCheck() == 1)
					bSave = true;
				break;
			case 6: //Friday
				if(m_Check_Friday.GetCheck() == 1)
					bSave = true;
				break;
			case 7: //Saturday
				if(m_Check_Saturday.GetCheck() == 1)
					bSave = true;
				break;
			default:
				break;
			}
		}
		else
			bSave = true;

		if(bSave == true){
			csSday = olDate.Format("%Y%m%d");
			if (!csWish.IsEmpty()){
				//if(csPrio.IsEmpty())
				//	csPrio = CString("0");
				pDrw = NULL;
				pDrw = ogDrwData.GetDrwByStfuWithTime(csStfu, csSday);
				if (pDrw != NULL){
					csPrioAlt = pDrw->Prio;
					csWishAlt = pDrw->Wisc;
					csRemsAlt = pDrw->Rems;
					if(csPrio != csPrioAlt || csWish != csWishAlt || csRems != csRemsAlt){
						// �nderungsflag setzen
						pDrw->IsChanged = DATA_CHANGED;
						// �nderungsdatum einstellen
						pDrw->Lstu = COleDateTime::GetCurrentTime();
						// Anwender einstellen
						strcpy(pDrw->Useu,pcgUser);
						//Priorit�t
						strcpy(pDrw->Prio, csPrio.GetBuffer(0));
						//Remarks Employee
						//if (!csRems.IsEmpty())
							strcpy(pDrw->Rems, csRems.GetBuffer(0));
						//Wunsch
						strcpy(pDrw->Wisc, csWish.GetBuffer(0));
						ogDrwData.Update(pDrw);
					}
				}
				else{
					//Neuer Satz
					DRWDATA *pDrw = new DRWDATA;
					// �nderungsflag setzen
					pDrw->IsChanged = DATA_NEW;
					// Erzeugungsdatum einstellen
					pDrw->Cdat = COleDateTime::GetCurrentTime();
					// Anwender (Ersteller) einstellen
					strcpy(pDrw->Usec,pcgUser);
					//n�chste freie Datensatz-Urno ermitteln und speichern
					pDrw->Urno = ogBasicData.GetNextUrno();
					//Home Airport
					strcpy(pDrw->Hopo,pcgHome);
					//Priorit�t
					strcpy(pDrw->Prio, csPrio.GetBuffer(0));
					//Remarks Employee
					if (!csRems.IsEmpty())
						strcpy(pDrw->Rems, csRems.GetBuffer(0));
					//Schichttag
					strcpy(pDrw->Sday, csSday.GetBuffer(0));
					//Urno Employee
					strcpy(pDrw->Stfu, csStfu.GetBuffer(0));
					//Wunsch
					strcpy(pDrw->Wisc, csWish.GetBuffer(0));
					ogDrwData.Insert(pDrw);
				}
			}
		}	
		olDate = olDate + olOneDay;
	}

	CDialog::OnOK();
}

void CMultipleInput::OnCheckEveryday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_EveryDay.GetCheck() == 1){
		m_Check_Monday.SetCheck(0);
		m_Check_Tuesday.SetCheck(0);
		m_Check_Wednesday.SetCheck(0);
		m_Check_Thursday.SetCheck(0);
		m_Check_Friday.SetCheck(0);
		m_Check_Saturday.SetCheck(0);
		m_Check_Sunday.SetCheck(0);
	}
}

void CMultipleInput::OnCheckFriday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_Friday.GetCheck() == 1)
		m_Check_EveryDay.SetCheck(0);
}

void CMultipleInput::OnCheckMonday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_Monday.GetCheck() == 1)
		m_Check_EveryDay.SetCheck(0);
}

void CMultipleInput::OnCheckSaturday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_Saturday.GetCheck() == 1)
		m_Check_EveryDay.SetCheck(0);
}

void CMultipleInput::OnCheckSunday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_Sunday.GetCheck() == 1)
		m_Check_EveryDay.SetCheck(0);
}

void CMultipleInput::OnCheckThursday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_Thursday.GetCheck() == 1)
		m_Check_EveryDay.SetCheck(0);
}

void CMultipleInput::OnCheckTuesday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_Tuesday.GetCheck() == 1)
		m_Check_EveryDay.SetCheck(0);
}

void CMultipleInput::OnCheckWednesday() 
{
	// TODO: Add your control notification handler code here
	if(m_Check_Wednesday.GetCheck() == 1)
		m_Check_EveryDay.SetCheck(0);
}

void CMultipleInput::FillCodeCombo()
{
	CString olLine, olWish, olDescription, olEmpty;
	
	olEmpty = "";
	olLine.Format("%-8s   %-40s", olEmpty, olEmpty);
	m_Combo_Code.AddString(olLine);
	
	for (int n=0; n<ogWisData.omData.GetSize(); n++){
		olWish = ogWisData.omData[n].Wisc;
		olDescription = ogWisData.omData[n].Wisd;
		if (!olWish.IsEmpty()){
			olLine.Format("%-8s   %-40s", olWish, olDescription);
			m_Combo_Code.AddString(olLine);
		}
	}

	m_Combo_Code.SetCurSel(0);
}

void CMultipleInput::OnSelchangeComboCode() 
{
	// TODO: Add your control notification handler code here
	//uhi 25.7.01 PRF 252
	/*if(m_Combo_Code.GetCurSel() != CB_ERR){
		m_Combo_Prio.EnableWindow(true);
		m_Edit_Remarks.EnableWindow(true);
		m_Check_EveryDay.EnableWindow(true);
		m_Check_Monday.EnableWindow(true);
		m_Check_Tuesday.EnableWindow(true);
		m_Check_Wednesday.EnableWindow(true);
		m_Check_Thursday.EnableWindow(true);
		m_Check_Friday.EnableWindow(true);
		m_Check_Saturday.EnableWindow(true);
		m_Check_Sunday.EnableWindow(true);
	}
	else{
		m_Combo_Prio.EnableWindow(false);
		m_Edit_Remarks.EnableWindow(false);
		m_Check_EveryDay.EnableWindow(false);
		m_Check_Monday.EnableWindow(false);
		m_Check_Tuesday.EnableWindow(false);
		m_Check_Wednesday.EnableWindow(false);
		m_Check_Thursday.EnableWindow(false);
		m_Check_Friday.EnableWindow(false);
		m_Check_Saturday.EnableWindow(false);
		m_Check_Sunday.EnableWindow(false);
	}*/
	
}

