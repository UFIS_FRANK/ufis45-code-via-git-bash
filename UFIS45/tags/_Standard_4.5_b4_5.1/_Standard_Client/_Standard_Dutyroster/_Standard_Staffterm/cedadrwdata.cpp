// CedaDrwData.cpp - Klasse f�r die Handhabung von Drw-Daten
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaDrwData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessDrwCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_Bud_CHANGE, BC_DRW_NEW und BC_DRW_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaDrwData::ProcessDrwBc() der entsprechenden 
//	Instanz von CedaDrwData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessDrwCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaDrwData *polDrwData = (CedaDrwData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polDrwData->ProcessDrwBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	AfxGetApp()->DoWaitCursor(-1);
}

static int CompareStfuAndTimes(const DRWDATA **e1, const DRWDATA **e2);

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaDrwData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaDrwData::CedaDrwData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Drw-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    BEGIN_CEDARECINFO(DRWDATA, DrwDataRecInfo)
		FIELD_OLEDATE	(Cdat,"CDAT")	// Erstellungsdatum
		FIELD_CHAR_TRIM	(Clos,"CLOS")	// Record Locked ('B')
		FIELD_CHAR_TRIM	(Hopo,"HOPO")	// Home Airport
		FIELD_OLEDATE	(Lstu,"LSTU")	// �nderungsdatum
		FIELD_CHAR_TRIM	(Prio,"PRIO")	// Priority
		FIELD_CHAR_TRIM	(Reme,"REME")	// External Remark Disponent
		FIELD_CHAR_TRIM	(Remi,"REMI")	// Internal Remark Disponent
		FIELD_CHAR_TRIM	(Rems,"REMS")	// Remark Employee
		FIELD_CHAR_TRIM	(Sday,"SDAY")	// Shift Day
		FIELD_CHAR_TRIM	(Stfu,"STFU")	// Urno Employee
		FIELD_LONG   	(Urno,"URNO")	// Urno
		FIELD_CHAR_TRIM	(Usec,"USEC")	// Ersteller
		FIELD_CHAR_TRIM	(Useu,"USEU")	// Anwender letzte �nderung
		FIELD_CHAR_TRIM	(Wisc,"WISC")	// Wish
	END_CEDARECINFO

	// Infostruktur kopieren
    for (int i = 0; i < sizeof(DrwDataRecInfo)/sizeof(DrwDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DrwDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Drw setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"CDAT,CLOS,HOPO,LSTU,PRIO,REME,REMI,REMS,SDAY,STFU,URNO,USEC,USEU,WISC");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Drws initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaDrwData::~CedaDrwData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaDrwData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaDrwData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Drw-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_DRW_CHANGE, CString("CedaDrwData"), CString("BC_DRW_CHANGE"),        ProcessDrwCf);
	ogDdx.Register((void *)this,BC_DRW_NEW, CString("CedaDrwData"), CString("BC_DRW_NEW"),		   ProcessDrwCf);
	ogDdx.Register((void *)this,BC_DRW_DELETE, CString("CedaDrwData"), CString("BC_DRW_DELETE"),        ProcessDrwCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaDrwData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadDrwByUrno: liest den Drw-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaDrwData::ReadDrwByUrno(long lpUrno, DRWDATA *prpDrw)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	DRWDATA *prlDrw = new DRWDATA;
	// und initialisieren
	if (GetBufferRecord(0,prpDrw) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlDrw;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaDrwData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRWDATA *prlDrw = new DRWDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlDrw);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlDrw->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlDrw, DRW_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlDrw;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlDrw;
		}*/
	} while (blMoreRecords);
	
	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();

	// Test: Anzahl der gelesenen Drw
	TRACE("Read-Drw: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpDrw> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrwData::Insert(DRWDATA *prpDrw, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpDrw->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpDrw) == false) return false;
	// Broadcast DRW_NEW abschicken
	InsertInternal(prpDrw,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrwData::InsertInternal(DRWDATA *prpDrw, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpDrw);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpDrw->Urno,prpDrw);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,DRW_NEW,(void *)prpDrw);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpDrw->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrwData::Update(DRWDATA *prpDrw,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetDrwByUrno(prpDrw->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpDrw->IsChanged == DATA_UNCHANGED)
		{
			prpDrw->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpDrw) == false) return false; 
		// Broadcast DRW_CHANGE versenden
		UpdateInternal(prpDrw);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrwData::UpdateInternal(DRWDATA *prpDrw, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
	// Drw in lokaler Datenhaltung suchen
/*	DRWDATA *prlDrw = GetDrwByStfuWithTime(prpDrw->Stfu, prpDrw->Sday); //,prpDrw->Stfu,prpDrw->Budn,prpDrw->Rosl);
	// Drw gefunden?
	if (prlDrw != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlDrw = *prpDrw;
	}*/

		// Broadcast senden, wenn gew�nscht
		if( bpSendDdx == true )
		{
//			TRACE("\nSending %d",DRW_CHANGE);
			ogDdx.DataChanged((void *)this,DRW_CHANGE,(void *)prpDrw);
		}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaDrwData::DeleteInternal(DRWDATA *prpDrw, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,DRW_DELETE,(void *)prpDrw);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpDrw->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpDrw->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpDrw> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaDrwData::Delete(DRWDATA *prpDrw, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpDrw->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpDrw)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpDrw,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessDrwBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaDrwData::ProcessDrwBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaDrwData::ProcessDrwBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	DRWDATA *prlDrw = NULL;

	switch(ipDDXType)
	{
	case BC_DRW_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlDrw = new DRWDATA;
			GetRecordFromItemList(prlDrw,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlDrw, DRW_SEND_DDX);
		}
		break;
	case BC_DRW_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrw = GetDrwByUrno(llUrno);
			if(prlDrw != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlDrw,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlDrw);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_DRW_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlDrw = GetDrwByUrno(llUrno);
			if (prlDrw != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlDrw);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaDrwData::ProcessDrwBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetDrwByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

DRWDATA *CedaDrwData::GetDrwByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	DRWDATA *prpDrw;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpDrw) == TRUE)
	{
		return prpDrw;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpDrw>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaDrwData::Save(DRWDATA *prpDrw)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpDrw->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDrw->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDrw);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDrw->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrw->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDrw);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDrw->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDrw->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaDrwData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaDrwData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyDrwValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaDrwData::CopyDrw(DRWDATA* popDrwDataSource,DRWDATA* popDrwdataTarget)
{
	// Daten kopieren
	/*strcpy(popDrwDataTarget->Rema,popDrwDataSource->Rema);
	strcpy(popDrwDataTarget->Sdac,popDrwDataSource->Sdac);
	popDrwDataTarget->Abfr = popDrwDataSource->Abfr;
	popDrwDataTarget->Abto = popDrwDataSource->Abto;*/
}

bool CedaDrwData::ReadDrwData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		DRWDATA *prlDrw = new DRWDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlDrw);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlDrw, DRW_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlDrw;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen Drw
	TRACE("Read-Drw: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

DRWDATA* CedaDrwData::GetDrwByStfuWithTime(CString opStfu, CString opTime)
{
CCS_TRY
	DRWDATA  *prlDrw = NULL;
	long llUrno;
	POSITION rlPos;

	for (rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; ){
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDrw);
		if(prlDrw != NULL && prlDrw->Stfu == opStfu && prlDrw->Sday == opTime)
				return prlDrw;
	}
	return NULL;
CCS_CATCH_ALL
	return NULL;
}

static int CompareStfuAndTimes(const DRWDATA **e1, const DRWDATA **e2)
{
	int ilCompareResult = 0;

	     if((**e1).Stfu>(**e2).Stfu) ilCompareResult = 1;
	else if((**e1).Stfu<(**e2).Stfu) ilCompareResult = -1;

	if(ilCompareResult == 0)
	{
		     if((**e1).Sday>(**e2).Sday) ilCompareResult = 1;
		else if((**e1).Sday<(**e2).Sday) ilCompareResult = -1;
	}
	return ilCompareResult;
}
