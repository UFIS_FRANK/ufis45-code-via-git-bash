// GridControl.h: interface for the CGridControl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_)
#define AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_

#include <gxall.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct DOONMOUSECLICK
{
	UINT	iMsg;
	WPARAM  iWparam;
	bool	bOnlyInside; 	
};

#define WM_GRID_LBUTTONDBLCLK  WM_USER+800
#define WM_GRID_LBUTTONCLK  WM_USER+801
#define WM_GRID_LBUTTONDOWN  WM_USER+802
#define WM_GRID_DRAGBEGIN  WM_USER+803

class CGridControl : public CGXGridWnd  
{
public:
	int GetColWidth(ROWCOL nCol);
	CGridControl ();
	CGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows );
	virtual ~CGridControl();

//Implementation
	BOOL SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) ;
	const CString& GetValue ( ROWCOL nRow, ROWCOL nCol ) ;
	virtual BOOL EnableGrid ( BOOL enable );
	void CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
						   bool bpResetValues=true );
	void CopyStyleLastLineFromPrev ( bool bpResetValues=true );
	void EnableAutoGrow ( bool enable=true );
	void EnableSorting ( bool enable=true );
	void SetLbDblClickAction ( UINT ipMsg, WPARAM ipWparam, bool bpOnlyInside=true ); 
	void SetDirtyFlag ( bool dirty=true );
	bool IsGridDirty ();
	void SetParentWnd(CWnd *popWnd) {pomWnd = popWnd;}
	void SetEnableDnD(bool bpEnableDnD) {bmIsDnDEnabled = bpEnableDnD;}
	void SetToolTipArray(CStringArray *popToolTipArray) {pomToolTipArray = popToolTipArray;}
	int FindRowByUrnoDataPointer(long lpUrno);
	void SetSelection(POSITION SelRectId, ROWCOL nTop = 0, ROWCOL nLeft = 0, ROWCOL nBottom = 0, ROWCOL nRight = 0);
	void SetColCheckbox(ROWCOL nCol);

protected:
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	virtual void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);
	virtual BOOL InsertBottomRow ();
	BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnTextNotFound(LPCTSTR);
	BOOL OnDeleteCell(ROWCOL nRow, ROWCOL nCol);

	//uhi 26.7.01
	BOOL OnValidateCell(ROWCOL nRow, ROWCOL nCol);

	virtual bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
//	BOOL GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt = gxCopy, int nType = 0);

//	BOOL OnSelDragMove(ROWCOL nRow1, ROWCOL nCol1, ROWCOL nRow, ROWCOL nCol);


//{{AFX_MSG(CGridControl)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint pt);
//	afx_msg void OnKillFocus ( CWnd* pNewWnd );
	//}}AFX_MSG
DECLARE_MESSAGE_MAP()

	
protected:
	UINT	umID;
	CWnd	*pomWnd;
	bool    bmAutoGrow;
	bool    bmSortEnabled;
	bool    bmSortAscend;
	DOONMOUSECLICK smLButtonDblClick;	
	bool	bmIsDirty ;
	bool	bmIsDnDEnabled;
	CStringArray	*pomToolTipArray;
};

struct GRIDNOTIFY
{
	UINT	idc;
	ROWCOL	row;
	ROWCOL	col;
	ROWCOL	headerrows;
	ROWCOL	headercols;
	POINT	point;
	CString value1;
	void* value2;
	CGridControl* source;
};


class CTitleGridControl : public CGridControl
{
public:
	CTitleGridControl ();
	CTitleGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows );
	virtual ~CTitleGridControl();
//Implementation
	void SetTitle ( CString &opTitle );
	CString GetTitle ();
	const CString &GetValue ( ROWCOL nRow, ROWCOL nCol ) ;
	BOOL SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) ;
	BOOL EnableGrid ( BOOL enable );
	void RemoveOneRow ( ROWCOL nRow );
	bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
protected:
	BOOL InsertBottomRow ();
	void SetRowHeadersText ();
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
private:
	CString omTitle;
};

#endif // !defined(AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_)
