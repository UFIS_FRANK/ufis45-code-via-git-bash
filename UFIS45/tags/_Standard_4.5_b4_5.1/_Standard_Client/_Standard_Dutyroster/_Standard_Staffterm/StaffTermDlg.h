// StaffTermDlg.h : header file
//

#if !defined(AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
#define AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_

#include <GridControl.h>
#include <CCSEdit.h>
#include <CedaDrwData.h>
#include <CedaDrrData.h>
#include <CedaAccData.h>
#include <MultipleInput.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MINROWS 1

class CGridControl;
/////////////////////////////////////////////////////////////////////////////
// CStaffTermDlg dialog
class CStaffTermDlg : public CDialog
{
// Construction
public:
	
	CMapStringToPtr omHolidayKeyMap;
	COleDateTime ctValidInput;
	//uhi 19.9.00
	bool bGav;
	CString Name;
	void ProcessDrrDelete(DRRDATA *prpDrr);
	void ProcessDrwDelete(DRWDATA *prpDrw);
	void ProcessDrwChange(DRWDATA *prpDrw);
	void ProcessDrrChange(DRRDATA *prpDrr);
	CStaffTermDlg(CWnd* pParent = NULL);	// standard constructor
	~CStaffTermDlg();


	long StfUrno;

	// Dialog Data
	//{{AFX_DATA(CStaffTermDlg)
	enum { IDD = IDD_STAFFTERM_DIALOG };
	CButton	m_MultipleInput;
	CStatic	m_Static_Input;
	CCSEdit	m_Edit_Hours;
	CCSEdit	m_Edit_Einsatz;
	CStatic	m_Static_Name;
	CCSEdit	m_Edit_Year;
	CSpinButtonCtrl	m_Spin_Year;
	CCSEdit	m_Edit_Month;
	CSpinButtonCtrl	m_Spin_Month;
	CString	m_Static_Einsatz;
	CString	m_Static_Hours;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStaffTermDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool DateStringToOleDateTime(CString opDate, COleDateTime &opTime);
	bool IsWeekend(CString opDateString);
	bool IsWeekend(COleDateTime opDate);
	void SaveAcc(CString opType, CString opYear, CString opMonth, CString opValue);
	CString GetAccValuePerMonth(ACCDATA *opAcc, CString opMonth);
	void Register();
	CGridControl *pomWish;
	void IniGrid();
	void NewMonth();
	bool SaveMonth();

	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CStaffTermDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual void OnOK();
	afx_msg void OnMultipleInput();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int iLastInput;
	int iMaxWish;
	int GetDaysOfMonth(COleDateTime opDay);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTAPPLDLG_H__EA4946E7_1ACF_11D4_8FA9_00010204A53B__INCLUDED_)
