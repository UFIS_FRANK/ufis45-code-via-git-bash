Attribute VB_Name = "localLib"
Option Explicit

Public strHOPO As String
Public strServer As String
Public strShowHiddenData As String
Public strAFT_URNO As String
Public strAFTFIELDS As String
Public idxAFTTAB As Integer
Public idxLOATAB As Integer
Public strTLX_URNO As String
Public strSep As String
Public UsedLoaTabFields As String
Public MainDataTable As String
Public MainFieldsFina As String
Public MainFieldsFele As String
Public MainFieldsAlig As String
Public MainFieldsMean As String
Public MainFieldsType As String
Public MainFieldsSize As String
Public MyFontSize As Integer
Public MyFontBold As Boolean
Public MyPrintLogo As String

Public LoatabTimeZoneOffset As Integer

Public LoatabEditMode As Boolean
Public LoatabEditReadOnly As Boolean
Public LoatabEditSetup As Boolean
Public LoatabEditGrids As EditGrids
'---------------##############Variables Added by Sai(start here)#################-------------------------
Public strADID As String
Public openedConnection As Boolean
Public Const strADID_D = "D"
Public Const strADID_A = "A"

Public Const strADID_B = "B"
Public Const strGRID_DEPART = "GRIDS_DEPARTURE"
Public Const strGRID_ARRV = "GRIDS_ARRIVAL"
'--------------##############Variables Added by Sai(end here)###################----------------------------

Public intLabelWidth As Integer 'igu on 26/03/2012

Sub Main()
    frmMain.Show
    Load frmHiddenData
    If frmHiddenData.tabContentCfg.GetLineCount = 0 Then
        MsgBox "Configfile is empty. Please check it!"
        End
    End If
End Sub

Public Sub ShutDownApplication(AskUser As Boolean)
    Dim RetVal As Integer
    RetVal = 1
    'If AskUser Then
    '    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer)
    'End If
    If RetVal = 1 Then
        ShutDownRequested = True
        'UfisServer.aCeda.CleanupCom
        On Error Resume Next
        CloseHtmlHelp
        Unload frmMain
        End
    End If
    'If not, we come back
End Sub

Public Sub HandleDisplayChanged()

End Sub
Public Sub HandleSysColorsChanged()

End Sub
Public Sub HandleTimeChanged()

End Sub

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    If ObjName = "LOAEDT" Then
        If CedaCmd = "REFR" And Data = frmMain.txtAFTURNO.Text Then
            'igu on 30 Jul 2010 Update only when broadcast sender is not myself
            If CedaSqlKey <> UfisServer.CdrhdlSock.LocalHostName & "," & CStr(App.ThreadID) Then 'igu on 30 Jul 2010
                LoatabEditGrids.ResetAllState
                frmMain.chkLoad.Value = 1
            End If 'igu on 30 Jul 2010
        Else
'            MsgBox "BC Received: " & vbLf & vbLf & _
'                    "ReqId:" & vbTab & ReqId & vbLf & _
'                    "DestName:" & vbTab & DestName & vbLf & _
'                    "RecvName:" & vbTab & RecvName & vbLf & _
'                    "CedaCmd:" & vbTab & CedaCmd & vbLf & _
'                    "ObjName:" & vbTab & ObjName & vbLf & _
'                    "Seq:" & vbTab & Seq & vbLf & _
'                    "Tws:" & vbTab & Tws & vbLf & _
'                    "Twe:" & vbTab & Twe & vbLf & _
'                    "CedaSqlKey:" & vbTab & CedaSqlKey & vbLf & _
'                    "Fields:" & vbTab & Fields & vbLf & _
'                    "Data:" & vbTab & Data & vbLf & _
'                    "BcNum:" & vbTab & BcNum & vbLf
        End If
    ElseIf CedaCmd = "CLO" Then
        MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
        End
    End If
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    If ApplicationIsStarted = True Then
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub

Public Sub SetAllFormsOnTop(SetValue As Boolean)
    SetFormOnTop frmMain, SetValue
End Sub

Public Function DecodeSsimDateTime(timestamp As String, InFormat As String, OutFormat As String) As String
Dim Result As String
Dim tmpTime As String
Dim tmpDay As String
Dim tmpMonth As String
Dim tmpYear As String
Dim tmpMonVal As Integer

    Result = timestamp
    Select Case InFormat
        Case "SSIM2"
            tmpDay = Left(timestamp, 2)
            tmpMonth = Mid(timestamp, 3, 3)
            tmpMonVal = GetItemNo(MonthList, tmpMonth)
            tmpMonth = Right("00" & Trim(Str(tmpMonVal)), 2)
            tmpYear = Mid(timestamp, 6, 2)
            If tmpYear = "99" Then tmpYear = "19" & tmpYear Else tmpYear = "20" & tmpYear
            tmpTime = Mid(timestamp, 8, 4)
        Case "SSIM4"
            tmpDay = Left(timestamp, 2)
            tmpMonth = Mid(timestamp, 3, 3)
            tmpMonVal = GetItemNo(MonthList, tmpMonth)
            tmpMonth = Right("00" & Trim(Str(tmpMonVal)), 2)
            tmpYear = Mid(timestamp, 6, 4)
            tmpTime = Mid(timestamp, 10, 4)
        Case "CEDA"
            tmpYear = Left(timestamp, 4)
            tmpMonth = Mid(timestamp, 5, 2)
            tmpDay = Mid(timestamp, 7, 2)
            tmpTime = Mid(timestamp, 9, 4)
        Case Else
    End Select
    
    ConvertUTCToLocalTime tmpYear, tmpMonth, tmpDay, tmpTime
    
    'here we always have a 4 digit year, 2 digit month and 2 digit day
    Select Case OutFormat
        Case "CEDA"
            Result = tmpYear & tmpMonth & tmpDay & tmpTime
        Case "SSIM2"
            tmpMonVal = Val(tmpMonth)
            tmpMonth = GetItem(MonthList, tmpMonVal, ",")
            tmpYear = Right(tmpYear, 2)
            Result = tmpDay & tmpMonth & tmpYear & "/" & tmpTime
        Case Else
    End Select
    DecodeSsimDateTime = Result
End Function

Public Sub ConvertUTCToLocalTime(ByRef tmpYear As String, ByRef tmpMonth As String, ByRef tmpDay As String, ByRef tmpTime As String)
    Dim UTCTime As Date
    Dim localTime As Date
    Dim offset As Date
    
    UTCTime = CDate(tmpYear & "-" & tmpMonth & "-" & tmpDay & " " & Left(tmpTime, 2) & ":" & Mid(tmpTime, 3, 2))
    localTime = DateAdd("n", LoatabTimeZoneOffset, UTCTime)
    
    tmpYear = Year(localTime)
    tmpMonth = Month(localTime)
    tmpDay = Day(localTime)
    tmpTime = Right("00" & Trim(Hour(localTime)), 2) & Right("00" & Trim(Minute(localTime)), 2)
End Sub


