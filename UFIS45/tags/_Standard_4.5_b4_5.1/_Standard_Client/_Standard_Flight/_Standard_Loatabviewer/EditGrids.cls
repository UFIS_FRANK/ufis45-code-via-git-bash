VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EditGrids"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"EditGrid"
Attribute VB_Ext_KEY = "Member0" ,"EditGrid"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mCol As Collection
Private mvarFontSize As String

Public Sub InitFromConfig(iniFile As String)
    Dim item As Variant
    Dim strGRID As String
    '-----Added by Sai for #Disembarkment defect for Arrival Flights-----------------------------------
    If strADID = strADID_D Then
        strGRID = strGRID_DEPART
    Else
        strGRID = strGRID_ARRV
    End If
    '------------------Ends Here-----------------------------------------------------
    mvarFontSize = GetIniEntry(iniFile, "EDIT", "", "FONT_SIZE", "20")
    For Each item In Split(GetIniEntry(iniFile, "EDIT", "", strGRID, ""), ",")
        Add((item)).InitFromConfig iniFile, mvarFontSize
    Next
End Sub

Private Function Add(Key As String) As EditGrid
    Dim grid As EditGrid
    Set grid = New EditGrid
    grid.IniName = Key
    mCol.Add grid, Key
    Set Add = grid
End Function

Public Sub CreateAll(parentForm As Form)
    Dim grid As EditGrid
    
    For Each grid In mCol
        grid.Create parentForm
    Next
End Sub

Public Sub Resize(Left As Long, Top As Long, Width As Long, height As Long)
    Dim grid As EditGrid
    Dim expandSize As Long
    Dim expandCount As Long
    Dim currentTop As Long
    Dim lineSize As Long
    
    expandSize = height
    lineSize = (mvarFontSize) / 96 * 1440
    For Each grid In mCol
        If grid.Size = "*" Then
            expandCount = expandCount + 1
        Else
            expandSize = expandSize - ((grid.Rows.Count + 1) * lineSize + 310)
        End If
    Next
    
    currentTop = Top
    For Each grid In mCol
        If grid.Size = "*" Then
            grid.Resize Left, currentTop, Width, expandSize / expandCount
            currentTop = currentTop + expandSize / expandCount
        Else
            grid.Resize Left, currentTop, Width, ((grid.Rows.Count + 1) * lineSize) + 310
            currentTop = currentTop + ((grid.Rows.Count + 1) * lineSize) + 310
        End If
    Next
End Sub

Public Sub LoadData(DataTab As TABLib.TAB)
    Dim grid As EditGrid
    
    For Each grid In mCol
        grid.LoadData DataTab
    Next
End Sub

Public Function GetChanges() As Collection
    Dim changes As Collection
    Dim grid As EditGrid
    
    Set changes = New Collection
    For Each grid In mCol
        grid.GetChanges changes
    Next
    Set GetChanges = changes
End Function
'kkh to insert / update the lastest figures for Total on board for Departure flight
Public Function GetInitialChanges() As Collection
    Dim changes As Collection
    Dim grid As EditGrid
    'Dim nTotalRows As Integer 'igu on 10 Nov 2009 'igu on 05 Apr 2010
    
    Set changes = New Collection
    For Each grid In mCol
        'nTotalRows = grid.Rows.Count 'igu on 10 Nov 2009 'igu on 05 Apr 2010
        'grid.GetInitialChanges changes  'igu on 10 Nov 2009
        'grid.GetInitialChanges changes, nTotalRows 'igu on 10 Nov 2009 'igu on 05 Apr 2010
        grid.GetInitialChanges changes
    Next
    Set GetInitialChanges = changes
End Function

Public Sub ResetAllState()
    Dim grid As EditGrid
    
    For Each grid In mCol
        grid.Cells.ResetState
    Next
End Sub

Public Property Get MinWidth() As Long
    Dim grid As EditGrid
    MinWidth = 0
    
    For Each grid In mCol
        If grid.Width > MinWidth Then MinWidth = grid.Width
    Next
End Property

Public Property Get MinHeight() As Long
    Dim grid As EditGrid
    Dim lineSize As Long
    
    lineSize = (mvarFontSize) / 96 * 1440
    For Each grid In mCol
        MinHeight = MinHeight + ((grid.Rows.Count + 1) * lineSize) + 310
    Next
End Property

Public Property Get item(vntIndexKey As Variant) As EditGrid
Attribute item.VB_UserMemId = 0
  Set item = mCol(vntIndexKey)
End Property

Public Property Get Count() As Long
    Count = mCol.Count
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub

