using System;

namespace FlightPermits
{
	/// <summary>
	/// AirlineCode - used for display of airline codes in a comboBox
	/// </summary>
	public class AirlineCode
	{
		private string myAlc3;
		private string myAlc2;
		private string myCode1;
		private string myCode2;
    
		public AirlineCode(string alc2, string alc3)
		{
			if(alc2.Length > 0 || alc3.Length > 0)
			{
				this.myCode1 = alc2 + "/" + alc3;
				this.myCode2 = alc3 + "/" + alc2;
			}
			else
			{
				this.myCode1 = "";
				this.myCode2 = "";
			}
			this.myAlc3 = alc3;
			this.myAlc2 = alc2;
		}

		public string Code
		{
			get
			{
				return myCode1;
			}
		}

		public string Code1
		{
			get
			{
				return myCode1;
			}
		}

		public string Code2
		{
			get
			{
				return myCode2;
			}
		}

		public string Alc3
		{
			get
			{
				return myAlc3;
			}
		}

		public string Alc2
		{
			get
			{
				return myAlc2;
			}
		}
	}
}
