// Utils.h : header file
//
// Modification History: 
// 22-nov-00	rkr		IsPostFlight() added
// 05-dec-00	rkr		ModifyWindowText() added

#if !defined(Utils_H__INCLUDED_)
#define Utils_H__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


int InsertSortInStrArray(CStringArray &ropStrArray, const CString &ropStr);
bool FindInStrArray(const CStringArray &ropStrArray, const CString &ropStr);
int FindIdInStrArray(const CStringArray &ropStrArray, const CString &ropStr);

bool IsRegularFlight(char Ftyp);
bool IsArrivalFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsArrival(const char *pcpOrg, const char *pcpDes);
bool IsDepartureFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsDeparture(const char *pcpOrg, const char *pcpDes);
bool IsCircularFlight(const char *pcpOrg, const char *pcpDes, char Ftyp);
bool IsCircular(const char *pcpOrg, const char *pcpDes);

bool DelayCodeToAlpha(char *pcpDcd);
bool DelayCodeToNum(char *pcpDcd);

int FindInListBox(const CListBox &ropListBox, const CString &ropStr);

// A substitute for CListBox::GetSelItems(...)
// because this function dont't work on some WinNT machines
int MyGetSelItems(CListBox &ropLB, int ipMaxAnz, int *prpItems);


long MyGetUrnoFromSelection(const CString &ropSel);


bool GetFltnInDBFormat(const CString &ropUserFltn, CString &ropDBFltn);

bool GetCurrentUtcTime(CTime &ropUtcTime);


#endif