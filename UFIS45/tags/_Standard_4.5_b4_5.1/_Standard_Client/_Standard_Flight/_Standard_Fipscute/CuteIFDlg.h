// CuteIFDlg.h : header file
//

#if !defined(AFX_CUTEIFDLG_H__2C783E36_9F84_11D4_9CBF_00D0B7840078__INCLUDED_)
#define AFX_CUTEIFDLG_H__2C783E36_9F84_11D4_9CBF_00D0B7840078__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CedaFldData.h"
#include "CedaCcaData.h"
#include "CedaAftData.h"
#include "FxtData.h"
#include "BltFreeTextDlg.h"
#include "CCSTable.h"
#include "CCSEdit.h"
#include "resource.h"
#include "UFISAmSink.h"
#include "cdib.h"

#define MAX_LOGO_SIZE_X 300
#define MAX_LOGO_SIZE_Y 85

#define REMCODE_BOARDING "BRD"
#define REMCODE_FINALCALL "FCL"

#define RELOAD_SEC 120 // reload every 2 minute, when counter/gate is closed!


/////////////////////////////////////////////////////////////////////////////
// CCuteIFDlg dialog

class CCuteIFDlg : public CDialog
{
// Construction
public:
	CCuteIFDlg(CWnd* pParent = NULL);	// standard constructor
	~CCuteIFDlg();

	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
	void  ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	void ExternCall(LPCSTR popMessage);


	CUFISAmSink m_UFISAmSink;


	// Dialog Data
	//{{AFX_DATA(CCuteIFDlg)
	enum { IDD = IDD_CUTEIF_DIALOG };
	CStatic	m_CS_State;
	CEdit	m_CE_Free1;
	CEdit	m_CE_Free2;
	CComboBox	m_CCB_LogoSelect; 
	CComboBox	m_CCB_CheckinSel; 
	CComboBox	m_CCB_GateSel; 
	CComboBox	m_CCB_FidsRemarks; 
	//CListBox	m_CLB_FlightList;
	CButton		m_CB_Transmit;
	CButton		m_CB_Close;
	CStatic		m_CS_FlightTBorder;
	CCSEdit		m_Text_Sign;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCuteIFDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL



// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCuteIFDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
    afx_msg void OnTimer(UINT nIDEvent);
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnLogoSelChange();
	afx_msg void OnCheckinSelChange();
	afx_msg void OnGateSelChange();
	afx_msg void OnCkicGateClose();
	afx_msg void OnTransmit();
	afx_msg void OnClose();
	afx_msg void OnCancel();
	afx_msg void OnOK();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	bool bmBCEnabled;

	CRect omLogoRect;

	bool bmClosed;

	CInternetSession *pomInternetSession;
	CHttpConnection *pomHttpServer;

	CDib omLogo;

	CString omCkicName;
	CString omGateName;

	//CStringArray omLogosShortN;

	AFTDATA rmSelFlight; // This flight was selected by the user when calling 'Display Settings' from FIPS!

	FLDDATA *prmFldData;
	RecordSet *prmFlzRecord;

	CPtrArray omCcaList;
	CPtrArray omGateList;

	CCSTable omFlightTable;

	CedaFldData omFldData;
	CedaCcaData omCcaData;
	CedaAftData omAftData;
	FxtData	omFxtData;

	BltFreeTextDlg *pomBltFreeTextDlg;

	CStringArray omExtMesBuffer;

	bool CheckInternetCon(const CString &ropHttpLogoPath);

	void DrawFlightTHeader();
	void MakeFlightTColList(const CString &ropJoint, const CString &ropFlno, const CString &ropAlc3, CCSPtrArray<TABLE_COLUMN> &olColList);


	void ResetDlg();
	void ActivateFIDSData();
	void DeactivateFIDSData();


	void NewCkic(const CString &ropCkic, long lpFlightUrno = 0);
	void NewGate(const CString &ropGate, long lpFlightUrno = 0);

	void LoadData(long lpFlightUrno = 0);
	void RefreshData();

	void DisplayData();
	void DisplayRecordData();
	bool SetLogo(const CString &ropFilename);
	bool SetLogoData(CFile *popFileData); //char *prpLogoData, long lpDataSize);

	void GetListOfLogos();
	void GetListOfCheckins();
	void GetListOfGates();
	void SetFidsRemarks();

	bool GetMonitorIP(CString &ropMonitorIP);
	void SetFlightListCca(const CPtrArray &ropCcaList);
	void SetFlightListGate(const CPtrArray &ropAftList);

	bool GetSelFIDSRemCode(CString &ropFIDSRemCode);
	bool GetOpenCcaRecs(CPtrArray *popCcas);
	bool GetOpenGateRecs(CPtrArray *popAfts);
	bool AreFlightsSelected();
	//bool GetSelListOfFlights(CString &ropFlightList);
	//bool GetAllOpenFlights(CString &ropFlightList) const;
	bool GetAlcFromSelFlight(CString &ropAlc);

	bool UpdateCkicData();
	bool UpdateGateData();

	bool SaveCuteRecord(long lpAftUrno, long lpCcaUrno);

	AFTDATA *GetCurrentAftData();
	CCADATA *GetCurrentCcaData();
	
	bool DBUpdateLogo(long lpFldu, const CString &ropName);

	bool GetDBLogo(long lpFldu, CString &ropName);

	bool InitCOMInterface();

	bool IsCkinOpen(long lpAftUrno, long lpCcaUrno) const;
	bool IsGateOpen(long lpAftUrno) const;

	bool ExtractText(const CString &ropText, const CString &ropKey, CString &ropValue) const;
	bool ExtractUrno(const CString &ropText, const CString &ropKey, long &rlpUrno) const;

	void ProcessExtMesBuffer();
	void ProcessExternMessage(const CString &ropMessage);



	void Exit();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CUTEIFDLG_H__2C783E36_9F84_11D4_9CBF_00D0B7840078__INCLUDED_)
