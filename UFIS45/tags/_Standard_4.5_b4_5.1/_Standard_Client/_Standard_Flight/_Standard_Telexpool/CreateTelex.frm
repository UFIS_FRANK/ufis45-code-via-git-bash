VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form CreateTelex 
   Caption         =   "Ufis Telex Generator"
   ClientHeight    =   8670
   ClientLeft      =   75
   ClientTop       =   450
   ClientWidth     =   18810
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "CreateTelex.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8670
   ScaleWidth      =   18810
   Tag             =   "Ufis Telex Generator"
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   5
      Left            =   8970
      TabIndex        =   354
      Top             =   4320
      Visible         =   0   'False
      Width           =   5595
      Begin VB.CheckBox chkCloseSetup 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   2430
         Style           =   1  'Graphical
         TabIndex        =   392
         Top             =   2070
         Width           =   855
      End
      Begin VB.CheckBox chkReload 
         Caption         =   "Reload"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   1500
         Style           =   1  'Graphical
         TabIndex        =   387
         Top             =   2070
         Width           =   855
      End
      Begin VB.Frame fraSetupXX 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Online Windows"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1755
         Index           =   0
         Left            =   2430
         TabIndex        =   366
         Top             =   240
         Width           =   3045
         Begin VB.TextBox TlxVpfr 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1590
            TabIndex        =   383
            Text            =   "-1"
            Top             =   1290
            Width           =   405
         End
         Begin VB.TextBox TlxVpto 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   2460
            TabIndex        =   382
            Text            =   "+1"
            Top             =   1290
            Width           =   405
         End
         Begin VB.TextBox TlxVpfr 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1590
            TabIndex        =   378
            Text            =   "-1"
            Top             =   960
            Width           =   405
         End
         Begin VB.TextBox TlxVpto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   2460
            TabIndex        =   377
            Text            =   "+1"
            Top             =   960
            Width           =   405
         End
         Begin VB.TextBox TlxVpfr 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   1590
            TabIndex        =   373
            Text            =   "-1"
            Top             =   630
            Width           =   405
         End
         Begin VB.TextBox TlxVpto 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   2460
            TabIndex        =   372
            Text            =   "+1"
            Top             =   630
            Width           =   405
         End
         Begin VB.TextBox TlxVpto 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   2460
            TabIndex        =   368
            Text            =   "+1"
            Top             =   300
            Width           =   405
         End
         Begin VB.TextBox TlxVpfr 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   1590
            TabIndex        =   367
            Text            =   "-1"
            Top             =   300
            Width           =   405
         End
         Begin VB.Label Label31 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Sent Out:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   180
            TabIndex        =   386
            Top             =   1350
            Width           =   750
         End
         Begin VB.Label Label30 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Plus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   2070
            TabIndex        =   385
            Top             =   1350
            Width           =   360
         End
         Begin VB.Label Label29 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Minus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1050
            TabIndex        =   384
            Top             =   1350
            Width           =   510
         End
         Begin VB.Label Label28 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Pending:"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   180
            TabIndex        =   381
            Top             =   1020
            Width           =   720
         End
         Begin VB.Label Label27 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Plus"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   2070
            TabIndex        =   380
            Top             =   1020
            Width           =   360
         End
         Begin VB.Label Label26 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Minus"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1050
            TabIndex        =   379
            Top             =   1020
            Width           =   510
         End
         Begin VB.Label Label25 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Created:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   180
            TabIndex        =   376
            Top             =   690
            Width           =   705
         End
         Begin VB.Label Label22 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Plus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   2070
            TabIndex        =   375
            Top             =   690
            Width           =   360
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Minus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1050
            TabIndex        =   374
            Top             =   690
            Width           =   510
         End
         Begin VB.Label Label24 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Minus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1050
            TabIndex        =   371
            Top             =   360
            Width           =   510
         End
         Begin VB.Label Label23 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Plus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   2070
            TabIndex        =   370
            Top             =   360
            Width           =   360
         End
         Begin VB.Label Label20 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Received:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   180
            TabIndex        =   369
            Top             =   360
            Width           =   795
         End
      End
      Begin VB.Frame fraSetupXX 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Flight Data Periods"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1755
         Index           =   1
         Left            =   120
         TabIndex        =   355
         Top             =   240
         Width           =   2205
         Begin VB.TextBox AftVpfr 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   780
            TabIndex        =   359
            Text            =   "-1"
            Top             =   630
            Width           =   405
         End
         Begin VB.TextBox AftVpfr 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   780
            TabIndex        =   358
            Text            =   "0"
            Top             =   1290
            Width           =   405
         End
         Begin VB.TextBox AftVpto 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   1650
            TabIndex        =   357
            Text            =   "+1"
            Top             =   630
            Width           =   405
         End
         Begin VB.TextBox AftVpto 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   1650
            TabIndex        =   356
            Text            =   "+2"
            Top             =   1290
            Width           =   405
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Arrival Flights"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   180
            TabIndex        =   365
            Top             =   360
            Width           =   1140
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Departure Flights"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   180
            TabIndex        =   364
            Top             =   1020
            Width           =   1425
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Plus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1260
            TabIndex        =   363
            Top             =   1350
            Width           =   360
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Plus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1260
            TabIndex        =   362
            Top             =   690
            Width           =   360
         End
         Begin VB.Label Label18 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Minus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   240
            TabIndex        =   361
            Top             =   690
            Width           =   510
         End
         Begin VB.Label Label19 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Minus"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   240
            TabIndex        =   360
            Top             =   1350
            Width           =   510
         End
      End
   End
   Begin VB.Frame fraTopButtons 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   3
      Left            =   7110
      TabIndex        =   350
      Top             =   15
      Width           =   1335
      Begin VB.CheckBox OnTop 
         Caption         =   "Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   675
         Style           =   1  'Graphical
         TabIndex        =   390
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox OpenPool 
         Caption         =   "Main"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   15
         Style           =   1  'Graphical
         TabIndex        =   351
         Top             =   0
         Width           =   645
      End
   End
   Begin VB.PictureBox MainSplitHorzS 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   1740
      ScaleHeight     =   195
      ScaleWidth      =   180
      TabIndex        =   349
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.PictureBox MainSplitHorzM 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   1470
      ScaleHeight     =   195
      ScaleWidth      =   180
      TabIndex        =   348
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2235
      Index           =   7
      Left            =   10740
      TabIndex        =   336
      Top             =   5730
      Visible         =   0   'False
      Width           =   5235
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Panel Borders"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1860
         Index           =   4
         Left            =   2820
         TabIndex        =   343
         Top             =   210
         Width           =   2235
         Begin VB.CheckBox chkBorder 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Life Style Border"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   180
            TabIndex        =   346
            Top             =   990
            Width           =   1905
         End
         Begin VB.Frame fraSetupX 
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   540
            Index           =   5
            Left            =   150
            TabIndex        =   344
            Top             =   1170
            Width           =   1965
            Begin MSComctlLib.Slider PanelSizeSlider 
               Height          =   315
               Index           =   0
               Left            =   60
               TabIndex        =   345
               Top             =   165
               Width           =   1845
               _ExtentX        =   3254
               _ExtentY        =   556
               _Version        =   393216
               LargeChange     =   4
               SmallChange     =   2
               Max             =   20
               SelStart        =   3
               TickStyle       =   3
               Value           =   3
               TextPosition    =   1
            End
         End
      End
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Application Resizing ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1860
         Index           =   3
         Left            =   150
         TabIndex        =   337
         Top             =   210
         Width           =   2535
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Moves all panels"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   4
            Left            =   120
            TabIndex        =   352
            Tag             =   "4"
            Top             =   1260
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Resizes the window"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   5
            Left            =   120
            TabIndex        =   342
            Tag             =   "5"
            Top             =   1500
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Moves main and right"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   3
            Left            =   120
            TabIndex        =   341
            Tag             =   "3"
            Top             =   1020
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Moves the right panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   120
            TabIndex        =   340
            Tag             =   "2"
            Top             =   780
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Centers the main panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   120
            TabIndex        =   339
            Tag             =   "1"
            Top             =   540
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Resizes the work area"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   338
            Tag             =   "0"
            Top             =   300
            Width           =   2280
         End
      End
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00C0C0FF&
      Caption         =   "For Test Purposes Only"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2355
      Index           =   6
      Left            =   9090
      TabIndex        =   328
      Top             =   4710
      Visible         =   0   'False
      Width           =   2745
      Begin VB.CheckBox chkTplPatch 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Create Main Fields Only"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   6
         Left            =   180
         TabIndex        =   347
         Top             =   570
         Width           =   2460
      End
      Begin VB.CheckBox chkTplPatch 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Insert Fake PAX and Delay"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   5
         Left            =   180
         TabIndex        =   334
         Top             =   1950
         Width           =   2460
      End
      Begin VB.CheckBox chkTplPatch 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Synchronize Field Groups"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   4
         Left            =   180
         TabIndex        =   333
         Top             =   1590
         Width           =   2460
      End
      Begin VB.CheckBox chkTplPatch 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Show Source Indicators"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   180
         TabIndex        =   332
         Top             =   870
         Width           =   2460
      End
      Begin VB.CheckBox chkTplPatch 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Enable Individual Updates"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   2
         Left            =   180
         TabIndex        =   331
         Top             =   1350
         Width           =   2460
      End
      Begin VB.CheckBox chkTplPatch 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Enable Cascading Updates"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   180
         TabIndex        =   330
         Top             =   1110
         Width           =   2460
      End
      Begin VB.CheckBox chkTplPatch 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Create Full Input Field Set"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   180
         TabIndex        =   329
         Top             =   330
         Width           =   2460
      End
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   0
      Left            =   9120
      TabIndex        =   324
      Top             =   4320
      Visible         =   0   'False
      Width           =   1875
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Index           =   0
         Left            =   0
         TabIndex        =   325
         Top             =   -135
         Width           =   1845
         Begin VB.CheckBox chkSetup 
            Caption         =   "Layout"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   7
            Left            =   930
            Style           =   1  'Graphical
            TabIndex        =   335
            Top             =   165
            Width           =   855
         End
         Begin VB.CheckBox chkSetup 
            Caption         =   "Demo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   6
            Left            =   2070
            Style           =   1  'Graphical
            TabIndex        =   327
            Top             =   165
            Width           =   855
         End
         Begin VB.CheckBox chkSetup 
            Caption         =   "Loader"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   326
            Top             =   165
            Width           =   855
         End
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   975
      Left            =   16140
      ScaleHeight     =   915
      ScaleWidth      =   1605
      TabIndex        =   321
      Top             =   5070
      Visible         =   0   'False
      Width           =   1665
      Begin VB.PictureBox picBallMini 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   0  'None
         Height          =   165
         Index           =   1
         Left            =   390
         Picture         =   "CreateTelex.frx":014A
         ScaleHeight     =   165
         ScaleWidth      =   195
         TabIndex        =   323
         Top             =   180
         Width           =   195
      End
      Begin VB.PictureBox picBallMini 
         BackColor       =   &H0080FFFF&
         BorderStyle     =   0  'None
         Height          =   165
         Index           =   0
         Left            =   150
         Picture         =   "CreateTelex.frx":0224
         ScaleHeight     =   165
         ScaleWidth      =   195
         TabIndex        =   322
         Top             =   180
         Width           =   195
      End
   End
   Begin VB.Frame fraMtpTmplText 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Plain Template Layout"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   960
      Left            =   14730
      TabIndex        =   317
      Tag             =   "VISIBLE"
      Top             =   1560
      Visible         =   0   'False
      Width           =   1710
      Begin VB.PictureBox TplEditPanel 
         BackColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   90
         ScaleHeight     =   615
         ScaleWidth      =   1065
         TabIndex        =   318
         Top             =   240
         Width           =   1125
         Begin VB.TextBox txtTplText 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   525
            Left            =   0
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   319
            Top             =   0
            Width           =   1845
         End
      End
   End
   Begin VB.PictureBox MainTool 
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   0
      Left            =   2100
      ScaleHeight     =   345
      ScaleWidth      =   1185
      TabIndex        =   316
      Top             =   8010
      Visible         =   0   'False
      Width           =   1185
      Begin VB.Image Image2 
         Height          =   375
         Left            =   450
         Picture         =   "CreateTelex.frx":02BE
         Top             =   -15
         Width           =   375
      End
      Begin VB.Image Image1 
         Height          =   375
         Left            =   780
         Picture         =   "CreateTelex.frx":0490
         Top             =   -15
         Width           =   375
      End
   End
   Begin VB.Frame fraFlightCover 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   16020
      TabIndex        =   314
      Tag             =   "VISIBLE"
      Top             =   1800
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.PictureBox SubSplitHorzS 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   570
      MousePointer    =   7  'Size N S
      ScaleHeight     =   195
      ScaleWidth      =   420
      TabIndex        =   313
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Frame SubFrameS 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   0
      Left            =   3420
      TabIndex        =   312
      Top             =   8010
      Width           =   315
   End
   Begin VB.PictureBox SubPanelS 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   570
      ScaleHeight     =   315
      ScaleWidth      =   450
      TabIndex        =   311
      TabStop         =   0   'False
      Top             =   7530
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.PictureBox SubSplitVertS 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   1065
      Index           =   0
      Left            =   1290
      ScaleHeight     =   1065
      ScaleWidth      =   165
      TabIndex        =   310
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   165
      Begin VB.Image PicVSplitS 
         Enabled         =   0   'False
         Height          =   480
         Index           =   0
         Left            =   -960
         Picture         =   "CreateTelex.frx":0768
         Stretch         =   -1  'True
         Top             =   0
         Width           =   1125
      End
   End
   Begin VB.PictureBox MainPanel 
      BackColor       =   &H0000C000&
      Height          =   465
      Index           =   1
      Left            =   1470
      ScaleHeight     =   405
      ScaleWidth      =   510
      TabIndex        =   309
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   570
   End
   Begin VB.PictureBox SubSplitVertM 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   1065
      Index           =   0
      Left            =   1050
      ScaleHeight     =   1065
      ScaleWidth      =   165
      TabIndex        =   305
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   165
      Begin VB.Image PicVSplitM 
         Enabled         =   0   'False
         Height          =   480
         Index           =   0
         Left            =   0
         Picture         =   "CreateTelex.frx":23AA
         Stretch         =   -1  'True
         Top             =   0
         Width           =   1125
      End
   End
   Begin VB.PictureBox SubSplitHorzM 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   30
      MousePointer    =   7  'Size N S
      ScaleHeight     =   195
      ScaleWidth      =   450
      TabIndex        =   304
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.PictureBox SubPanelM 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   30
      ScaleHeight     =   315
      ScaleWidth      =   510
      TabIndex        =   303
      TabStop         =   0   'False
      Top             =   7530
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.PictureBox MainPanel 
      BackColor       =   &H00C00000&
      Height          =   465
      Index           =   0
      Left            =   30
      ScaleHeight     =   405
      ScaleWidth      =   960
      TabIndex        =   302
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   1020
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   15720
      TabIndex        =   292
      Top             =   960
      Visible         =   0   'False
      Width           =   1065
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Index           =   2
         Left            =   0
         TabIndex        =   293
         Top             =   -135
         Width           =   975
         Begin VB.CheckBox chkSetup 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Delay"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   294
            Top             =   165
            Width           =   855
         End
      End
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   15720
      TabIndex        =   289
      Top             =   1380
      Visible         =   0   'False
      Width           =   1065
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Index           =   1
         Left            =   0
         TabIndex        =   290
         Top             =   -135
         Visible         =   0   'False
         Width           =   975
         Begin VB.CheckBox chkSetup 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Delay"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   291
            Top             =   165
            Width           =   855
         End
      End
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1305
      Index           =   4
      Left            =   17820
      TabIndex        =   279
      Top             =   3570
      Visible         =   0   'False
      Width           =   3600
      Begin VB.PictureBox SetupPanel 
         BackColor       =   &H008080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   1005
         Index           =   0
         Left            =   90
         ScaleHeight     =   1005
         ScaleWidth      =   3435
         TabIndex        =   280
         Top             =   240
         Width           =   3435
         Begin VB.TextBox MyTestCase 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   287
            Top             =   0
            Width           =   900
         End
         Begin VB.TextBox MyTestCase 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   960
            Locked          =   -1  'True
            TabIndex        =   286
            Top             =   0
            Width           =   2460
         End
         Begin VB.TextBox MyTestCase 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   960
            Locked          =   -1  'True
            TabIndex        =   285
            Top             =   330
            Width           =   2460
         End
         Begin VB.TextBox MyTestCase 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   960
            Locked          =   -1  'True
            TabIndex        =   284
            Top             =   990
            Visible         =   0   'False
            Width           =   1470
         End
         Begin VB.TextBox MyTestCase 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   960
            Locked          =   -1  'True
            TabIndex        =   283
            Top             =   660
            Width           =   1470
         End
         Begin VB.TextBox MyTestCase 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   2445
            Locked          =   -1  'True
            TabIndex        =   282
            Top             =   660
            Width           =   975
         End
         Begin VB.TextBox MyTestCase 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   2445
            Locked          =   -1  'True
            TabIndex        =   281
            Top             =   990
            Visible         =   0   'False
            Width           =   975
         End
      End
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      Caption         =   "TEST CASE GENERATOR"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1635
      Index           =   3
      Left            =   17790
      TabIndex        =   267
      Top             =   1770
      Visible         =   0   'False
      Width           =   6135
      Begin VB.CheckBox chkTestCase 
         Caption         =   "Update"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2640
         Style           =   1  'Graphical
         TabIndex        =   278
         Top             =   1230
         Width           =   900
      End
      Begin VB.TextBox AftTestValue 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   1905
         TabIndex        =   277
         Top             =   1230
         Width           =   600
      End
      Begin VB.TextBox AftTestValue 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   1905
         TabIndex        =   276
         Top             =   900
         Width           =   600
      End
      Begin VB.TextBox AftTestValue 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1290
         TabIndex        =   274
         Top             =   1230
         Width           =   600
      End
      Begin VB.TextBox AftTestValue 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1290
         TabIndex        =   272
         Top             =   900
         Width           =   600
      End
      Begin VB.TextBox AftTestValue 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1290
         TabIndex        =   269
         Top             =   570
         Width           =   600
      End
      Begin VB.TextBox AftTestValue 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1290
         TabIndex        =   268
         Top             =   240
         Width           =   600
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "DUR1 (DUR2)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   275
         Top             =   1290
         Width           =   975
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "DCO1 (DCO2)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   273
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Grace Period"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   271
         Top             =   630
         Width           =   1065
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total Delay"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   270
         Top             =   300
         Width           =   885
      End
   End
   Begin VB.Frame fraFlightPanel 
      BackColor       =   &H000040C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   840
      Index           =   6
      Left            =   14910
      TabIndex        =   253
      Top             =   3060
      Visible         =   0   'False
      Width           =   2820
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   2085
         ScaleHeight     =   315
         ScaleWidth      =   705
         TabIndex        =   254
         Tag             =   "|OFBL,TIME"
         Top             =   450
         Width           =   705
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            TabIndex        =   255
            Text            =   "12:10"
            Top             =   0
            Width           =   645
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   0
            TabIndex        =   256
            Top             =   45
            Width           =   45
         End
      End
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   11
         Left            =   2085
         ScaleHeight     =   315
         ScaleWidth      =   705
         TabIndex        =   260
         Tag             =   "|ETDI,TIME"
         Top             =   120
         Width           =   705
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   11
            Left            =   0
            TabIndex        =   261
            Text            =   "12:10"
            Top             =   0
            Width           =   645
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   11
            Left            =   0
            TabIndex        =   262
            Top             =   45
            Width           =   45
         End
      End
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   12
         Left            =   105
         ScaleHeight     =   315
         ScaleWidth      =   1995
         TabIndex        =   263
         Tag             =   "|ETDI,DATE"
         Top             =   120
         Width           =   1995
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   12
            Left            =   900
            TabIndex        =   264
            Text            =   "31.12.2009"
            Top             =   0
            Width           =   1065
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "ETD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   12
            Left            =   0
            TabIndex        =   265
            Top             =   45
            Width           =   300
         End
      End
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   4
         Left            =   105
         ScaleHeight     =   315
         ScaleWidth      =   1995
         TabIndex        =   257
         Tag             =   "|OFBL,DATE"
         Top             =   450
         Width           =   1995
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   900
            TabIndex        =   258
            Text            =   "31.12.2009"
            Top             =   0
            Width           =   1065
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "OFB (OUT)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   0
            TabIndex        =   259
            Top             =   45
            Width           =   810
         End
      End
   End
   Begin VB.Frame fraTopButtons 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   2
      Left            =   8790
      TabIndex        =   244
      Top             =   15
      Width           =   1350
      Begin VB.CheckBox chkExit 
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   246
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   245
         Top             =   0
         Width           =   645
      End
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   11880
      TabIndex        =   243
      Top             =   30
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   11220
      TabIndex        =   242
      Top             =   30
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.CheckBox chkGetCfg 
      Caption         =   "GetCfg"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   14400
      Style           =   1  'Graphical
      TabIndex        =   240
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame fraFlightPanel 
      BackColor       =   &H0000C0C0&
      Caption         =   "Flight Details"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4140
      Index           =   2
      Left            =   12000
      TabIndex        =   128
      Tag             =   "VISIBLE"
      Top             =   3960
      Visible         =   0   'False
      Width           =   6150
      Begin VB.Frame fraFlightPanel 
         Caption         =   "Arrival at SIN"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Index           =   13
         Left            =   3150
         TabIndex        =   200
         Top             =   2940
         Width           =   2895
         Begin VB.Frame fraFlightPanel 
            BackColor       =   &H000040C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame4"
            Height          =   840
            Index           =   14
            Left            =   30
            TabIndex        =   201
            Top             =   195
            Width           =   2820
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   30
               Left            =   2085
               ScaleHeight     =   315
               ScaleWidth      =   705
               TabIndex        =   202
               Top             =   450
               Width           =   705
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFFF&
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   30
                  Left            =   0
                  TabIndex        =   203
                  Text            =   "12:10"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   " "
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   30
                  Left            =   0
                  TabIndex        =   204
                  Top             =   45
                  Width           =   45
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   32
               Left            =   2085
               ScaleHeight     =   315
               ScaleWidth      =   705
               TabIndex        =   208
               Top             =   120
               Width           =   705
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFFF&
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   32
                  Left            =   0
                  TabIndex        =   209
                  Text            =   "12:10"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   " "
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   32
                  Left            =   0
                  TabIndex        =   210
                  Top             =   45
                  Width           =   45
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   33
               Left            =   105
               ScaleHeight     =   315
               ScaleWidth      =   1995
               TabIndex        =   211
               Tag             =   "LDNS;DATE"
               Top             =   120
               Width           =   1995
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFFF&
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   33
                  Left            =   900
                  TabIndex        =   212
                  Text            =   "31.12.2009"
                  Top             =   0
                  Width           =   1065
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "LND (ON)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   33
                  Left            =   0
                  TabIndex        =   213
                  Top             =   45
                  Width           =   705
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   31
               Left            =   105
               ScaleHeight     =   315
               ScaleWidth      =   1995
               TabIndex        =   205
               Tag             =   "OBNS;DATE"
               Top             =   450
               Width           =   1995
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFFF&
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   31
                  Left            =   900
                  TabIndex        =   206
                  Text            =   "31.12.2009"
                  Top             =   0
                  Width           =   1065
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "ONB (IN)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   31
                  Left            =   0
                  TabIndex        =   207
                  Top             =   45
                  Width           =   645
               End
            End
         End
      End
      Begin VB.Frame fraFlightPanel 
         Caption         =   "Departure from PRV"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Index           =   11
         Left            =   150
         TabIndex        =   198
         Top             =   2940
         Width           =   2895
         Begin VB.Frame fraFlightPanel 
            BackColor       =   &H000040C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame4"
            Height          =   840
            Index           =   12
            Left            =   30
            TabIndex        =   199
            Top             =   195
            Width           =   2820
         End
      End
      Begin VB.Frame fraFlightPanel 
         Caption         =   "Arrival at NXT"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Index           =   5
         Left            =   3135
         TabIndex        =   129
         Top             =   240
         Width           =   2895
         Begin VB.Frame fraFlightPanel 
            BackColor       =   &H000040C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame4"
            Height          =   840
            Index           =   8
            Left            =   30
            TabIndex        =   131
            Top             =   195
            Width           =   2820
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   2
               Left            =   1875
               ScaleHeight     =   315
               ScaleWidth      =   705
               TabIndex        =   234
               Tag             =   "|EETF,LONG,EETM,EETM"
               Top             =   120
               Width           =   705
               Begin VB.TextBox AftFldText 
                  Alignment       =   1  'Right Justify
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   2
                  Left            =   0
                  MaxLength       =   4
                  TabIndex        =   235
                  Text            =   "155"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "(M)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   2
                  Left            =   0
                  TabIndex        =   236
                  Top             =   45
                  Width           =   270
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   1
               Left            =   105
               ScaleHeight     =   315
               ScaleWidth      =   1785
               TabIndex        =   231
               Tag             =   "|EETF,HHMM,EETH,EETH"
               Top             =   120
               Width           =   1785
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   1
                  Left            =   1110
                  MaxLength       =   4
                  TabIndex        =   232
                  Text            =   "0235"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "EET to MMM"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   1
                  Left            =   0
                  TabIndex        =   233
                  Top             =   45
                  Width           =   990
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   10
               Left            =   2085
               ScaleHeight     =   315
               ScaleWidth      =   705
               TabIndex        =   167
               Tag             =   "|EETA,TIME,EANS"
               Top             =   450
               Width           =   705
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   10
                  Left            =   0
                  TabIndex        =   168
                  Text            =   "12:00"
                  Top             =   0
                  Width           =   645
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   9
               Left            =   105
               ScaleHeight     =   315
               ScaleWidth      =   1995
               TabIndex        =   164
               Tag             =   "|EETA,DATE,EANS"
               Top             =   450
               Width           =   1995
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFFF&
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   9
                  Left            =   900
                  TabIndex        =   165
                  Text            =   "31.12.2009"
                  Top             =   0
                  Width           =   1065
               End
               Begin VB.Label AftFldLabel 
                  Alignment       =   1  'Right Justify
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "[A]"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000011&
                  Height          =   210
                  Index           =   10
                  Left            =   600
                  TabIndex        =   241
                  Top             =   45
                  Width           =   240
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "ETA"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   9
                  Left            =   0
                  TabIndex        =   166
                  Top             =   45
                  Width           =   315
               End
            End
         End
      End
      Begin VB.Frame fraFlightPanel 
         Caption         =   "Departure from SIN"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Index           =   4
         Left            =   120
         TabIndex        =   130
         Top             =   240
         Width           =   2895
         Begin VB.Frame fraFlightPanel 
            BackColor       =   &H000040C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame4"
            Height          =   840
            Index           =   7
            Left            =   30
            TabIndex        =   132
            Top             =   195
            Visible         =   0   'False
            Width           =   2820
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   8
               Left            =   2085
               ScaleHeight     =   315
               ScaleWidth      =   705
               TabIndex        =   159
               Tag             =   "|AIRB,TIME"
               Top             =   450
               Width           =   705
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   8
                  Left            =   0
                  TabIndex        =   160
                  Text            =   "12:10"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   " "
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   8
                  Left            =   0
                  TabIndex        =   161
                  Top             =   45
                  Width           =   45
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   7
               Left            =   105
               ScaleHeight     =   315
               ScaleWidth      =   1995
               TabIndex        =   156
               Tag             =   "|AIRB,DATE"
               Top             =   450
               Width           =   1995
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   7
                  Left            =   900
                  TabIndex        =   157
                  Text            =   "31.12.2009"
                  Top             =   0
                  Width           =   1065
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "AIB (OFF)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   7
                  Left            =   0
                  TabIndex        =   158
                  Top             =   45
                  Width           =   735
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   6
               Left            =   2085
               ScaleHeight     =   315
               ScaleWidth      =   705
               TabIndex        =   153
               Tag             =   "|OFBL,TIME"
               Top             =   120
               Width           =   705
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   6
                  Left            =   0
                  TabIndex        =   154
                  Text            =   "12:10"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   " "
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   6
                  Left            =   0
                  TabIndex        =   155
                  Top             =   45
                  Width           =   45
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   5
               Left            =   105
               ScaleHeight     =   315
               ScaleWidth      =   1995
               TabIndex        =   150
               Tag             =   "|OFBL,DATE"
               Top             =   120
               Width           =   1995
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   5
                  Left            =   900
                  TabIndex        =   151
                  Text            =   "31.12.2009"
                  Top             =   0
                  Width           =   1065
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "OFB (OUT)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   5
                  Left            =   0
                  TabIndex        =   152
                  Top             =   45
                  Width           =   810
               End
            End
         End
      End
      Begin VB.Frame fraFlightPanel 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   900
         Index           =   9
         Left            =   120
         TabIndex        =   162
         Top             =   1290
         Width           =   5910
         Begin VB.Frame fraFlightPanel 
            BackColor       =   &H000000C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame4"
            Height          =   705
            Index           =   10
            Left            =   30
            TabIndex        =   163
            Top             =   150
            Visible         =   0   'False
            Width           =   5835
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   3
               Left            =   90
               ScaleHeight     =   315
               ScaleWidth      =   5715
               TabIndex        =   237
               Top             =   360
               Width           =   5715
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   3
                  Left            =   900
                  TabIndex        =   238
                  Text            =   "DELAYED"
                  Top             =   0
                  Width           =   4755
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "SI Text DL"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   3
                  Left            =   0
                  TabIndex        =   239
                  Top             =   45
                  Width           =   810
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   21
               Left            =   2640
               ScaleHeight     =   315
               ScaleWidth      =   465
               TabIndex        =   187
               Top             =   30
               Width           =   465
               Begin VB.TextBox AftFldText 
                  Alignment       =   1  'Right Justify
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   21
                  Left            =   0
                  TabIndex        =   188
                  Text            =   "30"
                  Top             =   0
                  Width           =   345
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   " "
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   21
                  Left            =   0
                  TabIndex        =   189
                  Top             =   45
                  Width           =   45
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   20
               Left            =   2040
               ScaleHeight     =   315
               ScaleWidth      =   615
               TabIndex        =   184
               Top             =   30
               Width           =   615
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   20
                  Left            =   0
                  TabIndex        =   185
                  Text            =   " 0035"
                  Top             =   0
                  Width           =   585
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   ".."
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   20
                  Left            =   0
                  TabIndex        =   186
                  Top             =   45
                  Width           =   90
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   19
               Left            =   90
               ScaleHeight     =   315
               ScaleWidth      =   1965
               TabIndex        =   181
               Top             =   30
               Width           =   1965
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   19
                  Left            =   900
                  TabIndex        =   182
                  Text            =   " [DELAYED]"
                  Top             =   0
                  Width           =   1035
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "DL Status"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   19
                  Left            =   0
                  TabIndex        =   183
                  Top             =   45
                  Width           =   780
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   18
               Left            =   5100
               ScaleHeight     =   315
               ScaleWidth      =   705
               TabIndex        =   178
               Tag             =   "|DUR2,DLYT,DTD2,DTD2"
               Top             =   30
               Width           =   705
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   18
                  Left            =   0
                  TabIndex        =   179
                  Text            =   "0010"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "(M)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   18
                  Left            =   0
                  TabIndex        =   180
                  Top             =   45
                  Width           =   270
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   17
               Left            =   4440
               ScaleHeight     =   315
               ScaleWidth      =   675
               TabIndex        =   175
               Tag             =   "|DUR1,DLYT,DTD1,DTD1"
               Top             =   30
               Width           =   675
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   17
                  Left            =   0
                  TabIndex        =   176
                  Text            =   "0025"
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "DT"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   17
                  Left            =   0
                  TabIndex        =   177
                  Top             =   45
                  Width           =   210
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   16
               Left            =   3930
               ScaleHeight     =   315
               ScaleWidth      =   540
               TabIndex        =   172
               Tag             =   "|DCO2,CODE,DCD2,DCD2"
               Top             =   30
               Width           =   540
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   16
                  Left            =   0
                  MaxLength       =   2
                  TabIndex        =   173
                  Text            =   "MM"
                  Top             =   0
                  Width           =   495
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "(M)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   16
                  Left            =   0
                  TabIndex        =   174
                  Top             =   45
                  Width           =   270
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   15
               Left            =   3120
               ScaleHeight     =   315
               ScaleWidth      =   825
               TabIndex        =   169
               Tag             =   "|DCO1,CODE,DCD1,DCD1"
               Top             =   30
               Width           =   825
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   15
                  Left            =   300
                  MaxLength       =   2
                  TabIndex        =   170
                  Text            =   "MM"
                  Top             =   0
                  Width           =   495
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "DL"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   15
                  Left            =   0
                  TabIndex        =   171
                  Top             =   45
                  Width           =   210
               End
            End
         End
      End
      Begin VB.Frame fraFlightPanel 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   570
         Index           =   3
         Left            =   120
         TabIndex        =   214
         Top             =   2160
         Width           =   5910
         Begin VB.Frame fraFlightPanel 
            BackColor       =   &H000000C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame4"
            Height          =   375
            Index           =   15
            Left            =   30
            TabIndex        =   215
            Top             =   150
            Width           =   5835
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   39
               Left            =   5235
               ScaleHeight     =   315
               ScaleWidth      =   570
               TabIndex        =   219
               Tag             =   "|PXIN,PXNO,PAX2,PAX4"
               Top             =   30
               Width           =   570
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BackColor       =   &H00FFFFFF&
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   39
                  Left            =   0
                  MaxLength       =   3
                  TabIndex        =   220
                  Text            =   "123"
                  ToolTipText     =   "INFANTS"
                  Top             =   0
                  Width           =   510
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "(M)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   39
                  Left            =   0
                  TabIndex        =   221
                  Top             =   45
                  Width           =   270
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   40
               Left            =   4590
               ScaleHeight     =   315
               ScaleWidth      =   630
               TabIndex        =   222
               Top             =   30
               Width           =   630
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   40
                  Left            =   0
                  MaxLength       =   3
                  TabIndex        =   223
                  Text            =   "123"
                  ToolTipText     =   "NOT USED"
                  Top             =   0
                  Width           =   570
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "DT"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   40
                  Left            =   0
                  TabIndex        =   224
                  Top             =   45
                  Width           =   210
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   41
               Left            =   4005
               ScaleHeight     =   315
               ScaleWidth      =   630
               TabIndex        =   225
               Tag             =   "|PXIF,PXNO,PAX1,PAX2"
               Top             =   30
               Width           =   630
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   41
                  Left            =   0
                  MaxLength       =   3
                  TabIndex        =   226
                  Text            =   "123"
                  ToolTipText     =   "INCLDG INFANTS"
                  Top             =   0
                  Width           =   570
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "(M)"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   41
                  Left            =   0
                  TabIndex        =   227
                  Top             =   45
                  Width           =   270
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Height          =   315
               Index           =   42
               Left            =   3120
               ScaleHeight     =   315
               ScaleWidth      =   930
               TabIndex        =   228
               Tag             =   "|PXEF,PXNO,PAX3,PAX1"
               Top             =   30
               Width           =   930
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   42
                  Left            =   300
                  MaxLength       =   3
                  TabIndex        =   229
                  Text            =   "123"
                  ToolTipText     =   "EXCLDG INFANTS"
                  Top             =   0
                  Width           =   570
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "PX"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   42
                  Left            =   0
                  TabIndex        =   230
                  Top             =   45
                  Width           =   210
               End
            End
            Begin VB.PictureBox AftFldPanel 
               BackColor       =   &H000080FF&
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   315
               Index           =   38
               Left            =   90
               ScaleHeight     =   315
               ScaleWidth      =   3015
               TabIndex        =   216
               Top             =   30
               Width           =   3015
               Begin VB.TextBox AftFldText 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   38
                  Left            =   900
                  TabIndex        =   217
                  Text            =   "SUPPLIMANTARY PAX"
                  Top             =   0
                  Width           =   1995
               End
               Begin VB.Label AftFldLabel 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "SI Text PX"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   38
                  Left            =   0
                  TabIndex        =   218
                  Top             =   45
                  Width           =   810
               End
            End
         End
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   8
         Left            =   0
         Picture         =   "CreateTelex.frx":406C
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.CheckBox chkCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   13500
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkOutFormat 
      Caption         =   "Format"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   13170
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkUcase 
      Caption         =   "UC"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   12885
      Style           =   1  'Graphical
      TabIndex        =   22
      ToolTipText     =   "Convert text to upper case"
      Top             =   30
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.CheckBox chkLcase 
      Caption         =   "LC"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   12570
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Allow lower case typing"
      Top             =   30
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Frame fraTopButtons 
      BackColor       =   &H0000FF00&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Index           =   1
      Left            =   4080
      TabIndex        =   124
      Top             =   15
      Width           =   3000
      Begin VB.CheckBox chkText 
         Caption         =   "Text"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   389
         Top             =   0
         Width           =   645
      End
      Begin VB.PictureBox LockButtons 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1530
         ScaleHeight     =   315
         ScaleWidth      =   795
         TabIndex        =   251
         Top             =   0
         Width           =   795
         Begin VB.CheckBox chkSend 
            Caption         =   "Send"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   252
            Top             =   0
            Width           =   795
         End
      End
      Begin VB.CheckBox chkSave 
         Caption         =   "Save"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   126
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkPreView 
         Caption         =   "Preview"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   660
         Style           =   1  'Graphical
         TabIndex        =   125
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.PictureBox FltDlgCover 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   17190
      ScaleHeight     =   315
      ScaleWidth      =   465
      TabIndex        =   123
      Top             =   2610
      Width           =   525
   End
   Begin VB.PictureBox FltDlgCover 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   0
      Left            =   17190
      ScaleHeight     =   345
      ScaleWidth      =   465
      TabIndex        =   122
      Top             =   2160
      Width           =   525
   End
   Begin VB.PictureBox TplListPanel 
      BackColor       =   &H0000C0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   16020
      ScaleHeight     =   345
      ScaleWidth      =   1065
      TabIndex        =   120
      Top             =   2610
      Visible         =   0   'False
      Width           =   1125
      Begin TABLib.TAB TplListTab 
         Height          =   435
         Left            =   60
         TabIndex        =   121
         Top             =   60
         Width           =   1695
         _Version        =   65536
         _ExtentX        =   2990
         _ExtentY        =   767
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraOnline 
      BackColor       =   &H00FFC0C0&
      Caption         =   "Online Windows"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1485
      Left            =   5970
      TabIndex        =   116
      Top             =   4350
      Visible         =   0   'False
      Width           =   2055
      Begin VB.CheckBox chkOnline 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Pending Telexes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   120
         TabIndex        =   250
         Top             =   870
         Width           =   1785
      End
      Begin VB.CheckBox chkOnline 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Sent Telexes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   4
         Left            =   120
         TabIndex        =   119
         Top             =   1140
         Width           =   1785
      End
      Begin VB.CheckBox chkOnline 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Created Telexes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   2
         Left            =   120
         TabIndex        =   118
         Top             =   600
         Width           =   1785
      End
      Begin VB.CheckBox chkOnline 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Received Telexes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   120
         TabIndex        =   117
         Top             =   330
         Width           =   1755
      End
   End
   Begin VB.Frame fraFlightPanel 
      BackColor       =   &H00E0E0E0&
      Caption         =   "      Flight Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Index           =   0
      Left            =   2070
      TabIndex        =   92
      Tag             =   "VISIBLE"
      Top             =   420
      Width           =   6150
      Begin VB.Frame fraFlightPanel 
         BackColor       =   &H000040C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   1005
         Index           =   1
         Left            =   120
         TabIndex        =   133
         Top             =   240
         Width           =   5925
         Begin VB.TextBox txtESTI 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   4530
            Locked          =   -1  'True
            TabIndex        =   141
            TabStop         =   0   'False
            Text            =   "1250 22DEC09"
            Top             =   660
            Width           =   1380
         End
         Begin VB.TextBox txtSKED 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   4530
            Locked          =   -1  'True
            TabIndex        =   140
            TabStop         =   0   'False
            Text            =   "1235 22DEC09"
            Top             =   330
            Width           =   1380
         End
         Begin VB.TextBox txtAPC3 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   2730
            Locked          =   -1  'True
            TabIndex        =   139
            TabStop         =   0   'False
            Text            =   "WWW"
            Top             =   330
            Width           =   765
         End
         Begin VB.TextBox txtADID 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   570
            Locked          =   -1  'True
            TabIndex        =   138
            TabStop         =   0   'False
            Text            =   "DEPARTURE"
            Top             =   330
            Width           =   1230
         End
         Begin VB.TextBox txtREGN 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   4530
            TabIndex        =   137
            TabStop         =   0   'False
            Text            =   "9VABC"
            Top             =   0
            Width           =   1380
         End
         Begin VB.TextBox txtACTP 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   2730
            Locked          =   -1  'True
            TabIndex        =   136
            TabStop         =   0   'False
            Text            =   "B747A"
            Top             =   0
            Width           =   765
         End
         Begin VB.TextBox txtFLNO 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   570
            Locked          =   -1  'True
            TabIndex        =   135
            TabStop         =   0   'False
            Text            =   "DLH1234 Z"
            Top             =   0
            Width           =   1230
         End
         Begin VB.TextBox txtVIAL 
            Alignment       =   2  'Center
            Height          =   330
            Left            =   570
            Locked          =   -1  'True
            TabIndex        =   134
            TabStop         =   0   'False
            Top             =   660
            Width           =   2925
         End
         Begin VB.Label Label9 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "Estimated:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   3585
            TabIndex        =   149
            Top             =   720
            Width           =   930
         End
         Begin VB.Label Label8 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "Scheduled:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   3585
            TabIndex        =   148
            Top             =   390
            Width           =   930
         End
         Begin VB.Label Label7 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "Dest.:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   1890
            TabIndex        =   147
            Top             =   390
            Width           =   825
         End
         Begin VB.Label Label6 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "Type:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   0
            TabIndex        =   146
            Top             =   390
            Width           =   735
         End
         Begin VB.Label Label5 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "Reg.No.:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   3585
            TabIndex        =   145
            Top             =   60
            Width           =   930
         End
         Begin VB.Label Label4 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "A/C Type:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   1890
            TabIndex        =   144
            Top             =   60
            Width           =   825
         End
         Begin VB.Label Label3 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "Flight:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   0
            TabIndex        =   143
            Top             =   60
            Width           =   735
         End
         Begin VB.Label Label10 
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "Via:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   0
            TabIndex        =   142
            Top             =   720
            Width           =   735
         End
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   1
         Left            =   0
         Picture         =   "CreateTelex.frx":41B6
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.PictureBox EditFldPanel 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      FillColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   0
      Left            =   14610
      ScaleHeight     =   1215
      ScaleWidth      =   1380
      TabIndex        =   86
      Top             =   1800
      Visible         =   0   'False
      Width           =   1380
   End
   Begin VB.PictureBox EditFldPanel 
      BackColor       =   &H000040C0&
      BorderStyle     =   0  'None
      FillColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Index           =   1
      Left            =   11550
      ScaleHeight     =   2145
      ScaleWidth      =   3015
      TabIndex        =   66
      Top             =   1770
      Visible         =   0   'False
      Width           =   3015
      Begin VB.CheckBox chkEditPX 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   2610
         Picture         =   "CreateTelex.frx":4300
         Style           =   1  'Graphical
         TabIndex        =   315
         Top             =   675
         Width           =   405
      End
      Begin VB.TextBox txtEditDL 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   2430
         MaxLength       =   4
         TabIndex        =   301
         Top             =   1770
         Width           =   450
      End
      Begin VB.TextBox txtEditPX 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   2430
         MaxLength       =   3
         TabIndex        =   300
         Top             =   1410
         Width           =   450
      End
      Begin VB.TextBox txtEditEET 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   2430
         MaxLength       =   4
         TabIndex        =   299
         TabStop         =   0   'False
         Top             =   1050
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.CheckBox chkEditEET 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   298
         Top             =   15
         Width           =   405
      End
      Begin VB.CheckBox chkEditDL 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   2610
         Picture         =   "CreateTelex.frx":460A
         Style           =   1  'Graphical
         TabIndex        =   297
         TabStop         =   0   'False
         Top             =   345
         Width           =   405
      End
      Begin VB.CheckBox chkEditPX 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   296
         ToolTipText     =   "INFANTS"
         Top             =   675
         Width           =   450
      End
      Begin VB.TextBox txtEditPX 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1950
         MaxLength       =   3
         TabIndex        =   295
         Top             =   1410
         Width           =   450
      End
      Begin VB.TextBox txtEditEET 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1950
         MaxLength       =   4
         TabIndex        =   109
         TabStop         =   0   'False
         Top             =   1050
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.TextBox txtEditEET 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   1470
         MaxLength       =   4
         TabIndex        =   108
         TabStop         =   0   'False
         Top             =   1050
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.TextBox txtEditEET 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   990
         MaxLength       =   4
         TabIndex        =   107
         TabStop         =   0   'False
         Top             =   1050
         Width           =   450
      End
      Begin VB.TextBox txtEditEET 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   510
         MaxLength       =   4
         TabIndex        =   106
         TabStop         =   0   'False
         Top             =   1050
         Width           =   450
      End
      Begin VB.TextBox txtEditEET 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   30
         MaxLength       =   4
         TabIndex        =   105
         TabStop         =   0   'False
         Top             =   1050
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.CheckBox chkEditEET 
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1650
         Style           =   1  'Graphical
         TabIndex        =   91
         Top             =   15
         Width           =   360
      End
      Begin VB.CheckBox chkEditEET 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   2010
         Style           =   1  'Graphical
         TabIndex        =   90
         ToolTipText     =   "Next Station"
         Top             =   15
         Width           =   600
      End
      Begin VB.CheckBox chkEditEET 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1110
         Style           =   1  'Graphical
         TabIndex        =   89
         ToolTipText     =   "Minutes"
         Top             =   15
         Width           =   540
      End
      Begin VB.CheckBox chkEditEET 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   570
         Style           =   1  'Graphical
         TabIndex        =   88
         ToolTipText     =   "Hour/Minutes"
         Top             =   15
         Width           =   540
      End
      Begin VB.CheckBox chkEditEET 
         Caption         =   "EET"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   15
         Style           =   1  'Graphical
         TabIndex        =   87
         Top             =   15
         Width           =   555
      End
      Begin VB.TextBox txtEditDL 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   510
         MaxLength       =   2
         TabIndex        =   84
         Top             =   1770
         Width           =   450
      End
      Begin VB.TextBox txtEditDL 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   30
         MaxLength       =   4
         TabIndex        =   83
         TabStop         =   0   'False
         Top             =   1770
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.TextBox txtEditDL 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   990
         MaxLength       =   2
         TabIndex        =   82
         Top             =   1770
         Width           =   450
      End
      Begin VB.TextBox txtEditDL 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   1470
         MaxLength       =   4
         TabIndex        =   81
         Top             =   1770
         Width           =   450
      End
      Begin VB.TextBox txtEditDL 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   1950
         MaxLength       =   4
         TabIndex        =   80
         Top             =   1770
         Width           =   450
      End
      Begin VB.CheckBox chkEditDL 
         Caption         =   "DL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   15
         Style           =   1  'Graphical
         TabIndex        =   79
         Top             =   345
         Width           =   555
      End
      Begin VB.CheckBox chkEditDL 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   570
         Style           =   1  'Graphical
         TabIndex        =   78
         TabStop         =   0   'False
         ToolTipText     =   "DC1"
         Top             =   345
         Width           =   450
      End
      Begin VB.CheckBox chkEditDL 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   77
         TabStop         =   0   'False
         ToolTipText     =   "DC2"
         Top             =   345
         Width           =   450
      End
      Begin VB.CheckBox chkEditDL 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   76
         TabStop         =   0   'False
         ToolTipText     =   "DT1"
         Top             =   345
         Width           =   570
      End
      Begin VB.CheckBox chkEditDL 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   75
         TabStop         =   0   'False
         ToolTipText     =   "DT2"
         Top             =   345
         Width           =   570
      End
      Begin VB.TextBox txtEditPX 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   30
         MaxLength       =   4
         TabIndex        =   74
         TabStop         =   0   'False
         Top             =   1410
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.TextBox txtEditPX 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   510
         MaxLength       =   3
         TabIndex        =   73
         Top             =   1410
         Width           =   450
      End
      Begin VB.TextBox txtEditPX 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   990
         MaxLength       =   3
         TabIndex        =   72
         Top             =   1410
         Width           =   450
      End
      Begin VB.TextBox txtEditPX 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   1470
         MaxLength       =   3
         TabIndex        =   71
         Top             =   1410
         Width           =   450
      End
      Begin VB.CheckBox chkEditPX 
         Caption         =   "PX"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   15
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   675
         Width           =   555
      End
      Begin VB.CheckBox chkEditPX 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   570
         Style           =   1  'Graphical
         TabIndex        =   69
         ToolTipText     =   "EXCLDG INFANTS"
         Top             =   675
         Width           =   450
      End
      Begin VB.CheckBox chkEditPX 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1020
         Style           =   1  'Graphical
         TabIndex        =   68
         ToolTipText     =   "INCLDG INFANTS"
         Top             =   675
         Width           =   450
      End
      Begin VB.CheckBox chkEditPX 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   1470
         Style           =   1  'Graphical
         TabIndex        =   67
         ToolTipText     =   "UNUSED"
         Top             =   675
         Width           =   450
      End
   End
   Begin VB.CheckBox chkTempl 
      Caption         =   "Templ"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   12000
      Style           =   1  'Graphical
      TabIndex        =   61
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin TABLib.TAB FldPropTab 
      Height          =   375
      Index           =   0
      Left            =   8250
      TabIndex        =   58
      Top             =   7710
      Visible         =   0   'False
      Width           =   3285
      _Version        =   65536
      _ExtentX        =   5794
      _ExtentY        =   661
      _StockProps     =   64
   End
   Begin VB.Frame fraTelexText 
      BackColor       =   &H00E0E0E0&
      Caption         =   "     SI - Manual Input"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      Index           =   1
      Left            =   2070
      TabIndex        =   50
      Top             =   6540
      Width           =   6135
      Begin VB.TextBox txtSiText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   1
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   51
         Text            =   "CreateTelex.frx":4914
         Top             =   270
         Width           =   5955
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   7
         Left            =   0
         Picture         =   "CreateTelex.frx":4939
         Tag             =   "SHOW"
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "UfisServer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3060
      TabIndex        =   49
      Top             =   15
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.CheckBox chkSelect 
      Caption         =   "Flights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   11460
      Style           =   1  'Graphical
      TabIndex        =   48
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkEdit 
      Caption         =   "Edit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   10950
      Style           =   1  'Graphical
      TabIndex        =   47
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox txtTplDesc 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   17310
      TabIndex        =   40
      Top             =   360
      Visible         =   0   'False
      Width           =   3500
   End
   Begin VB.TextBox txtTplName 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   17310
      TabIndex        =   39
      Top             =   30
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Frame fraFlightData 
      BackColor       =   &H00E0E0E0&
      Caption         =   "     Telex Data Editor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5895
      Left            =   8250
      TabIndex        =   35
      Top             =   1770
      Visible         =   0   'False
      Width           =   3255
      Begin VB.CheckBox chkView 
         Caption         =   "Details"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   4
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   197
         Top             =   1635
         Width           =   705
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   115
         Top             =   270
         Width           =   720
      End
      Begin VB.CheckBox chkView 
         Caption         =   "Tmpl"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   3
         Left            =   2040
         Style           =   1  'Graphical
         TabIndex        =   114
         Top             =   1635
         Width           =   585
      End
      Begin VB.CheckBox chkView 
         Caption         =   "Dep"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   2
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   113
         Top             =   1635
         Width           =   585
      End
      Begin VB.CheckBox chkView 
         Caption         =   "Arr"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   1
         Left            =   2640
         Style           =   1  'Graphical
         TabIndex        =   112
         Top             =   1635
         Width           =   525
      End
      Begin VB.CheckBox chkView 
         Caption         =   "Editor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   1410
         Style           =   1  'Graphical
         TabIndex        =   111
         Top             =   1635
         Width           =   615
      End
      Begin VB.PictureBox FltListPanel 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2385
         Index           =   0
         Left            =   90
         ScaleHeight     =   2325
         ScaleWidth      =   3015
         TabIndex        =   110
         Top             =   3420
         Visible         =   0   'False
         Width           =   3075
         Begin VB.Timer AftTimer 
            Enabled         =   0   'False
            Interval        =   60000
            Left            =   30
            Top             =   1380
         End
         Begin VB.CheckBox chkSplit 
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   3.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Index           =   0
            Left            =   0
            MousePointer    =   7  'Size N S
            Style           =   1  'Graphical
            TabIndex        =   196
            TabStop         =   0   'False
            Top             =   1155
            Width           =   2220
         End
         Begin VB.PictureBox FltListPanel 
            AutoRedraw      =   -1  'True
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   885
            Index           =   2
            Left            =   -30
            ScaleHeight     =   825
            ScaleWidth      =   3030
            TabIndex        =   191
            Top             =   1320
            Width           =   3090
            Begin TABLib.TAB TabFlights 
               Height          =   555
               Index           =   1
               Left            =   0
               TabIndex        =   194
               Top             =   240
               Width           =   3030
               _Version        =   65536
               _ExtentX        =   5345
               _ExtentY        =   979
               _StockProps     =   64
            End
            Begin TABLib.TAB TabFlights 
               Height          =   555
               Index           =   0
               Left            =   0
               TabIndex        =   195
               Top             =   0
               Width           =   3030
               _Version        =   65536
               _ExtentX        =   5345
               _ExtentY        =   979
               _StockProps     =   64
            End
         End
         Begin VB.PictureBox FltListPanel 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1185
            Index           =   1
            Left            =   -30
            ScaleHeight     =   1125
            ScaleWidth      =   3030
            TabIndex        =   190
            Top             =   -30
            Width           =   3090
            Begin TABLib.TAB TlxFlights 
               Height          =   555
               Index           =   1
               Left            =   0
               TabIndex        =   192
               Top             =   300
               Width           =   3030
               _Version        =   65536
               _ExtentX        =   5345
               _ExtentY        =   979
               _StockProps     =   64
            End
            Begin TABLib.TAB TlxFlights 
               Height          =   555
               Index           =   0
               Left            =   0
               TabIndex        =   193
               Top             =   0
               Width           =   3030
               _Version        =   65536
               _ExtentX        =   5345
               _ExtentY        =   979
               _StockProps     =   64
            End
         End
      End
      Begin VB.VScrollBar VScroll1 
         Height          =   975
         Index           =   0
         LargeChange     =   240
         Left            =   2925
         SmallChange     =   24
         TabIndex        =   101
         TabStop         =   0   'False
         Top             =   1935
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.PictureBox FltInputPanel 
         AutoRedraw      =   -1  'True
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   90
         ScaleHeight     =   1395
         ScaleWidth      =   2775
         TabIndex        =   93
         Top             =   1920
         Visible         =   0   'False
         Width           =   2835
         Begin VB.PictureBox FltInputSlide 
            AutoRedraw      =   -1  'True
            BackColor       =   &H000040C0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1005
            Left            =   0
            ScaleHeight     =   1005
            ScaleWidth      =   2760
            TabIndex        =   94
            Top             =   -30
            Width           =   2760
            Begin VB.PictureBox FltEditPanel 
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   0
               ScaleHeight     =   360
               ScaleWidth      =   2760
               TabIndex        =   95
               TabStop         =   0   'False
               Top             =   15
               Width           =   2760
               Begin VB.PictureBox picFldRelIdx 
                  BackColor       =   &H0080C0FF&
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "Courier New"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   180
                  Index           =   0
                  Left            =   0
                  ScaleHeight     =   180
                  ScaleWidth      =   180
                  TabIndex        =   320
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   180
               End
               Begin VB.CheckBox chkFltCheck 
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   0
                  Left            =   1560
                  Style           =   1  'Graphical
                  TabIndex        =   98
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   180
               End
               Begin VB.TextBox txtFltInput 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   0
                  Left            =   1755
                  Locked          =   -1  'True
                  TabIndex        =   97
                  Text            =   "12MAY04"
                  Top             =   30
                  Width           =   975
               End
               Begin VB.CheckBox chkFltTick 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   210
                  Index           =   0
                  Left            =   60
                  TabIndex        =   96
                  Top             =   75
                  Width           =   195
               End
               Begin VB.Label lblFltField 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "AIRB"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   210
                  Index           =   0
                  Left            =   315
                  TabIndex        =   100
                  Top             =   75
                  Width           =   375
               End
               Begin VB.Label lblDatType 
                  Alignment       =   1  'Right Justify
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "DDHHMM"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   210
                  Index           =   0
                  Left            =   780
                  TabIndex        =   99
                  Top             =   75
                  Width           =   720
               End
            End
         End
      End
      Begin VB.PictureBox EditDlgPanel 
         BackColor       =   &H0000C000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   0
         Left            =   90
         ScaleHeight     =   1005
         ScaleWidth      =   3015
         TabIndex        =   65
         Top             =   570
         Visible         =   0   'False
         Width           =   3075
         Begin VB.PictureBox EditDlgPanel 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   690
            Index           =   1
            Left            =   0
            ScaleHeight     =   699.079
            ScaleMode       =   0  'User
            ScaleWidth      =   1140
            TabIndex        =   85
            Top             =   0
            Width           =   1140
         End
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Close"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   2310
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   270
         Width           =   855
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   270
         Width           =   750
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Save"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   270
         Width           =   750
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "TMPL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   2415
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   0
         Visible         =   0   'False
         Width           =   630
      End
      Begin VB.PictureBox LockButtons 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1590
         ScaleHeight     =   315
         ScaleWidth      =   705
         TabIndex        =   102
         Top             =   240
         Width           =   705
         Begin VB.CheckBox chkAddData 
            Caption         =   "D"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   345
            Style           =   1  'Graphical
            TabIndex        =   104
            Tag             =   "AD"
            Top             =   30
            Width           =   360
         End
         Begin VB.CheckBox chkAddData 
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   103
            Tag             =   "AD"
            Top             =   30
            Width           =   345
         End
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   5
         Left            =   0
         Picture         =   "CreateTelex.frx":4A83
         Tag             =   "SHOW"
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.TextBox NewCallCode 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   17310
      TabIndex        =   34
      Top             =   1350
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.TextBox NewSendUrno 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   17310
      TabIndex        =   33
      Top             =   1020
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.TextBox CurrentRecord 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   17310
      TabIndex        =   24
      Top             =   690
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.CheckBox chkReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   10350
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame fraPreview 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Telex Text Preview"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Left            =   30
      TabIndex        =   17
      Top             =   5280
      Visible         =   0   'False
      Width           =   1995
      Begin VB.CheckBox chkOutCnt 
         Caption         =   "01"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   59
         Top             =   240
         Width           =   420
      End
      Begin VB.TextBox txtPreview 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   0
         Left            =   90
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   18
         Top             =   570
         Width           =   1815
      End
      Begin VB.Label lblChrCnt 
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1785
         TabIndex        =   60
         Top             =   300
         Width           =   90
      End
   End
   Begin VB.Frame fraTlxPrio 
      Caption         =   "Priority"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   11550
      TabIndex        =   15
      Top             =   420
      Visible         =   0   'False
      Width           =   1965
      Begin TABLib.TAB TlxPrioTab 
         Height          =   975
         Left            =   90
         TabIndex        =   16
         Top             =   255
         Width           =   1800
         _Version        =   65536
         _ExtentX        =   3175
         _ExtentY        =   1720
         _StockProps     =   64
         Enabled         =   0   'False
      End
   End
   Begin VB.Frame fraDblSign 
      Caption         =   "Double Sign"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   13560
      TabIndex        =   13
      Top             =   420
      Visible         =   0   'False
      Width           =   2010
      Begin TABLib.TAB DblSignTab 
         Height          =   975
         Left            =   90
         TabIndex        =   14
         Top             =   255
         Width           =   1830
         _Version        =   65536
         _ExtentX        =   3228
         _ExtentY        =   1720
         _StockProps     =   64
         Enabled         =   0   'False
      End
   End
   Begin VB.Frame fraOrigAddr 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Originator"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   30
      TabIndex        =   11
      Top             =   420
      Width           =   1350
      Begin TABLib.TAB TlxOrigTab 
         Height          =   975
         Left            =   60
         TabIndex        =   12
         Top             =   255
         Width           =   1230
         _Version        =   65536
         _ExtentX        =   2170
         _ExtentY        =   1720
         _StockProps     =   64
         Enabled         =   0   'False
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   0
         Left            =   0
         Picture         =   "CreateTelex.frx":4BCD
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.Frame fraTlxType 
      BackColor       =   &H00E0E0E0&
      Caption         =   "     Telex Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   8250
      TabIndex        =   9
      Top             =   420
      Width           =   3270
      Begin TABLib.TAB TlxTypeTab 
         Height          =   975
         Left            =   90
         TabIndex        =   10
         Top             =   255
         Width           =   3090
         _Version        =   65536
         _ExtentX        =   5450
         _ExtentY        =   1720
         _StockProps     =   64
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   2
         Left            =   0
         Picture         =   "CreateTelex.frx":4D17
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.Frame fraAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Addresses"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3465
      Left            =   30
      TabIndex        =   5
      Top             =   1770
      Width           =   1350
      Begin TABLib.TAB AddressTab 
         Height          =   495
         Index           =   1
         Left            =   60
         TabIndex        =   43
         Top             =   2880
         Width           =   1230
         _Version        =   65536
         _ExtentX        =   2170
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "eMail"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   270
         Width           =   600
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "Telex"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   270
         Width           =   600
      End
      Begin TABLib.TAB AddressTab 
         Height          =   495
         Index           =   0
         Left            =   60
         TabIndex        =   6
         Top             =   2310
         Width           =   1230
         _Version        =   65536
         _ExtentX        =   2170
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin TABLib.TAB ApcTab 
         Height          =   495
         Left            =   60
         TabIndex        =   8
         Top             =   1740
         Width           =   1230
         _Version        =   65536
         _ExtentX        =   2170
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin TABLib.TAB AlcTab 
         Height          =   495
         Left            =   60
         TabIndex        =   7
         Top             =   1200
         Width           =   1230
         _Version        =   65536
         _ExtentX        =   2170
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin VB.Frame fraChkAddr 
         BackColor       =   &H000000C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   60
         TabIndex        =   26
         Top             =   585
         Width           =   1230
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Clear"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   64
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "All"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   1230
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   300
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Load"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1230
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   0
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Add"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   615
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   300
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Airp."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   615
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   0
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Airl."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   27
            Top             =   0
            Width           =   600
         End
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   3
         Left            =   0
         Picture         =   "CreateTelex.frx":4E61
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   8385
      Width           =   18810
      _ExtentX        =   33179
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   30083
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraDestAddr 
      BackColor       =   &H00E0E0E0&
      Caption         =   "      Destination Telex Addresses"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2850
      Index           =   0
      Left            =   2070
      TabIndex        =   2
      Top             =   1770
      Visible         =   0   'False
      Width           =   6135
      Begin VB.CheckBox chkWeb 
         Caption         =   "Web"
         Height          =   285
         Left            =   4380
         Style           =   1  'Graphical
         TabIndex        =   353
         Top             =   270
         Width           =   840
      End
      Begin VB.CheckBox chkFldAddr 
         Appearance      =   0  'Flat
         Caption         =   "E-MAIL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   270
         Width           =   1200
      End
      Begin VB.CheckBox chkFldAddr 
         Appearance      =   0  'Flat
         Caption         =   "SITA TYPE B"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   270
         Width           =   1200
      End
      Begin VB.CheckBox chkEdit2 
         Caption         =   "Editor"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5250
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   270
         Width           =   825
      End
      Begin VB.Frame fraDestAddr 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Caption         =   "Included Home Telex Addresses"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Index           =   1
         Left            =   30
         TabIndex        =   52
         Top             =   1680
         Width           =   6045
         Begin VB.TextBox txtDestAddr 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   825
            Index           =   3
            Left            =   3180
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   53
            Text            =   "CreateTelex.frx":4FAB
            Top             =   240
            Visible         =   0   'False
            Width           =   2805
         End
         Begin VB.TextBox txtDestAddr 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   825
            Index           =   2
            Left            =   60
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   54
            Text            =   "CreateTelex.frx":4FE0
            Top             =   240
            Width           =   3045
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Included Home Telex Addresses"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   90
            TabIndex        =   55
            Top             =   0
            Width           =   2700
         End
      End
      Begin VB.TextBox txtDestAddr 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   1
         Left            =   3210
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   45
         Text            =   "CreateTelex.frx":5017
         Top             =   570
         Visible         =   0   'False
         Width           =   2865
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Arrange"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   4380
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   270
         Width           =   840
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Merge"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   270
         Width           =   900
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Reload"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   990
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   270
         Width           =   900
      End
      Begin VB.TextBox txtDestAddr 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   0
         Left            =   60
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   3
         Text            =   "CreateTelex.frx":503C
         Top             =   570
         Width           =   3075
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   4
         Left            =   0
         Picture         =   "CreateTelex.frx":5061
         Tag             =   "SHOW"
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.Frame fraTelexText 
      BackColor       =   &H00E0E0E0&
      Caption         =   "     Telex Text Generated from Template"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1845
      Index           =   0
      Left            =   2070
      TabIndex        =   0
      Top             =   4620
      Width           =   6135
      Begin VB.CheckBox chkTlxTxt 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   391
         Top             =   0
         Visible         =   0   'False
         Width           =   360
      End
      Begin VB.Frame fraSiTxtLabel 
         BackColor       =   &H00FF00FF&
         BorderStyle     =   0  'None
         Height          =   105
         Index           =   1
         Left            =   3210
         TabIndex        =   308
         Top             =   1650
         Width           =   3135
      End
      Begin VB.Frame fraSiTxtLabel 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   306
         Top             =   1380
         Width           =   3015
         Begin VB.Label SiTxtLabel 
            AutoSize        =   -1  'True
            BackColor       =   &H008080FF&
            BackStyle       =   0  'Transparent
            Caption         =   "SI - Generated from Template"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   90
            TabIndex        =   307
            Top             =   0
            Width           =   2445
         End
      End
      Begin VB.TextBox txtSiText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   825
         Index           =   0
         Left            =   3210
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   46
         Text            =   "CreateTelex.frx":51AB
         Top             =   240
         Width           =   2835
      End
      Begin VB.TextBox txtTelexText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   0
         Left            =   60
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   1
         Top             =   360
         Width           =   3075
      End
      Begin VB.Image PanelIcon 
         Height          =   240
         Index           =   6
         Left            =   0
         Picture         =   "CreateTelex.frx":51F4
         Stretch         =   -1  'True
         Tag             =   "SHOW"
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
   End
   Begin VB.Frame fraTopButtons 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Index           =   0
      Left            =   -30
      TabIndex        =   127
      Top             =   -120
      Width           =   5535
      Begin VB.CommandButton Command4 
         Caption         =   "CCO"
         Height          =   315
         Left            =   3420
         TabIndex        =   388
         Top             =   120
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Test"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   2430
         Style           =   1  'Graphical
         TabIndex        =   266
         Top             =   345
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Explain"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   2430
         Style           =   1  'Graphical
         TabIndex        =   288
         Top             =   135
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CheckBox FixWin 
         Caption         =   "Arrange"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   249
         Top             =   135
         Width           =   855
      End
      Begin VB.CheckBox chkOnline 
         BackColor       =   &H0080C0FF&
         Caption         =   "Online"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   930
         Style           =   1  'Graphical
         TabIndex        =   248
         Top             =   135
         Width           =   765
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Setup"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   1710
         Style           =   1  'Graphical
         TabIndex        =   247
         Top             =   135
         Visible         =   0   'False
         Width           =   705
      End
   End
   Begin MSComctlLib.ImageList MyIcons 
      Left            =   1410
      Top             =   510
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   49
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":533E
            Key             =   "airbus"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":5658
            Key             =   "blackarrow"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":57B2
            Key             =   "edittlx"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":5D4C
            Key             =   "ask"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":6066
            Key             =   "bulb"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":6380
            Key             =   "hand"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":669A
            Key             =   "mobile"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":69B4
            Key             =   "netfail"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":6CCE
            Key             =   "note"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":6FE8
            Key             =   "phone"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":7302
            Key             =   "runway"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":761C
            Key             =   "stop"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":7936
            Key             =   "timer"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":7C50
            Key             =   "ufis32"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":7F6A
            Key             =   "ufis44"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":8284
            Key             =   "write"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":859E
            Key             =   "export"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":89F0
            Key             =   "postit"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":8D0A
            Key             =   "ask2"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":9024
            Key             =   "days"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":933E
            Key             =   "tools"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":9658
            Key             =   "edit"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":97B2
            Key             =   "edit2"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":9ACC
            Key             =   "keys"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":9F1E
            Key             =   "zoom"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":A238
            Key             =   "mail"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":A552
            Key             =   "props"
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":A6AC
            Key             =   "aid"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":A9C6
            Key             =   "setup"
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":AE18
            Key             =   "stop2"
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":B132
            Key             =   "calc"
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":B44C
            Key             =   "clock1"
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":B766
            Key             =   "clock2"
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":BA80
            Key             =   "warn1"
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":BD9A
            Key             =   "warn2"
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":C0B4
            Key             =   "snap1"
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":C20E
            Key             =   "snap2"
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":C528
            Key             =   "work"
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":C842
            Key             =   "check"
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":CB5C
            Key             =   "green"
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":CE76
            Key             =   "screen"
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":D190
            Key             =   "oops1"
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":D4AA
            Key             =   "oops2"
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":D7C4
            Key             =   "oops3"
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":DADE
            Key             =   "lock"
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":DDF8
            Key             =   "grobi"
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":F94A
            Key             =   "infomsg"
         EndProperty
         BeginProperty ListImage48 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":FA36
            Key             =   "query"
         EndProperty
         BeginProperty ListImage49 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CreateTelex.frx":FB5B
            Key             =   "stopit"
         EndProperty
      EndProperty
   End
   Begin VB.Image imgAmpel 
      Height          =   240
      Index           =   3
      Left            =   16560
      Picture         =   "CreateTelex.frx":FC7A
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgAmpel 
      Height          =   240
      Index           =   2
      Left            =   16290
      Picture         =   "CreateTelex.frx":FDC4
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgAmpel 
      Height          =   240
      Index           =   1
      Left            =   16020
      Picture         =   "CreateTelex.frx":FF0E
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgAmpel 
      Height          =   240
      Index           =   0
      Left            =   15750
      Picture         =   "CreateTelex.frx":10058
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picChkMark 
      Height          =   480
      Index           =   1
      Left            =   16050
      Picture         =   "CreateTelex.frx":101A2
      Top             =   330
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picChkMark 
      Height          =   480
      Index           =   0
      Left            =   15600
      Picture         =   "CreateTelex.frx":104AC
      Top             =   330
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "CreateTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_Snap As CSnapDialog

Dim AutoSendInitLoop As Boolean
Dim TelexIsCleanToSend As Boolean
Dim CurSetupIdx As Integer

Dim TplFldCreateEditor As Boolean
Dim TplMainFieldsOnly As Boolean
Dim TplUpdateFldGroups As Boolean
Dim TplUpdateCascading As Boolean
Dim TplUpdateSingleFld As Boolean
Dim ShowFldDsscColFlag As Boolean

Dim TestScenario As Integer
Dim ShowHorzSplit As Boolean
Dim ShowVertSplit As Boolean

Dim MeIsVisible As Boolean
Dim TransmitEnabled As Boolean
Dim AutoSendTelexAddress As String
Dim AutoSendEmailAddress As String
Dim ActiveSendAddress As String
Dim CurFltTabOcx As TABLib.Tab
Dim CurFltLineNo As Long
Dim CurFltRecIsAft As Boolean
Dim CurFltRecIsTlx As Boolean
Dim CurFltAdidIsArr As Boolean
Dim CurFltAdidIsDep As Boolean

Dim CurTabOcxFldLst As String

Dim CfgAftRecFldNames As String
Dim CfgTplRecFldNames As String
Dim CfgTplRecFldTypes As String
Dim CfgTplFldDatTypes As String
Dim CfgTplFldDatProps As String

Dim MyTlxTabFields As String
Dim MyTlxTabRecord As String
Dim MyAftTabFields As String
Dim MyAftTabRecord As String
Dim MyTlxTemplate As String
Dim CurTlxTplText As String
Dim CurTlxTplType As String
Dim CurTlxTplCorr As String
Dim CurTlxCorrCnt As Integer
Dim CurTplFldList As String

Dim CurViewPoint As String
Dim CurAftFltUrno As String
Dim CurTlxFltUrno As String
Dim CurTlxTabUrno As String
Dim CurTlxTplUrno As String
Dim CurNewTplText As String
Dim CurUsrTplText As String
Dim TplTextIsUser As Boolean

Dim SereCol As Integer
Dim TtypCol As Integer
Dim Txt1Col As Integer
Dim Txt2Col As Integer
Dim StatCol As Integer
Dim WstaCol As Integer
Dim PrflCol As Integer
Dim FlnuCol As Integer
Dim UrnoCol As Integer

Dim TplRuleLineNo As Long
Dim TplTextLineNo As Long
Dim TplText As String
Dim SystemInput As Boolean
Dim FltEditSize As Long
Dim OutTlxIdx As Integer
Dim KeepCurrentView As Boolean
Dim MouseIsDown As Boolean
Dim MouseX As Long
Dim MouseY As Long
Dim FlightIsOnTime As Boolean
Dim FlightIsDelayed As Boolean
Dim TemplateHasDelayLine As Boolean
Dim TemplateHasPaxLine As Boolean
Dim TelexTextIsClean As Boolean
Dim TelexAddrIsClean As Boolean
Dim TelexAddrIsWrong As Boolean
Dim TelexOrigIsClean As Boolean

Public Sub SetNewTlxFlightTab(LineNoFlag As Long, TlxRecFields As String, TlxRecData As String)
    Dim CurSere As String
    Dim CurText As String
    Dim CurTtyp As String
    Dim CurStat As String
    Dim CurWsta As String
    Dim CurTxt1 As String
    Dim CurTxt2 As String
    Dim TplUrno As String
    Dim MadTead As String
    Dim MadMail As String
    Dim TlxText As String
    Dim tmpData As String
    Dim AftUrnoCol As Integer
    SystemInput = True
    tmpData = ""
    MyTlxTabFields = TlxRecFields
    MyTlxTabRecord = TlxRecData
    SereCol = GetItemNo(MyTlxTabFields, "SERE")
    TtypCol = GetItemNo(MyTlxTabFields, "TTYP")
    Txt1Col = GetItemNo(MyTlxTabFields, "TXT1")
    Txt2Col = GetItemNo(MyTlxTabFields, "TXT2")
    PrflCol = GetItemNo(MyTlxTabFields, "PRFL")
    StatCol = GetItemNo(MyTlxTabFields, "STAT")
    WstaCol = GetItemNo(MyTlxTabFields, "WSTA")
    FlnuCol = GetItemNo(MyTlxTabFields, "FLNU")
    UrnoCol = GetItemNo(MyTlxTabFields, "URNO")
    CurTxt1 = GetItem(MyTlxTabRecord, Txt1Col, ",")
    MyAftTabFields = GetItem(CurTxt1, 1, Chr(30))
    MyAftTabFields = CleanString(MyAftTabFields, SERVER_TO_CLIENT, True)
    MyAftTabRecord = GetItem(CurTxt1, 2, Chr(30))
    MyAftTabRecord = CleanString(MyAftTabRecord, SERVER_TO_CLIENT, True)
    InsertTlxFlightRecord LineNoFlag, MyAftTabFields, MyAftTabRecord, MyTlxTabFields, MyTlxTabRecord
    SystemInput = False
    If (Not Me.Visible) Or (Me.WindowState = vbMinimized) Then Load TextShape
End Sub
Public Function OpenCreatedTelex(TlxUrno As String) As Boolean
    Dim HitList As String
    Dim TlxRecData As String
    Dim TlxFldList As String
    Dim TlxFldData As String
    Dim retval As Boolean
    Dim ColNo As Long
    Dim LineNo As Long
    Dim i As Integer
    Dim idx As Integer
    PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("blackarrow")).Picture
    PanelIcon(6).Tag = "SHOW"
    txtTelexText(0).BackColor = vbWhite
    txtTelexText(0).Locked = True
    If Me.Visible = False Then Me.Visible = True
    If Me.WindowState <> vbNormal Then Me.WindowState = vbNormal
    Me.ZOrder
    Me.Caption = Me.Tag & " [Online Auto MVT]"
    retval = False
    chkView(1).ForeColor = vbBlack
    chkView(2).ForeColor = vbBlack
    If retval = False Then
        i = 0
        ColNo = GetRealItemNo(TlxFlights(i).LogicalFieldList, "TURN")
        If ColNo >= 0 Then
            HitList = TlxFlights(i).GetLinesByColumnValue(ColNo, TlxUrno, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                idx = i
                retval = True
            End If
        End If
    End If
    If retval = False Then
        i = 1
        ColNo = GetRealItemNo(TlxFlights(i).LogicalFieldList, "TURN")
        If ColNo >= 0 Then
            HitList = TlxFlights(i).GetLinesByColumnValue(ColNo, TlxUrno, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                idx = i
                retval = True
            End If
        End If
    End If
    If retval = True Then
        TlxFlights(idx).SetCurrentSelection LineNo
        TlxFlights(idx).OnVScrollTo LineNo
    '    TlxRecData = TlxFlights(i).GetLineTag(LineNo)
    '    GetKeyItem TlxFldList, TlxRecData, "{=FLD=}", "{=/FLD=}"
    '    GetKeyItem TlxFldData, TlxRecData, "{=DAT=}", "{=/DAT=}"
    '    SetCreatedTelex 1, TlxFldList, TlxFldData
    End If
    OpenCreatedTelex = retval
End Function

Public Sub SetCreatedTelex(CurCaller As Integer, TlxRecFields As String, TlxRecData As String)
    Dim tmpData As String
    chkReset.Value = 1
    tmpData = ""
    MyTlxTabFields = TlxRecFields
    MyTlxTabRecord = TlxRecData
    SereCol = GetItemNo(MyTlxTabFields, "SERE")
    TtypCol = GetItemNo(MyTlxTabFields, "TTYP")
    Txt1Col = GetItemNo(MyTlxTabFields, "TXT1")
    Txt2Col = GetItemNo(MyTlxTabFields, "TXT2")
    PrflCol = GetItemNo(MyTlxTabFields, "PRFL")
    StatCol = GetItemNo(MyTlxTabFields, "STAT")
    WstaCol = GetItemNo(MyTlxTabFields, "WSTA")
    FlnuCol = GetItemNo(MyTlxTabFields, "FLNU")
    UrnoCol = GetItemNo(MyTlxTabFields, "URNO")
    HandleTlxRecord CurCaller
End Sub
Public Sub UpdateFlightRecord(LineFlag As Long, BcNum As String, DestName As String, RecvName As String, cpTable As String, cpCmd As String, cpSelKey As String, cpFields As String, cpData As String)
    Dim AftUrno As String
    Dim FldList As String
    Dim HitList As String
    Dim LineNo As Long
    Dim ColNo As Long
    If LineFlag = 0 Then
        'AFTTAB from BroadCast
        AftUrno = GetFieldValue("URNO", cpData, cpFields)
        If AftUrno <> "" Then
            FldList = cpFields
            FldList = Replace(FldList, "BAAA", "EETF", 1, -1, vbBinaryCompare)
            FldList = Replace(FldList, "ETAI", "EETA", 1, -1, vbBinaryCompare)
            FldList = Replace(FldList, "DCD1", "DCO1", 1, -1, vbBinaryCompare)
            FldList = Replace(FldList, "DCD2", "DCO2", 1, -1, vbBinaryCompare)
            FldList = Replace(FldList, "DTD1", "DUR1", 1, -1, vbBinaryCompare)
            FldList = Replace(FldList, "DTD2", "DUR2", 1, -1, vbBinaryCompare)
            ColNo = GetRealItemNo(TlxFlights(1).LogicalFieldList, "URNO")
            HitList = TlxFlights(1).GetLinesByColumnValue(ColNo, AftUrno, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                TlxFlights(1).SetFieldValues LineNo, FldList, cpData
                If LineNo = TlxFlights(1).GetCurrentSelected Then
                    TlxFlights(1).SetCurrentSelection LineNo
                End If
            End If
            FldList = cpFields
            ColNo = GetRealItemNo(TabFlights(1).LogicalFieldList, "URNO")
            HitList = TabFlights(1).GetLinesByColumnValue(ColNo, AftUrno, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                TabFlights(1).SetFieldValues LineNo, FldList, cpData
            End If
        End If
    End If
End Sub
Private Sub InsertTlxFlightRecord(LineNoFlag As Long, AftFldList As String, AftFldData As String, TlxFldList As String, TlxFldData As String)
    Dim AftAdid As String
    Dim AftUrno As String
    Dim TlxUrno As String
    Dim TlxType As String
    Dim tmpData As String
    Dim tmpNstn As String
    Dim tmpDes3 As String
    Dim NewRec As String
    Dim TlxValues As String
    Dim LineNo As Long
    Dim i As Integer
    CheckAftUrnoField
    AftAdid = GetFieldValue("ADID", AftFldData, AftFldList)
    Select Case AftAdid
        Case "A"
            i = 0
        Case "D"
            i = 1
        Case Else
            i = -1
    End Select
    If i >= 0 Then
        NewRec = CreateEmptyLine(TlxFlights(i).LogicalFieldList)
        If LineNoFlag = 0 Then
            LineNo = 0
            TlxFlights(i).InsertTextLineAt LineNo, NewRec, False
        Else
            TlxFlights(i).InsertTextLine NewRec, False
            LineNo = TlxFlights(i).GetLineCount - 1
        End If
        TlxFlights(i).SetFieldValues LineNo, AftFldList, AftFldData
        AftUrno = GetFieldValue("FLNU", TlxFldData, TlxFldList)
        TlxFlights(i).SetFieldValues LineNo, "URNO", AftUrno
        TlxUrno = GetFieldValue("URNO", TlxFldData, TlxFldList)
        TlxFlights(i).SetFieldValues LineNo, "TURN", TlxUrno
        TlxType = GetFieldValue("STYP", TlxFldData, TlxFldList)
        TlxFlights(i).SetFieldValues LineNo, "STYP", TlxType
        If InStr(AftFldList, "VIA3") = 0 Then
            tmpData = GetFieldValue("NHOP", AftFldData, AftFldList)
            TlxFlights(i).SetFieldValues LineNo, "NSTN", tmpData
        End If
        tmpNstn = TlxFlights(i).GetFieldValue(LineNo, "NSTN")
        tmpDes3 = TlxFlights(i).GetFieldValue(LineNo, "DES3")
        If (tmpNstn <> "") And (tmpNstn = tmpDes3) Then
            TlxFlights(i).SetFieldValues LineNo, "DES3", ""
        End If
        
        If chkTplPatch(5).Value = 1 Then
            'For Testing
            'TlxFlights(i).SetFieldValues LineNo, "DCO1,DCO2,DUR1,DUR2", "C1,C2,0010,0020"
            'TlxFlights(i).SetFieldValues LineNo, "DCO1,DCO2,DUR1,DUR2", ",,0000,0000"
            TlxFlights(i).SetFieldValues LineNo, "REGN", "9VBERNI"
            TlxFlights(i).SetFieldValues LineNo, "DCO1,DUR1", "89,0010"
            TlxFlights(i).SetFieldValues LineNo, "PXIN,PXEF,PXIF", "12,123,135"
        End If
        
        TlxValues = ""
        TlxValues = TlxValues & "{=FLD=}" & TlxFldList & "{=/FLD=}"
        TlxValues = TlxValues & "{=DAT=}" & TlxFldData & "{=/DAT=}"
        TlxFlights(i).SetLineTag LineNo, TlxValues
        TlxFlights(i).AutoSizeColumns
    End If
End Sub

Private Sub CheckAftUrnoField()
    Dim AftUrnoCol As Integer
    Dim AftFldCol As Integer
    Dim AftFldList As String
    Dim TlxFldList As String
    Dim AftFldName As String
    Dim TlxFldName As String
    Dim AftFldValue As String
    Dim itm As Integer
    If InStr(MyAftTabFields, "URNO") = 0 Then
        CurAftFltUrno = GetFieldValue("FLNU", MyTlxTabRecord, MyTlxTabFields)
        MyAftTabFields = MyAftTabFields & ",URNO"
        MyAftTabRecord = MyAftTabRecord & "," & CurAftFltUrno
    Else
        CurAftFltUrno = GetFieldValue("URNO", MyAftTabRecord, MyAftTabFields)
        If CurAftFltUrno = "" Then
            AftUrnoCol = GetItemNo(MyAftTabFields, "URNO")
            CurAftFltUrno = GetFieldValue("FLNU", MyTlxTabRecord, MyTlxTabFields)
            SetItem MyAftTabRecord, AftUrnoCol, ",", CurAftFltUrno
        End If
    End If
    AftFldList = "ALC3,FLTN,FLTS,FLDA,FKEY"
    TlxFldList = "ALC3,FLTN,FLNS,FLDA,FKEY"
    itm = 0
    AftFldName = "START"
    While AftFldName <> ""
        itm = itm + 1
        AftFldName = GetItem(AftFldList, itm, ",")
        TlxFldName = GetItem(TlxFldList, itm, ",")
        If AftFldName <> "" Then
            If InStr(MyAftTabFields, AftFldName) = 0 Then
                AftFldValue = GetFieldValue(TlxFldName, MyTlxTabRecord, MyTlxTabFields)
                MyAftTabFields = MyAftTabFields & "," & AftFldName
                MyAftTabRecord = MyAftTabRecord & "," & AftFldValue
            Else
                AftFldValue = GetFieldValue(AftFldName, MyAftTabRecord, MyAftTabFields)
                If AftFldValue = "" Then
                    AftFldCol = GetItemNo(MyAftTabFields, AftFldName)
                    AftFldValue = GetFieldValue(TlxFldName, MyTlxTabRecord, MyTlxTabFields)
                    SetItem MyAftTabRecord, AftUrnoCol, ",", CurAftFltUrno
                End If
            End If
        End If
    Wend
End Sub

Private Sub HandleTlxRecord(CurCaller As Integer)
    Dim CurSere As String
    Dim CurText As String
    Dim CurTtyp As String
    Dim CurStat As String
    Dim CurWsta As String
    Dim CurTxt1 As String
    Dim CurTxt2 As String
    Dim TplUrno As String
    Dim MadTead As String
    Dim MadMail As String
    Dim TlxText As String
    
    SystemInput = True
    Select Case CurCaller
        Case 2
            'CheckAftUrnoField
            CurTlxTabUrno = ""
            CurAftFltUrno = GetItem(MyTlxTabRecord, UrnoCol, ",")
            CurTlxFltUrno = CurAftFltUrno
            CurTxt1 = ""
            MyAftTabFields = MyTlxTabFields
            MyAftTabRecord = MyTlxTabRecord
            InitFlightInfoPanel 1, MyAftTabFields, MyAftTabRecord
            TplUrno = ""
            MadTead = ""
            MadMail = ""
            FillAddrField False, 0, MadTead, 1, MadMail
            GetAutoTemplate TplUrno
            CreateFlightDataPanel "", MyAftTabFields, MyAftTabRecord, MyTlxTemplate
            chkEdit.Value = 1
            TlxText = GetTlxTextFromTemplate(CurTlxTplText, TelexTextIsClean)
            CheckSendStatus
            If Not TplTextIsUser Then TlxText = CurNewTplText
            txtTelexText(0).Text = TlxText
            'txtTelexText(0).Refresh
            RefreshControls 1
            chkFldAddr(0).Value = 1
        Case Else
            CurTlxTabUrno = GetItem(MyTlxTabRecord, UrnoCol, ",")
            CurAftFltUrno = GetItem(MyTlxTabRecord, FlnuCol, ",")
            CurTlxFltUrno = CurAftFltUrno
            CurTxt1 = GetItem(MyTlxTabRecord, Txt1Col, ",")
            MyAftTabFields = GetItem(CurTxt1, 1, Chr(30))
            MyAftTabFields = CleanString(MyAftTabFields, SERVER_TO_CLIENT, True)
            MyAftTabRecord = GetItem(CurTxt1, 2, Chr(30))
            MyAftTabRecord = CleanString(MyAftTabRecord, SERVER_TO_CLIENT, True)
            InitFlightInfoPanel 1, MyAftTabFields, MyAftTabRecord
            CurTxt2 = GetItem(MyTlxTabRecord, Txt2Col, ",")
            TplUrno = GetItem(CurTxt2, 1, Chr(30))
            MadTead = GetItem(CurTxt2, 2, Chr(30))
            MadMail = GetItem(CurTxt2, 3, Chr(30))
            MadTead = CleanString(MadTead, SERVER_TO_CLIENT, True)
            MadMail = CleanString(MadMail, SERVER_TO_CLIENT, True)
            FillAddrField False, 0, MadTead, 1, MadMail
            GetAutoTemplate TplUrno
            CreateFlightDataPanel "", MyAftTabFields, MyAftTabRecord, MyTlxTemplate
            chkEdit.Value = 1
            TlxText = GetTlxTextFromTemplate(CurTlxTplText, TelexTextIsClean)
            CheckSendStatus
            If Not TplTextIsUser Then TlxText = CurNewTplText
            txtTelexText(0).Text = TlxText
            RefreshControls 1
            chkFldAddr(0).Value = 1
    End Select
    SystemInput = False

End Sub

Private Sub InitFlightInfoPanel(CurCaller As Integer, AftFldList As String, AftFldData As String)
    Dim NewSize As Long
    Dim OldSize As Long
    Dim tmpAdid As String
    Dim tmpData As String
    Dim IsReset As Boolean
    Dim i As Integer
    Dim TimeVal
    On Error Resume Next
    IsReset = False
    If AftFldList = "" Then IsReset = True
    If Not IsReset Then
        If fraFlightPanel(0).Tag = CurAftFltUrno Then
            Exit Sub
        End If
    End If
    
    txtVIAL.Left = txtFLNO.Left
    txtVIAL.Width = txtACTP.Left + txtACTP.Width - txtVIAL.Left
    fraFlightPanel(0).Tag = CurAftFltUrno
    txtFLNO.Text = GetFieldValue("FLNO", AftFldData, AftFldList)
    tmpData = Trim(GetFieldValue("ACT5", AftFldData, AftFldList))
    If tmpData = "" Then tmpData = Trim(GetFieldValue("ACT3", AftFldData, AftFldList))
    txtACTP.Text = tmpData
    tmpData = GetFieldValue("REGN", AftFldData, AftFldList)
    If (tmpData = "") And (CurFltLineNo >= 0) Then tmpData = CurFltTabOcx.GetFieldValue(CurFltLineNo, "REGN")
    tmpData = Trim(tmpData)
    If Len(tmpData) > 0 Then
        'Just for having a break point
        tmpData = Trim(tmpData)
    End If
    txtRegn.Text = tmpData
    txtRegn.Tag = txtRegn.Text
    tmpAdid = GetFieldValue("ADID", AftFldData, AftFldList)
    If tmpAdid = "" Then tmpAdid = "?"
    Select Case tmpAdid
        Case "A"
            CurFltAdidIsArr = True
            CurFltAdidIsDep = False
            txtADID.Text = "ARRIVAL"
            Label7.Caption = "Origin:"
            txtAPC3.Text = GetFieldValue("ORG3", AftFldData, AftFldList)
            tmpData = GetFieldValue("STOA", AftFldData, AftFldList)
            If tmpData <> "" Then
                TimeVal = CedaFullDateToVb(tmpData)
                tmpData = Format(TimeVal, "hhmm / ddmmm")
                tmpData = UCase(tmpData)
            End If
            txtSKED.Text = tmpData
            tmpData = Trim(GetFieldValue("ETAI", AftFldData, AftFldList))
            If tmpData <> "" Then
                TimeVal = CedaFullDateToVb(tmpData)
                tmpData = Format(TimeVal, "hhmm / ddmmm")
                tmpData = UCase(tmpData)
            End If
            txtESTI.Text = tmpData
            tmpData = RTrim(GetFieldValue("VIAL", AftFldData, AftFldList))
            If ((CurCaller = 2) Or (CurFltRecIsAft)) And (tmpData <> "") Then tmpData = GetShortViaList(tmpData)
            If tmpData = "" Then tmpData = Trim(GetFieldValue("VIA3", AftFldData, AftFldList))
            tmpData = Replace(tmpData, "/", " / ", 1, -1, vbBinaryCompare)
            txtVIAL.Text = tmpData
            tmpData = " " & tmpData & " "
            OldSize = txtVIAL.Width
            NewSize = TextWidth(tmpData) + 60
            If NewSize > OldSize Then
                txtVIAL.Width = NewSize
                txtVIAL.Left = txtVIAL.Left - (NewSize - OldSize)
            End If
            tmpData = GetFieldValue("VIA3", AftFldData, AftFldList)
            If tmpData = "" Then tmpData = GetFieldValue("ORG3", AftFldData, AftFldList)
            tmpData = GetFieldValue("DES3", AftFldData, AftFldList)
            chkAddData(1).Value = 0
            chkAddData(0).Value = 1
        Case "D"
            CurFltAdidIsArr = False
            CurFltAdidIsDep = True
            txtADID.Text = "DEPARTURE"
            Label7.Caption = "Dest.:"
            txtAPC3.Text = GetFieldValue("DES3", AftFldData, AftFldList)
            tmpData = Trim(GetFieldValue("STOD", AftFldData, AftFldList))
            If tmpData <> "" Then
                TimeVal = CedaFullDateToVb(tmpData)
                tmpData = Format(TimeVal, "hhmm / ddmmm")
                tmpData = UCase(tmpData)
            End If
            txtSKED.Text = tmpData
            tmpData = Trim(GetFieldValue("ETDI", AftFldData, AftFldList))
            If tmpData <> "" Then
                TimeVal = CedaFullDateToVb(tmpData)
                tmpData = Format(TimeVal, "hhmm / ddmmm")
                tmpData = UCase(tmpData)
            End If
            txtESTI.Text = tmpData
            tmpData = RTrim(GetFieldValue("VIAL", AftFldData, AftFldList))
            If ((CurCaller = 2) Or (CurFltRecIsAft)) And (tmpData <> "") Then tmpData = GetShortViaList(tmpData)
            If tmpData = "" Then tmpData = Trim(GetFieldValue("VIA3", AftFldData, AftFldList))
            tmpData = Replace(tmpData, "/", " / ", 1, -1, vbBinaryCompare)
            txtVIAL.Text = tmpData
            tmpData = " " & tmpData & " "
            OldSize = txtVIAL.Width
            NewSize = TextWidth(tmpData) + 60
            If NewSize > OldSize Then
                txtVIAL.Width = NewSize
                txtVIAL.Left = txtVIAL.Left - (NewSize - OldSize)
            End If
            tmpData = Trim(GetFieldValue("EETD", AftFldData, AftFldList))
            tmpData = GetFieldValue("ORG3", AftFldData, AftFldList)
            chkAddData(0).Value = 0
            chkAddData(1).Value = 1
        Case "?"
            CurFltAdidIsArr = False
            CurFltAdidIsDep = False
            txtFLNO.Text = ""
            txtADID.Text = ""
            Label7.Caption = "Orig/Dest:"
            txtAPC3.Text = ""
            txtSKED.Text = ""
            txtVIAL.Text = ""
            txtRegn.Text = ""
            txtESTI.Text = ""
        Case Else
            CurFltAdidIsArr = False
            CurFltAdidIsDep = False
            txtADID.Text = ""
            Label7.Caption = "Dest.:"
            If AftFldList <> "" Then txtADID.Text = "NOT CLEAR"
            txtAPC3.Text = GetFieldValue("DES3", AftFldData, AftFldList)
            tmpData = Trim(GetFieldValue("STOD", AftFldData, AftFldList))
            If tmpData <> "" Then
                TimeVal = CedaFullDateToVb(tmpData)
                tmpData = Format(TimeVal, "hhmm / ddmmm")
                tmpData = UCase(tmpData)
            End If
            txtSKED.Text = tmpData
            tmpData = Trim(GetFieldValue("ETDI", AftFldData, AftFldList))
            If tmpData <> "" Then
                TimeVal = CedaFullDateToVb(tmpData)
                tmpData = Format(TimeVal, "hhmm / ddmmm")
                tmpData = UCase(tmpData)
            End If
            txtESTI.Text = tmpData
            tmpData = GetFieldValue("VIAL", AftFldData, AftFldList)
            txtVIAL.Text = tmpData
            tmpData = " " & tmpData & " "
            OldSize = txtVIAL.Width
            NewSize = TextWidth(tmpData) + 60
            If NewSize > OldSize Then
                txtVIAL.Width = NewSize
                txtVIAL.Left = txtVIAL.Left - (NewSize - OldSize)
            End If
            chkAddData(0).Value = 0
            chkAddData(1).Value = 0
    End Select
    If (Not IsReset) And (Trim(txtRegn.Text = "")) Then
        txtRegn.BackColor = LightYellow
        txtRegn.Locked = False
    Else
        txtRegn.BackColor = vbWhite
        txtRegn.Locked = True
    End If
    InitFlightInfoDetails CurCaller, AftFldList, AftFldData
    FillTlxAddrSearchKeys
End Sub
Private Sub FillTlxAddrSearchKeys()
    Dim tmpAlc3 As String
    Dim tmpApc3 As String
    Dim Apc3List As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim itm As Integer
    tmpAlc3 = txtFLNO.Text
    tmpAlc3 = Left(tmpAlc3, 3)
    tmpAlc3 = Trim(tmpAlc3)
    'chkMvtAddr(0).Value = 1
    'chkMvtAddr(0).Value = 0
    AlcTab.SetColumnValue 0, 0, tmpAlc3
    AlcTab.Refresh
    Apc3List = txtAPC3.Text
    tmpApc3 = Trim(txtVIAL.Text)
    If tmpApc3 <> "" Then Apc3List = Apc3List & "/" & tmpApc3
    Apc3List = Replace(Apc3List, " ", "", 1, -1, vbBinaryCompare)
    LineNo = 0
    ColNo = 0
    itm = 0
    tmpApc3 = "START"
    While tmpApc3 <> ""
        itm = itm + 1
        tmpApc3 = GetItem(Apc3List, itm, "/")
        If tmpApc3 <> "" Then
            ApcTab.SetColumnValue LineNo, ColNo, tmpApc3
            ColNo = ColNo + 1
            If ColNo > 1 Then
                LineNo = LineNo + 1
                ColNo = 0
            End If
        End If
    Wend
    ApcTab.Refresh
    chkMvtAddr(3).Value = 1
End Sub
Private Sub InitFlightInfoDetails(CurCaller As Integer, AftFldList As String, AftFldData As String)
    Dim AftAdid As String
    Dim AftOrg3 As String
    Dim AftDes3 As String
    Dim AftPrvStn As String
    Dim AftNxtStn As String
    Dim ObjProp As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim DatType As String
    Dim RelType As String
    Dim FldValue As String
    Dim DspValue As String
    Dim tmpValue As String
    Dim ObjItmNo As Integer
    Dim ShowPanel As Boolean
    Dim IsReset As Boolean
    Dim LineNo As Long
    Dim RelIdx As Integer
    Dim i As Integer
    Dim TimeVal
    On Error Resume Next
    IsReset = False
    If AftFldList = "" Then IsReset = True
    ShowPanel = False
    'If chkView(1).Value = 1 Then ShowPanel = True
    'If chkView(2).Value = 1 Then ShowPanel = True
    'If chkView(4).Value = 1 Then ShowPanel = True
    'If ShowPanel Then fraFlightCover(0).Visible = True
    'fraFlightPanel(2).Visible = False
    AftAdid = GetFieldValue("ADID", AftFldData, AftFldList)
    If AftAdid = "" Then AftAdid = "?"
    AftOrg3 = GetFieldValue("ORG3", AftFldData, AftFldList)
    AftDes3 = GetFieldValue("DES3", AftFldData, AftFldList)
    AftPrvStn = GetPrvStation(AftFldList, AftFldData)
    AftNxtStn = GetNxtStation(AftFldList, AftFldData)
    Select Case AftAdid
        Case "A"
            ObjItmNo = 1
            fraFlightPanel(11).Caption = "Departure from " & AftPrvStn
            fraFlightPanel(13).Caption = "Arrival at " & AftDes3
            fraFlightPanel(11).Visible = True
            fraFlightPanel(13).Visible = True
            fraFlightPanel(4).Visible = False
            fraFlightPanel(5).Visible = False
            fraFlightPanel(10).Visible = False
            fraFlightPanel(15).Visible = False
        Case "D"
            ObjItmNo = 2
            fraFlightPanel(4).Caption = "Departure from " & AftOrg3
            fraFlightPanel(5).Caption = "Arrival at " & AftNxtStn
            AftFldLabel(1).Caption = "EET to " & AftNxtStn
            txtEditEET(3).Text = AftNxtStn
            fraFlightPanel(4).Visible = True
            fraFlightPanel(5).Visible = True
            fraFlightPanel(10).Visible = True
            fraFlightPanel(15).Visible = True
            fraFlightPanel(11).Visible = False
            fraFlightPanel(13).Visible = False
        Case "?"
            If chkView(1).Value = 1 Then
                ObjItmNo = 1
                fraFlightPanel(11).Caption = "Departure"
                fraFlightPanel(13).Caption = "Arrival"
                fraFlightPanel(11).Visible = True
                fraFlightPanel(13).Visible = True
                fraFlightPanel(4).Visible = False
                fraFlightPanel(5).Visible = False
                fraFlightPanel(10).Visible = False
                fraFlightPanel(15).Visible = False
            End If
            If chkView(2).Value = 1 Then
                ObjItmNo = 2
                fraFlightPanel(4).Caption = "Departure"
                fraFlightPanel(5).Caption = "Arrival"
                AftFldLabel(1).Caption = "EET"
                txtEditEET(3).Text = ""
                fraFlightPanel(4).Visible = True
                fraFlightPanel(5).Visible = True
                fraFlightPanel(6).Visible = False
                fraFlightPanel(7).Visible = False
                fraFlightPanel(8).Visible = False
                fraFlightPanel(10).Visible = False
                fraFlightPanel(15).Visible = False
                fraFlightPanel(11).Visible = False
                fraFlightPanel(13).Visible = False
            End If
        Case Else
            ObjItmNo = 2
            fraFlightPanel(4).Visible = False
            fraFlightPanel(5).Visible = False
            fraFlightPanel(10).Visible = False
            fraFlightPanel(15).Visible = False
            fraFlightPanel(11).Visible = False
            fraFlightPanel(13).Visible = False
    End Select
    For i = 0 To AftFldPanel.UBound
        ObjProp = AftFldPanel(i).Tag
        FldProp = GetItem(ObjProp, ObjItmNo, "|")
        FldName = GetItem(FldProp, 1, ",")
        DatType = GetItem(FldProp, 2, ",")
        FldType = GetItem(FldProp, 3, ",")
        RelType = GetItem(FldProp, 4, ",")
        If FldName = "" Then FldName = "----"
        If FldType = "" Then FldType = "DATA"
        If DatType = "" Then DatType = FldType
        If RelType = "" Then RelType = "----"
        FldValue = GetFieldValue(FldName, AftFldData, AftFldList)
        Select Case FldType
            Case "EETH"
                If FldValue = "" Then
                    If Not IsReset Then
                        If CurFltLineNo >= 0 Then
                            If CurFltRecIsTlx Then FldValue = Trim(CurFltTabOcx.GetFieldValue(CurFltLineNo, "EETF"))
                            If CurFltRecIsAft Then FldValue = Trim(CurFltTabOcx.GetFieldValue(CurFltLineNo, "BAAA"))
                        End If
                    End If
                End If
                txtEditEET(2).Tag = ""
                SetEETMinutes FldValue
                FldValue = txtEditEET(1).Text
                'If ((Not IsReset) And (FldValue = "")) Then chkEditEET(1).Value = 1 Else chkEditEET(1).Value = 0
            Case "EETM"
                FldValue = txtEditEET(2).Text
            Case "DCD1"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "DCO1")
            Case "DCD2"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "DCO2")
            Case "DTD1"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "DUR1")
            Case "DTD2"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "DUR2")
            Case "PAX1"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "PXIF")
            Case "PAX2"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "PXIN")
            Case "PAX3"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "PXEF")
            Case "PAX4"
                If CurFltLineNo >= 0 Then FldValue = CurFltTabOcx.GetFieldValue(CurFltLineNo, "PXIN")
            Case Else
            DspValue = FldValue
        End Select
        Select Case DatType
            Case "DATE"
                DspValue = FldValue
                If FldValue <> "" Then
                    tmpValue = Left(FldValue, 8)
                    TimeVal = CedaFullDateToVb(tmpValue)
                    tmpValue = Format(TimeVal, MySetUp.DefDateFormat)
                    DspValue = tmpValue
                End If
            Case "TIME"
                DspValue = FldValue
                If FldValue <> "" Then
                    tmpValue = FldValue
                    TimeVal = CedaFullDateToVb(tmpValue)
                    tmpValue = Format(TimeVal, "hhmm")
                    DspValue = tmpValue
                End If
            Case "DLYT"
                DspValue = FldValue
                If Val(DspValue) = 0 Then DspValue = ""
            Case "PXNO"
                DspValue = FldValue
                'If Val(DspValue) = 0 Then DspValue = ""
            Case Else
                DspValue = FldValue
        End Select
        'FOR TESTING
        If Not IsReset Then
            If chkTestCase.Value = 1 Then
                Select Case RelType
                    Case "DCD1"
                        DspValue = AftTestValue(2).Text
                    Case "DCD2"
                        DspValue = AftTestValue(3).Text
                    Case "DTD1"
                        DspValue = AftTestValue(4).Text
                    Case "DTD2"
                        DspValue = AftTestValue(5).Text
                    Case Else
                End Select
            End If
        End If
        Select Case RelType
            Case "DCD1"
                If DspValue = "" Then
                    DspValue = "--"
                End If
            Case "DCD2"
                'DspValue = AftTestValue(3).Text
            Case "DTD1"
                'DspValue = AftTestValue(4).Text
            Case "DTD2"
                'DspValue = AftTestValue(5).Text
            Case "PAX1", "PAX2", "PAX3", "PAX4"
                    DspValue = DspValue
            Case Else
        End Select
        AftFldText(i).Text = DspValue
        AftFldText(i).Tag = DspValue
    Next
    If Not IsReset Then
        CalculateEans
        CalculateDelay
    End If
    SetTplDetailPanels
    'If (ShowPanel) And (AftAdid <> "?") Then
    '    fraFlightCover(0).Visible = True
    '    fraFlightPanel(2).Visible = True
    'Else
    '    fraFlightPanel(2).Visible = False
    '    fraFlightCover(0).Visible = False
    'End If
End Sub

Private Sub SetTplDetailPanels()
    fraFlightPanel(2).Caption = "Flight Details [" & CurTlxTplType & "]"
    Select Case CurTlxTplType
        Case "AD"
            fraFlightPanel(4).Visible = True
            fraFlightPanel(5).Visible = True
            fraFlightPanel(6).Visible = False
            fraFlightPanel(7).Visible = True
            fraFlightPanel(8).Visible = True
            fraFlightPanel(10).Visible = True
            fraFlightPanel(15).Visible = True
            fraFlightPanel(11).Visible = False
            fraFlightPanel(13).Visible = False
            fraFlightPanel(4).Refresh
            fraFlightPanel(5).Refresh
            fraFlightPanel(9).Refresh
            fraFlightPanel(3).Refresh
        Case "ED"
            fraFlightPanel(4).Visible = True
            fraFlightPanel(5).Visible = True
            fraFlightPanel(6).Visible = True
            fraFlightPanel(7).Visible = False
            fraFlightPanel(8).Visible = True
            fraFlightPanel(10).Visible = True
            fraFlightPanel(15).Visible = False
            fraFlightPanel(11).Visible = False
            fraFlightPanel(13).Visible = False
            fraFlightPanel(4).Refresh
            fraFlightPanel(5).Refresh
            fraFlightPanel(9).Refresh
            fraFlightPanel(3).Refresh
        Case Else
            fraFlightPanel(2).Caption = "Flight Details"
            If CurFltAdidIsDep Then
                fraFlightPanel(4).Visible = True
                fraFlightPanel(5).Visible = True
                fraFlightPanel(6).Visible = True
                fraFlightPanel(7).Visible = False
                fraFlightPanel(8).Visible = True
                fraFlightPanel(10).Visible = True
                fraFlightPanel(15).Visible = True
                fraFlightPanel(11).Visible = False
                fraFlightPanel(13).Visible = False
                fraFlightPanel(4).Refresh
                fraFlightPanel(5).Refresh
                fraFlightPanel(9).Refresh
                fraFlightPanel(3).Refresh
            End If
    End Select
End Sub
Private Function GetShortViaList(AftVial As String) As String
    Dim TlxVial As String
    Dim CurApc3 As String
    Dim ApcPos As Integer
    Dim MaxPos As Integer
    TlxVial = ""
    ApcPos = 2
    MaxPos = Len(AftVial)
    CurApc3 = "START"
    While CurApc3 <> ""
        CurApc3 = ""
        If ApcPos < MaxPos Then CurApc3 = Trim(Mid(AftVial, ApcPos, 3))
        If CurApc3 <> "" Then TlxVial = TlxVial & CurApc3 & "/"
        ApcPos = ApcPos + 120
    Wend
    MaxPos = Len(TlxVial) - 1
    If MaxPos > 0 Then TlxVial = Left(TlxVial, MaxPos)
    GetShortViaList = TlxVial
End Function
Private Function GetNxtStation(AftFldList As String, AftFldData As String) As String
    Dim NxtApc3 As String
    NxtApc3 = GetFieldValue("VIA3", AftFldData, AftFldList)
    If NxtApc3 = "" Then NxtApc3 = Left(Trim(GetFieldValue("VIAL", AftFldData, AftFldList)), 3)
    If NxtApc3 = "" Then NxtApc3 = GetFieldValue("DES3", AftFldData, AftFldList)
    GetNxtStation = NxtApc3
End Function
Private Function GetPrvStation(AftFldList As String, AftFldData As String) As String
    Dim PrvApc3 As String
    PrvApc3 = GetFieldValue("VIA3", AftFldData, AftFldList)
    If PrvApc3 = "" Then PrvApc3 = GetFieldValue("ORG3", AftFldData, AftFldList)
    GetPrvStation = PrvApc3
End Function
Private Sub GetAutoTemplate(UseUrno As String)
    Dim tmpTmpl As String
    Dim tmpType As String
    Dim tmpCode As String
    Dim tmpAlc3 As String
    Dim tmpAdid As String
    Dim tmpHmte As String
    Dim tmpHmem As String
    Dim tmpRfld As String
    Dim TlxCorCnt As Integer
    Dim LineNo As Long
    TplRuleLineNo = -1
    TplTextLineNo = -1
    If UseUrno <> "" Then
        UfisServer.BasicData(3).SetInternalLineBuffer True
        LineNo = Val(UfisServer.BasicData(3).GetLinesByIndexValue("URNO", UseUrno, 0))
        While LineNo >= 0
            LineNo = UfisServer.BasicData(3).GetNextResultLine
            If LineNo >= 0 Then
                TplRuleLineNo = LineNo
                TplTextLineNo = LineNo
            End If
        Wend
        UfisServer.BasicData(3).SetInternalLineBuffer False
    End If
    If TplRuleLineNo > 0 Then
        TplTextLineNo = TplRuleLineNo
        tmpAlc3 = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "ALC3"))
        tmpType = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "TYPE"))
        tmpAdid = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "ADID"))
        tmpHmte = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "HMTE"))
        tmpHmem = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "HMEM"))
        tmpTmpl = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "TMPL"))
        tmpRfld = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "RFLD"))
        If tmpTmpl = "" Then
            tmpCode = Left("#" & tmpType & tmpType & "        ", 6) & tmpAdid
            tmpTmpl = UfisServer.BasicData(3).GetLinesByColumnValue(0, tmpCode, 0)
            If tmpTmpl <> "" Then
                LineNo = Val(tmpTmpl)
                tmpTmpl = Trim(UfisServer.BasicData(3).GetFieldValue(LineNo, "TMPL"))
                tmpRfld = Trim(UfisServer.BasicData(3).GetFieldValue(LineNo, "RFLD"))
                TplTextLineNo = LineNo
            End If
        End If
        tmpTmpl = CleanString(tmpTmpl, SERVER_TO_CLIENT, True)
        'Due to malfunction in template config tool with NewLines
        tmpTmpl = Replace(tmpTmpl, vbCr, "", 1, -1, vbBinaryCompare)
        tmpTmpl = Replace(tmpTmpl, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
        If tmpTmpl = "" Then TplTextLineNo = -1
    End If
    tmpTmpl = UCase(tmpTmpl)
    MyTlxTemplate = tmpTmpl
    CurTlxTplType = tmpType
    CurTplFldList = tmpRfld
    CurTlxTplUrno = UseUrno
    CurTlxTplCorr = ""
    CurTlxCorrCnt = -1
    TlxCorCnt = OutTelex.GetMvtCorCount(CurAftFltUrno, "MVT,COR", tmpType)
    If TlxCorCnt > 0 Then CurTlxTplCorr = "COR"
    SetTelexDefault TlxTypeTab, 1, tmpType, CurTlxTplCorr
    tmpHmte = Replace(tmpHmte, " ", ",", 1, -1, vbBinaryCompare)
    tmpHmem = Replace(tmpHmem, " ", ",", 1, -1, vbBinaryCompare)
    FillAddrField False, 2, tmpHmte, 3, tmpHmem
    CalculateEans
    CalculateDelay
    SetTplDetailPanels
End Sub
Private Function GetTemplateUrnos(UseAdid As String, UseAlc3 As String) As String
    Dim TplUrnoList As String
    Dim TplUrno As String
    Dim TplAdid As String
    Dim tmpCode As String
    Dim TplLineNo As Long
    Dim LineNo As Long
    Dim ColNo As Long
    TplUrnoList = ""
    If UseAlc3 <> "" Then
        UfisServer.BasicData(3).SetInternalLineBuffer True
        tmpCode = Trim(UseAlc3)
        If tmpCode <> "" Then
            If Len(tmpCode) = 3 Then ColNo = 2 Else ColNo = 1
            LineNo = Val(UfisServer.BasicData(3).GetLinesByColumnValue(ColNo, tmpCode, 0))
            While LineNo >= 0
                LineNo = UfisServer.BasicData(3).GetNextResultLine
                If LineNo >= 0 Then
                    TplAdid = UfisServer.BasicData(3).GetFieldValue(LineNo, "ADID")
                    If TplAdid = UseAdid Then
                        TplUrno = UfisServer.BasicData(3).GetFieldValue(LineNo, "URNO")
                        TplUrnoList = TplUrnoList & TplUrno & ","
                    End If
                End If
            Wend
        End If
        UfisServer.BasicData(3).SetInternalLineBuffer False
    End If
    If Len(TplUrnoList) > 0 Then TplUrnoList = Left(TplUrnoList, Len(TplUrnoList) - 1)
    GetTemplateUrnos = TplUrnoList
End Function
Private Sub AddressTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpBeme As String
    If (Selected) And (LineNo >= 0) Then
        tmpBeme = AddressTab(Index).GetColumnValue(LineNo, 3)
        tmpBeme = CleanString(tmpBeme, SERVER_TO_CLIENT, True)
        StatusBar1.Panels(2).Text = tmpBeme
    End If
End Sub

Private Sub AddressTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim LineStatus As Long
    Dim tmpTead As String
    Dim tmpTxt As String
    If LineNo >= 0 Then
        tmpTead = AddressTab(Index).GetColumnValue(LineNo, 2)
        LineStatus = AddressTab(Index).GetLineStatusValue(LineNo)
        If LineStatus = 0 Then
            SetAddrMarker Index, LineNo, tmpTead, True
        Else
            SetAddrMarker Index, LineNo, tmpTead, False
            If Index = 0 Then
                tmpTxt = txtDestAddr(0).Text
                tmpTxt = Replace(tmpTxt, tmpTead, "", 1, -1, vbBinaryCompare)
                txtDestAddr(0).Text = tmpTxt
                txtDestAddr(0).Text = GetCleanAddrList(txtDestAddr(0).Text, 0, " ", 4, "", False)
            End If
        End If
        'FillAddrField False, 0, "", 1, ""
    ElseIf LineNo = -1 Then
        AddressTab(Index).Sort CStr(ColNo), True, True
        AddressTab(Index).AutoSizeColumns
        AddressTab(Index).Refresh
    End If
End Sub
Private Sub SetAddrMarker(Index As Integer, SetLineNo As Long, SetAddrList As String, SetMarker As Boolean)
    Dim LineNo As Long
    Dim tmpTead As String
    Dim tmpHit As String
    Dim CurLineNbr As String
    Dim AddrItm As Long
    Dim LineItm As Long
    AddrItm = 0
    tmpTead = GetRealItem(SetAddrList, AddrItm, ",")
    While tmpTead <> ""
        tmpHit = AddressTab(Index).GetLinesByColumnValue(2, tmpTead, 0)
        LineItm = 0
        CurLineNbr = GetRealItem(tmpHit, LineItm, ",")
        While CurLineNbr <> ""
            LineNo = Val(CurLineNbr)
            If LineNo >= 0 Then
                If SetMarker Then
                    AddressTab(Index).SetDecorationObject LineNo, 2, "SelMarker"
                    AddressTab(Index).SetLineStatusValue LineNo, 1
                    AddressTab(Index).SetLineColor LineNo, vbBlack, vbWhite
                Else
                    AddressTab(Index).ResetLineDecorations LineNo
                    AddressTab(Index).SetLineStatusValue LineNo, 0
                    AddressTab(Index).SetLineColor LineNo, vbBlack, LightGray
                End If
            End If
            LineItm = LineItm + 1
            CurLineNbr = GetRealItem(tmpHit, LineItm, ",")
        Wend
        AddrItm = AddrItm + 1
        tmpTead = GetRealItem(SetAddrList, AddrItm, ",")
    Wend
End Sub

Private Sub AftFldText_Change(Index As Integer)
    Dim tmpTag As String
    Dim tmpName As String
    Dim tmpText As String
    Dim PaxIdx As Integer
    tmpTag = AftFldPanel(Index).Tag
    tmpText = AftFldText(Index).Text
    If InStr(tmpTag, "EETH") > 0 Then txtEditEET(1).Text = tmpText
    If InStr(tmpTag, "EETM") > 0 Then txtEditEET(2).Text = tmpText
    If InStr(tmpTag, "DCD1") > 0 Then txtEditDL(1).Text = tmpText
    If InStr(tmpTag, "DCD2") > 0 Then txtEditDL(2).Text = tmpText
    If InStr(tmpTag, "DTD1") > 0 Then txtEditDL(3).Text = tmpText
    If InStr(tmpTag, "DTD2") > 0 Then txtEditDL(4).Text = tmpText
    If InStr(tmpTag, "PAX") > 0 Then
        tmpTag = GetItem(tmpTag, 2, "|")
        tmpName = GetItem(tmpTag, 4, ",")
        If tmpName <> "" Then
            PaxIdx = Val(Right(tmpName, 1))
            txtEditPX(PaxIdx).Text = tmpText
        End If
        tmpName = GetItem(tmpTag, 1, ",")
        If tmpName <> "" Then
            PaxIdx = GetFltInputFldIdx(0, tmpName)
            If PaxIdx >= 0 Then txtFltInput(PaxIdx).Text = tmpText
        End If
    End If
End Sub

Private Sub AftFldText_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub AftFldText_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim tmpTag As String
    Dim tmpChr As String
    Dim CmpChr As String
    Select Case KeyAscii
        Case 13
            AftFldText_LostFocus Index
        Case Else
        tmpTag = AftFldPanel(Index).Tag
        If InStr(tmpTag, "EETF") > 0 Then
                CmpChr = "0123456789" & Chr(8)
                tmpChr = Chr(KeyAscii)
                If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
        End If
        If InStr(tmpTag, "LONG") > 0 Then
                CmpChr = "0123456789" & Chr(8)
                tmpChr = Chr(KeyAscii)
                If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
        End If
        If InStr(tmpTag, "PAX") > 0 Then
                CmpChr = "0123456789" & Chr(8)
                tmpChr = Chr(KeyAscii)
                If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
        End If
    End Select
End Sub

Private Sub AftFldText_LostFocus(Index As Integer)
    Dim tmpTag As String
    tmpTag = AftFldPanel(Index).Tag
    If InStr(tmpTag, "EETH") > 0 Then txtEditEET_LostFocus 1
    If InStr(tmpTag, "EETM") > 0 Then txtEditEET_LostFocus 2
    If InStr(tmpTag, "DCD1") > 0 Then txtEditDL_LostFocus 1
    If InStr(tmpTag, "DCD2") > 0 Then txtEditDL_LostFocus 2
    If InStr(tmpTag, "DTD1") > 0 Then txtEditDL_LostFocus 3
    If InStr(tmpTag, "DTD2") > 0 Then txtEditDL_LostFocus 4
    If InStr(tmpTag, "PAX1") > 0 Then txtEditPX_LostFocus 1
    If InStr(tmpTag, "PAX2") > 0 Then txtEditPX_LostFocus 2
    If InStr(tmpTag, "PAX3") > 0 Then txtEditPX_LostFocus 3
    If InStr(tmpTag, "PAX4") > 0 Then txtEditPX_LostFocus 4
End Sub

Private Sub AftTestValue_Change(Index As Integer)
    chkTestCase.Value = 0
    If AftTestValue(Index).Text <> "" Then AftTestValue(Index).BackColor = LightYellow Else AftTestValue(Index).BackColor = vbWhite
End Sub

Private Sub AftTestValue_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case KeyAscii
        Case 13
            chkTestCase.Value = 1
        Case Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End Select
End Sub

Private Sub AftTimer_Timer()
    Dim tmpTag As String
    Dim MinVal As Integer
    AftTimer.Enabled = False
    tmpTag = AftTimer.Tag
    MinVal = Val(tmpTag) - 1
    If MinVal < 1 Then
        chkFltData(0).Value = 1
        MinVal = 5
    End If
    If MinVal = 1 Then
        chkFltData(0).BackColor = LightBlue
        chkFltData(0).ForeColor = vbWhite
    End If
    AftTimer.Tag = CStr(MinVal)
    AftTimer.Enabled = True
End Sub

Private Sub AftVpfr_Change(Index As Integer)
    chkFltData(0).BackColor = LightYellow
End Sub

Private Sub AftVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub AftVpto_Change(Index As Integer)
    chkFltData(0).BackColor = LightYellow
End Sub

Private Sub AftVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub AlcTab_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    AlcTab.Tag = ""
    LookForTlxAddr 0, "T", True, True, True
    LookForTlxAddr 1, "E", True, True, True
End Sub

Private Sub AlcTab_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    AlcTab.Tag = CStr(ColNo)
    If ColNo = 6 Then
        AlcTab.OnHScrollTo 0
        chkMvtAddr(1).Value = 1
    End If
End Sub

Private Sub AlcTab_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If AlcTab.Tag <> "" Then
        Select Case Key
            Case 40
                chkMvtAddr(1).Value = 1
            Case Else
        End Select
    End If
End Sub

Private Sub ApcTab_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    ApcTab.Tag = ""
    LookForTlxAddr 0, "T", True, True, True
    LookForTlxAddr 1, "E", True, True, True
End Sub

Private Sub ApcTab_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    On Error Resume Next
    ApcTab.Tag = CStr(ColNo)
    If ColNo = 6 Then
        ApcTab.OnHScrollTo 0
        AddressTab(0).SetFocus
    End If
End Sub

Private Sub ApcTab_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If ApcTab.Tag <> "" Then
        Select Case Key
            Case 38
                chkMvtAddr(0).Value = 1
            Case Else
        End Select
    End If
End Sub

Private Sub LookForTlxAddr(Index As Integer, GetType As String, HitList As Boolean, GetUnique As Boolean, RefreshList As Boolean)
    Dim AlcCol  As Long
    Dim ApcCol As Long
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim AddrList As String
    Dim CurAlc As String
    Dim CurApc As String
    Dim CurLook As String
    Dim ColList As String
    Dim NewLine As String
    Dim CurUrno As String
    Dim LastUrno As String
    Dim tmpCheck As String
    Dim tmpList As String
    CurLine = 0
    ColList = "1,3,5,6,7,8"
    'UfisServer.BasicDataInit 2, 1, "MADTAB", "CDAT,ALC2,ALC3,APC3,APC4,ADDR,TYPE,STAT,URNO", ""
    'AddressTab(Index).LogicalFieldList = "ALC3,APC3,TEAD,BEME,PAYE,URNO"
    If HitList Then
        UfisServer.BasicData(2).SetInternalLineBuffer True
        AddressTab(Index).ResetContent
        If GetUnique Then
            AddressTab(Index).SetUniqueFields "5"
        End If
        For AlcCol = 0 To 4
            CurAlc = Trim(AlcTab.GetColumnValue(0, AlcCol))
            tmpCheck = CurAlc
            If Len(tmpCheck) = 2 Then tmpCheck = TelexPoolHead.CheckAlcLookUp(tmpCheck, False)
            If Len(tmpCheck) > 3 Then tmpCheck = TelexPoolHead.AskForUniqueAlc(tmpCheck, CurAlc)
            CurAlc = tmpCheck
            If CurAlc <> "" Then
                CurAlc = Left(CurAlc & "   ", 3)
                For ApcCol = 0 To 4
                    CurApc = Trim(ApcTab.GetColumnValue(0, ApcCol))
                    If CurApc <> "" Then
                        CurApc = Left(CurApc & "   ", 3)
                        CurLook = CurAlc & CurApc & GetType
                        LineNo = Val(UfisServer.BasicData(2).GetLinesByIndexValue("ALCAPC", CurLook, 0))
                        While LineNo >= 0
                            LineNo = UfisServer.BasicData(2).GetNextResultLine
                            If LineNo >= 0 Then
                                NewLine = UfisServer.BasicData(2).GetColumnValues(LineNo, ColList)
                                AddressTab(Index).InsertTextLine NewLine, False
                                AddressTab(Index).SetDecorationObject CurLine, 2, "SelMarker"
                                AddressTab(Index).SetLineStatusValue CurLine, 1
                                CurLine = CurLine + 1
                            End If
                        Wend
                    End If
                Next
            End If
        Next
        UfisServer.BasicData(2).SetInternalLineBuffer False
    Else
        CurLine = 0
        UfisServer.BasicData(2).SetInternalLineBuffer True
        LineNo = Val(UfisServer.BasicData(2).GetLinesByColumnValue(6, GetType, 0))
        While LineNo >= 0
            LineNo = UfisServer.BasicData(2).GetNextResultLine
            If LineNo >= 0 Then
                NewLine = UfisServer.BasicData(2).GetColumnValues(LineNo, ColList)
                AddressTab(Index).InsertTextLine NewLine, False
                AddressTab(Index).SetDecorationObject CurLine, 2, "SelMarker"
                AddressTab(Index).SetLineStatusValue CurLine, 1
                CurLine = CurLine + 1
            End If
        Wend
        UfisServer.BasicData(2).SetInternalLineBuffer False
    End If
    AddressTab(Index).Sort 2, True, True
    AddressTab(Index).AutoSizeColumns
    AddressTab(Index).OnHScrollTo 2
    AlcTab.Refresh
    AddressTab(Index).Refresh
    tmpList = Trim(AddressTab(Index).SelectDistinct("4", "", "", ",", True))
    If Left(tmpList, 1) = "," Then tmpList = Mid(tmpList, 2)
    InitDblSignList tmpList
    AddressTab(Index).SetCurrentSelection 0
    If RefreshList Then FillAddrField False, 0, "", 1, ""
End Sub
Private Sub FillAddrField(ArrangeList As Boolean, AddrIdx As Integer, UseAddrList As String, MailIdx As Integer, UseMailList As String)
    Dim tmpList As String
    Dim tmpText As String
    Dim tmpTead As String
    Dim CurItm As Integer
    Dim idx As Integer
    TelexAddrIsClean = True
    If (UseAddrList <> "") Or (UseMailList <> "") Then
        txtDestAddr(AddrIdx).Text = UseAddrList
        txtDestAddr(AddrIdx).Text = Replace(txtDestAddr(AddrIdx).Text, ",", " ", 1, -1, vbBinaryCompare)
        txtDestAddr(MailIdx).Text = UseMailList
        txtDestAddr(MailIdx).Text = Replace(txtDestAddr(MailIdx).Text, ",", vbNewLine, 1, -1, vbBinaryCompare)
    Else
        For idx = AddrIdx To MailIdx
            If idx < 2 Then
                If idx = AddrIdx Then tmpList = GetOutAddrList(0)
                If idx = MailIdx Then tmpList = GetOutAddrList(1)
            Else
                'Home Addresses (only in template)
                tmpList = ""
            End If
            If chkTlxAddr(0).Value = 0 Then
                txtDestAddr(idx).Text = tmpList
            Else
                If idx = 0 Then
                    tmpText = txtDestAddr(idx).Text
                    CurItm = 1
                    tmpTead = GetItem(tmpList, CurItm, ",")
                    While tmpTead <> ""
                        If InStr(tmpText, tmpTead) = 0 Then tmpText = tmpText & " " & tmpTead
                        CurItm = CurItm + 1
                        tmpTead = GetItem(tmpList, CurItm, ",")
                    Wend
                    txtDestAddr(idx).Text = tmpText
                End If
            End If
            If idx = AddrIdx Then
                If ArrangeList Then
                    txtDestAddr(idx).Text = GetCleanAddrList(txtDestAddr(idx).Text, 8, " ", 4, "", False)
                Else
                    txtDestAddr(idx).Text = Replace(txtDestAddr(idx).Text, ",", " ", 1, -1, vbBinaryCompare)
                End If
            End If
            If idx = MailIdx Then
                FormatEmailAddressLists
            End If
        Next
    End If
    'If (Trim(txtDestAddr(0).Text) = "") And (Trim(txtDestAddr(2).Text) = "") Then TelexAddrIsClean = False
    TelexAddrIsClean = CheckAddressLists("ALL")
End Sub
Private Function CheckAddressLists(ForWhat As String) As Boolean
    Dim IsClean As Boolean
    IsClean = False
    If Trim(txtDestAddr(0).Text) <> "" Then IsClean = True
    If Trim(txtDestAddr(2).Text) <> "" Then IsClean = True
    If Trim(txtDestAddr(1).Text) <> "" Then IsClean = True
    If Trim(txtDestAddr(3).Text) <> "" Then IsClean = True
    If TelexAddrIsWrong = True Then IsClean = False
    CheckAddressLists = IsClean
End Function
Private Sub FormatEmailAddressLists()
    Dim tmpText As String
    Dim tmpCopy As String
    Dim tmpDoub As String
    Dim idx As Integer
    Dim i As Integer
    tmpDoub = vbLf & vbLf
    idx = 1
    For i = 1 To 2
        tmpText = txtDestAddr(idx).Text
        tmpText = Replace(tmpText, vbCr, vbLf, 1, -1, vbBinaryCompare)
        tmpText = Replace(tmpText, ",", vbLf, 1, -1, vbBinaryCompare)
        tmpText = Replace(tmpText, ";", vbLf, 1, -1, vbBinaryCompare)
        tmpText = Replace(tmpText, " ", vbLf, 1, -1, vbBinaryCompare)
        While Left(tmpText, 1) = vbLf
            tmpText = Mid(tmpText, 2)
        Wend
        While Right(tmpText, 1) = vbLf
            tmpText = Left(tmpText, Len(tmpText) - 1)
        Wend
        tmpCopy = "START"
        While tmpText <> tmpCopy
            tmpCopy = tmpText
            tmpText = Replace(tmpText, tmpDoub, vbLf, 1, -1, vbBinaryCompare)
        Wend
        tmpText = Replace(tmpText, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
        txtDestAddr(idx).Text = tmpText
        idx = 3
    Next
End Sub
Private Sub chkAddData_Click(Index As Integer)
    If chkAddData(Index).Value = 1 Then
        chkAddData(Index).BackColor = MyOwnButtonDown
        If Index = 0 Then chkAddData(1).Value = 0 Else chkAddData(0).Value = 0
        EditFldPanel(Index).Visible = True
        EditDlgPanel(0).Visible = True
    Else
        chkAddData(Index).BackColor = MyOwnButtonFace
        EditFldPanel(Index).Visible = False
        EditDlgPanel(0).Visible = False
    End If
End Sub

Private Sub chkBorder_Click(Index As Integer)
    Dim idx As Integer
    Dim NewSize As Long
    If chkBorder(Index).Value = 1 Then
        Select Case Index
            Case 0
            NewSize = 180
            For idx = 0 To SubSplitVertM.UBound
                SubSplitVertM(idx).Width = 165
                SubSplitVertS(idx).Width = 165
                PicVSplitM(idx).Visible = True
                PicVSplitS(idx).Visible = True
                NewSize = NewSize + SubSplitVertM(idx).Width
                NewSize = NewSize + SubPanelM(idx).Width
                NewSize = NewSize + SubSplitVertS(idx).Width
            Next
            Me.Width = NewSize
            PanelSizeSlider(0).Value = 11
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
            NewSize = 180
            For idx = 0 To SubSplitVertM.UBound
                PicVSplitM(idx).Visible = False
                PicVSplitS(idx).Visible = False
                SubSplitVertM(idx).Width = 45
                SubSplitVertS(idx).Width = 45
                NewSize = NewSize + SubSplitVertM(idx).Width
                NewSize = NewSize + SubPanelM(idx).Width
                NewSize = NewSize + SubSplitVertS(idx).Width
            Next
            Me.Width = NewSize
            PanelSizeSlider(0).Value = 3
            Case Else
        End Select
    End If
End Sub

Private Sub chkClose_Click()
    Dim VisCnt As Integer
    If chkClose.Value = 1 Then
        VisCnt = CountVisibleForms
        If VisCnt > 1 Then
            Me.Hide
            Me.Refresh
            DoEvents
            chkClose.Value = 0
        Else
            chkClose.Value = 0
            HiddenMain.SetShutDownTimer Me
        End If
    End If
End Sub

Public Sub SetTelexDefault(CurTab As TABLib.Tab, ColNo As Long, CurType As String, SetTlxType As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim SetLine As Long
    Dim tmpType As String
    Dim IsFound As Boolean
    CurTab.OnVScrollTo 0
    CurTab.SetCurrentSelection -1
    SetLine = -1
    If CurType <> "" Then
        MaxLine = CurTab.GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpType = CurTab.GetColumnValue(CurLine, ColNo)
            If tmpType = CurType Then
                tmpType = CurTab.GetColumnValue(CurLine, 0)
                If tmpType = "COR" Then
                    TlxTypeTab.SetLineColor CurLine, vbBlack, LightestYellow
                Else
                    TlxTypeTab.SetLineColor CurLine, vbBlack, LightestGreen
                End If
            Else
                TlxTypeTab.SetLineColor CurLine, vbBlack, vbWhite
            End If
        Next
        For CurLine = 0 To MaxLine
            tmpType = CurTab.GetColumnValue(CurLine, ColNo)
            If tmpType = CurType Then
                If SetLine < 0 Then SetLine = CurLine
                If SetTlxType = "" Then
                    CurTab.OnVScrollTo CurLine
                    CurTab.SetCurrentSelection CurLine
                    IsFound = True
                    Exit For
                Else
                    tmpType = CurTab.GetColumnValue(CurLine, 0)
                    If tmpType = SetTlxType Then
                        If SetLine < 0 Then SetLine = CurLine
                        CurTab.OnVScrollTo SetLine
                        CurTab.SetCurrentSelection CurLine
                        IsFound = True
                        SetLine = -1
                        Exit For
                    End If
                End If
            End If
        Next
    End If
    If Not IsFound Then
        If SetLine >= 0 Then
            CurTab.OnVScrollTo SetLine
            CurTab.SetCurrentSelection SetLine
        Else
            If CurType <> "" Then
                CurTab.InsertTextLine CurType & ",,,,,", False
                CurLine = CurTab.GetLineCount - 1
                CurTab.OnVScrollTo CurLine
                CurTab.SetCurrentSelection CurLine
            End If
        End If
    End If
    CurTab.Refresh
End Sub

Private Sub chkCloseSetup_Click(Index As Integer)
    If chkCloseSetup(Index).Value = 1 Then
        chkSetup(0).Value = 0
        chkCloseSetup(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkGetCfg_Click()
    Dim CfgTbl As String
    Dim CfgCmd As String
    Dim CfgKey As String
    Dim CfgFld As String
    Dim CfgDat As String
    Dim tmpData As String
    If chkGetCfg.Value = 1 Then
        CfgTbl = "TLXGEN"
        CfgCmd = "TLX"
        CfgKey = "CONFIG"
        CfgFld = "FLDLST"
        CfgDat = ""
        If UfisServer.HostName <> "LOCAL" Then
            UfisServer.CallCeda CfgDat, CfgCmd, CfgTbl, CfgFld, "", CfgKey, "", 0, True, False
        End If
        GetKeyItem CfgAftRecFldNames, CfgDat, "<AFT_REC_FLD_NAMES>", "</AFT_REC_FLD_NAMES>"
        GetKeyItem CfgTplRecFldNames, CfgDat, "<TPL_REC_FLD_NAMES>", "</TPL_REC_FLD_NAMES>"
        GetKeyItem CfgTplRecFldTypes, CfgDat, "<TPL_REC_FLD_TYPES>", "</TPL_REC_FLD_TYPES>"
        GetKeyItem CfgTplFldDatTypes, CfgDat, "<TPL_FLD_DAT_TYPES>", "</TPL_FLD_DAT_TYPES>"
        GetKeyItem CfgTplFldDatProps, CfgDat, "<TPL_FLD_DAT_PROPS>", "</TPL_FLD_DAT_PROPS>"
        GetKeyItem AutoSendTelexAddress, CfgDat, "<SENDER_TELEX_ADDR>", "</SENDER_TELEX_ADDR>"
        If AutoSendTelexAddress = "" Then
            AutoSendTelexAddress = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "AUTO_SEND_ADDR", "OOOOOPS")
        End If
        GetKeyItem AutoSendEmailAddress, CfgDat, "<SENDER_EMAIL_ADDR>", "</SENDER_EMAIL_ADDR>"
        GetKeyItem tmpData, CfgDat, "<SENDER_EMAIL_SMTP>", "</SENDER_EMAIL_SMTP>"
        If tmpData = "YES" Then SmtpOutIsActive = True Else SmtpOutIsActive = False
        chkGetCfg.Value = 0
    End If
End Sub

Private Sub chkEdit_Click()
    Dim ShowCover As Boolean
    If chkEdit.Value = 1 Then
        chkEdit.BackColor = MyOwnButtonDown
        fraFlightData.Visible = True
        fraFlightData.Refresh
        ShowCover = False
        'If chkView(1).Value = 1 Then ShowCover = True
        'If chkView(2).Value = 1 Then ShowCover = True
        'If (chkView(0).Value = 1) And (chkView(4).Value = 1) Then ShowCover = True
        'If ShowCover Then
        '    fraFlightCover(0).Visible = True
        '    fraFlightPanel(2).Visible = True
        '    fraFlightPanel(2).Refresh
        'End If
        FltInputPanel.Refresh
        FltListPanel(0).Refresh
        FltListPanel(1).Refresh
    Else
        chkEdit.BackColor = MyOwnButtonFace
        fraFlightData.Visible = False
        'fraFlightPanel(2).Visible = False
        'fraFlightCover(0).Visible = False
    End If
    Form_Resize
End Sub

Private Sub chkEdit2_Click()
    If chkEdit2.Value = 1 Then
        chkEdit.Value = 1
        chkEdit2.Value = 0
    Else
    End If
End Sub

Private Sub chkEditDL_Click(Index As Integer)
    On Error Resume Next
    If Not StopNestedCalls Then
        If chkEditDL(Index).Value = 1 Then
            Select Case Index
                Case 0
                    chkEditDL(Index).Value = 0
                Case 1 To 2
                    chkEditDL(Index).Visible = False
                    txtEditDL(Index).SetFocus
                Case 3 To 4
                    chkEditDL(Index).Visible = False
                    txtEditDL(Index).SetFocus
                Case 5
                    If Not fraSetup(4).Visible Then
                        Set fraSetup(4).Container = SubPanelM(2)
                        fraSetup(4).Left = SubPanelM(2).ScaleWidth - fraSetup(4).Width + 60
                        fraSetup(4).Top = fraTlxType.Top
                        MyTestCase(0).Left = 240
                        MyTestCase(0).Width = 690
                        fraSetup(4).Visible = True
                        fraSetup(4).ZOrder
                    Else
                        fraSetup(4).Visible = False
                    End If
                    chkEditDL(Index).Value = 0
                Case Else
                    chkEditDL(Index).Value = 0
            End Select
        End If
    End If
End Sub

Private Sub chkEditEET_Click(Index As Integer)
    On Error Resume Next
    If Not StopNestedCalls Then
        If chkEditEET(Index).Value = 1 Then
            Select Case Index
                Case 0
                    chkEditEET(Index).Value = 0
                Case 1 To 2
                    chkEditEET(Index).Visible = False
                    If Not SystemInput Then txtEditEET(Index).SetFocus
                Case 3 To 4
                    chkEditEET(Index).Value = 0
                Case Else
                    chkEditEET(Index).Value = 0
            End Select
        Else
            chkEditEET(Index).Visible = True
        End If
    End If
End Sub

Private Sub chkEditPX_Click(Index As Integer)
    On Error Resume Next
    If Not StopNestedCalls Then
        If chkEditPX(Index).Value = 1 Then
            Select Case Index
                Case 0
                    chkEditPX(Index).Value = 0
                Case 1 To 4
                    If Index <> 3 Then
                        chkEditPX(Index).Visible = False
                        txtEditPX(Index).SetFocus
                    Else
                        chkEditPX(Index).Value = 0
                    End If
                Case Else
                    chkEditPX(Index).Value = 0
            End Select
        End If
    End If
End Sub

Private Sub chkExit_Click()
    If chkExit.Value = 1 Then
        chkExit.BackColor = MyOwnButtonDown
        If Me.WindowState = vbMaximized Then
            Me.WindowState = vbNormal
            Me.Refresh
            DoEvents
        End If
        HiddenMain.SetShutDownTimer Me
        'chkExit.Value = 0
    Else
        chkExit.BackColor = MyOwnButtonFace
    End If
End Sub

Private Sub chkFldAddr_Click(Index As Integer)
    If chkFldAddr(Index).Value = 1 Then
        chkFldAddr(Index).BackColor = vbWhite
        chkFldAddr(Index).ForeColor = vbBlack
        chkFldAddr(Index).Height = 345
    Else
        chkFldAddr(Index).BackColor = MyOwnButtonFace
        chkFldAddr(Index).ForeColor = vbBlack
        chkFldAddr(Index).Height = 285
    End If
    chkSelAddr(Index).Value = chkFldAddr(Index).Value
    CheckAddrBtnColor
End Sub
Private Sub CheckAddrBtnColor()
    Dim idx As Integer
    For idx = 0 To 1
        Select Case idx
            Case 0
                If chkFldAddr(idx).Value = 0 Then
                    'Telex Addresses
                    If txtDestAddr(0).Text <> "" Then chkFldAddr(idx).ForeColor = vbBlue
                    If txtDestAddr(2).Text <> "" Then chkFldAddr(idx).ForeColor = vbBlue
                End If
            Case 1
                If chkFldAddr(idx).Value = 0 Then
                    'EMail Addresses
                    If txtDestAddr(1).Text <> "" Then chkFldAddr(idx).ForeColor = vbBlue
                    If txtDestAddr(3).Text <> "" Then chkFldAddr(idx).ForeColor = vbBlue
                End If
            Case Else
        End Select
    Next
End Sub
Private Sub chkFltCheck_Click(Index As Integer)
    Dim TlxText As String
    If chkFltCheck(Index).Value = 1 Then
        If txtFltInput(Index).Text <> chkFltCheck(Index).Tag Then
            chkFltCheck(Index).BackColor = vbYellow
            txtFltInput(Index).BackColor = LightestGreen
        Else
            chkFltCheck(Index).Value = 0
        End If
    Else
        chkFltCheck(Index).BackColor = MyOwnButtonFace
        If Not SystemInput Then
            SystemInput = True
            txtFltInput(Index).Text = chkFltCheck(Index).Tag
            SystemInput = False
            TlxText = GetTlxTextFromTemplate(CurTlxTplText, TelexTextIsClean)
            CheckSendStatus
            If Not TplTextIsUser Then TlxText = CurNewTplText
            SetTextBoxSelected txtTelexText(0)
            txtTelexText(0).SelText = TlxText
            'txtTelexText(0).Text = TlxText
            RefreshControls 1
            If chkFltTick(Index).Value = 1 Then
                txtFltInput(Index).BackColor = vbWhite
            Else
                txtFltInput(Index).BackColor = LightGrey
            End If
        End If
    End If
End Sub

Private Sub chkFltData_Click(Index As Integer)
    Dim AftFldList As String
    Dim AftFldData As String
    Dim TlxTplText As String
    Dim tmpFile As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    If chkFltData(Index).Value = 1 Then
        chkFltData(Index).BackColor = MyOwnButtonDown
        chkFltData(Index).ForeColor = vbBlack
        chkFltData(Index).Refresh
        Select Case Index
            Case 0
                If CedaIsConnected Then LoadFlightData
                chkFltData(Index).Value = 0
            Case 1  'Save
                SaveTlxAftData
                chkFltData(Index).Value = 0
            Case 2  '
                'txtTelexText(0).Text = CurTlxTplText
                'chkFltData(Index).Value = 0
            Case 3
                chkEdit.Value = 0
                chkFltData(Index).Value = 0
            Case 4
                chkReset.Value = 1
                chkFltData(Index).Value = 0
            Case Else
                chkFltData(Index).Value = 0
        End Select
    Else
        chkFltData(Index).BackColor = MyOwnButtonFace
        Select Case Index
            Case 0
            Case 1
            Case 2
                'txtTelexText(0).Text = GetTlxTextFromTemplate(CurTlxTplText)
            Case Else
        End Select
    End If
End Sub
Private Sub SaveTlxAftData()
    Dim tmpSaveTag As String
    Dim AftUrno As String
    Dim TlxUrno As String
    Dim AftName As String
    Dim TplName As String
    Dim FldData As String
    Dim SqlTbl As String
    Dim SqlCmd As String
    Dim SqlFld As String
    Dim SqlDat As String
    Dim SqlKey As String
    Dim RetCode As Integer
    Dim retval As String
    tmpSaveTag = chkFltData(1).Tag
    AftUrno = CurAftFltUrno
    SqlFld = ""
    SqlDat = ""
    If InStr(tmpSaveTag, "[EETM]") > 0 Then
        AftName = "BAAA"
        TplName = "EETM"
        SqlFld = SqlFld & "," & AftName
        FldData = txtEditEET(2).Text
        SqlDat = SqlDat & "," & FldData
    End If
    If InStr(tmpSaveTag, "[DLAY]") > 0 Then
        AftName = "DCD1"
        FldData = txtEditDL(1).Text
        SqlFld = SqlFld & "," & AftName
        SqlDat = SqlDat & "," & FldData
        AftName = "DCD2"
        FldData = txtEditDL(2).Text
        SqlFld = SqlFld & "," & AftName
        SqlDat = SqlDat & "," & FldData
        AftName = "DTD1"
        FldData = txtEditDL(3).Text
        SqlFld = SqlFld & "," & AftName
        SqlDat = SqlDat & "," & FldData
        AftName = "DTD2"
        FldData = txtEditDL(4).Text
        SqlFld = SqlFld & "," & AftName
        SqlDat = SqlDat & "," & FldData
    End If
    If SqlFld <> "" Then
        SqlTbl = "AFTTAB"
        SqlCmd = "UFR"
        SqlFld = Mid(SqlFld, 2)
        SqlDat = Mid(SqlDat, 2)
        SqlKey = "WHERE URNO=" & AftUrno
        If UfisServer.HostName <> "LOCAL" Then
            RetCode = UfisServer.CallCeda(retval, SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "", 0, True, False)
        End If
    End If
    chkFltData(1).Tag = ""
End Sub

Private Sub chkFltTick_Click(Index As Integer)
    Dim tmpText As String
    Dim NewSize As Long
    Dim NewLeft As Long
    If chkFltTick(Index).Value = 1 Then
        If txtFltInput(Index).Text = chkFltCheck(Index).Tag Then
            txtFltInput(Index).BackColor = vbWhite
        End If
        tmpText = "_" & txtFltInput(Index).Text & "_"
        NewSize = TextWidth(tmpText)
        NewLeft = FltEditPanel(Index).Width - NewSize - 45
        If NewSize > FltEditSize Then
            chkFltCheck(Index).Visible = False
            txtFltInput(Index).Left = NewLeft
            txtFltInput(Index).Width = NewSize
        End If
        txtFltInput(Index).Locked = False
    Else
        'If txtFltInput(Index).Text = chkFltCheck(Index).Tag Then
            txtFltInput(Index).BackColor = LightGrey
        'End If
        NewSize = FltEditSize
        NewLeft = FltEditPanel(Index).Width - NewSize - 45
        chkFltCheck(Index).Visible = True
        txtFltInput(Index).Left = NewLeft
        txtFltInput(Index).Width = NewSize
        txtFltInput(Index).Locked = True
    End If
End Sub

Private Sub chkLcase_Click()
    If chkLcase.Value = 1 Then chkLcase.BackColor = MyOwnButtonDown Else chkLcase.BackColor = MyOwnButtonFace
End Sub

Private Sub chkMvtAddr_Click(Index As Integer)
    Dim tmpLineNo As Integer
    Dim retval As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    
    If chkMvtAddr(Index).Value = 1 Then
        chkMvtAddr(Index).BackColor = MyOwnButtonDown
        Select Case Index
            Case 0  'Airline
                SetFirstFreeCell AlcTab
                chkMvtAddr(Index).Value = 0
            Case 1  'Airport
                SetFirstFreeCell ApcTab
                chkMvtAddr(Index).Value = 0
            Case 2  'Add
                chkTlxAddr(2).Value = 1
                chkMvtAddr(Index).Value = 0
            Case 3  'Load
                LookForTlxAddr 0, "T", True, True, True
                LookForTlxAddr 1, "E", True, True, True
                chkMvtAddr(Index).Value = 0
            Case 4  'Get All
                chkMvtAddr(5).Value = 1
                LookForTlxAddr 0, "T", False, False, False
                LookForTlxAddr 1, "E", False, False, False
                chkMvtAddr(Index).Value = 0
            Case 5  'Clear
                AddressTab(0).ResetContent
                AddressTab(0).Refresh
                AddressTab(1).ResetContent
                AddressTab(1).Refresh
                AlcTab.ResetContent
                AlcTab.InsertTextLine ",,,,,,", False
                AlcTab.Refresh
                ApcTab.ResetContent
                ApcTab.InsertTextLine ",,,,,,", False
                ApcTab.Refresh
                InitDblSignList ""
                StatusBar1.Panels(2).Text = ""
                chkMvtAddr(Index).Value = 0
            Case Else
        End Select
    Else
        chkMvtAddr(Index).BackColor = MyOwnButtonFace
    End If
End Sub
Private Sub SetFirstFreeCell(CurTab As TABLib.Tab)
    Dim CurCol As Long
    On Error Resume Next
    'For CurCol = 0 To 5
        
    'Next
    CurTab.SetFocus
    CurCol = 0
    CurTab.SetInplaceEdit 0, CurCol, False
    CurTab.Tag = "0"
End Sub

Private Sub chkOnline_Click(Index As Integer)
    If chkOnline(Index).Value = 1 Then
        Select Case Index
            Case 0
                chkOnline(Index).BackColor = MyOwnButtonDown
                fraOnline.Left = fraTopButtons(0).Left + chkOnline(Index).Left
                fraOnline.Top = fraTopButtons(0).Top + fraTopButtons(0).Height
                fraOnline.ZOrder
                StopNestedCalls = True
                If FormIsVisible("RcvTelex") Then chkOnline(1).Value = 1 Else chkOnline(1).Value = 0
                If FormIsVisible("SndTelex") Then chkOnline(2).Value = 1 Else chkOnline(2).Value = 0
                If FormIsVisible("PndTelex") Then chkOnline(3).Value = 1 Else chkOnline(3).Value = 0
                If FormIsVisible("OutTelex") Then chkOnline(4).Value = 1 Else chkOnline(4).Value = 0
                StopNestedCalls = False
                fraOnline.Visible = True
            Case 1
                If Not StopNestedCalls Then RcvTelex.Show
            Case 2
                If Not StopNestedCalls Then SndTelex.Show
            Case 3
                If Not StopNestedCalls Then
                    PndTelex.Show
                    If PoolConfig.OnLineCfg(5).Value = 0 Then
                        If FixWin.Value = 0 Then
                            FixWin.Value = 1
                            FixWin.Value = 0
                        Else
                            FixWin.Value = 0
                            FixWin.Value = 1
                        End If
                    End If
                End If
            Case 4
                If Not StopNestedCalls Then OutTelex.Show
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                chkOnline(Index).BackColor = MyOwnButtonFace
                fraOnline.Visible = False
            Case 1
                If Not StopNestedCalls Then RcvTelex.Hide
            Case 2
                If Not StopNestedCalls Then SndTelex.Hide
            Case 3
                If Not StopNestedCalls Then
                    PndTelex.Hide
                    If PoolConfig.OnLineCfg(5).Value = 0 Then
                        If FixWin.Value = 0 Then
                            FixWin.Value = 1
                            FixWin.Value = 0
                        Else
                            FixWin.Value = 0
                            FixWin.Value = 1
                        End If
                    End If
                End If
            Case 4
                If Not StopNestedCalls Then OutTelex.Hide
            Case Else
        End Select
    End If
    If FixWin.Value = 1 Then
        SystemResizing = True
        InitPosSize True
        SystemResizing = False
    End If
End Sub

Private Sub chkOutCnt_Click(Index As Integer)
    Dim tmpTag As String
    Dim idx As Integer
    If chkOutCnt(Index).Value = 1 Then
        If Not SystemInput Then
            chkOutCnt(Index).BackColor = MyOwnButtonDown
            tmpTag = chkOutCnt(0).Tag
            If tmpTag <> "" Then
                idx = Val(tmpTag)
                If idx <> Index Then chkOutCnt(idx).Value = 0
            End If
            lblChrCnt.Caption = "Text Length = " & CStr(Len(txtPreview(Index).Text))
            txtPreview(Index).Visible = True
            chkOutCnt(0).Tag = CStr(Index)
            OutTlxIdx = Index
            Form_Resize
        End If
    Else
        chkOutCnt(Index).BackColor = MyOwnButtonFace
        If Not SystemInput Then
            chkOutCnt(0).Tag = ""
            txtPreview(Index).Visible = False
            lblChrCnt.Caption = ""
        End If
    End If
End Sub

Private Sub chkOutFormat_Click()
    If chkOutFormat.Value = 1 Then
        chkOutFormat.BackColor = MyOwnButtonDown
        chkPreView.Value = 0
    Else
        chkOutFormat.BackColor = MyOwnButtonFace
    End If
    chkPreView.Value = chkOutFormat.Value
End Sub

Private Sub chkPreView_Click()
    If chkPreView.Value = 1 Then
        chkPreView.BackColor = MyOwnButtonDown
        chkText.Enabled = False
        CheckSendStatus
        InitTelexPreview False
        fraPreview.Visible = True
        fraTelexText(0).Visible = False
        txtTplName.Visible = False
        txtTplDesc.Visible = False
    Else
        chkPreView.BackColor = MyOwnButtonFace
        fraTelexText(0).Visible = True
        fraPreview.Visible = False
        CheckSendStatus
        chkText.Enabled = True
        If chkTlxTxt.Value = 1 Then
            chkTlxTxt.BackColor = LightYellow
            'chkTlxTxt.Picture = imgAmpel(0).Picture
            txtTelexText(0).Locked = False
            txtTelexText(0).BackColor = LightestYellow
            PanelIcon(6).Tag = "OPEN"
            txtTelexText(0).SetFocus
        End If
    End If
End Sub

Private Sub InitTelexPreview(WithFooter As Boolean)
    Select Case MySitaOutType
        Case "SITA_NODE"
            InitTpfNodePreview WithFooter
        Case "SITA_FILE"
            If chkOutFormat.Value = 1 Then
                InitSitaFilePreview WithFooter
            Else
                InitTpfNodePreview WithFooter
            End If
        Case Else
    End Select
End Sub

Private Sub InitTpfNodePreview(WithFooter As Boolean)
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim CleanText As String
    Dim NewText As String
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpUrno As String
    Dim tmpTtyp As String
    Dim tmpAllAddr As String
    Dim tmpTlxAddr As String
    Dim tmpAllMail As String
    Dim tmpTlxMail As String
    Dim TxtAllAddr As String
    Dim TxtCccAddr As String
    Dim tmpDateStamp As String
    Dim TextIsOk As Boolean
    Dim NewLeft As Long
    Dim iCount As Integer
    Dim idx As Integer
    SystemInput = True
    FormatEmailAddressLists
    CleanText = GetCleanTelexText(txtTelexText(0).Text, TextIsOk, True)
    TelexTextIsClean = TextIsOk
    CheckSendStatus
    If Not TextIsOk Then
        chkSend.BackColor = vbRed
        chkSend.ForeColor = vbWhite
    Else
        'chkSend.BackColor = MyOwnButtonFace
        'chkSend.ForeColor = vbBlack
    End If
    For idx = 0 To txtPreview.UBound
        txtPreview(idx).Text = ""
        txtPreview(idx).Visible = False
        chkOutCnt(idx).Visible = False
        chkOutCnt(idx).Tag = ""
        chkOutCnt(idx).Value = 0
    Next
    idx = 0
    NewLeft = chkOutCnt(idx).Left
    txtPreview(idx).Visible = True
    NewText = ""
    LineNo = TlxTypeTab.GetCurrentSelected
    tmpTtyp = TlxTypeTab.GetColumnValue(LineNo, 0)
    If tmpTtyp <> "SCOR" Then
        tmpAllAddr = ""
        tmpAllAddr = tmpAllAddr & txtDestAddr(0).Text & " "
        tmpAllAddr = tmpAllAddr & txtDestAddr(2).Text & " "
        tmpAllAddr = Trim(tmpAllAddr)
        'If tmpAllAddr = "" Then tmpAllAddr = "-------"
        If tmpAllAddr = "" Then tmpAllAddr = ActiveSendAddress
        TxtAllAddr = GetCleanAddrList(tmpAllAddr, 0, " ", 100, "", False)
        
        tmpAllMail = ""
        If Trim(txtDestAddr(1).Text) <> "" Then
            tmpAllMail = tmpAllMail & Trim(txtDestAddr(1).Text)
            If Trim(txtDestAddr(3).Text) <> "" Then tmpAllMail = tmpAllMail & vbNewLine
        End If
        If Trim(txtDestAddr(3).Text) <> "" Then tmpAllMail = tmpAllMail & Trim(txtDestAddr(3).Text)
        tmpAllMail = Trim(tmpAllMail)
        If tmpAllMail = "" Then tmpAllMail = "-------"
        
        
        If InStr(tmpAllAddr, "-------") > 0 Then TelexAddrIsClean = False
        If InStr("-------,OOOOOPS,ZZZZZZZ", ActiveSendAddress) > 0 Then TelexOrigIsClean = False
        
        CheckSendStatus
            
        While tmpAllAddr <> ""
            txtPreview(idx).Text = ""
            NewText = ""
            
            LineNo = TlxPrioTab.GetCurrentSelected
            tmpData = TlxPrioTab.GetColumnValue(LineNo, 0)
            NewText = NewText & tmpData & " "
            
            tmpTlxAddr = GetCleanAddrList(tmpAllAddr, 8, " ", 4, ActiveSendAddress, True)
            tmpTlxAddr = Trim(tmpTlxAddr)
            NewText = NewText & tmpTlxAddr & vbNewLine
            
            LineNo = TlxOrigTab.GetCurrentSelected
            tmpData = TlxOrigTab.GetColumnValue(LineNo, 0)
            tmpLine = "." & tmpData & " "
            LineNo = DblSignTab.GetCurrentSelected
            tmpData = Trim(DblSignTab.GetColumnValue(LineNo, 0))
            tmpData = MySetUp.GetUtcServerTime
            tmpLine = tmpLine & Mid(tmpData, 7, 6) & " "
            tmpDateStamp = Mid(tmpData, 7, 6)
            tmpUrno = Trim(NewSendUrno(idx).Text)
            If tmpUrno = "" Then
                UfisServer.UrnoPoolPrepare 1
                tmpUrno = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
                If tmpUrno = "" Then tmpUrno = UfisServer.UrnoPoolGetNext
            End If
            NewSendUrno(idx).Text = tmpUrno
            tmpData = DecodeSsimDayFormat(Left(tmpData, 8), "CEDA", "SSIM2")
            tmpDateStamp = tmpDateStamp & " " & Mid(tmpData, 3, 5)
            chkPreView.Tag = tmpDateStamp & " PPK " & Right(tmpUrno, 3)
            NewText = NewText & tmpLine & vbNewLine
            NewText = NewText & CleanText & vbNewLine
            tmpData = Trim(txtSiText(0).Text)
            If tmpData <> "" Then
                If Left(tmpData, 3) <> "SI " Then
                    tmpData = "SI " & tmpData
                End If
                NewText = NewText & tmpData & vbNewLine
            End If
            tmpData = Trim(txtSiText(1).Text)
            If tmpData <> "" Then
                If Left(tmpData, 3) <> "SI " Then
                    tmpData = "SI " & tmpData
                End If
                NewText = NewText & tmpData & vbNewLine
            End If
            If TxtAllAddr <> "-------" Then
                'NewText = NewText & vbNewLine
                tmpTlxAddr = Left(tmpTlxAddr, Len(tmpTlxAddr) - 7)
                tmpTlxAddr = GetCleanAddrList(tmpTlxAddr, 0, " ", 4, "", False)
                TxtCccAddr = Replace(TxtAllAddr, tmpTlxAddr, "", 1, -1, vbBinaryCompare)
                TxtCccAddr = GetCleanAddrList(TxtCccAddr, 8, " ", 100, "", True)
                If TxtCccAddr <> "" Then
                    If TxtCccAddr <> ActiveSendAddress Then
                        NewText = NewText & "SI CC" & vbNewLine
                        NewText = NewText & TxtCccAddr & vbNewLine
                    End If
                End If
            End If
            If idx = 0 Then
                'MsgBox "Reminder"
                'SmtpOutIsActive = True
                If SmtpOutIsActive Then
                    If (tmpAllMail <> "-------") And (tmpAllMail <> "") Then
                        'NewText = NewText & vbNewLine
                        'NewText = NewText & "SI SENT BY EMAIL TOO" & vbNewLine
                        NewText = NewText & "<EMAIL_ADDR>" & vbNewLine
                        NewText = NewText & tmpAllMail & vbNewLine
                        NewText = NewText & "</EMAIL_ADDR>" & vbNewLine
                        tmpAllMail = "-------"
                    End If
                End If
            End If
            txtPreview(idx).Text = NewText
            If tmpAllAddr <> "" Then
                txtPreview(idx).Visible = False
                chkOutCnt(idx).Left = NewLeft
                chkOutCnt(idx).Caption = Right("00" & CStr(idx + 1), 2)
                chkOutCnt(idx).Visible = True
                NewLeft = NewLeft + chkOutCnt(idx).Width
                idx = idx + 1
                If idx > txtPreview.UBound Then
                    Load txtPreview(idx)
                    Load chkOutCnt(idx)
                    Load NewSendUrno(idx)
                    Load NewCallCode(idx)
                    Set txtPreview(idx).Container = fraPreview
                    Set chkOutCnt(idx).Container = fraPreview
                End If
            End If
        Wend
        If idx > 0 Then
            txtPreview(idx).Visible = False
            chkOutCnt(idx).Left = NewLeft
            chkOutCnt(idx).Caption = Right("00" & CStr(idx + 1), 2)
            chkOutCnt(idx).Visible = True
            NewLeft = NewLeft + chkOutCnt(idx).Width + 90
        End If
        If idx = 0 Then NewLeft = chkOutCnt(0).Left + 30
        lblChrCnt.Left = NewLeft
        SystemInput = False
        chkOutCnt(0).Value = 1
    End If
    SystemInput = False
End Sub
Private Sub CheckSendStatus()
    Dim tmpText As String
    Dim TextIsOk As Boolean
    TelexAddrIsClean = CheckAddressLists("ALL")
    If chkTlxTxt.Value = 0 Then
        chkSend.Enabled = True
        If Trim(txtTelexText(0).Text) = "" Then TelexTextIsClean = False
        If Trim(txtDestAddr(0).Text) = "" Then TelexAddrIsClean = False
        If TplRuleLineNo >= 0 Then
            If (TelexTextIsClean) And (TelexAddrIsClean) And (TelexOrigIsClean) Then
                'PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("edittlx")).Picture
                PanelIcon(6).Picture = imgAmpel(3).Picture
                chkSend.Caption = "Send"
                chkSend.BackColor = MyOwnButtonFace
                chkSend.ForeColor = vbBlack
                LockButtons(1).Enabled = True
                PanelIcon(6).Tag = "EDIT"
            ElseIf Trim(txtTelexText(0).Text) = "" Then
                'PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("edittlx")).Picture
                PanelIcon(6).Picture = imgAmpel(3).Picture
                chkSend.Caption = "Send"
                chkSend.BackColor = MyOwnButtonFace
                chkSend.ForeColor = vbBlack
                LockButtons(1).Enabled = True
                PanelIcon(6).Tag = "EDIT"
            Else
                If chkPreView.Value = 0 Then
                    If Not TelexOrigIsClean Then chkSend.Caption = ">> Orig"
                    If Not TelexAddrIsClean Then chkSend.Caption = ">> Addr"
                    If Not TelexTextIsClean Then chkSend.Caption = ">> Text"
                    chkSend.BackColor = vbRed
                    chkSend.ForeColor = vbWhite
                End If
                PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("blackarrow")).Picture
                PanelIcon(6).Tag = "SHOW"
                txtTelexText(0).BackColor = vbWhite
                txtTelexText(0).Locked = True
                LockButtons(1).Enabled = False
            End If
        ElseIf Trim(txtTelexText(0).Text) = "" Then
            'PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("edittlx")).Picture
            PanelIcon(6).Picture = imgAmpel(3).Picture
            chkSend.Caption = "Send"
            chkSend.BackColor = MyOwnButtonFace
            chkSend.ForeColor = vbBlack
            LockButtons(1).Enabled = True
            PanelIcon(6).Tag = "EDIT"
        Else
            chkSend.Enabled = False
            chkSend.Caption = "Send"
            chkSend.BackColor = MyOwnButtonFace
            chkSend.ForeColor = vbBlack
            LockButtons(1).Enabled = False
            'PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("blackarrow")).Picture
            PanelIcon(6).Picture = imgAmpel(3).Picture
            txtTelexText(0).BackColor = vbWhite
            txtTelexText(0).Locked = True
            PanelIcon(6).Tag = "SHOW"
        End If
    End If
    TelexIsCleanToSend = True
    If Not TelexOrigIsClean Then
        chkSend.Caption = ">> Orig"
        chkSend.BackColor = vbRed
        chkSend.ForeColor = vbWhite
        LockButtons(1).Enabled = False
        chkSend.Enabled = True
        TelexIsCleanToSend = False
    End If
    If Not TelexAddrIsClean Then
        PanelIcon(4).Picture = imgAmpel(2).Picture
        chkSend.Caption = ">> Addr"
        chkSend.BackColor = vbRed
        chkSend.ForeColor = vbWhite
        LockButtons(1).Enabled = False
        chkSend.Enabled = True
        TelexIsCleanToSend = False
    Else
        PanelIcon(4).Picture = imgAmpel(0).Picture
    End If
    If Not TelexTextIsClean Then
        tmpText = Trim(GetCleanTelexText(txtTelexText(0).Text, TextIsOk, False))
        chkTlxTxt.Picture = PanelIcon(6).Picture
        If tmpText <> "" Then
            chkSend.Caption = ">> Text"
            chkSend.BackColor = vbRed
            chkSend.ForeColor = vbWhite
            LockButtons(1).Enabled = False
            chkSend.Enabled = True
        Else
            chkSend.Caption = "Send"
            chkSend.BackColor = vbButtonFace
            chkSend.ForeColor = vbBlack
            LockButtons(1).Enabled = False
            chkSend.Enabled = False
        End If
        TelexIsCleanToSend = False
    Else
        'PanelIcon(6).Picture = imgAmpel(3).Picture
        PanelIcon(6).Picture = imgAmpel(0).Picture
    End If
    If TelexIsCleanToSend Then
        LockButtons(1).Enabled = True
        chkSend.Enabled = True
        chkSend.Caption = "Send"
        chkSend.BackColor = vbButtonFace
        chkSend.ForeColor = vbBlack
    Else
        LockButtons(1).Enabled = False
        'chkSend.Enabled = True
        'chkSend.Caption = "Send"
        'chkSend.BackColor = vbButtonFace
        'chkSend.ForeColor = vbBlack
    End If
End Sub
Private Function GetCleanTelexText(RawText As String, TextIsOk As Boolean, ForSend As Boolean) As String
    Dim NewText As String
    NewText = RawText
    If ForSend = True Then
        If (FlightIsOnTime) And (TemplateHasDelayLine) Then NewText = RemoveDelayLine(NewText, "TEXT")
        NewText = RemovePaxLine(NewText, "TEXT")
        NewText = Replace(NewText, "/^^/", "/", 1, -1, vbBinaryCompare)
        NewText = Replace(NewText, "/^^^^", "", 1, -1, vbBinaryCompare)
        NewText = Replace(NewText, "INCLDG 0 INF", "", 1, -1, vbBinaryCompare)
        NewText = Replace(NewText, "INCLDG ^^^ INF", "", 1, -1, vbBinaryCompare)
        TextIsOk = True
        If InStr(NewText, "??") > 0 Then TextIsOk = False
        If InStr(NewText, "^^") > 0 Then TextIsOk = False
    Else
        If InStr(NewText, "??") > 0 Then
            TextIsOk = False
            PanelIcon(6).Picture = imgAmpel(2).Picture
        ElseIf InStr(NewText, "^^") > 0 Then
            TextIsOk = False
            PanelIcon(6).Picture = imgAmpel(1).Picture
        ElseIf NewText = "" Then
            TextIsOk = False
            PanelIcon(6).Picture = imgAmpel(2).Picture
        End If
    End If
    GetCleanTelexText = NewText
End Function
Private Function RemoveDelayLine(RawText As String, TextType As String) As String
    Dim NewText As String
    Dim TxtPos As Integer
    Dim txtLen As Integer
    NewText = RawText
    Select Case TextType
        Case "TEXT"
            TxtPos = InStr(NewText, "DL^^")
            If TxtPos > 0 Then
                txtLen = Len(NewText)
                While ((Mid(NewText, TxtPos, 1) <> vbCr) And (TxtPos <= txtLen))
                    Mid(NewText, TxtPos, 1) = " "
                    TxtPos = TxtPos + 1
                Wend
                NewText = RemoveDoubleSpaces(NewText)
            End If
        Case Else
    End Select
    RemoveDelayLine = NewText
End Function
Private Function RemovePaxLine(RawText As String, TextType As String) As String
    Dim NewText As String
    Dim TxtPos As Integer
    Dim txtLen As Integer
    Dim InfRemoved As Boolean
    NewText = RawText
    Select Case TextType
        Case "TEXT"
            TxtPos = InStr(NewText, "PX^^^")
            If TxtPos > 0 Then
                txtLen = Len(NewText)
                While ((Mid(NewText, TxtPos, 1) <> vbCr) And (TxtPos <= txtLen))
                    Mid(NewText, TxtPos, 1) = " "
                    TxtPos = TxtPos + 1
                Wend
                NewText = RemoveDoubleSpaces(NewText)
            End If
            InfRemoved = False
            TxtPos = InStr(NewText, "^^^ INF")
            If TxtPos > 0 Then
                NewText = Replace(NewText, "^^^ INF", "", 1, -1, vbBinaryCompare)
                InfRemoved = True
            End If
            TxtPos = InStr(NewText, " 0 INF")
            If TxtPos > 0 Then
                NewText = Replace(NewText, " 0 INF", "", 1, -1, vbBinaryCompare)
                InfRemoved = True
            End If
            If InfRemoved Then
                NewText = Replace(NewText, "INCLDG", "", 1, -1, vbBinaryCompare)
                NewText = Replace(NewText, "EXCLDG", "", 1, -1, vbBinaryCompare)
            End If
        Case Else
    End Select
    RemovePaxLine = NewText
End Function
Private Function RemoveDoubleSpaces(RawText As String) As String
    Dim NewText As String
    Dim OldText As String
    OldText = RawText
    NewText = Replace(RawText, "  ", " ", 1, -1, vbBinaryCompare)
    While NewText <> OldText
        OldText = NewText
        NewText = Replace(NewText, "  ", " ", 1, -1, vbBinaryCompare)
    Wend
    NewText = Replace(NewText, (vbNewLine & " " & vbNewLine), vbNewLine, 1, -1, vbBinaryCompare)
    RemoveDoubleSpaces = Trim(NewText)
End Function
Private Function TrimSpacesInArea(RawText As String, AreaChrBgn As String, AreaChrEnd)
    Dim NewText As String
    Dim RawLen As Integer
    Dim NewLen As Integer
    Dim BgnPos As Integer
    Dim EndPos As Integer
    Dim ChrPos As Integer
    Dim CurChr As String
    Dim CurAsc As Integer
    Dim iCnt As Integer
    Dim PosIsInArea As Boolean
    Dim TestOnly As Boolean
    TestOnly = True
    NewText = ""
    RawLen = Len(RawText)
    PosIsInArea = False
    BgnPos = 1
    For ChrPos = 1 To RawLen
        CurChr = Mid(RawText, ChrPos, 1)
        CurAsc = Asc(CurChr)
        If CurAsc < 32 Then
            'BreakPoint For Testing
            CurAsc = Asc(CurChr)
        End If
        If Not TestOnly Then
            If PosIsInArea Then
                Select Case CurChr
                    Case AreaChrEnd
                        PosIsInArea = False
                        If iCnt > 0 Then
                            BgnPos = ChrPos
                            EndPos = ChrPos
                        Else
                            EndPos = ChrPos
                        End If
                        NewLen = EndPos - BgnPos + 1
                        If NewLen > 0 Then NewText = NewText & Mid(RawText, BgnPos, NewLen)
                        BgnPos = ChrPos + 1
                        EndPos = 0
                    Case " "
                        iCnt = iCnt + 1
                    Case Else
                        iCnt = 0
                End Select
            Else
                EndPos = ChrPos
                If Mid(RawText, ChrPos, 1) = AreaChrBgn Then
                    PosIsInArea = True
                    NewLen = EndPos - BgnPos + 1
                    If NewLen > 0 Then NewText = NewText & Mid(RawText, BgnPos, NewLen)
                    BgnPos = ChrPos + 1
                    EndPos = 0
                End If
            End If
        End If
    Next
    If PosIsInArea Then EndPos = RawLen
    NewLen = EndPos - BgnPos + 1
    If NewLen > 0 Then NewText = NewText & Mid(RawText, BgnPos, NewLen)
    If NewText = "" Then NewText = RawText
    If TestOnly Then NewText = RawText
    TrimSpacesInArea = NewText
End Function
Private Sub InitSitaFilePreview(WithFooter As Boolean)
    Dim LineNo As Long
    Dim CurLine As Long
    Dim TabAddrList As String
    Dim NewText As String
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpUrno As String
    Dim tmpDateStamp As String
    Dim iCount As Integer
    Dim tmpTime As String
    
    txtPreview(0).Text = ""
    NewText = ""
    LineNo = TlxPrioTab.GetCurrentSelected
    tmpData = Trim(TlxPrioTab.GetColumnValue(LineNo, 0))
    If (tmpData <> "") And (tmpData <> "--") Then
        NewText = NewText & "=PRIORITY" & vbNewLine
        NewText = NewText & tmpData & " " & vbNewLine
    End If
    LineNo = DblSignTab.GetCurrentSelected
    tmpData = Trim(DblSignTab.GetColumnValue(LineNo, 0))
    If (tmpData <> "") And (tmpData <> "--") Then
        NewText = NewText & "=DBLSIG" & vbNewLine
        NewText = NewText & tmpData & " " & vbNewLine
    End If
    TabAddrList = GetCleanAddrList(txtDestAddr(0).Text, 8, ",", 4, "", False)
    If TabAddrList <> "" Then
        NewText = NewText & "=DESTINATION TYPE B" & vbNewLine
        iCount = 1
        tmpData = GetItem(TabAddrList, iCount, ",")
        While tmpData <> ""
            tmpData = CleanTrimString(tmpData)
            If tmpData <> "" Then
                NewText = NewText & "STX," & tmpData & vbNewLine
            End If
            iCount = iCount + 1
            tmpData = GetItem(TabAddrList, iCount, ",")
        Wend
    End If
    If MySitaOutOrig Then
        CurLine = TlxOrigTab.GetCurrentSelected
        If CurLine >= 0 Then
            tmpData = Trim(TlxOrigTab.GetColumnValue(CurLine, 0))
            If tmpData <> "" Then
                NewText = NewText & "=ORIGIN" & vbNewLine
                NewText = NewText & tmpData & vbNewLine
            End If
        End If
    End If
    If MySitaOutMsgId Then
        tmpUrno = Trim(NewSendUrno(0).Text)
        If tmpUrno = "" Then
            UfisServer.UrnoPoolPrepare 1
            tmpUrno = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
            If tmpUrno = "" Then tmpUrno = UfisServer.UrnoPoolGetNext
        End If
        NewSendUrno(0).Text = tmpUrno
        NewText = NewText & "=MSGID" & vbNewLine
        tmpTime = MySetUp.GetUtcServerTime
        tmpTime = Mid(tmpTime, 7, 6)
        NewText = NewText & tmpTime & vbNewLine
        'NewText = NewText & "UF" & tmpUrno & "IS" & vbNewLine
    End If
    If MySitaOutSmi Then
        CurLine = TlxTypeTab.GetCurrentSelected
        If CurLine >= 0 Then
            tmpData = Trim(TlxTypeTab.GetColumnValue(CurLine, 0))
            If tmpData = "FREE" Then tmpData = ""
            If tmpData <> "" Then
                NewText = NewText & "=SMI" & vbNewLine
                NewText = NewText & tmpData & vbNewLine
            End If
        End If
    End If
    tmpData = Trim(txtTelexText(0).Text)
    If tmpData <> "" Then
        NewText = NewText & "=TEXT" & vbNewLine
        NewText = NewText & tmpData & vbNewLine
    End If
    txtPreview(0).Text = NewText
End Sub
Private Function GetCleanAddrList(CurAddrList As String, AddrPerLine As Long, UseSepa As String, MaxLines As Long, AddOrigAddr As String, ClearList As Boolean) As String
    Dim Result As String
    Dim TxtAddrList As String
    Dim tmpData As String
    Dim itm As Integer
    Dim ItmCnt As Long
    Dim MaxCnt As Long
    Dim LinCnt As Long
    Dim ItmLen As Long
    MaxCnt = AddrPerLine * MaxLines
    TxtAddrList = CurAddrList
    TxtAddrList = Replace(TxtAddrList, Chr(10), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, Chr(13), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, Chr(9), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, " ", ",", 1, -1, vbBinaryCompare)
    tmpData = TxtAddrList
    TxtAddrList = Replace(tmpData, ",,", ",", 1, -1, vbBinaryCompare)
    While tmpData <> TxtAddrList
        tmpData = TxtAddrList
        TxtAddrList = Replace(tmpData, ",,", ",", 1, -1, vbBinaryCompare)
    Wend
    While Left(TxtAddrList, 1) = ","
        TxtAddrList = Mid(TxtAddrList, 2)
    Wend
    While Right(TxtAddrList, 1) = ","
        TxtAddrList = Left(TxtAddrList, Len(TxtAddrList) - 1)
    Wend
    If AddrPerLine > 0 Then
        If AddOrigAddr <> "" Then
            TxtAddrList = Replace(TxtAddrList, AddOrigAddr, "", 1, -1, vbBinaryCompare)
            MaxCnt = MaxCnt - 1
        End If
        Result = ""
        LinCnt = 1
        ItmCnt = 0
        itm = 1
        tmpData = GetItem(TxtAddrList, itm, ",")
        While (tmpData <> "") And (MaxCnt > 0)
            If (ItmCnt = AddrPerLine) And (LinCnt < MaxLines) Then
                Result = Result & vbNewLine
                LinCnt = LinCnt + 1
                ItmCnt = 0
            ElseIf ItmCnt > 0 Then
                Result = Result & UseSepa
            End If
            Result = Result & tmpData
            ItmCnt = ItmCnt + 1
            MaxCnt = MaxCnt - 1
            If ClearList Then
                ItmLen = Len(tmpData) + 2
                TxtAddrList = Mid(TxtAddrList, ItmLen)
                itm = itm - 1
            End If
            itm = itm + 1
            tmpData = GetItem(TxtAddrList, itm, ",")
        Wend
        If AddOrigAddr <> "" Then
            If ItmCnt = AddrPerLine Then
                Result = Result & vbNewLine
                Result = Result & AddOrigAddr
            Else
                Result = Result & UseSepa & AddOrigAddr
            End If
        End If
        If ClearList Then
            CurAddrList = TxtAddrList
        End If
    Else
        Result = TxtAddrList
        If UseSepa <> "," Then
            Result = Replace(Result, ",", UseSepa, 1, -1, vbBinaryCompare)
        End If
    End If
    GetCleanAddrList = Result
End Function

Private Function GetOutAddrList(Index As Integer) As String
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim AddrCount As Long
    Dim tmpTead As String
    Dim TabAddrList As String
    AddressTab(Index).SetInternalLineBuffer True
    TabAddrList = ""
    LineNo = Val(AddressTab(Index).GetLinesByStatusValue(1, 0))
    If LineNo > 0 Then
        While LineNo >= 0
            LineNo = AddressTab(Index).GetNextResultLine
            If LineNo >= 0 Then
                tmpTead = AddressTab(Index).GetColumnValue(LineNo, 2)
                If InStr(TabAddrList, tmpTead) = 0 Then
                    TabAddrList = TabAddrList & tmpTead & ","
                End If
            End If
        Wend
    End If
    AddressTab(Index).SetInternalLineBuffer False
    GetOutAddrList = TabAddrList
End Function

Private Sub chkReload_Click()
    If chkReload.Value = 1 Then
        chkReload.BackColor = LightGreen
        chkReload.Refresh
        chkFltData(0).Value = 1
        If FormIsLoaded("RcvTelex") Then
            RcvTelex.chkLoad.Value = 1
        End If
        If FormIsLoaded("SndTelex") Then
            SndTelex.chkLoad.Value = 1
        End If
        If FormIsLoaded("PndTelex") Then
            PndTelex.chkLoad.Value = 1
        End If
        If FormIsLoaded("OutTelex") Then
            OutTelex.chkLoad.Value = 1
        End If
        chkReload.Value = 0
    Else
        chkReload.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkReset_Click()
    Dim sveFlag As Boolean
    Dim i As Integer
    On Error Resume Next
    If chkReset.Value = 1 Then
        chkSend.BackColor = MyOwnButtonFace
        chkSend.ForeColor = vbBlack
        sveFlag = SystemInput
        SystemInput = True
        chkMvtAddr(5).Value = 1
        
        CurTlxTplText = ""
        CurTlxTplType = ""
        CurTlxTplCorr = ""
        CurTplFldList = ""
        CurTlxCorrCnt = -1
        
        CurViewPoint = ""
        CurAftFltUrno = ""
        CurTlxFltUrno = ""
        CurTlxTabUrno = ""
        CurTlxTplUrno = ""
        CurNewTplText = ""
        CurUsrTplText = ""
        TplTextIsUser = ""
        
        FltInputSlide.Height = 30
        InitFlightInfoPanel 0, "", ""
        CreateFlightDataPanel "", "", "", ""
        
        For i = 0 To txtFltInput.UBound
            FltEditPanel(i).Visible = False
            FltEditPanel(i).Tag = ""
            chkFltTick(i).Tag = ""
            chkFltCheck(i).Tag = ""
            lblFltField(i).Tag = ""
            lblDatType(i).Tag = ""
            txtFltInput(i).Tag = ""
            txtFltInput(i).Text = ""
            txtFltInput(i).BackColor = LightGrey
            chkFltCheck(i).Value = 0
            chkFltTick(i).Value = 0
        Next
        For i = 0 To AftFldText.UBound
            AftFldText(i).Text = ""
            AftFldText(i).BackColor = vbWhite
        Next
        AftFldText(40).BackColor = LightGrey
        AftFldLabel(10).Caption = ""
        For i = 0 To chkEditDL.UBound
            txtEditDL(i).Text = ""
            txtEditDL(i).Tag = ""
            chkEditDL(i).Tag = ""
            chkEditDL(i).Caption = ""
            chkEditDL(i).Value = 0
        Next
        For i = 0 To chkEditPX.UBound
            chkEditPX(i).Value = 0
            txtEditPX(i).Text = ""
            txtEditPX(i).Tag = ""
            chkEditPX(i).Tag = ""
            chkEditPX(i).Caption = ""
        Next
        For i = 0 To chkEditEET.UBound
            txtEditEET(i).Text = ""
            txtEditEET(i).Tag = ""
            chkEditEET(i).Tag = ""
            chkEditEET(i).Caption = ""
            chkEditEET(i).Value = 0
        Next
        chkEditEET(0).Caption = "EET"
        chkEditEET(4).Caption = "To"
        chkEditPX(0).Caption = "PX"
        chkEditDL(0).Caption = "DL"
        chkEditEET(5).Picture = LoadPicture("")
        chkEditDL(5).Picture = LoadPicture("")
        chkEditPX(5).Picture = LoadPicture("")
    
    'tmpCapt = txtVIAL.Text
    'If tmpCapt <> "" Then tmpCapt = Left(tmpCapt, 3) Else tmpCapt = txtAPC3.Text
    'txtEditEET(3).Text = tmpCapt
    
    'tmpCapt = Trim(GetFieldValue("EETF", UseAftData, UseCfgList))
    'SetEETMinutes tmpCapt
    
        For i = 0 To txtSiText.UBound
            txtSiText(i).Text = ""
        Next
        For i = 0 To txtDestAddr.UBound
            txtDestAddr(i).Text = ""
        Next
        For i = 0 To txtTelexText.UBound
            txtTelexText(i).Text = ""
            txtTelexText(i).Tag = ""
        Next
        For i = 0 To chkFltData.UBound
            chkFltData(i).Tag = ""
            chkFltData(i).BackColor = MyOwnButtonFace
            chkFltData(i).ForeColor = vbBlack
        Next
        StatusBar1.Panels(1).Text = ""
        StatusBar1.Panels(2).Text = ""
        
        chkPreView.Tag = ""
        For i = 0 To txtPreview.UBound
            txtPreview(i).Text = ""
            txtPreview(i).Tag = ""
        Next
        CurrentRecord.Text = ""
        For i = 0 To NewCallCode.UBound
            NewCallCode(i).Text = ""
        Next
        VScroll1(0).Value = 0
        VScroll1(0).Max = 2
        FltInputSlide.Top = 0
        chkPreView.Value = 0
        chkOutFormat.Value = 0
        chkFldAddr(0).Value = 0
        chkFldAddr(0).ForeColor = vbBlack
        chkFldAddr(1).Value = 0
        chkFldAddr(1).ForeColor = vbBlack
        TlxTypeTab.SetCurrentSelection -1
        CheckSendStatus
        'PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("blackarrow")).Picture
        PanelIcon(6).Tag = "SHOW"
        txtTelexText(0).BackColor = vbWhite
        txtTelexText(0).Locked = True
        SystemInput = sveFlag
        chkReset.Value = 0
    End If
End Sub

Private Sub chkSelAddr_Click(Index As Integer)
    Dim tmpTag As String
    Dim LastIndex As Integer
    If chkSelAddr(Index).Value = 1 Then
        fraDestAddr(0).Caption = "Destination Addresses"
        fraDestAddr(1).Caption = "Appended Home Addresses"
        Label1.Caption = fraDestAddr(1).Caption
        tmpTag = chkSelAddr(0).Tag
        If tmpTag <> "" Then
            LastIndex = Val(tmpTag)
            If LastIndex <> Index Then chkSelAddr(LastIndex).Value = 0
        End If
        chkSelAddr(Index).BackColor = MyOwnButtonDown
        chkSelAddr(0).Tag = CStr(Index)
        Select Case Index
            Case 0  ' MVT Telexes
                AddressTab(0).Visible = True
                AddressTab(1).Visible = False
                'chkTlxAddr(0).Visible = True
                'chkTlxAddr(1).Visible = True
                'chkTlxAddr(2).Visible = True
                txtDestAddr(0).Visible = True
                txtDestAddr(1).Visible = False
                txtDestAddr(2).Visible = True
                txtDestAddr(3).Visible = False
                fraDestAddr(0).Caption = "      Destination Telex Addresses"
                fraDestAddr(1).Caption = "Appended Home Telex Addresses"
                Label1.Caption = fraDestAddr(1).Caption
                chkFldAddr(0).Value = 1
            Case 1  ' eMail
                AddressTab(0).Visible = False
                AddressTab(1).Visible = True
                'chkTlxAddr(0).Visible = True
                'chkTlxAddr(1).Visible = True
                'chkTlxAddr(2).Visible = True
                txtDestAddr(0).Visible = False
                txtDestAddr(1).Visible = True
                txtDestAddr(2).Visible = False
                txtDestAddr(3).Visible = True
                fraDestAddr(0).Caption = "      Destination eMail Addresses"
                fraDestAddr(1).Caption = "Appended Home eMail Addresses"
                Label1.Caption = fraDestAddr(1).Caption
                chkFldAddr(1).Value = 1
            Case Else
        End Select
    Else
        chkSelAddr(Index).BackColor = MyOwnButtonFace
        Select Case Index
            Case 0
                chkFldAddr(0).Value = 0
            Case 1
                chkFldAddr(1).Value = 0
            Case Else
        End Select
    End If
End Sub

Private Sub chkSelect_Click()
    If chkSelect.Value = 1 Then
        chkSelect.BackColor = MyOwnButtonDown
        FltListPanel(0).Visible = True
        'TestCases.Show , Me
    Else
        'TestCases.Hide
        chkSelect.BackColor = MyOwnButtonFace
        FltListPanel(0).Visible = False
    End If
End Sub

Private Sub chkSetup_Click(Index As Integer)
    If chkSetup(Index).Value = 1 Then
        chkSetup(Index).BackColor = MyOwnButtonDown
        Select Case Index
            Case 0
                fraSetup(Index).Left = fraTopButtons(0).Left + chkSetup(Index).Left
                fraSetup(Index).Top = fraTopButtons(0).Top + fraTopButtons(0).Height
                fraSetup(Index).Visible = True
                fraSetup(Index).ZOrder
            Case 1
                'fraSetup(Index).Left = fraTopButtons(0).Left + chkSetup(Index).Left
                'fraSetup(Index).Top = fraTopButtons(0).Top + fraTopButtons(0).Height
                'fraSetup(Index).Visible = True
                'fraSetup(Index).ZOrder
                chkSetup(3).Value = 1
            Case 2
                fraSetup(Index).Left = fraTopButtons(0).Left + chkSetup(Index).Left
                fraSetup(Index).Top = fraTopButtons(0).Top + fraTopButtons(0).Height
                fraSetup(Index).Visible = True
                fraSetup(Index).ZOrder
                chkSetup(4).Value = 1
            Case 3
                'fraSetup(Index).Left = fraFlightPanel(0).Left
                'fraSetup(Index).Width = fraFlightPanel(0).Width
                'fraSetup(Index).Top = fraTopButtons(0).Top + fraTopButtons(0).Height
                fraSetup(Index).Visible = True
                'fraSetup(Index).ZOrder
            Case 4
                fraSetup(Index).Left = fraTopButtons(0).Left + chkSetup(2).Left
                'fraSetup(Index).Top = fraTopButtons(0).Top + fraTopButtons(0).Height
                fraSetup(Index).Top = fraSetup(2).Top + fraSetup(2).Height - 120
                fraSetup(Index).Visible = True
                fraSetup(Index).ZOrder
                fraSetup(2).ZOrder
            Case 5, 6, 7 'Open SubPanel
                fraSetup(Index).Left = fraSetup(0).Left + chkSetup(Index).Left '- 60
                fraSetup(Index).Top = fraSetup(0).Top + fraSetup(0).Height
                fraSetup(Index).Visible = True
                fraSetup(Index).ZOrder
            Case Else
        End Select
    Else
        chkSetup(Index).BackColor = MyOwnButtonFace
        fraSetup(Index).Visible = False
        Select Case Index
            Case 0
                chkSetup(5).Value = 0
                chkSetup(6).Value = 0
                chkSetup(7).Value = 0
            Case 1
                chkSetup(3).Value = 0
            Case 2
                chkSetup(4).Value = 0
            Case Else
        End Select
    End If
End Sub

Private Sub chkSplit_Click(Index As Integer)
    If chkSplit(Index).Value = 1 Then
        chkSplit(Index).BackColor = MyOwnButtonDown
        If Not MouseIsDown Then chkSplit(Index).Value = 0
    Else
        chkSplit(Index).BackColor = MyOwnButtonFace
    End If
End Sub

Private Sub chkSplit_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    chkSplit(Index).ZOrder
    MouseX = x
    MouseY = y
    MouseIsDown = True
    chkSplit(Index).Value = 1
End Sub

Private Sub chkSplit_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Static LastDiff As Long
    Dim MouseDiff As Long
    Dim NewTop As Long
    Dim MinTop As Long
    Dim MaxTop As Long
    Dim NewHeight As Long
    If MouseIsDown Then
        MouseDiff = y - MouseY
        If MouseDiff <> LastDiff Then
            LastDiff = MouseDiff
            NewTop = chkSplit(Index).Top + MouseDiff
            MinTop = ((MyFontSize * 5) * 15) + 30
            MaxTop = FltListPanel(0).ScaleHeight - MinTop - chkSplit(Index).Height + 30
            If (NewTop >= MinTop) And (NewTop <= MaxTop) Then
                chkSplit(Index).Top = NewTop
                FltListPanel(1).Height = FltListPanel(1).Height + MouseDiff
                TlxFlights(0).Height = FltListPanel(1).ScaleHeight
                TlxFlights(1).Height = FltListPanel(1).ScaleHeight
                TlxFlights(0).Refresh
                TlxFlights(1).Refresh
                FltListPanel(2).Top = FltListPanel(2).Top + MouseDiff
                FltListPanel(2).Height = FltListPanel(2).Height - MouseDiff
                TabFlights(0).Height = FltListPanel(2).ScaleHeight
                TabFlights(1).Height = FltListPanel(2).ScaleHeight
                FltListPanel(2).Refresh
                If TabFlights(0).Visible Then TabFlights(0).Refresh
                If TabFlights(1).Visible Then TabFlights(1).Refresh
                FltListPanel(2).Refresh
            End If
            DoEvents
        End If
    End If
End Sub

Private Sub chkSplit_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    chkSplit(Index).Value = 0
    MouseIsDown = False
End Sub

Private Sub chkTestCase_Click()
    Dim i As Integer
    Dim j As Integer
    On Error Resume Next
    If chkTestCase.Value = 1 Then
        chkTestCase.BackColor = vbYellow
        chkTestCase.Caption = "Updating"
        For i = 2 To 5
            j = i + 13
            AftFldText(j).Text = AftTestValue(i).Text
            AftFldText(j).Tag = AftFldText(j).Text
            AftFldText(j).BackColor = vbWhite
            AftFldText(j).ForeColor = vbBlack
        Next
        InitFlightInfoDetails 2, MyAftTabFields, MyAftTabRecord
        AftTestValue(1).SetFocus
    Else
        chkTestCase.BackColor = MyOwnButtonFace
        chkTestCase.Caption = "Update"
        For i = 0 To MyTestCase.UBound
            MyTestCase(i).Text = ""
        Next
    End If
End Sub

Private Sub chkText_Click()
    If chkText.Value = 1 Then
        chkText.BackColor = LightGreen
        txtTelexText_Change 0
        chkTlxTxt.Visible = True
        PanelIcon(6).Tag = "EDIT"
        chkTlxTxt.Value = 1
    Else
        chkText.BackColor = vbButtonFace
        chkTlxTxt.Value = 0
        chkTlxTxt.Visible = False
    End If
End Sub

Private Sub chkTlxAddr_Click(Index As Integer)
    If chkTlxAddr(Index).Value = 1 Then
        chkTlxAddr(Index).BackColor = MyOwnButtonDown
        Select Case Index
            Case 0
                chkTlxAddr(2).Enabled = False
            Case 1
                txtDestAddr(0).Text = GetCleanAddrList(txtDestAddr(0).Text, 8, " ", 4, "", False)
                chkTlxAddr(Index).Value = 0
            Case 2  'Reload
                FillAddrField False, 0, "", 1, ""
                chkTlxAddr(Index).Value = 0
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                chkTlxAddr(2).Enabled = True
            Case Else
        End Select
        chkTlxAddr(Index).BackColor = MyOwnButtonFace
    End If
End Sub

Private Sub chkTlxTxt_Click()
    If chkTlxTxt.Value = 1 Then
        chkTlxTxt.BackColor = LightYellow
        txtTelexText_Change 0
        'chkTlxTxt.Picture = imgAmpel(0).Picture
        txtTelexText(0).Locked = False
        txtTelexText(0).BackColor = LightestYellow
        PanelIcon(6).Tag = "OPEN"
        txtTelexText(0).SetFocus
    Else
        chkTlxTxt.BackColor = vbButtonFace
        'chkTlxTxt.Picture = imgAmpel(1).Picture
        txtTelexText(0).Locked = True
        txtTelexText(0).BackColor = vbWhite
        PanelIcon(6).Tag = "EDIT"
    End If
End Sub

Private Sub chkTplPatch_Click(Index As Integer)
    Dim i As Integer
    If chkTplPatch(Index).Value = 1 Then
        Select Case Index
            Case 0
                TplFldCreateEditor = True
            Case 1
                TplUpdateCascading = True
            Case 3
                ShowFldDsscColFlag = True
                For i = 0 To picFldRelIdx.UBound
                    picFldRelIdx(i).Visible = True
                Next
            Case 4
                TplUpdateFldGroups = True
            Case 6
                TplMainFieldsOnly = True
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                TplFldCreateEditor = False
            Case 1
                TplUpdateCascading = False
            Case 3
                ShowFldDsscColFlag = False
                For i = 0 To picFldRelIdx.UBound
                    picFldRelIdx(i).Visible = False
                Next
            Case 4
                TplUpdateFldGroups = False
            Case 6
                TplMainFieldsOnly = False
            Case Else
        End Select
    End If
End Sub

Private Sub chkUcase_Click()
    If chkUcase.Value = 1 Then
        chkUcase.BackColor = MyOwnButtonDown
        txtPreview(0).Tag = txtPreview(0).Text
        txtPreview(0).Text = UCase(txtPreview(0).Text)
    Else
        txtPreview(0).Text = txtPreview(0).Tag
        chkUcase.BackColor = MyOwnButtonFace
    End If
End Sub

Private Sub chkView_Click(Index As Integer)
    Dim AllBtnUp As Boolean
    AllBtnUp = True
    If chkView(0).Value = 1 Then AllBtnUp = False
    If chkView(1).Value = 1 Then AllBtnUp = False
    If chkView(2).Value = 1 Then AllBtnUp = False
    If chkView(Index).Value = 1 Then
        chkView(Index).BackColor = MyOwnButtonDown
        Select Case Index
            Case 0
                SwitchMainViewPoint 2, "TLX_TXT"
            Case 1
                SwitchMainViewPoint 2, "ANY_ARR"
                fraFlightPanel(2).Refresh
            Case 2
                SwitchMainViewPoint 2, "ANY_DEP"
                fraFlightPanel(2).Refresh
            Case 3
                If Not AllBtnUp Then
                    If chkView(0).Value = 1 Then
                        TplListPanel.Visible = False
                        fraMtpTmplText.Visible = True
                        fraMtpTmplText.ZOrder
                        txtTelexText(0).Text = CurNewTplText
                        TplTextIsUser = False
                    Else
                        TplListPanel.Visible = True
                        fraMtpTmplText.Visible = False
                    End If
                Else
                    chkView(3).Value = 0
                End If
            Case 4
                fraFlightCover(0).Visible = True
                fraFlightCover(0).Refresh
                fraFlightPanel(2).Visible = True
                fraFlightPanel(2).Refresh
            Case Else
        End Select
    Else
        chkView(Index).BackColor = MyOwnButtonFace
        Select Case Index
            Case 0
                FltInputPanel.Visible = False
                VScroll1(0).Visible = False
                If chkView(3).Value = 1 Then
                    chkView(3).Value = 0
                    chkView(3).Value = 1
                End If
            Case 1
                FltListPanel(0).Visible = False
                If chkView(3).Value = 1 Then
                    chkView(3).Value = 0
                    chkView(3).Value = 1
                End If
            Case 2
                FltListPanel(0).Visible = False
                If chkView(3).Value = 1 Then
                    chkView(3).Value = 0
                    chkView(3).Value = 1
                End If
            Case 3
                TplListPanel.Visible = False
                fraMtpTmplText.Visible = False
                txtTelexText(0).Text = CurUsrTplText
                TplTextIsUser = True
            Case 4
                fraFlightPanel(2).Visible = False
                fraFlightCover(0).Visible = False
        End Select
        AllBtnUp = True
        If chkView(0).Value = 1 Then AllBtnUp = False
        If chkView(1).Value = 1 Then AllBtnUp = False
        If chkView(2).Value = 1 Then AllBtnUp = False
        If AllBtnUp Then
            chkView(3).Value = 0
            'chkView(3).Enabled = False
        End If
    End If
End Sub

Private Sub chkWeb_Click()
    If chkWeb.Value = 1 Then
        chkWeb.BackColor = LightGreen
        MyWebBrowser.Show
        chkWeb.Value = 0
    Else
        chkWeb.BackColor = vbButtonFace
    End If
End Sub

Private Sub Command1_Click()
    UfisServer.Show
End Sub

Private Sub Command2_Click()
    Dim AftFldList As String
    Dim AftFldData As String
    Dim TlxTplText As String
    Dim tmpFile As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    Dim tmpPath As String
    'Change Drive D to C
    tmpPath = UFIS_TMP
    'tmpFile = "d:\tmp\afttabflights_0.txt"
    tmpFile = tmpPath + "\afttabflights_0.txt"
    TabFlights(0).WriteToFile tmpFile, False
    'tmpFile = "d:\tmp\afttabflights_1.txt"
    tmpFile = tmpPath + "\afttabflights_1.txt"
    TabFlights(1).WriteToFile tmpFile, False
    'tmpFile = "d:\tmp\tlxtabflights_0.txt"
    tmpFile = tmpPath + "\tlxtabflights_0.txt"
    TlxFlights(0).WriteToFile tmpFile, False
    'tmpFile = "d:\tmp\tlxtabflights_1.txt"
    tmpFile = tmpPath + "\tlxtabflights_1.txt"
    TlxFlights(1).WriteToFile tmpFile, False
    'tmpFile = "d:\tmp\sndtabflights.txt"
    tmpFile = tmpPath + "\sndtabflights.txt"
    SndTelex.tabTelexList.WriteToFile tmpFile, False
    'tmpFile = "d:\tmp\rcvtabflights_0.txt"
    tmpFile = tmpPath + "\rcvtabflights_0.txt"
    RcvTelex.tabTelexList(0).WriteToFile tmpFile, False
    'tmpFile = "d:\tmp\rcvtabflights_1.txt"
    tmpFile = tmpPath + "\rcvtabflights_1.txt"
    RcvTelex.tabTelexList(1).WriteToFile tmpFile, False
    
    'tmpFile = "d:\tmp\outtabflights.txt"
    tmpFile = tmpPath + "\outtabflights.txt"
    OutTelex.tabTelexList.WriteToFile tmpFile, False
    
    For i = 0 To UfisServer.BasicData.UBound
        'tmpFile = "d:\tmp\srvbasics_" & CStr(i) & ".txt"
        tmpFile = tmpPath + "\srvbasics_" & CStr(i) & ".txt"
        UfisServer.BasicData(i).WriteToFile tmpFile, False
    Next
    
    'tmpFile = "d:\tmp\tlxtablinetags_0.txt"
    tmpFile = tmpPath + "\tlxtablinetags_0.txt"
    Open tmpFile For Output As #1
    MaxLine = TlxFlights(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpData = TlxFlights(0).GetLineTag(CurLine)
        Print #1, tmpData
    Next
    Close #1

    'tmpFile = "d:\tmp\tlxtablinetags_1.txt"
    tmpFile = tmpPath + "\tlxtablinetags_1.txt"
    Open tmpFile For Output As #1
    MaxLine = TlxFlights(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpData = TlxFlights(1).GetLineTag(CurLine)
        Print #1, tmpData
    Next
    Close #1

End Sub

Private Sub Command3_Click()
    Static AlreadyDone As Boolean
    Dim AftFldList As String
    Dim AftFldData As String
    Dim TlxTplText As String
    Dim tmpFile As String
    Dim tmpData As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    Dim tmpPath As String
    
    tmpPath = UFIS_TMP
    
    If Not AlreadyDone Then
        AlreadyDone = True
        
        'tmpFile = "d:\tmp\afttabflights_0.txt"
        tmpFile = tmpPath + "\afttabflights_0.txt"
        TabFlights(0).ResetContent
        TabFlights(0).ReadFromFile tmpFile
        TabFlights(0).AutoSizeColumns
        
        'tmpFile = "d:\tmp\afttabflights_1.txt"
        tmpFile = tmpPath + "\afttabflights_1.txt"
        TabFlights(1).ResetContent
        TabFlights(1).ReadFromFile tmpFile
        TabFlights(1).AutoSizeColumns
    
        'tmpFile = "d:\tmp\tlxtabflights_0.txt"
        tmpFile = tmpPath + "\tlxtabflights_0.txt"
        TlxFlights(0).ResetContent
        TlxFlights(0).ReadFromFile tmpFile
        TlxFlights(0).AutoSizeColumns
        
        'tmpFile = "d:\tmp\tlxtabflights_1.txt"
        tmpFile = tmpPath + "\tlxtabflights_1.txt"
        TlxFlights(1).ResetContent
        TlxFlights(1).ReadFromFile tmpFile
        TlxFlights(1).AutoSizeColumns
    
        'tmpFile = "d:\tmp\sndtabflights.txt"
        tmpFile = tmpPath + "\sndtabflights.txt"
        SndTelex.tabTelexList.ResetContent
        SndTelex.tabTelexList.ReadFromFile tmpFile
        SndTelex.tabTelexList.AutoSizeColumns
        
        'tmpFile = "d:\tmp\rcvtabflights_0.txt"
        tmpFile = tmpPath + "\rcvtabflights_0.txt"
        RcvTelex.tabTelexList(0).ResetContent
        RcvTelex.tabTelexList(0).ReadFromFile tmpFile
        RcvTelex.tabTelexList(0).AutoSizeColumns
        
        'tmpFile = "d:\tmp\rcvtabflights_1.txt"
        tmpFile = tmpPath + "\rcvtabflights_1.txt"
        RcvTelex.tabTelexList(1).ResetContent
        RcvTelex.tabTelexList(1).ReadFromFile tmpFile
        RcvTelex.tabTelexList(1).AutoSizeColumns
        
        'tmpFile = "d:\tmp\outtabflights.txt"
        tmpFile = tmpPath + "\outtabflights.txt"
        OutTelex.tabTelexList.ReadFromFile tmpFile
        OutTelex.tabTelexList.AutoSizeColumns
        
        'tmpFile = "d:\tmp\tlxtablinetags_0.txt"
        tmpFile = tmpPath + "\tlxtablinetags_0.txt"
        MaxLine = TlxFlights(0).GetLineCount - 1
        Open tmpFile For Input As #1
        For CurLine = 0 To MaxLine
            Line Input #1, tmpData
            TlxFlights(0).SetLineTag CurLine, tmpData
        Next
        Close #1
        
        'tmpFile = "d:\tmp\tlxtablinetags_1.txt"
        tmpFile = tmpPath + "\tlxtablinetags_1.txt"
        MaxLine = TlxFlights(1).GetLineCount - 1
        Open tmpFile For Input As #1
        For CurLine = 0 To MaxLine
            Line Input #1, tmpData
            TlxFlights(1).SetLineTag CurLine, tmpData
            tmpData = TlxFlights(1).GetFieldValue(CurLine, "DCO1")
            If tmpData = "C1" Then TlxFlights(1).SetFieldValues CurLine, "DCO1,DCO2,DUR1,DUR2", ",,0000,0000"
        Next
        Close #1
        
        For i = 0 To UfisServer.BasicData.UBound
            'UfisServer.BasicData(i).ResetContent
            'tmpFile = "d:\tmp\srvbasics_" & CStr(i) & ".txt"
            tmpFile = tmpPath + "\srvbasics_" & CStr(i) & ".txt"
            UfisServer.BasicData(i).ReadFromFile tmpFile
        Next
                    
        UfisServer.BasicData(2).IndexCreate "ALCAPC", 0
        UfisServer.BasicData(2).AutoSizeColumns
        
        UfisServer.BasicData(3).IndexCreate "ALCTPL", 0
        UfisServer.BasicData(3).IndexCreate "URNO", 20
        UfisServer.BasicData(3).AutoSizeColumns
        
    End If

End Sub

Private Sub Command4_Click()
    Dim tmpData As String
    UfisServer.CallCeda tmpData, "CCO", "TCPCHECK", "F", "D", "W", "", 0, True, False
    'MsgBox tmpData
End Sub

Private Sub FixWin_Click()
    Dim sveFlag As Boolean
    If FixWin.Value = 1 Then
        Me.WindowState = vbNormal
        FixWin.BackColor = MyOwnButtonDown
        sveFlag = GeneratorIsReadyForUse
        GeneratorIsReadyForUse = False
        MoveSubPanels -1
        GeneratorIsReadyForUse = sveFlag
        Form_Resize
        SystemResizing = True
        InitPosSize True
        
    Else
        FixWin.BackColor = MyOwnButtonFace
        SystemResizing = False
    End If
End Sub

Private Sub Form_Activate()
    Static IsAlreadyDone As Boolean
    Static IsSnapped As Boolean
    Static GotTlxAddress As Boolean
    Dim TlxGenCfg As String
    Dim tmpData As String
    Dim retval As Integer
    If FormIsLoaded("TextShape") Then
        Unload TextShape
        Me.Refresh
        UfisServer.CallCeda tmpData, "SBC", "MVTGEN/TRG", "F", "D", "W", "", 0, False, False
    End If
    MainOpenedByUser = True
    If Not MeIsVisible Then
        Me.Caption = Me.Tag
        
        Me.Refresh
        MeIsVisible = True
        TplTextIsUser = True
        Me.Show
        Me.Refresh
        chkReset.Value = 1
        chkEdit2.Value = 1
        FltInputPanel.Refresh
        chkView(2).Value = 1
        Me.Refresh
        If Not GotTlxAddress Then
            GotTlxAddress = True
            chkGetCfg.Value = 1
            InitOrigList
        End If
        'If UfisServer.HostName = "LOCAL" Then
            AftTimer.Tag = "0"
            'AftTimer_Timer
        'End If
        chkFltData(0).Value = 1
        FltListPanel(0).Refresh
        Me.Refresh
        If (Not IsAlreadyDone) Then
            If UfisServer.HostName = "LOCAL" Then
                Command3_Click
            End If
        End If
    End If
    CreateTelexIsOpen = Me.Visible
    GeneratorIsReadyForUse = True
    HiddenMain.Hide
    'Unload HiddenMain
    GeneratorIsReadyForUse = True
    If (GeneratorIsReadyForUse) And (Not IsAlreadyDone) Then
        InitPanelIcons
        MainPanel(0).Visible = True
        MainPanel(1).Visible = False
        If (PoolConfig.OnLineCfg(1).Value = 1) Or (MainIsOnline = True) Then
            'RcvTelex.Show
            chkOnline(1).Value = 1
        End If
        If PoolConfig.OnLineCfg(2).Value = 1 Then
            chkOnline(2).Value = 1
            'SndTelex.Show
        End If
        If PoolConfig.OnLineCfg(5).Value = 1 Then
            chkOnline(3).Value = 1
            'PndTelex.Show
        End If
        If (PoolConfig.OnLineCfg(3).Value = 1) Or (MainIsOnline = True) Then
            chkOnline(4).Value = 1
            'OutTelex.Show
        End If
        If PoolConfig.OnLineCfg(4).Value = 1 Then
            'chkOnline(x).Value = 1
            'RcvInfo.Show
        End If
        CreateTelex.FixWin.Value = 1
        CreateTelex.FixWin.Value = 0
        IsAlreadyDone = True
        If (AutoSnapMvtGenerator) And (Not IsSnapped) Then
            IsSnapped = True
            Load MouseClock
'            If Not ApplIsInDesignMode Then
'                Set m_Snap = New CSnapDialog
'                m_Snap.hWnd = Me.hWnd
'            End If
        End If
    End If
End Sub

Private Sub Form_Load()
    Dim tmpData As String
    Dim tmpStrg As String
    Dim tmpFields As String
    Dim MainTop As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewSize As Long
    Dim i As Integer
    MeIsVisible = False
    GeneratorIsReadyForUse = False
    AutoSendInitLoop = True
    chkSend.Left = 0
    On Error Resume Next
        If (AutoSnapMvtGenerator) Then
            If Not ApplIsInDesignMode Then
                Set m_Snap = New CSnapDialog
                m_Snap.hwnd = Me.hwnd
            End If
        End If
    MainPanel(0).Visible = True
    Me.Move 0, Screen.Height, 600 * 15, 600 * 15
    chkTplPatch(0).Value = 1
    chkTplPatch(1).Value = 1
    chkTplPatch(4).Value = 1
    chkTplPatch(6).Value = 1
    
    
    TplRuleLineNo = -1
    chkEditEET(5).Picture = LoadPicture("")
    chkEditDL(5).Picture = LoadPicture("")
    chkEditPX(5).Picture = LoadPicture("")
    Select Case MySitaOutType
        Case "SITA_NODE"
            chkOutFormat.Enabled = False
        Case "SITA_FILE"
            chkOutFormat.Enabled = True
        Case Else
    End Select
    
    ShowHorzSplit = False
    ShowVertSplit = True
    MainSplitHorzM(0).Height = 90
    MainSplitHorzM(0).BorderStyle = 0
    MainSplitHorzS(0).Height = 90
    MainSplitHorzS(0).BorderStyle = 0
    SubSplitHorzM(0).Height = 90
    SubSplitHorzM(0).BorderStyle = 0
    SubSplitHorzS(0).Height = 90
    SubSplitHorzS(0).BorderStyle = 0
    
    SubSplitVertM(0).Width = 45
    SubSplitVertS(0).Width = 45
    
    MainTop = fraTopButtons(0).Top + fraTopButtons(0).Height
    MainTool(0).Top = MainTop
    'MainTop = MainTop + MainTool(0).Height
    MainPanel(0).Top = MainTop
    MainPanel(0).Left = 0
    MainPanel(0).ZOrder
    
    
    CreateSubPanels 2
    SubSplitVertM(1).ZOrder
    SubSplitVertS(1).ZOrder
    
    NewTop = MainSplitHorzM(0).Height
    NewLeft = 0 - SubSplitVertM(0).Width
    NewLeft = 0
    For i = 0 To SubPanelM.UBound
        Set MainSplitHorzM(i).Container = MainPanel(0)
        MainSplitHorzM(i).Top = 0
        MainSplitHorzM(i).Left = NewLeft
        MainSplitHorzM(i).Visible = True
        MainSplitHorzM(i).ZOrder
        
        Set SubSplitVertM(i).Container = MainPanel(0)
        SubSplitVertM(i).Top = 0
        SubSplitVertM(i).Left = NewLeft
        SubSplitVertM(i).Visible = True
        NewLeft = NewLeft + SubSplitVertM(i).Width
        Set SubPanelM(i).Container = MainPanel(0)
        SubPanelM(i).Top = NewTop
        SubPanelM(i).Left = NewLeft
        SubPanelM(i).Visible = True
        NewLeft = NewLeft + SubPanelM(i).Width
        Set SubSplitVertS(i).Container = MainPanel(0)
        SubSplitVertS(i).Top = 0
        SubSplitVertS(i).Left = NewLeft
        SubSplitVertS(i).Visible = True
        NewLeft = NewLeft + SubSplitVertM(i).Width
        Set SubPanelS(i).Container = MainPanel(0)
        SubPanelS(i).Top = 0
        SubPanelS(i).Left = NewLeft
        SubFrameS(i).Visible = False
        SubPanelS(i).Visible = True
        PicVSplitM(i).Height = Screen.Height
        PicVSplitS(i).Height = Screen.Height
        PicVSplitM(i).Visible = False
        PicVSplitS(i).Visible = False
        SubSplitHorzS(i).Visible = False
        
        Set MainSplitHorzS(i).Container = MainPanel(0)
        MainSplitHorzS(i).Top = MainPanel(0).ScaleHeight - MainSplitHorzS(i).Height
        MainSplitHorzS(i).Left = NewLeft
        MainSplitHorzS(i).Visible = True
        MainSplitHorzS(i).ZOrder
        NewLeft = NewLeft + SubPanelS(i).Width
    Next
    
    'Configure Startup Values
    optResize(3).Value = True
    
    'chkBorder(0).Value = 1
    chkBorder(0).Value = 0
    chkBorder(0).Value = 1
    
    InitSubPanelMs -1
    
    
    fraChkAddr.BackColor = MyOwnButtonFace
    
    fraDestAddr(0).Visible = True
    fraDestAddr(1).Visible = True
    
    For i = 0 To fraTopButtons.UBound
        fraTopButtons(i).BackColor = MyOwnButtonFace
    Next
    'fraTopButtons(1).ZOrder
    fraTopButtons(2).ZOrder
    'fraTopButtons(1).BackColor = LightYellow
    
    For i = 0 To fraFlightPanel.UBound
        fraFlightPanel(i).BackColor = MyOwnButtonFace
        fraFlightPanel(i).BackColor = LightGray
    Next
    For i = 0 To AftFldText.UBound
        AftFldPanel(i).BackColor = MyOwnButtonFace
        AftFldText(i).Text = ""
        AftFldText(i).BackColor = vbWhite
        AftFldText(i).ForeColor = vbBlack
    Next
    
    
    FltListPanel(0).Top = FltInputPanel.Top
    FltListPanel(1).Top = -30
    FltListPanel(1).Left = -30
    FltListPanel(1).Width = FltListPanel(0).Width
    FltListPanel(1).Height = ((MyFontSize * 9) * 15) + 60
    FltListPanel(2).Left = -30
    FltListPanel(2).Width = FltListPanel(0).Width
    
    chkSplit(0).Left = -30
    chkSplit(0).Height = 135
    chkSplit(0).Top = FltListPanel(1).Top + FltListPanel(1).Height
    chkSplit(0).Width = FltListPanel(0).Width + 30
    
    FltListPanel(2).Top = chkSplit(0).Top + chkSplit(0).Height - 30
    
    TlxFlights(0).Top = 0
    TlxFlights(0).Left = 0
    TlxFlights(0).Width = FltListPanel(1).ScaleWidth
    TlxFlights(0).Height = FltListPanel(1).ScaleHeight
    TlxFlights(1).Top = 0
    TlxFlights(1).Left = 0
    TlxFlights(1).Width = FltListPanel(1).ScaleWidth
    TlxFlights(1).Height = FltListPanel(1).ScaleHeight
    
    TabFlights(0).Top = 0
    TabFlights(0).Left = 0
    TabFlights(0).Width = FltListPanel(2).ScaleWidth
    TabFlights(0).Height = FltListPanel(2).ScaleHeight
    TabFlights(1).Top = 0
    TabFlights(1).Left = 0
    TabFlights(1).Width = FltListPanel(2).ScaleWidth
    TabFlights(1).Height = FltListPanel(2).ScaleHeight
    
    StatusBar1.ZOrder
    
    TlxPrioTab.ResetContent
    TlxPrioTab.FontName = "Courier New"
    TlxPrioTab.HeaderFontSize = MyFontSize
    TlxPrioTab.FontSize = MyFontSize
    TlxPrioTab.lineHeight = MyFontSize
    TlxPrioTab.SetTabFontBold MyFontBold
    TlxPrioTab.HeaderString = "Prio,Remark                         "
    TlxPrioTab.HeaderLengthString = "100,100"
    TlxPrioTab.LifeStyle = True
    'TlxPrioTab.CursorLifeStyle = True
    TlxPrioTab.AutoSizeByHeader = True
    TlxPrioTab.AutoSizeColumns
    TlxPrioTab.Tag = CStr(fraTlxPrio.Width) & "," & CStr(fraTlxPrio.Height)
    
    DblSignTab.ResetContent
    DblSignTab.FontName = "Courier New"
    DblSignTab.HeaderFontSize = MyFontSize
    DblSignTab.FontSize = MyFontSize
    DblSignTab.lineHeight = MyFontSize
    DblSignTab.SetTabFontBold MyFontBold
    DblSignTab.HeaderString = "LC,Remark                           "
    DblSignTab.HeaderLengthString = "100,100"
    DblSignTab.LifeStyle = True
    'DblSignTab.CursorLifeStyle = True
    DblSignTab.AutoSizeByHeader = True
    DblSignTab.AutoSizeColumns
    DblSignTab.Tag = CStr(fraDblSign.Width) & "," & CStr(fraDblSign.Height)

    
    FldPropTab(0).ResetContent
    tmpData = "TPOS,FINA,REPL,TYPE,LBL1,LBL2,DATA,DSSC"
    tmpStrg = "10,10,10,10,10,10,10,10"
    FldPropTab(0).HeaderString = tmpData
    FldPropTab(0).LogicalFieldList = tmpData
    FldPropTab(0).ColumnWidthString = tmpStrg
    FldPropTab(0).HeaderLengthString = tmpStrg
    FldPropTab(0).AutoSizeByHeader = True
    FldPropTab(0).AutoSizeColumns
    
    
    Set FltDlgCover(0).Container = fraFlightData
    FltDlgCover(0).Top = EditDlgPanel(0).Top
    FltDlgCover(0).Left = EditDlgPanel(0).Left
    FltDlgCover(0).Height = EditDlgPanel(0).Height
    FltDlgCover(0).Width = EditDlgPanel(0).Width
    
    Set FltDlgCover(1).Container = fraFlightData
    FltDlgCover(1).Top = FltInputPanel.Top
    FltDlgCover(1).Left = FltInputPanel.Left
    FltDlgCover(1).Width = FltListPanel(0).Width
    
    TabFlights(0).ResetContent
    tmpFields = "FLNO,STOA,LAND,ONBL,ORG3,VIA3,ETAI,ADID,REGN,ACT5,ACT3,DES3,VIAL,STOD,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(0).LogicalFieldList = tmpFields
    TabFlights(0).HeaderString = "FLNO,STOA,LAND,ONBL,ORG,VIA,ETAI,ADID,REGN,ACT5,ACT3,DES3,VIAL,STOD,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(0).HeaderLengthString = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100"
    TabFlights(0).ColumnAlignmentString = "L,C,C,C,C,C,C,C,L,C,C,C,L,C,L,L,L,L,L,L"
    TabFlights(0).HeaderAlignmentString = "L,C,C,C,C,C,C,C,L,C,C,C,L,C,L,L,L,L,L,L"
    TabFlights(0).FontName = "Courier New"
    TabFlights(0).HeaderFontSize = MyFontSize
    TabFlights(0).FontSize = MyFontSize
    TabFlights(0).lineHeight = MyFontSize
    TabFlights(0).SetTabFontBold MyFontBold
    TabFlights(0).MainHeader = True
    TabFlights(0).SetMainHeaderFont MyFontSize, False, False, True, 0, "Courier New"
    TabFlights(0).SetMainHeaderValues "4,16", "Actual ARR Flights,", ","
    TabFlights(0).DateTimeSetColumn 1
    TabFlights(0).DateTimeSetInputFormatString 1, "YYYYMMDDhhmmss"
    TabFlights(0).DateTimeSetOutputFormatString 1, "hhmm"
    TabFlights(0).DateTimeSetColumn 2
    TabFlights(0).DateTimeSetInputFormatString 2, "YYYYMMDDhhmmss"
    TabFlights(0).DateTimeSetOutputFormatString 2, "hhmm"
    TabFlights(0).DateTimeSetColumn 3
    TabFlights(0).DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
    TabFlights(0).DateTimeSetOutputFormatString 3, "hhmm"
    TabFlights(0).ShowHorzScroller True
    TabFlights(0).LifeStyle = True
    'TabFlights(0).CursorLifeStyle = True
    TabFlights(0).AutoSizeByHeader = True
    TabFlights(0).AutoSizeColumns
    
    TabFlights(1).ResetContent
    tmpFields = "FLNO,STOD,DES3,VIA3,ETDI,REGN,OFBL,AIRB,ACT5,ACT3,BAAA,ETAI,DCD1,DCD2,DTD1,DTD2,DELD,PAX1,PAX2,PAX3,ADID,VIAL,ORG3,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(1).LogicalFieldList = tmpFields
    TabFlights(1).HeaderString = "FLNO,STOD,DEST,VIA,ETDI,REGN,OFBL,AIRB,ACT5,ACT3,BAAA,ETAI,DCD1,DCD2,DTD1,DTD2,DELD,PAX1,PAX2,PAX3,ADID,VIAL,ORG3,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(1).HeaderLengthString = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100"
    TabFlights(1).ColumnAlignmentString = "L,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,L,L,L,L,L,L"
    TabFlights(1).HeaderAlignmentString = "L,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,L,L,L,L,L,L"
    TabFlights(1).FontName = "Courier New"
    TabFlights(1).HeaderFontSize = MyFontSize
    TabFlights(1).FontSize = MyFontSize
    TabFlights(1).lineHeight = MyFontSize
    TabFlights(1).SetTabFontBold MyFontBold
    TabFlights(1).MainHeader = True
    TabFlights(1).SetMainHeaderFont MyFontSize, False, False, True, 0, "Courier New"
    TabFlights(1).SetMainHeaderValues "4,25", "Actual DEP Flights,", ","
    TabFlights(1).DateTimeSetColumn 1
    TabFlights(1).DateTimeSetInputFormatString 1, "YYYYMMDDhhmmss"
    TabFlights(1).DateTimeSetOutputFormatString 1, "hhmm"
    TabFlights(1).DateTimeSetColumn 4
    TabFlights(1).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
    TabFlights(1).DateTimeSetOutputFormatString 4, "hhmm"
    TabFlights(1).ShowHorzScroller True
    TabFlights(1).LifeStyle = True
    'TabFlights(1).CursorLifeStyle = True
    TabFlights(1).AutoSizeByHeader = True
    TabFlights(1).AutoSizeColumns
    
    TplListTab.ResetContent
    TplListTab.FontName = "Courier New"
    TplListTab.HeaderFontSize = MyFontSize
    TplListTab.FontSize = MyFontSize
    TplListTab.lineHeight = MyFontSize
    TplListTab.SetTabFontBold MyFontBold
    TplListTab.LogicalFieldList = "TYPE,TEXT,TPLU"
    TplListTab.HeaderString = "TP,Telex Message Type               ,TPLU"
    TplListTab.HeaderLengthString = "100,100,100"
    TplListTab.LifeStyle = True
    'TplListTab.CursorLifeStyle = True
    TplListTab.AutoSizeByHeader = True
    TplListTab.AutoSizeColumns
    
    For i = 0 To LockButtons.UBound
        LockButtons(i).BackColor = MyOwnButtonFace
    Next
    
    FltEditSize = txtFltInput(0).Width
    
    EditDlgPanel(1).Width = EditDlgPanel(0).ScaleWidth
    EditDlgPanel(1).Height = EditDlgPanel(0).ScaleHeight
    Set TplListPanel.Container = fraFlightData
    TplListPanel.Top = EditDlgPanel(0).Top
    TplListPanel.Left = EditDlgPanel(0).Left
    TplListPanel.Height = EditDlgPanel(0).Height
    TplListPanel.Width = EditDlgPanel(0).Width
    TplListTab.Top = 0
    TplListTab.Left = 0
    TplListTab.Width = TplListPanel.ScaleWidth
    TplListTab.Height = TplListPanel.ScaleHeight
    
    Set fraMtpTmplText.Container = SubPanelM(2)
    fraMtpTmplText.Left = 0
    fraMtpTmplText.Top = fraTlxType.Top
    fraMtpTmplText.Width = SubPanelM(2).ScaleWidth
    fraMtpTmplText.Height = fraFlightData.Top + EditDlgPanel(0).Top + EditDlgPanel(0).Height - fraMtpTmplText.Top
    
    TplEditPanel.Left = 60
    TplEditPanel.Height = fraMtpTmplText.Height - TplEditPanel.Top - 60
    TplEditPanel.Width = fraMtpTmplText.Width - (TplEditPanel.Left * 2)
    txtTplText.Top = -30
    txtTplText.Left = -30
    txtTplText.Width = TplEditPanel.ScaleWidth + 60
    txtTplText.Height = TplEditPanel.ScaleHeight + 60
    
    For i = 0 To chkAddData.UBound
        Set EditFldPanel(i).Container = EditDlgPanel(1)
        EditFldPanel(i).Top = 0
        EditFldPanel(i).Left = 0
        EditFldPanel(i).Width = EditDlgPanel(1).ScaleWidth
        EditFldPanel(i).Height = EditDlgPanel(1).ScaleHeight
        EditFldPanel(i).BackColor = MyOwnButtonFace
        EditFldPanel(i).Visible = False
        chkAddData(i).ZOrder
    Next
    For i = 0 To chkEditDL.UBound
        txtEditDL(i).Top = chkEditDL(i).Top
        txtEditDL(i).Left = chkEditDL(i).Left
        txtEditDL(i).Width = chkEditDL(i).Width
        txtEditDL(i).Height = chkEditDL(i).Height
        chkEditDL(i).ZOrder
    Next
    For i = 0 To chkEditPX.UBound
        txtEditPX(i).Top = chkEditPX(i).Top
        txtEditPX(i).Left = chkEditPX(i).Left
        txtEditPX(i).Width = chkEditPX(i).Width
        txtEditPX(i).Height = chkEditPX(i).Height
        chkEditPX(i).ZOrder
    Next
    For i = 0 To chkEditEET.UBound
        txtEditEET(i).Top = chkEditEET(i).Top
        txtEditEET(i).Left = chkEditEET(i).Left
        txtEditEET(i).Width = chkEditEET(i).Width
        txtEditEET(i).Height = chkEditEET(i).Height
        chkEditEET(i).ZOrder
    
    Next
    
    picFldRelIdx(0).Top = chkFltTick(0).Top - 15
    picFldRelIdx(0).Left = chkFltTick(0).Left - 15
    picFldRelIdx(0).Height = 60
    picFldRelIdx(0).Width = 60
    
    EditDlgPanel(0).ZOrder
    TplListPanel.ZOrder
    TplEditPanel.ZOrder
    FltListPanel(0).ZOrder
    FltInputPanel.ZOrder
    VScroll1(0).ZOrder
    
    CreateFlightDataPanel "", "", "", ""
    chkReset.Value = 1
    Me.Show
    Me.Refresh
    Screen.MousePointer = 11
    InitTlxAddresses "INIT"
    InitTlxTypeList
    InitPrioList
    InitDblSignList ""
    Screen.MousePointer = 0
    chkSelAddr(0).Value = 1
    
    tmpStrg = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_BUTTON_SETUP", "NO")
    If tmpStrg = "YES" Then
        chkSetup(0).Visible = True
    Else
        chkSetup(0).Visible = False
        chkSetup(1).Left = chkSetup(0).Left
        chkSetup(2).Left = chkSetup(0).Left
    End If
    
    chkSetup(1).Visible = False
    chkSetup(2).Visible = False
    tmpStrg = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_BUTTON_EXPLAIN", "NO")
    If tmpStrg = "YES" Then chkSetup(2).Visible = True
    
    fraSetup(2).Width = fraSetupX(2).Width
    SetupPanel(0).BackColor = fraSetup(0).BackColor
    tmpStrg = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "DELAY_TEST_GENERATOR", "NO")
    If tmpStrg = "YES" Then
        Set fraSetup(4).Container = fraSetup(3)
        fraSetup(4).Top = 0
        fraSetup(4).Left = fraSetup(3).Width - fraSetup(4).Width
        fraSetup(4).Height = fraSetup(3).Height
        SetupPanel(0).Height = fraSetup(4).Height - SetupPanel(0).Top - 60
        MyTestCase(3).Visible = True
        MyTestCase(6).Visible = True
        fraSetup(4).Caption = "TEST CASE RESULT"
        fraSetup(4).Visible = True
        chkTestCase.ZOrder
        chkSetup(1).Top = chkSetup(2).Top
        chkSetup(1).Visible = True
        chkSetup(2).Visible = False
    End If
    
    MainPanel(1).ZOrder
    
    GeneratorIsReadyForUse = False
    MoveSubPanels -1
    GeneratorIsReadyForUse = True
    
    Me.Height = MaxScreenHeight
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = MaxScreenWidth - Me.Width - 3600
    NewSize = MaxScreenWidth / 15
    
    SetObjectFaceColor Me
    chkOnline(0).BackColor = MyOwnButtonFace
    For i = 0 To SubPanelS.UBound
        SubPanelS(i).BackColor = LightestBlue
    Next
    MainPanel(0).BackColor = LightBlue
    MainPanel(1).BackColor = LightGreen
    
    TlxVpfr(0).Text = GetItem(RecvPeriod, 1, ",")
    TlxVpto(0).Text = GetItem(RecvPeriod, 2, ",")
    TlxVpfr(1).Text = GetItem(SendPeriod, 1, ",")
    TlxVpto(1).Text = GetItem(SendPeriod, 2, ",")
    TlxVpfr(2).Text = GetItem(PendPeriod, 1, ",")
    TlxVpto(2).Text = GetItem(PendPeriod, 2, ",")
    TlxVpfr(3).Text = GetItem(SentPeriod, 1, ",")
    TlxVpto(3).Text = GetItem(SentPeriod, 2, ",")
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "UFIS_WEB_BROWSER", "YES")
    If tmpData = "YES" Then chkWeb.Enabled = True Else chkWeb.Enabled = False
    Me.Show
    fraFlightData.Visible = True
    Me.Refresh
    AutoSendInitLoop = False
End Sub
Private Sub CreateSubPanels(MaxIdx As Integer)
    Dim SubTop As Long
    Dim NewWidth As Long
    Dim NewTop As Long
    Dim idx As Integer
    Dim HSpl As Integer
    Dim SplCnt As Integer
    HSpl = 0
    SubTop = 0
    NewTop = 0
    
    idx = 0
    SubSplitVertM(idx).Enabled = ShowVertSplit
    SubSplitVertM(idx).Visible = True
    SubSplitVertS(idx).Enabled = ShowVertSplit
    SubSplitVertS(idx).Visible = True
    SubSplitHorzM(idx).Enabled = ShowHorzSplit
    SubSplitHorzM(idx).Visible = True
    SubSplitHorzS(idx).Enabled = ShowHorzSplit
    SubSplitHorzS(idx).Visible = True
    
    For idx = 1 To 2
        Load SubPanelM(idx)
        SubPanelM(idx).Visible = True
        Load SubSplitVertM(idx)
        SubSplitVertM(idx).Enabled = ShowVertSplit
        Load PicVSplitM(idx)
        Set PicVSplitM(idx).Container = SubSplitVertM(idx)
        PicVSplitM(idx).Visible = True
        SubSplitVertM(idx).Visible = True
        
        Load SubSplitVertS(idx)
        SubSplitVertS(idx).Enabled = ShowVertSplit
        Load PicVSplitS(idx)
        Set PicVSplitS(idx).Container = SubSplitVertS(idx)
        PicVSplitS(idx).Visible = True
        SubSplitVertS(idx).Visible = True
        
        Load SubPanelS(idx)
        SubPanelS(idx).Visible = True
        Load SubFrameS(idx)
        SubFrameS(idx).Visible = True
    
        Load MainSplitHorzM(idx)
        MainSplitHorzM(idx).Enabled = ShowVertSplit
        'Load PicVSplitS(idx)
        'Set PicVSplitS(idx).Container = SubSplitVertS(idx)
        'PicVSplitS(idx).Visible = True
        MainSplitHorzM(idx).Visible = True
    
        Load MainSplitHorzS(idx)
        MainSplitHorzS(idx).Enabled = ShowVertSplit
        'Load PicVSplitS(idx)
        'Set PicVSplitS(idx).Container = SubSplitVertS(idx)
        'PicVSplitS(idx).Visible = True
        MainSplitHorzS(idx).Visible = True
    Next
    
    idx = 0
    NewTop = SubTop
    NewWidth = fraOrigAddr.Width
    SubPanelM(idx).Width = NewWidth
    SplCnt = 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 0, idx, fraOrigAddr, NewTop, -1
    SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    SetSubPanelFrame 0, idx, fraAddress, NewTop, HSpl - 1
    
    idx = 1
    SplCnt = 0
    NewTop = SubTop
    NewWidth = fraFlightPanel(0).Width
    SubPanelM(idx).Width = NewWidth
    SplCnt = 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 0, idx, fraFlightPanel(0), NewTop, -1
    SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    SetSubPanelFrame 0, idx, fraDestAddr(0), NewTop, HSpl - 1
    SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    SetSubPanelFrame 0, idx, fraTelexText(0), NewTop, HSpl - 1
    SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    SetSubPanelFrame 0, idx, fraTelexText(1), NewTop, HSpl - 1
    
    idx = 2
    NewTop = SubTop
    NewWidth = fraFlightData.Width
    SubPanelM(idx).Width = NewWidth
    SplCnt = 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 0, idx, fraTlxType, NewTop, -1
    SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    SetSubPanelFrame 0, idx, fraFlightData, NewTop, HSpl - 1
    
    HSpl = 0
    SubTop = 0
    NewTop = 0
    
    idx = 0
    SplCnt = 1
    NewTop = SubTop
    NewWidth = 1200
    SubPanelS(idx).Width = NewWidth
    SetHorizSplitter 1, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 1, idx, SubFrameS(idx), NewTop, -1
    
    idx = 1
    SplCnt = 1
    NewTop = SubTop
    NewWidth = 1200
    SubPanelS(idx).Width = NewWidth
    SetHorizSplitter 1, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 1, idx, SubFrameS(idx), NewTop, -1
    
    idx = 2
    SplCnt = 1
    NewTop = SubTop
    NewWidth = 1200
    SubPanelS(idx).Width = NewWidth
    SetHorizSplitter 1, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 1, idx, SubFrameS(idx), NewTop, -1
    
End Sub
Private Sub SetSubPanelFrame(AsType As Integer, SubIdx As Integer, CurFrame As Frame, NewTop As Long, SplIdx As Integer)
    If AsType = 0 Then
        Set CurFrame.Container = SubPanelM(SubIdx)
        CurFrame.Left = 0
        CurFrame.Top = NewTop
        CurFrame.Width = SubPanelM(SubIdx).Width
        CurFrame.Tag = CStr(SplIdx)
        NewTop = NewTop + CurFrame.Height
    End If
    If AsType = 1 Then
        Set CurFrame.Container = SubPanelS(SubIdx)
        CurFrame.Left = 0
        CurFrame.Top = NewTop
        CurFrame.Width = SubPanelS(SubIdx).Width
        CurFrame.Tag = CStr(SplIdx)
        NewTop = NewTop + CurFrame.Height
    End If
End Sub
Private Sub SetHorizSplitter(AsType As Integer, SubIdx As Integer, SplIdx As Integer, NewTop As Long, ShowSplit As Boolean, CurCnt As Integer)
    If AsType = 0 Then
        If SplIdx > SubSplitHorzM.UBound Then Load SubSplitHorzM(SplIdx)
        Set SubSplitHorzM(SplIdx).Container = SubPanelM(SubIdx)
        SubSplitHorzM(SplIdx).Left = 0
        SubSplitHorzM(SplIdx).Top = NewTop
        SubSplitHorzM(SplIdx).Width = SubPanelM(SubIdx).Width
        'If CurCnt = 0 Then
            'SubSplitHorzM(SplIdx).Height = 90
            'SubSplitHorzM(SplIdx).BackColor = myownbuttondown
        'Else
            'SubSplitHorzM(SplIdx).BackColor = vbRed
            'SubSplitHorzM(SplIdx).Height = 90
        'End If
        NewTop = NewTop + SubSplitHorzM(SplIdx).Height
        SubSplitHorzM(SplIdx).Visible = True
        SubSplitHorzM(SplIdx).Enabled = ShowSplit
        SplIdx = SplIdx + 1
        CurCnt = CurCnt + 1
    End If
    If AsType = 1 Then
        If SplIdx > SubSplitHorzS.UBound Then Load SubSplitHorzS(SplIdx)
        Set SubSplitHorzS(SplIdx).Container = SubPanelS(SubIdx)
        SubSplitHorzS(SplIdx).Left = 0
        SubSplitHorzS(SplIdx).Top = NewTop
        SubSplitHorzS(SplIdx).Width = SubPanelS(SubIdx).Width
        NewTop = NewTop + SubSplitHorzS(SplIdx).Height
        SubSplitHorzS(SplIdx).Visible = True
        SubSplitHorzS(SplIdx).Enabled = ShowSplit
        SplIdx = SplIdx + 1
    End If
End Sub
Public Sub InitTlxAddresses(ForWhat As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpAlc As String
    Dim tmpApc As String
    Dim tmpTyp As String
    Dim KeyValue As String
    Screen.MousePointer = 11
    If (ForWhat = "MADTAB") Or (ForWhat = "INIT") Then
        'UfisServer.BasicData(2).Tag = "3"
        UfisServer.BasicDataInit 2, 1, "MADTAB", "CDAT,ALC2,ALC3,APC3,APC4,ADDR,TYPE,STAT,URNO", ""
        UfisServer.BasicData(2).ColumnWidthString = "8,2,3,3,4,7,1,1,10"
        MaxLine = UfisServer.BasicData(2).GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpAlc = UfisServer.BasicData(2).GetColumnValue(CurLine, 2)
            tmpAlc = Left(tmpAlc & "   ", 3)
            tmpApc = UfisServer.BasicData(2).GetColumnValue(CurLine, 3)
            tmpApc = Left(tmpApc & "   ", 3)
            tmpTyp = UfisServer.BasicData(2).GetColumnValue(CurLine, 6)
            tmpTyp = Left(tmpTyp & "   ", 1)
            KeyValue = tmpAlc & tmpApc & tmpTyp
            UfisServer.BasicData(2).SetColumnValue CurLine, 0, KeyValue
        Next
        UfisServer.BasicData(2).IndexCreate "ALCAPC", 0
        UfisServer.BasicData(2).AutoSizeColumns
    End If

    If (ForWhat = "MTPTAB") Or (ForWhat = "INIT") Then
        UfisServer.BasicDataInit 3, 1, "MTPTAB", "CDAT,ALC2,ALC3,TYPE,ADID,AUTO,MASI,MAPX,FSND,TSND,FLDD,TMED,TMPL,RFLD,DELA,ALLS,HMST,HMTE,HMEM,STAT,URNO", ""
        UfisServer.BasicData(3).ColumnWidthString = Replace("8   ,2   ,3   ,3   ,1   ,1   ,1   ,1   ,10  ,10  ,8   ,8   ,10  ,10  ,1   ,1   ,1   ,10  ,10  ,1   ,10", " ", "", 1, -1, vbBinaryCompare)
        MaxLine = UfisServer.BasicData(3).GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpAlc = UfisServer.BasicData(3).GetColumnValue(CurLine, 2)
            tmpAlc = Left(tmpAlc & "   ", 3)
            tmpApc = UfisServer.BasicData(3).GetColumnValue(CurLine, 3)
            tmpApc = Left(tmpApc & "   ", 3)
            tmpTyp = UfisServer.BasicData(3).GetColumnValue(CurLine, 4)
            tmpTyp = Left(tmpTyp & "   ", 1)
            KeyValue = tmpAlc & tmpApc & tmpTyp
            UfisServer.BasicData(3).SetColumnValue CurLine, 0, KeyValue
        Next
        UfisServer.BasicData(3).IndexCreate "ALCTPL", 0
        UfisServer.BasicData(3).IndexCreate "URNO", 20
        UfisServer.BasicData(3).AutoSizeColumns
        'UfisServer.Show
    End If
    Screen.MousePointer = 0


End Sub

Private Sub InitOrigList()
    Dim OrigList As String
    Dim CurAddr As String
    Dim CurBeme As String
    Dim ItemNo As Long
    Dim LineNo As Long
    Dim LineList As String
    Dim NewLine As String
    OrigList = AutoSendTelexAddress
    ItemNo = 0
    CurAddr = GetRealItem(OrigList, ItemNo, ",")
    While CurAddr <> ""
        CurBeme = ""
        LineList = UfisServer.BasicData(2).GetLinesByColumnValue(3, CurAddr, 0)
        If LineList <> "" Then
            LineNo = Val(LineList)
            CurBeme = UfisServer.BasicData(2).GetColumnValue(LineNo, 5)
        End If
        NewLine = CurAddr & "," & CurBeme
        TlxOrigTab.InsertTextLine NewLine, False
        ItemNo = ItemNo + 1
        CurAddr = GetRealItem(OrigList, ItemNo, ",")
    Wend
    TlxOrigTab.AutoSizeColumns
    TlxOrigTab.Refresh
    TlxOrigTab.SetCurrentSelection 0
End Sub
Private Sub InitTlxTypeList()
    Dim i As Integer
    Dim NewLine As String
    Dim CurList As String
    Dim CurType As String
    Dim CurAddi As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim itm As Integer
    NewLine = "MVT,AD,Actual Departure"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "COR,AD,Actual Departure"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "MVT,AA,Actual Arrival"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "COR,AA,Actual Arrival"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "MVT,ED,Estim. Departure"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "COR,ED,Estim. Departure"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "MVT,EA,Estim. Arrival"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "COR,EA,Estim. Arrival"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "MVT,AO,Actual Offblock"
    TlxTypeTab.InsertTextLine NewLine, False
    NewLine = "COR,AO,Actual Offblock"
    TlxTypeTab.InsertTextLine NewLine, False
    TlxTypeTab.AutoSizeColumns
    MaxLine = TlxTypeTab.GetLineCount - 1
    TlxTypeTab.Refresh
    TlxTypeTab.SetCurrentSelection -1
End Sub
Private Sub InitPrioList()
    TlxPrioTab.InsertTextLine "QU,Urgent", False
    TlxPrioTab.InsertTextLine "QD,Deferred", False
    TlxPrioTab.InsertTextLine "QX,Express", False
    TlxPrioTab.InsertTextLine "QK,", False
    TlxPrioTab.InsertTextLine "QL,", False
    TlxPrioTab.InsertTextLine "QS,Emergency", False
    TlxPrioTab.AutoSizeColumns
    TlxPrioTab.Refresh
    TlxPrioTab.SetCurrentSelection 0
End Sub
Private Sub InitDblSignList(CodeList As String)
    Dim CurItm As Integer
    Dim tmpCode As String
    Dim tmpLine As String
    DblSignTab.ResetContent
    DblSignTab.SetUniqueFields "0"
    DblSignTab.InsertTextLine "--,None", False
    'DblSignTab.InsertTextLine "11,None", False
    'DblSignTab.InsertTextLine "22,None", False
    'DblSignTab.InsertTextLine "33,None", False
    'DblSignTab.InsertTextLine "44,None", False
    'DblSignTab.InsertTextLine "55,None", False
    'DblSignTab.InsertTextLine "66,None", False
    'DblSignTab.InsertTextLine "77,None", False
    'DblSignTab.InsertTextLine "88,None", False
    CurItm = 1
    tmpCode = GetItem(CodeList, CurItm, ",")
    While tmpCode <> ""
        tmpLine = tmpCode & ","
        DblSignTab.InsertTextLine tmpLine, False
        CurItm = CurItm + 1
        tmpCode = GetItem(CodeList, CurItm, ",")
    Wend
    DblSignTab.AutoSizeColumns
    DblSignTab.Refresh
    If CodeList <> "" Then
        DblSignTab.SetCurrentSelection 1
    Else
        DblSignTab.SetCurrentSelection 0
    End If
End Sub
Private Sub InitTplListTab(AftAdid As String)
    Dim i As Integer
    Dim TplUrnoList As String
    Dim HitList As String
    Dim AftFlno As String
    Dim AftAlc3 As String
    Dim NewLine As String
    Dim CurType As String
    Dim CurUrno As String
    Dim CurText As String
    Dim LineNo As Long
    Dim itm As Integer
    TplListTab.ResetContent
    AftFlno = GetFieldValue("FLNO", MyAftTabRecord, MyAftTabFields)
    AftAlc3 = Trim(Left(AftFlno, 3))
    TplUrnoList = GetTemplateUrnos(AftAdid, AftAlc3)
    If TplUrnoList <> "" Then
        itm = 0
        CurUrno = "START"
        While CurUrno <> ""
            itm = itm + 1
            CurUrno = GetItem(TplUrnoList, itm, ",")
            If CurUrno <> "" Then
                CurText = ""
                HitList = UfisServer.BasicData(3).GetLinesByIndexValue("URNO", CurUrno, 0)
                If HitList <> "" Then
                    LineNo = Val(HitList)
                    CurType = UfisServer.BasicData(3).GetFieldValue(LineNo, "TYPE")
                    HitList = TlxTypeTab.GetLinesByColumnValue(1, CurType, 0)
                    If HitList <> "" Then
                        LineNo = Val(HitList)
                        CurText = TlxTypeTab.GetColumnValue(LineNo, 2)
                    End If
                    NewLine = ""
                    NewLine = NewLine & CurType & ","
                    NewLine = NewLine & CurText & ","
                    NewLine = NewLine & CurUrno & ""
                    TplListTab.InsertTextLine NewLine, False
                End If
            End If
        Wend
        CurType = CurTlxTplType
        If CurType <> "" Then
            HitList = TplListTab.GetLinesByColumnValue(0, CurType, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                TplListTab.SetCurrentSelection LineNo
            End If
        End If
    End If
    TplListTab.AutoSizeColumns
    TplListTab.Refresh
    TplListTab.Visible = True
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = CheckCancelExit(Me, 1)
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim BordHeight As Long
    Dim NewMainTop As Long
    Dim NewMainLeft As Long
    Dim NewMainWidth As Long
    Dim NewMainHeight As Long
    Dim NewSubTop As Long
    Dim NewSubLeft As Long
    Dim NewSubHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim NewValue As Long
    Dim OldHeight As Long
    Dim OldWidth As Long
    Dim i As Integer
    OldHeight = MainPanel(0).Height
    OldWidth = MainPanel(0).Width
    If Not GeneratorIsReadyForUse Then
        NewMainTop = 0
        NewMainLeft = 0
        NewMainWidth = Me.ScaleWidth
        NewMainHeight = Me.ScaleHeight
        If NewMainHeight < 60 Then NewMainHeight = 60
        MainPanel(1).Top = NewMainTop
        MainPanel(1).Left = NewMainLeft
        MainPanel(1).Width = NewMainWidth
        MainPanel(1).Height = NewMainHeight
    End If
    NewMainTop = MainPanel(0).Top
    NewMainLeft = 0
    NewMainWidth = Me.ScaleWidth
    NewMainHeight = Me.ScaleHeight
    NewMainHeight = NewMainHeight - NewMainTop
    NewMainHeight = NewMainHeight - StatusBar1.Height
    If NewMainHeight < 60 Then NewMainHeight = 60
    'If OldHeight < NewMainHeight Then MainPanel(0).Height = NewMainHeight
    'If OldHeight > NewMainHeight Then MainPanel(0).Height = NewMainHeight
    'If OldWidth < NewMainWidth Then MainPanel(0).Width = NewMainWidth
    BordHeight = MainSplitHorzM(0).Height + MainSplitHorzS(0).Height
    NewSubHeight = NewMainHeight - BordHeight - 60
    If NewSubHeight >= 60 Then
        For i = 0 To SubPanelM.UBound
            SubPanelM(i).Height = NewSubHeight
            SubSplitVertM(i).Height = NewMainHeight
            SubPanelS(i).Height = NewMainHeight
            SubSplitVertS(i).Height = NewMainHeight
            MainSplitHorzS(i).Top = NewMainHeight - MainSplitHorzS(i).Height - 60
        Next
        ResizeSubPanels -1
    End If
    RefreshControls -1
    MainPanel(0).Width = NewMainWidth
    MainPanel(0).Height = NewMainHeight
    MoveSubPanels -1
    ResizeTopArea 0
    If Not GeneratorIsReadyForUse Then
        MainPanel(0).Refresh
    End If
    MainPanel(0).Refresh
    Me.Refresh
    If FixWin.Value = 1 Then InitPosSize True
    
i = 1
If i = 0 Then
    If fraFlightData.Visible Then
        NewValue = Me.ScaleHeight - fraFlightData.Top - StatusBar1.Height '- 60
        If NewValue > 1750 Then
            fraFlightData.Height = NewValue
            NewValue = NewValue - FltInputPanel.Top - 90
            If NewValue > 600 Then
                FltInputPanel.Height = NewValue
                FltListPanel(0).Height = NewValue
                FltDlgCover(1).Height = NewValue
                NewValue = FltListPanel(0).ScaleHeight - FltListPanel(2).Top + 30
                If NewValue > 600 Then
                    FltListPanel(2).Height = NewValue
                    'TabFlights(0).Height = NewValue - 90
                    'TabFlights(1).Height = NewValue - 90
                    TabFlights(0).Height = FltListPanel(2).ScaleHeight
                    TabFlights(1).Height = FltListPanel(2).ScaleHeight
                End If
                DrawBackGround FltInputPanel, MyLifeStyleValue, False, True
                DrawBackGround FltInputSlide, MyLifeStyleValue, False, True
                VScroll1(0).Height = FltInputPanel.Height - 30
                NewValue = FltInputPanel.ScaleHeight - FltInputSlide.Height
                If (NewValue < 0) And (FltInputSlide.Top < -300) Then
                    FltInputSlide.Top = NewValue
                    VScroll1(0).Value = -(NewValue / 15)
                End If
                If VScroll1(0).Height > FltInputSlide.Height Then
                    VScroll1(0).Value = 0
                    VScroll1(0).Enabled = False
                Else
                    VScroll1(0).Enabled = True
                End If
            End If
        End If
        NewValue = Me.ScaleWidth - fraFlightData.Width - 45
        If NewValue >= fraDestAddr(0).Left Then fraFlightData.Left = NewValue
    End If
    NewValue = Me.ScaleWidth - fraDestAddr(0).Left - 45
    If NewValue > 1500 Then
        fraPreview.Width = NewValue
        txtPreview(0).Width = NewValue - (txtPreview(0).Left * 2) + 15
        txtPreview(OutTlxIdx).Width = txtPreview(0).Width
        fraPreview.ZOrder
    End If
    If fraFlightData.Visible Then NewValue = fraFlightData.Left - fraDestAddr(0).Left - 60
    If NewValue > 1500 Then
        fraFlightCover(0).Width = NewValue
        NewValue = fraDestAddr(0).Width - chkEdit2.Width - 90
        chkEdit2.Left = NewValue
    End If
    NewValue = Me.ScaleHeight - fraPreview.Top - StatusBar1.Height '- 60
    If NewValue > 1750 Then
        fraPreview.Height = NewValue
        txtPreview(0).Height = NewValue - txtPreview(0).Top - 90
        txtPreview(OutTlxIdx).Height = txtPreview(0).Height
    End If
    NewValue = Me.ScaleHeight - fraOrigAddr.Top - StatusBar1.Height '- 60
    If NewValue > 1200 Then
        If fraTlxPrio.Tag <> "" Then
            fraTlxPrio.Height = NewValue
            TlxPrioTab.Height = NewValue - TlxPrioTab.Top - 90
        End If
        If fraOrigAddr.Tag <> "" Then
            fraOrigAddr.Height = NewValue
            TlxOrigTab.Height = NewValue - TlxOrigTab.Top - 90
        End If
        If fraTlxType.Tag <> "" Then
            fraTlxType.Height = NewValue
            TlxTypeTab.Height = NewValue - TlxTypeTab.Top - 90
        End If
        If fraDblSign.Tag <> "" Then
            fraDblSign.Height = NewValue
            DblSignTab.Height = NewValue - DblSignTab.Top - 90
        End If
    End If
    NewValue = Me.ScaleHeight - StatusBar1.Height - fraTelexText(1).Height
    'If NewValue > 2100 Then
    fraTelexText(1).Top = NewValue
    NewValue = fraTelexText(1).Top - fraTelexText(0).Top - 30
    If NewValue > 900 Then
        fraTelexText(0).Height = NewValue
        NewValue = NewValue - txtSiText(0).Height - 105
        SiTxtLabel(0).Top = NewValue - SiTxtLabel(0).Height - 30
        txtSiText(0).Top = NewValue
        NewValue = NewValue - txtTelexText(0).Top - 270
        If NewValue > 1500 Then
            txtTelexText(0).Height = NewValue
        End If
    End If
    NewValue = Me.ScaleHeight - fraAddress.Top - StatusBar1.Height '- 60
    If NewValue > 3000 Then
        fraAddress.Height = NewValue
        AddressTab(0).Height = NewValue - AddressTab(0).Top - 90
        AddressTab(1).Height = NewValue - AddressTab(1).Top - 90
    End If
    ArrangeTopPanels
End If

End Sub
Private Sub ResizeTopArea(ForWhat As Integer)
    Dim NewLeft As Long
    Dim NewValue As Long
    If SubPanelM.UBound >= 2 Then
        fraTopButtons(2).Left = Me.ScaleWidth - fraTopButtons(2).Width - 15
        NewLeft = SubSplitVertS(1).Left + SubSplitVertS(1).Width - (fraTopButtons(1).Width) + 30
        fraTopButtons(1).Left = NewLeft
        fraTopButtons(0).Width = Me.ScaleWidth + 60
        NewLeft = SubSplitVertM(2).Left + 30
        fraTopButtons(3).Left = NewLeft
        fraTopButtons(3).Width = FullPanelWidth(2)
    End If
End Sub
Private Sub RefreshControls(ForWhat As Integer)
    Dim i As Integer
    If (ForWhat = 1) Or (ForWhat < 0) Then
        If chkPreView.Value = 0 Then
            For i = 0 To txtTelexText.UBound
                txtTelexText(i).Refresh
            Next
            'For i = 0 To txtDestAddr.UBound
                'txtDestAddr(i).Refresh
            'Next
        Else
            For i = 0 To txtPreview.UBound
                txtPreview(i).Refresh
            Next
        End If
    End If
    If (ForWhat = 2) Or (ForWhat < 0) Then
        VScroll1(0).Refresh
    End If
End Sub

Private Sub fraSetup_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    fraSetup(Index).ZOrder
End Sub

Private Sub lblDatType_Click(Index As Integer)
    If chkFltTick(Index).Value = 1 Then
        chkFltTick(Index).Value = 0
    Else
        chkFltTick(Index).Value = 1
    End If
End Sub

Private Sub lblFltField_Click(Index As Integer)
    If chkFltTick(Index).Value = 1 Then
        chkFltTick(Index).Value = 0
    Else
        chkFltTick(Index).Value = 1
    End If
End Sub

Private Sub MyTestCase_Change(Index As Integer)
    If Index = 0 Then
        If MyTestCase(Index).Text <> "" Then
            'If Left(MyTestCase(Index).Text, 4) <> "CASE" Then MyTestCase(Index).Text = "CASE " & MyTestCase(Index).Text
        End If
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = MyOwnButtonDown Else OnTop.BackColor = MyOwnButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False, False
    SetAllFormsOnTop True
End Sub

Private Sub OpenPool_Click()
    On Error Resume Next
    If OpenPool.Value = 1 Then
        SwitchMainWindows "MAIN"
        OpenPool.Value = 0
    End If
End Sub

Private Sub optResize_Click(Index As Integer)
    If optResize(Index).Value = True Then
        If (fraSetupX(3).Tag = "5") Or (Index = 5) Then
            MainPanel(1).Visible = False
            fraSetupX(3).Tag = "0"
            Form_Resize
            MoveSubPanels -1
            fraSetupX(3).Tag = optResize(Index).Tag
            MoveSubPanels -1
        Else
            fraSetupX(3).Tag = optResize(Index).Tag
            MoveSubPanels -1
        End If
    End If
End Sub

Private Sub PanelIcon_Click(Index As Integer)
    Dim tmpTag As String
    If Index = 6 Then
        tmpTag = PanelIcon(Index).Tag
        Select Case tmpTag
            Case "EDIT"
                chkText.Value = 1
                chkTlxTxt.Value = 1
            Case "OPEN"
                'Cannot be ...
                chkTlxTxt.Value = 0
            Case Else
                PanelIcon(Index).Tag = "SHOW"
        End Select
    Else
        PanelIcon(Index).Tag = "SHOW"
    End If
End Sub

Private Sub PanelSizeSlider_Change(Index As Integer)
    AdjustPanelSize Index
End Sub

Private Sub PanelSizeSlider_Click(Index As Integer)
    AdjustPanelSize Index
End Sub
Private Sub AdjustPanelSize(Index As Integer)
    Dim NewSize As Long
    Dim SetWidth As Long
    Dim idx As Integer
    SetWidth = PanelSizeSlider(Index).Value * 15
    Select Case Index
        Case 0
            NewSize = 180
            For idx = 0 To SubSplitVertM.UBound
                SubSplitVertM(idx).Width = SetWidth
                SubSplitVertS(idx).Width = SetWidth
                If chkBorder(0).Value = 1 Then
                    PicVSplitS(idx).Left = SetWidth - PicVSplitS(idx).Width
                End If
                NewSize = NewSize + SubSplitVertM(idx).Width
                NewSize = NewSize + SubPanelM(idx).Width
                NewSize = NewSize + SubSplitVertS(idx).Width
            Next
            Me.Width = NewSize
        Case Else
    End Select
    
End Sub

Private Sub PanelSizeSlider_Scroll(Index As Integer)
    AdjustPanelSize Index
End Sub

Private Sub SubPanelS_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    MouseClock.MouseMoves Index, Button, Shift, x, y
End Sub

Private Sub TabFlights_GotFocus(Index As Integer)
    Dim i As Integer
    i = 1
End Sub

Private Sub TabFlights_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    On Error Resume Next
    If LineNo >= 0 Then
        If Selected Then
            PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("blackarrow")).Picture
            PanelIcon(6).Tag = "SHOW"
            txtTelexText(0).BackColor = vbWhite
            txtTelexText(0).Locked = True
            CurTabOcxFldLst = TabFlights(Index).LogicalFieldList
            'TlxFlights(Index).SetCurrentSelection -1
            TabFlights(Index).Refresh
            Set CurFltTabOcx = TabFlights(Index)
            CurFltLineNo = LineNo
            CurFltRecIsAft = True
            CurFltRecIsTlx = False
            Select Case Index
                Case 0
                    SwitchMainViewPoint 3, "AFT_ARR"
                Case 1
                    SwitchMainViewPoint 3, "AFT_DEP"
                Case Else
            End Select
            TabFlights(Index).SetFocus
        End If
    End If
End Sub

Private Sub TabFlights_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim tmpCode As String
    tmpCode = "AUTO.TABFLT." & CStr(Index)
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = TabFlights(Index)
    SetMouseWheelObjectCode tmpCode
End Sub

Private Sub TlxFlights_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    On Error Resume Next
    If Not NewMsgFromBcHdl Then
        If Not SystemInput Then
            If LineNo >= 0 Then
                If Selected Then
                    PanelIcon(6).Picture = MyIcons.ListImages(GetIconIndex("blackarrow")).Picture
                    PanelIcon(6).Tag = "SHOW"
                    txtTelexText(0).BackColor = vbWhite
                    txtTelexText(0).Locked = True
                    CurTabOcxFldLst = TlxFlights(Index).LogicalFieldList
                    'TabFlights(Index).SetCurrentSelection -1
                    TlxFlights(Index).Refresh
                    Set CurFltTabOcx = TlxFlights(Index)
                    CurFltLineNo = LineNo
                    CurFltRecIsAft = False
                    CurFltRecIsTlx = True
                    Select Case Index
                        Case 0
                            SwitchMainViewPoint 4, "TLX_ARR"
                        Case 1
                            SwitchMainViewPoint 4, "TLX_DEP"
                        Case Else
                    End Select
                    TlxFlights(Index).SetFocus
                End If
            End If
        End If
    End If
End Sub

Private Sub TlxFlights_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim tmpCode As String
    tmpCode = "AUTO.TLXFLT." & CStr(Index)
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = TlxFlights(Index)
    SetMouseWheelObjectCode tmpCode
End Sub

Private Sub TlxOrigTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    TelexOrigIsClean = True
    If LineNo >= 0 Then
        ActiveSendAddress = TlxOrigTab.GetColumnValue(LineNo, 0)
    End If
    If InStr("-------,OOOOOPS,ZZZZZZZ", ActiveSendAddress) > 0 Then TelexOrigIsClean = False
End Sub

Private Sub TlxTypeTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim TlxText As String
    Dim TlxSTyp As String
    If (Selected) And (LineNo >= 0) Then
        TlxSTyp = TlxTypeTab.GetColumnValue(LineNo, 1)
        If TlxSTyp = CurTlxTplType Then
            TlxText = GetTlxTextFromTemplate(CurTlxTplText, TelexTextIsClean)
            CheckSendStatus
            If Not TplTextIsUser Then TlxText = CurNewTplText
            txtTelexText(0).Text = TlxText
        Else
            SetTelexDefault TlxTypeTab, 1, CurTlxTplType, CurTlxTplCorr
        End If
    End If
End Sub

Private Sub TlxVpfr_Change(Index As Integer)
    If Not AutoSendInitLoop Then
        Select Case Index
            Case 0
                If FormIsLoaded("RcvTelex") Then
                    RcvTelex.chkLoad.BackColor = LightYellow
                End If
            Case 1
                If FormIsLoaded("SndTelex") Then
                    SndTelex.chkLoad.BackColor = LightYellow
                End If
            Case 2
                If FormIsLoaded("PndTelex") Then
                    PndTelex.chkLoad.BackColor = LightYellow
                End If
            Case 3
                If FormIsLoaded("OutTelex") Then
                    OutTelex.chkLoad.BackColor = LightYellow
                End If
            Case Else
        End Select
    End If
End Sub

Private Sub TlxVpto_Change(Index As Integer)
    If Not AutoSendInitLoop Then
        Select Case Index
            Case 0
                If FormIsLoaded("RcvTelex") Then
                    RcvTelex.chkLoad.BackColor = LightYellow
                End If
            Case 1
                If FormIsLoaded("SndTelex") Then
                    SndTelex.chkLoad.BackColor = LightYellow
                End If
            Case 2
                If FormIsLoaded("PndTelex") Then
                    PndTelex.chkLoad.BackColor = LightYellow
                End If
            Case 3
                If FormIsLoaded("OutTelex") Then
                    OutTelex.chkLoad.BackColor = LightYellow
                End If
            Case Else
        End Select
    End If
End Sub

Private Sub TplListTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim TplUrno As String
    Dim TlxText As String
    If Selected Then
        If LineNo >= 0 Then
            TplUrno = TplListTab.GetFieldValue(LineNo, "TPLU")
            If TplUrno <> "" Then
                If TplUrno <> CurTlxTplUrno Then
                    SystemInput = True
                    GetAutoTemplate TplUrno
                    CreateFlightDataPanel "", MyAftTabFields, MyAftTabRecord, MyTlxTemplate
                    chkEdit.Value = 1
                    TlxText = GetTlxTextFromTemplate(CurTlxTplText, TelexTextIsClean)
                    CheckSendStatus
                    If Not TplTextIsUser Then TlxText = CurNewTplText
                    'SetTextBoxSelected txtTelexText(0)
                    'txtTelexText(0).SelText = TlxText
                    txtTelexText(0).Text = TlxText
                    RefreshControls 1
                    'txtTelexText(0).Refresh
                    chkFldAddr(0).Value = 1
                    SystemInput = False
                End If
            End If
        End If
    End If
End Sub

Private Sub txtDestAddr_Change(Index As Integer)
    Dim tmpText As String
    Dim tmpAddr As String
    Dim tmpData As String
    Dim curPos As Long
    Dim curLen As Long
    Dim txtLen As Long
    Dim itm As Integer
    Dim setRed As Boolean
    If Index = 0 Then
        TelexAddrIsWrong = False
        tmpText = txtDestAddr(Index).Text
        'curPos = txtDestAddr(Index).SelStart
        'curLen = txtDestAddr(Index).SelLength
        tmpText = Trim(tmpText)
        tmpText = Replace(tmpText, vbCr, " ", 1, -1, vbBinaryCompare)
        tmpText = Replace(tmpText, vbLf, " ", 1, -1, vbBinaryCompare)
        tmpData = tmpText
        tmpText = Replace(tmpText, "  ", " ", 1, -1, vbBinaryCompare)
        While tmpText <> tmpData
            tmpData = tmpText
            tmpText = Replace(tmpText, "  ", " ", 1, -1, vbBinaryCompare)
        Wend
        setRed = False
        tmpAddr = "START"
        While tmpAddr <> ""
            tmpAddr = GetItem(tmpText, 1, " ")
            txtLen = Len(tmpAddr)
            If tmpAddr <> "" Then
                If (txtLen <> 7) Or (setRed) Then
                    txtDestAddr(Index).ForeColor = vbRed
                    setRed = True
                Else
                    txtDestAddr(Index).ForeColor = vbBlack
                End If
                itm = InStr(tmpText, " ")
                If itm > 0 Then
                    tmpText = Mid(tmpText, itm)
                    tmpText = LTrim(tmpText)
                Else
                    tmpText = ""
                End If
            End If
        Wend
        If setRed Then TelexAddrIsWrong = True
        CheckSendStatus
    End If
End Sub

Private Sub txtEditDL_Change(Index As Integer)
    Dim tmpTxtText As String
    Dim tmpTagText As String
    Dim tmpTag As String
    Dim tmpTagItem As String
    Dim idx As Integer
    Dim itm As Integer
    chkEditDL(Index).Caption = txtEditDL(Index).Text
    tmpTxtText = Trim(txtEditDL(Index).Text)
    tmpTagText = Trim(txtEditDL(Index).Tag)
    If Val(tmpTagText) = 0 Then tmpTagText = ""
    If Val(tmpTxtText) = 0 Then tmpTxtText = ""
    If tmpTagText = "" Then
        tmpTagText = tmpTxtText
        txtEditDL(Index).Tag = tmpTagText
    End If
    CheckAftSaveStatus "[DLAY]", tmpTagText, tmpTxtText
    If Index = 1 Then AftFldText(15).Text = tmpTxtText
    If Index = 2 Then AftFldText(16).Text = tmpTxtText
    If Index = 3 Then AftFldText(17).Text = tmpTxtText
    If Index = 4 Then AftFldText(18).Text = tmpTxtText
    If chkEditDL(Index).Tag <> "" Then
        tmpTag = chkEditDL(Index).Tag
        tmpTagItem = "START"
        itm = 0
        While tmpTagItem <> ""
            itm = itm + 1
            tmpTagItem = GetItem(tmpTag, itm, ",")
            If tmpTagItem <> "" Then
                idx = Val(tmpTagItem)
                txtFltInput(idx).Text = tmpTxtText
            End If
        Wend
    End If
End Sub
Private Sub CheckAftSaveStatus(TypeCode As String, OldValue As String, NewValue As String)
    Dim tmpTagSave As String
    tmpTagSave = chkFltData(1).Tag
    If OldValue <> NewValue Then
        If InStr(tmpTagSave, TypeCode) = 0 Then tmpTagSave = tmpTagSave & TypeCode
    Else
        tmpTagSave = Replace(tmpTagSave, TypeCode, "", 1, -1, vbBinaryCompare)
    End If
    chkFltData(1).Tag = tmpTagSave
    
    If tmpTagSave <> "" Then
        chkFltData(1).BackColor = vbRed
        chkFltData(1).ForeColor = vbWhite
    Else
        chkFltData(1).BackColor = MyOwnButtonFace
        chkFltData(1).ForeColor = vbBlack
    End If
End Sub
Private Sub txtEditDL_GotFocus(Index As Integer)
    On Error Resume Next
    If Not StopNestedCalls Then
        StopNestedCalls = True
            Select Case Index
                Case 0
                Case 1 To 2
                    chkEditDL(Index).Visible = False
                    txtEditDL(Index).SetFocus
                Case 3 To 4
                    chkEditDL(Index).Visible = False
                    txtEditDL(Index).SetFocus
                Case Else
            End Select
        SetTextSelected
        StopNestedCalls = False
    End If
End Sub

Private Sub txtEditDL_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim tmpChr As String
    Dim CmpChr As String
    Select Case KeyAscii
        Case 13
            txtEditDL_LostFocus Index
        Case Else
        Select Case Index
            Case 3, 4
                CmpChr = "0123456789" & Chr(8)
                tmpChr = Chr(KeyAscii)
                If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
            Case Else
        End Select
    End Select
End Sub

Private Sub txtEditDL_LostFocus(Index As Integer)
    Dim tmpText As String
    If Not StopNestedCalls Then
        Select Case Index
            Case 0
            Case 1 To 2
                tmpText = Trim(txtEditDL(Index).Text)
                tmpText = Left(tmpText, 2)
                txtEditDL(Index).Text = tmpText
                chkEditDL(Index).Caption = tmpText
                chkEditDL(Index).Value = 0
                chkEditDL(Index).Visible = True
                If Index = 1 Then
                    AftFldText(15).Text = tmpText
                Else
                    AftFldText(16).Text = tmpText
                End If
            Case 3 To 4
                tmpText = Trim(txtEditDL(Index).Text)
                tmpText = Right("0000" & tmpText, 4)
                If tmpText = "0000" Then tmpText = ""
                txtEditDL(Index).Text = tmpText
                chkEditDL(Index).Caption = tmpText
                chkEditDL(Index).Value = 0
                chkEditDL(Index).Visible = True
                If Index = 3 Then
                    AftFldText(17).Text = tmpText
                Else
                    AftFldText(18).Text = tmpText
                End If
            Case Else
        End Select
        CalculateDelay
    End If
End Sub

Private Sub txtEditEET_Change(Index As Integer)
    Dim tmpTxtText As String
    Dim tmpTagText As String
    Dim tmpTagSave As String
    Dim idx As Integer
    chkEditEET(Index).Caption = txtEditEET(Index).Text
    If Index = 2 Then
        tmpTxtText = Trim(txtEditEET(Index).Text)
        tmpTagText = Trim(txtEditEET(Index).Tag)
        If tmpTagText = "" Then
            tmpTagText = tmpTxtText
            txtEditEET(Index).Tag = tmpTagText
        End If
        If Val(tmpTxtText) = 0 Then tmpTxtText = ""
        If Val(tmpTagText) = 0 Then tmpTagText = ""
        CheckAftSaveStatus "[EETM]", tmpTagText, tmpTxtText
    End If
End Sub

Private Sub txtEditEET_GotFocus(Index As Integer)
    On Error Resume Next
    If Not StopNestedCalls Then
        StopNestedCalls = True
        Select Case Index
            Case 0
            Case 1 To 2
                chkEditEET(Index).Visible = False
                txtEditEET(Index).SetFocus
            Case 3 To 4
            Case Else
        End Select
        SetTextSelected
        StopNestedCalls = False
    End If
End Sub

Private Sub txtEditEET_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim tmpChr As String
    Dim CmpChr As String
    Select Case KeyAscii
        Case 13
            txtEditEET_LostFocus Index
        Case Else
        CmpChr = "0123456789" & Chr(8)
        tmpChr = Chr(KeyAscii)
        If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
    End Select
End Sub

Private Sub txtEditEET_LostFocus(Index As Integer)
    Dim tmpText As String
    Dim tmpMM As String
    Dim tmpHH As String
    Dim iTime As Integer
    If Not StopNestedCalls Then
        Select Case Index
            Case 0
            Case 1
                tmpText = Trim(txtEditEET(Index).Text)
                tmpText = Right("0000" & tmpText, 4)
                If tmpText = "0000" Then tmpText = ""
                txtEditEET(Index).Text = tmpText
                chkEditEET(Index).Caption = tmpText
                If tmpText <> "" Then
                    chkEditEET(Index).Value = 0
                    chkEditEET(Index).Visible = True
                    tmpHH = Left(tmpText, 2)
                    tmpMM = Right(tmpText, 2)
                    iTime = (Val(tmpHH) * 60) + Val(tmpMM)
                    tmpText = CStr(iTime)
                End If
                txtEditEET(2).Text = tmpText
                CalculateEans
            Case 2
                tmpText = Trim(txtEditEET(Index).Text)
                SetEETMinutes tmpText
                CalculateEans
            Case 3 To 4
            Case Else
        End Select
    End If
End Sub
Private Sub CalculateDelay()
    Dim AftAdid As String
    Dim AftOfbl As String
    Dim AftStod As String
    Dim BstOfbl As String
    Dim ConText As String
    Dim TtlText As String
    Dim TplDela As String
    Dim TplDur1 As String
    Dim TplDur2 As String
    Dim TtlDiff As Integer
    Dim TplDiff As Integer
    Dim TplDtd1 As Integer
    Dim TplDtd2 As Integer
    Dim TplTtld As Integer
    Dim DlyIsMand As Boolean
    Dim i As Integer
    StopNestedCalls = True
    AftAdid = GetFieldValue("ADID", MyAftTabRecord, MyAftTabFields)
    If AftAdid = "D" Then
        FlightIsOnTime = True
        FlightIsDelayed = False
        If CurTlxTplType = "" Then DlyIsMand = True
        If CurTlxTplType = "AD" Then DlyIsMand = True
        If CurTlxTplType = "ED" Then DlyIsMand = False
        For i = 15 To 18
            If AftFldText(i).BackColor = LightestBlue Then
                AftFldText(i).Text = ""
                AftFldText(i).BackColor = vbWhite
                AftFldText(i).ForeColor = vbBlack
            End If
            If AftFldText(i).Text = AftFldText(i).Tag Then
                AftFldText(i).BackColor = vbWhite
                AftFldText(i).ForeColor = vbBlack
            End If
        Next
        If TplRuleLineNo >= 0 Then TplDela = Trim(UfisServer.BasicData(3).GetFieldValue(TplRuleLineNo, "DELA"))
        TplDiff = Val(TplDela)
        
        'FOR TESTING
        If chkTestCase.Value = 1 Then TplDiff = Val(AftTestValue(0).Text)
        
        TplDela = CStr(TplDiff)
        If TplDiff = 0 Then TplDela = ""
        
        AftOfbl = Trim(GetFieldValue("OFBL", MyAftTabRecord, MyAftTabFields))
        AftStod = Trim(GetFieldValue("STOD", MyAftTabRecord, MyAftTabFields))
        BstOfbl = AftOfbl
        If BstOfbl = "" Then BstOfbl = Trim(GetFieldValue("ETDI", MyAftTabRecord, MyAftTabFields))
        If BstOfbl = "" Then BstOfbl = Trim(GetFieldValue("STOD", MyAftTabRecord, MyAftTabFields))
        TtlDiff = CInt(CedaTimeDiff(AftStod, BstOfbl))
        
        'FOR TESTING
        If chkTestCase.Value = 1 Then TtlDiff = Val(AftTestValue(1).Text)
        
        MyTestCase(0).Text = "0.0.0"
        For i = 1 To MyTestCase.UBound
            MyTestCase(i).Text = ""
        Next
        MyTestCase(3).Text = "NOT TESTED"
        
        TplDtd1 = GetMinutesFromHHMM(AftFldText(17).Text)
        TplDtd2 = GetMinutesFromHHMM(AftFldText(18).Text)
        'We consider positive values only
        TplDtd1 = Abs(TplDtd1)
        TplDtd2 = Abs(TplDtd2)
        TplTtld = TplDtd1 + TplDtd2
        If TtlDiff > 0 Then
            If (AftFldText(15).Tag = "--") And (AftFldText(15).Text = "--") Then AftFldText(15).Text = "??"
            If AftFldText(16).Text = "??" Then
                AftFldText(16).BackColor = LightYellow
                txtEditDL(2).BackColor = LightYellow
                chkEditDL(2).Value = 1
                chkEditDL(2).Visible = False
            End If
            If AftFldText(15).Text = "??" Then
                AftFldText(15).BackColor = LightYellow
                txtEditDL(1).BackColor = LightYellow
                chkEditDL(1).Value = 1
                chkEditDL(1).Visible = False
            End If
            If TtlDiff > TplDiff Then
                FlightIsOnTime = False
                FlightIsDelayed = True
                If TplDiff = 0 Then
                    ConText = "DELAYED"
                Else
                    ConText = ">> GRACE"
                End If
            Else
                ConText = "GRACED"
                FlightIsOnTime = True
                FlightIsDelayed = False
            End If
        ElseIf TtlDiff = 0 Then
            ConText = "ON TIME"
            FlightIsOnTime = True
            FlightIsDelayed = False
            If (AftFldText(15).Tag = "--") And (AftFldText(15).Text = "??") Then
                AftFldText(15).Text = ""
            End If
            If (AftFldText(15).Tag = "--") And (AftFldText(15).Text = "--") Then
                AftFldText(15).Text = ""
            End If
        Else
            ConText = "EARLY"
            FlightIsOnTime = True
            FlightIsDelayed = False
        End If
        TtlText = GetHHMMFromMinutes(TtlDiff)
        If TtlText = "0000" Then TtlText = ""
        If (AftFldText(15).Text <> "") And (AftFldText(16).Text <> "") Then
            MyTestCase(0).Text = "1.0.0.0"
            MyTestCase(1).Text = "GOT BOTH DELAY CODES"
            If (TplDtd1 > 0) And (TplDtd2 > 0) Then
                MyTestCase(0).Text = "1.1.0.0"
                MyTestCase(1).Text = "BOTH CODES AND TIMES"
                If FlightIsDelayed Then
                    MyTestCase(0).Text = "1.1.1.0"
                    If TplTtld = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.1.1.1"
                        MyTestCase(2).Text = "MATCHING DELAY TIME"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX TEXT OK"
                        MyTestCase(5).Text = "OK"
                        MyTestCase(6).Text = "CONTINUE"
                        AftFldText(17).BackColor = vbWhite
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = vbWhite
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplTtld > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.1.1.2"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX TEXT OK"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplTtld < TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.1.1.3"
                        MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX TEXT OK"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        'No more cases
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "1.1.2.0"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TELEX TEXT OK"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(15).BackColor = LightestRed
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(16).BackColor = LightestRed
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(17).BackColor = LightestRed
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 = 0) And (TplDtd2 = 0) Then
                MyTestCase(0).Text = "1.2.0.0"
                MyTestCase(1).Text = "BOTH CODES BUT NO TIMES"
                If FlightIsDelayed Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "1.2.1.0"
                    MyTestCase(2).Text = "FLIGHT IS DELAYED"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "TIME REQ"
                    MyTestCase(6).Text = "INPUT"
                    AftFldText(17).BackColor = LightYellow
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightYellow
                    AftFldText(18).ForeColor = vbBlack
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "1.2.2.0"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(15).BackColor = LightestRed
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(16).BackColor = LightestRed
                    AftFldText(16).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 > 0) Then
                MyTestCase(0).Text = "1.3.0.0"
                MyTestCase(1).Text = "BOTH CODES AND TIME1"
                If FlightIsDelayed Then
                    MyTestCase(0).Text = "1.3.1.0"
                    If TplDtd1 = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.3.1.1"
                        MyTestCase(2).Text = "MATCHING DELAY TIME"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd1 > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.3.1.2"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd1 < TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.3.1.3"
                        MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TIME2 CREATED"
                        MyTestCase(5).Text = "TELEX OK"
                        MyTestCase(6).Text = "CONTINUE"
                        TplDtd2 = TtlDiff - TplDtd1
                        AftFldText(18).BackColor = LightestBlue
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        'No more cases
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "1.3.2.0"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(15).BackColor = LightestRed
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(16).BackColor = LightestRed
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(17).BackColor = LightestRed
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestBlue
                    AftFldText(18).ForeColor = vbBlack
                End If
            ElseIf (TplDtd2 > 0) Then
                MyTestCase(0).Text = "1.4.0.0"
                MyTestCase(1).Text = "BOTH CODES AND TIME2"
                If FlightIsDelayed Then
                    MyTestCase(0).Text = "1.4.1.0"
                    If TplDtd2 = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.4.1.1"
                        MyTestCase(2).Text = "MATCHING DELAY TIMES"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd2 > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.4.1.2"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd2 < TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "1.4.1.3"
                        MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TIME1 CREATED"
                        MyTestCase(5).Text = "TELEX OK"
                        MyTestCase(6).Text = "CONTINUE"
                        TplDtd1 = TtlDiff - TplDtd1
                        AftFldText(17).BackColor = LightestBlue
                        AftFldText(17).ForeColor = vbBlack
                    Else
                        'No more cases
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "1.4.2.0"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(15).BackColor = LightestRed
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(16).BackColor = LightestRed
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            Else
                '? No more cases
            End If
        ElseIf (AftFldText(15).Text <> "") Then
            MyTestCase(0).Text = "2.0.0.0"
            MyTestCase(1).Text = "GOT DELAY CODE1"
            MyTestCase(3).Text = "NOT TESTED"
            If (TplDtd1 = 0) And (TplDtd2 = 0) Then
                MyTestCase(0).Text = "2.1.0.0"
                MyTestCase(1).Text = "GOT CODE1 BUT NO TIMES"
                If FlightIsDelayed Then
                    TplDtd1 = TtlDiff
                    'DOCUMENTED
                    MyTestCase(0).Text = "2.1.1.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS DELAYED"
                    MyTestCase(4).Text = "TIME1 CREATED"
                    MyTestCase(5).Text = "TEXT OK"
                    MyTestCase(6).Text = "CONTINUE"
                    AftFldText(17).BackColor = LightestBlue
                    AftFldText(17).ForeColor = vbBlack
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "2.1.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(1).Text = "NO DELAY BUT GOT CODE1"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(15).BackColor = LightestRed
                    AftFldText(15).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 > 0) And (TplDtd2 = 0) Then
                MyTestCase(0).Text = "2.2.0.0"
                MyTestCase(1).Text = "DELAY CODE1 AND TIME1"
                If TplDtd1 = TtlDiff Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "2.2.1.0"
                    MyTestCase(2).Text = "MATCHING DELAY TIME"
                    If FlightIsDelayed Then
                        MyTestCase(0).Text = "2.2.1.0"
                        MyTestCase(3).Text = "RESULT IS OK"
                        AftFldText(17).BackColor = vbWhite
                        AftFldText(17).ForeColor = vbBlack
                        MyTestCase(4).Text = "TELEX TEXT OK"
                        MyTestCase(5).Text = "OK"
                        MyTestCase(6).Text = "CONTINUE"
                    Else
                        MyTestCase(0).Text = "2.2.1.2"
                        MyTestCase(2).Text = "CANNOT BE"
                    End If
                ElseIf TplDtd1 > TtlDiff Then
                    MyTestCase(0).Text = "2.2.2.0"
                    MyTestCase(2).Text = "TIME > DELAY"
                    If FlightIsDelayed Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.2.2.1"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                    Else
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.2.2.2"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(15).BackColor = LightestRed
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                    End If
                ElseIf TplDtd1 < TtlDiff Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "2.2.3.0"
                    MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TIME2 CREATED"
                    MyTestCase(5).Text = "CODE REQ"
                    MyTestCase(6).Text = "INPUT"
                    TplDtd2 = TtlDiff - TplDtd1
                    AftFldText(16).Text = "??"
                    AftFldText(16).BackColor = LightestBlue
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(17).BackColor = vbWhite
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestBlue
                    AftFldText(18).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 = 0) And (TplDtd2 > 0) Then
                MyTestCase(0).Text = "2.3.0.0"
                MyTestCase(1).Text = "DELAY CODE1 AND TIME2"
                If FlightIsDelayed Then
                    MyTestCase(0).Text = "2.3.1.0"
                    If TplDtd2 = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.3.1.1"
                        MyTestCase(2).Text = "DELAY TIME MATCHING"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd2 > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.3.1.2"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd2 < TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.3.1.3"
                        MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TIME1 CREATED"
                        MyTestCase(5).Text = "CODE REQ"
                        MyTestCase(6).Text = "INPUT"
                        TplDtd1 = TtlDiff - TplDtd2
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestBlue
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        'No more cases
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "2.3.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(16).Text = "??"
                    AftFldText(15).BackColor = LightestRed
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(16).BackColor = LightestBlue
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(17).BackColor = LightestBlue
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 > 0) And (TplDtd2 > 0) Then
                MyTestCase(0).Text = "2.4.0.0"
                MyTestCase(1).Text = "CODE1 AND BOTH TIMES"
                If FlightIsDelayed Then
                    MyTestCase(0).Text = "2.4.1.0"
                    If TplTtld = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.4.1.1"
                        MyTestCase(2).Text = "DELAY TIMES MATCHING"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "CODE REQ"
                        MyTestCase(6).Text = "INPUT"
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = vbWhite
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = vbWhite
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplTtld > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.4.1.2"
                        MyTestCase(2).Text = "BOTH TIMES > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplTtld < TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "2.4.1.3"
                        MyTestCase(2).Text = "BOTH TIMES < TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "CODE REQ"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        'No more cases
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "2.4.2.0"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(16).Text = "??"
                    AftFldText(16).BackColor = LightestBlue
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(17).BackColor = LightestRed
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            Else
                'No more cases?
            End If
        ElseIf (AftFldText(16).Text <> "") Then
            MyTestCase(0).Text = "3.0.0.0"
            MyTestCase(1).Text = "GOT DELAY CODE2"
            MyTestCase(3).Text = "NOT TESTED"
            If (TplDtd1 = 0) And (TplDtd2 = 0) Then
                MyTestCase(0).Text = "3.1.0.0"
                MyTestCase(1).Text = "GOT CODE2 BUT NO TIMES"
                If FlightIsDelayed Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "3.1.1.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS DELAYED"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "CODE REQ"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(15).Text = "??"
                    AftFldText(15).BackColor = LightestBlue
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(16).BackColor = vbWhite
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(17).BackColor = LightestRed
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "3.1.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(1).Text = "NO DELAY BUT GOT CODE2"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX TEXT OK"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(16).BackColor = LightestRed
                    AftFldText(16).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 > 0) And (TplDtd2 = 0) Then
                MyTestCase(0).Text = "3.2.0.0"
                MyTestCase(1).Text = "DELAY CODE2 AND TIME1"
                If TplDtd1 = TtlDiff Then
                    MyTestCase(0).Text = "3.2.1.0"
                    MyTestCase(2).Text = "MATCHING DELAY TIME"
                    If FlightIsDelayed Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.2.1.1"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "CODE REQ"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(16).BackColor = vbWhite
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        MyTestCase(0).Text = "3.2.1.2"
                        MyTestCase(2).Text = "CANNOT BE"
                    End If
                ElseIf TplDtd1 > TtlDiff Then
                    MyTestCase(0).Text = "3.2.2.0"
                    MyTestCase(2).Text = "TIME > DELAY"
                    If FlightIsDelayed Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.2.2.1"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.2.2.2"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                        MyTestCase(4).Text = "TELEX TEXT OK"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(16).BackColor = LightestRed
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                    End If
                ElseIf TplDtd1 < TtlDiff Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "3.2.3.0"
                    MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TIME2 CREATED"
                    MyTestCase(5).Text = "CODE REQ"
                    MyTestCase(6).Text = "INPUT"
                    TplDtd2 = TtlDiff - TplDtd1
                    AftFldText(15).Text = "??"
                    AftFldText(15).BackColor = LightestBlue
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(17).BackColor = vbWhite
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestBlue
                    AftFldText(18).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 = 0) And (TplDtd2 > 0) Then
                MyTestCase(0).Text = "3.3.0.0"
                MyTestCase(1).Text = "DELAY CODE2 AND TIME2"
                If FlightIsDelayed Then
                    MyTestCase(0).Text = "3.3.1.0"
                    If TplDtd2 = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.3.1.1"
                        MyTestCase(2).Text = "DELAY TIME MATCHING"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd2 > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.3.1.2"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestBlue
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd2 < TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.3.1.3"
                        MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TIME1 CREATED"
                        MyTestCase(5).Text = "CODE REQ"
                        MyTestCase(6).Text = "INPUT"
                        TplDtd1 = TtlDiff - TplDtd2
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestBlue
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        'No more cases
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "3.3.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(16).BackColor = LightestRed
                    AftFldText(16).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            ElseIf (TplDtd1 > 0) And (TplDtd2 > 0) Then
                MyTestCase(0).Text = "3.4.0.0"
                MyTestCase(1).Text = "CODE2 AND BOTH TIMES"
                If FlightIsDelayed Then
                    MyTestCase(0).Text = "3.4.1.0"
                    If TplTtld = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.4.1.1"
                        MyTestCase(2).Text = "DELAY TIMES MATCHING"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "CODE REQ"
                        MyTestCase(6).Text = "INPUT"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = vbWhite
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = vbWhite
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplTtld > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.4.1.2"
                        MyTestCase(2).Text = "BOTH TIMES > TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplTtld < TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "3.4.1.3"
                        MyTestCase(2).Text = "BOTH TIMES < TOTAL DELAY"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(4).Text = "TELEX UNCLEAR"
                        MyTestCase(5).Text = "CODE REQ"
                        MyTestCase(6).Text = "CLARIFY"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                    Else
                        'No more cases
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "3.4.2.0"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "CLARIFY"
                    AftFldText(15).Text = "??"
                    AftFldText(15).BackColor = LightestBlue
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(17).BackColor = LightestRed
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            Else
                'No more cases?
            End If
        ElseIf (AftFldText(15).Text = "") And (AftFldText(16).Text = "") Then
            MyTestCase(0).Text = "4.0.0.0"
            MyTestCase(1).Text = "NO DELAY CODES"
            MyTestCase(3).Text = "NOT TESTED"
            If (TplDtd1 = 0) And (TplDtd2 = 0) Then
                MyTestCase(0).Text = "4.1.0.0"
                MyTestCase(1).Text = "NO DELAY CODES / NO TIMES"
                MyTestCase(3).Text = "NOT TESTED"
                If FlightIsDelayed Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "4.1.1.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS DELAYED"
                    MyTestCase(4).Text = "TIME1 CREATED"
                    MyTestCase(5).Text = "CODE REQ"
                    MyTestCase(6).Text = "INPUT"
                    TplDtd1 = TtlDiff
                    AftFldText(15).Text = "??"
                    AftFldText(15).BackColor = LightestBlue
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(17).BackColor = LightestBlue
                    AftFldText(17).ForeColor = vbBlack
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "4.1.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX TEXT OK"
                    MyTestCase(5).Text = "OK"
                    MyTestCase(6).Text = "CONTINUE"
                End If
            ElseIf (TplDtd1 > 0) And (TplDtd2 > 0) Then
                If FlightIsDelayed Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "4.2.1.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(1).Text = "NO CODES BUT BOTH TIMES"
                    MyTestCase(2).Text = "NOT MATCHING DELAY TIME"
                    MyTestCase(4).Text = "TELEX UNCLEAR"
                    MyTestCase(5).Text = "CODE REQ"
                    MyTestCase(6).Text = "INPUT"
                    AftFldText(15).Text = "??"
                    AftFldText(15).BackColor = LightestBlue
                    AftFldText(15).ForeColor = vbBlack
                    AftFldText(16).Text = "??"
                    AftFldText(16).BackColor = LightestBlue
                    AftFldText(16).ForeColor = vbBlack
                    If TplTtld <> TtlDiff Then
                        'DOCUMENTED (A/B)
                        MyTestCase(0).Text = "4.2.1.1"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "INPUT"
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "4.2.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(1).Text = "NO CODES BUT BOTH TIMES"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX TEXT OK"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "IGNORE"
                    AftFldText(17).BackColor = LightestRed
                    AftFldText(17).ForeColor = vbBlack
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            ElseIf TplDtd1 > 0 Then
                MyTestCase(0).Text = "4.3.0.0"
                MyTestCase(1).Text = "NO DELAY CODES BUT TIME1"
                MyTestCase(3).Text = "NOT TESTED"
                If FlightIsDelayed Then
                    'DOCUMENTED
                    MyTestCase(0).Text = "4.3.1.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "MATCHING DELAY TIME"
                    MyTestCase(4).Text = "CODE REQUIRED"
                    MyTestCase(5).Text = "MANUAL"
                    MyTestCase(6).Text = "INPUT"
                    AftFldText(15).Text = "??"
                    AftFldText(15).BackColor = LightestBlue
                    AftFldText(15).ForeColor = vbBlack
                    If TplDtd1 <> TtlDiff Then
                        If TplDtd1 > TtlDiff Then
                            'DOCUMENTED
                            MyTestCase(0).Text = "4.3.1.1"
                            MyTestCase(3).Text = "RESULT IS OK"
                            MyTestCase(2).Text = "NOT MATCHING DELAY TIME"
                            MyTestCase(4).Text = "TIME > DELAY"
                            MyTestCase(5).Text = "WARN"
                            AftFldText(17).BackColor = LightestRed
                            AftFldText(17).ForeColor = vbBlack
                        Else
                            'DOCUMENTED
                            MyTestCase(0).Text = "4.3.1.2"
                            MyTestCase(3).Text = "RESULT IS OK"
                            MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                            MyTestCase(4).Text = "TIME2 CREATED"
                            MyTestCase(5).Text = "UNCLEAR"
                            MyTestCase(6).Text = "INPUT"
                            TplDtd2 = TtlDiff - TplDtd1
                            AftFldText(16).Text = "??"
                            AftFldText(16).BackColor = LightestBlue
                            AftFldText(16).ForeColor = vbBlack
                            AftFldText(17).BackColor = vbWhite
                            AftFldText(17).ForeColor = vbBlack
                            AftFldText(18).BackColor = LightestBlue
                            AftFldText(18).ForeColor = vbBlack
                        End If
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "4.3.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX TEXT OK"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "IGNORE"
                    AftFldText(17).BackColor = LightestRed
                    AftFldText(17).ForeColor = vbBlack
                End If
            ElseIf TplDtd2 > 0 Then
                MyTestCase(0).Text = "4.4.0.0"
                MyTestCase(1).Text = "NO DELAY CODES BUT TIME2"
                MyTestCase(3).Text = "NOT TESTED"
                If FlightIsDelayed Then
                    If TplDtd2 = TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "4.4.1.1"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(2).Text = "MATCHING DELAY TIME"
                        MyTestCase(4).Text = "CODES REQUIRED"
                        MyTestCase(5).Text = "WARN"
                        MyTestCase(6).Text = "INPUT"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    ElseIf TplDtd2 > TtlDiff Then
                        'DOCUMENTED
                        MyTestCase(0).Text = "4.4.1.2"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(2).Text = "DELAY TIME > TOTAL DELAY"
                        MyTestCase(4).Text = "CODES REQUIRED"
                        MyTestCase(5).Text = "ERROR"
                        MyTestCase(6).Text = "INPUT"
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestRed
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = LightestRed
                        AftFldText(18).ForeColor = vbBlack
                    Else
                        'DOCUMENTED
                        MyTestCase(0).Text = "4.4.1.3"
                        MyTestCase(3).Text = "RESULT IS OK"
                        MyTestCase(2).Text = "DELAY TIME < TOTAL DELAY"
                        MyTestCase(4).Text = "TIME1 CREATED"
                        MyTestCase(5).Text = "UNCLEAR"
                        MyTestCase(6).Text = "INPUT"
                        TplDtd1 = TtlDiff - TplDtd2
                        AftFldText(15).Text = "??"
                        AftFldText(15).BackColor = LightestBlue
                        AftFldText(15).ForeColor = vbBlack
                        AftFldText(16).Text = "??"
                        AftFldText(16).BackColor = LightestBlue
                        AftFldText(16).ForeColor = vbBlack
                        AftFldText(17).BackColor = LightestBlue
                        AftFldText(17).ForeColor = vbBlack
                        AftFldText(18).BackColor = vbWhite
                        AftFldText(18).ForeColor = vbBlack
                    End If
                Else
                    'DOCUMENTED
                    MyTestCase(0).Text = "4.4.2.0"
                    MyTestCase(3).Text = "RESULT IS OK"
                    MyTestCase(2).Text = "FLIGHT IS NOT DELAYED"
                    MyTestCase(4).Text = "TELEX TEXT OK"
                    MyTestCase(5).Text = "WARN"
                    MyTestCase(6).Text = "IGNORE"
                    AftFldText(18).BackColor = LightestRed
                    AftFldText(18).ForeColor = vbBlack
                End If
            End If
        Else
            MyTestCase(0).Text = "5.0.0.0"
            MyTestCase(1).Text = "THIS CANNOT BE"
            MyTestCase(2).Text = ""
            AftFldText(17).BackColor = vbWhite
            AftFldText(17).ForeColor = vbBlack
            AftFldText(18).BackColor = vbWhite
            AftFldText(18).ForeColor = vbBlack
        End If
        If AftOfbl = "" Then
            ConText = "[" & ConText & "]"
        Else
        End If
        AftFldText(19).Text = ConText
        AftFldText(20).Text = TtlText
        AftFldText(21).Text = TplDela
        If TplDtd1 < 0 Then TplDtd1 = 0
        If TplDtd2 < 0 Then TplDtd2 = 0
        TplDur1 = GetHHMMFromMinutes(TplDtd1)
        TplDur2 = GetHHMMFromMinutes(TplDtd2)
        If TplDtd1 = 0 Then TplDur1 = ""
        If TplDtd2 = 0 Then TplDur2 = ""
        AftFldText(17).Text = TplDur1
        AftFldText(18).Text = TplDur2
    End If
    StopNestedCalls = False
End Sub
Private Sub CalculateEans()
    'HardCoded
    Dim AftEans As String
    Dim AftBaaa As String
    Dim AftAirb As String
    Dim AftEtdi As String
    Dim AftOfbl As String
    Dim AftStod As String
    Dim AftBest As String
    Dim AftAdid As String
    Dim FldType As String
    Dim ConText As String
    Dim tmpValue As String
    Dim MinEETF As Integer
    Dim ItmPos As Integer
    Dim idx As Integer
    Dim EETIsMand As Boolean
    Dim TimeVal
    If CurTlxTplType = "AD" Then EETIsMand = True
    If CurTlxTplType = "" Then EETIsMand = True
    AftAdid = GetFieldValue("ADID", MyAftTabRecord, MyAftTabFields)
    If AftAdid = "D" Then
        AftAirb = GetFieldValue("AIRB", MyAftTabRecord, MyAftTabFields)
        AftEtdi = GetFieldValue("ETDI", MyAftTabRecord, MyAftTabFields)
        AftOfbl = GetFieldValue("OFBL", MyAftTabRecord, MyAftTabFields)
        AftStod = GetFieldValue("STOD", MyAftTabRecord, MyAftTabFields)
        'This field value is probably wrong
        'AftEans = GetFieldValue("EETA", MyAftTabRecord, MyAftTabFields)
        AftEans = ""
        AftFldText(1).Text = txtEditEET(1).Text
        AftFldText(2).Text = txtEditEET(2).Text
        AftBaaa = Trim(txtEditEET(2).Text)
        MinEETF = Val(AftBaaa)
        ConText = ""
        AftBest = ""
        If AftBest = "" Then
            AftBest = AftAirb
            ConText = "AIB"
        End If
        If AftBest = "" Then
            AftBest = AftOfbl
            ConText = "OFB"
        End If
        If AftBest = "" Then
            AftBest = AftEtdi
            ConText = "ETD"
        End If
        If AftBest = "" Then
            AftBest = AftStod
            ConText = "STD"
        End If
        tmpValue = ""
        If MinEETF > 0 Then
            TimeVal = CedaFullDateToVb(AftBest)
            TimeVal = DateAdd("n", MinEETF, TimeVal)
            AftEans = Format(TimeVal, "yyyymmddhhmm") & "00"
            tmpValue = Format(TimeVal, MySetUp.DefDateFormat)
        End If
        AftFldText(9).Text = tmpValue
        tmpValue = ""
        If MinEETF > 0 Then
            tmpValue = Format(TimeVal, "hhmm")
        End If
        AftFldText(10).Text = tmpValue
        ItmPos = GetItemNo(MyAftTabFields, "EETF")
        If ItmPos > 0 Then
            SetItem MyAftTabRecord, ItmPos, ",", AftBaaa
        Else
            MyAftTabFields = MyAftTabFields & "," & "EETF"
            MyAftTabRecord = MyAftTabRecord & "," & AftBaaa
        End If
        ItmPos = GetItemNo(MyAftTabFields, "EETA")
        If ItmPos > 0 Then
            SetItem MyAftTabRecord, ItmPos, ",", AftEans
        Else
            MyAftTabFields = MyAftTabFields & "," & "EETA"
            MyAftTabRecord = MyAftTabRecord & "," & AftEans
        End If
        idx = GetFltInputFldIdx(0, "EETA")
        If idx >= 0 Then
            FldType = lblDatType(idx).Tag
            tmpValue = ""
            If AftEans <> "" Then
                Select Case FldType
                    Case "DD"
                        tmpValue = Mid(AftEans, 7, 2)
                    Case "HHMM"
                        tmpValue = Mid(AftEans, 9, 4)
                    Case "DDHHMM"
                        tmpValue = Mid(AftEans, 7, 6)
                    Case Else
                        tmpValue = ""
                End Select
            End If
            txtFltInput(idx).Text = tmpValue
            If AftEans = "" Then
                chkFltTick(idx).Value = 1
            Else
                chkFltTick(idx).Value = 0
            End If
        End If
        If ConText <> "" Then
            AftFldLabel(10).Caption = "[" & ConText & "]"
        Else
            AftFldLabel(10).Caption = ""
        End If
        If AftEans = "" Then
            If EETIsMand Then
                AftFldText(9).BackColor = LightYellow
                AftFldText(10).BackColor = LightYellow
            Else
                AftFldText(9).BackColor = vbWhite
                AftFldText(10).BackColor = vbWhite
            End If
        Else
            If ConText = "AIB" Then
                AftFldText(9).BackColor = vbWhite
                AftFldText(10).BackColor = vbWhite
            Else
                AftFldText(9).BackColor = &HFFC0FF
                AftFldText(10).BackColor = &HFFC0FF
            End If
        End If
        If (EETIsMand) And (AftBaaa = "") Then
            chkEditEET(1).Value = 1
            AftFldText(1).BackColor = LightYellow
            AftFldText(2).BackColor = LightYellow
            txtEditEET(1).BackColor = LightYellow
            txtEditEET(2).BackColor = LightYellow
        Else
            chkEditEET(1).Value = 0
            AftFldText(1).BackColor = vbWhite
            AftFldText(2).BackColor = vbWhite
            txtEditEET(1).BackColor = vbWhite
            txtEditEET(2).BackColor = vbWhite
        End If
    End If
End Sub
Private Sub SetEETMinutes(SetValue As String)
    Dim Index As Integer
    Dim tmpText As String
    Dim tmpTag As String
    Dim tmpMM As String
    Dim tmpHH As String
    Dim iTime As Integer
    Index = 2
    tmpText = SetValue
    txtEditEET(Index).Text = tmpText
    chkEditEET(Index).Caption = tmpText
    If txtEditEET(Index).Tag = "" Then
        tmpTag = tmpText
        If tmpTag = "" Then tmpTag = "0"
        txtEditEET(Index).Tag = tmpTag
    End If
    chkEditEET(Index).Value = 0
    chkEditEET(Index).Visible = True
    If tmpText <> "" Then
        iTime = Val(tmpText)
        tmpHH = CStr(iTime \ 60)
        tmpMM = CStr(iTime Mod 60)
        tmpHH = Right("00" & tmpHH, 2)
        tmpMM = Right("00" & tmpMM, 2)
        tmpText = tmpHH & tmpMM
    End If
    txtEditEET(1).Text = tmpText
End Sub

Private Sub txtEditPX_Change(Index As Integer)
    Dim tmpTag As String
    Dim tmpTagItem As String
    Dim tmpText As String
    Dim idx As Integer
    Dim itm As Integer
    tmpText = txtEditPX(Index).Text
    chkEditPX(Index).Caption = tmpText
    If Index = 1 Then AftFldText(42).Text = tmpText
    If Index = 2 Then AftFldText(41).Text = tmpText
    If Index = 3 Then AftFldText(40).Text = tmpText
    If Index = 4 Then AftFldText(39).Text = tmpText
    If chkEditPX(Index).Tag <> "" Then
        tmpTag = chkEditPX(Index).Tag
        tmpTagItem = "START"
        itm = 0
        While tmpTagItem <> ""
            itm = itm + 1
            tmpTagItem = GetItem(tmpTag, itm, ",")
            If tmpTagItem <> "" Then
                idx = Val(tmpTagItem)
                txtFltInput(idx).Text = tmpText
            End If
        Wend
        'idx = Val(chkEditPX(Index).Tag)
        'txtFltInput(idx).Text = tmpText
    End If
End Sub

Private Sub txtEditPX_GotFocus(Index As Integer)
    On Error Resume Next
    If Not StopNestedCalls Then
        StopNestedCalls = True
            Select Case Index
                Case 0
                Case 1 To 4
                    chkEditPX(Index).Visible = False
                    txtEditPX(Index).SetFocus
                Case Else
            End Select
        SetTextSelected
        StopNestedCalls = False
    End If
End Sub

Private Sub txtEditPX_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim tmpChr As String
    Dim CmpChr As String
    Select Case KeyAscii
        Case 13
            txtEditPX_LostFocus Index
        Case Else
        Select Case Index
            Case 1 To 4
                CmpChr = "0123456789" & Chr(8)
                tmpChr = Chr(KeyAscii)
                If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
            Case Else
        End Select
    End Select
End Sub

Private Sub txtEditPX_LostFocus(Index As Integer)
    Dim tmpText As String
    If Not StopNestedCalls Then
        Select Case Index
            Case 0
            Case 1 To 4
                tmpText = Trim(txtEditPX(Index).Text)
                txtEditPX(Index).Text = tmpText
                chkEditPX(Index).Caption = tmpText
                chkEditPX(Index).Value = 0
                chkEditPX(Index).Visible = True
                If Index = 1 Then AftFldText(42).Text = tmpText
                If Index = 2 Then AftFldText(41).Text = tmpText
                If Index = 3 Then AftFldText(40).Text = tmpText
                If Index = 4 Then AftFldText(39).Text = tmpText
            Case Else
        End Select
    End If
End Sub

Private Sub txtFltInput_Change(Index As Integer)
    Dim tmpType As String
    Dim FldName As String
    Dim FldType As String
    Dim TlxText As String
    Dim idx As Integer
    If Not SystemInput Then
        If txtFltInput(Index).Text <> chkFltCheck(Index).Tag Then
            chkFltCheck(Index).Value = 1
        Else
            chkFltCheck(Index).Value = 0
        End If
    End If
    If (Not SystemInput) And (Not StopNestedCalls) Then
        StopNestedCalls = True
        If chkFltTick(Index).Tag <> "" Then
            idx = Val(chkFltTick(Index).Tag)
            tmpType = lblDatType(Index).Tag
            Select Case tmpType
                Case "DCD1", "DCD2", "DTD1", "DTD2"
                    txtEditDL(idx).Text = txtFltInput(Index).Text
                Case "PAX1", "PAX2", "PAX3", "PAX4"
                    txtEditPX(idx).Text = txtFltInput(Index).Text
                Case Else
            End Select
        Else
            If TplUpdateCascading Then
                FldName = FltEditPanel(Index).Tag
                FldType = lblDatType(Index).Tag
                idx = 0
                idx = GetFltInputFldIdx(idx, FldName)
                While idx >= 0
                    If idx <> Index Then
                        If lblDatType(idx).Tag = FldType Then
                            txtFltInput(idx).Text = txtFltInput(Index).Text
                        End If
                    End If
                    idx = idx + 1
                    idx = GetFltInputFldIdx(idx, FldName)
                Wend
            End If
        End If
        StopNestedCalls = False
        If Not SystemInput Then
            TlxText = GetTlxTextFromTemplate(CurTlxTplText, TelexTextIsClean)
            CheckSendStatus
            If Not TplTextIsUser Then TlxText = CurNewTplText
            txtTelexText(0).Text = TlxText
            RefreshControls 1
        End If
    End If
End Sub

Private Sub txtFltInput_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim tmpType As String
    Dim tmpChr As String
    Dim CmpChr As String
    Select Case KeyAscii
        Case 13
            txtFltInput_LostFocus Index
        Case Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
        tmpType = lblDatType(Index).Tag
        If tmpType <> "" Then
            CmpChr = "0123456789" & Chr(8)
            tmpChr = Chr(KeyAscii)
            Select Case tmpType
                Case "DTD1", "DTD2"
                    If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
                Case "PAX1", "PAX2", "PAX3", "PAX4"
                    If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
                Case "DD", "HHMM", "DDHHMM"
                    If InStr(CmpChr, tmpChr) = 0 Then KeyAscii = 0
                Case Else
            End Select
        End If
    End Select
End Sub

Private Sub txtFltInput_LostFocus(Index As Integer)
    Dim tmpText As String
    Dim tmpType As String
    tmpText = Trim(txtFltInput(Index).Text)
    tmpType = lblDatType(Index).Tag
    Select Case tmpType
        Case "DCD1", "DCD2"
            '
        Case "DTD1", "DTD2"
            tmpText = Right("0000" & tmpText, 4)
            If tmpText = "0000" Then tmpText = ""
        Case Else
    End Select
    txtFltInput(Index).Text = tmpText
End Sub

Private Sub txtPreview_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtDestAddr_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index = 0 Then
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub txtREGN_Change()
    Dim idx As Integer
    idx = 0
    idx = GetFltInputFldIdx(idx, "REGN")
    If idx >= 0 Then txtFltInput(idx).Text = Trim(txtRegn.Text)
    If Trim(txtRegn.Text) <> "" Then
        If txtRegn.Text = txtRegn.Tag Then
            txtRegn.BackColor = vbWhite
        Else
            txtRegn.BackColor = LightestGreen
        End If
    Else
        txtRegn.BackColor = LightYellow
    End If
End Sub

Private Sub txtREGN_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtSiText_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTelexText_Change(Index As Integer)
    Dim txt As String
    Dim idx As Integer
    Dim TextIsOk As Boolean
    txt = Trim(txtTelexText(0).Text)
    idx = Index
    TextIsOk = True
    If txt = "" Then TextIsOk = False
    'If TextIsOk Then
    '    chkTlxTxt.Picture = imgAmpel(0).Picture
    '    PanelIcon(6).Picture = imgAmpel(0).Picture
    'Else
    '    chkTlxTxt.Picture = imgAmpel(2).Picture
    '    PanelIcon(6).Picture = imgAmpel(2).Picture
    'End If
    txt = GetCleanTelexText(txt, TextIsOk, False)
    chkTlxTxt.Picture = PanelIcon(6).Picture
    If chkTlxTxt.Value = 1 Then
        TelexTextIsClean = TextIsOk
        CheckSendStatus
    End If
End Sub

Private Sub txtTelexText_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub chkSend_Click()
    Dim TelexText As String
    Dim ErrMsg As String
    Dim tmpResult As String
    Dim retCod As Integer
    Dim outData As String
    Dim outFields As String
    Dim outSqlKey As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpAdid As String
    Dim tmpAlc3 As String
    Dim tmpFkey As String
    Dim tmpFlda As String
    Dim tmpFlns As String
    Dim tmpFltn As String
    Dim tmpFlnu As String
    Dim tmpSked As String
    Dim tmpUrno As String
    Dim tmpTime As String
    Dim tmpData As String
    Dim tmpCallCode As String
    Dim TlxRec As String
    Dim tmpSere As String
    Dim LineNo As Long
    Dim SendIt As Boolean
    Dim NewUrno As String
    Dim SndTlxUrno As String
    Dim i As Integer
    Dim idx As Integer
    
    If chkSend.Value = 1 Then
        CheckSendStatus
        If Not TelexIsCleanToSend Then
            ErrMsg = "The telex cannot be sent."
            MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", ErrMsg, "stop", "", UserAnswer
            chkSend.Value = 0
            Exit Sub
        End If
        CheckAftUrnoField
        SndTlxUrno = CurTlxTabUrno
        Screen.MousePointer = 11
        chkSend.BackColor = MyOwnButtonDown
        If MySitaOutType = "SITA_FILE" Then
            chkOutFormat.Value = 1
        Else
            chkPreView.Value = 1
        End If
        For idx = 0 To chkOutCnt.UBound
            If (chkOutCnt(idx).Visible) Or (idx = 0) Then
                chkOutCnt(idx).Value = 1
                Me.Refresh
                DoEvents
                SendIt = True
                ErrMsg = ""
                If MySitaOutType = "SITA_NODE" Then
                    TelexText = Chr(13) & Chr(10) & Chr(1) & Trim(txtPreview(idx).Text) & Chr(13) & Chr(10) & Chr(3)
                Else
                    TelexText = Trim(txtPreview(idx).Text)
                End If
                If MySitaOutType = "SITA_FILE" Then
                    If InStr(TelexText, "=TEXT") = 0 Then
                        ErrMsg = ErrMsg & "Missing keyword: '=TEXT'" & vbNewLine
                        SendIt = False
                    End If
                    If InStr(TelexText, "=DESTINATION TYPE B") = 0 Then
                        ErrMsg = ErrMsg & "Missing keyword: '=DESTINATION TYPE B'" & vbNewLine
                        SendIt = False
                    End If
                End If
                If SendIt Then
                    TelexText = CleanString(TelexText, CLIENT_TO_SERVER, False)
                    If Len(TelexText) <= TlxTxtFldSize Then
                        tmpTxt1 = TelexText
                        tmpTxt2 = " "
                    Else
                        i = TlxTxtFldSize
                        While i > 1 And Mid(TelexText, i, 1) = " "
                            i = i - 1
                        Wend
                        tmpTxt1 = Mid(TelexText, 1, i)
                        tmpTxt2 = Mid(TelexText, i + 1)
                    End If
                    LineNo = TlxTypeTab.GetCurrentSelected
                    tmpTtyp = TlxTypeTab.GetColumnValue(LineNo, 0)
                    If tmpTtyp = "FREE" Then tmpTtyp = " "
                    tmpStyp = TlxTypeTab.GetColumnValue(LineNo, 1)
                    outFields = "URNO,SERE,STAT,WSTA,CDAT,FLNU,ALC3,FKEY,FLDA,FLNS,FLTN,TIME,TTYP,STYP,TXT1,TXT2"
                    tmpUrno = Trim(NewSendUrno(idx).Text)
                    tmpData = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
                    If (tmpUrno = "") Or (tmpUrno = tmpData) Then
                        tmpUrno = UfisServer.UrnoPoolGetNext
                    End If
                    NewUrno = tmpUrno
                    tmpCallCode = Trim(NewCallCode(idx).Text)
                    If tmpCallCode = "" Then tmpCallCode = "M"
                    NewCallCode(idx).Text = tmpCallCode
                    tmpFlnu = Trim(GetFieldValue("URNO", MyAftTabRecord, MyAftTabFields))
                    tmpAlc3 = GetFieldValue("ALC3", MyAftTabRecord, MyAftTabFields)
                    tmpFkey = GetFieldValue("FKEY", MyAftTabRecord, MyAftTabFields)
                    tmpFlda = GetFieldValue("FLDA", MyAftTabRecord, MyAftTabFields)
                    tmpFlns = GetFieldValue("FLNS", MyAftTabRecord, MyAftTabFields)
                    tmpFltn = GetFieldValue("FLTN", MyAftTabRecord, MyAftTabFields)
                    tmpAdid = GetFieldValue("ADID", MyAftTabRecord, MyAftTabFields)
                    If tmpFlnu = "" Then
                        tmpFlnu = "0"
                    End If
                    Select Case tmpAdid
                        Case "A"
                            tmpTime = GetFieldValue("STOA", MyAftTabRecord, MyAftTabFields)
                        Case "D"
                            tmpTime = GetFieldValue("STOD", MyAftTabRecord, MyAftTabFields)
                        Case "B"
                            tmpTime = GetFieldValue("STOD", MyAftTabRecord, MyAftTabFields)
                        Case Else
                            tmpTime = MySetUp.GetUtcServerTime & Format(Now, "ss")
                    End Select
                    tmpSked = tmpTime
                    tmpTime = MySetUp.GetUtcServerTime & Format(Now, "ss")
                    'outFields = "URNO,SERE,STAT,WSTA,CDAT,FLNU,ALC3,FKEY,FLDA,FLNS,FLTN,TIME,TTYP,STYP,TXT1,TXT2"
                    outData = tmpUrno & ",S,S"
                    outData = outData & "," & tmpCallCode
                    outData = outData & "," & tmpTime
                    
                    outData = outData & "," & tmpFlnu
                    outData = outData & "," & tmpAlc3
                    outData = outData & "," & tmpFkey
                    outData = outData & "," & tmpFlda
                    outData = outData & "," & tmpFlns
                    outData = outData & "," & tmpFltn
                    outData = outData & "," & tmpSked
                    
                    outData = outData & "," & tmpTtyp
                    outData = outData & "," & tmpStyp
                    outData = outData & "," & tmpTxt1
                    outData = outData & "," & tmpTxt2
                    retCod = UfisServer.CallCeda(tmpResult, "IRT", "TLXTAB", outFields, outData, "          ", "", 0, True, False)
                    DoEvents
                    NewSendUrno(idx).Text = ""
                    NewCallCode(idx).Text = ""
                Else
                    If TelexText = "" Then ErrMsg = "Can't send empty telexes!"
                    MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", ErrMsg, "stop", "", UserAnswer
                End If
            End If
        Next
        TlxRec = MyTlxTabRecord
        If TlxRec <> "" Then
            outFields = ""
            If SndTlxUrno <> "" Then
                outFields = "STAT,WSTA"
                outData = "P,S"
                tmpUrno = SndTlxUrno
            End If
            If outFields <> "" Then
                outSqlKey = "WHERE URNO=" & tmpUrno
                retCod = UfisServer.CallCeda(tmpResult, "URT", "TLXTAB", outFields, outData, outSqlKey, "", 0, True, False)
            End If
        End If
        CurrentRecord.Text = ""
        chkOutCnt(0).Value = 1
        Screen.MousePointer = 0
        DoEvents
        MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", "The telex is on the way now.", "hand", "", UserAnswer
        'Set CurFltTabOcx = TlxFlights(Index)
        'CurFltLineNo = LineNo
        'CurFltRecIsAft = False
        'CurFltRecIsTlx = True
        If CurFltRecIsTlx Then
            'Update Tlx Record locally
        End If
        chkSend.Value = 0
    Else
        If TelexIsCleanToSend Then
            chkText.Value = 0
            chkSend.BackColor = MyOwnButtonFace
            If MySitaOutType = "SITA_FILE" Then
                chkOutFormat.Value = 0
            Else
                chkPreView.Value = 0
            End If
            chkReset.Value = 1
        End If
    End If
End Sub

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    'Set MyParent = Caller
    'Set MyCallButton = CallButton
End Sub

Private Sub CreateFlightDataPanel(AftAdid As String, UseCfgList As String, UseCfgData As String, UseTplText As String)
    Dim TabList As String
    Dim CfgList As String
    Dim FldList As String
    Dim UseAftData As String
    Dim TypList As String
    Dim FldData As String
    Dim DspData As String
    Dim CurAdid As String
    Dim FldIdx As Integer
    Dim PaxIdx As Integer
    Dim itm As Long
    Dim FldItm As Integer
    Dim i As Integer
    Dim j As Integer
    Dim idx As Integer
    Dim FldLook As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim PaxType As String
    Dim FldLbl1 As String
    Dim FldLbl2 As String
    Dim FldRepl As String
    Dim tmpCapt As String
    Dim tmpName As String
    Dim tmpTpos As String
    Dim tmpTcnt As String
    Dim tmpTag As String
    Dim PicColor As String
    Dim CedaType As String
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim FldPos As Long
    Dim FldCnt As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim picBackColor As Long
    Dim IsNewPanel As Boolean
    
    UseAftData = UseCfgData
    CurTlxTplText = UseTplText
    txtTplText.Text = UseTplText
    FldPropTab(0).ZOrder
    FldPropTab(0).ResetContent
    FltEditPanel(0).Top = 15
    For i = 0 To FltEditPanel.UBound
        FltEditPanel(i).Visible = False
        chkFltTick(i).Tag = ""
        lblFltField(i).Tag = ""
        lblDatType(i).Tag = ""
        txtFltInput(i).Tag = ""
    Next
    
    FldList = UseCfgList
    CfgList = UseCfgList
    
    If FldList <> "" Then
        CheckTplFldListComplete FldList, UseAftData, CurTplFldList
        CfgList = FldList
    
        CalculateDelay
    End If
    
    i = 0
    itm = 0
    FldName = GetRealItem(CfgList, itm, ",")
    While FldName <> ""
        FldType = ""
        FldRepl = ""
        FldData = ""
        FldItm = GetItemNo(FldList, FldName)
        If FldItm > 0 Then
            If InStr(CurTabOcxFldLst, FldName) > 0 Then picBackColor = vbBlue Else picBackColor = vbBlack
            FldData = GetItem(UseAftData, FldItm, ",")
            FldLbl1 = FldName
            CedaType = FldName
            If InStr(CedaTimeFields, FldName) > 0 Then
                CedaType = "TIME"
            ElseIf InStr(TemplateDelayTypes, FldName) > 0 Then
                CedaType = FldName
                FldName = GetFieldValue(CedaType, TemplateDelayNames, TemplateDelayTypes)
            ElseIf InStr(TemplateDelayNames, FldName) > 0 Then
                CedaType = GetFieldValue(FldName, TemplateDelayTypes, TemplateDelayNames)
            ElseIf InStr(TemplatePaxNames, FldName) > 0 Then
                CedaType = GetFieldValue(FldName, TemplatePaxTypes, TemplatePaxNames)
            ElseIf FldName = "REGN" Then
                If FldData = "" Then
                    FldData = txtRegn.Text
                    picBackColor = vbMagenta
                End If
            ElseIf InStr("NHOP,NSTN", FldName) > 0 Then
                CedaType = "NSTN"
                If InStr(FldData, "?") > 0 Then FldData = ""
                If FldData = "" Then
                    FldData = GetNxtStation(FldList, UseAftData)
                End If
                picBackColor = vbCyan
            Else
                CedaType = "STRG"
            End If
            Select Case CedaType
                Case "TIME"
                    TypList = "DD,HHMM,DDHHMM"
                    FldType = "START"
                    j = 0
                    While FldType <> ""
                        j = j + 1
                        FldType = GetItem(TypList, j, ",")
                        If FldType <> "" Then
                            FldLbl2 = FldType
                            FldCnt = 0
                            FldPos = 1
                            While FldPos > 0
                                FldCnt = FldCnt + 1
                                tmpTcnt = Right("00" & CStr(FldCnt), 2)
                                FldLook = "[" & FldName & "|" & FldType & "]"
                                FldRepl = "[" & FldName & "|" & FldType & "|" & tmpTcnt & "]"
                                If TplMainFieldsOnly Then FldRepl = FldLook
                                FldPos = InStr(CurTlxTplText, FldLook)
                                If FldPos > 0 Then
                                    tmpTpos = Right("0000" & CStr(FldPos), 4)
                                    FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
                                    FldPropTab(0).InsertTextLine FldProp, False
                                    CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
                                End If
                                If TplMainFieldsOnly Then FldPos = 0
                            Wend
                        End If
                    Wend
                    FldType = "[SSIM]"
                    FldLbl2 = FldType
                    FldCnt = 0
                    FldPos = 1
                    While FldPos > 0
                        FldCnt = FldCnt + 1
                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
                        FldLook = "[" & FldName & "]"
                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
                        If TplMainFieldsOnly Then FldRepl = FldLook
                        FldPos = InStr(CurTlxTplText, FldLook)
                        If FldPos > 0 Then
                            tmpTpos = Right("0000" & CStr(FldPos), 4)
                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
                            FldPropTab(0).InsertTextLine FldProp, False
                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
                        End If
                        If TplMainFieldsOnly Then FldPos = 0
                    Wend
                Case "DCD1", "DCD2", "DTD1", "DTD2"
                    picBackColor = vbGreen
                    FldType = CedaType
                    Select Case CedaType
                        Case "DCD1"
                            FldData = AftFldText(15).Text
                        Case "DCD2"
                            FldData = AftFldText(16).Text
                        Case "DTD1"
                            FldData = AftFldText(17).Text
                        Case "DTD2"
                            FldData = AftFldText(18).Text
                        Case Else
                    End Select
                    FldLbl1 = FldName
                    FldLbl2 = ""
                    FldCnt = 0
                    FldPos = 1
                    While FldPos > 0
                        FldCnt = FldCnt + 1
                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
                        FldLook = "[" & FldName & "]"
                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
                        If TplMainFieldsOnly Then FldRepl = FldLook
                        FldPos = InStr(CurTlxTplText, FldLook)
                        If FldPos > 0 Then
                            tmpTpos = Right("0000" & CStr(FldPos), 4)
                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
                            FldPropTab(0).InsertTextLine FldProp, False
                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
                        End If
                        If TplMainFieldsOnly Then FldPos = 0
                    Wend
                Case "PAX1", "PAX2", "PAX3", "PAX4"
                    picBackColor = vbGreen
                    FldType = CedaType
                    PaxType = CedaType
                    FldIdx = GetPaxFieldIndex(3, CedaType, 1, FldName)
                    If FldIdx >= 0 Then
                        FldData = AftFldText(FldIdx).Text
                        tmpTag = AftFldPanel(FldIdx).Tag
                        tmpTag = GetItem(tmpTag, 2, "|")
                        PaxType = GetItem(tmpTag, 4, ",")
                    Else
                    End If
                    Select Case CedaType
                        Case "PAX1"
                            'FldData = AftFldText(42).Text
                        Case "PAX2"
                            'FldData = AftFldText(41).Text
                        Case "PAX3"
                            'FldData = AftFldText(40).Text
                        Case Else
                    End Select
                    FldType = PaxType
                    FldLbl1 = FldName
                    FldLbl2 = ""
                    FldCnt = 0
                    FldPos = 1
                    While FldPos > 0
                        FldCnt = FldCnt + 1
                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
                        FldLook = "[" & FldName & "]"
                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
                        If TplMainFieldsOnly Then FldRepl = FldLook
                        FldPos = InStr(CurTlxTplText, FldLook)
                        If FldPos > 0 Then
                            tmpTpos = Right("0000" & CStr(FldPos), 4)
                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
                            FldPropTab(0).InsertTextLine FldProp, False
                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
                        End If
                        If TplMainFieldsOnly Then FldPos = 0
                    Wend
                Case Else
                    FldType = FldName
                    FldLbl2 = ""
                    FldCnt = 0
                    FldPos = 1
                    While FldPos > 0
                        FldCnt = FldCnt + 1
                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
                        FldLook = "[" & FldName & "]"
                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
                        If TplMainFieldsOnly Then FldRepl = FldLook
                        FldPos = InStr(CurTlxTplText, FldLook)
                        If FldPos > 0 Then
                            tmpTpos = Right("0000" & CStr(FldPos), 4)
                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
                            FldPropTab(0).InsertTextLine FldProp, False
                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
                        End If
                        If TplMainFieldsOnly Then FldPos = 0
                    Wend
            End Select
        End If
        itm = itm + 1
        FldName = GetRealItem(CfgList, itm, ",")
    Wend
    
    MaxLine = FldPropTab(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        FldLook = FldPropTab(0).GetFieldValue(CurLine, "REPL")
        FldPos = InStr(CurTlxTplText, FldLook)
        tmpTpos = Right("0000" & CStr(FldPos), 4)
        FldPropTab(0).SetColumnValue CurLine, 0, tmpTpos
    Next
    FldPropTab(0).AutoSizeColumns
    FldPropTab(0).Sort "0", True, True
    
    If TplFldCreateEditor Then
        NewTop = FltEditPanel(0).Top
        NewLeft = FltEditPanel(0).Left
        i = 0
        If i = FltEditPanel.UBound Then
            FltEditPanel(i).AutoRedraw = True
            DrawBackGround FltEditPanel(i), MyLifeStyleValue, False, True
            FltEditPanel(i).Picture = FltEditPanel(i).Image
            FltEditPanel(i).Cls
            FltEditPanel(i).AutoRedraw = False
        End If
        i = 0
        itm = 0
        MaxLine = FldPropTab(0).GetLineCount - 1
        For CurLine = 0 To MaxLine
            FldName = FldPropTab(0).GetFieldValue(CurLine, "FINA")
            FldRepl = FldPropTab(0).GetFieldValue(CurLine, "REPL")
            FldType = FldPropTab(0).GetFieldValue(CurLine, "TYPE")
            FldLbl1 = FldPropTab(0).GetFieldValue(CurLine, "LBL1")
            FldLbl2 = FldPropTab(0).GetFieldValue(CurLine, "LBL2")
            FldData = FldPropTab(0).GetFieldValue(CurLine, "DATA")
            PicColor = FldPropTab(0).GetFieldValue(CurLine, "DSSC")
            picBackColor = Val(PicColor)
            IsNewPanel = False
            If i > FltEditPanel.UBound Then
                Load FltEditPanel(i)
                Load chkFltTick(i)
                Load lblFltField(i)
                Load lblDatType(i)
                Load txtFltInput(i)
                Load chkFltCheck(i)
                Load picFldRelIdx(i)
                Set FltEditPanel(i).Container = FltInputSlide
                Set chkFltTick(i).Container = FltEditPanel(i)
                Set lblFltField(i).Container = FltEditPanel(i)
                Set lblDatType(i).Container = FltEditPanel(i)
                Set txtFltInput(i).Container = FltEditPanel(i)
                Set chkFltCheck(i).Container = FltEditPanel(i)
                Set picFldRelIdx(i).Container = FltEditPanel(i)
                IsNewPanel = True
            End If
            Select Case FldType
                Case "FLNO"
                    DspData = Replace(FldData, " ", "", 1, -1, vbBinaryCompare)
                    'picBackColor = vbBlue
                Case "DD"
                    DspData = Mid(FldData, 7, 2)
                Case "HHMM"
                    'picBackColor = vbBlue
                    DspData = Mid(FldData, 9, 4)
                Case "DDHHMM"
                    'picBackColor = vbBlue
                    DspData = Mid(FldData, 7, 6)
                Case "[SSIM]"
                    'picBackColor = vbBlue
                    DspData = DecodeSsimDayFormat(Left(FldData, 8), "CEDA", "SSIM2")
                    DspData = DspData & ":" & Mid(FldData, 9, 4)
                Case "DCD1", "DCD2"
                    picBackColor = vbGreen
                    DspData = FldData
                    If FldType = "DCD1" Then idx = 1 Else idx = 2
                    tmpTag = chkEditDL(idx).Tag
                    If TplUpdateFldGroups Then
                        picBackColor = vbRed
                        txtEditDL(idx).Text = DspData
                        tmpTag = tmpTag & CStr(i) & ","
                        chkFltTick(i).Tag = CStr(idx)
                    ElseIf tmpTag = "" Then
                        txtEditDL(idx).Text = DspData
                        tmpTag = tmpTag & CStr(i)
                        chkFltTick(i).Tag = CStr(idx)
                    End If
                    chkEditDL(idx).Tag = tmpTag
                Case "DTD1", "DTD2"
                    picBackColor = vbGreen
                    DspData = Right("0000" & FldData, 4)
                    If DspData = "0000" Then DspData = ""
                    If FldType = "DTD1" Then idx = 3 Else idx = 4
                    tmpTag = chkEditDL(idx).Tag
                    If TplUpdateFldGroups Then
                        picBackColor = vbRed
                        txtEditDL(idx).Text = DspData
                        tmpTag = tmpTag & CStr(i) & ","
                        chkFltTick(i).Tag = CStr(idx)
                    ElseIf tmpTag = "" Then
                        txtEditDL(idx).Text = DspData
                        tmpTag = tmpTag & CStr(i)
                        chkFltTick(i).Tag = CStr(idx)
                    End If
                    chkEditDL(idx).Tag = tmpTag
                Case "PAX1", "PAX2", "PAX3", "PAX4"
                    picBackColor = vbGreen
                    DspData = FldData
                    idx = Val(Right(FldType, 1))
                    tmpTag = chkEditPX(idx).Tag
                    If TplUpdateFldGroups Then
                        picBackColor = vbRed
                        txtEditPX(idx).Text = DspData
                        tmpTag = tmpTag & CStr(i) & ","
                        chkFltTick(i).Tag = CStr(idx)
                    ElseIf tmpTag = "" Then
                        txtEditPX(idx).Text = DspData
                        tmpTag = tmpTag & CStr(i)
                        chkFltTick(i).Tag = CStr(idx)
                    End If
                    chkEditPX(idx).Tag = tmpTag
                Case Else
                    DspData = FldData
            End Select
            
            FltEditPanel(i).Tag = FldName
            
            lblFltField(i).Caption = FldLbl1
            lblFltField(i).Tag = FldRepl
            
            If FldLbl2 = "" Then FldLbl2 = ". . . . . . . ."
            lblDatType(i).Caption = FldLbl2
            lblDatType(i).Tag = FldType
            
            txtFltInput(i).Text = DspData
            txtFltInput(i).Tag = FldData
            
            chkFltCheck(i).Tag = DspData
            
            txtFltInput(i).BackColor = LightGrey
            picFldRelIdx(i).BackColor = picBackColor
            chkFltTick(i).Value = 0
            chkFltTick(i).Visible = True
            chkFltCheck(i).Visible = True
            lblFltField(i).Visible = True
            lblDatType(i).Visible = True
            txtFltInput(i).Visible = True
            If ShowFldDsscColFlag Then picFldRelIdx(i).Visible = True Else picFldRelIdx(i).Visible = False
            FltEditPanel(i).Top = NewTop
            FltEditPanel(i).Left = NewLeft
            FltEditPanel(i).Visible = True
            If (DspData = "") Or (InStr(DspData, "?") > 0) Then
                chkFltTick(i).Value = 1
            End If
            'HardCoded HotFix
            If FldName = "EETA" Then
                AftFldLabel(9).Tag = CStr(i)
            End If
            'DoEvents
            NewTop = NewTop + FltEditPanel(i).Height
            i = i + 1
        Next
    End If
    CalculateDelay
    CalculateEans
    
    If Trim(txtSiText(0).Text) = "" Then
        txtSiText(0).Visible = False
        fraSiTxtLabel(0).Visible = False
        fraSiTxtLabel(1).Visible = False
        'txtTelexText(0).Height = fraTelexText(0).Height - txtTelexText(0).Top - 90
    Else
        txtSiText(0).Visible = True
        fraSiTxtLabel(0).Visible = True
        fraSiTxtLabel(1).Visible = True
    End If
    NewTop = NewTop + 30
    FltInputSlide.Height = NewTop
    VScroll1(0).Max = NewTop / 15
    ResizeSubPanels 1
    ResizeSubPanels 2
End Sub
Private Sub CheckTplFldListComplete(AftFldList As String, AftDatList As String, TplFldList As String)
    Dim tmpItem As String
    Dim tmpName As String
    Dim ItmSep As String
    Dim FldLen As Integer
    Dim itm As Integer
    TemplateHasDelayLine = False
    TemplateHasPaxLine = False
    ItmSep = Chr(25)
    tmpItem = "START"
    itm = 0
    While tmpItem <> ""
        itm = itm + 1
        tmpItem = GetItem(TplFldList, itm, ItmSep)
        If tmpItem <> "" Then
            tmpName = Mid(tmpItem, 2, 4)
            Select Case tmpName
                Case "DCD1", "DCD2", "DTD1", "DTD2", "DCO1", "DCO2", "DUR1", "DUR2"
                    TemplateHasDelayLine = True
                Case "PXIF", "PXEF", "PXIN"
                    TemplateHasPaxLine = True
                Case Else
            End Select
            If InStr(AftFldList, tmpName) = 0 Then
                FldLen = GetTplFieldLength(tmpName, -1)
                AftFldList = AftFldList & "," & tmpName
                AftDatList = AftDatList & "," & Left("??????????", FldLen)
            End If
        End If
    Wend
End Sub
Private Function GetTlxTextFromTemplate(CurTlxTplText As String, TextIsClean As Boolean) As String
    Dim NewText As String
    Dim UsrText As String
    Dim FldName As String
    Dim FldData As String
    Dim FldText As String
    Dim TplText As String
    Dim TplLook As String
    Dim TlxType As String
    Dim LineNo As Long
    Dim FldLen As Integer
    Dim i As Integer
    TextIsClean = True
    If TplFldCreateEditor Then
        NewText = CurTlxTplText
        UsrText = CurTlxTplText
        For i = 0 To txtFltInput.UBound
            TplLook = lblFltField(i).Tag
            If TplLook <> "" Then
                FldText = txtFltInput(i).Text
                TplText = FldText
                If FldText = "" Then
                    'Check Mandatory Flags
                    FldLen = GetTplFieldLength("", i)
                    TplText = Left("^^^^^^^^^^", FldLen)
                End If
                'If TplMainFieldsOnly Then
                    'In fact there is no difference
                    UsrText = Replace(UsrText, TplLook, TplText, 1, -1, vbBinaryCompare)
                    If FldText <> "" Then
                        NewText = Replace(NewText, TplLook, TplText, 1, -1, vbBinaryCompare)
                    End If
                'Else
                '    UsrText = Replace(UsrText, TplLook, TplText, 1, -1, vbBinaryCompare)
                '    If FldText <> "" Then
                '        NewText = Replace(NewText, TplLook, TplText, 1, -1, vbBinaryCompare)
                '    End If
                'End If
            End If
        Next
    Else
        UsrText = GetTlxTextFromDataGrid(CurTlxTplText, NewText, TextIsClean)
    End If
    LineNo = TlxTypeTab.GetCurrentSelected
    If LineNo >= 0 Then
        TlxType = TlxTypeTab.GetColumnValue(LineNo, 0)
        If TlxType = "COR" Then
            If UsrText <> "" Then UsrText = "COR" & vbNewLine & UsrText
            If NewText <> "" Then NewText = "COR" & vbNewLine & NewText
        End If
    End If
    If (FlightIsOnTime) And (TemplateHasDelayLine) Then UsrText = RemoveDelayLine(UsrText, "TEXT")
    UsrText = RemovePaxLine(UsrText, "TEXT")
    CurNewTplText = NewText
    CurUsrTplText = UsrText
    If InStr(UsrText, "??") > 0 Then TextIsClean = False
    If InStr(UsrText, "^^") > 0 Then TextIsClean = False
    GetTlxTextFromTemplate = UsrText
End Function
Private Function GetTlxTextFromDataGrid(CurTlxTplText As String, NewText As String, TextIsClean As Boolean) As String
    Dim FldLook As String
    Dim FldProp As String
    Dim FldType As String
    Dim FldLbl1 As String
    Dim FldLbl2 As String
    Dim FldRepl As String
    Dim tmpCapt As String
    Dim tmpName As String
    Dim tmpTpos As String
    Dim tmpTcnt As String
    Dim CedaType As String
    Dim UsrText As String
    Dim FldName As String
    Dim FldData As String
    Dim FldText As String
    Dim DspData As String
    Dim TplText As String
    Dim TplLook As String
    Dim FldLen As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    TextIsClean = True
    NewText = CurTlxTplText
    UsrText = CurTlxTplText
    i = 0
    MaxLine = FldPropTab(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        FldName = FldPropTab(0).GetFieldValue(CurLine, "FINA")
        FldRepl = FldPropTab(0).GetFieldValue(CurLine, "REPL")
        FldType = FldPropTab(0).GetFieldValue(CurLine, "TYPE")
        FldLbl1 = FldPropTab(0).GetFieldValue(CurLine, "LBL1")
        FldLbl2 = FldPropTab(0).GetFieldValue(CurLine, "LBL2")
        FldData = FldPropTab(0).GetFieldValue(CurLine, "DATA")
        Select Case FldType
            Case "FLNO"
                DspData = Replace(FldData, " ", "", 1, -1, vbBinaryCompare)
            Case "DD"
                DspData = Mid(FldData, 7, 2)
            Case "HHMM"
                DspData = Mid(FldData, 9, 4)
            Case "DDHHMM"
                DspData = Mid(FldData, 7, 6)
            Case "[SSIM]"
                DspData = DecodeSsimDayFormat(Left(FldData, 8), "CEDA", "SSIM2")
                DspData = DspData & ":" & Mid(FldData, 9, 4)
            Case "DCD1", "DCD2"
                DspData = FldData
            Case "DTD1", "DTD2"
                DspData = Right("0000" & FldData, 4)
                If DspData = "0000" Then DspData = ""
            Case "PAX1", "PAX2", "PAX3", "PAX4"
                DspData = FldData
            Case Else
                DspData = FldData
        End Select
        FldText = DspData
        TplText = FldText
        TplLook = FldRepl
        If FldText = "" Then
            'Check Mandatory Flags
            FldLen = 2
            'FldLen = GetTplFieldLength("", i)
            TplText = Left("^^^^^^^^^^", FldLen)
        End If
        UsrText = Replace(UsrText, TplLook, TplText, 1, -1, vbBinaryCompare)
        If FldText <> "" Then
            NewText = Replace(NewText, TplLook, TplText, 1, -1, vbBinaryCompare)
        End If
    Next
    GetTlxTextFromDataGrid = UsrText
End Function

Private Sub InitPosSize(CheckOnline As Boolean)
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim WinCnt As Long
    Dim NewSize As Long
    Dim MaxSize As Long
    Dim WinSize As Long
    Dim DlgSize As Long
    Dim SubSize As Long
    On Error Resume Next
    If CheckOnline = True Then
        If Me.WindowState <> vbNormal Then Me.WindowState = vbNormal
        DlgSize = 0
        SubSize = 0
        Select Case LastResizeName
            Case Me.Name
                DlgSize = Me.Width
            Case "RcvTelex"
                If FormIsVisible("RcvTelex") Then SubSize = RcvTelex.Width
            Case "SndTelex"
                If FormIsVisible("SndTelex") Then SubSize = SndTelex.Width
            Case "PndTelex"
                If FormIsVisible("PndTelex") Then SubSize = PndTelex.Width
            Case "OutTelex"
                If FormIsVisible("OutTelex") Then SubSize = OutTelex.Width
            Case "RcvInfo"
                If FormIsVisible("RcvInfo") Then SubSize = RcvInfo.Width
            Case Else
        End Select
        WinCnt = 0
        If FormIsVisible("RcvTelex") Then
            WinCnt = WinCnt + 1
            If RcvTelex.Width > WinSize Then WinSize = RcvTelex.Width
        End If
        If FormIsVisible("SndTelex") Then
            WinCnt = WinCnt + 1
            If SndTelex.Width > WinSize Then WinSize = SndTelex.Width
        End If
        If FormIsVisible("PndTelex") Then
            WinCnt = WinCnt + 1
            If PndTelex.Width > WinSize Then WinSize = PndTelex.Width
        End If
        If FormIsVisible("OutTelex") Then
            WinCnt = WinCnt + 1
            If OutTelex.Width > WinSize Then WinSize = OutTelex.Width
        End If
        If FormIsVisible("RcvInfo") Then
            WinCnt = WinCnt + 1
            If RcvInfo.Width > WinSize Then WinSize = RcvInfo.Width
        End If
        If WinCnt > 0 Then
            If SubSize > 0 Then WinSize = SubSize
            If DlgSize > 0 Then
                MaxSize = MaxScreenWidth - Me.Left
                If MaxSize > WinSize Then MaxSize = WinSize
                WinSize = MaxSize
                If WinSize < 600 Then WinSize = 600
            End If
            NewHeight = Me.Height
            MaxLeft = MaxScreenWidth - WinSize
            NewLeft = Me.Left + Me.Width
            If NewLeft > MaxLeft Then
                NewLeft = MaxLeft
                NewSize = NewLeft - Me.Left
                If NewSize > 1500 Then
                    If Me.WindowState = vbNormal Then Me.Width = NewSize
                End If
            End If
            NewHeight = Me.Height
            NewLeft = Me.Left + Me.Width
            NewTop = Me.Top
            NewHeight = NewHeight / WinCnt
            If FormIsVisible("RcvInfo") Then
                RcvInfo.WindowState = vbNormal
                RcvInfo.Show
                RcvInfo.Top = NewTop
                RcvInfo.Left = NewLeft
                RcvInfo.Height = NewHeight
                RcvInfo.Width = WinSize
                RcvInfo.OnTop.Value = Me.OnTop.Value
                RcvInfo.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("RcvTelex") Then
                RcvTelex.WindowState = vbNormal
                RcvTelex.Show
                RcvTelex.Top = NewTop
                RcvTelex.Left = NewLeft
                RcvTelex.Height = NewHeight
                RcvTelex.Width = WinSize
                RcvTelex.OnTop.Value = Me.OnTop.Value
                RcvTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("SndTelex") Then
                SndTelex.WindowState = vbNormal
                SndTelex.Show
                SndTelex.Top = NewTop
                SndTelex.Left = NewLeft
                SndTelex.Height = NewHeight
                SndTelex.Width = WinSize
                SndTelex.OnTop.Value = Me.OnTop.Value
                SndTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("PndTelex") Then
                PndTelex.WindowState = vbNormal
                PndTelex.Show
                PndTelex.Top = NewTop
                PndTelex.Left = NewLeft
                PndTelex.Height = NewHeight
                PndTelex.Width = WinSize
                PndTelex.OnTop.Value = Me.OnTop.Value
                PndTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("OutTelex") Then
                OutTelex.WindowState = vbNormal
                OutTelex.Show
                OutTelex.Top = NewTop
                OutTelex.Left = NewLeft
                OutTelex.Height = NewHeight
                OutTelex.Width = WinSize
                OutTelex.OnTop.Value = Me.OnTop.Value
                OutTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
        End If
        'If TelexPoolHead.WindowState = vbMinimized Then
        '    If Not TelexPoolHead.Visible Then
        '        MainOpenedByUser = True
        '        TelexPoolHead.Show
        '        Me.SetFocus
        '    End If
        '    TelexPoolHead.WindowState = vbNormal
        '    TelexPoolHead.Hide
        'End If
        'TelexPoolHead.Top = Me.Top
        'TelexPoolHead.Left = Me.Left
        'TelexPoolHead.Width = Me.Width
        'TelexPoolHead.Height = Me.Height
        If Me.Visible = True Then
            'Me.SetFocus
            'Me.Refresh
        End If
    Else
        If GeneratorIsReadyForUse Then
        End If
        'Me.Left = 300
        'Me.Top = 650
        'Me.Height = MaxScreenHeight - Me.Top - 300
        'Me.Width = fraStatusFilter.Left + fraStatusFilter.Width + RightCover.Width + 150
    End If
End Sub

Private Sub VScroll1_Change(Index As Integer)
    AdjustScrollPos Index
End Sub
Private Sub AdjustScrollPos(Index As Integer)
    Dim NewTop As Long
    VScroll1(Index).Refresh
    NewTop = CLng(VScroll1(Index).Value) * 15
    FltInputSlide.Top = -NewTop
End Sub

Private Sub VScroll1_Scroll(Index As Integer)
    AdjustScrollPos Index
End Sub

Private Sub ArrangeTopPanels()
    Dim tmpTag As String
    Dim tmpLen As String
    Dim NewSize As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MaxTypeSize As Long
    Dim MaxPrioSize As Long
    Dim MaxSignSize As Long
    Dim MinTypeSize As Long
    Dim MinPrioSize As Long
    Dim MinSignSize As Long
    Dim NewTypeSize As Long
    Dim NewPrioSize As Long
    Dim NewSignSize As Long
    Dim MaxTypeLeft As Long
    Dim MaxPrioLeft As Long
    Dim MaxSignLeft As Long
    Dim MinTypeLeft As Long
    Dim MinPrioLeft As Long
    Dim MinSignLeft As Long
    Dim NewTypeLeft As Long
    Dim NewPrioLeft As Long
    Dim NewSignLeft As Long
    Dim MinPrevLeft As Long
    Dim MinPrevSize As Long
    Dim MaxPrevLeft As Long
    Dim MaxPrevSize As Long
    Dim CurRightPos As Long
    
    Exit Sub
    
    MaxPrevLeft = fraFlightPanel(0).Left + fraFlightPanel(0).Width
    MinPrevLeft = fraFlightPanel(0).Left + fraFlightPanel(0).Width
    
    If TlxTypeTab.Tag <> "" Then
        If fraTlxType.Visible Then
            MaxTypeLeft = fraFlightPanel(0).Left + fraFlightPanel(0).Width + 60
            tmpTag = TlxTypeTab.Tag
            MaxTypeSize = Val(GetItem(tmpTag, 1, ","))
            tmpLen = GetItem(TlxTypeTab.HeaderLengthString, 1, ",")
            MinTypeSize = TlxTypeTab.Left + ((Val(tmpLen) + 1) * 15) + TlxTypeTab.Left
            tmpLen = GetItem(TlxTypeTab.HeaderLengthString, 2, ",")
            MinTypeSize = MinTypeSize + ((Val(tmpLen) + 1) * 15)
            If TlxTypeTab.GetLineCount > 3 Then MinTypeSize = MinTypeSize + (14 * 15)   'ScrollBar
            'MinTypeSize = Val(fraTopButtons(1).Tag)
            MinTypeLeft = MaxTypeLeft
            MaxPrevLeft = MaxTypeLeft
            MinPrevLeft = MinTypeLeft
            MaxPrevSize = MaxTypeSize
            MinPrevSize = MinTypeSize
        End If
        
        If fraTlxPrio.Visible Then
            MaxPrioLeft = MaxPrevLeft + MaxPrevSize + 60
            tmpTag = TlxPrioTab.Tag
            MaxPrioSize = Val(GetItem(tmpTag, 1, ","))
            tmpLen = GetItem(TlxPrioTab.HeaderLengthString, 1, ",")
            MinPrioSize = TlxPrioTab.Left + ((Val(tmpLen) + 2) * 15) + TlxPrioTab.Left
            If TlxPrioTab.GetLineCount > 3 Then MinPrioSize = MinPrioSize + (14 * 15)   'ScrollBar
            MinPrioLeft = MinPrevLeft + MinPrevSize + 60
            MaxPrevLeft = MaxPrioLeft
            MinPrevLeft = MinPrioLeft
            MaxPrevSize = MaxPrioSize
            MinPrevSize = MinPrioSize
        End If
        
        If fraDblSign.Visible Then
            MaxSignLeft = MaxPrevLeft + MaxPrevSize + 60
            tmpTag = DblSignTab.Tag
            MaxSignSize = Val(GetItem(tmpTag, 1, ","))
            tmpLen = GetItem(DblSignTab.HeaderLengthString, 1, ",")
            MinSignSize = DblSignTab.Left + ((Val(tmpLen) + 2) * 15) + DblSignTab.Left
            If DblSignTab.GetLineCount > 3 Then MinSignSize = MinSignSize + (14 * 15)   'ScrollBar
            MinSignLeft = MinPrevLeft + MinPrevSize + 60
            MaxPrevLeft = MaxSignLeft
            MinPrevLeft = MinSignLeft
            MaxPrevSize = MaxSignSize
            MinPrevSize = MinSignSize
        End If
        
        If fraTlxType.Visible Then NewTypeSize = MaxTypeSize
        If fraTlxPrio.Visible Then NewPrioSize = MaxPrioSize
        If fraDblSign.Visible Then NewSignSize = MaxSignSize
        
        If fraTlxType.Visible Then NewTypeLeft = MaxTypeLeft
        If fraTlxPrio.Visible Then NewPrioLeft = MaxPrioLeft
        If fraDblSign.Visible Then NewSignLeft = MaxSignLeft
                
        CurRightPos = Me.ScaleWidth - 45
        If fraDblSign.Visible Then
            NewSignLeft = CurRightPos - MaxSignSize
            If NewSignLeft < MaxSignLeft Then
                NewSignSize = MaxSignSize - (MaxSignLeft - NewSignLeft)
                If NewSignSize < MinSignSize Then NewSignSize = MinSignSize
                NewSignLeft = CurRightPos - NewSignSize
            End If
            If NewSignLeft < MinSignLeft Then NewSignLeft = MinSignLeft
            If NewSignLeft > MaxSignLeft Then NewSignLeft = MaxSignLeft
            CurRightPos = NewSignLeft - 60
        End If
        
        If fraTlxPrio.Visible Then
            NewPrioLeft = CurRightPos - MaxPrioSize
            If NewPrioLeft < MaxPrioLeft Then
                NewPrioSize = MaxPrioSize - (MaxPrioLeft - NewPrioLeft)
                If NewPrioSize < MinPrioSize Then NewPrioSize = MinPrioSize
                NewPrioLeft = CurRightPos - NewPrioSize
            End If
            If NewPrioLeft < MinPrioLeft Then NewPrioLeft = MinPrioLeft
            If NewPrioLeft > MaxPrioLeft Then NewPrioLeft = MaxPrioLeft
            CurRightPos = NewPrioLeft - 60
        End If
                
        If fraTlxType.Visible Then
            NewTypeLeft = CurRightPos - MaxTypeSize
            If NewTypeLeft < MaxTypeLeft Then
                NewTypeSize = MaxTypeSize - (MaxTypeLeft - NewTypeLeft)
                If NewTypeSize < MinTypeSize Then NewTypeSize = MinTypeSize
                NewTypeLeft = CurRightPos - NewTypeSize
            End If
            If NewTypeLeft < MinTypeLeft Then NewTypeLeft = MinTypeLeft
            If NewTypeLeft > MaxTypeLeft Then NewTypeLeft = MaxTypeLeft
        End If
                
        If fraTlxType.Visible Then
            fraTlxType.Left = NewTypeLeft
            fraTlxType.Width = NewTypeSize
            TlxTypeTab.Width = NewTypeSize - 180
        End If
        
        If fraTlxPrio.Visible Then
            fraTlxPrio.Left = NewPrioLeft
            fraTlxPrio.Width = NewPrioSize
            TlxPrioTab.Width = NewPrioSize - 180
        End If
        
        If fraDblSign.Visible Then
            fraDblSign.Left = NewSignLeft
            fraDblSign.Width = NewSignSize
            DblSignTab.Width = NewSignSize - 180
        End If
        
        'fraTopButtons(1).Width = fraTlxType.Width
        
        'fraTlxType.ZOrder
        'fraTlxPrio.ZOrder
        'fraDblSign.ZOrder
    End If
End Sub
Private Sub LoadFlightData()
    Dim tmpFrom As String
    Dim tmpTo As String
    Dim tmpTime As String
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim tmpAlcList As String
    Dim UseVpfrVal As String
    Dim UseVpToVal As String
    Dim tmpStrg As String
    Dim tmpTip As String
    Dim ValOffsVpfr As Long
    Dim ValOffsVpto As Long
    Dim LinCnt As Long
    Dim CurSrvTime
    Dim i As Integer
    AftTimer.Enabled = False
    HiddenMain.SetStatusText 1, UCase("TELEX GENERATOR")
    Me.MousePointer = 11
    i = 0
    UseVpfrVal = AftVpfr(i).Text
    UseVpToVal = AftVpto(i).Text
    ValOffsVpfr = Abs(Val(UseVpfrVal))
    ValOffsVpto = Abs(Val(UseVpToVal))
    tmpTip = "ARR=## [FROM " & UseVpfrVal & " TO " & UseVpToVal & "]   "
    HiddenMain.SetStatusText 2, UCase("LOADING ARRIVAL FLIGHTS")
    TabFlights(i).ResetContent
    TabFlights(i).Refresh
    tmpAlcList = UfisServer.BasicData(3).SelectDistinct("1", "'", "'", ",", True)
    tmpAlcList = Replace(tmpAlcList, "''", "", 1, -1, vbBinaryCompare)
    tmpAlcList = Replace(tmpAlcList, ",,", ",", 1, -1, vbBinaryCompare)
    If Left(tmpAlcList, 1) = "," Then tmpAlcList = Mid(tmpAlcList, 2)
    If Right(tmpAlcList, 1) = "," Then tmpAlcList = Left(tmpAlcList, Len(tmpAlcList) - 1)
    tmpFields = TabFlights(i).LogicalFieldList
    tmpTime = MySetUp.GetUtcServerTime
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", -ValOffsVpfr, CurSrvTime)
    tmpFrom = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", ValOffsVpto, CurSrvTime)
    tmpTo = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    tmpSqlKey = "WHERE (TIFA BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "') "
    If tmpAlcList <> "" Then
        tmpSqlKey = tmpSqlKey & "AND (ALC2 IN (" & tmpAlcList & ")) "
    End If
    tmpSqlKey = tmpSqlKey & "AND ADID='A' "
    tmpSqlKey = tmpSqlKey & "AND FTYP IN ('O','S') "
    TabFlights(i).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    TabFlights(i).CedaHopo = UfisServer.HOPO
    TabFlights(i).CedaIdentifier = "IDX"
    TabFlights(i).CedaPort = "3357"
    TabFlights(i).CedaReceiveTimeout = "250"
    TabFlights(i).CedaRecordSeparator = vbLf
    TabFlights(i).CedaSendTimeout = "250"
    TabFlights(i).CedaServerName = UfisServer.HostName
    TabFlights(i).CedaTabext = UfisServer.TblExt
    TabFlights(i).CedaUser = gsUserName
    TabFlights(i).CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        TabFlights(i).CedaAction "GFR", "AFTTAB", tmpFields, "", tmpSqlKey
    End If
    TabFlights(i).Sort "1", True, True
    TabFlights(i).AutoSizeColumns
    
    i = 1
    UseVpfrVal = AftVpfr(i).Text
    UseVpToVal = AftVpto(i).Text
    ValOffsVpfr = Abs(Val(UseVpfrVal))
    ValOffsVpto = Abs(Val(UseVpToVal))
    tmpTip = tmpTip & "DEP=## [FROM " & UseVpfrVal & " TO " & UseVpToVal & "] "
    HiddenMain.SetStatusText 2, UCase("LOADING DEPARTUE FLIGHTS")
    TabFlights(i).ResetContent
    TabFlights(i).Refresh
    tmpFields = TabFlights(i).LogicalFieldList
    tmpTime = MySetUp.GetUtcServerTime
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", -ValOffsVpfr, CurSrvTime)
    tmpFrom = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", ValOffsVpto, CurSrvTime)
    tmpTo = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    tmpSqlKey = "WHERE (TIFD BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "') "
    If tmpAlcList <> "" Then
        tmpSqlKey = tmpSqlKey & "AND (ALC2 IN (" & tmpAlcList & ")) "
    End If
    tmpSqlKey = tmpSqlKey & "AND ADID='D' "
    tmpSqlKey = tmpSqlKey & "AND FTYP IN ('O','S') "
    TabFlights(i).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    TabFlights(i).CedaHopo = UfisServer.HOPO
    TabFlights(i).CedaIdentifier = "IDX"
    TabFlights(i).CedaPort = "3357"
    TabFlights(i).CedaReceiveTimeout = "250"
    TabFlights(i).CedaRecordSeparator = vbLf
    TabFlights(i).CedaSendTimeout = "250"
    TabFlights(i).CedaServerName = UfisServer.HostName
    TabFlights(i).CedaTabext = UfisServer.TblExt
    TabFlights(i).CedaUser = gsUserName
    TabFlights(i).CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        TabFlights(i).CedaAction "GFR", "AFTTAB", tmpFields, "", tmpSqlKey
    End If
    TabFlights(i).Sort "1", True, True
    TabFlights(i).Sort "12", True, True
    TabFlights(i).AutoSizeColumns
    
    LinCnt = TabFlights(0).GetLineCount
    tmpStrg = CStr(LinCnt)
    tmpTip = Replace(tmpTip, "##", tmpStrg, 1, 1, vbBinaryCompare)
    LinCnt = TabFlights(1).GetLineCount
    tmpStrg = CStr(LinCnt)
    tmpTip = Replace(tmpTip, "##", tmpStrg, 1, 1, vbBinaryCompare)
    chkFltData(0).ToolTipText = tmpTip
    
    AftTimer.Tag = "5"
    'AftTimer.Enabled = True
    AftTimer.Enabled = False
    Me.MousePointer = 0
End Sub

Private Sub SwitchMainViewPoint(CalledFrom As Integer, SetView As String)
    Dim SetViewData As String
    Dim AftFldList As String
    Dim AftFldData As String
    Dim TlxRecData As String
    Dim TlxFldList As String
    Dim TlxFldData As String
    Dim CurAdid As String
    Dim LineNo As Long
    Dim SetDlgEnv As Integer
    Dim SetFltArr As Boolean
    Dim SetFltDep As Boolean
    'Global Variables
    'Dim CurViewPoint As String
    'Dim CurAftFltUrno As String
    'Dim CurTlxFltUrno As String
    'Dim CurTlxTabUrno As String
    'Dim CurTlxTplUrno As String
    Select Case SetView
        Case "TLX_TXT"
            SetFltArr = False
            SetFltDep = False
            SetDlgEnv = 1
        Case "ANY_ARR"
            SetFltArr = True
            SetFltDep = False
            SetDlgEnv = 2
        Case "ANY_DEP"
            SetFltArr = False
            SetFltDep = True
            SetDlgEnv = 3
        Case "AFT_ARR"
            SetFltArr = True
            SetFltDep = False
            SetDlgEnv = 2
        Case "AFT_DEP"
            SetFltArr = False
            SetFltDep = True
            SetDlgEnv = 3
        Case "TLX_ARR"
            SetFltArr = True
            SetFltDep = False
            SetDlgEnv = 2
        Case "TLX_DEP"
            SetFltArr = False
            SetFltDep = True
            SetDlgEnv = 3
        Case Else
            SetDlgEnv = 1
    End Select
    'Check if we must first reset the view
    Select Case CalledFrom
        Case 1  'Main Function
            Me.Caption = Me.Tag & ""
        Case 2  'Buttons chkView(..)
            Me.Caption = Me.Tag & ""
        Case 3  'Tab.ocx TabFlights(..)
            Me.Caption = Me.Tag & " [Selected Flight]"
        Case 4  'Tab.ocx TLxFlights(..)
            Me.Caption = Me.Tag & " [Selected Auto MVT]"
        Case Else
            Me.Caption = Me.Tag & ""
    End Select
    Select Case SetDlgEnv
        Case 1  'Switch View to Telex
            If Not KeepCurrentView Then
                If chkView(1).Value = 1 Then
                    chkView(1).ForeColor = vbBlue
                End If
                'If chkView(2).Value = 1 Then
                    'chkView(2).ForeColor = vbBlue
                'End If
                chkView(0).Value = 1
                'chkView(1).Value = 0
                'chkView(2).Value = 0
                'chkView(4).Value = 0
                'chkView(4).Enabled = True
                If chkView(3).Value = 1 Then
                    chkView(3).Value = 0
                    chkView(3).Value = 1
                End If
                FltInputPanel.Visible = True
                VScroll1(0).Visible = True
                'chkView(3).Enabled = True
            End If
        Case 2  'Switch View to Arrivals
            chkView(1).ForeColor = vbBlack
            chkView(1).Value = 1
            'chkView(0).Value = 0
            chkView(2).Value = 0
            'chkView(4).Value = 0
            'chkView(4).Enabled = False
            If chkView(3).Value = 1 Then
                chkView(3).Value = 0
                chkView(3).Value = 1
            End If
            chkAddData(0).Value = 1
            TlxFlights(0).Visible = True
            TabFlights(0).Visible = True
            TlxFlights(1).Visible = False
            TabFlights(1).Visible = False
            chkView(3).Enabled = True
        Case 3  'Switch View to Departures
            chkView(2).ForeColor = vbBlack
            chkView(2).Value = 1
            'chkView(0).Value = 0
            chkView(1).Value = 0
            'chkView(4).Value = 0
            'chkView(4).Enabled = False
            If chkView(3).Value = 1 Then
                chkView(3).Value = 0
                chkView(3).Value = 1
            End If
            chkAddData(1).Value = 1
            TlxFlights(1).Visible = True
            TabFlights(1).Visible = True
            TlxFlights(0).Visible = False
            TabFlights(0).Visible = False
            chkView(3).Enabled = True
        Case Else
    End Select
    Select Case SetView
        Case "ANY_ARR"
            FltListPanel(0).Visible = True
            'fraFlightCover(0).Visible = True
            'fraFlightPanel(2).Visible = True
            If CurAftFltUrno = "" Then InitFlightInfoPanel 2, "", ""
            If CurFltAdidIsArr Then TplListTab.Visible = True Else TplListTab.Visible = False
        Case "ANY_DEP"
            FltListPanel(0).Visible = True
            'fraFlightCover(0).Visible = True
            'fraFlightPanel(2).Visible = True
            If CurAftFltUrno = "" Then InitFlightInfoPanel 2, "", ""
            If CurFltAdidIsDep Then TplListTab.Visible = True Else TplListTab.Visible = False
        Case "AFT_ARR"
            FltListPanel(0).Visible = True
            LineNo = TabFlights(0).GetCurrentSelected
            If LineNo >= 0 Then
                AftFldList = TabFlights(0).LogicalFieldList
                AftFldData = TabFlights(0).GetLineValues(LineNo)
                InitFlightInfoPanel 2, AftFldList, AftFldData
                TlxRecData = ""
                TlxFldList = AftFldList
                TlxFldData = AftFldData
                KeepCurrentView = True
                SetCreatedTelex 2, TlxFldList, TlxFldData
                KeepCurrentView = False
            End If
            InitTplListTab "A"
        Case "AFT_DEP"
            FltListPanel(0).Visible = True
            LineNo = TabFlights(1).GetCurrentSelected
            If LineNo >= 0 Then
                AftFldList = TabFlights(1).LogicalFieldList
                AftFldData = TabFlights(1).GetLineValues(LineNo)
                InitFlightInfoPanel 2, AftFldList, AftFldData
                TlxRecData = ""
                TlxFldList = AftFldList
                TlxFldData = AftFldData
                KeepCurrentView = True
                SetCreatedTelex 2, TlxFldList, TlxFldData
                KeepCurrentView = False
            End If
            InitTplListTab "D"
        Case "TLX_ARR"
            FltListPanel(0).Visible = True
            LineNo = TlxFlights(0).GetCurrentSelected
            If LineNo >= 0 Then
                TlxRecData = TlxFlights(0).GetLineTag(LineNo)
                GetKeyItem TlxFldList, TlxRecData, "{=FLD=}", "{=/FLD=}"
                GetKeyItem TlxFldData, TlxRecData, "{=DAT=}", "{=/DAT=}"
                KeepCurrentView = True
                SetCreatedTelex 3, TlxFldList, TlxFldData
                KeepCurrentView = False
            End If
            InitTplListTab "A"
        Case "TLX_DEP"
            FltListPanel(0).Visible = True
            LineNo = TlxFlights(1).GetCurrentSelected
            If LineNo >= 0 Then
                TlxRecData = TlxFlights(1).GetLineTag(LineNo)
                GetKeyItem TlxFldList, TlxRecData, "{=FLD=}", "{=/FLD=}"
                GetKeyItem TlxFldData, TlxRecData, "{=DAT=}", "{=/DAT=}"
                KeepCurrentView = True
                SetCreatedTelex 3, TlxFldList, TlxFldData
                KeepCurrentView = False
            End If
            InitTplListTab "D"
        Case Else
    End Select
End Sub
Private Sub CreateTlxRecFromAftRec(AftFldList As String, AftFldData As String)

End Sub
Private Function GetFltInputFldIdx(BgnIdx As Integer, FldName As String) As Integer
    Dim idx As Integer
    Dim i As Integer
    idx = -1
    For i = BgnIdx To FltEditPanel.UBound
        If lblFltField(i).Tag <> "" Then
            If InStr(lblFltField(i).Tag, FldName) > 0 Then
                idx = i
                Exit For
            End If
        End If
    Next
    GetFltInputFldIdx = idx
End Function

Private Function GetTplFieldLength(TplFldName As String, FldIndex As Integer) As Integer
    Dim FldName As String
    Dim FldType As String
    Dim FldIdx As Integer
    Dim FldLen As Integer
    FldLen = 2
    FldName = TplFldName
    FldIdx = FldIndex
    FldType = ""
    If FldIdx >= 0 Then
        FldName = FltEditPanel(FldIdx).Tag
        FldType = lblDatType(FldIdx).Tag
    End If
    FldLen = 2
    If FldType = "" Then FldType = FldName
    Select Case FldType
        Case "DD"
            FldLen = 2
        Case "HHMM"
            FldLen = 4
        Case "DDHHMM"
            FldLen = 6
        Case "DCD1", "DCD2"
            FldLen = 2
        Case "DTD1", "DTD2"
            FldLen = 4
        Case "PXIF", "PXEF", "PXIN"
            FldLen = 3
        Case Else
        If InStr(TemplatePaxNames, FldName) > 0 Then
            FldLen = 3
        Else
            Select Case FldName
                Case "REGN"
                    FldLen = 5
                Case Else
                FldLen = 2
            End Select
        End If
    End Select
    GetTplFieldLength = FldLen
End Function

Private Sub InitSubPanelMs(Index As Integer)
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim SubIdx As Integer
    Dim idx As Integer
    SubIdx = Index
    If (SubIdx = 0) Or (Index < 0) Then
        InitTlxOrigTab
        InitAlcTab
        InitApcTab
        InitAddressTab
        fraOrigAddr.ZOrder
    End If
    SubIdx = Index
    If (SubIdx = 1) Or (Index < 0) Then
        SubIdx = 1
        InitTelexArea
        
    End If
    SubIdx = Index
    If (SubIdx = 2) Or (Index < 0) Then
        SubIdx = 2
        InitTlxTypeTab
        fraTlxType.ZOrder
        
    End If
    
    
End Sub

Private Sub ResizeSubPanels(Index As Integer)
    Dim tmpTag As String
    Dim SubWidth As Long
    Dim SubHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewValue As Long
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim MinFraHeight As Long
    Dim MinTxtHeight As Long
    Dim SubIdx As Integer
    Dim BgnIdx As Integer
    Dim EndIdx As Integer
    Dim SplIdx As Integer
    Dim idx As Integer
    If Index >= 0 Then
        BgnIdx = Index
        EndIdx = Index
    Else
        BgnIdx = 0
        EndIdx = SubPanelM.UBound
    End If
    For SubIdx = BgnIdx To EndIdx
        Select Case SubIdx
            Case 0
                SubHeight = SubPanelM(SubIdx).ScaleHeight
                NewHeight = SubHeight - fraAddress.Top
                If NewHeight >= 60 Then
                    fraAddress.Height = NewHeight
                    NewHeight = NewHeight - AddressTab(0).Top - 90
                    If NewHeight >= 60 Then
                        AddressTab(0).Height = NewHeight
                        AddressTab(1).Height = NewHeight
                    End If
                End If
                SubHeight = SubPanelS(SubIdx).ScaleHeight
                NewHeight = SubHeight - SubFrameS(SubIdx).Top
                If NewHeight >= 60 Then SubFrameS(SubIdx).Height = NewHeight
            Case 1
                SubHeight = SubPanelM(SubIdx).ScaleHeight
                NewHeight = SubHeight - fraPreview.Top
                If NewHeight >= 600 Then
                    fraPreview.Height = NewHeight
                    NewHeight = NewHeight - txtPreview(0).Top - 90
                    If NewHeight > 450 Then
                        For idx = 0 To txtPreview.UBound
                            txtPreview(idx).Height = NewHeight
                        Next
                    End If
                End If
                NewHeight = SubHeight
                NewTop = NewHeight - fraTelexText(1).Height
                If NewTop >= 60 Then
                    fraTelexText(1).Top = NewTop
                    tmpTag = fraTelexText(1).Tag
                    SplIdx = Val(tmpTag)
                    NewTop = NewTop - SubSplitHorzM(0).Height
                    If SplIdx >= 0 Then
                        SubSplitHorzM(SplIdx).Top = NewTop
                        'SubSplitHorzM(SplIdx).ZOrder
                    End If
                End If
                RefreshControls 1
                'txtTelexText(0).Refresh
                If Trim(txtSiText(0).Text) = "" Then
                'If Not txtSiText(0).Visible Then
                    'fraTelexText(0).ZOrder
                    txtSiText(0).Visible = False
                    fraSiTxtLabel(0).Visible = False
                    fraSiTxtLabel(1).Visible = False
                    MinTxtHeight = txtSiText(1).Height
                    MinFraHeight = fraTelexText(1).Height
                    NewHeight = NewTop - fraTelexText(0).Top
                    If NewHeight >= MinFraHeight Then
                        fraTelexText(0).Height = NewHeight
                        NewHeight = NewHeight - txtTelexText(0).Top - 90
                        txtTelexText(0).Height = NewHeight
                    End If
                Else
                    MinFraHeight = fraTelexText(1).Height
                    MinTxtHeight = txtSiText(1).Height
                    NewHeight = NewTop - fraTelexText(0).Top
                    If NewHeight >= MinFraHeight Then
                        fraTelexText(0).Height = NewHeight
                        NewTop = NewHeight - txtSiText(0).Height - 60
                        txtSiText(0).Top = NewTop
                        NewTop = txtSiText(0).Top - fraSiTxtLabel(0).Height
                        fraSiTxtLabel(0).Top = NewTop
                        fraSiTxtLabel(0).Visible = True
                        NewHeight = -1
                    Else
                        fraSiTxtLabel(0).Visible = False
                        MinFraHeight = MinFraHeight - fraSiTxtLabel(0).Height '+ 60
                    End If
                    RefreshControls 1
                    'txtTelexText(0).Refresh
                    NewHeight = fraSiTxtLabel(0).Top - txtTelexText(0).Top
                    If NewHeight >= MinTxtHeight Then
                        txtTelexText(0).Height = NewHeight
                        NewTop = txtTelexText(0).Top + NewHeight
                        txtTelexText(0).Refresh
                        fraSiTxtLabel(1).Top = NewTop
                    Else
                        fraSiTxtLabel(0).Visible = False
                    End If
                End If
                'txtTelexText(0).Refresh
                SubHeight = SubPanelS(SubIdx).ScaleHeight
                NewHeight = SubHeight - SubFrameS(SubIdx).Top
                If NewHeight >= 60 Then SubFrameS(SubIdx).Height = NewHeight
                RefreshControls 1
            Case 2
                SubHeight = SubPanelM(SubIdx).ScaleHeight
                NewHeight = SubHeight - fraFlightData.Top
                If NewHeight >= 60 Then
                    fraFlightData.Height = NewHeight
                End If
                NewHeight = NewHeight - FltListPanel(0).Top - 90
                If NewHeight >= 60 Then
                    FltListPanel(0).Height = NewHeight
                    FltDlgCover(1).Height = NewHeight
                End If
                NewHeight = NewHeight - FltListPanel(2).Top - 15
                If NewHeight >= 180 Then
                    FltListPanel(2).Height = NewHeight
                    NewHeight = NewHeight - 60
                    TabFlights(0).Height = NewHeight
                    TabFlights(1).Height = NewHeight
                End If
                NewHeight = fraFlightData.Height - FltInputPanel.Top - 90
                If NewHeight >= 60 Then
                    FltInputPanel.Height = NewHeight
                    VScroll1(0).Height = NewHeight - 30
                    DrawBackGround FltInputPanel, MyLifeStyleValue, False, True
                    DrawBackGround FltInputSlide, MyLifeStyleValue, False, True
                    NewValue = FltInputPanel.ScaleHeight - FltInputSlide.Height
                    If (NewValue < 0) And (FltInputSlide.Top < -300) Then
                        FltInputSlide.Top = NewValue
                        VScroll1(0).Value = -(NewValue / 15)
                    End If
                    If VScroll1(0).Height > FltInputSlide.Height Then
                        VScroll1(0).Value = 0
                        VScroll1(0).Enabled = False
                    Else
                        VScroll1(0).Enabled = True
                    End If
                End If
                VScroll1(0).Refresh
                SubHeight = SubPanelS(SubIdx).ScaleHeight
                NewHeight = SubHeight - SubFrameS(SubIdx).Top
                If NewHeight >= 60 Then SubFrameS(SubIdx).Height = NewHeight
            Case Else
        End Select
    Next
End Sub
Private Sub MoveSubPanels(Index As Integer)
    Static FuncIsBusy As Boolean
    Dim tmpTag As String
    Dim MainWidth As Long
    Dim SubWidth As Long
    Dim SubHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewValue As Long
    Dim NewWidth As Long
    Dim SpcWidth As Long
    Dim NewHeight As Long
    Dim MinFraHeight As Long
    Dim MinTxtHeight As Long
    Dim SubIdx As Integer
    Dim BgnIdx As Integer
    Dim EndIdx As Integer
    Dim SplIdx As Integer
    Dim SpcType As Integer
    Dim MCIdx As Integer
    Dim idx As Integer
    If Not FuncIsBusy Then
        FuncIsBusy = True
        SpcType = 0
        If Index >= 0 Then
            BgnIdx = Index
            EndIdx = Index
        Else
            BgnIdx = 0
            EndIdx = SubPanelM.UBound
        End If
        'Check before move
        For SubIdx = BgnIdx To EndIdx
            Select Case SubIdx
                Case 0
                Case Else
            End Select
        Next
        MainWidth = MainPanel(0).ScaleWidth
        If SubPanelM.UBound >= 2 Then
            NewValue = 0
            NewValue = NewValue + FullPanelWidth(0)
            NewValue = NewValue + FullPanelWidth(1)
            NewLeft = MainWidth - FullPanelWidth(2)
            SpcWidth = NewLeft - NewValue
            NewValue = NewValue + FullPanelWidth(2)
            'If SpcWidth < 0 Then SpcWidth = 0
            'If SpcWidth > 0 Then
                'Distribute the space area
                SpcType = Val(fraSetupX(3).Tag)
                Select Case SpcType
                    Case 0
                        'Keep All Panels Together
                        NewLeft = 0
                        SubPanelM(0).Left = NewLeft + SubSplitVertM(0).Width
                        NewLeft = NewLeft + FullPanelWidth(0)
                        SubPanelM(1).Left = NewLeft + SubSplitVertM(1).Width
                        NewLeft = NewLeft + FullPanelWidth(1)
                        SubPanelM(2).Left = NewLeft + SubSplitVertM(2).Width
                        NewLeft = -1
                    Case 1
                        'Center Main Panel
                        SpcWidth = SpcWidth / 2
                        SubPanelM(2).Left = NewLeft + SubSplitVertM(2).Width
                        NewLeft = NewLeft - FullPanelWidth(1) - SpcWidth
                        SubPanelM(1).Left = NewLeft + SubSplitVertM(1).Width
                        NewLeft = 0
                        SubPanelM(0).Left = NewLeft + SubSplitVertM(0).Width
                        NewLeft = -1
                    Case 2
                        'Move Flight Data Panel Only
                        SubPanelM(2).Left = NewLeft + SubSplitVertM(2).Width
                        NewLeft = SubSplitVertM(0).Left
                        SubPanelM(0).Left = NewLeft + SubSplitVertM(0).Width
                        NewLeft = NewLeft + FullPanelWidth(0)
                        SubPanelM(1).Left = NewLeft + SubSplitVertM(1).Width
                        NewLeft = -1
                    Case 3
                        'Keep Main And Data Panel Together
                        If Not GeneratorIsReadyForUse Then
                            NewWidth = Me.Width - Me.ScaleWidth
                            NewWidth = NewWidth + FullPanelWidth(1)
                            NewWidth = NewWidth + FullPanelWidth(2)
                            Me.Width = NewWidth + 60
                        End If
                        SubPanelM(2).Left = NewLeft + SubSplitVertM(2).Width
                        NewLeft = NewLeft - FullPanelWidth(1)
                        SubPanelM(1).Left = NewLeft + SubSplitVertM(1).Width
                        SubSplitVertM(0).Left = 0
                        NewLeft = SubSplitVertM(0).Left
                        SubPanelM(0).Left = NewLeft + SubSplitVertM(0).Width
                        NewLeft = -1
                    Case 4
                        'Move All Panels
                        SubPanelM(2).Left = NewLeft + SubSplitVertM(2).Width
                        NewLeft = NewLeft - FullPanelWidth(1)
                        SubPanelM(1).Left = NewLeft + SubSplitVertM(1).Width
                        NewLeft = NewLeft - FullPanelWidth(0)
                        SubPanelM(0).Left = NewLeft + SubSplitVertM(0).Width
                        NewLeft = -1
                    Case 5
                        'Don't Resize Main Panel
                        If GeneratorIsReadyForUse Then
                            MainPanel(1).Top = MainPanel(0).Top
                            MainPanel(1).Height = MainPanel(0).Height
                            MainPanel(0).Width = NewValue + 60
                            MainPanel(1).Left = MainPanel(0).Left + MainPanel(0).Width
                            MainPanel(1).Visible = True
                            NewLeft = NewValue
                            SpcWidth = 0
                        End If
                        NewLeft = -1
                    Case Else
                        'Don't Resize Main Panel
                        If GeneratorIsReadyForUse Then
                            MainPanel(1).Top = MainPanel(0).Top
                            MainPanel(1).Height = MainPanel(0).Height
                            MainPanel(0).Width = NewValue + SubPanelM(2).Width + SubSplitVertS(1).Width + 60
                            MainPanel(1).Left = MainPanel(0).Left + MainPanel(0).Width
                            MainPanel(1).Visible = True
                            NewLeft = NewValue
                            SpcWidth = 0
                        End If
                        NewLeft = -1
                End Select
            'Else
            If (SpcWidth < 0) And (GeneratorIsReadyForUse) Then MainPanel(1).Visible = False
            If NewLeft >= 0 Then
                SubPanelM(2).Left = NewLeft + SubSplitVertM(2).Width
                NewLeft = NewLeft - FullPanelWidth(1)
                NewValue = SubSplitVertM(0).Left + FullPanelWidth(0)
                If NewLeft < NewValue Then
                    If NewLeft < 0 Then NewLeft = 0 - SubSplitVertM(1).Width
                    SubPanelM(1).Left = NewLeft + SubSplitVertM(1).Width
                ElseIf NewLeft > NewValue Then
                    NewLeft = NewValue
                    If SpcWidth > 0 Then NewLeft = NewLeft + SpcWidth
                    SubPanelM(1).Left = NewLeft + SubSplitVertM(0).Width
                End If
            End If
            For idx = 0 To SubPanelM.UBound
                MainSplitHorzM(idx).Left = SubPanelM(idx).Left
                MainSplitHorzM(idx).Width = SubPanelM(idx).Width
                MainSplitHorzS(idx).Left = SubPanelM(idx).Left
                MainSplitHorzS(idx).Width = SubPanelM(idx).Width
                NewValue = SubPanelM(idx).Left - SubSplitVertM(idx).Width
                SubSplitVertM(idx).Left = NewValue
                SubSplitVertM(idx).Visible = True
                NewLeft = SubPanelM(idx).Left + SubPanelM(idx).Width
                SubSplitVertS(idx).Left = NewLeft
                SubSplitVertS(idx).Visible = True
                NewLeft = NewLeft + SubSplitVertS(idx).Width
                SubPanelS(idx).Left = NewLeft
            Next
            MCIdx = -1
            MouseClock.StopMouseClock -1
            For idx = 0 To SubPanelS.UBound - 1
                NewLeft = SubSplitVertS(idx).Left + SubSplitVertS(idx).Width
                SpcWidth = SubSplitVertM(idx + 1).Left - NewLeft
                If SpcWidth > 0 Then
                    SubPanelS(idx).Width = SpcWidth
                    SubFrameS(idx).Width = SpcWidth
                    SubSplitHorzS(idx).Width = SpcWidth
                    SubPanelS(idx).Visible = True
                    If SpcWidth > 300 Then
                        'If MCIdx < 0 Then
                        '    MCIdx = idx
                            MouseClock.StartMouseClock idx, SubPanelS(idx)
                        'End If
                    Else
                        MouseClock.StopMouseClock idx
                    End If
                Else
                    MouseClock.StopMouseClock idx
                End If
            Next
            idx = SubPanelS.UBound
            NewLeft = SubSplitVertS(idx).Left + SubSplitVertS(idx).Width
            SpcWidth = MainPanel(0).ScaleWidth - NewLeft
            If SpcWidth > 0 Then
                SubPanelS(idx).Width = SpcWidth
                SubFrameS(idx).Width = SpcWidth
                SubSplitHorzS(idx).Width = SpcWidth
                SubPanelS(idx).Visible = True
                If SpcWidth > 300 Then
                    'If MCIdx < 0 Then
                    '    MCIdx = idx
                        MouseClock.StartMouseClock idx, SubPanelS(idx)
                    'End If
                Else
                    MouseClock.StopMouseClock idx
                End If
            Else
                MouseClock.StopMouseClock idx
            End If
        End If
        'Check after move
        For SubIdx = BgnIdx To EndIdx
            SpcWidth = SubFrameS(SubIdx).Width
            
            Select Case SubIdx
                Case 0
                Case Else
            End Select
        Next
        FuncIsBusy = False
    End If
End Sub
Private Function FullPanelWidth(Index As Integer) As Long
    Dim SubSize As Long
    SubSize = 0
    SubSize = SubSize + SubSplitVertM(Index).Width
    SubSize = SubSize + SubPanelM(Index).Width
    SubSize = SubSize + SubSplitVertS(Index).Width
    FullPanelWidth = SubSize
End Function
Private Sub InitAlcTab()
    AlcTab.Top = fraChkAddr.Top + fraChkAddr.Height + 30
    AlcTab.ResetContent
    AlcTab.FontName = "Courier New"
    AlcTab.HeaderFontSize = MyFontSize
    AlcTab.FontSize = MyFontSize
    AlcTab.lineHeight = MyFontSize
    AlcTab.SetTabFontBold MyFontBold
    AlcTab.HeaderString = "-ALC-,-ALC-,-ALC-"
    AlcTab.HeaderLengthString = "100,100,100"
    AlcTab.ColumnAlignmentString = "C,C,C"
    AlcTab.HeaderAlignmentString = "C,C,C"
    AlcTab.ShowVertScroller False
    AlcTab.ShowRowSelection = False
    AlcTab.InsertTextLine ",,", False
    AlcTab.EnableInlineEdit True
    AlcTab.LifeStyle = True
    AlcTab.AutoSizeByHeader = True
    AlcTab.AutoSizeColumns
    AlcTab.Height = ((AlcTab.lineHeight * 15) * 2) + 15
End Sub

Private Sub InitApcTab()
    ApcTab.Top = AlcTab.Top + AlcTab.Height + 15
    ApcTab.ResetContent
    ApcTab.FontName = "Courier New"
    ApcTab.HeaderFontSize = MyFontSize
    ApcTab.FontSize = MyFontSize
    ApcTab.lineHeight = MyFontSize
    ApcTab.SetTabFontBold MyFontBold
    ApcTab.HeaderString = "-APC-,-APC-,-APC-"
    ApcTab.HeaderLengthString = "100,100,100"
    ApcTab.ColumnAlignmentString = "C,C,C"
    ApcTab.HeaderAlignmentString = "C,C,C"
    ApcTab.ShowVertScroller False
    ApcTab.ShowRowSelection = False
    ApcTab.InsertTextLine ",,", False
    ApcTab.InsertTextLine ",,", False
    ApcTab.InsertTextLine ",,", False
    ApcTab.InsertTextLine ",,", False
    ApcTab.EnableInlineEdit True
    ApcTab.AutoSizeByHeader = True
    ApcTab.LifeStyle = True
    ApcTab.AutoSizeColumns
    ApcTab.Height = ((ApcTab.lineHeight * 15) * 5) + 15
End Sub
Private Sub InitAddressTab()
    AddressTab(0).Top = ApcTab.Top + ApcTab.Height + 15
    AddressTab(0).ResetContent
    AddressTab(0).FontName = "Courier New"
    AddressTab(0).HeaderFontSize = MyFontSize
    AddressTab(0).FontSize = MyFontSize
    AddressTab(0).lineHeight = MyFontSize
    AddressTab(0).SetTabFontBold MyFontBold
    AddressTab(0).HeaderString = "ALC,APC,SITA,Remark                      ,DS,URNO"
    AddressTab(0).HeaderLengthString = "100,100,100,100,100,100"
    AddressTab(0).LogicalFieldList = "ALC3,APC3,TEAD,BEME,PAYE,URNO"
    AddressTab(0).ColumnWidthString = "3,3,7,100,10,10"
    AddressTab(0).ColumnAlignmentString = "L,L,L,L,L,L"
    AddressTab(0).CreateDecorationObject "SelMarker", "L,R,T,B", "2,2,2,2", Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack)
    AddressTab(0).ShowHorzScroller True
    AddressTab(0).LifeStyle = True
    AddressTab(0).AutoSizeByHeader = True
    AddressTab(0).AutoSizeColumns
    
    AddressTab(1).ResetContent
    AddressTab(1).FontName = "Courier New"
    AddressTab(1).HeaderFontSize = MyFontSize
    AddressTab(1).FontSize = MyFontSize
    AddressTab(1).lineHeight = MyFontSize
    AddressTab(1).SetTabFontBold MyFontBold
    AddressTab(1).HeaderString = "ALC,APC,E-MAIL,Remark                      ,DS,URNO"
    AddressTab(1).HeaderLengthString = "100,100,100,100,100,100"
    AddressTab(1).LogicalFieldList = "ALC3,APC3,TEAD,BEME,PAYE,URNO"
    AddressTab(1).ColumnWidthString = "3,3,7,100,10,10"
    AddressTab(1).ColumnAlignmentString = "L,L,L,L,L,L"
    AddressTab(1).CreateDecorationObject "SelMarker", "L,R,T,B", "2,2,2,2", Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack)
    AddressTab(1).ShowHorzScroller True
    AddressTab(1).LifeStyle = True
    AddressTab(1).AutoSizeByHeader = True
    AddressTab(1).AutoSizeColumns
    
    AddressTab(1).Top = AddressTab(0).Top
    AddressTab(1).Left = AddressTab(0).Left
    AddressTab(1).Width = AddressTab(0).Width
End Sub
Private Sub InitTlxOrigTab()

    TlxOrigTab.ResetContent
    TlxOrigTab.FontName = "Courier New"
    TlxOrigTab.HeaderFontSize = MyFontSize
    TlxOrigTab.FontSize = MyFontSize
    TlxOrigTab.lineHeight = MyFontSize
    TlxOrigTab.SetTabFontBold MyFontBold
    TlxOrigTab.HeaderString = "Address,Remark                      "
    TlxOrigTab.HeaderLengthString = "100,100"
    TlxOrigTab.LifeStyle = True
    'TlxOrigTab.CursorLifeStyle = True
    TlxOrigTab.AutoSizeByHeader = True
    TlxOrigTab.AutoSizeColumns
    
End Sub
Private Sub InitTelexArea()
    Dim tmpTag As String
    Dim tmpFields As String
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim SplIdx As Integer
    Dim i As Integer
    
    'fraDestAddr(1).BackColor = MyOwnButtonFace
    
    NewWidth = SubPanelM(1).ScaleWidth
    Set fraPreview.Container = SubPanelM(1)
    fraPreview.Left = 0
    fraPreview.Width = NewWidth
    
    Set fraSetup(3).Container = SubPanelM(1)
    fraSetup(3).Top = fraFlightPanel(0).Top
    fraSetup(3).Left = 0
    
    NewLeft = 60
    NewWidth = fraTelexText(0).Width - (NewLeft * 2)
    
    NewTop = txtDestAddr(0).Top
    NewHeight = txtDestAddr(0).Height
    For i = 0 To 1
        txtDestAddr(i).Top = NewTop
        txtDestAddr(i).Left = NewLeft
        txtDestAddr(i).Width = NewWidth
        txtDestAddr(i).Height = NewHeight
    Next
    fraPreview.Top = fraDestAddr(0).Top
    
    NewTop = txtDestAddr(2).Top
    NewHeight = txtDestAddr(2).Height
    For i = 2 To 3
        txtDestAddr(i).Top = NewTop
        txtDestAddr(i).Left = NewLeft - 30
        txtDestAddr(i).Width = NewWidth
        txtDestAddr(i).Height = NewHeight
    Next
    
    txtSiText(0).Left = NewLeft
    txtSiText(1).Left = NewLeft
    
    NewTop = txtTelexText(0).Top
    NewHeight = txtTelexText(0).Height
    For i = 0 To 0
        txtTelexText(i).Top = NewTop
        txtTelexText(i).Left = NewLeft
        txtTelexText(i).Width = NewWidth
        txtTelexText(i).Height = NewHeight
    Next
    
    For i = 0 To 1
        txtSiText(i).Left = NewLeft
        txtSiText(i).Width = NewWidth
    Next
    
    For i = 0 To txtPreview.UBound
        txtPreview(i).Left = NewLeft
        txtPreview(i).Width = NewWidth
    Next

    fraSiTxtLabel(0).Height = SiTxtLabel(0).Height
    fraSiTxtLabel(1).Height = 60
    For i = 0 To 1
        fraSiTxtLabel(i).Left = NewLeft
        fraSiTxtLabel(i).Width = NewWidth
        'fraSiTxtLabel(i).BackColor = MyOwnButtonFace
        fraSiTxtLabel(i).Visible = True
    Next
    
    
    txtTelexText(0).FontBold = MyFontBold
    txtTelexText(0).FontSize = MyFontSize - 5
    txtPreview(0).FontBold = MyFontBold
    txtPreview(0).FontSize = MyFontSize - 5
    
    fraTelexText(1).ZOrder
    fraDestAddr(0).ZOrder
    fraTelexText(0).ZOrder
    txtSiText(0).ZOrder
    txtTelexText(0).ZOrder
    fraSiTxtLabel(1).ZOrder
    fraSiTxtLabel(0).ZOrder
    
    tmpTag = fraDestAddr(0).Tag
    SplIdx = Val(tmpTag)
    SubSplitHorzM(SplIdx).ZOrder
    
    tmpTag = fraTelexText(0).Tag
    SplIdx = Val(tmpTag)
    SubSplitHorzM(SplIdx).ZOrder
    
    fraFlightPanel(0).ZOrder
    
    Set fraFlightPanel(2).Container = SubPanelM(1)
    
    fraFlightPanel(2).Left = fraDestAddr(0).Left
    fraFlightPanel(2).Width = fraFlightPanel(0).Width
    fraFlightPanel(2).Top = fraDestAddr(0).Top
    fraFlightPanel(2).Height = fraDestAddr(0).Height
    fraFlightPanel(11).Top = fraFlightPanel(4).Top
    fraFlightPanel(11).Left = fraFlightPanel(4).Left
    fraFlightPanel(11).Height = fraFlightPanel(4).Height
    fraFlightPanel(11).Width = fraFlightPanel(4).Width
    fraFlightPanel(13).Top = fraFlightPanel(5).Top
    fraFlightPanel(13).Left = fraFlightPanel(5).Left
    fraFlightPanel(13).Height = fraFlightPanel(5).Height
    fraFlightPanel(13).Width = fraFlightPanel(5).Width
    
    Set fraFlightPanel(6).Container = fraFlightPanel(4)
    fraFlightPanel(6).Top = fraFlightPanel(7).Top
    fraFlightPanel(6).Left = fraFlightPanel(7).Left
    fraFlightPanel(6).Visible = False
    
    fraFlightPanel(7).Visible = False
    fraFlightPanel(8).Visible = False
    fraFlightPanel(10).Visible = False
    'fraFlightPanel(12).Visible = False
    'fraFlightPanel(14).Visible = False
    fraFlightPanel(15).Visible = False
    
    Set fraFlightCover(0).Container = SubPanelM(1)
    fraFlightCover(0).Left = 0
    fraFlightCover(0).Top = fraFlightPanel(2).Top
    fraFlightCover(0).Height = fraFlightPanel(2).Height
    fraFlightCover(0).Width = SubPanelM(1).ScaleWidth
    fraFlightCover(0).Visible = False
    fraFlightCover(0).ZOrder
    fraFlightPanel(2).ZOrder
    
        txtSiText(0).Visible = False
        fraSiTxtLabel(0).Visible = False
        fraSiTxtLabel(1).Visible = False
    
    tmpFields = "FLNO,STOA,LAND,ONBL,ORG3,VIA3,ETAI,ADID,REGN,ACT5,ACT3,DES3,VIAL,ALC3,FLTN,FLNS,FKEY,FLDA,URNO,TURN"
    TlxFlights(0).LogicalFieldList = tmpFields
    TlxFlights(0).HeaderString = "FLNO,STOA,LAND,ONBL,ORG3,VIA3,ETAI,ADID,REGN,ACT5,ACT3,DES3,VIAL,ALC3,FLTN,FLNS,FKEY,FLDA,URNO,TURN"
    TlxFlights(0).HeaderLengthString = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100"
    TlxFlights(0).ColumnAlignmentString = "L,C,C,C,C,C,C,C,L,C,C,C,L,L,L,L,L,L,L,L"
    TlxFlights(0).HeaderAlignmentString = "L,C,C,C,C,C,C,C,L,C,C,C,L,L,L,L,L,L,L,L"
    TlxFlights(0).FontName = "Courier New"
    TlxFlights(0).HeaderFontSize = MyFontSize
    TlxFlights(0).FontSize = MyFontSize
    TlxFlights(0).lineHeight = MyFontSize
    TlxFlights(0).SetTabFontBold MyFontBold
    TlxFlights(0).MainHeader = True
    TlxFlights(0).SetMainHeaderFont MyFontSize, False, False, True, 0, "Courier New"
    TlxFlights(0).SetMainHeaderValues "4,16", "Telex ARR Flights,", ","
    TlxFlights(0).DateTimeSetColumn 1
    TlxFlights(0).DateTimeSetInputFormatString 1, "YYYYMMDDhhmmss"
    TlxFlights(0).DateTimeSetOutputFormatString 1, "hhmm"
    TlxFlights(0).DateTimeSetColumn 2
    TlxFlights(0).DateTimeSetInputFormatString 2, "YYYYMMDDhhmmss"
    TlxFlights(0).DateTimeSetOutputFormatString 2, "hhmm"
    TlxFlights(0).DateTimeSetColumn 3
    TlxFlights(0).DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
    TlxFlights(0).DateTimeSetOutputFormatString 3, "hhmm"
    TlxFlights(0).ShowHorzScroller True
    TlxFlights(0).LifeStyle = True
    'TlxFlights(0).CursorLifeStyle = True
    TlxFlights(0).AutoSizeByHeader = True
    TlxFlights(0).AutoSizeColumns
    
    TlxFlights(1).ResetContent
    tmpFields = "STYP,FLNO,STOD,NSTN,DES3,ETDI,OFBL,AIRB,REGN,ACT5,ACT3,EETF,EETA,DCO1,DCO2,DUR1,DUR2,REAS,PXIF,PXIN,PXEF,ADID,VIAL,ORG3,FLTN,FLNS,FKEY,FLDA,URNO,TURN"
    TlxFlights(1).LogicalFieldList = tmpFields
    TlxFlights(1).HeaderString = "STYP,FLNO,STOD,NSTN,DES3,ETDI,OFBL,AIRB,REGN,ACT5,ACT3,EETF,EETA,DCO1,DCO2,DUR1,DUR2,REAS,PXIF,PXIN,PXEF,ADID,VIAL,ORG3,FLTN,FLNS,FKEY,FLDA,URNO,TURN"
    TlxFlights(1).HeaderLengthString = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100"
    TlxFlights(1).ColumnAlignmentString = "L,L,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,L,L,L,L,L,L"
    TlxFlights(1).HeaderAlignmentString = "L,L,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,L,L,L,L,L,L"
    TlxFlights(1).FontName = "Courier New"
    TlxFlights(1).HeaderFontSize = MyFontSize
    TlxFlights(1).FontSize = MyFontSize
    TlxFlights(1).lineHeight = MyFontSize
    TlxFlights(1).SetTabFontBold MyFontBold
    TlxFlights(1).MainHeader = True
    TlxFlights(1).SetMainHeaderFont MyFontSize, False, False, True, 0, "Courier New"
    TlxFlights(1).SetMainHeaderValues "5,25", "Telex DEP Flights,", ","
    TlxFlights(1).DateTimeSetColumn 2
    TlxFlights(1).DateTimeSetInputFormatString 2, "YYYYMMDDhhmmss"
    TlxFlights(1).DateTimeSetOutputFormatString 2, "hhmm"
    TlxFlights(1).DateTimeSetColumn 5
    TlxFlights(1).DateTimeSetInputFormatString 5, "YYYYMMDDhhmmss"
    TlxFlights(1).DateTimeSetOutputFormatString 5, "hhmm"
    TlxFlights(1).DateTimeSetColumn 6
    TlxFlights(1).DateTimeSetInputFormatString 6, "YYYYMMDDhhmmss"
    TlxFlights(1).DateTimeSetOutputFormatString 6, "hhmm"
    TlxFlights(1).DateTimeSetColumn 7
    TlxFlights(1).DateTimeSetInputFormatString 7, "YYYYMMDDhhmmss"
    TlxFlights(1).DateTimeSetOutputFormatString 7, "hhmm"
    TlxFlights(1).DateTimeSetColumn 12
    TlxFlights(1).DateTimeSetInputFormatString 12, "YYYYMMDDhhmmss"
    TlxFlights(1).DateTimeSetOutputFormatString 12, "hhmm"
    TlxFlights(1).ShowHorzScroller True
    TlxFlights(1).LifeStyle = True
    'TlxFlights(1).CursorLifeStyle = True
    TlxFlights(1).AutoSizeByHeader = True
    TlxFlights(1).AutoSizeColumns
    
    
    
    InitFlightInfoPanel -1, "", ""
    
    fraPreview.ZOrder
    fraSetup(3).ZOrder
End Sub
Private Sub InitTlxTypeTab()
    Dim NewWidth As Long
    'fraTopButtons(1).Tag = CStr(fraTopButtons(1).Width)
    NewWidth = fraTlxType.Width - TlxTypeTab.Left * 2
    TlxTypeTab.Width = NewWidth
    TlxTypeTab.ResetContent
    TlxTypeTab.FontName = "Courier New"
    TlxTypeTab.HeaderFontSize = MyFontSize
    TlxTypeTab.FontSize = MyFontSize
    TlxTypeTab.lineHeight = MyFontSize
    TlxTypeTab.SetTabFontBold MyFontBold
    TlxTypeTab.HeaderString = "Type,ST,Message Category               "
    TlxTypeTab.HeaderLengthString = "100,100,100"
    TlxTypeTab.LifeStyle = True
    'TlxTypeTab.CursorLifeStyle = True
    TlxTypeTab.AutoSizeByHeader = True
    TlxTypeTab.AutoSizeColumns
    TlxTypeTab.Tag = CStr(fraTlxType.Width) & "," & CStr(fraTlxType.Height)
End Sub
Private Sub InitPanelIcons()
    Dim tmpTag As String
    Dim tmpCapt As String
    Dim CurFrame As Frame
    Dim SetCapt As Boolean
    Dim idx As Integer
    For idx = 0 To PanelIcon.UBound
        SetCapt = True
        PanelIcon(idx).Left = 60
        Select Case idx
            Case 0
                Set CurFrame = fraOrigAddr
            Case 1
                Set CurFrame = fraFlightPanel(0)
            Case 2
                Set CurFrame = fraTlxType
            Case 3
                Set CurFrame = fraAddress
            Case 4
                Set CurFrame = fraDestAddr(0)
            Case 5
                Set CurFrame = fraFlightData
            Case 6
                Set CurFrame = fraTelexText(0)
            Case 7
                Set CurFrame = fraTelexText(1)
            Case 8
                Set CurFrame = fraFlightPanel(2)
            Case Else
            SetCapt = False
        End Select
        If SetCapt Then
            tmpTag = PanelIcon(idx).Tag
            If tmpTag = "SHOW" Then
                tmpCapt = Trim(CurFrame.Caption)
                PanelIcon(idx).Visible = True
                tmpCapt = "      " & tmpCapt
                CurFrame.Caption = tmpCapt
            ElseIf tmpTag <> "DONE" Then
                tmpCapt = Trim(CurFrame.Caption)
                CurFrame.Caption = tmpCapt
            End If
            PanelIcon(idx).Tag = "DONE"
        End If
    Next
End Sub
Private Function GetPaxFieldIndex(AftItm As Integer, AftName As String, TplItm As Integer, TplName As String) As Integer
    Dim tmpTag As String
    Dim tmpName As String
    Dim idx As Integer
    Dim i As Integer
    On Error Resume Next
    idx = -1
    For i = 0 To AftFldPanel.UBound
        tmpTag = AftFldPanel(i).Tag
        If tmpTag <> "" Then
            If TplName <> "" Then
                tmpTag = GetItem(tmpTag, 2, "|")
                tmpName = GetItem(tmpTag, TplItm, ",")
                If tmpName = TplName Then
                    idx = i
                    Exit For
                End If
            End If
            If AftName <> "" Then
                tmpTag = GetItem(tmpTag, 2, "|")
                tmpName = GetItem(tmpTag, AftItm, ",")
                If tmpName = AftName Then
                    idx = i
                    Exit For
                End If
            End If
        End If
    Next
    GetPaxFieldIndex = idx
End Function
Private Function GetIconIndex(IconKey As String) As Integer
    On Error GoTo ErrorHandler
    GetIconIndex = MyIcons.ListImages(IconKey).Index
    Exit Function
ErrorHandler:
    GetIconIndex = 2
    Resume Next
End Function

