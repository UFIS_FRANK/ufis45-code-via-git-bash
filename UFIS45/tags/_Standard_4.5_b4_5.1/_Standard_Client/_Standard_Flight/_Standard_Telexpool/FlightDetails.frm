VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form FlightDetails 
   Caption         =   "Flight Details"
   ClientHeight    =   8415
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   20370
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FlightDetails.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8415
   ScaleWidth      =   20370
   Tag             =   "Ufis Telex Generator"
   Begin VB.PictureBox Picture5 
      BackColor       =   &H008080FF&
      BorderStyle     =   0  'None
      Height          =   4275
      Left            =   2370
      ScaleHeight     =   4275
      ScaleWidth      =   6825
      TabIndex        =   228
      Top             =   2790
      Width           =   6825
   End
   Begin VB.Frame Frame3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3645
      Left            =   10110
      TabIndex        =   225
      Top             =   6780
      Width           =   3255
      Begin VB.PictureBox FltListPanel 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   5
         Left            =   90
         ScaleHeight     =   2985
         ScaleWidth      =   3015
         TabIndex        =   226
         Top             =   510
         Width           =   3075
         Begin TABLib.TAB TabFlights 
            Height          =   3015
            Index           =   3
            Left            =   0
            TabIndex        =   227
            Top             =   0
            Width           =   3000
            _Version        =   65536
            _ExtentX        =   5292
            _ExtentY        =   5318
            _StockProps     =   64
         End
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3645
      Left            =   30
      TabIndex        =   222
      Top             =   6780
      Width           =   3255
      Begin VB.PictureBox FltListPanel 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   4
         Left            =   90
         ScaleHeight     =   2985
         ScaleWidth      =   3015
         TabIndex        =   223
         Top             =   510
         Width           =   3075
         Begin TABLib.TAB TabFlights 
            Height          =   3015
            Index           =   2
            Left            =   0
            TabIndex        =   224
            Top             =   0
            Width           =   3030
            _Version        =   65536
            _ExtentX        =   5345
            _ExtentY        =   5318
            _StockProps     =   64
         End
      End
   End
   Begin VB.Frame fraFlightPanel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1170
      Index           =   0
      Left            =   3360
      TabIndex        =   220
      Tag             =   "VISIBLE"
      Top             =   6780
      Width           =   6690
      Begin VB.PictureBox FltListPanel 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   3
         Left            =   90
         ScaleHeight     =   2985
         ScaleWidth      =   6435
         TabIndex        =   221
         Top             =   510
         Width           =   6495
      End
   End
   Begin VB.Frame fraFlightPanel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3645
      Index           =   16
      Left            =   3360
      TabIndex        =   212
      Tag             =   "VISIBLE"
      Top             =   3120
      Width           =   6690
      Begin VB.PictureBox FltListPanel 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   2
         Left            =   90
         ScaleHeight     =   2985
         ScaleWidth      =   6435
         TabIndex        =   217
         Top             =   510
         Width           =   6495
      End
   End
   Begin VB.Frame FlightMainInfo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   7
      Left            =   4890
      TabIndex        =   211
      Tag             =   "VISIBLE"
      Top             =   2100
      Width           =   3615
   End
   Begin VB.Frame FlightMainInfo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   6
      Left            =   8550
      TabIndex        =   197
      Tag             =   "VISIBLE"
      Top             =   2100
      Width           =   2385
      Begin VB.Frame fraFlightPanel 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   840
         Index           =   14
         Left            =   30
         TabIndex        =   198
         Top             =   135
         Width           =   2280
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   31
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   208
            Tag             =   "OBNS;DATE"
            Top             =   450
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   46
               Left            =   420
               TabIndex        =   209
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "FCL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   31
               Left            =   0
               TabIndex        =   210
               Top             =   45
               Width           =   315
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   30
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   205
            Tag             =   "LDNS;DATE"
            Top             =   120
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   45
               Left            =   420
               TabIndex        =   206
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "ETD"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   30
               Left            =   0
               TabIndex        =   207
               Top             =   45
               Width           =   300
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   29
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   202
            Top             =   120
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   44
               Left            =   0
               TabIndex        =   203
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   29
               Left            =   0
               TabIndex        =   204
               Top             =   45
               Width           =   45
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   28
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   199
            Top             =   450
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   43
               Left            =   0
               TabIndex        =   200
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   28
               Left            =   0
               TabIndex        =   201
               Top             =   45
               Width           =   45
            End
         End
      End
   End
   Begin VB.Frame FlightMainInfo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   5
      Left            =   10980
      TabIndex        =   183
      Tag             =   "VISIBLE"
      Top             =   2100
      Width           =   2385
      Begin VB.Frame fraFlightPanel 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   840
         Index           =   7
         Left            =   30
         TabIndex        =   184
         Top             =   135
         Width           =   2280
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   27
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   194
            Tag             =   "OBNS;DATE"
            Top             =   450
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   33
               Left            =   420
               TabIndex        =   195
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "AIB"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   27
               Left            =   0
               TabIndex        =   196
               Top             =   45
               Width           =   270
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   26
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   191
            Tag             =   "LDNS;DATE"
            Top             =   120
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   32
               Left            =   420
               TabIndex        =   192
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "OBL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   26
               Left            =   0
               TabIndex        =   193
               Top             =   45
               Width           =   330
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   25
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   188
            Top             =   120
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   31
               Left            =   0
               TabIndex        =   189
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   25
               Left            =   0
               TabIndex        =   190
               Top             =   45
               Width           =   45
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   24
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   185
            Top             =   450
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   30
               Left            =   0
               TabIndex        =   186
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   24
               Left            =   0
               TabIndex        =   187
               Top             =   45
               Width           =   45
            End
         End
      End
   End
   Begin VB.Frame FlightMainInfo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   2
      Left            =   2460
      TabIndex        =   169
      Tag             =   "VISIBLE"
      Top             =   2100
      Width           =   2385
      Begin VB.Frame fraFlightPanel 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   840
         Index           =   4
         Left            =   30
         TabIndex        =   170
         Top             =   135
         Width           =   2280
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   8
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   180
            Tag             =   "OBNS;DATE"
            Top             =   450
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   8
               Left            =   420
               TabIndex        =   181
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "ONB"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   8
               Left            =   0
               TabIndex        =   182
               Top             =   45
               Width           =   330
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   7
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   177
            Tag             =   "LDNS;DATE"
            Top             =   120
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   7
               Left            =   420
               TabIndex        =   178
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "LND"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   7
               Left            =   0
               TabIndex        =   179
               Top             =   45
               Width           =   315
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   6
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   174
            Top             =   120
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   6
               Left            =   0
               TabIndex        =   175
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   6
               Left            =   0
               TabIndex        =   176
               Top             =   45
               Width           =   45
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   5
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   171
            Top             =   450
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   5
               Left            =   0
               TabIndex        =   172
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   5
               Left            =   0
               TabIndex        =   173
               Top             =   45
               Width           =   45
            End
         End
      End
   End
   Begin VB.Frame FlightMainInfo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Index           =   4
      Left            =   30
      TabIndex        =   155
      Tag             =   "VISIBLE"
      Top             =   2100
      Width           =   2385
      Begin VB.Frame fraFlightPanel 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   840
         Index           =   13
         Left            =   30
         TabIndex        =   156
         Top             =   135
         Width           =   2280
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   23
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   166
            Top             =   450
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   37
               Left            =   0
               TabIndex        =   167
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   23
               Left            =   0
               TabIndex        =   168
               Top             =   45
               Width           =   45
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   22
            Left            =   1605
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   163
            Top             =   120
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   36
               Left            =   0
               TabIndex        =   164
               Text            =   "12:10"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   22
               Left            =   0
               TabIndex        =   165
               Top             =   45
               Width           =   45
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   14
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   160
            Tag             =   "LDNS;DATE"
            Top             =   120
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   35
               Left            =   420
               TabIndex        =   161
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "ETA"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   14
               Left            =   0
               TabIndex        =   162
               Top             =   45
               Width           =   315
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   13
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1515
            TabIndex        =   157
            Tag             =   "OBNS;DATE"
            Top             =   450
            Width           =   1515
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   34
               Left            =   420
               TabIndex        =   158
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "TMO"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   13
               Left            =   0
               TabIndex        =   159
               Top             =   45
               Width           =   375
            End
         End
      End
   End
   Begin VB.Frame FlightMainInfo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1710
      Index           =   3
      Left            =   6750
      TabIndex        =   137
      Tag             =   "VISIBLE"
      Top             =   390
      Width           =   6630
      Begin VB.CheckBox chkSetup 
         Caption         =   "Button"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   8
         Left            =   5670
         Style           =   1  'Graphical
         TabIndex        =   219
         Top             =   270
         Width           =   855
      End
      Begin VB.PictureBox Picture4 
         BackColor       =   &H00FFFFFF&
         Height          =   1005
         Left            =   90
         ScaleHeight     =   945
         ScaleWidth      =   1275
         TabIndex        =   152
         Top             =   600
         Width           =   1340
      End
      Begin VB.Frame fraFlightPanel 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   1035
         Index           =   2
         Left            =   1485
         TabIndex        =   138
         Top             =   600
         Width           =   5085
         Begin VB.TextBox Text5 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2250
            TabIndex        =   150
            TabStop         =   0   'False
            Text            =   "9VABC"
            Top             =   345
            Width           =   1410
         End
         Begin VB.TextBox Text4 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2430
            Locked          =   -1  'True
            TabIndex        =   149
            TabStop         =   0   'False
            Text            =   "DLH1234 Z"
            Top             =   0
            Width           =   1230
         End
         Begin VB.TextBox txtVSA1 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   4350
            Locked          =   -1  'True
            TabIndex        =   148
            TabStop         =   0   'False
            Text            =   "SYD"
            Top             =   690
            Width           =   690
         End
         Begin VB.TextBox Text3 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1545
            Locked          =   -1  'True
            TabIndex        =   147
            TabStop         =   0   'False
            Text            =   "747"
            Top             =   345
            Width           =   690
         End
         Begin VB.TextBox Text2 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   690
            Locked          =   -1  'True
            TabIndex        =   146
            TabStop         =   0   'False
            Text            =   "B747A"
            Top             =   345
            Width           =   840
         End
         Begin VB.TextBox txtVSA1 
            Alignment       =   1  'Right Justify
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   690
            Locked          =   -1  'True
            TabIndex        =   145
            TabStop         =   0   'False
            Text            =   "BKK / SIN "
            Top             =   690
            Width           =   3630
         End
         Begin VB.PictureBox Picture2 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FF00&
            Height          =   660
            Left            =   3690
            ScaleHeight     =   600
            ScaleWidth      =   1290
            TabIndex        =   144
            Top             =   0
            Width           =   1350
            Begin VB.Image Image4 
               Height          =   510
               Left            =   45
               Picture         =   "FlightDetails.frx":014A
               Top             =   45
               Width           =   1200
            End
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   29
            Left            =   690
            TabIndex        =   143
            Text            =   "15.04.2010"
            Top             =   0
            Width           =   1065
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   28
            Left            =   1770
            TabIndex        =   142
            Text            =   "12:10"
            Top             =   0
            Width           =   645
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   27
            Left            =   0
            TabIndex        =   141
            Text            =   "INTL"
            Top             =   690
            Width           =   645
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   26
            Left            =   0
            TabIndex        =   140
            Text            =   "HDLL"
            Top             =   345
            Width           =   645
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   25
            Left            =   0
            TabIndex        =   139
            Text            =   "PAX"
            Top             =   0
            Width           =   645
         End
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Departure Flight"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5160
         TabIndex        =   154
         Top             =   0
         Width           =   1320
      End
   End
   Begin VB.Frame fraFlightPanel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   570
      Index           =   3
      Left            =   14010
      TabIndex        =   111
      Top             =   1260
      Width           =   5910
      Begin VB.Frame fraFlightPanel 
         BackColor       =   &H000000C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   375
         Index           =   15
         Left            =   30
         TabIndex        =   112
         Top             =   150
         Width           =   5835
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   38
            Left            =   90
            ScaleHeight     =   315
            ScaleWidth      =   3015
            TabIndex        =   125
            Top             =   30
            Width           =   3015
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   38
               Left            =   900
               TabIndex        =   126
               Text            =   "SUPPLIMANTARY PAX"
               Top             =   0
               Width           =   1995
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "SI Text PX"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   38
               Left            =   0
               TabIndex        =   127
               Top             =   45
               Width           =   810
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   42
            Left            =   3120
            ScaleHeight     =   315
            ScaleWidth      =   930
            TabIndex        =   122
            Tag             =   "|PXEF,PXNO,PAX3,PAX1"
            Top             =   30
            Width           =   930
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   42
               Left            =   300
               MaxLength       =   3
               TabIndex        =   123
               Text            =   "123"
               ToolTipText     =   "EXCLDG INFANTS"
               Top             =   0
               Width           =   570
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "PX"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   42
               Left            =   0
               TabIndex        =   124
               Top             =   45
               Width           =   210
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   41
            Left            =   4005
            ScaleHeight     =   315
            ScaleWidth      =   630
            TabIndex        =   119
            Tag             =   "|PXIF,PXNO,PAX1,PAX2"
            Top             =   30
            Width           =   630
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   41
               Left            =   0
               MaxLength       =   3
               TabIndex        =   120
               Text            =   "123"
               ToolTipText     =   "INCLDG INFANTS"
               Top             =   0
               Width           =   570
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "(M)"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   41
               Left            =   0
               TabIndex        =   121
               Top             =   45
               Width           =   270
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   40
            Left            =   4590
            ScaleHeight     =   315
            ScaleWidth      =   630
            TabIndex        =   116
            Top             =   30
            Width           =   630
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   40
               Left            =   0
               MaxLength       =   3
               TabIndex        =   117
               Text            =   "123"
               ToolTipText     =   "NOT USED"
               Top             =   0
               Width           =   570
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "DT"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   40
               Left            =   0
               TabIndex        =   118
               Top             =   45
               Width           =   210
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   39
            Left            =   5235
            ScaleHeight     =   315
            ScaleWidth      =   570
            TabIndex        =   113
            Tag             =   "|PXIN,PXNO,PAX2,PAX4"
            Top             =   30
            Width           =   570
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   39
               Left            =   0
               MaxLength       =   3
               TabIndex        =   114
               Text            =   "123"
               ToolTipText     =   "INFANTS"
               Top             =   0
               Width           =   510
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "(M)"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   39
               Left            =   0
               TabIndex        =   115
               Top             =   45
               Width           =   270
            End
         End
      End
   End
   Begin VB.Frame fraFlightPanel 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   900
      Index           =   9
      Left            =   14280
      TabIndex        =   85
      Top             =   0
      Width           =   5910
      Begin VB.Frame fraFlightPanel 
         BackColor       =   &H000000C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   705
         Index           =   10
         Left            =   30
         TabIndex        =   86
         Top             =   270
         Visible         =   0   'False
         Width           =   5835
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   15
            Left            =   3120
            ScaleHeight     =   315
            ScaleWidth      =   825
            TabIndex        =   108
            Tag             =   "|DCO1,CODE,DCD1,DCD1"
            Top             =   30
            Width           =   825
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   15
               Left            =   300
               MaxLength       =   2
               TabIndex        =   109
               Text            =   "MM"
               Top             =   0
               Width           =   495
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "DL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   15
               Left            =   0
               TabIndex        =   110
               Top             =   45
               Width           =   210
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   16
            Left            =   3930
            ScaleHeight     =   315
            ScaleWidth      =   540
            TabIndex        =   105
            Tag             =   "|DCO2,CODE,DCD2,DCD2"
            Top             =   30
            Width           =   540
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   16
               Left            =   0
               MaxLength       =   2
               TabIndex        =   106
               Text            =   "MM"
               Top             =   0
               Width           =   495
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "(M)"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   16
               Left            =   0
               TabIndex        =   107
               Top             =   45
               Width           =   270
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   17
            Left            =   4440
            ScaleHeight     =   315
            ScaleWidth      =   675
            TabIndex        =   102
            Tag             =   "|DUR1,DLYT,DTD1,DTD1"
            Top             =   30
            Width           =   675
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   17
               Left            =   0
               TabIndex        =   103
               Text            =   "0025"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "DT"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   17
               Left            =   0
               TabIndex        =   104
               Top             =   45
               Width           =   210
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   18
            Left            =   5100
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   99
            Tag             =   "|DUR2,DLYT,DTD2,DTD2"
            Top             =   30
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   18
               Left            =   0
               TabIndex        =   100
               Text            =   "0010"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "(M)"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   18
               Left            =   0
               TabIndex        =   101
               Top             =   45
               Width           =   270
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   19
            Left            =   90
            ScaleHeight     =   315
            ScaleWidth      =   1965
            TabIndex        =   96
            Top             =   30
            Width           =   1965
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   19
               Left            =   900
               TabIndex        =   97
               Text            =   " [DELAYED]"
               Top             =   0
               Width           =   1035
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "DL Status"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   19
               Left            =   0
               TabIndex        =   98
               Top             =   45
               Width           =   780
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   20
            Left            =   2040
            ScaleHeight     =   315
            ScaleWidth      =   615
            TabIndex        =   93
            Top             =   30
            Width           =   615
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   20
               Left            =   0
               TabIndex        =   94
               Text            =   " 0035"
               Top             =   0
               Width           =   585
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   ".."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   20
               Left            =   0
               TabIndex        =   95
               Top             =   45
               Width           =   90
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   21
            Left            =   2640
            ScaleHeight     =   315
            ScaleWidth      =   465
            TabIndex        =   90
            Top             =   30
            Width           =   465
            Begin VB.TextBox AftFldText 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   21
               Left            =   0
               TabIndex        =   91
               Text            =   "30"
               Top             =   0
               Width           =   345
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   " "
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   21
               Left            =   0
               TabIndex        =   92
               Top             =   45
               Width           =   45
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   3
            Left            =   90
            ScaleHeight     =   315
            ScaleWidth      =   5715
            TabIndex        =   87
            Top             =   360
            Width           =   5715
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   3
               Left            =   900
               TabIndex        =   88
               Text            =   "DELAYED"
               Top             =   0
               Width           =   4755
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "SI Text DL"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   3
               Left            =   0
               TabIndex        =   89
               Top             =   45
               Width           =   810
            End
         End
      End
   End
   Begin VB.Frame fraFlightPanel 
      Caption         =   "Departure from PRV"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1080
      Index           =   11
      Left            =   13710
      TabIndex        =   79
      Top             =   4860
      Width           =   2895
      Begin VB.Frame fraFlightPanel 
         BackColor       =   &H000040C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   840
         Index           =   12
         Left            =   750
         TabIndex        =   80
         Top             =   195
         Width           =   2820
      End
   End
   Begin VB.Frame fraFlightPanel 
      Caption         =   "Arrival at NXT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1080
      Index           =   5
      Left            =   13920
      TabIndex        =   65
      Top             =   3840
      Width           =   2895
      Begin VB.Frame fraFlightPanel 
         BackColor       =   &H000040C0&
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   840
         Index           =   8
         Left            =   30
         TabIndex        =   66
         Top             =   195
         Width           =   2820
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   9
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1995
            TabIndex        =   75
            Tag             =   "|EETA,DATE,EANS"
            Top             =   450
            Width           =   1995
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   9
               Left            =   900
               TabIndex        =   76
               Text            =   "31.12.2009"
               Top             =   0
               Width           =   1065
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "ETA"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   9
               Left            =   0
               TabIndex        =   78
               Top             =   45
               Width           =   315
            End
            Begin VB.Label AftFldLabel 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "[A]"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000011&
               Height          =   210
               Index           =   10
               Left            =   600
               TabIndex        =   77
               Top             =   45
               Width           =   240
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   10
            Left            =   2085
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   73
            Tag             =   "|EETA,TIME,EANS"
            Top             =   450
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   10
               Left            =   0
               TabIndex        =   74
               Text            =   "12:00"
               Top             =   0
               Width           =   645
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Height          =   315
            Index           =   1
            Left            =   105
            ScaleHeight     =   315
            ScaleWidth      =   1785
            TabIndex        =   70
            Tag             =   "|EETF,HHMM,EETH,EETH"
            Top             =   120
            Width           =   1785
            Begin VB.TextBox AftFldText 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   1
               Left            =   1110
               MaxLength       =   4
               TabIndex        =   71
               Text            =   "0235"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "EET to MMM"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   1
               Left            =   0
               TabIndex        =   72
               Top             =   45
               Width           =   990
            End
         End
         Begin VB.PictureBox AftFldPanel 
            BackColor       =   &H000080FF&
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   315
            Index           =   2
            Left            =   1875
            ScaleHeight     =   315
            ScaleWidth      =   705
            TabIndex        =   67
            Tag             =   "|EETF,LONG,EETM,EETM"
            Top             =   120
            Width           =   705
            Begin VB.TextBox AftFldText 
               Alignment       =   1  'Right Justify
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   2
               Left            =   0
               MaxLength       =   4
               TabIndex        =   68
               Text            =   "155"
               Top             =   0
               Width           =   645
            End
            Begin VB.Label AftFldLabel 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "(M)"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   210
               Index           =   2
               Left            =   0
               TabIndex        =   69
               Top             =   45
               Width           =   270
            End
         End
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3645
      Left            =   30
      TabIndex        =   61
      Top             =   3120
      Width           =   3255
      Begin VB.PictureBox FltListPanel 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   0
         Left            =   90
         ScaleHeight     =   2985
         ScaleWidth      =   3015
         TabIndex        =   64
         Top             =   510
         Width           =   3075
         Begin TABLib.TAB TabFlights 
            Height          =   3015
            Index           =   0
            Left            =   0
            TabIndex        =   216
            Top             =   0
            Width           =   3030
            _Version        =   65536
            _ExtentX        =   5345
            _ExtentY        =   5318
            _StockProps     =   64
         End
      End
   End
   Begin VB.Frame FlightMainInfo 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Departure Flight Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Index           =   1
      Left            =   9510
      TabIndex        =   60
      Tag             =   "VISIBLE"
      Top             =   390
      Width           =   3300
   End
   Begin VB.Frame FlightMainInfo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1710
      Index           =   0
      Left            =   30
      TabIndex        =   59
      Tag             =   "VISIBLE"
      Top             =   390
      Width           =   6660
      Begin VB.CheckBox chkSetup 
         Caption         =   "Button"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   218
         Top             =   270
         Width           =   855
      End
      Begin VB.PictureBox Picture3 
         BackColor       =   &H00FFFFFF&
         Height          =   1005
         Left            =   5205
         ScaleHeight     =   945
         ScaleWidth      =   1275
         TabIndex        =   151
         Top             =   600
         Width           =   1340
      End
      Begin VB.Frame fraFlightPanel 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   1035
         Index           =   1
         Left            =   90
         TabIndex        =   81
         Top             =   600
         Width           =   5085
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   24
            Left            =   4410
            TabIndex        =   136
            Text            =   "PAX"
            Top             =   0
            Width           =   645
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   23
            Left            =   4410
            TabIndex        =   135
            Text            =   "HDLL"
            Top             =   345
            Width           =   645
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   22
            Left            =   4410
            TabIndex        =   134
            Text            =   "INTL"
            Top             =   690
            Width           =   645
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   14
            Left            =   3705
            TabIndex        =   133
            Text            =   "12:10"
            Top             =   0
            Width           =   645
         End
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   13
            Left            =   2625
            TabIndex        =   132
            Text            =   "15.04.2010"
            Top             =   0
            Width           =   1065
         End
         Begin VB.PictureBox Picture1 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            Height          =   660
            Left            =   0
            ScaleHeight     =   600
            ScaleWidth      =   1290
            TabIndex        =   131
            Top             =   0
            Width           =   1350
            Begin VB.Image Image3 
               Height          =   510
               Left            =   45
               Picture         =   "FlightDetails.frx":102C
               Top             =   45
               Width           =   1200
            End
         End
         Begin VB.TextBox txtVSA1 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   720
            Locked          =   -1  'True
            TabIndex        =   130
            TabStop         =   0   'False
            Text            =   " FRA / ATH"
            Top             =   690
            Width           =   3630
         End
         Begin VB.TextBox txtACTP 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   3510
            Locked          =   -1  'True
            TabIndex        =   129
            TabStop         =   0   'False
            Text            =   "B747A"
            Top             =   345
            Width           =   840
         End
         Begin VB.TextBox Text1 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2805
            Locked          =   -1  'True
            TabIndex        =   128
            TabStop         =   0   'False
            Text            =   "747"
            Top             =   345
            Width           =   690
         End
         Begin VB.TextBox txtVSA1 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   84
            TabStop         =   0   'False
            Text            =   "LHR"
            Top             =   690
            Width           =   690
         End
         Begin VB.TextBox txtFLNO 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1380
            Locked          =   -1  'True
            TabIndex        =   83
            TabStop         =   0   'False
            Text            =   "DLH1234 Z"
            Top             =   0
            Width           =   1230
         End
         Begin VB.TextBox txtREGN 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1380
            TabIndex        =   82
            TabStop         =   0   'False
            Text            =   "9VABC"
            Top             =   345
            Width           =   1410
         End
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Arrival Flight"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   150
         TabIndex        =   153
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox MainSplitHorzS 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   1740
      ScaleHeight     =   195
      ScaleWidth      =   180
      TabIndex        =   57
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.PictureBox MainSplitHorzM 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   1470
      ScaleHeight     =   195
      ScaleWidth      =   180
      TabIndex        =   56
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2235
      Index           =   7
      Left            =   13710
      TabIndex        =   45
      Top             =   2880
      Visible         =   0   'False
      Width           =   5235
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Panel Borders"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1860
         Index           =   4
         Left            =   2820
         TabIndex        =   52
         Top             =   210
         Width           =   2235
         Begin VB.CheckBox chkBorder 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Life Style Border"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   180
            TabIndex        =   55
            Top             =   990
            Width           =   1905
         End
         Begin VB.Frame fraSetupX 
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   540
            Index           =   5
            Left            =   150
            TabIndex        =   53
            Top             =   1170
            Width           =   1965
            Begin MSComctlLib.Slider PanelSizeSlider 
               Height          =   315
               Index           =   0
               Left            =   60
               TabIndex        =   54
               Top             =   165
               Width           =   1845
               _ExtentX        =   3254
               _ExtentY        =   556
               _Version        =   393216
               LargeChange     =   4
               SmallChange     =   2
               Max             =   20
               SelStart        =   3
               TickStyle       =   3
               Value           =   3
               TextPosition    =   1
            End
         End
      End
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Application Resizing ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1860
         Index           =   3
         Left            =   150
         TabIndex        =   46
         Top             =   210
         Width           =   2535
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Moves all panels"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   4
            Left            =   120
            TabIndex        =   58
            Tag             =   "4"
            Top             =   1260
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Resizes the window"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   5
            Left            =   120
            TabIndex        =   51
            Tag             =   "5"
            Top             =   1500
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Moves main and right"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   3
            Left            =   120
            TabIndex        =   50
            Tag             =   "3"
            Top             =   1020
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Moves the right panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   120
            TabIndex        =   49
            Tag             =   "2"
            Top             =   780
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Centers the main panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   120
            TabIndex        =   48
            Tag             =   "1"
            Top             =   540
            Width           =   2280
         End
         Begin VB.OptionButton optResize 
            BackColor       =   &H00FFC0C0&
            Caption         =   "Resizes the work area"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   47
            Tag             =   "0"
            Top             =   300
            Width           =   2280
         End
      End
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Index           =   0
      Left            =   15570
      TabIndex        =   40
      Top             =   390
      Visible         =   0   'False
      Width           =   1875
      Begin VB.Frame fraSetupX 
         BackColor       =   &H00FFC0C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Index           =   0
         Left            =   0
         TabIndex        =   41
         Top             =   165
         Width           =   1845
         Begin VB.CheckBox chkSetup 
            Caption         =   "Layout"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   7
            Left            =   930
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   165
            Width           =   855
         End
         Begin VB.CheckBox chkSetup 
            Caption         =   "Demo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   6
            Left            =   2070
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   165
            Width           =   855
         End
         Begin VB.CheckBox chkSetup 
            Caption         =   "Loader"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   5
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   165
            Width           =   855
         End
      End
   End
   Begin VB.PictureBox MainTool 
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   0
      Left            =   2100
      ScaleHeight     =   345
      ScaleWidth      =   1185
      TabIndex        =   29
      Top             =   7890
      Visible         =   0   'False
      Width           =   1185
      Begin VB.Image Image2 
         Height          =   375
         Left            =   450
         Picture         =   "FlightDetails.frx":1F0E
         Top             =   -15
         Width           =   375
      End
      Begin VB.Image Image1 
         Height          =   375
         Left            =   780
         Picture         =   "FlightDetails.frx":20E0
         Top             =   -15
         Width           =   375
      End
   End
   Begin VB.PictureBox SubSplitHorzS 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   570
      MousePointer    =   7  'Size N S
      ScaleHeight     =   195
      ScaleWidth      =   420
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Frame SubFrameS 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   0
      Left            =   3420
      TabIndex        =   27
      Top             =   7890
      Visible         =   0   'False
      Width           =   315
   End
   Begin VB.PictureBox SubPanelS 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   570
      ScaleHeight     =   315
      ScaleWidth      =   450
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   7530
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.PictureBox SubSplitVertS 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   1065
      Index           =   0
      Left            =   1290
      ScaleHeight     =   1065
      ScaleWidth      =   165
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   165
      Begin VB.Image PicVSplitS 
         Enabled         =   0   'False
         Height          =   480
         Index           =   0
         Left            =   -960
         Picture         =   "FlightDetails.frx":23B8
         Stretch         =   -1  'True
         Top             =   0
         Width           =   1125
      End
   End
   Begin VB.PictureBox MainPanel 
      BackColor       =   &H0000C000&
      Height          =   465
      Index           =   1
      Left            =   1470
      ScaleHeight     =   405
      ScaleWidth      =   510
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   570
   End
   Begin VB.PictureBox SubSplitVertM 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   1065
      Index           =   0
      Left            =   1050
      ScaleHeight     =   1065
      ScaleWidth      =   165
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   165
      Begin VB.Image PicVSplitM 
         Enabled         =   0   'False
         Height          =   480
         Index           =   0
         Left            =   0
         Picture         =   "FlightDetails.frx":3FFA
         Stretch         =   -1  'True
         Top             =   0
         Width           =   1125
      End
   End
   Begin VB.PictureBox SubSplitHorzM 
      BackColor       =   &H00E0E0E0&
      Height          =   255
      Index           =   0
      Left            =   30
      MousePointer    =   7  'Size N S
      ScaleHeight     =   195
      ScaleWidth      =   450
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   7860
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.PictureBox SubPanelM 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   30
      ScaleHeight     =   315
      ScaleWidth      =   510
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   7530
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.PictureBox MainPanel 
      BackColor       =   &H00C00000&
      Height          =   465
      Index           =   0
      Left            =   30
      ScaleHeight     =   405
      ScaleWidth      =   960
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   7050
      Visible         =   0   'False
      Width           =   1020
   End
   Begin VB.Frame fraSetup 
      BackColor       =   &H00FFC0C0&
      Caption         =   "Flight Data Periods"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Index           =   5
      Left            =   12150
      TabIndex        =   19
      Top             =   7200
      Visible         =   0   'False
      Width           =   2265
      Begin VB.TextBox AftVpto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1680
         TabIndex        =   33
         Text            =   "+2"
         Top             =   1230
         Width           =   345
      End
      Begin VB.TextBox AftVpto 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   1680
         TabIndex        =   32
         Text            =   "+1"
         Top             =   570
         Width           =   345
      End
      Begin VB.TextBox AftVpfr 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   900
         TabIndex        =   31
         Text            =   "0"
         Top             =   1230
         Width           =   345
      End
      Begin VB.TextBox AftVpfr 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   900
         TabIndex        =   30
         Text            =   "-1"
         Top             =   570
         Width           =   345
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Minus"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   360
         TabIndex        =   39
         Top             =   1290
         Width           =   510
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Minus"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   360
         TabIndex        =   38
         Top             =   630
         Width           =   510
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Plus"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1290
         TabIndex        =   37
         Top             =   630
         Width           =   360
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Plus"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1290
         TabIndex        =   36
         Top             =   1290
         Width           =   360
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departure Flights"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   180
         TabIndex        =   35
         Top             =   960
         Width           =   1425
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Arrival Flights"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   180
         TabIndex        =   34
         Top             =   300
         Width           =   1140
      End
   End
   Begin VB.Frame fraFlightPanel 
      BackColor       =   &H000040C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   840
      Index           =   6
      Left            =   15510
      TabIndex        =   6
      Top             =   1890
      Visible         =   0   'False
      Width           =   2820
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   2085
         ScaleHeight     =   315
         ScaleWidth      =   705
         TabIndex        =   7
         Tag             =   "|OFBL,TIME"
         Top             =   450
         Width           =   705
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            TabIndex        =   8
            Text            =   "12:10"
            Top             =   0
            Width           =   645
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   0
            Left            =   0
            TabIndex        =   9
            Top             =   45
            Width           =   45
         End
      End
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   11
         Left            =   2085
         ScaleHeight     =   315
         ScaleWidth      =   705
         TabIndex        =   13
         Tag             =   "|ETDI,TIME"
         Top             =   120
         Width           =   705
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   11
            Left            =   0
            TabIndex        =   14
            Text            =   "12:10"
            Top             =   0
            Width           =   645
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " "
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   11
            Left            =   0
            TabIndex        =   15
            Top             =   45
            Width           =   45
         End
      End
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   12
         Left            =   105
         ScaleHeight     =   315
         ScaleWidth      =   1995
         TabIndex        =   16
         Tag             =   "|ETDI,DATE"
         Top             =   120
         Width           =   1995
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   12
            Left            =   900
            TabIndex        =   17
            Text            =   "31.12.2009"
            Top             =   0
            Width           =   1065
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "ETD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   12
            Left            =   0
            TabIndex        =   18
            Top             =   45
            Width           =   300
         End
      End
      Begin VB.PictureBox AftFldPanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   315
         Index           =   4
         Left            =   105
         ScaleHeight     =   315
         ScaleWidth      =   1995
         TabIndex        =   10
         Tag             =   "|OFBL,DATE"
         Top             =   450
         Width           =   1995
         Begin VB.TextBox AftFldText 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   900
            TabIndex        =   11
            Text            =   "31.12.2009"
            Top             =   0
            Width           =   1065
         End
         Begin VB.Label AftFldLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "OFB (OUT)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Index           =   4
            Left            =   0
            TabIndex        =   12
            Top             =   45
            Width           =   810
         End
      End
   End
   Begin TABLib.TAB FldPropTab 
      Height          =   375
      Index           =   0
      Left            =   8250
      TabIndex        =   2
      Top             =   7740
      Visible         =   0   'False
      Width           =   3285
      _Version        =   65536
      _ExtentX        =   5794
      _ExtentY        =   661
      _StockProps     =   64
   End
   Begin VB.Frame fraFlightData 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3645
      Left            =   10110
      TabIndex        =   1
      Top             =   3120
      Width           =   3255
      Begin VB.PictureBox FltListPanel 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3045
         Index           =   1
         Left            =   90
         ScaleHeight     =   2985
         ScaleWidth      =   3015
         TabIndex        =   62
         Top             =   510
         Width           =   3075
         Begin TABLib.TAB TabFlights 
            Height          =   3015
            Index           =   1
            Left            =   0
            TabIndex        =   63
            Top             =   0
            Width           =   3000
            _Version        =   65536
            _ExtentX        =   5292
            _ExtentY        =   5318
            _StockProps     =   64
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8130
      Width           =   20370
      _ExtentX        =   35930
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   32835
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraTopButtons 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Index           =   0
      Left            =   -30
      TabIndex        =   3
      Top             =   -120
      Width           =   13395
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   405
         Left            =   3690
         TabIndex        =   229
         Top             =   120
         Width           =   2325
      End
      Begin VB.CheckBox chkExit 
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   12510
         Style           =   1  'Graphical
         TabIndex        =   215
         Top             =   135
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   11640
         Style           =   1  'Graphical
         TabIndex        =   214
         Top             =   135
         Width           =   855
      End
      Begin VB.CheckBox OpenPool 
         Caption         =   "Main"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   213
         Top             =   135
         Width           =   855
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "On Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   45
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   135
         Width           =   840
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Setup"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   2700
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   135
         Visible         =   0   'False
         Width           =   855
      End
   End
End
Attribute VB_Name = "FlightDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_Snap As CSnapDialog

Dim TplFldCreateEditor As Boolean
Dim TplMainFieldsOnly As Boolean
Dim TplUpdateFldGroups As Boolean
Dim TplUpdateCascading As Boolean
Dim TplUpdateSingleFld As Boolean
Dim ShowFldDsscColFlag As Boolean

Dim TestScenario As Integer
Dim ShowHorzSplit As Boolean
Dim ShowVertSplit As Boolean

Dim MeIsVisible As Boolean
Dim TransmitEnabled As Boolean
Dim AutoSendAddress As String
Dim ActiveSendAddress As String
Dim CurFltTabOcx As TABLib.Tab
Dim CurFltLineNo As Long
Dim CurFltRecIsAft As Boolean
Dim CurFltRecIsTlx As Boolean
Dim CurFltAdidIsArr As Boolean
Dim CurFltAdidIsDep As Boolean

Dim CurTabOcxFldLst As String

Dim CfgAftRecFldNames As String
Dim CfgTplRecFldNames As String
Dim CfgTplRecFldTypes As String
Dim CfgTplFldDatTypes As String
Dim CfgTplFldDatProps As String

Dim MyTlxTabFields As String
Dim MyTlxTabRecord As String
Dim MyAftTabFields As String
Dim MyAftTabRecord As String
Dim MyTlxTemplate As String
Dim CurTlxTplText As String
Dim CurTlxTplType As String
Dim CurTplFldList As String

Dim CurViewPoint As String
Dim CurAftFltUrno As String
Dim CurTlxFltUrno As String
Dim CurTlxTabUrno As String
Dim CurTlxTplUrno As String
Dim CurNewTplText As String
Dim CurUsrTplText As String
Dim TplTextIsUser As Boolean

Dim SereCol As Integer
Dim TtypCol As Integer
Dim Txt1Col As Integer
Dim Txt2Col As Integer
Dim StatCol As Integer
Dim WstaCol As Integer
Dim PrflCol As Integer
Dim FlnuCol As Integer
Dim UrnoCol As Integer

Dim TplRuleLineNo As Long
Dim TplTextLineNo As Long
Dim TplText As String
Dim SystemInput As Boolean
Dim FltEditSize As Long
Dim OutTlxIdx As Integer
Dim KeepCurrentView As Boolean
Dim MouseIsDown As Boolean
Dim MouseX As Long
Dim MouseY As Long
Dim FlightIsOnTime As Boolean
Dim FlightIsDelayed As Boolean
Dim TemplateHasDelayLine As Boolean
Dim TemplateHasPaxLine As Boolean
Dim TelexTextIsClean As Boolean
Dim TelexAddrIsClean As Boolean
Dim TelexOrigIsClean As Boolean

Private Function GetShortViaList(AftVial As String) As String
    Dim TlxVial As String
    Dim CurApc3 As String
    Dim ApcPos As Integer
    Dim MaxPos As Integer
    TlxVial = ""
    ApcPos = 2
    MaxPos = Len(AftVial)
    CurApc3 = "START"
    While CurApc3 <> ""
        CurApc3 = ""
        If ApcPos < MaxPos Then CurApc3 = Trim(Mid(AftVial, ApcPos, 3))
        If CurApc3 <> "" Then TlxVial = TlxVial & CurApc3 & "/"
        ApcPos = ApcPos + 120
    Wend
    MaxPos = Len(TlxVial) - 1
    If MaxPos > 0 Then TlxVial = Left(TlxVial, MaxPos)
    GetShortViaList = TlxVial
End Function
Private Function GetNxtStation(AftFldList As String, AftFldData As String) As String
    Dim NxtApc3 As String
    NxtApc3 = GetFieldValue("VIA3", AftFldData, AftFldList)
    If NxtApc3 = "" Then NxtApc3 = Left(Trim(GetFieldValue("VIAL", AftFldData, AftFldList)), 3)
    If NxtApc3 = "" Then NxtApc3 = GetFieldValue("DES3", AftFldData, AftFldList)
    GetNxtStation = NxtApc3
End Function
Private Function GetPrvStation(AftFldList As String, AftFldData As String) As String
    Dim PrvApc3 As String
    PrvApc3 = GetFieldValue("VIA3", AftFldData, AftFldList)
    If PrvApc3 = "" Then PrvApc3 = GetFieldValue("ORG3", AftFldData, AftFldList)
    GetPrvStation = PrvApc3
End Function

Private Sub chkGetCfg_Click()

End Sub

Private Sub chkSelect_Click()

End Sub

Private Sub Command1_Click()
    TestSKin.Show
End Sub

Private Sub Form_Load()
    Dim tmpData As String
    Dim tmpStrg As String
    Dim tmpFields As String
    Dim MainTop As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewSize As Long
    Dim i As Integer
    
Exit Sub
    MeIsVisible = False
    GeneratorIsReadyForUse = False
    On Error Resume Next
        If (AutoSnapMvtGenerator) Then
            If Not ApplIsInDesignMode Then
                Set m_Snap = New CSnapDialog
                m_Snap.hwnd = Me.hwnd
            End If
        End If
    MainPanel(0).Visible = True
    Me.Move 0, Screen.Height, 600 * 15, 600 * 15
    
    ShowHorzSplit = False
    ShowVertSplit = True
    MainSplitHorzM(0).Height = 90
    MainSplitHorzM(0).BorderStyle = 0
    MainSplitHorzS(0).Height = 90
    MainSplitHorzS(0).BorderStyle = 0
    SubSplitHorzM(0).Height = 90
    SubSplitHorzM(0).BorderStyle = 0
    SubSplitHorzS(0).Height = 90
    SubSplitHorzS(0).BorderStyle = 0
    
    SubSplitVertM(0).Width = 45
    SubSplitVertS(0).Width = 45
    
    MainTop = fraTopButtons(0).Top + fraTopButtons(0).Height
    MainTool(0).Top = MainTop
    'MainTop = MainTop + MainTool(0).Height
    MainPanel(0).Top = MainTop
    MainPanel(0).Left = 0
    MainPanel(0).ZOrder
    
    
    CreateSubPanels 2
    SubSplitVertM(1).ZOrder
    SubSplitVertS(1).ZOrder
    
    NewTop = MainSplitHorzM(0).Height
    NewLeft = 0 - SubSplitVertM(0).Width
    NewLeft = 0
    For i = 0 To SubPanelM.UBound
        Set MainSplitHorzM(i).Container = MainPanel(0)
        MainSplitHorzM(i).Top = 0
        MainSplitHorzM(i).Left = NewLeft
        MainSplitHorzM(i).Visible = True
        MainSplitHorzM(i).ZOrder
        
        Set SubSplitVertM(i).Container = MainPanel(0)
        SubSplitVertM(i).Top = 0
        SubSplitVertM(i).Left = NewLeft
        SubSplitVertM(i).Visible = True
        NewLeft = NewLeft + SubSplitVertM(i).Width
        Set SubPanelM(i).Container = MainPanel(0)
        SubPanelM(i).Top = NewTop
        SubPanelM(i).Left = NewLeft
        SubPanelM(i).Visible = True
        NewLeft = NewLeft + SubPanelM(i).Width
        Set SubSplitVertS(i).Container = MainPanel(0)
        SubSplitVertS(i).Top = 0
        SubSplitVertS(i).Left = NewLeft
        SubSplitVertS(i).Visible = True
        NewLeft = NewLeft + SubSplitVertM(i).Width
        Set SubPanelS(i).Container = MainPanel(0)
        SubPanelS(i).Top = 0
        SubPanelS(i).Left = NewLeft
        SubFrameS(i).Visible = False
        SubPanelS(i).Visible = True
        PicVSplitM(i).Height = Screen.Height
        PicVSplitS(i).Height = Screen.Height
        PicVSplitM(i).Visible = False
        PicVSplitS(i).Visible = False
        SubSplitHorzS(i).Visible = False
        
        Set MainSplitHorzS(i).Container = MainPanel(0)
        MainSplitHorzS(i).Top = MainPanel(0).ScaleHeight - MainSplitHorzS(i).Height
        MainSplitHorzS(i).Left = NewLeft
        MainSplitHorzS(i).Visible = True
        MainSplitHorzS(i).ZOrder
        NewLeft = NewLeft + SubPanelS(i).Width
    Next
    
    'Configure Startup Values
    optResize(3).Value = True
    
    'chkBorder(0).Value = 1
    chkBorder(0).Value = 0
    chkBorder(0).Value = 1
    
    InitSubPanelMs -1
    
    
    
    For i = 0 To fraTopButtons.UBound
        fraTopButtons(i).BackColor = MyOwnButtonFace
    Next
    'fraTopButtons(1).ZOrder
    fraTopButtons(2).ZOrder
    
    For i = 0 To fraFlightPanel.UBound
        fraFlightPanel(i).BackColor = MyOwnButtonFace
        fraFlightPanel(i).BackColor = LightGray
    Next
    For i = 0 To AftFldText.UBound
        AftFldPanel(i).BackColor = MyOwnButtonFace
        AftFldText(i).Text = ""
        AftFldText(i).BackColor = vbWhite
        AftFldText(i).ForeColor = vbBlack
    Next
    
    
    TabFlights(0).Top = 0
    TabFlights(0).Left = 0
    TabFlights(0).Width = FltListPanel(2).ScaleWidth
    TabFlights(0).Height = FltListPanel(2).ScaleHeight
    TabFlights(1).Top = 0
    TabFlights(1).Left = 0
    TabFlights(1).Width = FltListPanel(2).ScaleWidth
    TabFlights(1).Height = FltListPanel(2).ScaleHeight
    
    StatusBar1.ZOrder
    
    FldPropTab(0).ResetContent
    tmpData = "TPOS,FINA,REPL,TYPE,LBL1,LBL2,DATA,DSSC"
    tmpStrg = "10,10,10,10,10,10,10,10"
    FldPropTab(0).HeaderString = tmpData
    FldPropTab(0).LogicalFieldList = tmpData
    FldPropTab(0).ColumnWidthString = tmpStrg
    FldPropTab(0).HeaderLengthString = tmpStrg
    FldPropTab(0).AutoSizeByHeader = True
    FldPropTab(0).AutoSizeColumns
    
    
    TabFlights(0).ResetContent
    tmpFields = "FLNO,STOA,LAND,ONBL,ORG3,VIA3,ETAI,ADID,REGN,ACT5,ACT3,DES3,VIAL,STOD,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(0).LogicalFieldList = tmpFields
    TabFlights(0).HeaderString = "FLNO,STOA,LAND,ONBL,ORG,VIA,ETAI,ADID,REGN,ACT5,ACT3,DES3,VIAL,STOD,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(0).HeaderLengthString = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100"
    TabFlights(0).ColumnAlignmentString = "L,C,C,C,C,C,C,C,L,C,C,C,L,C,L,L,L,L,L,L"
    TabFlights(0).HeaderAlignmentString = "L,C,C,C,C,C,C,C,L,C,C,C,L,C,L,L,L,L,L,L"
    TabFlights(0).FontName = "Courier New"
    TabFlights(0).HeaderFontSize = MyFontSize
    TabFlights(0).FontSize = MyFontSize
    TabFlights(0).lineHeight = MyFontSize
    TabFlights(0).SetTabFontBold MyFontBold
    TabFlights(0).MainHeader = True
    TabFlights(0).SetMainHeaderFont MyFontSize, False, False, True, 0, "Courier New"
    TabFlights(0).SetMainHeaderValues "4,16", "Actual ARR Flights,", ","
    TabFlights(0).DateTimeSetColumn 1
    TabFlights(0).DateTimeSetInputFormatString 1, "YYYYMMDDhhmmss"
    TabFlights(0).DateTimeSetOutputFormatString 1, "hhmm"
    TabFlights(0).DateTimeSetColumn 2
    TabFlights(0).DateTimeSetInputFormatString 2, "YYYYMMDDhhmmss"
    TabFlights(0).DateTimeSetOutputFormatString 2, "hhmm"
    TabFlights(0).DateTimeSetColumn 3
    TabFlights(0).DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
    TabFlights(0).DateTimeSetOutputFormatString 3, "hhmm"
    TabFlights(0).ShowHorzScroller True
    TabFlights(0).LifeStyle = True
    TabFlights(0).CursorLifeStyle = True
    TabFlights(0).AutoSizeByHeader = True
    TabFlights(0).AutoSizeColumns
    
    TabFlights(1).ResetContent
    tmpFields = "FLNO,STOD,DES3,VIA3,ETDI,REGN,OFBL,AIRB,ACT5,ACT3,BAAA,ETAI,DCD1,DCD2,DTD1,DTD2,DELD,PAX1,PAX2,PAX3,ADID,VIAL,ORG3,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(1).LogicalFieldList = tmpFields
    TabFlights(1).HeaderString = "FLNO,STOD,DEST,VIA,ETDI,REGN,OFBL,AIRB,ACT5,ACT3,BAAA,ETAI,DCD1,DCD2,DTD1,DTD2,DELD,PAX1,PAX2,PAX3,ADID,VIAL,ORG3,ALC3,FLTN,FLNS,FKEY,FLDA,URNO"
    TabFlights(1).HeaderLengthString = "100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100"
    TabFlights(1).ColumnAlignmentString = "L,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,L,L,L,L,L,L"
    TabFlights(1).HeaderAlignmentString = "L,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,L,L,L,L,L,L"
    TabFlights(1).FontName = "Courier New"
    TabFlights(1).HeaderFontSize = MyFontSize
    TabFlights(1).FontSize = MyFontSize
    TabFlights(1).lineHeight = MyFontSize
    TabFlights(1).SetTabFontBold MyFontBold
    TabFlights(1).MainHeader = True
    TabFlights(1).SetMainHeaderFont MyFontSize, False, False, True, 0, "Courier New"
    TabFlights(1).SetMainHeaderValues "4,25", "Actual DEP Flights,", ","
    TabFlights(1).DateTimeSetColumn 1
    TabFlights(1).DateTimeSetInputFormatString 1, "YYYYMMDDhhmmss"
    TabFlights(1).DateTimeSetOutputFormatString 1, "hhmm"
    TabFlights(1).DateTimeSetColumn 4
    TabFlights(1).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
    TabFlights(1).DateTimeSetOutputFormatString 4, "hhmm"
    TabFlights(1).ShowHorzScroller True
    TabFlights(1).LifeStyle = True
    TabFlights(1).CursorLifeStyle = True
    TabFlights(1).AutoSizeByHeader = True
    TabFlights(1).AutoSizeColumns
    
    CreateFlightDataPanel "", "", "", ""
    Me.Show
    Me.Refresh
    Screen.MousePointer = 11
    Screen.MousePointer = 0
    tmpStrg = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_BUTTON_SETUP", "NO")
    If tmpStrg = "YES" Then
        chkSetup(0).Visible = True
    Else
        chkSetup(0).Visible = False
        chkSetup(1).Left = chkSetup(0).Left
        chkSetup(2).Left = chkSetup(0).Left
    End If
    
    chkSetup(1).Visible = False
    chkSetup(2).Visible = False
    tmpStrg = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_BUTTON_EXPLAIN", "NO")
    If tmpStrg = "YES" Then chkSetup(2).Visible = True
    
    fraSetup(2).Width = fraSetupX(2).Width
    tmpStrg = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "DELAY_TEST_GENERATOR", "NO")
    
    MainPanel(1).ZOrder
    
    GeneratorIsReadyForUse = False
    GeneratorIsReadyForUse = True
    
    Me.Height = MaxScreenHeight
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = MaxScreenWidth - Me.Width - 3600
    NewSize = MaxScreenWidth / 15
    
    SetObjectFaceColor Me
    For i = 0 To SubPanelS.UBound
        SubPanelS(i).BackColor = LightestBlue
    Next
    MainPanel(0).BackColor = LightBlue
    MainPanel(1).BackColor = LightGreen
    
    Me.Show
    fraFlightData.Visible = True
    Me.Refresh
End Sub
Private Sub CreateSubPanels(MaxIdx As Integer)
    Dim SubTop As Long
    Dim NewWidth As Long
    Dim NewTop As Long
    Dim idx As Integer
    Dim HSpl As Integer
    Dim SplCnt As Integer
    HSpl = 0
    SubTop = 0
    NewTop = 0
    
    idx = 0
    SubSplitVertM(idx).Enabled = ShowVertSplit
    SubSplitVertM(idx).Visible = True
    SubSplitVertS(idx).Enabled = ShowVertSplit
    SubSplitVertS(idx).Visible = True
    SubSplitHorzM(idx).Enabled = ShowHorzSplit
    SubSplitHorzM(idx).Visible = True
    SubSplitHorzS(idx).Enabled = ShowHorzSplit
    SubSplitHorzS(idx).Visible = True
    
    For idx = 1 To 2
        Load SubPanelM(idx)
        SubPanelM(idx).Visible = True
        Load SubSplitVertM(idx)
        SubSplitVertM(idx).Enabled = ShowVertSplit
        Load PicVSplitM(idx)
        Set PicVSplitM(idx).Container = SubSplitVertM(idx)
        PicVSplitM(idx).Visible = True
        SubSplitVertM(idx).Visible = True
        
        Load SubSplitVertS(idx)
        SubSplitVertS(idx).Enabled = ShowVertSplit
        Load PicVSplitS(idx)
        Set PicVSplitS(idx).Container = SubSplitVertS(idx)
        PicVSplitS(idx).Visible = True
        SubSplitVertS(idx).Visible = True
        
        Load SubPanelS(idx)
        SubPanelS(idx).Visible = True
        Load SubFrameS(idx)
        SubFrameS(idx).Visible = True
    
        Load MainSplitHorzM(idx)
        MainSplitHorzM(idx).Enabled = ShowVertSplit
        'Load PicVSplitS(idx)
        'Set PicVSplitS(idx).Container = SubSplitVertS(idx)
        'PicVSplitS(idx).Visible = True
        MainSplitHorzM(idx).Visible = True
    
        Load MainSplitHorzS(idx)
        MainSplitHorzS(idx).Enabled = ShowVertSplit
        'Load PicVSplitS(idx)
        'Set PicVSplitS(idx).Container = SubSplitVertS(idx)
        'PicVSplitS(idx).Visible = True
        MainSplitHorzS(idx).Visible = True
    Next
    
    idx = 0
    NewTop = SubTop
    'NewWidth = fraOrigAddr.Width
    'SubPanelM(idx).Width = NewWidth
    SplCnt = 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, False, SplCnt
    'SetSubPanelFrame 0, idx, fraOrigAddr, NewTop, -1
    'SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    'SetSubPanelFrame 0, idx, fraAddress, NewTop, HSpl - 1
    
    idx = 1
    SplCnt = 0
    NewTop = SubTop
    NewWidth = fraFlightPanel(0).Width
    SubPanelM(idx).Width = NewWidth
    SplCnt = 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, False, SplCnt
    'SetSubPanelFrame 0, idx, fraFlightPanel(0), NewTop, -1
    'SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    'SetSubPanelFrame 0, idx, fraDestAddr(0), NewTop, HSpl - 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    'SetSubPanelFrame 0, idx, fraTelexText(0), NewTop, HSpl - 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    'SetSubPanelFrame 0, idx, fraTelexText(1), NewTop, HSpl - 1
    
    idx = 2
    NewTop = SubTop
    NewWidth = fraFlightData.Width
    SubPanelM(idx).Width = NewWidth
    SplCnt = 1
    'SetHorizSplitter 0, idx, HSpl, NewTop, False, SplCnt
    'SetSubPanelFrame 0, idx, fraTlxType, NewTop, -1
    'SetHorizSplitter 0, idx, HSpl, NewTop, ShowHorzSplit, SplCnt
    'SetSubPanelFrame 0, idx, fraFlightData, NewTop, HSpl - 1
    
    HSpl = 0
    SubTop = 0
    NewTop = 0
    
    idx = 0
    SplCnt = 1
    NewTop = SubTop
    NewWidth = 1200
    SubPanelS(idx).Width = NewWidth
    SetHorizSplitter 1, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 1, idx, SubFrameS(idx), NewTop, -1
    
    idx = 1
    SplCnt = 1
    NewTop = SubTop
    NewWidth = 1200
    SubPanelS(idx).Width = NewWidth
    SetHorizSplitter 1, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 1, idx, SubFrameS(idx), NewTop, -1
    
    idx = 2
    SplCnt = 1
    NewTop = SubTop
    NewWidth = 1200
    SubPanelS(idx).Width = NewWidth
    SetHorizSplitter 1, idx, HSpl, NewTop, False, SplCnt
    SetSubPanelFrame 1, idx, SubFrameS(idx), NewTop, -1
    
End Sub
Private Sub SetSubPanelFrame(AsType As Integer, SubIdx As Integer, CurFrame As Frame, NewTop As Long, SplIdx As Integer)
    If AsType = 0 Then
        Set CurFrame.Container = SubPanelM(SubIdx)
        CurFrame.Left = 0
        CurFrame.Top = NewTop
        CurFrame.Width = SubPanelM(SubIdx).Width
        CurFrame.Tag = CStr(SplIdx)
        NewTop = NewTop + CurFrame.Height
    End If
    If AsType = 1 Then
        Set CurFrame.Container = SubPanelS(SubIdx)
        CurFrame.Left = 0
        CurFrame.Top = NewTop
        CurFrame.Width = SubPanelS(SubIdx).Width
        CurFrame.Tag = CStr(SplIdx)
        NewTop = NewTop + CurFrame.Height
    End If
End Sub
Private Sub SetHorizSplitter(AsType As Integer, SubIdx As Integer, SplIdx As Integer, NewTop As Long, ShowSplit As Boolean, CurCnt As Integer)
    If AsType = 0 Then
        If SplIdx > SubSplitHorzM.UBound Then Load SubSplitHorzM(SplIdx)
        Set SubSplitHorzM(SplIdx).Container = SubPanelM(SubIdx)
        SubSplitHorzM(SplIdx).Left = 0
        SubSplitHorzM(SplIdx).Top = NewTop
        SubSplitHorzM(SplIdx).Width = SubPanelM(SubIdx).Width
        'If CurCnt = 0 Then
            'SubSplitHorzM(SplIdx).Height = 90
            'SubSplitHorzM(SplIdx).BackColor = myownbuttondown
        'Else
            'SubSplitHorzM(SplIdx).BackColor = vbRed
            'SubSplitHorzM(SplIdx).Height = 90
        'End If
        NewTop = NewTop + SubSplitHorzM(SplIdx).Height
        SubSplitHorzM(SplIdx).Visible = True
        SubSplitHorzM(SplIdx).Enabled = ShowSplit
        SplIdx = SplIdx + 1
        CurCnt = CurCnt + 1
    End If
    If AsType = 1 Then
        If SplIdx > SubSplitHorzS.UBound Then Load SubSplitHorzS(SplIdx)
        Set SubSplitHorzS(SplIdx).Container = SubPanelS(SubIdx)
        SubSplitHorzS(SplIdx).Left = 0
        SubSplitHorzS(SplIdx).Top = NewTop
        SubSplitHorzS(SplIdx).Width = SubPanelS(SubIdx).Width
        NewTop = NewTop + SubSplitHorzS(SplIdx).Height
        SubSplitHorzS(SplIdx).Visible = True
        SubSplitHorzS(SplIdx).Enabled = ShowSplit
        SplIdx = SplIdx + 1
    End If
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = CheckCancelExit(Me, 1)
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim BordHeight As Long
    Dim NewMainTop As Long
    Dim NewMainLeft As Long
    Dim NewMainWidth As Long
    Dim NewMainHeight As Long
    Dim NewSubTop As Long
    Dim NewSubLeft As Long
    Dim NewSubHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim NewValue As Long
    Dim OldHeight As Long
    Dim OldWidth As Long
    Dim i As Integer
    StatusBar1.Panels(1).Text = CStr(Me.Width / 15) & " / " & CStr(Me.Height / 15)
    fraFlightPanel(0).Height = Me.ScaleHeight - StatusBar1.Height - fraFlightPanel(0).Top
    FltListPanel(3).Height = fraFlightPanel(0).Height - FltListPanel(3).Top - 90
Exit Sub

    OldHeight = MainPanel(0).Height
    OldWidth = MainPanel(0).Width
    If Not GeneratorIsReadyForUse Then
        NewMainTop = 0
        NewMainLeft = 0
        NewMainWidth = Me.ScaleWidth
        NewMainHeight = Me.ScaleHeight
        If NewMainHeight < 60 Then NewMainHeight = 60
        MainPanel(1).Top = NewMainTop
        MainPanel(1).Left = NewMainLeft
        MainPanel(1).Width = NewMainWidth
        MainPanel(1).Height = NewMainHeight
    End If
    NewMainTop = MainPanel(0).Top
    NewMainLeft = 0
    NewMainWidth = Me.ScaleWidth
    NewMainHeight = Me.ScaleHeight
    NewMainHeight = NewMainHeight - NewMainTop
    NewMainHeight = NewMainHeight - StatusBar1.Height
    If NewMainHeight < 60 Then NewMainHeight = 60
    'If OldHeight < NewMainHeight Then MainPanel(0).Height = NewMainHeight
    'If OldHeight > NewMainHeight Then MainPanel(0).Height = NewMainHeight
    'If OldWidth < NewMainWidth Then MainPanel(0).Width = NewMainWidth
    BordHeight = MainSplitHorzM(0).Height + MainSplitHorzS(0).Height
    NewSubHeight = NewMainHeight - BordHeight - 60
    If NewSubHeight >= 60 Then
        For i = 0 To SubPanelM.UBound
            SubPanelM(i).Height = NewSubHeight
            SubSplitVertM(i).Height = NewMainHeight
            SubPanelS(i).Height = NewMainHeight
            SubSplitVertS(i).Height = NewMainHeight
            MainSplitHorzS(i).Top = NewMainHeight - MainSplitHorzS(i).Height - 60
        Next
        'ResizeSubPanels -1
    End If
    RefreshControls -1
    MainPanel(0).Width = NewMainWidth
    MainPanel(0).Height = NewMainHeight
    'MoveSubPanels -1
    ResizeTopArea 0
    If Not GeneratorIsReadyForUse Then
        MainPanel(0).Refresh
    End If
    MainPanel(0).Refresh
    Me.Refresh
    'If FixWin.Value = 1 Then InitPosSize True
    
End Sub
Private Sub ResizeTopArea(ForWhat As Integer)
    Dim NewLeft As Long
    Dim NewValue As Long
    'If SubPanelM.UBound >= 2 Then
    '    fraTopButtons(2).Left = Me.ScaleWidth - fraTopButtons(2).Width - 15
    '    NewLeft = SubSplitVertS(1).Left + SubSplitVertS(1).Width - (fraTopButtons(1).Width) + 30
    '    fraTopButtons(1).Left = NewLeft
    '    fraTopButtons(0).Width = Me.ScaleWidth + 60
    '    NewLeft = SubSplitVertM(2).Left + 30
    '    fraTopButtons(3).Left = NewLeft
    '    fraTopButtons(3).Width = FullPanelWidth(2)
    'End If
End Sub
Private Sub RefreshControls(ForWhat As Integer)
    Dim i As Integer
End Sub

Private Sub AdjustPanelSize(Index As Integer)
    Dim NewSize As Long
    Dim SetWidth As Long
    Dim idx As Integer
    SetWidth = PanelSizeSlider(Index).Value * 15
    Select Case Index
        Case 0
            NewSize = 180
            For idx = 0 To SubSplitVertM.UBound
                SubSplitVertM(idx).Width = SetWidth
                SubSplitVertS(idx).Width = SetWidth
                If chkBorder(0).Value = 1 Then
                    PicVSplitS(idx).Left = SetWidth - PicVSplitS(idx).Width
                End If
                NewSize = NewSize + SubSplitVertM(idx).Width
                NewSize = NewSize + SubPanelM(idx).Width
                NewSize = NewSize + SubSplitVertS(idx).Width
            Next
            Me.Width = NewSize
        Case Else
    End Select
    
End Sub

Private Sub PanelSizeSlider_Scroll(Index As Integer)
    AdjustPanelSize Index
End Sub

Private Sub SubPanelS_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    MouseClock.MouseMoves Index, Button, Shift, x, y
End Sub

Private Sub CreateFlightDataPanel(AftAdid As String, UseCfgList As String, UseCfgData As String, UseTplText As String)
    Dim TabList As String
    Dim CfgList As String
    Dim FldList As String
    Dim UseAftData As String
    Dim TypList As String
    Dim FldData As String
    Dim DspData As String
    Dim CurAdid As String
    Dim FldIdx As Integer
    Dim PaxIdx As Integer
    Dim itm As Long
    Dim FldItm As Integer
    Dim i As Integer
    Dim j As Integer
    Dim idx As Integer
    Dim FldLook As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldType As String
    Dim PaxType As String
    Dim FldLbl1 As String
    Dim FldLbl2 As String
    Dim FldRepl As String
    Dim tmpCapt As String
    Dim tmpName As String
    Dim tmpTpos As String
    Dim tmpTcnt As String
    Dim tmpTag As String
    Dim PicColor As String
    Dim CedaType As String
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim FldPos As Long
    Dim FldCnt As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim picBackColor As Long
    Dim IsNewPanel As Boolean
    
    FldPropTab(0).ZOrder
    FldPropTab(0).ResetContent
    
    FldList = UseCfgList
    CfgList = UseCfgList
    
    If FldList <> "" Then
        'CheckTplFldListComplete FldList, UseAftData, CurTplFldList
        'CfgList = FldList
    
        'CalculateDelay
    End If
    
'    i = 0
'    itm = 0
'    FldName = GetRealItem(CfgList, itm, ",")
'    While FldName <> ""
'        FldType = ""
'        FldRepl = ""
'        FldData = ""
'        FldItm = GetItemNo(FldList, FldName)
'        If FldItm > 0 Then
'            If InStr(CurTabOcxFldLst, FldName) > 0 Then picBackColor = vbBlue Else picBackColor = vbBlack
'            FldData = GetItem(UseAftData, FldItm, ",")
'            FldLbl1 = FldName
'            CedaType = FldName
'            If InStr(CedaTimeFields, FldName) > 0 Then
'                CedaType = "TIME"
'            ElseIf InStr(TemplateDelayTypes, FldName) > 0 Then
'                CedaType = FldName
'                FldName = GetFieldValue(CedaType, TemplateDelayNames, TemplateDelayTypes)
'            ElseIf InStr(TemplateDelayNames, FldName) > 0 Then
'                CedaType = GetFieldValue(FldName, TemplateDelayTypes, TemplateDelayNames)
'            ElseIf InStr(TemplatePaxNames, FldName) > 0 Then
'                CedaType = GetFieldValue(FldName, TemplatePaxTypes, TemplatePaxNames)
'            ElseIf FldName = "REGN" Then
'                If FldData = "" Then
'                    FldData = txtREGN.Text
'                    picBackColor = vbMagenta
'                End If
'            ElseIf InStr("NHOP,NSTN", FldName) > 0 Then
'                CedaType = "NSTN"
'                If InStr(FldData, "?") > 0 Then FldData = ""
'                If FldData = "" Then
'                    FldData = GetNxtStation(FldList, UseAftData)
'                End If
'                picBackColor = vbCyan
'            Else
'                CedaType = "STRG"
'            End If
'            Select Case CedaType
'                Case "TIME"
'                    TypList = "DD,HHMM,DDHHMM"
'                    FldType = "START"
'                    j = 0
'                    While FldType <> ""
'                        j = j + 1
'                        FldType = GetItem(TypList, j, ",")
'                        If FldType <> "" Then
'                            FldLbl2 = FldType
'                            FldCnt = 0
'                            FldPos = 1
'                            While FldPos > 0
'                                FldCnt = FldCnt + 1
'                                tmpTcnt = Right("00" & CStr(FldCnt), 2)
'                                FldLook = "[" & FldName & "|" & FldType & "]"
'                                FldRepl = "[" & FldName & "|" & FldType & "|" & tmpTcnt & "]"
'                                If TplMainFieldsOnly Then FldRepl = FldLook
'                                FldPos = InStr(CurTlxTplText, FldLook)
'                                If FldPos > 0 Then
'                                    tmpTpos = Right("0000" & CStr(FldPos), 4)
'                                    FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
'                                    FldPropTab(0).InsertTextLine FldProp, False
'                                    CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
'                                End If
'                                If TplMainFieldsOnly Then FldPos = 0
'                            Wend
'                        End If
'                    Wend
'                    FldType = "[SSIM]"
'                    FldLbl2 = FldType
'                    FldCnt = 0
'                    FldPos = 1
'                    While FldPos > 0
'                        FldCnt = FldCnt + 1
'                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
'                        FldLook = "[" & FldName & "]"
'                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
'                        If TplMainFieldsOnly Then FldRepl = FldLook
'                        FldPos = InStr(CurTlxTplText, FldLook)
'                        If FldPos > 0 Then
'                            tmpTpos = Right("0000" & CStr(FldPos), 4)
'                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
'                            FldPropTab(0).InsertTextLine FldProp, False
'                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
'                        End If
'                        If TplMainFieldsOnly Then FldPos = 0
'                    Wend
'                Case "DCD1", "DCD2", "DTD1", "DTD2"
'                    picBackColor = vbGreen
'                    FldType = CedaType
'                    Select Case CedaType
'                        Case "DCD1"
'                            FldData = AftFldText(15).Text
'                        Case "DCD2"
'                            FldData = AftFldText(16).Text
'                        Case "DTD1"
'                            FldData = AftFldText(17).Text
'                        Case "DTD2"
'                            FldData = AftFldText(18).Text
'                        Case Else
'                    End Select
'                    FldLbl1 = FldName
'                    FldLbl2 = ""
'                    FldCnt = 0
'                    FldPos = 1
'                    While FldPos > 0
'                        FldCnt = FldCnt + 1
'                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
'                        FldLook = "[" & FldName & "]"
'                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
'                        If TplMainFieldsOnly Then FldRepl = FldLook
'                        FldPos = InStr(CurTlxTplText, FldLook)
'                        If FldPos > 0 Then
'                            tmpTpos = Right("0000" & CStr(FldPos), 4)
'                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
'                            FldPropTab(0).InsertTextLine FldProp, False
'                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
'                        End If
'                        If TplMainFieldsOnly Then FldPos = 0
'                    Wend
'                Case "PAX1", "PAX2", "PAX3", "PAX4"
'                    picBackColor = vbGreen
'                    FldType = CedaType
'                    PaxType = CedaType
'                    FldIdx = GetPaxFieldIndex(3, CedaType, 1, FldName)
'                    If FldIdx >= 0 Then
'                        FldData = AftFldText(FldIdx).Text
'                        tmpTag = AftFldPanel(FldIdx).Tag
'                        tmpTag = GetItem(tmpTag, 2, "|")
'                        PaxType = GetItem(tmpTag, 4, ",")
'                    Else
'                    End If
'                    Select Case CedaType
'                        Case "PAX1"
'                            'FldData = AftFldText(42).Text
'                        Case "PAX2"
'                            'FldData = AftFldText(41).Text
'                        Case "PAX3"
'                            'FldData = AftFldText(40).Text
'                        Case Else
'                    End Select
'                    FldType = PaxType
'                    FldLbl1 = FldName
'                    FldLbl2 = ""
'                    FldCnt = 0
'                    FldPos = 1
'                    While FldPos > 0
'                        FldCnt = FldCnt + 1
'                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
'                        FldLook = "[" & FldName & "]"
'                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
'                        If TplMainFieldsOnly Then FldRepl = FldLook
'                        FldPos = InStr(CurTlxTplText, FldLook)
'                        If FldPos > 0 Then
'                            tmpTpos = Right("0000" & CStr(FldPos), 4)
'                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
'                            FldPropTab(0).InsertTextLine FldProp, False
'                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
'                        End If
'                        If TplMainFieldsOnly Then FldPos = 0
'                    Wend
'                Case Else
'                    FldType = FldName
'                    FldLbl2 = ""
'                    FldCnt = 0
'                    FldPos = 1
'                    While FldPos > 0
'                        FldCnt = FldCnt + 1
'                        tmpTcnt = Right("00" & CStr(FldCnt), 2)
'                        FldLook = "[" & FldName & "]"
'                        FldRepl = "[" & FldName & "|" & tmpTcnt & "]"
'                        If TplMainFieldsOnly Then FldRepl = FldLook
'                        FldPos = InStr(CurTlxTplText, FldLook)
'                        If FldPos > 0 Then
'                            tmpTpos = Right("0000" & CStr(FldPos), 4)
'                            FldProp = tmpTpos & "," & FldName & "," & FldRepl & "," & FldType & "," & FldLbl1 & "," & FldLbl2 & "," & FldData & "," & CStr(picBackColor)
'                            FldPropTab(0).InsertTextLine FldProp, False
'                            CurTlxTplText = Replace(CurTlxTplText, FldLook, FldRepl, 1, 1, vbBinaryCompare)
'                        End If
'                        If TplMainFieldsOnly Then FldPos = 0
'                    Wend
'            End Select
'        End If
'        itm = itm + 1
'        FldName = GetRealItem(CfgList, itm, ",")
'    Wend
'
'    MaxLine = FldPropTab(0).GetLineCount - 1
'    For CurLine = 0 To MaxLine
'        FldLook = FldPropTab(0).GetFieldValue(CurLine, "REPL")
'        FldPos = InStr(CurTlxTplText, FldLook)
'        tmpTpos = Right("0000" & CStr(FldPos), 4)
'        FldPropTab(0).SetColumnValue CurLine, 0, tmpTpos
'    Next
'    FldPropTab(0).AutoSizeColumns
'    FldPropTab(0).Sort "0", True, True
'
'    If TplFldCreateEditor Then
'        NewTop = FltEditPanel(0).Top
'        NewLeft = FltEditPanel(0).Left
'        i = 0
'        If i = FltEditPanel.UBound Then
'            FltEditPanel(i).AutoRedraw = True
'            DrawBackGround FltEditPanel(i), MyLifeStyleValue, False, True
'            FltEditPanel(i).Picture = FltEditPanel(i).Image
'            FltEditPanel(i).Cls
'            FltEditPanel(i).AutoRedraw = False
'        End If
'        i = 0
'        itm = 0
'        MaxLine = FldPropTab(0).GetLineCount - 1
'        For CurLine = 0 To MaxLine
'            FldName = FldPropTab(0).GetFieldValue(CurLine, "FINA")
'            FldRepl = FldPropTab(0).GetFieldValue(CurLine, "REPL")
'            FldType = FldPropTab(0).GetFieldValue(CurLine, "TYPE")
'            FldLbl1 = FldPropTab(0).GetFieldValue(CurLine, "LBL1")
'            FldLbl2 = FldPropTab(0).GetFieldValue(CurLine, "LBL2")
'            FldData = FldPropTab(0).GetFieldValue(CurLine, "DATA")
'            PicColor = FldPropTab(0).GetFieldValue(CurLine, "DSSC")
'            picBackColor = Val(PicColor)
'            IsNewPanel = False
'            If i > FltEditPanel.UBound Then
'                Load FltEditPanel(i)
'                Load chkFltTick(i)
'                Load lblFltField(i)
'                Load lblDatType(i)
'                Load txtFltInput(i)
'                Load chkFltCheck(i)
'                Load picFldRelIdx(i)
'                Set FltEditPanel(i).Container = FltInputSlide
'                Set chkFltTick(i).Container = FltEditPanel(i)
'                Set lblFltField(i).Container = FltEditPanel(i)
'                Set lblDatType(i).Container = FltEditPanel(i)
'                Set txtFltInput(i).Container = FltEditPanel(i)
'                Set chkFltCheck(i).Container = FltEditPanel(i)
'                Set picFldRelIdx(i).Container = FltEditPanel(i)
'                IsNewPanel = True
'            End If
'            Select Case FldType
'                Case "FLNO"
'                    DspData = Replace(FldData, " ", "", 1, -1, vbBinaryCompare)
'                    'picBackColor = vbBlue
'                Case "DD"
'                    DspData = Mid(FldData, 7, 2)
'                Case "HHMM"
'                    'picBackColor = vbBlue
'                    DspData = Mid(FldData, 9, 4)
'                Case "DDHHMM"
'                    'picBackColor = vbBlue
'                    DspData = Mid(FldData, 7, 6)
'                Case "[SSIM]"
'                    'picBackColor = vbBlue
'                    DspData = DecodeSsimDayFormat(Left(FldData, 8), "CEDA", "SSIM2")
'                    DspData = DspData & ":" & Mid(FldData, 9, 4)
'                Case "DCD1", "DCD2"
'                    picBackColor = vbGreen
'                    DspData = FldData
'                    If FldType = "DCD1" Then idx = 1 Else idx = 2
'                    tmpTag = chkEditDL(idx).Tag
'                    If TplUpdateFldGroups Then
'                        picBackColor = vbRed
'                        txtEditDL(idx).Text = DspData
'                        tmpTag = tmpTag & CStr(i) & ","
'                        chkFltTick(i).Tag = CStr(idx)
'                    ElseIf tmpTag = "" Then
'                        txtEditDL(idx).Text = DspData
'                        tmpTag = tmpTag & CStr(i)
'                        chkFltTick(i).Tag = CStr(idx)
'                    End If
'                    chkEditDL(idx).Tag = tmpTag
'                Case "DTD1", "DTD2"
'                    picBackColor = vbGreen
'                    DspData = Right("0000" & FldData, 4)
'                    If DspData = "0000" Then DspData = ""
'                    If FldType = "DTD1" Then idx = 3 Else idx = 4
'                    tmpTag = chkEditDL(idx).Tag
'                    If TplUpdateFldGroups Then
'                        picBackColor = vbRed
'                        txtEditDL(idx).Text = DspData
'                        tmpTag = tmpTag & CStr(i) & ","
'                        chkFltTick(i).Tag = CStr(idx)
'                    ElseIf tmpTag = "" Then
'                        txtEditDL(idx).Text = DspData
'                        tmpTag = tmpTag & CStr(i)
'                        chkFltTick(i).Tag = CStr(idx)
'                    End If
'                    chkEditDL(idx).Tag = tmpTag
'                Case "PAX1", "PAX2", "PAX3", "PAX4"
'                    picBackColor = vbGreen
'                    DspData = FldData
'                    idx = Val(Right(FldType, 1))
'                    tmpTag = chkEditPX(idx).Tag
'                    If TplUpdateFldGroups Then
'                        picBackColor = vbRed
'                        txtEditPX(idx).Text = DspData
'                        tmpTag = tmpTag & CStr(i) & ","
'                        chkFltTick(i).Tag = CStr(idx)
'                    ElseIf tmpTag = "" Then
'                        txtEditPX(idx).Text = DspData
'                        tmpTag = tmpTag & CStr(i)
'                        chkFltTick(i).Tag = CStr(idx)
'                    End If
'                    chkEditPX(idx).Tag = tmpTag
'                Case Else
'                    DspData = FldData
'            End Select
'
'            FltEditPanel(i).Tag = FldName
'
'            lblFltField(i).Caption = FldLbl1
'            lblFltField(i).Tag = FldRepl
'
'            If FldLbl2 = "" Then FldLbl2 = ". . . . . . . ."
'            lblDatType(i).Caption = FldLbl2
'            lblDatType(i).Tag = FldType
'
'            txtFltInput(i).Text = DspData
'            txtFltInput(i).Tag = FldData
'
'            chkFltCheck(i).Tag = DspData
'
'            txtFltInput(i).BackColor = LightGrey
'            picFldRelIdx(i).BackColor = picBackColor
'            chkFltTick(i).Value = 0
'            chkFltTick(i).Visible = True
'            chkFltCheck(i).Visible = True
'            lblFltField(i).Visible = True
'            lblDatType(i).Visible = True
'            txtFltInput(i).Visible = True
'            If ShowFldDsscColFlag Then picFldRelIdx(i).Visible = True Else picFldRelIdx(i).Visible = False
'            FltEditPanel(i).Top = NewTop
'            FltEditPanel(i).Left = NewLeft
'            FltEditPanel(i).Visible = True
'            If (DspData = "") Or (InStr(DspData, "?") > 0) Then
'                chkFltTick(i).Value = 1
'            End If
'            'HardCoded HotFix
'            If FldName = "EETA" Then
'                AftFldLabel(9).Tag = CStr(i)
'            End If
'            'DoEvents
'            NewTop = NewTop + FltEditPanel(i).Height
'            i = i + 1
'        Next
'    End If
'    CalculateDelay
'    CalculateEans
'
'    If Trim(txtSiText(0).Text) = "" Then
'        txtSiText(0).Visible = False
'        fraSiTxtLabel(0).Visible = False
'        fraSiTxtLabel(1).Visible = False
'        'txtTelexText(0).Height = fraTelexText(0).Height - txtTelexText(0).Top - 90
'    Else
'        txtSiText(0).Visible = True
'        fraSiTxtLabel(0).Visible = True
'        fraSiTxtLabel(1).Visible = True
'    End If
'    NewTop = NewTop + 30
'    FltInputSlide.Height = NewTop
'    VScroll1(0).Max = NewTop / 15
'    ResizeSubPanels 1
'    ResizeSubPanels 2
End Sub
Private Sub LoadFlightData()
    Dim tmpFrom As String
    Dim tmpTo As String
    Dim tmpTime As String
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim tmpAlcList As String
    Dim UseVpfrVal As String
    Dim UseVpToVal As String
    Dim tmpStrg As String
    Dim tmpTip As String
    Dim ValOffsVpfr As Long
    Dim ValOffsVpto As Long
    Dim LinCnt As Long
    Dim CurSrvTime
    Dim i As Integer
    HiddenMain.SetStatusText 1, UCase("TELEX GENERATOR")
    Me.MousePointer = 11
    i = 0
    UseVpfrVal = AftVpfr(i).Text
    UseVpToVal = AftVpto(i).Text
    ValOffsVpfr = Val(UseVpfrVal)
    ValOffsVpto = Val(UseVpToVal)
    tmpTip = "ARR=## [FROM " & UseVpfrVal & " TO " & UseVpToVal & "]   "
    HiddenMain.SetStatusText 2, UCase("LOADING ARRIVAL FLIGHTS")
    TabFlights(i).ResetContent
    TabFlights(i).Refresh
    tmpAlcList = UfisServer.BasicData(3).SelectDistinct("1", "'", "'", ",", True)
    tmpAlcList = Replace(tmpAlcList, "''", "", 1, -1, vbBinaryCompare)
    tmpAlcList = Replace(tmpAlcList, ",,", ",", 1, -1, vbBinaryCompare)
    If Left(tmpAlcList, 1) = "," Then tmpAlcList = Mid(tmpAlcList, 2)
    If Right(tmpAlcList, 1) = "," Then tmpAlcList = Left(tmpAlcList, Len(tmpAlcList) - 1)
    tmpFields = TabFlights(i).LogicalFieldList
    tmpTime = MySetUp.GetUtcServerTime
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", ValOffsVpfr, CurSrvTime)
    tmpFrom = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", ValOffsVpto, CurSrvTime)
    tmpTo = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    tmpSqlKey = "WHERE (TIFA BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "') "
    If tmpAlcList <> "" Then
        tmpSqlKey = tmpSqlKey & "AND (ALC2 IN (" & tmpAlcList & ")) "
    End If
    tmpSqlKey = tmpSqlKey & "AND ADID='A' "
    tmpSqlKey = tmpSqlKey & "AND FTYP IN ('O','S') "
    TabFlights(i).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    TabFlights(i).CedaHopo = UfisServer.HOPO
    TabFlights(i).CedaIdentifier = "IDX"
    TabFlights(i).CedaPort = "3357"
    TabFlights(i).CedaReceiveTimeout = "250"
    TabFlights(i).CedaRecordSeparator = vbLf
    TabFlights(i).CedaSendTimeout = "250"
    TabFlights(i).CedaServerName = UfisServer.HostName
    TabFlights(i).CedaTabext = UfisServer.TblExt
    TabFlights(i).CedaUser = gsUserName
    TabFlights(i).CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        TabFlights(i).CedaAction "GFR", "AFTTAB", tmpFields, "", tmpSqlKey
    End If
    TabFlights(i).Sort "1", True, True
    TabFlights(i).AutoSizeColumns
    
    i = 1
    UseVpfrVal = AftVpfr(i).Text
    UseVpToVal = AftVpto(i).Text
    ValOffsVpfr = Val(UseVpfrVal)
    ValOffsVpto = Val(UseVpToVal)
    tmpTip = tmpTip & "DEP=## [FROM " & UseVpfrVal & " TO " & UseVpToVal & "] "
    HiddenMain.SetStatusText 2, UCase("LOADING DEPARTUE FLIGHTS")
    TabFlights(i).ResetContent
    TabFlights(i).Refresh
    tmpFields = TabFlights(i).LogicalFieldList
    tmpTime = MySetUp.GetUtcServerTime
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", ValOffsVpfr, CurSrvTime)
    tmpFrom = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    CurSrvTime = CedaFullDateToVb(tmpTime)
    CurSrvTime = DateAdd("h", ValOffsVpto, CurSrvTime)
    tmpTo = Format(CurSrvTime, "yyyymmddhhmm") & "00"
    tmpSqlKey = "WHERE (TIFD BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "') "
    If tmpAlcList <> "" Then
        tmpSqlKey = tmpSqlKey & "AND (ALC2 IN (" & tmpAlcList & ")) "
    End If
    tmpSqlKey = tmpSqlKey & "AND ADID='D' "
    tmpSqlKey = tmpSqlKey & "AND FTYP IN ('O','S') "
    TabFlights(i).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    TabFlights(i).CedaHopo = UfisServer.HOPO
    TabFlights(i).CedaIdentifier = "IDX"
    TabFlights(i).CedaPort = "3357"
    TabFlights(i).CedaReceiveTimeout = "250"
    TabFlights(i).CedaRecordSeparator = vbLf
    TabFlights(i).CedaSendTimeout = "250"
    TabFlights(i).CedaServerName = UfisServer.HostName
    TabFlights(i).CedaTabext = UfisServer.TblExt
    TabFlights(i).CedaUser = gsUserName
    TabFlights(i).CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        TabFlights(i).CedaAction "GFR", "AFTTAB", tmpFields, "", tmpSqlKey
    End If
    TabFlights(i).Sort "1", True, True
    TabFlights(i).Sort "12", True, True
    TabFlights(i).AutoSizeColumns
    
    LinCnt = TabFlights(0).GetLineCount
    tmpStrg = CStr(LinCnt)
    tmpTip = Replace(tmpTip, "##", tmpStrg, 1, 1, vbBinaryCompare)
    LinCnt = TabFlights(1).GetLineCount
    tmpStrg = CStr(LinCnt)
    tmpTip = Replace(tmpTip, "##", tmpStrg, 1, 1, vbBinaryCompare)
    
    Me.MousePointer = 0
End Sub

Private Sub InitSubPanelMs(Index As Integer)
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim NewHeight As Long
    Dim SubIdx As Integer
    Dim idx As Integer
    SubIdx = Index
End Sub

Private Function FullPanelWidth(Index As Integer) As Long
    Dim SubSize As Long
    SubSize = 0
    SubSize = SubSize + SubSplitVertM(Index).Width
    SubSize = SubSize + SubPanelM(Index).Width
    SubSize = SubSize + SubSplitVertS(Index).Width
    FullPanelWidth = SubSize
End Function

