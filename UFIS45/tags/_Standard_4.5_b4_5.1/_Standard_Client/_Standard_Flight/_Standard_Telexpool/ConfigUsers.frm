VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form ConfigUsers 
   Caption         =   "Configuration of User Groups"
   ClientHeight    =   6555
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9540
   Icon            =   "ConfigUsers.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6555
   ScaleWidth      =   9540
   Begin VB.CheckBox chkWork 
      Caption         =   "Test"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   4380
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "New"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame fraGroupMembers 
      Caption         =   "Group Members"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      Left            =   60
      TabIndex        =   7
      Top             =   3735
      Width           =   5295
      Begin TABLib.TAB GroupMembers 
         Height          =   1665
         Index           =   0
         Left            =   90
         TabIndex        =   8
         Top             =   240
         Width           =   2535
         _Version        =   65536
         _ExtentX        =   4471
         _ExtentY        =   2937
         _StockProps     =   64
      End
      Begin TABLib.TAB GroupMembers 
         Height          =   1665
         Index           =   1
         Left            =   2670
         TabIndex        =   9
         Top             =   240
         Width           =   2535
         _Version        =   65536
         _ExtentX        =   4471
         _ExtentY        =   2937
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraUserGroups 
      Caption         =   "Telex Pool User Groups"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3195
      Left            =   60
      TabIndex        =   5
      Top             =   420
      Width           =   5295
      Begin TABLib.TAB UserGroups 
         Height          =   2865
         Left            =   90
         TabIndex        =   6
         Top             =   240
         Width           =   5115
         _Version        =   65536
         _ExtentX        =   9022
         _ExtentY        =   5054
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraUserPanel 
      Caption         =   "Ufis Users"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Left            =   5430
      TabIndex        =   3
      Top             =   420
      Width           =   2745
      Begin TABLib.TAB AllUsers 
         Height          =   2025
         Left            =   90
         TabIndex        =   4
         Top             =   240
         Width           =   2385
         _Version        =   65536
         _ExtentX        =   4207
         _ExtentY        =   3572
         _StockProps     =   64
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   5250
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   6270
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16298
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "ConfigUsers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub AllUsers_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
    ElseIf LineNo = -1 Then
        AllUsers.Sort CStr(ColNo), True, True
        AllUsers.AutoSizeColumns
    Else
    End If
End Sub

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'Close
                'Me.Hide
                'TelexPoolHead.chkAssFile.Value = 0
                chkWork(Index).Value = 0
            Case 1  'Refresh
                LoadUsers
            Case Else
                chkWork(Index).Value = 0
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
        Select Case Index
            Case Else
        End Select
    End If
End Sub

Private Sub Form_Load()
    InitMyForm
End Sub

Private Sub InitMyForm()
    Dim i As Integer
    StatusBar1.ZOrder
    AllUsers.ResetContent
    AllUsers.LogicalFieldList = "STAT,USID,NAME,REMA,URNO"
    AllUsers.FontName = "Courier New"
    AllUsers.SetTabFontBold True
    AllUsers.HeaderFontSize = 17
    AllUsers.FontSize = 17
    AllUsers.LineHeight = 17
    AllUsers.HeaderString = "S,User ID,Full Name,Remark,URNO"
    AllUsers.HeaderLengthString = "10,10,10,10,10"
    AllUsers.SetMainHeaderValues "5", "BDPS-SEC Definitions", ""
    AllUsers.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    AllUsers.SetColumnBoolProperty 0, "1", "0"
    AllUsers.SetBoolPropertyReadOnly 0, True
    AllUsers.ShowHorzScroller True
    AllUsers.ShowVertScroller True
    AllUsers.LifeStyle = True
    AllUsers.CursorLifeStyle = True
    AllUsers.MainHeader = True
    AllUsers.AutoSizeByHeader = True
    AllUsers.AutoSizeColumns

    UserGroups.ResetContent
    UserGroups.LogicalFieldList = "STAT,USID,NAME,REMA,URNO"
    UserGroups.FontName = "Courier New"
    UserGroups.SetTabFontBold True
    UserGroups.HeaderFontSize = 17
    UserGroups.FontSize = 17
    UserGroups.LineHeight = 17
    UserGroups.HeaderString = "S,Grp,Full Name,Remark,URNO"
    UserGroups.HeaderLengthString = "10,10,10,10,10"
    UserGroups.SetMainHeaderValues "5", "Configured Groups", ""
    UserGroups.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    UserGroups.SetColumnBoolProperty 0, "1", "0"
    UserGroups.ShowHorzScroller True
    UserGroups.ShowVertScroller True
    UserGroups.LifeStyle = True
    UserGroups.CursorLifeStyle = True
    UserGroups.MainHeader = True
    UserGroups.AutoSizeByHeader = True
    UserGroups.InsertTextLine "1,TM1,12345678901234567890123456789012,123456789012,123456789", False
    UserGroups.InsertTextLine "1,TM2,12345678901234567890123456789012,123456789012,123456789", False
    UserGroups.InsertTextLine "1,TM3,12345678901234567890123456789012,123456789012,123456789", False
    UserGroups.InsertTextLine "0,TM4,12345678901234567890123456789012,123456789012,123456789", False
    UserGroups.InsertTextLine "1,TM5,12345678901234567890123456789012,123456789012,123456789", False
    UserGroups.AutoSizeColumns
    
    For i = 0 To 1
        GroupMembers(i).ResetContent
        GroupMembers(i).LogicalFieldList = "STAT,USID,NAME,REMA,URNO"
        GroupMembers(i).FontName = "Courier New"
        GroupMembers(i).SetTabFontBold True
        GroupMembers(i).HeaderFontSize = 17
        GroupMembers(i).FontSize = 17
        GroupMembers(i).LineHeight = 17
        GroupMembers(i).HeaderString = "S,User ID,Full Name,Remark,URNO"
        GroupMembers(i).HeaderLengthString = "10,10,10,10,10"
        GroupMembers(i).SetColumnBoolProperty 0, "1", "0"
        GroupMembers(i).ShowHorzScroller True
        GroupMembers(i).ShowVertScroller True
        GroupMembers(i).LifeStyle = True
        GroupMembers(i).CursorLifeStyle = True
    Next
    GroupMembers(0).SetMainHeaderValues "5", "Available Members", ""
    GroupMembers(0).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    GroupMembers(0).MainHeader = True
    GroupMembers(0).AutoSizeByHeader = True
    GroupMembers(1).SetMainHeaderValues "5", "Selected Members", ""
    GroupMembers(1).SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    GroupMembers(1).MainHeader = True
    GroupMembers(1).AutoSizeByHeader = True
    GroupMembers(0).InsertTextLine "1,Sven,12345678901234567890123456789012,123456789012,123456789", False
    GroupMembers(1).InsertTextLine "1,Berni,12345678901234567890123456789012,123456789012,123456789", False
    GroupMembers(0).AutoSizeColumns
    GroupMembers(1).AutoSizeColumns

End Sub

Private Sub LoadUsers()
    Dim RetCode As Boolean
    Dim tmpSqlCmd As String
    Dim tmpSqlTbl As String
    Dim tmpSqlFld As String
    Dim tmpSqlKey As String
    Dim tmpErrMsg As String
    tmpSqlCmd = "RTA"
    tmpSqlTbl = "SECTAB"
    tmpSqlFld = AllUsers.LogicalFieldList
    tmpSqlKey = "WHERE TYPE='U'"
    AllUsers.ResetContent
    AllUsers.Refresh
    AllUsers.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    AllUsers.CedaHopo = UfisServer.HOPO
    AllUsers.CedaIdentifier = "IDX"
    AllUsers.CedaPort = "3357"
    AllUsers.CedaReceiveTimeout = "250"
    AllUsers.CedaRecordSeparator = vbLf
    AllUsers.CedaSendTimeout = "250"
    AllUsers.CedaServerName = UfisServer.HostName
    AllUsers.CedaTabext = UfisServer.TblExt
    AllUsers.CedaUser = gsUserName
    AllUsers.CedaWorkstation = UfisServer.GetMyWorkStationName
    'CedaStatus(0).Visible = True
    'CedaStatus(0).ZOrder
    'CedaStatus(0).Refresh
    If CedaIsConnected Then
        RetCode = AllUsers.CedaAction(tmpSqlCmd, tmpSqlTbl, tmpSqlFld, "", tmpSqlKey)
        If RetCode = False Then
            tmpErrMsg = tmpErrMsg & AllUsers.GetLastCedaError
        End If
    End If
    'CedaStatus(0).Visible = False
    AllUsers.AutoSizeColumns
    AllUsers.Refresh
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - fraUserPanel.Left - fraUserGroups.Left
    If NewSize > 600 Then
        fraUserPanel.Width = NewSize
        NewSize = NewSize - 180
        AllUsers.Width = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraGroupMembers.Top - 60
    If NewSize > 600 Then
        fraGroupMembers.Height = NewSize
    End If
    NewSize = NewSize - GroupMembers(0).Top - 90
    If NewSize > 600 Then
        GroupMembers(0).Height = NewSize
        GroupMembers(1).Height = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - fraUserPanel.Top - 60
    If NewSize > 600 Then
        fraUserPanel.Height = NewSize
    End If
    NewSize = NewSize - AllUsers.Top - 90
    If NewSize > 600 Then
        AllUsers.Height = NewSize
    End If
End Sub
