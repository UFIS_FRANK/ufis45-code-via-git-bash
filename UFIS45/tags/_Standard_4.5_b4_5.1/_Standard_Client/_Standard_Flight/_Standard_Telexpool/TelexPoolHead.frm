VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form TelexPoolHead 
   Caption         =   "Telex Pool"
   ClientHeight    =   8235
   ClientLeft      =   1065
   ClientTop       =   900
   ClientWidth     =   15420
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "TelexPoolHead.frx":0000
   KeyPreview      =   -1  'True
   LinkMode        =   1  'Source
   LinkTopic       =   "InfoServer"
   LockControls    =   -1  'True
   ScaleHeight     =   8235
   ScaleWidth      =   15420
   WindowState     =   1  'Minimized
   Begin VB.PictureBox ActionPanel 
      BackColor       =   &H00C0C0C0&
      Height          =   5895
      Left            =   12840
      ScaleHeight     =   5835
      ScaleWidth      =   1860
      TabIndex        =   265
      Top             =   60
      Visible         =   0   'False
      Width           =   1920
      Begin VB.PictureBox LogPanel 
         Height          =   2115
         Index           =   2
         Left            =   90
         ScaleHeight     =   2055
         ScaleWidth      =   1605
         TabIndex        =   274
         Top             =   3600
         Width           =   1665
         Begin TABLib.TAB BcLog 
            Height          =   945
            Index           =   2
            Left            =   0
            TabIndex        =   275
            Top             =   0
            Width           =   1605
            _Version        =   65536
            _ExtentX        =   2831
            _ExtentY        =   1667
            _StockProps     =   64
         End
      End
      Begin VB.CheckBox chkImbis 
         Caption         =   "Imbis"
         Enabled         =   0   'False
         Height          =   315
         Left            =   930
         Style           =   1  'Graphical
         TabIndex        =   271
         Top             =   90
         Width           =   810
      End
      Begin VB.PictureBox LogPanel 
         Height          =   2115
         Index           =   1
         Left            =   90
         ScaleHeight     =   2055
         ScaleWidth      =   1605
         TabIndex        =   269
         Top             =   1410
         Width           =   1665
         Begin TABLib.TAB BcLog 
            Height          =   945
            Index           =   1
            Left            =   0
            TabIndex        =   270
            Top             =   0
            Width           =   1605
            _Version        =   65536
            _ExtentX        =   2831
            _ExtentY        =   1667
            _StockProps     =   64
         End
      End
      Begin VB.PictureBox LogPanel 
         Height          =   765
         Index           =   0
         Left            =   90
         ScaleHeight     =   705
         ScaleWidth      =   1605
         TabIndex        =   267
         Top             =   480
         Width           =   1665
         Begin TABLib.TAB BcLog 
            Height          =   495
            Index           =   0
            Left            =   0
            TabIndex        =   268
            Top             =   0
            Width           =   1605
            _Version        =   65536
            _ExtentX        =   2831
            _ExtentY        =   873
            _StockProps     =   64
         End
      End
      Begin VB.CheckBox chkBcSpool 
         Caption         =   "Spooler"
         Height          =   315
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   266
         Top             =   90
         Width           =   810
      End
      Begin VB.Image AnyPic 
         Height          =   480
         Index           =   4
         Left            =   1140
         Picture         =   "TelexPoolHead.frx":0C02
         Stretch         =   -1  'True
         Top             =   0
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Image AnyPic 
         Height          =   480
         Index           =   3
         Left            =   0
         Picture         =   "TelexPoolHead.frx":1DC4
         Stretch         =   -1  'True
         Top             =   0
         Visible         =   0   'False
         Width           =   690
      End
   End
   Begin VB.Frame fraFlightStatus 
      Caption         =   "Flight Status"
      Height          =   2205
      Left            =   6810
      TabIndex        =   248
      Top             =   420
      Visible         =   0   'False
      Width           =   1395
      Begin VB.PictureBox FtypPanel 
         AutoRedraw      =   -1  'True
         BackColor       =   &H000040C0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1395
         Left            =   90
         ScaleHeight     =   1335
         ScaleWidth      =   1155
         TabIndex        =   250
         Top             =   270
         Width           =   1215
         Begin VB.CheckBox chkFtypFilter 
            BackColor       =   &H008080FF&
            Caption         =   "X"
            Height          =   225
            Index           =   0
            Left            =   45
            MaskColor       =   &H8000000F&
            TabIndex        =   253
            Tag             =   "-1"
            Top             =   390
            Width           =   420
         End
         Begin VB.TextBox txtFtypFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   30
            TabIndex        =   252
            Text            =   "FTYP FILTER"
            Top             =   810
            Visible         =   0   'False
            Width           =   2415
         End
         Begin VB.CheckBox chkFtypAll 
            BackColor       =   &H008080FF&
            Caption         =   "<"
            Height          =   225
            Index           =   0
            Left            =   45
            MaskColor       =   &H8000000F&
            TabIndex        =   251
            Tag             =   "-1"
            Top             =   60
            Width           =   420
         End
         Begin VB.Label lblFtypAllShadow 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "All"
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   0
            Left            =   810
            TabIndex        =   257
            Top             =   60
            Width           =   210
         End
         Begin VB.Label lblFtypShadow 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CXX"
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   0
            Left            =   585
            TabIndex        =   256
            Top             =   405
            Width           =   330
         End
         Begin VB.Label lblFtypName 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "CXX"
            ForeColor       =   &H00C0FFFF&
            Height          =   210
            Index           =   0
            Left            =   570
            TabIndex        =   255
            Top             =   390
            Width           =   330
         End
         Begin VB.Label lblFtypAll 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "All"
            ForeColor       =   &H00C0FFFF&
            Height          =   210
            Index           =   0
            Left            =   540
            TabIndex        =   254
            ToolTipText     =   "Checks or Unchecks All"
            Top             =   60
            Width           =   210
         End
      End
      Begin VB.CheckBox chkFsfClose 
         Caption         =   "Done"
         Height          =   315
         Left            =   270
         Style           =   1  'Graphical
         TabIndex        =   249
         Top             =   1740
         Width           =   855
      End
   End
   Begin VB.Timer KeybTimer 
      Enabled         =   0   'False
      Interval        =   250
      Left            =   8730
      Top             =   4710
   End
   Begin VB.Frame fraTimeFilter 
      Caption         =   "Date/Time Filter"
      Height          =   1035
      Left            =   30
      TabIndex        =   0
      Top             =   420
      Width           =   2865
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Height          =   645
         Left            =   2865
         TabIndex        =   239
         Top             =   300
         Width           =   750
         Begin VB.OptionButton optTimeType 
            Caption         =   "Main"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   241
            Top             =   0
            Width           =   750
         End
         Begin VB.OptionButton optTimeType 
            Caption         =   "Online"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   240
            Top             =   330
            Width           =   750
         End
      End
      Begin VB.OptionButton optLocal 
         Caption         =   "L"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2490
         Style           =   1  'Graphical
         TabIndex        =   65
         ToolTipText     =   "Times in ATH local (UTC + 180 min.)"
         Top             =   630
         Width           =   300
      End
      Begin VB.OptionButton optUtc 
         Caption         =   "U"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2490
         Style           =   1  'Graphical
         TabIndex        =   64
         ToolTipText     =   "Times in UTC (ATH Local - 180 min.)"
         Top             =   300
         Width           =   300
      End
      Begin VB.CheckBox ResetFilter 
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   63
         ToolTipText     =   "Reset Time Filter"
         Top             =   300
         Width           =   660
      End
      Begin VB.CheckBox chkFromTo 
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   60
         ToolTipText     =   "Confirm Time Filter"
         Top             =   630
         Width           =   660
      End
      Begin VB.TextBox txtDateFrom 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   720
         MaxLength       =   10
         TabIndex        =   4
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   300
         Width           =   1065
      End
      Begin VB.TextBox txtTimeFrom 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   1800
         MaxLength       =   5
         TabIndex        =   3
         ToolTipText     =   "Time hh:mm"
         Top             =   300
         Width           =   645
      End
      Begin VB.TextBox txtTimeTo 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   1800
         MaxLength       =   5
         TabIndex        =   2
         ToolTipText     =   "Time hh:mm"
         Top             =   630
         Width           =   645
      End
      Begin VB.TextBox txtDateTo 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   720
         MaxLength       =   10
         TabIndex        =   1
         ToolTipText     =   "Day Date dd.mm.yyyy"
         Top             =   630
         Width           =   1065
      End
   End
   Begin VB.PictureBox TopSplit 
      BackColor       =   &H00C0C000&
      BorderStyle     =   0  'None
      Height          =   180
      Index           =   1
      Left            =   10380
      ScaleHeight     =   180
      ScaleWidth      =   945
      TabIndex        =   209
      Top             =   30
      Width           =   945
   End
   Begin VB.PictureBox TopButtons 
      Height          =   1365
      Left            =   11610
      ScaleHeight     =   1305
      ScaleWidth      =   915
      TabIndex        =   203
      Top             =   5160
      Width           =   975
      Begin VB.CheckBox OnLine 
         Caption         =   "Online"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   207
         Top             =   330
         Width           =   855
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "On Top"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   206
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   205
         Top             =   990
         Width           =   855
      End
      Begin VB.CheckBox FixWin 
         Caption         =   "Arrange"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   204
         Top             =   660
         Width           =   855
      End
   End
   Begin VB.PictureBox basicButtons 
      Height          =   3075
      Left            =   8610
      ScaleHeight     =   3015
      ScaleWidth      =   885
      TabIndex        =   193
      Top             =   1470
      Width           =   945
      Begin VB.CheckBox chkCreate 
         Caption         =   "Create"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   202
         Top             =   1050
         Width           =   855
      End
      Begin VB.CheckBox chkRedirect 
         Caption         =   "&Redirect"
         Enabled         =   0   'False
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   201
         Top             =   720
         Width           =   855
      End
      Begin VB.CheckBox chkStatus 
         Caption         =   "Status"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   200
         Top             =   2430
         Width           =   855
      End
      Begin VB.CheckBox chkPrintTxt 
         Caption         =   "&Print"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   199
         Top             =   1290
         Width           =   855
      End
      Begin VB.CheckBox chkAssign 
         Caption         =   "Assign"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   198
         Top             =   1860
         Width           =   855
      End
      Begin VB.CheckBox chkFlight 
         Caption         =   "&Flight"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   197
         Top             =   1530
         Width           =   855
      End
      Begin VB.CheckBox chkReject 
         Caption         =   "Reject"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   196
         Top             =   2100
         Width           =   855
      End
      Begin VB.CheckBox chkNew 
         Caption         =   "New"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   195
         Top             =   390
         Width           =   855
      End
      Begin VB.CheckBox FullText 
         Caption         =   "Fulltext"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   194
         Top             =   2640
         Width           =   855
      End
      Begin VB.Image AnyPic 
         Height          =   1710
         Index           =   2
         Left            =   -1740
         Picture         =   "TelexPoolHead.frx":2F86
         Stretch         =   -1  'True
         Top             =   0
         Visible         =   0   'False
         Width           =   2730
      End
   End
   Begin VB.Frame UfisFolder 
      Height          =   915
      Left            =   8610
      TabIndex        =   38
      Top             =   5850
      Width           =   960
      Begin VB.Frame fraFolder 
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   0
         Left            =   120
         TabIndex        =   39
         Top             =   -30
         Visible         =   0   'False
         Width           =   705
         Begin VB.Frame fraLine 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   15
            Index           =   0
            Left            =   0
            TabIndex        =   53
            Top             =   420
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.CheckBox chkTabs 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   52
            ToolTipText     =   "Telex Type Load Filter"
            Top             =   600
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.CheckBox chkFolder 
            Height          =   345
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   51
            ToolTipText     =   "Telex Type Folder"
            Top             =   60
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Frame fraBlend 
            BackColor       =   &H000000FF&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   60
            Index           =   0
            Left            =   0
            TabIndex        =   54
            Top             =   480
            Visible         =   0   'False
            Width           =   645
         End
      End
   End
   Begin VB.PictureBox FipsPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000080FF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Left            =   30
      ScaleHeight     =   705
      ScaleWidth      =   7335
      TabIndex        =   98
      Top             =   6900
      Visible         =   0   'False
      Width           =   7395
      Begin VB.CheckBox chkFips 
         Caption         =   "Keep Me Open"
         Height          =   315
         Index           =   2
         Left            =   5310
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   102
         Top             =   300
         Width           =   1485
      End
      Begin VB.CheckBox chkFips 
         Caption         =   "Terminate Too"
         Height          =   315
         Index           =   1
         Left            =   3810
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   101
         Top             =   300
         Width           =   1485
      End
      Begin VB.CheckBox chkFips 
         Caption         =   "FIPS Terminated "
         Height          =   315
         Index           =   0
         Left            =   0
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   99
         Top             =   0
         Visible         =   0   'False
         Width           =   1605
      End
      Begin VB.Label lblFipsCaption 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Please choose what to do now:"
         Height          =   210
         Left            =   4020
         TabIndex        =   100
         Top             =   60
         Width           =   2595
      End
   End
   Begin VB.TextBox MsgFromClient 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2640
      Locked          =   -1  'True
      TabIndex        =   97
      Text            =   "MsgFromClient"
      Top             =   7530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.TextBox MsgToClient 
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3930
      TabIndex        =   96
      Text            =   "MsgToClient"
      Top             =   7530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Frame fraLoadProgress 
      Caption         =   "Progress ..."
      ForeColor       =   &H00000000&
      Height          =   1035
      Left            =   8610
      TabIndex        =   93
      Top             =   6810
      Visible         =   0   'False
      Width           =   2865
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         ForeColor       =   &H00FFFFFF&
         Height          =   675
         Left            =   60
         TabIndex        =   230
         Top             =   300
         Width           =   2745
         Begin VB.TextBox txtLoadFrom 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   660
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   238
            ToolTipText     =   "Day Date dd.mm.yyyy"
            Top             =   0
            Width           =   1065
         End
         Begin VB.TextBox txtLoadTo 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   660
            Locked          =   -1  'True
            MaxLength       =   10
            TabIndex        =   237
            ToolTipText     =   "Day Date dd.mm.yyyy"
            Top             =   330
            Width           =   1065
         End
         Begin VB.CheckBox Check1 
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   236
            ToolTipText     =   "Reset Time Filter"
            Top             =   0
            Width           =   660
         End
         Begin VB.CheckBox Check2 
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   235
            ToolTipText     =   "Confirm Time Filter"
            Top             =   330
            Width           =   660
         End
         Begin VB.TextBox txtLoadTimeFrom 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1740
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   234
            ToolTipText     =   "Time hh:mm"
            Top             =   0
            Width           =   645
         End
         Begin VB.TextBox txtLoadTimeTo 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1740
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   233
            ToolTipText     =   "Time hh:mm"
            Top             =   330
            Width           =   645
         End
         Begin VB.OptionButton Option1 
            Caption         =   "L"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2430
            Style           =   1  'Graphical
            TabIndex        =   232
            ToolTipText     =   "Times in ATH local (UTC + 180 min.)"
            Top             =   330
            Width           =   300
         End
         Begin VB.OptionButton Option2 
            BackColor       =   &H0080FF80&
            Caption         =   "U"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2430
            Style           =   1  'Graphical
            TabIndex        =   231
            ToolTipText     =   "Times in UTC (ATH Local - 180 min.)"
            Top             =   0
            Value           =   -1  'True
            Width           =   300
         End
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Ufis Server"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10200
      TabIndex        =   92
      Top             =   0
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.Timer SpoolTimer 
      Enabled         =   0   'False
      Interval        =   5
      Left            =   10890
      Top             =   1680
   End
   Begin TABLib.TAB InfoSpooler 
      Height          =   2865
      Left            =   10830
      TabIndex        =   90
      Top             =   2370
      Visible         =   0   'False
      Width           =   675
      _Version        =   65536
      _ExtentX        =   1191
      _ExtentY        =   5054
      _StockProps     =   64
   End
   Begin VB.TextBox InfoFromClient 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1320
      Locked          =   -1  'True
      TabIndex        =   89
      Text            =   "InfoFromClient"
      Top             =   7530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.TextBox InfoToClient 
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   30
      TabIndex        =   88
      Text            =   "InfoToClient"
      Top             =   7530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Frame FolderButtons 
      Height          =   1560
      Index           =   2
      Left            =   9630
      TabIndex        =   84
      Top             =   2850
      Visible         =   0   'False
      Width           =   1170
      Begin VB.CheckBox chkTool 
         Caption         =   "Imp.File"
         Enabled         =   0   'False
         Height          =   315
         Index           =   3
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   246
         Top             =   510
         Width           =   1020
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Imp.Tool"
         Enabled         =   0   'False
         Height          =   315
         Index           =   0
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   87
         Top             =   180
         Width           =   1020
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Spooler"
         Height          =   315
         Index           =   1
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   86
         Top             =   840
         Width           =   1020
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Transmit"
         Height          =   315
         Index           =   2
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   85
         Top             =   1170
         Width           =   1020
      End
   End
   Begin VB.PictureBox RightCover 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4065
      Left            =   11520
      ScaleHeight     =   4005
      ScaleWidth      =   1245
      TabIndex        =   73
      Top             =   60
      Width           =   1305
      Begin VB.PictureBox InfoPanel 
         Height          =   645
         Left            =   90
         ScaleHeight     =   585
         ScaleWidth      =   915
         TabIndex        =   276
         Top             =   2790
         Width           =   975
         Begin VB.PictureBox BcLed 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   11.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   90
            Index           =   3
            Left            =   30
            Picture         =   "TelexPoolHead.frx":6848
            ScaleHeight     =   90
            ScaleWidth      =   90
            TabIndex        =   280
            Top             =   120
            Visible         =   0   'False
            Width           =   90
         End
         Begin VB.PictureBox BcLed 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   11.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   90
            Index           =   2
            Left            =   720
            Picture         =   "TelexPoolHead.frx":689C
            ScaleHeight     =   90
            ScaleWidth      =   90
            TabIndex        =   279
            Top             =   120
            Visible         =   0   'False
            Width           =   90
         End
         Begin VB.CheckBox chkTlxCount 
            Caption         =   "Tlx/Flt"
            Height          =   315
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   277
            ToolTipText     =   "Activate the telex to flight relation counting function"
            Top             =   0
            Width           =   855
         End
         Begin VB.Label BcSpoolCnt 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   270
            Left            =   0
            TabIndex        =   278
            ToolTipText     =   "Update messages to be processed"
            Top             =   315
            Width           =   855
         End
      End
      Begin VB.Timer BcLogTimer 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   690
         Top             =   60
      End
      Begin VB.PictureBox TestPanel 
         Height          =   705
         Left            =   90
         ScaleHeight     =   645
         ScaleWidth      =   915
         TabIndex        =   258
         Top             =   630
         Visible         =   0   'False
         Width           =   975
         Begin VB.PictureBox BcLed 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   11.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   90
            Index           =   1
            Left            =   720
            Picture         =   "TelexPoolHead.frx":68F0
            ScaleHeight     =   90
            ScaleWidth      =   90
            TabIndex        =   273
            Top             =   120
            Visible         =   0   'False
            Width           =   90
         End
         Begin VB.PictureBox BcLed 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   11.25
               Charset         =   161
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   90
            Index           =   0
            Left            =   30
            Picture         =   "TelexPoolHead.frx":6944
            ScaleHeight     =   90
            ScaleWidth      =   90
            TabIndex        =   272
            Top             =   120
            Visible         =   0   'False
            Width           =   90
         End
         Begin VB.CheckBox chkApplTest 
            Caption         =   "Spool"
            Height          =   210
            Index           =   3
            Left            =   30
            TabIndex        =   264
            Tag             =   "Y"
            Top             =   1410
            Value           =   1  'Checked
            Width           =   915
         End
         Begin VB.CheckBox chkBcFlow 
            Caption         =   "BC Logs"
            Height          =   315
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   263
            Top             =   330
            Width           =   855
         End
         Begin VB.CheckBox chkApplTest 
            Caption         =   "Sequ"
            Height          =   210
            Index           =   2
            Left            =   30
            TabIndex        =   262
            Tag             =   "Y"
            Top             =   1170
            Value           =   1  'Checked
            Width           =   915
         End
         Begin VB.CheckBox chkPanic 
            Caption         =   "Export"
            Height          =   315
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   261
            Top             =   0
            Width           =   855
         End
         Begin VB.CheckBox chkApplTest 
            Caption         =   "Import"
            Height          =   240
            Index           =   0
            Left            =   30
            TabIndex        =   260
            Tag             =   "N"
            Top             =   690
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.CheckBox chkApplTest 
            Caption         =   "Time"
            Height          =   210
            Index           =   1
            Left            =   30
            TabIndex        =   259
            Tag             =   "Y"
            Top             =   930
            Value           =   1  'Checked
            Width           =   915
         End
      End
      Begin VB.Image AnyPic 
         Height          =   480
         Index           =   0
         Left            =   0
         Picture         =   "TelexPoolHead.frx":6998
         Stretch         =   -1  'True
         Top             =   0
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Image AnyPic 
         Height          =   480
         Index           =   1
         Left            =   555
         Picture         =   "TelexPoolHead.frx":7B5A
         Stretch         =   -1  'True
         Top             =   0
         Visible         =   0   'False
         Width           =   690
      End
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   150
      Index           =   3
      Left            =   10890
      Picture         =   "TelexPoolHead.frx":8D1C
      ScaleHeight     =   90
      ScaleWidth      =   90
      TabIndex        =   70
      Top             =   5400
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFF00&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   90
      Index           =   2
      Left            =   11070
      Picture         =   "TelexPoolHead.frx":8D70
      ScaleHeight     =   90
      ScaleWidth      =   90
      TabIndex        =   69
      Top             =   5400
      Visible         =   0   'False
      Width           =   90
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Index           =   1
      Left            =   10860
      Picture         =   "TelexPoolHead.frx":8DC4
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   68
      Top             =   6180
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Index           =   0
      Left            =   10860
      Picture         =   "TelexPoolHead.frx":90CE
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   67
      Top             =   5610
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.Frame fraListPanel 
      Height          =   5565
      Left            =   30
      TabIndex        =   61
      Top             =   1500
      Width           =   8535
      Begin VB.PictureBox AreaPanel 
         BackColor       =   &H00404080&
         BorderStyle     =   0  'None
         Height          =   1305
         Index           =   4
         Left            =   4170
         ScaleHeight     =   1305
         ScaleWidth      =   4095
         TabIndex        =   210
         Top             =   4170
         Visible         =   0   'False
         Width           =   4095
         Begin VB.PictureBox TitlePanel 
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   4
            Left            =   60
            ScaleHeight     =   375
            ScaleWidth      =   3945
            TabIndex        =   213
            Top             =   60
            Width           =   3945
            Begin VB.CheckBox chkTitleMax 
               BackColor       =   &H80000002&
               Caption         =   " +"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   4
               Left            =   3570
               Style           =   1  'Graphical
               TabIndex        =   218
               Top             =   0
               Width           =   315
            End
            Begin VB.CheckBox chkTitleBar 
               BackColor       =   &H80000002&
               Caption         =   "Telex Text"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   4
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   217
               Top             =   0
               Width           =   1155
            End
            Begin VB.Frame fraToggleRcv 
               BackColor       =   &H00FF8080&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   11.25
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   4
               Left            =   1770
               TabIndex        =   214
               Top             =   0
               Width           =   1005
               Begin VB.CheckBox chkShowTlx 
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   315
                  Index           =   4
                  Left            =   660
                  Picture         =   "TelexPoolHead.frx":9510
                  Style           =   1  'Graphical
                  TabIndex        =   245
                  ToolTipText     =   "Open Telex Viewer"
                  Top             =   0
                  Width           =   315
               End
               Begin VB.CheckBox chkToggleRcvd 
                  BackColor       =   &H80000002&
                  Enabled         =   0   'False
                  Height          =   315
                  Index           =   4
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   216
                  Top             =   0
                  Width           =   315
               End
               Begin VB.CheckBox chkToggleSent 
                  BackColor       =   &H80000002&
                  Enabled         =   0   'False
                  Height          =   315
                  Index           =   4
                  Left            =   330
                  Style           =   1  'Graphical
                  TabIndex        =   215
                  Top             =   0
                  Width           =   315
               End
            End
         End
         Begin VB.PictureBox ListPanel 
            BackColor       =   &H0000C000&
            BorderStyle     =   0  'None
            Height          =   675
            Index           =   4
            Left            =   60
            ScaleHeight     =   675
            ScaleWidth      =   3015
            TabIndex        =   211
            Top             =   510
            Width           =   3015
            Begin VB.TextBox txtTelexText 
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   11.25
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   585
               Left            =   30
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   212
               Top             =   30
               Width           =   1815
            End
         End
      End
      Begin VB.PictureBox MisCfgPanel 
         BackColor       =   &H00FFFFFF&
         Height          =   1305
         Index           =   0
         Left            =   7440
         ScaleHeight     =   1245
         ScaleWidth      =   765
         TabIndex        =   224
         Top             =   4170
         Visible         =   0   'False
         Width           =   825
         Begin VB.PictureBox TigerX 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Height          =   1215
            Index           =   0
            Left            =   630
            ScaleHeight     =   1215
            ScaleWidth      =   1755
            TabIndex        =   225
            Top             =   960
            Visible         =   0   'False
            Width           =   1755
            Begin VB.Image Tiger 
               Height          =   900
               Index           =   0
               Left            =   0
               Picture         =   "TelexPoolHead.frx":9A6C
               Top             =   0
               Width           =   1755
            End
         End
         Begin VB.Timer TigerTimer 
            Enabled         =   0   'False
            Interval        =   1000
            Left            =   0
            Top             =   0
         End
         Begin VB.Image Tiger 
            Height          =   885
            Index           =   8
            Left            =   4260
            Picture         =   "TelexPoolHead.frx":BACE
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Image Tiger 
            Height          =   900
            Index           =   7
            Left            =   3570
            Picture         =   "TelexPoolHead.frx":DAB8
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Image Tiger 
            Height          =   885
            Index           =   6
            Left            =   3000
            Picture         =   "TelexPoolHead.frx":FB1A
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Image Tiger 
            Height          =   885
            Index           =   5
            Left            =   2310
            Picture         =   "TelexPoolHead.frx":11B04
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Image Tiger 
            Height          =   900
            Index           =   4
            Left            =   1800
            Picture         =   "TelexPoolHead.frx":13AEE
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Image Tiger 
            Height          =   900
            Index           =   3
            Left            =   1290
            Picture         =   "TelexPoolHead.frx":15B50
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Image Tiger 
            Height          =   900
            Index           =   2
            Left            =   660
            Picture         =   "TelexPoolHead.frx":17BB2
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Image Tiger 
            Height          =   900
            Index           =   1
            Left            =   90
            Picture         =   "TelexPoolHead.frx":19C14
            Top             =   2940
            Visible         =   0   'False
            Width           =   1755
         End
      End
      Begin VB.CheckBox fraSplitter 
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   3.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   150
         Index           =   4
         Left            =   8310
         MousePointer    =   15  'Size All
         Style           =   1  'Graphical
         TabIndex        =   223
         Top             =   360
         Width           =   165
      End
      Begin VB.CheckBox fraSplitter 
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   3.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   150
         Index           =   1
         Left            =   8310
         MousePointer    =   15  'Size All
         Style           =   1  'Graphical
         TabIndex        =   222
         Top             =   210
         Width           =   165
      End
      Begin VB.CheckBox fraSplitter 
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   3.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   150
         Index           =   2
         Left            =   8310
         MousePointer    =   7  'Size N S
         Style           =   1  'Graphical
         TabIndex        =   221
         Top             =   570
         Width           =   165
      End
      Begin VB.CheckBox fraSplitter 
         BackColor       =   &H000000FF&
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   3.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1275
         Index           =   0
         Left            =   8310
         MousePointer    =   9  'Size W E
         Style           =   1  'Graphical
         TabIndex        =   220
         Top             =   2100
         Width           =   150
      End
      Begin VB.CheckBox fraSplitter 
         BackColor       =   &H000000FF&
         Caption         =   " "
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   3.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1275
         Index           =   3
         Left            =   8310
         MousePointer    =   9  'Size W E
         Style           =   1  'Graphical
         TabIndex        =   219
         Top             =   780
         Width           =   150
      End
      Begin VB.PictureBox AreaPanel 
         BackColor       =   &H0080FF80&
         BorderStyle     =   0  'None
         Height          =   2625
         Index           =   0
         Left            =   90
         ScaleHeight     =   2625
         ScaleWidth      =   4035
         TabIndex        =   103
         Top             =   150
         Width           =   4035
         Begin VB.PictureBox ListPanel 
            BackColor       =   &H000000C0&
            Height          =   2085
            Index           =   0
            Left            =   60
            ScaleHeight     =   2025
            ScaleWidth      =   3825
            TabIndex        =   110
            Top             =   450
            Width           =   3885
            Begin VB.PictureBox AreaCover 
               BackColor       =   &H000000FF&
               Height          =   465
               Index           =   0
               Left            =   3000
               ScaleHeight     =   405
               ScaleWidth      =   555
               TabIndex        =   189
               Top             =   1380
               Width           =   615
            End
            Begin VB.PictureBox ListGroup 
               BackColor       =   &H00E0E0E0&
               BorderStyle     =   0  'None
               Height          =   885
               Index           =   2
               Left            =   60
               ScaleHeight     =   885
               ScaleWidth      =   3645
               TabIndex        =   111
               Top             =   1020
               Width           =   3645
               Begin VB.PictureBox ListButtons 
                  BackColor       =   &H00C000C0&
                  BorderStyle     =   0  'None
                  Height          =   315
                  Index           =   2
                  Left            =   0
                  ScaleHeight     =   315
                  ScaleWidth      =   3555
                  TabIndex        =   112
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   3555
                  Begin VB.CheckBox chkTask 
                     Caption         =   "Load"
                     Height          =   285
                     Index           =   0
                     Left            =   1890
                     Style           =   1  'Graphical
                     TabIndex        =   227
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   855
                  End
                  Begin VB.Frame fraToggleCnt 
                     BackColor       =   &H00FF8080&
                     BorderStyle     =   0  'None
                     BeginProperty Font 
                        Name            =   "Courier New"
                        Size            =   11.25
                        Charset         =   161
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   345
                     Index           =   2
                     Left            =   1770
                     TabIndex        =   162
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   1665
                     Begin VB.CheckBox chkRcvCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   2
                        Left            =   0
                        Style           =   1  'Graphical
                        TabIndex        =   165
                        ToolTipText     =   "Rcvd Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                     Begin VB.CheckBox chkSntCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   2
                        Left            =   660
                        Style           =   1  'Graphical
                        TabIndex        =   164
                        ToolTipText     =   "Sent Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                     Begin VB.CheckBox chkShowTlx 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   2
                        Left            =   1320
                        Picture         =   "TelexPoolHead.frx":1BC76
                        Style           =   1  'Graphical
                        TabIndex        =   163
                        ToolTipText     =   "Open Telex Viewer"
                        Top             =   0
                        Width           =   300
                     End
                  End
                  Begin VB.CheckBox chkOnlineRefresh 
                     Caption         =   "Refresh"
                     Height          =   285
                     Index           =   2
                     Left            =   0
                     Style           =   1  'Graphical
                     TabIndex        =   114
                     Top             =   0
                     Width           =   855
                  End
                  Begin VB.CheckBox chkLock 
                     Caption         =   "Lock"
                     Height          =   285
                     Index           =   0
                     Left            =   870
                     Style           =   1  'Graphical
                     TabIndex        =   113
                     Top             =   0
                     Width           =   855
                  End
               End
               Begin TABLib.TAB tabTelexList 
                  Height          =   465
                  Index           =   2
                  Left            =   60
                  TabIndex        =   115
                  Top             =   360
                  Width           =   1440
                  _Version        =   65536
                  _ExtentX        =   2540
                  _ExtentY        =   820
                  _StockProps     =   64
                  Lines           =   10
                  HeaderString    =   "Type,Time,Number,Text Extract"
                  HeaderLengthString=   "30,30,30,30"
               End
            End
            Begin VB.PictureBox ListGroup 
               BorderStyle     =   0  'None
               Height          =   885
               Index           =   0
               Left            =   60
               ScaleHeight     =   885
               ScaleWidth      =   3645
               TabIndex        =   116
               Top             =   60
               Width           =   3645
               Begin VB.PictureBox ListButtons 
                  BackColor       =   &H00C000C0&
                  BorderStyle     =   0  'None
                  Height          =   315
                  Index           =   0
                  Left            =   0
                  ScaleHeight     =   315
                  ScaleWidth      =   3555
                  TabIndex        =   137
                  Top             =   0
                  Width           =   3555
                  Begin VB.Frame fraToggleCnt 
                     BackColor       =   &H00FF8080&
                     BorderStyle     =   0  'None
                     BeginProperty Font 
                        Name            =   "Courier New"
                        Size            =   11.25
                        Charset         =   161
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   345
                     Index           =   0
                     Left            =   1770
                     TabIndex        =   154
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   1665
                     Begin VB.CheckBox chkShowTlx 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   0
                        Left            =   1320
                        Picture         =   "TelexPoolHead.frx":1C1D2
                        Style           =   1  'Graphical
                        TabIndex        =   157
                        ToolTipText     =   "Open Telex Viewer"
                        Top             =   0
                        Width           =   300
                     End
                     Begin VB.CheckBox chkSntCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   0
                        Left            =   660
                        Style           =   1  'Graphical
                        TabIndex        =   156
                        ToolTipText     =   "Sent Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                     Begin VB.CheckBox chkRcvCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   0
                        Left            =   0
                        Style           =   1  'Graphical
                        TabIndex        =   155
                        ToolTipText     =   "Rcvd Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                  End
                  Begin VB.CheckBox chkLock 
                     Caption         =   "Refresh"
                     Enabled         =   0   'False
                     Height          =   285
                     Index           =   2
                     Left            =   870
                     Style           =   1  'Graphical
                     TabIndex        =   139
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   855
                  End
                  Begin VB.CheckBox chkOnlineRefresh 
                     Caption         =   "Reload"
                     Enabled         =   0   'False
                     Height          =   285
                     Index           =   0
                     Left            =   0
                     Style           =   1  'Graphical
                     TabIndex        =   138
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   855
                  End
               End
               Begin TABLib.TAB tabTelexList 
                  Height          =   465
                  Index           =   0
                  Left            =   90
                  TabIndex        =   117
                  Top             =   360
                  Width           =   1770
                  _Version        =   65536
                  _ExtentX        =   3122
                  _ExtentY        =   820
                  _StockProps     =   64
                  Lines           =   10
                  HeaderString    =   "Type,Time,Number,Text Extract"
                  HeaderLengthString=   "30,30,30,30"
               End
            End
         End
         Begin VB.PictureBox TitlePanel 
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   0
            Left            =   60
            ScaleHeight     =   375
            ScaleWidth      =   3885
            TabIndex        =   107
            Top             =   60
            Width           =   3885
            Begin VB.Frame fraToggleRcv 
               BackColor       =   &H00808080&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   11.25
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   345
               Index           =   0
               Left            =   2070
               TabIndex        =   148
               Top             =   0
               Visible         =   0   'False
               Width           =   1305
               Begin VB.CheckBox chkToggleRcvd 
                  BackColor       =   &H80000002&
                  Caption         =   "Rcvd"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   315
                  Index           =   0
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   150
                  Top             =   0
                  Width           =   645
               End
               Begin VB.CheckBox chkToggleSent 
                  BackColor       =   &H80000002&
                  Caption         =   "Sent"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   315
                  Index           =   0
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   149
                  Top             =   0
                  Width           =   645
               End
            End
            Begin VB.CheckBox chkTitleBar 
               BackColor       =   &H80000002&
               Caption         =   "Received Telexes"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   0
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   109
               Top             =   0
               Width           =   2010
            End
            Begin VB.CheckBox chkTitleMax 
               BackColor       =   &H80000002&
               Caption         =   " +"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   0
               Left            =   3450
               Style           =   1  'Graphical
               TabIndex        =   108
               Top             =   0
               Width           =   315
            End
         End
      End
      Begin VB.PictureBox AreaPanel 
         BackColor       =   &H00FF80FF&
         BorderStyle     =   0  'None
         Height          =   2205
         Index           =   3
         Left            =   4170
         ScaleHeight     =   2205
         ScaleWidth      =   4095
         TabIndex        =   106
         Top             =   1920
         Width           =   4095
         Begin VB.PictureBox TitlePanel 
            BackColor       =   &H00808080&
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   3
            Left            =   60
            ScaleHeight     =   375
            ScaleWidth      =   3855
            TabIndex        =   134
            Top             =   60
            Width           =   3855
            Begin VB.Frame fraToggleRcv 
               BackColor       =   &H00808080&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   11.25
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   405
               Index           =   3
               Left            =   1200
               TabIndex        =   173
               Top             =   0
               Visible         =   0   'False
               Width           =   1335
               Begin VB.CheckBox chkToggleSent 
                  Height          =   315
                  Index           =   3
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   175
                  Top             =   0
                  Width           =   645
               End
               Begin VB.CheckBox chkToggleRcvd 
                  Height          =   315
                  Index           =   3
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   174
                  Top             =   0
                  Width           =   645
               End
            End
            Begin VB.CheckBox chkTitleBar 
               BackColor       =   &H80000002&
               Caption         =   "0 Flights"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   3
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   136
               Top             =   0
               Width           =   1125
            End
            Begin VB.CheckBox chkTitleMax 
               BackColor       =   &H80000002&
               Caption         =   " +"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   3
               Left            =   2580
               Style           =   1  'Graphical
               TabIndex        =   135
               Top             =   0
               Width           =   315
            End
         End
         Begin VB.PictureBox ListPanel 
            BackColor       =   &H0080FF80&
            Height          =   1605
            Index           =   3
            Left            =   60
            ScaleHeight     =   1545
            ScaleWidth      =   3855
            TabIndex        =   133
            Top             =   480
            Width           =   3915
            Begin VB.PictureBox AreaCover 
               BackColor       =   &H000000FF&
               Height          =   465
               Index           =   3
               Left            =   3000
               ScaleHeight     =   405
               ScaleWidth      =   555
               TabIndex        =   192
               Top             =   900
               Width           =   615
            End
            Begin VB.PictureBox ListGroup 
               BorderStyle     =   0  'None
               Height          =   1455
               Index           =   5
               Left            =   60
               ScaleHeight     =   1455
               ScaleWidth      =   3705
               TabIndex        =   176
               Top             =   60
               Width           =   3705
               Begin TABLib.TAB tabFlightList 
                  Height          =   465
                  Index           =   2
                  Left            =   450
                  TabIndex        =   180
                  Top             =   930
                  Width           =   2190
                  _Version        =   65536
                  _ExtentX        =   3863
                  _ExtentY        =   820
                  _StockProps     =   64
               End
               Begin TABLib.TAB tabFlightList 
                  Height          =   465
                  Index           =   1
                  Left            =   210
                  TabIndex        =   179
                  Top             =   810
                  Width           =   2190
                  _Version        =   65536
                  _ExtentX        =   3863
                  _ExtentY        =   820
                  _StockProps     =   64
               End
               Begin VB.PictureBox ListButtons 
                  BackColor       =   &H00C000C0&
                  BorderStyle     =   0  'None
                  Height          =   645
                  Index           =   5
                  Left            =   0
                  ScaleHeight     =   645
                  ScaleWidth      =   3525
                  TabIndex        =   177
                  Top             =   0
                  Width           =   3525
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "Count"
                     Height          =   285
                     Index           =   4
                     Left            =   1620
                     Style           =   1  'Graphical
                     TabIndex        =   188
                     Top             =   300
                     Visible         =   0   'False
                     Width           =   675
                  End
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "Scroll"
                     Height          =   285
                     Index           =   3
                     Left            =   2310
                     Style           =   1  'Graphical
                     TabIndex        =   187
                     Top             =   300
                     Visible         =   0   'False
                     Width           =   615
                  End
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "Save"
                     Enabled         =   0   'False
                     Height          =   285
                     Index           =   7
                     Left            =   2190
                     Style           =   1  'Graphical
                     TabIndex        =   186
                     Top             =   0
                     Width           =   615
                  End
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "&Link"
                     Height          =   285
                     Index           =   6
                     Left            =   1560
                     Style           =   1  'Graphical
                     TabIndex        =   185
                     Top             =   0
                     Width           =   615
                  End
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "Multi"
                     Enabled         =   0   'False
                     Height          =   285
                     Index           =   5
                     Left            =   2820
                     Style           =   1  'Graphical
                     TabIndex        =   184
                     Top             =   0
                     Width           =   615
                  End
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "Dep"
                     Height          =   285
                     Index           =   2
                     Left            =   1080
                     Style           =   1  'Graphical
                     TabIndex        =   183
                     Top             =   0
                     Width           =   465
                  End
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "Arr"
                     Height          =   285
                     Index           =   1
                     Left            =   600
                     Style           =   1  'Graphical
                     TabIndex        =   182
                     Top             =   0
                     Width           =   465
                  End
                  Begin VB.CheckBox chkFlightList 
                     Caption         =   "Load"
                     Height          =   285
                     Index           =   0
                     Left            =   0
                     Style           =   1  'Graphical
                     TabIndex        =   181
                     Top             =   0
                     Width           =   585
                  End
               End
               Begin TABLib.TAB tabFlightList 
                  Height          =   465
                  Index           =   0
                  Left            =   0
                  TabIndex        =   178
                  Top             =   690
                  Width           =   2010
                  _Version        =   65536
                  _ExtentX        =   3545
                  _ExtentY        =   820
                  _StockProps     =   64
               End
            End
         End
      End
      Begin VB.PictureBox AreaPanel 
         BackColor       =   &H00FF8080&
         BorderStyle     =   0  'None
         Height          =   1695
         Index           =   2
         Left            =   4170
         ScaleHeight     =   1695
         ScaleWidth      =   4095
         TabIndex        =   105
         Top             =   180
         Width           =   4095
         Begin VB.PictureBox TitlePanel 
            BackColor       =   &H00808080&
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   2
            Left            =   90
            ScaleHeight     =   375
            ScaleWidth      =   3885
            TabIndex        =   130
            Top             =   60
            Width           =   3885
            Begin VB.Frame fraToggleRcv 
               BackColor       =   &H00FF8080&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   11.25
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   405
               Index           =   2
               Left            =   2100
               TabIndex        =   170
               Top             =   0
               Visible         =   0   'False
               Width           =   1335
               Begin VB.CheckBox chkToggleSent 
                  Height          =   315
                  Index           =   2
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   172
                  Top             =   0
                  Width           =   645
               End
               Begin VB.CheckBox chkToggleRcvd 
                  Height          =   315
                  Index           =   2
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   171
                  Top             =   0
                  Width           =   645
               End
            End
            Begin VB.CheckBox chkTitleBar 
               BackColor       =   &H80000002&
               Caption         =   "Assign Telex to Folder"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   2
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   132
               Top             =   0
               Width           =   2055
            End
            Begin VB.CheckBox chkTitleMax 
               BackColor       =   &H80000002&
               Caption         =   " +"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   2
               Left            =   3510
               Style           =   1  'Graphical
               TabIndex        =   131
               Top             =   0
               Width           =   315
            End
         End
         Begin VB.PictureBox ListPanel 
            BackColor       =   &H0080FF80&
            Height          =   1095
            Index           =   2
            Left            =   60
            ScaleHeight     =   1035
            ScaleWidth      =   3855
            TabIndex        =   129
            Top             =   480
            Width           =   3915
            Begin VB.PictureBox AreaCover 
               BackColor       =   &H000000FF&
               Height          =   465
               Index           =   2
               Left            =   3180
               ScaleHeight     =   405
               ScaleWidth      =   555
               TabIndex        =   191
               Top             =   60
               Width           =   615
            End
            Begin VB.PictureBox ListGroup 
               BorderStyle     =   0  'None
               Height          =   885
               Index           =   4
               Left            =   60
               ScaleHeight     =   885
               ScaleWidth      =   3045
               TabIndex        =   143
               Top             =   60
               Width           =   3045
               Begin VB.PictureBox ListButtons 
                  BackColor       =   &H00C000C0&
                  BorderStyle     =   0  'None
                  Height          =   315
                  Index           =   4
                  Left            =   0
                  ScaleHeight     =   315
                  ScaleWidth      =   3015
                  TabIndex        =   144
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   3015
                  Begin VB.CheckBox chkAutoRefresh 
                     Caption         =   "DB Reader"
                     Height          =   210
                     Left            =   1770
                     TabIndex        =   226
                     Top             =   60
                     Visible         =   0   'False
                     Width           =   1215
                  End
                  Begin VB.CheckBox chkTlxAssign 
                     Caption         =   "&Save"
                     Height          =   285
                     Left            =   870
                     Style           =   1  'Graphical
                     TabIndex        =   146
                     Top             =   0
                     Width           =   855
                  End
                  Begin VB.CheckBox chkLoadAss 
                     Caption         =   "Refresh"
                     Height          =   285
                     Left            =   0
                     Style           =   1  'Graphical
                     TabIndex        =   145
                     Top             =   0
                     Width           =   855
                  End
               End
               Begin TABLib.TAB tabAssign 
                  Height          =   435
                  Index           =   0
                  Left            =   30
                  TabIndex        =   147
                  Top             =   360
                  Width           =   1920
                  _Version        =   65536
                  _ExtentX        =   3387
                  _ExtentY        =   767
                  _StockProps     =   64
                  Lines           =   10
                  HeaderString    =   "Type,Time,Number,Text Extract"
                  HeaderLengthString=   "30,30,30,30"
               End
            End
         End
      End
      Begin VB.PictureBox AreaPanel 
         BackColor       =   &H00FFFF80&
         BorderStyle     =   0  'None
         Height          =   2655
         Index           =   1
         Left            =   90
         ScaleHeight     =   2655
         ScaleWidth      =   4035
         TabIndex        =   104
         Top             =   2820
         Width           =   4035
         Begin VB.PictureBox TitlePanel 
            BackColor       =   &H00808080&
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   1
            Left            =   60
            ScaleHeight     =   375
            ScaleWidth      =   3885
            TabIndex        =   126
            Top             =   60
            Width           =   3885
            Begin VB.Frame fraToggleRcv 
               BackColor       =   &H00808080&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   11.25
                  Charset         =   161
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   405
               Index           =   1
               Left            =   2070
               TabIndex        =   151
               Top             =   0
               Visible         =   0   'False
               Width           =   1305
               Begin VB.CheckBox chkToggleSent 
                  BackColor       =   &H80000002&
                  Caption         =   "Sent"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   315
                  Index           =   1
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   153
                  Top             =   0
                  Width           =   645
               End
               Begin VB.CheckBox chkToggleRcvd 
                  BackColor       =   &H80000002&
                  Caption         =   "Rcvd"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   315
                  Index           =   1
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   152
                  Top             =   0
                  Width           =   645
               End
            End
            Begin VB.CheckBox chkTitleBar 
               BackColor       =   &H80000002&
               Caption         =   "Sent Telexes"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   1
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   128
               Top             =   0
               Width           =   2010
            End
            Begin VB.CheckBox chkTitleMax 
               BackColor       =   &H80000002&
               Caption         =   " +"
               ForeColor       =   &H00FFFFFF&
               Height          =   315
               Index           =   1
               Left            =   3450
               Style           =   1  'Graphical
               TabIndex        =   127
               Top             =   0
               Width           =   315
            End
         End
         Begin VB.PictureBox ListPanel 
            BackColor       =   &H0000C000&
            Height          =   2085
            Index           =   1
            Left            =   60
            ScaleHeight     =   2025
            ScaleWidth      =   3825
            TabIndex        =   118
            Top             =   480
            Width           =   3885
            Begin VB.PictureBox AreaCover 
               BackColor       =   &H000000FF&
               Height          =   465
               Index           =   1
               Left            =   3000
               ScaleHeight     =   405
               ScaleWidth      =   555
               TabIndex        =   190
               Top             =   1410
               Width           =   615
            End
            Begin VB.PictureBox ListGroup 
               BorderStyle     =   0  'None
               Height          =   945
               Index           =   3
               Left            =   60
               ScaleHeight     =   945
               ScaleWidth      =   3645
               TabIndex        =   119
               Top             =   990
               Width           =   3645
               Begin VB.PictureBox ListButtons 
                  BackColor       =   &H0000C0C0&
                  BorderStyle     =   0  'None
                  Height          =   315
                  Index           =   3
                  Left            =   0
                  ScaleHeight     =   315
                  ScaleWidth      =   3555
                  TabIndex        =   120
                  Top             =   0
                  Visible         =   0   'False
                  Width           =   3555
                  Begin VB.CheckBox chkTask 
                     Caption         =   "Load"
                     Height          =   285
                     Index           =   1
                     Left            =   1920
                     Style           =   1  'Graphical
                     TabIndex        =   229
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   855
                  End
                  Begin VB.Frame fraToggleCnt 
                     BackColor       =   &H00FF8080&
                     BorderStyle     =   0  'None
                     BeginProperty Font 
                        Name            =   "Courier New"
                        Size            =   11.25
                        Charset         =   161
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   345
                     Index           =   3
                     Left            =   1800
                     TabIndex        =   166
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   1665
                     Begin VB.CheckBox chkRcvCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   3
                        Left            =   0
                        Style           =   1  'Graphical
                        TabIndex        =   169
                        ToolTipText     =   "Rcvd Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                     Begin VB.CheckBox chkSntCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   3
                        Left            =   660
                        Style           =   1  'Graphical
                        TabIndex        =   168
                        ToolTipText     =   "Sent Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                     Begin VB.CheckBox chkShowTlx 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   3
                        Left            =   1320
                        Picture         =   "TelexPoolHead.frx":1C72E
                        Style           =   1  'Graphical
                        TabIndex        =   167
                        ToolTipText     =   "Open Telex Viewer"
                        Top             =   0
                        Width           =   300
                     End
                  End
                  Begin VB.CheckBox chkOnlineRefresh 
                     Caption         =   "Refresh"
                     Height          =   285
                     Index           =   3
                     Left            =   0
                     Style           =   1  'Graphical
                     TabIndex        =   122
                     Top             =   0
                     Width           =   855
                  End
                  Begin VB.CheckBox chkLock 
                     Caption         =   "Lock"
                     Height          =   285
                     Index           =   1
                     Left            =   870
                     Style           =   1  'Graphical
                     TabIndex        =   121
                     Top             =   0
                     Width           =   855
                  End
               End
               Begin TABLib.TAB tabTelexList 
                  Height          =   465
                  Index           =   3
                  Left            =   60
                  TabIndex        =   123
                  Top             =   390
                  Width           =   1920
                  _Version        =   65536
                  _ExtentX        =   3387
                  _ExtentY        =   820
                  _StockProps     =   64
                  Lines           =   10
                  HeaderString    =   "Type,Time,Number,Text Extract"
                  HeaderLengthString=   "30,30,30,30"
               End
            End
            Begin VB.PictureBox ListGroup 
               BorderStyle     =   0  'None
               Height          =   915
               Index           =   1
               Left            =   60
               ScaleHeight     =   915
               ScaleWidth      =   3645
               TabIndex        =   124
               Top             =   30
               Width           =   3645
               Begin VB.PictureBox ListButtons 
                  BackColor       =   &H0000C0C0&
                  BorderStyle     =   0  'None
                  Height          =   315
                  Index           =   1
                  Left            =   0
                  ScaleHeight     =   315
                  ScaleWidth      =   3555
                  TabIndex        =   140
                  Top             =   0
                  Width           =   3555
                  Begin VB.Frame fraToggleCnt 
                     BackColor       =   &H00FF8080&
                     BorderStyle     =   0  'None
                     BeginProperty Font 
                        Name            =   "Courier New"
                        Size            =   11.25
                        Charset         =   161
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   345
                     Index           =   1
                     Left            =   1770
                     TabIndex        =   158
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   1665
                     Begin VB.CheckBox chkRcvCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   1
                        Left            =   0
                        Style           =   1  'Graphical
                        TabIndex        =   161
                        ToolTipText     =   "Rcvd Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                     Begin VB.CheckBox chkSntCnt 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   1
                        Left            =   660
                        Style           =   1  'Graphical
                        TabIndex        =   160
                        ToolTipText     =   "Sent Telex in Buffer"
                        Top             =   0
                        Width           =   645
                     End
                     Begin VB.CheckBox chkShowTlx 
                        BeginProperty Font 
                           Name            =   "Arial"
                           Size            =   8.25
                           Charset         =   161
                           Weight          =   400
                           Underline       =   0   'False
                           Italic          =   0   'False
                           Strikethrough   =   0   'False
                        EndProperty
                        Height          =   285
                        Index           =   1
                        Left            =   1320
                        Picture         =   "TelexPoolHead.frx":1CC8A
                        Style           =   1  'Graphical
                        TabIndex        =   159
                        ToolTipText     =   "Open Telex Viewer"
                        Top             =   0
                        Width           =   300
                     End
                  End
                  Begin VB.CheckBox chkLock 
                     Caption         =   "Refresh"
                     Enabled         =   0   'False
                     Height          =   285
                     Index           =   3
                     Left            =   870
                     Style           =   1  'Graphical
                     TabIndex        =   142
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   855
                  End
                  Begin VB.CheckBox chkOnlineRefresh 
                     Caption         =   "Reload"
                     Enabled         =   0   'False
                     Height          =   285
                     Index           =   1
                     Left            =   0
                     Style           =   1  'Graphical
                     TabIndex        =   141
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   855
                  End
               End
               Begin TABLib.TAB tabTelexList 
                  Height          =   465
                  Index           =   1
                  Left            =   60
                  TabIndex        =   125
                  Top             =   360
                  Width           =   2010
                  _Version        =   65536
                  _ExtentX        =   3545
                  _ExtentY        =   820
                  _StockProps     =   64
                  Lines           =   10
                  HeaderString    =   "Type,Time,Number,Text Extract"
                  HeaderLengthString=   "30,30,30,30"
               End
            End
         End
      End
   End
   Begin VB.Frame fraFolderToggles 
      Height          =   930
      Left            =   9600
      TabIndex        =   55
      Top             =   5850
      Width           =   1170
      Begin VB.CheckBox chkToggle 
         Height          =   285
         Index           =   2
         Left            =   915
         Style           =   1  'Graphical
         TabIndex        =   228
         ToolTipText     =   "Close the folder area"
         Top             =   540
         Width           =   180
      End
      Begin VB.CheckBox chkScroll 
         Caption         =   "Show"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   270
         Style           =   1  'Graphical
         TabIndex        =   59
         ToolTipText     =   "Show hidden folders"
         Top             =   540
         Width           =   630
      End
      Begin VB.HScrollBar FolderScroll 
         Height          =   285
         Left            =   270
         TabIndex        =   58
         TabStop         =   0   'False
         Top             =   195
         Width           =   825
      End
      Begin VB.CheckBox chkToggle 
         Height          =   285
         Index           =   1
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   57
         ToolTipText     =   "Filter Multi Select"
         Top             =   540
         Width           =   180
      End
      Begin VB.CheckBox chkToggle 
         Height          =   285
         Index           =   0
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   56
         ToolTipText     =   "Folder Multi Select"
         Top             =   195
         Width           =   180
      End
      Begin VB.Line BorderLine 
         BorderColor     =   &H80000010&
         Index           =   24
         X1              =   0
         X2              =   0
         Y1              =   120
         Y2              =   885
      End
   End
   Begin VB.Frame FolderButtons 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   1
      Left            =   9630
      TabIndex        =   44
      Top             =   4440
      Visible         =   0   'False
      Width           =   1170
      Begin VB.CheckBox AtcTransmit 
         Caption         =   "Transmit"
         Enabled         =   0   'False
         Height          =   285
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   480
         Width           =   1020
      End
      Begin VB.TextBox UseCSGN 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   315
         Left            =   75
         TabIndex        =   48
         ToolTipText     =   "Callsign used by UFIS"
         Top             =   1005
         Width           =   1020
      End
      Begin VB.TextBox AtcCSGN 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Enabled         =   0   'False
         Height          =   315
         Left            =   75
         Locked          =   -1  'True
         TabIndex        =   47
         ToolTipText     =   "Callsign used by ATC"
         Top             =   1335
         Width           =   1020
      End
      Begin VB.TextBox CsgnStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         Enabled         =   0   'False
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   75
         TabIndex        =   46
         Text            =   "UPDATE"
         Top             =   1665
         Width           =   1020
      End
      Begin VB.CheckBox AtcPreview 
         Caption         =   "Preview"
         Enabled         =   0   'False
         Height          =   285
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   180
         Width           =   1020
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "CallSign"
         Enabled         =   0   'False
         Height          =   195
         Left            =   165
         TabIndex        =   50
         Top             =   795
         Width           =   750
      End
   End
   Begin VB.Frame fraStatusFilter 
      Caption         =   "IF Filter"
      Height          =   1035
      Left            =   8880
      TabIndex        =   33
      Top             =   420
      Width           =   1035
      Begin VB.CheckBox chkFilter 
         Caption         =   "SD"
         Height          =   315
         Index           =   1
         Left            =   510
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Load Sent Telexes"
         Top             =   630
         Width           =   435
      End
      Begin VB.CheckBox chkFilter 
         Caption         =   "RC"
         Height          =   315
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Load Received Telexes"
         Top             =   630
         Width           =   435
      End
      Begin VB.CheckBox chkFilter 
         Caption         =   "Status"
         Height          =   315
         Index           =   2
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   34
         ToolTipText     =   "Select a Status Filter"
         Top             =   300
         Width           =   855
      End
   End
   Begin VB.Frame CedaBusyFlag 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1185
      Left            =   9960
      TabIndex        =   15
      Top             =   270
      Width           =   1185
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   3
         Left            =   0
         TabIndex        =   19
         Text            =   "NO LINK"
         Top             =   1050
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H000000FF&
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   2
         Left            =   0
         TabIndex        =   18
         Text            =   "LOG OFF"
         Top             =   600
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00004000&
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   0
         Left            =   0
         TabIndex        =   16
         Text            =   "LINE BUSY"
         Top             =   0
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   1
         Left            =   0
         TabIndex        =   17
         Text            =   "LOG ON"
         Top             =   300
         Visible         =   0   'False
         Width           =   1125
      End
   End
   Begin MSComctlLib.StatusBar MyStatusBar 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   14
      Top             =   7890
      Width           =   15420
      _ExtentX        =   27199
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   5292
            MinWidth        =   5292
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   7408
            MinWidth        =   7408
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11950
            MinWidth        =   176
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1940
            MinWidth        =   1940
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame MainButtons 
      BackColor       =   &H00FFFF00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   30
      TabIndex        =   22
      Top             =   -30
      Width           =   10470
      Begin VB.CheckBox chkWeb 
         Caption         =   "Web"
         Height          =   315
         Left            =   5940
         Style           =   1  'Graphical
         TabIndex        =   247
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkLoader 
         Enabled         =   0   'False
         Height          =   315
         Index           =   1
         Left            =   5070
         Style           =   1  'Graphical
         TabIndex        =   244
         Top             =   60
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.CheckBox chkLoader 
         Caption         =   "Loader"
         Height          =   315
         Index           =   0
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   242
         Top             =   60
         Width           =   855
      End
      Begin VB.PictureBox TopSplit 
         BackColor       =   &H0080FF80&
         Enabled         =   0   'False
         Height          =   345
         Index           =   0
         Left            =   8280
         ScaleHeight     =   285
         ScaleWidth      =   1425
         TabIndex        =   208
         Top             =   240
         Width           =   1485
         Begin VB.CheckBox TopStatus 
            Height          =   210
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   243
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.CheckBox chkConfig 
         Caption         =   "Config"
         Height          =   315
         Left            =   7680
         Style           =   1  'Graphical
         TabIndex        =   95
         Top             =   240
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkStop 
         Caption         =   "Stop"
         Height          =   315
         Left            =   870
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   94
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkOnlineRcv 
         Caption         =   "Online"
         Enabled         =   0   'False
         Height          =   315
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   91
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkAssFolder 
         Caption         =   "Folder"
         Enabled         =   0   'False
         Height          =   315
         Left            =   5070
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkExit 
         Caption         =   "Exit"
         Height          =   315
         Left            =   8550
         MousePointer    =   1  'Arrow
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkReset 
         Caption         =   "Reset"
         Height          =   315
         Left            =   4410
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   60
         Width           =   645
      End
      Begin VB.Frame optMemButtons 
         BackColor       =   &H000000FF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   2610
         TabIndex        =   27
         Tag             =   "3"
         Top             =   60
         Width           =   1800
         Begin VB.OptionButton optMem 
            Caption         =   "MF"
            Height          =   315
            Index           =   1
            Left            =   450
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   0
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            Caption         =   "MS"
            Height          =   315
            Index           =   3
            Left            =   1350
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   0
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            Caption         =   "MU"
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   0
            Width           =   435
         End
         Begin VB.OptionButton optMem 
            Caption         =   "MR"
            Height          =   315
            Index           =   2
            Left            =   900
            Style           =   1  'Graphical
            TabIndex        =   29
            Top             =   0
            Width           =   435
         End
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "Load"
         Height          =   315
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkPrint 
         Caption         =   "Print"
         Enabled         =   0   'False
         Height          =   315
         Left            =   6810
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkSetup 
         Caption         =   "Setup"
         Height          =   315
         Left            =   6810
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkAbout 
         Caption         =   "About"
         Height          =   315
         Left            =   7680
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   60
         Width           =   855
      End
      Begin VB.CheckBox chkHelp 
         Caption         =   "Help"
         Height          =   315
         Left            =   7680
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   60
         Width           =   855
      End
   End
   Begin VB.Frame FolderButtons 
      Height          =   1230
      Index           =   0
      Left            =   9600
      TabIndex        =   13
      Top             =   1560
      Visible         =   0   'False
      Width           =   1170
      Begin VB.CheckBox LoaInfo 
         Caption         =   "ULD Info"
         Height          =   315
         Index           =   2
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   840
         Width           =   1020
      End
      Begin VB.CheckBox LoaInfo 
         Caption         =   "Loading"
         Height          =   315
         Index           =   1
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   510
         Width           =   1020
      End
      Begin VB.CheckBox LoaInfo 
         Caption         =   "PAX Info"
         Height          =   315
         Index           =   0
         Left            =   75
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   180
         Width           =   1020
      End
   End
   Begin VB.Frame fraTextFilter 
      Caption         =   "Telex Text Filter"
      Height          =   1035
      Left            =   5910
      TabIndex        =   9
      Top             =   420
      Width           =   2925
      Begin VB.CheckBox chkTextFilt 
         Caption         =   "Text"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   79
         Top             =   630
         Width           =   600
      End
      Begin VB.CheckBox chkAddrFilt 
         Caption         =   "Addr."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   78
         Top             =   300
         Width           =   600
      End
      Begin VB.OptionButton optTlxText 
         Caption         =   "T"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   75
         ToolTipText     =   "Address Filter: Look in whole Telex Text"
         Top             =   630
         Width           =   300
      End
      Begin VB.OptionButton optTlxAddr 
         BackColor       =   &H0080FF80&
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   74
         ToolTipText     =   "Address Filter: Look in Telex Header"
         Top             =   300
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.TextBox txtAddr 
         Height          =   315
         Left            =   660
         TabIndex        =   12
         ToolTipText     =   "In the Telex Header"
         Top             =   300
         Width           =   1845
      End
      Begin VB.TextBox txtText 
         Height          =   315
         Left            =   660
         TabIndex        =   10
         ToolTipText     =   "Somewhere in the Telex Data"
         Top             =   630
         Width           =   1845
      End
      Begin VB.Label lblFilter 
         Caption         =   "Text"
         Height          =   195
         Index           =   3
         Left            =   150
         TabIndex        =   83
         Top             =   690
         Width           =   465
      End
      Begin VB.Label lblFilter 
         Caption         =   "Addr."
         Height          =   195
         Index           =   2
         Left            =   150
         TabIndex        =   82
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame fraFlightFilter 
      Caption         =   "Flight Filter"
      Height          =   1035
      Left            =   2940
      TabIndex        =   5
      Top             =   420
      Width           =   2925
      Begin VB.CheckBox chkRegList 
         Caption         =   "Regist."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   77
         Top             =   630
         Width           =   780
      End
      Begin VB.CheckBox chkFltList 
         Caption         =   "Flight"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   76
         Top             =   300
         Width           =   780
      End
      Begin VB.OptionButton optLookTlx 
         BackColor       =   &H0080FF80&
         Caption         =   "T"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   72
         ToolTipText     =   "Look in Telex Table"
         Top             =   300
         Value           =   -1  'True
         Width           =   300
      End
      Begin VB.OptionButton optLookAft 
         Caption         =   "F"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2550
         Style           =   1  'Graphical
         TabIndex        =   71
         ToolTipText     =   "Look in Flight Table"
         Top             =   630
         Width           =   300
      End
      Begin VB.TextBox txtRegn 
         Height          =   315
         Left            =   840
         TabIndex        =   11
         ToolTipText     =   "Reg.No. with Dash (-)"
         Top             =   630
         Width           =   1665
      End
      Begin VB.TextBox txtFlns 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   2160
         MaxLength       =   1
         TabIndex        =   8
         ToolTipText     =   "Flight Suffix"
         Top             =   300
         Width           =   345
      End
      Begin VB.TextBox txtFltn 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   1590
         MaxLength       =   5
         TabIndex        =   7
         ToolTipText     =   "Flight Number"
         Top             =   300
         Width           =   555
      End
      Begin VB.TextBox txtFlca 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   840
         MaxLength       =   3
         TabIndex        =   6
         ToolTipText     =   "Flight Carrier (2LC or 3LC)"
         Top             =   300
         Width           =   735
      End
      Begin VB.Label lblFilter 
         Caption         =   "Regist."
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   81
         Top             =   690
         Width           =   645
      End
      Begin VB.Label lblFilter 
         Caption         =   "Flight"
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   80
         Top             =   360
         Width           =   645
      End
   End
   Begin VB.TextBox CurrentUrno 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5250
      TabIndex        =   20
      Text            =   "CurrentUrno"
      Top             =   7530
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.TextBox CurrentRecord 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6360
      TabIndex        =   21
      Text            =   "CurrentRecord"
      Top             =   7530
      Visible         =   0   'False
      Width           =   1065
   End
   Begin TABLib.TAB HelperTab 
      Height          =   765
      Left            =   7470
      TabIndex        =   40
      Top             =   7110
      Visible         =   0   'False
      Width           =   1095
      _Version        =   65536
      _ExtentX        =   1931
      _ExtentY        =   1349
      _StockProps     =   64
   End
End
Attribute VB_Name = "TelexPoolHead"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_Snap As CSnapDialog

Dim MyFilterIniSection As String
Dim LayoutStyle As String
Dim FilterLayout As String
Dim LoadingConfig As Boolean
Dim RestoringFilter As Boolean
Dim ButtonPushedByUser As Boolean

Dim LastHitTlxUrno As String
Dim LastHitTlxTabIdx As Integer

Dim SaveSplitPos(0 To 6) As Long
Dim AreaMaximized(0 To 6) As Boolean
Dim VSplitterSync As Boolean

Dim InitialReset As Boolean
Dim OnlineRcvdIsReady As Boolean
Dim OnlineSentIsReady As Boolean

Dim MainIsOnlineNow As Boolean
Dim OnlineRefreshLoop As Boolean

Dim PrevWindowState As Integer
Dim FdiTimeFields As String
Dim MouseIsDown As Boolean
Dim AssignPanelSize As Long
Dim FlightPanelSize As Long
Private Type AdminMemory
    Folder As String
    AllTab As String
    ElsTab As String
    FreTab As String
    IatTab As String
    TabTag As String    'Tags of TypeFolderTabs
    TabCap As String    'Captions of TypeFolderTabs
    TabVal As String    'Values of TypeFolderTabs
    ChkTag As String    'Tags of TypeFilterButtons
    ChkCap As String    'Captions of TypeFilterButtons
    ChkVal As String    'Values of TypeFilterButtons
    FldVal As String    'FieldValues of the FilterDialog Used in The SqlKey
    TlxKey As String    'WhereClause for TLXTAB built
    TfrKey As String    'New WhereClause TFRTAB
    ExtKey As String    'Additional WhereClause from External (not yet used)
    LookTlx As Boolean
    LookAft As Boolean
    TlxAddr As Boolean
    TlxText As Boolean
End Type
Private ActFilterValues(0 To 8) As AdminMemory
Private NewFilterValues(0 To 8) As AdminMemory
Dim ShowTabButtons As Boolean
Dim ChkTabsIsBusy As Boolean
Dim ReorgFolders As Boolean
Dim MeIsVisible As Boolean
Dim CurButtonPanel As Integer
Dim RcvLoadCapt As String
Dim RcvOnlineCapt As String
Dim SndOnlineCapt As String
Dim SndLoadCapt As String
Dim StopLoading As Boolean
Dim DontRefreshList As Boolean
Dim AddFolderList As String
Dim EditMode As Boolean
Dim InsertTabAssign As Boolean
Dim SaveDateFrom As String
Dim SaveDateTo As String
Dim bmFolderButtons0 As Boolean
Dim LastTlxUrno(0 To 3) As String
Dim bmSortDirection As Boolean
Public Sub SetupAndStartWork(MyCaller As String)
    If Not MainFormIsReadyForUse Then MainFormPrepareLayout = True
    If (MainIsDeploy) Then PoolConfig.FipsLinkCfg(0).Value = 1
    Form_Activate
    SetFormOnTop Me, True, True
    'If MyMainPurpose <> "AUTO_MVT" Then
        If MainIsOnline Then
            If (Not OnlineRcvdIsReady) Or (Not OnlineSentIsReady) Then
                InitDateFields "USR"
                chkLoad.Caption = "Refresh"
                'Wait for the User to click on that button
                'chkLoad.Value = 1
            End If
        Else
        End If
    'End If
End Sub
Private Sub StoreFolderValues(MemIdx As Integer)
    Dim i As Integer
    Dim tmpData As String
    NewFilterValues(MemIdx).TabCap = ""
    NewFilterValues(MemIdx).TabTag = ""
    NewFilterValues(MemIdx).TabVal = ""
    NewFilterValues(MemIdx).ChkCap = ""
    NewFilterValues(MemIdx).ChkTag = ""
    NewFilterValues(MemIdx).ChkVal = ""
    For i = 0 To LastFolderIndex
        NewFilterValues(MemIdx).TabCap = NewFilterValues(MemIdx).TabCap & chkFolder(i).Caption & vbTab
        NewFilterValues(MemIdx).TabTag = NewFilterValues(MemIdx).TabTag & chkFolder(i).Tag & vbTab
        NewFilterValues(MemIdx).TabVal = NewFilterValues(MemIdx).TabVal & CStr(chkFolder(i).Value) & vbTab
        NewFilterValues(MemIdx).ChkCap = NewFilterValues(MemIdx).ChkCap & chkTabs(i).Caption & vbTab
        NewFilterValues(MemIdx).ChkTag = NewFilterValues(MemIdx).ChkTag & chkTabs(i).Tag & vbTab
        NewFilterValues(MemIdx).ChkVal = NewFilterValues(MemIdx).ChkVal & CStr(chkTabs(i).Value) & vbTab
    Next
    
    NewFilterValues(MemIdx).AllTab = GetFolderConfig(CInt(AllTypesCol))
    NewFilterValues(MemIdx).ElsTab = GetFolderConfig(CInt(OtherTypesCol))
    NewFilterValues(MemIdx).FreTab = GetFolderConfig(CInt(FreeTypesCol))
    NewFilterValues(MemIdx).IatTab = GetFolderConfig(CInt(IataTypesCol))
    
    NewFilterValues(MemIdx).FldVal = ""
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtDateFrom & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtTimeFrom & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtDateTo & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtTimeTo & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFlca & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFltn & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtFlns & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtREGN & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtAddr & vbTab
    NewFilterValues(MemIdx).FldVal = NewFilterValues(MemIdx).FldVal & txtText & vbTab
    NewFilterValues(MemIdx).LookTlx = optLookTlx.Value
    NewFilterValues(MemIdx).LookAft = optLookAft.Value
    NewFilterValues(MemIdx).TlxAddr = optTlxAddr.Value
    NewFilterValues(MemIdx).TlxText = optTlxText.Value
End Sub

Private Function GetFolderConfig(Index As Integer)
    Dim tmpData As String
    tmpData = ""
    tmpData = tmpData & chkFolder(Index).Caption & vbTab
    tmpData = tmpData & chkFolder(Index).Tag & vbTab
    tmpData = tmpData & CStr(chkFolder(Index).Value) & vbTab
    tmpData = tmpData & chkTabs(Index).Caption & vbTab
    tmpData = tmpData & chkTabs(Index).Tag & vbTab
    tmpData = tmpData & CStr(chkTabs(Index).Value) & vbTab
    GetFolderConfig = tmpData
End Function

Private Sub RestoreFolderValues(MemIdx As Integer, DoRefresh As Boolean)
    Dim i As Integer
    Dim itm As Integer
    Dim tmpData As String
    optLookTlx.Value = NewFilterValues(MemIdx).LookTlx
    optLookAft.Value = NewFilterValues(MemIdx).LookAft
    optTlxAddr.Value = NewFilterValues(MemIdx).TlxAddr
    optTlxText.Value = NewFilterValues(MemIdx).TlxText
    ReorgFolders = True
    For i = 0 To LastFolderIndex
        itm = i + 1
        If GetItem(NewFilterValues(MemIdx).TabCap, itm, vbTab) <> "" Then
            chkFolder(i).Tag = GetItem(NewFilterValues(MemIdx).TabTag, itm, vbTab)
            chkFolder(i).Caption = GetItem(NewFilterValues(MemIdx).TabCap, itm, vbTab)
            chkTabs(i).Tag = GetItem(NewFilterValues(MemIdx).ChkTag, itm, vbTab)
            chkTabs(i).Caption = GetItem(NewFilterValues(MemIdx).ChkCap, itm, vbTab)
            chkTabs(i).Value = Val(GetItem(NewFilterValues(MemIdx).ChkVal, itm, vbTab))
        End If
        ReorgFolderStatus i
    Next
    
    SetFolderConfig CInt(AllTypesCol), NewFilterValues(MemIdx).AllTab
    SetFolderConfig CInt(OtherTypesCol), NewFilterValues(MemIdx).ElsTab
    SetFolderConfig CInt(FreeTypesCol), NewFilterValues(MemIdx).FreTab
    SetFolderConfig CInt(IataTypesCol), NewFilterValues(MemIdx).IatTab
    
    txtDateFrom.Text = GetItem(NewFilterValues(MemIdx).FldVal, 1, vbTab)
    txtTimeFrom.Text = GetItem(NewFilterValues(MemIdx).FldVal, 2, vbTab)
    txtDateTo.Text = GetItem(NewFilterValues(MemIdx).FldVal, 3, vbTab)
    txtTimeTo.Text = GetItem(NewFilterValues(MemIdx).FldVal, 4, vbTab)
    txtFlca.Text = GetItem(NewFilterValues(MemIdx).FldVal, 5, vbTab)
    txtFltn.Text = GetItem(NewFilterValues(MemIdx).FldVal, 6, vbTab)
    txtFlns.Text = GetItem(NewFilterValues(MemIdx).FldVal, 7, vbTab)
    txtREGN.Text = GetItem(NewFilterValues(MemIdx).FldVal, 8, vbTab)
    txtAddr.Text = GetItem(NewFilterValues(MemIdx).FldVal, 9, vbTab)
    txtText.Text = GetItem(NewFilterValues(MemIdx).FldVal, 10, vbTab)
    ReorgFolders = False
    If Not DontRefreshList Then
        If DoRefresh Then LoadFolderData "", ""
        If Not InitialReset Then
            If AreaPanel(3).Visible Then chkFlightList(4).Value = 1
        End If
    End If
End Sub

Private Sub SetFolderConfig(Index As Integer, CfgList As String)
    chkFolder(Index).Caption = GetItem(CfgList, 1, vbTab)
    chkFolder(Index).Tag = GetItem(CfgList, 2, vbTab)
    chkTabs(Index).Caption = GetItem(CfgList, 4, vbTab)
    chkTabs(Index).Tag = GetItem(CfgList, 5, vbTab)
    chkTabs(Index).Value = Val(GetItem(CfgList, 6, vbTab))
    ReorgFolderStatus Index
End Sub
Private Sub ResetFolderValues(MemIdx As Integer)
    Dim i As Integer
    Dim itm As Integer
    ReorgFolders = True
    optLookTlx.Value = NewFilterValues(MemIdx).LookTlx
    optLookAft.Value = NewFilterValues(MemIdx).LookAft
    optTlxAddr.Value = NewFilterValues(MemIdx).TlxAddr
    optTlxText.Value = NewFilterValues(MemIdx).TlxText
    For i = 0 To LastFolderIndex
        itm = i + 1
        chkFolder(i).Tag = GetItem(ActFilterValues(MemIdx).TabTag, itm, vbTab)
        chkFolder(i).Caption = GetItem(ActFilterValues(MemIdx).TabCap, itm, vbTab)
        chkTabs(i).Tag = GetItem(ActFilterValues(MemIdx).ChkTag, itm, vbTab)
        chkTabs(i).Caption = GetItem(ActFilterValues(MemIdx).ChkCap, itm, vbTab)
        chkTabs(i).Value = Val(GetItem(ActFilterValues(MemIdx).ChkVal, itm, vbTab))
        ReorgFolderStatus i
    Next
    txtDateFrom.Text = GetItem(ActFilterValues(MemIdx).FldVal, 1, vbTab)
    txtTimeFrom.Text = GetItem(ActFilterValues(MemIdx).FldVal, 2, vbTab)
    txtDateTo.Text = GetItem(ActFilterValues(MemIdx).FldVal, 3, vbTab)
    txtTimeTo.Text = GetItem(ActFilterValues(MemIdx).FldVal, 4, vbTab)
    txtFlca.Text = GetItem(ActFilterValues(MemIdx).FldVal, 5, vbTab)
    txtFltn.Text = GetItem(ActFilterValues(MemIdx).FldVal, 6, vbTab)
    txtFlns.Text = GetItem(ActFilterValues(MemIdx).FldVal, 7, vbTab)
    txtREGN.Text = GetItem(ActFilterValues(MemIdx).FldVal, 8, vbTab)
    txtAddr.Text = GetItem(ActFilterValues(MemIdx).FldVal, 9, vbTab)
    txtText.Text = GetItem(ActFilterValues(MemIdx).FldVal, 10, vbTab)
    ReorgFolders = False
    LoadFolderData "", ""
End Sub

Private Sub InitFolderByName(cpName As String, bpSetTab As Boolean)
Dim i As Integer
Dim blFound As Boolean
    blFound = False
    If cpName <> "" Then
        i = 0
        While (i < chkFolder.count) And (Not blFound)
            If (UCase(chkFolder(i).Caption) = UCase(cpName)) Or (InStr(UCase(chkFolder(i).Tag), UCase(cpName)) > 0) Then
                If bpSetTab = True Then chkTabs(i).Value = 1
                chkFolder(i).Tag = "0"
                chkFolder(i).Value = 1
                blFound = True
            End If
            i = i + 1
        Wend
    End If
End Sub

Private Sub AtcCSGN_Change()
    If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
        CsgnStatus.Visible = True
    Else
        CsgnStatus.Visible = False
    End If
End Sub

Private Sub AtcPreview_Click()
    Dim AskRetVal As Integer
    If AtcPreview.Value = 1 Then
        AtcPreview.BackColor = LightestGreen
        AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC Interface Preview", "Still under construction.", "hand", "", UserAnswer)
        AtcPreview.Value = 0
    Else
        AtcPreview.BackColor = vbButtonFace
    End If
End Sub

Private Sub AtcTransmit_Click()
    Dim MsgText As String
    Dim OutFldLst As String
    Dim OldFldLst As String
    Dim OutDatLst As String
    Dim OldDatLst As String
    Dim CurLinTxt As String
    Dim CurNewDat As String
    Dim CurOldDat As String
    Dim CurFldNam As String
    Dim UseFldNam As String
    Dim CurFldDat As String
    Dim CedaFields As String
    Dim CedaData As String
    Dim linNbr As Integer
    Dim CedaResult As String
    Dim CedaRetVal As Integer
    Dim AskRetVal As Integer
    
'PALLAS: ARCID,ADEP,ADEPOLD,ADES,ADESOLD,ETD ,ETDOLD ,ETA ,ATD ,ATA ,RWYID ,ARCTYP,STND
'UFIS:   CSGN ,ORG4,ORG4old,DES4,DES4old,ETDA,ETDAold,ETAA,AIRA,LNDA,RWYA/D,ACT3,------

    If AtcTransmit.Value = 1 Then
        AtcTransmit.BackColor = LightestGreen
        AskRetVal = 1
        If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
            MsgText = ""
            MsgText = MsgText & "Do you want to change the" & vbNewLine
            MsgText = MsgText & "currently used callsign" & vbNewLine
            MsgText = MsgText & "from " & Trim(UseCSGN.Text) & " to " & Trim(AtcCSGN.Text) & " ?"
            MyMsgBox.InfoApi 0, "Updates the flight", "Hint"
            AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC Interface Transmit", MsgText, "ask", "Yes,No", UserAnswer)
            If AskRetVal = 1 Then
                AskRetVal = MyMsgBox.CallAskUser(0, 0, 0, "ATC CSGN Update", "Still under construction.", "hand", "", UserAnswer)
                AskRetVal = 1
            End If
        End If
        MsgText = Trim(txtTelexText.Text)
        If MsgText = "" Then AskRetVal = -1
        If AskRetVal = 1 Then
            Screen.MousePointer = 11
            OutFldLst = ""
            OldFldLst = ""
            OutDatLst = ""
            OldDatLst = ""
            linNbr = 0
            Do
                linNbr = linNbr + 1
                CurLinTxt = GetItem(MsgText, linNbr, vbNewLine)
                If CurLinTxt <> "" Then
                    CurNewDat = GetItem(CurLinTxt, 1, "OLD:")
                    CurOldDat = Trim(GetItem(CurLinTxt, 2, "OLD:"))
                    CurFldNam = Trim(GetItem(CurNewDat, 1, ":"))
                    CurFldDat = Trim(GetItem(CurNewDat, 2, ":"))
                    UseFldNam = CurFldNam
                    Select Case CurFldNam
                        Case "CSGN"
                        Case "ORG4"
                            'If CurFldDat = "LGAT" Then CurFldDat = "LGAV"
                            'If CurOldDat = "LGAT" Then CurOldDat = "LGAV"
                        Case "DES4"
                            'If CurFldDat = "LGAT" Then CurFldDat = "LGAV"
                            'If CurOldDat = "LGAT" Then CurOldDat = "LGAV"
                        Case "ETOD"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            If CurOldDat <> "" Then CurOldDat = Left(CurOldDat & "00", 14)
                            UseFldNam = "ETDA"
                        Case "ETOA"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "ETAA"
                        Case "LAND"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "LNDA"
                        Case "AIRB"
                            If CurFldDat <> "" Then CurFldDat = Left(CurFldDat & "00", 14)
                            UseFldNam = "AIRA"
                        Case "RWYA"
                        Case "RWYD"
                        Case "ACT3"
                        Case "DONE"
                            UseFldNam = ""
                            CurLinTxt = ""
                        Case Else
                            UseFldNam = ""
                    End Select
                    If UseFldNam <> "" Then
                        OutFldLst = OutFldLst & UseFldNam & ","
                        OutDatLst = OutDatLst & CurFldDat & ","
                        If CurOldDat <> "" Then
                            OldFldLst = OldFldLst & UseFldNam & ","
                            OldDatLst = OldDatLst & CurOldDat & ","
                        End If
                    End If
                End If
            Loop While CurLinTxt <> ""
            If OutFldLst <> "" Then
                OutFldLst = Left(OutFldLst, Len(OutFldLst) - 1)
                OutDatLst = Left(OutDatLst, Len(OutDatLst) - 1)
            End If
            If OldFldLst <> "" Then
                OldFldLst = Left(OldFldLst, Len(OldFldLst) - 1)
                OldDatLst = Left(OldDatLst, Len(OldDatLst) - 1)
            End If
            CedaFields = OutFldLst & vbLf & OldFldLst
            CedaData = OutDatLst & vbLf & OldDatLst
            'MsgBox CedaFields & vbNewLine & CedaData
            CedaRetVal = UfisServer.CallCeda(CedaResult, "EXCO", "AFTTAB", CedaFields, CedaData, "ATC", "", 0, True, False)
            Screen.MousePointer = 0
        End If
        AtcTransmit.Value = 0
    Else
        AtcTransmit.BackColor = vbButtonFace
    End If
End Sub

Private Sub BcLogTimer_Timer()
    Static LedIsOn As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim OldColor As Long
    Dim NewColor As Long
    Dim PrvTlxTime As String
    Dim CurTlxTime As String
    If Not LedIsOn Then LedIsOn = True Else LedIsOn = False
    BcLed(0).Visible = LedIsOn
    BcLed(1).Visible = LedIsOn
    
    OldColor = chkPanic.BackColor
    
    chkPanic.BackColor = vbButtonFace
    chkPanic.ForeColor = vbBlack
    
    PrvTlxTime = tabTelexList(2).GetFieldValue(0, "TIME")
    PrvTlxTime = Left(PrvTlxTime, 12)
    MaxLine = tabTelexList(2).GetLineCount - 1
    If MaxLine > 50 Then MaxLine = 50
    For CurLine = 0 To MaxLine
        CurTlxTime = tabTelexList(2).GetFieldValue(CurLine, "TIME")
        CurTlxTime = Left(CurTlxTime, 12)
        If CurTlxTime > PrvTlxTime Then
            chkPanic.BackColor = vbRed
            chkPanic.ForeColor = vbWhite
        End If
        PrvTlxTime = CurTlxTime
    Next

    NewColor = chkPanic.BackColor
    
    If (NewColor = vbRed) And (OldColor <> NewColor) Then
        If chkPanic.Tag <> "DONE" Then
            chkPanic.Value = 1
            chkPanic.Tag = "DONE"
        End If
    Else
        MaxLine = BcLog(2).GetLineCount
        If MaxLine > 0 Then chkPanic.BackColor = LightYellow
        If MaxLine > 3 Then
            If chkPanic.Tag <> "DONE" Then
                chkPanic.Value = 1
                chkPanic.Tag = "DONE"
            End If
        End If
    End If
        
    
End Sub

Private Sub chkAbout_Click()
    Dim myControls() As Control
    Dim CompLine As String
    Dim CompList As String
    If chkAbout.Value = 1 Then
        CompLine = "BcProxy.exe,"
        CompLine = CompLine & UfisServer.GetComponentVersion("BcProxy")
        CompList = CompList & CompLine & vbLf
        CompLine = "UfisAppMng.exe,"
        CompLine = CompLine & UfisServer.GetComponentVersion("ufisappmng")
        CompList = CompList & CompLine & vbLf
        
        MyAboutBox.SetComponents myControls(), CompList
        'MyAboutBox.SetGoldenBorder = True
        MyAboutBox.SetLifeStyle = True
        ShowChildForm Me, chkAbout, MyAboutBox, ""
        
        chkAbout.Value = 0
    End If
End Sub

Private Sub chkAddrFilt_Click()
    If chkAddrFilt.Value = 1 Then
        chkAddrFilt.Value = 0
    End If
End Sub

Private Sub chkApplTest_Click(Index As Integer)
    If chkApplTest(Index).Value = 1 Then
        'chkApplTest(Index).BackColor = vbGreen
        Select Case Index
            Case 0
                ShowFolderButtons "0", "R", "SCOR"
            Case 1
            Case Else
        End Select
    Else
        'chkApplTest(Index).BackColor = vbYellow
        Select Case Index
            Case 0
                ShowFolderButtons "0", "R", "----"
            Case 1
            Case Else
        End Select
    End If
End Sub

'Private Sub chkAssFile_Click()
'    If chkAssFile.Value = 1 Then
'        chkAssFile.BackColor = LightGreen
'        If OnTop.Value = 1 Then SetFormOnTop Me, False,false
'        'AssignFolder.Show , Me
'        If OnTop.Value = 1 Then SetFormOnTop Me, True, true
'    Else
'        'AssignFolder.Hide
'        chkAssFile.BackColor = vbButtonFace
'    End If
'End Sub

Private Sub chkAssFolder_Click()
    Dim TabList As String
    Dim TabTags As String
    Dim TabActs As String
    Dim tmpElse As String
    Dim tmpAll As String
    Dim OldOtherIdx As Long
    Dim OldAllIdx As Long
    
    tmpElse = GetFolderConfig(CInt(OtherTypesCol))
    tmpAll = GetFolderConfig(CInt(AllTypesCol))
    OldOtherIdx = OtherTypesCol
    OldAllIdx = AllTypesCol
    StoreFolderValues CurMem
    If chkAssFolder.Value = 1 Then
        chkAssFolder.BackColor = LightGreen
        If AddFolderList = "" Then chkLoadAss.Value = 1
        
        TabList = AddFolderList
        'Must be present, but might be empty
        TabTags = ""
        TabActs = ""
        chkTabs(OldOtherIdx).Value = 0
        chkTabs(OldAllIdx).Value = 0
        InitFolderList TabList, TabTags, TabActs
        RestoreFolderValues CurMem, False
        ReorgFolders = True
        SetFolderConfig CInt(OtherTypesCol), tmpElse
        SetFolderConfig CInt(AllTypesCol), tmpAll
        ReorgFolders = False
        StoreFolderValues CurMem
        SetFolderValues CurMem
    Else
        chkAssFolder.BackColor = vbButtonFace
        InitFolderList "", "", ""
        RestoreFolderValues CurMem, False
        ReorgFolders = True
        SetFolderConfig CInt(OtherTypesCol), tmpElse
        SetFolderConfig CInt(AllTypesCol), tmpAll
        ReorgFolders = False
        StoreFolderValues CurMem
    End If
    'StoreFolderValues CurMem
End Sub

Private Sub chkAssign_Click()
    If chkAssign.Value = 1 Then
        ShowChildForm Me, chkAssign, AssignFlight, CurrentRecord
        chkAssign.Value = 0
    End If
End Sub

Private Sub chkAssignBar_Click(Index As Integer)
    Dim tmpTag As String
    If Index = 1 Then
'        If chkAssignBar(Index).Value = 1 Then
'            tmpTag = chkTlxTitleBar(3).Tag
'            If tmpTag = "" Then
'                chkTlxTitleBar(3).Tag = "+"
'                chkAssignBar(Index).Caption = " -"
'                ListPanel(1).ZOrder
'                tabAssign(0).ZOrder
'                Form_Resize
'            Else
'                chkTlxTitleBar(3).Tag = ""
'                chkAssignBar(Index).Caption = " +"
'                ResizeTabs
'                ArrangeTabCursor tabAssign(0)
'            End If
'            chkAssignBar(Index).Value = 0
'        End If
    End If
End Sub

Private Sub chkAutoRefresh_Click()
    If chkAutoRefresh.Value = 1 Then
        chkAutoRefresh.BackColor = vbRed
        chkAutoRefresh.ForeColor = vbWhite
        chkAutoRefresh.Refresh
    Else
        chkAutoRefresh.BackColor = MyOwnButtonFace
        chkAutoRefresh.ForeColor = vbBlack
        chkAutoRefresh.Refresh
    End If
End Sub

Private Sub chkBcFlow_Click()
    If chkBcFlow.Value = 1 Then
        Me.WindowState = vbNormal
        If ActionPanel.Visible Then
            ActionPanel.Visible = False
            Me.Width = Me.Width - ActionPanel.Width
        Else
            ActionPanel.Visible = True
            Me.Width = Me.Width + ActionPanel.Width
            ActionPanel.ZOrder
        End If
        chkBcFlow.Value = 0
    End If
End Sub

Private Sub chkBcSpool_Click()
    If chkBcSpool.Value = 1 Then
        BcSpooler.Show
        chkBcSpool.Value = 0
    Else
        'BcSpooler.Hide
    End If
End Sub

Private Sub chkClose_Click()
    Dim VisCnt As Integer
    If chkClose.Value = 1 Then
        VisCnt = CountVisibleForms
        If (VisCnt > 1) Or ((OpenedFromFips) And (FipsIsUpAndRunning)) Then
            MeIsVisible = False
            Me.Hide
            Me.Refresh
            DoEvents
            chkClose.Value = 0
            If VisCnt <= 1 Then
                FindFips.FipsStatusStart "FIPS", chkFips(0), 0, 1
            Else
                FindFips.FipsStatusStop "", chkFips(0)
            End If
        Else
            'chkClose.Value = 0
            HiddenMain.SetShutDownTimer Me
        End If
    End If
End Sub

Private Sub chkCreate_Click()
    Dim CurTlxRec As String
    Dim TlxText As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    If chkCreate.Value = 1 Then
        SwitchMainWindows "CREATE"
        chkCreate.Value = 0
    End If
End Sub

Private Sub chkExit_Click()
    Dim VisCnt As Integer
    If chkExit.Value = 1 Then
        chkExit.BackColor = LightGreen
        chkExit.Refresh
        If Me.WindowState = vbMaximized Then
            Me.WindowState = vbNormal
            Me.Refresh
            DoEvents
        End If
        If ((OpenedFromFips) And (FipsIsUpAndRunning)) Then
            chkExit.Value = 0
            chkClose.Value = 1
        Else
            'chkExit.Value = 0
            HiddenMain.SetShutDownTimer Me
        End If
    Else
        chkExit.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkFilter_Click(Index As Integer)
    If chkFilter(Index).Value = 1 Then chkFilter(Index).BackColor = LightestGreen
    Select Case Index
        Case 0  'Received
            If chkFilter(2).Value = 1 Then
                If chkFilter(Index).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxRcvStat, ""
                    TlxRcvStat.SetMultiSelect True
                Else
                    TlxRcvStat.Hide
                End If
            End If
        Case 1  'Sent
            If chkFilter(2).Value = 1 Then
                If chkFilter(Index).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxSndStat, ""
                    TlxSndStat.SetMultiSelect True
                Else
                    TlxSndStat.Hide
                End If
            End If
        Case 2  'Status
            chkStatus.Value = 0
            If chkFilter(Index).Value = 1 Then
                If chkFilter(1).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxSndStat, ""
                    TlxSndStat.SetMultiSelect True
                End If
                If chkFilter(0).Value = 1 Then
                    ShowChildForm Me, chkFilter(Index), TlxRcvStat, ""
                    TlxRcvStat.SetMultiSelect True
                End If
            Else
                TlxRcvStat.Hide
                TlxSndStat.Hide
            End If
    End Select
    CheckLoadButton False
    If chkFilter(Index).Value = 0 Then chkFilter(Index).BackColor = vbButtonFace
End Sub

Private Sub chkFips_Click(Index As Integer)
    If chkFips(Index).Value = 1 Then
        Me.Visible = True
        chkFips(Index).BackColor = vbYellow
        Select Case Index
            Case 0
                If Me.WindowState <> vbNormal Then Me.WindowState = vbNormal
                FipsIsUpAndRunning = False
                chkExit.Caption = "Exit"
                FipsPanel.Visible = True
                SetFormOnTop Me, True, True
                chkFips(1).Value = 1
            Case 1
                OpenedFromFips = False
                FindFips.FipsStatusStop "", Nothing
                chkFips(0).Value = 0
                chkExit.Value = 1
            Case 2
                OpenedFromFips = False
                FindFips.FipsStatusStop "", Nothing
                chkFips(0).Value = 0
            Case Else
        End Select
    Else
        chkFips(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
                FipsIsUpAndRunning = OpenedFromFips
                If FipsIsUpAndRunning Then chkExit.Caption = "Close"
                FipsPanel.Visible = False
            Case Else
        End Select
    End If
    Form_Resize
End Sub

Private Sub chkFlight_Click()
Dim retval As Boolean
Dim tmpData As String
Dim tmpVpfr As String
Dim tmpVpto As String
Dim FipsFldLst As String
Dim FipsDatLst As String
Dim FipsMsgTxt As String
Dim CurTelexRecord As String
    If chkFlight.Value = 1 Then
        If Trim(CurrentUrno.Text) <> "" Then
            CurTelexRecord = CurrentRecord.Text
            'VPFR , VPTO, ALC3, Fltn, Flns, Regn, Time, FLNU
            '20001210000000,20001210235900,CSA,420, , , ,27641709
            FipsFldLst = "VPFR,VPTO,ALC3,FLTN,FLNS,REGN,TIME,FLNU"
            FipsDatLst = ""
            retval = GetValidTimeFrame(tmpVpfr, tmpVpto, True)
            If retval = True Then
                FipsDatLst = FipsDatLst & tmpVpfr & ","
                FipsDatLst = FipsDatLst & tmpVpto & ","
                tmpData = Trim(GetFieldValue("ALC3", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = Trim(txtFlca.Text)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLTN", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = txtFltn.Tag
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLNS", CurTelexRecord, DataPool.TlxTabFields))
                If tmpData = "" Then tmpData = Trim(txtFlns.Text)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(txtREGN.Text)
                tmpData = Replace(tmpData, "-", "", 1, -1, vbBinaryCompare)
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("TIME", CurTelexRecord, DataPool.TlxTabFields))
                FipsDatLst = FipsDatLst & tmpData & ","
                tmpData = Trim(GetFieldValue("FLNU", CurTelexRecord, DataPool.TlxTabFields))
                If Val(tmpData) < 1 Then tmpData = ""
                FipsDatLst = FipsDatLst & tmpData
                FipsMsgTxt = FipsFldLst & vbLf & FipsDatLst
                CheckFormOnTop Me, False, False
                UfisServer.SendMessageToFips FipsMsgTxt
            Else
                MyMsgBox.CallAskUser 0, 0, 0, "Show Flight Filter", "The actual filter does not match your selection.", "stop", "", UserAnswer
            End If
            chkAssign.Value = 0
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Show Flight", "No telex selected.", "hand", "", UserAnswer
            chkAssign.Value = 0
        End If
        chkFlight.Value = 0
    Else
        'Unload FlightRotation
    End If
End Sub

Private Sub chkFlightBar_Click(Index As Integer)
    Dim idx As Integer
    Dim SetVal As Boolean
    Dim tmpTag As String
'    Select Case Index
'        Case 0
'            If chkFlightBar(Index).Value = 0 Then SetVal = True Else SetVal = False
'            tabFlightList(0).Enabled = SetVal
'            tabFlightList(1).Enabled = SetVal
'            tabFlightList(2).Enabled = SetVal
'            For idx = 0 To chkFlightList.UBound
'                chkFlightList(idx).Enabled = SetVal
'            Next
'        Case 1
'            If chkFlightBar(Index).Value = 1 Then
'                tmpTag = chkFlightBar(Index).Tag
'                If tmpTag = "" Then
'                    chkFlightBar(Index).Tag = "+"
'                    chkFlightBar(Index).Caption = " -"
'                    AreaPanel(3).Top = -105
'                    AreaPanel(3).ZOrder
'                    Form_Resize
'                Else
'                    chkFlightBar(Index).Tag = ""
'                    chkFlightBar(Index).Caption = " +"
'                    fraSplitter(2).ZOrder
'                    fraSplitter(1).ZOrder
'                    fraSplitter(4).ZOrder
'                    Form_Resize
'                    ArrangeTabCursor tabFlightList(0)
'                    ArrangeTabCursor tabFlightList(1)
'                End If
'                chkFlightBar(Index).Value = 0
'            End If
'    End Select
End Sub

Private Sub chkFlightList_Click(Index As Integer)
    Dim OldVal As Integer
    Dim tmpCount As Long
    Dim ArrCount As Long
    Dim DepCount As Long
    Dim TlxCount As Long
    Dim tmpCheck As String
    Dim tmpCapt As String
    Dim SqlKey As String
    
    If chkFlightList(Index).Value = 1 Then
        chkFlightList(Index).BackColor = LightGreen
        chkFlightList(Index).Refresh
        Select Case Index
            Case 0  'Refresh
                tmpCheck = CheckValidFilterFields(False)
                If tmpCheck = "OK" Then
                    chkFlightList(0).Tag = ""
                    OldVal = chkFlightList(1).Value
                    chkFlightList(1).Value = 1
                    chkFlightList(1).Refresh
                    chkFlightList(2).Refresh
                    LoadFlightData 0, True
                    tmpCount = tabFlightList(0).GetLineCount
                    If chkFlightList(0).Tag <> "" Then
                        chkTitleBar(3).Caption = CStr(tmpCount) & " Arr. Flights"
                    End If
                    If tmpCount <= 0 Then chkFlightList(1).Value = 0
                    chkFlightList(2).Value = 1
                    chkFlightList(1).Refresh
                    chkFlightList(2).Refresh
                    LoadFlightData 1, True
                    tmpCount = tabFlightList(1).GetLineCount
                    If chkFlightList(0).Tag <> "" Then
                        chkTitleBar(3).Caption = CStr(tmpCount) & " Dep. Flights"
                    End If
                    If tmpCount <= 0 Then chkFlightList(2).Value = 0
                    chkFlightList(4).Value = 1
                    chkFlightList(1).Value = OldVal
                    chkFlightList(1).Refresh
                    chkFlightList(2).Refresh
                Else
                    If chkFromTo.Tag <> "CHK" Then
                        tmpCheck = "Wrong Filter Values!" & vbNewLine & tmpCheck
                    End If
                    MyMsgBox.CallAskUser 0, 0, 0, "Load Flights", tmpCheck, "stop", "", UserAnswer
                End If
                chkFlightList(Index).Value = 0
            Case 1  'Arr
                AreaCover(3).Visible = False
                chkFlightList(2).Value = 0
                tmpCount = tabFlightList(0).GetLineCount
                chkTitleBar(3).Caption = CStr(tmpCount) & " Arr. Flights"
                chkTitleBar(3).Refresh
                'DEBUGGED
                'tabFlightList(0).Visible = True
                'tabFlightList(1).Visible = False
                'tabFlightList(2).Visible = False
                'DEBUGGED
                tabFlightList(0).ZOrder
                'Me.Refresh
            Case 2  'Dep
                AreaCover(3).Visible = False
                chkFlightList(1).Value = 0
                tmpCount = tabFlightList(1).GetLineCount
                chkTitleBar(3).Caption = CStr(tmpCount) & " Dep. Flights"
                chkTitleBar(3).Refresh
                'DEBUGGED
                'tabFlightList(1).Visible = True
                'tabFlightList(0).Visible = False
                'tabFlightList(2).Visible = False
                'DEBUGGED
                tabFlightList(1).ZOrder
                'Me.Refresh
            Case 3  'Scroll
                SyncFlightFlist
                chkFlightList(Index).Value = 0
            Case 4  'Count
                CountFlightTelex
                chkFlightList(Index).Value = 0
            Case 6  'Link Telex and Flight
'                StopOnline = True
                chkFlightList(0).Enabled = False
                chkFlightList(1).Enabled = False
                chkFlightList(2).Enabled = False
                chkFlightList(7).Enabled = True
                chkFlightList(7).BackColor = vbRed
                chkFlightList(7).ForeColor = vbWhite
                chkFlightList(1).Value = 0
                chkFlightList(2).Value = 0
                'DEBUGGED
                'tabFlightList(0).Visible = False
                'tabFlightList(1).Visible = False
                'DEBUGGED
                'tabFlightList(2).Visible = True
                tabFlightList(2).ZOrder
                EditFlightsToTelex
            Case 7  'Insert Links of Telex and Flight
                AssignFlightsToTelex
                If chkTlxAssign.BackColor = vbRed Then
                    chkTlxAssign.Value = 1
                End If
                chkFlightList(7).Value = 0
            Case Else
        End Select
    Else
        chkFlightList(Index).BackColor = vbButtonFace
        chkFlightList(Index).Refresh
        Select Case Index
            Case 0
                TlxCount = DataPool.TelexData(CurMem).GetLineCount
                If TlxCount <= 0 Then
                    chkFlightList(1).Value = 0
                    chkFlightList(2).Value = 0
                    'ArrCount = tabFlightList(0).GetLineCount
                    'DepCount = tabFlightList(1).GetLineCount
                    'tmpCapt = CStr(ArrCount) & " ARR / " & CStr(DepCount) & " DEP Flights"
                End If
            Case 1  'Arr
                If chkFlightList(6).Value = 0 Then
                    If chkFlightList(2).Value = 0 Then
                        AreaCover(3).Visible = True
                        AreaCover(3).ZOrder
                        ArrCount = tabFlightList(0).GetLineCount
                        DepCount = tabFlightList(1).GetLineCount
                        If (ArrCount + DepCount) > 0 Then
                            tmpCapt = CStr(ArrCount) & " Arr. / " & CStr(DepCount) & " Dep. Flights"
                        Else
                            tmpCapt = "Flight Lists"
                        End If
                        chkTitleBar(3).Caption = tmpCapt
                    End If
                End If
            Case 2  'Dep
                If chkFlightList(6).Value = 0 Then
                    If chkFlightList(1).Value = 0 Then
                        AreaCover(3).Visible = True
                        AreaCover(3).ZOrder
                        ArrCount = tabFlightList(0).GetLineCount
                        DepCount = tabFlightList(1).GetLineCount
                        If (ArrCount + DepCount) > 0 Then
                            tmpCapt = CStr(ArrCount) & " Arr. / " & CStr(DepCount) & " Dep. Flights"
                        Else
                            tmpCapt = "Flight Lists"
                        End If
                        chkTitleBar(3).Caption = tmpCapt
                    End If
                End If
            Case 6  'Link Telex and Flight
                'chkFlightList(5).BackColor = vbButtonFace
                'chkFlightList(5).Value = 0
                'chkFlightList(5).Enabled = False
                If EditMode Then
                    chkFlightList(6).Value = 1
                Else
                    chkFlightList(0).Enabled = True
                    chkFlightList(1).Enabled = True
                    chkFlightList(2).Enabled = True
                    chkFlightList(7).Enabled = False
                    chkFlightList(7).BackColor = vbButtonFace
                    chkFlightList(7).ForeColor = vbBlack
                    chkFlightList(0).Value = 1
'                    AddToOnlineWindow
                End If
            Case 7  'Insert Links of Telex and Flight
                chkFlightList(7).BackColor = vbRed
                chkFlightList(7).ForeColor = vbWhite
            Case Else
        End Select
    End If
End Sub

Private Sub chkFltList_Click()
    If chkFltList.Value = 1 Then
        If optLookAft.Value = True Then
            chkFltList.BackColor = LightGreen
            fraFlightStatus.Top = fraFlightFilter.Top + chkFltList.Top - 105
            fraFlightStatus.Left = fraFlightFilter.Left + chkFltList.Left + chkFltList.Width + 15
            fraFlightStatus.Visible = True
            fraFlightStatus.ZOrder
        Else
            chkFltList.Value = 0
        End If
    Else
        fraFlightStatus.Visible = False
        chkFltList.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkFolder_Click(Index As Integer)
    Dim AddToOnline As Boolean
    
    If (chkFolder(Index).Value = 1) And (Not ReorgFolders) Then
        AddToOnline = False
        If chkFolder(Index).Tag <> "1" Then
            If Not ChkTabsIsBusy Then CheckToggledTabs Index, chkFolder, 0
            chkFolder(Index).Top = 0
            chkFolder(Index).Height = 375
            chkFolder(Index).BackColor = DarkestBlue
            chkFolder(Index).ForeColor = vbWhite
            fraBlend(Index).ZOrder
'            If InStr(AddFolderList, chkFolder(Index).Caption) > 0 Then
'                StopOnline = True
'            End If
        Else
            If Not ChkTabsIsBusy Then CheckToggledTabs Index, chkFolder, 0
            chkFolder(Index).Tag = "0"
            chkFolder(Index).Top = 60
            chkFolder(Index).Height = 345
            chkFolder(Index).BackColor = vbButtonFace
            chkFolder(Index).ForeColor = vbBlack
            fraLine(Index).ZOrder
'            If InStr(AddFolderList, chkFolder(Index).Caption) > 0 Then
'                AddToOnline = True
'            End If
        End If
        If Not StopNestedCalls Then LoadFolderData "", ""
'        If AddToOnline Then
'            AddToOnlineWindow
'        End If
        chkFolder(Index).Value = 0
    End If
End Sub
Private Sub ReorgFolderStatus(Index As Integer)
    If (chkFolder(Index).Tag <> "") And (chkFolder(Index).Tag <> "0") Then
        chkFolder(Index).Top = 0
        chkFolder(Index).Height = 375
        chkFolder(Index).BackColor = DarkestBlue
        chkFolder(Index).ForeColor = vbWhite
        fraBlend(Index).ZOrder
    Else
        chkFolder(Index).Top = 60
        chkFolder(Index).Height = 345
        chkFolder(Index).BackColor = vbButtonFace
        chkFolder(Index).ForeColor = vbBlack
        fraLine(Index).ZOrder
    End If
End Sub
Private Sub CheckToggledTabs(Index As Integer, CheckObj, ToggleIdx As Integer)
    Dim MaxIdx As Integer
    Dim LastIdx As Integer
    Dim DownValue As Integer
    Dim UpValue As Integer
    Dim i As Integer
    Dim j As Integer
    If Not ChkTabsIsBusy Then
        StopNestedCalls = True
        ChkTabsIsBusy = True
        If ToggleIdx = 0 Then UpValue = 1 Else UpValue = 0
        DownValue = 1
        If chkToggle(ToggleIdx).Value = 0 Then
            For i = 0 To CheckObj.UBound
                If (i <> Index) And (CheckObj(i).Tag <> "0") Then
                    CheckObj(i).Value = UpValue
                    CheckObj(i).Tag = "0"
                End If
            Next
            If CheckObj(Index).Tag = "1" Then CheckObj(Index).Tag = "0" Else CheckObj(Index).Tag = "1"
        Else
            LastIdx = CheckObj.UBound
            MaxIdx = CheckObj.UBound - 1
            If Index <= MaxIdx Then
                If (CheckObj(LastIdx).Tag = "1") Then
                    CheckObj(LastIdx).Value = UpValue
                    CheckObj(LastIdx).Tag = "0"
                End If
                For i = 0 To MaxIdx
                    If CheckObj(i).Tag = "2" Then CheckObj(i).Tag = "0"
                Next
            ElseIf Index = LastIdx Then
                If CheckObj(LastIdx).Tag = "0" Then
                    For i = 0 To MaxIdx
                        If CheckObj(i).Tag <> "0" Then
                            CheckObj(i).Value = UpValue
                            CheckObj(i).Tag = "2"
                        End If
                    Next
                Else
                    For i = 0 To MaxIdx
                        If CheckObj(i).Tag = "2" Then
                            CheckObj(i).Value = DownValue
                            CheckObj(i).Tag = "1"
                        End If
                    Next
                End If
            End If
            If CheckObj(Index).Tag = "1" Then CheckObj(Index).Tag = "0" Else CheckObj(Index).Tag = "1"
        End If
        ChkTabsIsBusy = False
        StopNestedCalls = False
    End If
End Sub

Private Sub LoadFolderData(AftUrno As String, ConText As String)
    Dim ShowAllTypes As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurTag As String
    Dim CurTyp As String
    Dim LineList As String
    Dim TlxLine As String
    Dim LineNbr As String
    Dim LineNo As String
    Dim AddTxt As String
    Dim SereCol As Long
    Dim Txt1Col As Long
    Dim TtypCol As Long
    Dim CurSere As String
    Dim CurTtyp As String
    Dim LineCount0 As Long
    Dim LineCount1 As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim i As Integer
    Dim itm As Integer
    Dim RcvIdx As Integer
    Dim SndIdx As Integer
    Dim AddCapt As String
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As Integer
    Dim count As Integer
    Dim CurLin As Integer
    Dim TlxUrnoList As String
    Dim CurUrno As String
    Dim UrnoCol As Long
    Dim memCount As Long
    Dim memLine As Long
    Dim ChkFolderList As String
    Dim ChkName As String
    On Error Resume Next
    
    If chkOnlineRcv.Value = 0 Then
        RcvIdx = 0
        SndIdx = 1
        Select Case CurMem
            Case 2
                chkToggleRcvd(0).Value = 1
                AddCapt = "(From Recvd)"
            Case 3
                chkToggleRcvd(1).Value = 1
                AddCapt = "(From Sent)"
            Case Else
            AddCapt = "(Loaded)"
        End Select
    Else
        RcvIdx = 2
        SndIdx = 3
        AddCapt = "(Online)"
    End If
    
    Screen.MousePointer = 11
    If AftUrno <> "" Then
        For i = 0 To chkFolder.UBound
            If chkFolder(i).Tag = "1" Then chkFolder(i).Value = 1
        Next
    End If
    If ConText <> "" Then AddTxt = " of " & ConText
    txtTelexText.Text = ""
    txtTelexText.Refresh
    tabTelexList(RcvIdx).ResetContent
    tabTelexList(RcvIdx).Refresh
    tabTelexList(SndIdx).ResetContent
    tabTelexList(SndIdx).Refresh
    TtypCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "TTYP"))
    SereCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "SERE"))
    Txt1Col = CLng(GetRealItemNo(DataPool.TlxTabFields, "TXT1"))
    UrnoCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "URNO"))
    LineCount0 = 0
    LineCount1 = 0
    DataPool.TelexData(CurMem).SetInternalLineBuffer False
    ShowAllTypes = False
    ChkFolderList = "," & AddFolderList & ","
    For i = 0 To chkFolder.UBound
        If (chkFolder(i).Tag = "1") Or (AftUrno <> "") Then
            If CLng(i) = AllTypesCol Then ShowAllTypes = True
            If AftUrno <> "" Then
                ShowAllTypes = False
                CurTag = AftUrno
            Else
                CurTag = fraFolder(i).Tag
                If CurTag = "" Then CurTag = "xxxx"
            End If
            itm = 1
            CurTyp = GetItem(CurTag, itm, ",")
            While CurTyp <> ""
                If Not ShowAllTypes Then
                    ChkName = "," & chkFolder(i).Caption & ","
                    If InStr(ChkFolderList, ChkName) > 0 Then
                        tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                        tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                        If tmpDatFrom <> tmpDatTo Then
                            tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
                        Else
                            tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
                        End If
                        ActResult = ""
                        ActCmd = "RTA"
                        ActTable = "TFNTAB"
                        ActFldLst = "DISTINCT TURN"
                        ActCondition = tmpDatFrom & " AND FNAM = '" & chkFolder(i).Caption & "'"
                        ActOrder = ""
                        ActDatLst = ""
                        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                        If retval >= 0 Then
                            count = UfisServer.DataBuffer(0).GetLineCount - 1
                            If count >= 0 Then
                                For CurLin = 0 To count
                                    TlxUrnoList = TlxUrnoList & UfisServer.DataBuffer(0).GetLineValues(CurLin) & ","
                                Next
                                TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                            End If
                            If Len(TlxUrnoList) > 0 Then
                                count = DataPool.TelexData(CurMem).GetLineCount - 1
                                For CurLin = 0 To count
                                    CurUrno = DataPool.TelexData(CurMem).GetColumnValue(CurLin, UrnoCol)
                                    If InStr(TlxUrnoList, CurUrno) > 0 Then
                                        LineList = LineList & CurLin & ","
                                    End If
                                Next
                                If Len(LineList) > 0 Then
                                    LineList = Left(LineList, Len(LineList) - 1)
                                End If
                            End If
                        End If
                    Else
                        If CLng(i) = OtherTypesCol Then
                            LineList = DataPool.TelexData(CurMem).GetLinesByStatusValue(2, 1)
                        ElseIf AftUrno = "" Then
                            LineList = DataPool.TelexData(CurMem).GetLinesByIndexValue("TTYP", CurTyp, 0)
                        Else
                            LineList = DataPool.TelexData(CurMem).GetLinesByIndexValue("FLNU", CurTyp, 0)
                            If chkAssFolder.Enabled Then
                                ActResult = ""
                                ActCmd = "RTA"
                                ActTable = "TLKTAB"
                                ActFldLst = "DISTINCT TURN"
                                ActCondition = "WHERE FURN = " & CurTyp
                                ActOrder = ""
                                ActDatLst = ""
                                retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                                If retval >= 0 Then
                                    count = UfisServer.DataBuffer(0).GetLineCount - 1
                                    If count >= 0 Then
                                        memCount = DataPool.TelexData(CurMem).GetLineCount - 1
                                        For CurLin = 0 To count
                                            CurUrno = UfisServer.DataBuffer(0).GetLineValues(CurLin)
                                            For memLine = 0 To memCount
                                                If CurUrno = DataPool.TelexData(CurMem).GetColumnValue(memLine, UrnoCol) Then
                                                    If LineList = "" Then
                                                        LineList = memLine
                                                    Else
                                                        LineList = LineList & "," & memLine
                                                    End If
                                                    Exit For
                                                End If
                                            Next
                                        Next
                                    End If
                                End If
                            End If
                        End If
                    End If
                    HelperTab.ResetContent
                    HelperTab.InsertBuffer LineList, ","
                    MaxLine = HelperTab.GetLineCount - 1
                Else
                    MaxLine = DataPool.TelexData(CurMem).GetLineCount - 1
                End If
                For CurLine = 0 To MaxLine
                    If (CurLine Mod 200) = 0 Then DoEvents
                    If Not ShowAllTypes Then
                        LineNo = CLng(HelperTab.GetColumnValue(CurLine, 0))
                    Else
                        LineNo = CurLine
                    End If
                    TlxLine = DataPool.TelexData(CurMem).GetColumnValues(LineNo, BrowserColumns) & "," & CStr(LineNo)
                    CurSere = DataPool.TelexData(CurMem).GetColumnValue(LineNo, SereCol)
                    CurTtyp = DataPool.TelexData(CurMem).GetColumnValue(LineNo, TtypCol)
                    DataPool.TelexData(CurMem).GetLineColor LineNo, CurForeColor, CurBackColor
                    If (CurSere = "R") Or (CurSere = "I") Then
                        tabTelexList(RcvIdx).InsertTextLine TlxLine, False
                        If CurTtyp = "FREE" Then tabTelexList(RcvIdx).SetColumnValue LineCount0, 2, ""
                        tabTelexList(RcvIdx).SetLineColor LineCount0, CurForeColor, CurBackColor
                        LineCount0 = LineCount0 + 1
                    Else
                        tabTelexList(SndIdx).InsertTextLine TlxLine, False
                        If CurTtyp = "FREE" Then tabTelexList(SndIdx).SetColumnValue LineCount1, 2, ""
                        If CurSere = "C" Then CurBackColor = LightestYellow
                        tabTelexList(SndIdx).SetLineColor LineCount1, CurForeColor, CurBackColor
                        LineCount1 = LineCount1 + 1
                    End If
                Next
                HelperTab.ResetContent
                itm = itm + 1
                CurTyp = GetItem(CurTag, itm, ",")
            Wend
        End If
        If AftUrno <> "" Then Exit For
    Next
    SetTlxTabCaption 0, RcvIdx, AddCapt
    SetTlxTabCaption 1, SndIdx, AddCapt
    'chktitlebar(0).Caption = CStr(tabTelexList(RcvIdx).GetLineCount) & " " & chktitlebar(0).Tag & " " & AddCapt & AddTxt
    'chktitlebar(1).Caption = CStr(tabTelexList(SndIdx).GetLineCount) & " " & chktitlebar(1).Tag & " " & AddCapt & AddTxt
    'chkRcvCnt(RcvIdx).Caption = CStr(tabTelexList(RcvIdx).GetLineCount)
    'chkSntCnt(SndIdx).Caption = CStr(tabTelexList(SndIdx).GetLineCount)
    chkRcvCnt(0).Caption = CStr(tabTelexList(0).GetLineCount)
    chkRcvCnt(1).Caption = CStr(tabTelexList(0).GetLineCount)
    chkRcvCnt(2).Caption = CStr(tabTelexList(2).GetLineCount)
    chkRcvCnt(3).Caption = CStr(tabTelexList(2).GetLineCount)
    chkSntCnt(0).Caption = CStr(tabTelexList(1).GetLineCount)
    chkSntCnt(1).Caption = CStr(tabTelexList(1).GetLineCount)
    chkSntCnt(2).Caption = CStr(tabTelexList(3).GetLineCount)
    chkSntCnt(3).Caption = CStr(tabTelexList(3).GetLineCount)
    tabTelexList(RcvIdx).Tag = AddTxt
    tabTelexList(SndIdx).Tag = AddTxt
    If RcvIdx = 1 Then
    '    tabTelexList(RcvIdx).Sort "4", True, True
    End If
    tabTelexList(RcvIdx).AutoSizeColumns
    tabTelexList(RcvIdx).Refresh
    If SndIdx = 1 Then
    '    tabTelexList(SndIdx).Sort "4", True, True
    End If
    tabTelexList(SndIdx).AutoSizeColumns
    tabTelexList(SndIdx).Refresh
    ShowFolderButtons "0", "", ""
    If tabTelexList(RcvIdx).GetLineCount > 0 Then
        tabTelexList(RcvIdx).SetCurrentSelection 0
    ElseIf tabTelexList(SndIdx).GetLineCount > 0 Then
        tabTelexList(SndIdx).SetCurrentSelection 0
    End If

    tabTelexList(0).Sort "4", bmSortDirection, True
    tabTelexList(1).Sort "4", bmSortDirection, True
    tabTelexList(0).RedrawTab
    tabTelexList(1).RedrawTab

    If ApplicationIsStarted Then
        If tabTelexList(RcvIdx).Visible = True Then
            tabTelexList(RcvIdx).SetFocus
        Else
            If tabTelexList(SndIdx).Visible = True Then
                tabTelexList(SndIdx).SetFocus
            End If
        End If
    End If
    Screen.MousePointer = 0
End Sub

'Private Sub chkFolder_DragDrop(Index As Integer, Source As Control, X As Single, Y As Single)
'    Dim tmpText As String
'    If TypeOf Source Is TABLib.Tab Then
'        tmpText = Source.GetLineValues(Source.GetCurrentSelected)
'        'chkAssFile.Value = 1
'    End If
'End Sub

'Private Sub chkFolder_DragOver(Index As Integer, Source As Control, X As Single, Y As Single, State As Integer)
'    Dim IsAllowed As Boolean
'    Dim tmpName As String
'    If TypeOf Source Is TABLib.Tab Then
'        If State = 0 Then IsAllowed = True
'        tmpName = chkFolder(Index).Caption
'        If InStr(AddFolderList, tmpName) > 0 Then IsAllowed = True Else IsAllowed = False
'        If State = 1 Then IsAllowed = False
'        If IsAllowed Then
'            Source.DragIcon = Picture1(1).Picture
'        Else
'            Source.DragIcon = Picture1(0).Picture
'        End If
'    End If
'End Sub

Private Sub chkFromTo_Click()
    Dim tmpMsg As String
    Dim retval As Integer
    Dim tmpMaxTag As String
    Dim tmpChkTag As String
    Dim tmpDate As String
    Dim maxDiff As Long
    Dim tmpDiff As Long
    Dim tmpVpfr
    Dim tmpVpto
    If chkFromTo.Value = 1 Then
        chkFromTo.BackColor = LightGreen
        chkFromTo.ForeColor = vbBlack
        tmpMaxTag = fraTimeFilter.Tag
        tmpChkTag = chkFromTo.Tag
        If tmpChkTag = "CHK" Then
            If tmpMaxTag <> "" Then
                maxDiff = Val(GetItem(tmpMaxTag, 1, ","))
                tmpVpfr = CedaFullDateToVb(txtDateFrom.Tag & txtTimeFrom.Tag)
                tmpVpto = CedaFullDateToVb(txtDateTo.Tag & txtTimeTo.Tag)
                tmpDiff = DateDiff("n", tmpVpfr, tmpVpto) / 1400
                If tmpDiff > maxDiff Then
                    tmpVpto = DateAdd("d", maxDiff, tmpVpfr)
                    tmpDate = Format(tmpVpto, MySetUp.DefDateFormat)
                    tmpMsg = ""
                    tmpMsg = tmpMsg & "Your input: " & txtDateFrom.Text & " - " & txtDateTo.Text & " (" & CStr(tmpDiff) & " Days)" & vbNewLine
                    tmpMsg = tmpMsg & "Limitation: " & txtDateFrom.Text & " - " & tmpDate & " (" & CStr(maxDiff) & " Days)" & vbNewLine
                    tmpMsg = tmpMsg & "Do you want to reduce the timeframe?"
                    retval = MyMsgBox.CallAskUser(0, 0, 0, "Time Filter Confirmation", tmpMsg, "ask", "Yes,No", UserAnswer)
                    If retval = 1 Then
                        txtDateTo.Text = tmpDate
                        'txtTimeTo.Text = Format(tmpVpto, "hh:mm")
                        tmpChkTag = "OK"
                    ElseIf retval = 2 Then
                        tmpChkTag = "ACK"
                    End If
                End If
                txtDateFrom.BackColor = vbWhite
                txtDateTo.BackColor = vbWhite
            End If
        End If
        chkFromTo.Tag = tmpChkTag
        chkFromTo.Value = 0
    Else
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
    End If
End Sub

Private Sub chkFsfClose_Click()
    If chkFsfClose.Value = 1 Then
        chkFltList.Value = 0
        fraFlightStatus.Visible = False
        chkFsfClose.Value = 0
    End If
End Sub

Private Sub chkHelp_Click()
    If chkHelp.Value = 1 Then
        ShowHtmlHelp Me, "", ""
        'DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
        chkHelp.Value = 0
    End If
End Sub

Private Sub chkImbis_Click()
    If UfisImbisIsActive Then
        If chkImbis.Value = 1 Then UfisImbis.Show
    End If
    chkImbis.Value = 0
End Sub

Private Sub chkLoad_Click()
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = vbGreen
        chkLoad.ForeColor = vbBlack
        chkLoad.Enabled = False
        HandleChkLoad "LOAD"
    Else
        chkLoad.BackColor = vbButtonFace
        chkLoad.ForeColor = vbBlack
    End If
End Sub
Private Sub HandleChkLoad(ForWhat As String)
    Dim tmpCheck As String
    Dim MemIdx As Integer
    Dim AllBtnUp As Boolean
    Dim i As Integer

    MemIdx = CurMem
    If chkLoad.Value = 1 Then
        LastHitTlxUrno = "NONE"
        txtLoadFrom.Text = txtDateFrom.Text
        txtLoadTo.Text = txtDateTo.Text
        txtLoadTimeFrom.Text = txtTimeFrom.Text
        txtLoadTimeTo.Text = txtTimeTo.Text
        fraLoadProgress.Visible = True
        fraLoadProgress.ZOrder
        fraLoadProgress.Refresh
        If Not MainIsOnlineNow Then
            AllBtnUp = True
            For i = 0 To optMem.UBound
                If optMem(i).Value = True Then
                    AllBtnUp = False
                End If
            Next
            If AllBtnUp Then optMem(0).Value = True
        End If
        MemIdx = CurMem
        If chkFlightList(6).Value = 1 Then
            chkFlightList(6).Value = 0
        End If
        StopLoading = False
        chkStop.Top = chkOnlineRcv.Top
        chkStop.Left = chkOnlineRcv.Left
        chkStop.Value = 0
        If MemIdx = 8 Then
            BcSpooler.BcSpoolerTab.ResetContent
            RefreshOnlineWindows "REF"
            If Not StopLoading Then
                chkRcvCnt(2).Caption = CStr(tabTelexList(2).GetLineCount)
                chkRcvCnt(3).Caption = CStr(tabTelexList(2).GetLineCount)
                chkSntCnt(2).Caption = CStr(tabTelexList(3).GetLineCount)
                chkSntCnt(3).Caption = CStr(tabTelexList(3).GetLineCount)
                If AreaPanel(3).Visible Then
                    'DEBUGGED: THIS IS ALREADY
                    'HANDLED IN RefreshOnlineWindows
                    'chkFlightList(0).Value = 1
                End If
            End If
        Else
            If Left(NewFilterValues(MemIdx).TlxKey, 11) = "WHERE URNO=" Then
                LoadTelexData MemIdx, "", NewFilterValues(MemIdx).TlxKey, NewFilterValues(MemIdx).TfrKey, "ALL", ""
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            Else
                tmpCheck = CheckValidFilterFields(True)
                If tmpCheck = "OK" Then
                    txtLoadFrom.Text = txtDateFrom.Text
                    txtLoadTo.Text = txtDateTo.Text
                    txtLoadTimeFrom.Text = txtTimeFrom.Text
                    txtLoadTimeTo.Text = txtTimeTo.Text
                    fraLoadProgress.Caption = "0 Telexes Loaded"
                    fraLoadProgress.Visible = True
                    fraLoadProgress.Refresh
                    tmpCheck = txtFlca.Tag
                    If Len(tmpCheck) > 3 Then txtFlca.Tag = AskForUniqueAlc(tmpCheck, txtFlca.Text)
                    chkStop.Visible = True
                    LoadTelexData MemIdx, "", NewFilterValues(MemIdx).TlxKey, NewFilterValues(MemIdx).TfrKey, "ALL", ""
                    tabTelexList(0).Refresh
                    tabTelexList(1).Refresh
                    Me.Refresh
                    If Not StopLoading Then
                        If AreaPanel(3).Visible Then
                            chkFlightList(0).Value = 1
                            If tabTelexList(0).GetLineCount > 0 Then
                                tabTelexList(0).SetCurrentSelection 0
                            Else
                                tabTelexList(1).SetCurrentSelection 0
                            End If
                        End If
                    End If
                    fraLoadProgress.Visible = False
                    fraLoadProgress.Refresh
                Else
                    If chkFromTo.Tag <> "CHK" Then
                        tmpCheck = "Wrong Filter Values!" & vbNewLine & tmpCheck
                    End If
                    MyMsgBox.CallAskUser 0, 0, 0, "Load Telexes", tmpCheck, "stop", "", UserAnswer
                End If
                CheckLoadButton False
            End If
        End If
        chkStop.Value = 0
        chkStop.Visible = False
        StopLoading = False
        chkLoad.Value = 0
        chkLoad.Enabled = True
        If MemIdx <> 8 Then
            CheckLoadButton False
        End If
        chkToggleRcvd(1).Value = 1
        'If tabTelexList(2).GetLineCount > 0 Then
        '    tabTelexList(2).SetCurrentSelection 0
        'Else
            'tabTelexList(3).SetCurrentSelection 0
        'End If
    Else
        chkLoad.BackColor = vbButtonFace
        chkLoad.ForeColor = vbBlack
    End If
    fraLoadProgress.Visible = False
    fraLoadProgress.Refresh
End Sub

Public Function AskForUniqueAlc(AlcList As String, ConText As String) As String
    Dim UseAlc As String
    Dim CurAlcd As String
    Dim AlfnList As String
    Dim CurAlfn As String
    Dim ReqList As String
    Dim AlcdItemNo As Long
    Dim AlfnItemNo As Long
    Dim AskText As String
    ReqList = ""
    AlcdItemNo = 0
    CurAlcd = GetRealItem(AlcList, AlcdItemNo, vbLf)
    While CurAlcd <> ""
        AlfnList = UfisServer.BasicLookUp(1, 1, 2, CurAlcd, 0, False)
        AlfnItemNo = 0
        CurAlfn = GetRealItem(AlfnList, AlfnItemNo, vbLf)
        While CurAlfn <> ""
            ReqList = ReqList & CurAlcd & "," & CurAlfn & vbLf
            AlfnItemNo = AlfnItemNo + 1
            CurAlfn = GetRealItem(AlfnList, AlfnItemNo, vbLf)
        Wend
        AlcdItemNo = AlcdItemNo + 1
        CurAlcd = GetRealItem(AlcList, AlcdItemNo, vbLf)
    Wend
    AskText = "{=LABEL=}Please select the desired airline"
    AskText = AskText & "{=DATA=}" & ReqList
    AskText = AskText & "{=HEAD=}3LC,Full Name"
    MyMsgBox.CallAskUser 1, 0, 0, "Basic Data Check for '" & ConText & "'", AskText, "hand", "", UserAnswer
    AskForUniqueAlc = UserAnswer
End Function
Private Function CheckValidFilterFields(FullFilter As Boolean) As String
    Dim tmpResult As String
    Dim tmpFlca As String
    Dim tmpAlc3 As String
    Dim tmpMaxTag As String
    Dim tmpChkTag As String
    Dim maxDiff As Long
    Dim tmpDiff As Long
    Dim tmpVpfr
    Dim tmpVpto
    tmpResult = ""
    If txtDateFrom.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Date From: " & txtDateFrom.Text
    If txtDateTo.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Date To: " & txtDateTo.Text
    If txtTimeFrom.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Time From: " & txtTimeFrom.Text
    If txtTimeTo.Tag = "" Then tmpResult = tmpResult & vbNewLine & "Time To: " & txtTimeTo.Text
    If FullFilter Then
        tmpFlca = Trim(txtFlca.Text)
        tmpAlc3 = Trim(txtFlca.Tag)
        If (tmpFlca <> "") And (tmpAlc3 = "") Then
            tmpResult = tmpResult & vbNewLine & "Airline Code"
        End If
    End If
    If tmpResult = "" Then
        tmpResult = "OK"
    End If
    If tmpResult = "OK" Then
        tmpMaxTag = fraTimeFilter.Tag
        tmpChkTag = chkFromTo.Tag
        If tmpChkTag = "CHK" Then tmpChkTag = ""
        If (tmpMaxTag <> "") And (tmpChkTag = "") Then
            maxDiff = Val(GetItem(tmpMaxTag, 1, ","))
            tmpVpfr = CedaFullDateToVb(txtDateFrom.Tag & txtTimeFrom.Tag)
            tmpVpto = CedaFullDateToVb(txtDateTo.Tag & txtTimeTo.Tag)
            tmpDiff = DateDiff("n", tmpVpfr, tmpVpto) / 1400
            If tmpDiff > maxDiff Then
                txtDateFrom.BackColor = vbYellow
                txtDateTo.BackColor = vbYellow
                chkFromTo.BackColor = vbRed
                chkFromTo.ForeColor = vbWhite
                tmpResult = vbNewLine & "The actual timeframe of " & CStr(tmpDiff) & " days"
                tmpResult = tmpResult & vbNewLine & "exceeds the limit of " & CStr(maxDiff) & " days."
                tmpResult = tmpResult & vbNewLine & "Please confirm your input."
                tmpChkTag = "CHK"
            End If
        End If
        chkFromTo.Tag = tmpChkTag
    End If
    If tmpResult <> "OK" Then tmpResult = Mid(tmpResult, 3)
    CheckValidFilterFields = tmpResult
End Function

Public Sub CheckLoadButton(AskBack As Boolean)
    If ApplicationIsStarted Then
        If TlxSqlKeyChanged(CurMem) Then
            If Not ReorgFolders Then
                chkLoad.BackColor = vbRed
                chkLoad.ForeColor = vbWhite
                chkReset.Enabled = True
            Else
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            End If
        Else
            If CurMem = 8 Then
                chkLoad.BackColor = vbButtonFace
                chkLoad.ForeColor = vbBlack
            Else
                If optMem(CurMem).Tag > 0 Then
                    chkLoad.BackColor = vbButtonFace
                    chkLoad.ForeColor = vbBlack
                Else
                    chkLoad.BackColor = DarkGreen
                    chkLoad.ForeColor = vbWhite
                End If
            End If
            chkReset.Enabled = False
        End If
    End If
End Sub

Private Function TlxSqlKeyChanged(MemIdx As Integer) As Boolean
Dim clSqlKey As String
Dim clTimeFilter As String
Dim clFlightFilter As String
Dim clTtypFilter As String
Dim clTextFilter As String
Dim clSereFilter As String
Dim clRcvStatFilter As String
Dim clSndStatFilter As String
Dim clOrderKey As String
Dim tmpData As String
Dim tmpRegn As String
Dim clDateFrom As String
Dim clDateTo As String
Dim clTimeFrom As String
Dim clTimeTo As String
Dim tmpLoopTimes As String
Dim i As Integer
    If ApplicationIsStarted Then
        clSqlKey = ""
        'Building the TimeFilter
        GetValidTimeFrame clDateFrom, clDateTo, True
        If optLookTlx.Value = True Then
            clTimeFilter = "TIME BETWEEN '[LOOPVPFR]00' AND '[LOOPVPTO]59'"
            clSqlKey = "(" & clTimeFilter & ")"
        End If
        tmpLoopTimes = clDateFrom & "," & clDateTo
        
        clFlightFilter = BuildFlightFilter("TLXTAB", clDateFrom, clDateTo, txtFlca.Tag, txtFltn.Tag, txtFlns.Text, txtREGN.Text)
        If clFlightFilter <> "" Then
            If clSqlKey <> "" Then clSqlKey = clSqlKey & " AND "
            clSqlKey = clSqlKey & "(" & clFlightFilter & ")"
        End If
        
        'Building the TTYP-Filter
        clTtypFilter = BuildTtypFilter
        If clTtypFilter <> "" Then
            clSqlKey = clSqlKey & " AND (" & clTtypFilter & ")"
        End If
        
        'Building the TextFilter
        clTextFilter = ""
        If optLookTlx.Value = True Then
            'REGN
            tmpData = Trim(txtREGN)
            If tmpData <> "" Then
                tmpRegn = Replace(tmpData, "-", "", 1, -1, vbBinaryCompare)
                If tmpRegn <> "" Then
                    If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
                    If tmpData <> tmpRegn Then
                        clTextFilter = clTextFilter & "((TXT1 LIKE '%" & tmpRegn & "%' OR TXT1 LIKE '%" & tmpData & "%') OR "
                        clTextFilter = clTextFilter & "(TXT2 LIKE '%" & tmpRegn & "%' OR TXT2 LIKE '%" & tmpData & "%'))"
                    Else
                        clTextFilter = clTextFilter & "(TXT1 LIKE '%" & txtREGN & "%' OR TXT2 LIKE '%" & txtREGN & "%')"
                    End If
                End If
            End If
        End If
        If Trim(txtAddr) <> "" Then
            If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
            tmpData = txtAddr.Text
            tmpData = Replace(tmpData, ".", "", 1, -1, vbBinaryCompare)
            clTextFilter = clTextFilter & "(TXT1 LIKE '%" & tmpData & "%' OR TXT2 LIKE '%" & tmpData & "%')"
        End If
        If Trim(txtText) <> "" Then
            If clTextFilter <> "" Then clTextFilter = clTextFilter & " AND "
            clTextFilter = clTextFilter & "(TXT1 LIKE '%" & txtText & "%' OR TXT2 LIKE '%" & txtText & "%')"
        End If
        If clTextFilter <> "" Then clSqlKey = clSqlKey & " AND " & clTextFilter
        
        'Building the SERE/STAT-Filter
        clSereFilter = ""
        If chkFilter(2).Value = 0 Then
            If chkFilter(0).Value = chkFilter(1).Value Then
                clSereFilter = "SERE IN ('R','I','S')"
            ElseIf chkFilter(0).Value = 1 Then
                clSereFilter = "SERE IN ('R','I')"
            Else
                clSereFilter = "SERE IN ('S')"
            End If
        Else
            clRcvStatFilter = ""
            clSndStatFilter = ""
            If chkFilter(2).Value = 1 Then
                clRcvStatFilter = TlxRcvStat.GetStatusFilter
                clSndStatFilter = TlxSndStat.GetStatusFilter
            End If
            If chkFilter(0).Value = chkFilter(1).Value Then
                If (clRcvStatFilter = "") And (clSndStatFilter = "") Then
                    clSereFilter = "SERE IN ('R','I','S')"
                ElseIf (clRcvStatFilter <> "") And (clSndStatFilter <> "") Then
                    clSereFilter = "(SERE IN ('R','I') AND " & clRcvStatFilter & ") OR (SERE IN ('S') AND " & clSndStatFilter & ")"
                ElseIf clRcvStatFilter <> "" Then
                    clSereFilter = "(SERE IN ('R','I') AND " & clRcvStatFilter & ") OR SERE IN ('S')"
                Else
                    clSereFilter = "SERE IN ('R','I') OR (SERE IN ('S') AND " & clSndStatFilter & ")"
                End If
            ElseIf chkFilter(0).Value = 1 Then
                If clRcvStatFilter = "" Then
                    clSereFilter = "SERE IN ('R','I')"
                Else
                    clSereFilter = "SERE IN ('R','I') AND " & clRcvStatFilter
                End If
            Else
                If clSndStatFilter = "" Then
                    clSereFilter = "SERE IN ('S')"
                Else
                    clSereFilter = "SERE IN ('S') AND " & clSndStatFilter
                End If
            End If
        End If
        If clSereFilter <> "" Then clSqlKey = clSqlKey & " AND (" & clSereFilter & ")"
        clSqlKey = clSqlKey & "[LOOPTIMES]" & tmpLoopTimes
        NewFilterValues(MemIdx).TlxKey = "WHERE " & clSqlKey
        If optLookAft.Value = True Then GetValidTimeFrame clDateFrom, clDateTo, True
        NewFilterValues(MemIdx).TfrKey = BuildFlightFilter("TFRTAB", clDateFrom, clDateTo, txtFlca.Tag, txtFltn.Tag, txtFlns.Text, txtREGN.Text)
        If NewFilterValues(MemIdx).TlxKey <> ActFilterValues(MemIdx).TlxKey Then TlxSqlKeyChanged = True Else TlxSqlKeyChanged = False
        NewFilterValues(MemIdx).TlxAddr = optTlxAddr.Value
        NewFilterValues(MemIdx).TlxText = optTlxText.Value
        If NewFilterValues(MemIdx).TlxAddr <> ActFilterValues(MemIdx).TlxAddr Then TlxSqlKeyChanged = True
    End If
End Function

Private Function BuildTtypFilter() As String
    Dim clTtypFilter As String
    Dim ActTypes As String
    Dim NotTypes As String
    Dim CurType As String
    Dim ShowElseTypes As Boolean
    Dim i As Integer
    ActTypes = ""
    NotTypes = ""
    clTtypFilter = ""
    If InStr(MainNoShow, "ELSE") > 0 Then
        ShowElseTypes = False
        chkTabs(OtherTypesCol).Enabled = False
        chkFolder(OtherTypesCol).Enabled = False
    Else
        ShowElseTypes = True
        chkTabs(OtherTypesCol).Enabled = True
        chkFolder(OtherTypesCol).Enabled = True
    End If
    If chkTabs(AllTypesCol).Value = 0 Then
        'Individual Types selected
        For i = 0 To LastFolderIndex
            CurType = fraFolder(i).Tag
            CurType = Replace(CurType, ",", "','", 1, -1, vbBinaryCompare)
            If chkTabs(i).Value = 1 Then
                ActTypes = ActTypes & ",'" & CurType & "'"
            Else
                If ShowElseTypes Then NotTypes = NotTypes & ",'" & CurType & "'"
            End If
        Next
    Else
        'All Types selected
        If Not ShowElseTypes Then
            For i = 0 To LastFolderIndex
                CurType = fraFolder(i).Tag
                CurType = Replace(CurType, ",", "','", 1, -1, vbBinaryCompare)
                ActTypes = ActTypes & ",'" & CurType & "'"
            Next
        End If
    End If
    If ShowElseTypes Then
        i = 1
        CurType = GetItem(MainNoShow, i, ",")
        While CurType <> ""
            If InStr(NotTypes, CurType) = 0 Then NotTypes = NotTypes & ",'" & CurType & "'"
            i = i + 1
            CurType = GetItem(MainNoShow, i, ",")
        Wend
    End If
    ActTypes = Mid(ActTypes, 2)
    NotTypes = Mid(NotTypes, 2)
    ActTypes = Replace(ActTypes, "FREE", " ", 1, -1, vbBinaryCompare)
    NotTypes = Replace(NotTypes, "FREE", " ", 1, -1, vbBinaryCompare)
    If ActTypes = "" Then ActTypes = "'--.--'"
    If NotTypes = "" Then NotTypes = "'--.--'"
    If (InStr(ActTypes, "MVT") > 0) And (InStr(ActTypes, "COR") = 0) Then ActTypes = ActTypes & ",'COR'"
    If chkTabs(AllTypesCol).Value = 1 Then
        'All Types wanted
        If ShowElseTypes Then
            'Include Else types wanted
            'Excluding NoShow types
            clTtypFilter = "TTYP NOT IN (" & NotTypes & ")"
        Else
            clTtypFilter = "TTYP IN (" & ActTypes & ")"
        End If
    Else
        'Individual Types selected
        If (chkTabs(OtherTypesCol).Value = 0) Or (Not ShowElseTypes) Then
            'No Else types wanted
            clTtypFilter = "TTYP IN (" & ActTypes & ")"
        Else
            'Include Else types wanted
            'Exclude NoShow types
            clTtypFilter = "(TTYP IN (" & ActTypes & ") OR TTYP NOT IN (" & NotTypes & "))"
        End If
    End If
    BuildTtypFilter = clTtypFilter
End Function

Private Function BuildFlightFilter(ForWhat As String, DateFrom As String, DateTo As String, _
                AftAlc3 As String, AftFltn As String, AftFlns As String, AftRegn As String)
Dim tlxFlightFilter As String
Dim tfrFlightFilter As String
Dim tmpTfrFilter As String
Dim tmpAftFilter As String
Dim tmpStoaFilter As String
Dim tmpStodFilter As String
Dim Result As String
'Dim tmpDateFrom As String
'Dim tmpDateTo As String
Dim tmpAlc3 As String
Dim tmpFltn As String
Dim tmpFlns As String
Dim tmpRegn As String
Dim tmpData As String
Dim tmpDateFrom
    Result = ""
    tmpFltn = Trim(AftFltn)
    tmpAlc3 = Trim(AftAlc3)
    tmpFlns = Trim(AftFlns)
    tmpRegn = Trim(AftRegn)
    tmpRegn = Replace(tmpRegn, "-", "", 1, -1, vbBinaryCompare)
    If ForWhat = "TLXTAB" Then
        If optLookTlx.Value = True Then
            tlxFlightFilter = AppendToFilter("", "AND", "FLTN", tmpFltn)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "ALC3", tmpAlc3)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "FLNS", tmpFlns)
        Else
            tmpStoaFilter = LoadFlightData(0, False)
            tmpStodFilter = LoadFlightData(1, False)
            tlxFlightFilter = ""
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "FLTN", tmpFltn)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "ALC3", tmpAlc3)
            tlxFlightFilter = AppendToFilter(tlxFlightFilter, "AND", "FLNS", tmpFlns)
            If tlxFlightFilter <> "" Then
                tmpStoaFilter = tmpStoaFilter & " AND " & tlxFlightFilter
                tmpStodFilter = tmpStodFilter & " AND " & tlxFlightFilter
            End If
            If tmpRegn <> "" Then
                If Len(tmpRegn) > 4 Then
                    tmpStoaFilter = tmpStoaFilter & " AND REGN='" & tmpRegn & "'"
                    tmpStodFilter = tmpStodFilter & " AND REGN='" & tmpRegn & "'"
                Else
                    tmpStoaFilter = tmpStoaFilter & " AND REGN LIKE '" & tmpRegn & "%'"
                    tmpStodFilter = tmpStodFilter & " AND REGN LIKE '" & tmpRegn & "%'"
                End If
            End If
            tmpAftFilter = "FLNU IN (SELECT URNO FROM AFTTAB WHERE ("
            tmpAftFilter = tmpAftFilter & "(" & tmpStoaFilter & ") OR "
            tmpAftFilter = tmpAftFilter & "(" & tmpStodFilter & ")))"
            tlxFlightFilter = tmpAftFilter
        End If
        Result = tlxFlightFilter
    ElseIf ForWhat = "TFRTAB" Then
        NewFilterValues(CurMem).TfrKey = ""
        'tmpDateFrom = Trim(Left(DateFrom, 8))
        'tmpDateTo = Trim(Left(DateTo, 8))
        tmpData = tmpAlc3 & tmpFltn & tmpFlns & tmpRegn
        If tmpData <> "" Then
            tfrFlightFilter = "(VATO>='" & DateFrom & "' AND VAFR<='" & DateTo & "')"
            tmpTfrFilter = ""
            If tmpFltn <> "" Then
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLTN", tmpFltn)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", tmpAlc3)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", tmpFlns)
                If tmpTfrFilter <> "" Then tfrFlightFilter = tfrFlightFilter & " AND ((" & tmpTfrFilter & ")"
            Else
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "ALC3", tmpAlc3)
                tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "FLNS", tmpFlns)
                If tmpTfrFilter <> "" Then tfrFlightFilter = tfrFlightFilter & " AND ((" & tmpTfrFilter & ")"
            End If
            If tmpRegn <> "" Then
                If tmpTfrFilter <> "" Then
                    tfrFlightFilter = tfrFlightFilter & " OR "
                Else
                    tfrFlightFilter = tfrFlightFilter & " AND ("
                End If
                tmpTfrFilter = ""
                If Len(tmpRegn) > 4 Then
                    tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "REGN", tmpRegn)
                Else
                    tmpTfrFilter = AppendToFilter(tmpTfrFilter, "AND", "REGN", tmpRegn & "%")
                End If
                tfrFlightFilter = tfrFlightFilter & "(" & tmpTfrFilter & ")"
            End If
            tfrFlightFilter = tfrFlightFilter & "))"
            Result = "WHERE URNO IN (SELECT DISTINCT(TURN) FROM " & "TFRTAB" & " WHERE (" & tfrFlightFilter & ")"
        End If
    End If
    BuildFlightFilter = Result
End Function
Private Function AppendToFilter(CurFilter As String, AppOperator As String, _
                                FieldName As String, FieldValue As String)
Dim Result As String
    Result = CurFilter
    If FieldValue <> "" Then
        If Result <> "" Then Result = Result & " " & AppOperator & " "
        If InStr(FieldValue, "%") = 0 Then
            Result = Result & FieldName & "='" & FieldValue & "'"
        Else
            Result = Result & FieldName & " LIKE '" & FieldValue & "'"
        End If
    End If
    AppendToFilter = Result
End Function

Private Sub chkLoadAss_Click()
    Dim tmpMax As Long
    Dim i As Integer
    Dim tmpFolderName As String
    Dim LastFolderName As String
    Dim SqlKey As String
    
    If chkLoadAss.Value = 1 Then
        chkLoadAss.BackColor = LightGreen
        chkLoadAss.Refresh
        CedaStatus(0).Visible = True
        CedaStatus(0).ZOrder
        CedaStatus(0).Refresh
        tabAssign(0).ResetContent
        tabAssign(0).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
        tabAssign(0).CedaHopo = UfisServer.HOPO
        tabAssign(0).CedaIdentifier = "IDX"
        tabAssign(0).CedaPort = "3357"
        tabAssign(0).CedaReceiveTimeout = "250"
        tabAssign(0).CedaRecordSeparator = vbLf
        tabAssign(0).CedaSendTimeout = "250"
        tabAssign(0).CedaServerName = UfisServer.HostName
        tabAssign(0).CedaTabext = UfisServer.TblExt
        tabAssign(0).CedaUser = gsUserName
        tabAssign(0).CedaWorkstation = UfisServer.GetMyWorkStationName
        If CedaIsConnected Then
            If Len(CurrentUrno.Text) = 0 Then
                SqlKey = "WHERE TURN = 0 ORDER BY FNAM"
            Else
                SqlKey = "WHERE TURN = 0 OR TURN = " & CurrentUrno.Text & " ORDER BY FNAM,DATC"
            End If
            tabAssign(0).CedaAction "RTA", "TFNTAB", "FNAM,FDAT,FREM,URNO,TURN,FTYP", " ", SqlKey
        End If
        tabAssign(0).InsertTextLine " , , , , , ", False
        tabAssign(0).AutoSizeColumns
        tabAssign(0).RedrawTab
        AddFolderList = ""
        LastFolderName = ""
        tmpMax = tabAssign(0).GetLineCount - 2
        For i = 0 To tmpMax
            tmpFolderName = tabAssign(0).GetColumnValue(i, 0)
            If tmpFolderName <> LastFolderName Then
                If Len(AddFolderList) = 0 Then
                    AddFolderList = tmpFolderName
                Else
                    If InStr(AddFolderList, tmpFolderName) <= 0 Then
                        AddFolderList = AddFolderList & "," & tmpFolderName
                    End If
                End If
                LastFolderName = tmpFolderName
            Else
                tabAssign(0).SetColumnValue i, 0, " "
            End If
        Next
        chkLoadAss.Value = 0
        chkTlxAssign.BackColor = vbButtonFace
        chkTlxAssign.ForeColor = vbBlack
        CedaStatus(0).Visible = False
        CedaStatus(0).Refresh
    Else
        chkLoadAss.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLoader_Click(Index As Integer)
    Dim AllBtnUp As Boolean
    Dim i As Integer
    If chkLoader(Index).Value = 1 Then
        Select Case Index
            Case 0
                chkOnlineRcv.Value = 0
                chkLoader(1).Value = 1
                chkLoader(0).BackColor = LightGreen
                chkLoader(0).Height = 360
                chkLoader(0).ZOrder
                chkLoader(0).Refresh
                chkLoader(1).BackColor = LightGreen
                chkLoader(1).Height = 360
                chkLoader(1).ZOrder
                chkLoader(1).Refresh
                If Not MainIsOnlineNow Then
                    AllBtnUp = True
                    For i = 0 To optMem.UBound
                        If optMem(i).Value = True Then
                            AllBtnUp = False
                        End If
                    Next
                    If AllBtnUp Then optMem(0).Value = True
                End If
                AdjustTopStatusToggle "LOADER"
            Case 1
                chkLoader(0).Value = 1
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                chkLoader(1).Value = 0
                chkLoader(0).BackColor = MyOwnButtonFace
                chkLoader(0).Height = 315
                chkLoader(1).BackColor = MyOwnButtonFace
                chkLoader(1).Height = 315
                AdjustTopStatusToggle "LOADER"
                If chkOnlineRcv.Enabled Then chkOnlineRcv.Value = 1
            Case 1
                chkLoader(0).Value = 0
            Case Else
        End Select
    End If
    chkLoader(0).Refresh
    chkLoader(1).Refresh
End Sub
Private Sub AdjustTopStatusToggle(ForWhat As String)
    TopStatus(0).Value = 0
    Select Case ForWhat
        Case "LOADER"
            If chkLoader(0).Value = 1 Then
                If chkOnlineRcv.Value = 1 Then chkOnlineRcv.Height = 315
                TopStatus(0).Tag = "LOADER"
                TopStatus(0).Value = 1
            Else
                If chkOnlineRcv.Value = 1 Then
                    chkOnlineRcv.Height = 360
                    TopStatus(0).Tag = "ONLINE"
                    TopStatus(0).Value = 1
                End If
            End If
        Case "ONLINE"
            If chkOnlineRcv.Value = 1 Then
                If chkLoader(0).Value = 1 Then chkLoader(0).Height = 315
                TopStatus(0).Tag = "ONLINE"
                TopStatus(0).Value = 1
            Else
                If chkLoader(0).Value = 1 Then
                    chkLoader(0).Height = 360
                    TopStatus(0).Tag = "LOADER"
                    TopStatus(0).Value = 1
                End If
            End If
        Case Else
    End Select
End Sub

Private Sub chkOnlineRefresh_Click(Index As Integer)
    If chkOnlineRefresh(Index).Value = 1 Then
        chkOnlineRefresh(Index).BackColor = LightGreen
        chkOnlineRefresh(Index).Refresh
        Select Case Index
            Case 2
                SwapTimeFilterPanel "ONLINE_RCVD"
            Case 3
                SwapTimeFilterPanel "ONLINE_SENT"
            Case Else
        End Select
    Else
        chkOnlineRefresh(Index).BackColor = vbButtonFace
        chkOnlineRefresh(Index).Refresh
        Select Case Index
            Case 2
                SwapTimeFilterPanel "RESTORE"
            Case 3
                SwapTimeFilterPanel "RESTORE"
            Case Else
        End Select
    End If
End Sub
Private Sub SwapTimeFilterPanel(ForWhat As String)
    Dim NewLeft As Long
    Dim NewTop As Long
    Select Case ForWhat
        Case "ONLINE_RCVD"
            fraLoadProgress.Top = fraFlightFilter.Top
            fraLoadProgress.Left = 30
            fraLoadProgress.Visible = True
            fraLoadProgress.ZOrder
            fraTimeFilter.Visible = False
            NewTop = fraListPanel.Top + AreaPanel(0).Top + ListPanel(0).Top + ListButtons(2).Top + ListButtons(2).Height
            NewTop = NewTop + 30
            NewLeft = fraListPanel.Left + AreaPanel(0).Left + ListPanel(0).Left + ListButtons(2).Left + chkOnlineRefresh(2).Left
            NewLeft = NewLeft + 30
            fraTimeFilter.Left = NewLeft
            fraTimeFilter.Top = NewTop
            fraTimeFilter.Width = Frame2.Left + Frame2.Width + 90
            fraTimeFilter.Visible = True
            fraTimeFilter.ZOrder
            chkTask(0).Left = chkLock(0).Left
            chkTask(0).Top = chkLock(0).Top
            chkTask(0).Visible = True
            chkTask(0).ZOrder
        Case "ONLINE_SENT"
            fraLoadProgress.Top = fraFlightFilter.Top
            fraLoadProgress.Left = 30
            fraLoadProgress.Visible = True
            fraLoadProgress.ZOrder
            fraTimeFilter.Visible = False
            NewTop = fraListPanel.Top + AreaPanel(1).Top + ListPanel(1).Top + ListButtons(3).Top + ListButtons(3).Height
            NewTop = NewTop + 30
            NewLeft = fraListPanel.Left + AreaPanel(1).Left + ListPanel(1).Left + ListButtons(3).Left + chkOnlineRefresh(3).Left
            NewLeft = NewLeft + 30
            fraTimeFilter.Left = NewLeft
            fraTimeFilter.Top = NewTop
            fraTimeFilter.Visible = True
            fraTimeFilter.ZOrder
            chkTask(1).Left = chkLock(1).Left
            chkTask(1).Top = chkLock(1).Top
            chkTask(1).Visible = True
            chkTask(1).ZOrder
        Case "RESTORE"
            fraLoadProgress.ZOrder
            fraTimeFilter.Top = fraFlightFilter.Top
            fraTimeFilter.Left = 30
            fraTimeFilter.Width = optUtc.Left + optUtc.Width + 75
            fraLoadProgress.Visible = False
            fraTimeFilter.Visible = True
            fraTimeFilter.ZOrder
            chkTask(0).Visible = False
            chkTask(1).Visible = False
        Case Else
    End Select
    fraLoadProgress.Caption = "0 Telexes Loaded"
End Sub

Private Sub chkPanic_Click()
    If chkPanic.Value = 1 Then
        PanicTools.Show
        PanicTools.Refresh
        If chkPanic.BackColor = vbRed Then
            PanicTools.chkTool(4).Value = 1
        End If
        If chkPanic.BackColor = LightYellow Then
            PanicTools.chkTool(4).Value = 1
        End If
    End If
    chkPanic.Value = 0
End Sub

Private Sub chkRcvCnt_Click(Index As Integer)
    If chkRcvCnt(Index).Value = 1 Then
        chkRcvCnt(Index).Value = 0
    End If
End Sub

Private Sub chkShowTlx_Click(Index As Integer)
    Static IAmBusy As Boolean
    Dim i As Integer
    If Not IAmBusy Then
        IAmBusy = True
        For i = 0 To chkShowTlx.UBound
            chkShowTlx(i).Value = chkShowTlx(Index).Value
        Next
        If chkShowTlx(Index).Value = 1 Then
            frmTelex.Show , Me
            frmTelex.txtTelex.Text = txtTelexText.Text
        Else
            frmTelex.Hide
        End If
        IAmBusy = False
    End If
End Sub

Private Sub chkSntCnt_Click(Index As Integer)
    If chkSntCnt(Index).Value = 1 Then
        chkSntCnt(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    If chkTask(Index).Value = 1 Then
        chkTask(Index).BackColor = MyOwnButtonDown
        chkOnlineRcv.Value = 1
        chkLoad.Value = 1
        chkTask(Index).Value = 0
    Else
        chkTask(Index).BackColor = MyOwnButtonFace
        chkOnlineRefresh(2).Value = 0
        chkOnlineRefresh(3).Value = 0
    End If
End Sub

Private Sub chkTitleBar_Click(Index As Integer)
    If chkTitleBar(Index).Value = 1 Then
        chkTitleBar(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkTitleMax_Click(Index As Integer)
    Dim tmpTag As String
    Dim NewTag As String
    Dim i As Integer
    If chkTitleMax(Index).Value = 1 Then
        For i = 0 To chkTitleMax.UBound
            If i <> Index Then
                tmpTag = chkTitleMax(i).Tag
                If tmpTag = "MAX" Then
                    chkTitleMax(i).Caption = " +"
                    NewTag = "MIN"
                    chkTitleMax(i).Tag = NewTag
                    ArrangeMinMaxArea i, NewTag
                End If
            End If
        Next
        tmpTag = chkTitleMax(Index).Tag
        If tmpTag = "" Then tmpTag = "MIN"
        Select Case tmpTag
            Case "MIN"
                chkTitleMax(Index).Caption = " -"
                NewTag = "MAX"
            Case "MAX"
                chkTitleMax(Index).Caption = " +"
                NewTag = "MIN"
            Case Else
        End Select
        chkTitleMax(Index).Tag = NewTag
        ArrangeMinMaxArea Index, NewTag
        chkTitleMax(Index).Value = 0
    End If
End Sub
Private Sub ArrangeMinMaxArea(Index As Integer, ForWhat As String)
    Dim NewLeft As Long
    Dim idx As Integer
    idx = Index
    Select Case ForWhat
        Case "MAX"
            SaveSplitterPosition "SAVE"
            Select Case Index
                Case 0
                    NewLeft = fraSplitter(0).Left
                    fraSplitter(3).Left = NewLeft
                    fraSplitter(4).Left = NewLeft - 30
                    AreaMaximized(idx) = True
                    VSplitterSync = True
                    AreaPanel(idx).ZOrder
                Case 2
                    NewLeft = fraSplitter(3).Left
                    fraSplitter(0).Left = NewLeft
                    fraSplitter(1).Left = NewLeft - 30
                    AreaMaximized(idx) = True
                    VSplitterSync = True
                    AreaPanel(idx).ZOrder
                Case 3
                    NewLeft = fraSplitter(3).Left
                    fraSplitter(0).Left = NewLeft
                    fraSplitter(1).Left = NewLeft - 30
                    AreaMaximized(idx) = True
                    VSplitterSync = True
                    AreaPanel(idx).ZOrder
                Case 4
                    NewLeft = fraSplitter(3).Left
                    fraSplitter(0).Left = NewLeft
                    fraSplitter(1).Left = NewLeft - 30
                    AreaMaximized(idx) = True
                    VSplitterSync = True
                    AreaPanel(idx).ZOrder
                Case Else
            End Select
        Case "MIN"
            SaveSplitterPosition "RESTORE"
            AreaMaximized(idx) = False
            VSplitterSync = False
        Case Else
    End Select
    ResizeTabs
End Sub
Private Sub chkTlxAssign_Click()
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As String
    Dim MaxLine As Integer
    Dim i As Integer
    Dim j As Integer
    Dim tmpFkt As String
    Dim tmpFnam As String
    Dim tmpFdat As String
    Dim tmpFrem As String
    Dim tmpUrno As String
    Dim tmpTurn As String
    Dim ErrorMsg As String
    Dim tmpDat As String
    Dim LineNo As Long
    Dim LastFolder As String
    Dim CurrentFolderList As String
    Dim tmpOldFnam As String
    Dim RecallFolder As Boolean
    Dim DeleteFolder As Boolean
    Dim InsertFolder As Boolean
    Dim TlxList As String
    Dim count As Integer
    Dim idx As Long
    Dim SaveTlxUrno As String
    Dim tmpIdx As Integer
    
    If chkTlxAssign.Value = 1 Then
        RecallFolder = False
        DeleteFolder = False
        InsertFolder = False
        TlxList = ""
        chkTlxAssign.BackColor = LightGreen
        chkTlxAssign.ForeColor = vbBlack
        ErrorMsg = ""
        chkTlxAssign.Value = 0
        CurrentFolderList = AddFolderList
        LineNo = tabAssign(0).GetCurrentSelected
        MaxLine = tabAssign(0).GetLineCount - 1
        For i = 0 To MaxLine
            If i = LineNo Then
                tabAssign(0).SetColumnValue i, 5, "N"
            End If
            tmpFkt = tabAssign(0).GetColumnValue(i, 5)
            If tmpFkt = "N" Then
                If Len(Trim(tabAssign(0).GetColumnValue(i, 0))) > 0 Then
                    LastFolder = Trim(tabAssign(0).GetColumnValue(i, 0))
                Else
                    For j = i - 1 To 0 Step -1
                        If Len(Trim(tabAssign(0).GetColumnValue(j, 0))) > 0 Then
                            LastFolder = Trim(tabAssign(0).GetColumnValue(j, 0))
                            Exit For
                        End If
                    Next
                End If
                tmpFrem = Trim(tabAssign(0).GetColumnValue(i, 2))
                If Len(tmpFrem) = 0 Then tmpFrem = " "
                tmpDat = Trim(tabAssign(0).GetColumnValue(i, 1))
                If Len(tmpDat) > 0 Then
                    If Len(tmpDat) = 2 Then tmpDat = ExpandMonth(tmpDat)
                    If Len(tmpDat) = 7 Then
                        tmpFdat = DecodeSsimDayFormat(tmpDat, "SSIM2", "CEDA")
                    Else
                        tmpFdat = "20" & Mid(tmpDat, 5, 2) & Mid(tmpDat, 3, 2) & Mid(tmpDat, 1, 2)
                        tmpDat = DecodeSsimDayFormat(tmpFdat, "CEDA", "SSIM2")
                    End If
                    If CheckValidDate(tmpFdat) = False Then
                        tabAssign(0).SetCurrentSelection i
                        ErrorMsg = "Invalid Date"
                        MyMsgBox.CallAskUser 0, 0, 0, "Save Telex Folder", ErrorMsg, "hand", "", UserAnswer
                        Exit For
                    End If
                Else
                    tmpFdat = ""
                End If
                tmpFnam = Trim(tabAssign(0).GetColumnValue(i, 0))
                tmpUrno = Trim(tabAssign(0).GetColumnValue(i, 3))
                tmpTurn = Trim(tabAssign(0).GetColumnValue(i, 4))
                If Len(tmpUrno) = 0 Then
                    If Len(tmpFnam) > 0 Then
                        ' Insert new folder
                        If InStr(CurrentFolderList, tmpFnam) <= 0 Then
                            ActResult = ""
                            ActCmd = "IRT"
                            ActTable = "TFNTAB"
                            ActFldLst = "URNO,HOPO,FNAM"
                            ActCondition = ""
                            ActOrder = ""
                            ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                        UfisServer.HOPO & "," & _
                                        tmpFnam
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            CurrentFolderList = CurrentFolderList & "," & tmpFnam
                            If chkAssFolder.Enabled = True And chkAssFolder.Value = 1 Then
                                RecallFolder = True
                            End If
                        End If
                    Else
                        tmpFnam = LastFolder
                    End If
                    If Len(tmpDat) > 0 Then
                        ' Insert folder with date
                        If Len(CurrentUrno.Text) = 0 Then
                            ErrorMsg = "No Telex selected"
                            MyMsgBox.CallAskUser 0, 0, 0, "Save Telex Folder", ErrorMsg, "hand", "", UserAnswer
                            Exit For
                        Else
                            If Len(tmpFnam) > 0 Then
                                ActResult = ""
                                ActCmd = "IRT"
                                ActTable = "TFNTAB"
                                ActFldLst = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
                                ActCondition = ""
                                ActOrder = ""
                                ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                            UfisServer.HOPO & "," & _
                                            tmpFnam & "," & _
                                            tmpDat & "," & _
                                            tmpFrem & "," & _
                                            CurrentUrno.Text & "," & _
                                            tmpFdat
                                retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                InsertFolder = True
                            End If
                        End If
                    End If
                Else
                    If Len(tmpDat) = 0 Then
                        If tmpTurn = "0" Then
                            ActResult = ""
                            ActCmd = "RTA"
                            ActTable = "TFNTAB"
                            ActFldLst = "FNAM"
                            ActCondition = "WHERE URNO = " & tmpUrno
                            ActOrder = ""
                            ActDatLst = ""
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            If Len(tmpFnam) = 0 Then
                                ' Delete complete folder
                                tmpFnam = Trim(ActResult)
                                If MyMsgBox.CallAskUser(0, 0, 0, "Save Telex Folder", "Do you really want to delete complete folder " & tmpFnam & " ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                                    ActResult = ""
                                    ActCmd = "RTA"
                                    ActTable = "TFNTAB"
                                    ActFldLst = "TURN"
                                    ActCondition = "WHERE FNAM = '" & tmpFnam & "'"
                                    ActOrder = ""
                                    ActDatLst = ""
                                    retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                                    TlxList = ""
                                    If retval >= 0 Then
                                        count = UfisServer.DataBuffer(0).GetLineCount - 1
                                        If count >= 0 Then
                                            For idx = 0 To count
                                                TlxList = TlxList & UfisServer.DataBuffer(0).GetLineValues(idx) & ","
                                            Next
                                            TlxList = Left(TlxList, Len(TlxList) - 1)
                                        End If
                                    End If
                                    ActResult = ""
                                    ActCmd = "DRT"
                                    ActTable = "TFNTAB"
                                    ActFldLst = ""
                                    ActCondition = "WHERE FNAM = '" & tmpFnam & "'"
                                    ActOrder = ""
                                    ActDatLst = ""
                                    retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                    If chkAssFolder.Enabled = True And chkAssFolder.Value = 1 Then
                                        RecallFolder = True
                                    End If
                                    DeleteFolder = True
                                End If
                            Else
                                ' Rename folder
                                tmpOldFnam = Trim(ActResult)
                                If tmpFnam <> tmpOldFnam Then
                                    If MyMsgBox.CallAskUser(0, 0, 0, "Save Telex Folder", "Do you really want to change folder name " & tmpOldFnam & " to " & tmpFnam & " ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                                        ActResult = ""
                                        ActCmd = "URT"
                                        ActTable = "TFNTAB"
                                        ActFldLst = "FNAM"
                                        ActCondition = "WHERE FNAM = '" & tmpOldFnam & "'"
                                        ActOrder = ""
                                        ActDatLst = tmpFnam
                                        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                        If chkAssFolder.Enabled = True And chkAssFolder.Value = 1 Then
                                            RecallFolder = True
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            ' Delete folder with date
                            ActResult = ""
                            ActCmd = "DRT"
                            ActTable = "TFNTAB"
                            ActFldLst = ""
                            ActCondition = "WHERE URNO = " & tmpUrno
                            ActOrder = ""
                            ActDatLst = ""
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            TlxList = CurrentUrno.Text
                            DeleteFolder = True
                        End If
                    Else
                        ' Insert folder with date
                        If Len(CurrentUrno.Text) = 0 Then
                            ErrorMsg = "No Telex selected"
                            MyMsgBox.CallAskUser 0, 0, 0, "Save Telex Folder", ErrorMsg, "hand", "", UserAnswer
                            Exit For
                        Else
                            If Len(tmpFnam) = 0 Then tmpFnam = LastFolder
                            If Len(tmpFnam) > 0 Then
                                If Len(tmpTurn) = 0 Or tmpTurn = "0" Then
                                    ActResult = ""
                                    ActCmd = "IRT"
                                    ActTable = "TFNTAB"
                                    ActFldLst = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
                                    ActCondition = ""
                                    ActOrder = ""
                                    ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                                UfisServer.HOPO & "," & _
                                                tmpFnam & "," & _
                                                tmpDat & "," & _
                                                tmpFrem & "," & _
                                                CurrentUrno.Text & "," & _
                                                tmpFdat
                                    retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                Else
                                    ActResult = ""
                                    ActCmd = "URT"
                                    ActTable = "TFNTAB"
                                    ActFldLst = "FDAT,FREM,TURN,DATC"
                                    ActCondition = "WHERE URNO = " & tmpUrno
                                    ActOrder = ""
                                    ActDatLst = tmpDat & "," & _
                                                tmpFrem & "," & _
                                                CurrentUrno.Text & "," & _
                                                tmpFdat
                                    retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                End If
                                InsertFolder = True
                            End If
                        End If
                    End If
                End If
            End If
        Next
        If Len(ErrorMsg) = 0 Then
            If DeleteFolder = True Then
                count = ItemCount(TlxList, ",") - 1
                If count >= 0 Then
                    SaveTlxUrno = CurrentUrno.Text
                    For idx = 0 To count
                        CurrentUrno.Text = GetRealItem(TlxList, idx, ",")
                        If CurrentUrno.Text <> "0" Then
                            ActResult = ""
                            ActCmd = "RTA"
                            ActTable = "TFNTAB"
                            ActFldLst = "TURN"
                            ActCondition = "WHERE TURN = " & CurrentUrno.Text
                            ActOrder = ""
                            ActDatLst = ""
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            If retval < 0 Then
                                ActResult = ""
                                ActCmd = "RTA"
                                ActTable = "TLKTAB"
                                ActFldLst = "TURN"
                                ActCondition = "WHERE TURN = " & CurrentUrno.Text
                                ActOrder = ""
                                ActDatLst = ""
                                retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                If retval < 0 Then
                                    If UpdateWstaOnClick = True Then UpdateTelexStatus "V"
                                End If
                            End If
                        End If
                    Next
                    CurrentUrno.Text = SaveTlxUrno
                End If
            Else
                If InsertFolder = True Then
                    If tabTelexList(2).Visible = True Then
                        tmpIdx = tabTelexList(2).GetCurrentSelected
                        tabTelexList(2).SetColumnValue tmpIdx, 1, "A"
                        tabTelexList(2).Refresh
                    End If
                    If tabTelexList(3).Visible = True Then
                        tmpIdx = tabTelexList(3).GetCurrentSelected
                        tabTelexList(3).SetColumnValue tmpIdx, 1, "A"
                        tabTelexList(3).Refresh
                    End If
                    UpdateTelexStatus "A"
                End If
            End If
            chkLoadAss.Value = 1
            If RecallFolder = True Then
                chkAssFolder.Value = 0
                chkAssFolder.Value = 1
            End If
            SetFolderValues CurMem
            If chkFlightList(7).Enabled = True Then
                chkFlightList(7).Value = 1
            End If
        End If
'        AddToOnlineWindow
    Else
        chkTlxAssign.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLock_Click(Index As Integer)
    Dim LineNo As Integer
    Dim tmpDatTim As String
    Dim tmpDat As String
    Dim tmpTim As String
    Dim TimeVal
    
    If chkLock(Index).Value = 1 Then
        chkLock(Index).BackColor = LightGreen
        tabTelexList(Index + 2).LockScroll True
'        LockBroadcasts = True
    Else
        tabTelexList(Index + 2).LockScroll False
        chkLock(Index).BackColor = vbButtonFace
'        LockBroadcasts = False
'        LineNo = tabTelexList(Index + 2).GetCurrentSelected
'        tmpDatTim = Left(tabTelexList(Index + 2).GetColumnValue(LineNo, 4), 12) & "00"
'        TimeVal = CedaFullDateToVb(tmpDatTim)
'        If optLocal.Value = True Then
'            TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
'        End If
'        txtDateFrom.Text = Format(TimeVal, MySetUp.DefDateFormat)
'        txtTimeFrom.Text = Format(TimeVal, "hh:mm")
'        chkLoad.Value = 1
    End If
End Sub

Private Sub chkNew_Click()
    Dim CurTlxRec As String
    Dim TlxText As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    If chkNew.Value = 1 Then
        ShowChildForm Me, chkNew, EditTelex, ""
        EditTelex.SetTelexDefault EditTelex.TlxPrioTab, MySitaDefPrio
        CurTlxRec = CurrentRecord.Text
        tmpSere = GetFieldValue("SERE", CurTlxRec, DataPool.TlxTabFields.Text)
        If tmpSere = "C" Then
            tmpTtyp = GetFieldValue("TTYP", CurTlxRec, DataPool.TlxTabFields.Text)
            TlxText = BuildTelexText(CurTlxRec, "TEXT")
            EditTelex.BuildAutoSendTelex "S", tmpSere, tmpTtyp, CurTlxRec, TlxText
        End If
        chkNew.Value = 0
    End If
End Sub

Private Sub chkOnlineRcv_Click()
    Dim i As Integer
    If chkOnlineRcv.Value = 1 Then
        chkLoader(0).Value = 0
        chkOnlineRcv.BackColor = LightGreen
        chkOnlineRcv.Height = 360
        chkOnlineRcv.ZOrder
        chkOnlineRcv.Refresh
        AdjustTopStatusToggle "ONLINE"
        
        MainIsOnlineNow = True
        InitDateFields "RCV"
        
        SetTlxTabCaption 0, 2, "(Online)"
        SetTlxTabCaption 1, 3, "(Online)"
        ListGroup(2).ZOrder
        ListGroup(3).ZOrder
        ListGroup(2).Refresh
        ListGroup(3).Refresh
        For i = 0 To optMem.UBound
            optMem(i).Value = False
            optMem(i).BackColor = vbButtonFace
        Next
        CurMem = 8
        fraFlightFilter.Enabled = False
        fraTextFilter.Enabled = False
        fraStatusFilter.Enabled = False
        If MyMainPurpose <> "AUTO_SEND" Then
            RcvTelex.Hide
            OutTelex.Hide
        End If
        If Len(SaveDateFrom) > 0 Then
            txtDateFrom.Text = Left(SaveDateFrom, 10)
            txtTimeFrom.Text = Mid(SaveDateFrom, 11)
            txtDateTo.Text = Left(SaveDateTo, 10)
            txtTimeTo.Text = Mid(SaveDateTo, 11)
        End If
        SetFolderValues (CurMem)
        StoreFolderValues (CurMem)
        ActFilterValues(CurMem) = NewFilterValues(CurMem)
        PrepareTelexExtract CurMem, True
        If AreaPanel(3).Visible Then chkFlightList(0).Value = 1
        If MainIsDeploy = False Then
            If tabTelexList(2).GetLineCount > 0 Then
                tabTelexList(2).SetCurrentSelection 0
            Else
                If tabTelexList(3).GetLineCount > 0 Then
                    tabTelexList(3).SetCurrentSelection 0
                End If
            End If
            'chktitlebar(0).Caption = CStr(tabTelexList(2).GetLineCount) & " (" & tabTelexList(2).GetCurrentSelected & ") " & RcvOnlineCapt
            'chktitlebar(1).Caption = CStr(tabTelexList(3).GetLineCount) & " (" & tabTelexList(3).GetCurrentSelected & ") " & SndOnlineCapt
        End If
        txtFlca.Text = ""
        txtFltn.Text = ""
        txtFlns.Text = ""
        txtREGN.Text = ""
        txtAddr.Text = ""
        txtText.Text = ""
    Else
        chkOnlineRcv.BackColor = vbButtonFace
        chkOnlineRcv.Height = 315
        chkOnlineRcv.Refresh
        AdjustTopStatusToggle "ONLINE"
        MainIsOnlineNow = False
        chkLoader(0).Value = 1
        SetTlxTabCaption 0, 0, "(Loaded)"
        SetTlxTabCaption 1, 1, "(Loaded)"
        ListGroup(0).ZOrder
        ListGroup(1).ZOrder
        ListGroup(0).Refresh
        ListGroup(1).Refresh
        SaveDateFrom = txtDateFrom.Text & txtTimeFrom.Text
        SaveDateTo = txtDateTo.Text & txtTimeTo.Text
        If MainIsDeploy = False Then
            'chkTitleBar(0).Caption = RcvLoadCapt
            'chkTitleBar(1).Caption = SndLoadCapt
        End If
        fraFlightFilter.Enabled = True
        fraTextFilter.Enabled = True
        fraStatusFilter.Enabled = True
        If MyMainPurpose <> "AUTO_SEND" Then
            'RcvTelex.Show
            'OutTelex.Show
        End If
        InitDateFields "USR"
        FixWin.Value = 1
        FixWin.Value = 0
    End If
    If MainIsDeploy = True Then
        'If chkToggleRcvd(1).Value = 0 Then
        '    chkToggleRcvd(1).Value = 1
        '    chkToggleRcvd(1).Value = 0
        'Else
        '    chkToggleRcvd(1).Value = 0
        '    chkToggleRcvd(1).Value = 1
        'End If
        'chkTlxCnt(0).Caption = tabTelexList(2).GetLineCount
        'chkTlxCnt(1).Caption = tabTelexList(3).GetLineCount
    End If
    'fraSplitter(0).ZOrder
End Sub

Private Sub chkPrint_Click()
    If chkPrint.Value = 1 Then
        MyMsgBox.CallAskUser 0, 0, 0, "Printer Manager", "Can't find a printer", "stop", "", UserAnswer
        chkPrint.Value = 0
    End If
End Sub

Private Sub chkPrintTxt_Click()
    Dim tmpUrno As String
    TelexPoolHead.MousePointer = 11
    If chkPrintTxt.Value = 1 Then
        tmpUrno = Trim(CurrentUrno.Text)
        If tmpUrno <> "" Then
            PrintServer.PrintOutput txtTelexText
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Print Manager", "No data selected for print.", "stop", "", UserAnswer
        End If
        chkPrintTxt.Value = 0
    End If
    TelexPoolHead.MousePointer = 0
End Sub

Private Sub chkRedirect_Click()
    Dim CurTlxRec As String
    Dim TlxText As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    On Error Resume Next
    If chkRedirect.Value = 1 Then
        ShowChildForm Me, chkRedirect, EditTelex, ""
        EditTelex.SetTelexDefault EditTelex.TlxPrioTab, MySitaDefPrio
        CurTlxRec = CurrentRecord.Text
        tmpSere = GetFieldValue("SERE", CurTlxRec, DataPool.TlxTabFields.Text)
        tmpTtyp = GetFieldValue("TTYP", CurTlxRec, DataPool.TlxTabFields.Text)
        If tmpTtyp = "" Then tmpTtyp = "FREE"
        If tmpSere = "C" Then
            TlxText = BuildTelexText(CurTlxRec, "TEXT")
        Else
            TlxText = GetFieldValue("TXT1", CurTlxRec, DataPool.TlxTabFields.Text)
            TlxText = TlxText & GetFieldValue("TXT2", CurTlxRec, DataPool.TlxTabFields.Text)
            TlxText = CleanString(TlxText, FOR_CLIENT, True)
        End If
        EditTelex.BuildAutoSendTelex "R", tmpSere, tmpTtyp, CurTlxRec, TlxText
        EditTelex.txtDestAddr.SetFocus
        chkRedirect.Value = 0
    End If
End Sub

Private Sub chkRegList_Click()
    If chkRegList.Value = 1 Then
        chkRegList.Value = 0
    End If
End Sub

Private Sub chkReject_Click()
    If chkReject.Value = 1 Then
        UpdateTelexStatus "X"
        chkReject.Value = 0
    End If
End Sub
Private Sub UpdateTelexStatus(SetStat As String)
Dim tmpUrno As String
Dim tmpStat As String
Dim tmpSqlKey As String
Dim retval As String
Dim RetCode As Integer
    tmpUrno = Trim(CurrentUrno.Text)
    If tmpUrno <> "" Then
        tmpStat = Trim(SetStat)
        If tmpStat <> "" Then
            tmpSqlKey = "WHERE URNO=" & tmpUrno
            RetCode = UfisServer.CallCeda(retval, "URT", DataPool.TableName, "WSTA", tmpStat, tmpSqlKey, "", 0, True, False)
        End If
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Update Manager", "No data selected to update.", "stop", "", UserAnswer
    End If
End Sub
Private Sub chkReset_Click()
    If chkReset.Value = 1 Then
        ResetFolderValues CurMem
        chkReset.Value = 0
    End If
End Sub

Private Sub chkScroll_Click()
    If chkScroll.Value = 1 Then chkScroll.BackColor = LightGreen Else chkScroll.BackColor = vbButtonFace
    ScrollFolderList
End Sub

Private Sub chkSetup_Click()
    If chkSetup.Value = 1 Then
        chkSetup.BackColor = LightestGreen
        'OnTop.Value = 0
        SetAllFormsOnTop False
        'ShowChildForm Me, chkSetup, PoolConfig, ""
        PoolConfig.Show , Me
    Else
        PoolConfig.Hide
        SetAllFormsOnTop True
        chkSetup.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkStatus_Click()
    Dim SelLine As Long
    Dim tmpUrno As String
    Dim tmpStat As String
    If chkStatus.Value = 1 Then
        tmpUrno = Trim(CurrentUrno.Text)
        If tmpUrno <> "" Then
            If chkTitleBar(1).Value = 0 Then
                ShowChildForm Me, chkStatus, TlxSndStat, ""
            End If
            If chkTitleBar(0).Value = 0 Then
                ShowChildForm Me, chkStatus, TlxRcvStat, ""
                SelLine = tabTelexList(0).GetCurrentSelected
                tmpStat = tabTelexList(0).GetColumnValue(SelLine, 0)
                TlxRcvStat.ExplainStatus "STAT", tmpStat, True
                tmpStat = tabTelexList(0).GetColumnValue(SelLine, 1)
                TlxRcvStat.ExplainStatus "WSTA", tmpStat, True
            End If
        Else
            chkStatus.Value = 0
        End If
    Else
        TlxRcvStat.Hide
        TlxSndStat.Hide
    End If
End Sub

Public Sub SearchFlightTelexes(tmpFldLst As String, tmpDatLst As String)
    Dim tmpData As String
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    InitialReset = True
    optMem(1).Value = False
    optMem(1).Value = True
    tmpData = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
    If tmpData = "D" Then
        tmpData = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
    Else
        tmpData = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
    End If
    
    UseServerTime = CedaFullDateToVb(tmpData)
    If optLocal.Value = True Then UseServerTime = DateAdd("n", UtcTimeDiff, UseServerTime)
    BeginTime = DateAdd("h", -Abs(Val(MySetUp.MainFrom)), UseServerTime)
    EndTime = DateAdd("h", Abs(Val(MySetUp.MainTo)), UseServerTime)
    txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
    txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
    txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
    txtDateTo.Tag = Format(EndTime, "yyyymmdd")
    txtTimeFrom.Text = Format(BeginTime, "hh:mm")
    txtTimeTo.Text = Format(EndTime, "hh:mm")
    
    txtFlca = GetFieldValue("ALC3", tmpDatLst, tmpFldLst)
    txtFltn = GetFieldValue("FLTN", tmpDatLst, tmpFldLst)
    txtFlns = GetFieldValue("FLNS", tmpDatLst, tmpFldLst)
    AftUrnoFromFips = GetFieldValue("URNO", tmpDatLst, tmpFldLst)
    Me.Refresh
    InitialReset = False
    chkLoad.Value = 1
End Sub

Private Sub chkStop_Click()
    Dim tmpTxt As String
    Dim retval As Integer
    If chkStop.Value = 1 Then
        chkStop.BackColor = LightGreen
        tmpTxt = ""
        tmpTxt = tmpTxt & "Do you want to stop this procedure ?"
        retval = MyMsgBox.CallAskUser(0, 0, 0, "Server Access Control", tmpTxt, "ask", "Yes,No", UserAnswer)
        If retval = 1 Then StopLoading = True
        chkStop.Value = 0
    Else
        chkStop.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTabs_Click(Index As Integer)
    If (Not ChkTabsIsBusy) And (Not ReorgFolders) Then CheckToggledTabs Index, chkTabs, 1
    If chkTabs(Index).Value = 1 Then
        chkTabs(Index).BackColor = LightGreen
    Else
        chkTabs(Index).BackColor = vbButtonFace
    End If
    If (Not StopNestedCalls) And (Not ReorgFolders) Then CheckLoadButton False
End Sub

Private Sub chkTextFilt_Click()
    If chkTextFilt.Value = 1 Then
        chkTextFilt.Value = 0
    End If
End Sub

Private Sub chkTlxCount_Click()
    If chkTlxCount.Value = 1 Then
        chkTlxCount.BackColor = LightGreen
        chkTlxCount.Refresh
        CountFlightTelex
    Else
        chkTlxCount.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkToggle_Click(Index As Integer)
    Dim TopDiff As Long
    If chkToggle(Index).Value = 1 Then
        chkToggle(Index).BackColor = LightGreen
        Select Case Index
            Case 2
                TopDiff = fraListPanel.Top - UfisFolder.Top
                fraListPanel.Top = UfisFolder.Top
                fraListPanel.Height = fraListPanel.Height + TopDiff
                fraSplitter(2).Top = fraSplitter(2).Top + TopDiff
                fraSplitter(1).Top = fraSplitter(2).Top
                fraSplitter(4).Top = fraSplitter(2).Top
                fraListPanel.ZOrder
                ArrangeFolderButtons "TOGGLE"
                ResizeTabs
            Case Else
        End Select
    Else
        chkToggle(Index).BackColor = vbButtonFace
        Select Case Index
            Case 2
                fraListPanel.Top = UfisFolder.Top + UfisFolder.Height
                TopDiff = fraListPanel.Top - UfisFolder.Top
                fraListPanel.Height = fraListPanel.Height - TopDiff
                fraSplitter(2).Top = fraSplitter(2).Top - TopDiff
                If fraSplitter(2).Top < 2100 Then fraSplitter(2).Top = 2100
                fraSplitter(1).Top = fraSplitter(2).Top
                fraSplitter(4).Top = fraSplitter(2).Top
                ArrangeFolderButtons "TOGGLE"
                ResizeTabs
            Case Else
        End Select
    End If
End Sub

Private Sub InitFolderList(AddList As String, AddTags As String, AddActs As String)
    Dim i As Long
    Dim j As Long
    Dim TabList As String
    Dim TabTags As String
    Dim TabActs As String
    Dim SetCaption As String
    Dim SetTag As String
    Dim SetAction As String
    Dim FolderSize As Long
    Dim MaxFolderSize As Long
    
    ShowTabButtons = True
    For i = 0 To fraFolder.UBound
        fraFolder(i).Visible = False
    Next
    fraFolder(0).Height = chkFolder(0).Height
    chkFolder(0).Height = 345
    fraBlend(0).Height = 60
    'chkToggle(0).Height = chkFolder(0).Height - 60
    fraFolder(0).Height = chkFolder(0).Height + fraBlend(0).Height + chkTabs(0).Height + 45
    
    FirstAssignFolder = ItemCount(FolderCaptions, ",")
    
    If AddList <> "" Then
        TabList = FolderCaptions & "," & AddList & ",ELSE,ALL"
        TabTags = FolderTypeList & "," & AddTags & ",,"
        TabActs = TypeActionList & "," & AddActs & ",,"
    Else
        TabList = FolderCaptions & ",ELSE,ALL"
        TabTags = FolderTypeList & ",,"
        TabActs = TypeActionList & ",,"
    End If
    MaxFolderSize = 0
    j = ItemCount(TabList, ",") - 1
    AllTypesCol = j
    OtherTypesCol = j - 1
    LastFolderIndex = j - 2
    For i = 0 To j
        SetCaption = GetRealItem(TabList, i, ",")
        SetTag = GetRealItem(TabTags, i, ",")
        SetAction = GetRealItem(TabActs, i, ",")
        If InStr(SetCaption, ":") > 0 Then
            SetCaption = GetRealItem(SetCaption, 0, ":")
            SetTag = GetRealItem(SetTag, 1, ":")
            SetTag = Replace(SetTag, ";", ",", 1, -1, vbBinaryCompare)
        End If
        If SetCaption = "MVT" Then
            If InStr(SetTag, "COR") = 0 Then
                SetTag = "MVT,COR"
            End If
        End If
        FolderSize = DefineFolderTab(CInt(i), SetCaption, SetTag, SetAction)
        If FolderSize > MaxFolderSize Then MaxFolderSize = FolderSize
    Next
    If FolderAutoSize Then
        For i = 0 To chkFolder.UBound
            chkFolder(i).Width = MaxFolderSize
            fraFolder(i).Width = chkFolder(i).Width
            chkTabs(i).Width = chkFolder(i).Width
            fraBlend(i).Width = chkFolder(i).Width
            fraLine(i).Width = chkFolder(i).Width
        Next
    End If
    
    FolderButtons(0).Left = RightCover.Left
    FolderScroll.Max = j
    FolderScroll.min = 0
    chkScroll.Tag = "-1"
    If Not ShowTabButtons Then chkToggle(1).Visible = False
    chkToggle(1).Value = 1
    Form_Resize
End Sub

Private Function DefineFolderTab(Index As Integer, SetCaption As String, SetTag As String, SetAction As String) As Long
    Dim NewLeft As Long
    Dim ButtonTop As Long
    Dim FolderTop As Long
    Dim BlendTop As Long
    Dim SelTop As Long
    Dim LineTop As Long
    Dim RightSide As Long
    Dim tmpTxt As String
    Dim ToolTipText As String
    Dim FolderSize As Long
    Dim MaxFolderSize As Long
    FolderSize = FolderFixedSize
    If FolderDynSize = True Then FolderSize = TextWidth(SetCaption) + 120
    If FolderSize < FolderMinSize Then FolderSize = FolderMinSize
    
    FolderTop = 135
    ButtonTop = FolderTop + 60
    LineTop = ButtonTop + chkFolder(0).Height - 30
    BlendTop = LineTop - 30
    SelTop = ButtonTop + chkFolder(0).Height + 30
    NewLeft = fraFolder(0).Width * Index + 90
    RightSide = NewLeft + fraFolder(0).Width - 15
    If Index > fraFolder.UBound Then
        Load fraFolder(Index)
        Load chkFolder(Index)
        Load chkTabs(Index)
        Load fraLine(Index)
        Load fraBlend(Index)
        Set chkFolder(Index).Container = fraFolder(Index)
        Set chkTabs(Index).Container = fraFolder(Index)
        Set fraLine(Index).Container = fraFolder(Index)
        Set fraBlend(Index).Container = fraFolder(Index)
    End If
    fraFolder(Index).Left = NewLeft
    chkFolder(Index).Width = FolderSize
    fraFolder(Index).Width = chkFolder(Index).Width
    chkTabs(Index).Width = chkFolder(Index).Width
    fraBlend(Index).Width = chkFolder(Index).Width
    fraLine(Index).Width = chkFolder(Index).Width
    
    chkFolder(Index).Left = 0
    chkTabs(Index).Left = 0
    fraLine(Index).Left = 0
    fraBlend(Index).Left = 0
    fraFolder(Index).Top = FolderTop
    chkFolder(Index).Top = 60
    fraLine(Index).Top = 375
    fraBlend(Index).Top = 345
    chkTabs(Index).Top = 420
    
    fraFolder(Index).BackColor = vbButtonFace
    fraBlend(Index).BackColor = vbButtonFace
    
    chkFolder(Index).Caption = SetCaption
    fraFolder(Index).Tag = SetTag
    fraBlend(Index).Tag = SetAction
    chkFolder(Index).Tag = "0"
    chkTabs(Index).Tag = "0"
    chkTabs(Index).Caption = ""
    
    If (SetTag <> "") And (SetTag <> SetCaption) Then
        If InStr(SetTag, ",") > 0 Then
            tmpTxt = "Telex Types: " & SetTag
        Else
            tmpTxt = "Telex Type: " & SetTag
        End If
    ElseIf SetTag <> "" Then
        tmpTxt = "Telex Type Folder"
    Else
        'Else or All
    End If
    If Len(SetCaption) > 0 Then
        ToolTipText = GetIniEntry(myIniFullName, "TOOLTIP_TEXT", "", SetCaption, "")
    Else
        ToolTipText = ""
    End If
    If Len(ToolTipText) > 0 Then
        chkFolder(Index).ToolTipText = ToolTipText
    Else
        chkFolder(Index).ToolTipText = tmpTxt
    End If
    
    If (CLng(Index) >= FirstAssignFolder) And (CLng(Index) < OtherTypesCol) Then
        chkFolder(Index).Picture = Picture1(2).Picture
        chkFolder(Index).ToolTipText = "Hold-File Folder"
    Else
        chkFolder(Index).Picture = LoadPicture("")
    End If
    
    fraFolder(Index).Visible = True
    chkFolder(Index).Visible = True
    fraLine(Index).Visible = True
    fraBlend(Index).Visible = True
    fraFolder(Index).ZOrder
    'chkFolder(Index).ZOrder
    fraBlend(Index).ZOrder
    fraLine(Index).ZOrder
    If ShowTabButtons Then
        chkTabs(Index).Visible = True
        'chkTabs(Index).ZOrder
    End If
    DefineFolderTab = FolderSize
End Function

Private Sub ScrollFolderList()
    Dim NewTop As Long
    Dim CurTop As Long
    CurTop = fraListPanel.Top
    NewTop = ArrangeFolderTabs(CurTop)
    If NewTop <> CurTop Then Form_Resize
End Sub

Private Sub chkToggleRcvd_Click(Index As Integer)
    Dim idx As Integer
    If chkToggleRcvd(Index).Value = 1 Then
        chkToggleRcvd(Index).BackColor = LightGreen
        chkToggleRcvd(Index).ForeColor = vbBlack
        chkOnlineRefresh(2).Value = 0
        chkOnlineRefresh(3).Value = 0
        Select Case Index
            Case 0
                'AreaPanel(0)=RCVD/Button RCVD(0) and SENT(0)
                'Button RCVD of RCVD Area
                AreaPanel(0).ZOrder
                chkToggleSent(0).Value = 0
                chkToggleSent(1).Value = 0
                chkToggleRcvd(1).Value = 0
                txtTelexText.Text = ""
                If MainIsOnlineNow Then idx = 2 Else idx = 0
                tabTelexList(idx).SetCurrentSelection tabTelexList(idx).GetCurrentSelected
            Case 1
                'AreaPanel(1)=SENT/Button RCVD(1) and SENT(1)
                'Button RCVD of SENT Area
                AreaPanel(0).ZOrder
                chkToggleSent(0).Value = 0
                chkToggleSent(1).Value = 0
                chkToggleRcvd(1).Value = 0
                chkToggleRcvd(0).Value = 1
            Case Else
            chkToggleRcvd(Index).Value = 0
        End Select
    Else
        chkToggleRcvd(Index).BackColor = chkTitleBar(0).BackColor
        chkToggleRcvd(Index).ForeColor = vbWhite
    End If
End Sub

Private Sub chkToggleSent_Click(Index As Integer)
    Dim idx As Integer
    If chkToggleSent(Index).Value = 1 Then
        chkToggleSent(Index).BackColor = LightGreen
        chkToggleSent(Index).ForeColor = vbBlack
        chkOnlineRefresh(2).Value = 0
        chkOnlineRefresh(3).Value = 0
        Select Case Index
            Case 0
                'Sent Button of RCVD Area
                AreaPanel(1).ZOrder
                chkToggleRcvd(0).Value = 0
                chkToggleRcvd(1).Value = 0
                chkToggleSent(0).Value = 0
                chkToggleSent(1).Value = 1
            Case 1
                'Sent Button of SENT Area
                AreaPanel(1).ZOrder
                chkToggleRcvd(0).Value = 0
                chkToggleRcvd(1).Value = 0
                chkToggleSent(0).Value = 0
                txtTelexText.Text = ""
                If MainIsOnlineNow Then idx = 3 Else idx = 1
                tabTelexList(idx).SetCurrentSelection tabTelexList(idx).GetCurrentSelected
            Case Else
            chkToggleSent(Index).Value = 0
        End Select
    Else
        chkToggleSent(Index).BackColor = chkTitleBar(0).BackColor
        chkToggleSent(Index).ForeColor = vbWhite
    End If
End Sub

Private Sub chkTool_Click(Index As Integer)
    Dim FileName As String
    Dim AppDrive As String
    AppDrive = UFIS_SYSTEM
    AppDrive = Left(AppDrive, 2)
    
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                If Not ImportIsConnected Then MsgToImportTool "0,START"
            Case 1
                MsgToImportTool "0,SPOOL"
            Case 2
                CheckImportType
                chkTool(Index).Value = 0
            Case 3
                '
                'FileName = "D:\Ufis\Data\ImportTool\DXB\SIN-SCORE-half.txt"
                FileName = AppDrive + "\Ufis\Data\ImportTool\DXB\SIN-SCORE-half.txt"
                
                FileName = "NAME[" & FileName & "]"
                MsgToImportTool "0," & FileName
                MsgToImportTool "0,FILE"
                chkTool(Index).Value = 0
            Case Else
        End Select
    Else
        If ImportIsConnected Then
            Select Case Index
                Case 0
                    MsgToImportTool "0,CLOSE"
                Case 1
                    MsgToImportTool "0,ACTIV"
                Case 2
                    'CheckImportType
                Case Else
            End Select
        End If
        chkTool(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub CheckImportType()
    Dim LineNo As Long
    Dim Index As Integer
    Dim tmpTtyp As String
    Dim TlxLine As String
    Dim RecNbr As String
    Index = -1
    If chkTitleBar(0).Value = 0 Then
        Index = 0
    ElseIf chkTitleBar(1).Value = 0 Then
        Index = 1
    End If
    If Index >= 0 Then
        LineNo = tabTelexList(Index).GetCurrentSelected
        If LineNo >= 0 Then
            tmpTtyp = tabTelexList(Index).GetColumnValue(LineNo, 2)
            Select Case tmpTtyp
                Case "SCOR"
                    TlxLine = tabTelexList(Index).GetLineValues(LineNo)
                    RecNbr = GetItem(TlxLine, 9, ",")
                    If RecNbr <> "" Then
                        LineNo = Val(RecNbr)
                        ScoreToImportTool CurMem, LineNo
                    End If
                Case Else
            End Select
        End If
    End If
End Sub
Public Sub ScoreToImportTool(MemIdx As Integer, LineNo As Long)
    Dim CurTlxRec As String
    Dim tmpText As String
    Dim tmpData As String
    Dim tmpWsta As String
    Dim tmpUrno As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim retval As String
    Dim CurItm As Long
    Dim ApcPos As Integer
    Dim tmpMsg As String
    Dim MyApc3 As String
    Dim tmpSeq As String
    MyApc3 = HomeAirport
    If LineNo >= 0 Then
        CurTlxRec = DataPool.TelexData(MemIdx).GetLineValues(LineNo)
        tmpWsta = GetFieldValue("WSTA", CurTlxRec, DataPool.TlxTabFields.Text)
        tmpUrno = GetFieldValue("URNO", CurTlxRec, DataPool.TlxTabFields.Text)
        If tmpUrno <> "" Then
            clSqlKey = "WHERE URNO=" & tmpUrno
            If (tmpWsta = "") Or (InStr("TIM", tmpWsta) = 0) Then
                tmpWsta = "T"
                RetCode = UfisServer.CallCeda(retval, "URT", DataPool.TableName, "WSTA", tmpWsta, clSqlKey, "", 0, True, False)
            End If
            tmpText = GetFieldValue("TXT1", CurTlxRec, DataPool.TlxTabFields.Text)
            tmpText = tmpText & GetFieldValue("TXT2", CurTlxRec, DataPool.TlxTabFields.Text)
            CurItm = 0
            tmpData = GetRealItem(tmpText, CurItm, Chr(28))
            If tmpData <> "" Then
                MsgToImportTool "0,PCK.BGN"
            End If
            While tmpData <> ""
                CurItm = CurItm + 1
                tmpSeq = CStr(CurItm)
                ApcPos = CheckScoreLineFormat(tmpData, MyApc3, tmpSeq)
                tmpMsg = tmpSeq & "," & Left(tmpData, 8) & "," & tmpData & "," & tmpUrno
                MsgToImportTool tmpMsg
                tmpData = GetRealItem(tmpText, CurItm, Chr(28))
            Wend
            If CurItm > 0 Then
                MsgToImportTool "0,PCK.END"
            End If
        End If
    End If
End Sub

Private Sub chkWeb_Click()
    If chkWeb.Value = 1 Then
        chkWeb.BackColor = LightGreen
        MyWebBrowser.Show
        chkWeb.Value = 0
    Else
        chkWeb.BackColor = vbButtonFace
        'MyWebBrowser.Hide
    End If
End Sub

Private Sub Command1_Click()
    UfisServer.Show , Me
End Sub

Private Sub FixWin_Click()
    If FixWin.Value = 1 Then
        AutoArrange = True
        FixWin.BackColor = LightGreen
        ArrangeAllWin
    Else
        AutoArrange = False
        FixWin.BackColor = vbButtonFace
    End If
End Sub

Public Sub ArrangeAllWin()
    If AutoArrange Then
        SystemResizing = True
        If Me.WindowState <> vbNormal Then Me.WindowState = vbNormal
        Me.Refresh
        InitPosSize True
        SystemResizing = False
    End If
End Sub

Private Sub FolderScroll_Change()
    ScrollFolderList
End Sub

Private Sub FolderScroll_Scroll()
    ScrollFolderList
End Sub

Private Sub Form_Activate()
    Static IsSnapped As Boolean
    Static IAmBusy As Boolean
    If Not IAmBusy Then
        IAmBusy = True
        If (Not MainFormIsReadyForUse) And (Not HiddenMain.Visible) Then HiddenMain.ShowHiddenMain True
        If (Not IsSnapped) And ((ApplicationIsStarted) Or (MainFormPrepareLayout)) Then
            MeIsVisible = True
            If Not MainFormIsReadyForUse Then
                If AutoOpenOnline Then OnLine.Value = 1
                If MyMainPurpose <> "AUTO_SEND" Then
                    MainFormIsReadyForUse = True
                    FixWin.Value = 1
                    FixWin.Value = 0
                    'If PoolConfig.OnLineCfg(0).Value = 0 Then Me.Hide
                    If OpenedFromFips Then
                        chkExit.Caption = "Close"
                        SetFormOnTop Me, False, False
                        SetFormOnTop Me, True, True
                        If HiddenMain.Visible Then
                            'SetFormOnTop HiddenMain, False, False
                            PutFormOnTop HiddenMain, False, True, True
                        End If
                        'chkExit.Visible = False
                    End If
                Else
                    If Not Me.Visible Then
                        Me.Visible = True
                    End If
                    If Me.WindowState <> vbNormal Then
                        Me.WindowState = vbNormal
                    End If
                    Me.Top = CreateTelex.Top
                    Me.Height = CreateTelex.Height
                    Me.Left = CreateTelex.Left + CreateTelex.Width - Me.Width
                    MainFormIsReadyForUse = True
                End If
            End If
            MainFormPrepareLayout = False
            If Me.WindowState = vbNormal Then Me.Refresh
            chkFilter(0).Value = MySetUp.RcvTabList.Value
            chkFilter(1).Value = MySetUp.SndTabList.Value
            chkFilter(0).Refresh
            chkFilter(1).Refresh
            RcvLoadCapt = "0 " & chkTitleBar(0).Tag & " (Loaded)"
            RcvOnlineCapt = chkTitleBar(0).Tag & " (Online)"
            SndLoadCapt = "0 " & chkTitleBar(1).Tag & " (Loaded)"
            SndOnlineCapt = chkTitleBar(1).Tag & " (Online)"
            AddFolderList = ""
            ArrangeMainOnline
            EditMode = False
            InsertTabAssign = False
            Me.Refresh
            DoEvents
            If (MyMainPurpose <> "AUTO_SEND") Or (MainOpenedByUser) Then
                'HiddenMain.HideHiddenMain
                HiddenMain.Hide
                HiddenMain.Refresh
            End If
            DoEvents
            If (Not IsSnapped) Then
                IsSnapped = True
            End If
            SetFormOnTop Me, False, False
            SetFormOnTop Me, True, True
            If HiddenMain.Visible Then
                'SetFormOnTop HiddenMain, False, False
                PutFormOnTop HiddenMain, False, True, True
            Else
                If MyMainPurpose <> "AUTO_SEND" Then
                    SetFormOnTop Me, False, False
                End If
            End If
        Else
            If Not IsSnapped Then
                SetFormOnTop Me, False, False
                SetFormOnTop Me, True, True
                If HiddenMain.Visible Then
                    'SetFormOnTop HiddenMain, False, False
                    PutFormOnTop HiddenMain, False, True, True
                End If
            End If
            If MainIsDeploy Then PoolConfig.FipsLinkCfg(0).Value = 1
        End If
        chkApplTest(0).ZOrder
        ApplicationIsReadyForUse = True
        
        MainWindowIsOpen = Me.Visible
        MainOpenedByUser = False
        IAmBusy = False
    End If
End Sub

Public Sub ArrangeMainOnline()
    Dim i As Integer
    On Error Resume Next
    chkOnlineRcv.Top = 60
    chkOnlineRcv.Visible = True
    AreaPanel(4).Visible = True
    If MainIsDeploy = True Then
        PoolConfig.FipsLinkCfg(0).Value = 1
    Else
        PoolConfig.FipsLinkCfg(0).Value = 0
        ArrangeBrowsers False
    End If
    PoolConfig.DataPoolCfg(1).Value = MySetUp.FlightBrowser.Value
    If MainIsFltDeploy Then
        PoolConfig.FipsLinkCfg(1).Value = 1
    Else
        PoolConfig.FipsLinkCfg(1).Value = 0
        ArrangeFlightList True
    End If
    If MainIsOnline = True Then
        chkOnlineRcv.Enabled = True
        MainIsOnlineNow = False
        chkOnlineRcv.Value = 1
        If MyMainPurpose <> "AUTO_SEND" Then
            RcvTelex.Show
            OutTelex.Show
            OnLine.Value = 1
            FixWin.Value = 1
            FixWin.Value = 0
            OnLine.Value = 0
        End If
    Else
        chkOnlineRcv.Enabled = False
        MainIsOnlineNow = False
        'AreaPanel(0).Visible = True
        'AreaPanel(1).Visible = True
        chkToggleRcvd(0).Value = 1
        ListGroup(0).ZOrder
        ListGroup(1).ZOrder
        OnLine.Value = 1
    End If
    If (MainIsDeploy) Or (chkAssFolder.Enabled) Then
        chkLoadAss.Value = 1
    End If
    Form_Resize
    Me.Refresh
    If chkAssFolder.Enabled = True Then chkAssFolder.Value = 1
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
    If KeyCode = vbKeyF2 Then chkNew.Value = 1
End Sub

Private Sub Form_LinkClose()
    'MsgBox "LinkClose"
    ImportIsConnected = False
    ImportIsPending = False
    InfoSpooler.ResetContent
    InfoSpooler.Refresh
    SpoolTimer.Enabled = False
    chkTool(0).Value = 0
    chkTool(1).Value = 0
    chkTool(2).Value = 0
End Sub

Private Sub Form_LinkError(LinkErr As Integer)
    MsgBox "LinkError: " & CStr(LinkErr)
End Sub

Private Sub Form_LinkExecute(CmdStr As String, Cancel As Integer)
    MsgBox "LinkExec:" & CmdStr
End Sub

Private Sub Form_LinkOpen(Cancel As Integer)
    'MsgBox "LinkOpen"
    InfoFromClient.ZOrder
    InfoToClient.ZOrder
    ImportIsConnected = True
    ImportIsPending = False
    chkTool(0).Value = 1
    CheckCompareInfo
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim NewTop As Long
    Dim MaxTop As Long
    On Error Resume Next
    If Not ShutDownRequested Then
        MeIsVisible = False
        'ApplIsInDesignMode = False
        'AutoSnapMainDialog = True
        If (AutoSnapMainDialog) Then
            If Not ApplIsInDesignMode Then
                Set m_Snap = New CSnapDialog
                m_Snap.hwnd = Me.hwnd
            End If
        End If
        TopStatus(0).Visible = False
        TopStatus(0).Top = -30
        TopStatus(0).Left = -30
        TopSplit(0).Height = 105
        TopSplit(0).BackColor = MyOwnButtonFace
        
        AnyPic(0).Top = 0
        AnyPic(1).Top = 0
        AnyPic(0).Left = 0
        AnyPic(1).Left = RightCover.ScaleWidth - AnyPic(1).Width
        
        MaxTop = Screen.Height
        RightCover.Height = MaxTop
        RightCover.AutoRedraw = True
        NewTop = 0
        While NewTop < MaxTop
            RightCover.PaintPicture AnyPic(1), AnyPic(1).Left, NewTop
            RightCover.PaintPicture AnyPic(0), AnyPic(0).Left, NewTop
            NewTop = NewTop + AnyPic(0).Height
        Wend
        RightCover.Picture = RightCover.Image
        RightCover.AutoRedraw = False
        RightCover.Cls
        
        ActionPanel.Top = 0
        ActionPanel.Left = 0
        'ActionPanel.Width = RightCover.Width
        
        AnyPic(3).Top = 0
        AnyPic(4).Top = 0
        AnyPic(3).Left = 0
        AnyPic(4).Left = ActionPanel.ScaleWidth - AnyPic(4).Width
        
        ActionPanel.Height = MaxTop
        ActionPanel.AutoRedraw = True
        NewTop = 0
        While NewTop < MaxTop
            ActionPanel.PaintPicture AnyPic(3), AnyPic(3).Left, NewTop
            ActionPanel.PaintPicture AnyPic(4), AnyPic(4).Left, NewTop
            NewTop = NewTop + AnyPic(3).Height
        Wend
        ActionPanel.Picture = ActionPanel.Image
        ActionPanel.AutoRedraw = False
        ActionPanel.Cls
        ActionPanel.ZOrder
        AnyPic(3).Visible = False
        AnyPic(4).Visible = False
        
        AnyPic(0).Visible = False
        AnyPic(1).Visible = False
        Tiger(0).Top = 0
        Tiger(0).Left = 0
        TigerX(0).Width = Tiger(0).Width
        TigerX(0).Height = Tiger(0).Height
        MainButtons.BackColor = MyOwnPanelColor
        optMemButtons.BackColor = MainButtons.BackColor
        TopSplit(1).BackColor = MainButtons.BackColor
        
        chkExit.Tag = CStr(chkExit.Left)
        CurButtonPanel = -1
        PoolConfig.TlxFolderCfg(0).Enabled = chkAssFolder.Enabled
        PoolConfig.TlxFolderCfg(1).Enabled = chkAssFolder.Enabled
        InfoSpooler.ResetContent
        InfoSpooler.HeaderString = "Message"
        InfoSpooler.HeaderLengthString = "1000"
        InfoSpooler.SetFieldSeparator Chr(16)
        InitPosSize False
        ReorgLayers
        UfisServer.SetIndicator CedaStatus(0).Left, CedaStatus(0).Top, CedaStatus(0), CedaStatus(3), CedaStatus(1), CedaStatus(2)
        'Why connecting here?
        'It implicitely disconnects!
        'UfisServer.ConnectToCeda
        UfisServer.ModName.Tag = gsUserName
        UfisServer.SetUtcTimeDiff
        optUtc.ToolTipText = "Times in UTC (" & HomeAirport & " Local -" & Str(UtcTimeDiff) & " min.)"
        optLocal.ToolTipText = "Times in " & HomeAirport & " Local (UTC +" & Str(UtcTimeDiff) & " min.)"
        FdiTimeFields = "NXTI,ETDE,OFBD,AIRD,ETAE,LNDD,ONBD"
        InitLoaInfo
        HandleTlxTitleBar
        InfoFromClient.Text = "WAIT"
        InfoToClient.Text = "0,0,INIT"
        InfoFromClient.ZOrder
        InfoToClient.ZOrder
        InfoSpooler.ZOrder
        If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SORT_ASCENDING", "TRUE") = "FALSE" Then
            bmSortDirection = False
        Else
            bmSortDirection = True
        End If
        InitMyForm
        If MyMainPurpose <> "AUTO_SEND" Then
            Me.Show
        End If
    Else
        End
        'Unload Me
    End If
End Sub
Private Sub InitBasicButtons()
    Dim btnList As String
    Dim NewTop As Long
    Dim OldTop As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewSpace As Long
    Dim DefSpace As Long
    Dim itm As Integer
    Dim i As Integer
    Dim tmpName As String
    Dim tmpSpace As String
    chkNew.Visible = False
    chkRedirect.Visible = False
    chkCreate.Visible = False
    chkPrintTxt.Visible = False
    chkFlight.Visible = False
    chkAssign.Visible = False
    chkReject.Visible = False
    chkStatus.Visible = False
    FullText.Visible = False
    Select Case MyMainPurpose
        Case "POOL"
            btnList = "SPACING:1,NEW,REDIRECT,PRINT,FLIGHT,ASSIGN,REJECT,STATUS,FULLTEXT"
        Case "AUTO_SEND"
            btnList = "SPACING:1,NEW,REDIRECT,CREATE,PRINT,FLIGHT,ASSIGN,REJECT,STATUS,FULLTEXT"
        Case Else
            btnList = "SPACING:1,NEW,REDIRECT,PRINT,FLIGHT,ASSIGN,REJECT,STATUS,FULLTEXT"
    End Select
    btnList = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "BASIC_BUTTONS", btnList)
    DefSpace = 0
    NewWidth = chkNew.Width
    NewTop = 0
    itm = 0
    tmpName = "START"
    While tmpName <> ""
        itm = itm + 1
        tmpName = GetItem(btnList, itm, ",")
        If InStr(tmpName, ":") > 0 Then
            tmpSpace = GetItem(tmpName, 2, ":")
            tmpName = GetItem(tmpName, 1, ":")
        Else
            tmpSpace = GetItem(tmpName, 2, ";")
            tmpName = GetItem(tmpName, 1, ";")
        End If
        If tmpName <> "" Then
            OldTop = NewTop
            Select Case tmpName
                Case "NEW"
                    chkNew.Top = NewTop
                    chkNew.Visible = True
                    NewTop = NewTop + chkNew.Height
                Case "REDIRECT"
                    chkRedirect.Top = NewTop
                    chkRedirect.Visible = True
                    NewTop = NewTop + chkRedirect.Height
                Case "CREATE"
                    chkCreate.Top = NewTop
                    chkCreate.Visible = True
                    NewTop = NewTop + chkCreate.Height
                Case "PRINT"
                    chkPrintTxt.Top = NewTop
                    chkPrintTxt.Visible = True
                    NewTop = NewTop + chkPrintTxt.Height
                Case "FLIGHT"
                    chkFlight.Top = NewTop
                    chkFlight.Visible = True
                    NewTop = NewTop + chkFlight.Height
                Case "ASSIGN"
                    chkAssign.Top = NewTop
                    chkAssign.Visible = True
                    NewTop = NewTop + chkAssign.Height
                Case "REJECT"
                    chkReject.Top = NewTop
                    chkReject.Visible = True
                    NewTop = NewTop + chkReject.Height
                Case "STATUS"
                    chkStatus.Top = NewTop
                    chkStatus.Visible = True
                    NewTop = NewTop + chkStatus.Height
                Case "FULLTEXT"
                    FullText.Top = NewTop
                    FullText.Visible = True
                    NewTop = NewTop + FullText.Height
                Case "SPACE"
                    DefSpace = Val(tmpSpace) * 15
                    If DefSpace < 0 Then DefSpace = 0
                    tmpSpace = ""
                Case Else
            End Select
            If NewTop <> OldTop Then
                NewSpace = Val(tmpSpace) * 15
                If NewSpace < 15 Then NewSpace = DefSpace
                NewTop = NewTop + NewSpace
            End If
        End If
    Wend
    NewTop = NewTop + 60
    NewWidth = NewWidth + 60
    basicButtons.Height = NewTop
    basicButtons.Width = NewWidth
    TopButtons.Width = NewWidth
    TopButtons.Height = chkClose.Top + chkClose.Height + 60
    Set TopButtons.Container = RightCover
    Set basicButtons.Container = RightCover
    NewLeft = (RightCover.ScaleWidth - NewWidth) / 2
    TopButtons.Left = NewLeft
    basicButtons.Left = NewLeft
    TopButtons.Top = ((UfisFolder.Top - TopButtons.Height) / 2) - 30
    AnyPic(2).Height = basicButtons.ScaleHeight
    AnyPic(2).Visible = True
    NewTop = chkPanic.Top + chkPanic.Height
    chkBcFlow.Top = NewTop
    NewTop = NewTop + chkBcFlow.Height
    NewTop = NewTop + 60
    'For i = 0 To chkApplTest.UBound
    '    If chkApplTest(i).Tag = "Y" Then
    '        chkApplTest(i).Top = NewTop
    '        chkApplTest(i).Visible = True
    '        NewTop = NewTop + chkApplTest(i).Height
    '    End If
    'Next
    'NewTop = NewTop + 30
    'NewTop = NewTop + 60
    TestPanel.Height = NewTop
    TestPanel.Width = basicButtons.Width
    TestPanel.Left = basicButtons.Left
    InfoPanel.Width = TestPanel.Width
    InfoPanel.Left = TestPanel.Left
    TestPanel.ZOrder
    TopButtons.ZOrder
    InitBcLogTab
End Sub

Private Sub InitLoaInfo()
    LoaInfo(0).Tag = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "PAX_INFO", "A,B")
    LoaInfo(1).Tag = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOA_INFO", "D,C")
    LoaInfo(2).Tag = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "ULD_INFO", "E")

    ' rro 08.11.04: hide the buttons if no LoatabViewer is installed
    Dim strTmp As String
    strTmp = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOADTAB_VIEWER", "")
    strTmp = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "LOADTAB_VIEWER", strTmp)
    If strTmp = "" Then
        'LoaInfo(0).Visible = False
        'LoaInfo(1).Visible = False
        'LoaInfo(2).Visible = False
        'FolderButtons(0).Visible = False
        bmFolderButtons0 = False
    Else
        'LoaInfo(0).Visible = True
        'LoaInfo(1).Visible = True
        'LoaInfo(2).Visible = True
        'FolderButtons(0).Visible = True
        bmFolderButtons0 = True
    End If
End Sub

Public Sub InitPosSize(CheckOnline As Boolean)
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim NewSize As Long
    Dim MaxSize As Long
    Dim WinSize As Long
    Dim DlgSize As Long
    Dim SubSize As Long
    Dim WinCnt As Long
    On Error Resume Next
    If CheckOnline = True Then
        If MyMainPurpose = "AUTO_SEND" Then
            FixWin.Value = 0
            Exit Sub
        End If
        DlgSize = 0
        SubSize = 0
        Select Case LastResizeName
            Case Me.Name
                DlgSize = Me.Width
            Case "RcvTelex"
                If FormIsVisible("RcvTelex") Then SubSize = RcvTelex.Width
            Case "SndTelex"
                If FormIsVisible("SndTelex") Then SubSize = SndTelex.Width
            Case "PndTelex"
                If FormIsVisible("PndTelex") Then SubSize = PndTelex.Width
            Case "OutTelex"
                If FormIsVisible("OutTelex") Then SubSize = OutTelex.Width
            Case "RcvInfo"
                If FormIsVisible("RcvInfo") Then SubSize = RcvInfo.Width
            Case Else
        End Select
        WinCnt = 0
        WinSize = 0
        If FormIsVisible("RcvTelex") Then
            WinCnt = WinCnt + 1
            If RcvTelex.Width > WinSize Then WinSize = RcvTelex.Width
        End If
        If FormIsVisible("SndTelex") Then
            WinCnt = WinCnt + 1
            If SndTelex.Width > WinSize Then WinSize = SndTelex.Width
        End If
        If FormIsVisible("PndTelex") Then
            WinCnt = WinCnt + 1
            If PndTelex.Width > WinSize Then WinSize = PndTelex.Width
        End If
        If FormIsVisible("OutTelex") Then
            WinCnt = WinCnt + 1
            If OutTelex.Width > WinSize Then WinSize = OutTelex.Width
        End If
        If FormIsVisible("RcvInfo") Then
            WinCnt = WinCnt + 1
            If RcvInfo.Width > WinSize Then WinSize = RcvInfo.Width
        End If
        If WinCnt > 0 Then
            If SubSize > 0 Then WinSize = SubSize
            If DlgSize > 0 Then
                MaxSize = MaxScreenWidth - Me.Left
                If MaxSize > WinSize Then MaxSize = WinSize
                WinSize = MaxSize
                If WinSize < 600 Then WinSize = 600
            End If
            NewHeight = Me.Height
            MaxLeft = MaxScreenWidth - WinSize
            NewLeft = Me.Left + Me.Width
            If NewLeft > MaxLeft Then
                NewLeft = MaxLeft
                NewSize = NewLeft - Me.Left
                If NewSize > 1500 Then
                    Me.Width = NewSize
                End If
            End If
            
            NewTop = Me.Top
            NewHeight = NewHeight / WinCnt
            If FormIsVisible("RcvInfo") Then
                RcvInfo.WindowState = vbNormal
                RcvInfo.Show
                RcvInfo.Top = NewTop
                RcvInfo.Left = NewLeft
                RcvInfo.Height = NewHeight
                RcvInfo.Width = WinSize
                RcvInfo.OnTop.Value = Me.OnTop.Value
                'RcvInfo.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("RcvTelex") Then
                RcvTelex.WindowState = vbNormal
                RcvTelex.Show
                RcvTelex.Top = NewTop
                RcvTelex.Left = NewLeft
                RcvTelex.Height = NewHeight
                RcvTelex.Width = WinSize
                RcvTelex.OnTop.Value = Me.OnTop.Value
                'RcvTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("SndTelex") Then
                SndTelex.WindowState = vbNormal
                SndTelex.Show
                SndTelex.Top = NewTop
                SndTelex.Left = NewLeft
                SndTelex.Height = NewHeight
                SndTelex.Width = WinSize
                SndTelex.OnTop.Value = Me.OnTop.Value
                'SndTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("PndTelex") Then
                PndTelex.WindowState = vbNormal
                PndTelex.Show
                PndTelex.Top = NewTop
                PndTelex.Left = NewLeft
                PndTelex.Height = NewHeight
                PndTelex.Width = WinSize
                PndTelex.OnTop.Value = Me.OnTop.Value
                'PndTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If FormIsVisible("OutTelex") Then
                OutTelex.WindowState = vbNormal
                OutTelex.Show
                OutTelex.Top = NewTop
                OutTelex.Left = NewLeft
                OutTelex.Height = NewHeight
                OutTelex.Width = WinSize
                OutTelex.OnTop.Value = Me.OnTop.Value
                'OutTelex.Refresh
                NewTop = NewTop + NewHeight
                WinCnt = WinCnt - 1
                If WinCnt = 1 Then NewHeight = Me.Top + Me.Height - NewTop
            End If
            If Me.Visible = True Then
                'Me.SetFocus
                'Me.Refresh
                SetFormOnTop Me, True, True
            End If
        End If
        If FormIsLoaded("CreateTelex") Then
            CreateTelex.WindowState = vbNormal
            CreateTelex.Top = Me.Top
            CreateTelex.Left = Me.Left
            CreateTelex.Height = Me.Height
            CreateTelex.Width = Me.Width
        End If
    ElseIf (OpenedFromFips) And (UseOldDefautlts) Then
        MaxScreenWidth = 1024 * 15
        Me.Left = 300
        Me.Top = 645
        Me.Height = Screen.Height - Me.Top - 300
        NewWidth = fraStatusFilter.Left + fraStatusFilter.Width + RightCover.Width + 165
        If ActionPanel.Visible Then NewWidth = NewWidth + ActionPanel.Width
        Me.Width = NewWidth
    ElseIf OpenedFromFips Then
        MaxScreenWidth = 1024 * 15
        Me.Left = 300
        Me.Top = 645
        Me.Height = Screen.Height - Me.Top - 300
        NewWidth = fraStatusFilter.Left + fraStatusFilter.Width + RightCover.Width + 165
        If ActionPanel.Visible Then NewWidth = NewWidth + ActionPanel.Width
        Me.Width = NewWidth
    ElseIf UseOldDefautlts Then
        MaxScreenWidth = 1024 * 15  '(Default Monitor Type)
        Me.Left = 300
        Me.Top = 645
        Me.Height = Screen.Height - Me.Top - 300
        NewWidth = fraStatusFilter.Left + fraStatusFilter.Width + RightCover.Width + 165
        If ActionPanel.Visible Then NewWidth = NewWidth + ActionPanel.Width
        Me.Width = NewWidth
    'ElseIf AutoOpenOnline = True Then
    '    Me.Left = 0
    '    Me.Top = 0
    '    Me.Height = MaxScreenHeight
    '    Me.Width = MaxScreenWidth
    Else
        'A new default
        Me.Height = MaxScreenHeight
        If MyMainPurpose = "AUTO_SEND" Then
            Me.Top = (Screen.Height - Me.Height) / 2
            Me.Width = fraStatusFilter.Left + fraStatusFilter.Width + RightCover.Width + 165
            Me.Left = (MaxScreenWidth - 3600) - Me.Width
        Else
            Me.Top = (Screen.Height - Me.Height) / 2
            Me.Width = MaxScreenWidth - 3600    'Space for Online Windows
            Me.Left = ((1024 * 15) - Me.Width) / 2
            If Me.Left < 0 Then Me.Left = 0
        End If
    End If
End Sub

Public Sub InitMyForm() 'Called by StartModule
    Dim tmpData As String
    Dim NewTop As Long
    Dim MaxGridInit As Integer
    Dim i As Integer
    TopSplit(0).Left = -30
    TopSplit(0).Width = Screen.Width
    TopSplit(0).Top = chkLoad.Top + chkLoad.Height + 15
    MainButtons.Height = TopSplit(0).Top + TopSplit(0).Height
    TopSplit(1).Left = TopSplit(0).Left
    TopSplit(1).Width = TopSplit(0).Width
    TopSplit(1).Height = 60
    NewTop = MainButtons.Top + MainButtons.Height
    TopSplit(1).Top = NewTop
    NewTop = NewTop + TopSplit(1).Height
    fraTimeFilter.Top = NewTop
    fraFlightFilter.Top = NewTop
    fraTextFilter.Top = NewTop
    fraStatusFilter.Top = NewTop
    
    UfisFolder.Left = fraTimeFilter.Left
    UfisFolder.Top = fraTimeFilter.Top + fraTimeFilter.Height
    fraFolderToggles.Top = UfisFolder.Top
    FolderFixedSize = 645
    DefineFolderTab 0, "", "", ""
    UfisFolder.Width = fraStatusFilter.Left + fraStatusFilter.Width - fraTimeFilter.Left
    fraListPanel.Left = UfisFolder.Left
    fraListPanel.Width = UfisFolder.Width
    'fraListPanel.Height = UfisFolder.Height
    fraListPanel.Top = UfisFolder.Top + UfisFolder.Height
    InitBasicButtons
    
    
    
    'fraFolderLine(0).Top = fraLine(0).Top
    'fraFolderLine(0).Width = UfisFolder.Width
    
    
    fraSplitter(0).BackColor = MyOwnSplitColor
    fraSplitter(1).BackColor = MyOwnSplitColor
    fraSplitter(2).BackColor = MyOwnSplitColor
    fraSplitter(3).BackColor = MyOwnSplitColor
    fraSplitter(4).BackColor = MyOwnSplitColor
    
    Load MySetUp
    Load DataPool
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ASSIGN_PANEL_SIZE", "200")
    If tmpData <> "" Then AssignPanelSize = Val(tmpData) * 15
    If AssignPanelSize < 1500 Then AssignPanelSize = 1500
    If AssignPanelSize > 6000 Then AssignPanelSize = 6000
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FLIGHT_PANEL_SIZE", "240")
    If tmpData <> "" Then FlightPanelSize = Val(tmpData) * 15
    If FlightPanelSize < 1500 Then FlightPanelSize = 1500
    If FlightPanelSize > 6000 Then FlightPanelSize = 6000
    
    MaxGridInit = 9
    If MaxFolderRows > 3 Then MaxGridInit = 6
    InitGrids MaxGridInit
    
    
    InitHiddenFields
    fraLoadProgress.Top = fraTimeFilter.Top
    fraLoadProgress.Left = fraTimeFilter.Left
    fraLoadProgress.ZOrder
    InitFolderList "", "", ""
    Me.Caption = Me.Tag
    UfisServer.BasicDataInit 1, 0, "ALTTAB", "ALC2,ALC3,ALFN,URNO", ""
    InitFolderByName "All", True
    If MySetUp.FolderMode.Value = 1 Then
        ArrangeLayout True
    Else
        ArrangeLayout False
    End If
    'If MySetUp.SndTabList.Value = 1 Then
    '    ArrangeBrowsers True
    'Else
    '    ArrangeBrowsers False
    'End If
    'If MySetUp.FlightBrowser.Value = 1 Then
    '    ArrangeFlightList True
    'Else
    '    ArrangeFlightList False
    'End If
    'tabTelexList(2).Visible = False
    'tabTelexList(3).Visible = False
    
    InitFtypPanel
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "UFIS_WEB_BROWSER", "YES")
    If tmpData = "YES" Then chkWeb.Enabled = True Else chkWeb.Enabled = False
    
    Me.Caption = Me.Tag & " (" & MySetUp.ServerTime.Text & " " & MySetUp.ServerTimeType & ")"
End Sub
Public Sub ArrangeLayout(SetValue As Boolean)
    UfisFolder.Visible = SetValue
    fraFolderToggles.Visible = SetValue
    If SetValue Then
        fraListPanel.Top = UfisFolder.Top + UfisFolder.Height
    Else
        fraListPanel.Top = UfisFolder.Top
    End If
    Form_Resize
End Sub

Public Sub ArrangeBrowsers(UseSetValue As Boolean)
    Dim SetValue As Boolean
    Dim i As Integer
    SetValue = False
    If PoolConfig.OnLineCfg(3).Value = 1 Then
        AreaPanel(1).Visible = True
        If PoolConfig.OnLineCfg(1).Value = 1 Then
            AreaPanel(0).Visible = True
            For i = 0 To 1
                fraToggleRcv(i).BackColor = MyOwnButtonFace
                fraToggleRcv(i).Visible = True
            Next
            For i = 0 To fraToggleCnt.UBound
                fraToggleCnt(i).BackColor = MyOwnButtonFace
                fraToggleCnt(i).Visible = True
            Next
        Else
            AreaPanel(0).Visible = False
            For i = 0 To 1
                fraToggleRcv(i).Visible = False
            Next
            For i = 0 To fraToggleCnt.UBound
                fraToggleCnt(i).Visible = False
            Next
        End If
    Else
        AreaPanel(1).Visible = False
        If PoolConfig.OnLineCfg(1).Value = 1 Then
            AreaPanel(0).Visible = True
        Else
            AreaPanel(0).Visible = False
        End If
        For i = 0 To 1
            fraToggleRcv(i).Visible = False
        Next
        For i = 0 To fraToggleCnt.UBound
            fraToggleCnt(i).Visible = False
        Next
    End If
    If MainIsDeploy Then
        AreaPanel(2).Visible = True
    Else
        AreaPanel(2).Visible = False
    End If
    fraToggleRcv(4).Top = 0
    fraToggleRcv(4).Width = 975
    fraToggleRcv(4).Visible = True
    Form_Resize
End Sub
Public Sub ArrangeFlightList(UseSetValue As Boolean)
    Dim SetValue As Boolean
    SetValue = False
    If MainIsFltDeploy Then SetValue = True
    If MySetUp.FlightBrowser.Value = 1 Then SetValue = True
    If PoolConfig.DataPoolCfg(1).Value = 1 Then SetValue = True
    AreaPanel(3).Visible = SetValue
    If MainIsFltDeploy Then
        chkFlightList(5).Visible = True
        chkFlightList(6).Visible = True
        chkFlightList(7).Visible = True
    Else
        chkFlightList(5).Visible = False
        chkFlightList(6).Visible = False
        chkFlightList(7).Visible = False
    End If
    Form_Resize
End Sub
Public Sub InitMemButtons()
    For CurMem = 0 To 3
        optMem(CurMem).Tag = -2
        'optMem(CurMem).ToolTipText = "Ready for use"
        CheckLoadButton False
        StoreFolderValues (CurMem)
        ActFilterValues(CurMem) = NewFilterValues(CurMem)
    Next
    If Not MainIsOnline Then
        optMem(0).Value = True
        CurMem = 0
    End If
End Sub
Public Sub InitDateFields(ForWhat As String)
    Dim ToggleLocalTime As Boolean
    Dim tmpVal As String
    Dim tmpTimeFrom As String
    Dim tmpTimeTo As String
    Dim tmpText As String
    Dim ConText As String
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    ToggleLocalTime = optLocal.Value
    optUtc.Value = True
    CurrentTimeFilter = ForWhat
    Select Case ForWhat
        Case "USR"
            txtDateFrom.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
            txtDateTo.ToolTipText = "Day Date " & MySetUp.DefDateFormat.Text
            UseServerTime = CedaFullDateToVb(MySetUp.ServerTime.Tag)
            If MySetUp.ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
            BeginTime = DateAdd("h", -Abs(Val(MySetUp.MainFrom)), UseServerTime)
            EndTime = DateAdd("h", Abs(Val(MySetUp.MainTo)), UseServerTime)
            txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
            txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
            txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
            txtDateTo.Tag = Format(EndTime, "yyyymmdd")
            txtTimeFrom.Text = Format(BeginTime, "hh:mm")
            txtTimeTo.Text = Format(EndTime, "hh:mm")
            ConText = "-" & CStr(Abs(Val(MySetUp.MainFrom))) & " / +" & CStr(Abs(Val(MySetUp.MainTo))) & " h"
            tmpText = "System Main Filter [" & ConText & "]"
            fraTimeFilter.ToolTipText = tmpText
            optTimeType(0).ToolTipText = tmpText
            fraTimeFilter.Caption = "Main Date/Time Filter"
        Case "RCV"
            tmpVal = GetOnlineTimeFrame("RCV", ConText)
            tmpTimeFrom = GetItem(tmpVal, 1, ",")
            BeginTime = CedaFullDateToVb(tmpTimeFrom)
            tmpTimeTo = GetItem(tmpVal, 2, ",")
            EndTime = CedaFullDateToVb(tmpTimeTo)
            txtDateFrom.Text = Format(BeginTime, MySetUp.DefDateFormat)
            txtDateFrom.Tag = Format(BeginTime, "yyyymmdd")
            txtDateTo.Text = Format(EndTime, MySetUp.DefDateFormat)
            txtDateTo.Tag = Format(EndTime, "yyyymmdd")
            txtTimeFrom.Text = Format(BeginTime, "hh:mm")
            txtTimeTo.Text = Format(EndTime, "hh:mm")
            tmpText = "System Online Filter [" & ConText & "]"
            fraTimeFilter.ToolTipText = tmpText
            optTimeType(1).ToolTipText = tmpText
            fraTimeFilter.Caption = "Online Date/Time Filter"
        Case Else
    End Select
    If txtTimeFrom.Visible Then
        txtTimeFrom.SetFocus
    End If
    optLocal.Value = ToggleLocalTime
End Sub
Private Sub InitHiddenFields()
    CurrentUrno = ""
    CurrentRecord = ""
End Sub
Private Sub InitGrids(ipLines As Integer)
    Dim NewTop As Long
    Dim i As Integer
    For i = 0 To AreaPanel.UBound
        AreaPanel(i).BackColor = MyOwnPanelColor
    Next
    For i = 0 To TitlePanel.UBound
        TitlePanel(i).Top = 0
        TitlePanel(i).Left = 0
        TitlePanel(i).Height = chkTitleBar(0).Height + 15
        TitlePanel(i).BackColor = MyOwnPanelColor
    Next
    For i = 0 To chkTitleBar.UBound
        chkTitleBar(i).Top = 0
        chkTitleBar(i).Left = 0
        chkTitleBar(i).Value = 0
    Next
    For i = 0 To fraToggleRcv.UBound
        fraToggleRcv(i).Visible = False
        fraToggleRcv(i).Top = 0
        fraToggleRcv(i).Height = chkToggleRcvd(0).Height
        fraToggleRcv(i).Width = chkToggleSent(i).Left + chkToggleSent(i).Width
        fraToggleRcv(i).BackColor = MyOwnPanelColor
    Next
    NewTop = TitlePanel(0).Top + TitlePanel(0).Height + 15
    For i = 0 To ListPanel.UBound
        ListPanel(i).BackColor = MyOwnPanelColor
        ListPanel(i).Top = NewTop
        ListPanel(i).Left = TitlePanel(i).Left
    Next
    For i = 0 To ListButtons.UBound
        ListButtons(i).BackColor = MyOwnPanelColor
        ListButtons(i).Left = 0
        ListButtons(i).Top = 0
        ListButtons(i).Height = chkOnlineRefresh(0).Height
        ListButtons(i).Visible = True
    Next
    For i = 0 To fraToggleCnt.UBound
        fraToggleCnt(i).Visible = False
        fraToggleCnt(i).Top = 0
        fraToggleCnt(i).Height = chkRcvCnt(0).Height
        fraToggleCnt(i).Width = chkShowTlx(0).Left + chkShowTlx(0).Width
    Next
    For i = 0 To ListGroup.UBound
        ListGroup(i).BackColor = MyOwnPanelColor
        ListGroup(i).Top = 0
        ListGroup(i).Left = 0
    Next
    NewTop = ListButtons(2).Top + ListButtons(2).Height
    For i = 0 To tabTelexList.UBound
        tabTelexList(i).Left = 0
        tabTelexList(i).Top = NewTop
    Next
    For i = 0 To tabAssign.UBound
        tabAssign(i).Left = 0
        tabAssign(i).Top = NewTop
    Next
    For i = 0 To tabFlightList.UBound
        tabFlightList(i).Left = 0
        tabFlightList(i).Top = NewTop
    Next
    For i = 0 To AreaCover.UBound
        AreaCover(i).Left = 0
        AreaCover(i).Top = NewTop
        'AreaCover(i).BackColor = MyOwnPanelColor
        AreaCover(i).BackColor = vbWhite
        AreaCover(i).Visible = False
    Next
    
    For i = 0 To 3
        tabTelexList(i).ResetContent
        tabTelexList(i).FontName = "Courier New"
        tabTelexList(i).HeaderFontSize = MyFontSize
        tabTelexList(i).FontSize = MyFontSize
        tabTelexList(i).lineHeight = MyFontSize
        tabTelexList(i).SetTabFontBold MyFontBold
        tabTelexList(i).LogicalFieldList = "STAT,WSTA,TTYP,STYP,TIME,MSNO,PFRL,URNO,INDX"
        tabTelexList(i).HeaderString = "S,W,Type,ST,Time,No,Text Extract                      ,URNO,INDEX"
        'tabTelexList(i).HeaderString = CStr(i) & ",W,Type,ST,Time,No,Text Extract                      ,URNO,INDEX"
        tabTelexList(i).HeaderLengthString = "1,1,4,2,5,3,200,10,10"
        tabTelexList(i).ShowHorzScroller True
        tabTelexList(i).LifeStyle = True
        tabTelexList(i).DateTimeSetColumn 4
        tabTelexList(i).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
        tabTelexList(i).DateTimeSetOutputFormatString 4, "hh':'mm"
        tabTelexList(i).AutoSizeByHeader = True
        If i < 2 Then
            tabTelexList(i).Height = (((ipLines + 1) * tabTelexList(i).lineHeight) + 6) * Screen.TwipsPerPixelY
        End If
    Next
        
    For i = 0 To 1
        tabFlightList(i).ResetContent
        tabFlightList(i).FontName = "Courier New"
        tabFlightList(i).HeaderFontSize = MyFontSize
        tabFlightList(i).FontSize = MyFontSize
        tabFlightList(i).lineHeight = MyFontSize
        tabFlightList(i).SetTabFontBold MyFontBold
        tabFlightList(i).AutoSizeByHeader = True
        tabFlightList(i).HeaderString = "TLX,S,Flight,Date/Time,I,D,Reg.No,A/C,APC,Via,Time Status,S,FIDS,Remark 1,Remark 2,URNO,RKEY"
        tabFlightList(i).HeaderLengthString = "10,10,10,10,10,-1,10,10,10,10,10,10,10,10,10,10,10"
        tabFlightList(i).ColumnWidthString = "3,1,9,14,1,1,12,3,3,3,14,1,5,20,20,10,10"
        tabFlightList(i).ColumnAlignmentString = "R,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L,L"
        tabFlightList(i).DateTimeSetColumn 3
        tabFlightList(i).DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
        tabFlightList(i).DateTimeSetOutputFormatString 3, "DDMMMYY'/'hh':'mm"
        tabFlightList(i).DateTimeSetColumn 10
        tabFlightList(i).DateTimeSetInputFormatString 10, "YYYYMMDDhhmmss"
        tabFlightList(i).DateTimeSetOutputFormatString 10, "DDMMMYY'/'hh':'mm"
        tabFlightList(i).LifeStyle = True
        tabFlightList(i).CursorLifeStyle = True
        tabFlightList(i).ShowHorzScroller True
        tabFlightList(i).AutoSizeByHeader = True
        tabFlightList(i).AutoSizeColumns
        tabFlightList(i).Refresh
    Next i
    
    tabFlightList(2).ResetContent
    tabFlightList(2).FontName = "Courier New"
    tabFlightList(2).HeaderFontSize = MyFontSize
    tabFlightList(2).FontSize = MyFontSize
    tabFlightList(2).lineHeight = MyFontSize
    tabFlightList(2).SetTabFontBold MyFontBold
    tabFlightList(2).AutoSizeByHeader = True
    tabFlightList(2).LogicalFieldList = "ADID,FLNO,FDAT,URNO,TURN,FURN,DATC"
    tabFlightList(2).HeaderString = "T,Flight   ,Date/Time    "
    tabFlightList(2).HeaderLengthString = "10,10,10"
    tabFlightList(2).ColumnWidthString = "1,9,7"
    tabFlightList(2).ColumnAlignmentString = "L,L,L"
    
'    tabFlightList(2).DateTimeSetColumn 3
'    tabFlightList(2).DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
'    tabFlightList(2).DateTimeSetOutputFormatString 3, "DDMMMYY'/'hh':'mm"
'    tabFlightList(2).DateTimeSetColumn 10
'    tabFlightList(2).DateTimeSetInputFormatString 10, "YYYYMMDDhhmmss"
'    tabFlightList(2).DateTimeSetOutputFormatString 10, "DDMMMYY'/'hh':'mm"

    tabFlightList(2).LifeStyle = True
    tabFlightList(2).CursorLifeStyle = True
    tabFlightList(2).ShowHorzScroller True
    tabFlightList(2).AutoSizeByHeader = True
    tabFlightList(2).AutoSizeColumns
    tabFlightList(2).SetColumnCount 7
    tabFlightList(2).EnableInlineEdit True
    tabFlightList(2).Refresh
    
    tabFlightList(0).Width = AreaPanel(3).Width - 75
    tabFlightList(1).Left = tabFlightList(0).Left
    tabFlightList(1).Top = tabFlightList(0).Top
    tabFlightList(2).Left = tabFlightList(0).Left
    tabFlightList(2).Top = tabFlightList(0).Top
    For i = 0 To tabFlightList.UBound
        tabFlightList(i).Visible = True
    Next
    tabFlightList(1).ZOrder
    
    fraSplitter(2).Top = tabTelexList(0).Height + ListButtons(0).Height
    
    fraSplitter(3).Left = fraListPanel.Width - FlightPanelSize - fraSplitter(3).Width - 120
    
    'fraSplitter(0).Left = ((fraListPanel.Width - fraSplitter(0).Width) / 2) + 150
    fraSplitter(0).Left = (fraListPanel.Width - AssignPanelSize) - fraSplitter(0).Width - 120
    
    fraSplitter(2).Width = ListPanel(3).Left + ListPanel(3).Width - ListPanel(2).Left
    fraSplitter(1).Width = fraSplitter(0).Width + 60
    fraSplitter(1).Left = fraSplitter(0).Left - 30
    fraSplitter(1).Top = fraSplitter(2).Top
    fraSplitter(1).Height = fraSplitter(2).Height
    fraSplitter(4).Width = fraSplitter(3).Width + 60
    fraSplitter(4).Left = fraSplitter(3).Left - 30
    fraSplitter(4).Top = fraSplitter(2).Top
    fraSplitter(4).Height = fraSplitter(2).Height
    
    txtTelexText.Left = 60
    txtTelexText.Top = 0
    txtTelexText.FontSize = MyFontSize - 5
    txtTelexText.FontBold = MyFontBold
    
    RightCover.Top = 0
    tabTelexList(2).AutoSizeColumns
    tabTelexList(3).AutoSizeColumns
    
    tabAssign(0).ResetContent
    tabAssign(0).HeaderString = "Folder Name,Date   ,Remark                "
    tabAssign(0).LogicalFieldList = "FNAM,FDAT,FREM,URNO,TURN,FTYP"
    tabAssign(0).FontName = "Courier New"
    tabAssign(0).HeaderFontSize = MyFontSize
    tabAssign(0).FontSize = MyFontSize
    tabAssign(0).lineHeight = MyFontSize
    tabAssign(0).SetTabFontBold MyFontBold
    tabAssign(0).HeaderLengthString = "20,10,128,10,10,1"
    tabAssign(0).ColumnWidthString = "8,7,32"
    tabAssign(0).ColumnAlignmentString = "L,L,L"
    tabAssign(0).LifeStyle = True
    tabAssign(0).CursorLifeStyle = True
    tabAssign(0).ShowHorzScroller True
    'tabAssign(0).SetColumnBoolProperty 0, "1", "0"
    tabAssign(0).AutoSizeByHeader = True
    tabAssign(0).AutoSizeColumns
    tabAssign(0).NoFocusColumns = "3,4,5"
    tabAssign(0).SetColumnCount 6
    tabAssign(0).PostEnterBehavior = 3
    tabAssign(0).EnableInlineEdit True
    tabAssign(0).Refresh
    
    ResizeTabs
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    SetMouseWheelObjectCode "MAIN"
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            If Not OpenedFromFips Then
                Cancel = CheckCancelExit(Me, 1)
            Else
                chkExit.Value = 1
            End If
            'Must be set!!
            Cancel = True
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    Dim NewLeft As Long
    Dim MaxSize As Long
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim SizeDiff As Long
    Dim NewPanelTop As Long
    Dim MinFolderHeight As Long
    Dim MinFolderWidth As Long
    Dim NewFolderHeight As Long
    Dim NewFolderBottom As Long
    Dim NewFolderWidth As Long
    On Error Resume Next
    If (PrevWindowState = vbNormal) And (Me.WindowState = vbNormal) And (Not SystemResizing) Then LastResizeName = Me.Name
    
    If Me.WindowState <> vbMinimized Then
        NewLeft = Me.ScaleWidth - RightCover.Width
        'If Not AreaPanel(3).Visible Then fraSplitter(3).Left = NewLeft
        MainButtons.Width = NewLeft - MainButtons.Left
        If Me.ScaleHeight > MyStatusBar.Height + 120 Then
            NewLeft = Me.ScaleWidth - ActionPanel.Width
            ActionPanel.Left = NewLeft
            If Not ActionPanel.Visible Then NewLeft = Me.ScaleWidth
            RightCover.Left = NewLeft - RightCover.Width
            RightCover.Refresh
            fraFolderToggles.Left = RightCover.Left
            RightCover.Height = Me.ScaleHeight - MyStatusBar.Height
            ActionPanel.Height = RightCover.Height
            NewTop = ActionPanel.ScaleHeight - LogPanel(2).Height - 90
            LogPanel(2).Top = NewTop
            NewTop = NewTop - LogPanel(1).Height - 120
            LogPanel(1).Top = NewTop
            NewHeight = NewTop - LogPanel(0).Top - 120
            If NewHeight > 300 Then LogPanel(0).Height = NewHeight
            AdjustBcLogScroll
            
            FolderButtons(0).Left = RightCover.Left
            InfoPanel.Top = TelexPoolHead.ScaleHeight - InfoPanel.Height - MyStatusBar.Height - 120
            TestPanel.Top = InfoPanel.Top - TestPanel.Height - 60
            If TestPanel.Visible Then
                basicButtons.Top = TestPanel.Top - basicButtons.Height - 60
            Else
                basicButtons.Top = InfoPanel.Top - basicButtons.Height - 60
            End If
            If basicButtons.Top < TopButtons.Top + TopButtons.Height Then
                TopButtons.BackColor = vbBlack
            Else
                TopButtons.BackColor = vbButtonFace
            End If
            MinFolderWidth = 1500
            NewFolderWidth = RightCover.Left - UfisFolder.Left * 2 - 15
            If NewFolderWidth > MinFolderWidth Then
                SizeDiff = NewFolderWidth - UfisFolder.Width
                UfisFolder.Width = UfisFolder.Width + SizeDiff
                fraListPanel.Width = fraListPanel.Width + SizeDiff
            End If
            NewPanelTop = ArrangeFolderTabs(fraListPanel.Top)
            UfisFolder.Height = NewPanelTop
            NewPanelTop = UfisFolder.Top + UfisFolder.Height
            FipsPanel.Left = 0
            fraListPanel.Top = NewPanelTop
            'FolderButtons(0).Top = fraListPanel.Top
            FipsPanel.Width = fraListPanel.Width + 30
            FipsPanel.Top = Me.ScaleHeight - MyStatusBar.Height - FipsPanel.Height
            NewFolderHeight = Me.ScaleHeight - MyStatusBar.Height - fraListPanel.Top ' - 30
            If FipsPanel.Visible Then NewFolderHeight = NewFolderHeight - FipsPanel.Height
            If NewFolderHeight > 300 Then
                    fraListPanel.Height = NewFolderHeight
            End If
            CedaBusyFlag.Top = TelexPoolHead.ScaleHeight - MyStatusBar.Height + 30
            CedaBusyFlag.Left = TelexPoolHead.ScaleWidth - CedaBusyFlag.Width - (12 * 15)
            If FixWin.Value = 1 Then InitPosSize True
        End If
        NewLeft = RightCover.Left - chkExit.Width - 30
        If NewLeft < Val(chkExit.Tag) Then chkExit.Left = NewLeft Else chkExit.Left = Val(chkExit.Tag)
        ResizeTabs
        ArrangeFolderButtons "RESIZE"
        chkExit.ZOrder
        Me.Refresh
    End If
End Sub
Private Sub ResizeTabs()
    Dim AreaTop(0 To 6) As Long
    Dim AreaLeft(0 To 6) As Long
    Dim AreaHeight(0 To 6) As Long
    Dim AreaWidth(0 To 6) As Long
    Dim idx As Integer
    On Error Resume Next
    
    If Not MainFormIsReadyForUse Then Exit Sub
    If Me.Visible = False Then Exit Sub
    
    If (Not fraSplitter(0).Visible) And ((AreaPanel(0).Visible) Or (AreaPanel(1).Visible)) Then
        If AreaPanel(2).Visible Then
            fraSplitter(0).Left = (fraListPanel.Width / 2) - 60
            fraSplitter(1).Left = fraSplitter(1).Left - 30
            fraSplitter(0).Visible = True
            fraSplitter(1).Visible = True
        End If
    End If
    If (Not fraSplitter(0).Visible) And (AreaPanel(2).Visible) Then
        If (AreaPanel(0).Visible) Or (AreaPanel(1).Visible) Then
            fraSplitter(0).Left = (fraListPanel.Width / 2) - 60
            fraSplitter(1).Left = fraSplitter(1).Left - 30
            fraSplitter(0).Visible = True
            fraSplitter(1).Visible = True
        End If
    End If
    If (Not fraSplitter(0).Visible) And (Not AreaPanel(2).Visible) Then
        If (AreaPanel(0).Visible) And (AreaPanel(1).Visible) Then
            fraSplitter(0).Left = (fraListPanel.Width / 2) - 60
            fraSplitter(1).Left = fraSplitter(1).Left - 30
            fraSplitter(0).Visible = True
            fraSplitter(1).Visible = True
        End If
    End If
    If (Not fraSplitter(3).Visible) And (AreaPanel(3).Visible) Then
        fraSplitter(3).Left = (fraListPanel.Width / 2) - 60
        fraSplitter(4).Left = fraSplitter(3).Left - 30
        fraSplitter(3).Visible = True
        fraSplitter(4).Visible = True
    End If
    fraSplitter(0).Top = 180 '+ TitlePanel(0).Height + 30
    fraSplitter(0).Height = fraSplitter(2).Top - fraSplitter(0).Top + 60
    fraSplitter(3).Top = fraSplitter(2).Top + fraSplitter(2).Height  '+ TitlePanel(0).Height + 30
    fraSplitter(3).Height = fraListPanel.Height - fraSplitter(3).Top + 60
    fraSplitter(2).Left = 60
    fraSplitter(2).Width = fraListPanel.Width - fraSplitter(2).Left - 75
    For idx = 0 To fraSplitter.UBound
        fraSplitter(idx).Refresh
    Next
    fraListPanel.Refresh
    If Not AreaPanel(3).Visible Then
        fraSplitter(3).Left = fraListPanel.Width - 60
        fraSplitter(3).Visible = False
        fraSplitter(4).Visible = False
    End If
    If (Not AreaPanel(1).Visible) And (Not AreaPanel(2).Visible) Then
        fraSplitter(0).Left = fraListPanel.Width - 60
        fraSplitter(0).Visible = False
        fraSplitter(1).Visible = False
    End If
    If (Not AreaPanel(0).Visible) And (Not AreaPanel(2).Visible) Then
        fraSplitter(0).Left = 60 - fraSplitter(0).Width
        fraSplitter(0).Visible = False
        fraSplitter(1).Visible = False
    End If
    AreaTop(0) = 180
    AreaLeft(0) = 60
    AreaWidth(0) = fraSplitter(0).Left - AreaLeft(0)
    If AreaWidth(0) < 210 Then AreaWidth(0) = 210
    If Not AreaMaximized(0) Then
        AreaHeight(0) = fraSplitter(2).Top - AreaTop(0)
    Else
        AreaHeight(0) = fraListPanel.Height - AreaTop(0) - 60
    End If
    If AreaHeight(0) < 210 Then AreaHeight(0) = 210
    
    If Not MainIsDeploy Then
        AreaTop(1) = 180
        AreaLeft(1) = fraSplitter(0).Left + fraSplitter(0).Width
        AreaWidth(1) = fraListPanel.Width - AreaLeft(1) - 60
        If AreaWidth(1) < 210 Then AreaWidth(1) = 210
        AreaHeight(1) = fraSplitter(2).Top - AreaTop(1)
        If AreaHeight(1) < 210 Then AreaHeight(1) = 210
    Else
        AreaTop(1) = AreaTop(0)
        AreaLeft(1) = AreaLeft(0)
        AreaWidth(1) = AreaWidth(0)
        AreaHeight(1) = AreaHeight(0)
    End If
    
    AreaTop(2) = 180
    AreaLeft(2) = fraSplitter(0).Left + fraSplitter(0).Width
    AreaWidth(2) = fraListPanel.Width - AreaLeft(2) - 60
    If AreaWidth(2) < 210 Then AreaWidth(2) = 210
    
    If Not AreaMaximized(2) Then
        AreaHeight(2) = fraSplitter(2).Top - AreaTop(2)
    Else
        AreaHeight(2) = fraListPanel.Height - AreaTop(2) - 60
    End If
    If AreaHeight(2) < 210 Then AreaHeight(2) = 210
    
    If MeIsVisible = True Then
        MisCfgPanel(0).Top = AreaTop(0)
        If (Not AreaPanel(2).Visible) Then
            If (Not AreaPanel(0).Visible) And (Not AreaPanel(1).Visible) Then
                MisCfgPanel(0).Left = AreaLeft(2)
                MisCfgPanel(0).Width = AreaWidth(2)
                MisCfgPanel(0).Height = AreaHeight(2)
                MisCfgPanel(0).Visible = True
                TigerTimer.Tag = "10"
                TigerTimer.Enabled = True
            Else
                MisCfgPanel(0).Visible = False
                TigerTimer.Tag = "OFF"
            End If
        Else
            If (Not AreaPanel(0).Visible) And (Not AreaPanel(1).Visible) Then
                MisCfgPanel(0).Left = AreaLeft(0)
                MisCfgPanel(0).Width = AreaWidth(0)
                MisCfgPanel(0).Height = AreaHeight(0)
                MisCfgPanel(0).Visible = True
                TigerTimer.Tag = "10"
                TigerTimer.Enabled = True
            Else
                MisCfgPanel(0).Visible = False
                TigerTimer.Tag = "OFF"
            End If
        End If
        TigerX(0).Top = MisCfgPanel(0).ScaleHeight - TigerX(0).Height
    End If
    
    If Not AreaMaximized(3) Then
        AreaTop(3) = fraSplitter(2).Top + fraSplitter(2).Height
        AreaLeft(3) = fraSplitter(3).Left + fraSplitter(3).Width
        AreaWidth(3) = fraListPanel.Width - AreaLeft(3) - 60
        If AreaWidth(3) < 210 Then AreaWidth(3) = 210
        AreaHeight(3) = fraListPanel.Height - AreaTop(3) - 60
        If AreaHeight(3) < 210 Then AreaHeight(3) = 210
    Else
        AreaTop(3) = AreaTop(0)
        AreaLeft(3) = fraSplitter(3).Left + fraSplitter(3).Width
        AreaWidth(3) = fraListPanel.Width - AreaLeft(3) - 60
        AreaHeight(3) = fraListPanel.Height - AreaTop(3) - 60
        If AreaHeight(3) < 210 Then AreaHeight(3) = 210
    End If
    
    If Not AreaMaximized(4) Then
        AreaTop(4) = fraSplitter(2).Top + fraSplitter(2).Height
        AreaLeft(4) = 60
        AreaWidth(4) = fraSplitter(3).Left - AreaLeft(4)
        AreaHeight(4) = fraListPanel.Height - AreaTop(4) - 60
        If AreaHeight(4) < 210 Then AreaHeight(4) = 210
    Else
        AreaTop(4) = AreaTop(0)
        AreaLeft(4) = 60
        AreaWidth(4) = fraSplitter(3).Left - AreaLeft(4)
        AreaHeight(4) = fraListPanel.Height - AreaTop(4) - 60
    End If
    
    For idx = 0 To AreaPanel.UBound
        AreaPanel(idx).Top = AreaTop(idx)
        AreaPanel(idx).Left = AreaLeft(idx)
        AreaPanel(idx).Width = AreaWidth(idx)
        AreaPanel(idx).Height = AreaHeight(idx)
        AreaWidth(idx) = AreaPanel(idx).ScaleWidth
        If AreaWidth(idx) < 600 Then AreaWidth(idx) = 600
        AreaHeight(idx) = AreaPanel(idx).ScaleHeight - ListPanel(idx).Top
        If AreaHeight(idx) < 210 Then AreaHeight(idx) = 210
        TitlePanel(idx).Width = AreaWidth(idx)
        chkTitleMax(idx).Left = AreaWidth(idx) - chkTitleMax(idx).Width - 15
        If fraToggleRcv(idx).Visible Then
            fraToggleRcv(idx).Left = chkTitleMax(idx).Left - fraToggleRcv(idx).Width - 15
            chkTitleBar(idx).Width = fraToggleRcv(idx).Left - chkTitleBar(idx).Left - 15
        Else
            chkTitleBar(idx).Width = chkTitleMax(idx).Left - chkTitleBar(idx).Left - 15
        End If
        TitlePanel(idx).Refresh
        ListPanel(idx).Width = AreaWidth(idx)
        ListPanel(idx).Height = AreaHeight(idx)
        AreaWidth(idx) = ListPanel(idx).ScaleWidth
        AreaHeight(idx) = ListPanel(idx).ScaleHeight
    Next
    For idx = 0 To AreaCover.UBound
        AreaCover(idx).Width = AreaWidth(idx)
        AreaCover(idx).Height = AreaHeight(idx) - AreaCover(idx).Top
    Next
    If txtTelexText.Top <> 0 Then txtTelexText.Top = 0
    If txtTelexText.Left <> 0 Then txtTelexText.Left = 0
    If txtTelexText.Width <> AreaWidth(4) Then txtTelexText.Width = AreaWidth(4)
    If txtTelexText.Height <> AreaHeight(4) Then txtTelexText.Height = AreaHeight(4)
    txtTelexText.Refresh
    AreaPanel(4).Refresh
    
    AreaHeight(0) = ListPanel(0).ScaleHeight
    AreaHeight(1) = ListPanel(1).ScaleHeight
    AreaHeight(2) = AreaHeight(0)
    AreaHeight(3) = AreaHeight(1)
    AreaHeight(4) = ListPanel(2).ScaleHeight
    AreaHeight(5) = ListPanel(3).ScaleHeight
    
    AreaWidth(0) = ListPanel(0).ScaleWidth
    AreaWidth(1) = ListPanel(1).ScaleWidth
    AreaWidth(2) = AreaWidth(0)
    AreaWidth(3) = AreaWidth(1)
    AreaWidth(4) = ListPanel(2).ScaleWidth
    AreaWidth(5) = ListPanel(3).ScaleWidth
    
    For idx = 0 To ListGroup.UBound
        ListGroup(idx).Height = AreaHeight(idx)
        ListGroup(idx).Width = AreaWidth(idx)
        AreaHeight(idx) = ListGroup(idx).ScaleHeight
        AreaWidth(idx) = ListGroup(idx).ScaleWidth
    Next
    For idx = 0 To ListButtons.UBound
        ListButtons(idx).Width = AreaWidth(idx)
    Next
    For idx = 0 To fraToggleCnt.UBound
        fraToggleCnt(idx).Left = ListButtons(idx).Width - fraToggleCnt(idx).Width
    Next
    For idx = 0 To tabTelexList.UBound
        tabTelexList(idx).Height = AreaHeight(idx) - tabTelexList(idx).Top
        tabTelexList(idx).Width = AreaWidth(idx)
        tabTelexList(idx).Refresh
    Next
    For idx = 0 To tabAssign.UBound
        tabAssign(idx).Height = AreaHeight(4) - tabAssign(idx).Top
        tabAssign(idx).Width = AreaWidth(4)
        tabAssign(idx).Refresh
    Next
    For idx = 0 To tabFlightList.UBound
        tabFlightList(idx).Height = AreaHeight(5) - tabFlightList(idx).Top
        tabFlightList(idx).Width = AreaWidth(5)
        tabFlightList(idx).Refresh
    Next
    fraListPanel.Refresh
End Sub
Private Function ArrangeFolderTabs(LastPanelTop As Long) As Long
    Dim VisibleTabs As Integer
    Dim CurTabList As String
    Dim NewTabList As String
    Dim NewSize As Long
    Dim NewTop As Long
    Dim NewPanelTop As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MaxRight As Long
    Dim TabsCount As Integer
    Dim RowCount As Integer
    Dim LastVisible As Integer
    Dim TabIsVisible As Boolean
    Dim i As Integer
    NewPanelTop = LastPanelTop
    If MySetUp.FolderMode.Value = 1 Then
        NewTabList = ""
        CurTabList = chkScroll.Tag
        NewSize = UfisFolder.Width - 120
        VisibleTabs = Int(NewSize / fraFolder(0).Width)
        If (VisibleTabs >= 2) Or (Not fraFolder(0).Visible) Then
            LastVisible = -1
            TabsCount = 0
            RowCount = 0
            NewTop = fraFolder(0).Top
            NewLeft = 90
            NewTabList = ""
            MaxRight = UfisFolder.Width + 45
            'For i = 0 To fraFolder.UBound
            For i = 0 To AllTypesCol
                TabIsVisible = False
                If chkScroll.Value = 0 Then
                    If (i >= FolderScroll.Value) And (RowCount < MaxFolderRows) Then TabIsVisible = True
                Else
                    If GetRealItem(CurTabList, CLng(i), ",") = "H" Then TabIsVisible = True
                End If
                If TabIsVisible Then
                    fraFolder(i).Top = NewTop
                    fraFolder(i).Left = NewLeft
                    fraFolder(i).Visible = True
                    If chkScroll.Value = 0 Then NewTabList = NewTabList & "V,"
                    LastVisible = i
                    NewLeft = NewLeft + fraFolder(i).Width
                    MaxLeft = MaxRight - fraFolder(i).Width
                    If NewLeft > MaxLeft Then
                        RowCount = RowCount + 1
                        NewLeft = 90
                        NewTop = fraFolder(i).Top + fraFolder(i).Height '+ 15
                        TabsCount = 0
                    End If
                    TabsCount = TabsCount + 1
                    'If TabsCount >= VisibleTabs Then
                    '    RowCount = RowCount + 1
                    '    NewLeft = 90
                    '    NewTop = fraFolder(i).Top + fraFolder(i).Height + 15
                    '    TabsCount = 0
                    'End If
                Else
                    fraFolder(i).Visible = False
                    If chkScroll.Value = 0 Then NewTabList = NewTabList & "H,"
                End If
            Next
            If LastVisible >= 0 Then
                NewPanelTop = fraFolder(LastVisible).Top + fraFolder(LastVisible).Height
            End If
        End If
        If NewTabList <> "" Then
            chkScroll.Tag = NewTabList
            If InStr(NewTabList, "H") > 0 Then chkScroll.Enabled = True Else chkScroll.Enabled = False
        End If
        'fraFolderLine(1).ZOrder
        'fraFolderLine(2).ZOrder
        NewPanelTop = NewPanelTop + 60
    End If
    ArrangeFolderTabs = NewPanelTop
End Function

Private Sub fraFolderToggles_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    SetMouseWheelObjectCode "MAIN.UFIS.FOLDER"
End Sub

Private Sub fraSplitter_Click(Index As Integer)
    fraSplitter(Index).Value = 0
End Sub

Private Sub fraSplitter_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    MouseIsDown = True
    fraSplitter(Index).BackColor = DarkGreen
    fraSplitter(0).Tag = CStr(x)
    fraSplitter(2).Tag = CStr(y)
End Sub

Private Sub fraSplitter_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim ilDiffX As Long
    Dim ilDiffY As Long
    Dim OldX As Long
    Dim OldY As Long
    Dim LeftWidth As Long
    Dim RightWidth As Long
    Dim topHeight As Long
    Dim bottomHeight As Long
    Dim ilDone As Boolean
    If MouseIsDown Then
        OldX = Val(fraSplitter(0).Tag)
        OldY = Val(fraSplitter(2).Tag)
        If Button > 0 Then
            If Index >= 3 Then
                If (OldX > 0) And (x <> OldX) Then
                    ilDiffX = x - OldX
                    If ilDiffX <> 0 Then
                        'If Not AreaMaximized(0) Then
                            LeftWidth = AreaPanel(4).Width + ilDiffX
                            RightWidth = (fraListPanel.Width - fraSplitter(3).Left) - fraSplitter(3).Width - ilDiffX
                            If (LeftWidth >= 900) And (RightWidth >= 900) Then
                                fraSplitter(3).Left = fraSplitter(3).Left + ilDiffX
                                fraSplitter(4).Left = fraSplitter(3).Left - 30
                                If VSplitterSync Then
                                    fraSplitter(0).Left = fraSplitter(3).Left
                                    fraSplitter(1).Left = fraSplitter(4).Left
                                End If
                                ResizeTabs
                                ilDone = True
                            End If
                        'End If
                    End If
                End If
            End If
            If Index <= 1 Then
                If (OldX > 0) And (x <> OldX) Then
                    ilDiffX = x - OldX
                    If ilDiffX <> 0 Then
                        'If Not AreaMaximized(0) Then
                            LeftWidth = AreaPanel(0).Width + ilDiffX
                            RightWidth = (fraListPanel.Width - fraSplitter(0).Left) - fraSplitter(0).Width - 60 - ilDiffX
                            If (LeftWidth >= 900) And (RightWidth >= 900) Then
                                fraSplitter(0).Left = fraSplitter(0).Left + ilDiffX
                                fraSplitter(1).Left = fraSplitter(0).Left - 30
                                If VSplitterSync Then
                                    fraSplitter(3).Left = fraSplitter(0).Left
                                    fraSplitter(4).Left = fraSplitter(1).Left
                                End If
                                ResizeTabs
                                ilDone = True
                            End If
                        'End If
                    End If
                End If
            End If
            If (Index >= 1) And (Index <> 3) Then
                If OldY > 0 And y <> OldY Then
                    ilDiffY = y - OldY
                    If ilDiffY <> 0 Then
                        bottomHeight = AreaPanel(4).Height - ilDiffY
                        topHeight = fraListPanel.Height - AreaPanel(0).Top - fraSplitter(2).Height - bottomHeight - 60
                        If (topHeight >= 1200) And (bottomHeight >= 1200) Then
                            fraSplitter(1).Top = fraSplitter(1).Top + ilDiffY
                            fraSplitter(4).Top = fraSplitter(1).Top
                            fraSplitter(2).Top = fraSplitter(2).Top + ilDiffY
                            ResizeTabs
                            ilDone = True
                        End If
                    End If
                End If
            End If
        End If
        If Button < 0 Then ilDone = True
'        If (chkShowTelex(0).Value = 1) And (ilDone = True) Then
'            If Index = 0 Then
'                ilDiffX = fraSplitter(0).Left - fraSplitter(3).Left
'                If ilDiffX <> 0 Then
'                    fraSplitter(3).Left = fraSplitter(3).Left + ilDiffX
'                    fraSplitter(4).Left = fraSplitter(3).Left - 30
'                    ResizeTabs
'                End If
'            End If
'            If Index = 3 Then
'                ilDiffX = fraSplitter(3).Left - fraSplitter(0).Left
'                If ilDiffX <> 0 Then
'                    fraSplitter(0).Left = fraSplitter(0).Left + ilDiffX
'                    fraSplitter(1).Left = fraSplitter(1).Left + ilDiffX
'                    ResizeTabs
'                End If
'            End If
'        End If
    End If
End Sub
Private Sub fraSplitter_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    MouseIsDown = False
    fraSplitter(Index).BackColor = MyOwnPanelColor
    fraSplitter(0).Tag = "-1"
    fraSplitter(2).Tag = "-1"
End Sub
Public Sub LoadTelexData(MemIdx As Integer, TlxUrno As String, tlxSqlKey As String, TfrSqlKey As String, ChooseFolder As String, ClickOnList As String)
    Dim ReqLoop As Long
    Dim ActSqlKey As String
    Dim UseSqlKey As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim Line As String
    Dim i As Long
    Dim RetCode As Boolean
    Dim TabLineCount As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim tmpErrMsg As String
    Dim sbText1 As String
    Dim sbText2 As String
    Dim tmpLoopTimes As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim SqlTimeBegin
    Dim SqlTimeEnd
    Dim SqlTimeMax
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As Integer
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim TlxUrnoList As String
    Dim count As Integer
    Dim FlnuList As String
    Dim tmpVal As Integer
    Dim tmpFolderCount As Boolean
    
    If MemIdx <= optMem.UBound Then
        MyStatusBar.Panels(1).Text = ""
        MyStatusBar.Panels(2).Text = ""
        sbText1 = MyStatusBar.Panels(1).Text
        sbText2 = MyStatusBar.Panels(2).Text
        If ClickOnList <> "CRT" Then
            If Me.Visible <> True Then Me.Show
            If Me.WindowState = vbMinimized Then Me.WindowState = vbNormal
            Me.Refresh
        End If
        TelexPoolHead.MousePointer = 11
        optMem(MemIdx).Tag = -1
        optMem(MemIdx).Value = True     'Changes global CurMem also
        ResetTlxTypeFolder
        tabTelexList(0).ResetContent
        tabTelexList(1).ResetContent
        tabTelexList(0).Refresh
        tabTelexList(1).Refresh
        txtTelexText.Text = ""
        DataPool.TelexData(MemIdx).ResetContent
        For i = 0 To chkTabs.UBound
            chkTabs(i).Caption = ""
        Next
        NewFilterValues(MemIdx).TlxKey = tlxSqlKey
        NewFilterValues(MemIdx).TfrKey = TfrSqlKey
    Else
        DataPool.TelexData(MemIdx).ResetContent
    End If
    tmpFolderCount = False
    tmpVal = InStr(tlxSqlKey, "TXT1 LIKE")
    If Len(AddFolderList) > 0 And Len(TfrSqlKey) = 0 And Len(ClickOnList) = 0 And tmpVal <= 0 Then
        TlxUrnoList = ""
        tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
        tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
        If tmpDatFrom <> tmpDatTo Then
            tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
        Else
            tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
        End If
        TlxUrnoList = ""
        ActResult = ""
        ActCmd = "RTA"
        ActTable = "TFNTAB"
        ActFldLst = "DISTINCT TURN"
        ActCondition = tmpDatFrom
        ActOrder = ""
        ActDatLst = ""
        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
        If retval >= 0 Then
            count = UfisServer.DataBuffer(0).GetLineCount - 1
            If count >= 0 Then
                For i = 0 To count
                    TlxUrnoList = TlxUrnoList & UfisServer.DataBuffer(0).GetLineValues(i) & ","
                Next
                TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                'TlxUrnoList = "WHERE URNO IN (" & TlxUrnoList & ")"
                tmpFolderCount = True
            End If
        End If
    End If
    tmpFields = DataPool.TlxTabFields.Text
    tmpData = ""
    tmpCmd = "RTA"
    tmpTable = DataPool.TableName.Text
    ActSqlKey = tlxSqlKey
    tmpErrMsg = ""
    Me.Refresh
    For ReqLoop = 1 To 2
        If ActSqlKey <> "" Then
            MyStatusBar.Panels(1).Text = "Loading Data ..."
            If ReqLoop = 1 Then
                fraLoadProgress.Caption = "Loading Telexes ..."
                MyStatusBar.Panels(2).Text = "Valid Telexes ..."
            Else
                MyStatusBar.Panels(2).Text = "Assigned Telexes ..."
                fraLoadProgress.Caption = "Loading Assigned Telexes ..."
            End If
            fraLoadProgress.Refresh
            HiddenMain.SetStatusText 1, UCase(MyStatusBar.Panels(1).Text)
            HiddenMain.SetStatusText 2, UCase(MyStatusBar.Panels(2).Text)
            DataPool.TelexData(MemIdx).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            DataPool.TelexData(MemIdx).CedaHopo = UfisServer.HOPO
            DataPool.TelexData(MemIdx).CedaIdentifier = "IDX"
            DataPool.TelexData(MemIdx).CedaPort = "3357"
            DataPool.TelexData(MemIdx).CedaReceiveTimeout = "250"
            DataPool.TelexData(MemIdx).CedaRecordSeparator = vbLf
            DataPool.TelexData(MemIdx).CedaSendTimeout = "250"
            DataPool.TelexData(MemIdx).CedaServerName = UfisServer.HostName
            DataPool.TelexData(MemIdx).CedaTabext = UfisServer.TblExt
            DataPool.TelexData(MemIdx).CedaUser = gsUserName
            DataPool.TelexData(MemIdx).CedaWorkstation = UfisServer.GetMyWorkStationName
            DataPool.TelexData(MemIdx).CedaPacketSize = 500
            CedaStatus(0).Visible = True
            CedaStatus(0).ZOrder
            CedaStatus(0).Refresh
            If CedaIsConnected Then
                UseSqlKey = GetItem(ActSqlKey, 1, "[LOOPTIMES]")
                tmpLoopTimes = GetItem(ActSqlKey, 2, "[LOOPTIMES]")
                If tmpLoopTimes <> "" Then
                    ActSqlKey = UseSqlKey
                    tmpVpfr = GetItem(tmpLoopTimes, 1, ",")
                    tmpVpto = GetItem(tmpLoopTimes, 2, ",")
                    SqlTimeBegin = CedaFullDateToVb(tmpVpfr)
                    SqlTimeMax = CedaFullDateToVb(tmpVpto)
                    While (SqlTimeBegin <= SqlTimeMax) And (Not StopLoading)
                        If Not StopLoading Then
                            SqlTimeEnd = DateAdd("n", 1439, SqlTimeBegin)
                            If SqlTimeEnd > SqlTimeMax Then SqlTimeEnd = SqlTimeMax
                            tmpVpfr = Format(SqlTimeBegin, "yyyymmddhhmm")
                            tmpVpto = Format(SqlTimeEnd, "yyyymmddhhmm")
                            txtLoadFrom.Text = Format(SqlTimeBegin, MySetUp.DefDateFormat)
                            txtLoadTo.Text = Format(SqlTimeEnd, MySetUp.DefDateFormat)
                            txtLoadTimeFrom.Text = Format(SqlTimeBegin, "hh:mm")
                            txtLoadTimeTo.Text = Format(SqlTimeEnd, "hh:mm")
                            MaxLine = DataPool.TelexData(MemIdx).GetLineCount
                            fraLoadProgress.Caption = CStr(MaxLine) & " Telexes Loaded ..."
                            fraLoadProgress.Refresh
                            'Me.Refresh
                            DoEvents
                            UseSqlKey = ActSqlKey
                            UseSqlKey = Replace(UseSqlKey, "[LOOPVPFR]", tmpVpfr, 1, -1, vbBinaryCompare)
                            UseSqlKey = Replace(UseSqlKey, "[LOOPVPTO]", tmpVpto, 1, -1, vbBinaryCompare)
                            RetCode = DataPool.TelexData(MemIdx).CedaAction(tmpCmd, tmpTable, tmpFields, "", UseSqlKey)
                            If (RetCode = False) And (tmpErrMsg = "") Then tmpErrMsg = tmpErrMsg & DataPool.TelexData(MemIdx).GetLastCedaError
                            Me.Refresh
                            SqlTimeBegin = DateAdd("n", 1, SqlTimeEnd)
                        End If
                    Wend
                Else
                    'Me.Refresh
                    RetCode = DataPool.TelexData(MemIdx).CedaAction(tmpCmd, tmpTable, tmpFields, "", UseSqlKey)
                    If RetCode = False Then tmpErrMsg = tmpErrMsg & DataPool.TelexData(MemIdx).GetLastCedaError
                    'Me.Refresh
                End If
            Else
                If SaveLocalTestData Then
                    If (UfisServer.HostName = "LOCAL") And (ReqLoop = 1) Then
                        DataPool.LoadTestData MemIdx
                    End If
                End If
            End If
            CedaStatus(0).Visible = False
            MyStatusBar.Panels(1).Text = sbText1
            MyStatusBar.Panels(2).Text = sbText2
            Me.Refresh
        End If
        If ReqLoop = 1 Then TabLineCount = DataPool.TelexData(MemIdx).GetLineCount
        ActSqlKey = TfrSqlKey
        If StopLoading Then Exit For
    Next
    If Len(TlxUrnoList) > 0 And CedaIsConnected Then
        TlxUrnoList = CheckTlxUrnolist(TlxUrnoList, MemIdx)
        If Len(TlxUrnoList) > 0 Then
            CedaStatus(0).Visible = True
            CedaStatus(0).ZOrder
            CedaStatus(0).Refresh
            RetCode = DataPool.TelexData(MemIdx).CedaAction(tmpCmd, "TLXTAB", tmpFields, "", TlxUrnoList)
            CedaStatus(0).Visible = False
            CedaStatus(0).ZOrder
            CedaStatus(0).Refresh
        End If
    End If
    If Len(TfrSqlKey) > 0 And CedaIsConnected And chkAssFolder.Enabled Then
        FlnuList = GetFlnuList(MemIdx)
        If Len(FlnuList) = 0 And MemIdx = 1 And Len(AftUrnoFromFips) > 0 Then
            FlnuList = "WHERE FURN = " & AftUrnoFromFips
        End If
        If Len(FlnuList) > 0 Then
            TlxUrnoList = ""
            ActResult = ""
            ActCmd = "RTA"
            ActTable = "TLKTAB"
            ActFldLst = "DISTINCT TURN"
            ActCondition = FlnuList
            ActOrder = ""
            ActDatLst = ""
            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
            If retval >= 0 Then
                count = UfisServer.DataBuffer(0).GetLineCount - 1
                If count >= 0 Then
                    For CurLine = 0 To count
                        tmpData = UfisServer.DataBuffer(0).GetLineValues(CurLine)
                        TlxUrnoList = TlxUrnoList & GetItem(tmpData, 1, ",") & ","
                    Next
                    If TlxUrnoList <> "" Then
                        TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                        TlxUrnoList = CheckTlxUrnolist(TlxUrnoList, MemIdx)
                        If Len(TlxUrnoList) > 0 Then
                            CedaStatus(0).Visible = True
                            CedaStatus(0).ZOrder
                            CedaStatus(0).Refresh
                            RetCode = DataPool.TelexData(MemIdx).CedaAction(tmpCmd, "TLXTAB", tmpFields, "", TlxUrnoList)
                            CedaStatus(0).Visible = False
                            CedaStatus(0).ZOrder
                            CedaStatus(0).Refresh
                        End If
                    End If
                End If
            End If
        End If
    End If
    If MemIdx <> CurMem Then
        If MemIdx <= optMem.UBound Then
            DontRefreshList = True
            optMem(MemIdx).Tag = -1
            optMem(MemIdx).Value = True     'Changes global CurMem also
            DontRefreshList = False
            'CheckLoadButton False
        End If
    End If
    If Not StopLoading Then
        If tmpErrMsg <> "" Then
            tmpErrMsg = Replace(tmpErrMsg, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
            MyMsgBox.CallAskUser 0, 0, 0, "Server Access Report", tmpErrMsg, "stop", "", UserAnswer
        End If
        If UfisServer.HostName <> "LOCAL" Then
            If SaveLocalTestData Then DataPool.SaveTestData MemIdx
        End If
        If MemIdx <= optMem.UBound Then
            MaxLine = DataPool.TelexData(MemIdx).GetLineCount
            MyStatusBar.Panels(2).Text = "Preparing Telex Folder View ... "
            fraLoadProgress.Caption = CStr(MaxLine) & " Telexes Loaded ..."
            Me.Refresh
            DoEvents
            MaxLine = MaxLine - 1
            For CurLine = TabLineCount To MaxLine
                DataPool.TelexData(MemIdx).SetLineStatusValue CurLine, 1
            Next
            If optMem(0).Value = False Or tmpFolderCount = False Then
                ReadFolderCount = False
            End If
            If ClickOnList <> "CRT" Then
                PrepareTelexExtract MemIdx, True
                ReadFolderCount = True
                Me.Refresh
                DoEvents
                If ChooseFolder <> "" Then
                    MyStatusBar.Panels(2).Text = "Publishing Telex Folder Data ... "
                    InitFolderByName ChooseFolder, False
                End If
                MyStatusBar.Panels(2).Text = ""
                Me.Refresh
                DoEvents
                If ClickOnList <> "" Then
                    If ClickOnList = "RCV" Then
                        tabTelexList(0).SetCurrentSelection 0
                    ElseIf ClickOnList = "SND" Then
                        tabTelexList(1).SetCurrentSelection 0
                    ElseIf ClickOnList = "CRT" Then
                        tabTelexList(1).SetCurrentSelection 0
                    End If
                Else
                    If tabTelexList(0).GetLineCount > 0 Then
                        tabTelexList(0).SetCurrentSelection 0
                    ElseIf tabTelexList(1).GetLineCount > 0 Then
                        tabTelexList(1).SetCurrentSelection 0
                    End If
                End If
                MaxLine = DataPool.TelexData(MemIdx).GetLineCount
                optMem(MemIdx).Tag = MaxLine
                Select Case optMem(MemIdx).Tag
                    Case Is < 0
                        optMem(MemIdx).ToolTipText = tmpErrMsg
                    Case Is = 0
                        optMem(MemIdx).ToolTipText = "No data found for this filter."
                    Case Else
                        optMem(MemIdx).ToolTipText = optMem(MemIdx).Tag & " Telexes "
                End Select
                StoreFolderValues MemIdx
                ActFilterValues(MemIdx) = NewFilterValues(MemIdx)
                If Left(NewFilterValues(MemIdx).TlxKey, 11) = "WHERE URNO=" Then
                    chkLoad.BackColor = vbButtonFace
                    chkLoad.ForeColor = vbBlack
                End If
                MyStatusBar.Panels(2).Text = ""
            Else
                PrepareTelexExtract MemIdx, False
                MyStatusBar.Panels(2).Text = ""
                fraLoadProgress.Caption = "Ready"
            End If
        End If
    Else
        DataPool.TelexData(MemIdx).ResetContent
        chkLoad.Value = 0
    End If
    Me.Refresh
    TelexPoolHead.MousePointer = 0
End Sub

Private Sub chkTlxTitleBar_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpTabIdx As Integer
    Dim tmpTxtIdx As Integer
    Dim tmpWinIdx As Integer
'    If chkLoad.Value = 0 Then
'        If Index < 2 Then
'            If MainIsDeploy = False Then
'                If MySetUp.SndTabList.Value = 1 Then
'                    chkTlxTitleBar(1 - Index).Value = 1 - chkTlxTitleBar(Index).Value
'                    If chkOnlineRcv.Value = 0 Then
'                        FillTelexTextField (-1)
'                    End If
'                    If MySetUp.FolderMode.Value = 1 Then
'                        If chkTitleBar(0).Value = 0 And bmFolderButtons0 = True Then
'                            FolderButtons(0).Visible = True
'                        Else
'                            FolderButtons(0).Visible = False
'                        End If
'                    End If
'                Else
'                    If MySetUp.FolderMode.Value = 0 Then FolderButtons(0).Top = fraListPanel.Top + 15
'                    If bmFolderButtons0 = True Then FolderButtons(0).Visible = True
'                    If chkTitleBar(0).Value = 1 Then chkTitleBar(0).Value = 0
'                End If
'            Else
'                chkTlxTitleBar(Index).Value = 0
'            End If
'        Else
'            If chkTlxTitleBar(Index).Value = 1 Then
'                tmpTag = chkTlxTitleBar(Index).Tag
'                Select Case Index
'                    Case 2
'                        tmpTxtIdx = 0
'                        tmpWinIdx = 2
'                        If chkOnlineRcv.Value = 0 Then
'                            tmpTabIdx = 0
'                        Else
'                            tmpTabIdx = 2
'                        End If
'                    Case 3
'                        tmpTxtIdx = 1
'                        tmpWinIdx = 3
'                        If chkOnlineRcv.Value = 0 Then
'                            tmpTabIdx = 1
'                        Else
'                            tmpTabIdx = 3
'                        End If
'                    Case Else
'                End Select
'                If tmpTag = "" Then
'                    chkTlxTitleBar(Index).Tag = "+"
'                    chkTlxTitleBar(Index).Caption = " -"
'                    ListPanel(tmpTxtIdx).ZOrder
'                    ListButtons(0).ZOrder
'                    ListButtons(1).ZOrder
'                    tabTelexList(tmpTabIdx).ZOrder
'                    fraSplitter(0).ZOrder
'                    Form_Resize
'                Else
'                    chkTlxTitleBar(Index).Tag = ""
'                    chkTlxTitleBar(Index).Caption = " +"
'                    ResizeTabs
'                    ArrangeTabCursor tabTelexList(tmpTabIdx)
'                End If
'                chkTlxTitleBar(Index).Value = 0
'            End If
'        End If
'    End If
End Sub

Private Sub fraTimeFilter_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    SetMouseWheelObjectCode "MAIN.FRAME.TIME"
End Sub

Private Sub FullText_Click()
    LastTlxUrno(0) = ""
    LastTlxUrno(1) = ""
    LastHitTlxUrno = "NONE"
    If FullText.Value = 1 Then FullText.BackColor = LightGreen Else FullText.BackColor = vbButtonFace
    FillTelexTextField (-1)
End Sub

Private Sub KeybTimer_Timer()
    Dim tmpTag As String
    Dim OldLineNo As Long
    Dim SelLineNo As Long
    Dim MaxLineNo As Long
    Dim idx As Integer
    'Exit Sub
    KeybTimer.Enabled = False
    If LastHitTlxUrno <> "RESET" Then
        tmpTag = KeybTimer.Tag
        If tmpTag <> "CHECKED" Then
            idx = Val(GetItem(tmpTag, 1, ","))
            OldLineNo = Val(GetItem(tmpTag, 2, ","))
            SelLineNo = tabTelexList(idx).GetCurrentSelected
            MaxLineNo = tabTelexList(idx).GetLineCount - 1
            If OldLineNo = SelLineNo Then
                'LastHitTlxUrno = "RESET"
                KeybTimer.Tag = "CHECKED"
                KeybTimer.Enabled = False
                If ((SelLineNo > 0) And (SelLineNo < MaxLineNo)) Or (chkAutoRefresh.Value = 0) Then
                    chkAutoRefresh.Value = 1
                    tabTelexList(idx).SetCurrentSelection SelLineNo
                End If
            End If
        End If
    End If
End Sub

Private Sub LoaInfo_Click(Index As Integer)
    If LoaInfo(Index).Value = 1 Then
        If Trim(CurrentUrno.Text) <> "" Then
            CheckLoaTabViewer CurrentRecord.Text, Index
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Show " & LoaInfo(Index).Caption, "No telex selected.", "hand", "", UserAnswer
        End If
        LoaInfo(Index).Value = 0
    End If
End Sub

Private Sub MsgFromClient_Change()
    Dim tmpData As String
    Dim ImpMsgNum As Long
    Dim ImpPakNum As Long
    Dim tmpVal As Integer
    Dim tmpList As String
    Dim ImpCmd As String
    Dim FldSep As String
    Dim LineNo As Long
    tmpData = MsgFromClient.Text
    If tmpData <> "" Then
        FldSep = Chr(15)
        ImpMsgNum = Val(GetItem(tmpData, 1, FldSep))
        ImpPakNum = Val(GetItem(tmpData, 2, FldSep))
        If ImpPakNum > 0 Then
        Else
            ImpCmd = GetItem(tmpData, 3, FldSep)
            Select Case ImpCmd
                Case "SPOOL"
                Case "ACTIV"
                Case "CLOSE"
                Case "ATSET"
                    RcvInfo.chkImp(0).Value = 1
                Case "ATOFF"
                    RcvInfo.chkImp(0).Value = 0
                Case "AISET"
                    RcvInfo.chkImp(1).Value = 1
                Case "AIOFF"
                    RcvInfo.chkImp(1).Value = 0
                Case "ERROR"
                    tmpList = GetItem(tmpData, 4, FldSep)
                    HandleImpUpdate tmpList, "STAT", "E"
                Case "IMPORTED"
                    tmpList = GetItem(tmpData, 4, FldSep)
                    HandleImpUpdate tmpList, "WSTA", "I"
                Case "MANUALLY"
                    tmpList = GetItem(tmpData, 4, FldSep)
                    HandleImpUpdate tmpList, "WSTA", "M"
                Case "READY"
                    tmpList = GetItem(tmpData, 4, FldSep)
                    
                Case Else
            End Select
            MsgToClient.Text = "GOT:" & GetItem(tmpData, 1, FldSep)
        End If
        'LastMsgNum = ImpMsgNum + 1
    End If
End Sub
Private Sub HandleImpUpdate(UrnoList As String, UseField As String, useValue As String)
    Dim RetCode As Integer
    Dim retval As String
    Dim clSqlKey As String
    Dim tmpUrno As String
    Dim tmpStat As String
    Dim itm As Integer
    itm = 1
    tmpUrno = GetItem(UrnoList, itm, ";")
    While tmpUrno <> ""
        clSqlKey = "WHERE URNO=" & tmpUrno
        RetCode = UfisServer.CallCeda(retval, "URT", DataPool.TableName, UseField, useValue, clSqlKey, "", 0, True, False)
        itm = itm + 1
        tmpUrno = GetItem(UrnoList, itm, ";")
    Wend
End Sub

Private Sub MyStatusBar_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        ShowFdiInfo 2, False
    End If
End Sub

Private Sub MyStatusBar_PanelDblClick(ByVal Panel As MSComctlLib.Panel)
    ShowFdiInfo Panel.Index, True
End Sub

Private Sub ShowFdiInfo(Index As Integer, SsimDate As Boolean)
    Dim tmpTag As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpInfo As String
    tmpTag = MyStatusBar.Panels(Index).Tag
    Select Case Index
        Case 1
            'MsgBox tmpTag
        Case 2
            tmpFields = GetRealItem(tmpTag, 0, "/")
            tmpData = GetRealItem(tmpTag, 1, "/")
            tmpFields = Replace(tmpFields, "#", ",", 1, -1, vbBinaryCompare)
            tmpData = Replace(tmpData, "#", ",", 1, -1, vbBinaryCompare)
            tmpText = PrepareFdiInfo(tmpFields, tmpData, SsimDate)
            If tmpText <> "" Then
                tmpInfo = MyStatusBar.Panels(1).Text
                If Left(tmpInfo, 1) = "(" Then tmpInfo = "(No flight assigned)"
                tmpText = "Updates on Flight" & vbNewLine & tmpInfo & vbNewLine & vbNewLine & tmpText
                MyMsgBox.CallAskUser 0, 0, 0, "Telex Interpreter", tmpText, "hand", "", UserAnswer
            End If
        Case Else
    End Select
End Sub
Private Function PrepareFdiInfo(FieldList As String, DataList As String, SsimDate As Boolean) As String
    Dim Result As String
    Dim CurItm As Long
    Dim CurFld As String
    Dim CurDat As String
    Dim tmpDat As String
    Dim tmpTime
    Result = ""
    CurItm = 0
    CurFld = GetRealItem(FieldList, CurItm, ",")
    While CurFld <> ""
        CurDat = GetRealItem(DataList, CurItm, ",")
        If CurDat <> "" Then
            If InStr(FdiTimeFields, CurFld) > 0 Then
                If SsimDate Then
                    tmpTime = CedaFullDateToVb(CurDat)
                    CurDat = Format(tmpTime, "ddmmmyy \/ hh:mm")
                    CurDat = UCase(CurDat)
                Else
                    tmpDat = Left(CurDat, 4) & "." & Mid(CurDat, 5, 2) & "." & Mid(CurDat, 7, 2) & " / "
                    tmpDat = tmpDat & Mid(CurDat, 9, 2) & ":" & Mid(CurDat, 11, 2)
                    CurDat = tmpDat
                End If
            End If
        Else
            CurDat = "--"
        End If
        Result = Result & CurFld & ": " & CurDat & vbNewLine
        CurItm = CurItm + 1
        CurFld = GetRealItem(FieldList, CurItm, ",")
    Wend
    PrepareFdiInfo = Result
End Function
Private Sub OnLine_Click()
    If OnLine.Value = 1 Then
        OnLine.BackColor = LightGreen
        If MyMainPurpose <> "AUTO_SEND" Then
            If MainFormIsReadyForUse Then
                If PoolConfig.OnLineCfg(1).Value = 1 Then
                    RcvTelex.Show
                    RcvTelex.WindowState = vbNormal
                Else
                    If FormIsVisible("RcvTelex") Then RcvTelex.Hide
                End If
                If PoolConfig.OnLineCfg(2).Value = 1 Then
                    SndTelex.Show
                    SndTelex.WindowState = vbNormal
                Else
                    If FormIsVisible("SndTelex") Then SndTelex.Hide
                End If
                If PoolConfig.OnLineCfg(5).Value = 1 Then
                    PndTelex.Show
                    PndTelex.WindowState = vbNormal
                Else
                    If FormIsVisible("PndTelex") Then PndTelex.Hide
                End If
                If PoolConfig.OnLineCfg(3).Value = 1 Then
                    OutTelex.Show
                    OutTelex.WindowState = vbNormal
                Else
                    If FormIsVisible("OutTelex") Then OutTelex.Hide
                End If
                If PoolConfig.OnLineCfg(4).Value = 1 Then
                    RcvInfo.Show
                    RcvInfo.WindowState = vbNormal
                Else
                    If FormIsVisible("RcvInfo") Then RcvInfo.Hide
                End If
            End If
        End If
        If Not AutoOpenOnline Then
            FixWin.Value = 1
            FixWin.Value = 0
            AutoOpenOnline = True
        End If
    Else
        OnLine.BackColor = vbButtonFace
        If MyMainPurpose <> "AUTO_SEND" Then
            If MainFormIsReadyForUse Then
                If FormIsVisible("RcvTelex") Then RcvTelex.Hide
                If FormIsVisible("SndTelex") Then SndTelex.Hide
                If FormIsVisible("PndTelex") Then PndTelex.Hide
                If FormIsVisible("OutTelex") Then OutTelex.Hide
                If FormIsVisible("RcvInfo") Then RcvInfo.Hide
            End If
        End If
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False, False
    SetAllFormsOnTop True
    If OnTop.Value = 1 Then SetFormOnTop Me, True, True
End Sub

Private Sub optLookAft_Click()
    optLookAft.BackColor = LightestGreen
    optLookTlx.BackColor = vbButtonFace
    If Not ReorgFolders Then CheckLoadButton False
End Sub

Private Sub optLookTlx_Click()
    optLookTlx.BackColor = LightestGreen
    optLookAft.BackColor = vbButtonFace
    If Not ReorgFolders Then CheckLoadButton False
End Sub

Private Sub optMem_Click(Index As Integer)
    On Error Resume Next
    If optMem(Index).Value = True Then
        chkOnlineRcv.Value = 0
        chkLoader(0).Value = 1
        optMem(optMemButtons.Tag).BackColor = vbButtonFace
        optMem(Index).BackColor = LightGreen
        optMem(Index).Refresh
        
        StoreFolderValues CurMem
        CurMem = Index
        RestoreFolderValues CurMem, True
        If txtDateFrom.Text = "" Then InitDateFields "USR"
        optMemButtons.Tag = Index
        If Left(NewFilterValues(Index).TlxKey, 11) = "WHERE URNO=" Then
            chkLoad.BackColor = vbButtonFace
            chkLoad.ForeColor = vbBlack
        Else
            CheckLoadButton False
        End If
    End If
End Sub

Private Sub optLocal_Click()
    Dim Vpfr As String
    Dim Vpto As String
    Dim TimeVal
    optLocal.BackColor = LightestGreen
    optUtc.BackColor = vbButtonFace
    Option1.Value = True
    Option1.BackColor = LightestGreen
    Option2.BackColor = vbButtonFace
    GetValidTimeFrame Vpfr, Vpto, False
    TimeVal = CedaFullDateToVb(Vpfr)
    TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
    txtDateFrom = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeFrom = Format(TimeVal, "hh:mm")
    TimeVal = CedaFullDateToVb(Vpto)
    TimeVal = DateAdd("n", UtcTimeDiff, TimeVal)
    txtDateTo = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeTo = Format(TimeVal, "hh:mm")
    SetTabUtcDiff UtcTimeDiff
    txtLoadFrom.Text = txtDateFrom.Text
    txtLoadTo.Text = txtDateTo.Text
    txtLoadTimeFrom.Text = txtTimeFrom.Text
    txtLoadTimeTo.Text = txtTimeTo.Text
End Sub

Private Sub optTimeType_Click(Index As Integer)
    If optTimeType(Index).Value = True Then
        Select Case Index
            Case 0
                InitDateFields "USR"
            Case 1
                InitDateFields "RCV"
        End Select
    End If
End Sub

Private Sub optTlxAddr_Click()
    If MySetUp.AddrFilter.Value = 1 Then
        optTlxAddr.BackColor = LightestGreen
    Else
        optTlxAddr.BackColor = LightestGreen
    End If
    txtAddr.ToolTipText = "In the Telex Header"
    optTlxText.BackColor = vbButtonFace
    CheckLoadButton False
End Sub

Private Sub optTlxText_Click()
    optTlxText.BackColor = LightestGreen
    optTlxAddr.BackColor = vbButtonFace
    txtAddr.ToolTipText = "Somewhere in the Telex Data"
    CheckLoadButton False
End Sub

Private Sub optUtc_Click()
    Dim Vpfr As String
    Dim Vpto As String
    Dim TimeVal
    optUtc.BackColor = LightestGreen
    optLocal.BackColor = vbButtonFace
    Option2.Value = True
    Option2.BackColor = LightestGreen
    Option1.BackColor = vbButtonFace
    GetValidTimeFrame Vpfr, Vpto, False
    TimeVal = CedaFullDateToVb(Vpfr)
    TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
    txtDateFrom = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeFrom = Format(TimeVal, "hh:mm")
    TimeVal = CedaFullDateToVb(Vpto)
    TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
    txtDateTo = Format(TimeVal, MySetUp.DefDateFormat)
    txtTimeTo = Format(TimeVal, "hh:mm")
    SetTabUtcDiff 0
    txtLoadFrom.Text = txtDateFrom.Text
    txtLoadTo.Text = txtDateTo.Text
    txtLoadTimeFrom.Text = txtTimeFrom.Text
    txtLoadTimeTo.Text = txtTimeTo.Text
End Sub
Private Sub SetTabUtcDiff(SetDiff As Integer)
    Dim CurDiff As Long
    Dim count As Integer
    Dim idx As Integer
    Dim tmpDat As String
    Dim UseServerTime
    Dim tmpMin As String
    Dim tmpDate As String
    
    CurDiff = SetDiff
    tabTelexList(0).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(0).Refresh
    tabTelexList(1).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(1).Refresh
    tabTelexList(2).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(2).Refresh
    tabTelexList(3).DateTimeSetUTCOffsetMinutes 4, CurDiff
    tabTelexList(3).Refresh
    tabFlightList(0).DateTimeSetUTCOffsetMinutes 3, CurDiff
    tabFlightList(0).DateTimeSetUTCOffsetMinutes 10, CurDiff
    tabFlightList(0).Refresh
    tabFlightList(1).DateTimeSetUTCOffsetMinutes 3, CurDiff
    tabFlightList(1).DateTimeSetUTCOffsetMinutes 10, CurDiff
    tabFlightList(1).Refresh
    count = tabFlightList(2).GetLineCount - 1
    If count >= 0 Then
        For idx = 0 To count
            tmpDat = tabFlightList(2).GetColumnValue(idx, 6)
            If Len(tmpDat) > 1 Then
                UseServerTime = CedaFullDateToVb(tmpDat)
                If optLocal.Value = True Then
                    UseServerTime = DateAdd("n", UtcTimeDiff, UseServerTime)
                End If
                'tmpDat = Format(UseServerTime, "ddmmmyy\/hh:mm")
                'tmpDat = UCase(tmpDat)
                tmpDat = Format(UseServerTime, "yyyymmddhhmmss")
                tmpMin = Mid(tmpDat, 9, 2) & ":" & Mid(tmpDat, 11, 2)
                tmpDat = Mid(tmpDat, 1, 8)
                tmpDate = DecodeSsimDayFormat(tmpDat, "CEDA", "SSIM2")
                tmpDat = tmpDate & "/" & tmpMin
                tabFlightList(2).SetColumnValue idx, 2, tmpDat
            End If
        Next
    tabFlightList(2).Refresh
    End If
    
    If FormIsLoaded("RcvInfo") Then
        RcvInfo.tabTelexList(0).DateTimeSetUTCOffsetMinutes 4, CurDiff
        RcvInfo.tabTelexList(0).Refresh
    End If
    If FormIsLoaded("RcvTelex") Then
        RcvTelex.tabTelexList(0).DateTimeSetUTCOffsetMinutes 4, CurDiff
        RcvTelex.tabTelexList(0).Refresh
    End If
    If FormIsLoaded("SndTelex") Then
        SndTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 6, CurDiff
        SndTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 7, CurDiff
        SndTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 8, CurDiff
        SndTelex.tabTelexList.Refresh
    End If
    If FormIsLoaded("OutTelex") Then
        OutTelex.tabTelexList.DateTimeSetUTCOffsetMinutes 4, CurDiff
        OutTelex.tabTelexList.Refresh
    End If
End Sub
Private Sub CheckLoaTabViewer(TlxTabRec As String, Index As Integer)
    Dim AftUrno As String
    Dim ShowWhat As String
    Dim LoaCode As String
    Dim InfoTag As String
    Dim CmdLine As String
    AftUrno = Trim(GetFieldValue("FLNU", TlxTabRec, DataPool.TlxTabFields))
    If (AftUrno <> "") And (AftUrno <> "0") Then
        InfoTag = LoaInfo(Index).Tag
        LoaCode = GetItem(InfoTag, 1, ",")
        If InStr(InfoTag, ",") > 0 Then
            ShowWhat = Trim(GetFieldValue("TTYP", TlxTabRec, DataPool.TlxTabFields))
            Select Case Index
                Case 0
                    If ShowWhat = "PTM" Then LoaCode = GetItem(InfoTag, 2, ",")
                Case 1
                Case 2
                Case Else
            End Select
        End If
        CmdLine = AftUrno & "," & LoaCode & "," & CStr(MyFontSize) & "," & CStr(CInt(MyFontBold))
        ShowLoaTabViewer CmdLine
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Show Flight Details", "No relation to a flight.", "hand", "", UserAnswer
    End If
End Sub
Public Sub ShowLoaTabViewer(CmdLineParameter As String)
    Static ViewerPath As String
    Static ViewerAppId 'As Variant
    Dim MsgTxt As String
    Dim retval As Integer
    On Error Resume Next
    If ViewerAppId > 0 Then
        Err.Description = ""
        AppActivate ViewerAppId, False
        If Err.Description = "" Then
            SendKeys "%{F4}", True
        End If
    End If
    If CmdLineParameter <> "CLOSE" Then
        If ViewerPath = "" Then
            'ViewerPath = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOADTAB_VIEWER", "c:\ufis\appl\LoadTabViewer.exe")
            ViewerPath = GetIniEntry(myIniFullName, "LOATAB_INFO", "", "LOADTAB_VIEWER", UFIS_APPL & "\LoadTabViewer.exe")
            ViewerPath = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "LOADTAB_VIEWER", ViewerPath)
        End If
        Err.Description = ""
        ViewerAppId = Shell(ViewerPath & " " & CmdLineParameter, vbNormalFocus)
        If Err.Description <> "" Then
            MsgTxt = "Program not found:" & vbNewLine & ViewerPath
            retval = MyMsgBox.CallAskUser(0, 0, 0, "UFIS Components Control", MsgTxt, "warn1", "", UserAnswer)
        End If
    End If
End Sub

Private Sub ResetFilter_Click()
    If ResetFilter.Value = 1 Then InitDateFields "USR"
    ResetFilter.Value = 0
End Sub

Private Sub RightCover_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    SetMouseWheelObjectCode "MAIN.RIGHT.PANEL"
End Sub

Private Sub SpoolTimer_Timer()
    Dim tmpMsg As String
    If (ImportIsConnected) And (InfoFromClient.Text <> "WAIT") Then
        SpoolTimer.Enabled = False
        tmpMsg = InfoSpooler.GetColumnValue(0, 0)
        InfoSpooler.DeleteLine 0
        InfoSpooler.Refresh
        InfoFromClient = "WAIT"
        InfoToClient.Text = tmpMsg
        If InfoSpooler.GetLineCount > 0 Then SpoolTimer.Enabled = True
    End If
End Sub

Private Sub tabAssign_GotFocus(Index As Integer)
    If tabAssign(0).GetLineCount > 0 Then chkTitleBar(1).Value = 0
End Sub

Private Sub tabAssign_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        chkTlxAssign.BackColor = vbRed
        chkTlxAssign.ForeColor = vbWhite
        tabAssign(0).SetColumnValue LineNo, 5, "N"
'        StopOnline = True
    End If
End Sub

Private Sub tabAssign_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim tmpCode As String
    tmpCode = "MAIN.TABASS." & CStr(Index)
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = tabAssign(Index)
    SetMouseWheelObjectCode tmpCode
End Sub

Private Sub tabFlightList_GotFocus(Index As Integer)
    Dim i As Integer
    i = 1
End Sub

Private Sub tabFlightList_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim i As Integer
    i = 1
End Sub

Private Sub tabFlightList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Static lastColNo(2) As Long
    Dim tmpAftUrno As String
    Dim tmpAftFlno As String
    If Index < 2 Then
        If LineNo >= 0 Then
            tmpAftUrno = tabFlightList(Index).GetColumnValue(LineNo, 15)
            tmpAftFlno = "Flight " & tabFlightList(Index).GetColumnValue(LineNo, 2)
            LoadFolderData tmpAftUrno, tmpAftFlno
        Else
            If lastColNo(Index) = ColNo Then
                If tabFlightList(Index).SortOrderASC Then
                    tabFlightList(Index).Sort CStr(ColNo), False, True
                Else
                    tabFlightList(Index).Sort CStr(ColNo), True, True
                End If
            Else
                tabFlightList(Index).Sort CStr(ColNo), True, True
            End If
            tabFlightList(Index).AutoSizeColumns
            lastColNo(Index) = ColNo
        End If
    End If
End Sub

Private Sub tabFlightList_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim tmpCode As String
    tmpCode = "MAIN.TABFLT." & CStr(Index)
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = tabFlightList(Index)
    SetMouseWheelObjectCode tmpCode
End Sub

Private Sub tabTelexList_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Static MyLastOne As String
    Dim CurTlxUrno As String
    Dim tmpTag As String
    CurTlxUrno = tabTelexList(Index).GetFieldValue(LineNo, "URNO")
    If CurTlxUrno <> MyLastOne Then
        MyLastOne = CurTlxUrno
        If chkAutoRefresh.Value = 1 Then
            chkAutoRefresh.Value = 0
            tmpTag = CStr(Index) & "," & CStr(LineNo)
            KeybTimer.Tag = tmpTag
            KeybTimer.Enabled = True
        End If
    End If
End Sub

Private Sub tabTelexList_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpTag As String
    Dim CurTlxUrno As String
    Dim idx As Integer
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim AddCapt As String
    
    If DoNotRefreshTelexList Then Exit Sub 'igu on 5 Aug 2010
    
    idx = Index
    If idx > 1 Then
        idx = idx - 2
        AddCapt = "(Online)"
    Else
        Select Case CurMem
            Case 2
                AddCapt = "(From Recvd)"
            Case 3
                AddCapt = "(From Sent)"
            Case Else
            AddCapt = "(Loaded)"
        End Select
    End If
    If LineNo >= 0 Then
        If Selected Then
            LastHitTlxTabIdx = Index
            CurTlxUrno = tabTelexList(Index).GetFieldValue(LineNo, "URNO")
            'LastHitTlxUrno = "NONE"
            If LastHitTlxUrno <> CurTlxUrno Then
                LastHitTlxUrno = CurTlxUrno
                KeybTimer.Enabled = False
                tabTelexList(Index).Refresh
                Select Case Index
                    Case 0
                        chkToggleRcvd(0).Value = 1
                    Case 1
                        chkToggleSent(1).Value = 1
                    Case 2
                        chkToggleRcvd(0).Value = 1
                    Case 3
                        chkToggleSent(1).Value = 1
                    Case Else
                End Select
                'DoEvents
                'chkTlxTitleBar(idx).Value = 0
                'If Len(LastTlxUrno(Index)) = 0 Or LastTlxUrno(Index) <> tabTelexList(Index).GetColumnValue(LineNo, 7) Or Len(txtTelexText.Text) = 0 Then
                    LastTlxUrno(Index) = tabTelexList(Index).GetColumnValue(LineNo, 7)
                    If Index >= 2 Then
                        If chkOnlineRcv.Value = 1 Then
                            FillTelexTextField (Index)
                        End If
                    Else
                        FillTelexTextField (Index)
                    End If
                    If Index >= 2 Then
                        If MainIsOnline And Trim(tabTelexList(Index).GetColumnValue(LineNo, 1)) = "" Then
                            If UpdateWstaOnClick = True Then
                                UpdateTelexStatus "V"
                                tabTelexList(Index).SetColumnValue LineNo, 1, "V"
                                ColorPool.GetStatusColor "V", "", CurForeColor, CurBackColor
                                tabTelexList(Index).SetLineColor LineNo, CurForeColor, CurBackColor
                            End If
                        End If
                    End If
                    SetTlxTabCaption idx, Index, AddCapt
                    If Not NewMsgFromBcHdl Then
                        If FormIsVisible("AssignFlight") Then chkAssign.Value = 1
                    End If
                    'If FormIsVisible("frmTelex") Then frmTelex.txtTelex.Text = txtTelexText.Text
                    If MainIsDeploy Then
                        'DEBUGGED
                        'MAIN PERFORMANCE BREAK
                        If Not NewMsgFromBcHdl Then
                            If chkAutoRefresh.Value = 1 Then
                                chkLoadAss.Value = 1
                            End If
                        End If
                    End If
                'End If
                tabTelexList(Index).Refresh
                'tabTelexList(Index).SetFocus
                If Not NewMsgFromBcHdl Then
                    If chkAutoRefresh.Value = 0 Then
                        If KeybTimer.Tag <> "CHECKED" Then
                            tmpTag = CStr(Index) & "," & CStr(LineNo)
                            KeybTimer.Tag = tmpTag
                            KeybTimer.Enabled = True
                        End If
                    End If
                End If
            Else
                FillTelexTextField (Index)
            End If
        End If
    Else
        LastTlxUrno(Index) = ""
        LastHitTlxUrno = "NONE"
    End If
End Sub

Private Sub FillTelexTextField(idx As Integer)
    Dim Index As Integer
    Dim tmpLen As Integer
    Dim LineNo As Long
    Dim clTlxLin As String
    Dim CurTlxRec As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpContext As String
    Dim tmpFldDat As String
    Dim tmpSere As String
    Dim tmpRema As String
    Dim tmpFlnu As String
    Dim tmpAftUrno As String
    Dim tmpStat As String
    Dim tmpText As String
    Dim RecNbr As String
    Dim TlxIsFileType As Boolean
    On Error Resume Next
    Index = idx
    txtTelexText.Text = ""
    txtTelexText = ""
    CurrentUrno = ""
    CurrentRecord = ""
    AtcCSGN.Text = ""
    UseCSGN.Text = ""
    tmpSere = ""
    tmpTtyp = ""
    tmpFlnu = "0"
    If Index = -1 Then
        Index = LastHitTlxTabIdx
        'If chkTitleBar(0).Value = 0 Then
        '    Index = 0
        'ElseIf chkTitleBar(1).Value = 0 Then
        '    Index = 1
        'End If
        'If chkOnlineRcv.Value = 1 And Index >= 0 Then Index = Index + 2
    End If
    'TelexTabToDataRelation (CurMem)
    If Index >= 0 Then
        LastHitTlxTabIdx = Index
        LineNo = tabTelexList(Index).GetCurrentSelected
        If LineNo >= 0 Then
            clTlxLin = tabTelexList(Index).GetLineValues(LineNo)
            If (MainIsOnline) And (Index > 1) Then
                RecNbr = GetMemRecNbr(Index, LineNo, CurMem)
            Else
                RecNbr = GetItem(clTlxLin, 9, ",")
            End If
            If RecNbr <> "" Then
                TelexPoolHead.MousePointer = 11
                LineNo = Val(RecNbr)
                CurTlxRec = DataPool.TelexData(CurMem).GetLineValues(LineNo)
                CurrentRecord = CurTlxRec
                tmpSere = GetFieldValue("SERE", CurTlxRec, DataPool.TlxTabFields.Text)
                If tmpSere = "C" Then
                    'TlxIsFileType = True
                    'clTlxLin = BuildTelexText(CurTlxRec, "TEXT")
                    chkRedirect.Enabled = False
                    chkAssign.Enabled = False
                    'chkNew.Caption = "Send"
                    clTlxLin = ""
                Else
                    clTlxLin = GetFieldValue("TXT1", CurTlxRec, DataPool.TlxTabFields.Text) & _
                               GetFieldValue("TXT2", CurTlxRec, DataPool.TlxTabFields.Text)
                    If InStr(clTlxLin, "=TEXT") > 0 Then TlxIsFileType = True
                    If AllowRedirect = True Then
                        chkRedirect.Enabled = True
                    End If
                    chkAssign.Enabled = True
                    chkNew.Caption = "New"
                End If
                tmpTtyp = Trim(GetFieldValue("TTYP", CurTlxRec, DataPool.TlxTabFields.Text))
                tmpStyp = Trim(GetFieldValue("STYP", CurTlxRec, DataPool.TlxTabFields.Text))
                Select Case tmpTtyp
                    Case "KRIS"
                        tmpText = CleanString(clTlxLin, FOR_CLIENT, False)
                    Case "ATC"
                        If InStr(clTlxLin, "MESSAGE CONT.") > 0 Then
                            clTlxLin = Replace(clTlxLin, Chr(179), Chr(28), 1, -1, vbBinaryCompare)
                            clTlxLin = Replace(clTlxLin, "/", Chr(28), 1, -1, vbBinaryCompare)
                        End If
                        tmpText = CleanString(clTlxLin, FOR_CLIENT, True)
                        tmpFldDat = GetCsgnFromTelex(clTlxLin)
                        UseCSGN.Text = tmpFldDat
                        AtcCSGN.Text = tmpFldDat
                    Case Else
                        tmpText = TextString(clTlxLin, FullText.Value)
                        If FullText.Value = 0 Then
                            If TlxIsFileType Then
                                GetKeyItem tmpContext, clTlxLin, "=SMI", "="
                                tmpContext = CleanString(tmpContext, FOR_CLIENT, True)
                                tmpContext = CleanTrimString(tmpContext)
                                tmpLen = Len(tmpContext)
                                If tmpLen > 0 Then
                                    If InStr(tmpText, (vbLf & tmpContext)) > 0 Then
                                        tmpContext = ""
                                    ElseIf Left(tmpText, tmpLen) = tmpContext Then
                                        tmpContext = ""
                                    Else
                                        tmpContext = tmpContext & vbNewLine
                                    End If
                                End If
                            End If
                        End If
                End Select
                If tmpText = "" Then tmpText = "EMPTY TEXT BODY"
                tmpText = tmpContext & tmpText
                txtTelexText.Text = CleanTrimString(tmpText)
                txtTelexText.Refresh
                'DoEvents
                CurrentUrno.Text = GetFieldValue("URNO", CurTlxRec, DataPool.TlxTabFields.Text)
                TelexPoolHead.MousePointer = 0
                tmpFlnu = GetFieldValue("FLNU", CurTlxRec, DataPool.TlxTabFields.Text)
                tmpAftUrno = tmpFlnu
                If tmpFlnu = "" Then tmpFlnu = "0"
                tmpRema = GetFieldValue("BEME", CurTlxRec, DataPool.TlxTabFields.Text)
                MyStatusBar.Panels(2).Tag = tmpRema
                tmpRema = GetItem(tmpRema, 1, "/")
                tmpRema = Replace(tmpRema, "#", "/", 1, -1, vbBinaryCompare)
                MyStatusBar.Panels(2).Text = tmpRema
                tmpRema = GetFieldValue("MSTX", CurTlxRec, DataPool.TlxTabFields.Text)
                If (tmpRema = "") And (tmpFlnu = "0") Then
                    tmpStat = GetFieldValue("STAT", CurTlxRec, DataPool.TlxTabFields.Text)
                    tmpRema = TlxRcvStat.ExplainStatus("STAT", tmpStat, False)
                    tmpRema = "(" & tmpRema & ")"
                End If
                MyStatusBar.Panels(1).Text = tmpRema
                MyStatusBar.Panels(1).Tag = tmpRema & vbLf & tmpFlnu
                If AreaPanel(3).Visible Then
                    If chkFlightList(6).Value = 1 Then
                        EditFlightsToTelex
                    Else
                        SelectFlightInfo tmpAftUrno, LineNo
                    End If
                End If
            End If
        End If
        On Error Resume Next
        If ApplicationIsStarted Then
            If tabTelexList(Index).Visible Then
                tabTelexList(Index).SetFocus
            End If
        End If
        ShowFolderButtons tmpFlnu, tmpSere, tmpTtyp
    End If
End Sub
Private Sub ShowFolderButtons(CurFlnu As String, CurSere As String, CurTtyp As String)
    Dim CurIdx As Integer
    Dim ShowLoad As Boolean
    Dim UseActTypes As String
    Dim ChkActCode As String
    Dim CurActCode As String
    Dim IsVisible As Boolean
    IsVisible = FolderButtons(0).Visible
    CurIdx = -1
    If CurFlnu <> "0" Then ShowLoad = True Else ShowLoad = False
    If (CurSere <> "") And (CurTtyp <> "") Then
        Select Case CurSere
            Case "R"
                UseActTypes = RecvTypeAction
            Case "I"
                UseActTypes = RecvTypeAction
            Case Else
                'ShowLoad = False
        End Select
        CurIdx = -1
        ChkActCode = CurTtyp & ";"
        CurActCode = GetItem(UseActTypes, 2, ChkActCode)
        CurActCode = GetItem(CurActCode, 1, "|")
        If CurTtyp = "SCOR" Then CurActCode = "IMP"
        Select Case CurActCode
            Case "ATC"
                CurIdx = 1
            Case "IMP"
                CurIdx = 2
            Case Else
                CurIdx = -1
        End Select
    End If
    If bmFolderButtons0 = True Then FolderButtons(0).Visible = ShowLoad
    If CurButtonPanel <> CurIdx Then
        If CurIdx > 0 Then
            FolderButtons(CurIdx).Left = FolderButtons(0).Left
            If FolderButtons(0).Visible Then
                FolderButtons(CurIdx).Top = FolderButtons(0).Top + FolderButtons(0).Height
            Else
                FolderButtons(CurIdx).Top = FolderButtons(0).Top
            End If
            FolderButtons(CurIdx).Visible = True
            FolderButtons(CurIdx).Tag = CurFlnu & "," & CurSere & "," & CurTtyp
        End If
        If CurButtonPanel > 0 Then FolderButtons(CurButtonPanel).Visible = False
        CurButtonPanel = CurIdx
        ArrangeFolderButtons "NEW"
    End If
End Sub
Private Sub ArrangeFolderButtons(ForWhat As String)
    Dim NewTop As Long
    Dim MinTop As Long
    Dim BtnVisible As Boolean
    Dim idx As Integer
    BtnVisible = False
    MinTop = fraFolderToggles.Top + fraFolderToggles.Height
    NewTop = fraListPanel.Top
    If NewTop < MinTop Then NewTop = MinTop
    For idx = 0 To FolderButtons.UBound
        If FolderButtons(idx).Visible Then
            BtnVisible = True
            FolderButtons(idx).Top = NewTop
            If ForWhat = "NEW" Then FolderButtons(idx).Refresh
            NewTop = NewTop + FolderButtons(idx).Height
        End If
    Next
    If Not BtnVisible Then
        If ForWhat = "NEW" Then RightCover.Refresh
    End If
End Sub
Private Function BuildTelexText(cpTlxRec As String, FormatType As String) As String
    Dim Result As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpStat As String
    Dim tmpSere As String
    Dim tmpTlxCode As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpAdid As String
    Dim tmpFlda As String
    Dim tmpPlan As String
    Dim tmpAct1 As String
    Dim tmpAct2 As String
    Dim tmpData As String
    Dim FlightLine As String
    Dim TimeLine As String
    Dim DelayLine As String
    Dim TextPart As String
    Dim clFields As String
    Dim tmpTlxAdrLst As String
    Dim tmpPaye As String
    Dim LineSep As String
    Result = ""
    clFields = DataPool.TlxTabFields.Text
    tmpSere = GetFieldValue("SERE", cpTlxRec, clFields)
    tmpTtyp = GetFieldValue("TTYP", cpTlxRec, clFields)
    tmpStyp = GetFieldValue("STYP", cpTlxRec, clFields)
    tmpStat = GetFieldValue("STAT", cpTlxRec, clFields)
    If tmpSere = "C" Then
        If FormatType = "TEXT" Then
            LineSep = vbNewLine
        Else
            LineSep = "|"
        End If
        If tmpTtyp = "" Then tmpTtyp = "MVT"
        If tmpTtyp = "MVT" Then
            tmpTxt1 = GetFieldValue("TXT1", cpTlxRec, clFields)
            tmpFldLst = GetItem(tmpTxt1, 1, Chr(30))
            tmpFldLst = CleanString(tmpFldLst, FOR_CLIENT, False)
            tmpDatLst = GetItem(tmpTxt1, 2, Chr(30))
            tmpDatLst = CleanString(tmpDatLst, FOR_CLIENT, False)
            'MsgBox tmpFldLst & vbNewLine & tmpDatLst
            If Left(tmpDatLst, 1) = "," Then tmpDatLst = Mid(tmpDatLst, 2)
            tmpOrg3 = GetFieldValue("ORG3", tmpDatLst, tmpFldLst)
            tmpDes3 = GetFieldValue("DES3", tmpDatLst, tmpFldLst)
            tmpFlda = GetFieldValue("FLDA", tmpDatLst, tmpFldLst)
            If tmpFlda = "" Then tmpFlda = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
            If tmpStyp = "XX" Then tmpStyp = ""
            If tmpStyp = "" Then
                tmpAdid = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
                If tmpAdid = "A" Then
                    If tmpStyp = "" Then tmpStyp = "AA"
                ElseIf tmpAdid = "D" Then
                    If tmpStyp = "" Then tmpStyp = "AD"
                Else
                End If
            End If
            FlightLine = ""
            FlightLine = FlightLine & GetFieldValue("FLNO", tmpDatLst, tmpFldLst) & "/"
            FlightLine = FlightLine & Mid(tmpFlda, 7, 2) & "."
            FlightLine = FlightLine & GetFieldValue("REGN", tmpDatLst, tmpFldLst) & "."
            Select Case tmpStyp
                Case "AA"
                    FlightLine = FlightLine & tmpDes3 & LineSep
                    tmpPlan = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("LAND", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("ONBL", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TimeLine = TimeLine & Mid(tmpAct1, 9, 4) & "/"
                    TimeLine = TimeLine & Mid(tmpAct2, 9, 4) & LineSep
                    TextPart = FlightLine & TimeLine
                Case "AD"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("OFBL", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("AIRB", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TimeLine = TimeLine & Mid(tmpAct1, 9, 4) & "/"
                    TimeLine = TimeLine & Mid(tmpAct2, 9, 4)
                    tmpData = GetFieldValue("ETAI", tmpDatLst, tmpFldLst)
                    'If tmpData <> "" Then
                        TimeLine = TimeLine & " EA" & Mid(tmpData, 9, 4)
                        tmpData = Left(GetFieldValue("VIAL", tmpDatLst, tmpFldLst), 3)
                        If tmpData = "" Then tmpData = tmpDes3
                        TimeLine = TimeLine & " " & tmpData
                    'End If
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case "ED"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpAct1 = GetFieldValue("ETDI", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp & Mid(tmpAct1, 7, 6)
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case "NI"
                    FlightLine = FlightLine & tmpOrg3 & LineSep
                    tmpAct1 = GetFieldValue("NXTI", tmpDatLst, tmpFldLst)
                    TimeLine = tmpStyp
                    TextPart = FlightLine & TimeLine
                    DelayLine = BuildDelayLine(tmpFldLst, tmpDatLst)
                    If DelayLine <> "" Then TextPart = TextPart & LineSep & DelayLine
                Case Else
            End Select
            If FormatType = "LINE" Then
                'Browser Sent Telexes
                Result = TextPart
            Else
                'Telex Text Field
                Result = "=PRIORITY" & vbNewLine & "QU" & vbNewLine
                tmpTxt2 = GetFieldValue("TXT2", cpTlxRec, clFields)
                BuildTlxAdressList tmpTxt2, tmpTlxAdrLst, tmpPaye
                Result = Result & tmpTlxAdrLst
                If tmpPaye <> "" Then Result = Result & tmpPaye
                Result = Result & "=TEXT" & vbNewLine & tmpTtyp & vbNewLine
                Result = Result & TextPart & vbNewLine
            End If
        End If
    End If
    BuildTelexText = Result
End Function
Private Function BuildDelayLine(UseFldLst As String, UseDatLst As String) As String
    Dim tmpDtd1 As String
    Dim tmpDtd2 As String
    Dim tmpDcd1 As String
    Dim tmpDcd2 As String
    Dim DelayLine As String
    DelayLine = ""
    tmpDtd1 = GetFieldValue("DTD1", UseDatLst, UseFldLst)
    tmpDtd2 = GetFieldValue("DTD2", UseDatLst, UseFldLst)
    tmpDcd1 = GetFieldValue("DCD1", UseDatLst, UseFldLst)
    tmpDcd2 = GetFieldValue("DCD2", UseDatLst, UseFldLst)
    If Val(tmpDtd1) <= 0 Then tmpDtd1 = ""
    If Val(tmpDtd2) <= 0 Then tmpDtd2 = ""
    If Val(tmpDtd1) > 0 And tmpDcd1 = "" Then tmpDcd1 = ".."
    If Val(tmpDtd2) > 0 And tmpDcd2 = "" Then tmpDcd2 = ".."
    If (tmpDcd1 & tmpDcd2) <> "" Then
        DelayLine = "DL" & tmpDcd1
        If tmpDcd2 <> "" Then DelayLine = DelayLine & "/" & tmpDcd2
        If tmpDtd1 <> "" Then DelayLine = DelayLine & "/" & tmpDtd1
        If tmpDtd2 <> "" Then DelayLine = DelayLine & "/" & tmpDtd2
    End If
    BuildDelayLine = DelayLine
End Function

Private Sub BuildTlxAdressList(FromTxt2 As String, OutAdrLst As String, OutPaye As String)
    Dim tmpAdrLst As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    Dim ItmDat As String
    Dim ilLen As Integer
    OutAdrLst = ""
    OutPaye = ""
    tmpAdrLst = CleanString(FromTxt2, FOR_CLIENT, False)
    'MsgBox tmpAdrLst
    ItmCnt = ItemCount(tmpAdrLst, ",")
    'MsgBox ItmCnt
    For CurItm = 1 To ItmCnt
        ItmDat = GetItem(tmpAdrLst, CurItm, ",")
        ilLen = Len(ItmDat)
        If ilLen > 3 Then
            OutAdrLst = OutAdrLst & "STX," & ItmDat & vbNewLine
        Else
            If ilLen > 1 Then OutPaye = OutPaye & ItmDat & ","
        End If
    Next
    If OutAdrLst <> "" Then
        OutAdrLst = "=DESTINATION TYPE B" & vbNewLine & OutAdrLst
    End If
    If OutPaye <> "" Then
        OutPaye = "=DBLSIG" & vbNewLine & GetItem(OutPaye, 1, ",") & vbNewLine
    End If
    'MsgBox OutAdrLst
    'MsgBox OutPaye
End Sub
Private Function GetCsgnFromTelex(TlxTxt As String)
    Dim Result As String
    Result = Trim(GetItem(TlxTxt, 2, "CSGN:"))
    Result = Trim(GetItem(Result, 1, vbNewLine))
    GetCsgnFromTelex = Result
End Function
Private Function TextString(cpText As String, ipMeth As Integer) As String
    Dim clTlxLin As String
    Dim tmpData As String
    Dim ilOffs As Integer
    Dim ilTxtBgn As Integer
    Dim chkTxtCod As String
    Dim CutTxtCod As Boolean
    clTlxLin = CleanString(cpText, FOR_CLIENT, True)
    If FullText.Value = 0 Then
        If InStr(clTlxLin, "=TEXT") > 0 Then
            chkTxtCod = vbLf & "=TEXT"
            CutTxtCod = True
        Else
            chkTxtCod = vbLf & "."
            CutTxtCod = False
        End If
        ilTxtBgn = 0
        ilOffs = 1
        While ilOffs > 0
            ilOffs = InStr(ilOffs, clTlxLin, chkTxtCod)
            If ilOffs > 0 Then
                ilTxtBgn = ilOffs + 1
                ilOffs = ilOffs + 1
                ilOffs = 0 'we need it only once
            End If
        Wend
        If ilTxtBgn > 0 Then
            If CutTxtCod Then ilTxtBgn = ilTxtBgn + Len(chkTxtCod)
            clTlxLin = Mid(clTlxLin, ilTxtBgn)
            chkTxtCod = Left(clTlxLin, 1)
            While (chkTxtCod = vbLf) Or (chkTxtCod = vbCr)
                clTlxLin = Mid(clTlxLin, 2)
                chkTxtCod = Left(clTlxLin, 1)
            Wend
        End If
    End If
    TextString = clTlxLin
End Function

Private Sub tabTelexList_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim LastSortCol As Long
    Dim SortAsc As Boolean
    If LineNo < 0 Then
        LastSortCol = Val(tabTelexList(Index).Tag)
        If LastSortCol <> ColNo Then
            SortAsc = True
        Else
            SortAsc = Not tabTelexList(Index).SortOrderASC
        End If
        tabTelexList(Index).Sort Str(ColNo), SortAsc, True
        tabTelexList(Index).AutoSizeColumns
        tabTelexList(Index).Tag = CStr(ColNo)
    End If
End Sub

Private Sub tabTelexList_SendMouseMove(Index As Integer, ByVal Line As Long, ByVal Col As Long, ByVal Flags As String)
    Static lastLine(0 To 4) As Long
    Dim tmpCode As String
    Dim RecIdx 'as variant
    Dim FldIdx As Integer
    Dim tmpData As String
'    If Flags = "LBUTTON" Then
'        If chkAssFolder.Enabled Then
'            tabTelexList(Index).DragIcon = Picture1(0).Picture
'            tabTelexList(Index).Drag vbBeginDrag
'        End If
'    End if
    tmpCode = "MAIN.TABTLX." & CStr(Index)
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = tabTelexList(Index)
    SetMouseWheelObjectCode tmpCode
    

    If (Line + 1) <> lastLine(Index) Then
        RecIdx = tabTelexList(Index).GetColumnValue(Line, 8)
        If RecIdx <> "" Then
            tmpData = ""
            Select Case Col
                Case 0
                    FldIdx = GetItemNo(DataPool.TlxTabFields, "STAT") - 1
                    If FldIdx >= 0 Then
                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, FldIdx)
                        tmpData = TlxRcvStat.ExplainStatus("STAT", tmpData, False)
                    End If
                Case 1
                    FldIdx = GetItemNo(DataPool.TlxTabFields, "WSTA") - 1
                    If FldIdx >= 0 Then
                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, FldIdx)
                        tmpData = TlxRcvStat.ExplainStatus("WSTA", tmpData, False)
                    End If
                Case 4
                    FldIdx = GetItemNo(DataPool.TlxTabFields, "CDAT") - 1
                    If FldIdx >= 0 Then
                        tmpData = DataPool.TelexData(CurMem).GetColumnValue(RecIdx, FldIdx)
                        tmpData = Format(CedaDateToVb(tmpData), MySetUp.DefDateFormat)
                    End If
                Case Else
                    'ShowTip = False
            End Select
            tabTelexList(Index).ToolTipText = tmpData   'Set or Reset the ToolTipText
        End If
        lastLine(Index) = Line + 1  'to force line 0
    End If
End Sub

Private Sub tabTelexList_SendRButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    'MsgBox "R-Click"
End Sub

Public Function BuildTlxBrowser(FormatType As String, cpFields As String, cpTlxRec As String, AftUrno As String, TxtSubject As Boolean) As String
    Dim clTlxLin As String
    Dim tmpTtyp As String
    Dim tmpStyp As String
    Dim tmpStat As String
    Dim tmpWsta As String
    Dim tmpSere As String
    Dim tmpFlnu As String
    Dim tmpTlxCode As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpTxt1 As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpAdid As String
    Dim tmpPlan As String
    Dim tmpAct1 As String
    Dim tmpAct2 As String
    Dim tmpData As String
    tmpSere = GetFieldValue("SERE", cpTlxRec, cpFields)
    tmpTtyp = GetFieldValue("TTYP", cpTlxRec, cpFields)
    tmpStyp = GetFieldValue("STYP", cpTlxRec, cpFields)
    tmpStat = GetFieldValue("STAT", cpTlxRec, cpFields)
    tmpWsta = GetFieldValue("WSTA", cpTlxRec, cpFields)
    tmpFlnu = GetFieldValue("FLNU", cpTlxRec, cpFields)
    AftUrno = tmpFlnu
    If tmpSere = "C" And FormatType = "C" Then
        If tmpStat = "C" Then
            tmpTxt1 = GetFieldValue("TXT1", cpTlxRec, cpFields)
            tmpFldLst = GetItem(tmpTxt1, 1, Chr(30))
            tmpFldLst = CleanString(tmpFldLst, FOR_CLIENT, False)
            tmpDatLst = GetItem(tmpTxt1, 2, Chr(30))
            tmpDatLst = CleanString(tmpDatLst, FOR_CLIENT, False)
            If Left(tmpDatLst, 1) = "," Then tmpDatLst = Mid(tmpDatLst, 2)
            tmpOrg3 = GetFieldValue("ORG3", tmpDatLst, tmpFldLst)
            tmpDes3 = GetFieldValue("DES3", tmpDatLst, tmpFldLst)
            tmpAdid = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
            If tmpStyp = "XX" Then tmpStyp = ""
            If tmpAdid = "A" Then
                If tmpStyp = "" Then tmpStyp = "AA"
            ElseIf tmpAdid = "D" Then
                If tmpStyp = "" Then tmpStyp = "AD"
            Else
            End If
            Select Case tmpStyp
                Case "AA"
                    tmpPlan = GetFieldValue("STOA", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("LAND", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("ONBL", tmpDatLst, tmpFldLst)
                Case "AD"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("OFBL", tmpDatLst, tmpFldLst)
                    tmpAct2 = GetFieldValue("AIRB", tmpDatLst, tmpFldLst)
                Case "ED"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("ETDI", tmpDatLst, tmpFldLst)
                Case "NI"
                    tmpPlan = GetFieldValue("STOD", tmpDatLst, tmpFldLst)
                    tmpAct1 = GetFieldValue("NXTI", tmpDatLst, tmpFldLst)
                Case Else
            End Select
            clTlxLin = tmpStat & "," & tmpWsta & ","
            clTlxLin = clTlxLin & tmpTtyp & "," & tmpStyp & ","
            clTlxLin = clTlxLin & GetFieldValue("FLNO", tmpDatLst, tmpFldLst) & ","
            clTlxLin = clTlxLin & GetFieldValue("REGN", tmpDatLst, tmpFldLst) & ","
            clTlxLin = clTlxLin & tmpPlan & ","
            clTlxLin = clTlxLin & tmpAct1 & ","
            clTlxLin = clTlxLin & tmpAct2 & ","
            clTlxLin = clTlxLin & GetFieldValue("URNO", cpTlxRec, cpFields)
        End If
    Else
        clTlxLin = tmpStat & "," & tmpWsta & "," & tmpTtyp & "," & tmpStyp & ","
        tmpData = GetFieldValue("CDAT", cpTlxRec, cpFields)
        clTlxLin = clTlxLin & tmpData & ","
        clTlxLin = clTlxLin & GetFieldValue("TXNO", cpTlxRec, cpFields) & ","
        tmpData = GetFieldValue("TXT1", cpTlxRec, cpFields) & GetFieldValue("TXT2", cpTlxRec, cpFields)
        If tmpSere <> "C" Then
            If TxtSubject = True Then tmpData = GetTelexExtract(tmpSere, tmpTtyp, tmpData, 80)
        Else
            tmpData = BuildTelexText(cpTlxRec, "LINE")
        End If
        clTlxLin = clTlxLin & tmpData & ","
        clTlxLin = clTlxLin & GetFieldValue("URNO", cpTlxRec, cpFields)
    End If
    clTlxLin = Replace(clTlxLin, Chr(28), "|", 1, -1, vbBinaryCompare)
    BuildTlxBrowser = clTlxLin
End Function

Public Sub PrepareTelexExtract(CurMemIdx As Integer, AsTelex As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim SereCol As Long
    Dim TtypCol As Long
    Dim Txt1Col As Long
    Dim Txt2Col As Long
    Dim StatCol As Long
    Dim WstaCol As Long
    Dim PrflCol As Long
    Dim FlnuCol As Long
    Dim LineStatus As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim CurSere As String
    Dim CurText As String
    Dim CurTtyp As String
    Dim CurTypeList As String
    Dim CurStat As String
    Dim CurWsta As String
    Dim ShortText As String
    Dim CurTlxRec As String
    Dim i As Integer
    Dim itm As Integer
    Dim LineList As String
    Dim AllCount As Long
    Dim TypCount As Long
    Dim ShowTelex As Boolean
    On Error Resume Next
    If AsTelex Then
        SereCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "SERE"))
        TtypCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "TTYP"))
        Txt1Col = CLng(GetRealItemNo(DataPool.TlxTabFields, "TXT1"))
        Txt2Col = CLng(GetRealItemNo(DataPool.TlxTabFields, "TXT2"))
        PrflCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "PRFL"))
        StatCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "STAT"))
        WstaCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "WSTA"))
        FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
        MaxLine = DataPool.TelexData(CurMemIdx).GetLineCount - 1
        For CurLine = 0 To MaxLine
            If ShutDownRequested Then Exit For
            If (CurLine Mod 200) = 0 Then DoEvents
            CurText = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, Txt1Col)
            CurText = Trim(CurText & DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, Txt2Col))
            CurTtyp = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, TtypCol)
            CurSere = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, SereCol)
            LineStatus = DataPool.TelexData(CurMemIdx).GetLineStatusValue(CurLine)
            ShowTelex = True
            If (CedaIsConnected) Or (UfisServer.HostName = "LOCAL") Then
                If CurSere <> "C" Then
                    If (MySetUp.AddrFilter.Value = 1) And (LineStatus = 0) Then
                        ShowTelex = FilterTelexAddress(CurText, MySetUp.HomeTlxAddr.Text, AddrNoCheck, CurTtyp, optTlxAddr.Value, CurSere)
                    End If
                    If optTlxAddr.Value = True Then
                        ShowTelex = FilterAddressHeader(CurText, Trim(txtAddr.Text), AddrNoCheck, CurTtyp, ShowTelex)
                    End If
                    ShowTelex = FilterTelexText(CurText, IgnoreAllTlxText, ShowTelex)
                    ShowTelex = FilterTelexTypeText(CurTtyp, CurText, TextFilterList, ShowTelex)
                End If
                If (CurText = "") Then
                    If (IgnoreEmptyText) Then ShowTelex = False
                End If
                If ShowTelex Then
                    If CurSere = "C" Then
                        CurText = DataPool.TelexData(CurMemIdx).GetLineValues(CurLine)
                        CurText = BuildTelexText(CurText, "TEXT")
                        CurText = CleanString(CurText, FOR_SERVER, False)
                    End If
                    ShortText = GetTelexExtract(CurSere, CurTtyp, CurText, 80)
                    DataPool.TelexData(CurMemIdx).SetColumnValue CurLine, PrflCol, ShortText
                    If LineStatus = 0 Then
                        CurStat = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, StatCol)
                        CurWsta = DataPool.TelexData(CurMemIdx).GetColumnValue(CurLine, WstaCol)
                        ColorPool.GetStatusColor CurWsta, "", CurForeColor, CurBackColor
                        DataPool.TelexData(CurMemIdx).SetLineColor CurLine, CurForeColor, CurBackColor
                    Else
                        DataPool.TelexData(CurMemIdx).SetLineColor CurLine, vbBlack, LightYellow
                    End If
                    If CurTtyp = "" Then
                        CurTtyp = "FREE"
                        DataPool.TelexData(CurMemIdx).SetColumnValue CurLine, TtypCol, CurTtyp
                    End If
                    If InStr(FolderTypeList, CurTtyp) = 0 Then
                        LineStatus = LineStatus Or 2
                        DataPool.TelexData(CurMemIdx).SetLineStatusValue CurLine, LineStatus
                    End If
                Else
                    DataPool.TelexData(CurMemIdx).SetLineStatusValue CurLine, 99
                End If
            End If
        Next
    Else
        If Not ShutDownRequested Then
            CurTlxRec = DataPool.TelexData(CurMemIdx).GetLineValues(0)
            If CurTlxRec <> "" Then
                CreateTelex.SetCreatedTelex 0, DataPool.TlxTabFields, CurTlxRec
            End If
            DataPool.TelexData(CurMemIdx).SetLineStatusValue 0, 99
        End If
    End If
    If Not ShutDownRequested Then
        CurLine = 0
        While CurLine <= MaxLine
            If DataPool.TelexData(CurMemIdx).GetLineStatusValue(CurLine) = 99 Then
                DataPool.TelexData(CurMemIdx).DeleteLine CurLine
                CurLine = CurLine - 1
                MaxLine = MaxLine - 1
            End If
            CurLine = CurLine + 1
        Wend
        SetFolderValues CurMemIdx
    End If
End Sub
Public Function GetTelexExtract(TlxSere As String, TlxType As String, TlxText As String, MaxLen As Long) As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpTlxCode As String
    Dim tmpTlxCode2 As String
    Dim NxtLine As String
    Dim FirstChar As String
    Dim DotPos As Integer
    Dim CodPos As Integer
    Dim NxtPos As Integer
    Dim CheckDotPos As Boolean
    Dim i As Integer
    Dim cnt As Integer
    Dim tmpChr As String
    Dim tmpAsc As Integer
    'If TlxSere = "C" Then
        'tmpData = BuildTelexText(TlxText, "TEXT")
    'Else
        tmpData = TlxText
    'End If
    If (TlxSere = "S") And (MySitaOutType = "SITA_FILE") Then
        tmpTlxCode = "=TEXT"
        tmpData = GetItem(TlxText, 2, tmpTlxCode)
        If tmpData = "" Then tmpData = TlxText
    End If
    tmpText = tmpData
    
    tmpTlxCode = "."
    tmpTlxCode = ""
    
    'If (TlxSere = "R") Or ((TlxSere = "S") And (MySitaOutType = "SITA_NODE")) Then
    'If TlxSere <> "C" Then
        CheckDotPos = True
        Select Case TlxType
            Case "ATC"
                CheckDotPos = False
                If InStr(tmpText, "CSGN:") > 0 Then
                    tmpTlxCode = "CSGN:"
                    CheckDotPos = True
                End If
                If InStr(tmpText, "Info:") > 0 Then
                    tmpTlxCode = "Info:"
                    CheckDotPos = True
                End If
                If InStr(tmpText, "CALLSIGN") > 0 Then
                    tmpTlxCode = "CALLSIGN"
                    CheckDotPos = True
                End If
                'For i = 1 To 20
                '    tmpChr = Mid(tmpText, i, 1)
                '    tmpAsc = Asc(tmpChr)
                'Next
            Case "BTM"
                tmpTlxCode = ".F/"
            Case "DLS"
                tmpTlxCode = "STATEMENT"
            Case "FWD"
                tmpTlxCode = "ORIGINALLY ON"
            Case "KRIS"
                If InStr(tmpText, "<=SCREENCONTENT=>") > 0 Then
                    GetKeyItem tmpData, tmpText, "<=SCREENCONTENT=>", "<=/SCREENCONTENT=>"
                    tmpText = tmpData
                End If
                CheckDotPos = False
            Case "DFS"
                tmpTlxCode = "ARCID"
            Case Else
                If InStr(tmpText, "<=SCREENCONTENT=>") > 0 Then
                    GetKeyItem tmpData, tmpText, "<=SCREENCONTENT=>", "<=/SCREENCONTENT=>"
                    tmpText = tmpData
                    CheckDotPos = False
                Else
                    tmpTlxCode = Chr(28) & TlxType
                    tmpTlxCode2 = "|" & TlxType
                End If
        End Select
        If CheckDotPos Then
            DotPos = InStr(tmpText, ".")
            If DotPos > 0 Then
                NxtLine = Mid(tmpText, DotPos)
                NxtLine = GetItem(NxtLine, 1, " ")
            End If
            While (DotPos > 0) And (Len(NxtLine) < 8)
                DotPos = DotPos + 1
                DotPos = InStr(DotPos, tmpText, ".")
                If DotPos > 0 Then
                    NxtLine = Mid(tmpText, DotPos)
                    NxtLine = GetItem(NxtLine, 1, " ")
                End If
            Wend
            If tmpTlxCode <> Chr(28) Then CodPos = InStr(tmpText, tmpTlxCode) Else CodPos = 0
            If (CodPos = 0) And (tmpTlxCode2 <> "") Then
                CodPos = InStr(tmpText, tmpTlxCode2)
            End If
            If CodPos > DotPos Then
                CodPos = CodPos + Len(tmpTlxCode)
                tmpData = Mid(tmpText, CodPos)
            ElseIf (CodPos > 0) And (DotPos > CodPos) Then
                While (CodPos > 0) And (DotPos > CodPos)
                    CodPos = CodPos + Len(tmpTlxCode)
                    tmpData = Mid(tmpText, CodPos)
                    CodPos = InStr(CodPos, tmpText, tmpTlxCode)
                Wend
                If CodPos > DotPos Then
                    CodPos = CodPos + Len(tmpTlxCode)
                    tmpData = Mid(tmpText, CodPos)
                End If
            ElseIf (CodPos = 0) And (DotPos > 0) Then
                tmpData = Mid(tmpData, DotPos)
                NxtPos = InStr(tmpData, Chr(28))
                If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
                NxtLine = Replace(GetItem(tmpData, 1, Chr(28)), " ", "", 1, -1, vbBinaryCompare)
                If (Len(TlxType) > 0) And (Left(NxtLine, Len(TlxType)) = TlxType) Then
                    NxtPos = InStr(tmpData, Chr(28))
                    If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
                Else
                    NxtLine = GetItem(tmpData, 1, " ")
                    If (Len(NxtLine) = 2) And (Left(NxtLine, 1) = "Q") Then
                        DotPos = InStr(tmpData, ".")
                        If DotPos > 0 Then tmpData = Mid(tmpData, DotPos)
                        NxtPos = InStr(tmpData, Chr(28))
                        If NxtPos > 0 Then tmpData = Mid(tmpData, NxtPos + 1)
                    End If
                End If
            End If
            tmpData = LTrim(tmpData)
            If Left(tmpData, 1) = ":" Then tmpData = Mid(tmpData, 2)
            tmpData = Replace(tmpData, Chr(179), Chr(28), 1, -1, vbBinaryCompare)
        End If
    'Else
    'ElseIf TlxSere = "C" Then
    '    tmpData = tmpData
    'End If
    If InStr(tmpData, "=TEXT") > 0 Then
        tmpData = GetItem(tmpData, 2, "=TEXT")
    End If
    tmpData = LTrim(tmpData)
    FirstChar = Left(tmpData, 1)
    While ((FirstChar = Chr(28)) Or (FirstChar = Chr(29)) Or (FirstChar = "|"))
        tmpData = Mid(tmpData, 2)
        tmpData = LTrim(tmpData)
        FirstChar = Left(tmpData, 1)
    Wend
    If tmpData = "" Then
        tmpData = tmpText
        FirstChar = Left(tmpData, 1)
        While ((FirstChar = Chr(28)) Or (FirstChar = Chr(29)) Or (FirstChar = "|"))
            tmpData = Mid(tmpData, 2)
            tmpData = LTrim(tmpData)
            FirstChar = Left(tmpData, 1)
        Wend
    End If
    tmpData = Replace(tmpData, Chr(29), "", 1, -1, vbBinaryCompare)
    tmpData = Replace(tmpData, Chr(28), "|", 1, -1, vbBinaryCompare)
    tmpData = Replace(tmpData, " |", "|", 1, -1, vbBinaryCompare)
    tmpText = tmpData
    cnt = ItemCount(tmpText, "|")
    tmpData = ""
    For i = 1 To cnt
        tmpChr = GetItem(tmpText, i, "|")
        If tmpChr <> "" Then tmpData = tmpData & tmpChr & "|"
    Next
    If MaxLen > 32 Then tmpData = Left(tmpData, MaxLen)
    GetTelexExtract = tmpData
End Function
Private Sub ResetTlxTypeFolder()
    Dim ilOldTab As Integer
    'ilOldTab = Val(tabTlxTypeFolder.Tag)
    'If ilOldTab > 0 Then
    '    tabTlxTypeFolder.Tabs(ilOldTab).HighLighted = False
    '    tabTlxTypeFolder.Tag = 0
    'End If
    chkTitleBar(0).Value = 1
    chkTitleBar(1).Value = 1
End Sub

Private Sub TigerTimer_Timer()
    Static i As Integer
    Static j As Long
    Dim NewTop As Long
    TigerTimer.Enabled = False
    If TigerTimer.Tag = "OFF" Then
        TigerX(0).Visible = False
        Picture1(2).Visible = False
        TigerTimer.Tag = "10"
        Exit Sub
    End If
    If Val(TigerTimer.Tag) > 0 Then
        i = Val(TigerTimer.Tag) - 1
        If i >= 9 Then
            TrimPicture Picture1(2), vbCyan
            Picture1(2).Top = fraListPanel.Top + TigerX(0).Top + (TigerX(0).Height / 2) + 150
            Picture1(2).Left = TigerX(0).Left + TigerX(0).Width + 180
            Picture1(2).Visible = True
            Picture1(2).ZOrder
        End If
        If i <= 0 Then
            i = 0
            TigerX(0).Picture = Tiger(0).Picture
            Tiger(0).Visible = False
            TigerTimer.Interval = 20
        End If
        TigerTimer.Tag = CStr(i)
        TigerTimer.Enabled = True
    Else
        If i = 0 Then i = 1
        If j = 0 Then j = 60
        If i < 8 Then i = i + 1 Else i = 1
        NewTop = MisCfgPanel(0).ScaleHeight - TigerX(0).Height
        If TigerX(0).Top <> NewTop Then TigerX(0).Top = NewTop
        If (TigerX(0).Left < (Me.ScaleWidth) And j > 0) Or (TigerX(0).Left > 0 And j < 0) Then
            TigerX(0).Left = TigerX(0).Left + j
            Picture1(2).Left = TigerX(0).Left + TigerX(0).Width + 180
            TigerX(0).Picture = Tiger(i).Picture
        Else
            i = 0
            TigerX(0).Left = -(TigerX(0).Width)
            TigerX(0).Visible = True
        End If
        TigerTimer.Enabled = True
    End If
End Sub

Private Sub TopSplit_Resize(Index As Integer)
    Select Case Index
        Case 0
            TopStatus(0).Width = TopSplit(Index).ScaleWidth
        Case Else
    End Select
End Sub

Private Sub TopStatus_Click(Index As Integer)
    Dim tmpTag As String
    If TopStatus(Index).Value = 1 Then
        tmpTag = TopStatus(Index).Tag
        Select Case tmpTag
            Case "ONLINE"
                'TopStatus(Index).BackColor = chkOnlineRcv.BackColor
                'TopStatus(Index).Visible = True
                TopStatus(Index).Refresh
            Case "LOADER"
                'TopStatus(Index).BackColor = chkLoader(0).BackColor
                'TopStatus(Index).Visible = True
                TopStatus(Index).Refresh
            Case Else
                TopStatus(Index).Value = 0
        End Select
    Else
        TopStatus(Index).BackColor = vbButtonFace
        TopStatus(Index).Tag = ""
        TopStatus(Index).Visible = False
        TopStatus(Index).Refresh
    End If
End Sub

Private Sub txtAddr_Change()
    CheckLoadButton False
End Sub

Private Sub txtAddr_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtDateFrom_Change()
    If chkFromTo.BackColor = vbRed Then
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
    End If
    ResetFromToCheck
    If CheckDateField(txtDateFrom, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckLoadButton False
End Sub

Private Sub txtDateFrom_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(".-/", Chr(KeyAscii)) > 0 Then
            If InStr(MySetUp.DefDateFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtDateFrom_Validate(Cancel As Boolean)
    If CheckDateField(txtDateFrom, MySetUp.DefDateFormat, True) <> True Then DoNothing
End Sub

Private Sub txtDateTo_Change()
    If chkFromTo.BackColor = vbRed Then
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
    End If
    ResetFromToCheck
    If CheckDateField(txtDateTo, MySetUp.DefDateFormat, False) <> True Then DoNothing
    CheckLoadButton False
End Sub

Private Sub txtDateTo_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(".-/", Chr(KeyAscii)) > 0 Then
            If InStr(MySetUp.DefDateFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtDateTo_Validate(Cancel As Boolean)
    If CheckDateField(txtDateTo, MySetUp.DefDateFormat, True) <> True Then DoNothing
End Sub

Private Sub txtFlca_Change()
    txtFlca.Tag = CheckAlcLookUp(txtFlca.Text, False)
    CheckLoadButton False
End Sub

Private Sub txtFlca_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFlca_LostFocus()
    txtFlca.Tag = CheckAlcLookUp(txtFlca.Text, False)
    CheckLoadButton False
End Sub
Public Function CheckAlcLookUp(AlcCode As String, AskBack As Boolean) As String
    Dim CurTxt As String
    Dim CurAlc As String
    CurTxt = Trim(AlcCode)
    CurAlc = ""
    If CurTxt <> "" Then
        If Len(CurTxt) = 2 Then
            CurAlc = UfisServer.BasicLookUp(1, 0, 1, CurTxt, 0, False)
        Else
            CurAlc = UfisServer.BasicLookUp(1, 1, 1, CurTxt, 0, False)
        End If
    End If
    CheckAlcLookUp = CurAlc
End Function
Private Sub txtFlns_Change()
    CheckLoadButton False
End Sub

Private Sub txtFlns_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtFltn_Change()
    Dim tmpFltn As String
    tmpFltn = Trim(txtFltn.Text)
    If tmpFltn <> "" Then
        If Len(tmpFltn) < 3 Then tmpFltn = Right("000" & tmpFltn, 3)
    End If
    txtFltn.Tag = tmpFltn
    CheckLoadButton False
End Sub

Private Sub txtFltn_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 8 Then
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End If
End Sub

Private Sub txtREGN_Change()
    CheckLoadButton False
End Sub

Private Sub txtREGN_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTelexText_Change()
    If FormIsVisible("frmTelex") Then frmTelex.txtTelex.Text = txtTelexText.Text
End Sub

Private Sub txtTelexText_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    SetMouseWheelObjectCode "MAIN.TELEX.TEXT"
End Sub

Private Sub txtText_Change()
    CheckLoadButton False
End Sub

Private Sub txtText_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTimeFrom_Change()
    Dim tmpVal As String
    Dim tmpHour As String
    Dim tmpMin As String
    Dim ValIsOk As Boolean
    ValIsOk = True
    ResetFromToCheck
    tmpVal = txtTimeFrom.Text
    If InStr(tmpVal, ":") > 0 Then
        If Len(tmpVal) <> 5 Then
            ValIsOk = False
        Else
            tmpHour = GetItem(tmpVal, 1, ":")
            tmpMin = GetItem(tmpVal, 2, ":")
        End If
    ElseIf Len(tmpVal) <> 4 Then
        ValIsOk = False
    Else
        tmpHour = Left(tmpVal, 2)
        tmpMin = Right(tmpVal, 2)
    End If
    If ValIsOk Then
        If Val(tmpHour) > 23 Then ValIsOk = False
        If Val(tmpHour) < 0 Then ValIsOk = False
        If Val(tmpMin) > 59 Then ValIsOk = False
        If Val(tmpMin) < 0 Then ValIsOk = False
    End If
    If ValIsOk Then
        txtTimeFrom.BackColor = vbWhite
        txtTimeFrom.ForeColor = vbBlack
        txtTimeFrom.Tag = Replace(tmpVal, ":", "", 1, -1, vbBinaryCompare)
    Else
        txtTimeFrom.BackColor = vbRed
        txtTimeFrom.ForeColor = vbWhite
        txtTimeFrom.Tag = ""
    End If
    CheckLoadButton False
End Sub
Private Sub ResetFromToCheck()
    If chkFromTo.Tag <> "" Then
        chkFromTo.Tag = ""
        chkFromTo.BackColor = vbButtonFace
        chkFromTo.ForeColor = vbBlack
        txtDateFrom.BackColor = vbWhite
        txtDateTo.BackColor = vbWhite
    End If
End Sub

Private Sub txtTimeFrom_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) And (Chr(KeyAscii) <> ":") Then
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End If
End Sub

Private Sub txtTimeTo_Change()
    Dim tmpVal As String
    Dim tmpHour As String
    Dim tmpMin As String
    Dim ValIsOk As Boolean
    ValIsOk = True
    ResetFromToCheck
    tmpVal = txtTimeTo.Text
    If InStr(tmpVal, ":") > 0 Then
        If Len(tmpVal) <> 5 Then
            ValIsOk = False
        Else
            tmpHour = GetItem(tmpVal, 1, ":")
            tmpMin = GetItem(tmpVal, 2, ":")
        End If
    ElseIf Len(tmpVal) <> 4 Then
        ValIsOk = False
    Else
        tmpHour = Left(tmpVal, 2)
        tmpMin = Right(tmpVal, 2)
    End If
    If ValIsOk Then
        If Val(tmpHour) > 23 Then ValIsOk = False
        If Val(tmpHour) < 0 Then ValIsOk = False
        If Val(tmpMin) > 59 Then ValIsOk = False
        If Val(tmpMin) < 0 Then ValIsOk = False
    End If
    If ValIsOk Then
        txtTimeTo.BackColor = vbWhite
        txtTimeTo.ForeColor = vbBlack
        txtTimeTo.Tag = Replace(tmpVal, ":", "", 1, -1, vbBinaryCompare)
    Else
        txtTimeTo.BackColor = vbRed
        txtTimeTo.ForeColor = vbWhite
        txtTimeTo.Tag = ""
    End If
    CheckLoadButton False
End Sub

Public Sub DistributeBroadcast(BcNum As String, DestName As String, RecvName As String, cpTable As String, cpCmd As String, cpSelKey As String, cpFields As String, cpData As String)
    Dim tmpUrno As String
    Dim tmpStat As String
    Dim RcvIdx As Integer
    Dim InfIdx As Integer
    Dim UrnoIdx As Integer
    Dim LineIdx As Long
    Dim FldIdx As Integer
    Dim tmpList As String
    Dim tmpRcvList As String
    Dim tmpInfList As String
    Dim CurRecDat As String
    Dim RcvTlxLine As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItmNbr As Integer
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim tmpData As String
    Dim tmpTxtData As String
    Dim RcvRefresh As Boolean
    Dim InfRefresh As Boolean
    Dim ShowTelex As Boolean
    Dim MaxLinCnt As Long
    Dim FilterLookUp As String
    Dim FlnuCol As Long
    Dim tmpFlnu As String
    Dim tmpLine As Long
    Dim tmpFnam As String
    Dim tmpFdat As String
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim i As Integer
    Dim tmpFolderCount As String
    Dim tmpPos As Integer
    Dim tmpVal As String
    Dim idx As Integer
    Dim tmpTurn As String
    Dim tmpFurn As String
    Dim HitLine As String
    Dim HitLineNo As Long
    Dim HitIdx As Integer
    Dim tmpTlxCnt As String
    'only called by URT or UBT
    'Should be redesigned
    
    If MainIsOnline Then
        DataPool.UpdateOnlineTelex cpFields, cpData, cpSelKey
        tabTelexList(2).Refresh
        tabTelexList(3).Refresh
        If chkAssFolder.Enabled And MainIsDeploy = False Then
            If cpTable = "TFNTAB" And (cpCmd = "URT" Or cpCmd = "DRT") Then
                tmpFnam = GetFieldValue("FNAM", cpData, cpFields)
                tmpFdat = GetFieldValue("FDAT", cpData, cpFields)
                If Len(tmpFdat) = 0 And (InStr(AddFolderList, tmpFnam) = 0 Or cpCmd = "DRT") Then
                    chkAssFolder.Value = 0
                    chkAssFolder.Value = 1
                End If
            End If
            If cpCmd = "IRT" And cpTable = "TFNTAB" Then
                tmpFnam = GetFieldValue("FNAM", cpData, cpFields)
                tmpFdat = GetFieldValue("FDAT", cpData, cpFields)
                If Len(tmpFdat) = 0 Then
                    chkAssFolder.Value = 0
                    chkAssFolder.Value = 1
                Else
                    tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                    tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                    tmpDatFrom = DecodeSsimDayFormat(tmpDatFrom, "CEDA", "SSIM2")
                    tmpDatTo = DecodeSsimDayFormat(tmpDatTo, "CEDA", "SSIM2")
                    If tmpFdat = tmpDatFrom Or tmpFdat = tmpDatTo Then
                        For i = 0 To fraFolder.UBound - 2
                            If chkFolder(i).Caption = tmpFnam Then
                                tmpFolderCount = chkTabs(i).Caption
                                tmpPos = InStr(tmpFolderCount, "/")
                                If tmpPos > 0 Then
                                    tmpVal = Mid(tmpFolderCount, tmpPos + 1)
                                    tmpVal = tmpVal + 1
                                    tmpFolderCount = Left(tmpFolderCount, tmpPos)
                                    tmpFolderCount = tmpFolderCount & tmpVal
                                Else
                                    tmpFolderCount = tmpFolderCount & "/1"
                                End If
                                chkTabs(i).Caption = tmpFolderCount
                                chkTabs(i).BackColor = vbYellow
                                chkLoad.BackColor = vbRed
                                chkLoad.ForeColor = vbWhite
                                Exit For
                            End If
                        Next
                    End If
                End If
            End If
        End If
        If chkAssFolder.Enabled And AreaPanel(3).Visible Then
            If cpCmd = "IRT" And cpTable = "TLKTAB" Then
                tmpFdat = GetFieldValue("FDAT", cpData, cpFields)
                tmpFdat = Left(tmpFdat, 7)
                tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                tmpDatFrom = DecodeSsimDayFormat(tmpDatFrom, "CEDA", "SSIM2")
                tmpDatTo = DecodeSsimDayFormat(tmpDatTo, "CEDA", "SSIM2")
                If tmpFdat = tmpDatFrom Or tmpFdat = tmpDatTo Then
                    tmpFurn = GetFieldValue("FURN", cpData, cpFields)
                    HitIdx = 1
                    HitLine = tabFlightList(HitIdx).GetLinesByColumnValue(15, tmpFurn, 0)
                    If HitLine = "" Then
                        HitIdx = 0
                        HitLine = tabFlightList(HitIdx).GetLinesByColumnValue(15, tmpFurn, 0)
                    End If
                    If HitLine <> "" Then
                        HitLineNo = Val(HitLine)
                        tmpTlxCnt = tabFlightList(HitIdx).GetColumnValue(HitLineNo, 0)
                        tmpTlxCnt = CStr(Val(tmpTlxCnt) + 1)
                        tabFlightList(HitIdx).SetColumnValue HitLineNo, 0, tmpTlxCnt
                        'Text1.Text = CStr(Val(Text1.Text) + 1)
                    Else
                        'Why count telexes for not loaded Flight?
                        'chkFlightList(4).Value = 1
                        'Text2.Text = CStr(Val(Text2.Text) + 1)
                    End If
                End If
            End If
        End If
    End If
    tmpUrno = GetFieldValue("URNO", cpData, cpFields)
    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(cpSelKey)
    If tmpUrno <> "" Then
        RcvRefresh = False
        InfRefresh = False
        RcvIdx = -1
        InfIdx = -1
        UrnoIdx = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
        tmpList = DataPool.TelexData(CurMem).GetLinesByColumnValue(UrnoIdx, tmpUrno, 1)
        If tmpList <> "" Then
            LineIdx = Val(GetItem(tmpList, 1, ","))
            DataPool.TelexData(CurMem).SetFieldValues LineIdx, cpFields, cpData
            RefreshListData 0, tmpUrno
            RefreshListData 1, tmpUrno
        End If
        
        tmpRcvList = ""
        If FormIsLoaded("RcvTelex") Then
            RcvIdx = 1
            tmpRcvList = RcvTelex.tabTelexList(RcvIdx).GetLinesByColumnValue(7, tmpUrno, 1)
            If tmpRcvList = "" Then
                RcvIdx = 0
                tmpRcvList = RcvTelex.tabTelexList(RcvIdx).GetLinesByColumnValue(7, tmpUrno, 1)
            End If
            If tmpRcvList <> "" Then
                LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                tmpData = GetFieldValue("TTYP", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 2, tmpData
                tmpData = GetFieldValue("STYP", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 3, tmpData
                tmpData = GetFieldValue("CDAT", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 4, tmpData
                tmpData = GetFieldValue("TXNO", cpData, cpFields)
                If tmpData <> "" Then RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 5, tmpData
                If InStr(cpFields, "TXT1") > 0 Then
                    tmpData = Trim(GetFieldValue("TXT1", cpData, cpFields) & GetFieldValue("TXT2", cpData, cpFields))
                    If (tmpData <> "") Or (Not IgnoreEmptyText) Then
                        RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 6, tmpData
                    Else
                        If RcvIdx = 1 Then
                            RcvTelex.tabTelexList(RcvIdx).DeleteLine LineIdx
                            If RcvTelex.tabTelexList(RcvIdx).GetLineCount < 1 Then RcvTelex.Timer1.Enabled = False
                            RcvIdx = -1
                            tmpRcvList = ""
                        End If
                    End If
                End If
                RcvRefresh = True
            End If
        End If
        
        If (tmpRcvList = "") And (RcvIdx >= 0) Then
            If FormIsLoaded("RcvInfo") Then
                InfIdx = 1
                tmpInfList = RcvInfo.tabTelexList(InfIdx).GetLinesByColumnValue(7, tmpUrno, 1)
                If tmpInfList = "" Then
                    InfIdx = 0
                    tmpInfList = RcvInfo.tabTelexList(InfIdx).GetLinesByColumnValue(7, tmpUrno, 1)
                End If
                If tmpInfList <> "" Then
                    LineIdx = Val(GetItem(tmpInfList, 1, ","))
                    tmpData = GetFieldValue("TTYP", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 2, tmpData
                    tmpData = GetFieldValue("STYP", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 3, tmpData
                    tmpData = GetFieldValue("CDAT", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 4, tmpData
                    tmpData = GetFieldValue("TXNO", cpData, cpFields)
                    If tmpData <> "" Then RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 5, tmpData
                    If InStr(cpFields, "TXT1") > 0 Then
                        tmpData = Trim(GetFieldValue("TXT1", cpData, cpFields) & GetFieldValue("TXT2", cpData, cpFields))
                        If (tmpData <> "") Or (Not IgnoreEmptyText) Then
                            RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 6, tmpData
                        Else
                            If InfIdx = 1 Then
                                RcvInfo.tabTelexList(InfIdx).DeleteLine LineIdx
                                If RcvInfo.tabTelexList(InfIdx).GetLineCount < 1 Then RcvInfo.Timer1.Enabled = False
                                InfIdx = -1
                                tmpInfList = ""
                            End If
                        End If
                    End If
                    InfRefresh = True
                End If
            End If
        End If
        
        tmpStat = GetFieldValue("STAT", cpData, cpFields)
        If tmpStat <> "" Then
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            If FormIsLoaded("RcvTelex") Then
                If tmpRcvList <> "" Then
                    LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                    RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 0, tmpStat
                    RcvTelex.tabTelexList(RcvIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    RcvRefresh = True
                End If
            End If
            If FormIsLoaded("SndTelex") Then
                tmpList = SndTelex.tabTelexList.GetLinesByColumnValue(9, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    SndTelex.tabTelexList.SetColumnValue LineIdx, 0, tmpStat
                    SndTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    SndTelex.tabTelexList.AutoSizeColumns
                    SndTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("OutTelex") Then
                tmpList = OutTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    OutTelex.tabTelexList.SetColumnValue LineIdx, 0, tmpStat
                    OutTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    OutTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("RcvInfo") Then
                If tmpInfList <> "" Then
                    LineIdx = Val(GetItem(tmpInfList, 1, ","))
                    RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 0, tmpStat
                    RcvInfo.tabTelexList(InfIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    InfRefresh = True
                End If
            End If
        End If
        
        tmpStat = GetFieldValue("WSTA", cpData, cpFields)
        If tmpStat <> "" Then
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            If FormIsLoaded("RcvTelex") Then
                If tmpRcvList <> "" Then
                    LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                    RcvTelex.tabTelexList(RcvIdx).SetColumnValue LineIdx, 1, tmpStat
                    RcvTelex.tabTelexList(RcvIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    RcvRefresh = True
                End If
            End If
            If FormIsLoaded("SndTelex") Then
                tmpList = SndTelex.tabTelexList.GetLinesByColumnValue(9, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    SndTelex.tabTelexList.SetColumnValue LineIdx, 1, tmpStat
                    SndTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    SndTelex.tabTelexList.AutoSizeColumns
                    SndTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("OutTelex") Then
                tmpList = OutTelex.tabTelexList.GetLinesByColumnValue(7, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = Val(GetItem(tmpList, 1, ","))
                    OutTelex.tabTelexList.SetColumnValue LineIdx, 1, tmpStat
                    OutTelex.tabTelexList.SetLineColor LineIdx, myTextColor, myBackColor
                    OutTelex.tabTelexList.Refresh
                End If
            End If
            If FormIsLoaded("RcvInfo") Then
                If tmpInfList <> "" Then
                    LineIdx = Val(GetItem(tmpInfList, 1, ","))
                    RcvInfo.tabTelexList(InfIdx).SetColumnValue LineIdx, 1, tmpStat
                    RcvInfo.tabTelexList(InfIdx).SetLineColor LineIdx, myTextColor, myBackColor
                    InfRefresh = True
                End If
            End If
            If MainIsOnline Then
                For idx = 2 To 3
                    tmpList = tabTelexList(idx).GetLinesByColumnValue(7, tmpUrno, 1)
                    If tmpList <> "" Then
                        LineIdx = Val(GetItem(tmpList, 1, ","))
                        tabTelexList(idx).SetColumnValue LineIdx, 1, tmpStat
                        tabTelexList(idx).SetLineColor LineIdx, myTextColor, myBackColor
                        tabTelexList(idx).Refresh
                    End If
                Next
            End If
        End If
        
        If RcvIdx = 1 Then
            If tmpRcvList <> "" Then
                LineIdx = Val(GetItem(tmpRcvList, 1, ","))
                tmpStat = RcvTelex.tabTelexList(1).GetColumnValue(LineIdx, 0)
                If (tmpStat <> "") And (InStr("W1", tmpStat) = 0) Then ShowTelex = True Else ShowTelex = False
                If ShowTelex Then
                    If StopOnline = False Then
                        RcvTelex.ShowHiddenTelex LineIdx
                        If MainIsOnline Then
                            ReadFolderCount = False
                            PrepareTelexExtract CurMem, True
                            ReadFolderCount = True
                            If AreaPanel(3).Visible Then
                                If chkAssFolder.Enabled = False Then
                                    chkFlightList(4).Value = 1
                                Else
                                    FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
                                    tmpLine = DataPool.TelexData(CurMem).GetLineCount - 1
                                    tmpFlnu = DataPool.TelexData(CurMem).GetColumnValue(tmpLine, FlnuCol)
                                    If tmpFlnu <> "" Then
                                        AddCountFlightTelex tmpFlnu
                                    End If
                                End If
                            End If
                            If chkOnlineRcv.Value = 1 Then
                                SetTlxTabCaption 0, 2, "(Online)"
                                SetTlxTabCaption 1, 3, "(Online)"
                                chkRcvCnt(2).Caption = CStr(tabTelexList(2).GetLineCount)
                                chkRcvCnt(3).Caption = CStr(tabTelexList(2).GetLineCount)
                                chkSntCnt(2).Caption = CStr(tabTelexList(3).GetLineCount)
                                chkSntCnt(3).Caption = CStr(tabTelexList(3).GetLineCount)
                            End If
                        End If
                    End If
                End If
            End If
        End If
        If InfIdx = 1 Then
            If tmpInfList <> "" Then
                LineIdx = Val(GetItem(tmpInfList, 1, ","))
                tmpStat = RcvInfo.tabTelexList(1).GetColumnValue(LineIdx, 0)
                If (tmpStat <> "") And (InStr("W1", tmpStat) = 0) Then ShowTelex = True Else ShowTelex = False
                If ShowTelex Then RcvInfo.ShowHiddenTelex LineIdx
            End If
        End If
        
        If RcvRefresh Then RcvTelex.tabTelexList(RcvIdx).Refresh
        If InfRefresh Then RcvInfo.tabTelexList(InfIdx).Refresh
    End If
End Sub
Private Sub RefreshListData(Index As Integer, RecUrno As String)
    Dim tmpList As String
    Dim LineIdx 'as Variant
    Dim DataIdx 'as long
    Dim RecData As String
    Dim RecLine As String
    Dim tmpStat As String
    Dim AftUrno As String
    Dim myTextColor As Long
    Dim myBackColor As Long
    tmpList = tabTelexList(Index).GetLinesByColumnValue(7, RecUrno, 1)
    If tmpList <> "" Then
        LineIdx = GetItem(tmpList, 1, ",")
        DataIdx = tabTelexList(Index).GetColumnValue(LineIdx, 8)
        If DataIdx <> "" Then
            RecData = DataPool.TelexData(CurMem).GetLineValues(DataIdx)
            RecLine = BuildTlxBrowser("RS", DataPool.TlxTabFields, RecData, AftUrno, True) & "," & DataIdx
            RecLine = RecLine & "," & AftUrno
            tabTelexList(Index).UpdateTextLine LineIdx, RecLine, True
            tmpStat = GetFieldValue("STAT", RecData, DataPool.TlxTabFields.Text)
            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
            tabTelexList(Index).SetLineColor LineIdx, myTextColor, myBackColor
            tabTelexList(Index).RedrawTab
        End If
    End If
End Sub

Public Function EvaluateBc(Caller As Integer, LineNo As Long, BcNum As String, DestName As String, RecvName As String, cpTable As String, cpCmd As String, cpSelKey As String, cpFields As String, cpData As String, DTabLine As Long) As Long
    Dim clTlxLin As String
    Dim tmpSere As String
    Dim tmpStat As String
    Dim tmpWsta As String
    Dim tmpType As String
    Dim tmpTxt1 As String
    Dim tmpUrno As String
    Dim AftUrno As String
    Dim FilterLookUp As String
    Dim RcvIdx As Integer
    Dim InfIdx As Integer
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim MaxLinCnt As Long
    Dim UseLineNo As Long
    Dim ShowTelex As Boolean
    Dim PutInRcv As Boolean
    Dim PutInInf As Boolean
    Dim NewLine As Long
    Dim OnlLineNo As Long
    Dim retval As Boolean
    Dim BcLogRec As String
    'BcLog(i).HeaderString = "Sequ,S,V,Type,-----URNO-----,Remark" & Space(50)
    If Caller = 0 Then
        'Broadcast
        tmpSere = Trim(GetFieldValue("SERE", cpData, cpFields))
        tmpStat = Trim(GetFieldValue("STAT", cpData, cpFields))
        tmpWsta = Trim(GetFieldValue("WSTA", cpData, cpFields))
        tmpType = Trim(GetFieldValue("TTYP", cpData, cpFields))
        tmpUrno = Trim(GetFieldValue("URNO", cpData, cpFields))
        BcLogRec = ""
        BcLogRec = BcLogRec & BcNum & ","
        BcLogRec = BcLogRec & tmpSere & ","
        BcLogRec = BcLogRec & tmpStat & ","
        'BcLogRec = BcLogRec & tmpWsta & ","
        BcLogRec = BcLogRec & tmpType & ","
        BcLogRec = BcLogRec & tmpUrno & ","
        AppendToBcLog 0, BcLogRec
    End If
    If Caller = 1 Then
        'Refresh Online Windows
    End If
    If Caller = 2 Then
        'Online Rcv Window Show Hidden
        tmpSere = Trim(GetFieldValue("SERE", cpData, cpFields))
        tmpStat = Trim(GetFieldValue("STAT", cpData, cpFields))
        tmpWsta = Trim(GetFieldValue("WSTA", cpData, cpFields))
        tmpType = Trim(GetFieldValue("TTYP", cpData, cpFields))
        tmpUrno = Trim(GetFieldValue("URNO", cpData, cpFields))
        BcLogRec = ""
        BcLogRec = BcLogRec & BcNum & ","
        BcLogRec = BcLogRec & tmpSere & ","
        BcLogRec = BcLogRec & tmpStat & ","
        'BcLogRec = BcLogRec & tmpWsta & ","
        BcLogRec = BcLogRec & tmpType & ","
        BcLogRec = BcLogRec & tmpUrno & ","
        AppendToBcLog 1, BcLogRec
        Exit Function
    End If
    If Caller = 3 Then
        'Online Rcv Window Show Hidden
        'Telex Time (CDAT) is too old
        tmpSere = Trim(GetFieldValue("SERE", cpData, cpFields))
        tmpStat = Trim(GetFieldValue("STAT", cpData, cpFields))
        tmpWsta = Trim(GetFieldValue("WSTA", cpData, cpFields))
        tmpType = Trim(GetFieldValue("TTYP", cpData, cpFields))
        tmpUrno = Trim(GetFieldValue("URNO", cpData, cpFields))
        BcLogRec = ""
        BcLogRec = BcLogRec & BcNum & ","
        BcLogRec = BcLogRec & tmpSere & ","
        BcLogRec = BcLogRec & tmpStat & ","
        'BcLogRec = BcLogRec & tmpWsta & ","
        BcLogRec = BcLogRec & tmpType & ","
        BcLogRec = BcLogRec & tmpUrno & ","
        AppendToBcLog 2, BcLogRec
        Exit Function
    End If
    MaxLinCnt = 0
    If LockBroadcasts = False Then
        If cpTable = "TLXTAB" Then
            tmpSere = GetFieldValue("SERE", cpData, cpFields)
            'Also called from RefreshOnlineWindows
            If InStr("IBT,IRT,MVT", cpCmd) > 0 Then
                tmpTxt1 = Trim(GetFieldValue("TXT1", cpData, cpFields) & GetFieldValue("TXT2", cpData, cpFields))
                If (tmpTxt1 <> "") Or (Not IgnoreEmptyText) Then
                    ShowTelex = True
                    If ShowTelex Then
                        tmpType = GetFieldValue("TTYP", cpData, cpFields)
                        tmpStat = GetFieldValue("STAT", cpData, cpFields)
                        tmpWsta = GetFieldValue("WSTA", cpData, cpFields)
                        If tmpWsta <> "" Then
                            ColorPool.GetStatusColor tmpWsta, "", myTextColor, myBackColor
                        Else
                            ColorPool.GetStatusColor tmpStat, "", myTextColor, myBackColor
                        End If
                        If tmpSere = "R" Then
                            If FormIsLoaded("RcvTelex") Then
                                If InStr("W1", tmpStat) > 0 Then
                                    If MainIsOnline Then DataPool.InsertOnlineTelex 9, cpFields, cpData
                                    RcvIdx = 1
                                    clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, AftUrno, False) & ",0"
                                    clTlxLin = clTlxLin & "," & AftUrno
                                    RcvTelex.tabTelexList(RcvIdx).InsertTextLineAt 0, clTlxLin, False
                                    RcvTelex.tabTelexList(RcvIdx).TimerSetValue 0, 60
                                    RcvTelex.Timer1.Enabled = True
                                Else
                                    RcvIdx = 0
                                    If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpTxt1, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpType, True, tmpSere) Else ShowTelex = True
                                    ShowTelex = FilterTelexText(tmpTxt1, IgnoreAllTlxText, ShowTelex)
                                    ShowTelex = FilterTelexTypeText(tmpType, tmpTxt1, TextFilterList, ShowTelex)
                                    'If (tmpTxt1 = "") And (IgnoreEmptyText) Then ShowTelex = False
                                    If ShowTelex Then
                                        FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpType)
                                        If FilterLookUp = "OK" Then
                                            PutInRcv = True
                                            If PutInRcv Then
                                                'when we come here then it is either a refresh of the online window
                                                'or it is a IRT/IBT broadcast which was not originated by FDIHDL.
                                                'Note: It is still a weak point that FDIHDL does not create real IRT BCs.
                                                If NewMsgFromBcHdl = True Then
                                                    'we got a broadcast
                                                    '==================
                                                    UseLineNo = 0
                                                    clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, AftUrno, True) & "," & CStr(DTabLine)
                                                    clTlxLin = clTlxLin & "," & AftUrno
                                                    tabTelexList(2).InsertTextLineAt UseLineNo, clTlxLin, False
                                                    tabTelexList(2).SetLineColor UseLineNo, myTextColor, myBackColor
                                                    RcvTelex.tabTelexList(RcvIdx).InsertTextLineAt UseLineNo, clTlxLin, False
                                                    RcvTelex.tabTelexList(RcvIdx).SetLineColor UseLineNo, myTextColor, myBackColor
                                                    If MainIsOnline Then SetTlxTabCaption 0, 2, "(Online)"
                                                    RcvTelex.tabTelexList(RcvIdx).RedrawTab
                                                    UseLineNo = tabTelexList(2).GetLineCount
                                                    If UseLineNo > MaxOnlineCount Then
                                                        tabTelexList(2).DeleteLine UseLineNo
                                                        tabTelexList(2).Refresh
                                                    End If
                                                    UseLineNo = RcvTelex.tabTelexList(RcvIdx).GetLineCount
                                                    If UseLineNo > MaxOnlineCount Then
                                                        RcvTelex.tabTelexList(RcvIdx).DeleteLine UseLineNo
                                                        RcvTelex.tabTelexList(RcvIdx).Refresh
                                                    End If
                                                Else
                                                    'Must have been called from RefreshOnlineWindow
                                                    '==============================================
                                                    UseLineNo = tabTelexList(2).GetLineCount
                                                    clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, AftUrno, True) & "," & CStr(DTabLine)
                                                    clTlxLin = clTlxLin & "," & AftUrno
                                                    tabTelexList(2).InsertTextLine clTlxLin, False
                                                    tabTelexList(2).SetLineColor UseLineNo, myTextColor, myBackColor
                                                    UseLineNo = RcvTelex.tabTelexList(RcvIdx).GetLineCount
                                                    If UseLineNo <= MaxOnlineCount Then
                                                        RcvTelex.tabTelexList(RcvIdx).InsertTextLine clTlxLin, False
                                                        RcvTelex.tabTelexList(RcvIdx).SetLineColor UseLineNo, myTextColor, myBackColor
                                                    End If
                                                    If MainIsOnline Then SetTlxTabCaption 0, 2, "(Online)"
                                                    If Not OnlineRefreshLoop Then
                                                        RcvTelex.tabTelexList(RcvIdx).RedrawTab
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        ElseIf tmpSere = "I" Then
                            If FormIsLoaded("RcvInfo") Then
                                If InStr("W1", tmpStat) > 0 Then
                                    InfIdx = 1
                                    clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, AftUrno, False) & ",0"
                                    clTlxLin = clTlxLin & "," & AftUrno
                                    RcvInfo.tabTelexList(InfIdx).InsertTextLineAt 0, clTlxLin, False
                                    RcvInfo.tabTelexList(InfIdx).TimerSetValue 0, 60
                                    RcvInfo.Timer1.Enabled = True
                                Else
                                    InfIdx = 0
                                    If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpTxt1, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpType, True, tmpSere) Else ShowTelex = True
                                    ShowTelex = FilterTelexText(tmpTxt1, IgnoreAllTlxText, ShowTelex)
                                    ShowTelex = FilterTelexTypeText(tmpType, tmpTxt1, TextFilterList, ShowTelex)
                                    'If (tmpTxt1 = "") And (IgnoreEmptyText) Then ShowTelex = False
                                    If ShowTelex Then
                                        FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpType)
                                        If FilterLookUp = "OK" Then
                                            PutInInf = True
                                            If InfoTypeList <> "" And tmpType <> "" Then
                                                If InStr(InfoTypeList, tmpType) = 0 Then PutInInf = False
                                            End If
                                            If PutInInf Then
                                                MaxLinCnt = RcvInfo.tabTelexList(0).GetLineCount
                                                If LineNo >= 0 Then UseLineNo = LineNo Else UseLineNo = MaxLinCnt
                                                clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, AftUrno, True) & ",0"
                                                clTlxLin = clTlxLin & "," & AftUrno
                                                RcvInfo.tabTelexList(0).InsertTextLineAt UseLineNo, clTlxLin, True
                                                RcvInfo.tabTelexList(0).SetLineColor UseLineNo, myTextColor, myBackColor
                                                MaxLinCnt = MaxLinCnt + 1
                                                If LineNo = 0 Then
                                                    If MaxLinCnt > MaxOnlineCount Then
                                                        MaxLinCnt = MaxLinCnt - 1
                                                        RcvInfo.tabTelexList(0).DeleteLine MaxLinCnt
                                                    End If
                                                    RcvInfo.Caption = CStr(MaxLinCnt) & " " & RcvTelex.Tag
                                                End If
                                                RcvInfo.tabTelexList(0).RedrawTab
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        ElseIf tmpSere = "S" Then
                            If FormIsLoaded("OutTelex") Then
                                If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpTxt1, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpType, True, tmpSere) Else ShowTelex = True
                                ShowTelex = FilterTelexText(tmpTxt1, IgnoreAllTlxText, ShowTelex)
                                ShowTelex = FilterTelexTypeText(tmpType, tmpTxt1, TextFilterList, ShowTelex)
                                If ShowTelex Then
                                    FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_OUT", tmpType)
                                    If FilterLookUp = "OK" Then
                                        UseLineNo = tabTelexList(3).GetLineCount
                                        clTlxLin = BuildTlxBrowser("RS", cpFields, cpData, AftUrno, True) & "," & CStr(DTabLine)
                                        clTlxLin = clTlxLin & "," & AftUrno
                                        tabTelexList(3).InsertTextLine clTlxLin, False
                                        tabTelexList(3).SetLineColor UseLineNo, myTextColor, myBackColor
                                        If NewMsgFromBcHdl = True Then
                                            OutTelex.tabTelexList.InsertTextLineAt 0, clTlxLin, False
                                            OutTelex.tabTelexList.SetLineColor 0, myTextColor, myBackColor
                                        ElseIf UseLineNo <= MaxOnlineCount Then
                                            OutTelex.tabTelexList.InsertTextLine clTlxLin, False
                                            OutTelex.tabTelexList.SetLineColor UseLineNo, myTextColor, myBackColor
                                        End If
                                        MaxLinCnt = OutTelex.tabTelexList.GetLineCount
                                        If MaxLinCnt > MaxOnlineCount Then
                                            OutTelex.tabTelexList.DeleteLine (MaxLinCnt - 1)
                                            MaxLinCnt = MaxLinCnt - 1
                                        End If
                                        OutTelex.Caption = CStr(MaxLinCnt) & " " & OutTelex.Tag
                                        If MainIsOnline Then SetTlxTabCaption 1, 3, "(Online)"
                                        If Not OnlineRefreshLoop Then
                                            OutTelex.tabTelexList.RedrawTab
                                        End If
                                    End If
                                End If
                            End If
                        ElseIf tmpSere = "C" Then
                            If FormIsLoaded("SndTelex") Then
                                MaxLinCnt = SndTelex.tabTelexList.GetLineCount
                                If LineNo >= 0 Then UseLineNo = LineNo Else UseLineNo = MaxLinCnt
                                clTlxLin = BuildTlxBrowser("C", cpFields, cpData, AftUrno, True) & ",0"
                                clTlxLin = clTlxLin & "," & AftUrno
                                SndTelex.tabTelexList.InsertTextLineAt UseLineNo, clTlxLin, True
                                SndTelex.tabTelexList.SetLineColor UseLineNo, myTextColor, myBackColor
                                MaxLinCnt = MaxLinCnt + 1
                                If LineNo = 0 Then
                                    If MaxLinCnt > MaxOnlineCount Then
                                        MaxLinCnt = MaxLinCnt - 1
                                        SndTelex.tabTelexList.DeleteLine MaxLinCnt
                                    End If
                                    SndTelex.Caption = CStr(MaxLinCnt) & " " & SndTelex.Tag
                                End If
                                SndTelex.tabTelexList.AutoSizeColumns
                                'SndTelex.tabTelexList.Refresh
                                If Not FormIsLoaded("CreateTelex") Then Load CreateTelex
                                If FormIsLoaded("CreateTelex") Then
                                    CreateTelex.SetNewTlxFlightTab LineNo, cpFields, cpData
                                End If
                            End If
                        End If
                    End If
                End If
            ElseIf InStr("UBT,URT", cpCmd) > 0 Then
                DistributeBroadcast BcNum, DestName, RecvName, cpTable, cpCmd, cpSelKey, cpFields, cpData
            ElseIf InStr("DRT", cpCmd) > 0 Then
                'Can only be "Created Telexes or Pending Telex"
                'We need to search the URNO!
                If FormIsLoaded("SndTelex") Then
                    retval = SndTelex.DeleteLineByUrno(cpSelKey)
                End If
            End If
        ElseIf cpTable = "AFTTAB" Then
            'Check AUTO_MVT Feature
            If FormIsLoaded("CreateTelex") Then
                CreateTelex.UpdateFlightRecord LineNo, BcNum, DestName, RecvName, cpTable, cpCmd, cpSelKey, cpFields, cpData
            End If
        ElseIf cpTable = "TFRTAB" Then
            'Nothing to do
        ElseIf cpTable = "TFNTAB" Or cpTable = "TLKTAB" Then
            DistributeBroadcast BcNum, DestName, RecvName, cpTable, cpCmd, cpSelKey, cpFields, cpData
        End If
    End If
    EvaluateBc = MaxLinCnt
End Function
Private Sub AppendToBcLog(Index As Integer, BcLogRec As String)
    Dim MaxLine As Long
    If LoggingIsActive Then
        BcLog(Index).InsertTextLine BcLogRec, False
        MaxLine = BcLog(Index).GetLineCount
        If MaxLine > MaxLoggingLines Then
            BcLog(Index).DeleteLine 0
        End If
        If Index = 1 Then
            Index = Index
        End If
        AdjustBcLogScroll
    End If
End Sub
Private Function GetOnlineTimeFrame(Caller As String, ConText As String) As String
    Dim UseServerTime
    Dim BeginTime
    Dim EndTime
    Dim ItemGrp As String
    Dim ItemBgn As String
    Dim ItemEnd As String
    Dim TimeFrom As String
    Dim TimeTo As String
    UseServerTime = CedaFullDateToVb(MySetUp.ServerTime.Tag)
    If MySetUp.ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
    If Caller <> "INF" Then
        ItemGrp = ""
        ItemBgn = ""
        ItemEnd = ""
        Select Case Caller
            Case "RCV"
                ItemGrp = RecvPeriod
            Case "SND"
                ItemGrp = SendPeriod
            Case "OUT"
                ItemGrp = SentPeriod
            Case "PND"
                ItemGrp = PendPeriod
            Case Else
        End Select
        If ItemGrp = "" Then ItemGrp = MySetUp.OnlineFrom & "," & MySetUp.OnlineTo
        ItemBgn = GetItem(ItemGrp, 1, ",")
        ItemEnd = GetItem(ItemGrp, 2, ",")
        BeginTime = DateAdd("h", -Abs(Val(ItemBgn)), UseServerTime)
        EndTime = DateAdd("h", Abs(Val(ItemEnd)), UseServerTime)
    Else
        BeginTime = DateAdd("d", -Abs(Val("2")), UseServerTime)
        EndTime = DateAdd("d", Abs(Val("2")), UseServerTime)
    End If
    ConText = "-" & CStr(Abs(Val(MySetUp.OnlineFrom))) & " / +" & CStr(Abs(Val(MySetUp.OnlineTo))) & " h"
    TimeFrom = Format(BeginTime, "yyyymmddhhmmss")
    TimeTo = Format(EndTime, "yyyymmddhhmmss")
    GetOnlineTimeFrame = TimeFrom & "," & TimeTo
End Function
Public Sub RefreshOnlineWindows(Caller As String)
    Dim tmpTxt As String
    Dim SqlKey As String
    Dim retval As Integer
    Dim tmpVal As String
    Dim tmpFields As String
    Dim tmpTimeFilter As String
    Dim tmpCdatFilter As String
    Dim tmpRcvFilter As String
    Dim tmpOutFilter As String
    Dim tmpInfFilter As String
    Dim tmpRefFilter As String
    Dim UrnoCol As Long
    Dim SortCol As Long
    Dim count As Long
    Dim TlxRec As String
    Dim i As Long
    Dim j As Long
    Dim MemIdx As Integer
    Dim sbText As String
    Dim sbText1 As String
    Dim sbText2 As String
    Dim TimeFrom As String
    Dim TimeTo As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim clTtypFilter As String
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim TlxUrnoList As String
    Dim ConText As String
    Dim tmpAddressFilter
    On Error Resume Next
    If Not ShutDownRequested Then
        sbText1 = MyStatusBar.Panels(1).Text
        sbText2 = MyStatusBar.Panels(2).Text
        MyStatusBar.Panels(1).Text = "Loading Online Data ..."
        count = 0
        MemIdx = 7
        tmpVal = GetOnlineTimeFrame(Caller, ConText)
        TimeFrom = GetItem(tmpVal, 1, ",")
        TimeTo = GetItem(tmpVal, 2, ",")
        SqlKey = ""
        tmpTimeFilter = "(TIME BETWEEN '" & TimeFrom & "' AND '" & TimeTo & "')"
        tmpRcvFilter = ""
        If Caller = "RCV" Then
            If FormIsLoaded("RcvTelex") Then
                HiddenMain.SetStatusText 1, "LOADING ONLINE '" & Caller & "' TELEXES"
                RcvTelex.tabTelexList(0).ResetContent
                RcvTelex.tabTelexList(1).ResetContent
                RcvTelex.tabTelexList(0).SetUniqueFields "7"
                RcvTelex.tabTelexList(1).SetUniqueFields "7"
                RcvTelex.Caption = count & " " & RcvTelex.Tag
                RcvTelex.tabTelexList(0).Refresh
                tabTelexList(2).ResetContent
                tabTelexList(2).Refresh
                tmpRcvFilter = MySetUp.CheckOnlineFilter("RCV_FILTER", "")
                If tmpRcvFilter <> "" Then tmpRcvFilter = "(SERE='R' AND " & tmpRcvFilter & ")"
                SqlKey = "WHERE " & tmpTimeFilter & " AND " & tmpRcvFilter & " AND WSTA<>'X'"
                sbText = "Received Telexes ..."
                MemIdx = 4
            End If
            OnlineRcvdIsReady = True
        End If
        If Caller = "SND" Then
            If FormIsLoaded("SndTelex") Then
                HiddenMain.SetStatusText 1, "LOADING ONLINE '" & Caller & "' TELEXES"
                SndTelex.tabTelexList.ResetContent
                SndTelex.tabTelexList.SetUniqueFields "9"
                If Not FormIsLoaded("CreateTelex") Then Load CreateTelex
                If FormIsLoaded("CreateTelex") Then
                    CreateTelex.TlxFlights(0).ResetContent
                    CreateTelex.TlxFlights(1).ResetContent
                    CreateTelex.TlxFlights(0).Refresh
                    CreateTelex.TlxFlights(1).Refresh
                End If
                SndTelex.Caption = count & " " & SndTelex.Tag
                SndTelex.tabTelexList.Refresh
                SqlKey = "WHERE " & tmpTimeFilter & " AND (SERE='C' AND STAT='C' AND TTYP<>'AAD' AND TTYP<>' ') AND WSTA<>'X'"
                sbText = "Created Telexes ..."
                MemIdx = 5
            End If
        End If
        If Caller = "PND" Then
            If FormIsLoaded("PndTelex") Then
                HiddenMain.SetStatusText 1, "LOADING ONLINE '" & Caller & "' TELEXES"
                PndTelex.tabTelexList.ResetContent
                'If Not FormIsLoaded("CreateTelex") Then Load CreateTelex
                'If FormIsLoaded("CreateTelex") Then
                    'CreateTelex.TlxFlights(0).ResetContent
                    'CreateTelex.TlxFlights(1).ResetContent
                    'CreateTelex.TlxFlights(0).Refresh
                    'CreateTelex.TlxFlights(1).Refresh
                    'CreateTelex.chkView(2).Value = 1
                'End If
                PndTelex.Caption = count & " " & PndTelex.Tag
                PndTelex.tabTelexList.Refresh
                SqlKey = "WHERE " & tmpTimeFilter & " AND (SERE='P' AND STAT='C' AND TTYP<>'AAD' AND TTYP<>' ') AND WSTA<>'X'"
                sbText = "Pending Telexes ..."
                MemIdx = 5
            End If
        End If
        tmpOutFilter = ""
        If Caller = "OUT" Then
            OnlineSentIsReady = True
            If FormIsLoaded("OutTelex") Then
                HiddenMain.SetStatusText 1, "LOADING ONLINE '" & Caller & "' TELEXES"
                OutTelex.tabTelexList.ResetContent
                OutTelex.tabTelexList.SetUniqueFields "7"
                OutTelex.Caption = count & " " & OutTelex.Tag
                OutTelex.tabTelexList.Refresh
                tmpOutFilter = MySetUp.CheckOnlineFilter("OUT_FILTER", "")
                If tmpOutFilter <> "" Then tmpOutFilter = "(SERE='S' AND " & tmpOutFilter & ")"
                SqlKey = "WHERE " & tmpTimeFilter & " AND " & tmpOutFilter & " AND WSTA<>'X'"
                sbText = "Sent Telexes ..."
                MemIdx = 6
            End If
        End If
        If Caller = "INF" Then
            If FormIsLoaded("RcvInfo") Then
                HiddenMain.SetStatusText 1, "LOADING ONLINE '" & Caller & "' TELEXES"
                RcvInfo.tabTelexList(0).ResetContent
                RcvInfo.tabTelexList(1).ResetContent
                RcvInfo.Caption = count & " " & RcvInfo.Tag
                RcvInfo.tabTelexList(0).Refresh
                tmpInfFilter = MySetUp.CheckOnlineFilter("INF_FILTER", "")
                If tmpInfFilter <> "" Then tmpInfFilter = "(SERE='I' AND " & tmpInfFilter & ")"
                SqlKey = "WHERE " & tmpTimeFilter & " AND " & tmpInfFilter
                'SqlKey = "WHERE URNO>0"
                sbText = "Info Messages ..."
                MemIdx = 7
            End If
        End If
        If Caller = "REF" Then
            OnlineRefreshLoop = True
            OnlineSentIsReady = True
            OnlineRcvdIsReady = True
            HiddenMain.SetStatusText 1, "LOADING ONLINE '" & Caller & "' TELEXES"
            GetValidTimeFrame tmpVpfr, tmpVpto, True
            tmpTimeFilter = "(TIME BETWEEN '" & tmpVpfr & "' AND '" & tmpVpto & "')"
            'MsgBox NewFilterValues(8).TlxKey
            'MsgBox NewFilterValues(8).TfrKey
            clTtypFilter = BuildTtypFilter
            tmpRefFilter = "(SERE IN ('R','S'))"
            tmpCdatFilter = "(CDAT >= '" & tmpVpfr & "')"
            SqlKey = "WHERE " & tmpTimeFilter & " AND (" & clTtypFilter & ") AND " & tmpRefFilter & " AND WSTA<>'X' AND " & tmpCdatFilter
            If Len(txtAddr.Text) > 0 Then
                tmpAddressFilter = " AND (TXT1 LIKE '%" & txtAddr.Text & "%')"
                SqlKey = SqlKey & tmpAddressFilter
            End If
            'DEBUGGED AND MOVED DOWNWARDS
            'If Len(AddFolderList) > 0 Then
            '...........
            sbText = "Online Telexes ..."
            MemIdx = 8
            For i = 0 To LastFolderIndex + 1
                chkTabs(i).Caption = ""
                chkTabs(i).BackColor = vbButtonFace
            Next
            chkTitleBar(0).Caption = "0 " & chkTitleBar(0).Tag & " (Online)"
            chkTitleBar(1).Caption = "0 " & chkTitleBar(1).Tag & " (Online)"
        End If
        
        'MsgBox SqlKey
        If SqlKey <> "" Then
            MyStatusBar.Panels(2).Text = sbText
            Screen.MousePointer = 11
            tmpFields = DataPool.TlxTabFields
            DataPool.TelexData(MemIdx).ResetContent
            DataPool.TelexData(MemIdx).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            DataPool.TelexData(MemIdx).CedaHopo = UfisServer.HOPO
            DataPool.TelexData(MemIdx).CedaIdentifier = "IDX"
            DataPool.TelexData(MemIdx).CedaPort = "3357"
            DataPool.TelexData(MemIdx).CedaReceiveTimeout = "250"
            DataPool.TelexData(MemIdx).CedaRecordSeparator = vbLf
            DataPool.TelexData(MemIdx).CedaSendTimeout = "250"
            DataPool.TelexData(MemIdx).CedaServerName = UfisServer.HostName
            DataPool.TelexData(MemIdx).CedaTabext = UfisServer.TblExt
            DataPool.TelexData(MemIdx).CedaUser = gsUserName
            DataPool.TelexData(MemIdx).CedaWorkstation = UfisServer.GetMyWorkStationName
            DataPool.TelexData(MemIdx).CedaPacketSize = 500
            If CedaIsConnected Then
                CedaStatus(0).Visible = True
                CedaStatus(0).ZOrder
                CedaStatus(0).Refresh
                DataPool.TelexData(MemIdx).ShowHorzScroller False
                
                DataPool.ShowLoadProgress = True
                DataPool.TelexData(MemIdx).CedaAction "RTA", "TLXTAB", tmpFields, " ", SqlKey
                DataPool.ShowLoadProgress = False
                count = DataPool.TelexData(MemIdx).GetLineCount
                CedaStatus(0).Visible = False
                CedaStatus(0).ZOrder
                CedaStatus(0).Refresh
                If (count > 0) And (Len(AddFolderList) > 0) Then
                    tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                    tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                    If tmpDatFrom <> tmpDatTo Then
                        tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
                    Else
                        tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
                    End If
                    TlxUrnoList = ""
                    ActResult = ""
                    ActCmd = "RTA"
                    ActTable = "TFNTAB"
                    ActFldLst = "DISTINCT TURN"
                    ActCondition = tmpDatFrom
                    ActOrder = ""
                    ActDatLst = ""
                    CedaStatus(0).Visible = True
                    CedaStatus(0).ZOrder
                    CedaStatus(0).Refresh
                    retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                    If retval >= 0 Then
                        count = UfisServer.DataBuffer(0).GetLineCount - 1
                        If count >= 0 Then
                            For i = 0 To count
                                TlxUrnoList = TlxUrnoList & UfisServer.DataBuffer(0).GetLineValues(i) & ","
                            Next
                            TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                        End If
                    End If
                    CedaStatus(0).Visible = False
                    CedaStatus(0).ZOrder
                    CedaStatus(0).Refresh
                End If
                If Len(TlxUrnoList) > 0 Then
                    TlxUrnoList = CheckTlxUrnolist(TlxUrnoList, MemIdx)
                    If Len(TlxUrnoList) > 0 Then
                        CedaStatus(0).Visible = True
                        CedaStatus(0).ZOrder
                        CedaStatus(0).Refresh
                        DataPool.TelexData(MemIdx).CedaAction "RTA", "TLXTAB", tmpFields, " ", TlxUrnoList
                        CedaStatus(0).Visible = False
                        CedaStatus(0).ZOrder
                        CedaStatus(0).Refresh
                    End If
                End If
                If SaveLocalTestData Then DataPool.SaveTestData MemIdx
            Else
                If SaveLocalTestData Then
                    DataPool.TelexData(MemIdx).ShowHorzScroller False
                    If UfisServer.HostName = "LOCAL" Then DataPool.LoadTestData MemIdx
                End If
            End If
            'Me.Refresh
            'DoEvents
            If Caller = "REF" Then
                OnlineSentIsReady = True
                OnlineRcvdIsReady = True
                tabTelexList(2).ResetContent
                tabTelexList(2).ShowHorzScroller False
                tabTelexList(2).Refresh
                tabTelexList(3).ResetContent
                tabTelexList(3).ShowHorzScroller False
                tabTelexList(3).Refresh
                RcvTelex.tabTelexList(0).ResetContent
                RcvTelex.tabTelexList(0).ShowHorzScroller False
                RcvTelex.tabTelexList(0).Refresh
                RcvTelex.tabTelexList(1).ResetContent
                RcvTelex.tabTelexList(1).ShowHorzScroller False
                RcvTelex.tabTelexList(1).Refresh
                OutTelex.tabTelexList.ResetContent
                OutTelex.tabTelexList.ShowHorzScroller False
                OutTelex.tabTelexList.Refresh
            End If
            CedaStatus(0).Visible = False
            'UrnoCol = CLng(GetRealItemNo(tmpFields, "URNO"))
            SortCol = CLng(GetRealItemNo(tmpFields, "CDAT"))
            DataPool.TelexData(MemIdx).Sort CStr(SortCol), False, True
            count = DataPool.TelexData(MemIdx).GetLineCount - 1
            If Caller = "REF" Then
                If count >= 0 Then
                    For i = 0 To count
                        TlxRec = DataPool.TelexData(MemIdx).GetLineValues(i)
                        EvaluateBc 1, -1, "", "", "", "TLXTAB", "IBT", SqlKey, tmpFields, TlxRec, i
                    Next
                End If
                'tabTelexList(2).RedrawTab
                'tabTelexList(3).RedrawTab
                tabTelexList(2).OnVScrollTo 0
                tabTelexList(2).ShowHorzScroller True
                tabTelexList(2).AutoSizeColumns
                tabTelexList(3).OnVScrollTo 0
                tabTelexList(3).ShowHorzScroller True
                tabTelexList(3).AutoSizeColumns
                RcvTelex.tabTelexList(0).ShowHorzScroller True
                RcvTelex.tabTelexList(0).AutoSizeColumns
                RcvTelex.tabTelexList(1).ShowHorzScroller True
                RcvTelex.tabTelexList(1).AutoSizeColumns
                OutTelex.tabTelexList.ShowHorzScroller True
                OutTelex.tabTelexList.AutoSizeColumns
           Else
                If count > MaxOnlineCount Then count = MaxOnlineCount
                If count >= 0 Then
                    For i = 0 To count
                        TlxRec = DataPool.TelexData(MemIdx).GetLineValues(i)
                        EvaluateBc 1, -1, "", "", "", "TLXTAB", "IBT", SqlKey, tmpFields, TlxRec, i
                    Next
                End If
                DataPool.TelexData(MemIdx).ResetContent
            End If
            If Caller = "RCV" Then
                If FormIsLoaded("RcvTelex") Then
                    count = RcvTelex.tabTelexList(0).GetLineCount
                    If count > 0 Then
                        RcvTelex.Caption = CStr(count) & " " & RcvTelex.Tag
                        RcvTelex.tabTelexList(0).AutoSizeColumns
                        RcvTelex.tabTelexList(0).Refresh
                        tabTelexList(2).Refresh
                    End If
                End If
            End If
            If Caller = "SND" Then
                If FormIsLoaded("SndTelex") Then
                    count = SndTelex.tabTelexList.GetLineCount
                    If count > 0 Then
                        SndTelex.Caption = CStr(count) & " " & SndTelex.Tag
                        SndTelex.tabTelexList.AutoSizeColumns
                        SndTelex.tabTelexList.Refresh
                    End If
                End If
            End If
            If Caller = "PND" Then
                If FormIsLoaded("PndTelex") Then
                    count = PndTelex.tabTelexList.GetLineCount
                    If count > 0 Then
                        PndTelex.Caption = CStr(count) & " " & SndTelex.Tag
                        PndTelex.tabTelexList.AutoSizeColumns
                        PndTelex.tabTelexList.Refresh
                    End If
                End If
            End If
            If Caller = "OUT" Then
                If FormIsLoaded("OutTelex") Then
                    count = OutTelex.tabTelexList.GetLineCount
                    If count > 0 Then
                        OutTelex.Caption = CStr(count) & " " & OutTelex.Tag
                        OutTelex.tabTelexList.AutoSizeColumns
                        OutTelex.tabTelexList.Refresh
                    End If
                End If
            End If
            If Caller = "INF" Then
                If FormIsLoaded("RcvInfo") Then
                    count = RcvInfo.tabTelexList(0).GetLineCount
                    If count > 0 Then
                        RcvInfo.Caption = CStr(count) & " " & RcvInfo.Tag
                        RcvInfo.tabTelexList(0).AutoSizeColumns
                        RcvInfo.tabTelexList(0).Refresh
                    End If
                End If
            End If
            If Caller = "REF" Then
                OnlineSentIsReady = True
                OnlineRcvdIsReady = True
                StoreFolderValues (MemIdx)
                ActFilterValues(MemIdx) = NewFilterValues(MemIdx)
                If AreaPanel(3).Visible Then chkFlightList(0).Value = 1
                PrepareTelexExtract MemIdx, True
                'SetFolderValues MemIdx
                OnlineRefreshLoop = False
                SetTlxTabCaption 0, 2, "(Online)"
                SetTlxTabCaption 1, 3, "(Online)"
            End If
            Screen.MousePointer = 0
        End If
        MyStatusBar.Panels(1).Text = sbText1
        MyStatusBar.Panels(2).Text = sbText2
    End If
End Sub
Private Sub ReorgLayers()
    Dim i As Integer
    'ListButtons(0).ZOrder
    'fraSplitter(0).ZOrder
    'ListPanel(1).ZOrder
    'tabTelexList(0).ZOrder
    'tabTelexList(1).ZOrder
    'tabTelexList(2).ZOrder
    'tabTelexList(3).ZOrder
    'AssignButtonPanel.ZOrder
    'tabAssign(0).ZOrder
    fraListPanel.ZOrder
    UfisFolder.ZOrder
    fraTimeFilter.ZOrder
    fraFlightFilter.ZOrder
    fraTextFilter.ZOrder
    fraStatusFilter.ZOrder
    MainButtons.ZOrder
    RightCover.ZOrder
    For i = 0 To FolderButtons.UBound
        FolderButtons(i).ZOrder
    Next
    fraFolderToggles.ZOrder
    basicButtons.ZOrder
    TopButtons.ZOrder
    MyStatusBar.ZOrder
    'FipsStatusFlags.ZOrder
    CedaBusyFlag.ZOrder
End Sub
Public Function GetValidTimeFrame(Vpfr As String, Vpto As String, CheckUtc As Boolean) As Boolean
    Dim retval As Boolean
    Dim clDateFrom As String
    Dim clDateTo As String
    Dim clTimeFrom As String
    Dim clTimeTo As String
    Dim TimeVal
    retval = False
    clDateFrom = txtDateFrom.Tag
    clDateTo = txtDateTo.Tag
    clTimeFrom = Left(txtTimeFrom.Text, 2) & Right(txtTimeFrom.Text, 2) & "00"
    clTimeTo = Left(txtTimeTo.Text, 2) & Right(txtTimeTo.Text, 2) & "59"
    Vpfr = clDateFrom & clTimeFrom
    Vpto = clDateTo & clTimeTo
    If (CheckUtc) And (optLocal.Value = True) Then
        TimeVal = CedaFullDateToVb(Vpfr)
        TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
        Vpfr = Format(TimeVal, "yyyymmddhhmmss")
        TimeVal = CedaFullDateToVb(Vpto)
        TimeVal = DateAdd("n", -UtcTimeDiff, TimeVal)
        Vpto = Format(TimeVal, "yyyymmddhhmmss")
    End If
    retval = True
    GetValidTimeFrame = retval
End Function

Private Sub txtTimeTo_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) And (Chr(KeyAscii) <> ":") Then
        If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
    End If
End Sub

Private Sub UfisFolder_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    SetMouseWheelObjectCode "MAIN.UFIS.FOLDER"
End Sub

Private Sub UseCSGN_Change()
    If Trim(UseCSGN.Text) <> Trim(AtcCSGN.Text) Then
        CsgnStatus.Visible = True
    Else
        CsgnStatus.Visible = False
    End If
End Sub
Private Function LoadFlightData(Index As Integer, FetchData As Boolean) As String
    Dim tmpFrom As String
    Dim tmpTo As String
    Dim tmpTag As String
    Dim LastFrom As String
    Dim LastTo As String
    Dim UseSqlKey As String
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim CurDataFile As String
    Dim FtypList As String
    Dim sbText1 As String
    Dim sbText2 As String
    Dim tmpErrMsg As String
    Dim tmpLoopTimes As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim MaxLine As Long
    Dim RetCode As Boolean
    Dim FraWasVisible As Boolean
    Dim SqlTimeBegin
    Dim SqlTimeEnd
    Dim SqlTimeMax
    
    GetValidTimeFrame tmpFrom, tmpTo, True
    tmpTag = AreaPanel(3).Tag
    LastFrom = GetItem(tmpTag, 1, ",")
    LastTo = GetItem(tmpTag, 2, ",")
    FtypList = Trim(txtFtypFilter.Text)
    If FtypList = "" Then FtypList = "'O','S'"
    If Index = 0 Then
        tmpFields = "' ',FTYP,FLNO,STOA,ADID,DOOA,REGN,ACT3,ORG3,VIA3,TIFA,TISA,REMP,REM1,REM2,URNO,RKEY"
        'If FetchData Then
        '    tmpSqlKey = "(STOA BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "')"
        'Else
            tmpSqlKey = "(STOA BETWEEN '[LOOPVPFR]00' AND '[LOOPVPTO]59')"
        'End If
        tmpSqlKey = tmpSqlKey & " AND DES3='" & HomeAirport & "'"
        tmpSqlKey = tmpSqlKey & " AND FTYP IN (" & FtypList & ")"
    Else
        tmpFields = "' ',FTYP,FLNO,STOD,ADID,DOOD,REGN,ACT3,DES3,VIA3,TIFD,TISD,REMP,REM1,REM2,URNO,RKEY"
        'If FetchData Then
        '    tmpSqlKey = "(STOD BETWEEN '" & tmpFrom & "' AND '" & tmpTo & "')"
        'Else
            tmpSqlKey = "(STOD BETWEEN '[LOOPVPFR]00' AND '[LOOPVPTO]59')"
        'End If
        tmpSqlKey = tmpSqlKey & " AND ORG3='" & HomeAirport & "'"
        tmpSqlKey = tmpSqlKey & " AND FTYP IN (" & FtypList & ")"
    End If
    If (FetchData) And ((OnlineRcvdIsReady) And (OnlineSentIsReady)) Then
        If (LastFrom <> tmpFrom) Or (LastTo <> tmpTo) Then
            MyStatusBar.Panels(1).Text = "Loading Data ..."
            chkFlightList(0).Tag = "LOADED"
            If Index = 0 Then
                fraLoadProgress.Caption = "Loading Arrival Flights ..."
                MyStatusBar.Panels(2).Text = "Arrival Flights"
            Else
                fraLoadProgress.Caption = "Loading Departure Flights ..."
                MyStatusBar.Panels(2).Text = "Departure Flights"
            End If
            HiddenMain.SetStatusText 1, UCase(MyStatusBar.Panels(1).Text)
            HiddenMain.SetStatusText 2, UCase(MyStatusBar.Panels(2).Text)
            Me.MousePointer = 11
            Me.Refresh
            DoEvents
            tmpVpfr = tmpFrom
            tmpVpto = tmpTo
            tmpSqlKey = "WHERE " & tmpSqlKey
            tabFlightList(Index).ResetContent
            tabFlightList(Index).Refresh
            tabFlightList(Index).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            tabFlightList(Index).CedaHopo = UfisServer.HOPO
            tabFlightList(Index).CedaIdentifier = "IDX"
            tabFlightList(Index).CedaPort = "3357"
            tabFlightList(Index).CedaReceiveTimeout = "250"
            tabFlightList(Index).CedaRecordSeparator = vbLf
            tabFlightList(Index).CedaSendTimeout = "250"
            tabFlightList(Index).CedaServerName = UfisServer.HostName
            tabFlightList(Index).CedaTabext = UfisServer.TblExt
            tabFlightList(Index).CedaUser = gsUserName
            tabFlightList(Index).CedaWorkstation = UfisServer.GetMyWorkStationName
            'CurDataFile = "c:\tmp\TlxAftData" & CStr(Index) & ".txt"
            CurDataFile = UFIS_TMP & "\TlxAftData" & CStr(Index) & ".txt"
            CedaStatus(0).Visible = True
            CedaStatus(0).ZOrder
            CedaStatus(0).Refresh
            If CedaIsConnected Then
                FraWasVisible = fraLoadProgress.Visible
                fraLoadProgress.Visible = True
                fraLoadProgress.Refresh
                SqlTimeBegin = CedaFullDateToVb(tmpVpfr)
                SqlTimeMax = CedaFullDateToVb(tmpVpto)
                While (SqlTimeBegin <= SqlTimeMax) And (Not StopLoading)
                    If Not StopLoading Then
                        SqlTimeEnd = DateAdd("n", 1439, SqlTimeBegin)
                        If SqlTimeEnd > SqlTimeMax Then SqlTimeEnd = SqlTimeMax
                        tmpVpfr = Format(SqlTimeBegin, "yyyymmddhhmm")
                        tmpVpto = Format(SqlTimeEnd, "yyyymmddhhmm")
                        txtLoadFrom.Text = Format(SqlTimeBegin, MySetUp.DefDateFormat)
                        txtLoadTo.Text = Format(SqlTimeEnd, MySetUp.DefDateFormat)
                        txtLoadTimeFrom.Text = Format(SqlTimeBegin, "hh:mm")
                        txtLoadTimeTo.Text = Format(SqlTimeEnd, "hh:mm")
                        MaxLine = tabFlightList(Index).GetLineCount
                        fraLoadProgress.Caption = CStr(MaxLine) & " Flights Loaded ..."
                        fraLoadProgress.Refresh
                        'Me.Refresh
                        DoEvents
                        UseSqlKey = tmpSqlKey
                        UseSqlKey = Replace(UseSqlKey, "[LOOPVPFR]", tmpVpfr, 1, -1, vbBinaryCompare)
                        UseSqlKey = Replace(UseSqlKey, "[LOOPVPTO]", tmpVpto, 1, -1, vbBinaryCompare)
                        RetCode = tabFlightList(Index).CedaAction("GFR", "AFTTAB", tmpFields, "", UseSqlKey)
                        If (RetCode = False) And (tmpErrMsg = "") Then tmpErrMsg = tmpErrMsg & tabFlightList(Index).GetLastCedaError
                        Me.Refresh
                        SqlTimeBegin = DateAdd("n", 1, SqlTimeEnd)
                    End If
                Wend
                If SaveLocalTestData Then tabFlightList(Index).WriteToFile CurDataFile, False
                fraLoadProgress.Visible = FraWasVisible
                fraLoadProgress.Refresh
            Else
                If SaveLocalTestData Then tabFlightList(Index).ReadFromFile CurDataFile
            End If
            CedaStatus(0).Visible = False
            CedaStatus(0).ZOrder
            CedaStatus(0).Refresh
            tabFlightList(Index).Sort "2", True, True
            tabFlightList(Index).AutoSizeColumns
            tabFlightList(Index).IndexCreate "AFTURNO", 15
            tabFlightList(Index).IndexCreate "AFTRKEY", 16
            If Index = 1 Then AreaPanel(3).Tag = tmpFrom & "," & tmpTo
            MyStatusBar.Panels(1).Text = sbText1
            MyStatusBar.Panels(2).Text = sbText2
            Me.MousePointer = 0
            Me.Refresh
            DoEvents
        End If
    End If
    LoadFlightData = tmpSqlKey
End Function

Private Sub SelectFlightInfo(AftUrno As String, MemLineNo As Long)
    Dim LineNo As Long
    Dim CurIdx As Integer
    Dim HitList As String
    Dim tmpRkey As String
    Dim tmpFTag As String
    Dim tmpTag As String
    Dim tmpFld As String
    Dim UrnoList As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As Integer
    Dim count As Integer
    Dim CurLin As Integer
    Dim itm As Integer
    Dim CurUrno As String
    Dim i As Integer
    Dim AftuCol As Long
    Dim tmpData As String
    Dim gPos As Integer
    
    HitList = ""
    For CurIdx = 0 To 1
        tmpFTag = tabFlightList(CurIdx).Tag
        tmpFld = GetItem(tmpFTag, 1, ",")
        itm = 2
        tmpTag = GetItem(tmpFTag, itm, ",")
        While tmpTag <> ""
            If tmpFld = "URNO" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", tmpTag, 0)
            If tmpFld = "RKEY" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTRKEY", tmpTag, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbWhite
            End If
            itm = itm + 1
            tmpTag = GetItem(tmpFTag, itm, ",")
        Wend
        tabFlightList(CurIdx).Refresh
        tabFlightList(CurIdx).Tag = ""
        chkFlightList(CurIdx + 1).ForeColor = vbBlack
    Next
    UrnoList = AftUrno
    If chkAssFolder.Enabled Then
'        ActResult = ""
'        ActCmd = "RTA"
'        ActTable = "TLKTAB"
'        ActFldLst = "FURN"
'        ActCondition = "WHERE TURN = " & CurrentUrno.Text
'        ActOrder = ""
'        ActDatLst = ""
'        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
'        If RetVal >= 0 Then
'            count = UfisServer.DataBuffer(0).GetLineCount - 1
'            If count >= 0 Then
'                UrnoList = ""
'                For CurLin = count To 0 Step -1
'                    UrnoList = UrnoList & UfisServer.DataBuffer(0).GetLineValues(CurLin) & ","
'                Next
'                UrnoList = Left(UrnoList, Len(UrnoList) - 1)
'                If AftUrno <> "" And AftUrno <> "0" Then
'                    UrnoList = UrnoList & "," & AftUrno
'                End If
'            End If
'        End If
        AftuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "USEC"))
        tmpData = DataPool.TelexData(CurMem).GetColumnValue(MemLineNo, AftuCol)
        If AftUrno <> "" And AftUrno <> "0" Then
            If Len(tmpData) > 0 Then
                tmpData = tmpData & "," & AftUrno
            Else
                tmpData = AftUrno
            End If
        End If
        gPos = InStr(tmpData, "#")
        While gPos > 0
            Mid(tmpData, gPos, 1) = ","
            gPos = InStr(tmpData, "#")
        Wend
        UrnoList = tmpData
    End If
    itm = 1
    CurUrno = GetItem(UrnoList, itm, ",")
    While CurUrno <> ""
        chkFlightList(3).Enabled = False
        If CurUrno <> "" And Len(CurUrno) <= 10 Then
            CurIdx = 0
            HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", CurUrno, 0)
            If HitList = "" Then
                CurIdx = 1
                HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", CurUrno, 0)
            End If
            If HitList <> "" Then
                LineNo = Val(HitList)
                If tabFlightList(CurIdx).Tag = "" Then tabFlightList(CurIdx).OnVScrollTo LineNo
                If CurIdx = 0 Then
                    tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbYellow
                Else
                    tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbGreen
                End If
                If Len(tabFlightList(CurIdx).Tag) = 0 Then
                    tabFlightList(CurIdx).Tag = "URNO," & CurUrno
                Else
                    tabFlightList(CurIdx).Tag = tabFlightList(CurIdx).Tag & "," & CurUrno
                End If
                chkFlightList(CurIdx + 1).Value = 1
                tmpRkey = tabFlightList(CurIdx).GetColumnValue(LineNo, 16)
                chkFlightList(3).Enabled = True
                If CurIdx = 1 Then CurIdx = 0 Else CurIdx = 1
                HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTRKEY", tmpRkey, 0)
                If HitList <> "" Then
                    LineNo = Val(HitList)
                    tabFlightList(CurIdx).OnVScrollTo LineNo
                    If CurIdx = 0 Then
                        tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbYellow
                    Else
                        tabFlightList(CurIdx).SetLineColor LineNo, vbBlack, vbGreen
                    End If
                    chkFlightList(CurIdx + 1).ForeColor = vbBlue
                    If Len(tabFlightList(CurIdx).Tag) = 0 Then
                        tabFlightList(CurIdx).Tag = "RKEY," & tmpRkey
                    Else
                        tabFlightList(CurIdx).Tag = tabFlightList(CurIdx).Tag & "," & tmpRkey
                    End If
                Else
                    'chkFlightList(1).Value = 0
                    'chkFlightList(2).Value = 0
                End If
            Else
                chkFlightList(1).Value = 0
                chkFlightList(2).Value = 0
            End If
        End If
        itm = itm + 1
        CurUrno = GetItem(UrnoList, itm, ",")
    Wend
    For CurIdx = 0 To 1
        tabFlightList(CurIdx).Refresh
    Next
End Sub

Private Sub SyncFlightFlist()
    Dim LineNo As Long
    Dim CurIdx As Integer
    Dim tmpTag As String
    Dim HitList As String
    Dim tmpFld As String
    For CurIdx = 0 To 1
        tmpTag = tabFlightList(CurIdx).Tag
        tmpFld = GetItem(tmpTag, 1, ",")
        tmpTag = GetItem(tmpTag, 2, ",")
        If tmpFld = "URNO" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", tmpTag, 0)
        If tmpFld = "RKEY" Then HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTRKEY", tmpTag, 0)
        If HitList <> "" Then
            LineNo = Val(HitList)
            tabFlightList(CurIdx).OnVScrollTo LineNo
        End If
        tabFlightList(CurIdx).Refresh
    Next
End Sub
Public Sub CountFlightTelex()
    Dim CurIdx As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim AftUrno As String
    Dim HitList As String
    Dim TlxCurLine As Long
    Dim TlxMaxLine As Long
    Dim TlxFlnu As String
    Dim TlxHitList As Long
    Dim AftUrnoList As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As String
    Dim count As Long
    Dim itm As Integer
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim TlxUrnoList As String
    Dim tmpData As String
    Dim ConText As String
    
    If (MainIsOnlineNow) And ((Not OnlineRcvdIsReady) Or (Not OnlineSentIsReady)) Then
        Exit Sub
    End If
    
        'If BcLed(3).Visible = False Then
        '    BcLed(3).Visible = True
        '    BcLed(2).Visible = False
        'Else
        '    BcLed(3).Visible = False
        '    BcLed(2).Visible = True
        'End If
    
    If chkTlxCount.Value = 0 Then
        Exit Sub
    End If
    
    Screen.MousePointer = 11
    DataPool.TelexData(CurMem).SetInternalLineBuffer True
    For CurIdx = 0 To 1
        MaxLine = DataPool.TelexData(CurMem).GetLineCount
        If MaxLine > 0 Then
            If CurIdx = 0 Then
                MyStatusBar.Panels(1).Text = "Counting Telexes"
                MyStatusBar.Panels(2).Text = "For Arrival Flights"
            Else
                MyStatusBar.Panels(2).Text = "For Departure Flights"
            End If
            HiddenMain.SetStatusText 1, UCase(MyStatusBar.Panels(1).Text)
            HiddenMain.SetStatusText 2, UCase(MyStatusBar.Panels(2).Text)
            MaxLine = tabFlightList(CurIdx).GetLineCount - 1
            For CurLine = 0 To MaxLine
                AftUrno = tabFlightList(CurIdx).GetColumnValue(CurLine, 15)
                tmpData = tabFlightList(CurIdx).GetColumnValue(CurLine, 2)
                ConText = MyStatusBar.Panels(2).Text & " [" & tmpData & "]"
                HiddenMain.SetStatusText 2, ConText
                HitList = DataPool.TelexData(CurMem).GetLinesByIndexValue("FLNU", AftUrno, 0)
                If Val(HitList) = 0 Then HitList = ""
                If CurMem <> 8 Then
                    HitList = DataPool.TelexData(CurMem).GetLinesByIndexValue("FLNU", AftUrno, 0)
                    If Val(HitList) = 0 Then HitList = ""
                Else
                    TlxHitList = 0
                    TlxMaxLine = DataPool.TelexData(CurMem).GetLineCount - 1
                    For TlxCurLine = 0 To TlxMaxLine
                        TlxFlnu = DataPool.TelexData(CurMem).GetColumnValue(TlxCurLine, 6)
                        If TlxFlnu = AftUrno Then
                            TlxHitList = TlxHitList + 1
                        End If
                    Next
                    If TlxHitList = 0 Then
                        HitList = ""
                    Else
                        HitList = CStr(TlxHitList)
                    End If
                End If
                tabFlightList(CurIdx).SetColumnValue CurLine, 0, HitList
            Next
        Else
            HitList = ""
            MaxLine = tabFlightList(CurIdx).GetLineCount - 1
            For CurLine = 0 To MaxLine
                tabFlightList(CurIdx).SetColumnValue CurLine, 0, HitList
            Next
        End If
        tabFlightList(CurIdx).Refresh
    Next
    DataPool.TelexData(CurMem).SetInternalLineBuffer False
    MaxLine = DataPool.TelexData(CurMem).GetLineCount
    If (MaxLine > 0) And (chkAssFolder.Enabled) Then
        MyStatusBar.Panels(1).Text = "Counting Assigned Telexes"
        HiddenMain.SetStatusText 1, UCase(MyStatusBar.Panels(1).Text)
        MyStatusBar.Panels(2).Text = ""
        ConText = MyStatusBar.Panels(2).Text
        HiddenMain.SetStatusText 2, ConText
        tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
        If Len(Trim(txtTimeFrom.Text)) = 5 Then
            tmpDatFrom = tmpDatFrom & Mid(txtTimeFrom.Text, 1, 2) & Mid(txtTimeFrom.Text, 4, 2) & "00"
        Else
            tmpDatFrom = tmpDatFrom & Mid(txtTimeFrom.Text, 1, 2) & Mid(txtTimeFrom.Text, 3, 2) & "00"
        End If
        tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
        If Len(Trim(txtTimeTo.Text)) = 5 Then
            tmpDatTo = tmpDatTo & Mid(txtTimeTo.Text, 1, 2) & Mid(txtTimeTo.Text, 4, 2) & "00"
        Else
            tmpDatTo = tmpDatTo & Mid(txtTimeTo.Text, 1, 2) & Mid(txtTimeTo.Text, 3, 2) & "00"
        End If
        tmpDatFrom = "WHERE DATC BETWEEN '" & tmpDatFrom & "' AND '" & tmpDatTo & "'"
        AftUrnoList = ""
        TlxUrnoList = ""
        ActResult = ""
        ActCmd = "RTA"
        ActTable = "TLKTAB"
        ActFldLst = "FURN,TURN"
        ActCondition = tmpDatFrom
        ActOrder = ""
        ActDatLst = ""
        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
        If retval >= 0 Then
            count = UfisServer.DataBuffer(0).GetLineCount - 1
            If count >= 0 Then
                For CurLine = 0 To count
                    tmpData = UfisServer.DataBuffer(0).GetLineValues(CurLine)
                    AftUrnoList = AftUrnoList & GetItem(tmpData, 1, ",") & ","
                    TlxUrnoList = TlxUrnoList & GetItem(tmpData, 2, ",") & ","
                Next
                If AftUrnoList <> "" Then
                    AftUrnoList = Left(AftUrnoList, Len(AftUrnoList) - 1)
                    TlxUrnoList = Left(TlxUrnoList, Len(TlxUrnoList) - 1)
                    itm = 1
                    AftUrno = GetItem(AftUrnoList, itm, ",")
                    While AftUrno <> ""
                        AddCountFlightTelex AftUrno
                        itm = itm + 1
                        AftUrno = GetItem(AftUrnoList, itm, ",")
                    Wend
                End If
                If TlxUrnoList <> "" Then
                    AssignFlightTelex TlxUrnoList, AftUrnoList
                End If
            End If
        End If
    End If
    MyStatusBar.Panels(1).Text = ""
    MyStatusBar.Panels(2).Text = ""
    Screen.MousePointer = 0
End Sub

Private Sub AssignFlightTelex(TlxUrnoList As String, AftUrnoList As String)
    Dim AftuCol As Long
    Dim UrnoCol As Long
    Dim i As Integer
    Dim TlxUrno As String
    Dim AftUrno As String
    Dim ItemNo As Integer
    Dim tmpData As String
    Dim tmpItem As Long
    Dim count As Integer
    Dim HitList As String
    Dim CurLine As Integer
    Dim tmpAddTxt As String
    Dim ConText As String
    
    AftuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "USEC"))
    UrnoCol = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
    'FlnoCol = GetItemNo(DataPool.TlxTabFields, "FLNO") - 1
'    For i = 0 To DataPool.TelexData(CurMem).GetLineCount - 1
'        TlxUrno = DataPool.TelexData(CurMem).GetColumnValue(i, UrnoCol)
'        ItemNo = GetItemNo(TlxUrnoList, TlxUrno)
'        If ItemNo > 0 Then
'            AftUrno = GetItem(AftUrnoList, ItemNo, ",")
'            tmpData = DataPool.TelexData(CurMem).GetColumnValue(i, AftuCol)
'            If Len(tmpData) = 0 Then
'                DataPool.TelexData(CurMem).SetColumnValue i, AftuCol, AftUrno
'            Else
'                If InStr(tmpData, AftUrno) <= 0 Then
'                    tmpData = tmpData & "#" & AftUrno
'                    DataPool.TelexData(CurMem).SetColumnValue i, AftuCol, tmpData
'                End If
'            End If
'            AddCountFlightTelex AftUrno
'            tmpItem = ItemNo
'            tmpData = RemoveItem(TlxUrnoList, tmpItem)
'            TlxUrnoList = tmpData
'            tmpData = RemoveItem(AftUrnoList, tmpItem)
'            AftUrnoList = tmpData
'            If Len(TlxUrnoList) = 0 Then
'                Exit For
'            End If
'        End If
'    Next
    tmpData = ""
    count = ItemCount(TlxUrnoList, ",")
    For i = 1 To count
        TlxUrno = GetItem(TlxUrnoList, i, ",")
        HitList = DataPool.TelexData(CurMem).GetLinesByIndexValue("URNO", TlxUrno, 0)
        If HitList <> "" Then
            CurLine = Val(HitList)
            AftUrno = GetItem(AftUrnoList, i, ",")
            tmpData = DataPool.TelexData(CurMem).GetColumnValue(CurLine, AftuCol)
            If Len(tmpData) = 0 Then
                DataPool.TelexData(CurMem).SetColumnValue CurLine, AftuCol, AftUrno
            Else
                If InStr(tmpData, AftUrno) <= 0 Then
                    tmpData = tmpData & "#" & AftUrno
                    DataPool.TelexData(CurMem).SetColumnValue CurLine, AftuCol, tmpData
                End If
            End If
            AddCountFlightTelex AftUrno
        End If
    Next
End Sub
Private Sub AddCountFlightTelex(AftUrno As String)
    Dim CurIdx As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpCount As String
    Dim found As Boolean
    Dim HitList As String
    
    Screen.MousePointer = 11
    found = False
    For CurIdx = 0 To 1
'        MaxLine = tabFlightList(CurIdx).GetLineCount - 1
'        For CurLine = 0 To MaxLine
'            If AftUrno = tabFlightList(CurIdx).GetColumnValue(CurLine, 15) Then
'                tmpCount = tabFlightList(CurIdx).GetColumnValue(CurLine, 0)
'                If tmpCount = "" Then
'                    tmpCount = "1"
'                Else
'                    tmpCount = tmpCount + 1
'                End If
'                tabFlightList(CurIdx).SetColumnValue CurLine, 0, tmpCount
'                found = True
'                Exit For
'            End If
'        Next
        HitList = tabFlightList(CurIdx).GetLinesByIndexValue("AFTURNO", AftUrno, 0)
        If HitList <> "" Then
            CurLine = Val(HitList)
            tmpCount = tabFlightList(CurIdx).GetColumnValue(CurLine, 0)
            If tmpCount = "" Then
                tmpCount = "1"
            Else
                tmpCount = tmpCount + 1
            End If
            tabFlightList(CurIdx).SetColumnValue CurLine, 0, tmpCount
            found = True
            Exit For
        End If
        tabFlightList(CurIdx).Refresh
        If found Then Exit For
    Next
    Screen.MousePointer = 0
End Sub

Public Sub MsgToImportTool(MyMsg As String)
    Static CurMsgNum As Long
    Dim tmpMsg As String
    Dim tmpData As String
    On Error GoTo errhdl
    If (Not ImportIsConnected) And (Not ImportIsPending) Then
        ImportIsPending = True
        InfoFromClient.Text = "WAIT"
        If Me.WindowState <> vbMinimized Then
            InfoToClient.Text = CStr(CurMsgNum) & ",0,INIT," & CStr(Me.Left + 300) & "," & CStr(Me.Top + 300) & "," & CStr(OnTop.Value)
        Else
            InfoToClient.Text = CStr(CurMsgNum) & ",0,INIT,1200,1200" & "," & CStr(OnTop.Value)
        End If
        tmpData = PathToImportTool & " COMPARE"
        tmpData = tmpData & " " & gsUserName
        Shell tmpData
        If RcvInfo.chkImp(0).Value = 1 Then
            CurMsgNum = CurMsgNum + 1
            tmpMsg = CStr(CurMsgNum) & "," & "0,ATSET"
            tmpMsg = Replace(tmpMsg, ",", Chr(15), 1, -1, vbBinaryCompare)
            InfoSpooler.InsertTextLine tmpMsg, False
        End If
        If RcvInfo.chkImp(1).Value = 1 Then
            CurMsgNum = CurMsgNum + 1
            tmpMsg = CStr(CurMsgNum) & "," & "0,AISET"
            tmpMsg = Replace(tmpMsg, ",", Chr(15), 1, -1, vbBinaryCompare)
            InfoSpooler.InsertTextLine tmpMsg, False
        End If
    End If
    CurMsgNum = CurMsgNum + 1
    tmpMsg = CStr(CurMsgNum) & "," & MyMsg
    tmpMsg = Replace(tmpMsg, ",", Chr(15), 1, -1, vbBinaryCompare)
    InfoSpooler.InsertTextLine tmpMsg, False
    InfoSpooler.AutoSizeColumns
    InfoSpooler.Refresh
    SpoolTimer.Enabled = True
    Exit Sub
errhdl:
    tmpMsg = Err.Description & vbNewLine & PathToImportTool
    If MyMsgBox.CallAskUser(0, 0, 0, "Application Control", tmpMsg, "stop", "", UserAnswer) = 1 Then DoNothing
    InfoSpooler.ResetContent
    InfoSpooler.Refresh
    SpoolTimer.Enabled = False
End Sub

Private Sub CheckCompareInfo()
    If ImportIsConnected Then
        If InfoFromClient.LinkMode = 0 Then
            InfoFromClient.LinkTopic = "ImportFlights|CompareInfo"
            InfoFromClient.LinkItem = "InfoToServer"
            InfoFromClient.LinkMode = vbLinkAutomatic
        End If
        If MsgFromClient.LinkMode = 0 Then
            MsgFromClient.LinkTopic = "ImportFlights|CompareInfo"
            MsgFromClient.LinkItem = "MsgToServer"
            MsgFromClient.LinkMode = vbLinkAutomatic
        End If
    End If
End Sub

Private Sub chkConfig_Click()

    chkConfig.Value = 0
    ConfigTool.Visible = True
End Sub

Private Sub TelexTabToDataRelation(MemIdx As Long)
    Dim TlxTabMax As Long
    Dim TlxDataMax As Long
    Dim i As Long
    Dim j As Long
    Dim TlxTabUrno As String
    Dim TlxDataUrno As String
    Dim UrnoCol As Long

    UrnoCol = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
    TlxDataMax = DataPool.TelexData(MemIdx).GetLineCount - 1
    TlxTabMax = tabTelexList(2).GetLineCount - 1
    For i = 0 To TlxTabMax
        tabTelexList(2).SetColumnValue i, 8, " "
        TlxTabUrno = tabTelexList(2).GetColumnValue(i, 7)
        For j = 0 To TlxDataMax
            TlxDataUrno = DataPool.TelexData(MemIdx).GetColumnValue(j, UrnoCol)
            If TlxDataUrno = TlxTabUrno Then
                tabTelexList(2).SetColumnValue i, 8, CStr(j)
                Exit For
            End If
        Next
    Next
    TlxTabMax = tabTelexList(3).GetLineCount - 1
    For i = 0 To TlxTabMax
        tabTelexList(3).SetColumnValue i, 8, " "
        TlxTabUrno = tabTelexList(3).GetColumnValue(i, 7)
        For j = 0 To TlxDataMax
            TlxDataUrno = DataPool.TelexData(MemIdx).GetColumnValue(j, UrnoCol)
            If TlxDataUrno = TlxTabUrno Then
                tabTelexList(3).SetColumnValue i, 8, CStr(j)
                Exit For
            End If
        Next
    Next
End Sub

Private Function GetMemRecNbr(Index As Integer, LineNo As Long, MemIdx As Integer) As String
    Dim TlxDataMax As Long
    Dim i As Long
    Dim TlxTabUrno As String
    Dim TlxDataUrno As String
    Dim Result As String
    Dim UrnoCol As Long

    UrnoCol = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
    Result = ""
    TlxDataMax = DataPool.TelexData(MemIdx).GetLineCount - 1
    tabTelexList(Index).SetColumnValue LineNo, 8, " "
    TlxTabUrno = tabTelexList(Index).GetColumnValue(LineNo, 7)
    For i = 0 To TlxDataMax
        TlxDataUrno = DataPool.TelexData(MemIdx).GetColumnValue(i, UrnoCol)
        If TlxDataUrno = TlxTabUrno Then
            tabTelexList(Index).SetColumnValue LineNo, 8, CStr(i)
            Result = CStr(i)
            Exit For
        End If
    Next
    GetMemRecNbr = Result
End Function

Private Sub tabAssign_InplaceEditCell(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
'MsgBox Index & vbLf & LineNo & vbLf & ColNo & vbLf & NewValue & vbLf & OldValue

End Sub

Private Sub tabAssign_EditPositionChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    On Error Resume Next

    If LineNo >= 0 Then
        tabAssign(0).SetColumnValue LineNo, 5, "N"
        If InsertTabAssign Then
            tabAssign(0).RedrawTab
            tabAssign(0).Refresh
            If tabAssign(0).Visible Then
                tabAssign(0).SetFocus
            End If
            tabAssign(0).PostEnterBehavior = 3
            tabAssign(0).SetCurrentSelection LineNo
            tabAssign(0).SetInplaceEdit LineNo, 1, False
            InsertTabAssign = False
        End If
    End If
End Sub

Private Sub tabAssign_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    If LineNo >= 0 Then
        If Key = 13 Then
            chkTlxAssign.BackColor = vbRed
            chkTlxAssign.ForeColor = vbWhite
            tabAssign(0).SetColumnValue LineNo, 5, "N"
            tabAssign(0).InsertTextLineAt LineNo + 1, " , , , , , ", False
            InsertTabAssign = True
        End If
    End If
End Sub

Private Sub tabFlightList_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    
    If Index = 2 And LineNo >= 0 Then
        If Key = 13 Then
            EditMode = True
            chkFlightList(6).Value = 0
        End If
    End If
End Sub

Public Sub SetFolderValues(CurMemIdx As Integer)
    Dim i As Integer
    Dim AllCount As Long
    Dim CurTypeList As String
    Dim TypCount As Long
    Dim itm As Integer
    Dim CurTtyp As String
    Dim LineList As String
    Dim TtypCol As Long
    Dim FlnuCol As Long
    Dim UrnoCol As Long
    Dim tmpDatFrom As String
    Dim tmpDatTo As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As Integer
    Dim ChkFolderList As String
    Dim ChkName As String
    Dim tmpAddTxt As String
    Dim ConText As String
    
    If (MainIsOnlineNow) And ((Not OnlineRcvdIsReady) Or (Not OnlineSentIsReady)) Then
        Exit Sub
    End If
    AllCount = DataPool.TelexData(CurMemIdx).GetLineCount
    If AllCount <= 0 Then
        Exit Sub
    End If
    TtypCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "TTYP"))
    FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
    UrnoCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "URNO"))
    DataPool.TelexData(CurMemIdx).IndexCreate "TTYP", TtypCol
    DataPool.TelexData(CurMemIdx).SetInternalLineBuffer True
    AllCount = 0
    ChkFolderList = "," & AddFolderList & ","
    For i = 0 To fraFolder.UBound - 2
        CurTypeList = fraFolder(i).Tag
        ChkName = "," & chkFolder(i).Caption & ","
        If InStr(ChkFolderList, ChkName) > 0 Then
            If ReadFolderCount Then
                tmpDatFrom = Mid(txtDateFrom.Text, 7, 4) & Mid(txtDateFrom.Text, 4, 2) & Mid(txtDateFrom.Text, 1, 2)
                tmpDatTo = Mid(txtDateTo.Text, 7, 4) & Mid(txtDateTo.Text, 4, 2) & Mid(txtDateTo.Text, 1, 2)
                If tmpDatFrom <> tmpDatTo Then
                    tmpDatFrom = "WHERE (DATC = '" & tmpDatFrom & "' OR DATC = '" & tmpDatTo & "')"
                Else
                    tmpDatFrom = "WHERE DATC = '" & tmpDatFrom & "'"
                End If
                ActResult = ""
                ActCmd = "RTA"
                ActTable = "TFNTAB"
                ActFldLst = "DISTINCT TURN"
                tmpAddTxt = chkFolder(i).Caption
                ConText = "ASSIGNED TO FOLDER" & " [ " & tmpAddTxt & " ]"
                HiddenMain.SetStatusText 2, ConText
                HiddenMain.CedaStatus(2).Refresh
                ActCondition = tmpDatFrom & " AND FNAM = '" & chkFolder(i).Caption & "'"
                ActOrder = ""
                ActDatLst = ""
                Screen.MousePointer = 11
                retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                If retval >= 0 Then
                    chkTabs(i).Caption = UfisServer.DataBuffer(0).GetLineCount
                Else
                    chkTabs(i).Caption = ""
                End If
                chkTabs(i).BackColor = vbButtonFace
            End If
        Else
            TypCount = 0
            itm = 1
            CurTtyp = GetItem(CurTypeList, itm, ",")
            While CurTtyp <> ""
                LineList = DataPool.TelexData(CurMemIdx).GetLinesByIndexValue("TTYP", CurTtyp, 0)
                TypCount = TypCount + Val(LineList)
                itm = itm + 1
                CurTtyp = GetItem(CurTypeList, itm, ",")
            Wend
            If TypCount > 0 Then
                chkTabs(i).Caption = CStr(TypCount)
            Else
                chkTabs(i).Caption = ""
            End If
            chkTabs(i).BackColor = vbButtonFace
            AllCount = AllCount + TypCount
        End If
    Next
    TypCount = DataPool.TelexData(CurMemIdx).GetLineCount
    chkTabs(AllTypesCol).Caption = CStr(TypCount)
    'If (TypCount - AllCount) > 0 Then chkTabs(OtherTypesCol).Caption = CStr(TypCount - AllCount)
    chkTabs(OtherTypesCol).Caption = CStr(TypCount - AllCount)
    DataPool.TelexData(CurMemIdx).SetInternalLineBuffer False
    DataPool.TelexData(CurMemIdx).IndexCreate "FLNU", FlnuCol
    DataPool.TelexData(CurMemIdx).IndexCreate "URNO", UrnoCol
    Screen.MousePointer = 0
End Sub

Private Sub EditFlightsToTelex()
    Dim LineNo As Long
    Dim SqlKey As String
    Dim UseServerTime
    Dim count As Integer
    Dim idx As Integer
    Dim tmpDat As String
    Dim tmpStat As String
    Dim tmpIdx As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As String
    Dim tmpMin As String
    Dim tmpDate As String
    On Error Resume Next
    
    If Not EditMode Then
        tabFlightList(2).ResetContent
        If Len(CurrentUrno.Text) > 0 Then
            tabFlightList(2).CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            tabFlightList(2).CedaHopo = UfisServer.HOPO
            tabFlightList(2).CedaIdentifier = "IDX"
            tabFlightList(2).CedaPort = "3357"
            tabFlightList(2).CedaReceiveTimeout = "250"
            tabFlightList(2).CedaRecordSeparator = vbLf
            tabFlightList(2).CedaSendTimeout = "250"
            tabFlightList(2).CedaServerName = UfisServer.HostName
            tabFlightList(2).CedaTabext = UfisServer.TblExt
            tabFlightList(2).CedaUser = gsUserName
            tabFlightList(2).CedaWorkstation = UfisServer.GetMyWorkStationName
            If CedaIsConnected Then
                CedaStatus(0).Visible = True
                CedaStatus(0).ZOrder
                CedaStatus(0).Refresh
                SqlKey = "WHERE TURN = " & CurrentUrno.Text & " ORDER BY FLNO,DATC"
                tabFlightList(2).CedaAction "RTA", "TLKTAB", "ADID,FLNO,FDAT,URNO,TURN,FURN,DATC", " ", SqlKey
                CedaStatus(0).Visible = False
                CedaStatus(0).ZOrder
                CedaStatus(0).Refresh
            End If
            count = tabFlightList(2).GetLineCount - 1
            If count >= 0 Then
                For idx = 0 To count
                    tmpDat = tabFlightList(2).GetColumnValue(idx, 6)
                    UseServerTime = CedaFullDateToVb(tmpDat)
                    If optLocal.Value = True Then
                        UseServerTime = DateAdd("n", UtcTimeDiff, UseServerTime)
                    End If
                    'tmpDat = Format(UseServerTime, "ddmmmyy\/hh:mm")
                    'tmpDat = UCase(tmpDat)
                    tmpDat = Format(UseServerTime, "yyyymmddhhmmss")
                    tmpMin = Mid(tmpDat, 9, 2) & ":" & Mid(tmpDat, 11, 2)
                    tmpDat = Mid(tmpDat, 1, 8)
                    tmpDate = DecodeSsimDayFormat(tmpDat, "CEDA", "SSIM2")
                    tmpDat = tmpDate & "/" & tmpMin
                    tabFlightList(2).SetColumnValue idx, 2, tmpDat
                Next
            End If
            tabFlightList(2).AutoSizeColumns
            tabFlightList(2).RedrawTab
            tmpStat = ""
            If count >= 0 Then
                tmpStat = "A"
            Else
                ActResult = ""
                ActCmd = "RTA"
                ActTable = "TFNTAB"
                ActFldLst = "TURN"
                ActCondition = "WHERE TURN = " & CurrentUrno.Text
                ActOrder = ""
                ActDatLst = ""
                retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                If retval < 0 Then
                    If UpdateWstaOnClick = True Then tmpStat = "V"
                End If
            End If
            If Len(tmpStat) > 0 Then
                For idx = 2 To 3
                    If tabTelexList(idx).Visible = True Then
                        tmpIdx = tabTelexList(idx).GetCurrentSelected
                        tabTelexList(idx).SetColumnValue tmpIdx, 1, tmpStat
                        tabTelexList(idx).Refresh
                    End If
                Next
                UpdateTelexStatus tmpStat
            End If
        End If
    End If
    tabFlightList(2).InsertTextLine " , , , , , , ", False
    'tabFlightList(2).RedrawTab
    tabFlightList(2).Refresh
    If tabFlightList(2).Visible Then
        tabFlightList(2).SetFocus
    End If
    tabFlightList(2).PostEnterBehavior = 3
    LineNo = tabFlightList(2).GetLineCount - 1
    tabFlightList(2).SetCurrentSelection LineNo
    tabFlightList(2).SetInplaceEdit LineNo, 0, False
    EditMode = False
End Sub

Private Sub AssignFlightsToTelex()
    Dim MaxLine As Long
    Dim i As Long
    Dim j As Long
    Dim tmpFlno As String
    Dim tmpDat As String
    Dim tmpFdat As String
    Dim tmpAdid As String
    Dim tmpFurn As String
    Dim ErrorMsg As String
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As String
    Dim tmpResult As String
    Dim tmpTime As String
    Dim SqlKey As String
    Dim tmpAlc2 As String
    Dim tmpAlc3 As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpFkey As String
    
    If Len(CurrentUrno.Text) > 0 Then
        TelexPoolHead.MousePointer = 11
        MaxLine = tabFlightList(2).GetLineCount - 1
        For i = 0 To MaxLine
            tmpAdid = Trim(tabFlightList(2).GetColumnValue(i, 0))
            If tmpAdid = RepeatChar And i > 0 Then
                tmpAdid = Trim(tabFlightList(2).GetColumnValue(i - 1, 0))
                tabFlightList(2).SetColumnValue i, 0, tmpAdid
                tmpFlno = Trim(tabFlightList(2).GetColumnValue(i - 1, 1))
                tabFlightList(2).SetColumnValue i, 1, tmpFlno
            End If
            tmpFlno = Trim(tabFlightList(2).GetColumnValue(i, 1))
            If Len(Trim(tabFlightList(2).GetColumnValue(i, 2))) < 13 Then
                tmpDat = Mid(Trim(tabFlightList(2).GetColumnValue(i, 2)), 1, 7)
                If Len(tmpDat) = 0 Then
                    tmpDat = Mid(GetTimeStamp(0), 7, 2)
                End If
                If Len(tmpDat) = 2 Then tmpDat = ExpandMonth(tmpDat)
                If Len(tmpDat) = 6 Then
                    tmpFdat = "20" & Mid(tmpDat, 5, 2) & Mid(tmpDat, 3, 2) & Mid(tmpDat, 1, 2)
                    tmpDat = DecodeSsimDayFormat(tmpFdat, "CEDA", "SSIM2")
                End If
                If Len(tmpFlno) > 0 And Len(tmpDat) > 0 Then
                    tmpFlno = StripAftFlno(tmpFlno, "", "", "")
                    tmpFdat = DecodeSsimDayFormat(tmpDat, "SSIM2", "CEDA")
                    If CheckValidDate(tmpFdat) = False Then
                        ErrorMsg = "Invalid Flight Date"
                    End If
                    If tmpAdid <> "A" And tmpAdid <> "D" Then
                        ErrorMsg = "Invalid Flight Type"
                    End If
                    If Len(ErrorMsg) = 0 Then
                        tmpAlc2 = Trim(Left(tmpFlno, 3))
                        tmpFltn = Trim(Mid(tmpFlno, 4, 5))
                        tmpFlns = Trim(Mid(tmpFlno, 9, 1))
                        If Len(tmpAlc2) = 2 Then
                            tmpAlc3 = UfisServer.BasicLookUp(1, 0, 1, tmpAlc2, 0, True)
                        Else
                            tmpAlc3 = Left(tmpAlc2, 3)
                        End If
                        tmpFkey = BuildAftFkey(tmpAlc3, tmpFltn, tmpFlns, tmpFdat, tmpAdid)
                        ActResult = ""
                        ActCmd = "RTA"
                        ActTable = "AFTTAB"
                        If tmpAdid = "A" Then
                            ActFldLst = "URNO,STOA"
                            'ActCondition = "WHERE FLNO = '" & tmpFlno & "' AND STOA LIKE '" & tmpFdat & "%' AND ADID = 'A'"
                        Else
                            ActFldLst = "URNO,STOD"
                            'ActCondition = "WHERE FLNO = '" & tmpFlno & "' AND STOD LIKE '" & tmpFdat & "%' AND ADID = 'D'"
                        End If
                        ActCondition = "WHERE FKEY = '" & tmpFkey & "'"
                        ActOrder = ""
                        ActDatLst = ""
                        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, False, False)
                        If retval >= 0 Then
                            tmpResult = UfisServer.DataBuffer(0).GetLineValues(0)
                            tmpTime = Mid(GetItem(tmpResult, 2, ","), 9, 4)
                            tabFlightList(2).SetColumnValue i, 1, tmpFlno
                            tabFlightList(2).SetColumnValue i, 2, tmpDat & "/" & Mid(tmpTime, 1, 2) & ":" & Mid(tmpTime, 3, 2)
                            tabFlightList(2).SetColumnValue i, 3, "0"
                            tabFlightList(2).SetColumnValue i, 4, "0"
                            tabFlightList(2).SetColumnValue i, 5, GetItem(tmpResult, 1, ",")
                            tabFlightList(2).SetColumnValue i, 6, GetItem(tmpResult, 2, ",")
                        Else
                            ErrorMsg = "Flight not found"
                        End If
                    End If
                    If Len(ErrorMsg) > 0 Then
                        tabFlightList(2).SetCurrentSelection i
                        MyMsgBox.CallAskUser 0, 0, 0, "Link Flight to Telex", ErrorMsg, "hand", "", UserAnswer
                        Exit For
                    End If
                Else
                    tabFlightList(2).SetColumnValue i, 5, ""
                End If
            Else
                If Len(tmpFlno) = 0 Then
                    tabFlightList(2).SetColumnValue i, 5, ""
                End If
            End If
        Next
        If Len(ErrorMsg) = 0 Then
            ActResult = ""
            ActCmd = "DRT"
            ActTable = "TLKTAB"
            ActFldLst = ""
            ActCondition = "WHERE TURN = " & CurrentUrno.Text
            ActOrder = ""
            ActDatLst = ""
            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
            For i = 0 To MaxLine
                tmpFurn = Trim(tabFlightList(2).GetColumnValue(i, 5))
                If Len(tmpFurn) > 0 Then
                    For j = i - 1 To 0 Step -1
                        If tmpFurn = Trim(tabFlightList(2).GetColumnValue(j, 5)) Then
                            tmpFurn = ""
                            Exit For
                        End If
                    Next
                End If
                If Len(tmpFurn) > 0 Then
                    ActResult = ""
                    ActCmd = "IRT"
                    ActTable = "TLKTAB"
                    ActFldLst = "URNO,HOPO,TURN,FURN,ADID,FLNO,FDAT,DATC"
                    ActCondition = ""
                    ActOrder = ""
                    ActDatLst = UfisServer.UrnoPoolGetNext & "," & _
                                UfisServer.HOPO & "," & _
                                CurrentUrno.Text & "," & _
                                tabFlightList(2).GetColumnValue(i, 5) & "," & _
                                tabFlightList(2).GetColumnValue(i, 0) & "," & _
                                tabFlightList(2).GetColumnValue(i, 1) & "," & _
                                tabFlightList(2).GetColumnValue(i, 2) & "," & _
                                tabFlightList(2).GetColumnValue(i, 6)
                    retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                End If
            Next
            EditFlightsToTelex
        End If
        TelexPoolHead.MousePointer = 0
    Else
        ErrorMsg = "No Telex selected"
        MyMsgBox.CallAskUser 0, 0, 0, "Link Flight to Telex", ErrorMsg, "hand", "", UserAnswer
    End If
End Sub

Private Function ExpandMonth(Day As String) As String
    Dim tmpCurDat1 As String
    Dim tmpCurDat2 As String
    Dim tmpYear As String
    Dim tmpMonth As String
    Dim Result As String
    
    tmpCurDat1 = Left(GetTimeStamp(0), 8)
    tmpCurDat2 = Left(tmpCurDat1, 6) & Day
    If tmpCurDat2 < tmpCurDat1 Then
        tmpYear = Mid(tmpCurDat1, 1, 4)
        tmpMonth = Mid(tmpCurDat1, 5, 2)
        tmpMonth = tmpMonth + 1
        If Len(tmpMonth) = 1 Then tmpMonth = "0" & tmpMonth
        If tmpMonth = "13" Then
            tmpMonth = "01"
            tmpYear = tmpYear + 1
        End If
        Result = tmpYear & tmpMonth & Day
    Else
        Result = tmpCurDat2
    End If
    Result = DecodeSsimDayFormat(Result, "CEDA", "SSIM2")
    ExpandMonth = Result
End Function

Private Sub AddToOnlineWindow()
    Dim count As Long
    Dim FlnuCol As Long
    Dim tmpLine As Long
    Dim tmpFlnu As String
    
    DataPool.TelexData(9).Refresh
    While DataPool.TelexData(9).GetLineCount > 0
        count = DataPool.TelexData(9).GetLineCount - 1
        RcvTelex.ShowHiddenTelex count
        ReadFolderCount = False
        PrepareTelexExtract CurMem, True
        ReadFolderCount = True
        If AreaPanel(3).Visible Then
            FlnuCol = CLng(GetRealItemNo(DataPool.TlxTabFields, "FLNU"))
            tmpLine = DataPool.TelexData(CurMem).GetLineCount - 1
            tmpFlnu = DataPool.TelexData(CurMem).GetColumnValue(tmpLine, FlnuCol)
            If tmpFlnu <> "" Then
                AddCountFlightTelex tmpFlnu
            End If
        End If
        If chkOnlineRcv.Value = 1 Then
            SetTlxTabCaption 0, 2, "(Online)"
            SetTlxTabCaption 1, 3, "(Online)"
            'chktitlebar(0).Caption = CStr(tabTelexList(2).GetLineCount) & " (" & tabTelexList(2).GetCurrentSelected & ") " & chktitlebar(0).Tag & " (Online)"
            'chktitlebar(1).Caption = CStr(tabTelexList(3).GetLineCount) & " (" & tabTelexList(3).GetCurrentSelected & ") " & chktitlebar(1).Tag & " (Online)"
            'chkRcvCnt(2).Caption = CStr(tabTelexList(2).GetLineCount)
            'chkSntCnt(3).Caption = CStr(tabTelexList(3).GetLineCount)
        End If
        DataPool.TelexData(9).Refresh
    Wend
    StopOnline = False
End Sub

Private Sub HandleTlxTitleBar()
    Dim strTmp As String
    Dim strDef As String
    If MyMainPurpose = "AUTO_SEND" Then strDef = "YES" Else strDef = "NO"
    strTmp = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SENT", strDef)
    If MyMainPurpose = "AUTO_SEND" Then strTmp = "YES"
    If strTmp = "NO" Then
        'chkTitleBar(1).Visible = False
        'chkTlxTitleBar(3).Visible = False
    Else
        'chkTitleBar(1).Visible = True
        'chkTlxTitleBar(3).Visible = True
    End If
End Sub

Private Function CheckTlxUrnolist(TlxUrnoList As String, MemIdx As Integer)
    Dim ChkUrnoList As String
    Dim ChkUrno As String
    Dim tmpData As String
    Dim i As Integer
    Dim j As Integer
    Dim UrnoCol As Long
    Dim tmpUrno As String
    Dim tmpItem As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    'UrnoCol = GetItemNo(DataPool.TlxTabFields, "URNO") - 1
    'MaxLine = DataPool.TelexData(MemIdx).GetLineCount - 1
    'For CurLine = 0 To MaxLine
    '    tmpUrno = DataPool.TelexData(MemIdx).GetColumnValue(CurLine, UrnoCol)
    '    'DEBUGGED:
    '    'GetItemNo assumes that all items
    '    'Are of the same length.
    '    'Thus this cannot work with URNO!
    '    tmpItem = GetItemNo(TlxUrnoList, tmpUrno)
    '    If tmpItem > 0 Then
    '       tmpData = RemoveItem(TlxUrnoList, tmpItem)
    '        TlxUrnoList = tmpData
    '    End If
    'Next
    
    If Trim(TlxUrnoList) <> "" Then
        UrnoCol = GetRealItemNo(DataPool.TlxTabFields, "URNO")
        ChkUrnoList = "," & TlxUrnoList & ","
        MaxLine = DataPool.TelexData(MemIdx).GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpUrno = DataPool.TelexData(MemIdx).GetColumnValue(CurLine, UrnoCol)
            ChkUrno = "," & tmpUrno & ","
            ChkUrnoList = Replace(ChkUrnoList, ChkUrno, ",", 1, -1, vbBinaryCompare)
        Next
        ChkUrnoList = Replace(ChkUrnoList, ",,", ",", 1, -1, vbBinaryCompare)
        If Left(ChkUrnoList, 1) = "," Then ChkUrnoList = Mid(ChkUrno, 2)
        If Right(ChkUrnoList, 1) = "," Then ChkUrnoList = Left(ChkUrnoList, Len(ChkUrnoList) - 1)
        TlxUrnoList = ChkUrnoList
    End If
    
    
    If Len(TlxUrnoList) > 0 Then
        TlxUrnoList = "WHERE URNO IN (" & TlxUrnoList & ")"
    End If
    CheckTlxUrnolist = TlxUrnoList
End Function

Private Function GetFlnuList(MemIdx As Integer)
    Dim tmpData As String
    Dim i As Integer
    Dim FlnuCol As Long
    Dim tmpFlnu As String
    Dim tmpItem As Long
    
    tmpData = ""
    FlnuCol = GetItemNo(DataPool.TlxTabFields, "FLNU") - 1
    For i = 0 To DataPool.TelexData(MemIdx).GetLineCount - 1
        tmpFlnu = DataPool.TelexData(MemIdx).GetColumnValue(i, FlnuCol)
        tmpItem = GetItemNo(tmpData, tmpFlnu)
        If tmpItem < 0 Then
            If Len(tmpData) > 0 Then
                tmpData = tmpData & ","
            End If
            tmpData = tmpData & tmpFlnu
        End If
    Next
    If Len(tmpData) > 0 Then
        tmpData = "WHERE FURN IN (" & tmpData & ")"
    End If
    GetFlnuList = tmpData
End Function

Private Function RemoveItem(ItemList As String, ItemNo As Long)
    Dim tmpData As String
    Dim count As Long
    Dim i As Integer
    
    tmpData = ""
    count = ItemCount(ItemList, ",")
    For i = 1 To ItemNo - 1
        tmpData = tmpData & GetItem(ItemList, i, ",") & ","
    Next
    For i = ItemNo + 1 To count
        tmpData = tmpData & GetItem(ItemList, i, ",") & ","
    Next
    If Len(tmpData) > 0 Then
        tmpData = Left(tmpData, Len(tmpData) - 1)
    End If
    RemoveItem = tmpData
End Function

Private Sub SetTlxTabCaption(IdxBar As Integer, IdxTab As Integer, AddCap As String)
    Dim HitList As String
    Dim Result As Long
    Dim Strg1 As String
    Dim Strg2 As String
    Dim Strg3 As String
    If tabTelexList(IdxTab).Visible = True Then
        If (Not OnlineRefreshLoop) Then
            tabTelexList(IdxTab).SetInternalLineBuffer True
            HitList = tabTelexList(IdxTab).GetLinesByColumnValues(1, "V,A,R,M,X", 0)
            Result = Val(HitList)
            tabTelexList(IdxTab).SetInternalLineBuffer False
            Result = tabTelexList(IdxTab).GetLineCount - Result
            Strg1 = CStr(tabTelexList(IdxTab).GetLineCount)
            Strg2 = CStr(Result)
            Strg3 = CStr(tabTelexList(IdxTab).GetCurrentSelected + 1)
            chkTitleBar(IdxBar).Caption = Strg1 & " / " & Strg2 & " / " & Strg3 & " " & chkTitleBar(IdxBar).Tag & " " & AddCap
        Else
            Result = tabTelexList(IdxTab).GetLineCount
            If (Result Mod 50) = 0 Then
                Strg1 = CStr(Result)
                Strg2 = "-"
                Strg3 = "-"
                chkTitleBar(IdxBar).Caption = Strg1 & " " & chkTitleBar(IdxBar).Tag & " " & AddCap
                ArrangeTabMaxScroll tabTelexList(IdxTab)
                DoEvents
            End If
        End If
    End If
End Sub

Private Sub SaveSplitterPosition(ForWhat As String)
    Dim i As Integer
    Select Case ForWhat
        Case "SAVE"
            For i = 0 To fraSplitter.UBound
                SaveSplitPos(i) = fraSplitter(i).Left
            Next
        Case "RESTORE"
            For i = 0 To fraSplitter.UBound
                fraSplitter(i).Left = SaveSplitPos(i)
            Next
        Case Else
    End Select
End Sub
Private Sub InitFtypPanel()
    Dim Index As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim NewSection As Boolean
    Dim FilterCount As Integer
    Dim CurFilter As Integer
    Dim FtypLayout As String
    Dim tmpItem As String
    LayoutStyle = "AODB_FILTER"
    If LayoutStyle <> FilterLayout Then
        FilterLayout = LayoutStyle
        LoadingConfig = True
        For Index = 0 To chkFtypFilter.UBound
            chkFtypFilter(Index).BackColor = LightGray
            chkFtypFilter(Index).Visible = False
            lblFtypName(Index).Visible = False
            lblFtypShadow(Index).Visible = False
            chkFtypFilter(Index).Value = 0
        Next
        FilterCount = Val(GetFilterConfig(LayoutStyle, "FILTER_FTYPES", -1, ""))
        If FilterCount > 0 Then
            'NewTop = chkAppl(2).Top + chkAppl(2).Height + 15
            NewTop = chkFtypAll(0).Top
            NewLeft = 45
            chkFtypAll(0).Top = NewTop
            chkFtypAll(0).BackColor = LightGray
            lblFtypAll(0).Top = NewTop + 15
            lblFtypAll(0).Left = chkFtypAll(0).Left + chkFtypAll(0).Width + 60
            lblFtypAllShadow(0).Top = NewTop + 30
            lblFtypAllShadow(0).Left = lblFtypAll(0).Left + 15
            lblFtypAll(0).ZOrder
            
            NewTop = NewTop + chkFtypAll(0).Height + 60
            Index = -1
            For CurFilter = 1 To FilterCount
                FtypLayout = GetFilterConfig(LayoutStyle, "FTYP_LAYOUT", CurFilter, "")
                Index = Index + 1
                NewSection = False
                If Index > chkFtypFilter.UBound Then
                    Load chkFtypFilter(Index)
                    Load lblFtypName(Index)
                    Load lblFtypShadow(Index)
                    Set chkFtypFilter(Index).Container = FtypPanel
                    Set lblFtypName(Index).Container = FtypPanel
                    Set lblFtypShadow(Index).Container = FtypPanel
                    NewSection = True
                End If
                chkFtypFilter(Index).Top = NewTop
                chkFtypFilter(Index).Left = NewLeft
                lblFtypName(Index).Top = NewTop + 15
                lblFtypName(Index).Left = chkFtypFilter(Index).Left + chkFtypFilter(Index).Width + 60
                lblFtypShadow(Index).Top = NewTop + 30
                lblFtypShadow(Index).Left = lblFtypName(Index).Left + 15
                chkFtypFilter(Index).Caption = GetItem(FtypLayout, 1, ",")
                lblFtypName(Index).Caption = GetItem(FtypLayout, 2, ",")
                lblFtypShadow(Index).Caption = lblFtypName(Index).Caption
                lblFtypName(Index).ZOrder
                'If (LayoutChanged) Or (NewSection) Then
                    tmpItem = GetItem(FtypLayout, 3, ",")
                    If tmpItem = "Y" Then chkFtypFilter(Index).Value = 1 Else chkFtypFilter(Index).Value = 0
                'End If
                tmpItem = GetItem(FtypLayout, 4, ",")
                If tmpItem = "Y" Then chkFtypFilter(Index).Enabled = True Else chkFtypFilter(Index).Enabled = False
                lblFtypName(Index).ToolTipText = GetItem(FtypLayout, 5, ",")
                chkFtypFilter(Index).Visible = True
                lblFtypName(Index).Visible = True
                lblFtypShadow(Index).Visible = True
                NewTop = NewTop + chkFtypFilter(Index).Height + 15
            Next
            NewTop = NewTop + 120
            NewHeight = NewTop
            FtypPanel.Height = NewHeight
            NewHeight = NewHeight + FtypPanel.Top + chkFsfClose.Height + 180
            NewTop = NewHeight - chkFsfClose.Height - 105
            chkFsfClose.Top = NewTop
            fraFlightStatus.Height = NewHeight
            'DrawBackGround FtypPanel, 7, False, True
            'Me.Width = Me.Width + FtypPanel.Width
        End If
        LoadingConfig = False
        CheckFtypFilter
    End If

End Sub

Private Function GetFilterConfig(LayoutType As String, ForWhat As String, Index As Integer, UseDefault As String) As String
    Dim Result As String
    Dim TypeCode As String
    Dim WhatCode As String
    Dim UseIniKey As String
    Dim tmpDflt As String
    Result = ""
    TypeCode = GetRealItem(LayoutType, 0, ",")
    WhatCode = GetRealItem(ForWhat, 0, ",")
    Select Case TypeCode
        Case "AODB_FILTER"
            Select Case WhatCode
                Case "FILTER_PANELS"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_PANELS", "0")
                Case "FILTER_CAPTION"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_CAPTION", "AODB Flight Data Filter")
                Case "PANEL_LAYOUT"
                    UseIniKey = "AODB_FILTER_PANEL_" & CStr(Index)
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "??|??,??")
                Case "FILTER_FTYPES"
                    Result = GetIniEntry(myIniFullName, "", "AODB_FILTER", "AODB_FILTER_FTYPES", "-")
                    If Result = "-" Then Result = "9"
                Case "FTYP_LAYOUT"
                    UseIniKey = "AODB_FILTER_FTYP_" & CStr(Index)
                    Result = GetIniEntry(myIniFullName, "AODB_FILTER", "", UseIniKey, "?,???,???")
                    If Left(Result, 1) = "?" Then
                        Result = GetDefaultFtypSet(UseIniKey)
                    End If
                Case "FILTER_PERIOD"
                    UseIniKey = "AODB_FILTER_PERIOD"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "-2,+8")
                Case "FILTER_RELOAD"
                    UseIniKey = "AODB_FILTER_RELOAD"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, UseDefault)
                Case Else
            End Select
        Case Else
    End Select
    GetFilterConfig = Result
End Function
Private Function GetDefaultFtypSet(UseIniKey As String) As String
    Dim Result As String
    Select Case UseIniKey
        Case "AODB_FILTER_FTYP_1"
            Result = "S,SKD,Y,Y,Scheduled Flights"
        Case "AODB_FILTER_FTYP_2"
            Result = "O,OPS,Y,Y,Operational Flights"
        Case "AODB_FILTER_FTYP_3"
            Result = "X,CXX,Y,Y,Cancelled Flights"
        Case "AODB_FILTER_FTYP_4"
            Result = "N,NOP,N,Y,Non Operational"
        Case "AODB_FILTER_FTYP_5"
            Result = "D,DIV,Y,Y,Diverted Flights"
        Case "AODB_FILTER_FTYP_6"
            Result = "B,RTX,Y,Y,Return Taxi"
        Case "AODB_FILTER_FTYP_7"
            Result = "Z,RTF,Y,Y,Return Flight"
        Case "AODB_FILTER_FTYP_8"
            Result = "T,TOW,N,N,Towing"
        Case "AODB_FILTER_FTYP_9"
            Result = "G,MVT,N,N,Ground Movement"
        Case Else
            Result = "?,???,???"
    End Select
    GetDefaultFtypSet = Result
End Function

Private Sub lblFtypName_Click(Index As Integer)
    If chkFtypFilter(Index).Enabled = True Then
        If chkFtypFilter(Index).Value = 0 Then chkFtypFilter(Index).Value = 1 Else chkFtypFilter(Index).Value = 0
    End If
End Sub

Private Sub lblFtypAll_Click(Index As Integer)
    If chkFtypAll(Index).Value = 0 Then chkFtypAll(Index).Value = 1 Else chkFtypAll(Index).Value = 0
End Sub

Private Sub chkFtypAll_Click(Index As Integer)
    Dim i As Integer
    If (Not ButtonPushedByUser) And (Not LoadingConfig) And (Not RestoringFilter) Then
        StopNestedCalls = True
        For i = 0 To chkFtypFilter.UBound
            If chkFtypFilter(i).Visible Then
                If chkFtypFilter(i).Enabled Then
                    chkFtypFilter(i).Value = (1 - chkFtypAll(Index).Value)
                End If
            End If
        Next
        StopNestedCalls = False
    End If
End Sub

Private Sub chkFtypFilter_Click(Index As Integer)
    If Not RestoringFilter Then
        If (Not StopNestedCalls) Then ButtonPushedByUser = True
        CheckFtypFilter
        ButtonPushedByUser = False
        If (Not ReorgFolders) And (Not LoadingConfig) Then CheckLoadButton False
    End If
End Sub
Private Sub CheckFtypFilter()
    Dim tmpData As String
    Dim i As Integer
    tmpData = ""
    For i = 0 To chkFtypFilter.UBound
        'If chkFtypFilter(i).Visible Then
            If chkFtypFilter(i).Value = 1 Then
                tmpData = tmpData & "'" & chkFtypFilter(i).Caption & "',"
            End If
        'End If
    Next
    If tmpData <> "" Then
        tmpData = Left(tmpData, Len(tmpData) - 1)
        If (Not StopNestedCalls) And (Not LoadingConfig) Then chkFtypAll(0).Value = 0
    Else
        If (Not StopNestedCalls) And (Not LoadingConfig) Then chkFtypAll(0).Value = 1
    End If
    txtFtypFilter.Text = tmpData
End Sub

Private Function CheckScoreLineFormat(ScoreLine As String, MyApc3 As String, SeqId As String) As Integer
    Dim ChkApc As String
    Dim tmpData As String
    Dim ApcPos As Integer
    Dim OffSet As Integer
    Dim PosDif As Integer
    Dim PosChk As Boolean
    PosChk = False
    If PosChk = False Then
        OffSet = InStr(ScoreLine, MyApc3)
        If OffSet = 1 Then
            tmpData = ""
            tmpData = tmpData & Right("        " & SeqId, 8)
            tmpData = tmpData & "K0SEA"
            tmpData = tmpData & ScoreLine
            ScoreLine = tmpData
            ApcPos = 14
        ElseIf OffSet > 0 Then
            PosDif = 14 - OffSet
            If PosDif > 0 Then
                ScoreLine = Space(PosDif) & ScoreLine
                ApcPos = 14
            ElseIf PosDif < 0 Then
            End If
        Else
            'Wrong File Format
        End If
    End If
    CheckScoreLineFormat = ApcPos
End Function

Private Sub InitBcLogTab()
    Dim i As Integer
    Dim K As Integer
    
    For i = 0 To 0
        BcLog(i).ResetContent
        BcLog(i).HeaderString = "Sequ,S,V,Type,-----URNO-----,Remark" & Space(50)
        BcLog(i).HeaderLengthString = "10,10,10,10,10"
        BcLog(i).ColumnAlignmentString = "R,C,C,C,R,L"
        BcLog(i).HeaderAlignmentString = "C,C,C,C,R,L"
        If i = 0 Then
            BcLog(i).SetMainHeaderValues "4,1,1", "Received, ,Rem", ""
        Else
            BcLog(i).SetMainHeaderValues "4,1,1", "Dropped, ,Rem", ""
        End If
        BcLog(i).MainHeader = True
        BcLog(i).FontName = "Arial"
        BcLog(i).SetMainHeaderFont 14, False, False, True, 0, "Arial"
        BcLog(i).LifeStyle = True
        BcLog(i).AutoSizeByHeader = True
        'For K = 1 To 500
        '    BcLog(i).InsertTextLine CStr(K) & ",R,V,MVT,12345678,", False
        'Next
        BcLog(i).AutoSizeColumns
        BcLog(i).ShowHorzScroller True
        BcLog(i).Refresh
    Next
    
    For i = 1 To 1
        BcLog(i).ResetContent
        BcLog(i).HeaderString = "Time,S,V,Type,-----URNO-----,Remark" & Space(50)
        BcLog(i).HeaderLengthString = "10,10,10,10,10"
        BcLog(i).ColumnAlignmentString = "C,C,C,C,R,L"
        BcLog(i).HeaderAlignmentString = "C,C,C,C,R,L"
        If i = 0 Then
            BcLog(i).SetMainHeaderValues "4,1,1", "Received, ,Rem", ""
        Else
            BcLog(i).SetMainHeaderValues "4,1,1", "Dropped, ,Rem", ""
        End If
        BcLog(i).MainHeader = True
        BcLog(i).FontName = "Arial"
        BcLog(i).SetMainHeaderFont 14, False, False, True, 0, "Arial"
        BcLog(i).LifeStyle = True
        BcLog(i).DateTimeSetColumn 0
        BcLog(i).DateTimeSetInputFormatString 0, "YYYYMMDDhhmmss"
        BcLog(i).DateTimeSetOutputFormatString 0, "hhmm"
        BcLog(i).AutoSizeByHeader = True
        'For K = 1 To 500
        '    BcLog(i).InsertTextLine "1243,R,V,MVT,12345678,", False
        'Next
        BcLog(i).AutoSizeColumns
        BcLog(i).ShowHorzScroller True
        BcLog(i).Refresh
    Next
    
    For i = 2 To 2
        BcLog(i).ResetContent
        BcLog(i).HeaderString = "Time,S,V,Type,-----URNO-----,Remark" & Space(50)
        BcLog(i).HeaderLengthString = "10,10,10,10,10"
        BcLog(i).ColumnAlignmentString = "C,C,C,C,R,L"
        BcLog(i).HeaderAlignmentString = "C,C,C,C,R,L"
        If i = 0 Then
            BcLog(i).SetMainHeaderValues "4,1,1", "Received, ,Rem", ""
        Else
            BcLog(i).SetMainHeaderValues "4,1,1", "Discarded, ,Rem", ""
        End If
        BcLog(i).MainHeader = True
        BcLog(i).FontName = "Arial"
        BcLog(i).SetMainHeaderFont 14, False, False, True, 0, "Arial"
        BcLog(i).LifeStyle = True
        BcLog(i).DateTimeSetColumn 0
        BcLog(i).DateTimeSetInputFormatString 0, "YYYYMMDDhhmmss"
        BcLog(i).DateTimeSetOutputFormatString 0, "hhmm"
        BcLog(i).AutoSizeByHeader = True
        'For K = 1 To 500
        '    BcLog(i).InsertTextLine "1243,R,V,MVT,12345678,", False
        'Next
        BcLog(i).AutoSizeColumns
        BcLog(i).ShowHorzScroller True
        BcLog(i).Refresh
    Next
    
    'BcLog(1).ZOrder
End Sub
Private Sub AdjustBcLogScroll()
    Dim MaxLine As Long
    Dim ScrLine As Long
    Dim VisLine As Long
    BcLog(0).Height = LogPanel(0).ScaleHeight
    MaxLine = BcLog(0).GetLineCount
    VisLine = (BcLog(0).Height / (BcLog(0).lineHeight * 15))
    VisLine = VisLine - 3
    VisLine = VisLine - 1
    ScrLine = MaxLine - VisLine
    BcLog(0).OnVScrollTo ScrLine
    BcLog(1).Height = LogPanel(1).ScaleHeight
    MaxLine = BcLog(1).GetLineCount
    VisLine = (BcLog(1).Height / (BcLog(1).lineHeight * 15))
    VisLine = VisLine - 3
    VisLine = VisLine - 1
    ScrLine = MaxLine - VisLine
    BcLog(1).OnVScrollTo ScrLine
    BcLog(2).Height = LogPanel(2).ScaleHeight
    MaxLine = BcLog(2).GetLineCount
    VisLine = (BcLog(2).Height / (BcLog(2).lineHeight * 15))
    VisLine = VisLine - 3
    VisLine = VisLine - 1
    ScrLine = MaxLine - VisLine
    BcLog(2).OnVScrollTo ScrLine
End Sub
