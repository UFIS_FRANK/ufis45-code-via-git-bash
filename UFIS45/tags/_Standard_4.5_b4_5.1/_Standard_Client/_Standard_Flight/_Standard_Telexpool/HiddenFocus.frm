VERSION 5.00
Begin VB.Form HiddenFocus 
   Caption         =   "TelexPool Hidden Focus Control"
   ClientHeight    =   1320
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4905
   ControlBox      =   0   'False
   Icon            =   "HiddenFocus.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1320
   ScaleWidth      =   4905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Label Label1 
      Caption         =   "of your monitor configuration."
      Height          =   225
      Index           =   2
      Left            =   60
      TabIndex        =   2
      Top             =   690
      Width           =   4815
   End
   Begin VB.Label Label1 
      Caption         =   "Please disable the ""Open Program Windows in Display ..."" feature"
      Height          =   195
      Index           =   1
      Left            =   60
      TabIndex        =   1
      Top             =   480
      Width           =   4815
   End
   Begin VB.Label Label1 
      Caption         =   "When you see this window:"
      Height          =   225
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   4815
   End
End
Attribute VB_Name = "HiddenFocus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Me.Top = -5000
End Sub
