VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form BcSpooler 
   Caption         =   "UFIS IMBIS Message Spooler"
   ClientHeight    =   5640
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12525
   Icon            =   "BcSpooler.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5640
   ScaleWidth      =   12525
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command2 
      Caption         =   "TEST GAP3"
      Height          =   315
      Index           =   2
      Left            =   9600
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.CommandButton Command2 
      Caption         =   "TEST GAP2"
      Height          =   315
      Index           =   1
      Left            =   8550
      TabIndex        =   11
      Top             =   0
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.CommandButton Command2 
      Caption         =   "TEST GAP1"
      Height          =   315
      Index           =   0
      Left            =   7500
      TabIndex        =   10
      Top             =   0
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   6
      Left            =   6300
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   0
      Width           =   1035
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Imbis"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   5
      Left            =   5250
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   0
      Width           =   1035
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Save"
      Height          =   315
      Left            =   11400
      TabIndex        =   7
      Top             =   30
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Scrolling"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   4
      Left            =   1050
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   0
      Width           =   1035
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "ScrollBar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   2100
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   0
      Width           =   1035
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   4200
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   0
      Width           =   1035
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Spooling"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   1035
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "AutoSize"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   3150
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   0
      Width           =   1035
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   5355
      Width           =   12525
      _ExtentX        =   22093
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18997
         EndProperty
      EndProperty
   End
   Begin VB.Timer BcSpoolTimer 
      Enabled         =   0   'False
      Interval        =   5
      Left            =   10800
      Top             =   0
   End
   Begin TABLib.TAB BcSpoolerTab 
      Height          =   1935
      Left            =   30
      TabIndex        =   0
      Top             =   330
      Width           =   12375
      _Version        =   65536
      _ExtentX        =   21828
      _ExtentY        =   3413
      _StockProps     =   64
   End
End
Attribute VB_Name = "BcSpooler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Spooling As Boolean
Dim Scrolling As Boolean

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                BcSpoolerTab.AutoSizeColumns
                chkWork(Index).Value = 0
            Case 1
                Spooling = True
                BcSpoolReleased = False
                BcSpoolTimer.Enabled = False
            Case 2
                BcSpoolerTab.Refresh
                chkWork(Index).Value = 0
            Case 3
                BcSpoolerTab.ShowHorzScroller True
                BcSpoolerTab.Refresh
            Case 4
                Scrolling = True
            Case 5
                UfisImbis.Show
                chkWork(Index).Value = 0
            Case 6
                Me.Hide
                chkWork(Index).Value = 0
            Case Else
        End Select
    Else
        chkWork(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
            Case 1
                Spooling = False
                BcSpoolReleased = True
                BcSpoolTimer.Enabled = True
            Case 2
            Case 3
                BcSpoolerTab.ShowHorzScroller False
                BcSpoolerTab.Refresh
            Case 4
                Scrolling = False
            Case 5
                'UfisImbis.Hide
            Case Else
        End Select
    End If
End Sub

Private Sub Command1_Click()
    Dim tmpFileName As String
    Dim tmpPath As String
    Dim tmpName As String
    tmpPath = UFIS_TMP
    'tmpName = ApplFuncCode & "_BcSpool.txt"
    'tmpFileName = tmpPath & "\" & tmpName
    'BcSpoolerTab.WriteToFile tmpFileName, False
    
End Sub

Private Sub Command2_Click(Index As Integer)
    Dim SqlCmd As String
    Dim SqlTbl As String
    Dim SqlSel As String
    Dim SqlFld As String
    Dim SqlDat As String
    Screen.MousePointer = 11
    chkWork(1).Value = 1
    SqlCmd = "GAP" & CStr(Index + 1)
    SqlSel = "CASE" & CStr(Index + 1)
    SqlTbl = "BCTEST"
    SqlFld = "FIELDS"
    SqlDat = "DATA"
    UfisServer.CallCeda CedaDataAnswer, SqlCmd, SqlTbl, SqlFld, SqlDat, SqlSel, "", 0, True, False
    Screen.MousePointer = 0
End Sub

Private Sub Form_Load()
    Dim BcFields As String
    BcSpoolerTab.ResetContent
    BcFields = "BSEQ,BNUM,BRSQ,BCMD,BTBL,BSEL,BDAT,BFLD,BTWS,BTWE,BWKS,BUSR,REMA"
    BcSpoolerTab.HeaderString = BcFields & Space(500)
    BcSpoolerTab.LogicalFieldList = BcFields
    BcSpoolerTab.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10"
    BcSpoolerTab.LifeStyle = True
    BcSpoolerTab.SetFieldSeparator (Chr(16))
    BcSpoolerTab.AutoSizeByHeader = True
    BcSpoolerTab.AutoSizeColumns
End Sub

Private Sub BcSpoolTimer_Timer()
    If BcSpoolerTab.GetLineCount > 0 Then GetBcFromSpooler
End Sub

Public Sub GetBcFromSpooler()
    Dim ReqId As String
    Dim DestName As String
    Dim RecvName As String
    Dim CedaCmd As String
    Dim ObjName As String
    Dim Seq As String
    Dim tws As String
    Dim twe As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    Dim BcNum As String
    Dim MaxLine As Long
    If Not Spooling Then
        If BcSpoolerTab.GetLineCount > 0 Then
            'BcFields = "BSEQ,BNUM,BRSQ,BCMD,BTBL,BSEL,BDAT,BFLD,BTWS,BTWE,BWKS,BUSR,REMA"
            'TabColNo = "0   ,1   ,2   ,3   ,4   ,5   ,6   ,7   ,8   ,9   ,10  ,11  ,12
            Seq = BcSpoolerTab.GetColumnValue(0, 0)
            BcNum = BcSpoolerTab.GetColumnValue(0, 1)
            ReqId = BcSpoolerTab.GetColumnValue(0, 2)
            CedaCmd = BcSpoolerTab.GetColumnValue(0, 3)
            ObjName = BcSpoolerTab.GetColumnValue(0, 4)
            CedaSqlKey = BcSpoolerTab.GetColumnValue(0, 5)
            Data = BcSpoolerTab.GetColumnValue(0, 6)
            Fields = BcSpoolerTab.GetColumnValue(0, 7)
            tws = BcSpoolerTab.GetColumnValue(0, 8)
            twe = BcSpoolerTab.GetColumnValue(0, 9)
            DestName = BcSpoolerTab.GetColumnValue(0, 10)
            RecvName = BcSpoolerTab.GetColumnValue(0, 11)
            'BcRema = BcSpoolerTab.GetColumnValue(0, 12)
            BcSpoolerTab.DeleteLine 0
            HandleBroadCast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, tws, twe, CedaSqlKey, Fields, Data, BcNum
        End If
    End If
    BcSpoolTimer.Enabled = Not Spooling
    MaxLine = BcSpoolerTab.GetLineCount
    TelexPoolHead.BcSpoolCnt.Caption = CStr(MaxLine)
    If MaxLine > 0 Then
        TelexPoolHead.BcSpoolCnt.BackColor = LightYellow
    Else
        TelexPoolHead.BcSpoolCnt.BackColor = vbButtonFace
        BcSpoolTimer.Enabled = False
        BcSpoolReleased = False
    End If
    If Scrolling Then BcSpoolerTab.OnVScrollTo BcSpoolerTab.GetLineCount
    If Me.Visible Then BcSpoolerTab.RedrawTab
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = True
            Me.Hide
        End If
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - (BcSpoolerTab.Left * 2)
    If NewSize > 600 Then
        BcSpoolerTab.Width = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height - BcSpoolerTab.Top
    If NewSize > 600 Then
        BcSpoolerTab.Height = NewSize
    End If
    
End Sub
