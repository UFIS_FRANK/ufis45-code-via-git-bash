Attribute VB_Name = "StartModule"
Option Explicit

Public DoNotRefreshTelexList As Boolean 'igu on 5 Aug 2010

Public CurrentTimeFilter As String
Public NewTlxFromBcHdl As Boolean
Public NewMsgFromBcHdl As Boolean
Public NewMsgFromFips As Boolean

Public RecvPeriod As String
Public SendPeriod As String
Public SentPeriod As String
Public PendPeriod As String


Public MyOwnButtonFace As Long
Public MyOwnButtonDown As Long
Public MyOwnPanelColor As Long
Public MyOwnFrameColor As Long
Public MyOwnSplitColor As Long

Public UseSnapFeature As Boolean
Public AutoSnapMainDialog As Boolean
Public AutoSnapMvtGenerator As Boolean
Public ApplicationIsReadyForUse As Boolean
Public MainFormPrepareLayout As Boolean
Public MainFormIsReadyForUse As Boolean
Public GeneratorIsReadyForUse As Boolean
Public OpenedFromFips As Boolean
Public FipsIsUpAndRunning As Boolean
Public FipsWasConnected As Boolean
Public AutoOpenOnline As Boolean
Public AutoRefreshOnline As Boolean
Public UseOldDefautlts As Boolean

Public MainOpenedByUser As Boolean
Public MainWindowIsOpen As Boolean
Public CreateTelexIsOpen As Boolean
Public ToggleOpenWindows As Boolean

Public MaxScreenWidth As Long
Public MaxScreenHeight As Long
Public SetMainWinLeftPos As Long
Public SetMainWinTopPos As Long

Public MyMainPurpose As String
Public CedaTimeFields As String
Public TemplateDelayNames As String
Public TemplateDelayTypes As String
Public TemplatePaxNames As String
Public TemplatePaxTypes As String

Public LastResizeName As String
Public SystemResizing As Boolean

Public ReadFolderCount As Boolean
Public StopOnline As Boolean
Public LockBroadcasts As Boolean
Public MainIsOnline As Boolean
Public MainIsDeploy As Boolean
Public MainIsFltDeploy As Boolean
Public StartUpMain As Boolean
Public SaveLocalTestData As Boolean
Public BrowserColumns As String
Public AllTypesCol As Long
Public OtherTypesCol As Long
Public FreeTypesCol As Long
Public IataTypesCol As Long
Public LastFolderIndex As Long
Public FirstAssignFolder As Long
Public FolderFixedSize As Long
Public FolderMinSize As Long
Public FolderDynSize As Boolean
Public FolderAutoSize As Boolean
Public AllTelexTypes As String
Public FolderCaptions As String
Public FolderTypeList As String
Public TypeActionList As String
Public InfoTypeList As String
Public RecvNoShow As String
Public MainNoShow As String
Public AddrNoCheck As String
Public IgnoreAllTlxText As String
Public TextFilterList As String
Public IgnoreEmptyText As Boolean
Public MyFontSize As Integer
Public MyFontBold As Boolean
Public MySitaOutType As String
Public MySitaDefPrio As String
Public MySitaOutOrig As Boolean
Public MySitaOutMsgId As Boolean
Public MySitaOutSmi As Boolean
Public MySitaOutDblSign As Boolean
Public TelexTemplates As Boolean
Public TestCaseMode As Boolean
Public OnlineWindows As Integer
Public LoadedWindows As Integer
Public MaxOnlineCount As Long
Public MaxFolderRows As Integer
Public MyLifeStyleValue As Integer
Public RecvTypeAction As String
Public SendTypeAction As String
Public ImportIsConnected As Boolean
Public ImportIsPending As Boolean
Public PathToImportTool As String
Public RepeatChar As String
Public TlxUrnoFromRedirect As String
Public TlxTxtFldSize As Integer
Public AftUrnoFromFips As String
Public AllowRedirect As Boolean
Public gsUserName As String
Public UpdateWstaOnClick As Boolean
Private Declare Function SetWindowRgn Lib "user32" (ByVal hwnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal x1 As Long, ByVal y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

Public Function GetModuleVersion()
    GetModuleVersion = ""
End Function

Sub Main()
    Dim LoginIsOk As Boolean
    Dim CmdLine As String
    Dim tmpData As String
    Dim UseSnap As String
    Dim tmpMainIsDeploy As Boolean
    Dim tmpParam1 As String
    Dim tmpParam2 As String
    
    'MyOwnButtonFace = vbButtonFace
    MyOwnButtonFace = RGB(236, 233, 216)
    MyOwnButtonDown = LightGreen
    MyOwnPanelColor = MyOwnButtonFace
    MyOwnFrameColor = MyOwnButtonFace
    MyOwnSplitColor = MyOwnButtonFace
    
    UseSnapFeature = False
    
    MaxScreenWidth = 1024 * 15
    MaxScreenHeight = Screen.Height
    
    Screen.MousePointer = 11
    ApplicationIsStarted = False
    
    Load BcSpooler
    Load UfisServer
    UfisServer.aCeda.StayConnected = True
    
    
    MyFontSize = 14
    MyFontBold = False
    
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
    LoginIsOk = DetermineIniFile
    If Not LoginIsOk Then
        ShutDownApplication False
    End If
    MyFontSize = Val(GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_SIZE", "14"))
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_BOLD", "NO") = "YES" Then MyFontBold = True Else MyFontBold = False
    
    AutoArrange = False
    StartUpMain = True
    ShutDownRequested = False
    OnlineWindows = 0
    'If App.PrevInstance = True Then
    '    MyMsgBox.InfoApi 1, "", ""
    '    With Screen
    '        If MyMsgBox.AskUser(0, .Width / 2, .Height / 2, App.EXEName & ": Application Control", "This module is already open!", "stop", "", UserAnswer) >= 0 Then ShutDownRequested = True
    '    End With
    '    End
    'End If
    'Screen.MousePointer = 11
    
    LoginIsOk = False
    
    OpenedFromFips = UfisServer.GetCommandLine()
    'For Testing
    'OpenedFromFips = True
    FipsIsUpAndRunning = OpenedFromFips
    FipsWasConnected = OpenedFromFips
    LoginIsOk = OpenedFromFips
    
    ShapeMyForm "INTRO", HiddenMain, HiddenMain.Image5
    HiddenMain.ShowHiddenMain True
    HiddenMain.SetStatusText 1, "CONNECTING TO SERVER '" & UfisServer.HostName & "'"
    HiddenMain.SetStatusText 2, "WAITING FOR CONNECTION"
    UfisServer.ConnectToCeda
    If Not CedaIsConnected Then
        HiddenMain.SetStatusText 2, "COULD NOT CONNECT"
        LoginIsOk = False
    Else
        HiddenMain.SetStatusText 2, "CONNECTION ESTABLISHED"
        If ApplIsInDesignMode Then LoginIsOk = True
    End If
    
    'For Login in Design Mode
    'LoginIsOk = False
    If Not LoginIsOk Then
        If UfisServer.HostName <> "LOCAL" Then
            Screen.MousePointer = 0
            LoginIsOk = LoginProcedure
        Else
            LoginIsOk = True
        End If
    End If
    If Not LoginIsOk Then
        UfisServer.aCeda.CleanupCom
        ShutDownApplication False
    End If
    
    If OpenedFromFips Then
        CmdLine = Command
        If InStr(CmdLine, "CONNECTED") > 0 Then
            tmpParam1 = GetRealItem(CmdLine, 0, " ")
            tmpParam2 = GetRealItem(CmdLine, 1, " ")
            If tmpParam2 <> "" Then
                gsUserName = tmpParam2
            Else
                gsUserName = "TelexPool"
            End If
        Else
            gsUserName = "TelexPool"
        End If
    End If
    
    Screen.MousePointer = 11
    SystemResizing = True
    
    'If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_INTRO", "YES") = "YES" Then
        'ShapeMyForm "INTRO", HiddenMain, HiddenMain.Image5
        HiddenMain.ShowHiddenMain True
        HiddenMain.Refresh
        'SetFormOnTop HiddenMain, True, False
    'End If
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_EXPORT_PANEL", "NO")
    If tmpData = "YES" Then LoggingIsActive = True Else LoggingIsActive = False
    TelexPoolHead.TestPanel.Visible = LoggingIsActive
    TelexPoolHead.BcLogTimer = LoggingIsActive
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_LOGGING_LINES", "500")
    MaxLoggingLines = Val(tmpData)
    
    UseSnap = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ACTIVATE_AUTOSNAP", "NO")
    If UseSnap = "YES" Then UseSnapFeature = True
    AutoSnapMainDialog = UseSnapFeature
    AutoSnapMvtGenerator = UseSnapFeature
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "AUTOSNAP_MAINDIALOG", UseSnap)
    If tmpData = "YES" Then AutoSnapMainDialog = True Else AutoSnapMainDialog = False
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "AUTOSNAP_MVTGENERATOR", UseSnap)
    If tmpData = "YES" Then AutoSnapMvtGenerator = True Else AutoSnapMvtGenerator = False
    
    MyMainPurpose = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_PURPOSE", "POOL")
    
    tmpData = "YES"
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "USE_OLD_DEFAULTS", tmpData)
    If tmpData = "YES" Then UseOldDefautlts = True Else UseOldDefautlts = False
    
    MaxScreenWidth = Screen.Width
    MaxScreenHeight = Screen.Height
    If MyMainPurpose = "AUTO_SEND" Then
        tmpData = CStr(MaxScreenWidth / 15)
        tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_AUTO_WIDTH", tmpData)
        MaxScreenWidth = Val(tmpData) * 15
        tmpData = CStr(MaxScreenHeight / 15)
        tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_AUTO_HEIGHT", tmpData)
        MaxScreenHeight = Val(tmpData) * 15
    ElseIf (Not OpenedFromFips) Then
        tmpData = CStr(MaxScreenWidth / 15)
        tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_MAIN_WIDTH", tmpData)
        MaxScreenWidth = Val(tmpData) * 15
        tmpData = CStr(MaxScreenHeight / 15)
        tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_MAIN_HEIGHT", tmpData)
        MaxScreenHeight = Val(tmpData) * 15
    ElseIf OpenedFromFips Then
        tmpData = CStr(MaxScreenWidth / 15)
        tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_POOL_WIDTH", tmpData)
        MaxScreenWidth = Val(tmpData) * 15
        tmpData = CStr(MaxScreenHeight / 15)
        tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAX_POOL_HEIGHT", tmpData)
        MaxScreenHeight = Val(tmpData) * 15
    End If
    If MaxScreenWidth < (800 * 15) Then MaxScreenWidth = 800 * 15
    If MaxScreenHeight < (600 * 15) Then MaxScreenHeight = 600 * 15
    If MaxScreenWidth > Screen.Width Then MaxScreenWidth = Screen.Width
    If MaxScreenHeight > Screen.Height Then MaxScreenHeight = Screen.Height
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "TEST_MODE_LOCAL", "NO")
    If tmpData = "YES" Then
        UfisServer.HostName.Text = "LOCAL"
        'This loads the Main form!
        'So disable it
        'TelexPoolHead.CedaStatus(3).Text = "LOCAL"
        UfisServer.aCeda.CleanupCom
        CedaIsConnected = False
    End If
    
    tmpData = "YES"
    'If OpenedFromFips Then tmpData = "NO"
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "AUTO_OPEN_ONLINE", tmpData)
    If tmpData = "YES" Then AutoOpenOnline = True Else AutoOpenOnline = False
    
    tmpData = "YES"
    'If OpenedFromFips Then tmpData = "NO"
    If Not AutoOpenOnline Then tmpData = "NO"
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "AUTO_REFRESH_ONLINE", tmpData)
    If tmpData = "YES" Then AutoRefreshOnline = True Else AutoRefreshOnline = False
    
    
    'THE CONFIGURATION SHOULD NEVER
    'SET PROPERTIES OF OTHER FORMS!
    '(BECAUSE THIS LOADS THE FORM)
    Load PoolConfig
    
    If (InStr(CmdLine, "NOMAIN") > 0) Or (PoolConfig.OnLineCfg(0).Value = 0) Then StartUpMain = False
    Screen.MousePointer = 11
    MyFontSize = Val(GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_SIZE", "14"))
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "FONT_BOLD", "NO") = "YES" Then MyFontBold = True Else MyFontBold = False
    
    'THIS MUST BE THE ONE AND ONLY
    'START UP CALL OF THE MODULE !
    'MainFormPrepareLayout = True
    Load TelexPoolHead
    DoEvents
    
    StopOnline = False
    LockBroadcasts = False
    ReadFolderCount = True
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_ONLINE", "NO")
    If tmpData = "YES" Then
        MainIsOnline = True
        StartUpMain = True
    Else
        MainIsOnline = False
    End If
    
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_FOL_DEPLOY", "NO")
    If tmpData = "YES" Then
        tmpMainIsDeploy = True
    Else
        tmpMainIsDeploy = False
    End If
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "MAIN_FLT_DEPLOY", "NO")
    If tmpData = "YES" Then
        MainIsFltDeploy = True
    Else
        MainIsFltDeploy = False
    End If
    
    Screen.MousePointer = 11
    
    DoEvents
    DoEvents
    
    Screen.MousePointer = 11
    If MyMainPurpose <> "AUTO_SEND" Then
        If PoolConfig.OnLineCfg(2).Value = 1 Then MyMainPurpose = "AUTO_SEND"
        If PoolConfig.OnLineCfg(5).Value = 1 Then MyMainPurpose = "AUTO_SEND"
    End If
    If MyMainPurpose = "AUTO_SEND" Then
        PoolConfig.OnLineCfg(2).Value = 1
    End If
    
    DoEvents
    OnlineWindows = 0
    If PoolConfig.OnLineCfg(2).Value = 1 Then OnlineWindows = OnlineWindows + 1
    If PoolConfig.OnLineCfg(5).Value = 1 Then OnlineWindows = OnlineWindows + 1
    If (PoolConfig.OnLineCfg(1).Value = 1) Or (MainIsOnline = True) Then OnlineWindows = OnlineWindows + 1
    If (PoolConfig.OnLineCfg(3).Value = 1) Or (MainIsOnline = True) Then OnlineWindows = OnlineWindows + 1
    If PoolConfig.OnLineCfg(4).Value = 1 Then OnlineWindows = OnlineWindows + 1
    DoEvents
    
    If PoolConfig.OnLineCfg(4).Value = 1 Then Load RcvInfo
    DoEvents
    If (PoolConfig.OnLineCfg(1).Value = 1) Or (MainIsOnline = True) Then Load RcvTelex
    DoEvents
    If PoolConfig.OnLineCfg(2).Value = 1 Then Load SndTelex
    DoEvents
    If PoolConfig.OnLineCfg(5).Value = 1 Then Load PndTelex
    DoEvents
    If (PoolConfig.OnLineCfg(3).Value = 1) Or (MainIsOnline = True) Then Load OutTelex
    
    TelexPoolHead.InitMemButtons
    If GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "START_ON_TOP", "YES") = "YES" Then TelexPoolHead.OnTop.Value = 1
    If MyMainPurpose <> "AUTO_SEND" Then
        MainFormPrepareLayout = True
        TelexPoolHead.WindowState = vbNormal
    End If
    If OnlineWindows > 0 Then
        If AutoOpenOnline Then
            If MyMainPurpose <> "AUTO_SEND" Then
                'THIS IS TOO EARLY AND MUST BE
                'MANAGED IN THE MAIN FORM ITSELF
                'TelexPoolHead.OnLine.Value = 1
                'TelexPoolHead.FixWin.Value = 1
                'TelexPoolHead.FixWin.Value = 0
            End If
        End If
    Else
        TelexPoolHead.WindowState = vbNormal
    End If
    
    'THIS DO-EVENTS WILL REFRESH
    'AND INITIALIZE THE MAIN FORM
    'IF NOT STARTED AS AUTO-MVT
    DoEvents
    
    MainIsDeploy = tmpMainIsDeploy
    
    Screen.MousePointer = 11
    If (CedaIsConnected) And ((Not MainIsOnline) Or (MyMainPurpose = "AUTO_SEND")) Then
        If (AutoRefreshOnline) Or (MyMainPurpose = "AUTO_SEND") Then
            If FormIsLoaded("RcvInfo") Then TelexPoolHead.RefreshOnlineWindows "INF"
            DoEvents
            If FormIsLoaded("RcvTelex") Then TelexPoolHead.RefreshOnlineWindows "RCV"
            DoEvents
            If FormIsLoaded("SndTelex") Then TelexPoolHead.RefreshOnlineWindows "SND"
            DoEvents
            If FormIsLoaded("PndTelex") Then TelexPoolHead.RefreshOnlineWindows "PND"
            DoEvents
            If FormIsLoaded("OutTelex") Then TelexPoolHead.RefreshOnlineWindows "OUT"
            DoEvents
        End If
    End If
    
    If MyMainPurpose = "AUTO_SEND" Then
        'Most probably already loaded
        '(From loading online records)
        Load CreateTelex
    End If
    
    
    'This causes ambigous focus
    'events in the main forms
    'SetAllFormsOnTop True
    
    If CedaIsConnected Then
        UfisServer.UrnoPoolInit 1
        'DoEvents
    End If
    
    'Due to problems in bcserv32 (ignoring first broadcasts)
    '-------------------------------------------------------
    If CedaIsConnected Then UfisServer.ConnectToBcProxy
    If UfisServer.HostName <> "LOCAL" Then
        If (Not MainIsOnline) And (CedaIsConnected) Then
            Screen.MousePointer = 11
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "1/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "2/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "3/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "4/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "5/6", "", 0, True, False
            UfisServer.CallCeda UserAnswer, "SBC", "TLXTAB", "LOGIN", "TELEXPOOL", "6/6", "", 0, True, False
        End If
    End If
    '-------------------------------------------------------
    
    SystemResizing = False
    ApplicationIsStarted = True
    'DoEvents
    UfisServer.ConnectToApplMgr
    'DoEvents
    If MyMainPurpose <> "AUTO_SEND" Then
        TelexPoolHead.SetupAndStartWork "MAIN"
    Else
        If (ApplicationIsReadyForUse) And (Not MainFormIsReadyForUse) Then
            TelexPoolHead.SetupAndStartWork "MAIN"
        End If
    End If
    Screen.MousePointer = 0
End Sub

Private Function DetermineIniFile() As Boolean
    Dim tmpStr As String
    
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
    tmpStr = GetItem(Command, 2, "INI=")
    If tmpStr <> "" Then
        myIniFile = GetItem(tmpStr, 1, " ")
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
        If tmpStr = "" Then
            tmpStr = "Sorry, can't find config file:" & vbNewLine
            tmpStr = tmpStr & Chr(34) & myIniFullName & Chr(34) & vbNewLine
            tmpStr = tmpStr & "Using default configuration."
            MsgBox tmpStr
            tmpStr = ""
        End If
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        myIniFile = App.EXEName & UfisServer.HOPO & ".ini"
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        myIniFile = App.EXEName & ".ini"
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        GetFileAndPath DEFAULT_CEDA_INI, myIniFile, myIniPath
        myIniFullName = myIniPath & "\" & myIniFile
        tmpStr = Dir(myIniFullName)
    End If
    If UCase(tmpStr) <> UCase(myIniFile) Then
        DetermineIniFile = False
    Else
        DetermineIniFile = True
    End If
End Function

Private Function LoginProcedure() As Boolean
    Dim LoginAnsw As String
    Dim tmpRegStrg As String
    Set UfisServer.LoginCall.UfisComCtrl = UfisServer.aCeda
    UfisServer.LoginCall.applicationName = "TelexPool"
    'LoginAnsw = UfisServer.LoginCall.DoLoginSilentMode("UFIS$ADMIN", "Passwort")
    tmpRegStrg = ""
    tmpRegStrg = tmpRegStrg & "TelexPool" & ","
    '                  FKTTAB:  SUBD   ,  FUNC  ,    FUAL              ,TYPE,STAT
    tmpRegStrg = tmpRegStrg & "InitModu,InitModu,Initialisieren (InitModu),B,-"
    'tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
    'tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
    UfisServer.LoginCall.InfoAppVersion = UfisServer.GetApplVersion(True)
    UfisServer.LoginCall.VersionString = UfisServer.GetApplVersion(True)
    UfisServer.LoginCall.RegisterApplicationString = tmpRegStrg
    UfisServer.LoginCall.InfoCaption = "Info about TelexPool"
    UfisServer.LoginCall.InfoButtonVisible = True
    UfisServer.LoginCall.InfoUfisVersion = "UFIS Version 4.5"
    UfisServer.LoginCall.InfoAppVersion = CStr("Telexpool " + UfisServer.GetApplVersion(True))
    UfisServer.LoginCall.InfoCopyright = "� 2001-2010 UFIS Airport Solutions GmbH"
    UfisServer.LoginCall.InfoAAT = "UFIS Airport Solutions GmbH"
    'ufisserver.LoginCall.u
    UfisServer.LoginCall.UserNameLCase = True
    
    'UfisServer.LoginCall.InfoAppVersion = "TelexPool"
    'UfisServer.LoginCall.ApplicationName = "TelexPool"
    'UfisServer.LoginCall.InfoCaption = "TelexPool"
    'UfisServer.LoginCall.InfoUfisVersion = "TelexPool"
    'UfisServer.LoginCall.UseVersionInfo = False
    'UfisServer.LoginCall.Version = "TelexPool"
    
    UfisServer.LoginCall.ShowLoginDialog
    LoginAnsw = UfisServer.LoginCall.GetPrivileges("InitModu")
    gsUserName = "TelexPool"
    If LoginAnsw <> "" Then
        LoginProcedure = True
        gsUserName = UfisServer.LoginCall.GetUserName()
        UfisServer.ModName.Tag = gsUserName
    Else
        LoginProcedure = False
    End If
End Function

Public Sub RefreshMain()
    'Nothing to do
End Sub

Public Function CheckCancelExit(CurForm As Form, MaxForms As Integer) As Boolean
    Dim UseMaxForms As Integer
    UseMaxForms = 1
    If CountVisibleForms <= UseMaxForms Then
        'CheckCancelExit = False
        HiddenMain.SetShutDownTimer CurForm
        CheckCancelExit = True
    Else
        CurForm.Hide
        CheckCancelExit = True
    End If
End Function
Public Sub ShutDownApplication(ShowMsgBox As Boolean)
    Dim FormName As String
    Dim ErrMsg As String
    Dim i As Integer
    Dim cnt As Integer
    Dim MsgBoxAnsw As Integer
    Dim sveBool As Boolean
    On Error GoTo ErrorHandler
    ShutDownRequested = False
    sveBool = OpenedFromFips
    OpenedFromFips = FipsWasConnected
    MsgBoxAnsw = 1
    If ShowMsgBox Then MsgBoxAnsw = MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer)
    If MsgBoxAnsw = 1 Then
        ShutDownRequested = True
        If FormIsLoaded("FindFips") Then
            FindFips.FipsStatusStop "", Nothing
            Unload FindFips
        End If
        If FormIsLoaded("MouseClock") Then
            MouseClock.StopMouseClock -1
            DoEvents
            Unload MouseClock
            DoEvents
        End If
        'MUST BE UNLOADED FIRST
        'BUT I DON'T KNOW WHY
        'ELSE VB6 WILL CRASH
        If FormIsLoaded("HiddenMain") Then
            Unload HiddenMain
            DoEvents
        End If
        
        Unload UfisServer
        DoEvents
        Unload TelexPoolHead
        DoEvents
        
        For i = 0 To Forms.count - 1
            Forms(i).Hide
            Forms(i).Refresh
            DoEvents
        Next
        cnt = Forms.count
        While cnt > 0
            i = 0
            Unload Forms(i)
            DoEvents
            cnt = Forms.count
        Wend
    End If
    If OpenedFromFips = False Then OpenedFromFips = sveBool
    If Not ShowMsgBox Then End
    Exit Sub
ErrorHandler:
    ErrMsg = Err.Description
    'Resume Next
    End
End Sub
Public Function GetFormIndex(FormName As String) As Integer
    Dim i As Integer
    Dim idx As Integer
    For i = 0 To Forms.count - 1
        If Forms(i).Name = FormName Then
            idx = i
            Exit For
        End If
    Next i
    GetFormIndex = idx
End Function
Public Sub SetAllFormsOnTop(SetValue As Boolean)

'========
 'Exit Sub
'========

    If FormIsLoaded("RcvInfo") Then CheckFormOnTop RcvTelex, SetValue, False
    If FormIsLoaded("RcvTelex") Then CheckFormOnTop RcvTelex, SetValue, False
    If FormIsLoaded("SndTelex") Then CheckFormOnTop SndTelex, SetValue, False
    If FormIsLoaded("OutTelex") Then CheckFormOnTop OutTelex, SetValue, False
    If FormIsLoaded("EditTelex") Then CheckFormOnTop EditTelex, SetValue, False
    If FormIsLoaded("CreateTelex") Then CheckFormOnTop CreateTelex, SetValue, False
    CheckFormOnTop TelexPoolHead, SetValue, True
End Sub

Public Sub HandleBroadCastSpooler(BcLineCount As Long, BcEvent As String)
    Dim BcCnt As Long
    UfisBroadCastIsAvailable = True
    BcCnt = BcLineCount
    'MainDialog.AdjustBroadCastPanel BcLineCount, BcEvent, 0
End Sub
Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, tws As String, twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    Dim s As String
    Dim clDest As String
    Dim clRecv As String
    Dim clCmd As String
    Dim clTable As String
    Dim clSelKey As String
    Dim clFields As String
    Dim clData As String
    If Not ShutDownRequested Then
        NewMsgFromBcHdl = True
        UfisBroadCastIsAvailable = True
        clTable = ObjName
        If InStr("TLXTAB,AFTTAB,TFRTAB,TFNTAB,TLKTAB", clTable) > 0 Then
            clCmd = CedaCmd
            clSelKey = CedaSqlKey
            clFields = Fields
            clData = Data
            TelexPoolHead.EvaluateBc 0, 0, BcNum, DestName, RecvName, clTable, clCmd, clSelKey, clFields, clData, -1
        ElseIf InStr("MADTAB,MTPTAB", clTable) > 0 Then
            If FormIsLoaded("CreateTelex") Then
                CreateTelex.InitTlxAddresses clTable
            End If
        ElseIf InStr("MVTGEN", clTable) > 0 Then
            If FormIsLoaded("TextShape") Then
                Unload TextShape
            End If
        ElseIf CedaCmd = "CLO" Then
            MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute"
            ShutDownApplication False
        End If
        NewMsgFromBcHdl = False
    End If
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim nlPos As Integer
    Dim tmpParam1 As String
    Dim tmpParam2 As String
    NewMsgFromFips = True
    tmpParam1 = GetRealItem(Data, 0, " ")
    tmpParam2 = GetRealItem(Data, 1, " ")
    If Len(tmpParam2) > 0 And tmpParam2 <> gsUserName Then
        gsUserName = tmpParam2
        UfisServer.ModName.Tag = gsUserName
    End If
    If Not OpenedFromFips Then
        OpenedFromFips = True
        FipsWasConnected = True
        FipsIsUpAndRunning = True
        TelexPoolHead.chkExit.Caption = "Close"
    End If
    If (ApplicationIsStarted) And (Not ModalMsgIsOpen) Then
        TelexPoolHead.Show
        If TelexPoolHead.WindowState <> vbNormal Then TelexPoolHead.WindowState = vbNormal
        TelexPoolHead.Refresh
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            If Left(tmpDatLst, 2) <> "-1" Then
                CheckFormOnTop TelexPoolHead, True, True
                TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
            End If
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
    NewMsgFromFips = False
End Sub

Public Sub HandleDisplayChanged()
    With TelexPoolHead
        If .Width > UfisTools.SysInfo.WorkAreaWidth Then
            .Left = UfisTools.SysInfo.WorkAreaLeft
            .Width = UfisTools.SysInfo.WorkAreaWidth
        End If
        If .Height > UfisTools.SysInfo.WorkAreaHeight Then
            .Top = UfisTools.SysInfo.WorkAreaTop
            .Height = UfisTools.SysInfo.WorkAreaHeight
        End If
    End With
End Sub

Public Sub HandleSysColorsChanged()
    TelexPoolHead.ArrangeAllWin
End Sub

Public Sub HandleTimeChanged()
    'TelexPoolHead.InitDateFields
End Sub

Public Function FilterTelexAddress(UseText As String, AddrList As String, NoCheckTypes As String, CurTtyp As String, HeaderOnly As Boolean, CurSere As String) As Boolean
    Dim CurAddress As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    Dim CurText As String
    Dim CurOrig As String
    
    ShowTelex = False
    If (NoCheckTypes <> "") And (CurTtyp <> "") Then
        If InStr(NoCheckTypes, CurTtyp) > 0 Then ShowTelex = True
    End If
    If Not ShowTelex Then
        CurText = UseText
        CurOrig = "xx"
        If HeaderOnly Then
            If InStr(UseText, "=DESTINATION TYPE B") > 0 Then
                GetKeyItem CurText, UseText, "=DESTINATION TYPE B", "="
                CurText = Replace(CurText, ".", " ", 1, -1, vbBinaryCompare)
                GetKeyItem CurOrig, UseText, "=ORIGIN", "="
                CurOrig = "." & Mid(CurOrig, 2)
            Else
                CurText = GetItem(UseText, 1, ".")
                CurOrig = "." & Left(GetItem(UseText, 2, "."), 10)
            End If
        End If
        CurItem = 0
        CurAddress = GetRealItem(AddrList, CurItem, ",")
        If CurSere <> "S" Then
            While (CurAddress <> "") And (ShowTelex = False)
                If InStr(CurText, CurAddress) > 0 Then ShowTelex = True
                CurItem = CurItem + 1
                CurAddress = GetRealItem(AddrList, CurItem, ",")
            Wend
        Else
            While (CurAddress <> "") And (ShowTelex = False)
                If InStr(CurOrig, CurAddress) > 0 Then ShowTelex = True
                CurItem = CurItem + 1
                CurAddress = GetRealItem(AddrList, CurItem, ",")
            Wend
        End If
    End If
    FilterTelexAddress = ShowTelex
End Function

Public Function FilterAddressHeader(UseText As String, AddrList As String, NoCheckTypes As String, CurTtyp As String, retval As Boolean) As Boolean
    Dim CurAddress As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    Dim CurText As String
    Dim CurOrig As String
    ShowTelex = retval
    If ShowTelex Then
        If (AddrList <> "") And (NoCheckTypes <> "") And (CurTtyp <> "") Then
            If InStr(NoCheckTypes, CurTtyp) > 0 Then ShowTelex = False
        End If
        If (ShowTelex) And (AddrList <> "") Then
            If InStr(UseText, "=DESTINATION TYPE B") > 0 Then
                GetKeyItem CurText, UseText, "=DESTINATION TYPE B", "="
                CurText = Replace(CurText, ".", " ", 1, -1, vbBinaryCompare)
                GetKeyItem CurOrig, UseText, "=ORIGIN", "="
                CurOrig = "." & Mid(CurOrig, 2)
            Else
                CurText = GetItem(UseText, 1, ".")
                CurOrig = "." & Left(GetItem(UseText, 2, "."), 10)
            End If
            CurItem = 0
            CurAddress = GetRealItem(AddrList, CurItem, ",")
            While (CurAddress <> "") And (ShowTelex = True)
                If (InStr(CurText, CurAddress) = 0) And (InStr(CurOrig, CurAddress) = 0) Then ShowTelex = False
                CurItem = CurItem + 1
                CurAddress = GetRealItem(AddrList, CurItem, ",")
            Wend
        End If
    End If
    FilterAddressHeader = ShowTelex
End Function

Public Function FilterTelexText(CurText As String, PatternList As String, DefRetVal As Boolean) As Boolean
    Dim CurPattern As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    ShowTelex = DefRetVal
    If ShowTelex Then
        CurItem = 0
        CurPattern = GetRealItem(PatternList, CurItem, ",")
        While (CurPattern <> "") And (ShowTelex = True)
            If InStr(CurText, CurPattern) > 0 Then ShowTelex = False
            CurItem = CurItem + 1
            CurPattern = GetRealItem(PatternList, CurItem, ",")
        Wend
    End If
    FilterTelexText = ShowTelex
End Function
Public Function FilterTelexTypeText(CurType As String, CurText As String, FilterList As String, DefRetVal As Boolean) As Boolean
    Dim PatternList As String
    Dim CurCode As String
    Dim CurPattern As String
    Dim CurItem As Long
    Dim ShowTelex As Boolean
    ShowTelex = DefRetVal
    If ShowTelex Then
        If CurType = "" Then
            CurCode = "{=FREE=}"
        Else
            CurCode = "{=" & CurType & "=}"
        End If
        GetKeyItem PatternList, FilterList, CurCode, "{="
        If PatternList <> "" Then
            CurItem = 0
            CurPattern = GetRealItem(PatternList, CurItem, ",")
            While (CurPattern <> "") And (ShowTelex = True)
                If InStr(CurText, CurPattern) > 0 Then ShowTelex = False
                CurItem = CurItem + 1
                CurPattern = GetRealItem(PatternList, CurItem, ",")
            Wend
        End If
    End If
    FilterTelexTypeText = ShowTelex
End Function

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    'MyPanel.Cls
    MyPanel.AutoRedraw = True
    ReturnColor = vbWhite
    'Exit Function
    
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    End If
    MyPanel.Picture = MyPanel.Image
    MyPanel.AutoRedraw = False
    MyPanel.Cls
    DrawBackGround = ReturnColor
End Function

Public Function CleanTrimString(CurText As String) As String
    Dim UseText As String
    Dim tmpChr As String
    UseText = CurText
    UseText = Replace(UseText, Chr(1), "", 1, -1, vbBinaryCompare)
    UseText = Replace(UseText, Chr(3), "", 1, -1, vbBinaryCompare)
    tmpChr = Left(UseText, 1)
    While (tmpChr = vbLf) Or (tmpChr = vbCr)
        UseText = Mid(UseText, 2)
        tmpChr = Left(UseText, 1)
    Wend
    tmpChr = Right(UseText, 1)
    While (tmpChr = vbLf) Or (tmpChr = vbCr)
        UseText = Left(UseText, Len(UseText) - 1)
        tmpChr = Right(UseText, 1)
    Wend
    tmpChr = vbCr & vbCr
    UseText = Replace(UseText, tmpChr, vbCr, 1, -1, vbBinaryCompare)
    CleanTrimString = UseText
End Function

Public Sub ArrangeTabCursor(CurTab As TABLib.Tab)
    Dim VisLines As Long
    Dim SelLine As Long
    Dim TopLine As Long
    Dim BotLine As Long
    SelLine = CurTab.GetCurrentSelected
    If SelLine >= 0 Then
        TopLine = CurTab.GetVScrollPos
        VisLines = ((CurTab.Height / 15) / CurTab.lineHeight) - 1
        VisLines = VisLines - 1
        BotLine = TopLine + VisLines - 1
        If (SelLine < TopLine) Or (SelLine > BotLine) Then
            CurTab.OnVScrollTo SelLine - 1
        End If
    End If
End Sub

Public Sub ArrangeTabMaxScroll(CurTab As TABLib.Tab)
    Dim VisLines As Long
    Dim SelLine As Long
    Dim TopLine As Long
    Dim BotLine As Long
    BotLine = CurTab.GetLineCount - 1
    If BotLine > 0 Then
        VisLines = ((CurTab.Height / 15) / CurTab.lineHeight) - 1
        VisLines = VisLines - 1
        TopLine = BotLine - VisLines + 1
        If (TopLine > 0) Then
            CurTab.OnVScrollTo TopLine
        End If
    End If
End Sub

Public Sub SyncWindowSize(NewLeft As Long, NewWidth As Long)
    Dim WinSize As Long
    If KeybdIsCtrl Then
        If Not SystemResizing Then
            SystemResizing = True
            WinSize = NewWidth
            If FormIsVisible("RcvInfo") Then
                If RcvInfo.WindowState = vbNormal Then
                    RcvInfo.Left = NewLeft
                    RcvInfo.Width = WinSize
                End If
            End If
            If FormIsVisible("RcvTelex") Then
                If RcvTelex.WindowState = vbNormal Then
                    RcvTelex.Left = NewLeft
                    RcvTelex.Width = WinSize
                End If
            End If
            If FormIsVisible("SndTelex") Then
                If SndTelex.WindowState = vbNormal Then
                    SndTelex.Left = NewLeft
                    SndTelex.Width = WinSize
                End If
            End If
            If FormIsVisible("PndTelex") Then
                If PndTelex.WindowState = vbNormal Then
                    PndTelex.Left = NewLeft
                    PndTelex.Width = WinSize
                End If
            End If
            If FormIsVisible("OutTelex") Then
                If OutTelex.WindowState = vbNormal Then
                    OutTelex.Left = NewLeft
                    OutTelex.Width = WinSize
                End If
            End If
            SystemResizing = False
        End If
    End If
End Sub

Public Function CreateEmptyLine(FieldList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim FldNam As String
    Result = ""
    ItemNo = 1
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        Result = Result & ","
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    CreateEmptyLine = Result
End Function

Public Function CedaTimeDiff(CurDateTime1 As String, CurDateTime2 As String) As Long
    Dim tmpDate1
    Dim tmpDate2
    If (CurDateTime1 <> "") And (CurDateTime2 <> "") Then
        tmpDate1 = CedaFullDateToVb(CurDateTime1)
        tmpDate2 = CedaFullDateToVb(CurDateTime2)
        CedaTimeDiff = DateDiff("n", tmpDate1, tmpDate2)
    Else
        CedaTimeDiff = 0
    End If
End Function

Public Function GetMinutesFromHHMM(StrgTime As String) As Integer
    Dim tmpStrg As String
    Dim tmpFix As String
    Dim iMin As Integer
    tmpFix = ""
    tmpStrg = Trim(StrgTime)
    If Left(tmpStrg, 1) = "-" Then
        tmpFix = Left(tmpStrg, 1)
        tmpStrg = Mid(tmpStrg, 2)
    End If
    If Len(tmpStrg) <> 4 Then tmpStrg = Right("0000" & tmpStrg, 4)
    iMin = (Val(Mid(tmpStrg, 1, 2)) * 60) + Val(Mid(tmpStrg, 3, 2))
    If tmpFix <> "" Then iMin = -iMin
    GetMinutesFromHHMM = iMin
End Function

Public Function GetHHMMFromMinutes(MinValue As Integer) As String
    Dim tmpStrg As String
    Dim tmpHH As String
    Dim tmpMM As String
    Dim tmpFix As String
    Dim useValue As Integer
    useValue = MinValue
    tmpFix = ""
    If MinValue < 0 Then
        tmpFix = "-"
        useValue = -useValue
    End If
    tmpHH = CStr(useValue \ 60)
    tmpMM = CStr(useValue Mod 60)
    tmpHH = Right("00" & tmpHH, 2)
    tmpMM = Right("00" & tmpMM, 2)
    tmpStrg = tmpHH & tmpMM
    tmpStrg = tmpFix & tmpStrg
    GetHHMMFromMinutes = tmpStrg
End Function

Public Sub SwitchMainWindows(ForWhat As String)
    Dim CurSize As Long
    On Error Resume Next
    If ForWhat = "MAIN" Then
        MainOpenedByUser = True
        If Not TelexPoolHead.Visible Then TelexPoolHead.Show
        If TelexPoolHead.WindowState <> vbNormal Then TelexPoolHead.WindowState = vbNormal
        TelexPoolHead.ZOrder
        If Not MainFormIsReadyForUse Then TelexPoolHead.SetupAndStartWork "MAIN"
        'Dirty Trick but efficient
        'CurSize = TelexPoolHead.Width
        'TelexPoolHead.Width = CurSize - 15
        'TelexPoolHead.Width = CurSize
        DoEvents
    End If
    If ForWhat = "CREATE" Then
        If Not CreateTelex.Visible Then CreateTelex.Show
        If CreateTelex.WindowState <> vbNormal Then CreateTelex.WindowState = vbNormal
        CreateTelex.ZOrder
    End If
End Sub

Public Sub ShapeMyForm(ForWhat As String, CurForm As Form, CurImage As Image)
    Dim TopDiff As Long
    Dim lftDiff As Long
    Dim rgn As Long
    Dim Top As Single
    Dim lft As Single
    Dim wid As Single
    Dim hgt As Single
    Dim UseTop As Long
    Dim UseLeft As Long
    Dim UseHeight As Long
    Dim UseWidth As Long
    Select Case ForWhat
        Case "INTRO"
            lftDiff = (CurForm.Width - CurForm.ScaleWidth) / 2
            TopDiff = (CurForm.Height - CurForm.ScaleHeight) - lftDiff
            UseTop = CurImage.Top + TopDiff - 15
            UseLeft = CurImage.Left + lftDiff - 15
            UseLeft = UseLeft
            UseTop = UseTop
            UseWidth = CurImage.Width
            UseHeight = CurImage.Height
            Top = (UseTop / 15) + 2
            lft = UseLeft / 15
            wid = UseWidth / 15
            hgt = (UseHeight / 15) - 3
            rgn = CreateEllipticRgn(lft, Top, lft + wid, Top + hgt)
            SetWindowRgn CurForm.hwnd, rgn, True
            DeleteObject rgn
        Case Else
    End Select
End Sub

Public Sub SetTextBoxSelected(MyTextBox As TextBox)
    Dim i As Integer
    If TypeName(MyTextBox) = "TextBox" Then
        i = Len(MyTextBox.Text)
        MyTextBox.SelStart = 0
        MyTextBox.SelLength = i
    End If
End Sub

Public Function GetMyStartupWindowPlace(MyName As String) As Long
    Static NxtPlace As Long
    Dim MyPlace As Long
    MyPlace = NxtPlace
    NxtPlace = NxtPlace + 1
    If (PoolConfig.OnLineCfg(4).Value = 1) And (MyName = "RcvInfo") Then MyPlace = NxtPlace
    If (PoolConfig.OnLineCfg(1).Value = 1) And (MyName = "RcvTelex") Then MyPlace = NxtPlace
    If (PoolConfig.OnLineCfg(2).Value = 1) And (MyName = "SndTelex") Then MyPlace = NxtPlace
    If (PoolConfig.OnLineCfg(5).Value = 1) And (MyName = "PndTelex") Then MyPlace = NxtPlace
    If (PoolConfig.OnLineCfg(3).Value = 1) And (MyName = "OutTelex") Then MyPlace = NxtPlace
    NxtPlace = MyPlace
    GetMyStartupWindowPlace = MyPlace
End Function

Public Function SetObjectFaceColor(CurForm As Form)
    Dim ctl As Control
    Dim CtlName As String
    Dim strCtlType As String
    Dim idx As Integer
    'On Error Resume Next
    For Each ctl In CurForm.Controls
        strCtlType = TypeName(ctl)
        CtlName = ctl.Name
        If InStr("fraSetupX,optResize,chkBorder,chkSetup,fraOnline,chkOnline", CtlName) = 0 Then
           Select Case strCtlType
               Case "OptionButton", "Label"
                   ctl.BackColor = MyOwnButtonFace
               Case "CheckBox"
                    ctl.BackColor = MyOwnButtonFace
               Case "Frame"
                   If InStr(CtlName, "Frame") = 0 Then
                       ctl.BackColor = MyOwnFrameColor
                   End If
               Case "PictureBox"
                   ctl.BackColor = MyOwnPanelColor
               'Case "StatusBar"
               '    does not work
               Case Else
            End Select
        End If
    Next
End Function


