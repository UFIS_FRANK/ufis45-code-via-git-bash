VERSION 5.00
Begin VB.Form PoolConfig 
   Caption         =   "Telex Pool Configuration"
   ClientHeight    =   6225
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5145
   Icon            =   "PoolConfig.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6225
   ScaleWidth      =   5145
   Begin VB.CheckBox DataPoolCfg 
      Caption         =   "Memory"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   3990
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   630
      Width           =   1020
   End
   Begin VB.Frame DataPoolConfig 
      Caption         =   "Additional Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1605
      Left            =   2010
      TabIndex        =   20
      Top             =   90
      Width           =   1815
      Begin VB.CheckBox DataPoolCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   1020
         Width           =   1020
      End
      Begin VB.PictureBox Picture5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   120
         Picture         =   "PoolConfig.frx":030A
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox DataPoolCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   690
         Width           =   1020
      End
      Begin VB.CheckBox DataPoolCfg 
         Caption         =   "Flights"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   360
         Width           =   1020
      End
   End
   Begin VB.Frame FipsLinkConfig 
      Caption         =   "Deploy && Assign"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1125
      Left            =   2010
      TabIndex        =   18
      Top             =   2910
      Width           =   1815
      Begin VB.PictureBox Picture6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   120
         Picture         =   "PoolConfig.frx":074C
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox FipsLinkCfg 
         Caption         =   "Flights"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   690
         Width           =   1020
      End
      Begin VB.CheckBox FipsLinkCfg 
         Caption         =   "Folders"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   360
         Width           =   1020
      End
   End
   Begin VB.Frame TlxGenConfig 
      Caption         =   "Telex Generator"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1770
      Left            =   2010
      TabIndex        =   13
      Top             =   4080
      Width           =   1815
      Begin VB.CheckBox TlxGenCfg 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   1350
         Width           =   1020
      End
      Begin VB.CheckBox TlxGenCfg 
         Caption         =   "AAD"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   1020
         Width           =   1020
      End
      Begin VB.CheckBox TlxGenCfg 
         Caption         =   "BRS"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   690
         Width           =   1020
      End
      Begin VB.PictureBox Picture3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   120
         Picture         =   "PoolConfig.frx":0A56
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox TlxGenCfg 
         Caption         =   "MVT"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   360
         Width           =   1020
      End
   End
   Begin VB.Frame EditorConfig 
      Caption         =   "Folders && Groups"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1125
      Left            =   2010
      TabIndex        =   11
      Top             =   1740
      Width           =   1815
      Begin VB.PictureBox Picture4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   120
         Picture         =   "PoolConfig.frx":0D60
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox TlxFolderCfg 
         Caption         =   "Users"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   690
         Width           =   1020
      End
      Begin VB.CheckBox TlxFolderCfg 
         Caption         =   "Folders"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   360
         Width           =   1020
      End
   End
   Begin VB.Frame OnlineConfig 
      Caption         =   "Windows"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2445
      Left            =   120
      TabIndex        =   8
      Top             =   1590
      Width           =   1815
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Pending"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   1350
         Width           =   1020
      End
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "SCORE IF"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   2010
         Width           =   1020
      End
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Sent Out"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   1680
         Width           =   1020
      End
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Created"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   1020
         Width           =   1020
      End
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Received"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   690
         Width           =   1020
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   120
         Picture         =   "PoolConfig.frx":11A2
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox OnLineCfg 
         Caption         =   "Main"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   360
         Width           =   1020
      End
   End
   Begin VB.Frame MainConfig 
      Caption         =   "Main Module"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      TabIndex        =   5
      Top             =   90
      Width           =   1815
      Begin VB.CheckBox MainCfg 
         Caption         =   "Monitor"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   1020
         Width           =   1020
      End
      Begin VB.CheckBox MainCfg 
         Caption         =   "Loader"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   690
         Width           =   1020
      End
      Begin VB.CheckBox MainCfg 
         Caption         =   "System"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   360
         Width           =   1020
      End
      Begin VB.PictureBox IconBox 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   120
         Picture         =   "PoolConfig.frx":14AC
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
   End
   Begin VB.CheckBox chkSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3990
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   5100
      Width           =   1020
   End
   Begin VB.CheckBox FixWin 
      Caption         =   "Arrange"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3990
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   330
      Width           =   1020
   End
   Begin VB.CheckBox chkReset 
      Caption         =   "Reset"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3990
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   4800
      Width           =   1020
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3990
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   1020
   End
   Begin VB.CommandButton btnClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3990
      TabIndex        =   0
      Top             =   5385
      Width           =   1020
   End
   Begin VB.Frame FltHdlConfig 
      Caption         =   "Online Filter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1755
      Left            =   120
      TabIndex        =   16
      Top             =   4080
      Width           =   1815
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   120
         Picture         =   "PoolConfig.frx":18EE
         ScaleHeight     =   480
         ScaleWidth      =   480
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   270
         Width           =   480
      End
      Begin VB.CheckBox BcHdlCfg 
         Caption         =   "SCORE IF"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   1350
         Width           =   1020
      End
      Begin VB.CheckBox BcHdlCfg 
         Caption         =   "Sent Out"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   1020
         Width           =   1020
      End
      Begin VB.CheckBox BcHdlCfg 
         Caption         =   "Created"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   690
         Width           =   1020
      End
      Begin VB.CheckBox BcHdlCfg 
         Caption         =   "Received"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   360
         Width           =   1020
      End
   End
End
Attribute VB_Name = "PoolConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Variant
Private MyCallButton As Control

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    Set MyCallButton = CallButton
End Sub

Private Sub BcHdlCfg_Click(Index As Integer)
    CheckFormOnTop Me, False, False
    If BcHdlCfg(Index).Value = 1 Then
        Select Case Index
            Case 0
                'BcHdlCfg(1).Value = 0
                MySetUp.ShowSetupGroup 2, "Online Windows Telex Type Filter"
                MySetUp.Show , Me
            Case 1
        End Select
    Else
        MySetUp.Hide
    End If
    CheckFormOnTop Me, True, False
End Sub

Private Sub btnClose_Click()
    TelexPoolHead.chkSetup.Value = 0
    Me.Hide
End Sub

Private Sub DataPoolCfg_Click(Index As Integer)
    CheckFormOnTop Me, False, False
    If DataPoolCfg(Index).Value = 1 Then
        DataPoolCfg(Index).BackColor = MyOwnButtonDown
        Select Case Index
            Case 0
                DataPool.Show , Me
            Case 1
                MySetUp.FlightBrowser.Value = 1
                TelexPoolHead.ArrangeFlightList True
            Case Else
                DataPoolCfg(Index).Value = 0
        End Select
    Else
        DataPoolCfg(Index).BackColor = MyOwnButtonFace
        Select Case Index
            Case 0
                DataPool.Hide
            Case 1
                MySetUp.FlightBrowser.Value = 0
                TelexPoolHead.ArrangeFlightList False
            Case Else
        End Select
    End If
    CheckFormOnTop Me, True, False
End Sub

Private Sub FipsLinkCfg_Click(Index As Integer)
    If FipsLinkCfg(Index).Value = 1 Then
        FipsLinkCfg(Index).BackColor = MyOwnButtonDown
        Select Case Index
            Case 0
                MainIsDeploy = True
                TelexPoolHead.ArrangeBrowsers True
            Case 1
                MainIsFltDeploy = True
                TelexPoolHead.ArrangeFlightList True
            Case Else
        End Select
    Else
        FipsLinkCfg(Index).BackColor = MyOwnButtonFace
        Select Case Index
            Case 0
                MainIsDeploy = False
                TelexPoolHead.ArrangeBrowsers False
            Case 1
                MainIsFltDeploy = False
                TelexPoolHead.ArrangeFlightList False
            Case Else
        End Select
    End If
End Sub

Private Sub FixWin_Click()
    If FixWin.Value = 1 Then TelexPoolHead.FixWin.Value = 0
    TelexPoolHead.FixWin.Value = FixWin.Value
End Sub

Private Sub Form_Activate()
    Static isDone As Boolean
    On Error Resume Next
    With MyParent
        OnTop.Value = .OnTop.Value
    End With
    If Not isDone Then
        isDone = True
        'GetLocation Me
        'CenterForm Me
        'ResizeForm Me
        'lblInstructions.Font = SetFontSize()
    End If
End Sub

Private Sub Form_Load()
    Dim x As String
    Dim clTxtDef As String
    On Error Resume Next
    With MyParent
        Left = .Left + 50 * Screen.TwipsPerPixelX
        Top = .Top + 50 * Screen.TwipsPerPixelY
    End With
    If MyMainPurpose = "AUTO_SEND" Then clTxtDef = "NO" Else clTxtDef = "YES"
    x = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_MAIN", clTxtDef)
    If MyMainPurpose = "AUTO_SEND" Then x = "YES"
    'HOTFIX BECAUSE VB6 CRASHES
    'WHEN MAIN IS NOT VISIBLE
    x = "YES"
    If x = "YES" Then OnLineCfg(0).Value = 1
    x = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_RECV", "YES")
    If x = "YES" Then OnLineCfg(1).Value = 1
    If MyMainPurpose = "AUTO_SEND" Then clTxtDef = "YES" Else clTxtDef = "NO"
    x = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SEND", clTxtDef)
    If MyMainPurpose = "AUTO_SEND" Then x = "YES"
    If x = "YES" Then OnLineCfg(2).Value = 1
    If MyMainPurpose = "AUTO_SEND" Then clTxtDef = "YES" Else clTxtDef = "NO"
    x = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_PEND", clTxtDef)
    'If MyMainPurpose = "AUTO_SEND" Then X = "YES"
    If x = "YES" Then OnLineCfg(5).Value = 1 Else OnLineCfg(5).Value = 0
    If MyMainPurpose = "AUTO_SEND" Then clTxtDef = "YES" Else clTxtDef = "NO"
    x = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_SENT", clTxtDef)
    If MyMainPurpose = "AUTO_SEND" Then x = "YES"
    If x = "YES" Then OnLineCfg(3).Value = 1
    x = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "SHOW_INFO", "NO")
    If x = "YES" Then OnLineCfg(4).Value = 1
    LoadedWindows = 0
    LoadedWindows = LoadedWindows + OnLineCfg(1).Value
    LoadedWindows = LoadedWindows + OnLineCfg(2).Value
    LoadedWindows = LoadedWindows + OnLineCfg(3).Value
    LoadedWindows = LoadedWindows + OnLineCfg(4).Value
    LoadedWindows = LoadedWindows + OnLineCfg(5).Value
    If LoadedWindows = 0 Then OnLineCfg(0).Value = 1
    'Note: This loads and opens the main dialog
    'Moved to Main Dialog form_load
    'TlxFolderCfg(0).Enabled = TelexPoolHead.chkAssFolder.Enabled
    'TlxFolderCfg(1).Enabled = TelexPoolHead.chkAssFolder.Enabled
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        'MyCallButton.Value = 0
    End If
End Sub

Private Sub Form_Resize()
    'ResizeControls Me
    'SetFontSize
End Sub

Private Sub MainCfg_Click(Index As Integer)
    CheckFormOnTop Me, False, False
    If MainCfg(Index).Value = 1 Then
        Select Case Index
            Case 0
                MainCfg(1).Value = 0
                MySetUp.ShowSetupGroup 0, "General System Configuration"
                MySetUp.Show , Me
            Case 1
                MainCfg(0).Value = 0
                MySetUp.ShowSetupGroup 1, "Data Loader Configuration"
                MySetUp.Show , Me
        End Select
    Else
        MySetUp.Hide
    End If
    CheckFormOnTop Me, True, False
End Sub

Private Sub OnLineCfg_Click(Index As Integer)
    Dim OldValue As Integer
    If OnLineCfg(Index).Value = 1 Then OnLineCfg(Index).BackColor = LightGreen Else OnLineCfg(Index).BackColor = vbButtonFace
    If ApplicationIsStarted Then
        OldValue = TelexPoolHead.FixWin.Value
        Select Case Index
            Case 0
                If OnLineCfg(Index).Value = 1 Then
                    TelexPoolHead.Show
                Else
                    TelexPoolHead.Hide
                End If
            Case 1
                If OnLineCfg(Index).Value = 1 Then
                    RcvTelex.Show
                    LoadedWindows = LoadedWindows + 1
                Else
                    RcvTelex.Hide
                    LoadedWindows = LoadedWindows - 1
                End If
                TelexPoolHead.ArrangeBrowsers True
            Case 2
                If OnLineCfg(Index).Value = 1 Then
                    SndTelex.Show
                    LoadedWindows = LoadedWindows + 1
                Else
                    SndTelex.Hide
                    LoadedWindows = LoadedWindows - 1
                End If
            Case 3
                If OnLineCfg(Index).Value = 1 Then
                    OutTelex.Show
                    LoadedWindows = LoadedWindows + 1
                Else
                    OutTelex.Hide
                    LoadedWindows = LoadedWindows - 1
                End If
                TelexPoolHead.ArrangeBrowsers True
            Case 4
                If OnLineCfg(Index).Value = 1 Then
                    RcvInfo.Show
                    LoadedWindows = LoadedWindows + 1
                Else
                    RcvInfo.Hide
                    LoadedWindows = LoadedWindows - 1
                End If
            Case 5
                If OnLineCfg(Index).Value = 1 Then
                    PndTelex.Show
                    LoadedWindows = LoadedWindows + 1
                Else
                    PndTelex.Hide
                    LoadedWindows = LoadedWindows - 1
                End If
            Case Else
        End Select
        If LoadedWindows = 0 Then OnLineCfg(0).Value = 1
        FixWin.Value = 0
        FixWin.Value = 1
        FixWin.Value = OldValue
    End If
End Sub

Private Sub OnTop_Click()
    On Error Resume Next
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    MyParent.OnTop.Value = OnTop.Value
    Me.SetFocus
End Sub

Private Sub TlxFolderCfg_Click(Index As Integer)
    CheckFormOnTop Me, False, False
    If TlxFolderCfg(Index).Value = 1 Then
        TlxFolderCfg(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                ConfigFolders.Show
            Case 1
                ConfigUsers.Show
            Case Else
        End Select
    Else
        TlxFolderCfg(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
                ConfigFolders.Hide
            Case 1
                ConfigUsers.Hide
            Case Else
        End Select
    End If
    CheckFormOnTop Me, True, False
End Sub

