Attribute VB_Name = "WinEvents"
Option Explicit

'Option Compare Text


Public CMouse As CMouseWheel
Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
    (ByVal hwnd As Long, _
    ByVal nIndex As Long, _
    ByVal dwNewLong As Long) As Long

'Public Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" _
'    (ByVal lpPrevWndFunc As Long, _
'     ByVal hwnd As Long, _
'     ByVal msg As Long, _
'     ByVal wParam As Long, _
'     ByVal lParam As Long) As Long
     
'Public Const GWL_WNDPROC = -4
Public Const WM_MouseWheel = &H20A
Public lpPrevWndProc As Long
Public Function WindowProc(ByVal hwnd As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Select Case uMsg
        Case WM_MouseWheel
            CMouse.FireWheelRotation ByVal hwnd, ByVal uMsg, ByVal wParam, ByVal lParam
            If CMouse.UfisMouseWheelCancel = False Then
                WindowProc = CallWindowProc(lpPrevWndProc, hwnd, uMsg, wParam, lParam)
            End If
            
        Case Else
           WindowProc = CallWindowProc(lpPrevWndProc, hwnd, uMsg, wParam, lParam)
    End Select
End Function


