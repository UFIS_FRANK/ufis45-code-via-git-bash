VERSION 5.00
Begin VB.Form TlxRcvStat 
   Caption         =   "RCV Telex Status"
   ClientHeight    =   2820
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8280
   Icon            =   "TlxRcvStat.frx":0000
   LinkTopic       =   "Form3"
   ScaleHeight     =   2820
   ScaleWidth      =   8280
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkWork 
      Caption         =   "Update"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   7380
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   1470
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   7380
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   1770
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   7380
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   2070
      Width           =   855
   End
   Begin VB.Frame fraRcvStat 
      Caption         =   "Work Flow"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   1
      Left            =   4830
      TabIndex        =   17
      Top             =   300
      Width           =   2505
      Begin VB.CheckBox chkRecv 
         Caption         =   "27"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   27
         Left            =   120
         TabIndex        =   60
         Top             =   2190
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "26"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   26
         Left            =   120
         TabIndex        =   58
         Top             =   1975
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "25"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   25
         Left            =   120
         TabIndex        =   45
         Top             =   1760
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "24"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   24
         Left            =   120
         TabIndex        =   44
         Top             =   1545
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "23"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   23
         Left            =   120
         TabIndex        =   43
         Top             =   1330
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "22"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   22
         Left            =   120
         TabIndex        =   42
         Top             =   1115
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "21"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   21
         Left            =   120
         TabIndex        =   41
         Top             =   900
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "20"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   20
         Left            =   120
         TabIndex        =   40
         Top             =   685
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "19"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   19
         Left            =   120
         TabIndex        =   39
         Top             =   470
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "18"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   120
         TabIndex        =   38
         Top             =   270
         Width           =   525
      End
      Begin VB.Label lblRecv 
         Caption         =   "27"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   27
         Left            =   660
         TabIndex        =   61
         Top             =   2198
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "26"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   26
         Left            =   660
         TabIndex        =   59
         Top             =   1982
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "25"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   25
         Left            =   660
         TabIndex        =   53
         Top             =   1768
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "24"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   24
         Left            =   660
         TabIndex        =   52
         Top             =   1560
         Width           =   1725
      End
      Begin VB.Label lblRecv 
         Caption         =   "23"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   23
         Left            =   660
         TabIndex        =   51
         Top             =   1340
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "22"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   22
         Left            =   660
         TabIndex        =   50
         Top             =   1126
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "21"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   21
         Left            =   660
         TabIndex        =   49
         Top             =   912
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "20"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   660
         TabIndex        =   48
         Top             =   698
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "19"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   19
         Left            =   660
         TabIndex        =   47
         Top             =   484
         Width           =   1455
      End
      Begin VB.Label lblRecv 
         Caption         =   "18"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   660
         TabIndex        =   46
         Top             =   270
         Width           =   1455
      End
   End
   Begin VB.Frame fraRcvStat 
      Caption         =   "System"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Index           =   0
      Left            =   15
      TabIndex        =   0
      Top             =   300
      Width           =   4785
      Begin VB.CheckBox chkRecv 
         Caption         =   "17"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   17
         Left            =   2310
         TabIndex        =   55
         Top             =   2198
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "16"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   16
         Left            =   120
         TabIndex        =   54
         Top             =   2190
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   2310
         TabIndex        =   29
         Top             =   270
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   9
         Left            =   2310
         TabIndex        =   28
         Top             =   497
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   10
         Left            =   2310
         TabIndex        =   27
         Top             =   739
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "11"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   11
         Left            =   2310
         TabIndex        =   26
         Top             =   981
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "12"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   12
         Left            =   2310
         TabIndex        =   25
         Top             =   1223
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "13"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   13
         Left            =   2310
         TabIndex        =   24
         Top             =   1465
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "14"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   14
         Left            =   2310
         TabIndex        =   23
         Top             =   1707
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "15"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   15
         Left            =   2310
         TabIndex        =   22
         Top             =   1949
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   7
         Left            =   120
         TabIndex        =   8
         Top             =   1942
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   496
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   737
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   3
         Left            =   120
         TabIndex        =   4
         Top             =   978
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   4
         Left            =   120
         TabIndex        =   5
         Top             =   1219
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   5
         Left            =   120
         TabIndex        =   6
         Top             =   1460
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   6
         Left            =   120
         TabIndex        =   7
         Top             =   1701
         Width           =   525
      End
      Begin VB.CheckBox chkRecv 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   270
         Width           =   525
      End
      Begin VB.Label lblRecv 
         Caption         =   "17"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   2850
         TabIndex        =   57
         Top             =   2198
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "16"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   660
         TabIndex        =   56
         Top             =   2198
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   2850
         TabIndex        =   37
         Top             =   255
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "14"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   14
         Left            =   2850
         TabIndex        =   36
         Top             =   1707
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "13"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   13
         Left            =   2850
         TabIndex        =   35
         Top             =   1465
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "12"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   2850
         TabIndex        =   34
         Top             =   1223
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "11"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   2850
         TabIndex        =   33
         Top             =   981
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   2850
         TabIndex        =   32
         Top             =   739
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   2850
         TabIndex        =   31
         Top             =   497
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "15"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   15
         Left            =   2850
         TabIndex        =   30
         Top             =   1949
         Width           =   1845
      End
      Begin VB.Label lblRecv 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   660
         TabIndex        =   16
         Top             =   1949
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   660
         TabIndex        =   15
         Top             =   1707
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   660
         TabIndex        =   14
         Top             =   1465
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   660
         TabIndex        =   13
         Top             =   1223
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   660
         TabIndex        =   12
         Top             =   981
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   660
         TabIndex        =   11
         Top             =   739
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   660
         TabIndex        =   10
         Top             =   497
         Width           =   1605
      End
      Begin VB.Label lblRecv 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   660
         TabIndex        =   9
         Top             =   255
         Width           =   1605
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Status of Received Telexes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   30
      TabIndex        =   21
      Top             =   30
      Width           =   2235
   End
End
Attribute VB_Name = "TlxRcvStat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MyParent As Variant
Private MyCallButton As Control
Private UseMultiSel As Boolean

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    Set MyCallButton = CallButton
End Sub
Public Sub SetMultiSelect(SetValue As Boolean)
    UseMultiSel = SetValue
    chkWork(2).Enabled = Not SetValue
End Sub
Private Sub InitForm(ForWhat As Integer)
    Dim StatTxt As String
    Dim tmpStat As String
    Dim i As Integer
    Dim j As Integer
    UseMultiSel = False
    Select Case ForWhat
        Case 1
            For i = 0 To 27
                chkRecv(i).Caption = ""
                chkRecv(i).Enabled = False
                lblRecv(i).Caption = ""
            Next
            'System
            StatTxt = ""
            StatTxt = StatTxt & "U,Flight updated,"                     '0
            StatTxt = StatTxt & "H,Historical,"                         '1
            StatTxt = StatTxt & "T,Type recognized,"                    '2
            StatTxt = StatTxt & "A,Flight assigned,"                    '3
            StatTxt = StatTxt & "N,Flight not found,"                   '4
            StatTxt = StatTxt & "O,Other Airport,"                      '5
            StatTxt = StatTxt & "?,Too many flights,"                   '6
            StatTxt = StatTxt & "Z,Unknown Type,"                       '7
            StatTxt = StatTxt & "P,Partially stored (PTM),"             '8
            StatTxt = StatTxt & ".,Msg too early (ATC),"                '9
            StatTxt = StatTxt & "L,Msg too late (ATC),"                 '10
            StatTxt = StatTxt & "R,Bad Routing (ATC),"                  '11
            StatTxt = StatTxt & "F,No Updates (ATC),"                   '12
            StatTxt = StatTxt & "W,Initially written (DB),"             '13
            StatTxt = StatTxt & "1,Processing 1.Step,"                  '14
            StatTxt = StatTxt & "E,Erroneous text,"                     '15
            StatTxt = StatTxt & "J,Flight Too Old,"                     '16
            StatTxt = StatTxt & "K,Flight Already Closed"               '17
            tmpStat = "START"
            i = 0
            j = 0
            While tmpStat <> ""
                j = j + 1
                tmpStat = GetItem(StatTxt, j, ",")
                If tmpStat <> "" Then
                    chkRecv(i).Caption = tmpStat
                    j = j + 1
                    lblRecv(i).Caption = GetItem(StatTxt, j, ",")
                    chkRecv(i).Enabled = True
                End If
                i = i + 1
            Wend
            'Work Flow
            StatTxt = ""
            StatTxt = StatTxt & "A,Telex assigned to flight,"           '18
            StatTxt = StatTxt & "C,Created by Tlxgen,"                  '19
            StatTxt = StatTxt & "E,Error,"                              '20
            StatTxt = StatTxt & "I,Imported,"                           '21
            StatTxt = StatTxt & "M,Manual Creation,"                    '22
            StatTxt = StatTxt & "O,Open,"                               '23
            StatTxt = StatTxt & "R,Received" + "/" + "Redirect,"                  '24
            StatTxt = StatTxt & "S,Telex sent by Tlxgen,"               '25
            StatTxt = StatTxt & "T,Send to Import Tool,"                '26
            StatTxt = StatTxt & "V,Already View"                        '27
            tmpStat = "START"
            i = 18
            j = 0
            While tmpStat <> ""
                j = j + 1
                tmpStat = GetItem(StatTxt, j, ",")
                If tmpStat <> "" Then
                    chkRecv(i).Caption = tmpStat
                    j = j + 1
                    lblRecv(i).Caption = GetItem(StatTxt, j, ",")
                    chkRecv(i).Enabled = True
                End If
                i = i + 1
            Wend
        Case Else
    End Select
End Sub
Public Function ExplainStatus(tmpFld, tmpVal, CheckIt As Boolean) As String
    Dim tmpResult As String
    Dim i As Integer
    Dim j1 As Integer
    Dim j2 As Integer
    Dim UseIdx As Integer
    SetMultiSelect (Not CheckIt)
    tmpResult = ""
    Select Case tmpFld
        Case "STAT"
            UseIdx = 0
            j1 = 0
            j2 = 17
        Case "WSTA"
            UseIdx = 1
            j1 = 18
            j2 = 27
        Case Else
            j1 = 0
            j2 = -1
    End Select
    For i = j1 To j2
        If CheckIt Then chkRecv(i).Value = 0
        If (chkRecv(i).Enabled) And (chkRecv(i).Caption = tmpVal) Then
            fraRcvStat(UseIdx).Tag = Str(i)
            If CheckIt Then
                chkRecv(i).Value = 1
            Else
                tmpResult = lblRecv(i).Caption
                Exit For
            End If
        End If
    Next
    If tmpResult <> "" Then tmpResult = tmpVal & " = " & tmpResult
    ExplainStatus = tmpResult
End Function

Private Sub chkRecv_Click(Index As Integer)
    Dim LastIdx As Integer
    Dim UseIdx As Integer
    If (Not UseMultiSel) Then
        If Index < 8 Then UseIdx = 0 Else UseIdx = 8
        If chkRecv(UseIdx).Tag <> "" Then LastIdx = Val(chkRecv(UseIdx).Tag) Else LastIdx = -1
        If chkRecv(Index).Value = 1 Then
            If LastIdx >= 0 Then chkRecv(LastIdx).Value = 0
            chkRecv(UseIdx).Tag = Str(Index)
            chkWork(2).Tag = chkRecv(0).Tag & "," & chkRecv(8).Tag
            If chkWork(2).Tag <> (fraRcvStat(0).Tag & "," & fraRcvStat(1).Tag) Then chkWork(2).Enabled = True Else chkWork(2).Enabled = False
        Else
            If LastIdx = Index Then chkRecv(UseIdx).Tag = ""
            chkWork(2).Tag = chkRecv(0).Tag & "," & chkRecv(8).Tag
        End If
    Else
        TelexPoolHead.CheckLoadButton False
    End If
End Sub

Private Sub chkWork_Click(Index As Integer)
    On Error Resume Next
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
    Select Case Index
        Case 0  'close
            If chkWork(Index).Value = 1 Then
                If IsObject(MyCallButton) Then MyCallButton.Value = 0
                chkWork(Index).Value = 0
            End If
        Case 2
            If chkWork(Index).Value = 1 Then
                UpdateStatusFields
                chkWork(Index).Value = 0
            End If
    End Select
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
End Sub

Private Sub Form_Load()
    Me.Top = TelexPoolHead.Top + 130 * Screen.TwipsPerPixelY
    Me.Left = TelexPoolHead.Left + 250 * Screen.TwipsPerPixelX
    InitForm 1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        chkWork(0).Value = 1
        Cancel = True
    End If
End Sub

Private Sub UpdateStatusFields()
    Dim tmpUrno As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpSqlKey As String
    Dim retval As String
    Dim RetCode As Integer
    Dim UseIdx As Integer
    tmpUrno = Trim(TelexPoolHead.CurrentUrno.Text)
    If tmpUrno <> "" Then
        tmpFldLst = ""
        tmpDatLst = ""
        If chkRecv(0).Tag <> "" Then
            UseIdx = Val(chkRecv(0).Tag)
            tmpFldLst = tmpFldLst & ",STAT"
            tmpDatLst = tmpDatLst & "," & chkRecv(UseIdx).Caption
        End If
        If chkRecv(8).Tag <> "" Then
            UseIdx = Val(chkRecv(8).Tag)
            tmpFldLst = tmpFldLst & ",WSTA"
            tmpDatLst = tmpDatLst & "," & chkRecv(UseIdx).Caption
        End If
        If tmpFldLst <> "" Then
            tmpFldLst = Mid(tmpFldLst, 2)
            tmpDatLst = Mid(tmpDatLst, 2)
            tmpSqlKey = "WHERE URNO=" & tmpUrno
            RetCode = UfisServer.CallCeda(retval, "URT", DataPool.TableName, tmpFldLst, tmpDatLst, tmpSqlKey, "", 0, True, False)
        End If
    'Else
    '    MyMsgBox.CallAskUser 0, 0, 0, "Update Manager", "No data selected to update.", "stop", "", UserAnswer
    End If
End Sub

Public Function GetStatusFilter() As String
    Dim Result As String
    Dim tmpList As String
    Dim i As Integer
    Result = ""
    tmpList = ""
    For i = 0 To 7
        If chkRecv(i).Value = 1 Then tmpList = tmpList & ",'" & chkRecv(i).Caption & "'"
    Next
    tmpList = Mid(tmpList, 2)
    If tmpList <> "" Then Result = "STAT IN (" & tmpList & ")"
    tmpList = ""
    For i = 8 To 17
        If chkRecv(i).Value = 1 Then tmpList = tmpList & ",'" & chkRecv(i).Caption & "'"
    Next
    tmpList = Mid(tmpList, 2)
    If tmpList <> "" Then
        If Result <> "" Then Result = Result & " AND "
        Result = Result & "WSTA IN (" & tmpList & ")"
    End If
    GetStatusFilter = Result
End Function
