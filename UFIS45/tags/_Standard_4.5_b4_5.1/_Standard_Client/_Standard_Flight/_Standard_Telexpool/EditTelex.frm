VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form EditTelex 
   Caption         =   "Ufis Telex Editor"
   ClientHeight    =   7665
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12885
   Icon            =   "EditTelex.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7665
   ScaleWidth      =   12885
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkTplSel 
      Caption         =   "L"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   3500
      Style           =   1  'Graphical
      TabIndex        =   68
      ToolTipText     =   "View linked telex templates"
      Top             =   2010
      Width           =   250
   End
   Begin VB.CheckBox chkTplSel 
      Caption         =   "C"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   3250
      Style           =   1  'Graphical
      TabIndex        =   67
      ToolTipText     =   "View combined telex templates"
      Top             =   2010
      Width           =   250
   End
   Begin VB.CheckBox chkTemplate 
      Caption         =   "Flights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   6750
      Style           =   1  'Graphical
      TabIndex        =   63
      Top             =   2300
      Width           =   900
   End
   Begin VB.TextBox txtTplDesc 
      Height          =   285
      Left            =   2760
      TabIndex        =   65
      Top             =   6960
      Width           =   3500
   End
   Begin VB.TextBox txtTplName 
      Height          =   285
      Left            =   120
      TabIndex        =   64
      Top             =   6960
      Width           =   2415
   End
   Begin VB.Frame fraFlightData 
      Caption         =   "Flight Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1515
      Left            =   10080
      TabIndex        =   49
      Top             =   1770
      Visible         =   0   'False
      Width           =   2385
      Begin VB.CheckBox chkFltData 
         Caption         =   "Read"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   240
         Width           =   600
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Save"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   705
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   240
         Width           =   585
      End
      Begin VB.CheckBox chkFltData 
         Caption         =   "Telex"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1305
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   240
         Width           =   600
      End
      Begin VB.PictureBox FltInputPanel 
         AutoRedraw      =   -1  'True
         Height          =   855
         Left            =   60
         ScaleHeight     =   795
         ScaleWidth      =   2205
         TabIndex        =   50
         Top             =   570
         Width           =   2265
         Begin VB.PictureBox FltEditPanel 
            AutoRedraw      =   -1  'True
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   0
            Left            =   30
            ScaleHeight     =   375
            ScaleWidth      =   2145
            TabIndex        =   54
            TabStop         =   0   'False
            Top             =   30
            Width           =   2145
            Begin VB.CheckBox chkFltTick 
               Height          =   210
               Index           =   0
               Left            =   60
               TabIndex        =   57
               Top             =   75
               Width           =   195
            End
            Begin VB.TextBox txtFltInput 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   1110
               MaxLength       =   7
               TabIndex        =   56
               Text            =   "12MAY04"
               Top             =   30
               Width           =   975
            End
            Begin VB.CheckBox chkFltCheck 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   780
               Style           =   1  'Graphical
               TabIndex        =   55
               Top             =   30
               Visible         =   0   'False
               Width           =   180
            End
            Begin VB.Label lblFltField 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "A/C-Chg"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   300
               TabIndex        =   58
               Top             =   75
               Width           =   735
            End
         End
      End
   End
   Begin VB.TextBox NewCallCode 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7920
      TabIndex        =   48
      Top             =   6210
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.TextBox NewSendUrno 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5940
      TabIndex        =   47
      Top             =   6210
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.TextBox CurrentRecord 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3960
      TabIndex        =   33
      Top             =   6210
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.CheckBox chkOutFormat 
      Caption         =   "Format"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4380
      Style           =   1  'Graphical
      TabIndex        =   32
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame fraTestCase 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   345
      Left            =   9180
      TabIndex        =   28
      Top             =   0
      Visible         =   0   'False
      Width           =   3465
      Begin VB.CheckBox chkTest 
         Caption         =   "Tool"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   30
         Width           =   855
      End
      Begin VB.Label lblTestCase 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Test Case"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1050
         TabIndex        =   29
         Top             =   30
         Width           =   2235
      End
   End
   Begin VB.CheckBox chkUcase 
      Caption         =   "UC"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3075
      Style           =   1  'Graphical
      TabIndex        =   27
      ToolTipText     =   "Convert text to upper case"
      Top             =   30
      Width           =   420
   End
   Begin VB.CheckBox chkLcase 
      Caption         =   "LC"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   26
      ToolTipText     =   "Allow lower case typing"
      Top             =   30
      Width           =   420
   End
   Begin VB.CheckBox chkTransmit 
      Caption         =   "Transmit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5250
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame fraPreview 
      Caption         =   "Telex Text Preview"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Left            =   7290
      TabIndex        =   21
      Top             =   1770
      Visible         =   0   'False
      Width           =   2715
      Begin VB.TextBox txtPreview 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2025
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   22
         Top             =   300
         Width           =   2055
      End
   End
   Begin VB.Frame fraTlxPrio 
      Caption         =   "Telex Priority [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   3960
      TabIndex        =   19
      Top             =   420
      Width           =   2985
      Begin TABLib.TAB TlxPrioTab 
         Height          =   915
         Left            =   90
         TabIndex        =   20
         Top             =   240
         Width           =   2805
         _Version        =   65536
         _ExtentX        =   4948
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraDblSign 
      Caption         =   "Double Sign [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   9840
      TabIndex        =   17
      Top             =   420
      Width           =   2985
      Begin TABLib.TAB DblSignTab 
         Height          =   915
         Left            =   90
         TabIndex        =   18
         Top             =   240
         Width           =   2805
         _Version        =   65536
         _ExtentX        =   4948
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraOrigAddr 
      Caption         =   "Telex Originator [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   60
      TabIndex        =   15
      Top             =   420
      Width           =   3795
      Begin TABLib.TAB TlxOrigTab 
         Height          =   915
         Left            =   90
         TabIndex        =   16
         Top             =   240
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraTlxType 
      Caption         =   "Telex Type [+]"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   7050
      TabIndex        =   13
      Top             =   420
      Width           =   2685
      Begin TABLib.TAB TlxTypeTab 
         Height          =   915
         Left            =   90
         TabIndex        =   14
         Top             =   240
         Width           =   2505
         _Version        =   65536
         _ExtentX        =   4419
         _ExtentY        =   1614
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraAddress 
      Caption         =   "Address Pool"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4995
      Left            =   60
      TabIndex        =   9
      Top             =   1770
      Width           =   3795
      Begin VB.CheckBox chkTplSel 
         Caption         =   "T"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2940
         Style           =   1  'Graphical
         TabIndex        =   66
         ToolTipText     =   "View / Edit telex template"
         Top             =   240
         Width           =   250
      End
      Begin TABLib.TAB TemplateTab 
         Height          =   1215
         Left            =   90
         TabIndex        =   60
         Top             =   3360
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   2143
         _StockProps     =   64
      End
      Begin TABLib.TAB GroupTab 
         Height          =   1815
         Left            =   90
         TabIndex        =   59
         Top             =   2760
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   3201
         _StockProps     =   64
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "Templates"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2000
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "View / Edit selected telex template category"
         Top             =   240
         Width           =   950
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "Groups"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1040
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "View / Edit telex address groups"
         Top             =   240
         Width           =   950
      End
      Begin VB.CheckBox chkSelAddr 
         Caption         =   "MVT Tlx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   240
         Width           =   950
      End
      Begin TABLib.TAB AddressTab 
         Height          =   2415
         Left            =   90
         TabIndex        =   10
         Top             =   2130
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   4260
         _StockProps     =   64
      End
      Begin TABLib.TAB ApcTab 
         Height          =   585
         Left            =   90
         TabIndex        =   12
         Top             =   1500
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   1032
         _StockProps     =   64
      End
      Begin TABLib.TAB AlcTab 
         Height          =   585
         Left            =   90
         TabIndex        =   11
         Top             =   870
         Width           =   3615
         _Version        =   65536
         _ExtentX        =   6376
         _ExtentY        =   1032
         _StockProps     =   64
      End
      Begin VB.Frame fraChkAddr 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   90
         TabIndex        =   37
         Top             =   555
         Width           =   3645
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Del"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   3030
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   0
            Width           =   585
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Upd"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   2430
            Style           =   1  'Graphical
            TabIndex        =   43
            Top             =   0
            Width           =   585
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Ins"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1830
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   0
            Width           =   585
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Clear"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   1215
            Style           =   1  'Graphical
            TabIndex        =   41
            Top             =   0
            Width           =   600
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Airp."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   615
            Style           =   1  'Graphical
            TabIndex        =   39
            Top             =   0
            Width           =   585
         End
         Begin VB.CheckBox chkMvtAddr 
            Caption         =   "Airl."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   38
            Top             =   0
            Width           =   600
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   7380
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19632
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraDestAddr 
      Caption         =   "Destination Addresses"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1905
      Left            =   3960
      TabIndex        =   6
      Top             =   1770
      Visible         =   0   'False
      Width           =   6045
      Begin VB.CheckBox chkTemplate 
         Caption         =   "Link"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   62
         ToolTipText     =   "Link (save) telex template with address group"
         Top             =   400
         Width           =   900
      End
      Begin VB.CheckBox chkTemplate 
         Caption         =   "Combine"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2790
         Style           =   1  'Graphical
         TabIndex        =   61
         ToolTipText     =   "Combine (save) telex template and telex addresses"
         Top             =   240
         Width           =   900
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Arrange"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   990
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   240
         Width           =   900
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Merge"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   240
         Width           =   900
      End
      Begin VB.CheckBox chkTlxAddr 
         Caption         =   "Reload"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1890
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   240
         Width           =   900
      End
      Begin VB.TextBox txtDestAddr 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1245
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   7
         Top             =   570
         Width           =   5775
      End
   End
   Begin VB.Frame fraTelexText 
      Caption         =   "Telex Text"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   3960
      TabIndex        =   4
      Top             =   3780
      Width           =   6045
      Begin VB.TextBox txtTelexText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1755
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   5
         Top             =   240
         Width           =   5745
      End
   End
   Begin VB.CheckBox chkCopy 
      Caption         =   "Copy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1770
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkSend 
      Caption         =   "Sen&d"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6990
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkPreView 
      Caption         =   "Preview"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3510
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7860
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Width           =   855
   End
End
Attribute VB_Name = "EditTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MeIsVisible As Boolean
Dim TransmitEnabled As Boolean

Private Sub AddressTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpBeme As String
    If (Selected) And (LineNo >= 0) Then
        tmpBeme = AddressTab.GetColumnValue(LineNo, 3)
        tmpBeme = CleanString(tmpBeme, FOR_CLIENT, True)
        StatusBar1.Panels(2).Text = tmpBeme
    End If
End Sub

Private Sub AddressTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim LineStatus As Long
    Dim tmpTead As String
    Dim tmpTxt As String
    If LineNo >= 0 Then
        tmpTead = AddressTab.GetColumnValue(LineNo, 2)
        LineStatus = AddressTab.GetLineStatusValue(LineNo)
        If LineStatus = 0 Then
            SetAddrMarker LineNo, tmpTead, True
        Else
            SetAddrMarker LineNo, tmpTead, False
            If chkTlxAddr(0).Value = 1 Then
                tmpTxt = txtDestAddr.Text
                tmpTxt = Replace(tmpTxt, tmpTead, "", 1, -1, vbBinaryCompare)
                txtDestAddr.Text = tmpTxt
                txtDestAddr.Text = GetCleanAddrList(txtDestAddr.Text, 8, " ")
            End If
        End If
        FillAddrField
    ElseIf LineNo = -1 Then
        AddressTab.Sort CStr(ColNo), True, True
        AddressTab.AutoSizeColumns
        AddressTab.Refresh
    End If
End Sub
Private Sub SetAddrMarker(SetLineNo As Long, SetAddrList As String, SetMarker As Boolean)
    Dim LineNo As Long
    Dim tmpTead As String
    Dim tmpHit As String
    Dim CurLineNbr As String
    Dim AddrItm As Long
    Dim LineItm As Long
    AddrItm = 0
    tmpTead = GetRealItem(SetAddrList, AddrItm, ",")
    While tmpTead <> ""
        tmpHit = AddressTab.GetLinesByColumnValue(2, tmpTead, 0)
        LineItm = 0
        CurLineNbr = GetRealItem(tmpHit, LineItm, ",")
        While CurLineNbr <> ""
            LineNo = Val(CurLineNbr)
            If LineNo >= 0 Then
                If SetMarker Then
                    AddressTab.SetDecorationObject LineNo, 2, "SelMarker"
                    AddressTab.SetLineStatusValue LineNo, 1
                    AddressTab.SetLineColor LineNo, vbBlack, vbWhite
                Else
                    AddressTab.ResetLineDecorations LineNo
                    AddressTab.SetLineStatusValue LineNo, 0
                    AddressTab.SetLineColor LineNo, vbBlack, LightGray
                End If
            End If
            LineItm = LineItm + 1
            CurLineNbr = GetRealItem(tmpHit, LineItm, ",")
        Wend
        AddrItm = AddrItm + 1
        tmpTead = GetRealItem(SetAddrList, AddrItm, ",")
    Wend
End Sub

Private Sub AlcTab_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    AlcTab.Tag = ""
    LookForTlxAddr True
End Sub

Private Sub AlcTab_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    AlcTab.Tag = CStr(ColNo)
    If ColNo = 6 Then
        AlcTab.OnHScrollTo 0
        chkMvtAddr(1).Value = 1
    End If
End Sub

Private Sub AlcTab_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If AlcTab.Tag <> "" Then
        Select Case Key
            Case 40
                chkMvtAddr(1).Value = 1
            Case Else
        End Select
    End If
End Sub

Private Sub ApcTab_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    ApcTab.Tag = ""
    LookForTlxAddr True
End Sub

Private Sub ApcTab_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    On Error Resume Next
    ApcTab.Tag = CStr(ColNo)
    If ColNo = 6 Then
        ApcTab.OnHScrollTo 0
        AddressTab.SetFocus
    End If
End Sub

Private Sub ApcTab_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If ApcTab.Tag <> "" Then
        Select Case Key
            Case 38
                chkMvtAddr(0).Value = 1
            Case Else
        End Select
    End If
End Sub

Private Sub LookForTlxAddr(RefreshList As Boolean)
    Dim AlcCol  As Long
    Dim ApcCol As Long
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim AddrList As String
    Dim CurAlc As String
    Dim CurApc As String
    Dim CurLook As String
    Dim ColList As String
    Dim NewLine As String
    Dim CurUrno As String
    Dim LastUrno As String
    Dim tmpCheck As String
    Dim tmpList As String
    CurLine = 0
    ColList = "1,2,3,5,4,6"
    UfisServer.BasicData(0).SetInternalLineBuffer True
    AddressTab.ResetContent
    AddressTab.SetUniqueFields "5"
    For AlcCol = 0 To 5
        CurAlc = Trim(AlcTab.GetColumnValue(0, AlcCol))
        tmpCheck = CurAlc
        If Len(tmpCheck) = 2 Then tmpCheck = TelexPoolHead.CheckAlcLookUp(tmpCheck, False)
        If Len(tmpCheck) > 3 Then tmpCheck = TelexPoolHead.AskForUniqueAlc(tmpCheck, CurAlc)
        CurAlc = tmpCheck
        If CurAlc <> "" Then
            CurAlc = Left(CurAlc & "   ", 3)
            For ApcCol = 0 To 6
                CurApc = Trim(ApcTab.GetColumnValue(0, ApcCol))
                If (CurApc <> "") Or (ApcCol = 6) Then
                    CurApc = Left(CurApc & "   ", 3)
                    CurLook = CurAlc & CurApc
                    LineNo = Val(UfisServer.BasicData(0).GetLinesByIndexValue("ALCAPC", CurLook, 0))
                    While LineNo >= 0
                        LineNo = UfisServer.BasicData(0).GetNextResultLine
                        If LineNo >= 0 Then
                            NewLine = UfisServer.BasicData(0).GetColumnValues(LineNo, ColList)
                            AddressTab.InsertTextLine NewLine, False
                            AddressTab.SetDecorationObject CurLine, 2, "SelMarker"
                            AddressTab.SetLineStatusValue CurLine, 1
                            CurLine = CurLine + 1
                        End If
                    Wend
                End If
            Next
        End If
    Next
    UfisServer.BasicData(0).SetInternalLineBuffer False
    AddressTab.Sort 2, True, True
    AddressTab.AutoSizeColumns
    AddressTab.OnHScrollTo 2
    AlcTab.Refresh
    AddressTab.Refresh
    tmpList = Trim(AddressTab.SelectDistinct("4", "", "", ",", True))
    If Left(tmpList, 1) = "," Then tmpList = Mid(tmpList, 2)
    InitDblSignList tmpList
    AddressTab.SetCurrentSelection 0
    If RefreshList Then FillAddrField
End Sub
Private Sub FillAddrField()
    Dim tmpList As String
    Dim tmpText As String
    Dim tmpTead As String
    Dim CurItm As Integer
    tmpList = GetOutAddrList
    If chkTlxAddr(0).Value = 0 Then
        txtDestAddr.Text = tmpList
    Else
        tmpText = txtDestAddr.Text
        CurItm = 1
        tmpTead = GetItem(tmpList, CurItm, ",")
        While tmpTead <> ""
            If InStr(tmpText, tmpTead) = 0 Then tmpText = tmpText & " " & tmpTead
            CurItm = CurItm + 1
            tmpTead = GetItem(tmpList, CurItm, ",")
        Wend
        txtDestAddr.Text = tmpText
    End If
    txtDestAddr.Text = GetCleanAddrList(txtDestAddr.Text, 8, " ")
End Sub

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        'If TestCaseMode Then
            chkTest.Value = 0
            Unload TestCases
        'End If
        Unload EditGroups
        Unload EditTemplates
        MeIsVisible = False
        Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub chkCopy_Click()
    Dim TlxRec As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    Dim tmpText As String
    Dim tmpData As String
    Dim tmpVal As String
    Dim tmpLen As Integer
    Dim TlxIsFileType As Boolean
    On Error Resume Next
    If chkCopy.Value = 1 Then
        chkPreView.Value = 0
        fraFlightData.Visible = False
        Form_Resize
        TlxRec = TelexPoolHead.CurrentRecord.Text
        Me.CurrentRecord.Text = TlxRec
        Me.NewCallCode.Text = "C"
        tmpSere = GetFieldValue("SERE", TlxRec, DataPool.TlxTabFields)
        tmpTtyp = GetFieldValue("TTYP", TlxRec, DataPool.TlxTabFields)
        tmpData = TelexPoolHead.txtTelexText
        If TelexPoolHead.FullText.Value = 1 Then
            If tmpSere = "C" Then TlxIsFileType = True
            If InStr(tmpData, "=DESTINATION TYPE B") > 0 Then TlxIsFileType = True
            If TlxIsFileType Then
                tmpText = tmpData
                GetKeyItem tmpData, tmpText, "=TEXT", "="
                If tmpTtyp = "FREE" Then
                    GetKeyItem tmpVal, tmpText, "=SMI", "="
                    tmpVal = CleanTrimString(tmpVal)
                    If tmpVal <> "" Then tmpTtyp = tmpVal
                End If
            End If
        End If
        tmpText = CleanTrimString(tmpData)
        If tmpTtyp = "" Then tmpTtyp = "FREE"
        SetTelexDefault TlxTypeTab, tmpTtyp
        If TlxIsFileType Then
            If tmpTtyp <> "FREE" Then
                tmpLen = Len(tmpTtyp)
                If tmpLen > 0 Then
                    If Left(tmpText, tmpLen) <> tmpTtyp Then
                        tmpText = tmpTtyp & vbNewLine & tmpText
                    End If
                End If
            End If
        End If
        txtTelexText.Text = tmpText
        chkCopy.Value = 0
        chkPreView.Tag = ""
        txtTelexText.SetFocus
    End If
End Sub
Public Sub SetTelexDefault(CurTab As TABLib.Tab, CurType As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpType As String
    Dim IsFound As Boolean
    CurTab.OnVScrollTo 0
    MaxLine = CurTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpType = CurTab.GetColumnValue(CurLine, 0)
        If tmpType = CurType Then
            CurTab.OnVScrollTo CurLine
            CurTab.SetCurrentSelection CurLine
            IsFound = True
            Exit For
        End If
    Next
    If Not IsFound Then
        CurTab.InsertTextLine CurType & ",,,,,", False
        CurLine = CurTab.GetLineCount - 1
        CurTab.OnVScrollTo CurLine
        CurTab.SetCurrentSelection CurLine
    End If
    CurTab.Refresh
End Sub

Private Sub chkLcase_Click()
    If chkLcase.Value = 1 Then chkLcase.BackColor = LightGreen Else chkLcase.BackColor = vbButtonFace
End Sub

Private Sub chkMvtAddr_Click(Index As Integer)
    Dim tmpLineNo As Integer
    Dim RetVal As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    
    If chkMvtAddr(Index).Value = 1 Then
        chkMvtAddr(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'Airline
                SetFirstFreeCell AlcTab
                chkMvtAddr(Index).Value = 0
            Case 1  'Airport
                SetFirstFreeCell ApcTab
                chkMvtAddr(Index).Value = 0
            Case 2  'Clear
                AddressTab.ResetContent
                AddressTab.Refresh
                AlcTab.ResetContent
                AlcTab.InsertTextLine ",,,,,,", False
                AlcTab.Refresh
                ApcTab.ResetContent
                ApcTab.InsertTextLine ",,,,,,", False
                ApcTab.Refresh
                InitDblSignList ""
                StatusBar1.Panels(2).Text = ""
                chkMvtAddr(Index).Value = 0
            Case 3  ' Insert
                If OnTop.Value = 1 Then
                    OnTop.Value = 0
                End If
                If chkSelAddr(0).Tag = 1 Then
                    EditGroups.txtGrpName.Text = ""
                    EditGroups.txtGrpDesc.Text = ""
                    EditGroups.txtGrpAddr.Text = txtDestAddr.Text
                    tmpLineNo = TlxOrigTab.GetCurrentSelected
                    If tmpLineNo >= 0 Then
                        EditGroups.txtGrpOwner.Text = TlxOrigTab.GetColumnValue(tmpLineNo, 0)
                    Else
                        EditGroups.txtGrpOwner.Text = ""
                    End If
                    EditGroups.txtGrpName.Tag = "INS"
                    EditGroups.Visible = True
                Else
                    If chkSelAddr(0).Tag = 2 Then
                        EditTemplates.txtTplName.Text = ""
                        EditTemplates.txtTplDesc.Text = ""
                        EditTemplates.txtTplText.Text = ""
                        tmpLineNo = TlxOrigTab.GetCurrentSelected
                        If tmpLineNo >= 0 Then
                            EditTemplates.txtTplOwner.Text = TlxOrigTab.GetColumnValue(tmpLineNo, 0)
                        Else
                            EditTemplates.txtTplOwner.Text = ""
                        End If
                        EditTemplates.txtTplName.Tag = "INS"
                        EditTemplates.Visible = True
                    End If
                End If
                chkMvtAddr(Index).Value = 0
            Case 4  ' Update
                If OnTop.Value = 1 Then
                    OnTop.Value = 0
                End If
                If chkSelAddr(0).Tag = 1 Then
                    tmpLineNo = GroupTab.GetCurrentSelected
                    If tmpLineNo >= 0 Then
                        EditGroups.txtGrpName.Text = GroupTab.GetColumnValue(tmpLineNo, 0)
                        EditGroups.txtGrpDesc.Text = GroupTab.GetColumnValue(tmpLineNo, 1)
                        EditGroups.txtGrpAddr.Text = CleanString(GroupTab.GetColumnValue(tmpLineNo, 2), FOR_CLIENT, False)
                        EditGroups.txtGrpOwner.Text = GroupTab.GetColumnValue(tmpLineNo, 4)
                        EditGroups.txtGrpName.Tag = GroupTab.GetColumnValue(tmpLineNo, 3)
                        EditGroups.Visible = True
                    Else
                        MyMsgBox.CallAskUser 0, 0, 0, "Edit Groups", "No line sected", "hand", "", UserAnswer
                    End If
                Else
                    If chkSelAddr(0).Tag = 2 Then
                        tmpLineNo = TemplateTab.GetCurrentSelected
                        If tmpLineNo >= 0 Then
                            EditTemplates.txtTplName.Text = TemplateTab.GetColumnValue(tmpLineNo, 1)
                            EditTemplates.txtTplDesc.Text = TemplateTab.GetColumnValue(tmpLineNo, 2)
                            EditTemplates.txtTplText.Text = CleanString(TemplateTab.GetColumnValue(tmpLineNo, 3), FOR_CLIENT, False)
                            If Len(TemplateTab.GetColumnValue(tmpLineNo, 7)) > 0 Then
                                EditTemplates.txtTplText.Text = EditTemplates.txtTplText.Text & CleanString(TemplateTab.GetColumnValue(tmpLineNo, 7), FOR_CLIENT, False)
                            End If
                            EditTemplates.txtTplOwner.Text = TemplateTab.GetColumnValue(tmpLineNo, 8)
                            EditTemplates.txtTplName.Tag = TemplateTab.GetColumnValue(tmpLineNo, 4)
                            EditTemplates.txtTplType.Tag = TemplateTab.GetColumnValue(tmpLineNo, 6)
                            EditTemplates.Visible = True
                        Else
                            MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", "No line sected", "hand", "", UserAnswer
                        End If
                    End If
                End If
                chkMvtAddr(Index).Value = 0
            Case 5  ' Delete
                If chkSelAddr(0).Tag = 1 Then
                    tmpLineNo = GroupTab.GetCurrentSelected
                    If tmpLineNo >= 0 Then
                        If MyMsgBox.CallAskUser(0, 0, 0, "Edit Groups", "Do you really want to delete selected record ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                            If tmpLineNo = GroupTab.GetLineCount - 1 Then
                                If tmpLineNo <> 0 Then
                                    chkSelAddr(1).Tag = GroupTab.GetColumnValue(tmpLineNo - 1, 3)
                                Else
                                    chkSelAddr(1).Tag = ""
                                End If
                            Else
                                chkSelAddr(1).Tag = GroupTab.GetColumnValue(tmpLineNo + 1, 3)
                            End If
                            ActResult = ""
                            ActCmd = "DRT"
                            ActTable = "TTPTAB"
                            ActFldLst = ""
                            ActCondition = "WHERE URNO = " & GroupTab.GetColumnValue(tmpLineNo, 3)
                            ActOrder = ""
                            ActDatLst = ""
                            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            LoadGroupData
                        End If
                    Else
                        MyMsgBox.CallAskUser 0, 0, 0, "Edit Groups", "No line sected", "hand", "", UserAnswer
                    End If
                Else
                    If chkSelAddr(0).Tag = 2 Then
                        tmpLineNo = TemplateTab.GetCurrentSelected
                        If tmpLineNo >= 0 Then
                            If MyMsgBox.CallAskUser(0, 0, 0, "Edit Templates", "Do you really want to delete selected record ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
                                If tmpLineNo = TemplateTab.GetLineCount - 1 Then
                                    If tmpLineNo <> 0 Then
                                        chkSelAddr(2).Tag = TemplateTab.GetColumnValue(tmpLineNo - 1, 4)
                                    Else
                                        chkSelAddr(2).Tag = ""
                                    End If
                                Else
                                    chkSelAddr(2).Tag = TemplateTab.GetColumnValue(tmpLineNo + 1, 4)
                                End If
                                ActResult = ""
                                ActCmd = "DRT"
                                ActTable = "TTPTAB"
                                ActFldLst = ""
                                ActCondition = "WHERE URNO = " & TemplateTab.GetColumnValue(tmpLineNo, 4)
                                ActOrder = ""
                                ActDatLst = ""
                                RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                                LoadTemplateData
                            End If
                        Else
                            MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", "No line sected", "hand", "", UserAnswer
                        End If
                    End If
                End If
                chkMvtAddr(Index).Value = 0
            Case Else
        End Select
    Else
        chkMvtAddr(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub SetFirstFreeCell(CurTab As TABLib.Tab)
    Dim CurCol As Long
    On Error Resume Next
    'For CurCol = 0 To 5
        
    'Next
    CurTab.SetFocus
    CurCol = 0
    CurTab.SetInplaceEdit 0, CurCol, False
    CurTab.Tag = "0"
End Sub
Private Sub chkOutFormat_Click()
    If chkOutFormat.Value = 1 Then
        chkOutFormat.BackColor = LightGreen
        chkPreView.Value = 0
    Else
        chkOutFormat.BackColor = vbButtonFace
    End If
    chkPreView.Value = chkOutFormat.Value
End Sub

Private Sub chkPreView_Click()
    On Error Resume Next
    If chkPreView.Value = 1 Then
        chkPreView.BackColor = LightGreen
        InitTelexPreview False
        fraPreview.Visible = True
        fraTelexText.Visible = False
        chkTemplate(2).Visible = False
        txtTplName.Visible = False
        txtTplDesc.Visible = False
        txtPreview.SetFocus
    Else
        fraTelexText.Visible = True
        fraPreview.Visible = False
        If chkSelAddr(1).Value = 1 Or chkSelAddr(2).Value = 1 Then
            chkTemplate(2).Visible = True
            If chkTemplate(2).Enabled = True Then
                txtTplName.Visible = True
                txtTplDesc.Visible = True
            End If
        End If
        chkPreView.BackColor = vbButtonFace
    End If
End Sub

Private Sub InitTelexPreview(WithFooter As Boolean)
    Select Case MySitaOutType
        Case "SITA_NODE"
            InitTpfNodePreview WithFooter
        Case "SITA_FILE"
            If chkOutFormat.Value = 1 Then
                InitSitaFilePreview WithFooter
            Else
                InitTpfNodePreview WithFooter
            End If
        Case Else
    End Select
End Sub

Private Sub InitTpfNodePreview(WithFooter As Boolean)
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim NewText As String
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpUrno As String
    Dim tmpTtyp As String
    Dim tmpDateStamp As String
    Dim iCount As Integer
    txtPreview.Text = ""
    NewText = ""
    LineNo = TlxTypeTab.GetCurrentSelected
    tmpTtyp = TlxTypeTab.GetColumnValue(LineNo, 0)
    If tmpTtyp <> "SCOR" Then
        LineNo = TlxPrioTab.GetCurrentSelected
        tmpData = TlxPrioTab.GetColumnValue(LineNo, 0)
        NewText = NewText & tmpData & " "
        tmpLine = GetCleanAddrList(txtDestAddr.Text, 8, " ")
        NewText = NewText & tmpLine & vbNewLine
        
        LineNo = TlxOrigTab.GetCurrentSelected
        tmpData = TlxOrigTab.GetColumnValue(LineNo, 0)
        tmpLine = "." & tmpData & " "
        LineNo = DblSignTab.GetCurrentSelected
        tmpData = Trim(DblSignTab.GetColumnValue(LineNo, 0))
        tmpData = MySetUp.GetUtcServerTime
        tmpLine = tmpLine & Mid(tmpData, 7, 6) & " "
        tmpDateStamp = Mid(tmpData, 7, 6)
        tmpUrno = Trim(NewSendUrno.Text)
        If tmpUrno = "" Then
            UfisServer.UrnoPoolPrepare 1
            tmpUrno = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
            If tmpUrno = "" Then tmpUrno = UfisServer.UrnoPoolGetNext
        End If
        NewSendUrno.Text = tmpUrno
        'UfisServer.UrnoPoolPrepare 1
        'tmpUrno = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
        tmpData = DecodeSsimDayFormat(Left(tmpData, 8), "CEDA", "SSIM2")
        tmpDateStamp = tmpDateStamp & " " & Mid(tmpData, 3, 5)
        chkPreView.Tag = tmpDateStamp & " PPK " & Right(tmpUrno, 3)
        NewText = NewText & tmpLine & vbNewLine
    End If
    NewText = NewText & txtTelexText.Text & vbNewLine
    txtPreview.Text = NewText
End Sub

Private Sub InitSitaFilePreview(WithFooter As Boolean)
    Dim LineNo As Long
    Dim CurLine As Long
    Dim TabAddrList As String
    Dim NewText As String
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpUrno As String
    Dim tmpDateStamp As String
    Dim iCount As Integer
    Dim tmpTime As String
    
    txtPreview.Text = ""
    NewText = ""
    LineNo = TlxPrioTab.GetCurrentSelected
    tmpData = Trim(TlxPrioTab.GetColumnValue(LineNo, 0))
    If (tmpData <> "") And (tmpData <> "--") Then
        NewText = NewText & "=PRIORITY" & vbNewLine
        NewText = NewText & tmpData & " " & vbNewLine
    End If
    LineNo = DblSignTab.GetCurrentSelected
    tmpData = Trim(DblSignTab.GetColumnValue(LineNo, 0))
    If (tmpData <> "") And (tmpData <> "--") Then
        NewText = NewText & "=DBLSIG" & vbNewLine
        NewText = NewText & tmpData & " " & vbNewLine
    End If
    TabAddrList = GetCleanAddrList(txtDestAddr.Text, 8, ",")
    If TabAddrList <> "" Then
        NewText = NewText & "=DESTINATION TYPE B" & vbNewLine
        iCount = 1
        tmpData = GetItem(TabAddrList, iCount, ",")
        While tmpData <> ""
            tmpData = CleanTrimString(tmpData)
            If tmpData <> "" Then
                NewText = NewText & "STX," & tmpData & vbNewLine
            End If
            iCount = iCount + 1
            tmpData = GetItem(TabAddrList, iCount, ",")
        Wend
    End If
    If MySitaOutOrig Then
        CurLine = TlxOrigTab.GetCurrentSelected
        If CurLine >= 0 Then
            tmpData = Trim(TlxOrigTab.GetColumnValue(CurLine, 0))
            If tmpData <> "" Then
                NewText = NewText & "=ORIGIN" & vbNewLine
                NewText = NewText & tmpData & vbNewLine
            End If
        End If
    End If
    If MySitaOutMsgId Then
        tmpUrno = Trim(NewSendUrno.Text)
        If tmpUrno = "" Then
            UfisServer.UrnoPoolPrepare 1
            tmpUrno = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
            If tmpUrno = "" Then tmpUrno = UfisServer.UrnoPoolGetNext
        End If
        NewSendUrno.Text = tmpUrno
        NewText = NewText & "=MSGID" & vbNewLine
        tmpTime = MySetUp.GetUtcServerTime
        tmpTime = Mid(tmpTime, 7, 6)
        NewText = NewText & tmpTime & vbNewLine
        'NewText = NewText & "UF" & tmpUrno & "IS" & vbNewLine
    End If
    If MySitaOutSmi Then
        CurLine = TlxTypeTab.GetCurrentSelected
        If CurLine >= 0 Then
            tmpData = Trim(TlxTypeTab.GetColumnValue(CurLine, 0))
            If tmpData = "FREE" Then tmpData = ""
            If tmpData <> "" Then
                NewText = NewText & "=SMI" & vbNewLine
                NewText = NewText & tmpData & vbNewLine
            End If
        End If
    End If
    tmpData = Trim(txtTelexText.Text)
    If tmpData <> "" Then
        NewText = NewText & "=TEXT" & vbNewLine
        NewText = NewText & tmpData & vbNewLine
    End If
    txtPreview.Text = NewText
End Sub
Private Function GetCleanAddrList(CurAddrList As String, AddrPerLine As Long, UseSepa As String) As String
    Dim Result As String
    Dim TxtAddrList As String
    Dim tmpData As String
    Dim itm As Integer
    Dim cnt As Long
    TxtAddrList = CurAddrList
    TxtAddrList = Replace(TxtAddrList, Chr(10), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, Chr(13), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, Chr(9), ",", 1, -1, vbBinaryCompare)
    TxtAddrList = Replace(TxtAddrList, " ", ",", 1, -1, vbBinaryCompare)
    tmpData = TxtAddrList
    TxtAddrList = Replace(tmpData, ",,", ",", 1, -1, vbBinaryCompare)
    While tmpData <> TxtAddrList
        tmpData = TxtAddrList
        TxtAddrList = Replace(tmpData, ",,", ",", 1, -1, vbBinaryCompare)
    Wend
    While Left(TxtAddrList, 1) = ","
        TxtAddrList = Mid(TxtAddrList, 2)
    Wend
    While Right(TxtAddrList, 1) = ","
        TxtAddrList = Left(TxtAddrList, Len(TxtAddrList) - 1)
    Wend
    If AddrPerLine > 0 Then
        Result = ""
        cnt = 0
        itm = 1
        tmpData = GetItem(TxtAddrList, itm, ",")
        While tmpData <> ""
            Result = Result & tmpData & UseSepa
            cnt = cnt + 1
            If cnt = AddrPerLine Then
                Result = Result & vbNewLine
                cnt = 0
            End If
            itm = itm + 1
            tmpData = GetItem(TxtAddrList, itm, ",")
        Wend
    Else
        Result = TxtAddrList
        If UseSepa <> "," Then
            Result = Replace(Result, ",", UseSepa, 1, -1, vbBinaryCompare)
        End If
    End If
    GetCleanAddrList = Result
End Function

Private Function GetOutAddrList() As String
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim AddrCount As Long
    Dim tmpTead As String
    Dim TabAddrList As String
    AddressTab.SetInternalLineBuffer True
    TabAddrList = ""
    LineNo = Val(AddressTab.GetLinesByStatusValue(1, 0))
    If LineNo > 0 Then
        While LineNo >= 0
            LineNo = AddressTab.GetNextResultLine
            If LineNo >= 0 Then
                tmpTead = AddressTab.GetColumnValue(LineNo, 2)
                If InStr(TabAddrList, tmpTead) = 0 Then
                    TabAddrList = TabAddrList & tmpTead & ","
                End If
            End If
        Wend
    End If
    AddressTab.SetInternalLineBuffer False
    GetOutAddrList = TabAddrList
End Function

Private Sub chkReset_Click()
    If chkReset.Value = 1 Then
        txtTelexText.Text = ""
        txtTelexText.Tag = ""
        txtDestAddr.Text = ""
        AddressTab.ResetContent
        AddressTab.Refresh
        AlcTab.ResetContent
        AlcTab.InsertTextLine ",,,,,", False
        AlcTab.Refresh
        ApcTab.ResetContent
        ApcTab.InsertTextLine ",,,,,", False
        ApcTab.Refresh
        InitDblSignList ""
        StatusBar1.Panels(2).Text = ""
        chkPreView.Tag = ""
        SetTelexDefault TlxTypeTab, "FREE"
        SetTelexDefault TlxPrioTab, MySitaDefPrio
        Me.CurrentRecord.Text = ""
        Me.NewCallCode.Text = ""
        fraFlightData.Visible = False
        Form_Resize
        chkPreView.Value = 0
        chkOutFormat.Value = 0
        chkReset.Value = 0
    End If
End Sub

Private Sub chkTplSel_Click(Index As Integer)
    Dim tmpTag As String
    Dim LastIndex As Integer
    
    If chkTplSel(Index).Value = 1 Then
        tmpTag = chkTplSel(0).Tag
        If tmpTag <> "" Then
            LastIndex = Val(tmpTag)
            If LastIndex <> Index Then chkTplSel(LastIndex).Value = 0
        End If
        chkTplSel(Index).BackColor = LightGreen
        chkTplSel(0).Tag = CStr(Index)
        If chkSelAddr(2).Value = 1 Then
            chkSelAddr(2).Value = 0
            chkSelAddr(2).Value = 1
        End If
        If Index = 0 Then
            chkMvtAddr(3).Enabled = True
            chkMvtAddr(4).Enabled = True
            chkTemplate(0).Enabled = True
            chkTemplate(1).Enabled = True
            txtDestAddr.Enabled = True
        Else
            chkMvtAddr(3).Enabled = False
            chkMvtAddr(4).Enabled = False
            If Index = 1 Then
                chkTemplate(0).Enabled = True
                chkTemplate(1).Enabled = False
                txtDestAddr.Enabled = True
            Else
                chkTemplate(0).Enabled = False
                chkTemplate(1).Enabled = True
                txtDestAddr.Enabled = False
            End If
        End If
    Else
        chkTplSel(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkSelAddr_Click(Index As Integer)
    Dim tmpTag As String
    Dim LastIndex As Integer
    If chkSelAddr(Index).Value = 1 Then
        fraDestAddr.Caption = "Destination Addresses"
        tmpTag = chkSelAddr(0).Tag
        If tmpTag <> "" Then
            LastIndex = Val(tmpTag)
            If LastIndex <> Index Then chkSelAddr(LastIndex).Value = 0
        End If
        chkSelAddr(Index).BackColor = LightGreen
        chkSelAddr(0).Tag = CStr(Index)
        Select Case Index
            Case 0  ' MVT Telexes
                fraChkAddr.Visible = True
                chkMvtAddr(0).Visible = True
                chkMvtAddr(1).Visible = True
                chkMvtAddr(2).Visible = True
                chkMvtAddr(3).Visible = False
                chkMvtAddr(4).Visible = False
                chkMvtAddr(5).Visible = False
                AlcTab.Visible = True
                ApcTab.Visible = True
                AddressTab.Visible = True
                GroupTab.Visible = False
                TemplateTab.Visible = False
                chkTlxAddr(0).Visible = True
                chkTlxAddr(1).Visible = True
                chkTlxAddr(2).Visible = True
                chkTemplate(0).Visible = False
                chkTemplate(1).Visible = False
                chkTemplate(2).Visible = False
                txtTplName.Visible = False
                txtTplDesc.Visible = False
                txtDestAddr.Enabled = True
            Case 1  ' Groups
                chkSelAddr(1).Tag = ""
                StatusBar1.Panels(2).Text = ""
                LoadGroupData
                fraChkAddr.Visible = True
                chkMvtAddr(0).Visible = False
                chkMvtAddr(1).Visible = False
                chkMvtAddr(2).Visible = False
                chkMvtAddr(3).Visible = True
                chkMvtAddr(4).Visible = True
                chkMvtAddr(5).Visible = True
                chkMvtAddr(3).Enabled = True
                chkMvtAddr(4).Enabled = True
                AlcTab.Visible = False
                ApcTab.Visible = False
                AddressTab.Visible = False
                GroupTab.Visible = True
                TemplateTab.Visible = False
                chkTlxAddr(0).Visible = False
                chkTlxAddr(1).Visible = False
                chkTlxAddr(2).Visible = False
                chkTemplate(0).Enabled = False
                chkTemplate(0).Visible = True
                chkTemplate(0).Left = 90
                chkTemplate(1).Enabled = False
                chkTemplate(1).Visible = True
                chkTemplate(1).Left = 990
                chkTemplate(1).Top = 240
                chkTemplate(2).Enabled = False
                chkTemplate(2).Visible = True
                chkTemplate(2).Left = 5850
                chkTemplate(2).Top = 2010
                txtTplName.Visible = False
                txtTplDesc.Visible = False
                txtDestAddr.Enabled = True
            Case 2  ' Templates
                StatusBar1.Panels(2).Text = ""
                LoadGroupData
                If chkTplSel(0).Value = 0 And chkTplSel(1).Value = 0 And _
                   chkTplSel(2).Value = 0 Then
                    chkTplSel(0).Value = 1
                End If
                LoadTemplateData
                fraChkAddr.Visible = True
                chkMvtAddr(0).Visible = False
                chkMvtAddr(1).Visible = False
                chkMvtAddr(2).Visible = False
                chkMvtAddr(3).Visible = True
                chkMvtAddr(4).Visible = True
                chkMvtAddr(5).Visible = True
                If chkTplSel(0).Value = 1 Then
                    chkMvtAddr(3).Enabled = True
                    chkMvtAddr(4).Enabled = True
                Else
                    chkMvtAddr(3).Enabled = False
                    chkMvtAddr(4).Enabled = False
                End If
                AlcTab.Visible = False
                ApcTab.Visible = False
                AddressTab.Visible = False
                GroupTab.Visible = False
                TemplateTab.Visible = True
                chkTlxAddr(0).Visible = False
                chkTlxAddr(1).Visible = False
                chkTlxAddr(2).Visible = False
                If Len(txtDestAddr.Text) > 0 Then
                    If chkTplSel(0).Value = 1 Or chkTplSel(1).Value = 1 Then
                        chkTemplate(0).Enabled = True
                    Else
                        chkTemplate(0).Enabled = False
                    End If
                    If chkTplSel(0).Value = 1 Or chkTplSel(2).Value = 1 Then
                        chkTemplate(1).Enabled = True
                    Else
                        chkTemplate(1).Enabled = False
                    End If
                    chkTemplate(2).Enabled = True
                Else
                    chkTemplate(0).Enabled = False
                    chkTemplate(1).Enabled = False
                    chkTemplate(2).Enabled = False
                End If
                chkTemplate(0).Visible = True
                chkTemplate(0).Left = 90
                chkTemplate(1).Visible = True
                chkTemplate(1).Left = 990
                chkTemplate(1).Top = 240
                chkTemplate(2).Visible = True
                chkTemplate(2).Left = 5850
                chkTemplate(2).Top = 2010
                If Len(txtDestAddr.Text) > 0 Then
                    txtTplName.Visible = True
                    txtTplDesc.Visible = True
                    If chkTplSel(0).Value = 1 Then
                        txtTplName.Text = ""
                        txtTplDesc.Text = ""
                    End If
                Else
                    txtTplName.Visible = False
                    txtTplDesc.Visible = False
                End If
                txtTplName.Left = 6800
                txtTplName.Top = 2010
                txtTplDesc.Left = 9200
                txtTplDesc.Top = 2010
                If chkTplSel(2).Value = 1 Then
                    txtDestAddr.Enabled = False
                Else
                    txtDestAddr.Enabled = True
                End If
            Case Else
        End Select
    Else
        chkSelAddr(Index).BackColor = vbButtonFace
'        Select Case Index
'            Case 0
                fraChkAddr.Visible = False
                AlcTab.Visible = False
                ApcTab.Visible = False
                AddressTab.Visible = False
                GroupTab.Visible = False
                TemplateTab.Visible = False
'            Case Else
'        End Select
    End If
End Sub

Private Sub chkTest_Click()
    If chkTest.Value = 1 Then ShowChildForm Me, chkTest, TestCases, ""
End Sub

Private Sub chkTlxAddr_Click(Index As Integer)
    If chkTlxAddr(Index).Value = 1 Then
        chkTlxAddr(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                chkTlxAddr(2).Enabled = False
            Case 1
                txtDestAddr.Text = GetCleanAddrList(txtDestAddr.Text, 8, " ")
                chkTlxAddr(Index).Value = 0
            Case 2
                FillAddrField
                chkTlxAddr(Index).Value = 0
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                chkTlxAddr(2).Enabled = True
            Case Else
        End Select
        chkTlxAddr(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTransmit_Click()
    Dim TelexText As String
    Dim ErrMsg As String
    Dim tmpResult As String
    Dim retCod As Integer
    Dim outData As String
    Dim outFields As String
    Dim tmpSelKey As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpTime As String
    Dim tmpTtyp As String
    Dim tmpData As String
    Dim SendIt As Boolean
    Dim LineNo As Long
    If chkTransmit.Value = 1 Then
        chkTransmit.BackColor = LightGreen
        chkSend.Value = 0
        chkOutFormat.Value = 0
        chkPreView.Value = 1
        SendIt = True
        ErrMsg = ""
        TelexText = Trim(txtPreview.Text)
        LineNo = TlxTypeTab.GetCurrentSelected
        tmpTtyp = TlxTypeTab.GetColumnValue(LineNo, 0)
        If tmpTtyp <> "SCOR" Then
            If InStr(TelexText, "@NNNN") = 0 Then
                tmpData = chkPreView.Tag
                If tmpData <> "" Then
                    tmpData = Replace(tmpData, "PPK", "TRM", 1, -1, vbBinaryCompare)
                    TelexText = TelexText & tmpData & vbNewLine
                End If
                TelexText = TelexText & "@NNNN" & vbNewLine
                txtPreview.Text = TelexText
            End If
            tmpSelKey = "TELEX,5"
        Else
            tmpSelKey = "TELEX,11"
        End If
        If SendIt Then
            retCod = UfisServer.CallCeda(tmpResult, "TLX", "TLXTAB", "TEXT", TelexText, tmpSelKey, "", 0, True, False)
            MyMsgBox.CallAskUser 0, 0, 0, "Transmit Telex", "The telex has been transmitted to the server.", "export", "", UserAnswer
        Else
            If TelexText = "" Then ErrMsg = "Can't send empty telexes!"
            MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", ErrMsg, "stop", "", UserAnswer
        End If
        chkTransmit.Value = 0
    Else
        chkPreView.Value = 0
        chkTransmit.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkUcase_Click()
    If chkUcase.Value = 1 Then
        chkUcase.BackColor = LightGreen
        txtPreview.Tag = txtPreview.Text
        txtPreview.Text = UCase(txtPreview.Text)
    Else
        txtPreview.Text = txtPreview.Tag
        chkUcase.BackColor = vbButtonFace
    End If
End Sub

Private Sub Form_Activate()
    If Not MeIsVisible Then
        TransmitEnabled = chkTransmit.Enabled
        'SetTelexDefault TlxPrioTab, MySitaDefPrio
        If TestCaseMode Then
            chkTest.Value = 0
            chkTest.Value = 1
        End If
        MeIsVisible = True
    End If
End Sub

Private Sub Form_Load()
    Dim tmpData As String
    MeIsVisible = False
    tmpData = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "ALLOW_TRANSMIT", "NO")
    If tmpData = "NO" Then
        'WAS CALLED TOO EARLY
        'MySetUp (LOADING THIS FORM)
        chkTransmit.Enabled = False
    End If
    Select Case MySitaOutType
        Case "SITA_NODE"
            chkOutFormat.Enabled = False
        Case "SITA_FILE"
            chkOutFormat.Enabled = True
        Case Else
    End Select
    fraDestAddr.Visible = True
    fraPreview.Top = fraAddress.Top
    fraPreview.Left = fraTelexText.Left
    fraFlightData.Top = fraPreview.Top
    fraTelexText.ZOrder
    fraFlightData.ZOrder
    fraOrigAddr.ZOrder
    fraTlxPrio.ZOrder
    fraTlxType.ZOrder
    fraDblSign.ZOrder
    StatusBar1.ZOrder
    
    If TelexTemplates = False Then
        chkSelAddr(1).Visible = False
        chkSelAddr(2).Visible = False
        chkTplSel(0).Visible = False
        chkTplSel(1).Visible = False
        chkTplSel(2).Visible = False
    End If
    
    AddressTab.ResetContent
    AddressTab.FontName = "Courier New"
    AddressTab.HeaderFontSize = MyFontSize
    AddressTab.FontSize = MyFontSize
    AddressTab.lineHeight = MyFontSize
    AddressTab.SetTabFontBold MyFontBold
    AddressTab.HeaderString = "ALC,APC,Address,Remark                      ,DS,URNO"
    AddressTab.HeaderLengthString = "100,100,100,100,100,100"
    AddressTab.LogicalFieldList = "ALC3,APC3,TEAD,BEME,PAYE,URNO"
    AddressTab.ColumnWidthString = "3,3,7,100,10,10"
    AddressTab.ColumnAlignmentString = "L,L,L,L,L,L"
    AddressTab.CreateDecorationObject "SelMarker", "L,R,T,B", "2,2,2,2", Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack)
    AddressTab.ShowHorzScroller True
    AddressTab.LifeStyle = True
    AddressTab.AutoSizeByHeader = True
    AddressTab.AutoSizeColumns
    
    AlcTab.ResetContent
    AlcTab.FontName = "Courier New"
    AlcTab.HeaderFontSize = MyFontSize
    AlcTab.FontSize = MyFontSize
    AlcTab.lineHeight = MyFontSize
    AlcTab.SetTabFontBold MyFontBold
    AlcTab.HeaderString = "-ALC-,-ALC-,-ALC-,-ALC-,-ALC-,-ALC-, "
    AlcTab.HeaderLengthString = "100,100,100,100,100,100,100"
    AlcTab.ColumnAlignmentString = "C,C,C,C,C,C,C"
    AlcTab.HeaderAlignmentString = "C,C,C,C,C,C,C"
    AlcTab.ShowVertScroller False
    AlcTab.ShowRowSelection = False
    AlcTab.InsertTextLine ",,,,,,", False
    AlcTab.EnableInlineEdit True
    AlcTab.LifeStyle = True
    AlcTab.AutoSizeByHeader = True
    AlcTab.AutoSizeColumns
    
    ApcTab.ResetContent
    ApcTab.FontName = "Courier New"
    ApcTab.HeaderFontSize = MyFontSize
    ApcTab.FontSize = MyFontSize
    ApcTab.lineHeight = MyFontSize
    ApcTab.SetTabFontBold MyFontBold
    ApcTab.HeaderString = "-APC-,-APC-,-APC-,-APC-,-APC-,-APC-, "
    ApcTab.HeaderLengthString = "100,100,100,100,100,100,100"
    ApcTab.ColumnAlignmentString = "C,C,C,C,C,C,C"
    ApcTab.HeaderAlignmentString = "C,C,C,C,C,C,C"
    ApcTab.ShowVertScroller False
    ApcTab.ShowRowSelection = False
    ApcTab.InsertTextLine ",,,,,,", False
    ApcTab.EnableInlineEdit True
    ApcTab.AutoSizeByHeader = True
    ApcTab.LifeStyle = True
    ApcTab.AutoSizeColumns
    ApcTab.Top = AlcTab.Top + 510
    
    AddressTab.Top = ApcTab.Top + 510
    
    TlxTypeTab.ResetContent
    TlxTypeTab.FontName = "Courier New"
    TlxTypeTab.HeaderFontSize = MyFontSize
    TlxTypeTab.FontSize = MyFontSize
    TlxTypeTab.lineHeight = MyFontSize
    TlxTypeTab.SetTabFontBold MyFontBold
    TlxTypeTab.HeaderString = "Type,ST,Message Category               "
    TlxTypeTab.HeaderLengthString = "100,100,100"
    TlxTypeTab.LifeStyle = True
    TlxTypeTab.AutoSizeByHeader = True
    TlxTypeTab.AutoSizeColumns
    
    TlxOrigTab.ResetContent
    TlxOrigTab.FontName = "Courier New"
    TlxOrigTab.HeaderFontSize = MyFontSize
    TlxOrigTab.FontSize = MyFontSize
    TlxOrigTab.lineHeight = MyFontSize
    TlxOrigTab.SetTabFontBold MyFontBold
    TlxOrigTab.HeaderString = "Address,Remark                      "
    TlxOrigTab.HeaderLengthString = "100,100"
    TlxOrigTab.LifeStyle = True
    TlxOrigTab.AutoSizeByHeader = True
    TlxOrigTab.AutoSizeColumns
    
    TlxPrioTab.ResetContent
    TlxPrioTab.FontName = "Courier New"
    TlxPrioTab.HeaderFontSize = MyFontSize
    TlxPrioTab.FontSize = MyFontSize
    TlxPrioTab.lineHeight = MyFontSize
    TlxPrioTab.SetTabFontBold MyFontBold
    TlxPrioTab.HeaderString = "Prio,Remark                         "
    TlxPrioTab.HeaderLengthString = "100,100"
    TlxPrioTab.LifeStyle = True
    TlxPrioTab.AutoSizeByHeader = True
    TlxPrioTab.AutoSizeColumns
    
    DblSignTab.ResetContent
    DblSignTab.FontName = "Courier New"
    DblSignTab.HeaderFontSize = MyFontSize
    DblSignTab.FontSize = MyFontSize
    DblSignTab.lineHeight = MyFontSize
    DblSignTab.SetTabFontBold MyFontBold
    DblSignTab.HeaderString = "LC,Remark                           "
    DblSignTab.HeaderLengthString = "100,100"
    DblSignTab.LifeStyle = True
    DblSignTab.AutoSizeByHeader = True
    DblSignTab.AutoSizeColumns
    
    GroupTab.ResetContent
    GroupTab.FontName = "Courier New"
    GroupTab.HeaderFontSize = MyFontSize
    GroupTab.FontSize = MyFontSize
    GroupTab.lineHeight = MyFontSize
    GroupTab.SetTabFontBold MyFontBold
    GroupTab.HeaderString = "Group Name  ,Description                       "
    GroupTab.HeaderLengthString = "100,100"
    GroupTab.ColumnWidthString = "16,32"
    GroupTab.ColumnAlignmentString = "L,L"
    GroupTab.HeaderAlignmentString = "C,C"
    GroupTab.ShowVertScroller True
    GroupTab.ShowHorzScroller True
    'GroupTab.ShowRowSelection = False
    GroupTab.InsertTextLine ",", False
    GroupTab.EnableInlineEdit False
    GroupTab.AutoSizeByHeader = True
    GroupTab.LifeStyle = True
    GroupTab.AutoSizeColumns
    GroupTab.Top = 870
    GroupTab.Height = 4615
    
    TemplateTab.ResetContent
    TemplateTab.FontName = "Courier New"
    TemplateTab.HeaderFontSize = MyFontSize
    TemplateTab.FontSize = MyFontSize
    TemplateTab.lineHeight = MyFontSize
    TemplateTab.SetTabFontBold MyFontBold
    TemplateTab.HeaderString = "T,Template    ,Description                       "
    TemplateTab.HeaderLengthString = "100,100,100"
    TemplateTab.ColumnWidthString = "1,16,32"
    TemplateTab.ColumnAlignmentString = "L,L,L"
    TemplateTab.HeaderAlignmentString = "C,C,C"
    TemplateTab.ShowVertScroller True
    TemplateTab.ShowHorzScroller True
    'TemplateTab.ShowRowSelection = False
    TemplateTab.InsertTextLine ",", False
    TemplateTab.EnableInlineEdit False
    TemplateTab.AutoSizeByHeader = True
    TemplateTab.LifeStyle = True
    TemplateTab.AutoSizeColumns
    TemplateTab.Top = 870
    TemplateTab.Height = 4615
    
    InitTlxAddresses
    InitOrigList
    InitTlxTypeList
    InitPrioList
    InitDblSignList ""
    txtDestAddr.FontBold = MyFontBold
    txtDestAddr.FontSize = MyFontSize - 5
    txtTelexText.FontBold = MyFontBold
    txtTelexText.FontSize = MyFontSize - 5
    txtPreview.FontBold = MyFontBold
    txtPreview.FontSize = MyFontSize - 5
    chkSelAddr(0).Value = 1
    
    CreateFlightDataPanel ""
    
    If TestCaseMode Then fraTestCase.Visible = True

End Sub

Private Sub InitTlxAddresses()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpAlc As String
    Dim tmpApc As String
    Screen.MousePointer = 11
    UfisServer.BasicDataInit 0, 1, "MVTTAB", "TEMA,ALC3,APC3,TEAD,PAYE,BEME,URNO", ""
    UfisServer.BasicData(0).ColumnWidthString = "6,3,3,7,3,40,10"
    MaxLine = UfisServer.BasicData(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAlc = UfisServer.BasicData(0).GetColumnValue(CurLine, 1)
        tmpAlc = Left(tmpAlc & "   ", 3)
        tmpApc = UfisServer.BasicData(0).GetColumnValue(CurLine, 2)
        tmpApc = Left(tmpApc & "   ", 3)
        UfisServer.BasicData(0).SetColumnValue CurLine, 0, tmpAlc & tmpApc
    Next
    UfisServer.BasicData(0).IndexCreate "ALCAPC", 0
    UfisServer.BasicData(0).AutoSizeColumns
    Screen.MousePointer = 0
End Sub

Private Sub InitOrigList()
    Dim OrigList As String
    Dim CurAddr As String
    Dim CurBeme As String
    Dim ItemNo As Long
    Dim LineNo As Long
    Dim LineList As String
    Dim NewLine As String
    OrigList = MySetUp.HomeTlxAddr
    ItemNo = 0
    CurAddr = GetRealItem(OrigList, ItemNo, ",")
    While CurAddr <> ""
        CurBeme = ""
        LineList = UfisServer.BasicData(0).GetLinesByColumnValue(3, CurAddr, 0)
        If LineList <> "" Then
            LineNo = Val(LineList)
            CurBeme = UfisServer.BasicData(0).GetColumnValue(LineNo, 5)
        End If
        NewLine = CurAddr & "," & CurBeme
        TlxOrigTab.InsertTextLine NewLine, False
        ItemNo = ItemNo + 1
        CurAddr = GetRealItem(OrigList, ItemNo, ",")
    Wend
    TlxOrigTab.AutoSizeColumns
    TlxOrigTab.Refresh
    TlxOrigTab.SetCurrentSelection 0
End Sub
Private Sub InitTlxTypeList()
    Dim i As Integer
    Dim NewLine As String
    Dim CurList As String
    Dim CurType As String
    Dim CurAddi As String
    Dim itm As Integer
    NewLine = "FREE,,Free Text"
    TlxTypeTab.InsertTextLine NewLine, False
    For i = 0 To LastFolderIndex
        CurList = TelexPoolHead.fraFolder(i).Tag
        itm = 1
        CurType = GetItem(CurList, itm, ",")
        While CurType <> ""
            If CurType <> "FREE" Then
                CurAddi = ""
                NewLine = CurType & ",," & CurAddi
                TlxTypeTab.InsertTextLine NewLine, False
            End If
            itm = itm + 1
            CurType = GetItem(CurList, itm, ",")
        Wend
    Next
    TlxTypeTab.AutoSizeColumns
    'TlxTypeTab.Sort "0", True, True
    TlxTypeTab.Refresh
    TlxTypeTab.SetCurrentSelection 0
End Sub
Private Sub InitPrioList()
    TlxPrioTab.InsertTextLine "QU,Queue Urgent", False
    TlxPrioTab.InsertTextLine "QD,Queue Deferred", False
    TlxPrioTab.InsertTextLine "QX,Queue Express", False
    TlxPrioTab.InsertTextLine "QK,", False
    TlxPrioTab.InsertTextLine "QL,", False
    TlxPrioTab.InsertTextLine "QS,Emergency", False
    TlxPrioTab.AutoSizeColumns
    TlxPrioTab.Refresh
    TlxPrioTab.SetCurrentSelection 0
End Sub
Private Sub InitDblSignList(CodeList As String)
    Dim CurItm As Integer
    Dim tmpCode As String
    Dim tmpLine As String
    DblSignTab.ResetContent
    DblSignTab.SetUniqueFields "0"
    DblSignTab.InsertTextLine "--,None", False
    CurItm = 1
    tmpCode = GetItem(CodeList, CurItm, ",")
    While tmpCode <> ""
        tmpLine = tmpCode & ","
        DblSignTab.InsertTextLine tmpLine, False
        CurItm = CurItm + 1
        tmpCode = GetItem(CodeList, CurItm, ",")
    Wend
    DblSignTab.AutoSizeColumns
    DblSignTab.Refresh
    If CodeList <> "" Then
        DblSignTab.SetCurrentSelection 1
    Else
        DblSignTab.SetCurrentSelection 0
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Unload TestCases
        Unload EditGroups
        Unload EditTemplates
    End If
End Sub

Private Sub Form_Resize()
    Dim NewValue As Long
    If fraFlightData.Visible Then
        NewValue = Me.ScaleHeight - fraFlightData.Top - StatusBar1.Height '- 60
        If NewValue > 1750 Then
            fraFlightData.Height = NewValue
            FltInputPanel.Height = NewValue - FltInputPanel.Top - 90
            DrawBackGround FltInputPanel, MyLifeStyleValue, False, True
        End If
        NewValue = Me.ScaleWidth - fraFlightData.Width - 60
        If NewValue >= fraDestAddr.Left Then fraFlightData.Left = NewValue
    End If
    NewValue = Me.ScaleWidth - fraDestAddr.Left - 60
    If fraFlightData.Visible Then NewValue = NewValue - fraFlightData.Width - 30
    If NewValue > 1500 Then
        fraTelexText.Width = NewValue
        fraPreview.Width = NewValue
        fraDestAddr.Width = NewValue
        NewValue = NewValue - (txtDestAddr.Left * 2)
        txtTelexText.Width = NewValue
        txtDestAddr.Width = NewValue
        txtPreview.Width = NewValue
    End If
    NewValue = Me.ScaleHeight - fraPreview.Top - StatusBar1.Height '- 60
    If NewValue > 1750 Then
        fraPreview.Height = NewValue
        txtPreview.Height = NewValue - txtPreview.Top - 90
    End If
    NewValue = Me.ScaleHeight - fraOrigAddr.Top - StatusBar1.Height '- 60
    If NewValue > 1200 Then
        If fraTlxPrio.Tag <> "" Then
            fraTlxPrio.Height = NewValue
            TlxPrioTab.Height = NewValue - TlxPrioTab.Top - 90
        End If
        If fraOrigAddr.Tag <> "" Then
            fraOrigAddr.Height = NewValue
            TlxOrigTab.Height = NewValue - TlxOrigTab.Top - 90
        End If
        If fraTlxType.Tag <> "" Then
            fraTlxType.Height = NewValue
            TlxTypeTab.Height = NewValue - TlxTypeTab.Top - 90
        End If
        If fraDblSign.Tag <> "" Then
            fraDblSign.Height = NewValue
            DblSignTab.Height = NewValue - DblSignTab.Top - 90
        End If
    End If
    NewValue = Me.ScaleHeight - fraTelexText.Top - StatusBar1.Height '- 60
    If NewValue > 900 Then
        fraTelexText.Height = NewValue
        txtTelexText.Height = NewValue - txtTelexText.Top - 90
    End If
    NewValue = Me.ScaleHeight - fraAddress.Top - StatusBar1.Height '- 60
    If NewValue > 2700 Then
        fraAddress.Height = NewValue
        AddressTab.Height = NewValue - AddressTab.Top - 90
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'If TestCaseMode Then Unload TestCases
End Sub

Private Sub fraDblSign_DblClick()
    Dim tmpTag As String
    tmpTag = fraDblSign.Tag
    If tmpTag = "" Then
        fraDblSign.Tag = CStr(fraDblSign.Width) & "," & CStr(fraDblSign.Height)
        fraDblSign.Caption = "Double Sign [-]"
        Form_Resize
    Else
        fraDblSign.Width = Val(GetItem(tmpTag, 1, ","))
        fraDblSign.Height = Val(GetItem(tmpTag, 2, ","))
        DblSignTab.Height = fraDblSign.Height - DblSignTab.Top - 90
        fraDblSign.Caption = "Double Sign [+]"
        fraDblSign.Tag = ""
        ArrangeTabCursor DblSignTab
    End If
End Sub

Private Sub fraOrigAddr_DblClick()
    Dim tmpTag As String
    tmpTag = fraOrigAddr.Tag
    If tmpTag = "" Then
        fraOrigAddr.Tag = CStr(fraOrigAddr.Width) & "," & CStr(fraOrigAddr.Height)
        fraOrigAddr.Caption = "Telex Originator [-]"
        Form_Resize
    Else
        fraOrigAddr.Width = Val(GetItem(tmpTag, 1, ","))
        fraOrigAddr.Height = Val(GetItem(tmpTag, 2, ","))
        TlxOrigTab.Height = fraOrigAddr.Height - TlxOrigTab.Top - 90
        fraOrigAddr.Caption = "Telex Originator [+]"
        fraOrigAddr.Tag = ""
        ArrangeTabCursor TlxOrigTab
    End If
End Sub

Private Sub fraTlxPrio_DblClick()
    Dim tmpTag As String
    tmpTag = fraTlxPrio.Tag
    If tmpTag = "" Then
        fraTlxPrio.Tag = CStr(fraTlxPrio.Width) & "," & CStr(fraTlxPrio.Height)
        fraTlxPrio.Caption = "Telex Priority [-]"
        Form_Resize
    Else
        fraTlxPrio.Width = Val(GetItem(tmpTag, 1, ","))
        fraTlxPrio.Height = Val(GetItem(tmpTag, 2, ","))
        TlxPrioTab.Height = fraTlxPrio.Height - TlxPrioTab.Top - 90
        fraTlxPrio.Caption = "Telex Priority [+]"
        fraTlxPrio.Tag = ""
        ArrangeTabCursor TlxPrioTab
    End If
End Sub

Private Sub fraTlxType_DblClick()
    Dim tmpTag As String
    tmpTag = fraTlxType.Tag
    If tmpTag = "" Then
        fraTlxType.Tag = CStr(fraTlxType.Width) & "," & CStr(fraTlxType.Height)
        fraTlxType.Caption = "Telex Type [-]"
        Form_Resize
    Else
        fraTlxType.Width = Val(GetItem(tmpTag, 1, ","))
        fraTlxType.Height = Val(GetItem(tmpTag, 2, ","))
        TlxTypeTab.Height = fraTlxType.Height - TlxTypeTab.Top - 90
        fraTlxType.Caption = "Telex Type [+]"
        fraTlxType.Tag = ""
        ArrangeTabCursor TlxTypeTab
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False, False
    SetAllFormsOnTop True
End Sub

Private Sub TlxOrigTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)

    If chkSelAddr(1).Visible = True And chkSelAddr(1).Value = 1 Then
        LoadGroupData
    End If
    If chkSelAddr(2).Visible = True And chkSelAddr(2).Value = 1 Then
        LoadTemplateData
    End If
End Sub

Private Sub txtDestAddr_Change()
    Dim tmpText As String
    Dim tmpAddr As String
    Dim tmpData As String
    Dim curPos As Long
    Dim curLen As Long
    Dim txtLen As Long
    Dim itm As Integer
    Dim setRed As Boolean
    tmpText = txtDestAddr.Text
    'curPos = txtDestAddr.SelStart
    'curLen = txtDestAddr.SelLength
    tmpText = Trim(tmpText)
    tmpText = Replace(tmpText, vbCr, " ", 1, -1, vbBinaryCompare)
    tmpText = Replace(tmpText, vbLf, " ", 1, -1, vbBinaryCompare)
    tmpData = tmpText
    tmpText = Replace(tmpText, " ", " ", 1, -1, vbBinaryCompare)
    While tmpText <> tmpData
        tmpData = tmpText
        tmpText = Replace(tmpText, " ", " ", 1, -1, vbBinaryCompare)
    Wend
    setRed = False
    tmpAddr = "START"
    While tmpAddr <> ""
        tmpAddr = GetItem(tmpText, 1, " ")
        txtLen = Len(tmpAddr)
        If tmpAddr <> "" Then
            If (txtLen <> 7) Or (setRed) Then
                txtDestAddr.ForeColor = vbRed
                setRed = True
            Else
                txtDestAddr.ForeColor = vbBlack
            End If
            itm = InStr(tmpText, " ")
            If itm > 0 Then
                tmpText = Mid(tmpText, itm)
                tmpText = LTrim(tmpText)
            Else
                tmpText = ""
            End If
        End If
    Wend
    If setRed Then chkSend.Enabled = False Else chkSend.Enabled = True
    If (Not setRed) And (TransmitEnabled) Then chkTransmit.Enabled = True Else chkTransmit.Enabled = False
End Sub

Private Sub txtPreview_KeyPress(KeyAscii As Integer)
    If chkLcase.Value = 0 Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtDestAddr_KeyPress(KeyAscii As Integer)
    If chkLcase.Value = 0 Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTelexText_KeyPress(KeyAscii As Integer)
    If chkLcase.Value = 0 Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub chkSend_Click()
    Dim TelexText As String
    Dim ErrMsg As String
    Dim tmpResult As String
    Dim retCod As Integer
    Dim outData As String
    Dim outFields As String
    Dim outSqlKey As String
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpTime As String
    Dim tmpTtyp As String
    Dim tmpFlnu As String
    Dim tmpUrno As String
    Dim tmpData As String
    Dim tmpCallCode As String
    Dim TlxRec As String
    Dim tmpSere As String
    Dim LineNo As Long
    Dim SendIt As Boolean
    Dim NewUrno As String
    Dim i As Integer
    
    If chkSend.Value = 1 Then
        chkSend.BackColor = LightGreen
        chkTransmit.Value = 0
        If MySitaOutType = "SITA_FILE" Then
            chkOutFormat.Value = 1
        Else
            chkPreView.Value = 1
        End If
        SendIt = True
        ErrMsg = ""
        If MySitaOutType = "SITA_NODE" Then
            TelexText = Chr(13) & Chr(10) & Chr(1) & Trim(txtPreview.Text) & Chr(13) & Chr(10) & Chr(3)
        Else
            TelexText = Trim(txtPreview.Text)
        End If
        If MySitaOutType = "SITA_FILE" Then
            If InStr(TelexText, "=TEXT") = 0 Then
                ErrMsg = ErrMsg & "Missing keyword: '=TEXT'" & vbNewLine
                SendIt = False
            End If
            If InStr(TelexText, "=DESTINATION TYPE B") = 0 Then
                ErrMsg = ErrMsg & "Missing keyword: '=DESTINATION TYPE B'" & vbNewLine
                SendIt = False
            End If
        End If
        If SendIt Then
            TelexText = CleanString(TelexText, CLIENT_TO_SERVER, False)
'            If Len(TelexText) > 2000 Then
'                tmpTxt1 = Left(TelexText, 2000)
'                tmpTxt2 = Mid(TelexText, 2001, 2000)
'            Else
'                tmpTxt1 = TelexText
'                tmpTxt2 = " "
'            End If
            If Len(TelexText) <= TlxTxtFldSize Then
                tmpTxt1 = TelexText
                tmpTxt2 = " "
            Else
                i = TlxTxtFldSize
                While i > 1 And Mid(TelexText, i, 1) = " "
                    i = i - 1
                Wend
                tmpTxt1 = Mid(TelexText, 1, i)
                tmpTxt2 = Mid(TelexText, i + 1)
            End If
            LineNo = TlxTypeTab.GetCurrentSelected
            tmpTtyp = TlxTypeTab.GetColumnValue(LineNo, 0)
            If tmpTtyp = "FREE" Then tmpTtyp = " "
            outFields = "URNO,SERE,STAT,WSTA,CDAT,TIME,TTYP,TXT1,TXT2"
            tmpUrno = Trim(NewSendUrno.Text)
            tmpData = Trim(UfisServer.UrnoTab.GetColumnValue(0, 0))
            If (tmpUrno = "") Or (tmpUrno = tmpData) Then
                tmpUrno = UfisServer.UrnoPoolGetNext
            End If
            NewUrno = tmpUrno
            tmpCallCode = Trim(NewCallCode.Text)
            If tmpCallCode = "" Then tmpCallCode = "M"
            NewCallCode.Text = tmpCallCode
            outData = tmpUrno & ",S,S"
            outData = outData & "," & tmpCallCode
            tmpTime = MySetUp.GetUtcServerTime & Format(Now, "ss")
            outData = outData & "," & tmpTime
            outData = outData & "," & tmpTime
            outData = outData & "," & tmpTtyp
            outData = outData & "," & tmpTxt1
            outData = outData & "," & tmpTxt2
            retCod = UfisServer.CallCeda(tmpResult, "IRT", "TLXTAB", outFields, outData, "          ", "", 0, True, False)
            TlxRec = Me.CurrentRecord.Text
            If TlxRec <> "" Then
                outFields = ""
                tmpSere = GetFieldValue("SERE", TlxRec, DataPool.TlxTabFields)
                Select Case tmpSere
                    Case "C"
                        outFields = "STAT,WSTA"
                        outData = "A,S"
                    Case Else
                        outFields = "WSTA"
                        outData = "R"
                End Select
                If outFields <> "" Then
                    tmpUrno = GetFieldValue("URNO", TlxRec, DataPool.TlxTabFields)
                    outSqlKey = "WHERE URNO=" & tmpUrno
                    retCod = UfisServer.CallCeda(tmpResult, "URT", "TLXTAB", outFields, outData, outSqlKey, "", 0, True, False)
                End If
            End If
            MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", "The telex is on the way now.", "export", "", UserAnswer
            If Len(TlxUrnoFromRedirect) > 0 Then
                InsertFolderAndLink (NewUrno)
            End If
            Me.CurrentRecord.Text = ""
            NewSendUrno.Text = ""
            NewCallCode.Text = ""
        Else
            If TelexText = "" Then ErrMsg = "Can't send empty telexes!"
            MyMsgBox.CallAskUser 0, 0, 0, "Send Telex", ErrMsg, "stop", "", UserAnswer
        End If
        chkSend.Value = 0
        TlxUrnoFromRedirect = ""
    Else
        If MySitaOutType = "SITA_FILE" Then
            chkOutFormat.Value = 0
        Else
            chkPreView.Value = 0
        End If
        chkSend.BackColor = vbButtonFace
    End If
End Sub

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    'Set MyParent = Caller
    'Set MyCallButton = CallButton
End Sub

Public Sub BuildAutoSendTelex(CallCode As String, TlxSere As String, TlxType As String, TlxRecord As String, SitaText As String)
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpList As String
    Dim SetAddrList As String
    Dim tmpItem As String
    Dim clFields As String
    Dim tmpTxt1 As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpAdid As String
    Dim tmpAlc3 As String
    Dim tmpOrg3 As String
    Dim tmpVia3 As String
    Dim tmpVial As String
    Dim tmpDes3 As String
    Dim itm As Integer
    Dim cnt As Integer
    Dim TlxIsFileType As Boolean
    Dim TlxUrnoCol As Integer
    
    TlxUrnoCol = GetItemNo(DataPool.TlxTabFields, "URNO")
    TlxUrnoFromRedirect = GetItem(TlxRecord, TlxUrnoCol, ",")
    chkReset.Value = 1
    SetTelexDefault TlxTypeTab, TlxType
    Me.CurrentRecord.Text = TlxRecord
    Me.NewCallCode.Text = CallCode
    GetKeyItem SetAddrList, SitaText, "=DESTINATION TYPE B", "="
    If SetAddrList <> "" Then TlxIsFileType = True
    If TlxSere = "C" Then TlxIsFileType = True
    If TlxIsFileType Then
        GetKeyItem tmpData, SitaText, "=TEXT", "="
        tmpText = CleanTrimString(tmpData)
        GetKeyItem tmpData, SitaText, "=SMI", "="
        If tmpData = "" Then tmpData = TlxType
        If tmpData = "FREE" Then tmpData = ""
        If tmpData <> "" Then
            tmpData = CleanTrimString(tmpData)
            If tmpData <> "" Then
                cnt = Len(tmpData)
                If Left(tmpText, cnt) <> tmpData Then
                    tmpText = tmpData & vbNewLine & tmpText
                End If
            End If
        End If
        If TlxSere <> "C" Then
            GetKeyItem tmpData, SitaText, "=ORIGIN", "="
            If tmpData <> "" Then
                tmpData = CleanTrimString(tmpData)
                tmpText = "ORG." & tmpData & vbNewLine & tmpText
            End If
            tmpData = SetAddrList
            tmpData = CleanTrimString(tmpData)
            tmpData = Replace(tmpData, ".", ",", 1, -1, vbBinaryCompare)
            tmpList = ""
            SetAddrList = ""
            cnt = 0
            itm = 2
            tmpItem = GetItem(tmpData, itm, "STX,")
            While tmpItem <> ""
                If cnt = 8 Then
                    tmpList = tmpList & vbNewLine
                    cnt = 0
                ElseIf cnt > 0 Then
                    tmpList = tmpList & " "
                End If
                tmpItem = CleanTrimString(tmpItem)
                tmpList = tmpList & tmpItem
                SetAddrList = SetAddrList & "," & tmpItem
                cnt = cnt + 1
                itm = itm + 1
                tmpItem = GetItem(tmpData, itm, "STX,")
            Wend
            SetAddrList = Mid(SetAddrList, 2)
            If SetAddrList <> "" Then
                tmpData = GetCleanAddrList(SetAddrList, 8, " ")
                tmpText = "SNT." & tmpData & vbNewLine & tmpText
            End If
            GetKeyItem tmpData, SitaText, "=HEADER", "="
            If tmpData <> "" Then
                tmpData = CleanTrimString(tmpData)
                tmpText = tmpData & vbNewLine & tmpText
            End If
        Else
            clFields = DataPool.TlxTabFields.Text
            tmpTxt1 = GetFieldValue("TXT1", TlxRecord, clFields)
            tmpFldLst = GetItem(tmpTxt1, 1, Chr(30))
            tmpFldLst = CleanString(tmpFldLst, FOR_CLIENT, False)
            tmpDatLst = GetItem(tmpTxt1, 2, Chr(30))
            tmpDatLst = CleanString(tmpDatLst, FOR_CLIENT, False)
            If Left(tmpDatLst, 1) = "," Then tmpDatLst = Mid(tmpDatLst, 2)
            tmpAdid = GetFieldValue("ADID", tmpDatLst, tmpFldLst)
            tmpOrg3 = GetFieldValue("ORG3", tmpDatLst, tmpFldLst)
            tmpDes3 = GetFieldValue("DES3", tmpDatLst, tmpFldLst)
            tmpVia3 = GetFieldValue("VIA3", tmpDatLst, tmpFldLst)
            tmpVial = GetFieldValue("VIAL", tmpDatLst, tmpFldLst)
            tmpAlc3 = GetFieldValue("ALC3", tmpDatLst, tmpFldLst)
            AlcTab.SetColumnValue 0, 0, tmpAlc3
            Select Case tmpAdid
                Case "A"
                    ApcTab.SetColumnValue 0, 0, tmpOrg3
                    ApcTab.SetColumnValue 0, 1, tmpVial
                Case "D"
                    ApcTab.SetColumnValue 0, 0, tmpDes3
                    ApcTab.SetColumnValue 0, 1, tmpVial
                Case Else
            End Select
            AlcTab.Refresh
            ApcTab.Refresh
            LookForTlxAddr True
        End If
    Else
        tmpData = SitaText
        tmpData = CleanTrimString(tmpData)
        tmpText = tmpData
    End If
    txtTelexText.Text = tmpText
    'If TlxSere = "C" Then
    '    If Not FltInputPanel.Visible Then
    '        FltInputPanel.Visible = True
    '        Form_Resize
    '    End If
    'Else
    '    If FltInputPanel.Visible Then
    '        FltInputPanel.Visible = False
    '        Form_Resize
    '    End If
    'End If
End Sub

Private Sub CreateFlightDataPanel(UseCfgList As String)
    Dim CfgList As String
    Dim itm As Long
    Dim i As Integer
    Dim FldProp As String
    Dim tmpCapt As String
    Dim tmpName As String
    Dim NewTop As Long
    Dim NewLeft As Long
    '"FLNO,ALC3,STOA,STOD,ADID,REGN,ACT3,LAND,ONBL,OFBL,AIRB,PAX1,PAX2,PAX3,FKEY,FLNS,FLTN,URNO,ORG3,DES3,DCD1,DCD2,DTD1,DTD2,ETAI,ETDI,VIAL,FTYP,PAXT"
    For i = 0 To FltEditPanel.UBound
        FltEditPanel(i).Visible = False
    Next
    If UseCfgList <> "" Then
        CfgList = UseCfgList
    Else
        CfgList = "FLNO,ALC3,STOA,STOD,ADID,REGN,ACT3,LAND,ONBL,OFBL,AIRB,PAX1,PAX2,PAX3,FKEY,FLNS,FLTN,URNO,ORG3,DES3,DCD1,DCD2,DTD1,DTD2,ETAI,ETDI,VIAL,FTYP,PAXT"
        CfgList = Replace(CfgList, ",", "|", 1, -1, vbBinaryCompare)
    End If
    NewTop = FltEditPanel(0).Top
    NewLeft = FltEditPanel(0).Left
    
    i = 0
    itm = 0
    FldProp = GetRealItem(CfgList, itm, "|")
    While FldProp <> ""
        If i > FltEditPanel.UBound Then
            Load FltEditPanel(i)
            Load chkFltTick(i)
            Load lblFltField(i)
            Load txtFltInput(i)
            Load chkFltCheck(i)
            Set FltEditPanel(i).Container = FltInputPanel
            Set chkFltTick(i).Container = FltEditPanel(i)
            Set lblFltField(i).Container = FltEditPanel(i)
            Set txtFltInput(i).Container = FltEditPanel(i)
            Set chkFltCheck(i).Container = FltEditPanel(i)
        End If
        lblFltField(i).Caption = GetItem(FldProp, 1, ",")
        txtFltInput(i).Text = ""
        txtFltInput(i).BackColor = NormalGray
        chkFltTick(i).Value = 0
        chkFltTick(i).Visible = True
        lblFltField(i).Visible = True
        txtFltInput(i).Visible = True
        FltEditPanel(i).Top = NewTop
        FltEditPanel(i).Left = NewLeft
        FltEditPanel(i).Visible = True
        DrawBackGround FltEditPanel(i), MyLifeStyleValue, False, True
        NewTop = NewTop + FltEditPanel(i).Height
        i = i + 1
        itm = itm + 1
        FldProp = GetRealItem(CfgList, itm, "|")
    Wend
    Form_Resize
End Sub

Public Sub LoadGroupData()
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim i As Integer
    Dim SelLine As Integer
    Dim tlxOrigLineNo As Integer
    
    GroupTab.ResetContent
    GroupTab.Refresh
    Me.MousePointer = 11
    tmpFields = "GRPN,GRDE,GRAL,URNO,TPOW"
    tmpSqlKey = "WHERE TYPE = 'G'"
    tlxOrigLineNo = TlxOrigTab.GetCurrentSelected
    If tlxOrigLineNo >= 0 Then
        tmpSqlKey = tmpSqlKey & " AND TPOW = '" & TlxOrigTab.GetColumnValue(tlxOrigLineNo, 0) & "'"
    End If
    GroupTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    GroupTab.CedaHopo = UfisServer.HOPO
    GroupTab.CedaIdentifier = "IDX"
    GroupTab.CedaPort = "3357"
    GroupTab.CedaReceiveTimeout = "250"
    GroupTab.CedaRecordSeparator = vbLf
    GroupTab.CedaSendTimeout = "250"
    GroupTab.CedaServerName = UfisServer.HostName
    GroupTab.CedaTabext = UfisServer.TblExt
    GroupTab.CedaUser = gsUserName
    GroupTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        GroupTab.CedaAction "RT", "TTPTAB", tmpFields, "", tmpSqlKey
    End If
    GroupTab.Sort "0", True, True
    GroupTab.AutoSizeColumns
    If chkSelAddr(1).Tag = "" Then
        SelLine = -1
    Else
        For i = 0 To GroupTab.GetLineCount - 1
            If GroupTab.GetColumnValue(i, 3) = chkSelAddr(1).Tag Then
                SelLine = i
            End If
        Next
    End If
    GroupTab.SetCurrentSelection SelLine
    GroupTab.OnVScrollTo SelLine
    If SelLine >= 0 Then
        txtDestAddr.Text = CleanString(GroupTab.GetColumnValue(SelLine, 2), FOR_CLIENT, False)
    End If
    Me.MousePointer = 0
End Sub

Public Sub LoadTemplateData()
    Dim tmpSqlKey As String
    Dim tmpFields As String
    Dim i As Integer
    Dim SelLine As Integer
    Dim tlxOrigLineNo As Integer
    
    TemplateTab.ResetContent
    TemplateTab.Refresh
    Me.MousePointer = 11
    tmpFields = "TYPE,TPLN,TPDE,TPTX,URNO,GRAL,TPTP,TPT2,TPOW"
    If chkTplSel(0).Value = 1 Then
        tmpSqlKey = "WHERE TYPE = 'T'"
    Else
        If chkTplSel(1).Value = 1 Then
            tmpSqlKey = "WHERE TYPE = 'C'"
        Else
            If chkTplSel(2).Value = 1 Then
                tmpSqlKey = "WHERE TYPE = 'L'"
            Else
                tmpSqlKey = "WHERE TYPE = 'T'"
            End If
        End If
    End If
    tlxOrigLineNo = TlxOrigTab.GetCurrentSelected
    If tlxOrigLineNo >= 0 Then
        tmpSqlKey = tmpSqlKey & " AND TPOW = '" & TlxOrigTab.GetColumnValue(tlxOrigLineNo, 0) & "'"
    End If
    TemplateTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    TemplateTab.CedaHopo = UfisServer.HOPO
    TemplateTab.CedaIdentifier = "IDX"
    TemplateTab.CedaPort = "3357"
    TemplateTab.CedaReceiveTimeout = "250"
    TemplateTab.CedaRecordSeparator = vbLf
    TemplateTab.CedaSendTimeout = "250"
    TemplateTab.CedaServerName = UfisServer.HostName
    TemplateTab.CedaTabext = UfisServer.TblExt
    TemplateTab.CedaUser = gsUserName
    TemplateTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    If CedaIsConnected Then
        TemplateTab.CedaAction "RT", "TTPTAB", tmpFields, "", tmpSqlKey
    End If
    TemplateTab.Sort "1", True, True
    TemplateTab.AutoSizeColumns
    If chkSelAddr(2).Tag = "" Then
        SelLine = 0
    Else
        For i = 0 To TemplateTab.GetLineCount - 1
            If TemplateTab.GetColumnValue(i, 4) = chkSelAddr(2).Tag Then
                SelLine = i
            End If
        Next
    End If
    TemplateTab.SetCurrentSelection SelLine
    TemplateTab.OnVScrollTo SelLine
    txtTelexText.Text = CleanString(TemplateTab.GetColumnValue(SelLine, 3), FOR_CLIENT, False)
    If Len(TemplateTab.GetColumnValue(SelLine, 7)) > 0 Then
        txtTelexText.Text = txtTelexText.Text & CleanString(TemplateTab.GetColumnValue(SelLine, 7), FOR_CLIENT, False)
    End If
    Me.MousePointer = 0
End Sub

Private Sub GroupTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpLineNo As Integer
    
    If OnTop.Value = 1 Then
        OnTop.Value = 0
    End If
    tmpLineNo = GroupTab.GetCurrentSelected
    If tmpLineNo >= 0 Then
        EditGroups.txtGrpName.Text = GroupTab.GetColumnValue(tmpLineNo, 0)
        EditGroups.txtGrpDesc.Text = GroupTab.GetColumnValue(tmpLineNo, 1)
        EditGroups.txtGrpAddr.Text = CleanString(GroupTab.GetColumnValue(tmpLineNo, 2), FOR_CLIENT, False)
        EditGroups.txtGrpOwner.Text = GroupTab.GetColumnValue(tmpLineNo, 4)
        EditGroups.txtGrpName.Tag = GroupTab.GetColumnValue(tmpLineNo, 3)
        EditGroups.Visible = True
    Else
        MyMsgBox.CallAskUser 0, 0, 0, "Edit Groups", "No line sected", "hand", "", UserAnswer
    End If
End Sub

Private Sub TemplateTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpLineNo As Integer
    Dim i As Integer
    Dim found As Boolean
    Dim NewLine As String

    If OnTop.Value = 1 Then
        OnTop.Value = 0
    End If
    If TemplateTab.GetColumnValue(tmpLineNo, 0) = "T" Then
        tmpLineNo = TemplateTab.GetCurrentSelected
        If tmpLineNo >= 0 Then
            EditTemplates.txtTplName.Text = TemplateTab.GetColumnValue(tmpLineNo, 1)
            EditTemplates.txtTplDesc.Text = TemplateTab.GetColumnValue(tmpLineNo, 2)
            EditTemplates.txtTplText.Text = CleanString(TemplateTab.GetColumnValue(tmpLineNo, 3), FOR_CLIENT, False)
            If Len(TemplateTab.GetColumnValue(tmpLineNo, 7)) > 0 Then
                EditTemplates.txtTplText.Text = EditTemplates.txtTplText.Text & CleanString(TemplateTab.GetColumnValue(tmpLineNo, 7), FOR_CLIENT, False)
            End If
            EditTemplates.txtTplOwner.Text = TemplateTab.GetColumnValue(tmpLineNo, 8)
            EditTemplates.txtTplName.Tag = TemplateTab.GetColumnValue(tmpLineNo, 4)
            EditTemplates.txtTplType.Text = TemplateTab.GetColumnValue(tmpLineNo, 6)
            EditTemplates.Visible = True
            found = False
            For i = 0 To TlxTypeTab.GetLineCount - 1
                If TemplateTab.GetColumnValue(tmpLineNo, 6) = TlxTypeTab.GetColumnValue(i, 0) Then
                    TlxTypeTab.SetCurrentSelection i
                    TlxTypeTab.OnVScrollTo i
                    found = True
                End If
            Next
            If found = False Then
                NewLine = TemplateTab.GetColumnValue(tmpLineNo, 6) & ",,"
                TlxTypeTab.InsertTextLine NewLine, False
                TlxTypeTab.SetCurrentSelection TlxTypeTab.GetLineCount - 1
                TlxTypeTab.OnVScrollTo TlxTypeTab.GetLineCount - 1
            End If
        Else
            MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", "No line sected", "hand", "", UserAnswer
        End If
    End If
End Sub

Private Sub GroupTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)

    If (Selected) And (LineNo >= 0) And (chkSelAddr(1).Value = 1) Then
        txtDestAddr.Text = CleanString(GroupTab.GetColumnValue(LineNo, 2), FOR_CLIENT, False)
        chkSelAddr(1).Tag = GroupTab.GetColumnValue(LineNo, 3)
    End If
End Sub

Private Sub TemplateTab_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim i As Integer
    Dim found As Boolean
    Dim NewLine As String
    
    If (Selected) And (LineNo >= 0) And (chkSelAddr(2).Value = 1) Then
        fraDestAddr.Caption = "Destination Addresses"
        txtTelexText.Text = CleanString(TemplateTab.GetColumnValue(LineNo, 3), FOR_CLIENT, False)
        If Len(TemplateTab.GetColumnValue(LineNo, 7)) > 0 Then
            txtTelexText.Text = txtTelexText.Text & CleanString(TemplateTab.GetColumnValue(LineNo, 7), FOR_CLIENT, False)
        End If
        txtTelexText.Tag = ""
        chkSelAddr(2).Tag = TemplateTab.GetColumnValue(LineNo, 4)
        If TemplateTab.GetColumnValue(LineNo, 0) = "C" Then
            txtDestAddr.Text = CleanString(TemplateTab.GetColumnValue(LineNo, 5), FOR_CLIENT, False)
        Else
            If TemplateTab.GetColumnValue(LineNo, 0) = "L" Then
                txtDestAddr.Text = ""
                For i = 0 To GroupTab.GetLineCount - 1
                    If GroupTab.GetColumnValue(i, 3) = TemplateTab.GetColumnValue(LineNo, 5) Then
                        txtDestAddr.Text = CleanString(GroupTab.GetColumnValue(i, 2), FOR_CLIENT, False)
                        fraDestAddr.Caption = "Destination Addresses" & _
                                              " (Linked to Group : " & _
                                              GroupTab.GetColumnValue(i, 0) & ")"
                    End If
                Next
            End If
        End If
        If TemplateTab.GetColumnValue(LineNo, 0) <> "T" Then
            txtTplName.Text = TemplateTab.GetColumnValue(LineNo, 1)
            txtTplDesc.Text = TemplateTab.GetColumnValue(LineNo, 2)
            If TemplateTab.GetColumnValue(LineNo, 0) = "C" Then
                chkTemplate(0).Enabled = True
                chkTemplate(1).Enabled = False
            Else
                chkTemplate(0).Enabled = False
                chkTemplate(1).Enabled = True
            End If
            chkTemplate(2).Enabled = True
            txtTplName.Visible = True
            txtTplDesc.Visible = True
        Else
            txtTplName.Text = ""
            txtTplDesc.Text = ""
        End If
        found = False
        For i = 0 To TlxTypeTab.GetLineCount - 1
            If TemplateTab.GetColumnValue(LineNo, 6) = TlxTypeTab.GetColumnValue(i, 0) Then
                TlxTypeTab.SetCurrentSelection i
                TlxTypeTab.OnVScrollTo i
                found = True
            End If
        Next
        If found = False Then
            NewLine = TemplateTab.GetColumnValue(LineNo, 6) & ",,"
            TlxTypeTab.InsertTextLine NewLine, False
            TlxTypeTab.SetCurrentSelection TlxTypeTab.GetLineCount - 1
            TlxTypeTab.OnVScrollTo TlxTypeTab.GetLineCount - 1
        End If
    End If
End Sub

Private Sub chkTemplate_Click(Index As Integer)
    Dim RetVal As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim tmpLineNo As Integer
    Dim ErrorMsg As String
    Dim i As Integer
    Dim found As Boolean
    Dim tmpDestAddr As String
    Dim TlxTypLineNo As Integer
    Dim tlxOrigLineNo As Integer
    Dim tmpText As String
    Dim tmpText1 As String
    Dim tmpText2 As String

    If chkTemplate(Index).Value = 1 Then
        chkTemplate(Index).BackColor = LightGreen
        If Index = 2 Then
            If Len(txtDestAddr.Text) > 0 Then
                chkTest.Value = 0
                chkTest.Value = 1
            End If
        Else
            tmpLineNo = TemplateTab.GetCurrentSelected
            If tmpLineNo >= 0 Then
                ErrorMsg = ""
                If Len(txtTplName.Text) = 0 Then
                    ErrorMsg = "Missing Template Name"
                End If
                If Len(txtDestAddr.Text) = 0 Then
                    If Len(ErrorMsg) > 0 Then
                        ErrorMsg = ErrorMsg & " and Telex Addresses"
                    Else
                        ErrorMsg = "Missing Telex Addresses"
                    End If
                End If
                If Len(txtTelexText.Text) = 0 Then
                    If Len(ErrorMsg) > 0 Then
                        ErrorMsg = ErrorMsg & " and Telex Text"
                    Else
                        ErrorMsg = "Missing Telex Text"
                    End If
                End If
                If Len(ErrorMsg) > 0 Then
                    ErrorMsg = ErrorMsg & "!"
                    MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", ErrorMsg, "hand", "", UserAnswer
                Else
                    If Len(txtTplDesc.Text) = 0 Then
                        txtTplDesc.Text = " "
                    End If
                    tmpText = CleanString(txtTelexText.Text, FOR_SERVER, False)
                    If Len(tmpText) <= 4000 Then
                        tmpText1 = tmpText
                        tmpText2 = " "
                    Else
                        i = 4000
                        While i > 1 And Mid(tmpText, i, 1) = " "
                            i = i - 1
                        Wend
                        tmpText1 = Mid(tmpText, 1, i)
                        tmpText2 = Mid(tmpText, i + 1)
                    End If
                    If TemplateTab.GetColumnValue(tmpLineNo, 0) = "T" Then
                        ActResult = ""
                        ActCmd = "IRT"
                        ActTable = "TTPTAB"
                        ActFldLst = "URNO,HOPO,TYPE,TPLN,TPDE,TPTX,TPT2,TPTP,TPOW,GRAL"
                        ActCondition = ""
                        ActOrder = ""
                        chkSelAddr(2).Tag = UfisServer.UrnoPoolGetNext
                        ActDatLst = chkSelAddr(2).Tag & "," & _
                                    UfisServer.HOPO
                        If Index = 0 Then
                            ActDatLst = ActDatLst & ",C,"
                        Else
                            ActDatLst = ActDatLst & ",L,"
                        End If
                        TlxTypLineNo = TlxTypeTab.GetCurrentSelected
                        tlxOrigLineNo = TlxOrigTab.GetCurrentSelected
                        ActDatLst = ActDatLst & _
                                    txtTplName.Text & "," & _
                                    txtTplDesc.Text & "," & _
                                    tmpText1 & "," & _
                                    tmpText2 & "," & _
                                    TlxTypeTab.GetColumnValue(TlxTypLineNo, 0) & "," & _
                                    TlxOrigTab.GetColumnValue(tlxOrigLineNo, 0) & ","
                        If Index = 0 Then
                            ActDatLst = ActDatLst & CleanString(txtDestAddr.Text, FOR_SERVER, False)
                            found = True
                        Else
                            tmpDestAddr = CleanString(txtDestAddr.Text, FOR_SERVER, False)
                            found = False
                            For i = 0 To GroupTab.GetLineCount - 1
                                If GroupTab.GetColumnValue(i, 2) = tmpDestAddr Then
                                    ActDatLst = ActDatLst & GroupTab.GetColumnValue(i, 3)
                                    found = True
                                End If
                            Next
                        End If
                        If found = True Then
                            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                            chkTplSel(0).Value = 0
                            If Index = 0 Then
                                chkTplSel(1).Value = 1
                            Else
                                chkTplSel(2).Value = 1
                            End If
                        Else
                            MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", "Telex Addresses not found in any Group", "hand", "", UserAnswer
                        End If
                    Else
                        ActResult = ""
                        ActCmd = "URT"
                        ActTable = "TTPTAB"
                        If Index = 0 Then
                            ActFldLst = "TPLN,TPDE,TPTX,TPT2,GRAL"
                        Else
                            ActFldLst = "TPLN,TPDE,TPTX,TPT2"
                        End If
                        ActCondition = "WHERE URNO = " & TemplateTab.GetColumnValue(tmpLineNo, 4)
                        ActOrder = ""
                        ActDatLst = txtTplName.Text & "," & _
                                    txtTplDesc.Text & "," & _
                                    tmpText1 & "," & _
                                    tmpText2
                        If Index = 0 Then
                            ActDatLst = ActDatLst & "," & CleanString(txtDestAddr.Text, FOR_SERVER, False)
                        End If
                        RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
                    End If
                    LoadTemplateData
                End If
            Else
                MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", "No line sected", "hand", "", UserAnswer
            End If
        End If
        chkTemplate(Index).Value = 0
    Else
        chkTemplate(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub InsertFolderAndLink(NewTlxUrno As String)
    Dim ActResultI As String
    Dim ActCmdI As String
    Dim ActTableI As String
    Dim ActFldLstI As String
    Dim ActConditionI As String
    Dim ActOrderI As String
    Dim ActDatLstI As String
    Dim RetValI As Integer
    Dim ActResultO As String
    Dim ActCmdO As String
    Dim ActTableO As String
    Dim ActFldLstO As String
    Dim ActConditionO As String
    Dim ActOrderO As String
    Dim ActDatLstO As String
    Dim RetValO As Integer
    Dim count As Integer
    Dim CurLin As Integer
    Dim OldData As String
    Dim NewData As String
    
    If TelexPoolHead.chkTlxAssign.BackColor = vbRed Then
        TelexPoolHead.chkTlxAssign.Value = 1
    Else
        If TelexPoolHead.chkFlightList(7).Enabled = True Then
            TelexPoolHead.chkFlightList(7).Value = 1
        End If
    End If
    ActResultI = ""
    ActCmdI = "RTA"
    ActTableI = "TFNTAB"
    ActFldLstI = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
    ActConditionI = "WHERE TURN = " & TlxUrnoFromRedirect
    ActOrderI = ""
    ActDatLstI = ""
    RetValI = UfisServer.CallCeda(ActResultI, ActCmdI, ActTableI, ActFldLstI, ActDatLstI, ActConditionI, ActOrderI, 0, False, False)
    If RetValI >= 0 Then
        count = UfisServer.DataBuffer(0).GetLineCount - 1
        If count >= 0 Then
            For CurLin = 0 To count
                OldData = UfisServer.DataBuffer(0).GetLineValues(CurLin)
                NewData = UfisServer.UrnoPoolGetNext & "," & _
                          Trim(GetItem(OldData, 2, ",")) & "," & _
                          Trim(GetItem(OldData, 3, ",")) & "," & _
                          Trim(GetItem(OldData, 4, ",")) & "," & _
                          Trim(GetItem(OldData, 5, ",")) & "," & _
                          NewTlxUrno & "," & _
                          Trim(GetItem(OldData, 7, ","))
                ActResultO = ""
                ActCmdO = "IRT"
                ActTableO = "TFNTAB"
                ActFldLstO = "URNO,HOPO,FNAM,FDAT,FREM,TURN,DATC"
                ActConditionO = ""
                ActOrderO = ""
                ActDatLstO = NewData
                RetValO = UfisServer.CallCeda(ActResultO, ActCmdO, ActTableO, ActFldLstO, ActDatLstO, ActConditionO, ActOrderO, 1, True, False)
            Next
            TelexPoolHead.SetFolderValues CurMem
        End If
    End If
    ActResultI = ""
    ActCmdI = "RTA"
    ActTableI = "TLKTAB"
    ActFldLstI = "URNO,HOPO,TURN,FURN,ADID,FLNO,FDAT,DATC"
    ActConditionI = "WHERE TURN = " & TlxUrnoFromRedirect
    ActOrderI = ""
    ActDatLstI = ""
    RetValI = UfisServer.CallCeda(ActResultI, ActCmdI, ActTableI, ActFldLstI, ActDatLstI, ActConditionI, ActOrderI, 0, False, False)
    If RetValI >= 0 Then
        count = UfisServer.DataBuffer(0).GetLineCount - 1
        If count >= 0 Then
            For CurLin = 0 To count
                OldData = UfisServer.DataBuffer(0).GetLineValues(CurLin)
                NewData = UfisServer.UrnoPoolGetNext & "," & _
                          Trim(GetItem(OldData, 2, ",")) & "," & _
                          NewTlxUrno & "," & _
                          Trim(GetItem(OldData, 4, ",")) & "," & _
                          Trim(GetItem(OldData, 5, ",")) & "," & _
                          Trim(GetItem(OldData, 6, ",")) & "," & _
                          Trim(GetItem(OldData, 7, ",")) & "," & _
                          Trim(GetItem(OldData, 8, ","))
                ActResultO = ""
                ActCmdO = "IRT"
                ActTableO = "TLKTAB"
                ActFldLstO = "URNO,HOPO,TURN,FURN,ADID,FLNO,FDAT,DATC"
                ActConditionO = ""
                ActOrderO = ""
                ActDatLstO = NewData
                RetValO = UfisServer.CallCeda(ActResultO, ActCmdO, ActTableO, ActFldLstO, ActDatLstO, ActConditionO, ActOrderO, 1, True, False)
            Next
            TelexPoolHead.CountFlightTelex
        End If
    End If
End Sub
