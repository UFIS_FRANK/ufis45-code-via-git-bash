VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form MainDialog 
   Caption         =   "Common Data Input Dialog"
   ClientHeight    =   9270
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12540
   LinkTopic       =   "Form1"
   ScaleHeight     =   9270
   ScaleWidth      =   12540
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog cdgSave 
      Left            =   1440
      Top             =   7320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox picWorkArea 
      Height          =   4815
      Left            =   2520
      ScaleHeight     =   4755
      ScaleWidth      =   6075
      TabIndex        =   0
      Top             =   1800
      Width           =   6135
      Begin VB.PictureBox picMsg 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   855
         Left            =   0
         ScaleHeight     =   855
         ScaleWidth      =   2535
         TabIndex        =   4
         Top             =   0
         Visible         =   0   'False
         Width           =   2535
         Begin VB.Shape shpMsg 
            BackColor       =   &H8000000F&
            BorderColor     =   &H00000000&
            BorderWidth     =   2
            FillColor       =   &H8000000F&
            Height          =   825
            Left            =   0
            Top             =   0
            Width           =   2500
         End
         Begin VB.Label lblMsg 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Message"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1080
            TabIndex        =   5
            Top             =   240
            Width           =   765
         End
      End
      Begin VB.Label lblLoading 
         Caption         =   "Loading data... Please wait"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   720
         TabIndex        =   11
         Top             =   960
         Width           =   3975
      End
   End
   Begin MSComctlLib.TabStrip tbsFunctions 
      Height          =   375
      Left            =   2520
      TabIndex        =   10
      Top             =   1440
      Visible         =   0   'False
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   661
      ImageList       =   "imlFunctions"
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imlFunctions 
      Left            =   9960
      Top             =   2760
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MainDialog.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MainDialog.frx":039A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MainDialog.frx":0734
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MainDialog.frx":0ACE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picSizer 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   3720
      ScaleHeight     =   855
      ScaleWidth      =   15
      TabIndex        =   9
      Top             =   6720
      Visible         =   0   'False
      Width           =   20
   End
   Begin VB.PictureBox picSep 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   3480
      MousePointer    =   9  'Size W E
      ScaleHeight     =   855
      ScaleWidth      =   15
      TabIndex        =   8
      Top             =   6720
      Width           =   20
   End
   Begin VB.PictureBox picFlightList 
      Height          =   6135
      Left            =   0
      ScaleHeight     =   6075
      ScaleWidth      =   2475
      TabIndex        =   7
      Top             =   435
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.PictureBox picFlightDetails 
      Height          =   975
      Left            =   2520
      ScaleHeight     =   915
      ScaleWidth      =   8655
      TabIndex        =   3
      Top             =   435
      Visible         =   0   'False
      Width           =   8715
   End
   Begin VB.PictureBox picMainButtons 
      Height          =   435
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   11175
      TabIndex        =   6
      Top             =   0
      Visible         =   0   'False
      Width           =   11235
   End
   Begin VB.PictureBox picRightButtons 
      Align           =   4  'Align Right
      Height          =   9270
      Left            =   11775
      ScaleHeight     =   9210
      ScaleWidth      =   705
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   765
      Begin VB.CheckBox chkRightButtons 
         Caption         =   "Right Button"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   765
      End
   End
End
Attribute VB_Name = "MainDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public AppType As String
Public FlightUrno As String
Public StandAlone As Boolean

Private IsFormActivated As Boolean
Private ChildForms() As Form

Private Sub LoadBasicData()
    Dim blnShowSplashScreen As Boolean
    
    blnShowSplashScreen = (GetConfigEntry(colConfigs, AppType, "SHOW_SPLASH_SCREEN", "NO") = "YES")
    pstrBasicData = GetConfigEntry(colConfigs, AppType, "BASIC_DATA", "NONE")
    
    If pstrBasicData <> "NONE" Then
        BasicData.ShowSplashScreen = blnShowSplashScreen
        BasicData.DataTables = pstrBasicData
        Call ChangeFormBorderStyle(BasicData, vbBSNone)
        BasicData.Show vbModal
    End If
End Sub

Private Function GetActiveForm() As Form
    Dim intSelectedOption As Integer

    intSelectedOption = tbsFunctions.SelectedItem.Index - 1
    If intSelectedOption >= 0 Then
        If UBound(ChildForms) >= intSelectedOption Then
            Set GetActiveForm = ChildForms(intSelectedOption)
        End If
    End If
End Function

Public Sub EvaluateBc(ByVal CedaCmd As String, ByVal ObjName As String, ByVal CedaSqlKey As String, Optional ByVal ChangedFields As String)
    Dim ActiveForm As Form
    Dim rsAft As ADODB.Recordset
    Dim vntBookmark As Variant
    
    Set ActiveForm = GetActiveForm()
    Set rsAft = colDataSources.Item("AFTTAB")
    
    Select Case ObjName
        Case "AFTTAB"
            If Not (rsAft.BOF And rsAft.EOF) Then
                If rsAft.BOF Or rsAft.EOF Then rsAft.MoveFirst
                If rsAft.Fields("URNO").Value <> Val(CedaSqlKey) Then
                    'save current pos
                    vntBookmark = rsAft.Bookmark
                    If CedaSqlKey <> "" Then
                        If IsNumeric(CedaSqlKey) Then
                            rsAft.Find "URNO = " & CedaSqlKey, , , 1
                        End If
                    End If
                End If
            End If
            If rsAft.EOF Then
                If Not IsEmpty(vntBookmark) Then
                    'go back to prev pos
                    rsAft.Bookmark = vntBookmark
                End If
                
                Exit Sub
            End If
            
            Select Case CedaCmd
                Case "IFR", "DFR"
                    If FormIsLoaded(ActiveForm.Name) Then
                        Select Case ActiveForm.Name
                            Case "Deicing"
                                ActiveForm.RefreshDisplay CedaCmd, ObjName, CedaSqlKey, ChangedFields
                        End Select
                    End If
                
                Case "UFR"
                    If FormIsLoaded("FlightDetails") Then
                        If FlightDetails.optSelFlight(0).Tag = CedaSqlKey Or _
                        FlightDetails.optSelFlight(1).Tag = CedaSqlKey Then
                            FlightDetails.RefreshFlightDetails CedaSqlKey, ChangedFields
                        End If
                    End If
                    
                    If FormIsLoaded("FlightList") Then
                        FlightList.RefreshTabByUrno CedaSqlKey
                    End If
                    
                    If FormIsLoaded(ActiveForm.Name) Then
                        Select Case ActiveForm.Name
                            Case "FlightUpdate"
                                ActiveForm.RefreshDisplay CedaSqlKey, ChangedFields
                            Case "CICUpdate", "CSList", "Deicing"
                                ActiveForm.RefreshDisplay CedaCmd, ObjName, CedaSqlKey, ChangedFields
                        End Select
                    End If
                    
            End Select
        Case "CCATAB", "CSCTAB", "CSDTAB"
            If FormIsLoaded(ActiveForm.Name) Then
                Select Case ActiveForm.Name
                    Case "CICUpdate", "CSList"
                        ActiveForm.RefreshDisplay CedaCmd, ObjName, CedaSqlKey, ChangedFields
                End Select
            End If
        Case "VCDTAB"
            If FormIsLoaded("MainButtons") Then
                MainButtons.UpdateView CedaCmd, CedaSqlKey, ChangedFields
            End If
            
            If FormIsLoaded("FlightView") Then
                FlightView.UpdateView CedaCmd, CedaSqlKey, ChangedFields
            End If
        Case "ICETAB"
            If FormIsLoaded(ActiveForm.Name) Then
                Select Case ActiveForm.Name
                    Case "Deicing"
                        ActiveForm.RefreshDisplay CedaCmd, ObjName, CedaSqlKey, ChangedFields
                End Select
            End If
        Case "DCFTAB"
            If FormIsLoaded(ActiveForm.Name) Then
                Select Case ActiveForm.Name
                    Case "DelaysDialog"
                        ActiveForm.RefreshDisplay CedaCmd, ObjName, CedaSqlKey, ChangedFields
                End Select
            End If
    End Select
End Sub

Private Sub EvaluateCommands(ByVal AppCommand As String, Optional ByVal ADID As String)
    Dim vntAppCommands As Variant
    Dim vntAppCommand As Variant
    Dim colCommand As New CollectionExtended
    Dim intTabCount As Integer
    Dim i As Integer
    Dim vntTitles As Variant
    Dim strMainTitle As String
    Dim strTitle As String
    Dim vntCodes As Variant
    Dim strCode As String
    Dim vntCriteria As Variant
    Dim strCriteria As String
    Dim strCommand As String
    Dim blnShowTab As Boolean
    
    vntAppCommands = Split(AppCommand, ",")
    For Each vntAppCommand In vntAppCommands
        strMainTitle = GetConfigEntry(colConfigs, vntAppCommand, "WINDOW_TITLE", "")
        strTitle = GetConfigEntry(colConfigs, vntAppCommand, "WINDOW_TAB_TITLES", strMainTitle)
        vntTitles = Split(strTitle, ",")
        
        strCode = GetConfigEntry(colConfigs, vntAppCommand, "WINDOW_TAB_COMMANDS", vntAppCommand)
        vntCodes = Split(strCode, ",")
        
        strCriteria = GetConfigEntry(colConfigs, vntAppCommand, "WINDOW_TAB_CRITERIA", "")
        vntCriteria = Split(strCriteria, ",")
        
        intTabCount = Val(GetConfigEntry(colConfigs, vntAppCommand, "WINDOW_TAB_COUNT", "1"))
        
        For i = 0 To intTabCount - 1
            If i <= UBound(vntCriteria) Then
                strCriteria = vntCriteria(i)
            Else
                strCriteria = ""
            End If
            blnShowTab = (strCriteria = "") Or (strCriteria = "ALL") Or _
                (strCriteria = "ARR" And ADID = "A") Or _
                (strCriteria = "ARRIVAL" And ADID = "A") Or _
                (strCriteria = "DEP" And ADID <> "A") Or _
                (strCriteria = "DEPARTURE" And ADID <> "A") Or _
                (ADID = "")

            If blnShowTab Then
                If i <= UBound(vntTitles) Then
                    strTitle = vntTitles(i)
                Else
                    strTitle = strMainTitle
                End If
                If i <= UBound(vntCodes) Then
                    strCode = vntCodes(i)
                Else
                    strCode = vntAppCommand
                End If
                
                strCommand = strTitle & "|" & strCode
                
                colCommand.Add strCommand
            End If
        Next i
    Next vntAppCommand
    
    Call InitMainButtons(colCommand)
End Sub

Public Sub LoadData(ByVal strUrno As String)
    Dim frm As Form
    Dim intSelectedIndex As Integer
    
    intSelectedIndex = tbsFunctions.SelectedItem.Index - 1
    If intSelectedIndex >= 0 Then
        Set frm = ChildForms(intSelectedIndex)
        If Not (frm Is Nothing) Then
            frm.LoadData strUrno
        End If
    End If
End Sub

Private Sub FillInFlightData()
    Dim vntUrnos As Variant 'igu on 12/02/2012
    Dim strArrUrno As String
    Dim strDepUrno As String
    Dim rsAft As ADODB.Recordset

    Set rsAft = colDataSources.Item("AFTTAB")
    If Not (rsAft Is Nothing) Then
        If Not (rsAft.BOF And rsAft.EOF) Then
'------------------
'igu on 12/02/2012
'-----------------
'            rsAft.MoveFirst
'            Do While Not rsAft.EOF
'                If RTrim(rsAft.Fields("ADID").Value) = "A" Then
'                    strArrUrno = rsAft.Fields("URNO").Value
'                Else
'                    strDepUrno = rsAft.Fields("URNO").Value
'                End If
'
'                rsAft.MoveNext
'            Loop
            
            vntUrnos = Split(FlightUrno, ",")
            strArrUrno = vntUrnos(0)
            strDepUrno = vntUrnos(1)
'------------------
'igu on 12/02/2012
'-----------------
            
            FlightDetails.ShowFlightDetails strArrUrno, strDepUrno
            
            Set tbsFunctions.SelectedItem = tbsFunctions.Tabs(1)
        End If
    End If
End Sub

Private Sub InitRightButtons()
    Dim i As Integer
    Dim ButtonCaptions As Variant
    Dim ButtonCaptionItem As Variant
    Dim ButtonPictures As Variant
    
    ButtonCaptions = Array("ON TOP", "EXIT")
    ButtonPictures = Array(imlFunctions.ListImages.Item(3).Picture, _
                           imlFunctions.ListImages.Item(4).Picture)
    i = 0
    For Each ButtonCaptionItem In ButtonCaptions
        If i > 0 Then
            Load chkRightButtons(i)
            
            chkRightButtons(i).Top = chkRightButtons(i - 1).Top + chkRightButtons(i - 1).Height
            chkRightButtons(i).Left = chkRightButtons(i - 1).Left
        End If
        
        chkRightButtons(i).Caption = ButtonCaptionItem
        Set chkRightButtons(i).Picture = ButtonPictures(i)
        If ButtonPictures(i) Is Nothing Then
            chkRightButtons(i).Height = 375
        End If
        chkRightButtons(i).Visible = True
        
        i = i + 1
    Next ButtonCaptionItem
End Sub

Private Sub InitMainButtons(ButtonCaptions As CollectionExtended)
    Dim i As Integer, j As Integer
    Dim ButtonCaptionItem As Variant
    Dim ButtonCaptionArr As Variant
    Dim ButtonCaption As String
    Dim ButtonKey As String
    Dim oTab As MSComctlLib.Tab
            
    ReDim ChildForms(ButtonCaptions.Count - 1)
    
    For i = 1 To ButtonCaptions.Count
        ButtonCaptionItem = ButtonCaptions.Item(i)
        'Assign default values
        ButtonCaption = Space(5)
        ButtonKey = ""
        
        ButtonCaptionArr = Split(ButtonCaptionItem, "|")
        For j = 0 To UBound(ButtonCaptionArr)
            Select Case j
                Case 0
                    ButtonCaption = ButtonCaptionArr(j)
                Case 1
                    ButtonKey = ButtonCaptionArr(j)
            End Select
        Next j
    
        If i = 1 Then
            Set oTab = tbsFunctions.Tabs(i)
        Else
            Set oTab = tbsFunctions.Tabs.Add
        End If
        oTab.Key = ButtonKey
        oTab.Caption = ButtonCaption
        oTab.Image = 1
    Next i
End Sub

Private Sub ShowChildForm(FormToShow As Form, ByVal Index As Integer, ByVal LoadInMainForm As Boolean)
    Dim blnFormAlreadyInMemory As Boolean
    
    blnFormAlreadyInMemory = False
    If Index = 0 Then
        If Not (ChildForms(Index) Is Nothing) Then
            blnFormAlreadyInMemory = True
        End If
    Else
        If Index > UBound(ChildForms) Then
            ReDim Preserve ChildForms(Index)
        Else
            If Not (ChildForms(Index) Is Nothing) Then
                blnFormAlreadyInMemory = True
            End If
        End If
    End If
    If Not blnFormAlreadyInMemory Then
        Set ChildForms(Index) = FormToShow
        If LoadInMainForm Then
            Call ChangeFormBorderStyle(FormToShow, vbBSNone)
            FormWithinForm picWorkArea, FormToShow
            FormToShow.Move 0, 0, picWorkArea.Width, picWorkArea.Height
        Else
            FormToShow.Move MyMainForm.Left, MyMainForm.Top, MyMainForm.Width, MyMainForm.Height
        End If
        FormToShow.Show
    End If
    
    If LoadInMainForm Then
        If Not (ChildForms(Index) Is Nothing) Then
            ChildForms(Index).ZOrder
        End If
    Else
        MyMainForm.Hide
    End If
End Sub

Private Sub Form_Activate()
    If Not IsFormActivated Then
        If pblnIsStandAlone Then
            If pblnShowFlightList Then
                FlightList.LoadData ""
            End If
            Set tbsFunctions.SelectedItem = tbsFunctions.Tabs(1)
        Else
            FillInFlightData
        End If
        
        IsFormActivated = True
    End If
End Sub

Private Sub chkRightButtons_Click(Index As Integer)
    Dim ButtonTypeColor As Variant
    Dim ButtonType As String
    Dim ButtonColor As String
    Dim ButtonState As Boolean
    
    If chkRightButtons(Index).Value = vbChecked Then
        chkRightButtons(Index).BackColor = LightGreen
    Else
        chkRightButtons(Index).BackColor = vbButtonFace
    End If
    
    Select Case Index
        Case 0 '"TOP"
            pblnIsOnTop = (chkRightButtons(Index).Value = vbChecked)
            SetFormOnTop Me, pblnIsOnTop
        Case 1 '"EXIT"
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    Dim strApplCommands As String
    Dim strAppName As String
    Dim strADID As String
    Dim FormToShow As Form
    Dim rsAft As ADODB.Recordset
    
    If InStr(GetConfigEntry(colConfigs, "MAIN", "VALID_COMMAND", "") & ",", AppType & ",") <= 0 Then
        AppType = "MAIN"
    End If
    
    If Not StandAlone Then
        If Not ReadFlightRecordByUrno(FlightUrno, AppType) Then
            MsgBoxEx "Cannot find the flight record!", vbInformation, Me.Caption, , , eCentreDialog
            Unload Me
            Exit Sub
        End If
        Set rsAft = colDataSources.Item("AFTTAB")
    End If
    
    If Not (rsAft Is Nothing) Then
        If Not (rsAft.BOF And rsAft.EOF) Then
            If rsAft.RecordCount = 1 Then
                strADID = RTrim(rsAft.Fields("ADID").Value)
            End If
        End If
    End If
    
    If AppType = "MAIN" Then
        strApplCommands = GetConfigEntry(colConfigs, "MAIN", "DEFAULT_COMMAND", "")
        If InStr(strApplCommands, ",") = 0 Then
            AppType = strApplCommands
        End If
    Else
        strApplCommands = AppType
    End If
    If strApplCommands = "" Then
        MsgBoxEx "Unknown command!", vbInformation, Me.Caption, , , eCentreDialog
        Unload Me
        Exit Sub
    End If
    
    Call EvaluateCommands(strApplCommands, strADID)
    
    strAppName = GetConfigEntry(colConfigs, AppType, "APP_NAME", "CDID")
    colParameters.Add "'" & strAppName & "'", "AppName"
    
    Call LoadBasicData
    
    'get the time diff in winter and summer
    'If colDataSources.Exists("APTTAB") Then 'igu on 12/01/2012
    If colDataSources.Exists("APTTAB") And IsTimeInLocal Then 'igu on 12/01/2012
        Set colUTCTimeDiff = AptUTCTimeDiff(colDataSources.Item("APTTAB"), oUfisServer.HOPO, UtcTimeDiff)
    Else
        Set colUTCTimeDiff = AptUTCTimeDiff(Nothing, oUfisServer.HOPO, UtcTimeDiff)
    End If
    
    tbsFunctions.Visible = (tbsFunctions.Tabs.Count > 1)
    
    Call InitRightButtons
    pblnIsOnTop = (GetConfigEntry(colConfigs, AppType, "WINDOW_ON_TOP", "YES") = "YES")
    chkRightButtons(0).Value = IIf(pblnIsOnTop, vbChecked, vbUnchecked)
    
    'read settings
    pblnIsStandAlone = StandAlone
    
    IsUTCTimeZone = (GetConfigEntry(colConfigs, AppType, "DEFAULT_TIME_ZONE", "LOCAL") = "UTC")
    
    pblnHandleBc = (GetConfigEntry(colConfigs, AppType, "HANDLE_BC", "YES") = "YES")
    pblnShowFlightList = (GetConfigEntry(colConfigs, AppType, "SHOW_FLIGHT_LIST", "YES") = "YES")
    pblnShowFlightFilterOnStartup = (GetConfigEntry(colConfigs, AppType, "SHOW_FLIGHT_FILTER_ON_STARTUP", "YES") = "YES")
    pblnShowFlightDetails = (GetConfigEntry(colConfigs, AppType, "SHOW_FLIGHT_DETAILS", "YES") = "YES")
    pblnShowRightButtons = (GetConfigEntry(colConfigs, AppType, "SHOW_RIGHT_BUTTONS", "YES") = "YES")
    
    pblnFlightListMainButtons = (GetConfigEntry(colConfigs, "FLIGHT_LIST", "SHOW_MAIN_BUTTONS", "YES") = "YES")
    pblnFlightListWindow = (GetConfigEntry(colConfigs, "FLIGHT_LIST", "FLOATING_WINDOW", "YES") = "YES")
    
    Me.Caption = GetConfigEntry(colConfigs, AppType, "WINDOW_TITLE", "Common Data Input Dialog")
    
    Call SetWindowSizePos(Me, colConfigs, AppType)
    'end of settings
    
    If pblnHandleBc Then
        oUfisServer.ConnectToBcProxy
    End If
    
    picRightButtons.Visible = pblnShowRightButtons
    
    If (Not pblnIsStandAlone) Or pblnFlightListMainButtons Then
        picMainButtons.Visible = False
    Else
        Set FormToShow = MainButtons
        Call ChangeFormBorderStyle(FormToShow, vbBSNone)
        FormWithinForm picMainButtons, FormToShow
        'FormToShow.Move 0, 0, picMainButtons.Width, picMainButtons.Height
        FormToShow.Show
    
        picMainButtons.Visible = True
    End If
    
    If (Not pblnIsStandAlone) Or pblnFlightListWindow Or (Not pblnShowFlightList) Then
        picFlightList.Visible = False
        picSep.Visible = False
        
        If pblnIsStandAlone And pblnFlightListWindow And pblnShowFlightList Then
            FlightList.Show
        End If
    Else
        Set FormToShow = FlightList
        Call ChangeFormBorderStyle(FormToShow, vbBSNone)
        FormWithinForm picFlightList, FormToShow
        FormToShow.Move 0, 0, picFlightList.Width, picFlightList.Height
        FormToShow.Show
        
        picFlightList.Width = CSng(GetConfigEntry(colConfigs, "FLIGHT_LIST", "WINDOW_WIDTH", "7190"))
        
        picFlightList.Visible = True
        picSep.Visible = True
    End If
    
'    If Not pblnShowFlightDetails Then
'        picFlightDetails.Visible = False
'    Else
'        Set FormToShow = FlightDetails
'        Call ChangeFormBorderStyle(FormToShow, vbBSNone)
'        FormWithinForm picFlightDetails, FormToShow
'        FormToShow.Move 0, 0, picFlightDetails.Width, picFlightDetails.Height
'        FormToShow.Show
'
'        picFlightDetails.Visible = True
'    End If
    
    Call Form_Resize
    
    IsFormActivated = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim i As Integer
    
    If StandAlone Then
        pblnCanExitApp = True
        If MsgBox("Exit program?", vbQuestion + vbYesNo + vbDefaultButton2, Me.Caption) = vbNo Then
            Cancel = True
            pblnCanExitApp = False
            Exit Sub
        End If
    End If
    
    'try to unload current active form
    On Error Resume Next
    
    i = tbsFunctions.SelectedItem.Index - 1
    If UBound(ChildForms) >= i Then
        If Not (ChildForms(i) Is Nothing) Then
            pblnCanExitApp = True
            Unload ChildForms(i)
            If Not pblnCanExitApp Then
                Cancel = True
                Exit Sub
            End If
        End If
    End If
    
    For i = 0 To UBound(ChildForms)
        If Not (ChildForms(i) Is Nothing) Then
            Unload ChildForms(i)
        End If
    Next i
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    Dim sngTop As Single
    Dim sngLeftWidth As Single
    Dim sngRightWidth As Single
    Dim sngWidth As Single
    
    On Error Resume Next
    
    If Me.WindowState <> vbMinimized Then
        sngTop = 0
        If picMainButtons.Visible Then sngTop = sngTop + picMainButtons.Height
        picFlightList.Top = sngTop
        picFlightList.Height = Me.ScaleHeight - sngTop
        picSep.Top = picFlightList.Top
        picSep.Height = picFlightList.Height
        tbsFunctions.Top = sngTop
        
        If tbsFunctions.Visible Then sngTop = sngTop + tbsFunctions.Height
        picFlightDetails.Top = sngTop
           
        If picFlightDetails.Visible Then sngTop = sngTop + picFlightDetails.Height
        
        'calculate the width
        sngLeftWidth = 0
        If picFlightList.Visible Then sngLeftWidth = sngLeftWidth + picFlightList.Width
        picSep.Left = sngLeftWidth
        
        If picSep.Visible Then sngLeftWidth = sngLeftWidth + picSep.Width
        
        sngRightWidth = 0
        If picRightButtons.Visible Then sngRightWidth = picRightButtons.Width
        
        sngWidth = Me.ScaleWidth - sngRightWidth
        picMainButtons.Width = sngWidth
        
        sngWidth = sngWidth - sngLeftWidth
        tbsFunctions.Left = sngLeftWidth
        tbsFunctions.Width = sngWidth
        picFlightDetails.Left = sngLeftWidth
        picFlightDetails.Width = sngWidth
        
        picWorkArea.Move sngLeftWidth, sngTop, sngWidth, _
            Me.ScaleHeight - sngTop
        
        If Me.Visible Then
            For i = 0 To UBound(ChildForms)
                If Not (ChildForms(i) Is Nothing) Then
                    ChildForms(i).Move 0, 0, picWorkArea.Width, picWorkArea.Height
                End If
            Next i
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If pintBcLogFileHandle > 0 Then
        On Error Resume Next
    
        Close #pintBcLogFileHandle
    End If
    
    End
End Sub

Private Sub picFlightDetails_Resize()
    FlightDetails.Move 0, 0, picFlightDetails.Width, picFlightDetails.Height
End Sub

Private Sub picFlightList_Resize()
    If (pblnIsStandAlone And Not pblnFlightListWindow) And pblnShowFlightList Then
        If FormIsLoaded("FlightList") Then
            FlightList.Move 0, 0, picFlightList.Width, picFlightList.Height
        End If
    End If
End Sub

Private Sub picMainButtons_Resize()
    If pblnIsStandAlone And Not pblnFlightListMainButtons Then
        MainButtons.Move 0, 0, picMainButtons.Width, picMainButtons.Height
    End If
End Sub

Private Sub picSep_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        picSizer.Move picSep.Left, picSep.Top, picSep.Width, picSep.Height
        picSizer.Visible = True
    End If
End Sub

Private Sub picSep_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Const MIN_WIDTH = 1500
    Dim nLeft As Single
    
    If Button = vbLeftButton Then
        nLeft = picSep.Left + x
        If nLeft < MIN_WIDTH Then nLeft = MIN_WIDTH
        If nLeft > Me.ScaleWidth - MIN_WIDTH Then nLeft = Me.ScaleWidth - MIN_WIDTH
        picSizer.Left = nLeft
    End If
End Sub

Private Sub picSep_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        picSizer.Visible = False
        
        picFlightList.Width = picSizer.Left - picSizer.Width
        Call Form_Resize
    End If
End Sub

Private Sub tbsFunctions_Click()
    Dim TabKey As String
    Dim oTab As MSComctlLib.Tab
    Dim Index As Integer
    Dim frm As Form
    Dim strUrno As String
    Dim blnLoadInMainForm As Boolean
    
    blnLoadInMainForm = True
    
    For Each oTab In tbsFunctions.Tabs
        If tbsFunctions.SelectedItem = oTab Then
            oTab.Image = 2
        Else
            oTab.Image = 1
        End If
    Next oTab
    
    Index = tbsFunctions.SelectedItem.Index - 1
    TabKey = tbsFunctions.SelectedItem.Key
    If ChildForms(Index) Is Nothing Then
        Select Case TabKey
            Case "FLUPD"
                Set frm = New FlightUpdate
            Case "CCUPD"
                Set frm = New CICUpdate
                frm.FormType = ftyDeparture
            Case "BLUPD"
                Set frm = New CICUpdate
                frm.FormType = ftyArrival
            Case "FCSL"
                Set frm = New CSList
            Case "FDLY"
                Set frm = New DelaysDialog
                blnLoadInMainForm = False
            Case "DE-ICING"
                Set frm = New Deicing
                blnLoadInMainForm = False
        End Select
        Load frm
    Else
        Set frm = ChildForms(Index)
    End If
    
    Select Case TabKey
        Case "CCUPD"
            If FlightDetails.optSelFlight(1).Enabled Then
                If Not FlightDetails.optSelFlight(1).Value Then
                    FlightDetails.optSelFlight(1).Value = True
                End If
            End If
        Case "BLUPD"
            If FlightDetails.optSelFlight(0).Enabled Then
                If Not FlightDetails.optSelFlight(0).Value Then
                    FlightDetails.optSelFlight(0).Value = True
                End If
            End If
    End Select

    strUrno = FlightDetails.CurrentUrno
    frm.LoadData strUrno
    
    Call ShowChildForm(frm, Index, blnLoadInMainForm)
End Sub
