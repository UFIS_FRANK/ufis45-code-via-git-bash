VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form DelaysDialog 
   Caption         =   "Delays"
   ClientHeight    =   6765
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12615
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   14.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   6765
   ScaleWidth      =   12615
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picFlightDetails 
      Align           =   1  'Align Top
      BorderStyle     =   0  'None
      Height          =   975
      Left            =   0
      ScaleHeight     =   975
      ScaleWidth      =   12615
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   0
      Width           =   12615
   End
   Begin VB.Timer tmrTimer 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   0
      Top             =   4200
   End
   Begin VB.PictureBox picSep 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   60
      Left            =   0
      ScaleHeight     =   60
      ScaleWidth      =   12615
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   6705
      Width           =   12615
   End
   Begin MSComctlLib.ImageList imlDelays 
      Left            =   6960
      Top             =   1680
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":0000
            Key             =   "new"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":039A
            Key             =   "insert"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":0734
            Key             =   "delete"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":0ACE
            Key             =   "save"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":0E68
            Key             =   "print"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":1202
            Key             =   "excel"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":159C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":1936
            Key             =   "telex"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":1CD0
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "DelaysDialog.frx":206A
            Key             =   "close"
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB tabDelays 
      Height          =   4095
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   7223
      _StockProps     =   64
      FontSize        =   8
      FontName        =   "MS Sans Serif"
   End
   Begin TABLib.TAB tabHelper 
      Height          =   4095
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   7223
      _StockProps     =   64
      FontSize        =   8
      FontName        =   "MS Sans Serif"
   End
   Begin MSComctlLib.Toolbar tbrDelays 
      Align           =   2  'Align Bottom
      Height          =   360
      Index           =   1
      Left            =   0
      TabIndex        =   2
      Tag             =   "2,1"
      Top             =   6345
      Visible         =   0   'False
      Width           =   12615
      _ExtentX        =   22251
      _ExtentY        =   635
      ButtonWidth     =   1640
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      ImageList       =   "imlDelays"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   11
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Print"
            Key             =   "print"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "print_sep"
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Excel"
            Key             =   "excel"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "excel_sep"
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "PDF"
            Key             =   "pdf"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "pdf_sep"
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Telex"
            Key             =   "telex"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "telex_sep"
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "On Top"
            Key             =   "top"
            ImageIndex      =   9
            Style           =   1
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "top_sep"
            Style           =   3
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Close"
            Key             =   "close"
            ImageIndex      =   10
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.Toolbar tbrDelays 
      Align           =   2  'Align Bottom
      Height          =   360
      Index           =   0
      Left            =   0
      TabIndex        =   3
      Tag             =   "1,1"
      Top             =   5985
      Width           =   12615
      _ExtentX        =   22251
      _ExtentY        =   635
      ButtonWidth     =   1535
      ButtonHeight    =   582
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      ImageList       =   "imlDelays"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Add"
            Key             =   "add"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "add_sep"
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Insert"
            Key             =   "insert"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "insert_sep"
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Delete"
            Key             =   "delete"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "delete_sep"
            Style           =   3
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Save"
            Key             =   "save"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "save_sep"
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "info"
            Style           =   4
            Object.Width           =   2775
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin VB.PictureBox picInfo 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   360
         Left            =   6840
         ScaleHeight     =   360
         ScaleWidth      =   2775
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   0
         Width           =   2775
         Begin VB.Label lblInfo 
            Caption         =   "Right Click on Code to get choice list!"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   0
            TabIndex        =   5
            Top             =   75
            Width           =   2775
         End
      End
   End
   Begin TABLib.TAB tabSaved 
      Height          =   4095
      Index           =   0
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Visible         =   0   'False
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   7223
      _StockProps     =   64
      FontSize        =   8
      FontName        =   "MS Sans Serif"
   End
   Begin TABLib.TAB tabDelays 
      Height          =   4095
      Index           =   1
      Left            =   0
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   0
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   7223
      _StockProps     =   64
      FontSize        =   8
      FontName        =   "MS Sans Serif"
   End
   Begin TABLib.TAB tabSaved 
      Height          =   4095
      Index           =   1
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Visible         =   0   'False
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   7223
      _StockProps     =   64
      FontSize        =   8
      FontName        =   "MS Sans Serif"
   End
   Begin MSComctlLib.TabStrip tbsDelays 
      Height          =   5295
      Left            =   4680
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   240
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   9340
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "DelaysDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const NOT_FOUND_MSG = "Error! Delay code not found in basic data"
Private Const DEFAULT_FORE_COLOR = vbBlack
Private Const DEFAULT_BACK_COLOR = vbWhite
Private Const WARNING_FORE_COLOR = vbRed
Private Const WARNING_BACK_COLOR = vbWhite
Private Const NEW_FORE_COLOR = vbBlack
Private Const NEW_BACK_COLOR = vbYellow

Private PDFExport As ActiveReportsPDFExport.ARExportPDF
Private ExcelExport As ActiveReportsExcelExport.ARExportExcel

Private FlightUrno As String

Private strDataFormat(1) As String
Private strBoolColumns(1) As String
Private m_blnReadOnly As Boolean

Private rsAft As ADODB.Recordset
Private rsDelayCodes As ADODB.Recordset
Private rsDelayInfo As ADODB.Recordset
Private rsDelayHist As ADODB.Recordset
Private rsTelexes As ADODB.Recordset
Private rsSeason As ADODB.Recordset
    
Private IsOnEdit As Boolean
Private lngCurrLine As Long
Private lngCurrCol As Long
Private OldValue As Variant

Private CountColumnExists As Boolean 'igu on 19/05/2011

Private FormToShow As Form

Public Sub LoadData(ByVal strUrno As String)
    Dim blnFLightFound As Boolean
    
    blnFLightFound = (strUrno <> "")
    If blnFLightFound Then
        rsAft.Find "URNO = " & strUrno, , , 1
        blnFLightFound = Not (rsAft.EOF)
    End If
    
    If blnFLightFound Then
        FlightUrno = strUrno
        
        Call DisplayDelays(rsAft)
    Else
        FlightUrno = ""
        
        Call DisplayDelays(Nothing)
    End If
End Sub

Public Sub RefreshDisplay(ByVal CedaCmd As String, ByVal TableName As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
    Dim vntDataSources As Variant
    Dim vntDataSource As Variant
    
    If FlightUrno = Urno Then
        If CedaCmd = "SBC" Then
            If ChangedFields <> oUfisServer.GetMyWorkStationName & ",CDID," & LoginUserName & "," & App.hInstance Then
                'refresh memory table
                vntDataSources = Split(pstrDataSource, ",")
                For Each vntDataSource In vntDataSources
                    If vntDataSource <> "AFTTAB" Then
                        Call AddDataSource(MyMainForm.AppType, colDataSources, vntDataSource, colParameters)
                    End If
                Next vntDataSource
                
                Set rsDelayInfo = colDataSources.Item("DCFTAB")
                Set rsDelayHist = colDataSources.Item("DCMTAB")
                
                Call PrepareTab
                Call DisplayDelays(rsAft)
                Call Form_Activate
            End If
        End If
    End If
End Sub

Private Sub EnableSaveButton(ByVal Enabled As Boolean)
    tbrDelays(0).Buttons("save").Enabled = Enabled
End Sub

Private Sub PrintDelays(ByVal Index As Integer, ByVal Media As String)
    Dim rpt As New PrintDelays
    Dim colFlightDetails As Collection
    Dim lbl As Label
    Dim strFileName As String
    Dim strDefPath As String
    Dim strDefFile As String
    
    If pblnShowFlightDetails Then
        Set colFlightDetails = New Collection
        
        For Each lbl In FlightDetails.lblArr
            If FlightDetails.optSelFlight(0).Value Then
                colFlightDetails.Add lbl.Caption, lbl.Name & CStr(lbl.Index)
            Else
                colFlightDetails.Add "", lbl.Name & CStr(lbl.Index)
            End If
        Next lbl
        For Each lbl In FlightDetails.lblDep
            If FlightDetails.optSelFlight(1).Value Or lbl.Tag = "SHARED" Then
                colFlightDetails.Add lbl.Caption, lbl.Name & CStr(lbl.Index)
            Else
                colFlightDetails.Add "", lbl.Name & CStr(lbl.Index)
            End If
        Next lbl
    End If
    
    Load rpt
    rpt.InitReport tabDelays(Index), colFlightDetails, Media
    If Media = "print" Then
        rpt.Show , Me
        'rpt.Refresh
    Else
        rpt.Visible = False
        If pblnIsOnTop Then
            'SetFormOnTop MyMainForm, False
            SetFormOnTop Me, False
        End If

        strDefPath = GetConfigEntry(colConfigs, "FDLY", "EXPORT_" & UCase(Media) & "_PATH", App.Path)
        If Dir(strDefPath, vbDirectory) = "" Then
            If Not CreateFolder(strDefPath) Then
                MsgBox "Folder " & strDefPath & " could not be created!", vbCritical, Me.Caption
                Exit Sub
            End If
        End If
        
        strDefFile = "Delays_" & Format(Now, "YYYYMMDDhhmmss")
        strFileName = GetFileName(MyMainForm.cdgSave, strDefPath, strDefFile, Media)
        If strFileName <> "" Then
            If Dir(strFileName) <> "" Then
                On Error Resume Next
                Err.Number = 0
                Kill strFileName
                If Err.Number <> 0 Then
                    MsgBox "Could not overwrite file " & strFileName & vbNewLine & Err.Description, vbCritical, Me.Caption
                    Err.Number = 0
                    Exit Sub
                End If
                On Error GoTo 0
            End If
            
            rpt.Run False

            Select Case Media
                Case "excel"
                    Set ExcelExport = New ActiveReportsExcelExport.ARExportExcel
                    ExcelExport.FileName = strFileName
                    ExcelExport.Export rpt.Pages

                    Set ExcelExport = Nothing
                Case "pdf"
                    Set PDFExport = New ActiveReportsPDFExport.ARExportPDF
                    PDFExport.FileName = strFileName
                    PDFExport.Export rpt.Pages

                    Set PDFExport = Nothing
            End Select
        End If

        If pblnIsOnTop Then
            'SetFormOnTop MyMainForm, pblnIsOnTop
            SetFormOnTop Me, pblnIsOnTop
        End If

        Unload rpt
    End If
    
    Set rpt = Nothing
End Sub

Private Sub ShowTelex(ByVal Index As Integer)
    Dim intLineNo As Integer
    Dim strTelexUrno As String
    Dim strTelexMsg As String

    intLineNo = tabDelays(Index).GetCurrentSelected
    strTelexUrno = Trim(tabDelays(Index).GetFieldValue(intLineNo, "TURN"))
    If Trim(strTelexUrno <> "") Then
        If rsTelexes Is Nothing Then
            Call RetrieveTelex(rsTelexes, strTelexUrno)
        Else
            rsTelexes.Find "URNO = " & strTelexUrno, , , 1
            If rsTelexes.EOF Then
                Call RetrieveTelex(rsTelexes, strTelexUrno)
            End If
        End If
        If Not (rsTelexes Is Nothing) Then
            rsTelexes.Find "URNO = " & strTelexUrno, , , 1
            If Not rsTelexes.EOF Then
                strTelexMsg = rsTelexes.Fields("TXT1").Value & rsTelexes.Fields("TXT2").Value
            End If
        End If
        
        Call DisplayTelex(strTelexMsg)
    End If
End Sub

Private Sub RetrieveTelex(rsTelexes As ADODB.Recordset, ByVal Urno As String)
    Dim strOnDemandDataSource As String
    Dim strDataSource As String
    Dim colLocalParams As CollectionExtended
    
    strOnDemandDataSource = GetConfigEntry(colConfigs, "FDLY", "ON_DEMAND_DATA_SOURCE", "")
    If InStr(strOnDemandDataSource & ",", "TLXTAB" & ",") > 0 Then
        strDataSource = "TLXTAB"
    End If
    If strDataSource <> "" Then
        Set colLocalParams = New CollectionExtended
        colLocalParams.Add Urno, "TelexUrno"
        
        Call AddDataSource(MyMainForm.AppType, colDataSources, strDataSource, colLocalParams, , , , , False)
        
        If rsTelexes Is Nothing Then
            Set rsTelexes = colDataSources.Item("TLXTAB")
        End If
    End If
End Sub

Private Sub DisplayTelex(ByVal TelexMessage As String)
    Dim oTextDisplay As New TextDisplay

    TelexMessage = Replace(TelexMessage, Chr(10), vbNewLine)

    Load oTextDisplay
    oTextDisplay.DataSource = TelexMessage
    Set oTextDisplay.Icon = imlDelays.ListImages.Item("telex").Picture
    oTextDisplay.Caption = "Telex of Selected Entry"
    oTextDisplay.IsOnTop = pblnIsOnTop
    oTextDisplay.Show , Me
End Sub

Private Sub PrepareTab()
    Dim i As Integer
    Dim strActualFieldList(1) As String
    Dim strLogicalFieldList(1) As String
    Dim strHeaderString(1) As String
    Dim strHeaderLengthString(1) As String
    Dim strHeaderAlignmentString(1) As String
    Dim strColumnAlignmentString(1) As String
    Dim strMainHeaderLengthString(1) As String
    Dim strMainHeaderCaption(1) As String
    Dim strReadOnlyColumns(1) As String
    Dim vntBoolColumns As Variant
    Dim strPrintColumns(1) As String
    Dim blnReadOnly As Boolean
    Dim TabObject As TABLib.Tab
    Dim strAdd As String
    
    strActualFieldList(0) = "DCFTAB.DECN|DENTAB.DECN,DCFTAB.DECA|DENTAB.DECA,DCFTAB.DECS|DENTAB.DECS,DCFTAB.READ,DCFTAB.DURA,DCFTAB.MEAN|DENTAB.DENA,DCFTAB.TURN"
    strLogicalFieldList(0) = "DECN,DECA,DECS,READ,DURA,MEAN,TURN"
    strHeaderString(0) = "Nu,Al,SC,Read,Duration,Description,Telex Urno"
    strHeaderLengthString(0) = "30,30,30,40,80,500,0"
    strHeaderAlignmentString(0) = "C,C,C,C,C,L,L"
    strColumnAlignmentString(0) = strHeaderAlignmentString(0)
    strMainHeaderLengthString(0) = "3,4"
    strMainHeaderCaption(0) = "Delay Code,"
    strReadOnlyColumns(0) = "5,6"
    strBoolColumns(0) = "3"
    strPrintColumns(0) = "0,1,2,3,4,5"
    strDataFormat(0) = ""
    
    For i = 0 To UBound(strActualFieldList)
        If i = 0 Then
            strAdd = ""
        Else
            strAdd = "HIST_"
        End If
    
        strActualFieldList(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "ACTFLDLIST", strActualFieldList(0))
        strLogicalFieldList(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "LOGFLDLIST", strLogicalFieldList(0))
        strHeaderString(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "HDRSTRING", strHeaderString(0))
        strHeaderLengthString(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "HDRLENGTH", strHeaderLengthString(0))
        strHeaderAlignmentString(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "HDRALIGNMENT", strHeaderAlignmentString(0))
        strColumnAlignmentString(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "COLALIGNMENT", strColumnAlignmentString(0))
        strMainHeaderLengthString(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "MAINHEADERLENGTH", strMainHeaderLengthString(0))
        strMainHeaderCaption(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "MAINHEADERCAPTION", strMainHeaderCaption(0))
        strReadOnlyColumns(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "READONLYCOLUMNS", strReadOnlyColumns(0))
        strBoolColumns(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "BOOLCOLUMNS", strBoolColumns(0))
        strPrintColumns(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "PRINTCOLUMNS", strPrintColumns(0))
        strDataFormat(i) = GetConfigEntry(colConfigs, "FDLY", "TAB_" & strAdd & "DATAFORMAT", strDataFormat(0))
    Next i
    
    For Each TabObject In tabDelays
        With TabObject
            .ResetContent
            .Tag = strActualFieldList(TabObject.Index)
            .myTag = strPrintColumns(TabObject.Index)
            .LogicalFieldList = strLogicalFieldList(TabObject.Index)
            .HeaderString = strHeaderString(TabObject.Index)
            .HeaderLengthString = strHeaderLengthString(TabObject.Index)
            .HeaderAlignmentString = strHeaderAlignmentString(TabObject.Index)
            .ColumnAlignmentString = strColumnAlignmentString(TabObject.Index)
            .SetMainHeaderValues strMainHeaderLengthString(TabObject.Index), strMainHeaderCaption(TabObject.Index), ""
            .NoFocusColumns = strReadOnlyColumns(TabObject.Index)
            .SetMainHeaderFont .FontSize, False, False, True, 0, .FontName
            .MainHeader = True
            .PostEnterBehavior = 3
            .InplaceEditSendKeyEvents = True
            '.CreateDecorationObject "BottomBorder", "B", "1", CStr(vbBlack)
            '.CreateCellObj "UserEntry", LightestBlue, vbBlack, 0, False, False, False, 0, ""
            
            vntBoolColumns = Split(strBoolColumns(TabObject.Index), ",")
            For i = 0 To UBound(vntBoolColumns)
                .SetColumnBoolProperty vntBoolColumns(i), "Y", "N"
                
                blnReadOnly = (InStr("," & strReadOnlyColumns(TabObject.Index) & ",", "," & vntBoolColumns(i) & ",") > 0)
                .SetBoolPropertyReadOnly vntBoolColumns(i), blnReadOnly Or m_blnReadOnly
            Next i
    
            .ShowHorzScroller True
            .ShowVertScroller True
        End With
    Next TabObject
    
    'tabHelper
    With tabHelper
        .ResetContent
        .LogicalFieldList = tabDelays(0).LogicalFieldList
        .HeaderString = tabDelays(0).HeaderString
    End With
    
    'tab Saved
    For Each TabObject In tabSaved
        With TabObject
            .ResetContent
            .LogicalFieldList = tabDelays(TabObject.Index).LogicalFieldList & ",#FLAG#"
            .HeaderString = tabDelays(TabObject.Index).HeaderString & ",#FLAG#"
'            .SetMainHeaderValues strMainHeaderLengthString, strMainHeaderCaption, ""
'            .SetMainHeaderFont .FontSize, False, False, True, 0, .FontName
        End With
    Next TabObject
End Sub

Private Sub PrepareTabstrip()
    tbsDelays.Tabs(1).Caption = "Delays"
    tbsDelays.Tabs.Add , , "Delays' History"
End Sub

Private Sub PrepareToolbar()
    Dim blnAllowUpdate As Boolean
    Dim blnAllowDelete As Boolean
    Dim blnAllowChanges As Boolean
    Dim blnAllowPrint As Boolean
    Dim blnAllowExcel As Boolean
    Dim blnAllowPDF As Boolean
    Dim blnAllowExport As Boolean
    Dim blnAllowTelex As Boolean
    Dim blnShowToolbar As Boolean
    
    blnAllowUpdate = (GetConfigEntry(colConfigs, "FDLY", "ALLOW_UPDATE", "NO") = "YES")
    blnAllowDelete = (GetConfigEntry(colConfigs, "FDLY", "ALLOW_DELETE", "NO") = "YES")
    blnAllowPrint = (GetConfigEntry(colConfigs, "FDLY", "ALLOW_PRINT", "NO") = "YES")
    blnAllowExcel = (GetConfigEntry(colConfigs, "FDLY", "ALLOW_EXCEL", "NO") = "YES")
    blnAllowPDF = (GetConfigEntry(colConfigs, "FDLY", "ALLOW_PDF", "NO") = "YES")
    blnAllowTelex = (GetConfigEntry(colConfigs, "FDLY", "ALLOW_SHOW_TELEX", "NO") = "YES")
    
    blnAllowChanges = blnAllowUpdate Or blnAllowDelete
    m_blnReadOnly = Not blnAllowUpdate
    blnAllowExport = blnAllowPrint Or blnAllowExcel Or blnAllowPDF
    
    tbrDelays(0).Buttons("add").Visible = blnAllowUpdate
    tbrDelays(0).Buttons("add_sep").Visible = blnAllowUpdate
    tbrDelays(0).Buttons("insert").Visible = blnAllowUpdate
    tbrDelays(0).Buttons("insert_sep").Visible = blnAllowUpdate
    tbrDelays(0).Buttons("delete").Visible = blnAllowDelete
    tbrDelays(0).Buttons("delete_sep").Visible = blnAllowDelete
    tbrDelays(1).Buttons("print").Visible = blnAllowPrint
    tbrDelays(1).Buttons("print_sep").Visible = blnAllowPrint
    tbrDelays(1).Buttons("excel").Visible = blnAllowExcel
    tbrDelays(1).Buttons("excel_sep").Visible = blnAllowExcel
    tbrDelays(1).Buttons("pdf").Visible = blnAllowPDF
    tbrDelays(1).Buttons("pdf_sep").Visible = blnAllowPDF
    tbrDelays(1).Buttons("telex").Visible = blnAllowTelex
    tbrDelays(1).Buttons("telex_sep").Visible = blnAllowTelex
    tbrDelays(1).Buttons("top").Visible = Not pblnShowRightButtons
    tbrDelays(1).Buttons("top_sep").Visible = Not pblnShowRightButtons
    tbrDelays(1).Buttons("close").Visible = Not pblnShowRightButtons
    
    blnShowToolbar = blnAllowExport Or blnAllowTelex Or (Not pblnShowRightButtons)
    tbrDelays(0).Visible = blnAllowChanges
    tbrDelays(1).Visible = blnShowToolbar
    
    With tbrDelays(0)
        picInfo.ZOrder
        picInfo.Move .Buttons("info").Left, .Buttons("info").Top, .Buttons("info").Width
    End With
    
    tbrDelays(1).Buttons("top").Value = IIf(pblnIsOnTop, tbrPressed, tbrUnpressed)
End Sub

Private Sub DisplayDelays(rsAft As ADODB.Recordset)
    Dim strValue As String
    Dim strDesc As String
    Dim strMainCode As String
    Dim i As Integer
    Dim LineNo As Integer
    'Dim LineCount As Integer 'igu on 15/03/2011
    Dim blnLink As Boolean
    Dim blnSearch As Boolean
    Dim strTable As String
    Dim strFields As String
    Dim intFieldsCount As Integer
    Dim strData As String
    Dim TabObject As TABLib.Tab
    Dim rsDelayRef As ADODB.Recordset
    Dim Index As Integer
    
    'igu on 15/03/2011
    'change the way of sorting
    Dim intDECNColNo As Integer
    Dim intDECAColNo As Integer
    Dim intDECSColNo As Integer
    Dim intUSECColNo As Integer
    Dim strMainCols As String, strCols As String
    Dim strValues As String
    Dim strLines As String
    Dim vntLines As Variant, vntSubLines As Variant
    Dim intLineIndex As Integer
    Dim intDelCount As Integer
    'end of change the way of sorting
    'igu on 15/03/2011
    
    'empty the delays grid
    For Each TabObject In tabDelays
        Do While TabObject.GetLineCount > 0
            TabObject.DeleteLine 0
        Loop
    Next TabObject
    
    For Each TabObject In tabSaved
        Do While TabObject.GetLineCount > 0
            TabObject.DeleteLine 0
        Loop
    Next TabObject
    
    For Each TabObject In tabDelays
        Index = TabObject.Index
        If Index = 0 Then
            Set rsDelayRef = rsDelayInfo
        Else
            Set rsDelayRef = rsDelayHist
        End If
        
        If Not (rsAft Is Nothing) Then
            If Not (rsAft.BOF And rsAft.EOF) Then
                rsAft.MoveFirst
            End If
                    
            blnLink = (InStr(tabDelays(Index).Tag, "DENTAB.") > 0)
            
            rsDelayRef.Find "FURN = " & rsAft.Fields("URNO").Value, , , 1
            Do While Not rsDelayRef.EOF
                If rsDelayRef.Fields("FURN").Value <> rsAft.Fields("URNO").Value Then
                    Exit Do
                End If
                
                blnSearch = blnLink And (rsDelayRef.Fields("MEAN").Value = "")
                            
                If blnSearch Then
                    rsDelayCodes.Find "URNO = " & rsDelayRef.Fields("DURN").Value, , , 1
                End If
                
                strValue = GetTextLine(colDataSources, tabDelays(Index).Tag, blnSearch, strDataFormat(Index))
                
                If Index = 0 Then
                    'insert the data into tabHelper
                    TabInsertTextLine tabHelper, strValue, False
                    If rsDelayRef.Fields("DURN").Value = 0 Then
                        tabHelper.SetLineTag tabHelper.GetLineCount - 1, "DURN=0"
                    End If
                Else
                    'insert the data directly into tabDelays
                    TabInsertTextLine TabObject, strValue, False
                    If rsDelayRef.Fields("DURN").Value = 0 Then
                        TabObject.SetLineColor TabObject.GetLineCount - 1, WARNING_FORE_COLOR, WARNING_BACK_COLOR
                    End If
                    'save the copy into tabSaved
                    Call MoveTabData(TabObject, tabSaved(Index), TabObject.GetLineCount - 1, False)
                End If
                
                rsDelayRef.MoveNext
            Loop
            
            If Index = 0 Then
                'Copy data from tabHelper to tabDelays with specific order
                If tabHelper.GetLineCount > 0 Then
                    If RTrim(tabHelper.GetFieldValue(0, "USEQ")) <> "" Then
                        'sort by USEQ
                        tabHelper.Sort GetRealItemNo(tabHelper.LogicalFieldList, "USEQ"), True, True
                        Do While tabHelper.GetLineCount > 0
                            Call MoveTabData(tabHelper, tabDelays(Index), 0, False)
                            Call MoveTabData(tabHelper, tabSaved(Index), 0, True)
                        Loop
                    Else
                        'igu on 15/03/2011
                        'change the way of sorting
                        
'                        Do While tabHelper.GetLineCount > 0
'                            LineNo = 0
'                            LineCount = tabHelper.GetLineCount - 2
'
'                            strMainCode = Trim(tabHelper.GetFieldValues(LineNo, "DECN,DECA,USEC"))
'
'                            Call MoveTabData(tabHelper, tabDelays(Index), LineNo, False, True)
'                            Call MoveTabData(tabHelper, tabSaved(Index), LineNo, True, True)
'
'                            Do While LineNo <= LineCount
'                                If strMainCode = Trim(tabHelper.GetFieldValues(LineNo, "DECN,DECA,USEC")) Then
'                                    Call MoveTabData(tabHelper, tabDelays(Index), LineNo, False, True)
'                                    Call MoveTabData(tabHelper, tabSaved(Index), LineNo, True, True)
'
'                                    LineNo = LineNo - 1
'                                    LineCount = LineCount - 1
'                                End If
'
'                                LineNo = LineNo + 1
'                            Loop
'
''                            'Grouping the main delay code by drawing line
''                            Call DrawTabBottomLine(tabDelays, tabDelays(Index).GetLineCount - 1)
''                            'set the line status to 1
''                            tabDelays(Index).SetLineStatusValue tabDelays(Index).GetLineCount - 1, 1
'
'                            'update USEQ in database
'                            strTable = "DCFTAB"
'                            strFields = "USEQ"
'                            intFieldsCount = UBound(Split(strFields, ",")) + 1
'                            strData = ""
'                            For LineNo = 0 To tabDelays(Index).GetLineCount - 1
'                                strData = strData & "*CMD*," & strTable & ",URT," & CStr(intFieldsCount) & "," & _
'                                    strFields & ",[URNO=:VURNO]" & vbLf
'                                strData = strData & tabDelays(Index).GetFieldValues(LineNo, strFields) & "," & _
'                                    tabDelays(Index).GetFieldValues(LineNo, "URNO") & vbLf
'                            Next LineNo
'                            Call InsertUpdateRecords(strTable, strFields, strData)
'                        Loop
                        
                        intDECNColNo = GetRealItemNo(tabHelper.LogicalFieldList, "DECN")
                        intDECAColNo = GetRealItemNo(tabHelper.LogicalFieldList, "DECA")
                        intDECSColNo = GetRealItemNo(tabHelper.LogicalFieldList, "DECS")
                        intUSECColNo = GetRealItemNo(tabHelper.LogicalFieldList, "USEC")
                        
                        strMainCols = CStr(intDECNColNo) & "," & CStr(intDECAColNo) & "," & _
                            CStr(intDECSColNo) & "," & CStr(intUSECColNo)
                        strCols = CStr(intDECNColNo) & "," & CStr(intDECAColNo) & "," & _
                            CStr(intUSECColNo)
                            
                        Do While tabHelper.GetLineCount > 0
                            LineNo = 0
                        
                            strValues = Trim(tabHelper.GetFieldValues(LineNo, "DECN,DECA")) & _
                                ",," & Trim(tabHelper.GetFieldValues(LineNo, "USEC"))
                            
                            strLines = tabHelper.GetLinesByMultipleColumnValue(strMainCols, strValues, 0)
                            If strLines = "" Then
                                Call MoveTabData(tabHelper, tabDelays(Index), LineNo, False, True)
                                Call MoveTabData(tabHelper, tabSaved(Index), LineNo, True, True)
                            Else
                                vntLines = Split(strLines, ",")
                                
                                LineNo = Val(vntLines(0))
                                
                                strValues = Trim(tabHelper.GetFieldValues(LineNo, "DECN,DECA,USEC"))
                                
                                Call MoveTabData(tabHelper, tabDelays(Index), LineNo, False, True)
                                Call MoveTabData(tabHelper, tabSaved(Index), LineNo, True, True)
                                
                                strLines = tabHelper.GetLinesByMultipleColumnValue(strCols, strValues, 0)
                                If strLines <> "" Then
                                    vntSubLines = Split(strLines, ",")
                                    intDelCount = 0
                                    
                                    For intLineIndex = 0 To UBound(vntSubLines)
                                        LineNo = Val(vntSubLines(intLineIndex)) - intDelCount
                                        
                                        If Trim(tabHelper.GetFieldValue(LineNo, "DECS")) <> "" Then
                                            Call MoveTabData(tabHelper, tabDelays(Index), LineNo, False, True)
                                            Call MoveTabData(tabHelper, tabSaved(Index), LineNo, True, True)
                                            
                                            If UBound(vntLines) > 0 Then
                                                Exit For
                                            Else
                                                intDelCount = intDelCount + 1
                                            End If
                                        End If
                                    Next intLineIndex
                                End If
                            End If
                        Loop

                        'update USEQ in database
                        strTable = "DCFTAB"
                        strFields = "USEQ"
                        intFieldsCount = UBound(Split(strFields, ",")) + 1
                        strData = ""
                        For LineNo = 0 To tabDelays(Index).GetLineCount - 1
                            strData = strData & "*CMD*," & strTable & ",URT," & CStr(intFieldsCount) & "," & _
                                strFields & ",[URNO=:VURNO]" & vbLf
                            strData = strData & tabDelays(Index).GetFieldValues(LineNo, strFields) & "," & _
                                tabDelays(Index).GetFieldValues(LineNo, "URNO") & vbLf
                        Next LineNo
                        Call InsertUpdateRecords(strTable, strFields, strData)
                        
                        'end of change the way of sorting
                        'igu on 15/03/2011
                    End If
                End If
            End If
        End If
        
        TabObject.Refresh
    Next TabObject
    
    Call EnableSaveButton(False)
    
    'igu on 19/05/2011
    If CountColumnExists Then Call FillInCountColumn
End Sub

'igu on 19/05/2011
Private Sub FillInCountColumn()
    Dim i As Integer
    Dim intCount As Integer
    Dim strCount As String
    
    intCount = 0
    With tabDelays(0)
        For i = 0 To .GetLineCount - 1
            If Trim(.GetFieldValue(i, "DECS")) = "" Then
                intCount = intCount + 1
                
                strCount = CStr(intCount)
            Else
                strCount = " "
            End If
            
            .SetFieldValues i, "COUN", strCount
        Next i
    End With
End Sub

Private Sub MoveTabData(TabSource As TABLib.Tab, TabDest As TABLib.Tab, ByVal LineNo As Integer, _
Optional ByVal DeleteSource As Boolean = True, Optional ByVal AlwaysReorder As Boolean = False)
    TabDest.InsertTextLine TabSource.GetFieldValues(LineNo, TabSource.LogicalFieldList), False
    'insert the sequence number
    If AlwaysReorder Or Trim(TabDest.GetFieldValue(TabDest.GetLineCount - 1, "USEQ")) = "" Then
        TabDest.SetFieldValues TabDest.GetLineCount - 1, "USEQ", TabDest.GetLineCount
    End If
    If TabSource.GetLineTag(LineNo) = "DURN=0" Then
        TabDest.SetLineColor TabDest.GetLineCount - 1, WARNING_FORE_COLOR, WARNING_BACK_COLOR
    End If
    If DeleteSource Then
        TabSource.DeleteLine LineNo
    End If
End Sub

Private Sub DrawTabBottomLine(TabObject As TABLib.Tab, ByVal LineNo As Integer)
    Dim i As Integer

    For i = 0 To TabObject.GetColumnCount - 1
        TabObject.SetDecorationObject LineNo, i, "BottomBorder"
    Next i
End Sub

Private Function GetTextLine(colDataSources As CollectionExtended, ByVal ActFields As String, _
ByVal UseAlt As Boolean, ByVal DataFormat As String, Optional ByVal Separator As String = vbTab) As String
    Dim vntActFields As Variant
    Dim vntActField As Variant
    Dim vntDataFormat As Variant
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    Dim strValue As String
    Dim strReturn As String
    Dim i As Integer
    
    strReturn = ""
    vntActFields = Split(ActFields, ",")
    vntDataFormat = Split(DataFormat, ",")
    i = 0
    For Each vntActField In vntActFields
        If InStr(vntActField, "|") > 0 Then
            If UseAlt Then
                vntActField = Split(vntActField, "|")(1)
            Else
                vntActField = Split(vntActField, "|")(0)
            End If
        End If
        
        vntTableField = Split(vntActField, ".")
        strTable = vntTableField(0)
        strField = vntTableField(1)
        
        strValue = ""
        'igu on 19/05/2011
        'check for virtual table
        If strTable = "#" Then
            'virtual fields, for future implement
            Select Case strField
                Case "COUN"
                    If Not CountColumnExists Then CountColumnExists = True
            End Select
        Else
            With colDataSources.Item(strTable)
                If Not (.BOF Or .EOF) Then
                    If Not IsNull(.Fields(strField).Value) Then
                        If .Fields(strField).Type = adDate Then
                            strValue = UTCDateToLocal(.Fields(strField).Value, UtcTimeDiff)
                        Else
                            strValue = .Fields(strField).Value
                        End If
                    
                        strValue = GetFormattedData(strValue, vntDataFormat(i))
                    End If
                End If
            End With
        End If
        
        strReturn = strReturn & Separator & strValue
        i = i + 1
    Next vntActField
    strReturn = Mid(strReturn, 2)
    
    GetTextLine = strReturn
End Function

Private Sub StartEditMode(ByVal Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If IsColumnEditable(Index, ColNo) Then
        lngCurrLine = LineNo
        lngCurrCol = ColNo
        
        'Save the old value and Aft Urno
        OldValue = tabDelays(Index).GetColumnValue(LineNo, ColNo)

        'select the line
        If tabDelays(Index).GetCurrentSelected <> LineNo Then
            tabDelays(Index).SetCurrentSelection LineNo
        End If

        tabDelays(Index).EnableInlineEdit True
        tabDelays(Index).SetInplaceEdit lngCurrLine, lngCurrCol, False
        IsOnEdit = True
    End If
End Sub

Private Sub ValidateUserEntry(ByVal Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim NewValue As Variant
    Dim strField As String
    Dim strValue As String
    Dim blnValid As Boolean
    Dim strSavedLineText As String
    Dim lngSavedForeColor As Long
    Dim lngSavedBackColor As Long
    Dim strLineText As String
    Dim lngForeColor As Long
    Dim lngBackColor As Long
    Dim i As Integer
    Dim blnFound As Boolean
    Dim blnChanged As Boolean
    
    tabDelays(Index).EnableInlineEdit False
    IsOnEdit = False
    
    NewValue = tabDelays(Index).GetColumnValue(LineNo, ColNo)
    
    blnValid = True
    strField = GetRealItem(tabDelays(Index).LogicalFieldList, ColNo, ",")
    blnChanged = (OldValue <> NewValue)
    
    Select Case strField
        Case "USEQ"
            If IsValidSeqNum(NewValue, 1, tabDelays(Index).GetLineCount) Then
                If NewValue <> OldValue Then
                    NewValue = Val(NewValue)
                    OldValue = Val(OldValue)
                    
                    strSavedLineText = tabDelays(Index).GetFieldValues(OldValue - 1, tabDelays(Index).LogicalFieldList)
                    tabDelays(Index).GetLineColor OldValue - 1, lngSavedForeColor, lngSavedBackColor
                    If NewValue > OldValue Then 'Shift up
                        For i = OldValue To NewValue - 1
                            strLineText = tabDelays(Index).GetFieldValues(i, tabDelays(Index).LogicalFieldList)
                            tabDelays(Index).GetLineColor i, lngForeColor, lngBackColor
                            
                            tabDelays(Index).SetFieldValues i - 1, tabDelays(Index).LogicalFieldList, strLineText
                            tabDelays(Index).SetFieldValues i - 1, "USEQ", i
                            tabDelays(Index).SetLineColor i - 1, lngForeColor, lngBackColor
                        Next i
                    Else 'Shift down
                        For i = OldValue - 1 To NewValue Step -1
                            strLineText = tabDelays(Index).GetFieldValues(i - 1, tabDelays(Index).LogicalFieldList)
                            tabDelays(Index).GetLineColor i - 1, lngForeColor, lngBackColor
                            
                            tabDelays(Index).SetFieldValues i, tabDelays(Index).LogicalFieldList, strLineText
                            tabDelays(Index).SetFieldValues i, "USEQ", i + 1
                            tabDelays(Index).SetLineColor i, lngForeColor, lngBackColor
                        Next i
                    End If
                    tabDelays(Index).SetFieldValues NewValue - 1, tabDelays(Index).LogicalFieldList, strSavedLineText
                    tabDelays(Index).SetFieldValues NewValue - 1, "USEQ", NewValue
                    tabDelays(Index).SetLineColor NewValue - 1, lngSavedForeColor, lngSavedBackColor
                    
                    tabDelays(Index).SetCurrentSelection NewValue - 1
                        
                    If Index = 0 And CountColumnExists Then Call FillInCountColumn 'igu on 19/05/2011
                        
                    tabDelays(Index).Refresh
                End If
            Else
                blnValid = False
            End If
        Case "ALC3", "DECN", "DECA", "DECS"
            If blnChanged Then
                If strField = "ALC3" Then
                    If Not (rsAft.BOF And rsAft.EOF) Then
                        rsAft.MoveFirst
            
                        If RTrim(NewValue) <> "" And RTrim(NewValue) <> rsAft.Fields("ALC3").Value Then
                            blnValid = False
                        End If
                    End If
                End If
            
                If blnValid Then
                    blnFound = False
                
                    rsDelayCodes.Filter = "ALC3 = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "ALC3")) & "' AND " & _
                        "DECN = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECN")) & "' AND " & _
                        "DECA = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECA")) & "' AND " & _
                        "DECS = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECS")) & "'"
                    If rsDelayCodes.EOF Then
                        rsDelayCodes.Filter = "DECN = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECN")) & "' AND " & _
                            "DECA = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECA")) & "' AND " & _
                            "DECS = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECS")) & "'"
                        If rsDelayCodes.EOF Then
                            If RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECN")) <> "" And _
                            RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECA")) = "" Then 'Only DECN provided
                                rsDelayCodes.Filter = "ALC3 = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "ALC3")) & "' AND " & _
                                    "DECN = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECN")) & "' AND " & _
                                    "DECS = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECS")) & "'"
                                If rsDelayCodes.EOF Then
                                    rsDelayCodes.Filter = "DECN = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECN")) & "' AND " & _
                                        "DECS = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECS")) & "'"
                                    blnFound = Not rsDelayCodes.EOF
                                Else
                                    blnFound = True
                                End If
                            ElseIf RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECN")) = "" And _
                            RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECA")) <> "" Then 'Only DECA provided
                                rsDelayCodes.Filter = "ALC3 = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "ALC3")) & "' AND " & _
                                    "DECA = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECA")) & "' AND " & _
                                    "DECS = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECS")) & "'"
                                If rsDelayCodes.EOF Then
                                    rsDelayCodes.Filter = "DECA = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECA")) & "' AND " & _
                                        "DECS = '" & RTrim(tabDelays(Index).GetFieldValue(LineNo, "DECS")) & "'"
                                    blnFound = Not rsDelayCodes.EOF
                                Else
                                    blnFound = True
                                End If
                            End If
                        Else
                            blnFound = True
                        End If
                    Else
                        blnFound = True
                    End If
                    
                    If blnFound Then
                        strValue = tabDelays(Index).GetFieldValues(LineNo, "DURN,ALC3,DECN,DECA,DECS,MEAN")
                        NewValue = rsDelayCodes.Fields("URNO").Value & "," & _
                            rsDelayCodes.Fields("ALC3").Value & "," & _
                            rsDelayCodes.Fields("DECN").Value & "," & _
                            rsDelayCodes.Fields("DECA").Value & "," & _
                            rsDelayCodes.Fields("DECS").Value & "," & _
                            Replace(rsDelayCodes.Fields("DENA").Value, ",", ";")
                        
                        If strValue <> NewValue Then
                            tabDelays(Index).SetFieldValues LineNo, "DURN,ALC3,DECN,DECA,DECS,MEAN", _
                                NewValue
                            If tabDelays(Index).GetFieldValue(LineNo, "URNO") <> "" Then
                                tabDelays(Index).SetLineColor LineNo, DEFAULT_FORE_COLOR, DEFAULT_BACK_COLOR
                            Else
                                tabDelays(Index).SetLineColor LineNo, DEFAULT_FORE_COLOR, NEW_BACK_COLOR
                            End If
                            
                            blnChanged = True
                            
                            If Index = 0 And CountColumnExists Then Call FillInCountColumn 'igu on 19/05/2011
                        End If
                    Else
                        tabDelays(Index).SetFieldValues LineNo, "DURN,MEAN", "0," & NOT_FOUND_MSG
                        If tabDelays(Index).GetFieldValue(LineNo, "URNO") <> "" Then
                            tabDelays(Index).SetLineColor LineNo, WARNING_FORE_COLOR, WARNING_BACK_COLOR
                        Else
                            tabDelays(Index).SetLineColor LineNo, WARNING_FORE_COLOR, NEW_BACK_COLOR
                        End If
                        
                        If Index = 0 And CountColumnExists Then Call FillInCountColumn 'igu on 19/05/2011
                    End If
                    tabDelays(Index).Refresh
                    
                    rsDelayCodes.Filter = adFilterNone
                End If
            End If
        Case "DURA"
            NewValue = CheckValidDuration(NewValue)
            If NewValue = "ERROR" Then
                blnValid = False
            Else
                If NewValue = "NULL" Then
                    NewValue = " "
                End If
            
                tabDelays(Index).SetColumnValue LineNo, ColNo, NewValue
                tabDelays(Index).Refresh
            End If
    End Select
    
    If blnValid Then
        If blnChanged Then
            Call EnableSaveButton(True)
        End If
    Else
        tabDelays(Index).SetColumnValue LineNo, ColNo, OldValue
    End If
End Sub

Private Function IsColumnEditable(ByVal Index As Integer, ByVal ColNo As Long, Optional ByVal CheckForBoolColumns As Boolean = True) As Boolean
    Dim blnReturn As Boolean
    
    blnReturn = (InStr("," & tabDelays(Index).NoFocusColumns & ",", "," & CStr(ColNo) & ",") <= 0)
    If CheckForBoolColumns Then
        If blnReturn Then
            blnReturn = (InStr("," & strBoolColumns(Index) & ",", "," & CStr(ColNo) & ",") <= 0)
        End If
    End If
    
    IsColumnEditable = blnReturn
End Function

Private Sub EmptyTabFlag(ByVal Index As Integer, ByVal LineNo As Integer)
    Dim i As Integer
    Dim j As Long
    Dim MaxLine As Long
    
    For i = 0 To Index
        If i = Index Then
            MaxLine = LineNo - 1
        Else
            MaxLine = tabSaved(i).GetLineCount - 1
        End If
        For j = 0 To MaxLine
            tabSaved(i).SetFieldValues j, "#FLAG#", ""
        Next j
    Next i
End Sub

Private Function GetFormattedData(ByVal ActValue As Variant, ByVal DataFormat As String) As String
    Dim strValue As String
    Dim dtmBaseDate As Date
    Dim vntDataFormat As Variant
    Dim strDataFormat As String
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    
    strValue = Trim(NVL(ActValue, ""))
    If Trim(DataFormat) <> "" Then
        If InStr(DataFormat, "|") > 0 Then
            vntDataFormat = Split(DataFormat, "|")
            strDataFormat = vntDataFormat(0)
            vntTableField = Split(vntDataFormat(1), ".")
            strTable = vntTableField(0)
            strField = vntTableField(1)
            
            If colDataSources.Exists(strTable) Then
                If colDataSources.Item(strTable).Fields(strField).Type = adDate Then
                    dtmBaseDate = UTCDateToLocal(colDataSources.Item(strTable).Fields(strField).Value, _
                        UtcTimeDiff)
                            
                    strValue = FullDateTimeToShortTime(dtmBaseDate, NVL(ActValue, CDate(0)))
                End If
            End If
        Else
            strValue = Format(strValue, DataFormat)
        End If
    End If
    
    GetFormattedData = strValue
End Function

Private Sub TabInsertTextLine(TabObject As TABLib.Tab, ByVal LineText As String, ByVal Redraw As Boolean, Optional ByVal Separator As String = vbTab)
    Dim vntLineText As Variant
    Dim i As Integer
    
    vntLineText = Split(LineText, Separator)
    TabObject.InsertTextLine "", Redraw
    For i = 0 To TabObject.GetColumnCount - 1
        TabObject.SetColumnValue TabObject.GetLineCount - 1, i, Replace(vntLineText(i), ",", ";")
    Next i
End Sub

Private Sub AddRecord(ByVal Index As Integer, ByVal NewLineAtBottom As Boolean)
    Dim intCurrLine As Integer
    Dim i As Integer

    With tabDelays(Index)
        If NewLineAtBottom Then
            .InsertTextLine "", False
            intCurrLine = .GetLineCount - 1
        Else
            intCurrLine = .GetCurrentSelected
            If intCurrLine < 0 Then intCurrLine = 0
            .InsertTextLineAt intCurrLine, "", False
        End If
        For i = intCurrLine To .GetLineCount - 1
            .SetFieldValues i, "USEQ", i + 1
        Next i

        .SetFieldValues intCurrLine, "DURN,DSRC", "0,USR"
        .SetCurrentSelection intCurrLine
        .SetLineColor intCurrLine, NEW_FORE_COLOR, NEW_BACK_COLOR
        
        .Refresh
    End With
    
    Call EnableSaveButton(True)
    
    'igu on 19/05/2011
    If Index = 0 And CountColumnExists Then
        Call FillInCountColumn
        tabDelays(Index).Refresh
    End If
End Sub

Private Sub DeleteRecord(ByVal Index As Integer)
    Dim intCurrLine As Integer
    Dim intLineCount As Integer
    Dim i As Integer
    Dim IsRecountNeeded As Boolean 'igu on 19/05/2011

    With tabDelays(Index)
        intCurrLine = .GetCurrentSelected
        intLineCount = .GetLineCount
        If intCurrLine > -1 And intCurrLine < intLineCount Then
            If MsgBox("Are you sure to delete current selected record?", _
                vbQuestion + vbYesNo + vbDefaultButton2, MyMainForm.Caption) = vbYes Then
            
                IsRecountNeeded = (Index = 0 And CountColumnExists And _
                    Trim(.GetFieldValue(intCurrLine, "DECS")) = "") 'igu on 19/05/2011
            
                .DeleteLine intCurrLine

                If intCurrLine < intLineCount - 1 Then
                    For i = intCurrLine To .GetLineCount - 1
                        .SetFieldValues i, "USEQ", i + 1
                    Next i
                    .SetCurrentSelection intCurrLine
                Else
                    If intLineCount > 1 Then
                        .SetCurrentSelection intCurrLine - 1
                    Else
                        .Refresh
                    End If
                End If
                
                Call EnableSaveButton(True)
            End If
        End If
    End With
    
    'igu on 19/05/2011
    If IsRecountNeeded Then Call FillInCountColumn
End Sub

Private Function SaveRecords() As Boolean
    Dim i As Long
    Dim strAftUrno As String
    Dim strUrno As String
    Dim lngUrnoCol As Long
    Dim strLineNo As String
    Dim lngLineNo As Long
    Dim strUrnoList As String
    Dim strUserAndTime As String
    Dim lngInsertCount As Long
    Dim strTable As String
    Dim strCommand As String
    Dim strFields As String
    Dim strInsFields As String
    Dim strUpdFields As String
    Dim strDelFields As String
    Dim strDataTemp As String
    Dim strData As String
    Dim strWhere As String
    Dim strResult As String
    Dim intInsFieldsCount As String
    Dim intUpdFieldsCount As String
    Dim intDelFieldsCount As String
    Dim fld As ADODB.Field
    Dim strFind As String
    Dim dtmNow As Date
    Dim TabObject As TABLib.Tab
    Dim Index As Integer
    
    If Not (rsAft.BOF And rsAft.EOF) Then
        rsAft.MoveFirst
        strAftUrno = rsAft.Fields("URNO").Value
    End If
    
    strFields = "DURN,USEQ,DECN,DECA,DECS,READ,DURA,MEAN,REMA,DSRC,ALC3"
    strInsFields = strFields & ",USEC,CDAT,FURN,URNO"
    strUpdFields = strFields & ",USEU,LSTU"
    strDelFields = ""
    For Each fld In rsDelayInfo.Fields
        strDelFields = strDelFields & "," & fld.Name
    Next fld
    If strDelFields <> "" Then strDelFields = Mid(strDelFields, 2)
    intInsFieldsCount = UBound(Split(strInsFields, ",")) + 1
    intUpdFieldsCount = UBound(Split(strUpdFields, ",")) + 1
    intDelFieldsCount = UBound(Split(strDelFields, ",")) + 1
    
    dtmNow = LocalDateToUTC(Now, colUTCTimeDiff, rsSeason)
    strUserAndTime = LoginUserName & "," & Format(dtmNow, "YYYYMMDDHHmmSS")
    
    strData = ""
    lngInsertCount = 0
    
    For Each TabObject In tabDelays
        Index = TabObject.Index
        If Index = 0 Then
            strTable = "DCFTAB"
        Else
            strTable = "DCMTAB"
        End If
    
        lngUrnoCol = GetRealItemNo(tabSaved(Index).LogicalFieldList, "URNO")
        'compare data in tabDelays with tabSaved
        For i = 0 To tabDelays(Index).GetLineCount - 1
            strUrno = tabDelays(Index).GetFieldValue(i, "URNO")
            If strUrno <> "" Then 'existing record
                strLineNo = tabSaved(Index).GetLinesByColumnValue(lngUrnoCol, strUrno, 0)
                If strLineNo = "" Then 'add new (just in case, by right, won't happen)
                    If tabDelays(Index).GetFieldValue(i, "DURN") = "0" Then 'invalid delay code
                        Call EmptyTabFlag(Index, i)
                    
                        If tbsDelays.SelectedItem.Index <> Index + 1 Then
                            Set tbsDelays.SelectedItem = tbsDelays.Tabs.Item(Index + 1)
                        End If
                        MsgBox "Invalid delay code found in line " & CStr(i + 1) & "!", vbInformation, Me.Caption
                        Exit Function
                    Else
                        lngInsertCount = lngInsertCount + 1
                        
                        strData = strData & "*CMD*," & strTable & ",IRT," & CStr(intInsFieldsCount) & "," & _
                            strInsFields & vbLf
                        strData = strData & tabDelays(Index).GetFieldValues(i, strFields) & "," & _
                            strUserAndTime & "," & strAftUrno & ",#UrnoPoolGetNext" & CStr(lngInsertCount) & "#" & vbLf
                    End If
                Else
                    lngLineNo = CLng(Val(strLineNo))
                    'check if it gets updated
                    If IsRecordUpdated(tabDelays(Index), i, tabSaved(Index), lngLineNo) Then      'record gets updated
                        If tabDelays(Index).GetFieldValue(i, "DURN") = "0" And _
                        tabSaved(Index).GetFieldValues(lngLineNo, "DECN,DECA,DECS") <> tabDelays(Index).GetFieldValues(i, "DECN,DECA,DECS") Then 'invalid delay code
                            Call EmptyTabFlag(Index, i)
                            
                            If tbsDelays.SelectedItem.Index <> Index + 1 Then
                                Set tbsDelays.SelectedItem = tbsDelays.Tabs.Item(Index + 1)
                            End If
                            MsgBox "Invalid delay code found in line " & CStr(i + 1) & "!", vbInformation, Me.Caption
                            Exit Function
                        Else
                            strData = strData & "*CMD*," & strTable & ",URT," & CStr(intUpdFieldsCount) & "," & _
                                strUpdFields & ",[URNO=:VURNO]" & vbLf
                            strData = strData & tabDelays(Index).GetFieldValues(i, strFields) & "," & _
                                strUserAndTime & "," & tabDelays(Index).GetFieldValues(i, "URNO") & vbLf
                        End If
                    Else 'no update
                        'do nothing
                    End If
                    
                    'mark tabSaved as deleted
                    tabSaved(Index).SetFieldValues lngLineNo, "#FLAG#", "*"
                End If
            Else 'new record
                If tabDelays(Index).GetFieldValue(i, "DURN") = "0" Then 'invalid delay code
                    Call EmptyTabFlag(Index, i)
                
                    If tbsDelays.SelectedItem.Index <> Index + 1 Then
                        Set tbsDelays.SelectedItem = tbsDelays.Tabs.Item(Index + 1)
                    End If
                    MsgBox "Invalid delay code found in line " & CStr(i + 1) & "!", vbInformation, Me.Caption
                    Exit Function
                Else
                    lngInsertCount = lngInsertCount + 1
                    
                    strData = strData & "*CMD*," & strTable & ",IRT," & CStr(intInsFieldsCount) & "," & _
                        strInsFields & vbLf
                    strData = strData & tabDelays(Index).GetFieldValues(i, strFields) & "," & _
                        strUserAndTime & "," & strAftUrno & ",#UrnoPoolGetNext" & CStr(lngInsertCount) & "#" & vbLf
                End If
            End If
        Next i
    
        'check for deletion by user
        If Index = 0 Then
            strUrnoList = ""
            strTable = "DCMTAB"
            For i = 0 To tabSaved(Index).GetLineCount - 1
                If tabSaved(Index).GetFieldValue(i, "#FLAG#") = "" Then
                    'before deleting, insert into DCMTAB
                    'look from memory table
                    rsDelayInfo.Find "URNO = '" & tabSaved(Index).GetFieldValue(i, "URNO") & "'", , , 1
                    If Not rsDelayInfo.EOF Then
                        lngInsertCount = lngInsertCount + 1
                        
                        strData = strData & "*CMD*," & strTable & ",IRT," & CStr(intDelFieldsCount) & "," & strDelFields & vbLf
                        strDataTemp = ""
                        For Each fld In rsDelayInfo.Fields
                            If fld.Name = "URNO" Then
                                strDataTemp = strDataTemp & ",#UrnoPoolGetNext" & CStr(lngInsertCount) & "#"
                            Else
                                If IsNull(fld.Value) Then
                                    strDataTemp = strDataTemp & ","
                                Else
                                    If fld.Type = adDate Then
                                        strDataTemp = strDataTemp & "," & Format(LocalDateToUTC(fld.Value, UtcTimeDiff), "YYYYMMDDhhmmss")
                                    Else
                                        strDataTemp = strDataTemp & "," & fld.Value
                                    End If
                                End If
                            End If
                        Next fld
                        If strDataTemp <> "" Then strDataTemp = Mid(strDataTemp, 2)
                        strData = strData & strDataTemp & vbLf
                    End If
                    
                    'collect the URNO
                    strUrnoList = strUrnoList & "," & tabSaved(Index).GetFieldValue(i, "URNO")
                End If
            Next i
            If strUrnoList <> "" Then strUrnoList = Mid(strUrnoList, 2)
        End If
    Next TabObject
    
    If strData = "" Then
        Call EnableSaveButton(False)
    Else
        'get the new urno for insertion
        If lngInsertCount > 0 Then
            oUfisServer.UrnoPoolInit lngInsertCount
            oUfisServer.UrnoPoolPrepare lngInsertCount
            
            For i = 1 To lngInsertCount
                strFind = "#UrnoPoolGetNext" & CStr(i) & "#"
                strUrno = oUfisServer.UrnoPoolGetNext
                
                strData = Replace(strData, strFind, strUrno)
            Next i
        End If
                    
        'ok, send the DRT
        If strUrnoList <> "" Then
            strTable = "DCFTAB"
            strCommand = "DRT"
            strFields = ""
            strWhere = "WHERE URNO IN (" & strUrnoList & ")"
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, "", strWhere, "", 0, False, False
        End If
            
        'then send IRT/URT using RelHdl
        Call InsertUpdateRecords(strTable, strInsFields, strData)
        
        strFields = "WKSNAME,APPNAME,USERNAME,APPID"
        strData = oUfisServer.GetMyWorkStationName & ",CDID," & LoginUserName & "," & App.hInstance
        strWhere = strAftUrno
        'send BC
        oUfisServer.CallCeda strResult, "SBC", "DCFTAB", strFields, strData, strWhere, "", 0, False, False
        
        'do not wait for broadcast, re-read the records
        Call RefreshDisplay("SBC", "DCFTAB", strAftUrno, "")
    End If
    
    SaveRecords = True
End Function

Private Function IsRecordUpdated(TabCurrent As TABLib.Tab, ByVal TabCurrentLineNo As Long, tabSaved As TABLib.Tab, ByVal TabSavedLineNo As Long)
    Dim i As Long
    Dim strOldValue As String
    Dim strNewValue As String
    Dim blnReturn As Boolean
    
    blnReturn = False
    For i = 0 To TabCurrent.GetColumnCount - 1
        If IsColumnEditable(TabCurrent.Index, i, False) Then
            strOldValue = tabSaved.GetColumnValue(TabSavedLineNo, i)
            strNewValue = TabCurrent.GetColumnValue(TabCurrentLineNo, i)
            
            If strOldValue <> strNewValue Then
                blnReturn = True
                Exit For
            End If
        End If
    Next i
    
    IsRecordUpdated = blnReturn
End Function

Private Function IsValidSeqNum(ByVal Value As Variant, ByVal MinValue As Integer, ByVal MaxValue As Integer) As Boolean
    Dim blnReturn As Boolean
    
    blnReturn = False
    If IsNumeric(Value) Then 'numeric
        If CStr(Val(Value)) = CStr(Value) Then 'no decimal
            Value = Val(Value)
            If Value >= MinValue And Value <= MaxValue Then
                blnReturn = True
            End If
        End If
    End If
    IsValidSeqNum = blnReturn
End Function

Private Sub Form_Activate()
    Static IsActivated As Boolean
    
    If Not IsActivated Then
        SetFormOnTop Me, pblnIsOnTop
        IsActivated = True
    End If
    
    On Error Resume Next
    
    tabDelays(0).SetFocus
    If tabDelays(0).GetCurrentSelected = -1 Then
        If tabDelays(0).GetLineCount > 1 Then
            tabDelays(0).SetCurrentSelection 0
        End If
        Call tabDelays_RowSelectionChanged(0, tabDelays(0).GetCurrentSelected, True)
    End If
End Sub

Private Sub Form_Load()
    Set rsAft = colDataSources.Item("AFTTAB")
    Set rsDelayInfo = colDataSources.Item("DCFTAB")
    Set rsDelayHist = colDataSources.Item("DCMTAB")
    Set rsDelayCodes = colDataSources.Item("DENTAB")
    If colDataSources.Exists("SEATAB") Then
        Set rsSeason = colDataSources.Item("SEATAB")
    End If
    
    If Not pblnShowFlightDetails Then
        picFlightDetails.Visible = False
    Else
        Set FormToShow = FlightDetails
        Call ChangeFormBorderStyle(FormToShow, vbBSNone)
        FormWithinForm picFlightDetails, FormToShow
        FormToShow.Move 0, 0, picFlightDetails.Width, picFlightDetails.Height
        FormToShow.Show
        
        picFlightDetails.Visible = True
    End If
    
    Call PrepareTabstrip
    Call PrepareToolbar
    Call PrepareTab
    
    lngCurrLine = -1
    lngCurrCol = -1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Select Case UnloadMode
        Case vbFormControlMenu, vbFormCode  '0   The user chose the Close command from the Control menu on the form.
                                            '1   The Unload statement is invoked from code.
            If tbrDelays(0).Buttons("save").Enabled Then
                Select Case MsgBox("Do you want to save your changes before closing this application?", _
                    vbYesNoCancel, Me.Caption)
                    
                    Case vbYes
                        If Not SaveRecords Then
                            Cancel = True
'                            MyMainForm.chkRightButtons(1).Value = vbUnchecked
'                            pblnCanExitApp = False
                        End If
                    Case vbNo
                        'do nothing
                    Case vbCancel
                        Cancel = True
'                        MyMainForm.chkRightButtons(1).Value = vbUnchecked
'                        pblnCanExitApp = False
                End Select
            End If
        Case vbAppWindows    '2   The current Microsoft Windows operating environment session is ending.
        Case vbAppTaskManager    '3   The Microsoft Windows Task Manager is closing the application.
        Case vbFormMDIForm   '4   An MDI child form is closing because the MDI form is closing.
        Case vbFormOwner    '5   A form is closing because its owner is closing.
    End Select
    
    If Not Cancel Then
        Unload MyMainForm
    End If
End Sub

Private Sub Form_Resize()
    Dim sngWidth As Single
    Dim sngHeight As Single
    Dim tbr As MSComctlLib.Toolbar
    Dim TabObject As TABLib.Tab
    Dim sngTop As Single
    
    If Me.WindowState <> vbMinimized Then
'        If fraToolbar.Visible Then
'            sngWidth = Me.ScaleWidth
'            If sngWidth < 0 Then sngWidth = 0
'
'            If sngToolbarHeight = 0 Then
'                CalculateToolbarHeight
'                sngToolbarHeight = fraToolbar.Height
'            End If
'
'            sngTop = Me.ScaleHeight - fraToolbar.Height
'            If sngTop < 0 Then sngTop = 0
'
'            fraToolbar.Move 0, sngTop, sngWidth
'        End If

        If picFlightDetails.Visible Then
            sngTop = picFlightDetails.Height
        End If
        
        sngWidth = Me.ScaleWidth - 160
        If sngWidth < 0 Then sngWidth = 0

        sngHeight = Me.ScaleHeight - picSep.Height - 160
        For Each tbr In tbrDelays
            If tbr.Visible Then
                sngHeight = sngHeight - tbr.Height
            End If
        Next tbr
        If sngHeight < 0 Then sngHeight = 0

        tbsDelays.Move 80, 80 + sngTop, sngWidth, sngHeight - sngTop
        For Each TabObject In tabDelays
            TabObject.Move 120, 440 + sngTop, sngWidth - 80, sngHeight - sngTop - 400
        Next TabObject
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set ExcelExport = Nothing
    Set PDFExport = Nothing
    
    If Not (FormToShow Is Nothing) Then
        Unload FormToShow
    End If
End Sub

Private Sub picFlightDetails_Resize()
    If Not (FormToShow Is Nothing) Then
        FormToShow.Move 0, 0, picFlightDetails.Width, picFlightDetails.Height
    End If
End Sub

Private Sub tabDelays_BoolPropertyChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As Boolean)
    Call EnableSaveButton(True)
End Sub

Private Sub tabDelays_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Dim lngLineNo As Long
    Dim lngColNo As Long
    Dim blnEditingDone As Boolean
    Dim blnMoveEdit As Boolean

    Select Case Key
        Case vbKeyReturn, vbKeyTab, vbKeyUp, vbKeyDown
            If IsOnEdit Then
                blnEditingDone = True
                blnMoveEdit = False
            
                lngLineNo = lngCurrLine
                lngColNo = lngCurrCol
                Select Case Key
                    Case vbKeyUp
                        lngLineNo = lngLineNo - 1
                        blnEditingDone = (lngLineNo >= 0)
                        blnMoveEdit = True
                    Case vbKeyDown
                        lngLineNo = lngLineNo + 1
                        blnEditingDone = (lngLineNo < tabDelays(Index).GetLineCount)
                        blnMoveEdit = True
                    Case vbKeyTab
                        Do
                            lngColNo = lngColNo + 1
                            If lngColNo >= tabDelays(Index).GetColumnCount Then
                                lngColNo = 0
                                lngLineNo = lngLineNo + 1
                                If lngLineNo >= tabDelays(Index).GetLineCount Then
                                    Exit Do
                                End If
                            End If
                        Loop Until IsColumnEditable(Index, lngColNo)
                        blnEditingDone = True
                        blnMoveEdit = (lngLineNo < tabDelays(Index).GetLineCount) And _
                            (lngColNo < tabDelays(Index).GetColumnCount)
                End Select
                
                If blnEditingDone Then
                    Call ValidateUserEntry(Index, lngCurrLine, lngCurrCol)
                    
                    If tabDelays(Index).GetCurrentSelected <> lngCurrLine Then
                        lngLineNo = tabDelays(Index).GetCurrentSelected
                        Select Case Key
                            Case vbKeyUp
                                lngLineNo = lngLineNo - 1
                                blnMoveEdit = (lngLineNo >= 0)
                            Case vbKeyDown
                                lngLineNo = lngLineNo + 1
                                blnMoveEdit = (lngLineNo < tabDelays(Index).GetLineCount)
                            Case vbKeyTab
                                lngColNo = lngCurrCol
                                Do
                                    lngColNo = lngColNo + 1
                                    If lngColNo >= tabDelays(Index).GetColumnCount Then
                                        lngColNo = 0
                                        lngLineNo = lngLineNo + 1
                                        If lngLineNo >= tabDelays(Index).GetLineCount Then
                                            Exit Do
                                        End If
                                    End If
                                Loop Until IsColumnEditable(Index, lngColNo)
                                blnMoveEdit = (lngLineNo < tabDelays(Index).GetLineCount) And _
                                    (lngColNo < tabDelays(Index).GetColumnCount)
                        End Select
                    End If
                    
                    If blnMoveEdit Then
                        tmrTimer.Tag = CStr(Index) & "," & CStr(lngLineNo) & "," & CStr(lngColNo)
                        tmrTimer.Enabled = True
                    End If
                End If
            End If
    End Select
End Sub

Private Sub tabDelays_LostFocus(Index As Integer)
    If IsOnEdit Then
        Call ValidateUserEntry(Index, lngCurrLine, lngCurrCol)
    End If
End Sub

Private Sub tabDelays_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strTelexUrno As String

    If Selected Then
        strTelexUrno = Trim(tabDelays(Index).GetFieldValue(LineNo, "TURN"))
        tbrDelays(1).Buttons("telex").Enabled = (strTelexUrno <> "")
    End If
End Sub

Private Sub tabDelays_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        If (LineNo <> lngCurrLine Or ColNo <> lngCurrCol) And IsOnEdit Then
            Call ValidateUserEntry(Index, lngCurrLine, lngCurrCol)
        End If
        tmrTimer.Tag = CStr(Index) & "," & CStr(LineNo) & "," & CStr(ColNo)
        tmrTimer.Enabled = True
    End If
End Sub

Private Sub tabDelays_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strField As String
    Dim strSelectedData As String
    
    If LineNo >= 0 Then
        If IsColumnEditable(Index, ColNo) Then
            strField = GetRealItem(tabDelays(Index).LogicalFieldList, ColNo, ",")
        
            Select Case strField
                Case "DECN", "DECA", "DECS"
                    Set LookupForm.DataSource = rsDelayCodes
                    LookupForm.Columns = "URNO,ALC3,DECN,DECA,DECS,DENA"
                    LookupForm.ColumnHeaders = "URNO,ALC,Nu,Al,SC,Description"
                    LookupForm.ColumnWidths = "0,550,500,500,500,5850"
                    LookupForm.KeyColumns = "URNO,ALC3,DECN,DECA,DECS"
                    LookupForm.Caption = "Delay Codes"
                    LookupForm.AllowMultiSelection = False
                    If Not (rsAft.BOF And rsAft.EOF) Then
                        rsAft.MoveFirst
                        LookupForm.Filters = "Airline Specific,ALC3 = '" & rsAft.Fields("ALC3").Value & "'|Global,ALC3 = ''"
                    End If
                    LookupForm.FilterIndex = 0
                    LookupForm.IsOnTop = pblnIsOnTop
                    LookupForm.Show vbModal, Me
        
                    strSelectedData = LookupForm.SelectedData
                    If strSelectedData <> "" Then
                        tabDelays(Index).SetFieldValues LineNo, "DURN,ALC3,DECN,DECA,DECS", strSelectedData
                        rsDelayCodes.Find "URNO = " & tabDelays(Index).GetFieldValue(LineNo, "DURN"), , , 1
                        If rsDelayCodes.EOF Then
                            tabDelays(Index).SetFieldValues LineNo, "MEAN", NOT_FOUND_MSG
                            If tabDelays(Index).GetFieldValue(LineNo, "URNO") <> "" Then
                                tabDelays(Index).SetLineColor LineNo, WARNING_FORE_COLOR, WARNING_BACK_COLOR
                            Else
                                tabDelays(Index).SetLineColor LineNo, WARNING_FORE_COLOR, NEW_BACK_COLOR
                            End If
                        Else
                            tabDelays(Index).SetFieldValues LineNo, "MEAN", Replace(rsDelayCodes.Fields("DENA").Value, ",", ";")
                            If tabDelays(Index).GetFieldValue(LineNo, "URNO") <> "" Then
                                tabDelays(Index).SetLineColor LineNo, DEFAULT_FORE_COLOR, DEFAULT_BACK_COLOR
                            Else
                                tabDelays(Index).SetLineColor LineNo, DEFAULT_FORE_COLOR, NEW_BACK_COLOR
                            End If
                        End If
                        
                        If Index = 0 And CountColumnExists Then Call FillInCountColumn 'igu on 19/05/2011
                        
                        tabDelays(Index).Refresh
                        
                        Call EnableSaveButton(True)
                    End If
            End Select
        End If
    End If
End Sub

Private Sub tbrDelays_ButtonClick(Index As Integer, ByVal Button As MSComctlLib.Button)
    Dim TabObjectIndex As Integer

    TabObjectIndex = tbsDelays.SelectedItem.Index - 1

    picSep.SetFocus
    tabDelays(TabObjectIndex).SetFocus

    Select Case Button.Key
        Case "add"
            Call AddRecord(TabObjectIndex, True)
        Case "insert"
            Call AddRecord(TabObjectIndex, False)
        Case "delete"
            Call DeleteRecord(TabObjectIndex)
        Case "save"
            Call SaveRecords
        Case "print", "excel", "pdf"
            Call PrintDelays(TabObjectIndex, Button.Key)
        Case "telex"
            Call ShowTelex(TabObjectIndex)
        Case "top"
            'MyMainForm.chkRightButtons(0).Value = IIf(Button.Value = tbrPressed, vbChecked, vbUnchecked)
            pblnIsOnTop = (Button.Value = tbrPressed)
            SetFormOnTop Me, pblnIsOnTop
        Case "close"
'            MyMainForm.chkRightButtons(1).Value = vbChecked
            Unload Me
    End Select
End Sub

Private Sub tbsDelays_Click()
    Dim TabObject As TABLib.Tab
    Dim blnEnabled As Boolean

    For Each TabObject In tabDelays
        TabObject.Visible = (TabObject.Index = tbsDelays.SelectedItem.Index - 1)
        TabObject.TabStop = TabObject.Visible
        If TabObject.Visible Then
            TabObject.SetFocus
            If TabObject.GetCurrentSelected < 0 Then
                If TabObject.GetLineCount > 0 Then
                    TabObject.SetCurrentSelection 0
                End If
            End If
            Call tabDelays_RowSelectionChanged(TabObject.Index, TabObject.GetCurrentSelected, True)
        End If
    Next TabObject
    
    blnEnabled = (tbsDelays.SelectedItem.Index = 1)
    tbrDelays(0).Buttons("add").Enabled = blnEnabled
    tbrDelays(0).Buttons("insert").Enabled = blnEnabled
    tbrDelays(0).Buttons("delete").Enabled = blnEnabled
End Sub

Private Sub tmrTimer_Timer()
    Dim vntTag As Variant
    Dim Index As Integer
    Dim LineNo As Long
    Dim ColNo As Long
    
    tmrTimer.Enabled = False
    
    vntTag = Split(tmrTimer.Tag, ",")
    Index = vntTag(0)
    LineNo = vntTag(1)
    If LineNo = -1 Then LineNo = tabDelays(Index).GetCurrentSelected
    ColNo = vntTag(2)
    If ColNo = -1 Then ColNo = lngCurrCol
    
    Call StartEditMode(Index, LineNo, ColNo)
End Sub
