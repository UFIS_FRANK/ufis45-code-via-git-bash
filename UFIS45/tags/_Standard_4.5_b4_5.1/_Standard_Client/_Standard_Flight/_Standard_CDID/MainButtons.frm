VERSION 5.00
Begin VB.Form MainButtons 
   Caption         =   "Main Buttons"
   ClientHeight    =   480
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11895
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   480
   ScaleWidth      =   11895
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkOnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   0
      Width           =   975
   End
   Begin VB.Frame fraTimeZone 
      Height          =   375
      Left            =   6720
      TabIndex        =   5
      Top             =   0
      Width           =   1455
      Begin VB.OptionButton optTimeZone 
         Caption         =   "LOCAL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   0
         Width           =   735
      End
      Begin VB.OptionButton optTimeZone 
         Caption         =   "UTC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   0
         Value           =   -1  'True
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdMainButtons 
      Caption         =   "Excel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   5640
      TabIndex        =   4
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton cmdMainButtons 
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   4680
      TabIndex        =   3
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton cmdMainButtons 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   3600
      TabIndex        =   2
      Top             =   0
      Width           =   975
   End
   Begin VB.ComboBox cboView 
      Height          =   315
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   30
      Width           =   2535
   End
   Begin VB.CommandButton cmdMainButtons 
      Caption         =   "View"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   855
   End
End
Attribute VB_Name = "MainButtons"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ContainerForm As Form

Private rsView As ADODB.Recordset
Private IsFirstLoad As Boolean

Public Sub LoadExtraButton(ByVal Caption As String, ByVal Key As String)
    Dim ButtonIndex As Long
    Dim sngMoveDistance As Single
    
    ButtonIndex = cmdMainButtons.UBound + 1
    Load cmdMainButtons(ButtonIndex)
    cmdMainButtons(ButtonIndex).Caption = Caption
    cmdMainButtons(ButtonIndex).Tag = Key
    
    cmdMainButtons(ButtonIndex).Left = cmdMainButtons(ButtonIndex - 1).Left + cmdMainButtons(ButtonIndex - 1).Width + 105
    cmdMainButtons(ButtonIndex).Width = Len(Caption) * Me.TextWidth("A")
    cmdMainButtons(ButtonIndex).Visible = True
    
    sngMoveDistance = cmdMainButtons(ButtonIndex).Width + 105
    fraTimeZone.Left = fraTimeZone.Left + sngMoveDistance
    chkOnTop.Left = chkOnTop.Left + sngMoveDistance
End Sub

Public Sub UpdateView(ByVal CedaCmd As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
    Call UpdateViewCombo(cboView, CedaCmd, Urno, ChangedFields)
End Sub

Private Sub cboView_Click()
    Dim lngUrno As Long
    
    If Not IsFirstLoad Then
        lngUrno = cboView.ItemData(cboView.ListIndex)
        ContainerForm.MainControlsExecuted "viewselect", Array(cboView.ListIndex, lngUrno)
    End If
End Sub

Private Sub chkOnTop_Click()
    If chkOnTop.Value = vbChecked Then
        chkOnTop.BackColor = vbGreen
    Else
        chkOnTop.BackColor = vbButtonFace
    End If
    If Not IsFirstLoad Then
        ContainerForm.MainControlsExecuted "ontop", chkOnTop.Value
    End If
End Sub

Private Sub cmdMainButtons_Click(Index As Integer)
    Select Case Index
        Case 0 'View
            ContainerForm.MainControlsExecuted "view"
        Case 1 'Search
            ContainerForm.MainControlsExecuted "search"
        Case 2 'Print
            ContainerForm.MainControlsExecuted "print"
        Case 3 'Excel
            ContainerForm.MainControlsExecuted "excel"
        Case Else 'For other purposes
            ContainerForm.MainControlsExecuted cmdMainButtons(Index).Tag
    End Select
End Sub

Private Sub Form_Load()
    IsFirstLoad = True
    
    Set rsView = colDataSources.Item("VCDTAB")
    Call FillViewComboBox(rsView, cboView)
    cboView.ListIndex = 0
    
    optTimeZone(0).Value = IsUTCTimeZone
    optTimeZone(1).Value = Not IsUTCTimeZone
    Call optTimeZone_Click(0)
    
    chkOnTop.Value = IIf(pblnIsOnTop, vbChecked, vbUnchecked)
    
    IsFirstLoad = False
End Sub

Private Sub optTimeZone_Click(Index As Integer)
    Dim opt As OptionButton
    
    For Each opt In Me.optTimeZone
        If opt.Value Then
            opt.BackColor = vbGreen
        Else
            opt.BackColor = vbButtonFace
        End If
    Next opt
    
    If Not IsFirstLoad Then
        ContainerForm.MainControlsExecuted "timezone", optTimeZone(0).Value
    End If
End Sub
