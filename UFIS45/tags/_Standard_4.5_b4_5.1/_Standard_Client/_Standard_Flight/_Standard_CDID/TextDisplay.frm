VERSION 5.00
Begin VB.Form TextDisplay 
   Caption         =   "TextDisplay"
   ClientHeight    =   5895
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6435
   Icon            =   "TextDisplay.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5895
   ScaleWidth      =   6435
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtText 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   120
      Width           =   4215
   End
End
Attribute VB_Name = "TextDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public DataSource As String
Public WindowWidth As Single
Public WindowHeight As Single
Public IsOnTop As Boolean

Private IsActivated As Boolean

Private Sub Form_Activate()
    If Not IsActivated Then
        If IsOnTop Then
            SetFormOnTop Me, IsOnTop
        End If
    
        txtText.Text = DataSource
        
        If WindowWidth > 0 Then Me.Width = WindowWidth
        If WindowHeight > 0 Then Me.Height = WindowHeight
        
        IsActivated = True
    End If
End Sub

Private Sub Form_Load()
    IsActivated = False
    IsOnTop = False
End Sub

Private Sub Form_Resize()
    Dim sngWidth As Single
    Dim sngHeight As Single
    Dim sngTop As Single
    
    If Me.WindowState <> vbMinimized Then
        sngTop = 0
        
        sngWidth = Me.ScaleWidth
        If sngWidth < 0 Then sngWidth = 0
        
        sngHeight = Me.ScaleHeight
        If sngHeight < 0 Then sngHeight = 0
        
        txtText.Move 0, sngTop, sngWidth, sngHeight
    End If
End Sub
