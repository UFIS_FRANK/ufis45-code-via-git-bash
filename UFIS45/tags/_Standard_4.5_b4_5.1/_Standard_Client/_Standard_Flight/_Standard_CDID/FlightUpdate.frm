VERSION 5.00
Begin VB.Form FlightUpdate 
   BorderStyle     =   0  'None
   Caption         =   "Flight Update"
   ClientHeight    =   7575
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9465
   LinkTopic       =   "Form1"
   ScaleHeight     =   7575
   ScaleWidth      =   9465
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrSuccess 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   8160
      Top             =   0
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3360
      TabIndex        =   14
      Top             =   3960
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   3960
      Width           =   1095
   End
   Begin VB.TextBox txtFields 
      DataField       =   "NULL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   1920
      TabIndex        =   2
      Tag             =   "ETAI/ETDI|ETAU/ETDU"
      Text            =   "ETAI/ETDI|ETAU/ETDU"
      Top             =   1080
      Width           =   1815
   End
   Begin VB.TextBox txtFields 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   2
      Left            =   1920
      MaxLength       =   12
      TabIndex        =   7
      Tag             =   "REGN"
      Text            =   "REGN"
      Top             =   2280
      Width           =   3135
   End
   Begin VB.TextBox txtFields 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   1920
      MaxLength       =   5
      TabIndex        =   4
      Tag             =   "PSTA/PSTD"
      Text            =   "PSTA/PSTD"
      Top             =   1680
      Width           =   1455
   End
   Begin VB.TextBox txtFields 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   3
      Left            =   1920
      MaxLength       =   3
      TabIndex        =   9
      Tag             =   "ACT3"
      Text            =   "ACT"
      Top             =   2880
      Width           =   855
   End
   Begin VB.TextBox txtFields 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   4
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   10
      Tag             =   "ACT5"
      Text            =   "ACT5"
      Top             =   2880
      Width           =   1335
   End
   Begin VB.CommandButton cmdHelp 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   0
      Left            =   3360
      Style           =   1  'Graphical
      TabIndex        =   5
      TabStop         =   0   'False
      Tag             =   "PST"
      Top             =   1680
      Width           =   375
   End
   Begin VB.CommandButton cmdHelp 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   1
      Left            =   4080
      Style           =   1  'Graphical
      TabIndex        =   11
      TabStop         =   0   'False
      Tag             =   "ACT"
      Top             =   2880
      Width           =   375
   End
   Begin VB.Label lblMsg 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Message"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1920
      TabIndex        =   0
      Top             =   600
      Width           =   3375
   End
   Begin VB.Label lblSuccess 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "The flight record has been successfully updated."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   375
      Left            =   1920
      TabIndex        =   12
      Top             =   3480
      Visible         =   0   'False
      Width           =   5895
   End
   Begin VB.Label lblLabels 
      Caption         =   "A/C Type:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   480
      TabIndex        =   8
      Top             =   3000
      Width           =   1335
   End
   Begin VB.Label lblLabels 
      Caption         =   "Reg. number:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   480
      TabIndex        =   6
      Top             =   2280
      Width           =   1335
   End
   Begin VB.Label lblLabels 
      Caption         =   "Parking stand:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   480
      TabIndex        =   3
      Top             =   1680
      Width           =   1335
   End
   Begin VB.Label lblLabels 
      Caption         =   "ETA/ETD:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   480
      TabIndex        =   1
      Top             =   1080
      Width           =   1335
   End
End
Attribute VB_Name = "FlightUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
 
Private FlightUrno As String
Private IsChangesFromCode As Boolean

Private rsAft As ADODB.Recordset
Private rsAircraftPos As ADODB.Recordset
Private rsAircraftTypes As ADODB.Recordset
Private rsAircraftRegNums As ADODB.Recordset

Public Sub RefreshDisplay(ByVal Urno As String, ByVal ChangedFields As String)
    If Urno = FlightUrno Then
        If IsRefreshNeeded(ChangedFields) Then
            IsChangesFromCode = True
            Call FillInData(rsAft)
            IsChangesFromCode = False
        End If
    End If
End Sub

Private Function IsRefreshNeeded(ByVal ChangedFields As String) As Boolean
    Dim strFields As String
    
    strFields = Split(GetFieldsAndDataString(True, False, False), ";")(0)
    
    IsRefreshNeeded = IsInList(ChangedFields, strFields)
End Function

Private Function GetFieldsAndDataString(Optional ByVal IncludeFields As Boolean = True, _
Optional ByVal IncludeData As Boolean = True, Optional ByVal UpdateMemTable As Boolean = True) As String
    Dim strFields As String
    Dim strData As String
    Dim strADID As String
    Dim strTag As String
    Dim vntTag As Variant
    Dim vField As Variant
    Dim sField As String
    Dim sField2 As String
    Dim dtmBaseDate As Date
    Dim dtmStdt As Date
    Dim txt As TextBox

    strADID = RTrim(rsAft.Fields("ADID").Value)
    If strADID = "A" Then
        dtmBaseDate = UTCDateToLocal(rsAft.Fields("STOA").Value, UtcTimeDiff)
    Else
        dtmBaseDate = UTCDateToLocal(rsAft.Fields("STOD").Value, UtcTimeDiff)
    End If
    dtmBaseDate = DateTimeToDate(dtmBaseDate)
        
    strFields = ""
    strData = ""
    For Each txt In Me.txtFields
        If IncludeFields Then
            strTag = txt.Tag
            vntTag = Split(strTag, "|")
            If UBound(vntTag) = 0 Then
                strTag = vntTag(0)
            Else
                strTag = vntTag(1)
            End If
            vField = Split(strTag, "/")
            If UBound(vField) > 0 Then
                If strADID = "A" Then
                    sField = vField(0)
                Else
                    sField = vField(1)
                End If
            Else
                sField = vField(0)
            End If
            
            strFields = strFields & "," & sField
        End If
        
        If IncludeData Then
            sField2 = sField
            If UBound(vntTag) > 0 Then
                strTag = vntTag(0)
                vField = Split(strTag, "/")
                If UBound(vField) > 0 Then
                    If strADID = "A" Then
                        sField2 = vField(0)
                    Else
                        sField2 = vField(1)
                    End If
                Else
                    sField2 = vField(0)
                End If
            End If
            
            If txt.DataField = "" Then
                strData = strData & "," & txt.Text
                
                If UpdateMemTable Then rsAft.Fields(sField2).Value = txt.Text
            Else
                If txt.DataField = "NULL" Then
                    strData = strData & ","
                    
                    If UpdateMemTable Then rsAft.Fields(sField2).Value = Null
                Else
                    dtmStdt = ShortTimeToFullDateTime(dtmBaseDate, txt.DataField)
                    dtmStdt = LocalDateToUTC(dtmStdt, UtcTimeDiff)
                    strData = strData & "," & Format(dtmStdt, "YYYYMMDDhhmmss")
                    
                    If UpdateMemTable Then rsAft.Fields(sField2).Value = dtmStdt
                End If
            End If
        End If
    Next txt
    
    If strFields <> "" Then strFields = Mid(strFields, 2)
    If strData <> "" Then strData = Mid(strData, 2)
    
    GetFieldsAndDataString = strFields & ";" & strData
End Function

Public Sub LoadData(ByVal strUrno As String)
    Dim blnFLightFound As Boolean
    
    blnFLightFound = (strUrno <> "")
    If blnFLightFound Then
        rsAft.Find "URNO = " & strUrno, , , 1
        blnFLightFound = Not (rsAft.EOF)
    End If
    
    IsChangesFromCode = True
    If blnFLightFound Then
        FlightUrno = strUrno
        
        Call FillInData(rsAft)
        Call EnableAllControls(True)
        lblMsg.Caption = ""
    Else
        FlightUrno = ""
        
        Call ClearAllFields
        Call EnableAllControls(False)
        lblMsg.Caption = "Flight not found!"
    End If
    IsChangesFromCode = False
    
    cmdOK.BackColor = vbButtonFace
End Sub

Private Sub ClearAllFields()
    Dim i As Integer
    
    For i = 0 To txtFields.UBound
        txtFields(i).Text = ""
    Next i
End Sub

Private Sub EnableAllControls(ByVal Enabled As Boolean)
    Dim i As Integer
    
    For i = 0 To txtFields.UBound
        txtFields(i).Enabled = Enabled
    Next i
    For i = 0 To cmdHelp.UBound
        cmdHelp(i).Enabled = Enabled
    Next i
    cmdOK.Enabled = Enabled
    cmdCancel.Enabled = Enabled
End Sub

Private Function ValidateUserInput() As Boolean
    Dim strTime As String
    Dim strFind As String
    
    ValidateUserInput = True
   
    'ETA/ETD must be in correct format
    strTime = txtFields(0).DataField
    If strTime = "ERROR" Then
        MsgBoxEx lblLabels(1) & " not valid!", vbInformation, Me.Caption, , , eCentreDialog
        txtFields(0).SetFocus
        ValidateUserInput = False
        Exit Function
    End If
    
    'Parking Stand must be in list
    If Trim(txtFields(1).Text) <> "" Then
        With rsAircraftPos
            .Find "PNAM = '" & txtFields(1).Text & "'", , , 1
            If .EOF Then
                MsgBoxEx "Parking stand not valid!", vbInformation, Me.Caption, , , eCentreDialog
                txtFields(1).SetFocus
                ValidateUserInput = False
                Exit Function
            End If
        End With
    End If
    
    'A/C type must be in list
    If Trim(txtFields(3).Text) <> "" Or Trim(txtFields(4).Text) <> "" Then
        With rsAircraftTypes
            .Filter = "ACT3 = '" & txtFields(3).Text & "' " & _
                "AND ACT5 = '" & txtFields(4).Text & "'"
            If .EOF Then
                MsgBoxEx "Aircraft type not valid!", vbInformation, Me.Caption, , , eCentreDialog
                txtFields(3).SetFocus
                ValidateUserInput = False
                .Filter = adFilterNone
                Exit Function
            End If
            .Filter = adFilterNone
        End With
    End If
    
    'Regn must be in list and match with A/C type
    If Trim(txtFields(2).Text) <> "" Then
        With rsAircraftRegNums
            .Filter = "REGN = '" & txtFields(2).Text & "' " & _
                "AND ACT3 = '" & txtFields(3).Text & "' " & _
                "AND ACT5 = '" & txtFields(4).Text & "'"
            If .EOF Then
                MsgBoxEx "Aircraft registration number and aircraft type do not match!", vbInformation, Me.Caption, , , eCentreDialog
                txtFields(2).SetFocus
                ValidateUserInput = False
                .Filter = adFilterNone
                Exit Function
            End If
            .Filter = adFilterNone
        End With
    End If
End Function

Private Function SaveUpdates() As Boolean
    Dim strFieldsAndData As String
    Dim vntFieldsAndData As Variant
    Dim strTable As String
    Dim strCommand As String
    Dim strFields As String
    Dim strWhere As String
    Dim strData As String
    Dim strResult As String
    Dim i As Integer
    Dim strUrno As String

    On Error GoTo ErrSave
    
    Screen.MousePointer = vbHourglass
    
    Call ShowFloatingMessage("Updating flight record..." & vbNewLine & "Please wait.")
    
    SaveUpdates = False
    
    rsAft.Find "URNO = " & FlightUrno, , , 1
    If Not rsAft.EOF Then
        strFieldsAndData = GetFieldsAndDataString(True, True)
        vntFieldsAndData = Split(strFieldsAndData, ";")
    
        strFields = vntFieldsAndData(0)
        strData = vntFieldsAndData(1)
        
        strTable = "AFTTAB"
        strCommand = "UFR"
        strWhere = "WHERE URNO = " & FlightUrno
        
        oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
        
        SaveUpdates = (strResult = "BUFFER0,OK")
        
        If SaveUpdates Then
            rsAft.Update
            
            strUrno = CStr(rsAft.Fields("URNO").Value)
            
            If FormIsLoaded("FlightDetails") Then
                FlightDetails.RefreshFlightDetails strUrno
            End If
                
            If FormIsLoaded("FlightList") Then
                FlightList.RefreshTabByUrno strUrno
            End If
        Else
            rsAft.CancelUpdate
        End If
    End If
    
    Call HideFloatingMessage
    
    Screen.MousePointer = vbDefault
    
    Exit Function
    
ErrSave:
    Call HideFloatingMessage
    SaveUpdates = False
    Screen.MousePointer = vbDefault
End Function

Private Sub FillInData(rsData As ADODB.Recordset)
    Dim strADID As String
    Dim dtmBaseDate As Date
    Dim txt As TextBox
    Dim vField As Variant
    Dim sField As String
    Dim intLoc As Integer
    Dim strTag As String
    Dim dtmDate As Date
    
    strADID = RTrim(rsData.Fields("ADID").Value)
    If strADID = "A" Then
        dtmBaseDate = UTCDateToLocal(rsData.Fields("STOA").Value, UtcTimeDiff)
        lblLabels(0).Caption = "ETA"
    Else
        dtmBaseDate = UTCDateToLocal(rsData.Fields("STOD").Value, UtcTimeDiff)
        lblLabels(0).Caption = "ETD"
    End If
    
    For Each txt In Me.txtFields
        strTag = txt.Tag
        intLoc = InStr(strTag, "|")
        If intLoc > 0 Then
            strTag = Left(strTag, intLoc - 1)
        End If
        vField = Split(strTag, "/")
        If UBound(vField) > 0 Then
            If strADID = "A" Then
                sField = vField(0)
            Else
                sField = vField(1)
            End If
        Else
            sField = vField(0)
        End If
        
        If txt.DataField = "" Then
            txt.Text = RTrim(rsData.Fields(sField).Value)
        Else
            If IsNull(rsData.Fields(sField).Value) Then
                txt.Text = ""
            Else
                dtmDate = UTCDateToLocal(rsData.Fields(sField).Value, UtcTimeDiff)
                txt.Text = FullDateTimeToShortTime(dtmBaseDate, dtmDate)
            End If
        End If
    Next txt
End Sub

Private Sub cmdCancel_Click()
    Call LoadData(FlightUrno)
    
    cmdOK.BackColor = vbButtonFace
    txtFields(0).SetFocus
End Sub

Private Sub cmdHelp_Click(Index As Integer)
    Dim strSelectedData As String
    Dim vntSelectedData As Variant

    Select Case Index
        Case 0 'Position
            Set LookupForm.DataSource = rsAircraftPos
            LookupForm.Columns = "PNAM"
            LookupForm.ColumnHeaders = "Name"
            LookupForm.ColumnWidths = "6000"
            LookupForm.KeyColumns = "PNAM"
            LookupForm.Caption = "Aircraft Position"
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            strSelectedData = LookupForm.SelectedData
            If strSelectedData <> "" Then
                txtFields(1).Text = RTrim(strSelectedData)
            End If
            txtFields(1).SetFocus
        Case 1 'A/C Type
            Set LookupForm.DataSource = rsAircraftTypes
            LookupForm.Columns = "ACT3,ACT5"
            LookupForm.ColumnHeaders = "ACT3,ACT5"
            LookupForm.ColumnWidths = "1200,1500"
            LookupForm.KeyColumns = "ACT3,ACT5"
            LookupForm.Caption = "Aircraft Types"
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            strSelectedData = LookupForm.SelectedData
            If strSelectedData <> "" Then
                vntSelectedData = Split(strSelectedData, ",")
                txtFields(3).Text = RTrim(vntSelectedData(0))
                txtFields(4).Text = RTrim(vntSelectedData(1))
            End If
            txtFields(3).SetFocus
    End Select
End Sub

Private Sub cmdOK_Click()
    If ValidateUserInput() Then
        If SaveUpdates() Then
            lblSuccess.Visible = True
            tmrSuccess.Enabled = True
            cmdOK.BackColor = vbButtonFace
        Else
            MsgBoxEx "Failed to update the flight record.", vbCritical, Me.Caption, , , eCentreDialog
        End If
    End If
End Sub

Private Sub Form_Load()
    If colDataSources.Exists("AFTTAB") Then
        Set rsAft = colDataSources.Item("AFTTAB")
    End If
    
    Set rsAircraftPos = colDataSources.Item("PSTTAB")
    Set rsAircraftTypes = colDataSources.Item("ACTTAB")
    Set rsAircraftRegNums = colDataSources.Item("ACRTAB")

    IsChangesFromCode = False
End Sub

Private Sub tmrSuccess_Timer()
    lblSuccess.Visible = False
    tmrSuccess.Enabled = False
End Sub

Private Sub txtFields_Change(Index As Integer)
    Dim strData As String
    
    If Index = 0 Then
        strData = CheckValidTimeExt(txtFields(Index).Text)
    
        If strData = "ERROR" Then
            txtFields(Index).BackColor = &HC0C0FF
        Else
            txtFields(Index).BackColor = vbWindowBackground
        End If
        
        txtFields(Index).DataField = strData
    End If
    
    If Not IsChangesFromCode Then cmdOK.BackColor = vbRed
End Sub

Private Sub txtFields_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtFields_Validate(Index As Integer, Cancel As Boolean)
    Dim strField As String
    Dim strPartnerField As String
    Dim intPartnerIndex As Integer
    
    If Trim(txtFields(Index).Text) <> "" Then
        Select Case Index
            Case 2 'REGN
                With rsAircraftRegNums
                    .Find "REGN = '" & txtFields(Index).Text & "'", , , 1
                    If Not .EOF Then
                        txtFields(3).Text = RTrim(.Fields("ACT3").Value)
                        txtFields(4).Text = RTrim(.Fields("ACT5").Value)
                    End If
                End With
            Case 3, 4 'ACT3, ACT5
                With rsAircraftTypes
                    .Filter = "ACT3 = '" & txtFields(3).Text & "' " & _
                        "AND ACT5 = '" & txtFields(4).Text & "'"
                    If .EOF Then
                        .Filter = adFilterNone
                        
                        If Index = 3 Then
                            strField = "ACT3"
                            strPartnerField = "ACT5"
                            intPartnerIndex = 4
                        Else
                            strField = "ACT5"
                            strPartnerField = "ACT3"
                            intPartnerIndex = 3
                        End If
                        
                        .Filter = strField & " = '" & txtFields(Index).Text & "'"
                        If Not .EOF Then
                            If .RecordCount = 1 Then
                                txtFields(intPartnerIndex).Text = .Fields(strPartnerField).Value
                            Else
                                Call cmdHelp_Click(1)
                            End If
                        End If
                    End If
                    .Filter = adFilterNone
                End With
        End Select
    End If
End Sub
