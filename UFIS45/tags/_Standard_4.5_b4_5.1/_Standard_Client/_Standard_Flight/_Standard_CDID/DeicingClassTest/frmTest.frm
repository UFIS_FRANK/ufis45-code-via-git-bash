VERSION 5.00
Begin VB.Form frmTest 
   Caption         =   "Test"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdTest 
      Caption         =   "Test"
      Height          =   495
      Left            =   1200
      TabIndex        =   0
      Top             =   960
      Width           =   2415
   End
End
Attribute VB_Name = "frmTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim obj As DeicingDialog

Private Sub cmdTest_Click()
    If Not obj.ShowDialog(False) Then
        MsgBox "Error!"
    End If
End Sub

Private Sub Form_Load()
    Set obj = New DeicingDialog
    obj.AppName = "FIPS-Report"
    obj.AppVersion = "4.6.1.9"
    'obj.IniFilePath = "D:\Ufis\System\De-icing.ini"
    obj.HandleBc = True
    obj.TimeZone = UTC
    obj.UserName = "UFIS$ADMIN"
    obj.LoadBasicData
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set obj = Nothing
End Sub
