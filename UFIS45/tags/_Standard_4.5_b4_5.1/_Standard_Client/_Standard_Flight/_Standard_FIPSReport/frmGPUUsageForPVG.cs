using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Data;
using Ufis.Utils;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmGPUUsage.
	/// </summary>
	public class frmGPUUsageForPVG : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))";

		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;

		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "FLNO,ADID,SCHEDULE,ACTUAL,ACTYPE,REG,POS,GPU1,GPU2,GPU3";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "Flight  ,A/D  ,Schedule  ,Actual,A/C  ,REGN  ,POS , GPU 1,GPU 2,GPU 3";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "70,25,80,80,35,70,40,83,83,83";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myGPA;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;

		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmGPUUsageForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "GPUUsage";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmGPUUsageForPVG));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.ucView1 = new UserControls.ucView();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 108);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(744, 398);
			this.panelBody.TabIndex = 3;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(744, 382);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(744, 382);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(744, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(744, 108);
			this.panelTop.TabIndex = 2;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 56);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 72);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 13;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 72);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 10;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 72);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 9;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 72);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 72);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 8;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(20, 12);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 14;
			// 
			// frmGPUUsageForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(744, 506);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmGPUUsageForPVG";
			this.Text = "GPU Usage ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmGPUUsageForPVG_Closing);
			this.Load += new System.EventHandler(this.frmGPUUsageForPVG_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmGPUUsageForPVG_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if (this.ucView1.Viewer.Arrival == false)
			{
				strRet += ilErrorCount.ToString() +  ". Arrival must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Departure must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == true)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must not be selected!\n";
				ilErrorCount++;
			}


			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpAFTWhere;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}

			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,PSTA,PSTD,REGN", 
							  "10,12,2,3,3,14,14,14,14,3,5,5,12", 
							  "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,PSTA,PSTD,REGN");
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Clear();
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Show();
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

				strTmpAFTWhere = "WHERE ";
				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpAFTWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpAFTWhere += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpAFTWhere += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpAFTWhere += strWhereRawD;
					strTmpAFTWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpAFTWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				//patch the where statement according to the user's entry
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@HOPO", UT.Hopo);

				if (this.ucView1.Viewer.Rotation)
				{
					strTmpAFTWhere += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpAFTWhere += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}

				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "Loading GPU Data";
//			progressBar1.Hide();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

			myDB.Unbind("GPA");
			myGPA = myDB.Bind("GPA", "GPA", "FLNU,ASEQ,GAAB,GAAE", "10,1,14,14", "FLNU,ASEQ,GAAB,GAAE");
			myGPA .TimeFields = "GAAB,GAAE";
			myGPA .Clear();
			myGPA .TimeFieldsInitiallyInUtc = true;
			int loops = 0;
			progressBar1.Value = 0;
			progressBar1.Refresh();
			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) ilTotal = 1;
			string strFlnus = "";
			string strTmpWhere = "";
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus += myAFT[i]["URNO"] + ",";
				if((i % 300) == 0 && i > 0)
				{
					loops++;
					int percent = Convert.ToInt32((loops * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpWhere = "WHERE FLNU IN (" + strFlnus + ")";
					myGPA.Load(strTmpWhere);
					strFlnus = "";
				}
			}
			//Load the rest of the GPAs.
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpWhere = "WHERE FLNU IN (" + strFlnus + ")";
				myGPA.Load(strTmpWhere);
				strFlnus = "";
			}
			progressBar1.Hide();
			lblProgress.Text = "";
			myGPA.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			myGPA.CreateIndex("FLNU", "FLNU");
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;
			lblProgress.Text = "Preparing Data";
			lblProgress.Refresh();
			progressBar1.Visible = true;
			progressBar1.Value = 0;
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			StringBuilder sb = new StringBuilder(10000);
			for(int i = 0; i < myAFT.Count; i++)
			{
				string strAdid = myAFT[i]["ADID"];
				string strValues = "";
				if( i % 100 == 0)
				{
					int percent = Convert.ToInt32((i * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
				}
				strValues += myAFT[i]["FLNO"] + ",";
				strValues += strAdid + ",";
				if(strAdid == "A")
				{
					imArrivals++;
					imTotalFlights++;
					//strValues += myAFT[i]["ORG3"] + ",";
					strValues += Helper.DateString( myAFT[i]["STOA"], "dd.MM'-'HH:mm") + ",";
					strValues += Helper.DateString( myAFT[i]["TIFA"], "dd.MM'-'HH:mm") + ",";
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += myAFT[i]["REGN"] + ",";
					strValues += myAFT[i]["PSTA"] + ",";
				}
				if(strAdid == "D")
				{
					imDepartures++;
					imTotalFlights++;
					//strValues += myAFT[i]["DES3"] + ",";
					strValues += Helper.DateString( myAFT[i]["STOD"], "dd.MM'-'HH:mm") + ",";
					strValues += Helper.DateString( myAFT[i]["TIFD"], "dd.MM'-'HH:mm") + ",";
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += myAFT[i]["REGN"] + ",";
					strValues += myAFT[i]["PSTD"] + ",";
				}
				IRow [] rows = myGPA.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
				string strGPU1 = "";
				string strGPU2 = "";
				string strGPU3 = "";
				for(int j = 0; j < rows.Length; j++)
				{
					if(rows[j]["ASEQ"] == "1")
						strGPU1 = Helper.DateString(rows[j]["GAAB"], "HH:mm") + " - " + Helper.DateString(rows[j]["GAAE"], "HH:mm");
					if(rows[j]["ASEQ"] == "2")
						strGPU2 = Helper.DateString(rows[j]["GAAB"], "HH:mm") + " - " + Helper.DateString(rows[j]["GAAE"], "HH:mm");
					if(rows[j]["ASEQ"] == "3")
						strGPU3 = Helper.DateString(rows[j]["GAAB"], "HH:mm") + " - " + Helper.DateString(rows[j]["GAAE"], "HH:mm");
				}
				if(rows.Length > 0)
				{
					strValues += strGPU1 + "," + strGPU2 + "," + strGPU3 + "\n";
					sb.Append(strValues);
				}
			}
			lblProgress.Text = "";
			lblProgress.Refresh();
			progressBar1.Visible = false;
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Sort("2", true, true);
			//tabResult.AutoSizeColumns();
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm") + " to " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "View: " + this.ucView1.Viewer.CurrentView;
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			rptFIPS rpt = new rptFIPS(tabResult, frmMain.strPrefixVal + "GPU usage", strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmGPUUsageForPVG_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
		}

		private void ucView1_viewEvent(string view)
		{
			btnOK_Click(this,null);
		}
	}
}
