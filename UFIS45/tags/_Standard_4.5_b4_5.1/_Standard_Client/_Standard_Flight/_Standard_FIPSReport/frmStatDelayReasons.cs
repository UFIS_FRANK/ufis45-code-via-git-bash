using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmStatDelayReasons.This class is created for the support of the "Statistics of the Delay Reasons".
	/// This is created for solving the PRF 8523
	/// </summary>
	public class frmStatDelayReasons : System.Windows.Forms.Form
	{
		/// <summary>
		/// Declare the  
		/// </summary>
		#region _MyMembers
		private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@DEST' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@ORIG')) AND (FTYP='O' OR FTYP='S')AND (DCD1 LIKE '@@DELCODE' OR DCD2 LIKE '@@DELCODE')";	
		private string strLogicalFields = "DELCODE,DELRES,ALC,NOARRFL,NODEPFL";
		private string strWhereRawDEN = "WHERE (DECA = '@@DELCODE' OR DECN = '@@DELCODE')";
		/// <summary>
		/// The length of each column
		/// </summary>
		private string strTabHeaderLens = "80,150,60,130,130"; 
		private string strTabHeader = "Delay Code,Delay Reason,Airlines,No. of Arriv Flights,No. of Dept Flights";
		private IDatabase myDB;
		private ITable myAFT;
		private ITable myDEN;
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;

		private class DelayCodeRowData
		{
			public string strDelayCode = "";
			public string strDelayReason = "";
			public string strAirline = "";
			public string strFlightNo = "";
			public int imNoOfArrivalFlights = 0;
			public int imNoOfDepartureFlights = 0;			
		}
		#endregion _MyMembers

		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxDelayCode;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button buttonPrintPView;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button buttonHelp;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		/// <summary>
		/// Constructor 
		/// </summary>
		public frmStatDelayReasons()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStatDelayReasons));
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxDelayCode = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.buttonPrintPView = new System.Windows.Forms.Button();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(264, 8);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(224, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 6;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(88, 8);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "Date from:";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(8, 74);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 9;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// panelBody
			// 
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Location = new System.Drawing.Point(0, 112);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(752, 408);
			this.panelBody.TabIndex = 10;
			// 
			// panelTab
			// 
			this.panelTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(752, 392);
			this.panelTab.TabIndex = 3;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(752, 392);
			this.tabResult.TabIndex = 1;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(752, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 44);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 11;
			this.label3.Text = "Delay Code :";
			// 
			// textBoxDelayCode
			// 
			this.textBoxDelayCode.Location = new System.Drawing.Point(88, 40);
			this.textBoxDelayCode.Name = "textBoxDelayCode";
			this.textBoxDelayCode.Size = new System.Drawing.Size(128, 20);
			this.textBoxDelayCode.TabIndex = 12;
			this.textBoxDelayCode.Text = "";
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(244, 74);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 20;
			this.btnCancel.Text = "&Close";
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(167, 74);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 19;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// buttonPrintPView
			// 
			this.buttonPrintPView.BackColor = System.Drawing.SystemColors.Control;
			this.buttonPrintPView.Location = new System.Drawing.Point(85, 74);
			this.buttonPrintPView.Name = "buttonPrintPView";
			this.buttonPrintPView.Size = new System.Drawing.Size(80, 23);
			this.buttonPrintPView.TabIndex = 18;
			this.buttonPrintPView.Text = "&Print Preview";
			this.buttonPrintPView.Click += new System.EventHandler(this.buttonPrintPView_Click);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(456, 49);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(456, 73);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 21;
			this.progressBar1.Visible = false;
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(321, 74);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 30;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// frmStatDelayReasons
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(752, 520);
			this.Controls.Add(this.buttonHelp);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLoadPrint);
			this.Controls.Add(this.buttonPrintPView);
			this.Controls.Add(this.textBoxDelayCode);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.dtTo);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.dtFrom);
			this.Controls.Add(this.label1);
			this.Name = "frmStatDelayReasons";
			this.Text = "Delay Reasons (Summary)";
			this.Load += new System.EventHandler(this.frmStatDelayReasons_Load);
			this.HelpRequested += new HelpEventHandler(frmStatDelayReasons_HelpRequested);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmStatDelayReasons_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
		}
		/// <summary>
		/// Initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// InitTab is used to initialize the table to be displayed.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
			PrepareReportData();
		}
		/// <summary>
		/// Event handler for the "Load" button..
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		///LoadReportData is used to read all the required data to be displayed, from the table
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,NOSE,ALC2,ALC3,DCD1,DCD2,DTD1,DTD2", 
				"10,10,12,1,3,3,14,14,14,14,3,5,14,14,12,3,2,3,2,2,4,4", 
				"URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,NOSE,ALC2,ALC3,DCD1,DCD2,DTD1,DTD2");

			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;
			
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);

			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			int loopCnt = 1;
			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;

				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				strTmpWhere = strWhereRaw;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@DEST", UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@ORIG", UT.Hopo);
				if(textBoxDelayCode.Text.Length != 0)
				{
					strTmpWhere = strTmpWhere.Replace("@@DELCODE",textBoxDelayCode.Text);
				}
				else
				{
					strTmpWhere = strTmpWhere.Replace("@@DELCODE","%");
				}
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom = datReadFrom.AddDays(1);//+=  new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "T" || myAFT[i]["FTYP"] == "G" || 
					myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" 
					|| (myAFT[i]["DTD1"].Length == 0 && myAFT[i]["DTD2"].Length == 0))
				{
					myAFT.Remove(i);
				}
			}
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

			//Load the coresponding Delay Reasons
			strTmpWhere = strWhereRawDEN;
			if(textBoxDelayCode.Text.Length != 0)
			{
				strTmpWhere = strTmpWhere.Replace("@@DELCODE",textBoxDelayCode.Text);
			}
			else
			{
				strTmpWhere = strTmpWhere.Replace("@@DELCODE"," ");
			}
			myDB.Unbind("DEN");
			myDEN = myDB.Bind("DEN","DEN","DECA,DECN,DENA","2,2,75","DECA,DECN,DENA");
			myDEN.Clear();
			myDEN.Load(strTmpWhere);
		}
		/// <summary>
		/// PrepareReportData is used to prepare the data for the display.
		/// </summary>
		private void PrepareReportData()
		{
			if (myAFT == null)
				return;			
			System.Collections.Hashtable hashDelayCodes = new Hashtable();
			StringBuilder sb = new StringBuilder(10000);
			myAFT.Sort("ALC3",true);			
			
			for(int i = 0; i < myAFT.Count; i++)
			{	
				bool blIsDelayCodeOK = true;
				bool blIsDelayTimeOK = false;				

				if(textBoxDelayCode.Text.Length != 0 
					&& myAFT[i]["DCD1"].Trim().Equals(textBoxDelayCode.Text.Trim()) == false)
				{
					blIsDelayCodeOK = false;
				}
				
				if(myAFT[i]["ADID"] == "A")
				{
					long llDelay = 0;
					//if((myAFT[i]["DCD1"] != "" || myAFT[i]["DCD2"] != "" || myAFT[i]["DTD1"] != "" || myAFT[i]["DTD2"] != "") &&
					if(myAFT[i]["STOA"] != "" && myAFT[i]["ONBL"] != "" && myAFT[i]["STOA"] != myAFT[i]["ONBL"])// )
					{
						DateTime olStoa = UT.CedaFullDateToDateTime(myAFT[i]["STOA"]);
						DateTime olOnbl = UT.CedaFullDateToDateTime(myAFT[i]["ONBL"]);
						TimeSpan olDelay = olOnbl - olStoa;						
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if( llDelay >= 0)
						{
							blIsDelayTimeOK = true;
						}
					}
				}

				if(myAFT[i]["ADID"] == "D")
				{
					long llDelay = 0;
					//if((myAFT[i]["DCD1"] != "" || myAFT[i]["DCD2"] != "" || myAFT[i]["DTD1"] != "" || myAFT[i]["DTD2"] != "") &&
					if(myAFT[i]["STOD"] != "" && myAFT[i]["OFBL"] != "" && myAFT[i]["STOD"] != myAFT[i]["OFBL"])// )
					{
						DateTime olStod = UT.CedaFullDateToDateTime(myAFT[i]["STOD"]);
						DateTime olOfbl = UT.CedaFullDateToDateTime(myAFT[i]["OFBL"]);
						TimeSpan olDelay = olOfbl - olStod;						
						llDelay = Convert.ToInt64(olDelay.TotalMinutes);
						if(llDelay >= 0)
						{
							blIsDelayTimeOK = true;
						}
					}
				}

				if(blIsDelayTimeOK == true && blIsDelayCodeOK == true)
				{
					string strDelayCode = myAFT[i]["DCD1"] + myAFT[i]["ALC3"];
					if(myAFT[i]["DCD1"].Trim().Length == 0)
					{
						strDelayCode = "@EMPTYDELAYCODE@" + strDelayCode;
					}
					if(myAFT[i]["DCD1"].Trim().Length != 0)
					{
						if(hashDelayCodes[strDelayCode] == null)
						{
							DelayCodeRowData delayCodeRowData = new DelayCodeRowData();
							delayCodeRowData.strAirline = myAFT[i]["ALC3"];
							delayCodeRowData.strDelayCode = myAFT[i]["DCD1"];
							
							string strTmpWhere = strWhereRawDEN;
							if(myAFT[i]["DCD1"].Length != 0)
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE",myAFT[i]["DCD1"].Trim());
							}
							else
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE"," ");
							}
							strTmpWhere += " AND ALC3 = '" + myAFT[i]["ALC3"] + "'";							

							myDB.Unbind("DEN");
							myDEN = myDB.Bind("DEN","DEN","DECA,DECN,DENA","2,2,75","DECA,DECN,DENA");
							myDEN.Clear();
							myDEN.Load(strTmpWhere);

							if(myDEN.Count > 0)
							{
								delayCodeRowData.strDelayReason = myDEN[0]["DENA"];
							}

							if(myAFT[i]["ADID"] == "A")
							{
								delayCodeRowData.imNoOfArrivalFlights++;
							}
							else
							{
								delayCodeRowData.imNoOfDepartureFlights++;
							}
							delayCodeRowData.strFlightNo += myAFT[i]["FLNO"].ToString();
							hashDelayCodes.Add(strDelayCode,delayCodeRowData);
						}
						else
						{
							DelayCodeRowData delayCodeRowData = (DelayCodeRowData)hashDelayCodes[strDelayCode];
							if(delayCodeRowData.strFlightNo.IndexOf(myAFT[i]["FLNO"]) == -1)
							{
								delayCodeRowData.strFlightNo = delayCodeRowData.strFlightNo + myAFT[i]["FLNO"];
								if(myAFT[i]["ADID"] == "A")
								{
									delayCodeRowData.imNoOfArrivalFlights++;
								}
								else
								{
									delayCodeRowData.imNoOfDepartureFlights++;
								}							
							}			
						}
					}
				}

				blIsDelayCodeOK = true;
				if(textBoxDelayCode.Text.Length != 0 				
					&& myAFT[i]["DCD2"].Trim().Equals(textBoxDelayCode.Text.Trim()) == false)
				{					
					blIsDelayCodeOK = false;
				}
				if(blIsDelayTimeOK == true && blIsDelayCodeOK == true)
				{
					string strDelayCode = myAFT[i]["DCD2"] + myAFT[i]["ALC3"];;
					if(myAFT[i]["DCD2"].Trim().Length == 0)
					{
						strDelayCode = "@EMPTYDELAYCODE@" + strDelayCode;
					}
					if(myAFT[i]["DCD2"].Trim().Length != 0 || (myAFT[i]["DCD1"].Trim().Length == 0 && myAFT[i]["DCD2"].Trim().Length == 0))
					{						
						if(hashDelayCodes[strDelayCode] == null)
						{
							DelayCodeRowData delayCodeRowData = new DelayCodeRowData();
							delayCodeRowData.strAirline = myAFT[i]["ALC3"];
							delayCodeRowData.strDelayCode = myAFT[i]["DCD2"];
						
							string strTmpWhere = strWhereRawDEN;
							if(myAFT[i]["DCD2"].Length != 0)
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE",myAFT[i]["DCD2"].Trim());
							}
							else
							{
								strTmpWhere = strTmpWhere.Replace("@@DELCODE"," ");
							}
							strTmpWhere += " AND ALC3 = '" + myAFT[i]["ALC3"] + "'";

							myDB.Unbind("DEN");
							myDEN = myDB.Bind("DEN","DEN","DECA,DECN,DENA","2,2,75","DECA,DECN,DENA");
							myDEN.Clear();
							myDEN.Load(strTmpWhere);

							if(myDEN.Count > 0)
							{
								delayCodeRowData.strDelayReason = myDEN[0]["DENA"];
							}

							if(myAFT[i]["ADID"] == "A")
							{
								delayCodeRowData.imNoOfArrivalFlights++;
							}
							else
							{
								delayCodeRowData.imNoOfDepartureFlights++;
							}
							delayCodeRowData.strFlightNo += myAFT[i]["FLNO"].ToString();
							hashDelayCodes.Add(strDelayCode,delayCodeRowData);
						}
						else
						{
							DelayCodeRowData delayCodeRowData = (DelayCodeRowData)hashDelayCodes[strDelayCode];
							if(delayCodeRowData.strFlightNo.IndexOf(myAFT[i]["FLNO"]) == -1)
							{
								delayCodeRowData.strFlightNo = delayCodeRowData.strFlightNo + myAFT[i]["FLNO"];
								if(myAFT[i]["ADID"] == "A")
								{
									delayCodeRowData.imNoOfArrivalFlights++;
								}
								else
								{
									delayCodeRowData.imNoOfDepartureFlights++;
								}
							}
						}
					}
				}
			}

			IDictionaryEnumerator iDictEnumerator = hashDelayCodes.GetEnumerator();
			
			while(iDictEnumerator.MoveNext() == true)
			{
				string strValues = "";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).strDelayCode + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).strDelayReason + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).strAirline + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).imNoOfArrivalFlights.ToString() + ",";
				strValues += ((DelayCodeRowData)iDictEnumerator.Value).imNoOfDepartureFlights.ToString() + "," + "\n";
				sb.Append(strValues);
			}			

			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Sort("0",true,true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Event Handler for the Print Preview button 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonPrintPView_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// this function initializes the preview page with headers and footers
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();
			strSubHeader.Append("From: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'23:59"));
			strSubHeader.Append(" (Delay Reasons)");
			strReportHeader = "Delay Reasons (Summary)";
			strReportSubHeader = strSubHeader.ToString();
			
			//rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Statistics - Arrival / Departure", strSubHeader.ToString(), "", 8);
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,strReportHeader,strSubHeader.ToString(),"",8);
			//rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmStatDelayReasons_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();

		}
		/// <summary>
		/// Event Handler for the Load and Preview button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Event Handler for the double click of the LButton mouse on the column header of the table.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmStatDelayReasons_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmStatDelayReasons_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmStatDelayReasons_MouseLeave);
		}

		private void frmStatDelayReasons_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmStatDelayReasons_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmStatDelayReasons_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmStatDelayReasons_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabResult,strReportHeader,strReportSubHeader,strTabHeaderLens,8,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmStatDelayReasons_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
