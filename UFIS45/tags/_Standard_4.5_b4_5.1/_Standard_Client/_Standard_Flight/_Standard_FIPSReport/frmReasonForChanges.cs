using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using System.Text;
namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmReasonForChanges. This class is created for the support of the
	/// Reasons for changes.This is created while solving the PRF 8523
	/// </summary>
	public class frmReasonForChanges : System.Windows.Forms.Form
	{
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBoxResource;
		private System.Windows.Forms.CheckBox checkBoxPos;
		private System.Windows.Forms.CheckBox checkBoxGat;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Button btnLoad;
		private int iTotalFlights = 0;
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		//private string strWhereRaw = "WHERE CDAT BETWEEN '@@FROM' AND '@@TO'"; //igu on 29.03.2012
        private string strWhereRaw = "WHERE CDAT BETWEEN '{0}' AND '{1}'"; //igu on 29.03.2012
		
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "FLNO,FTYP,SCDT,ALOC,CDAT,OVAL,NVAL,CODE,REMA,USEC";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string strTabHeader ="FlightNo,FlightType,ScheduleTime,AllocationField,TimeOfChange,OldValue,NewValue,ReasonCode,Reason,User";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "80,80,90,90,90,60,65,80,200,60";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		private ITable myCRA;
		private ITable myCRC;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		#endregion _My Members
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button buttonPrintPView;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button buttonHelp;
		
		private System.ComponentModel.Container components = null;
		/// <summary>
		/// The constructor
		/// </summary>
		public frmReasonForChanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmReasonForChanges));
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBoxResource = new System.Windows.Forms.GroupBox();
			this.checkBoxGat = new System.Windows.Forms.CheckBox();
			this.checkBoxPos = new System.Windows.Forms.CheckBox();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.buttonPrintPView = new System.Windows.Forms.Button();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.groupBoxResource.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(272, 8);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 6;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(96, 8);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "Date from:";
			// 
			// groupBoxResource
			// 
			this.groupBoxResource.Controls.Add(this.checkBoxGat);
			this.groupBoxResource.Controls.Add(this.checkBoxPos);
			this.groupBoxResource.Location = new System.Drawing.Point(16, 40);
			this.groupBoxResource.Name = "groupBoxResource";
			this.groupBoxResource.Size = new System.Drawing.Size(376, 64);
			this.groupBoxResource.TabIndex = 7;
			this.groupBoxResource.TabStop = false;
			this.groupBoxResource.Text = "Resources...";
			// 
			// checkBoxGat
			// 
			this.checkBoxGat.Location = new System.Drawing.Point(224, 24);
			this.checkBoxGat.Name = "checkBoxGat";
			this.checkBoxGat.TabIndex = 1;
			this.checkBoxGat.Text = "Gates";
			// 
			// checkBoxPos
			// 
			this.checkBoxPos.Location = new System.Drawing.Point(40, 24);
			this.checkBoxPos.Name = "checkBoxPos";
			this.checkBoxPos.TabIndex = 0;
			this.checkBoxPos.Text = "Positions";
			// 
			// panelBody
			// 
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Location = new System.Drawing.Point(0, 144);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(944, 384);
			this.panelBody.TabIndex = 8;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(944, 368);
			this.panelTab.TabIndex = 3;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(944, 368);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(944, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnLoad
			// 
			this.btnLoad.BackColor = System.Drawing.SystemColors.Control;
			this.btnLoad.Location = new System.Drawing.Point(16, 112);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.TabIndex = 13;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(251, 112);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 20;
			this.btnCancel.Text = "&Close";
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(174, 112);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 19;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// buttonPrintPView
			// 
			this.buttonPrintPView.BackColor = System.Drawing.SystemColors.Control;
			this.buttonPrintPView.Location = new System.Drawing.Point(92, 112);
			this.buttonPrintPView.Name = "buttonPrintPView";
			this.buttonPrintPView.Size = new System.Drawing.Size(80, 23);
			this.buttonPrintPView.TabIndex = 18;
			this.buttonPrintPView.Text = "&Print Preview";
			this.buttonPrintPView.Click += new System.EventHandler(this.buttonPrintPView_Click);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(520, 88);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(520, 112);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 21;
			this.progressBar1.Visible = false;
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(328, 112);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 30;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// frmReasonForChanges
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(944, 525);
			this.Controls.Add(this.buttonHelp);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLoadPrint);
			this.Controls.Add(this.buttonPrintPView);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.groupBoxResource);
			this.Controls.Add(this.dtTo);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.dtFrom);
			this.Controls.Add(this.label1);
			this.Name = "frmReasonForChanges";
			this.Text = "Reason for Changes";
			this.Load += new System.EventHandler(this.frmReasonForChanges_Load);
			this.HelpRequested += new HelpEventHandler(frmReasonForChanges_HelpRequested);
			this.groupBoxResource.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		/// <summary>
		/// Initializes the report while loading
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmReasonForChanges_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
		}

		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the table.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
			checkBoxPos.Checked = true;
		}
		/// <summary>
		/// Event Handler for the Load button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the from date and to date.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			//			TimeSpan span = dtTo.Value - dtFrom.Value;
			//			if(span.TotalDays > 10)
			//			{
			//				strRet += ilErrorCount.ToString() +  ". Please do not load more the 10 days!\n";
			//				ilErrorCount++;
			//			}
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Reads the necessary data from the table for the display
		/// </summary>
		private void LoadReportData()
		{
            //string strTmpWhere = strWhereRaw; igu on 29.03.2012
         
			//Clear the tab
			tabResult.ResetContent();
			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			myDB.Unbind("CRA");
			myDB.Unbind("CRC");
			myCRA = myDB.Bind("CRA", "CRA", "URNO,CURN,FURN,ALOC,OVAL,NVAL,USEC,CDAT,HOPO", 
				"10,10,10,10,10,10,32,14,3", 
				"URNO,CURN,FURN,ALOC,OVAL,NVAL,USEC,CDAT,HOPO");

			myCRA.Clear();
			myCRA.TimeFields = "CDAT";
			myCRA.Command("read",",GFR,");
			myCRA.TimeFieldsInitiallyInUtc = true;
			TimeSpan tsDays = (datTo - datFrom);
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) 
				ilTotal = 1;
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			int loopCnt = 1;
			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) 
					datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

                //igu on 29.03.2012
				//strTmpWhere = strTmpWhere.Replace("@@FROM",strDateFrom);
				//strTmpWhere = strTmpWhere.Replace("@@TO",strDateTo);
                string strTmpWhere = string.Format(strWhereRaw, strDateFrom, strDateTo);
                //

				myCRA.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			} while(datReadFrom <= datReadTo);
			myCRA.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			//-------------------------------------------------------------------
			//Load the flights based on the FURNO in the CRATAB
			myDB.Unbind("AFT");	
			myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,FTYP,ADID,STOA,STOD", 
				"10,12,1,1,14,14","URNO,FLNO,FTYP,ADID,STOA,STOD");
			
			lblProgress.Text = "Loading Aircrafts Data and Reason Code data";
			lblProgress.Refresh();
			int loops = 0;
			progressBar1.Value = 0;
			progressBar1.Refresh();
			ilTotal = (int)(myCRA.Count/300);
			if(ilTotal == 0) ilTotal = 1;

			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD";
			myAFT.Command("read",",GFR,");
			myAFT.TimeFieldsInitiallyInUtc = true;

			string strFlUrnos = "";
			string strFlCurnos = "";
			for(int i = 0; i < myCRA.Count; i++)
			{
                //igu on 29.03.2012
                //if((checkBoxPos.Checked) && !(checkBoxGat.Checked))
                //{
                //    if((myCRA[i]["ALOC"] == "GTA1") || (myCRA[i]["ALOC"] == "GTA2")
                //        || (myCRA[i]["ALOC"] == "GTD1") || (myCRA[i]["ALOC"] == "GTD2"))
                //    {
                //        continue ;
                //    }
                //}
                //if((checkBoxPos.Checked) && !(checkBoxGat.Checked))
                //{
                //    if((myCRA[i]["ALOC"] == "PSTA") || (myCRA[i]["PSTD"] == "PSTD"))
                //    {
                //        continue ;
                //    }
                //}
                if (!IsWithinCriteria(myCRA[i]["ALOC"], checkBoxPos.Checked, checkBoxGat.Checked))
                    continue;
                //                
				strFlUrnos += myCRA[i]["FURN"] + ",";
				strFlCurnos+= myCRA[i]["CURN"] + ","; 
				loops++;
				int percent = Convert.ToInt32((loops * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
			}
			if(strFlUrnos.Length > 0)
			{
				strFlUrnos = strFlUrnos.Remove(strFlUrnos.Length-1, 1);
				//string olstrWhereAFT = "Where URNO =" + strFlUrnos //igu on 29.03.2012
                string olstrWhereAFT = "Where URNO IN (" + strFlUrnos + ")"; //igu on 29.03.2012
				myAFT.Load(olstrWhereAFT);
			}
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
            myAFT.CreateIndex("URNO", "URNO"); //igu on 29.03.2012
			//-------------------------------------------------------------------
			//Load the records based on the CURNO from the CRCTAB
			myDB.Unbind("CRC");
			myCRC = myDB.Bind("CRC","CRC","URNO,CODE,REMA","10,20,20","URNO,CODE,REMA");
			
			if(strFlCurnos.Length > 0)
			{
				strFlCurnos = strFlCurnos.Remove(strFlCurnos.Length-1,1);
                //string olstrWhereCRC = "Where URNO =" + strFlCurnos; //igu on 29.03.2012
                string olstrWhereCRC = "Where URNO IN (" + strFlCurnos + ")"; //igu on 29.03.2012
				myCRC.Load(olstrWhereCRC);
			}
            myCRC.CreateIndex("URNO", "URNO"); //igu on 29.03.2012
			lblProgress.Text = "";
			progressBar1.Hide();
		}
        //igu on 29.03.2012
        /// <summary>
        /// Checks whether the current CRA record satisfies the criteria
        /// </summary>
        /// <returns>True or false.</returns>
        private bool IsWithinCriteria(string allocation, bool includePosition, bool includeGate)
        {
            string[] arrGateFields = new string[] { "GTA1", "GTA2", "GTD1", "GTD2" };
            string[] arrPosFields = new string[] { "PSTA", "PSTD" };

            bool bWithinCriteria = false;
            if (includePosition)
                bWithinCriteria = (Array.IndexOf(arrPosFields, allocation) >= 0);
            if (!bWithinCriteria)
            {
                if (includeGate)
                    bWithinCriteria = (Array.IndexOf(arrGateFields, allocation) >= 0);
            }
            return bWithinCriteria;
        }
		/// <summary>
		/// Prepares the data for the display in the report
		/// </summary>
		private void PrepareReportData()
		{
			iTotalFlights = 0;
			if (myCRA == null)
				return;
			StringBuilder sb = new StringBuilder(10000);

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm";
			}

			IRow [] rows;
			for (int i = 0; i < myCRA.Count; i++)
			{
                //igu on 29.03.2012
                if (!IsWithinCriteria(myCRA[i]["ALOC"], checkBoxPos.Checked, checkBoxGat.Checked))
                    continue;
                //

				string strFlightValues = "";
				string strCrcValues = "";
				string strFlightTyp = "";
				string strValues = "";
				//Get the Flight details from AFT table...
				rows = myAFT.RowsByIndexValue("URNO", myCRA[i]["FURN"]);
                if (rows != null) //igu on 29.03.2012
                {
                    for (int j = 0; j < rows.Length; j++)
                    {
                        strFlightValues = rows[j]["FLNO"] + ",";
                        strFlightValues += rows[j]["ADID"] + ",";
                        strFlightTyp = rows[j]["ADID"];
                        if (strFlightTyp.Equals("A"))
                            strFlightValues += Helper.DateString(rows[j]["STOA"], strDateDisplayFormat) + ",";
                        else
                            strFlightValues += Helper.DateString(rows[j]["STOD"], strDateDisplayFormat) + ",";
                    }
                }
				if(strFlightValues.Length > 0)
					strValues = strFlightValues;
				else
					strValues = ",,,";
				strValues += myCRA[i]["ALOC"] + "," ;
				//strValues += myCRA[i]["CDAT"] + "," ; igu on 29/03/2012
                strValues += Helper.DateString(myCRA[i]["CDAT"], strDateDisplayFormat) + ","; //igu on 29/03/2012
				strValues += myCRA[i]["OVAL"] + "," ;
				strValues += myCRA[i]["NVAL"] + "," ;

				//Get the Data from CRC table..
				rows = myCRC.RowsByIndexValue("URNO", myCRA[i]["CURN"]);
                if (rows != null) //igu on 29.03.2012
                {
                    for (int k = 0; k < rows.Length; k++)
                    {
                        strCrcValues = rows[k]["CODE"] + ",";
                        strCrcValues += rows[k]["REMA"] + ",";
                    }
                }
				if(strCrcValues.Length > 0)
					strValues += strCrcValues;
				else
					strValues += ",,";
				
				strValues = strValues.Remove(strValues.Length-1, 1);
				strValues += "\n";
				iTotalFlights++;
				sb.Append(strValues);
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Refresh();
			progressBar1.Visible = false;
			lblProgress.Text = "";
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Event handler for the Print Preview button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonPrintPView_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Intializes the Preview page 
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();
			strSubHeader.Append("From: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append(dtFrom.Value.ToString("dd.MM.yy'/'23:59"));
			strSubHeader.Append(" (Resons: ").Append(iTotalFlights.ToString());
			strSubHeader.Append(")");

			strReportHeader = "Reason for Changes ";
			strReportSubHeader = strSubHeader.ToString();
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,strReportHeader,strSubHeader.ToString(),"",8);
			//rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmReasonForChanges_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();

		}
		/// <summary>
		/// Event handler for the Load and Preview button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			lblProgress.Text = "";
			PrepareReportData();

			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Event handler for the double click of the LButton mouse on the column header of the table
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmReasonForChanges_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmReasonForChanges_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmReasonForChanges_MouseLeave);
		}

		private void frmReasonForChanges_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmReasonForChanges_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmReasonForChanges_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmReasonForChanges_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabResult,strReportHeader,strReportSubHeader,strTabHeaderLens,8,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmReasonForChanges_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
