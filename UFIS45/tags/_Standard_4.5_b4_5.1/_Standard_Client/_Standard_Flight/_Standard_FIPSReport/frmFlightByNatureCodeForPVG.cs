using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmFlightByNatureCode.
	/// </summary>
	public class frmFlightByNatureCodeForPVG : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))";

		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;

		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");

		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "FLNO,ADID,ORGDES,VIAA,SCHEDULE,ACTUAL,ACTYPE,ONOFBL,TTYP,REGN";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes ==> for Rotation
		/// </summary>
		private string strLogicalFieldsRotation = "SORT,FLNO_A,ORG,VIA_A,SCHED_A,ACTUAL_A,ACT_A,ONBL,TTYP_A,REGN_A,FLNO_D,VIA_D,DES,SCHED_D,ACT_D,ACTUAL_D,OFBL,TTYP_D,REGN_D";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string strTabHeader =     "Flight  ,A/D  ,Org/Des  ,Via ,Schedule  ,Actual  ,A/C  , On/Ofbl  ,Na  ,Reg";
		/// The header columns, which must be used for the report output
		/// for rotation
		/// </summary>
		private string strTabHeaderRotation = "Sort,Arrival  ,Org  ,Via ,Schedule  ,Actual  ,A/C  , Onbl  ,Na  ,Reg,Depart.  ,Des  ,Via ,Schedule  ,Actual  ,A/C  , Ofbl  ,Na  ,Reg";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "80,30,50,50,70,70,40,60,40,70";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// for rotation
		/// </summary>
		private string strTabHeaderLensRotation = "0,65,50,50,70,70,40,60,40,65,65,50,50,70,70,40,60,40,65";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		#endregion _My Members

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label lblProgress;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmFlightByNatureCodeForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "FlightByNatureCode";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmFlightByNatureCodeForPVG));
			this.panelTop = new System.Windows.Forms.Panel();
			this.ucView1 = new UserControls.ucView();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(752, 96);
			this.panelTop.TabIndex = 0;
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(12, 4);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 29;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 40);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 56);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 13;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 56);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 10;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 56);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 9;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 56);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 56);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 8;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 96);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(752, 338);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(752, 322);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(752, 322);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(752, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmFlightByNatureCodeForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(752, 434);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmFlightByNatureCodeForPVG";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Flights By Natures / Aircrafts / Airlines / Origin / Destination / Registration /" +
				" Owner ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmFlightByNatureCodeForPVG_Closing);
			this.Load += new System.EventHandler(this.frmFlightByNatureCodeForPVG_Load);
			this.panelTop.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmFlightByNatureCodeForPVG_Load(object sender, System.EventArgs e)
		{
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			if (this.ucView1.Viewer.Rotation == false)
			{
				tabResult.HeaderString = strTabHeader;
				tabResult.LogicalFieldList = strLogicalFields;
				tabResult.HeaderLengthString = strTabHeaderLens;
				PrepareReportData();
			}
			else
			{
				tabResult.HeaderString = strTabHeaderRotation;
				tabResult.LogicalFieldList = strLogicalFieldsRotation;
				tabResult.HeaderLengthString = strTabHeaderLensRotation;
				PrepareReportDataRotations();
			}
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{

		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}

			this.Cursor = Cursors.WaitCursor;
			InitTab();
			LoadReportData();
			if(this.ucView1.Viewer.Rotation == false)
				PrepareReportData();
			else
				PrepareReportDataRotations();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{

			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}
			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,VIA3", 
							  "10,10,12,1,3,3,14,14,14,14,3,5,14,14,12,3", 
							  "URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,VIA3");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

				string strTmpWhere = "WHERE ";
				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawD;
					strTmpWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );

				if (this.ucView1.Viewer.Rotation)
				{
					strTmpWhere += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpWhere += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}

				
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom = datReadFrom.AddDays(1);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations()
		{
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;

			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			StringBuilder sb = new StringBuilder(10000);

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm";
			}

			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					++this.imRotations;
					++this.imArrivals;
					++this.imTotalFlights;
					++this.imDepartures;
					++this.imTotalFlights;

					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						//The arrival part of rotation
						strValues += myAFT[llCurr]["STOA"] + myAFT[llCurr+1]["STOD"] + ",";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["ORG3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateDisplayFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFA"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ONBL"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += myAFT[llCurr]["REGN"] + ",";
						//-----------------------------------The departure part of rotation
						strValues += myAFT[llCurr+1]["FLNO"] + ",";
						strValues += myAFT[llCurr+1]["DES3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr+1]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["STOD"], strDateDisplayFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["TIFD"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr+1]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["OFBL"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr+1]["TTYP"] + ",";
						strValues += myAFT[llCurr+1]["REGN"] + "\n";
						sb.Append(strValues);
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						//The arrival part of rotation
						strValues += myAFT[llCurr+1]["STOA"] + myAFT[llCurr]["STOD"]  + ",";
						strValues += myAFT[llCurr+1]["FLNO"] + ",";
						strValues += myAFT[llCurr+1]["ORG3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr+1]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["STOA"], strDateDisplayFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["TIFA"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr+1]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr+1]["ONBL"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr+1]["TTYP"] + ",";
						strValues += myAFT[llCurr+1]["REGN"] + ",";
						//-----------------------------------The departure part of rotation
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["DES3"] + ",";
						//Changed for the PRF 8523
						strValues += myAFT[llCurr]["VIA3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOD"], strDateDisplayFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFD"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += myAFT[llCurr]["REGN"] + "\n";
						sb.Append(strValues);
					}

				}//if(strCurrRkey == strNextRkey)
				else
				{
					++this.imTotalFlights;
					ilStep = 1;
					if( strCurrAdid == "A")
					{
						++this.imArrivals;
						strValues += myAFT[llCurr]["STOA"] + ",";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["ORG3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateDisplayFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFA"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["ONBL"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += myAFT[llCurr]["REGN"] + ",";
						strValues += ",,,,,,,\n";
						sb.Append(strValues);
					}
					else if( strCurrAdid == "D")
					{
						++this.imDepartures;
						strValues += myAFT[llCurr]["STOD"] + ",,,,,,,,,";
						strValues += myAFT[llCurr]["FLNO"] + ",";
						strValues += myAFT[llCurr]["DES3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["STOD"], strDateDisplayFormat) + ",";
						strValues += Helper.DateString(myAFT[llCurr]["TIFD"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["ACT3"] + ",";
						strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateDisplayFormat) + ",";
						strValues += myAFT[llCurr]["TTYP"] + ",";
						strValues += myAFT[llCurr]["REGN"] + "\n";
						sb.Append(strValues);
					}
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			tabResult.Sort("0", true, true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			if(myAFT == null) 
				return;
			string strSchedule = "";
			DateTime tmpDate;
			lblProgress.Text = "Preparing Data";
			lblProgress.Refresh();
			progressBar1.Visible = true;
			progressBar1.Value = 0;
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;
			StringBuilder sb = new StringBuilder(10000);

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm";
			}
			
			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
				string strAdid = myAFT[i]["ADID"];
				if( i % 100 == 0)
				{
					int percent = Convert.ToInt32((i * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
				}
				if(strAdid == "A")
				{
					imArrivals++;
					imTotalFlights++;
					strValues += myAFT[i]["FLNO"] + ",";
					strValues += strAdid + ",";
					strValues += myAFT[i]["ORG3"] + ",";
					//changed for the PRF 8523
					strValues += myAFT[i]["VIA3"] + ",";
					strValues += Helper.DateString(myAFT[i]["STOA"], strDateDisplayFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["TIFA"], strDateDisplayFormat) + ",";
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += Helper.DateString(myAFT[i]["ONBL"], strDateDisplayFormat) + ",";
					strValues += myAFT[i]["TTYP"] + ",";
					strValues += myAFT[i]["REGN"] + "\n";
					sb.Append(strValues);
				}
				if(strAdid == "D")
				{
					imDepartures++;
					imTotalFlights++;
					strValues += myAFT[i]["FLNO"] + ",";
					strValues += strAdid + ",";
					strValues += myAFT[i]["DES3"] + ",";
					//Changed for the PRF 8523
					strValues += myAFT[i]["VIA3"] + ",";
					strValues += Helper.DateString(myAFT[i]["STOD"], strDateDisplayFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["TIFD"], strDateDisplayFormat) + ",";
					tmpDate = UT.CedaFullDateToDateTime(strSchedule);
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += Helper.DateString(myAFT[i]["OFBL"], strDateDisplayFormat) + ",";
					strValues += myAFT[i]["TTYP"] + ",";
					strValues += myAFT[i]["REGN"] + "\n";
					sb.Append(strValues);
				}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			tabResult.Sort("4", true, true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm") + " to " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += " View: " + this.ucView1.Viewer.CurrentView;
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " ARR: " + imArrivals.ToString();
			strSubHeader += " DEP: " + imDepartures.ToString() + ")";
			if (this.ucView1.Viewer.Rotation == false)
			{
				rptFIPS rpt = new rptFIPS(tabResult, frmMain.strPrefixVal + 
					"Flights by Nat/A-C/Airline/Org/Des/Reg/Own:",strSubHeader, "", 10);
				rpt.TextBoxCanGrow = false;
				frmPrintPreview frm = new frmPrintPreview(rpt);
				frm.Show();
			}
			else
			{
				prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,frmMain.strPrefixVal +  
					"Flights by Nat/A-C/Airline/Org/Des/Reg: View:" + this.ucView1.Viewer.CurrentView +"]", 
					strSubHeader, "", 10);
				rpt.TextBoxCanGrow = false;
				frmPrintPreview frm = new frmPrintPreview(rpt);
				frm.Show();
			}
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}

			this.Cursor = Cursors.WaitCursor;
			InitTab();
			LoadReportData();
			if(this.ucView1.Viewer.Rotation == false)
				PrepareReportData();
			else
				PrepareReportDataRotations();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmFlightByNatureCodeForPVG_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
		}

		private void label9_Click(object sender, System.EventArgs e)
		{
		
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}

			return strRet;
		}

		private void ucView1_viewEvent(string view)
		{
			btnOK_Click(this,null);
		}
	}
}
