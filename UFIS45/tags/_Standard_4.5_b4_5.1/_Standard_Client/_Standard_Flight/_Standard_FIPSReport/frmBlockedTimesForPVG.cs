using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using ZedGraph;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmBlockedTimes.
	/// </summary>
	public class frmBlockedTimesForPVG : System.Windows.Forms.Form
	{
		#region _My_Members
		/// <summary>
		/// The where statement for Arrivals: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawA =  "(TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B')) AND PSTA <> ' ')";

		private string strWhereRawD =  "(TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B')) AND PSTD <> ' ')" ;

		private string strWhereRawB = string.Format("({0} OR {1})","(TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B')) AND PSTA <> ' ')","(TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B')) AND PSTD <> ' ')");

		/// <summary>
		/// Where statement for the PSTTAB
		/// </summary>
		private string strWherePst = "WHERE (VAFR<'@@TO' AND (VATO >='@@FROM' OR VATO=' '))";
		private string strWhereGate = "WHERE (VAFR<'@@TO' AND (VATO >='@@FROM' OR VATO=' '))";
		private string strWhereCheckin = "WHERE (VAFR<'@@TO' AND (VATO >='@@FROM' OR VATO=' '))";
		private string strWhereBelt = "WHERE (VAFR<'@@TO' AND (VATO >='@@FROM' OR VATO=' '))";
		/// <summary>
		/// Where statement for the BLKTAB
		/// </summary>
		private string strWherePosBlk = "WHERE BURN IN (@@URNOS) AND TABN='PST'";
		private string strWhereGateBlk = "WHERE BURN IN (@@URNOS) AND TABN='GAT'";
		private string strWhereBeltBlk = "WHERE BURN IN (@@URNOS) AND TABN='BLT'";
		private string strWhereCheckinBlk = "WHERE BURN IN (@@URNOS) AND TABN='CIC'";
		private string strLogicalFields = "BURN,NAFR,NATO";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader ="Name,Blocked From,Blocked To";
		private string strPositionTabHeader ="Position,Blocked From,Blocked To";
		private string strCheckinTabHeader="Counter,Blocked From,Blocked To";
		private string strGateTabHeader="Gate Name,Blocked From,Blocked To";
		private string strBeltTabHeader="Belt Name,Blocked From,Blocked To";
		private string strTabHeaderLens = "80,90,90";

		private string strPositionGraphHeader ="Total Number of Blocked Positions";
		private string strCheckinGraphHeader="Total Number of Blocked Check-in Counters";
		private string strGateGraphHeader="Total Number of Blocked Gates";
		private string strBeltGraphHeader="Total Number of Blocked Baggage Belts";

		private string strYPosition= "Number of Positions Blocked";
		private string strYCheckin= "Number of Check-in Counters Blocked";
		private string strYGates= "Number of Gates Blocked";
		private string strYBelts= "Number of Belts Blocked";

		private string strBarPosition= "Blocked Positions";
		private string strBarCheckin= "Blocked Check-in Counters";
		private string strBarGates= "Blocked Gates";
		private string strBarBelts= "Blocked Belts";
		/// <summary>
		/// Loaded Timeframe From 
		/// </summary>
		private DateTime TimeFrameFrom;
		/// <summary>
		/// Loaded Timeframe To 
		/// </summary>
		private DateTime TimeFrameTo;
		/// <summary>
		/// Necessary for none valid times
		/// </summary>
		private DateTime TIMENULL = new DateTime(0);
		/// <summary>
		/// Chart values for the blockings/none validities
		/// </summary>
		private double [] YBlockingData;
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the CICTAB
		/// </summary>
		private ITable myCIC;
		/// <summary>
		/// ITable object for the PSTTAB
		/// </summary>
		private ITable myPST;
		/// <summary>
		/// ITable object for the GATTAB
		/// </summary>
		private ITable myGAT;
		/// <summary>
		/// ITable object for the BLTTAB
		/// </summary>
		private ITable myBLT;
		/// <summary>
		/// ITable object for the BLKTAB
		/// </summary>
		private ITable myBLK;
		/// <summary>
		/// ITable object for the result Availability table
		/// </summary>
		private ITable resAvailTable;
		/// <summary>
		/// ITable object for the result BLKTAB
		/// </summary>
		private ITable resBlkTable;
		
		/// <summary>
		/// iHours gives the number of Hou-bars to be shown in the chart
		/// </summary>
		private int iHours = 0;

		/// <summary>
		/// strArrivalTimeKey identifies the afttab field to be loaded into iTable for Arrivals
		/// </summary>
		private string strArrivalTimeKey = "STOA";

		/// <summary>
		/// strDepartureTimeKey identifies the afttab field to be loaded into iTable for Departures
		/// </summary>
		private string strDepartureTimeKey = "STOD";

		/// <summary>
		/// strTimeFields identifies which timefields are used for the current time-base-selection
		/// </summary>
		private string strTimeFields = "STOA,STOD";
		/// <summary>
		/// 
		/// </summary>
		private ZedGraph.ZedGraphControl myGraph;
		

		#endregion _My_Members

		private System.Windows.Forms.Label lblResults;
		private AxTABLib.AxTAB tabResult;
		private ZedGraph.ZedGraphControl zGraph;
		private System.Windows.Forms.RadioButton rdbGraph;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton rdbGraphical;
		private System.Windows.Forms.RadioButton rdbStatData;
		private System.Windows.Forms.RadioButton rdbBelts;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnLoadData;
		private System.Windows.Forms.RadioButton rdbPosition;
		private System.Windows.Forms.RadioButton rdbGates;
		private System.Windows.Forms.RadioButton rdbCheckin;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Panel panelTop;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmBlockedTimesForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "BlockedResources";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmBlockedTimesForPVG));
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.rdbGraph = new System.Windows.Forms.RadioButton();
			this.lblResults = new System.Windows.Forms.Label();
			this.tabResult = new AxTABLib.AxTAB();
			this.zGraph = new ZedGraph.ZedGraphControl();
			this.panel1 = new System.Windows.Forms.Panel();
			this.rdbGraphical = new System.Windows.Forms.RadioButton();
			this.rdbStatData = new System.Windows.Forms.RadioButton();
			this.rdbBelts = new System.Windows.Forms.RadioButton();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnLoadData = new System.Windows.Forms.Button();
			this.rdbPosition = new System.Windows.Forms.RadioButton();
			this.rdbGates = new System.Windows.Forms.RadioButton();
			this.rdbCheckin = new System.Windows.Forms.RadioButton();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnPrint = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.panelTop = new System.Windows.Forms.Panel();
			this.ucView1 = new UserControls.ucView();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.panel1.SuspendLayout();
			this.panelTop.SuspendLayout();
			this.SuspendLayout();
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(0, 0);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.TabIndex = 0;
			// 
			// rdbGraph
			// 
			this.rdbGraph.Location = new System.Drawing.Point(0, 0);
			this.rdbGraph.Name = "rdbGraph";
			this.rdbGraph.TabIndex = 0;
			// 
			// lblResults
			// 
			this.lblResults.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 240);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(872, 16);
			this.lblResults.TabIndex = 7;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tabResult
			// 
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 256);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(872, 597);
			this.tabResult.TabIndex = 8;
			this.tabResult.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabResult_SendLButtonClick);
			// 
			// zGraph
			// 
			this.zGraph.Dock = System.Windows.Forms.DockStyle.Fill;
			this.zGraph.IsEnableHPan = true;
			this.zGraph.IsEnableVPan = true;
			this.zGraph.IsEnableZoom = true;
			this.zGraph.IsScrollY2 = false;
			this.zGraph.IsShowContextMenu = true;
			this.zGraph.IsShowHScrollBar = false;
			this.zGraph.IsShowPointValues = false;
			this.zGraph.IsShowVScrollBar = false;
			this.zGraph.IsZoomOnMouseCenter = false;
			this.zGraph.Location = new System.Drawing.Point(0, 256);
			this.zGraph.Name = "zGraph";
			this.zGraph.PanButtons = System.Windows.Forms.MouseButtons.Left;
			this.zGraph.PanButtons2 = System.Windows.Forms.MouseButtons.Middle;
			this.zGraph.PanModifierKeys = System.Windows.Forms.Keys.Shift;
			this.zGraph.PanModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zGraph.PointDateFormat = "g";
			this.zGraph.PointValueFormat = "G";
			this.zGraph.ScrollMaxX = 0;
			this.zGraph.ScrollMaxY = 0;
			this.zGraph.ScrollMaxY2 = 0;
			this.zGraph.ScrollMinX = 0;
			this.zGraph.ScrollMinY = 0;
			this.zGraph.ScrollMinY2 = 0;
			this.zGraph.Size = new System.Drawing.Size(872, 357);
			this.zGraph.TabIndex = 9;
			this.zGraph.ZoomButtons = System.Windows.Forms.MouseButtons.Left;
			this.zGraph.ZoomButtons2 = System.Windows.Forms.MouseButtons.None;
			this.zGraph.ZoomModifierKeys = System.Windows.Forms.Keys.None;
			this.zGraph.ZoomModifierKeys2 = System.Windows.Forms.Keys.None;
			this.zGraph.ZoomStepFraction = 0.1;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panel1.Controls.Add(this.rdbGraphical);
			this.panel1.Controls.Add(this.rdbStatData);
			this.panel1.Location = new System.Drawing.Point(68, 200);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(480, 36);
			this.panel1.TabIndex = 30;
			// 
			// rdbGraphical
			// 
			this.rdbGraphical.Location = new System.Drawing.Point(220, 8);
			this.rdbGraphical.Name = "rdbGraphical";
			this.rdbGraphical.Size = new System.Drawing.Size(124, 24);
			this.rdbGraphical.TabIndex = 1;
			this.rdbGraphical.Text = "Graphical Data";
			this.rdbGraphical.CheckedChanged += new System.EventHandler(this.rdbGraphical_CheckedChanged);
			// 
			// rdbStatData
			// 
			this.rdbStatData.Checked = true;
			this.rdbStatData.Location = new System.Drawing.Point(24, 8);
			this.rdbStatData.Name = "rdbStatData";
			this.rdbStatData.TabIndex = 0;
			this.rdbStatData.TabStop = true;
			this.rdbStatData.Text = "Statistical Data";
			this.rdbStatData.CheckedChanged += new System.EventHandler(this.rdbStatData_CheckedChanged);
			// 
			// rdbBelts
			// 
			this.rdbBelts.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.rdbBelts.Location = new System.Drawing.Point(200, 127);
			this.rdbBelts.Name = "rdbBelts";
			this.rdbBelts.TabIndex = 23;
			this.rdbBelts.Text = "Belts";
			this.rdbBelts.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(144, 166);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 6;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnLoadData
			// 
			this.btnLoadData.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.btnLoadData.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadData.Location = new System.Drawing.Point(64, 166);
			this.btnLoadData.Name = "btnLoadData";
			this.btnLoadData.Size = new System.Drawing.Size(76, 23);
			this.btnLoadData.TabIndex = 5;
			this.btnLoadData.Text = "&Load Data";
			this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
			// 
			// rdbPosition
			// 
			this.rdbPosition.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.rdbPosition.Checked = true;
			this.rdbPosition.Location = new System.Drawing.Point(200, 56);
			this.rdbPosition.Name = "rdbPosition";
			this.rdbPosition.TabIndex = 2;
			this.rdbPosition.TabStop = true;
			this.rdbPosition.Text = "Position";
			this.rdbPosition.CheckedChanged += new System.EventHandler(this.btnStaStd_CheckedChanged);
			// 
			// rdbGates
			// 
			this.rdbGates.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.rdbGates.Location = new System.Drawing.Point(200, 104);
			this.rdbGates.Name = "rdbGates";
			this.rdbGates.TabIndex = 4;
			this.rdbGates.Text = "Gates";
			this.rdbGates.CheckedChanged += new System.EventHandler(this.btnLandAirb_CheckedChanged);
			// 
			// rdbCheckin
			// 
			this.rdbCheckin.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.rdbCheckin.Location = new System.Drawing.Point(200, 80);
			this.rdbCheckin.Name = "rdbCheckin";
			this.rdbCheckin.Size = new System.Drawing.Size(136, 24);
			this.rdbCheckin.TabIndex = 3;
			this.rdbCheckin.Text = "Check-in Counters";
			this.rdbCheckin.CheckedChanged += new System.EventHandler(this.btnOnblOfbl_CheckedChanged);
			// 
			// lblProgress
			// 
			this.lblProgress.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.lblProgress.Location = new System.Drawing.Point(404, 150);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.progressBar1.Location = new System.Drawing.Point(404, 166);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 21;
			this.progressBar1.Visible = false;
			// 
			// btnPrint
			// 
			this.btnPrint.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrint.Location = new System.Drawing.Point(224, 166);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.TabIndex = 7;
			this.btnPrint.Text = "Loa&d + Print";
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(304, 166);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.panel1);
			this.panelTop.Controls.Add(this.rdbBelts);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnLoadData);
			this.panelTop.Controls.Add(this.rdbPosition);
			this.panelTop.Controls.Add(this.rdbGates);
			this.panelTop.Controls.Add(this.rdbCheckin);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnPrint);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(872, 240);
			this.panelTop.TabIndex = 5;
			this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(28, 8);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 31;
			// 
			// frmBlockedTimesForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(872, 613);
			this.Controls.Add(this.zGraph);
			this.Controls.Add(this.tabResult);
			this.Controls.Add(this.lblResults);
			this.Controls.Add(this.panelTop);
			this.Name = "frmBlockedTimesForPVG";
			this.Text = "Statistics of Blocked Resources";
			this.Load += new System.EventHandler(this.frmBlockedTimesForPVG_Load);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panelTop.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
		}

		private void frmBlockedTimesForPVG_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
		}

		private void btnStaStd_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnOnblOfbl_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnLandAirb_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void radioButton1_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void panelTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}

		private void btnLoadData_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
		
			tabResult.ResetContent();
			LoadReportData();
			if (rdbGraphical .Checked)  PrepareResourceData();
			if (rdbGraphical.Checked) PrintGraph();
			this.Cursor = Cursors.Arrow;
			if (rdbGraphical.Checked)
			{
				zGraph.Visible = true;
				tabResult.Visible =false;
			}
			if (rdbGraphical.Checked)zGraph.Refresh();
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Arrival == false || this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Both Arrival and Departure must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == false)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must be selected!\n";
				ilErrorCount++;
			}
			
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere;
			//Clear the tab

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}
			// Extract the true Nature code from the selected entry of the combobox
			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", 
				"RKEY,URNO,ADID,FLNO,PSTA,PSTD,STOA,STOD,TIFA,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES,MING", 
				"10,10,1,12,5,5,14,14,14,14,14,14,14,14,14,14,14,14,4",
				"RKEY,URNO,ADID,FLNO,PSTA,PSTD,STOA,STOD,TIFA,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES,MING");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,PABA,PABS,PAEA,PAES,PDBA,PDBS,PDEA,PDES";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = "WHERE ";

				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere += strWhereRawD;
					strTmpWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@HOPO", UT.Hopo );

				if (this.ucView1.Viewer.Rotation)
				{
					strTmpWhere += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpWhere += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}

				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			//Filter out the FTYPs which are selected due to rotations
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "X" ||
					myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" )
				{
					myAFT.Remove(i);
				}
			}
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
			
			//Now reading the PSTTAB with the valid from /to records
			myDB.Unbind("BLK_A");
			
			if (rdbPosition.Checked )
			{
				myDB.Unbind("PST");
				myDB.Unbind("PST_A");
				myDB.Unbind("BLK_A");
				myPST = myDB.Bind("PST", "PST", "URNO,PNAM,VAFR,VATO", "10,5,14,14", "URNO,PNAM,VAFR,VATO");
				myPST.Clear();
				myPST.TimeFields = "VAFR,VATO";
				myPST.TimeFieldsInitiallyInUtc = true;
				myPST.Load("");
				myPST.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				//Now reading the BLKTAB with the valid from /to records
				string strBURNs = "";
				for(int i = 0; i < myPST.Count; i++)
				{
					strBURNs += myPST[i]["URNO"] + ",";
				}
				if(strBURNs != "")
				{
					strBURNs = strBURNs.Remove(strBURNs.Length-1, 1);
				}
				myDB.Unbind("BLK");
				myBLK = myDB.Bind("BLK", "BLK", "BURN,NAFR,NATO,DAYS,TIFR,TITO", "10,14,14,7,4,4", "BURN,NAFR,NATO,DAYS,TIFR,TITO");
				myBLK.Clear();
				myBLK.TimeFields = "NAFR,NATO";
				myBLK.TimeFieldsInitiallyInUtc = true;
				string strTmpWhereBlk = strWherePosBlk;
				strTmpWhereBlk = strTmpWhereBlk.Replace("@@URNOS", strBURNs);
				myBLK.Load(strTmpWhereBlk);
				myBLK.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

				resAvailTable = myDB.Bind("PST_A", "PST", "URNO,NAME,VAFR,VATO", "10,5,14,14", "URNO,NAME,VAFR,VATO");
				resBlkTable = myDB.Bind("BLK_A", "BLK", "URNO,NAME,NAFR,NATO", "10,5,14,14", "URNO,NAME,NAFR,NATO");
				UT.GetAvailability(ref myPST,"VAFR","VATO","PNAM",ref myBLK,"NAFR","NATO",this.ucView1.Viewer.PeriodFrom ,this.ucView1.Viewer.PeriodTo ,ref resAvailTable,ref resBlkTable);

			}
			if (rdbCheckin.Checked )
			{
				myDB.Unbind("CIC");
				myDB.Unbind("CIC_A");
				myDB.Unbind("BLK_A");
				myCIC = myDB.Bind("CIC", "CIC", "URNO,CNAM,VAFR,VATO", "10,5,14,14", "URNO,CNAM,VAFR,VATO");
				myCIC.Clear();
				myCIC.TimeFields = "VAFR,VATO";
				myCIC.TimeFieldsInitiallyInUtc = true;
				myCIC.Load("");
				myCIC.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				//Now reading the BLKTAB with the valid from /to records
				string strBURNs = "";
				for(int i = 0; i < myCIC.Count; i++)
				{
					strBURNs += myCIC[i]["URNO"] + ",";
				}
				if(strBURNs != "")
				{
					strBURNs = strBURNs.Remove(strBURNs.Length-1, 1);
				}
				myDB.Unbind("BLK");
				myBLK = myDB.Bind("BLK", "BLK", "BURN,NAFR,NATO,DAYS,TIFR,TITO", "10,14,14,7,4,4", "BURN,NAFR,NATO,DAYS,TIFR,TITO");
				myBLK.Clear();
				myBLK.TimeFields = "NAFR,NATO";
				myBLK.TimeFieldsInitiallyInUtc = true;
				string strTmpWhereBlk = strWhereCheckinBlk;
				strTmpWhereBlk = strTmpWhereBlk.Replace("@@URNOS", strBURNs);
				myBLK.Load(strTmpWhereBlk);
				myBLK.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

				resAvailTable = myDB.Bind("CIC_A", "CIC", "URNO,NAME,VAFR,VATO", "10,5,14,14", "URNO,NAME,VAFR,VATO");
				resBlkTable = myDB.Bind("BLK_A", "BLK", "URNO,NAME,NAFR,NATO", "10,5,14,14", "URNO,NAME,NAFR,NATO");
				UT.GetAvailability(ref myCIC,"VAFR","VATO","CNAM",ref myBLK,"NAFR","NATO",this.ucView1.Viewer.PeriodFrom ,this.ucView1.Viewer.PeriodTo ,ref resAvailTable,ref resBlkTable);

			}
			if (rdbGates.Checked )
			{
				myDB.Unbind("GAT");
				myDB.Unbind("GAT_A");
				myDB.Unbind("BLK_A");
				myGAT = myDB.Bind("GAT", "GAT", "URNO,GNAM,VAFR,VATO", "10,5,14,14", "URNO,GNAM,VAFR,VATO");
				myGAT.Clear();
				myGAT.TimeFields = "VAFR,VATO";
				myGAT.TimeFieldsInitiallyInUtc = true;
				myGAT.Load("");
				myGAT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				//Now reading the BLKTAB with the valid from /to records
				string strBURNs = "";
				for(int i = 0; i < myGAT.Count; i++)
				{
					strBURNs += myGAT[i]["URNO"] + ",";
				}
				if(strBURNs != "")
				{
					strBURNs = strBURNs.Remove(strBURNs.Length-1, 1);
				}
				myDB.Unbind("BLK");
				myBLK = myDB.Bind("BLK", "BLK", "BURN,NAFR,NATO,DAYS,TIFR,TITO", "10,14,14,7,4,4", "BURN,NAFR,NATO,DAYS,TIFR,TITO");
				myBLK.Clear();
				myBLK.TimeFields = "NAFR,NATO";
				myBLK.TimeFieldsInitiallyInUtc = true;
				string strTmpWhereBlk = strWhereGateBlk;
				strTmpWhereBlk = strTmpWhereBlk.Replace("@@URNOS", strBURNs);
				myBLK.Load(strTmpWhereBlk);
				myBLK.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

				resAvailTable = myDB.Bind("GAT_A", "GAT", "URNO,NAME,VAFR,VATO", "10,5,14,14", "URNO,NAME,VAFR,VATO");
				resBlkTable = myDB.Bind("BLK_A", "BLK", "URNO,NAME,NAFR,NATO", "10,5,14,14", "URNO,NAME,NAFR,NATO");
				UT.GetAvailability(ref myGAT,"VAFR","VATO","GNAM",ref myBLK,"NAFR","NATO",this.ucView1.Viewer.PeriodFrom ,this.ucView1.Viewer.PeriodTo ,ref resAvailTable,ref resBlkTable);
			}

			if (rdbBelts.Checked )
			{
				myDB.Unbind("BLT");
				myDB.Unbind("BLT_A");
				myDB.Unbind("BLK_A");
				myBLT = myDB.Bind("BLT", "BLT", "URNO,BNAM,VAFR,VATO", "10,5,14,14", "URNO,BNAM,VAFR,VATO");
				myBLT.Clear();
				myBLT.TimeFields = "VAFR,VATO";
				myBLT.TimeFieldsInitiallyInUtc = true;
				myBLT.Load("");
				myBLT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				//Now reading the BLKTAB with the valid from /to records
				string strBURNs = "";
				for(int i = 0; i < myBLT.Count; i++)
				{
					strBURNs += myBLT[i]["URNO"] + ",";
				}
				if(strBURNs != "")
				{
					strBURNs = strBURNs.Remove(strBURNs.Length-1, 1);
				}
				myDB.Unbind("BLK");
				myBLK = myDB.Bind("BLK", "BLK", "BURN,NAFR,NATO,DAYS,TIFR,TITO", "10,14,14,7,4,4", "BURN,NAFR,NATO,DAYS,TIFR,TITO");
				myBLK.Clear();
				myBLK.TimeFields = "NAFR,NATO";
				myBLK.TimeFieldsInitiallyInUtc = true;
				string strTmpWhereBlk = strWhereBeltBlk ;
				strTmpWhereBlk = strTmpWhereBlk.Replace("@@URNOS", strBURNs);
				myBLK.Load(strTmpWhereBlk);
				myBLK.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

				resAvailTable = myDB.Bind("BLT_A", "BLT", "URNO,NAME,VAFR,VATO", "10,5,14,14", "URNO,NAME,VAFR,VATO");
				resBlkTable = myDB.Bind("BLK_A", "BLK", "URNO,NAME,NAFR,NATO", "10,5,14,14", "URNO,NAME,NAFR,NATO");
				UT.GetAvailability(ref myBLT,"VAFR","VATO","BNAM",ref myBLK,"NAFR","NATO",this.ucView1.Viewer.PeriodFrom ,this.ucView1.Viewer.PeriodTo ,ref resAvailTable,ref resBlkTable);
			}
		 lblProgress.Text ="";
		 PreparePositionReportData();
		}
		/// <summary>
		/// Initiaites the result tab
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString=strTabHeader;
			tabResult.LogicalFieldList=strLogicalFields;
			tabResult.HeaderLengthString= strTabHeaderLens;
			tabResult.Visible = true;
			zGraph.Visible = false;

		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PreparePositionReportData()
		{
			StringBuilder sb = new StringBuilder(10000);
			tabResult.ResetContent ();
			if (rdbPosition.Checked )tabResult.HeaderString = strPositionTabHeader ;
			if(rdbCheckin.Checked)tabResult.HeaderString = strCheckinTabHeader ;
			if(rdbGates.Checked)tabResult.HeaderString = strGateTabHeader;
			if(rdbBelts.Checked)tabResult.HeaderString = strBeltTabHeader;
			
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm:ss";
			}
			else
			{
				strDateDisplayFormat += ":ss";
			}

			for(int i = 0; i < resBlkTable.Count; i++)
			{
				string strValues = "";
					{		strValues += resBlkTable[i]["NAME"] + ",";
							strValues += Helper.DateString(resBlkTable[i]["NAFR"], strDateDisplayFormat) + ","; //ddHHmmss, earlier it was "dd'/'HH:mm:ss"
							strValues += Helper.DateString(resBlkTable[i]["NATO"], strDateDisplayFormat) +  "\n";
							sb.Append(strValues);
					}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Refresh();
			tabResult.Sort ("0,1",true,true);
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			tabResult.Visible = true;
			zGraph.Visible = false;
		}
		/// <summary>
		/// Prepare the chart values according to the 
		/// flight data
		/// </summary>
		private void PrepareResourceData()
		{
			//Prepare the hour data to get the timeframes in hours
			//to allocate the double list for the flight curve
			DateTime datFrom;
			DateTime datTo;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			datFrom = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, 0,0,0);
			datTo   = new DateTime(datTo.Year, datTo.Month, datTo.Day, 23,59,59);
			TimeFrameFrom = datFrom;
			TimeFrameTo = datTo;
			TimeSpan ts = TimeFrameTo - TimeFrameFrom;
			int ilHours = Convert.ToInt32(ts.TotalMinutes/60);
			YBlockingData    = new double[ilHours];

			for (int i = 0; i < this.resBlkTable.Count; i++)
			{
				DateTime olStart = UT.CedaFullDateToDateTime(resBlkTable[i]["NAFR"]); 
				DateTime olEnd   = UT.CedaFullDateToDateTime(resBlkTable[i]["NATO"]);
				//Fill the entire arry due to undefined end
				//and start earlier than timeframe and for 
				//the case that start and end exceed both the timeframe
				if(olStart >= datFrom && olEnd != TIMENULL)
				{
					int fromIDX = 0;
					int toIDX = 0;
					TimeSpan tmpTS = olStart - datFrom;
					fromIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
					tmpTS = olEnd - olStart;
					toIDX = Convert.ToInt32(tmpTS.TotalMinutes/60);
					for(int j = 0; j < toIDX; j++)
					{
						YBlockingData[fromIDX + j]++;
					}
				}
			}
		}
		/// <summary>
		/// Setup and draw the chart
		/// </summary>
		private void PrintGraph()
		{
			
			if(YBlockingData == null)
				return;
			
			// MaxBarValue stores maximum of all bars during the loaded timeframe
			int MaxBarValue = 0;
			ZedGraph.BarItem mySecondCurve = null;
			string strBar ="";

			ZedGraph.GraphPane myPane = zGraph.GraphPane;
			myPane.GraphItemList.Clear();
			zGraph.Controls.Clear();
			
			zGraph.ResetText ();
			myPane.CurveList.Clear();
			
			
			// Set the titles and axis labels
			if (rdbPosition.Checked )  myPane.Title = strPositionGraphHeader;
			if (rdbCheckin.Checked )  myPane.Title = strCheckinGraphHeader;
			if (rdbGates.Checked )  myPane.Title = strGateGraphHeader;
			if (rdbBelts.Checked )  myPane.Title = strBeltGraphHeader;
			myPane.MarginLeft = 0.5f;
			myPane.MarginRight = 0f;
			myPane.XAxis.Title = "time (in hours)";
			myPane.XAxis.TitleFontSpec.Size = 10f;
			if (rdbPosition.Checked )  myPane.YAxis.Title  = strYPosition;
			if (rdbCheckin.Checked )  myPane.YAxis.Title  = strYCheckin;
			if (rdbGates.Checked )  myPane.YAxis.Title  = strYGates;
			if (rdbBelts.Checked )  myPane.YAxis.Title  = strYBelts;
		 
			myPane.YAxis.ScaleFontSpec.Size = 8f;

			// Make up some random data points
			string [] str = new string[YBlockingData.Length];
			int ilCurrHour = 0;
			for(int i = 0; i < YBlockingData.Length; i++)
			{
				if(ilCurrHour == 24)
				{
					ilCurrHour = 0;
				}
				str[i] = ilCurrHour.ToString();
				ilCurrHour++;
			}
			MaxBarValue = 0;
			for (int i = 0; i < YBlockingData.Length; i++)
			{
				if(YBlockingData[i] > MaxBarValue) MaxBarValue = Convert.ToInt32(YBlockingData[i]);
			}
			// Add a bar to the graph
			if (rdbPosition.Checked )  strBar   = strBarPosition;
			if (rdbCheckin.Checked )  strBar  = strBarCheckin;
			if (rdbGates.Checked )  strBar  = strBarGates;
			if (rdbBelts.Checked )  strBar  = strBarBelts;
			ZedGraph.BarItem myCurve = myPane.AddBar( strBar, null, YBlockingData, Color.White );
			myCurve.Bar.Fill = new Fill( Color.Blue, Color.White, Color.Blue, 0F );
			// turn off the bar border
			myCurve.Bar.Border.IsVisible = false;
			
			
			// Draw the X tics between the labels instead of at the labels
			myPane.XAxis.IsTicsBetweenLabels = true;
			myPane.XAxis.ScaleFontSpec.Size = 8f;

			// Set the XAxis labels
			myPane.XAxis.TextLabels = str;

			// Set the XAxis to Text type
			myPane.XAxis.Type = AxisType.Text;

			// Fill the axis background with a color gradient
			myPane.AxisFill = new Fill( Color.White, Color.LightGray, 45.0f );

			// disable the legend
			myPane.Legend.IsVisible = true;
			
			myPane.AxisChange( zGraph.CreateGraphics());
			//if more than 3 days should be shown in the report than the values will not be written to the bars
			//because the valuse can no longer be read (oferlapping texts)
			if(YBlockingData.Length <= 72)
			{
				// The ValueHandler is a helper that does some position calculations for us.
				ValueHandler valueHandler = new ValueHandler( myPane, true );
				// Display a value for the maximum of each bar cluster
				// Shift the text items by x user scale units above the bars
				//const float shift = 0.5f;
				float shift = (3/100*MaxBarValue)*MaxBarValue;
				int ord = 0;
				foreach ( CurveItem curve in myPane.CurveList )
				{
					BarItem bar = curve as BarItem;

					if ( bar != null )
					{
						
						PointPairList points = curve.Points;

						for ( int i=0; i<points.Count; i++ )
						{
							
							double xVal = points[i].X ;//- 0.3d;

							// Calculate the Y value at the center of each bar
							//double yVal = valueHandler.BarCenterValue( curve, curve.GetBarWidth( myPane ),i, points[i].Y, ord );
							double yVal = points[i].Y;

							// format the label string to have 1 decimal place
							//string lab = xVal.ToString( "F0" );
							string lab = yVal.ToString();
				
							// don't show value "0" in the chart
							if(yVal != 0)
							{
							
								// create the text item (assumes the x axis is ordinal or text)
								// for negative bars, the label appears just above the zero value
				
								TextItem text = new TextItem( lab,(float) xVal ,(float) yVal + ( yVal >= 0 ? shift : -shift ));

								// tell Zedgraph to use user scale units for locating the TextItem
								text.Location.CoordinateFrame = CoordType.AxisXYScale;
								//text.Location.CoordinateFrame = CoordType.AxisXY2Scale;
								text.FontSpec.Size = 6;
								// AlignH the left-center of the text to the specified point
									text.Location.AlignH =  AlignH.Center;
								
								text.Location.AlignV =  yVal > 0 ? AlignV.Bottom : AlignV.Top;
								text.FontSpec.Border.IsVisible = false;
								// rotate the text 90 degrees
								text.FontSpec.Angle = 0;
								text.FontSpec.Fill.IsVisible = false;
								// add the TextItem to the list
								myPane.GraphItemList.Add( text );
							}
						}
					}
					ord++;
				}
			}
			zGraph.Show();
			zGraph.Refresh();
		}
		private void tabResult_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader1="";
			string strSubHeader2 = "";
			strSubHeader2 = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm") + " to " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader2 += " View: " + this.ucView1.Viewer.CurrentView;
			if (rdbPosition.Checked )
			{
				strSubHeader1 = "Statistics according to Blocked Positions:";
				strSubHeader2 += "(Postions: " + tabResult.GetLineCount ().ToString()+")";
			}
			if (rdbCheckin.Checked )
			{
				strSubHeader1 = "Statistics according to Blocked Check-in Counters:";
				strSubHeader2 += ""+ "(Check-in Counters: " + tabResult.GetLineCount ().ToString() +")";
			}
			if (rdbGates.Checked )
			{
				strSubHeader1 = "Statistics according to Blocked Gates:";
				strSubHeader2 += "(Gates: " + tabResult.GetLineCount ().ToString()+")";
			}
			if (rdbBelts.Checked )
			{
				strSubHeader1 = "Statistics according to Blocked Belts:";
				strSubHeader2 += "(Belts: " + tabResult.GetLineCount ().ToString()+")";
			}
			rptFIPS rpt = new rptFIPS(tabResult,strSubHeader1,strSubHeader2,"",10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			if(rdbStatData.Checked )
			{
				RunReport();
			}
			
			if (rdbGraphical.Checked )
			{
				printGraph();
			}
			
		}
		/// <summary>
		/// Prints the Graphical Data
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>

		private void printGraph()
		{
			string strHeader1 = "";
			string strHeader2 = "From: " + TimeFrameFrom.ToString("dd.MM.yy'/'HH:mm") + 
				"  To: " + TimeFrameTo.ToString("dd.MM.yy'/'HH:mm");
			string strBar ="";
			string  strYtitle ="";
			string strHeader3="";

			if (rdbPosition.Checked )  strHeader1 = strPositionGraphHeader;
			if (rdbCheckin.Checked )  strHeader1 = strCheckinGraphHeader;
			if (rdbGates.Checked )  strHeader1 = strGateGraphHeader;
			if (rdbBelts.Checked )  strHeader1 = strBeltGraphHeader;

			if (rdbPosition.Checked )  strHeader3 = strPositionGraphHeader;
			if (rdbCheckin.Checked )  strHeader3 = strCheckinGraphHeader;
			if (rdbGates.Checked )  strHeader3 = strGateGraphHeader;
			if (rdbBelts.Checked )  strHeader3 = strBeltGraphHeader;
			
			if (rdbPosition.Checked )  strYtitle  = strYPosition;
			if (rdbCheckin.Checked )  strYtitle = strYCheckin;
			if (rdbGates.Checked )  strYtitle  = strYGates;
			if (rdbBelts.Checked )  strYtitle = strYBelts;
		 
			if (rdbPosition.Checked )  strBar   = strBarPosition;
			if (rdbCheckin.Checked )  strBar  = strBarCheckin;
			if (rdbGates.Checked )  strBar  = strBarGates;
			if (rdbBelts.Checked )  strBar  = strBarBelts;
			//rptGenericChart rpt = new rptGenericChart(zGraph.GraphPane.Image);
				
			rptSpotUsing rpt = new rptSpotUsing(strHeader1, strHeader2, YBlockingData, YBlockingData, false,
				strHeader3, strYtitle,
				"Time (Hours)", strBar, "");
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
		
			tabResult.ResetContent();
			LoadReportData();
			if (rdbGraphical .Checked)  PrepareResourceData();
			if (rdbGraphical.Checked) PrintGraph();
			this.Cursor = Cursors.Arrow;
			if (rdbGraphical.Checked)
			{
				zGraph.Visible = true;
				tabResult.Visible =false;
			}
			if (rdbGraphical.Checked)zGraph.Refresh();
			if (rdbStatData.Checked ) RunReport();
			if (rdbGraphical.Checked)printGraph();
			this.Cursor = Cursors.Arrow;
		}

		private void rdbGraphical_CheckedChanged(object sender, System.EventArgs e)
		{
			if (tabResult.GetLineCount() !=0)  PrepareResourceData();
			if (tabResult.GetLineCount() !=0)  PrintGraph();
			zGraph.Visible =true;
			tabResult .Visible = false;
		}

		private void rdbStatData_CheckedChanged(object sender, System.EventArgs e)
		{
			if (tabResult.GetLineCount() !=0)  PreparePositionReportData();
			zGraph.Visible =false;
			tabResult .Visible = true;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			tabResult.ResetContent ();
			myDB.Unbind("AFT");
			myDB.Unbind("PST");
			myDB.Unbind("GAT");
			myDB.Unbind("CIC");
			myDB.Unbind("BLT");

		}

		private void ucView1_viewEvent(string view)
		{
			btnLoadData_Click(this,null);
		}
	}
}
