using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmStatistics_Arr_Dep.
	/// </summary>
	public class frmStatistics_Arr_Dep : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@DEST') OR (STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@ORIG'))  " +
			"AND (FTYP='O' OR FTYP='S') " ;
		
		private string strTerminalWhere = " AND (STEV = '@@STEV')";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "URNO,ADID,STOA,STOD";
		private string stractLogicalFields = "URNO,STOA,STOD";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string strTabHeader =     "Date  ,No.of Arr Flights  ,No.of Dep Flights ";
		private string stractTabHeader =     "Date  ,No.of Arr Flights  ,No.of Dep Flights ";
		
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "100,100,100";
		private string stractTabHeaderLens = "100,120,120";
		///<summary>
		///String that stores the previous stoa
		///</summary>
		///<summary>
		///Define a arrival collection
		///</summary>
		//private Array 

		private string strprvSTOA = "";
		///<summary>
		///String that stores the previous stoa
		///</summary>
		private string strprvSTOD = "";
		///
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// Total of arrival flights in the report on a day
		/// </summary>
//		private int iTotalcountArr = 0;
//		private int iprevcountArr = 0;
//		private int icurrcountArr = 0;
//		private int iarrv= 0;
//		private int idep = 0;
//		private int testarrv=0;
//		private int testdep=0;
		/// <summary>
		/// Total of dep flights in the report on a day
		/// </summary>
//		private int iTotalcountDep = 0;
//		private int iprevcountDep = 0;
//		private int icurrcountDep = 0;
		private int[] NumberofFlights ;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private int imTotalFlights = 0;
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;

		#endregion _My Members
		
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel pnlDep;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.CheckBox cbDepFlights;
		private System.Windows.Forms.Label lblDep;
		private System.Windows.Forms.DateTimePicker dtDepFrom;
		private System.Windows.Forms.DateTimePicker dtDepTo;
		private AxTABLib.AxTAB actualtabResult;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.Label lblTerminal;
		private System.Windows.Forms.TextBox txtTerminal;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmStatistics_Arr_Dep()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStatistics_Arr_Dep));
			this.panelTop = new System.Windows.Forms.Panel();
			this.txtTerminal = new System.Windows.Forms.TextBox();
			this.lblTerminal = new System.Windows.Forms.Label();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.pnlDep = new System.Windows.Forms.Panel();
			this.dtDepTo = new System.Windows.Forms.DateTimePicker();
			this.label4 = new System.Windows.Forms.Label();
			this.dtDepFrom = new System.Windows.Forms.DateTimePicker();
			this.lblDep = new System.Windows.Forms.Label();
			this.cbDepFlights = new System.Windows.Forms.CheckBox();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTab = new System.Windows.Forms.Panel();
			this.actualtabResult = new AxTABLib.AxTAB();
			this.tabResult = new AxTABLib.AxTAB();
			this.panelTop.SuspendLayout();
			this.pnlDep.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.actualtabResult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.txtTerminal);
			this.panelTop.Controls.Add(this.lblTerminal);
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.pnlDep);
			this.panelTop.Controls.Add(this.cbDepFlights);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(728, 104);
			this.panelTop.TabIndex = 1;
			this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
			// 
			// txtTerminal
			// 
			this.txtTerminal.Location = new System.Drawing.Point(104, 32);
			this.txtTerminal.Name = "txtTerminal";
			this.txtTerminal.Size = new System.Drawing.Size(128, 20);
			this.txtTerminal.TabIndex = 4;
			this.txtTerminal.Text = "";
			// 
			// lblTerminal
			// 
			this.lblTerminal.Location = new System.Drawing.Point(16, 36);
			this.lblTerminal.Name = "lblTerminal";
			this.lblTerminal.Size = new System.Drawing.Size(72, 16);
			this.lblTerminal.TabIndex = 31;
			this.lblTerminal.Text = "Terminal:";
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(396, 72);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 9;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// pnlDep
			// 
			this.pnlDep.Controls.Add(this.dtDepTo);
			this.pnlDep.Controls.Add(this.label4);
			this.pnlDep.Controls.Add(this.dtDepFrom);
			this.pnlDep.Controls.Add(this.lblDep);
			this.pnlDep.Location = new System.Drawing.Point(84, 56);
			this.pnlDep.Name = "pnlDep";
			this.pnlDep.Size = new System.Drawing.Size(640, 24);
			this.pnlDep.TabIndex = 24;
			this.pnlDep.Visible = false;
			// 
			// dtDepTo
			// 
			this.dtDepTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtDepTo.Location = new System.Drawing.Point(300, 2);
			this.dtDepTo.Name = "dtDepTo";
			this.dtDepTo.Size = new System.Drawing.Size(128, 20);
			this.dtDepTo.TabIndex = 4;
			this.dtDepTo.ValueChanged += new System.EventHandler(this.dtArrTo_ValueChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(252, 4);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(24, 16);
			this.label4.TabIndex = 3;
			this.label4.Text = "to:";
			// 
			// dtDepFrom
			// 
			this.dtDepFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtDepFrom.Location = new System.Drawing.Point(100, 2);
			this.dtDepFrom.Name = "dtDepFrom";
			this.dtDepFrom.Size = new System.Drawing.Size(128, 20);
			this.dtDepFrom.TabIndex = 2;
			this.dtDepFrom.ValueChanged += new System.EventHandler(this.dtDepFrom_ValueChanged);
			// 
			// lblDep
			// 
			this.lblDep.Location = new System.Drawing.Point(4, 4);
			this.lblDep.Name = "lblDep";
			this.lblDep.Size = new System.Drawing.Size(88, 16);
			this.lblDep.TabIndex = 1;
			this.lblDep.Text = "Dep Time from:";
			// 
			// cbDepFlights
			// 
			this.cbDepFlights.Location = new System.Drawing.Point(496, 4);
			this.cbDepFlights.Name = "cbDepFlights";
			this.cbDepFlights.Size = new System.Drawing.Size(160, 16);
			this.cbDepFlights.TabIndex = 3;
			this.cbDepFlights.Text = "Departure Flights";
			this.cbDepFlights.Visible = false;
			this.cbDepFlights.CheckedChanged += new System.EventHandler(this.cbTimeInLocal_CheckedChanged);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(480, 56);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(480, 72);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 72);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 7;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 72);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 6;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 72);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 8;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 72);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 5;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(304, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 3;
			this.dtTo.ValueChanged += new System.EventHandler(this.dtTo_ValueChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(256, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(104, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 1;
			this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(76, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = " Time from:";
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 104);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(728, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.actualtabResult);
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 120);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(728, 285);
			this.panelTab.TabIndex = 3;
			// 
			// actualtabResult
			// 
			this.actualtabResult.ContainingControl = this;
			this.actualtabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.actualtabResult.Location = new System.Drawing.Point(0, 0);
			this.actualtabResult.Name = "actualtabResult";
			this.actualtabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("actualtabResult.OcxState")));
			this.actualtabResult.Size = new System.Drawing.Size(728, 285);
			this.actualtabResult.TabIndex = 0;
			this.actualtabResult.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.actualtabResult_SendLButtonClick);
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(728, 285);
			this.tabResult.TabIndex = 0;
			// 
			// frmStatistics_Arr_Dep
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(728, 405);
			this.Controls.Add(this.panelTab);
			this.Controls.Add(this.lblResults);
			this.Controls.Add(this.panelTop);
			this.Name = "frmStatistics_Arr_Dep";
			this.Text = "Flights According to Arrival / Departure  (Summary)";
			this.Load += new System.EventHandler(this.frmStatistics_Arr_Dep_Load);
			this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmStatistics_Arr_Dep_HelpRequested);
			this.panelTop.ResumeLayout(false);
			this.pnlDep.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.actualtabResult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmStatistics_Arr_Dep_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			NumberofFlights = new int[4];
			NumberofFlights.Initialize();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtDepFrom.Value = olFrom;
			dtDepTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtDepFrom.CustomFormat  = "dd.MM.yyyy - HH:mm";
			dtDepTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
			tabResult.Visible=false;

			actualtabResult.ResetContent();
			actualtabResult.ShowHorzScroller(true);
			actualtabResult.EnableHeaderSizing(true);
			actualtabResult.SetTabFontBold(true);
			actualtabResult.LifeStyle = true;
			actualtabResult.LineHeight = 16;
			actualtabResult.FontName = "Arial";
			actualtabResult.FontSize = 14;
			actualtabResult.HeaderFontSize = 12;
			actualtabResult.AutoSizeByHeader = true;
			actualtabResult.HeaderString = stractTabHeader;
			actualtabResult.LogicalFieldList = stractLogicalFields;
			actualtabResult.HeaderLengthString = stractTabHeaderLens;

			string[] strLabelHeaderEntry = new string[2];
			if((bmShowStev = Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,false)) == true)
			{
				lblTerminal.Text = strLabelHeaderEntry[0] + ":";
			}
			else
			{
				lblTerminal.Hide();
				txtTerminal.Hide();
			}
			PrepareReportData();				
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			if(myAFT == null) 
				return;
			string strSchedule = "";
			DateTime tmpDate;
			lblProgress.Text = "Preparing Data";
			lblProgress.Refresh();
			progressBar1.Visible = true;
			progressBar1.Value = 0;
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imDepartures =0 ;
			imTotalFlights = 0;
			StringBuilder sb = new StringBuilder(10000);
			
			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
				string strAdid = myAFT[i]["ADID"];
				
				if( i % 100 == 0)
				{
					int percent = Convert.ToInt32((i * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
				}
				if(strAdid == "A")
				{
					imTotalFlights++;
					string strSTOA_showdate = Helper.DateString(myAFT[i]["STOA"],"yyyyMMdd");
					strValues += strSTOA_showdate + ",";
				//	strValues += strAdid + ",";
					strValues += "1" + ",";
					strValues += "0" + "\n";
					sb.Append(strValues);
				}
				if(strAdid == "D")
				{
					imTotalFlights++;
					string strSTOD_showdate = Helper.DateString(myAFT[i]["STOD"],"yyyyMMdd");
					imTotalFlights++;
					strValues += strSTOD_showdate + ",";
				//	strValues += strAdid + ",";
					strValues += "0" + ",";
					strValues += "1" + "\n";
					sb.Append(strValues);
				}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Refresh();
			tabResult.Visible =false;
			tabResult.Sort("0",true,true);
			actualtabResult.Visible =true;
			actualtabResult.Refresh();
			prepareActualReportdata();
		}
		/// <summary>
		/// Counts the Arrival flights
		/// </summary>
		private void prepareActualReportdata()
		{
			string currDateVal="";
			string prevDateVal = "";
			string currArrcellValue="";
			string currDepValue= "";
			
			for(int i = 0 ; i < 4 ; i++)
			{
				NumberofFlights[i] = 0;;
			}
			
			StringBuilder sb1 = new StringBuilder(10000);			
	
			int Linecount = tabResult.GetLineCount();
			for (int k=0; k <Linecount ; k++)
			{
				string strValuesact = "";
				currDateVal = tabResult.GetColumnValue(k,0);
				currArrcellValue =tabResult.GetColumnValue(k,1);
				currDepValue =tabResult.GetColumnValue(k,2);
				if (k==0)
				{
					prevDateVal=currDateVal;					
				}
				if ((prevDateVal==currDateVal))
				{
					if((currArrcellValue =="0") && (currDepValue =="1"))
					{
						NumberofFlights[1]++;
						NumberofFlights[3]++;
						prevDateVal=currDateVal;
					}
					
					if ( (currArrcellValue =="1") && (currDepValue =="0"))
					{
						NumberofFlights[0]++;
						NumberofFlights[2]++;
						prevDateVal=currDateVal;
					}
					if(k==Linecount-1)
					{
						strValuesact ="";
						strValuesact += currDateVal + ",";
						strValuesact += NumberofFlights[0].ToString() + ",";
						strValuesact += NumberofFlights[1].ToString() + "\n";
						sb1.Append(strValuesact);
					}
				}
				if ((prevDateVal!=currDateVal))
				{
					strValuesact += prevDateVal + ",";
					strValuesact += NumberofFlights[0].ToString() + ",";
					strValuesact += NumberofFlights[1].ToString() + "\n";
					sb1.Append(strValuesact);
					NumberofFlights[0]=0;
					NumberofFlights[1]=0;
					
					if((currArrcellValue =="0") && (currDepValue =="1"))
					{
						NumberofFlights[1]++;
						NumberofFlights[3]++;
						prevDateVal=currDateVal;
					}					
					if ( (currArrcellValue =="1") && (currDepValue =="0"))
					{
						NumberofFlights[0]++;
						NumberofFlights[2]++;
						prevDateVal=currDateVal;
					}
					if(k==Linecount-1)
					{
						strValuesact="";
						strValuesact += currDateVal + ",";
						strValuesact += NumberofFlights[0].ToString() + ",";
						strValuesact += NumberofFlights[1].ToString() + "\n";
						sb1.Append(strValuesact);
					}
				}				
			}
			actualtabResult .InsertBuffer(sb1.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			actualtabResult.Refresh();
			tabResult.Visible =false;
			actualtabResult.Visible =true;
			actualtabResult.Sort("0",true,true);
//			actualtabResult.Sort("1",true,true);
			lblResults.Text = "Report Results: (" + actualtabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Counts the Arrival flights
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void panelTop_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			tabResult.ResetContent ();
			actualtabResult.ResetContent ();
			PrepareReportData();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			DateTime datDepFrom;
			DateTime datDepTo;

			string strDateFrom = "";
			string strDateTo = "";
			string strDepDateFrom = "";
			string strDepDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if (cbDepFlights.Checked)
			{
				datDepFrom = dtDepFrom.Value ;
				datDepTo = dtDepTo.Value;
			}
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				datDepFrom = UT.LocalToUtc(dtDepFrom.Value) ;
				datDepTo = UT.LocalToUtc(dtDepTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
				strDepDateFrom = UT.DateTimeToCeda (datDepFrom);
				strDepDateTo = UT.DateTimeToCeda (datDepTo);
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
				strDepDateFrom = UT.DateTimeToCeda (dtDepFrom.Value);
				strDepDateTo = UT.DateTimeToCeda (dtDepTo.Value);
			}
			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,ADID,STOA,STOD,STEV","10,2,14,14,1","URNO,ADID,STOA,STOD,STEV");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				if(strTerminal != "")
				{
					strTmpWhere += strTerminalWhere;
				}
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@ORIG",UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@DEST",UT.Hopo);
				strTmpWhere = strTmpWhere.Replace("@@STEV",strTerminal);
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			tabResult.Visible =false;
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Arr Date From is less than Dep Date To!\n";
				dtFrom.Select ();
				ilErrorCount++;
			}
			if (dtDepFrom.Value  >= dtDepTo .Value )
			{
				strRet += ilErrorCount.ToString () + ". Dep Date From is less than Dep Date To !\n";
				dtDepFrom.Select () ;
				ilErrorCount++;
			}
			return strRet;
		}
		private void cbTimeInLocal_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cbDepFlights.Checked)
			{
				pnlDep.Visible =true;
			}
			if (!cbDepFlights.Checked)
			{
				pnlDep.Visible= false;
			}
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{			
			RunReport();		
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			imTotalFlights = NumberofFlights[2] + NumberofFlights[3];
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " +" " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString() +" ";
			strSubHeader += "  "+" ARR: " + NumberofFlights[2].ToString();
			strSubHeader += "  "+" DEP: " + NumberofFlights[3].ToString() + ")";
			
			strReportHeader = "Flights According to Arrival / Departure  (Summary): ";
			strReportSubHeader = strSubHeader;
			
			rptFIPS rpt = new rptFIPS(actualtabResult, strReportHeader, strReportSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmStatistics_Arr_Dep_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}

		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			tabResult.ResetContent();
			actualtabResult.ResetContent ();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("AFT");
			this.Close ();
		}

		private void dtDepFrom_ValueChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dtArrTo_ValueChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dtFrom_ValueChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dtTo_ValueChanged(object sender, System.EventArgs e)
		{
		
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void actualtabResult_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == actualtabResult.CurrentSortColumn)
				{
					if( actualtabResult.SortOrderASC == true)
					{
						actualtabResult.Sort(e.colNo.ToString(),false, true);  // made from false to true
					}
					else
					{
						actualtabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					 actualtabResult.Sort( e.colNo.ToString(), true, true);
				}
				actualtabResult.Refresh();
			}
		}

		private void InitializeMouseEvents()
		{			
			((System.Windows.Forms.Control)actualtabResult).MouseWheel += new MouseEventHandler(frmStatistics_Arr_Dep_MouseWheel);
			((System.Windows.Forms.Control)actualtabResult).MouseEnter += new EventHandler(frmStatistics_Arr_Dep_MouseEnter);
			((System.Windows.Forms.Control)actualtabResult).MouseLeave += new EventHandler(frmStatistics_Arr_Dep_MouseLeave);
		}

		private void frmStatistics_Arr_Dep_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(actualtabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && actualtabResult.GetVScrollPos() > 0)
			{				
				actualtabResult.OnVScrollTo(actualtabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (actualtabResult.GetVScrollPos() + 1) < actualtabResult.GetLineCount())
			{
				actualtabResult.OnVScrollTo(actualtabResult.GetVScrollPos() + 1);
			}
		}

		private void frmStatistics_Arr_Dep_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmStatistics_Arr_Dep_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmStatistics_Arr_Dep_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(actualtabResult,strReportHeader,strReportSubHeader,stractTabHeaderLens,10,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmStatistics_Arr_Dep_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}

