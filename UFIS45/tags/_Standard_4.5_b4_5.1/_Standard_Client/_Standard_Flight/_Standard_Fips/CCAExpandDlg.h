#if !defined(AFX_CCAEXPANDDLG_H__75021EC1_3BE9_11D2_859B_0000C04D916B__INCLUDED_)
#define AFX_CCAEXPANDDLG_H__75021EC1_3BE9_11D2_859B_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CCAExpandDlg.h : header file
//

#include <CCSEdit.h>
#include <CCSGlobl.h>
/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg dialog

class CCAExpandDlg : public CDialog
{
// Construction
public:
	CCAExpandDlg(CWnd* pParent = NULL, int ipSubOrig = SUB_MOD_ALL, CString opStev = " ", CTime opRefDate = TIMENULL, CTime opViewFrom = TIMENULL, CTime opViewTo = TIMENULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCAExpandDlg)
	enum { IDD = IDD_CCAEXPAND };
	CButton	m_CB_AllVT;
	CProgressCtrl	m_CP_Progess;
	CCSEdit	m_From;
	CCSEdit	m_To;
	CCSEdit	m_Ref_Date;
	CButton	m_CB_check1;
	CButton	m_CB_check2;
	CButton	m_CB_check3;
	CButton	m_CB_check4;
	CButton	m_CB_check5;
	CButton	m_CB_check6;
	CButton	m_CB_check7;
	CButton	m_CB_common;
	CButton	m_CB_flight;
	CButton	m_CB_Precheck;
	CButton	m_CB_AC;
	CButton	m_CB_STD;
	CButton	m_CB_DEST;
	CButton	m_CB_POS;
	CButton	m_CB_GAT;
	CButton	m_CB_BLT;
	CButton	m_CB_WRO;
	CButton	m_CB_EXIT;
	CButton	m_CB_Overwrite;
	BOOL	m_check1;
	BOOL	m_check2;
	BOOL	m_check3;
	BOOL	m_check4;
	BOOL	m_check5;
	BOOL	m_check6;
	BOOL	m_check7;
	BOOL	m_common;
	BOOL	m_flight;
	BOOL	m_PreCheck;
	BOOL	m_AC;
	BOOL	m_STD;
	BOOL	m_DEST;
	BOOL	m_POS;
	BOOL	m_GAT;
	BOOL	m_BLT;
	BOOL	m_WRO;
	BOOL	m_EXIT;
	BOOL	m_OverWrite;
	//}}AFX_DATA

	bool m_only;
	int imSubMod;
	CString omStev ;
	CTime omRefDate;

	CTime omViewFrom;
	CTime omViewTo;

 
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCAExpandDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCAExpandDlg)
	afx_msg void OnOnly();
	afx_msg void OnAll();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	bool CreateExcelFiles(ofstream& ofResult, ofstream& ofFailed, CString& olFileResult, CString& olFileFailed, char* opTrenner);
	void CreateCICProtokoll(CStringArray& olCopied, CStringArray& olNotCopied);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCAEXPANDDLG_H__75021EC1_3BE9_11D2_859B_0000C04D916B__INCLUDED_)
