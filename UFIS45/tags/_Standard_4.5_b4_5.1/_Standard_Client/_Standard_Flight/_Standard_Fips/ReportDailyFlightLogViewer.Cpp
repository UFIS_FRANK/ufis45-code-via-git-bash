// ReportDailyFlightLogViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <ReportDailyFlightLogViewer.h>
#include <CcsGlobl.h>
#include <resrc1.h>

#include <math.h>
 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif 


int ReportDailyFlightLogViewer::imTableColCharWidths[REPORTDAILYFLIGHTLOG_COLCOUNT]={9, 2, 3, 3, 3, 5, 5, 5, 5, 5, 3, 5, 5, 5, 4, 3, 6, 9, 2, 3, 3, 3, 5, 5, 5, 5, 5, 2, 4, 2, 4, 3, 5, 5, 5, 20};


/////////////////////////////////////////////////////////////////////////////
// ReportDailyFlightLogViewer
//

ReportDailyFlightLogViewer::ReportDailyFlightLogViewer() :
	imOrientation(PRINT_LANDSCAPE),
	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_9),
	fmTableHeaderFontWidth(10), fmTableLinesFontWidth(7.5)
	
{

	imFlightNum = 0;
	imArr = 0;
	imDep = 0;

	dgCCSPrintFactor = 2.7;

	int i=0;
	// Table header strings
	omTableHeadlines[i++]=GetString(IDS_STRING1902);	// FLNR 
 	omTableHeadlines[i++]=GetString(IDS_STRING1916);    // DT 
 	omTableHeadlines[i++]=GetString(IDS_STRING298);		// ORG
	omTableHeadlines[i++]=GetString(IDS_STRING299);		// VIA
	omTableHeadlines[i++]=GetString(IDS_STRING1903);	// NA
 	omTableHeadlines[i++]=GetString(IDS_STRING323);		// STA
	omTableHeadlines[i++]=GetString(IDS_STRING302);		// ETA
	omTableHeadlines[i++]=GetString(IDS_STRING303);		// TMO
	omTableHeadlines[i++]=GetString(IDS_STRING1904);	// LAND
	omTableHeadlines[i++]=GetString(IDS_STRING1905);	// ONBL 
	omTableHeadlines[i++]=GetString(IDS_PAXTTL);		// P/TTL 
	omTableHeadlines[i++]=GetString(IDS_STRING308);		// POS 
	omTableHeadlines[i++]=GetString(IDS_STRING309);		// GAT
	omTableHeadlines[i++]=GetString(IDS_STRING1555);	// BLT 
	omTableHeadlines[i++]=GetString(IDS_STRING1906);	// C/N 
	omTableHeadlines[i++]=GetString(IDS_STRING311);		// A/C
	omTableHeadlines[i++]=GetString(IDS_STRING310);		// REG 
	omTableHeadlines[i++]=GetString(IDS_STRING1902);	// FLNR 
	omTableHeadlines[i++]=GetString(IDS_STRING1916);    // DT		 
	omTableHeadlines[i++]=GetString(IDS_STRING315);		// DES
	omTableHeadlines[i++]=GetString(IDS_STRING299);		// VIA 
	omTableHeadlines[i++]=GetString(IDS_STRING1903);	// NA
	omTableHeadlines[i++]=GetString(IDS_STRING316);		// STD
	omTableHeadlines[i++]=GetString(IDS_STRING317);		// ETD
	omTableHeadlines[i++]=GetString(IDS_STRING1907);	// SLOT 
	omTableHeadlines[i++]=GetString(IDS_STRING1908);	// OFBL
	omTableHeadlines[i++]=GetString(IDS_STRING1909);	// AIRB 
	omTableHeadlines[i++]=GetString(IDS_STRING1910);	// DEL1 
	omTableHeadlines[i++]=GetString(IDS_STRING1911);	// MIN1
	omTableHeadlines[i++]=GetString(IDS_STRING1912);	// DEL2
	omTableHeadlines[i++]=GetString(IDS_STRING1913);	// MIN2
	omTableHeadlines[i++]=GetString(IDS_PAXTTL);		// P/TTL
	omTableHeadlines[i++]=GetString(IDS_STRING308);		// POS 
	omTableHeadlines[i++]=GetString(IDS_STRING309);		// GAT
	omTableHeadlines[i++]=GetString(IDS_STRING1917);	// LGE
	omTableHeadlines[i++]=GetString(IDS_STRING1915);	// REMARKS
	
	// calculate table column widths for displaying
	for (i=0; i < REPORTDAILYFLIGHTLOG_COLCOUNT; i++)
	{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);
	}

	pomTable = NULL;
}



ReportDailyFlightLogViewer::~ReportDailyFlightLogViewer()
{
    DeleteAll();
	omLoaData.Clear(); 
}


// Connects the viewer with a table
void ReportDailyFlightLogViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

// delete intern table lines
void ReportDailyFlightLogViewer::DeleteAll(void)
{
    omLines.DeleteAll();
}




// Load the intern line data from the given data and displays the table
void ReportDailyFlightLogViewer::ChangeViewTo(CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData, char *popDateStr, int ipIATA_ICAO)
{
	imIATA_ICAO = ipIATA_ICAO;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// load pax data
	CString olFlightUrnos; 
	char buffer[65];

	for (int i=0; i < ropData.GetSize(); i++)
	{
		// collect flight-urnos
		ltoa (ropData[i].Urno, buffer, 10);
		olFlightUrnos += CString(buffer) + CString(",");
	}

	if(!olFlightUrnos.IsEmpty())
	{
		olFlightUrnos = olFlightUrnos.Left(olFlightUrnos.GetLength() - 1);
	}
	omLoaData.ReadFlnus(olFlightUrnos); 


	pomDateStr = popDateStr;
	// Rebuild intern data    
    MakeLines(ropData);
	// Rebuild and display table
	UpdateDisplay();
  
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}


// Transfer the data of the database to the intern data structures
void ReportDailyFlightLogViewer::MakeLines(const CCSPtrArray<ROTATIONDLGFLIGHTDATA> &ropData)
{ 
	// Delete the intern lines
	DeleteAll();

	REPORTDAILYFLIGHTLOG_LINEDATA rlLineData;

	const ROTATIONDLGFLIGHTDATA *prlAFlight;
	const ROTATIONDLGFLIGHTDATA *prlDFlight;
	const ROTATIONDLGFLIGHTDATA *prlNextFlight = NULL;
	const ROTATIONDLGFLIGHTDATA *prlFlight;

	bool blRDeparture = false;

 	imFlightNum=ropData.GetSize();

	// Create the intern lines
	for (int ilLc = 0; ilLc < imFlightNum; ilLc++)
	{
		prlFlight = &(ropData[ilLc]);

		if(ilLc + 1 < imFlightNum)
			prlNextFlight = &(ropData[ilLc + 1]);	
		else
			prlNextFlight = NULL;	

		if(!((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) != 0)))
		{

			//Arrival
			if((strcmp(prlFlight->Org3, pcgHome) != 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
			{
				prlAFlight = prlFlight;
				prlDFlight = NULL;
				if(prlNextFlight != NULL)
				{
					if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
					{
						prlDFlight = prlNextFlight;

						if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
						{
							// Rotation
							ilLc++;
						}
						else
							blRDeparture = true;
					}
				}
			}
			else
			{
				// Departure
				if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) != 0))
				{
					prlAFlight = NULL;
					prlDFlight = prlFlight;
					blRDeparture = false;
				}
				else
				{
					//Turnaround
					if((strcmp(prlFlight->Org3, pcgHome) == 0) && (strcmp(prlFlight->Des3, pcgHome) == 0))
					{
						if(blRDeparture)
						{
							blRDeparture = false;
							prlAFlight = prlFlight;
							prlDFlight = NULL;
							if(prlNextFlight != NULL)
							{
								if((prlNextFlight->Rkey == prlFlight->Rkey) && (prlFlight->Rkey != NULL))
								{
									prlDFlight = prlNextFlight;
									if((strcmp(prlNextFlight->Org3, pcgHome) == 0) && (strcmp(prlNextFlight->Des3, pcgHome) != 0))
									{
										// Rotation
										ilLc++;
									}
									else
										blRDeparture = true;
								}
							}
						}
						else
						{
 							prlAFlight		= NULL;
							prlDFlight		= prlFlight;
							blRDeparture = true;
							ilLc--;


						}
					}
				}
			}
			// create one intern line
	  		MakeLine(prlAFlight, prlDFlight);
 
		} // if(!((strcmp(prlFlight->Org3, pcg ...
	}

//generate the headerinformation
	CString olTimeSet = GetString(IDS_STRING1920); //UTC
	if(bgReportLocal)
		olTimeSet = GetString(IDS_STRING1921); //LOCAL

	char pclHeader[256];
	sprintf(pclHeader, GetString(IDS_R_HEADER1), GetString(REPORTS_RB_DailyFlightLog), "***", pomDateStr, imArr+imDep, imArr, imDep, olTimeSet);
	if(bgReports)
	 sprintf(pclHeader, "%s%s with %s from %s (Flights: %d / ARR: %d / DEP: %d) - %s",ogPrefixReports, GetString(REPORTS_RB_DailyFlightLog), "***", pomDateStr, imArr+imDep, imArr, imDep, olTimeSet);
	omTableName = pclHeader;
}


// Create one intern line
bool ReportDailyFlightLogViewer::MakeLine(const ROTATIONDLGFLIGHTDATA *prpAFlight, const ROTATIONDLGFLIGHTDATA *prpDFlight)
{
	REPORTDAILYFLIGHTLOG_LINEDATA rlLineData;

	if (MakeLineData(rlLineData, prpAFlight, prpDFlight))
	{
		CreateLine(rlLineData);

//		int l = sizeof (*prpAFlight);
//		l = sizeof (rlLineData);
		return true;
	}
	return false;

}

 
// Copy the data from the db-record to the table-record
bool ReportDailyFlightLogViewer::MakeLineData(REPORTDAILYFLIGHTLOG_LINEDATA &rrpLineData, const ROTATIONDLGFLIGHTDATA *prpAFlight, const ROTATIONDLGFLIGHTDATA *prpDFlight)
{
	if (!prpAFlight && !prpDFlight) return false;

	const ROTATIONDLGFLIGHTDATA *prlFlight=NULL;

	if (imIATA_ICAO == 1)
		int z = 1;

	if (prpAFlight)
	{
		rrpLineData.AUrno = prpAFlight->Urno;

		rrpLineData.AFlno = prpAFlight->Flno;
		if(CString(prpAFlight->Alc2).IsEmpty() || imIATA_ICAO == 1)
			rrpLineData.AFlno = CString(prpAFlight->Alc3) + CString(prpAFlight->Flno).Mid(3,CString(prpAFlight->Flno).GetLength()-1);

		rrpLineData.ADateDay = prpAFlight->Stoa.Format("%d");

		rrpLineData.AOrg3 = prpAFlight->Org3;
		if(rrpLineData.AOrg3.IsEmpty() || imIATA_ICAO == 1)
			rrpLineData.AOrg3 = prpAFlight->Org4;

		rrpLineData.AVia3 = prpAFlight->Via3;
		if(rrpLineData.AVia3.IsEmpty() || imIATA_ICAO == 1)
			rrpLineData.AVia3 = prpAFlight->Via4;

		rrpLineData.ATTyp = prpAFlight->Ttyp;
		rrpLineData.AStoa = prpAFlight->Stoa;
		rrpLineData.AEtai = prpAFlight->Etai;
		rrpLineData.ATmoa = prpAFlight->Tmoa;
		rrpLineData.ALand = prpAFlight->Land;
		rrpLineData.AOnbl = prpAFlight->Onbl;

		if (strcmp(pcgHome, "DXB") == 0)
			rrpLineData.APaxt = prpAFlight->Paxt;
		else
		{
			if (strcmp(pcgHome, "WAW") == 0)
				rrpLineData.APaxt = CString(omLoaData.GetLoaWAW(prpAFlight->Urno, CString("PXT"),CString("LDM,USR,MVT,KRI"),CString("WAW"))->Valu);
			else
				rrpLineData.APaxt = CString((omLoaData.GetLoa(prpAFlight->Urno, CString("PXT"))->Valu));
		}

		rrpLineData.APsta = prpAFlight->Psta;
		rrpLineData.AGta1 = prpAFlight->Gta1;
		rrpLineData.ABlt1 = prpAFlight->Blt1;

		prlFlight = prpAFlight;

		imArr++;
	}
	if (prpDFlight)
	{
		rrpLineData.DUrno = prpDFlight->Urno;

		rrpLineData.DFlno = prpDFlight->Flno;
		if(CString(prpDFlight->Alc2).IsEmpty() || imIATA_ICAO == 1)
			rrpLineData.DFlno = CString(prpDFlight->Alc3) + CString(prpDFlight->Flno).Mid(3,CString(prpDFlight->Flno).GetLength()-1);

		rrpLineData.DDateDay = prpDFlight->Stod.Format("%d");

		rrpLineData.DDes3 = prpDFlight->Des3;
		if(rrpLineData.DDes3.IsEmpty() || imIATA_ICAO == 1)
			rrpLineData.DDes3 = prpDFlight->Des4;

		rrpLineData.DVia3 = prpDFlight->Via3;
		if(rrpLineData.DVia3.IsEmpty() || imIATA_ICAO == 1)
			rrpLineData.DVia3 = prpDFlight->Via4;

		rrpLineData.DTTyp = prpDFlight->Ttyp;
		rrpLineData.DStod = prpDFlight->Stod;
		rrpLineData.DEtdi = prpDFlight->Etdi;
		rrpLineData.DSlot = prpDFlight->Slot;
		rrpLineData.DOfbl = prpDFlight->Ofbl;
		rrpLineData.DAirb = prpDFlight->Airb;
		rrpLineData.DDcd1 = prpDFlight->Dcd1;
		rrpLineData.DDtd1 = prpDFlight->Dtd1;
		rrpLineData.DDcd2 = prpDFlight->Dcd2;
		rrpLineData.DDtd2 = prpDFlight->Dtd2;

		if (strcmp(pcgHome, "DXB") == 0)
			rrpLineData.DPaxt = prpDFlight->Paxt;
		else
		{
			if (strcmp(pcgHome, "WAW") == 0)
				rrpLineData.DPaxt = CString(omLoaData.GetLoa(prpDFlight->Urno, CString("PXT"))->Valu);
			else
				rrpLineData.DPaxt = CString((omLoaData.GetLoa(prpDFlight->Urno, CString("PXT"))->Valu));
		}

		rrpLineData.DPstd = prpDFlight->Pstd;
		rrpLineData.DGtd1 = prpDFlight->Gtd1;
		rrpLineData.DWro1 = prpDFlight->Wro1;

		prlFlight = prpDFlight;

		imDep++;
	}


	if (prlFlight)
	{
		// Common data
		switch (prlFlight->Ftyp[0])
		{
		case 'X': 
			rrpLineData.Status = "Cxx";
			break;
		case 'N': 
			rrpLineData.Status = "NoOp";
			break;
		case 'S': 
			rrpLineData.Status = "Plan";
			break;
		case 'D': 
			rrpLineData.Status = "Div";
			break;
		case 'R': 
			rrpLineData.Status = "Rer";
			break;
		case 'Z': 
			rrpLineData.Status = "RetF";
			break;
		case 'B': 
			rrpLineData.Status = "RetT";
			break;
		default:
			rrpLineData.Status = "";
			break;
		}
		rrpLineData.Act3 = prlFlight->Act3;
		if(rrpLineData.Act3.IsEmpty() || imIATA_ICAO == 1)
			rrpLineData.Act3 = prlFlight->Act5;

		rrpLineData.Regn = prlFlight->Regn;
//		rrpLineData.Rem1 = prlFlight->Rem1;

		//aufbereiten von remark (steuerzeichen verursachen einen zeilenumbruch)
		char olRem1[258];
		CString olSrc = prlFlight->Rem1;
		CleanSteurzeichen (olRem1, olSrc);
		rrpLineData.Rem1 = CString(olRem1);
	}

	
	// Local times choosen?
	if(bgReportLocal)
	{
		UtcToLocal(rrpLineData);
	}


	return true;		
}


bool ReportDailyFlightLogViewer::UtcToLocal(REPORTDAILYFLIGHTLOG_LINEDATA &rrpLineData) const
{
	ogBasicData.UtcToLocal(rrpLineData.AStoa);
	rrpLineData.ADateDay = rrpLineData.AStoa.Format("%d");
	ogBasicData.UtcToLocal(rrpLineData.AEtai);
	ogBasicData.UtcToLocal(rrpLineData.ATmoa);
	ogBasicData.UtcToLocal(rrpLineData.ALand);
	ogBasicData.UtcToLocal(rrpLineData.AOnbl);

	ogBasicData.UtcToLocal(rrpLineData.DStod);
	rrpLineData.DDateDay = rrpLineData.DStod.Format("%d");
	ogBasicData.UtcToLocal(rrpLineData.DEtdi);
	ogBasicData.UtcToLocal(rrpLineData.DSlot);
	ogBasicData.UtcToLocal(rrpLineData.DOfbl);
	ogBasicData.UtcToLocal(rrpLineData.DAirb);

	return true;
}



// Create a intern table data record
int ReportDailyFlightLogViewer::CreateLine(REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine)
{
    for (int ilLineno = omLines.GetSize(); ilLineno > 0; ilLineno--)
	{
		if (CompareLines(rrpLine, omLines[ilLineno-1]) >= 0)
		{
            break;  // should be inserted after Lines[ilLineno-1]
		}
	}

	// Insert new line
    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}


// Rebuild the table from the intern data
void ReportDailyFlightLogViewer::UpdateDisplay()
{
	// Clear the table
	pomTable->ResetContent();
	
	DrawTableHeader();
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(omLines[ilLc], olColList);
		// add table line
		pomTable->AddTextLine(olColList, (void*)(&omLines[ilLc]));
			
	}

    pomTable->DisplayTable();
}

 


void ReportDailyFlightLogViewer::DrawTableHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &romTableHeaderFont;

	// for all rows
	for (int i=0; i < REPORTDAILYFLIGHTLOG_COLCOUNT; i++)
	{
		rlHeader.Alignment = COLALIGN_CENTER;
		rlHeader.Length = imTableColWidths[i]; 
		rlHeader.Text = omTableHeadlines[i]; 
		omHeaderDataArray.New(rlHeader);
	}

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}




// Fills one row of the table
bool ReportDailyFlightLogViewer::MakeColList(const REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const
{
	TABLE_COLUMN rlColumnData;

	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &romTableLinesFont;
	
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AFlno;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.ADateDay;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AOrg3;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AVia3;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.ATTyp;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AStoa.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AEtai.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);
 
	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.ATmoa.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.ALand.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AOnbl.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.APaxt;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.APsta;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.AGta1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.ABlt1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Status;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Act3;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Regn;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DFlno;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DDateDay;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DDes3;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DVia3;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DTTyp;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DStod.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DEtdi.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DSlot.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DOfbl.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DAirb.Format("%H:%M");
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DDcd1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DDtd1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DDcd2;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DDtd2;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DPaxt;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DPstd;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DGtd1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.DWro1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	rlColumnData.Alignment = COLALIGN_LEFT;
	rlColumnData.Text = rrpLine.Rem1;
	ropColList.NewAt(ropColList.GetSize(), rlColumnData);

	return true;
}


// Compares two lines of the table
int ReportDailyFlightLogViewer::CompareLines(const REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine1, const REPORTDAILYFLIGHTLOG_LINEDATA &rrpLine2) const 
{
	// Sorted by Stoa/Stod, Flno

	const CTime *polSto1 = NULL;
	const CTime *polSto2 = NULL;
	const CString *polFlno1 = NULL;
	const CString *polFlno2 = NULL;

	// Get the data
	if (rrpLine1.AUrno > 0)
	{
		// First line is a arrival flight
		polSto1 = &rrpLine1.AStoa;
		polFlno1 = &rrpLine1.AFlno;
	}
	else
	{
		// First line is a departure flight
		polSto1 = &rrpLine1.DStod;
		polFlno1 = &rrpLine1.DFlno;
	}
	if (rrpLine2.AUrno > 0)
	{
		// Second line is a arrival flight
		polSto2 = &rrpLine2.AStoa;
		polFlno2 = &rrpLine2.AFlno;
	}
	else
	{
		// Second line is a departure flight
		polSto2 = &rrpLine2.DStod;
		polFlno2 = &rrpLine2.DFlno;
	}

	// Compare first the times
	if (*polSto1 < *polSto2) return -1;
	if (*polSto1 > *polSto2) return 1;

	// Compare second the flight numbers
	if (*polFlno1 < *polFlno2) return -1;
	if (*polFlno1 > *polFlno2) return 1;

	return 0;	
}


int ReportDailyFlightLogViewer::GetFlightCount(void) const
{
	return imFlightNum;
}



//////////////////////////////////////////////////////////////////
//// Printing routines
 
// Print table to paper
void ReportDailyFlightLogViewer::PrintTableView(void) {


	CCSPrint olPrint(NULL, imOrientation);

	CString olFooterLeft,olFooterCenter,olFooterRight;
	// Set left footer to: "Flights: <xxx>"
//	olFooterLeft.Format("%s %d", GetString(IDS_STRING1256), imFlightNum);
	olFooterLeft.Format("%s (Flights: %d / ARR: %d / DEP: %d) - %s", GetString(REPORTS_RB_DailyFlightLog), imArr+imDep, imArr, imDep, "");
	// Set right footer to: "printed at: <date>"
 	olFooterRight.Format("%s %s", GetString(IDS_STRING1481),
		(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
	if(bgReports)
	{
	olFooterLeft.Format("%s%s (Flights: %d / ARR: %d / DEP: %d) - %s",ogPrefixReports, GetString(REPORTS_RB_DailyFlightLog), imArr+imDep, imArr, imDep,pcgUser);
	}
	int ilPrnLeftOffset;
	int ilPrnAddLeftOffset;
  	if (olPrint.InitializePrinter(imOrientation) == TRUE)
	{ 
		if (olPrint.smPaperSize == DMPAPER_A3)
		{
			// select fonts
			pomPrintHeaderFont = &olPrint.ogCourierNew_Bold_7;
			pomPrintLinesFont = &olPrint.ogCourierNew_Bold_7;
			fmPrintHeaderFontWidth = 7;
			fmPrintLinesFontWidth = 7;

			olPrint.imLineHeight = 37;
			olPrint.imMaxLines = 68;	// Def. imMaxLines: 57 Portrait / 38 Landscape
			// select left offset
			ilPrnLeftOffset = 50;
			ilPrnAddLeftOffset = 60;  // for centering the lines
		}
		else
		{
			// select fonts
			pomPrintHeaderFont = &olPrint.ogCourierNew_Bold_4;
			pomPrintLinesFont = &olPrint.ogCourierNew_Bold_4;
			fmPrintHeaderFontWidth = 4;
			fmPrintLinesFontWidth = 4;

			olPrint.imLineHeight = 25;
			olPrint.imMaxLines = 68;	// Def. imMaxLines: 57 Portrait / 38 Landscape
			// select left offset
			ilPrnLeftOffset = 50;
			ilPrnAddLeftOffset = 140;   // for centering the lines
		}
		olPrint.imLeftOffset = ilPrnLeftOffset;
		// calculate table column widths for printing
		for (int i=0; i < REPORTDAILYFLIGHTLOG_COLCOUNT; i++)
		{
  			imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
		}

		// Calculate number of pages
		const double dlPages = ceil((double)pomTable->GetLinesCount() / (double)(olPrint.imMaxLines) * 2);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		// At first a pagebreak
		olPrint.imLineNo = olPrint.imMaxLines + 1;	
		DOCINFO	rlDocInfo;
		memset(&rlDocInfo, 0, sizeof(DOCINFO));
		rlDocInfo.cbSize = sizeof( DOCINFO );
		rlDocInfo.lpszDocName = "DailyFlightLog";//GetString(IDS_STRING1901);	
		olPrint.omCdc.StartDoc( &rlDocInfo );
		olPrint.imPageNo = 0;
		// Print all table lines
		bool blEmptyLine = false;
		int ilLc = 0;
		while (ilLc < pomTable->GetLinesCount())
		{
			// Page break
 			if(olPrint.imLineNo > olPrint.imMaxLines)
			{
				if(olPrint.imPageNo > 0)
				{
					//// print footer
					// Set center footer to: "Page: %d/%d"
					olFooterCenter.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
					// print footer
					olPrint.PrintUIFFooter(olFooterLeft, olFooterCenter, olFooterRight);
					olPrint.omCdc.EndPage();
				}
				// print header
				PrintTableHeader(olPrint, ilPrnLeftOffset, ilPrnAddLeftOffset);
			}				
			olPrint.imLeftOffset += ilPrnAddLeftOffset;
			if (!blEmptyLine)
			{
				// print line			
				PrintTableLine(olPrint, ilLc);
				ilLc++;
				blEmptyLine = true;
			}
			else
			{
				// print empty line
				PrintTableLine(olPrint, -1);
				blEmptyLine = false;
			}
			olPrint.imLeftOffset = ilPrnLeftOffset;
		}
		olPrint.imLeftOffset += ilPrnAddLeftOffset;
		if (blEmptyLine)
		{
			// print empty line
			PrintTableLine(olPrint, -1);
			blEmptyLine = false;
		}
		olPrint.imLeftOffset = ilPrnLeftOffset;
		//// print footer
		// Set center footer to: "Page: %d/%d"
		olFooterCenter.Format(GetString(IDS_STRING1199),olPrint.imPageNo, dlPages);
		// print footer
		olPrint.PrintUIFFooter(olFooterLeft, olFooterCenter, olFooterRight);
		olPrint.omCdc.EndPage();

 		olPrint.omCdc.EndDoc();
	}  // if (olPrint.InitializePrin...
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




bool ReportDailyFlightLogViewer::PrintTableHeader(CCSPrint &ropPrint, int ipLeftOffset, int ipAddLeftOffset)
{
	ropPrint.omCdc.StartPage();
	ropPrint.imPageNo++;
	ropPrint.imLineNo = 0;
	//double dgCCSPrintFactor = 2.7 ;
	int ilTimesStringId;
 
	if(bgReportLocal)
	{
		ilTimesStringId = IDS_STRING1921;
	}
	else
	{
		ilTimesStringId = IDS_STRING1920;
	}

  
	// print page headline
	ropPrint.imLeftOffset = ipLeftOffset;
//	ropPrint.PrintUIFHeader3(GetString(ilTimesStringId), GetString(IDS_STRING1919), CString(pomDateStr), ropPrint.imFirstLine-10);
	ropPrint.PrintUIFHeader("", omTableName, ropPrint.imFirstLine-10);

	// for centering the lines
	ropPrint.imLeftOffset += ipAddLeftOffset;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_NOFRAME;
	rlElement.pFont       = pomPrintHeaderFont;

	// Create table headline
	for (int ilCc = 0; ilCc < REPORTDAILYFLIGHTLOG_COLCOUNT; ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 

		rlElement.Text = omTableHeadlines[ilCc]; 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(), rlElement);
 	}
	// Print table headline
 	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	// reset left offset
	ropPrint.imLeftOffset = ipLeftOffset;

	return true;
}


 

bool ReportDailyFlightLogViewer::PrintTableLine(CCSPrint &ropPrint, int ipLineNo) {
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	
	//double dgCCSPrintFactor = 2.7 ;

	PRINTELEDATA rlElement;
	rlElement.pFont = pomPrintLinesFont;
	rlElement.FrameTop = PRINT_FRAMETHIN;
 	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;

	CString olCellValue;
	// create table line
	for (int ilCc = 0; ilCc < REPORTDAILYFLIGHTLOG_COLCOUNT; ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
		// thik borderline after line 14, 17 and 35
		if (ilCc == 14 || ilCc == 17 || ilCc == 35)
		{
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
		}
		else
		{
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
		}
 		rlElement.Length = (int)(imPrintColWidths[ilCc]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		if (ipLineNo >= 0)
		{
			// Get printing text from table	
			pomTable->GetTextFieldValue(ipLineNo, ilCc, olCellValue);

			//aufbereiten (steuerzeichen verursachen einen zeilenumbruch)
			char olRem1[258];
			//CString olSrc = prlFlight->Rem1;
			CleanSteurzeichen (olRem1, olCellValue);
			rlElement.Text = CString(olRem1);

//			rlElement.Text = olCellValue; 
		}
		else
		{
			// Empty line
			rlElement.Text.Empty(); 
		}
		
 		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	}
	// print table line
	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	
	return true;
}


 
// Print table to file
bool ReportDailyFlightLogViewer::PrintPlanToFile(char *pclTrenner)
{
	ofstream of;
	CString olFileName = omTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	olFileName.Remove(',');
	
	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;
	of  << setw(ilwidth) << olFileName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	// Header
	for (int ilCc = 0; ilCc < REPORTDAILYFLIGHTLOG_COLCOUNT; ilCc++)
	{
		of  << omTableHeadlines[ilCc];
		if (ilCc < REPORTDAILYFLIGHTLOG_COLCOUNT-1)
		{
			of << pclTrenner;
		}
	}
	of << endl;

	CString olCellValue;
	 // Lines
	for (int ilLc = 0; ilLc < pomTable->GetLinesCount(); ilLc++)
	{
		for (int ilCc = 0; ilCc < REPORTDAILYFLIGHTLOG_COLCOUNT; ilCc++)
		{
			// get text from table
			pomTable->GetTextFieldValue(ilLc, ilCc, olCellValue);
			of  << olCellValue;
			if (ilCc < REPORTDAILYFLIGHTLOG_COLCOUNT-1)
			{
				of << pclTrenner;
			}
		}
		of << endl;
	}

	of.close();

	return true;
}

// im fall von return flight werden destination und sta im remark zwischengespeichert,
// um bei ruecknahme die urspruenglichen werte wieder zu setzen. die daten beinhalten 
// steuerzeichen (/0) welche im view zu einem unerwuenschten zeilenumbruch fuehren.
bool ReportDailyFlightLogViewer::CleanSteurzeichen (char *opChar, CString &opString)
{
	strcpy (opChar,  opString);

	if (strlen(opChar) > 0)
	{
		for (int z1 = 0; opChar[z1] != '\0'; ++z1)
		{
			if (opChar[z1] >= 0 && opChar[z1] <= 31)
				opChar[z1] = ' ';
		}
	}
	return true;
}





 