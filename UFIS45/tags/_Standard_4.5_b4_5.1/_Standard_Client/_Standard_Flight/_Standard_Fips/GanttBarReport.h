#ifndef GANTT_BAR_REPORT
#define GANTT_BAR_REPORT

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// GanttBarReport.h : header file
//
#include <GanttChartReport.h>
#include <CColor.h>


/////////////////////////////////////////////////////////////////////////////
// GanttBarReport window


class GanttBarReport
{
	public:
		GanttBarReport(GanttChartReport* popGanttChartReport, CColor Color, 
			           unsigned int ipData, CString opText = "");
		virtual ~GanttBarReport();

		void Draw(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, CString opBarText = "");
		bool Print(CDC* ppDCD, int ipSpace, int ipBarNo, int ipHeight, double dpFactorY, 
			       CString opBarText = "");

	protected:

	private:
		COLORREF omColor ;
		GanttChartReport* pomGanttChartReport;
		int imData;
		CString imXText;

};


#endif 
