#if !defined(AFX_ROTATIONTABLEREPORTSELECT_H__51C21181_506B_11D6_8042_0001022205EE__INCLUDED_)
#define AFX_ROTATIONTABLEREPORTSELECT_H__51C21181_506B_11D6_8042_0001022205EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationTableReportSelect.h : header file
//
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// RotationTableReportSelect dialog

class RotationTableReportSelect : public CDialog
{
// Construction
public:
	RotationTableReportSelect(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(RotationTableReportSelect)
	enum { IDD = IDD_ROTTABLESEL };
	BOOL	m_check1;
	BOOL	m_check2;
	BOOL	m_check3;
	BOOL	m_check4;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationTableReportSelect)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationTableReportSelect)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONTABLEREPORTSELECT_H__51C21181_506B_11D6_8042_0001022205EE__INCLUDED_)
