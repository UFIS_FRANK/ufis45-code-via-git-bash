#if !defined(AFX_SpecialConflictDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_)
#define AFX_SpecialConflictDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// SpecialConflictDlg.h : header file
//


#include <CCSTable.h>
#include <Konflikte.h>
#include <resrc1.h>
#include <KonflikteDlg.h>




 
/////////////////////////////////////////////////////////////////////////////
// SpecialConflictDlg dialog

class SpecialConflictDlg : public CDialog
{
// Construction
public:
	SpecialConflictDlg(CWnd* pParent = NULL);   // standard constructor

	~SpecialConflictDlg();

// Dialog Data
	//{{AFX_DATA(SpecialConflictDlg)
	enum { IDD = IDD_SPECIAL_CONFLICT };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SpecialConflictDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

public:

	void ShowDialog(); 
	void Activate();

	void AttachButton( CCSButtonCtrl *popButton);
	void DeAttachButton();


	void ChangeViewTo();

	void MakeLines();
	void MakeLine(const KonfIdent &rrpKonfId, KONFDATA *prpData);

	void DeleteAll();

	int CreateLine(KONF_LINEDATA &rpKonf);

	int CompareKonf(KONF_LINEDATA *prpKonf1, KONF_LINEDATA *prpKonf2);

	void UpdateDisplay();

	int FindLine(const KonfIdent &rrpKonfId);

	void InitTable(); 
	CCSTable *pomTable;

	CCSPtrArray<KONF_LINEDATA> omLines;

	bool bmAttention;
	bool bmIsActiv;


	void DdxChangeKonf(KonfIdent &rrpIdNotify);
	void DdxInsertKonf(KonfIdent &rrpIdNotify);
	void DdxDeleteKonf(KonfIdent &rrpIdNotify);

	CString omFileName;
	void SaveToReg();

// Implementation
protected:
	bool bmIsCreated;
	CString m_key; 

	CCSButtonCtrl *pomButton;

	// Generated message map functions
	//{{AFX_MSG(SpecialConflictDlg)
	virtual BOOL OnInitDialog();
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SpecialConflictDlg_H__BBB97141_04EA_11D2_8564_0000C04D916B__INCLUDED_)
