#ifndef AFX_FLIGHTPERMITDLG_H__38DCC492_6ACA_4104_9458_F50BE307EE95__INCLUDED_
#define AFX_FLIGHTPERMITDLG_H__38DCC492_6ACA_4104_9458_F50BE307EE95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FlightPermitDialog.h : header file
//

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaFpeData.h>

////////////////////////
////////////////////////////////////////////////////
// FlightPermitDialog dialog


class FlightPermitDialog : public CDialog
{
private:
	CCSPtrArray <FPEDATA> omData; 
	FPEDATA *prmSelectFpe;
// Construction
public:
	FPEDATA* GetSelectedFlightPermit();
	void FlightPermitDialog::AddFlightPermit(FPEDATA* prlFpe);
	FlightPermitDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(FlightPermitDialog)
	enum { IDD = IDD_FLIGHTPERMITS_DLG };
	CListCtrl	m_LstCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlightPermitDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FlightPermitDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLIGHTPERMITDLG_H__38DCC492_6ACA_4104_9458_F50BE307EE95__INCLUDED_)
