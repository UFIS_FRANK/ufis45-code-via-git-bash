// FlnoAskDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <FlnoAskDlg.h>
#include <CedaFlnoData.h>
#include <CCSGlobl.h>
#include <resrc1.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FlnoAskDlg dialog


FlnoAskDlg::FlnoAskDlg(CString opSeas, CString opAlc,CWnd* pParent /*=NULL*/)
	: CDialog(FlnoAskDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FlnoAskDlg)
	m_CC_ViewActiv = FALSE;
	//}}AFX_DATA_INIT

	omSeas = opSeas;
	omAlc = opAlc;

//	Create(IDD,pParent);
//	ShowWindow(SW_SHOWNORMAL);
}

FlnoAskDlg::~FlnoAskDlg(void)
{
}

BOOL FlnoAskDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

m_FlnoList.SetFont(&ogCourier_Regular_10);
	if (omAlc.GetLength() == 2)
	{
		CString olTmp;
		if(ogBCD.GetField("ALT", "ALC2", "ALC3", omAlc, olTmp, olTmp))
		{
			omAlc = olTmp;
		}
	}

	ogFlnoData.ReadFlnoData(omSeas,omAlc);
	
	for (int index = 0; index < ogFlnoData.omData.GetSize(); index++) {
		char tmpString[14];

		FLNODATA tmpFlno = ogFlnoData.omData[index];
		sprintf(tmpString,"%6s %s",tmpFlno.Flno,tmpFlno.Adid);
		int tmpListIndex = m_FlnoList.AddString(tmpString);
		m_FlnoList.SetItemDataPtr(tmpListIndex,&ogFlnoData.omData[index]);
	}

	return TRUE;
}

void FlnoAskDlg::SetMessage(CString opMessage)
{
	m_FlnoList.AddString(opMessage);
	if ((m_FlnoList.GetCount()-6) > 0)
		m_FlnoList.SetTopIndex(m_FlnoList.GetCount()-6);
	UpdateWindow();

	SetWindowPos(&wndTop,0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
}


void FlnoAskDlg::RemoveAllMessage()
{
	m_FlnoList.ResetContent();
	UpdateWindow();
}



BOOL FlnoAskDlg::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	//pogFlnoAskDlg = NULL;
//	delete this;
	return blRc;
}

void FlnoAskDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlnoAskDlg)
	DDX_Control(pDX, IDCANCEL, m_CB_Cancel);
	DDX_Control(pDX, IDC_OK, m_CB_Ok);
//	DDX_Control(pDX, IDC_DOWNLOAD, m_CB_DownLoad);
	DDX_Control(pDX, IDC_DATALIST, m_FlnoList);
//	DDX_Check(pDX, IDC_VIEWACTIV, m_CC_ViewActiv);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlnoAskDlg, CDialog)
	//{{AFX_MSG_MAP(FlnoAskDlg)
//	ON_BN_CLICKED(IDC_DOWNLOAD, OnDownload)
ON_BN_CLICKED(IDC_OK, OnOK)
ON_BN_CLICKED(IDCANCEL, OnCancel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlnoAskDlg message handlers


void FlnoAskDlg::OnOK()
{
	char clFlno[24];

	pomFlno = NULL;

	int ilIndex = m_FlnoList.GetCurSel();
	if(ilIndex != LB_ERR)
	{
		m_FlnoList.GetText(ilIndex,clFlno);
		pomFlno = (FLNODATA *)m_FlnoList.GetItemDataPtr(ilIndex);
	} 

	CDialog::OnOK();
}

void FlnoAskDlg::OnCancel()
{
	CDialog::OnCancel();
}

