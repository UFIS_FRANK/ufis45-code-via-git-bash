#ifndef AFX_SEASONCOLLECTADASKDLG_H__89520551_AAB2_11D1_A3D1_0000B45A33F5__INCLUDED_
#define AFX_SEASONCOLLECTADASKDLG_H__89520551_AAB2_11D1_A3D1_0000B45A33F5__INCLUDED_

// SeasonCollectADAskDlg.h : Header-Datei
//
#include <SeasonCedaFlightdata.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SeasonCollectADAskDlg 

class SeasonCollectADAskDlg : public CDialog
{
// Konstruktion
public:
	SeasonCollectADAskDlg(CWnd* pParent = NULL, SEASONFLIGHTDATA *prpAFlight = NULL, SEASONFLIGHTDATA *prpDFlight = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(SeasonCollectADAskDlg)
	enum { IDD = IDD_SEASONCOLLECTASKAD };
	CButton	m_CB_OKD;
	CButton	m_CB_OKA;
	//}}AFX_DATA


	SEASONFLIGHTDATA *prmAFlight;
	SEASONFLIGHTDATA *prmDFlight;

	// return value
	SEASONFLIGHTDATA *prmSelFlight;
	CString omAdid;
	CTime omSto;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(SeasonCollectADAskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(SeasonCollectADAskDlg)
	afx_msg void OnOka();
	afx_msg void OnOkd();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SEASONCOLLECTADASKDLG_H__89520551_AAB2_11D1_A3D1_0000B45A33F5__INCLUDED_
