#if !defined(AFX_SpotAllocateDlg_H__E99642A7_F6F0_11D2_A1A9_0000B4984BBE__INCLUDED_)
#define AFX_SpotAllocateDlg_H__E99642A7_F6F0_11D2_A1A9_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpotAllocateDlg.h : header file
//

#include <DiaCedaFlightData.h>
#include <CCSEdit.h>
#include <CCSGlobl.h>

/////////////////////////////////////////////////////////////////////////////
// SpotAllocateDlg dialog

class SpotAllocateDlg : public CDialog
{
// Construction
public:


	std::vector <CString> vCBOData;  // TSC

	SpotAllocateDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SpotAllocateDlg)
	enum { IDD = IDD_ALLOCATE_GATPOS };
	CButton		m_CB_Allocate_Wro;
	CButton		m_CB_Allocate_Pos;
	CButton		m_CB_Allocate_Gat;
	CButton		m_CB_Allocate_Blt;
	CComboBox	m_CB_Allocate_User;
	CProgressCtrl	m_Progress;
	CCSEdit		m_CE_GatLeftBuffer;
	CCSEdit		m_CE_GatRightBuffer;
	CCSEdit		m_CE_PstLeftBuffer;
	CCSEdit		m_CE_PstRightBuffer;
	CCSEdit		m_CE_Date_From;
	CCSEdit		m_CE_Date_To;
	CCSEdit		m_CE_Time_From;
	CCSEdit		m_CE_Time_To;
	int			m_GatLeftBuffer;
	int			m_GatRightBuffer;
	int			m_PstLeftBuffer;
	int			m_PstRightBuffer;
	//}}AFX_DATA

	void OnReset();

	void GetParameters() ;

	
	
	void OnUserAllocate();//TSC 080904
	
	void CheckUserAllocateCBO();//TSC 090904
	
	void OnSelchangeCombo();//TSC 090904

	void LoadUserAllocate(); // TSC 100904
	
	AllocateParameters omAllocParameter;



// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SpotAllocateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

    CCSPtrArray<DIAFLIGHTDATA> omSaveData;

	// Generated message map functions
	//{{AFX_MSG(SpotAllocateDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg LONG OnProgressUpdate(UINT wParam, LPARAM lParam);
    afx_msg LONG OnProgressInit(UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	bool CheckAll(void);
	void UpdateGatPosDiagrams();

};



//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SpotAllocateDlg_H__E99642A7_F6F0_11D2_A1A9_0000B4984BBE__INCLUDED_)
