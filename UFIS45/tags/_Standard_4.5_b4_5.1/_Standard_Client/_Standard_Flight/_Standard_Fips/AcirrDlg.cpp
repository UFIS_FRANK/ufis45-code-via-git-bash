// AcirrDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <AcirrDlg.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <RecordSet.h>
#include <PrivList.h>
#include <Utils.h>
  
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CAcirrDlg 


CAcirrDlg::CAcirrDlg(CWnd* pParent, CString opReg)
	: CDialog(CAcirrDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAcirrDlg)
	m_Lfzkennung = _T("");
	m_Rem = _T("");
	m_Apu = FALSE;
	//}}AFX_DATA_INIT
	omReg = opReg;
	m_key = "DialogPosition\\AircraftIrregularities";
}


void CAcirrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAcirrDlg)
	DDX_Control(pDX, IDOK, m_CB_OK);
	DDX_Control(pDX, IDC_APU, m_CE_Apu);
	DDX_Control(pDX, IDC_REM, m_CE_Rem);
	DDX_Control(pDX, IDC_LFZKENNUNG, m_CE_Lfzkennung);
	DDX_Text(pDX, IDC_LFZKENNUNG, m_Lfzkennung);
	DDX_Text(pDX, IDC_REM, m_Rem);
	DDX_Check(pDX, IDC_APU, m_Apu);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAcirrDlg, CDialog)
	//{{AFX_MSG_MAP(CAcirrDlg)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_WM_SIZE()
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CAcirrDlg 

BOOL CAcirrDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_key = "DialogPosition\\AircraftIrregularities";

	SetWndStatAll(ogPrivList.GetStat("ACIRRDLG_m_CB_OK"),m_CB_OK);

	// Rem Feld auf 40 Zeichen begrenzen
	m_CE_Rem.SetTextLimit( 0, 40, true);
	m_CE_Lfzkennung.SetTextLimit( 5, 12);

	if (omReg.GetLength())
	{
		m_CE_Lfzkennung.SetInitText(omReg);
		FillFields(omReg);
	}

	m_resizeHelper.Init(this->m_hWnd);

	CPoint point;
	::GetCursorPos(&point);

	CRect olRect;
	this->GetWindowRect(&olRect);
	this->MoveWindow(point.x, point.y, olRect.Width(), olRect.Height());
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);// | SWP_NOMOVE );


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CAcirrDlg::OnSize(UINT nType, int cx, int cy)  
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	m_resizeHelper.OnSize();
	this->Invalidate();
}

void CAcirrDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CAcirrDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}

void CAcirrDlg::OnOK() 
{
		CString olApu;
		if(m_CE_Apu.GetCheck() == 1)
			olApu = "X";	
		else
			olApu = " ";

		m_CE_Rem.GetWindowText(m_Rem);
		m_CE_Lfzkennung.GetWindowText(m_Lfzkennung);

		{
		CString olWhere;
		m_Lfzkennung.TrimLeft();
		if(!m_Lfzkennung.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", m_Lfzkennung);
			ogBCD.Read( "ACR", olWhere);
		}
		}
		CString olTmp;
		bool blRet = ogBCD.GetField("ACR", "REGN", m_Lfzkennung, "APUI", olTmp );

		if(!blRet)
		{
			MessageBox(GetString(IDS_STRING900), GetString(ST_FEHLER),MB_ICONERROR);
			return;
		}

		// Feldinhalte in Stammdaten
		char pclTmp[128];
		CString olFields("REGN,APUI,REMA");
		sprintf(pclTmp,"%s,%s,%s", m_Lfzkennung,olApu,m_Rem);
		CString olData(pclTmp); // Komma getrennte Liste der Feldinhalte

		sprintf(pclTmp,"WHERE REGN = '%s'", m_Lfzkennung);
		CString olWhere(pclTmp);

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		
		blRet = ogBCD.UpdateSpecial( "ACR", olFields, olWhere, olData);	// blRet==0

		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		
		CDialog::OnOK();
}


bool CAcirrDlg::FillFields( CString opRegn)
{
	CString olTmp;
	CString olStdApc;
	CString olStdRem;
	bool blRet = false;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	m_CE_Lfzkennung.GetWindowText(m_Lfzkennung);

	if(!m_Lfzkennung.IsEmpty())
	{
		CString olWhere;
		m_Lfzkennung.TrimLeft();
		if(!m_Lfzkennung.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", m_Lfzkennung);
			ogBCD.Read( "ACR", olWhere);
		}

		blRet = ogBCD.GetField("ACR", "REGN", m_Lfzkennung, "APUI", olStdApc );
		if(blRet == true)
		{
			CCSPtrArray<RecordSet> omData;
			char pclWhere[128];
			CString olFields("APUI,REMA");
			
			sprintf(pclWhere,"WHERE REGN = '%s'", m_Lfzkennung);
			CString olWhere(pclWhere);
			if(ogBCD.ReadSpecial( "ACR", "APUI,REMA", pclWhere, omData))
			{

				if(omData.GetSize() > 0)
				{
					RecordSet *prlRecord = &omData[0];
				
					olStdApc =  (*prlRecord)[0];
					olStdRem =  (*prlRecord)[1];

					if(olStdApc == "X")
						m_CE_Apu.SetCheck(TRUE);
					else
						m_CE_Apu.SetCheck(FALSE);

					m_CE_Rem.SetInitText(olStdRem);
				}
			}
		}
		else
		{
			MessageBox(GetString(IDS_STRING900), GetString(ST_FEHLER),MB_ICONERROR);
		}
	}

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	return blRet;

}



LONG CAcirrDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if((UINT)m_CE_Lfzkennung.imID == wParam)
	{
		prlNotify->UserStatus = true;
		prlNotify->Status = FillFields(prlNotify->Text);
	}
	return 0L;
}



void CAcirrDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

