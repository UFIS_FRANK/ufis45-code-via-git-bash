#if !defined(AFX_DGanttCHARTREPORT_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
#define AFX_DGanttCHARTREPORT_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DGanttChartReport.h : header file
//
#include <CColor.h>
#include <CCSPtrArray.h>
#include <CCSPrint.h>

/////////////////////////////////////////////////////////////////////////////
// DGanttChartReport window

class DGanttBarReport;

class DGanttChartReport : public CScrollView
{
// Construction
public:
	DGanttChartReport(CWnd* ppParent, CRect opRect, CString opXAxe, CString opYAxe, 
		             CString opTotalNumber, CColor opColor);
	DGanttChartReport();
	DECLARE_DYNCREATE(DGanttChartReport)

	virtual ~DGanttChartReport();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DGanttChartReport)
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetData1(const CUIntArray &opData);
	void SetData2(const CUIntArray &opData);
	void SetXAxeText(const CStringArray& opXAxeText);
	void SetBarText(const CStringArray& opBarText);
	void SetTotalNumnber(int ipTotalNumber);
	void SetLegende(CString opLeft, CString opRight, CString opMiddle);

	int GetGanttBarWidth();
	int GetGanttBarWidthPrint();
	int GetGanttYBeginn();
	int GetGanttYBeginnPrint();
	CFont* GetGanttChartFont();
	void Print(CDC* popDC, CCSPrint* popPrint, CString opHeader, CString opFooterLeft);

	// Generated message map functions
protected:
	//{{AFX_MSG(GanttChartReport)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	void SetHighAndLowData();
	int GetBarHeight1(int ipIndex);
	int GetBarHeight1Print(int ipIndex);
	int GetBarHeight2(int ipIndex);
	int GetBarHeight2Print(int ipIndex);

	// PRINTING
	void PrintHeader(CCSPrint* popPrint, CString opHeader);
	void PrintFooter(CCSPrint* popPrint, CString opFooter, int ipPageNo);
	void PrintCoordinateSystem(CDC* popDC);
	void PrintBars(CDC* popDC, CCSPrint* popPrint);

private:
	CRect omRectWnd;
	CRect omRectPrn;
	CString omXAxe;
	CString omYAxe;
	CString omTotalNumber;
	CStringArray omXAxeText;
	CStringArray omBarText;
	CString omLeft;
	CString omRight;
	CString omLeftPrint;
	CString omRightPrint;
	CString omMiddle;
	CUIntArray omData1;
	CUIntArray omData2;
	CColor omColor;
	int imDataCount;
	int imTotalNumber;
	CCSPtrArray <DGanttBarReport> omBars1;
	CCSPtrArray <DGanttBarReport> omBars2;
	int imHigh;
	int imLow;
	double dmFactorX;
	double dmFactorY;
	CString omHeader;
	CString omFooterLeft;
	int imBarWidth;
	int imSizeX;
	CFont omArial_7;

	DECLARE_MESSAGE_MAP()

private:
	BYTE lmCharSet;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DGanttCHARTREPORT_H__004960BF_0881_11D3_AB42_00001C019D0B__INCLUDED_)
