#if !defined(AFX_ReasonOverviewDlg_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
#define AFX_ReasonOverviewDlg_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReasonOverviewDlg.h : header file
//
// Modification History: 
//	161100	rkr	Dialog nach Vorgaben PRF Athen umgestellt:int CheckUpArrOrDEP() neu;
//	171100	rkr	Dialog unabhängig von Datenstruktur


#include <CCSTable.h>
#include <DlgResizeHelper.h>

#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// ReasonOverviewDlg dialog

class ReasonOverviewDlg : public CDialog
{
// Construction
public:
	ReasonOverviewDlg(CWnd* pParent = NULL, long lpFurn = 0);
		           
	~ReasonOverviewDlg();

// Dialog Data
	//{{AFX_DATA(ReasonOverviewDlg)
	enum { IDD = IDD_REASONFORCHANGE_OVERVIEW };
	CStatic	m_CS_GMBorder;
	//}}AFX_DATA


	void InitTable();
	void DrawHeader();
	void InitDialog();

	bool FillTableLine();

	CWnd* pomParent;

	long lmFurn;

	CCSTable *pomTable;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ReasonOverviewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ReasonOverviewDlg)
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy); 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()



	DlgResizeHelper m_resizeHelper;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ReasonOverviewDlg_H__89A8CE31_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
