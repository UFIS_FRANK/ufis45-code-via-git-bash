
// CPP-FILE 

#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaLoaTabData.h>
#include <BasicData.h>


CedaLoaTabData ogLoaTabData;

CedaLoaTabData::CedaLoaTabData()
{
	// Create an array of CEDARECINFO for LOADATA
	BEGIN_CEDARECINFO(LOADATA,LoaDataRecInfo)
		CCS_FIELD_LONG(Flnu,"FLNU", "Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_LONG(Urno,"URNO", "Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_DATE(Time,"TIME", "Erstellungsdatum", 1)
		CCS_FIELD_CHAR_TRIM(Rmrk,"RMRK", "Anwender (Ersteller)", 1)
		CCS_FIELD_CHAR_TRIM(Dssn,"DSSN", "Anwenderfeld", 1)
		CCS_FIELD_CHAR_TRIM(Type,"TYPE", "Data Type", 1)
		CCS_FIELD_CHAR_TRIM(Styp,"STYP", "Data Sub-Type", 1)
		CCS_FIELD_CHAR_TRIM(Sstp,"SSTP", "Data SubSub-Type", 1)
		CCS_FIELD_CHAR_TRIM(Ssst,"SSST", "Data SubSubSub-Type", 1)
		CCS_FIELD_CHAR_TRIM(Valu,"VALU", "Wert", 1)
		CCS_FIELD_CHAR_TRIM(Apc3,"APC3", "Via3", 1)
	END_CEDARECINFO //(LOADATA)

	// Copy the record structure
	for (int i=0; i< sizeof(LoaDataRecInfo)/sizeof(LoaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&LoaDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"LOA");
	strcpy(pcmFList,"FLNU,URNO,TIME,RMRK,DSSN,TYPE,STYP,SSTP,SSST,VALU,APC3");
	pcmFieldList = pcmFList;


}; // end Constructor



CedaLoaTabData::~CedaLoaTabData()
{
	omRecInfo.DeleteAll();
	omData.DeleteAll();
}






void CedaLoaTabData::Clear()
{
	POSITION pos;
	void  *pVoid;
	CCSPtrArray<LOADATA> *prlArray;
	for( pos = omFlnuMap.GetStartPosition(); pos != NULL; )
	{
		omFlnuMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );
		prlArray->RemoveAll();
		delete prlArray;
	}


	omFlnuMap.RemoveAll();
 	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

// only Userdata!!
void CedaLoaTabData::Read( long lpFlightUrno)
{
	Clear();

	char pclSel[256];

	if ((strcmp(pcgHome, "WAW") == 0))
		sprintf(pclSel, "WHERE FLNU = %ld AND DSSN IN ('USR','LDM','PTM','MVT')", lpFlightUrno);
	else
		sprintf(pclSel, "WHERE FLNU = %ld AND DSSN = 'USR' AND APC3 like ' %%'", lpFlightUrno);

	if (CedaAction("RT", pclSel) == false)
	{
		return;
	}

	bool ilRc = true;

    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		LOADATA *prlApx = new LOADATA;
		if ((ilRc = GetBufferRecord(ilLc,prlApx)) == true)
		{
			InsertInternal(prlApx);
		}
		else
		{
			delete prlApx;
		}
	}

}

//all data for flights 
bool CedaLoaTabData::ReadFlnus(const CString &ropFlnus) {
	CString olSelection;
	CStringArray olUrnosLists;
	bool blRc;
	bool blRet = false;
	char pclSel[10000];

	Clear();

	// Split urnolist in 100 pieces blocks
	for(int i = SplitItemList(ropFlnus, &olUrnosLists, 100) - 1; i >= 0; i--)
	{
		if ((strcmp(pcgHome, "WAW") == 0))
			sprintf(pclSel, "WHERE FLNU IN (%s)", olUrnosLists[i]);
		else
			sprintf(pclSel, "WHERE FLNU IN (%s) AND APC3 like ' %'", olUrnosLists[i]);

		blRet = CedaAction("RT", pclSel);
 		blRc = blRet;
		// Read in internal buffer
		while (blRc == true)
		{
			LOADATA *prlApx = new LOADATA;
			if ((blRc = GetFirstBufferRecord(prlApx)) == true)
			{
				InsertInternal(prlApx);
			}
			else 
			{
				delete prlApx;
			}
		}

	} 
	TRACE("CedaLoaData::ReadFlnus: Result: %ld records read!\n", omData.GetSize());

    return blRet;
}

//WAW special function
bool CedaLoaTabData::GetLoaKey(CString &opName, CString &opType, CString &opStyp, CString &opSstp, CString &opSsst, CString &opApc3, bool bpArrival)
{
	if(!opApc3.IsEmpty() && bpArrival)
	{
		opType = "";
		if (opName == "PXT" )
		{
			opType = "PAX";
			opStyp = "S";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "PXT_MVT" )
		{
			opType = "PAX";
			opStyp = "T";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "Pxfirst")
		{
			opType = "PAX";
			opStyp = "F";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "Pxbus")
		{
			opType = "PAX";
			opStyp = "B";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "Pxeco")
		{
			opType = "PAX";
			opStyp = "E";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "Pxinf")
		{
			opType = "PAX";
			opStyp = "";
			opSstp = "O";
			opSsst = "I";
		}
		else if (opName == "TrPXT")
		{
			opType = "PAX";
			opStyp = "T";
			opSstp = "R";
			opSsst = "";
		}
		else if (opName == "TP")
		{
			opType = "PAX";
			opStyp = "T";
			opSstp = "T";
			opSsst = "";
		}
		else if (opName == "PADeco")
		{
			opType = "PAD";
			opStyp = "S";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "StripMAIL")
		{
			opType = "LOA";
			opStyp = "M";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "StripCGOT")
		{
			opType = "LOA";
			opStyp = "C";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "StripBAGN")
		{
			opType = "LOA";
			opStyp = "B";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "StripBAGW")
		{
			opType = "LOA";
			if (opApc3.IsEmpty())
				opStyp = "T";
			else
				opStyp = "S";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "JumpSeat")
		{
			opType = "PCF";
			opStyp = "J";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "FConfiguration")
		{
			opType = "PCF";
			opStyp = "F";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "BConfiguration")
		{
			opType = "PCF";
			opStyp = "B";
			opSstp = "";
			opSsst = "O";
		}
		else if (opName == "EConfiguration")
		{
			opType = "PCF";
			opStyp = "E";
			opSstp = "";
			opSsst = "O";
		}
	}
	else
	{
		opType = "";
		if (opName == "PXT" )
		{
			opType = "PAX";
			opStyp = "T";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "PXT_MVT" )
		{
			opType = "PAX";
			opStyp = "T";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "Pxfirst")
		{
			opType = "PAX";
			opStyp = "F";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "Pxbus")
		{
			opType = "PAX";
			opStyp = "B";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "Pxeco")
		{
			opType = "PAX";
			opStyp = "E";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "Pxinf")
		{
			opType = "PAX";
			opStyp = "";
			opSstp = "";
			opSsst = "I";
		}
		else if (opName == "TrPXT")
		{
			opType = "PAX";
			opStyp = "T";
			opSstp = "R";
			opSsst = "";
		}
		else if (opName == "TP")
		{
			opType = "PAX";
			opStyp = "T";
			opSstp = "T";
			opSsst = "";
		}
		else if (opName == "PADeco")
		{
			opType = "PAD";
			opStyp = "T";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "StripMAIL")
		{
			opType = "LOA";
			opStyp = "M";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "StripCGOT")
		{
			opType = "LOA";
			opStyp = "C";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "StripBAGN")
		{
			opType = "LOA";
			opStyp = "B";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "StripBAGW")
		{
			opType = "LOA";
			opStyp = "S";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "JumpSeat")
		{
			opType = "PCF";
			opStyp = "J";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "FConfiguration")
		{
			opType = "PCF";
			opStyp = "F";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "BConfiguration")
		{
			opType = "PCF";
			opStyp = "B";
			opSstp = "";
			opSsst = "";
		}
		else if (opName == "EConfiguration")
		{
			opType = "PCF";
			opStyp = "E";
			opSstp = "";
			opSsst = "";
		}

	}

	if (opType.IsEmpty())
		return false;

	return true;
}


bool CedaLoaTabData::GetLoaKey(CString &opName, CString &opType, CString &opStyp, CString &opSstp, CString &opSsst)
{

	opType = "";
	if (opName == "PXT")
	{
		opType = "PAX";
		opStyp = "T";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "Pxfirst")
	{
		opType = "PAX";
		opStyp = "F";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "Pxbus")
	{
		opType = "PAX";
		opStyp = "B";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "Pxeco")
	{
		opType = "PAX";
		opStyp = "E";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "Pxinf")
	{
		opType = "PAX";
		opStyp = "";
		opSstp = "";
		opSsst = "I";
	}
	else if (opName == "TrPXT")
	{
		opType = "PAX";
		opStyp = "T";
		opSstp = "R";
		opSsst = "";
	}
	else if (opName == "TP")
	{
		opType = "PAX";
		opStyp = "T";
		opSstp = "T";
		opSsst = "";
	}
	else if (opName == "PADeco")
	{
		opType = "PAD";
		opStyp = "T";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "StripMAIL")
	{
		opType = "LOA";
		opStyp = "M";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "StripCGOT")
	{
		opType = "LOA";
		opStyp = "C";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "StripBAGN")
	{
		opType = "LOA";
		opStyp = "B";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "StripBAGW")
	{
		opType = "LOA";
		opStyp = "T";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "JumpSeat")
	{
		opType = "PCF";
		opStyp = "J";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "FConfiguration")
	{
		opType = "PCF";
		opStyp = "F";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "BConfiguration")
	{
		opType = "PCF";
		opStyp = "B";
		opSstp = "";
		opSsst = "";
	}
	else if (opName == "EConfiguration")
	{
		opType = "PCF";
		opStyp = "E";
		opSstp = "";
		opSsst = "";
	}

	if (opType.IsEmpty())
		return false;

	return true;
}

// only userdata
bool CedaLoaTabData::SetLoaData(LOADATA *prpLoaData, CString &opName, CString &opValue, long lpFlnu)
{
	if ((strcmp(pcgHome, "WAW") == 0))
		ASSERT (FALSE);

	if (prpLoaData->IsChanged == DATA_UNCHANGED)
		return true; // no change, nothing to do

	switch(prpLoaData->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		prpLoaData->Urno = ogBasicData.GetNextUrno();
		prpLoaData->Flnu = lpFlnu;
		strcpy(prpLoaData->Dssn,CString("USR"));
		if (opName == "PXT")
		{
			strcpy(prpLoaData->Type,CString("PAX"));
			strcpy(prpLoaData->Styp,CString("T"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "Pxfirst")
		{
			strcpy(prpLoaData->Type,CString("PAX"));
			strcpy(prpLoaData->Styp,CString("F"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "Pxbus")
		{
			strcpy(prpLoaData->Type,CString("PAX"));
			strcpy(prpLoaData->Styp,CString("B"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "Pxeco")
		{
			strcpy(prpLoaData->Type,CString("PAX"));
			strcpy(prpLoaData->Styp,CString("E"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "Pxinf")
		{
			strcpy(prpLoaData->Type,CString("PAX"));
			strcpy(prpLoaData->Styp,CString(""));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString("I"));
		}
		else if (opName == "TrPXT")
		{
			strcpy(prpLoaData->Type,CString("PAX"));
			strcpy(prpLoaData->Styp,CString("T"));
			strcpy(prpLoaData->Sstp,CString("R"));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "TP")
		{
			strcpy(prpLoaData->Type,CString("PAX"));
			strcpy(prpLoaData->Styp,CString("T"));
			strcpy(prpLoaData->Sstp,CString("T"));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "PADeco")
		{
			strcpy(prpLoaData->Type,CString("PAD"));
			strcpy(prpLoaData->Styp,CString("T"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "StripMAIL")
		{
			strcpy(prpLoaData->Type,CString("LOA"));
			strcpy(prpLoaData->Styp,CString("M"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "StripCGOT")
		{
			strcpy(prpLoaData->Type,CString("LOA"));
			strcpy(prpLoaData->Styp,CString("C"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "StripBAGN")
		{
			strcpy(prpLoaData->Type,CString("LOA"));
			strcpy(prpLoaData->Styp,CString("B"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "StripBAGW")
		{
			strcpy(prpLoaData->Type,CString("LOA"));
			strcpy(prpLoaData->Styp,CString("T"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "JumpSeat")
		{
			strcpy(prpLoaData->Type,CString("PCF"));
			strcpy(prpLoaData->Styp,CString("J"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "FConfiguration")
		{
			strcpy(prpLoaData->Type,CString("PCF"));
			strcpy(prpLoaData->Styp,CString("F"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "BConfiguration")
		{
			strcpy(prpLoaData->Type,CString("PCF"));
			strcpy(prpLoaData->Styp,CString("B"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		else if (opName == "EConfiguration")
		{
			strcpy(prpLoaData->Type,CString("PCF"));
			strcpy(prpLoaData->Styp,CString("E"));
			strcpy(prpLoaData->Sstp,CString(""));
			strcpy(prpLoaData->Ssst,CString(""));
		}
		break;
	case DATA_CHANGED:
		break;
	}

	strncpy(prpLoaData->Valu,opValue,6);
	strncpy(prpLoaData->Rmrk, CString(ogBasicData.omUserID), 40); 

	CTime olTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olTime);
	prpLoaData->Time = olTime;

	return true;
}

//waw special function
bool CedaLoaTabData::SetLoaDataWAW(LOADATA *prpLoaData, CString &opName, CString &opValue, long lpFlnu, CString &opApc3, bool bpArrival)
{
	if (!(strcmp(pcgHome, "WAW") == 0))
		ASSERT (FALSE);

	if (prpLoaData->IsChanged == DATA_UNCHANGED)
		return true; // no change, nothing to do


	CString olType;
	CString olStyp;
	CString olSstp;
	CString olSsst;
	CString olDssn = "LDM";

	if(opName == "PXT_MVT")
		olDssn = "MVT";

	if(opName == "TP")
		olDssn = "PTM";



	switch(prpLoaData->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		prpLoaData->Urno = ogBasicData.GetNextUrno();
		prpLoaData->Flnu = lpFlnu;

		GetLoaKey(opName, olType, olStyp, olSstp, olSsst, opApc3, bpArrival);
		
		
		strcpy(prpLoaData->Apc3, opApc3);
		strcpy(prpLoaData->Type, olType);
		strcpy(prpLoaData->Styp, olStyp);
		strcpy(prpLoaData->Sstp, olSstp);
		strcpy(prpLoaData->Ssst, olSsst);
		strcpy(prpLoaData->Dssn, olDssn);
		break;
	case DATA_CHANGED:
		break;
	}

	strncpy(prpLoaData->Valu,opValue,6);
	strncpy(prpLoaData->Rmrk, CString(ogBasicData.omUserID), 40); 

	CTime olTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olTime);
	prpLoaData->Time = olTime;

	return true;
}




LOADATA *CedaLoaTabData::GetLoa(CString &opName, CString &opDssn /*== "USR"*/)
{
	if ((strcmp(pcgHome, "WAW") == 0))
		ASSERT (FALSE);

	CString olType;
	CString olStyp;
	CString olSstp;
	CString olSsst;

	if (GetLoaKey(opName, olType, olStyp, olSstp, olSsst))
	{
		int ilCount = omData.GetSize();

		LOADATA *prlApx;	
		for (int i = 0; i < ilCount; i++)
		{
			prlApx = &omData[i];
			if (   CString(omData[i].Type) == olType && CString(omData[i].Styp) == olStyp
				&& CString(omData[i].Sstp) == olSstp && CString(omData[i].Ssst) == olSsst
				&& CString(omData[i].Dssn) == opDssn )
			{
				return &omData[i];
			}
		}
	}

	return NULL;
}

//waw special function
LOADATA *CedaLoaTabData::GetLoaWAW(CString &opName, CString &opDssn /*== "LDM"*/, CString &opApc3, bool bpArrival)
{
	if (!(strcmp(pcgHome, "WAW") == 0))
		ASSERT (FALSE);

	CString olType;
	CString olStyp;
	CString olSstp;
	CString olSsst;
	CString olTest;


	if (GetLoaKey(opName, olType, olStyp, olSstp, olSsst, opApc3, bpArrival))
	{
		int ilCount = omData.GetSize();

		LOADATA *prlApx;	
		for (int i = 0; i < ilCount; i++)
		{
			prlApx = &omData[i];

			if((CString(prlApx->Styp) == "T") && (CString(prlApx->Dssn) == "MVT"))
				olTest = omData[i].Apc3;

			if (   CString(omData[i].Type) == olType && CString(omData[i].Styp) == olStyp
				&& CString(omData[i].Sstp) == olSstp && CString(omData[i].Ssst) == olSsst
				&& CString(omData[i].Dssn) == opDssn && CString(omData[i].Apc3) == opApc3 )
			{
				return &omData[i];
			}

			if (   CString(omData[i].Type) == olType && CString(omData[i].Styp) == olStyp
				&& CString(omData[i].Sstp) == olSstp && CString(omData[i].Ssst) == olSsst
				&& CString(omData[i].Dssn) == opDssn && CString(omData[i].Apc3).IsEmpty() && opApc3 == CString(pcgHome) )
			{
				return &omData[i];
			}
		
		}
	}

	return NULL;
}



LOADATA *CedaLoaTabData::GetLoa(long lpFlnu, CString &opName, CString &opDssn /*== "USR,LDM,MVT"*/)
{

	CString olType;
	CString olStyp;
	CString olSstp;
	CString olSsst;

	if (GetLoaKey(opName, olType, olStyp, olSstp, olSsst))
	{
		int ilCount = omData.GetSize();

		CStringArray olDssnArray;
		::ExtractItemList(opDssn,&olDssnArray,',');

		LOADATA *prlApx;	
		CCSPtrArray<LOADATA> *prlArray;
		CMapStringToPtr olLoaMap;

		if(omFlnuMap.Lookup((void *)lpFlnu,(void *& )prlArray) == TRUE)
		{
			for(int i = prlArray->GetSize() - 1; i >= 0; i--)
			{
				prlApx = &(*prlArray)[i];
				if (prlApx)
				{
					if (   CString(prlApx->Type) == olType && CString(prlApx->Styp) == olStyp
						&& CString(prlApx->Sstp) == olSstp && CString(prlApx->Ssst) == olSsst )
					{
						if (olLoaMap.GetCount() == olDssnArray.GetSize())
							break;

						for (int j = 0; j < olDssnArray.GetSize(); j++)
						{
							if ( CString(prlApx->Dssn) == olDssnArray[j] )
							{
								olLoaMap.SetAt((LPCSTR)olDssnArray[j], prlApx);
							}
						}
					}
				}
			}
		}
		else
			return NULL;

		// return best value in order if not empty
		for (int l = 0; l < olDssnArray.GetSize(); l++)
		{
			prlApx = NULL;
			if (olLoaMap.Lookup(olDssnArray.GetAt(l),(void *& )prlApx))
			{
				if (prlApx)
				{
					if (CString(prlApx->Dssn) == olDssnArray.GetAt(l))
					{
						if (!CString(prlApx->Valu).IsEmpty())
							break;
					}
				}
			}
		}


		if (prlApx)
		{
			if (omUrnoMap.Lookup((void *)prlApx->Urno,(void *& )prlApx) == TRUE)
				return prlApx;
		}
		else
			return NULL;
	}

	return NULL;
}

//waw special function
LOADATA *CedaLoaTabData::GetLoaWAW(long lpFlnu, CString &opName, CString &opDssn /*== "LDM,USR,MVT,KRI"*/, CString &opApc3, bool bpArrival )
{
	if (!(strcmp(pcgHome, "WAW") == 0))
		ASSERT (FALSE);

	CString olType;
	CString olStyp;
	CString olSstp;
	CString olSsst;

	if (GetLoaKey(opName, olType, olStyp, olSstp, olSsst, opApc3, bpArrival))
	{
		int ilCount = omData.GetSize();

		CStringArray olDssnArray;
		::ExtractItemList(opDssn,&olDssnArray,',');

		LOADATA *prlApx;	
		CCSPtrArray<LOADATA> *prlArray;
		CMapStringToPtr olLoaMap;

		if(omFlnuMap.Lookup((void *)lpFlnu,(void *& )prlArray) == TRUE)
		{
			for(int i = prlArray->GetSize() - 1; i >= 0; i--)
			{
				prlApx = &(*prlArray)[i];
				if (prlApx)
				{
					if (   CString(prlApx->Type) == olType && CString(prlApx->Styp) == olStyp
						&& CString(prlApx->Sstp) == olSstp && CString(prlApx->Ssst) == olSsst 
						&& CString(prlApx->Apc3) == opApc3 )
					{
						if (olLoaMap.GetCount() == olDssnArray.GetSize())
							break;

						for (int j = 0; j < olDssnArray.GetSize(); j++)
						{
							if ( CString(prlApx->Dssn) == olDssnArray[j] )
							{
								olLoaMap.SetAt((LPCSTR)olDssnArray[j], prlApx);
							}
						}
					}
				}
			}
		}
		else
			return NULL;

		// return best value in order if not empty
		for (int l = 0; l < olDssnArray.GetSize(); l++)
		{
			prlApx = NULL;
			if (olLoaMap.Lookup(olDssnArray.GetAt(l),(void *& )prlApx))
			{
				if (prlApx)
				{
					if (CString(prlApx->Dssn) == olDssnArray.GetAt(l))
					{
						if (!CString(prlApx->Valu).IsEmpty())
							break;
					}
				}
			}
		}


		if (prlApx)
		{
			if (omUrnoMap.Lookup((void *)prlApx->Urno,(void *& )prlApx) == TRUE)
				return prlApx;
		}
		else
			return NULL;
	}

	return NULL;
}



void CedaLoaTabData::InsertInternal(LOADATA *prpApx)
{
	omData.Add(prpApx);
	omUrnoMap.SetAt((void *)prpApx->Urno,prpApx);


	TRACE("\n %s %s %s %s  %s", prpApx->Apc3, prpApx->Type, prpApx->Styp, prpApx->Sstp, prpApx->Ssst);


	CCSPtrArray<LOADATA> *prlArray;
	if(omFlnuMap.Lookup((void *)prpApx->Flnu,(void *& )prlArray) == TRUE)
	{
		for(int i = prlArray->GetSize() - 1; i >= 0; i--)
		{
			if(((*prlArray)[i]).Urno == prpApx->Urno)
			{
				prlArray->RemoveAt(i);
				break;
			}
		}
		prlArray->Add(prpApx);
	}
	else
	{
		prlArray = new CCSPtrArray<LOADATA>;
		omFlnuMap.SetAt((void *)prpApx->Flnu,prlArray);
		prlArray->Add(prpApx);
	}
}




///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


bool CedaLoaTabData::UpdateInternal(LOADATA *prpApx)
{
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpApx->Urno)
		{
			omData[i] = *prpApx;
			return true;
		}
	}
	return false;
}






void CedaLoaTabData::DeleteInternal(LOADATA *prpApx)
{
	omUrnoMap.RemoveKey((void *)prpApx->Urno);

	CCSPtrArray<LOADATA> *prlArray;
	if(omFlnuMap.Lookup((void *)prpApx->Flnu,(void *& )prlArray) == TRUE)
	{
		for(int i = prlArray->GetSize() - 1; i >= 0; i--)
		{
			if(((*prlArray)[i]).Urno == prpApx->Urno)
			{
				prlArray->RemoveAt(i);
				break;
			}
		}
		if(prlArray->GetSize() == 0)
		{
			omFlnuMap.RemoveKey((void *)prpApx->Flnu);
			delete prlArray;
		}
	}

	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpApx->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}

}


bool CedaLoaTabData::Save(LOADATA *prpApx)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpApx->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpApx->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		prpApx->Urno = ogBasicData.GetNextUrno();
		MakeCedaData(&omRecInfo,olListOfData,prpApx);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpApx->IsChanged = DATA_UNCHANGED;

		InsertInternal(prpApx);
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpApx->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpApx);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpApx->IsChanged = DATA_UNCHANGED;
		UpdateInternal(prpApx);
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpApx->Urno);
		olRc = CedaAction("DRT",pclSelection);
		DeleteInternal(prpApx);
		break;
	}

	return true;
}





