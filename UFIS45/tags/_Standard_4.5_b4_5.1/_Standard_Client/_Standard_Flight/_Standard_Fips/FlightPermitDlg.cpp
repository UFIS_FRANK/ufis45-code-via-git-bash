// FlightPermitDialog.cpp : implementation file
//

#include <stdafx.h>
#include <resrc1.h>
#include <BasicData.h>
#include <FlightPermitDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FlightPermitDialog dialog


FlightPermitDialog::FlightPermitDialog(CWnd* pParent /*=NULL*/)
	: CDialog(FlightPermitDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(FlightPermitDialog)
	prmSelectFpe = NULL;
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void FlightPermitDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightPermitDialog)
	DDX_Control(pDX, IDC_FP_LIST, m_LstCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlightPermitDialog, CDialog)
	//{{AFX_MSG_MAP(FlightPermitDialog)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightPermitDialog message handlers

BOOL FlightPermitDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_LstCtrl.InsertColumn(0, _T("Permit Number"), LVCFMT_CENTER,100);
	m_LstCtrl.InsertColumn(1, _T("Airline Code"), LVCFMT_LEFT,70);
	m_LstCtrl.InsertColumn(2, _T("A/D"), LVCFMT_LEFT,38);
	m_LstCtrl.InsertColumn(3, _T("Flight No"), LVCFMT_LEFT,55);
	m_LstCtrl.InsertColumn(4, _T("Suffix"), LVCFMT_LEFT,43);
	m_LstCtrl.InsertColumn(5, _T("Registration"), LVCFMT_LEFT,100);
	m_LstCtrl.InsertColumn(6, _T("Day"), LVCFMT_LEFT,55);
	m_LstCtrl.InsertColumn(7, _T("ATD Number"), LVCFMT_LEFT,90);	
	m_LstCtrl.InsertColumn(8, _T("Valid From"), LVCFMT_LEFT,100);
	m_LstCtrl.InsertColumn(9, _T("Valid To"), LVCFMT_LEFT,100);

	CString olDateTime;
//	char pclUrno[15];

	// List all the Valid Flight Permits for the Flight
	for(int i= 0; i < omData.GetSize();i++)
	{
		FPEDATA *prlFpe = &omData[i];
		m_LstCtrl.InsertItem(i,prlFpe->Pern);
		m_LstCtrl.SetItemText(i,1,prlFpe->Alco);
		m_LstCtrl.SetItemText(i,2,prlFpe->Adid);
		m_LstCtrl.SetItemText(i,3,prlFpe->Flno);
		m_LstCtrl.SetItemText(i,4,prlFpe->Sufx);
		m_LstCtrl.SetItemText(i,5,prlFpe->Regn);
		m_LstCtrl.SetItemText(i,6,prlFpe->Doop);
		m_LstCtrl.SetItemText(i,7,prlFpe->AtdNo);

		// Convert the time to UTC to Local
		CTime olValidFrom(prlFpe->Vafr);
		CTime olValidTo(prlFpe->Vato);

		ogBasicData.UtcToLocal(olValidFrom);
		ogBasicData.UtcToLocal(olValidTo);

		m_LstCtrl.SetItemText(i,8,olValidFrom.Format("%d.%m.%Y %H:%M"));
		m_LstCtrl.SetItemText(i,9,olValidTo.Format("%d.%m.%Y %H:%M"));
	}

	//Initialize the prmSelectFpe with the first match.
	if(omData.GetSize() > 0)
		prmSelectFpe = &omData[0];

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FlightPermitDialog::OnOK() 
{
	int iIndex = -1;
	iIndex = m_LstCtrl.GetNextItem(iIndex,LVNI_SELECTED);

	if (iIndex == -1)
	{
		::MessageBox(NULL, "Please select a Flight Permit", "Warning", MB_OK | MB_ICONWARNING);
		return;
	}
	
	// Return the Selected Flight Permit or the First Flight Permit
	prmSelectFpe = &omData[iIndex];

	CDialog::OnOK();
}

void FlightPermitDialog::OnClose()
{

	int iIndex = -1;
	iIndex = m_LstCtrl.GetNextItem(iIndex,LVNI_SELECTED);

	
	// Return the Selected Flight Permit or the First Flight Permit
	prmSelectFpe = NULL;

	CDialog::OnClose();
}

// Add the Flight Permit to the omData Array
void FlightPermitDialog::AddFlightPermit(FPEDATA *prlFpe)
{
	omData.Add(prlFpe);
}

// Return the Selected Flight Permit
FPEDATA* FlightPermitDialog::GetSelectedFlightPermit()
{
	return prmSelectFpe;
}
