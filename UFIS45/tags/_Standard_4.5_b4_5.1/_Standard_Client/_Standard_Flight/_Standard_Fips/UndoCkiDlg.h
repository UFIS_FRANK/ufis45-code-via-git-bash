#if !defined(AFX_UndoCkiDlg_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_)
#define AFX_UndoCkiDlg_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// UndoCkiDlg.h : header file
//

#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// UndoCkiDlg dialog

class UndoCkiDlg : public CDialog
{
// Construction
public:
	UndoCkiDlg(CWnd* pParent = NULL);   // standard constructor

	~UndoCkiDlg();

	void AppExit();	

	void SetMessage(CString opMessage);
	void Reset();


// Dialog Data
	//{{AFX_DATA(RotationJoinDlg)
	enum { IDD = IDD_UNDO_CKI };
	//}}AFX_DATA

	CListBox	m_List;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UndoCkiDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(UndoCkiDlg)
	virtual void OnUndo();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UndoCkiDlg_H__F562F070_7882_11D1_8347_0080AD1DC701__INCLUDED_)
