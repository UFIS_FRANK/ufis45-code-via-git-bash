// RotationTableChart.h : header file
//

#ifndef _ROTATIONTABLECHART_
#define _ROTATIONTABLECHART_


//#include "RotationTables.h"
#include <CCSButtonCtrl.h>
#include <CCSTable.h>
#include <CCS3DStatic.h>
#include <CCSDragDropCtrl.h>
//#include "RotationTableViewer.h"

/*
#ifndef _CHART_STATE_
#define _CHART_STATE_
	enum ChartState { Minimized, Normal, Maximized };
#endif // _CHART_STATE_
*/

enum GROUPS
{
	UD_ARRIVAL,
	UD_AGROUND,
	UD_DGROUND,
	UD_DEPARTURE,
	UD_FAIL
};


/////////////////////////////////////////////////////////////////////////////
// FlightTableChart frame

class RotationTables;
class RotationTableViewer;

class RotationTableChart : public CFrameWnd
{
	friend RotationTables;

    DECLARE_DYNCREATE(RotationTableChart)
public:
    RotationTableChart(CWnd *popParent = NULL, int ipGroupID = -1, CString opGroupText = "",  RotationTableViewer *popViewer = NULL); 

    virtual ~RotationTableChart();

// Operations
public:

    
	
	int GetHeight();
    ChartState GetState(void) { return imState; };
    void SetState(ChartState ipState) { imState = ipState; };
    


    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };
    
    //MWO ==> this should be solved in near future 
	 // GateDiagramViewer *GetViewer(void) { return pomViewer; };
    int GetGroupNo() { return imGroupID; };
    //MWO ==> this should be solved in near future 
    //void SetViewer(GateDiagramViewer *popViewer, int ipGroupNo)
    //{
    //   pomViewer = popViewer;
    //    imGroupNo = ipGroupNo;
    //};
    

    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };

    CCSTable *GetGanttPtr(void) { return &omTable; };
    CCSButtonCtrl *GetChartButtonPtr(void) { return &omButton; };
    CCS3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };

    


// Overrides
public:

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(RotationTableChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnChartButton();
    afx_msg void OnChartButton2();
    afx_msg void OnChartButton3();
    afx_msg void OnChartButton4();
    afx_msg void OnChartButtonTowing();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDestroy();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnEditLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableIPEdit(UINT wParam, LONG lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg LONG OnTableReturnPressed(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

public:
	RotationTables *GetParentRotationTable();		// 050224 MVy: return the parent window


protected:
	CWnd *pomParent;
    ChartState imState;
    int imHeight;
	int imTableHeight;
	BOOL bmSet;    
	BOOL bmIsResizing;
    CCSButtonCtrl omButton;
    CCSButtonCtrl omButton2;
    CCSButtonCtrl omButton4;
    CCSButtonCtrl omButtonMax;
    CCSButtonCtrl omButtonTowing;
    CCS3DStatic *pomTopScaleText;
    
    CStatusBar *pomStatusBar;
    //MWO ==> this should be solved in near future 
    //GateDiagramViewer *pomViewer;
    int imGroupID;
	CString omGroupText;
    RotationTableViewer *pomViewer;

    CTime omStartTime;
    CTimeSpan omInterval;

protected:
	CCSTable omTable;
public:
	CCSTable* GetTable();		// 050302 MVy: accessing table

private:    
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
    int imStartTopScaleTextPos;
	int imStartVerticalScalePos;

	BOOL HandlePostFlight(BOOL bplPostflight);
	BOOL HandleWindowText(BOOL bplPostflight);

// Drag-and-drop section
public:
    CCSDragDropCtrl m_ChartButtonDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;

	// Definition of drag-and-drop object
	CCSDragDropCtrl omDragDropObject;
	CCSDragDropCtrl omDragDropTarget;


	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);
};

/////////////////////////////////////////////////////////////////////////////

#endif // _ROTATIONTABLECHART_
