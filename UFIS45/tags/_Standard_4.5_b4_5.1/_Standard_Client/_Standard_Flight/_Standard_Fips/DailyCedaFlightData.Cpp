// cflightd.cpp - Read flight data
// 

#include <stdafx.h>
#include <ccsglobl.h>
#include <BasicData.h>
#include <DailyCedaFlightData.h>
#include <Utils.h>
#include <RotationDlgCedaFlightData.h>
#include <Fpms.h>
#include <ButtonListDlg.h>
#include <CedaCcaData.h>

// Local function prototype
static void ProcessDailyFlightCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);

#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))


static int CompareRotationFlight(const DAILYFLIGHTDATA **e1, const DAILYFLIGHTDATA **e2)
{
	if((**e1).Rkey == (**e2).Rkey)
	{
		if( strcmp((**e1).Adid ,(**e2).Adid ) == 0)
		{
			return (((**e1).Tifa == (**e2).Tifa) ? 0 : (((**e1).Tifa > (**e2).Tifa) ? 1 : -1));
		}
		else
		{
			return ( strcmp((**e1).Adid , (**e2).Adid) );
		}
	}
	else
	{
		return ((**e1).Rkey >  (**e2).Rkey)? 1: -1;
	}


}

static int CompareRotation(const DAILYFLIGHTDATA **e1, const DAILYFLIGHTDATA **e2)
{
	CTime olTime1;
	CTime olTime2;
	int ilRet = 0;

	/*
	if((strcmp((**e1).Des3, pcgHome) == 0) && (strcmp((**e1).Org3, pcgHome) != 0) && (strcmp((**e2).Des3, pcgHome) != 0) && (strcmp((**e2).Org3, pcgHome) == 0))
		return -1;

	if((strcmp((**e1).Des3, pcgHome) != 0) && (strcmp((**e1).Org3, pcgHome) == 0) && (strcmp((**e2).Des3, pcgHome) == 0) && (strcmp((**e2).Org3, pcgHome) != 0))
		return 1;
	*/

	if(strcmp((**e2).Adid, "B") == 0 && strcmp((**e1).Adid, "B") == 0)
	{

		if((**e1).Tifa == TIMENULL)
			olTime1 = (**e1).Tifd;
		else
			olTime1 = (**e1).Tifa;

		if((**e2).Tifa == TIMENULL)
			olTime2 = (**e2).Tifd;
		else
			olTime2 = (**e2).Tifa;

		ilRet =  ((olTime1 == olTime2) ? 0 : ((olTime1 > olTime2) ? 1 : -1));
	}
	else
	{
		ilRet = strcmp((**e1).Adid, (**e2).Adid);

	}


	return ilRet;
}



////////////////////////////////////////////////////////////////////////////
// Construktor   
//
DailyCedaFlightData::DailyCedaFlightData() : CCSCedaData(&ogCommHandler)
{
	//pomCommHandler = popCommHandler;
    /* Create an array of CEDARECINFO for DAILYFLIGHTDATA*/
	BEGIN_CEDARECINFO(DAILYFLIGHTDATA,AftDataRecInfo)


	CCS_FIELD_CHAR_TRIM(Act3,"ACT3","Flugzeug-3-Letter Code", 1)
	CCS_FIELD_CHAR_TRIM(Act5,"ACT5","Flugzeug-5-Letter Code", 1)
	CCS_FIELD_DATE(Airb,"AIRB","Startzeit (Beste Zeit)", 1)
	CCS_FIELD_DATE(Airu,"AIRU","Startzeit User", 1)
	CCS_FIELD_CHAR_TRIM(Csgn,"CSGN","Call- Sign", 1)
	CCS_FIELD_DATE(Slot,"SLOT","Slot-Zeit", 1)
	CCS_FIELD_CHAR_TRIM(Des3,"DES3","Bestimmungsflughafen 4-Lettercode", 1)
	CCS_FIELD_CHAR_TRIM(Des4,"DES4","Bestimmungsflughafen 4-Lettercode", 1)
	CCS_FIELD_DATE(Etdc,"ETDC","ETD-Confirmed", 1)
	CCS_FIELD_DATE(Etai,"ETAI","ETA", 1)
	CCS_FIELD_DATE(Etdi,"ETDI","ETD", 1)
	CCS_FIELD_CHAR(Flno,0,"FLNO","komplette Flugnummer (Airline, Nummer, Suffix)", 1)
	CCS_FIELD_CHAR_TRIM(Ftyp,"FTYP","Type des Flugs [F, P, R, X, D, T, ...]", 1)
	CCS_FIELD_CHAR_TRIM(Gta1,"GTA1","Gate 1 Ankunft", 1)
	CCS_FIELD_CHAR_TRIM(Gta2,"GTA2","Gate 2 Ankunft", 1)
	CCS_FIELD_CHAR_TRIM(Gtd1,"GTD1","Gate 1 Abflug", 1)
	CCS_FIELD_CHAR_TRIM(Gtd2,"GTD2","Doppelgate Abflug", 1)
	CCS_FIELD_CHAR_TRIM(Ifra,"IFRA","Anflugverfahren", 1)
	CCS_FIELD_CHAR_TRIM(Ifrd,"IFRD","Abflugverfahren", 1)
	CCS_FIELD_CHAR_TRIM(Isre,"ISRE","Bemerkungskennzeichen intern", 1)
	CCS_FIELD_DATE(Land,"LAND","Landezeit (Beste Zeit)", 1)
	CCS_FIELD_DATE(Lstu,"LSTU","Datum letzte �nderung", 1)
	CCS_FIELD_DATE(Ofbl,"OFBL","Offblock- Zeit (Beste Zeit)", 1)
	CCS_FIELD_DATE(Onbe,"ONBE","Est. Onblock-Zeit", 1)
	CCS_FIELD_DATE(Onbl,"ONBL","Onblock-Zeit (Beste Zeit)", 1)
	CCS_FIELD_CHAR_TRIM(Org3,"ORG3","Ausgangsflughafen 4-Lettercode", 1)
	CCS_FIELD_CHAR_TRIM(Org4,"ORG4","Ausgangsflughafen 4-Lettercode", 1)
	CCS_FIELD_CHAR_TRIM(Psta,"PSTA","Position Ankunft", 1)
	CCS_FIELD_CHAR_TRIM(Pstd,"PSTD","Position Abflug", 1)
	CCS_FIELD_CHAR_TRIM(Regn,"REGN","LFZ-Kennzeichen", 1)
	CCS_FIELD_CHAR_TRIM(Remp,"REMP","Public Remark FIDS", 1)
	CCS_FIELD_LONG(Rkey,"RKEY","Rotationsschl�ssel", 1)
	CCS_FIELD_CHAR_TRIM(Rwya,"RWYA","Landebahn", 1)
	CCS_FIELD_CHAR_TRIM(Rwyd,"RWYD","Startbahn", 1)
	CCS_FIELD_CHAR_TRIM(Stab,"STAB","Datenstatus AIRB", 1)
	CCS_FIELD_DATE(Stoa,"STOA","Planm��ige Ankunftszeit STA", 1)
	CCS_FIELD_DATE(Stod,"STOD","Planm��ige Abflugzeit", 1)
	CCS_FIELD_DATE(Tifa,"TIFA","Zeitrahmen Ankunft", 1)
	CCS_FIELD_DATE(Tifd,"TIFD","Zeitrahmen Abflug", 1)
	CCS_FIELD_DATE(Tmoa,"TMOA","TMO (Beste Zeit)", 1)
	CCS_FIELD_LONG(Urno,"URNO","Eindeutige Datensatz-Nr.", 1)
	CCS_FIELD_CHAR_TRIM(Useu,"USEU","Anwender (letzte �nderung)", 1)
	CCS_FIELD_CHAR_TRIM(Via4,"VIA4","", 1)
	CCS_FIELD_CHAR_TRIM(Vian,"VIAN","Anzahl der Zwischenstationen - 1", 1)
	CCS_FIELD_CHAR_TRIM(Ttyp,"TTYP","Verkehrsart", 1)
	CCS_FIELD_CHAR_TRIM(Chgi,"CHGI","", 1)
	CCS_FIELD_CHAR_TRIM(Rtyp,"RTYP","", 1)
	CCS_FIELD_CHAR_TRIM(Adid,"ADID","", 1)
	CCS_FIELD_DATE(Nxti,"NXTI","", 1)
	CCS_FIELD_CHAR_TRIM(Blt1,"BLT1","Gep�ckband 1", 1)
	CCS_FIELD_DATE(Paba,"PABA","Belegung Position Ankunft aktueller Beginn", 1)
	CCS_FIELD_DATE(Paea,"PAEA","Belegung Position Ankunft aktuelles Ende", 1)
	CCS_FIELD_DATE(Pdba,"PDBA","Belegung Position Abflug aktueller Beginn", 1)
	CCS_FIELD_DATE(Pdea,"PDEA","Belegung Position Abflug aktuelles Ende", 1)
	CCS_FIELD_DATE(Paba,"PABS","Belegung Position Ankunft geplanter Beginn", 1)
	CCS_FIELD_DATE(Paea,"PAES","Belegung Position Ankunft geplantes Ende", 1)
	CCS_FIELD_DATE(Pdba,"PDBS","Belegung Position Abflug geplanter Beginn", 1)
	CCS_FIELD_DATE(Pdea,"PDES","Belegung Position Abflug geplantes Ende", 1)
	CCS_FIELD_CHAR_TRIM(Ming,"MING","Mindestbodenzeit", 1)
	CCS_FIELD_CHAR_TRIM(Ader,"ADER","", 1)
	CCS_FIELD_CHAR_TRIM(Vial,"VIAL","Liste der Zwischenstationen/-zeiten", 1)
	CCS_FIELD_CHAR_TRIM(Ckif,"CKIF","Check-In From", 1)
	CCS_FIELD_CHAR_TRIM(Ckit,"CKIT","Check-In To", 1)
	CCS_FIELD_CHAR_TRIM(Blt2,"BLT2","Gep�ckband 2", 1)
	CCS_FIELD_CHAR_TRIM(Flti,"FLTI","", 1)
	CCS_FIELD_CHAR_TRIM(Via3,"VIA3","", 1)
	CCS_FIELD_CHAR_TRIM(Alc3,"ALC3","Fluggesellschaft (Airline 3-Letter Code)", 1)
	CCS_FIELD_CHAR_TRIM(Fltn,"FLTN","Flugnummer", 1)
	CCS_FIELD_CHAR_TRIM(Flns,"FLNS","Suffix", 1)
	CCS_FIELD_CHAR(Prfl,0,"PRFL","Prfl- Sign", 1)
	CCS_FIELD_DATE(B1ba,"B1BA","", 1)
	CCS_FIELD_DATE(B1ea,"B1EA","", 1)
	CCS_FIELD_CHAR_TRIM(Ckf2,"CKF2","Second CKI from", 1)
	CCS_FIELD_CHAR_TRIM(Ckt2,"CKT2","Second CKI to", 1)
	CCS_FIELD_CHAR_TRIM(Stev,"STEV","Keycode 1", 1)
	CCS_FIELD_CHAR_TRIM(Ste2,"STE2","Keycode 2", 3)
	CCS_FIELD_CHAR_TRIM(Ste3,"STE3","Keycode 3", 3)
	CCS_FIELD_CHAR_TRIM(Ste4,"STE4","Keycode 4", 3)
	CCS_FIELD_CHAR_TRIM(Faog,"FAOG","AOG Flag", 1)
	CCS_FIELD_CHAR_TRIM(Adho,"ADHO","Adhoc indicator", 1)
	END_CEDARECINFO //(AFTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(AftDataRecInfo)/sizeof(AftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AftDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
    strcpy(pcmTableName,"AFT");
                                                                                                                                                                                                                                                                                      
	strcpy(pcmAftFieldList,"ACT3,ACT5,AIRB,AIRU,CSGN,SLOT,DES3,DES4,ETDC,ETAI,ETDI,FLNO,FTYP,GTA1,GTA2,GTD1,GTD2,IFRA,IFRD,ISRE,LAND,LSTU,OFBL,ONBE,ONBL,ORG3,ORG4,PSTA,PSTD,REGN,REMP,RKEY,RWYA,RWYD,STAB,STOA,STOD,TIFA,TIFD,TMOA,URNO,USEU,VIA4,VIAN,TTYP,CHGI,RTYP,ADID,NXTI,BLT1,PABA,PAEA,PDBA,PDEA,PABS,PAES,PDBS,PDES,MING,ADER,VIAL,CKIF,CKIT,BLT2,FLTI,VIA3,ALC3,FLTN,FLNS,PRFL,B1BA,B1EA");

	if(bgShowAOG)
	{
		strcat(pcmAftFieldList,",FAOG");
	}

	if(bgShowADHOC || bgSeasonShowADHOC)
	{
		strcat(pcmAftFieldList,",ADHO");
	}

	pcmFieldList = pcmAftFieldList;
	int ilrst = sizeof(DAILYFLIGHTDATA);

	lmBaseID = IDM_ROTATION;



	//Do not read data before user has select a view <> Default


}; // end Constructor




////////////////////////////////////////////////////////////////////////////
// Destruktor   
//
DailyCedaFlightData::~DailyCedaFlightData(void)
{
	TRACE("DailyCedaFlightData::~DailyCedaFlightData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();
	ClearAll();
}




void DailyCedaFlightData::SetFieldList(void)
{

	if(bgSecondGateDemandFlag)
	{
		if( CString(pcmAftFieldList).Find("CKF2,CKT2") < 0 )
			strcat(pcmAftFieldList,",CKF2,CKT2");
	}
	if (bgUseAddKeyfields)
	{
		if( CString(pcmAftFieldList).Find("STEV,STE2,STE3,STE4") < 0 )
		strcat(pcmAftFieldList,",STEV,STE2,STE3,STE4");
	}


}


////////////////////////////////////////////////////////////////////////////
// alle Datens�tze l�schen   
//
void DailyCedaFlightData::ClearAll(void)
{
	ogDdx.UnRegister(this,NOTUSED);
	omCcaData.UnRegister();

	// VIP
	omDailyVipCedaData.ClearAll( );

 	omUrnoMap.RemoveAll();
	CString olTmp;
	DAILYSCHEDULERKEYLIST *prlRkey;
	POSITION pos;
	void *pVoid;
	for( pos = omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		omRkeyMap.GetNextAssoc( pos, pVoid , (void *&)prlRkey );
		prlRkey->Rotation.RemoveAll();
		delete prlRkey;
	}
	omRkeyMap.RemoveAll();


	omCcaData.ClearAll();


	omData.DeleteAll();

}



bool DailyCedaFlightData::ReadAllFlights(CString &opWhere, bool bpRotation)
{
	bmRotation = bpRotation;

	ClearAll();

	ogDdx.Register((void *)this,BC_FLIGHT_CHANGE,CString("DAILYFLIGHTDATA"), CString("Flight changed"),ProcessDailyFlightCf);
	ogDdx.Register((void *)this,BC_FLIGHT_DELETE,CString("DAILYFLIGHTDATA"), CString("Flight changed"),ProcessDailyFlightCf);
	ogDdx.Register(this, DIACCA_CHANGE, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), ProcessDailyFlightCf);
	ogDdx.Register((void *)this,BC_RAC_RELOAD,CString("DAILYFLIGHTDATA"), CString("Flight changed"),ProcessDailyFlightCf);
	omCcaData.Register();

	
	omSelection = opWhere;



	bool blRet = false;

	if(!omSelection.IsEmpty())
	{
		blRet = ReadFlights(NULL,false);
		ReadAllCca();
	}

	// ---
	// VIP
	// ---
	// UrnoArray aus AFT aufbauen
	CUIntArray olUrnos;
	// P�ckchen mit max Groesse 100
	CUIntArray olUrnos100;

	for (int i = omData.GetSize() - 1; i >= 0; i--)
	{
		olUrnos.Add(omData[i].Urno);
	}

	// in 100-Schritte lesen --> sonst u.U. Time-out
	int ilSteps; 
	int ilCurrStep = 0;
	int ilSizeUrnos = olUrnos.GetSize();

	if (ilSizeUrnos > 100)
	{
		ilSteps = omData.GetSize() / 100;
		// hunderter packen 
		while (ilCurrStep <= ilSteps)
		{
			// hunderte Packete f�llen
			if (ilCurrStep < ilSteps)
			{
				for (int i = 0; i < 100; i++)
				{
					olUrnos100.Add(olUrnos[i + (ilCurrStep*100)]);
				}			
			}
			// den Rest (< 100) auff�llen
			else
			{
				for (int i = 0; (ilCurrStep*100) + i < ilSizeUrnos; i++)
				{
					olUrnos100.Add(olUrnos[i + (ilCurrStep*100)]);
				}			
			}

			// Lesen der 100 Records mit URNO = FLNU
			omDailyVipCedaData.ReadAllVips(olUrnos100);

			// bereits gelesene URNO's loeschen
			olUrnos100.RemoveAll();
			// Step erhoehen 
			ilCurrStep++;
		}
	}
	else
	{
		omDailyVipCedaData.ReadAllVips(olUrnos);
	}



	for (i = omData.GetSize() -1; i >= 0; i--)
	{
		omData[i].Vip = omDailyVipCedaData.LookUpUrno(omData[i].Urno);
	}
	// ---
	// VIP
	// ---

    return blRet;

} 


void DailyCedaFlightData::ReadAllCca()
{
	char buffer[65];
	CString olUrnos; 

	omCcaData.Register();

	DAILYFLIGHTDATA *prlFlight;

	for(int i = omData.GetSize() - 1; i >= 0; i --)
	{
		prlFlight = &omData[i];

		if(strcmp(prlFlight->Org3 , pcgHome) == 0)
		{
			ltoa(prlFlight->Urno, buffer, 10);
			olUrnos += CString(buffer) + CString(",");
		}
	}

	if(!olUrnos.IsEmpty())
	{
		olUrnos = olUrnos.Left(olUrnos.GetLength() - 1);
	}
		
	CMapStringToString olFilterMap;
//	omCcaData.ReadAll(olUrnos, omFrom, omTo, olFilterMap);
	omCcaData.ReadAll(olUrnos, omFrom, omTo, olFilterMap, true, true);

}



void DailyCedaFlightData::SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps)
{
	omTo = opTo;
	omFrom = opFrom;
	omFtyps = opFtyps;
}


bool DailyCedaFlightData::ReadRkeys(CString &olRkeys, char *pcpSelection)
{
	char pclTable[64] = "AFT";
	char pclFieldList[1024] = "RKEY";
	char pclData[256] = "";


	bool blRet = CedaAction("GFR", pclTable, pclFieldList, pcpSelection, "", pclData, "BUF1");
	CFPMSApp::CheckCedaError(*this);

	CStringArray* olDataBuf = GetDataBuff();
	for (int i = olDataBuf->GetSize() - 1; i >= 0; i--)
//	for (int i = GetBufferSize() - 1; i >= 0; i--)
	{
		olRkeys += olDataBuf->GetAt(i) + CString(","); 
	}
	if(olRkeys.GetLength() > 0)
		olRkeys = olRkeys.Left(olRkeys.GetLength()-1);

	return blRet;
}





////////////////////////////////////////////////////////////////////////////
// Datens�tze aus der DB laden   
//
bool DailyCedaFlightData::ReadFlights(char *pcpSelection, bool bpDdx, bool bpIngoreView )
{
	bool blReadLoop = true;
	char pclSelection[10240] = "WHERE ";


	if((omSelection.GetLength() == 0) && (!bpIngoreView))
		return false;


	if((omSelection.GetLength() > 0) && (!bpIngoreView))
	{

		if(pcpSelection != NULL)
		{
			if(strlen(pcpSelection) > 0)
			{
				blReadLoop = false;
				strcat(pclSelection, pcpSelection);
				strcat(pclSelection, " AND ");
			}
		}
		strcat(pclSelection, (LPCSTR)omSelection);
	}
	else
	{
		if(pcpSelection != NULL)
		{
			if(strlen(pcpSelection) > 0)
			{
				strcat(pclSelection, pcpSelection);
			}
			else
				return false;
		}
		else
			return false;
	}

	bool blRet;
	bool blRc ;


	if(bmRotation)
	{
		strcat(pclSelection, "[ROTATIONS]");
/*		if (!omFtyps.IsEmpty())
		{
			CString olFilter = CString("[FILTER=FTYP IN ( ") + omFtyps + CString(")]");
			strcat(pclSelection, olFilter);
		}
*/
	}

	if (!omFtyps.IsEmpty())
	{
		CString olFilter = CString("[FILTER=FTYP IN ( ") + omFtyps + CString(")]");
		strcat(pclSelection, olFilter);
	}

	if (omSelection.Find("[LOOP_TIME_FR]",0) != -1)
	{
		CTimeSpan olStep (0,0,1440,0);
		CTimeSpan olStepInit (0,0,1439,59);
		CTime olFrom = omFrom;
		CTime olTo   = olFrom + olStepInit;
		if (olTo > omTo)
			olTo = omTo;

		if (!blReadLoop)
			olTo = omTo;

		char polSelection[10240] = "";
		while (olFrom < omTo)
		{
			CString olTime_Fr = CTimeToDBString(olFrom, TIMENULL);
			CString olTime_To =	CTimeToDBString(olTo,   TIMENULL);
			CString olDate_Fr = olTime_Fr.Left(8);
			CString olDate_To = olTime_To.Left(8);

			CString olSelection = pclSelection;
			int n = olSelection.Replace("[LOOP_TIME_FR]",olTime_Fr);
			n = olSelection.Replace("[LOOP_TIME_TO]",olTime_To);
			n = olSelection.Replace("[FRDATE]",olDate_Fr);
			n = olSelection.Replace("[TODATE]",olDate_To);

//			int ilTest = olSelection.GetLength();

			strcpy(polSelection, olSelection);
//			::MessageBox(NULL,polSelection,"Selection sel",MB_OK);

			blRet = CedaAction("GFR", polSelection);
			CFPMSApp::CheckCedaError(*this);
			blRc = blRet;
			for (int ilLc = 0; blRc == true; ilLc++)
			{
				DAILYFLIGHTDATA *prpFlight = new DAILYFLIGHTDATA();
				if ((blRc = GetFirstBufferRecord(prpFlight)) == true)
				{
					AddFlightInternal(prpFlight, bpDdx);
				}
				else
				{
					delete prpFlight;
				}
			}

			olFrom += olStep;
			olTo   += olStep;

			if (olTo > omTo)
				olTo = omTo;

			if (!blReadLoop)
				break;
		}
	}
	else
	{
		blRet = CedaAction("GFR", pclSelection);
		CFPMSApp::CheckCedaError(*this);
		blRc = blRet;
		for (int ilLc = 0; blRc == true; ilLc++)
		{
			DAILYFLIGHTDATA *prpFlight = new DAILYFLIGHTDATA();
			if ((blRc = GetFirstBufferRecord(prpFlight)) == true)
			{
				AddFlightInternal(prpFlight, bpDdx);
			}
			else
			{
				delete prpFlight;
			}
		}
	}

	omData.Sort(CompareRotationFlight);
	return blRet;
}



////////////////////////////////////////////////////////////////////////////
// Addflight- datensatz einf�gen oder �ndern
//
bool DailyCedaFlightData::AddFlightInternal(DAILYFLIGHTDATA *prpFlight, bool bpDdx)
{
	DeleteFlightInternal(prpFlight->Urno, false);
	omData.Add(prpFlight);
	AddToKeyMap(prpFlight);
	if(bpDdx)
	{
		ogDdx.DataChanged((void *)this, R_FLIGHT_CHANGE,(void *)prpFlight );
	}
	return true;
}




















////////////////////////////////////////////////////////////////////////////
// Datensatz mit Urno suchen
//
DAILYFLIGHTDATA *DailyCedaFlightData::GetFlightByUrno(long lpUrno)
{
	DAILYFLIGHTDATA *prlFlight;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlFlight) == TRUE)
		return prlFlight;
	else
		return NULL;
}



////////////////////////////////////////////////////////////////////////////
// Flug l�schen
//
bool DailyCedaFlightData::DeleteFlightInternal(long lpUrno, bool bpDdx)
{
	DAILYFLIGHTDATA *prlFlight;
	
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlFlight) == TRUE)
	{
		DeleteFromKeyMap(prlFlight);
		if(bpDdx)
			ogDdx.DataChanged((void *)this, R_FLIGHT_DELETE,(void *)prlFlight );
		for (int ilLc = omData.GetSize() - 1; ilLc >= 0; ilLc--)
		{
			if (omData[ilLc].Urno == lpUrno)
			{
				omData.DeleteAt(ilLc);
				return true;
			}
		}
	}
    return false;
}


////////////////////////////////////////////////////////////////////////////
// Datensatz in die Keymaps einf�gen 
//
bool DailyCedaFlightData::AddToKeyMap(DAILYFLIGHTDATA *prpFlight )
{
	DAILYSCHEDULERKEYLIST *prlRkey;

	CString olPst;


	if (prpFlight != NULL )
	{
		omUrnoMap.SetAt((void *)prpFlight->Urno,prpFlight);
	
		if(prpFlight->Rkey > 0)
		{
			if(omRkeyMap.Lookup((void *)prpFlight->Rkey,(void *& )prlRkey) == TRUE)
			{
				for(int i = prlRkey->Rotation.GetSize() - 1; i >= 0; i--)
				{
					if(prlRkey->Rotation[i].Urno == prpFlight->Urno)
					{
						prlRkey->Rotation.RemoveAt(i);
						break;
					}
				}
				//TRACE("\nAddToKeyMap: %ld <%ld> <%s> ",prpFlight->Urno,prpFlight->Rkey, prpFlight->Flno);
				prlRkey->Rotation.Add(prpFlight);;
				prlRkey->Rotation.Sort(CompareRotationFlight);;
			}
			else
			{
				//TRACE("\nNewToKeyMap: %ld <%ld> <%s> ",prpFlight->Urno,prpFlight->Rkey, prpFlight->Flno);
				prlRkey = new DAILYSCHEDULERKEYLIST;
				omRkeyMap.SetAt((void *)prpFlight->Rkey,prlRkey);
				prlRkey->Rotation.Add(prpFlight);;
			}
		}
	}
    return true;
}




////////////////////////////////////////////////////////////////////////////
// Datensatz aus den Keymaps l�schen 
//
bool DailyCedaFlightData::DeleteFromKeyMap(DAILYFLIGHTDATA *prpFlight )
{
	DAILYSCHEDULERKEYLIST *prlRkey;

	if (prpFlight != NULL)
	{
		omUrnoMap.RemoveKey((void *)prpFlight->Urno);

		if(omRkeyMap.Lookup((void *)prpFlight->Rkey,(void *& )prlRkey) == TRUE)
		{
			for(int i = prlRkey->Rotation.GetSize() - 1; i >= 0; i--)
			{
				if(prlRkey->Rotation[i].Urno == prpFlight->Urno)
				{
					prlRkey->Rotation.RemoveAt(i);
					break;
				}
			}
			if(prlRkey->Rotation.GetSize() == 0)
			{
				omRkeyMap.RemoveKey((void *)prpFlight->Rkey);
				delete prlRkey;
			}
		}

	}
    return true;
}

////////////////////////////////////////////////////////////////////////////
// reaktion auf broadcast 
//
void DailyCedaFlightData::ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	long llRkeyNew1 = 0;
	long llRkeyNew2 = 0;
	long llRkeyOld1 = 0;
	long llRkeyOld2 = 0;

	CStringArray olStrArray;
	DAILYSCHEDULERKEYLIST *prlRkey;
	DAILYFLIGHTDATA *prlFlight;
	bool blIgnoreView = false;

	BcStruct *prlBcStruct = (BcStruct *) vpDataPointer;

	if(prlBcStruct == NULL)
		return;

	if(atol(prlBcStruct->Tws) == IDM_IMPORT)
		return;


	if (ipDDXType == BC_FLIGHT_CHANGE)
	{

		if(strcmp(prlBcStruct->Cmd, "UFR") == 0)
		{
			ProcessFlightUFR( prlBcStruct );
			return;
		}

		/*
		if(strcmp(prlBcStruct->Cmd, "IFR") == 0)
		{
			ProcessFlightIFR( prlBcStruct );
			return;
		}
		*/

		if(strcmp(prlBcStruct->Cmd, "ISF") == 0)
		{
			ProcessFlightISF( prlBcStruct );
			return;
		}


		if(strcmp(prlBcStruct->Cmd, "UPS") == 0)
		{
			ProcessFlightUFR( prlBcStruct );
			return;
		}


		if(strcmp(prlBcStruct->Cmd, "UPJ") == 0)
		{
			ProcessFlightUFR( prlBcStruct );
			return;
		}

		/////////////////////////////////////////////////////////////////////////////
		// BC_Split
		if(strcmp(prlBcStruct->Cmd, "SPR") == 0)
		{
			ExtractItemList(CString(prlBcStruct->Data), &olStrArray);
			
			if(olStrArray.GetSize() == 2)
			{
				llRkeyOld1 = atol(olStrArray[0]);
				llRkeyNew1 = atol(olStrArray[1]);
			
				if(omRkeyMap.Lookup((void *)llRkeyOld1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
				}
				if(omRkeyMap.Lookup((void *)llRkeyNew1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
				}
			}
			return;
		}
		
		/////////////////////////////////////////////////////////////////////////////
		// BC_Join
		if(strcmp(prlBcStruct->Cmd, "JOF") == 0)
		{
			ExtractItemList(CString(prlBcStruct->Data), &olStrArray);

			if(olStrArray.GetSize() == 3)
			{
				llRkeyOld1 = atol(olStrArray[0]);
				llRkeyOld2 = atol(olStrArray[1]);
				llRkeyNew1 = atol(olStrArray[2]);
				
				if(omRkeyMap.Lookup((void *)llRkeyOld1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
				}
				if(omRkeyMap.Lookup((void *)llRkeyOld2,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
				}
				if(omRkeyMap.Lookup((void *)llRkeyNew1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
				}
			}
			return;
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	// BC_Delete
	if (ipDDXType == BC_FLIGHT_DELETE)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		

		ExtractItemList(CString(prlBcStruct->Data), &olStrArray);
		long llUrno = 0;

		if (olStrArray.GetSize() > 0)
			llUrno = atol(olStrArray[0]);
		else
			return;

		if(omUrnoMap.Lookup((void *)llUrno,(void *& )prlFlight) == TRUE)
		{
			if(omRkeyMap.Lookup((void *)prlFlight->Rkey,(void *& )prlRkey) == TRUE)
			{
				if(prlRkey->Rotation.GetSize() > 1)
				{
					ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
					DeleteFlightInternal(llUrno,true);
					ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
				}
				else
				{
					if(prlRkey->Rotation.GetSize() == 1)
					{
						ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
						DeleteFlightInternal(llUrno, true);
					}
				}
			}
		}
/*
		ExtractItemList(CString(prlBcStruct->Data), &olStrArray);

		for(int ilLc = olStrArray.GetSize() - 1; ilLc >= 0; ilLc--)
		{
			if(omUrnoMap.Lookup((void *)atol(olStrArray[ilLc]),(void *& )prlFlight) == TRUE)
			{
				if(omRkeyMap.Lookup((void *)prlFlight->Rkey,(void *& )prlRkey) == TRUE)
				{
					if(prlRkey->Rotation.GetSize() > 1)
					{
						ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
						DeleteFlightInternal(atol(olStrArray[ilLc]),true);
						ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
					}
					else
					{
						if(prlRkey->Rotation.GetSize() == 1)
						{
							ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
							DeleteFlightInternal(atol(olStrArray[ilLc]), true);
						}
					}
				}
			}
		}
		*/
	}

}


bool DailyCedaFlightData::ProcessFlightISF(BcStruct *prlBcStruct)
{
	char buffer[256];
	bool blIgnoreView = false;
	CString olSkey;

	if(GetListItemByField(CString(prlBcStruct->Data), CString(prlBcStruct->Fields), CString("SKEY"), olSkey))
	{
		olSkey.TrimRight();

		if(!olSkey.IsEmpty() && olSkey != "0")
		{

			sprintf(buffer, " SKEY = %s", olSkey);
//			pogButtonList->SendMessage( BC_TO_REREAD, 0, 0);
			if(ReadFlights(buffer, false))
			{
				ogDdx.DataChanged((void *)this, DS_FLIGHT_UPDATE, NULL );
			}
			/*

			if((strcmp(prlBcStruct->Dest2, ogCommHandler.pcmReqId) == 0) && (atol(prlBcStruct->Tws) == lmBaseID))
			{
				blIgnoreView = true;
			}

			if(blIgnoreView || PreSelectionCheck( CString(prlBcStruct->Data), CString(prlBcStruct->Fields)))
			{
				sprintf(buffer, " SKEY = %s", olSkey);
				if(ReadFlights(buffer, blIgnoreView))
					ogDdx.DataChanged((void *)this, DS_FLIGHT_UPDATE, NULL );
			}
			*/
		}
	}	
	return true;
}


bool DailyCedaFlightData::ProcessFlightUFR(BcStruct *prlBcStruct)
{
	long llUrno;
	CStringArray olStrArray;
	int ilAnz;
	char buffer[256];
	bool blIgnoreView = false;
	bool blRead = false;
	DAILYFLIGHTDATA *prlFlight; 
	DAILYSCHEDULERKEYLIST *prlRkey;

	if(ilAnz = ExtractItemList(CString(prlBcStruct->Selection), &olStrArray) == 0) return false;

	for(int ilLc = olStrArray.GetSize() - 1; ilLc >= 0; ilLc--)
	{
		llUrno = atol(olStrArray[ilLc]);
		if(llUrno != -1)
		{

			if(omUrnoMap.Lookup((void *)llUrno,(void *& )prlFlight) == TRUE)
			{
				blIgnoreView = true;

				DeleteFromKeyMap(prlFlight);
				if(blRead = FillRecord((void *)prlFlight, CString(prlBcStruct->Fields), CString(prlBcStruct->Data)) )
				{
					AddToKeyMap(prlFlight);
					ogDdx.DataChanged((void *)this, DS_FLIGHT_CHANGE, (void *)prlFlight );
				}
				else
					AddToKeyMap(prlFlight);
			}
			if(!blRead && PreSelectionCheck( CString(prlBcStruct->Data), CString(prlBcStruct->Fields), true, omSelection))
			{
				if (strcmp(prlBcStruct->Cmd, "UPS") != 0 && strcmp(prlBcStruct->Cmd, "UPJ") != 0)
				{
					sprintf(buffer, " URNO = %ld", llUrno);
					ReadFlights(buffer, blIgnoreView);
					pogButtonList->SendMessage( BC_TO_REREAD, 0, 0);

					if(omUrnoMap.Lookup((void *)llUrno,(void *& )prlFlight) == TRUE)
					{
						if(blIgnoreView)
						{
							ogDdx.DataChanged((void *)this, DS_FLIGHT_CHANGE, (void *)prlFlight );
						}
						else
						{
							if(omRkeyMap.Lookup((void *)prlFlight->Rkey,(void *& )prlRkey) == TRUE)
							{
								ogDdx.DataChanged((void *)this, DS_FLIGHT_DELETE, (void *)prlRkey );
								ogDdx.DataChanged((void *)this, DS_FLIGHT_INSERT, (void *)prlRkey );
							}
						}
					}
				}
			}
		}
	}
	return true;
}


bool DailyCedaFlightData::PreSelectionCheck(CString &opData, CString &opFields, bool bpCheckOldNewVal, CString opSelection)
{
	
	// return false means DB-Read has no Sinn
	// return true means DB-Read has Sinn

	if ((egStructBcfields == EXT_CHANGED_OLDVAL || egStructBcfields == EXT_CHANGED_NEWVAL) && bpCheckOldNewVal)
		return NewPreSelectionCheck(opData, opFields, omFtyps, omFrom, omTo, opSelection);

	// old code for EXT_CHANGED_NO

	CTime olTifa;
	CTime olTifd;
	CString olFtyp;

	bool blFtyp = false;
	bool blTif = false;

	if(!GetListItemByField(opData, opFields, CString("TIFA"), olTifa))
		return true;

	if(!GetListItemByField(opData, opFields, CString("TIFD"), olTifd))
		return true;

	if(!GetListItemByField(opData, opFields, CString("FTYP"), olFtyp))
		return true;


	if((olTifd == TIMENULL) || 	(olTifa == TIMENULL)) 
		return true;


	if(olFtyp.GetLength() > 1)
	{
		for(int i = olFtyp.GetLength() - 1; i >= 0; i--)
		{
			if(omFtyps.Find(olFtyp[i]) >= 0)
			{
				blFtyp = true;
				break;
			}
		}
	}
	else
	{
		if(omFtyps.Find(olFtyp) >= 0)
			blFtyp = true;
	}

	
	if((olTifa <= omTo) && (olTifd >= omFrom))
 		blTif = true;

/*
	CString ol1 = omFrom.Format("%H:%M  %d.%m.%Y");
	CString ol2 = omTo.Format("%H:%M  %d.%m.%Y");
	CString ol3 = olTifa.Format("%H:%M  %d.%m.%Y");
	CString ol4 = olTifd.Format("%H:%M  %d.%m.%Y");
*/


	if(blFtyp && blTif)
		return true;
	else
		return false;
}


bool DailyCedaFlightData::JoinFlightPreCheck(long &ll1Urno, long &ll2Urno)
{
	bool blRet = true;

	if(ll1Urno == ll2Urno)
		return false;

	DAILYFLIGHTDATA *prlFlight1 = GetFlightByUrno(ll1Urno);
	DAILYFLIGHTDATA *prlFlight2 = GetFlightByUrno(ll2Urno);

	if((prlFlight1 == NULL) || (prlFlight2 == NULL))
		return false;

	if(prlFlight1->Rkey == prlFlight2->Rkey)
		return false;

	CCSPtrArray<DAILYFLIGHTDATA> olRotation;


	olRotation.Add(prlFlight1);
	olRotation.Add(prlFlight2);
	olRotation.Sort(CompareRotation);

	prlFlight1 = &olRotation[0];
	prlFlight2 = &olRotation[1];


	if((strcmp(prlFlight1->Des3, pcgHome) != 0) || (strcmp(prlFlight2->Org3, pcgHome) != 0))
 		blRet = false;

	
	if(prlFlight1->Stoa > prlFlight2->Stod)
		blRet = false;

	olRotation.RemoveAll();

	ll1Urno = prlFlight1->Urno;
	ll2Urno = prlFlight2->Urno;
	return blRet;

}



bool DailyCedaFlightData::UpdateFlight(DAILYFLIGHTDATA *prpFlight, DAILYFLIGHTDATA *prpFlightSave)
{
	CString olFieldList = CString(pcmFieldList);
	CString olListOfData;
	char pclFieldList[2048];
	char pclSelection[124];
	char pclData[3072];

	if((prpFlight == NULL) || (prpFlightSave == NULL))
		return false;

	if((prpFlight->Urno <= 0) || (prpFlightSave->Urno <= 0))
		return false;

	MakeCedaData(&omRecInfo,olListOfData, olFieldList, prpFlightSave, prpFlight);

	if(olFieldList.IsEmpty())
		return true;

	strcpy(pclFieldList, olFieldList);
	strcpy(pclData, olListOfData);
	sprintf(pclSelection,"%s%d","WHERE URNO = ", prpFlight->Urno);

    bool blret = CedaAction("UFR","", pclFieldList, pclSelection,"", pclData, "BUF1", false); 
	CFPMSApp::CheckCedaError(*this);
	return blret;
}


bool DailyCedaFlightData::SaveSpecial(char *pclSelection, char *pclField, char *pclValue)
{

	
	CString olFieldList(pclField);
	CString olListOfData(pclValue);

	bool blret = CedaAction("UFR",pcmTableName, pclField, pclSelection,"", pclValue, "BUF1", false); 
	CFPMSApp::CheckCedaError(*this);
	return blret;
}


DAILYFLIGHTDATA *DailyCedaFlightData::GetArrival(DAILYFLIGHTDATA *prpFlight)
{

	DAILYFLIGHTDATA *prlFlight;
	RKEYLIST *prlRkey;
	int ilCount;
	int i = 0;

	if(omRkeyMap.Lookup((void *)prpFlight->Rkey,(void *& )prlRkey) == TRUE)
	{
		ilCount = prlRkey->Rotation.GetSize();
		while(i < ilCount) 			
		{
			prlFlight = &prlRkey->Rotation[i];
			if (IsArrivalFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0])) 
				return prlFlight;
//			if(prlFlight->Urno == prpFlight->Urno)
//				break;
			i++;
		}

/*		for( i--; i >= 0; i--) 			
		{
			prlFlight = &prlRkey->Rotation[i];
			if((prlFlight->Urno != prpFlight->Urno) &&  
				(strcmp(prlFlight->Ftyp, "G") != 0) &&
				(strcmp(prlFlight->Ftyp, "T") != 0))
				return prlFlight;
		}*/
	}

	return NULL;

}

DAILYFLIGHTDATA *DailyCedaFlightData::GetDeparture(DAILYFLIGHTDATA *prpFlight)
{

	DAILYFLIGHTDATA *prlFlight;
	RKEYLIST *prlRkey;
	int ilCount;
	int i = 0;

	if(omRkeyMap.Lookup((void *)prpFlight->Rkey,(void *& )prlRkey) == TRUE)
	{
		ilCount = prlRkey->Rotation.GetSize();
		while(i < ilCount) 			
		{
			prlFlight = &prlRkey->Rotation[i];
			if (IsDepartureFlight(prlFlight->Org3, prlFlight->Des3, prlFlight->Ftyp[0])) 
				return prlFlight;
			i++;
		}
	}

	return NULL;
}

// delete rotation internaly
bool DailyCedaFlightData::DeleteFlightsInternal(long lpRkey, bool bpDdx /* = true */)
{

	RKEYLIST *prlRkeyMapRkey = NULL;
	if(omRkeyMap.Lookup((void *)lpRkey,(void *& )prlRkeyMapRkey) == TRUE)
	{
		for(int i = prlRkeyMapRkey->Rotation.GetSize() - 1; i >= 0; i--)
		{
			DeleteFlightInternal(prlRkeyMapRkey->Rotation[i].Urno, bpDdx);
		}
	}
	
	return true;
}



////////////////////////////////////////////////////////////////////////////

void ProcessDailyFlightCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_FLIGHT_CHANGE :
	case BC_FLIGHT_DELETE :
		((DailyCedaFlightData *)popInstance)->ProcessFlightBc(ipDDXType,
								vpDataPointer,ropInstanceName);
		break;
	case DIACCA_CHANGE :
			((DailyCedaFlightData *)popInstance)->ProcessCcaChange((long *)vpDataPointer);
			break;
	case BC_RAC_RELOAD :
		((DailyCedaFlightData *)popInstance)->ReloadFlights(vpDataPointer);
		break;
	}
}

void DailyCedaFlightData::ReloadFlights(void *vpDataPointer)
{
	BcStruct *prlBcStruct = (BcStruct *) vpDataPointer;

	if(prlBcStruct == NULL)
		return;

	CString olSelection = CString(prlBcStruct->Selection);
	CString olRkeys      = CString(prlBcStruct->Data);

	if(olRkeys.IsEmpty())
		return;

	CStringArray olStrArray;
	int ilSizeArray = SplitItemList(olSelection, &olStrArray, 1);
	CTime olReloadFrom = TIMENULL;
	CTime olReloadTo = TIMENULL;
	if (olStrArray.GetSize() > 2)
	{
		olReloadFrom = DBStringToDateTime(olStrArray.GetAt(1));
		olReloadTo = DBStringToDateTime(olStrArray.GetAt(2));
	}
	else
		return;

	if (olReloadFrom == TIMENULL || olReloadTo == TIMENULL)
		return;

	if (IsOverlapped(olReloadFrom, olReloadTo, omFrom, omTo))
	{
		CStringArray olRkeysLists;
//		CString olSelection;
		CStringArray olDelArray;

		int ilDelArray = SplitItemList(olRkeys, &olDelArray, 1);
		DAILYFLIGHTDATA *prlFlight = NULL;
		for (int l=0; l<olDelArray.GetSize(); l++)
		{
			long llRkey = atol(olDelArray.GetAt(l));
			DAILYSCHEDULERKEYLIST *prlRkey;
			if(omRkeyMap.Lookup((void *)llRkey,(void *& )prlRkey) == TRUE)
			{
				DeleteFlightsInternal(llRkey, true);
			}
		}


		for(int i = SplitItemList(olRkeys, &olRkeysLists, 250) - 1; i >= 0; i--)
		{
			char pclSelection[7000] = "";
			olSelection = CString(" RKEY IN (");
			olSelection += olRkeysLists[i] + CString(")");
			strcpy(pclSelection, olSelection);

			// reload new rotation
//			pogButtonList->SendMessage( BC_TO_REREAD, 0, 0);
			if(ReadFlights(pclSelection, false))
				ogDdx.DataChanged((void *)this, DS_FLIGHT_UPDATE, NULL );
		}
	}
}


void DailyCedaFlightData::ProcessCcaChange(long *lpUrno)
{

	DIACCADATA *prlCca = omCcaData.GetCcaByUrno(*lpUrno);

	if(prlCca != NULL)
	{
		DAILYFLIGHTDATA *prlFlight = GetFlightByUrno(prlCca->Flnu);
		if(prlFlight != NULL)
		{
			ogDdx.DataChanged((void *)this, DS_FLIGHT_CHANGE,(void *)prlFlight );
		}
	}
}


bool DailyCedaFlightData::DeleteFlight(CString opUrnoList)
{
	if(opUrnoList.IsEmpty())
		return false;

	char pclSelection[1024];
	CStringArray olStrArray;
	bool blRet = true;
	for(int i = ExtractItemList(opUrnoList, &olStrArray) - 1; i >= 0; i--)
	{
		sprintf(pclSelection,"WHERE URNO = %s",olStrArray[i]);
		if(!CedaAction("DFR","","", pclSelection,"", pcgDataBuf, "BUF1"))
			blRet = false;
		CFPMSApp::CheckCedaError(*this);
		CedaCcaData olCca;
		olCca.DeleteSpecial(olStrArray[i]);
	}


	return blRet;
}