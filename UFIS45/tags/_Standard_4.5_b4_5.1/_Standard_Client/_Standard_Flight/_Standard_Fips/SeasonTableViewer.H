#ifndef __SEASONTABLEVIEWER_H__
#define __SEASONTABLEVIEWER_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <SeasonCedaFlightData.h>
#include <CViewer.h>
#include <CCSPrint.h> // BWi
 
/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SEASONTABLE_LINEDATA1
{
	long AUrno;
	long ARkey;
	CString		AFtyp;
	CString		AFlti;
	CString		AFlno;
	CString		AJcnt;
	CTime		ATifa;
	CTime		AStoa;
	CTime		AStod;
	CTime		AB1ba;
	CTime		AB1ea;
	CString		ADooa;
	CString		ATtyp;
	CString		AStev;
	CString		ASte2;
	CString		ASte3;
	CString		ASte4;

	CString		AStyp;
	CString		AOrg3;
	CString		AVial;
	CString		APsta;
	CString		AGta1;
	CString		AGta2;
	CString		AAct3;
	CString		ABems;
	CString		ADes3;
	CString		ABlt1;
	CString		ARem1;
	CString		ARem2;

	long DUrno;
	long DRkey;
	CString		DFtyp;
	CString		DFlti;
	CString		DFlno;
	CString		DJcnt;
	CTime		DTifd;
	CTime		DStod;
	CString		DDooa;
	CString		DTtyp;
	CString		DStev;
	CString		DSte2;
	CString		DSte3;
	CString		DSte4;
	CString		DStyp;
	CString		DDes3;
	CString		DVial;
	CString		DPstd;
	CString		DGtd1;
	CString		DGtd2;
	CString		DBems;
	CString		DOrg3;
	CString		DCic;
	CString		Regn;
	CString		DRem1;
	CString		DRem2;


	SEASONTABLE_LINEDATA1(void)
	{ 
		AUrno = 0;
		DUrno = 0;
		ARkey = 0;
		DRkey = 0;
		Regn = "TEST";
		ATifa = TIMENULL;
		AStoa = TIMENULL;
		AStod = TIMENULL;
		DTifd = TIMENULL;
		DStod = TIMENULL;
		AB1ea = TIMENULL;
		AB1ba = TIMENULL;
	}
};



/////////////////////////////////////////////////////////////////////////////
// SeasonTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of SeasonTableViewer

//@Man:
//@Memo: SeasonTableViewer
//@See:  STDAFX, CCSCedaData, CedaSEASONFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class SeasonTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    SeasonTableViewer();
    //@ManMemo: Default destructor
    ~SeasonTableViewer();

    //@ManMemo: Attach
	void Attach(CCSTable *popAttachWnd);

	void SetParentDlg(CDialog* ppParentDlg);
    //void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);

	void SelectLine(long lpUrno);
	bool ShowFlight(long lpUrno);
	void DeselectAll();


    //@ManMemo: omLines
    CCSPtrArray<SEASONTABLE_LINEDATA1> omLines;
// Internal data processing routines
private:
	bool IsPassFilter(SEASONFLIGHTDATA *prpFlight);
	int  CompareFlight(SEASONTABLE_LINEDATA1 *prpFlight1, SEASONTABLE_LINEDATA1 *prpFlight2);

    void MakeLines(CCSPtrArray<SEASONFLIGHTDATA> *popFlights, bool bpInsert = false);
	int  MakeLine(SEASONFLIGHTDATA *prpAFlight, SEASONFLIGHTDATA *prpDFlight);
	void MakeLineData(SEASONFLIGHTDATA *prpAFlight, SEASONFLIGHTDATA *prpDFlight, SEASONTABLE_LINEDATA1 &rpLine);
	void MakeColList(SEASONTABLE_LINEDATA1 *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CreateLine(SEASONTABLE_LINEDATA1 &rpLine);

	int  FindLine(long lpUrno, int &rilLineno1, int &rilLineno2);
	bool FindLine(long lpUrno, int &rilLineno);
	
	void InsertDisplayLine( int ipLineNo);

	void UtcToLocal(SEASONTABLE_LINEDATA1 &rrpLine);
	bool CleanSteurzeichen (char *opChar, CString &opString);

public:
	void UpdateWindowTitle();		// 050302 MVy: generate and change the dialog title
private:
	CString GenerateTableName();		// 050224 MVy: create a string containing information about current table data
	void SetWindowTitle( CString sTitle );		// 050224 MVy: modifiy the window title by specified text

// Operations
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
 
// Window refreshing routines
public:
	void UpdateDisplay();

	void DrawHeader();
	void SetFilterString(CString opFilter);
	void SetOperatorString(CString opOperator);
	void SetStartEndTime(CTime opStart, CTime opEnd);

	
	void ProcessFlightChange(SEASONFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(RKEYLIST *prpFlight);
	void ProcessFlightUpdate(void);

	void ProcessFlightInsert(RKEYLIST  *prpRotation);

	void DeleteRotation(long lpRkey);
	bool CreateExcelFile(CString opTrenner);
	
	void SetTableSort(void);

	//Print
	void GetHeader(void); // BWi
	void PrintTableView(void); // BWi
	bool PrintTableHeader(void); // BWi
	bool PrintTableLine(SEASONTABLE_LINEDATA1 *prpLine,bool bpLastLine); // BWi
	CCSPtrArray <TABLE_HEADER_COLUMN> omPrintHeadHeaderArray; // BWi
	CCSPrint *pomPrint; //auch BWI


// Attributes used for filtering condition
private:
    CString omDate;
	bool bmIsFromSearch;
	CString omFilter;
	CString omOperator;
    CString omTableName;
    CString omFooterName;
    CString mySeparator;

	// Attributes
private:
	CCSTable *pomTable;
	CDialog* pomParentDlg;


// Methods which handle changes (from Data Distributor)
public:
    //@ManMemo: omStartTime
    CTime omStartTime;
    //@ManMemo: omEndTime
	CTime omEndTime;
    //@ManMemo: omDay
	CString omDay;

	CStringArray omSort;
    CString omFileName;

};

#endif //__SEASONTABLEVIEWER_H__
