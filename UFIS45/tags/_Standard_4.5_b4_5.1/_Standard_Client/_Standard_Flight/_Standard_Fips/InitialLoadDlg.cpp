/////////////////////////////////////////////////////////////////////////////
// CInitialLoadDlg dialog
#include <stdafx.h>
#include <InitialLoadDlg.h>
#include <CCSGlobl.h>
#include <Utils.h>


CInitialLoadDlg::CInitialLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInitialLoadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInitialLoadDlg)
	//}}AFX_DATA_INIT
	Create(IDD,pParent);
	ShowWindow(SW_SHOWNORMAL);

	m_Progress.SetRange(0,130);

}

CInitialLoadDlg::~CInitialLoadDlg(void)
{
	pogInitialLoad = NULL;
}

void CInitialLoadDlg::SetRange(int ipMax)
{
	m_Progress.SetRange(0, ipMax);
}


void CInitialLoadDlg::SetProgress(int ipProgress)
{
	m_Progress.OffsetPos(ipProgress);
}

void CInitialLoadDlg::SetMessage(CString opMessage)
{
	m_MsgList.AddString(opMessage);
	if ((m_MsgList.GetCount()-12) > 0)
		m_MsgList.SetTopIndex(m_MsgList.GetCount()-12);
	pogInitialLoad->UpdateWindow();
}

BOOL CInitialLoadDlg::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

void CInitialLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInitialLoadDlg)
	DDX_Control(pDX, IDC_MSGLIST, m_MsgList);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInitialLoadDlg, CDialog)
	//{{AFX_MSG_MAP(CInitialLoadDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitialLoadDlg message handlers
BOOL CInitialLoadDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect olRect;
	GetWindowRect(olRect);

	int left = (1024 - (olRect.right - olRect.left)) /2;
	int top =  (768  - (olRect.bottom - olRect.top)) /2;

	CString m_key = "DialogPosition\\Login";
	GetDialogFromReg(olRect, m_key);

	SetWindowPos(&wndTopMost,olRect.left, olRect.top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);
//	SetWindowPos(&wndTop,left, top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);

	SetWindowText(ogAppName + CString(" ") + GetString(IDS_STRING1206));
	m_MsgList.SetFocus();
	UpdateWindow();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

