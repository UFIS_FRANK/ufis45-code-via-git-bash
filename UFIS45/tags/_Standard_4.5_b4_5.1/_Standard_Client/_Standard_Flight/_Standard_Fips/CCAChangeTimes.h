#if !defined(AFX_CCACHANGETIMES_H__6EFF3651_C256_11D2_B015_00001C019205__INCLUDED_)
#define AFX_CCACHANGETIMES_H__6EFF3651_C256_11D2_B015_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CCAChangeTimes.h : header file
//

#include <CCSEdit.h>


/////////////////////////////////////////////////////////////////////////////
// CCAChangeTimes dialog

class CCAChangeTimes : public CDialog
{
// Construction
public:
	CCAChangeTimes(CWnd* pParent, const CString &ropCaption, bool bpLocalTimes, CTime opStart = TIMENULL, CTime opEnd = TIMENULL);   // standard constructor

	CTime omCkes;
	CTime omCkbs;

	CString omCaption;
	bool bmLocalTimes;


// Dialog Data
	//{{AFX_DATA(CCAChangeTimes)
	enum { IDD = IDD_CCA_CHANGE_TIMES };
	CCSEdit	m_CE_Ckes;
	CCSEdit	m_CE_Ckbs;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCAChangeTimes)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCAChangeTimes)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCACHANGETIMES_H__6EFF3651_C256_11D2_B015_00001C019205__INCLUDED_)
