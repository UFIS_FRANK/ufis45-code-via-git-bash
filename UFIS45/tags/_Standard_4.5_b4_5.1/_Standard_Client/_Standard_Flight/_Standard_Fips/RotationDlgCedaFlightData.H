#ifndef _RotationDlgCedaFlightData_H_
#define _RotationDlgCedaFlightData_H_
 
#include <BasicData.h>
#include <CCSDefines.h>
#include <CCSGlobl.h>



struct DAILYRKEYLIST ;	// >> DAILY Rotationen

struct JOINTS			// Verkn�pfungen 
{
	CTime Flugtag ;		// yyyymmddhhmmss
	long  RKey ;		// verkn�pfter Turnaround
	long  RefUrno ;		// Referenz Urno in omRangeMap

	JOINTS() { Flugtag=TIMENULL ; RKey=0L ; RefUrno=0L ; }
} ;

struct DAILYRANGE		// enth�lt Zeitrauminfos f�r Seasondarstellung des Daily
{
	CString Range ;					// von - bis: (dd.mm.yyyy-dd.mm.yyyy)
	CCSPtrArray<JOINTS> Joint ;		// EinzelFlug Infos

	DAILYRANGE() { Range.Empty() ; }
	~DAILYRANGE() { ; }
} ;
 

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: ROTATIONDLGFLIGHTDATAStruct
//@See: RotationDlgCedaFlightData
/*@Doc:
  A structure for reading flight data. We read all data from database (or get 
  the data from AFTLSG) into this struct and store the data in omData.

*/

 
struct ROTATIONDLGFLIGHTDATA 
{
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	CTime 	 Airb; 	// Startzeit (Beste Zeit)
	CTime 	 Airu; 	// Startzeit Anwender
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)
	CTime 	 B1ba; 	// Belegung Gep�ckband 1 aktueller Beginn
	CTime 	 B1ea; 	// Belegung Gep�ckband 1 aktuelles Ende
	CTime 	 B2ba; 	// Belegung Gep�ckband 2 aktueller Beginn
	CTime 	 B2ea; 	// Belegung Gep�ckband 2 aktuelles Ende
	char 	 Bagn[7]; 	// Anzahl Gep�ckst�cke
	char 	 Blt1[7]; 	// Gep�ckband 1
	char 	 Blt2[7]; 	// Gep�ckband 2
	CTime 	 Cdat; 	// Erstellungsdatum
	char 	 Cgot[7]; 	// Fracht in Tonnen
	char 	 Ckif[7]; 	// Check-In From
	char 	 Ckit[7]; 	// Check-In To
	char 	 Csgn[10]; 	// Call- Sign
	CTime 	 Ctot; 	// Slot-Zeit per Telex
	char 	 Dcd1[4]; 	// Versp�tungscode Abflug 1
	char 	 Dcd2[4]; 	// Versp�tungscode Abflug 2
	char 	 Des3[5]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	char 	 Divr[16]; 	// Bemerkung f�r "Diverted"
	char 	 Dooa[3]; 	// Flugtag der Ankunft
	char 	 Dood[3]; 	// Flugtag des Abflugs
	char	 Dssf[129];  // Data Source Status Flag
	char 	 Dtd1[6]; 	// Versp�tungszeit Abflug 1
	char 	 Dtd2[6]; 	// Versp�tungszeit Abflug 2
	CTime 	 Etac; 	// ETA-Confirmed
	CTime 	 Etai; 	// ETA-Intern (Beste Zeit)
	CTime 	 Etau; 	// ETA- Anwender
	CTime 	 Etdc; 	// ETD-Confirmed
	CTime 	 Etdi; 	// ETD-Intern (Beste Zeit)
	CTime 	 Etdu; 	// ETD-Anwender
	CTime 	 Etoa; 	// ETA-Puplikumsanzeige
	CTime 	 Etod; 	// ETD-Publikumsanzeige
	char 	 Ext1[7]; 	// Ausgang 1 Ankunft
	char 	 Ext2[7]; 	// Ausgang 2 Ankunft
	CTime 	 Fdat; 	// Fluko-Datum
	char	 Flda[9];	// Date of departure
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Flns[3]; 	// Suffix
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	CTime 	 Ga1x; 	// Belegung Gate 1 Ankunft aktueller Beginn
	CTime 	 Ga1y; 	// Belegung Gate 1 Ankunft aktuelles Ende
	CTime 	 Ga2x; 	// Belegung Gate 2 Ankunft aktueller Beginn
	CTime 	 Ga2y; 	// Belegung Gate 2 Ankunft aktuelles Ende
	CTime 	 Gd1x; 	// Belegung Gate 1 Abflug aktueller Beginn
	CTime 	 Gd1y; 	// Belegung Gate 1 Abflug aktuelles Ende
	CTime 	 Gd2x; 	// Belegung Gate 2 Abflug aktueller Beginn
	CTime 	 Gd2y; 	// Belegung Gate 2 Abflug aktuelles Ende
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gta2[7]; 	// Gate 2 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	char 	 Gtd2[7]; 	// Doppelgate Abflug
	char 	 Htyp[4]; 	// Abfertigungsart
	char 	 Ifra[3]; 	// Anflugverfahren
	char 	 Ifrd[3]; 	// Abflugverfahren
	CTime 	 Iskd; 	// Informative Planzeit
	char 	 Isre[3]; 	// Bemerkungskennzeichen intern
	char 	 Jcnt[3]; 	// Anzahl verbundene Flugnummern
	char 	 Jfno[112]; 	// Verbundene Flugnummern (max. 10)
	CTime 	 Land; 	// Landezeit (Beste Zeit)
	CTime 	 Lndu; 	// LANDE-Zeit durch Anwendereingabe
	CTime 	 Lstu; 	// Datum letzte �nderung
	char 	 Mail[7]; 	// Post in St�ck
	char 	 Ming[6]; 	// Mindestbodenzeit
	char 	 Nose[5]; 	// Mindestbodenzeit
	CTime 	 Nxti; 	// N�chste Info
	CTime 	 Ofbl; 	// Offblock- Zeit (Beste Zeit)
	CTime 	 Ofbu; 	// OFBL-Anwender
	CTime 	 Onbe; 	// Est. Onblock-Zeit
	CTime 	 Onbl; 	// Onblock-Zeit (Beste Zeit)
	CTime 	 Onbu; 	// ONBL-Anwender
	char 	 Org3[5]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	CTime 	 Paba; 	// Belegung Position Ankunft aktueller Beginn
	CTime 	 Paea; 	// Belegung Position Ankunft aenvyktuelles Ende
	char 	 Paid[3]; 	// Barzahler Kennzeichen
	char 	 Pax1[5]; 	// Anzahl Passagiere 1. Klasse
	char 	 Pax2[5]; 	// Anzahl Passagiere 2. Klasse
	char 	 Pax3[5]; 	// Anzahl Passagiere 3. Klasse
	char 	 Paxf[5]; 	// Anzahl Passagiere 3. Klasse
	char 	 Paxt[5]; 	// Anzahl Passagiere 3. Klasse
	char 	 Paxi[5]; 	// Anzahl Transit Passagiere
	CTime 	 Pdba; 	// Belegung Position Abflug aktueller Beginn
	CTime 	 Pdea; 	// Belegung Position Abflug aktuelles Ende
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Raco[7]; 	// Luftfahrzeug im Radar
	char 	 Regn[14]; 	// LFZ-Kennzeichen
	char 	 Rem1[258]; 	// Bemerkung zum Flug
	char 	 Rem2[258]; 	// Zusatz-Bemerkung zum Flug
	char 	 Dela[258]; 	// Zusatz-Bemerkung zum Flug
	char 	 Remp[6]; 	// Public Remark FIDS
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rtyp[3]; 	// Verkettungstyp (Art/Quelle der Verkettung)
	char 	 Rwya[6]; 	// Landebahn
	char 	 Rwyd[6]; 	// Startbahn
	char 	 Seas[8]; 	// Saisonname
	long 	 Skey; 	// Eindeutiger Flugschl�ssel (Saisonschl.)
	CTime 	 Slot; 	// aktuelle Slotzeit
	CTime 	 Slou; 	// Slotzeit Anwender
	char 	 Ssrc[6]; 	// SSR-Code
	char 	 Stab[3]; 	// Datenstatus AIRB
	char 	 Stat[12]; 	// Status
	char 	 Stea[3]; 	// Datenstatus ETAI
	char 	 Sted[3]; 	// Datenstatus ETDI
	char 	 Stev[5]; 	// Statistikkennung
	char 	 Ste2[4]; 	// Statistikkennung
	char 	 Ste3[4]; 	// Statistikkennung
	char 	 Ste4[4]; 	// Statistikkennung
	char 	 Stht[3]; 	// Datenstatus HTYP
	char 	 Stld[3]; 	// Datenstatus LAND
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	char 	 Stof[3]; 	// Datenstatus OFBL
	char 	 Ston[3]; 	// Datenstatus ONBL
	char 	 Stsl[3]; 	// Datenstatus SLOT
	char 	 Sttm[3]; 	// Datenstatus TMOA
	char 	 Sttt[3]; 	// Datenstatus TTYP
	char 	 Styp[4]; 	// Service Typ (Fluko)
	char 	 Tet1[3]; 	// Terminal Ausgang 1 Ankunft
	char 	 Tet2[3]; 	// Terminal Ausgang 2 Ankunft
	char 	 Tga1[3]; 	// Terminal Gate 1 Ankunft
	char 	 Tga2[3]; 	// Terminal Gate 2 Ankunft
	char 	 Tgd1[3]; 	// Terminal Gate 1 Abflug
	char 	 Tgd2[3]; 	// Terminal Doppelgate Abflug
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	char 	 Tisa[3]; 	// Statusdaten f�r TIFA
	char 	 Tisd[3]; 	// Statusdaten f�r TISD
	CTime 	 Tmau; 	// TMO-Anwender
	char 	 Tmb1[3]; 	// Terminal Gep�ckband 1
	char 	 Tmb2[3]; 	// Terminal Gep�ckband 2
	CTime 	 Tmoa; 	// TMO (Beste Zeit)
	char 	 Ttyp[7]; 	// Verkehrsart
	char 	 Twr1[3]; 	// Warteraum Terminal
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Vers[22]; 	// Flugplanversion
	char 	 Vial[1026]; 	// Liste der Zwischenstationen/-zeiten
	char 	 Vian[4]; 	// Anzahl der Zwischenstationen - 1
	CTime 	 W1ba; 	// Belegung Warteraum 1 aktueller Beginn
	CTime 	 W1ea; 	// Belegung Warteraum 1 Aktuelles Ende
	char 	 Wro1[7]; 	// Warteraum
	char 	 Via3[4]; 	// Letzte Station vor Ankunft - Abflug
	char 	 Via4[5]; 	// Letzte Station vor Ankunft - Abflug
	char 	 Adid[2]; 	// 
	char 	 Gd2d[2]; 	// 
	char 	 Rgd1[6]; 	// Separate Gate FIDS Remark
	char 	 Rgd2[6]; 	// Separate Gate FIDS Remark
	char 	 Ckf2[6];	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon) 	
	char 	 Ckt2[6]; 	// CKI second set (Terminal 2 CKI for mixed flights in Lisbon)

	char 	 Cht3[3+1]; 	// Veranstalter 
	char 	 Cxxr[6]; 	
	char	 Pcom[201];//comment for payment Added by Christine
	int      Vcnt;//Vip Count 
	
	
	CTime    Chon;//Chocks On
	CTime    Chof;//Chocks Of
	CTime    Chou;//Chocks On by user
	CTime    Chfu;//Chocks Of by user
	CTime    Cho1;//Chocks On 1
	CTime    Cho2;//Chocks On 2
	CTime    Chf1;//Chocks Off 1
	CTime    Chf2;//Chocks Off 2

	char 	 Stcn[3];//Status of Chocks on 
	char 	 Stcf[3];//Status of Chocks off

	CTime	Tmaa;
	CTime	Aira;
	CTime	Aird;
	CTime	Lnda;
	CTime	Lndd;
	CTime	Onbs;
	CTime	Onbd;
	CTime	Ofbs;
	CTime	Ofbd;
	CTime	Etde;
	CTime	Etae;
	CTime	 Nfes;


	CTime	Bbaa;
	CTime	Bbfa;

	char 	 Chgi[20+1]; 	// Veranstalter 
	char 	 Baz1[7]; 	
	char 	 Baz4[7]; 	
	CTime	 Bao1;
	CTime	 Bac1;
	CTime	 Bao4;
	CTime	 Bac4;
	char 	 Flti[3]; 	// Veranstalter 
	CTime	 Etaa;
	CTime	 Etda;
	char 	 Bagw[8]; 	// 

	CTime 	Pabs; 	// Belegung Position Ankunft aktueller Beginn
	CTime 	Paes; 	// Belegung Position Ankunft aktuelles Ende
	CTime 	Pdbs; 	// Belegung Position Abflug aktueller Beginn
	CTime 	Pdes; 	// Belegung Position Abflug aktuelles Ende
	CTime	B1bs;	
	CTime	B1es;	
	CTime	B2bs;	
	CTime	B2es;	
	CTime	Ga1b;	
	CTime	Ga1e;	
	CTime	Ga2b;	
	CTime	Ga2e;	
	CTime	Gd1b;	
	CTime	Gd1e;	
	CTime	Gd2b;	
	CTime	Gd2e;	
	CTime	W1bs;	
	CTime	W1es;	

	char 	 Wro2[7]; 	// Warteraum
	CTime	W2bs;	
	CTime	W2es;	
	CTime 	 W2ba; 	// Belegung Warteraum 1 aktueller Beginn
	CTime 	 W2ea; 	// Belegung Warteraum 1 Aktuelles Ende
	char 	 Baa1[5]; 	// FlightReports
	char 	 Baa2[5]; 	// SpecialReq(Read)
	char 	 Baa3[5]; 	// SpecialReq(UnRead)

	CTime	Tobt;	
	CTime	Tsat;	
	char	Tobf[3];	

	CTime	Bas1;	
	CTime	Bae1;	
	CTime	Bas2;	
	CTime	Bae2;	
	char 	Baz6[7]; 	
	char	 Vipa[2];
	char	 Vipd[2];
	char	 Fpsa[2];
	char	 Fpsd[2];

	//char 	 Fpsa[3];	//Flag: Fixed position for arrival		
	char 	 Fga1[2];	//Flag: Fixed gate 1 for arrival
	char 	 Fga2[2];	//Flag: Fixed gate 2 for arrival
	char 	 Fbl1[2];	//Flag: Fixed baggage belt 1
	char 	 Fbl2[2];	//Flag: Fixed baggage belt 2
	//char 	 Fpsd[3];	//Flag: Fixed position for departure
	char 	 Fgd1[2];	//Flag: Fixed gate 1 for departure
	char 	 Fgd2[2];	//Flag: Fixed gate 2 for departure
	char     Rtow[2];   //Flag: Ready for Towing
	
	char 	 Faog[2];	//Flag: AOG
	char     Adho[2];   //Flag: Adhoc

	char     Coby[101];   //Collected By
	char     Hdag[101];   //Handling Agent
	char     Blto[201];   //Bill To
	char     Prmk[201];   //Payment Remark
	char	 Mopa[2];     //For new Mode of Payment implementation			


	// DataCreated by this class
	bool      IsChanged;


	void ClearAPos(void);
	void ClearDPos(void);
	void ClearAGat(int ilNum);
	void ClearDGat(int ilNum);
	void ClearBlt(int ilNum);
	void ClearWro(void);

	bool CheckBltOpeningTime(int ipBltNo, CString &ropText) const;
	bool CheckBltClosingTime(int ipBltNo, CString &ropText) const;

	bool CheckDepBltOpeningTime(int ipBltNo, CString &ropText) const;
	bool CheckDepBltClosingTime(int ipBltNo, CString &ropText) const;

	bool IsDepGateOpen(int ipGateNo) const;
	bool UpdDepGateFIDSMonitor(int ipGateNo) const;


	ROTATIONDLGFLIGHTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = true;
		Bas1 = -1;
		Bas2 = -1;
		Bae1 = -1;
		Bae2 = -1;
		Tobt = -1;
		Tsat = -1;
		Nfes = -1;
		Airb = -1;
		Airu = -1;
		B1ba = -1;
		B1ea = -1;
		B2ba = -1;
		B2ea = -1;
		Cdat = -1;
		Ctot = -1;
		Etac = -1;
		Etai = -1;
		Etau = -1;
		Etdc = -1;
		Etdi = -1;
		Etdu = -1;
		Etoa = -1;
		Etod = -1;
		Fdat = -1;
		Ga1x = -1;
		Ga1y = -1;
		Ga2x = -1;
		Ga2y = -1;
		Gd1x = -1;
		Gd1y = -1;
		Gd2x = -1;
		Gd2y = -1;
		Iskd = -1;
		Land = -1;
		Lndu = -1;
		Lstu = -1;
		Nxti = -1;
		Ofbl = -1;
		Ofbu = -1;
		Onbe = -1;
		Onbl = -1;
		Onbu = -1;
		Paba = -1;
		Paea = -1;
		Pdba = -1;
		Pdea = -1;
		Slot = -1;
		Slou = -1;
		Stoa = -1;
		Stod = -1;
		Tifa = -1;
		Tifd = -1;
		Tmau = -1;
		Tmoa = -1;
		W1ba = -1;
		W1ea = -1;
		Bbaa = -1;
		Bbfa = -1;
		Bao1 = -1;
		Bac1 = -1;
		Bao4 = -1;
		Bac4 = -1;
		W2ba = -1;
		W2ea = -1;
		Chon = -1;
		Chof = -1;
		Chou = -1;
		Chfu = -1;
		Cho1 = -1;
		Cho2 = -1;
		Chf1 = -1;
		Chf2 = -1;
		Vcnt = 0;
		strcpy(Fpsa,"");
		strcpy(Fpsd,"");
		strcpy(Fga1,"");
		strcpy(Fga2,"");
		strcpy(Fpsd,"");
		strcpy(Fgd1,"");
		strcpy(Fgd2,"");
		strcpy(Fbl1,"");
		strcpy(Fbl2,"");
	}

	ROTATIONDLGFLIGHTDATA(const SEASONDLGFLIGHTDATA &rrpFlight);


}; // end ROTATIONDLGFLIGHTDATA
	

struct FLIGHTCHANGE
{
	ROTATIONDLGFLIGHTDATA Flight1;
	ROTATIONDLGFLIGHTDATA Flight2;
};

/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf RotationDlgCedaFlightData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class RotationDlgCedaFlightData: public CCSCedaData
{
public:

	// Attributes
	enum { NoSort, FlightSort, RotationSort, DefSort = 2 } ;	// regelt Sortierung in ReadSpecial() 

    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omUrnoMap;

	CMapPtrToPtr omRkeyMap;	// DAILY
    CMapPtrToPtr omRangeMap;
	bool omDailySeason ;

	long lmRkey;
	long lmCalledBy;
	bool bmWithGround;
	bool bmIsFieldListInit;


   //@ManMemo: ROTATIONDLGFLIGHTDATA records read by ReadAllFlight().
    CCSPtrArray<ROTATIONDLGFLIGHTDATA> omData;
	FLIGHTCHANGE rmFlights;

	char pcmAftFieldList[2096];

	CString omFtyps;
	CTime omFrom;
	CTime omTo;


// Operations
public:

    RotationDlgCedaFlightData( bool bpAFTonly = true );	// false => inkl. Archivtabelle 
	~RotationDlgCedaFlightData();

	void SetFieldList(void);

	void ClearAll(void);

	void UnRegister();
	void Register();
	

    // internal data access.
	bool AddFlightInternal(ROTATIONDLGFLIGHTDATA *prpFlight);
	bool DeleteFlightInternal(long lpUrno);

	bool ReadFlights( char *pcpSelection);
	bool ReadDailyFlights( const char *pcpSelection );

	bool ReadSpecial( CCSPtrArray<ROTATIONDLGFLIGHTDATA> *popFlights, const char *pcpSelection, const char *pcpFields, bool bpOnlyAFT = false, bool bpSort = true , int ilSortKind = DefSort );

	bool ReadRotation(long lpRkey = -1, long lpCalledBy = -1, bool bpWithGround = false);

	void SetPreSelection(CTime &opFrom, CTime &opTo, CString &opFtyps);
	bool PreSelectionCheck(CString &opData, CString &opFields);

	bool AddSeasonFlights(CTime opFromdate, CTime opToDate, CString opADaySel, CString opDDaySel, CString ipFreq, CTime opARefDat, CTime opDRefDat, ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, CCSPtrArray<CCADATA> &opCca );

	bool UpdateFlight(ROTATIONDLGFLIGHTDATA *prpFlight, ROTATIONDLGFLIGHTDATA *prpFlightSave);
	bool UpdateFlight(long lpUrno, char *pcpFieldList, char *pcpDataList);
	
	
	bool SearchFlight(const CString &ropAlc, const CString &ropFltn, const CString &ropFlns, const CTime &ropDate, char clAdid, ROTATIONDLGFLIGHTDATA &rpFlight);
	bool SearchFlightToJoin(const CString &ropAlc, const CString &ropFltn, const CString &ropFlns, const CTime &ropDateFrom, const CTime &ropDateTo, char clAdid, ROTATIONDLGFLIGHTDATA &rpFlight, CString opStev = "");

	void InitFieldList();

	long GetRkey(long lpUrno);

    //@ManMemo: Handle Broadcasts for flights.
	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightUPS(BcStruct *prlBcStruct);
	bool ProcessFlightDFR(BcStruct *prlBcStruct);
	bool WakeUpFlight(CString opCmd, long lpUrno);
	
	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);
	
	ROTATIONDLGFLIGHTDATA *GetArrival(ROTATIONDLGFLIGHTDATA *prpFlight);
	ROTATIONDLGFLIGHTDATA *GetDeparture(ROTATIONDLGFLIGHTDATA *prpFlight);
	ROTATIONDLGFLIGHTDATA *GetJoinFlight(ROTATIONDLGFLIGHTDATA *prpFlight, CString opAid);
	
	ROTATIONDLGFLIGHTDATA *GetRKeyArrival(ROTATIONDLGFLIGHTDATA *prpFlight) ;

	int GetJfnoArray(CCSPtrArray<JFNODATA> *opJfno, ROTATIONDLGFLIGHTDATA *prpFlight);
	int GetViaArray(CCSPtrArray<VIADATA> *opVias, ROTATIONDLGFLIGHTDATA *prpFlight);
	bool AddToKeyMap(ROTATIONDLGFLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(ROTATIONDLGFLIGHTDATA *prpFlight );
	ROTATIONDLGFLIGHTDATA *GetFlightByUrno(long lpUrno); 

	// Zeitr�ume f�r Saisondarstellung DAILY
	void SetDailySeason( bool bpFlag = true ) { omDailySeason = bpFlag ; } ;
	bool AddToRangeMap(ROTATIONDLGFLIGHTDATA *prpFlight, ROTATIONDLGFLIGHTDATA*& prpJointFlight );
	void GetFlightDataRange( CString *ropRange, ROTATIONDLGFLIGHTDATA *prpFlight, ROTATIONDLGFLIGHTDATA *prpCmpFlight ) ;
	bool SetFlightDataRange( DAILYRANGE *prpRange, ROTATIONDLGFLIGHTDATA *prpFlight ) ;
	CString *GetRangeByUrno(long lpUrno, CString &ropRange, bool bpShort = false, long ipRef=0L, bool bpCalcLocal=false ) ;
	bool GetFreqString( CCSPtrArray<JOINTS> &ropPArray, CString &ropFreqStr, bool bpShort=true, bool bpSort=false, long ipRef=0L, bool bpCalcLocal=false ) ;

	ROTATIONDLGFLIGHTDATA *GetLastTowInRot(long lpUrno, long lpRkey);
	ROTATIONDLGFLIGHTDATA *GetFirstTowInRot(long lpUrno, long lpRkey);

	CString CheckDupFlights(CTime opFromdate, CTime opToDate, CString opADaySel, CString opDDaySel, CString ipFreq, CTime opARefDat, CTime opDRefDat, ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight, CCSPtrArray<CCADATA> &opCca);

	// nur Test
	void GetPartner( long &Urno, CCSPtrArray<JOINTS> &ropPArray ) ;

	void ReadRotationRAC(void *vpDataPointer);
	void ReloadCcaSBC(void *vpDataPointer);

};


struct DAILYRKEYLIST
{
	CCSPtrArray<ROTATIONDLGFLIGHTDATA> Rotation;

};


#endif
