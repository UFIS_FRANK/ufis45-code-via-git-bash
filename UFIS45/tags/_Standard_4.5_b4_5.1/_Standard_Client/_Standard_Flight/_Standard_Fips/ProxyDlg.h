// DlgProxy.h : header file
//

#if !defined(AFX_DLGPROXY_H__FC3BA6FB_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
#define AFX_DLGPROXY_H__FC3BA6FB_1AED_11D1_82C3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CButtonListDlg;

/////////////////////////////////////////////////////////////////////////////
// CAutoProxyDlg command target

class CAutoProxyDlg : public CCmdTarget
{
	DECLARE_DYNCREATE(CAutoProxyDlg)

	CAutoProxyDlg();           // protected constructor used by dynamic creation

// Attributes
public:
	CButtonListDlg* m_pDialog;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutoProxyDlg)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CAutoProxyDlg();

	// Generated message map functions
	//{{AFX_MSG(CAutoProxyDlg)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CAutoProxyDlg)

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CAutoProxyDlg)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPROXY_H__FC3BA6FB_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
