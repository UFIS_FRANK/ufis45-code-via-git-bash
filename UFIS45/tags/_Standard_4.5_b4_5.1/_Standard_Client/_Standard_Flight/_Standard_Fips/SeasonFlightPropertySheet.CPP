// flplanps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <FPMS.h>
#include <CCSGlobl.h>
#include <CCSCedadata.h>
//#include "calocd.h"
//#include "cjobd.h"
//#include "cdemandd.h"
//#include "cjobdemandd.h"
//#include "cempd.h"
//#include "cshiftd.h"

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
//#include "StrConst.h"
//#include "FilterPg.h"
//#include "FlPlanSo.h"
//#include "FlPlanBo.h"
//#include "ZeitPage.h"
//#include "BaseProp.h"
//#include "FlPlanPS.h"
#include <SeasonFlightPropertySheet.h>
#include <StringConst.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CStringArray ogFlightAlcd;

/////////////////////////////////////////////////////////////////////////////
// SeasonFlightTablePropertySheet
//





SeasonFlightTablePropertySheet::SeasonFlightTablePropertySheet(CString opCalledFrom, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage, LPCSTR pszCaption) : 
	BasePropertySheet(ID_SHEET_FLIGHT_SEASON_TABLE, pParentWnd, popViewer, iSelectPage),
	m_SearchFlightPage(bgSeasonLocal)
{
	SetTitle(pszCaption);
	AddPage(&m_SearchFlightPage);
	m_SearchFlightPage.SetCalledFrom(opCalledFrom);
	AddPage(&m_PSUniFilter);
	m_PSUniFilter.SetCalledFrom(opCalledFrom);
	//AddPage(&m_SpecialFilterPage);
	AddPage(&m_PSUniSortPage);
	m_PSUniSortPage.SetCalledFrom(opCalledFrom);
	AddPage(&m_SpecialSortPage);
	//m_PSUniFilter.SetCaption("Allgem. Filter");

}

SeasonFlightTablePropertySheet::~SeasonFlightTablePropertySheet()
{
}




void SeasonFlightTablePropertySheet::LoadDataFromViewer()
{
	//pomViewer->GetFilter("SPECFILTER", m_SpecialFilterPage.omValues);
	pomViewer->GetFilter("UNIFILTER", m_PSUniFilter.omValues);
	pomViewer->GetSort("FLIGHTSORT", m_SpecialSortPage.omSortOrders);
	pomViewer->GetSort("UNISORT", m_PSUniSortPage.omValues);
	pomViewer->GetSearch("FLIGHTSEARCH", m_SearchFlightPage.omValues);
}

void SeasonFlightTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	//m_PSUniFilter.UpdateData();
	//m_SpecialFilterPage.UpdateData();
	//SearchFlightPage.UpdateData();
	//m_PSUniSortPage.UpdateData();
	//m_SpecialSortPage.UpdateData();

	//MWO
	//pomViewer->SetFilter("SPECFILTER", m_SpecialFilterPage.omValues);
	pomViewer->SetFilter("UNIFILTER", m_PSUniFilter.omValues);
	pomViewer->SetSort("FLIGHTSORT", m_SpecialSortPage.omSortOrders);
	pomViewer->SetSort("UNISORT", m_PSUniSortPage.omValues);
	pomViewer->SetSearch("FLIGHTSEARCH", m_SearchFlightPage.omValues);

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}

}

int SeasonFlightTablePropertySheet::QueryForDiscardChanges()
{
/*	CStringArray olSelectedAirlines;
	CStringArray olSelectedArrivals;
	CStringArray olSelectedDepartures;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Airline", olSelectedAirlines);
	pomViewer->GetFilter("Gate Arrival", olSelectedArrivals);
	pomViewer->GetFilter("Gate Departure", olSelectedDepartures);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();
	pomViewer->GetFilter("Zeit", olSelectedTime);

	CStringArray olInbound, olOutbound;
	pomViewer->GetFilter("Inbound", olInbound);
	pomViewer->GetFilter("Outbound", olOutbound);

	if (!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
		!IsIdentical(olSelectedArrivals, m_pageArrival.omSelectedItems) ||
		!IsIdentical(olSelectedDepartures, m_pageDeparture.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0') ||
		!IsIdentical(olSelectedTime, m_pageZeit.omFilterValues) ||
		olInbound[0] != CString(m_pageBound.m_Inbound + '0') ||
		olOutbound[0] != CString(m_pageBound.m_Outbound + '0'))
		return MessageBox(ID_MESSAGE_LOST_CHANGES, NULL, MB_ICONQUESTION | MB_OKCANCEL);
*/
	return IDOK;
}
