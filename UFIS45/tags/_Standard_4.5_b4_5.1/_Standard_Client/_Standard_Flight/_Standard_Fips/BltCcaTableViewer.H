#ifndef __BltCcaTableViewer_H__
#define __BltCcaTableViewer_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <DiaCedaFlightData.h>
#include <CViewer.h>
#include <CCSPrint.h>


/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct BLTCCATABLE_LINEDATA
{
	long FUrno;
	long Urno;
	CString		Counter;
	CString		Flno;
	CTime		Stod;
	CString		FidsRemark;
	CTime		Open;
	CTime		Close;
  
	BLTCCATABLE_LINEDATA(void)
	{ 
		Urno = 0;
		FUrno = 0;
		Counter.Empty();
		Flno.Empty();
		Stod = TIMENULL;
		FidsRemark.Empty();
		Open = TIMENULL;
		Close = TIMENULL; 
	}
};



/////////////////////////////////////////////////////////////////////////////
// BltCcaTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of BltCcaTableViewer

//@Man:
//@Memo: BltCcaTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/

#define BLTCCATABLE_COLCOUNT 6

class BltCcaTableViewer : public CViewer
{

public:
    //@ManMemo: Default constructor
    BltCcaTableViewer(BltCcaTableDlg *popParentDlg);
    //@ManMemo: Default destructor
    ~BltCcaTableViewer();
	
	void ProcessCcaChange(long lpCcaUrno);
	bool ProcessCcaDelete(long lpCcaUrno);

	//void ProcessFlightInsert(DiaCedaFlightData::RKEYLIST  *prpRotation);

	void UnRegister();

  	void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(DiaCedaFlightData *popFlightData);

	// Print table to paper
	void PrintTableView(void);
	// Print table to file
	bool PrintPlanToFile(CString opFilePath) const;


// Internal data processing routines
private:
	const int imOrientation;

	static int imTableColCharWidths[BLTCCATABLE_COLCOUNT];
	CString omTableHeadlines[BLTCCATABLE_COLCOUNT];
	int imTableColWidths[BLTCCATABLE_COLCOUNT];
	int imPrintColWidths[BLTCCATABLE_COLCOUNT];

	// Table fonts and widths
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont *pomPrintHeaderFont; 
	CFont *pomPrintLinesFont;
	float fmPrintHeaderFontWidth; 
	float fmPrintLinesFontWidth;

 	void DeleteAll();
 	void DeleteLine(int ipLineno);
 
  	void UpdateDisplay();

	void DrawHeader();

 	int CompareLines(const BLTCCATABLE_LINEDATA &rrpLine1, const BLTCCATABLE_LINEDATA &rrpLine2) const;

    void MakeLines(void);

	int  MakeLine(const DIACCADATA &rrpCca);
	void MakeLineData(const DIACCADATA &rrpCca, BLTCCATABLE_LINEDATA &rrpLine);
	void MakeColList(const BLTCCATABLE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const; 
	int  CreateLine(const BLTCCATABLE_LINEDATA &rrpLine);

	void InsertCca(const DIACCADATA &rrpCca);

	bool FindLine(long lpUrno, int &ripLineno) const;
	
	void InsertDisplayLine(int ipLineNo);

	bool PrintTableHeader(CCSPrint &ropPrint) const;
 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo) const;
	
	bool UtcToLocal(BLTCCATABLE_LINEDATA &rrpLine);

	bool IsPassFilter(const DIACCADATA &rrpData) const;

    CCSPtrArray<BLTCCATABLE_LINEDATA> omLines;

	CCSTable *pomTable;

	DiaCedaFlightData *pomFlightData;

	BltCcaTableDlg *pomParentDlg;
	
};

#endif //__BltCcaTableViewer_H__
