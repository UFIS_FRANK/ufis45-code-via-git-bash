// UndoDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <UndoDlg.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <DiaCedaFlightData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void UndoCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// UndoDlg dialog


UndoDlg::UndoDlg(CWnd* pParent )
	: CDialog(UndoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(UndoDlg)
	//}}AFX_DATA_INIT
		
    CDialog::Create(UndoDlg::IDD, pParent);

    ogDdx.Register(this, APP_EXIT, CString("UNDODLG"), CString("Appli. exit"), UndoCf);

}


UndoDlg::~UndoDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void UndoCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    UndoDlg *polDlg = (UndoDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}


void UndoDlg::AppExit()
{
	EndDialog(IDCANCEL);
}




BOOL UndoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_List.SetFont(&ogCourier_Bold_8);
	
	 // return TRUE unless you set the focus to a control
	 // EXCEPTION: OCX Property Pages should return FALSE
	return TRUE;
}



void UndoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(UndoDlg)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UndoDlg, CDialog)
	//{{AFX_MSG_MAP(UndoDlg)
	ON_BN_CLICKED(IDC_UNDO, OnUndo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UndoDlg message handlers



void UndoDlg::SetMessage(CString opMessage)
{
	m_List.AddString(opMessage);
	if ((m_List.GetCount()-12) > 0)
		m_List.SetTopIndex(m_List.GetCount()-12);
	UpdateWindow();
}


void UndoDlg::OnUndo() 
{
	CString olText;
	int ilId = -1;

	int ilCount = m_List.GetCount( );

	if(ilCount <= 0)
		return;

	m_List.GetText( ilCount-1, olText); 


	m_List.DeleteString( ilCount-1);

	int ilPos = olText.Find("#");
	
	if( ilPos > 0)
	{
		olText = olText.Right(  olText.GetLength() - ilPos - 1);

		ilId = atol(olText);
	}

	if(ilId > 0)
		ogPosDiaFlightData.Undo(ilId);


}

void UndoDlg::Reset()
{
	if(m_hWnd != NULL)
	{
		m_List.ResetContent();
		ShowWindow(SW_HIDE);
	}

}
