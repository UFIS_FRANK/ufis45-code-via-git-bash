// CedaMopData.h

#ifndef __CEDAMOPDATA__
#define __CEDAMOPDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct MOPDATA 
{
	long	 Urno; 		
	CTime	 Cdat;		
	char 	 Usec[34]; 	
	CTime	 Lstu;	 	
	char 	 Useu[34]; 	

	char 	 Rurn[20]; 	
	char 	 Valu[50]; 	
	char 	 Ctyp[1]; 	
	char 	 Avil[1];
	char 	 Hopo[4]; 
	char 	 Cref[14]; 
	


	//DataCreated by this class
	int		 IsChanged;

	MOPDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
		Cdat=-1;
		Lstu=-1;
	}

}; // end MOPDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaMOPData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

	CCSPtrArray<MOPDATA> omData;
    CCSPtrArray<MOPDATA> omAData;
	CCSPtrArray<MOPDATA> omDData;

// Operations
public:
    CedaMOPData();
	~CedaMOPData();
	
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<MOPDATA> *popAct,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertMOP(MOPDATA *prpACT,BOOL bpSendDdx = TRUE);
	bool InsertMOPInternal(MOPDATA *prpACT,CString ADID ="A");
	bool UpdateMOP(MOPDATA *prpACT,BOOL bpSendDdx = TRUE);
	bool UpdateMOPInternal(MOPDATA *prpACT);
	bool DeleteMOP(long lpUrno);
	bool DeleteMOPInternal(MOPDATA *prpACT);
	MOPDATA  *GetMOPByUrno(long lpUrno);
	bool SaveMOP(MOPDATA *prpACT);
	char pcmMOPFieldList[2048];
	//void ProcessACTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	//bool InsertMOPInternal(MOPDATA *prpMOP,CString ADID);
	bool SaveMOPInternal(long flightUrno,CString ADID);

	bool ClearData(CString ADID);

	char m_pclAData[3072];
	char m_pclDData[3072];

	void InsertMOPInternalData(char* pclData,CString ADID);
	char* GetMOPInternalData(CString ADID);


	// Private methods
private:
    void PrepareMOPData(MOPDATA *prpMOPData);
	CString omWhere; //Prf: 8795
	bool ValidateMOPBcData(const long& lrpUnro); //Prf: 8795
};
void	EliminateColons(char *);

//---------------------------------------------------------------------------------------------------------
extern CedaMOPData ogCedaMOPData;

#endif //__CedaMOPData__
