// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <InitialLoadDlg.h>

#include <CCSBasic.h>
#include <CedaBasicData.h>
#include <PrivList.h>

#include <CedaPSTData.h>
#include <CedaHTYData.h>
#include <CedaGATData.h>
#include <CedaCICData.h>
#include <CedaBLTData.h>
#include <CedaALTData.h>
#include <CedaAPTData.h>
#include <CedaACTData.h>
#include <CedaACRData.h>
#include <CedaGhsData.h>
#include <CedaGegData.h>
#include <CedaPerData.h>
#include <CedaGrmData.h>
#include <CedaGrnData.h>
#include <CedaGhpData.h>
#include <CedaGpmData.h>
#include <CedaPfcData.h>
#include <CedaPrcData.h>
#include <CedaBsdData.h>

// start bch
#include <CedaNATData.h>
#include <CedaACTData.h>
#include <CedaALTData.h>
#include <CedaAPTData.h>
#include <CedaBLTData.h>
#include <CedaPSTData.h>
#include <CedaGATData.h>
#include <CedaHTYData.h>

#include <CedaParData.h>
#include <CedaValData.h>
#include <CedaWroData.h>
#include <CedaAloData.h>
#include <CedaSgrData.h>
#include <CedaSgmData.h>

CString GetString(UINT ID)
{
	CString olRet;
	olRet.LoadString(ID);
	return olRet;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

const char *pcgAppName = "Rules";

CString ogFltiPrio = "";
char pcgTableExt[10];

CCSLog          ogLog(pcgAppName);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, pcgAppName);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);

CBasicData       ogBasicData;
CCSBasic ogCCSBasic;
CedaBasicData	ogBCD("", &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);
long  igWhatIfUrno;
CString ogWhatIfKey;

CedaParData ogParData;
CedaValData ogValData;
CedaCfgData ogCfgData;
CedaACRData ogAcrData;
CedaACTData ogActData;
CedaALTData ogAltData;
CedaAPTData ogAptData;
//CedaBLTData ogBltData;
CedaBsdData ogBsdData;
CedaCICData ogCicData;
CedaGATData ogGatData;
CedaGegData ogGegData;
CedaGhpData ogGhpData;
CedaGhsData ogGhsData;
CedaGpmData ogGpmData;
CedaGrmData ogGrmData;
CedaGrnData ogGrnData;
CedaHTYData ogHtyData;
CedaNATData ogNatData;
CedaPerData ogPerData; 
CedaPfcData ogPfcData;
CedaPrcData ogPrcData;
CedaPSTData ogPstData;
CedaWROData ogWROData;
CedaAloData	ogAloData;
CedaSgrData	ogSgrData;
CedaSgmData ogSgmData;

PrivList ogPrivList;

char pcgHome[4] = "HAJ";
bool bgNoScroll;
bool bgOnline = true;
bool bgIsButtonListMovable = false;
CInitialLoadDlg *pogInitialLoad;
bool IsRemaExist = false;
bool bgDoesFidsRemaExist = false;
bool IsAdjacentGatRestriction = false;
bool bgUseMultiAirport = false;


CString ogAppName = "Rules"; // Name of *.exe file of this

CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogSmallFonts_Regular_8;
CFont ogSmallFonts_Bold_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogCourier_Bold_10;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Bold_8;
CFont ogCourier_Regular_9;


CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;

CFont ogScalingFonts[30];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;


CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);


/////////////////////////////////////////////////////////////////////////////

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;


// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}

	// create a break job brush pattern
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}


void InitFont() 
{
    CDC dc;
    ASSERT(dc.CreateCompatibleDC(NULL));

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_6.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_7.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, /*"Small Fonts"*/"MS LineDraw");
    ASSERT(ogSmallFonts_Regular_8.CreateFontIndirect(&logFont));
	//ASSERT(ogSmallFonts_Regular_7.CreatePointFont(70, "Small Fonts"));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Bold_10.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_10.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Bold_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_9.CreateFontIndirect(&logFont));

	for(int i = 0; i < 8; i++)
	{
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfQuality = PROOF_QUALITY;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Small Fonts"); 
		ASSERT(ogScalingFonts[i].CreateFontIndirect(&logFont));
	}
/*	for( i = 5; i < 8; i++)
	{
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS; 
		logFont.lfQuality = PROOF_QUALITY;
		lstrcpy(logFont.lfFaceName, "Arial");
		ASSERT(ogScalingFonts[i].CreateFontIndirect(&logFont));
	}
*/
	for( i = 8; i < 20; i++)
	{ 
		logFont.lfCharSet= DEFAULT_CHARSET;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		logFont.lfQuality = PROOF_QUALITY;

		lstrcpy(logFont.lfFaceName, "MS Sans Serif"); /*"MS Sans Serif""Arial"*/
		ASSERT(ogScalingFonts[i/*GANTT_S_FONT*/].CreateFontIndirect(&logFont));
	}
/*    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[].CreateFontIndirect(&logFont));

	// logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[GANTT_L_FONT].CreateFontIndirect(&logFont));
 
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[GANTT_XL_FONT].CreateFontIndirect(&logFont));
*/
/*	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;
*/
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Regular_8.CreateFontIndirect(&logFont));

//Times	

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(30, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ASSERT(ogTimesNewRoman_30.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ASSERT(ogTimesNewRoman_16.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ASSERT(ogTimesNewRoman_12.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ASSERT(ogTimesNewRoman_9.CreateFontIndirect(&logFont));

    dc.DeleteDC();
}

