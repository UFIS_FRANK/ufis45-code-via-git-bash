#ifndef _TABLE_H_
#define _TABLE_H_

//#include "ccsobj.h"
#include "CCSDragDropCtrl.h"

// Constants for CTable::SetTextLineSeparator()
#define ST_NORMAL	0
#define ST_THICK	1

//@Man:
//@Memo: Display data as a list of textlines
//@See: MFC's CWnd, MFC's CListBox, CCSObject
/*Doc
  The class CTable displays table (lists) of textlines. The apperance of such a CTable
  field had already shown in the PEPPER prototype. The CTable class contains these
  following graphical elements:

  \begin{itemize}
  \item Background (colour and margins are configurable)
  \item Title
  \item Data fields in the title
  \item TextLines, automatic height depending on the text font
  \item Line separator (currently a dotted line)
  \item 3D-look column separator (ridges), placement is calculated automatically based
        on the size of the fields in the title or via format array in dialog units.
        Proportional fonts are possible.
  \item Vertical scroll bar will be displayed when the number of lines is larger than
        the viewing area.
  \item Horizontal scroll bar will be displayed when the number of columns is larger
        than the viewing area.
  \item The surrounding window can call the SetPosition(..) member function with appropriate
        parameters to adjust the table window to the new size and new position.
  \end{itemize}    

  In this first implementation, the CTable will handle the mouse events the same way as
  the standard Windows list box does (Click, Shift+Click and Ctrl+Click).
*/

class CTableListBox;

class CTable: public CWnd
{
// Public members
public:
    /*@ManDoc:
      Creates an empty CTable object.
    */
    CTable();

    /*@ManDoc:
      Creates CTable object. Initializes data members with data passed as parameters.
      It will not create or display the window table, this should be dont by the
      DisplayTable(..) method only. The calling function is responsible for passing valid
      data. See also: CTable::SetTableData().
    */
	CTable::CTable(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor = ::GetSysColor(COLOR_WINDOWTEXT),
        COLORREF lpTextBkColor = ::GetSysColor(COLOR_WINDOW),
        COLORREF lpHighlightColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightBkColor = ::GetSysColor(COLOR_HIGHLIGHT),
        CFont *popTextFont = NULL,
        CFont *popHeaderFont = NULL
    );

    ~CTable();

	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

    /*@ManDoc:
      Set data members to data passed as parameters. The calling function is responsible
      for passing valid data. By now, this member function always return {\bf RCSuccess}.

      {\bf ATTENTION!}
      Please notice that the passed parameters have changed from the specification of
      {\bf ..\BKK0410A\TABLE.DOC}. The HANDLE hmParentWindow was changed to {\bf (CWnd *)}
      for MFC compatibility. Window positioning parameters changed from {\bf long}
      to {\bf int} because finally they must converted to {\bf CRect} which providing
      constructor only for type {\bf int}. Actually, I feel that we should just pass the
      {\bf CRect} directly to this method. Text, both normal and selected (highlighted),
      colors and font parameters were changed. (Note that the header bar and the vertical
      separators or ridges use the system color of button shading.) Furthermore, many
      parameters also provide default value.
    */
    bool SetTableData(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor = ::GetSysColor(COLOR_WINDOWTEXT),
        COLORREF lpTextBkColor = ::GetSysColor(COLOR_WINDOW),
        COLORREF lpHighlightColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightBkColor = ::GetSysColor(COLOR_HIGHLIGHT),
        CFont *popTextFont = NULL,
        CFont *popHeaderFont = NULL
    );
    
    /*@ManDoc:
      Displays the table. Calling this method is a must after you have change window
      parameters, move/resize window, or update data in the table. Except for
      SetPosition(..), AddTextLine(..), ChangeTextLine(..), and DeleteTextLine(..),
      other provided public methods should call this method after finish update table.
      The DisplayTable(..) will create the window if it is not already created.
      This method returns {\bf RCSuccess} on success, or {\bf RCFailure} if it cannot
      create windows for a CTable instance.
    */
    bool DisplayTable();

    /*@ManDoc:
      Sets the position data members to the passed parameters if the CTable window is
      currently displayed. Also update the display. This method always returns
      {\bf RCSuccess}.
    */
    bool SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd);

    //@ManMemo: Returns the associate listbox of the this table.
    CListBox *GetCTableListBox();

	//@ManMemo: Returns the line which the point (logical related to listbox) is on.
	int GetLinenoFromPoint(CPoint opPoint);

    /*@ManDoc:
      Fill the {\bf omFieldNames} array with the text data passed in {\bf opFieldNames}.
      {\bf opFieldNames} passes the text of the field names in a comma-separated list.
      The first item in this list will be copied to {\bf omFieldNames[0]}. These field names
      will be used in {\bf GetFieldValue(..)}. This method always returns {\bf RCSuccess}.
    */
    bool SetFieldNames(CString opFieldNames);

    /*@ManDoc:
      Fill the {\bf omHeaderFields} array with the text data passed in {\bf opHeaderFields}.
      {\bf opHeaderFields} passes the text of the table header fields in a vertbar-separated
      list. The first item in this list will be copied to {\bf omHeaderFields[0]}. This
      method always returns {\bf RCSuccess}.
    */
    bool SetHeaderFields(CString opHeaderFields);

    /*@ManDoc:
      Fill the {\bf omFormatList} array with the text data passed in {\bf opFormatList}.
      {\bf opFormatList} passes the text of the format descriptors in a vertbar-separated list.
      The first item in this list will be copied to {\bf omFieldNames[0]}. These format
      descriptors will be used to format each column entry.

      {\bf ATTENTION!}
      Due to some suspicious in the format descriptors in the specification (details below),
      I just leave out the implementation of format descriptors described in the
      specification for now, and invent just simply formatting like this:

      Examples:
      "20,40,6,40"      -- indicate 4 fields, the third field is 6 characters long

      {\bf PROBLEM WITH THE SPECIFICATION}
      Formatting character field is clear, but there is no way to specify decimal points
      in a numeric field. For example, 1234.56 formatted by "N-.010,." will be "..1,234.56".
      What about an integer field, or value with more (or less) than 2 decimal points.

      Date formatting is strange and not follow the same rule with character and numerical
      fields. For example, I expected that 199601101640 should be formatted by "D+ 008/"
      to get "10/04/96" but in the specification, we need "D+ /008" instead. Moreover, the
      specification allows the D format to format time also, as in "D+ :005".

      To solve this problem, I suggest that we should move to use some kind of enumerated
      format specifier. For example, you may use the format "D1" for the result "100496",
      "D2" for the result "10041996", "D3" for "10.04.96". This way is simpler and offer
      a chance to indicate the length and padding character of the date field also.
    */
    bool SetFormatList(CString opFormatList);
	bool SetTypeList(CString opTypeList);
    //@ManMemo: Remove items from the table content.
    bool ResetContent();

    //@ManMemo: Returns zero-based index of the item that has the focus rectangle.
    int GetCurrentLine();

    /*@ManDoc:
      Formats the passed {\bf opText} and add it to the internal {\bf omData} member.
      Update the display if necessary. The parameter "pvpData" is a pointer to the
      corresponding of the formatted {\bf opText}. This is required for passing the
      {\bf pvpData} back to the parent window when user press a double-click and generate
      a WM_TABLETEXTSELECT message. Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    bool AddTextLine(CString opText, void *pvpData = NULL);

    /*@ManDoc:
      Formats the passed {\bf opText} and insert it to the internal {\bf omData} member.
      Unlike the {\bf AddTextLine} member function, {\bf InsertTextLine} will insert the
      given {\bf opText} to be at the line number given in {\bf ipLineNo}. If {\bf ipLineNo}
      parameter is -1, the string is added to the end of the list. Update the display if
      necessary. The parameter "pvpData" is a pointer to the corresponding of the formatted
      {\bf opText}. This is required for passing the {\bf pvpData} back to the parent window
      when user press a double-click and generate a WM_TABLETEXTSELECT message. Returns
      {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    bool InsertTextLine(int ipLineNo, CString opText, void *pvpData = NULL);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, formats the passed {\bf opText} and
      replace the line in {\bf omData} member. ({\bf ipLineNo} -1 indicates current line.)
      The parameter "pvpData" is a pointer to the corresponding of the formatted
      {\bf opText}. This is required for passing the {\bf pvpData} back to the parent window
      when user press a double-click and generate a WM_TABLETEXTSELECT message.
      Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    bool ChangeTextLine(int ipLineNo, CString opText, void *pvpData = NULL);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, delete the line from {\bf omData}
      member. ({\bf ipLineNo} -1 indicates current line.) Update display if necessary.
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    bool DeleteTextLine(int ipLineNo);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change color of the line.
      ({\bf ipLineNo} -1 indicates current line.) Update display if necessary.
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    bool SetTextLineColor(int ipLineNo, COLORREF lpTextColor, COLORREF lpTextBkColor);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the line separator of the
	  line. The separator may be TLS_NORMAL or TLS_THICK. ({\bf ipLineNo} -1 indicates
	  current line.) Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
	bool SetTextLineSeparator(int ipLineNo, int ipSeparatorType);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the font of the line.
	  Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
	bool SetTextLineFont(int ipLineNo, CFont *popFont);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the current drag status of
      the given line ({\bf ipLineNo} -1 indicates current line.) The user can Only the lines with
	  dragging enabled will be dragg
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    bool SetTextLineDragEnable(int ipLineNo, BOOL bpEnable);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, get the text from the line in
      {\bf omData} member ({\bf ipLineNo} -1 indicates current line), copy it to {\bf opText}.
      Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The specification expected this function formats line back to internal format
      (trim characer fields, format date and time fields to Ceda format). However, this
      feature was removed since it's not make sense since the destination is CString.
      Moreover, CTable does not know anything about the data record structure. But the
      caller do.
    */
    bool GetTextLineValue(int ipLineNo, CString &opText);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, return a void pointer which previously
      stored together with text line when AddTextLine(), InsertTextLine(), or ChangeTextLine().
      Otherwise, return NULL if there is no current line or window is not created yet.
    */
    void *GetTextLineData(int ipLineNo);

	//@ManMemo: returns current drag enable status of the specified text line
	BOOL GetTextLineDragEnable(int ipLineNo);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number and {\bf ipFieldNo} a valid field
      number, get the text of the requested field from the line in {\bf omData} member
      ({\bf ipLineNo} -1 indicates current line), copy it to {\bf opText}. Returns
      {\bf RCSuccess} or {\bf RCFailure}.
      
      {\bf ATTENTION!}
      The specification expected this function formats line back to internal format
      (trim characer fields, format date and time fields to Ceda format). However, this
      feature was removed since it's not make sense since the destination is CString.
      Moreover, CTable does not know anything about the data record structure. But the
      caller do.
    */
    bool GetTextFieldValue(int ipLineNo, int ipFieldNo, CString &opText);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number and {\bf opFieldName} a valid field
      name, get the text of the requested field (calculated field number from
      {\bf omFieldNames}) from the line in {\bf omData} member. Returns {\bf RCSuccess}
      or {\bf RCFailure}.
    */
    bool GetTextFieldValue(int ipLineNo, CString opFieldName, CString &opText);

    /*@ManDoc:
      If {\bf ipFieldNo} contains a valid field number, search through the table for the
      first line containing {\bf opText}. Returns line number (base-0) containing search
      string ({\bf opText}), or -1 if not found. On success, remember {\bf imNextLineToSearch}
      and the text {\bf opText} in {\bf omLastSeachText}. On further calls to
      SearchTextLine(..) with the same text in {\bf opText} start at this next line.
    */
    int SearchTextLine(int ipFieldNo, CString opText);

	void SetSelectMode(int ipSelectMode);
// Protected members
protected:
    // This variable indicates that the member function SetTableData(..) has already
    // been called or not. It will be set to FALSE on the default constructor, and changed
    // to TRUE on success SetTableData(..). The member function DisplayTable(..) cannot
    // create the window until "bmInited" is set to TRUE.
    //
    BOOL bmInited;

    // these protected data members are corresponding to parameters in SetTableData()
    CWnd *pomParentWindow;
    int imXStart, imXEnd;
    int imYStart, imYEnd;
    COLORREF lmTextColor;
    COLORREF lmTextBkColor;
    COLORREF lmHighlightColor;
    COLORREF lmHighlightBkColor;
    CFont *pomTextFont;
    CFont *pomHeaderFont;

    // These three variables will be changed by SetHeaderFields(), SetFieldNames(), and
    // SetFormatList(). Actually, we may implement to keep the comma-separated strings
    // which passed into these member functions directly. However, for sake of some
    // efficiency, they are split into a form of array, which we could access any elements
    // directly and more efficiently.
    //
    CStringArray omHeaderFields;    // array of column headers
    CStringArray omFieldNames;      // array of column (field) names
    CStringArray omFormatList;      // array of column format specifiers
	CStringArray omTypeList;		// array of column type specifiers (text or color)
    // This array represents the whole data of the table.
    // Each record is kept in the vertbar-separated format, and will be extracted on the fly.
    //
    // ATTENTION!
    // Type of this member changed from the speicification (CPtrArray) which was defined
    // as an array of structs, because just handling string is simpler, use less resource,
    // and the specification say something unclear about the raw data struct with unknown
    // format or details of content.
    //
    CStringArray omData;
    CPtrArray omDataPtr;		// associated data pointer for each text line
	CDWordArray omColor;		// associated color for each text line
	CDWordArray omBkColor;		// associated background color for each text line
	CWordArray omDragEnabled;	// associated DragEnable boolean flag for each text line
	CWordArray omSeparatorType;	// associated separator type for each text line
	CPtrArray omFontPtr;		// associated font for each text line

    // internal members used for searching routine -- SearchTextLine(..)
    int imNextLineToSearch;
    CString omLastSearchText;

    // private data for pixel calculation -- mostly used in CalculateWidgets()
    int imTextFontHeight;
    int imHeaderFontHeight;
    CDWordArray omLBorder;       // array of column's left-border X position
    CDWordArray omRBorder;       // array of column's right-border X position
    int imTableContentOffset;
    int imHorizontalExtent;     // list box width (in pixel)
    CRect omHeaderBarRect;

    void CalculateWidgets();

    // The scrollable list box represents the table content:
   // class CTableListBox;
    friend CTableListBox;       // needed since CTableListBox calls method DrawItem()
public:
	CTableListBox *pomListBox;
	int imItemHeight;

    // helper drawing object -- created in constructor, destroyed in destructor.
    CPen FacePen, ShadowPen, WhitePen, BlackPen;
    CBrush FaceBrush, SeparatorBrush;

    // Message handling:
    // CTable is implemented as an owner-drawn listbox within a child window.
    // These member functions will handle essential messages to allow CTable work correctly.
    //
    // Please understand that, following to OOP concept and MFC style, we should
    // implement MeasureItem() and DrawItem() in the owner-draw list box itself instead of
    // in a CTable which is its parent. However, since MeasureItem() and DrawItem() need
    // to access too many of CTable protected members, I moved to use OnMeasureItem() and
    // OnDrawItem() of CTable instead. On the other hand, if we want to follow OOP concept
    // and MFC style, we should have a new class, CTableContent derived from CListBox, for
    // the table content and handling its own MeasureItem() and DrawItem(). Then, CTable
    // will have a CTableContent as a member, and only draw its header bar.
    //
    //{{AFX_MSG(CTable)
    afx_msg void OnPaint();
    afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    afx_msg void OnDrawItem(int NIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnDestroy();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

    // helper functions for drawing header bar and table
    void DrawHeaderBar();
    void DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
        BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus);

public:
	int tempFlag;    
	int imCurrentColumn;
	int imSelectMode;
};

#endif
