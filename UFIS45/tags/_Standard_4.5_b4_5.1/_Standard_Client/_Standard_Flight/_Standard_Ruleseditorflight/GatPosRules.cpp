// GatPosRules.cpp : implementation file
//

#include "stdafx.h"
#include "Rules.h"
#include "CCSGlobl.h"
#include "CedaGatData.h"
#include "CedaPstData.h"
#include "GatPosRules.h"
#include "BasicData.h"
#include "PstRuleDlg.h"
#include "GatRuleDlg.h"
#include "DutyPreferences.h"

#include <iomanip.h>
#include <fstream.h>
#include <iostream.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DutyPreferences *pogDutyPreferences;


/////////////////////////////////////////////////////////////////////////////
// GatPosRules property page

IMPLEMENT_DYNCREATE(GatPosRules, CPropertyPage)

static int ComparePnam(const PSTDATA **e1, const PSTDATA **e2)
{
	return strcmp((**e1).Pnam, (**e2).Pnam);
}

static int CompareGnam(const GATDATA **e1, const GATDATA **e2)
{
	return strcmp((**e1).Gnam, (**e2).Gnam);
}

GatPosRules::GatPosRules() : CPropertyPage(GatPosRules::IDD)
{
	//{{AFX_DATA_INIT(GatPosRules)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
//	pomPosList = new GxGridTab(this);
//	pomGatList = new GxGridTab(this);
	pomPosList = new CCSTable;
	pomGatList = new CCSTable;
}

GatPosRules::~GatPosRules()
{
	delete pomPosList;
	delete pomGatList;
}

void GatPosRules::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosRules)
	DDX_Control(pDX, IDC_GATE1_LIST, m_GateListTable);
	DDX_Control(pDX, IDC_POS1_LIST, m_PosListTable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosRules, CPropertyPage)
	//{{AFX_MSG_MAP(GatPosRules)
	  ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnGridDblClk)
	  ON_MESSAGE(CLK_BUTTON_PRINT,OnPrintPosition)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosRules message handlers

BOOL GatPosRules::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
//omPosList
//omGatList
	int i=0;
	CRect olRect;
	//omPstUrnos;
	//omGatUrnos;




	CStringArray olGatHeader;
	CStringArray olPosHeader;

	ogPstData.omData.Sort(ComparePnam);
	ogGatData.omData.Sort(CompareGnam);
	for(i = 0; i < ogPstData.omData.GetSize(); i++)
	{
		omPstUrnos.Add(ogPstData.omData[i].Urno);
	}
	for(i = 0; i < ogGatData.omData.GetSize(); i++)
	{
		omGatUrnos.Add(ogGatData.omData[i].Urno);
	}


	m_GateListTable.GetClientRect(olRect);
	
	pomGatList->SetShowSelection(FALSE);
//	pomGatList->SetTableData(this, olRect.left +13 , olRect.right +13, olRect.top + 357, olRect.bottom +359);
	pomGatList->SetTableData(this, olRect.left , olRect.right, olRect.top, olRect.bottom);


	pomGatList->SetShowSelection(FALSE);
	m_PosListTable.GetClientRect(olRect);

	pomPosList->SetTableData(this, olRect.left + 13, olRect.right + 13, olRect.top +13, olRect.bottom + 13);


	CString olViewName = "<Default>";



	
	omGatContextItem = -1;
	omGatRulesViewer.Attach(pomGatList);
	omPosContextItem = -1;
	omPosRulesViewer.Attach(pomPosList);

	
	omGatRulesViewer.ChangeViewTo(olViewName);
	omPosRulesViewer.ChangeViewTo(olViewName);

	UpdateView();



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosRules::UpdateView()
{

//	CString olViewName = omViewer.GetViewName();
	CString olViewName = "<Default>";

    //omPosRulesViewer.pomTable->DisplayTable();
    //omGatRulesViewer.pomTable->DisplayTable();

	int ilPosLine = pomPosList->pomListBox->GetTopIndex();

	int ilGatLine = pomGatList->pomListBox->GetTopIndex();
	
	AfxGetApp()->DoWaitCursor(1);
    omGatRulesViewer.ChangeViewTo(olViewName);
	omPosRulesViewer.ChangeViewTo(olViewName);

	pomPosList->pomListBox->SetTopIndex(ilPosLine);

	pomGatList->pomListBox->SetTopIndex(ilGatLine);
	

	AfxGetApp()->DoWaitCursor(-1);

}


void GatPosRules::MakePosLineData(PSTDATA *popPst, CStringArray &ropArray)
{
	CString olReGates;
	POSITION rlPos;
	for ( rlPos = ogGatData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		GATDATA *prlGat;
		ogGatData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlGat);
		if(prlGat != NULL)
		{
			if(strcmp(popPst->Pnam, prlGat->Rga2) == 0)
			{
				olReGates+=prlGat->Gnam + CString("  ");
			}
			if(strcmp(popPst->Pnam, prlGat->Rga1) == 0)
			{
				olReGates+=prlGat->Gnam + CString("  ");
			}
		}
	}
	ropArray.Add(olReGates);
	CStringArray olAcgrList;
//	ExtractItemList(popPst->Acgr, &olAcgrList, '/');
	CString olTmpPosr = popPst->Posr;
	if(olTmpPosr.Replace(";","#") < 3)
	{
		olTmpPosr = popPst->Posr;
		olTmpPosr.Replace(";","|");
		strcpy(popPst->Posr, olTmpPosr.GetBuffer(0));
	}
	
	ExtractItemList(popPst->Posr, &olAcgrList, ';');

	for(int i = 0; i < olAcgrList.GetSize(); i++)
	{
		ropArray.Add(olAcgrList[i]);
	}
}

void GatPosRules::MakeGatLineData(GATDATA *popGat, CStringArray &ropArray)
{
	CStringArray olResbList;
//	ExtractItemList(popGat->Resb, &olResbList, '/');
	CString olTmpGatr = popGat->Gatr;
	if(olTmpGatr.Replace(";","#") < 3)
	{
		olTmpGatr =popGat->Gatr;
		olTmpGatr.Replace("/",";");
		strcpy(popGat->Gatr, olTmpGatr.GetBuffer(0));
	}
	

	ExtractItemList(popGat->Gatr, &olResbList, ';');
	for(int i = 0; i < olResbList.GetSize(); i++)
	{
		ropArray.Add(olResbList[i]);
	}
}
LONG GatPosRules::OnGridDblClk(UINT wParam, LONG lParam)
{

	int ilCol = HIWORD(wParam);
	int ilRow = LOWORD(wParam);
		
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lParam;


	if(polNotify->SourceTable == pomPosList)
	{
		if(ilRow >= 0 && ilRow < omPstUrnos.GetSize())
		{
			long llUrno = (long)omPstUrnos[ilRow];
			PSTDATA *polPst = ogPstData.GetPSTByUrno(llUrno);
			if(polPst != NULL)
			{
				PstRuleDlg olDlg(this, polPst);
				if(olDlg.DoModal() == IDOK)
				{
					UpdateView();
				} 
			}
		}
	}
	if(polNotify->SourceTable == pomGatList)
	{
		if(ilRow >= 0 && ilRow < omGatUrnos.GetSize())
		{
			long llUrno = (long)omGatUrnos[ilRow];
			GATDATA *polGat = ogGatData.GetGATByUrno(llUrno);
			if(polGat != NULL)
			{
				GatRuleDlg olDlg(this, polGat);
				if(olDlg.DoModal() == IDOK)
				{
					UpdateView();
				}
			}
		}
	}

	return 0L;
}

BOOL GatPosRules::OnKillActive() 
{
	// TODO: Add your specialized code here and/or call the base class

	pogDutyPreferences->pomSaveButton->EnableWindow(TRUE);	
	

	return CPropertyPage::OnKillActive();
}

BOOL GatPosRules::OnSetActive() 
{

	pogDutyPreferences->pomSaveButton->EnableWindow(FALSE);	
	

	return CPropertyPage::OnSetActive();
}

void GatPosRules::OnPrintPosition()
{
	
	char pclExcelPath[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString("FIPS", "Excel", "DEFAULT",
        pclExcelPath, sizeof pclExcelPath, pclConfigPath);

	if(!strcmp(pclExcelPath, "DEFAULT"))
		AfxMessageBox("Unable to Find MSExcel");

	ofstream of;
	CString omTableName = "PsoitionRules";
	CString olFileName = omTableName;
	CString opTrenner = ",";
	
	//Column Headers//
	CString HeadColu1 = "No.";
	CString HeadColu2 = "Pos";
	CString HeadColu3 = "Wing Span";
	CString HeadColu4 = "Length";
	CString HeadColu5 = "Height";
	CString HeadColu6 = "A/C Parameter List";
	CString HeadColu7 = "Pos";
	CString HeadColu8 = "Max";
	CString HeadColu9 = "Pos";
	CString HeadColu10 = "Max";
	CString HeadColu11 = "M";
	CString HeadColu12 = "P";
	CString HeadColu13 = "Nat";
	CString HeadColu14 = "A/L Parameter";
	CString HeadColu15 = "AirCraft Groups";
	CString HeadColu16 = "Sequence";
	CString HeadColu17 = "Groundtime";
	CString HeadColu18 = "Service Types";
	CString HeadColu19 = "Flight ID:ARR";
	CString HeadColu20 = "Flight ID:DEP";
	CString HeadColu21 = "Registration"; 

	char pHeader[256];
	strcpy (pHeader, CCSLog::GetTmpPath());
	CString path = pHeader;
	CString omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);

	int ilwidth = 1;

	of  << setw(ilwidth) << omTableName << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;


	of  << setw(ilwidth) << HeadColu1
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu2
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu3
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu4
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu5
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu6
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu7
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu8
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu9
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu10
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu11
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu12
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu13
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu14
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu15
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu16
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu17
		<< setw(1) << opTrenner
		<< setw(ilwidth) << HeadColu18
		<< setw(1) << opTrenner;

		if (!ogFltiPrio.IsEmpty())
		{
			of << setw(ilwidth) << HeadColu19
			   << setw(1) << opTrenner
			   << setw(ilwidth) << HeadColu20
			   << setw(1) << opTrenner;
		}

		if( ( (CRulesApp *)AfxGetApp() )->CanHandleAcrRegn() )
		{
			of << setw(ilwidth) << HeadColu21
			   << setw(1) << opTrenner;
		}

		of	<< endl; 

	int ilCount = omPosRulesViewer.omLines.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		POSTABLE_LINEDATA *polPst = &omPosRulesViewer.omLines.GetAt(i);
		if(polPst != NULL)
		{
			CString PosNo ;
			PosNo.Format(_T("%d"),polPst->Rtnr);
			of.setf(ios::left, ios::adjustfield);
			of  << setw(1) << PosNo
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Position
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->WSpan
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Length
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Height
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->ACExL
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Pos1
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Max1
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Pos2
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Max2
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->M
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Prio
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Nat
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->PAL
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Aircraftgroups
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Sequ
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Groundtime
				<< setw(1) << opTrenner 
				<< setw(1) << polPst->Service
				<< setw(1) << opTrenner ;
				
			if (!ogFltiPrio.IsEmpty())
			{
				of	<< setw(1) << polPst->IdDEP
					<< setw(1) << opTrenner 
					<< setw(1) << polPst->IdARR
					<< setw(1) << opTrenner ;
				
			}

			if( ( (CRulesApp *)AfxGetApp() )->CanHandleAcrRegn() )
			{
				of	<< setw(1) << polPst->Registration; 
				
			}
			
			of << endl;
				 
		}
	
	}

	of.close();

	char pclTmp[256] ; 
	omFileName.TrimLeft();
	omFileName.TrimRight();
	strcpy(pclTmp ,omFileName);
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int li_Path = _spawnv( _P_NOWAIT , pclExcelPath, args );

	if(li_Path == -1)
		AfxMessageBox("The EXCEL path in Ceda.ini is not proper.Please correct it");
	
}
