// Rules.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <Rules.h>
#include <ButtonListDlg.h>
#include <InitialLoadDlg.h>
#include <LoginDlg.h>
#include <BasicData.h>
#include <BackGround.h>

#include <AatHelp.h>
//------------------------------
#include <CedaPSTData.h>
#include <CedaHTYData.h>
#include <CedaGATData.h>
#include <CedaCICData.h>
#include <CedaBLTData.h>
#include <CedaBLAData.h>
#include <CedaALTData.h>
#include <CedaAPTData.h>
#include <CedaACTData.h>
#include <CedaACRData.h>
#include <CedaGhsData.h>
#include <CedaGegData.h>
#include <CedaPerData.h>
#include <CedaGrmData.h>
#include <CedaGrnData.h>
#include <CedaGhpData.h>
#include <CedaGpmData.h>
#include <CedaPfcData.h>
#include <CedaPrcData.h>

// start bch
#include <CedaNATData.h>
#include <CedaACTData.h>
#include <CedaALTData.h>
#include <CedaAPTData.h>
#include <CedaBLTData.h>
#include <CedaPSTData.h>
#include <CedaGATData.h>
#include <CedaHTYData.h>

#include <CedaParData.h>
#include <CedaValData.h>
#include <CedaWroData.h>

#include <CedaAloData.h>
#include <CedaSgrData.h>
#include <CedaSgmData.h>

//------------------------------
#include <DutyPreferences.h>
#include <RegisterDlg.h>
#include <PrivList.h>
#include <ResRc1.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>
#include <CedaBasicData.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CRulesApp

BEGIN_MESSAGE_MAP(CRulesApp, CWinApp)
	//{{AFX_MSG_MAP(CRulesApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRulesApp construction

CRulesApp::CRulesApp()
{
	pogInitialLoad = NULL;
}

CRulesApp::~CRulesApp()
{
	TRACE("CCSPrjApp::~CCSPrjApp\n");
	if (pogDutyPreferences)
	{
		pogDutyPreferences->DestroyWindow();
		delete pogDutyPreferences;
		pogDutyPreferences = NULL;
	}
	DeleteBrushes();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CRulesApp object

CRulesApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CRulesApp initialization

BOOL CRulesApp::InitInstance()
{
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	#ifdef _AFXDLL
		Enable3dControls();			// Call this when using MFC in a shared DLL
	#else
		Enable3dControlsStatic();	// Call this when linking to MFC statically
	#endif

	// Parse the command line to see if launched as OLE server
	if (RunEmbedded() || RunAutomated())
	{
		// Register all OLE server (factories) as running.  This enables the
		//  OLE libraries to create objects from other applications.
		COleTemplateServer::RegisterAll();
	}
	else
	{
		// When a server application is launched stand-alone, it is a good idea
		//  to update the system registry in case it has been damaged.
		COleObjectFactory::UpdateRegistryAll();
	}

	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

//	GXInit();
	CStringArray olCmdLineStghArray;
	olCmdLineStghArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	//m_lpCmdLine => "AppName,UserID,Password"
	if(olCmdLine.GetLength() > 0)
	{
		if(ExtractItemList(olCmdLine,&olCmdLineStghArray) != 2)
		{
			MessageBox(NULL,GetString(IDS_STRING325),GetString(IDS_STRING326),MB_ICONERROR);
			return FALSE;
		}
	}

	AatHelp::Initialize(ogAppName);
	
	// Standard CCS-Fonts and Brushes initialization
    InitFont();
	CreateBrushes();

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclHome[64];

	GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pclHome, sizeof pclHome, pclConfigPath);

	char cTempAcrRegn[10];
	// 050228 MVy: handle Aircraft Registrations only when configuration key POSRULE_WITH_REGN is set
	GetPrivateProfileString( "FIPS", "POSRULE_WITH_REGN", "FALSE", cTempAcrRegn, sizeof cTempAcrRegn, pclConfigPath );
	m_bHandleAcrRegn = !strncmp( cTempAcrRegn, "TRUE", 5 );

	char cTempBltFlti[10];
	GetPrivateProfileString( "FIPS", "BELTRULE_WITH_FLTI", "FALSE", cTempBltFlti, sizeof cTempBltFlti, pclConfigPath );
	m_bBltFlti = !strncmp( cTempBltFlti, "TRUE", 5 );

	char clTempBltAlloc[10];
	GetPrivateProfileString( "FIPS", "BELTRULE_ALLOC", "FALSE", clTempBltAlloc, sizeof clTempBltAlloc, pclConfigPath );
	m_bBltAloc = !strncmp( clTempBltAlloc, "TRUE", 5 );

	char cTempRulesPrint[10];
	GetPrivateProfileString( "FIPS", "PRINT_FIPSRULES", "FALSE", cTempRulesPrint, sizeof cTempRulesPrint, pclConfigPath );
	m_bPrintRules = !strncmp( cTempRulesPrint , "TRUE", 5 );   	

	//Fids Remarks
    char cTempFidsRemark[10];
	GetPrivateProfileString( "FIPS", "FIDSREMARK_FIELD", "FALSE", cTempFidsRemark, sizeof cTempFidsRemark, pclConfigPath );
	IsRemaExist = !strncmp( cTempFidsRemark, "TRUE", 5 );



	GetPrivateProfileString( "FIPS", "CENTERLINE_DIST", "FALSE", cTempBltFlti, sizeof cTempBltFlti, pclConfigPath );
	m_bCenterLine = !strncmp( cTempBltFlti, "TRUE", 5 );

	GetPrivateProfileString( "FIPS", "PST_RULE_FLNO", "FALSE", cTempBltFlti, sizeof cTempBltFlti, pclConfigPath );
	m_bHandleFlno = !strncmp( cTempBltFlti, "TRUE", 5 );

	GetPrivateProfileString( "FIPS", "POS_MINMAXPAX", "FALSE", cTempBltFlti, sizeof cTempBltFlti, pclConfigPath );
	m_bPosMinMaxPax = !strncmp( cTempBltFlti, "TRUE", 5 );

	GetPrivateProfileString( "FIPS", "BELT_MAXBAG", "FALSE", cTempBltFlti, sizeof cTempBltFlti, pclConfigPath );
	m_bBeltBag = !strncmp( cTempBltFlti, "TRUE", 5 );
    
	char cAdjacentGatRestriction[20];
	GetPrivateProfileString( "FIPS", "ADJACENT_GAT_RESTRICTION", "FALSE", cAdjacentGatRestriction, sizeof cAdjacentGatRestriction, pclConfigPath );
	IsAdjacentGatRestriction = !strncmp( cAdjacentGatRestriction, "TRUE", 5 );


	//setting for multi airports
	char pclTmpString[64];
	GetPrivateProfileString("GLOBAL", "MULTI_AIRPORT", "FALSE",pclTmpString,sizeof pclTmpString, pclConfigPath );
	if (strcmp(pclTmpString,"TRUE")==0)
		bgUseMultiAirport = true;
	else
		bgUseMultiAirport = false;


	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pclHome);
	strcpy(CCSCedaData::pcmApplName, "RULES");

	ogBcHandle.SetHomeAirport(pclHome);

	strcpy(pcgHome, pclHome);

	// CCS-Desktop background with an bitmap (logo or copyright etc.. 
	// loaded in the methode CBackGround::OnPaint)
//	pomBackGround = new CBackGround;
//	pomBackGround->Create(NULL, IDD_BACKGROUND);





    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(GetString(IDS_STRING329) + ogCommHandler.LastError());
        return FALSE;
    }

	//if(MessageBox(NULL,"Only for internal use.\n Continue?","Warning!!!!!!!!!", MB_YESNO) == IDNO)
	//	return 0;






#if	0
	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,pomBackGround);



	if(olCmdLineStghArray.GetSize() == 2)
	{
		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, olCmdLineStghArray.GetAt(0));

		ogBasicData.omUserID = olCmdLineStghArray.GetAt(0);
		ogCommHandler.SetUser(olCmdLineStghArray.GetAt(0));

	
				
		if(!olLoginDlg.Login(olCmdLineStghArray.GetAt(0),olCmdLineStghArray.GetAt(1) ) )
			return FALSE;

	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			//strcpy(pcgUser,olLoginDlg.omUsername);
			//strcpy(pcgPasswd,olLoginDlg.omPassword);
			ogBasicData.omUserID = olLoginDlg.omUsername;
			ogCommHandler.SetUser(olLoginDlg.omUsername);
			int ilStartApp = IDOK;
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_INFO,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName(ogAppName);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

	if(olCmdLineStghArray.GetSize() == 2)
	{
		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, olCmdLineStghArray.GetAt(0));

		ogBasicData.omUserID = olCmdLineStghArray.GetAt(0);
		ogCommHandler.SetUser(olCmdLineStghArray.GetAt(0));

		CString olUserName = olCmdLineStghArray.GetAt(0);
		CString olPassword = olCmdLineStghArray.GetAt(1);
		if(olLoginCtrl.DoLoginSilentMode(olUserName,olPassword) != "OK")
			return FALSE;

	}
	else
	{
		if (olLoginCtrl.ShowLoginDialog() != "OK")
		{
			olDummyDlg.DestroyWindow();
			return FALSE;
		}

	}

	ogBasicData.omUserID = olLoginCtrl.GetUserName_();
	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

//	olDummyDlg.DestroyWindow();
#endif

		if(bgUseMultiAirport)
		{
			CString strSelectedExt= olLoginCtrl.GetTableExt();
			strSelectedExt.TrimLeft();
			strSelectedExt.TrimRight();
			if(strSelectedExt.GetLength()>0)
			{
				strcpy(pcgTableExt,olLoginCtrl.GetTableExt());
				strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
				
				ogBCD.SetTableExtension(CString(pcgTableExt));
				
			}
		}


	// Register your broadcasts here
	//ogBcHandle.AddTableCommand(CString("AFTHAJ"), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SGM") + pcgTableExt, CString("IRT"), BC_SGM_NEW, false,"RULES");
	ogBcHandle.AddTableCommand(CString("SGM") + pcgTableExt, CString("URT"), BC_SGM_CHANGE, false,"RULES");
	ogBcHandle.AddTableCommand(CString("SGM") + pcgTableExt, CString("DRT"), BC_SGM_DELETE, false,"RULES");
	
	ogBcHandle.AddTableCommand(CString("SGR") + pcgTableExt, CString("IRT"), BC_SGR_NEW, false,"RULES");
	ogBcHandle.AddTableCommand(CString("SGR") + pcgTableExt, CString("URT"), BC_SGR_CHANGE, false,"RULES");
	ogBcHandle.AddTableCommand(CString("SGR") + pcgTableExt, CString("DRT"), BC_SGR_DELETE, false,"RULES");


//	CLoginDlg olLoginDlg;
//	if (olLoginDlg.DoModal() != IDOK)
//        return FALSE;


	// InitialLoadDialog
//    InitialLoad(NULL);
    InitialLoad(&olDummyDlg);

	olDummyDlg.DestroyWindow();

	// should the buttonlist be movable ?
	bgIsButtonListMovable = false;

	// everything ok, we can display the buttonlist
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		
	char pclCaption[256]="";
	strcpy(pclCaption, GetString(IDS_STRING102));
	pogDutyPreferences = new DutyPreferences(IDS_STRING102, NULL, 0);
	pogDutyPreferences->m_psh.dwFlags &= ~(PSP_HASHELP);
	pogDutyPreferences->m_psh.dwFlags |= PSH_NOAPPLYNOW;

	int nResponse = pogDutyPreferences->DoModal();
	if (nResponse == IDOK || IDCANCEL)
	{
		if (pogDutyPreferences)
		{
			pogDutyPreferences->DestroyWindow();
			delete pogDutyPreferences;
			pogDutyPreferences = NULL;
		}
	}


/*
	DutyPreferences dialog(IDS_STRING102, NULL, 0);

	dialog.m_psh.dwFlags &= ~(PSP_HASHELP);
	dialog.m_psh.dwFlags |= PSH_NOAPPLYNOW;
	dialog.DoModal();
*/
//	CButtonListDlg dlg(pomBackGround);
//	m_pMainWnd = &dlg;
//	int nResponse = dlg.DoModal();
//	if (nResponse == IDOK)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with OK
//	}
//	else if (nResponse == IDCANCEL)
//	{
//		// TODO: Place code here to handle when the dialog is
//		//  dismissed with Cancel
//	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

void CRulesApp::OnCancel()
{
	if (pogDutyPreferences)
	{
		pogDutyPreferences->DestroyWindow();
		delete pogDutyPreferences;
		pogDutyPreferences = NULL;
	}
}

void CRulesApp::InitialLoad(CWnd *pParent)
{
/*
CedaACRData.h
CedaACTData.h
CedaALTData.h
CedaAPTData.h
CedaBLTData.h
CedaBsdData.h
CedaCfgData.h
CedaCICData.h
CedaGATData.h
CedaGegData.h
CedaGhpData.h
CedaGhsData.h
CedaGpmData.h
CedaGrmData.h
CedaGrnData.h
CedaHTYData.h
CedaNATData.h
CedaPerData.h
CedaPfcData.h
CedaPrcData.h
CedaPSTData.h
*/
//	ogBasicData.omUserID = CString("Rules");
	ogCicData.SetTableName(CString(CString("CIC") + CString(pcgTableExt)));
	ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
	ogAcrData.SetTableName(CString(CString("ACR") + CString(pcgTableExt)));
	ogActData.SetTableName(CString(CString("ACT") + CString(pcgTableExt)));
	ogAltData.SetTableName(CString(CString("ALT") + CString(pcgTableExt)));
	ogAptData.SetTableName(CString(CString("APT") + CString(pcgTableExt)));
	ogBltData.SetTableName(CString(CString("BLT") + CString(pcgTableExt)));

	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltAloc() == TRUE) {
		ogBlaData.SetTableName(CString(CString("BLA") + CString(pcgTableExt)));
	}

	ogBsdData.SetTableName(CString(CString("BSD") + CString(pcgTableExt)));
	ogGatData.SetTableName(CString(CString("GAT") + CString(pcgTableExt)));
	ogGegData.SetTableName(CString(CString("GEG") + CString(pcgTableExt)));
	ogGhpData.SetTableName(CString(CString("GHP") + CString(pcgTableExt)));
	ogGhsData.SetTableName(CString(CString("GHS") + CString(pcgTableExt)));
	ogGpmData.SetTableName(CString(CString("GPM") + CString(pcgTableExt)));
	ogGrmData.SetTableName(CString(CString("GRM") + CString(pcgTableExt)));
	ogGrnData.SetTableName(CString(CString("GRN") + CString(pcgTableExt)));
	ogHtyData.SetTableName(CString(CString("HTY") + CString(pcgTableExt)));
	ogNatData.SetTableName(CString(CString("NAT") + CString(pcgTableExt)));
	ogPerData.SetTableName(CString(CString("PER") + CString(pcgTableExt)));
	ogPfcData.SetTableName(CString(CString("PFC") + CString(pcgTableExt)));
	ogPrcData.SetTableName(CString(CString("PRC") + CString(pcgTableExt)));
	ogPstData.SetTableName(CString(CString("PST") + CString(pcgTableExt)));

//	ogParData.SetTableName(CString(CString("PAR") + CString(pcgTableExt)));
//	ogValData.SetTableName(CString(CString("VAL") + CString(pcgTableExt)));
//	ogWROData.SetTableName(CString(CString("WRO") + CString(pcgTableExt)));

	pogInitialLoad = new CInitialLoadDlg(pParent);
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING130));
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	bgIsInitialized = false;

	ogBCD.SetObject("GPM");
	int index = ogBCD.GetFieldIndex("GPM","REMA");
	if(index == -1)
	{
		bgDoesFidsRemaExist = false;
	}
	else
	{
		bgDoesFidsRemaExist = true;
	}

	ogGhsData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING131));
	ogGhpData.Read(" WHERE APPL = 'POPS'");
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING132));
	ogGpmData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING133));
	ogPrcData.Read();
//	ogCfgData.ReadCfgData();
//	ogCfgData.SetCfgData();
//	ogCfgData.ReadMonitorSetup();
	ogPfcData.Read();
	ogGegData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING134));
	ogActData.ReadAllACTs("");
	if( CanHandleAcrRegn() )		// 050228 MVy: handle Aircraft Registrations only when configuration key POSRULE_WITH_REGN is set
	{
		pogInitialLoad->SetMessage(LoadStg(IDS_STRING331));		// 050228 MVy: Loading Aircraft Registrations
		ogAcrData.ReadAllACRs("");
	};
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING135));
	ogAptData.ReadAllAPTs();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING136));
	ogAltData.ReadAllALTs();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING137));
	ogPstData.ReadAllPSTs();
	ogGrnData.Read(" WHERE APPL='POPS'");
	ogGrmData.Read();
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING138));
	ogNatData.ReadAllNATs();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING140));
	ogBltData.ReadAllBLTs();
	if( ( (CRulesApp *)AfxGetApp() )->CanHandleBltAloc() == TRUE) {
		ogBlaData.ReadAllBlas();
	}

	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING141));
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING142));
	ogGatData.ReadAllGATs();
	pogInitialLoad->SetProgress(2);
/*
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING143));
	ogHtyData.ReadAllHTYs();
	pogInitialLoad->SetProgress(2);
*/
	//pogInitialLoad->SetMessage("Check-in counters werden geladen");
	//ogCcaData.Read();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING144));
	ogPerData.Read();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING145));
	ogPfcData.Read();
	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING146));
	ogBsdData.Read();

	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING1458));
	ogWROData.Read();

	pogInitialLoad->SetProgress(2);
	pogInitialLoad->SetMessage(LoadStg(IDS_STRING147));


	ogAloData.ReadAloData();
	ogSgrData.Read();
	ogSgmData.Read();

	ogBCD.SetObject("STY", "URNO,SNAM,STYP");
	ogBCD.AddKeyMap("STY", "STYP");
	ogBCD.Read(CString("STY"));
/*
	ogBCD.SetObject("GPM");
	int index = ogBCD.GetFieldIndex("GPM","REMA");
	if(index == -1)
	{
		bgDoesFidsRemaExist = false;
	}
	else
	{
		bgDoesFidsRemaExist = true;
	}
*/
    /////////////////////////////////////////////////////////////////////////////////////
    // add here your cedaXXXdata read methods
    /////////////////////////////////////////////////////////////////////////////////////

		
	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}

}

void CRulesApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	AatHelp::WinHelp(dwData,nCmd);	
}

// 050228 MVy: handle Aircraft Registrations only when configuration key POSRULE_WITH_REGN is set
BOOL CRulesApp::CanHandleAcrRegn()
{
	return m_bHandleAcrRegn ;
};	// CanHandleAcrRegn

BOOL CRulesApp::CanHandleBltFlti()
{
	return m_bBltFlti ;
};	// CanHandleAcrRegn

BOOL CRulesApp::CanHandleBltAloc()
{
	return m_bBltAloc ;
};	// CanHandleAcrRegn

BOOL CRulesApp::CanHandlePrintRules()
{
	return m_bPrintRules;
}


BOOL CRulesApp::CanHandleCenterLine()
{
	return m_bCenterLine;
}

BOOL CRulesApp::CanHandlePosMinMaxPax()
{
	return m_bPosMinMaxPax;
}

BOOL CRulesApp::CanHandleBeltBag()
{
	return m_bBeltBag;
}

BOOL CRulesApp::CanHandleFlno()
{
	return m_bHandleFlno ;
};	// CanHandleAcrRegn



