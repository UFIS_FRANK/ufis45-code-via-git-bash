// CedaPSTData.h

#ifndef __CEDAPSTDATA__
#define __CEDAPSTDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

//@See: CedaPSTData
//@Doc:	A structure for reading Duty data. We read all data from database and put them
//		into this struct and store the data in omData.  

//---------------------------------------------------------------------------------------------------------
struct PSTDATA 
{
	char 	 Acus[3]; 	// ACU Status
	CTime	 Cdat; 		// Erstellungsdatum
	char 	 Fuls[3]; 	// Fuel Pit Status
	char 	 Gpus[3]; 	// GPU Status
	CTime	 Lstu; 		// Datum letzte �nderung
	CTime	 Nafr; 		// Nicht verf�gbar vom
	CTime	 Nato; 		// Nicht verf�gbar bis
	char 	 Pcas[3]; 	// PCA Status
	char 	 Pnam[7]; 	// Positionsname
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Pubk[3]; 	// Pushback Position
	char 	 Taxi[7]; 	// Angeschlossener Taxiway
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato;		// G�ltig bis
	char 	 Resn[42]; 	// Grund f�r die Sperrung
	char 	 Dgss[3]; 	// Andocksystem verf�gbar
	char	 Acgr[1026];//Aircraftgroup
	char	 Posr[2001];//Regeln

	//DataCreated by this class
	int      IsChanged;

	PSTDATA(void)
	{ memset(this,'\0',sizeof(*this));
	 Cdat=-1;Lstu=-1;Nafr=-1;Nato=-1;Vafr=-1;Vato=-1;
	}

}; // end PSTDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

//@Memo: Read and write PST(duty) data
//@See: CedaData, CedaDutyData
/*@Doc:
  Reads and writes PST(duty) data from and to database. Stores PST data in memory and
  provides methods for searching and retrieving PSTs from its internal list. Does
  some data preparations.

  Data of this class has to be updated by CEDA through broadcasts. This procedure and the
  necessary methods will be described later with the description of broadcast handling.

  {\bf CedaPSTData} handles data from the server process. 
  See the specification for table descriptions.*/

class CedaPSTData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Urno field of all loaded PSTs.
    CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omPnamMap;

    //@ManMemo: PSTDATA records read by ReadAllPST().
    CCSPtrArray<PSTDATA> omData;
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf PSTDATA},
      the {\bf pcmTableName} with table name {\bf PSTLSG}, the data members {\bf pcmFieldList}
      contains a list of used fields of {\bf PSTDATA}.
    */
    CedaPSTData();
	~CedaPSTData();
	void Register(void);
	
	void CedaPSTData::Convert();
	void ClearAll(void);
	void SaveAll(void);

    //@ManMemo: Read all PSTs.
    /*@Doc:
      Read all PSTs. 

      This method will clear {\bf omData} and {\bf omKeyMap}. Convert each record
      to a PSTDATA and store it in {\bf omData} and the Urno field in {\bf omKeyMap}.
    */
    bool ReadAllPSTs();
    //@AreMemo: Read all PST-Data corresponding to Where-condition
	bool ReadSpecial(CCSPtrArray<PSTDATA> &ropPst,char *pspWhere);
    //@ManMemo: Add a new PST and store the new data to the Database.
	bool InsertPST(PSTDATA *prpPST,BOOL bpSendDdx = TRUE);
    //@ManMemo: Add data for a new PST to the internal data.
	bool InsertPSTInternal(PSTDATA *prpPST);
	//@ManMemo: Change the data of a special PST
	bool UpdatePST(PSTDATA *prpPST,BOOL bpSendDdx = TRUE);
    //@AreMemo: Change the data of a special PST in the internal data.
	bool UpdatePSTInternal(PSTDATA *prpPST);
    //@ManMemo: Delete a special PST, also in the Database.
	bool DeletePST(long lpUrno);
    //@ManMemo: Delete a special PST only in the internal data.
	bool DeletePSTInternal(PSTDATA *prpPST);
    //@AreMemo: Selects all PSTs with a special Urno.
	PSTDATA  *GetPSTByUrno(long lpUrno);
	PSTDATA  *GetPstByPnam(char *pspPnam);
    //@ManMemo: Search a PST with a special Key.
	CString PSTExists(PSTDATA *prpPST);
    //@AreMemo: Search a Pnam
	bool ExistPnam(char* popPnam);
	//@ManMemo: Writes a special PST to the Database.
	bool SavePST(PSTDATA *prpPST);
    //@ManMemo: Contains the valid data for all existing PSTs.
	char pcmPSTFieldList[2048];
    //@ManMemo: Handle Broadcasts for PSTs.
	void ProcessPSTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	long GetUrnoByPst(char *pspPst);
	// Private methods
private:
    void PreparePSTData(PSTDATA *prpPSTData);
};


extern CedaPSTData ogPstData;

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAPSTDATA__
