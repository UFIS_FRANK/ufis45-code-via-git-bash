// CedaACTData.cpp
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaACTData.h>

// Local function prototype
static void ProcessACTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------
CedaACTData::CedaACTData()
{
    // Create an array of CEDARECINFO for ACTDATA
	BEGIN_CEDARECINFO(ACTDATA,ACTDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Act3,"ACT3")
		FIELD_CHAR_TRIM	(Act5,"ACT5")
		FIELD_CHAR_TRIM	(Acti,"ACTI")
		FIELD_CHAR_TRIM	(Acfn,"ACFN")
		FIELD_CHAR_TRIM	(Acws,"ACWS")
		FIELD_CHAR_TRIM	(Acle,"ACLE")
		FIELD_CHAR_TRIM	(Ache,"ACHE")
		FIELD_CHAR_TRIM	(Seat,"SEAT")
		FIELD_CHAR_TRIM	(Enty,"ENTY")
		FIELD_CHAR_TRIM	(Enno,"ENNO")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Nads,"NADS")
	END_CEDARECINFO //(ACTDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(ACTDataRecInfo)/sizeof(ACTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ACTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ACT");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmACTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,SEAT,ENTY,ENNO,VAFR,VATO,NADS");
	pcmFieldList = pcmACTFieldList;
	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}


void CedaACTData::Register(void)
{
	ogDdx.Register((void *)this,BC_ACT_CHANGE,CString("ACTDATA"), CString("ACT-changed"),ProcessACTCf);
	ogDdx.Register((void *)this,BC_ACT_DELETE,CString("ACTDATA"), CString("ACT-deleted"),ProcessACTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaACTData::~CedaACTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaACTData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
	omAct3Map.RemoveAll();
	omAct5Map.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaACTData::ReadAllACTs(char *pspWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omAct3Map.RemoveAll();
	omAct5Map.RemoveAll();
    omData.DeleteAll();
	if(strcmp(pspWhere, "") != 0)
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pspWhere);
	}
	else
	{
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	}
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ACTDATA *prpACT = new ACTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpACT)) == true)
		{
			prpACT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpACT);//Update omData
			omUrnoMap.SetAt((void *)prpACT->Urno,prpACT);
			omAct3Map.SetAt(prpACT->Act3,prpACT);
			omAct5Map.SetAt(prpACT->Act5,prpACT);
		}
		else
		{
			delete prpACT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaACTData::InsertACT(ACTDATA *prpACT,BOOL bpSendDdx)
{
	ACTDATA *prlACT = new ACTDATA;
	memcpy(prlACT,prpACT,sizeof(ACTDATA));
	prlACT->IsChanged = DATA_NEW;
	SaveACT(prlACT); //Update Database
	InsertACTInternal(prlACT);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaACTData::InsertACTInternal(ACTDATA *prpACT)
{
	//PrepareACTData(prpACT);
	ogDdx.DataChanged((void *)this, ACT_CHANGE,(void *)prpACT ); //Update Viewer
	omData.Add(prpACT);//Update omData
	omUrnoMap.SetAt((void *)prpACT->Urno,prpACT);
	omAct3Map.SetAt(prpACT->Act3,prpACT);
	omAct5Map.SetAt(prpACT->Act5,prpACT);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaACTData::DeleteACT(long lpUrno)
{
	bool olRc = true;
	ACTDATA *prlACT = GetACTByUrno(lpUrno);
	if (prlACT != NULL)
	{
		prlACT->IsChanged = DATA_DELETED;
		olRc = SaveACT(prlACT); //Update Database
		DeleteACTInternal(prlACT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaACTData::DeleteACTInternal(ACTDATA *prpACT)
{
	ogDdx.DataChanged((void *)this,ACT_DELETE,(void *)prpACT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpACT->Urno);
	omAct3Map.RemoveKey(prpACT->Act3);
	omAct5Map.RemoveKey(prpACT->Act5);
	int ilACTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilACTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpACT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaACTData::PrepareACTData(ACTDATA *prpACT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaACTData::UpdateACT(ACTDATA *prpACT,BOOL bpSendDdx)
{
	if (GetACTByUrno(prpACT->Urno) != NULL)
	{
		if (prpACT->IsChanged == DATA_UNCHANGED)
		{
			prpACT->IsChanged = DATA_CHANGED;
		}
		SaveACT(prpACT); //Update Database
		UpdateACTInternal(prpACT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaACTData::UpdateACTInternal(ACTDATA *prpACT)
{
	ACTDATA *prlACT = GetACTByUrno(prpACT->Urno);
	if (prlACT != NULL)
	{
		omAct3Map.RemoveKey(prlACT->Act3);
		omAct5Map.RemoveKey(prlACT->Act5);
		*prlACT = *prpACT; //Update omData
		omAct3Map.SetAt(prlACT->Act3,prlACT);
		omAct5Map.SetAt(prlACT->Act5,prlACT);
		ogDdx.DataChanged((void *)this,ACT_CHANGE,(void *)prlACT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ACTDATA *CedaACTData::GetACTByUrno(long lpUrno)
{
	ACTDATA  *prlACT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlACT) == TRUE)
	{
		return prlACT;
	}
	return NULL;
}

//--READSPECIALACTDATA-------------------------------------------------------------------------------------

ACTDATA * CedaACTData::GetActByAct3(char *pcpAct3)
{
	POSITION rlPos;
	bool blFound = false;
	ACTDATA *prlAct;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlAct);
		if(prlAct != NULL)
		{
			if(strcmp(prlAct->Act3, pcpAct3) == 0)
			{
				blFound = true;
			}
		}
	}
	if(blFound == true)
	{
		return prlAct;
	}
	return NULL;
}

ACTDATA *CedaACTData::GetActByAct5(char *pcpAct5)
{
	POSITION rlPos;
	bool blFound = false;
	ACTDATA *prlAct;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlAct);
		if(prlAct != NULL)
		{
			if(strcmp(prlAct->Act5, pcpAct5) == 0)
			{
				blFound = true;
			}
		}
	}
	if(blFound == true)
	{
		return prlAct;
	}
	return NULL;
}



bool CedaACTData::ReadSpecial(CCSPtrArray<ACTDATA> &ropAct,char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		ilRc = CedaAction("RT");
		if (ilRc != true)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		ilRc = CedaAction("RT",pspWhere);
		if (ilRc != true)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ACTDATA *prpAct = new ACTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpAct)) == true)
		{
			ropAct.Add(prpAct);
		}
		else
		{
			delete prpAct;
		}
	}
    return true;
}

//--EXIST-I-----------------------------------------------------------------------------------------------
long CedaACTData::GetUrnoByAct(char *pspAct)
{
	ACTDATA  *prlACT;
	if(strcmp(pspAct, "") == 0)
	{
		return 0;
	}

	if (omAct3Map.Lookup((LPCSTR)pspAct,(void *&)prlACT) == TRUE)
	{
		return prlACT->Urno;
	}

	if (omAct5Map.Lookup((LPCSTR)pspAct,(void *&)prlACT) == TRUE)
	{
		return prlACT->Urno;
	}
	return 0;
}

CString CedaACTData::ACTExists(ACTDATA *prpACT)
{
	CString olField = "";
	ACTDATA  *prlACT;

	if (omAct3Map.Lookup((LPCSTR)prpACT->Act3,(void *&)prlACT) == TRUE)
	{
		if(prlACT->Urno != prpACT->Urno)
		{
			olField += GetString(IDS_STRING189);
		}
	}

	if (omAct5Map.Lookup((LPCSTR)prpACT->Act5,(void *&)prlACT) == TRUE)
	{
		if(prlACT->Urno != prpACT->Urno)
		{
			olField += GetString(IDS_STRING190);
		}
	}

	return olField;
}

//--EXIST-II----------------------------------------------------------------------------------------------

bool CedaACTData::ExistsSpecial(char *popAct3,char *popAct5)
{
	bool ilRc = true;
	char pclSelection[512];
	sprintf(pclSelection,"WHERE ACT3='%s'",popAct3);
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"),pclSelection);
	if (ilRc != true)
	{
		return false;
	}
	ACTDATA rlACT;
	if ((GetBufferRecord(0,&rlACT)) == true)
	{
		strcpy(popAct5,rlACT.Act5);
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaACTData::SaveACT(ACTDATA *prpACT)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpACT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpACT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpACT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpACT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpACT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpACT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpACT->Urno);
		olRc = CedaAction("DRT",pclSelection);
		//prpACT->IsChanged = DATA_UNCHANGED;
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}

    return true;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessACTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_ACT_CHANGE :
	case BC_ACT_DELETE :
		((CedaACTData *)popInstance)->ProcessACTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaACTData::ProcessACTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlACTData;
	long llUrno;
	prlACTData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlACTData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlACTData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	ACTDATA *prlACT;
	if(ipDDXType == BC_ACT_CHANGE)
	{
		if((prlACT = GetACTByUrno(llUrno)) != NULL)
		{
			GetRecordFromItemList(prlACT,prlACTData->Fields,prlACTData->Data);
			UpdateACTInternal(prlACT);
		}
		else
		{
			prlACT = new ACTDATA;
			GetRecordFromItemList(prlACT,prlACTData->Fields,prlACTData->Data);
			InsertACTInternal(prlACT);
		}
	}
	if(ipDDXType == BC_ACT_DELETE)
	{
		prlACT = GetACTByUrno(llUrno);
		if (prlACT != NULL)
		{
			DeleteACTInternal(prlACT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
