#if !defined(AFX_GROUPSELDLG_H__27FC8383_C5A9_4BE3_BBD1_19A4A04EF50D__INCLUDED_)
#define AFX_GROUPSELDLG_H__27FC8383_C5A9_4BE3_BBD1_19A4A04EF50D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupSelDlg.h : header file
//
#include <resrc1.h>
/////////////////////////////////////////////////////////////////////////////
// CGroupSelDlg dialog

class CGroupSelDlg : public CDialog
{
// Construction
public:
	CGroupSelDlg(CWnd* pParent, CString& opGrpStr);   // standard constructor

	CString omGrpStr;
// Dialog Data
	//{{AFX_DATA(CGroupSelDlg)
	enum { IDD = IDD_GROUP_SELECT };
		CListBox	m_GroupNameList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupSelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGroupSelDlg)
	virtual BOOL OnInitDialog();
	void OnOk();
//	void OnWithoutPrefix();
	void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPSELDLG_H__27FC8383_C5A9_4BE3_BBD1_19A4A04EF50D__INCLUDED_)
