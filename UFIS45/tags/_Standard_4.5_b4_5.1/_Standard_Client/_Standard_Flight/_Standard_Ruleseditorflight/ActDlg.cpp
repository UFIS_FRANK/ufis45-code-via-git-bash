// ActDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CCSEdit.h"
#include "CCSGlobl.h"
#include "CedaActData.h"
#include "ActDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ActDlg dialog
static int CompareToActl(const ACTDATA **e1, const ACTDATA **e2);


ActDlg::ActDlg(CWnd* pParent, char *pspActl)
	: CDialog(ActDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ActDlg)
	//}}AFX_DATA_INIT
	if(strcmp(pspActl, "") != 0)
	{
		char pclWhere[100];
		sprintf(pclWhere, " WHERE ACT3 LIKE '%s%%' OR ACT5 LIKE '%s%%'", pspActl, pspActl);
		ogActData.ReadSpecial(omActData,pclWhere);
	}
	if(omActData.GetSize() == 0)
	{
		ogActData.ReadSpecial(omActData,NULL);
	}
	omActData.Sort(CompareToActl);

}

ActDlg::~ActDlg()
{
	omActData.DeleteAll();
}
void ActDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ActDlg)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ActDlg, CDialog)
	//{{AFX_MSG_MAP(ActDlg)
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ActDlg message handlers

BOOL ActDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	char pclList[500];
	m_List.SetFont(&ogCourier_Regular_8);
	int ilCount = omActData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		sprintf(pclList, "%3s %5s %4s  %-30s", 
			     omActData[i].Act3, omActData[i].Act5, 
				  omActData[i].Acti, omActData[i].Acfn);
		m_List.AddString(pclList);
		ACTDATA *prlAct = &omActData.GetAt(i);
		m_List.SetItemDataPtr( i, (void*)prlAct);
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));	
	return TRUE;
}

void ActDlg::OnDblclkList1() 
{
	OnOK();	
}

void ActDlg::OnOK() 
{
	int ilIndex = m_List.GetCurSel();
	//ACTDATA *prlAlt;
	if(ilIndex != LB_ERR)
	{
		omAircraft = CString(omActData[ilIndex].Act5);
	}
	CDialog::OnOK();
}

static int CompareToActl(const ACTDATA **e1, const ACTDATA **e2)
{
//	return (int)(strcmp((**e1).Act3,(**e2).Act3));

	if( strcmp((**e1).Act3 ,(**e2).Act3 ) != 0)
	{
		return ( strcmp((**e1).Act3 , (**e2).Act3) );
	}

	if( strcmp((**e1).Act5 ,(**e2).Act5 ) != 0)
	{
		return ( strcmp((**e1).Act5 , (**e2).Act5) ); 
	}

	return 1;
}
