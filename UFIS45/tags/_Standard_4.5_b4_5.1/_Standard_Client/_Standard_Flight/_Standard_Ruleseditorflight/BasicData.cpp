// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <CedaParData.h>
#include <CedaValData.h>
#include <CedaAloData.h>
#include <CedaSgrData.h>
#include <CedaSgmData.h>
#include <CedaPstData.h>
#include <CedaBltData.h>
#include <CedaGatData.h>
#include <CedaWroData.h>
#include <CedaGrnData.h>

#define N_URNOS_AT_ONCE 500

static int CompareArray( const CString **e1, const CString **e2);

int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}
CString LoadStg(UINT nID)
{
	CString olString = "";
	olString.LoadString(nID);
	return olString;
}


bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = olData[i];
				return true; 
		}
	}
	return false;
}

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = DBStringToDateTime(olData[i]);
				return true; 
		}
	}
	return false;
}


int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}


CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}

CString SortItemList(CString opSubString, char cpTrenner)
{
	CString *polText;
	CCSPtrArray<CString> olArray;

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			polText = new CString;
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				*polText = opSubString;
			}
			else
			{
				*polText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			olArray.Add(polText);
		}
	}

	CString olSortedString;
	olArray.Sort(CompareArray);
	for(int i=0; i<olArray.GetSize(); i++)
	{
		olSortedString += olArray[i] + cpTrenner;
	}
	
	olArray.DeleteAll();
	return olSortedString.Left(olSortedString.GetLength()-1);
}


CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(pcgAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
	char pclDaysToRead[10]="";
	GetPrivateProfileString(pcgAppName, "DAYSTOREAD", "1",
      pclDaysToRead, sizeof pclDaysToRead, pclConfigPath);
}



CBasicData::~CBasicData(void)
{
}


long CBasicData::GetNextUrno(void)
{
	bool	olRc = true;
	long				llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(500);
	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);

		if ( (llNextUrno != 0L) && (olRc == true) )
		{
			return(llNextUrno);
		}
	}
	::MessageBox(NULL,GetString(IDS_STRING739), GetString(IDS_STRING161) ,MB_OK);
	return -1;	
}

int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner)
{

	CString olSubStr;

	popStrArray->RemoveAll();
	
	CStringArray olStrArray;

	int ilCount = ExtractItemList(opString, &olStrArray);
	int ilSubCount = 0;

	for(int i = 0; i < ilCount; i++)
	{
		if(ilSubCount >= ipMaxItem)
		{
			if(!olSubStr.IsEmpty())
				olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
			popStrArray->Add(olSubStr);
			ilSubCount = 0;
			olSubStr = "";
		}
		ilSubCount++;
		olSubStr = olSubStr + cpTrenner + olStrArray[i];
	}
	if(!olSubStr.IsEmpty())
	{
		olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
		popStrArray->Add(olSubStr);
	}
	
	return	popStrArray->GetSize();
}


bool CBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];

	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

	ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= N_URNOS_AT_ONCE) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}


int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS234");//opWsName;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}


bool CBasicData::GetWindowPosition(CRect& rlPos,CString olMonitor)
{

	int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{
		if (ogCfgData.rmUserSetup.RESO[0] == '1')
		{
			if (ogCfgData.rmUserSetup.RESO[1] == '0')
			{
				XResolution = 1024;
				YResolution = 768;
			}
		}
		else
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	}
  

	int ilMonitor;
	if (olMonitor[0] == 'L')
		ilMonitor = 0;
	if (olMonitor[0] == 'M')
		ilMonitor = 1;
	if (olMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	return true;
}
int CBasicData::GetUtcDifference(void)
{

	static int ilUtcDifference = 1;
	static BOOL blIsInitialized = FALSE;

	if (blIsInitialized == FALSE)
	{
		struct tm *_tm;
		time_t    now;
		
		int hour_gm,hour_local;
		blIsInitialized = TRUE;

		now = time(NULL);
		_tm = (struct tm *)gmtime(&now);
		hour_gm = _tm->tm_hour;
		_tm = (struct tm *)localtime(&now);
		hour_local = _tm->tm_hour;
		if (hour_gm > hour_local)
		{
			ilUtcDifference = ((hour_local+24-hour_gm)*3600);
		}
		else
		{
			ilUtcDifference = ((hour_local-hour_gm)*3600);
		}
	}

	return ilUtcDifference;

}
void CBasicData::SetLocalDiff()
{
	int		ilTdi1;
	int		ilTdi2;
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime olCurr;
	RecordSet *prlRecord;	

	CCSPtrArray<RecordSet> olData;

	CString olWhere = CString("WHERE APC3 = '") + CString(pcgHome) + CString("'");

	if(ogBCD.ReadSpecial( "APT", "TICH,TDI1,TDI2", olWhere, olData))
	{
		if(olData.GetSize() > 0)
		{
			prlRecord = &olData[0];
		
			olTich = DBStringToDateTime((*prlRecord)[0]);
			omTich = olTich;
			ilTdi1 = atoi((*prlRecord)[1]);
			ilTdi2 = atoi((*prlRecord)[2]);
			
			ilHour = ilTdi1 / 60;
			ilMin  = ilTdi1 % 60;
			
			CTimeSpan	olTdi1(0,ilHour ,ilMin ,0);

			ilHour = ilTdi2 / 60;
			ilMin  = ilTdi2 % 60;
			
			CTimeSpan	olTdi2(0,ilHour ,ilMin ,0);

			olCurr = CTime::GetCurrentTime();

			if(olTich != TIMENULL) 
			{
				omLocalDiff1 = olTdi1;
				omLocalDiff2 = olTdi2;
			}
		}
	}
	olData.DeleteAll();
}


void CBasicData::LocalToUtc(CTime &opTime)
{
	if(opTime != TIMENULL)
	{
		if(opTime < omTich)
		{
			opTime -= omLocalDiff1;
		}
		else
		{
			opTime -= omLocalDiff2;
		}
	}

}

void CBasicData::UtcToLocal(CTime &opTime)
{
	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);

	if(opTime != TIMENULL)
	{
		if(opTime < olTichUtc)
		{
			opTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
		}
	}
}

bool CBasicData::IsGatPosEnabled()
{
	static int ilIsGatPosEnabled = -1;
	if (ilIsGatPosEnabled == -1)
	{
		ogParData.Read();
		ogValData.Read();
		CString olValue = ogParData.GetParValue("GLOBAL","ID_GAT_POS");
		if (olValue.CompareNoCase("Y") == 0)
			ilIsGatPosEnabled = 1;		
		else
			ilIsGatPosEnabled = 0;		
	}

	return ilIsGatPosEnabled != 0;
}

static int CompareArray( const CString **e1, const CString **e2)
{
	return (strcmp((**e1),(**e2)));
}


int CBasicData::GetDependingPositions(long lpPosUrno,CStringArray& ropPositions)
{
	ALODATA *polAloc = ogAloData.GetAloByName("POSPOS");
	if (polAloc == NULL)
		return 0;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpPosUrno);
	if (polSgr == NULL)
		return 0;

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		PSTDATA *polPst = ogPstData.GetPSTByUrno(olSgmList[i].Uval);
		if (polPst)
			ropPositions.Add(polPst->Pnam);
	}
	
	return ropPositions.GetSize();
}

CString CBasicData::GetDependingAircraftGroups(long lpFromPosUrno,long lpToPosUrno,const CString& ropACFrom,const CString& ropACTo)
{
	ALODATA *polAloc = ogAloData.GetAloByName("POSPOS");
	if (polAloc == NULL)
		return " ";

	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpFromPosUrno);
	if (polSgr == NULL)
		return " ";

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgmUrno = 0;
	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		PSTDATA *polPst = ogPstData.GetPSTByUrno(olSgmList[i].Uval);
		if (polPst && polPst->Urno == lpToPosUrno)
		{
			llSgmUrno = olSgmList[i].Urno;
			break;
		}
	}
	olSgmList.DeleteAll();

	if (llSgmUrno == 0)
		return " ";

	polAloc = ogAloData.GetAloByName("POSACT");
	if (!polAloc)
		return " ";

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgmUrno);
	if (polSgr == NULL)
		return " ";

	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	llSgmUrno = 0;
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACFrom)
		{
			llSgmUrno = olSgmList[i].Urno;
			break;
		}
	}
	olSgmList.DeleteAll();

	if (llSgmUrno == 0)
		return " ";

	polAloc = ogAloData.GetAloByName("ACTACT");
	if (!polAloc)
		return " ";

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgmUrno);
	if (polSgr == NULL)
		return " ";

	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACTo)
		{
			llSgmUrno = olSgmList[i].Urno;
			if (strlen(olSgmList[i].Prfl) == 0)
				return " ";
			else
				return olSgmList[i].Prfl;
		}
	}
	olSgmList.DeleteAll();

	return " ";
}

bool CBasicData::SetDependingAircraftGroups(long lpFromPosUrno,long lpToPosUrno,const CString& ropACFrom,const CString& ropACTo,const CString& ropValue)
{
	ALODATA *polAloc = ogAloData.GetAloByName("POSPOS");
	if (polAloc == NULL)
		return false;

	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpFromPosUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"RULE_AFT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = lpFromPosUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,ogBasicData.omUserID);
//		polSgr->Useu =

		if (!ogSgrData.Insert(polSgr,TRUE))
			return false;
	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgmUrno = 0;
	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		PSTDATA *polPst = ogPstData.GetPSTByUrno(olSgmList[i].Uval);
		if (polPst && polPst->Urno == lpToPosUrno)
		{
			llSgmUrno = olSgmList[i].Urno;
			break;
		}
	}
	olSgmList.DeleteAll();

	if (llSgmUrno == 0)
	{
		SGMDATA *polSgm = new SGMDATA;
		polSgm->IsChanged = DATA_NEW;
		polSgm->Cdat = CTime::GetCurrentTime();
		strcpy(polSgm->Hopo,pcgHome);
//		polSgm->Prfl =
		strncpy(polSgm->Tabn,polAloc->Reft,3);
		polSgm->Tabn[3] = '\0';
		polSgm->Ugty = polAloc->Urno;
		polSgm->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgm->Usec,ogBasicData.omUserID);
//		polSgm->Useu =
		polSgm->Usgr = polSgr->Urno;
		polSgm->Uval = lpToPosUrno;
		polSgm->Valu = lpFromPosUrno;
		if (!ogSgmData.Insert(polSgm,true))
			return false;
		else
			llSgmUrno = polSgm->Urno;
	}

	polAloc = ogAloData.GetAloByName("POSACT");
	if (!polAloc)
		return false;

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgmUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"RULE_AFT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = llSgmUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,ogBasicData.omUserID);
//		polSgr->Useu =

		if (!ogSgrData.Insert(polSgr,TRUE))
			return false;

	}

	olSgmList.RemoveAll();
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgm2Urno = 0;
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACFrom)
		{
			llSgm2Urno = olSgmList[i].Urno;
			break;
		}
	}

	if (llSgm2Urno == 0)
	{
		CCSPtrArray<GRNDATA> olAircraftGroups;
		ogGrnData.GetGrnsByTabn(olAircraftGroups,"ACTTAB","POPS");
		for (i = 0; i < olAircraftGroups.GetSize(); i++)
		{
			if (ropACFrom == olAircraftGroups[i].Grpn)
			{
				SGMDATA *polSgm = new SGMDATA;
				polSgm->IsChanged = DATA_NEW;
				polSgm->Cdat = CTime::GetCurrentTime();
				strcpy(polSgm->Hopo,pcgHome);
		//		polSgm->Prfl =
				strncpy(polSgm->Tabn,polAloc->Reft,3);
				polSgm->Tabn[3] = '\0';
				polSgm->Ugty = polAloc->Urno;
				polSgm->Urno = ogBasicData.GetNextUrno();
				strcpy(polSgm->Usec,ogBasicData.omUserID);
		//		polSgm->Useu =
				polSgm->Usgr = polSgr->Urno;
				polSgm->Uval = olAircraftGroups[i].Urno;
				polSgm->Valu = llSgmUrno;
				if (!ogSgmData.Insert(polSgm,true))
					return false;
				else
				{
					llSgm2Urno = polSgm->Urno;
					break;
				}
			}
		}
		olAircraftGroups.DeleteAll();
	}

	polAloc = ogAloData.GetAloByName("ACTACT");
	if (polAloc == NULL)
		return false;

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgm2Urno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"RULE_AFT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = llSgm2Urno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,ogBasicData.omUserID);
//		polSgr->Useu =

		if (!ogSgrData.Insert(polSgr,TRUE))
			return false;

	}

	olSgmList.DeleteAll();
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgm3Urno = 0;
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACTo)
		{
			llSgm3Urno = olSgmList[i].Urno;
			strcpy(olSgmList[i].Prfl,ropValue);
			return ogSgmData.Update(&olSgmList[i],TRUE);
		}
	}
	olSgmList.DeleteAll();

	CCSPtrArray<GRNDATA> olAircraftGroups;
	ogGrnData.GetGrnsByTabn(olAircraftGroups,"ACTTAB","POPS");
	for (i = 0; i < olAircraftGroups.GetSize(); i++)
	{
		if (ropACTo == olAircraftGroups[i].Grpn)
		{
			SGMDATA *polSgm = new SGMDATA;
			polSgm->IsChanged = DATA_NEW;
			polSgm->Cdat = CTime::GetCurrentTime();
			strcpy(polSgm->Hopo,pcgHome);
			strcpy(polSgm->Prfl,ropValue);
			strncpy(polSgm->Tabn,polAloc->Reft,3);
			polSgm->Tabn[3] = '\0';
			polSgm->Ugty = polAloc->Urno;
			polSgm->Urno = ogBasicData.GetNextUrno();
			strcpy(polSgm->Usec,ogBasicData.omUserID);
		//	polSgm->Useu =
			polSgm->Usgr = polSgr->Urno;
			polSgm->Uval = olAircraftGroups[i].Urno;
			polSgm->Valu = llSgm2Urno;
			if (!ogSgmData.Insert(polSgm,true))
				return false;
			else
				return true;
		}
	}
	olAircraftGroups.DeleteAll();

	return false;
}

//###
CString CBasicData::GetDependingXXXGroups(long lpFromPosUrno,long lpToPosUrno,const CString& ropACFrom,const CString& ropACTo, const CString& ropAlo, const CString& ropGrp)
{
	CString olName = ropAlo + ropAlo;
	ALODATA *polAloc = ogAloData.GetAloByName(olName);
	if (polAloc == NULL)
		return " ";

	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpFromPosUrno);
	if (polSgr == NULL)
		return " ";

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgmUrno = 0;
	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		if (ropAlo == "POS")
		{
			PSTDATA *polPst = ogPstData.GetPSTByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else if (ropAlo == "GAT")
		{
			GATDATA *polPst = ogGatData.GetGATByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else if (ropAlo == "BLT")
		{
			BLTDATA *polPst = ogBltData.GetBLTByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else if (ropAlo == "WRO")
		{
			WRODATA *polPst = ogWROData.GetWROByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else
		{
			PSTDATA *polPst = ogPstData.GetPSTByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
	}
	olSgmList.DeleteAll();

	if (llSgmUrno == 0)
		return " ";

	olName = ropAlo + ropGrp;
	polAloc = ogAloData.GetAloByName(olName);
	if (!polAloc)
		return " ";

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgmUrno);
	if (polSgr == NULL)
		return " ";

	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	llSgmUrno = 0;
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACFrom)
		{
			llSgmUrno = olSgmList[i].Urno;
			break;
		}
	}
	olSgmList.DeleteAll();

	if (llSgmUrno == 0)
		return " ";

	olName = ropGrp + ropGrp;
	polAloc = ogAloData.GetAloByName(olName);
	if (!polAloc)
		return " ";

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgmUrno);
	if (polSgr == NULL)
		return " ";

	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACTo)
		{
			llSgmUrno = olSgmList[i].Urno;
			if (strlen(olSgmList[i].Prfl) == 0)
				return " ";
			else
				return olSgmList[i].Prfl;
		}
	}
	olSgmList.DeleteAll();

	return " ";
}

bool CBasicData::SetDependingXXXGroups(long lpFromPosUrno,long lpToPosUrno,const CString& ropACFrom,const CString& ropACTo,const CString& ropValue, const CString& ropAlo, const CString& ropGrp)
{
	CString olName = ropAlo + ropAlo;
	ALODATA *polAloc = ogAloData.GetAloByName(olName);
	if (polAloc == NULL)
		return false;

	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpFromPosUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"RULE_AFT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = lpFromPosUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,ogBasicData.omUserID);
//		polSgr->Useu =

		if (!ogSgrData.Insert(polSgr,TRUE))
			return false;
	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgmUrno = 0;
	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		if (ropAlo == "POS")
		{
			PSTDATA *polPst = ogPstData.GetPSTByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else if (ropAlo == "GAT")
		{
			GATDATA *polPst = ogGatData.GetGATByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else if (ropAlo == "BLT")
		{
			BLTDATA *polPst = ogBltData.GetBLTByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else if (ropAlo == "WRO")
		{
			WRODATA *polPst = ogWROData.GetWROByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
		else
		{
			PSTDATA *polPst = ogPstData.GetPSTByUrno(olSgmList[i].Uval);
			if (polPst && polPst->Urno == lpToPosUrno)
			{
				llSgmUrno = olSgmList[i].Urno;
				break;
			}
		}
	}
	olSgmList.DeleteAll();

	if (llSgmUrno == 0)
	{
		SGMDATA *polSgm = new SGMDATA;
		polSgm->IsChanged = DATA_NEW;
		polSgm->Cdat = CTime::GetCurrentTime();
		strcpy(polSgm->Hopo,pcgHome);
//		polSgm->Prfl =
		strncpy(polSgm->Tabn,polAloc->Reft,3);
		polSgm->Tabn[3] = '\0';
		polSgm->Ugty = polAloc->Urno;
		polSgm->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgm->Usec,ogBasicData.omUserID);
//		polSgm->Useu =
		polSgm->Usgr = polSgr->Urno;
		polSgm->Uval = lpToPosUrno;
		polSgm->Valu = lpFromPosUrno;
		if (!ogSgmData.Insert(polSgm,true))
			return false;
		else
			llSgmUrno = polSgm->Urno;
	}

	olName = ropAlo + ropGrp;
	polAloc = ogAloData.GetAloByName(olName);
	if (!polAloc)
		return false;

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgmUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"RULE_AFT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = llSgmUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,ogBasicData.omUserID);
//		polSgr->Useu =

		if (!ogSgrData.Insert(polSgr,TRUE))
			return false;

	}

	olSgmList.RemoveAll();
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgm2Urno = 0;
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACFrom)
		{
			llSgm2Urno = olSgmList[i].Urno;
			break;
		}
	}

	if (llSgm2Urno == 0)
	{
		olName = ropGrp + CString("TAB");
		char pclName[10];
		sprintf(pclName,"%s",olName);
		CCSPtrArray<GRNDATA> olAircraftGroups;
		ogGrnData.GetGrnsByTabn(olAircraftGroups,pclName,"POPS");
		for (i = 0; i < olAircraftGroups.GetSize(); i++)
		{
			if (ropACFrom == olAircraftGroups[i].Grpn)
			{
				SGMDATA *polSgm = new SGMDATA;
				polSgm->IsChanged = DATA_NEW;
				polSgm->Cdat = CTime::GetCurrentTime();
				strcpy(polSgm->Hopo,pcgHome);
		//		polSgm->Prfl =
				strncpy(polSgm->Tabn,polAloc->Reft,3);
				polSgm->Tabn[3] = '\0';
				polSgm->Ugty = polAloc->Urno;
				polSgm->Urno = ogBasicData.GetNextUrno();
				strcpy(polSgm->Usec,ogBasicData.omUserID);
		//		polSgm->Useu =
				polSgm->Usgr = polSgr->Urno;
				polSgm->Uval = olAircraftGroups[i].Urno;
				polSgm->Valu = llSgmUrno;
				if (!ogSgmData.Insert(polSgm,true))
					return false;
				else
				{
					llSgm2Urno = polSgm->Urno;
					break;
				}
			}
		}
		olAircraftGroups.DeleteAll();
	}

	olName = ropGrp + ropGrp;
	polAloc = ogAloData.GetAloByName(olName);
	if (polAloc == NULL)
		return false;

	polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,llSgm2Urno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"RULE_AFT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = llSgm2Urno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,ogBasicData.omUserID);
//		polSgr->Useu =

		if (!ogSgrData.Insert(polSgr,TRUE))
			return false;

	}

	olSgmList.DeleteAll();
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	long llSgm3Urno = 0;
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GRNDATA *polGrn = ogGrnData.GetGrnByUrno(olSgmList[i].Uval);
		if (polGrn && polGrn->Grpn == ropACTo)
		{
			llSgm3Urno = olSgmList[i].Urno;
			strcpy(olSgmList[i].Prfl,ropValue);
			return ogSgmData.Update(&olSgmList[i],TRUE);
		}
	}
	olSgmList.DeleteAll();

	olName = ropGrp + CString("TAB");
	char pclName[10];
	sprintf(pclName,"%s",olName);
	CCSPtrArray<GRNDATA> olAircraftGroups;
	ogGrnData.GetGrnsByTabn(olAircraftGroups,pclName,"POPS");
	for (i = 0; i < olAircraftGroups.GetSize(); i++)
	{
		if (ropACTo == olAircraftGroups[i].Grpn)
		{
			SGMDATA *polSgm = new SGMDATA;
			polSgm->IsChanged = DATA_NEW;
			polSgm->Cdat = CTime::GetCurrentTime();
			strcpy(polSgm->Hopo,pcgHome);
			strcpy(polSgm->Prfl,ropValue);
			strncpy(polSgm->Tabn,polAloc->Reft,3);
			polSgm->Tabn[3] = '\0';
			polSgm->Ugty = polAloc->Urno;
			polSgm->Urno = ogBasicData.GetNextUrno();
			strcpy(polSgm->Usec,ogBasicData.omUserID);
		//	polSgm->Useu =
			polSgm->Usgr = polSgr->Urno;
			polSgm->Uval = olAircraftGroups[i].Urno;
			polSgm->Valu = llSgm2Urno;
			if (!ogSgmData.Insert(polSgm,true))
				return false;
			else
				return true;
		}
	}
	olAircraftGroups.DeleteAll();

	return false;
}