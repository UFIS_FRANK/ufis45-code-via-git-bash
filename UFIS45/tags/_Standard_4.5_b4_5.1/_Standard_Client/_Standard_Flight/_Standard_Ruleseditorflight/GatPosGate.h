#if !defined(AFX_GATPOSGATE_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_GATPOSGATE_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosGate.h : header file
//
//#include "GxGridTab.h"
#include "CcsTable.h"
#include "GatRulesViewer.h"
#include "resource.h"
#include "resrc1.h"
/////////////////////////////////////////////////////////////////////////////
// GatPosGate dialog


class GatPosGate : public CPropertyPage
{
	DECLARE_DYNCREATE(GatPosGate)

// Construction
public:
	GatPosGate();
	~GatPosGate();

// Dialog Data
	//{{AFX_DATA(GatPosGate)
	enum { IDD = IDD_RULES_GATE };
	CStatic	m_GateListTable;
	//}}AFX_DATA

	CCSTable *pomGatList;

	GatRulesViewer omGatRulesViewer;

	CUIntArray omGatUrnos;

	void MakeGatLineData(GATDATA *popGat, CStringArray &ropArray);
	void GatPosGate::UpdateView();
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GatPosGate)
	public:
	virtual BOOL OnKillActive();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GatPosGate)
	virtual BOOL OnInitDialog();
	afx_msg LONG OnGridDblClk(UINT wParam, LONG lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	void OnPrintGates();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSGATE_H__BDF1B1D1_F17A_11D2_A1A5_0000B4984BBE__INCLUDED_)
