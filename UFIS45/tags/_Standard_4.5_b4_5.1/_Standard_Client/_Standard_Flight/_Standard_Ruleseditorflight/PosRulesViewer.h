// PosRulesViewer.h: Schnittstelle f�r die Klasse PosRulesViewer.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PosRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
#define AFX_PosRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

struct POSTABLE_LINEDATA
{
	int Rtnr;
	CString Position;
	CString RelGat;
	CString WSpan;
	CString Length;
	CString Height;
	CString ACExL;
	CString Pos1;
	CString	Max1;
	CString Pos2;
	CString	Max2;
	CString	M;
	CString	Prio;
	CString	Nat;
	CString	PAL;
	CString Aircraftgroups;
	CString	Sequ;
	CString	Groundtime;
	CString	Service;
	CString IdDEP;
	CString IdARR;
	CString Registration;
	CString MinPax;
	CString MaxPax;
	CString Destination;
};



#include "CVIEWER.H"
#include "CCSTable.H"
#include "CedaGatData.h"
#include "CedaPstData.h"


class PosRulesViewer : public CViewer  
{
public:
	PosRulesViewer();
	virtual ~PosRulesViewer();

	void Attach(CCSTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<POSTABLE_LINEDATA> omLines;
	bool bmNoUpdatesNow;
private:
	
	
	int ComparePos(POSTABLE_LINEDATA *prpPos1, POSTABLE_LINEDATA *prpPos2);

    void MakeLines();

	void MakePosLineData(PSTDATA *popPos, CStringArray &ropArray);
	int MakeLine(PSTDATA *prpPos1);
	int MakeLine();
	void MakeHeaderData();
	void MakeColumnData(CCSPtrArray<TABLE_COLUMN> &ropColList,POSTABLE_LINEDATA *prpLine);

	BOOL FindLine(char *pcpKeya, char *pcpKeyd, int &rilLineno);
	BOOL FindLine(long lpRtnr, int &rilLineno);
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: CreateLine
	//@ManMemo: SetStartEndTime
	int CreateLine(POSTABLE_LINEDATA *prpPos);
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
	void ClearAll();


// Window refreshing routines
public:


    //@ManMemo: UpdateDisplay
	void UpdateDisplay();
private:
    CString omDate;
	// Attributes
private:
	CCSTable *pomTable;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	int imTotalLines;

// Methods which handle changes (from Data Distributor)
public:
    //@ManMemo: omStartTime
    
    //@ManMemo: omEndTime
	
    //@ManMemo: omDay
	

};

#endif // !defined(AFX_PosRulesViewer_H__7CC9F681_68A9_11D2_A4B5_0000B45A33F5__INCLUDED_)
