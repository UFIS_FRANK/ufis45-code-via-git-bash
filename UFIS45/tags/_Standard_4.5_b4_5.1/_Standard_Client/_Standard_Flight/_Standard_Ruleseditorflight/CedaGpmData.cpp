
// cJobd.cpp - Class for handling Job data
//

#include <stdafx.h>
#include <afxwin.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CedaGpmData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>



void ProcessGpmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


CedaGpmData::CedaGpmData()
{   
/* 	if(!bgDoesFidsRemaExist)
	{
		BEGIN_CEDARECINFO(GPMDATA, GpmDataRecInfo)
			CCS_FIELD_DATE(Cdat,"CDAT"		,"", 1)
			CCS_FIELD_INT(Didu,"DIDU"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Disp,"DISP"	,"", 1)
			CCS_FIELD_INT(Dtim,"DTIM"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Dtyp,"DTYP"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Duch,"DUCH"	,"", 1)
			CCS_FIELD_INT(Etot,"ETOT"	,"", 1)
			CCS_FIELD_LONG(Ghsu,"GHSU"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Lkco,"LKCO"	,"", 1)
			CCS_FIELD_DATE(Lstu,"LSTU"		,"", 1)
			CCS_FIELD_INT(Noma,"NOMA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Perm,"PERM"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Pfkt,"PFKT"	,"", 1)
			CCS_FIELD_INT(Pota,"POTA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Prfl,"PRFL"	,"", 1)
			CCS_FIELD_INT(Prio,"PRIO"	,"", 1)
			CCS_FIELD_INT(Prta,"PRTA"	,"", 1)
			CCS_FIELD_LONG(Rkey,"RKEY"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Rtyp, "RTYP", "", 1)
			CCS_FIELD_INT(Tfdu,"TFDU"	,"", 1)
			CCS_FIELD_INT(Ttdu,"TTDU"	,"", 1)
			CCS_FIELD_LONG(Urno,"URNO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Usec,"USEC"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Useu,"USEU"	,"", 1)
			CCS_FIELD_DATE(Vafr,"VAFR"		,"", 1)
			CCS_FIELD_DATE(Vato,"VATO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Vrgc,"VRGC"	,"", 1)
		END_CEDARECINFO

		for (int i = 0; i < sizeof(GpmDataRecInfo)/sizeof(GpmDataRecInfo[0]); i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&GpmDataRecInfo[i],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
		// Initialize table names and field names
		strcpy(pcmTableName,"GPM");
		strcat(pcmTableName,pcgTableExt);

		sprintf(pcmListOfFields, "CDAT,DIDU,DISP,DTIM,DTYP,DUCH,ETOT,GHSU,LKCO,LSTU,NOMA,PERM,PFKT,"
								 "POTA,PRFL,PRIO,PRTA,RKEY,RTYP,TFDU,TTDU,URNO,USEC,USEU,VAFR,VATO,VRGC");

		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}
	else
	{
		BEGIN_CEDARECINFO(GPMDATA, GpmDataRecInfo)
			CCS_FIELD_DATE(Cdat,"CDAT"		,"", 1)
			CCS_FIELD_INT(Didu,"DIDU"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Disp,"DISP"	,"", 1)
			CCS_FIELD_INT(Dtim,"DTIM"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Dtyp,"DTYP"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Duch,"DUCH"	,"", 1)
			CCS_FIELD_INT(Etot,"ETOT"	,"", 1)
			CCS_FIELD_LONG(Ghsu,"GHSU"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Lkco,"LKCO"	,"", 1)
			CCS_FIELD_DATE(Lstu,"LSTU"		,"", 1)
			CCS_FIELD_INT(Noma,"NOMA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Perm,"PERM"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Pfkt,"PFKT"	,"", 1)
			CCS_FIELD_INT(Pota,"POTA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Prfl,"PRFL"	,"", 1)
			CCS_FIELD_INT(Prio,"PRIO"	,"", 1)
			CCS_FIELD_INT(Prta,"PRTA"	,"", 1)
			CCS_FIELD_LONG(Rkey,"RKEY"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Rtyp, "RTYP", "", 1)
			CCS_FIELD_INT(Tfdu,"TFDU"	,"", 1)
			CCS_FIELD_INT(Ttdu,"TTDU"	,"", 1)
			CCS_FIELD_LONG(Urno,"URNO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Usec,"USEC"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Useu,"USEU"	,"", 1)
			CCS_FIELD_DATE(Vafr,"VAFR"		,"", 1)
			CCS_FIELD_DATE(Vato,"VATO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Vrgc,"VRGC"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Rema,"REMA","",1)
		END_CEDARECINFO

		for (int i = 0; i < sizeof(GpmDataRecInfo)/sizeof(GpmDataRecInfo[0]); i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&GpmDataRecInfo[i],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
		// Initialize table names and field names
		strcpy(pcmTableName,"GPM");
		strcat(pcmTableName,pcgTableExt);

		sprintf(pcmListOfFields, "CDAT,DIDU,DISP,DTIM,DTYP,DUCH,ETOT,GHSU,LKCO,LSTU,NOMA,PERM,PFKT,"
		                     "POTA,PRFL,PRIO,PRTA,RKEY,RTYP,TFDU,TTDU,URNO,USEC,USEU,VAFR,VATO,VRGC,REMA");

		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}
*/
	omData.RemoveAll();
	int ilC = omData.GetSize();

	omOnBlock = CTime(1980, 1, 1, 1, 30, 0);
	omOfBlock = CTime(1980, 1, 1, 5, 30, 0);

	ogDdx.Register((void *)this,BC_GPM_CHANGE,CString("BC_JOBCHANGE"), CString("JobDataChange"),ProcessGpmCf);
	ogDdx.Register((void *)this,BC_GPM_NEW,CString("BC_JOBCHANGE"), CString("JobDataChange"),ProcessGpmCf);
	ogDdx.Register((void *)this,BC_GPM_DELETE,CString("BC_JOBCHANGE"), CString("JobDataChange"),ProcessGpmCf);

}

CedaGpmData::~CedaGpmData()
{
	ogDdx.UnRegister(this,NOTUSED);
	ClearAll();
	
}	

bool CedaGpmData::Read(char *pspWhere /*NULL*/)
{

 	if(!bgDoesFidsRemaExist)
	{
		BEGIN_CEDARECINFO(GPMDATA, GpmDataRecInfo)
			CCS_FIELD_DATE(Cdat,"CDAT"		,"", 1)
			CCS_FIELD_INT(Didu,"DIDU"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Disp,"DISP"	,"", 1)
			CCS_FIELD_INT(Dtim,"DTIM"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Dtyp,"DTYP"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Duch,"DUCH"	,"", 1)
			CCS_FIELD_INT(Etot,"ETOT"	,"", 1)
			CCS_FIELD_LONG(Ghsu,"GHSU"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Lkco,"LKCO"	,"", 1)
			CCS_FIELD_DATE(Lstu,"LSTU"		,"", 1)
			CCS_FIELD_INT(Noma,"NOMA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Perm,"PERM"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Pfkt,"PFKT"	,"", 1)
			CCS_FIELD_INT(Pota,"POTA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Prfl,"PRFL"	,"", 1)
			CCS_FIELD_INT(Prio,"PRIO"	,"", 1)
			CCS_FIELD_INT(Prta,"PRTA"	,"", 1)
			CCS_FIELD_LONG(Rkey,"RKEY"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Rtyp, "RTYP", "", 1)
			CCS_FIELD_INT(Tfdu,"TFDU"	,"", 1)
			CCS_FIELD_INT(Ttdu,"TTDU"	,"", 1)
			CCS_FIELD_LONG(Urno,"URNO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Usec,"USEC"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Useu,"USEU"	,"", 1)
			CCS_FIELD_DATE(Vafr,"VAFR"		,"", 1)
			CCS_FIELD_DATE(Vato,"VATO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Vrgc,"VRGC"	,"", 1)
		END_CEDARECINFO

		for (int i = 0; i < sizeof(GpmDataRecInfo)/sizeof(GpmDataRecInfo[0]); i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&GpmDataRecInfo[i],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
		// Initialize table names and field names
		strcpy(pcmTableName,"GPM");
		strcat(pcmTableName,pcgTableExt);

		sprintf(pcmListOfFields, "CDAT,DIDU,DISP,DTIM,DTYP,DUCH,ETOT,GHSU,LKCO,LSTU,NOMA,PERM,PFKT,"
								 "POTA,PRFL,PRIO,PRTA,RKEY,RTYP,TFDU,TTDU,URNO,USEC,USEU,VAFR,VATO,VRGC");

		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}
	else
	{
		BEGIN_CEDARECINFO(GPMDATA, GpmDataRecInfo)
			CCS_FIELD_DATE(Cdat,"CDAT"		,"", 1)
			CCS_FIELD_INT(Didu,"DIDU"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Disp,"DISP"	,"", 1)
			CCS_FIELD_INT(Dtim,"DTIM"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Dtyp,"DTYP"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Duch,"DUCH"	,"", 1)
			CCS_FIELD_INT(Etot,"ETOT"	,"", 1)
			CCS_FIELD_LONG(Ghsu,"GHSU"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Lkco,"LKCO"	,"", 1)
			CCS_FIELD_DATE(Lstu,"LSTU"		,"", 1)
			CCS_FIELD_INT(Noma,"NOMA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Perm,"PERM"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Pfkt,"PFKT"	,"", 1)
			CCS_FIELD_INT(Pota,"POTA"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Prfl,"PRFL"	,"", 1)
			CCS_FIELD_INT(Prio,"PRIO"	,"", 1)
			CCS_FIELD_INT(Prta,"PRTA"	,"", 1)
			CCS_FIELD_LONG(Rkey,"RKEY"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Rtyp, "RTYP", "", 1)
			CCS_FIELD_INT(Tfdu,"TFDU"	,"", 1)
			CCS_FIELD_INT(Ttdu,"TTDU"	,"", 1)
			CCS_FIELD_LONG(Urno,"URNO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Usec,"USEC"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Useu,"USEU"	,"", 1)
			CCS_FIELD_DATE(Vafr,"VAFR"		,"", 1)
			CCS_FIELD_DATE(Vato,"VATO"		,"", 1)
			CCS_FIELD_CHAR_TRIM(Vrgc,"VRGC"	,"", 1)
			CCS_FIELD_CHAR_TRIM(Rema,"REMA","",1)
		END_CEDARECINFO

		for (int i = 0; i < sizeof(GpmDataRecInfo)/sizeof(GpmDataRecInfo[0]); i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&GpmDataRecInfo[i],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
		// Initialize table names and field names
		strcpy(pcmTableName,"GPM");
		strcat(pcmTableName,pcgTableExt);

		sprintf(pcmListOfFields, "CDAT,DIDU,DISP,DTIM,DTYP,DUCH,ETOT,GHSU,LKCO,LSTU,NOMA,PERM,PFKT,"
		                     "POTA,PRFL,PRIO,PRTA,RKEY,RTYP,TFDU,TTDU,URNO,USEC,USEU,VAFR,VATO,VRGC,REMA");

		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}

/*	if(!bgDoesFidsRemaExist)
	{
		sprintf(pcmListOfFields, "CDAT,DIDU,DISP,DTIM,DTYP,DUCH,ETOT,GHSU,LKCO,LSTU,NOMA,PERM,PFKT,"
		                     "POTA,PRFL,PRIO,PRTA,RKEY,RTYP,TFDU,TTDU,URNO,USEC,USEU,VAFR,VATO,VRGC");
	}
	else
	{
		if (bgDoesFidsRemaExist)
		{
			sprintf(pcmListOfFields, "CDAT,DIDU,DISP,DTIM,DTYP,DUCH,ETOT,GHSU,LKCO,LSTU,NOMA,PERM,PFKT,"
								 "POTA,PRFL,PRIO,PRTA,RKEY,RTYP,TFDU,TTDU,URNO,USEC,USEU,VAFR,VATO,VRGC,REMA");
		}
	}

		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
*/

    //char where[512];
//TO DO  delete reactivate
//	omData.DeleteAll();
	bool ilRc = true;

	CTime omTime;

    // Select data from the database
	if(pspWhere == NULL)
	{	
		if (CedaAction("RT", "") == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		if (CedaAction("RT", pspWhere) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				char pclMsg[2048]="";
				sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
				::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
				 return false;
			}
		}
	}


	ClearAll();

    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		GPMDATA *prlGpm = new GPMDATA;
		if ((ilRc = GetBufferRecord(ilLc,prlGpm)) == true)
		{
			InsertInternal(prlGpm);
			//omData.Add(prlGpm);
		}
		else
		{
			delete prlGpm;
		}
	}

    return true;
}
bool CedaGpmData::ClearAll()
{
	omUrnoMap.RemoveAll();
    omData.DeleteAll();
    return true;
}



/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class)

bool CedaGpmData::Delete(GPMDATA *prpGpm, BOOL bpWithSave)
{
	ogDdx.DataChanged((void *)this,GPM_DELETE,(void *)prpGpm);
//	if(prpGpm->IsChanged == DATA_UNCHANGED)
	{
		prpGpm->IsChanged = DATA_DELETED;
	}
	if(bpWithSave == TRUE)
	{
		Save(prpGpm);
	}
	return true;
}

bool CedaGpmData::Insert(GPMDATA *prpGpm, BOOL bpWithSave)
{
	if(prpGpm->IsChanged == DATA_UNCHANGED)
	{
		prpGpm->IsChanged = DATA_NEW;
	}
	if(bpWithSave == TRUE)
	{
		Save(prpGpm);
	}

	return true;
}
void CedaGpmData::InsertInternal(GPMDATA *prpGpm, bool bpWithDDX)
{
	omData.Add(prpGpm);
	omUrnoMap.SetAt((void *)prpGpm->Urno,prpGpm);
	if(bpWithDDX == true)
	{
		ogDdx.DataChanged((void *)this,GPM_NEW,(void *)prpGpm);
	}

}
void CedaGpmData::UpdateInternal(GPMDATA *prpGpm, bool bpWithDDX)
{
	GPMDATA *prlGpm = GetGpmByUrno(prpGpm->Urno);
	if (prlGpm != NULL)
	{
		if(prpGpm->IsChanged != DATA_NEW)
		{
			prpGpm->IsChanged = DATA_CHANGED;
		}
		omUrnoMap.RemoveKey((void *)prlGpm->Urno);
		*prlGpm = *prpGpm; //Update omData
		omUrnoMap.SetAt((void *)prlGpm->Urno,prlGpm);
		if(bpWithDDX == true)
		{
			ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prpGpm);
		}
	}
}

GPMDATA *CedaGpmData::GetGpmByUrno(long lpUrno)
{
	GPMDATA  *prlGpm;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlGpm) == TRUE)
	{
		return prlGpm;
	}
	return NULL;
}

void CedaGpmData::DeleteInternal(GPMDATA *prpGpm, bool bpWithDDX)
{
	if(bpWithDDX == true)
	{
		ogDdx.DataChanged((void *)this,GPM_DELETE,(void *)prpGpm);
	}
	omUrnoMap.RemoveKey((void *)prpGpm->Urno);
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpGpm->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
}

bool CedaGpmData::Update(GPMDATA *prpGpm, BOOL bpWithSave)
{
	ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prpGpm);
	if(prpGpm->IsChanged == DATA_UNCHANGED)
	{
		prpGpm->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		Save(prpGpm);
	}
	return true;

}



void  ProcessGpmCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogGpmData.ProcessGpmBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaGpmData::ProcessGpmBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	GPMDATA *prlGpm = (GPMDATA *)vpDataPointer;
	switch(ipDDXType)
	{
	case BC_GPM_NEW:
		omData.NewAt(omData.GetSize(), *prlGpm);
		ogDdx.DataChanged((void *)this,GPM_NEW,(void *)prlGpm);
		break;
	case BC_GPM_CHANGE:
		{
			int ilC = omData.GetSize();
			for(int i = 0; i < ilC; i++)
			{
				if(omData[i].Urno == prlGpm->Urno)
				{
					omData.SetAt(i, prlGpm);
					ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prlGpm);
					i = ilC;
				}
			}
		}
		break;
	case BC_GPM_DELETE:
		{
			int ilC = omData.GetSize();
			for(int i = 0; i < ilC; i++)
			{
				if(omData[i].Urno == prlGpm->Urno)
				{
					ogDdx.DataChanged((void *)this,GPM_DELETE,(void *)prlGpm);
					omData.DeleteAt(i);
					i = ilC;
				}
			}
		}
		break;
	default:
		break;
	}
}


bool CedaGpmData::Save(GPMDATA *prpGpm)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpGpm->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpGpm->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpGpm);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpGpm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGpm->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpGpm);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpGpm->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpGpm->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if (olRc == true)
		{
/*			int ilC = omData.GetSize();
			for(int i = 0; i < ilC; i++)
			{
				if(omData[i].Urno == prpGpm->Urno)
				{
					omData.DeleteAt(i);
				}
			}
*/
		}
		break;
	}

	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}
   return true;
}


void CedaGpmData::GetGpmByRkey(CCSPtrArray<GPMDATA> &ropGpm, long lpRkey)
{
	GPMDATA *prlGpm;
	POSITION pos;
	void *pVoid;
	int ilCount = 0;
	for( pos = omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		omUrnoMap.GetNextAssoc( pos, pVoid , (void *&)prlGpm );
		if(prlGpm->Rkey == lpRkey)
		{
			ropGpm.Add(prlGpm);
		}
	}

/*	int ilCount = omData.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		GPMDATA rlGpm = omData[i];
		if(omData[i].Rkey == lpRkey)
		{
			ropGpm.Add(&omData[i]);
		}
	}
*/
}

bool CedaGpmData::ChangeJobTime(long lpUrno, CTime opNewStart, CTime opNewEnd)
{
	CTime olOldStartTime;
	CTime olOldEndTime;
	CTime olNewStartTime;
	CTime olNewEndTime;
	GPMDATA *prlGpm = GetGpmByUrno(lpUrno);
	if(prlGpm != NULL)
	{
		if(strcmp(prlGpm->Dtyp, "T") == 0)
		{
			olOldStartTime = omOnBlock;
			olOldEndTime = omOfBlock;
		}
		else if(strcmp(prlGpm->Dtyp, "F") == 0)
		{
			olOldStartTime = omOfBlock;
			olOldEndTime = omOfBlock;
		}
		else if(strcmp(prlGpm->Dtyp, "N") == 0)
		{
			olOldStartTime = omOnBlock;
			olOldEndTime = omOnBlock;
		}
		olOldStartTime -= CTimeSpan(prlGpm->Prta*60);
		olOldStartTime -= CTimeSpan(prlGpm->Ttdu*60);
		olOldEndTime += CTimeSpan(prlGpm->Pota*60);
		olOldEndTime += CTimeSpan(prlGpm->Tfdu*60);
		//First we have to calculate the difference between new times and old times
		CTimeSpan olStartDiff = opNewStart - olOldStartTime;
		CTimeSpan olEndDiff = opNewEnd - olOldEndTime;
		CString ol1 = olOldStartTime.Format("%d.%m.&Y-%H:%M");
		ol1 = opNewStart.Format("%d.%m.&Y-%H:%M");
		ol1 = olOldEndTime.Format("%d.%m.&Y-%H:%M");
		ol1 = opNewEnd.Format("%d.%m.&Y-%H:%M");
		prlGpm->Prta -= olStartDiff.GetTotalMinutes();
		prlGpm->Pota += olEndDiff.GetTotalMinutes();
		ogDdx.DataChanged((void *)this,GPM_CHANGE,(void *)prlGpm);
		if (prlGpm->IsChanged == DATA_UNCHANGED)
		{
			prlGpm->IsChanged = DATA_CHANGED;
		}
		
	}
	return true;
}

CString CedaGpmData::GetFieldList()
{
	CString olFieldList = CString(pcmFieldList);
	return olFieldList;
}

bool CedaGpmData::IsDataChanged(GPMDATA *prpOld, GPMDATA *prpNew)
{
	CString olFieldList;
	CString olListOfData;

	olFieldList = GetFieldList();
	MakeCedaData(olListOfData, olFieldList, (void*)prpOld, (void*)prpNew);
	if(olFieldList.IsEmpty())
	{
		return false;
	}
	else
	{
		return true;
	}

}