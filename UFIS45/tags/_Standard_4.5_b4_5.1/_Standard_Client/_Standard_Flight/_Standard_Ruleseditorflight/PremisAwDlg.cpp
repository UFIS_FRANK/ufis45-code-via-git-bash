// PremisAwDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CCSEdit.h"
#include "PremisAwDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PremisAwDlg dialog

extern PremisAwDlg *pogPremisAwDlg;

PremisAwDlg::PremisAwDlg(CWnd* pParent, CRect opParentRect)
	: CDialog(PremisAwDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(PremisAwDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nDialogBarHeight = 0;
	omParentRect = opParentRect;
	CDialog::Create(PremisAwDlg::IDD);
	pomTable = new CTable;
    pomTable->tempFlag = 2;
	
	CRect rect;
	GetClientRect(&rect);
	rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);

	UpdateDisplay();

	//LoadList();
	ShowWindow(SW_SHOWNORMAL);
}


void PremisAwDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PremisAwDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PremisAwDlg, CDialog)
	//{{AFX_MSG_MAP(PremisAwDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblClk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PremisAwDlg message handlers

void PremisAwDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL PremisAwDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CDialog::OnInitDialog();
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
	rect = omParentRect;
	ClientToScreen(&rect);
    MoveWindow(&rect);
	//HICON h_icon = AfxGetApp()->LoadIcon(IDI_UFIS);
	//SetWindowText(omCaption);
	//SetIcon(h_icon, FALSE);
	return TRUE;  
}

BOOL PremisAwDlg::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	BOOL rc;
	rc = CDialog::DestroyWindow();
	if(rc == TRUE)
	{
		delete this;
		pogPremisAwDlg = NULL;
	}

	return rc;
}

void PremisAwDlg::UpdateDisplay()
{
   pomTable->SetHeaderFields(GetString(IDS_STRING314));
   pomTable->SetFormatList("4|12|100");

	pomTable->ResetContent();
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		GHPDATA rlGhp = omLines[i];
		//CreateLine(&rlFlight);
		pomTable->AddTextLine(Format(&omLines[i]), &omLines[i]);
	}
	pomTable->DisplayTable();
}

CString PremisAwDlg::Format(GHPDATA *prpGhp)
{
   CString s;

	s =  CString(prpGhp->Prco) + "|";
	s+=  CString(prpGhp->Prsn) + "|";
	s+=  prpGhp->Vpfr.Format("%d.%m.%Y - ") + prpGhp->Vpto.Format("%d.%m.%Y") + "|";
	return s;
}

void PremisAwDlg::CreateLine(GHPDATA *prpGhp)
{
	GHPDATA rlGhp;
	rlGhp = *prpGhp;
	omLines.NewAt(omLines.GetSize(), rlGhp);
}

LONG PremisAwDlg::OnTableLButtonDblClk(UINT wParam, LONG lParam)
{
	return 0L;
}

