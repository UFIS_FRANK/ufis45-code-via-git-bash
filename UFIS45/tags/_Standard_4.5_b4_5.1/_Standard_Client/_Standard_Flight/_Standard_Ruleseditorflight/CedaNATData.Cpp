// CedaNATData.cpp
 
#include <stdafx.h>
#include <CedaNATData.h>


// Local function prototype
static void ProcessNATCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//CedaNATData ogNatData;
//--CEDADATA-----------------------------------------------------------------------------------------------


CedaNATData::CedaNATData() : CCSCedaData(&ogCommHandler)
{
	BEGIN_CEDARECINFO(NATDATA,NATDataRecInfo)
		CCS_FIELD_DATE		(Cdat,"CDAT","Erstellungsdatum",0)
		CCS_FIELD_DATE		(Lstu,"LSTU","Datum letzte �nderung",0)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL","Protokollierungskennzeichen",0)
		CCS_FIELD_CHAR_TRIM	(Tnam,"TNAM","Verkehrsart Name",0)
		CCS_FIELD_CHAR_TRIM	(Ttyp,"TTYP","Verkehrsart",0)
		CCS_FIELD_LONG		(Urno,"URNO","Eindeutige Datensatz-Nr.",0)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC","Anwender (Ersteller,0)",0)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU","Anwender (letzte �nderung,0)",0)
		CCS_FIELD_DATE		(Vafr,"VAFR","G�ltig von",0)
		CCS_FIELD_DATE		(Vato,"VATO","G�ltig bis",0)
		CCS_FIELD_CHAR_TRIM	(Alga,"ALGA","",0)
		CCS_FIELD_CHAR_TRIM	(Alpo,"ALPO","",0)
		CCS_FIELD_CHAR_TRIM	(Albb,"ALBB","",0)
		CCS_FIELD_CHAR_TRIM	(Alwr,"ALWR","",0)
	END_CEDARECINFO //(NATDATA)
/* Macro laesst CString nicht zu MBR 11.01.1999
    // Create an array of CEDARECINFO for NATDATA
	BEGIN_CEDARECINFO(NATDATA,NATDataRecInfo)
		CCS_FIELD_DATE		(Cdat,"CDAT",GetString(IDS_STRING163),0)
		CCS_FIELD_DATE		(Lstu,"LSTU",GetString(IDS_STRING165),0)
		CCS_FIELD_CHAR_TRIM	(Prfl,"PRFL",GetString(IDS_STRING166),0)
		CCS_FIELD_CHAR_TRIM	(Tnam,"TNAM",GetString(IDS_STRING298),0)
		CCS_FIELD_CHAR_TRIM	(Ttyp,"TTYP",GetString(IDS_STRING124),0)
		CCS_FIELD_LONG		(Urno,"URNO",GetString(IDS_STRING162),0)
		CCS_FIELD_CHAR_TRIM	(Usec,"USEC",GetString(IDS_STRING299),0)
		CCS_FIELD_CHAR_TRIM	(Useu,"USEU",GetString(IDS_STRING300),0)
		CCS_FIELD_DATE		(Vafr,"VAFR",GetString(IDS_STRING187),0)
		CCS_FIELD_DATE		(Vato,"VATO",GetString(IDS_STRING186),0)
	END_CEDARECINFO //(NATDATA)
*/

	// Copy the record structure
	for (int i=0; i< sizeof(NATDataRecInfo)/sizeof(NATDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&NATDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"NAT");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmNATFieldList,"CDAT,LSTU,PRFL,TNAM,TTYP,URNO,USEC,USEU,VAFR,VATO,ALGA,ALPO,ALBB,ALWR");
	pcmFieldList = pcmNATFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}


void CedaNATData::Register(void)
{
	ogDdx.Register((void *)this,BC_NAT_CHANGE,CString("NATDATA"), CString("NAT-changed"),ProcessNATCf);
	ogDdx.Register((void *)this,BC_NAT_DELETE,CString("NATDATA"), CString("NAT-deleted"),ProcessNATCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaNATData::~CedaNATData(void)
{
	TRACE("CedaNATData::~CedaNATData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaNATData::ClearAll(void)
{
	TRACE("CedaNATData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omTtypMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaNATData::ReadAllNATs()
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omTtypMap.RemoveAll();
    omData.DeleteAll();


	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		NATDATA *prpNAT = new NATDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpNAT)) == true)
		{
			prpNAT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpNAT);//Update omData
			omUrnoMap.SetAt((void *)prpNAT->Urno,prpNAT);
			omTtypMap.SetAt(prpNAT->Ttyp,prpNAT);
		}
		else
		{
			delete prpNAT;
		}
	}
	ogLog.Trace("LOADNAT"," %d gelesen\n",ilLc-1);
	TRACE("ReadAllNATs: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaNATData::InsertNAT(NATDATA *prpNAT,BOOL bpSendDdx)
{
	prpNAT->IsChanged = DATA_NEW;
	if(SaveNAT(prpNAT) == false) return false; //Update Database
	InsertNATInternal(prpNAT);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaNATData::InsertNATInternal(NATDATA *prpNAT)
{
	//PrepareNATData(prpNAT);
	ogDdx.DataChanged((void *)this, NAT_CHANGE,(void *)prpNAT ); //Update Viewer
	omData.Add(prpNAT);//Update omData
	omUrnoMap.SetAt((void *)prpNAT->Urno,prpNAT);
	omTtypMap.SetAt(prpNAT->Ttyp,prpNAT);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaNATData::DeleteNAT(long lpUrno)
{
	NATDATA *prlNAT = GetNATByUrno(lpUrno);
	if (prlNAT != NULL)
	{
		prlNAT->IsChanged = DATA_DELETED;
		if(SaveNAT(prlNAT) == false) return false; //Update Database
		DeleteNATInternal(prlNAT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaNATData::DeleteNATInternal(NATDATA *prpNAT)
{
	ogDdx.DataChanged((void *)this,NAT_DELETE,(void *)prpNAT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpNAT->Urno);
	omTtypMap.RemoveKey(prpNAT->Ttyp);
	int ilNATCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilNATCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpNAT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaNATData::PrepareNATData(NATDATA *prpNAT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaNATData::UpdateNAT(NATDATA *prpNAT,BOOL bpSendDdx)
{
	if (GetNATByUrno(prpNAT->Urno) != NULL)
	{
		if (prpNAT->IsChanged == DATA_UNCHANGED)
		{
			prpNAT->IsChanged = DATA_CHANGED;
		}
		if(SaveNAT(prpNAT) == false) return false; //Update Database
		UpdateNATInternal(prpNAT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaNATData::UpdateNATInternal(NATDATA *prpNAT)
{
	NATDATA *prlNAT = GetNATByUrno(prpNAT->Urno);
	if (prlNAT != NULL)
	{
		omTtypMap.RemoveKey(prlNAT->Ttyp);
		*prlNAT = *prpNAT; //Update omData
		omTtypMap.SetAt(prlNAT->Ttyp,prlNAT);
		ogDdx.DataChanged((void *)this,NAT_CHANGE,(void *)prlNAT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

NATDATA *CedaNATData::GetNATByUrno(long lpUrno)
{
	NATDATA  *prlNAT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlNAT) == TRUE)
	{
		return prlNAT;
	}
	return NULL;
	
}

long CedaNATData::GetUrnoByTtyp(char *pspTtyp)
{
	NATDATA  *prlNAT;
	if(strcmp(pspTtyp, "") == 0)
	{
		return 0;
	}

	if (omTtypMap.Lookup((LPCSTR)pspTtyp,(void *&)prlNAT) == TRUE)
	{
		return prlNAT->Urno;
	}

	if (omTtypMap.Lookup((LPCSTR)pspTtyp,(void *&)prlNAT) == TRUE)
	{
		return prlNAT->Urno;
	}
	return 0;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaNATData::ReadSpecialData(CCSPtrArray<NATDATA> *popNat,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false)
		{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
		}
	}
	if(popNat != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			NATDATA *prpNat = new NATDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpNat,CString(pclFieldList))) == true)
			{
				popNat->Add(prpNat);
			}
			else
			{
				delete prpNat;
			}
		}
		if(popNat->GetSize() == 0)
			return false;
	}
    return true;
}

//--EXIST-I------------------------------------------------------------------------------------------------

bool CedaNATData::NATExists(NATDATA *prpNAT)
{
	NATDATA  *prlNAT;
	if (omTtypMap.Lookup((LPCSTR)prpNAT->Ttyp,(void *&)prlNAT) == TRUE)
	{
		if(prlNAT->Urno != prpNAT->Urno) return true;
	}
	return false;
}

//--EXIST-II----------------------------------------------------------------------------------------------

bool CedaNATData::ExistTtyp(char* popTtyp)
{
	bool ilRc = true;
	char pclSelection[512];
	sprintf(pclSelection,"WHERE TTYP='%s'",popTtyp);
	//ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"),pcmTableName,"TTYP",pclSelection,"",pcgDataBuf);
	ilRc = CedaAction("SYS",pcmTableName,"TTYP",pclSelection,"",pcgDataBuf);
	TRACE("ExistNAT: TTYP=%s, CedaReturn=%d\n",popTtyp,ilRc);
	return ilRc;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaNATData::SaveNAT(NATDATA *prpNAT)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpNAT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpNAT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpNAT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpNAT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpNAT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpNAT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpNAT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpNAT->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessNATCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_NAT_CHANGE :
	case BC_NAT_DELETE :
		((CedaNATData *)popInstance)->ProcessNATBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaNATData::ProcessNATBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlNATData;
	long llUrno;
	prlNATData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlNATData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlNATData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	NATDATA *prlNAT;
	prlNAT = GetNATByUrno(llUrno);
	if(ipDDXType == BC_NAT_CHANGE)
	{
		if (prlNAT != NULL)
		{
			//ogLog.Trace("BCNAT_Update","NAT-Urno: <%ld>",llUrno);
			GetRecordFromItemList(prlNAT,prlNATData->Fields,prlNATData->Data);
			UpdateNATInternal(prlNAT);
		}
		else
		{
			prlNAT = new NATDATA;
			GetRecordFromItemList(prlNAT,prlNATData->Fields,prlNATData->Data);
			InsertNATInternal(prlNAT);
		}
	}
	if(ipDDXType == BC_NAT_DELETE)
	{
		if (prlNAT != NULL)
		{
			//ogLog.Trace("BCNAT_Delete","NAT-Urno: <%ld>",llUrno);
			DeleteNATInternal(prlNAT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
