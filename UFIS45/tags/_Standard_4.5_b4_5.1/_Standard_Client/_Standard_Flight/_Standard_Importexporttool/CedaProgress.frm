VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form CedaProgress 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Import Progress"
   ClientHeight    =   3000
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5655
   Icon            =   "CedaProgress.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   5655
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ProgressBar ProgressBar 
      Height          =   285
      Index           =   0
      Left            =   60
      TabIndex        =   2
      Top             =   1800
      Width           =   4725
      _ExtentX        =   8334
      _ExtentY        =   503
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.TextBox MsgData 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   1410
      Visible         =   0   'False
      Width           =   5535
   End
   Begin TABLib.TAB MsgList 
      Height          =   1275
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   2249
      _StockProps     =   0
   End
   Begin MSComctlLib.ProgressBar ProgressBar 
      Height          =   285
      Index           =   1
      Left            =   60
      TabIndex        =   4
      Top             =   2130
      Width           =   4725
      _ExtentX        =   8334
      _ExtentY        =   503
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.Label ProgrPerc 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   1
      Left            =   4845
      TabIndex        =   5
      Top             =   2130
      Width           =   750
   End
   Begin VB.Label ProgrPerc 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   0
      Left            =   4845
      TabIndex        =   3
      Top             =   1800
      Width           =   750
   End
End
Attribute VB_Name = "CedaProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TabLines As Long

Private Sub Form_Load()
    TabLines = 8
    InitForm TabLines
End Sub

Private Sub InitForm(ShowLines As Long)
    MsgList(0).ResetContent
    MsgList(0).ShowVertScroller True
    'MsgList(0).ShowHorzScroller True
    MsgList(0).FontName = "Courier New"
    MsgList(0).SetTabFontBold True
    MsgList(0).HeaderFontSize = 17
    MsgList(0).FontSize = 17
    MsgList(0).LineHeight = 17
    MsgList(0).Height = (ShowLines + 1) * MsgList(0).LineHeight * Screen.TwipsPerPixelY
    MsgList(0).LeftTextOffset = 2
    MsgList(0).GridLineColor = DarkGray
    MsgList(0).HeaderString = "REGISTR.,  MIN TIME,  MAX TIME,F,R,   LineCode,Data"
    MsgList(0).HeaderLengthString = "80,116,116,20,20,40,100"
    MsgList(0).ColumnAlignmentString = "L,L,L,L,L,R,l"
    MsgList(0).ColumnWidthString = "12,12,12,1,1,6,1000"
    MsgList(0).SetColumnBoolProperty 3, "X", " "
    MsgList(0).SetColumnBoolProperty 4, "X", " "
    MsgList(0).DateTimeSetColumn 1
    MsgList(0).DateTimeSetInputFormatString 1, "YYYYMMDDhhmm"
    MsgList(0).DateTimeSetOutputFormatString 1, "DDMMMYY'/'hhmm"
    MsgList(0).DateTimeSetColumn 2
    MsgList(0).DateTimeSetInputFormatString 2, "YYYYMMDDhhmm"
    MsgList(0).DateTimeSetOutputFormatString 2, "DDMMMYY'/'hhmm"
    MsgList(0).RedrawTab
    MsgData.Top = MsgList(0).Top + MsgList(0).Height + 4 * Screen.TwipsPerPixelY
    Me.Height = MsgData.Top + MsgData.Height + 30 * Screen.TwipsPerPixelY
    ProgressBar(0).Top = MsgData.Top
    ProgrPerc(0).Top = ProgressBar(0).Top
    ProgressBar(1).Top = ProgressBar(0).Top + ProgressBar(0).Height + 3 * Screen.TwipsPerPixelY
    ProgrPerc(1).Top = ProgressBar(1).Top
    ProgressBar(0).Visible = False
    ProgrPerc(0).Visible = False
    ProgressBar(1).Visible = False
    ProgrPerc(1).Visible = False
    MsgData.Visible = True
End Sub

Public Function AppendToMyList(NewLine As String) As String
    Dim LineCode As String
    Dim MaxLin As Long
    On Error Resume Next
    LineCode = Trim(Str(MsgList(0).GetLineCount))
    MsgList(0).InsertTextLine NewLine, False
    MsgList(0).SetColumnValue Val(LineCode), 5, LineCode
    MaxLin = Val(LineCode) - 1
    MsgList(0).OnVScrollTo MaxLin - TabLines + 3
    MsgList(0).RedrawTab
    Me.Show
    Me.Refresh
    AppendToMyList = LineCode
End Function

Public Sub UpdateMyList(LineCode As String, LineValues As String)
    Dim LineList As String
    Dim ColNo As Long
    Dim LineNo As Long
    On Error Resume Next
    If Val(LineCode) >= 0 Then
        LineList = MsgList(0).GetLinesByColumnValue(5, LineCode, 1)
        If LineList <> "" Then
            LineNo = Val(LineList)
            If LineNo >= 0 Then
                MsgList(0).UpdateTextLine LineNo, LineValues, False
                MsgList(0).SetColumnValue LineNo, 5, LineCode
                MsgList(0).OnVScrollTo LineNo - TabLines + 3
                MsgList(0).RedrawTab
            End If
            Me.Show
            Me.Refresh
        Else
            AppendToMyList LineValues
        End If
    End If
End Sub

Public Sub EvaluateBc(BcCmd As String, BcTws As String, BcSelect As String, BcFields As String, BcData As String)
    Static AfterAccMax As Long
    Static AfterAccCur As Long
    Dim LineCode As String
    Dim CurValue As String
    Dim MaxValue As String
    Dim ShowProgress As Boolean
    ShowProgress = True
    Select Case BcCmd
        Case "RAC"
            LineCode = GetItem(BcTws, 3, ".")
            UpdateMyList LineCode, BcSelect & ",X,X,-,"
            StorePendingRegn BcSelect, False
            If AfterAccMax > 0 Then
                AfterAccCur = AfterAccCur + 1
                HandleProgress 0, AfterAccCur, AfterAccMax
                ShowProgress = False
            End If
        Case "RAX"
            StorePendingRegn BcSelect, True
            ShowProgress = False
        Case "ACCBGN"
            AfterAccMax = -1
            AfterAccCur = -1
            ProgrPerc(0).BackColor = vbButtonFace
            ShowProgress = False
        Case "ACCEND"
            AfterAccMax = Val(BcData)
            If AfterAccMax <= 0 Then
                MsgList(0).Sort 0, True, True
                MsgList(0).OnVScrollTo 0
                MsgList(0).RedrawTab
                ProgressBar(0).Visible = False
                ProgrPerc(0).Visible = False
                MsgData.Visible = True
                Me.Height = MsgData.Top + MsgData.Height + 30 * Screen.TwipsPerPixelY
                AfterAccMax = -1
                AfterAccCur = -1
            Else
                ProgrPerc(0).BackColor = LightYellow
                AfterAccCur = 0
                HandleProgress 0, AfterAccCur, AfterAccMax
            End If
            ShowProgress = False
        Case Else
    End Select
    If ShowProgress Then
        CurValue = GetItem(BcTws, 4, ".")
        MaxValue = GetItem(BcTws, 5, ".")
        HandleProgress 0, Val(CurValue), Val(MaxValue)
    End If
End Sub

Private Sub StorePendingRegn(LineValues As String, RaxMessage As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim SaveIt As Boolean
    Dim BrcRegn As String
    Dim CurRegn As String
    Dim MinTime As String
    Dim MaxTime As String
    BrcRegn = GetItem(LineValues, 1, ",")
    SaveIt = False
    MaxLine = MsgList(0).GetLineCount - 1
    'For CurLine = 0 To MaxLine
    '    If MsgList(0).GetLineStatusValue(CurLine) <> 1 Then
    '        CurRegn = MsgList(0).GetColumnValue(CurLine, 0)
    '        If CurRegn = BrcRegn Then
    '            SaveIt = True
    '            Exit For
    '        End If
    '    End If
    'Next
    SaveIt = True
    If SaveIt Then
        NewLine = -1
        For CurLine = 0 To MaxLine
            If MsgList(0).GetLineStatusValue(CurLine) = 1 Then
                CurRegn = MsgList(0).GetColumnValue(CurLine, 0)
                If CurRegn = BrcRegn Then
                    NewLine = CurLine
                    Exit For
                End If
            End If
        Next
        If (RaxMessage) And (NewLine < 0) Then
            MsgList(0).InsertTextLine LineValues & ",,,-,", False
            NewLine = MaxLine + 1
            MsgList(0).SetLineStatusValue NewLine, 1
            MsgList(0).SetLineColor NewLine, vbBlack, LightYellow
        End If
        If NewLine >= 0 Then
            If RaxMessage Then
                MinTime = GetItem(LineValues, 2, ",")
                If MinTime < MsgList(0).GetColumnValue(NewLine, 1) Then
                    MsgList(0).SetColumnValue NewLine, 1, MinTime
                End If
                MaxTime = GetItem(LineValues, 3, ",")
                If MaxTime > MsgList(0).GetColumnValue(NewLine, 2) Then
                    MsgList(0).SetColumnValue NewLine, 2, MaxTime
                End If
                MsgList(0).SetColumnValue NewLine, 3, " "
                MsgList(0).SetColumnValue NewLine, 4, " "
            Else
                MsgList(0).SetColumnValue NewLine, 3, "X"
                MsgList(0).SetColumnValue NewLine, 4, "X"
            End If
            MsgList(0).OnVScrollTo NewLine - TabLines + 3
        End If
        MsgList(0).RedrawTab
    End If
End Sub
Public Sub HandleProgress(Index As Integer, CurValue As Long, MaxValue As Long)
    Dim useCur As Integer
    Dim useMax As Integer
    Dim tmpVal As Long
    On Error Resume Next
    If (MaxValue > 0) And (CurValue <= MaxValue) Then
        useCur = CInt(CurValue)
        useMax = CInt(MaxValue)
        If useMax <> ProgressBar(Index).Max Then
            ProgressBar(Index).Value = 0
            ProgressBar(Index).Max = useMax
        End If
        ProgressBar(Index).Value = useCur
        If Not ProgressBar(Index).Visible Then
            ProgrPerc(Index).BackColor = vbButtonFace
            MsgData.Visible = False
            ProgressBar(Index).Visible = True
            ProgrPerc(Index).Visible = True
            If Index = 1 Then
                If Not ProgressBar(0).Visible Then MsgData.Visible = True
                Me.Height = ProgressBar(1).Top + ProgressBar(1).Height + 30 * Screen.TwipsPerPixelY
                Me.Refresh
            End If
        End If
        tmpVal = CLng(ProgressBar(Index).Value) * 100 / CLng(ProgressBar(Index).Max)
        If tmpVal > 100 Then tmpVal = 100
        ProgrPerc(Index).Caption = Str(tmpVal) & "%"
        ProgrPerc(Index).Refresh
    Else
        ProgressBar(Index).Visible = False
        ProgrPerc(Index).Visible = False
        If Index = 1 Then
            Me.Height = ProgressBar(0).Top + ProgressBar(0).Height + 30 * Screen.TwipsPerPixelY
            Me.Refresh
        End If
    End If
End Sub

Private Sub MsgList_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    If (Selected) And (LineNo >= 0) Then
        MsgData = Replace(MsgList(0).GetColumnValue(LineNo, 6), ";", ",", 1, -1, vbBinaryCompare)
    End If
End Sub

Public Sub AppendRaxData(LineValues As String, DataValue As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim SaveIt As Boolean
    Dim BrcRegn As String
    Dim CurRegn As String
    Dim CurData As String
    BrcRegn = GetItem(LineValues, 1, ",")
    MaxLine = MsgList(0).GetLineCount - 1
    SaveIt = True
    If SaveIt Then
        NewLine = -1
        For CurLine = 0 To MaxLine
            If MsgList(0).GetLineStatusValue(CurLine) = 1 Then
                CurRegn = MsgList(0).GetColumnValue(CurLine, 0)
                If CurRegn = BrcRegn Then
                    NewLine = CurLine
                    Exit For
                End If
            End If
        Next
        If NewLine >= 0 Then
            MsgList(0).OnVScrollTo NewLine - TabLines + 3
            CurData = MsgList(0).GetColumnValue(NewLine, 6)
            If InStr(CurData, DataValue) <= 0 Then
                If CurData <> "" Then CurData = CurData & ";"
                CurData = CurData & DataValue
                MsgList(0).SetColumnValue NewLine, 6, CurData
            End If
        End If
    End If
End Sub
