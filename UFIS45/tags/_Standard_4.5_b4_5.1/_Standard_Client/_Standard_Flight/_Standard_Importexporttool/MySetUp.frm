VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MySetUp 
   Caption         =   "Import Tool Setup"
   ClientHeight    =   7320
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12105
   Icon            =   "MySetUp.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7320
   ScaleWidth      =   12105
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   5910
      Top             =   0
   End
   Begin VB.TextBox ServerTime 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9030
      Locked          =   -1  'True
      TabIndex        =   41
      Tag             =   "200205240800"
      Text            =   "24.05.2002 / 08:00"
      Top             =   30
      Width           =   1800
   End
   Begin VB.TextBox ServerTimeType 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10860
      Locked          =   -1  'True
      TabIndex        =   40
      Text            =   "LOC"
      Top             =   30
      Width           =   525
   End
   Begin VB.TextBox MyUtcTimeDiff 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   11430
      Locked          =   -1  'True
      TabIndex        =   39
      Text            =   "60"
      ToolTipText     =   "UTC Time Difference"
      Top             =   30
      Width           =   660
   End
   Begin VB.Frame Frame1 
      Caption         =   "Font Size"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   60
      TabIndex        =   34
      Top             =   6420
      Width           =   7245
      Begin MSComctlLib.Slider FontSlider 
         Height          =   540
         Left            =   30
         TabIndex        =   35
         ToolTipText     =   "Font Size Slider"
         Top             =   210
         Width           =   7185
         _ExtentX        =   12674
         _ExtentY        =   953
         _Version        =   393216
         MousePointer    =   1
         LargeChange     =   2
         Min             =   8
         Max             =   32
         SelStart        =   8
         Value           =   8
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Global Data Lookup && Server Access"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6795
      Left            =   7470
      TabIndex        =   31
      Top             =   450
      Width           =   4605
      Begin VB.CheckBox chkServer 
         Caption         =   "CDR"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   3030
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   270
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Used"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   270
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1830
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   270
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Active"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   270
         Value           =   1  'Checked
         Width           =   855
      End
      Begin TABLib.TAB GdlTabData 
         Height          =   6090
         Left            =   90
         TabIndex        =   36
         Top             =   600
         Width           =   4425
         _Version        =   65536
         _ExtentX        =   7805
         _ExtentY        =   10742
         _StockProps     =   64
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Date Format"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   5400
      TabIndex        =   28
      Top             =   450
      Width           =   1905
      Begin VB.CheckBox MonitorSetting 
         BackColor       =   &H00C0FFC0&
         Caption         =   "User"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   270
         Value           =   1  'Checked
         Width           =   855
      End
      Begin VB.CheckBox MonitorSetting 
         Caption         =   "System"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   270
         Width           =   855
      End
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame Frame4 
      Caption         =   "Seasonal Period Preset"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   60
      TabIndex        =   5
      Top             =   450
      Width           =   2445
      Begin VB.CheckBox chkWork 
         Caption         =   "Clear"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   990
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   270
         Width           =   855
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Season"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   270
         Width           =   855
      End
      Begin VB.Label OpTimeOffset 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1890
         TabIndex        =   43
         ToolTipText     =   "Min. hours from 'Now'"
         Top             =   600
         Width           =   465
      End
      Begin VB.Label AutoClear 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1890
         TabIndex        =   12
         ToolTipText     =   "Clear Out 'Now plus ... Days'"
         Top             =   270
         Width           =   465
      End
   End
   Begin VB.Frame MyFileInfo 
      Caption         =   "Import File Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4605
      Left            =   60
      TabIndex        =   1
      Top             =   1620
      Width           =   7245
      Begin TABLib.TAB SessionTab 
         Height          =   2775
         Left            =   90
         TabIndex        =   44
         Top             =   1710
         Width           =   7065
         _Version        =   65536
         _ExtentX        =   12462
         _ExtentY        =   4895
         _StockProps     =   64
      End
      Begin VB.CheckBox chkReadDbf 
         Caption         =   "Reader"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   330
         Width           =   855
      End
      Begin VB.CheckBox chkLoad 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   705
         Width           =   855
      End
      Begin VB.CheckBox chkFilter 
         Caption         =   "Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   1005
         Width           =   855
      End
      Begin VB.CheckBox chkSplit 
         Caption         =   "Split"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   1305
         Width           =   855
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "Error Remarks"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   12
         Left            =   1140
         TabIndex        =   27
         ToolTipText     =   "Error Remarks"
         Top             =   1380
         Width           =   5925
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "Transaction Remarks"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   11
         Left            =   1140
         TabIndex        =   26
         ToolTipText     =   "Transaction Remarks"
         Top             =   1170
         Width           =   5925
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "+3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   6540
         TabIndex        =   25
         ToolTipText     =   "Local UTC Time Difference"
         Top             =   960
         Width           =   525
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "UTC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   6000
         TabIndex        =   24
         ToolTipText     =   "Import Data Time Type"
         Top             =   960
         Width           =   525
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "S/C"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   5190
         TabIndex        =   23
         ToolTipText     =   "Import Data Profile (S/C - GA - ROT)"
         Top             =   960
         Width           =   795
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "W02"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   4380
         TabIndex        =   22
         ToolTipText     =   "Decoded Season"
         Top             =   960
         Width           =   795
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "ATH"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   1140
         TabIndex        =   21
         ToolTipText     =   "Related Airport"
         Top             =   960
         Width           =   795
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "Full"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   3570
         TabIndex        =   20
         ToolTipText     =   "Import Data Completeness"
         Top             =   960
         Width           =   795
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "Season"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   2760
         TabIndex        =   18
         ToolTipText     =   "Import Data File Type"
         Top             =   960
         Width           =   795
      End
      Begin VB.Label CurFileInfo 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Caption         =   "SLOT"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   1950
         TabIndex        =   17
         ToolTipText     =   "Import Data System Type"
         Top             =   960
         Width           =   795
      End
      Begin VB.Label CurFileInfo 
         BackColor       =   &H0000FFFF&
         Caption         =   "Last Input Line"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   1140
         TabIndex        =   16
         ToolTipText     =   "Last Input Line"
         Top             =   750
         Width           =   5925
      End
      Begin VB.Label CurFileInfo 
         BackColor       =   &H0000FFFF&
         Caption         =   "First Input Line"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   1140
         TabIndex        =   15
         ToolTipText     =   "First Input Line"
         Top             =   540
         Width           =   5925
      End
      Begin VB.Label CurFileInfo 
         BackColor       =   &H0000FFFF&
         Caption         =   "File Path && Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   1140
         TabIndex        =   14
         ToolTipText     =   "Opened File Path & Name"
         Top             =   330
         Width           =   5925
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Monitor && Display Settings"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   2550
      TabIndex        =   0
      Top             =   450
      Width           =   2805
      Begin VB.CheckBox MaxMon 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   90
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   270
         Width           =   855
      End
      Begin VB.CheckBox MonitorSetting 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Child"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   270
         Value           =   1  'Checked
         Width           =   855
      End
      Begin VB.CheckBox MonitorSetting 
         Caption         =   "Parent"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   990
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   270
         Width           =   855
      End
   End
   Begin VB.Label Label5 
      Caption         =   "Server Reference Time"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6960
      TabIndex        =   42
      Top             =   90
      Width           =   2085
   End
End
Attribute VB_Name = "MySetUp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public UsedGldTabKeys As String
Public NoopListIsOpen As Boolean
Dim UseTimeTag As String

Private Sub chkAppl_Click(Index As Integer)
    Select Case Index
        Case 0
            If chkAppl(Index).Value = 1 Then
                MainDialog.TotalReset = True
                MainDialog.ResetMain 0, False
                MainDialog.TotalReset = False
                InitForm True
                MainDialog.InitApplication
                chkAppl(Index).Value = 0
            End If
        Case 1
            If chkAppl(Index).Value = 1 Then
                chkAppl(Index).Value = 0
                MainDialog.chkAppl(4).Value = 0
                MainDialog.chkAppl(4).BackColor = vbButtonFace
                Me.Hide
            End If
        Case Else
    End Select
End Sub

Private Sub chkReadDbf_Click()
    If Not RestoreAction Then
        If chkReadDbf.Value = 1 Then
            chkReadDbf.BackColor = LightGreen
            DbfReader.Show
        Else
            chkReadDbf.BackColor = vbButtonFace
            DbfReader.Hide
        End If
    End If
End Sub

Private Sub chkFilter_Click()
    If Not RestoreAction Then
        MainDialog.chkFilter.Value = chkFilter.Value
        If chkFilter.Value = 1 Then chkFilter.BackColor = LightGreen Else chkFilter.BackColor = vbButtonFace
        Me.Refresh
    End If
End Sub

Private Sub chkLoad_Click()
    If Not RestoreAction Then
        MainDialog.chkLoad.Value = chkLoad.Value
        If chkLoad.Value = 1 Then chkLoad.BackColor = LightGreen Else chkLoad.BackColor = vbButtonFace
        Me.Refresh
    End If
End Sub

Private Sub chkServer_Click(Index As Integer)
    If Not RestoreAction Then
        If chkServer(Index).Value = 1 Then chkServer(Index).BackColor = LightGreen Else chkServer(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkSplit_Click()
    If Not RestoreAction Then
        MainDialog.chkSplit.Value = chkSplit.Value
        If chkSplit.Value = 1 Then chkSplit.BackColor = LightGreen Else chkSplit.BackColor = vbButtonFace
        Me.Refresh
    End If
End Sub

Private Sub chkWork_Click(Index As Integer)
    If Not RestoreAction Then
        If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0
                If chkWork(Index).Value = 1 Then
                    If MySetUp.MonitorSetting(3).Value = 1 Then
                        SeasonData.Show , Me
                    Else
                        SeasonData.Show
                    End If
                Else
                    SeasonData.Hide
                End If
            Case 1
                If chkWork(Index).Value = 1 Then
                    MainDialog.chkClear.Enabled = True
                    MainDialog.ActSeason(3).Enabled = True
                Else
                    MainDialog.chkClear.Enabled = False
                    MainDialog.ActSeason(3).Enabled = False
                End If
            Case 3  'load
                If chkWork(Index).Value = 1 Then
                    chkWork(4).Value = 0
                    LoadGdlTabData
                    chkWork(Index).Value = 0
                End If
            Case 4
                If chkWork(Index).Value = 1 Then
                    GdlTabData.InsertBuffer UsedGldTabKeys, vbLf
                    GdlTabData.AutoSizeColumns
                    GdlTabData.RedrawTab
                    If UsedGldTabKeys <> "" Then
                        MainDialog.chkAppl(4).BackColor = vbYellow
                    End If
                Else
                    GdlTabData.ResetContent
                    GdlTabData.RedrawTab
                End If
            Case Else
        End Select
        If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_Load()
    InitForm False
    InitTabs
End Sub
Private Sub InitTabs()
    GdlTabData.HeaderString = "TYPE,LKEY,LVAL,RVAL,URNO,Remark" & Space(30)
    GdlTabData.HeaderLengthString = "90,90,180,180,180,300"
    GdlTabData.FontSize = 17
    GdlTabData.HeaderFontSize = 17
    GdlTabData.LineHeight = 17
    GdlTabData.EmptyAreaRightColor = vbWhite
    GdlTabData.SetTabFontBold True
    GdlTabData.ShowVertScroller True
    GdlTabData.ShowHorzScroller True
    GdlTabData.ResetContent
    GdlTabData.AutoSizeByHeader = True
    GdlTabData.AutoSizeColumns
    
    SessionTab.HeaderString = "File,Data,Range,Type,Imp,Lines,Count,Remark" & Space(30)
    SessionTab.HeaderLengthString = "90,90,180,180,180,90,90,300"
    SessionTab.ColumnAlignmentString = "L,L,L,L,L,R,R,L"
    SessionTab.FontSize = 17
    SessionTab.HeaderFontSize = 17
    SessionTab.LineHeight = 17
    SessionTab.EmptyAreaRightColor = vbWhite
    SessionTab.SetTabFontBold True
    SessionTab.ShowVertScroller True
    SessionTab.ShowHorzScroller True
    SessionTab.ResetContent
    SessionTab.AutoSizeByHeader = True
    SessionTab.AutoSizeColumns
End Sub

Private Sub LoadGdlTabData()
    Dim FldLst As String
    Dim SqlKey As String
    Dim RetCode As Integer
    Dim tmpResult As String
    Dim CurLine As Long
    Dim MaxLine As Long
    FldLst = "TYPE,LKEY,LVAL,RVAL,URNO"
    SqlKey = "ORDER BY TYPE,LKEY,LVAL DESC"
    GdlTabData.ResetContent
    RetCode = UfisServer.CallCeda(tmpResult, "RT", "GDLTAB", FldLst, "", SqlKey, "", 0, True, False)
    tmpResult = TrimRecordData(tmpResult)
    GdlTabData.InsertBuffer tmpResult, vbLf
    'GdlTabData.Sort "2", True, True
    'GdlTabData.Sort "1", True, True
    'GdlTabData.Sort "0", True, True
    GdlTabData.AutoSizeColumns
    GdlTabData.RedrawTab
End Sub

Private Function TrimRecordData(RecData As String) As String
    Dim OldResult As String
    Dim NewResult As String
    Dim LastNonBlank As Long
    Dim CurPos As Long
    Dim MaxPos As Long
    OldResult = RecData
    NewResult = Replace(OldResult, " ,", ",", 1, -1, vbBinaryCompare)
    While NewResult <> OldResult
        OldResult = NewResult
        NewResult = Replace(OldResult, " ,", ",", 1, -1, vbBinaryCompare)
    Wend
    TrimRecordData = RTrim(NewResult)
End Function


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        MainDialog.chkAppl(4).Value = 0
        MainDialog.chkAppl(4).BackColor = vbButtonFace
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'MainDialog.chkAppl(4).Value = 0
End Sub

Private Sub FontSlider_Scroll()
    Refresh
    MainDialog.InitGrids False
    If MainDialog.chkExtract.Value = 1 Then
        FlightExtract.ChangeMyDataFont FontSlider.Value
        If FlightExtract.chkWork(7).Value = 1 Then FlightDetails.ChangeMyDataFont FontSlider.Value
        If FlightExtract.chkWork(14).Value = 1 Then FlightStatistic.ChangeMyDataFont FontSlider.Value
    End If
End Sub

Public Sub InitForm(FullReset As Boolean)
    Static IsInit As Boolean
    Dim tmpData As String
    Dim X As String
    Dim SrvSec As Long
    Dim CliSec As Long
    If (FullReset) Or (Not IsInit) Then
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "FONT_SIZE", "8,8,30")
        FontSlider.Min = Val(GetItem(tmpData, 1, ","))
        FontSlider.Value = Val(GetItem(tmpData, 2, ","))
        FontSlider.Max = Val(GetItem(tmpData, 3, ","))
        MaxMon.Caption = GetIniEntry(myIniFullName, "MAIN", "", "MONITORS", "AUTO")
        If Val(MaxMon.Caption) <= 0 Then AutoMonCount
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "USE_FKEY_TIME", "NO")
        If tmpData = "YES" Then UseFkeyTime = True Else UseFkeyTime = False
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "SHRINK_NEW_DEL", "NO")
        If tmpData = "YES" Then ShrinkNewDel = True Else ShrinkNewDel = False
        DefaultFtyp = GetIniEntry(myIniFullName, "MAIN", "", "DEFAULT_FTYP", "S")
        FtypOnDelete = GetIniEntry(myIniFullName, "MAIN", "", "FTYP_ON_DELETE", "N")
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "NOOP_ON_DELETE", "YES")
        If tmpData = "YES" Then UpdateFtypOnDelete = True Else UpdateFtypOnDelete = False
        AutoPatchHomeApc = GetIniEntry(myIniFullName, "MAIN", "", "PATCH_LOCAL_APC", "")
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "SHOW_STOP_BUTTON", "NO")
        If tmpData = "YES" Then ShowStopButton = True Else ShowStopButton = False
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "USE_CDRHDL", "YES")
        If tmpData = "YES" Then chkServer(0).Value = 1 Else chkServer(0).Value = 0
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "ENABLE_CDR", "NO")
        If tmpData = "YES" Then chkServer(0).Enabled = True Else chkServer(0).Enabled = False
        tmpData = GetIniEntry(myIniFullName, "MAIN", "", "IMPORT_HOPO_LOCAL", "NO")
        If tmpData = "YES" Then ImportHopoLocal = True Else ImportHopoLocal = False
        UfisServer.SetUtcTimeDiff
        X = UfisServer.GetServerConfigItem("SRVT", 1, Format(Now, "yyyymmddhhmmss"))
        SrvSec = Val(Right(X, 2))
        Mid(X, 13, 2) = "00"
        ServerTime.Tag = X
        UseTimeTag = GetIniEntry(myIniFullName, "MAIN", "", "SET_SERVER_TIME", "")
        If UseTimeTag <> "" Then ServerTime.Tag = UseTimeTag
        ServerTimeType.Text = UfisServer.GetServerConfigItem("SRVT", 2, "LOC")
        ServerTime.Text = Format(CedaFullDateToVb(ServerTime.Tag), ("dd.mm.yyyy \/ hh:mm"))
        MyUtcTimeDiff.Text = Trim(Str(UtcTimeDiff))
        SynchronizeTimeDisplays
        CliSec = (60 - SrvSec) * 1000
        If CliSec <= 0 Then CliSec = 60000
        Timer1.Interval = CliSec
        Timer1.Enabled = True
        SetAutoClearLimits "MAIN"
        IsInit = True
    End If
End Sub

Private Sub GdlTabData_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If LineNo >= 0 Then
        'MsgBox Key
        Select Case Key
            Case 46 'Delete
                HandleGdlDelete LineNo
            Case Else
        End Select
    End If
End Sub
Private Sub HandleGdlDelete(LineNo As Long)
    Dim tmpUrno As String
    Dim SqlKey As String
    Dim tmpResult As String
    If chkWork(4).Value = 0 Then
        tmpUrno = GdlTabData.GetColumnValue(LineNo, 4)
        If (tmpUrno <> "") And (Left(tmpUrno, 1) <> "C") Then
            SqlKey = "WHERE URNO=" & tmpUrno
            UfisServer.CallCeda tmpResult, "DRT", "GDLTAB", "", "", SqlKey, "", 0, False, False
            GdlTabData.DeleteLine LineNo
            GdlTabData.RedrawTab
        End If
    End If
End Sub
Private Sub GdlTabData_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        GdlTabData.Sort Str(ColNo), True, True
    End If
End Sub

Private Sub MaxMon_Click()
    Dim MsgTxt As String
    Dim Answ As Integer
    If MaxMon.Value = 1 Then
        MaxMon.BackColor = LightestGreen
        MsgTxt = ""
        MsgTxt = MsgTxt & "Size: " & Trim(Str(Screen.Width / Screen.TwipsPerPixelX)) & "/" & Trim(Str(Screen.Height / Screen.TwipsPerPixelY)) & " Pixel" & vbNewLine
        MsgTxt = MsgTxt & "Monitors: " & Trim(Str(DetermineMonitors))
        Answ = MyMsgBox.CallAskUser(0, 0, 0, "Monitor Control", MsgTxt, "screen", "", UserAnswer)
        MaxMon.Value = 0
    Else
        MaxMon.BackColor = vbButtonFace
    End If
End Sub
Private Function DetermineMonitors() As Integer
    DetermineMonitors = Screen.Width / Screen.Height / (800 / 600)
End Function
Private Sub AutoMonCount()
    MaxMon.Caption = Str(DetermineMonitors)
End Sub
Private Sub MonitorSetting_Click(Index As Integer)
    Dim tmpSetting As String
    Dim tmpData As String
    If Not RestoreAction Then
        Select Case Index
            Case 0 To 1
                If MonitorSetting(Index).Value = 1 Then
                    MonitorSetting(1 - Index).Value = 0
                    Refresh
                    If Index = 0 Then
                        MainDialog.ToggleDateFormat False
                    Else
                        MainDialog.ToggleDateFormat True
                    End If
                Else
                    MonitorSetting(1 - Index).Value = 1
                End If
            Case 2
                If MonitorSetting(Index).Value = 1 Then MonitorSetting(3).Value = 0 Else MonitorSetting(3).Value = 1
            Case 3
                If MonitorSetting(Index).Value = 1 Then MonitorSetting(2).Value = 0 Else MonitorSetting(2).Value = 1
            Case Else
        End Select
        If MonitorSetting(Index).Value = 1 Then MonitorSetting(Index).BackColor = LightGreen Else MonitorSetting(Index).BackColor = vbButtonFace
    End If
End Sub

Public Function DetermineFileType(ForWhat As String) As String
    Dim ForWhatCode As String
    Dim CurFilePath As String
    Dim CurFileName As String
    Dim SessionRec As String
    Dim ErrMsg As String
    Dim ErrLineNo As Long
    Dim CurItem As Long
    Dim ErrLineList As String
    Dim ErrLineNbr As String
    Dim FileCount As Integer
    
    Dim i As Integer
    DetermineFileType = "NOK"
    ForWhatCode = GetItem(ForWhat, 1, ",")
    Select Case ForWhatCode
        Case "SESSION"
            For i = 0 To CurFileInfo.Count - 1
                CurFileInfo(i).Caption = ""
            Next
            SessionTab.ResetContent
            SessionTab.Refresh
            Me.Refresh
            MainDialog.chkAppl(4).ForeColor = vbBlack
            MainDialog.chkAppl(4).BackColor = vbButtonFace
            DetermineFileType = "OK"
        Case "RESET"
            DetermineFileType = "OK"
            For i = 0 To CurFileInfo.Count - 1
                CurFileInfo(i).Caption = ""
            Next
        Case "INIT"
            CurFileInfo(0).Caption = GetItem(ForWhat, 2, Chr(30))
            FileCount = GetFileAndPath(CurFileInfo(0).Caption, CurFileName, CurFilePath)
            If FileCount > 1 Then CurFileName = GetItem(CurFileName, 1, " ")
            SessionTab.Tag = CurFileName
            MyFileInfo.Caption = CurFileName + " Import File Information"
            CurFileInfo(3).Caption = DataSystemType
            CurFileInfo(6).Caption = HomeAirport
            DetermineFileType = "OK"
            Select Case DataSystemType
                Case "SLOT"
                    DetermineFileType = CheckSlotFile(CurFilePath, CurFileName)
                Case "SSIM"
                    DetermineFileType = CheckSsimFile(CurFilePath, CurFileName)
                Case "OAFOS"
                    DetermineFileType = CheckOafosFile(CurFilePath, CurFileName)
                Case "FHK"
                Case "RMSLNK"
                    DetermineFileType = CheckRmsLinkFile(CurFilePath, CurFileName, ForWhat)
                Case Else
            End Select
        Case "HEADER", "FOOTER"
            Select Case DataSystemType
                Case "SLOT"
                    DetermineFileType = CheckSlotFile(CurFilePath, CurFileName)
                Case "SSIM"
                    DetermineFileType = CheckSsimFile(CurFilePath, CurFileName)
                Case "OAFOS"
                    DetermineFileType = CheckOafosFile(CurFilePath, CurFileName)
                Case "FHK"
                    DetermineFileType = CheckFhkFile(CurFilePath, CurFileName)
                Case "RMSLNK"
                    DetermineFileType = CheckRmsLinkFile(CurFilePath, CurFileName, ForWhat)
                Case Else
                    CurFileInfo(5).Caption = "Full"
                    DetermineFileType = "OK"
            End Select
        Case "FINISH"
            DetermineFileType = "OK"
            Select Case DataSystemType
                Case "SLOT"
                    DetermineFileType = CheckSlotFile(CurFilePath, CurFileName)
                Case "SSIM"
                    DetermineFileType = CheckSsimFile(CurFilePath, CurFileName)
                Case "OAFOS"
                    DetermineFileType = CheckOafosFile(CurFilePath, CurFileName)
                Case "FHK"
                    'DetermineFileType = CheckFhkFile(CurFilePath, CurFileName)
                Case "RMSLNK"
                    'DetermineFileType = CheckRmsLinkFile(CurFilePath, CurFileName, ForWhat)
                Case Else
                    CurFileInfo(5).Caption = "Full"
                    DetermineFileType = "OK"
            End Select
            ErrLineList = SessionTab.GetLinesByBackColor(vbRed)
            If ErrLineList <> "" Then
                ErrMsg = ""
                CurItem = 0
                ErrLineNbr = GetRealItem(ErrLineList, CurItem, ",")
                While ErrLineNbr <> ""
                    ErrLineNo = Val(ErrLineNbr)
                    ErrMsg = ErrMsg & SessionTab.GetColumnValue(ErrLineNo, 0) & ":" & vbNewLine
                    ErrMsg = ErrMsg & SessionTab.GetColumnValue(ErrLineNo, 7) & vbNewLine
                    
                    CurItem = CurItem + 1
                    ErrLineNbr = GetRealItem(ErrLineList, CurItem, ",")
                Wend
                DetermineFileType = ErrMsg
            End If
        Case Else
    End Select
    SessionTab.AutoSizeColumns
    SessionTab.Refresh
    If DataSystemMode = "FULL" Then CurFileInfo(5).Caption = "Full"
    If DataSystemMode = "LINK" Then CurFileInfo(5).Caption = "Link"
    If DataSystemMode = "DAYS" Then CurFileInfo(5).Caption = "Update"
    If CurFileInfo(5).Caption = "Update" Then UpdateMode = True Else UpdateMode = False
    If CurFileInfo(5).Caption = "Full" Then FullMode = True Else FullMode = False
    If CurFileInfo(5).Caption = "Link" Then RotationMode = True Else RotationMode = False
    If UpdateMode Then chkWork(1).Enabled = False
    'MsgBox "RMS Link Mode Modified !!!"
    'FullMode = True
    'RotationMode = False
    'UpdateMode = False
    Me.Refresh
End Function

Private Function CheckRmsLinkFile(CurFilePath As String, CurFileName As String, ForWhat As String) As String
    Dim Result As String
    Dim ErrMsg As String
    Dim ActMsg As String
    Dim FirstLine As String
    Dim LastLine As String
    Dim SessionRec As String
    Dim tmpData As String
    Dim ForWhatCode As String
    Dim ActGrpLineCnt As Long
    Dim ImpGrpLineCnt As Long
    Dim LineNo As Long
    ErrMsg = ""
    ActMsg = ""
    Result = "OK"
    ForWhatCode = GetItem(ForWhat, 1, ",")
    Select Case ForWhatCode
        Case "INIT"
            CurFileInfo(4).Caption = "Season"
            CurFileInfo(5).Caption = "Link"
            CurFileInfo(9).Caption = MainDialog.UsedTimeType
            If MainDialog.UsedTimeType = "APC" Then CurFileInfo(10).Caption = "APC" Else CurFileInfo(10).Caption = MyUtcTimeDiff.Text
        Case "HEADER"
            CurFileInfo(1).Caption = GetItem(ForWhat, 2, Chr(30))
            FirstLine = CurFileInfo(1).Caption
            tmpData = Trim(Mid(FirstLine, 23, 5))
            If tmpData <> "" Then
                CurFileInfo(8).Caption = tmpData
            Else
                CurFileInfo(8).Caption = "???"
                ActMsg = ActMsg & "Missing Aircraft Type" & vbNewLine
                ErrMsg = ErrMsg & "ACT5,"
                Result = "NOK"
            End If
        Case "FOOTER"
            CurFileInfo(2).Caption = GetItem(ForWhat, 2, Chr(30))
            CurFileInfo(2).Tag = GetItem(ForWhat, 2, ",")
            FirstLine = CurFileInfo(1).Caption
            LastLine = CurFileInfo(2).Caption
            tmpData = Left(FirstLine, 1)
            If tmpData <> "0" Then
                ActMsg = ActMsg & "Wrong Header: '" & tmpData & "'" & vbNewLine
                ErrMsg = ErrMsg & "HEAD,"
                Result = "NOK"
            End If
            tmpData = Trim(Mid(FirstLine, 2, 8))
            If tmpData <> "" Then
                If tmpData <> "SLOTS" Then
                    ActMsg = ActMsg & "Wrong System Name: '" & tmpData & "'" & vbNewLine
                    ErrMsg = ErrMsg & "NAME,"
                    Result = "NOK"
                End If
            End If
            tmpData = Trim(Mid(FirstLine, 10, 10))
            If tmpData <> "" Then
        '        If tmpData <> "SLOTS" Then
        '            ActMsg = ActMsg & "Wrong Creation Date: '" & tmpData & "'" & vbNewLine
        '            ErrMsg = ErrMsg & "DATE,"
        '            Result = "NOK"
        '        End If
            End If
            tmpData = Trim(Mid(FirstLine, 20, 3))
            If tmpData <> "" Then
        '        If tmpData <> "SLOTS" Then
        '            ActMsg = ActMsg & "Wrong Season: '" & tmpData & "'" & vbNewLine
        '            ErrMsg = ErrMsg & "SEAS,"
        '            Result = "NOK"
        '        End If
                CurFileInfo(7).Caption = tmpData
            Else
                ActMsg = ActMsg & "Missing Season Info" & vbNewLine
                ErrMsg = ErrMsg & "SEAS,"
                Result = "NOK"
            End If
            tmpData = Trim(Mid(FirstLine, 23, 5))
            If tmpData <> "" Then
                CurFileInfo(8).Caption = tmpData
            Else
                ActMsg = ActMsg & "Missing Aircraft Type" & vbNewLine
                ErrMsg = ErrMsg & "ACT5,"
                Result = "NOK"
            End If
            ActGrpLineCnt = Val(CurFileInfo(2).Tag)
            ImpGrpLineCnt = Val(Mid(LastLine, 10, 7))
            If ActGrpLineCnt <> ImpGrpLineCnt Then
                ActMsg = ActMsg & "Different Line Count ("
                ActMsg = ActMsg & "File:" & Str(ImpGrpLineCnt) & " / "
                ActMsg = ActMsg & "Read:" & Str(ActGrpLineCnt) & ")" & vbNewLine
                ErrMsg = ErrMsg & "COUNT,"
                Result = "NOK"
            End If
            CurFileInfo(11).Caption = ActMsg
            CurFileInfo(12).Caption = ErrMsg
            SessionRec = SessionTab.Tag & ","
            SessionRec = SessionRec & CurFileInfo(8).Caption & ","
            SessionRec = SessionRec & CurFileInfo(4).Caption & ","
            SessionRec = SessionRec & CurFileInfo(5).Caption & ","
            SessionRec = SessionRec & CurFileInfo(7).Caption & ","
            SessionRec = SessionRec & Trim(Str(ImpGrpLineCnt)) & ","
            SessionRec = SessionRec & Trim(Str(ActGrpLineCnt)) & ","
            SessionRec = SessionRec & ActMsg & ","
            SessionTab.InsertTextLine SessionRec, False
            If Result = "NOK" Then
                LineNo = SessionTab.GetLineCount - 1
                SessionTab.SetLineColor LineNo, vbWhite, vbRed
            End If
        Case Else
    End Select
    CheckRmsLinkFile = Result
End Function

Private Function CheckFhkFile(CurFilePath As String, CurFileName As String) As String
    Dim Result As String
    Dim FirstLine As String
    Dim LastLine As String
    Dim FileHopo As String
    Dim LineHopo As String
    Dim ToolHopo As String
    Dim FileType As String
    Dim LineType As String
    Dim FileSyst As String
    Dim LineSyst As String
    Dim FileSeas As String
    Dim ToolSeas As String
    Dim LineSeas As String
    Dim FileYear As String
    Dim FileMonth As String
    Dim FileDay As String
    Dim FileDate As String
    Dim FileSeason As String
    Dim ValidSeason As String
    Dim ToolLines As Long
    Dim filelines As Long
    Dim ErrMsg As String
    Dim ActMsg As String
    ErrMsg = ""
    ActMsg = ""
    FirstLine = CurFileInfo(1).Caption
    LastLine = CurFileInfo(2).Caption
    FileYear = Mid(CurFileName, 11, 2)
    FileMonth = Mid(CurFileName, 4, 2)
    FileDay = Mid(CurFileName, 6, 2)
    FileDate = "20" & FileYear & FileMonth & FileDay
    ValidSeason = SeasonData.GetValidSeason(FileDate)
    FileSeason = GetItem(ValidSeason, 1, ",")
    LineSeas = UCase(GetItem(FirstLine, 6, ";"))
    If UCase(GetItem(FirstLine, 2, ";")) <> "FHK" Then ErrMsg = ErrMsg & "FHK?,"
    ToolHopo = HomeAirport
    FileHopo = UCase(Left(CurFileName, 3))
    LineHopo = UCase(GetItem(FirstLine, 3, ";"))
    If (FileHopo <> LineHopo) Or (FileHopo <> ToolHopo) Then ErrMsg = ErrMsg & "HOPO,"
    FileSyst = UCase(Mid(CurFileName, 8, 1))
    LineSyst = UCase(GetItem(FirstLine, 5, ";"))
    If FileSyst <> LineSyst Then ErrMsg = ErrMsg & "S?G?,"
    Select Case LineSyst
        Case "G"
            CurFileInfo(8).Caption = "GA"
        Case "S"
            CurFileInfo(8).Caption = "S/C"
        Case Else
            CurFileInfo(8).Caption = "?"
    End Select
    FileType = UCase(Mid(CurFileName, 10, 1))
    LineType = UCase(GetItem(FirstLine, 1, ";"))
    Select Case LineType
        Case "DN"
            CurFileInfo(4).Caption = "Daily"
            CurFileInfo(5).Caption = "Full"
            FileSeas = FileSeason
        Case "DU"
            CurFileInfo(4).Caption = "Daily"
            CurFileInfo(5).Caption = "Update"
            FileSeas = FileSeason
        Case "SU"
            CurFileInfo(4).Caption = "Season"
            CurFileInfo(5).Caption = "Update"
        Case "SN"
            CurFileInfo(4).Caption = "Season"
            CurFileInfo(5).Caption = "Full"
        Case Else
            CurFileInfo(4).Caption = "?"
            CurFileInfo(5).Caption = "?"
    End Select
    Select Case FileType
        Case "D"
            FileSeas = FileSeason
            If (LineType <> "DN") And (LineType <> "DU") Then ErrMsg = ErrMsg & "TYPE,"
        Case "U"
            FileSeas = "S" & FileYear
            If LineType <> "SU" Then ErrMsg = ErrMsg & "TYPE,"
        Case "V"
            FileSeas = "W" & FileYear
            If LineType <> "SU" Then ErrMsg = ErrMsg & "TYPE,"
        Case "W"
            FileSeas = "W" & FileYear
            If LineType <> "SN" Then ErrMsg = ErrMsg & "TYPE,"
        Case "X"
            FileSeas = "S" & FileYear
            If LineType <> "SN" Then ErrMsg = ErrMsg & "TYPE,"
        Case Else
            FileSeas = LineSeas
    End Select
    CurFileInfo(7).Caption = FileSeas
    CurFileInfo(9).Caption = GetItem(FirstLine, 9, ";")
    ToolSeas = Trim(MainDialog.ActSeason(0).Caption)
    If LineSeas = "" Then LineSeas = FileSeas
    If ToolSeas = "" Then ToolSeas = LineSeas
    If (ToolSeas <> FileSeas) Or (ToolSeas <> LineSeas) Then ErrMsg = ErrMsg & "SEAS,"
    If LastLine <> "" Then
        If UCase(GetItem(LastLine, 1, ";")) <> "E" Then ErrMsg = ErrMsg & "LAST,"
        ToolLines = Val(MainDialog.StatusBar.Panels(2).Text)
        filelines = Val(GetItem(LastLine, 2, ";"))
        If ToolLines <> filelines Then ErrMsg = ErrMsg & "COUNT,"
    End If
    If (CurFileInfo(4) <> "Season") Or (CurFileInfo(8) <> "S/C") Then
        ActMsg = "Not supported !"
        Result = "NOK"
    Else
        ActMsg = "OK"
        Result = "OK"
    End If
    If ErrMsg <> "" Then ErrMsg = Left(ErrMsg, Len(ErrMsg) - 1)
    If (ErrMsg <> "") Or (ActMsg <> "OK") Then
        MainDialog.chkAppl(4).ForeColor = vbRed
        'MainDialog.chkAppl(4).BackColor = vbRed
    Else
        MainDialog.chkAppl(4).ForeColor = vbBlack
        MainDialog.chkAppl(4).BackColor = vbButtonFace
    End If
    CurFileInfo(11).Caption = ActMsg
    CurFileInfo(12).Caption = ErrMsg
    CheckFhkFile = Result
End Function

Private Function CheckSlotFile(CurFilePath As String, CurFileName As String) As String
    Dim Result As String
    CurFileInfo(5).Caption = "Full"
    Result = "OK"
    CheckSlotFile = Result
End Function

Private Function CheckSsimFile(CurFilePath As String, CurFileName As String) As String
    Dim Result As String
    CurFileInfo(5).Caption = "Full"
    Result = "OK"
    CheckSsimFile = Result
End Function

Private Function CheckOafosFile(CurFilePath As String, CurFileName As String) As String
    Dim Result As String
    CurFileInfo(5).Caption = "Update"
    Result = "OK"
    CheckOafosFile = Result
End Function

Private Sub Timer1_Timer()
    Dim CurTime
    CurTime = CedaFullDateToVb(ServerTime.Tag)
    CurTime = DateAdd("n", 1, CurTime)
    ServerTime.Text = Format(CurTime, ("dd.mm.yyyy \/ hh:mm"))
    ServerTime.Tag = Format(CurTime, "yyyymmddhhmmss")
    If Timer1.Interval < 60000 Then Timer1.Interval = 60000
    SynchronizeTimeDisplays
End Sub

Public Sub SynchronizeTimeDisplays()
    Dim UseServerTime
    Dim UseMinOpTime
    Dim CedaTime As String
    If NoopListIsOpen Then
        UseServerTime = CedaFullDateToVb(ServerTime.Tag)
        If ServerTimeType <> "UTC" Then UseServerTime = DateAdd("n", -UtcTimeDiff, UseServerTime)
        UseMinOpTime = UseServerTime
        UseServerTime = DateAdd("d", Abs(Val(AutoClear.Caption)), UseServerTime)
        CedaTime = Format(UseServerTime, "yyyymmdd") & "000000"
        UseServerTime = CedaFullDateToVb(CedaTime)
        UseMinOpTime = DateAdd("h", Abs(Val(OpTimeOffset.Caption)), UseMinOpTime)
        If UseMinOpTime > UseServerTime Then UseServerTime = UseMinOpTime
        NoopFlights.ServerTime.Text = Format(UseServerTime, ("dd.mm.yyyy \/ hh:mm"))
        NoopFlights.ServerTime.Tag = Format(UseServerTime, "yyyymmddhhmm")
        If UseTimeTag <> "" Then
            NoopFlights.ServerTime.BackColor = vbRed
            NoopFlights.ServerTime.ForeColor = vbWhite
        End If
    End If
End Sub

Public Sub SetAutoClearLimits(useSection As String)
    Dim tmpData As String
    Dim tmpItmDat As String
    tmpData = GetIniEntry(myIniFullName, useSection, "", "AUTO_CLEAR", "NO,0")
    tmpItmDat = GetItem(tmpData, 1, ",")
    If tmpItmDat = "YES" Then
        MySetUp.chkWork(1).Value = 0
        MySetUp.chkWork(1).Value = 1
    Else
        MySetUp.chkWork(1).Value = 1
        MySetUp.chkWork(1).Value = 0
    End If
    tmpItmDat = GetItem(tmpData, 2, ",")
    MySetUp.AutoClear.Caption = Trim(Str(Abs(Val(tmpItmDat))))
    tmpData = GetIniEntry(myIniFullName, useSection, "", "TIME_OFFSET", "4")
    MySetUp.OpTimeOffset.Caption = Trim(Str(Abs(Val(tmpData))))
End Sub
