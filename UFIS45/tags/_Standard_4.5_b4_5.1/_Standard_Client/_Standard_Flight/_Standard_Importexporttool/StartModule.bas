Attribute VB_Name = "StartModule"
Option Explicit

Public DefaultFileHeader As String

Public StopMyApplEvents As Boolean
Public StrTabCaller As String
Public CurSeasData As String
Public CurSeasName As String
Public CurSeasVpfr As String
Public CurSeasVpto As String

Public CurFdIdx As Integer
Public DoubleGridInit As Boolean
Public IsDoubleGridView As Boolean
Public Tab2IsUsed As Boolean

Public myLoadPath As String
Public myLoadName As String
Public CurLoadPath As String
Public CurLoadName As String

Public DateFieldFormat As String
Public UseDefaultSeason As Boolean
Public DefaultSeasonRec As String

Public SeasonActYear As String
Public SeasonNxtYear As String
Public SeasonBegin As String
Public SeasonEnd As String
Public SeasonFirstMonth As Integer
Public SeasonLastMonth As Integer

Public PreCheckFile As Boolean
Public StopFileInput As Boolean

Public MainFieldList As String
Public HeadFieldList As String
Public LogicalMainFieldList As String
Public LogicalHeadFieldList As String
Public SSIM7FieldGroups As String
Public MainExpandHeader As String
Public MainFldWidLst As String
Public MainColWidLst As String
Public MainHeaderTitle As String
Public MainHeaderWidth As String
Public MainHeaderColor As String
Public ExpPosLst As String
Public FldLenLst As String
Public CurrentHeader As String
Public CurFldPosLst As String

Public ImportHopoLocal As Boolean
Public FileTimeMode As String
Public DataTimeMode As String
Public AodbTimeMode As String
Public NextTimeMode As String
Public SsimSchedBegin As String
Public SsimSchedEnd As String
Public ImportActionType As String
Public StopAllLoops As Boolean
Public UseFileReader As Boolean
Public NewLayoutType As Boolean
Public DataSystemType As String
Public DataDisplayType As String
Public DataSystemMode As String
Public DataSystemAlc3 As String
Public CheckFilePeriod As String
Public FullMode As Boolean
Public UpdateMode As Boolean
Public CheckAutoUpdateMode As Boolean
Public AutoUpdateMode As Boolean
Public RotationMode As Boolean
Public NoopFullMode As Boolean
Public NoopCheckDoubles As Boolean
Public StartedAsImportTool As Boolean
Public StartedAsCompareTool As Boolean
Public ShowStrTabStyle As Integer
Public UseGfrCmd As String
Public AlcErrorList As String
Public ChkErrorList As String
Public AutoSwitch As Boolean
Public Initializing As Boolean
Public FullFieldList As String
Public DateFieldList As String
Public TimeFieldList As String
Public myViewFieldList As String
Public mySlotFieldList As String
Public MainPacketMinDate As String
Public MainPacketMaxDate As String
Public RoutWithTimes As Boolean
Public UseStypField As String
Public MainStypField As String
Public AutoFileTtyp As String
Public DefaultTtyp As String
Public FilterCalledAsDispo As Boolean
Public InfoServerIsConnected As Boolean
Public MinImpVpfr As String
Public MaxImpVpto As String
Public UseRoutField As Boolean
Public UseFldaField As Boolean
Public ShrinkNewDel As Boolean
Public UseFkeyTime As Boolean
Public ShowStopButton As Boolean
Public CheckStopButton As Boolean
Public ImportAct3Act5 As Boolean
Public DefaultFtyp As String
Public UpdateFtypOnDelete As Boolean
Public FtypOnDelete As String
Public AutoPatchHomeApc As String
Public gsUserName As String

'Feature SCORE Append Values
'First we need Flags
Public AddOnDataFound As Boolean
Public AddOnDataMerge As Boolean
Public AddOnDataLength As Integer
'As well we keep the input lines
Public AddOnLineAppend As String
Public AddOnLineHeader As String
'We must create extensions
'for some config KeyWords
Public AddOnMainTitle As String 'MAIN_HEADER_TITLE
Public AddOnMainWidth As String 'MAIN_HEADER_WIDTH
Public AddOnMainFields As String 'MAIN_FIELDS
Public AddOnFinalHead As String 'FINAL_HEAD
Public AddOnFinalLen As String  'FINAL_LEN
Public AddOnFinalPos As String  'FINAL_POS
Public AddOnGridSize As String  'Internal HeaderLengthString
'Finally we need the AddOn field names
'for Arr/Dep in order to pick
'the values from the grid (AddOnFields)
'and to send them to Ceda (AftFields)
Public AddOnArrFields As String
Public AddOnArrAftFld As String
Public AddOnDepFields As String
Public AddOnDepAftFld As String

'Public AddOnArrTitles As String
'Public AddOnDepTitles As String
'Public AddOnArrFldPos As String
'Public AddOnArrFldLen As String
'Public AddOnDepFldPos As String
'Public AddOnDepFldLen As String

Sub Main()
    Dim LoginIsOk As Boolean
    Dim tmpStr As String
    Dim i As Integer
    Dim tmpParam1 As String
    Dim tmpParam2 As String
    
    FullFieldList = "VPFR,VPTO,FRQD,FRQW,FLCA,FLTA,STOA,FLCD,FLTD,DAOF,OVNI,ACT3,ACT5,ACTI,ROUT"
    mySlotFieldList = "FLCA,FLTA,FLCD,FLTD,VPFR,VPTO,FREQ,ACT3,NOSE,ORG3,VSA3,STOA,STOD,DAOF,VSD3,DES3,NATA,NATD,FREW,REGN,FTYP,ACT5,ORG4,VSA4,DES4,VSD4"
    DateFieldList = "VPFR,VPTO"
    TimeFieldList = "STOD,STOA"
    UseGfrCmd = "GFR"
    Load UfisServer
    For i = 6 To 8
        Load UfisServer.BasicData(i)
    Next
    For i = 0 To UfisServer.BasicData.UBound
        UfisServer.BasicData(i).ResetContent
    Next
    FileTimeMode = "-"
    DataTimeMode = "-"
    AodbTimeMode = "-"
    Load MyConfig
    'MyConfig.Show
    UfisServer.TwsCode.Text = ".NBC."
    LoginIsOk = False
    gsUserName = "ImportTool"
    If InStr(Command, "COMPARE") > 0 Then
        LoginIsOk = True
        tmpParam1 = GetRealItem(Command, 0, " ")
        tmpParam2 = GetRealItem(Command, 1, " ")
        If tmpParam2 <> "" Then
            gsUserName = tmpParam2
        End If
    End If
    If InStr(Command, "NOLOG") > 0 Then LoginIsOk = True
    If UfisServer.HostName = "LOCAL" Then LoginIsOk = True
'LoginIsOk = True
    If Not LoginIsOk Then LoginIsOk = LoginProcedure
    If Not LoginIsOk Then
        UfisServer.aCeda.CleanupCom
        End
    End If
    'myIniPath = "c:\ufis\system"
    myIniPath = UFIS_SYSTEM
    myIniFile = "ImportTool" & UfisServer.HOPO & ".ini"
    myIniFullName = myIniPath & "\" & myIniFile
    tmpStr = Dir(myIniFullName)
    If UCase(tmpStr) <> UCase(myIniFile) Then
        myIniFile = "ImportTool.ini"
        myIniFullName = myIniPath & "\" & myIniFile
    End If
    
    If InStr(Command, "SHOWSTRTAB") > 0 Then
        StartedAsImportTool = False
        ShowStrTabStyle = 1
        Load StandardRotations
        StandardRotations.Show
        StandardRotations.Refresh
    ElseIf InStr(Command, "COMPARE") > 0 Then
        StartedAsImportTool = False
        StartedAsCompareTool = True
        Load NoopFlights
        NoopFlights.Show
        NoopFlights.Refresh
    Else
        StartedAsImportTool = True
        ShowStrTabStyle = 0
        MainDialog.Show
        UfisServer.SetIndicator MainDialog.CedaStatus(0).Left, MainDialog.CedaStatus(0).Top, MainDialog.CedaStatus(0), MainDialog.CedaStatus(3), MainDialog.CedaStatus(1), MainDialog.CedaStatus(2)
        MainDialog.Refresh
    End If
    If UfisServer.ConnectToCeda = True Then
        Screen.MousePointer = 11
        SeasonData.LoadSeaTab
        Screen.MousePointer = 0
    Else
    
    End If
    UfisServer.ModName.Text = "ImportTool"
    UfisServer.ConnectToBcProxy
    ApplicationIsStarted = True
End Sub

Private Function LoginProcedure() As Boolean
    Dim LoginAnsw As String
    Dim tmpRegStrg As String
    Set UfisServer.LoginCall.UfisComCtrl = UfisServer.aCeda
    UfisServer.LoginCall.ApplicationName = "ImportTool"
    'LoginAnsw = UfisServer.LoginCall.DoLoginSilentMode("UFIS$ADMIN", "Passwort")
    tmpRegStrg = ""
    tmpRegStrg = tmpRegStrg & "ImportTool" & ","
    '                  FKTTAB:  SUBD   ,  FUNC  ,    FUAL              ,TYPE,STAT
    tmpRegStrg = tmpRegStrg & "InitModu,InitModu,Initialisieren (InitModu),B,-"
    'tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
    'tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
    UfisServer.LoginCall.RegisterApplicationString = tmpRegStrg
    UfisServer.LoginCall.InfoAppVersion = UfisServer.GetApplVersion(True)
    UfisServer.LoginCall.VersionString = UfisServer.GetApplVersion(True)
    UfisServer.LoginCall.InfoCaption = "Info about ImportFlights"
    UfisServer.LoginCall.InfoButtonVisible = True
    UfisServer.LoginCall.InfoUfisVersion = "UFIS Version 4.5"
    UfisServer.LoginCall.InfoAppVersion = CStr("ImportFlights " + UfisServer.GetApplVersion(True))
    UfisServer.LoginCall.InfoCopyright = "� 2001 - 2005 UFIS Airport Solutions GmbH"
    UfisServer.LoginCall.InfoAAT = "UFIS Airport Solutions GmbH"
    UfisServer.LoginCall.ShowLoginDialog
    LoginAnsw = UfisServer.LoginCall.GetPrivileges("InitModu")
    gsUserName = "ImportTool"
    If LoginAnsw <> "" Then
        LoginProcedure = True
        gsUserName = UfisServer.LoginCall.GetUserName()
        UfisServer.ModName.Tag = gsUserName
    Else
        LoginProcedure = False
    End If
End Function


Public Sub SetAllFormsOnTop(SetValue As Boolean)
'we do nothing here
End Sub

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
Dim tmpCOD As String
Dim tmpWKS As String
Dim tmpCmd As String
    If InStr("AFTTAB,PROGRESS", ObjName) > 0 Then
        'MsgBox "BC Received: " & ObjName & vbNewLine & CedaCmd & vbNewLine & CedaSqlKey & vbNewLine & Fields & vbNewLine & Data
        tmpWKS = GetItem(Tws, 6, ".")
        If CedaCmd = "IMX" Then
            ImportProgress.EvaluateBc RecvName, Tws, CedaSqlKey, Fields, Data
        ElseIf RecvName = UfisServer.GetMyWorkStationName Then
            'BC from my WKS
            tmpCOD = GetItem(Tws, 1, ".")
            If tmpCOD = "F" Then
                'Progress Message from OAFOS (REGN)
                Select Case CedaCmd
                    Case "RAC"
                        CedaProgress.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                        NoopFlights.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                    Case "RAX"
                        CedaProgress.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                        NoopFlights.EvaluateBc CedaCmd, Tws, CedaSqlKey, Fields, Data
                    Case "EXCO"
                        tmpCmd = GetItem(CedaSqlKey, 1, ",")
                        Select Case tmpCmd
                            Case "ACCBGN", "ACCEND"
                                CedaProgress.EvaluateBc tmpCmd, Tws, CedaSqlKey, Fields, Data
                                NoopFlights.EvaluateBc tmpCmd, Tws, CedaSqlKey, Fields, Data
                            Case Else
                        End Select
                    Case Else
                End Select
            End If
        End If
    ElseIf CedaCmd = "CLO" Then
        MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
        End
    End If
End Sub

Public Sub MsgFromUFISAppManager(Orig As Long, Data As String)
Dim tmpFldLst As String
Dim tmpDatLst As String
Dim nlPos As Integer
    If ApplicationIsStarted = True Then
        nlPos = InStr(Data, vbLf)
        If nlPos > 1 Then
            tmpFldLst = Left(Data, nlPos - 1)
            tmpDatLst = Mid(Data, nlPos + 1)
            'TelexPoolHead.SearchFlightTelexes tmpFldLst, tmpDatLst
        Else
            'If MyMsgBox.CallAskUser(0, 0, 0, "Communication Control", "Undefined message received from FIPS", "hand", "", UserAnswer) > 0 Then DoNothing
        End If
    End If
End Sub

Public Sub HandleDisplayChanged()
    
End Sub

Public Sub HandleSysColorsChanged()
    
End Sub

Public Sub HandleTimeChanged()
    
End Sub

Public Sub ShutDownApplication()
Dim i As Integer
Dim cnt As Integer
    On Error GoTo ErrorHandler
    If MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "Yes,No;F", UserAnswer) = 1 Then
        ShutDownRequested = True
        UfisServer.aCeda.CleanupCom
        CloseHtmlHelp
        'cnt = Forms.Count - 1
        'For i = cnt To 0 Step -1
        '    Unload Forms(i)
        'Next
        'Here we leave
        End
    End If
    'If not, we come back
    Exit Sub
ErrorHandler:
    Resume Next
End Sub

Public Sub RefreshMain()
    'do nothing
End Sub

Public Function TranslateItemList(CurItemList As String, CheckList As String, AllItems As Boolean) As String
    Dim Result As String
    Dim MainItem As String
    Dim SubItem As String
    Dim MainItemNo As Integer
    Dim SubItemNo As Integer
    Dim CurItemNo As Integer
    Dim CurItemTxt As String
    Result = CurItemList
    MainItemNo = 1
    MainItem = Trim(GetItem(CurItemList, MainItemNo, ","))
    While MainItem <> ""
        SubItemNo = 1
        SubItem = Trim(GetItem(MainItem, SubItemNo, "+"))
        While SubItem <> ""
            If Len(SubItem) = 4 Then
                CurItemNo = GetItemNo(CheckList, SubItem)
                If CurItemNo > 0 Then
                    CurItemTxt = Trim(Str(CurItemNo))
                    Result = Replace(Result, SubItem, CurItemTxt, 1, -1, vbBinaryCompare)
                Else
                    If AllItems Then
                        Result = Replace(Result, SubItem, "-1", 1, -1, vbBinaryCompare)
                    End If
                End If
            End If
            SubItemNo = SubItemNo + 1
            SubItem = Trim(GetItem(MainItem, SubItemNo, "+"))
        Wend
        MainItemNo = MainItemNo + 1
        MainItem = Trim(GetItem(CurItemList, MainItemNo, ","))
    Wend
    TranslateItemList = Result
End Function

Public Sub CreateTypeList(ResultList As String, CheckList As String, FieldType As String, ItemOffset As Integer)
    Dim CurFldNam As String
    Dim LineList As String
    Dim LineNbr As String
    Dim CurLine As Long
    Dim LineNo As Long
    ResultList = ""
    LineList = MyConfig.CfgTab(0).GetLinesByColumnValue(1, FieldType, 0)
    CurLine = 0
    LineNbr = GetRealItem(LineList, CurLine, ",")
    While LineNbr <> ""
        LineNo = Val(LineNbr)
        CurFldNam = MyConfig.CfgTab(0).GetColumnValue(LineNo, 0)
        AddItemToList ResultList, CheckList, CurFldNam, ItemOffset
        CurLine = CurLine + 1
        LineNbr = GetRealItem(LineList, CurLine, ",")
    Wend
End Sub

Public Sub AddItemToList(ResultList As String, ItemList As String, ItemName As String, ItemOffset As Integer)
    Dim ItemNo As Integer
    ItemNo = GetItemNo(ItemList, ItemName)
    If ItemNo > 0 Then
        If ResultList <> "" Then ResultList = ResultList & ","
        ResultList = ResultList & Trim(Str(ItemNo + ItemOffset))
    End If
End Sub

Public Function TranslateRecordData(CurRecData As String, InFieldList As String, OutFieldList As String) As String
    Dim NewRecData As String
    Dim CurItemNo As Integer
    Dim CurFldNam As String
    Dim CurFldNbr As Integer
    'If InStr(InFieldList, "ADID") = 0 Then
        NewRecData = ""
        NewRecData = NewRecData & GetItem(CurRecData, 1, ",") & ","
        NewRecData = NewRecData & GetItem(CurRecData, 2, ",") & ","
        NewRecData = NewRecData & GetItem(CurRecData, 3, ",") & ","
        CurFldNbr = 1
        CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
        While CurFldNam <> ""
            If Not StartedAsCompareTool Then
                CurItemNo = GetItemNo(InFieldList, CurFldNam) + 3
                If CurItemNo > 3 Then
                    NewRecData = NewRecData & GetItem(CurRecData, CurItemNo, ",") & ","
                Else
                    NewRecData = NewRecData & ","
                End If
            Else
                CurItemNo = GetItemNo(InFieldList, CurFldNam)
                If CurItemNo > 0 Then
                    NewRecData = NewRecData & GetItem(CurRecData, CurItemNo, ",") & ","
                Else
                    NewRecData = NewRecData & ","
                End If
            End If
            CurFldNbr = CurFldNbr + 1
            CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
        Wend
    'Else
    '    NewRecData = TranslateRecordFromFlight(CurRecData, InFieldList, OutFieldList)
    'End If
    TranslateRecordData = NewRecData
End Function
Public Function TranslateRecordFromFlight(CurRecData As String, InFieldList As String, OutFieldList As String) As String
    Dim NewRecData As String
    Dim CurItemNo As Integer
    Dim CurFldNam As String
    Dim CurFldNbr As Integer
    Dim CurAdid As String
    Dim OutAdidFields As String
    
    NewRecData = ""
    NewRecData = NewRecData & GetItem(CurRecData, 1, ",") & ","
    NewRecData = NewRecData & GetItem(CurRecData, 2, ",") & ","
    NewRecData = NewRecData & GetItem(CurRecData, 3, ",") & ","
    
    CurItemNo = GetItemNo(InFieldList, "ADID") + 3
    CurAdid = GetItem(CurRecData, CurItemNo, ",")
    'HardCoded to Old DataModel !!
    '"FLCA,FLTA,FLCD,FLTD,VPFR,VPTO,FREQ,ACT3,NOSE,ORG3,VSA3,STOA,STOD,DAOF,VSD3,DES3,NATA,NATD,FREW,REGN,FTYP"
    '"FLCB,FLTB,....,....,SKED,SKED,....,....,....,....,....,SKED,....,....,....,....,....,....,....,....,...."
    '"....,....,FLCB,FLTB,SKED,SKED,....,....,....,....,....,....,SKED,....,....,....,....,....,....,....,...."
    Select Case CurAdid
        Case "A"
            OutAdidFields = "FLCB,FLTB,....,....,SKED,SKED,....,....,....,....,....,SKED,....,....,....,....,TTYP,....,....,....,...."
        Case "D"
            OutAdidFields = "....,....,FLCB,FLTB,SKED,SKED,....,....,....,....,....,....,SKED,....,....,....,....,TTYP,....,....,...."
        Case Else
    End Select
    
    CurFldNbr = 1
    CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
    While CurFldNam <> ""
        CurItemNo = GetItemNo(InFieldList, CurFldNam) + 3
        If CurItemNo > 3 Then
            NewRecData = NewRecData & GetItem(CurRecData, CurItemNo, ",") & ","
        Else
            CurFldNam = GetItem(OutAdidFields, CurFldNbr, ",")
            If CurFldNam <> "" Then
                CurItemNo = GetItemNo(InFieldList, CurFldNam) + 3
            Else
                CurItemNo = 0
            End If
            If CurItemNo > 3 Then
                NewRecData = NewRecData & GetItem(CurRecData, CurItemNo, ",") & ","
            Else
                NewRecData = NewRecData & ","
            End If
        End If
        CurFldNbr = CurFldNbr + 1
        CurFldNam = GetItem(OutFieldList, CurFldNbr, ",")
    Wend
    TranslateRecordFromFlight = NewRecData
End Function

Public Function ResolveDataLists(LookField As String, UseDataField As String, ListofData As String, ListOfFields As String) As String
    Dim Result As String
    Result = GetFieldValue(UseDataField, ListofData, ListOfFields)
    Result = ExtractFromItemList(LookField, Result)
    ResolveDataLists = Result
End Function
Public Function ExtractFromItemList(LookField As String, DataList As String) As String
    Dim Result As String
    Dim ItemTxt As String
    Dim ItemNbr As Integer
    Dim ItemCnt As Integer
    ItemCnt = ItemCount(DataList, "|")
    Select Case LookField
        Case "ORG3"
            Result = GetItem(DataList, 1, "|")
            Result = GetItem(Result, 1, ":")
            If Result = HomeAirport Then
                Result = GetItem(DataList, 2, "|")
                Result = GetItem(Result, 1, ":")
            End If
        Case "DES3"
            ItemNbr = ItemCount(DataList, "|")
            If ItemNbr > 0 Then
                Result = GetItem(DataList, ItemNbr, "|")
                Result = GetItem(Result, 1, ":")
            End If
            If Result = HomeAirport Then
                Result = GetItem(DataList, ItemNbr - 1, "|")
                Result = GetItem(Result, 1, ":")
            End If
        Case "VSA3"
            ItemTxt = GetItem(DataList, 1, "|")
            ItemTxt = GetItem(ItemTxt, 1, ":")
            If ItemTxt = HomeAirport Then ItemNbr = 1 Else ItemNbr = 0
            ItemTxt = "START"
            While ItemTxt <> ""
                ItemNbr = ItemNbr + 1
                ItemTxt = Trim(GetItem(DataList, ItemNbr, "|"))
                ItemTxt = GetItem(ItemTxt, 1, ":")
                If ItemTxt = HomeAirport Then
                    If ItemNbr > 2 Then
                        Result = GetItem(DataList, ItemNbr - 1, "|")
                        Result = GetItem(Result, 1, ":")
                    End If
                    ItemTxt = ""
                End If
            Wend
        Case "VSD3"
            ItemNbr = 0
            ItemTxt = "START"
            While ItemTxt <> ""
                ItemNbr = ItemNbr + 1
                ItemTxt = Trim(GetItem(DataList, ItemNbr, "|"))
                ItemTxt = GetItem(ItemTxt, 1, ":")
                If ItemTxt = HomeAirport Then
                    If (ItemCnt - ItemNbr) > 1 Then
                        Result = GetItem(DataList, ItemNbr + 1, "|")
                        Result = GetItem(Result, 1, ":")
                    End If
                    ItemTxt = ""
                End If
            Wend
        Case Else
    End Select
    ExtractFromItemList = Result
End Function
Public Function ShiftFrqd(UseFrqd As String, ShiftValue As Integer) As String
    Dim tmpFrqd As String
    Dim iVal As Integer
    Dim i As Integer
    tmpFrqd = Trim(UseFrqd)
    If tmpFrqd <> "" Then
        tmpFrqd = "......."
        For i = 1 To 7
            If Mid(UseFrqd, i, 1) <> "." Then
                iVal = i + ShiftValue
                If iVal > 7 Then iVal = iVal - 7
                If iVal < 1 Then iVal = 7 - iVal
                Mid(tmpFrqd, iVal, 1) = Trim(Str(iVal))
            End If
        Next
    End If
    ShiftFrqd = tmpFrqd
End Function

Public Function FormatFrequency(FreqDays As String, FormatType As String, IsValid As Boolean) As String
    Dim Result As String
    Dim tmpPos As Integer
    Dim tmpChr As String
    Dim tmpVal As String
    Dim FreqType As Integer
    Dim i As Integer
    Dim l As Integer
    IsValid = True
    Result = Trim(FreqDays)
    If Result <> "" Then
        FreqType = -1
        Select Case FormatType
            Case "123"
                FreqType = 0
            Case "111"
                FreqType = 1
            Case Else
            If (InStr(FreqDays, "0") > 0) Or (InStr(FreqDays, "1111") > 0) Then
                'Might be more analysis needed
                FreqType = 1
            Else
                FreqType = 0
            End If
        End Select
        Result = "......."
        Select Case FreqType
            Case 0  'Type 1234567
                l = Len(FreqDays)
                For i = 1 To l
                    tmpChr = Mid(FreqDays, i, 1)
                    tmpPos = Val(tmpChr)
                    If (tmpPos > 0) And (tmpPos < 8) Then
                        If Mid(Result, tmpPos, 1) = "." Then
                            Mid(Result, tmpPos, 1) = tmpChr
                        Else
                            IsValid = False
                        End If
                    Else
                        If InStr(". -0", tmpChr) = 0 Then IsValid = False
                    End If
                    If Not IsValid Then Exit For
                Next
            Case 1  'Type 0011001
                l = Len(FreqDays)
                If l > 7 Then l = 7
                For i = 1 To l
                    tmpChr = Mid(FreqDays, i, 1)
                    If tmpChr = "1" Then
                        Mid(Result, i, 1) = Trim(Str(i))
                    End If
                Next
            Case Else
                Result = FreqDays
        End Select
        If Not IsValid Then Result = FreqDays
    End If
    FormatFrequency = Result
End Function


Public Function ApcLocalToUtc(AptCode As String, LocValue As String) As String
    Dim NewValue As String
    Dim CurLen As Integer
    Dim UtcOffset As Double
    Dim TimeValue
    UtcOffset = 480
    CurLen = Len(LocValue)
    If CurLen = 8 Then
        TimeValue = CedaDateToVb(LocValue)
        TimeValue = DateAdd("n", -(UtcOffset), TimeValue)
        NewValue = Format(TimeValue, "yyyymmdd")
    ElseIf CurLen = 12 Then
        TimeValue = CedaFullDateToVb(LocValue)
        TimeValue = DateAdd("n", -(UtcOffset), TimeValue)
        NewValue = Format(TimeValue, "yyyymmddhhmm")
    Else
        NewValue = LocValue
    End If
    ApcLocalToUtc = NewValue
End Function

Public Function ApcUtcToLocal(AptCode As String, UtcValue As String) As String
    Dim NewValue As String
    Dim CurLen As Integer
    Dim UtcOffset As Double
    Dim TimeValue
    UtcOffset = 480
    CurLen = Len(UtcValue)
    If CurLen = 8 Then
        TimeValue = CedaDateToVb(UtcValue)
        TimeValue = DateAdd("n", UtcOffset, TimeValue)
        NewValue = Format(TimeValue, "yyyymmdd")
    ElseIf CurLen = 12 Then
        TimeValue = CedaFullDateToVb(UtcValue)
        TimeValue = DateAdd("n", UtcOffset, TimeValue)
        NewValue = Format(TimeValue, "yyyymmddhhmm")
    Else
        NewValue = UtcValue
    End If
    ApcUtcToLocal = NewValue
End Function

Public Function TurnImpFkeyToAftFkey(CurFkey As String) As String
    TurnImpFkeyToAftFkey = Mid(CurFkey, 4, 5) & Left(CurFkey, 3) & Mid(CurFkey, 9)
End Function

Public Function TurnAftFkey(AftFkey As String) As String
   TurnAftFkey = Mid(AftFkey, 6, 3) + Left(AftFkey, 5) + Mid(AftFkey, 9)
End Function


Public Sub BasicDetails(UseType As String, tmpText As String)
    Dim tmpSqlKey As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim RetVal As Integer
    Dim CurItm As Integer
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim MsgTxt As String
    Dim tmpFlno As String
    Dim tmpFlca As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim ReqButtons As String
    
    Screen.MousePointer = 11
    If tmpText <> "" Then
        ReqButtons = ""
        tmpTable = ""
        Select Case UseType
            Case "ALC"
                tmpTable = "ALTTAB"
                tmpFields = "ALC2,ALC3,ALFN,CTRY,CASH,ADD1,ADD2,ADD3,ADD4,PHON,TFAX,SITA,TELX,WEBS,BASE"
                If Len(tmpText) = 2 Then
                    tmpSqlKey = "WHERE ALC2='" & tmpText & "'"
                End If
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE ALC3='" & tmpText & "'"
                End If
                'ReqButtons = "OK,Flights"
            Case "FLNO"
                If Not FilterCalledAsDispo Then
                    tmpFlno = StripAftFlno(tmpText, tmpFlca, tmpFltn, tmpFlns)
                    ShowFlights.Show
                    If Not StartedAsCompareTool Then
                        ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpFlca, tmpFltn, tmpFlns
                    Else
                        ShowFlights.LoadFlightData MinImpVpfr, MaxImpVpto, tmpFlca, tmpFltn, tmpFlns
                    End If
                End If
            Case "APT"
                tmpTable = "APTTAB"
                tmpFields = "APC3,APC4,APSN,APFN,APN2,APN3,APN4,LAND,APTT"
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE APC3='" & tmpText & "'"
                End If
                If Len(tmpText) = 4 Then
                    tmpSqlKey = "WHERE APC4='" & tmpText & "'"
                End If
            Case "ACT"
                tmpTable = "ACTTAB"
                tmpFields = "ACT3,ACT5,ACTI,ACFN,SEAF,SEAB,SEAE,SEAT,ENTY"
                If Len(tmpText) = 3 Then
                    tmpSqlKey = "WHERE ACT3='" & tmpText & "'"
                Else
                    tmpSqlKey = "WHERE ACT5='" & tmpText & "'"
                End If
            Case "FLIGHT"
                tmpFlno = StripAftFlno(tmpText, tmpFlca, tmpFltn, tmpFlns)
                ShowFlights.Show
                tmpVpfr = MinImpVpfr
                tmpVpto = MaxImpVpto
                If tmpVpfr = "" Then tmpVpfr = MainDialog.ActSeason(1).Tag & "000000"
                If tmpVpto = "" Then tmpVpto = MainDialog.ActSeason(2).Tag & "235959"
                ShowFlights.LoadFlightData tmpVpfr, tmpVpto, tmpFlca, tmpFltn, tmpFlns
            Case Else
        End Select
        If tmpTable <> "" Then
            RetVal = UfisServer.CallCeda(UserAnswer, "RT", tmpTable, tmpFields, "", tmpSqlKey, "", 0, True, False)
            MsgTxt = ""
            CurItm = 0
            Do
                CurItm = CurItm + 1
                tmpFldNam = GetItem(tmpFields, CurItm, ",")
                If tmpFldNam <> "" Then
                    tmpFldVal = GetItem(UserAnswer, CurItm, ",")
                    If tmpFldVal <> "" Then
                        MsgTxt = MsgTxt & tmpFldNam & ": " & tmpFldVal & vbNewLine
                    End If
                End If
            Loop While tmpFldNam <> ""
            If MsgTxt <> "" Then
                MsgTxt = Left(MsgTxt, Len(MsgTxt) - 2)
            Else
                MsgTxt = "'" & tmpText & "' is not in your basic data."
            End If
            If MyMsgBox.CallAskUser(0, 0, 0, "Basic Data Information", MsgTxt, "infomsg", ReqButtons, UserAnswer) = 2 Then
                'Me.Refresh
                UserAnswer = Replace(UserAnswer, "&", "", 1, -1, vbBinaryCompare)
                Select Case UserAnswer
                    Case "Flights"
                        If Not FilterCalledAsDispo Then
                            ShowFlights.Show , MainDialog
                            ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpText, tmpFltn, tmpFlns
                        End If
                    Case Else
                End Select
            End If
        End If
    End If
    Screen.MousePointer = 0
End Sub

Public Function GetAddOnLength(KeyWord As String, LineText As String) As Integer
    Static AddOnLength As Integer
    Dim KeyList As String
    Dim FldDesc As String
    Dim FldCeda As String
    Dim FldCode As String
    Dim FldName As String
    Dim FldAdid As String
    Dim FldMean As String
    Dim FldLPos As String
    Dim FldSize As String
    Dim FldPatt As String
    Dim NewDesc As String
    Dim NewText As String
    Dim FldPos As Integer
    Dim ArrCnt As Integer
    Dim DepCnt As Integer
    Dim itm As Integer
    'SCORE Length is 105
    Select Case KeyWord
        Case "[HEADER]"
            'Future Use
            AddOnLength = AddOnDataLength
        Case "[APPEND]"
            AddOnDataFound = False
            AddOnDataMerge = False
            'Here we create the default
            'layout of the main grid,
            'which will be modified
            'by the [HEADER] definition
            
            'We must create extensions
            'for some config KeyWords
            AddOnMainTitle = ""  'MAIN_HEADER_TITLE = Title of MainHeader
            AddOnMainWidth = ""  'MAIN_HEADER_WIDTH = MainHeader Groups
            AddOnMainFields = "" 'MAIN_FIELDS = Logical Fields of the Grid
            AddOnFinalHead = ""  'FINAL_HEAD = Grid Column Titles
            AddOnFinalLen = ""   'FINAL_LEN = List of Grid Column Field Length
            AddOnFinalPos = ""   'FINAL_POS = List of Grid Column Field Pos in TextLine
            AddOnGridSize = ""   'Internal HeaderLengthString
            
            'Finally we need the AddOn field names
            'for Arr/Dep in order to pick
            'the values from the grid (AddOnFields)
            'and to send them to Ceda (AftFields)
            AddOnArrFields = ""   'Logical Field Names
            AddOnArrAftFld = ""   'AFTTAB Field Names
            AddOnDepFields = ""
            AddOnDepAftFld = ""
            
            FldPos = 106
            AddOnLength = 0
            ArrCnt = 0
            DepCnt = 0
            KeyList = Replace(LineText, "[", "", 1, -1, vbBinaryCompare)
            itm = 0
            FldDesc = "START"
            While FldDesc <> ""
                itm = itm + 1
                FldDesc = GetItem(KeyList, itm, "]")
                If FldDesc <> "" Then
                    FldCode = GetItem(FldDesc, 1, "=")
                    FldDesc = GetItem(FldDesc, 2, "=")
                    FldAdid = GetItem(FldDesc, 1, ".")
                    FldCeda = GetItem(FldDesc, 2, ".")
                    FldLPos = GetItem(FldDesc, 3, ".")
                    FldSize = GetItem(FldDesc, 4, ".")
                    FldMean = GetItem(FldDesc, 5, ".")
                    FldName = FldAdid & Right("000" & CStr(itm), 3)
                    If FldLPos = "P" Then FldLPos = CStr(FldPos)
                    Select Case FldAdid
                        Case "A"
                            ArrCnt = ArrCnt + 1
                            AddOnArrFields = AddOnArrFields & "," & FldName
                            AddOnArrAftFld = AddOnArrAftFld & "," & FldCeda
                        Case "D"
                            DepCnt = DepCnt + 1
                            AddOnDepFields = AddOnDepFields & "," & FldName
                            AddOnDepAftFld = AddOnDepAftFld & "," & FldCeda
                        Case Else
                            FldName = ""
                    End Select
                    If FldName <> "" Then
                        'Collect Grid Configuration
                        AddOnMainFields = AddOnMainFields & "," & FldName
                        AddOnFinalHead = AddOnFinalHead & "," & FldMean
                        AddOnFinalPos = AddOnFinalPos & "," & FldLPos
                        AddOnFinalLen = AddOnFinalLen & "," & FldSize
                        AddOnGridSize = AddOnGridSize & ",100"
                        NewDesc = FldCode & "="
                        NewDesc = NewDesc & FldAdid & "."
                        NewDesc = NewDesc & FldName & "."
                        NewDesc = NewDesc & FldCeda & "."
                        NewDesc = NewDesc & FldLPos & "."
                        NewDesc = NewDesc & FldSize & "."
                        NewDesc = NewDesc & FldMean
                        NewText = NewText & "[" & NewDesc & "]"
                    End If
                    AddOnLength = AddOnLength + Val(FldSize)
                    FldPos = FldPos + Val(FldSize)
                End If
            Wend
            If AddOnLength > 0 Then
                If AddOnArrFields <> "" Then
                    AddOnArrFields = Mid(AddOnArrFields, 2)
                    AddOnArrAftFld = Mid(AddOnArrAftFld, 2)
                End If
                If AddOnDepFields <> "" Then
                    AddOnDepFields = Mid(AddOnDepFields, 2)
                    AddOnDepAftFld = Mid(AddOnDepAftFld, 2)
                End If
                AddOnDataFound = True
                AddOnMainFields = Mid(AddOnMainFields, 2)
                AddOnFinalHead = Mid(AddOnFinalHead, 2)
                AddOnFinalPos = Mid(AddOnFinalPos, 2)
                AddOnFinalLen = Mid(AddOnFinalLen, 2)
                AddOnGridSize = Mid(AddOnGridSize, 2)
                AddOnMainTitle = "Appendix"
                AddOnMainWidth = CStr(ArrCnt + DepCnt)
            End If
            AddOnDataLength = AddOnLength
        Case Else
    End Select
    GetAddOnLength = AddOnLength
End Function
Public Function ConvertDataLine(LineText As String, InFormat As String, OutFormat As String, Prefix As String, Context As String, AddOnLength As Integer) As String
    Dim Result As String
    Result = Prefix
    Result = Result & Mid(LineText, 1, 48)
    Result = Result & "    "
    Result = Result & Mid(LineText, 49, 6)
    Result = Result & "        "
    Result = Result & Mid(LineText, 55, 15)
    Result = Result & "        "
    Result = Result & Mid(LineText, 70, 3)
    If AddOnLength > 0 Then Result = Result & Mid(LineText, 73, AddOnLength)
    ConvertDataLine = Result
End Function

Public Sub ReadAnyFile(DataTab As TABLib.Tab, UseFileName As String)
    Dim CurFileName As String
    Dim CurFileNbr As Integer
    Dim LineText As String
    Dim SaveLine As String
    Dim LineCount As Long
    Dim tmpData As String
    Dim ConvertFile As Boolean
    Dim tmpInFormat As String
    Dim tmpOutFormat As String
    Dim tmpPrefix As String
    Dim tmpContext As String
    Dim tmpCode As String
    Dim iLen As Integer
    Dim AddOnLength As Integer
    Dim StopLoop As Boolean
    Dim SkipLine As Boolean
    Dim HeaderChecked As Boolean
    AddOnLength = 0
    CurFileName = UseFileName
    CurFileNbr = FreeFile
    DataTab.ResetContent
    DataTab.SetFieldSeparator ","
    DataTab.HeaderString = "File Content && Consistency Check,Remarks"
    DataTab.LogicalFieldList = "IMPL,ERRE"
    DataTab.HeaderLengthString = "1000,1000"
    DataTab.ColumnWidthString = "1000,1000"
    DataTab.SetFieldSeparator Chr(15)
    DataTab.HeaderFontSize = 14
    DataTab.FontSize = 14
    DataTab.LineHeight = 14
    DataTab.SetTabFontBold False
    DataTab.ResetContent
    DataTab.Refresh
    If myIniSection = "" Then myIniSection = "SCORE"
    tmpData = UCase(GetIniEntry(myIniFullName, myIniSection, "", "FILE_CONVERT", "NO"))
    If tmpData = "YES" Then
        ConvertFile = True
    Else
        ConvertFile = False
    End If
    If DataSystemType = "SCORE" Then
        DefaultFileHeader = GetIniEntry(myIniFullName, myIniSection, "", "ADD_FILE_HEADER", "")
    End If
    HeaderChecked = False
    SkipLine = False
    LineCount = 0
    Open CurFileName For Input As #CurFileNbr
    While (Not EOF(CurFileNbr) And (Not StopLoop))
        LineCount = LineCount + 1
        If Not SkipLine Then
            If (PreCheckFile) And (LineCount > 2) Then StopLoop = True
            If LineCount Mod 500 = 0 Then
                DataTab.OnVScrollTo LineCount
                DataTab.Refresh
            End If
            Line Input #CurFileNbr, LineText
        End If
        If LineCount = 1 Then
            If Not HeaderChecked Then
                HeaderChecked = True
                If Left(LineText, 1) <> "[" Then
                    If DefaultFileHeader <> "" Then
                        SaveLine = LineText
                        LineText = DefaultFileHeader
                        SkipLine = True
                    End If
                End If
            Else
                SkipLine = False
            End If
        End If
        If Left(LineText, 1) <> "[" Then
            If RTrim(LineText) <> "" Then
                If ConvertFile = True Then
                    'Note: This works only for SCORE Files
                    tmpPrefix = "        0K..."
                    Mid(tmpPrefix, 1) = CStr(LineCount)
                    LineText = ConvertDataLine(LineText, tmpInFormat, tmpOutFormat, tmpPrefix, tmpContext, AddOnLength)
                End If
                DataTab.InsertTextLine LineText, False
            Else
                LineCount = LineCount - 1
            End If
        Else
            If ConvertFile = True Then
                tmpPrefix = "0       0K..."
            End If
            tmpCode = GetItem(LineText, 1, "]") & "]"
            iLen = Len(tmpCode)
            LineText = Mid(LineText, iLen + 1)
            Select Case tmpCode
                Case "[APPEND]"
                    AddOnLength = GetAddOnLength(tmpCode, LineText)
                Case "[HEADER]"
                    AddOnLength = GetAddOnLength(tmpCode, LineText)
                    If PreCheckFile Then StopLoop = True
                Case "[REM]"
                Case Else
            End Select
            LineText = tmpPrefix & tmpCode & LineText
            DataTab.InsertTextLine LineText, False
            LineCount = LineCount - 1
            If SkipLine = True Then
                LineText = SaveLine
            End If
        End If
    Wend
    Close CurFileNbr
    If myIniSection = "SCORE" Then
        DataTab.ShowHorzScroller True
        DataTab.ShowVertScroller True
    End If
    DataTab.Refresh
    'Me.Refresh
    PreCheckFile = False
End Sub

Public Sub ModifyTabLayout(ForWhat As Integer)
    Select Case ForWhat
        Case 1  'SCORE File Appendix Called from MainDialog
            If (AddOnDataFound) And (Not AddOnDataMerge) Then
                MainFieldList = MainFieldList & "," & AddOnMainFields
                MainExpandHeader = MainExpandHeader & "," & AddOnFinalHead
                MainFldWidLst = MainFldWidLst & "," & AddOnGridSize
                ExpPosLst = ExpPosLst & "," & AddOnFinalPos
                FldLenLst = FldLenLst & "," & AddOnFinalLen
                'Adjust MainHeader
                MainHeaderWidth = Left(MainHeaderWidth, Len(MainHeaderWidth) - 1)
                MainHeaderWidth = MainHeaderWidth & AddOnMainWidth & ",4"
                MainHeaderTitle = Left(MainHeaderTitle, Len(MainHeaderTitle) - 6)
                MainHeaderTitle = MainHeaderTitle & AddOnMainTitle & ",System"
                AddOnDataMerge = True
                'Set Current Values
                CurrentHeader = MainExpandHeader
                CurFldPosLst = ExpPosLst
            End If
        Case 2  'SCORE File Appendix Called from Compare
            If (AddOnDataFound) And (Not AddOnDataMerge) Then
                'MainFieldList = "TRID,LTID,FLAG,SEAS,HOPO,FLCA,FLTA,FLCD,FLTD,VPFR,VPTO,FREQ,NOSE,ACT3,ACT5,ORG3,VSA3,ORG4,VSA4,STOA,STOD,DAOF,VSD3,DES3,VSD4,DES4,NATA,NATD,FREW,URNO"
                'MainExpandHeader = "TR.ID,LT,S,SSC,APC,FCA,FLTA,FCD,FLTD,P.BEGIN,P.END,OP.DAYS,CAP,ACT,ICAO,ORG,VIA,ORG4,VIA4,STOA,STOD,O,VIA,DST,VIA4,DST4,A,D,W,TLX.URNO"
                'MainFldWidLst = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
                'MainColWidLst = "8,1,1,3,3,3,5,3,5,8,8,7,3,3,4,3,3,4,4,4,4,1,3,3,4,4,1,1,1,10"
                MainFieldList = Left(MainFieldList, Len(MainFieldList) - 5)
                MainFieldList = MainFieldList & "," & AddOnMainFields & ",URNO"
                MainExpandHeader = Left(MainExpandHeader, Len(MainExpandHeader) - 9)
                MainExpandHeader = MainExpandHeader & "," & AddOnFinalHead & ",URNO"
                MainFldWidLst = Left(MainFldWidLst, Len(MainFldWidLst) - 3)
                MainFldWidLst = MainFldWidLst & "," & AddOnGridSize & ",10"
                MainHeaderWidth = Left(MainHeaderWidth, Len(MainHeaderWidth) - 1)
                MainHeaderWidth = MainHeaderWidth & AddOnMainWidth & ",4"
                MainHeaderTitle = Replace(MainHeaderTitle, ",TLXTAB", "", 1, -1, vbBinaryCompare)
                MainHeaderTitle = MainHeaderTitle & "," & AddOnMainTitle & ",TLXTAB"
                AddOnDataMerge = True
            End If
        Case Else
    End Select
End Sub

Public Function CheckDateFieldFormat(tmpFldNbr As Integer, tmpDate As String, ListOfDateFields As String, ListOfTimeFields As String, FullCheck As Boolean) As String
    Dim Result As String
    Dim tmpVal As String
    Dim ItmData As String
    Dim itmChr As String
    Result = tmpDate
    tmpVal = "," & Trim(Str(tmpFldNbr)) & ","
    If (InStr(ListOfDateFields, tmpVal) > 0) Or (Not FullCheck) Then
        Select Case DateFieldFormat
            Case "DDMMYYYY"
                Result = Right(tmpDate, 4) & Mid(tmpDate, 3, 2) & Left(tmpDate, 2)
            Case "DDMMMYY", "SSIM2"
                ItmData = tmpDate
                If Len(ItmData) <> 7 Then
                    itmChr = Mid(tmpDate, 2, 1)
                    If (itmChr < "0") Or (itmChr > "9") Then ItmData = "0" & ItmData
                    If Len(ItmData) <> 7 Then
                        If Not UseDefaultSeason Then FindOutDefSeason
                        If UseDefaultSeason Then ItmData = BuildCleanSsimDateFormat(ItmData)
                    End If
                End If
                Result = DecodeSsimDayFormat(ItmData, "SSIM2", "CEDA")
            Case "DD/MM/YY"
                Result = DecodeSlotDayFormat(tmpDate, 1, 2, 3)
            Case "MM/DD/YY"
                Result = DecodeSlotDayFormat(tmpDate, 2, 1, 3)
            Case Else
        End Select
    End If
    If FullCheck Then
        If InStr(ListOfTimeFields, tmpVal) > 0 Then
            '''Change the Time when 2400 to 2359
            '''PHYOE
            If tmpDate = "2400" Then
                tmpDate = "2359"
            End If
            Result = Replace(tmpDate, ":", "", 1, -1, vbBinaryCompare)
        End If
    End If
    CheckDateFieldFormat = Result
End Function

Public Sub FindOutDefSeason()
    DefaultSeasonRec = SeasonData.GetSeasonByName(UCase(Left(myLoadName, 3)))
    If DefaultSeasonRec = "" Then DefaultSeasonRec = SeasonData.GetSeasonByUser(myLoadName)
    If DefaultSeasonRec <> "" Then
        SeasonBegin = GetItem(DefaultSeasonRec, 5, ",")
        SeasonEnd = GetItem(DefaultSeasonRec, 6, ",")
        SeasonActYear = Left(SeasonBegin, 4)
        SeasonNxtYear = Left(SeasonEnd, 4)
        SeasonFirstMonth = Val(Mid(SeasonBegin, 5, 2))
        SeasonLastMonth = Val(Mid(SeasonEnd, 5, 2))
        UseDefaultSeason = True
    End If
End Sub

Public Function BuildCleanSsimDateFormat(ActDate As String)
    Dim Result As String
    Dim tmpDay As String
    Dim tmpMonth As String
    Dim CurMonth As Integer
    Result = ActDate
    tmpDay = Left(ActDate, 2)
    tmpMonth = Mid(ActDate, 3, 3)
    CurMonth = GetItemNo(MonthList, tmpMonth)
    If (CurMonth >= SeasonFirstMonth) And (CurMonth <= 12) Then
        Result = tmpDay & tmpMonth & Mid(SeasonActYear, 3, 2)
    Else
        Result = tmpDay & tmpMonth & Mid(SeasonNxtYear, 3, 2)
    End If
    BuildCleanSsimDateFormat = Result
End Function

Public Function DecodeSlotDayFormat(tmpDate As String, DayItm As Integer, MonthItm As Integer, YearItm As Integer) As String
Dim Result As String
Dim tmpDay As String
Dim tmpMonth As String
Dim tmpYear As String
    tmpDay = Trim(GetItem(tmpDate, DayItm, "/"))
    tmpDay = Right("00" & tmpDay, 2)
    tmpMonth = Trim(GetItem(tmpDate, MonthItm, "/"))
    tmpMonth = Right("00" & tmpMonth, 2)
    tmpYear = Trim(GetItem(tmpDate, YearItm, "/"))
    If Len(tmpYear) < 2 Then tmpYear = Right("00" & tmpYear, 2)
    Result = tmpYear & tmpMonth & tmpDay
    If Left(Result, 2) = 99 Then Result = "19" & Result Else Result = "20" & Result
    DecodeSlotDayFormat = Result
End Function

Public Sub SynchronizeMyTabs(MyCaller As String, ForWhat As String, LineNo As Long)
    Dim Caller As String
    Dim SyncLine As Long
    Dim FileIsUsed As Boolean  '-> FileData(0)
    Dim CopyIsUsed As Boolean  '-> FileData(1)
    Dim PushIsUsed As Boolean  '-> Tab2
    Dim CheckScroll As Boolean
    Dim CheckCursor As Boolean
    Dim FileTab As TABLib.Tab
    Dim FileCurrPos As Long
    Dim FileCurrTop As Long
    Dim FileSyncTop As Long
    Dim FileSyncCsr As Long
    Dim FileSyncBot As Long
    Dim FileVisible As Long
    Dim CopyTab As TABLib.Tab
    Dim CopyCurrPos As Long
    Dim CopyCurrTop As Long
    Dim CopySyncTop As Long
    Dim CopySyncCsr As Long
    Dim CopySyncBot As Long
    Dim CopyVisible As Long
    Dim PushTab As TABLib.Tab
    Dim PushCurrPos As Long
    Dim PushCurrTop As Long
    Dim PushSyncTop As Long
    Dim PushSyncCsr As Long
    Dim PushSyncBot As Long
    Dim PushVisible As Long
    Dim GridTab As TABLib.Tab
    Dim GridCurrPos As Long
    Dim GridCurrTop As Long
    Dim GridSyncTop As Long
    Dim GridSyncCsr As Long
    Dim GridSyncBot As Long
    Dim GridVisible As Long
    If Not StopMyApplEvents Then
        Caller = MyCaller
        StopMyApplEvents = True
        FileIsUsed = False
        CopyIsUsed = False
        PushIsUsed = False
        FileIsUsed = True
        Set FileTab = MainDialog.FileData(0)
        FileCurrPos = FileTab.GetCurrentSelected
        FileCurrTop = FileTab.GetVScrollPos
        Select Case Caller
            Case "DBF_DATA"
                Set GridTab = DbfReader.DataTab
                GridCurrPos = GridTab.GetCurrentSelected
                GridCurrTop = GridTab.GetVScrollPos
                FileSyncCsr = GetSyncKeyLineNo(GridTab, "", FileTab, "", GridCurrPos, True)
                If FileSyncCsr >= 0 Then
                    FileVisible = GetCursorBounds(FileTab, FileSyncCsr, FileSyncTop, FileSyncBot)
                    If FileSyncTop <> FileCurrTop Then FileTab.OnVScrollTo FileSyncTop
                End If
                If FileSyncCsr <> FileCurrPos Then FileTab.SetCurrentSelection FileSyncCsr
                Caller = "FILE"
            Case "DBF_FLIGHT"
                Set GridTab = DbfReader.FlightData
                GridCurrPos = GridTab.GetCurrentSelected
                GridCurrTop = GridTab.GetVScrollPos
                FileSyncCsr = GetSyncKeyLineNo(GridTab, "RSNO", FileTab, "", GridCurrPos, True)
                If FileSyncCsr >= 0 Then
                    FileVisible = GetCursorBounds(FileTab, FileSyncCsr, FileSyncTop, FileSyncBot)
                    If FileSyncTop <> FileCurrTop Then FileTab.OnVScrollTo FileSyncTop
                End If
                If FileSyncCsr <> FileCurrPos Then FileTab.SetCurrentSelection FileSyncCsr
                Caller = "FILE"
            Case Else
        End Select
        FileCurrPos = FileTab.GetCurrentSelected
        FileCurrTop = FileTab.GetVScrollPos
        If MainDialog.FileData.Count >= 2 Then
            CopyIsUsed = True
            Set CopyTab = MainDialog.FileData(1)
            CopyCurrPos = CopyTab.GetCurrentSelected
            CopyCurrTop = CopyTab.GetVScrollPos
        End If
        If Tab2IsUsed Then
            PushIsUsed = True
            Set PushTab = MainDialog.TAB2
            PushCurrPos = PushTab.GetCurrentSelected
            PushCurrTop = PushTab.GetVScrollPos
        End If
        Select Case Caller
            Case "FILE"
                If CopyIsUsed Then
                    CopySyncCsr = GetSyncKeyLineNo(FileTab, "", CopyTab, "", FileCurrPos, True)
                    If CopySyncCsr >= 0 Then
                        CopyVisible = GetCursorBounds(CopyTab, CopySyncCsr, CopySyncTop, CopySyncBot)
                        If CopySyncTop <> CopyCurrTop Then CopyTab.OnVScrollTo CopySyncTop
                    End If
                    If CopySyncCsr <> CopyCurrPos Then CopyTab.SetCurrentSelection CopySyncCsr
                End If
                If PushIsUsed Then
                    PushSyncCsr = GetSyncKeyLineNo(FileTab, "", PushTab, "", FileCurrPos, True)
                    If PushSyncCsr >= 0 Then
                        PushVisible = GetCursorBounds(PushTab, PushSyncCsr, PushSyncTop, PushSyncBot)
                        If PushSyncTop <> PushCurrTop Then PushTab.OnVScrollTo PushSyncTop
                    End If
                    If PushSyncCsr <> PushCurrPos Then PushTab.SetCurrentSelection PushSyncCsr
                End If
            Case "COPY"
                FileSyncCsr = GetSyncKeyLineNo(CopyTab, "", FileTab, "", CopyCurrPos, True)
                If FileSyncCsr >= 0 Then
                    FileVisible = GetCursorBounds(FileTab, FileSyncCsr, FileSyncTop, FileSyncBot)
                    If FileSyncTop <> FileCurrTop Then FileTab.OnVScrollTo FileSyncTop
                End If
                If FileSyncCsr <> FileCurrPos Then FileTab.SetCurrentSelection FileSyncCsr
            Case "PUSH"
                FileSyncCsr = GetSyncKeyLineNo(PushTab, "", FileTab, "", PushCurrPos, True)
                If FileSyncCsr >= 0 Then
                    FileVisible = GetCursorBounds(FileTab, FileSyncCsr, FileSyncTop, FileSyncBot)
                    If FileSyncTop <> FileCurrTop Then FileTab.OnVScrollTo FileSyncTop
                End If
                If FileSyncCsr <> FileCurrPos Then FileTab.SetCurrentSelection FileSyncCsr
            Case Else
        End Select
        FileCurrPos = FileTab.GetCurrentSelected
        Select Case DataSystemType
            Case "SSIM45"
                If MyCaller <> "DBF_FLIGHT" Then
                    Set GridTab = DbfReader.FlightData
                    GridCurrPos = GridTab.GetCurrentSelected
                    GridCurrTop = GridTab.GetVScrollPos
                    GridSyncCsr = GetSyncKeyLineNo(FileTab, "", GridTab, "RSNO", FileCurrPos, True)
                    If GridSyncCsr >= 0 Then
                        GridVisible = GetCursorBounds(GridTab, GridSyncCsr, GridSyncTop, GridSyncBot)
                        If GridSyncTop <> GridCurrTop Then GridTab.OnVScrollTo GridSyncTop
                    End If
                    If GridSyncCsr <> GridCurrPos Then GridTab.SetCurrentSelection GridSyncCsr
                End If
                If MyCaller <> "DBF_DATA" Then
                    Set GridTab = DbfReader.DataTab
                    GridCurrPos = GridTab.GetCurrentSelected
                    GridCurrTop = GridTab.GetVScrollPos
                    GridSyncCsr = GetSyncKeyLineNo(FileTab, "", GridTab, "", FileCurrPos, True)
                    If GridSyncCsr >= 0 Then
                        GridVisible = GetCursorBounds(GridTab, GridSyncCsr, GridSyncTop, GridSyncBot)
                        If GridSyncTop <> GridCurrTop Then GridTab.OnVScrollTo GridSyncTop
                    End If
                    If GridSyncCsr <> GridCurrPos Then GridTab.SetCurrentSelection GridSyncCsr
                End If
            Case Else
        End Select
        StopMyApplEvents = False
    End If
End Sub

Public Function GetCursorBounds(UseTab As TABLib.Tab, ChkCsrLine As Long, NewTopLine As Long, NewBotLine As Long) As Long
    Dim CurTopLine As Long
    Dim CurBotLine As Long
    Dim VisLines As Long
    VisLines = (UseTab.Height / 15) / CLng(UseTab.LineHeight)
    VisLines = VisLines - 1 'Header
    If UseTab.MainHeader Then VisLines = VisLines - 1
    VisLines = VisLines - 1 'Horiz.Scrollerr
    CurTopLine = UseTab.GetVScrollPos
    CurBotLine = CurTopLine + VisLines - 1
    NewTopLine = CurTopLine
    NewBotLine = CurBotLine
    If ChkCsrLine < NewTopLine Then
        NewTopLine = ChkCsrLine
        NewBotLine = NewTopLine + VisLines - 1
    End If
    If ChkCsrLine >= NewBotLine Then
        NewTopLine = ChkCsrLine - VisLines + 1
        If NewTopLine < 0 Then NewTopLine = 0
        NewBotLine = NewTopLine + VisLines - 1
    End If
    GetCursorBounds = VisLines
End Function

Public Function GetSyncKeyLineNo(SrcTab As TABLib.Tab, SrcFld As String, TgtTab As TABLib.Tab, IdxName As String, SrcLineNo As Long, ValidOnly As Boolean) As Long
    Dim KeyLine As Long
    Dim KeyCol As Long
    Dim KeyName As String
    Dim KeyData As String
    Dim HitData As String
    If ValidOnly = True Then
        KeyLine = -1
    Else
        KeyLine = SrcLineNo
    End If
    KeyName = SrcFld
    If KeyName = "" Then KeyName = "LKEY"
    KeyData = Trim(SrcTab.GetFieldValue(SrcLineNo, KeyName))
    If KeyData = "" Then
        KeyCol = CLng(GetRealItemNo(SrcTab.HeaderString, "Unique Key"))
        If KeyCol < 0 Then
            KeyCol = SrcTab.GetColumnCount - 1
        End If
        KeyData = Trim(SrcTab.GetColumnValue(SrcLineNo, KeyCol))
    End If
    If KeyData <> "" Then
        KeyName = IdxName
        If KeyName = "" Then KeyName = "LKEY"
        HitData = TgtTab.GetLinesByIndexValue(KeyName, KeyData, 0)
        If HitData <> "" Then
            KeyLine = Val(HitData)
        End If
    End If
    GetSyncKeyLineNo = KeyLine
End Function

