﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.MVVM.ViewModel;
using Ufis.Utilities;
using Ufis.MVVM.Command;
using Ufis.Data;
using Ufis.Entities;
using System.Collections.ObjectModel;
using System.Windows.Data;
using DevExpress.Utils;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Core;
using System.ComponentModel;
using Ufis.Flight.Filter;
using Ufis.Data.Ceda;
using GroundControlManager.DataAccess;
using GroundControlManager.Helpers;
using Ufis.Broadcast.Ceda;
using GroundControlManager.Entities;

namespace GroundControlManager.MainWindow
{
    /// <summary>
    /// The ViewModel for the application's main window.
    /// </summary>
    public class MainWindowViewModel : MultiWorkspaceViewModel
    {
        private EntityCollectionBase<EntFlight> _flights;
        private FlightFilterViewModel _flightFilterViewModel;
        private readonly FlightFilter _flightFilter;
        private bool _isLoading;

        #region Public Properties

        public EntityCollectionBase<EntFlight> Flights
        {
            get
            {
                return _flights;
            }
            set
            {
                if (_flights == value)
                    return;

                if (_flights != null)
                {
                    _flights.BroadcastReceived -= Flights_BroadcastReceived;   
                    _flights.Close();
                }

                _flights = value;
                _flights.BroadcastReceived += Flights_BroadcastReceived;  
                OnPropertyChanged("Flights");
            }
        }

        public ObservableCollection<UfisGridColumn> Columns { get; set; }

        public FlightFilterViewModel FlightFilter
        {
            get
            {
                if (_flightFilterViewModel == null)
                {
                    _flightFilterViewModel = new FlightFilterViewModel("FloatHost", _flightFilter)
                        { 
                            CanFilterByFlightTypes = true,
                            FlightTypes = FlightType.Towing | FlightType.GroundMovement
                        };
                    _flightFilterViewModel.RequestFilter += (o, e) =>
                        {
                            IsLoading = true;
                            using (BackgroundWorker worker = new BackgroundWorker())
                            {
                                worker.DoWork += (ob, ev) =>
                                    {
                                        LoadFlightData();
                                    };
                                worker.RunWorkerCompleted += (ob, ev) =>
                                    {
                                        IsLoading = false;
                                    };
                                worker.RunWorkerAsync();
                            }
                        };
                }

                return _flightFilterViewModel;
            }
        }

        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading == value)
                    return;

                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }

        #endregion

        #region Public Methods

        public string LoadFlightData()
        {
            return LoadFlightData(true);
        }

        public string LoadFlightData(bool reload)
        {
            string strAttrList = "[Urno],[ReadyForTowing],[FullFlightNumber],[RegistrationNumber],[AircraftIATACode]," +
                "[PositionOfDeparture],[PositionOfArrival],[StandardTimeOfDeparture],[StandardTimeOfArrival]," +
                "[OffblockTime],[OnblockTime],[Remark1],[CallSign],[OperationalType]";
            string strWhere = "WHERE (([StandardTimeOfArrival] BETWEEN '{0}' AND '{1}') OR ([TimeframeOfArrival] BETWEEN '{0}' AND '{1}'))" +
                " AND (([StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}') OR ([TimeframeOfDeparture] BETWEEN '{0}' AND '{1}'))" +
                " AND FTYP IN ({2})";
            DateTime dtmUTCStartDate = TimeZoneInfo.ConvertTime(_flightFilter.AbsoluteStartDate, 
                DlUfisData.Current.DataContext.DefaultTimeZoneInfo);
            string strUTCStartDate = CedaUtils.DateTimeToCedaDateTime(dtmUTCStartDate);
            DateTime dtmUTCEndDate = TimeZoneInfo.ConvertTime(_flightFilter.AbsoluteEndDate, 
                DlUfisData.Current.DataContext.DefaultTimeZoneInfo);
            string strUTCEndDate = CedaUtils.DateTimeToCedaDateTime(dtmUTCEndDate);
            string strFlightTypeList = string.Join(",", 
                _flightFilter.FlightTypes.Select(t => string.Format("'{0}'", t.Code)));
            strWhere = string.Format(strWhere, strUTCStartDate, strUTCEndDate, strFlightTypeList);
            
            string strErrorMsg = null;

            try
            {
                if (reload)
                {
                    if (_flights != null)
                    {
                        _flights.BroadcastReceived -= Flights_BroadcastReceived;
                        _flights.Close();
                    }

                    Flights = DlGroundControlManager.LoadFlights(strAttrList, strWhere);
                }
                else
                {
                    DlGroundControlManager.FillFlights(Flights, strAttrList, strWhere);
                }
            }
            catch (Exception e)
            {
                strErrorMsg = e.Message;
                if (e is DataException)
                    strErrorMsg += "\n" + ((DataException)e).Message;                
            }

            return strErrorMsg;
        }

        void Flights_BroadcastReceived(object sender, Ufis.Broadcast.BroadcastEventArgs e)
        {
            EntityCollectionBase<EntFlight> flights = sender as EntityCollectionBase<EntFlight>;

            if (flights == null)
                return;

            CedaBroadcastMessage message = (CedaBroadcastMessage)e.BroadcastMessage;
            switch (message.Command)
            {
                //ignore single record insertion
                case "IRT":
                case "IFR":
                    e.Handled = true;
                    break;
                //insertion only come from ISF
                case "ISF":
                    HandeISF(message);
                    e.Handled = true;
                    break;
                //make sure the DFR handled correctly
                //should be take out after installing Ufis.Data.Ceda file ver 4.5.0.5 onward
                //case "DFR":
                //    HandleDFR(flights, message);
                //    e.Handled = true;
                //    break;
                //anything else, leave as default
                default:
                    e.Handled = false;
                   break;
            }
        }

        public void UpdateDb(EntFlight flight)
        {
            if (flight != null)
            {
                try
                {
                    flight.SetEntityState(Entity.EntityState.Modified);
                    _flights.Save(flight);
                }
                catch (Exception ex)
                {
                    string strErrMsg = ex.Message;
                    if (ex is DataException)
                        strErrMsg += "\n" + ((DataException)ex).Message;

                    DXMessageBox.Show("Failed to update the database.\nFollwoing error(s) occured:\n" + strErrMsg);
                }
            }
        }

        private void HandeISF(CedaBroadcastMessage message)
        {
            string strWhere = message.Selection;
            string insertKeys = strWhere.Replace("WHERE ", string.Empty);
            string[] arrInsertKeys = insertKeys.Split(',');
            DateTime startDate;
            if (!CedaUtils.CedaDateToDateTime(arrInsertKeys[0], DateTimeKind.Local, out startDate))
                return;
            DateTime endDate;
            if (!CedaUtils.CedaDateToDateTime(arrInsertKeys[1], DateTimeKind.Local, out endDate))
                return;
            endDate = endDate.AddDays(1).AddSeconds(-1);

            DateTime dtmUTCStartDate = TimeZoneInfo.ConvertTime(_flightFilter.AbsoluteStartDate,
                    DlUfisData.Current.DataContext.DefaultTimeZoneInfo);
            DateTime dtmUTCEndDate = TimeZoneInfo.ConvertTime(_flightFilter.AbsoluteEndDate,
                    DlUfisData.Current.DataContext.DefaultTimeZoneInfo);

            if ((dtmUTCStartDate <= endDate) && (dtmUTCEndDate >= startDate))
                LoadFlightData(false);
        }

        //private void HandleDFR(EntityCollectionBase<EntDbFlight> flights, CedaBroadcastMessage message)
        //{
        //    string[] arrData = message.Data.Split(',');
        //    if (arrData.Length == 0)
        //        return;

        //    string strUrno = arrData[0];
        //    int intUrno;
        //    if (!int.TryParse(strUrno, out intUrno))
        //        return;

        //    EntDbFlight flight = new EntDbFlight() { Urno = intUrno };
        //    int intIndex = flights.IndexOf(flight);
        //    if (intIndex >= 0)
        //        flights.RemoveAt(intIndex);
        //}

        #endregion

        #region Constructor

        public MainWindowViewModel()
        {
            DisplayName = HpAppInfo.Current.ProductTitle;

            Columns = new ObservableCollection<UfisGridColumn>()
            {
                new UfisGridColumn() { FieldName = "ReadyForTowing", Header = "Ready", Width = 60, AllowEditing = DefaultBoolean.True, Settings = UfisGridColumnSettingsType.Check, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "FullFlightNumber", Header = "Flight No", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "RegistrationNumber", Header = "Reg. No", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "AircraftIATACode", Header = "A/C Type", Width = 80, AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "PositionOfDeparture", Header = "From Bay", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "PositionOfArrival", Header = "To Bay", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "StandardTimeOfDeparture", Header = "STD", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "StandardTimeOfArrival", Header = "STA", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "OffblockTime", Header = "Offblock", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "OnblockTime", Header = "Onblock", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "Remark1", Header = "Remark", Width = 300, AllowEditing = DefaultBoolean.False }
            };

            DateTime dtmNow = DateTime.Now;
            _flightFilter = new FlightFilter()
            {
                DateFilterType = FlightDateFilterType.AbsoluteDate,
                AbsoluteStartDate = dtmNow.AddHours(-2),
                AbsoluteEndDate = dtmNow.AddHours(8),
                RelativeStartRange = -2,
                RelativeEndRange = 8,
                FlightTypes = FlightTypesBuilder.GetFlightTypeList(FlightType.Towing | FlightType.GroundMovement)
            };
        }
        #endregion // Constructor

        #region Commands

        protected override List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>()            
            {
                new CommandViewModel("Filter Flights", new RelayCommand(RunCommand))
                {
                    Glyph = ImageGetter.GetImage("filter_16x16.png"),
                    LargeGlyph = ImageGetter.GetImage("filter_32x32.png")
                }
            };
        }

        private void RunCommand(object parameter)
        {
            string strParameter = (parameter == null ? string.Empty : parameter.ToString());
            switch (strParameter)
            {
                case "Filter Flights":
                    OpenWorkspace(FlightFilter);
                    break;
                default:
                    break;
            }
        }

        #endregion // Commands

        ~MainWindowViewModel()
        {
            if (_flights != null)
                _flights.BroadcastReceived -= Flights_BroadcastReceived;  
        }
    }
}