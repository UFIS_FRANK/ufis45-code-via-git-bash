﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using Ufis.Security;

namespace GroundControlManager.Helpers
{
    public class HpUser
    {
        public static EntUser ActiveUser { get; set; }
        public static UserPrivileges Privileges { get; set; }
    }
}
