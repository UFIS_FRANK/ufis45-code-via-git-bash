﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace GroundControlManager.Entities
{
    [Entity(SerializedName="AFTTAB")]
    public class EntFlight : EntDbFlight
    {
        [Entity(SerializedName = "FLNO", MaxLength = 9)]
        public override string FullFlightNumber
        {
            get
            {
                string flightNo = base.FullFlightNumber;
                if (string.IsNullOrEmpty(flightNo))
                    flightNo = CallSign;
                return flightNo;
            }
            set
            {
                base.FullFlightNumber = value;
            }
        }

        [Entity(SerializedName = "CSGN", MaxLength = 8)]
        public override string CallSign
        {
            get
            {
                return base.CallSign;
            }
            set
            {
                if (base.CallSign != value)
                {
                    base.CallSign = value;
                    if (string.IsNullOrEmpty(base.FullFlightNumber))
                        OnPropertyChanged("FullFlightNumber");
                }
            }
        }
    }
}
