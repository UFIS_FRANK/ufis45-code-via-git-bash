﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Ufis.Data.Ceda;
using GroundControlManager.Entities;

namespace GroundControlManager.DataAccess
{
    public class DlGroundControlManager
    {
        public static EntityCollectionBase<EntFlight> LoadFlights(string attributeList, string whereClause)
        {
            EntityCollectionBase<EntFlight> flights = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntFlight),
                AttributeList = attributeList,
                EntityWhereClause = (whereClause ?? string.Empty)
            };

            try
            {
                flights = dataContext.OpenEntityCollection<EntFlight>(command);
            }
            catch (Exception e)
            {
                throw e;
            }

            return flights;
        }

        public static void FillFlights(EntityCollectionBase<EntFlight> flights, string attributeList, string whereClause)
        {
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntFlight),
                AttributeList = attributeList,
                EntityWhereClause = (whereClause ?? string.Empty)
            };

            try
            {
                dataContext.FillEntityCollection<EntFlight>(flights, command);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
