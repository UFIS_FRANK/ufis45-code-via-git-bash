﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;

namespace EFPS
{
    public partial class frmMain : Form
    {

        #region My Members
        /// <summary>
        /// The one and only IDatabase member reference within 
        /// the application. This is reused in all other modules.
        /// </summary>
        private IDatabase myDB;
        private int isCustomerPVG = -1;
        public string[] args;
        #endregion My Members

        private ITable myAFT;
        private ITable myEFP;
        private string strArrivalURNO = string.Empty;
        private string strDepartureURNO = string.Empty;
        private const string AFTTAB_DB_FIELDS = "URNO,FLNO,ADID,STOA,STOD";
        private const string AFTTAB_FIELD_LENGTHS = "10,10,1,14,14";

        private const string EFPTAB_DB_FIELDS = "URNO,UAFT,ACID,LAND,AIRB,ACT5,CJSS,CIRT,CLRR,CLRI,ORG4,DES4,ETOA,ETOD,FPAL,FREC,GRDR,GATA,INTX,LACL,MISS,GATD,PUSR,PUSI,REPK,REAE,ROUT,SIDE,TACR,TACI,TSAT,TRFL,TOCI,TRAN,WAKE,CDAT,LSTU,EFPI,EFPU,HOPO,STOA,STOD";
        private const string EFPTAB_FIELD_LENGTHS = "10,10,8,14,14,5,20,1,14,14,4,4,14,14,12,14,600,4,4,14,14,4,14,14,1,1,1000,10,14,14,14,1,14,4,8,14,14,14,14,3,14,14";
        string strUrno = string.Empty;
        string strFlno = string.Empty;
        string strAdid = string.Empty;
        string strStoa = string.Empty;
        string strStod = string.Empty;

        public frmMain()
        {
            InitializeComponent();
        }

        [STAThread]
        static void Main(string[] args)
        {
            frmMain olDlg = new frmMain();
            olDlg.args = args; 
            Application.Run(olDlg);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (!LoginProcedure())
            {
                Application.Exit();
            }

            string strUTCConvertion = string.Empty;
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
            UT.ServerName = myIni.IniReadValue("GLOBAL", "HOSTNAME");
            UT.Hopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
            strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");

            if (UT.UserName.Length == 0)
                UT.UserName = LoginControl.GetUserName();


            if (strUTCConvertion == "NO")
            {
                UT.UTCOffset = 0;
            }
            else
            {
                DateTime datLocal = DateTime.Now;
                DateTime datUTC = DateTime.UtcNow;
                TimeSpan span = datLocal - datUTC;

                UT.UTCOffset = (short)span.Hours;
            }
            try
            {
                myDB = UT.GetMemDB();
                bool result = myDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, "TAB", "EFPS");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connecting to Server Failed!", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            CheckRecord();
        }

        private void CheckRecord()
        {
            myAFT = myDB.Bind("AFT", "AFT", AFTTAB_DB_FIELDS, AFTTAB_FIELD_LENGTHS, AFTTAB_DB_FIELDS);
            myAFT.Clear();
            myAFT.Load("WHERE URNO = '" + strArrivalURNO + "' OR URNO = '" + strDepartureURNO + "'");            
            bool isRotational = false;
            if (myAFT.Count > 0)
            {
                if (myAFT.Count == 2)
                    isRotational = true;
                else
                    isRotational = false;
                for (int i = 0; i < myAFT.Count; i++)
                {
                    strUrno = myAFT[i]["URNO"];
                    strFlno = myAFT[i]["FLNO"];
                    strAdid = myAFT[i]["ADID"];
                    strStoa = myAFT[i]["STOA"];
                    strStod= myAFT[i]["STOD"];
                     

                    myEFP = myDB.Bind("EFP", "EFP", EFPTAB_DB_FIELDS, EFPTAB_FIELD_LENGTHS, EFPTAB_DB_FIELDS);
                    myEFP.Clear();
                    myEFP.Load("WHERE UAFT ='" + strUrno + "'"); 
                    if (myEFP.Count > 0 && strAdid == "A")
                    {
                        FillArrivalValues(isRotational);
                    }
                    else if (myEFP.Count > 0 && strAdid == "D")
                    {
                        FillDepartureValues();
                    }
                }
            }
        }

        private void FillArrivalValues(bool isRotational)
        {
            if (myEFP.Count == 1)
            {
                for (int i = 0; i < myEFP.Count; i++)
                {
                    if (!isRotational)
                    {
                        txtSID.Text = myEFP[i]["SIDE"];
                        txtTCode.Text = myEFP[i]["TRAN"];
                        txtFlightAltitude.Text = myEFP[i]["FPAL"];
                        if (myEFP[i]["CIRT"] == "1")
                            cBoxCircuit.Checked = true;
                        else
                            cBoxCircuit.Checked = false;

                        if (myEFP[i]["TRFL"] == "1")
                            cBoxTraining.Checked = true;
                        else
                            cBoxTraining.Checked = false; 
                    }

                    if (!string.IsNullOrEmpty(strFlno))
                    {
                        ArrtxtFlno1.Text = strFlno.Substring(0, 2);
                        ArrtxtFlno2.Text = strFlno.Substring(3, strFlno.Length - 3);
                    }
                    ArrtxtCallSign.Text = myEFP[i]["ACID"];
                    ArrtxtAircraftType.Text = myEFP[i]["ACT5"];
                    ArrtxtCJS.Text = myEFP[i]["CJSS"];
                    ArrtxtRoute.Text = myEFP[i]["ROUT"];
                    txtDepartAirport.Text = myEFP[i]["ORG4"];
                    txtInboundGate.Text = myEFP[i]["GATA"];
                    txtIntersection.Text = myEFP[i]["INTX"];
                    txtArrivalGroudRoute.Text = myEFP[i]["GRDR"];
                    Helper.StandardDateTime = UT.UtcToLocal(UT.CedaDateToDateTime(strStoa));
                    //if (!string.IsNullOrEmpty(strStoa))
                    //    txtScheduedArrivalTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(strStoa)));
                    if (!string.IsNullOrEmpty(myEFP[i]["STOA"]))
                        txtScheduedArrivalTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["STOA"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["FREC"]))
                        ArrtxtFreqChange.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["FREC"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["LACL"]))
                        txtLandingClearance.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["LACL"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["EFPI"]))
                        txtArrivalCreatedOn.Text = Helper.DateTimetoUFISDateTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["EFPI"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["EFPU"]))
                        txtArrivalUpdatedOn.Text = Helper.DateTimetoUFISDateTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["EFPU"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["MISS"]))
                        txtMissedTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["MISS"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["ETOA"]))
                        txtEstimatedArrivalTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["ETOA"])));                  
                    if (!string.IsNullOrEmpty(myEFP[i]["LAND"]))
                        txtActualArrivalTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["LAND"])));
                }
            }
        }

        private void FillDepartureValues()
        {
            if (myEFP.Count == 1)
            {
                for (int i = 0; i < myEFP.Count; i++)
                {
                    if (!string.IsNullOrEmpty(strFlno))
                    {
                        DeptxtFlno1.Text = strFlno.Substring(0, 2);
                        DeptxtFlno2.Text = strFlno.Substring(3, strFlno.Length - 3);
                    }
                    txtSID.Text = myEFP[i]["SIDE"];
                    txtTCode.Text = myEFP[i]["TRAN"];
                    txtFlightAltitude.Text = myEFP[i]["FPAL"];
                    if (myEFP[i]["CIRT"] == "1")
                        cBoxCircuit.Checked = true;
                    else
                        cBoxCircuit.Checked = false;

                    if (myEFP[i]["TRFL"] == "1")
                        cBoxTraining.Checked = true;
                    else
                        cBoxTraining.Checked = false;

                    if (myEFP[i]["REAE"] == "1")
                        cBoxAerodrome.Checked = true;
                    else
                        cBoxAerodrome.Checked = false;

                    if (myEFP[i]["REPK"] == "1")
                        cBoxParking.Checked = true;
                    else
                        cBoxParking.Checked = false;

                    DeptxtCallSign.Text = myEFP[i]["ACID"];
                    DeptxtAircraftType.Text = myEFP[i]["ACT5"];
                    DeptxtCJS.Text = myEFP[i]["CJSS"];
                    DeptxtRoute.Text = myEFP[i]["ROUT"];
                    txtDesignationAirport.Text = myEFP[i]["DES4"];
                    txtOutboundGate.Text = myEFP[i]["GATD"];
                    txtDepartureGroudRoute.Text = myEFP[i]["GRDR"];
                    txtWake.Text = myEFP[i]["WAKE"];


                     Helper.StandardDateTime = UT.UtcToLocal(UT.CedaDateToDateTime(strStod)); 
                    //if (!string.IsNullOrEmpty(strStod))
                    //    txtScheduedDepartureTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(strStod)));
                    if (!string.IsNullOrEmpty(myEFP[i]["STOD"]))
                        txtScheduedDepartureTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["STOD"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["FREC"]))
                        DeptxtFreqChange.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["FREC"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["PUSR"]))
                        txtPuchRequest.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["PUSR"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["PUSI"]))
                        txtPushIssued.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["PUSI"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["CLRR"]))
                        txtClearanceRequest.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["CLRR"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["CLRI"]))
                        txtClearanceIssued.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["CLRI"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["EFPI"]))
                        txtDepartureCreatedOn.Text = Helper.DateTimetoUFISDateTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["EFPI"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["EFPU"]))
                        txtDepartureUpdatedOn.Text = Helper.DateTimetoUFISDateTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["EFPU"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["TACR"]))
                        txtTaxiRequest.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["TACR"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["TACI"]))
                        txtTaxiIssued.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["TACI"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["TOCI"]))
                        txtTakeoffIssued.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["TOCI"])));                 
                    if (!string.IsNullOrEmpty(myEFP[i]["ETOD"]))
                        txtEstimatedDepartureTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["ETOD"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["AIRB"]))
                        txtActualDepartureTime.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["AIRB"])));
                    if (!string.IsNullOrEmpty(myEFP[i]["TSAT"]))
                        txtTsat.Text = Helper.DateTimetoUFISTime(UT.UtcToLocal(UT.CedaDateToDateTime(myEFP[i]["TSAT"])));
                }
            }
        }

        private void FillValues()
        {
            if (myEFP.Count == 1)
            {
                for (int i = 0; i < myEFP.Count; i++)
                {
                    DeptxtCallSign.Text = myEFP[i]["ACID"];
                    DeptxtAircraftType.Text = myEFP[i]["AIRB"];
                    ArrtxtCJS.Text = myEFP[i]["CJSS"];
                    txtTCode.Text = myEFP[i]["TRAN"];
                    txtFlightAltitude.Text = myEFP[i]["FPAL"];
                    ArrtxtFreqChange.Text = myEFP[i]["FREC"];
                    txtMissedTime.Text = myEFP[i]["MISS"];
                    txtSID.Text = myEFP[i]["SIDE"];
                    ArrtxtRoute.Text = myEFP[i]["ROUT"];
                    if (myEFP[i]["CIRT"] == "1")
                        cBoxCircuit.Checked = true;
                    else
                        cBoxCircuit.Checked = false;

                    if (myEFP[i]["TRFL"] == "1")
                        cBoxTraining.Checked = true;
                    else
                        cBoxTraining.Checked = false;

                    if (myEFP[i]["REAE"] == "1")
                        cBoxAerodrome.Checked = true;
                    else
                        cBoxAerodrome.Checked = false;

                    if (myEFP[i]["REPK"] == "1")
                        cBoxParking.Checked = true;
                    else
                        cBoxParking.Checked = false;

                    txtDepartAirport.Text = myEFP[i]["ORG4"];
                    txtEstimatedArrivalTime.Text = myEFP[i]["ETOA"];
                    txtActualArrivalTime.Text = myEFP[i]["LAND"];
                    txtActualDepartureTime.Text = myEFP[i]["AIRB"];
                    txtInboundGate.Text = myEFP[i]["GATA"];
                    txtLandingClearance.Text = myEFP[i]["LACL"];
                    txtIntersection.Text = myEFP[i]["INTX"];
                    txtArrivalGroudRoute.Text = myEFP[i]["GRDR"];
                    txtArrivalCreatedOn.Text = UT.CedaDateToDateTime(myEFP[i]["EFPI"]).ToString();
                    txtArrivalUpdatedOn.Text = UT.CedaDateToDateTime(myEFP[i]["EFPO"]).ToString();

                    txtDesignationAirport.Text = myEFP[i]["DES4"];
                    txtEstimatedDepartureTime.Text = myEFP[i]["ETOD"];
                    txtOutboundGate.Text = myEFP[i]["GATD"];
                    txtPuchRequest.Text = myEFP[i]["PUSR"];
                    txtPushIssued.Text = myEFP[i]["PUSI"];
                    txtClearanceRequest.Text = myEFP[i]["CLRR"];
                    txtClearanceIssued.Text = myEFP[i]["CLRI"];
                    txtDepartureGroudRoute.Text = myEFP[i]["GRDR"];
                    txtTaxiRequest.Text = myEFP[i]["TACR"];
                    txtTaxiIssued.Text = myEFP[i]["TACI"];
                    txtTakeoffIssued.Text = myEFP[i]["TOCI"];
                    txtWake.Text = myEFP[i]["WAKE"];
                    txtTsat.Text = myEFP[i]["TSAT"];
                    txtDepartureCreatedOn.Text = UT.CedaDateToDateTime(myEFP[i]["EFPI"]).ToString();
                    txtDepartureUpdatedOn.Text = UT.CedaDateToDateTime(myEFP[i]["EFPU"]).ToString();
                }
            }
        }


        private void btnOk_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// This function pops up the login dialog and opens the application or closes the application
        /// depending on the user rights.This is done for the PRF 8523
        /// </summary>
        /// <returns></returns>
        private bool LoginProcedure()
        {
            string LoginAnsw = "";
            string tmpRegStrg = "";
            string strRet = "";

            LoginControl.ApplicationName = "EFPS";
            LoginControl.VersionString = Application.ProductVersion;
            LoginControl.InfoCaption = "Info about User Account Manager";
            LoginControl.InfoButtonVisible = true;
            LoginControl.InfoUfisVersion = "Ufis Version 4.5";
            LoginControl.InfoAppVersion = "EFPS " + Application.ProductVersion.ToString();
            LoginControl.InfoAAT = "UFIS Airport Solutions GmbH";
            LoginControl.UserNameLCase = true;

            tmpRegStrg = "EFPS" + ",";
            tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
            LoginControl.RegisterApplicationString = tmpRegStrg;
            if (args.Length == 0)
            {
                strRet = LoginControl.ShowLoginDialog();
            }
            else
            {
                string userId = args[0];
                string password = "";
                bool hasUserIdNPwd = false;
                string[] stParams = args[0].Split('|');
                string[] stParam = stParams[0].Split(',');
                if (stParams.Length >= 4)
                {
                    string[] stTemps = stParams[1].Split(',');
                    userId = stTemps[0];
                    password = stParams[3];
                    if (stParam[0] != "EFPS")
                        this.Close();
                    else if (stParam[1] != string.Empty)
                        strArrivalURNO = stParam[1];
                    if (stParam[2] != string.Empty)
                        strDepartureURNO = stParam[2];
                    hasUserIdNPwd = true;
                }
                else if (args.Length == 2)
                {
                    password = args[1];
                    hasUserIdNPwd = true;
                }
                if (hasUserIdNPwd)
                {
                    strRet = LoginControl.DoLoginSilentMode(userId, password);
                }
                if (strRet != "OK")
                    strRet = LoginControl.ShowLoginDialog();
            }
            LoginAnsw = LoginControl.GetPrivileges("InitModu");
            if (LoginAnsw != "" && strRet != "CANCEL")
            {
                return true;
            }
            else
                return false;
        }
    }
}
