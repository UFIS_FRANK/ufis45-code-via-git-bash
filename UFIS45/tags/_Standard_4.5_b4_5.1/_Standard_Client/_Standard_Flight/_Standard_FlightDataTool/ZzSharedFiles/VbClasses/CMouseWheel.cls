VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMouseWheel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Option Compare Text

Private frm As Object
Private intCancel As Integer
Public Event WheelRotation(ByVal hwnd As Long, _
    ByVal uMsg As Long, _
    ByVal wParam As Long, _
    ByVal lParam As Long, Cancel As Integer)

Public Property Set Form(frmIn As Object)
    Set frm = frmIn
End Property

Public Property Get UfisMouseWheelCancel() As Integer
    UfisMouseWheelCancel = intCancel
End Property

Public Sub SubClassHookForm()
    lpPrevWndProc = SetWindowLong(frm.hwnd, GWL_WNDPROC, _
                                    AddressOf WindowProc)
      Set CMouse = Me
   End Sub

Public Sub SubClassUnHookForm()
    Call SetWindowLong(frm.hwnd, GWL_WNDPROC, lpPrevWndProc)
End Sub

Public Sub FireWheelRotation(ByVal xhwnd As Long, _
    ByVal xuMsg As Long, _
    ByVal xwParam As Long, _
    ByVal xlParam As Long)
    RaiseEvent WheelRotation(ByVal xhwnd, _
    xuMsg, _
    xwParam, _
    xlParam, intCancel)
End Sub


