VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form PdaInterface 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "UFIS PDA"
   ClientHeight    =   8250
   ClientLeft      =   1125
   ClientTop       =   1500
   ClientWidth     =   14955
   FillColor       =   &H00FFFFFF&
   Icon            =   "PdaInterface.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   Picture         =   "PdaInterface.frx":08CA
   ScaleHeight     =   550
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   997
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PdaPanel 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   4785
      Index           =   3
      Left            =   10620
      ScaleHeight     =   4785
      ScaleWidth      =   3585
      TabIndex        =   102
      Top             =   900
      Visible         =   0   'False
      Width           =   3585
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1110
         Index           =   12
         Left            =   30
         TabIndex        =   104
         Top             =   -60
         Width           =   3510
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            BackColor       =   &H000000FF&
            Caption         =   $"PdaInterface.frx":18FCAC
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   855
            Index           =   7
            Left            =   60
            TabIndex        =   105
            Top             =   180
            Width           =   3375
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1470
         Index           =   13
         Left            =   30
         TabIndex        =   103
         Top             =   1020
         Width           =   3510
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Caption         =   "Message Text"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1215
            Index           =   8
            Left            =   60
            TabIndex        =   107
            Top             =   180
            Width           =   3390
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1170
         Index           =   15
         Left            =   30
         TabIndex        =   110
         Top             =   2460
         Width           =   3510
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Caption         =   "Message Text"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   915
            Index           =   9
            Left            =   60
            TabIndex        =   111
            Top             =   180
            Width           =   3390
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1125
         Index           =   14
         Left            =   30
         TabIndex        =   106
         Top             =   3615
         Width           =   3510
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Deny"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   20
            Left            =   1830
            Style           =   1  'Graphical
            TabIndex        =   109
            Top             =   690
            Width           =   1560
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Allow"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   19
            Left            =   120
            Style           =   1  'Graphical
            TabIndex        =   108
            Top             =   690
            Width           =   1560
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "No, Discard the message."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   825
            Index           =   10
            Left            =   1800
            TabIndex        =   114
            Top             =   210
            Width           =   1605
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Yes, unlock work area."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   825
            Index           =   12
            Left            =   90
            TabIndex        =   113
            Top             =   210
            Width           =   1605
         End
      End
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Skin"
      Height          =   315
      Left            =   4710
      TabIndex        =   82
      Top             =   1950
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Alert"
      Height          =   315
      Left            =   4710
      TabIndex        =   81
      Top             =   1620
      Visible         =   0   'False
      Width           =   525
   End
   Begin MSComctlLib.ProgressBar ProgressBar 
      Height          =   60
      Left            =   10590
      TabIndex        =   80
      Top             =   5715
      Visible         =   0   'False
      Width           =   3675
      _ExtentX        =   6482
      _ExtentY        =   106
      _Version        =   393216
      Appearance      =   0
      Enabled         =   0   'False
      Scrolling       =   1
   End
   Begin VB.Timer PdaTimer 
      Enabled         =   0   'False
      Left            =   480
      Top             =   6750
   End
   Begin VB.Timer InetTimer 
      Enabled         =   0   'False
      Left            =   30
      Top             =   6750
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "Top"
      Height          =   315
      Left            =   4710
      Style           =   1  'Graphical
      TabIndex        =   67
      TabStop         =   0   'False
      Top             =   960
      Visible         =   0   'False
      Width           =   510
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Test"
      Height          =   315
      Left            =   4710
      TabIndex        =   35
      Top             =   1290
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.PictureBox PdaPanel 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   4380
      Index           =   2
      Left            =   10620
      ScaleHeight     =   4380
      ScaleWidth      =   3585
      TabIndex        =   4
      Top             =   1305
      Width           =   3585
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Registration Basic Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1005
         Index           =   5
         Left            =   30
         TabIndex        =   36
         Top             =   90
         Visible         =   0   'False
         Width           =   3495
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Refresh"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   7
            Left            =   2400
            Style           =   1  'Graphical
            TabIndex        =   42
            Top             =   600
            Width           =   1005
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   22
            Left            =   90
            MaxLength       =   3
            TabIndex        =   41
            Top             =   600
            Width           =   675
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   21
            Left            =   780
            MaxLength       =   5
            TabIndex        =   40
            Top             =   600
            Width           =   765
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   20
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   39
            Top             =   600
            Width           =   795
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   90
            TabIndex        =   38
            Top             =   240
            Width           =   2265
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Update"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   2400
            Style           =   1  'Graphical
            TabIndex        =   37
            Top             =   600
            Width           =   1005
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H000040C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4440
         Index           =   9
         Left            =   960
         TabIndex        =   68
         Top             =   3420
         Visible         =   0   'False
         Width           =   3630
         Begin SHDocVwCtl.WebBrowser WebBrowser 
            Height          =   4455
            Left            =   -30
            TabIndex        =   69
            Top             =   270
            Width           =   3660
            ExtentX         =   6456
            ExtentY         =   7858
            ViewMode        =   0
            Offline         =   0
            Silent          =   0
            RegisterAsBrowser=   0
            RegisterAsDropTarget=   1
            AutoArrange     =   0   'False
            NoClientEdge    =   0   'False
            AlignLeft       =   0   'False
            NoWebView       =   0   'False
            HideFileNames   =   0   'False
            SingleClick     =   0   'False
            SingleSelection =   0   'False
            NoFolders       =   0   'False
            Transparent     =   0   'False
            ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
            Location        =   ""
         End
      End
      Begin TABLib.TAB PdaTab 
         Height          =   3285
         Index           =   3
         Left            =   0
         TabIndex        =   51
         Top             =   2730
         Visible         =   0   'False
         Width           =   3585
         _Version        =   65536
         _ExtentX        =   6324
         _ExtentY        =   5794
         _StockProps     =   64
         Enabled         =   0   'False
      End
      Begin TABLib.TAB PdaTab 
         Height          =   3285
         Index           =   4
         Left            =   0
         TabIndex        =   60
         Top             =   2400
         Visible         =   0   'False
         Width           =   3585
         _Version        =   65536
         _ExtentX        =   6324
         _ExtentY        =   5794
         _StockProps     =   64
      End
      Begin TABLib.TAB PdaTab 
         Height          =   3285
         Index           =   0
         Left            =   0
         TabIndex        =   8
         Top             =   2040
         Visible         =   0   'False
         Width           =   3585
         _Version        =   65536
         _ExtentX        =   6324
         _ExtentY        =   5794
         _StockProps     =   64
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "A/C Type Basic Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1005
         Index           =   7
         Left            =   960
         TabIndex        =   53
         Top             =   840
         Visible         =   0   'False
         Width           =   3495
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Refresh"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   9
            Left            =   2400
            Style           =   1  'Graphical
            TabIndex        =   54
            Top             =   600
            Width           =   1005
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Update"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   10
            Left            =   2400
            Style           =   1  'Graphical
            TabIndex        =   59
            Top             =   600
            Width           =   1005
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   26
            Left            =   90
            MaxLength       =   35
            TabIndex        =   58
            Top             =   240
            Width           =   2265
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   25
            Left            =   1560
            MaxLength       =   5
            TabIndex        =   57
            Top             =   600
            Width           =   795
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   24
            Left            =   780
            MaxLength       =   5
            TabIndex        =   56
            Top             =   600
            Width           =   765
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   23
            Left            =   90
            MaxLength       =   3
            TabIndex        =   55
            Top             =   600
            Width           =   675
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1125
         Index           =   6
         Left            =   690
         TabIndex        =   50
         Top             =   150
         Visible         =   0   'False
         Width           =   3495
      End
      Begin VB.Image PdaIcon 
         Height          =   1110
         Index           =   7
         Left            =   -2550
         MouseIcon       =   "PdaInterface.frx":18FD3E
         MousePointer    =   99  'Custom
         Picture         =   "PdaInterface.frx":18FE90
         Stretch         =   -1  'True
         Top             =   150
         Visible         =   0   'False
         Width           =   3570
      End
   End
   Begin VB.PictureBox PdaPanel 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   4380
      Index           =   1
      Left            =   705
      ScaleHeight     =   4380
      ScaleWidth      =   3585
      TabIndex        =   3
      Top             =   1305
      Width           =   3585
      Begin TABLib.TAB PdaTab 
         Height          =   2010
         Index           =   1
         Left            =   0
         TabIndex        =   19
         Top             =   0
         Width           =   3585
         _Version        =   65536
         _ExtentX        =   6324
         _ExtentY        =   3545
         _StockProps     =   64
      End
      Begin TABLib.TAB PdaTab 
         Height          =   2010
         Index           =   2
         Left            =   0
         TabIndex        =   43
         Top             =   2385
         Width           =   3585
         _Version        =   65536
         _ExtentX        =   6324
         _ExtentY        =   3545
         _StockProps     =   64
      End
      Begin VB.Image PdaIcon 
         Height          =   240
         Index           =   3
         Left            =   30
         MouseIcon       =   "PdaInterface.frx":19DB22
         MousePointer    =   99  'Custom
         Picture         =   "PdaInterface.frx":19DC74
         Top             =   2100
         Width           =   240
      End
      Begin VB.Label PdaCption 
         Alignment       =   2  'Center
         BackColor       =   &H80000001&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Notification Reasons"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   345
         Index           =   1
         Left            =   -45
         TabIndex        =   52
         Top             =   2040
         Width           =   3675
      End
   End
   Begin VB.PictureBox PdaPanel 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   4380
      Index           =   0
      Left            =   5655
      ScaleHeight     =   4380
      ScaleWidth      =   3585
      TabIndex        =   2
      Top             =   1305
      Width           =   3585
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Message Details"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   10
         Left            =   45
         TabIndex        =   83
         Top             =   2010
         Width           =   3495
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   8
            Left            =   315
            Locked          =   -1  'True
            TabIndex        =   93
            Top             =   255
            Width           =   1035
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   9
            Left            =   2715
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   92
            Top             =   255
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   10
            Left            =   1995
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   91
            Top             =   255
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   11
            Left            =   1365
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   90
            Top             =   255
            Width           =   615
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   28
            Left            =   315
            Locked          =   -1  'True
            TabIndex        =   89
            Top             =   585
            Width           =   1035
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   29
            Left            =   2715
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   88
            Top             =   585
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   30
            Left            =   1995
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   87
            Top             =   585
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFC0C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   31
            Left            =   1365
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   86
            Top             =   585
            Width           =   615
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "I"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   11
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   85
            ToolTipText     =   "External Message Data"
            Top             =   255
            Width           =   240
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "F"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   13
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   84
            ToolTipText     =   "Current Flight Data"
            Top             =   585
            Width           =   240
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "S"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   12
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   94
            ToolTipText     =   "Snapshot of Flight Data"
            Top             =   255
            Width           =   240
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   15
            Left            =   315
            Locked          =   -1  'True
            TabIndex        =   95
            Top             =   255
            Width           =   1035
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   14
            Left            =   2715
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   96
            Top             =   255
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   13
            Left            =   1995
            Locked          =   -1  'True
            MaxLength       =   5
            TabIndex        =   97
            Top             =   255
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   12
            Left            =   1365
            Locked          =   -1  'True
            MaxLength       =   3
            TabIndex        =   98
            Top             =   255
            Width           =   615
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Index           =   3
         Left            =   2460
         TabIndex        =   24
         Top             =   60
         Width           =   1095
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Insert"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   61
            ToolTipText     =   "Create (or update) a new A/C Type"
            Top             =   510
            Width           =   975
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Basic Data"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   26
            Top             =   180
            Width           =   975
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   4
         Left            =   2460
         TabIndex        =   25
         Top             =   990
         Width           =   1095
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Basic Data"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   255
            Width           =   975
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Insert"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   27
            ToolTipText     =   "Create (or update) a new A/C Registration"
            Top             =   585
            Width           =   975
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Pending Flight Update"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   930
         Index           =   2
         Left            =   45
         TabIndex        =   18
         Top             =   3030
         Width           =   3495
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   19
            Left            =   315
            TabIndex        =   23
            Top             =   540
            Width           =   1035
         End
         Begin VB.Frame PdaFrame 
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   11
            Left            =   90
            TabIndex        =   99
            ToolTipText     =   "Update A/C Type Only (Remove Registration From Flight)"
            Top             =   330
            Width           =   1245
            Begin VB.Label lblAny 
               AutoSize        =   -1  'True
               BackColor       =   &H00000000&
               BackStyle       =   0  'Transparent
               Caption         =   "Registration"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   240
               Index           =   6
               Left            =   240
               TabIndex        =   100
               Top             =   0
               Width           =   1035
            End
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   18
            Left            =   2715
            MaxLength       =   5
            TabIndex        =   22
            Top             =   540
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   17
            Left            =   1995
            MaxLength       =   5
            TabIndex        =   21
            Top             =   540
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   16
            Left            =   1365
            MaxLength       =   3
            TabIndex        =   20
            Top             =   540
            Width           =   615
         End
         Begin VB.CheckBox chkFunc 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   14
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   76
            ToolTipText     =   "Update A/C Type Only (Remove Registration From Flight)"
            Top             =   300
            Width           =   1335
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00000000&
            BackStyle       =   0  'Transparent
            Caption         =   "IATA"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   5
            Left            =   1485
            TabIndex        =   34
            Top             =   300
            Width           =   405
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00000000&
            BackStyle       =   0  'Transparent
            Caption         =   "ICAO"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   4
            Left            =   2130
            TabIndex        =   33
            Top             =   300
            Width           =   435
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00000000&
            BackStyle       =   0  'Transparent
            Caption         =   "Internal"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   0
            Left            =   2745
            TabIndex        =   32
            Top             =   300
            Width           =   645
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "New Registration"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Index           =   1
         Left            =   30
         TabIndex        =   13
         Top             =   990
         Width           =   2505
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   17
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   79
            ToolTipText     =   "Copy Aircraft Type"
            Top             =   580
            Width           =   240
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   16
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   78
            ToolTipText     =   "Copy Registration"
            Top             =   255
            Width           =   240
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   7
            Left            =   300
            MaxLength       =   3
            TabIndex        =   17
            Top             =   585
            Width           =   645
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   960
            MaxLength       =   5
            TabIndex        =   16
            Top             =   580
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   15
            Top             =   580
            Width           =   720
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   300
            Locked          =   -1  'True
            TabIndex        =   14
            Top             =   255
            Width           =   2100
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "New Aircraft Type"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Index           =   0
         Left            =   30
         TabIndex        =   9
         Top             =   60
         Width           =   2505
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   ">"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   15
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   77
            ToolTipText     =   "Copy Aircraft Type"
            Top             =   480
            Width           =   240
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   27
            Left            =   390
            Locked          =   -1  'True
            TabIndex        =   66
            Top             =   780
            Visible         =   0   'False
            Width           =   435
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   12
            Top             =   480
            Width           =   720
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   960
            MaxLength       =   5
            TabIndex        =   11
            Top             =   480
            Width           =   705
         End
         Begin VB.TextBox txtFunc 
            Alignment       =   2  'Center
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   300
            MaxLength       =   3
            TabIndex        =   10
            Top             =   480
            Width           =   645
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00000000&
            BackStyle       =   0  'Transparent
            Caption         =   "IATA"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   1
            Left            =   390
            TabIndex        =   31
            Top             =   240
            Width           =   405
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00000000&
            BackStyle       =   0  'Transparent
            Caption         =   "ICAO"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   2
            Left            =   1065
            TabIndex        =   30
            Top             =   240
            Width           =   435
         End
         Begin VB.Label lblAny 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00000000&
            BackStyle       =   0  'Transparent
            Caption         =   "Internal"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   3
            Left            =   1740
            TabIndex        =   29
            Top             =   240
            Width           =   645
         End
      End
      Begin VB.Frame PdaFrame 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   540
         Index           =   8
         Left            =   45
         TabIndex        =   62
         Top             =   3810
         Width           =   3495
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   " Ignore"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   18
            Left            =   60
            Style           =   1  'Graphical
            TabIndex        =   101
            ToolTipText     =   "Remove this Notification Message"
            Top             =   180
            Width           =   765
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Flight A/C"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   1590
            Style           =   1  'Graphical
            TabIndex        =   65
            ToolTipText     =   "Refresh the Flight A/C Display (F)"
            Top             =   180
            Width           =   945
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Confirm"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   2550
            Style           =   1  'Graphical
            TabIndex        =   64
            ToolTipText     =   "Update the flight record"
            Top             =   180
            Width           =   885
         End
         Begin VB.CheckBox chkFunc 
            BackColor       =   &H00E0E0E0&
            Caption         =   "FIPS"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   8
            Left            =   840
            Style           =   1  'Graphical
            TabIndex        =   63
            ToolTipText     =   "Show the flight in FIPS Rotation Dialog Mask"
            Top             =   180
            Width           =   735
         End
      End
   End
   Begin VB.TextBox Text1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   960
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   6870
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.PictureBox picShape 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   810
      Index           =   0
      Left            =   -7050
      MousePointer    =   5  'Size
      ScaleHeight     =   810
      ScaleWidth      =   7860
      TabIndex        =   0
      Top             =   7200
      Visible         =   0   'False
      Width           =   7860
   End
   Begin VB.Label lblAny 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0FF&
      Caption         =   "Note: The Work Area Is Locked."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   11
      Left            =   6060
      TabIndex        =   112
      Top             =   900
      Visible         =   0   'False
      Width           =   3180
   End
   Begin VB.Image Image1 
      Height          =   345
      Index           =   8
      Left            =   2670
      Picture         =   "PdaInterface.frx":19DDBE
      Tag             =   "582,30"
      Top             =   7170
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   7
      Left            =   1860
      Picture         =   "PdaInterface.frx":19E7B4
      Tag             =   "608,16"
      Top             =   7590
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   6
      Left            =   1530
      Picture         =   "PdaInterface.frx":19EEF6
      Tag             =   "369,16"
      ToolTipText     =   "Toggle Topmost Position (Actually OFF)"
      Top             =   7590
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   5
      Left            =   1200
      Picture         =   "PdaInterface.frx":19F638
      Tag             =   "369,16"
      Top             =   7590
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image Image1 
      Height          =   480
      Index           =   4
      Left            =   870
      Picture         =   "PdaInterface.frx":19FD7A
      Tag             =   "369,16"
      ToolTipText     =   "Toggle Topmost Position (Actually ON)"
      Top             =   7590
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image PdaButton 
      Height          =   405
      Index           =   12
      Left            =   10590
      MouseIcon       =   "PdaInterface.frx":1A04BC
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A060E
      Top             =   405
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Image PdaButton 
      Height          =   345
      Index           =   0
      Left            =   8700
      MouseIcon       =   "PdaInterface.frx":1A0EC0
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A1012
      ToolTipText     =   "Close"
      Top             =   435
      Width           =   525
   End
   Begin VB.Image PdaButton 
      Height          =   480
      Index           =   1
      Left            =   5535
      MouseIcon       =   "PdaInterface.frx":1A1A08
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A1B5A
      Top             =   240
      Width           =   270
   End
   Begin VB.Image PdaButton 
      Height          =   405
      Index           =   11
      Left            =   660
      MouseIcon       =   "PdaInterface.frx":1A229C
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A23EE
      Top             =   405
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Image Image1 
      Height          =   405
      Index           =   3
      Left            =   2220
      Picture         =   "PdaInterface.frx":1A2CA0
      Tag             =   "373,27"
      Top             =   7170
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Image Image1 
      Height          =   405
      Index           =   2
      Left            =   1770
      Picture         =   "PdaInterface.frx":1A3552
      Tag             =   "373,27"
      ToolTipText     =   "Toggle Topmost Position (Actually OFF)"
      Top             =   7170
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Image PdaIcon 
      Height          =   360
      Index           =   8
      Left            =   13800
      MouseIcon       =   "PdaInterface.frx":1A3E04
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A3F56
      Top             =   900
      Visible         =   0   'False
      Width           =   360
   End
   Begin VB.Image PdaIcon 
      Height          =   240
      Index           =   5
      Left            =   4335
      MouseIcon       =   "PdaInterface.frx":1A4160
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A42B2
      Top             =   1920
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image PdaIcon 
      Height          =   240
      Index           =   4
      Left            =   10365
      MouseIcon       =   "PdaInterface.frx":1A43FC
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A454E
      Top             =   2850
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image PdaIcon 
      Height          =   480
      Index           =   2
      Left            =   10650
      MouseIcon       =   "PdaInterface.frx":1A4698
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A47EA
      Top             =   840
      Width           =   480
   End
   Begin VB.Image PdaIcon 
      Height          =   375
      Index           =   1
      Left            =   720
      MouseIcon       =   "PdaInterface.frx":1A4C2C
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A4D7E
      Stretch         =   -1  'True
      Top             =   900
      Width           =   375
   End
   Begin VB.Label PdaConText 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BOTTOM LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   210
      Index           =   2
      Left            =   11790
      TabIndex        =   48
      Top             =   5835
      Width           =   1290
   End
   Begin VB.Label PdaConText 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BOTTOM LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   210
      Index           =   0
      Left            =   6780
      TabIndex        =   46
      Top             =   5835
      Width           =   1290
   End
   Begin VB.Label PdaConText 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BOTTOM LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   210
      Index           =   1
      Left            =   1830
      TabIndex        =   45
      Top             =   5835
      Width           =   1290
   End
   Begin VB.Label PdaConLite 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BOTTOM LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   1
      Left            =   1815
      TabIndex        =   44
      Top             =   5820
      Width           =   1290
   End
   Begin VB.Image PdaIcon 
      Height          =   375
      Index           =   0
      Left            =   5640
      MouseIcon       =   "PdaInterface.frx":1A5088
      MousePointer    =   99  'Custom
      Top             =   900
      Width           =   375
   End
   Begin VB.Image PdaButton 
      Height          =   510
      Index           =   6
      Left            =   11985
      MouseIcon       =   "PdaInterface.frx":1A51DA
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A532C
      ToolTipText     =   "Function disabled"
      Top             =   7710
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.Image Image1 
      Height          =   405
      Index           =   1
      Left            =   1320
      MouseIcon       =   "PdaInterface.frx":1A61C6
      Picture         =   "PdaInterface.frx":1A64D0
      Tag             =   "373,27"
      Top             =   7170
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Image Image1 
      Height          =   405
      Index           =   0
      Left            =   870
      MouseIcon       =   "PdaInterface.frx":1A6D82
      Picture         =   "PdaInterface.frx":1A6ED4
      Tag             =   "373,27"
      ToolTipText     =   "Toggle Topmost Position (Actually ON)"
      Top             =   7170
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Label PdaCaption 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Basic Data Check"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   2
      Left            =   11220
      TabIndex        =   7
      Top             =   960
      Width           =   2475
   End
   Begin VB.Label PdaCaption 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Aircraft Change Notification"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   1
      Left            =   1200
      TabIndex        =   6
      Top             =   960
      Width           =   2865
   End
   Begin VB.Label PdaCption 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Confirmation Workflow"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   0
      Left            =   6150
      TabIndex        =   5
      Top             =   960
      Width           =   2895
   End
   Begin VB.Image PdaButton 
      Height          =   345
      Index           =   10
      Left            =   11940
      MouseIcon       =   "PdaInterface.frx":1A7786
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A78D8
      Top             =   8220
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.Image PdaButton 
      Height          =   390
      Index           =   9
      Left            =   11925
      MouseIcon       =   "PdaInterface.frx":1A83E2
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A8534
      Top             =   7305
      Visible         =   0   'False
      Width           =   630
   End
   Begin VB.Image PdaButton 
      Height          =   585
      Index           =   8
      Left            =   12510
      MouseIcon       =   "PdaInterface.frx":1A9276
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1A93C8
      ToolTipText     =   "Function disabled"
      Top             =   7650
      Visible         =   0   'False
      Width           =   405
   End
   Begin VB.Image PdaButton 
      Height          =   585
      Index           =   7
      Left            =   11580
      MouseIcon       =   "PdaInterface.frx":1AA0D6
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1AA228
      ToolTipText     =   "Function disabled"
      Top             =   7680
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Image PdaButton 
      Height          =   480
      Index           =   5
      Left            =   13560
      MouseIcon       =   "PdaInterface.frx":1AAE9A
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1AAFEC
      ToolTipText     =   "Function disabled"
      Top             =   7185
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image PdaButton 
      Height          =   495
      Index           =   4
      Left            =   12855
      MouseIcon       =   "PdaInterface.frx":1ABC2E
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1ABD80
      ToolTipText     =   "Function disabled"
      Top             =   7170
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Image PdaButton 
      Height          =   480
      Index           =   3
      Left            =   11145
      MouseIcon       =   "PdaInterface.frx":1ACAA6
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1ACBF8
      ToolTipText     =   "Function disabled"
      Top             =   7185
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image PdaButton 
      Height          =   465
      Index           =   2
      Left            =   10470
      MouseIcon       =   "PdaInterface.frx":1AD83A
      MousePointer    =   99  'Custom
      Picture         =   "PdaInterface.frx":1AD98C
      ToolTipText     =   "Function disabled"
      Top             =   7185
      Visible         =   0   'False
      Width           =   450
   End
   Begin VB.Label PdaConLite 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BOTTOM LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   0
      Left            =   6765
      TabIndex        =   47
      Top             =   5820
      Width           =   1290
   End
   Begin VB.Label PdaConLite 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BOTTOM LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   2
      Left            =   11775
      TabIndex        =   49
      Top             =   5820
      Width           =   1290
   End
   Begin VB.Label PdaConLite 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOP LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   3
      Left            =   11940
      TabIndex        =   71
      Top             =   510
      Width           =   960
   End
   Begin VB.Label PdaConLite 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOP LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   4
      Left            =   1950
      TabIndex        =   73
      Top             =   510
      Width           =   960
   End
   Begin VB.Label PdaConLite 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOP LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Index           =   5
      Left            =   6855
      MouseIcon       =   "PdaInterface.frx":1AE4F2
      MousePointer    =   99  'Custom
      TabIndex        =   75
      Top             =   510
      Width           =   1110
   End
   Begin VB.Label PdaConText 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOP LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   240
      Index           =   5
      Left            =   6870
      MouseIcon       =   "PdaInterface.frx":1AE7FC
      MousePointer    =   99  'Custom
      TabIndex        =   74
      Top             =   525
      Width           =   1110
   End
   Begin VB.Label PdaConText 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOP LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Index           =   4
      Left            =   1965
      TabIndex        =   72
      Top             =   525
      Width           =   960
   End
   Begin VB.Label PdaConText 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOP LABEL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   225
      Index           =   3
      Left            =   11955
      TabIndex        =   70
      Top             =   525
      Width           =   960
   End
End
Attribute VB_Name = "PdaInterface"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public IsActive As Boolean
Public PdaTestMode As Boolean
Dim InetDefAddr As String
Dim InetDefName As String
Dim InetUrlAddr As String
Dim InetUrlName As String
Dim CurUrlName As String
Dim MySkinPath As String
Dim MySkinName As String
Dim CurTimeDiff As Integer
Dim AutoCopy As Boolean
Dim AutoSearch As Boolean
Dim CurLookUpFldIdx As Integer

Const HTCAPTION = 2
Const WM_NCLBUTTONDOWN = &HA1
Private Declare Function ReleaseCapture Lib "user32" () As Long
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Long) As Long

Private Declare Function CreateRectRgn Lib "gdi32" (ByVal x1 As Long, ByVal y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal x1 As Long, ByVal y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long
Private Declare Function SetWindowRgn Lib "user32" (ByVal hWnd As Long, ByVal hRgn As Long, ByVal bRedraw As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

Private Declare Function GetObject Lib "gdi32" Alias "GetObjectA" (ByVal hObject As Long, ByVal nCount As Long, lpObject As Any) As Long
Private Declare Function GetDIBits Lib "gdi32" (ByVal aHDC As Long, ByVal hBitmap As Long, ByVal nStartScan As Long, ByVal nNumScans As Long, lpBits As Any, lpBI As BITMAPINFO, ByVal wUsage As Long) As Long
Private Type BITMAP '14 bytes
    bmType As Long
    bmWidth As Long
    bmHeight As Long
    bmWidthBytes As Long
    bmPlanes As Integer
    bmBitsPixel As Integer
    bmBits As Long
End Type
Private Type BITMAPINFOHEADER '40 bytes
    biSize As Long
    biWidth As Long
    biHeight As Long
    biPlanes As Integer
    biBitCount As Integer
    biCompression As Long
    biSizeImage As Long
    biXPelsPerMeter As Long
    biYPelsPerMeter As Long
    biClrUsed As Long
    biClrImportant As Long
End Type
Private Type RGBQUAD
    rgbBlue As Byte
    rgbGreen As Byte
    rgbRed As Byte
    rgbReserved As Byte
End Type
Private Type BITMAPINFO
    bmiHeader As BITMAPINFOHEADER
    bmiColors As RGBQUAD
End Type
Private Const DIB_RGB_COLORS = 0&
Private Const BI_RGB = 0&

Private Const pixR As Integer = 3
Private Const pixG As Integer = 2
Private Const pixB As Integer = 1

Dim MbMov As Boolean
Dim PrevX As Long, PrevY As Long

Public Sub ExplainDetails(UseTab As TABLib.Tab, LineNo As Long)
    'Currently HardCoded
    Dim PdeUrno As String
    Dim AftUrno As String
    Dim tmpHit As String
    Dim tmpRec As String
    Dim HitLine As Long
    Dim ColNo As Long
    Dim iOff As Integer
    AftUrno = Trim(UseTab.GetFieldValue(LineNo, "FLNU"))
    PdaTab(1).ResetContent
    PdaTab(1).DateTimeSetUTCOffsetMinutes 1, UtcTimeDisp
    PdaTab(1).DateTimeSetUTCOffsetMinutes 7, UtcTimeDisp
    ColNo = GetRealItemNo(UseTab.LogicalFieldList, "FLNU")
    tmpHit = UseTab.GetLinesByColumnValue(ColNo, AftUrno, 0)
    While tmpHit <> ""
        HitLine = Val(tmpHit)
        tmpRec = UseTab.GetLineValues(HitLine)
        PdaTab(1).InsertTextLine tmpRec, False
        iOff = InStr(tmpHit, ",")
        If iOff > 0 Then
            tmpHit = Mid(tmpHit, iOff + 1)
        Else
            tmpHit = ""
        End If
    Wend
    PdaTab(1).Sort "1", False, True
    PdaTab(1).AutoSizeColumns
    PdeUrno = Trim(UseTab.GetFieldValue(LineNo, "URNO"))
    ColNo = GetRealItemNo(PdaTab(1).LogicalFieldList, "URNO")
    tmpHit = PdaTab(1).GetLinesByColumnValue(ColNo, PdeUrno, 0)
    If tmpHit <> "" Then
        HitLine = Val(tmpHit)
        PdaTab(1).SetCurrentSelection HitLine
    Else
        PdaTab(2).ResetContent
        PdaTab(2).Refresh
        DoEvents
        AdjustPanelData -1
    End If
End Sub

Public Sub UpdateAcrtabBasicData(BcCmd As String, AcrUrno As String, AcrFields As String, AcrData As String)
    Dim tmpHit As String
    Dim tmpNew As String
    Dim ColNo As Long
    Dim SelLine As Long
    Dim LineNo As Long
    ColNo = CLng(GetRealItemNo(PdaTab(0).LogicalFieldList, "URNO"))
    tmpHit = PdaTab(0).GetLinesByColumnValue(ColNo, AcrUrno, 0)
    If tmpHit <> "" Then
        LineNo = Val(tmpHit)
        If BcCmd <> "DRT" Then
            PdaTab(0).SetFieldValues LineNo, AcrFields, AcrData
        Else
            PdaTab(0).DeleteLine LineNo
        End If
    Else
        tmpNew = CreateEmptyLine(PdaTab(0).LogicalFieldList)
        PdaTab(0).InsertTextLineAt 0, tmpNew, False
        PdaTab(0).SetFieldValues 0, AcrFields, AcrData
    End If
    PdaTab(0).AutoSizeColumns
    PdaTab(0).Refresh
    DoEvents
End Sub
Public Sub UpdateActtabBasicData(BcCmd As String, AcrUrno As String, AcrFields As String, AcrData As String)
    Dim tmpHit As String
    Dim tmpNew As String
    Dim ColNo As Long
    Dim LineNo As Long
    ColNo = CLng(GetRealItemNo(PdaTab(4).LogicalFieldList, "URNO"))
    tmpHit = PdaTab(4).GetLinesByColumnValue(ColNo, AcrUrno, 0)
    If tmpHit <> "" Then
        LineNo = Val(tmpHit)
        If BcCmd <> "DRT" Then
            PdaTab(4).SetFieldValues LineNo, AcrFields, AcrData
            If PdaTab(4).GetCurrentSelected = LineNo Then
                PdaTab(4).SetCurrentSelection LineNo
            End If
        Else
            PdaTab(4).DeleteLine LineNo
        End If
    Else
        tmpNew = CreateEmptyLine(PdaTab(4).LogicalFieldList)
        PdaTab(4).InsertTextLineAt 0, tmpNew, False
        PdaTab(4).SetFieldValues 0, AcrFields, AcrData
    End If
    PdaTab(4).AutoSizeColumns
    PdaTab(4).Refresh
    DoEvents
End Sub
Public Sub UpdateAfttabFlight(BcSel As String, BcFld As String, BcDat As String)
    Dim AftUrno As String
    AftUrno = GetUrnoFromBcEvent(BcFld, BcDat, BcSel)
    If (AftUrno <> "") And (chkFunc(4).Tag = AftUrno) Then
        If InStr(BcFld, "REGN") > 0 Then
            txtFunc(28).Text = GetFieldValue("REGN", BcDat, BcFld)
        End If
        If InStr(BcFld, "ACT3") > 0 Then
            txtFunc(31).Text = GetFieldValue("ACT3", BcDat, BcFld)
        End If
        If InStr(BcFld, "ACT5") > 0 Then
            txtFunc(30).Text = GetFieldValue("ACT5", BcDat, BcFld)
        End If
        If InStr(BcFld, "ACTI") > 0 Then
            txtFunc(29).Text = GetFieldValue("ACTI", BcDat, BcFld)
        End If
    End If
End Sub
Public Sub ToggleTimeValues(UseTimeDiff As Integer)
    CurTimeDiff = UseTimeDiff
    PdaTab(1).DateTimeSetUTCOffsetMinutes 1, CurTimeDiff
    PdaTab(1).DateTimeSetUTCOffsetMinutes 7, CurTimeDiff
    PdaTab(1).SetCurrentSelection PdaTab(1).GetCurrentSelected
End Sub
Private Sub UnRGB(ByRef color As Long, ByRef r As Byte, ByRef g As Byte, ByRef b As Byte)
    r = color And &HFF&
    g = (color And &HFF00&) \ &H100&
    b = (color And &HFF0000) \ &H10000
End Sub


'Fix the form to the pixels that don't match the color.
Private Sub ShapeForm(ByVal pic As PictureBox, ByVal transparent_color As Long, CaptionType As Integer)
Const RGN_OR = 2
Dim bytes_per_scanLine As Integer
Dim wid As Long
Dim hgt As Long
Dim bitmap_info As BITMAPINFO
Dim pixels() As Byte
Dim buffer() As Byte
Dim transparent_r As Byte
Dim transparent_g As Byte
Dim transparent_b As Byte
Dim border_width As Single
Dim title_height As Single
Dim x0 As Long
Dim Y0 As Long
Dim start_c As Integer
Dim stop_c As Integer
Dim r As Integer
Dim c As Integer
Dim combined_rgn As Long
Dim form_rgn As Long
Dim new_rgn As Long
Dim iPic As Integer
Dim NewSize As Long
Dim BarHeight As Long

'Combine the outer shape of a group
'of overlapping pictures
'For iPic = 2 To 1 Step -1
'pic.Picture = Image1(iPic).Picture

    
BarHeight = (Me.Height / 15) - Me.ScaleHeight
    
    ScaleMode = vbPixels
    pic.ScaleMode = vbPixels
    pic.AutoRedraw = True
    pic.Picture = pic.Image

    ' Prepare the bitmap description.
    wid = pic.ScaleWidth
    hgt = pic.ScaleHeight
    With bitmap_info.bmiHeader
        .biSize = 40
        .biWidth = wid
        ' Use negative height to scan top-down.
        .biHeight = -hgt
        .biPlanes = 1
        .biBitCount = 32
        .biCompression = BI_RGB
        bytes_per_scanLine = ((((.biWidth * .biBitCount) + 31) \ 32) * 4)
        .biSizeImage = bytes_per_scanLine * hgt
    End With

    ' Load the bitmap's data.
    ReDim pixels(1 To 4, 1 To wid, 1 To hgt)
    GetDIBits pic.hDC, pic.Image, _
        0, hgt, pixels(1, 1, 1), _
        bitmap_info, DIB_RGB_COLORS

    ' Process the pixels.
    ' Break the tansparent color apart.
    UnRGB transparent_color, transparent_r, transparent_g, transparent_b

    ' Find the form's corner.
    border_width = (ScaleX(Width, vbTwips, vbPixels) - ScaleWidth) / 2
    title_height = ScaleX(Height, vbTwips, vbPixels) - border_width - ScaleHeight

    ' Find the picture's corner.
    x0 = pic.Left + border_width
    Y0 = pic.Top + title_height - 1
    'combined_rgn = 0

    ' Create the form's regions.
    For r = 1 To hgt
        ' Create a region for this row.
        c = 1
        Do While c <= wid
            start_c = 1
            stop_c = 1

            ' Find the next non-white column.
            Do While c <= wid
                If pixels(pixR, c, r) <> transparent_r Or _
                   pixels(pixG, c, r) <> transparent_g Or _
                   pixels(pixB, c, r) <> transparent_b _
                Then
                    Exit Do
                End If
                c = c + 1
            Loop
            start_c = c

            ' Find the next white column.
            Do While c <= wid
                If pixels(pixR, c, r) = transparent_r And _
                   pixels(pixG, c, r) = transparent_g And _
                   pixels(pixB, c, r) = transparent_b _
                Then
                    Exit Do
                End If
                c = c + 1
            Loop
            stop_c = c - 1

            ' Make a region from start_c to stop_c.
            If start_c <= wid Then
                If stop_c > wid Then stop_c = wid

                ' Create the region.
                new_rgn = CreateRectRgn( _
                    start_c + x0, r + Y0, _
                    stop_c + x0, r + Y0 + 1)

                ' Add it to what we have so far.
                If combined_rgn = 0 Then
                    combined_rgn = new_rgn
                Else
                    CombineRgn combined_rgn, _
                        combined_rgn, new_rgn, RGN_OR
                    DeleteObject new_rgn
                End If
            End If
        Loop
    Next r

    If form_rgn = 0 Then
        form_rgn = combined_rgn
    Else
        CombineRgn form_rgn, form_rgn, combined_rgn, RGN_OR
    End If
    
'Next
DeleteObject new_rgn
    
'Now add controls to the visible shape of the form
'Control dimensions in pixel!
'X,Y of upper left corner (0,0)
'X,Y of lower right corner (50,90)
NewSize = pic.Width
NewSize = NewSize + ((Me.Width \ 15) - Me.ScaleWidth)

    Select Case CaptionType
        Case 0
            new_rgn = CreateRectRgn(0, 0, NewSize, BarHeight)
            CombineRgn form_rgn, form_rgn, new_rgn, RGN_OR
            DeleteObject new_rgn
        Case 1
            new_rgn = CreateRectRgn(0, 0, 85, BarHeight - 3)
            CombineRgn form_rgn, form_rgn, new_rgn, RGN_OR
            DeleteObject new_rgn
            
            new_rgn = CreateRectRgn(NewSize - 58, 0, NewSize, BarHeight - 3)
            CombineRgn form_rgn, form_rgn, new_rgn, RGN_OR
            DeleteObject new_rgn
            
            new_rgn = CreateEllipticRgn(59, 3, 99, BarHeight - 3)
            CombineRgn form_rgn, form_rgn, new_rgn, RGN_OR
            DeleteObject new_rgn
            
            new_rgn = CreateEllipticRgn(95, 3, 95 + BarHeight - 6, BarHeight - 3)
            CombineRgn form_rgn, form_rgn, new_rgn, RGN_OR
            DeleteObject new_rgn
            
            new_rgn = CreateEllipticRgn(NewSize - 73, 3, NewSize - 33, BarHeight - 3)
            CombineRgn form_rgn, form_rgn, new_rgn, RGN_OR
            DeleteObject new_rgn
            
            new_rgn = CreateEllipticRgn(NewSize - 95, 3, NewSize - 95 + BarHeight - 6, BarHeight - 3)
            CombineRgn form_rgn, form_rgn, new_rgn, RGN_OR
            DeleteObject new_rgn
        Case Else
    End Select
    
' Restrict the form to the region.
SetWindowRgn hWnd, form_rgn, True
DeleteObject form_rgn
DeleteObject combined_rgn
    
    pic.AutoRedraw = False
    pic.Cls
    
Me.Picture = pic.Picture
'pic.Visible = False
End Sub

Private Sub chkFunc_Click(Index As Integer)
    Dim ErrMsg As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpSqlKey As String
    Dim tmpAddFval As String
    Dim tmpAddRfld As String
    Dim AddCaseFld As String
    Dim AddCaseDat As String
    Dim tmpData As String
    Dim tmpFields As String
    Dim tmpUser As String
    Dim UpdUser As String
    Dim tmpTag As String
    Dim LineNo As Long
    Dim CaseValue As Integer
    Dim i As Integer
    If chkFunc(Index).Value = 1 Then
        tmpUser = ApplMainCode
        chkFunc(Index).BackColor = LightGreen
        chkFunc(Index).Refresh
        DoEvents
        Select Case Index
            Case 0  'Open A/C Type Basic Data
                WebBrowser.Visible = False
                PdaTab(3).Visible = False
                PdaFrame(6).Visible = False
                PdaFrame(9).Visible = False
                PdaTab(4).Visible = True
                PdaFrame(7).Visible = True
                chkFunc(2).Value = 0
                PdaConText(3).Caption = "Aircraft Types"
                PdaConLite(3).Caption = PdaConText(3).Caption
                If PdaTab(4).GetLineCount <= 0 Then chkFunc(9).Value = 1
                AdjustPanelIcon PdaTab(4), PdaPanel(2).Top, 4
            Case 1  'Confirm New A/C Type
                LineNo = PdaTab(1).GetCurrentSelected
                If LineNo >= 0 Then
                    tmpDatLst = ""
                    tmpDatLst = tmpDatLst & txtFunc(1).Text & ","
                    tmpDatLst = tmpDatLst & txtFunc(2).Text & ","
                    tmpDatLst = tmpDatLst & txtFunc(3).Text
                    tmpFldLst = "ACT3,ACT5,ACTI"
                    
                    AddCaseFld = "CASE;CCSE"
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "CASE")
                    UpdUser = tmpUser & "_" & tmpData
                    AddCaseDat = tmpData & ";"
                    CaseValue = Val(tmpData)
                    CaseValue = (CaseValue Or 1024)
                    tmpData = CStr(CaseValue)
                    AddCaseDat = AddCaseDat & tmpData
                    
                    tmpAddRfld = AddCaseFld & "|"
                    tmpAddFval = AddCaseDat & "|"
                    
                    tmpFields = PdaTab(1).GetFieldValue(LineNo, "PDXF")
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "PDXD")
                    tmpAddRfld = tmpAddRfld & tmpFields & "|"
                    tmpAddFval = tmpAddFval & tmpData & "|"
                    
                    tmpFields = PdaTab(1).GetFieldValue(LineNo, "PDFF")
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "PDFD")
                    tmpAddRfld = tmpAddRfld & tmpFields & "|"
                    tmpAddFval = tmpAddFval & tmpData & "|"
                    
                    tmpFields = Replace(tmpFldLst, ",", ";", 1, -1, vbBinaryCompare)
                    tmpData = Replace(tmpDatLst, ",", ";", 1, -1, vbBinaryCompare)
                    tmpAddRfld = tmpAddRfld & tmpFields & "|"
                    tmpAddFval = tmpAddFval & tmpData & "|"
                    
                    tmpFldLst = tmpFldLst & ",RFLD,FVAL"
                    tmpDatLst = tmpDatLst & "," & tmpAddRfld
                    tmpDatLst = tmpDatLst & "," & tmpAddFval
                    'ErrMsg = MainDialog.InsertActtab("INS", PdaFrame(0).Tag, chkFunc(1).Tag, "", tmpFldLst, tmpDatLst)
                    ErrMsg = MainDialog.InsertActtab("INS", "UPD", chkFunc(1).Tag, "", tmpFldLst, tmpDatLst, UpdUser)
                    If ErrMsg <> "" Then
                        If (MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", ErrMsg, "write", "OK", UserAnswer) = 1) Then DoNothing
                    End If
                    chkFunc(Index).Refresh
                    DoEvents
                End If
                chkFunc(Index).Value = 0
            Case 2  'Open A/C REGN Basic Data
                WebBrowser.Visible = False
                PdaTab(3).Visible = False
                PdaFrame(6).Visible = False
                PdaFrame(9).Visible = False
                PdaTab(0).Visible = True
                PdaFrame(5).Visible = True
                chkFunc(0).Value = 0
                PdaConText(3).Caption = "Aircraft Registrations"
                PdaConLite(3).Caption = PdaConText(3).Caption
                If PdaTab(0).GetLineCount <= 0 Then chkFunc(7).Value = 1
                AdjustPanelIcon PdaTab(0), PdaPanel(2).Top, 4
            Case 3  'Confirm New A/C Registration
                LineNo = PdaTab(1).GetCurrentSelected
                If LineNo >= 0 Then
                    tmpDatLst = ""
                    tmpDatLst = tmpDatLst & txtFunc(7).Text & ","
                    tmpDatLst = tmpDatLst & txtFunc(6).Text & ","
                    tmpDatLst = tmpDatLst & txtFunc(5).Text & ","
                    tmpDatLst = tmpDatLst & txtFunc(4).Text
                    tmpFldLst = "ACT3,ACT5,ACTI,REGN"
                    
                    AddCaseFld = "CASE;CCSE"
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "CASE")
                    UpdUser = tmpUser & "_" & tmpData
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "CCSE")
                    If tmpData = "" Then tmpData = PdaTab(1).GetFieldValue(LineNo, "CASE")
                    AddCaseDat = tmpData & ";"
                    CaseValue = Val(tmpData)
                    CaseValue = (CaseValue Or 2048)
                    tmpData = CStr(CaseValue)
                    AddCaseDat = AddCaseDat & tmpData
                    
                    tmpAddRfld = AddCaseFld & "|"
                    tmpAddFval = AddCaseDat & "|"
                    
                    tmpFields = PdaTab(1).GetFieldValue(LineNo, "PDXF")
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "PDXD")
                    tmpAddRfld = tmpAddRfld & tmpFields & "|"
                    tmpAddFval = tmpAddFval & tmpData & "|"
                    
                    tmpFields = PdaTab(1).GetFieldValue(LineNo, "PDFF")
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "PDFD")
                    tmpAddRfld = tmpAddRfld & tmpFields & "|"
                    tmpAddFval = tmpAddFval & tmpData & "|"
                    
                    tmpFields = Replace(tmpFldLst, ",", ";", 1, -1, vbBinaryCompare)
                    tmpData = Replace(tmpDatLst, ",", ";", 1, -1, vbBinaryCompare)
                    tmpAddRfld = tmpAddRfld & tmpFields & "|"
                    tmpAddFval = tmpAddFval & tmpData & "|"
                    
                    tmpFldLst = tmpFldLst & ",RFLD,FVAL"
                    tmpDatLst = tmpDatLst & "," & tmpAddRfld
                    tmpDatLst = tmpDatLst & "," & tmpAddFval
                
                    'ErrMsg = MainDialog.InsertAcrtab("INS", PdaFrame(1).Tag, chkFunc(3).Tag, "", tmpFldLst, tmpDatLst)
                    ErrMsg = MainDialog.InsertAcrtab("INS", "UPD", chkFunc(3).Tag, "", tmpFldLst, tmpDatLst)
                    If ErrMsg <> "" Then
                        If (MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", ErrMsg, "write", "OK", UserAnswer) = 1) Then DoNothing
                    End If
                    chkFunc(Index).Refresh
                    DoEvents
                End If
                chkFunc(Index).Value = 0
            Case 4  'Load Flight Data
                tmpFldLst = "REGN,ACT3,ACT5,ACTI"
                tmpDatLst = ""
                tmpSqlKey = "WHERE URNO=" & chkFunc(4).Tag
                UfisServer.CallCeda tmpDatLst, "RTA", "AFTTAB", tmpFldLst, "", tmpSqlKey, "", 0, True, False
                txtFunc(28).Text = GetItem(tmpDatLst, 1, ",")
                txtFunc(31).Text = GetItem(tmpDatLst, 2, ",")
                txtFunc(30).Text = GetItem(tmpDatLst, 3, ",")
                txtFunc(29).Text = GetItem(tmpDatLst, 4, ",")
                chkFunc(Index).Refresh
                DoEvents
                chkFunc(Index).Value = 0
            Case 5  'Confirm Flight Update
                LineNo = PdaTab(1).GetCurrentSelected
                If LineNo >= 0 Then
                    tmpData = PdaTab(1).GetFieldValue(LineNo, "CASE")
                    UpdUser = tmpUser & "_" & tmpData
                    tmpFldLst = "ACT3,ACT5,ACTI,REGN"
                    tmpDatLst = ""
                    tmpDatLst = tmpDatLst & txtFunc(16).Text & ","
                    tmpDatLst = tmpDatLst & txtFunc(17).Text & ","
                    tmpDatLst = tmpDatLst & txtFunc(18).Text & ","
                    If chkFunc(14).Value = 1 Then
                        tmpDatLst = tmpDatLst & "@"
                    ElseIf txtFunc(19).Text <> "" Then
                        tmpDatLst = tmpDatLst & txtFunc(19).Text
                    Else
                        tmpDatLst = tmpDatLst & txtFunc(19).Text
                    End If
                    'If PdaFrame(2).Tag = "ACK" Then
                    '    ErrMsg = MainDialog.InsertActtab("DEL", "---", chkFunc(5).Tag, chkFunc(4).Tag, tmpFldLst, tmpDatLst)
                    'End If
                    ErrMsg = MainDialog.InsertActtab("UFR", "---", chkFunc(5).Tag, chkFunc(4).Tag, tmpFldLst, tmpDatLst, UpdUser)
                    If ErrMsg <> "" Then
                        If (MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", ErrMsg, "write", "OK", UserAnswer) = 1) Then DoNothing
                        chkFunc(Index).Refresh
                        DoEvents
                    Else
                        chkFunc(Index).Caption = "Done"
                    End If
                End If
                chkFunc(Index).Value = 0
            Case 6  'Update ACRTAB from Basic Data Grid
                tmpFldLst = "ACT3,ACT5,ACTI,REGN"
                tmpDatLst = ""
                tmpDatLst = tmpDatLst & txtFunc(22).Text & ","
                tmpDatLst = tmpDatLst & txtFunc(21).Text & ","
                tmpDatLst = tmpDatLst & txtFunc(20).Text & ","
                tmpDatLst = tmpDatLst & txtFunc(0).Text
                ErrMsg = MainDialog.InsertAcrtab("UPD", "---", "", chkFunc(6).Tag, tmpFldLst, tmpDatLst)
                If ErrMsg <> "" Then
                    If (MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", ErrMsg, "write", "OK", UserAnswer) = 1) Then DoNothing
                End If
                chkFunc(Index).Refresh
                DoEvents
                chkFunc(Index).Value = 0
            Case 7  'Load A/C REGN Basic Data
                txtFunc(0).Text = ""
                txtFunc(20).Text = ""
                txtFunc(21).Text = ""
                txtFunc(22).Text = ""
                MainDialog.LoadAcrtab PdaTab(0)
                chkFunc(Index).Value = 0
            Case 8  'Call FIPS
                MainDialog.CallFipsFlight PdaTab(1), -1, chkFunc(4).Tag
                chkFunc(Index).Value = 0
            Case 9  'Load A/C Type Basic Data
                txtFunc(23).Text = ""
                txtFunc(24).Text = ""
                txtFunc(25).Text = ""
                txtFunc(26).Text = ""
                MainDialog.LoadActtab PdaTab(4)
                chkFunc(Index).Value = 0
            Case 10 'Update ACTTAB from Basic Data Grid
                tmpDatLst = ""
                tmpDatLst = tmpDatLst & txtFunc(23).Text & ","
                tmpDatLst = tmpDatLst & txtFunc(24).Text & ","
                tmpDatLst = tmpDatLst & txtFunc(25).Text & ","
                tmpDatLst = tmpDatLst & txtFunc(26).Text
                tmpFldLst = "ACT3,ACT5,ACTI,ACFN"
                ErrMsg = MainDialog.InsertActtab("UPD", "---", "", chkFunc(10).Tag, tmpFldLst, tmpDatLst, "")
                If ErrMsg <> "" Then
                    If (MyMsgBox.CallAskUser(0, 0, 0, "Transaction Control", ErrMsg, "write", "OK", UserAnswer) = 1) Then DoNothing
                End If
                chkFunc(Index).Refresh
                DoEvents
                chkFunc(Index).Value = 0
            Case 11
                For i = 11 To 17
                    If i <> Index Then chkFunc(i).Value = 0
                Next
                txtFunc(19).Text = txtFunc(8).Text
                txtFunc(16).Text = txtFunc(11).Text
                txtFunc(17).Text = txtFunc(10).Text
                txtFunc(18).Text = txtFunc(9).Text
                chkFunc(Index).Value = 0
            Case 12
                For i = 11 To 17
                    If i <> Index Then chkFunc(i).Value = 0
                Next
                txtFunc(19).Text = txtFunc(15).Text
                txtFunc(16).Text = txtFunc(12).Text
                txtFunc(17).Text = txtFunc(13).Text
                txtFunc(18).Text = txtFunc(14).Text
                chkFunc(Index).Value = 0
            Case 13
                For i = 11 To 17
                    If i <> Index Then chkFunc(i).Value = 0
                Next
                chkFunc(4).Value = 1
                txtFunc(19).Text = txtFunc(28).Text
                txtFunc(16).Text = txtFunc(31).Text
                txtFunc(17).Text = txtFunc(30).Text
                txtFunc(18).Text = txtFunc(29).Text
                chkFunc(Index).Value = 0
            Case 14
                For i = 11 To 17
                    If i <> Index Then chkFunc(i).Value = 0
                Next
                txtFunc(19).Locked = True
                txtFunc(19).Tag = txtFunc(19).Text
                txtFunc(19).Text = "----"
                PdaFrame(11).BackColor = LightestGreen
            Case 15
                For i = 11 To 17
                    If i <> Index Then chkFunc(i).Value = 0
                Next
                'txtFunc(19).Text = ""
                txtFunc(16).Text = txtFunc(1).Text
                txtFunc(17).Text = txtFunc(2).Text
                txtFunc(18).Text = txtFunc(3).Text
                If txtFunc(4).Text <> "" Then
                    txtFunc(7).Text = txtFunc(1).Text
                    txtFunc(6).Text = txtFunc(2).Text
                    txtFunc(5).Text = txtFunc(3).Text
                End If
                chkFunc(Index).Value = 0
            Case 16
                For i = 11 To 17
                    If i <> Index Then chkFunc(i).Value = 0
                Next
                txtFunc(19).Text = txtFunc(4).Text
                txtFunc(16).Text = ""
                txtFunc(17).Text = ""
                txtFunc(18).Text = ""
                chkFunc(Index).Value = 0
            Case 17
                For i = 11 To 17
                    If i <> Index Then chkFunc(i).Value = 0
                Next
                'txtFunc(19).Text = ""
                txtFunc(16).Text = txtFunc(7).Text
                txtFunc(17).Text = txtFunc(6).Text
                txtFunc(18).Text = txtFunc(5).Text
                chkFunc(Index).Value = 0
            Case 18 'Ignore
                DiscardMessage
                chkFunc(Index).Value = 0
            Case 19 'Allow
                PdaPanel(3).Visible = False
                lblAny(11).Visible = False
                PdaPanel(0).Enabled = True
                chkFunc(Index).Value = 0
                chkFunc(5).BackColor = LightGreen
                tmpTag = chkFunc(19).Tag
                chkFunc(0).Value = Val(GetItem(tmpTag, 1, ","))
                chkFunc(2).Value = Val(GetItem(tmpTag, 2, ","))
                If (chkFunc(0).Value = 0) And (chkFunc(2).Value = 0) Then
                    PdaConText(3).Caption = CurUrlName
                    PdaConLite(3).Caption = PdaConText(3).Caption
                    PdaIcon(4).Visible = False
                    WebBrowser.Visible = True
                    PdaFrame(9).Visible = True
                End If
            Case 20 'Deny
                DiscardMessage
                chkFunc(Index).Value = 0
                
            Case Else
                chkFunc(Index).Value = 0
        End Select
    ElseIf chkFunc(Index).Value = 0 Then
        chkFunc(Index).BackColor = LightGrey
        chkFunc(Index).Refresh
        DoEvents
        Select Case Index
            Case 0
                PdaTab(4).Visible = False
                PdaFrame(7).Visible = False
                If (PdaPanel(3).Visible = False) And (chkFunc(2).Value = 0) Then
                    PdaConText(3).Caption = CurUrlName
                    PdaConLite(3).Caption = PdaConText(3).Caption
                    'PdaTab(3).Visible = True
                    'PdaFrame(6).Visible = True
                    PdaIcon(4).Visible = False
                    WebBrowser.Visible = True
                    PdaFrame(9).Visible = True
                End If
            Case 2
                PdaTab(0).Visible = False
                PdaFrame(5).Visible = False
                If (PdaPanel(3).Visible = False) And (chkFunc(0).Value = 0) Then
                    PdaConText(3).Caption = CurUrlName
                    PdaConLite(3).Caption = PdaConText(3).Caption
                    'PdaTab(3).Visible = True
                    'PdaFrame(6).Visible = True
                    PdaIcon(4).Visible = False
                    WebBrowser.Visible = True
                    PdaFrame(9).Visible = True
                End If
            Case 14
                txtFunc(19).Text = txtFunc(19).Tag
                txtFunc(19).Locked = False
                PdaFrame(11).BackColor = LightGrey
            Case Else
        End Select
        DoEvents
    End If
End Sub

Private Sub Command1_Click()
    Dim i As Integer
    Dim IsVis As Boolean
    IsVis = Not PdaButton(1).Visible
    For i = 0 To PdaButton.UBound
        PdaButton(i).Visible = IsVis
    Next
    'ProgressBar.Max = 100000
    'ProgressBar.Value = 100000
End Sub

Private Sub Command2_Click()
    If TextShape.Visible = True Then
        Unload TextShape
    Else
        TextShape.Show
    End If
End Sub

Private Sub Command3_Click()
    Dim SkinFile As String
    Dim NewSkin As String
    Dim CaptStyle As Integer
    On Error GoTo ErrExit
    Select Case MySkinName
        Case "PdaSkinPDA"
            NewSkin = "PdaSkinPPC"
            CaptStyle = 0
        Case "PdaSkinPPC"
            NewSkin = "PdaSkinPDA"
            CaptStyle = 1
        Case Else
            NewSkin = ""
    End Select
    If NewSkin <> "" Then
        CaptStyle = 2   'Don't show caption bar
        SkinFile = MySkinPath & "\" & NewSkin & ".bmp"
        picShape(0).Picture = LoadPicture(SkinFile)
        MySkinName = NewSkin
        picShape(0).Left = 0
        picShape(0).Top = 0
        ShapeForm picShape(0), vbYellow, CaptStyle
        picShape(0).Picture = LoadPicture("")
        picShape(0).Visible = False
        ArrangePdaSkinButtons
    End If
ErrExit:
    Exit Sub
End Sub

Private Sub Form_Activate()
    Dim i As Integer
    If Not IsActive Then
        CloneGridLayout PdaTab(0), MainDialog.FuncTab(1)
        CloneGridLayout PdaTab(1), MainDialog.FuncTab(3)
        CloneGridLayout PdaTab(4), MainDialog.FuncTab(5)
        PdaTab(1).OnHScrollTo 3
        PdaTab(2).ResetContent
        PdaTab(2).LogicalFieldList = "CASE,MEAN,REMA"
        PdaTab(2).HeaderString = "Case,Meaning,Remark" & Space(1000)
        PdaTab(2).HeaderLengthString = "10,10,10"
        PdaTab(2).ColumnAlignmentString = "C,L,L"
        PdaTab(2).FontName = "Arial"
        PdaTab(2).FontSize = MainDialog.FontSlider.Value + 6
        PdaTab(2).HeaderFontSize = MainDialog.FontSlider.Value + 6
        PdaTab(2).LineHeight = MainDialog.FontSlider.Value + 8
        PdaTab(2).SetTabFontBold True
        PdaTab(2).LifeStyle = True
        PdaTab(2).CursorLifeStyle = True
        PdaTab(2).ShowVertScroller True
        PdaTab(2).ShowHorzScroller True
        PdaTab(2).AutoSizeByHeader = True
        PdaTab(2).AutoSizeColumns
        PdaTab(2).OnHScrollTo 1
        PdaTab(2).Refresh
        PdaTab(3).ResetContent
        PdaTab(3).LogicalFieldList = "BKEY,MEAN,REMA"
        PdaTab(3).HeaderString = "Key,Values,Remark" & Space(1000)
        PdaTab(3).HeaderLengthString = "10,10,10"
        PdaTab(3).ColumnAlignmentString = "C,L,L"
        PdaTab(3).FontName = "Arial"
        PdaTab(3).FontSize = MainDialog.FontSlider.Value + 6
        PdaTab(3).HeaderFontSize = MainDialog.FontSlider.Value + 6
        PdaTab(3).LineHeight = MainDialog.FontSlider.Value + 8
        PdaTab(3).SetTabFontBold True
        PdaTab(3).LifeStyle = True
        PdaTab(3).CursorLifeStyle = True
        PdaTab(3).ShowVertScroller True
        PdaTab(3).ShowHorzScroller True
        PdaTab(3).AutoSizeByHeader = True
        PdaTab(3).AutoSizeColumns
        PdaTab(3).Refresh
        'For i = 0 To PdaConText.UBound
        '    PdaConLite(i).Caption = ""
        '    PdaConText(i).Caption = ""
        'Next
        PdaFrame(9).Visible = True
        PdaFrame(9).Refresh
        IsActive = True
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim tmpTag As String
    Dim tmpText As String
    Dim ColNo As Long
    Select Case KeyCode
        Case vbKeyF3
            If chkFunc(2).Value = 1 Then
                tmpTag = chkFunc(2).Tag
                If tmpTag <> "" Then
                    ColNo = Val(GetItem(tmpTag, 1, ","))
                    tmpText = GetItem(tmpTag, 2, ",")
                    AutoSearch = True
                    SearchInTabList PdaTab(0), ColNo, tmpText, False, "", ""
                    AutoSearch = False
                    PdaTab(0).SetCurrentSelection PdaTab(0).GetCurrentSelected
                End If
            End If
            If chkFunc(0).Value = 1 Then
                tmpTag = chkFunc(0).Tag
                If tmpTag <> "" Then
                    ColNo = Val(GetItem(tmpTag, 1, ","))
                    tmpText = GetItem(tmpTag, 2, ",")
                    AutoSearch = True
                    SearchInTabList PdaTab(4), ColNo, tmpText, False, "", ""
                    AutoSearch = False
                    PdaTab(4).SetCurrentSelection PdaTab(4).GetCurrentSelected
                End If
            End If
        Case Else
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Dim tmpTag As String
    Dim tmpText As String
    Dim ColNo As Long
    Select Case KeyAscii
        Case vbKeyEscape
            MainDialog.chkFunc(24).Value = 0
        Case Else
    End Select
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim ReturnVal As Long
    If Button = 1 Then
        X = ReleaseCapture()
        ReturnVal = SendMessage(hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0)
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Select Case UnloadMode
        Case 0
            PdaButton_Click 0
            Cancel = True
        Case Else
            Unload Me
    End Select
End Sub

Private Sub InetTimer_Timer()
    Dim tmpTag As String
    InetTimer.Enabled = False
    tmpTag = InetTimer.Tag
    InetTimer.Tag = ""
    Select Case tmpTag
        Case "OFF", "INI"
            WebBrowser.Navigate InetDefAddr
        Case Else
    End Select
End Sub

Private Sub lblAny_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 6
            PdaFrame(11).Tag = CStr(chkFunc(14).Value)
            lblAny(Index).Tag = PdaFrame(11).Tag
            chkFunc(14).Value = 1
        Case Else
    End Select
End Sub

Private Sub lblAny_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 6
            If Val(PdaFrame(11).Tag) = 1 Then
                chkFunc(14).Value = 0
            Else
                chkFunc(14).Value = 1
            End If
        Case Else
    End Select
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then
        SetFormOnTop Me, True
    Else
        SetFormOnTop Me, False
    End If
    ArrangePdaSkinButtons
End Sub

Private Sub PdaButton_Click(Index As Integer)
    Dim tmpTag As String
    Dim LineNo As Long
    Dim SelLine As Long
    Select Case Index
        Case 0
            Me.Top = Screen.Height + 180
            MainDialog.chkFunc(24).Value = 0
        Case 1
            If OnTop.Value = 0 Then OnTop.Value = 1 Else OnTop.Value = 0
        Case 9
            LineNo = PdaTab(1).GetCurrentSelected - 1
            If LineNo >= 0 Then PdaTab(1).SetCurrentSelection LineNo
        Case 10
            LineNo = PdaTab(1).GetCurrentSelected + 1
            If LineNo < PdaTab(1).GetLineCount Then PdaTab(1).SetCurrentSelection LineNo
        Case Else
    End Select
End Sub

Private Sub Form_Load()
    Dim tmpData As String
    Dim i As Integer
    On Error Resume Next
    IsActive = False
    KeyPreview = True
    tmpData = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "PDA_TEST_MODE", "NO")
    If tmpData = "YES" Then PdaTestMode = True Else PdaTestMode = False
    picShape(0).Picture = Me.Picture
    tmpData = UFIS_SYSTEM & "\PdaCfg\Skins"
    MySkinPath = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "PDA_SKIN_PATH", tmpData)
    tmpData = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "PDA_SKIN_NAME", "PdaSkinPDA.bmp")
    If InStr(tmpData, ".") = 0 Then tmpData = tmpData & ".bmp"
    MySkinName = GetItem(tmpData, 1, ".")
    tmpData = MySkinPath & "\" & tmpData
    
    Err.Clear
    picShape(0).Picture = LoadPicture(tmpData)
    If Err.Number <> 0 Then MySkinName = "PdaSkinPDA"
    
    
    picShape(0).Left = 0
    picShape(0).Top = 0
    Me.Width = picShape(0).Width * 15 + 60
    Me.Height = picShape(0).Height * 15 + 450
    Me.Move (MaxScreenWidth - Width) / 2, (Screen.Height - Height) / 2
        
    Select Case MySkinName
        Case "PdaSkinPDA"
            i = 1
        Case "PdaSkinPPC"
            i = 0
        Case Else
            i = 0
    End Select
    
    i = 2
    ShapeForm picShape(0), vbYellow, i
    picShape(0).Picture = LoadPicture("")
    picShape(0).Visible = False
    
    ArrangePdaSkinButtons
    
    
    PdaIcon(7).Left = 0
    PdaIcon(7).Top = 0
    
    PdaFrame(6).Left = 45
    PdaFrame(6).Top = 0
    PdaFrame(5).Left = 45
    PdaFrame(5).Top = 45
    PdaFrame(7).Left = 45
    PdaFrame(7).Top = 45
    
    PdaTab(0).Top = 1095
    PdaTab(3).Top = 1095
    PdaTab(4).Top = 1095
    PdaTab(0).Left = 0
    PdaTab(3).Left = 0
    PdaTab(4).Left = 0
    
    For i = 0 To PdaConText.UBound
        PdaConLite(i).Caption = ""
        PdaConText(i).Caption = ""
    Next
    
    PdaFrame(9).Left = -30
    PdaFrame(9).Top = -30
    WebBrowser.Left = 0
    WebBrowser.Top = 0
    
    lblAny(7).Caption = vbNewLine & "Message From The" & vbNewLine & "Interface Priority Filter"
    lblAny(8).Caption = ""
    lblAny(9).Caption = ""
    lblAny(12).Caption = "Yes," & vbNewLine & "Unlock Work Area"
    lblAny(10).Caption = "No," & vbNewLine & "Discard Message"
    
    InetUrlAddr = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "INET_URL_ADDR", "D:\Ufis\System\FdtHtm\MyBlank.htm")
    InetUrlName = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "INET_URL_NAME", "")
    InetDefAddr = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "INET_DEF_ADDR", "D:\Ufis\System\FdtHtm\MyBlank.htm")
    'InetDefAddr = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "INET_DEF_ADDR", "")
    InetDefName = GetIniEntry(myIniFullName, "PDA_FUNCTIONS", "", "INET_DEF_NAME", "")
    
    InetTimer.Tag = "INI"
    InetTimer_Timer
    
End Sub

Private Sub PdaButton_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim l As Long
    Dim T As Long
    Dim W As Long
    Dim h As Long
    l = PdaButton(Index).Left
    T = PdaButton(Index).Top
    W = PdaButton(Index).Width
    h = PdaButton(Index).Height
    If PdaButton(Index).Tag = "" Then PdaButton(Index).Tag = CStr(l) & "," & CStr(T)
    Select Case Index
        Case 7
            l = Val(GetItem(PdaButton(Index).Tag, 1, ",")) - 1
        Case 8
            l = Val(GetItem(PdaButton(Index).Tag, 1, ",")) + 1
        Case 9
            T = Val(GetItem(PdaButton(Index).Tag, 2, ",")) - 1
        Case 10
            T = Val(GetItem(PdaButton(Index).Tag, 2, ",")) + 1
        Case Else
            l = Val(GetItem(PdaButton(Index).Tag, 1, ",")) + 1
            T = Val(GetItem(PdaButton(Index).Tag, 2, ",")) + 1
    End Select
    PdaButton(Index).Move l, T, W, h
    PdaButton(Index).Refresh
    DoEvents
End Sub

Private Sub PdaButton_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim l As Long
    Dim T As Long
    Dim W As Long
    Dim h As Long
    h = PdaButton(Index).Height
    W = PdaButton(Index).Width
    l = Val(GetItem(PdaButton(Index).Tag, 1, ","))
    T = Val(GetItem(PdaButton(Index).Tag, 2, ","))
    PdaButton(Index).Move l, T, W, h
    PdaButton(Index).Refresh
    DoEvents
End Sub

Private Sub PdaConLite_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 5
            'PdaConLite(Index).MouseIcon = Image1(1).MouseIcon
            MbMov = True
            PrevX = X
            PrevY = Y
        Case Else
    End Select
End Sub

Private Sub PdaConLite_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewPos As Long
    Select Case Index
        Case 5
            If MbMov Then
                'Me.Move (Me.Left + X - PrevX), (Me.Top + Y - PrevY)
                NewPos = Me.Left + X - PrevX
                If Me.Left <> NewPos Then Me.Left = NewPos
                NewPos = Me.Top + Y - PrevY
                If Me.Top <> NewPos Then Me.Top = NewPos
            End If
        Case Else
    End Select
End Sub

Private Sub PdaConLite_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 5
            'PdaConLite(Index).MouseIcon = Image1(0).MouseIcon
            MbMov = False
        Case Else
    End Select
End Sub

Private Sub PdaConText_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 5
            PdaConText(Index).MouseIcon = Image1(1).MouseIcon
            MbMov = True
            PrevX = X
            PrevY = Y
        Case Else
    End Select
End Sub

Private Sub PdaConText_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim NewPos As Long
    Select Case Index
        Case 5
            If MbMov Then
                'Me.Move (Me.Left + X - PrevX), (Me.Top + Y - PrevY)
                NewPos = Me.Left + X - PrevX
                If Me.Left <> NewPos Then Me.Left = NewPos
                NewPos = Me.Top + Y - PrevY
                If Me.Top <> NewPos Then Me.Top = NewPos
            End If
        Case Else
    End Select
End Sub

Private Sub PdaConText_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 5
            PdaConText(Index).MouseIcon = Image1(0).MouseIcon
            MbMov = False
        Case Else
    End Select
End Sub

Private Sub PdaFrame_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 11
            PdaFrame(Index).Tag = CStr(chkFunc(14).Value)
            lblAny(6).Tag = PdaFrame(Index).Tag
            chkFunc(14).Value = 1
        Case Else
    End Select
End Sub

Private Sub PdaFrame_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 11
            If Val(PdaFrame(Index).Tag) = 1 Then
                chkFunc(14).Value = 0
            Else
                chkFunc(14).Value = 1
            End If
        Case Else
    End Select
End Sub

Private Sub PdaIcon_Click(Index As Integer)
    'Select Case Index
        'Case 2
        '    If InetUrlAddr <> "" Then
        '        WebBrowser.Navigate InetUrlAddr
        '    End If
        'Case Else
    'End Select
End Sub

Private Sub PdaIcon_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Index
        Case 0  'Ufis Icon
            If Shift = 2 Then
                Command3_Click
            End If
        Case 2  'Aircraft Icon
            If Shift = 2 Then
                If InetUrlAddr <> "" Then
                    WebBrowser.Navigate InetUrlAddr
                End If
            End If
            If Shift = 0 Then
                If InetDefAddr <> "" Then
                    WebBrowser.Navigate InetDefAddr
                End If
            End If
        Case Else
    End Select
End Sub

Private Sub PdaTab_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    If Key = 13 Then
        Select Case Index
            Case 0
                PdaTab_SendLButtonDblClick Index, LineNo, 0
            Case 4
                PdaTab_SendLButtonDblClick Index, LineNo, 0
            Case Else
        End Select
    End If
End Sub

Private Sub PdaTab_OnVScroll(Index As Integer, ByVal LineNo As Long)
    Select Case Index
        Case 0
            AdjustPanelIcon PdaTab(Index), PdaPanel(0).Top, 4
        Case 1
            AdjustPanelIcon PdaTab(Index), PdaPanel(1).Top, 5
        Case 4
            AdjustPanelIcon PdaTab(Index), PdaPanel(2).Top, 4
        Case Else
    End Select
End Sub

Private Sub PdaTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim PdeCase As String
    Dim PdeCcse As String
    Dim ExplRec As String
    Dim tmpText As String
    Dim tmpRema As String
    Dim tmpCase As String
    Dim VisLines As Long
    Dim MsgCase As Integer
    Dim CfmCase As Integer
    Dim i As Integer
    Select Case Index
        Case 0  'ACRTAB Registration Basic Data
            AutoCopy = True
            If (Selected) And (LineNo >= 0) Then
                chkFunc(6).Tag = ""
                If (Not AutoSearch) Or (CurLookUpFldIdx >= 0) Then
                    If CurLookUpFldIdx <> 0 Then txtFunc(0).Text = PdaTab(Index).GetFieldValue(LineNo, "REGN")
                    If CurLookUpFldIdx <> 22 Then txtFunc(22).Text = PdaTab(Index).GetFieldValue(LineNo, "ACT3")
                    If CurLookUpFldIdx <> 21 Then txtFunc(21).Text = PdaTab(Index).GetFieldValue(LineNo, "ACT5")
                    If CurLookUpFldIdx <> 20 Then txtFunc(20).Text = PdaTab(Index).GetFieldValue(LineNo, "ACTI")
                    chkFunc(6).Tag = PdaTab(Index).GetFieldValue(LineNo, "URNO")
                End If
                If chkFunc(2).Value = 1 Then
                    AdjustPanelIcon PdaTab(Index), PdaPanel(2).Top, 4
                End If
            Else
                If Not AutoSearch Then
                    txtFunc(0).Text = ""
                    txtFunc(22).Text = ""
                    txtFunc(21).Text = ""
                    txtFunc(20).Text = ""
                    chkFunc(6).Tag = ""
                End If
            End If
            AutoCopy = False
        Case 1  'PDETAB FDC Notifications
            PdaTab(2).ResetContent
            If (Selected) And (LineNo >= 0) Then
                PdeCase = PdaTab(Index).GetFieldValue(LineNo, "CASE")
                MsgCase = Val(PdeCase)
                PdeCcse = PdaTab(Index).GetFieldValue(LineNo, "CCSE")
                CfmCase = Val(PdeCcse)
                i = 1
                While i < 512
                    tmpCase = GetCaseMeaning(MsgCase, CfmCase, i, tmpText, tmpRema)
                    If tmpCase <> "" Then
                        ExplRec = tmpCase & "," & tmpText & "," & tmpRema
                        PdaTab(2).InsertTextLine ExplRec, False
                    End If
                    i = i * 2
                Wend
                PdaTab(2).AutoSizeColumns
                AdjustPanelData LineNo
            End If
            PdaTab(2).Refresh
            DoEvents
        Case 4  'ACTTAB A/C Type Basic Data
            AutoCopy = True
            If (Selected) And (LineNo >= 0) Then
                chkFunc(10).Tag = ""
                If (Not AutoSearch) Or (CurLookUpFldIdx >= 0) Then
                    If CurLookUpFldIdx <> 26 Then txtFunc(26).Text = PdaTab(Index).GetFieldValue(LineNo, "ACFN")
                    If CurLookUpFldIdx <> 23 Then txtFunc(23).Text = PdaTab(Index).GetFieldValue(LineNo, "ACT3")
                    If CurLookUpFldIdx <> 24 Then txtFunc(24).Text = PdaTab(Index).GetFieldValue(LineNo, "ACT5")
                    If CurLookUpFldIdx <> 25 Then txtFunc(25).Text = PdaTab(Index).GetFieldValue(LineNo, "ACTI")
                    chkFunc(10).Tag = PdaTab(Index).GetFieldValue(LineNo, "URNO")
                End If
                If chkFunc(0).Value = 1 Then
                    AdjustPanelIcon PdaTab(Index), PdaPanel(2).Top, 4
                End If
            Else
                If Not AutoSearch Then
                    txtFunc(26).Text = ""
                    txtFunc(23).Text = ""
                    txtFunc(24).Text = ""
                    txtFunc(25).Text = ""
                    chkFunc(10).Tag = ""
                End If
            End If
            AutoCopy = False
        Case Else
    End Select
End Sub
Private Sub AdjustPanelData(LineNo As Long)
    Dim PdeCase As String
    Dim PdeCcse As String
    Dim PdeUrno As String
    Dim AftFldLst As String
    Dim AftDatLst As String
    Dim AftRegn As String
    Dim AftUrno As String
    Dim AftAct3 As String
    Dim AftAct5 As String
    Dim AftActi As String
    Dim AftPbay As String
    Dim ExcFldLst As String
    Dim ExcDatLst As String
    Dim ExcRegn As String
    Dim ExcAct3 As String
    Dim ExcAct5 As String
    Dim ExcActi As String
    Dim CfmFldLst As String
    Dim CfmDatLst As String
    Dim CfmRegn As String
    Dim CfmAct3 As String
    Dim CfmAct5 As String
    Dim CfmActi As String
    Dim UpdRegn As String
    Dim UpdAct3 As String
    Dim UpdAct5 As String
    Dim UpdActi As String
    Dim MsgCase As Integer
    Dim CfmCase As Integer
    Dim i As Integer
    Dim k As Integer
    MsgCase = 0
    PdaFrame(0).Enabled = False
    PdaFrame(1).Enabled = False
    PdaFrame(2).Enabled = False
    chkFunc(1).Enabled = False
    chkFunc(3).Enabled = False
    chkFunc(4).Enabled = False
    chkFunc(5).Enabled = False
    chkFunc(8).Enabled = False
    chkFunc(1).Tag = ""
    chkFunc(3).Tag = ""
    chkFunc(4).Tag = ""
    chkFunc(5).Tag = ""
    chkFunc(1).Caption = "No Action"
    chkFunc(3).Caption = "No Action"
    For i = 11 To 17
        chkFunc(i).Value = 0
    Next
    If LineNo >= 0 Then
        PdaTimer.Enabled = False
        PdaPanel(0).Enabled = True
        PdeCase = Trim(PdaTab(1).GetFieldValue(LineNo, "CASE"))
        PdeCcse = Trim(PdaTab(1).GetFieldValue(LineNo, "CCSE"))
        PdeUrno = Trim(PdaTab(1).GetFieldValue(LineNo, "URNO"))
        AftPbay = Trim(PdaTab(1).GetFieldValue(LineNo, "PBAY"))
        MsgCase = Val(PdeCase)
        CfmCase = Val(PdeCcse)
        If (MsgCase And 512) = 512 Then
            PdaPanel(3).Visible = True
            lblAny(11).Visible = True
        Else
            PdaPanel(3).Visible = False
            lblAny(11).Visible = False
        End If
        AftFldLst = Replace(PdaTab(1).GetFieldValue(LineNo, "PDFF"), ";", ",", 1, -1, vbBinaryCompare)
        AftDatLst = Replace(PdaTab(1).GetFieldValue(LineNo, "PDFD"), ";", ",", 1, -1, vbBinaryCompare)
        AftUrno = GetFieldValue("URNO", AftDatLst, AftFldLst)
        AftRegn = GetFieldValue("REGN", AftDatLst, AftFldLst)
        AftAct3 = GetFieldValue("ACT3", AftDatLst, AftFldLst)
        AftAct5 = GetFieldValue("ACT5", AftDatLst, AftFldLst)
        AftActi = GetFieldValue("ACTI", AftDatLst, AftFldLst)
        txtFunc(15).Text = AftRegn
        txtFunc(12).Text = AftAct3
        txtFunc(13).Text = AftAct5
        txtFunc(14).Text = AftActi
    
        ExcFldLst = Replace(PdaTab(1).GetFieldValue(LineNo, "PDXF"), ";", ",", 1, -1, vbBinaryCompare)
        ExcDatLst = Replace(PdaTab(1).GetFieldValue(LineNo, "PDXD"), ";", ",", 1, -1, vbBinaryCompare)
        ExcRegn = GetFieldValue("REGN", ExcDatLst, ExcFldLst)
        ExcAct3 = GetFieldValue("ACT3", ExcDatLst, ExcFldLst)
        ExcAct5 = GetFieldValue("ACT5", ExcDatLst, ExcFldLst)
        ExcActi = GetFieldValue("ACTI", ExcDatLst, ExcFldLst)
        
        CfmFldLst = Replace(PdaTab(1).GetFieldValue(LineNo, "CFMF"), ";", ",", 1, -1, vbBinaryCompare)
        CfmDatLst = Replace(PdaTab(1).GetFieldValue(LineNo, "CFMD"), ";", ",", 1, -1, vbBinaryCompare)
        CfmRegn = GetFieldValue("REGN", CfmDatLst, CfmFldLst)
        CfmAct3 = GetFieldValue("ACT3", CfmDatLst, CfmFldLst)
        CfmAct5 = GetFieldValue("ACT5", CfmDatLst, CfmFldLst)
        CfmActi = GetFieldValue("ACTI", CfmDatLst, CfmFldLst)
        
        txtFunc(8).Text = ExcRegn
        txtFunc(11).Text = ExcAct3
        txtFunc(10).Text = ExcAct5
        txtFunc(9).Text = ExcActi
        
        UpdRegn = ExcRegn
        UpdAct3 = ExcAct3
        UpdAct5 = ExcAct5
        UpdActi = ExcActi
        
        'A/C Type not in Basic Data
        If ((MsgCase And 256) = 256) Or ((MsgCase And 128) = 128) Then
            If (CfmCase And 1024) = 1024 Then
                PdaFrame(0).Caption = "Confirmed Aircraft Type"
                txtFunc(1).Text = CfmAct3
                txtFunc(2).Text = CfmAct5
                txtFunc(3).Text = CfmActi
                UpdAct3 = CfmAct3
                UpdAct5 = CfmAct5
                UpdActi = CfmActi
                chkFunc(1).Caption = "Update"
            Else
                PdaFrame(0).Caption = "New Aircraft Type"
                txtFunc(1).Text = ExcAct3
                txtFunc(2).Text = ExcAct5
                txtFunc(3).Text = ExcActi
                chkFunc(1).Caption = "Insert"
            End If
            chkFunc(1).Tag = PdeUrno
            If InStr("256,128", PdeCase) > 0 Then PdaFrame(0).Tag = "DEL" Else PdaFrame(0).Tag = "UPD"
        Else
            txtFunc(1).Text = ""
            txtFunc(2).Text = ""
            txtFunc(3).Text = ""
            chkFunc(1).Tag = ""
            chkFunc(1).Caption = "No Action"
        End If
        'A/C Registration not in Basic Data
        If (MsgCase And 8) = 8 Then
            If (CfmCase And 2048) = 2048 Then
                PdaFrame(1).Caption = "Confirmed Registration"
                txtFunc(4).Text = CfmRegn
                txtFunc(7).Text = CfmAct3
                txtFunc(6).Text = CfmAct5
                txtFunc(5).Text = CfmActi
                UpdRegn = CfmRegn
                UpdAct3 = CfmAct3
                UpdAct5 = CfmAct5
                UpdActi = CfmActi
                chkFunc(3).Caption = "Update"
            Else
                PdaFrame(1).Caption = "New Registration"
                If (CfmCase And 1024) = 1024 Then
                    txtFunc(4).Text = ExcRegn
                    txtFunc(7).Text = CfmAct3
                    txtFunc(6).Text = CfmAct5
                    txtFunc(5).Text = CfmActi
                Else
                    txtFunc(4).Text = ExcRegn
                    txtFunc(7).Text = ExcAct3
                    txtFunc(6).Text = ExcAct5
                    txtFunc(5).Text = ExcActi
                End If
                chkFunc(3).Caption = "Insert"
            End If
            chkFunc(3).Tag = PdeUrno
            If InStr("008", PdeCase) > 0 Then PdaFrame(1).Tag = "DEL" Else PdaFrame(1).Tag = "UPD"
        Else
            txtFunc(4).Text = ""
            txtFunc(7).Text = ""
            txtFunc(6).Text = ""
            txtFunc(5).Text = ""
            chkFunc(3).Tag = ""
            PdaFrame(1).Tag = ""
            chkFunc(3).Caption = "No Action"
        End If
        
        txtFunc(19).Text = UpdRegn
        txtFunc(16).Text = UpdAct3
        txtFunc(17).Text = UpdAct5
        txtFunc(18).Text = UpdActi
        chkFunc(4).Tag = AftUrno
        chkFunc(5).Tag = PdeUrno
        If chkFunc(8).Tag <> AftUrno Then
            'txtFunc(28).Text = ""
            'txtFunc(29).Text = ""
            'txtFunc(30).Text = ""
            'txtFunc(31).Text = ""
        End If
        chkFunc(8).Tag = AftUrno
        
        If UpdRegn <> "" Then
            txtFunc(16).BackColor = vbWhite
            txtFunc(17).BackColor = vbWhite
        Else
            txtFunc(16).BackColor = vbYellow
            txtFunc(17).BackColor = vbYellow
        End If
        
        'If InStr("016,---", PdeCase) > 0 Then
        '    txtFunc(19).Text = ""
        '    txtFunc(16).Text = ""
        '    txtFunc(17).Text = ""
        '    txtFunc(18).Text = ""
        '    txtFunc(16).BackColor = LightGrey
        '    txtFunc(17).BackColor = LightGrey
        '    txtFunc(18).BackColor = LightGrey
        '    txtFunc(19).BackColor = LightGrey
        '    chkFunc(5).Caption = "ACK"
        '    PdaFrame(2).Tag = "ACK"
        'Else
            chkFunc(5).Caption = "Confirm"
            PdaFrame(2).Tag = "CFM"
        'End If
        
        PdaConText(0).Caption = ""
        PdaConLite(0).Caption = PdaConText(0).Caption
        If chkFunc(1).Tag <> "" Then
            PdaFrame(0).Enabled = True
            chkFunc(1).Enabled = True
            'PdaConText(0).Caption = "Create New A/C Type"
            'PdaConLite(0).Caption = PdaConText(0).Caption
        End If
        If chkFunc(3).Tag <> "" Then
            PdaFrame(1).Enabled = True
            chkFunc(3).Enabled = True
            'PdaConText(0).Caption = "Create New A/C Registration"
            'PdaConLite(0).Caption = PdaConText(0).Caption
        End If
        If chkFunc(4).Tag <> "" Then
            PdaFrame(2).Enabled = True
            chkFunc(4).Enabled = True
            chkFunc(8).Enabled = True
            'PdaConText(0).Caption = "Update Related Flight"
            'PdaConLite(0).Caption = PdaConText(0).Caption
        End If
        If chkFunc(5).Tag <> "" Then
            PdaFrame(2).Enabled = True
            chkFunc(5).Enabled = True
            'PdaConText(0).Caption = "Update Related Flight"
            'PdaConLite(0).Caption = PdaConText(0).Caption
        End If
        AdjustPanelIcon PdaTab(1), PdaPanel(1).Top, 5
        
        If (MsgCase And 1) = 1 Then
            chkFunc(2).Value = 1
            txtFunc(0).Text = ExcRegn
            k = 13
            txtFunc_KeyPress 0, k
            txtFunc(4).Refresh
            txtFunc(5).Refresh
            txtFunc(6).Refresh
            txtFunc(7).Refresh
        End If
        
        If (ExcRegn = "") And (InStr(ExcFldLst, "REGN") > 0) Then
            If AftRegn <> "" Then
                chkFunc(14).Value = 1
            End If
        End If
        
        CheckFdcRequest LineNo, MsgCase
    Else
        PdaPanel(0).Enabled = True
        PdaFrame(1).Tag = ""
        PdaIcon(5).Visible = False
        txtFunc(1).Text = ""
        txtFunc(2).Text = ""
        txtFunc(3).Text = ""
        txtFunc(4).Text = ""
        txtFunc(5).Text = ""
        txtFunc(6).Text = ""
        txtFunc(7).Text = ""
        txtFunc(8).Text = ""
        txtFunc(9).Text = ""
        txtFunc(10).Text = ""
        txtFunc(11).Text = ""
        txtFunc(12).Text = ""
        txtFunc(13).Text = ""
        txtFunc(14).Text = ""
        txtFunc(15).Text = ""
        txtFunc(16).Text = ""
        txtFunc(17).Text = ""
        txtFunc(18).Text = ""
        txtFunc(19).Text = ""
        txtFunc(28).Text = ""
        txtFunc(29).Text = ""
        txtFunc(30).Text = ""
        txtFunc(31).Text = ""
        chkFunc(1).Tag = ""
        chkFunc(3).Tag = ""
        chkFunc(4).Tag = ""
        chkFunc(5).Tag = ""
        PdaTimer.Tag = "CLS"
        PdaTimer.Interval = 2000
        PdaTimer.Enabled = True
    End If
    If AftPbay = "" Then AftPbay = "N/A"
    PdaConText(5).Caption = GetFlightLabel(PdaTab(1), LineNo)
    PdaConLite(5).Caption = PdaConText(5).Caption
    PdaConText(4).Caption = "Parking Bay:  " & AftPbay
    PdaConLite(4).Caption = PdaConText(4).Caption
End Sub
Private Sub AdjustPanelIcon(UseTab As TABLib.Tab, Y As Long, IcoIdx As Integer)
    Dim VisLines As Long
    Dim SelLine As Long
    Dim TopLine As Long
    Dim BotLine As Long
    Dim NewTop As Long
    VisLines = VisibleTabLines(UseTab)
    TopLine = UseTab.GetVScrollPos()
    BotLine = TopLine + VisLines - 1
    SelLine = UseTab.GetCurrentSelected
    If (SelLine >= TopLine) And (SelLine <= BotLine) Then
        NewTop = (UseTab.Top \ 15) + Y
        NewTop = NewTop + ((SelLine - TopLine + 2) * UseTab.LineHeight)
        PdaIcon(IcoIdx).Top = NewTop
        PdaIcon(IcoIdx).Visible = True
    Else
        PdaIcon(IcoIdx).Visible = False
    End If

End Sub
Private Function GetCaseMeaning(MsgCase As Integer, CfmCase As Integer, ChkCase As Integer, GetText As String, GetRema As String) As String
    Dim Result As String
    Result = ""
    If (MsgCase And ChkCase) = ChkCase Then
        Select Case ChkCase
            Case 1
                GetText = "A/C CHANGE WITH VALID REGN"
                GetRema = "Attention"
                Result = "001"
            Case 2
                GetText = "FLIGHT EQUIPMENT CHANGE"
                GetRema = "Attention"
                Result = "002"
            Case 4
                GetText = "MISMATCHING A/C TYPES OF REGN"
                GetRema = "Attention"
                Result = "004"
            Case 8
                If (CfmCase And 2048) = 2048 Then
                    GetText = "REGISTRATION CONFIRMED"
                    GetRema = "OK"
                    Result = "008"
                Else
                    GetText = "REGISTR. NOT IN BASIC DATA"
                    GetRema = ""
                    Result = "008"
                End If
            Case 16
                GetText = "DIFFERENT A/C TYPES"
                GetRema = "F.Y.I."
                Result = "016"
            Case 32
                GetText = "FLIGHT EQUIPMENT CHANGE"
                GetRema = ""
                Result = "032"
            Case 64
                GetText = "REMOVES REGISTR. FROM FLIGHT"
                GetRema = ""
                Result = "064"
            Case 128
                If (CfmCase And 1024) = 1024 Then
                    GetText = "A/C TYPE CONFIRMED"
                    GetRema = "OK"
                    Result = "128"
                Else
                    GetText = "A/C TYPE NOT IN BASIC DATA"
                    GetRema = ""
                    Result = "128"
                End If
            Case 256
                If (CfmCase And 1024) = 1024 Then
                    GetText = "A/C TYPE COMBI CONFIRMED"
                    GetRema = ""
                    Result = "256"
                Else
                    GetText = "TYPE COMBI NOT IN BASIC DATA"
                    GetRema = ""
                    Result = "256"
                End If
            Case 512
                GetText = ""
                GetRema = ""
                Result = "512"
            Case Else
        End Select
    End If
    GetCaseMeaning = Result
End Function

Private Sub PdaTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then
        AutoCopy = True
        Select Case Index
            Case 0  'ACRTAB A/C Registration Basic Data
                If (chkFunc(3).Enabled = True) Or (PdaFrame(1).Enabled = True) Then
                    txtFunc(7).Text = txtFunc(22).Text
                    txtFunc(6).Text = txtFunc(21).Text
                    txtFunc(5).Text = txtFunc(20).Text
                    If chkFunc(3).Enabled = False Then
                        txtFunc(4).Text = txtFunc(0).Text
                    Else
                    End If
                    If chkFunc(16).Value = 1 Then
                        chkFunc(16).Value = 0
                        chkFunc(16).Value = 1
                    End If
                    If chkFunc(17).Value = 1 Then
                        chkFunc(17).Value = 0
                        chkFunc(17).Value = 1
                    End If
                End If
            Case 4  'ACTTAB A/C Type Basic Data
                If (chkFunc(1).Enabled = True) Or (PdaFrame(0).Enabled = True) Then
                    txtFunc(1).Text = txtFunc(23).Text
                    txtFunc(2).Text = txtFunc(24).Text
                    txtFunc(3).Text = txtFunc(25).Text
                    txtFunc(27).Text = txtFunc(26).Text
                    If chkFunc(15).Value = 1 Then
                        chkFunc(15).Value = 0
                        chkFunc(15).Value = 1
                    End If
                End If
            Case Else
        End Select
        AutoCopy = False
    ElseIf LineNo = -1 Then
        If ColNo >= 0 Then
            Select Case Index
                Case 0, 4
                    If (PdaTab(Index).CurrentSortColumn = ColNo) And (PdaTab(Index).SortOrderASC = True) Then
                        PdaTab(Index).Sort CStr(ColNo), False, True
                    Else
                        PdaTab(Index).Sort CStr(ColNo), True, True
                    End If
                    PdaTab(Index).AutoSizeColumns
                    PdaTab(Index).Refresh
                    DoEvents
                Case Else
            End Select
        End If
    End If
End Sub

Private Sub PdaTimer_Timer()
    Dim tmpTag As String
    PdaTimer.Enabled = False
    tmpTag = PdaTimer.Tag
    PdaTimer.Tag = ""
    Select Case tmpTag
        Case "CLS"
            PdaButton_Click 0
        Case Else
    End Select
End Sub

Private Sub txtFunc_Change(Index As Integer)
    Dim idx As Integer
    If Not AutoSearch Then CurLookUpFldIdx = -1
    Select Case Index
        Case 0
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(0), 0, txtFunc(Index).Text, True, "", ""
                chkFunc(2).Tag = "0," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case 1
            If chkFunc(15).Value = 1 Then txtFunc(16).Text = txtFunc(Index).Text
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 0, txtFunc(Index).Text, True, "", ""
                AutoSearch = False
            End If
        Case 2
            If chkFunc(15).Value = 1 Then txtFunc(17).Text = txtFunc(Index).Text
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 1, txtFunc(Index).Text, True, "", ""
                AutoSearch = False
            End If
        Case 3
            If chkFunc(15).Value = 1 Then txtFunc(18).Text = txtFunc(Index).Text
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 2, txtFunc(Index).Text, True, "", ""
                AutoSearch = False
            End If
        Case 4
            If chkFunc(16).Value = 1 Then txtFunc(19).Text = txtFunc(Index).Text
        Case 5
            If chkFunc(17).Value = 1 Then txtFunc(18).Text = txtFunc(Index).Text
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(0), 1, txtFunc(Index).Text, True, "", ""
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 0, txtFunc(Index).Text, True, "", ""
                AutoSearch = False
            End If
        Case 6
            If chkFunc(17).Value = 1 Then txtFunc(17).Text = txtFunc(Index).Text
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(0), 1, txtFunc(Index).Text, True, "", ""
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 0, txtFunc(Index).Text, True, "", ""
                AutoSearch = False
            End If
        Case 7
            If chkFunc(17).Value = 1 Then txtFunc(16).Text = txtFunc(Index).Text
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(0), 1, txtFunc(Index).Text, True, "", ""
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 0, txtFunc(Index).Text, True, "", ""
                AutoSearch = False
            End If
        Case 12
            txtFunc(31).Text = txtFunc(Index).Text
        Case 13
            txtFunc(30).Text = txtFunc(Index).Text
        Case 14
            txtFunc(29).Text = txtFunc(Index).Text
        Case 15
            txtFunc(28).Text = txtFunc(Index).Text
        Case 16 To 19
            If Trim(txtFunc(19).Text) = "" Then
                If (Trim(txtFunc(16).Text) = "") And (Trim(txtFunc(17).Text) = "") Then
                    txtFunc(16).BackColor = vbYellow
                    txtFunc(17).BackColor = vbYellow
                ElseIf (Trim(txtFunc(16).Text) <> "") And (Trim(txtFunc(17).Text) <> "") Then
                    txtFunc(16).BackColor = vbYellow
                    txtFunc(17).BackColor = vbWhite
                ElseIf (Trim(txtFunc(16).Text) <> "") Then
                    txtFunc(16).BackColor = vbYellow
                    txtFunc(17).BackColor = vbWhite
                ElseIf (Trim(txtFunc(17).Text) <> "") Then
                    txtFunc(16).BackColor = vbWhite
                    txtFunc(17).BackColor = vbYellow
                End If
            Else
                txtFunc(16).BackColor = vbWhite
                txtFunc(17).BackColor = vbWhite
            End If
        Case 20
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(0), 3, txtFunc(Index).Text, True, "", ""
                chkFunc(2).Tag = "3," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case 21
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(0), 2, txtFunc(Index).Text, True, "", ""
                chkFunc(2).Tag = "2," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case 22
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(0), 1, txtFunc(Index).Text, True, "", ""
                chkFunc(2).Tag = "1," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case 23
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 0, txtFunc(Index).Text, True, "", ""
                chkFunc(0).Tag = "0," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case 24
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 1, txtFunc(Index).Text, True, "", ""
                chkFunc(0).Tag = "1," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case 25
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 2, txtFunc(Index).Text, True, "", ""
                chkFunc(0).Tag = "2," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case 26
            If Not AutoCopy Then
                AutoSearch = True
                CurLookUpFldIdx = Index
                SearchInTabList PdaTab(4), 3, txtFunc(Index).Text, True, "", ""
                chkFunc(0).Tag = "3," & txtFunc(Index).Text
                AutoSearch = False
            End If
        Case Else
    End Select
End Sub

Private Sub txtFunc_GotFocus(Index As Integer)
    SetTextSelected
End Sub
Private Sub txtFunc_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim tmpChr As String
    If KeyAscii <> 13 Then
        If Index <> 26 Then
            If (Index = 19) And (chkFunc(14).Value = 1) Then KeyAscii = 0
            KeyAscii = Asc(UCase(Chr(KeyAscii)))
            Select Case KeyAscii
                Case 48 To 57, 65 To 90 'Text and numbers
                Case 8  'Function Keys
                Case Else
                    KeyAscii = 0
            End Select
        Else
            tmpChr = Chr(KeyAscii)
            Select Case KeyAscii
                Case 48 To 57, 65 To 90 'Text and numbers
                Case 8  'Function Keys
                Case 34
                    KeyAscii = 0
                Case Else
                    If InStr(",'", tmpChr) > 0 Then KeyAscii = 0
            End Select
        End If
    Else
        Select Case Index
            Case 0
                PdaFrame(1).Enabled = True
                PdaTab(0).SetCurrentSelection PdaTab(0).GetCurrentSelected
                PdaTab_SendLButtonDblClick 0, PdaTab(0).GetCurrentSelected, 0
                PdaFrame(1).Caption = "Existing Registration"
                chkFunc(3).Enabled = False
                chkFunc(3).Caption = "No Action"
            Case 23 To 26
                PdaFrame(0).Enabled = True
                PdaTab(4).SetCurrentSelection PdaTab(4).GetCurrentSelected
                PdaTab_SendLButtonDblClick 4, PdaTab(4).GetCurrentSelected, 0
                PdaFrame(0).Caption = "Existing A/C Type"
                chkFunc(1).Enabled = False
                chkFunc(1).Caption = "No Action"
            Case 20 To 22
                PdaFrame(1).Enabled = True
                PdaTab(0).SetCurrentSelection PdaTab(0).GetCurrentSelected
                PdaTab_SendLButtonDblClick 0, PdaTab(0).GetCurrentSelected, 0
                PdaFrame(1).Caption = "Existing Registration"
                chkFunc(3).Enabled = False
                chkFunc(3).Caption = "No Action"
            Case Else
        End Select
        KeyAscii = 0
    End If
End Sub

Private Sub WebBrowser_DocumentComplete(ByVal pDisp As Object, URL As Variant)
    Dim CurUrl As String
    CurUrl = URL
    CurUrl = UCase(CurUrl)
    If InetTimer.Tag <> "" Then
        InetTimer.Enabled = True
    End If
    If CurUrl = UCase(InetUrlAddr) Then
        CurUrlName = InetUrlName
        PdaConText(3).Caption = CurUrlName
        PdaConLite(3).Caption = PdaConText(3).Caption
    End If
    If CurUrl = UCase(InetDefAddr) Then
        CurUrlName = InetDefName
        PdaConText(3).Caption = CurUrlName
        PdaConLite(3).Caption = PdaConText(3).Caption
    End If
End Sub

Private Sub WebBrowser_NavigateError(ByVal pDisp As Object, URL As Variant, Frame As Variant, StatusCode As Variant, Cancel As Boolean)
    Cancel = True
    InetTimer.Tag = "OFF"
    InetTimer.Interval = 2000
    InetTimer.Enabled = False
End Sub

Private Sub WebBrowser_ProgressChange(ByVal Progress As Long, ByVal ProgressMax As Long)
    Dim tmpTxt As String
    Dim lCur As Long
    Dim lMax As Long
    lCur = Progress
    lMax = ProgressMax
    tmpTxt = CStr(lCur) & "/" & CStr(lMax)
    If lCur >= lMax Then tmpTxt = ""
    If lMax <= 0 Then tmpTxt = ""
    If lCur <= 0 Then tmpTxt = ""
    If tmpTxt <> "" Then
        If (lCur * 10) < lMax Then lCur = lCur * 10
        ProgressBar.Max = lMax
        ProgressBar.Value = lCur
        ProgressBar.Visible = True
        tmpTxt = CStr((lCur * 100) \ lMax) & " %"
    Else
        ProgressBar.Visible = False
    End If
    PdaConText(2).Caption = tmpTxt
    PdaConLite(2).Caption = PdaConText(2).Caption
End Sub

Private Sub WebBrowser_StatusTextChange(ByVal Text As String)
    Dim tmpTxt As String
    tmpTxt = Text
End Sub

Private Function GetFlightLabel(UseTab As TABLib.Tab, LineNo As Long) As String
    Dim tmpText As String
    Dim tmpAdid As String
    Dim tmpFlno As String
    Dim tmpTist As String
    Dim tmpStdt As String
    Dim tmpType As String
    Dim tmpTime As String
    Dim TimeVal As Date
    tmpText = ""
    If LineNo >= 0 Then
        tmpAdid = UseTab.GetFieldValue(LineNo, "ADID")
        tmpFlno = UseTab.GetFieldValue(LineNo, "FLNO")
        tmpTist = UseTab.GetFieldValue(LineNo, "TIST")
        tmpStdt = UseTab.GetFieldValue(LineNo, "STDT")
        Select Case tmpAdid
            Case "A"
                Select Case tmpTist
                    Case "S"
                        tmpType = "STA"
                    Case "E"
                        tmpType = "ETA"
                    Case "A"
                        tmpType = "ATA"
                    Case "O"
                        tmpType = "ONB"
                    Case Else
                        tmpType = "#" & tmpTist & "#"
                End Select
            Case "D", "B"
                Select Case tmpTist
                    Case "S"
                        tmpType = "STD"
                    Case "E"
                        tmpType = "ETD"
                    Case "A"
                        tmpType = "ATD"
                    Case "O"
                        tmpType = "OFB"
                    Case Else
                        tmpType = "#" & tmpTist & "#"
                End Select
            Case Else
        End Select
        If tmpStdt <> "" Then
            TimeVal = CedaFullDateToVb(tmpStdt)
            TimeVal = DateAdd("n", UtcTimeDisp, TimeVal)
            tmpTime = Format(TimeVal, "hh:mm/dd")
            End If
        tmpText = ""
        tmpText = tmpText & tmpFlno & "  "
        tmpText = tmpText & tmpType & " "
        tmpText = tmpText & tmpTime
        If UtcTimeDisp = 0 Then
            tmpText = tmpText & "z"
        Else
            tmpText = tmpText & " LT"
        End If
    End If
    GetFlightLabel = tmpText
End Function
Private Sub ArrangePdaSkinButtons()
    Dim tmpTag As String
    Select Case MySkinName
        Case "PdaSkinPDA"
            If OnTop.Value = 1 Then
                PdaButton(1).Picture = Image1(0).Picture
                PdaButton(1).Tag = Image1(0).Tag
                PdaButton(1).ToolTipText = Image1(0).ToolTipText
            Else
                PdaButton(1).Picture = Image1(2).Picture
                PdaButton(1).Tag = Image1(2).Tag
                PdaButton(1).ToolTipText = Image1(2).ToolTipText
            End If
            PdaButton(0).Picture = Image1(8).Picture
            PdaButton(0).Tag = Image1(8).Tag
        Case "PdaSkinPPC"
            If OnTop.Value = 1 Then
                PdaButton(1).Picture = Image1(4).Picture
                PdaButton(1).Tag = Image1(4).Tag
                PdaButton(1).ToolTipText = Image1(4).ToolTipText
            Else
                PdaButton(1).Picture = Image1(6).Picture
                PdaButton(1).Tag = Image1(6).Tag
                PdaButton(1).ToolTipText = Image1(6).ToolTipText
            End If
            PdaButton(0).Picture = Image1(7).Picture
            PdaButton(0).Tag = Image1(7).Tag
        Case Else
    End Select
    tmpTag = PdaButton(1).Tag
    If tmpTag <> "" Then
        PdaButton(1).Left = Val(GetItem(tmpTag, 1, ","))
        PdaButton(1).Top = Val(GetItem(tmpTag, 2, ","))
    End If
    tmpTag = PdaButton(0).Tag
    If tmpTag <> "" Then
        PdaButton(0).Left = Val(GetItem(tmpTag, 1, ","))
        PdaButton(0).Top = Val(GetItem(tmpTag, 2, ","))
    End If
End Sub

Private Sub CheckFdcRequest(LineNo As Long, MsgCase As Integer)
    Dim tmpPdxf As String
    Dim tmpPdxd As String
    Dim tmpRegn As String
    Dim tmpAct3 As String
    Dim tmpAct5 As String
    Dim tmpActi As String
    Dim tmpActp As String
    Dim tmpUser As String
    Dim tmpFdcu As String
    Dim tmpDssu As String
    Dim tmpAppl As String
    Dim tmpMsg As String
    If (MsgCase And 512) = 512 Then
        tmpPdxf = PdaTab(1).GetFieldValue(LineNo, "PDXF")
        tmpPdxd = PdaTab(1).GetFieldValue(LineNo, "PDXD")
        tmpPdxf = Replace(tmpPdxf, ";", ",", 1, -1, vbBinaryCompare)
        tmpPdxd = Replace(tmpPdxd, ";", ",", 1, -1, vbBinaryCompare)
        tmpRegn = GetFieldValue("REGN", tmpPdxd, tmpPdxf)
        tmpAct3 = GetFieldValue("ACT3", tmpPdxd, tmpPdxf)
        tmpAct5 = GetFieldValue("ACT5", tmpPdxd, tmpPdxf)
        tmpActi = GetFieldValue("ACTI", tmpPdxd, tmpPdxf)
        tmpActp = tmpAct3 & tmpAct5 & tmpActi
        tmpFdcu = GetFieldValue("FDCU", tmpPdxd, tmpPdxf)
        tmpDssu = GetFieldValue("DSSU", tmpPdxd, tmpPdxf)
        tmpAppl = GetFieldValue("APPL", tmpPdxd, tmpPdxf)
        tmpMsg = ""
        tmpMsg = tmpMsg & vbNewLine
        tmpMsg = tmpMsg & "The interface (or application) " & Chr(34) & " " & tmpFdcu & " " & Chr(34) & vbNewLine
        If tmpRegn <> "" Then
            tmpMsg = tmpMsg & "wants to override the A/C registration" & vbNewLine
        ElseIf (tmpRegn = "") And (tmpActp = "") Then
            tmpMsg = tmpMsg & "wants to remove the A/C registration" & vbNewLine
        ElseIf (tmpRegn = "") And (tmpActp <> "") Then
            tmpMsg = tmpMsg & "wants to remove the A/C registration" & vbNewLine
            tmpMsg = tmpMsg & "and to change the aircraft type" & vbNewLine
        ElseIf tmpActp <> "" Then
            tmpMsg = tmpMsg & "wants to modify the operating A/C type" & vbNewLine
        Else
            tmpMsg = tmpMsg & "wants to update the actual aircraft data" & vbNewLine
        End If
        tmpMsg = tmpMsg & "of flight " & GetFlightLabel(PdaTab(1), LineNo) & "." & vbNewLine
        lblAny(8).Caption = tmpMsg

        tmpMsg = ""
        tmpMsg = tmpMsg & "The last recent originator of the data" & vbNewLine
        tmpMsg = tmpMsg & "is interface (or application) " & Chr(34) & " " & tmpDssu & " " & Chr(34) & "." & vbNewLine
        tmpMsg = tmpMsg & "---" & vbNewLine
        tmpMsg = tmpMsg & "Do you allow the transaction?" & vbNewLine
        lblAny(9).Caption = tmpMsg

        
        PdaPanel(3).Visible = True
        PdaPanel(3).ZOrder
        chkFunc(19).Tag = CStr(chkFunc(0).Value) & "," & CStr(chkFunc(2).Value)
        chkFunc(2).Value = 0
        chkFunc(0).Value = 0
        PdaPanel(0).Enabled = False
        lblAny(11).Visible = True
    Else
        'PdaPanel(0).Enabled = True
        'PdaPanel(3).Visible = False
    End If

End Sub
Private Sub DiscardMessage()
    Dim PdeUrno As String
    Dim AftUrno As String
    Dim tmpTbl As String
    Dim tmpFldLst As String
    Dim tmpDatLst As String
    Dim tmpSqlKey As String
    Dim tmpResult As String
    PdeUrno = chkFunc(5).Tag
    AftUrno = chkFunc(4).Tag
    If PdeUrno <> "" Then
        tmpSqlKey = "WHERE URNO=" & PdeUrno
        tmpTbl = "PDETAB"
        tmpFldLst = ""
        tmpDatLst = ""
        'UfisServer.CallCeda tmpResult, "DRT", tmpTbl, "", "", tmpSqlKey, "", 0, False, False
        MainDialog.InsertActtab "DEL", "---", PdeUrno, AftUrno, tmpFldLst, tmpDatLst, ""
    End If
End Sub
