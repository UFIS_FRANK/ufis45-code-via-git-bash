VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form StatusChart 
   Caption         =   "UFIS GOCC Status Chart"
   ClientHeight    =   7590
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13350
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "StatusChart.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7590
   ScaleWidth      =   13350
   Begin VB.PictureBox DepFlight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Index           =   0
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   7335
      TabIndex        =   62
      TabStop         =   0   'False
      Top             =   5340
      Visible         =   0   'False
      Width           =   7365
      Begin VB.Image picDepStat 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   270
         Index           =   0
         Left            =   15
         Picture         =   "StatusChart.frx":014A
         Stretch         =   -1  'True
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblDepFtyp 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   300
         TabIndex        =   86
         ToolTipText     =   "Status"
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblDepPstd 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4935
         TabIndex        =   73
         ToolTipText     =   "Bay"
         Top             =   15
         Width           =   510
      End
      Begin VB.Label lblDepOfbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4305
         TabIndex        =   72
         ToolTipText     =   "ATD (OFB)"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblDepFlti 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1680
         TabIndex        =   71
         Top             =   15
         Width           =   300
      End
      Begin VB.Label lblDepVia3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   2535
         TabIndex        =   70
         ToolTipText     =   "Next Station"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblDepEtdi 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3705
         TabIndex        =   69
         ToolTipText     =   "ETD"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblDepStod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3105
         TabIndex        =   68
         ToolTipText     =   "STD"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblDepDes3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1995
         TabIndex        =   67
         ToolTipText     =   "Destination"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblDepFlno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   585
         TabIndex        =   66
         Top             =   15
         Width           =   1110
      End
      Begin VB.Label lblDepGtd1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   5430
         TabIndex        =   65
         ToolTipText     =   "Gate"
         Top             =   15
         Width           =   510
      End
      Begin VB.Label lblDepRegn 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   5955
         TabIndex        =   64
         ToolTipText     =   "Aircraft"
         Top             =   15
         Width           =   855
      End
      Begin VB.Label lblDepAct3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   6795
         TabIndex        =   63
         ToolTipText     =   "Type"
         Top             =   15
         Width           =   525
      End
   End
   Begin VB.PictureBox ArrFlight 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Index           =   0
      Left            =   120
      ScaleHeight     =   300
      ScaleWidth      =   7335
      TabIndex        =   50
      TabStop         =   0   'False
      Top             =   4980
      Visible         =   0   'False
      Width           =   7365
      Begin VB.Image picArrStat 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   270
         Index           =   0
         Left            =   7050
         Picture         =   "StatusChart.frx":0294
         Stretch         =   -1  'True
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblArrFtyp 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   6765
         TabIndex        =   85
         ToolTipText     =   "Status"
         Top             =   15
         Width           =   270
      End
      Begin VB.Label lblArrAct3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   6225
         TabIndex        =   61
         ToolTipText     =   "Type"
         Top             =   15
         Width           =   525
      End
      Begin VB.Label lblArrRegn 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   5385
         TabIndex        =   60
         ToolTipText     =   "Aircraft"
         Top             =   15
         Width           =   855
      End
      Begin VB.Label lblArrGta1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4860
         TabIndex        =   59
         ToolTipText     =   "Gate"
         Top             =   15
         Width           =   510
      End
      Begin VB.Label lblArrFlno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   15
         TabIndex        =   58
         Top             =   15
         Width           =   1110
      End
      Begin VB.Label lblArrOrg3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1425
         TabIndex        =   57
         ToolTipText     =   "Origin"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblArrStoa 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   2535
         TabIndex        =   56
         ToolTipText     =   "STA"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblArrEtai 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3135
         TabIndex        =   55
         ToolTipText     =   "ETA"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblArrVia3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1965
         TabIndex        =   54
         ToolTipText     =   "Previous Station"
         Top             =   15
         Width           =   555
      End
      Begin VB.Label lblArrFlti 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   1110
         TabIndex        =   53
         Top             =   15
         Width           =   300
      End
      Begin VB.Label lblArrOnbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   3735
         TabIndex        =   52
         ToolTipText     =   "ATA (ONB)"
         Top             =   15
         Width           =   615
      End
      Begin VB.Label lblArrPsta 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   0
         Left            =   4365
         TabIndex        =   51
         ToolTipText     =   "Bay"
         Top             =   15
         Width           =   510
      End
   End
   Begin VB.PictureBox TimeScroll2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1410
      Index           =   0
      Left            =   11520
      ScaleHeight     =   1350
      ScaleWidth      =   270
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   1020
      Width           =   330
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   13
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   132
         Top             =   0
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Caption         =   "A"
         Enabled         =   0   'False
         Height          =   270
         Index           =   9
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   77
         ToolTipText     =   "Set Alert Time Ranges"
         Top             =   1080
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Caption         =   "Z"
         Height          =   270
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   76
         ToolTipText     =   "Zoom Time Scale"
         Top             =   810
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   6
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   270
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Caption         =   "F"
         Height          =   270
         Index           =   7
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Fix the Time Line"
         Top             =   540
         Width           =   270
      End
   End
   Begin VB.PictureBox TimeScroll1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1410
      Index           =   0
      Left            =   1080
      ScaleHeight     =   1350
      ScaleWidth      =   270
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   1020
      Visible         =   0   'False
      Width           =   330
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   12
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   131
         Top             =   0
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Caption         =   "A"
         Enabled         =   0   'False
         Height          =   270
         Index           =   3
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   78
         ToolTipText     =   "Set Alert Time Ranges"
         Top             =   1080
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Caption         =   "Z"
         Height          =   270
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   75
         ToolTipText     =   "Zoom Time Scale"
         Top             =   810
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Caption         =   "F"
         Height          =   270
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   42
         ToolTipText     =   "Fix the Time Line"
         Top             =   540
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   270
         Width           =   270
      End
   End
   Begin VB.PictureBox HorizScroll3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Index           =   0
      Left            =   11520
      ScaleHeight     =   810
      ScaleWidth      =   270
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   3960
      Width           =   330
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   14
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   139
         Top             =   540
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   11
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   80
         Top             =   270
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Height          =   270
         Index           =   10
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Show/Hide the Right Scroll Bar"
         Top             =   0
         Width           =   270
      End
   End
   Begin VB.PictureBox HorizScroll1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Index           =   0
      Left            =   1080
      ScaleHeight     =   810
      ScaleWidth      =   270
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   3960
      Width           =   330
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   15
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   140
         Top             =   540
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Enabled         =   0   'False
         Height          =   270
         Index           =   5
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   79
         Top             =   270
         Width           =   270
      End
      Begin VB.CheckBox chkSwitch 
         Height          =   270
         Index           =   4
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   24
         ToolTipText     =   "Show/Hide the Left Scroll Bar"
         Top             =   0
         Width           =   270
      End
   End
   Begin VB.PictureBox HorizScroll2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Index           =   0
      Left            =   1440
      ScaleHeight     =   270
      ScaleWidth      =   9975
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   3960
      Width           =   10035
      Begin VB.HScrollBar HScroll2 
         Height          =   270
         Index           =   0
         LargeChange     =   30
         Left            =   0
         Max             =   3600
         SmallChange     =   5
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   0
         Width           =   7635
      End
   End
   Begin VB.PictureBox BottomRemark 
      AutoRedraw      =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Index           =   0
      Left            =   1440
      ScaleHeight     =   480
      ScaleWidth      =   9975
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   4290
      Width           =   10035
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "                                         "
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   1
         Left            =   60
         TabIndex        =   47
         Top             =   15
         Width           =   1845
      End
   End
   Begin VB.PictureBox TopRemark 
      AutoRedraw      =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   570
      Index           =   0
      Left            =   1440
      ScaleHeight     =   510
      ScaleWidth      =   9975
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   1020
      Width           =   10035
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "                     "
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   0
         Left            =   60
         TabIndex        =   46
         Top             =   15
         Width           =   945
      End
   End
   Begin VB.PictureBox WorkArea 
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      Index           =   0
      Left            =   1440
      ScaleHeight     =   1365
      ScaleWidth      =   9975
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   2490
      Width           =   10035
      Begin VB.PictureBox VScrollArea 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   0
         Left            =   90
         ScaleHeight     =   1005
         ScaleWidth      =   9705
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   90
         Width           =   9765
         Begin VB.PictureBox HScrollArea 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   765
            Index           =   0
            Left            =   960
            ScaleHeight     =   705
            ScaleWidth      =   7605
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   120
            Width           =   7665
            Begin VB.PictureBox TimeRange 
               Appearance      =   0  'Flat
               BackColor       =   &H00000000&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   570
               Index           =   3
               Left            =   300
               ScaleHeight     =   570
               ScaleWidth      =   45
               TabIndex        =   119
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox TimeRange 
               Appearance      =   0  'Flat
               BackColor       =   &H00000000&
               BorderStyle     =   0  'None
               FillColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   570
               Index           =   2
               Left            =   210
               ScaleHeight     =   570
               ScaleWidth      =   45
               TabIndex        =   118
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               FillColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   1
               Left            =   120
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   84
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   30
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   83
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox ChartLine2 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   0
               Left            =   510
               ScaleHeight     =   360
               ScaleWidth      =   6975
               TabIndex        =   36
               TabStop         =   0   'False
               Top             =   120
               Width           =   7035
               Begin VB.Label lblRightBarCover 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00C0C0C0&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   150
                  Index           =   0
                  Left            =   30
                  TabIndex        =   133
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   285
               End
               Begin VB.Image picSortTime 
                  Height          =   240
                  Index           =   0
                  Left            =   3450
                  Picture         =   "StatusChart.frx":03DE
                  ToolTipText     =   "Time Check Point"
                  Top             =   -30
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picDep1 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   5250
                  Picture         =   "StatusChart.frx":0528
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picDep4 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   6060
                  Picture         =   "StatusChart.frx":0C2A
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picArr4 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   2520
                  Picture         =   "StatusChart.frx":11B4
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picDep3 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   5790
                  Picture         =   "StatusChart.frx":12FE
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picDep2 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   5520
                  Picture         =   "StatusChart.frx":1888
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picArr3 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   2190
                  Picture         =   "StatusChart.frx":1E12
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Image picArr2 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   1890
                  Picture         =   "StatusChart.frx":239C
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Label lblArrJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   420
                  TabIndex        =   91
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   690
                  TabIndex        =   90
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   825
               End
               Begin VB.Label lblDepJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   4110
                  TabIndex        =   89
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblDepJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   4440
                  TabIndex        =   88
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   765
               End
               Begin VB.Image picArr1 
                  Appearance      =   0  'Flat
                  Height          =   240
                  Index           =   0
                  Left            =   1560
                  Picture         =   "StatusChart.frx":27DE
                  Stretch         =   -1  'True
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   240
               End
               Begin VB.Label lblLeftBarCover 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00C0C0C0&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   150
                  Index           =   0
                  Left            =   30
                  TabIndex        =   87
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   285
               End
               Begin VB.Label lblTowBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   3390
                  TabIndex        =   82
                  Top             =   60
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.Label lblDepBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   4050
                  TabIndex        =   81
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FFFF&
                  BorderStyle     =   1  'Fixed Single
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   360
                  TabIndex        =   49
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   1005
               End
            End
            Begin VB.PictureBox NowTime 
               Appearance      =   0  'Flat
               BackColor       =   &H000000FF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   1
               Left            =   390
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   35
               TabStop         =   0   'False
               Top             =   0
               Width           =   45
            End
         End
         Begin VB.PictureBox RightScale 
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   765
            Index           =   0
            Left            =   8700
            ScaleHeight     =   705
            ScaleWidth      =   855
            TabIndex        =   32
            Top             =   120
            Visible         =   0   'False
            Width           =   915
            Begin VB.PictureBox ChartLine3 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   0
               Left            =   90
               ScaleHeight     =   360
               ScaleWidth      =   555
               TabIndex        =   33
               TabStop         =   0   'False
               Top             =   120
               Width           =   615
            End
         End
         Begin VB.PictureBox LeftScale 
            BackColor       =   &H00C0C0C0&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   765
            Index           =   0
            Left            =   30
            ScaleHeight     =   705
            ScaleWidth      =   795
            TabIndex        =   30
            Top             =   120
            Visible         =   0   'False
            Width           =   855
            Begin VB.PictureBox ChartLine1 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   0
               Left            =   90
               ScaleHeight     =   360
               ScaleWidth      =   555
               TabIndex        =   31
               TabStop         =   0   'False
               Top             =   120
               Width           =   615
            End
         End
      End
   End
   Begin VB.PictureBox VertScroll2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      Index           =   0
      Left            =   11520
      ScaleHeight     =   1365
      ScaleWidth      =   270
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2490
      Width           =   330
      Begin VB.VScrollBar VScroll2 
         Height          =   1095
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   0
         Width           =   270
      End
   End
   Begin VB.PictureBox TimePanel 
      BackColor       =   &H00C0C0C0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Index           =   0
      Left            =   1440
      ScaleHeight     =   795
      ScaleWidth      =   9975
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   1590
      Width           =   10035
      Begin VB.Frame fraZoomOptions 
         BackColor       =   &H00E0E0E0&
         Height          =   855
         Left            =   0
         TabIndex        =   120
         Top             =   -90
         Visible         =   0   'False
         Width           =   4515
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Very Large"
            Height          =   210
            Index           =   8
            Left            =   3030
            TabIndex        =   129
            TabStop         =   0   'False
            Tag             =   "240"
            Top             =   600
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Larger"
            Height          =   210
            Index           =   7
            Left            =   3030
            TabIndex        =   128
            TabStop         =   0   'False
            Tag             =   "180"
            Top             =   390
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Large"
            Height          =   210
            Index           =   6
            Left            =   3030
            TabIndex        =   127
            TabStop         =   0   'False
            Tag             =   "120"
            Top             =   180
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Standard XL"
            Height          =   210
            Index           =   5
            Left            =   1560
            TabIndex        =   126
            TabStop         =   0   'False
            Tag             =   "90"
            Top             =   600
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Standard XM"
            Height          =   210
            Index           =   4
            Left            =   1560
            TabIndex        =   125
            TabStop         =   0   'False
            Tag             =   "75"
            Top             =   390
            Value           =   -1  'True
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Standard XS"
            Height          =   210
            Index           =   3
            Left            =   1560
            TabIndex        =   124
            TabStop         =   0   'False
            Tag             =   "60"
            Top             =   180
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Very Small"
            Height          =   210
            Index           =   2
            Left            =   90
            TabIndex        =   123
            TabStop         =   0   'False
            Tag             =   "15"
            Top             =   600
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Smaller"
            Height          =   210
            Index           =   1
            Left            =   90
            TabIndex        =   122
            TabStop         =   0   'False
            Tag             =   "30"
            Top             =   390
            Width           =   1395
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Small"
            Height          =   210
            Index           =   0
            Left            =   90
            TabIndex        =   121
            TabStop         =   0   'False
            Tag             =   "45"
            Top             =   180
            Width           =   1395
         End
      End
      Begin VB.PictureBox TimeScale 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00E0E0E0&
         Height          =   855
         Index           =   0
         Left            =   4680
         ScaleHeight     =   795
         ScaleWidth      =   4725
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   -30
         Width           =   4785
         Begin VB.PictureBox TimeRange 
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   570
            Index           =   0
            Left            =   570
            ScaleHeight     =   570
            ScaleWidth      =   45
            TabIndex        =   117
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.PictureBox TimeRange 
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   0  'None
            FillColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   570
            Index           =   1
            Left            =   660
            ScaleHeight     =   570
            ScaleWidth      =   45
            TabIndex        =   116
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Timer TimeScaleTimer 
            Enabled         =   0   'False
            Interval        =   1000
            Left            =   30
            Top             =   90
         End
         Begin VB.PictureBox NowTime 
            Appearance      =   0  'Flat
            BackColor       =   &H000000FF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1440
            Index           =   0
            Left            =   3840
            ScaleHeight     =   1410
            ScaleWidth      =   15
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   285
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Image NowTimeHint 
            Height          =   240
            Index           =   1
            Left            =   4260
            Picture         =   "StatusChart.frx":2EE0
            Top             =   15
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image NowTimeHint 
            Height          =   240
            Index           =   0
            Left            =   3240
            Picture         =   "StatusChart.frx":302A
            Top             =   15
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image icoZoomRange 
            Height          =   480
            Index           =   1
            Left            =   2790
            Picture         =   "StatusChart.frx":3174
            ToolTipText     =   "End of loaded time window"
            Top             =   480
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.Image icoZoomRange 
            Height          =   480
            Index           =   0
            Left            =   2280
            Picture         =   "StatusChart.frx":347E
            ToolTipText     =   "Begin of loaded time window"
            Top             =   480
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.Label lblDuration 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "01:00"
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   1
            Left            =   870
            TabIndex        =   115
            Top             =   570
            Visible         =   0   'False
            Width           =   405
         End
         Begin VB.Image icoBarRange 
            Height          =   240
            Index           =   0
            Left            =   0
            Picture         =   "StatusChart.frx":3788
            Top             =   540
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image icoBarRange 
            Height          =   240
            Index           =   1
            Left            =   300
            Picture         =   "StatusChart.frx":38D2
            Top             =   540
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label lblOneMinuteWidth 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   120
            Left            =   0
            TabIndex        =   114
            Top             =   0
            Visible         =   0   'False
            Width           =   435
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   1
            Left            =   3930
            Picture         =   "StatusChart.frx":3A1C
            Top             =   540
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   0
            Left            =   3570
            Picture         =   "StatusChart.frx":3B66
            Top             =   540
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label lblBgnTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H000080FF&
            Caption         =   "24SEP07"
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   750
            TabIndex        =   74
            Top             =   30
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblDuration 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "01:00"
            ForeColor       =   &H80000008&
            Height          =   225
            Index           =   0
            Left            =   780
            TabIndex        =   48
            Top             =   555
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.Label lblEndTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "16:00"
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   0
            Left            =   780
            TabIndex        =   39
            Top             =   270
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Label lblCurTime 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "00:00:00"
            ForeColor       =   &H00000000&
            Height          =   270
            Index           =   0
            Left            =   3480
            TabIndex        =   38
            Top             =   15
            Visible         =   0   'False
            Width           =   780
         End
      End
   End
   Begin VB.PictureBox TopPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   60
      ScaleHeight     =   405
      ScaleWidth      =   11715
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   510
      Visible         =   0   'False
      Width           =   11775
   End
   Begin VB.PictureBox VertScroll1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      Index           =   0
      Left            =   1080
      ScaleHeight     =   1365
      ScaleWidth      =   270
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   2490
      Visible         =   0   'False
      Width           =   330
      Begin VB.VScrollBar VScroll1 
         Height          =   1035
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   0
         Width           =   270
      End
   End
   Begin VB.PictureBox ButtonPanel 
      AutoRedraw      =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   60
      ScaleHeight     =   315
      ScaleWidth      =   11715
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   120
      Width           =   11775
      Begin VB.CheckBox chkWork 
         Caption         =   "Reload"
         Enabled         =   0   'False
         Height          =   315
         Index           =   7
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   142
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Options"
         Enabled         =   0   'False
         Height          =   315
         Index           =   6
         Left            =   4200
         Style           =   1  'Graphical
         TabIndex        =   141
         ToolTipText     =   "Hide Already Handled Arrival Flights"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CommandButton cmdSyncTime 
         Caption         =   "ST"
         Height          =   315
         Left            =   10500
         TabIndex        =   138
         ToolTipText     =   "Sync TimeLine (Test Only)"
         Top             =   0
         Visible         =   0   'False
         Width           =   360
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Overview"
         Height          =   315
         Index           =   5
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   137
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Hide ARR"
         Height          =   315
         Index           =   4
         Left            =   6330
         Style           =   1  'Graphical
         TabIndex        =   136
         ToolTipText     =   "Hide Already Handled Arrival Flights"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Hide NOP"
         Height          =   315
         Index           =   3
         Left            =   8430
         Style           =   1  'Graphical
         TabIndex        =   135
         ToolTipText     =   "Hide Non Operating (Cancelled) Flights"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Hide AIRB"
         Height          =   315
         Index           =   2
         Left            =   7380
         Style           =   1  'Graphical
         TabIndex        =   134
         ToolTipText     =   "Hide Already Departed Flights"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Overlap"
         Height          =   315
         Index           =   1
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   130
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Redraw"
         Height          =   315
         Index           =   0
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox LeftPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3225
      Index           =   0
      Left            =   60
      ScaleHeight     =   3165
      ScaleWidth      =   915
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1020
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6780
      Left            =   11910
      ScaleHeight     =   6720
      ScaleWidth      =   1155
      TabIndex        =   0
      Top             =   120
      Width           =   1215
      Begin VB.PictureBox ComPicPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   570
         Index           =   0
         Left            =   30
         ScaleHeight     =   540
         ScaleWidth      =   1080
         TabIndex        =   105
         Top             =   4260
         Visible         =   0   'False
         Width           =   1110
         Begin VB.CommandButton cmdComPicMode 
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   8.25
               Charset         =   161
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   112
            Top             =   0
            Width           =   255
         End
         Begin VB.Image picComPic 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Index           =   0
            Left            =   -15
            Picture         =   "StatusChart.frx":3CB0
            Stretch         =   -1  'True
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblComPicType 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "A"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   108
            Top             =   270
            Width           =   255
         End
         Begin VB.Label lblComPicCount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "123"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   510
            TabIndex        =   107
            Top             =   270
            Width           =   570
         End
         Begin VB.Label lblComPicText 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Alert Cm"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   106
            Top             =   -15
            Width           =   810
         End
         Begin VB.Label lblComPicBack 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   -15
            TabIndex        =   109
            Top             =   270
            Width           =   270
         End
      End
      Begin VB.PictureBox DepPicPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   570
         Index           =   0
         Left            =   30
         ScaleHeight     =   540
         ScaleWidth      =   1080
         TabIndex        =   100
         Top             =   3540
         Visible         =   0   'False
         Width           =   1110
         Begin VB.CommandButton cmdDepPicMode 
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   8.25
               Charset         =   161
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   111
            ToolTipText     =   "Zoom/Reset Icon Size"
            Top             =   0
            Width           =   255
         End
         Begin VB.Label lblDepPicText 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Alert Dep"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   104
            Top             =   -15
            Width           =   810
         End
         Begin VB.Label lblDepPicCount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "123"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   510
            TabIndex        =   103
            Top             =   270
            Width           =   570
         End
         Begin VB.Label lblDepPicType 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "A"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   102
            Top             =   270
            Width           =   255
         End
         Begin VB.Image picDepPic 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Index           =   0
            Left            =   -15
            Picture         =   "StatusChart.frx":40F2
            Stretch         =   -1  'True
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblDepPicBack 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   -15
            TabIndex        =   101
            Top             =   270
            Width           =   270
         End
      End
      Begin VB.PictureBox ArrPicPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   570
         Index           =   0
         Left            =   30
         ScaleHeight     =   540
         ScaleWidth      =   1080
         TabIndex        =   95
         Top             =   2850
         Visible         =   0   'False
         Width           =   1110
         Begin VB.CommandButton cmdArrPicMode 
            BeginProperty Font 
               Name            =   "Arial Black"
               Size            =   8.25
               Charset         =   161
               Weight          =   900
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   110
            ToolTipText     =   "Zoom/Reset Icon Size"
            Top             =   0
            Width           =   255
         End
         Begin VB.Image picArrPic 
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Height          =   270
            Index           =   0
            Left            =   -15
            Picture         =   "StatusChart.frx":4534
            Stretch         =   -1  'True
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblArrPicBack 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   -15
            TabIndex        =   99
            Top             =   270
            Width           =   270
         End
         Begin VB.Label lblArrPicType 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "A"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   98
            Top             =   270
            Width           =   255
         End
         Begin VB.Label lblArrPicCount 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "123"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   510
            TabIndex        =   97
            Top             =   270
            Width           =   570
         End
         Begin VB.Label lblArrPicText 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Alert Arr"
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   270
            TabIndex        =   96
            Top             =   -15
            Width           =   810
         End
      End
      Begin VB.CheckBox chkTitle 
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   8.25
            Charset         =   161
            Weight          =   900
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   30
         MaskColor       =   &H8000000F&
         Style           =   1  'Graphical
         TabIndex        =   94
         Tag             =   "-1"
         Top             =   1500
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Frame fraYButtonPanel 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   990
         Index           =   0
         Left            =   60
         TabIndex        =   3
         Top             =   390
         Width           =   1035
         Begin VB.CheckBox chkAppl 
            Caption         =   "Close"
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   8
            Tag             =   "CLOSE"
            Top             =   0
            Width           =   1035
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "UTC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   7
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to UTC Times"
            Top             =   330
            Width           =   495
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "LOC"
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   6
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to Local Times"
            Top             =   330
            Width           =   525
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "RS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   5
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Right Scale ON/OFF"
            Top             =   660
            Width           =   495
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "LS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   4
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   660
            Width           =   525
         End
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "Top"
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox chkTerminate 
         Enabled         =   0   'False
         Height          =   315
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.Label lblTxtBackLight 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   270
         Index           =   0
         Left            =   15
         TabIndex        =   113
         Top             =   1785
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.Label lblTxtText 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Index           =   0
         Left            =   330
         TabIndex        =   93
         Top             =   1470
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Label lblTxtShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ShadoW"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Index           =   0
         Left            =   480
         TabIndex        =   92
         Top             =   1470
         Visible         =   0   'False
         Width           =   720
      End
      Begin VB.Line linLine 
         BorderColor     =   &H00FFFFFF&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   1440
         Y2              =   1440
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   9
      Top             =   7275
      Width           =   13350
      _ExtentX        =   23548
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15187
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1429
            MinWidth        =   1058
            TextSave        =   "12/4/2009"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1244
            MinWidth        =   1058
            TextSave        =   "3:02 PM"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   953
            MinWidth        =   706
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   794
            MinWidth        =   706
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   714
            MinWidth        =   706
            TextSave        =   "INS"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   6
      Left            =   1980
      Picture         =   "StatusChart.frx":4976
      Top             =   6510
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   5
      Left            =   1680
      Picture         =   "StatusChart.frx":4AC0
      Top             =   6510
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   4
      Left            =   1380
      Picture         =   "StatusChart.frx":4E4A
      Top             =   6510
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   3
      Left            =   1080
      Picture         =   "StatusChart.frx":51D4
      Top             =   6510
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   2
      Left            =   780
      Picture         =   "StatusChart.frx":555E
      Top             =   6510
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   1
      Left            =   480
      Picture         =   "StatusChart.frx":56A8
      Top             =   6510
      Width           =   240
   End
   Begin VB.Image picAftFtyp 
      Height          =   240
      Index           =   0
      Left            =   180
      Picture         =   "StatusChart.frx":57F2
      Top             =   6510
      Width           =   240
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   4
      Left            =   1350
      Picture         =   "StatusChart.frx":593C
      Stretch         =   -1  'True
      Top             =   6060
      Width           =   270
   End
   Begin VB.Image picApplIcon 
      Height          =   240
      Left            =   1680
      Picture         =   "StatusChart.frx":5A86
      Top             =   5760
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   3
      Left            =   1050
      Picture         =   "StatusChart.frx":5BD0
      Stretch         =   -1  'True
      Top             =   6060
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   750
      Picture         =   "StatusChart.frx":615A
      Stretch         =   -1  'True
      Top             =   6060
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   450
      Picture         =   "StatusChart.frx":659C
      Stretch         =   -1  'True
      Top             =   6060
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   150
      Picture         =   "StatusChart.frx":69DE
      Stretch         =   -1  'True
      Top             =   6060
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   810
      Picture         =   "StatusChart.frx":6D68
      Stretch         =   -1  'True
      Top             =   5730
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   480
      Picture         =   "StatusChart.frx":71AA
      Stretch         =   -1  'True
      Top             =   5730
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   150
      Picture         =   "StatusChart.frx":7534
      Stretch         =   -1  'True
      Top             =   5730
      Width           =   270
   End
End
Attribute VB_Name = "StatusChart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MainAreaColor As Integer
Dim DataAreaColor As Integer
Dim WorkAreaColor As Integer
Dim ChartBarBackColor As Long
Dim MinimumTop As Long
Dim MinimumLeft As Long
Dim MaxChartHeight As Long
Dim MaxChartWidth As Long
Dim MaxBarCnt As Integer
Dim CurBarMax As Integer
Dim CurChartHeight As Long
Dim CurChartWidth As Long
Dim CurScrollHeight As Long
Dim CurScrollWidth As Long
Dim CurBarIdx As Integer
Dim PrvBarIdx As Integer
Dim DataWindowBeginUtc As String
Dim DataWindowBeginLoc As String
Dim ServerUtcTimeStr As String
Dim ServerLocTimeStr As String
Dim ChartName As String
Dim LastLocTimeCheck
Dim LastUtcTimeCheck
Dim ServerUtcTimeVal
Dim TimeScaleUtcBegin
Dim ShowAllIcons As Boolean
Dim ObjectIsMoving As Boolean
Dim DontScroll As Boolean
Dim DontResyncScroll As Boolean
Dim DontAdjustOverview As Boolean
Public FullBarHeight As Long
Public FirstRotLine As Long
Public LastRotLine As Long
Public TotalRotLines As Long
Public AvailableBars As Long
Public VisibleBars As Long
Public LoadedBars As Long
Public MinuteWidth As Long
Public MinDepWidth As Long
Public MaxDepWidth As Long
Public MinArrWidth As Long
Public MaxArrWidth As Long
Public Sub ReSyncCursor(RotLine As Long, LeftPos As Long)
    Dim TopLine As Long
    Dim BotLine As Long
    Dim HitLine As Long
    Dim CurIdx As Integer
    Dim ReLoad As Boolean
    If RotLine >= 0 Then
        ReLoad = False
        'First check if we must reload flights
        'The desired line should be in the middle of the chart view.
        TopLine = RotLine - (VisibleBars \ 2)
        BotLine = TopLine + VisibleBars - 1
        If TopLine < FirstRotLine Then ReLoad = True
        If BotLine >= LoadedBars Then ReLoad = True
        If ReLoad = True Then
            'The chart view area should be in the middle of loaded flights.
            ReLoad = False
            TopLine = RotLine - (AvailableBars \ 2)
            If TopLine < 0 Then TopLine = 0
            BotLine = TopLine + AvailableBars - 1
            If BotLine >= TotalRotLines Then
                TopLine = TotalRotLines - AvailableBars
                If TopLine < 0 Then TopLine = 0
            End If
            If TopLine <> FirstRotLine Then ReLoad = True
        End If
        If ReLoad = True Then
            DontAdjustOverview = True
            GetFlightRotations TopLine, False
            DontAdjustOverview = False
        End If
        
        'Now locate the desired flight and area
        HitLine = RotLine - FirstRotLine
        TopLine = RotLine - (VisibleBars \ 2)
        If TopLine < 0 Then TopLine = 0
        SynchronizeChartArea TopLine, LeftPos, False
        CurIdx = CInt(HitLine)
    Else
        CurIdx = CurBarIdx
    End If
    HighlightCurrentBar CurIdx, ChartName
End Sub
Public Sub SynchronizeChartArea(TopRotLine As Long, LeftPos As Long, ReloadFlights As Boolean)
    Dim TopLine As Long
    Dim BotLine As Long
    Dim iBar As Integer
    If LeftPos <> -1 Then
        TimeScale(0).Left = LeftPos
        TimeScale(0).Refresh
        HScrollArea(0).Left = LeftPos
        HScrollArea(0).Refresh
        AdjustScrollValues
    End If
    If TopRotLine >= 0 Then
        If Not ReloadFlights Then
            iBar = CInt(TopRotLine - FirstRotLine)
            If (iBar >= 0) And (iBar <= VScroll2(0).Max) Then
                DontResyncScroll = True
                VScroll1(0).Value = iBar
                VScroll2(0).Value = iBar
                DontResyncScroll = False
            End If
        Else
            TopLine = TopRotLine
            GetFlightRotations TopLine, False
        End If
    End If
End Sub
Public Sub ScrollSyncExtern(RotLine As Long)
    Dim tmpGidx As String
    Dim tmpBest As String
    Dim tmpBestPos As Long
    Dim NewScroll As Integer
    Dim tmpScrVal As Integer
    Dim PosIsValid As Boolean
    Dim CedaTime
    If RotLine >= 0 Then
        tmpGidx = TabRotFlightsTab.GetFieldValue(RotLine, "GOCX")
        If tmpGidx <> "------" Then
            tmpScrVal = Val(tmpGidx)
            If tmpScrVal > VScroll2(0).Max Then tmpScrVal = VScroll2(0).Max
            VScroll2(0).Value = tmpScrVal
            tmpBest = TabRotFlightsTab.GetFieldValue(RotLine, "BEST")
            PosIsValid = GetTimeScalePos(tmpBest, CedaTime, tmpBestPos)
            tmpBestPos = tmpBestPos \ MinuteWidth
            If tmpBestPos > 3600 Then tmpBestPos = 3600
            If tmpBestPos < 0 Then tmpBestPos = 0
            tmpBestPos = tmpBestPos - (TimePanel(0).Width \ MinuteWidth \ 2)
            If tmpBestPos < 0 Then tmpBestPos = 0
            NewScroll = CInt(tmpBestPos)
            HScroll2(0).Value = NewScroll
            'tmpScrVal = Val(tmpGidx)
            'VScroll2(0).Value = tmpScrVal
            HighlightCurrentBar tmpScrVal, "EXT"
            If SyncOriginator = "EXT" Then SyncOriginator = ""
        End If
    End If
End Sub
Public Function GetFlightInfo(RotLine As Long, FlightAdid As String, InfoType As Integer) As String
    Dim tmpInfo As String
    Dim tmpData As String
    Dim iBar As Integer
    tmpInfo = ""
    If (RotLine >= FirstRotLine) And (RotLine <= LastRotLine) Then
        iBar = CInt(RotLine - FirstRotLine)
        Select Case FlightAdid
            Case "A"
                Select Case InfoType
                    Case 0
                        tmpData = lblArrFlno(iBar).Caption
                        If tmpData <> "" Then
                            tmpInfo = "Arrival: "
                            tmpInfo = tmpInfo & tmpData & " "
                            tmpData = lblArrStoa(iBar).Caption
                            tmpInfo = tmpInfo & "(STA " & tmpData & ") "
                        End If
                    Case Else
                End Select
            Case "D"
                Select Case InfoType
                    Case 0
                        tmpData = lblDepFlno(iBar).Caption
                        If tmpData <> "" Then
                            tmpInfo = "Departure: "
                            tmpInfo = tmpInfo & tmpData & " "
                            tmpData = lblDepStod(iBar).Caption
                            tmpInfo = tmpInfo & "(STD " & tmpData & ") "
                        End If
                    Case Else
                End Select
            Case Else
                tmpInfo = "Internal Error (ADID)"
        End Select
    Else
    End If
    GetFlightInfo = tmpInfo
End Function
Private Sub AdjustScrollValues()
    Dim ChkLeft As Long
    Dim ChkWidth As Long
    Dim ChkTop As Long
    Dim ChkHeight As Long
    Dim ScrollPos As Integer
    DontScroll = True
    ChkLeft = (-HScrollArea(0).Left) \ MinuteWidth
    ChkWidth = (HScrollArea(0).Width - WorkArea(0).Width) \ MinuteWidth
    If ChkWidth > 0 Then
        ScrollPos = CInt(ChkLeft)
        If ScrollPos < 0 Then ScrollPos = 0
        If ScrollPos > HScroll2(0).Max Then ScrollPos = HScroll2(0).Max
        HScroll2(0).Value = ScrollPos
    End If
    DontScroll = False
End Sub
Private Sub ArrFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub HighlightCurrentBar(Index As Integer, KeyOrig As String)
    Dim LineNo As Long
    Dim CurGocx As String
    Dim iCol As Integer
    If SyncOriginator = "" Then SyncOriginator = KeyOrig
    CurBarIdx = Index
    If (PrvBarIdx <> CurBarIdx) And (PrvBarIdx >= 0) Then
        ChartLine2(PrvBarIdx).Cls
        ChartLine2(PrvBarIdx).AutoRedraw = False
        ChartLine2(PrvBarIdx).Refresh
        HighlightStatusPics PrvBarIdx, False
    End If
    iCol = Val(lblLeftBarCover(CurBarIdx).Tag)
    ChartLine2(CurBarIdx).AutoRedraw = True
    DrawBackGround ChartLine2(CurBarIdx), iCol, True, True
    ChartLine2(CurBarIdx).Refresh
    HighlightStatusPics CurBarIdx, True
    AdjustOverviewPanel
    If SyncOriginator = ChartName Then
        'This is a QuickHack (But it works fine)
        CurGocx = Right("000000" & CStr(Index), 6)
        MarkLookupLines TabRotFlightsTab, "GOCX", CurGocx, True, "DRGN,AFLT,DFLT", True, "'S1'", "DecoMarkerLB"
        LineNo = TabRotFlightsTab.GetCurrentSelected
        MainDialog.RotationData_RowSelectionChanged LineNo, True
    End If
    PrvBarIdx = Index
    If SyncOriginator = KeyOrig Then SyncOriginator = ""
End Sub
Private Sub HighlightStatusPics(Index As Integer, SetFlag As Boolean)
    SetArrPicHighLight picArr1(Index).Tag, SetFlag
    SetArrPicHighLight picArr2(Index).Tag, SetFlag
    SetArrPicHighLight picArr3(Index).Tag, SetFlag
    SetArrPicHighLight picArr4(Index).Tag, SetFlag
    SetDepPicHighLight picDep1(Index).Tag, SetFlag
    SetDepPicHighLight picDep2(Index).Tag, SetFlag
    SetDepPicHighLight picDep3(Index).Tag, SetFlag
    SetDepPicHighLight picDep4(Index).Tag, SetFlag
End Sub
Private Sub SetArrPicHighLight(PicTag As String, SetFlag As Boolean)
    Dim tmpIdx As String
    Dim jIdx As Integer
    tmpIdx = GetItem(PicTag, 1, ",")
    If tmpIdx <> "" Then
        jIdx = Val(tmpIdx)
        If jIdx >= 0 Then
            If SetFlag Then
                lblArrPicText(jIdx).BackColor = vbWhite
                lblArrPicType(jIdx).BackColor = vbWhite
                lblArrPicCount(jIdx).BackColor = vbWhite
            Else
                lblArrPicText(jIdx).BackColor = LightGrey
                lblArrPicType(jIdx).BackColor = LightGrey
                lblArrPicCount(jIdx).BackColor = LightGrey
            End If
            lblArrPicText(jIdx).Refresh
        End If
    End If
End Sub
Private Sub SetDepPicHighLight(PicTag As String, SetFlag As Boolean)
    Dim tmpIdx As String
    Dim jIdx As Integer
    tmpIdx = GetItem(PicTag, 1, ",")
    If tmpIdx <> "" Then
        jIdx = Val(tmpIdx)
        If jIdx >= 0 Then
            If SetFlag Then
                lblDepPicText(jIdx).BackColor = vbWhite
                lblDepPicType(jIdx).BackColor = vbWhite
                lblDepPicCount(jIdx).BackColor = vbWhite
            Else
                lblDepPicText(jIdx).BackColor = LightGrey
                lblDepPicType(jIdx).BackColor = LightGrey
                lblDepPicCount(jIdx).BackColor = LightGrey
            End If
            lblDepPicText(jIdx).Refresh
        End If
    End If
End Sub

Private Sub ChartLine1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ChartLine2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    HighlightCurrentBar Index, ChartName
End Sub


Private Sub ChartLine3_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then
        chkAppl(Index).BackColor = LightGreen
        Select Case Index
            Case 0
            'REMOVED
                GocChartOverview.Hide
                Me.Hide
                MainDialog.PushWorkButton "GOCC_CHART", False
                chkAppl(Index).Value = 0
            Case Else
        End Select
    Else
        chkAppl(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkSelDeco_Click(Index As Integer)
    If chkSelDeco(Index).Value = 1 Then
        chkSelDeco(Index).BackColor = LightGreen
        If Index = 0 Then
            ToggleLeftScaleBars True
        Else
            ToggleRightScaleBars True
        End If
    Else
        chkSelDeco(Index).BackColor = vbButtonFace
        If Index = 0 Then
            ToggleLeftScaleBars False
        Else
            ToggleRightScaleBars False
        End If
    End If
End Sub
Private Sub ToggleLeftScaleBars(ShowLeftScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To ArrFlight.UBound
        ArrFlight(i).Visible = False
        If ShowLeftScale = True Then
            Set ArrFlight(i).Container = ChartLine1(i)
            ArrFlight(i).Left = 15
            ArrFlight(i).Visible = lblArrBar(i).Visible
        Else
            Set ArrFlight(i).Container = ChartLine2(i)
            NewLeft = lblArrBar(i).Left - ArrFlight(i).Width - 30
            ArrFlight(i).Left = NewLeft
            ArrFlight(i).Visible = lblArrBar(i).Visible
        End If
    Next
    Screen.MousePointer = 0
    LeftScale(0).Visible = ShowLeftScale
    Form_Resize
End Sub
Private Sub ToggleRightScaleBars(ShowRightScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To DepFlight.UBound
        DepFlight(i).Visible = False
        If ShowRightScale = True Then
            Set DepFlight(i).Container = ChartLine3(i)
            DepFlight(i).Left = 15
            DepFlight(i).Visible = lblDepBar(i).Visible
        Else
            Set DepFlight(i).Container = ChartLine2(i)
            NewLeft = lblDepBar(i).Left + lblDepBar(i).Width + 30
            DepFlight(i).Left = NewLeft
            DepFlight(i).Visible = lblDepBar(i).Visible
        End If
    Next
    Screen.MousePointer = 0
    RightScale(0).Visible = ShowRightScale
    Form_Resize
End Sub

Private Sub chkSwitch_Click(Index As Integer)
    Dim i As Integer
    If chkSwitch(Index).Value = 1 Then
        chkSwitch(Index).BackColor = LightGreen
        Select Case Index
            Case 0, 6
            Case 1, 7
                chkSwitch(1).Value = 1
                chkSwitch(7).Value = 1
            Case 2, 8
                If Index = 2 Then
                    chkSwitch(8).Value = 0
                    ZoomTimeScale 0, True
                Else
                    chkSwitch(2).Value = 0
                    ZoomTimeScale 1, True
                End If
            Case 3, 9
            Case 4, 10
                If Index = 4 Then
                    If VertScroll1(0).Visible = True Then
                        VertScroll1(0).Visible = False
                        TimeScroll1(0).Visible = False
                    Else
                        VertScroll1(0).Visible = True
                        TimeScroll1(0).Visible = True
                    End If
                Else
                    If VertScroll2(0).Visible = True Then
                        VertScroll2(0).Visible = False
                        TimeScroll2(0).Visible = False
                    Else
                        VertScroll2(0).Visible = True
                        TimeScroll2(0).Visible = True
                    End If
                End If
                chkSwitch(Index).Value = 0
                Form_Resize
            Case 5, 11
            Case Else
        End Select
    Else
        Select Case Index
            Case 0, 6
            Case 1, 7
                chkSwitch(1).Value = 0
                chkSwitch(7).Value = 0
            Case 2, 8
                ZoomTimeScale 0, False
            Case 3, 9
            Case 4, 10
            Case 5, 11
            Case Else
        End Select
        chkSwitch(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ZoomTimeScale(SetAlign As Integer, ShowZoom As Boolean)
    fraZoomOptions.Left = 0
    If SetAlign = 1 Then fraZoomOptions.Left = TimePanel(0).ScaleWidth - fraZoomOptions.Width
    fraZoomOptions.Visible = ShowZoom
End Sub
Private Sub chkTitle_Click(Index As Integer)
    Dim tmpTag As String
    tmpTag = chkTitle(Index).Tag
    If chkTitle(Index).Value = 1 Then
        Select Case tmpTag
            'Case "TASKS"
            '    ToggleTaskBars True, "AD"
            Case "AFSA"
                If chkTitle(Index).Caption = "X" Then
                    chkTitle(Index).BackColor = vbButtonFace
                    ToggleTaskBars False, "A"
                    chkTitle(Index).Caption = "="
                Else
                    chkTitle(Index).BackColor = LightYellow
                    ToggleTaskBars True, "A"
                    chkTitle(Index).Caption = "X"
                End If
                chkTitle(Index).Value = 0
            Case "AFSD"
                If chkTitle(Index).Caption = "X" Then
                    chkTitle(Index).BackColor = vbButtonFace
                    ToggleTaskBars False, "D"
                    chkTitle(Index).Caption = "="
                Else
                    chkTitle(Index).BackColor = LightGreen
                    ToggleTaskBars True, "D"
                    chkTitle(Index).Caption = "X"
                End If
                chkTitle(Index).Value = 0
            Case Else
                chkTitle(Index).Value = 0
        End Select
    Else
        'Select Case tmpTag
        '    Case "TASKS"
        '        ToggleTaskBars False, "AD"
        '    Case "AFSA"
        '        ToggleTaskBars False, "A"
        '    Case "AFSD"
        '        ToggleTaskBars False, "D"
        '    Case Else
        'End Select
    End If
End Sub
Private Sub ToggleTaskBars(SetFlag As Boolean, ForWhat As String)
    Dim iBar As Integer
    For iBar = 0 To ChartLine2.UBound
        If (ForWhat = "A") Or (ForWhat = "AD") Then
            If lblArrBar(iBar).Visible = True Then
                lblArrJob1(iBar).Visible = SetFlag
                lblArrJob2(iBar).Visible = SetFlag
            End If
        End If
        If (ForWhat = "D") Or (ForWhat = "AD") Then
            If lblDepBar(iBar).Visible = True Then
                lblDepJob1(iBar).Visible = SetFlag
                lblDepJob2(iBar).Visible = SetFlag
            End If
        End If
    Next
End Sub
Private Sub chkUtc_Click(Index As Integer)
    If chkUtc(Index).Value = 1 Then
        chkUtc(Index).BackColor = LightGreen
        If Index = 0 Then chkUtc(1).Value = 0 Else chkUtc(0).Value = 0
        CreateTimeScaleArea TimeScale(0)
        TimeScaleTimer_Timer
        If Index = 1 Then
            ToggleFlightBarTimes UtcTimeDiff
        Else
            ToggleFlightBarTimes 0
        End If
    Else
        If Index = 0 Then chkUtc(1).Value = 1 Else chkUtc(0).Value = 1
        chkUtc(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub CreateTimeScaleArea(TimeScale As PictureBox)
    Dim iMin As Integer
    Dim iMin60 As Integer
    Dim iHour As Integer
    Dim iFirstHour As Integer
    Dim MaxScale As Integer
    Dim iBar As Integer
    Dim lWidth As Long
    Dim LeftSpace As Long
    Dim RightSpace As Long
    Dim LeftPos As Long
    Dim LeftUtcOffset As Long
    Dim LeftOff1 As Long
    Dim LeftOff2 As Long
    Dim TopPos As Long
    Dim tmpVal As Long
    Dim tmpText As String
    
    Dim ScaleBottom As Long
    Dim X0A As Long
    Dim X0B As Long
    Dim Y0A As Long
    Dim Y1A As Long
    Dim Y2A As Long
    Dim Y3A As Long
    Dim Y4A As Long
    Dim Y0B As Long
    
    Dim XD As Long
    Dim YD As Long
    
    Dim DisplayDateTime As String
    Dim CurDate As String
    Dim CurSsimDate As String
    Dim CurUtcNowTime
    Dim CurLocNowTime
    Dim CurTimeVal
    Dim i As Integer
    Dim ShowDate As Boolean
    Dim iCheckDate As Integer
    
    If DataWindowBeginUtc = "" Then
        LastLocTimeCheck = Now
        'Create a Default UTC Time
        CurUtcNowTime = DateAdd("n", -UtcTimeDiff, LastLocTimeCheck)
        LastUtcTimeCheck = CurUtcNowTime
        'Create a Default Data Window Begin
        CurUtcNowTime = DateAdd("h", -2, CurUtcNowTime)
        DataWindowBeginUtc = Format(CurUtcNowTime, "YYYYMMDDhhmmss")
    End If
    
    If ServerUtcTimeStr = "" Then
        'Create a Default Server Reference Time
        ServerUtcTimeVal = LastUtcTimeCheck
        ServerUtcTimeStr = Format(ServerUtcTimeVal, "YYYYMMDDhhmmss")
    End If
    
    If DataFilterVpfr <> "" Then DataWindowBeginUtc = DataFilterVpfr
    'We always start at full hours in Utc
    Mid(DataWindowBeginUtc, 11) = "0000"
    lblBgnTime(0).Tag = DataWindowBeginUtc
    
    CurUtcNowTime = CedaFullDateToVb(DataWindowBeginUtc)
    TimeScaleUtcBegin = CurUtcNowTime
    CurLocNowTime = DateAdd("n", UtcTimeDiff, CurUtcNowTime)
    DataWindowBeginLoc = Format(CurLocNowTime, "YYYYMMDDhhmmss")
    
    If chkUtc(0).Value = 1 Then
        DisplayDateTime = DataWindowBeginUtc
    Else
        DisplayDateTime = DataWindowBeginLoc
    End If
    
    lblBgnTime(0).Caption = DisplayDateTime
    
    iHour = Val(Mid(DisplayDateTime, 9, 2)) - 1
    CurDate = Left(DisplayDateTime, 8)
    CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
    lblBgnTime(0).Caption = CurSsimDate
    iFirstHour = iHour
    
    ScaleBottom = 510
    Y0B = ScaleBottom
    Y0A = Y0B - 60
    Y1A = Y0B - 120
    Y2A = Y0B - 180
    Y3A = Y0B - 240
    Y4A = Y0B - 300
    
    lWidth = lblOneMinuteWidth.Width
    MinuteWidth = lWidth
    LeftSpace = ArrFlight(0).Width
    RightSpace = DepFlight(0).Width
    MaxScale = CInt((245745 - (LeftSpace + RightSpace)) / MinuteWidth)
    MaxScale = (MaxScale \ 60) * 60 'Set amount to full hours
    'LeftOff1 = (lblHour(0).Width / 2)
    'LeftOff2 = (lblDate(0).Width / 2)
    
    'TimeScale.Cls
    tmpVal = TimeScale.Height
    TimeScale.Height = tmpVal * 6
    DrawBackGround TimeScale, 7, True, True
    TimeScale.Height = tmpVal
    
    iMin60 = -1
    Select Case lWidth
        Case 15
            iCheckDate = 12
        Case 30
            iCheckDate = 6
        Case 45
            iCheckDate = 3
        Case 60
            iCheckDate = 1
        Case 75 To 120
            iCheckDate = 1
        Case Else
            iCheckDate = 1
    End Select
    
    ShowDate = True
    LeftPos = LeftSpace
    TimeRange(0).Top = 0
    TimeRange(0).Left = LeftPos
    TimeRange(0).Width = 15
    TimeRange(0).Height = TimeScale.ScaleHeight
    'TimeRange(0).Visible = True
    
    For iMin = 0 To MaxScale
        X0A = LeftPos - 15
        X0B = LeftPos + 15
        If iMin Mod 60 = 0 Then
            TimeScale.Line (X0A - 15, Y4A - 15)-(X0B, Y0B), vbWhite, BF
            TimeScale.Line (X0A, Y4A)-(X0B, Y0B), vbBlack, BF
            iMin60 = iMin60 + 1
            iHour = iHour + 1
            If iHour > 23 Then
                iHour = 0
                CurDate = CedaDateAdd(CurDate, 1)
                CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
                ShowDate = True
            End If
            tmpText = Right("00" & CStr(iHour), 2) & ":00"
            LeftOff1 = (TextWidth(tmpText) \ 2) - 30
            PrintBackGroundText TimeScale, 8, X0A - LeftOff1, 0, tmpText, XD, YD
            If iHour Mod iCheckDate = 0 Then ShowDate = True
            If ShowDate = True Then
                LeftOff1 = (TextWidth(CurSsimDate) \ 2) - 30
                PrintBackGroundText TimeScale, 8, X0A - LeftOff1, Y0B + 45, CurSsimDate, XD, YD
                ShowDate = False
            End If
        ElseIf iMin Mod 30 = 0 Then
            If lWidth > 15 Then
                TimeScale.Line (X0A - 15, Y3A - 15)-(X0B, Y0B), vbWhite, BF
                TimeScale.Line (X0A, Y3A)-(X0B, Y0B), vbBlack, BF
            Else
                TimeScale.Line (X0A - 15, Y2A - 15)-(X0B, Y0B), vbWhite, BF
                TimeScale.Line (X0A, Y2A)-(X0B, Y0B), vbBlack, BF
            End If
        ElseIf iMin Mod 10 = 0 Then
            If lWidth > 15 Then
                TimeScale.Line (X0A - 15, Y2A - 15)-(X0B, Y0B), vbWhite, BF
                TimeScale.Line (X0A, Y2A)-(X0B, Y0B), vbBlack, BF
            Else
                TimeScale.Line (X0A - 15, Y1A - 15)-(X0B, Y0B), vbWhite, BF
                TimeScale.Line (X0A, Y1A)-(X0B, Y0B), vbBlack, BF
            End If
        ElseIf iMin Mod 5 = 0 Then
            If lWidth > 15 Then
                TimeScale.Line (X0A - 15, Y1A - 15)-(X0B, Y0B), vbWhite, BF
                TimeScale.Line (X0A, Y1A)-(X0B, Y0B), vbBlack, BF
            Else
                TimeScale.Line (X0A, Y0A - 15)-(X0B - 30, Y0B), vbWhite, BF
                TimeScale.Line (X0A + 15, Y0A)-(X0B - 15, Y0B), vbBlack, BF
            End If
        ElseIf lWidth > 30 Then
            TimeScale.Line (LeftPos - 15, Y0A - 15)-(LeftPos, Y0B), vbWhite, BF
            TimeScale.Line (LeftPos, Y0A)-(LeftPos, Y0B), vbBlack, BF
        End If
        LeftPos = LeftPos + lWidth
    Next
    VScrollArea(0).Refresh
    Me.Refresh
    
    TimeRange(1).Top = 0
    TimeRange(1).Left = LeftPos - lWidth
    TimeRange(1).Width = 15
    TimeRange(1).Height = TimeScale.ScaleHeight
    'TimeRange(1).Visible = True
    
    CurChartWidth = LeftPos + RightSpace
    TimeScale.Width = CurChartWidth
    'Get the real value (VB Restriction)
    CurChartWidth = TimeScale.Width
    HScroll2(0).Max = MaxScale
    
    If DataFilterVpfr <> "" Then
        CurTimeVal = CedaFullDateToVb(DataFilterVpfr)
        LeftUtcOffset = DateDiff("n", TimeScaleUtcBegin, CurTimeVal)
        LeftPos = LeftSpace + (LeftUtcOffset * lWidth)
        TimeRange(2).Top = 0
        TimeRange(2).Left = LeftPos
        TimeRange(2).Width = 15
        TimeRange(2).Height = HScrollArea(0).Height
        TimeRange(2).Visible = True
        icoZoomRange(0).Left = LeftPos - 180
        icoZoomRange(0).Visible = True
        
        GetTimeScalePos DataFilterVpto, CurTimeVal, LeftPos
        LeftPos = LeftPos + MinuteWidth
        TimeRange(3).Top = 0
        TimeRange(3).Left = LeftPos
        TimeRange(3).Width = 15
        TimeRange(3).Height = HScrollArea(0).Height
        TimeRange(3).Visible = True
        icoZoomRange(1).Left = LeftPos - 270
        icoZoomRange(1).Visible = True
    End If
    
    AdjustRangeCover
    
    TimeScaleTimer.Enabled = True
End Sub

Public Sub CreateChartBarLayout(NeededBars As Integer)
    Dim iBar As Integer
    Dim iMin As Integer
    Dim iMax As Integer
    Dim iUse As Integer
    Dim lWidth As Long
    Dim LeftPos As Long
    Dim LeftOff As Long
    Dim TopPos As Long
    Me.MousePointer = 11
    WorkArea(0).Visible = False
    iMin = ChartLine2.UBound
    If iMin > 0 Then
        TopPos = ChartLine2(iMin).Top + ChartLine2(0).Height
        iMin = iMin + 1
    Else
        TopPos = ChartLine2(iMin).Top
    End If
    iMax = NeededBars - 1
    If iMax > MaxBarCnt Then iMax = MaxBarCnt
    If (iMax >= CurBarMax) Then
        For iBar = iMin To iMax
            If iBar > ChartLine2.UBound Then
                Load ChartLine1(iBar)
                Load ChartLine2(iBar)
                Load ChartLine3(iBar)
                Load lblArrBar(iBar)
                Load lblArrJob1(iBar)
                Load lblArrJob2(iBar)
                Load picArr1(iBar)
                Load picArr2(iBar)
                Load picArr3(iBar)
                Load picArr4(iBar)
                Load lblDepBar(iBar)
                Load lblDepJob1(iBar)
                Load lblDepJob2(iBar)
                Load picDep1(iBar)
                Load picDep2(iBar)
                Load picDep3(iBar)
                Load picDep4(iBar)
                Load lblTowBar(iBar)
                Load lblLeftBarCover(iBar)
                Load lblRightBarCover(iBar)
                Load picSortTime(iBar)
                
                Set ChartLine1(iBar).Container = LeftScale(0)
                Set ChartLine2(iBar).Container = HScrollArea(0)
                Set ChartLine3(iBar).Container = RightScale(0)
                
                Set lblArrBar(iBar).Container = ChartLine2(iBar)
                Set lblArrJob1(iBar).Container = ChartLine2(iBar)
                Set lblArrJob2(iBar).Container = ChartLine2(iBar)
                Set picArr1(iBar).Container = ChartLine2(iBar)
                Set picArr2(iBar).Container = ChartLine2(iBar)
                Set picArr3(iBar).Container = ChartLine2(iBar)
                Set picArr4(iBar).Container = ChartLine2(iBar)
                Set lblDepBar(iBar).Container = ChartLine2(iBar)
                Set lblDepJob1(iBar).Container = ChartLine2(iBar)
                Set lblDepJob2(iBar).Container = ChartLine2(iBar)
                Set picDep1(iBar).Container = ChartLine2(iBar)
                Set picDep2(iBar).Container = ChartLine2(iBar)
                Set picDep3(iBar).Container = ChartLine2(iBar)
                Set picDep4(iBar).Container = ChartLine2(iBar)
                Set lblTowBar(iBar).Container = ChartLine2(iBar)
                Set lblLeftBarCover(iBar).Container = ChartLine2(iBar)
                Set lblRightBarCover(iBar).Container = ChartLine2(iBar)
                Set picSortTime(iBar).Container = ChartLine2(iBar)
                'Set picSortTime(iBar).Container = HScrollArea(0)
            End If
            ChartLine1(iBar).Top = TopPos
            ChartLine2(iBar).Top = TopPos
            ChartLine3(iBar).Top = TopPos
            'picSortTime(iBar).Top = TopPos
            
            lblLeftBarCover(iBar).Left = -15
            lblLeftBarCover(iBar).Top = -15
            lblLeftBarCover(iBar).Height = ChartLine2(iBar).ScaleHeight + 30
            lblLeftBarCover(iBar).Width = TimeRange(2).Left + 30
            lblLeftBarCover(iBar).BackColor = &HC0C0C0
            lblLeftBarCover(iBar).Visible = True
            
            lblRightBarCover(iBar).Left = TimeRange(3).Left
            lblRightBarCover(iBar).Top = -15
            lblRightBarCover(iBar).Height = ChartLine2(iBar).ScaleHeight + 30
            lblRightBarCover(iBar).Width = CurChartWidth - TimeRange(3).Left
            lblRightBarCover(iBar).BackColor = &HC0C0C0
            lblRightBarCover(iBar).Visible = True
            
            lblArrBar(iBar).ZOrder
            lblDepBar(iBar).ZOrder
                    
            CreateArrFlightPanel iBar
            Set ArrFlight(iBar).Container = ChartLine2(iBar)
            ArrFlight(iBar).Visible = False
            
            CreateDepFlightPanel iBar
            Set DepFlight(iBar).Container = ChartLine2(iBar)
            DepFlight(iBar).Visible = False
            
            ChartLine1(iBar).Visible = True
            ChartLine2(iBar).Visible = True
            ChartLine3(iBar).Visible = True
            
            'lblArrFlno(iBar).Caption = CStr(iBar)
            
            TopPos = TopPos + ChartLine2(0).Height
        Next
        CurBarMax = iMax
    Else
        iMin = NeededBars
        iMax = CurBarMax
        CurBarMax = iMin - 1
    End If
    FullBarHeight = ChartLine2(0).Height
    TopPos = ChartLine2(CurBarMax).Top + ChartLine2(0).Height
    CurChartHeight = TopPos + 30
    VScrollArea(0).Height = CurChartHeight
    HScrollArea(0).Height = CurChartHeight
    LeftScale(0).Height = CurChartHeight
    RightScale(0).Height = CurChartHeight
    NowTime(1).Height = CurChartHeight
    NowTime(1).ZOrder
    RangeLine(0).Height = CurChartHeight
    RangeLine(0).ZOrder
    RangeLine(1).Height = CurChartHeight
    RangeLine(1).ZOrder
    TopPos = VScrollArea(0).Height
    VScroll1(0).Max = CurBarMax
    VScroll2(0).Max = CurBarMax
    WorkArea(0).Visible = True
    Me.MousePointer = 0
End Sub
Private Sub AdjustRangeCover()
    Dim iBar As Integer
    On Error Resume Next
    For iBar = 0 To lblLeftBarCover.UBound
        lblLeftBarCover(iBar).Width = TimeRange(2).Left + 30
        lblRightBarCover(iBar).Left = TimeRange(3).Left
        lblRightBarCover(iBar).Width = CurChartWidth - TimeRange(3).Left
    Next
End Sub
Private Sub CreateArrFlightPanel(iBar As Integer)
    If iBar > ArrFlight.UBound Then
        Load ArrFlight(iBar)
        Load picArrStat(iBar)
        Load lblArrFtyp(iBar)
        Load lblArrFlno(iBar)
        Load lblArrFlti(iBar)
        Load lblArrOrg3(iBar)
        Load lblArrVia3(iBar)
        Load lblArrStoa(iBar)
        Load lblArrEtai(iBar)
        Load lblArrOnbl(iBar)
        Load lblArrPsta(iBar)
        Load lblArrGta1(iBar)
        Load lblArrRegn(iBar)
        Load lblArrAct3(iBar)
        Set picArrStat(iBar).Container = ArrFlight(iBar)
        Set lblArrFtyp(iBar).Container = ArrFlight(iBar)
        Set lblArrFlno(iBar).Container = ArrFlight(iBar)
        Set lblArrFlti(iBar).Container = ArrFlight(iBar)
        Set lblArrOrg3(iBar).Container = ArrFlight(iBar)
        Set lblArrVia3(iBar).Container = ArrFlight(iBar)
        Set lblArrStoa(iBar).Container = ArrFlight(iBar)
        Set lblArrEtai(iBar).Container = ArrFlight(iBar)
        Set lblArrOnbl(iBar).Container = ArrFlight(iBar)
        Set lblArrPsta(iBar).Container = ArrFlight(iBar)
        Set lblArrGta1(iBar).Container = ArrFlight(iBar)
        Set lblArrRegn(iBar).Container = ArrFlight(iBar)
        Set lblArrAct3(iBar).Container = ArrFlight(iBar)
    End If
    ArrFlight(iBar).Top = 15
    'ArrFlight(iBar).Height = 330
    picArrStat(iBar).Visible = True
    lblArrFtyp(iBar).Visible = True
    lblArrFlno(iBar).Visible = True
    lblArrFlti(iBar).Visible = True
    lblArrOrg3(iBar).Visible = True
    lblArrVia3(iBar).Visible = True
    lblArrStoa(iBar).Visible = True
    lblArrEtai(iBar).Visible = True
    lblArrOnbl(iBar).Visible = True
    lblArrPsta(iBar).Visible = True
    lblArrGta1(iBar).Visible = True
    lblArrRegn(iBar).Visible = True
    lblArrAct3(iBar).Visible = True
End Sub

Private Sub CreateDepFlightPanel(iBar As Integer)
    If iBar > DepFlight.UBound Then
        Load DepFlight(iBar)
        Load picDepStat(iBar)
        Load lblDepFtyp(iBar)
        Load lblDepFlno(iBar)
        Load lblDepFlti(iBar)
        Load lblDepDes3(iBar)
        Load lblDepVia3(iBar)
        Load lblDepStod(iBar)
        Load lblDepEtdi(iBar)
        Load lblDepOfbl(iBar)
        Load lblDepPstd(iBar)
        Load lblDepGtd1(iBar)
        Load lblDepRegn(iBar)
        Load lblDepAct3(iBar)
        Set picDepStat(iBar).Container = DepFlight(iBar)
        Set lblDepFtyp(iBar).Container = DepFlight(iBar)
        Set lblDepFlno(iBar).Container = DepFlight(iBar)
        Set lblDepFlti(iBar).Container = DepFlight(iBar)
        Set lblDepDes3(iBar).Container = DepFlight(iBar)
        Set lblDepVia3(iBar).Container = DepFlight(iBar)
        Set lblDepStod(iBar).Container = DepFlight(iBar)
        Set lblDepEtdi(iBar).Container = DepFlight(iBar)
        Set lblDepOfbl(iBar).Container = DepFlight(iBar)
        Set lblDepPstd(iBar).Container = DepFlight(iBar)
        Set lblDepGtd1(iBar).Container = DepFlight(iBar)
        Set lblDepRegn(iBar).Container = DepFlight(iBar)
        Set lblDepAct3(iBar).Container = DepFlight(iBar)
    End If
    DepFlight(iBar).Top = 15
    'DepFlight(iBar).Height = 330
    picDepStat(iBar).Visible = True
    lblDepFtyp(iBar).Visible = True
    lblDepFlno(iBar).Visible = True
    lblDepFlti(iBar).Visible = True
    lblDepDes3(iBar).Visible = True
    lblDepVia3(iBar).Visible = True
    lblDepStod(iBar).Visible = True
    lblDepEtdi(iBar).Visible = True
    lblDepOfbl(iBar).Visible = True
    lblDepPstd(iBar).Visible = True
    lblDepGtd1(iBar).Visible = True
    lblDepRegn(iBar).Visible = True
    lblDepAct3(iBar).Visible = True
End Sub

Private Sub chkWork_Click(Index As Integer)
    Dim RotLine As Long
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        chkWork(Index).Refresh
        Select Case Index
            Case 0  'Refresh
                RotLine = FirstRotLine
                GetFlightRotations RotLine, False
                chkWork(Index).Value = 0
            Case 1  'Overlap
                ShowBarsOverlapped True
            Case 5  'Overview
                GocChartOverview.Show , Me
            Case 7  'Reload
                chkWork(Index).Value = 0
            Case Else
        End Select
    Else
        Select Case Index
            Case 1  'Overlap
                ShowBarsOverlapped False
            Case 5  'Overview
                GocChartOverview.Hide
            Case Else
        End Select
        chkWork(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ShowBarsOverlapped(ShowOverlap As Boolean)
    Dim ArrLeft As Long
    Dim DepLeft As Long
    Dim iBar As Integer
    Screen.MousePointer = 11
    For iBar = 0 To ChartLine2.UBound
        If (lblArrFtyp(iBar).Tag <> "") And (lblDepFtyp(iBar).Tag <> "") Then
            If ShowOverlap Then
                ArrFlight(iBar).Top = -30
                lblArrBar(iBar).Top = 0
                lblArrJob1(iBar).Top = lblArrBar(iBar).Top + 45
                lblArrJob2(iBar).Top = lblArrJob1(iBar).Top + 120
                DepFlight(iBar).Top = 60
                lblDepBar(iBar).Top = 90
                lblDepJob1(iBar).Top = lblDepBar(iBar).Top + 45
                lblDepJob2(iBar).Top = lblDepJob1(iBar).Top + 120
            Else
                ArrFlight(iBar).Top = 15
                lblArrBar(iBar).Top = 45
                lblArrJob1(iBar).Top = lblArrBar(iBar).Top + 45
                lblArrJob2(iBar).Top = lblArrJob1(iBar).Top + 120
                DepFlight(iBar).Top = 15
                lblDepBar(iBar).Top = 45
                lblDepJob1(iBar).Top = lblDepBar(iBar).Top + 45
                lblDepJob2(iBar).Top = lblDepJob1(iBar).Top + 120
            End If
            If picArr1(iBar).Height = 240 Then picArr1(iBar).Top = lblArrBar(iBar).Top + 15
            If picArr2(iBar).Height = 240 Then picArr2(iBar).Top = lblArrBar(iBar).Top + 15
            If picArr3(iBar).Height = 240 Then picArr3(iBar).Top = lblArrBar(iBar).Top + 15
            If picArr4(iBar).Height = 240 Then picArr4(iBar).Top = lblArrBar(iBar).Top + 15
            If picDep1(iBar).Height = 240 Then picDep1(iBar).Top = lblDepBar(iBar).Top + 15
            If picDep2(iBar).Height = 240 Then picDep2(iBar).Top = lblDepBar(iBar).Top + 15
            If picDep3(iBar).Height = 240 Then picDep3(iBar).Top = lblDepBar(iBar).Top + 15
            If picDep4(iBar).Height = 240 Then picDep4(iBar).Top = lblDepBar(iBar).Top + 15
        End If
    Next
    Screen.MousePointer = 0
End Sub

Private Sub cmdArrPicMode_Click(Index As Integer)
    Dim NewSize As Long
    If cmdArrPicMode(Index).Tag = "360" Then
        cmdArrPicMode(Index).Tag = "240"
    Else
        cmdArrPicMode(Index).Tag = "360"
    End If
    NewSize = Val(cmdArrPicMode(Index).Tag)
    AdjustArrIconSize Index, NewSize
End Sub

Private Sub cmdDepPicMode_Click(Index As Integer)
    Dim NewSize As Long
    If cmdDepPicMode(Index).Tag = "360" Then
        cmdDepPicMode(Index).Tag = "240"
    Else
        cmdDepPicMode(Index).Tag = "360"
    End If
    NewSize = Val(cmdDepPicMode(Index).Tag)
    AdjustDepIconSize Index, NewSize
End Sub

Private Sub cmdSyncTime_Click()
    Dim NewServerUtc As String
    Dim tmpOffset As Long
    tmpOffset = (TimePanel(0).Width / 2) - TimeScale(0).Left - TimeRange(0).Left
    tmpOffset = tmpOffset / MinuteWidth
    NewServerUtc = CedaDateTimeAdd(DataFilterVpfr, tmpOffset)
    ServerUtcTimeVal = CedaFullDateToVb(NewServerUtc)
    LastUtcTimeCheck = ServerUtcTimeVal
End Sub

Private Sub DepFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Public Sub Form_Activate()
    Static IsActivated As Boolean
    Dim tmpWidth As Long
    Dim tmpHeight As Long
    Dim tmpSize As Long
    If IsActivated = False Then
        If MainLifeStyle Then
            tmpWidth = MaxScreenWidth + 2100
            tmpHeight = Screen.Height + 2100
            ButtonPanel.Width = tmpWidth
            tmpSize = ButtonPanel.Height
            ButtonPanel.Height = tmpSize * 2
            DrawBackGround ButtonPanel, WorkAreaColor, True, True
            ButtonPanel.Height = tmpSize
            
            RightPanel.Height = tmpHeight
            DrawBackGround RightPanel, WorkAreaColor, True, True
            
            tmpSize = TimeScale(0).Height
            TimeScale(0).Height = tmpSize * 6
            DrawBackGround TimeScale(0), MainAreaColor, True, True
            TimeScale(0).Height = tmpSize
            
            TopRemark(0).Width = tmpWidth
            tmpSize = TopRemark(0).Height
            TopRemark(0).Height = tmpSize * 2
            DrawBackGround TopRemark(0), MainAreaColor, True, True
            TopRemark(0).Height = tmpSize
            
            BottomRemark(0).Width = tmpWidth
            tmpSize = BottomRemark(0).Height
            BottomRemark(0).Height = tmpSize * 2
            DrawBackGround BottomRemark(0), MainAreaColor, True, True
            BottomRemark(0).Height = tmpSize
            
            WorkArea(0).Height = tmpHeight
            WorkArea(0).Width = tmpWidth
            DrawBackGround WorkArea(0), MainAreaColor, True, True
            'DrawBackGround TopPanel, WorkAreaColor, True, True
        Else
            'lblArrRow.ForeColor = vbButtonText
            'lblDepRow.ForeColor = vbButtonText
        End If
        Me.Refresh
        Me.Top = 0
        Me.Height = Screen.Height - 300
        Me.Refresh
        IsActivated = True
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ScrollIt As Boolean
    Dim NxtIdx As Integer
    Dim TopIdx As Integer
    Dim BotIdx As Integer
    Dim LinCnt As Integer
    Dim NewVal As Integer
    If Shift = 1 Then ScrollIt = True Else ScrollIt = False
    TopIdx = VScroll2(0).Value
    LinCnt = Int(WorkArea(0).Height \ FullBarHeight)
    BotIdx = TopIdx + LinCnt - 1
    Select Case KeyCode
        Case vbKeyDown
            NxtIdx = CurBarIdx + 1
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                VScroll2(0).Value = VScroll2(0).Value + 1
            End If
            KeyCode = 0
        Case vbKeyUp
            NxtIdx = CurBarIdx - 1
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                VScroll2(0).Value = VScroll2(0).Value - 1
            End If
            KeyCode = 0
        Case vbKeyPageDown
            NxtIdx = CurBarIdx + LinCnt
            If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                NxtIdx = VScroll2(0).Value + LinCnt
                If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
                If NxtIdx > VScroll2(0).Max Then NxtIdx = VScroll2(0).Max
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyPageUp
            NxtIdx = CurBarIdx - LinCnt
            If NxtIdx < 0 Then NxtIdx = 0
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                NxtIdx = VScroll2(0).Value - LinCnt
                If NxtIdx < 0 Then NxtIdx = 0
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyLeft
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value - 5
                Case 1
                    NewVal = HScroll2(0).Value - 1
                Case 2
                    NewVal = HScroll2(0).Value - 20
                Case Else
                    NewVal = HScroll2(0).Value - 5
            End Select
            If NewVal < 0 Then NewVal = 0
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case vbKeyRight
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value + 5
                Case 1
                    NewVal = HScroll2(0).Value + 1
                Case 2
                    NewVal = HScroll2(0).Value + 20
                Case Else
                    NewVal = HScroll2(0).Value + 5
            End Select
            If NewVal > HScroll2(0).Max Then NewVal = HScroll2(0).Max
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case Else
    End Select
End Sub

Private Sub Form_Load()
    Dim tmpCfg As String
    ChartName = "GOC"
    MainAreaColor = Val(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_AREA_COLOR", "7"))
    WorkAreaColor = MainAreaColor
    ChartBarBackColor = LightGray
    lblLeftBarCover(0).Tag = "7"
    tmpCfg = GetIniEntry(myIniFullName, "MAIN", "", "SYNC_TIMELINE", "NO")
    If tmpCfg = "YES" Then cmdSyncTime.Visible = True Else cmdSyncTime.Visible = False
    MinimumTop = 30
    MinimumLeft = 30
    CurChartWidth = 0
    CurChartHeight = 0
    FullBarHeight = ChartLine2(0).Height
    lblOneMinuteWidth.Width = 75
    Me.Top = Screen.Height + 1000
    Me.Left = 0
    Me.Width = MaxScreenWidth
    Me.Height = Screen.Height
    chkUtc(1).Value = 1
    'chkSelDeco(0).Value = 1
    'chkSelDeco(1).Value = 1
    'We have a restriction of a maximum height of a picture box (Panel)
    'So this is the maximum number of bars on the chart:
    MaxBarCnt = CInt(245745 \ FullBarHeight) - 1
    'Maximum is 584, but together with the ConnexChart
    'we run out of memory so we set it to 350
    MaxBarCnt = 250
    'MaxBarCnt = 100 'Test only
    CurBarMax = 0
    GetChartConfig
    InitChartArea
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    chkAppl(0).Value = 1
    Cancel = True
End Sub

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim i As Integer
    
    VScroll1(0).Visible = False
    VScroll2(0).Visible = False
    HScroll2(0).Visible = False
    
    NewTop = MinimumTop
    ButtonPanel.Top = NewTop
    RightPanel.Top = NewTop
    NewLeft = MinimumLeft
    ButtonPanel.Left = NewLeft
    TopPanel.Left = NewLeft
    LeftPanel(0).Left = NewLeft
    
    NewWidth = Me.ScaleWidth
    NewLeft = NewWidth - RightPanel.Width - 15
    RightPanel.Left = NewLeft
    NewWidth = NewLeft - MinimumLeft - 15
    If NewWidth > 300 Then
        ButtonPanel.Width = NewWidth
        TopPanel.Width = NewWidth
        cmdSyncTime.Left = NewWidth - cmdSyncTime.Width - 60
    End If
    NewLeft = RightPanel.Left - VertScroll2(0).Width - 15
    VertScroll2(0).Left = NewLeft
    HorizScroll3(0).Left = NewLeft
    TimeScroll2(0).Left = NewLeft
    
    NewHeight = Me.ScaleHeight - StatusBar.Height - MinimumTop
    If NewHeight > 300 Then
        RightPanel.Height = NewHeight
    End If
    
    NewTop = MinimumTop
    NewTop = NewTop + ButtonPanel.Height + 15
    If TopPanel.Visible Then
        TopPanel.Top = NewTop
        NewTop = NewTop + TopPanel.Height + 15
    End If
    LeftPanel(0).Top = NewTop
    TopRemark(0).Top = NewTop
    TimeScroll1(0).Top = NewTop
    TimeScroll2(0).Top = NewTop
    
    NewHeight = NewHeight - NewTop + MinimumTop
    If NewHeight > 300 Then
        LeftPanel(0).Height = NewHeight
    End If
    
    NewTop = NewTop + TopRemark(0).Height '+ 15
    
    'TimeScroll1(0).Top = NewTop
    'TimeScroll2(0).Top = NewTop
    TimePanel(0).Top = NewTop
    
    NewTop = NewTop + TimePanel(0).Height + 15
    VertScroll1(0).Top = NewTop
    WorkArea(0).Top = NewTop
    VertScroll2(0).Top = NewTop
    
    NewLeft = MinimumLeft
    If LeftPanel(0).Visible Then
        NewLeft = NewLeft + LeftPanel(0).Width + 15
    End If
    TimeScroll1(0).Left = NewLeft
    TopRemark(0).Left = NewLeft
    
    NewWidth = RightPanel.Left - NewLeft - 15
    If NewWidth > 300 Then
        'TopRemark(0).Width = NewWidth
        'BottomRemark(0).Width = NewWidth
    End If
    
    If VertScroll1(0).Visible Then
        VertScroll1(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewLeft = NewLeft + VertScroll1(0).Width + 15
        WorkArea(0).Left = NewLeft
        HorizScroll2(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    Else
        WorkArea(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            NewLeft = NewLeft + HorizScroll1(0).Width + 15
            HorizScroll2(0).Left = NewLeft
            NewWidth = NewWidth - HorizScroll1(0).Width - 15
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    End If
    
    TimePanel(0).Left = WorkArea(0).Left
    TimePanel(0).Width = WorkArea(0).Width
    TopRemark(0).Left = WorkArea(0).Left
    TopRemark(0).Width = WorkArea(0).Width
    If chkSwitch(8).Value = 1 Then fraZoomOptions.Left = TimePanel(0).Width - fraZoomOptions.Width - 60
    
    BottomRemark(0).Left = HorizScroll2(0).Left
    BottomRemark(0).Width = HorizScroll2(0).Width
    
    NewTop = Me.ScaleHeight - StatusBar.Height - BottomRemark(0).Height
    BottomRemark(0).Top = NewTop '- 30
    NewTop = NewTop - HScroll2(0).Height - 45
    HorizScroll1(0).Top = NewTop
    HorizScroll2(0).Top = NewTop
    HorizScroll3(0).Top = NewTop
    NewHeight = NewTop - WorkArea(0).Top - 15
    If NewHeight > 300 Then
        VertScroll1(0).Height = NewHeight
        VScroll1(0).Height = NewHeight - 60
        WorkArea(0).Height = NewHeight
        VertScroll2(0).Height = NewHeight
        VScroll2(0).Height = NewHeight - 60
    End If
    
    VisibleBars = WorkArea(0).Height \ FullBarHeight
    i = CInt(LoadedBars - VisibleBars)
    If i < 0 Then i = 0
    VScroll1(0).Max = i
    VScroll2(0).Max = i
    
    ArrangeChartArea
    
    VScroll1(0).Visible = True
    VScroll1(0).Refresh
    VScroll2(0).Visible = True
    VScroll2(0).Refresh
    HScroll2(0).Visible = True
    HScroll2(0).Refresh
  
    DrawBackGround TopRemark(0), 7, True, False
    DrawBackGround BottomRemark(0), 7, True, True
    
    WorkArea(0).Refresh
    AdjustOverviewPanel
    Me.Refresh
End Sub
Private Sub AdjustOverviewPanel()
    Dim TopLine As Long
    Dim BotLine As Long
    Dim ActLine As Long
    Dim CurLeft As Long
    Dim CurRight As Long
    Dim LinCnt As Long
    If (Not DontResyncScroll) And (Not DontAdjustOverview) Then
        TopLine = CLng(VScroll2(0).Value)
        BotLine = TopLine + VisibleBars - 1
        TopLine = TopLine + FirstRotLine
        BotLine = BotLine + FirstRotLine
        ActLine = CLng(CurBarIdx) + FirstRotLine
        CurLeft = -HScrollArea(0).Left
        CurRight = CurLeft + WorkArea(0).Width
                'REMOVED
        GocChartOverview.SynchronizeChartArea ActLine, TopLine, BotLine, CurLeft, CurRight
    End If
End Sub
Private Sub InitChartArea()
    Dim NewColor As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Screen.MousePointer = 11
    
    InitRightPanel
    
    NewColor = ChartBarBackColor
    ChartLine1(0).BackColor = NewColor
    ChartLine2(0).BackColor = NewColor
    ChartLine3(0).BackColor = NewColor
    ArrFlight(0).BackColor = NewColor
    DepFlight(0).BackColor = NewColor
    LeftScale(0).BackColor = NewColor
    RightScale(0).BackColor = NewColor
    
    lblBgnTime(0).BackColor = LightGray
    
    NewLeft = -30
    VScrollArea(0).Left = NewLeft
    HScrollArea(0).Left = NewLeft
    LeftScale(0).Left = NewLeft
    
    NewLeft = 30
    ChartLine1(0).Left = NewLeft
    ChartLine3(0).Left = NewLeft
    
    NewLeft = -30
    ChartLine2(0).Left = NewLeft
    
    NewTop = -30
    VScrollArea(0).Top = NewTop
    HScrollArea(0).Top = NewTop
    LeftScale(0).Top = NewTop
    RightScale(0).Top = NewTop
    
    NewTop = 30
    ChartLine1(0).Top = NewTop
    ChartLine2(0).Top = NewTop
    ChartLine3(0).Top = NewTop
    
    NewTop = -30
    TimePanel(0).Height = TimeScroll1(0).Height - TopRemark(0).Height
    TimeScale(0).Top = NewTop
    TimeScale(0).Height = TimePanel(0).Height + 60
    
    CreateTimeScaleArea TimeScale(0)
    HScrollArea(0).Width = CurChartWidth
    VScrollArea(0).Width = CurChartWidth
    ChartLine1(0).Width = ArrFlight(0).Width + 90
    LeftScale(0).Width = ChartLine1(0).Width + 120
    ChartLine2(0).Width = CurChartWidth
    ChartLine3(0).Width = DepFlight(0).Width + 90
    RightScale(0).Width = ChartLine3(0).Width + 120
    
    
    Screen.MousePointer = 11
    CreateChartBarLayout 680
    Screen.MousePointer = 11
    
    NowTime(1).ZOrder
    lblCurTime(0).ZOrder
    LeftScale(0).ZOrder
    RightScale(0).ZOrder
    LeftPanel(0).ZOrder
    ButtonPanel.ZOrder
    TopPanel.ZOrder
    TimePanel(0).ZOrder
    TopRemark(0).ZOrder
    WorkArea(0).ZOrder
    BottomRemark(0).ZOrder
    RightPanel.ZOrder
    StatusBar.ZOrder
    Screen.MousePointer = 0
    
End Sub

Private Sub ArrangeChartArea()
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewValue As Long
    
    NewWidth = WorkArea(0).Width + 60
    VScrollArea(0).Width = NewWidth
    NewLeft = VScrollArea(0).Width - RightScale(0).Width - 90
    RightScale(0).Left = NewLeft
    RightScale(0).Refresh
    
    TimeScale(0).Left = HScrollArea(0).Left
    TimeScale(0).Width = HScrollArea(0).Width
    TimeScale(0).Refresh
    
    CurScrollWidth = CurChartWidth - WorkArea(0).Width
    NewValue = CLng(HScroll2(0).Value)
    AdjustHorizScroll NewValue
    
End Sub

Private Sub HScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub HScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub AdjustHorizScroll(ScrollValue As Long)
    Dim NewLeft As Long
    If Not DontScroll Then
        NewLeft = ScrollValue * MinuteWidth
        HScrollArea(0).Left = -NewLeft
        WorkArea(0).Refresh
        TimeScale(0).Left = -NewLeft
        TimeScale(0).Refresh
        AdjustOverviewPanel
    End If
End Sub

Private Sub icoCflRange_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    icoCflRange(Index).Tag = CStr(x)
    RangeLine(Index).Left = icoCflRange(Index).Left + 105
    RangeLine(Index).Visible = True
    If Index = 0 Then
        ShowTimeRangeDuration True, RangeLine(Index).Left, NowTime(0).Left
    Else
        ShowTimeRangeDuration True, NowTime(0).Left, RangeLine(Index).Left
    End If
End Sub

Private Sub icoCflRange_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim OldX As Long
    Dim NewX As Long
    Dim diffX As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MinLeft As Long
    Dim DurLeft As Long
    Dim DurRight As Long
    OldX = CLng(Val(icoCflRange(Index).Tag))
    NewX = CLng(x)
    If OldX > 0 Then
        If OldX <> NewX Then
            diffX = NewX - OldX
            NewLeft = icoCflRange(Index).Left + diffX
            NewLeft = (NewLeft \ MinuteWidth) * MinuteWidth
            If Index = 0 Then
                'MinLeft = lblMin1(0).Left
                MaxLeft = NowTime(0).Left - (MinuteWidth * 10)
                DurLeft = NewLeft + 105
                DurRight = NowTime(0).Left
            Else
                MinLeft = NowTime(0).Left + (MinuteWidth * 10)
                MaxLeft = CurChartWidth - 1200
                DurRight = NewLeft + 105
                DurLeft = NowTime(0).Left
            End If
            If (NewLeft >= MinLeft) And (NewLeft <= MaxLeft) Then
                RangeLine(Index).Visible = False
                HScrollArea(0).Refresh
                icoCflRange(Index).Left = NewLeft
                NewLeft = NewLeft + 105
                RangeLine(Index).Left = NewLeft
                RangeLine(Index).Visible = True
                RangeLine(Index).Refresh
                ShowTimeRangeDuration True, DurLeft, DurRight
            End If
        End If
    End If
End Sub
Private Function ShowTimeRangeDuration(ShowLabel As Boolean, CurLeft As Long, CurRight As Long) As Long
    Dim tmpLeft As Long
    Dim tmpRight As Long
    Dim tmpWidth As Long
    Dim tmpDuration As Long
    Dim tmpVal As Long
    Dim tmpText As String
    lblDuration(0).Visible = False
    If CurRight > CurLeft Then
        tmpLeft = CurLeft
        tmpRight = CurRight
    Else
        tmpLeft = CurRight
        tmpRight = CurLeft
    End If
    tmpWidth = tmpRight - tmpLeft + 15
    lblDuration(0).Left = tmpLeft
    lblDuration(0).Width = tmpWidth
    tmpDuration = tmpWidth \ MinuteWidth
    tmpVal = tmpDuration Mod 60
    tmpText = Right("00" & CStr(tmpVal), 2)
    tmpVal = (tmpDuration - tmpVal) \ 60
    tmpText = Right("00" & CStr(tmpVal), 2) & ":" & tmpText
    lblDuration(0).Caption = tmpText
    lblDuration(0).Visible = ShowLabel
    lblDuration(0).Refresh
    ShowTimeRangeDuration = tmpDuration
End Function

Private Sub icoCflRange_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    icoCflRange(Index).Tag = "-1"
    RangeLine(Index).Visible = False
    lblDuration(0).Visible = False
End Sub

Private Sub lblArrAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringArrPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrBar(Index), True, x, False
End Sub

Private Sub lblArrBar_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblArrBar(Index), False, x, True
End Sub

Private Sub lblArrBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblArrBar(Index), False, x, False
End Sub

Private Sub lblArrJob1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringArrPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrJob1(Index), True, x, False
End Sub
Private Sub lblArrJob1_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblArrJob1(Index), False, x, False
End Sub

Private Sub lblArrJob2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringArrPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrJob2(Index), True, x, False
End Sub
Private Sub lblArrJob2_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblArrJob2(Index), False, x, False
End Sub
Private Sub BringArrPanelToFront(Index As Integer)
    ArrFlight(Index).ZOrder
    picArr1(Index).ZOrder
    picArr2(Index).ZOrder
    picArr3(Index).ZOrder
    picArr4(Index).ZOrder
End Sub


Private Sub lblArrEtai_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrGta1_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOnbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOrg3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub AdjustArrIconSize(jIdx As Integer, NewSize As Long)
    Dim tmpIdxTag As String
    Dim iBar As Integer
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim MaxHeight As Long
    Dim NewLeft As Long
    NewHeight = NewSize
    MaxHeight = ChartLine2(0).Height - 60
    If NewHeight > MaxHeight Then NewHeight = MaxHeight
    NewTop = (MaxHeight - NewHeight) \ 2
    tmpIdxTag = CStr(jIdx)
    For iBar = 0 To CurBarMax
        If NewSize = 240 Then NewTop = lblArrBar(iBar).Top + 15
        If (jIdx < 0) Or (picArr1(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr1(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picArr2(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr2(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picArr3(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr3(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picArr4(iBar).Tag = tmpIdxTag) Then SetIconDimension picArr4(iBar), NewTop, NewHeight
        NewLeft = lblArrBar(iBar).Left + 15
        picArr1(iBar).Left = NewLeft
        NewLeft = NewLeft + picArr1(iBar).Width
        picArr2(iBar).Left = NewLeft
        NewLeft = NewLeft + picArr2(iBar).Width
        picArr3(iBar).Left = NewLeft
        NewLeft = NewLeft + picArr3(iBar).Width
        picArr4(iBar).Left = NewLeft
    Next
End Sub
Private Sub AdjustDepIconSize(jIdx As Integer, NewSize As Long)
    Dim tmpIdxTag As String
    Dim iBar As Integer
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim MaxHeight As Long
    Dim NewLeft As Long
    NewHeight = NewSize
    MaxHeight = ChartLine2(0).Height - 60
    If NewHeight > MaxHeight Then NewHeight = MaxHeight
    NewTop = (MaxHeight - NewHeight) \ 2
    tmpIdxTag = CStr(jIdx)
    For iBar = 0 To CurBarMax
        If NewSize = 240 Then NewTop = lblDepBar(iBar).Top + 15
        If (jIdx < 0) Or (picDep1(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep1(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picDep2(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep2(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picDep3(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep3(iBar), NewTop, NewHeight
        If (jIdx < 0) Or (picDep4(iBar).Tag = tmpIdxTag) Then SetIconDimension picDep4(iBar), NewTop, NewHeight
        NewLeft = lblDepBar(iBar).Left + lblDepBar(iBar).Width - 15
        NewLeft = NewLeft - picDep4(iBar).Width
        picDep4(iBar).Left = NewLeft
        NewLeft = NewLeft - picDep3(iBar).Width
        picDep3(iBar).Left = NewLeft
        NewLeft = NewLeft - picDep2(iBar).Width
        picDep2(iBar).Left = NewLeft
        NewLeft = NewLeft - picDep1(iBar).Width
        picDep1(iBar).Left = NewLeft
    Next
End Sub
Private Sub SetIconDimension(CurImg As Image, SetTop As Long, SetSize As Long)
    CurImg.Top = SetTop
    CurImg.Height = SetSize
    CurImg.Width = SetSize
End Sub
Private Sub lblArrPsta_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrStoa_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringDepPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepBar(Index), True, x, False
End Sub

Private Sub lblDepBar_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblDepBar(Index), False, x, True
End Sub

Private Sub lblDepBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblDepBar(Index), False, x, False
End Sub

Private Sub lblDepDes3_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim tmpData As String
    If Button = 2 Then
        tmpData = Trim(lblDepDes3(Index).Caption)
        ShowBasicDetails "APT", tmpData
    End If
End Sub

Private Sub lblDepJob1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringDepPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepJob1(Index), True, x, False
End Sub
Private Sub lblDepJob1_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblDepJob1(Index), False, x, False
End Sub
Private Sub lblDepJob2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringDepPanelToFront Index
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepJob2(Index), True, x, False
End Sub
Private Sub lblDepJob2_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ShowRotaTimeFrame Index, lblDepJob2(Index), False, x, False
End Sub

Private Sub BringDepPanelToFront(Index As Integer)
    DepFlight(Index).ZOrder
    picDep1(Index).ZOrder
    picDep2(Index).ZOrder
    picDep3(Index).ZOrder
    picDep4(Index).ZOrder
End Sub

Private Sub lblDepDes3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepEtdi_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepGtd1_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepOfbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepPstd_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepStod_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepVia3_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim tmpData As String
    If Button = 2 Then
        tmpData = Trim(lblDepVia3(Index).Caption)
        ShowBasicDetails "APT", tmpData
    End If
End Sub

Private Sub lblTowBar_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ShowRotaTimeFrame(Index As Integer, CurBarObj As Label, ShowRange As Boolean, XPos As Single, MovePos As Boolean)
    Dim RotLeft As Long
    Dim RotRight As Long
    Dim RotWidth As Long
    Dim lDuration As Long
    Dim tmpVal As Long
    Dim tmpText As String
    If (ShowRange = True) Or (MovePos = True) Then
        RotLeft = CurBarObj.Left - 15
        If ShowRange = True Then
            RotRight = CurBarObj.Left + CurBarObj.Width - 15
            RangeLine(0).Left = RotLeft
            RangeLine(1).Left = RotRight
            icoBarRange(0).Left = RotLeft - 105
            icoBarRange(1).Left = RotRight - 105
            RotWidth = RotRight - RotLeft + 15
            lblDuration(0).Left = RotLeft
            lblDuration(0).Width = RotWidth
            lDuration = Val(CurBarObj.Tag)
            tmpVal = lDuration Mod 60
            tmpText = Right("00" & CStr(tmpVal), 2)
            tmpVal = (lDuration - tmpVal) \ 60
            tmpText = Right("00" & CStr(tmpVal), 2) & ":" & tmpText
            lblDuration(0).Caption = ""
            lblDuration(1).Caption = tmpText
            RangeLine(0).Visible = True
            RangeLine(1).Visible = True
            icoBarRange(0).Visible = True
            icoBarRange(1).Visible = True
            lblDuration(0).Visible = True
            lblDuration(0).ZOrder
            lblDuration(1).ZOrder
            icoBarRange(0).ZOrder
            icoBarRange(1).ZOrder
            ObjectIsMoving = True
        End If
        If ObjectIsMoving Then
            RotLeft = RotLeft + XPos - (lblDuration(1).Width \ 2) + 30
            tmpVal = icoBarRange(0).Left + icoBarRange(0).Width + 30
            If RotLeft < tmpVal Then RotLeft = tmpVal
            tmpVal = icoBarRange(1).Left - lblDuration(1).Width - 30
            If RotLeft > tmpVal Then RotLeft = tmpVal
            lblDuration(1).Left = RotLeft
            lblDuration(1).Visible = True
        End If
    Else
        RangeLine(0).Visible = False
        RangeLine(1).Visible = False
        icoBarRange(0).Visible = False
        icoBarRange(1).Visible = False
        lblDuration(0).Visible = False
        lblDuration(1).Visible = False
        ObjectIsMoving = False
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then
        OnTop.BackColor = LightGreen
        SetFormOnTop Me, True
    Else
        OnTop.BackColor = MyOwnButtonFace
        SetFormOnTop Me, False
    End If
End Sub

Private Sub Option1_Click(Index As Integer)
    Dim RotLine As Long
    lblOneMinuteWidth.Width = Val(Option1(Index).Tag)
    CreateTimeScaleArea TimeScale(0)
    TimeScale(0).SetFocus
    GetFlightRotations -1, False
End Sub

Private Sub picArr1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringArrPanelToFront Index
End Sub
Private Sub picArr2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringArrPanelToFront Index
End Sub
Private Sub picArr3_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringArrPanelToFront Index
End Sub
Private Sub picArr4_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringArrPanelToFront Index
End Sub

Private Sub picArrPic_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim iBar As Integer
    Dim iNxt As Integer
    Dim iMax As Integer
    Dim iLoop1 As Integer
    Dim iLoop2 As Integer
    Dim iGotBar As Integer
    Dim LookVal As String
    Dim CurScrollValue As Integer
    Dim NewScrollValue As Integer
    Dim TopIdx As Integer
    Dim LinCnt As Integer
    Dim BotIdx As Integer
    Dim BarLeft As Long
    Dim WinLeft As Long
    Dim BarRight As Long
    Dim WinRight As Long
    Dim NewLeft As Long
    Dim ScrollIt As Boolean
    picArrPic(Index).Top = picArrPic(Index).Top + 15
    picArrPic(Index).Left = picArrPic(Index).Left + 15
    picArrPic(Index).Refresh
    LookVal = CStr(Index)
    If CurBarIdx < 0 Then CurBarIdx = VScroll2(0).Value
    iGotBar = -1
    iNxt = CurBarIdx + 1
    iMax = CurBarMax
    iLoop1 = 0
    While (iLoop1 < 2) And (iGotBar < 0)
        iLoop1 = iLoop1 + 1
        iBar = iNxt
        While (iBar <= iMax) And (iGotBar < 0)
            If picArr1(iBar).Tag = LookVal Then iGotBar = iBar
            If picArr2(iBar).Tag = LookVal Then iGotBar = iBar
            If picArr3(iBar).Tag = LookVal Then iGotBar = iBar
            If picArr4(iBar).Tag = LookVal Then iGotBar = iBar
            iBar = iBar + 1
        Wend
        If iGotBar < 0 Then
            iNxt = 0
            iMax = CurBarIdx
        End If
    Wend
    If iGotBar >= 0 Then
        CurScrollValue = VScroll2(0).Value
        LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height))
        TopIdx = CurScrollValue
        BotIdx = TopIdx + LinCnt - 1
        NewScrollValue = -1
        If iGotBar < TopIdx Then NewScrollValue = iGotBar
        If iGotBar > BotIdx Then NewScrollValue = iGotBar - LinCnt + 1
        If NewScrollValue >= 0 Then VScroll2(0).Value = NewScrollValue
        BarLeft = lblArrBar(iGotBar).Left
        BarRight = BarLeft + 2000
        BarLeft = BarLeft - ArrFlight(iGotBar).Width
        WinLeft = Abs(HScrollArea(0).Left)
        WinRight = WinLeft + WorkArea(0).Width
        ScrollIt = False
        If BarLeft < WinLeft Then
            NewLeft = BarLeft
            ScrollIt = True
        End If
        If BarRight > WinRight Then
            NewLeft = BarRight - WorkArea(0).Width
            ScrollIt = True
        End If
        If ScrollIt = True Then
            NewScrollValue = CInt(NewLeft \ MinuteWidth)
            If NewScrollValue < 0 Then NewScrollValue = 0
            If NewScrollValue > HScroll2(0).Max Then NewScrollValue = HScroll2(0).Max
            HScroll2(0).Value = NewScrollValue
        End If
        HighlightCurrentBar iGotBar, ChartName
    End If
End Sub

Private Sub picArrPic_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    picArrPic(Index).Top = picArrPic(Index).Top - 15
    picArrPic(Index).Left = picArrPic(Index).Left - 15
    picArrPic(Index).Refresh
End Sub

Private Sub picDep1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringDepPanelToFront Index
End Sub
Private Sub picDep2_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringDepPanelToFront Index
End Sub
Private Sub picDep3_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringDepPanelToFront Index
End Sub
Private Sub picDep4_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    BringDepPanelToFront Index
End Sub

Private Sub picDepPic_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim iBar As Integer
    Dim iNxt As Integer
    Dim iMax As Integer
    Dim iLoop1 As Integer
    Dim iLoop2 As Integer
    Dim iGotBar As Integer
    Dim LookVal As String
    Dim CurScrollValue As Integer
    Dim NewScrollValue As Integer
    Dim TopIdx As Integer
    Dim LinCnt As Integer
    Dim BotIdx As Integer
    Dim BarLeft As Long
    Dim WinLeft As Long
    Dim BarRight As Long
    Dim WinRight As Long
    Dim NewLeft As Long
    Dim ScrollIt As Boolean
    picDepPic(Index).Top = picDepPic(Index).Top + 15
    picDepPic(Index).Left = picDepPic(Index).Left + 15
    picDepPic(Index).Refresh
    LookVal = CStr(Index)
    If CurBarIdx < 0 Then CurBarIdx = VScroll2(0).Value
    iGotBar = -1
    iNxt = CurBarIdx + 1
    iMax = CurBarMax
    iLoop1 = 0
    While (iLoop1 < 2) And (iGotBar < 0)
        iLoop1 = iLoop1 + 1
        iBar = iNxt
        While (iBar <= iMax) And (iGotBar < 0)
            If picDep1(iBar).Tag = LookVal Then iGotBar = iBar
            If picDep2(iBar).Tag = LookVal Then iGotBar = iBar
            If picDep3(iBar).Tag = LookVal Then iGotBar = iBar
            If picDep4(iBar).Tag = LookVal Then iGotBar = iBar
            iBar = iBar + 1
        Wend
        If iGotBar < 0 Then
            iNxt = 0
            iMax = CurBarIdx
        End If
    Wend
    If iGotBar >= 0 Then
        CurScrollValue = VScroll2(0).Value
        LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height))
        TopIdx = CurScrollValue
        BotIdx = TopIdx + LinCnt - 1
        NewScrollValue = -1
        If iGotBar < TopIdx Then NewScrollValue = iGotBar
        If iGotBar > BotIdx Then NewScrollValue = iGotBar - LinCnt + 1
        If NewScrollValue >= 0 Then VScroll2(0).Value = NewScrollValue
        BarLeft = lblDepBar(iGotBar).Left + lblDepBar(iGotBar).Width
        BarRight = BarLeft + DepFlight(iGotBar).Width
        BarLeft = BarLeft - 2000
        WinLeft = Abs(HScrollArea(0).Left)
        WinRight = WinLeft + WorkArea(0).Width
        ScrollIt = False
        If BarLeft < WinLeft Then
            NewLeft = BarLeft
            ScrollIt = True
        End If
        If BarRight > WinRight Then
            NewLeft = BarRight - WorkArea(0).Width
            ScrollIt = True
        End If
        If ScrollIt = True Then
            NewScrollValue = CInt(NewLeft \ MinuteWidth)
            If NewScrollValue < 0 Then NewScrollValue = 0
            If NewScrollValue > HScroll2(0).Max Then NewScrollValue = HScroll2(0).Max
            HScroll2(0).Value = NewScrollValue
        End If
        HighlightCurrentBar iGotBar, ChartName
    End If
End Sub

Private Sub picDepPic_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    picDepPic(Index).Top = picDepPic(Index).Top - 15
    picDepPic(Index).Left = picDepPic(Index).Left - 15
    picDepPic(Index).Refresh
End Sub

Private Sub picDepStat_DblClick(Index As Integer)
    Dim tmpTag As String
    Dim tmpIdx As Integer
    tmpTag = picDepStat(Index).Tag
    tmpIdx = Val(tmpTag) + 1
    If tmpIdx > picMarker.UBound Then tmpIdx = 0
    picDepStat(Index).Picture = picMarker(tmpIdx).Picture
    picDepStat(Index).Tag = CStr(tmpIdx)
End Sub

Private Sub TimeScaleTimer_Timer()
    Dim DisplayTime As String
    Dim DisplayDate As String
    Dim NewLeft As Long
    Dim TimeOff As Long
    Dim retval As Boolean
    Dim tmpTime
    Dim CurLocTime
    Dim CurUtcTime
    Dim SecDiff
    
    'The time synchronization will be done based on the server time.
    'So we must calculate the differences of the elapsed time since the last timer event.
    CurLocTime = Now
    SecDiff = DateDiff("s", LastLocTimeCheck, CurLocTime)
    LastLocTimeCheck = CurLocTime
    ServerUtcTimeVal = DateAdd("s", SecDiff, ServerUtcTimeVal)
    LastUtcTimeCheck = ServerUtcTimeVal
    
    If chkUtc(0).Value = 1 Then tmpTime = LastUtcTimeCheck Else tmpTime = DateAdd("n", UtcTimeDiff, ServerUtcTimeVal)
    DisplayTime = Format(tmpTime, "hh:mm:ss")
    DisplayDate = Format(tmpTime, "YYYYMMDD")
    
    DisplayDate = DecodeSsimDayFormat(DisplayDate, "CEDA", "SSIM2")
    lblCurTime(0).Caption = DisplayTime
    lblCurTime(0).ToolTipText = DisplayDate
    
    retval = GetTimeScalePos("", ServerUtcTimeVal, NewLeft)
    If retval = True Then
        NewLeft = NewLeft - 15
        TimeOff = NewLeft - NowTime(0).Left
        If TimeOff <> 0 Then
            lblCurTime(0).Visible = False
            NowTime(0).Visible = False
            NowTime(1).Visible = False
            NowTime(0).Left = NewLeft
            NowTime(1).Left = NewLeft
            lblCurTime(0).Left = NewLeft - (lblCurTime(0).Width \ 2) + 30
            NowTimeHint(0).Left = lblCurTime(0).Left - NowTimeHint(0).Width
            NowTimeHint(1).Left = lblCurTime(0).Left + lblCurTime(0).Width
            If chkSwitch(1).Value = 1 Then
                If HScroll2(0).Value < HScroll2(0).Max Then
                    HScroll2(0).Value = HScroll2(0).Value + 1
                End If
            End If
            NowTime(1).Visible = True
            NowTime(0).Visible = True
            lblCurTime(0).Visible = True
            NowTimeHint(0).Visible = True
            NowTimeHint(1).Visible = True
        End If
    Else
        lblCurTime(0).Visible = False
        NowTime(0).Visible = False
        NowTime(1).Visible = False
        NowTimeHint(0).Visible = False
        NowTimeHint(1).Visible = False
    End If
                'REMOVED
    GocChartOverview.SynchronizeNowTime NewLeft, DisplayTime
End Sub

Public Function GetTimeScalePos(CedaUtcTime As String, CedaTimeValue, ScalePos As Long) As Boolean
    Dim MinDiff As Long
    On Error Resume Next
    GetTimeScalePos = True
    If CedaUtcTime <> "" Then CedaTimeValue = CedaFullDateToVb(CedaUtcTime)
    MinDiff = CLng(DateDiff("n", TimeScaleUtcBegin, CedaTimeValue))
    ScalePos = TimeRange(0).Left + (MinDiff * MinuteWidth)
    If ScalePos < TimeRange(0).Left Then GetTimeScalePos = False
    If ScalePos > TimeRange(1).Left Then GetTimeScalePos = False
End Function
Private Sub VScroll1_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll1_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub VScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub AdjustVertScroll(ScrollValue As Long)
    Dim NewTop As Long
    NewTop = ScrollValue * ChartLine2(0).Height
    VScrollArea(0).Top = -NewTop
    WorkArea(0).Refresh
    AdjustOverviewPanel
End Sub

Private Sub ToggleFlightBarTimes(TimeOffs As Integer)
    Dim i As Integer
    For i = 0 To CurBarMax
        SetTimeFieldCaption lblArrStoa(i), TimeOffs
        SetTimeFieldCaption lblArrEtai(i), TimeOffs
        SetTimeFieldCaption lblArrOnbl(i), TimeOffs
        SetTimeFieldCaption lblDepStod(i), TimeOffs
        SetTimeFieldCaption lblDepEtdi(i), TimeOffs
        SetTimeFieldCaption lblDepOfbl(i), TimeOffs
    Next
End Sub
Private Sub SetTimeFieldCaption(CurLbl As Label, TimeOffs As Integer)
    Dim tmpTag As String
    Dim tmpTxt As String
    Dim tmpTime
    tmpTag = Trim(CurLbl.Tag)
    tmpTxt = ""
    If tmpTag <> "" Then
        tmpTime = CedaFullDateToVb(tmpTag)
        tmpTime = DateAdd("n", TimeOffs, tmpTime)
        tmpTxt = Format(tmpTime, "hh:mm")
    End If
    CurLbl.Caption = tmpTxt
End Sub

Public Sub GetFlightRotations(TopLine As Long, ForRepaintOverview As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim DepLine As Long
    Dim ArrLine As Long
    Dim iBar As Integer
    Dim jBar As Integer
    Dim BarIdx As Integer
    Dim i As Integer
    Dim tmpData As String
    Dim tmpAidx As String
    Dim tmpDidx As String
    Dim tmpRidx As String
    Dim tmpGidx As String
    Dim tmpView As String
    Dim tmpHitLine As String
    Dim RotBest As String
    Dim ArrTifa As String
    Dim DepTifd As String
    Dim DepOfbl As String
    Dim TopPos As Long
    Dim RotScalePos As Long
    Dim ArrScalePos As Long
    Dim DepScalePos As Long
    Dim DepLeft As Long
    Dim ArrLeft As Long
    Dim NewLeft As Long
    Dim PicLeft As Long
    Dim RotWidth As Long
    Dim ArrWidth As Long
    Dim DepWidth As Long
    Dim MinRotWidth As Long
    Dim ArrDuration As Long
    Dim DepDuration As Long
    Dim MinDepDura As Long
    Dim MaxDepDura As Long
    Dim MinArrDura As Long
    Dim MaxArrDura As Long
    Dim IdxColNo As Long
    Dim ArrColor As Long
    Dim DepColor As Long
    Dim IsValidRotScalePos As Boolean
    Dim IsValidArrScalePos As Boolean
    Dim IsValidDepScalePos As Boolean
    Dim ArrVisible As Boolean
    Dim DepVisible As Boolean
    Dim ShowJobs As Boolean
    Dim RotBestValue
    Dim DepTifdValue
    Dim ArrTifaValue
    Screen.MousePointer = 11
    
    
    CurLine = TopLine
    If CurLine < 0 Then CurLine = FirstRotLine
    
'CurLine = 150

    Load GocChartOverview
    GocChartOverview.StopActivities
    If ForRepaintOverview = False Then
        tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "SHOW_ALL_ICONS", "NO"))
        If tmpData = "YES" Then ShowAllIcons = True Else ShowAllIcons = False
        For i = 0 To lblArrPicCount.UBound
            lblArrPicCount(i).Caption = ""
            lblArrPicCount(i).Tag = ""
        Next
        For i = 0 To lblDepPicCount.UBound
            lblDepPicCount(i).Caption = ""
            lblDepPicCount(i).Tag = ""
        Next
        LeftScale(0).Refresh
        RightScale(0).Refresh
        CreateTimeScaleArea TimeScale(0)
        MaxDepDura = 60
        MaxArrDura = 60
        MinDepDura = 20
        MinArrDura = 20
        MaxDepWidth = MaxDepDura * MinuteWidth
        MaxArrWidth = MaxArrDura * MinuteWidth
        MinDepWidth = MinDepDura * MinuteWidth
        MinArrWidth = MinArrDura * MinuteWidth
        MinRotWidth = MinuteWidth * 15
        
        TabRotFlightsTab.IndexDestroy "GOCX"
        ShowJobs = False
    End If
    MaxLine = TabRotFlightsTab.GetLineCount
    MaxLine = MaxLine - 1
    
    FirstRotLine = CurLine
    
    
    TotalRotLines = MaxLine + 1
    AvailableBars = CLng(CurBarMax + 1)
    LastRotLine = FirstRotLine + AvailableBars - 1
    If LastRotLine >= TotalRotLines Then
        LastRotLine = TotalRotLines - 1
        FirstRotLine = LastRotLine - AvailableBars + 1
        If FirstRotLine < 0 Then FirstRotLine = 0
    End If
    GocChartOverview.InitChartArea
    
    iBar = -1
    For CurLine = 0 To MaxLine
        BarIdx = -1
        ArrLine = -1
        ArrColor = 0
        DepLine = -1
        DepColor = 0
        tmpView = "1"
        tmpGidx = "------"
        tmpRidx = TabRotFlightsTab.GetFieldValue(CurLine, "RIDX")
        tmpHitLine = TabArrFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
        If tmpHitLine <> "" Then ArrLine = Val(tmpHitLine)
        tmpHitLine = TabDepFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
        If tmpHitLine <> "" Then DepLine = Val(tmpHitLine)
        If (CurLine >= FirstRotLine) And (iBar < ChartLine2.UBound) Then
            iBar = iBar + 1
            BarIdx = iBar
            If ForRepaintOverview = False Then
                tmpGidx = Right("000000" & CStr(iBar), 6)
                GetArrFlightData ArrLine, iBar
                GetDepFlightData DepLine, iBar
                picSortTime(iBar).Visible = False
                RotBest = TabRotFlightsTab.GetFieldValue(CurLine, "BEST")
                IsValidRotScalePos = GetTimeScalePos(RotBest, RotBestValue, RotScalePos)
                picSortTime(iBar).Left = RotScalePos - (picSortTime(iBar).Width \ 2) - 15
                If (ArrLine >= 0) And (DepLine >= 0) Then
                     'ROTATION
                     DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                     IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                     ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                     IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                     
                     ArrDuration = DateDiff("n", ArrTifaValue, DepTifdValue)
                     
                     RotWidth = DepScalePos - ArrScalePos + 15
                     If RotWidth >= MinRotWidth Then
                         lblLeftBarCover(iBar).Tag = "7"
                     Else
                         lblLeftBarCover(iBar).Tag = "1"
                     End If
                     DepDuration = ArrDuration \ 2
                     If DepDuration > MaxDepDura Then DepDuration = MaxDepDura
                     If DepDuration < MinDepDura Then DepDuration = MinDepDura
                     ArrWidth = ArrDuration * MinuteWidth
                     If ArrScalePos < -200000 Then
                         ArrScalePos = -200000
                         ArrWidth = DepScalePos - ArrScalePos
                     End If
                     DepWidth = DepDuration * MinuteWidth
                     If DepWidth > MaxDepWidth Then DepWidth = MaxDepWidth
                     If DepWidth < MinDepWidth Then DepWidth = MinDepWidth
                     DepLeft = DepScalePos - DepWidth
                     If ArrWidth < MinArrWidth Then ArrWidth = MinArrWidth
                     ArrLeft = ArrScalePos
                     ArrVisible = True
                     DepVisible = True
                 ElseIf DepLine >= 0 Then
                     'SINGLE DEPARTURE
                     DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                     IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                     DepWidth = MaxDepWidth
                     DepDuration = MaxDepDura
                     DepLeft = DepScalePos - DepWidth
                     ArrLeft = 0
                     ArrWidth = 150
                     ArrDuration = -1
                     ArrVisible = False
                     DepVisible = True
                 Else
                     'SINGLE ARRIVAL
                     ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                     IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                     ArrWidth = MaxArrWidth
                     ArrDuration = MaxArrDura
                     ArrLeft = ArrScalePos
                     DepLeft = 0
                     DepWidth = 150
                     DepDuration = -1
                     ArrVisible = True
                     DepVisible = False
                 End If
                 
                 lblArrBar(iBar).Left = ArrLeft
                 lblArrBar(iBar).Width = ArrWidth
                 lblArrBar(iBar).Tag = CStr(ArrDuration)
                 lblArrBar(iBar).Visible = ArrVisible
                 lblArrJob1(iBar).Left = ArrLeft
                 lblArrJob1(iBar).Width = ArrWidth
                 lblArrJob2(iBar).Left = ArrLeft
                 lblArrJob2(iBar).Width = ArrWidth
                 lblArrJob1(iBar).ZOrder
                 lblArrJob2(iBar).ZOrder
                 
                 lblArrJob1(iBar).Visible = False
                 lblArrJob2(iBar).Visible = False
                
                 picArr1(iBar).Visible = False
                 picArr2(iBar).Visible = False
                 picArr3(iBar).Visible = False
                 picArr4(iBar).Visible = False
                 If ArrVisible = True Then
                     PicLeft = ArrLeft
                     If picArr1(iBar).Tag <> "" Then
                         picArr1(iBar).Left = PicLeft
                         picArr1(iBar).Visible = True
                         picArr1(iBar).ZOrder
                         PicLeft = PicLeft + picArr1(iBar).Width
                     End If
                     If picArr2(iBar).Tag <> "" Then
                         picArr2(iBar).Left = PicLeft
                         picArr2(iBar).Visible = True
                         picArr2(iBar).ZOrder
                         PicLeft = PicLeft + picArr2(iBar).Width
                     End If
                     If picArr3(iBar).Tag <> "" Then
                         picArr3(iBar).Left = PicLeft
                         picArr3(iBar).Visible = True
                         picArr3(iBar).ZOrder
                         PicLeft = PicLeft + picArr3(iBar).Width
                     End If
                     If picArr4(iBar).Tag <> "" Then
                         picArr4(iBar).Left = PicLeft
                         picArr4(iBar).Visible = True
                         picArr4(iBar).ZOrder
                     End If
                 End If
                
                 lblDepBar(iBar).Left = DepLeft
                 lblDepBar(iBar).Width = DepWidth
                 lblDepBar(iBar).Tag = CStr(DepDuration)
                 lblDepBar(iBar).Visible = DepVisible
                 lblDepJob1(iBar).Left = DepLeft
                 lblDepJob1(iBar).Width = DepWidth
                 lblDepJob2(iBar).Left = DepLeft
                 lblDepJob2(iBar).Width = DepWidth
                 lblDepBar(iBar).ZOrder
                 lblDepJob1(iBar).ZOrder
                 lblDepJob2(iBar).ZOrder
                 
                 lblDepJob1(iBar).Visible = False
                 lblDepJob2(iBar).Visible = False
                 
                 picDep1(iBar).Visible = False
                 picDep2(iBar).Visible = False
                 picDep3(iBar).Visible = False
                 picDep4(iBar).Visible = False
                 If DepVisible = True Then
                     PicLeft = DepLeft + DepWidth
                     If picDep4(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep4(iBar).Width
                         picDep4(iBar).Left = PicLeft
                         picDep4(iBar).Visible = True
                         picDep4(iBar).ZOrder
                     End If
                     If picDep3(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep3(iBar).Width
                         picDep3(iBar).Left = PicLeft
                         picDep3(iBar).Visible = True
                         picDep3(iBar).ZOrder
                     End If
                     If picDep2(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep2(iBar).Width
                         picDep2(iBar).Left = PicLeft
                         picDep2(iBar).Visible = True
                         picDep2(iBar).ZOrder
                     End If
                     If picDep1(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep1(iBar).Width
                         picDep1(iBar).Left = PicLeft
                         picDep1(iBar).Visible = True
                         picDep1(iBar).ZOrder
                     End If
                 End If
                 'picSortTime(iBar).Visible = True
                 picSortTime(iBar).ZOrder
                 
                 ArrColor = SetArrBarColors(ArrLine, DepLine, iBar)
                 DepColor = SetDepBarColors(ArrLine, DepLine, iBar)
                 
                 If chkSelDeco(0).Value = 0 Then
                     NewLeft = lblArrBar(iBar).Left - ArrFlight(iBar).Width - 30
                     ArrFlight(iBar).Left = NewLeft
                 End If
                 ArrFlight(iBar).Visible = lblArrBar(iBar).Visible
                 If chkSelDeco(1).Value = 0 Then
                     NewLeft = lblDepBar(iBar).Left + lblDepBar(iBar).Width + 30
                     DepFlight(iBar).Left = NewLeft
                 End If
                 DepFlight(iBar).Visible = lblDepBar(iBar).Visible
                 
            End If
        End If
        If BarIdx >= 0 Then
            'For RepaintOverview (saves some milliseconds)
            If ArrLine >= 0 Then ArrColor = lblArrBar(BarIdx).BackColor
            If DepLine >= 0 Then DepColor = lblDepBar(BarIdx).BackColor
        Else
             ArrColor = SetArrBarColors(ArrLine, DepLine, iBar)
             DepColor = SetDepBarColors(ArrLine, DepLine, iBar)
        End If
                'REMOVED
        GocChartOverview.DrawFlightRotation CurLine, ArrLine, DepLine, ArrColor, DepColor, BarIdx, True
        If ForRepaintOverview = False Then
            tmpGidx = tmpView & "," & tmpGidx
            TabRotFlightsTab.SetFieldValues CurLine, "VIEW,GOCX", tmpGidx
        End If
    Next
    If ForRepaintOverview = False Then
        jBar = iBar
        If jBar < 0 Then jBar = 0
        TopPos = ChartLine2(jBar).Top + ChartLine2(0).Height
        CurChartHeight = TopPos + 60
        VScrollArea(0).Height = CurChartHeight
        HScrollArea(0).Height = CurChartHeight
        TimeRange(2).Height = CurChartHeight
        TimeRange(3).Height = CurChartHeight
        LeftScale(0).Height = CurChartHeight
        RightScale(0).Height = CurChartHeight
        NowTime(1).Height = CurChartHeight
        RangeLine(0).Height = CurChartHeight
        RangeLine(1).Height = CurChartHeight
        TimeRange(2).ZOrder
        TimeRange(3).ZOrder
        
        LoadedBars = CLng(jBar + 1)
        i = CInt(LoadedBars - VisibleBars)
        If i < 0 Then i = 0
        VScroll1(0).Max = i
        VScroll2(0).Max = i
        
        TabRotFlightsTab.AutoSizeColumns
        IdxColNo = CLng(GetRealItemNo(TabRotFlightsTab.LogicalFieldList, "GOCX"))
        TabRotFlightsTab.IndexCreate "GOCX", IdxColNo
        TabRotFlightsTab.Refresh
        Me.Refresh
        'chkSelDeco(1).Value = 0
        'chkSelDeco(0).Value = 0
        'LeftScale(0).Refresh
        'RightScale(0).Refresh
        Me.Refresh
    End If
                'REMOVED
    GocChartOverview.FinishChartArea
    AdjustOverviewPanel
    Screen.MousePointer = 0
End Sub

Public Sub UpdateFlightRotation(RotLine As Long, ChartBarIdx As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim DepLine As Long
    Dim ArrLine As Long
    Dim iBar As Integer
    Dim jBar As Integer
    Dim BarIdx As Integer
    Dim i As Integer
    Dim tmpData As String
    Dim tmpAidx As String
    Dim tmpDidx As String
    Dim tmpRidx As String
    Dim tmpGidx As String
    Dim tmpView As String
    Dim tmpHitLine As String
    Dim RotBest As String
    Dim ArrTifa As String
    Dim DepTifd As String
    Dim DepOfbl As String
    Dim TopPos As Long
    Dim RotScalePos As Long
    Dim ArrScalePos As Long
    Dim DepScalePos As Long
    Dim DepLeft As Long
    Dim ArrLeft As Long
    Dim NewLeft As Long
    Dim PicLeft As Long
    Dim RotWidth As Long
    Dim ArrWidth As Long
    Dim DepWidth As Long
    Dim MinRotWidth As Long
    Dim ArrDuration As Long
    Dim DepDuration As Long
    Dim MinDepDura As Long
    Dim MaxDepDura As Long
    Dim MinArrDura As Long
    Dim MaxArrDura As Long
    Dim IdxColNo As Long
    Dim ArrColor As Long
    Dim DepColor As Long
    Dim IsValidRotScalePos As Boolean
    Dim IsValidArrScalePos As Boolean
    Dim IsValidDepScalePos As Boolean
    Dim ArrVisible As Boolean
    Dim DepVisible As Boolean
    Dim ShowJobs As Boolean
    Dim RotBestValue
    Dim DepTifdValue
    Dim ArrTifaValue
    Screen.MousePointer = 11
    
    
'    CurLine = TopLine
'    If CurLine < 0 Then CurLine = FirstRotLine
    
'CurLine = 150

'    Load GocChartOverview
'    GocChartOverview.StopActivities
'    If ForRepaintOverview = False Then
'        tmpData = Trim(GetIniEntry(myIniFullName, "MAIN", "", "SHOW_ALL_ICONS", "NO"))
'        If tmpData = "YES" Then ShowAllIcons = True Else ShowAllIcons = False
'        For i = 0 To lblArrPicCount.UBound
'            lblArrPicCount(i).Caption = ""
'            lblArrPicCount(i).Tag = ""
'        Next
'        For i = 0 To lblDepPicCount.UBound
'            lblDepPicCount(i).Caption = ""
'            lblDepPicCount(i).Tag = ""
'        Next
'        LeftScale(0).Refresh
'        RightScale(0).Refresh
'        CreateTimeScaleArea TimeScale(0)
        MaxDepDura = 60
        MaxArrDura = 60
        MinDepDura = 20
        MinArrDura = 20
        MaxDepWidth = MaxDepDura * MinuteWidth
        MaxArrWidth = MaxArrDura * MinuteWidth
        MinDepWidth = MinDepDura * MinuteWidth
        MinArrWidth = MinArrDura * MinuteWidth
        MinRotWidth = MinuteWidth * 15
        
'        TabRotFlightsTab.IndexDestroy "GOCX"
'        ShowJobs = False
'    End If
'    MaxLine = TabRotFlightsTab.GetLineCount
'    MaxLine = MaxLine - 1
    
'    FirstRotLine = CurLine
    
    
'    TotalRotLines = MaxLine + 1
'    AvailableBars = CLng(CurBarMax + 1)
'    LastRotLine = FirstRotLine + AvailableBars - 1
'    If LastRotLine >= TotalRotLines Then
'        LastRotLine = TotalRotLines - 1
'        FirstRotLine = LastRotLine - AvailableBars + 1
'        If FirstRotLine < 0 Then FirstRotLine = 0
'    End If
'    GocChartOverview.InitChartArea
    
    iBar = -1
    For CurLine = RotLine To RotLine
        iBar = ChartBarIdx
        BarIdx = iBar
        ArrLine = -1
        ArrColor = 0
        DepLine = -1
        DepColor = 0
        'tmpView = "1"
        'tmpGidx = "------"
        tmpRidx = TabRotFlightsTab.GetFieldValue(CurLine, "RIDX")
        tmpHitLine = TabArrFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
        If tmpHitLine <> "" Then ArrLine = Val(tmpHitLine)
        tmpHitLine = TabDepFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
        If tmpHitLine <> "" Then DepLine = Val(tmpHitLine)
        If (CurLine >= FirstRotLine) And (iBar < ChartLine2.UBound) Then
            'If ForRepaintOverview = False Then
                tmpGidx = Right("000000" & CStr(iBar), 6)
                GetArrFlightData ArrLine, iBar
                GetDepFlightData DepLine, iBar
                picSortTime(iBar).Visible = False
                RotBest = TabRotFlightsTab.GetFieldValue(CurLine, "BEST")
                IsValidRotScalePos = GetTimeScalePos(RotBest, RotBestValue, RotScalePos)
                picSortTime(iBar).Left = RotScalePos - (picSortTime(iBar).Width \ 2) - 15
                If (ArrLine >= 0) And (DepLine >= 0) Then
                     'ROTATION
                     DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                     IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                     ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                     IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                     
                     ArrDuration = DateDiff("n", ArrTifaValue, DepTifdValue)
                     
                     RotWidth = DepScalePos - ArrScalePos + 15
                     If RotWidth >= MinRotWidth Then
                         lblLeftBarCover(iBar).Tag = "7"
                     Else
                         lblLeftBarCover(iBar).Tag = "1"
                     End If
                     DepDuration = ArrDuration \ 2
                     If DepDuration > MaxDepDura Then DepDuration = MaxDepDura
                     If DepDuration < MinDepDura Then DepDuration = MinDepDura
                     ArrWidth = ArrDuration * MinuteWidth
                     If ArrScalePos < -200000 Then
                         ArrScalePos = -200000
                         ArrWidth = DepScalePos - ArrScalePos
                     End If
                     DepWidth = DepDuration * MinuteWidth
                     If DepWidth > MaxDepWidth Then DepWidth = MaxDepWidth
                     If DepWidth < MinDepWidth Then DepWidth = MinDepWidth
                     DepLeft = DepScalePos - DepWidth
                     If ArrWidth < MinArrWidth Then ArrWidth = MinArrWidth
                     ArrLeft = ArrScalePos
                     ArrVisible = True
                     DepVisible = True
                 ElseIf DepLine >= 0 Then
                     'SINGLE DEPARTURE
                     DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                     IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                     DepWidth = MaxDepWidth
                     DepDuration = MaxDepDura
                     DepLeft = DepScalePos - DepWidth
                     ArrLeft = 0
                     ArrWidth = 150
                     ArrDuration = -1
                     ArrVisible = False
                     DepVisible = True
                 Else
                     'SINGLE ARRIVAL
                     ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                     IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                     ArrWidth = MaxArrWidth
                     ArrDuration = MaxArrDura
                     ArrLeft = ArrScalePos
                     DepLeft = 0
                     DepWidth = 150
                     DepDuration = -1
                     ArrVisible = True
                     DepVisible = False
                 End If
                 
                 lblArrBar(iBar).Left = ArrLeft
                 lblArrBar(iBar).Width = ArrWidth
                 lblArrBar(iBar).Tag = CStr(ArrDuration)
                 lblArrBar(iBar).Visible = ArrVisible
                 lblArrJob1(iBar).Left = ArrLeft
                 lblArrJob1(iBar).Width = ArrWidth
                 lblArrJob2(iBar).Left = ArrLeft
                 lblArrJob2(iBar).Width = ArrWidth
                 lblArrJob1(iBar).ZOrder
                 lblArrJob2(iBar).ZOrder
                 
                 lblArrJob1(iBar).Visible = False
                 lblArrJob2(iBar).Visible = False
                
                 picArr1(iBar).Visible = False
                 picArr2(iBar).Visible = False
                 picArr3(iBar).Visible = False
                 picArr4(iBar).Visible = False
                 If ArrVisible = True Then
                     PicLeft = ArrLeft
                     If picArr1(iBar).Tag <> "" Then
                         picArr1(iBar).Left = PicLeft
                         picArr1(iBar).Visible = True
                         picArr1(iBar).ZOrder
                         PicLeft = PicLeft + picArr1(iBar).Width
                     End If
                     If picArr2(iBar).Tag <> "" Then
                         picArr2(iBar).Left = PicLeft
                         picArr2(iBar).Visible = True
                         picArr2(iBar).ZOrder
                         PicLeft = PicLeft + picArr2(iBar).Width
                     End If
                     If picArr3(iBar).Tag <> "" Then
                         picArr3(iBar).Left = PicLeft
                         picArr3(iBar).Visible = True
                         picArr3(iBar).ZOrder
                         PicLeft = PicLeft + picArr3(iBar).Width
                     End If
                     If picArr4(iBar).Tag <> "" Then
                         picArr4(iBar).Left = PicLeft
                         picArr4(iBar).Visible = True
                         picArr4(iBar).ZOrder
                     End If
                 End If
                
                 lblDepBar(iBar).Left = DepLeft
                 lblDepBar(iBar).Width = DepWidth
                 lblDepBar(iBar).Tag = CStr(DepDuration)
                 lblDepBar(iBar).Visible = DepVisible
                 lblDepJob1(iBar).Left = DepLeft
                 lblDepJob1(iBar).Width = DepWidth
                 lblDepJob2(iBar).Left = DepLeft
                 lblDepJob2(iBar).Width = DepWidth
                 lblDepBar(iBar).ZOrder
                 lblDepJob1(iBar).ZOrder
                 lblDepJob2(iBar).ZOrder
                 
                 lblDepJob1(iBar).Visible = False
                 lblDepJob2(iBar).Visible = False
                 
                 picDep1(iBar).Visible = False
                 picDep2(iBar).Visible = False
                 picDep3(iBar).Visible = False
                 picDep4(iBar).Visible = False
                 If DepVisible = True Then
                     PicLeft = DepLeft + DepWidth
                     If picDep4(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep4(iBar).Width
                         picDep4(iBar).Left = PicLeft
                         picDep4(iBar).Visible = True
                         picDep4(iBar).ZOrder
                     End If
                     If picDep3(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep3(iBar).Width
                         picDep3(iBar).Left = PicLeft
                         picDep3(iBar).Visible = True
                         picDep3(iBar).ZOrder
                     End If
                     If picDep2(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep2(iBar).Width
                         picDep2(iBar).Left = PicLeft
                         picDep2(iBar).Visible = True
                         picDep2(iBar).ZOrder
                     End If
                     If picDep1(iBar).Tag <> "" Then
                         PicLeft = PicLeft - picDep1(iBar).Width
                         picDep1(iBar).Left = PicLeft
                         picDep1(iBar).Visible = True
                         picDep1(iBar).ZOrder
                     End If
                 End If
                 'picSortTime(iBar).Visible = True
                 picSortTime(iBar).ZOrder
                 
                 ArrColor = SetArrBarColors(ArrLine, DepLine, iBar)
                 DepColor = SetDepBarColors(ArrLine, DepLine, iBar)
                 
                 If chkSelDeco(0).Value = 0 Then
                     NewLeft = lblArrBar(iBar).Left - ArrFlight(iBar).Width - 30
                     ArrFlight(iBar).Left = NewLeft
                 End If
                 ArrFlight(iBar).Visible = lblArrBar(iBar).Visible
                 If chkSelDeco(1).Value = 0 Then
                     NewLeft = lblDepBar(iBar).Left + lblDepBar(iBar).Width + 30
                     DepFlight(iBar).Left = NewLeft
                 End If
                 DepFlight(iBar).Visible = lblDepBar(iBar).Visible
                 
            'End If
        End If
        If BarIdx >= 0 Then
            'For RepaintOverview (saves some milliseconds)
            If ArrLine >= 0 Then ArrColor = lblArrBar(BarIdx).BackColor
            If DepLine >= 0 Then DepColor = lblDepBar(BarIdx).BackColor
        Else
             ArrColor = SetArrBarColors(ArrLine, DepLine, iBar)
             DepColor = SetDepBarColors(ArrLine, DepLine, iBar)
        End If
        GocChartOverview.DrawFlightRotation CurLine, ArrLine, DepLine, ArrColor, DepColor, BarIdx, True
'        If ForRepaintOverview = False Then
'            tmpGidx = tmpView & "," & tmpGidx
'            TabRotFlightsTab.SetFieldValues CurLine, "VIEW,GOCX", tmpGidx
'        End If
    Next
'    If ForRepaintOverview = False Then
'        jBar = iBar
'        If jBar < 0 Then jBar = 0
'        TopPos = ChartLine2(jBar).Top + ChartLine2(0).Height
'        CurChartHeight = TopPos + 60
'        VScrollArea(0).Height = CurChartHeight
'        HScrollArea(0).Height = CurChartHeight
'        TimeRange(2).Height = CurChartHeight
'        TimeRange(3).Height = CurChartHeight
'        LeftScale(0).Height = CurChartHeight
'        RightScale(0).Height = CurChartHeight
'        NowTime(1).Height = CurChartHeight
'        RangeLine(0).Height = CurChartHeight
'        RangeLine(1).Height = CurChartHeight
'        TimeRange(2).ZOrder
'        TimeRange(3).ZOrder
        
'        LoadedBars = CLng(jBar + 1)
'        i = CInt(LoadedBars - VisibleBars)
'        If i < 0 Then i = 0
'        VScroll1(0).Max = i
'        VScroll2(0).Max = i
        
'        TabRotFlightsTab.AutoSizeColumns
'        IdxColNo = CLng(GetRealItemNo(TabRotFlightsTab.LogicalFieldList, "GOCX"))
'        TabRotFlightsTab.IndexCreate "GOCX", IdxColNo
'        TabRotFlightsTab.Refresh
'        Me.Refresh
        'chkSelDeco(1).Value = 0
        'chkSelDeco(0).Value = 0
        'LeftScale(0).Refresh
        'RightScale(0).Refresh
'        Me.Refresh
'    End If
'    GocChartOverview.FinishChartArea
'    AdjustOverviewPanel
    Screen.MousePointer = 0
End Sub

Private Sub GetArrFlightData(AftLineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim iBar As Integer
    Dim ArrFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim ArrFldItm As Integer
    Dim TimeOffs As Integer
    If chkUtc(1).Value = 1 Then TimeOffs = UtcTimeDiff Else TimeOffs = 0
    CurLine = AftLineNo
    iBar = BarIdx
    ClearArrFlight iBar
    ArrFields = "FTYP,FLNO,FLTI,ORG3,VIA3,STOA,ETAI,ONBL,PSTA,GTA1,REGN,ACT3,VERS"
    ArrFldItm = 1
    FldName = "START"
    While FldName <> ""
        FldName = GetItem(ArrFields, ArrFldItm, ",")
        If FldName <> "" Then
            FldValue = Trim(TabArrFlightsTab.GetFieldValue(CurLine, FldName))
            Select Case FldName
                Case "FTYP"
                    lblArrFtyp(iBar).Tag = FldValue
                    lblArrFtyp(iBar).Caption = FldValue
                Case "FLNO"
                    lblArrFlno(iBar).Tag = FldValue
                    lblArrFlno(iBar).Caption = FldValue
                Case "FLTI"
                    lblArrFlti(iBar).Tag = FldValue
                    lblArrFlti(iBar).Caption = FldValue
                Case "ORG3"
                    lblArrOrg3(iBar).Tag = FldValue
                    lblArrOrg3(iBar).Caption = FldValue
                Case "VIA3"
                    lblArrVia3(iBar).Tag = FldValue
                    lblArrVia3(iBar).Caption = FldValue
                Case "STOA"
                    lblArrStoa(iBar).Tag = FldValue
                    SetTimeFieldCaption lblArrStoa(iBar), TimeOffs
                Case "ETAI"
                    lblArrEtai(iBar).Tag = FldValue
                    SetTimeFieldCaption lblArrEtai(iBar), TimeOffs
                Case "ONBL"
                    lblArrOnbl(iBar).Tag = FldValue
                    SetTimeFieldCaption lblArrOnbl(iBar), TimeOffs
                Case "PSTA"
                    lblArrPsta(iBar).Tag = FldValue
                    lblArrPsta(iBar).Caption = FldValue
                Case "GTA1"
                    lblArrGta1(iBar).Tag = FldValue
                    lblArrGta1(iBar).Caption = FldValue
                Case "REGN"
                    lblArrRegn(iBar).Tag = FldValue
                    lblArrRegn(iBar).Caption = FldValue
                Case "ACT3"
                    lblArrAct3(iBar).Tag = FldValue
                    lblArrAct3(iBar).Caption = FldValue
                Case "VERS"
                    If ShowAllIcons Then
                        If FldValue = "" Then FldValue = "ABCDEFG"
                    End If
                    CreateArrStatusPics iBar, FldValue
                Case Else
            End Select
        End If
        ArrFldItm = ArrFldItm + 1
    Wend
    ArrFlight(iBar).Visible = True
End Sub

Private Function SetArrBarColors(ArrLineNo As Long, DepLineNo As Long, BarIdx As Integer) As Long
    Dim iBar As Integer
    Dim ArrFtyp As String
    Dim ArrTisa As String
    Dim DepTisd As String
    Dim BarBackColor As Long
    Dim BarContext As String
    BarBackColor = 0
    If ArrLineNo >= 0 Then
        iBar = BarIdx
        SetArrPanelColors iBar, vbBlack, vbWhite
        ArrFtyp = Trim(TabArrFlightsTab.GetFieldValue(ArrLineNo, "FTYP"))
        If ArrFtyp = "" Then ArrFtyp = " "
        If InStr("XND", ArrFtyp) > 0 Then
            BarBackColor = vbRed
            If iBar >= 0 Then
                lblArrBar(iBar).BackColor = BarBackColor
                lblArrBar(iBar).ForeColor = vbWhite
                lblArrFlno(iBar).BackColor = vbRed
                lblArrFlno(iBar).ForeColor = vbWhite
                lblArrBar(iBar).ToolTipText = ""
                Select Case ArrFtyp
                    Case "X"
                        lblArrBar(iBar).Caption = "CXX"
                        picArrStat(iBar).Picture = picAftFtyp(3).Picture
                    Case "N"
                        lblArrBar(iBar).Caption = "NOP"
                        picArrStat(iBar).Picture = picAftFtyp(4).Picture
                    Case "D"
                        lblArrBar(iBar).Caption = "DIV"
                        picArrStat(iBar).Picture = picAftFtyp(5).Picture
                    Case Else
                        'We never come here
                End Select
            End If
        Else
            If iBar >= 0 Then
                Select Case ArrFtyp
                    Case "O"
                        picArrStat(iBar).Picture = picAftFtyp(0).Picture
                    Case "S"
                        picArrStat(iBar).Picture = picAftFtyp(1).Picture
                    Case " "
                        picArrStat(iBar).Picture = picAftFtyp(2).Picture
                    Case Else
                        picArrStat(iBar).Picture = picAftFtyp(2).Picture
                End Select
                lblArrBar(iBar).Caption = ""
            End If
            ArrTisa = Trim(TabArrFlightsTab.GetFieldValue(ArrLineNo, "TISA"))
            If ArrTisa = "" Then ArrTisa = " "
            DepTisd = ""
            If DepLineNo >= 0 Then DepTisd = Trim(TabDepFlightsTab.GetFieldValue(DepLineNo, "TISD"))
            If DepTisd <> "A" Then
                Select Case ArrTisa
                    Case "S"
                        BarBackColor = RGB(235, 235, 255)
                        BarContext = "Scheduled"
                    Case "E"
                        BarBackColor = vbWhite
                        BarContext = "Estimated"
                    Case "T"
                        BarBackColor = LightYellow
                        BarContext = "Ten Miles Out"
                    Case "L"
                        BarBackColor = LightGreen
                        BarContext = "Landed"
                    Case "O"
                        BarBackColor = vbGreen
                        BarContext = "Onblock"
                    Case Else
                        BarBackColor = vbMagenta
                        BarContext = "Time Status Unknown"
                End Select
                If iBar >= 0 Then
                    lblArrBar(iBar).BackColor = BarBackColor
                    lblArrBar(iBar).ToolTipText = BarContext
                End If
            Else
                BarBackColor = LightGrey
                If iBar >= 0 Then
                    SetArrPanelColors iBar, vbBlack, LightGrey
                    lblArrBar(iBar).BackColor = BarBackColor
                    lblArrBar(iBar).ToolTipText = "Already Departed"
                End If
            End If
        End If
    End If
    SetArrBarColors = BarBackColor
End Function

Private Sub CreateArrStatusPics(iBar As Integer, FldValue As String)
    Dim i As Integer
    Dim j As Integer
    Dim iLen As Integer
    Dim iCnt As Integer
    Dim jIdx As Integer
    Dim tmpChar As String
    Dim tmpVers As String
    tmpVers = UCase(Trim(FldValue))
    tmpVers = Replace(tmpVers, " ", "", 1, -1, vbBinaryCompare)
    iCnt = 0
    iLen = Len(tmpVers)
    For i = 1 To iLen
        tmpChar = Mid(tmpVers, i, 1)
        jIdx = -1
        For j = 0 To lblArrPicType.UBound
            If lblArrPicType(j).Tag = tmpChar Then
                jIdx = j
                Exit For
            End If
        Next
        If jIdx >= 0 Then
            If iCnt < 4 Then
                iCnt = iCnt + 1
                Select Case iCnt
                    Case 1
                        SetArrStatusPic iBar, jIdx, picArr1(iBar)
                    Case 2
                        SetArrStatusPic iBar, jIdx, picArr2(iBar)
                    Case 3
                        SetArrStatusPic iBar, jIdx, picArr3(iBar)
                    Case 4
                        SetArrStatusPic iBar, jIdx, picArr4(iBar)
                    Case Else
                End Select
            End If
        End If
    Next
    'Clear unused pics
    iCnt = iCnt + 1
    For i = iCnt To 4
        Select Case i
            Case 1
                ClearArrStatusPic iBar, picArr1(iBar)
            Case 2
                ClearArrStatusPic iBar, picArr2(iBar)
            Case 3
                ClearArrStatusPic iBar, picArr3(iBar)
            Case 4
                ClearArrStatusPic iBar, picArr4(iBar)
            Case Else
        End Select
    Next
End Sub
Private Sub SetArrStatusPic(iBar As Integer, jIdx As Integer, CurPic As Image)
    Dim PicCnt As Integer
    Dim tmpTag As String
    CurPic.Picture = picArrPic(jIdx).Picture
    PicCnt = Val(lblArrPicCount(jIdx).Caption) + 1
    lblArrPicCount(jIdx).Caption = CStr(PicCnt)
    tmpTag = CStr(jIdx)
    CurPic.Tag = tmpTag
    tmpTag = lblArrPicText(jIdx).Caption
    CurPic.ToolTipText = tmpTag
End Sub
Private Sub ClearArrStatusPic(iBar As Integer, CurPic As Image)
    CurPic.Tag = ""
End Sub

Private Sub GetDepFlightData(AftLineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim iBar As Integer
    Dim DepFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim GetValue As String
    Dim DepFldItm As Integer
    Dim TimeOffs As Integer
    If chkUtc(1).Value = 1 Then TimeOffs = UtcTimeDiff Else TimeOffs = 0
    CurLine = AftLineNo
    iBar = BarIdx
    ClearDepFlight iBar
    DepFields = "FTYP,FLNO,FLTI,DES3,VIA3,STOD,ETDI,OFBL,PSTD,GTD1,REGN,ACT3,VERS"
    DepFldItm = 1
    FldName = "START"
    While FldName <> ""
        FldName = GetItem(DepFields, DepFldItm, ",")
        If FldName <> "" Then
            FldValue = TabDepFlightsTab.GetFieldValue(CurLine, FldName)
            Select Case FldName
                Case "FTYP"
                    lblDepFtyp(iBar).Tag = FldValue
                    lblDepFtyp(iBar).Caption = FldValue
                Case "FLNO"
                    lblDepFlno(iBar).Tag = FldValue
                    lblDepFlno(iBar).Caption = FldValue
                Case "FLTI"
                    lblDepFlti(iBar).Tag = FldValue
                    lblDepFlti(iBar).Caption = FldValue
                Case "DES3"
                    lblDepDes3(iBar).Tag = FldValue
                    lblDepDes3(iBar).Caption = FldValue
                Case "VIA3"
                    lblDepVia3(iBar).Tag = FldValue
                    lblDepVia3(iBar).Caption = FldValue
                Case "STOD"
                    lblDepStod(iBar).Tag = FldValue
                    SetTimeFieldCaption lblDepStod(iBar), TimeOffs
                Case "ETDI"
                    lblDepEtdi(iBar).Tag = FldValue
                    SetTimeFieldCaption lblDepEtdi(iBar), TimeOffs
                Case "OFBL"
                    lblDepOfbl(iBar).Tag = FldValue
                    SetTimeFieldCaption lblDepOfbl(iBar), TimeOffs
                Case "PSTD"
                    lblDepPstd(iBar).Tag = FldValue
                    lblDepPstd(iBar).Caption = FldValue
                Case "GTD1"
                    lblDepGtd1(iBar).Tag = FldValue
                    lblDepGtd1(iBar).Caption = FldValue
                Case "REGN"
                    lblDepRegn(iBar).Tag = FldValue
                    lblDepRegn(iBar).Caption = FldValue
                Case "ACT3"
                    lblDepAct3(iBar).Tag = FldValue
                    lblDepAct3(iBar).Caption = FldValue
                Case "VERS"
                    If ShowAllIcons Then
                        If FldValue = "" Then FldValue = "ABCDEFG"
                    End If
                    CreateDepStatusPics iBar, FldValue
                Case Else
            End Select
        End If
        DepFldItm = DepFldItm + 1
    Wend
    DepFlight(iBar).Visible = True
End Sub

Private Function SetDepBarColors(ArrLineNo As Long, DepLineNo As Long, BarIdx As Integer) As Long
    Dim iBar As Integer
    Dim DepFtyp As String
    Dim ArrTisa As String
    Dim DepTisd As String
    Dim BarBackColor As Long
    Dim BarContext As String
    Dim FldValue As String
    Dim GetValue As String
    BarBackColor = 0
    If DepLineNo >= 0 Then
        iBar = BarIdx
        SetDepPanelColors iBar, vbBlack, vbWhite
        DepFtyp = Trim(TabDepFlightsTab.GetFieldValue(DepLineNo, "FTYP"))
        If DepFtyp = "" Then DepFtyp = " "
        If InStr("XND", DepFtyp) > 0 Then
            BarBackColor = vbRed
            If iBar >= 0 Then
                lblDepBar(iBar).BackColor = vbRed
                lblDepBar(iBar).ForeColor = vbWhite
                lblDepFlno(iBar).BackColor = vbRed
                lblDepFlno(iBar).ForeColor = vbWhite
                lblDepBar(iBar).ToolTipText = ""
                Select Case DepFtyp
                    Case "X"
                        lblDepBar(iBar).Caption = "CXX"
                        picDepStat(iBar).Picture = picAftFtyp(3).Picture
                    Case "N"
                        lblDepBar(iBar).Caption = "NOP"
                        picDepStat(iBar).Picture = picAftFtyp(4).Picture
                    Case "D"
                        lblDepBar(iBar).Caption = "DIV"
                        picDepStat(iBar).Picture = picAftFtyp(5).Picture
                    Case Else
                        'We never come here
                End Select
            End If
        Else
            If iBar >= 0 Then
                Select Case DepFtyp
                    Case "O"
                        picDepStat(iBar).Picture = picAftFtyp(0).Picture
                    Case "S"
                        picDepStat(iBar).Picture = picAftFtyp(1).Picture
                    Case " "
                        picDepStat(iBar).Picture = picAftFtyp(2).Picture
                    Case Else
                        picDepStat(iBar).Picture = picAftFtyp(2).Picture
                End Select
                lblDepBar(iBar).Caption = ""
            End If
            DepTisd = Trim(TabDepFlightsTab.GetFieldValue(DepLineNo, "TISD"))
            If DepTisd = "" Then DepTisd = " "
            'ArrTisa = ""
            'If ArrLineNo >= 0 Then ArrTisa = Trim(TabArrFlightsTab.GetFieldValue(ArrLineNo, "TISA"))
            'If ArrTisa <> "?" Then
                Select Case DepTisd
                    Case "S"
                        BarBackColor = RGB(235, 235, 255)
                        BarContext = "Scheduled"
                    Case "E"
                        BarBackColor = vbWhite
                        BarContext = "Estimated"
                    Case "O"
                        BarBackColor = LightBlue
                        BarContext = "Offblock"
                    Case "A"
                        BarBackColor = LightGrey
                        BarContext = "Airborne"
                        SetDepPanelColors iBar, vbBlack, LightGrey
                    Case Else
                        BarBackColor = vbMagenta
                        BarContext = "Time Status Unknown"
                End Select
                If iBar >= 0 Then
                    lblDepBar(iBar).BackColor = BarBackColor
                    lblDepBar(iBar).ToolTipText = BarContext
                    lblDepDes3(iBar).ToolTipText = "Destination"
                    lblDepVia3(iBar).ToolTipText = "Next Station"
                    If DepTisd <> "A" Then
                        FldValue = lblDepDes3(iBar).Tag
                        If FldValue <> "" Then
                            GetValue = HiddenData.BasicDataLookUp("ATRTAB", "APC3", FldValue, "ATRP", True)
                            If GetValue <> "" Then
                                lblDepDes3(iBar).BackColor = vbYellow
                                lblDepDes3(iBar).ToolTipText = "Dest. Restriction: " & GetValue
                            End If
                        End If
                        FldValue = lblDepVia3(iBar).Tag
                        If FldValue <> "" Then
                            GetValue = HiddenData.BasicDataLookUp("ATRTAB", "APC3", FldValue, "ATRP", True)
                            If GetValue <> "" Then
                                lblDepVia3(iBar).BackColor = vbYellow
                                lblDepVia3(iBar).ToolTipText = "Next Station Restriction: " & GetValue
                            End If
                        End If
                    End If
                End If
            'Else
                'SetArrPanelColors iBar, vbBlack, LightGrey
                'lblArrBar(iBar).BackColor = LightGrey
                'lblArrBar(iBar).ToolTipText = "Already Departed"
            'End If
        End If
    End If
    SetDepBarColors = BarBackColor
End Function


Private Sub CreateDepStatusPics(iBar As Integer, FldValue As String)
    Dim i As Integer
    Dim j As Integer
    Dim iLen As Integer
    Dim iCnt As Integer
    Dim jIdx As Integer
    Dim tmpChar As String
    Dim tmpVers As String
    tmpVers = UCase(Trim(FldValue))
    tmpVers = Replace(tmpVers, " ", "", 1, -1, vbBinaryCompare)
    'tmpVers = "BC"
    iCnt = 0
    iLen = Len(tmpVers)
    For i = 1 To iLen
        tmpChar = Mid(tmpVers, i, 1)
        jIdx = -1
        For j = 0 To lblDepPicType.UBound
            If lblDepPicType(j).Tag = tmpChar Then
                jIdx = j
                Exit For
            End If
        Next
        If jIdx >= 0 Then
            If iCnt < 4 Then
                iCnt = iCnt + 1
                Select Case iCnt
                    Case 1
                        SetDepStatusPic iBar, jIdx, picDep4(iBar)
                    Case 2
                        SetDepStatusPic iBar, jIdx, picDep3(iBar)
                    Case 3
                        SetDepStatusPic iBar, jIdx, picDep2(iBar)
                    Case 4
                        SetDepStatusPic iBar, jIdx, picDep1(iBar)
                    Case Else
                End Select
            End If
        End If
    Next
    'Clear unused pics
    iCnt = iCnt + 1
    For i = iCnt To 4
        Select Case i
            Case 1
                ClearDepStatusPic iBar, picDep4(iBar)
            Case 2
                ClearDepStatusPic iBar, picDep3(iBar)
            Case 3
                ClearDepStatusPic iBar, picDep2(iBar)
            Case 4
                ClearDepStatusPic iBar, picDep1(iBar)
            Case Else
        End Select
    Next
End Sub
Private Sub SetDepStatusPic(iBar As Integer, jIdx As Integer, CurPic As Image)
    Dim tmpTag As String
    Dim PicCnt As Integer
    CurPic.Picture = picDepPic(jIdx).Picture
    PicCnt = Val(lblDepPicCount(jIdx).Caption) + 1
    lblDepPicCount(jIdx).Caption = CStr(PicCnt)
    tmpTag = CStr(jIdx)
    CurPic.Tag = tmpTag
    CurPic.Visible = True
    tmpTag = lblDepPicText(jIdx).Caption
    CurPic.ToolTipText = tmpTag
End Sub
Private Sub ClearDepStatusPic(iBar As Integer, CurPic As Image)
    CurPic.Tag = ""
    CurPic.Visible = False
End Sub

Private Sub ClearArrFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    lblArrFtyp(iBar).Tag = FldValue
    lblArrFtyp(iBar).Caption = FldValue
    lblArrFlno(iBar).Tag = FldValue
    lblArrFlno(iBar).Caption = FldValue
    lblArrFlti(iBar).Tag = FldValue
    lblArrFlti(iBar).Caption = FldValue
    lblArrOrg3(iBar).Tag = FldValue
    lblArrOrg3(iBar).Caption = FldValue
    lblArrVia3(iBar).Tag = FldValue
    lblArrVia3(iBar).Caption = FldValue
    lblArrStoa(iBar).Tag = FldValue
    lblArrStoa(iBar).Caption = FldValue
    lblArrEtai(iBar).Tag = FldValue
    lblArrEtai(iBar).Caption = FldValue
    lblArrOnbl(iBar).Tag = FldValue
    lblArrOnbl(iBar).Caption = FldValue
    lblArrPsta(iBar).Tag = FldValue
    lblArrPsta(iBar).Caption = FldValue
    lblArrGta1(iBar).Tag = FldValue
    lblArrGta1(iBar).Caption = FldValue
    lblArrRegn(iBar).Tag = FldValue
    lblArrRegn(iBar).Caption = FldValue
    lblArrAct3(iBar).Tag = FldValue
    lblArrAct3(iBar).Caption = FldValue
    picArr1(iBar).Tag = FldValue
    picArr2(iBar).Tag = FldValue
    picArr3(iBar).Tag = FldValue
    picArr4(iBar).Tag = FldValue
End Sub
Private Sub SetArrPanelColors(iBar As Integer, SetForeColor As Long, SetBackColor As Long)
    If iBar >= 0 Then
        lblArrFtyp(iBar).ForeColor = SetForeColor
        lblArrFtyp(iBar).BackColor = SetBackColor
        lblArrFlno(iBar).ForeColor = SetForeColor
        lblArrFlno(iBar).BackColor = SetBackColor
        lblArrFlti(iBar).ForeColor = SetForeColor
        lblArrFlti(iBar).BackColor = SetBackColor
        lblArrOrg3(iBar).ForeColor = SetForeColor
        lblArrOrg3(iBar).BackColor = SetBackColor
        lblArrVia3(iBar).ForeColor = SetForeColor
        lblArrVia3(iBar).BackColor = SetBackColor
        lblArrStoa(iBar).ForeColor = SetForeColor
        lblArrStoa(iBar).BackColor = SetBackColor
        lblArrEtai(iBar).ForeColor = SetForeColor
        lblArrEtai(iBar).BackColor = SetBackColor
        lblArrOnbl(iBar).ForeColor = SetForeColor
        lblArrOnbl(iBar).BackColor = SetBackColor
        lblArrPsta(iBar).ForeColor = SetForeColor
        lblArrPsta(iBar).BackColor = SetBackColor
        lblArrGta1(iBar).ForeColor = SetForeColor
        lblArrGta1(iBar).BackColor = SetBackColor
        lblArrRegn(iBar).ForeColor = SetForeColor
        lblArrRegn(iBar).BackColor = SetBackColor
        lblArrAct3(iBar).ForeColor = SetForeColor
        lblArrAct3(iBar).BackColor = SetBackColor
    End If
End Sub


Private Sub ClearDepFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    lblDepFtyp(iBar).Tag = FldValue
    lblDepFtyp(iBar).Caption = FldValue
    lblDepFlno(iBar).Tag = FldValue
    lblDepFlno(iBar).Caption = FldValue
    lblDepFlti(iBar).Tag = FldValue
    lblDepFlti(iBar).Caption = FldValue
    lblDepDes3(iBar).Tag = FldValue
    lblDepDes3(iBar).Caption = FldValue
    lblDepVia3(iBar).Tag = FldValue
    lblDepVia3(iBar).Caption = FldValue
    lblDepStod(iBar).Tag = FldValue
    lblDepStod(iBar).Caption = FldValue
    lblDepEtdi(iBar).Tag = FldValue
    lblDepEtdi(iBar).Caption = FldValue
    lblDepOfbl(iBar).Tag = FldValue
    lblDepOfbl(iBar).Caption = FldValue
    lblDepPstd(iBar).Tag = FldValue
    lblDepPstd(iBar).Caption = FldValue
    lblDepGtd1(iBar).Tag = FldValue
    lblDepGtd1(iBar).Caption = FldValue
    lblDepRegn(iBar).Tag = FldValue
    lblDepRegn(iBar).Caption = FldValue
    lblDepAct3(iBar).Tag = FldValue
    lblDepAct3(iBar).Caption = FldValue
    picDep1(iBar).Tag = FldValue
    picDep2(iBar).Tag = FldValue
    picDep3(iBar).Tag = FldValue
    picDep4(iBar).Tag = FldValue
End Sub
Private Sub SetDepPanelColors(iBar As Integer, SetForeColor As Long, SetBackColor As Long)
    If iBar >= 0 Then
        lblDepFtyp(iBar).ForeColor = SetForeColor
        lblDepFtyp(iBar).BackColor = SetBackColor
        lblDepFlno(iBar).ForeColor = SetForeColor
        lblDepFlno(iBar).BackColor = SetBackColor
        lblDepFlti(iBar).ForeColor = SetForeColor
        lblDepFlti(iBar).BackColor = SetBackColor
        lblDepDes3(iBar).ForeColor = SetForeColor
        lblDepDes3(iBar).BackColor = SetBackColor
        lblDepVia3(iBar).ForeColor = SetForeColor
        lblDepVia3(iBar).BackColor = SetBackColor
        lblDepStod(iBar).ForeColor = SetForeColor
        lblDepStod(iBar).BackColor = SetBackColor
        lblDepEtdi(iBar).ForeColor = SetForeColor
        lblDepEtdi(iBar).BackColor = SetBackColor
        lblDepOfbl(iBar).ForeColor = SetForeColor
        lblDepOfbl(iBar).BackColor = SetBackColor
        lblDepPstd(iBar).ForeColor = SetForeColor
        lblDepPstd(iBar).BackColor = SetBackColor
        lblDepGtd1(iBar).ForeColor = SetForeColor
        lblDepGtd1(iBar).BackColor = SetBackColor
        lblDepRegn(iBar).ForeColor = SetForeColor
        lblDepRegn(iBar).BackColor = SetBackColor
        lblDepAct3(iBar).ForeColor = SetForeColor
        lblDepAct3(iBar).BackColor = SetBackColor
    End If
End Sub


Private Sub InitRightPanel()
    Dim CfgVers As String
    Dim CfgPics As String
    Dim CfgCapt As String
    Dim CfgText As String
    Dim tmpVers As String
    Dim tmpPics As String
    Dim tmpCapt As String
    Dim tmpText As String
    Dim LoopIdx As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim TxtIdx As Integer
    Dim LinIdx As Integer
    Dim ArrIdx As Integer
    Dim DepIdx As Integer
    Dim IdxCnt As Integer
    Dim IdxMax As Integer
    
    CfgVers = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_VERS", "")
    CfgPics = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_PICS", "")
    CfgCapt = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_CAPT", "")
    CfgText = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSA_STATUS_TEXT", "")
 
    TxtIdx = 0
    LinIdx = 0
    NewLeft = 60
    NewTop = fraYButtonPanel(0).Top + fraYButtonPanel(0).Height + 90
    CreateLinLine LinIdx, NewTop, vbWhite
    NewTop = NewTop + 30
    CreateChkTitle TxtIdx, "Inbound", "AFSA", NewTop
    chkTitle(TxtIdx).Caption = "="
    chkTitle(TxtIdx).ToolTipText = "Show/Hide Task Bars"
    lblTxtBackLight(TxtIdx).BackColor = LightYellow
    NewTop = NewTop + lblTxtText(0).Height + 30
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    LinIdx = LinIdx + 1
    NewTop = NewTop + 15
    CreateLinLine LinIdx, NewTop, vbWhite
    
    NewTop = NewTop + 45
    IdxCnt = Len(CfgVers)
    IdxMax = IdxCnt - 1
    For ArrIdx = 0 To IdxMax
        LoopIdx = CLng(ArrIdx)
        tmpVers = Mid(CfgVers, (LoopIdx + 1), 1)
        tmpCapt = GetRealItem(CfgCapt, LoopIdx, ",")
        tmpText = GetRealItem(CfgText, LoopIdx, ",")
        tmpPics = GetRealItem(CfgPics, LoopIdx, ",")
        CreateArrPicPanel ArrIdx, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + ArrPicPanel(0).Height + 30
    Next
    If IdxCnt > 4 Then
        IdxMax = IdxMax + 1
        tmpVers = CfgVers
        tmpCapt = "?"
        tmpText = "Others"
        tmpPics = ""
        CreateArrPicPanel IdxMax, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + ArrPicPanel(0).Height + 30
    End If
    
    CfgVers = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_VERS", "")
    CfgPics = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_PICS", "")
    CfgCapt = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_CAPT", "")
    CfgText = GetIniEntry(myIniFullName, "GOCC_CHART", "", "AFSD_STATUS_TEXT", "")
    
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    NewTop = NewTop + 120
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbWhite
    NewTop = NewTop + 30
    TxtIdx = TxtIdx + 1
    CreateChkTitle TxtIdx, "Outbound", "AFSD", NewTop
    chkTitle(TxtIdx).Caption = "="
    chkTitle(TxtIdx).ToolTipText = "Show/Hide Task Bars"
    lblTxtBackLight(TxtIdx).BackColor = LightGreen
    NewTop = NewTop + lblTxtText(0).Height + 30
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    LinIdx = LinIdx + 1
    NewTop = NewTop + 15
    CreateLinLine LinIdx, NewTop, vbWhite
    
    NewTop = NewTop + 45
    IdxCnt = Len(CfgVers)
    IdxMax = IdxCnt - 1
    For DepIdx = 0 To IdxMax
        LoopIdx = CLng(DepIdx)
        tmpVers = Mid(CfgVers, (LoopIdx + 1), 1)
        tmpCapt = GetRealItem(CfgCapt, LoopIdx, ",")
        tmpText = GetRealItem(CfgText, LoopIdx, ",")
        tmpPics = GetRealItem(CfgPics, LoopIdx, ",")
        CreateDepPicPanel DepIdx, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + DepPicPanel(0).Height + 30
    Next
    If IdxCnt > 4 Then
        IdxMax = IdxMax + 1
        tmpVers = CfgVers
        tmpCapt = "?"
        tmpText = "Others"
        tmpPics = ""
        CreateDepPicPanel IdxMax, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
        NewTop = NewTop + DepPicPanel(0).Height + 30
    End If
    
    LinIdx = LinIdx + 1
    CreateLinLine LinIdx, NewTop, vbBlack
    
    CfgVers = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_VERS", "")
    CfgPics = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_PICS", "")
    CfgCapt = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_CAPT", "")
    CfgText = GetIniEntry(myIniFullName, "GOCC_CHART", "", "COMM_STATUS_TEXT", "")
    If CfgVers <> "" Then
        NewTop = NewTop + 120
        LinIdx = LinIdx + 1
        CreateLinLine LinIdx, NewTop, vbWhite
        NewTop = NewTop + 30
        TxtIdx = TxtIdx + 1
        CreateChkTitle TxtIdx, "Common", "COMM", NewTop
        lblTxtBackLight(TxtIdx).BackColor = vbBlue
        lblTxtText(TxtIdx).ForeColor = vbWhite
        lblTxtShadow(TxtIdx).ForeColor = vbBlack
        
        NewTop = NewTop + lblTxtText(0).Height + 30
        LinIdx = LinIdx + 1
        CreateLinLine LinIdx, NewTop, vbBlack
        LinIdx = LinIdx + 1
        NewTop = NewTop + 15
        CreateLinLine LinIdx, NewTop, vbWhite
        
        NewTop = NewTop + 45
        IdxCnt = Len(CfgVers)
        IdxMax = IdxCnt - 1
        For ArrIdx = 0 To IdxMax
            LoopIdx = CLng(ArrIdx)
            tmpVers = Mid(CfgVers, (LoopIdx + 1), 1)
            tmpCapt = GetRealItem(CfgCapt, LoopIdx, ",")
            tmpText = GetRealItem(CfgText, LoopIdx, ",")
            tmpPics = GetRealItem(CfgPics, LoopIdx, ",")
            CreateComPicPanel ArrIdx, NewTop, tmpVers, tmpCapt, tmpText, tmpPics
            NewTop = NewTop + ComPicPanel(0).Height + 30
        Next
        
        LinIdx = LinIdx + 1
        CreateLinLine LinIdx, NewTop, vbBlack
    End If
    
End Sub
Private Sub CreateLinLine(LinIdx, UseTop As Long, UseColor As Long)
    If LinIdx > linLine.UBound Then
        Load linLine(LinIdx)
        Set linLine(LinIdx).Container = RightPanel
    End If
    linLine(LinIdx).y1 = UseTop
    linLine(LinIdx).Y2 = UseTop
    linLine(LinIdx).BorderColor = UseColor
    linLine(LinIdx).Visible = True
End Sub
Private Sub CreateArrPicPanel(Index As Integer, TopPos As Long, VersCode As String, VersCapt As String, VersText As String, VersIcon As String)
    Dim FullPicPath As String
    Dim NewTop As Long
    On Error Resume Next
    If Index > ArrPicPanel.UBound Then
        Load ArrPicPanel(Index)
        Load cmdArrPicMode(Index)
        Load lblArrPicText(Index)
        Load lblArrPicBack(Index)
        Load lblArrPicType(Index)
        Load lblArrPicCount(Index)
        Load picArrPic(Index)
        Set ArrPicPanel(Index).Container = RightPanel
        Set cmdArrPicMode(Index).Container = ArrPicPanel(Index)
        Set lblArrPicText(Index).Container = ArrPicPanel(Index)
        Set lblArrPicBack(Index).Container = ArrPicPanel(Index)
        Set lblArrPicType(Index).Container = ArrPicPanel(Index)
        Set lblArrPicCount(Index).Container = ArrPicPanel(Index)
        Set picArrPic(Index).Container = ArrPicPanel(Index)
    End If
    NewTop = TopPos
    ArrPicPanel(Index).Top = NewTop
    lblArrPicText(Index).BackColor = LightGrey
    lblArrPicType(Index).BackColor = LightGrey
    lblArrPicCount(Index).BackColor = LightGrey
    lblArrPicBack(Index).BackColor = LightestYellow
    lblArrPicText(Index).Caption = ""
    lblArrPicType(Index).Caption = ""
    lblArrPicCount(Index).Caption = ""
    
    lblArrPicText(Index).Caption = VersText
    lblArrPicType(Index).Caption = VersCapt
    lblArrPicType(Index).Tag = VersCode
    picArrPic(Index).Picture = picComPics(4).Picture
    If VersIcon <> "" Then
        FullPicPath = StatusPicPath & "\" & VersIcon
        picArrPic(Index).Picture = LoadPicture(FullPicPath)
    End If
    
    cmdArrPicMode(Index).Visible = True
    lblArrPicText(Index).Visible = True
    lblArrPicBack(Index).Visible = True
    lblArrPicType(Index).Visible = True
    lblArrPicCount(Index).Visible = True
    picArrPic(Index).Visible = True
    picArrPic(Index).ZOrder
    ArrPicPanel(Index).Visible = True
    
End Sub

Private Sub CreateDepPicPanel(Index As Integer, TopPos As Long, VersCode As String, VersCapt As String, VersText As String, VersIcon As String)
    Dim FullPicPath As String
    Dim NewTop As Long
    On Error Resume Next
    If Index > DepPicPanel.UBound Then
        Load DepPicPanel(Index)
        Load cmdDepPicMode(Index)
        Load lblDepPicText(Index)
        Load lblDepPicBack(Index)
        Load lblDepPicType(Index)
        Load lblDepPicCount(Index)
        Load picDepPic(Index)
        Set DepPicPanel(Index).Container = RightPanel
        Set cmdDepPicMode(Index).Container = DepPicPanel(Index)
        Set lblDepPicText(Index).Container = DepPicPanel(Index)
        Set lblDepPicBack(Index).Container = DepPicPanel(Index)
        Set lblDepPicType(Index).Container = DepPicPanel(Index)
        Set lblDepPicCount(Index).Container = DepPicPanel(Index)
        Set picDepPic(Index).Container = DepPicPanel(Index)
    End If
    NewTop = TopPos
    DepPicPanel(Index).Top = NewTop
    lblDepPicText(Index).BackColor = LightGrey
    lblDepPicType(Index).BackColor = LightGrey
    lblDepPicCount(Index).BackColor = LightGrey
    lblDepPicBack(Index).BackColor = LightestGreen
    lblDepPicText(Index).Caption = ""
    lblDepPicType(Index).Caption = ""
    lblDepPicCount(Index).Caption = ""
    
    lblDepPicText(Index).Caption = VersText
    lblDepPicType(Index).Caption = VersCapt
    lblDepPicType(Index).Tag = VersCode
    picDepPic(Index).Picture = picComPics(4).Picture
    If VersIcon <> "" Then
        FullPicPath = StatusPicPath & "\" & VersIcon
        picDepPic(Index).Picture = LoadPicture(FullPicPath)
    End If
    
    cmdDepPicMode(Index).Visible = True
    lblDepPicText(Index).Visible = True
    lblDepPicBack(Index).Visible = True
    lblDepPicType(Index).Visible = True
    lblDepPicCount(Index).Visible = True
    picDepPic(Index).Visible = True
    picDepPic(Index).ZOrder
    DepPicPanel(Index).Visible = True
    
End Sub

Private Sub CreateComPicPanel(Index As Integer, TopPos As Long, VersCode As String, VersCapt As String, VersText As String, VersIcon As String)
    Dim FullPicPath As String
    Dim NewTop As Long
    On Error Resume Next
    If Index > ComPicPanel.UBound Then
        Load ComPicPanel(Index)
        Load cmdComPicMode(Index)
        Load lblComPicText(Index)
        Load lblComPicBack(Index)
        Load lblComPicType(Index)
        Load lblComPicCount(Index)
        Load picComPic(Index)
        Set ComPicPanel(Index).Container = RightPanel
        Set cmdComPicMode(Index).Container = ComPicPanel(Index)
        Set lblComPicText(Index).Container = ComPicPanel(Index)
        Set lblComPicBack(Index).Container = ComPicPanel(Index)
        Set lblComPicType(Index).Container = ComPicPanel(Index)
        Set lblComPicCount(Index).Container = ComPicPanel(Index)
        Set picComPic(Index).Container = ComPicPanel(Index)
    End If
    NewTop = TopPos
    ComPicPanel(Index).Top = NewTop
    cmdComPicMode(Index).BackColor = LightGrey
    lblComPicText(Index).BackColor = LightGrey
    lblComPicType(Index).BackColor = LightGrey
    lblComPicCount(Index).BackColor = LightGrey
    lblComPicBack(Index).BackColor = vbWhite
    lblComPicText(Index).Caption = ""
    lblComPicType(Index).Caption = ""
    lblComPicCount(Index).Caption = ""
    
    lblComPicText(Index).Caption = VersText
    lblComPicType(Index).Caption = VersCapt
    lblComPicType(Index).Tag = VersCode
    picComPic(Index).Picture = picComPics(4).Picture
    If VersIcon <> "" Then
        FullPicPath = StatusPicPath & "\" & VersIcon
        picComPic(Index).Picture = LoadPicture(FullPicPath)
    End If
    
    cmdComPicMode(Index).Visible = True
    lblComPicText(Index).Visible = True
    lblComPicBack(Index).Visible = True
    lblComPicType(Index).Visible = True
    lblComPicCount(Index).Visible = True
    picComPic(Index).Visible = True
    picComPic(Index).ZOrder
    ComPicPanel(Index).Visible = True
    
End Sub


Private Sub CreateChkTitle(TxtIdx As Integer, UseText As String, UseTag As String, UseTop As Long)
    Dim NewLeft As Long
    If TxtIdx > lblTxtText.UBound Then
        Load lblTxtBackLight(TxtIdx)
        Set lblTxtBackLight(TxtIdx).Container = RightPanel
        Load chkTitle(TxtIdx)
        Set chkTitle(TxtIdx).Container = RightPanel
        Load lblTxtText(TxtIdx)
        Set lblTxtText(TxtIdx).Container = RightPanel
        Load lblTxtShadow(TxtIdx)
        Set lblTxtShadow(TxtIdx).Container = RightPanel
    End If
    lblTxtBackLight(TxtIdx).Top = UseTop
    lblTxtBackLight(TxtIdx).Visible = True
    chkTitle(TxtIdx).Top = UseTop + 15
    chkTitle(TxtIdx).Left = 15
    chkTitle(TxtIdx).Visible = True
    NewLeft = chkTitle(TxtIdx).Width + 45
    Set3DText UseText, lblTxtText(TxtIdx), lblTxtShadow(TxtIdx), NewLeft, UseTop + 15, True
    chkTitle(TxtIdx).Tag = UseTag
End Sub
Private Sub Set3DText(UseText As String, TextObj As Label, ShadObj As Label, UseLeft As Long, UseTop As Long, SetDark As Boolean)
    TextObj.Visible = False
    ShadObj.Visible = False
    TextObj.Caption = UseText
    ShadObj.Caption = UseText
    TextObj.Top = UseTop
    ShadObj.Top = UseTop + 15
    TextObj.Left = UseLeft
    ShadObj.Left = UseLeft + 15
    TextObj.Visible = True
    ShadObj.Visible = True
    ShadObj.Refresh
    ShadObj.ZOrder
    TextObj.ZOrder
    TextObj.Refresh
End Sub

Private Sub GetChartConfig()
    Dim tmpCfg As String
    Dim tmpData As String
    On Error Resume Next
    Me.Caption = GetIniEntry(myIniFullName, "GOCC_CHART", "", "GOCC_CHART_TITLE", Me.Caption)
    tmpCfg = GetIniEntry(myIniFullName, "GOCC_CHART", "", "GOCC_CHART_ICON", "")
    If tmpCfg <> "" Then
        If InStr(tmpCfg, "\") = 0 Then tmpCfg = UFIS_SYSTEM & "\" & tmpCfg
        picApplIcon.Picture = LoadPicture(tmpCfg)
    End If
    Me.Icon = picApplIcon.Picture
    
End Sub
