VERSION 5.00
Begin VB.Form ImagePool 
   Caption         =   "ImagePool"
   ClientHeight    =   5280
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13365
   Icon            =   "ImagePool.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5280
   ScaleWidth      =   13365
   StartUpPosition =   3  'Windows Default
   Begin VB.Image imgMetBut2 
      Height          =   660
      Index           =   2
      Left            =   6210
      MouseIcon       =   "ImagePool.frx":08CA
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":0BD4
      Top             =   2220
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgMetBut2 
      Height          =   660
      Index           =   1
      Left            =   5550
      MouseIcon       =   "ImagePool.frx":22C6
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":25D0
      Top             =   2220
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgMetBut2 
      Height          =   660
      Index           =   0
      Left            =   4890
      MouseIcon       =   "ImagePool.frx":3CC2
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":3FCC
      Top             =   2220
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   7
      Left            =   5910
      Picture         =   "ImagePool.frx":56BE
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   6
      Left            =   5400
      Picture         =   "ImagePool.frx":59C8
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   5
      Left            =   4890
      Picture         =   "ImagePool.frx":5CD2
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   4
      Left            =   4380
      Picture         =   "ImagePool.frx":5FDC
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   3
      Left            =   3870
      Picture         =   "ImagePool.frx":62E6
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   2
      Left            =   3360
      Picture         =   "ImagePool.frx":65F0
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   1
      Left            =   2850
      Picture         =   "ImagePool.frx":68FA
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorDW 
      Height          =   480
      Index           =   0
      Left            =   2340
      Picture         =   "ImagePool.frx":6C04
      Top             =   3060
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   7
      Left            =   5910
      Picture         =   "ImagePool.frx":6F0E
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   6
      Left            =   5400
      Picture         =   "ImagePool.frx":7218
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   5
      Left            =   4890
      Picture         =   "ImagePool.frx":7522
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   4
      Left            =   4380
      Picture         =   "ImagePool.frx":782C
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   3
      Left            =   3870
      Picture         =   "ImagePool.frx":7B36
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   2
      Left            =   3360
      Picture         =   "ImagePool.frx":7E40
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   1
      Left            =   2850
      Picture         =   "ImagePool.frx":814A
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picMisc 
      Height          =   105
      Index           =   1
      Left            =   2220
      Picture         =   "ImagePool.frx":8454
      Top             =   2640
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.Image picSensorLW 
      Height          =   480
      Index           =   0
      Left            =   2340
      Picture         =   "ImagePool.frx":8C8E
      Top             =   2880
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picMisc 
      Height          =   480
      Index           =   0
      Left            =   2190
      Picture         =   "ImagePool.frx":8F98
      Top             =   2130
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image UfisLogoBmp 
      Height          =   480
      Index           =   1
      Left            =   720
      Picture         =   "ImagePool.frx":9C62
      Top             =   3120
      Width           =   480
   End
   Begin VB.Image UfisLogoBmp 
      Height          =   480
      Index           =   0
      Left            =   90
      Picture         =   "ImagePool.frx":A8A4
      Top             =   3120
      Width           =   480
   End
   Begin VB.Image UfisLogoIco 
      Height          =   480
      Index           =   0
      Left            =   120
      Picture         =   "ImagePool.frx":B4E6
      Top             =   2580
      Width           =   480
   End
   Begin VB.Image UfisLogoIco 
      Height          =   480
      Index           =   1
      Left            =   750
      Picture         =   "ImagePool.frx":BDB0
      Top             =   2580
      Width           =   480
   End
   Begin VB.Image ChkSlide 
      Height          =   360
      Index           =   2
      Left            =   1050
      Picture         =   "ImagePool.frx":C67A
      Top             =   2130
      Width           =   345
   End
   Begin VB.Image picImpFlag 
      Height          =   480
      Index           =   4
      Left            =   2190
      Picture         =   "ImagePool.frx":CD7C
      Top             =   1560
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picImpFlag 
      Height          =   480
      Index           =   3
      Left            =   1650
      Picture         =   "ImagePool.frx":D646
      Top             =   1560
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picImpFlag 
      Height          =   480
      Index           =   2
      Left            =   1110
      Picture         =   "ImagePool.frx":DF10
      Top             =   1560
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picImpFlag 
      Height          =   480
      Index           =   1
      Left            =   570
      Picture         =   "ImagePool.frx":E7DA
      Top             =   1560
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picImpFlag 
      Height          =   480
      Index           =   0
      Left            =   30
      Picture         =   "ImagePool.frx":F0A4
      Top             =   1560
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image ChkSlide 
      Height          =   360
      Index           =   1
      Left            =   660
      Picture         =   "ImagePool.frx":F96E
      Top             =   2130
      Width           =   345
   End
   Begin VB.Image ChkSlide 
      Height          =   360
      Index           =   0
      Left            =   270
      Picture         =   "ImagePool.frx":10070
      Top             =   2130
      Width           =   345
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   7
      Left            =   1740
      Picture         =   "ImagePool.frx":10772
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   6
      Left            =   1500
      Picture         =   "ImagePool.frx":109BC
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   5
      Left            =   1260
      Picture         =   "ImagePool.frx":10C06
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   4
      Left            =   1020
      Picture         =   "ImagePool.frx":10E50
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   3
      Left            =   780
      Picture         =   "ImagePool.frx":1109A
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   2
      Left            =   540
      Picture         =   "ImagePool.frx":112E4
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   1
      Left            =   300
      Picture         =   "ImagePool.frx":1152E
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   0
      Left            =   60
      Picture         =   "ImagePool.frx":11778
      ToolTipText     =   "Tooltip 0"
      Top             =   1290
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   7
      Left            =   1740
      Picture         =   "ImagePool.frx":119C2
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   6
      Left            =   1500
      Picture         =   "ImagePool.frx":11C0C
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   5
      Left            =   1260
      Picture         =   "ImagePool.frx":11E56
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   4
      Left            =   1020
      Picture         =   "ImagePool.frx":120A0
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   3
      Left            =   780
      Picture         =   "ImagePool.frx":122EA
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   2
      Left            =   540
      Picture         =   "ImagePool.frx":12534
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   1
      Left            =   300
      Picture         =   "ImagePool.frx":1277E
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   0
      Left            =   60
      Picture         =   "ImagePool.frx":129C8
      ToolTipText     =   "Tooltip 0"
      Top             =   1050
      Width           =   195
   End
   Begin VB.Image imgMetBut0 
      Height          =   660
      Index           =   0
      Left            =   4890
      MouseIcon       =   "ImagePool.frx":12C12
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":12F1C
      Top             =   900
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgMetBut1 
      Height          =   660
      Index           =   0
      Left            =   4890
      MouseIcon       =   "ImagePool.frx":13AEE
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":13DF8
      Top             =   1560
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgMetBut0 
      Height          =   660
      Index           =   1
      Left            =   5550
      MouseIcon       =   "ImagePool.frx":154EA
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":157F4
      Top             =   900
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgMetBut1 
      Height          =   660
      Index           =   1
      Left            =   5550
      MouseIcon       =   "ImagePool.frx":163C6
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":166D0
      Top             =   1560
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgMetBut0 
      Height          =   660
      Index           =   2
      Left            =   6210
      MouseIcon       =   "ImagePool.frx":17DC2
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":180CC
      Top             =   900
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgMetBut1 
      Height          =   660
      Index           =   2
      Left            =   6210
      MouseIcon       =   "ImagePool.frx":18C9E
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":18FA8
      Top             =   1560
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.Image imgCtl 
      Height          =   480
      Index           =   0
      Left            =   10620
      Picture         =   "ImagePool.frx":1A69A
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgCtl 
      Height          =   480
      Index           =   1
      Left            =   11010
      Picture         =   "ImagePool.frx":1AF64
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgCtl 
      Height          =   480
      Index           =   2
      Left            =   11400
      Picture         =   "ImagePool.frx":1B82E
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgAnyPic2 
      Height          =   240
      Index           =   0
      Left            =   4560
      Picture         =   "ImagePool.frx":1C0F8
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgInfo2 
      Height          =   240
      Left            =   5760
      Picture         =   "ImagePool.frx":1C242
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgExtract 
      Height          =   240
      Left            =   6060
      Picture         =   "ImagePool.frx":1C38C
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgInfo1 
      Height          =   240
      Left            =   5460
      Picture         =   "ImagePool.frx":1C916
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgLookUpA 
      Height          =   240
      Left            =   3360
      Picture         =   "ImagePool.frx":1CA60
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgLookUpB 
      Height          =   240
      Left            =   3630
      Picture         =   "ImagePool.frx":1CFEA
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgAnyPic1 
      Height          =   240
      Index           =   0
      Left            =   4260
      Picture         =   "ImagePool.frx":1D574
      ToolTipText     =   "Steps through the tool box"
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPos 
      Height          =   240
      Index           =   0
      Left            =   3960
      Picture         =   "ImagePool.frx":1DAFE
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPosUp 
      Height          =   240
      Left            =   5160
      Picture         =   "ImagePool.frx":1E088
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPosDn 
      Height          =   240
      Left            =   4860
      Picture         =   "ImagePool.frx":1E612
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   0
      Left            =   60
      Picture         =   "ImagePool.frx":1EB9C
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   1
      Left            =   330
      Picture         =   "ImagePool.frx":1ECE6
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   2
      Left            =   600
      Picture         =   "ImagePool.frx":1EE30
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   3
      Left            =   870
      Picture         =   "ImagePool.frx":1EF7A
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   4
      Left            =   1140
      Picture         =   "ImagePool.frx":1F0C4
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   5
      Left            =   1410
      Picture         =   "ImagePool.frx":1F20E
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   6
      Left            =   1680
      Picture         =   "ImagePool.frx":1F358
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   7
      Left            =   1950
      Picture         =   "ImagePool.frx":1F4A2
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   8
      Left            =   2220
      Picture         =   "ImagePool.frx":1F5EC
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   9
      Left            =   2490
      Picture         =   "ImagePool.frx":1F736
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   10
      Left            =   2760
      Picture         =   "ImagePool.frx":1F880
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   11
      Left            =   3030
      Picture         =   "ImagePool.frx":1F9CA
      Top             =   570
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   20
      Left            =   9600
      MouseIcon       =   "ImagePool.frx":1FB14
      MousePointer    =   99  'Custom
      Picture         =   "ImagePool.frx":1FC66
      Top             =   0
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   0
      Left            =   0
      Picture         =   "ImagePool.frx":20530
      Top             =   0
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   19
      Left            =   9090
      Picture         =   "ImagePool.frx":20DFA
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   15
      Left            =   6990
      Picture         =   "ImagePool.frx":2123C
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   12
      Left            =   5460
      Picture         =   "ImagePool.frx":21B06
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   11
      Left            =   5010
      Picture         =   "ImagePool.frx":21E10
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   10
      Left            =   4560
      Picture         =   "ImagePool.frx":2211A
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   9
      Left            =   4110
      Picture         =   "ImagePool.frx":22424
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   18
      Left            =   8550
      Picture         =   "ImagePool.frx":2272E
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   17
      Left            =   8040
      Picture         =   "ImagePool.frx":22FF8
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   16
      Left            =   7500
      Picture         =   "ImagePool.frx":238C2
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   14
      Left            =   6480
      Picture         =   "ImagePool.frx":2418C
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   13
      Left            =   5970
      Picture         =   "ImagePool.frx":24A56
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   8
      Left            =   3660
      Picture         =   "ImagePool.frx":25320
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   7
      Left            =   3210
      Picture         =   "ImagePool.frx":2562A
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   6
      Left            =   2760
      Picture         =   "ImagePool.frx":25934
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   5
      Left            =   2310
      Picture         =   "ImagePool.frx":25C3E
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   4
      Left            =   1860
      Picture         =   "ImagePool.frx":25F48
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   3
      Left            =   1410
      Picture         =   "ImagePool.frx":26252
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   2
      Left            =   960
      Picture         =   "ImagePool.frx":2655C
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   1
      Left            =   510
      Picture         =   "ImagePool.frx":26866
      Top             =   0
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "ImagePool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

