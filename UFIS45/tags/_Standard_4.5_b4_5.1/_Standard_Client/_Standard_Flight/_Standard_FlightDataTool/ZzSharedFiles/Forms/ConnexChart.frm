VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form ConnexChart 
   Caption         =   "UFIS Flight Connex Chart"
   ClientHeight    =   10875
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15780
   Icon            =   "ConnexChart.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   10875
   ScaleWidth      =   15780
   Begin TABLib.TAB TabArrCnxDetails 
      Height          =   975
      Left            =   60
      TabIndex        =   187
      Top             =   9210
      Width           =   7815
      _Version        =   65536
      _ExtentX        =   13785
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin VB.Timer MouseTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   750
      Left            =   14790
      Top             =   5940
   End
   Begin VB.Timer MouseTimer 
      Enabled         =   0   'False
      Index           =   1
      Interval        =   250
      Left            =   15240
      Top             =   5940
   End
   Begin VB.PictureBox RightScCon1 
      BackColor       =   &H000040C0&
      Height          =   1845
      Index           =   0
      Left            =   10110
      ScaleHeight     =   1785
      ScaleWidth      =   5565
      TabIndex        =   105
      Top             =   7110
      Visible         =   0   'False
      Width           =   5625
      Begin TABLib.TAB TabDepInfo 
         Height          =   465
         Index           =   0
         Left            =   30
         TabIndex        =   111
         ToolTipText     =   "Connecting Departure Flights Outside The Visible Data Set"
         Top             =   780
         Visible         =   0   'False
         Width           =   5505
         _Version        =   65536
         _ExtentX        =   9710
         _ExtentY        =   820
         _StockProps     =   64
      End
      Begin VB.Frame fraTabCaption 
         BackColor       =   &H00FF0000&
         BorderStyle     =   0  'None
         Caption         =   "Frame9"
         Height          =   300
         Index           =   2
         Left            =   0
         TabIndex        =   110
         Top             =   0
         Width           =   9165
      End
   End
   Begin VB.PictureBox LeftScCon1 
      BackColor       =   &H000040C0&
      Height          =   1845
      Index           =   0
      Left            =   60
      ScaleHeight     =   1785
      ScaleWidth      =   4095
      TabIndex        =   104
      Top             =   7110
      Visible         =   0   'False
      Width           =   4155
      Begin VB.Frame fraTabCaption 
         BackColor       =   &H00FF0000&
         BorderStyle     =   0  'None
         Caption         =   "Frame9"
         Height          =   300
         Index           =   1
         Left            =   0
         TabIndex        =   109
         Top             =   0
         Width           =   9165
      End
   End
   Begin VB.PictureBox MidScCon1 
      Height          =   1845
      Index           =   0
      Left            =   4470
      ScaleHeight     =   1785
      ScaleWidth      =   5505
      TabIndex        =   103
      Top             =   7110
      Visible         =   0   'False
      Width           =   5565
      Begin VB.Timer CnxRefresh 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   3120
         Top             =   930
      End
      Begin VB.Frame fraTabCaption 
         BackColor       =   &H00FF0000&
         BorderStyle     =   0  'None
         Caption         =   "Frame9"
         Height          =   300
         Index           =   0
         Left            =   0
         TabIndex        =   106
         Top             =   0
         Width           =   9165
         Begin VB.Label lblTabCaptionShadow 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Transfer PAX (Arrival)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Index           =   0
            Left            =   30
            TabIndex        =   107
            Top             =   0
            Width           =   3345
         End
         Begin VB.Label lblTabCaption 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Transfer PAX (Arrival)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   60
            TabIndex        =   108
            Top             =   15
            Width           =   5085
         End
      End
      Begin VB.Frame CnxFilterPanel 
         Height          =   510
         Left            =   15
         TabIndex        =   128
         Top             =   240
         Width           =   5475
         Begin VB.Frame CnxMidFilterPanel 
            Height          =   510
            Left            =   75
            TabIndex        =   134
            Top             =   0
            Width           =   5250
            Begin VB.CheckBox chkCnxAdid 
               Caption         =   "DEP"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   1
               Left            =   4680
               Style           =   1  'Graphical
               TabIndex        =   190
               Top             =   150
               Width           =   510
            End
            Begin VB.CheckBox chkCnxAdid 
               Caption         =   "ARR"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   0
               Left            =   60
               Style           =   1  'Graphical
               TabIndex        =   189
               Top             =   150
               Width           =   510
            End
            Begin VB.Frame fraCnxTypeFilter 
               Height          =   510
               Left            =   600
               TabIndex        =   144
               Top             =   0
               Width           =   1650
               Begin VB.CheckBox chkCnxType 
                  Caption         =   "PAX"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   147
                  ToolTipText     =   "Main Filter PAX Connections"
                  Top             =   150
                  Width           =   510
               End
               Begin VB.CheckBox chkCnxType 
                  Caption         =   "BAG"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   1
                  Left            =   570
                  Style           =   1  'Graphical
                  TabIndex        =   146
                  ToolTipText     =   "Main Filter BAG Connections"
                  Top             =   150
                  Width           =   510
               End
               Begin VB.CheckBox chkCnxType 
                  Caption         =   "ULD"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   2
                  Left            =   1080
                  Style           =   1  'Graphical
                  TabIndex        =   145
                  ToolTipText     =   "Main Filter ULD Connections"
                  Top             =   150
                  Width           =   510
               End
            End
            Begin VB.Frame fraCnxClassFilter 
               Height          =   510
               Left            =   2280
               TabIndex        =   139
               Top             =   0
               Width           =   1320
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "F"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   143
                  ToolTipText     =   "First Class"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "C"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   1
                  Left            =   360
                  Style           =   1  'Graphical
                  TabIndex        =   142
                  ToolTipText     =   "Business"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "Y"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   2
                  Left            =   960
                  Style           =   1  'Graphical
                  TabIndex        =   141
                  ToolTipText     =   "Economy"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxClass 
                  Caption         =   "U"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   3
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   140
                  ToolTipText     =   "Premium Economy"
                  Top             =   150
                  Width           =   300
               End
            End
            Begin VB.Frame fraCnxConnexFilter 
               Height          =   510
               Left            =   3630
               TabIndex        =   135
               Top             =   0
               Width           =   1020
               Begin VB.CheckBox chkCnxShort 
                  Caption         =   "S"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   138
                  ToolTipText     =   "Short Connections"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxCrit 
                  Caption         =   "C"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   360
                  Style           =   1  'Graphical
                  TabIndex        =   137
                  ToolTipText     =   "Critical Connections"
                  Top             =   150
                  Width           =   300
               End
               Begin VB.CheckBox chkCnxOr 
                  Caption         =   "?"
                  Enabled         =   0   'False
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   300
                  Index           =   0
                  Left            =   660
                  Style           =   1  'Graphical
                  TabIndex        =   136
                  ToolTipText     =   "(Future Use)"
                  Top             =   150
                  Width           =   300
               End
            End
         End
      End
      Begin VB.Image picDepCnxCnt 
         Appearance      =   0  'Flat
         Height          =   240
         Index           =   0
         Left            =   570
         Picture         =   "ConnexChart.frx":0442
         ToolTipText     =   "Flight Status"
         Top             =   930
         Width           =   240
      End
      Begin VB.Image picArrCnxCnt 
         Appearance      =   0  'Flat
         Height          =   240
         Index           =   0
         Left            =   270
         Picture         =   "ConnexChart.frx":058C
         ToolTipText     =   "Flight Status"
         Top             =   930
         Width           =   240
      End
   End
   Begin VB.PictureBox TimeScroll2 
      Height          =   1125
      Index           =   0
      Left            =   13110
      ScaleHeight     =   1065
      ScaleWidth      =   255
      TabIndex        =   46
      Top             =   1200
      Width           =   315
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   810
         Width           =   255
      End
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   540
         Width           =   255
      End
      Begin VB.CheckBox chkTime2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkTime1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   270
         Width           =   255
      End
   End
   Begin VB.PictureBox TimeScroll1 
      Height          =   1125
      Index           =   0
      Left            =   1320
      ScaleHeight     =   1065
      ScaleWidth      =   255
      TabIndex        =   43
      Top             =   1200
      Visible         =   0   'False
      Width           =   315
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   810
         Width           =   255
      End
      Begin VB.CheckBox chkTime3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   540
         Width           =   255
      End
      Begin VB.CheckBox chkTime1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkTime2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll3 
      Height          =   585
      Index           =   0
      Left            =   13110
      ScaleHeight     =   525
      ScaleWidth      =   255
      TabIndex        =   28
      Top             =   6420
      Width           =   315
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkScroll2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll1 
      Height          =   585
      Index           =   0
      Left            =   1320
      ScaleHeight     =   525
      ScaleWidth      =   255
      TabIndex        =   26
      Top             =   6420
      Width           =   315
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   270
         Width           =   255
      End
      Begin VB.CheckBox chkScroll1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox HorizScroll2 
      Height          =   315
      Index           =   0
      Left            =   1680
      ScaleHeight     =   255
      ScaleWidth      =   11325
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   6420
      Width           =   11385
      Begin VB.HScrollBar HScroll2 
         Height          =   255
         Index           =   0
         LargeChange     =   30
         Left            =   0
         Max             =   3600
         SmallChange     =   5
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   0
         Width           =   8415
      End
   End
   Begin VB.PictureBox BottomRemark 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      Height          =   300
      Index           =   0
      Left            =   1680
      ScaleHeight     =   240
      ScaleWidth      =   11325
      TabIndex        =   23
      Top             =   6720
      Width           =   11385
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "                                         "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   1
         Left            =   60
         TabIndex        =   50
         Top             =   15
         Visible         =   0   'False
         Width           =   1845
      End
   End
   Begin VB.PictureBox TopRemark 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00C0C0C0&
      Height          =   300
      Index           =   0
      Left            =   1680
      ScaleHeight     =   240
      ScaleWidth      =   11325
      TabIndex        =   22
      Top             =   1200
      Width           =   11385
      Begin VB.Label lblMidTime 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "00:00:00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   3750
         TabIndex        =   133
         Top             =   0
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblRightTime 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "00:00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   4560
         TabIndex        =   132
         Top             =   0
         Visible         =   0   'False
         Width           =   525
      End
      Begin VB.Label lblRightDate 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "07MAR07"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   5100
         TabIndex        =   131
         Top             =   0
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblLeftTime 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "00:00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   3135
         TabIndex        =   130
         Top             =   0
         Visible         =   0   'False
         Width           =   525
      End
      Begin VB.Label lblLeftDate 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "07MAR07"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   2250
         TabIndex        =   129
         Top             =   0
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblTopText 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         Caption         =   "                     "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Index           =   0
         Left            =   60
         TabIndex        =   49
         Top             =   15
         Visible         =   0   'False
         Width           =   945
      End
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00800000&
      Height          =   3915
      Index           =   0
      Left            =   1680
      ScaleHeight     =   3855
      ScaleWidth      =   11325
      TabIndex        =   21
      Top             =   2400
      Width           =   11385
      Begin VB.PictureBox VScrollArea 
         BackColor       =   &H00E0E0E0&
         Height          =   3645
         Index           =   0
         Left            =   90
         ScaleHeight     =   3585
         ScaleWidth      =   11055
         TabIndex        =   32
         Top             =   90
         Width           =   11115
         Begin VB.PictureBox DepFlight 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            ForeColor       =   &H80000008&
            Height          =   660
            Index           =   0
            Left            =   5550
            ScaleHeight     =   630
            ScaleWidth      =   5370
            TabIndex        =   168
            Top             =   1320
            Width           =   5400
            Begin VB.Label lblDepPstd 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               TabIndex        =   186
               ToolTipText     =   "Bay"
               Top             =   30
               Width           =   645
            End
            Begin VB.Label lblDepOfbl 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4050
               TabIndex        =   185
               ToolTipText     =   "ATD (OFB)"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblDepFlti 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1125
               TabIndex        =   184
               Top             =   30
               Width           =   300
            End
            Begin VB.Label lblDepVia3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   870
               TabIndex        =   183
               ToolTipText     =   "Next Station"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblDepEtdi 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3450
               TabIndex        =   182
               ToolTipText     =   "ETD"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblDepStod 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               TabIndex        =   181
               ToolTipText     =   "STD"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblDepDes3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   330
               TabIndex        =   180
               ToolTipText     =   "Destination"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblDepFlno 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   30
               TabIndex        =   179
               Top             =   30
               Width           =   1110
            End
            Begin VB.Label lblDepTot3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4200
               MouseIcon       =   "ConnexChart.frx":06D6
               TabIndex        =   178
               ToolTipText     =   "Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblDepTot2 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3300
               MouseIcon       =   "ConnexChart.frx":09E0
               TabIndex        =   177
               ToolTipText     =   "Business"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblDepTot1 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               MouseIcon       =   "ConnexChart.frx":0CEA
               TabIndex        =   176
               ToolTipText     =   "First Class"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblDepCsct 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               MouseIcon       =   "ConnexChart.frx":0FF4
               TabIndex        =   175
               ToolTipText     =   "Shortest Connection Time (Minutes)"
               Top             =   330
               Width           =   645
            End
            Begin VB.Label lblDepRegn 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               TabIndex        =   174
               ToolTipText     =   "Aircraft"
               Top             =   30
               Width           =   855
            End
            Begin VB.Label lblDepAct3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2295
               TabIndex        =   173
               ToolTipText     =   "Type"
               Top             =   30
               Width           =   525
            End
            Begin VB.Label lblDepTotUld 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2355
               MouseIcon       =   "ConnexChart.frx":12FE
               TabIndex        =   172
               ToolTipText     =   "Total Transfer ULD (Amount)"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblDepTotBag 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1905
               MouseIcon       =   "ConnexChart.frx":1608
               TabIndex        =   171
               ToolTipText     =   "Total Transfer BAG (Pieces)"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblDepTotPax 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               MouseIcon       =   "ConnexChart.frx":1912
               TabIndex        =   170
               ToolTipText     =   "Total Transfer PAX"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblDepTot4 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3750
               MouseIcon       =   "ConnexChart.frx":1C1C
               TabIndex        =   169
               ToolTipText     =   "Premium Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Image picDepStatus 
               Appearance      =   0  'Flat
               BorderStyle     =   1  'Fixed Single
               Height          =   270
               Index           =   0
               Left            =   30
               Picture         =   "ConnexChart.frx":1F26
               ToolTipText     =   "Flight Status"
               Top             =   330
               Width           =   270
            End
         End
         Begin VB.PictureBox ArrFlight 
            Appearance      =   0  'Flat
            BackColor       =   &H00E0E0E0&
            ForeColor       =   &H80000008&
            Height          =   660
            Index           =   0
            Left            =   90
            ScaleHeight     =   630
            ScaleWidth      =   5370
            TabIndex        =   149
            Top             =   1320
            Width           =   5400
            Begin VB.Label lblArrAct3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2295
               TabIndex        =   167
               ToolTipText     =   "Aircraft Type"
               Top             =   30
               Width           =   525
            End
            Begin VB.Label lblArrRegn 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               TabIndex        =   166
               ToolTipText     =   "Aircraft Registration"
               Top             =   30
               Width           =   855
            End
            Begin VB.Label lblArrCsct 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               MouseIcon       =   "ConnexChart.frx":2070
               TabIndex        =   165
               ToolTipText     =   "Shortest Connection Time (Minutes)"
               Top             =   330
               Width           =   645
            End
            Begin VB.Label lblArrTot1 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               MouseIcon       =   "ConnexChart.frx":237A
               TabIndex        =   164
               ToolTipText     =   "First Class"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblArrTot2 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3300
               MouseIcon       =   "ConnexChart.frx":2684
               TabIndex        =   163
               ToolTipText     =   "Business"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblArrTot3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4200
               MouseIcon       =   "ConnexChart.frx":298E
               TabIndex        =   162
               ToolTipText     =   "Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblArrFlno 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   30
               TabIndex        =   161
               Top             =   30
               Width           =   1110
            End
            Begin VB.Label lblArrOrg3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   330
               TabIndex        =   160
               ToolTipText     =   "Origin"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblArrStoa 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2850
               TabIndex        =   159
               ToolTipText     =   "STA"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblArrEtai 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3450
               TabIndex        =   158
               ToolTipText     =   "ETA"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblArrVia3 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   870
               TabIndex        =   157
               ToolTipText     =   "Previous Station"
               Top             =   330
               Width           =   555
            End
            Begin VB.Label lblArrFlti 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1125
               TabIndex        =   156
               ToolTipText     =   "Route Identifier"
               Top             =   30
               Width           =   300
            End
            Begin VB.Label lblArrOnbl 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4050
               TabIndex        =   155
               ToolTipText     =   "ATA (ONB)"
               Top             =   30
               Width           =   615
            End
            Begin VB.Label lblArrPsta 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   4695
               TabIndex        =   154
               ToolTipText     =   "Bay"
               Top             =   30
               Width           =   645
            End
            Begin VB.Image picArrStatus 
               Appearance      =   0  'Flat
               BorderStyle     =   1  'Fixed Single
               Height          =   270
               Index           =   0
               Left            =   30
               Picture         =   "ConnexChart.frx":2C98
               ToolTipText     =   "Flight Status"
               Top             =   330
               Width           =   270
            End
            Begin VB.Label lblArrTot4 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   3750
               MouseIcon       =   "ConnexChart.frx":2DE2
               TabIndex        =   153
               ToolTipText     =   "Premium Economy"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblArrTotPax 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1455
               MouseIcon       =   "ConnexChart.frx":30EC
               TabIndex        =   152
               ToolTipText     =   "Total Transfer PAX"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblArrTotBag 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   1905
               MouseIcon       =   "ConnexChart.frx":33F6
               TabIndex        =   151
               ToolTipText     =   "Total Transfer BAG (Pieces)"
               Top             =   330
               Width           =   465
            End
            Begin VB.Label lblArrTotUld 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   270
               Index           =   0
               Left            =   2355
               MouseIcon       =   "ConnexChart.frx":3700
               TabIndex        =   150
               ToolTipText     =   "Total Transfer ULD (Amount)"
               Top             =   330
               Width           =   465
            End
         End
         Begin VB.PictureBox MidScConx 
            AutoRedraw      =   -1  'True
            Height          =   1425
            Index           =   0
            Left            =   90
            ScaleHeight     =   1365
            ScaleWidth      =   10785
            TabIndex        =   79
            Top             =   2040
            Visible         =   0   'False
            Width           =   10845
            Begin VB.PictureBox DepInfoPanel 
               Appearance      =   0  'Flat
               AutoRedraw      =   -1  'True
               BackColor       =   &H00000000&
               ForeColor       =   &H80000008&
               Height          =   300
               Index           =   0
               Left            =   6180
               ScaleHeight     =   270
               ScaleWidth      =   2490
               TabIndex        =   84
               Top             =   60
               Visible         =   0   'False
               Width           =   2520
               Begin VB.Label DepCnxCsct 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   1845
                  TabIndex        =   102
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label DepCnxSum1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   0
                  TabIndex        =   88
                  ToolTipText     =   "First Class"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label DepCnxSum2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   450
                  TabIndex        =   87
                  ToolTipText     =   "Business"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label DepCnxSum3 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   1350
                  TabIndex        =   86
                  ToolTipText     =   "Economy"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label DepCnxSum4 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   900
                  TabIndex        =   85
                  ToolTipText     =   "Premium Economy"
                  Top             =   0
                  Width           =   465
               End
            End
            Begin VB.PictureBox ArrInfoPanel 
               Appearance      =   0  'Flat
               AutoRedraw      =   -1  'True
               BackColor       =   &H00000000&
               ForeColor       =   &H80000008&
               Height          =   300
               Index           =   0
               Left            =   3090
               ScaleHeight     =   270
               ScaleWidth      =   2490
               TabIndex        =   97
               Top             =   60
               Visible         =   0   'False
               Width           =   2520
               Begin VB.Label ArrCnxCsct 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   0
                  TabIndex        =   148
                  Top             =   0
                  Width           =   645
               End
               Begin VB.Label ArrCnxSum4 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   1575
                  TabIndex        =   101
                  ToolTipText     =   "Premium Economy"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label ArrCnxSum3 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00000000&
                  Height          =   270
                  Index           =   0
                  Left            =   2025
                  TabIndex        =   100
                  ToolTipText     =   "Economy"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label ArrCnxSum2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   1125
                  TabIndex        =   99
                  ToolTipText     =   "Business"
                  Top             =   0
                  Width           =   465
               End
               Begin VB.Label ArrCnxSum1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FFFFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   675
                  TabIndex        =   98
                  ToolTipText     =   "First Class"
                  Top             =   0
                  Width           =   465
               End
            End
            Begin VB.PictureBox DepLblPanel 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               ForeColor       =   &H80000008&
               Height          =   660
               Index           =   0
               Left            =   2520
               ScaleHeight     =   630
               ScaleWidth      =   435
               TabIndex        =   93
               Top             =   60
               Visible         =   0   'False
               Width           =   465
               Begin VB.Label lblDepTpax 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   255
                  Index           =   0
                  Left            =   -30
                  MouseIcon       =   "ConnexChart.frx":3A0A
                  TabIndex        =   96
                  ToolTipText     =   "Total Transfer Pax"
                  Top             =   -30
                  Width           =   345
               End
               Begin VB.Label lblDepTbag 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   -60
                  MouseIcon       =   "ConnexChart.frx":3D14
                  TabIndex        =   95
                  ToolTipText     =   "Total Pieces Baggage"
                  Top             =   210
                  Width           =   375
               End
               Begin VB.Label lblDepTuld 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00FF0000&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   225
                  Index           =   0
                  Left            =   -60
                  MouseIcon       =   "ConnexChart.frx":401E
                  TabIndex        =   94
                  ToolTipText     =   "Amount of ULDs"
                  Top             =   435
                  Width           =   465
               End
            End
            Begin VB.PictureBox ArrLblPanel 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               ForeColor       =   &H80000008&
               Height          =   660
               Index           =   0
               Left            =   780
               ScaleHeight     =   630
               ScaleWidth      =   435
               TabIndex        =   89
               Top             =   60
               Visible         =   0   'False
               Width           =   465
               Begin VB.Label lblArrTpax 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   225
                  Index           =   0
                  Left            =   -255
                  MouseIcon       =   "ConnexChart.frx":4328
                  MousePointer    =   99  'Custom
                  TabIndex        =   90
                  ToolTipText     =   "Total Transfer Pax"
                  Top             =   -15
                  Width           =   465
               End
               Begin VB.Label lblArrTuld 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00E0E0E0&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H00FFFFFF&
                  Height          =   225
                  Index           =   0
                  Left            =   -15
                  MouseIcon       =   "ConnexChart.frx":4632
                  TabIndex        =   92
                  ToolTipText     =   "Amount of ULDs"
                  Top             =   405
                  Width           =   465
               End
               Begin VB.Label lblArrTbag 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00E0E0E0&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   -15
                  MouseIcon       =   "ConnexChart.frx":493C
                  TabIndex        =   91
                  ToolTipText     =   "Total Pieces Baggage"
                  Top             =   210
                  Width           =   465
               End
            End
            Begin VB.PictureBox MidTime 
               Appearance      =   0  'Flat
               BackColor       =   &H000000FF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   240
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   81
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.Image picLeftBarCnx 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   1380
               MouseIcon       =   "ConnexChart.frx":4C46
               MousePointer    =   99  'Custom
               Picture         =   "ConnexChart.frx":4F50
               Top             =   450
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Image picRightBarCnx 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   1740
               MouseIcon       =   "ConnexChart.frx":509A
               MousePointer    =   99  'Custom
               Picture         =   "ConnexChart.frx":53A4
               Top             =   450
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Label lblCnxUldBar 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H00FF0000&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   105
               Index           =   0
               Left            =   1350
               TabIndex        =   83
               Top             =   300
               Visible         =   0   'False
               Width           =   1065
            End
            Begin VB.Label lblCnxBagBar 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   105
               Index           =   0
               Left            =   1350
               TabIndex        =   82
               Top             =   180
               Visible         =   0   'False
               Width           =   1065
            End
            Begin VB.Label lblCnxPaxBar 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H0000FF00&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   105
               Index           =   0
               Left            =   1350
               TabIndex        =   80
               Top             =   60
               Visible         =   0   'False
               Width           =   1065
            End
            Begin VB.Image picDepCnx 
               Appearance      =   0  'Flat
               Height          =   240
               Index           =   0
               Left            =   2100
               Picture         =   "ConnexChart.frx":54EE
               Top             =   450
               Visible         =   0   'False
               Width           =   240
            End
         End
         Begin VB.PictureBox HScrollArea 
            Height          =   1125
            Index           =   0
            Left            =   1080
            ScaleHeight     =   1065
            ScaleWidth      =   8865
            TabIndex        =   37
            Top             =   90
            Width           =   8925
            Begin VB.PictureBox TimeLine2 
               Appearance      =   0  'Flat
               BackColor       =   &H000000FF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   3810
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   38
               Top             =   0
               Width           =   45
            End
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               FillColor       =   &H00FFFFFF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   1
               Left            =   120
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   69
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox RangeLine 
               Appearance      =   0  'Flat
               BackColor       =   &H0000FFFF&
               ForeColor       =   &H80000008&
               Height          =   1440
               Index           =   0
               Left            =   30
               ScaleHeight     =   1410
               ScaleWidth      =   15
               TabIndex        =   68
               Top             =   0
               Visible         =   0   'False
               Width           =   45
            End
            Begin VB.PictureBox ChartLine2 
               Height          =   780
               Index           =   0
               Left            =   240
               ScaleHeight     =   720
               ScaleWidth      =   8415
               TabIndex        =   39
               Top             =   120
               Visible         =   0   'False
               Width           =   8475
               Begin VB.Image picDep4 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   5370
                  Picture         =   "ConnexChart.frx":5638
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picArr4 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   2490
                  Picture         =   "ConnexChart.frx":5782
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picDep3 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   5070
                  Picture         =   "ConnexChart.frx":58CC
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picDep2 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   4770
                  Picture         =   "ConnexChart.frx":5A16
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picArr3 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   2190
                  Picture         =   "ConnexChart.frx":5B60
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picArr2 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   1890
                  Picture         =   "ConnexChart.frx":5CAA
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Label lblArrJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   420
                  TabIndex        =   75
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   870
                  TabIndex        =   74
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.Label lblDepJob2 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   3300
                  TabIndex        =   73
                  Top             =   210
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblDepJob1 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   90
                  Index           =   0
                  Left            =   3510
                  TabIndex        =   72
                  Top             =   90
                  Visible         =   0   'False
                  Width           =   885
               End
               Begin VB.Image picArr1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   1590
                  Picture         =   "ConnexChart.frx":5DF4
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Image picDep1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   1  'Fixed Single
                  Height          =   270
                  Index           =   0
                  Left            =   4470
                  Picture         =   "ConnexChart.frx":5F3E
                  Stretch         =   -1  'True
                  Top             =   45
                  Visible         =   0   'False
                  Width           =   270
               End
               Begin VB.Label lblBarLineColor 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H00808080&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   300
                  Index           =   0
                  Left            =   30
                  TabIndex        =   71
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   285
               End
               Begin VB.Label lblTowBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   2820
                  TabIndex        =   67
                  Top             =   360
                  Visible         =   0   'False
                  Width           =   375
               End
               Begin VB.Label lblDepBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FF00&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   3240
                  TabIndex        =   66
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   1095
               End
               Begin VB.Label lblArrBar 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H0000FFFF&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   161
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   270
                  Index           =   0
                  Left            =   360
                  TabIndex        =   58
                  Top             =   30
                  Visible         =   0   'False
                  Width           =   1005
               End
            End
         End
         Begin VB.PictureBox RightScale 
            BackColor       =   &H00C0C0C0&
            Height          =   1125
            Index           =   0
            Left            =   10140
            ScaleHeight     =   1065
            ScaleWidth      =   765
            TabIndex        =   35
            Top             =   90
            Width           =   825
            Begin VB.PictureBox ChartLine3 
               Height          =   780
               Index           =   0
               Left            =   90
               ScaleHeight     =   720
               ScaleWidth      =   495
               TabIndex        =   36
               Top             =   120
               Width           =   555
            End
         End
         Begin VB.PictureBox LeftScale 
            BackColor       =   &H00C0C0C0&
            Height          =   1125
            Index           =   0
            Left            =   90
            ScaleHeight     =   1065
            ScaleWidth      =   825
            TabIndex        =   33
            Top             =   90
            Width           =   885
            Begin VB.PictureBox ChartLine1 
               Height          =   780
               Index           =   0
               Left            =   90
               ScaleHeight     =   720
               ScaleWidth      =   525
               TabIndex        =   34
               Top             =   120
               Width           =   585
            End
         End
      End
   End
   Begin VB.PictureBox VertScroll2 
      Height          =   3315
      Index           =   0
      Left            =   13110
      ScaleHeight     =   3255
      ScaleWidth      =   255
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   2400
      Width           =   315
      Begin VB.VScrollBar VScroll2 
         Height          =   1605
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox TimePanel 
      BackColor       =   &H00E0E0E0&
      Height          =   825
      Index           =   0
      Left            =   1680
      ScaleHeight     =   765
      ScaleWidth      =   11325
      TabIndex        =   17
      Top             =   1500
      Width           =   11385
      Begin VB.PictureBox TimeScale 
         AutoRedraw      =   -1  'True
         Height          =   825
         Index           =   0
         Left            =   1230
         ScaleHeight     =   765
         ScaleWidth      =   5925
         TabIndex        =   31
         Top             =   -30
         Width           =   5985
         Begin VB.Timer TimeScaleTimer 
            Enabled         =   0   'False
            Interval        =   1000
            Left            =   1260
            Top             =   0
         End
         Begin VB.PictureBox TimeLine1 
            Appearance      =   0  'Flat
            BackColor       =   &H000000FF&
            ForeColor       =   &H80000008&
            Height          =   1440
            Index           =   0
            Left            =   5400
            ScaleHeight     =   1410
            ScaleWidth      =   15
            TabIndex        =   40
            Top             =   255
            Width           =   45
         End
         Begin VB.Label lblDate 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "24SEP07"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4215
            TabIndex        =   70
            Top             =   540
            Visible         =   0   'False
            Width           =   675
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   1
            Left            =   5490
            Picture         =   "ConnexChart.frx":6088
            Top             =   510
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image icoCflRange 
            Height          =   240
            Index           =   0
            Left            =   5130
            Picture         =   "ConnexChart.frx":61D2
            Top             =   510
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label lblBgnTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H000080FF&
            Caption         =   "24SEP07"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   120
            TabIndex        =   59
            Top             =   540
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblMin10 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   165
            Index           =   0
            Left            =   4680
            TabIndex        =   57
            Top             =   345
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin30 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4590
            TabIndex        =   56
            Top             =   300
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin1 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   60
            Index           =   0
            Left            =   4860
            TabIndex        =   55
            Top             =   450
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin5 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   120
            Index           =   0
            Left            =   4770
            TabIndex        =   54
            Top             =   390
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblMin60 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00000000&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Index           =   0
            Left            =   4500
            TabIndex        =   53
            Top             =   240
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Label lblDuration 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            Caption         =   "01:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   1140
            TabIndex        =   52
            Top             =   525
            Visible         =   0   'False
            Width           =   2895
         End
         Begin VB.Label lblHour 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "15:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Index           =   0
            Left            =   4305
            TabIndex        =   51
            Top             =   45
            Visible         =   0   'False
            Width           =   435
         End
         Begin VB.Label lblEndTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "16:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   0
            Left            =   2700
            TabIndex        =   42
            Top             =   30
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Label lblCurTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "00:00:00"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Index           =   0
            Left            =   5070
            TabIndex        =   41
            Top             =   30
            Width           =   735
         End
      End
   End
   Begin VB.PictureBox TopPanel 
      BackColor       =   &H000040C0&
      Height          =   690
      Left            =   60
      ScaleHeight     =   630
      ScaleWidth      =   13305
      TabIndex        =   16
      Top             =   450
      Visible         =   0   'False
      Width           =   13365
      Begin VB.Frame Frame7 
         Caption         =   "Test Panel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   4830
         TabIndex        =   125
         Top             =   0
         Width           =   1215
         Begin VB.CheckBox chkCnxTest 
            Caption         =   "CNX"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   127
            Top             =   240
            Width           =   525
         End
         Begin VB.CheckBox chkCnxTest 
            Caption         =   "LAY"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   615
            Style           =   1  'Graphical
            TabIndex        =   126
            Top             =   240
            Width           =   525
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Data Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   3090
         TabIndex        =   121
         Top             =   0
         Visible         =   0   'False
         Width           =   1695
         Begin VB.CheckBox chkCnxData 
            Caption         =   "ACT"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1110
            Style           =   1  'Graphical
            TabIndex        =   124
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkCnxData 
            Caption         =   "APC"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   600
            Style           =   1  'Graphical
            TabIndex        =   123
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkCnxData 
            Caption         =   "ALC"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   122
            Top             =   240
            Width           =   510
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Layout Types"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   6090
         TabIndex        =   113
         Top             =   0
         Visible         =   0   'False
         Width           =   2145
         Begin VB.CheckBox chkLayout 
            Caption         =   "5"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   6
            Left            =   3210
            Style           =   1  'Graphical
            TabIndex        =   120
            Top             =   0
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "3"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   5
            Left            =   2730
            Style           =   1  'Graphical
            TabIndex        =   119
            Top             =   0
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   2220
            Style           =   1  'Graphical
            TabIndex        =   118
            Top             =   0
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   540
            Style           =   1  'Graphical
            TabIndex        =   117
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   116
            Top             =   240
            Width           =   450
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "3"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1050
            Style           =   1  'Graphical
            TabIndex        =   115
            Top             =   240
            Width           =   510
         End
         Begin VB.CheckBox chkLayout 
            Caption         =   "4"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   1560
            Style           =   1  'Graphical
            TabIndex        =   114
            Top             =   240
            Width           =   510
         End
      End
   End
   Begin VB.PictureBox VertScroll1 
      Height          =   3315
      Index           =   0
      Left            =   1320
      ScaleHeight     =   3255
      ScaleWidth      =   255
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2400
      Visible         =   0   'False
      Width           =   315
      Begin VB.VScrollBar VScroll1 
         Height          =   1605
         Index           =   0
         LargeChange     =   10
         Left            =   0
         Max             =   100
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   0
         Width           =   255
      End
   End
   Begin VB.PictureBox ButtonPanel 
      AutoRedraw      =   -1  'True
      Height          =   375
      Left            =   60
      ScaleHeight     =   315
      ScaleWidth      =   13305
      TabIndex        =   14
      Top             =   60
      Width           =   13365
      Begin VB.CheckBox chkWork 
         Caption         =   "Connection Time"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   5190
         Style           =   1  'Graphical
         TabIndex        =   194
         Tag             =   "AODB_FILTER"
         Top             =   0
         Width           =   1875
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   2070
         Style           =   1  'Graphical
         TabIndex        =   193
         Tag             =   "AODB_REREAD"
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   192
         Tag             =   "AODB_FILTER"
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Reload"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   191
         Tag             =   "AODB_RELOAD"
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Options"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   4140
         Style           =   1  'Graphical
         TabIndex        =   112
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Overlap"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   3090
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox LeftPanel 
      Height          =   5175
      Index           =   0
      Left            =   60
      ScaleHeight     =   5115
      ScaleWidth      =   1125
      TabIndex        =   13
      Top             =   1200
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   6330
      Left            =   13500
      ScaleHeight     =   6270
      ScaleWidth      =   1155
      TabIndex        =   0
      Top             =   60
      Width           =   1215
      Begin VB.Frame fraYButtonPanel 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   1800
         Index           =   0
         Left            =   60
         TabIndex        =   4
         Top             =   390
         Width           =   1035
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "CL"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   78
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   1380
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "CR"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   525
            Style           =   1  'Graphical
            TabIndex        =   77
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Right Scale ON/OFF"
            Top             =   1380
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "CX Panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   76
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   660
            Width           =   1035
         End
         Begin VB.CheckBox chkAppl 
            Caption         =   "Close"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   9
            Tag             =   "CLOSE"
            Top             =   0
            Width           =   1035
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "UTC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   525
            Style           =   1  'Graphical
            TabIndex        =   8
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to UTC Times"
            Top             =   330
            Width           =   510
         End
         Begin VB.CheckBox chkUtc 
            Caption         =   "LOC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   7
            Tag             =   "SETUP"
            ToolTipText     =   "Switch to Local Times"
            Top             =   330
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "RS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   525
            Style           =   1  'Graphical
            TabIndex        =   6
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Right Scale ON/OFF"
            Top             =   1050
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.CheckBox chkSelDeco 
            Caption         =   "LS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   5
            Tag             =   "SETUP"
            ToolTipText     =   "Use of Left Scale ON/OFF"
            Top             =   1050
            Visible         =   0   'False
            Width           =   510
         End
      End
      Begin VB.CheckBox chkTabIsVisible 
         Height          =   225
         Index           =   0
         Left            =   30
         MaskColor       =   &H8000000F&
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "-1"
         Top             =   2790
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.CheckBox OnTop 
         Caption         =   "Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox chkTerminate 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.Label lblTabShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Index           =   0
         Left            =   270
         TabIndex        =   11
         Top             =   3030
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Label lblTabName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Index           =   0
         Left            =   300
         TabIndex        =   10
         Top             =   2850
         Visible         =   0   'False
         Width           =   570
      End
      Begin VB.Line linDark 
         BorderColor     =   &H00404040&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   3360
         Y2              =   3360
      End
      Begin VB.Line linLight 
         BorderColor     =   &H00FFFFFF&
         Index           =   0
         Visible         =   0   'False
         X1              =   0
         X2              =   1200
         Y1              =   2760
         Y2              =   2760
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   12
      Top             =   10560
      Width           =   15780
      _ExtentX        =   27834
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19500
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1058
            TextSave        =   "21/05/2012"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            AutoSize        =   2
            Object.Width           =   1058
            MinWidth        =   1058
            TextSave        =   "12:11"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   953
            MinWidth        =   706
            TextSave        =   "CAPS"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   794
            MinWidth        =   706
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            Alignment       =   1
            AutoSize        =   2
            Enabled         =   0   'False
            Object.Width           =   714
            MinWidth        =   706
            TextSave        =   "INS"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TABLib.TAB TabDepCnxDetails 
      Height          =   975
      Left            =   7920
      TabIndex        =   188
      Top             =   9210
      Width           =   7815
      _Version        =   65536
      _ExtentX        =   13785
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin VB.Image CnxArrIcon 
      Appearance      =   0  'Flat
      Height          =   240
      Index           =   6
      Left            =   15450
      Picture         =   "ConnexChart.frx":631C
      Top             =   510
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image CnxArrIcon 
      Appearance      =   0  'Flat
      Height          =   240
      Index           =   5
      Left            =   15450
      Picture         =   "ConnexChart.frx":6466
      Top             =   810
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image CnxArrIcon 
      Appearance      =   0  'Flat
      Height          =   240
      Index           =   4
      Left            =   14850
      Picture         =   "ConnexChart.frx":65B0
      Top             =   1110
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image CnxArrIcon 
      Appearance      =   0  'Flat
      Height          =   240
      Index           =   3
      Left            =   15450
      Picture         =   "ConnexChart.frx":66FA
      Top             =   1110
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image CnxArrIcon 
      Appearance      =   0  'Flat
      Height          =   240
      Index           =   0
      Left            =   14820
      Picture         =   "ConnexChart.frx":6844
      Top             =   510
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image CnxArrIcon 
      Appearance      =   0  'Flat
      Height          =   240
      Index           =   1
      Left            =   14850
      Picture         =   "ConnexChart.frx":698E
      Top             =   810
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image CnxArrIcon 
      Appearance      =   0  'Flat
      Height          =   240
      Index           =   2
      Left            =   15150
      Picture         =   "ConnexChart.frx":6AD8
      Top             =   1110
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picNeutral 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   15420
      Picture         =   "ConnexChart.frx":6C22
      Stretch         =   -1  'True
      Top             =   3270
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image imgLookUpB 
      Height          =   240
      Left            =   15120
      Picture         =   "ConnexChart.frx":6D6C
      Top             =   3270
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgLookUpA 
      Height          =   240
      Left            =   14850
      Picture         =   "ConnexChart.frx":72F6
      Top             =   3270
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPosDn 
      Height          =   240
      Left            =   14820
      Picture         =   "ConnexChart.frx":7880
      Top             =   3660
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgCurPosUp 
      Height          =   240
      Left            =   15090
      Picture         =   "ConnexChart.frx":7E0A
      Top             =   3660
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   6
      Left            =   14970
      Picture         =   "ConnexChart.frx":8394
      Top             =   270
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   5
      Left            =   14790
      Picture         =   "ConnexChart.frx":84DE
      Top             =   270
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   4
      Left            =   15150
      Picture         =   "ConnexChart.frx":8628
      Top             =   270
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   3
      Left            =   15330
      Picture         =   "ConnexChart.frx":8772
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   2
      Left            =   15150
      Picture         =   "ConnexChart.frx":88BC
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   1
      Left            =   14970
      Picture         =   "ConnexChart.frx":8A06
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picSmallIcon 
      Height          =   240
      Index           =   0
      Left            =   14790
      Picture         =   "ConnexChart.frx":8B50
      Top             =   90
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picInfoPics 
      Height          =   240
      Index           =   1
      Left            =   15090
      Picture         =   "ConnexChart.frx":8C9A
      Top             =   2880
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picInfoPics 
      Height          =   240
      Index           =   0
      Left            =   14790
      Picture         =   "ConnexChart.frx":8DE4
      Top             =   2880
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   14790
      Picture         =   "ConnexChart.frx":8F2E
      Stretch         =   -1  'True
      Top             =   1920
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   15120
      Picture         =   "ConnexChart.frx":9078
      Stretch         =   -1  'True
      Top             =   1920
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image picMarker 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   15450
      Picture         =   "ConnexChart.frx":9402
      Stretch         =   -1  'True
      Top             =   1920
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   0
      Left            =   14790
      Picture         =   "ConnexChart.frx":9844
      Stretch         =   -1  'True
      Top             =   2250
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   1
      Left            =   15090
      Picture         =   "ConnexChart.frx":9BCE
      Stretch         =   -1  'True
      Top             =   2250
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   2
      Left            =   15390
      Picture         =   "ConnexChart.frx":A010
      Stretch         =   -1  'True
      Top             =   2250
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image picComPics 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   270
      Index           =   3
      Left            =   14790
      Picture         =   "ConnexChart.frx":A452
      Stretch         =   -1  'True
      Top             =   2550
      Visible         =   0   'False
      Width           =   270
   End
End
Attribute VB_Name = "ConnexChart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MainAreaColor As Integer
Dim DataAreaColor As Integer
Dim WorkAreaColor As Integer
Dim ChartBarBackColor As Long
Dim MinimumTop As Long
Dim MinimumLeft As Long
Dim MinuteWidth As Long
Dim MaxChartHeight As Long
Dim MaxChartWidth As Long
Dim MaxBarCnt As Integer
Dim CurBarMax As Integer
Dim CurChartHeight As Long
Dim CurChartWidth As Long
Dim CurScrollHeight As Long
Dim CurScrollWidth As Long
Dim CurBarIdx As Integer
Dim PrvBarIdx As Integer
Dim DataWindowBeginUtc As String
Dim DataWindowBeginLoc As String
Dim ServerUtcTimeStr As String
Dim ServerLocTimeStr As String
Dim ChartName As String
Dim LastLocTimeCheck
Dim LastUtcTimeCheck
Dim ServerUtcTimeVal
Dim TimeScaleUtcBegin
Dim CurUtcTimeDiff As Integer
Dim CnxLayoutType As Integer
Dim CurLayoutType As Integer
Dim MinCnxLayoutWidth As Long
Dim MinCnxConnexWidth As Long
Dim CurCnxLayoutHours As Long
Dim CurCnxBarIdx As Integer
Dim LastCnxArrBar As Integer
Dim LastCnxDepBar As Integer
Dim CnxFilterAdid As Integer
Dim CnxFilterType As Integer
Dim CnxFilterClass As Integer
Dim CnxFilterShort As Integer
Dim CnxFilterCritical As Integer
Dim ChartIsReadyForUse As Boolean
Dim CnxRangeMin As Long
Dim DontRefreshCnx As Boolean
Dim CurCnxView As Integer
Dim CnxMouseObj As String

Public Sub ToggleFlightMarker(FltAdid As String, FltBar As Integer, SetMarker As Boolean)
    Dim PicIdx As Integer
    If FltAdid = "A" Then
        If SetMarker = True Then PicIdx = 1 Else PicIdx = 0
        picArrStatus(FltBar).Picture = picMarker(PicIdx).Picture
        picArrStatus(FltBar).Tag = CStr(PicIdx)
    End If
    If FltAdid = "D" Then
        If SetMarker = True Then PicIdx = 1 Else PicIdx = 0
        picDepStatus(FltBar).Picture = picMarker(PicIdx).Picture
        picDepStatus(FltBar).Tag = CStr(PicIdx)
    End If
End Sub
Public Sub ScrollSyncExtern(RotLine As Long)
    Dim tmpGidx As String
    Dim tmpBest As String
    Dim tmpBestPos As Long
    Dim NewScroll As Integer
    Dim tmpScrVal As Integer
    Dim PosIsValid As Boolean
    Dim CedaTime
    If RotLine >= 0 Then
        tmpGidx = TabRotFlightsTab.GetFieldValue(RotLine, "CONX")
        If tmpGidx <> "------" Then
            tmpScrVal = Val(tmpGidx)
            VScroll2(0).Value = tmpScrVal
            tmpBest = TabRotFlightsTab.GetFieldValue(RotLine, "BEST")
            PosIsValid = GetTimeScalePos(tmpBest, CedaTime, tmpBestPos)
            tmpBestPos = tmpBestPos \ MinuteWidth
            If tmpBestPos > 3600 Then tmpBestPos = 3600
            If tmpBestPos < 0 Then tmpBestPos = 0
            tmpBestPos = tmpBestPos - (TimePanel(0).Width \ MinuteWidth \ 2)
            If tmpBestPos < 0 Then tmpBestPos = 0
            NewScroll = CInt(tmpBestPos)
            HScroll2(0).Value = NewScroll
            HighlightCurrentBar tmpScrVal, "CNX"
            If SyncOriginator = "CNX" Then SyncOriginator = ""
        End If
    End If
End Sub

Private Sub ArrFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ArrFlight_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub ChartLine1_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
    LeftScale(0).ZOrder
End Sub

Private Sub ChartLine1_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub ChartLine2_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub
Private Sub HighlightCurrentBar(Index As Integer, KeyOrig As String)
    Dim LineNo As Long
    Dim CurGocx As String
    Dim iCol As Integer
    If Index <> CurBarIdx Then
        If SyncOriginator = "" Then SyncOriginator = KeyOrig
        CurBarIdx = Index
        If (PrvBarIdx <> CurBarIdx) And (PrvBarIdx >= 0) Then
            ChartLine1(PrvBarIdx).BackColor = lblBarLineColor(PrvBarIdx).BackColor
            ChartLine1(PrvBarIdx).Refresh
            ChartLine3(PrvBarIdx).BackColor = lblBarLineColor(PrvBarIdx).BackColor
            ChartLine3(PrvBarIdx).Refresh
            ChartLine2(PrvBarIdx).BackColor = lblBarLineColor(PrvBarIdx).BackColor
            ChartLine2(PrvBarIdx).Refresh
        End If
        iCol = Val(lblBarLineColor(CurBarIdx).Tag)
        DrawBackGround ChartLine2(CurBarIdx), iCol, True, True
        ChartLine2(CurBarIdx).Refresh
        If ArrFlight(CurBarIdx).Visible = True Then
            DrawBackGround ChartLine1(CurBarIdx), iCol, True, True
            ChartLine1(CurBarIdx).Refresh
        End If
        If DepFlight(CurBarIdx).Visible = True Then
            DrawBackGround ChartLine3(CurBarIdx), iCol, True, True
            ChartLine3(CurBarIdx).Refresh
        End If
        If SyncOriginator = ChartName Then
            CurGocx = Right("000000" & CStr(Index), 6)
            MarkLookupLines TabRotFlightsTab, "CONX", CurGocx, True, "DRGN,AFLT,DFLT", True, "'S1'", "DecoMarkerLB"
            LineNo = TabRotFlightsTab.GetCurrentSelected
            MainDialog.RotationData_RowSelectionChanged LineNo, True
        End If
        PrvBarIdx = Index
        If SyncOriginator = KeyOrig Then SyncOriginator = ""
    End If
End Sub

Private Sub ChartLine3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
    RightScale(0).ZOrder
End Sub

Private Sub ChartLine3_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then
        chkAppl(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                
                If sSHOW_FILTER_BUTTON = "YES" Then 'If the application is standalone, shut it down straight away
                    ShutDownApplication True
                Else ' If not, go by as usual
                    Me.Hide
                    MainDialog.PushWorkButton "CONX_CHART", False
                    chkAppl(Index).Value = 0
                End If
                
            Case Else
        End Select
    Else
        chkAppl(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkCnxAdid_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurCurCnxBarIdx As Integer
    If chkCnxAdid(Index).Value = 1 Then
        chkCnxAdid(Index).BackColor = LightGreen
        tmpTag = chkCnxAdid(0).Tag
        chkCnxAdid(0).Tag = CStr(Index)
        CnxFilterAdid = Index
        CurCurCnxBarIdx = CurCnxBarIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxAdid(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            If Index = 0 Then CurCurCnxBarIdx = LastCnxArrBar Else CurCurCnxBarIdx = LastCnxDepBar
            If CurCurCnxBarIdx >= 0 Then
                ShowConnexDetails CurCurCnxBarIdx
            Else
                ShowAllConnexLines
            End If
        End If
    Else
        chkCnxAdid(Index).BackColor = vbButtonFace
        tmpTag = chkCnxAdid(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxAdid(0).Tag = ""
                CnxFilterAdid = -1
                If Index = 0 Then chkCnxAdid(1).Value = 1 Else chkCnxAdid(0).Value = 1
            End If
        End If
    End If
End Sub

Private Sub chkCnxClass_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurCurCnxBarIdx As Integer
    If chkCnxClass(Index).Value = 1 Then
        chkCnxClass(Index).BackColor = LightGreen
        tmpTag = chkCnxClass(0).Tag
        chkCnxClass(0).Tag = CStr(Index)
        CnxFilterClass = Index + 1
        CurCurCnxBarIdx = CurCnxBarIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxClass(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            If CurCurCnxBarIdx >= 0 Then
                ShowConnexDetails CurCurCnxBarIdx
            Else
                ShowAllConnexLines
            End If
        End If
    Else
        chkCnxClass(Index).BackColor = vbButtonFace
        tmpTag = chkCnxClass(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxClass(0).Tag = ""
                CnxFilterClass = 0
                If DontRefreshCnx = False Then
                    CurCurCnxBarIdx = CurCnxBarIdx
                    If CurCurCnxBarIdx >= 0 Then
                        ShowConnexDetails CurCurCnxBarIdx
                    Else
                        ShowAllConnexLines
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkCnxCrit_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurCurCnxBarIdx As Integer
    If chkCnxCrit(Index).Value = 1 Then
        chkCnxCrit(Index).BackColor = LightGreen
        tmpTag = chkCnxCrit(0).Tag
        chkCnxCrit(0).Tag = CStr(Index)
        CnxFilterCritical = Index + 1
        CurCurCnxBarIdx = CurCnxBarIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxCrit(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            'Assuming we have only one button
            'of "Short" and "Critical"
            DontRefreshCnx = True
            chkCnxShort(0).Value = 0
            DontRefreshCnx = False
            If CurCurCnxBarIdx >= 0 Then
                ShowConnexDetails CurCurCnxBarIdx
            Else
                ShowAllConnexLines
            End If
        End If
    Else
        chkCnxCrit(Index).BackColor = vbButtonFace
        tmpTag = chkCnxCrit(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxCrit(0).Tag = ""
                CnxFilterCritical = 0
                If DontRefreshCnx = False Then
                    CurCurCnxBarIdx = CurCnxBarIdx
                    If CurCurCnxBarIdx >= 0 Then
                        ShowConnexDetails CurCurCnxBarIdx
                    Else
                        ShowAllConnexLines
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkCnxShort_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurCurCnxBarIdx As Integer
    If chkCnxShort(Index).Value = 1 Then
        chkCnxShort(Index).BackColor = LightGreen
        tmpTag = chkCnxShort(0).Tag
        chkCnxShort(0).Tag = CStr(Index)
        CnxFilterShort = Index + 1
        CurCurCnxBarIdx = CurCnxBarIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxShort(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            'Assuming we have only one button
            'of "Short" and "Critical"
            DontRefreshCnx = True
            chkCnxCrit(0).Value = 0
            DontRefreshCnx = False
            If CurCurCnxBarIdx >= 0 Then
                ShowConnexDetails CurCurCnxBarIdx
            Else
                ShowAllConnexLines
            End If
        End If
    Else
        chkCnxShort(Index).BackColor = vbButtonFace
        tmpTag = chkCnxShort(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxShort(0).Tag = ""
                CnxFilterShort = 0
                If DontRefreshCnx = False Then
                    CurCurCnxBarIdx = CurCnxBarIdx
                    If CurCurCnxBarIdx >= 0 Then
                        ShowConnexDetails CurCurCnxBarIdx
                    Else
                        ShowAllConnexLines
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkCnxType_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurCurCnxBarIdx As Integer
    If chkCnxType(Index).Value = 1 Then
        chkCnxType(Index).BackColor = LightGreen
        tmpTag = chkCnxType(0).Tag
        chkCnxType(0).Tag = CStr(Index)
        CnxFilterType = Index + 1
        CurCurCnxBarIdx = CurCnxBarIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            chkCnxType(i).Value = 0
        End If
        If DontRefreshCnx = False Then
            If CurCurCnxBarIdx >= 0 Then
                ShowConnexDetails CurCurCnxBarIdx
            Else
                ShowAllConnexLines
            End If
        End If
    Else
        chkCnxType(Index).BackColor = vbButtonFace
        tmpTag = chkCnxType(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkCnxType(0).Tag = ""
                CnxFilterType = 0
                If DontRefreshCnx = False Then
                    CurCurCnxBarIdx = CurCnxBarIdx
                    If CurCurCnxBarIdx >= 0 Then
                        ShowConnexDetails CurCurCnxBarIdx
                    Else
                        ShowAllConnexLines
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub chkLayout_Click(Index As Integer)
    Dim tmpTag As String
    Dim i As Integer
    Dim CurCurCnxBarIdx As Integer
    If chkLayout(Index).Value = 1 Then
        chkLayout(Index).BackColor = LightGreen
        tmpTag = chkLayout(0).Tag
        chkLayout(0).Tag = CStr(Index)
        CurCurCnxBarIdx = CurCnxBarIdx
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If i <> Index Then chkLayout(i).Value = 0
        End If
        CnxLayoutType = Index
        If CurCurCnxBarIdx >= 0 Then
            If DontRefreshCnx = False Then ShowConnexDetails CurCurCnxBarIdx
        End If
        If CnxLayoutType > 1 Then CurLayoutType = CnxLayoutType
        If CurLayoutType < 1 Then CurLayoutType = 4
    Else
        chkLayout(Index).BackColor = vbButtonFace
        tmpTag = chkLayout(0).Tag
        If tmpTag <> "" Then
            i = Val(tmpTag)
            If Index = i Then
                chkLayout(Index).Value = 1
                chkLayout(Index).BackColor = LightGreen
            End If
        End If
    End If
End Sub

Private Sub chkScroll1_Click(Index As Integer)
    If chkScroll1(Index).Value = 1 Then
        Select Case Index
            Case 0
                If VertScroll1(0).Visible = True Then
                    VertScroll1(0).Visible = False
                    TimeScroll1(0).Visible = False
                Else
                    VertScroll1(0).Visible = True
                    TimeScroll1(0).Visible = True
                End If
                chkScroll1(Index).Value = 0
                Form_Resize
            Case Else
                chkScroll1(Index).Value = 0
        End Select
    Else
    End If
End Sub
Private Sub chkScroll2_Click(Index As Integer)
    If chkScroll2(Index).Value = 1 Then
        Select Case Index
            Case 0
                If VertScroll2(0).Visible = True Then
                    VertScroll2(0).Visible = False
                    TimeScroll2(0).Visible = False
                Else
                    VertScroll2(0).Visible = True
                    TimeScroll2(0).Visible = True
                End If
                chkScroll2(Index).Value = 0
                Form_Resize
            Case Else
                chkScroll2(Index).Value = 0
        End Select
    Else
    End If
End Sub

Private Sub chkSelDeco_Click(Index As Integer)
    If chkSelDeco(Index).Value = 1 Then
        chkSelDeco(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                ToggleLeftScaleBars True
            Case 1
                ToggleRightScaleBars True
            Case 2
                ToggleMidCnxPanel True
                chkSelDeco(0).Value = 1
                chkSelDeco(1).Value = 1
                chkSelDeco(3).Value = 1
                chkSelDeco(4).Value = 1
            Case 3
                ToggleLeftCnxPanel True
            Case 4
                ToggleRightCnxPanel True
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                ToggleLeftScaleBars False
            Case 1
                ToggleRightScaleBars False
            Case 2
                ToggleMidCnxPanel False
                chkSelDeco(0).Value = 0
                chkSelDeco(1).Value = 0
                chkSelDeco(3).Value = 0
                chkSelDeco(4).Value = 0
            Case 3
                ToggleLeftCnxPanel False
            Case 4
                ToggleRightCnxPanel False
            Case Else
        End Select
        chkSelDeco(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ToggleLeftCnxPanel(ShowPanel As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To ArrInfoPanel.UBound
        If ShowPanel = True Then
            'Set ArrInfoPanel(i).Container = MidScConx(0)
            'ArrInfoPanel(i).Top = ChartLine1(i).Top + 15
            'ArrInfoPanel(i).Left = 150
            'Set ArrInfoPanel(i).Container = MidScConx(0)
        Else
            'Set ArrInfoPanel(i).Container = ChartLine2(i)
            'ArrInfoPanel(i).Top = -15
            'ArrInfoPanel(i).Left = lblArrBar(i).Left
        End If
    Next
    Screen.MousePointer = 0
End Sub

Private Sub ToggleMidCnxPanel(ShowPanel As Boolean)
    'RightScCon1(0).Left = RightScale(0).Left
    'RightScCon1(0).Width = RightScale(0).Width
    'fraTabCaption(0).Width = MidScCon1(0).Width
    'lblTabCaption(0).Width = fraTabCaption(0).Width - 240
    'lblTabCaptionShadow(0).Width = lblTabCaption(0).Width
    LeftScCon1(0).Visible = ShowPanel
    MidScConx(0).Visible = ShowPanel
    MidScCon1(0).Visible = ShowPanel
    RightScCon1(0).Visible = ShowPanel
End Sub

Private Sub ToggleRightCnxPanel(ShowPanel As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    'NewLeft = RightTime(0).Left + 150
    For i = 0 To DepInfoPanel.UBound
        If ShowPanel = True Then
            'Set DepInfoPanel(i).Container = MidScConx(0)
            'DepInfoPanel(i).Top = ChartLine3(i).Top + 15
            'DepInfoPanel(i).Left = NewLeft
        Else
            'Set DepInfoPanel(i).Container = ChartLine2(i)
            'DepInfoPanel(i).Top = -15
            'NewLeft = lblDepBar(i).Left + lblDepBar(i).Width - DepInfoPanel(i).Width
            'DepInfoPanel(i).Left = NewLeft
        End If
    Next
    Screen.MousePointer = 0
End Sub
Private Sub ToggleLeftScaleBars(ShowLeftScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To ArrFlight.UBound
        ArrFlight(i).Visible = False
        If ShowLeftScale = True Then
            Set ArrFlight(i).Container = ChartLine1(i)
            NewLeft = 30
            ArrFlight(i).Left = NewLeft
            'NewLeft = NewLeft + ArrFlight(i).Width + 30
            If lblArrBar(i).Tag = "Y" Then
                ArrFlight(i).Visible = True
            End If
        Else
            Set ArrFlight(i).Container = ChartLine2(i)
            NewLeft = lblArrBar(i).Left
            NewLeft = NewLeft - ArrFlight(i).Width - 30
            ArrFlight(i).Left = NewLeft
            If lblArrBar(i).Tag = "Y" Then
                ArrFlight(i).Visible = True
                'lblArrBar(i).Visible = True
            End If
        End If
    Next
    Screen.MousePointer = 0
    LeftScale(0).Visible = ShowLeftScale
End Sub
Private Sub ToggleRightScaleBars(ShowRightScale As Boolean)
    Dim i As Integer
    Dim NewLeft As Long
    Screen.MousePointer = 11
    For i = 0 To DepFlight.UBound
        DepFlight(i).Visible = False
        'DepLblPanel(i).Visible = False
        If ShowRightScale = True Then
            Set DepFlight(i).Container = ChartLine3(i)
            NewLeft = 30
            DepFlight(i).Left = NewLeft
            'NewLeft = NewLeft + DepFlight(i).Width + 30
            If lblDepBar(i).Tag = "Y" Then
                DepFlight(i).Visible = True
            End If
        Else
            Set DepFlight(i).Container = ChartLine2(i)
            NewLeft = lblDepBar(i).Left + lblDepBar(i).Width
            NewLeft = NewLeft + 30
            DepFlight(i).Left = NewLeft
            If lblDepBar(i).Tag = "Y" Then
                DepFlight(i).Visible = True
                'lblDepBar(i).Visible = True
            End If
        End If
    Next
    Screen.MousePointer = 0
    RightScale(0).Visible = ShowRightScale
End Sub

Private Sub chkTime1_Click(Index As Integer)
    If chkTime1(Index).Value = 1 Then
        chkTime1(Index).BackColor = LightGreen
        chkTime1(0).Value = 1
        chkTime1(1).Value = 1
    Else
        chkTime1(0).Value = 0
        chkTime1(1).Value = 0
        chkTime1(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkUtc_Click(Index As Integer)
    If chkUtc(Index).Value = 1 Then
        chkUtc(Index).BackColor = LightGreen
        If Index = 0 Then chkUtc(1).Value = 0 Else chkUtc(0).Value = 0
        CreateTimeScaleArea TimeScale(0)
        TimeScaleTimer_Timer
        If Index = 1 Then
            ToggleFlightBarTimes UtcTimeDiff
            CurUtcTimeDiff = UtcTimeDiff
        Else
            ToggleFlightBarTimes 0
            CurUtcTimeDiff = 0
        End If
        If chkCnxAdid(0).Value = 1 Then SetCnxArrTimeScale CurCnxBarIdx
        If chkCnxAdid(1).Value = 1 Then SetCnxDepTimeScale CurCnxBarIdx
    Else
        If Index = 0 Then chkUtc(1).Value = 1 Else chkUtc(0).Value = 1
        chkUtc(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub CreateTimeScaleArea(TimeScale As PictureBox)
    Dim iMin As Integer
    Dim iMin5 As Integer
    Dim iMin10 As Integer
    Dim iMin30 As Integer
    Dim iMin60 As Integer
    Dim iHour As Integer
    Dim iBar As Integer
    Dim lWidth As Long
    Dim LeftPos As Long
    Dim LeftOff1 As Long
    Dim LeftOff2 As Long
    Dim TopPos As Long
    Dim DisplayDateTime As String
    Dim CurDate As String
    Dim CurSsimDate As String
    Dim CurUtcNowTime
    Dim CurLocNowTime
    
    If DataWindowBeginUtc = "" Then
        LastLocTimeCheck = Now
        'Create a Default UTC Time
        CurUtcNowTime = DateAdd("n", -UtcTimeDiff, LastLocTimeCheck)
        LastUtcTimeCheck = CurUtcNowTime
        'Create a Default Data Window Begin
        CurUtcNowTime = DateAdd("h", -2, CurUtcNowTime)
        DataWindowBeginUtc = Format(CurUtcNowTime, "YYYYMMDDhhmmss")
    End If
    
    If ServerUtcTimeStr = "" Then
        'Create a Default Server Reference Time
        ServerUtcTimeVal = LastUtcTimeCheck
        ServerUtcTimeStr = Format(ServerUtcTimeVal, "YYYYMMDDhhmmss")
    End If
    
    If DataFilterVpfr <> "" Then DataWindowBeginUtc = DataFilterVpfr
    'We always start at full hours in Utc
    Mid(DataWindowBeginUtc, 11) = "0000"
    lblBgnTime(0).Tag = DataWindowBeginUtc
    
    CurUtcNowTime = CedaFullDateToVb(DataWindowBeginUtc)
    TimeScaleUtcBegin = CurUtcNowTime
    CurLocNowTime = DateAdd("n", UtcTimeDiff, CurUtcNowTime)
    DataWindowBeginLoc = Format(CurLocNowTime, "YYYYMMDDhhmmss")
    
    If chkUtc(0).Value = 1 Then
        DisplayDateTime = DataWindowBeginUtc
    Else
        DisplayDateTime = DataWindowBeginLoc
    End If
    
    lblBgnTime(0).Caption = DisplayDateTime
    
    iHour = Val(Mid(DisplayDateTime, 9, 2)) - 1
    CurDate = Left(DisplayDateTime, 8)
    CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
    lblBgnTime(0).Caption = CurSsimDate
    
    lblMin1(0).Width = 15
    lWidth = 60
    MinuteWidth = lWidth
    LeftOff1 = (lblHour(0).Width / 2) - 15
    LeftOff2 = (lblDate(0).Width / 2) - 15
    iMin5 = -1
    iMin10 = -1
    iMin30 = -1
    iMin60 = -1
    LeftPos = 1200
    For iMin = 0 To 3600
        If iMin > lblMin1.UBound Then
            Load lblMin1(iMin)
            Set lblMin1(iMin).Container = TimeScale
        End If
        lblMin1(iMin).Left = LeftPos + 15
        lblMin1(iMin).Visible = True
        If iMin Mod 5 = 0 Then
            iMin5 = iMin5 + 1
            If iMin5 > lblMin5.UBound Then
                Load lblMin5(iMin5)
                Set lblMin5(iMin5).Container = TimeScale
            End If
            lblMin5(iMin5).Left = LeftPos
            lblMin5(iMin5).Visible = True
        End If
        If iMin Mod 10 = 0 Then
            iMin10 = iMin10 + 1
            If iMin10 > lblMin10.UBound Then
                Load lblMin10(iMin10)
                Set lblMin10(iMin10).Container = TimeScale
            End If
            lblMin10(iMin10).Left = LeftPos
            lblMin10(iMin10).Visible = True
        End If
        If iMin Mod 30 = 0 Then
            iMin30 = iMin30 + 1
            If iMin30 > lblMin30.UBound Then
                Load lblMin30(iMin30)
                Set lblMin30(iMin30).Container = TimeScale
            End If
            lblMin30(iMin30).Left = LeftPos
            lblMin30(iMin30).Visible = True
        End If
        If iMin Mod 60 = 0 Then
            iMin60 = iMin60 + 1
            If iMin60 > lblMin60.UBound Then
                Load lblMin60(iMin60)
                Set lblMin60(iMin60).Container = TimeScale
                Load lblHour(iMin60)
                Set lblHour(iMin60).Container = TimeScale
                Load lblDate(iMin60)
                Set lblDate(iMin60).Container = TimeScale
            End If
            lblMin60(iMin60).Left = LeftPos
            lblMin60(iMin60).Visible = True
            lblHour(iMin60).Left = LeftPos - LeftOff1
            iHour = iHour + 1
            If iHour > 23 Then
                iHour = 0
                CurDate = CedaDateAdd(CurDate, 1)
                CurSsimDate = DecodeSsimDayFormat(CurDate, "CEDA", "SSIM2")
            End If
            lblHour(iMin60).Caption = Right("00" & CStr(iHour), 2) & ":00"
            lblHour(iMin60).ToolTipText = CurSsimDate
            lblHour(iMin60).Tag = CurDate
            lblHour(iMin60).Visible = True
            lblDate(iMin60).Caption = CurSsimDate
            lblDate(iMin60).Tag = CurDate
            lblDate(iMin60).Left = LeftPos - LeftOff2
            lblDate(iMin60).Visible = True
        End If
        LeftPos = LeftPos + lWidth
    Next
    CurChartWidth = LeftPos + 1200
    TimeScaleTimer.Enabled = True
End Sub

Public Sub CreateChartBarLayout(NeededBars As Integer)
    Dim iBar As Integer
    Dim iMin As Integer
    Dim iMax As Integer
    Dim iUse As Integer
    Dim lWidth As Long
    Dim LeftPos As Long
    Dim LeftOff As Long
    Dim TopPos As Long
    Dim LeftLblPos As Long
    Me.MousePointer = 11
    WorkArea(0).Visible = False
    iMin = ChartLine2.UBound
    If iMin > 0 Then
        TopPos = ChartLine2(iMin).Top + ChartLine2(0).Height + 60
        iMin = iMin + 1
    Else
        TopPos = ChartLine2(iMin).Top
    End If
    iMax = NeededBars - 1
    If iMax > MaxBarCnt Then iMax = MaxBarCnt
    If (iMax >= CurBarMax) Then
        For iBar = iMin To iMax
            If iBar > ChartLine2.UBound Then
                Load ChartLine1(iBar)
                Load ChartLine2(iBar)
                Load ChartLine3(iBar)
                Load lblArrBar(iBar)
                Load lblArrJob1(iBar)
                Load lblArrJob2(iBar)
                Load picArr1(iBar)
                Load picArr2(iBar)
                Load picArr3(iBar)
                Load picArr4(iBar)
                Load lblDepBar(iBar)
                Load lblDepJob1(iBar)
                Load lblDepJob2(iBar)
                Load picDep1(iBar)
                Load picDep2(iBar)
                Load picDep3(iBar)
                Load picDep4(iBar)
                Load lblTowBar(iBar)
                Load lblBarLineColor(iBar)
                Set lblArrBar(iBar).Container = ChartLine2(iBar)
                Set lblArrJob1(iBar).Container = ChartLine2(iBar)
                Set lblArrJob2(iBar).Container = ChartLine2(iBar)
                Set picArr1(iBar).Container = ChartLine2(iBar)
                Set picArr2(iBar).Container = ChartLine2(iBar)
                Set picArr3(iBar).Container = ChartLine2(iBar)
                Set picArr4(iBar).Container = ChartLine2(iBar)
                Set lblDepBar(iBar).Container = ChartLine2(iBar)
                Set lblDepJob1(iBar).Container = ChartLine2(iBar)
                Set lblDepJob2(iBar).Container = ChartLine2(iBar)
                Set picDep1(iBar).Container = ChartLine2(iBar)
                Set picDep2(iBar).Container = ChartLine2(iBar)
                Set picDep3(iBar).Container = ChartLine2(iBar)
                Set picDep4(iBar).Container = ChartLine2(iBar)
                Set lblTowBar(iBar).Container = ChartLine2(iBar)
                Set lblBarLineColor(iBar).Container = ChartLine2(iBar)
                
                Load ArrInfoPanel(iBar)
                Load ArrCnxSum1(iBar)
                Load ArrCnxSum2(iBar)
                Load ArrCnxSum3(iBar)
                Load ArrCnxSum4(iBar)
                Load ArrCnxCsct(iBar)
                Set ArrCnxSum1(iBar).Container = ArrInfoPanel(iBar)
                Set ArrCnxSum2(iBar).Container = ArrInfoPanel(iBar)
                Set ArrCnxSum3(iBar).Container = ArrInfoPanel(iBar)
                Set ArrCnxSum4(iBar).Container = ArrInfoPanel(iBar)
                Set ArrCnxCsct(iBar).Container = ArrInfoPanel(iBar)
                
                Load lblCnxPaxBar(iBar)
                Load lblCnxBagBar(iBar)
                Load lblCnxUldBar(iBar)
                Load picLeftBarCnx(iBar)
                Load picDepCnx(iBar)
                Load picRightBarCnx(iBar)
                
                Load DepInfoPanel(iBar)
                Load DepCnxSum1(iBar)
                Load DepCnxSum2(iBar)
                Load DepCnxSum3(iBar)
                Load DepCnxSum4(iBar)
                Load DepCnxCsct(iBar)
                Set DepCnxSum1(iBar).Container = DepInfoPanel(iBar)
                Set DepCnxSum2(iBar).Container = DepInfoPanel(iBar)
                Set DepCnxSum3(iBar).Container = DepInfoPanel(iBar)
                Set DepCnxSum4(iBar).Container = DepInfoPanel(iBar)
                Set DepCnxCsct(iBar).Container = DepInfoPanel(iBar)
                
                'Create Object
                Load ArrLblPanel(iBar)
                Load lblArrTpax(iBar)
                Load lblArrTbag(iBar)
                Load lblArrTuld(iBar)
                Set lblArrTpax(iBar).Container = ArrLblPanel(iBar)
                Set lblArrTbag(iBar).Container = ArrLblPanel(iBar)
                Set lblArrTuld(iBar).Container = ArrLblPanel(iBar)
                
                Set DepInfoPanel(iBar).Container = MidScConx(0)
            End If
            
            'LeftScale Panel
            Set ChartLine1(iBar).Container = LeftScale(0)
            ChartLine1(iBar).Top = TopPos
            ChartLine1(iBar).Left = 30
            CreateArrFlightPanel iBar
            Set ArrFlight(iBar).Container = ChartLine1(iBar)
            ArrFlight(iBar).Left = 30
            ChartLine1(iBar).Width = ArrFlight(iBar).Left + ArrFlight(iBar).Width + 90
            
            
            '===== MidScale Panel ================
            'Panel Totals PAX/BAG/ULD Arrival
            Set ArrLblPanel(iBar).Container = MidScConx(0)
            ArrLblPanel(iBar).Top = TopPos + 60
            ArrLblPanel(iBar).Left = 0
            ArrLblPanel(iBar).Visible = False
            'Graphical AnchorPoint (Arrival)
            Set picLeftBarCnx(iBar).Container = MidScConx(0)
            picLeftBarCnx(iBar).Top = ArrLblPanel(iBar).Top + (lblArrTpax(iBar).Height \ 2) - (picLeftBarCnx(iBar).Height \ 2)
            picLeftBarCnx(iBar).Left = 90
            'The PAX/BAG/ULD Colored Bars
            Set lblCnxPaxBar(iBar).Container = MidScConx(0)
            Set lblCnxBagBar(iBar).Container = MidScConx(0)
            Set lblCnxUldBar(iBar).Container = MidScConx(0)
            lblCnxPaxBar(iBar).Top = ArrLblPanel(iBar).Top + lblArrTpax(iBar).Top + (lblArrTpax(iBar).Height \ 2) - (lblCnxPaxBar(iBar).Height \ 2)
            lblCnxBagBar(iBar).Top = lblCnxPaxBar(iBar).Top + lblCnxPaxBar(iBar).Height - 15
            lblCnxUldBar(iBar).Top = lblCnxBagBar(iBar).Top + lblCnxBagBar(iBar).Height - 15
            lblCnxPaxBar(iBar).Left = 0
            lblCnxBagBar(iBar).Left = 0
            lblCnxUldBar(iBar).Left = 0
            lblCnxPaxBar(iBar).ZOrder
            lblCnxBagBar(iBar).ZOrder
            lblCnxUldBar(iBar).ZOrder
            
            'Panel Totals Per Class (1-4) per Connection Arrival
            Set ArrInfoPanel(iBar).Container = MidScConx(0)
            
            
            ArrInfoPanel(iBar).Top = TopPos + 60
            ArrInfoPanel(iBar).Left = 150
            
            DepInfoPanel(iBar).Top = TopPos + 60
            
            DepInfoPanel(iBar).Left = 0
            
            Set picDepCnx(iBar).Container = MidScConx(0)
            picDepCnx(iBar).Top = picLeftBarCnx(iBar).Top
            picDepCnx(iBar).Left = DepInfoPanel(iBar).Left + DepInfoPanel(iBar).Width
            
            Set picRightBarCnx(iBar).Container = MidScConx(0)
            picRightBarCnx(iBar).Top = picLeftBarCnx(iBar).Top
            picRightBarCnx(iBar).Left = 100
            
            lblDepBar(iBar).ZOrder
                    
            
            
            
            ArrFlight(iBar).Visible = True
            
            
            
            'RightScale Panel
            Set ChartLine3(iBar).Container = RightScale(0)
            ChartLine3(iBar).Top = TopPos
            ChartLine3(iBar).Left = 30
            CreateDepFlightPanel iBar
            Set DepFlight(iBar).Container = ChartLine3(iBar)
            DepFlight(iBar).Left = 30
            
            'Gantt Chart Panel
            Set ChartLine2(iBar).Container = HScrollArea(0)
            ChartLine2(iBar).Top = TopPos
            
            
            Set DepLblPanel(iBar).Container = MidScConx(0)
            DepLblPanel(iBar).Top = ArrLblPanel(iBar).Top
            DepLblPanel(iBar).Left = 0
            ChartLine3(iBar).Width = DepFlight(iBar).Left + DepFlight(iBar).Width + 90
            DepLblPanel(iBar).Visible = False
            DepLblPanel(iBar).ZOrder
            picDepCnx(iBar).ZOrder
            DepFlight(iBar).Visible = True
            
            
            ChartLine1(iBar).Visible = True
            ChartLine2(iBar).Visible = True
            ChartLine3(iBar).Visible = True
            
            ArrCnxSum1(iBar).Visible = True
            ArrCnxSum2(iBar).Visible = True
            ArrCnxSum3(iBar).Visible = True
            ArrCnxSum4(iBar).Visible = True
            ArrCnxCsct(iBar).Visible = True
            ArrInfoPanel(iBar).Visible = False
            
            DepCnxSum1(iBar).Visible = True
            DepCnxSum2(iBar).Visible = True
            DepCnxSum3(iBar).Visible = True
            DepCnxSum4(iBar).Visible = True
            DepCnxCsct(iBar).Visible = True
            DepInfoPanel(iBar).Visible = False
            
            'lblArrFlno(iBar).Caption = CStr(iBar)
            
            TopPos = TopPos + ChartLine2(0).Height + 30
        Next
        CurBarMax = iMax
    Else
        iMin = NeededBars
        iMax = CurBarMax
        CurBarMax = iMin - 1
    End If
    TopPos = ChartLine2(CurBarMax).Top + ChartLine2(0).Height
    CurChartHeight = TopPos + 30
    VScrollArea(0).Height = CurChartHeight
    HScrollArea(0).Height = CurChartHeight
    LeftScale(0).Height = CurChartHeight
    RightScale(0).Height = CurChartHeight
    TimeLine2(0).Height = CurChartHeight
    TimeLine2(0).ZOrder
    RangeLine(0).Height = CurChartHeight
    RangeLine(0).ZOrder
    RangeLine(1).Height = CurChartHeight
    RangeLine(1).ZOrder
    TopPos = VScrollArea(0).Height
    VScroll1(0).Max = CurBarMax
    VScroll2(0).Max = CurBarMax
    WorkArea(0).Visible = True
    Me.MousePointer = 0
End Sub
Private Sub CreateArrFlightPanel(iBar As Integer)
    If iBar > ArrFlight.UBound Then
        Load ArrFlight(iBar)
        'Load lblArrFtyp(iBar)
        Load lblArrFlno(iBar)
        Load lblArrFlti(iBar)
        Load lblArrOrg3(iBar)
        Load lblArrVia3(iBar)
        Load lblArrStoa(iBar)
        Load lblArrEtai(iBar)
        Load lblArrOnbl(iBar)
        Load lblArrPsta(iBar)
        'Load lblArrGta1(iBar)
        Load lblArrRegn(iBar)
        Load lblArrAct3(iBar)
        Load lblArrTot1(iBar)
        Load lblArrTot2(iBar)
        Load lblArrTot3(iBar)
        Load lblArrTot4(iBar)
        Load lblArrTotPax(iBar)
        Load lblArrTotBag(iBar)
        Load lblArrTotUld(iBar)
        Load lblArrCsct(iBar)
        Load picArrStatus(iBar)
        'Set lblArrFtyp(iBar).Container = ArrFlight(iBar)
        Set lblArrFlno(iBar).Container = ArrFlight(iBar)
        Set lblArrFlti(iBar).Container = ArrFlight(iBar)
        Set lblArrOrg3(iBar).Container = ArrFlight(iBar)
        Set lblArrVia3(iBar).Container = ArrFlight(iBar)
        Set lblArrStoa(iBar).Container = ArrFlight(iBar)
        Set lblArrEtai(iBar).Container = ArrFlight(iBar)
        Set lblArrOnbl(iBar).Container = ArrFlight(iBar)
        Set lblArrPsta(iBar).Container = ArrFlight(iBar)
        'Set lblArrGta1(iBar).Container = ArrFlight(iBar)
        Set lblArrRegn(iBar).Container = ArrFlight(iBar)
        Set lblArrAct3(iBar).Container = ArrFlight(iBar)
        Set lblArrTot1(iBar).Container = ArrFlight(iBar)
        Set lblArrTot2(iBar).Container = ArrFlight(iBar)
        Set lblArrTot3(iBar).Container = ArrFlight(iBar)
        Set lblArrTot4(iBar).Container = ArrFlight(iBar)
        Set lblArrTotPax(iBar).Container = ArrFlight(iBar)
        Set lblArrTotBag(iBar).Container = ArrFlight(iBar)
        Set lblArrTotUld(iBar).Container = ArrFlight(iBar)
        Set lblArrCsct(iBar).Container = ArrFlight(iBar)
        Set picArrStatus(iBar).Container = ArrFlight(iBar)
    End If
   
    ArrFlight(iBar).Top = 30
    'ArrFlight(iBar).Height = 330
    
    'lblArrFtyp(iBar).Visible = True
    lblArrFlno(iBar).Visible = True
    lblArrFlti(iBar).Visible = True
    lblArrOrg3(iBar).Visible = True
    lblArrVia3(iBar).Visible = True
    lblArrStoa(iBar).Visible = True
    lblArrEtai(iBar).Visible = True
    lblArrOnbl(iBar).Visible = True
    lblArrPsta(iBar).Visible = True
    'lblArrGta1(iBar).Visible = True
    lblArrRegn(iBar).Visible = True
    lblArrAct3(iBar).Visible = True
    lblArrTot1(iBar).Visible = True
    lblArrTot2(iBar).Visible = True
    lblArrTot3(iBar).Visible = True
    lblArrTot4(iBar).Visible = True
    lblArrTotPax(iBar).Visible = True
    lblArrTotBag(iBar).Visible = True
    lblArrTotUld(iBar).Visible = True
    lblArrCsct(iBar).Visible = True
    picArrStatus(iBar).Visible = True
    
    lblArrTpax(iBar).Visible = True
    lblArrTbag(iBar).Visible = True
    lblArrTuld(iBar).Visible = True
End Sub

Private Sub CreateDepFlightPanel(iBar As Integer)
    If iBar > DepFlight.UBound Then
        Load DepFlight(iBar)
        'Load picDepStat(iBar)
        'Load lblDepFtyp(iBar)
        Load lblDepFlno(iBar)
        Load lblDepFlti(iBar)
        Load lblDepDes3(iBar)
        Load lblDepVia3(iBar)
        Load lblDepStod(iBar)
        Load lblDepEtdi(iBar)
        Load lblDepOfbl(iBar)
        Load lblDepPstd(iBar)
        'Load lblDepGtd1(iBar)
        Load lblDepRegn(iBar)
        Load lblDepAct3(iBar)
        Load lblDepTot1(iBar)
        Load lblDepTot2(iBar)
        Load lblDepTot3(iBar)
        Load lblDepTot4(iBar)
        Load lblDepTotPax(iBar)
        Load lblDepTotBag(iBar)
        Load lblDepTotUld(iBar)
        Load lblDepCsct(iBar)
        Load picDepStatus(iBar)
        'Set picDepStat(iBar).Container = DepFlight(iBar)
        'Set lblDepFtyp(iBar).Container = DepFlight(iBar)
        Set lblDepFlno(iBar).Container = DepFlight(iBar)
        Set lblDepFlti(iBar).Container = DepFlight(iBar)
        Set lblDepDes3(iBar).Container = DepFlight(iBar)
        Set lblDepVia3(iBar).Container = DepFlight(iBar)
        Set lblDepStod(iBar).Container = DepFlight(iBar)
        Set lblDepEtdi(iBar).Container = DepFlight(iBar)
        Set lblDepOfbl(iBar).Container = DepFlight(iBar)
        Set lblDepPstd(iBar).Container = DepFlight(iBar)
        'Set lblDepGtd1(iBar).Container = DepFlight(iBar)
        Set lblDepRegn(iBar).Container = DepFlight(iBar)
        Set lblDepAct3(iBar).Container = DepFlight(iBar)
        Set lblDepTot1(iBar).Container = DepFlight(iBar)
        Set lblDepTot2(iBar).Container = DepFlight(iBar)
        Set lblDepTot3(iBar).Container = DepFlight(iBar)
        Set lblDepTot4(iBar).Container = DepFlight(iBar)
        Set lblDepTotPax(iBar).Container = DepFlight(iBar)
        Set lblDepTotBag(iBar).Container = DepFlight(iBar)
        Set lblDepTotUld(iBar).Container = DepFlight(iBar)
        Set lblDepCsct(iBar).Container = DepFlight(iBar)
        Set picDepStatus(iBar).Container = DepFlight(iBar)
        
        Load DepLblPanel(iBar)
        Load lblDepTpax(iBar)
        Load lblDepTbag(iBar)
        Load lblDepTuld(iBar)
    End If
    DepFlight(iBar).Top = 30
    DepLblPanel(iBar).Top = 30
    
    Set lblDepTpax(iBar).Container = DepLblPanel(iBar)
    Set lblDepTbag(iBar).Container = DepLblPanel(iBar)
    Set lblDepTuld(iBar).Container = DepLblPanel(iBar)
    
    'DepFlight(iBar).Height = 330
    'picDepStat(iBar).Visible = True
    'lblDepFtyp(iBar).Visible = True
    lblDepFlno(iBar).Visible = True
    lblDepFlti(iBar).Visible = True
    lblDepDes3(iBar).Visible = True
    lblDepVia3(iBar).Visible = True
    lblDepStod(iBar).Visible = True
    lblDepEtdi(iBar).Visible = True
    lblDepOfbl(iBar).Visible = True
    lblDepPstd(iBar).Visible = True
    'lblDepGtd1(iBar).Visible = True
    lblDepRegn(iBar).Visible = True
    lblDepAct3(iBar).Visible = True
    lblDepTotPax(iBar).Visible = True
    lblDepTotBag(iBar).Visible = True
    lblDepTotUld(iBar).Visible = True
    lblDepTot1(iBar).Visible = True
    lblDepTot2(iBar).Visible = True
    lblDepTot3(iBar).Visible = True
    lblDepTot4(iBar).Visible = True
    lblDepCsct(iBar).Visible = True
    picDepStatus(iBar).Visible = True

    lblDepTpax(iBar).Visible = True
    lblDepTbag(iBar).Visible = True
    lblDepTuld(iBar).Visible = True

End Sub

Private Sub chkWork_Click(Index As Integer)

    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        chkWork(Index).Refresh
        Select Case Index
            Case 0
                ShowBarsOverlapped True
            Case 1
                TopPanel.Visible = True
                Form_Resize
            Case 2 'Reload
                MainDialog.chkWork(1).Value = 1
                MainDialog.chkWork_Click 1
                chkWork(Index).Value = 0
                MainDialog.Hide
            Case 3 'Filter
                MainDialog.PushWorkButton "AODB_FILTER", 1
                chkWork(Index).Value = 0
                MainDialog.Hide
            Case 4 'Refresh filter
                MainDialog.chkWork(2).Value = 1
                MainDialog.chkWork_Click 2
                chkWork(Index).Value = 0
                MainDialog.Hide
            Case 5
                frmLoad.CallLoadData
                frmConnectionTimes.Show (vbModal)
                chkWork(5).Value = 0
                chkWork(4).Value = 1
                chkWork_Click 1
                
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                ShowBarsOverlapped False
            Case 1
                TopPanel.Visible = False
                Form_Resize
            Case Else
        End Select
        chkWork(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ShowBarsOverlapped(ShowOverlap As Boolean)
    Dim ArrLeft As Long
    Dim DepLeft As Long
    Dim iBar As Integer
    Screen.MousePointer = 11
    'For iBar = 0 To ChartLine2.UBound
        'If (lblArrFtyp(iBar).Tag <> "") And (lblDepFtyp(iBar).Tag <> "") Then
        '    If ShowOverlap Then
        '        ArrFlight(iBar).Top = -30
        '        lblArrBar(iBar).Top = 0
        '        DepFlight(iBar).Top = 60
        '        lblDepBar(iBar).Top = 90
        '    Else
        '        ArrFlight(iBar).Top = 15
        '        lblArrBar(iBar).Top = 45
        '        DepFlight(iBar).Top = 15
        '        lblDepBar(iBar).Top = 45
        '    End If
        '    'End If
        'End If
    'Next
    Screen.MousePointer = 0
End Sub


Private Sub CnxFilterPanel_Click()
    MidScConx(0).ZOrder
End Sub

Private Sub CnxFilterPanel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub CnxRefresh_Timer()
    'QuickHack
    'RefreshCurrentCnxChart
    RefreshCurrentCnxChart
    CnxRefresh.Enabled = False
End Sub

Private Sub DepFlight_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub DepFlight_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Public Sub Form_Activate()
    Static IsActivated As Boolean
    Dim tmpWidth As Long
    Dim tmpHeight As Long
    Dim tmpSize As Long
    If IsActivated = False Then
        If MainLifeStyle Then
            If sSHOW_FILTER_BUTTON = "YES" Then
                chkWork(2).Visible = True
                chkWork(3).Visible = True
                chkWork(4).Visible = True
                chkWork(5).Visible = True
                chkWork(0).Left = 3090
                chkWork(1).Left = 4140
            Else
                chkWork(0).Left = 0
                chkWork(1).Left = 1050
                chkWork(2).Visible = False
                chkWork(3).Visible = False
                chkWork(4).Visible = False
                chkWork(5).Visible = False
            End If
            tmpWidth = MaxScreenWidth + 2100
            tmpHeight = Screen.Height + 2100
            ButtonPanel.Width = tmpWidth
            tmpSize = ButtonPanel.Height
            ButtonPanel.Height = tmpSize * 2
            DrawBackGround ButtonPanel, WorkAreaColor, True, True
            ButtonPanel.Height = tmpSize
            
            RightPanel.Height = tmpHeight
            DrawBackGround RightPanel, WorkAreaColor, True, True
            
            tmpSize = TimeScale(0).Height
            TimeScale(0).Height = tmpSize * 6
            DrawBackGround TimeScale(0), MainAreaColor, True, True
            TimeScale(0).Height = tmpSize
            
            TopRemark(0).Width = tmpWidth
            tmpSize = TopRemark(0).Height
            TopRemark(0).Height = tmpSize * 2
            DrawBackGround TopRemark(0), MainAreaColor, True, True
            TopRemark(0).Height = tmpSize
            
            BottomRemark(0).Width = tmpWidth
            tmpSize = BottomRemark(0).Height
            BottomRemark(0).Height = tmpSize * 2
            DrawBackGround BottomRemark(0), MainAreaColor, True, True
            BottomRemark(0).Height = tmpSize
            
            WorkArea(0).Height = tmpHeight
            WorkArea(0).Width = tmpWidth
            DrawBackGround WorkArea(0), MainAreaColor, True, True
            'DrawBackGround TopPanel, WorkAreaColor, True, True
        Else
            'lblArrRow.ForeColor = vbButtonText
            'lblDepRow.ForeColor = vbButtonText
        End If
        'Me.Refresh
        Me.Top = 0
        Me.Height = Screen.Height '- 300
        Form_Resize
        Me.Refresh
        IsActivated = True
        chkSelDeco(2).Value = 1
    End If
End Sub
Private Sub ShowAllArrConnex()
    Dim iBar As Integer
    Dim tmpData As String
    DontRefreshCnx = True
    chkLayout(0).Value = 1
    'DrawBackGround MidScConx(0), 7, False, True
    
    For iBar = 0 To CurBarMax
        picLeftBarCnx(iBar).MousePointer = 0
        tmpData = lblArrCsct(iBar).Tag
        'Contains "CNX" of flights with connections
        If tmpData <> "" Then
            CurCnxBarIdx = -1
            CurCnxView = 0
            ShowConnexDetails iBar
            picLeftBarCnx(iBar).MousePointer = 99
        End If
    Next
    CurCnxBarIdx = -1
    LastCnxArrBar = -1
    CurCnxView = 0
    chkLayout(CurLayoutType).Value = 1
    DontRefreshCnx = False
End Sub
Private Sub ShowAllDepConnex()
    Dim iBar As Integer
    Dim tmpData As String
    DontRefreshCnx = True
    chkLayout(0).Value = 1
    'DrawBackGround MidScConx(0), 7, False, True
    
    For iBar = 0 To CurBarMax
        picLeftBarCnx(iBar).MousePointer = 0
        tmpData = lblArrCsct(iBar).Tag
        'Contains "CNX" of flights with connections
        If tmpData <> "" Then
            CurCnxBarIdx = -1
            CurCnxView = 0
            ShowDepConnexDetails iBar
            picLeftBarCnx(iBar).MousePointer = 99
        End If
    Next
    CurCnxBarIdx = -1
    LastCnxDepBar = -1
    CurCnxView = 0
    chkLayout(CurLayoutType).Value = 1
    DontRefreshCnx = False
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim ScrollIt As Boolean
    Dim NxtIdx As Integer
    Dim TopIdx As Integer
    Dim BotIdx As Integer
    Dim LinCnt As Integer
    Dim NewVal As Integer
    If Shift = 1 Then ScrollIt = True Else ScrollIt = False
    TopIdx = VScroll2(0).Value
    LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height + 30))
    BotIdx = TopIdx + LinCnt - 1
    Select Case KeyCode
        Case vbKeyDown
            NxtIdx = CurBarIdx + 1
            If NxtIdx > VScroll2(0).Max Then NxtIdx = VScroll2(0).Max
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                VScroll2(0).Value = VScroll2(0).Value + 1
            End If
            KeyCode = 0
        Case vbKeyUp
            NxtIdx = CurBarIdx - 1
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                VScroll2(0).Value = VScroll2(0).Value - 1
            End If
            KeyCode = 0
        Case vbKeyPageDown
            NxtIdx = CurBarIdx + LinCnt
            If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
            If NxtIdx > VScroll2(0).Max Then NxtIdx = VScroll2(0).Max
            If (ScrollIt = False) And (NxtIdx <= CurBarMax) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx > BotIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value < VScroll2(0).Max) Then
                NxtIdx = VScroll2(0).Value + LinCnt
                If NxtIdx > CurBarMax Then NxtIdx = CurBarMax
                If NxtIdx > VScroll2(0).Max Then NxtIdx = VScroll2(0).Max
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyPageUp
            NxtIdx = CurBarIdx - LinCnt
            If NxtIdx < 0 Then NxtIdx = 0
            If (ScrollIt = False) And (NxtIdx >= 0) Then
                HighlightCurrentBar NxtIdx, ChartName
                If CurBarIdx < TopIdx Then ScrollIt = True
            End If
            If (ScrollIt = True) And (VScroll2(0).Value > 0) Then
                NxtIdx = VScroll2(0).Value - LinCnt
                If NxtIdx < 0 Then NxtIdx = 0
                VScroll2(0).Value = NxtIdx
            End If
            KeyCode = 0
        Case vbKeyLeft
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value - 5
                Case 1
                    NewVal = HScroll2(0).Value - 1
                Case 2
                    NewVal = HScroll2(0).Value - 20
                Case Else
                    NewVal = HScroll2(0).Value - 5
            End Select
            If NewVal < 0 Then NewVal = 0
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case vbKeyRight
            Select Case Shift
                Case 0
                    NewVal = HScroll2(0).Value + 5
                Case 1
                    NewVal = HScroll2(0).Value + 1
                Case 2
                    NewVal = HScroll2(0).Value + 20
                Case Else
                    NewVal = HScroll2(0).Value + 5
            End Select
            If NewVal > HScroll2(0).Max Then NewVal = HScroll2(0).Max
            HScroll2(0).Value = NewVal
            KeyCode = 0
        Case Else
    End Select
End Sub

Private Sub Form_Load()
    ChartName = "CNX"
    CnxLayoutType = -1
    CurLayoutType = 4
    MainAreaColor = Val(GetIniEntry(myIniFullName, "MAIN", "", "MAIN_AREA_COLOR", "7"))
    WorkAreaColor = MainAreaColor
    ChartBarBackColor = LightGray
    lblBarLineColor(0).BackColor = ChartBarBackColor
    lblBarLineColor(0).Tag = "7"
    MinimumTop = 30
    MinimumLeft = 30
    CurChartWidth = 0
    CurChartHeight = 0
    CurCnxBarIdx = -1
    LastCnxArrBar = -1
    LastCnxDepBar = -1
    Me.Top = Screen.Height + 1000
    Me.Left = 0
    Me.Width = MaxScreenWidth
    Me.Height = Screen.Height
    
    chkUtc(1).Value = 1
    
    'We have a restriction of a maximum height of a picture box (Panel)
    'The max height is 245745 twips
    'So the maximum number of bars on the chart is:
    MaxBarCnt = CInt(245745 \ (ChartLine2(0).Height + 30)) - 1
    'Maximum is 302, but together with the StatusChart
    'we run out of memory so we set it to 100
    MaxBarCnt = 100
    'MaxBarCnt = 300
    CurBarMax = 0
    CurBarIdx = -1
    InitDetailTabs
    InitChartArea
End Sub

Private Sub InitDetailTabs()
    InitTabArrCnxDetails
    InitTabDepCnxDetails
End Sub
Private Sub InitTabArrCnxDetails()
    Dim FieldList As String
    Dim LengthList As String
    'This tab grid assembles all available data of PAX/BAG/ULD per Connex Line.
    'The values are fetched from the grids of the main dialog and merged together.
    'Each record represents one single connection line in the chart.
    'The "Line" is represented by a unique key "UKEY" that consists of AURN&DURN.
    'The access to the values can be done using UKEY for a dedicated (known) connection
    'or by running in a loop fetching records of AURN (or DURN).
    'Note: The data view is CFITAB RECORDS OF ARRIVAL!
    'This means: All arrival flight data and values are complete even though some
    'departure flights might not be loaded in the related grids of the main dialog.
    FieldList = "UKEY,AURN,DURN,PAXT,PAX1,PAX2,PAX3,PAX4,BAGT,BAG1,BAG2,BAG3,BAG4,ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,TIFA,ADDI"
    LengthList = "20 ,10  ,10  ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,1   ,5   ,14  ,20  "
    TabArrCnxDetails.ResetContent
    TabArrCnxDetails.LogicalFieldList = FieldList
    TabArrCnxDetails.HeaderString = FieldList
    TabArrCnxDetails.HeaderLengthString = LengthList
    TabArrCnxDetails.ShowHorzScroller False
    TabArrCnxDetails.AutoSizeByHeader = True
    TabArrCnxDetails.AutoSizeColumns
End Sub
Private Sub InitTabDepCnxDetails()
    Dim FieldList As String
    Dim LengthList As String
    'This tab grid assembles all available data of PAX/BAG/ULD per Connex Line.
    'The values are fetched from the grids of the main dialog and merged together.
    'Each record represents one single connection line in the chart.
    'The "Line" is represented by a unique key "UKEY" that consists of AURN&DURN.
    'The access to the values can be done using UKEY for a dedicated (known) connection
    'or by running in a loop fetching records of AURN (or DURN).
    'Note: The data view is CFITAB RECORDS OF DEPARTURE!
    'This means: All departure flight data and values are complete even though some
    'arrival flights might not be loaded in the related grids of the main dialog.
    FieldList = "UKEY,AURN,DURN,PAXT,PAX1,PAX2,PAX3,PAX4,BAGT,BAG1,BAG2,BAG3,BAG4,ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,TIFD,ADDI"
    LengthList = "20 ,10  ,10  ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,5   ,1   ,5   ,14  ,20  "
    TabDepCnxDetails.ResetContent
    TabDepCnxDetails.LogicalFieldList = FieldList
    TabDepCnxDetails.HeaderString = FieldList
    TabDepCnxDetails.HeaderLengthString = LengthList
    TabDepCnxDetails.ShowHorzScroller False
    TabDepCnxDetails.AutoSizeByHeader = True
    TabDepCnxDetails.AutoSizeColumns
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    chkAppl(0).Value = 1
    Cancel = True
End Sub

Private Sub Form_Resize()
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim TimePanelSize As Long
    Dim TimePanelLeft As Long
    
    VScroll1(0).Visible = False
    VScroll2(0).Visible = False
    HScroll2(0).Visible = False
    
    NewTop = MinimumTop
    ButtonPanel.Top = NewTop
    RightPanel.Top = NewTop
    NewLeft = MinimumLeft
    ButtonPanel.Left = NewLeft
    TopPanel.Left = NewLeft
    LeftPanel(0).Left = NewLeft
    
    NewWidth = Me.ScaleWidth
    NewLeft = NewWidth
    If RightPanel.Visible = True Then
        NewLeft = NewWidth - RightPanel.Width - 15
    End If
    RightPanel.Left = NewLeft
    NewWidth = NewLeft - MinimumLeft - 15
    If NewWidth > 300 Then
        ButtonPanel.Width = NewWidth
        TopPanel.Width = NewWidth
    End If
    NewLeft = RightPanel.Left - VertScroll2(0).Width - 15
    VertScroll2(0).Left = NewLeft
    HorizScroll3(0).Left = NewLeft
    TimeScroll2(0).Left = NewLeft
    
    NewHeight = Me.ScaleHeight - StatusBar.Height - MinimumTop
    If NewHeight > 300 Then
        RightPanel.Height = NewHeight
    End If
    
    NewTop = MinimumTop
    NewTop = NewTop + ButtonPanel.Height + 15
    If TopPanel.Visible Then
        TopPanel.Top = NewTop
        NewTop = NewTop + TopPanel.Height + 15
    End If
    LeftPanel(0).Top = NewTop
    TopRemark(0).Top = NewTop
    TimeScroll1(0).Top = NewTop
    TimeScroll2(0).Top = NewTop
    
    NewHeight = NewHeight - NewTop + MinimumTop
    If NewHeight > 300 Then
        LeftPanel(0).Height = NewHeight
    End If
    
    NewTop = NewTop + TopRemark(0).Height '+ 15
    TimePanel(0).Top = NewTop
    
    NewTop = NewTop + TimePanel(0).Height + 15
    VertScroll1(0).Top = NewTop
    WorkArea(0).Top = NewTop
    VertScroll2(0).Top = NewTop
    
    NewLeft = MinimumLeft
    If LeftPanel(0).Visible Then
        NewLeft = NewLeft + LeftPanel(0).Width + 15
    End If
    TimeScroll1(0).Left = NewLeft
    TopRemark(0).Left = NewLeft
    
    NewWidth = RightPanel.Left - NewLeft - 15
    If NewWidth > 300 Then
        'TopRemark(0).Width = NewWidth
        'BottomRemark(0).Width = NewWidth
    End If
    
    If VertScroll1(0).Visible Then
        VertScroll1(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewLeft = NewLeft + VertScroll1(0).Width + 15
        WorkArea(0).Left = NewLeft
        HorizScroll2(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    Else
        WorkArea(0).Left = NewLeft
        HorizScroll1(0).Left = NewLeft
        NewWidth = RightPanel.Left - NewLeft - 15
        If VertScroll2(0).Visible = True Then NewWidth = NewWidth - VertScroll2(0).Width - 15
        If NewWidth > 300 Then
            WorkArea(0).Width = NewWidth
            NewLeft = NewLeft + HorizScroll1(0).Width + 15
            HorizScroll2(0).Left = NewLeft
            NewWidth = NewWidth - HorizScroll1(0).Width - 15
            If VertScroll2(0).Visible = False Then NewWidth = NewWidth - HorizScroll3(0).Width - 15
            If NewWidth > 300 Then
                HorizScroll2(0).Width = NewWidth
                HScroll2(0).Width = NewWidth - 60
            End If
        End If
    End If
    
    
    TimePanel(0).Left = WorkArea(0).Left
    TimePanel(0).Width = WorkArea(0).Width
    TopRemark(0).Left = WorkArea(0).Left
    TopRemark(0).Width = WorkArea(0).Width
    
    BottomRemark(0).Left = HorizScroll2(0).Left
    BottomRemark(0).Width = HorizScroll2(0).Width
    
    NewTop = Me.ScaleHeight - StatusBar.Height - BottomRemark(0).Height
    BottomRemark(0).Top = NewTop - 30
    NewTop = NewTop - BottomRemark(0).Height - 15
    HorizScroll1(0).Top = NewTop
    HorizScroll2(0).Top = NewTop
    HorizScroll3(0).Top = NewTop
    NewHeight = NewTop - WorkArea(0).Top - 15
    If NewHeight > 300 Then
        VertScroll1(0).Height = NewHeight
        VScroll1(0).Height = NewHeight - 60
        WorkArea(0).Height = NewHeight
        VertScroll2(0).Height = NewHeight
        VScroll2(0).Height = NewHeight - 60
    End If
    
    ArrangeChartArea
    
    VScroll1(0).Visible = True
    VScroll1(0).Refresh
    VScroll2(0).Visible = True
    VScroll2(0).Refresh
    HScroll2(0).Visible = True
    HScroll2(0).Refresh
  
    WorkArea(0).Refresh
    
    Me.Refresh
End Sub

Private Sub InitChartArea()
    Dim NewColor As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Screen.MousePointer = 11
    
    NewColor = ChartBarBackColor
    ChartLine1(0).BackColor = NewColor
    'ArrInfoPanel(0).BackColor = NewColor
    ChartLine2(0).BackColor = NewColor
    ChartLine3(0).BackColor = NewColor
    'DepInfoPanel(0).BackColor = NewColor
    ArrFlight(0).BackColor = NewColor
    DepFlight(0).BackColor = NewColor
    LeftScale(0).BackColor = NewColor
    RightScale(0).BackColor = NewColor
    MidScConx(0).BackColor = NewColor
    'MidScCon1(0).BackColor = NewColor
    LeftScCon1(0).BackColor = vbButtonFace
    RightScCon1(0).BackColor = vbButtonFace
    
    lblArrTpax(0).Left = -15
    lblArrTbag(0).Left = -15
    lblArrTuld(0).Left = -15
    lblArrTpax(0).Height = 225
    lblArrTbag(0).Height = 240
    lblArrTuld(0).Height = 225
    lblArrTpax(0).Width = 465
    lblArrTbag(0).Width = 465
    lblArrTuld(0).Width = 465
    lblArrTpax(0).Top = -15
    lblArrTbag(0).Top = lblArrTpax(0).Top + lblArrTpax(0).Height - 15
    lblArrTuld(0).Top = lblArrTbag(0).Top + lblArrTbag(0).Height - 15
    lblArrTpax(0).BackColor = LightGreen
    lblArrTbag(0).BackColor = LightYellow
    lblArrTuld(0).BackColor = LightBlue
    lblArrTpax(0).ZOrder
    lblArrTuld(0).ZOrder
    
    lblDepTpax(0).Left = -15
    lblDepTbag(0).Left = -15
    lblDepTuld(0).Left = -15
    lblDepTpax(0).Height = 225
    lblDepTbag(0).Height = 240
    lblDepTuld(0).Height = 225
    lblDepTpax(0).Width = 465
    lblDepTbag(0).Width = 465
    lblDepTuld(0).Width = 465
    lblDepTpax(0).Top = -15
    lblDepTbag(0).Top = lblDepTpax(0).Top + lblDepTpax(0).Height - 15
    lblDepTuld(0).Top = lblDepTbag(0).Top + lblDepTbag(0).Height - 15
    lblDepTpax(0).BackColor = LightGreen
    lblDepTbag(0).BackColor = LightYellow
    lblDepTuld(0).BackColor = LightBlue
    lblDepTpax(0).ZOrder
    lblDepTuld(0).ZOrder
    
    lblBgnTime(0).BackColor = LightGray
    
    NewLeft = -30
    VScrollArea(0).Left = NewLeft
    HScrollArea(0).Left = NewLeft
    LeftScale(0).Left = NewLeft
    
    NewLeft = 30
    ChartLine1(0).Left = NewLeft
    ChartLine3(0).Left = NewLeft
    
    NewLeft = -30
    ChartLine2(0).Left = NewLeft
    
    NewTop = -30
    VScrollArea(0).Top = NewTop
    HScrollArea(0).Top = NewTop
    LeftScale(0).Top = NewTop
    RightScale(0).Top = NewTop
    MidScConx(0).Top = NewTop
    
    NewTop = 30
    ChartLine1(0).Top = NewTop
    ChartLine2(0).Top = NewTop
    ChartLine3(0).Top = NewTop
    
    NewTop = -30
    TimePanel(0).Height = TimeScroll1(0).Height - 300
    TimeScale(0).Top = NewTop
    TimeScale(0).Height = TimePanel(0).Height + 60
    
    CreateTimeScaleArea TimeScale(0)
    HScrollArea(0).Width = CurChartWidth
    VScrollArea(0).Width = CurChartWidth
    ChartLine2(0).Width = CurChartWidth
    
    ChartLine3(0).Width = ArrFlight(0).Width + 90
    ChartLine3(0).Width = DepFlight(0).Width + 90
    
    Screen.MousePointer = 11
    CreateChartBarLayout 680
    LeftScale(0).Width = ChartLine1(0).Width + 120
    RightScale(0).Width = ChartLine3(0).Width + 120
    MidScConx(0).Height = CurChartHeight
    
    Set MidScCon1(0).Container = TimePanel(0)
    MidScCon1(0).Top = -30
    MidScCon1(0).Left = LeftScale(0).Left
    MidScCon1(0).Height = TimePanel(0).Height + 60
    MidScCon1(0).Width = LeftScale(0).Width
    Set LeftScCon1(0).Container = TimePanel(0)
    LeftScCon1(0).Top = -30
    LeftScCon1(0).Left = LeftScale(0).Left
    LeftScCon1(0).Height = TimePanel(0).Height + 60
    LeftScCon1(0).Width = LeftScale(0).Width
    Set RightScCon1(0).Container = TimePanel(0)
    RightScCon1(0).Top = -30
    RightScCon1(0).Height = TimePanel(0).Height + 60
    RightScCon1(0).Left = RightScale(0).Left
    RightScCon1(0).Width = RightScale(0).Width
    
    TabDepInfo(0).Top = 0
    TabDepInfo(0).Left = 0
    TabDepInfo(0).Height = RightScCon1(0).Height - 120
    TabDepInfo(0).Width = RightScCon1(0).Width - 60
    InitDepInfoTab
    
    Screen.MousePointer = 11
    
    TimeLine2(0).ZOrder
    lblCurTime(0).ZOrder
    LeftScale(0).ZOrder
    RightScale(0).ZOrder
    LeftPanel(0).ZOrder
    ButtonPanel.ZOrder
    TopPanel.ZOrder
    TimePanel(0).ZOrder
    TopRemark(0).ZOrder
    WorkArea(0).ZOrder
    BottomRemark(0).ZOrder
    RightPanel.ZOrder
    StatusBar.ZOrder
    Screen.MousePointer = 0
    
End Sub

Private Sub InitDepInfoTab()
    Dim DepInfoFields As String
    TabDepInfo(0).ResetContent
    If MainLifeStyle Then
        TabDepInfo(0).LifeStyle = True
        TabDepInfo(0).CursorLifeStyle = True
    End If
    DepInfoFields = "AFSD,FLND,STOD,DES3,PSTD,SCID,TPAX,TBAG,TULD,REMA,DURN"
    TabDepInfo(0).LogicalFieldList = DepInfoFields
    TabDepInfo(0).HeaderString = "S,Flight,STD,Dest,Bay,C,PAX,BAG,ULD,Remark                               .,DURN"
    TabDepInfo(0).HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    TabDepInfo(0).ColumnAlignmentString = "C,L,C,C,L,C,R,R,R,L,R"
    TabDepInfo(0).HeaderAlignmentString = "C,L,C,C,L,C,R,R,R,L,R"
    TabDepInfo(0).FontName = "Arial"
    TabDepInfo(0).FontSize = 9 + 6
    TabDepInfo(0).HeaderFontSize = 9 + 6
    TabDepInfo(0).LineHeight = 9 + 8
    TabDepInfo(0).LeftTextOffset = 1
    TabDepInfo(0).SetTabFontBold True
    TabDepInfo(0).MainHeader = False
    TabDepInfo(0).DisplayBackColor = vbYellow
    TabDepInfo(0).DateTimeSetColumn 2
    TabDepInfo(0).DateTimeSetInputFormatString 2, "YYYYMMDDhhmm"
    TabDepInfo(0).DateTimeSetOutputFormatString 2, "hh':'mm'/'DD"
    TabDepInfo(0).AutoSizeByHeader = True
    TabDepInfo(0).AutoSizeColumns
    TabDepInfo(0).ShowVertScroller True
    TabDepInfo(0).Visible = True
    TabDepInfo(0).ZOrder
End Sub
Private Sub ArrangeChartArea()
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewSize As Long
    Dim NewValue As Long
    Dim MinSize As Long
    Dim tmpTag As String
    Dim tmpData As String
    Dim x1 As Long
    Dim X2 As Long
    Dim X3 As Long
    Dim LastSize As Long
    Dim PrvHours As Long
    Dim NewHours As Long
    Dim MinHours As Long
    Dim MaxHours As Long
    Dim MinCnxSize As Long
    Dim MovePanel As Boolean
    Dim LayOutIdx As Integer
    Dim PrvLayOut As Integer
    Dim NxtLayOut As Integer

    NewWidth = WorkArea(0).Width + 60
    VScrollArea(0).Width = NewWidth
    NewLeft = VScrollArea(0).Width - RightScale(0).Width - 90
    RightScale(0).Left = NewLeft
    RightScCon1(0).Left = NewLeft
    MinSize = CnxMidFilterPanel.Width + 75
    NewSize = NewLeft - LeftScCon1(0).Left - LeftScCon1(0).Width
    If NewSize < MinSize Then NewSize = MinSize
    MidScCon1(0).Width = NewSize
    MidScConx(0).Width = NewSize
    
    NewSize = NewSize - 75
    CnxFilterPanel.Width = NewSize
    fraTabCaption(0).Width = NewSize + 30
    lblTabCaptionShadow(0).Width = NewSize - 60
    lblTabCaption(0).Width = NewSize - 60
    NewLeft = (NewSize - CnxMidFilterPanel.Width) \ 2
    CnxMidFilterPanel.Left = NewLeft
    
    
    CurScrollWidth = CurChartWidth - WorkArea(0).Width
    NewValue = CLng(HScroll2(0).Value)
    AdjustHorizScroll NewValue
    
    MovePanel = False
    LayOutIdx = -1
    tmpTag = MidScConx(0).Tag
    If tmpTag <> "" Then
        GetKeyItem tmpData, tmpTag, "{X3}", "{/X3}"
        LastSize = Val(tmpData)
        If LastSize <> MidScConx(0).Width Then
            GetKeyItem tmpData, tmpTag, "{H}", "{/H}"
            PrvHours = Val(tmpData)
            Select Case CurLayoutType
                Case 0, 1
                Case 2, 3
                    MinHours = 1
                    MaxHours = 6
                Case 4
                    MinHours = 2
                    MaxHours = 6
                    MinCnxSize = 5160
                    PrvLayOut = 4
                    x1 = 1605
                    X3 = 1755
                Case Else
            End Select
            X2 = MidScConx(0).Width - x1 - X3
            NewHours = X2 \ 15 \ 60
            If NewHours < MinHours Then NewHours = MinHours
            If NewHours > MaxHours Then NewHours = MaxHours
            CurCnxLayoutHours = NewHours
            X2 = NewHours * 60 * 15
            NewSize = x1 + X2 + X3
            If NewSize >= MinCnxSize Then
            Else
                'LayOutIdx = PrvLayOut
                
            End If
                'Calculation: 6Hours * 60Min * 15Twips
                'CnxRangeMin = CurCnxLayoutHours * 60
                'NewLeft = LeftTime(0).Left + (CnxRangeMin * 15)
            
        End If
    End If
    
    NewLeft = LeftScale(0).Left + LeftScale(0).Width
    NewSize = RightScale(0).Left - NewLeft
    X2 = MidScConx(0).Width
    If X2 > NewSize Then NewLeft = NewLeft - ((X2 - NewSize) \ 2)
    MidScCon1(0).Left = NewLeft
    MidScConx(0).Left = NewLeft
    MidScCon1(0).ZOrder
    MidScConx(0).ZOrder
    
    TimeScale(0).Left = HScrollArea(0).Left
    TimeScale(0).Width = HScrollArea(0).Width
    'TimeScale(0).Refresh
    
    If LayOutIdx > 0 Then
        DontRefreshCnx = True
        chkLayout(LayOutIdx).Value = 1
        DontRefreshCnx = False
    End If
    CnxRefresh.Enabled = True
    CheckSelFlightPanels
    
End Sub
Private Sub RefreshCurrentCnxChart()
    Dim CurCurCnxBarIdx As Integer
    CurCurCnxBarIdx = CurCnxBarIdx
    If CurCurCnxBarIdx >= 0 Then
        ShowConnexDetails CurCurCnxBarIdx
    Else
        ShowAllConnexLines
    End If
End Sub

Private Sub fraTabCaption_DblClick(Index As Integer)
    RefreshCurrentCnxChart
End Sub

Private Sub HScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub HScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(HScroll2(Index).Value)
    AdjustHorizScroll NewValue
End Sub
Private Sub AdjustHorizScroll(ScrollValue As Long)
    Dim NewLeft As Long
    NewLeft = ScrollValue * MinuteWidth
    HScrollArea(0).Left = -NewLeft
    WorkArea(0).Refresh
    TimeScale(0).Left = -NewLeft
    TimeScale(0).Refresh
    CheckSelFlightPanels
End Sub

Private Sub icoCflRange_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    icoCflRange(Index).Tag = CStr(X)
    RangeLine(Index).Left = icoCflRange(Index).Left + 105
    RangeLine(Index).Visible = True
    If Index = 0 Then
        ShowTimeRangeDuration True, RangeLine(Index).Left, TimeLine1(0).Left
    Else
        ShowTimeRangeDuration True, TimeLine1(0).Left, RangeLine(Index).Left
    End If
End Sub

Private Sub icoCflRange_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim OldX As Long
    Dim NewX As Long
    Dim diffX As Long
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MinLeft As Long
    Dim DurLeft As Long
    Dim DurRight As Long
    OldX = CLng(Val(icoCflRange(Index).Tag))
    NewX = CLng(X)
    If OldX > 0 Then
        If OldX <> NewX Then
            diffX = NewX - OldX
            NewLeft = icoCflRange(Index).Left + diffX
            NewLeft = (NewLeft \ MinuteWidth) * MinuteWidth
            If Index = 0 Then
                MinLeft = lblMin1(0).Left
                MaxLeft = TimeLine1(0).Left - (MinuteWidth * 10)
                DurLeft = NewLeft + 105
                DurRight = TimeLine1(0).Left
            Else
                MinLeft = TimeLine1(0).Left + (MinuteWidth * 10)
                MaxLeft = CurChartWidth - 1200
                DurRight = NewLeft + 105
                DurLeft = TimeLine1(0).Left
            End If
            If (NewLeft >= MinLeft) And (NewLeft <= MaxLeft) Then
                RangeLine(Index).Visible = False
                HScrollArea(0).Refresh
                icoCflRange(Index).Left = NewLeft
                NewLeft = NewLeft + 105
                RangeLine(Index).Left = NewLeft
                RangeLine(Index).Visible = True
                RangeLine(Index).Refresh
                ShowTimeRangeDuration True, DurLeft, DurRight
            End If
        End If
    End If
End Sub
Private Function ShowTimeRangeDuration(ShowLabel As Boolean, CurLeft As Long, CurRight As Long) As Long
    Dim tmpLeft As Long
    Dim tmpRight As Long
    Dim tmpWidth As Long
    Dim tmpDuration As Long
    Dim tmpVal As Long
    Dim tmpText As String
    lblDuration.Visible = False
    If CurRight > CurLeft Then
        tmpLeft = CurLeft
        tmpRight = CurRight
    Else
        tmpLeft = CurRight
        tmpRight = CurLeft
    End If
    tmpWidth = tmpRight - tmpLeft + 15
    lblDuration.Left = tmpLeft
    lblDuration.Width = tmpWidth
    tmpDuration = tmpWidth \ MinuteWidth
    tmpVal = tmpDuration Mod 60
    tmpText = Right("00" & CStr(tmpVal), 2)
    tmpVal = (tmpDuration - tmpVal) \ 60
    tmpText = Right("00" & CStr(tmpVal), 2) & ":" & tmpText
    lblDuration.Caption = tmpText
    lblDuration.Visible = ShowLabel
    lblDuration.Refresh
    ShowTimeRangeDuration = tmpDuration
End Function

Private Sub icoCflRange_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    icoCflRange(Index).Tag = "-1"
    RangeLine(Index).Visible = False
    lblDuration.Visible = False
End Sub

Private Sub lblArrAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ArrFlight(Index).ZOrder
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblArrBar(Index), True
End Sub

Private Sub lblArrBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblArrBar(Index), False
End Sub

Private Sub lblArrCsct_Click(Index As Integer)
    Dim i As Integer
    If lblArrCsct(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        If lblArrCsct(Index).BackColor = vbRed Then chkCnxShort(0).Value = 1
        If lblArrCsct(Index).BackColor = vbCyan Then chkCnxCrit(0).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrCsct_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub lblArrEtai_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOnbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrOrg3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrPsta_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrPsta_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub lblArrRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrStoa_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblArrTbag_Click(Index As Integer)
    If lblArrTbag(Index).Caption <> "" Then ToggleCnxView Index, "ARR", "BAG"
End Sub

Private Sub lblArrTbag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If lblArrTbag(Index).MousePointer = 99 Then TriggerMouseOverObj Index, "ARRBAG_" & CStr(Index), "ARR"
End Sub

Private Sub lblArrTot1_Click(Index As Integer)
    If lblArrTot1(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxClass(0).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrTot2_Click(Index As Integer)
    If lblArrTot2(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxClass(1).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrTot3_Click(Index As Integer)
    If lblArrTot3(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxClass(2).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrTot4_Click(Index As Integer)
    If lblArrTot4(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxClass(3).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrTotBag_Click(Index As Integer)
    Dim i As Integer
    If lblArrTotBag(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxType(1).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrTotPax_Click(Index As Integer)
    Dim i As Integer
    If lblArrTotPax(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxType(0).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrTotUld_Click(Index As Integer)
    Dim i As Integer
    If lblArrTotUld(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxType(2).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblArrTpax_Click(Index As Integer)
    If lblArrTpax(Index).Caption <> "" Then ToggleCnxView Index, "ARR", "PAX"
End Sub
Private Sub ToggleCnxView(Index As Integer, SetAdid As String, SetType As String)
    DontRefreshCnx = True
    Select Case SetAdid
        Case "ARR"
            chkCnxAdid(0).Value = 1
        Case "DEP"
            chkCnxAdid(1).Value = 1
        Case Else
    End Select
    Select Case SetType
        Case "PAX"
            chkCnxType(0).Value = 1
        Case "BAG"
            chkCnxType(1).Value = 1
        Case "ULD"
            chkCnxType(2).Value = 1
        Case Else
    End Select
    DontRefreshCnx = False
    If CurCnxBarIdx >= 0 Then
        ShowConnexDetails Index
    Else
        ShowAllConnexLines
    End If
End Sub

Private Sub lblArrTpax_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If lblArrTpax(Index).MousePointer = 99 Then TriggerMouseOverObj Index, "ARRPAX_" & CStr(Index), "ARR"
End Sub

Private Sub TriggerMouseOverObj(Index As Integer, ObjKey As String, TimerKey As String)
    Dim tmpTag As String
    Dim tmpType As String
    Dim tmpIdx As String
    Dim tmpObj As String
    Dim iBar As Integer
    If CurCnxView = 0 Then
        'tmpObj = ""
        'If CnxMouseObj <> tmpObj Then
            CnxMouseObj = tmpObj
            tmpTag = MouseTimer(0).Tag
            tmpType = GetItem(tmpTag, 3, ",")
            tmpIdx = GetItem(tmpTag, 2, ",")
            iBar = Val(tmpIdx)
            If (Index <> iBar) Or (tmpType <> ObjKey) Then
                MouseTimer(0).Enabled = False
                tmpType = Left(tmpType, 3)
                Select Case tmpType
                    Case "ARR"
                        ArrInfoPanel(iBar).Visible = False
                    Case "DEP"
                        DepInfoPanel(iBar).Visible = False
                    Case Else
                End Select
                MouseTimer(0).Tag = TimerKey & "," & CStr(Index) & "," & ObjKey
                MouseTimer(0).Enabled = True
            End If
        'End If
    End If
End Sub

Private Sub lblArrTuld_Click(Index As Integer)
    If lblArrTuld(Index).Caption <> "" Then ToggleCnxView Index, "ARR", "ULD"
End Sub

Private Sub lblArrTuld_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If lblArrTuld(Index).MousePointer = 99 Then TriggerMouseOverObj Index, "ARRULD_" & CStr(Index), "ARR"
End Sub

Private Sub lblArrVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepAct3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepBar_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    DepFlight(Index).ZOrder
    HighlightCurrentBar Index, ChartName
    ShowRotaTimeFrame Index, lblDepBar(Index), True
End Sub

Private Sub lblDepBar_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    ShowRotaTimeFrame Index, lblDepBar(Index), False
End Sub

Private Sub lblDepCsct_Click(Index As Integer)
    Dim i As Integer
    If lblDepCsct(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        If lblDepCsct(Index).BackColor = vbRed Then chkCnxShort(0).Value = 1
        If lblDepCsct(Index).BackColor = vbCyan Then chkCnxCrit(0).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepDes3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepEtdi_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlno_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepFlti_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepOfbl_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepPstd_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepRegn_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepStod_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblDepTbag_Click(Index As Integer)
    If lblDepTbag(Index).Caption <> "" Then ToggleCnxView Index, "DEP", "BAG"
End Sub

Private Sub lblDepTbag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TriggerMouseOverObj Index, "DEPBAG_" & CStr(Index), "DEP"
End Sub

Private Sub lblDepTot1_Click(Index As Integer)
    If lblDepTot1(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        chkCnxClass(0).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepTot2_Click(Index As Integer)
    If lblDepTot2(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        chkCnxClass(1).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepTot3_Click(Index As Integer)
    If lblDepTot3(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        chkCnxClass(2).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepTot4_Click(Index As Integer)
    If lblDepTot4(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        chkCnxClass(3).Value = 1
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepTotBag_Click(Index As Integer)
    Dim i As Integer
    If lblDepTotBag(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        chkCnxType(1).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepTotPax_Click(Index As Integer)
    Dim i As Integer
    If lblDepTotPax(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        chkCnxType(0).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepTotUld_Click(Index As Integer)
    Dim i As Integer
    If lblDepTotUld(Index).Caption <> "" Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        chkCnxType(2).Value = 1
        If chkCnxClass(0).Tag <> "" Then
            i = Val(chkCnxClass(0).Tag)
            chkCnxClass(i).Value = 0
        End If
        chkCnxShort(0).Value = 0
        chkCnxCrit(0).Value = 0
        DontRefreshCnx = False
        ShowConnexDetails Index
    End If
End Sub

Private Sub lblDepTpax_Click(Index As Integer)
    If lblDepTpax(Index).Caption <> "" Then ToggleCnxView Index, "DEP", "PAX"
End Sub

Private Sub lblDepTpax_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TriggerMouseOverObj Index, "DEPPAX_" & CStr(Index), "DEP"
End Sub

Private Sub lblDepTuld_Click(Index As Integer)
    If lblDepTuld(Index).Caption <> "" Then ToggleCnxView Index, "DEP", "ULD"
End Sub

Private Sub lblDepTuld_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    TriggerMouseOverObj Index, "DEPULD_" & CStr(Index), "DEP"
End Sub

Private Sub lblDepVia3_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub lblSelDepDes3_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub lblSelDepFlno_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub lblTabCaptionShadow_Click(Index As Integer)
    MidScConx(0).ZOrder
End Sub

Private Sub lblTabCaptionShadow_DblClick(Index As Integer)
    RefreshCurrentCnxChart
End Sub

Private Sub lblTabCaptionShadow_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub lblTowBar_Click(Index As Integer)
    HighlightCurrentBar Index, ChartName
End Sub

Private Sub ShowRotaTimeFrame(Index As Integer, CurBarObj As Label, ShowRange As Boolean)
    Dim RotLeft As Long
    Dim RotRight As Long
    If ShowRange = True Then
        RotLeft = CurBarObj.Left - 15
        RotRight = CurBarObj.Left + CurBarObj.Width - 15
        RangeLine(0).Left = RotLeft
        RangeLine(1).Left = RotRight
        icoCflRange(0).Left = RotLeft - 105
        icoCflRange(1).Left = RotRight - 105
        RangeLine(0).Visible = True
        RangeLine(1).Visible = True
        icoCflRange(0).Visible = True
        icoCflRange(1).Visible = True
    Else
        RangeLine(0).Visible = False
        RangeLine(1).Visible = False
        icoCflRange(0).Visible = False
        icoCflRange(1).Visible = False
    End If
End Sub

Private Sub LeftScCon1_Click(Index As Integer)
    LeftScale(0).ZOrder
End Sub

Private Sub MidScConx_Click(Index As Integer)
    MidScConx(0).ZOrder
End Sub

Private Sub MidScConx_DblClick(Index As Integer)
    If chkWork(1).Value = 0 Then
        'chkWork(1).Value = 1
    Else
        'chkWork(1).Value = 0
    End If
End Sub

Private Sub MidScConx_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub
Private Sub HideMouseInfoPanel()
    Dim tmpTagA As String
    Dim tmpTagB As String
    Dim tmpType As String
    Dim tmpIdx As String
    Dim tmpObj As String
    Dim iBar As Integer
    If CurCnxView = 0 Then
        MouseTimer(0).Enabled = False
        MouseTimer(1).Enabled = False
        tmpTagA = MouseTimer(0).Tag
        tmpTagB = MouseTimer(1).Tag
        MouseTimer(0).Tag = ""
        MouseTimer(1).Tag = ""
        If tmpTagA <> "" Then
            tmpType = GetItem(tmpTagA, 1, ",")
            tmpIdx = GetItem(tmpTagA, 2, ",")
            iBar = Val(tmpIdx)
            Select Case tmpType
                Case "ARR"
                    ArrInfoPanel(iBar).Visible = False
                Case "DEP"
                    DepInfoPanel(iBar).Visible = False
                Case Else
            End Select
        End If
        If tmpTagB <> "" Then
            tmpType = GetItem(tmpTagB, 1, ",")
            tmpIdx = GetItem(tmpTagB, 2, ",")
            iBar = Val(tmpIdx)
            Select Case tmpType
                Case "ARR"
                    ArrInfoPanel(iBar).Visible = False
                Case "DEP"
                    DepInfoPanel(iBar).Visible = False
                Case Else
            End Select
        End If
    End If
End Sub
Private Sub MouseTimer_Timer(Index As Integer)
    Dim tmpTag As String
    Dim tmpType As String
    Dim tmpIdx As String
    Dim tmpObj As String
    Dim tmpKey As String
    Dim tmpTxt As String
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim CnxBackColor As Long
    Dim CnxForeColor As Long
    Dim iBar As Integer
    Dim iItm As Integer
    If CurCnxView = 0 Then
        Select Case Index
            Case 0
                MouseTimer(0).Enabled = False
                tmpTag = MouseTimer(0).Tag
                If tmpTag <> "" Then
                    tmpType = GetItem(tmpTag, 1, ",")
                    tmpIdx = GetItem(tmpTag, 2, ",")
                    tmpObj = GetItem(tmpTag, 3, ",")
                    iBar = Val(tmpIdx)
                    NewTop = ArrLblPanel(iBar).Top
                    tmpKey = Mid(tmpObj, 4, 3)
                    Select Case tmpKey
                        Case "PAX"
                            iItm = 1
                            NewTop = NewTop + lblArrTpax(iBar).Top + (lblArrTpax(iBar).Height \ 2)
                            CnxBackColor = LightGreen
                            CnxForeColor = vbBlack
                        Case "BAG"
                            iItm = 2
                            NewTop = NewTop + lblArrTbag(iBar).Top + (lblArrTbag(iBar).Height \ 2)
                            CnxBackColor = LightYellow
                            CnxForeColor = vbBlack
                        Case "ULD"
                            iItm = 3
                            NewTop = NewTop + lblArrTuld(iBar).Top + (lblArrTuld(iBar).Height \ 2)
                            CnxBackColor = LightBlue
                            CnxForeColor = vbWhite
                        Case Else
                            iItm = 4
                            CnxBackColor = LightGrey
                            CnxForeColor = vbBlack
                    End Select
                    Select Case tmpType
                        Case "ARR"
                            ArrCnxSum1(iBar).Caption = GetItem(ArrCnxSum1(iBar).Tag, iItm, ",")
                            ArrCnxSum2(iBar).Caption = GetItem(ArrCnxSum2(iBar).Tag, iItm, ",")
                            ArrCnxSum3(iBar).Caption = GetItem(ArrCnxSum3(iBar).Tag, iItm, ",")
                            ArrCnxSum4(iBar).Caption = GetItem(ArrCnxSum4(iBar).Tag, iItm, ",")
                            If ArrCnxSum1(iBar).Caption = "0" Then ArrCnxSum1(iBar).Caption = ""
                            If ArrCnxSum2(iBar).Caption = "0" Then ArrCnxSum2(iBar).Caption = ""
                            If ArrCnxSum3(iBar).Caption = "0" Then ArrCnxSum3(iBar).Caption = ""
                            If ArrCnxSum4(iBar).Caption = "0" Then ArrCnxSum4(iBar).Caption = ""
                            ArrCnxSum1(iBar).BackColor = CnxBackColor
                            ArrCnxSum2(iBar).BackColor = CnxBackColor
                            ArrCnxSum3(iBar).BackColor = CnxBackColor
                            ArrCnxSum4(iBar).BackColor = CnxBackColor
                            ArrCnxSum1(iBar).ForeColor = CnxForeColor
                            ArrCnxSum2(iBar).ForeColor = CnxForeColor
                            ArrCnxSum3(iBar).ForeColor = CnxForeColor
                            ArrCnxSum4(iBar).ForeColor = CnxForeColor
                            ArrCnxCsct(iBar).Caption = tmpKey
                            ArrCnxCsct(iBar).ForeColor = vbBlack
                            ArrCnxCsct(iBar).BackColor = vbWhite
                            NewTop = NewTop - (ArrInfoPanel(iBar).Height \ 2)
                            NewLeft = picLeftBarCnx(iBar).Left + picLeftBarCnx(iBar).Width + 120
                            ArrInfoPanel(iBar).Top = NewTop
                            ArrInfoPanel(iBar).Left = NewLeft
                            ArrInfoPanel(iBar).Visible = True
                        Case "DEP"
                            DepCnxSum1(iBar).Caption = GetItem(DepCnxSum1(iBar).Tag, iItm, ",")
                            DepCnxSum2(iBar).Caption = GetItem(DepCnxSum2(iBar).Tag, iItm, ",")
                            DepCnxSum3(iBar).Caption = GetItem(DepCnxSum3(iBar).Tag, iItm, ",")
                            DepCnxSum4(iBar).Caption = GetItem(DepCnxSum4(iBar).Tag, iItm, ",")
                            If DepCnxSum1(iBar).Caption = "0" Then DepCnxSum1(iBar).Caption = ""
                            If DepCnxSum2(iBar).Caption = "0" Then DepCnxSum2(iBar).Caption = ""
                            If DepCnxSum3(iBar).Caption = "0" Then DepCnxSum3(iBar).Caption = ""
                            If DepCnxSum4(iBar).Caption = "0" Then DepCnxSum4(iBar).Caption = ""
                            DepCnxSum1(iBar).BackColor = CnxBackColor
                            DepCnxSum2(iBar).BackColor = CnxBackColor
                            DepCnxSum3(iBar).BackColor = CnxBackColor
                            DepCnxSum4(iBar).BackColor = CnxBackColor
                            DepCnxSum1(iBar).ForeColor = CnxForeColor
                            DepCnxSum2(iBar).ForeColor = CnxForeColor
                            DepCnxSum3(iBar).ForeColor = CnxForeColor
                            DepCnxSum4(iBar).ForeColor = CnxForeColor
                            DepCnxCsct(iBar).Caption = tmpKey
                            DepCnxCsct(iBar).ForeColor = vbBlack
                            DepCnxCsct(iBar).BackColor = vbWhite
                            NewTop = NewTop - (DepInfoPanel(iBar).Height \ 2)
                            NewLeft = picRightBarCnx(iBar).Left - DepInfoPanel(iBar).Width - 120
                            DepInfoPanel(iBar).Top = NewTop
                            DepInfoPanel(iBar).Left = NewLeft
                            DepInfoPanel(iBar).Visible = True
                        Case Else
                    End Select
                End If
            Case 1
                MouseTimer(1).Enabled = False
                tmpTag = MouseTimer(1).Tag
                If tmpTag <> "" Then
                    tmpType = GetItem(tmpTag, 1, ",")
                    tmpIdx = GetItem(tmpTag, 2, ",")
                    iBar = Val(tmpIdx)
                    Select Case tmpType
                        Case "ARR"
                            ArrInfoPanel(iBar).Visible = False
                        Case "DEP"
                            DepInfoPanel(iBar).Visible = False
                        Case Else
                    End Select
                End If
                MouseTimer(1).Tag = ""
            Case Else
        End Select
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then
        OnTop.BackColor = LightGreen
        SetFormOnTop Me, True
    Else
        OnTop.BackColor = MyOwnButtonFace
        SetFormOnTop Me, False
    End If
End Sub

Private Sub picLeftBarCnx_Click(Index As Integer)
    If picLeftBarCnx(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        'we keep the current other filters
        DontRefreshCnx = False
        If CurCnxBarIdx >= 0 Then
            ShowAllConnexLines
        Else
            ShowConnexDetails Index
        End If
    End If
End Sub

Private Sub picLeftBarCnx_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    'TriggerMouseOverObj Index, "ARRCNX_" & CStr(Index), "ARR"
End Sub

Private Sub picDepCnx_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub picDepStatus_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    HideMouseInfoPanel
End Sub

Private Sub picRightBarCnx_Click(Index As Integer)
    If picRightBarCnx(Index).MousePointer = 99 Then
        DontRefreshCnx = True
        chkCnxAdid(1).Value = 1
        'we keep the current other filters
        DontRefreshCnx = False
        If CurCnxBarIdx >= 0 Then
            ShowAllConnexLines
        Else
            ShowConnexDetails Index
        End If
    End If
End Sub

Private Sub RightScCon1_Click(Index As Integer)
    RightScale(0).ZOrder
End Sub

Private Sub TimeScaleTimer_Timer()
    Dim DisplayTime As String
    Dim DisplayDate As String
    Dim NewLeft As Long
    Dim LeftOff As Long
    Dim TimeOff As Long
    Dim retval As Boolean
    Dim CurLocTime
    Dim CurUtcTime
    Dim SecDiff
    
    'The time synchronization will be done based on the server time.
    'So we must calculate the differences of the elapsed time since the last timer event.
    CurLocTime = Now
    SecDiff = DateDiff("s", LastLocTimeCheck, CurLocTime)
    LastLocTimeCheck = CurLocTime
    ServerUtcTimeVal = DateAdd("s", SecDiff, ServerUtcTimeVal)
    LastUtcTimeCheck = ServerUtcTimeVal
    If chkUtc(0).Value = 1 Then
        DisplayTime = Format(LastUtcTimeCheck, "hh:mm:ss")
        DisplayDate = Format(LastUtcTimeCheck, "YYYYMMDD")
    Else
        DisplayTime = Format(LastLocTimeCheck, "hh:mm:ss")
        DisplayDate = Format(LastLocTimeCheck, "YYYYMMDD")
    End If
    DisplayDate = DecodeSsimDayFormat(DisplayDate, "CEDA", "SSIM2")
    lblCurTime(0).Caption = DisplayTime
    lblCurTime(0).ToolTipText = DisplayDate
    
    retval = GetTimeScalePos("", ServerUtcTimeVal, NewLeft)
    If retval = True Then
        NewLeft = NewLeft - 15
        TimeOff = NewLeft - TimeLine1(0).Left
        If TimeOff <> 0 Then
            lblCurTime(0).Visible = False
            TimeLine1(0).Visible = False
            TimeLine2(0).Visible = False
            LeftOff = TimeLine1(0).Left - lblCurTime(0).Left
            TimeLine1(0).Left = NewLeft
            TimeLine2(0).Left = NewLeft
            lblCurTime(0).Left = NewLeft - LeftOff
            If chkTime1(0).Value = 1 Then
                If HScroll2(0).Value < 3600 Then
                    HScroll2(0).Value = HScroll2(0).Value + 1
                End If
            End If
            TimeLine2(0).Visible = True
            TimeLine1(0).Visible = True
            lblCurTime(0).Visible = True
        End If
    Else
        lblCurTime(0).Visible = False
        TimeLine1(0).Visible = False
        TimeLine2(0).Visible = False
    End If
End Sub

Private Function GetTimeScalePos(CedaUtcTime As String, CedaTimeValue, ScalePos As Long) As Boolean
    Dim MinDiff As Long
    If CedaUtcTime <> "" Then CedaTimeValue = CedaFullDateToVb(CedaUtcTime)
    MinDiff = CLng(DateDiff("n", TimeScaleUtcBegin, CedaTimeValue))
    If (MinDiff >= 0) And (MinDiff <= CLng(lblMin1.UBound)) Then
        ScalePos = lblMin1(CInt(MinDiff)).Left
        GetTimeScalePos = True
    Else
        ScalePos = MinDiff * MinuteWidth
        GetTimeScalePos = False
    End If
End Function
Private Sub VScroll1_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll1_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll1(Index).Value)
    AdjustVertScroll NewValue
    VScroll2(Index).Value = VScroll1(Index).Value
End Sub
Private Sub VScroll2_Change(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub VScroll2_Scroll(Index As Integer)
    Dim NewValue As Long
    NewValue = CLng(VScroll2(Index).Value)
    AdjustVertScroll NewValue
    VScroll1(Index).Value = VScroll2(Index).Value
End Sub
Private Sub AdjustVertScroll(ScrollValue As Long)
    Dim NewTop As Long
    NewTop = ScrollValue * (ChartLine2(0).Height + 30)
    VScrollArea(0).Top = -NewTop
    WorkArea(0).Refresh
    CheckSelFlightPanels
End Sub

Private Sub CheckSelFlightPanels()
    Dim NewLeft As Long
    Dim ScrollValue As Integer
    Dim LinCnt As Integer
    Dim BotIdx As Integer
    Dim ShowPanel As Boolean
    ScrollValue = VScroll2(0).Value
    LinCnt = Int(WorkArea(0).Height / (ChartLine2(0).Height + 30))
    BotIdx = ScrollValue + LinCnt - 1
    If CurCnxBarIdx >= 0 Then
        ShowPanel = False
        If chkSelDeco(2).Value = 0 Then
            NewLeft = ArrFlight(CurCnxBarIdx).Left + HScrollArea(0).Left
            If NewLeft < 0 Then ShowPanel = True
            NewLeft = NewLeft - WorkArea(0).Width + ArrFlight(CurCnxBarIdx).Width
            If NewLeft > 0 Then ShowPanel = True
        End If
        If CurCnxBarIdx < ScrollValue Then
            'picSelArrCursor(0).Picture = imgCurPosUp.Picture
            ShowPanel = True
        End If
        If CurCnxBarIdx > BotIdx Then
            'picSelArrCursor(0).Picture = imgCurPosDn.Picture
            ShowPanel = True
        End If
        'SelArrPanel(0).Visible = ShowPanel
        If chkSelDeco(2).Value = 0 Then
            LeftScCon1(0).Visible = ShowPanel
            LeftScCon1(0).Refresh
        End If
    End If
End Sub
Private Sub ToggleFlightBarTimes(TimeOffs As Integer)
    Dim i As Integer
    For i = 0 To CurBarMax
        SetTimeFieldCaption lblArrStoa(i), TimeOffs
        SetTimeFieldCaption lblArrEtai(i), TimeOffs
        SetTimeFieldCaption lblArrOnbl(i), TimeOffs
        SetTimeFieldCaption lblDepStod(i), TimeOffs
        SetTimeFieldCaption lblDepEtdi(i), TimeOffs
        SetTimeFieldCaption lblDepOfbl(i), TimeOffs
    Next
    TabDepInfo(0).DateTimeSetUTCOffsetMinutes 2, TimeOffs
    TabDepInfo(0).Refresh
End Sub
Private Sub SetTimeFieldCaption(CurLbl As Label, TimeOffs As Integer)
    Dim tmpTag As String
    Dim tmpTxt As String
    Dim tmpTip As String
    Dim tmpDay As String
    Dim tmpTime
    tmpTag = CurLbl.Tag
    tmpTxt = ""
    tmpTip = GetItem(CurLbl.ToolTipText, 1, "[") & "  "
    If Trim(tmpTag) <> "" Then
        tmpTime = CedaFullDateToVb(tmpTag)
        tmpTime = DateAdd("n", TimeOffs, tmpTime)
        tmpTxt = Format(tmpTime, "hh:mm")
        tmpDay = Format(tmpTime, "yyyymmdd")
        tmpDay = DecodeSsimDayFormat(tmpDay, "CEDA", "SSIM2")
        tmpTip = tmpTip & "[" & tmpDay & "]"
    End If
    CurLbl.Caption = tmpTxt
    CurLbl.ToolTipText = Trim(tmpTip)
End Sub

Public Sub GetFlightRotations()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim DepLine As Long
    Dim ArrLine As Long
    Dim ArrRotLine As Long
    Dim DepRotLine As Long
    Dim iBar As Integer
    Dim jBar As Integer
    Dim tmpData As String
    Dim tmpAidx As String
    Dim tmpDidx As String
    Dim tmpRidx As String
    Dim tmpGidx As String
    Dim tmpView As String
    Dim ArrPaxHitLine As String
    Dim ArrBagHitLine As String
    Dim ArrUldHitLine As String
    Dim DepPaxHitLine As String
    Dim DepBagHitLine As String
    Dim DepUldHitLine As String
    Dim tmpHitLine As String
    Dim ArrUrno As String
    Dim ArrTifa As String
    Dim DepUrno As String
    Dim DepTifd As String
    Dim DepOfbl As String
    Dim TopPos As Long
    Dim ArrScalePos As Long
    Dim DepScalePos As Long
    Dim DepLeft As Long
    Dim ArrLeft As Long
    Dim RotWidth As Long
    Dim ArrWidth As Long
    Dim DepWidth As Long
    Dim MinDepWidth As Long
    Dim MaxDepWidth As Long
    Dim MinArrWidth As Long
    Dim MinRotWidth As Long
    Dim MaxArrWidth As Long
    Dim IdxColNo As Long
    Dim IsValidArrScalePos As Boolean
    Dim IsValidDepScalePos As Boolean
    Dim ArrVisible As Boolean
    Dim DepVisible As Boolean
    Dim GetFlights As Boolean
    Dim DepTifdValue
    Dim ArrTifaValue
    chkSelDeco(1).Value = 1
    chkSelDeco(0).Value = 1
    LeftScale(0).Refresh
    RightScale(0).Refresh
    CreateTimeScaleArea TimeScale(0)
    MaxDepWidth = MinuteWidth * 60
    MaxArrWidth = MinuteWidth * 60
    MinDepWidth = MinuteWidth * 20
    MinArrWidth = MinuteWidth * 20
    MinRotWidth = MinuteWidth * 15
    TabRotFlightsTab.IndexDestroy "CONX"
    TabArrCnxDetails.ResetContent
    TabDepCnxDetails.ResetContent
    iBar = 0
    MaxLine = TabRotFlightsTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        If iBar <= ChartLine2.UBound Then
            tmpView = "1"
            'tmpView = TabRotFlightsTab.GetFieldValue(CurLine, "VIEW")
            If tmpView = 1 Then
                ArrLine = -1
                DepLine = -1
                ArrRotLine = -1
                DepRotLine = -1
                GetFlights = False
                ArrPaxHitLine = ""
                ArrBagHitLine = ""
                ArrUldHitLine = ""
                DepPaxHitLine = ""
                DepBagHitLine = ""
                DepUldHitLine = ""
                ArrUrno = TabRotFlightsTab.GetFieldValue(CurLine, "AURN")
                GetFlights = GetArrCnxDetails(ArrUrno, GetFlights)
                DepUrno = TabRotFlightsTab.GetFieldValue(CurLine, "DURN")
                GetFlights = GetDepCnxDetails(DepUrno, GetFlights)
                If GetFlights = True Then
                    tmpRidx = TabRotFlightsTab.GetFieldValue(CurLine, "RIDX")
                    tmpHitLine = TabArrFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
                    If tmpHitLine <> "" Then ArrRotLine = Val(tmpHitLine)
                    tmpHitLine = TabDepFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
                    If tmpHitLine <> "" Then DepRotLine = Val(tmpHitLine)
                    If ArrRotLine >= 0 Then
                        tmpAidx = TabRotFlightsTab.GetFieldValue(CurLine, "AIDX")
                        tmpData = TabArrFlightsTab.GetFieldValue(ArrRotLine, "'S3'")
                        ArrLine = ArrRotLine
                        If tmpAidx <> tmpData Then
                            'Here we have a problem
                        End If
                    End If
                    If DepRotLine >= 0 Then
                        tmpDidx = TabRotFlightsTab.GetFieldValue(CurLine, "DIDX")
                        tmpData = TabDepFlightsTab.GetFieldValue(DepRotLine, "'S3'")
                        DepLine = DepRotLine
                        If tmpDidx <> tmpData Then
                            'Here we have a problem
                        End If
                    End If
                    tmpGidx = Right("000000" & CStr(iBar), 6)
                    GetArrFlightData ArrLine, iBar
                    GetArrCnxTotals ArrUrno, iBar
                    GetDepFlightData DepLine, iBar
                    GetDepCnxTotals DepUrno, iBar
                    If (ArrLine >= 0) And (DepLine >= 0) Then
                        'ROTATION
                        DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                        IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                        ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                        IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                        
                        RotWidth = DepScalePos - ArrScalePos + 15
                        If RotWidth >= MinRotWidth Then
                            lblBarLineColor(iBar).BackColor = ChartBarBackColor
                            lblBarLineColor(iBar).Tag = "7"
                        Else
                            lblBarLineColor(iBar).BackColor = vbRed
                            lblBarLineColor(iBar).Tag = "1"
                        End If
                        
                        ArrWidth = RotWidth \ 2
                        DepWidth = RotWidth - ArrWidth
                        If DepWidth > MaxDepWidth Then DepWidth = MaxDepWidth
                        If DepWidth < MinDepWidth Then DepWidth = MinDepWidth
                        DepLeft = DepScalePos - DepWidth + 15
                        ArrWidth = RotWidth
                        If ArrWidth < MinArrWidth Then ArrWidth = MinArrWidth
                        ArrLeft = ArrScalePos
                        ArrVisible = True
                        DepVisible = True
                        lblArrBar(iBar).Tag = "Y"
                        lblDepBar(iBar).Tag = "Y"
                    ElseIf DepLine >= 0 Then
                        'SINGLE DEPARTURE
                        DepTifd = TabRotFlightsTab.GetFieldValue(CurLine, "TIFD")
                        IsValidDepScalePos = GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
                        DepWidth = MaxDepWidth
                        DepLeft = DepScalePos - DepWidth
                        ArrLeft = 0
                        ArrWidth = 150
                        ArrVisible = False
                        DepVisible = True
                        lblArrBar(iBar).Tag = "N"
                        lblDepBar(iBar).Tag = "Y"
                    Else
                        'SINGLE ARRIVAL
                        ArrTifa = TabRotFlightsTab.GetFieldValue(CurLine, "TIFA")
                        IsValidArrScalePos = GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
                        ArrWidth = MaxArrWidth
                        ArrLeft = ArrScalePos
                        DepLeft = 0
                        DepWidth = 150
                        ArrVisible = True
                        DepVisible = False
                        lblArrBar(iBar).Tag = "Y"
                        lblDepBar(iBar).Tag = "N"
                    End If
                    lblArrBar(iBar).Width = ArrWidth
                    lblDepBar(iBar).Width = DepWidth
                    lblDepBar(iBar).Left = DepLeft
                    lblArrBar(iBar).Left = ArrLeft
                    lblArrBar(iBar).Visible = ArrVisible
                    lblDepBar(iBar).Visible = DepVisible
                    lblDepBar(iBar).ZOrder
                    ArrFlight(iBar).Visible = ArrVisible
                    DepFlight(iBar).Visible = DepVisible
                    'tmpGidx = tmpView & "," & tmpGidx
                    'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpGidx
                    'TabRotFlightsTab.SetLineColor CurLine, vbBlack, vbWhite
                    TabRotFlightsTab.SetFieldValues CurLine, "CONX", tmpGidx
                    ChartLine2(iBar).BackColor = lblBarLineColor(iBar).BackColor
                    iBar = iBar + 1
                Else
                    'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpView & ",------"
                    'TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
                    TabRotFlightsTab.SetFieldValues CurLine, "CONX", "------"
                End If
            Else
                'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpView & ",------"
                'TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
                TabRotFlightsTab.SetFieldValues CurLine, "CONX", "------"
            End If
        Else
            'tmpView = "0"
            'TabRotFlightsTab.SetFieldValues CurLine, "VIEW,CONX", tmpView & ",------"
            'TabRotFlightsTab.SetLineColor CurLine, vbBlack, LightGray
            TabRotFlightsTab.SetFieldValues CurLine, "CONX", "------"
        End If
    Next
    jBar = iBar - 1
    If jBar < 0 Then
        jBar = 0
    End If
    TopPos = ChartLine2(jBar).Top + ChartLine2(0).Height
    CurChartHeight = TopPos + 90
    VScrollArea(0).Height = CurChartHeight
    HScrollArea(0).Height = CurChartHeight
    LeftScale(0).Height = CurChartHeight
    MidScConx(0).Height = CurChartHeight
    RightScale(0).Height = CurChartHeight
    TimeLine2(0).Height = CurChartHeight
    RangeLine(0).Height = CurChartHeight
    RangeLine(1).Height = CurChartHeight
    VScroll1(0).Max = jBar
    VScroll2(0).Max = jBar
    
    TabRotFlightsTab.AutoSizeColumns
    IdxColNo = CLng(GetRealItemNo(TabRotFlightsTab.LogicalFieldList, "CONX"))
    TabRotFlightsTab.IndexCreate "CONX", IdxColNo
    TabRotFlightsTab.Refresh
    TabArrCnxDetails.IndexCreate "UKEY", 0
    TabArrCnxDetails.IndexCreate "AURN", 1
    TabArrCnxDetails.IndexCreate "DURN", 2
    TabDepCnxDetails.IndexCreate "UKEY", 0
    TabDepCnxDetails.IndexCreate "AURN", 1
    TabDepCnxDetails.IndexCreate "DURN", 2
    If (TabArrCnxBagIdx >= 0) Or (TabDepCnxBagIdx >= 0) Then chkCnxType(1).Enabled = True Else chkCnxType(1).Enabled = False
    If (TabArrCnxUldIdx >= 0) Or (TabDepCnxUldIdx >= 0) Then chkCnxType(2).Enabled = True Else chkCnxType(2).Enabled = False
    Me.Refresh
    'chkSelDeco(1).Value = 0
    'chkSelDeco(0).Value = 0
    LeftScale(0).Refresh
    RightScale(0).Refresh
    Me.Refresh
    CnxRefresh.Enabled = False
    If ChartIsReadyForUse = False Then
        ChartIsReadyForUse = True
        CurBarIdx = -1
        HighlightCurrentBar 0, ChartName
        CnxFilterAdid = 0
        CnxFilterType = 0
        CnxFilterClass = 0
        CnxFilterShort = 0
        CnxFilterCritical = 0
        DontRefreshCnx = True
        chkCnxAdid(0).Value = 1
        chkCnxType(0).Value = 0
        DontRefreshCnx = False
        ShowAllConnexLines
        'chkCnxAdid(1).Value = 1
        'CnxFilterAdid = 1
        'ShowAllDepPaxConnex
    Else
        RefreshCurrentCnxChart
    End If
End Sub

Private Function GetArrCnxDetails(ArrUrno As String, retval As Boolean) As Boolean
    Dim IsCnxFlight As Boolean
    Dim CfiHitLine As String
    Dim CnxHitLine As String
    Dim DepUrno As String
    Dim CnxUkey As String
    Dim CfiAurn As String
    Dim CfiDurn As String
    Dim CnxRec As String
    Dim EmpRec As String
    Dim CnxFldLst As String
    Dim CfiLineNo As Long
    Dim CnxLineNo As Long
    Dim CfiValue As Integer
    Dim CnxValue As Integer
    'Note: Currently the function assumes that the
    'categories PAX/BAG/ULD are in different grids!
    'When the layout of the main dialog combines
    'the Cfitab records in one grid then we must
    'change the workflow and use the CFI.TYPE field
    'in order to separate the the PAX/BAG/ULD data.
    IsCnxFlight = retval
    If ArrUrno <> "" Then
        'FieldList = "UKEY,AURN,DURN,PAXT,PAX1,PAX2,PAX3,PAX4,BAGT,BAG1,BAG2,BAG3,BAG4,ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,TIFA,ADDI"
        EmpRec = CreateEmptyLine(TabArrCnxDetails.LogicalFieldList)
        'Cut off the first three item values (2 commas)
        EmpRec = Mid(EmpRec, 3)
        CfiAurn = Right("0000000000" & ArrUrno, 10)
        
        'Note: Each record of CFITAB already contains totals per class and type.
        'But there may be more than one PAX (or BAG/ULD) record for a given connection
        'because e.g. the transfer PAX information can be related to a code share flight.
        
        'Here we have to summarize the totals of the operating flights.
        'Thus we must run through all ARR records with the same DURN.
        'Workflow: We run through all CFI Arrival flights and
        'create a record in CnxDetails for each connection.
        'If the connex record already exists then add the new values
        'to the existing fields and update them.
        
        '1.) Collect PAX values per class and total.
        If TabArrCnxPaxIdx >= 0 Then
            TabArrCnxPaxTab.SetInternalLineBuffer True
            CfiHitLine = TabArrCnxPaxTab.GetLinesByIndexValue("AURN", ArrUrno, 0)
            If Val(CfiHitLine) > 0 Then
                IsCnxFlight = True
                CnxFldLst = "PAXT,PAX1,PAX2,PAX3,PAX4,SCID,CTMT,TIFA"
                CfiLineNo = TabArrCnxPaxTab.GetNextResultLine
                While CfiLineNo >= 0
                    DepUrno = TabArrCnxPaxTab.GetFieldValue(CfiLineNo, "DURN")
                    CfiDurn = Right("0000000000" & DepUrno, 10)
                    CnxUkey = CfiAurn & CfiDurn
                    CnxHitLine = TabArrCnxDetails.GetLinesByColumnValue(0, CnxUkey, 0)
                    If CnxHitLine <> "" Then
                        CnxLineNo = Val(CnxHitLine)
                    Else
                        CnxRec = CnxUkey & "," & CfiAurn & "," & CfiDurn & EmpRec
                        TabArrCnxDetails.InsertTextLine CnxRec, False
                        CnxLineNo = TabArrCnxDetails.GetLineCount - 1
                    End If
                    CnxRec = ""
                    CfiValue = Val(TabArrCnxPaxTab.GetFieldValue(CfiLineNo, "TALL"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "PAXT"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxPaxTab.GetFieldValue(CfiLineNo, "TCLF"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "PAX1"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxPaxTab.GetFieldValue(CfiLineNo, "TCLC"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "PAX2"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxPaxTab.GetFieldValue(CfiLineNo, "TCLY"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "PAX3"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxPaxTab.GetFieldValue(CfiLineNo, "TOTH"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "PAX4"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CnxRec = CnxRec & TabArrCnxPaxTab.GetFieldValues(CfiLineNo, "SCID,CTMT,TIFA")
                    TabArrCnxDetails.SetFieldValues CnxLineNo, CnxFldLst, CnxRec
                    CfiLineNo = TabArrCnxPaxTab.GetNextResultLine
                Wend
            End If
            TabArrCnxPaxTab.SetInternalLineBuffer False
        End If
        
        '2.) Collect BAG values per class and total.
        If TabArrCnxBagIdx >= 0 Then
            TabArrCnxBagTab.SetInternalLineBuffer True
            CfiHitLine = TabArrCnxBagTab.GetLinesByIndexValue("AURN", ArrUrno, 0)
            If Val(CfiHitLine) > 0 Then
                IsCnxFlight = True
                CnxFldLst = "BAGT,BAG1,BAG2,BAG3,BAG4,SCID,CTMT,TIFA"
                CfiLineNo = TabArrCnxBagTab.GetNextResultLine
                While CfiLineNo >= 0
                    DepUrno = TabArrCnxBagTab.GetFieldValue(CfiLineNo, "DURN")
                    CfiDurn = Right("0000000000" & DepUrno, 10)
                    CnxUkey = CfiAurn & CfiDurn
                    CnxHitLine = TabArrCnxDetails.GetLinesByColumnValue(0, CnxUkey, 0)
                    If CnxHitLine <> "" Then
                        CnxLineNo = Val(CnxHitLine)
                    Else
                        CnxRec = CnxUkey & "," & CfiAurn & "," & CfiDurn & EmpRec
                        TabArrCnxDetails.InsertTextLine CnxRec, False
                        CnxLineNo = TabArrCnxDetails.GetLineCount - 1
                    End If
                    CnxRec = ""
                    CfiValue = Val(TabArrCnxBagTab.GetFieldValue(CfiLineNo, "TALL"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "BAGT"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxBagTab.GetFieldValue(CfiLineNo, "TCLF"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "BAG1"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxBagTab.GetFieldValue(CfiLineNo, "TCLC"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "BAG2"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxBagTab.GetFieldValue(CfiLineNo, "TCLY"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "BAG3"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxBagTab.GetFieldValue(CfiLineNo, "TOTH"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "BAG4"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CnxRec = CnxRec & TabArrCnxBagTab.GetFieldValues(CfiLineNo, "SCID,CTMT,TIFA")
                    TabArrCnxDetails.SetFieldValues CnxLineNo, CnxFldLst, CnxRec
                    CfiLineNo = TabArrCnxBagTab.GetNextResultLine
                Wend
            End If
            TabArrCnxBagTab.SetInternalLineBuffer False
        End If
        
        '3.) Collect ULD values per "class" and total.
        If TabArrCnxUldIdx >= 0 Then
            TabArrCnxUldTab.SetInternalLineBuffer True
            CfiHitLine = TabArrCnxUldTab.GetLinesByIndexValue("AURN", ArrUrno, 0)
            If Val(CfiHitLine) > 0 Then
                IsCnxFlight = True
                CnxFldLst = "ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,TIFA"
                CfiLineNo = TabArrCnxUldTab.GetNextResultLine
                While CfiLineNo >= 0
                    DepUrno = TabArrCnxUldTab.GetFieldValue(CfiLineNo, "DURN")
                    CfiDurn = Right("0000000000" & DepUrno, 10)
                    CnxUkey = CfiAurn & CfiDurn
                    CnxHitLine = TabArrCnxDetails.GetLinesByColumnValue(0, CnxUkey, 0)
                    If CnxHitLine <> "" Then
                        CnxLineNo = Val(CnxHitLine)
                    Else
                        CnxRec = CnxUkey & "," & CfiAurn & "," & CfiDurn & EmpRec
                        TabArrCnxDetails.InsertTextLine CnxRec, False
                        CnxLineNo = TabArrCnxDetails.GetLineCount - 1
                    End If
                    CnxRec = ""
                    CfiValue = Val(TabArrCnxUldTab.GetFieldValue(CfiLineNo, "TALL"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "ULDT"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxUldTab.GetFieldValue(CfiLineNo, "TCLF"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "ULD1"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxUldTab.GetFieldValue(CfiLineNo, "TCLC"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "ULD2"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxUldTab.GetFieldValue(CfiLineNo, "TCLY"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "ULD3"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabArrCnxUldTab.GetFieldValue(CfiLineNo, "TOTH"))
                    CnxValue = CfiValue + Val(TabArrCnxDetails.GetFieldValue(CnxLineNo, "ULD4"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CnxRec = CnxRec & TabArrCnxUldTab.GetFieldValues(CfiLineNo, "SCID,CTMT,TIFA")
                    TabArrCnxDetails.SetFieldValues CnxLineNo, CnxFldLst, CnxRec
                    CfiLineNo = TabArrCnxUldTab.GetNextResultLine
                Wend
            End If
            TabArrCnxUldTab.SetInternalLineBuffer False
        End If
    End If
    GetArrCnxDetails = IsCnxFlight
End Function

Private Function GetDepCnxDetails(DepUrno As String, retval As Boolean) As Boolean
    Dim IsCnxFlight As Boolean
    Dim CfiHitLine As String
    Dim CnxHitLine As String
    Dim ArrUrno As String
    Dim CnxUkey As String
    Dim CfiDurn As String
    Dim CfiAurn As String
    Dim CnxRec As String
    Dim EmpRec As String
    Dim CnxFldLst As String
    Dim CfiLineNo As Long
    Dim CnxLineNo As Long
    Dim CfiValue As Integer
    Dim CnxValue As Integer
    'Note: Currently the function assumes that the
    'categories PAX/BAG/ULD are in different grids!
    'When the layout of the main dialog combines
    'the Cfitab records in one grid then we must
    'change the workflow and use the CFI.TYPE field
    'in order to separate the the PAX/BAG/ULD data.
    IsCnxFlight = retval
    If DepUrno <> "" Then
        'FieldList = "UKEY,AURN,DURN,PAXT,PAX1,PAX2,PAX3,PAX4,BAGT,BAG1,BAG2,BAG3,BAG4,ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,TIFD,ADDI"
        EmpRec = CreateEmptyLine(TabDepCnxDetails.LogicalFieldList)
        'Cut off the first three item values (2 commas) for UKEY,AURN,DURN
        EmpRec = Mid(EmpRec, 3)
        CfiDurn = Right("0000000000" & DepUrno, 10)
        
        'Note: Each record of CFITAB already contains totals per class and type.
        'But there may be more than one PAX (or BAG/ULD) record for a given connection
        'because e.g. the transfer PAX information can be related to a code share flight.
        
        'Here we have to summarize the totals of the operating flights.
        'Thus we must run through all Dep records with the same DURN.
        'Workflow: We run through all CFI Departure flights and
        'create a record in CnxDetails for each connection.
        'If the connex record already exists then add the new values
        'to the existing fields and update them.
        
        '1.) Collect PAX values per class and total.
        If TabDepCnxPaxIdx >= 0 Then
            TabDepCnxPaxTab.SetInternalLineBuffer True
            CfiHitLine = TabDepCnxPaxTab.GetLinesByIndexValue("DURN", DepUrno, 0)
            If Val(CfiHitLine) > 0 Then
                IsCnxFlight = True
                CnxFldLst = "PAXT,PAX1,PAX2,PAX3,PAX4,SCID,CTMT,TIFD"
                CfiLineNo = TabDepCnxPaxTab.GetNextResultLine
                While CfiLineNo >= 0
                    ArrUrno = TabDepCnxPaxTab.GetFieldValue(CfiLineNo, "AURN")
                    CfiAurn = Right("0000000000" & ArrUrno, 10)
                    CnxUkey = CfiAurn & CfiDurn
                    CnxHitLine = TabDepCnxDetails.GetLinesByColumnValue(0, CnxUkey, 0)
                    If CnxHitLine <> "" Then
                        CnxLineNo = Val(CnxHitLine)
                    Else
                        CnxRec = CnxUkey & "," & CfiAurn & "," & CfiDurn & EmpRec
                        TabDepCnxDetails.InsertTextLine CnxRec, False
                        CnxLineNo = TabDepCnxDetails.GetLineCount - 1
                    End If
                    CnxRec = ""
                    CfiValue = Val(TabDepCnxPaxTab.GetFieldValue(CfiLineNo, "TALL"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "PAXT"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxPaxTab.GetFieldValue(CfiLineNo, "TCLF"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "PAX1"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxPaxTab.GetFieldValue(CfiLineNo, "TCLC"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "PAX2"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxPaxTab.GetFieldValue(CfiLineNo, "TCLY"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "PAX3"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxPaxTab.GetFieldValue(CfiLineNo, "TOTH"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "PAX4"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CnxRec = CnxRec & TabDepCnxPaxTab.GetFieldValues(CfiLineNo, "SCID,CTMT,TIFD")
                    TabDepCnxDetails.SetFieldValues CnxLineNo, CnxFldLst, CnxRec
                    CfiLineNo = TabDepCnxPaxTab.GetNextResultLine
                Wend
            End If
            TabDepCnxPaxTab.SetInternalLineBuffer False
        End If
        
        '2.) Collect BAG values per class and total.
        If TabDepCnxBagIdx >= 0 Then
            TabDepCnxBagTab.SetInternalLineBuffer True
            CfiHitLine = TabDepCnxBagTab.GetLinesByIndexValue("DURN", DepUrno, 0)
            If Val(CfiHitLine) > 0 Then
                IsCnxFlight = True
                CnxFldLst = "BAGT,BAG1,BAG2,BAG3,BAG4,SCID,CTMT,TIFD"
                CfiLineNo = TabDepCnxBagTab.GetNextResultLine
                While CfiLineNo >= 0
                    ArrUrno = TabDepCnxBagTab.GetFieldValue(CfiLineNo, "AURN")
                    CfiAurn = Right("0000000000" & ArrUrno, 10)
                    CnxUkey = CfiAurn & CfiDurn
                    CnxHitLine = TabDepCnxDetails.GetLinesByColumnValue(0, CnxUkey, 0)
                    If CnxHitLine <> "" Then
                        CnxLineNo = Val(CnxHitLine)
                    Else
                        CnxRec = CnxUkey & "," & CfiAurn & "," & CfiDurn & EmpRec
                        TabDepCnxDetails.InsertTextLine CnxRec, False
                        CnxLineNo = TabDepCnxDetails.GetLineCount - 1
                    End If
                    CnxRec = ""
                    CfiValue = Val(TabDepCnxBagTab.GetFieldValue(CfiLineNo, "TALL"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "BAGT"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxBagTab.GetFieldValue(CfiLineNo, "TCLF"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "BAG1"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxBagTab.GetFieldValue(CfiLineNo, "TCLC"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "BAG2"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxBagTab.GetFieldValue(CfiLineNo, "TCLY"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "BAG3"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxBagTab.GetFieldValue(CfiLineNo, "TOTH"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "BAG4"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CnxRec = CnxRec & TabDepCnxBagTab.GetFieldValues(CfiLineNo, "SCID,CTMT,TIFD")
                    TabDepCnxDetails.SetFieldValues CnxLineNo, CnxFldLst, CnxRec
                    CfiLineNo = TabDepCnxBagTab.GetNextResultLine
                Wend
            End If
            TabDepCnxBagTab.SetInternalLineBuffer False
        End If
        
        '3.) Collect ULD values per "class" and total.
        If TabDepCnxUldIdx >= 0 Then
            TabDepCnxUldTab.SetInternalLineBuffer True
            CfiHitLine = TabDepCnxUldTab.GetLinesByIndexValue("DURN", DepUrno, 0)
            If Val(CfiHitLine) > 0 Then
                IsCnxFlight = True
                CnxFldLst = "ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,TIFD"
                CfiLineNo = TabDepCnxUldTab.GetNextResultLine
                While CfiLineNo >= 0
                    ArrUrno = TabDepCnxUldTab.GetFieldValue(CfiLineNo, "AURN")
                    CfiAurn = Right("0000000000" & ArrUrno, 10)
                    CnxUkey = CfiAurn & CfiDurn
                    CnxHitLine = TabDepCnxDetails.GetLinesByColumnValue(0, CnxUkey, 0)
                    If CnxHitLine <> "" Then
                        CnxLineNo = Val(CnxHitLine)
                    Else
                        CnxRec = CnxUkey & "," & CfiAurn & "," & CfiDurn & EmpRec
                        TabDepCnxDetails.InsertTextLine CnxRec, False
                        CnxLineNo = TabDepCnxDetails.GetLineCount - 1
                    End If
                    CnxRec = ""
                    CfiValue = Val(TabDepCnxUldTab.GetFieldValue(CfiLineNo, "TALL"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "ULDT"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxUldTab.GetFieldValue(CfiLineNo, "TCLF"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "ULD1"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxUldTab.GetFieldValue(CfiLineNo, "TCLC"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "ULD2"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxUldTab.GetFieldValue(CfiLineNo, "TCLY"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "ULD3"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CfiValue = Val(TabDepCnxUldTab.GetFieldValue(CfiLineNo, "TOTH"))
                    CnxValue = CfiValue + Val(TabDepCnxDetails.GetFieldValue(CnxLineNo, "ULD4"))
                    CnxRec = CnxRec & CStr(CnxValue) & ","
                    CnxRec = CnxRec & TabDepCnxUldTab.GetFieldValues(CfiLineNo, "SCID,CTMT,TIFD")
                    TabDepCnxDetails.SetFieldValues CnxLineNo, CnxFldLst, CnxRec
                    CfiLineNo = TabDepCnxUldTab.GetNextResultLine
                Wend
            End If
            TabDepCnxUldTab.SetInternalLineBuffer False
        End If
    End If
    GetDepCnxDetails = IsCnxFlight
End Function

Private Sub GetArrFlightData(LineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim BgnLine As Long
    Dim iBar As Integer
    Dim ArrFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim tmpTag As String
    Dim ArrFldIdx As Integer
    Dim TimeOffs As Integer
    Dim BarColor As Long
    Dim FldColor As Long
    If chkUtc(1).Value = 1 Then
        TimeOffs = UtcTimeDiff
    Else
        TimeOffs = 0
    End If
    If LineNo >= 0 Then
        BgnLine = LineNo
        MaxLine = LineNo
        iBar = BarIdx
    Else
        If BarIdx >= 0 Then
            iBar = BarIdx
            'ClearArrFlight iBar
            BgnLine = 0
            MaxLine = -1
        Else
            BgnLine = 0
            MaxLine = TabArrFlightsTab.GetLineCount - 1
            iBar = 0
        End If
    End If
    ArrFields = "FTYP,FLNO,FLTI,ORG3,VIA3,STOA,ETAI,ONBL,PSTA,GTA1,REGN,ACT3,TIFA,TISA,'C3'"
    For CurLine = BgnLine To MaxLine
        ClearArrFlight iBar
        ArrFldIdx = 1
        FldName = "START"
        While FldName <> ""
            FldName = GetItem(ArrFields, ArrFldIdx, ",")
            If FldName <> "" Then
                FldValue = Trim(TabArrFlightsTab.GetFieldValue(CurLine, FldName))
                Select Case FldName
                    Case "FTYP"
                        'lblArrFtyp(iBar).Tag = FldValue
                        'lblArrFtyp(iBar).Caption = FldValue
                        'Select Case FldValue
                        '    Case "X"
                        '        lblArrBar(iBar).Caption = "CXX"
                        '        lblArrBar(iBar).Width = 600
                        '        BarColor = LightGrey
                        '    Case "N"
                        '        lblArrBar(iBar).Caption = "NOP"
                        '        lblArrBar(iBar).Width = 600
                        '        BarColor = LightGrey
                        '    Case "D"
                        '        lblArrBar(iBar).Caption = "DIV"
                        '        lblArrBar(iBar).Width = 600
                        '        BarColor = LightGrey
                        '    Case Else
                        '        lblArrBar(iBar).Caption = ""
                        '        'BarColor = LightGrey
                        'End Select
                    Case "FLNO"
                        lblArrFlno(iBar).Tag = FldValue
                        lblArrFlno(iBar).Caption = FldValue
                    Case "FLTI"
                        lblArrFlti(iBar).Tag = FldValue
                        lblArrFlti(iBar).Caption = FldValue
                    Case "ORG3"
                        lblArrOrg3(iBar).Tag = FldValue
                        lblArrOrg3(iBar).Caption = FldValue
                    Case "VIA3"
                        lblArrVia3(iBar).Tag = FldValue
                        lblArrVia3(iBar).Caption = FldValue
                    Case "STOA"
                        lblArrStoa(iBar).Tag = FldValue
                        SetTimeFieldCaption lblArrStoa(iBar), TimeOffs
                        If FldValue <> "" Then BarColor = LightGrey
                    Case "ETAI"
                        lblArrEtai(iBar).Tag = FldValue
                        SetTimeFieldCaption lblArrEtai(iBar), TimeOffs
                        If FldValue <> "" Then BarColor = vbWhite
                    Case "ONBL"
                        lblArrOnbl(iBar).Tag = FldValue
                        SetTimeFieldCaption lblArrOnbl(iBar), TimeOffs
                        If FldValue <> "" Then BarColor = vbGreen
                    Case "PSTA"
                        lblArrPsta(iBar).Tag = FldValue
                        lblArrPsta(iBar).Caption = FldValue
                    Case "GTA1"
                        'lblArrGta1(iBar).Tag = FldValue
                        'lblArrGta1(iBar).Caption = FldValue
                    Case "REGN"
                        lblArrRegn(iBar).Tag = FldValue
                        lblArrRegn(iBar).Caption = FldValue
                    Case "ACT3"
                        lblArrAct3(iBar).Tag = FldValue
                        lblArrAct3(iBar).Caption = FldValue
                    Case "TIFA"
                        tmpTag = "{AFTTIFA}" & FldValue & "{/AFTTIFA}"
                        ArrFlight(iBar).Tag = tmpTag
                    Case "TISA"
                        tmpTag = ArrFlight(iBar).Tag
                        tmpTag = tmpTag & "{AFTTISA}" & FldValue & "{/AFTTISA}"
                        ArrFlight(iBar).Tag = tmpTag
                    Case "'C3'"
                        If FldValue = "S" Then
                            picArrStatus(iBar).Picture = picMarker(1).Picture
                            picArrStatus(iBar).Tag = "1"
                        Else
                            picArrStatus(iBar).Picture = picMarker(0).Picture
                            picArrStatus(iBar).Tag = "0"
                        End If
                    Case Else
                End Select
            End If
            ArrFldIdx = ArrFldIdx + 1
        Wend
        'lblArrBar(iBar).BackColor = BarColor
        ArrFlight(iBar).Visible = True
        iBar = iBar + 1
    Next
End Sub
Private Sub GetArrCnxTotals(ArrUrno As String, iBar As Integer)
    Dim HitLines As String
    Dim CnxAurn As String
    Dim tmpData As String
    Dim tmpTifa As String
    Dim tmpTag As String
    Dim CnxCtmt As String
    Dim FrmCtmt As String
    Dim LineNo As Long
    Dim CurCTim As Long
    Dim MinCtim As Long
    Dim PaxTot1 As Integer
    Dim PaxTot2 As Integer
    Dim PaxTot3 As Integer
    Dim PaxTot4 As Integer
    Dim PaxTots As Integer
    Dim BagTot1 As Integer
    Dim BagTot2 As Integer
    Dim BagTot3 As Integer
    Dim BagTot4 As Integer
    Dim BagTots As Integer
    Dim UldTot1 As Integer
    Dim UldTot2 As Integer
    Dim UldTot3 As Integer
    Dim UldTot4 As Integer
    Dim UldTots As Integer
    Dim IsShort As Boolean
    Dim IsCritical As Boolean
    'The related grid contains the totals per connection.
    'Here we now calculate the totals per arr flight.
    IsShort = False
    IsCritical = False
    MinCtim = 999999
    PaxTot1 = 0
    PaxTot2 = 0
    PaxTot3 = 0
    PaxTot4 = 0
    BagTot1 = 0
    BagTot2 = 0
    BagTot3 = 0
    BagTot4 = 0
    UldTot1 = 0
    UldTot2 = 0
    UldTot3 = 0
    UldTot4 = 0
    tmpTifa = ""
    CnxCtmt = ""
    FrmCtmt = ""
    CnxAurn = Right("0000000000" & ArrUrno, 10)
    TabArrCnxDetails.SetInternalLineBuffer True
    HitLines = TabArrCnxDetails.GetLinesByColumnValue(1, CnxAurn, 0)
    If Val(HitLines) > 0 Then
        LineNo = TabArrCnxDetails.GetNextResultLine
        While LineNo >= 0
            'FieldList = "UKEY,AURN,DURN,PAXT,PAX1,PAX2,PAX3,PAX4,BAGT,BAG1,BAG2,BAG3,BAG4,ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,ADDI"
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "PAX1")
            PaxTot1 = PaxTot1 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "PAX2")
            PaxTot2 = PaxTot2 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "PAX3")
            PaxTot3 = PaxTot3 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "PAX4")
            PaxTot4 = PaxTot4 + Val(tmpData)
            
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "BAG1")
            BagTot1 = BagTot1 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "BAG2")
            BagTot2 = BagTot2 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "BAG3")
            BagTot3 = BagTot3 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "BAG4")
            BagTot4 = BagTot4 + Val(tmpData)
            
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "ULD1")
            UldTot1 = UldTot1 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "ULD2")
            UldTot2 = UldTot2 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "ULD3")
            UldTot3 = UldTot3 + Val(tmpData)
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "ULD4")
            UldTot4 = UldTot4 + Val(tmpData)
            
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "SCID")
            If tmpData = "X" Then IsShort = True
            If tmpData = "C" Then IsCritical = True
            tmpData = TabArrCnxDetails.GetFieldValue(LineNo, "CTMT")
            If tmpData <> "" Then
                FrmCtmt = tmpData
                tmpData = Replace(tmpData, "H", ":", 1, -1, vbBinaryCompare)
                If InStr(tmpData, ":") > 0 Then
                    If Left(tmpData, 1) = "-" Then
                        CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) - Val(GetItem(tmpData, 2, ":"))
                    Else
                        CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) + Val(GetItem(tmpData, 2, ":"))
                    End If
                Else
                    CurCTim = Val(tmpData)
                End If
                If (CurCTim > 0) And (CurCTim < MinCtim) Then
                    MinCtim = CurCTim
                    CnxCtmt = FrmCtmt
                End If
            End If
            If tmpTifa = "" Then
                tmpTag = ArrFlight(iBar).Tag
                tmpTifa = TabArrCnxDetails.GetFieldValue(LineNo, "TIFA")
                If tmpTifa = "" Then
                    GetKeyItem tmpTifa, tmpTag, "{AFTTIFA}", "{/AFTTIFA}"
                    tmpTag = tmpTag & "{CFITIFA}" & tmpTifa & "{/CFITIFA}"
                    ArrFlight(iBar).Tag = tmpTag
                End If
            End If
            LineNo = TabArrCnxDetails.GetNextResultLine
        Wend
    End If
    TabArrCnxDetails.SetInternalLineBuffer False
    
    PaxTots = PaxTot1 + PaxTot2 + PaxTot3 + PaxTot4
    BagTots = BagTot1 + BagTot2 + BagTot3 + BagTot4
    UldTots = UldTot1 + UldTot2 + UldTot3 + UldTot4

    lblArrTotPax(iBar).Tag = CStr(PaxTots)
    If PaxTots > 0 Then
        lblArrTotPax(iBar).Caption = lblArrTotPax(iBar).Tag
        lblArrTotPax(iBar).MousePointer = 99
    Else
        lblArrTotPax(iBar).Caption = ""
        lblArrTotPax(iBar).MousePointer = 0
    End If
    lblArrTotBag(iBar).Tag = CStr(BagTots)
    If BagTots > 0 Then
        lblArrTotBag(iBar).Caption = lblArrTotBag(iBar).Tag
        lblArrTotBag(iBar).MousePointer = 99
    Else
        lblArrTotBag(iBar).Caption = ""
        lblArrTotBag(iBar).MousePointer = 0
    End If
    lblArrTotUld(iBar).Tag = CStr(UldTots)
    If UldTots > 0 Then
        lblArrTotUld(iBar).Caption = lblArrTotUld(iBar).Tag
        lblArrTotUld(iBar).MousePointer = 99
    Else
        lblArrTotUld(iBar).Caption = ""
        lblArrTotUld(iBar).MousePointer = 0
    End If


    lblArrTot1(iBar).Tag = CStr(PaxTot1)
    If PaxTot1 > 0 Then
        lblArrTot1(iBar).Caption = lblArrTot1(iBar).Tag
        lblArrTot1(iBar).MousePointer = 99
    Else
        lblArrTot1(iBar).Caption = ""
        lblArrTot1(iBar).MousePointer = 0
    End If
    lblArrTot2(iBar).Tag = CStr(PaxTot2)
    If PaxTot2 > 0 Then
        lblArrTot2(iBar).Caption = lblArrTot2(iBar).Tag
        lblArrTot2(iBar).MousePointer = 99
    Else
        lblArrTot2(iBar).Caption = ""
        lblArrTot2(iBar).MousePointer = 0
    End If
    lblArrTot3(iBar).Tag = CStr(PaxTot3)
    If PaxTot3 > 0 Then
        lblArrTot3(iBar).Caption = lblArrTot3(iBar).Tag
        lblArrTot3(iBar).MousePointer = 99
    Else
        lblArrTot3(iBar).Caption = ""
        lblArrTot3(iBar).MousePointer = 0
    End If
    lblArrTot4(iBar).Tag = CStr(PaxTot4)
    If PaxTot4 > 0 Then
        lblArrTot4(iBar).Caption = lblArrTot4(iBar).Tag
        lblArrTot4(iBar).MousePointer = 99
    Else
        lblArrTot4(iBar).Caption = ""
        lblArrTot4(iBar).MousePointer = 0
    End If

    lblArrCsct(iBar).BackColor = vbWhite
    lblArrCsct(iBar).ForeColor = vbBlack
    lblArrCsct(iBar).Caption = ""
    lblArrCsct(iBar).MousePointer = 0
    If MinCtim < 999999 Then
        lblArrCsct(iBar).Caption = CnxCtmt
        If IsShort Then
            lblArrCsct(iBar).BackColor = vbRed
            lblArrCsct(iBar).ForeColor = vbWhite
            lblArrCsct(iBar).MousePointer = 99
        End If
        If IsCritical Then
            lblArrCsct(iBar).BackColor = vbCyan
            lblArrCsct(iBar).ForeColor = vbBlack
            lblArrCsct(iBar).MousePointer = 99
        End If
    End If
    lblArrCsct(iBar).Tag = "CNX"
End Sub

Private Sub GetDepCnxTotals(DepUrno As String, iBar As Integer)
    Dim HitLines As String
    Dim CnxDurn As String
    Dim tmpData As String
    Dim tmpTifd As String
    Dim CnxCtmt As String
    Dim FrmCtmt As String
    Dim tmpTag As String
    Dim LineNo As Long
    Dim CurCTim As Long
    Dim MinCtim As Long
    Dim PaxTot1 As Integer
    Dim PaxTot2 As Integer
    Dim PaxTot3 As Integer
    Dim PaxTot4 As Integer
    Dim PaxTots As Integer
    Dim BagTot1 As Integer
    Dim BagTot2 As Integer
    Dim BagTot3 As Integer
    Dim BagTot4 As Integer
    Dim BagTots As Integer
    Dim UldTot1 As Integer
    Dim UldTot2 As Integer
    Dim UldTot3 As Integer
    Dim UldTot4 As Integer
    Dim UldTots As Integer
    Dim IsShort As Boolean
    Dim IsCritical As Boolean
    'The related grid contains the totals per connection.
    'Here we now calculate the totals per Dep flight.
    IsShort = False
    MinCtim = 999999
    PaxTot1 = 0
    PaxTot2 = 0
    PaxTot3 = 0
    PaxTot4 = 0
    BagTot1 = 0
    BagTot2 = 0
    BagTot3 = 0
    BagTot4 = 0
    UldTot1 = 0
    UldTot2 = 0
    UldTot3 = 0
    UldTot4 = 0
    tmpTifd = ""
    CnxCtmt = ""
    FrmCtmt = ""
    CnxDurn = Right("0000000000" & DepUrno, 10)
    TabDepCnxDetails.SetInternalLineBuffer True
    HitLines = TabDepCnxDetails.GetLinesByColumnValue(2, CnxDurn, 0)
    If Val(HitLines) > 0 Then
        LineNo = TabDepCnxDetails.GetNextResultLine
        While LineNo >= 0
            'FieldList = "UKEY,AURN,DURN,PAXT,PAX1,PAX2,PAX3,PAX4,BAGT,BAG1,BAG2,BAG3,BAG4,ULDT,ULD1,ULD2,ULD3,ULD4,SCID,CTMT,ADDI"
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "PAX1")
            PaxTot1 = PaxTot1 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "PAX2")
            PaxTot2 = PaxTot2 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "PAX3")
            PaxTot3 = PaxTot3 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "PAX4")
            PaxTot4 = PaxTot4 + Val(tmpData)
            
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "BAG1")
            BagTot1 = BagTot1 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "BAG2")
            BagTot2 = BagTot2 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "BAG3")
            BagTot3 = BagTot3 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "BAG4")
            BagTot4 = BagTot4 + Val(tmpData)
            
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "ULD1")
            UldTot1 = UldTot1 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "ULD2")
            UldTot2 = UldTot2 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "ULD3")
            UldTot3 = UldTot3 + Val(tmpData)
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "ULD4")
            UldTot4 = UldTot4 + Val(tmpData)
            
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "SCID")
            If tmpData = "X" Then IsShort = True
            If tmpData = "C" Then IsCritical = True
            tmpData = TabDepCnxDetails.GetFieldValue(LineNo, "CTMT")
            If tmpData <> "" Then
                FrmCtmt = tmpData
                tmpData = Replace(tmpData, "H", ":", 1, -1, vbBinaryCompare)
                If InStr(tmpData, ":") > 0 Then
                    If Left(tmpData, 1) = "-" Then
                        CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) - Val(GetItem(tmpData, 2, ":"))
                    Else
                        CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) + Val(GetItem(tmpData, 2, ":"))
                    End If
                Else
                    CurCTim = Val(tmpData)
                End If
                If (CurCTim > 0) And (CurCTim < MinCtim) Then
                    MinCtim = CurCTim
                    CnxCtmt = FrmCtmt
                End If
            End If
            If tmpTifd = "" Then
                tmpTag = DepFlight(iBar).Tag
                tmpTifd = TabDepCnxDetails.GetFieldValue(LineNo, "TIFD")
                If tmpTifd = "" Then
                    GetKeyItem tmpTifd, tmpTag, "{AFTTIFD}", "{/AFTTIFD}"
                    tmpTag = tmpTag & "{CFITIFD}" & tmpTifd & "{/CFITIFD}"
                    DepFlight(iBar).Tag = tmpTag
                End If
            End If
            LineNo = TabDepCnxDetails.GetNextResultLine
        Wend
    End If
    TabDepCnxDetails.SetInternalLineBuffer False
    
    PaxTots = PaxTot1 + PaxTot2 + PaxTot3 + PaxTot4
    BagTots = BagTot1 + BagTot2 + BagTot3 + BagTot4
    UldTots = UldTot1 + UldTot2 + UldTot3 + UldTot4

    lblDepTotPax(iBar).Tag = CStr(PaxTots)
    If PaxTots > 0 Then
        lblDepTotPax(iBar).Caption = lblDepTotPax(iBar).Tag
        lblDepTotPax(iBar).MousePointer = 99
    Else
        lblDepTotPax(iBar).Caption = ""
        lblDepTotPax(iBar).MousePointer = 0
    End If
    lblDepTotBag(iBar).Tag = CStr(BagTots)
    If BagTots > 0 Then
        lblDepTotBag(iBar).Caption = lblDepTotBag(iBar).Tag
        lblDepTotBag(iBar).MousePointer = 99
    Else
        lblDepTotBag(iBar).Caption = ""
        lblDepTotBag(iBar).MousePointer = 0
    End If
    lblDepTotUld(iBar).Tag = CStr(UldTots)
    If UldTots > 0 Then
        lblDepTotUld(iBar).Caption = lblDepTotUld(iBar).Tag
        lblDepTotUld(iBar).MousePointer = 99
    Else
        lblDepTotUld(iBar).Caption = ""
        lblDepTotUld(iBar).MousePointer = 0
    End If


    lblDepTot1(iBar).Tag = CStr(PaxTot1)
    If PaxTot1 > 0 Then
        lblDepTot1(iBar).Caption = lblDepTot1(iBar).Tag
        lblDepTot1(iBar).MousePointer = 99
    Else
        lblDepTot1(iBar).Caption = ""
        lblDepTot1(iBar).MousePointer = 0
    End If
    lblDepTot2(iBar).Tag = CStr(PaxTot2)
    If PaxTot2 > 0 Then
        lblDepTot2(iBar).Caption = lblDepTot2(iBar).Tag
        lblDepTot2(iBar).MousePointer = 99
    Else
        lblDepTot2(iBar).Caption = ""
        lblDepTot2(iBar).MousePointer = 0
    End If
    lblDepTot3(iBar).Tag = CStr(PaxTot3)
    If PaxTot3 > 0 Then
        lblDepTot3(iBar).Caption = lblDepTot3(iBar).Tag
        lblDepTot3(iBar).MousePointer = 99
    Else
        lblDepTot3(iBar).Caption = ""
        lblDepTot3(iBar).MousePointer = 0
    End If
    lblDepTot4(iBar).Tag = CStr(PaxTot4)
    If PaxTot4 > 0 Then
        lblDepTot4(iBar).Caption = lblDepTot4(iBar).Tag
        lblDepTot4(iBar).MousePointer = 99
    Else
        lblDepTot4(iBar).Caption = ""
        lblDepTot4(iBar).MousePointer = 0
    End If

    lblDepCsct(iBar).BackColor = vbWhite
    lblDepCsct(iBar).ForeColor = vbBlack
    lblDepCsct(iBar).Caption = ""
    lblDepCsct(iBar).MousePointer = 0
    If MinCtim < 999999 Then
        lblDepCsct(iBar).Caption = CnxCtmt
        If IsShort Then
            lblDepCsct(iBar).BackColor = vbRed
            lblDepCsct(iBar).ForeColor = vbWhite
            lblDepCsct(iBar).MousePointer = 99
        End If
        If IsCritical Then
            lblDepCsct(iBar).BackColor = vbCyan
            lblDepCsct(iBar).ForeColor = vbBlack
            lblDepCsct(iBar).MousePointer = 99
        End If
    End If
    lblDepCsct(iBar).Tag = "CNX"
End Sub

Private Sub GetDepFlightData(LineNo As Long, BarIdx As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim BgnLine As Long
    Dim iBar As Integer
    Dim DepFields As String
    Dim FldName As String
    Dim FldValue As String
    Dim tmpTag As String
    Dim DepFldIdx As Integer
    Dim TimeOffs As Integer
    If chkUtc(1).Value = 1 Then
        TimeOffs = UtcTimeDiff
    Else
        TimeOffs = 0
    End If
    If LineNo >= 0 Then
        BgnLine = LineNo
        MaxLine = LineNo
        iBar = BarIdx
    Else
        If BarIdx >= 0 Then
            iBar = BarIdx
            'ClearDepFlight iBar
            BgnLine = 0
            MaxLine = -1
        Else
            BgnLine = 0
            MaxLine = TabDepFlightsTab.GetLineCount - 1
            iBar = 0
        End If
    End If
    DepFields = "FTYP,FLNO,FLTI,DES3,VIA3,STOD,ETDI,OFBL,PSTD,GTD1,REGN,ACT3,TIFD,TISD,'C3'"
    For CurLine = BgnLine To MaxLine
        ClearDepFlight iBar
        DepFldIdx = 1
        FldName = "START"
        While FldName <> ""
            FldName = GetItem(DepFields, DepFldIdx, ",")
            If FldName <> "" Then
                FldValue = TabDepFlightsTab.GetFieldValue(CurLine, FldName)
                Select Case FldName
                    Case "FTYP"
                        'lblDepFtyp(iBar).Tag = FldValue
                        'lblDepFtyp(iBar).Caption = FldValue
                    Case "FLNO"
                        lblDepFlno(iBar).Tag = FldValue
                        lblDepFlno(iBar).Caption = FldValue
                    Case "FLTI"
                        lblDepFlti(iBar).Tag = FldValue
                        lblDepFlti(iBar).Caption = FldValue
                    Case "DES3"
                        lblDepDes3(iBar).Tag = FldValue
                        lblDepDes3(iBar).Caption = FldValue
                    Case "VIA3"
                        lblDepVia3(iBar).Tag = FldValue
                        lblDepVia3(iBar).Caption = FldValue
                    Case "STOD"
                        lblDepStod(iBar).Tag = FldValue
                        SetTimeFieldCaption lblDepStod(iBar), TimeOffs
                    Case "ETDI"
                        lblDepEtdi(iBar).Tag = FldValue
                        SetTimeFieldCaption lblDepEtdi(iBar), TimeOffs
                    Case "OFBL"
                        lblDepOfbl(iBar).Tag = FldValue
                        SetTimeFieldCaption lblDepOfbl(iBar), TimeOffs
                    Case "PSTD"
                        lblDepPstd(iBar).Tag = FldValue
                        lblDepPstd(iBar).Caption = FldValue
                    Case "GTD1"
                        'lblDepGtd1(iBar).Tag = FldValue
                        'lblDepGtd1(iBar).Caption = FldValue
                    Case "REGN"
                        lblDepRegn(iBar).Tag = FldValue
                        lblDepRegn(iBar).Caption = FldValue
                    Case "ACT3"
                        lblDepAct3(iBar).Tag = FldValue
                        lblDepAct3(iBar).Caption = FldValue
                    Case "TIFD"
                        tmpTag = "{AFTTIFD}" & FldValue & "{/AFTTIFD}"
                        DepFlight(iBar).Tag = tmpTag
                    Case "TISD"
                        tmpTag = DepFlight(iBar).Tag
                        tmpTag = tmpTag & "{AFTTISD}" & FldValue & "{/AFTTISD}"
                        DepFlight(iBar).Tag = tmpTag
                    Case "'C3'"
                        If FldValue = "S" Then
                            picDepStatus(iBar).Picture = picMarker(1).Picture
                            picDepStatus(iBar).Tag = "1"
                        Else
                            picDepStatus(iBar).Picture = picMarker(0).Picture
                            picDepStatus(iBar).Tag = "0"
                        End If
                    Case Else
                End Select
            End If
            DepFldIdx = DepFldIdx + 1
        Wend
        DepFlight(iBar).Visible = True
        iBar = iBar + 1
    Next
End Sub

Private Sub ClearArrFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    ArrFlight(iBar).Tag = FldValue
    'lblArrFtyp(iBar).Tag = FldValue
    'lblArrFtyp(iBar).Caption = FldValue
    lblArrFlno(iBar).Tag = FldValue
    lblArrFlno(iBar).Caption = FldValue
    lblArrFlti(iBar).Tag = FldValue
    lblArrFlti(iBar).Caption = FldValue
    lblArrOrg3(iBar).Tag = FldValue
    lblArrOrg3(iBar).Caption = FldValue
    lblArrVia3(iBar).Tag = FldValue
    lblArrVia3(iBar).Caption = FldValue
    lblArrStoa(iBar).Tag = FldValue
    lblArrStoa(iBar).Caption = FldValue
    lblArrEtai(iBar).Tag = FldValue
    lblArrEtai(iBar).Caption = FldValue
    lblArrOnbl(iBar).Tag = FldValue
    lblArrOnbl(iBar).Caption = FldValue
    lblArrPsta(iBar).Tag = FldValue
    lblArrPsta(iBar).Caption = FldValue
    'lblArrGta1(iBar).Tag = FldValue
    'lblArrGta1(iBar).Caption = FldValue
    lblArrRegn(iBar).Tag = FldValue
    lblArrRegn(iBar).Caption = FldValue
    lblArrAct3(iBar).Tag = FldValue
    lblArrAct3(iBar).Caption = FldValue
    lblArrTpax(iBar).Tag = FldValue
    lblArrTpax(iBar).Caption = FldValue
    lblArrTbag(iBar).Tag = FldValue
    lblArrTbag(iBar).Caption = FldValue
    lblArrTuld(iBar).Tag = FldValue
    lblArrTuld(iBar).Caption = FldValue
End Sub

Private Sub ClearDepFlight(iBar As Integer)
    Dim FldValue As String
    FldValue = ""
    DepFlight(iBar).Tag = FldValue
    'lblDepFtyp(iBar).Tag = FldValue
    'lblDepFtyp(iBar).Caption = FldValue
    lblDepFlno(iBar).Tag = FldValue
    lblDepFlno(iBar).Caption = FldValue
    lblDepFlti(iBar).Tag = FldValue
    lblDepFlti(iBar).Caption = FldValue
    lblDepDes3(iBar).Tag = FldValue
    lblDepDes3(iBar).Caption = FldValue
    lblDepVia3(iBar).Tag = FldValue
    lblDepVia3(iBar).Caption = FldValue
    lblDepStod(iBar).Tag = FldValue
    lblDepStod(iBar).Caption = FldValue
    lblDepEtdi(iBar).Tag = FldValue
    lblDepEtdi(iBar).Caption = FldValue
    lblDepOfbl(iBar).Tag = FldValue
    lblDepOfbl(iBar).Caption = FldValue
    lblDepPstd(iBar).Tag = FldValue
    lblDepPstd(iBar).Caption = FldValue
    'lblDepGtd1(iBar).Tag = FldValue
    'lblDepGtd1(iBar).Caption = FldValue
    lblDepRegn(iBar).Tag = FldValue
    lblDepRegn(iBar).Caption = FldValue
    lblDepAct3(iBar).Tag = FldValue
    lblDepAct3(iBar).Caption = FldValue
    lblDepTpax(iBar).Tag = FldValue
    lblDepTpax(iBar).Caption = FldValue
    lblDepTbag(iBar).Tag = FldValue
    lblDepTbag(iBar).Caption = FldValue
    lblDepTuld(iBar).Tag = FldValue
    lblDepTuld(iBar).Caption = FldValue
End Sub
Private Sub ShowAllConnexLines()
    Screen.MousePointer = 11
    ClearConnexDetails
    Select Case CnxFilterAdid
        Case 0
            ShowAllArrConnex
            LastCnxArrBar = -1
        Case 1
            ShowAllDepConnex
            LastCnxDepBar = -1
        Case Else
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub ShowConnexDetails(Index As Integer)
    Screen.MousePointer = 11
    Select Case CnxFilterAdid
        Case 0
            ShowArrConnexDetails Index
        Case 1
            ShowDepConnexDetails Index
        Case Else
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub ShowArrConnexDetails(Index As Integer)
    Static LastColor As Long
    Dim CurMouse As Integer
    Dim iBar As Integer
    Dim iArrBar As Integer
    Dim iDepBar As Integer
    Dim ArrUrno As String
    Dim CnxAurn As String
    Dim DepUrno As String
    Dim CurConx As String
    Dim RotHitLine As String
    Dim PaxHitLine As String
    Dim ArrRotLine As Long
    Dim DepRotLine As Long
    Dim PaxCfiLine As Long
    Dim LinColor As Long
    Dim NewPicTop As Long
    Dim NewBarTop As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim MaxWidth As Long
    Dim CurCTim As Long
    Dim IsShortest As Boolean
    Dim tmpData As String
    Dim tmpTag As String
    
    Dim tmpDepTag As String
    Dim tmpCnxTag As String
    
    'Variables for the current connection record
    Dim CnxPaxTot0 As Integer   'Total PAX
    Dim CnxPaxTot1 As Integer   'Class F
    Dim CnxPaxTot2 As Integer   'Class C
    Dim CnxPaxTot3 As Integer   'Class Y
    Dim CnxPaxTot4 As Integer   'Class U
    
    Dim CnxBagTot0 As Integer   'Total PAX
    Dim CnxBagTot1 As Integer   'Class F
    Dim CnxBagTot2 As Integer   'Class C
    Dim CnxBagTot3 As Integer   'Class Y
    Dim CnxBagTot4 As Integer   'Class U
    
    Dim CnxUldTot0 As Integer   'Total PAX
    Dim CnxUldTot1 As Integer   'Class F
    Dim CnxUldTot2 As Integer   'Class C
    Dim CnxUldTot3 As Integer   'Class Y
    Dim CnxUldTot4 As Integer   'Class U
    
    'We must calculate the totals per type and class
    'for each connection according to the active filters.
    
    'Variables for collecting totals related to
    'types and classes leaving the arrival flight:
    Dim ArrPaxTot0 As Integer   'Total PAX
    Dim ArrPaxTot1 As Integer   'Class F
    Dim ArrPaxTot2 As Integer   'Class C
    Dim ArrPaxTot3 As Integer   'Class Y
    Dim ArrPaxTot4 As Integer   'Class U
    
    Dim ArrBagTot0 As Integer   'Total PAX
    Dim ArrBagTot1 As Integer   'Class F
    Dim ArrBagTot2 As Integer   'Class C
    Dim ArrBagTot3 As Integer   'Class Y
    Dim ArrBagTot4 As Integer   'Class U
    
    Dim ArrUldTot0 As Integer   'Total PAX
    Dim ArrUldTot1 As Integer   'Class F
    Dim ArrUldTot2 As Integer   'Class C
    Dim ArrUldTot3 As Integer   'Class Y
    Dim ArrUldTot4 As Integer   'Class U
    
    'Variables for collecting totals related to
    'types and classes feeding the departure flights:
    Dim DepPaxTot0 As Integer   'Total PAX
    Dim DepPaxTot1 As Integer   'Class F
    Dim DepPaxTot2 As Integer   'Class C
    Dim DepPaxTot3 As Integer   'Class Y
    Dim DepPaxTot4 As Integer   'Class U
    
    Dim DepBagTot0 As Integer   'Total PAX
    Dim DepBagTot1 As Integer   'Class F
    Dim DepBagTot2 As Integer   'Class C
    Dim DepBagTot3 As Integer   'Class Y
    Dim DepBagTot4 As Integer   'Class U
    
    Dim DepUldTot0 As Integer   'Total PAX
    Dim DepUldTot1 As Integer   'Class F
    Dim DepUldTot2 As Integer   'Class C
    Dim DepUldTot3 As Integer   'Class Y
    Dim DepUldTot4 As Integer   'Class U
    
    
    Dim ErrCase As Integer
    Dim ErrCnt1 As Integer
    Dim ErrCnt2 As Integer
    Dim iItm As Integer
    
    Dim x0 As Long
    Dim x1 As Long
    Dim X2 As Long
    Dim X3 As Long
    Dim X4 As Long
    
    Dim Y1A As Long
    Dim Y1B As Long
    Dim Y2A As Long
    Dim Y2B As Long
    
    'For Top/Bottom of the TimeLines
    Dim Y1Min As Long
    Dim Y1Max As Long
    
    Dim ShowDep As Boolean
    Dim ShowArr As Boolean
    Dim FilterIsValid As Boolean
    
    Dim LeftLine As Long
    Dim RightLine As Long
    Dim TopIndex As Integer
    Dim BotIndex As Integer
    
    CurMouse = Screen.MousePointer
    Screen.MousePointer = 11

    If CurCnxLayoutHours < 1 Then CurCnxLayoutHours = 5
    CurCnxBarIdx = -1
    ShowArr = False
    ShowDep = False
    
    If (CnxLayoutType >= 0) And (Index >= 0) Then
        If CnxLayoutType > 0 Then
            ClearConnexDetails
            CurCnxView = 1
            LastCnxArrBar = Index
        Else
            CurCnxView = 0
        End If
        ComposeChartTitle
        
        iArrBar = Index
        TopIndex = iArrBar
        BotIndex = iArrBar
        
        'Prepare the graphical objects of the arrival flight
        ArrCnxCsct(iArrBar).BackColor = vbWhite
        ArrCnxCsct(iArrBar).ForeColor = vbBlack
        
        NewPicTop = ArrLblPanel(iArrBar).Top + 15
        Select Case CnxFilterType
            Case 0
                NewPicTop = NewPicTop + lblArrTbag(iArrBar).Top + (lblArrTbag(iArrBar).Height \ 2)
                ArrCnxCsct(iArrBar).Caption = "N/A"
            Case 1
                NewPicTop = NewPicTop + lblArrTpax(iArrBar).Top + (lblArrTpax(iArrBar).Height \ 2)
                ArrCnxCsct(iArrBar).Caption = "PAX"
            Case 2
                NewPicTop = NewPicTop + lblArrTbag(iArrBar).Top + (lblArrTbag(iArrBar).Height \ 2)
                ArrCnxCsct(iArrBar).Caption = "BAG"
            Case 3
                NewPicTop = NewPicTop + lblArrTuld(iArrBar).Top + (lblArrTuld(iArrBar).Height \ 2)
                ArrCnxCsct(iArrBar).Caption = "ULD"
            Case Else
        End Select
        picLeftBarCnx(iArrBar).Top = NewPicTop - (picLeftBarCnx(iArrBar).Height \ 2)
        Y1A = picLeftBarCnx(iArrBar).Top + (picLeftBarCnx(iArrBar).Height \ 2)
        Y1B = Y1A - 15
        Y2A = Y1A
        Y2B = Y1B
        X3 = MidScConx(0).Width
        LeftLine = ArrLblPanel(iArrBar).Left + ArrLblPanel(iArrBar).Width + 150
        If CnxLayoutType = 4 Then LeftLine = LeftLine + (1 * (60 * 15))
        x1 = LeftLine
        x0 = x1
        If CnxLayoutType = 4 Then x0 = ArrLblPanel(iArrBar).Left + ArrLblPanel(iArrBar).Width + 150
        picLeftBarCnx(iArrBar).Left = x0 - 120
        ArrInfoPanel(iArrBar).Left = LeftLine - ArrCnxCsct(iArrBar).Width - 15
        ArrInfoPanel(iArrBar).Top = Y1A - (ArrInfoPanel(iArrBar).Height \ 2)
        X4 = X3 - 600
        'TimeScale of MiniGanttChart
        'Calculation: xHours * 60Min * 15Twips
        CnxRangeMin = CurCnxLayoutHours * 60
        NewLeft = LeftLine + (CnxRangeMin * 15)
        RightLine = NewLeft
        Select Case CnxLayoutType
            Case 0, 1
                X2 = X3 - 600
            Case 2, 3
                X2 = x1 - 120
            Case 4
                X2 = x1 - 60
            Case Else
        End Select
        'If CnxLayoutType > 1 Then
        '    MidScConx(0).Line (X0, Y1A)-(MidScConx(0).ScaleWidth, 0), vbWhite
        '    MidScConx(0).Line (X0, Y1A)-(MidScConx(0).ScaleWidth, MidScConx(0).ScaleHeight), vbWhite
        'End If
        
        CurConx = Right("000000" & CStr(iArrBar), 6)
        RotHitLine = TabRotFlightsTab.GetLinesByIndexValue("CONX", CurConx, 0)
        If RotHitLine <> "" Then
            CurCnxBarIdx = iArrBar
            If CnxLayoutType > 0 Then
                ArrFlight(iArrBar).BackColor = vbYellow
                'CopySelArrFlight iArrBar
                HighlightCurrentBar Index, ChartName
                SetCnxArrTimeScale iArrBar
            End If
            picLeftBarCnx(iArrBar).ToolTipText = ""
            
            ArrRotLine = Val(RotHitLine)
            ErrCnt1 = 0
            ErrCnt2 = 0
            'We could as well get the AURN easier
            'when we store it somewhere in the ARR flight panel
            ArrUrno = TabRotFlightsTab.GetFieldValue(ArrRotLine, "AURN")
            If ArrUrno <> "" Then
                'Here we identified an ARR record of CFITAB
                'and we know the iBar line and AURN of the flight too.
                
                'First initialize the counter variables of the arrival
                'We'll get the arrival flight always only once per call
                'of this function, so we can set all counters to zero
                ArrPaxTot0 = 0
                ArrPaxTot1 = 0
                ArrPaxTot2 = 0
                ArrPaxTot3 = 0
                ArrPaxTot4 = 0
                ArrBagTot0 = 0
                ArrBagTot1 = 0
                ArrBagTot2 = 0
                ArrBagTot3 = 0
                ArrBagTot4 = 0
                ArrUldTot0 = 0
                ArrUldTot1 = 0
                ArrUldTot2 = 0
                ArrUldTot3 = 0
                ArrUldTot4 = 0
                
                'Now we need the connected departure flights
                CnxAurn = Right("0000000000" & ArrUrno, 10)
                TabArrCnxDetails.SetInternalLineBuffer True
                PaxHitLine = TabArrCnxDetails.GetLinesByIndexValue("AURN", CnxAurn, 0)
                If Val(PaxHitLine) > 0 Then
                    'We got some hits of AURN in CFITAB (Arrival)
                    'So we can run the loop through all connection records
                    
                    PaxCfiLine = TabArrCnxDetails.GetNextResultLine
                    While PaxCfiLine >= 0
                        'First we must initialize all field values
                        'and check the filter condition
                        tmpCnxTag = ""
                        
                        'Assume that the connection record is not valid ...
                        ShowDep = False
                        'and check if this record matches the current filter.
                        'First we check the status conditions and decide
                        'if this connection should be published
                        If ShowDep = False Then
                            IsShortest = False
                            tmpCnxTag = TabArrCnxDetails.GetFieldValue(PaxCfiLine, "SCID")
                            tmpData = TabArrCnxDetails.GetFieldValue(PaxCfiLine, "CTMT")
                            If tmpData <> "" Then
                                If lblArrCsct(iArrBar).Caption = tmpData Then IsShortest = True
                            End If
                            
                            If (CnxFilterCritical > 0) And (CnxFilterShort > 0) Then
                                If (InStr(tmpCnxTag, "C") > 0) Then ShowDep = True
                                If (InStr(tmpCnxTag, "S") > 0) Then ShowDep = True
                                If (InStr(tmpCnxTag, "X") > 0) Then ShowDep = True
                            ElseIf CnxFilterCritical > 0 Then
                                If (InStr(tmpCnxTag, "C") > 0) Then ShowDep = True
                            ElseIf CnxFilterShort > 0 Then
                                If (InStr(tmpCnxTag, "S") > 0) Then ShowDep = True
                                If (InStr(tmpCnxTag, "X") > 0) Then ShowDep = True
                            Else
                                ShowDep = True
                            End If
                        End If
                        
                        If ShowDep = True Then
                            'We need all values of this connection record.
                            CnxPaxTot0 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "PAXT"))
                            CnxPaxTot1 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "PAX1"))
                            CnxPaxTot2 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "PAX2"))
                            CnxPaxTot3 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "PAX3"))
                            CnxPaxTot4 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "PAX4"))
                            CnxBagTot0 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "BAGT"))
                            CnxBagTot1 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "BAG1"))
                            CnxBagTot2 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "BAG2"))
                            CnxBagTot3 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "BAG3"))
                            CnxBagTot4 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "BAG4"))
                            CnxUldTot0 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "ULDT"))
                            CnxUldTot1 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "ULD1"))
                            CnxUldTot2 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "ULD2"))
                            CnxUldTot3 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "ULD3"))
                            CnxUldTot4 = Val(TabArrCnxDetails.GetFieldValue(PaxCfiLine, "ULD4"))
                        End If
                        
                        'Now we have to check the other filters
                        If ShowDep = True Then
                            'Filter per Type in general PAX/BAG/ULD or ALL
                            Select Case CnxFilterType
                                Case 0  'ALL
                                Case 1  'PAX
                                    If CnxPaxTot0 <= 0 Then ShowDep = False
                                Case 2  'BAG
                                    If CnxBagTot0 <= 0 Then ShowDep = False
                                Case 3  'ULD
                                    If CnxUldTot0 <= 0 Then ShowDep = False
                                Case Else
                            End Select
                        End If
                        
                        If ShowDep = True Then
                            'Filter per class, but we must consider the type filter too.
                            '(Seems to be the same like above)
                            FilterIsValid = False
                            Select Case CnxFilterType
                                Case 0  'ALL (Any Connection)
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxPaxTot1 > 0 Then FilterIsValid = True
                                            If CnxBagTot1 > 0 Then FilterIsValid = True
                                            If CnxUldTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxPaxTot2 > 0 Then FilterIsValid = True
                                            If CnxBagTot2 > 0 Then FilterIsValid = True
                                            If CnxUldTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxPaxTot3 > 0 Then FilterIsValid = True
                                            If CnxBagTot3 > 0 Then FilterIsValid = True
                                            If CnxUldTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxPaxTot4 > 0 Then FilterIsValid = True
                                            If CnxBagTot4 > 0 Then FilterIsValid = True
                                            If CnxUldTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxPaxTot0 > 0 Then FilterIsValid = True
                                            If CnxBagTot0 > 0 Then FilterIsValid = True
                                            If CnxUldTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case 1  'PAX (Only PAX Connections)
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxPaxTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxPaxTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxPaxTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxPaxTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxPaxTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case 2  'BAG
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxBagTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxBagTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxBagTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxBagTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxBagTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case 3  'ULD
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxUldTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxUldTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxUldTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxUldTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxUldTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case Else
                            End Select
                            
                            ShowDep = FilterIsValid
                            
                        End If
                        
                        If ShowDep = True Then
                            'We have valid data of the arrival connection
                            'so we activate the connex grahpic objects
                            ShowArr = True
                            'and we can add the current values to the
                            'counter variables of the arrival flight
                            ArrPaxTot1 = ArrPaxTot1 + CnxPaxTot1
                            ArrPaxTot2 = ArrPaxTot2 + CnxPaxTot2
                            ArrPaxTot3 = ArrPaxTot3 + CnxPaxTot3
                            ArrPaxTot4 = ArrPaxTot4 + CnxPaxTot4
                            
                            ArrBagTot1 = ArrBagTot1 + CnxBagTot1
                            ArrBagTot2 = ArrBagTot2 + CnxBagTot2
                            ArrBagTot3 = ArrBagTot3 + CnxBagTot3
                            ArrBagTot4 = ArrBagTot4 + CnxBagTot4
                            
                            ArrUldTot1 = ArrUldTot1 + CnxUldTot1
                            ArrUldTot2 = ArrUldTot2 + CnxUldTot2
                            ArrUldTot3 = ArrUldTot3 + CnxUldTot3
                            ArrUldTot4 = ArrUldTot4 + CnxUldTot4
                            
                            ErrCase = 0
                            'We need the iBar line of the departure flight
                            'So we must first get the URNO of the departure
                            DepUrno = TabArrCnxDetails.GetFieldValue(PaxCfiLine, "DURN")
                            While Left(DepUrno, 1) = "0"
                                DepUrno = Mid(DepUrno, 2)
                            Wend
                            If DepUrno <> "" Then
                                'Now searching the departure (DURN) in the rotation grid
                                RotHitLine = TabRotFlightsTab.GetLinesByIndexValue("DURN", DepUrno, 0)
                                If RotHitLine <> "" Then
                                    'Found the departure flight
                                    DepRotLine = Val(RotHitLine)
                                    'Now we need the iBar line index of this chart
                                    RotHitLine = TabRotFlightsTab.GetFieldValue(DepRotLine, "CONX")
                                    If RotHitLine <> "------" Then
                                        'Got the line (iDepBar) of that DURN.
                                        iDepBar = Val(RotHitLine)
                                        'Highlight the flight as selected.
                                        If CnxLayoutType > 0 Then DepFlight(iDepBar).BackColor = vbBlue
                                        
                                        'Connections to a particular departure flight can be many.
                                        'Thus we need to keep the actual totals somewhere in relation
                                        'to the departure flight objects.
                                        'The values are kept in tags per type attached to the
                                        'according labels of the departure info panel.
                                        'So we must initialize the variables of the departure
                                        'connex totals by reading the values from the tag lists.
                                        tmpTag = DepCnxSum1(iDepBar).Tag
                                        DepPaxTot1 = Val(GetItem(tmpTag, 1, ","))
                                        DepBagTot1 = Val(GetItem(tmpTag, 2, ","))
                                        DepUldTot1 = Val(GetItem(tmpTag, 3, ","))
                                        tmpTag = DepCnxSum2(iDepBar).Tag
                                        DepPaxTot2 = Val(GetItem(tmpTag, 1, ","))
                                        DepBagTot2 = Val(GetItem(tmpTag, 2, ","))
                                        DepUldTot2 = Val(GetItem(tmpTag, 3, ","))
                                        tmpTag = DepCnxSum3(iDepBar).Tag
                                        DepPaxTot3 = Val(GetItem(tmpTag, 1, ","))
                                        DepBagTot3 = Val(GetItem(tmpTag, 2, ","))
                                        DepUldTot3 = Val(GetItem(tmpTag, 3, ","))
                                        tmpTag = DepCnxSum4(iDepBar).Tag
                                        DepPaxTot4 = Val(GetItem(tmpTag, 1, ","))
                                        DepBagTot4 = Val(GetItem(tmpTag, 2, ","))
                                        DepUldTot4 = Val(GetItem(tmpTag, 3, ","))
                                        'Now we add the current values of the connex record
                                        DepPaxTot1 = DepPaxTot1 + CnxPaxTot1
                                        DepPaxTot2 = DepPaxTot2 + CnxPaxTot2
                                        DepPaxTot3 = DepPaxTot3 + CnxPaxTot3
                                        DepPaxTot4 = DepPaxTot4 + CnxPaxTot4
                                        DepBagTot1 = DepBagTot1 + CnxBagTot1
                                        DepBagTot2 = DepBagTot2 + CnxBagTot2
                                        DepBagTot3 = DepBagTot3 + CnxBagTot3
                                        DepBagTot4 = DepBagTot4 + CnxBagTot4
                                        DepUldTot1 = DepUldTot1 + CnxUldTot1
                                        DepUldTot2 = DepUldTot2 + CnxUldTot2
                                        DepUldTot3 = DepUldTot3 + CnxUldTot3
                                        DepUldTot4 = DepUldTot4 + CnxUldTot4
                                        'And push them back to the tags.
                                        tmpTag = CStr(DepPaxTot1) & "," & CStr(DepBagTot1) & "," & CStr(DepUldTot1)
                                        DepCnxSum1(iDepBar).Tag = tmpTag
                                        tmpTag = CStr(DepPaxTot2) & "," & CStr(DepBagTot2) & "," & CStr(DepUldTot2)
                                        DepCnxSum2(iDepBar).Tag = tmpTag
                                        tmpTag = CStr(DepPaxTot3) & "," & CStr(DepBagTot3) & "," & CStr(DepUldTot3)
                                        DepCnxSum3(iDepBar).Tag = tmpTag
                                        tmpTag = CStr(DepPaxTot4) & "," & CStr(DepBagTot4) & "," & CStr(DepUldTot4)
                                        DepCnxSum4(iDepBar).Tag = tmpTag
                                        
                                        'We need as well the totals per type
                                        'We'll attach as tag of the DepLblPanels
                                        DepPaxTot0 = DepPaxTot1 + DepPaxTot2 + DepPaxTot3 + DepPaxTot4
                                        lblDepTpax(iDepBar).Tag = CStr(DepPaxTot0)
                                        DepBagTot0 = DepBagTot1 + DepBagTot2 + DepBagTot3 + DepBagTot4
                                        lblDepTbag(iDepBar).Tag = CStr(DepBagTot0)
                                        DepUldTot0 = DepUldTot1 + DepUldTot2 + DepUldTot3 + DepUldTot4
                                        lblDepTuld(iDepBar).Tag = CStr(DepUldTot0)
                                        
                                        'Note: Publishing the current values of the
                                        'type totals slows down the performance because
                                        'VB refreshes the label when you change the caption.
                                        'So it would be better to do this in a final loop.
                                        '
                                        If DepPaxTot0 > 0 Then
                                            lblDepTpax(iDepBar).Caption = CStr(DepPaxTot0)
                                            lblDepTpax(iDepBar).BackColor = LightGreen
                                            lblDepTpax(iDepBar).MousePointer = 99
                                        Else
                                            lblDepTpax(iDepBar).Caption = ""
                                            lblDepTpax(iDepBar).BackColor = LightGrey
                                            lblDepTpax(iDepBar).MousePointer = 0
                                        End If
                                        If DepBagTot0 > 0 Then
                                            lblDepTbag(iDepBar).Caption = CStr(DepBagTot0)
                                            lblDepTbag(iDepBar).BackColor = LightYellow
                                            lblDepTbag(iDepBar).MousePointer = 99
                                        Else
                                            lblDepTbag(iDepBar).Caption = ""
                                            lblDepTbag(iDepBar).BackColor = LightGrey
                                            lblDepTbag(iDepBar).MousePointer = 0
                                        End If
                                        If DepUldTot0 > 0 Then
                                            lblDepTuld(iDepBar).Caption = CStr(DepUldTot0)
                                            lblDepTuld(iDepBar).BackColor = LightBlue
                                            lblDepTuld(iDepBar).MousePointer = 99
                                        Else
                                            lblDepTuld(iDepBar).Caption = ""
                                            lblDepTuld(iDepBar).BackColor = LightGrey
                                            lblDepTuld(iDepBar).MousePointer = 0
                                        End If
                                        
                                        If CnxFilterType > 0 Then
                                            iItm = CnxFilterType
                                            iBar = iDepBar
                                            DepCnxSum1(iBar).Caption = GetItem(DepCnxSum1(iBar).Tag, iItm, ",")
                                            DepCnxSum2(iBar).Caption = GetItem(DepCnxSum2(iBar).Tag, iItm, ",")
                                            DepCnxSum3(iBar).Caption = GetItem(DepCnxSum3(iBar).Tag, iItm, ",")
                                            DepCnxSum4(iBar).Caption = GetItem(DepCnxSum4(iBar).Tag, iItm, ",")
                                            If DepCnxSum1(iBar).Caption = "0" Then DepCnxSum1(iBar).Caption = ""
                                            If DepCnxSum2(iBar).Caption = "0" Then DepCnxSum2(iBar).Caption = ""
                                            If DepCnxSum3(iBar).Caption = "0" Then DepCnxSum3(iBar).Caption = ""
                                            If DepCnxSum4(iBar).Caption = "0" Then DepCnxSum4(iBar).Caption = ""
                                        End If
                                        
                                        lblCnxPaxBar(iDepBar).BackColor = lblDepTpax(iDepBar).BackColor
                                        lblCnxBagBar(iDepBar).BackColor = lblDepTbag(iDepBar).BackColor
                                        lblCnxUldBar(iDepBar).BackColor = lblDepTuld(iDepBar).BackColor
                                        '
                                        'End of comment. See above.
                                        
                                        LinColor = vbBlack
                                        
                                        'THIS PART TO BE MODIFIED!!
                                        DepCnxCsct(iDepBar).BackColor = vbWhite
                                        DepCnxCsct(iDepBar).ForeColor = vbBlack
                                        picLeftBarCnx(iDepBar).Picture = CnxArrIcon(0).Picture
                                        If InStr(tmpCnxTag, "X") > 0 Then
                                            LinColor = vbRed
                                            DepCnxCsct(iDepBar).BackColor = LinColor
                                            DepCnxCsct(iDepBar).ForeColor = vbWhite
                                            picLeftBarCnx(iDepBar).Picture = CnxArrIcon(1).Picture
                                        End If
                                        If InStr(tmpCnxTag, "S") > 0 Then
                                            LinColor = vbRed
                                            DepCnxCsct(iDepBar).BackColor = LinColor
                                            DepCnxCsct(iDepBar).ForeColor = vbWhite
                                            picLeftBarCnx(iDepBar).Picture = CnxArrIcon(1).Picture
                                        End If
                                        If InStr(tmpCnxTag, "C") > 0 Then
                                            LinColor = vbCyan
                                            DepCnxCsct(iDepBar).BackColor = LinColor
                                            DepCnxCsct(iDepBar).ForeColor = vbBlack
                                            picLeftBarCnx(iDepBar).Picture = CnxArrIcon(2).Picture
                                        End If
                                        tmpData = TabArrCnxDetails.GetFieldValue(PaxCfiLine, "CTMT")
                                        DepCnxCsct(iDepBar).Caption = tmpData
                                        If tmpData <> "" Then
                                            If tmpData <> "" Then
                                                tmpData = Replace(tmpData, "H", ":", 1, -1, vbBinaryCompare)
                                                If InStr(tmpData, ":") > 0 Then
                                                    If Left(tmpData, 1) = "-" Then
                                                        CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) - Val(GetItem(tmpData, 2, ":"))
                                                    Else
                                                        CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) + Val(GetItem(tmpData, 2, ":"))
                                                    End If
                                                Else
                                                    CurCTim = Val(tmpData)
                                                End If
                                            End If
                                        End If
                                        'SEE ABOVE
                                        
                                        If ShowDep = True Then
                                            
                                            NewPicTop = DepLblPanel(iDepBar).Top + 30
                                            Select Case CnxFilterType
                                                Case 0
                                                    NewPicTop = NewPicTop + lblDepTbag(iDepBar).Top + (lblDepTbag(iDepBar).Height \ 2)
                                                    NewBarTop = NewPicTop - (lblCnxBagBar(iDepBar).Height \ 2) - lblCnxPaxBar(iDepBar).Height + 15
                                                Case 1
                                                    NewPicTop = NewPicTop + lblDepTpax(iDepBar).Top + (lblDepTpax(iDepBar).Height \ 2)
                                                    NewBarTop = NewPicTop - (lblCnxPaxBar(iDepBar).Height \ 2) - 15
                                                Case 2
                                                    NewPicTop = NewPicTop + lblDepTbag(iDepBar).Top + (lblDepTbag(iDepBar).Height \ 2)
                                                    NewBarTop = NewPicTop - (lblCnxBagBar(iDepBar).Height \ 2) - lblCnxPaxBar(iDepBar).Height + 15
                                                Case 3
                                                    NewPicTop = NewPicTop + lblDepTuld(iDepBar).Top + (lblDepTuld(iDepBar).Height \ 2)
                                                    NewBarTop = NewPicTop - (lblCnxUldBar(iDepBar).Height \ 2) - lblCnxBagBar(iDepBar).Height - lblCnxPaxBar(iDepBar).Height + 30
                                                Case Else
                                            End Select
                                            picDepCnx(iDepBar).Top = NewPicTop - (picDepCnx(iDepBar).Height \ 2)
                                            picRightBarCnx(iDepBar).Top = NewPicTop - (picRightBarCnx(iDepBar).Height \ 2)
                                            picLeftBarCnx(iDepBar).Top = NewPicTop - (picLeftBarCnx(iDepBar).Height \ 2)
                                            lblCnxPaxBar(iDepBar).Top = NewBarTop
                                            lblCnxBagBar(iDepBar).Top = lblCnxPaxBar(iDepBar).Top + lblCnxPaxBar(iDepBar).Height - 15
                                            lblCnxUldBar(iDepBar).Top = lblCnxBagBar(iDepBar).Top + lblCnxBagBar(iDepBar).Height - 15
                                            
                                            
                                            tmpDepTag = picRightBarCnx(iDepBar).Tag
                                            If InStr(tmpCnxTag, "C") > 0 Then
                                                picRightBarCnx(iDepBar).Picture = picSmallIcon(6).Picture
                                                If InStr(tmpDepTag, "S") = 0 Then tmpDepTag = tmpDepTag & "S"
                                            ElseIf InStr(tmpCnxTag, "S") > 0 Then
                                                picRightBarCnx(iDepBar).Picture = picSmallIcon(1).Picture
                                                If InStr(tmpDepTag, "C") = 0 Then tmpDepTag = tmpDepTag & "C"
                                            ElseIf InStr(tmpCnxTag, "X") > 0 Then
                                                picRightBarCnx(iDepBar).Picture = picSmallIcon(1).Picture
                                                If InStr(tmpDepTag, "S") = 0 Then tmpDepTag = tmpDepTag & "S"
                                            End If
                                            picRightBarCnx(iDepBar).Tag = tmpDepTag
                                            
                                            DepLblPanel(iDepBar).Left = X3 - DepLblPanel(iDepBar).Width - 60
                                            picDepCnx(iDepBar).Left = DepLblPanel(iDepBar).Left - picDepCnx(iDepBar).Width
                                            X4 = picDepCnx(iDepBar).Left - 120
                                            picRightBarCnx(iDepBar).Left = X4 - 120
                                            picRightBarCnx(iDepBar).Visible = True
                                            DepLblPanel(iDepBar).Visible = True
                                            MaxWidth = picRightBarCnx(iDepBar).Left - x1
                                            'DepInfoPanel(iDepBar).Width = DepCnxCsct(iDepBar).Left + DepCnxCsct(iDepBar).Width + 30
                                            
                                            Y2A = NewPicTop
                                            Y2B = Y2A - 15
                                            If iDepBar < TopIndex Then TopIndex = iDepBar
                                            If iDepBar > BotIndex Then BotIndex = iDepBar
                                            
                                            If CnxLayoutType <= 1 Then
                                                If LinColor = vbCyan Then MidScConx(0).Line (x1, Y1A)-(X4, Y2A), vbBlack
                                                If IsShortest Then
                                                    MidScConx(0).Line (x1, Y1A - 30)-(X4, Y2A - 30), vbYellow
                                                    MidScConx(0).Line (x1, Y1A)-(X4, Y2A), vbRed
                                                End If
                                                MidScConx(0).Line (x1, Y1B)-(X4, Y2B), LinColor
                                                If CnxFilterType > 0 Then
                                                    MidScConx(0).Line (X4, Y2B)-(X3, Y2B), LinColor
                                                    MidScConx(0).Line (X4, Y2A)-(X3, Y2A), vbBlack
                                                Else
                                                    MidScConx(0).Line (X4, Y2A)-(DepLblPanel(iDepBar).Left, DepLblPanel(iDepBar).Top + 120), vbBlack
                                                    MidScConx(0).Line (X4, Y2A)-(DepLblPanel(iDepBar).Left, DepLblPanel(iDepBar).Top + DepLblPanel(iDepBar).Height - 120), vbBlack
                                                End If
                                                If (CnxFilterShort = 0) And (CnxFilterCritical = 0) Then
                                                    'DepInfoPanel(iDepBar).Width = DepCnxCsct(iDepBar).Left
                                                End If
                                                DepInfoPanel(iDepBar).Left = picRightBarCnx(iDepBar).Left - DepInfoPanel(iDepBar).Width + 30
                                                DepInfoPanel(iDepBar).Top = Y2A - (DepInfoPanel(iDepBar).Height \ 2)
                                            Else
                                                If CnxLayoutType = 4 Then
                                                    If LinColor <> vbBlack Then
                                                        If Y2B <= Y1B Then
                                                            MidScConx(0).Line (x0 + 15, Y1B)-(X2 + 15, Y2B), vbBlack
                                                        Else
                                                            MidScConx(0).Line (x0 - 15, Y1B)-(X2 - 15, Y2B), vbBlack
                                                        End If
                                                    End If
                                                    MidScConx(0).Line (x0, Y1B)-(X2, Y2B), LinColor
                                                End If
                                                If CnxFilterType > 0 Then
                                                    MidScConx(0).Line (x1, Y2A)-(X3, Y2A), vbBlack
                                                    MidScConx(0).Line (x1, Y2B)-(X3, Y2B), LinColor
                                                Else
                                                    MidScConx(0).Line (x1, Y2A)-(X4, Y2A), vbBlack
                                                    MidScConx(0).Line (x1, Y2B)-(X4, Y2B), LinColor
                                                    MidScConx(0).Line (X4, Y2A)-(DepLblPanel(iDepBar).Left, DepLblPanel(iDepBar).Top + 120), vbBlack
                                                    MidScConx(0).Line (X4, Y2A)-(DepLblPanel(iDepBar).Left, DepLblPanel(iDepBar).Top + DepLblPanel(iDepBar).Height - 120), vbBlack
                                                End If
                                                lblCnxPaxBar(iDepBar).Left = x1
                                                lblCnxBagBar(iDepBar).Left = x1
                                                lblCnxUldBar(iDepBar).Left = x1
                                                NewWidth = CurCTim * 30
                                                If NewWidth > MaxWidth Then NewWidth = MaxWidth
                                                If NewWidth < 0 Then NewWidth = 0
                                                lblCnxPaxBar(iDepBar).Width = NewWidth
                                                lblCnxBagBar(iDepBar).Width = NewWidth
                                                lblCnxUldBar(iDepBar).Width = NewWidth
                                                lblCnxPaxBar(iDepBar).Visible = True
                                                lblCnxBagBar(iDepBar).Visible = True
                                                lblCnxUldBar(iDepBar).Visible = True
                                                DepLblPanel(iDepBar).Visible = True
                                                If CnxLayoutType > 2 Then
                                                    DepInfoPanel(iDepBar).Left = RightLine - DepInfoPanel(iDepBar).Width + 690
                                                    DepInfoPanel(iDepBar).Top = Y2A - (DepInfoPanel(iDepBar).Height \ 2)
                                                    DepInfoPanel(iDepBar).Visible = True
                                                End If
                                                picLeftBarCnx(iDepBar).Left = x1 - 120
                                                picLeftBarCnx(iDepBar).Visible = True
                                            End If
                                            If CnxFilterType > 0 Then picDepCnx(iDepBar).Visible = True
                                        End If
                                    Else
                                        ErrCase = 3
                                        ErrCnt1 = ErrCnt1 + 1
                                    End If
                                Else
                                    ErrCase = 2
                                    ErrCnt1 = ErrCnt1 + 1
                                End If
                            Else
                                ErrCase = 1
                                ErrCnt1 = ErrCnt1 + 1
                            End If
                        End If
                        If ErrCase > 0 Then
                            If CnxLayoutType > 0 Then CreateDepInfoRec PaxCfiLine, ErrCase
                            ShowArr = True
                        End If
                        SetInfoPanelColors iArrBar, iDepBar
                        PaxCfiLine = TabArrCnxDetails.GetNextResultLine
                    Wend
                End If
                TabArrCnxDetails.SetInternalLineBuffer False
                
                If ShowArr = True Then
                    'Keep the totals per type and class in the arr connex info tags.
                    tmpTag = CStr(ArrPaxTot1) & "," & CStr(ArrBagTot1) & "," & CStr(ArrUldTot1)
                    ArrCnxSum1(iArrBar).Tag = tmpTag
                    tmpTag = CStr(ArrPaxTot2) & "," & CStr(ArrBagTot2) & "," & CStr(ArrUldTot2)
                    ArrCnxSum2(iArrBar).Tag = tmpTag
                    tmpTag = CStr(ArrPaxTot3) & "," & CStr(ArrBagTot3) & "," & CStr(ArrUldTot3)
                    ArrCnxSum3(iArrBar).Tag = tmpTag
                    tmpTag = CStr(ArrPaxTot4) & "," & CStr(ArrBagTot4) & "," & CStr(ArrUldTot4)
                    ArrCnxSum4(iArrBar).Tag = tmpTag
                    
                    ArrPaxTot0 = ArrPaxTot1 + ArrPaxTot2 + ArrPaxTot3 + ArrPaxTot4
                    If ArrPaxTot0 > 0 Then
                        lblArrTpax(iArrBar).Caption = CStr(ArrPaxTot0)
                        lblArrTpax(iArrBar).BackColor = LightGreen
                        lblArrTpax(iArrBar).MousePointer = 99
                    Else
                        lblArrTpax(iArrBar).Caption = ""
                        lblArrTpax(iArrBar).BackColor = LightGrey
                        lblArrTpax(iArrBar).MousePointer = 0
                    End If
                    ArrBagTot0 = ArrBagTot1 + ArrBagTot2 + ArrBagTot3 + ArrBagTot4
                    If ArrBagTot0 > 0 Then
                        lblArrTbag(iArrBar).Caption = CStr(ArrBagTot0)
                        lblArrTbag(iArrBar).BackColor = LightYellow
                        lblArrTbag(iArrBar).MousePointer = 99
                    Else
                        lblArrTbag(iArrBar).Caption = ""
                        lblArrTbag(iArrBar).BackColor = LightGrey
                        lblArrTbag(iArrBar).MousePointer = 0
                    End If
                    ArrUldTot0 = ArrUldTot1 + ArrUldTot2 + ArrUldTot3 + ArrUldTot4
                    If ArrUldTot0 > 0 Then
                        lblArrTuld(iArrBar).Caption = CStr(ArrUldTot0)
                        lblArrTuld(iArrBar).BackColor = LightBlue
                        lblArrTuld(iArrBar).MousePointer = 99
                    Else
                        lblArrTuld(iArrBar).Caption = ""
                        lblArrTuld(iArrBar).BackColor = LightGrey
                        lblArrTuld(iArrBar).MousePointer = 0
                    End If
                    ArrLblPanel(iArrBar).Visible = True
                    
                    If (CnxFilterType > 0) And (CnxLayoutType > 2) Then
                        iItm = CnxFilterType
                        iBar = iArrBar
                        ArrCnxSum1(iBar).Caption = GetItem(ArrCnxSum1(iBar).Tag, iItm, ",")
                        ArrCnxSum2(iBar).Caption = GetItem(ArrCnxSum2(iBar).Tag, iItm, ",")
                        ArrCnxSum3(iBar).Caption = GetItem(ArrCnxSum3(iBar).Tag, iItm, ",")
                        ArrCnxSum4(iBar).Caption = GetItem(ArrCnxSum4(iBar).Tag, iItm, ",")
                        If ArrCnxSum1(iBar).Caption = "0" Then ArrCnxSum1(iBar).Caption = ""
                        If ArrCnxSum2(iBar).Caption = "0" Then ArrCnxSum2(iBar).Caption = ""
                        If ArrCnxSum3(iBar).Caption = "0" Then ArrCnxSum3(iBar).Caption = ""
                        If ArrCnxSum4(iBar).Caption = "0" Then ArrCnxSum4(iBar).Caption = ""
                        ArrInfoPanel(iArrBar).Visible = True
                        MidScConx(0).Line (x0, Y1A)-(ArrInfoPanel(iArrBar).Left, Y1A), vbBlack
                        MidScConx(0).Line (x0, Y1B)-(ArrInfoPanel(iArrBar).Left, Y1B), vbBlack
                    End If
                    
                    If CnxFilterType > 0 Then
                        MidScConx(0).Line (0, Y1A)-(x0, Y1A), vbBlack
                        MidScConx(0).Line (0, Y1B)-(x0, Y1B), vbBlack
                        picLeftBarCnx(iArrBar).Picture = CnxArrIcon(0).Picture
                    Else
                        MidScConx(0).Line (ArrLblPanel(iArrBar).Width, ArrLblPanel(iArrBar).Top)-(x0 + 15, Y1A), vbBlack
                        MidScConx(0).Line (ArrLblPanel(iArrBar).Width, ArrLblPanel(iArrBar).Top + ArrLblPanel(iArrBar).Height - 15)-(x0, Y1B), vbBlack
                        picLeftBarCnx(iArrBar).Picture = picSmallIcon(0).Picture
                    End If
                    picLeftBarCnx(iArrBar).Visible = True
                End If
                Select Case CnxLayoutType
                    Case 0, 1
                    Case 2, 3, 4
                        Y1Min = DepLblPanel(BotIndex).Top - 120
                        Y1Max = DepLblPanel(BotIndex).Top + DepLblPanel(BotIndex).Height + 120
                        DrawTimeLines LeftLine, Y1Max, 0, vbBlack
                        DrawTimeLines RightLine, Y1Max, MidScConx(0).ScaleWidth - 30, vbBlack
                        lblLeftTime(0).Left = LeftLine + MidScCon1(0).Left + 60
                        lblLeftDate(0).Left = lblLeftTime(0).Left - lblLeftDate(0).Width
                        lblRightTime(0).Left = RightLine - lblRightTime(0).Width + MidScCon1(0).Left + 45
                        lblRightDate(0).Left = RightLine + MidScCon1(0).Left + 45
                        lblLeftDate(0).Visible = True
                        lblLeftTime(0).Visible = True
                        lblRightTime(0).Visible = True
                        lblRightDate(0).Visible = True
                    Case Else
                End Select
                
            
                If (ErrCnt1 + ErrCnt2) > 0 Then
                    picLeftBarCnx(iArrBar).Picture = picInfoPics(1)
                    If ErrCnt1 = 1 Then
                        tmpData = "--> " & CStr(ErrCnt1) & " Departure Flight Is Too Far In The Future."
                    End If
                    If ErrCnt1 > 1 Then
                        tmpData = "--> " & CStr(ErrCnt1) & " Departure Flights Are Too Far In The Future."
                    End If
                    picLeftBarCnx(iArrBar).ToolTipText = tmpData
                End If
                If TabDepInfo(0).GetLineCount > 0 Then
                    TabDepInfo(0).AutoSizeColumns
                    TabDepInfo(0).Sort "2", True, True
                    TabDepInfo(0).Visible = True
                End If
                tmpData = ""
                tmpData = tmpData & "{X3}" & CStr(X3) & "{/X3}"
                tmpData = tmpData & "{H}" & CStr(CurCnxLayoutHours) & "{/H}"
                MidScConx(0).Tag = tmpData
                If CnxLayoutType = 0 Then
                    If ShowArr = True Then
                        picArrCnxCnt(0).Tag = CStr(Val(picArrCnxCnt(0).Tag) + 1)
                        picArrCnxCnt(0).Picture = imgLookUpA
                    End If
                    If ShowDep = True Then
                        picDepCnxCnt(0).Tag = CStr(Val(picDepCnxCnt(0).Tag) + 1)
                        picDepCnxCnt(0).Picture = imgLookUpB
                    End If
                End If
            End If
        End If
    End If
    MidScConx(0).ZOrder
    Screen.MousePointer = CurMouse
End Sub

Private Sub ShowDepConnexDetails(Index As Integer)
    Static LastColor As Long
    Dim iBar As Integer
    Dim iDepBar As Integer
    Dim iArrBar As Integer
    Dim DepUrno As String
    Dim CnxDurn As String
    Dim ArrUrno As String
    Dim CurConx As String
    Dim RotHitLine As String
    Dim PaxHitLine As String
    Dim DepRotLine As Long
    Dim ArrRotLine As Long
    Dim PaxCfiLine As Long
    Dim LinColor As Long
    Dim NewPicTop As Long
    Dim NewBarTop As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim MaxWidth As Long
    Dim CurCTim As Long
    Dim IsShort As Boolean
    Dim tmpData As String
    Dim tmpTag As String
    
    Dim tmpArrTag As String
    Dim tmpCnxTag As String
    
    'Variables for the current connection record
    Dim CnxPaxTot0 As Integer   'Total PAX
    Dim CnxPaxTot1 As Integer   'Class F
    Dim CnxPaxTot2 As Integer   'Class C
    Dim CnxPaxTot3 As Integer   'Class Y
    Dim CnxPaxTot4 As Integer   'Class U
    
    Dim CnxBagTot0 As Integer   'Total PAX
    Dim CnxBagTot1 As Integer   'Class F
    Dim CnxBagTot2 As Integer   'Class C
    Dim CnxBagTot3 As Integer   'Class Y
    Dim CnxBagTot4 As Integer   'Class U
    
    Dim CnxUldTot0 As Integer   'Total PAX
    Dim CnxUldTot1 As Integer   'Class F
    Dim CnxUldTot2 As Integer   'Class C
    Dim CnxUldTot3 As Integer   'Class Y
    Dim CnxUldTot4 As Integer   'Class U
    
    'We must calculate the totals per type and class
    'for each connection according to the active filters.
    
    'Variables for collecting totals related to
    'types and classes leaving the Depival flight:
    Dim DepPaxTot0 As Integer   'Total PAX
    Dim DepPaxTot1 As Integer   'Class F
    Dim DepPaxTot2 As Integer   'Class C
    Dim DepPaxTot3 As Integer   'Class Y
    Dim DepPaxTot4 As Integer   'Class U
    
    Dim DepBagTot0 As Integer   'Total PAX
    Dim DepBagTot1 As Integer   'Class F
    Dim DepBagTot2 As Integer   'Class C
    Dim DepBagTot3 As Integer   'Class Y
    Dim DepBagTot4 As Integer   'Class U
    
    Dim DepUldTot0 As Integer   'Total PAX
    Dim DepUldTot1 As Integer   'Class F
    Dim DepUldTot2 As Integer   'Class C
    Dim DepUldTot3 As Integer   'Class Y
    Dim DepUldTot4 As Integer   'Class U
    
    'Variables for collecting totals related to
    'types and classes feeding the Arrarture flights:
    Dim ArrPaxTot0 As Integer   'Total PAX
    Dim ArrPaxTot1 As Integer   'Class F
    Dim ArrPaxTot2 As Integer   'Class C
    Dim ArrPaxTot3 As Integer   'Class Y
    Dim ArrPaxTot4 As Integer   'Class U
    
    Dim ArrBagTot0 As Integer   'Total PAX
    Dim ArrBagTot1 As Integer   'Class F
    Dim ArrBagTot2 As Integer   'Class C
    Dim ArrBagTot3 As Integer   'Class Y
    Dim ArrBagTot4 As Integer   'Class U
    
    Dim ArrUldTot0 As Integer   'Total PAX
    Dim ArrUldTot1 As Integer   'Class F
    Dim ArrUldTot2 As Integer   'Class C
    Dim ArrUldTot3 As Integer   'Class Y
    Dim ArrUldTot4 As Integer   'Class U
    
    
    Dim ErrCase As Integer
    Dim ErrCnt1 As Integer
    Dim ErrCnt2 As Integer
    Dim iItm As Integer
    
    Dim x0 As Long
    Dim x1 As Long
    Dim X2 As Long
    Dim X3 As Long
    Dim X4 As Long
    Dim Y0A As Long
    Dim Y0B As Long
    Dim y1 As Long
    Dim Y2 As Long
    Dim Y1A As Long
    Dim Y1B As Long
    Dim Y2A As Long
    Dim Y2B As Long
    Dim Y4A As Long
    Dim Y4B As Long
    Dim Y1Min As Long
    Dim Y1Max As Long
    
    Dim ShowArr As Boolean
    Dim ShowDep As Boolean
    Dim FilterIsValid As Boolean
    Dim SetVisible As Boolean
    Dim LeftLine As Long
    Dim RightLine As Long
    Dim TopIndex As Integer
    Dim BotIndex As Integer

    If CurCnxLayoutHours < 1 Then CurCnxLayoutHours = 5
    CurCnxBarIdx = -1
    ShowDep = False
    ShowArr = False
    
    If (CnxLayoutType >= 0) And (Index >= 0) Then
        If CnxLayoutType > 0 Then
            ClearConnexDetails
            CurCnxView = 1
            LastCnxDepBar = Index
        Else
            CurCnxView = 0
        End If
        ComposeChartTitle
        
        iDepBar = Index
        TopIndex = iDepBar
        BotIndex = iDepBar
        'Prepare the graphical objects of the Departure flight
        DepCnxCsct(iDepBar).BackColor = vbWhite
        DepCnxCsct(iDepBar).ForeColor = vbBlack
        
        y1 = DepLblPanel(iDepBar).Top + 15
        Y0A = DepLblPanel(iDepBar).Top + 120
        Y0B = DepLblPanel(iDepBar).Top + DepLblPanel(iDepBar).Height - 120
        Select Case CnxFilterType
            Case 0
                y1 = y1 + lblDepTbag(iDepBar).Top + (lblDepTbag(iDepBar).Height \ 2)
                DepCnxCsct(iDepBar).Caption = "N/A"
            Case 1
                y1 = y1 + lblDepTpax(iDepBar).Top + (lblDepTpax(iDepBar).Height \ 2)
                DepCnxCsct(iDepBar).Caption = "PAX"
            Case 2
                y1 = y1 + lblDepTbag(iDepBar).Top + (lblDepTbag(iDepBar).Height \ 2)
                DepCnxCsct(iDepBar).Caption = "BAG"
            Case 3
                y1 = y1 + lblDepTuld(iDepBar).Top + (lblDepTuld(iDepBar).Height \ 2)
                DepCnxCsct(iDepBar).Caption = "ULD"
            Case Else
        End Select
        picRightBarCnx(iDepBar).Top = y1 - (picRightBarCnx(iDepBar).Height \ 2) + 15
        picDepCnx(iDepBar).Top = picRightBarCnx(iDepBar).Top
        DepInfoPanel(iDepBar).Top = y1 - (DepInfoPanel(iDepBar).Height \ 2)
        Y1A = y1 - 15
        Y1B = y1 + 15
        
        x0 = MidScConx(0).ScaleWidth - DepLblPanel(iDepBar).Width
        DepLblPanel(iDepBar).Left = x0
        x1 = x0 - picDepCnx(iDepBar).Width - (picRightBarCnx(iDepBar).Width \ 2)
        picRightBarCnx(iDepBar).Left = x1 - (picRightBarCnx(iDepBar).Width \ 2)
        
        X2 = x1
        If CnxLayoutType = 4 Then X2 = X2 - (1 * (60 * 15))
        RightLine = X2 - 30
        DepInfoPanel(iDepBar).Left = X2 - DepCnxCsct(iDepBar).Left - 15
        
        X4 = ArrLblPanel(0).Width '- 15
        X3 = ArrLblPanel(0).Width + (picLeftBarCnx(0).Width \ 2) + 30
        
        'TimeScale of MiniGanttChart
        'Calculation: xHours * 60Min * 15Twips
        CnxRangeMin = CurCnxLayoutHours * 60
        NewLeft = RightLine - (CnxRangeMin * 15)
        LeftLine = NewLeft
        
        CurConx = Right("000000" & CStr(iDepBar), 6)
        RotHitLine = TabRotFlightsTab.GetLinesByIndexValue("CONX", CurConx, 0)
        If RotHitLine <> "" Then
            CurCnxBarIdx = iDepBar
            If CnxLayoutType > 0 Then
                DepFlight(iDepBar).BackColor = vbYellow
                'CopySelDepFlight iDepBar
                HighlightCurrentBar Index, ChartName
                SetCnxDepTimeScale iDepBar
            End If
            picRightBarCnx(iDepBar).ToolTipText = ""
            
            DepRotLine = Val(RotHitLine)
            ErrCnt1 = 0
            ErrCnt2 = 0
            'We could as well get the AURN easier
            'when we store it somewhere in the Dep flight panel
            DepUrno = TabRotFlightsTab.GetFieldValue(DepRotLine, "DURN")
            If DepUrno <> "" Then
                'Here we identified an Dep record of CFITAB
                'and we know the iBar line and DURN of the flight too.
                
                'First initialize the counter variables of the Departure
                'We'll get the Departure flight always only once per call
                'of this function, so we can set all counters to zero
                DepPaxTot0 = 0
                DepPaxTot1 = 0
                DepPaxTot2 = 0
                DepPaxTot3 = 0
                DepPaxTot4 = 0
                DepBagTot0 = 0
                DepBagTot1 = 0
                DepBagTot2 = 0
                DepBagTot3 = 0
                DepBagTot4 = 0
                DepUldTot0 = 0
                DepUldTot1 = 0
                DepUldTot2 = 0
                DepUldTot3 = 0
                DepUldTot4 = 0
                
                'Now we need the connected Arrival flights
                CnxDurn = Right("0000000000" & DepUrno, 10)
                TabDepCnxDetails.SetInternalLineBuffer True
                PaxHitLine = TabDepCnxDetails.GetLinesByIndexValue("DURN", CnxDurn, 0)
                If Val(PaxHitLine) > 0 Then
                    'We got some hits of DURN in CFITAB (Depival)
                    'So we can run the loop through all connection records
                    
                    PaxCfiLine = TabDepCnxDetails.GetNextResultLine
                    While PaxCfiLine >= 0
                        'First we must initialize all field values
                        'and check the filter condition
                        tmpCnxTag = ""
                        
                        'Assume that the connection record is not valid ...
                        ShowArr = False
                        'and check if this record matches the current filter.
                        'First we check the status conditions and decide
                        'if this connection should be published
                        If ShowArr = False Then
                            IsShort = False
                            tmpData = TabDepCnxDetails.GetFieldValue(PaxCfiLine, "SCID")
                            If tmpData <> "" Then
                                If InStr(tmpCnxTag, "S") = 0 Then tmpCnxTag = tmpCnxTag & "S"
                            End If
                            tmpData = TabDepCnxDetails.GetFieldValue(PaxCfiLine, "CTMT")
                            If tmpData <> "" Then
                                If InStr(tmpCnxTag, "S") > 0 Then
                                    If tmpData = lblDepCsct(iDepBar).Caption Then IsShort = True
                                End If
                            End If
                            
                            If (CnxFilterCritical > 0) And (CnxFilterShort > 0) Then
                                If (InStr(tmpCnxTag, "C") > 0) Or (InStr(tmpCnxTag, "S") > 0) Then ShowArr = True
                            ElseIf CnxFilterCritical > 0 Then
                                If (InStr(tmpCnxTag, "C") > 0) Then ShowArr = True
                            ElseIf CnxFilterShort > 0 Then
                                If (InStr(tmpCnxTag, "S") > 0) Then ShowArr = True
                            Else
                                ShowArr = True
                            End If
                        End If
                        
                        If ShowArr = True Then
                            'We need all values of this connection record.
                            CnxPaxTot0 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "PAXT"))
                            CnxPaxTot1 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "PAX1"))
                            CnxPaxTot2 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "PAX2"))
                            CnxPaxTot3 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "PAX3"))
                            CnxPaxTot4 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "PAX4"))
                            CnxBagTot0 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "BAGT"))
                            CnxBagTot1 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "BAG1"))
                            CnxBagTot2 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "BAG2"))
                            CnxBagTot3 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "BAG3"))
                            CnxBagTot4 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "BAG4"))
                            CnxUldTot0 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "ULDT"))
                            CnxUldTot1 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "ULD1"))
                            CnxUldTot2 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "ULD2"))
                            CnxUldTot3 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "ULD3"))
                            CnxUldTot4 = Val(TabDepCnxDetails.GetFieldValue(PaxCfiLine, "ULD4"))
                        End If
                        
                        'Now we have to check the other filters
                        If ShowArr = True Then
                            'Filter per Type in general PAX/BAG/ULD or ALL
                            Select Case CnxFilterType
                                Case 0  'ALL
                                Case 1  'PAX
                                    If CnxPaxTot0 <= 0 Then ShowArr = False
                                Case 2  'BAG
                                    If CnxBagTot0 <= 0 Then ShowArr = False
                                Case 3  'ULD
                                    If CnxUldTot0 <= 0 Then ShowArr = False
                                Case Else
                            End Select
                        End If
                        
                        If ShowArr = True Then
                            'Filter per class, but we must consider the type filter too.
                            '(Seems to be the same like above)
                            FilterIsValid = False
                            Select Case CnxFilterType
                                Case 0  'ALL (Any Connection)
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxPaxTot1 > 0 Then FilterIsValid = True
                                            If CnxBagTot1 > 0 Then FilterIsValid = True
                                            If CnxUldTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxPaxTot2 > 0 Then FilterIsValid = True
                                            If CnxBagTot2 > 0 Then FilterIsValid = True
                                            If CnxUldTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxPaxTot3 > 0 Then FilterIsValid = True
                                            If CnxBagTot3 > 0 Then FilterIsValid = True
                                            If CnxUldTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxPaxTot4 > 0 Then FilterIsValid = True
                                            If CnxBagTot4 > 0 Then FilterIsValid = True
                                            If CnxUldTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxPaxTot0 > 0 Then FilterIsValid = True
                                            If CnxBagTot0 > 0 Then FilterIsValid = True
                                            If CnxUldTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case 1  'PAX (Only PAX Connections)
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxPaxTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxPaxTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxPaxTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxPaxTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxPaxTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case 2  'BAG
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxBagTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxBagTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxBagTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxBagTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxBagTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case 3  'ULD
                                    Select Case CnxFilterClass
                                        Case 1
                                            If CnxUldTot1 > 0 Then FilterIsValid = True
                                        Case 2
                                            If CnxUldTot2 > 0 Then FilterIsValid = True
                                        Case 3
                                            If CnxUldTot3 > 0 Then FilterIsValid = True
                                        Case 4
                                            If CnxUldTot4 > 0 Then FilterIsValid = True
                                        Case Else
                                            If CnxUldTot0 > 0 Then FilterIsValid = True
                                    End Select
                                Case Else
                            End Select
                            
                            ShowArr = FilterIsValid
                            
                        End If
                        
                        If ShowArr = True Then
                            'We have valid data of the Departure connection
                            'so we activate the connex grahpic objects
                            ShowDep = True
                            'and we can add the current values to the
                            'counter variables of the Departure flight
                            DepPaxTot1 = DepPaxTot1 + CnxPaxTot1
                            DepPaxTot2 = DepPaxTot2 + CnxPaxTot2
                            DepPaxTot3 = DepPaxTot3 + CnxPaxTot3
                            DepPaxTot4 = DepPaxTot4 + CnxPaxTot4
                            
                            DepBagTot1 = DepBagTot1 + CnxBagTot1
                            DepBagTot2 = DepBagTot2 + CnxBagTot2
                            DepBagTot3 = DepBagTot3 + CnxBagTot3
                            DepBagTot4 = DepBagTot4 + CnxBagTot4
                            
                            DepUldTot1 = DepUldTot1 + CnxUldTot1
                            DepUldTot2 = DepUldTot2 + CnxUldTot2
                            DepUldTot3 = DepUldTot3 + CnxUldTot3
                            DepUldTot4 = DepUldTot4 + CnxUldTot4
                            
                            ErrCase = 0
                            'We need the iBar line of the Arrival flight
                            'So we must first get the URNO of the Arrival
                            ArrUrno = TabDepCnxDetails.GetFieldValue(PaxCfiLine, "AURN")
                            While Left(ArrUrno, 1) = "0"
                                ArrUrno = Mid(ArrUrno, 2)
                            Wend
                            If ArrUrno <> "" Then
                                'Now searching the Arrival (AURN) in the rotation grid
                                RotHitLine = TabRotFlightsTab.GetLinesByIndexValue("AURN", ArrUrno, 0)
                                If RotHitLine <> "" Then
                                    'Found the Arrarture flight
                                    ArrRotLine = Val(RotHitLine)
                                    'Now we need the iBar line index of this chart
                                    RotHitLine = TabRotFlightsTab.GetFieldValue(ArrRotLine, "CONX")
                                    If RotHitLine <> "------" Then
                                        'Got the line (iArrBar) of that DURN.
                                        iArrBar = Val(RotHitLine)
                                        'Highlight the flight as selected.
                                        If CnxLayoutType > 0 Then ArrFlight(iArrBar).BackColor = vbBlue
                                        
                                        'Connections to a particular Arrarture flight can be many.
                                        'Thus we need to keep the actual totals somewhere in relation
                                        'to the Arrarture flight objects.
                                        'The values are kept in tags per type attached to the
                                        'according labels of the Arrarture info panel.
                                        'So we must initialize the variables of the Arrarture
                                        'connex totals by reading the values from the tag lists.
                                        tmpTag = ArrCnxSum1(iArrBar).Tag
                                        ArrPaxTot1 = Val(GetItem(tmpTag, 1, ","))
                                        ArrBagTot1 = Val(GetItem(tmpTag, 2, ","))
                                        ArrUldTot1 = Val(GetItem(tmpTag, 3, ","))
                                        tmpTag = ArrCnxSum2(iArrBar).Tag
                                        ArrPaxTot2 = Val(GetItem(tmpTag, 1, ","))
                                        ArrBagTot2 = Val(GetItem(tmpTag, 2, ","))
                                        ArrUldTot2 = Val(GetItem(tmpTag, 3, ","))
                                        tmpTag = ArrCnxSum3(iArrBar).Tag
                                        ArrPaxTot3 = Val(GetItem(tmpTag, 1, ","))
                                        ArrBagTot3 = Val(GetItem(tmpTag, 2, ","))
                                        ArrUldTot3 = Val(GetItem(tmpTag, 3, ","))
                                        tmpTag = ArrCnxSum4(iArrBar).Tag
                                        ArrPaxTot4 = Val(GetItem(tmpTag, 1, ","))
                                        ArrBagTot4 = Val(GetItem(tmpTag, 2, ","))
                                        ArrUldTot4 = Val(GetItem(tmpTag, 3, ","))
                                        'Now we add the current values of the connex record
                                        ArrPaxTot1 = ArrPaxTot1 + CnxPaxTot1
                                        ArrPaxTot2 = ArrPaxTot2 + CnxPaxTot2
                                        ArrPaxTot3 = ArrPaxTot3 + CnxPaxTot3
                                        ArrPaxTot4 = ArrPaxTot4 + CnxPaxTot4
                                        ArrBagTot1 = ArrBagTot1 + CnxBagTot1
                                        ArrBagTot2 = ArrBagTot2 + CnxBagTot2
                                        ArrBagTot3 = ArrBagTot3 + CnxBagTot3
                                        ArrBagTot4 = ArrBagTot4 + CnxBagTot4
                                        ArrUldTot1 = ArrUldTot1 + CnxUldTot1
                                        ArrUldTot2 = ArrUldTot2 + CnxUldTot2
                                        ArrUldTot3 = ArrUldTot3 + CnxUldTot3
                                        ArrUldTot4 = ArrUldTot4 + CnxUldTot4
                                        'And push them back to the tags.
                                        tmpTag = CStr(ArrPaxTot1) & "," & CStr(ArrBagTot1) & "," & CStr(ArrUldTot1)
                                        ArrCnxSum1(iArrBar).Tag = tmpTag
                                        tmpTag = CStr(ArrPaxTot2) & "," & CStr(ArrBagTot2) & "," & CStr(ArrUldTot2)
                                        ArrCnxSum2(iArrBar).Tag = tmpTag
                                        tmpTag = CStr(ArrPaxTot3) & "," & CStr(ArrBagTot3) & "," & CStr(ArrUldTot3)
                                        ArrCnxSum3(iArrBar).Tag = tmpTag
                                        tmpTag = CStr(ArrPaxTot4) & "," & CStr(ArrBagTot4) & "," & CStr(ArrUldTot4)
                                        ArrCnxSum4(iArrBar).Tag = tmpTag
                                        
                                        'We need as well the totals per type
                                        'We'll attach as tag of the ArrLblPanels
                                        ArrPaxTot0 = ArrPaxTot1 + ArrPaxTot2 + ArrPaxTot3 + ArrPaxTot4
                                        lblArrTpax(iArrBar).Tag = CStr(ArrPaxTot0)
                                        ArrBagTot0 = ArrBagTot1 + ArrBagTot2 + ArrBagTot3 + ArrBagTot4
                                        lblArrTbag(iArrBar).Tag = CStr(ArrBagTot0)
                                        ArrUldTot0 = ArrUldTot1 + ArrUldTot2 + ArrUldTot3 + ArrUldTot4
                                        lblArrTuld(iArrBar).Tag = CStr(ArrUldTot0)
                                        
                                        'Note: Publishing the current values of the
                                        'type totals slows down the performance because
                                        'VB refreshes the label when you change the caption.
                                        'So it would be better to do this in a final loop.
                                        '
                                        If ArrPaxTot0 > 0 Then
                                            lblArrTpax(iArrBar).Caption = CStr(ArrPaxTot0)
                                            lblArrTpax(iArrBar).BackColor = LightGreen
                                        Else
                                            lblArrTpax(iArrBar).Caption = ""
                                            lblArrTpax(iArrBar).BackColor = LightGrey
                                        End If
                                        If ArrBagTot0 > 0 Then
                                            lblArrTbag(iArrBar).Caption = CStr(ArrBagTot0)
                                            lblArrTbag(iArrBar).BackColor = LightYellow
                                        Else
                                            lblArrTbag(iArrBar).Caption = ""
                                            lblArrTbag(iArrBar).BackColor = LightGrey
                                        End If
                                        If ArrUldTot0 > 0 Then
                                            lblArrTuld(iArrBar).Caption = CStr(ArrUldTot0)
                                            lblArrTuld(iArrBar).BackColor = LightBlue
                                        Else
                                            lblArrTuld(iArrBar).Caption = ""
                                            lblArrTuld(iArrBar).BackColor = LightGrey
                                        End If
                                        
                                        If CnxFilterType > 0 Then
                                            iItm = CnxFilterType
                                            iBar = iArrBar
                                            ArrCnxSum1(iBar).Caption = GetItem(ArrCnxSum1(iBar).Tag, iItm, ",")
                                            ArrCnxSum2(iBar).Caption = GetItem(ArrCnxSum2(iBar).Tag, iItm, ",")
                                            ArrCnxSum3(iBar).Caption = GetItem(ArrCnxSum3(iBar).Tag, iItm, ",")
                                            ArrCnxSum4(iBar).Caption = GetItem(ArrCnxSum4(iBar).Tag, iItm, ",")
                                            If ArrCnxSum1(iBar).Caption = "0" Then ArrCnxSum1(iBar).Caption = ""
                                            If ArrCnxSum2(iBar).Caption = "0" Then ArrCnxSum2(iBar).Caption = ""
                                            If ArrCnxSum3(iBar).Caption = "0" Then ArrCnxSum3(iBar).Caption = ""
                                            If ArrCnxSum4(iBar).Caption = "0" Then ArrCnxSum4(iBar).Caption = ""
                                        End If
                                        
                                        lblCnxPaxBar(iArrBar).BackColor = lblArrTpax(iArrBar).BackColor
                                        lblCnxBagBar(iArrBar).BackColor = lblArrTbag(iArrBar).BackColor
                                        lblCnxUldBar(iArrBar).BackColor = lblArrTuld(iArrBar).BackColor
                                        '
                                        'End of comment. See above.
                                        
                                        LinColor = vbBlack
                                        
                                        'THIS PART TO BE MODIFIED!!
                                        tmpData = TabDepCnxDetails.GetFieldValue(PaxCfiLine, "SCID")
                                        ArrCnxCsct(iArrBar).BackColor = vbWhite
                                        ArrCnxCsct(iArrBar).ForeColor = vbBlack
                                        picRightBarCnx(iArrBar).Picture = CnxArrIcon(6).Picture
                                        If tmpData <> "" Then
                                            LinColor = vbRed
                                            ArrCnxCsct(iArrBar).BackColor = LinColor
                                            ArrCnxCsct(iArrBar).ForeColor = vbWhite
                                            picRightBarCnx(iArrBar).Picture = CnxArrIcon(5).Picture
                                            If InStr(tmpCnxTag, "S") = 0 Then tmpCnxTag = tmpCnxTag & "S"
                                        End If
                                        tmpData = TabDepCnxDetails.GetFieldValue(PaxCfiLine, "CTMT")
                                        ArrCnxCsct(iArrBar).Caption = tmpData
                                        If tmpData <> "" Then
                                            tmpData = Replace(tmpData, "H", ":", 1, -1, vbBinaryCompare)
                                            If InStr(tmpData, ":") > 0 Then
                                                If Left(tmpData, 1) = "-" Then
                                                    CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) - Val(GetItem(tmpData, 2, ":"))
                                                Else
                                                    CurCTim = (Val(GetItem(tmpData, 1, ":")) * 60) + Val(GetItem(tmpData, 2, ":"))
                                                End If
                                            Else
                                                CurCTim = Val(tmpData)
                                            End If
                                            If CurCTim <= 0 Then
                                                LinColor = vbCyan
                                                ArrCnxCsct(iArrBar).BackColor = LinColor
                                                ArrCnxCsct(iArrBar).ForeColor = vbBlack
                                                picRightBarCnx(iArrBar).Picture = CnxArrIcon(3).Picture
                                            End If
                                        End If
                                        'SEE ABOVE
                                        
                                        If ShowArr = True Then
                                            
                                            Y2 = ArrLblPanel(iArrBar).Top + 15
                                            Select Case CnxFilterType
                                                Case 0
                                                    Y2 = Y2 + lblArrTbag(iArrBar).Top + (lblArrTbag(iArrBar).Height \ 2)
                                                    NewBarTop = Y2 - (lblCnxBagBar(iArrBar).Height \ 2) - lblCnxPaxBar(iArrBar).Height + 15
                                                Case 1
                                                    Y2 = Y2 + lblArrTpax(iArrBar).Top + (lblArrTpax(iArrBar).Height \ 2)
                                                    NewBarTop = Y2 - (lblCnxPaxBar(iArrBar).Height \ 2) - 15
                                                Case 2
                                                    Y2 = Y2 + lblArrTbag(iArrBar).Top + (lblArrTbag(iArrBar).Height \ 2)
                                                    NewBarTop = Y2 - (lblCnxBagBar(iArrBar).Height \ 2) - lblCnxPaxBar(iArrBar).Height + 15
                                                Case 3
                                                    Y2 = Y2 + lblArrTuld(iArrBar).Top + (lblArrTuld(iArrBar).Height \ 2)
                                                    NewBarTop = Y2 - (lblCnxUldBar(iArrBar).Height \ 2) - lblCnxBagBar(iArrBar).Height - lblCnxPaxBar(iArrBar).Height + 30
                                                Case Else
                                            End Select
                                            picLeftBarCnx(iArrBar).Top = Y2 - (picLeftBarCnx(iArrBar).Height \ 2)
                                            picRightBarCnx(iArrBar).Top = picLeftBarCnx(iArrBar).Top
                                            lblCnxPaxBar(iArrBar).Top = NewBarTop - 15
                                            lblCnxBagBar(iArrBar).Top = lblCnxPaxBar(iArrBar).Top + lblCnxPaxBar(iArrBar).Height - 15
                                            lblCnxUldBar(iArrBar).Top = lblCnxBagBar(iArrBar).Top + lblCnxBagBar(iArrBar).Height - 15
                                            Y2 = Y2 - 15    'Need a correction to fit the layout of arrival connex
                                            
                                            tmpArrTag = picRightBarCnx(iArrBar).Tag
                                            If CnxFilterType > 0 Then
                                                picLeftBarCnx(iArrBar).Picture = CnxArrIcon(0).Picture
                                                If InStr(tmpCnxTag, "C") > 0 Then
                                                    picLeftBarCnx(iArrBar).Picture = CnxArrIcon(4).Picture
                                                    If InStr(tmpArrTag, "S") = 0 Then tmpArrTag = tmpArrTag & "S"
                                                ElseIf InStr(tmpCnxTag, "S") > 0 Then
                                                    picLeftBarCnx(iArrBar).Picture = CnxArrIcon(1).Picture
                                                    If InStr(tmpArrTag, "C") = 0 Then tmpArrTag = tmpArrTag & "C"
                                                End If
                                            Else
                                                picLeftBarCnx(iArrBar).Picture = picSmallIcon(0).Picture
                                                If InStr(tmpCnxTag, "C") > 0 Then
                                                    picLeftBarCnx(iArrBar).Picture = picSmallIcon(6).Picture
                                                    If InStr(tmpArrTag, "S") = 0 Then tmpArrTag = tmpArrTag & "S"
                                                ElseIf InStr(tmpCnxTag, "S") > 0 Then
                                                    picLeftBarCnx(iArrBar).Picture = picSmallIcon(1).Picture
                                                    If InStr(tmpArrTag, "C") = 0 Then tmpArrTag = tmpArrTag & "C"
                                                End If
                                            End If
                                            picRightBarCnx(iArrBar).Tag = tmpArrTag
                                            ArrLblPanel(iArrBar).Left = 0
                                            
                                            picRightBarCnx(iArrBar).Left = X2 - (picRightBarCnx(iArrBar).Width \ 2)
                                            
                                            
                                            Y2A = Y2 - 15
                                            Y2B = Y2 + 15
                                            Y4A = ArrLblPanel(iArrBar).Top '+ 15
                                            Y4B = ArrLblPanel(iArrBar).Top + ArrLblPanel(iArrBar).Height - 15
                                            If iArrBar > BotIndex Then BotIndex = iArrBar
                                            If iArrBar < TopIndex Then TopIndex = iArrBar
                                            If CnxLayoutType <= 1 Then
                                                If LinColor = vbCyan Then MidScConx(0).Line (x1, Y1B)-(X3, Y2B), vbBlack
                                                If IsShort Then
                                                    MidScConx(0).Line (x1, Y1A)-(X3, Y2A), vbYellow
                                                    MidScConx(0).Line (x1, Y1B)-(X3, Y2B), vbRed
                                                End If
                                                MidScConx(0).Line (x1, y1)-(X3, Y2), LinColor
                                                If CnxFilterType > 0 Then
                                                    MidScConx(0).Line (X4, Y2)-(X3, Y2), LinColor
                                                    MidScConx(0).Line (X4, Y2B)-(X3, Y2B), vbBlack
                                                Else
                                                    MidScConx(0).Line (X3, Y2)-(X4, Y4A), vbBlack
                                                    MidScConx(0).Line (X3, Y2)-(X4, Y4B), vbBlack
                                                End If
                                            Else
                                                If CnxLayoutType = 4 Then
                                                    If LinColor <> vbBlack Then
                                                        If Y2 >= y1 Then
                                                            MidScConx(0).Line (x1 + 15, y1)-(X2 + 15, Y2), vbBlack
                                                        Else
                                                            MidScConx(0).Line (x1 - 15, y1)-(X2 - 15, Y2), vbBlack
                                                        End If
                                                    End If
                                                    MidScConx(0).Line (x1, y1)-(X2, Y2), LinColor
                                                End If
                                                If CnxFilterType > 0 Then
                                                    MidScConx(0).Line (X2, Y2)-(0, Y2), vbBlack
                                                    MidScConx(0).Line (X2, Y2A)-(0, Y2A), LinColor
                                                Else
                                                    MidScConx(0).Line (X2, Y2)-(X3, Y2), vbBlack
                                                    MidScConx(0).Line (X2, Y2A)-(X3, Y2A), LinColor
                                                    MidScConx(0).Line (X3, Y2A)-(X4, Y4A), vbBlack
                                                    MidScConx(0).Line (X3, Y2)-(X4, Y4B), vbBlack
                                                End If
                                                SetVisible = True
                                                NewWidth = CurCTim * 30
                                                If NewWidth < 0 Then
                                                    NewWidth = 0
                                                    SetVisible = False
                                                End If
                                                NewLeft = X2 - NewWidth
                                                If NewLeft < (X3 + 150) Then
                                                    NewLeft = X3 + 150
                                                    NewWidth = X2 - X3 - 150
                                                End If
                                                lblCnxPaxBar(iArrBar).Width = NewWidth
                                                lblCnxBagBar(iArrBar).Width = NewWidth
                                                lblCnxUldBar(iArrBar).Width = NewWidth
                                                lblCnxPaxBar(iArrBar).Left = NewLeft
                                                lblCnxBagBar(iArrBar).Left = NewLeft
                                                lblCnxUldBar(iArrBar).Left = NewLeft
                                                lblCnxPaxBar(iArrBar).Visible = SetVisible
                                                lblCnxBagBar(iArrBar).Visible = SetVisible
                                                lblCnxUldBar(iArrBar).Visible = SetVisible
                                                If CnxLayoutType > 2 Then
                                                    ArrInfoPanel(iArrBar).Left = LeftLine - ArrCnxCsct(iArrBar).Width - 15
                                                    ArrInfoPanel(iArrBar).Top = Y2 - (ArrInfoPanel(iArrBar).Height \ 2)
                                                    ArrInfoPanel(iArrBar).Visible = True
                                                End If
                                                picRightBarCnx(iArrBar).Visible = True
                                            End If
                                            ArrLblPanel(iArrBar).Visible = True
                                            picLeftBarCnx(iArrBar).Left = X3 - 120
                                            picLeftBarCnx(iArrBar).Visible = True
                                        End If
                                    Else
                                        ErrCase = 3
                                        ErrCnt1 = ErrCnt1 + 1
                                    End If
                                Else
                                    ErrCase = 2
                                    ErrCnt1 = ErrCnt1 + 1
                                End If
                            Else
                                ErrCase = 1
                                ErrCnt1 = ErrCnt1 + 1
                            End If
                        End If
                        If ErrCase > 0 Then
                            'If CnxLayoutType > 0 Then CreateArrInfoRec PaxCfiLine, ErrCase
                            ShowDep = True
                        End If
                        SetInfoPanelColors iArrBar, iDepBar
                        PaxCfiLine = TabDepCnxDetails.GetNextResultLine
                    Wend
                End If
                TabDepCnxDetails.SetInternalLineBuffer False
                
                If ShowDep = True Then
                    'Keep the totals per type and class in the Dep connex info tags.
                    tmpTag = CStr(DepPaxTot1) & "," & CStr(DepBagTot1) & "," & CStr(DepUldTot1)
                    DepCnxSum1(iDepBar).Tag = tmpTag
                    tmpTag = CStr(DepPaxTot2) & "," & CStr(DepBagTot2) & "," & CStr(DepUldTot2)
                    DepCnxSum2(iDepBar).Tag = tmpTag
                    tmpTag = CStr(DepPaxTot3) & "," & CStr(DepBagTot3) & "," & CStr(DepUldTot3)
                    DepCnxSum3(iDepBar).Tag = tmpTag
                    tmpTag = CStr(DepPaxTot4) & "," & CStr(DepBagTot4) & "," & CStr(DepUldTot4)
                    DepCnxSum4(iDepBar).Tag = tmpTag
                    
                    DepPaxTot0 = DepPaxTot1 + DepPaxTot2 + DepPaxTot3 + DepPaxTot4
                    If DepPaxTot0 > 0 Then
                        lblDepTpax(iDepBar).Caption = CStr(DepPaxTot0)
                        lblDepTpax(iDepBar).BackColor = LightGreen
                        lblDepTpax(iDepBar).MousePointer = 99
                    Else
                        lblDepTpax(iDepBar).Caption = ""
                        lblDepTpax(iDepBar).BackColor = LightGrey
                        lblDepTpax(iDepBar).MousePointer = 0
                    End If
                    DepBagTot0 = DepBagTot1 + DepBagTot2 + DepBagTot3 + DepBagTot4
                    If DepBagTot0 > 0 Then
                        lblDepTbag(iDepBar).Caption = CStr(DepBagTot0)
                        lblDepTbag(iDepBar).BackColor = LightYellow
                        lblDepTbag(iDepBar).MousePointer = 99
                    Else
                        lblDepTbag(iDepBar).Caption = ""
                        lblDepTbag(iDepBar).BackColor = LightGrey
                        lblDepTbag(iDepBar).MousePointer = 0
                    End If
                    DepUldTot0 = DepUldTot1 + DepUldTot2 + DepUldTot3 + DepUldTot4
                    If DepUldTot0 > 0 Then
                        lblDepTuld(iDepBar).Caption = CStr(DepUldTot0)
                        lblDepTuld(iDepBar).BackColor = LightBlue
                        lblDepTuld(iDepBar).MousePointer = 99
                    Else
                        lblDepTuld(iDepBar).Caption = ""
                        lblDepTuld(iDepBar).BackColor = LightGrey
                        lblDepTuld(iDepBar).MousePointer = 0
                    End If
                    DepLblPanel(iDepBar).Visible = True
                    
                    If (CnxFilterType > 0) And (CnxLayoutType > 2) Then
                        iItm = CnxFilterType
                        iBar = iDepBar
                        DepCnxSum1(iBar).Caption = GetItem(DepCnxSum1(iBar).Tag, iItm, ",")
                        DepCnxSum2(iBar).Caption = GetItem(DepCnxSum2(iBar).Tag, iItm, ",")
                        DepCnxSum3(iBar).Caption = GetItem(DepCnxSum3(iBar).Tag, iItm, ",")
                        DepCnxSum4(iBar).Caption = GetItem(DepCnxSum4(iBar).Tag, iItm, ",")
                        If DepCnxSum1(iBar).Caption = "0" Then DepCnxSum1(iBar).Caption = ""
                        If DepCnxSum2(iBar).Caption = "0" Then DepCnxSum2(iBar).Caption = ""
                        If DepCnxSum3(iBar).Caption = "0" Then DepCnxSum3(iBar).Caption = ""
                        If DepCnxSum4(iBar).Caption = "0" Then DepCnxSum4(iBar).Caption = ""
                        DepInfoPanel(iDepBar).Visible = True
                        MidScConx(0).Line (x0, y1)-(DepInfoPanel(iDepBar).Left, y1), vbBlack
                        MidScConx(0).Line (x0, Y1B)-(DepInfoPanel(iDepBar).Left, Y1B), vbBlack
                    End If
                    
                    If CnxFilterType > 0 Then
                        MidScConx(0).Line (x1, y1)-(x0, y1), vbBlack
                        MidScConx(0).Line (x1, Y1B)-(x0, Y1B), vbBlack
                        picRightBarCnx(iDepBar).Picture = picSmallIcon(0).Picture
                        picDepCnx(iDepBar).Visible = True
                    Else
                        MidScConx(0).Line (x1, Y1B)-(x0, Y0A), vbBlack
                        MidScConx(0).Line (x1, Y1B)-(x0, Y0B), vbBlack
                        picRightBarCnx(iDepBar).Picture = picSmallIcon(0).Picture
                    End If
                    picRightBarCnx(iDepBar).Visible = True
                    
                End If
                Select Case CnxLayoutType
                    Case 0, 1
                    Case 2, 3, 4
                        Y1Min = ArrLblPanel(BotIndex).Top - 120
                        Y1Max = ArrLblPanel(BotIndex).Top + ArrLblPanel(BotIndex).Height + 120
                        DrawTimeLines LeftLine, Y1Max, 0, vbBlack
                        DrawTimeLines RightLine, Y1Max, MidScConx(0).ScaleWidth - 30, vbBlack
                        lblLeftTime(0).Left = LeftLine + MidScCon1(0).Left + 60
                        lblLeftDate(0).Left = lblLeftTime(0).Left - lblLeftDate(0).Width
                        lblRightTime(0).Left = RightLine - lblRightTime(0).Width + MidScCon1(0).Left + 45
                        lblRightDate(0).Left = RightLine + MidScCon1(0).Left + 45
                        lblLeftDate(0).Visible = True
                        lblLeftTime(0).Visible = True
                        lblRightTime(0).Visible = True
                        lblRightDate(0).Visible = True
                    Case Else
                End Select
                
                If (ErrCnt1 + ErrCnt2) > 0 Then
                    picRightBarCnx(iDepBar).Picture = picInfoPics(1)
                    If ErrCnt1 = 1 Then
                        tmpData = "--> " & CStr(ErrCnt1) & " Arrival Flight Is Too Far In The Past."
                    End If
                    If ErrCnt1 > 1 Then
                        tmpData = "--> " & CStr(ErrCnt1) & " Arrival Flights Are Too Far In The Past."
                    End If
                    picRightBarCnx(iDepBar).ToolTipText = tmpData
                End If
                'If TabArrInfo(0).GetLineCount > 0 Then
                '    TabArrInfo(0).AutoSizeColumns
                '    TabArrInfo(0).Sort "2", True, True
                '    TabArrInfo(0).Visible = True
                'End If
                tmpData = ""
                tmpData = tmpData & "{X3}" & CStr(X3) & "{/X3}"
                tmpData = tmpData & "{H}" & CStr(CurCnxLayoutHours) & "{/H}"
                MidScConx(0).Tag = tmpData
                If CnxLayoutType = 0 Then
                    If ShowDep = True Then
                        picDepCnxCnt(0).Tag = CStr(Val(picDepCnxCnt(0).Tag) + 1)
                        picDepCnxCnt(0).Picture = imgLookUpA
                    End If
                    If ShowArr = True Then
                        picArrCnxCnt(0).Tag = CStr(Val(picArrCnxCnt(0).Tag) + 1)
                        picArrCnxCnt(0).Picture = imgLookUpB
                    End If
                End If
            End If
        End If
    End If
    MidScConx(0).ZOrder
End Sub
Private Sub DrawTimeLines(LineLeft As Long, LineHeight As Long, LineBottom As Long, LineColor As Long)
    MidScConx(0).Line (LineLeft, 0)-(LineLeft + 15, LineHeight), LineColor, BF
    MidScConx(0).Line (LineLeft + 30, 0)-(LineLeft + 30, LineHeight), vbWhite
    MidScConx(0).Line (LineLeft, LineHeight)-(LineBottom, MidScConx(0).ScaleHeight), LineColor
    MidScConx(0).Line (LineLeft + 15, LineHeight)-(LineBottom + 15, MidScConx(0).ScaleHeight), LineColor
    MidScConx(0).Line (LineLeft + 30, LineHeight)-(LineBottom + 30, MidScConx(0).ScaleHeight), vbWhite
End Sub
Private Sub SetInfoPanelColors(iArrBar As Integer, iDepBar As Integer)
    Dim SetBackColor As Long
    Dim SetTypeColor As Long
    If iArrBar >= 0 Then
        SetTypeColor = vbWhite
        SetBackColor = SetTypeColor
        If CnxFilterType = 0 Then SetBackColor = LightGrey
        If CnxFilterClass > 0 Then SetBackColor = LightGrey
        ArrCnxSum1(iArrBar).BackColor = SetBackColor
        ArrCnxSum2(iArrBar).BackColor = SetBackColor
        ArrCnxSum3(iArrBar).BackColor = SetBackColor
        ArrCnxSum4(iArrBar).BackColor = SetBackColor
        ArrCnxSum1(iArrBar).ForeColor = vbBlack
        ArrCnxSum2(iArrBar).ForeColor = vbBlack
        ArrCnxSum3(iArrBar).ForeColor = vbBlack
        ArrCnxSum4(iArrBar).ForeColor = vbBlack
        If CnxFilterClass > 0 Then SetTypeColor = vbWhite
        Select Case CnxFilterClass
            Case 1
                ArrCnxSum1(iArrBar).BackColor = SetTypeColor
            Case 2
                ArrCnxSum2(iArrBar).BackColor = SetTypeColor
            Case 3
                ArrCnxSum3(iArrBar).BackColor = SetTypeColor
            Case 4
                ArrCnxSum4(iArrBar).BackColor = SetTypeColor
            Case Else
        End Select
    End If
    If iDepBar >= 0 Then
        SetTypeColor = vbWhite
        SetBackColor = SetTypeColor
        If CnxFilterType = 0 Then SetBackColor = LightGrey
        If CnxFilterClass > 0 Then SetBackColor = LightGrey
        DepCnxSum1(iDepBar).BackColor = SetBackColor
        DepCnxSum2(iDepBar).BackColor = SetBackColor
        DepCnxSum3(iDepBar).BackColor = SetBackColor
        DepCnxSum4(iDepBar).BackColor = SetBackColor
        DepCnxSum1(iDepBar).ForeColor = vbBlack
        DepCnxSum2(iDepBar).ForeColor = vbBlack
        DepCnxSum3(iDepBar).ForeColor = vbBlack
        DepCnxSum4(iDepBar).ForeColor = vbBlack
        If CnxFilterType > 0 Then SetTypeColor = vbWhite
        Select Case CnxFilterClass
            Case 1
                DepCnxSum1(iDepBar).BackColor = SetTypeColor
            Case 2
                DepCnxSum2(iDepBar).BackColor = SetTypeColor
            Case 3
                DepCnxSum3(iDepBar).BackColor = SetTypeColor
            Case 4
                DepCnxSum4(iDepBar).BackColor = SetTypeColor
            Case Else
        End Select
    End If
End Sub

Private Sub CreateDepInfoRec(CnxLine As Long, ForWhat As Integer)
    Dim tmpData As String
    Dim tmpRec As String
    '
    'TO BE MODIFIED USING TabArrCnxDetails FOR TYPE TOTALS!
    '
    'We must fetch each single field
    'because tab.ocx has a bug in GetFieldValues:
    'Creates a corrupt data list when a grid field is unknown
    tmpRec = ""
    'DepInfoFields = "AFSD,FLND,STOD,TIFD,DES3,PSTD,SCID,TPAX,TBAG,TULD,REMA,DURN"
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "AFSD") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "FLND") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "STOD") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "DES3") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "PSTD") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "SCID") & ","
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "TALL") & ","
    tmpRec = tmpRec & "" & ","  'TBAG
    tmpRec = tmpRec & "" & ","  'TULD
    tmpRec = tmpRec & "" & ","  'REMA
    tmpRec = tmpRec & TabArrCnxPaxTab.GetFieldValue(CnxLine, "DURN")
    TabDepInfo(0).InsertTextLine tmpRec, False
End Sub
Private Sub ClearConnexDetails()
    Dim iBar As Integer
    MouseTimer(0).Enabled = False
    MouseTimer(1).Enabled = False
    MouseTimer(0).Tag = ""
    MouseTimer(1).Tag = ""
    
    For iBar = 0 To CurBarMax
        ArrFlight(iBar).BackColor = LightGrey
        DepFlight(iBar).BackColor = LightGrey
        ArrInfoPanel(iBar).Visible = False
        DepInfoPanel(iBar).Visible = False
        lblCnxPaxBar(iBar).Visible = False
        lblCnxBagBar(iBar).Visible = False
        lblCnxUldBar(iBar).Visible = False
        picLeftBarCnx(iBar).Visible = False
        ArrLblPanel(iBar).Visible = False
        picRightBarCnx(iBar).Visible = False
        picDepCnx(iBar).Visible = False
        DepLblPanel(iBar).Visible = False
        If picRightBarCnx(iBar).Tag <> "" Then picRightBarCnx(iBar).Picture = picSmallIcon(0).Picture
        picRightBarCnx(iBar).Tag = ""
        
        ArrCnxSum1(iBar).Tag = "0,0,0"
        ArrCnxSum2(iBar).Tag = "0,0,0"
        ArrCnxSum3(iBar).Tag = "0,0,0"
        ArrCnxSum4(iBar).Tag = "0,0,0"
        ArrCnxSum1(iBar).Caption = ""
        ArrCnxSum2(iBar).Caption = ""
        ArrCnxSum3(iBar).Caption = ""
        ArrCnxSum4(iBar).Caption = ""
        
        DepCnxSum1(iBar).Tag = "0,0,0"
        DepCnxSum2(iBar).Tag = "0,0,0"
        DepCnxSum3(iBar).Tag = "0,0,0"
        DepCnxSum4(iBar).Tag = "0,0,0"
        DepCnxSum1(iBar).Caption = ""
        DepCnxSum2(iBar).Caption = ""
        DepCnxSum3(iBar).Caption = ""
        DepCnxSum4(iBar).Caption = ""
        
        lblArrTpax(iBar).Tag = "0"
        lblArrTbag(iBar).Tag = "0"
        lblArrTuld(iBar).Tag = "0"
        lblArrTpax(iBar).Caption = ""
        lblArrTbag(iBar).Caption = ""
        lblArrTuld(iBar).Caption = ""
        lblDepTpax(iBar).Tag = "0"
        lblDepTbag(iBar).Tag = "0"
        lblDepTuld(iBar).Tag = "0"
        lblDepTpax(iBar).Caption = ""
        lblDepTbag(iBar).Caption = ""
        lblDepTuld(iBar).Caption = ""
    Next
    MidScConx(0).Cls
    lblLeftDate(0).Visible = False
    lblLeftTime(0).Visible = False
    lblRightTime(0).Visible = False
    lblRightDate(0).Visible = False
    'SelArrPanel(0).Visible = False
    'SelDepPanel(0).Visible = False
    TabDepInfo(0).Visible = False
    TabDepInfo(0).ResetContent
    TabDepInfo(0).Refresh
    picArrCnxCnt(0).Tag = "0"
    picArrCnxCnt(0).Picture = picNeutral(0).Picture
    picDepCnxCnt(0).Tag = "0"
    picDepCnxCnt(0).Picture = picNeutral(0).Picture
    picArrCnxCnt(0).Refresh
    picDepCnxCnt(0).Refresh
    
    CurCnxView = -1
End Sub

Private Sub ComposeChartTitle()
    Dim tmpTitle As String
    Dim tmpText As String
    Dim tmpAdid As String
    Dim tmpType As String
    Dim tmpClass As String
    Dim tmpShort As String
    Dim tmpCritical As String
    tmpText = ""
    Select Case CnxFilterAdid
        Case 0
            tmpAdid = "Arrival"
        Case 1
            tmpAdid = "Departure"
        Case Else
            tmpAdid = ""
    End Select
    Select Case CnxFilterType
        Case 1
            tmpType = "PAX"
        Case 2
            tmpType = "BAG"
        Case 3
            tmpType = "ULD"
        Case Else
            tmpType = "Connections"
    End Select
    Select Case CnxFilterClass
        Case 1
            tmpClass = "First Class"
        Case 2
            tmpClass = "Business Class"
        Case 3
            tmpClass = "Economy Class"
        Case 4
            tmpClass = "Premium Economy"
        Case Else
            tmpClass = ""
    End Select
    tmpCritical = ""
    If (CnxFilterShort > 0) And (CnxFilterCritical > 0) Then
        tmpShort = "Critical Short Connections"
    ElseIf CnxFilterShort > 0 Then
        tmpShort = "Short Connections"
    ElseIf CnxFilterCritical > 0 Then
        tmpCritical = "Critical Connections"
    Else
        tmpShort = ""
    End If
    
    If CnxLayoutType < 1 Then
        'tmpText = "Transfer Connections"
    Else
        'tmpText = "Transfer"
    End If
    
    tmpText = "Transfer"
    Select Case CnxLayoutType
        'Case 0
        Case -2
            tmpText = tmpText & " (" & tmpAdid & ")"
        Case Else
            tmpText = tmpText & " " & tmpType
            tmpText = tmpText & " (" & tmpAdid & ")"
            If tmpClass <> "" Then tmpText = tmpText & " " & tmpClass
            If tmpShort <> "" Then tmpText = tmpText & " " & tmpShort
            If tmpCritical <> "" Then tmpText = tmpText & " " & tmpCritical
    End Select
    
    lblTabCaption(0).Caption = tmpText
    lblTabCaptionShadow(0).Caption = tmpText
    
    'If chkSelDeco(2).Value = 0 Then
        tmpTitle = Me.Caption
        tmpTitle = GetItem(tmpTitle, 1, "[")
        tmpTitle = tmpTitle & "  [" & tmpText & "]"
        Me.Caption = tmpTitle
    'End If
    
End Sub

Private Sub SetCnxArrTimeScale(iBar)
    Dim tmpTag As String
    Dim tmpCfiTifa As String
    Dim tmpAftTifa As String
    Dim tmpDate As String
    Dim tmpRange As String
    If iBar >= 0 Then
        tmpTag = ArrFlight(iBar).Tag
        GetKeyItem tmpCfiTifa, tmpTag, "{CFITIFA}", "{/CFITIFA}"
        GetKeyItem tmpAftTifa, tmpTag, "{AFTTIFA}", "{/AFTTIFA}"
        If tmpCfiTifa = "" Then tmpCfiTifa = tmpAftTifa
        lblLeftTime(0).Tag = tmpCfiTifa
        SetTimeFieldCaption lblLeftTime(0), CurUtcTimeDiff
        tmpDate = CedaDateTimeAdd(tmpCfiTifa, CLng(CurUtcTimeDiff))
        tmpDate = Left(tmpDate, 8)
        tmpDate = DecodeSsimDayFormat(tmpDate, "CEDA", "SSIM2")
        'tmpDate = Left(tmpDate, 5)
        lblLeftDate(0).Caption = tmpDate
        
        tmpRange = CedaDateTimeAdd(tmpCfiTifa, CnxRangeMin)
        lblRightTime(0).Tag = tmpRange
        SetTimeFieldCaption lblRightTime(0), CurUtcTimeDiff
        tmpDate = CedaDateTimeAdd(tmpRange, CLng(CurUtcTimeDiff))
        tmpDate = Left(tmpDate, 8)
        tmpDate = DecodeSsimDayFormat(tmpDate, "CEDA", "SSIM2")
        lblRightDate(0).Caption = tmpDate
    End If
End Sub
Private Sub SetCnxDepTimeScale(iBar)
    Dim tmpTag As String
    Dim tmpCfiTifd As String
    Dim tmpAftTifd As String
    Dim tmpDate As String
    Dim tmpRange As String
    If iBar >= 0 Then
        tmpTag = DepFlight(iBar).Tag
        GetKeyItem tmpCfiTifd, tmpTag, "{CFITIFD}", "{/CFITIFD}"
        GetKeyItem tmpAftTifd, tmpTag, "{AFTTIFD}", "{/AFTTIFD}"
        If tmpCfiTifd = "" Then tmpCfiTifd = tmpAftTifd
        lblRightTime(0).Tag = tmpCfiTifd
        SetTimeFieldCaption lblRightTime(0), CurUtcTimeDiff
        tmpDate = CedaDateTimeAdd(tmpCfiTifd, CLng(CurUtcTimeDiff))
        tmpDate = Left(tmpDate, 8)
        tmpDate = DecodeSsimDayFormat(tmpDate, "CEDA", "SSIM2")
        lblRightDate(0).Caption = tmpDate
        
        tmpRange = CedaDateTimeAdd(tmpCfiTifd, -CnxRangeMin)
        lblLeftTime(0).Tag = tmpRange
        SetTimeFieldCaption lblLeftTime(0), CurUtcTimeDiff
        tmpDate = CedaDateTimeAdd(tmpRange, CLng(CurUtcTimeDiff))
        tmpDate = Left(tmpDate, 8)
        tmpDate = DecodeSsimDayFormat(tmpDate, "CEDA", "SSIM2")
        lblLeftDate(0).Caption = tmpDate
    End If
End Sub

