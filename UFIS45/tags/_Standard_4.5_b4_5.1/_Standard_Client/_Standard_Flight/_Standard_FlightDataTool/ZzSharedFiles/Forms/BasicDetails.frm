VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form BasicDetails 
   Caption         =   "Airport Traffic Restrictions (Curfew)"
   ClientHeight    =   5460
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7995
   Icon            =   "BasicDetails.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5460
   ScaleWidth      =   7995
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtAtrTab 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   6
      Left            =   90
      TabIndex        =   11
      Top             =   4740
      Width           =   6315
   End
   Begin VB.TextBox txtAtrTab 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3195
      Index           =   5
      Left            =   90
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   1440
      Width           =   6315
   End
   Begin VB.TextBox txtAtrTab 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   4
      Left            =   90
      TabIndex        =   9
      Top             =   990
      Width           =   6315
   End
   Begin VB.TextBox txtAtrTab 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   90
      TabIndex        =   8
      Top             =   600
      Width           =   6315
   End
   Begin VB.TextBox txtAtrTab 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   1590
      TabIndex        =   7
      Top             =   150
      Width           =   4815
   End
   Begin VB.TextBox txtAtrTab 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   810
      TabIndex        =   6
      Top             =   150
      Width           =   675
   End
   Begin VB.TextBox txtAtrTab 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   90
      TabIndex        =   5
      Top             =   150
      Width           =   645
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   3
      Top             =   5145
      Width           =   7995
      _ExtentX        =   14102
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11007
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox RightPanel 
      Align           =   4  'Align Right
      AutoRedraw      =   -1  'True
      Height          =   5145
      Left            =   6525
      ScaleHeight     =   5085
      ScaleWidth      =   1410
      TabIndex        =   0
      Top             =   0
      Width           =   1470
      Begin VB.CheckBox chkTool 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   6
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   17
         Tag             =   "ABOUT"
         Top             =   1020
         Width           =   1365
      End
      Begin VB.CheckBox chkTool 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   16
         Tag             =   "ABOUT"
         Top             =   690
         Width           =   675
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Save"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   15
         Tag             =   "ABOUT"
         Top             =   690
         Width           =   675
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   14
         Tag             =   "ABOUT"
         Top             =   360
         Width           =   675
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   13
         Tag             =   "ABOUT"
         Top             =   360
         Width           =   675
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Read"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   4
         Tag             =   "ABOUT"
         Top             =   30
         Width           =   675
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Excel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   1
         Tag             =   "ABOUT"
         Top             =   30
         Width           =   675
      End
      Begin TABLib.TAB AtrTab 
         Height          =   1245
         Index           =   0
         Left            =   0
         TabIndex        =   2
         Top             =   1410
         Width           =   1275
         _Version        =   65536
         _ExtentX        =   2249
         _ExtentY        =   2196
         _StockProps     =   64
      End
      Begin TABLib.TAB AtrTab 
         Height          =   1335
         Index           =   1
         Left            =   0
         TabIndex        =   12
         Top             =   2820
         Visible         =   0   'False
         Width           =   1275
         _Version        =   65536
         _ExtentX        =   2249
         _ExtentY        =   2355
         _StockProps     =   64
      End
   End
End
Attribute VB_Name = "BasicDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents MsExcel As Excel.Application
Attribute MsExcel.VB_VarHelpID = -1
Dim ExcelJustOpened As Boolean
Dim ExcelIsOpen As Boolean
Dim ExcelShutDown As Boolean
Dim MsWkBook As Workbook
Dim MsWkSheet As Worksheet
Dim DontCheck As Boolean
Dim PrvTabLine As Long

Private Sub AtrTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpData As String
    Dim tmpTag As String
    Dim idx As Integer
    Dim ColNo As Long
    If Index = 0 Then
        If (Selected = True) And (LineNo >= 0) Then
            AtrTab(0).SetFocus
            If PrvTabLine >= 0 Then
                For ColNo = 0 To 6
                    idx = CInt(ColNo)
                    tmpData = Trim(txtAtrTab(idx).Text)
                    tmpData = CleanString(tmpData, FOR_SERVER, False)
                    tmpTag = AtrTab(0).GetColumnValue(PrvTabLine, ColNo)
                    If tmpData <> tmpTag Then
                        AtrTab(0).SetColumnValue PrvTabLine, ColNo, tmpData
                        AtrTab(0).SetLineStatusValue PrvTabLine, 98
                        AtrTab(0).SetLineColor PrvTabLine, vbBlack, vbYellow
                    End If
                Next
                PrvTabLine = -1
            End If
            For ColNo = 0 To 6
                DontCheck = True
                idx = CInt(ColNo)
                tmpData = AtrTab(0).GetColumnValue(LineNo, ColNo)
                tmpData = CleanString(tmpData, FOR_CLIENT, False)
                txtAtrTab(idx).Text = tmpData
                txtAtrTab(idx).Tag = tmpData
                txtAtrTab(idx).BackColor = vbWhite
                DontCheck = False
            Next
            PrvTabLine = LineNo
        End If
    End If
End Sub

Private Sub chkTool_Click(Index As Integer)
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        Select Case Index
            Case 0  'Excel
                HandleExcel False
            Case 1  'Get Excel Data
                If chkTool(0).Value = 1 Then
                    GetExcelData
                Else
                    chkTool(Index).Value = 0
                End If
            Case 2  'Read ATRTAB
                If (chkTool(3).Value = 1) Or (chkTool(1).Value = 1) Then
                    LoadAtrTabData 1, ""
                Else
                    PrvTabLine = -1
                    LoadAtrTabData 0, ""
                    AtrTab(0).SetCurrentSelection 0
                    AtrTab(0).SetFocus
                End If
                chkTool(Index).Value = 0
            Case 3  'Compare
                chkTool(2).Value = 1
                HandleCompare
                chkTool(Index).Value = 0
            Case 4  'Save
                HandleSaveAtrRecords
                chkTool(Index).Value = 0
            Case 6
                Me.Hide
                Unload Me
            Case Else
                chkTool(Index).Value = 0
        End Select
    Else
        chkTool(Index).BackColor = vbButtonFace
        Select Case Index
            Case 0  'Excel
                chkTool(1).Value = 0
                HandleExcel True
            Case Else
        End Select
    End If
    'Me.Refresh
End Sub
Private Sub HandleSaveAtrRecords()
    Dim tmpRelHdlCmd As String
    Dim tmpSqlTab As String
    Dim tmpSqlFld As String
    Dim tmpSqlDat As String
    Dim tmpSqlKey As String
    Dim tmpNewFld As String
    Dim tmpNewDat As String
    Dim tmpData As String
    Dim tmpUrno As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CntInsert As Long
    Dim CntUpdate As Long
    'Inserts
    Screen.MousePointer = 11
    tmpSqlTab = "ATRTAB"
    tmpSqlFld = AtrTab(0).LogicalFieldList
    tmpSqlFld = Replace(tmpSqlFld, ",'  '", "", 1, -1, vbBinaryCompare)
    tmpRelHdlCmd = ""
    AtrTab(0).SetInternalLineBuffer True
    CntInsert = Val(AtrTab(0).GetLinesByStatusValue(99, 0))
    If CntInsert > 0 Then
        UfisServer.UrnoPoolInit 50
        UfisServer.UrnoPoolPrepare CntInsert
        tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",IRT," & CStr(ItemCount(tmpSqlFld, ",")) & "," & tmpSqlFld & vbLf
        While CntInsert >= 0
            CntInsert = AtrTab(0).GetNextResultLine
            If CntInsert >= 0 Then
                tmpNewDat = ""
                tmpData = UfisServer.UrnoPoolGetNext
                tmpNewDat = tmpNewDat & tmpData & ","
                tmpData = GetTimeStamp(0)
                tmpNewDat = tmpNewDat & tmpData & ","
                tmpNewDat = tmpNewDat & HomeAirport & ","
                AtrTab(0).SetFieldValues CntInsert, "URNO,CDAT,HOPO", tmpNewDat
                tmpSqlDat = AtrTab(0).GetFieldValues(CntInsert, tmpSqlFld)
                tmpSqlDat = CleanNullValues(tmpSqlDat)
                tmpRelHdlCmd = tmpRelHdlCmd & tmpSqlDat & vbLf
                AtrTab(0).SetLineColor CntInsert, vbBlack, vbWhite
                AtrTab(0).SetLineStatusValue CntInsert, 0
            End If
        Wend
    End If
    AtrTab(0).SetInternalLineBuffer False
    
    AtrTab(0).SetInternalLineBuffer True
    CntUpdate = Val(AtrTab(0).GetLinesByStatusValue(98, 0))
    If CntUpdate > 0 Then
        tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",URT," & CStr(ItemCount(tmpSqlFld, ",")) & "," & tmpSqlFld & ",[URNO=:VURNO]" & vbLf
        While CntUpdate >= 0
            CntUpdate = AtrTab(0).GetNextResultLine
            If CntUpdate >= 0 Then
                tmpUrno = AtrTab(0).GetFieldValue(CntUpdate, "URNO")
                tmpSqlDat = AtrTab(0).GetFieldValues(CntUpdate, tmpSqlFld)
                tmpSqlDat = CleanNullValues(tmpSqlDat)
                tmpRelHdlCmd = tmpRelHdlCmd & tmpSqlDat & "," & tmpUrno & vbLf
                AtrTab(0).SetLineColor CntUpdate, vbBlack, vbWhite
                AtrTab(0).SetLineStatusValue CntUpdate, 0
            End If
        Wend
    End If
'            CntDelete = Val(FileData(Index).GetLinesByStatusValue(3, 0))
'            If CntDelete > 0 Then
'                tmpRelHdlCmd = tmpRelHdlCmd & "*CMD*," & tmpSqlTab & ",DRT,-1,[URNO=:VURNO]" & vbLf
'                While CntDelete > 0
'                    CntDelete = FileData(Index).GetNextResultLine
'                    If CntDelete >= 0 Then
'                        tmpUrno = FileData(Index).GetFieldValue(CntDelete, "URNO")
'                        If tmpUrno <> "" Then
'                            tmpRelHdlCmd = tmpRelHdlCmd & tmpUrno & vbLf
'                        End If
'                        FileData(Index).DeleteLine CntDelete
'                    End If
'                    CntDelete = Val(FileData(Index).GetLinesByStatusValue(3, 0))
'                Wend
'            End If
    
    AtrTab(0).SetInternalLineBuffer False
    
    If tmpRelHdlCmd <> "" Then
        UfisServer.CallCeda CedaDataAnswer, "REL", tmpSqlTab, tmpSqlFld, tmpRelHdlCmd, "LATE,NOBC,NOACTION,NOLOG", "", 0, False, False
        tmpSqlFld = ""
        tmpSqlDat = ""
        tmpSqlKey = UfisServer.CdrhdlSock(0).LocalHostName
        tmpSqlKey = tmpSqlKey & "," & CStr(App.ThreadID) & "," & "Curfew"
        tmpSqlKey = tmpSqlKey & "," & "Excel"
        UfisServer.CallCeda CedaDataAnswer, "SBC", tmpSqlTab & "/REFR", tmpSqlFld, tmpSqlDat, tmpSqlKey, "", 0, False, False
    End If
    Screen.MousePointer = 0
End Sub
Private Sub HandleCompare()
    Dim CurApc3 As String
    Dim CurApc4 As String
    Dim CurLine As Long
    Dim CurMaxL As Long
    Dim AtrApc3 As String
    Dim AtrApc4 As String
    Dim AtrLine As Long
    Dim AtrMaxL As Long
    Dim AtrFldList As String
    Dim ColName As String
    Dim CurData As String
    Dim AtrData As String
    Dim ColNo As Long
    Dim MaxCol As Long
    Dim RecFound As Boolean
    Dim RecChanged As Boolean
    AtrFldList = AtrTab(0).LogicalFieldList
    CurMaxL = AtrTab(0).GetLineCount - 1
    For CurLine = 0 To CurMaxL
        CurApc3 = Trim(AtrTab(0).GetFieldValue(CurLine, "APC3"))
        CurApc4 = Trim(AtrTab(0).GetFieldValue(CurLine, "APC4"))
        RecFound = False
        AtrMaxL = AtrTab(1).GetLineCount - 1
        For AtrLine = 0 To AtrMaxL
            AtrApc3 = Trim(AtrTab(1).GetFieldValue(AtrLine, "APC3"))
            AtrApc4 = Trim(AtrTab(1).GetFieldValue(AtrLine, "APC4"))
            If (AtrApc3 = CurApc3) And (AtrApc4 = CurApc4) Then RecFound = True
            If (CurApc3 <> "") And (CurApc3 = AtrApc3) Then RecFound = True
            If (CurApc4 <> "") And (CurApc4 = AtrApc4) Then RecFound = True
            If (CurApc3 = "") And (CurApc4 = AtrApc4) Then RecFound = True
            If (CurApc4 = "") And (CurApc3 = AtrApc3) Then RecFound = True
            If RecFound Then
                'Record exists. Check and merge updates.
                RecChanged = False
                MaxCol = 6
                For ColNo = 0 To MaxCol
                    CurData = AtrTab(0).GetColumnValue(CurLine, ColNo)
                    AtrData = AtrTab(1).GetColumnValue(AtrLine, ColNo)
                    If (CurData <> AtrData) Then
                        AtrTab(1).SetColumnValue AtrLine, ColNo, CurData
                        RecChanged = True
                    End If
                Next
                If AtrTab(0).GetLineStatusValue(CurLine) = 98 Then RecChanged = True
                If AtrTab(1).GetLineStatusValue(CurLine) = 98 Then RecChanged = True
                If RecChanged = True Then
                    AtrData = AtrTab(1).GetFieldValues(AtrLine, AtrFldList)
                    AtrTab(0).SetFieldValues CurLine, AtrFldList, AtrData
                    AtrTab(0).SetLineColor CurLine, vbBlack, vbWhite
                    AtrTab(0).SetLineStatusValue CurLine, 98
                    AtrTab(1).SetLineStatusValue AtrLine, 98
                Else
                    AtrTab(0).SetLineStatusValue CurLine, 0
                    AtrTab(0).SetLineColor CurLine, vbBlack, LightGrey
                End If
                Exit For
            End If
        Next
        If RecFound = False Then
            'New record.
            AtrTab(0).SetLineColor CurLine, vbBlack, LightGreen
            AtrTab(0).SetLineStatusValue CurLine, 99
        End If
        AtrTab(0).Refresh
    Next
End Sub
Private Sub HandleExcel(QuitExcel As Boolean)
    Dim NewSize As Long
    On Error Resume Next
    
    If (Not (MsExcel Is Nothing)) Or (QuitExcel = True) Then
        If (Not (MsExcel Is Nothing)) Then
            ExcelShutDown = True
            MsWkBook.Saved = True
            MsExcel.Quit
            ExcelShutDown = False
        End If
        ExcelIsOpen = False
    End If
    If Not QuitExcel Then
        Set MsExcel = New Application
        'MsExcel.Workbooks.Add
        'Set MsWkBook = MsExcel.Workbooks(1)
        'Set MsWkSheet = MsWkBook.Worksheets(1)
        'Kill CurFile
        'MsWkBook.SaveAs CurFile
        MsExcel.WindowState = xlNormal
        MsExcel.Top = (Me.Top / 20) + 40
        MsExcel.Left = Me.Left / 20
        MsExcel.Width = Me.Width / 20
        NewSize = Me.Height / 20
        MsExcel.Height = NewSize
        MsExcel.Visible = True
        'MsExcel.Parent = App
    End If

End Sub
Private Sub GetExcelData()
    Dim tmpData As String
    Dim tmpAtrRec As String
    Dim tmpApc3 As String
    Dim tmpApc4 As String
    Dim tmpApfn As String
    Dim Apc3Len As Integer
    Dim Apc4Len As Integer
    Dim DataLen As Integer
    Dim i As Integer
    Dim j As Integer
    Dim StopLoop As Boolean
    Dim CheckApc As Boolean
    'This function currently has been designed
    'for import of ATRTAB (Airport Traffic Restriction)
    On Error Resume Next
    Set MsWkSheet = MsExcel.ActiveSheet
    If Not MsWkSheet Is Nothing Then
        AtrTab(0).ResetContent
        i = 2
        j = 0
        StopLoop = False
        While StopLoop = False
            i = i + 1
            tmpApc3 = Trim(MsWkSheet.Cells(i, 2))
            tmpApc3 = CleanString(tmpApc3, FOR_SERVER, False)
            Apc3Len = Len(tmpApc3)
            tmpApc4 = Trim(MsWkSheet.Cells(i, 3))
            tmpApc4 = CleanString(tmpApc4, FOR_SERVER, False)
            Apc4Len = Len(tmpApc4)
            DataLen = Apc3Len + Apc4Len
            If DataLen = 0 Then StopLoop = True
            If StopLoop = False Then
                CheckApc = False
                If (Apc3Len > 0) And (Apc3Len <> 3) Then CheckApc = True
                If (Apc4Len > 0) And (Apc4Len <> 4) Then CheckApc = True
                If CheckApc = True Then
                    If tmpApc3 = "" Then tmpApc3 = "-"
                    If tmpApc4 = "" Then tmpApc4 = "-"
                    tmpData = "Sorry, this doesn't look like airport data." & vbNewLine
                    tmpData = tmpData & "( " & tmpApc3 & " / " & tmpApc4 & " )"
                    'If MyMsgBox.CallAskUser(0, 0, 0, "Basic Data Check", tmpData, "infomsg", "OK", UserAnswer) = 2 Then DoNothing
                    'StopLoop = True
                End If
            End If
            If (StopLoop = False) And (CheckApc = False) Then
                tmpAtrRec = tmpApc3 & "," & tmpApc4
                tmpApfn = Trim(MsWkSheet.Cells(i, 1))
                tmpApfn = CleanString(tmpApfn, FOR_SERVER, False)
                tmpAtrRec = tmpAtrRec & "," & tmpApfn
                For j = 4 To 7
                    tmpData = Trim(MsWkSheet.Cells(i, j))
                    tmpData = CleanString(tmpData, FOR_SERVER, False)
                    tmpAtrRec = tmpAtrRec & "," & tmpData
                Next
                tmpAtrRec = tmpAtrRec & ",,,,,,,,,"
                AtrTab(0).InsertTextLine tmpAtrRec, False
            End If
        Wend
        AtrTab(0).AutoSizeColumns
        AtrTab(0).Refresh
    End If
End Sub

Private Sub Form_Activate()
    Static IsActive As Boolean
    If IsActive = False Then
        Me.Refresh
        IsActive = True
    End If
End Sub

Private Sub Form_Load()
    InitAtrTabGrid
    RightPanel.ZOrder
    StatusBar1.ZOrder
    PrvTabLine = -1
End Sub
Private Sub InitAtrTabGrid()
    Dim i As Integer
    For i = 0 To AtrTab.UBound
        AtrTab(i).ResetContent
        AtrTab(i).LogicalFieldList = "APC3,APC4,APFN,ATRI,ATRP,REMA,OTHR,PRFL,USEC,CDAT,USEU,LSTU,HOPO,URNO,'  '"
        AtrTab(i).HeaderString = "IATA,ICAO,NAME,ATRI,ATRP,REMA,OTHR,PRFL,USEC,CDAT,USEU,LSTU,HOPO,URNO,                   ."
        AtrTab(i).HeaderLengthString = "10,10,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,100"
        AtrTab(i).FontName = "Arial"
        AtrTab(i).FontSize = 16
        AtrTab(i).LineHeight = 16
        AtrTab(i).HeaderFontSize = 16
        AtrTab(i).SetTabFontBold True
        AtrTab(i).LifeStyle = True
        AtrTab(i).CursorLifeStyle = True
        AtrTab(i).AutoSizeByHeader = True
        AtrTab(i).ShowVertScroller True
        AtrTab(i).AutoSizeColumns
    Next
    AtrTab(0).ZOrder
End Sub
Public Sub LoadAtrTabData(idx As Integer, ForWhat As String)
    Dim CurTab As TABLib.Tab
    Dim CurSqlKey As String
    Dim CurFldLst As String
    Dim CurTblNam As String
    Dim i As Integer
    Set CurTab = AtrTab(idx)
    CurTab.ResetContent
    If idx = 0 Then
        PrvTabLine = -1
        DontCheck = True
        For i = 0 To txtAtrTab.UBound
            txtAtrTab(i).Text = ""
            txtAtrTab(i).Tag = ""
            txtAtrTab(i).BackColor = vbWhite
        Next
        DontCheck = False
    End If
    CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
    CurTab.CedaHopo = UfisServer.HOPO
    CurTab.CedaIdentifier = "IDX"
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = UfisServer.HostName
    CurTab.CedaTabext = UfisServer.TblExt
    CurTab.CedaUser = UfisServer.ModName
    CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
    CurTab.CedaPacketSize = 500
    CurTblNam = "ATRTAB"
    CurFldLst = CurTab.LogicalFieldList
    CurSqlKey = ""
    If ForWhat <> "" Then
        CurSqlKey = ForWhat
    End If
    CurTab.CedaAction "RTA", CurTblNam, CurFldLst, "", CurSqlKey
    AtrTab(idx).Refresh
    If idx = 0 Then
        AtrTab(idx).SetCurrentSelection 0
    End If
End Sub
Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleHeight - StatusBar1.Height - RightPanel.Top - AtrTab(0).Top - 60
    If NewSize > 600 Then AtrTab(0).Height = NewSize
    AtrTab(0).Left = 0
    AtrTab(0).Width = RightPanel.ScaleWidth
    AtrTab(1).Top = AtrTab(0).Top
    AtrTab(1).Width = AtrTab(0).Width
    AtrTab(1).Left = AtrTab(0).Left
    AtrTab(1).Height = AtrTab(0).Height
    
    NewSize = Me.ScaleHeight - StatusBar1.Height - txtAtrTab(6).Height - 60
    txtAtrTab(6).Top = NewSize
    NewSize = txtAtrTab(6).Top - txtAtrTab(5).Top - 120
    If NewSize > 600 Then txtAtrTab(5).Height = NewSize
    
    NewSize = Me.ScaleWidth - RightPanel.Width - (txtAtrTab(5).Left * 2) - 30
    If NewSize > 600 Then
        txtAtrTab(3).Width = NewSize
        txtAtrTab(4).Width = NewSize
        txtAtrTab(5).Width = NewSize
        txtAtrTab(6).Width = NewSize
    End If
    NewSize = Me.ScaleWidth - RightPanel.Width - txtAtrTab(2).Left - 120
    If NewSize > 600 Then txtAtrTab(2).Width = NewSize
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim QuitExcel As Boolean
    On Error Resume Next
    QuitExcel = True
    If (Not (MsExcel Is Nothing)) Or (QuitExcel = True) Then
        If (Not (MsExcel Is Nothing)) Then
            ExcelShutDown = True
            MsWkBook.Saved = True
            MsExcel.Quit
            ExcelShutDown = False
        End If
        ExcelIsOpen = False
    End If
    MainDialog.PushWorkButton "CURFEW", 0
End Sub

Private Sub MsExcel_NewWorkbook(ByVal Wb As Excel.Workbook)
    Set MsWkBook = Wb
End Sub

Private Sub MsExcel_SheetActivate(ByVal Sh As Object)
    Set MsWkSheet = Sh
End Sub

Private Sub MsExcel_SheetChange(ByVal Sh As Object, ByVal Target As Excel.Range)
    'Set MsWkSheet = Sh
End Sub

Private Sub MsExcel_WorkbookBeforeClose(ByVal Wb As Excel.Workbook, Cancel As Boolean)
    If ExcelShutDown = False Then chkTool(0).Value = 0
End Sub

Private Sub MsExcel_WorkbookOpen(ByVal Wb As Excel.Workbook)
    Set MsWkBook = Wb
End Sub

Private Sub txtAtrTab_Change(Index As Integer)
    Dim tmpTxt As String
    Dim tmpTag As String
    If DontCheck = False Then
        tmpTxt = txtAtrTab(Index).Text
        tmpTag = txtAtrTab(Index).Tag
        If tmpTxt <> tmpTag Then
            txtAtrTab(Index).BackColor = vbYellow
        Else
            txtAtrTab(Index).BackColor = vbWhite
        End If
    End If
End Sub

Private Sub txtAtrTab_LostFocus(Index As Integer)
    Dim tmpTxt As String
    Dim tmpTag As String
    Dim ColNo As Long
    If DontCheck = False Then
        tmpTxt = txtAtrTab(Index).Text
        tmpTag = txtAtrTab(Index).Tag
        If tmpTxt <> tmpTag Then
            txtAtrTab(Index).BackColor = vbYellow
            If PrvTabLine >= 0 Then
                ColNo = CLng(Index)
                tmpTxt = CleanString(tmpTxt, FOR_SERVER, False)
                AtrTab(0).SetColumnValue PrvTabLine, ColNo, tmpTxt
                AtrTab(0).SetLineColor PrvTabLine, vbBlack, vbYellow
                AtrTab(0).SetLineStatusValue PrvTabLine, 98
                AtrTab(0).Refresh
            End If
        Else
            txtAtrTab(Index).BackColor = vbWhite
        End If
    End If
End Sub
