VERSION 5.00
Begin VB.Form ObjectsCollection 
   Caption         =   "Objects Moved From Main Dialog"
   ClientHeight    =   4980
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13050
   LinkTopic       =   "Form1"
   ScaleHeight     =   4980
   ScaleWidth      =   13050
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox CfgSubPanel 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   1
      Left            =   30
      ScaleHeight     =   315
      ScaleWidth      =   4185
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   1920
      Visible         =   0   'False
      Width           =   4215
      Begin VB.CheckBox chkCfg 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Chart Size"
         Height          =   315
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkCfg 
         BackColor       =   &H00E0E0E0&
         Caption         =   "H-Zoom"
         Height          =   315
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkCfg 
         BackColor       =   &H00E0E0E0&
         Caption         =   "V-Zoom"
         Height          =   315
         Index           =   3
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkCfg 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Blinker"
         Height          =   315
         Index           =   4
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox CfgOptPanel 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   1
      Left            =   60
      ScaleHeight     =   315
      ScaleWidth      =   1635
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1635
      Begin VB.Frame AnyFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Chart Layout"
         Height          =   1065
         Index           =   0
         Left            =   60
         TabIndex        =   16
         Top             =   60
         Visible         =   0   'False
         Width           =   1515
      End
   End
   Begin VB.PictureBox CfgOptPanel 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   2
      Left            =   1740
      ScaleHeight     =   315
      ScaleWidth      =   1605
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1605
   End
   Begin VB.PictureBox CfgOptPanel 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   3
      Left            =   3390
      ScaleHeight     =   315
      ScaleWidth      =   1635
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1635
      Begin VB.Frame AnyFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Chart Layout"
         Height          =   1065
         Index           =   1
         Left            =   60
         TabIndex        =   13
         Top             =   60
         Width           =   1515
      End
   End
   Begin VB.PictureBox CfgOptPanel 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   4
      Left            =   5070
      ScaleHeight     =   315
      ScaleWidth      =   1635
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1635
      Begin VB.Frame AnyFrame 
         BackColor       =   &H00FFC0C0&
         Caption         =   "Chart Layout"
         ForeColor       =   &H00000000&
         Height          =   1065
         Index           =   2
         Left            =   60
         TabIndex        =   11
         Top             =   60
         Width           =   1515
      End
      Begin VB.Frame AnyFrame 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Chart Layout"
         Height          =   1065
         Index           =   3
         Left            =   60
         TabIndex        =   10
         Top             =   1200
         Width           =   1515
      End
   End
   Begin VB.PictureBox CfgTopBkGrd 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   0
      Left            =   60
      ScaleHeight     =   345
      ScaleWidth      =   735
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   540
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.PictureBox CfgSubPanel 
      BackColor       =   &H008080FF&
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   0
      Left            =   810
      ScaleHeight     =   345
      ScaleWidth      =   735
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   900
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.PictureBox CfgOptBkGrd 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   0
      Left            =   60
      ScaleHeight     =   345
      ScaleWidth      =   735
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1260
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.PictureBox CfgSubBkGrd 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   0
      Left            =   60
      ScaleHeight     =   345
      ScaleWidth      =   735
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   900
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.PictureBox CfgOptPanel 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   0
      Left            =   810
      ScaleHeight     =   345
      ScaleWidth      =   735
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1260
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.PictureBox CfgTopPanel 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      ForeColor       =   &H80000008&
      Height          =   405
      Index           =   0
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   9435
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   9465
      Begin VB.PictureBox CfgTopPanel 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         ForeColor       =   &H80000008&
         Height          =   345
         Index           =   1
         Left            =   0
         ScaleHeight     =   315
         ScaleWidth      =   7875
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   0
         Width           =   7905
         Begin VB.CheckBox chkCfg 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Options"
            Height          =   315
            Index           =   0
            Left            =   570
            Style           =   1  'Graphical
            TabIndex        =   3
            Top             =   0
            Width           =   1035
         End
         Begin VB.CheckBox chkCfg 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Layout"
            Height          =   315
            Index           =   5
            Left            =   3270
            Style           =   1  'Graphical
            TabIndex        =   2
            Top             =   0
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.Image AnyPic 
            Height          =   480
            Index           =   18
            Left            =   0
            Picture         =   "ObjectsCollection.frx":0000
            Top             =   0
            Width           =   2250
         End
         Begin VB.Image AnyPic 
            Height          =   480
            Index           =   19
            Left            =   4770
            Picture         =   "ObjectsCollection.frx":38C2
            Top             =   0
            Width           =   2250
         End
         Begin VB.Image AnyPic 
            Height          =   480
            Index           =   20
            Left            =   2640
            Picture         =   "ObjectsCollection.frx":7184
            Stretch         =   -1  'True
            Top             =   0
            Visible         =   0   'False
            Width           =   1920
         End
      End
   End
End
Attribute VB_Name = "ObjectsCollection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub chkCfg_Click(Index As Integer)
    Dim tmpTag As String
    Dim TopIdx As Integer
    Dim SubIdx As Integer
    Dim SubBck As Integer
    Dim OptIdx As Integer
    Dim OptBck As Integer
    Dim OptShd As Integer
    Dim PrvIdx As Integer
    Dim ShowTop As Boolean
    Dim ShowSub As Boolean
    If chkCfg(Index).Value = 1 Then
        chkCfg(Index).BackColor = LightYellow
        Select Case Index
            Case 0
                TopIdx = 1
                SubIdx = 1
                SubBck = 0
                ShowCfgTopPanel Index, 1, 1, 0
            Case 1 To 4
                TopIdx = 1
                SubIdx = 1
                SubBck = 0
                OptIdx = Index
                OptBck = 0
                OptShd = 0
'                tmpTag = chkCfg(1).Tag
'                If tmpTag <> "" Then
'                    PrvIdx = Val(tmpTag)
'                    If PrvIdx <> Index Then chkCfg(Val(PrvIdx)).Value = 0
'                End If
'                chkCfg(1).Tag = CStr(Index)
                CfgOptPanel(OptBck).Top = CfgSubPanel(SubIdx).Top + CfgSubPanel(SubIdx).Height
                CfgOptPanel(OptBck).Left = CfgSubPanel(SubIdx).Left + chkCfg(Index).Left - 15
                CfgOptPanel(OptBck).Width = CfgOptPanel(OptIdx).Width + 30
                CfgOptPanel(OptBck).Height = CfgOptPanel(OptIdx).Height + 30
                CfgOptPanel(OptIdx).Top = CfgOptPanel(OptBck).Top + 15
                CfgOptPanel(OptIdx).Left = CfgOptPanel(OptBck).Left + 15
                CfgOptBkGrd(OptShd).Width = CfgOptPanel(OptBck).Width
                CfgOptBkGrd(OptShd).Height = CfgOptPanel(OptBck).Height + 90
                CfgOptBkGrd(OptShd).Left = CfgOptPanel(OptBck).Left + 90
                CfgOptBkGrd(OptShd).Top = CfgOptPanel(OptBck).Top
                CfgOptBkGrd(OptShd).ZOrder
                CfgOptPanel(OptBck).ZOrder
                CfgOptPanel(OptIdx).ZOrder
                CfgOptPanel(OptBck).Visible = True
                CfgOptBkGrd(OptShd).Visible = True
                CfgOptPanel(OptIdx).Visible = True
            Case Else
                chkCfg(Index).Value = 0
        End Select
    Else
        Select Case Index
            Case 0
                CfgSubPanel(1).Visible = False
                CfgSubBkGrd(0).Visible = False
            Case 1 To 4
'                tmpTag = chkCfg(1).Tag
'                PrvIdx = Val(tmpTag)
'                If (tmpTag <> "") And (PrvIdx = Index) Then
'                    CfgSubPanel(Index).Visible = False
'                    CfgShadow(1).Visible = False
'                    CfgSubPanel(0).Visible = False
'                    chkCfg(1).Tag = ""
'                End If
            
            Case Else
        End Select
        chkCfg(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub ShowCfgTopPanel(Index As Integer, TopIdx As Integer, SubIdx As Integer, SubBck As Integer)
    CfgSubPanel(SubIdx).Left = CfgTopPanel(TopIdx).Left + chkCfg(Index).Left
    CfgSubPanel(SubIdx).Top = CfgTopPanel(TopIdx).Top + CfgTopPanel(TopIdx).Height
    CfgSubBkGrd(SubBck).Width = CfgSubPanel(SubIdx).Width
    CfgSubBkGrd(SubBck).Height = CfgSubPanel(SubIdx).Height + 30
    CfgSubBkGrd(SubBck).Left = CfgSubPanel(SubIdx).Left + 30
    CfgSubBkGrd(SubBck).Top = CfgSubPanel(SubIdx).Top
    CfgSubBkGrd(SubBck).ZOrder
    CfgSubPanel(SubIdx).ZOrder
    CfgSubPanel(SubIdx).Visible = True
    CfgSubBkGrd(SubBck).Visible = True
End Sub

Private Sub Form_Load()
    Dim i As Integer
    CfgSubPanel(0).BackColor = vbBlack
    For i = 1 To CfgSubPanel.UBound
        CfgSubPanel(i).BackColor = LightestBlue
    Next
    For i = 0 To AnyFrame.UBound
        AnyFrame(i).BackColor = LightestBlue
    Next
    'For i = 0 To AnyOpt.UBound
    '    AnyOpt(i).BackColor = LightestBlue
    'Next
    

End Sub
