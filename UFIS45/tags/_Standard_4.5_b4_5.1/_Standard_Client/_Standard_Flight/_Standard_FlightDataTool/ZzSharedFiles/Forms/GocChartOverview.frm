VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form GocChartOverview 
   Caption         =   "Status Chart Overview"
   ClientHeight    =   9105
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6945
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "GocChartOverview.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   9105
   ScaleWidth      =   6945
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer MouseTimer 
      Enabled         =   0   'False
      Interval        =   250
      Left            =   4170
      Top             =   2640
   End
   Begin VB.PictureBox ButtonPanel 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   1
      Left            =   30
      ScaleHeight     =   315
      ScaleWidth      =   4185
      TabIndex        =   77
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   4215
      Begin VB.CheckBox chkWork 
         Caption         =   "Chart Size"
         Height          =   315
         Index           =   1
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   81
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "H-Zoom"
         Height          =   315
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   80
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "V-Zoom"
         Height          =   315
         Index           =   3
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   79
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Blinker"
         Height          =   315
         Index           =   4
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   78
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox PanelShadow 
      Appearance      =   0  'Flat
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   315
      Index           =   1
      Left            =   4740
      ScaleHeight     =   315
      ScaleWidth      =   1635
      TabIndex        =   82
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.Timer ScrollTimer 
      Enabled         =   0   'False
      Index           =   1
      Interval        =   25
      Left            =   4170
      Top             =   3540
   End
   Begin VB.Timer ScrollTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   25
      Left            =   4170
      Top             =   3090
   End
   Begin VB.PictureBox OptionPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Index           =   4
      Left            =   1710
      ScaleHeight     =   2265
      ScaleWidth      =   1515
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   4440
      Visible         =   0   'False
      Width           =   1515
      Begin VB.Frame fraInterval 
         Caption         =   "Grey"
         Height          =   1545
         Index           =   1
         Left            =   780
         TabIndex        =   28
         Top             =   660
         Width           =   675
         Begin VB.OptionButton optFreqOff 
            Alignment       =   1  'Right Justify
            Caption         =   "1"
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   33
            Tag             =   "1"
            Top             =   270
            Width           =   405
         End
         Begin VB.OptionButton optFreqOff 
            Alignment       =   1  'Right Justify
            Caption         =   "2"
            Height          =   240
            Index           =   1
            Left            =   120
            TabIndex        =   32
            Tag             =   "2"
            Top             =   510
            Width           =   405
         End
         Begin VB.OptionButton optFreqOff 
            Alignment       =   1  'Right Justify
            Caption         =   "3"
            Height          =   240
            Index           =   2
            Left            =   120
            TabIndex        =   31
            Tag             =   "3"
            Top             =   750
            Width           =   405
         End
         Begin VB.OptionButton optFreqOff 
            Alignment       =   1  'Right Justify
            Caption         =   "4"
            Height          =   240
            Index           =   3
            Left            =   120
            TabIndex        =   30
            Tag             =   "4"
            Top             =   990
            Width           =   405
         End
         Begin VB.OptionButton optFreqOff 
            Alignment       =   1  'Right Justify
            Caption         =   "5"
            Height          =   240
            Index           =   4
            Left            =   120
            TabIndex        =   29
            Tag             =   "5"
            Top             =   1230
            Width           =   405
         End
      End
      Begin VB.Frame fraInterval 
         Caption         =   "Red"
         Height          =   1545
         Index           =   0
         Left            =   60
         TabIndex        =   22
         Top             =   660
         Width           =   675
         Begin VB.OptionButton optFreqOn 
            Caption         =   "5"
            Height          =   240
            Index           =   4
            Left            =   120
            TabIndex        =   27
            Tag             =   "5"
            Top             =   1230
            Width           =   375
         End
         Begin VB.OptionButton optFreqOn 
            Caption         =   "4"
            Height          =   240
            Index           =   3
            Left            =   120
            TabIndex        =   26
            Tag             =   "4"
            Top             =   990
            Width           =   375
         End
         Begin VB.OptionButton optFreqOn 
            Caption         =   "3"
            Height          =   240
            Index           =   2
            Left            =   120
            TabIndex        =   25
            Tag             =   "3"
            Top             =   750
            Width           =   375
         End
         Begin VB.OptionButton optFreqOn 
            Caption         =   "2"
            Height          =   240
            Index           =   1
            Left            =   120
            TabIndex        =   24
            Tag             =   "2"
            Top             =   510
            Width           =   375
         End
         Begin VB.OptionButton optFreqOn 
            Caption         =   "1"
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   23
            Tag             =   "1"
            Top             =   270
            Width           =   375
         End
      End
      Begin VB.Frame fraBlinker 
         Caption         =   "Blinker"
         Height          =   555
         Left            =   60
         TabIndex        =   19
         Top             =   60
         Width           =   1395
         Begin VB.OptionButton optBlink 
            Alignment       =   1  'Right Justify
            Caption         =   "Off"
            Height          =   210
            Index           =   1
            Left            =   690
            TabIndex        =   21
            Tag             =   "OFF"
            Top             =   270
            Width           =   555
         End
         Begin VB.OptionButton optBlink 
            Caption         =   "On"
            Height          =   240
            Index           =   0
            Left            =   150
            TabIndex        =   20
            Tag             =   "ON"
            Top             =   270
            Width           =   525
         End
      End
   End
   Begin VB.PictureBox OptionPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   3
      Left            =   1710
      ScaleHeight     =   1815
      ScaleWidth      =   2415
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   6750
      Visible         =   0   'False
      Width           =   2415
      Begin VB.Frame fraBarFix 
         Caption         =   "Space"
         Height          =   825
         Index           =   3
         Left            =   1230
         TabIndex        =   44
         Top             =   930
         Width           =   1125
         Begin VB.OptionButton optSpcPix 
            Caption         =   "3"
            Height          =   255
            Index           =   3
            Left            =   600
            TabIndex        =   49
            Tag             =   "3"
            Top             =   510
            Width           =   360
         End
         Begin VB.OptionButton optSpcPix 
            Caption         =   "1"
            Height          =   255
            Index           =   1
            Left            =   600
            TabIndex        =   47
            Tag             =   "1"
            Top             =   270
            Width           =   360
         End
         Begin VB.OptionButton optSpcPix 
            Caption         =   "0"
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   46
            Tag             =   "0"
            Top             =   270
            Width           =   360
         End
         Begin VB.OptionButton optSpcPix 
            Caption         =   "2"
            Height          =   255
            Index           =   2
            Left            =   90
            TabIndex        =   45
            Tag             =   "2"
            Top             =   510
            Width           =   360
         End
      End
      Begin VB.Frame fraBarFix 
         Caption         =   "Height"
         Height          =   825
         Index           =   2
         Left            =   60
         TabIndex        =   40
         Top             =   930
         Width           =   1125
         Begin VB.OptionButton optBarPix 
            Caption         =   "4"
            Height          =   255
            Index           =   3
            Left            =   600
            TabIndex        =   48
            Tag             =   "4"
            Top             =   510
            Width           =   360
         End
         Begin VB.OptionButton optBarPix 
            Caption         =   "3"
            Height          =   255
            Index           =   2
            Left            =   90
            TabIndex        =   43
            Tag             =   "3"
            Top             =   510
            Width           =   360
         End
         Begin VB.OptionButton optBarPix 
            Caption         =   "1"
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   42
            Tag             =   "1"
            Top             =   270
            Width           =   360
         End
         Begin VB.OptionButton optBarPix 
            Caption         =   "2"
            Height          =   255
            Index           =   1
            Left            =   600
            TabIndex        =   41
            Tag             =   "2"
            Top             =   270
            Width           =   360
         End
      End
      Begin VB.Frame fraBarFix 
         Caption         =   "Line Space"
         Height          =   825
         Index           =   1
         Left            =   1230
         TabIndex        =   37
         Top             =   60
         Width           =   1125
         Begin VB.OptionButton optSpaceFix 
            Caption         =   "Fixed"
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   39
            Tag             =   "FIX"
            Top             =   270
            Width           =   870
         End
         Begin VB.OptionButton optSpaceFix 
            Caption         =   "Auto"
            Height          =   255
            Index           =   1
            Left            =   90
            TabIndex        =   38
            Tag             =   "AUTO"
            Top             =   510
            Width           =   870
         End
      End
      Begin VB.Frame fraBarFix 
         Caption         =   "Bar Height"
         Height          =   825
         Index           =   0
         Left            =   60
         TabIndex        =   34
         Top             =   60
         Width           =   1125
         Begin VB.OptionButton optBarFix 
            Caption         =   "Auto"
            Height          =   255
            Index           =   1
            Left            =   90
            TabIndex        =   36
            Tag             =   "AUTO"
            Top             =   510
            Width           =   870
         End
         Begin VB.OptionButton optBarFix 
            Caption         =   "Fixed"
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   35
            Tag             =   "FIX"
            Top             =   270
            Width           =   870
         End
      End
   End
   Begin VB.PictureBox OptionPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3945
      Index           =   2
      Left            =   60
      ScaleHeight     =   3945
      ScaleWidth      =   1605
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   4440
      Visible         =   0   'False
      Width           =   1605
      Begin VB.Frame fraZoomFix 
         Caption         =   "Round Up"
         Height          =   1305
         Index           =   2
         Left            =   60
         TabIndex        =   63
         Top             =   2580
         Width           =   1485
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "3"
            Height          =   255
            Index           =   2
            Left            =   90
            TabIndex        =   71
            Tag             =   "3"
            Top             =   750
            Width           =   630
         End
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "0"
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   70
            Tag             =   "0"
            Top             =   270
            Width           =   630
         End
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "2"
            Height          =   255
            Index           =   1
            Left            =   90
            TabIndex        =   69
            Tag             =   "2"
            Top             =   510
            Width           =   630
         End
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "4"
            Height          =   255
            Index           =   3
            Left            =   90
            TabIndex        =   68
            Tag             =   "4"
            Top             =   990
            Width           =   630
         End
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "5"
            Height          =   255
            Index           =   4
            Left            =   750
            TabIndex        =   67
            Tag             =   "5"
            Top             =   270
            Width           =   630
         End
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "6"
            Height          =   255
            Index           =   5
            Left            =   750
            TabIndex        =   66
            Tag             =   "6"
            Top             =   510
            Width           =   630
         End
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "8"
            Height          =   255
            Index           =   6
            Left            =   750
            TabIndex        =   65
            Tag             =   "8"
            Top             =   750
            Width           =   630
         End
         Begin VB.OptionButton optZoomRnd 
            Caption         =   "10"
            Height          =   255
            Index           =   7
            Left            =   750
            TabIndex        =   64
            Tag             =   "10"
            Top             =   990
            Width           =   630
         End
      End
      Begin VB.Frame fraZoomFix 
         Caption         =   "Fixed Factor"
         Height          =   1305
         Index           =   1
         Left            =   60
         TabIndex        =   54
         Top             =   1200
         Width           =   1485
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/50"
            Height          =   255
            Index           =   7
            Left            =   750
            TabIndex        =   62
            Tag             =   "50"
            Top             =   990
            Width           =   630
         End
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/30"
            Height          =   255
            Index           =   6
            Left            =   750
            TabIndex        =   61
            Tag             =   "30"
            Top             =   750
            Width           =   630
         End
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/25"
            Height          =   255
            Index           =   5
            Left            =   750
            TabIndex        =   60
            Tag             =   "25"
            Top             =   510
            Width           =   630
         End
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/20"
            Height          =   255
            Index           =   4
            Left            =   750
            TabIndex        =   59
            Tag             =   "20"
            Top             =   270
            Width           =   630
         End
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/16"
            Height          =   255
            Index           =   3
            Left            =   90
            TabIndex        =   58
            Tag             =   "16"
            Top             =   990
            Width           =   630
         End
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/4"
            Height          =   255
            Index           =   1
            Left            =   90
            TabIndex        =   57
            Tag             =   "4"
            Top             =   510
            Width           =   630
         End
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/2"
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   56
            Tag             =   "2"
            Top             =   270
            Width           =   630
         End
         Begin VB.OptionButton optZoomVal 
            Caption         =   "1/8"
            Height          =   255
            Index           =   2
            Left            =   90
            TabIndex        =   55
            Tag             =   "8"
            Top             =   750
            Width           =   630
         End
      End
      Begin VB.Frame fraZoomFix 
         Caption         =   "Zoom Factor"
         Height          =   1065
         Index           =   0
         Left            =   60
         TabIndex        =   50
         Top             =   60
         Width           =   1485
         Begin VB.OptionButton optZoomFix 
            Caption         =   "Preferred"
            Height          =   255
            Index           =   2
            Left            =   90
            TabIndex        =   53
            Tag             =   "PREF"
            Top             =   510
            Width           =   1170
         End
         Begin VB.OptionButton optZoomFix 
            Caption         =   "Fixed"
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   52
            Tag             =   "FIXD"
            Top             =   270
            Width           =   870
         End
         Begin VB.OptionButton optZoomFix 
            Caption         =   "Auto"
            Height          =   255
            Index           =   1
            Left            =   90
            TabIndex        =   51
            Tag             =   "AUTO"
            Top             =   750
            Width           =   870
         End
      End
   End
   Begin VB.PictureBox OptionPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1185
      Index           =   1
      Left            =   4170
      ScaleHeight     =   1185
      ScaleWidth      =   1635
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1200
      Visible         =   0   'False
      Width           =   1635
      Begin VB.Frame fraLayout 
         Caption         =   "Chart Layout"
         Height          =   1065
         Index           =   0
         Left            =   60
         TabIndex        =   72
         Top             =   60
         Width           =   1515
         Begin VB.OptionButton optLayout 
            Caption         =   "Auto"
            Height          =   255
            Index           =   2
            Left            =   90
            TabIndex        =   75
            Top             =   750
            Value           =   -1  'True
            Width           =   1305
         End
         Begin VB.OptionButton optLayout 
            Caption         =   "Landscape"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   90
            TabIndex        =   74
            Top             =   510
            Width           =   1305
         End
         Begin VB.OptionButton optLayout 
            Caption         =   "Portrait"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   90
            TabIndex        =   73
            Top             =   270
            Width           =   1305
         End
      End
   End
   Begin VB.PictureBox OptionPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   165
      Index           =   0
      Left            =   4170
      ScaleHeight     =   165
      ScaleWidth      =   2205
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   750
      Visible         =   0   'False
      Width           =   2205
   End
   Begin VB.PictureBox PanelShadow 
      Appearance      =   0  'Flat
      BackColor       =   &H80000010&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Index           =   0
      Left            =   4140
      ScaleHeight     =   195
      ScaleWidth      =   2265
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   960
      Visible         =   0   'False
      Width           =   2265
   End
   Begin VB.Timer MyBlinker 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   4170
      Top             =   3990
   End
   Begin TABLib.TAB TabBlinker 
      Height          =   2295
      Left            =   3270
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   4440
      Visible         =   0   'False
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   4048
      _StockProps     =   64
   End
   Begin VB.PictureBox CornerCover 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   2
      Left            =   3870
      ScaleHeight     =   285
      ScaleWidth      =   195
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   4050
      Width           =   255
   End
   Begin VB.PictureBox CornerCover 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   0
      Left            =   3840
      ScaleHeight     =   285
      ScaleWidth      =   195
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   750
      Width           =   255
   End
   Begin VB.PictureBox ButtonPanel 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   345
      Index           =   0
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   6615
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   0
      Width           =   6645
      Begin VB.CheckBox chkWork 
         Caption         =   "Options"
         Height          =   315
         Index           =   6
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   83
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Re-Adjust"
         Enabled         =   0   'False
         Height          =   315
         Index           =   5
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   76
         Top             =   0
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Redraw"
         Height          =   315
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.PictureBox TopPanel 
      BackColor       =   &H00808080&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   15
      ScaleHeight     =   300
      ScaleWidth      =   3765
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   720
      Width           =   3825
      Begin VB.Label lblNowTime 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "00:00:00"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   0
         Left            =   2070
         TabIndex        =   85
         Top             =   60
         Visible         =   0   'False
         Width           =   810
      End
   End
   Begin VB.PictureBox BottomPanel 
      BackColor       =   &H00808080&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   15
      ScaleHeight     =   300
      ScaleWidth      =   3765
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   4050
      Width           =   3825
      Begin VB.PictureBox NowTime 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   1
         Left            =   840
         MousePointer    =   1  'Arrow
         ScaleHeight     =   270
         ScaleWidth      =   15
         TabIndex        =   87
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   45
      End
      Begin VB.Label lblNowTime 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "00:00:00"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   2
         Left            =   990
         TabIndex        =   88
         Top             =   30
         Visible         =   0   'False
         Width           =   720
      End
      Begin VB.Label lblNowTime 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "00:00:00"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   1
         Left            =   30
         TabIndex        =   86
         Top             =   30
         Visible         =   0   'False
         Width           =   720
      End
   End
   Begin VB.PictureBox WorkArea 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Left            =   15
      ScaleHeight     =   2565
      ScaleWidth      =   3765
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1110
      Width           =   3825
      Begin VB.PictureBox ChartArea 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00800000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   0
         MouseIcon       =   "GocChartOverview.frx":058A
         MousePointer    =   6  'Size NE SW
         ScaleHeight     =   2115
         ScaleWidth      =   3105
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   0
         Width           =   3165
         Begin VB.PictureBox NowTime 
            Appearance      =   0  'Flat
            BackColor       =   &H000000FF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1440
            Index           =   0
            Left            =   0
            MousePointer    =   1  'Arrow
            ScaleHeight     =   1410
            ScaleWidth      =   15
            TabIndex        =   84
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   45
         End
         Begin VB.Image picHint 
            Height          =   480
            Index           =   1
            Left            =   2340
            MouseIcon       =   "GocChartOverview.frx":06DC
            MousePointer    =   99  'Custom
            Picture         =   "GocChartOverview.frx":09E6
            ToolTipText     =   "Departure"
            Top             =   450
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.Image picHint 
            Height          =   480
            Index           =   0
            Left            =   1800
            MouseIcon       =   "GocChartOverview.frx":0CF0
            MousePointer    =   99  'Custom
            Picture         =   "GocChartOverview.frx":0FFA
            ToolTipText     =   "Arrival"
            Top             =   450
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.Image picCursor 
            Height          =   240
            Index           =   1
            Left            =   2340
            MousePointer    =   2  'Cross
            Picture         =   "GocChartOverview.frx":1304
            Top             =   1590
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Image picCursor 
            Height          =   240
            Index           =   0
            Left            =   960
            MousePointer    =   1  'Arrow
            Picture         =   "GocChartOverview.frx":144E
            Top             =   1590
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Shape Shape1 
            BorderColor     =   &H00E0E0E0&
            FillColor       =   &H00E0E0E0&
            FillStyle       =   7  'Diagonal Cross
            Height          =   615
            Left            =   120
            Top             =   780
            Visible         =   0   'False
            Width           =   1545
         End
         Begin VB.Shape ViewArea 
            BorderColor     =   &H00FFFF00&
            BorderWidth     =   2
            FillColor       =   &H00FFFFFF&
            Height          =   615
            Left            =   120
            Top             =   120
            Visible         =   0   'False
            Width           =   1545
         End
      End
   End
   Begin VB.PictureBox CornerCover 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   3870
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   3780
      Width           =   255
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      LargeChange     =   10
      Left            =   0
      Max             =   100
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3780
      Width           =   3855
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   2625
      LargeChange     =   10
      Left            =   3870
      Max             =   100
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1110
      Width           =   255
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8820
      Width           =   6945
      _ExtentX        =   12250
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9155
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "GocChartOverview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim BarHeight As Long
Dim BarOffset As Long
Dim FlightWidth As Long
Dim FlightPanelColor As Long
Dim DataTop As Long
Dim DataBottom As Long
Dim DataLeft As Long
Dim DataRight As Long
Dim MaxRight As Long
Dim MaxLineCount As Long
Dim MinTopLine As Long
Dim MaxBottomLine As Long
Dim TopLineCount As Long
Dim BottomLineCount As Long
Dim XZoom As Long
Dim IsReadyForUse As Boolean
Dim MouseIsDown As Boolean
Dim MouseIsWaiting As Boolean
Dim MouseIsMoving As Boolean
Dim MouseCursorIsBusy As Boolean
Dim MouseX As Long
Dim MouseY As Long
Dim ChartX As Long
Dim ChartY As Long
Dim DontScroll As Boolean
Public Sub StopActivities()
    MyBlinker.Enabled = False
    IsReadyForUse = False
End Sub
Public Sub InitChartArea()
    Dim MaxChartHeight As Long
    Dim MaxChartWidth As Long
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewZoom As Long
    Dim DatWidth As Long
    Dim DataBgnPos As Long
    Dim DataEndPos As Long
    Dim BlockSize As Long
    Dim BlockRest As Long
    Dim BlockAbove As Long
    Dim BlockBelow As Long
    Dim tmpVal As Long
    Dim x1 As Long
    Dim X2 As Long
    Dim X3 As Long
    Dim X4 As Long
    Dim y1 As Long
    Dim Y2 As Long
    Dim Y3 As Long
    Dim Y4 As Long
    Dim DataBgn
    Dim DataEnd
    Dim IsValidBgnScalePos As Boolean
    Dim IsValidEndScalePos As Boolean
    Dim EndLoop As Boolean
    IsReadyForUse = False
    MyBlinker.Enabled = False
    
    TabBlinker.ResetContent
    ChartArea.Visible = False
    'TopPanel.Visible = False
    'BottomPanel.Visible = False
    TopPanel.Width = MaxScreenWidth
    TopPanel.AutoRedraw = True
    DrawBackGround TopPanel, 7, True, False
    BottomPanel.Width = MaxScreenWidth
    BottomPanel.AutoRedraw = True
    DrawBackGround BottomPanel, 7, True, True
    CornerCover(0).Visible = False
    CornerCover(2).Visible = False
    ChartArea.AutoRedraw = False
    ViewArea.Visible = False
    NowTime(0).Visible = False
    NowTime(1).Visible = False
    lblNowTime(0).Visible = False
    lblNowTime(1).Visible = False
    lblNowTime(2).Visible = False
    
    TopLineCount = 0
    BottomLineCount = 0
    
    'Set some defaults (Maximum Square)
    MaxChartHeight = 36000
    MaxChartWidth = 36000
    
    BarHeight = Val(fraBarFix(2).Tag) * 15
    BarOffset = Val(fraBarFix(3).Tag) * 15
    If BarHeight < 15 Then BarHeight = 15
    
    IsValidBgnScalePos = StatusChart.GetTimeScalePos(DataFilterVpfr, DataBgn, DataBgnPos)
    IsValidEndScalePos = StatusChart.GetTimeScalePos(DataFilterVpto, DataEnd, DataEndPos)
    
    FlightWidth = 7500
    'FlightPanelColor = vbCyan
    FlightPanelColor = vbBlack
    
    DatWidth = DataEndPos + FlightWidth
    NewZoom = (DatWidth \ MaxChartWidth) + 1
    
    tmpVal = Val(fraZoomFix(2).Tag)
    If tmpVal > 0 Then NewZoom = (((NewZoom - 1) \ tmpVal) + 1) * tmpVal
    
    tmpVal = Val(fraZoomFix(1).Tag)
    Select Case fraZoomFix(0).Tag
        Case "FIXD"
            NewZoom = tmpVal
        Case "AUTO"
        Case "PREF"
            If NewZoom < tmpVal Then NewZoom = tmpVal
        Case Else
    End Select
    XZoom = NewZoom
    NewWidth = (DatWidth \ XZoom)
    'NewWidth = (((NewWidth - 15) \ 150) + 1) * 150
    
    If NewWidth > MaxChartWidth Then NewWidth = MaxChartWidth
    If NewWidth < 300 Then NewWidth = 300
    
    Me.Caption = GetItem(Me.Caption, 1, "(") & " (Zoom 1/" & CStr(XZoom) & ")"
    ChartArea.Width = NewWidth
    MaxRight = NewWidth
    FlightWidth = FlightWidth \ XZoom
    
    'We always try to show all rotations of the loaded flights ...
    'At least the best matching sequence of lines
    NewHeight = StatusChart.TotalRotLines * (BarHeight + BarOffset)
    EndLoop = False
    If (fraBarFix(0).Tag <> "AUTO") And (fraBarFix(1).Tag <> "AUTO") Then EndLoop = True
    While (NewHeight > MaxChartHeight) And (EndLoop = False)
        EndLoop = True
        If (fraBarFix(1).Tag = "AUTO") And (BarOffset > 15) Then
            EndLoop = False
            BarOffset = BarOffset - 15
            NewHeight = StatusChart.TotalRotLines * (BarHeight + BarOffset)
        End If
        If NewHeight > MaxChartHeight Then
            If (fraBarFix(0).Tag = "AUTO") And (BarHeight > 15) Then
                EndLoop = False
                BarHeight = BarHeight - 15
                NewHeight = StatusChart.TotalRotLines * (BarHeight + BarOffset)
            End If
        End If
    Wend
    If NewHeight > MaxChartHeight Then NewHeight = MaxChartHeight
    If NewHeight < 300 Then NewHeight = 300
    ChartArea.Height = NewHeight
    MaxLineCount = NewHeight \ (BarHeight + BarOffset)
    'Here we know how many lines fit on the selected image height
    If MaxLineCount >= StatusChart.TotalRotLines Then
        'Everything can be displayed
        MinTopLine = 0
        MaxBottomLine = MaxLineCount - 1
    Else
        'Must ignore some lines ...
        'Put the chart area in the middle ...
        BlockSize = StatusChart.TotalRotLines
        If BlockSize <= MaxLineCount Then
            BlockRest = MaxLineCount - BlockSize
            BlockAbove = BlockRest \ 2
            If BlockAbove > StatusChart.FirstRotLine Then BlockAbove = StatusChart.FirstRotLine + 1
            BlockBelow = BlockRest - BlockAbove
            MinTopLine = StatusChart.FirstRotLine - BlockAbove + 1
            MaxBottomLine = MinTopLine + BlockSize + BlockBelow - 1
        Else
            'We can't even display the main chart area
            BlockSize = MaxLineCount
            MinTopLine = StatusChart.FirstRotLine
            MaxBottomLine = MinTopLine + BlockSize - 1
        End If
    End If
    
    ChartArea.AutoRedraw = True
    ChartArea.Cls
    'DrawBackGround ChartArea, 12, False, True
    
    y1 = 0
    Y2 = NewHeight
    x1 = DataBgnPos \ XZoom
    DataLeft = x1
    X2 = x1 + 15
    If x1 < 0 Then x1 = 0
    If X2 > 0 Then ChartArea.Line (x1, y1)-(X2, Y2), vbMagenta, BF
    X3 = DataEndPos \ XZoom
    DataRight = X3
    X4 = X3 + 15
    If X3 < 0 Then X3 = 0
    If X4 > 0 Then ChartArea.Line (X3, y1)-(X4, Y2), vbMagenta, BF
    y1 = ((StatusChart.FirstRotLine - MinTopLine) * (BarHeight + BarOffset))
    DataTop = y1
    Y2 = y1 + BarHeight - 15
    ChartArea.Line (x1, y1)-(X3, Y2), vbCyan, BF
    Y3 = ((StatusChart.LastRotLine - MinTopLine) * (BarHeight + BarOffset))
    DataBottom = Y3
    Y4 = Y3 + BarHeight - 15
    ChartArea.Line (x1, Y3)-(X3, Y4), vbCyan, BF
    ChartArea.Line (x1, y1)-(X2, Y4), vbCyan, BF
    ChartArea.Line (X3, y1)-(X4, Y4), vbCyan, BF
    NowTime(0).Top = 0
    NowTime(0).Height = ChartArea.ScaleHeight
    picHint(0).ZOrder
    picHint(1).ZOrder
End Sub
Public Sub FinishChartArea()
    If TopLineCount > 0 Then
        TopPanel.Visible = True
        CornerCover(0).Visible = True
    End If
    If BottomLineCount > 0 Then
        BottomPanel.Visible = True
        CornerCover(2).Visible = True
    End If
    ChartArea.Visible = True
    ViewArea.Visible = True
    Form_Resize
    IsReadyForUse = True
    If (fraBlinker.Tag = "ON") And (TabBlinker.GetLineCount > 0) Then MyBlinker.Enabled = True
End Sub

Public Sub SynchronizeChartArea(ActLine As Long, TopLine As Long, BotLine As Long, LeftPos As Long, RightPos As Long)
    Dim NewTop As Long
    Dim OldTop As Long
    Dim ChkTop As Long
    Dim NewBot As Long
    Dim NewLeft As Long
    Dim OldLeft As Long
    Dim ChkLeft As Long
    Dim NewWidth As Long
    Dim ChkWidth As Long
    Dim NewHeight As Long
    Dim PanelPos As Long
    Dim PanelEnd As Long
    Dim RightEnd As Long
    Dim NewLeftPos As Long
    Dim NewTopPos As Long
    Dim ScrollPos As Integer
    If IsReadyForUse Then
        OldTop = -ChartArea.Top
        OldLeft = -ChartArea.Left
        NewTop = ((TopLine - MinTopLine) * (BarHeight + BarOffset))
        NewBot = ((BotLine - MinTopLine) * (BarHeight + BarOffset))
        'Set the rectangle above and below the bar lines
        'VB calculates the shape dimension based on borderwidth=1
        'but we have borderwidth=2 (painted outside of the shape)
        NewTop = NewTop - 30
        NewBot = NewBot + BarHeight + 15
        NewHeight = NewBot - NewTop + 30
        NewLeft = LeftPos \ XZoom
        NewWidth = RightPos \ XZoom
        NewWidth = NewWidth - NewLeft
        PanelPos = NewLeft + ChartArea.Left - 210
        PanelEnd = NewLeft + NewWidth + 330
        RightEnd = HScroll1.Width - ChartArea.Left
        If PanelPos < 0 Then
            NewLeftPos = -(ChartArea.Left - PanelPos)
            If Not MouseIsDown Then ChartArea.Left = -NewLeftPos
        ElseIf PanelEnd > RightEnd Then
            NewLeftPos = PanelEnd - RightEnd - ChartArea.Left
            If Not MouseIsDown Then ChartArea.Left = -NewLeftPos
        End If
        PanelPos = NewTop + ChartArea.Top
        PanelEnd = NewTop + NewHeight
        RightEnd = VScroll1.Height - ChartArea.Top
        If PanelPos < 0 Then
            NewTopPos = -(ChartArea.Top - PanelPos)
            If Not MouseIsDown Then ChartArea.Top = -NewTopPos
        ElseIf PanelEnd > RightEnd Then
            NewTopPos = PanelEnd - RightEnd - ChartArea.Top
            If Not MouseIsDown Then ChartArea.Top = -NewTopPos
        End If
        ViewArea.Move NewLeft, NewTop, NewWidth, NewHeight
        picCursor(0).Left = NewLeft - 240
        picCursor(1).Left = NewLeft + NewWidth
        If ActLine >= 0 Then
            NewTop = NewTop + ((ActLine - TopLine) * (BarHeight + BarOffset)) - 75
            picCursor(0).Top = NewTop
            picCursor(1).Top = NewTop
            picHint(0).Visible = False
            picHint(1).Visible = False
            picHint(0).Top = picCursor(0).Top - 135
            picHint(1).Top = picCursor(1).Top - 135
            picHint(0).ToolTipText = StatusChart.GetFlightInfo(ActLine, "A", 0)
            picHint(1).ToolTipText = StatusChart.GetFlightInfo(ActLine, "D", 0)
            If picHint(0).ToolTipText <> "" Then picHint(0).MousePointer = 99 Else picHint(0).MousePointer = 0
            If picHint(1).ToolTipText <> "" Then picHint(1).MousePointer = 99 Else picHint(1).MousePointer = 0
            picHint(0).Visible = NowTime(0).Visible
            picHint(1).Visible = NowTime(0).Visible
            picCursor(0).ToolTipText = picHint(0).ToolTipText
            picCursor(1).ToolTipText = picHint(1).ToolTipText
            picCursor(0).Visible = True
            picCursor(1).Visible = True
        Else
            picCursor(0).Visible = False
            picCursor(1).Visible = False
            picHint(0).Visible = False
            picHint(1).Visible = False
        End If
        AdjustScrollValues
        AdjustIndicatorColor
    End If
End Sub
Public Sub SynchronizeNowTime(TimePos As Long, TimeText As String)
    Dim NewLeft As Long
    If IsReadyForUse Then
        NewLeft = TimePos \ XZoom
        If (NewLeft > 0) And (NewLeft <= ChartArea.ScaleWidth) Then
            lblNowTime(0).Caption = TimeText
            'lblNowTime(1).Caption = TimeText
            'lblNowTime(2).Caption = TimeText
            NowTime(0).Left = NewLeft
            NowTime(1).Left = ChartArea.Left + NewLeft + 30
            lblNowTime(0).Left = NowTime(1).Left - (lblNowTime(0).Width \ 2) + 45
            lblNowTime(1).Left = NowTime(1).Left - lblNowTime(1).Width
            lblNowTime(2).Left = NowTime(1).Left + NowTime(1).Width + 15
            NowTime(0).Visible = True
            picHint(0).Left = NewLeft - picHint(0).Width
            picHint(1).Left = NewLeft + NowTime(0).Width
        Else
            NowTime(0).Visible = False
        End If
        'If picHint(0).ToolTipText <> "" Then picHint(0).Visible = NowTime(0).Visible
        'If picHint(1).ToolTipText <> "" Then picHint(1).Visible = NowTime(0).Visible
        picHint(0).Visible = NowTime(0).Visible
        picHint(1).Visible = NowTime(0).Visible
        NowTime(1).Visible = NowTime(0).Visible
        lblNowTime(0).Visible = NowTime(0).Visible
        lblNowTime(1).Visible = NowTime(0).Visible
        lblNowTime(2).Visible = NowTime(0).Visible
    End If

End Sub
Private Sub AdjustIndicatorColor()
    Dim PanelLeft As Long
    Dim PanelRight As Long
    Dim PanelTop As Long
    Dim PanelBottom As Long
    Dim UseBordColor As Long
    Dim UseFillColor As Long
    Dim SignalColor As Long
    Dim ReLoad As Boolean
    ViewArea.Visible = False
    UseBordColor = vbWhite
    UseFillColor = vbWhite
    ReLoad = CheckReloadFrame(False)
    If ReLoad = True Then SignalColor = vbRed Else SignalColor = vbCyan
    PanelTop = ViewArea.Top + 60
    PanelBottom = PanelTop + ViewArea.Height - 120
    If PanelTop < DataTop Then UseFillColor = SignalColor
    If PanelBottom > DataBottom Then UseFillColor = SignalColor
    'PanelLeft = ViewArea.Left
    'PanelRight = PanelLeft + ViewArea.Width
    'If PanelLeft < DataLeft Then UseFillColor = vbYellow
    'If PanelRight > DataRight Then UseFillColor = vbYellow
    ViewArea.FillColor = UseFillColor
    If ViewArea.FillStyle = 1 Then ViewArea.BorderColor = UseFillColor
    ViewArea.Visible = True
End Sub
Public Sub DrawFlightRotation(CurRotLine As Long, CurArrLine As Long, CurDepLine As Long, ArrColor As Long, DepColor As Long, CurBarIdx As Integer, ShowTimePointer As Boolean)
    Dim tmpView As String
    Dim tmpRidx As String
    Dim tmpHitLine As String
    Dim RotLine As Long
    Dim ArrLine As Long
    Dim DepLine As Long
    Dim BarIdx As Integer
    Dim RotBest As String
    Dim ArrTifa As String
    Dim DepTifd As String
    Dim ArrBarColor As Long
    Dim DepBarColor As Long
    Dim ArrScalePos As Long
    Dim DepScalePos As Long
    Dim DotScalePos As Long
    Dim ArrWidth As Long
    Dim DepWidth As Long
    Dim ArrLeft As Long
    Dim DepLeft As Long
    Dim x1 As Long  'ArrFlight.Left
    Dim X2 As Long  'ArrFlight.Right
    Dim X3 As Long  'ArrBar.Left
    Dim X4 As Long  'ArrBar.Right
    Dim X5 As Long  'DepBar.Left
    Dim X6 As Long  'DepBar.Right
    Dim X7 As Long  'DepFlight.Left
    Dim X8 As Long  'DepFlight.Right
    Dim X9 As Long  'RedDot.Left
    Dim X10 As Long 'RedDot.Right
    Dim y1 As Long  'ChartBar.Top
    Dim Y2 As Long  'ChartBar.Bottom
    Dim IsValidArrScalePos As Boolean
    Dim IsValidDepScalePos As Boolean
    Dim IsValidDotScalePos As Boolean
    Dim ShowRedDot As Boolean
    Dim ArrTifaValue
    Dim DepTifdValue
    Dim RotBestValue
    'On Error Resume Next
    
    RotLine = CurRotLine
    ArrLine = CurArrLine
    DepLine = CurDepLine
    ShowRedDot = True
    ArrBarColor = ArrColor
    DepBarColor = DepColor
    If ArrBarColor = 0 Then ArrBarColor = vbYellow
    If DepBarColor = 0 Then DepBarColor = vbGreen
    
    y1 = ((RotLine - MinTopLine) * (BarHeight + BarOffset))
    Y2 = y1 + BarHeight - 15
    'Y1 = (Y1 * 15) \ 15
    'Y2 = (Y2 * 15) \ 15
    
    If (ArrLine < 0) And (DepLine < 0) Then
        tmpView = "1"
        'tmpView = TabRotFlightsTab.GetFieldValue(RotLine, "VIEW")
        If tmpView = "1" Then
            tmpRidx = TabRotFlightsTab.GetFieldValue(RotLine, "RIDX")
            tmpHitLine = TabArrFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
            If tmpHitLine <> "" Then ArrLine = Val(tmpHitLine)
            tmpHitLine = TabDepFlightsTab.GetLinesByIndexValue("RIDX", tmpRidx, 0)
            If tmpHitLine <> "" Then DepLine = Val(tmpHitLine)
        End If
    End If
    If (RotLine >= MinTopLine) And (RotLine <= MaxBottomLine) Then
        If (ArrLine >= 0) And (DepLine >= 0) Then
            'ROTATION
            'First calculate the dimensions of the original chart bar ...
            ArrTifa = TabRotFlightsTab.GetFieldValue(RotLine, "TIFA")
            DepTifd = TabRotFlightsTab.GetFieldValue(RotLine, "TIFD")
            IsValidArrScalePos = StatusChart.GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
            IsValidDepScalePos = StatusChart.GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
            ArrWidth = DepScalePos - ArrScalePos
            If ArrWidth < StatusChart.MinArrWidth Then ArrWidth = StatusChart.MinArrWidth
            DepWidth = ArrWidth \ 2
            If DepWidth > StatusChart.MaxDepWidth Then DepWidth = StatusChart.MaxDepWidth
            If DepWidth < StatusChart.MinDepWidth Then DepWidth = StatusChart.MinDepWidth
            ArrLeft = ArrScalePos
            ArrWidth = ArrLeft + ArrWidth
            DepLeft = DepScalePos - DepWidth
            DepWidth = DepLeft + DepWidth
            '... and now zoom to fit the overview resolution
            X3 = ArrLeft \ XZoom
            X4 = ArrWidth \ XZoom
            X5 = DepLeft \ XZoom
            X6 = DepWidth \ XZoom
            X7 = DepScalePos \ XZoom
            X8 = X7 + FlightWidth
            x1 = X3 - FlightWidth
            X2 = x1 + FlightWidth
            If x1 < 0 Then x1 = 0
            If X2 > MaxRight Then X2 = MaxRight
            If X2 > 0 Then ChartArea.Line (x1, y1)-(X2, Y2), FlightPanelColor, BF
            If X3 < 0 Then X3 = 0
            If X4 > MaxRight Then X4 = MaxRight
            If X4 > 0 Then ChartArea.Line (X3, y1)-(X4, Y2), ArrBarColor, BF
            If X5 < 0 Then X5 = 0
            If X6 > MaxRight Then X6 = MaxRight
            If X6 > 0 Then ChartArea.Line (X5, y1)-(X6, Y2), DepBarColor, BF
            If X7 < 0 Then X7 = 0
            If X8 > MaxRight Then X8 = MaxRight
            If X8 > 0 Then ChartArea.Line (X7, y1)-(X8, Y2), FlightPanelColor, BF
        ElseIf DepLine >= 0 Then
            'SINGLE DEPARTURE
            DepTifd = TabRotFlightsTab.GetFieldValue(RotLine, "TIFD")
            IsValidDepScalePos = StatusChart.GetTimeScalePos(DepTifd, DepTifdValue, DepScalePos)
            DepWidth = StatusChart.MaxDepWidth
            DepLeft = DepScalePos - DepWidth
            DepWidth = DepLeft + DepWidth
            X5 = DepLeft \ XZoom
            X6 = DepWidth \ XZoom
            X7 = DepScalePos \ XZoom
            X8 = X7 + FlightWidth
            If X5 < 0 Then X5 = 0
            If X6 > MaxRight Then X6 = MaxRight
            If X6 > 0 Then ChartArea.Line (X5, y1)-(X6, Y2), DepBarColor, BF
            If X7 < 0 Then X7 = 0
            If X8 > MaxRight Then X8 = MaxRight
            If X8 > 0 Then ChartArea.Line (X7, y1)-(X8, Y2), FlightPanelColor, BF
        ElseIf ArrLine >= 0 Then
            'SINGLE ARRIVAL
            ArrTifa = TabRotFlightsTab.GetFieldValue(RotLine, "TIFA")
            IsValidArrScalePos = StatusChart.GetTimeScalePos(ArrTifa, ArrTifaValue, ArrScalePos)
            ArrWidth = StatusChart.MaxArrWidth
            ArrLeft = ArrScalePos
            ArrWidth = ArrLeft + ArrWidth
            X3 = ArrLeft \ XZoom
            X4 = ArrWidth \ XZoom
            x1 = X3 - FlightWidth
            X2 = x1 + FlightWidth
            If x1 < 0 Then x1 = 0
            If X2 > MaxRight Then X2 = MaxRight
            If X2 > 0 Then ChartArea.Line (x1, y1)-(X2, Y2), FlightPanelColor, BF
            If X3 < 0 Then X3 = 0
            If X4 > MaxRight Then X4 = MaxRight
            If X4 > 0 Then ChartArea.Line (X3, y1)-(X4, Y2), ArrBarColor, BF
        Else
            ShowRedDot = False
        End If
        If ShowRedDot = True Then
            RotBest = TabRotFlightsTab.GetFieldValue(RotLine, "BEST")
            IsValidDotScalePos = StatusChart.GetTimeScalePos(RotBest, RotBestValue, DotScalePos)
            X9 = DotScalePos \ XZoom
            X10 = X9 + 15
            If X9 < 0 Then X9 = 0
            If X10 > 0 Then ChartArea.Line (X9, y1)-(X10, Y2), vbBlack, BF
            'TEST ONLY
            'If RotLine Mod 10 = 0 Then
            '    If ArrLine >= 0 Then AddLineToBlinker RotLine, "A", ArrTifa, RotBest
            '    If DepLine >= 0 Then AddLineToBlinker RotLine, "D", DepTifd, RotBest
            'End If
        End If
    Else
        If RotLine < MinTopLine Then TopLineCount = TopLineCount + 1
        If RotLine > MaxBottomLine Then BottomLineCount = BottomLineCount + 1
    End If
End Sub

Private Sub ButtonPanel_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    chkWork(6).Value = 0
End Sub

Private Sub ChartArea_DblClick()
    Dim MainLeft As Long
    Dim MainLine As Long
    If ChartArea.MousePointer = 1 Then
        MouseIsDown = True
        MouseCursorIsBusy = True
        MainLeft = ViewArea.Left
        MainLeft = MainLeft * XZoom
        If MainLeft < 0 Then MainLeft = 0
        MainLine = ChartY \ (BarHeight + BarOffset)
        StatusChart.ReSyncCursor MainLine, -MainLeft
        MouseCursorIsBusy = False
        MouseIsDown = False
    End If
End Sub

Private Sub ChartArea_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim MainLeft As Long
    Dim MainLine As Long
    chkWork(6).Value = 0
    MouseTimer.Enabled = False
    MouseIsWaiting = False
    MouseIsMoving = False
    If (ChartArea.MousePointer = 1) Or (Shift = 1) Then
        'If Shift = 1 Then ChartArea.MousePointer = 15
        ChartArea.MousePointer = 11
        MouseIsWaiting = True
        MouseTimer.Enabled = True
        MouseX = CLng(x) - ViewArea.Left
        MouseY = CLng(y) - ViewArea.Top
        ChartX = CLng(x)
        ChartY = CLng(y)
    ElseIf ChartArea.MousePointer = 99 Then
        MouseIsDown = True
        MouseCursorIsBusy = True
        MainLeft = CLng(x) - (ViewArea.Width \ 2)
        MainLeft = MainLeft * XZoom
        If MainLeft < 0 Then MainLeft = 0
        MainLine = CLng(y) \ (BarHeight + BarOffset)
        ViewArea.Left = CLng(x) - (ViewArea.Width \ 2)
        ViewArea.Top = CLng(y) - (ViewArea.Height \ 2)
        MouseX = CLng(x) - ViewArea.Left
        MouseY = CLng(y) - ViewArea.Top
        ChartX = CLng(x)
        ChartY = CLng(y)
        StatusChart.ReSyncCursor MainLine, -MainLeft
        MouseCursorIsBusy = False
    End If
End Sub

Private Sub ChartArea_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim CurX As Long
    Dim CurY As Long
    Dim x1 As Long
    Dim X2 As Long
    Dim Y0 As Long
    Dim y1 As Long
    Dim Y2 As Long
    Dim ShpX1 As Long
    Dim ShpX2 As Long
    Dim ShpY1 As Long
    Dim ShpY2 As Long
    Dim OldLeft As Long
    Dim NewLeft As Long
    Dim OldTop As Long
    Dim NewTop As Long
    Dim MainLeft As Long
    Dim MainTopLine As Long
    If Not MouseCursorIsBusy Then
        CurX = CLng(x)
        CurY = CLng(y)
        If Not MouseIsMoving Then
            ShpX1 = ViewArea.Left
            ShpX2 = ShpX1 + ViewArea.Width
            ShpY1 = ViewArea.Top
            ShpY2 = ShpY1 + ViewArea.Height
            If (CurX >= ShpX1) And (CurX <= ShpX2) And (CurY >= ShpY1) And (CurY <= ShpY2) Then
                If Not MouseIsDown Then
                    ChartArea.MousePointer = 1
                Else
                    ChartArea.MousePointer = 0
                End If
            Else
                ChartArea.MousePointer = 99
            End If
        Else    'Mouse Is Moving
            If Shift = 0 Then
                ChartX = CurX
                ChartY = CurY
            End If
            OldLeft = ViewArea.Left
            NewLeft = CurX - MouseX
            OldTop = ViewArea.Top
            NewTop = CurY - MouseY
            AdjustIndicatorColor
            If (NewLeft <> OldLeft) Or (NewTop <> OldTop) Then
                If NewLeft >= 0 Then ViewArea.Left = NewLeft
                If NewTop >= 0 Then ViewArea.Top = NewTop
                MainLeft = NewLeft * XZoom
                If MainLeft < 0 Then MainLeft = 0
                MainTopLine = (NewTop + 45) \ (BarHeight + BarOffset)
                If picCursor(0).Tag = "" Then picCursor(0).Tag = CStr(MainTopLine)
                StatusChart.SynchronizeChartArea MainTopLine, -MainLeft, False
            End If
            
            
            If Shift = 1 Then
                NewLeft = ChartArea.Left + (CurX - ChartX)
                If NewLeft <= 0 Then ChartArea.Left = NewLeft
                NewTop = ChartArea.Top + (CurY - ChartY)
                If NewTop <= 0 Then ChartArea.Top = NewTop
                ChartArea.Refresh
                AdjustScrollValues
            ElseIf Shift = 0 Then
                If OldTop <> NewTop Then
                    y1 = ViewArea.Top + ChartArea.Top
                    ShpY2 = NewTop + ViewArea.Height
                    Y2 = Abs(ChartArea.Top) + WorkArea.Height
                    If (ShpY2 > Y2) And (VScroll1.Value < 100) Then
                        ScrollTimer(1).Tag = "+1"
                        ScrollTimer(1).Enabled = True
                    ElseIf (y1 < 0) And (VScroll1.Value > 0) Then
                        ScrollTimer(1).Tag = "-1"
                        ScrollTimer(1).Enabled = True
                    Else
                        ScrollTimer(1).Enabled = False
                    End If
                End If
                If OldLeft <> NewLeft Then
                    x1 = ViewArea.Left + ChartArea.Left
                    ShpX2 = NewLeft + ViewArea.Width
                    X2 = Abs(ChartArea.Left) + WorkArea.Width
                    If (ShpX2 > X2) And (HScroll1.Value < 100) Then
                        ScrollTimer(0).Tag = "+1"
                        ScrollTimer(0).Enabled = True
                    ElseIf (x1 < 0) And (HScroll1.Value > 0) Then
                        ScrollTimer(0).Tag = "-1"
                        ScrollTimer(0).Enabled = True
                    Else
                        ScrollTimer(0).Enabled = False
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub ChartArea_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    ScrollTimer(0).Enabled = False
    ScrollTimer(1).Enabled = False
    MouseTimer.Enabled = False
    MouseIsWaiting = False
    MouseIsDown = False
    MouseIsMoving = False
    CheckReloadFrame True
End Sub
Private Function CheckReloadFrame(ReloadFlights As Boolean) As Boolean
    Dim MainTopLine As Long
    Dim BlockArea As Long
    Dim BlockSize As Long
    Dim ReLoad As Boolean
    ReLoad = False
    BlockSize = ViewArea.Height \ 2
    If ViewArea.Top <= DataTop Then
        BlockArea = DataTop - ViewArea.Top
        If BlockArea > BlockSize Then ReLoad = True
        If (DataTop < ViewArea.Height) And (StatusChart.FirstRotLine > 0) Then ReLoad = True
    End If
    If (ViewArea.Top + ViewArea.Height) >= DataBottom Then
        BlockArea = DataBottom - ViewArea.Top
        If BlockArea < BlockSize Then ReLoad = True
    End If
    If (ReLoad = True) And (ReloadFlights = True) Then
        MainTopLine = (ViewArea.Top + 45) \ (BarHeight + BarOffset)
        If (DataTop < ViewArea.Height) And (StatusChart.FirstRotLine > 0) Then MainTopLine = 0
        StatusChart.SynchronizeChartArea MainTopLine, -1, True
    End If
    CheckReloadFrame = ReLoad
End Function
Private Sub chkWork_Click(Index As Integer)
    Dim tmpTag As String
    Dim PrvIdx As String
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                If IsReadyForUse Then StatusChart.GetFlightRotations -1, True
                chkWork(Index).Value = 0
            Case 1 To 4
                tmpTag = chkWork(1).Tag
                PrvIdx = Val(tmpTag)
                If tmpTag <> "" Then
                    chkWork(Val(PrvIdx)).Value = 0
                End If
                chkWork(1).Tag = CStr(Index)
                OptionPanel(0).Top = ButtonPanel(1).Top + ButtonPanel(0).Height - 15
                OptionPanel(0).Left = ButtonPanel(1).Left + chkWork(Index).Left
                OptionPanel(0).Width = OptionPanel(Index).Width
                OptionPanel(0).Height = OptionPanel(Index).Height
                OptionPanel(Index).Visible = True
                PanelShadow(0).Width = OptionPanel(0).Width
                PanelShadow(0).Height = OptionPanel(0).Height
                PanelShadow(0).Left = OptionPanel(0).Left + 90
                PanelShadow(0).Top = OptionPanel(0).Top + 90
                AdjustButtonPanels
                PanelShadow(0).Visible = True
                OptionPanel(0).Visible = True
            Case 5
                If picCursor(0).Tag <> "" Then
                    ViewArea.Left = picCursor(0).Left + 240
                    StatusChart.SynchronizeChartArea Val(picCursor(0).Tag), -ViewArea.Left, False
                    'StatusChart.ReSyncCursor -1, -1
                End If
                chkWork(Index).Value = 0
            Case 6
                'AdjustButtonPanels
                PanelShadow(1).Visible = True
                ButtonPanel(1).Visible = True
                If chkWork(1).Tag <> "" Then
                    PanelShadow(0).Visible = True
                    OptionPanel(0).Visible = True
                End If
            Case Else
                chkWork(Index).Value = 0
        End Select
    Else
        Select Case Index
            Case 1 To 4
                tmpTag = chkWork(1).Tag
                PrvIdx = Val(tmpTag)
                If (tmpTag <> "") And (PrvIdx = Index) Then
                    OptionPanel(Index).Visible = False
                    PanelShadow(0).Visible = False
                    OptionPanel(0).Visible = False
                    chkWork(1).Tag = ""
                End If
            Case 6
                PanelShadow(1).Visible = False
                ButtonPanel(1).Visible = False
                PanelShadow(0).Visible = False
                OptionPanel(0).Visible = False
            Case Else
        End Select
        chkWork(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub AdjustButtonPanels()
    Dim OldLeft As Long
    Dim NewLeft As Long
    Dim DefLeft As Long
    OldLeft = ButtonPanel(1).Left
    NewLeft = Me.ScaleWidth - ButtonPanel(1).Width - VScroll1.Width
    DefLeft = ButtonPanel(0).Left + chkWork(6).Left
    If NewLeft > DefLeft Then NewLeft = DefLeft
    If NewLeft < 0 Then NewLeft = 0
    ButtonPanel(1).Left = NewLeft
    PanelShadow(1).Left = NewLeft + 90
    OptionPanel(0).Left = OptionPanel(0).Left + (NewLeft - OldLeft)
    PanelShadow(0).Left = OptionPanel(0).Left + 90
End Sub
Private Sub Form_Load()
    Dim i As Integer
    IsReadyForUse = False
    For i = 1 To 4
        Set OptionPanel(i).Container = OptionPanel(0)
        OptionPanel(i).Top = 0
        OptionPanel(i).Left = 0
    Next
    TopPanel.Top = ButtonPanel(0).Top + ButtonPanel(0).Height
    
    ButtonPanel(1).Top = ButtonPanel(0).Top + ButtonPanel(0).Height
    ButtonPanel(1).Left = ButtonPanel(0).Left + chkWork(6).Left
    PanelShadow(1).Width = ButtonPanel(1).Width
    PanelShadow(1).Height = ButtonPanel(1).Height + 90
    PanelShadow(1).Left = ButtonPanel(1).Left + 90
    PanelShadow(1).Top = ButtonPanel(1).Top
    
    CornerCover(0).Height = TopPanel.Height
    CornerCover(0).Top = TopPanel.Top - 15
    CornerCover(0).Width = VScroll1.Width + 30
    CornerCover(1).Height = HScroll1.Height + 15
    CornerCover(1).Width = VScroll1.Width + 30
    CornerCover(2).Height = BottomPanel.Height + 30
    CornerCover(2).Width = VScroll1.Width + 30
    ViewArea.ZOrder
    
    PanelShadow(1).ZOrder
    PanelShadow(0).ZOrder
    CornerCover(1).ZOrder
    HScroll1.ZOrder
    VScroll1.ZOrder
    ButtonPanel(0).ZOrder
    OptionPanel(0).ZOrder
    ButtonPanel(1).ZOrder
    StatusBar1.ZOrder
    InitMyTabs
    InitMyDefaults
End Sub
Private Sub InitMyDefaults()
    optBlink(1).Value = True
    optFreqOn(1).Value = True
    optFreqOff(1).Value = True
    optBarFix(1).Value = True
    optSpaceFix(1).Value = True
    optBarPix(1).Value = True
    optSpcPix(1).Value = True
    optZoomFix(1).Value = True
    optZoomVal(3).Value = True
    optZoomRnd(0).Value = True
End Sub
Private Sub InitMyTabs()
    TabBlinker.ResetContent
    TabBlinker.HeaderString = "LINE,ADID,TIME,BEST"
    TabBlinker.HeaderLengthString = "10,1,14,14"
    TabBlinker.LogicalFieldList = "LINE,ADID,TIME,BEST"
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Me.Hide
    StatusChart.chkWork(5).Value = 0
    Cancel = True
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    Dim MaxSize As Long
    Dim OldWidth As Long
    Dim OldHeight As Long
    OldWidth = WorkArea.Width
    OldHeight = WorkArea.Height
    ButtonPanel(0).Width = Me.ScaleWidth + 15
    NewSize = Me.ScaleWidth - VScroll1.Width
    VScroll1.Left = NewSize
    CornerCover(0).Left = NewSize - 30
    CornerCover(1).Left = NewSize - 30
    CornerCover(2).Left = NewSize - 30
    HScroll1.Left = WorkArea.Left
    NewSize = NewSize - WorkArea.Left
    If NewSize > 300 Then
        HScroll1.Width = NewSize
        WorkArea.Width = NewSize
        TopPanel.Width = NewSize
        BottomPanel.Width = NewSize
    End If
    If ChartArea.Left < 0 Then
        MaxSize = -(ChartArea.Width - WorkArea.Width + 60)
        NewSize = ChartArea.Left - OldWidth + NewSize
        If NewSize < MaxSize Then NewSize = MaxSize
        If NewSize > 0 Then NewSize = 0
        ChartArea.Left = NewSize
    End If
    
    NewSize = Me.ScaleHeight - StatusBar1.Height
    If BottomPanel.Visible Then
        NewSize = NewSize - BottomPanel.Height
        BottomPanel.Top = NewSize
        CornerCover(2).Top = NewSize - 15
    End If
    NewSize = NewSize - HScroll1.Height
    CornerCover(1).Top = NewSize - 30
    HScroll1.Top = NewSize
    If TopPanel.Visible Then
        WorkArea.Top = TopPanel.Top + TopPanel.Height
    Else
        WorkArea.Top = ButtonPanel(0).Top + ButtonPanel(0).Height
    End If
    VScroll1.Top = WorkArea.Top
    NewSize = NewSize - WorkArea.Top
    If NewSize > 300 Then
        VScroll1.Height = NewSize
        WorkArea.Height = NewSize
    End If
    If ChartArea.Top < 0 Then
        MaxSize = -(ChartArea.Height - WorkArea.Height + 60)
        NewSize = ChartArea.Top - OldHeight + NewSize
        If NewSize < MaxSize Then NewSize = MaxSize
        If NewSize > 0 Then NewSize = 0
        ChartArea.Top = NewSize
    End If
    AdjustScrollValues
    AdjustButtonPanels
End Sub

Private Sub HScroll1_Change()
    Dim NewLeft As Long
    If DontScroll = False Then
        NewLeft = (ChartArea.Width - WorkArea.Width + 60) * CLng(HScroll1.Value) \ 100
        If NewLeft < 0 Then NewLeft = 0
        ChartArea.Left = -NewLeft
    End If
End Sub

Private Sub HScroll1_Scroll()
    Dim NewLeft As Long
    If DontScroll = False Then
        NewLeft = (ChartArea.Width - WorkArea.Width + 60) * CLng(HScroll1.Value) \ 100
        If NewLeft < 0 Then NewLeft = 0
        ChartArea.Left = -NewLeft
    End If
End Sub

Private Sub MouseTimer_Timer()
    MouseTimer.Enabled = False
    If MouseIsWaiting = True Then
        ChartArea.MousePointer = 15
        MouseIsMoving = True
    End If
End Sub

Private Sub MyBlinker_Timer()
    TabBlinker.TimerCheck
End Sub

Private Sub optBarFix_Click(Index As Integer)
    fraBarFix(0).Tag = optBarFix(Index).Tag
    chkWork(0).Value = 1
End Sub

Private Sub optBarPix_Click(Index As Integer)
    fraBarFix(2).Tag = optBarPix(Index).Tag
    chkWork(0).Value = 1
End Sub

Private Sub optBlink_Click(Index As Integer)
    fraBlinker.Tag = optBlink(Index).Tag
    If IsReadyForUse Then
        Select Case fraBlinker.Tag
            Case "ON"
                MyBlinker.Enabled = True
            Case "OFF"
                MyBlinker.Enabled = False
            Case Else
        End Select
    End If
End Sub

Private Sub optFreqOff_Click(Index As Integer)
    fraInterval(1).Tag = optFreqOff(Index).Tag
End Sub

Private Sub optFreqOn_Click(Index As Integer)
    fraInterval(0).Tag = optFreqOn(Index).Tag
End Sub

Private Sub optLayout_Click(Index As Integer)
    chkWork(0).Value = 1
End Sub

Private Sub optSpaceFix_Click(Index As Integer)
    fraBarFix(1).Tag = optSpaceFix(Index).Tag
    chkWork(0).Value = 1
End Sub

Private Sub optSpcPix_Click(Index As Integer)
    fraBarFix(3).Tag = optSpcPix(Index).Tag
    chkWork(0).Value = 1
End Sub

Private Sub optZoomFix_Click(Index As Integer)
    fraZoomFix(0).Tag = optZoomFix(Index).Tag
    chkWork(0).Value = 1
End Sub

Private Sub optZoomRnd_Click(Index As Integer)
    fraZoomFix(2).Tag = optZoomRnd(Index).Tag
    chkWork(0).Value = 1
End Sub

Private Sub optZoomVal_Click(Index As Integer)
    fraZoomFix(1).Tag = optZoomVal(Index).Tag
    chkWork(0).Value = 1
End Sub

Private Sub ScrollTimer_Timer(Index As Integer)
    Dim XDiff As Integer
    Dim OldLeft As Long
    Dim NewLeft As Long
    'Dim MainLeft As Long
    Dim LeftDiff As Long
    Dim YDiff As Integer
    Dim OldTop As Long
    Dim NewTop As Long
    Dim TopDiff As Long
    If Not MouseCursorIsBusy Then
        ViewArea.Visible = False
        Select Case Index
            Case 0
                XDiff = Val(ScrollTimer(Index).Tag)
                If ((XDiff > 0) And (HScroll1.Value < 100)) Or ((XDiff < 0) And (HScroll1.Value > 0)) Then
                    OldLeft = ChartArea.Left
                    HScroll1.Value = HScroll1.Value + XDiff
                    NewLeft = ChartArea.Left
                    If OldLeft <> NewLeft Then
                        LeftDiff = OldLeft - NewLeft
                        ViewArea.Left = ViewArea.Left + LeftDiff
                    End If
                Else
                    ScrollTimer(Index).Enabled = False
                End If
            Case 1
                YDiff = Val(ScrollTimer(Index).Tag)
                If ((YDiff > 0) And (VScroll1.Value < 100)) Or ((YDiff < 0) And (VScroll1.Value > 0)) Then
                    OldTop = ChartArea.Top
                    VScroll1.Value = VScroll1.Value + YDiff
                    NewTop = ChartArea.Top
                    If OldTop <> NewTop Then
                        TopDiff = OldTop - NewTop
                        ViewArea.Top = ViewArea.Top + TopDiff
                    End If
                Else
                    ScrollTimer(Index).Enabled = False
                End If
        End Select
        ChartArea.Refresh
        ViewArea.Visible = True
        ViewArea.Refresh
    End If
End Sub

Private Sub TabBlinker_TimerExpired(ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim tmpLine As String
    Dim tmpAdid As String
    Dim tmpTime As String
    Dim RotLine As Long
    Dim DotScalePos As Long
    Dim BckColor As Long
    Dim TxtColor As Long
    Dim NewVal As Long
    Dim x0 As Long
    Dim x1 As Long
    Dim X2 As Long
    Dim y1 As Long
    Dim Y2 As Long
    Dim TimeValue
    Dim IsValidDotScalePos As Boolean
    If IsReadyForUse Then
        tmpLine = TabBlinker.GetFieldValue(LineNo, "LINE")
        tmpAdid = TabBlinker.GetFieldValue(LineNo, "ADID")
        tmpTime = TabBlinker.GetFieldValue(LineNo, "TIME")
        TabBlinker.GetLineColor LineNo, TxtColor, BckColor
        RotLine = Val(tmpLine)
        y1 = ((RotLine - MinTopLine) * (BarHeight + BarOffset))
        Y2 = y1 + BarHeight - 15
        IsValidDotScalePos = StatusChart.GetTimeScalePos(tmpTime, TimeValue, DotScalePos)
        x0 = DotScalePos \ XZoom
        Select Case tmpAdid
            Case "A"
                x1 = x0 - FlightWidth
                X2 = x1 + FlightWidth
            Case "D"
                x1 = x0
                X2 = x1 + FlightWidth
            Case Else
                tmpLine = ""
        End Select
        If tmpLine <> "" Then
            If x1 < 0 Then x1 = 0
            If X2 > MaxRight Then X2 = MaxRight
            If X2 > 0 Then ChartArea.Line (x1, y1)-(X2, Y2), BckColor, BF
            tmpTime = TabBlinker.GetFieldValue(LineNo, "BEST")
            IsValidDotScalePos = StatusChart.GetTimeScalePos(tmpTime, TimeValue, DotScalePos)
            x1 = DotScalePos \ XZoom
            X2 = x1 + 15
            If x1 < 0 Then x1 = 0
            If X2 > MaxRight Then X2 = MaxRight
            If X2 > 0 Then ChartArea.Line (x1, y1)-(X2, Y2), vbBlack, BF
            TabBlinker.SetLineColor LineNo, BckColor, TxtColor
        End If
    End If
    If BckColor = vbRed Then NewVal = Val(fraInterval(0).Tag) Else NewVal = Val(fraInterval(1).Tag)
    TabBlinker.SetLineStatusValue LineNo, NewVal
    TabBlinker.TimerSetValue LineNo, CInt(NewVal)
End Sub

Private Sub VScroll1_Change()
    Dim NewTop As Long
    If DontScroll = False Then
        NewTop = (ChartArea.Height - WorkArea.Height + 60) * CLng(VScroll1.Value) \ 100
        If NewTop < 0 Then NewTop = 0
        ChartArea.Top = -NewTop
    End If
End Sub

Private Sub VScroll1_Scroll()
    Dim NewTop As Long
    If DontScroll = False Then
        NewTop = (ChartArea.Height - WorkArea.Height + 60) * CLng(VScroll1.Value) \ 100
        If NewTop < 0 Then NewTop = 0
        ChartArea.Top = -NewTop
    End If
End Sub
Private Sub AdjustScrollValues()
    Dim ChkLeft As Long
    Dim ChkWidth As Long
    Dim ChkTop As Long
    Dim ChkHeight As Long
    Dim ScrollPos As Integer
    DontScroll = True
    ChkLeft = -ChartArea.Left
    ChkWidth = ChartArea.Width - WorkArea.Width + 60
    If ChkWidth > 0 Then
        ScrollPos = CInt((ChkLeft * 100 \ ChkWidth))
        If ScrollPos < 0 Then ScrollPos = 0
        If ScrollPos > 100 Then ScrollPos = 100
        HScroll1.Value = ScrollPos
    End If
    ChkTop = -ChartArea.Top
    ChkHeight = ChartArea.Height - WorkArea.Height + 60
    If ChkHeight > 0 Then
        ScrollPos = CInt((ChkTop * 100 \ ChkHeight))
        If ScrollPos < 0 Then ScrollPos = 0
        If ScrollPos > 100 Then ScrollPos = 100
        VScroll1.Value = ScrollPos
    End If
    DontScroll = False
End Sub
Public Sub AddLineToBlinker(RotLine As Long, CurAdid As String, CedaTime As String, CheckTime As String)
    Dim tmpRec As String
    Dim LineNo As Long
    Dim NewVal As Long
    tmpRec = CStr(RotLine) & "," & CurAdid & "," & CedaTime & "," & CheckTime
    TabBlinker.InsertTextLine tmpRec, False
    LineNo = TabBlinker.GetLineCount - 1
    NewVal = Val(fraInterval(0).Tag)
    TabBlinker.TimerSetValue LineNo, CInt(NewVal)
    TabBlinker.SetLineColor LineNo, FlightPanelColor, vbRed
End Sub
