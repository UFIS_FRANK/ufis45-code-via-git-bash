VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form FilterDialog 
   Caption         =   "Record Filter Preset"
   ClientHeight    =   5520
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FilterDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5520
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox ServerPanel 
      BackColor       =   &H00808080&
      Height          =   375
      Left            =   60
      ScaleHeight     =   315
      ScaleWidth      =   7905
      TabIndex        =   112
      Top             =   60
      Visible         =   0   'False
      Width           =   7965
      Begin VB.CheckBox chkAppl 
         Caption         =   "&Load"
         Height          =   315
         Index           =   4
         Left            =   2730
         Style           =   1  'Graphical
         TabIndex        =   113
         Tag             =   "AODB_FILTER"
         Top             =   0
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label ServerLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   1
         Left            =   1080
         TabIndex        =   115
         Top             =   60
         Width           =   540
      End
      Begin VB.Label ServerLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Index           =   0
         Left            =   90
         TabIndex        =   114
         Top             =   45
         Width           =   540
      End
   End
   Begin VB.Timer AutoStartTimer 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   2940
      Top             =   3330
   End
   Begin VB.PictureBox FtypPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2985
      Left            =   8550
      ScaleHeight     =   2925
      ScaleWidth      =   1365
      TabIndex        =   65
      Top             =   60
      Width           =   1425
      Begin VB.CheckBox chkFtypAll 
         BackColor       =   &H008080FF&
         Caption         =   "<"
         Height          =   225
         Index           =   0
         Left            =   45
         MaskColor       =   &H8000000F&
         TabIndex        =   70
         Tag             =   "-1"
         Top             =   1440
         Width           =   420
      End
      Begin VB.TextBox txtFtypFilter 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         TabIndex        =   69
         Text            =   "FTYP FILTER"
         Top             =   2280
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CheckBox chkFtypFilter 
         BackColor       =   &H008080FF&
         Caption         =   "X"
         Height          =   225
         Index           =   0
         Left            =   45
         MaskColor       =   &H8000000F&
         TabIndex        =   66
         Tag             =   "-1"
         Top             =   1980
         Width           =   420
      End
      Begin VB.Label lblFtypAll 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "All"
         ForeColor       =   &H00C0FFFF&
         Height          =   210
         Index           =   0
         Left            =   540
         TabIndex        =   71
         ToolTipText     =   "Checks or Unchecks All"
         Top             =   1440
         Width           =   210
      End
      Begin VB.Label lblFtypName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CXX"
         ForeColor       =   &H00C0FFFF&
         Height          =   210
         Index           =   0
         Left            =   570
         TabIndex        =   68
         Top             =   1980
         Width           =   330
      End
      Begin VB.Label lblFtypShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CXX"
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   0
         Left            =   585
         TabIndex        =   67
         Top             =   1995
         Width           =   330
      End
      Begin VB.Label lblFtypAllShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "All"
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   0
         Left            =   810
         TabIndex        =   72
         Top             =   1440
         Width           =   210
      End
   End
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2985
      Left            =   10215
      ScaleHeight     =   2925
      ScaleWidth      =   1095
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   60
      Width           =   1155
      Begin VB.CheckBox chkAppl 
         Caption         =   "&Load"
         Height          =   315
         Index           =   5
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   118
         Tag             =   "AODB_FILTER_LOAD"
         Top             =   690
         Width           =   1035
      End
      Begin VB.HScrollBar FilterScroll 
         Height          =   315
         Left            =   30
         TabIndex        =   55
         Top             =   30
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Reset"
         Height          =   315
         Index           =   3
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   18
         Tag             =   "RESETFILTER"
         Top             =   1350
         Width           =   1035
      End
      Begin VB.CheckBox chkFilterIsVisible 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   45
         MaskColor       =   &H8000000F&
         TabIndex        =   60
         Tag             =   "-1"
         Top             =   2520
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.CheckBox chkFullSize 
         Enabled         =   0   'False
         Height          =   225
         Left            =   60
         TabIndex        =   57
         Top             =   2010
         Width           =   210
      End
      Begin VB.CheckBox chkToggle 
         Height          =   225
         Left            =   60
         TabIndex        =   56
         Top             =   2250
         Width           =   210
      End
      Begin VB.CheckBox chkOnTop 
         Caption         =   "Top"
         Enabled         =   0   'False
         Height          =   315
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   30
         Width           =   720
      End
      Begin VB.CheckBox Check2 
         Enabled         =   0   'False
         Height          =   315
         Left            =   750
         Style           =   1  'Graphical
         TabIndex        =   53
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Refresh"
         Height          =   315
         Index           =   2
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   19
         Tag             =   "AODBREFRESH"
         Top             =   1680
         Width           =   1035
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "&Save"
         Height          =   315
         Index           =   1
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   16
         Tag             =   "AODB_FILTER"
         Top             =   390
         Width           =   1035
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Close"
         Height          =   315
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   17
         Tag             =   "CLOSE"
         Top             =   1020
         Width           =   1035
      End
      Begin VB.Label lblToggle 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Toggle"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   63
         Top             =   2220
         Width           =   600
      End
      Begin VB.Label lblToggle 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Toggle"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   315
         TabIndex        =   64
         Top             =   2295
         Width           =   600
      End
      Begin VB.Label lblFilterShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   390
         TabIndex        =   62
         Top             =   2700
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Label lblFilterName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   61
         Top             =   2550
         Visible         =   0   'False
         Width           =   585
      End
      Begin VB.Label lblFullSize 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Full"
         Enabled         =   0   'False
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   58
         Top             =   1980
         Width           =   315
      End
      Begin VB.Label lblFullSize 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Full"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   465
         TabIndex        =   59
         Top             =   2025
         Width           =   315
      End
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Left            =   60
      ScaleHeight     =   2865
      ScaleWidth      =   8235
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   540
      Width           =   8295
      Begin VB.Frame fraFilterFileName 
         Caption         =   "Saved Filter List"
         Height          =   675
         Left            =   5880
         TabIndex        =   116
         Top             =   30
         Width           =   2250
         Begin VB.ComboBox cboSavedFilters 
            Height          =   330
            Left            =   90
            TabIndex        =   117
            Text            =   "<Default>"
            Top             =   270
            Width           =   2070
         End
      End
      Begin VB.Frame fraBuFilter 
         Caption         =   "Stored Filters"
         Height          =   1305
         Left            =   3180
         TabIndex        =   107
         Top             =   1410
         Visible         =   0   'False
         Width           =   4455
         Begin VB.TextBox txtFilterName 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Index           =   0
            Left            =   3690
            TabIndex        =   111
            Top             =   270
            Width           =   705
         End
         Begin TABLib.TAB tabBuFilter 
            Height          =   975
            Left            =   60
            TabIndex        =   110
            Top             =   270
            Width           =   3585
            _Version        =   65536
            _ExtentX        =   6324
            _ExtentY        =   1720
            _StockProps     =   64
         End
         Begin VB.CheckBox chkSave 
            Caption         =   "Show"
            Height          =   315
            Index           =   1
            Left            =   3690
            Style           =   1  'Graphical
            TabIndex        =   109
            Tag             =   "AODB_FILTER"
            Top             =   600
            Width           =   705
         End
         Begin VB.CheckBox chkSave 
            Caption         =   "Save"
            Height          =   315
            Index           =   0
            Left            =   3690
            Style           =   1  'Graphical
            TabIndex        =   108
            Tag             =   "AODB_FILTER"
            Top             =   930
            Width           =   705
         End
      End
      Begin VB.Frame fraDayOffset 
         Caption         =   "+/- Days"
         Height          =   1305
         Left            =   4530
         TabIndex        =   82
         Top             =   30
         Width           =   1275
         Begin VB.CheckBox chkFullUnits 
            Caption         =   "Full Units"
            Height          =   210
            Index           =   0
            Left            =   120
            TabIndex        =   95
            Top             =   270
            Width           =   1095
         End
         Begin VB.CommandButton cmdDayOffset 
            Caption         =   ">"
            Height          =   315
            Index           =   3
            Left            =   900
            TabIndex        =   88
            Tag             =   "+1"
            Top             =   900
            Width           =   285
         End
         Begin VB.CommandButton cmdDayOffset 
            Caption         =   "<"
            Height          =   315
            Index           =   2
            Left            =   90
            TabIndex        =   87
            Tag             =   "-1"
            Top             =   900
            Width           =   285
         End
         Begin VB.CommandButton cmdDayOffset 
            Caption         =   ">"
            Height          =   315
            Index           =   1
            Left            =   900
            TabIndex        =   86
            Tag             =   "+1"
            Top             =   570
            Width           =   285
         End
         Begin VB.CommandButton cmdDayOffset 
            Caption         =   "<"
            Height          =   315
            Index           =   0
            Left            =   90
            TabIndex        =   85
            Tag             =   "-1"
            Top             =   570
            Width           =   285
         End
         Begin VB.TextBox txtTimeOffset 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   360
            MaxLength       =   5
            TabIndex        =   84
            Text            =   "0"
            Top             =   570
            Width           =   540
         End
         Begin VB.TextBox txtTimeOffset 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   360
            MaxLength       =   5
            TabIndex        =   83
            Text            =   "0"
            Top             =   900
            Width           =   540
         End
      End
      Begin VB.Frame fraTimeOffset 
         Caption         =   "+/- Hours"
         Height          =   1305
         Left            =   3195
         TabIndex        =   81
         Top             =   30
         Width           =   1275
         Begin VB.CheckBox chkFullUnits 
            Caption         =   "Full Units"
            Height          =   210
            Index           =   1
            Left            =   120
            TabIndex        =   96
            Top             =   270
            Value           =   1  'Checked
            Width           =   1095
         End
         Begin VB.TextBox txtTimeOffset 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   360
            MaxLength       =   5
            TabIndex        =   94
            Text            =   "+8"
            Top             =   900
            Width           =   540
         End
         Begin VB.TextBox txtTimeOffset 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            CausesValidation=   0   'False
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   360
            MaxLength       =   5
            TabIndex        =   93
            Text            =   "-2"
            Top             =   570
            Width           =   540
         End
         Begin VB.CommandButton cmdHourOffset 
            Caption         =   "<"
            Height          =   315
            Index           =   0
            Left            =   90
            TabIndex        =   92
            Tag             =   "-2"
            Top             =   570
            Width           =   285
         End
         Begin VB.CommandButton cmdHourOffset 
            Caption         =   ">"
            Height          =   315
            Index           =   1
            Left            =   900
            TabIndex        =   91
            Tag             =   "+2"
            Top             =   570
            Width           =   285
         End
         Begin VB.CommandButton cmdHourOffset 
            Caption         =   "<"
            Height          =   315
            Index           =   2
            Left            =   90
            TabIndex        =   90
            Tag             =   "-2"
            Top             =   900
            Width           =   285
         End
         Begin VB.CommandButton cmdHourOffset 
            Caption         =   ">"
            Height          =   315
            Index           =   3
            Left            =   900
            TabIndex        =   89
            Tag             =   "+2"
            Top             =   900
            Width           =   285
         End
      End
      Begin VB.Frame fraTimeFilter 
         Caption         =   "Date/Time Filter"
         Height          =   2805
         Left            =   60
         TabIndex        =   73
         Top             =   30
         Width           =   3060
         Begin VB.TextBox txtTimeTo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            Height          =   315
            Index           =   1
            Left            =   1950
            MaxLength       =   2
            TabIndex        =   11
            ToolTipText     =   "Hour (hh)"
            Top             =   2280
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTimeFrom 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            Height          =   315
            Index           =   1
            Left            =   1950
            MaxLength       =   2
            TabIndex        =   5
            ToolTipText     =   "Hour (hh)"
            Top             =   1950
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.Frame fraFixTime 
            BackColor       =   &H000040C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame2"
            Enabled         =   0   'False
            Height          =   675
            Index           =   1
            Left            =   2280
            TabIndex        =   100
            Top             =   1950
            Visible         =   0   'False
            Width           =   345
            Begin VB.TextBox txtDummy 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0FFFF&
               Height          =   315
               Index           =   3
               Left            =   0
               MaxLength       =   5
               TabIndex        =   102
               TabStop         =   0   'False
               Text            =   "59"
               ToolTipText     =   "Time hh:mm"
               Top             =   330
               Width           =   315
            End
            Begin VB.TextBox txtDummy 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0FFFF&
               Height          =   315
               Index           =   2
               Left            =   0
               MaxLength       =   5
               TabIndex        =   101
               TabStop         =   0   'False
               Text            =   "00"
               ToolTipText     =   "Time hh:mm"
               Top             =   0
               Width           =   315
            End
         End
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   90
            TabIndex        =   97
            Top             =   255
            Width           =   2895
            Begin VB.OptionButton optInput 
               Alignment       =   1  'Right Justify
               Caption         =   "Input Fields"
               Height          =   270
               Index           =   2
               Left            =   1590
               TabIndex        =   99
               Top             =   0
               Width           =   1275
            End
            Begin VB.OptionButton optInput 
               Caption         =   " Date/Time"
               Height          =   270
               Index           =   1
               Left            =   0
               TabIndex        =   98
               Top             =   0
               Width           =   1185
            End
         End
         Begin VB.TextBox txtTito 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   765
            MaxLength       =   4
            TabIndex        =   6
            Top             =   1590
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtTito 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   1260
            MaxLength       =   2
            TabIndex        =   7
            Top             =   1590
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTito 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1575
            MaxLength       =   2
            TabIndex        =   8
            Top             =   1590
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTito 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1950
            MaxLength       =   2
            TabIndex        =   9
            Top             =   1590
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTito 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   2265
            MaxLength       =   2
            TabIndex        =   10
            Top             =   1590
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTifr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   4
            Left            =   2265
            MaxLength       =   2
            TabIndex        =   4
            Top             =   1260
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTifr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   3
            Left            =   1950
            MaxLength       =   2
            TabIndex        =   3
            Top             =   1260
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTifr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   1575
            MaxLength       =   2
            TabIndex        =   2
            Top             =   1260
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTifr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   1260
            MaxLength       =   2
            TabIndex        =   1
            Top             =   1260
            Visible         =   0   'False
            Width           =   315
         End
         Begin VB.TextBox txtTifr 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0FFFF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1033
               SubFormatType   =   0
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   765
            MaxLength       =   4
            TabIndex        =   0
            Top             =   1260
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtDateTo 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            Height          =   315
            Left            =   765
            MaxLength       =   10
            TabIndex        =   14
            ToolTipText     =   "Day Date dd.mm.yyyy"
            Top             =   900
            Visible         =   0   'False
            Width           =   1125
         End
         Begin VB.TextBox txtTimeTo 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            Height          =   315
            Index           =   0
            Left            =   1950
            MaxLength       =   5
            TabIndex        =   15
            ToolTipText     =   "Time hh:mm"
            Top             =   900
            Visible         =   0   'False
            Width           =   630
         End
         Begin VB.TextBox txtTimeFrom 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            Height          =   315
            Index           =   0
            Left            =   1950
            MaxLength       =   5
            TabIndex        =   13
            ToolTipText     =   "Time hh:mm"
            Top             =   570
            Visible         =   0   'False
            Width           =   630
         End
         Begin VB.TextBox txtDateFrom 
            Alignment       =   2  'Center
            BackColor       =   &H00C0FFFF&
            Height          =   315
            Left            =   765
            MaxLength       =   10
            TabIndex        =   12
            ToolTipText     =   "Day Date dd.mm.yyyy"
            Top             =   570
            Visible         =   0   'False
            Width           =   1125
         End
         Begin VB.CheckBox chkFromTo 
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   77
            Top             =   900
            Width           =   660
         End
         Begin VB.CheckBox ResetFilter 
            Caption         =   "&From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   76
            ToolTipText     =   "Reset Time Filter"
            Top             =   570
            Width           =   660
         End
         Begin VB.OptionButton optTimeZone 
            BackColor       =   &H0080FF80&
            Caption         =   "U"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   2655
            Style           =   1  'Graphical
            TabIndex        =   75
            ToolTipText     =   "Times in UTC (ATH Local - 180 min.)"
            Top             =   570
            Value           =   -1  'True
            Width           =   315
         End
         Begin VB.OptionButton optTimeZone 
            Caption         =   "L"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   2655
            Style           =   1  'Graphical
            TabIndex        =   74
            ToolTipText     =   "Times in ATH local (UTC + 180 min.)"
            Top             =   900
            Width           =   315
         End
         Begin VB.Frame fraFixTime 
            BackColor       =   &H000040C0&
            BorderStyle     =   0  'None
            Caption         =   "Frame2"
            Enabled         =   0   'False
            Height          =   1365
            Index           =   0
            Left            =   90
            TabIndex        =   78
            Top             =   1260
            Visible         =   0   'False
            Width           =   645
            Begin VB.TextBox txtDummy 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0FFFF&
               BeginProperty DataFormat 
                  Type            =   0
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
               Height          =   315
               Index           =   4
               Left            =   0
               MaxLength       =   2
               TabIndex        =   106
               Text            =   "00"
               Top             =   690
               Visible         =   0   'False
               Width           =   315
            End
            Begin VB.TextBox txtDummy 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0FFFF&
               BeginProperty DataFormat 
                  Type            =   0
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
               Height          =   315
               Index           =   5
               Left            =   315
               MaxLength       =   2
               TabIndex        =   105
               Text            =   "00"
               Top             =   690
               Visible         =   0   'False
               Width           =   315
            End
            Begin VB.TextBox txtDummy 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0FFFF&
               BeginProperty DataFormat 
                  Type            =   0
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
               Height          =   315
               Index           =   7
               Left            =   315
               MaxLength       =   2
               TabIndex        =   104
               Text            =   "59"
               Top             =   1020
               Visible         =   0   'False
               Width           =   315
            End
            Begin VB.TextBox txtDummy 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00C0FFFF&
               BeginProperty DataFormat 
                  Type            =   0
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
               Height          =   315
               Index           =   6
               Left            =   0
               MaxLength       =   2
               TabIndex        =   103
               Text            =   "23"
               Top             =   1020
               Visible         =   0   'False
               Width           =   315
            End
            Begin VB.TextBox txtDummy 
               Alignment       =   2  'Center
               BackColor       =   &H00C0FFFF&
               Height          =   315
               Index           =   0
               Left            =   0
               MaxLength       =   5
               TabIndex        =   80
               TabStop         =   0   'False
               Text            =   "00:00"
               ToolTipText     =   "Time hh:mm"
               Top             =   0
               Visible         =   0   'False
               Width           =   630
            End
            Begin VB.TextBox txtDummy 
               Alignment       =   2  'Center
               BackColor       =   &H00C0FFFF&
               Height          =   315
               Index           =   1
               Left            =   0
               MaxLength       =   5
               TabIndex        =   79
               TabStop         =   0   'False
               Text            =   "23:59"
               ToolTipText     =   "Time hh:mm"
               Top             =   330
               Visible         =   0   'False
               Width           =   630
            End
         End
      End
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2025
      Left            =   60
      ScaleHeight     =   1965
      ScaleWidth      =   2685
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   3510
      Width           =   2745
      Begin VB.PictureBox FilterPanel 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   11.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1845
         Index           =   0
         Left            =   30
         ScaleHeight     =   1785
         ScaleWidth      =   2295
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   30
         Width           =   2355
         Begin VB.PictureBox ButtonPanel 
            AutoRedraw      =   -1  'True
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   11.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   0
            Left            =   0
            ScaleHeight     =   345
            ScaleWidth      =   2115
            TabIndex        =   50
            TabStop         =   0   'False
            Top             =   1350
            Width           =   2175
            Begin VB.CheckBox chkRightFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               Height          =   285
               Index           =   0
               Left            =   1215
               Style           =   1  'Graphical
               TabIndex        =   52
               Top             =   30
               Width           =   765
            End
            Begin VB.CheckBox chkLeftFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               Height          =   285
               Index           =   0
               Left            =   180
               Style           =   1  'Graphical
               TabIndex        =   51
               Top             =   30
               Width           =   765
            End
         End
         Begin VB.TextBox txtFilterVal 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            Height          =   315
            Index           =   0
            Left            =   870
            TabIndex        =   45
            Top             =   15
            Width           =   855
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Filter"
            Height          =   315
            Index           =   0
            Left            =   15
            Style           =   1  'Graphical
            TabIndex        =   44
            ToolTipText     =   "Activates/De-Activates the filter "
            Top             =   15
            Width           =   855
         End
         Begin TABLib.TAB LeftFilterTab 
            Height          =   915
            Index           =   0
            Left            =   15
            TabIndex        =   46
            Top             =   345
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB RightFilterTab 
            Height          =   915
            Index           =   0
            Left            =   870
            TabIndex        =   47
            Top             =   345
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin VB.Frame fraDispoButtonPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   3900
      TabIndex        =   37
      Top             =   5940
      Visible         =   0   'False
      Width           =   7005
      Begin VB.CheckBox Check1 
         Enabled         =   0   'False
         Height          =   285
         Left            =   3870
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   0
         Width           =   855
      End
      Begin TABLib.TAB NatTabCombo 
         Height          =   315
         Index           =   0
         Left            =   0
         TabIndex        =   40
         Top             =   0
         Visible         =   0   'False
         Width           =   3855
         _Version        =   65536
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin VB.CheckBox chkSetDispo 
         Caption         =   "Perform"
         Height          =   285
         Left            =   4740
         Style           =   1  'Graphical
         TabIndex        =   39
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         Height          =   285
         Left            =   5610
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFilterButtonPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   31
      Top             =   5580
      Visible         =   0   'False
      Width           =   5175
      Begin VB.CheckBox chkTool 
         Caption         =   "Adjust"
         Enabled         =   0   'False
         Height          =   285
         Index           =   3
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Close"
         Height          =   285
         Index           =   0
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Refresh"
         Height          =   285
         Index           =   1
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Load"
         Height          =   285
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Compare"
         Height          =   285
         Index           =   4
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFredButtonPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   11.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   21
      Top             =   5910
      Visible         =   0   'False
      Width           =   3585
      Begin VB.CheckBox chkWkDay 
         Caption         =   "All"
         Height          =   285
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   30
         ToolTipText     =   "Select all frequency days"
         Top             =   0
         Width           =   765
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "7"
         Height          =   285
         Index           =   7
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Frequency filter for Sunday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "6"
         Height          =   285
         Index           =   6
         Left            =   2130
         Style           =   1  'Graphical
         TabIndex        =   28
         ToolTipText     =   "Frequency filter for Saturday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "5"
         Height          =   285
         Index           =   5
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Frequency filter for Friday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "4"
         Height          =   285
         Index           =   4
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Frequency filter for Thursday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "3"
         Height          =   285
         Index           =   3
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "Frequency filter for Wednesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "2"
         Height          =   285
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   24
         ToolTipText     =   "Frequency filter for Thuesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "1"
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Frequency filter for Monday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "None"
         Height          =   285
         Index           =   0
         Left            =   2670
         Style           =   1  'Graphical
         TabIndex        =   22
         Tag             =   "......."
         ToolTipText     =   "Unselect all frequency days"
         Top             =   0
         Width           =   795
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   20
      Top             =   5235
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   20417
            MinWidth        =   1764
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FilterDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim InternalResize As Boolean
Dim LayoutChanged As Boolean
Dim MyGlobalResult As String
Dim MyFilterIniSection As String
Dim MyCurParent As Form
Dim ButtonPushedByUser As Boolean
Dim LoadingConfig As Boolean
Dim DontJump As Boolean
Dim DontCheck As Boolean
Dim DontEvaluate As Boolean
Dim ButtonPushedInternal As Boolean
Dim RestoringFilter As Boolean
Dim FilterCfgBgn As String
Dim FilterCfgEnd As String
Dim FilterCfgChk As String
Public sGRPNValue As String
Dim sCaller As String 'Define Caller to save filter detail at db

Public Function InitMyLayout(MyParent As Form, XOffset As Long, YOffset As Long, ShowModal As Boolean, MyLayoutStyle As String, CallFrom As String) As String
    Dim LayoutStyle As String
    Dim LayoutCmd As String
    Dim MyTop As Long
    Dim MyLeft As Long
    Set MyCurParent = MyParent
    LayoutStyle = GetItem(MyLayoutStyle, 1, ",")
    LayoutCmd = GetItem(MyLayoutStyle, 2, ",")
    If LayoutStyle <> Me.Tag Then LayoutChanged = True Else LayoutChanged = False
    MyFilterIniSection = GetIniEntry(myIniFullName, myIniSection, "", "AODB_FILTER_SECTION", myIniSection)
    BindFilterList 'Load Saved Filter Detail
    Me.Tag = ""
    InitTopPanel LayoutStyle
    InitWorkArea LayoutStyle
    InitFtypFilterPanel LayoutStyle
    InitFilterPeriod LayoutStyle
    Me.Tag = LayoutStyle
    MyTop = MyCurParent.Top + ((MyCurParent.ScaleHeight - Me.Height) / 2)
    MyTop = MyTop + YOffset
    MyLeft = MyCurParent.Left + ((MyCurParent.Width - Me.Width) / 2)
    MyLeft = MyLeft + XOffset
    If MyTop > Screen.Height - Me.Height Then MyTop = Screen.Height - Me.Height
    If MyTop < 0 Then MyTop = 0
    If MyLeft > MaxScreenWidth - Me.Width Then MyLeft = MaxScreenWidth - Me.Width
    If MyLeft < 0 Then MyLeft = 0
    'QuickHack
    If MainDialog.WindowState = vbMinimized Then MyTop = Screen.Height + 600
    If MyTop < MinScreenTopPos Then MyTop = MinScreenTopPos
    'If ApplshortCode = "CAP" Then MyTop = Screen.Height + 600
    Me.Top = MyTop
    MyLeft = MainDialog.Left + MainDialog.WorkArea.Left + ((MainDialog.WorkArea.Width - Me.Width) / 2) + 60
    Me.Left = MyLeft
    Me.Caption = GetFilterConfig(LayoutStyle, "FILTER_CAPTION", -1, "")
    Select Case FilterActionType
        Case 1
            ResetFilter.Value = 1
        Case Else
    End Select
    FilterActionType = 0
    If RestoreFilter Then LayoutCmd = "RESTORE"
    AutoStartTimer.Tag = LayoutCmd
    Select Case LayoutCmd
        Case "LOAD"
            If ShowModal Then
                If AutoAodbFilterLoad Then AutoStartTimer.Enabled = True
                Me.Show vbModal, MyParent
            Else
                Me.Show , MyParent
            End If
        Case "BACKUP"
        Case "RESTORE"
            If ShowModal Then
                'If AutoAodbFilterload Then
                    AutoStartTimer.Enabled = True
                'End If
                Me.Show vbModal, MyParent
            Else
                Me.Show , MyParent
            End If
        Case Else
    End Select
'    If sFilterName <> "" Then
'        cboSavedFilters.SelText = sFilterName
'    End If
    FilterAutoReset = True
    InitMyLayout = MyGlobalResult
End Function

Private Sub InitTopPanel(LayoutType As String)
    Dim i As Integer
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewSize As Long
    Dim tmpData As String
    Dim tmpNow
    If LayoutChanged Then
        NewTop = 0
        If ApplIsFdsMain Then
            ServerPanel.Left = 0
            ServerPanel.Top = NewTop
            ServerPanel.Visible = True
            NewTop = NewTop + ServerPanel.Height
        End If
        TopPanel.Top = NewTop
        TopPanel.Left = 0
        For i = 0 To txtTifr.UBound
            txtTifr(i).Top = ResetFilter.Top
            txtTito(i).Top = chkFromTo.Top
        Next
        fraFixTime(0).Left = txtTimeFrom(0).Left
        fraFixTime(0).Top = ResetFilter.Top
        fraFixTime(1).Left = txtTifr(4).Left
        fraFixTime(1).Top = ResetFilter.Top
        fraFixTime(0).Height = fraFixTime(1).Height
        txtTimeFrom(1).Top = txtTimeFrom(0).Top
        txtTimeFrom(1).Left = txtTimeFrom(0).Left
        txtTimeTo(1).Top = txtTimeTo(0).Top
        txtTimeTo(1).Left = txtTimeTo(0).Left
        txtDummy(4).Top = txtDummy(0).Top
        txtDummy(5).Top = txtDummy(0).Top
        txtDummy(6).Top = txtDummy(1).Top
        txtDummy(7).Top = txtDummy(1).Top
        fraTimeFilter.Height = fraDayOffset.Height
        TopPanel.Height = fraTimeFilter.Height + 120
        fraBuFilter.Top = fraTimeOffset.Top
        fraBuFilter.Left = fraTimeOffset.Left + fraTimeOffset.Width + 120
        fraBuFilter.Height = fraTimeOffset.Height
        InitBuFilterTab
        tmpData = "digital"
        Select Case UCase(tmpData)
            'Case "DATE"
            '    optInput(0).Value = True
            Case "DATE/TIME"
                optInput(1).Value = True
            Case "DIGITAL"
                optInput(2).Value = True
            Case Else
                optInput(2).Value = True
        End Select
        Select Case CurrentTimeCode
            Case "UTC"
                optTimeZone(0).Value = True
                optTimeZone(0).BackColor = LightGreen
                optTimeZone(1).BackColor = vbButtonFace
            Case "LOC"
                optTimeZone(1).Value = True
                optTimeZone(1).BackColor = LightGreen
                optTimeZone(0).BackColor = vbButtonFace
            Case Else
                optTimeZone(1).Value = True
                optTimeZone(1).BackColor = LightGreen
                optTimeZone(0).BackColor = vbButtonFace
        End Select
        ResetFilter.Value = 1
    End If
End Sub
Private Sub InitBuFilterTab()
    tabBuFilter.ResetContent
    tabBuFilter.HeaderString = "BU ,Filter Name      ,Remark                                  "
    tabBuFilter.HeaderLengthString = "10,10,10"
    tabBuFilter.HeaderAlignmentString = "L,L,L"
    tabBuFilter.LifeStyle = True
    tabBuFilter.FontName = "Arial"
    tabBuFilter.FontSize = 18
    tabBuFilter.LineHeight = 18
    tabBuFilter.AutoSizeByHeader = True
    tabBuFilter.AutoSizeColumns
End Sub
Private Sub InitWorkArea(LayoutStyle As String)
    Dim UsedPanels As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim TabLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim TabCfgLine As String
    Dim ItemNo As Long
    Dim NewSection As Boolean
    Dim FilterCount As Integer
    Dim CurFilter As Integer
    Dim PanelLayout As String
    Dim tmpHeader As String
    Dim tmpActive As String
    Dim tmpToggle As String
    
    UsedPanels = FilterPanel.UBound
    For UsedPanels = 0 To FilterPanel.UBound
        FilterPanel(UsedPanels).Visible = False
        chkFilterIsVisible(UsedPanels).Visible = False
        lblFilterName(UsedPanels).Visible = False
        lblFilterShadow(UsedPanels).Visible = False
        'RightFilterTab(UsedPanels).myTag = ""
        HiddenData.UpdateTabConfig LeftFilterTab(UsedPanels).myName, "STAT", "H"
    Next
    NewTop = TopPanel.Top + TopPanel.Height
    WorkArea.Left = 0
    WorkArea.Top = NewTop
    FilterCount = Val(GetFilterConfig(LayoutStyle, "FILTER_PANELS", -1, ""))
    
    NewLeft = 120
    NewTop = chkToggle.Top + chkToggle.Height + 90
    UsedPanels = -1
    ItemNo = 0
    For CurFilter = 1 To FilterCount
        PanelLayout = GetFilterConfig(LayoutStyle, "PANEL_LAYOUT", CurFilter, "")
        UsedPanels = UsedPanels + 1
        NewSection = False
        If UsedPanels > FilterPanel.UBound Then
            Load FilterPanel(UsedPanels)
            Load txtFilterVal(UsedPanels)
            Load chkWork(UsedPanels)
            Load LeftFilterTab(UsedPanels)
            Load RightFilterTab(UsedPanels)
            Load ButtonPanel(UsedPanels)
            Load chkFilterIsVisible(UsedPanels)
            Load lblFilterName(UsedPanels)
            Load lblFilterShadow(UsedPanels)
            Set txtFilterVal(UsedPanels).Container = FilterPanel(UsedPanels)
            Set chkWork(UsedPanels).Container = FilterPanel(UsedPanels)
            Set LeftFilterTab(UsedPanels).Container = FilterPanel(UsedPanels)
            Set RightFilterTab(UsedPanels).Container = FilterPanel(UsedPanels)
            Set ButtonPanel(UsedPanels).Container = FilterPanel(UsedPanels)
            Load chkLeftFilterSet(UsedPanels)
            Load chkRightFilterSet(UsedPanels)
            Set chkLeftFilterSet(UsedPanels).Container = ButtonPanel(UsedPanels)
            Set chkRightFilterSet(UsedPanels).Container = ButtonPanel(UsedPanels)
            NewSection = True
        End If
        
        FilterPanel(UsedPanels).Top = 120
        FilterPanel(UsedPanels).Left = NewLeft
        FilterPanel(UsedPanels).Visible = True
        txtFilterVal(UsedPanels).Visible = True
        If (LayoutChanged) Or (NewSection) Then
            LeftFilterTab(UsedPanels).ResetContent
            tmpHeader = GetRealItem(PanelLayout, 4, "|")
            tmpHeader = BuildDummyHeader(tmpHeader) & ",-----"
            LeftFilterTab(UsedPanels).FontName = "Courier New"
            LeftFilterTab(UsedPanels).SetMainHeaderFont MainDialog.FontSlider.Value + 6, False, False, False, 0, "Courier New"
            LeftFilterTab(UsedPanels).HeaderString = tmpHeader
            LeftFilterTab(UsedPanels).FontSize = MainDialog.FontSlider.Value + 6
            LeftFilterTab(UsedPanels).HeaderFontSize = MainDialog.FontSlider.Value + 6
            LeftFilterTab(UsedPanels).LineHeight = MainDialog.FontSlider.Value + 6
            LeftFilterTab(UsedPanels).HeaderLengthString = "10,10,10"
            If (LayoutChanged) Or (NewSection) Then LeftFilterTab(UsedPanels).LogicalFieldList = GetRealItem(PanelLayout, 3, "|")
            LeftFilterTab(UsedPanels).SelectBackColor = DarkestYellow
            LeftFilterTab(UsedPanels).SelectTextColor = vbWhite
            LeftFilterTab(UsedPanels).EmptyAreaBackColor = LightGray
            LeftFilterTab(UsedPanels).EmptyAreaRightColor = LightGray
            LeftFilterTab(UsedPanels).MainHeaderOnly = True
            LeftFilterTab(UsedPanels).LeftTextOffset = 1
            LeftFilterTab(UsedPanels).SetTabFontBold True
            LeftFilterTab(UsedPanels).AutoSizeByHeader = True
            LeftFilterTab(UsedPanels).AutoSizeColumns
            WidthList = LeftFilterTab(UsedPanels).HeaderLengthString
            TabWidth = (Val(GetRealItem(WidthList, 0, ",")) * 15) + (Val(GetRealItem(WidthList, 1, ",")) * 15)
            'If GetRealItem(PanelLayout, 2, "|") = "Y" Then TabWidth = TabWidth + Val(GetRealItem(WidthList, 1, ",")) * 15
            TabWidth = TabWidth + 16 * 15 'ScrollBar
            If TabWidth < 855 Then TabWidth = 855
            LeftFilterTab(UsedPanels).Width = TabWidth
        End If
        LeftFilterTab(UsedPanels).Visible = True
        
        If (LayoutChanged) Or (NewSection) Then
            RightFilterTab(UsedPanels).ResetContent
            RightFilterTab(UsedPanels).Width = TabWidth
            RightFilterTab(UsedPanels).HeaderString = tmpHeader
            RightFilterTab(UsedPanels).HeaderLengthString = "10,10,10"
            RightFilterTab(UsedPanels).FontName = "Courier New"
            RightFilterTab(UsedPanels).SetMainHeaderFont MainDialog.FontSlider.Value + 6, False, False, False, 0, "Courier New"
            RightFilterTab(UsedPanels).FontSize = MainDialog.FontSlider.Value + 6
            RightFilterTab(UsedPanels).HeaderFontSize = MainDialog.FontSlider.Value + 6
            RightFilterTab(UsedPanels).LineHeight = MainDialog.FontSlider.Value + 6
            If (LayoutChanged) Or (NewSection) Then RightFilterTab(UsedPanels).LogicalFieldList = GetRealItem(PanelLayout, 3, "|")
            RightFilterTab(UsedPanels).Left = LeftFilterTab(UsedPanels).Left + TabWidth + 15
            RightFilterTab(UsedPanels).SelectBackColor = DarkBlue
            RightFilterTab(UsedPanels).SelectTextColor = vbWhite
            RightFilterTab(UsedPanels).EmptyAreaBackColor = LightGray
            RightFilterTab(UsedPanels).EmptyAreaRightColor = LightGray
            RightFilterTab(UsedPanels).MainHeaderOnly = True
            RightFilterTab(UsedPanels).LeftTextOffset = 1
            RightFilterTab(UsedPanels).SetTabFontBold True
            RightFilterTab(UsedPanels).AutoSizeByHeader = True
            RightFilterTab(UsedPanels).AutoSizeColumns
        End If
        RightFilterTab(UsedPanels).Visible = True
        
        If (LayoutChanged) Or (NewSection) Then
            chkWork(UsedPanels).Width = TabWidth
            txtFilterVal(UsedPanels).Width = TabWidth
            txtFilterVal(UsedPanels).Left = RightFilterTab(UsedPanels).Left
            FilterPanel(UsedPanels).Width = RightFilterTab(UsedPanels).Left + RightFilterTab(UsedPanels).Width + 60
            
            chkLeftFilterSet(UsedPanels).Width = TabWidth - 90
            chkRightFilterSet(UsedPanels).Width = TabWidth - 90
            chkLeftFilterSet(UsedPanels).Left = LeftFilterTab(UsedPanels).Left + ((TabWidth - chkLeftFilterSet(UsedPanels).Width) / 2)
            chkRightFilterSet(UsedPanels).Left = RightFilterTab(UsedPanels).Left + ((TabWidth - chkRightFilterSet(UsedPanels).Width) / 2) - 15
            chkLeftFilterSet(UsedPanels).Visible = True
            chkRightFilterSet(UsedPanels).Visible = True
            ButtonPanel(UsedPanels).Left = -30
            ButtonPanel(UsedPanels).Width = FilterPanel(UsedPanels).Width + 30
            DrawBackGround ButtonPanel(UsedPanels), 7, True, True
        End If
        ButtonPanel(UsedPanels).Visible = True
        
        tmpActive = GetRealItem(PanelLayout, 1, "|")
        chkWork(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        If (LayoutChanged) Or (NewSection) Then
            chkWork(UsedPanels).Value = 0
            chkWork(UsedPanels).Value = 1
            If GetRealItem(tmpActive, 1, ",") <> "Y" Then chkWork(UsedPanels).Value = 0
            chkWork(UsedPanels).Tag = ""
        End If
        chkWork(UsedPanels).Visible = True
        
        LeftFilterTab(UsedPanels).myName = "FILTER_LEFT_" & CStr(UsedPanels)
        DefineDataConfig LeftFilterTab(UsedPanels), LayoutStyle, PanelLayout
        If (LayoutChanged) Or (NewSection) Then RightFilterTab(UsedPanels).myTag = GetRealItem(RightFilterTab(UsedPanels).LogicalFieldList, 0, ",")
        
        chkFilterIsVisible(UsedPanels).Top = NewTop
        lblFilterName(UsedPanels).Top = NewTop + 15
        lblFilterShadow(UsedPanels).Top = lblFilterName(UsedPanels).Top - 15
        lblFilterName(UsedPanels).Left = chkFilterIsVisible(UsedPanels).Left + chkFilterIsVisible(UsedPanels).Width + 45
        lblFilterShadow(UsedPanels).Left = lblFilterName(UsedPanels).Left - 15
        lblFilterName(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        lblFilterShadow(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        lblFilterName(UsedPanels).ZOrder
        chkFilterIsVisible(UsedPanels).Value = 1
        If GetRealItem(tmpActive, 0, ",") <> "Y" Then chkFilterIsVisible(UsedPanels).Value = 0
        If RightFilterTab(UsedPanels).GetLineCount > 0 Then chkFilterIsVisible(UsedPanels).Value = 1
        chkFilterIsVisible(UsedPanels).Visible = True
        lblFilterName(UsedPanels).Visible = True
        lblFilterShadow(UsedPanels).Visible = True
        If (LayoutChanged) Or (NewSection) Then
            If GetRealItem(tmpActive, 2, ",") = "T" Then
                HandleToggleFilter chkFilterIsVisible(UsedPanels), UsedPanels
            End If
        End If
        
        NewTop = NewTop + chkFilterIsVisible(UsedPanels).Height + 15
        If chkFilterIsVisible(UsedPanels).Value = 1 Then
            NewLeft = FilterPanel(UsedPanels).Left + FilterPanel(UsedPanels).Width + 120
        End If
        ItemNo = ItemNo + 1
    Next
    RightPanel.Height = Screen.Height
    DrawBackGround RightPanel, 7, True, True
    Me.Width = NewLeft + RightPanel.Width + 180
End Sub
Private Sub InitFtypFilterPanel(LayoutStyle As String)
    Dim Index As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim NewSection As Boolean
    Dim FilterCount As Integer
    Dim CurFilter As Integer
    Dim FtypLayout As String
    Dim tmpItem As String
    
    LoadingConfig = True
    For Index = 0 To chkFtypFilter.UBound
        chkFtypFilter(Index).Visible = False
        lblFtypName(Index).Visible = False
        lblFtypShadow(Index).Visible = False
        If LayoutChanged Then chkFtypFilter(Index).Value = 0
    Next
    FilterCount = Val(GetFilterConfig(LayoutStyle, "FILTER_FTYPES", -1, ""))
    If FilterCount > 0 Then
        'NewTop = chkAppl(2).Top + chkAppl(2).Height + 15
        NewTop = chkFtypAll(0).Top
        NewLeft = 45
        chkFtypAll(0).Top = NewTop
        chkFtypAll(0).BackColor = LightGray
        lblFtypAll(0).Top = NewTop + 15
        lblFtypAll(0).Left = chkFtypAll(0).Left + chkFtypAll(0).Width + 60
        lblFtypAllShadow(0).Top = NewTop + 30
        lblFtypAllShadow(0).Left = lblFtypAll(0).Left + 15
        
        'NewTop = chkFullSize.Top
        NewTop = NewTop + chkFtypAll(0).Height + 60
        Index = -1
        For CurFilter = 1 To FilterCount
            FtypLayout = GetFilterConfig(LayoutStyle, "FTYP_LAYOUT", CurFilter, "")
            Index = Index + 1
            NewSection = False
            If Index > chkFtypFilter.UBound Then
                Load chkFtypFilter(Index)
                Load lblFtypName(Index)
                Load lblFtypShadow(Index)
                Set chkFtypFilter(Index).Container = FtypPanel
                Set lblFtypName(Index).Container = FtypPanel
                Set lblFtypShadow(Index).Container = FtypPanel
                NewSection = True
            End If
            chkFtypFilter(Index).Top = NewTop
            chkFtypFilter(Index).Left = NewLeft
            lblFtypName(Index).Top = NewTop + 15
            lblFtypName(Index).Left = chkFtypFilter(Index).Left + chkFtypFilter(Index).Width + 60
            lblFtypShadow(Index).Top = NewTop + 30
            lblFtypShadow(Index).Left = lblFtypName(Index).Left + 15
            chkFtypFilter(Index).Caption = GetItem(FtypLayout, 1, ",")
            lblFtypName(Index).Caption = GetItem(FtypLayout, 2, ",")
            lblFtypShadow(Index).Caption = lblFtypName(Index).Caption
            lblFtypName(Index).ZOrder
            If (LayoutChanged) Or (NewSection) Then
                tmpItem = GetItem(FtypLayout, 3, ",")
                If tmpItem = "Y" Then chkFtypFilter(Index).Value = 1 Else chkFtypFilter(Index).Value = 0
            End If
            tmpItem = GetItem(FtypLayout, 4, ",")
            If tmpItem = "Y" Then chkFtypFilter(Index).Enabled = True Else chkFtypFilter(Index).Enabled = False
            lblFtypName(Index).ToolTipText = GetItem(FtypLayout, 5, ",")
            chkFtypFilter(Index).Visible = True
            lblFtypName(Index).Visible = True
            lblFtypShadow(Index).Visible = True
            NewTop = NewTop + chkFtypFilter(Index).Height + 15
        Next
        FtypPanel.Height = Screen.Height
        DrawBackGround FtypPanel, 7, False, True
        Me.Width = Me.Width + FtypPanel.Width
    End If
    LoadingConfig = False
End Sub
Private Sub InitFilterPeriod(LayoutStyle As String)
    Dim tmpCfg As String
    Dim tmpBgn As String
    Dim tmpEnd As String
    Dim tmpChk As String
    tmpCfg = GetFilterConfig(LayoutStyle, "FILTER_PERIOD", -1, "")
    If tmpCfg <> "" Then
        tmpBgn = GetItem(tmpCfg, 1, ",")
        tmpEnd = GetItem(tmpCfg, 2, ",")
        tmpChk = GetItem(tmpCfg, 3, ",")
        txtTimeOffset(0).Text = tmpBgn
        txtTimeOffset(1).Text = tmpEnd
        If tmpChk = "" Then tmpChk = "Y"
        If tmpChk = "Y" Then
            chkFullUnits(1).Value = 1
        Else
            chkFullUnits(1).Value = 0
        End If
        FilterCfgBgn = tmpBgn
        FilterCfgEnd = tmpEnd
        FilterCfgChk = tmpChk
    End If
    tmpCfg = GetFilterConfig(LayoutStyle, "FILTER_RELOAD", -1, CStr(FilterAutoReload))
    If tmpCfg <> "" Then
        FilterAutoReload = Val(tmpCfg)
    End If
End Sub

Private Function BuildDummyHeader(SizeList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim ItemVal As String
    Dim ColWidth As Long
    ItemNo = 0
    ItemVal = GetRealItem(SizeList, ItemNo, ",")
    While ItemVal <> ""
        ColWidth = Val(ItemVal)
        'QuickHack
        If ColWidth < 1 Then ColWidth = 1
        Result = Result & String(ColWidth, "-") & ","
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(SizeList, ItemNo, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    BuildDummyHeader = Result
End Function
Private Sub DefineDataConfig(CurTab As TABLib.Tab, LayoutStyle As String, PanelLayout As String)
    Dim TabCfgLine As String
    Dim TabCfgFields As String
    Dim TabCfgData As String
    Dim HiddenTab As String
    Dim tmpFldLst As String
    Dim tmpSqlKey As String
    Dim ItemNo As Long
    Dim ItemVal As String
    TabCfgLine = CurTab.myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "SEL" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & CurTab.LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    HiddenData.CheckTabConfig TabCfgLine
    HiddenTab = HiddenData.CreateFilterTab(CurTab.Index)
    HiddenData.UpdateTabConfig CurTab.myName, "DSRC", HiddenTab
    
    TabCfgLine = GetFilterConfig(LayoutStyle, "READAODB," & CStr(CurTab.Index), -1, "")
    TabCfgFields = "DTAB,LFLD,DFLD,DSQL"
    TabCfgData = ""
    TabCfgData = TabCfgData & GetRealItem(PanelLayout, 5, "|") & Chr(15)
    TabCfgData = TabCfgData & GetRealItem(PanelLayout, 3, "|") & Chr(15)
    ItemNo = 6
    ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
    While ItemVal <> ""
        tmpFldLst = tmpFldLst & ItemVal & Chr(14)
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
        tmpSqlKey = tmpSqlKey & ItemVal & Chr(14)
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
    Wend
    If tmpFldLst <> "" Then tmpFldLst = Left(tmpFldLst, Len(tmpFldLst) - 1)
    If tmpSqlKey <> "" Then tmpSqlKey = Left(tmpSqlKey, Len(tmpSqlKey) - 1)
    TabCfgData = TabCfgData & tmpFldLst & Chr(15) & tmpSqlKey
    
    HiddenData.UpdateTabConfig HiddenTab, TabCfgFields, TabCfgData
End Sub
Private Function GetFilterConfig(LayoutType As String, ForWhat As String, Index As Integer, UseDefault As String) As String
    Dim Result As String
    Dim TypeCode As String
    Dim WhatCode As String
    Dim UseIniKey As String
    Dim tmpDflt As String
    Result = ""
    TypeCode = GetRealItem(LayoutType, 0, ",")
    WhatCode = GetRealItem(ForWhat, 0, ",")
    Select Case TypeCode
        Case "AODB_FILTER"
            Select Case WhatCode
                Case "FILTER_PANELS"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_PANELS", "0")
                Case "FILTER_CAPTION"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_CAPTION", "AODB Flight Data Filter")
                Case "PANEL_LAYOUT"
                    UseIniKey = "AODB_FILTER_PANEL_" & CStr(Index)
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "??|??,??")
                Case "FILTER_FTYPES"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_FTYPES", "0")
                Case "FTYP_LAYOUT"
                    UseIniKey = "AODB_FILTER_FTYP_" & CStr(Index)
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "?,???,???")
                Case "FILTER_PERIOD"
                    UseIniKey = "AODB_FILTER_PERIOD"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "-2,+8")
                Case "FILTER_RELOAD"
                    UseIniKey = "AODB_FILTER_RELOAD"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, UseDefault)
                Case Else
            End Select
        Case Else
    End Select
    GetFilterConfig = Result
End Function

Private Sub AutoStartTimer_Timer()
    Dim tmpTag As String
    Dim bTmp As Boolean
    AutoStartTimer.Enabled = False
    tmpTag = AutoStartTimer.Tag
    Select Case tmpTag
        Case "LOAD"
            'Write the last saved filter to a file
            If WriteLastFilteredTextFile = True Then
                'Restore to Filter List
                RestoreFilterDialog
                
                'Load last save criteria to main form
                chkAppl(5).Value = 1
            Else
                chkAppl(5).Value = 1
            End If
        Case "BACKUP"
        Case "RESTORE"
            RestoreFilterDialog
        Case Else
            chkAppl(1).Value = 1
    End Select
End Sub
'Get User Saved filter detail and write to text file
Private Sub GetSavedFilter()
Dim sResult As String
    Dim sTableName As String
    Dim sFieldName As String
    Dim sCondition As String
    Dim sOrder As String
    Dim iReturnValue As Integer
    Dim NoOfLines As Integer
    
    Dim MainFile As String
    Dim myLoadPath As String
    Dim myLoadName As String
    Dim tmpFileName As String
    Dim DatFileSize As Long
    Dim DatFileData As String
    Dim DatFileName As String
    Dim tmpFile As String
    Dim KPass As String
    Dim fn As Integer
    Dim ffn As Integer
    Dim i As Integer
    
    sTableName = "VCDTAB"
    sFieldName = "TEXT"
    sCondition = "WHERE CKEY = '" & Trim(cboSavedFilters.Text) & "' AND URNO = " & cboSavedFilters.ItemData(cboSavedFilters.ListIndex)
    sOrder = "CTYP"
'    sFilterName = Trim(cboSavedFilters.Text)
    iReturnValue = UfisServer.CallCeda(sResult, "RT", sTableName, sFieldName, "", sCondition, sOrder, 0, True, False)
    
    Dim arrRecords As Variant
    arrRecords = Split(sResult, vbNewLine)
    NoOfLines = UBound(arrRecords) + 1
    
    If iReturnValue = 0 Then
        'Temp File Path
        myLoadPath = UFIS_TMP
        'Temp File Path + Name
        myLoadName = ApplMainCode & "_" & ApplFuncCode & "_Filter"
        tmpFileName = myLoadPath & "\" & myLoadName
        MainFile = myLoadPath & "\" & myLoadName & "_All.usf"
        FilterFileName = MainFile
        RestoreFilter = False
        tmpFile = Dir(FilterFileName)
        If tmpFile <> "" Then Kill FilterFileName
        ffn = FreeFile
        
        Open MainFile For Binary As #ffn
        
        DatFileData = ""
        
        For i = 0 To NoOfLines - 1
            'Change the server string value to client string
            DatFileData = CleanString(CStr(arrRecords(i)), FOR_CLIENT, False)
            Put #ffn, , DatFileData
        Next i
        Close #ffn
        
    End If
    
    'Shown filter at the filter dialog form
    RestoreFilterDialog
    'Load Data
    HandleAodbFilter chkAppl(5), True, ""
End Sub
'// Retrieve Last filtered Text File
'Phyoe (22-Nov-2011)
Private Function WriteLastFilteredTextFile() As Boolean
    Dim sResult As String
    Dim sTableName As String
    Dim sFieldName As String
    Dim sCondition As String
    Dim sOrder As String
    Dim iReturnValue As Integer
    Dim NoOfLines As Integer
    
    Dim MainFile As String
    Dim myLoadPath As String
    Dim myLoadName As String
    Dim tmpFileName As String
    Dim DatFileSize As Long
    Dim DatFileData As String
    Dim DatFileName As String
    Dim tmpFile As String
    Dim KPass As String
    Dim fn As Integer
    Dim ffn As Integer
    Dim i As Integer
    
    sTableName = "VCDTAB"
    sFieldName = "TEXT, CKEY"
    sCondition = "WHERE PKNO = '" & LoginUserName & "' AND APPN = 'UFIS-FDT' AND VAFR = (SELECT MAX(VAFR) FROM VCDTAB WHERE PKNO = '" & LoginUserName & "' AND APPN = 'UFIS-FDT')"
    sOrder = "CTYP"
        
    iReturnValue = UfisServer.CallCeda(sResult, "RT", sTableName, sFieldName, "", sCondition, sOrder, 0, True, False)
    
    Dim arrRecords As Variant
    arrRecords = Split(sResult, vbNewLine)
    NoOfLines = UBound(arrRecords) + 1
    
    If iReturnValue = 0 Then
        'Temp File Path
        myLoadPath = UFIS_TMP
        'Temp File Path + Name
        myLoadName = ApplMainCode & "_" & ApplFuncCode & "_Filter"
        tmpFileName = myLoadPath & "\" & myLoadName
        MainFile = myLoadPath & "\" & myLoadName & "_All.usf"
        FilterFileName = MainFile
        RestoreFilter = False
        tmpFile = Dir(FilterFileName)
        If tmpFile <> "" Then Kill FilterFileName
        ffn = FreeFile
        Open MainFile For Binary As #ffn
        DatFileData = ""
        'Write to file
        For i = 0 To NoOfLines - 1
            'GetItem(CStr(arrRecords(i)), 1, ",")
            DatFileData = CleanString(GetItem(CStr(arrRecords(i)), 1, ","), FOR_CLIENT, False)
            Put #ffn, , DatFileData
        Next i
        Close #ffn
        sFilterName = GetItem(CStr(arrRecords(i - 1)), 2, ",")
        WriteLastFilteredTextFile = True
    Else
        WriteLastFilteredTextFile = False
    End If
    
End Function

Private Sub cboSavedFilters_Click()
    Dim bReturnValue As Boolean
    If cboSavedFilters.ListIndex > 0 Then
        bReturnValue = GetFilteredTextFileByFileName(cboSavedFilters.Text, cboSavedFilters.ItemData(cboSavedFilters.ListIndex))
        If bReturnValue = True Then
            'Restore to Filter List
            RestoreFilterDialog
            'Load last save criteria to main form
            'chkAppl(5).Value = 1
            sFilterName = Trim(cboSavedFilters.Text)
            iFilterIdex = cboSavedFilters.ListIndex
        End If
    End If
'    If cboSavedFilters.ListIndex = 0 And sCaller = "S" Then
'        sFilterName = cboSavedFilters.Text
'        HandleResetFilter chkAppl(3)
'    End If
    
End Sub

Private Sub chkAppl_Click(Index As Integer)
On Error GoTo ErrHandler
    If chkAppl(Index).Value = 1 Then
        Screen.MousePointer = vbHourglass
        chkAppl(Index).BackColor = LightGreen
        chkAppl(Index).Refresh
        Select Case chkAppl(Index).Tag
            Case "CLOSE"
                HandleClose chkAppl(Index)
            Case "AODB_FILTER"
                
                HandleAodbFilter chkAppl(Index), True, "S"
                
            Case "AODB_FILTER_LOAD"
                If cboSavedFilters.ListIndex = 0 Then
'                    sFilterName = ""
                    'call sub
                    HandleAodbFilter chkAppl(Index), True, ""
                    
                ElseIf cboSavedFilters.ListIndex > 0 Then
                    'Call sub
                    'GetSavedFilter
                    'sFilterName = cboSavedFilters.Text
                    HandleAodbFilter chkAppl(Index), True, ""
'                ElseIf cboSavedFilters.ListIndex = -1 Then
'                    HandleAodbFilter chkAppl(Index), True, ""
                End If
            Case "AODBREFRESH"
                HandleAodbRefresh chkAppl(Index)
            Case "RESETFILTER"
                HandleResetFilter chkAppl(Index)
'                cboSavedFilters.ListIndex = 0
'                sFilterName = ""
            Case Else
                chkAppl(Index).Value = 0
        End Select
        Screen.MousePointer = vbDefault
    Else
        chkAppl(Index).BackColor = MyOwnButtonFace
        Screen.MousePointer = vbDefault
    End If
    Screen.MousePointer = vbDefault
    Exit Sub
ErrHandler:
    Screen.MousePointer = vbDefault
    MsgBox Err.Description & ";" & Err.Number, vbExclamation
    Resume Next
End Sub

Private Sub HandleResetFilter(CurButton As CheckBox)
    Dim LCnt As Long
    Dim i As Integer
    LCnt = 0
    For i = 0 To chkFilterIsVisible.UBound
        If chkFilterIsVisible(i).Visible Then LCnt = LCnt + RightFilterTab(i).GetLineCount
    Next
    If LCnt > 0 Then
        For i = 0 To chkFilterIsVisible.UBound
            If chkFilterIsVisible(i).Visible Then
                chkRightFilterSet(i).Value = 1
                txtFilterVal(i).Text = ""
                'chkWork(i).Value = 0
            End If
        Next
    Else
        For i = 0 To chkFilterIsVisible.UBound
            If chkFilterIsVisible(i).Visible Then
                LeftFilterTab(i).ResetContent
                chkLeftFilterSet(i).Caption = "0"
                chkRightFilterSet(i).Caption = "0"
                txtFilterVal(i).Text = ""
                LeftFilterTab(i).Refresh
                chkWork(i).Value = 0
                chkWork(i).Tag = ""
                
            End If
        Next
        'cboSavedFilters.ListIndex = 0
        'BindFilterList
    End If
    
    CurButton.Value = 0
End Sub
Private Sub HandleToggleFilter(CurButton As CheckBox, Index As Integer)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If (Index < 0) Or (Index = i) Then
            'If chkFilterIsVisible(i).Visible Then
            ToggleTabColumns LeftFilterTab(i)
            ToggleTabColumns RightFilterTab(i)
            'End If
        End If
    Next
End Sub
Private Sub ToggleTabColumns(CurTab As TABLib.Tab)
    Dim tmpLogFields As String
    Dim tmpNewFields As String
    Dim tmpBuff As String
    Dim LineNo As Long
    tmpLogFields = CurTab.LogicalFieldList
    tmpNewFields = GetRealItem(tmpLogFields, 1, ",") & "," & GetRealItem(tmpLogFields, 0, ",")
    LineNo = CurTab.GetLineCount - 1
    If LineNo >= 0 Then
        tmpBuff = CurTab.GetBufferByFieldList(0, LineNo, tmpNewFields, vbLf)
        CurTab.ResetContent
        CurTab.InsertBuffer tmpBuff, vbLf
    End If
    CurTab.LogicalFieldList = tmpNewFields
    tmpLogFields = CurTab.HeaderString
    tmpNewFields = GetRealItem(tmpLogFields, 1, ",") & "," & GetRealItem(tmpLogFields, 0, ",") & "," & GetRealItem(tmpLogFields, 2, ",")
    CurTab.HeaderString = tmpNewFields
    CurTab.Sort "0", True, True
    CurTab.Sort "2", True, True
    CurTab.AutoSizeColumns
    CurTab.Refresh
End Sub
Private Sub HandleAodbRefresh(CurButton As CheckBox)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            If chkWork(i).Value = 1 Then
                chkWork(i).Value = 0
                chkWork(i).Value = 1
            End If
        End If
    Next
    CurButton.Value = 0
End Sub
Private Sub HandleAodbFilter(CurButton As CheckBox, CloseWin As Boolean, sCaller As String)
    Dim tmpMsg As String
    Dim tmpFields As String
    Dim tmpTest As String
    Dim tmpData As String
    Dim tmpBuff As String
    Dim tmpFld As String
    Dim i As Integer
    Dim j As Long
    Dim LineNo As Long
    tmpMsg = ""
    If CloseWin Then
        If txtTifr(0).Tag = "" Then tmpMsg = tmpMsg & "Time Period Begin" & vbNewLine
        If txtTito(0).Tag = "" Then tmpMsg = tmpMsg & "Time Period End" & vbNewLine
    End If
    If tmpMsg = "" Then
        DataFilterVpfr = Left(txtTifr(2).Tag, 12) & "00"
        DataFilterVpto = Left(txtTito(2).Tag, 12) & "59"
        MyGlobalResult = ""
        tmpFields = "ZONE" & Chr(15) & "VPFR" & Chr(15) & "VPTO" & Chr(15) & "HOPO"
        tmpFields = tmpFields & Chr(15) & "TIFR" & Chr(15) & "TITO"
        tmpData = optTimeZone(0).Tag & Chr(15) & Left(txtTifr(2).Tag, 8) & Chr(15) & Left(txtTito(2).Tag, 8) & Chr(15) & UfisServer.HOPO
        tmpData = tmpData & Chr(15) & Mid(txtTifr(2).Tag, 9, 4) & Chr(15) & Mid(txtTito(2).Tag, 9, 4)
        If (FtypPanel.Visible) And (txtFtypFilter.Text <> "") Then
            tmpFields = tmpFields & Chr(15) & "FTYP"
            tmpData = tmpData & Chr(15) & txtFtypFilter.Text
        End If
        For i = 0 To FilterPanel.UBound
            If FilterPanel(i).Visible Then
                'Filter count is greater than n equal 1
                If chkWork(i).Value = 1 Then
                    'If left panel have nothing, system will query the result with default key....
                    If LeftFilterTab(i).GetLineCount >= 0 Then
                        LineNo = RightFilterTab(i).GetLineCount - 1
                        If LineNo >= 0 Then
                            tmpTest = RightFilterTab(i).LogicalFieldList
                            tmpFld = RightFilterTab(i).myTag
                            '/*****************************\'
                            '/* If Field name is GRPN, Retrieve the airport list that
                            '/* has been tied to the static group
                            '/* PHYOE (13/Nov/2011)
                            If tmpFld <> "GRPN" And LeftFilterTab(i).GetLineCount > 0 Then
                                tmpFields = tmpFields & Chr(15) & tmpFld
                                tmpBuff = RightFilterTab(i).GetBufferByFieldList(0, LineNo, tmpFld, ",")
                                tmpBuff = "'" & Replace(tmpBuff, ",", "','", 1, -1, vbBinaryCompare) & "'"
                                tmpBuff = Replace(tmpBuff, "''", "' '", 1, -1, vbBinaryCompare)
                                tmpData = tmpData & Chr(15) & tmpBuff
                            ElseIf tmpFld = "GRPN" Then
                                Dim sAirports As String
                                Dim sField As String
                                Dim sCondition As String
                                Dim sTable As String
                                Dim iReturnValue As Integer
                                Dim arrString() As String
                                Dim iInt As Integer
                                Dim sAptValue As String
                                Dim sChkString As String
                                
                                If CedaIsConnected Then
                                    sAirports = ""
                                    sTable = "SGRTAB, SGMTAB, APTTAB"
                                    sField = "APC3"
                                    tmpFields = tmpFields & Chr(15) & tmpFld
                                    tmpBuff = RightFilterTab(i).GetBufferByFieldList(0, LineNo, tmpFld, ",")
                                    arrString = Split(tmpBuff, ",")
                                    If UBound(arrString) >= 0 Then
                                        sAptValue = ""
                                        For iInt = 0 To UBound(arrString)
                                            sCondition = "WHERE SGRTAB.URNO = SGMTAB.USGR AND SGMTAB.UVAL = APTTAB.URNO AND GRPN = '" + arrString(iInt) + "'"
                                            iReturnValue = UfisServer.CallCeda(sAirports, "RT", sTable, sField, "", sCondition, "", 0, True, False)
                                            sChkString = ""
                                            sChkString = Replace(sAirports, Chr(10), "", , , vbBinaryCompare)
                                            sChkString = Replace(sChkString, Chr(13), "", , , vbBinaryCompare)
                                            If Len(RTrim(LTrim(sChkString))) > 0 Then
                                                sAirports = Replace(sAirports, Chr(10), ",", , , vbBinaryCompare)
                                                sAirports = Replace(sAirports, Chr(13), "", , , vbBinaryCompare)
                                                sAptValue = sAptValue + "'" & Replace(sAirports, ",", "','", 1, -1, vbBinaryCompare) & "'"
                                                sAptValue = Replace(sAptValue, "''", "' '", 1, -1, vbBinaryCompare)
                                                If iInt = 0 Then
                                                    tmpData = tmpData & Chr(15) & sAptValue
                                                Else
                                                    tmpData = tmpData + sAptValue
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                            '/******************************\'
                        End If
                    End If
                End If
            End If
        Next
        MyGlobalResult = tmpFields & Chr(16) & tmpData
        If CloseWin Then
            SaveFilterDialog sCaller
            Me.Hide
        End If
    Else
        tmpMsg = "Missing or wrong input for:" & vbNewLine & tmpMsg
        If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", tmpMsg, "stop2", "", UserAnswer) >= 0 Then DoNothing
    End If
    If CloseWin Then CurButton.Value = 0
End Sub
Private Sub HandleClose(CurButton As CheckBox)
    MyGlobalResult = ""
    Me.Hide
    CurButton.Value = 0
End Sub

Private Sub chkFilterIsVisible_Click(Index As Integer)
    Dim MyOldWidth As Long
    Dim MyNewWidth As Long
    Dim NewLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim i As Integer
    MyOldWidth = Me.Width
    NewLeft = FilterPanel(0).Left
    For i = 0 To LeftFilterTab.UBound
        If RightFilterTab(i).myTag <> "" Then
            If chkFilterIsVisible(i).Value = 1 Then
                FilterPanel(i).Left = NewLeft
                FilterPanel(i).Visible = True
                NewLeft = FilterPanel(i).Left + FilterPanel(i).Width + 120
            Else
                FilterPanel(i).Visible = False
            End If
        End If
    Next
    InternalResize = True
    MyNewWidth = NewLeft + RightPanel.Width + 180
    If FtypPanel.Visible Then MyNewWidth = MyNewWidth + FtypPanel.Width
    NewLeft = Me.Left + MyOldWidth - MyNewWidth
    Me.Move NewLeft, Me.Top, MyNewWidth, Me.Height
    InternalResize = False
    Form_Resize
End Sub

Private Sub chkFromTo_Click()
    chkFromTo.Value = 0
End Sub

Private Sub chkFtypAll_Click(Index As Integer)
    Dim i As Integer
    If (Not ButtonPushedByUser) And (Not LoadingConfig) And (Not RestoringFilter) Then
        StopNestedCalls = True
        For i = 0 To chkFtypFilter.UBound
            If chkFtypFilter(i).Visible Then
                If chkFtypFilter(i).Enabled Then
                    chkFtypFilter(i).Value = (1 - chkFtypAll(Index).Value)
                End If
            End If
        Next
        StopNestedCalls = False
    End If
End Sub

Private Sub chkFtypFilter_Click(Index As Integer)
    If Not RestoringFilter Then
        If (Not StopNestedCalls) Then ButtonPushedByUser = True
        CheckFtypFilter
        ButtonPushedByUser = False
    End If
End Sub
Private Sub CheckFtypFilter()
    Dim tmpData As String
    Dim i As Integer
    tmpData = ""
    For i = 0 To chkFtypFilter.UBound
        If chkFtypFilter(i).Visible Then
            If chkFtypFilter(i).Value = 1 Then
                tmpData = tmpData & "'" & chkFtypFilter(i).Caption & "',"
            End If
        End If
    Next
    If tmpData <> "" Then
        tmpData = Left(tmpData, Len(tmpData) - 1)
        If (Not StopNestedCalls) And (Not LoadingConfig) Then chkFtypAll(0).Value = 0
    Else
        If (Not StopNestedCalls) And (Not LoadingConfig) Then chkFtypAll(0).Value = 1
    End If
    txtFtypFilter.Text = tmpData
End Sub
Private Sub chkFullSize_Click()
    Dim MyOldWidth As Long
    Dim MyNewWidth As Long
    Dim NewLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim i As Integer
    MyOldWidth = Me.Width
    NewLeft = FilterPanel(0).Left
    For i = 0 To LeftFilterTab.UBound
        If RightFilterTab(i).myTag <> "" Then
            WidthList = LeftFilterTab(i).HeaderLengthString
            TabWidth = Val(GetRealItem(WidthList, 0, ",")) * 15
            If chkFullSize.Value = 1 Then TabWidth = TabWidth + Val(GetRealItem(WidthList, 1, ",")) * 15
            TabWidth = TabWidth + 16 * 15 'ScrollBar
            If TabWidth < 855 Then TabWidth = 855
            LeftFilterTab(i).Width = TabWidth
            RightFilterTab(i).Width = TabWidth
            RightFilterTab(i).Left = LeftFilterTab(i).Left + TabWidth + 15
            FilterPanel(i).Left = NewLeft
            FilterPanel(i).Width = RightFilterTab(i).Left + RightFilterTab(i).Width + 60
            chkWork(i).Width = TabWidth
            txtFilterVal(i).Width = TabWidth
            txtFilterVal(i).Left = RightFilterTab(i).Left
            FilterPanel(i).Width = RightFilterTab(i).Left + RightFilterTab(i).Width + 60
            
            chkLeftFilterSet(i).Width = TabWidth - 90
            chkRightFilterSet(i).Width = TabWidth - 90
            chkLeftFilterSet(i).Left = LeftFilterTab(i).Left + ((TabWidth - chkLeftFilterSet(i).Width) / 2)
            chkRightFilterSet(i).Left = RightFilterTab(i).Left + ((TabWidth - chkRightFilterSet(i).Width) / 2) - 15
            
            ButtonPanel(i).Left = -30
            ButtonPanel(i).Width = FilterPanel(i).Width + 30
            DrawBackGround ButtonPanel(i), 7, True, True
            
            NewLeft = FilterPanel(i).Left + FilterPanel(i).Width + 120
        End If
    Next
    InternalResize = True
    MyNewWidth = NewLeft + RightPanel.Width + 180
    NewLeft = Me.Left + MyOldWidth - MyNewWidth
    Me.Move NewLeft, Me.Top, MyNewWidth, Me.Height
    InternalResize = False
    Form_Resize
End Sub

Private Sub chkFullUnits_Click(Index As Integer)
    Dim i As Integer
    If chkFullUnits(Index).Value = 1 Then
        Select Case Index
            Case 0  'Full Days
                If optInput(1).Value = True Then
                    'Date Input Format
                    txtTimeFrom(0).Visible = False
                    txtTimeTo(0).Visible = False
                    txtDummy(0).Visible = True
                    txtDummy(1).Visible = True
                    For i = 4 To 7
                        txtDummy(i).Visible = False
                    Next
                Else
                    'Field Input Format
                    For i = 3 To 4
                        txtTifr(i).Visible = False
                        txtTito(i).Visible = False
                    Next
                    txtDummy(0).Visible = False
                    txtDummy(1).Visible = False
                    For i = 4 To 7
                        txtDummy(i).Visible = True
                    Next
                End If
                fraFixTime(0).Visible = True
                fraTimeOffset.Enabled = False
                txtTimeOffset(0).BackColor = LightGrey
                txtTimeOffset(1).BackColor = LightGrey
            Case 1  'Full Hours
                fraFixTime(1).Visible = True
                If optInput(1).Value = True Then
                    'Date Input Format
                    txtTimeFrom(0).Visible = False
                    txtTimeTo(0).Visible = False
                    txtTimeFrom(1).Visible = True
                    txtTimeTo(1).Visible = True
                Else
                    'Field Input Format
                    txtTifr(4).Visible = False
                    txtTito(4).Visible = False
                End If
            Case Else
        End Select
    Else
        Select Case Index
            Case 0  'Full Days
                fraFixTime(0).Visible = False
                If optInput(1).Value = True Then
                    'Date Input Format
                    txtTimeFrom(0).Visible = True
                    txtTimeTo(0).Visible = True
                Else
                    'Field Input Format
                    For i = 3 To 4
                        txtTifr(i).Visible = True
                        txtTito(i).Visible = True
                    Next
                End If
                fraTimeOffset.Enabled = True
                txtTimeOffset(0).BackColor = LightestYellow
                txtTimeOffset(1).BackColor = LightestYellow
            Case 1  'Full Hours
                fraFixTime(1).Visible = False
                If optInput(1).Value = True Then
                    'Date Input Format
                    txtTimeFrom(0).Visible = True
                    txtTimeTo(0).Visible = True
                    txtTimeFrom(1).Visible = False
                    txtTimeTo(1).Visible = False
                Else
                    'Field Input Format
                    txtTifr(4).Visible = True
                    txtTito(4).Visible = True
                End If
            Case Else
        End Select
    End If
    SetFilterFieldValues -1
End Sub

Private Sub chkLeftFilterSet_Click(Index As Integer)
    Dim tmpBuff As String
    Dim LineNo As Long
    If chkLeftFilterSet(Index).Value = 1 Then
        LineNo = LeftFilterTab(Index).GetLineCount - 1
        If LineNo >= 0 Then
            tmpBuff = LeftFilterTab(Index).GetBuffer(0, LineNo, vbLf)
            RightFilterTab(Index).InsertBuffer tmpBuff, vbLf
            LeftFilterTab(Index).ResetContent
            LeftFilterTab(Index).Refresh
            RightFilterTab(Index).Sort "0", True, True
            RightFilterTab(Index).Refresh
            chkLeftFilterSet(Index).Caption = "0"
            chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        End If
        chkLeftFilterSet(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkRightFilterSet_Click(Index As Integer)
    Dim tmpBuff As String
    Dim LineNo As Long
    If chkRightFilterSet(Index).Value = 1 Then
        LineNo = RightFilterTab(Index).GetLineCount - 1
        If LineNo >= 0 Then
            tmpBuff = RightFilterTab(Index).GetBuffer(0, LineNo, vbLf)
            LeftFilterTab(Index).InsertBuffer tmpBuff, vbLf
            RightFilterTab(Index).ResetContent
            RightFilterTab(Index).Refresh
            LeftFilterTab(Index).Sort "0", True, True
            LeftFilterTab(Index).Refresh
            chkRightFilterSet(Index).Caption = "0"
            chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        End If
        chkRightFilterSet(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkToggle_Click()
    HandleToggleFilter chkToggle, -1
End Sub

Private Sub chkWork_Click(Index As Integer)
    On Error Resume Next
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        If Not RestoringFilter Then
            CheckFilterTask Index, chkWork(Index), LeftFilterTab(Index)
        End If
        txtFilterVal(Index).Enabled = True
        txtFilterVal(Index).BackColor = vbWhite
        LeftFilterTab(Index).DisplayBackColor = LightYellow
        LeftFilterTab(Index).DisplayTextColor = vbBlack
        LeftFilterTab(Index).Enabled = True
        LeftFilterTab(Index).AutoSizeColumns
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).DisplayBackColor = NormalBlue
        RightFilterTab(Index).DisplayTextColor = vbWhite
        RightFilterTab(Index).Enabled = True
        RightFilterTab(Index).AutoSizeColumns
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Enabled = True
        chkRightFilterSet(Index).Enabled = True
        txtFilterVal(Index).SetFocus
    Else
        chkWork(Index).BackColor = MyOwnButtonFace
        txtFilterVal(Index).BackColor = LightGray
        txtFilterVal(Index).Enabled = False
        LeftFilterTab(Index).DisplayBackColor = LightGray
        LeftFilterTab(Index).DisplayTextColor = vbBlack
        LeftFilterTab(Index).Refresh
        LeftFilterTab(Index).Enabled = False
        RightFilterTab(Index).DisplayBackColor = LightGray
        RightFilterTab(Index).DisplayTextColor = vbBlack
        RightFilterTab(Index).Refresh
        RightFilterTab(Index).Enabled = False
        chkLeftFilterSet(Index).Enabled = False
        chkRightFilterSet(Index).Enabled = False
    End If
End Sub

Private Function CheckFilterTask(Index As Integer, CurButton As CheckBox, CurTab As TABLib.Tab) As Boolean
    Dim NewData As Boolean
    Dim tmpTag As String
    Dim tmpLayout As String
    Dim tmpTimeFrame As String
    Dim AodbResult As String
    Dim PatchCodes As String
    Dim PatchValues As String
    Dim FilterList As String
    
    NewData = False
    If WorkArea.Visible Then
        tmpTag = Me.Tag
        tmpLayout = GetRealItem(tmpTag, 0, ",")
        Select Case tmpLayout
            Case "AODB_FILTER"
                HandleAodbFilter CurButton, False, "CFT"
                FilterList = MyGlobalResult
                tmpTimeFrame = txtTifr(2).Tag & "/" & txtTito(2).Tag
                If Len(tmpTimeFrame) = 25 Then
                    tmpTag = CurButton.Tag
                    If tmpTag <> FilterList Then
                        If RightFilterTab(Index).GetLineCount = 0 Then
                            CurTab.ResetContent
                            CurTab.Refresh
                            RightFilterTab(Index).ResetContent
                            RightFilterTab(Index).Refresh
                            chkRightFilterSet(Index).Caption = "0"
                            PatchCodes = GetRealItem(FilterList, 0, Chr(16))
                            PatchValues = GetRealItem(FilterList, 1, Chr(16))
                            AodbResult = HiddenData.GetFilterData(CurTab.myName, CurTab.LogicalFieldList, PatchCodes, PatchValues)
                            CurTab.InsertBuffer AodbResult, vbLf
                            CurButton.Tag = FilterList
                            CurTab.Sort "0", True, True
                            CurTab.Sort "2", True, True
                            chkLeftFilterSet(Index).Caption = CStr(CurTab.GetLineCount)
                            NewData = True
                        End If
                    End If
                End If
            Case Else
        End Select
    End If
    CheckFilterTask = NewData
End Function

Private Sub chkWork_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then HandleToggleFilter chkFilterIsVisible(Index), Index
End Sub

Private Sub cmdDayOffset_Click(Index As Integer)
    Dim NewVal As Long
    Dim tmpVal As String
    Dim i As Integer
    If Index > 1 Then i = 3 Else i = 2
    NewVal = Val(txtTimeOffset(i).Text) + Val(cmdDayOffset(Index).Tag)
    tmpVal = CStr(NewVal)
    If NewVal > 0 Then tmpVal = "+" & tmpVal
    txtTimeOffset(i).Text = tmpVal
End Sub

Private Sub cmdHourOffset_Click(Index As Integer)
    Dim NewVal As Long
    Dim tmpVal As String
    Dim i As Integer
    If Index > 1 Then i = 1 Else i = 0
    NewVal = Val(txtTimeOffset(i).Text) + Val(cmdHourOffset(Index).Tag)
    tmpVal = CStr(NewVal)
    If NewVal > 0 Then tmpVal = "+" & tmpVal
    txtTimeOffset(i).Text = tmpVal
End Sub

Private Sub SaveFilterDialog(sCallFrom As String)
    Dim i As Integer
    Dim MainFile As String
    Dim myLoadPath As String
    Dim myLoadName As String
    Dim tmpFileName As String
    Dim DatFileSize As Long
    Dim DatFileData As String
    Dim DatFileName As String
    Dim tmpData As String
    Dim tmpFile As String
    Dim KPass As String
    Dim fn As Integer
    Dim ffn As Integer
    
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim ActDatLst As String
    Dim retval As String
    Dim sDateFormat As String
    Dim stmpCaller As String
    Dim iURNO As Long
    Dim bTmpReturn As Boolean
    
    stmpCaller = sCallFrom
    
    myLoadPath = UFIS_TMP
    myLoadName = ApplMainCode & "_" & ApplFuncCode & "_Filter"
    tmpFileName = myLoadPath & "\" & myLoadName
    MainFile = myLoadPath & "\" & myLoadName & "_All.usf"
    FilterFileName = MainFile
    RestoreFilter = False
    tmpFile = Dir(FilterFileName)
    If tmpFile <> "" Then Kill FilterFileName
    ffn = FreeFile
    Open MainFile For Binary As #ffn
    DatFileData = ""
    tmpData = ""
    For i = 0 To chkFilterIsVisible.UBound
        tmpData = tmpData & CStr(i) & ";" & CStr(chkFilterIsVisible(i).Value) & ","
    Next
    DatFileData = DatFileData & "[=CBOX=]" & tmpData & "[/=CBOX=]" & vbNewLine
    tmpData = "ALL;" & CStr(chkFtypAll(0).Value) & ","
    For i = 0 To chkFtypFilter.UBound
        tmpData = tmpData & CStr(i) & ";" & CStr(chkFtypFilter(i).Value) & ","
    Next
    DatFileData = DatFileData & "[=FTYP=]" & tmpData & "[/=FTYP=]" & vbNewLine
    tmpData = "FULL;" & CStr(chkFullSize.Value) & ","
    tmpData = tmpData & "TOGL;" & CStr(chkToggle.Value) & ","
    DatFileData = DatFileData & "[=TOOL=]" & tmpData & "[/=TOOL=]" & vbNewLine
    tmpData = ""
    For i = 0 To chkFullUnits.UBound
        tmpData = tmpData & CStr(i) & ";" & CStr(chkFullUnits(i).Value) & ","
    Next
    DatFileData = DatFileData & "[=UNIT=]" & tmpData & "[/=UNIT=]" & vbNewLine
    tmpData = ""
    For i = 0 To txtTimeOffset.UBound
        tmpData = tmpData & CStr(i) & ";" & txtTimeOffset(i).Text & ","
    Next
    DatFileData = DatFileData & "[=OFFS=]" & tmpData & "[/=OFFS=]" & vbNewLine
    tmpData = ""
    For i = 0 To txtTifr.UBound
        tmpData = tmpData & CStr(i) & ";" & txtTifr(i).Text & ","
    Next
    DatFileData = DatFileData & "[=TIFR=]" & tmpData & "[/=TIFR=]" & vbNewLine
    tmpData = ""
    For i = 0 To txtTito.UBound
        tmpData = tmpData & CStr(i) & ";" & txtTito(i).Text & ","
    Next
    DatFileData = DatFileData & "[=TITO=]" & tmpData & "[/=TITO=]" & vbNewLine
    
    tmpData = MyGlobalResult
    DatFileData = DatFileData & "[=KEYS=]" & tmpData & "[/=KEYS=]" & vbNewLine
    
    tmpData = ""
    For i = 0 To chkWork.UBound
        tmpData = tmpData & CStr(i) & ";" & CStr(chkWork(i).Value) & ","
    Next
    DatFileData = DatFileData & "[=WORK=]" & tmpData & "[/=WORK=]" & vbNewLine
    'Write to file
    DatFileData = SaveToFile(ffn, "GLOBAL", "FILTER", DatFileData, KPass)
    'Update the string value to understand by ceda
    DatFileData = CleanString(DatFileData, FOR_SERVER, False)
    'Get current datetime in string
    sDateFormat = Format(Now, "yyyymmddhhmmss")
    'Trim the datetime string value
    sDateFormat = LTrim(RTrim(sDateFormat))
    'Get Latest URNO
    iURNO = UfisServer.UrnoPoolGetNext
    Dim sTmpCKEY As String
    Dim lTmpURNO As Long
    sTmpCKEY = ""
    lTmpURNO = 0
    
    ActResult = ""
    ActCmd = "IRT"
    ActTable = "VCDTAB"
    ActFldLst = "APPN,CKEY,CTYP,HOPO, PKNO, TEXT, URNO, VAFR, VATO"
    '/******** Save Header Filter to DB If the user click save button*******\
    If stmpCaller = "S" Then
        'New Entry
        If cboSavedFilters.Text <> "" And cboSavedFilters.ListIndex < 0 Then
            ActDatLst = "UFIS-FDT," & RTrim(cboSavedFilters.Text) & ",1," & UfisServer.HOPO & "," & LoginUserName & "," & DatFileData & "," & iURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
        End If
        If cboSavedFilters.Text <> "" And cboSavedFilters.ListIndex > 0 Then
            sTmpCKEY = Trim(cboSavedFilters.Text)
            lTmpURNO = CLng(cboSavedFilters.ItemData(cboSavedFilters.ListIndex))
            
            bTmpReturn = DeleteExistingFilter(Trim(cboSavedFilters.Text), CLng(cboSavedFilters.ItemData(cboSavedFilters.ListIndex)))
            
            If bTmpReturn = True Then
                ActDatLst = "UFIS-FDT," & RTrim(sTmpCKEY) & ",1," & UfisServer.HOPO & "," & LoginUserName & "," & DatFileData & "," & lTmpURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
            End If
        End If
    End If
    '/******************
    Dim j As Integer
    Dim k As Integer
    Dim sChopString As String
    Dim iStart As Integer
    Const max_l As Integer = 2000
    Dim lth As Integer
    Dim noofloop As Integer
    Dim intMod As Integer
    Dim l As Integer
    j = 1
    fn = FreeFile
    For i = 0 To chkFilterIsVisible.UBound
        If chkFilterIsVisible(i).Visible Then
            DatFileName = tmpFileName & "_Left_" & CStr(i) & ".usd"
            LeftFilterTab(i).WriteToFile DatFileName, False
            Open DatFileName For Binary As #fn
            DatFileSize = LOF(fn)
            DatFileData = String$(DatFileSize, 32)
            Get #fn, , DatFileData
            Close #fn
            'Save To file
            DatFileData = SaveToFile(ffn, "TAB_LEFT," & CStr(i), DatFileName, DatFileData, KPass)
            'Update the string value to understand by ceda
            DatFileData = CleanString(DatFileData, FOR_SERVER, False)
            'Save n Load
            If stmpCaller = "S" Then
                'New Filter Detail
                If cboSavedFilters.Text <> "" And cboSavedFilters.ListIndex < 0 Then
                    'if DatFileData string is more than 2000 Bytes
                    If Len(DatFileData) > 2000 Then
                        'Set Total lenght of data
                        lth = Len(DatFileData)
                        'Calculate loop count
                        noofloop = lth \ max_l
                        'Check Modulus
                        intMod = lth Mod max_l
                        'If modulous has, add one more loop
                        If intMod > 0 Then noofloop = noofloop + 1
                        'Initialise istart value
                        iStart = 1
                        For l = 1 To noofloop
                            sChopString = Mid(DatFileData, iStart, max_l)
                            j = j + 1
                            'SAVE TO DB
                            ActResult = ""
                            ActDatLst = "UFIS-FDT," & RTrim(cboSavedFilters.Text) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & sChopString & "," & iURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                            
                            iStart = iStart + max_l
                        Next l
                    ElseIf Len(DatFileData) < 2000 Then
                        j = j + 1
                        'SAVE TO DB
                        ActResult = ""
                        ActDatLst = "UFIS-FDT," & RTrim(cboSavedFilters.Text) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & DatFileData & "," & iURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                    
                    End If
                ElseIf bTmpReturn = True Then
                    'if DatFileData string is more than 2000 Bytes
                    If Len(DatFileData) > 2000 Then
                        'Set Total lenght of data
                        lth = Len(DatFileData)
                        'Calculate loop count
                        noofloop = lth \ max_l
                        'Check Modulus
                        intMod = lth Mod max_l
                        'If modulous has, add one more loop
                        If intMod > 0 Then noofloop = noofloop + 1
                        'Initialise istart value
                        iStart = 1
                        For l = 1 To noofloop
                            sChopString = Mid(DatFileData, iStart, max_l)
                            j = j + 1
                            'SAVE TO DB
                            ActResult = ""
                            ActDatLst = "UFIS-FDT," & RTrim(sTmpCKEY) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & sChopString & "," & lTmpURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                            
                            iStart = iStart + max_l
                        Next l
                    ElseIf Len(DatFileData) < 2000 Then
                        j = j + 1
                        'SAVE TO DB
                        ActResult = ""
                        ActDatLst = "UFIS-FDT," & RTrim(sTmpCKEY) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & DatFileData & "," & lTmpURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                    
                    End If
                End If
            End If
            Kill DatFileName
            DatFileName = tmpFileName & "_Right_" & CStr(i) & ".usd"
            RightFilterTab(i).WriteToFile DatFileName, False
            Open DatFileName For Binary As #fn
            DatFileSize = LOF(fn)
            DatFileData = String$(DatFileSize, 32)
            Get #fn, , DatFileData
            Close #fn
            'Save To file
            DatFileData = SaveToFile(ffn, "TAB_RIGHT," & CStr(i), DatFileName, DatFileData, KPass)
            'Update the string value to understand by ceda
            DatFileData = CleanString(DatFileData, FOR_SERVER, False)
            If stmpCaller = "S" Then
                'New Filter Detail
                If cboSavedFilters.Text <> "" And cboSavedFilters.ListIndex < 0 Then
                    'If filter detail is more than 2000 characters
                    If Len(DatFileData) > 2000 Then
                        'Set Total lenght of data
                        lth = Len(DatFileData)
                        'Calculate loop count
                        noofloop = lth \ max_l
                        'Check Modulus
                        intMod = lth Mod max_l
                        'If modulous has, add one more loop
                        If intMod > 0 Then noofloop = noofloop + 1
                        'Initialise istart value
                        iStart = 1
                        For l = 1 To noofloop
                            sChopString = Mid(DatFileData, iStart, max_l)
                            j = j + 1
                            'SAVE TO DB
                            ActResult = ""
                            ActDatLst = "UFIS-FDT," & RTrim(cboSavedFilters.Text) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & sChopString & "," & iURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                            iStart = iStart + max_l
                        Next l
                    ElseIf Len(DatFileData) < 2000 Then
                    'SAVE TO DB
                        j = j + 1
                        ActResult = ""
                        ActDatLst = "UFIS-FDT," & RTrim(cboSavedFilters.Text) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & DatFileData & "," & iURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                        
                    End If
                ElseIf bTmpReturn = True Then
                    'if DatFileData string is more than 2000 Bytes
                    If Len(DatFileData) > 2000 Then
                        'Set Total lenght of data
                        lth = Len(DatFileData)
                        'Calculate loop count
                        noofloop = lth \ max_l
                        'Check Modulus
                        intMod = lth Mod max_l
                        'If modulous has, add one more loop
                        If intMod > 0 Then noofloop = noofloop + 1
                        'Initialise istart value
                        iStart = 1
                        For l = 1 To noofloop
                            sChopString = Mid(DatFileData, iStart, max_l)
                            j = j + 1
                            'SAVE TO DB
                            ActResult = ""
                            ActDatLst = "UFIS-FDT," & RTrim(sTmpCKEY) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & sChopString & "," & lTmpURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                            retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                            
                            iStart = iStart + max_l
                        Next l
                    ElseIf Len(DatFileData) < 2000 Then
                        j = j + 1
                        'SAVE TO DB
                        ActResult = ""
                        ActDatLst = "UFIS-FDT," & RTrim(sTmpCKEY) & "," & j & "," & UfisServer.HOPO & "," & LoginUserName & "," & DatFileData & "," & lTmpURNO & "," & Trim(sDateFormat) & "," & Trim(sDateFormat)
                        retval = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, "", "", 0, True, False)
                    
                    End If
                End If
            End If
            Kill DatFileName
        End If
    Next
    Close #ffn
End Sub

Private Sub RestoreFilterDialog()
    Dim i As Integer
    Dim MainFile As String
    Dim myLoadPath As String
    Dim myLoadName As String
    Dim tmpFileName As String
    Dim DatFileSize As Long
    Dim DatFileData As String
    Dim DatFileName As String
    Dim tmpData As String
    Dim tmpItem As String
    Dim tmpFile As String
    Dim FData As String
    Dim KSize As String
    Dim KHead As String
    Dim KTail As String
    Dim CHead As String
    Dim CTail As String
    Dim THead As String
    Dim TTail As String
    Dim DHead As String
    Dim DTail As String
    Dim KCode As String
    Dim KName As String
    Dim KText As String
    Dim KData As String
    Dim KPass As String
    Dim DSize As Long
    Dim RSize As Long
    Dim FSize As Long
    Dim PSize As Long
    Dim BreakOut As Boolean
    Dim TabIdx As Integer
    Dim fn As Integer
    Dim ffn As Integer
    On Error Resume Next
    myLoadPath = UFIS_TMP
    myLoadName = ApplMainCode & "_" & ApplFuncCode & "_Filter"
    tmpFileName = myLoadPath & "\" & myLoadName
    MainFile = myLoadPath & "\" & myLoadName & "_All.usf"
    'FilterFileName = MainFile
    tmpFile = Dir(FilterFileName)
    If tmpFile <> "" Then
        'Reset All (Needs to be done twice)
        chkAppl(3).Value = 1
        chkAppl(3).Value = 1
        RestoringFilter = True
        tmpFileName = FilterFileName '& "_test"
        KHead = "{*SIZE*}"
        KSize = "0000000000"
        KTail = "{/*SIZE*}"
        CHead = "{*CODE*}"
        CTail = "{/*CODE*}"
        THead = "{*TEXT*}"
        TTail = "{/*TEXT*}"
        DHead = "{*DATA*}"
        DTail = "{/*DATA*}"
        KCode = KHead & KSize & KTail & vbNewLine
        DSize = Len(KCode)
        KSize = String$(DSize, 32)
            
        ffn = FreeFile
        Open tmpFileName For Binary As #ffn
        'ProgShown = False
        FSize = LOF(ffn)
        'Get File Prefix
        'FileHead = String$(7, 32)
        'Get #ffn, , FileHead
        'PSize = Len(FileHead)
        'KPass = ""
        'tmpData = Left(FileHead, 6)
        PSize = 0
        BreakOut = False
        While ((PSize < FSize) And (Not BreakOut))
            Get #ffn, , KSize
            'If FileCrypt Then KSize = EnCryptBuffer(KSize, KPass)
            GetKeyItem FData, KSize, KHead, KTail
            RSize = Val(FData) - DSize
            FData = String$(RSize, 32)
            Get #ffn, , FData
            'If FileCrypt Then FData = EnCryptBuffer(FData, KPass)
            GetKeyItem KCode, FData, CHead, CTail
            GetKeyItem KText, FData, THead, TTail
            GetKeyItem KData, FData, DHead, DTail
            PSize = PSize + DSize + RSize
            KName = GetItem(KCode, 1, ",")
            Select Case KName
                Case "TAB_LEFT"
                    tmpItem = GetItem(KCode, 2, ",")
                    If tmpItem <> "" Then
                        TabIdx = Val(tmpItem)
                        LeftFilterTab(TabIdx).ResetContent
                        fn = FreeFile
                        Open KText For Output As #fn
                        Print #fn, KData
                        Close #fn
                        LeftFilterTab(TabIdx).readFromFile KText
                        chkLeftFilterSet(TabIdx).Caption = CStr(LeftFilterTab(TabIdx).GetLineCount)
                    End If
                Case "TAB_RIGHT"
                    tmpItem = GetItem(KCode, 2, ",")
                    If tmpItem <> "" Then
                        TabIdx = Val(tmpItem)
                        RightFilterTab(TabIdx).ResetContent
                        fn = FreeFile
                        Open KText For Output As #fn
                        Print #fn, KData
                        Close #fn
                        RightFilterTab(TabIdx).readFromFile KText
                        chkRightFilterSet(TabIdx).Caption = CStr(RightFilterTab(TabIdx).GetLineCount)
                    End If
                Case "GLOBAL"
                    tmpItem = GetItem(KCode, 2, ",")
                    RestoreFilterObjects KData
                    'GetSetGlobalValues "PUT", "", KData
                    'tmpData = GetSetGlobalValues("GET", "PASS", "")
                    'LoginUserPass = EnCryptBuffer(tmpData, "")
                Case Else
                    'Something went wrong
                    BreakOut = True
            End Select
            'DoEvents
        Wend
        Close #ffn
        RestoringFilter = False
    End If
    RestoreFilter = False
End Sub
Private Sub RestoreFilterObjects(CfgList As String)
    Dim ObjLst As String
    Dim ObjGrp As String
    Dim ObjIdx As String
    Dim ObjVal As String
    Dim Itm As Integer
    Dim idx As Integer
    'RESTORE: Righthand Filter CheckBoxes
    On Error Resume Next
    GetKeyItem ObjLst, CfgList, "[=CBOX=]", "[/=CBOX=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            idx = Val(ObjIdx)
            chkFilterIsVisible(idx).Value = Val(ObjVal)
        End If
    Wend
    'RESTORE: FTYP Filter CheckBoxes
    GetKeyItem ObjLst, CfgList, "[=FTYP=]", "[/=FTYP=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            If ObjIdx = "ALL" Then
                chkFtypAll(0).Value = Val(ObjVal)
            Else
                idx = Val(ObjIdx)
                chkFtypFilter(idx).Value = Val(ObjVal)
            End If
        End If
    Wend
    CheckFtypFilter
    'RESTORE: Tool CheckBoxes
    GetKeyItem ObjLst, CfgList, "[=TOOL=]", "[/=TOOL=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            If ObjIdx = "FULL" Then
                chkFullSize.Value = Val(ObjVal)
            End If
            If ObjIdx = "TOGL" Then
                chkToggle.Value = Val(ObjVal)
            End If
        End If
    Wend
    'RESTORE: Time Unit CheckBoxes
    GetKeyItem ObjLst, CfgList, "[=UNIT=]", "[/=UNIT=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            idx = Val(ObjIdx)
            chkFullUnits(idx).Value = Val(ObjVal)
        End If
    Wend
    'RESTORE: Time Unit Offset Text Fields
    GetKeyItem ObjLst, CfgList, "[=OFFS=]", "[/=OFFS=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            idx = Val(ObjIdx)
            txtTimeOffset(idx).Text = ObjVal
        End If
    Wend
    'RESTORE: Timeframe Begin Text Fields
    GetKeyItem ObjLst, CfgList, "[=TIFR=]", "[/=TIFR=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            idx = Val(ObjIdx)
            txtTifr(idx).Text = ObjVal
        End If
    Wend
    'RESTORE: Timeframe End Text Fields
    GetKeyItem ObjLst, CfgList, "[=TITO=]", "[/=TITO=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            idx = Val(ObjIdx)
            txtTito(idx).Text = ObjVal
        End If
    Wend
    'RESTORE: Filter Work Buttons (CheckBoxes)
    GetKeyItem ObjLst, CfgList, "[=WORK=]", "[/=WORK=]"
    Itm = 0
    ObjGrp = "START"
    While ObjGrp <> ""
        Itm = Itm + 1
        ObjGrp = GetItem(ObjLst, Itm, ",")
        If ObjGrp <> "" Then
            ObjIdx = GetItem(ObjGrp, 1, ";")
            ObjVal = GetItem(ObjGrp, 2, ";")
            idx = Val(ObjIdx)
            chkWork(idx).Value = Val(ObjVal)
        End If
    Wend
End Sub

Private Sub Form_Activate()
    Dim tmpText As String
    Dim idx1 As Integer
    Dim idx2 As Integer
    tmpText = ""
    idx1 = 0
    idx2 = -1
    If ApplMainCode = "FDS" Then
        If ApplIsFdsMain Then
            If MainDialog.AnyOpt(0).Value = True Then
                'Load from production
                idx1 = 1
            End If
            If MainDialog.AnyOpt(1).Value = True Then
                'Load from test server
                idx1 = 2
            End If
            If MainDialog.AnyOpt(2).Value = True Then
                'Load from FDS production
                If MainDialog.CtrlChk(13).Value = 1 Then idx1 = 1 Else idx1 = -1
                If MainDialog.CtrlChk(14).Value = 1 Then idx2 = 2 Else idx1 = -1
            End If
        End If
    End If
    If idx1 >= 0 Then
        tmpText = tmpText & "Reading from "
        tmpText = tmpText & Chr(34) & UfisServer.FdtFuncName(idx1).Text & Chr(34)
        tmpText = tmpText & " [" & UfisServer.FdtHostName(idx1).Text & "]"
    End If
    If idx2 > 0 Then
        If idx1 >= 0 Then
            tmpText = tmpText & " and "
        Else
            tmpText = tmpText & "Reading from "
        End If
        tmpText = tmpText & Chr(34) & UfisServer.FdtFuncName(idx2).Text & Chr(34)
        tmpText = tmpText & " [" & UfisServer.FdtHostName(idx2).Text & "]"
    End If
    'For testing
    'tmpText = ""
    If tmpText = "" Then
        If ApplIsFdsMain Then
            tmpText = "Reading Nothing! Please check section'Data Grid Loader' in the Option Panel"
            MainDialog.chkTask(43).Value = 1
            MainDialog.chkTask(14).Value = 1
            If Left(MainDialog.CtrlImg(1).Tag, 3) = "CLS" Then MainDialog.ArrangeCtrlBoxes 0, 1, "USER", ""
        End If
    End If
    ServerLabel(0).Top = 45
    ServerLabel(0).Left = 90
    ServerLabel(1).Top = 60
    ServerLabel(1).Left = 105
    
    ServerLabel(0).Caption = tmpText
    ServerLabel(1).Caption = tmpText
    ServerLabel(0).ZOrder
    CheckFtypFilter
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'    Select Case KeyCode
'        Case 13
'            chkAppl(1).Value = 1
'        Case Else
'    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case 13
            AutoStartTimer.Tag = "LOAD"
            AutoStartTimer.Enabled = True
        Case Else
    End Select

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
'    Select Case KeyCode
'        Case 13
'            'chkAppl(1).Value = 1
'            HandleAodbFilter chkAppl(1), True
'        Case Else
'    End Select

End Sub

Private Sub Form_Load()
    Dim i As Integer
    SetButtonFaceStyle Me
    For i = 0 To chkFtypFilter.UBound
        chkFtypFilter(i).BackColor = LightGray
    Next
    lblFullSize(0).Top = chkFullSize.Top + 15
    lblFullSize(0).Left = chkFullSize.Left + chkFullSize.Width + 45
    lblFullSize(1).Top = lblFullSize(0).Top - 15
    lblFullSize(1).Left = lblFullSize(0).Left - 15
    lblToggle(0).Top = chkToggle.Top + 15
    lblToggle(0).Left = chkToggle.Left + chkToggle.Width + 45
    lblToggle(1).Left = lblToggle(0).Left - 15
    lblToggle(1).Top = lblToggle(0).Top - 15
    StatusBar.ZOrder
    optTimeZone(0).ToolTipText = "Times in UTC (" & HomeAirport & " Local -" & Str(UtcTimeDiff) & " min.)"
    optTimeZone(1).ToolTipText = "Times in " & HomeAirport & " Local (UTC +" & Str(UtcTimeDiff) & " min.)"
    
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        MyGlobalResult = ""
        Me.Tag = ""
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim MaxLin As Long
    If Not InternalResize Then
        NewTop = 0
        NewWidth = Me.ScaleWidth
        'WorkArea.Visible = False
        'If RightPanel.Visible Then
            NewWidth = NewWidth - RightPanel.Width
            NewLeft = NewWidth
            RightPanel.Top = NewTop
            RightPanel.Left = NewLeft
        'End If
        If ServerPanel.Visible Then
            ServerPanel.Top = NewTop
            ServerPanel.Left = 0
            If NewWidth > 600 Then
                ServerPanel.Width = NewWidth
            End If
            NewTop = NewTop + ServerPanel.Height
        End If
        'If FtypPanel.Visible Then
            NewWidth = NewWidth - FtypPanel.Width
            NewLeft = NewWidth
            FtypPanel.Top = NewTop
            FtypPanel.Left = NewLeft
        'End If
        If NewWidth > 600 Then
            WorkArea.Width = NewWidth
            TopPanel.Width = NewWidth
            'ServerPanel.Width = NewWidth
        End If
        NewHeight = Me.ScaleHeight - StatusBar.Height
        If NewHeight > 1500 Then
            RightPanel.Height = NewHeight
            FtypPanel.Height = NewHeight - FtypPanel.Top
        End If
        NewHeight = NewHeight - WorkArea.Top
        If NewHeight > 1500 Then
            WorkArea.Height = NewHeight
            NewHeight = NewHeight - 300
            NewTop = NewHeight - ButtonPanel(0).Height - 30
            For i = 0 To FilterPanel.UBound
                FilterPanel(i).Height = NewHeight
                ButtonPanel(i).Top = NewTop
            Next
            NewHeight = ButtonPanel(0).Top - LeftFilterTab(0).Top - 15
            For i = 0 To LeftFilterTab.UBound
                LeftFilterTab(i).Height = NewHeight
                RightFilterTab(i).Height = NewHeight
            Next
        End If
        DrawBackGround WorkArea, 7, True, True
        'WorkArea.Visible = True
        NewLeft = 0
        For i = FilterPanel.UBound To 0 Step -1
            If FilterPanel(i).Visible Then
                NewLeft = FilterPanel(i).Left + (FilterPanel(i).Width / 2)
                Exit For
            End If
        Next
        If WorkArea.Width >= NewLeft Then
            FilterScroll.Visible = False
        Else
            FilterScroll.Visible = True
            FilterScroll.ZOrder
        End If
    End If
End Sub

Private Sub lblFilterName_Click(Index As Integer)
    If chkFilterIsVisible(Index).Value = 0 Then chkFilterIsVisible(Index).Value = 1 Else chkFilterIsVisible(Index).Value = 0
End Sub

Private Sub lblFtypAll_Click(Index As Integer)
    If chkFtypAll(Index).Value = 0 Then chkFtypAll(Index).Value = 1 Else chkFtypAll(Index).Value = 0
End Sub

Private Sub lblFtypName_Click(Index As Integer)
    If chkFtypFilter(Index).Enabled = True Then
        If chkFtypFilter(Index).Value = 0 Then chkFtypFilter(Index).Value = 1 Else chkFtypFilter(Index).Value = 0
    End If
End Sub

Private Sub lblFullSize_Click(Index As Integer)
    If Index = 0 Then
        If chkFullSize.Value = 0 Then chkFullSize.Value = 1 Else chkFullSize.Value = 0
    End If
End Sub

Private Sub lblToggle_Click(Index As Integer)
    If chkToggle.Value = 0 Then chkToggle.Value = 1 Else chkToggle.Value = 0
End Sub

Private Sub LeftFilterTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpBuff As String
    If LineNo >= 0 Then
        tmpBuff = LeftFilterTab(Index).GetLineValues(LineNo)
        RightFilterTab(Index).InsertTextLine tmpBuff, False
        LeftFilterTab(Index).DeleteLine LineNo
        RightFilterTab(Index).Sort "0", True, True
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        CheckRefreshSettings
    End If
End Sub

Private Sub optInput_Click(Index As Integer)
    Dim i As Integer
    If optInput(Index).Value = True Then
        Select Case Index
            Case 1  'Date/Time
                For i = 0 To txtTifr.UBound
                    txtTifr(i).Visible = False
                    txtTito(i).Visible = False
                Next
                txtDateFrom.Visible = True
                txtDateTo.Visible = True
                If chkFullUnits(0).Value = 0 Then
                    If chkFullUnits(1).Value = 0 Then
                        txtTimeFrom(0).Visible = True
                        txtTimeTo(0).Visible = True
                        txtTimeFrom(1).Visible = False
                        txtTimeTo(1).Visible = False
                        fraFixTime(0).Visible = False
                        fraFixTime(1).Visible = False
                    Else
                        txtTimeFrom(0).Visible = False
                        txtTimeTo(0).Visible = False
                        txtTimeFrom(1).Visible = True
                        txtTimeTo(1).Visible = True
                        fraFixTime(1).Visible = True
                        fraFixTime(0).Visible = False
                    End If
                Else
                    txtTimeFrom(1).Visible = False
                    txtTimeTo(1).Visible = False
                    fraFixTime(1).Visible = False
                    txtDummy(0).Visible = True
                    txtDummy(1).Visible = True
                    For i = 4 To 7
                        txtDummy(i).Visible = False
                    Next
                    fraFixTime(0).Visible = True
                End If
            Case 2  'Digital Input Fields
                For i = 0 To 2
                    txtTifr(i).Visible = True
                    txtTito(i).Visible = True
                Next
                If chkFullUnits(0).Value = 0 Then
                    If chkFullUnits(1).Value = 0 Then
                        For i = 3 To 4
                            txtTifr(i).Visible = True
                            txtTito(i).Visible = True
                        Next
                        fraFixTime(0).Visible = False
                        fraFixTime(1).Visible = False
                        txtDateFrom.Visible = False
                        txtDateTo.Visible = False
                        txtTimeFrom(0).Visible = False
                        txtTimeTo(0).Visible = False
                        txtTimeFrom(1).Visible = False
                        txtTimeTo(1).Visible = False
                    Else
                        txtTifr(3).Visible = True
                        txtTito(3).Visible = True
                        txtTifr(4).Visible = False
                        txtTito(4).Visible = False
                        fraFixTime(0).Visible = False
                        txtDateFrom.Visible = False
                        txtDateTo.Visible = False
                        txtTimeFrom(0).Visible = False
                        txtTimeTo(0).Visible = False
                        txtTimeFrom(1).Visible = False
                        txtTimeTo(1).Visible = False
                        fraFixTime(1).Visible = True
                    End If
                Else
                    txtDummy(0).Visible = False
                    txtDummy(1).Visible = False
                    For i = 4 To 7
                        txtDummy(i).Visible = True
                    Next
                    fraFixTime(0).Visible = True
                End If
            Case Else
        End Select
    End If
End Sub

Private Sub optTimeZone_Click(Index As Integer)
    If optTimeZone(Index).Value = True Then
        Select Case Index
            Case 0  'UTC
                optTimeZone(0).BackColor = LightGreen
                optTimeZone(1).BackColor = vbButtonFace
                optTimeZone(0).Tag = "UTC"
            Case 1  'LOC
                optTimeZone(1).BackColor = LightGreen
                optTimeZone(0).BackColor = vbButtonFace
                optTimeZone(0).Tag = "LOC"
        End Select
        SetFilterFieldValues -1
    End If
End Sub

Private Sub ResetFilter_Click()
    Dim MyDateInputFormat As String
    Dim MyTimeInputFormat As String
    Dim tmpData As String
    Dim tmpTag As String
    Dim tmpTimeBgnUtc As String
    Dim tmpTimeEndUtc As String
    Dim tmpTimeBgnLoc As String
    Dim tmpTimeEndLoc As String
    Dim NowLoc
    Dim VpfrLoc
    Dim VpfrUtc
    Dim VptoLoc
    Dim VptoUtc
    If ResetFilter.Value = 1 Then
        If ButtonPushedInternal = False Then ResetFilterDefaults
        'NOW is supposed to be in Local Time
        tmpTag = fraTimeFilter.Tag
        If tmpTag = "AUTO" Then NowLoc = Now Else NowLoc = CedaFullDateToVb(tmpTag)
        If tmpTag <> "" Then
            VpfrLoc = DateAdd("d", Val(txtTimeOffset(2).Text), NowLoc)
            If chkFullUnits(0).Value = 0 Then VpfrLoc = DateAdd("h", Val(txtTimeOffset(0).Text), VpfrLoc)
            VpfrUtc = DateAdd("n", -UtcTimeDiff, VpfrLoc)
            tmpTimeBgnUtc = Format(VpfrUtc, "yyyymmddhhmm")
            tmpTimeBgnLoc = Format(VpfrLoc, "yyyymmddhhmm")
            
            'I found it better to calculate the
            'end time based on the begin time
            'when the user enters the begin time
            If tmpTag <> "AUTO" Then NowLoc = VpfrLoc
            VptoLoc = DateAdd("d", Val(txtTimeOffset(3).Text), NowLoc)
            If chkFullUnits(0).Value = 0 Then VptoLoc = DateAdd("h", Val(txtTimeOffset(1).Text), VptoLoc)
            VptoUtc = DateAdd("n", -UtcTimeDiff, VptoLoc)
            tmpTimeEndUtc = Format(VptoUtc, "yyyymmddhhmm")
            tmpTimeEndLoc = Format(VptoLoc, "yyyymmddhhmm")
            txtTifr(0).Tag = tmpTimeBgnUtc
            txtTifr(1).Tag = tmpTimeBgnLoc
            txtTito(0).Tag = tmpTimeEndUtc
            txtTito(1).Tag = tmpTimeEndLoc
            SetFilterFieldValues -1
        End If
        ResetFilter.Value = 0
    End If
End Sub
Private Sub ResetFilterDefaults()
    DontEvaluate = True
    fraTimeFilter.Caption = GetItem(fraTimeFilter.Caption, 1, "(") '& " (Based on Now)"
    fraTimeFilter.Tag = "AUTO"
    txtTimeOffset(0).Text = FilterCfgBgn
    txtTimeOffset(1).Text = FilterCfgEnd
    txtTimeOffset(2).Text = "0"
    txtTimeOffset(3).Text = "0"
    If FilterCfgChk = "Y" Then
        chkFullUnits(1).Value = 1
    Else
        chkFullUnits(1).Value = 0
    End If
    
    DontEvaluate = False
End Sub
Private Sub SetFilterFieldValues(ActTimeZone As Integer)
    SetFilterTifrValues ActTimeZone
    SetFilterTitoValues ActTimeZone
End Sub
Private Sub SetFilterTifrValues(ActTimeZone As Integer)
    Dim tmpData As String
    Dim TimeVal
    Dim i As Integer
    If ActTimeZone < 0 Then
        If optTimeZone(0).Value = True Then i = 0 Else i = 1
    Else
        i = ActTimeZone
    End If
    tmpData = txtTifr(i).Tag
    If tmpData <> "" Then
        TimeVal = CedaFullDateToVb(tmpData)
        DontCheck = True
        'Update the visible input fields
        txtDateFrom.Text = Format(TimeVal, MyDateInputFormat)
        txtTimeFrom(0).Text = Format(TimeVal, MyTimeInputFormat)
        txtTimeFrom(1).Text = Mid(tmpData, 9, 2)
        txtTifr(0).Text = Mid(tmpData, 1, 4)
        txtTifr(1).Text = Mid(tmpData, 5, 2)
        txtTifr(2).Text = Mid(tmpData, 7, 2)
        txtTifr(3).Text = Mid(tmpData, 9, 2)
        txtTifr(4).Text = Mid(tmpData, 11, 2)
        'Update the Filter Result (Must be UTC)
        If chkFullUnits(0).Value = 1 Then
            'Select Full Days (00:00-23:59)
            If optTimeZone(0).Value = True Then
                'When the user enters UTC we take and patch it:
                tmpData = txtTifr(0).Tag    'This is UTC Time
                Mid(tmpData, 9, 4) = "0000"
            Else
                'When the user enters LOC we patch it
                'and calculate the UTC value
                tmpData = txtTifr(1).Tag    'This is Local Time
                Mid(tmpData, 9, 4) = "0000"
                tmpData = CedaDateTimeAdd(tmpData, -UtcTimeDiff)
            End If
        ElseIf chkFullUnits(1).Value = 1 Then
            'Select Full Hours
            tmpData = txtTifr(0).Tag    'This is UTC Time
            Mid(tmpData, 11, 2) = "00"
        Else
            tmpData = txtTifr(0).Tag    'This is UTC Time
        End If
        tmpData = Left(tmpData, 12)
        txtTifr(2).Tag = tmpData
        DontCheck = False
    End If
End Sub
Private Sub SetFilterTitoValues(ActTimeZone As Integer)
    Dim tmpData As String
    Dim TimeVal
    Dim i As Integer
    If ActTimeZone < 0 Then
        If optTimeZone(0).Value = True Then i = 0 Else i = 1
    Else
        i = ActTimeZone
    End If
    tmpData = txtTito(i).Tag
    If tmpData <> "" Then
        TimeVal = CedaFullDateToVb(tmpData)
        DontCheck = True
        'Update the visible input fields
        txtDateTo.Text = Format(TimeVal, MyDateInputFormat)
        txtTimeTo(0).Text = Format(TimeVal, MyTimeInputFormat)
        txtTimeTo(1).Text = Mid(tmpData, 9, 2)
        txtTito(0).Text = Mid(tmpData, 1, 4)
        txtTito(1).Text = Mid(tmpData, 5, 2)
        txtTito(2).Text = Mid(tmpData, 7, 2)
        txtTito(3).Text = Mid(tmpData, 9, 2)
        txtTito(4).Text = Mid(tmpData, 11, 2)
        'Update the Filter Result (Must be UTC)
        If chkFullUnits(0).Value = 1 Then
            'Select Full Days (00:00-23:59)
            If optTimeZone(0).Value = True Then
                'When the user enters UTC we take and patch it:
                tmpData = txtTito(0).Tag    'This is UTC Time
                Mid(tmpData, 9, 4) = "2359"
            Else
                'When the user enters LOC we patch it
                'and calculate the UTC value
                tmpData = txtTito(1).Tag    'This is Local Time
                Mid(tmpData, 9, 4) = "2359"
                tmpData = CedaDateTimeAdd(tmpData, -UtcTimeDiff)
            End If
        ElseIf chkFullUnits(1).Value = 1 Then
            'Select Full Hours
            tmpData = txtTito(0).Tag    'This is UTC Time
            Mid(tmpData, 11, 2) = "00"
        Else
            tmpData = txtTito(0).Tag    'This is UTC Time
        End If
        tmpData = Left(tmpData, 12)
        txtTito(2).Tag = tmpData
        DontCheck = False
    End If
End Sub

Private Sub RightFilterTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpBuff As String
    If LineNo >= 0 Then
        tmpBuff = RightFilterTab(Index).GetLineValues(LineNo)
        LeftFilterTab(Index).InsertTextLine tmpBuff, False
        RightFilterTab(Index).DeleteLine LineNo
        LeftFilterTab(Index).Sort "0", True, True
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        CheckRefreshSettings
    End If
End Sub

Private Sub txtDateFrom_Change()
    CheckInputDateTimeFrom
End Sub

Private Sub CheckInputDateTimeFrom()
    Dim tmpText As String
    Dim tmpDate As String
    Dim tmpTime As String
    Dim tmpCedaDateTime As String
    Dim DateIsOK As Boolean
    Dim TimeIsOK As Boolean
    Dim i As Integer
    If DontCheck = False Then
        DateIsOK = False
        TimeIsOK = False
        tmpText = txtDateFrom.Text
        tmpDate = DateInputFormatToCeda(tmpText, MyDateInputFormat)
        If tmpDate <> "" Then
            DateIsOK = True
            txtDateFrom.BackColor = LightestYellow
            txtDateFrom.ForeColor = vbBlack
            If chkFullUnits(0).Value = 0 Then
                If chkFullUnits(1).Value = 0 Then
                    i = 0
                    tmpTime = txtTimeFrom(i).Text
                Else
                    i = 1
                    tmpTime = txtTimeFrom(i).Text & "00"
                End If
                tmpTime = Replace(tmpTime, ":", "", 1, -1, vbBinaryCompare)
                TimeIsOK = CheckTimeValues(tmpTime, True, True)
                If TimeIsOK = True Then
                    txtTimeFrom(i).BackColor = LightestYellow
                    txtTimeFrom(i).ForeColor = vbBlack
                Else
                    txtTimeFrom(i).BackColor = vbRed
                    txtTimeFrom(i).ForeColor = vbWhite
                End If
            Else
                TimeIsOK = True
                tmpTime = txtTimeFrom(0).Text
                tmpTime = Replace(tmpTime, ":", "", 1, -1, vbBinaryCompare)
            End If
        Else
            txtDateFrom.BackColor = vbRed
            txtDateFrom.ForeColor = vbWhite
        End If
        If (DateIsOK = True) And (TimeIsOK = True) Then
            tmpTime = Replace(tmpTime, ":", "", 1, -1, vbBinaryCompare)
            tmpCedaDateTime = tmpDate & tmpTime
            'If CheckValidCedaTime(tmpCedaDateTime) = True Then
                fraTimeFilter.Caption = GetItem(fraTimeFilter.Caption, 1, "(") & " (Individual)"
                DontEvaluate = True
                txtTimeOffset(0).Text = "0"
                txtTimeOffset(2).Text = "0"
                DontEvaluate = False
                If optTimeZone(0).Value = True Then
                    'The user enters UTC time
                    txtTifr(0).Tag = tmpCedaDateTime
                    txtTifr(1).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(UtcTimeDiff))
                Else
                    'The user enters LOC time
                    txtTifr(0).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(-UtcTimeDiff))
                    txtTifr(1).Tag = tmpCedaDateTime
                End If
                fraTimeFilter.Tag = txtTifr(1).Tag
                ButtonPushedInternal = True
                ResetFilter.Value = 1
                ButtonPushedInternal = False
                AdjustFilterButtons True
            'End If
        Else
            txtTifr(0).Tag = ""
            txtTifr(1).Tag = ""
            fraTimeFilter.Tag = ""
            AdjustFilterButtons False
        End If
    End If
End Sub

Private Sub CheckInputDateTimeTo()
    Dim tmpText As String
    Dim tmpData As String
    Dim tmpDate As String
    Dim tmpTime As String
    Dim tmpCedaDateTime As String
    Dim DateIsOK As Boolean
    Dim TimeIsOK As Boolean
    Dim i As Integer
    Dim TimeDiff As Long
    Dim NewVal As Long
    Dim UtcVpfr
    Dim UtcVpto
    If DontCheck = False Then
        DateIsOK = False
        TimeIsOK = False
        tmpText = txtDateTo.Text
        tmpDate = DateInputFormatToCeda(tmpText, MyDateInputFormat)
        If tmpDate <> "" Then
            DateIsOK = True
            txtDateTo.BackColor = LightestYellow
            txtDateTo.ForeColor = vbBlack
            If chkFullUnits(0).Value = 0 Then
                If chkFullUnits(1).Value = 0 Then
                    i = 0
                    tmpTime = txtTimeTo(i).Text
                Else
                    i = 1
                    tmpTime = txtTimeTo(i).Text & "00"
                End If
                tmpTime = Replace(tmpTime, ":", "", 1, -1, vbBinaryCompare)
                TimeIsOK = CheckTimeValues(tmpTime, True, True)
                If TimeIsOK = True Then
                    txtTimeTo(i).BackColor = LightestYellow
                    txtTimeTo(i).ForeColor = vbBlack
                Else
                    txtTimeTo(i).BackColor = vbRed
                    txtTimeTo(i).ForeColor = vbWhite
                End If
            Else
                TimeIsOK = True
                tmpTime = txtTimeTo(0).Text
                tmpTime = Replace(tmpTime, ":", "", 1, -1, vbBinaryCompare)
            End If
        Else
            txtDateTo.BackColor = vbRed
            txtDateTo.ForeColor = vbWhite
            DateIsOK = False
        End If
        If (DateIsOK = True) And (TimeIsOK = True) Then
            tmpTime = Replace(tmpTime, ":", "", 1, -1, vbBinaryCompare)
            tmpCedaDateTime = tmpDate & tmpTime
            'If CheckValidCedaTime(tmpCedaDateTime) = True Then
                fraTimeFilter.Caption = GetItem(fraTimeFilter.Caption, 1, "(") & " (Individual)"
                
                If optTimeZone(0).Value = True Then
                    'The user enters UTC time
                    txtTito(0).Tag = tmpCedaDateTime
                    txtTito(1).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(UtcTimeDiff))
                Else
                    'The user enters LOC time
                    txtTito(0).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(-UtcTimeDiff))
                    txtTito(1).Tag = tmpCedaDateTime
                End If
                fraTimeFilter.Caption = GetItem(fraTimeFilter.Caption, 1, "(") & " (Individual)"
                fraTimeFilter.Tag = txtTifr(1).Tag
                DontEvaluate = True
                txtTimeOffset(0).Text = "0"
                txtTimeOffset(2).Text = "0"
                tmpCedaDateTime = txtTifr(0).Tag
                UtcVpfr = CedaFullDateToVb(tmpCedaDateTime)
                tmpCedaDateTime = txtTito(0).Tag
                UtcVpto = CedaFullDateToVb(tmpCedaDateTime)
                TimeDiff = DateDiff("h", UtcVpfr, UtcVpto)
                NewVal = TimeDiff Mod 24
                If NewVal > 0 Then tmpData = "+" & CStr(NewVal) Else tmpData = CStr(NewVal)
                txtTimeOffset(1).Text = tmpData
                NewVal = (TimeDiff - NewVal) \ 24
                If NewVal > 0 Then tmpData = "+" & CStr(NewVal) Else tmpData = CStr(NewVal)
                txtTimeOffset(3).Text = tmpData
                DontEvaluate = False
                
                ButtonPushedInternal = True
                ResetFilter.Value = 1
                ButtonPushedInternal = False
                AdjustFilterButtons True
            'End If
        Else
            txtTito(0).Tag = ""
            txtTito(1).Tag = ""
            AdjustFilterButtons False
        End If
    End If
End Sub

Private Sub txtDateFrom_GotFocus()
    SetTextSelected
End Sub

Private Sub txtDateFrom_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(".-/", Chr(KeyAscii)) > 0 Then
            If InStr(MyDateInputFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtDateTo_GotFocus()
    SetTextSelected
End Sub

Private Sub txtDateTo_KeyPress(KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(".-/", Chr(KeyAscii)) > 0 Then
            If InStr(MyDateInputFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtFilterVal_Change(Index As Integer)
    SearchInList Index, 1
End Sub

Private Sub txtFilterVal_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim LineNo As Long
    Dim HitText As String
    If KeyAscii = 13 Then
        LineNo = LeftFilterTab(Index).GetCurrentSelected
        If LineNo >= 0 Then
            HitText = LeftFilterTab(Index).GetColumnValue(LineNo, 0)
            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then
                LeftFilterTab_SendLButtonDblClick Index, LineNo, 0
                SetTextSelected
            End If
        End If
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub txtTifr_Change(Index As Integer)
    Dim tmpCedaDateTime As String
    Dim i As Integer
    On Error Resume Next
    'TimeZone Handling:
    'Tifr(0).tag keeps the UTC value
    'Tifr(1).tag keeps the LOC value
    'Tifr(2).tag keeps the UTC Filter Result
    If DontCheck = False Then
        tmpCedaDateTime = ""
        For i = 0 To 4
            tmpCedaDateTime = tmpCedaDateTime & Trim(txtTifr(i).Text)
        Next
        If CheckValidCedaTime(tmpCedaDateTime) = True Then
            fraTimeFilter.Caption = GetItem(fraTimeFilter.Caption, 1, "(") & " (Individual)"
            DontEvaluate = True
            txtTimeOffset(0).Text = "0"
            txtTimeOffset(2).Text = "0"
            DontEvaluate = False
            If optTimeZone(0).Value = True Then
                'The user enters UTC time
                txtTifr(0).Tag = tmpCedaDateTime
                txtTifr(1).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(UtcTimeDiff))
            Else
                'The user enters LOC time
                txtTifr(0).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(-UtcTimeDiff))
                txtTifr(1).Tag = tmpCedaDateTime
            End If
            For i = 0 To 4
                txtTifr(i).BackColor = LightestYellow
                txtTifr(i).ForeColor = vbBlack
            Next
            fraTimeFilter.Tag = txtTifr(1).Tag
            ButtonPushedInternal = True
            ResetFilter.Value = 1
            ButtonPushedInternal = False
            AdjustFilterButtons True
            If Len(txtTifr(Index).Text) = txtTifr(Index).MaxLength Then
                If DontJump = False Then
                    If Index < 4 Then
                        txtTifr(Index + 1).SetFocus
                    Else
                        txtTito(0).SetFocus
                    End If
                End If
            End If
        Else
            txtTifr(0).Tag = ""
            txtTifr(1).Tag = ""
            fraTimeFilter.Tag = ""
            txtTifr(Index).BackColor = vbRed
            txtTifr(Index).ForeColor = vbWhite
            AdjustFilterButtons False
        End If
    Else
        If ButtonPushedInternal = False Then
            fraTimeFilter.Caption = GetItem(fraTimeFilter.Caption, 1, "(") '& " (Based on Now)"
            fraTimeFilter.Tag = "AUTO"
        End If
        For i = 0 To 4
            txtTifr(i).BackColor = LightestYellow
            txtTifr(i).ForeColor = vbBlack
        Next
        AdjustFilterButtons True
    End If
    If (txtTifr(0).Tag = "") Or (txtTito(0).Tag = "") Then
        optTimeZone(0).Enabled = False
        optTimeZone(1).Enabled = False
    Else
        optTimeZone(0).Enabled = True
        optTimeZone(1).Enabled = True
    End If
End Sub

Private Sub txtTifr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtTifr_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0
End Sub

Private Sub txtTimeFrom_Change(Index As Integer)
    CheckInputDateTimeFrom
End Sub

Private Sub txtTimeFrom_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtTimeFrom_KeyPress(Index As Integer, KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(":", Chr(KeyAscii)) > 0 Then
            If InStr(MyTimeInputFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtTimeOffset_Change(Index As Integer)
    If DontEvaluate = False Then
        ButtonPushedInternal = True
        ResetFilter.Value = 1
        ButtonPushedInternal = False
    End If
End Sub

Private Sub txtTimeTo_Change(Index As Integer)
    CheckInputDateTimeTo
End Sub

Private Sub txtTimeTo_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtTimeTo_KeyPress(Index As Integer, KeyAscii As Integer)
    If (KeyAscii <> 8) Then
        If InStr(":", Chr(KeyAscii)) > 0 Then
            If InStr(MyTimeInputFormat, Chr(KeyAscii)) = 0 Then KeyAscii = 0
        Else
            If Not IsNumeric(Chr(KeyAscii)) Then KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtTito_Change(Index As Integer)
    Dim tmpCedaDateTime As String
    Dim tmpData As String
    Dim i As Integer
    Dim NewVal As Long
    Dim TimeDiff As Long
    Dim UtcVpfr
    Dim UtcVpto
    On Error Resume Next
    'TimeZone Handling:
    'Tito(0).tag keeps the UTC value
    'Tito(1).tag keeps the LOC value
    'Tito(2).tag keeps the UTC Filter Result
    If DontCheck = False Then
        tmpCedaDateTime = ""
        For i = 0 To 4
            tmpCedaDateTime = tmpCedaDateTime & Trim(txtTito(i).Text)
        Next
        If CheckValidCedaTime(tmpCedaDateTime) = True Then
            If optTimeZone(0).Value = True Then
                'The user enters UTC time
                txtTito(0).Tag = tmpCedaDateTime
                txtTito(1).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(UtcTimeDiff))
            Else
                'The user enters LOC time
                txtTito(0).Tag = CedaDateTimeAdd(tmpCedaDateTime, CLng(-UtcTimeDiff))
                txtTito(1).Tag = tmpCedaDateTime
            End If
            For i = 0 To 4
                txtTito(i).BackColor = LightestYellow
                txtTito(i).ForeColor = vbBlack
            Next
            fraTimeFilter.Caption = GetItem(fraTimeFilter.Caption, 1, "(") & " (Individual)"
            fraTimeFilter.Tag = txtTifr(1).Tag
            DontEvaluate = True
            txtTimeOffset(0).Text = "0"
            txtTimeOffset(2).Text = "0"
            tmpCedaDateTime = txtTifr(0).Tag
            UtcVpfr = CedaFullDateToVb(tmpCedaDateTime)
            tmpCedaDateTime = txtTito(0).Tag
            UtcVpto = CedaFullDateToVb(tmpCedaDateTime)
            TimeDiff = DateDiff("h", UtcVpfr, UtcVpto)
            NewVal = TimeDiff Mod 24
            If NewVal > 0 Then tmpData = "+" & CStr(NewVal) Else tmpData = CStr(NewVal)
            txtTimeOffset(1).Text = tmpData
            NewVal = (TimeDiff - NewVal) \ 24
            If NewVal > 0 Then tmpData = "+" & CStr(NewVal) Else tmpData = CStr(NewVal)
            txtTimeOffset(3).Text = tmpData
            DontEvaluate = False
            ButtonPushedInternal = True
            ResetFilter.Value = 1
            ButtonPushedInternal = False
            AdjustFilterButtons True
            If Len(txtTito(Index).Text) = txtTito(Index).MaxLength Then
                If DontJump = False Then
                    If Index < 4 Then
                        txtTito(Index + 1).SetFocus
                    Else
                        txtTito(0).SetFocus
                    End If
                End If
            End If
        Else
            txtTito(0).Tag = ""
            txtTito(1).Tag = ""
            txtTito(Index).BackColor = vbRed
            txtTito(Index).ForeColor = vbWhite
            AdjustFilterButtons False
        End If
    Else
        For i = 0 To 4
            txtTito(i).BackColor = LightestYellow
            txtTito(i).ForeColor = vbBlack
        Next
        AdjustFilterButtons True
    End If
    If (txtTifr(0).Tag = "") Or (txtTito(0).Tag = "") Then
        optTimeZone(0).Enabled = False
        optTimeZone(1).Enabled = False
    Else
        optTimeZone(0).Enabled = True
        optTimeZone(1).Enabled = True
    End If
End Sub

Private Sub txtTito_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub AdjustFilterButtons(SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            chkWork(i).Enabled = SetEnable
        End If
    Next
    AdjustApplButton "AODB_FILTER", SetEnable
    AdjustApplButton "FILE_FILTER", SetEnable
    AdjustApplButton "TOGGLE", SetEnable
    If SetEnable = True Then
        CheckRefreshSettings
    Else
        AdjustApplButton "AODBREFRESH", SetEnable
        AdjustApplButton "FILEREFRESH", SetEnable
    End If
End Sub
Private Sub AdjustApplButton(KeyCode As String, SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To chkAppl.UBound
        If chkAppl(i).Tag = KeyCode Then
            chkAppl(i).Enabled = SetEnable
            Exit For
        End If
    Next
End Sub
Private Sub CheckRefreshSettings()
    'Dim tmpTimeFrame As String
    Dim tmpTag As String
    Dim FilterList As String
    Dim ChkValue As Boolean
    Dim i As Integer
    ChkValue = False
    HandleAodbFilter chkWork(0), False, "CRS"
    FilterList = MyGlobalResult
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            If chkWork(i).Value = 1 Then
                tmpTag = chkWork(i).Tag
                If tmpTag <> FilterList Then
                    ChkValue = True
                    Exit For
                End If
            End If
        End If
    Next
    'QuickHack
    ChkValue = True
    AdjustApplButton "AODBREFRESH", ChkValue
    AdjustApplButton "FILEREFRESH", ChkValue
End Sub

Private Sub SearchInList(UseIndex As Integer, Method As Integer)
    Static LastIndex As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColNbr As String
    Dim Index As Integer
    Dim UseCol As Long
    On Error Resume Next
    Index = UseIndex
    If Index < 0 Then Index = LastIndex
    LastIndex = Index
    tmpText = Trim(txtFilterVal(Index).Text)
    UseCol = 0
    If tmpText <> "" Then
        LoopCount = 0
        Do
            HitLst = LeftFilterTab(Index).GetNextLineByColumnValue(UseCol, tmpText, Method)
            HitRow = Val(HitLst)
            If HitLst <> "" Then
                If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                LeftFilterTab(Index).OnVScrollTo NewScroll
            Else
                LeftFilterTab(Index).SetCurrentSelection -1
                LeftFilterTab(Index).OnVScrollTo 0
            End If
            LoopCount = LoopCount + 1
        Loop While LoopCount < 2 And HitLst = ""
        LeftFilterTab(Index).SetCurrentSelection HitRow
    End If
    txtFilterVal(Index).SetFocus
End Sub


Private Sub txtTito_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0
End Sub
'/****************************\
'/Bind the Filter Detail Name By User \
'/Phyoe
Private Sub BindFilterList()
    Dim sTableName As String
    Dim sFieldNames As String
    Dim sCondition As String
    Dim sResult As String
    Dim sOrderBy As String
    Dim ilRC As Integer
    Dim NoOfLines As Integer
    Dim i As Integer
    cboSavedFilters.Clear
    
    sTableName = "VCDTAB"
    sFieldNames = "DISTINCT CKEY, URNO"
    sCondition = "WHERE PKNO = '" & RTrim(LTrim(LoginUserName)) & "' AND APPN = 'UFIS-FDT'"
    sOrderBy = "CKEY"
    
    If CedaIsConnected Then
        ilRC = UfisServer.CallCeda(sResult, "RT", sTableName, sFieldNames, "", sCondition, sOrderBy, 0, True, False)
    End If
    
    Dim arrRecords As Variant
    arrRecords = Split(sResult, vbNewLine)
    NoOfLines = UBound(arrRecords) + 1
    
    cboSavedFilters.AddItem "< Today >"
    cboSavedFilters.ListIndex = 0
    
    If ilRC = 0 Then
        For i = 0 To NoOfLines - 1
            cboSavedFilters.AddItem GetItem(CStr(arrRecords(i)), 1, ",")
            If sFilterName = GetItem(CStr(arrRecords(i)), 1, ",") Then
                iFilterIdex = i + 1
            End If
            cboSavedFilters.ItemData(cboSavedFilters.NewIndex) = GetItem(CStr(arrRecords(i)), 2, ",")
        Next i
    End If
    'Assign the current load filter name to combo box
    If iFilterIdex > 0 Then
        cboSavedFilters.ListIndex = iFilterIdex
        'cboSavedFilters_Click
    End If
End Sub
'//Delete existing filter detail before update it
Private Function DeleteExistingFilter(sCKey As String, lURNO As Long) As Boolean
    Dim sTableName As String
    Dim sFieldNames As String
    Dim sCondition As String
    Dim sResult As String
    Dim sOrderBy As String
    Dim ilRC As Integer
    Dim NoOfLines As Integer
    Dim i As Integer
    cboSavedFilters.Clear
    
    sTableName = "VCDTAB"
    sCondition = "WHERE URNO = " & lURNO & " AND CKEY = '" & Trim(sCKey) & "'"
    sOrderBy = ""
    ilRC = UfisServer.CallCeda(sResult, "DRT", sTableName, sFieldNames, "", sCondition, "", 0, True, False)
    
    If ilRC = 0 Then
        DeleteExistingFilter = True
    Else
        DeleteExistingFilter = False
    End If
       
End Function
'// Retrieve Last filtered Text File
Private Function GetFilteredTextFileByFileName(sCKey As String, lURNO As Long) As Boolean
    Dim sResult As String
    Dim sTableName As String
    Dim sFieldName As String
    Dim sCondition As String
    Dim sOrder As String
    Dim iReturnValue As Integer
    Dim NoOfLines As Integer
    
    Dim MainFile As String
    Dim myLoadPath As String
    Dim myLoadName As String
    Dim tmpFileName As String
    Dim DatFileSize As Long
    Dim DatFileData As String
    Dim DatFileName As String
    Dim tmpFile As String
    Dim KPass As String
    Dim fn As Integer
    Dim ffn As Integer
    Dim i As Integer
    
    sTableName = "VCDTAB"
    sFieldName = "TEXT"
    sCondition = "WHERE CKEY = '" & Trim(sCKey) & "' AND URNO = " & lURNO
    sOrder = "CKEY"
        
    iReturnValue = UfisServer.CallCeda(sResult, "RT", sTableName, sFieldName, "", sCondition, sOrder, 0, True, False)
    
    Dim arrRecords As Variant
    arrRecords = Split(sResult, vbNewLine)
    NoOfLines = UBound(arrRecords) + 1
    
    If iReturnValue = 0 Then
        'Temp File Path
        myLoadPath = UFIS_TMP
        'Temp File Path + Name
        myLoadName = ApplMainCode & "_" & ApplFuncCode & "_Filter"
        tmpFileName = myLoadPath & "\" & myLoadName
        MainFile = myLoadPath & "\" & myLoadName & "_All.usf"
        FilterFileName = MainFile
        RestoreFilter = False
        tmpFile = Dir(FilterFileName)
        If tmpFile <> "" Then Kill FilterFileName
        ffn = FreeFile
        Open MainFile For Binary As #ffn
        DatFileData = ""
        
        For i = 0 To NoOfLines - 1
            DatFileData = CleanString(CStr(arrRecords(i)), FOR_CLIENT, False)
            Put #ffn, , DatFileData
        Next i
        Close #ffn
        GetFilteredTextFileByFileName = True
    Else
        GetFilteredTextFileByFileName = False
    End If
    
End Function
