Attribute VB_Name = "GanttTasks"
Option Explicit
Public Sub ClearAllGanttCharts()
    Dim i As Integer
    For i = 0 To MainDialog.chkTabIsVisible.UBound
        MainDialog.TabGantt(i).ResetContent
    Next
End Sub
Public Sub CreateAllGanttCharts()
    Static IsToggled As Boolean
    Dim tmpTxt1 As String
    Dim tmpTxt2 As String
    Dim tmpCfg As String
    Dim i As Integer
    Dim BgnTime
    tmpTxt1 = "Creating Gantt Charts"
    tmpTxt2 = "Start ...."
    MainDialog.MsgPanelShow 0, tmpTxt1, tmpTxt2, -1, -1, True
    'MainDialog.ResizeSidePanel 1
    MainDialog.WaitSidePanel 1, 1, ""
    CreateTabIndex TabArrFlightsIdx, TabArrFlightsTab, "INDEXES_IMP", "RKEY", True
    CreateTabIndex TabArrTowingIdx, TabArrTowingTab, "INDEXES_IMP", "RKEY", True
    CreateTabIndex TabDepFlightsIdx, TabDepFlightsTab, "INDEXES_IMP", "RKEY", True
    For i = 0 To MainDialog.chkTabIsVisible.UBound
        tmpTxt2 = MainDialog.lblTabCaption(i).Caption
        MainDialog.MsgPanelShow 0, tmpTxt1, tmpTxt2, -1, -1, True
        CreateUGanttView i, True
        If Not IsToggled Then
            tmpCfg = MainDialog.GetTabProperty(i, "GANTT_CHART", 1, "")
            If tmpCfg = "YES" Then
                MainDialog.ArrangeToolPanel i, 4
            End If
        End If
    Next
    'BgnTime = Timer
    tmpTxt2 = "Position Gantt Chart"
    MainDialog.MsgPanelShow 0, tmpTxt1, tmpTxt2, -1, -1, True
    GanttChartPos.CreateMyChartView
    tmpTxt2 = "Belt Gantt Chart"
    MainDialog.MsgPanelShow 0, tmpTxt1, tmpTxt2, -1, -1, True
    GanttChartBelt.CreateMyChartView
    'MsgBox Timer - BgnTime
    MainDialog.WaitSidePanel 1, 2, ""
    IsToggled = True
End Sub
Public Sub SaveGanttChartPictures(ForWhat As String, UseContext As String)
    Dim tmpCfg As String
    Dim NewTip As String
    Dim BgnTime
    Dim UsedTime
    Dim i As Integer
    If Not ChartAutoSaveIsBusy Then
        ChartAutoSaveIsBusy = True
        BgnTime = Timer
        If ForWhat = "TIST" Then
            MainDialog.SaveChartSnapshot -1, ForWhat, "", UseContext
        End If
        For i = 0 To MainDialog.chkTabIsVisible.UBound
            tmpCfg = MainDialog.GetTabProperty(i, "GANTT_IMAGE", 1, "")
            If tmpCfg <> "" Then
                MainDialog.SaveChartSnapshot i, "SNAP", tmpCfg, UseContext
            End If
        Next
        MainDialog.SaveChartSnapshot -TabDepFlightsIdx, "POS", "BAYS", UseContext
        MainDialog.SaveChartSnapshot -TabArrBeltsIdx, "BLT", "BELT", UseContext
        
        tmpCfg = GetTimeStamp(0)
        NewTip = "UFIS Cappuccino" & vbLf
        NewTip = NewTip & UseContext & ": " & Mid(tmpCfg, 9, 2) & ":" & Mid(tmpCfg, 11, 2) & "h"
        UsedTime = Timer - BgnTime
        tmpCfg = CStr(UsedTime)
        tmpCfg = GetItem(tmpCfg, 1, ".")
        NewTip = NewTip & " (" & tmpCfg & "s)"
        NewTip = NewTip & vbLf
        NewTip = NewTip & "TIME MODE: " & CurrentTimeCode
        MainSysTray.ModifyIconInTray 0, NewTip
        ChartAutoSaveIsBusy = False
    End If
End Sub
Public Sub CreateUGanttView(Index As Integer, FullReset As Boolean)
    Dim MaxLine As Long
    Dim CreateThisGantt As Boolean
    If CreateGanttOnDemandOnly Then CreateThisGantt = False Else CreateThisGantt = True
    If FullReset Then MainDialog.TabGantt(Index).ResetContent
    MaxLine = MainDialog.TabGantt(Index).GetTotalBarCount()
    If MaxLine <= 0 Then
        InitThisGantt Index, MainDialog.TabGantt(Index)
        If CreateThisGantt Then
            CreateGanttLeftScale Index, MainDialog.TabGantt(Index), MainDialog.FileData(Index), ""
            
            CreateLineSeparators Index, MainDialog.TabGantt(Index), MainDialog.FileData(Index), -1, False
            'CreateBackgroundBars Index, MainDialog.TabGantt(Index), MainDialog.FileData(Index), "FLNO,STOD,DES3"
            
            CreateFlightBars Index, MainDialog.TabGantt(Index), MainDialog.FileData(Index), "FLNO,STOD,DES3"
            CreateFlightDecoObjects Index, MainDialog.TabGantt(Index), MainDialog.FileData(Index), "FLNO,STOD,DES3"
        End If
    End If
End Sub
    

Public Sub InitThisGantt(Index As Integer, CurGantt As UGANTTLib.UGantt)
    Dim GanttRange As String
    Dim strTimeFrameFrom As String
    Dim strTimeFrameTo As String
    Dim datTF As Date
    Dim datTT As Date
    Dim TFFrom As Date
    Dim TFTo As Date
    'Setting default layout and time frame
    CurGantt.ResetContent
    CurGantt.TabHeaderLengthString = "100"
    CurGantt.TabSetHeaderText ("Left Scale Grid")
    CurGantt.SplitterPosition = 100
    CurGantt.TabBodyFontName = "Arial"
    CurGantt.TabFontSize = 14
    CurGantt.TabHeaderFontName = "Arial"
    CurGantt.TabHeaderFontSize = 16
    CurGantt.TimeScaleHeaderHeight = 40
    CurGantt.BarOverlapOffset = 18
    CurGantt.BarNumberExpandLine = 0
    CurGantt.TabBodyFontBold = True
    CurGantt.TabHeaderFontBold = True
    CurGantt.LifeStyle = True
    GanttRange = MainDialog.GetTabProperty(Index, "GANTT_RANGE", 1, "")
    
    'DataFilterVpfr, DataFilterVpto
    If DataFilterVpfr <> "" Then
        strTimeFrameFrom = DataFilterVpfr
        strTimeFrameTo = DataFilterVpto
        TFFrom = CedaFullDateToVb(strTimeFrameFrom)
        TFFrom = DateAdd("d", -1, TFFrom)
        TFTo = CedaFullDateToVb(strTimeFrameTo)
        TFTo = DateAdd("d", 1, TFTo)
    Else
        strTimeFrameFrom = GetTimeStamp(0)
        strTimeFrameTo = GetTimeStamp(0)
        TFFrom = CedaFullDateToVb(strTimeFrameFrom)
        TFFrom = DateAdd("d", -1, TFFrom)
        TFTo = CedaFullDateToVb(strTimeFrameTo)
        TFTo = DateAdd("d", 1, TFTo)
    End If
    TFFrom = DateAdd("n", UtcTimeDisp, TFFrom)
    TFTo = DateAdd("n", UtcTimeDisp, TFTo)
    CurGantt.TimeFrameFrom = TFFrom
    CurGantt.TimeFrameTo = TFTo
    CurGantt.TabLineHeight = 20
    MainDialog.GanttSlider(Index).Value = Val(GanttRange)
    CurGantt.TimeScaleDuration = MainDialog.GanttSlider(Index).Value
    CreateTimeSeparators Index, CurGantt, ""
End Sub
Public Sub CreateGanttLeftScale(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, UseTabFields As String)
    Dim TabFieldLst As String
    Dim TabHeadStrg As String
    Dim TabHeadSize As String
    Dim GttHeadStrg As String
    Dim GttHeadSize As String
    Dim GanttScale As String
    Dim TabFields As String
    Dim TabFldName As String
    Dim TabFldHead As String
    Dim TabFldData As String
    Dim TabFldSize As String
    Dim TabUrno As String
    Dim TabFlno As String
    Dim GttLeftRec As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LeftWidth As Long
    Dim FldColNo As Long
    Dim Itm As Integer
    Dim CheckFlno As Boolean
    GanttScale = UseTabFields
    If GanttScale = "" Then
        GanttScale = MainDialog.GetTabProperty(Index, "GANTT_SCALE", 1, "")
    End If
    TabFields = GanttScale
    TabFieldLst = DataTab.LogicalFieldList
    TabHeadStrg = DataTab.HeaderString
    TabHeadSize = DataTab.HeaderLengthString
    GttHeadStrg = ""
    GttHeadSize = ""
    LeftWidth = 0
    Itm = 0
    TabFldName = "START"
    While TabFldName <> ""
        Itm = Itm + 1
        TabFldName = GetItem(TabFields, Itm, ",")
        If TabFldName <> "" Then
            FldColNo = GetRealItemNo(TabFieldLst, TabFldName)
            If FldColNo >= 0 Then
                TabFldHead = GetRealItem(TabHeadStrg, FldColNo, ",")
                If InStr(MainDialog.ListOfCedaTimeFields, TabFldName) > 0 Then
                    If InStr("STOD,STOA", TabFldName) > 0 Then
                        TabFldSize = "83"
                        TabFldHead = TabFldHead & " <" & CurrentTimeCode & ">"
                    Else
                        TabFldSize = "32"
                    End If
                ElseIf InStr("REGN", TabFldName) > 0 Then
                    TabFldSize = "50"
                Else
                    TabFldSize = GetRealItem(TabHeadSize, FldColNo, ",")
                End If
                GttHeadSize = GttHeadSize & TabFldSize & ","
                LeftWidth = LeftWidth + Val(TabFldSize)
                GttHeadStrg = GttHeadStrg & TabFldHead & ","
            End If
        End If
    Wend
    If GttHeadStrg <> "" Then
        GttHeadStrg = GttHeadStrg & "Remark"
        GttHeadSize = GttHeadSize & "100"
        CurGantt.ResetContent
        CurGantt.TabHeaderLengthString = GttHeadSize
        CurGantt.TabSetHeaderText GttHeadStrg
        CurGantt.SplitterPosition = LeftWidth
        MainDialog.GanttSlider(Index).Width = (LeftWidth + 8) * 15
        MainDialog.GanttIcon(Index).Left = (LeftWidth + 6) * 15
        CurGantt.Refresh
        If InStr(TabFields, "FLNO") > 0 Then CheckFlno = True Else CheckFlno = False
        MaxLine = DataTab.GetLineCount
        CurGantt.TabLines = 0
        MaxLine = MaxLine - 1
        For CurLine = 0 To MaxLine
            TabUrno = DataTab.GetFieldValue(CurLine, "URNO")
            If CheckFlno Then
                TabFlno = Trim(DataTab.GetFieldValue(CurLine, "FLNO"))
                If TabFlno = "" Then
                    If TabFlno = "" Then TabFlno = Trim(DataTab.GetFieldValue(CurLine, "CSGN"))
                    If TabFlno = "" Then TabFlno = Trim(DataTab.GetFieldValue(CurLine, "REGN"))
                    If TabFlno = "" Then TabFlno = "N/A"
                    TabFlno = "[" & TabFlno & "]"
                    DataTab.SetFieldValues CurLine, "FLNO", TabFlno
                End If
            End If
            GttLeftRec = DataTab.GetFieldValues(CurLine, TabFields)
            GttLeftRec = AutoFormatFieldValues(TabFields, GttLeftRec, "hh:mm / dd.mm.yy", "hh:mm")
            GttLeftRec = GttLeftRec & ", "
            CurGantt.AddLineAt CurLine, TabUrno, GttLeftRec
        Next
        CurGantt.Refresh
    End If
End Sub
Public Function AutoFormatFieldValues(UseFields As String, UseData As String, UseSkedFormat As String, UseAnyFormat As String) As String
    Dim NewData As String
    Dim FldName As String
    Dim FldData As String
    Dim SkedFormat As String
    Dim TimeFormat As String
    Dim TimeValue As Date
    Dim i As Integer
    'Quick Hack
    'SkedFormat = "hh:mm / dd mmm yyyy"
    SkedFormat = "hh:mm / dd.mm.yy"
    TimeFormat = "hh:mm"
    NewData = ""
    i = 0
    FldName = "START"
    While FldName <> ""
        i = i + 1
        FldName = GetItem(UseFields, i, ",")
        If FldName <> "" Then
            FldData = GetItem(UseData, i, ",")
            If FldData <> "" Then
                If InStr(MainDialog.ListOfCedaTimeFields, FldName) > 0 Then
                    TimeValue = CedaFullDateToVb(FldData)
                    TimeValue = DateAdd("n", UtcTimeDisp, TimeValue)
                    If InStr("STOD,STOA", FldName) > 0 Then
                        FldData = Format(TimeValue, SkedFormat)
                    Else
                        FldData = Format(TimeValue, TimeFormat)
                    End If
                    FldData = UCase(FldData)
                End If
            End If
            NewData = NewData & FldData & ","
        End If
    Wend
    AutoFormatFieldValues = NewData
End Function
Public Sub CreateFlightBars(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, UseTabFields As String)
    Dim TabFields As String
    Dim TxtGroups As String
    Dim BarLTextCfg As String
    Dim BarRTextCfg As String
    Dim BarBgnText As String
    Dim BarEndText As String
    Dim BarGroups As String
    Dim BarFields As String
    Dim BarUrno As String
    Dim BarAurn As String
    Dim BarAdid As String
    Dim BarFtyp As String
    Dim BarRkey As String
    Dim BarStat As String
    Dim FldProp As String
    Dim FldName As String
    Dim FldOffs As String
    Dim tmpFltBarBgn As String
    Dim tmpFltBarEnd As String
    Dim OutFldName As String
    Dim OutFldData As String
    Dim BarKey As String
    Dim BarTxt As String
    Dim CurLine As Long
    Dim BarColor As Long
    Dim BarStyle As Integer
    Dim MaxLine As Long
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim BgnIsValid As Boolean
    Dim EndIsValid As Boolean
    Dim TimeOff As Integer
    Dim iGrp As Integer
    Dim iFld As Integer
    GanttDataPool.TabFlightBars(0).ResetContent
    TxtGroups = MainDialog.GetTabProperty(Index, "GANTT_FLBAR", 1, "")
    BarGroups = MainDialog.GetTabProperty(Index, "GANTT_TIMES", 1, "")
    BarLTextCfg = MainDialog.GetTabProperty(Index, "GANTT_LTEXT", 1, "")
    BarRTextCfg = MainDialog.GetTabProperty(Index, "GANTT_RTEXT", 1, "")
    MaxLine = DataTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        BarUrno = Trim(DataTab.GetFieldValue(CurLine, "URNO"))
        BarRkey = Trim(DataTab.GetFieldValue(CurLine, "RKEY"))
        BarAurn = Trim(DataTab.GetFieldValue(CurLine, "AURN"))
        BarFtyp = Trim(DataTab.GetFieldValue(CurLine, "FTYP"))
        BarAdid = Trim(DataTab.GetFieldValue(CurLine, "ADID"))
        Select Case Index
            Case TabArrFlightsIdx
                GanttDataPool.InitFlightBarList BarRkey, BarAurn, BarUrno
            Case TabDepFlightsIdx
                GanttDataPool.InitFlightBarList BarRkey, BarAurn, BarUrno
            Case Else
        End Select
        iGrp = 0
        BarFields = "START"
        While BarFields <> ""
            BgnIsValid = False
            EndIsValid = False
            iGrp = iGrp + 1
            BarFields = GetItem(BarGroups, iGrp, "|")
            TabFields = GetItem(TxtGroups, iGrp, "|")
            If BarFields <> "" Then
                tmpFltBarBgn = ""
                tmpFltBarEnd = ""
                FldProp = GetItem(BarFields, 1, ",")
                iFld = 0
                FldName = "START"
                While (FldName <> "") And (BgnIsValid = False)
                    iFld = iFld + 1
                    FldName = GetItem(FldProp, iFld, ";")
                    TimeOff = 0
                    If InStr(FldName, "+") > 0 Then
                        FldOffs = GetItem(FldName, 2, "+")
                        FldName = GetItem(FldName, 1, "+")
                        TimeOff = Val(FldOffs)
                    End If
                    If InStr(FldName, "-") > 0 Then
                        FldOffs = GetItem(FldName, 2, "-")
                        FldName = GetItem(FldName, 1, "-")
                        TimeOff = Val(FldOffs) * -1
                    End If
                    If FldName <> "" Then
                        Select Case Index
                            Case TabArrFlightsIdx
                                BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "A", "A", tmpFltBarBgn, tmpFltBarEnd)
                            Case TabDepFlightsIdx
                                BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "D", "D", tmpFltBarBgn, tmpFltBarEnd)
                            Case Else
                                tmpFltBarBgn = DataTab.GetFieldValue(CurLine, FldName)
                                tmpFltBarBgn = Trim(tmpFltBarBgn) 'igu on 18 Aug 2010
                        End Select
                        If tmpFltBarBgn = "" Then
                            tmpFltBarBgn = DataTab.GetFieldValue(CurLine, FldName)
                            tmpFltBarBgn = Trim(tmpFltBarBgn) 'igu on 18 Aug 2010
                        End If
                        If tmpFltBarBgn <> "" Then
                            BarBgn = CedaFullDateToVb(tmpFltBarBgn)
                            BarBgn = DateAdd("n", TimeOff, BarBgn)
                            BgnIsValid = True
                        End If
                    End If
                Wend
                FldProp = GetItem(BarFields, 2, ",")
                iFld = 0
                FldName = "START"
                While (FldName <> "") And (EndIsValid = False)
                    iFld = iFld + 1
                    FldName = GetItem(FldProp, iFld, ";")
                    TimeOff = 0
                    If InStr(FldName, "+") > 0 Then
                        FldOffs = GetItem(FldName, 2, "+")
                        FldName = GetItem(FldName, 1, "+")
                        TimeOff = Val(FldOffs)
                    End If
                    If InStr(FldName, "-") > 0 Then
                        FldOffs = GetItem(FldName, 2, "-")
                        FldName = GetItem(FldName, 1, "-")
                        TimeOff = Val(FldOffs) * -1
                    End If
                    If FldName <> "" Then
                        If tmpFltBarEnd = "" Then
                            Select Case Index
                                Case TabArrFlightsIdx
                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "A", "A", tmpFltBarBgn, tmpFltBarEnd)
                                Case TabDepFlightsIdx
                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "D", "D", tmpFltBarBgn, tmpFltBarEnd)
                                Case Else
                                    tmpFltBarEnd = DataTab.GetFieldValue(CurLine, FldName)
                                    tmpFltBarEnd = Trim(tmpFltBarEnd) 'igu on 18 Aug 2010
                            End Select
                        End If
                        If tmpFltBarEnd = "" Then
                            tmpFltBarEnd = DataTab.GetFieldValue(CurLine, FldName)
                            tmpFltBarEnd = Trim(tmpFltBarEnd) 'igu on 18 Aug 2010
                        End If
                        If tmpFltBarEnd <> "" Then
                            BarEnd = CedaFullDateToVb(tmpFltBarEnd)
                            BarEnd = DateAdd("n", TimeOff, BarEnd)
                            EndIsValid = True
                        End If
                    End If
                Wend
                BarTxt = DataTab.GetFieldValues(CurLine, TabFields)
                If (BgnIsValid = True) And (EndIsValid = True) Then
                    BarTxt = Replace(BarTxt, ",", " / ", 1, -1, vbBinaryCompare)
                    BarBgn = DateAdd("n", UtcTimeDisp, BarBgn)
                    BarEnd = DateAdd("n", UtcTimeDisp, BarEnd)
                    BarKey = "#" & CStr(iGrp) & "#" & BarUrno
                    Select Case BarAdid
                        Case "A"
                            BarStyle = 1
                            BarColor = vbYellow
                        Case "B"
                            If (BarFtyp = "T") Or (BarFtyp = "G") Then
                                BarStyle = 0
                                BarColor = LightGrey
                            Else
                                BarStyle = 1
                                BarColor = DarkGreen
                            End If
                        Case "D"
                            BarStyle = 1
                            BarColor = vbGreen
                        Case Else
                            BarStyle = 1
                            BarColor = vbMagenta
                    End Select
                    Select Case BarFtyp
                        Case "O"
                        Case "S"
                        Case "X"
                            BarColor = vbRed
                            'ShowBar = False
                        Case "N"
                            BarColor = vbRed
                            'ShowBar = False
                        Case "D"
                            BarColor = vbRed
                            'ShowBar = False
                        Case Else
                    End Select
                    BarBgnText = ""
                    BarEndText = ""
                    If BarLTextCfg <> "" Then
                        OutFldName = GetItem(BarLTextCfg, 2, "]")
                        OutFldData = DataTab.GetFieldValue(CurLine, OutFldName)
                        If OutFldData <> "" Then
                            GetKeyItem BarBgnText, BarLTextCfg, "[", "]"
                            BarBgnText = BarBgnText & " "
                            If InStr(MainDialog.ListOfCedaTimeFields, FldName) > 0 Then
                                BarBgnText = BarBgnText & GetItem(AutoFormatFieldValues(OutFldName, OutFldData, "hh:mm", "hh:mm"), 1, ",")
                            Else
                                BarBgnText = BarBgnText & OutFldData
                            End If
                            BarBgnText = BarBgnText & " "
                        End If
                    End If
                    If BarRTextCfg <> "" Then
                        OutFldName = GetItem(BarRTextCfg, 2, "]")
                        OutFldData = DataTab.GetFieldValue(CurLine, OutFldName)
                        If OutFldData <> "" Then
                            GetKeyItem BarEndText, BarRTextCfg, "[", "]"
                            BarEndText = BarEndText & " "
                            If InStr(MainDialog.ListOfCedaTimeFields, FldName) > 0 Then
                                BarEndText = BarEndText & GetItem(AutoFormatFieldValues(OutFldName, OutFldData, "hh:mm", "hh:mm"), 1, ",")
                            Else
                                BarEndText = BarEndText & OutFldData
                            End If
                            BarEndText = " " & BarEndText
                        End If
                    End If
    'CurGantt.TimeFrameFrom = TFFrom
    'CurGantt.TimeFrameTo = TFTo
                    If (BarBgn < CurGantt.TimeFrameTo) And (BarEnd > CurGantt.TimeFrameFrom) Then
                        CurGantt.AddBarToLine CurLine, BarKey, BarBgn, BarEnd, BarTxt, BarStyle, BarColor, vbBlack, 0, 0, 0, BarBgnText, BarEndText, vbBlack, vbBlack
                    End If
                End If
            End If
        Wend
        Select Case Index
            Case TabArrFlightsIdx
                AddShadowBars TabArrTowingIdx, CurLine, 0, vbCyan, BarRkey, CurGantt, TabArrTowingTab
                AddShadowBars TabDepFlightsIdx, CurLine, 0, LightGreen, BarRkey, CurGantt, TabDepFlightsTab
            Case TabDepFlightsIdx
                AddShadowBars TabArrTowingIdx, CurLine, 0, vbCyan, BarRkey, CurGantt, TabArrTowingTab
                AddShadowBars TabArrFlightsIdx, CurLine, 0, LightYellow, BarRkey, CurGantt, TabArrFlightsTab
            Case Else
        End Select
    Next
    CurGantt.Refresh
End Sub
Public Sub AddShadowBars(Index As Integer, BarLine As Long, BarStyle As Integer, BarColor As Long, UseRkey As String, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab)
    Dim TxtGroups As String
    Dim BarGroups As String
    Dim TabFields As String
    Dim BarFields As String
    Dim BarUrno As String
    Dim BarStat As String
    Dim FldName As String
    Dim HitList As String
    Dim HitItem As String
    Dim tmpFltBarBgn As String
    Dim tmpFltBarEnd As String
    Dim BarKey As String
    Dim BarTxt As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim BgnIsValid As Boolean
    Dim EndIsValid As Boolean
    Dim iHit As Integer
    Dim iGrp As Integer
    If Index >= 0 Then
        TxtGroups = MainDialog.GetTabProperty(Index, "GANTT_FLBAR", 1, "")
        BarGroups = MainDialog.GetTabProperty(Index, "GANTT_TIMES", 1, "")
        ColNo = GetRealItemNo(DataTab.LogicalFieldList, "RKEY")
        HitList = DataTab.GetLinesByColumnValue(ColNo, UseRkey, 0)
        If HitList <> "" Then
            iHit = 0
            HitItem = "START"
            While HitItem <> ""
                iHit = iHit + 1
                HitItem = GetItem(HitList, iHit, ",")
                If HitItem <> "" Then
                    LineNo = Val(HitItem)
                    BgnIsValid = False
                    EndIsValid = False
                    BarUrno = DataTab.GetFieldValue(LineNo, "URNO")
                    iGrp = 0
                    BarFields = "START"
                    While BarFields <> ""
                        iGrp = iGrp + 1
                        BarFields = GetItem(BarGroups, iGrp, "|")
                        TabFields = GetItem(TxtGroups, iGrp, "|")
                        If BarFields <> "" Then
                            tmpFltBarBgn = ""
                            tmpFltBarEnd = ""
                            FldName = GetItem(BarFields, 1, ",")
                            If FldName <> "" Then
                                If tmpFltBarBgn = "" Then
                                    Select Case Index
                                        Case TabArrFlightsIdx
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "A", "A", tmpFltBarBgn, tmpFltBarEnd)
                                        Case TabArrTowingIdx
                                            Select Case FldName
                                                Case "PDBS"
                                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "D", tmpFltBarBgn, tmpFltBarEnd)
                                                Case "PABS"
                                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "A", tmpFltBarBgn, tmpFltBarEnd)
                                                Case Else
                                            End Select
                                        Case TabDepFlightsIdx
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "D", "D", tmpFltBarBgn, tmpFltBarEnd)
                                        Case Else
                                            tmpFltBarBgn = DataTab.GetFieldValue(LineNo, FldName)
                                            tmpFltBarBgn = Trim(tmpFltBarBgn) 'igu on 18 Aug 2010
                                    End Select
                                End If
                                If tmpFltBarBgn = "" Then
                                    tmpFltBarBgn = DataTab.GetFieldValue(LineNo, FldName)
                                    tmpFltBarBgn = Trim(tmpFltBarBgn) 'igu on 18 Aug 2010
                                End If
                                If tmpFltBarBgn <> "" Then
                                    BarBgn = CedaFullDateToVb(tmpFltBarBgn)
                                    BgnIsValid = True
                                End If
                            End If
                            FldName = GetItem(BarFields, 2, ",")
                            If FldName <> "" Then
                                If tmpFltBarEnd = "" Then
                                    Select Case Index
                                        Case TabArrFlightsIdx
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "A", "A", tmpFltBarBgn, tmpFltBarEnd)
                                        Case TabArrTowingIdx
                                            Select Case FldName
                                                Case "PDES"
                                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "D", tmpFltBarBgn, tmpFltBarEnd)
                                                Case "PAES"
                                                    BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "B", "A", tmpFltBarBgn, tmpFltBarEnd)
                                                Case Else
                                            End Select
                                        Case TabDepFlightsIdx
                                            BarStat = GanttDataPool.GetFlightBarTimes(BarUrno, "D", "D", tmpFltBarBgn, tmpFltBarEnd)
                                        Case Else
                                        tmpFltBarEnd = DataTab.GetFieldValue(LineNo, FldName)
                                        tmpFltBarEnd = Trim(tmpFltBarEnd) 'igu on 18 Aug 2010
                                    End Select
                                End If
                                If tmpFltBarEnd = "" Then
                                    tmpFltBarEnd = DataTab.GetFieldValue(LineNo, FldName)
                                    tmpFltBarEnd = Trim(tmpFltBarEnd) 'igu on 18 Aug 2010
                                End If
                                If tmpFltBarEnd <> "" Then
                                    BarEnd = CedaFullDateToVb(tmpFltBarEnd)
                                    EndIsValid = True
                                End If
                            End If
                            BarTxt = DataTab.GetFieldValues(LineNo, TabFields)
                            If (BgnIsValid = True) And (EndIsValid = True) Then
                                BarTxt = Replace(BarTxt, ",", " / ", 1, -1, vbBinaryCompare)
                                BarBgn = DateAdd("n", UtcTimeDisp, BarBgn)
                                BarEnd = DateAdd("n", UtcTimeDisp, BarEnd)
                                BarKey = "#" & CStr(iGrp) & "#" & BarUrno
                                If (BarBgn < CurGantt.TimeFrameTo) And (BarEnd > CurGantt.TimeFrameFrom) Then
                                    CurGantt.AddBarToLine BarLine, BarKey, BarBgn, BarEnd, BarTxt, BarStyle, BarColor, vbBlack, 0, 0, 0, "", "", 0, 0
                                End If
                            End If
                        End If
                    Wend
                End If
            Wend
        End If
    End If

End Sub
Public Sub CreateFlightDecoObjects(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, TabFields As String)
    Dim tmpFltBarBgn As String
    Dim tmpFltBarEnd As String
    Dim BarKey As String
    Dim BarTxt As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim DecoPix As String
    
    'TL = Triangle at Top Left Corner
    'TR = Triangle at Top Right Corner
    'BL = Triangle at Bottom Left Corner
    'BR = Triangle as Bottom Right Corner
    'BTL = Big Triangle at Left Edge
    'BTR = Big Triangle at Right Edge
    'T = Block at Top Side
    'L = Block at Left Side
    'B = Block at Bottom Side
    'R = Block at Right Side
    
    DecoPix = CStr(DataTab.LineHeight - 8)
    DecoPix = DecoPix & "," & DecoPix
    CurGantt.DecorationObjectCreate "BARTLTR", "TL,BR", DecoPix, CStr(vbGreen) & "," & CStr(vbRed)
    
    MaxLine = DataTab.GetLineCount
    MaxLine = MaxLine - 1
    For CurLine = 0 To MaxLine
        BarKey = DataTab.GetFieldValue(CurLine, "URNO")
        CurGantt.DecorationObjectSetToBar "BARTLTR", BarKey
    Next
    CurGantt.Refresh
End Sub
Public Sub CreateLineSeparators(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, LineNo As Long, SetSelected As Boolean)
    Dim GanttScale As String
    Dim FldName As String
    Dim CurData As String
    Dim NxtData As String
    Dim CurLine As Long
    Dim BgnLine As Long
    Dim MaxLine As Long
    Dim SepColor As Long
    Dim SepStyle As Integer
    Dim SepLineHeight As Integer
    Dim SepBarHeight As Integer
    Dim SepBegin As Integer
    Dim BckBarInfo As String
    FldName = MainDialog.GanttIcon(Index).Tag
    If FldName = "" Then
        GanttScale = MainDialog.GetTabProperty(Index, "GANTT_SCALE", 1, "")
        FldName = GetItem(GanttScale, 1, ",")
    End If
    SepStyle = 0
    SepLineHeight = 1
    SepBarHeight = 3
    SepBegin = 1
    If LineNo >= 0 Then
        MaxLine = LineNo
        BgnLine = LineNo
        If SetSelected Then SepColor = vbCyan Else SepColor = vbBlack
    Else
        MaxLine = DataTab.GetLineCount - 1
        BgnLine = 0
        SepColor = vbBlack
    End If
    For CurLine = BgnLine To MaxLine
        If LineNo < 0 Then
            CurData = DataTab.GetFieldValue(CurLine, FldName)
            NxtData = DataTab.GetFieldValue(CurLine + 1, FldName)
            If NxtData <> CurData Then
                'If SetSelected Then
                    CurGantt.SetLineSeparator CurLine, SepLineHeight, SepColor, SepStyle, SepBarHeight, SepBegin
                    If LineNo > 0 Then
                        CurGantt.SetLineSeparator CurLine - 1, SepLineHeight, SepColor, SepStyle, SepBarHeight, SepBegin
                    End If
                'Else
                'End If
            Else
                'If Not SetSelected Then
                '    CurGantt.DeleteLineSeparator CurLine
                '    If LineNo > 0 Then
                '        CurGantt.DeleteLineSeparator CurLine - 1
                '    End If
                'Else
                'End If
            End If
        End If
        If LineNo >= 0 Then
            BckBarInfo = CurGantt.GetBkBarsByLine(CurLine)
            BckBarInfo = GetItem(BckBarInfo, 1, ";")
            If SetSelected Then
                CurGantt.SetBkBarColor BckBarInfo, LightestBlue
            Else
                CurGantt.SetBkBarColor BckBarInfo, vbBlue
            End If
        End If
    Next
    CurGantt.Refresh
    'CurGantt.RefreshArea "GANTT,TAB"
End Sub
Public Sub CreateBackgroundBars(Index As Integer, CurGantt As UGANTTLib.UGantt, DataTab As TABLib.Tab, TabFields As String)
    Dim BckBarTimes As String
    Dim BarBgnFldName As String
    Dim BarEndFldName As String
    Dim tmpFltBarBgn As String
    Dim tmpFltBarEnd As String
    Dim BarKey As String
    Dim BarTxt As String
    Dim BarBgn As Date
    Dim BarEnd As Date
    Dim BarColor As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    BarTxt = ""
    BarColor = vbBlue
    BckBarTimes = MainDialog.GetTabProperty(Index, "GANTT_TIMES", 1, "")
    If BckBarTimes <> "" Then
        MaxLine = DataTab.GetLineCount
        MaxLine = MaxLine - 1
        For CurLine = 0 To MaxLine
            BarKey = ""
            BarBgnFldName = GetItem(BckBarTimes, 1, ",")
            BarEndFldName = GetItem(BckBarTimes, 2, ",")
            tmpFltBarBgn = DataTab.GetFieldValue(CurLine, BarBgnFldName)
            tmpFltBarBgn = "20090101000000"
            If tmpFltBarBgn <> "" Then
                BarBgn = CedaFullDateToVb(tmpFltBarBgn)
                'BarBgn = DateAdd("d", -1, BarBgn)
                BarKey = DataTab.GetFieldValue(CurLine, "URNO")
            End If
            tmpFltBarEnd = DataTab.GetFieldValue(CurLine, BarEndFldName)
            tmpFltBarEnd = "20110101000000"
            If tmpFltBarEnd <> "" Then
                BarEnd = CedaFullDateToVb(tmpFltBarEnd)
                'BarEnd = DateAdd("d", 1, BarEnd)
                BarKey = DataTab.GetFieldValue(CurLine, "URNO")
            End If
            If BarKey <> "" Then
                CurGantt.AddBkBarToLine CurLine, BarKey, BarBgn, BarEnd, BarColor, BarTxt
            End If
        Next
        CurGantt.Refresh
    End If
End Sub

Public Sub CreateTimeSeparators(Index As Integer, CurGantt As UGANTTLib.UGantt, ForWhat As String)
    Dim TFFrom As Date
    Dim TFTo As Date
    Dim MrkTime As Date
    CurGantt.DeleteAllTimeMarkers
'    TFFrom = CurGantt.TimeFrameFrom
'    TFTo = CurGantt.TimeFrameTo
'    TFFrom = DateAdd("d", 1, TFFrom)
'    TFTo = DateAdd("d", -1, TFTo)
'    MrkTime = TFFrom
'    CurGantt.SetTimeMarker MrkTime, vbBlack
'    MrkTime = DateAdd("h", 1, MrkTime)
'    While MrkTime < TFTo
'        CurGantt.SetTimeMarker MrkTime, vbWhite
'        MrkTime = DateAdd("h", 1, MrkTime)
'    Wend
'    MrkTime = TFTo
'    CurGantt.SetTimeMarker MrkTime, vbBlack
End Sub

Public Function GetBarLineInTab(Key As String) As Long
    Dim strBarLine As String
    Dim llBarLine As Long

'    With GanttDataPool.TabGanttBars
'        strBarLine = .GetLinesByColumnValue(1, Key, 1)
'        If strBarLine <> "" Then
'            llBarLine = CLng(strBarLine)
'        End If
'    End With
    GetBarLineInTab = llBarLine
End Function
Public Function GetBkBarLineInTab(Key As String) As Long
    Dim strBkBarLine As String
    Dim llBkBarLine As Long

'    With GanttDataPool.TabGanttBkBars
'        strBkBarLine = .GetLinesByColumnValue(1, Key, 1)
'        If strBkBarLine <> "" Then
'            llBkBarLine = CLng(strBkBarLine)
'        End If
'    End With
    GetBkBarLineInTab = llBkBarLine
End Function

Public Function GetBarLineByKey(Key As String) As Long
    Dim strBarLine, strTmpLine As String
    Dim llBarLine, llTmpLine As Long
    Dim strEURN As String

    'look for the bar in the TabGanttBars-table
'    With GanttDataPool.TabGanttBars
'        strTmpLine = .GetLinesByColumnValue(1, Key, 0)
'        If strTmpLine <> "" Then
'            llTmpLine = CLng(strTmpLine)
'            strEURN = .GetColumnValue(llTmpLine, 0)
'        End If
'    End With

    'look for the line the bar is in
'    With GanttDataPool.TabGanttLines
'        strBarLine = .GetLinesByColumnValue(0, strEURN, 0)
'        If strBarLine <> "" Then
'            llBarLine = CLng(strBarLine)
'        End If
'    End With

    GetBarLineByKey = llBarLine
End Function

Public Function GetBkBarLineByKey(Key As String) As Long
    Dim strBarLine, strTmpLine As String
    Dim llBarLine, llTmpLine As Long
    Dim strEURN As String

    'look for the bar in the TabGanttBars-table
'    With GanttDataPool.TabGanttBkBars
'        strTmpLine = .GetLinesByColumnValue(1, Key, 1)
'        If strTmpLine <> "" Then
'            llTmpLine = CLng(strTmpLine)
'            strEURN = .GetColumnValue(llTmpLine, 0)
'        End If
'    End With

    'look for the line the bar is in
'    With GanttDataPool.TabGanttLines
'        strBarLine = .GetLinesByColumnValue(0, strEURN, 1)
'        If strBarLine <> "" Then
'            llBarLine = CLng(strBarLine)
'        End If
'    End With

    GetBkBarLineByKey = llBarLine
End Function


