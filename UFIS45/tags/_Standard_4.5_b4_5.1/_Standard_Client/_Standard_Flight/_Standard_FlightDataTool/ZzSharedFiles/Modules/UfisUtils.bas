Attribute VB_Name = "UfisUtils"
Option Explicit
Public RestoreAction As Boolean
Public BackUpAction As Boolean
Public ListOfValidCtrlTypes As String


Public Sub SaveForm(UseFileName As String, frm As Form, pWithTab As Boolean)
    Dim ctl As Control
    Dim Obj As Object
    Dim IsValid As Boolean
    Dim strObjId As String
    Dim strCtlType As String
    Dim strSectionName As String
    On Error Resume Next
    BackUpAction = True
    strSectionName = frm.Name
    SetIniFileEntry strSectionName, frm.Name & ".Caption", frm.Caption, UseFileName
    For Each ctl In frm.Controls
        If IsArray(ctl) Then strObjId = ctl.Name & "_" & Trim(Str(ctl.Index)) Else strObjId = ctl.Name
        strCtlType = TypeName(ctl)
        IsValid = True
        Select Case strCtlType
            Case "OptionButton", "CheckBox"
                SetIniFileEntry strSectionName, strObjId & ".Value", CStr(ctl.Value), UseFileName
                SetIniFileEntry strSectionName, strObjId & ".Caption", ctl.Caption, UseFileName
            Case "TextBox"
                SetIniFileEntry strSectionName, strObjId & ".Text", ctl.Text, UseFileName
            Case "ComboBox"
                SetIniFileEntry strSectionName, strObjId & ".Text", ctl.Text, UseFileName
            Case "Label"
                SetIniFileEntry strSectionName, strObjId & ".Caption", ctl.Caption, UseFileName
            Case "Frame"
                SetIniFileEntry strSectionName, strObjId & ".Caption", ctl.Caption, UseFileName
            Case "TAB"
                'SetIniFileEntry strSectionName, strObjId & ".Visible", CInt(ctl.Visible), UseFileName
                SetIniFileEntry strSectionName, strObjId & ".Tag", ctl.Tag, UseFileName
                If pWithTab = True Then SaveTab strObjId, ctl, strSectionName, UseFileName
                IsValid = False
            Case Else
                IsValid = False
        End Select
        If IsValid Then
            'SetIniFileEntry strSectionName, strObjId & ".Visible", CInt(ctl.Visible), UseFileName
            SetIniFileEntry strSectionName, strObjId & ".Enabled", CInt(ctl.Enabled), UseFileName
            SetIniFileEntry strSectionName, strObjId & ".ForeColor", CStr(ctl.ForeColor), UseFileName
            SetIniFileEntry strSectionName, strObjId & ".BackColor", CStr(ctl.BackColor), UseFileName
            SetIniFileEntry strSectionName, strObjId & ".Tag", ctl.Tag, UseFileName
            SetIniFileEntry strSectionName, strObjId & ".ToolTip", ctl.ToolTipText, UseFileName
        End If
    Next
    BackUpAction = False
End Sub

Public Sub ReadForm(UseFileName As String, frm As Form, pWithTab As Boolean)
    Dim ctl As Control
    Dim Obj As Object
    Dim IsValid As Boolean
    Dim strObjId As String
    Dim strCtlType As String
    Dim strSectionName As String
    On Error Resume Next
    RestoreAction = True
    strSectionName = frm.Name
    frm.Caption = GetIniFileEntry(strSectionName, frm.Name & ".Caption", frm.Caption, UseFileName)
    For Each ctl In frm.Controls
        If IsArray(ctl) Then strObjId = ctl.Name & "_" & Trim(Str(ctl.Index)) Else strObjId = ctl.Name
        strCtlType = TypeName(ctl)
        IsValid = True
        Select Case strCtlType
            Case "OptionButton", "CheckBox"
                ctl.Caption = GetIniFileEntry(strSectionName, strObjId & ".Caption", ctl.Caption, UseFileName)
                ctl.Value = GetIniFileEntry(strSectionName, strObjId & ".Value", ctl.Value, UseFileName)
            Case "TextBox"
                ctl.Text = GetIniFileEntry(strSectionName, strObjId & ".Text", ctl.Text, UseFileName)
            Case "ComboBox"
                ctl.Text = GetIniFileEntry(strSectionName, strObjId & ".Text", ctl.Text, UseFileName)
            Case "Label"
                ctl.Caption = GetIniFileEntry(strSectionName, strObjId & ".Caption", ctl.Caption, UseFileName)
            Case "Frame"
                ctl.Caption = GetIniFileEntry(strSectionName, strObjId & ".Caption", ctl.Caption, UseFileName)
            Case "TAB"
                'ctl.Visible = Val(GetIniFileEntry(strSectionName, strObjId & ".Visible", Str(CInt(ctl.Visible)), UseFileName))
                ctl.Tag = GetIniFileEntry(strSectionName, strObjId & ".Tag", ctl.Tag, UseFileName)
                If pWithTab = True Then ReadTab strObjId, ctl, strSectionName, False, UseFileName
                IsValid = False
            Case Else
                IsValid = False
        End Select
        If IsValid Then
            'ctl.Visible = Val(GetIniFileEntry(strSectionName, strObjId & ".Visible", Str(CInt(ctl.Visible)), UseFileName))
            ctl.Enabled = val(GetIniFileEntry(strSectionName, strObjId & ".Enabled", Str(CInt(ctl.Enabled)), UseFileName))
            ctl.Tag = GetIniFileEntry(strSectionName, strObjId & ".Tag", ctl.Tag, UseFileName)
            ctl.ToolTipText = GetIniFileEntry(strSectionName, strObjId & ".ToolTip", ctl.ToolTipText, UseFileName)
            ctl.ForeColor = GetIniFileEntry(strSectionName, strObjId & ".ForeColor", ctl.ForeColor, UseFileName)
            ctl.BackColor = GetIniFileEntry(strSectionName, strObjId & ".BackColor", ctl.BackColor, UseFileName)
        End If
    Next
    RestoreAction = False
End Sub

Public Sub SaveTab(ByVal UseObjId As String, ByRef rTab As TABLib.Tab, ByVal pSectionName As String, ByVal pFileName As String)
    Dim strLineData As String
    Dim strTag As String
    Dim l As Long

    strTag = UseObjId

    SetIniFileEntry pSectionName, strTag & "_FIELDSEPARATOR", Asc(rTab.GetFieldSeparator), pFileName
    SetIniFileEntry pSectionName, strTag & "_HEADERSTRING", rTab.HeaderString, pFileName
    SetIniFileEntry pSectionName, strTag & "_HEADERLENGTHSTRING", rTab.HeaderLengthString, pFileName
    SetIniFileEntry pSectionName, strTag & "_COLUMNWIDTHSTRING", rTab.ColumnWidthString, pFileName
    SetIniFileEntry pSectionName, strTag & "_MAINHEADER", CInt(rTab.MainHeader), pFileName
    SetIniFileEntry pSectionName, strTag & "_MAINHEADERONLY", CInt(rTab.MainHeaderOnly), pFileName
    SetIniFileEntry pSectionName, strTag & "_MAINHEADERRANGES", rTab.GetMainHeaderRanges, pFileName
    SetIniFileEntry pSectionName, strTag & "_MAINHEADERVALUES", rTab.GetMainHeaderValues, pFileName
'    SetIniFileEntry pSectionName, strTag & "_SHOWHORZSCROLLER", rTab.ShowHorzScroller, pFileName
'    SetIniFileEntry pSectionName, strTag & "_SHOWVERTSCROLLER", rTab.ShowVertScroller, pFileName
'    SetIniFileEntry pSectionName, strTag & "_ENABLEHEADERSIZING", rTab.EnableHeaderSizing, pFileName
    SetIniFileEntry pSectionName, strTag & "_FONTNAME", rTab.FontName, pFileName
    SetIniFileEntry pSectionName, strTag & "_FONTSIZE", rTab.FontSize, pFileName
'    SetIniFileEntry pSectionName, strTag & "_SETTABFONTBOLD", rTab.SetTabFontBold, pFileName
    SetIniFileEntry pSectionName, strTag & "_COLUMNALIGNMENTSTRING", rTab.ColumnAlignmentString, pFileName
    SetIniFileEntry pSectionName, strTag & "_EMPTYAREABACKCOLOR", rTab.EmptyAreaBackColor, pFileName
    SetIniFileEntry pSectionName, strTag & "_EMPTYAREARIGHTCOLOR", rTab.EmptyAreaRightColor, pFileName
    SetIniFileEntry pSectionName, strTag & "_SHOWROWSELECTION", CInt(rTab.ShowRowSelection), pFileName
    SetIniFileEntry pSectionName, strTag & "_AUTOSIZEBYHEADER", CInt(rTab.AutoSizeByHeader), pFileName
    'SetIniFileEntry pSectionName, strTag & "_DATECOLUMNS", rTab.Date, pFileName
    
    strLineData = pFileName & "." & pSectionName & "." & strTag & ".dat"
    SetIniFileEntry pSectionName, strTag & "_DATA", strLineData, pFileName
    
    rTab.WriteToFileWithProperties strLineData

'    For l = 0 To rTab.GetLineCount - 1
'        SetIniFileEntry pSectionName, strTag & "_DATA" & CStr(l), rTab.GetLineValues(l), pFileName
'    Next l

End Sub

Public Sub ReadTab(ByVal UseObjId As String, ByRef rTab As TABLib.Tab, ByVal pSectionName As String, WithInternalData As Boolean, ByVal pFileName As String)
    Dim strLineData As String
    Dim blFound As String
    Dim strTag As String
    Dim strTmp As String
    Dim l As Long

    strTag = UseObjId
    If WithInternalData Then
        strTmp = Chr(val(GetIniFileEntry(pSectionName, strTag & "_FIELDSEPARATOR", "", pFileName)))
        rTab.SetFieldSeparator strTmp
    End If
    rTab.HeaderString = GetIniFileEntry(pSectionName, strTag & "_HEADERSTRING", rTab.HeaderString, pFileName)
    rTab.HeaderLengthString = GetIniFileEntry(pSectionName, strTag & "_HEADERLENGTHSTRING", rTab.HeaderLengthString, pFileName)
    rTab.ColumnWidthString = GetIniFileEntry(pSectionName, strTag & "_COLUMNWIDTHSTRING", rTab.ColumnWidthString, pFileName)
    rTab.MainHeader = val(GetIniFileEntry(pSectionName, strTag & "_MAINHEADER", Str(CInt(rTab.MainHeader)), pFileName))
    rTab.MainHeaderOnly = val(GetIniFileEntry(pSectionName, strTag & "_MAINHEADERONLY", Str(CInt(rTab.MainHeaderOnly)), pFileName))
    rTab.SetMainHeaderValues GetIniFileEntry(pSectionName, strTag & "_MAINHEADERRANGES", rTab.GetMainHeaderRanges, pFileName), _
                             GetIniFileEntry(pSectionName, strTag & "_MAINHEADERVALUES", rTab.GetMainHeaderValues, pFileName), ""
'    rTab.ShowHorzScroller GetIniFileEntry(pSectionName, strTag & "_SHOWHORZSCROLLER", rTab.ShowHorzScroller, pFileName)
'    rTab.ShowVertScroller GetIniFileEntry(pSectionName, strTag & "_SHOWVERTSCROLLER", rTab.ShowVertScroller, pFileName)
'    rTab.EnableHeaderSizing GetIniFileEntry(pSectionName, strTag & "_ENABLEHEADERSIZING", rTab.EnableHeaderSizing, pFileName)
    rTab.FontName = GetIniFileEntry(pSectionName, strTag & "_FONTNAME", rTab.FontName, pFileName)
    rTab.FontSize = GetIniFileEntry(pSectionName, strTag & "_FONTSIZE", rTab.FontSize, pFileName)
'    rTab.SetTabFontBold = GetIniFileEntry(pSectionName, strTag & "_SETTABFONTBOLD", rTab.SetTabFontBold, pFileName)
    rTab.ColumnAlignmentString = GetIniFileEntry(pSectionName, strTag & "_COLUMNALIGNMENTSTRING", rTab.ColumnAlignmentString, pFileName)
    rTab.EmptyAreaBackColor = GetIniFileEntry(pSectionName, strTag & "_EMPTYAREABACKCOLOR", rTab.EmptyAreaBackColor, pFileName)
    rTab.EmptyAreaRightColor = GetIniFileEntry(pSectionName, strTag & "_EMPTYAREARIGHTCOLOR", rTab.EmptyAreaRightColor, pFileName)
    rTab.ShowRowSelection = val(GetIniFileEntry(pSectionName, strTag & "_SHOWROWSELECTION", Str(CInt(rTab.ShowRowSelection)), pFileName))
    rTab.AutoSizeByHeader = val(GetIniFileEntry(pSectionName, strTag & "_AUTOSIZEBYHEADER", Str(CInt(rTab.AutoSizeByHeader)), pFileName))
    If Not WithInternalData Then
        strTmp = Chr(val(GetIniFileEntry(pSectionName, strTag & "_FIELDSEPARATOR", "", pFileName)))
        rTab.SetFieldSeparator strTmp
    End If

    If WithInternalData Then
        blFound = True
        l = 0
        While blFound = True
            strLineData = GetIniFileEntry(pSectionName, strTag & "_DATA" & CStr(l), "", pFileName)
            If Len(strLineData) > 0 Then
                rTab.InsertTextLine strLineData, False
            Else
                blFound = False
            End If
            l = l + 1
        Wend
    Else
        strLineData = GetIniFileEntry(pSectionName, strTag & "_DATA", "", pFileName)
        If strLineData <> "" Then rTab.ReadFromFileWithProperties strLineData
    End If

End Sub

Public Function GetIniFileEntry(ByVal pSectionName As String, ByVal pKeyName As String, ByVal pDefaultValue As String, ByVal pFileName As String) As String
    Dim ret As Long
    Dim strRet As String
    strRet = Space(500)

    ret = GetPrivateProfileString(pSectionName, pKeyName, pDefaultValue, strRet, Len(strRet), pFileName)

    If ret > 0 Then
        strRet = Left(strRet, ret)
    Else
        strRet = pDefaultValue
    End If
    GetIniFileEntry = strRet
End Function

Public Function SetIniFileEntry(ByVal pSectionName As String, ByVal pKeyName As String, ByVal pValue As String, ByVal pFileName As String) As String
    WritePrivateProfileString pSectionName, pKeyName, pValue, pFileName
End Function

Public Function IsArray(ByRef rCtrl As Control) As Boolean
    On Error GoTo Error
    Dim i As Integer
    i = rCtrl.Index
    IsArray = True
    Exit Function
Error:
    IsArray = False
    Err.Clear
    Exit Function
End Function

