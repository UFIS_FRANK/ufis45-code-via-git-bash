Attribute VB_Name = "SlideMethods"
Option Explicit
Public SlideActive As Boolean
Public SlideShow As Boolean
Public SlideHoldLeft As Boolean
Public SlideHoldMid As Boolean
Public SlideHoldRight As Boolean
Public SlideHoldAll As Boolean
Public SlideDouble As Boolean
Public SlideMaximized As Boolean
Public SlideIndex As Integer
Public UfisPicForm As Form
Public UfisPicArea As PictureBox
Public MyDetailsIsOpen As Boolean
Dim UfisPicXDiff As Long
Dim UfisPicYDiff As Long

Public Sub InitUfisIntro(UsePicForm As Form, UsePicArea As PictureBox, XDiff As Long, YDiff As Long)
    Set UfisPicForm = UsePicForm
    Set UfisPicArea = UsePicArea
    UfisPicXDiff = XDiff
    UfisPicYDiff = YDiff
    SetUfisIntroFrames False
    SlideActive = False
End Sub

Public Sub SetUfisIntroFrames(SetVisible As Boolean)
    Dim i As Integer
    SlideActive = SetVisible
    If Not SetVisible Then
        For i = 0 To UfisPicForm.UfisIntroFrame.UBound
            UfisPicForm.UfisIntroFrame(i).Visible = SetVisible
        Next
        SlideShow = False
        SlideHoldLeft = False
        SlideHoldMid = False
        SlideHoldRight = False
        SlideHoldAll = False
        SlideDouble = False
        SlideMaximized = False
        SlideIndex = 0
        Unload SlideControls
    End If
    If SetVisible Then CheckUfisIntroPicture -1, False, 0, MyIntroPicture, UFIS_SYSTEM
    SetUfisIntroTimer -1, SetVisible
End Sub

Public Sub SetUfisIntroTimer(Index As Integer, SetEnable As Boolean)
    SlideControls.UfisIntroTimer(0).Enabled = SetEnable
End Sub

Public Sub CheckUfisIntroResize()
    If SlideActive Then SlideControls.UfisIntroTimer(1).Enabled = True
End Sub

Public Sub CheckUfisIntroPicture(CalledFrom As Integer, RefreshPicture As Boolean, IncIdx As Integer, MyPicture As String, MyPath As String)
    Static PicFileIndex As Integer
    Static PicFileCount As Integer
    Static PicCountDownLimit As Long
    Static PicCountDownValue As Long
    Static LastPicFileName As String
    Static PicturePath As String
    Static FuncFileIndex As Integer
    Static FuncFileCount As Integer
    Static FuncCountDownLimit As Long
    Static FuncCountDownValue As Long
    Static FuncScenarioPath As String
    Static LastPathName As String
    Static LastFileName As String
    Static FuncScenario As Boolean
    Dim FileIndex As Integer
    Dim FileCount As Integer
    Dim CountDownLimit As Long
    Dim CountDownValue As Long
    Dim FullName As String
    Dim FileName As String
    Dim PathName As String
    Dim PicName As String
    Dim FilePattern As String
    Dim tmpData As String
    Dim newEntry As String
    Dim NewSize As Long
    Dim TimeStep As Long
    Dim i As Integer
    Dim LoadMyPicture As Boolean
    Dim TryAgain As Boolean
    Dim ActPicX
    Dim ActPicY
    Dim ActZoom
    On Error GoTo ErrorHandle
    If (SlideActive) Or (RefreshPicture = True) Then
        If PicturePath = "" Then
            PicCountDownValue = 1
            PicCountDownLimit = 1
            FuncCountDownValue = 1
            FuncCountDownLimit = 1
            PicturePath = GetIniEntry(myIniFullName, "MAIN", "", "APPL_INTRO_ALBUM", UFIS_SYSTEM)
            TimeStep = Val(GetIniEntry(myIniFullName, "MAIN", "", "APPL_INTRO_TIMER", "5"))
            If TimeStep < 1 Then TimeStep = 5
            If TimeStep > 60 Then
                PicCountDownLimit = TimeStep
                TimeStep = 1
            End If
            TimeStep = TimeStep * 1000
            SlideControls.UfisIntroTimer(0).Interval = TimeStep
            SlideControls.UfisIntroTimer(0).Enabled = False
            If UCase(PicturePath) <> UCase(UFIS_SYSTEM) Then
                SlideControls.UfisIntroTimer(0).Enabled = True
            Else
                FileName = MyIntroPicture
                MyIntroPictureNotFound = False
            End If
        ElseIf SlideControls.UfisIntroTimer(0).Interval < 1000 Then
            TimeStep = Val(GetIniEntry(myIniFullName, "MAIN", "", "APPL_INTRO_TIMER", "5"))
            If TimeStep < 1 Then TimeStep = 5
            If TimeStep > 60 Then
                PicCountDownLimit = TimeStep
                TimeStep = 1
            End If
            TimeStep = TimeStep * 1000
            SlideControls.UfisIntroTimer(0).Interval = TimeStep
        End If
        
        LoadMyPicture = False
        PathName = MyPath
        If FileName = "" Then FileName = MyPicture
        LoadMyPicture = RefreshPicture
        If LoadMyPicture = False Then
            Select Case CalledFrom
                Case -3 'Restore Slide Show
                    LoadMyPicture = True
                Case -2 'FileDialog
                    LoadMyPicture = True
                Case -1 'MainDialog Static Picture
                    If FuncScenario = True Then
                        FileIndex = 0
                        CountDownValue = 0
                        FuncScenario = False
                    End If
                    LoadMyPicture = True
                Case 0  'MainTimer (Slide Show)
                    If SlideShow = False Then
                        SlideControls.UfisIntroTimer(0).Enabled = False
                        If FuncScenario = True Then
                            PathName = FuncScenarioPath
                            CountDownValue = FuncCountDownValue
                            CountDownLimit = FuncCountDownLimit
                            FileIndex = FuncFileIndex
                            FileCount = FuncFileCount
                        Else
                            PathName = PicturePath
                            CountDownValue = PicCountDownValue
                            CountDownLimit = PicCountDownLimit
                            FileIndex = PicFileIndex
                            FileCount = PicFileCount
                        End If
                        If UCase(PathName) <> UCase(UFIS_SYSTEM) Then
                            CountDownValue = CountDownValue - 1
                            If CountDownValue <= 0 Then
                                FileIndex = FileIndex + IncIdx
                                FilePattern = "*.jpg;*.bmp"
                                FileName = SlideControls.GetNextFile(PathName, FilePattern, FileIndex, FileCount, True)
                                If FileName <> "" Then LoadMyPicture = True
                            Else
                                If LastFileName <> LastPicFileName Then
                                    FileName = LastPicFileName
                                    LoadMyPicture = True
                                End If
                            End If
                            If FuncScenario = True Then
                                FuncCountDownValue = CountDownValue
                                FuncCountDownLimit = CountDownLimit
                                FuncFileIndex = FileIndex
                                FuncFileCount = FileCount
                            Else
                                PicCountDownValue = CountDownValue
                                PicCountDownLimit = CountDownLimit
                                PicFileIndex = FileIndex
                                PicFileCount = FileCount
                                If FileName <> "" Then LastPicFileName = FileName
                            End If
                        End If
                    End If
                Case 1  'RefreshTimer (Cannot happen here)
                    'LoadMyPicture = True
                Case 2  'SlideTimer
                    LoadMyPicture = True
                Case 3  'MainFunctionScenario SlideShow
                    If SlideShow = False Then
                        SlideControls.UfisIntroTimer(0).Enabled = False
                        FuncScenario = True
                        If MyPath <> FuncScenarioPath Then
                            FuncFileIndex = 0
                            FuncFileCount = 0
                            FuncScenarioPath = MyPath
                        End If
                        If UCase(FuncScenarioPath) <> UCase(UFIS_SYSTEM) Then
                            PathName = FuncScenarioPath
                            FilePattern = "*.jpg;*.bmp"
                            FileName = SlideControls.GetNextFile(PathName, FilePattern, FuncFileIndex, FuncFileCount, True)
                            If FileName <> "" Then LoadMyPicture = True
                        End If
                        FileCount = FuncFileCount
                    End If
                Case Else
            End Select
        End If
        
        If LoadMyPicture = True Then
            TryAgain = False
            If (FileName <> "") Or (RefreshPicture = True) Then
                For i = 0 To 1
                    UfisPicForm.UfisIntroPicture(i).Visible = False
                    UfisPicForm.UfisIntroPicture(i).Stretch = False
                Next
                If RefreshPicture = False Then
                    PicName = FileName
                    LastFileName = FileName
                    newEntry = "," & FileName & "," & PathName & ","
                    If (SlideHoldRight = False) And (SlideDouble = False) Then
                        UfisPicForm.UfisIntroPicture(1).Picture = UfisPicForm.UfisIntroPicture(0).Picture
                        If SlideShow = True Then UfisPicForm.UfisIntroPicture(1).Tag = UfisPicForm.UfisIntroPicture(0).Tag
                    End If
                    If SlideHoldLeft = False Then
                        If InStr(PicName, ".") = 0 Then FileName = PicName & ".bmp"
                        newEntry = "," & FileName & "," & PathName & ","
                        UfisPicForm.UfisIntroPicture(0).Picture = LoadPicture(PathName & "\" & FileName)
                        If TryAgain = True Then
                            TryAgain = False
                            MyIntroPictureNotFound = False
                            If InStr(PicName, ".") = 0 Then FileName = PicName & ".jpg"
                            newEntry = "," & FileName & "," & PathName & ","
                            UfisPicForm.UfisIntroPicture(0).Picture = LoadPicture(PathName & "\" & FileName)
                        End If
                        If (SlideShow = True) And (TryAgain = False) Then UfisPicForm.UfisIntroPicture(0).Tag = newEntry
                        If TryAgain = True Then UfisPicForm.UfisIntroPicture(0).Tag = "ERROR"
                        If (SlideDouble = True) And (SlideHoldRight = False) Then
                            UfisPicForm.UfisIntroPicture(1).Picture = UfisPicForm.UfisIntroPicture(0).Picture
                            If SlideShow = True Then UfisPicForm.UfisIntroPicture(1).Tag = UfisPicForm.UfisIntroPicture(0).Tag
                            If TryAgain = True Then UfisPicForm.UfisIntroPicture(1).Tag = "ERROR"
                        End If
                    ElseIf SlideHoldRight = False Then
                        UfisPicForm.UfisIntroPicture(1).Picture = LoadPicture(PathName & "\" & FileName)
                        If (SlideShow = True) And (TryAgain = False) Then UfisPicForm.UfisIntroPicture(1).Tag = newEntry
                        If TryAgain = True Then UfisPicForm.UfisIntroPicture(1).Tag = "ERROR"
                    End If
                End If
                For i = 0 To 1
                    ActPicX = UfisPicForm.UfisIntroPicture(i).Width
                    ActPicY = UfisPicForm.UfisIntroPicture(i).Height
                    NewSize = (UfisPicArea.ScaleWidth) / 2
                    If ActPicX > NewSize Then
                        UfisPicForm.UfisIntroPicture(i).Stretch = True
                        ActZoom = ActPicX / NewSize
                        UfisPicForm.UfisIntroPicture(i).Width = ActPicX / ActZoom
                        UfisPicForm.UfisIntroPicture(i).Height = ActPicY / ActZoom
                    End If
                    NewSize = UfisPicArea.ScaleHeight
                    ActPicX = UfisPicForm.UfisIntroPicture(i).Width
                    ActPicY = UfisPicForm.UfisIntroPicture(i).Height
                    If ActPicY > NewSize Then
                        UfisPicForm.UfisIntroPicture(i).Stretch = True
                        ActZoom = ActPicY / NewSize
                        UfisPicForm.UfisIntroPicture(i).Width = ActPicX / ActZoom
                        UfisPicForm.UfisIntroPicture(i).Height = ActPicY / ActZoom
                    End If
                    UfisPicForm.UfisIntroFrame(i).Width = UfisPicForm.UfisIntroPicture(i).Width
                    UfisPicForm.UfisIntroFrame(i).Height = UfisPicForm.UfisIntroPicture(i).Height
                    UfisPicForm.UfisIntroPicture(i).Visible = True
                    UfisPicForm.UfisIntroFrame(i).Visible = (SlideShow Or FuncScenario)
                Next
                If (SlideShow = False) And ((CalledFrom = 0) Or (CalledFrom = 3)) Then
                    If FileCount > 1 Then SlideControls.UfisIntroTimer(0).Enabled = True Else SlideControls.UfisIntroTimer(0).Enabled = False
                End If
            End If
            UfisPicForm.UfisIntroFrame(0).Top = ((UfisPicArea.ScaleHeight - UfisPicForm.UfisIntroPicture(0).Height) \ 2)
            UfisPicForm.UfisIntroFrame(0).Left = ((UfisPicArea.ScaleWidth \ 2) - UfisPicForm.UfisIntroPicture(0).Width) \ 2
            UfisPicForm.UfisIntroFrame(1).Top = ((UfisPicArea.ScaleHeight - UfisPicForm.UfisIntroPicture(1).Height) \ 2)
            UfisPicForm.UfisIntroFrame(1).Left = (((UfisPicArea.ScaleWidth \ 2) - UfisPicForm.UfisIntroPicture(1).Width) \ 2) + (UfisPicArea.ScaleWidth \ 2)
            LoadMyPicture = False
            If (TryAgain = False) And (SlideDouble = False) And (UfisPicForm.UfisIntroPicture(0).Width > UfisPicForm.UfisIntroPicture(0).Height) Then LoadMyPicture = True
            If (UfisPicForm.UfisIntroFrame(0).Visible = False) And (SlideDouble = False) Then LoadMyPicture = True
            If (TryAgain = False) And (SlideDouble = True) And (UfisPicForm.UfisIntroPicture(0).Width > UfisPicForm.UfisIntroPicture(0).Height) Then LoadMyPicture = True
            If LoadMyPicture = True Then
                UfisPicForm.UfisIntroPicture(2).Stretch = False
                UfisPicForm.UfisIntroPicture(2).Visible = False
                UfisPicForm.UfisIntroPicture(2).Picture = UfisPicForm.UfisIntroPicture(0).Picture
                UfisPicForm.UfisIntroPicture(2).Tag = UfisPicForm.UfisIntroPicture(0).Tag
                ActPicX = UfisPicForm.UfisIntroPicture(2).Width
                ActPicY = UfisPicForm.UfisIntroPicture(2).Height
                NewSize = UfisPicArea.ScaleWidth
                If ActPicX > NewSize Then
                    UfisPicForm.UfisIntroPicture(2).Stretch = True
                    ActZoom = ActPicX / NewSize
                    UfisPicForm.UfisIntroPicture(2).Width = ActPicX / ActZoom
                    UfisPicForm.UfisIntroPicture(2).Height = ActPicY / ActZoom
                End If
                NewSize = UfisPicArea.ScaleHeight
                ActPicX = UfisPicForm.UfisIntroPicture(2).Width
                ActPicY = UfisPicForm.UfisIntroPicture(2).Height
                If ActPicY > NewSize Then
                    UfisPicForm.UfisIntroPicture(2).Stretch = True
                    ActZoom = ActPicY / NewSize
                    UfisPicForm.UfisIntroPicture(2).Width = ActPicX / ActZoom
                    UfisPicForm.UfisIntroPicture(2).Height = ActPicY / ActZoom
                End If
                UfisPicForm.UfisIntroFrame(2).Width = UfisPicForm.UfisIntroPicture(2).Width
                UfisPicForm.UfisIntroFrame(2).Height = UfisPicForm.UfisIntroPicture(2).Height
                UfisPicForm.UfisIntroFrame(2).Top = ((UfisPicArea.ScaleHeight - UfisPicForm.UfisIntroPicture(2).Height) \ 2)
                UfisPicForm.UfisIntroFrame(2).Left = (UfisPicArea.ScaleWidth - UfisPicForm.UfisIntroPicture(2).Width) \ 2
                If UfisPicForm.UfisIntroPicture(2).Tag <> "ERROR" Then
                    UfisPicForm.UfisIntroPicture(2).Visible = True
                    UfisPicForm.UfisIntroFrame(2).Visible = True
                    UfisPicForm.UfisIntroFrame(2).ZOrder
                End If
            Else
                UfisPicForm.UfisIntroFrame(2).Visible = False
            End If
            UfisPicArea.Refresh
            AdjustUfisIntroDetails
        End If
        If (SlideShow = False) And (FuncScenario = False) Then
            If (RefreshPicture = False) And (PicCountDownValue <= 0) Then PicCountDownValue = PicCountDownLimit
            If PicCountDownValue > 0 Then SlideControls.UfisIntroTimer(0).Enabled = True
        End If
    End If
    Exit Sub
ErrorHandle:
    tmpData = Err.Description
    If InStr(tmpData, MyIntroPicture) > 0 Then MyIntroPictureNotFound = True
    PathName = UFIS_SYSTEM
    FileName = MyIntroPicture
    TryAgain = True
    Resume Next
End Sub

Public Sub AdjustUfisIntroDetails()
    Dim NewSize As Long
    Dim i As Integer
    Dim ActPicX
    Dim ActPicY
    Dim ActZoom
    On Error Resume Next
    If MyDetailsIsOpen Then
        For i = 0 To 1
            SlideFullSize.UfisIntroPicture(i).Stretch = False
            SlideFullSize.UfisIntroPicture(i).Visible = False
            SlideFullSize.UfisIntroPicture(i).Picture = UfisPicForm.UfisIntroPicture(i).Picture
            ActPicX = SlideFullSize.UfisIntroPicture(i).Width
            ActPicY = SlideFullSize.UfisIntroPicture(i).Height
            NewSize = (SlideFullSize.Width) / 2
            If ActPicX > NewSize Then
                SlideFullSize.UfisIntroPicture(i).Stretch = True
                ActZoom = ActPicX / NewSize
                SlideFullSize.UfisIntroPicture(i).Width = ActPicX / ActZoom
                SlideFullSize.UfisIntroPicture(i).Height = ActPicY / ActZoom
            End If
            NewSize = SlideFullSize.Height
            ActPicX = SlideFullSize.UfisIntroPicture(i).Width
            ActPicY = SlideFullSize.UfisIntroPicture(i).Height
            If ActPicY > NewSize Then
                SlideFullSize.UfisIntroPicture(i).Stretch = True
                ActZoom = ActPicY / NewSize
                SlideFullSize.UfisIntroPicture(i).Width = ActPicX / ActZoom
                SlideFullSize.UfisIntroPicture(i).Height = ActPicY / ActZoom
            End If
            SlideFullSize.UfisIntroFrame(i).Width = SlideFullSize.UfisIntroPicture(i).Width
            SlideFullSize.UfisIntroFrame(i).Height = SlideFullSize.UfisIntroPicture(i).Height
            SlideFullSize.UfisIntroPicture(i).Visible = True
            SlideFullSize.UfisIntroFrame(i).Visible = True
        Next
        SlideFullSize.UfisIntroFrame(0).Top = ((SlideFullSize.Height - SlideFullSize.UfisIntroPicture(0).Height) \ 2)
        SlideFullSize.UfisIntroFrame(0).Left = ((SlideFullSize.Width \ 2) - SlideFullSize.UfisIntroPicture(0).Width) \ 2
        SlideFullSize.UfisIntroFrame(1).Top = ((SlideFullSize.Height - SlideFullSize.UfisIntroPicture(1).Height) \ 2)
        SlideFullSize.UfisIntroFrame(1).Left = (((SlideFullSize.Width \ 2) - SlideFullSize.UfisIntroPicture(1).Width) \ 2) + (SlideFullSize.Width \ 2)
        If UfisPicForm.UfisIntroFrame(2).Visible = True Then
            SlideFullSize.UfisIntroPicture(2).Stretch = False
            SlideFullSize.UfisIntroPicture(2).Visible = False
            SlideFullSize.UfisIntroPicture(2).Picture = UfisPicForm.UfisIntroPicture(2).Picture
            NewSize = SlideFullSize.Height
            ActPicX = SlideFullSize.UfisIntroPicture(2).Width
            ActPicY = SlideFullSize.UfisIntroPicture(2).Height
            If ActPicY > NewSize Then
                SlideFullSize.UfisIntroPicture(2).Stretch = True
                ActZoom = ActPicY / NewSize
                SlideFullSize.UfisIntroPicture(2).Width = ActPicX / ActZoom
                SlideFullSize.UfisIntroPicture(2).Height = ActPicY / ActZoom
            End If
            NewSize = SlideFullSize.Width
            ActPicX = SlideFullSize.UfisIntroPicture(2).Width
            ActPicY = SlideFullSize.UfisIntroPicture(2).Height
            If ActPicX > NewSize Then
                SlideFullSize.UfisIntroPicture(2).Stretch = True
                ActZoom = ActPicX / NewSize
                SlideFullSize.UfisIntroPicture(2).Width = ActPicX / ActZoom
                SlideFullSize.UfisIntroPicture(2).Height = ActPicY / ActZoom
            End If
            SlideFullSize.UfisIntroFrame(2).Width = SlideFullSize.UfisIntroPicture(2).Width
            SlideFullSize.UfisIntroFrame(2).Height = SlideFullSize.UfisIntroPicture(2).Height
            SlideFullSize.UfisIntroFrame(2).Top = ((SlideFullSize.Height - SlideFullSize.UfisIntroPicture(2).Height) \ 2)
            SlideFullSize.UfisIntroFrame(2).Left = (SlideFullSize.Width - SlideFullSize.UfisIntroPicture(2).Width) \ 2
            SlideFullSize.UfisIntroPicture(2).Visible = True
            SlideFullSize.UfisIntroFrame(2).Visible = True
            SlideFullSize.UfisIntroFrame(2).ZOrder
        Else
            SlideFullSize.UfisIntroFrame(2).Visible = False
        End If
        SlideFullSize.Refresh
    End If
End Sub


