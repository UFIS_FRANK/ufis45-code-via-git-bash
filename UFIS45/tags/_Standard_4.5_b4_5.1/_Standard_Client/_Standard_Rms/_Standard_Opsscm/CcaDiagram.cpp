// CcaDiagram.cpp : implementation file
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <opsscm.h>
#include <CcaDiagram.h>
#include <InitialLoadDlg.h>
#include <CcaDiaPropertySheet.h>
#include <CcaCedaFlightData.h>
#include <CViewer.h>
#include <CcaChart.h>
#include <CcaGantt.h>
#include <TimePacket.h>
#include <CicDemandTableDlg.h>
#include <CicNoDemandTableDlg.h>
#include <CicConfTableDlg.h>
#include <CCAExpandDlg.h>
#include <DataSet.h>
#include <DelelteCounterDlg.h>
#include <AllocateCca.h>
#include <AllocateCcaParameter.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void CcaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(CcaDiagram, CFrameWnd)

CcaDiagram::CcaDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("CCADIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogCcaDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	pogCicDemandTableDlg = NULL;//new CicDemandTableDlg(this);
}

CcaDiagram::~CcaDiagram()
{

	if(pogCicDemandTableDlg != NULL)
	{
		delete pogCicDemandTableDlg;
	}
	if(pogCicNoDemandTableDlg != NULL)
	{
		delete pogCicNoDemandTableDlg;
	}
	if(pogCicConfTableDlg != NULL)
	{
		delete pogCicConfTableDlg;
	}
	if(pogCicFConfTableDlg != NULL)
	{
		delete pogCicFConfTableDlg;
	}
	if (pogCommonCcaDlg != NULL)
	{
		pogCommonCcaDlg->DestroyWindow();
		delete pogCommonCcaDlg;
		pogCommonCcaDlg = NULL;
	}
	if (pogSeasonDlg != NULL)
	{
		pogSeasonDlg->DestroyWindow();
		delete pogSeasonDlg;
		pogSeasonDlg = NULL;
	}
    omPtrArray.RemoveAll();
	pogCcaDiagram = NULL;
}


BEGIN_MESSAGE_MAP(CcaDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(CcaDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
	ON_BN_CLICKED(IDC_CICDEMAND, OnDemand )
	ON_BN_CLICKED(IDC_CIC_CONF, OnCicConf )
	ON_BN_CLICKED(IDC_COMMON_CCA, OnCommonCca )
	ON_BN_CLICKED(IDC_EXPAND, OnExpand )
	ON_BN_CLICKED(IDC_ALLOCATE, OnAllocate)
	ON_BN_CLICKED(IDC_FLIGHTS_WITHOUT_DEM, OnFlightsWithoutResource)
	ON_BN_CLICKED(IDC_DELETE_TIMEFRAME, OnDeleteTimeFrame)
	ON_BN_CLICKED(IDC_PRINT_GANTT, OnPrintGantt)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_BN_CLICKED(IDC_PREV, OnNextChart)
    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
    ON_CBN_SELCHANGE(IDC_SEASON_SEL, OnSeasonViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static void CcaDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CcaDiagram *polDiagram = (CcaDiagram *)popInstance;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CcaDiagram message handlers
void CcaDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    CcaChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (CcaChart *) omPtrArray.GetAt(ilIndex);
        
        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }
        
        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;

        olChartRect.top = ilLastY;

        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
        
        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        
        polChart->MoveWindow(&olChartRect, FALSE);
		polChart->ShowWindow(SW_SHOW);
	}
    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}


void CcaDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
        
    char clBuf[64];
    sprintf(clBuf, "%02d%02d%02d  %d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

CListBox *CcaDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);

	// Searching for the bottommost chart
	CcaChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (CcaChart *)omPtrArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}

	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omPtrArray.GetSize()-1))
		return NULL;

	return &((CcaChart *)omPtrArray[ilLc])->omGantt;
}

void CcaDiagram::SetCaptionText(void)
{
	SetWindowText("");
}


int CcaDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	{
	pogCicDemandTableDlg = new CicDemandTableDlg(this);
	pogCicNoDemandTableDlg = new CicNoDemandTableDlg(this);
	pogCicConfTableDlg = new CicConfTableDlg(this);
	pogCicFConfTableDlg = new CicFConfTableDlg(this);
	pogCommonCcaDlg = new CCcaCommonDlg(this);

	CRect olRect;
	GetWindowRect(olRect);

	pogCicNoDemandTableDlg->MoveWindow(CRect(olRect.right - 500, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25));

	pogCicDemandTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.top + pogButtonList->m_nDialogBarHeight + 25, olRect.right -25, olRect.top + 550));

	pogCicConfTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25));

	pogCicFConfTableDlg->MoveWindow(CRect(olRect.right - 1024, olRect.bottom - 300, olRect.right - 25, olRect.bottom - 25));

	pogCicNoDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicDemandTableDlg->ShowWindow(SW_HIDE);
	pogCicConfTableDlg->ShowWindow(SW_HIDE);
	pogCicFConfTableDlg->ShowWindow(SW_HIDE);
	pogCommonCcaDlg->ShowWindow(SW_HIDE);
	}
/*
	pogCicDemandTableDlg = new CicDemandTableDlg(this);
	CRect olNoResRect;
	pogCicDemandTableDlg->GetWindowRect(&olNoResRect);
	int Nleft, Ntop, Nright, Nbottom;
	CRect olThisRect;
	GetWindowRect(&olThisRect);
	Nleft = 0;
	Ntop = pogButtonList->m_nDialogBarHeight;
	Nbottom = Ntop+500;
	Nleft = GetSystemMetrics(SM_CXSCREEN) - 1024;
	Nright = GetSystemMetrics(SM_CXSCREEN);


	olNoResRect.top = Ntop;
	olNoResRect.bottom = Nbottom;
	olNoResRect.left = Nleft;
	olNoResRect.right = Nright;
	WINDOWPLACEMENT rlP;
    rlP.flags = WPF_SETMINPOSITION;
    rlP.showCmd = SW_MINIMIZE;
	rlP.ptMinPosition = CPoint(Nright - 190, pogButtonList->m_nDialogBarHeight + 1);
	rlP.ptMaxPosition = CPoint(Nleft, Ntop);
	rlP.rcNormalPosition = CRect(Nleft, Ntop - pogButtonList->m_nDialogBarHeight + 1, Nright, Nbottom);
 	pogCicDemandTableDlg->SetWindowPlacement( &rlP); 

*/


    SetTimer(0, (UINT) 60 * 1000, NULL);
    SetTimer(1, (UINT) 60 * 1000 * 2, NULL);

	omDialogBar.Create( this, IDD_CCADIAGRAM, CBRS_TOP, IDD_CCADIAGRAM );
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	//CButton *polCB;


	if(((ogCustomer == "LIS") && (ogAppName == "FIPS")) || 	((ogCustomer == "HAJ") && (ogAppName == "FPMS")))
	{
	    ((CButton *) omDialogBar.GetDlgItem(IDC_CICDEMAND))->ShowWindow(SW_HIDE);
	    ((CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE))->ShowWindow(SW_HIDE);
	    ((CButton *) omDialogBar.GetDlgItem(IDC_FLIGHTS_WITHOUT_DEM))->ShowWindow(SW_HIDE);
	    ((CButton *) omDialogBar.GetDlgItem(IDC_DELETE_TIMEFRAME))->ShowWindow(SW_HIDE);
	    ((CButton *) omDialogBar.GetDlgItem(IDC_EXPAND))->ShowWindow(SW_HIDE);
	    ((CButton *) omDialogBar.GetDlgItem(IDC_CIC_CONF))->ShowWindow(SW_HIDE);
	}

    ((CButton *) omDialogBar.GetDlgItem(IDC_SEASON_SEL))->ShowWindow(SW_HIDE);



    omViewer.Attach(this);
	UpdateComboBox();

    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();

	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olCurrentTime);


    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[64];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];
	
	sprintf(pclDate, "%02d.%02d.%02dz", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
	sprintf(pclTimes, "%02d:%02d/%02d:%02dz", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE, CRect(olRect.right - 184, 3, olRect.right - 84, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,  CRect(olRect.right - 82, 3, olRect.right - 4, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
	
	

    sprintf(olBuf, "%02d%02d%02d  %d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100, GetDayOfWeek(omTSStartTime));
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	omBB1.ShowWindow(SW_HIDE);
	omBB2.ShowWindow(SW_HIDE);

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
    
    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    CcaChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CcaChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

    OnTimer(0);

	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), CcaDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), CcaDiagramCf);	// for updating the yellow lines

	 
	ogCcaDiaFlightData.Register();	 

	//SetWndPos();	

	 return 0;

}



void CcaDiagram::OnBeenden() 
{
	ogCcaDiaFlightData.UnRegister();	 
	ogCcaDiaFlightData.ClearAll();	 
	DestroyWindow();	
}


void CcaDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}


void CcaDiagram::OnAnsicht()
{
//	CViewer olViewer;
	//omViewer.SelectView(<Default>);

	//MWO
	SetFocus();
	//END MWO

	CcaDiaPropertySheet olDlg("CCADIA", this, &omViewer, 0, GetString(IDS_STRING1646));
	bmIsViewOpen = true;
	if(olDlg.DoModal() != IDCANCEL)
	{
		omViewer.SelectView(omViewer.GetViewName());

		
		switch(igAnsichtPageIndex)
		{
		case CCA_ZEITRAUM_PAGE:
			LoadFlights();
			break;
		case CCA_FLUGSUCHEN_PAGE:
			//MessageBox("Flug Suchen", "", MB_OK);
			break;
		case CCA_GEOMETRIE_PAGE:
			//MessageBox("Geometrie", "", MB_OK);
			break;
		default:
			break;
		}
		omViewer.MakeMasstab();
		CTime olT = omViewer.GetGeometrieStartTime();

		CTime olFrom;
		CTime olTo;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


		CString olS = olT.Format("%d.%m.%Y-%H:%M");

		if(	bgCcaDiaSeason )
			olT = ogCcaDiaDate;


		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olT); //RSTL


		SetTSStartTime(olT);
		omTSDuration = omViewer.GetGeometryTimeSpan();
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
		ChangeViewTo(omViewer.GetViewName(), false);
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
		UpdateComboBox();
		ActivateTables();
	}
	bmIsViewOpen = false;


}

bool CcaDiagram::IsPassFlightFilter(CCAFLIGHTDATA *prpFlight)
{
	bool blRet = false;
	{
		/*

		if(strcmp(prpFlight->Htyp, "K") != 0)
		{
			if(strcmp(prpFlight->Ttyp, "CG") != 0)
			{
				if(strcmp(prpFlight->Flns, "F") != 0)
				{
					if(strcmp(prpFlight->Htyp, "T") != 0)
					{
						blRet = true;
					}
				}
			}
		}
		*/
	}
	return blRet;
}

bool CcaDiagram::HasAllocatedCcas(CCSPtrArray<DIACCADATA> &ropCcaList)
{
	bool blRet = false;
	int ilCount = ropCcaList.GetSize();
	for(int i = 0; ((i < ilCount) && (blRet == false)); i++)
	{
	}
	return blRet;
}


void CcaDiagram::LoadFlights(char *pspView)
{
	CString olView;

	if(pspView != NULL)
	{
		omViewer.SelectView(pspView);
		olView = CString(pspView);
	}
	else
	{
		omViewer.SelectView(omViewer.GetViewName());
		olView = CString(omViewer.GetViewName());
	}
	
	CString olWhere;
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	int ilDOO;
	bool blRotation;



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();
	
	ogDataSet.InitCcaConflictStrings();


	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	ogCcaDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);


	ilDOO = omViewer.GetDOO();

	char pclSelection[7000] = "";
	ogCcaDiaDate = TIMENULL;


    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_SEASON_SEL);


	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin read:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	if( (ilDOO >= 0) )
	{
		polCB->ShowWindow(SW_SHOW);
		polCB->ResetContent();
		polCB->AddString(CString(olView));
		polCB->SelectString(-1, CString(olView));

		ogCcaDiaSingleDate = TIMENULL;

		CString olText;
		ogCcaDiaFlightData.ClearAll();

		CTime olStart = olFrom;
		CTime olEnd;

		CString olFromTime	= olFrom.Format("%H%M");
		CString olToTime	= olTo.Format("%H%M");

		CTimeSpan olOneDay(1,0,0,0);

		TRACE("\nSTART %s------------------------------", CTime::GetCurrentTime().Format("%H:%M:%S"));

		while(olStart < olTo)
		{
			
			if(ilDOO == GetDayOfWeek(olStart))
			{
				olWhere = "";
				olStart = HourStringToDate(olFromTime, olStart);
				olEnd   = HourStringToDate(olToTime, olStart);


				if(ogCcaDiaDate == TIMENULL)
					ogCcaDiaDate = olStart;


				polCB->AddString(olStart.Format("%d.%m.%Y"));


				omViewer.GetFlightSearchWhereString(olWhere, blRotation, olStart, olEnd);

				strcpy(pclSelection, olWhere);

				olText = CString("Fl�ge: ") + olStart.Format("%d.%m.%Y");
				omStatusBar.SetPaneText(0, olText);
				omStatusBar.UpdateWindow();
				ogCcaDiaFlightData.ReadFlights(pclSelection, false, true);

				olText = CString("Cca: ") + olStart.Format("%d.%m.%Y");
				omStatusBar.SetPaneText(0, olText);
				omStatusBar.UpdateWindow();
				ogCcaDiaFlightData.ReadCcas(olStart, olEnd);

				//TRACE("\n %s", olWhere);

			}
			olStart += olOneDay;
		}

		TRACE("\nEnd read:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
		TRACE("\n-----------------------------------------------");

		omViewer.SetLoadTimeFrame(ogCcaDiaDate, olEnd);
		

		bgCcaDiaSeason = true;


	}
	else
	{
		polCB->ShowWindow(SW_HIDE);

		bgCcaDiaSeason = false;
		ogCcaDiaDate = TIMENULL;

		omViewer.GetZeitraumWhereString(olWhere, blRotation);

		//olWhere += CString(" AND FLNO > ' '");
	
		ogCcaDiaFlightData.ReadAllFlights(olWhere, blRotation);

	}


	TRACE("\nBegin read BLK:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	CString olText = CString("Blk... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

	char pclWhere[512]="";
	
	sprintf(pclWhere, " WHERE (NATO >= '%s' and NAFR <= '%s') OR (NAFR <= '%s' and NATO = ' ') OR  (NATO >= '%s'  and NAFR = ' ')",
												 olFrom.Format("%Y%m%d%H%M00"), olTo.Format("%Y%m%d%H%M59"),
												 olTo.Format("%Y%m%d%H%M59"),
												 olFrom.Format("%Y%m%d%H%M00"));
	ogBCD.Read(CString("BLK"), pclWhere);


	TRACE("\nEnd read BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	CString olCnams = ogCcaDiaFlightData.omCcaData.MakeBlkData(TIMENULL, ilDOO);

	TRACE("\nEnd Make BLK:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	omViewer.SetCnams(olCnams);
	omViewer.SetDOO(ilDOO);


	//int ilC5 = ogBCD.GetDataCount("BLK");


	//int ilBnam = ogBCD.GetFieldIndex("CIC", "CNAM");
	//ogBCD.SetSort("CIC", "TERM+,CNAM+", true); //true ==> sort immediately




 
	//END MWO
//----------------------------------------------------------------------------------------------------
	

	if(bgCcaDiaSeason)
	{
		TRACE("\nBegin compress:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

		olText = CString("Compress FLights/Cki... ");
		omStatusBar.SetPaneText(0, olText);
		omStatusBar.UpdateWindow();

		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();

		TRACE("\nEnd compress:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
		TRACE("\n-----------------------------------------------");
	
	}

	TRACE("\nBegin tables:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	olText = CString("Load tables... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();

	ActivateTables();

	TRACE("\nEnd tables:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");




	TRACE("\n ANZ FLIGHT: %d\n", ogCcaDiaFlightData.omData.GetSize());
	TRACE("\n ANZ CCA   : %d\n", ogCcaDiaFlightData.omCcaData.omData.GetSize());
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");

	olText = CString("Check overlapping... ");
	omStatusBar.SetPaneText(0, olText);
	omStatusBar.UpdateWindow();
	
	TRACE("\nBegin Overlap:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	int ilCount = ogBCD.GetDataCount("CIC");

	for(int i = 0; i < ilCount; i++)
	{
		ogDataSet.CheckCCaForOverlapping(ogBCD.GetField("CIC", i, "CNAM"));
	}
	TRACE("\nEnd Overlap:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");




	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

}

void CcaDiagram::OnDestroy() 
{
//	if (bgModal == TRUE)
//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("CcaDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void CcaDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void CcaDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL CcaDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(SILVER/*lmBkColor*/);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void CcaDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void CcaDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	//PositionChild();
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
//	SetAllStaffAreaButtonsColor();

	GetClientRect(&olRect);
	
	omTime.MoveWindow( CRect(olRect.right - 150, 3, olRect.right - 66, 22));

    omDate.MoveWindow( CRect(olRect.right - 66, 3, olRect.right - 2, 22));


}

void CcaDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);


	CUIntArray olLines;

	int ilCurrLine;

	CcaChart *polChart;
	for (int ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (CcaChart *)omPtrArray[ilLc];

		ilCurrLine = polChart->omGantt.GetTopIndex();
		olLines.Add(ilCurrLine);
	}


    long llTotalMin;
    int ilPos;
    
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////

        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

			SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
			////ChangeViewTo(omViewer.SelectView());
            
            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            
            SetScrollPos(SB_HORZ, nPos, TRUE);

            //char clBuf[64];
            //wsprintf(clBuf, "Thumb Position : %d", nPos);
            //omStatusBar.SetWindowText(clBuf);
        break;

        //case SB_THUMBPOSITION /* released */:
            //OutputDebugString("ThumbPosition\n\r");
        //break;
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        case SB_THUMBPOSITION:	// the thumb was just released?
			ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV,TRUE);
            omClientWnd.Invalidate(FALSE);

////////////////////////////////////////////////////////////////////////

        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }


	for (ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (CcaChart *)omPtrArray[ilLc];
		polChart->omGantt.SetTopIndex(olLines[ilLc]);
	}


}

void CcaDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void CcaDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omPtrArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void CcaDiagram::OnFirstChart()
{
	imFirstVisibleChart = 0;
	OnUpdatePrevNext();
	PositionChild();
}

void CcaDiagram::OnLastChart()
{
	imFirstVisibleChart = omPtrArray.GetUpperBound();
	OnUpdatePrevNext();
	PositionChild();
}

void CcaDiagram::OnTimer(UINT nIDEvent)
{
	if(bmNoUpdatesNow == FALSE)
	{


		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		char pclTimes[100];
		char pclDate[100];
		
		sprintf(pclDate, "%02d.%02d.%02dz", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d/%02d:%02dz", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());

		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);

		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);

		CcaChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (CcaChart *) omPtrArray.GetAt(ilIndex);

			if(bgGatPosLocal)
				polChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
			else
				polChart->GetGanttPtr()->SetCurrentTime(olUtcTime);
		}


		CFrameWnd::OnTimer(nIDEvent);
	}
}

void CcaDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
		CcaChart *polChart;
		for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
		{
			polChart = (CcaChart *) omPtrArray.GetAt(ilIndex);
			polChart->SetMarkTime(opStartTime, opEndTime);
		}
}


LONG CcaDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG CcaDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    CcaChart *polCcaChart = (CcaChart *) omPtrArray.GetAt(ipGroupNo);
    CcaGantt *polCcaGantt = polCcaChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            // CreateChild(ipGroupNo);

            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            polCcaChart->GetChartButtonPtr()->SetWindowText(olStr);
            polCcaChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
            polCcaChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            polCcaChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;
        
        case UD_DELETEGROUP :
            delete omPtrArray.GetAt(ipGroupNo);
            //omPtrArray.GetAt(ipGroupNo) -> DestroyWindow();
            omPtrArray.RemoveAt(ipGroupNo);
            
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart--;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        // line message
        case UD_INSERTLINE :
            polCcaGantt->InsertString(ipLineNo, "");
			polCcaGantt->RepaintItemHeight(ipLineNo);
			
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polCcaChart->GetCountTextPtr()->SetWindowText(clBuf);
			polCcaChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polCcaGantt->RepaintVerticalScale(ipLineNo);
            polCcaGantt->RepaintGanttChart(ipLineNo);
			//SetStaffAreaButtonColor(ipGroupNo);
        break;

        case UD_DELETELINE :
            polCcaGantt->DeleteString(ipLineNo);
            
			ilCount = omViewer.GetLineCount(ipGroupNo);
			sprintf(clBuf, "%d", ilCount);
			polCcaChart->GetCountTextPtr()->SetWindowText(clBuf);
			polCcaChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ipGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polCcaGantt->RepaintItemHeight(ipLineNo);
			//SetStaffAreaButtonColor(ipGroupNo);
            PositionChild();
		break;
    }

    return 0L;
}

void CcaDiagram::OnUpdatePrevNext(void)
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	SetFocus();
	//END MWO

	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);

	if (imFirstVisibleChart == omPtrArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void CcaDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void CcaDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	if (bgNoScroll == TRUE)
		return;

	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();

	CRect olRect; omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));


    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		if (RememberPositions == TRUE)
		{
			int ilState = ((CcaChart *)omPtrArray[ilIndex])->GetState();
			olChartStates.NewAt(ilIndex,ilState);
		}
		// Id 30-Sep-96
		// This will remove a lot of warning message when the user change view.
		// If we just delete a staff chart, MFC will produce two warning message.
		// First, Revoke not called before the destructor.
		// Second, calling DestroyWindow() in CWnd::~CWnd.
        //delete (CcaChart *)omPtrArray.GetAt(ilIndex);
		((CcaChart *)omPtrArray[ilIndex])->DestroyWindow();
    }
    omPtrArray.RemoveAll();

    CcaChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new CcaChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
		if (ilI < olChartStates.GetSize())
		{
			polChart->SetState(olChartStates[ilI]);
		}
		else
		{
				polChart->SetState(Maximized);
		}

        polChart->Create(NULL, "CcaChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);

		CTime olCurr = CTime::GetCurrentTime();
		if(!bgGatPosLocal)
			ogBasicData.LocalToUtc(olCurr);
		polChart->GetGanttPtr()->SetCurrentTime(olCurr);
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	olChartStates.DeleteAll();
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void CcaDiagram::OnViewSelChange()
{
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;
	LoadFlights(clText);
	omViewer.SelectView(clText);
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();

	if(	bgCcaDiaSeason )
		olT = ogCcaDiaDate;


	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	//if((olFrom > olT) || (olTo < olT))
	//	olT = olFrom;

	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(clText, false);

	ActivateTables();
}


void CcaDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	SetFocus();
	//END MWO
}

void CcaDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//MWO
	SetFocus();
	//END MWO

	// TODO: Add your command handler code here
	CTime olTime = CTime::GetCurrentTime();

	if(!bgGatPosLocal)
		ogBasicData.LocalToUtc(olTime);


	//Die wollen local
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
}


BOOL CcaDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogCcaDiagram = NULL; 

	delete pogCommonCcaTable;
	pogCommonCcaTable = NULL;

	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// CcaDiagram keyboard handling

void CcaDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
	case VK_PRIOR:
		blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		blIsControl? OnLastChart(): OnNextChart();
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void CcaDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// CcaDiagram -- implementation of DDX call back function

static void CcaDiagramCf(void *popInstance, enum enumDDXTypes ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CcaDiagram *polDiagram = (CcaDiagram *)popInstance;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	}
}

void CcaDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
	SetCaptionText();
}

void CcaDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void CcaDiagram::UpdateTimeBand()
{
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		CcaChart *polChart = (CcaChart *)omPtrArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
}

LONG CcaDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}






void CcaDiagram::OnExpand()
{
	CCAExpandDlg olDlg;
	olDlg.DoModal();

}
void CcaDiagram::OnDeleteTimeFrame()
{
	DelelteCounterDlg *polDlg = new DelelteCounterDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		OnViewSelChange();
	}
}

void CcaDiagram::OnPrintGantt()
{
	omViewer.PrintGantt(omPtrArray);
}

void CcaDiagram::OnAllocate()
{
	int ilRet = 0;
	AllocateCcaParameter *polDlg = new AllocateCcaParameter(this);
	ilRet = polDlg->DoModal();
	delete polDlg;
	
	if(ilRet == -1)
		return;

	AllocateCca olAlloc(ilRet);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1492)); // Einteilung l�uft---------------------
	omStatusBar.UpdateWindow();
	olAlloc.Allocate();
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1493)); // Speichern des Plans---------------------
	omStatusBar.UpdateWindow();

	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin Save Allo: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	ogCcaDiaFlightData.omCcaData.Release();

	TRACE("\nEnd Save Allo:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");

	if(bgCcaDiaSeason)
	{

		TRACE("\n-----------------------------------------------");
		TRACE("\nBegin compress: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	
		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();

		TRACE("\nEnd compress:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
		TRACE("\n-----------------------------------------------");
	
	}

	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin repaint: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	RepaintAll(0,0);

	TRACE("\nend repaint:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");


	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin tables: %s", CTime::GetCurrentTime().Format("%H:%M:%S"));

	
	ActivateTables();
	
	TRACE("\nEnd tables:   %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}



void CcaDiagram::OnCommonCca()
{

	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);


	if (pogCommonCcaTable == NULL)
	{
		pogCommonCcaTable = new CCcaCommonTableDlg(this, olFrom, olTo);
	}
	else
	{
		pogCommonCcaTable->SetTimeFrame(olFrom, olTo);
		pogCommonCcaTable->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
	}

}


void CcaDiagram::SetWndPos()
{
	CRect olRect;

	GetWindowRect(&olRect);

	int ilWidth = olRect.right -olRect.left;
	int ilWhichMonitor = ogCfgData.GetMonitorForWindow(CString(MON_CCADIA_STRING));
	int ilMonitors = ogCfgData.GetMonitorForWindow(CString(MON_COUNT_STRING));
	int left, top, right, bottom;
	int ilCXMonitor;

	bottom = ::GetSystemMetrics(SM_CYSCREEN);

	top = 0;  


	imWhichMonitor = ilWhichMonitor;
	imMonitorCount = ilMonitors;

	if(ilWhichMonitor == 1)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 2)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}
	else if(ilWhichMonitor == 3)
	{
		switch(ilMonitors)
		{
		case 1:
			ilCXMonitor = ::GetSystemMetrics(SM_CXSCREEN);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 2:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/2);
			left = (int)((ilCXMonitor-ilWidth)/2);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		case 3:
			ilCXMonitor = (int)(::GetSystemMetrics(SM_CXSCREEN)/3);
			left = (int)((ilCXMonitor-ilWidth)/2)+(2*ilCXMonitor);
			right = left + ilWidth;//GetSystemMetrics(SM_CXSCREEN);
			break;
		default:
			right = GetSystemMetrics(SM_CXSCREEN);
			break;
		}
	}

	MoveWindow(CRect(left, top, right, ::GetSystemMetrics(SM_CYSCREEN)));


	//SetWindowPos(&wndTop,left , top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);



}



void CcaDiagram::OnFlightsWithoutResource()
{
	pogCicNoDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicNoDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );
}


void CcaDiagram::OnDemand()
{
	pogCicDemandTableDlg->ShowWindow(SW_SHOW);
	pogCicDemandTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}
 

void CcaDiagram::OnCicConf()
{
	pogCicConfTableDlg->ShowWindow(SW_SHOW);
	pogCicConfTableDlg->SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

}



void CcaDiagram::OnSeasonViewSelChange()
{

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
    
	CString olText;
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_SEASON_SEL);
    
	polCB->GetLBText(polCB->GetCurSel(), olText);
	

	if(olText == CString(omViewer.GetViewName()))
	{
		ogCcaDiaSingleDate = TIMENULL;
		omTSStartTime = CTime(ogCcaDiaDate.GetYear(),ogCcaDiaDate.GetMonth(), ogCcaDiaDate.GetDay(), 0,0,0 );
		ogCcaDiaFlightData.Kompress();
		ogCcaDiaFlightData.omCcaData.KompressND();
	}
	else
	{
		ogCcaDiaSingleDate = DateStringToDate(olText);
		omTSStartTime = CTime(ogCcaDiaSingleDate.GetYear(),ogCcaDiaSingleDate.GetMonth(), ogCcaDiaSingleDate.GetDay(), 0,0,0 );
	}
	

	//omViewer.MakeMasstab();
	//CTime olT = omViewer.GetGeometrieStartTime();


	SetTSStartTime(omTSStartTime);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(omViewer.SelectView(), false);

	ActivateTables();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}

void CcaDiagram::ActivateTables()
{
	pogCicDemandTableDlg->Reset();
	pogCicDemandTableDlg->Activate();

	pogCicNoDemandTableDlg->Reset();
	pogCicNoDemandTableDlg->Activate();
	
	pogCicConfTableDlg->Reset();
	pogCicConfTableDlg->Activate();

}

