#if !defined(AFX_CCIDEMANDDETAILDLG_H__CA3D3C53_13B9_11D5_80E4_00010215BFDE__INCLUDED_)
#define AFX_CCIDEMANDDETAILDLG_H__CA3D3C53_13B9_11D5_80E4_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CciDemandDetailDlg.h : header file
//

#include <CCSTable.h>
#include <resource.h>
#include <CcaCedaFlightData.h>

//void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, 
//					  CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CciDemandDetailDlg dialog

class CciDemandDetailDlg : public CDialog
{
// Construction
public:
//	CciDemandDetailDlg(CWnd* pParent,long lpDemUrno,char* opCounterName);   // standard constructor
	CciDemandDetailDlg(CWnd* pParent,CUIntArray &lpDemUrno,char* opCounterName);   // standard constructor

	void Register(void);
	void ProcessDemandChange(const long *plpDemUrno);
	void ProcessFlightChange(const CCAFLIGHTDATA *prpFlight);

// Dialog Data
	//{{AFX_DATA(CciDemandDetailDlg)
	enum { IDD = IDD_CCI_DEMANDDETAIL };
	CString	m_RuleName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CciDemandDetailDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CciDemandDetailDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSchlieben();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
    void		UpdateDisplay();
	void InitTable();
	void DrawHeader();
	void InitDialog();
	CString		Format(CCAFLIGHTDATA *prpFlight);
private : // implementation data
    CCSTable*	omTable; // visual object, the table content
//	long		lmDemUrno;	
	CUIntArray	lmDemUrno;	

	CString		omCounter;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCIDEMANDDETAILDLG_H__CA3D3C53_13B9_11D5_80E4_00010215BFDE__INCLUDED_)
