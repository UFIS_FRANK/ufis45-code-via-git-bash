// Utils.cpp : Defines utility functions
//
// Modification History: 
// 22-nov-00	rkr		IsPostFlight() added
// 05-dec-00	rkr		ModifyWindowText() added
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <BasicData.h>

#include <Utils.h>



int InsertSortInStrArray(CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr < ropStrArray[i])
		{
			break;
		}
	}
	ropStrArray.InsertAt(i, ropStr);
	return i;
}


bool FindInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return true;
	}
	return false;
}


int FindIdInStrArray(const CStringArray &ropStrArray, const CString &ropStr) 
{
	for (int i=0; i < ropStrArray.GetSize(); i++)
	{
		if (ropStr == ropStrArray[i])
			return i;
	}
	return -1;
}


bool IsRegularFlight(char Ftyp)
{
	return (Ftyp != 'T' && Ftyp != 'G');
}


bool IsArrivalFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsArrival(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsArrival(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpDes, pcgHome) == 0);
}



bool IsDepartureFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsDeparture(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));		
}


bool IsDeparture(const char *pcpOrg, const char *pcpDes)
{
	return (strcmp(pcpOrg, pcgHome) == 0);
}
 

bool IsCircular(const char *pcpOrg, const char *pcpDes)
{
	return (IsArrival(pcpOrg, pcpDes) && IsDeparture(pcpOrg, pcpDes));
}


bool IsCircularFlight(const char *pcpOrg, const char *pcpDes, char Ftyp)
{
	return (IsCircular(pcpOrg, pcpDes) && IsRegularFlight(Ftyp));
}


bool DelayCodeToAlpha(char *pcpDcd)
{
	CString olDcdAlpha;
	if (!ogBCD.GetField("DEN", "DECN", CString(pcpDcd), "DECA", olDcdAlpha))
		return false;

	strncpy(pcpDcd, olDcdAlpha, 3);  
	return true;
}


bool DelayCodeToNum(char *pcpDcd)
{
	CString olDcdNum;
	if (!ogBCD.GetField("DEN", "DECA", CString(pcpDcd), "DECN", olDcdNum))
		return false;

	strncpy(pcpDcd, olDcdNum, 3);  
	return true;
}



int FindInListBox(const CListBox &ropListBox, const CString &ropStr)
{
	int ilCount = ropListBox.GetCount();
	CString olItem;
	for (int i = 0; i < ilCount; i++)
	{
		ropListBox.GetText(i, olItem);
		if (olItem == ropStr)
			return i;
	}
	return -1;
}


// A substitute for CListBox::GetSelItems(...)
int MyGetSelItems(CListBox &ropLB, int ipMaxAnz, int *prpItems)
{
	int ilAnz = 0;
	// Scan all Items
	for (int ilLBCount = 0; ilLBCount < ropLB.GetCount(); ilLBCount++)
	{
		if (ilAnz >= ipMaxAnz) break;

		if (ropLB.GetSel(ilLBCount) > 0)
		{
			// store index in array
			prpItems[ilAnz] = ilLBCount;
			ilAnz++;
		}
	}
	// return the count of selected items
	return ilAnz;
}


COLORREF GetColorOfFlight(char FTyp)
{
	ASSERT(FALSE);
	return lgBkColor;
/*
	switch (FTyp)
	{
	case 'S': return COLOR_FLIGHT_SCHEDULED;
	case ' ':
	case '\0': return COLOR_FLIGHT_PROGNOSE;
	case 'D': return COLOR_FLIGHT_DIVERTED;
	case 'R': return COLOR_FLIGHT_REROUTED;
	case 'B': return COLOR_FLIGHT_RETTAXI;
	case 'Z': return COLOR_FLIGHT_RETFLIGHT;
	case 'X': return COLOR_FLIGHT_CXX;
	case 'N': return COLOR_FLIGHT_NOOP;
	}

	return COLOR_FLIGHT_OPERATION;
*/ 
}


long MyGetUrnoFromSelection(const CString &ropSel)
{
	int ilFirst = ropSel.Find("URNO");
	if (ilFirst == -1) return -1;
	ilFirst += 4;

	CString olNums("0123456789");

	while (ilFirst < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilFirst]) != -1)
			break;
		ilFirst++;
	}
	if (ilFirst >= ropSel.GetLength())
		return -1;

	int ilLast = ilFirst;
	while (ilLast < ropSel.GetLength())
	{
		if (olNums.Find(ropSel[ilLast]) == -1)
			break;
		ilLast++;
	}

	long llUrno = atol(ropSel.Mid(ilFirst,ilLast-ilFirst));
	if (llUrno == 0)
		return -1;
	else
		return llUrno;
}



bool GetPosDefAllocDur(const CString &ropPos, int ipMinGT, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("PST", "PNAM", ropPos, "DEFD", olDura))
		olDura = "0";
	
	// get the maximum of minimal ground time and default allocation duration
	int ilDura = max(atoi(olDura), ipMinGT);

	if (ilDura > 0)
	{
		ropDura = CTimeSpan(0, 0, ilDura, 0);
	}
	else
	{
		// if no minimal ground time and no default allocation duration exists
		// get the hard coded default allocation 
		ropDura = ogPosDefAllocDur;
	}

	return true;
}


bool GetGatDefAllocDur(const CString &ropGate, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("GAT", "GNAM", ropGate, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogGatDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}


bool GetBltDefAllocDur(const CString &ropBlt, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("BLT", "BNAM", ropBlt, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogBltDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}



bool GetWroDefAllocDur(const CString &ropWro, CTimeSpan &ropDura)
{
	CString olDura;

	if (!ogBCD.GetField("WRO", "WNAM", ropWro, "DEFD", olDura))
		olDura = "0";

	if (atoi(olDura) <= 0)
	{
		ropDura = ogWroDefAllocDur;
	}
	else
	{
		ropDura = CTimeSpan(0, 0, atoi(olDura), 0);
	}
	return true;
}



bool GetFltnInDBFormat(const CString &ropUserFltn, CString &ropDBFltn)
{
	int ilFltn = atoi(ropUserFltn);
	ropDBFltn.Format("%03d", ilFltn);

	return true;
}



bool GetCurrentUtcTime(CTime &ropUtcTime)
{
	ropUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(ropUtcTime);
	return true;
}


// true/false; outside/inside the timelimit
bool IsPostFlight(const CTime& opTimeFromFlight)
{
	ASSERT(FALSE);
	return false;

/*
	if (opTimeFromFlight == TIMENULL)
		return false;

	// there's no timespan set (don't except negative values)
	CTimeSpan olSpanPast (0,0,0,0);
	CTimeSpan olSpanFuture (0,0,0,0);

	if (ogTimeSpanPostFlight.GetTotalSeconds() > 0)
		olSpanPast = ogTimeSpanPostFlight;

	if (ogTimeSpanPostFlightInFuture.GetTotalSeconds() > 0)
		olSpanFuture = ogTimeSpanPostFlightInFuture;

//	CString curTime = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
//	CString flightTime = opTimeFromFlight.Format("%Y%m%d%H%M%S");

	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

	CTime olBegin = olUtcTime - olSpanPast;
	CTime olEnd   = olUtcTime + olSpanFuture;

	if (olSpanPast.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight < olBegin)
			return true;
	}

	if (olSpanFuture.GetTotalSeconds() > 0)
	{
		if (opTimeFromFlight > olEnd)
			return true;
	}
	
	return false;
*/
}

BOOL ModifyWindowText(CWnd *opWnd, CString& opStrWnd, BOOL bplClean)
{
	if (opWnd)
	{
		CString olStrWndOld;
		opWnd->GetWindowText(olStrWndOld);

		if (olStrWndOld.Find(opStrWnd) < 0)
		{
			olStrWndOld += 	opStrWnd;
			opWnd->SetWindowText(olStrWndOld);
		}

		if (bplClean)
		{
			int i = olStrWndOld.Find(opStrWnd);
			if (i >= 0)
			{
				olStrWndOld.TrimRight(opStrWnd);
				opWnd->SetWindowText(olStrWndOld);
			}
		}

		return TRUE;
	}
	else
		return FALSE;

}



bool DBStrIsEmpty(const char *prpStr)
{
	if (prpStr == NULL)
		return false;

	if (prpStr[0] == '\0')
		return true;
	if (prpStr[0] == ' ' && prpStr[1] == '\0')
		return true;

	return false;
}
