// List.cpp : implementation file
//

#include <stdafx.h>
#include <opsscm.h>
#include <List.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// List dialog


List::List(CWnd* pParent /*=NULL*/, CStringArray *popItems)
	: CDialog(List::IDD, pParent)
{
	//{{AFX_DATA_INIT(List)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT


	for(int i = 0; i < popItems->GetSize(); i++)
	{
		omItems.Add((*popItems)[i]);
	}

}


void List::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(List)
	DDX_Control(pDX, IDC_LIST, m_CL_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(List, CDialog)
	//{{AFX_MSG_MAP(List)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()






/////////////////////////////////////////////////////////////////////////////
// List message handlers

BOOL List::OnInitDialog() 
{
	CDialog::OnInitDialog();
	

	
	for(int i = 0; i < omItems.GetSize(); i++)
	{
		m_CL_List.AddString(omItems[i]);
	}





	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
