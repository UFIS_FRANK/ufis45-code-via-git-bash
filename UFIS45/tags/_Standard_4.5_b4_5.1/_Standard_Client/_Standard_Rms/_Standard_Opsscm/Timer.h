#ifndef __TIMER_H__
#define __TIMER_H__

class Timer 
{
public:
	Timer(unsigned long lpLoopNum);

	void Start(void);
	bool Step(void);

	unsigned int GetMin(void) const;
	unsigned int GetSec(void) const;

	bool Valid(void) const;

private:
	unsigned long lmLoopNum;
	unsigned long lmLoopCount;

	unsigned int imMin;
	unsigned int imSec;

	CTime omStart;
	CTimeSpan omElapsedTime;
	CTimeSpan omElapsedTime2;

	bool bmValid;
};


#endif //__TIMER_H__



