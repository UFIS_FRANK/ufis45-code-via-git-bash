// seasontableviewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <CicDemandTableViewer.h>
#include <CcsGlobl.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <DataSet.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif




// Local function prototype
static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))


/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer
//

CicDemandTableViewer::CicDemandTableViewer()
{
    pomTable = NULL;
	bmInit = false;
}


void CicDemandTableViewer::UnRegister()
{
	ogDdx.UnRegister(this, NOTUSED);
 
}

CicDemandTableViewer::~CicDemandTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void CicDemandTableViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

void CicDemandTableViewer::ClearAll()
{
	bmInit = false;
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
    pomTable->ResetContent();
}



void CicDemandTableViewer::ChangeViewTo(const char *pcpViewName)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	
	ogDdx.UnRegister(this, NOTUSED);

	ogDdx.Register(this, DIACCA_CHANGE, CString("DIACCA_CHANGE"), CString("DIACCA_CHANGE"), FlightTableCf);
	ogDdx.Register(this, DIACCA_DELETE, CString("DIACCA_CHANGE"), CString("DIACCA_CHANGE"), FlightTableCf);
	ogDdx.Register(this, CCA_KKEY_CHANGE,	CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, DEMAND_KKEY_CHANGE,	CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), FlightTableCf);
	ogDdx.Register(this, DEMAND_CHANGE, CString("DEMAND_CHANGE"), CString("DEMAND_CHANGE"), FlightTableCf);


    pomTable->ResetContent();
    DeleteAll();    
	MakeLines();
   	UpdateDisplay();

	bmInit = true;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}







/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer -- code specific to this class




bool CicDemandTableViewer::IsPassFilter(DEMANDDATA *prpDemand)
{
	bool blRet = false;

	if(bgCcaDiaSeason && ogCcaDiaSingleDate != TIMENULL)
	{
		if(!IsOverlapped( ogCcaDiaSingleDate, ogCcaDiaSingleDate + CTimeSpan(0,24,0,0), prpDemand->Debe, prpDemand->Deen  ))
			return false;
	}

	if(prpDemand != NULL)
	{
		blRet = !ogCcaDiaFlightData.omCcaData.ExistCca(prpDemand->Urno); 
	}
//###################
/*
	if (prpDemand->Dety[0] != OUTBOUND_DEMAND)
		return false;

		CString olRety;
		char buffer[64];

	
		ltoa(prpDemand->Urud, buffer, 10);

		olRety = ogBCD.GetField("RUD", "URNO", CString(buffer),"RETY");

		if(olRety != "001")
		{
			return false;
		}

*/
//##################
	return blRet;
}



bool CicDemandTableViewer::IsPassFilter(CCSPtrArray<DEMANDDATA> &opDemands)
{
	if(opDemands.GetSize() == 0)
		return false;

	DEMANDDATA *prpDemand = &opDemands[0];

	bool blRet = false;
	if(prpDemand != NULL)
	{
		blRet = !ogCcaDiaFlightData.omCcaData.ExistCca(prpDemand->Urno); 
	}
	return blRet;
}




void CicDemandTableViewer::MakeLines()
{
	POSITION rlPos;
	CCSPtrArray<DEMANDDATA> olDemands;

	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
	{
		for(int j = ogCcaDiaFlightData.omDemandData.omKKeyArray.GetSize() -1 ; j >= 0; j --)
		{
			olDemands.RemoveAll();
			if(ogCcaDiaFlightData.omDemandData.GetDemandsByKKey(olDemands, ogCcaDiaFlightData.omDemandData.omKKeyArray[j]))
			{
				if(IsPassFilter(olDemands))
				{
					MakeLine(olDemands);
				}
			}
		}
	}
	else
	{
		for ( rlPos = ogCcaDiaFlightData.omDemandData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			DEMANDDATA *prlDemand;
			long llUrno;
			ogCcaDiaFlightData.omDemandData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDemand);
			if(prlDemand != NULL)
			{
				if(IsPassFilter(prlDemand))
				{
					MakeLine(prlDemand);
				}
			}
		}
	}
}


bool  CicDemandTableViewer::MakeLine(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
			CICDEMANDTABLE_LINEDATA rlLineData;
			MakeLineData(&rlLineData, prpDemand);
			int ilLineNo = CreateLine(rlLineData);
			InsertDisplayLine(ilLineNo);
	}
	return true;
}


bool  CicDemandTableViewer::MakeLine( CCSPtrArray<DEMANDDATA> &opDemands)
{
	CICDEMANDTABLE_LINEDATA rlLineData;
	MakeLineData(&rlLineData, opDemands);
	int ilLineNo = CreateLine(rlLineData);
	InsertDisplayLine(ilLineNo);
	return true;
}




	
void CicDemandTableViewer::MakeLineData(CICDEMANDTABLE_LINEDATA *prpLineData, CCSPtrArray<DEMANDDATA> &opDemands)
{
// 	char buffer[65];   

	if(opDemands.GetSize() > 0)
	{
		DEMANDDATA *prpDemand = &opDemands[0];

		prpLineData->Urno = prpDemand->Urno;
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(opDemands[0].Ouro);

/*		CCAFLIGHTDATA *prlFlight2;

		
		if(opDemands.GetSize() == 1)
			prlFlight2 = prlFlight;
		else
			prlFlight2 = ogCcaDiaFlightData.GetFlightByUrno(opDemands[opDemands.GetSize() - 1].Ouro);

*/		
		if(prlFlight != NULL)
		{
			CCAFLIGHTDATA *prlFlight2;
			
			if(opDemands.GetSize() == 1)
				prlFlight2 = prlFlight;
			else
				prlFlight2 = ogCcaDiaFlightData.GetFlightByUrno(opDemands[opDemands.GetSize() - 1].Ouro);

			prpLineData->FlightUrno = prlFlight->Urno;
			prpLineData->KKey = prpDemand->KKey;
			prpLineData->Flno = prlFlight->Flno;

			prpLineData->Sto  = prlFlight->Stod;
			prpLineData->Sto2  = prlFlight2->Stod;
			prpLineData->Freq  = prlFlight2->Freq;

			prpLineData->Act3 = prlFlight->Act5;
			prpLineData->OrgDes = CString(prlFlight->Des3);
			prpLineData->Nose = prlFlight->Nose;
			prpLineData->Ftyp = prlFlight->Ftyp;

			prpLineData->Ckbs = prpDemand->Debe;
			prpLineData->Ckes = prpDemand->Deen;
			//prpLineData->Ckic = prpDemand->Ckic;

			char buffer[64];
			ltoa(prpDemand->Urud, buffer, 10);

			CString olRueUrno = ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE");
			CString olRuleName = ogBCD.GetField("RUE", "URNO", olRueUrno,"RUNA");
			prpLineData->Remark = olRuleName;
			prpLineData->CounterGroup = ogCcaDiaFlightData.omDemandData.GetGroupNames(prpDemand->Urno);

		}
		else //common demand
		{
			DEMANDDATA *prlDemand2 = &opDemands[opDemands.GetSize() - 1];

			prpLineData->FlightUrno = 0;
			prpLineData->KKey = prpDemand->KKey; 
//			prpLineData->Flno = "COMMON";
//			prpLineData->Flno = ogCcaDiaFlightData.omDemandData.GetTextFromCciDemand(opDemands);
			CString olText = ogCcaDiaFlightData.omDemandData.GetTextFromCciDemand(opDemands);
			if (olText.IsEmpty())
				prpLineData->Flno = "COMMON";
			else
				prpLineData->Flno = olText;

			prpLineData->Sto  = prpDemand->Deen;
			prpLineData->Sto2 = prlDemand2->Deen;
			prpLineData->Freq = prpDemand->Freq;
			prpLineData->Ckbs = prpDemand->Debe;
			prpLineData->Ckes = prpDemand->Deen;

			char buffer[64];
			ltoa(prpDemand->Urud, buffer, 10);
			CString olRueUrno = ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE");
			CString olRuleName = ogBCD.GetField("RUE", "URNO", olRueUrno,"RUNA");

			prpLineData->Remark = olRuleName;
			prpLineData->CounterGroup = ogCcaDiaFlightData.omDemandData.GetGroupNames(prpDemand->Urno);
			prpLineData->Error = false;
		}

		if (bgGatPosLocal)
		{
			UtcToLocal(*prpLineData);
		}
	}
}

void CicDemandTableViewer::MakeLineData(CICDEMANDTABLE_LINEDATA *prpLineData, DEMANDDATA *prpDemand)
{
	char buffer[65];

	if(prpDemand != NULL)
	{
		prpLineData->Urno = prpDemand->Urno;
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prpDemand->Ouro);
		if(prlFlight != NULL)
		{

			prpLineData->FlightUrno = prlFlight->Urno;
			prpLineData->Flno = prlFlight->Flno;
			prpLineData->Sto  = prlFlight->Stod;
			prpLineData->Act3 = prlFlight->Act5;
			prpLineData->OrgDes = CString(prlFlight->Des3);
			prpLineData->Nose = prlFlight->Nose;
			prpLineData->Ftyp = prlFlight->Ftyp;

			prpLineData->Ckbs = prpDemand->Debe;
			prpLineData->Ckes = prpDemand->Deen;
			//prpLineData->Ckic = prpDemand->Ckic;

			ltoa(prpDemand->Urud, buffer, 10);
			CString olRueUrno = ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE");
			CString olRuleName = ogBCD.GetField("RUE", "URNO", olRueUrno,"RUNA");

			prpLineData->Remark = olRuleName;
			prpLineData->CounterGroup = ogCcaDiaFlightData.omDemandData.GetGroupNames(prpDemand->Urno);

			prpLineData->Error = false;

		}
		else//common demands
		{
			prpLineData->FlightUrno = 0;
//			prpLineData->Flno = "COMMON";
//			prpLineData->Flno = ogCcaDiaFlightData.omDemandData.GetTextFromCciDemand(prpDemand->Urno);
			CString olText = ogCcaDiaFlightData.omDemandData.GetTextFromCciDemand(prpDemand->Urno);
			if (olText.IsEmpty())
				prpLineData->Flno = "COMMON";
			else
				prpLineData->Flno = olText;


			prpLineData->Sto  = prpDemand->Deen;
			prpLineData->Ckbs = prpDemand->Debe;
			prpLineData->Ckes = prpDemand->Deen;

			ltoa(prpDemand->Urud, buffer, 10);
			CString olRueUrno = ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE");
			CString olRuleName = ogBCD.GetField("RUE", "URNO", olRueUrno,"RUNA");

			prpLineData->Remark = olRuleName;
			prpLineData->CounterGroup = ogCcaDiaFlightData.omDemandData.GetGroupNames(prpDemand->Urno);
			prpLineData->Error = false;
		}

		if (bgGatPosLocal)
		{
			UtcToLocal(*prpLineData);
		}
	}
}





int CicDemandTableViewer::CreateLine(CICDEMANDTABLE_LINEDATA &rpLine)
{
    int ilLineCount = omLines.GetSize();

    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        //if (CompareFlight(&rpLine, &omLines[ilLineno-1]) >= 0)
		if (CompareLines(&rpLine, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rpLine);
	return ilLineno;
}


void CicDemandTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);

	pomTable->DeleteTextLine(ipLineno);

}



bool CicDemandTableViewer::FindLine(long lpUrno, int &rilLineno)
{
	rilLineno = -1;
    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno) || (omLines[i].KKey == lpUrno))
	  {
		rilLineno = i;
		return true;
	  }
	}
	return false;
}






/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - CICDEMANDTABLE_LINEDATA array maintenance

void CicDemandTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}


/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - display drawing routine



// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void CicDemandTableViewer::UpdateDisplay()
{
	pomTable->ResetContent();

	DrawHeader();

	CICDEMANDTABLE_LINEDATA *prlLine;
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		int ilColumnNo = 0;
		prlLine = &omLines[ilLc];
		CCSPtrArray<TABLE_COLUMN> olColList;
	
		MakeColList(prlLine, olColList);

		pomTable->AddTextLine(olColList, (void*)(prlLine));
		olColList.DeleteAll();
	}


    pomTable->DisplayTable();
}



void CicDemandTableViewer::InsertDisplayLine( int ipLineNo)
{
	if(!((ipLineNo >= 0) && (ipLineNo < omLines.GetSize())))
		return;
	CCSPtrArray<TABLE_COLUMN> olColList;
	MakeColList(&omLines[ipLineNo], olColList);
	pomTable->InsertTextLine(ipLineNo, olColList, &omLines[ipLineNo]);
	olColList.DeleteAll();
    pomTable->DisplayTable();
}






void CicDemandTableViewer::DrawHeader()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
	pomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Font = &ogCourier_Bold_10;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 67; 
	rlHeader.Text =  GetString(IDS_STRING347);//CString("Flugnummer");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Length = 40; 
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING316);//CString("STD");
	omHeaderDataArray.New(rlHeader);


	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		rlHeader.Length = 200; 
	else
		rlHeader.Length = 65; 

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Text = GetString(IDS_STRING332);//CString("Datum");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING1447);//CString("ORG/DES");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 30; 
	rlHeader.Text = GetString(IDS_STRING311);//CString("A/C");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING1073);//CString("Von");
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(HD_DSR_AVTA_TIME);//CString("bis");
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 50; 
	rlHeader.Text = GetString(IDS_STRING1503) ;// CString("Conf")
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 40; 
	rlHeader.Text = GetString(IDS_STRING1504) ;//CString("Ftyp")
	omHeaderDataArray.New(rlHeader);


	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 300; 
	rlHeader.Text = GetString(IDS_STRING1506) ;//"Regel"
	omHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 300; 
	rlHeader.Text = GetString(IDS_STRING1507) ;//"Counter Grp"
	omHeaderDataArray.New(rlHeader);


	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	pomTable->SetDefaultSeparator();

}


void CicDemandTableViewer::MakeColList(CICDEMANDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList)
{
		TABLE_COLUMN rlColumnData;

		if(prlLine->Error == true)
		{
			rlColumnData.TextColor = COLORREF(RED);
		}
		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.Font = &ogCourier_Regular_9;
	
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Flno;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		rlColumnData.Text = prlLine->Sto.Format("%H:%M");
		rlColumnData.Alignment = COLALIGN_LEFT;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		{
			if(prlLine->Sto == prlLine->Sto2)
				rlColumnData.Text = prlLine->Sto.Format("%d.%m.%Y");
			else
			{
				char buffer[64];
				itoa(prlLine->Freq, buffer, 10);
				rlColumnData.Text = prlLine->Sto.Format("%d.%m.%Y - ") + prlLine->Sto2.Format("%d.%m.%Y  ") + CString(buffer);
			}
		}
		else
		{
			rlColumnData.Text = prlLine->Sto.Format("%d.%m.%y");
		}


		rlColumnData.Alignment = COLALIGN_LEFT;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->OrgDes;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckbs.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ckes.Format("%H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
//MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Nose;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Ftyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

//end MWO
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->Remark;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = prlLine->CounterGroup;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CicDemandTableViewer *polViewer = (CicDemandTableViewer *)popInstance;

	if(ipDDXType == DIACCA_DELETE)
		polViewer->ProcessCcaDelete((long *)vpDataPointer);
	if(ipDDXType == DIACCA_CHANGE)
		polViewer->ProcessCcaChange((long *)vpDataPointer);
	if(ipDDXType == DEMAND_KKEY_CHANGE)
		polViewer->ProcessKKeyChange((long *)vpDataPointer);
	if(ipDDXType == DEMAND_CHANGE)
		polViewer->ProcessDemandChange((long *)vpDataPointer);
}


void CicDemandTableViewer::ProcessCcaDelete(long *plpCcaGhpu)
{
	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		return;
	


	DeleteLine(*plpCcaGhpu);

	
	DEMANDDATA *prlDemand =ogCcaDiaFlightData.omDemandData.GetDemandByUrno(*plpCcaGhpu);

	if(prlDemand == NULL)
	{
		return;
	}

	if(IsPassFilter(prlDemand))
	{
		MakeLine(prlDemand);
	}
}




void CicDemandTableViewer::ProcessCcaChange(long *plpCcaUrno)
{
	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		return;
	


	DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(*plpCcaUrno);

	if(prlCca == NULL)
	{
		return;
	}
	
	
	DeleteLine(prlCca->Ghpu);

	
	DEMANDDATA *prlDemand =ogCcaDiaFlightData.omDemandData.GetDemandByUrno(prlCca->Ghpu);

	if(prlDemand == NULL)
	{
		return;
	}

	if(IsPassFilter(prlDemand))
	{
		MakeLine(prlDemand);
	}
}  

void CicDemandTableViewer::ProcessDemandChange(long *plpDemandUrno)
{
	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
		return;
	

	
	DeleteLine(*plpDemandUrno);

	
	DEMANDDATA *prlDemand =ogCcaDiaFlightData.omDemandData.GetDemandByUrno(*plpDemandUrno);

	if(prlDemand == NULL)
	{
		return;
	}

	if(IsPassFilter(prlDemand))
	{
		MakeLine(prlDemand);
	}
}

void CicDemandTableViewer::ProcessKKeyChange(long *prpDemandUrno)
{
	DeleteLine(*prpDemandUrno);

	CCSPtrArray<DEMANDDATA> olDemands;	
	
	if(ogCcaDiaFlightData.omDemandData.GetDemandsByKKey(olDemands, *prpDemandUrno))
	{

		if(IsPassFilter(olDemands))
		{
			MakeLine(olDemands);
		}
	}
	olDemands.RemoveAll();
}






int CicDemandTableViewer::CompareFlight(CICDEMANDTABLE_LINEDATA *prpLine1, CICDEMANDTABLE_LINEDATA *prpLine2)
{
		return (prpLine1->Sto == prpLine2->Sto)? 0:	(prpLine1->Sto > prpLine2->Sto)? 1: -1;
}

int CicDemandTableViewer::CompareLines(CICDEMANDTABLE_LINEDATA *prpLine1, CICDEMANDTABLE_LINEDATA *prpLine2)
{

	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
	{
		if(prpLine1->Sto.Format("%H:M") == prpLine2->Sto.Format("%H:M"))
		{
			return prpLine1->Flno > prpLine2->Flno ? 1: -1;

		}
		else
		{
			return prpLine1->Sto.Format("%H:M") > prpLine2->Sto.Format("%H:M") ? 1: -1;
		}
	}
	else
	{
		return (prpLine1->Sto == prpLine2->Sto)? 0:	
		       (prpLine1->Sto > prpLine2->Sto)? 1: -1;
	}
}





bool CicDemandTableViewer::DeleteLine(long lpUrno)
{
	if (lpUrno <= 0)
		return true;

    for (int i = omLines.GetSize() - 1; i >= 0; i--)
	{
	  if((omLines[i].Urno == lpUrno) || (omLines[i].KKey == lpUrno))
	  {
		omLines.DeleteAt(i);
		pomTable->DeleteTextLine(i);
	  }
	}
	return true;
}


void CicDemandTableViewer::UtcToLocal(CICDEMANDTABLE_LINEDATA &rrpLineData)
{
	ogBasicData.UtcToLocal(rrpLineData.Sto);
	ogBasicData.UtcToLocal(rrpLineData.Sto2);
	ogBasicData.UtcToLocal(rrpLineData.Ckbs);
	ogBasicData.UtcToLocal(rrpLineData.Ckes);
}

