#if !defined(AFX_DAILYCCAPRINTDLG_H__657FF803_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
#define AFX_DAILYCCAPRINTDLG_H__657FF803_4EDA_11D2_8369_0000C03B916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// DailyCcaPrintDlg.h : header file
//

#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CDailyCcaPrintDlg dialog

class CDailyCcaPrintDlg : public CDialog
{
// Construction
public:
	CDailyCcaPrintDlg(CWnd* pParent);   // constructor

// Dialog Data
	//{{AFX_DATA(CDailyCcaPrintDlg)
	enum { IDD = IDD_DAILYCCAPRINT };
	CStatic	m_CE_Vonzeit;
	CCSEdit	m_CE_Vomtag;
	CString	m_Vonzeit;
	CString	m_Vomtag;
	//}}AFX_DATA

	CString omMaxDateStr;
	CString omMinDateStr;
 	CTime omMaxDate;
	CTime omMinDate;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDailyCcaPrintDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDailyCcaPrintDlg)
	virtual void OnOK();
	afx_msg void OnKillfocusVomtag(UINT, UINT);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAILYCCAPRINTDLG_H__657FF803_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
