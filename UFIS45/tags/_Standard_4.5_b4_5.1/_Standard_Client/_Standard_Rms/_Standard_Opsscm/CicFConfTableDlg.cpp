// CicFConfTableDlg.cpp : implementation file
//

#include <stdafx.h>
#include <OPSSCM.h>
#include <CicFConfTableDlg.h>
#include <CcaCedaFlightData.h>
#include <SeasonDlg.h>
#include <DataSet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CicFConfTableDlg dialog


CicFConfTableDlg::CicFConfTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CicFConfTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicFConfTableDlg)
	//}}AFX_DATA_INIT
	isCreated = false;
	// Display all conflict-records as default
	pomCurrData = &ogCflData;
    CDialog::Create(CicFConfTableDlg::IDD, pParent);
	isCreated = true;
}


CicFConfTableDlg::~CicFConfTableDlg()
{
	delete pomTable;
}


void CicFConfTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicFConfTableDlg)
	DDX_Control(pDX, IDC_R_ALLCONF, m_R_AllConf);
	DDX_Control(pDX, IDC_R_FLIGHTCONF, m_R_FlightConf);
	DDX_Radio(pDX, IDC_R_ALLCONF, m_R_Conf_Value);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CicFConfTableDlg, CDialog)
	//{{AFX_MSG_MAP(CicFConfTableDlg)
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_B_CONFIRMALL, OnBConfirmAll)
	ON_BN_CLICKED(IDC_B_CONFIRM, OnBConfirm)
	ON_BN_CLICKED(IDC_R_ALLCONF, OnRAllConf)
	ON_BN_CLICKED(IDC_R_FLIGHTCONF, OnRFlightConf)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CicFConfTableDlg message handlers

BOOL CicFConfTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	omDragDropObject.RegisterTarget(this, this);

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

	pomTable->bmMultiDrag = true;

	CRect olRect;
    GetClientRect(&olRect);

	// Get the bottom edge coordinate of the confirm button
	WINDOWPLACEMENT* prlWinPlace = new WINDOWPLACEMENT;
	GetDlgItem(IDC_B_CONFIRM)->GetWindowPlacement(prlWinPlace);
	lmButtonSpace = prlWinPlace->rcNormalPosition.bottom + prlWinPlace->rcNormalPosition.top;
	delete prlWinPlace;
	
    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	// Position the table below the confirm button
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top+lmButtonSpace, olRect.bottom);

	omViewer.Attach(pomTable);
	// Activate the AllConflicts-Radiobutton
	CheckRadioButton(IDC_R_ALLCONF, IDC_R_FLIGHTCONF, IDC_R_ALLCONF);
	omViewer.ChangeViewTo(pomCurrData);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CicFConfTableDlg::Activate()
{
	if(!omViewer.bmInit)
	{
 		omViewer.ChangeViewTo(pomCurrData);
	}
}


void CicFConfTableDlg::Reset()
{
	omViewer.ClearAll();
}


void CicFConfTableDlg::OnClose() 
{
	ShowWindow(SW_HIDE);
}


void CicFConfTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{	
		CDialog::OnSize(nType, cx, cy);
		if (nType != SIZE_MINIMIZED)
		{
			pomTable->SetPosition(1, cx+1, lmButtonSpace, cy+1);
		}
	}
	
}


// Confirm all table entries
void CicFConfTableDlg::OnBConfirmAll() 
{
	// mark all table entries
	pomTable->GetCTableListBox()->SetSel(-1,true);
	// confirm marked entries
	OnBConfirm();

}

// confirm marked table entries
void CicFConfTableDlg::OnBConfirm() 
{
	CTime olCurrSysTime = CTime::GetCurrentTime();
	long llCflUrno;

	// Iterate over all items
	for (int ilLineNo=pomTable->GetLinesCount()-1; ilLineNo >= 0; ilLineNo--)
	{
		// is line selected?
		if (pomTable->GetCTableListBox()->GetSel(ilLineNo) != 0)
		{
			omViewer.GetUrno(ilLineNo, llCflUrno);
			// insert confirm data into cfl-record
			pomCurrData->ConfirmRecord(llCflUrno, olCurrSysTime.Format("%Y%m%d%H%M%S"), pcgUser);
			if (pomCurrData == &ogCflData)
			{
				// Update the cfl-record in 'conflicts of loaded flights'
				ogCcaDiaFlightData.omCflData.UpdateData(ogCflData, llCflUrno);
			}
			// delete cfl-record only in 'new conflicts'
			ogCflData.DeleteByUrno(llCflUrno);
			
			//if (pomCurrData == &ogCflData)
			//{
				// remove line in table
			//	omViewer.DeleteLine(ilLineNo);
			//}
		}
	}
	// display table
	omViewer.ChangeViewTo(pomCurrData);
}


// Display all conflict-records in the table
void CicFConfTableDlg::OnRAllConf() 
{
	TRACE("CicFConfTableDlg::OnRAllConf\n");
	pomCurrData = &ogCflData;
	omViewer.ChangeViewTo(pomCurrData);
}


// Display only conflict-records of the loaded flights in the table
void CicFConfTableDlg::OnRFlightConf() 
{
	TRACE("CicFConfTableDlg::OnRFlightConf\n");
	if (ogCcaDiaFlightData.omCflData.bmInvalid)
	{
		if (MessageBox(GetString(IDS_CONFIRM_CONF_LOAD), GetString(ST_FRAGE), MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			ogCcaDiaFlightData.ReadAllCflData();
		}
		else
		{
			//m_R_AllConf.SetCheck(1);
			//m_R_FlightConf.SetCheck(0);
			m_R_Conf_Value = 0;
			UpdateData(FALSE); // TODO: Geht nicht!
		}
	}
  	pomCurrData = &ogCcaDiaFlightData.omCflData;
	omViewer.ChangeViewTo(pomCurrData);
}


LONG CicFConfTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	CICFCONFTABLE_LINEDATA *prlTableLine = (CICFCONFTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		CFLDATA *prlCfl = pomCurrData->GetCflByUrno(prlTableLine->Urno);
		if (!prlCfl)
			return 0L;

		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlCfl->Rurn);;

		if(prlFlight != NULL)
		{
			if (pogSeasonDlg)
				pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
		}
		else
			if (pogSeasonDlg)
				pogSeasonDlg->NewData(this, 0, prlCfl->Rurn, DLG_CHANGE, bgGatPosLocal);
	}
	
	return 0L;
}

LONG CicFConfTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	return 0L;

}
