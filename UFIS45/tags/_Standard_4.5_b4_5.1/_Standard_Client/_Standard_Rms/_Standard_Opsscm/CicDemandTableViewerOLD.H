#ifndef __CicDemandTableViewer_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <DiaCedaFlightData.h>
#include <CViewer.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CICDEMANDTABLE_LINEDATA
{
	long Urno;
	CString		Flno;
	CTime		Sto;
	CString		Act3;
	CString		OrgDes;
	CTime		Ckbs;
	CTime		Ckes;
	CString     Ckic;
	CString		Nose;
	CString		Ftyp;
	CString		Remark;

	CICDEMANDTABLE_LINEDATA(void)
	{ 
		Ckbs = -1;
		Ckes = -1;
		Urno = 0;
		Sto = -1;
	}
};



/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of CicDemandTableViewer

//@Man:
//@Memo: CicDemandTableViewer
//@See:  STDAFX, CCSCedaData, CedaDIAFLIGHTDATA, CCSPTRARRAY,  CCSTable
/*@Doc:
  No comment on this up to now.
*/
class CicDemandTableViewer : public CViewer
{
// Constructions
public:
    //@ManMemo: Default constructor
    CicDemandTableViewer();
    //@ManMemo: Default destructor
    ~CicDemandTableViewer();

	void UnRegister();

	void ClearAll();

    //@ManMemo: Attach
	void Attach(CCSTable *popAttachWnd);
    //void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    //@ManMemo: ChangeViewTo
    void ChangeViewTo(const char *pcpViewName);
    //@ManMemo: omLines
    CCSPtrArray<CICDEMANDTABLE_LINEDATA> omLines;
// Internal data processing routines
private:
	bool IsPassFilter(DIAFLIGHTDATA *prpFlight);
	int  CompareFlight(CICDEMANDTABLE_LINEDATA *prpFlight1, CICDEMANDTABLE_LINEDATA *prpFlight2);

    void MakeLines(CCSPtrArray<DIAFLIGHTDATA> *popFlights, bool bpInsert = false);

	bool  MakeLine(DIAFLIGHTDATA *prpFlight, bool bpInsert);
	void MakeLineData(DIAFLIGHTDATA *prpFlight, CICDEMANDTABLE_LINEDATA &rpLine);
	void MakeColList(CICDEMANDTABLE_LINEDATA *prlLine, CCSPtrArray<TABLE_COLUMN> &olColList);
	int  CreateLine(CICDEMANDTABLE_LINEDATA &rpLine);

	bool FindLine(long lpUrno, int &rilLineno);
	
	void SelectLine(long ilCurrUrno);

	void InsertDisplayLine( int ipLineNo);

	bool DeleteFlight(long lpUrno);


// Operations
public:
    //@ManMemo: DeleteAll
	void DeleteAll();
    //@ManMemo: DeleteLine
	void DeleteLine(int ipLineno);
 
// Window refreshing routines
public:
	void UpdateDisplay();

	void DrawHeader();

	
	void ProcessFlightChange(DIAFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(DIAFLIGHTDATA *prpFlight);

	//void ProcessFlightInsert(DiaCedaFlightData::RKEYLIST  *prpRotation);

	

	bool bmInit;


private:
	CCSTable *pomTable;


// Methods which handle changes (from Data Distributor)
public:


};

#endif //__CicDemandTableViewer_H__
