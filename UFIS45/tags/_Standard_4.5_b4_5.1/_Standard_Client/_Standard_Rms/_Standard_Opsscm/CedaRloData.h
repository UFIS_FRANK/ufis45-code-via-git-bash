#ifndef _RLODATA_H_
#define _RLODATA_H_

#include <CCSCedaData.h>

void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, 
					  CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct RLODATA 
{
    long    Urno;       
	long    Urud;       
    char    Gtab[9];       
    char    Reft[9];       
    char    Rloc[11];       

	RLODATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

};



/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRloData: public CCSCedaData
{
// Attributes
public:
	CString omUruds;

    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUrudMap;
    CCSPtrArray<RLODATA> omData;


// Operations
public:
    CedaRloData();
    ~CedaRloData();

	void Register(void);

	bool ReadAll();
	bool Read(char *pspWhere /*NULL*/, bool bpClearAll, bool bpDdx);
	bool InsertInternal(RLODATA *prpDem, bool bpDdx = true);
	bool UpdateInternal(RLODATA *prpDem, bool bpDdx);
	bool DeleteInternal(RLODATA *prpDem, bool bpDdx  = true);

	bool ClearAll(bool bpWithRegistration = true);

	void AddToKeyMap(RLODATA *prpDem);
	void DelFromKeyMap(RLODATA *prpDem);

	bool  GetRlosByUrud(CCSPtrArray<RLODATA> &ropRloList, long lpUrud);

	RLODATA  *GetRloByUrno(long lpUrno);

	void  ProcessDemandBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	



};

#endif
