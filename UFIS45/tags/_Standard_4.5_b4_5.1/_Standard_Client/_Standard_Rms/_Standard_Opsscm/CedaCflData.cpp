// CedaCflData.cpp - Read and write flight conflict data 
//
//
// Written by:
// TVO, December 16, 1999
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssCm.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CedaCflData.h>
#include <CcaCedaFlightConfData.h>
#include <CedaBasicData.h>
 


void  ProcessCflCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaCflData::CedaCflData()
{
	// Create an array of CEDARECINFO for CFLDATA
   BEGIN_CEDARECINFO(CFLDATA, CflDataRecInfo)
        CCS_FIELD_LONG(Urno,"URNO", "",1) 
        CCS_FIELD_CHAR_TRIM(Hopo,"HOPO", "",1)
        CCS_FIELD_DATE(Time,"TIME", "",1)
        CCS_FIELD_CHAR_TRIM(Meno,"MENO", "",1)
        CCS_FIELD_CHAR_TRIM(Mety,"METY", "",1)
		CCS_FIELD_CHAR_TRIM(Prio,"PRIO", "",1)
		CCS_FIELD_CHAR_TRIM(Rtab,"RTAB", "",1)
		CCS_FIELD_LONG(Rurn,"RURN", "",1)
		CCS_FIELD_CHAR_TRIM(Flst,"FLST", "",1)
		CCS_FIELD_CHAR_TRIM(Nval,"NVAL", "",1)
		CCS_FIELD_CHAR_TRIM(Oval,"OVAL", "",1)
		CCS_FIELD_CHAR_TRIM(Akus,"AKUS", "",1)
		CCS_FIELD_CHAR_TRIM(Akti,"AKTI", "",1)
		CCS_FIELD_CHAR_TRIM(Akst,"AKST", "",1)
   END_CEDARECINFO

   // Copy the record structure
   for (int i = 0; i < sizeof(CflDataRecInfo)/sizeof(CflDataRecInfo[0]); i++)
	{
		CEDARECINFO *prlCedaRecInfo = new CEDARECINFO;
		memcpy(prlCedaRecInfo,&CflDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prlCedaRecInfo);
	}
	// Initialize table names and field names


	strcpy(pcmTableName,"CFL");
	//strcat(pcmTableName,pcgTableExt);
	pcmFieldList = "URNO,HOPO,TIME,MENO,METY,PRIO,RTAB,RURN,FLST,NVAL,OVAL,AKUS,AKTI,AKST";


	bmAFTRead = false;

	bmInvalid = true;
}


CedaCflData::~CedaCflData()
{
	TRACE("CedaCflData::~CedaCflData\n");
	ClearAll();
	//ogDdx.UnRegister((void *)this,BC_DEMAND_CHANGE);
}


void CedaCflData::Register(void)
{
	//ogDdx.Register((void *)this,BC_DEMAND_CHANGE,	CString("RLODATA"), CString("Cca-changed"),	ProcessRloCf);
	//ogDdx.Register((void *)this,BC_DEMAND_NEW,		CString("RLODATA"), CString("Cca-new"),		ProcessRloCf);
	//ogDdx.Register((void *)this,BC_DEMAND_DELETE,	CString("RLODATA"), CString("Cca-deleted"),	ProcessRloCf);
}




bool CedaCflData::ClearAll(bool bpWithRegistration)
{

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}

    omUrnoMap.RemoveAll();
    omData.DeleteAll();

    return true;
}



void CedaCflData::ReadFlightData(const CString &opFlightUrnos) {

	// Read AFT-Data
	CString olAFTWhere;
	olAFTWhere.Format("ORG3 = '%s' AND (FTYP='O' OR FTYP='S')", pcgHome);
	omFlightData.ReadFlights(olAFTWhere, opFlightUrnos, true);
	TRACE("CedaCflData::ReadFlightData(): %i Records\n", omFlightData.omData.GetSize());

}



// Read all conflict data of the given flight urnos
bool CedaCflData::ReadCflOfFlights(const CString &opFlightUrnos, bool bpClearAll, bool bpDdx) {
	CString olSelection;
	CStringArray olUrnosLists;
	bool blRc;
	bool blRet;

	if(bpClearAll)
	{
		ClearAll();
	}
	// Split urnolist in 100 pieces blocks
	for(int i = SplitItemList(opFlightUrnos, &olUrnosLists, 100) - 1; i >= 0; i--)
	{
		olSelection.Format("WHERE RURN IN (%s)", olUrnosLists[i]);
		// call CEDA
		blRet = CedaAction("RT", olSelection.GetBuffer(olSelection.GetLength()));
		//TRACE("CedaCflData::ReadCflOfFlights: CedaAction: %s\n", olSelection.GetBuffer(olSelection.GetLength()));
		blRc = blRet;
		// Read in internal buffer
		while (blRc == true)
		{
			CFLDATA *prlCfl = new CFLDATA;
			if ((blRc = GetFirstBufferRecord(prlCfl)) == true)
			{
				if(UpdateInternal(prlCfl, bpDdx))
				{
					delete prlCfl;
				}
				else
				{
					InsertInternal(prlCfl, bpDdx);
				}
			}
			else 
			{
				delete prlCfl;
			}
		}

	} 
	TRACE("CedaCflData::ReadCflOfFlights: Result: %ld conflicts read!\n", omData.GetSize());
	bmInvalid = false;

    return blRet;
}


bool CedaCflData::ReadAllCfl(bool bpClearAll, bool bpDdx)
{

	bool ilRc = true;

	if(bpClearAll)
	{
		ClearAll();
	}
	// Call CEDA
	ilRc = CedaAction("RT", "WHERE AKTI = ' '");
	if (ilRc != true)
	{
		if(!omLastErrorMessage.IsEmpty())
		{
			if(omLastErrorMessage.Find("CFL") != -1)
			{
				char pclMsg[2048]="";
				sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
				::MessageBox(NULL,pclMsg,"Datenbankfehler",MB_OK);
				return false;
			}
		}
	}

	CString olAFTUrnos;
	CString olTmp;
	CMapPtrToPtr olAFTUrnosSet;
	void *pvlDummy;
	// Load data from CedaData into the dynamic array of record
	while (ilRc == true) 
	{
		CFLDATA *prlCfl = new CFLDATA;
		if ((ilRc = GetFirstBufferRecord(prlCfl)) == true)
		{
			if(UpdateInternal(prlCfl, bpDdx))
			{
				delete prlCfl;
			}
			else
			{   
				// collect AFTUrnos only once
				if (olAFTUrnosSet.Lookup((void *)prlCfl->Rurn, pvlDummy) == 0)
				{
					olAFTUrnosSet.SetAt((void *)prlCfl->Rurn, pvlDummy);
					olTmp.Format("%ld", prlCfl->Rurn);
					olAFTUrnos += olTmp + ",";
				}

				InsertInternal(prlCfl, bpDdx);
			}
		}
		else
		{
			delete prlCfl;
		}
	}
	if(olAFTUrnos.GetLength() > 0)
	{
		olAFTUrnos = olAFTUrnos.Left(olAFTUrnos.GetLength()-1);
	}
	
	TRACE("CedaCflData::ReadAllCfl: %d records read!\n", omData.GetSize());
	//TRACE("olAFTUrnos = %s\n", olAFTUrnos);
	// read AFT-Data
	ReadFlightData(olAFTUrnos);

	// delete cfl-record with wrong flight origin
	for (int ilLc = omData.GetSize()-1; ilLc >= 0; ilLc--)
	{
		if (!TestFlightOrigin(&omData[ilLc]))
		{
			DelFromKeyMap(&omData[ilLc]);
			omData.DeleteAt(ilLc);//Update omData
		}
	}	

	bmInvalid = false;
    return true;

}


bool CedaCflData::TestFlightOrigin(const CFLDATA *prpCfl) const {
	CCAFLIGHTCONFDATA *prlFlight;
	// Only flightrecords with 'ORG3==pcgHome' has been loaded
	return (omFlightData.omUrnoMap.Lookup((void *)prpCfl->Rurn,(void *& )prlFlight)!=0);
}



bool CedaCflData::InsertInternal(CFLDATA *prpCfl, bool bpDdx)
{
	const CFLDATA *prlCfl = GetCflByUrno(prpCfl->Urno);
	if(prlCfl != NULL)
	{
		DeleteInternal(prlCfl);
	}
	
	omData.Add(prpCfl);//Update omData
	AddToKeyMap(prpCfl);
	//if(bpDdx)
		//ogDdx.DataChanged((void *)this, DEMAND_CHANGE,(void *)&prpCfl->Urno ); //Update Viewer
    return true;
}


bool CedaCflData::UpdateInternal(const CFLDATA *prpCfl, bool bpDdx)
{
	CFLDATA *prlCfl = GetCflByUrno(prpCfl->Urno);

	DelFromKeyMap(prlCfl);

	if (prlCfl != NULL)
	{
		*prlCfl = *prpCfl; //Update omData
		AddToKeyMap(prlCfl);

		/////if(bpDdx)
			//ogDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)&prlCfl->Urno); //Update Viewer

		return true;
	}
    return false;
}


bool CedaCflData::DeleteInternal(const CFLDATA *prpCfl, bool bpDdx )
{
	long llUrno = prpCfl->Urno;

	DelFromKeyMap(prpCfl);
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCfl->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
	//if(bpDdx)
		//ogDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)&llUrno); //Update Viewer
    return true;
}


bool CedaCflData::DeleteByUrno(long lpUrno)
{
	// get cfl-data struct
	CFLDATA *prlCflData;
	if (!(prlCflData = GetCflByUrno(lpUrno)))
	{
		return false;
	}
	return DeleteInternal(prlCflData);
}


void CedaCflData::AddToKeyMap(CFLDATA *prpCfl)
{
	if (prpCfl != NULL )
	{
		omUrnoMap.SetAt((void *)prpCfl->Urno,prpCfl);
	}
}



void CedaCflData::DelFromKeyMap(const CFLDATA *prpCfl)
{
	if (prpCfl != NULL )
	{
		omUrnoMap.RemoveKey((void *)prpCfl->Urno);
	}
}



CFLDATA  *CedaCflData::GetCflByUrno(long lpUrno) const
{
	CFLDATA  *prlCfl;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCfl) == TRUE)
	{
		return prlCfl;
	}
	return NULL;
}


void CedaCflData::CflDataToStr(const CFLDATA &prpData, CString &opData) const 
{
	opData.Empty();
	opData.Format("%i | %s | %s | %s | %s | %s | %s | %i | %s | %s | %s | %s | %s | %s", 
		prpData.Urno, prpData.Hopo, prpData.Time.Format("%d.%b.%Y %H:%M:%S"),
		ogBCD.GetField("LBL", "TKEY", prpData.Meno, "TEXT"),
		prpData.Mety, prpData.Prio, prpData.Rtab, prpData.Rurn,
		prpData.Flst, prpData.Nval, prpData.Oval, prpData.Akus,
		prpData.Akti, prpData.Akst); 
	
}



bool CedaCflData::ConfirmRecord(long lpUrno, const char *cpTime, const char *cpUser)
{
	TRACE("CedaCflData::ConfirmRecord(Urno=%ld, Time=%s, User=%s) START!\n", lpUrno, cpTime, cpUser);

	// get cfl-data struct
	CFLDATA *prlCflData;
	if (!(prlCflData = GetCflByUrno(lpUrno)))
	{
		return false;
	}

	// update values
	strcpy(prlCflData->Akus, cpUser);
	strcpy(prlCflData->Akti, cpTime);
	
	char pclSelection[200];
	sprintf(pclSelection, "WHERE URNO = %ld", lpUrno);
	// update record in database
	CString olListOfData;
	MakeCedaData(&omRecInfo, olListOfData, prlCflData);
	return CedaAction("URT",pclSelection,"",olListOfData.GetBuffer(olListOfData.GetLength()));
}


bool CedaCflData::UpdateData(const CedaCflData &opDataSource, long lpUrno)
{
	CFLDATA *prlCflData;

	prlCflData = opDataSource.GetCflByUrno(lpUrno);
	if (prlCflData)
	{
		return UpdateInternal(prlCflData, false);
	}
	return true;
}


void  ProcessCflCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaCflData *)popInstance)->ProcessDemandBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaCflData::ProcessDemandBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	struct BcStruct *prlBcData;			
	long llUrno;
	CString olSelection;
	prlBcData = (struct BcStruct *) vpDataPointer;
	CFLDATA *prlCfland;


	if(ipDDXType == BC_DEMAND_NEW)
	{
		prlCfland = new CFLDATA;
		GetRecordFromItemList(prlCfland,prlBcData->Fields,prlBcData->Data);
		if(prlCfland->Urno > 0)
		{
			InsertInternal(prlCfland);
			return;
		}
		delete prlCfland;
	}

	if((ipDDXType == BC_DEMAND_CHANGE) || (ipDDXType == BC_DEMAND_DELETE))
	{
		prlCfland = new CFLDATA;
		
		GetRecordFromItemList(prlCfland,prlBcData->Fields,prlBcData->Data);

		if(prlCfland->Urno == 0 )
		{
			olSelection = (CString)prlBcData->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcData->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			prlCfland->Urno = llUrno;
		}				
				
		if(prlCfland->Urno > 0 )
		{
			UpdateInternal(prlCfland, true);
		}
		delete prlCfland;
		return;

	}
	
		
}



