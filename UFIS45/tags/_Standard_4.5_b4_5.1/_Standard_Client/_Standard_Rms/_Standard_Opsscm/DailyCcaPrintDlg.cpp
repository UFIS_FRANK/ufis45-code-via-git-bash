// DailyCcaPrintDlg.cpp : implementation file
//

#include <stdafx.h>
#include <OPSSCM.h>
#include <DailyCcaPrintDlg.h>
//#include "ReportSelectDlg.h"
#include <CCSGLOBL.H>
#include <BasicData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// 
 
//const int imMontagsID = 1159 ;	// IDS_STRING1159 "Montag" ( s. auch DailyPrintDlg.h ! )


/////////////////////////////////////////////////////////////////////////////
// CDailyCcaPrintDlg dialog


CDailyCcaPrintDlg::CDailyCcaPrintDlg(CWnd* pParent)
	: CDialog(CDailyCcaPrintDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDailyCcaPrintDlg)
	m_Vonzeit = _T("");
	m_Vomtag = _T("");
	//}}AFX_DATA_INIT

}


void CDailyCcaPrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDailyCcaPrintDlg)
	DDX_Control(pDX, IDC_VONZEIT, m_CE_Vonzeit);
	DDX_Control(pDX, IDC_VOMTAG, m_CE_Vomtag);
	DDX_Text(pDX, IDC_VONZEIT, m_Vonzeit);
	DDX_Text(pDX, IDC_VOMTAG, m_Vomtag);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDailyCcaPrintDlg, CDialog)
	//{{AFX_MSG_MAP(CDailyCcaPrintDlg)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocusVomtag)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDailyCcaPrintDlg message handlers

void CDailyCcaPrintDlg::OnOK() 
{
	
	CString olErrorText;
 
	bool blNoEx = false ;
	CTime   olMinDate;
	CTime   olMaxDate;
	CString olHStr1( "00:00" ) ;
	CString olHStr2( "23:59" ) ;
	CString olDay;

	//// Check user input
	if(!m_CE_Vomtag.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE),GetString(ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}
	

	UpdateData();

	m_CE_Vomtag.GetWindowText(olDay);

	if(olDay.IsEmpty())
	{
		// Default: heute
		CTime olTime = CTime::GetCurrentTime();
		olDay = olTime.Format( "%d.%m.%Y" );
	}
 
 
	olMinDate = DateHourStringToDate( olDay, olHStr1 );
	olMaxDate = DateHourStringToDate( olDay, olHStr2 );


	// CeDa Zugriff in UTC Zeit
	if( bgReportLocal ) 
	{
		ogBasicData.LocalToUtc(olMinDate);
		ogBasicData.LocalToUtc(olMaxDate);
	}

	omMaxDate = olMaxDate;
	omMinDate = olMinDate;
 
 
	if(!olErrorText.IsEmpty())	
	{
		if( MessageBox( olErrorText,GetString( IDS_WARNING), MB_YESNO ) == IDNO )
		{ 
			return;
		}
	}
	
	CDialog::OnOK();
}



void CDailyCcaPrintDlg::OnKillfocusVomtag( UINT wParam, UINT lParam ) 
{
	
	// Infofenster VonZeit mit Auswahltag versorgen
    CString olLokdum ;

	UpdateData();

	if(!m_CE_Vomtag.GetStatus())
	{
 		MessageBox(GetString(IDS_NO_DATE) ,GetString( ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}

	// Wochentag bestimmen
	CTime olLocalTime = DateStringToDate( m_Vomtag ) ;
	// wenn Datum kleiner 01.01.1970 oder groesser .2038 --> Absturz, MBR 21.05.1999
	if (olLocalTime == TIMENULL)
	{
		CTime olTime = CTime::GetCurrentTime();
		m_Vomtag = olTime.Format( "%d.%m.%Y" );
		m_CE_Vomtag.SetWindowText(m_Vomtag);
		//pomSelectDlg->omLastValues.SetMaxDate( m_Vomtag ) ;
		olLocalTime = DateStringToDate( m_Vomtag ) ;
		MessageBox(GetString(IDS_DATE_RANGE), GetString(IMFK_REPORT), MB_OK);
	}

	int ilWDay = olLocalTime.GetDayOfWeek() ;
	int ilWDay2;
 	if( ilWDay > 1 )
	{
 		ilWDay2 = ilWDay-1 ;
	}
	else
	{
		// Sonntag
 		ilWDay2 = 7; ;
	}
	m_Vonzeit = olLocalTime.Format("%A")+", "+olLocalTime.Format( "%d.%m.%y" ) ;
	olLokdum.Format(",  %s %d", GetString( IDS_DAY_OF_TRAFFIC ), ilWDay2 ) ;
	m_Vonzeit += olLokdum ;
	
	m_CE_Vonzeit.SetWindowText( m_Vonzeit ) ;
	m_CE_Vonzeit.UpdateWindow() ;
	
}


BOOL CDailyCcaPrintDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_CE_Vomtag.SetTypeToDate();
	m_CE_Vomtag.SetBKColor( YELLOW );
	
 	CTime olLocalTime = CTime::GetCurrentTime() ;
	m_Vomtag = olLocalTime.Format( "%d.%m.%Y" )  ;
 
	m_CE_Vomtag.SetWindowText( m_Vomtag ) ;
	m_CE_Vomtag.UpdateWindow() ;

	// Hinweisfenster Vonzeit versorgen
	OnKillfocusVomtag( 0, 0 ) ;	
 
	UpdateData(FALSE) ;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
