// RulesList.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <RulesList.h>
#include <CedaBasicData.h>
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSTime.h>
#include <MainFrm.h>
#include <RulesFormView.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define RULESLIST_COL_COUNT 15

int igRuleSortingCol = -1;
bool bgRuleSortAscend = true;
ROWCOL igRuleTopRow = 1;


//-----------------------------------------------------------------------------------------------------------------------
//					statics, globals, defines, etc
//-----------------------------------------------------------------------------------------------------------------------
int ValComp(const RecordSet **ppDateStr1, const RecordSet **ppDateStr2)
{
	int ilCompareResult = (((**ppDateStr1).Values[igValVafrIdx]) == ((**ppDateStr2).Values[igValVafrIdx]))? 0:
                (((**ppDateStr1).Values[igValVafrIdx])  > ((**ppDateStr2).Values[igValVafrIdx]) )? 1: -1;

	return ilCompareResult;
}



//-----------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//-----------------------------------------------------------------------------------------------------------------------
RulesList::RulesList(bool bpIsShowIndep, CWnd* pParent /*=NULL*/)
	: CDialog(RulesList::IDD, pParent)
{
	//pomGrid = new CGridFenster(this);
	pomGrid = 0;	
	bmIsShowIndep = bpIsShowIndep;
	imWarnBefore = GetPrivateProfileInt(ogAppName, "EXPIRATION_WARNING", 0, pcgConfigPath);
	lmSelectedTplUrno = -1;

	//pomGrid->SetSortingEnabled(false); // sorting of grid is done by RulesList
	pomActView = 0;
	imLastWidth = 0;
	imLastHeight = 0;
}
//-----------------------------------------------------------------------------------------------------------------------

RulesList::~RulesList()
{
	delete pomGrid;
	pomGrid = NULL;
}



//-----------------------------------------------------------------------------------------------------------------------
//					data exchange, message map
//-----------------------------------------------------------------------------------------------------------------------
void RulesList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RulesList)
	DDX_Control(pDX, RULIST_CHB_INDEP, m_Chb_Indep);
	DDX_Control(pDX, IDC_COMBO_BOX, omComboBox);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDOK, pomDlgControls[0]);
	DDX_Control(pDX, IDCANCEL, pomDlgControls[1]);
	DDX_Control(pDX, ID_FILE_PRINT, pomDlgControls[2]);
	DDX_Control(pDX, IDC_TPL_TXT, pomDlgControls[3]);
	DDX_Control(pDX, ID_FILE_PRINT_PREVIEW, pomDlgControls[4]);
}
//-----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(RulesList, CDialog)
	//{{AFX_MSG_MAP(RulesList)
		ON_CBN_SELCHANGE(IDC_COMBO_BOX, OnSelchangeCOMBObox)
		ON_MESSAGE(GRID_MESSAGE_CELLCLICK, OnGridClick)
		ON_MESSAGE(GRID_MESSAGE_DOUBLECLICK, OnGridDoubleClick)
		ON_BN_CLICKED(RULIST_CHB_INDEP, OnIndepCheckbox)
	ON_WM_SIZE()
	ON_BN_CLICKED(ID_FILE_PRINT, OnFilePrint)
	ON_BN_CLICKED(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//{{AFX_DATA_INIT(RulesList)
		// NOTE: the ClassWizard will add member initialization here
//}}AFX_DATA_INIT



//-----------------------------------------------------------------------------------------------------------------------
//					message handlers
//-----------------------------------------------------------------------------------------------------------------------
BOOL RulesList::OnInitDialog() 
{
	CCS_TRY
	
	CDialog::OnInitDialog();
	SetStaticTexts();


	if (bmIsShowIndep == true)
	{
		m_Chb_Indep.SetCheck(CHECKED);
	}
	else
	{
		m_Chb_Indep.SetCheck(UNCHECKED);
	}
	
	
	//--- Grid initialisieren
	pomGrid = new CGridFenster(this);
	pomGrid->SetSortingEnabled(false); // sorting of grid is done by RulesList

	BOOL blErr = pomGrid->SubclassDlgItem(IDC_RULES_LIST, this);
	pomGrid->Initialize();

	pomGrid->SetColCount(RULESLIST_COL_COUNT);
	pomGrid->SetRowCount(0);

	pomGrid->SetColWidth(0, 0, 20);		// row header
	pomGrid->SetColWidth(1, 1, 30);		// priority index
	pomGrid->SetColWidth(2, 2, 22);		// excluded
	pomGrid->SetColWidth(3, 3, 100);	// rule short name
	pomGrid->SetColWidth(4, 4, 250);	// rule name
	pomGrid->SetColWidth(5, 5, 22);		// rule type (S/C/I)
	pomGrid->SetColWidth(6, 6, 90);		// valid from
	pomGrid->SetColWidth(7, 7, 90);		// valid to
	pomGrid->SetColWidth(8, 8, 20);		// flag: more than one VAL record
	pomGrid->SetColWidth(9, 9, 100);	// template name
	pomGrid->SetColWidth(10, 10, 22);		// status (active / deactivated)
	pomGrid->SetColWidth(11, 11, 70);	// rule urno
	pomGrid->SetColWidth(12, 12, 70);	// template urno
	pomGrid->SetColWidth(13, 13, 300);	// rule priority string
	pomGrid->SetColWidth(14, 14, 15);	// event type (0 = turnaround, 1 = inbound, 2 = outbound)
	pomGrid->SetColWidth(15, 15, 90);	// last update

	IniColWidths ();

	pomGrid->SetValueRange(CGXRange(0,1), GetString(1453));	// priority index
	pomGrid->SetValueRange(CGXRange(0,2), GetString(1492)); // excluded
	pomGrid->SetValueRange(CGXRange(0,3), GetString(KURZNAME)); // short name
	pomGrid->SetValueRange(CGXRange(0,4), GetString(217)); // name
	pomGrid->SetValueRange(CGXRange(0,5), GetString(1496)); // rule type (S/C/I)
	pomGrid->SetValueRange(CGXRange(0,6), GetString(VALFROM)); // vafr
	pomGrid->SetValueRange(CGXRange(0,7), GetString(VALTO));//vato
	pomGrid->SetValueRange(CGXRange(0,8), GetString(2000));//flag: more than one val record
	pomGrid->SetValueRange(CGXRange(0,9), GetString(1452)); // template name
	pomGrid->SetValueRange(CGXRange(0,10), "Status"); // status (active/inactive - archived rules aren't shown!)	
	pomGrid->SetValueRange(CGXRange(0,11), "RUE.URNO "); // rule urno
	pomGrid->SetValueRange(CGXRange(0,12), "TPL.URNO"); // template urno
	pomGrid->SetValueRange(CGXRange(0,13), "PRIO"); // 
	pomGrid->SetValueRange(CGXRange(0,14), "EVTY"); // 
	pomGrid->SetValueRange(CGXRange(0,15), GetString(IDS_LAST_CHANGE)); // 

	// Hide columns for urno and template urno 
	if (bgDebug == false)
	{
		pomGrid->HideCols(10, 14);
	}

	// disable numbering and tracking for rows
	pomGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomGrid->GetParam()->EnableTrackRowHeight(0); 
	// pomGrid->GetParam()->EnableTrackColWidth(0);

	// set background to white
	pomGrid->GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, ogColors[WHITE_IDX]);

	//---- Currently in FormView->ComboBox chosen Template 
	int  ilRecordCount = ogBCD.GetDataCount("TPL");
	int ilPos,  ilCurSel ;
	CString	olStringToSelect="*.*";
	CString olUrnoToSelect;

	if (lmSelectedTplUrno >= 0)
	{
		olUrnoToSelect.Format("%ld", lmSelectedTplUrno);
		olStringToSelect = ogBCD.GetField("TPL", "URNO", olUrnoToSelect, "TNAM");
	}
	
	for(int i = 0; i < ilRecordCount; i++)
	{
 		CString olName = ogBCD.GetField("TPL", i, "TNAM");
		if(ogPrivList.GetStat(olName) == '-')
			continue;
	
		CString olTpst =  ogBCD.GetField("TPL", i,"TPST");
		if ((olTpst!='2') && (omComboBox.FindStringExact(-1, olName) == CB_ERR))
		{
			ilPos = omComboBox.InsertString(0, olName);
		}
	}

	//--- make possibility to choose all 
    ilPos  = omComboBox.InsertString(0, "*.*");


	//--- set combobox to selection made in main window
	ilCurSel = omComboBox.FindStringExact(-1, olStringToSelect);
	ilCurSel = max(ilCurSel,0);
	omComboBox.SetCurSel(ilCurSel);
	
	//--- fill grid
	omComboBox.GetLBText (ilCurSel, olStringToSelect);

	FillGrid(olStringToSelect);
	

	if ( igRuleTopRow <= pomGrid->GetRowCount() )
		pomGrid->SetTopRow ( igRuleTopRow );
	
	// select rule that is shown in main window
	/*	now in SetSorting
	for (int ilRow = 1; ilRow < pomGrid->GetRowCount(); ilRow++)
	{
		if (pomGrid->GetValueRowCol(ilRow, 11) == omRueUrno)
		{
			pomGrid->SelectRange(CGXRange(ilRow, 0, ilRow, pomGrid->GetColCount()), TRUE);
			break;
		}
	}
	*/
	pomActView = 0;
	CMainFrame *polMainWnd = (CMainFrame*) AfxGetMainWnd();
	if (polMainWnd)
	{
		CView *pActiveView = polMainWnd->GetActiveView();

		if (pActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView)))
		{
			pomActView = (CRulesFormView*) polMainWnd->GetActiveView();
		}
	}
	IniWindowPosition ( this, "RulesList" );

	CCS_CATCH_ALL

	return TRUE;  
}
//----------------------------------------------------------------------------------------------------------------------


void RulesList::OnSelchangeCOMBObox()
{
	
//----- Alte Eintr�ge l�schen aus Listbox
	int ilRowCount = pomGrid->GetRowCount();
	if (ilRowCount > 0)
	{
		pomGrid->RemoveRows(1, ilRowCount);
	}
	
//----- get template urno
	int ilSel;
	CString olSelection = "*.*";
	ilSel = omComboBox.GetCurSel();
	if (ilSel >= 0)
	{
		omComboBox.GetLBText( ilSel, olSelection );
	}

	igRuleTopRow = 1;
	pomGrid->SetTopRow ( igRuleTopRow, GX_SMART, FALSE  );

//----- fill grid
	FillGrid(olSelection);
}
//----------------------------------------------------------------------------------------------------------------------

void RulesList::OnOK() 
{
	CCS_TRY

	CRowColArray  rlArray;
	
	SaveWindowPosition ( this, "RulesList" );
	if ( pomGrid )
	{
		SaveColWidths ();
		igRuleTopRow = pomGrid->m_nTopRow;
	}
    if (pomGrid->GetSelectedRows(rlArray) > 0)
	{
		// open first selected rule in rule editor
		int ilRow = rlArray[0];
		omDataStringRuleUrno = pomGrid->GetValueRowCol(ilRow, 11);

		if(atol(omDataStringRuleUrno) > 0)
		{
			//--- Daten aus RUE holen
			ogBCD.GetField("RUE", "URNO", omDataStringRuleUrno, "EVTT",  omDataStringRotation); 
			ogBCD.GetField("RUE", "URNO", omDataStringRuleUrno, "EVTA",  omDataStringArrival); 	
			ogBCD.GetField("RUE", "URNO", omDataStringRuleUrno, "EVTD",  omDataStringDeparture); 

			omDataStringRuleName = pomGrid->GetValueRowCol(ilRow, 4);
			omDataStringRuleShortName = pomGrid->GetValueRowCol(ilRow, 3);
			omTemplateName = pomGrid->GetValueRowCol(ilRow, 9);

			CDialog::OnOK();	
		}
	}
	else
	{
		MessageBox(GetString(1475), GetString(1437), MB_ICONEXCLAMATION);
	}
	

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void RulesList::OnGridClick(WPARAM wParam, LPARAM lParam) 
{
	CCS_TRY

	ROWCOL ilRow = ((CELLPOS*)lParam)->Row;
	
	if (ilRow == 0)
	{
		// save sorting criterium
		if ( igRuleSortingCol != ((CELLPOS*)lParam)->Col )
		{
			bgRuleSortAscend = true;
			igRuleSortingCol = ((CELLPOS*)lParam)->Col;
		}
		else
			bgRuleSortAscend = !bgRuleSortAscend ;
		SetSorting();
	}
	else
		if(atol(pomGrid->GetValueRowCol(ilRow, 11)) > 0)
		{
			omDataStringRuleUrno = pomGrid->GetValueRowCol(ilRow, 11);
			pomGrid->SelectRange(CGXRange(ilRow, 0, ilRow, pomGrid->GetColCount()), TRUE);
		}
		
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void RulesList::OnGridDoubleClick(WPARAM wParam, LPARAM lParam) 
{
	CCS_TRY
	
	ROWCOL ilRow = ((CELLPOS*)lParam)->Row;

	// added by mne, 001023
	/*
	CMainFrame *polFrameWnd = static_cast <CMainFrame*>(AfxGetMainWnd()); 
	if (polFrameWnd != NULL)
			polFrameWnd->SaveGridColWidths(pomGrid);
	// end mne


	omDataStringRuleUrno = pomGrid->GetValueRowCol(ilRow, 11);
	       
	if(atol(omDataStringRuleUrno) > 0)
	{
		//--- Daten aus RUE holen
		  ogBCD.GetField("RUE", "URNO", omDataStringRuleUrno, "EVTT",  omDataStringRotation); 
		  ogBCD.GetField("RUE", "URNO", omDataStringRuleUrno, "EVTA",  omDataStringArrival); 	
		  ogBCD.GetField("RUE", "URNO", omDataStringRuleUrno, "EVTD",  omDataStringDeparture); 

		  omDataStringRuleName = pomGrid->GetValueRowCol(ilRow, 4);
		  omDataStringRuleShortName = pomGrid->GetValueRowCol(ilRow, 3);
 		  omTemplateName = pomGrid->GetValueRowCol(ilRow, 9);

		  EndDialog(IDOK);
	}
	*/
	if ( ilRow >= 1 )
		OnOK ();

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void RulesList::OnCancel() 
{
	SaveWindowPosition ( this, "RulesList" );
	if ( pomGrid )
	{
		SaveColWidths ();
		igRuleTopRow = pomGrid->m_nTopRow;
	}
	CDialog::OnCancel();
}
//----------------------------------------------------------------------------------------------------------------------

void RulesList::OnIndepCheckbox() 
{
	if (bmIsShowIndep == true)
	{
		bmIsShowIndep = false;
		m_Chb_Indep.SetCheck(UNCHECKED);
	}
	else
	{
		bmIsShowIndep = true;
		m_Chb_Indep.SetCheck(CHECKED);
	}

	// refill grid
	//long llUrno = (long)omComboBox.GetItemData(omComboBox.GetCurSel());
	int ilSel;
	CString olSelection = "*.*";
	ilSel = omComboBox.GetCurSel();
	if ( ilSel >= 0 )
		omComboBox.GetLBText( ilSel, olSelection );
	FillGrid(olSelection);
}



//-----------------------------------------------------------------------------------------------------------------------
//					helper functions
//-----------------------------------------------------------------------------------------------------------------------
void RulesList::FillGrid(CString opTplName)
{
	CCS_TRY

	CGXSortInfoArray rlSortInfo;
	RecordSet  olRueRecord;
	int	i, j;
			
	pomGrid->SetReadOnly(FALSE);
	pomGrid->LockUpdate(TRUE);
	
	// empty grid	
	int ilRowCount = pomGrid->GetRowCount();
	if (ilRowCount > 0)
	{
		pomGrid->RemoveRows(1, ilRowCount);
	}

	//--- Spezial Fall - alle einlesen 
	if(opTplName == CString("*.*"))
	{ 
		// refill grid 
		int ilCount = ogBCD.GetDataCount("RUE");
		if (ilCount > 0)
		{
			for ( i = 0; i < ilCount; i++)
			{	
				ogBCD.GetRecord("RUE", i, olRueRecord);
				DisplayOneRule(&olRueRecord);
			}
		}
	}
	//--- Sonst filtern
	else 
	{
		CCSPtrArray <RecordSet> olRecords;
		CCSPtrArray <RecordSet> olTplUrnoList;
		CString olTplUrno;

		ogBCD.GetRecords("TPL", "TNAM", opTplName, &olTplUrnoList );
		
		int ilCheck = 0;
		for ( i = 0; i < olTplUrnoList.GetSize(); i++)
		{
			olTplUrno = olTplUrnoList[i].Values[igTplUrnoIdx];
			ogBCD.GetRecords("RUE", "UTPL", olTplUrno, &olRecords);
			int ilCount = olRecords.GetSize();
			for (j = 0; j < ilCount; j++)
			{
				DisplayOneRule(&(olRecords[j]));
				ilCheck++;
			}
			olRecords.DeleteAll();
		}
		olTplUrnoList.DeleteAll();
		
		// sort grid rows, descending  by priority
		if (ilCheck > 0)
		{
			SetSorting(true);	// ignore User's sorting for correct Prio
		}

		// fill index column
		if (bmIsShowIndep == false)
		{
			int j = 0;
			CString olIndex;
			CString olLastText, olText;

			for (i = 1; i <= (int)pomGrid->GetRowCount(); i++)
			{
				/*if (pomGrid->GetValueRowCol(i + 1, 3).IsEmpty() == FALSE)
				{
					CString olIndex;
					olIndex.Format("%d", i + 1);
					pomGrid->SetValueRange(CGXRange(i + 1, 1), olIndex);
				}*/
				
				if (pomGrid->GetValueRowCol(i, 3).IsEmpty() == FALSE)
				{
					olLastText = pomGrid->GetValueRowCol(i - 1, 13);
					olText = pomGrid->GetValueRowCol(i, 13);

					if (olLastText == olText)
					{
						//olIndex = CString("");	
						olIndex.Format("%d", j);
					}
					else
					{
						j++;
						olIndex.Format("%d", j);
					}

					pomGrid->SetValueRange(CGXRange(i, 1), olIndex);
				}
			}
		}
	}
	
	CMainFrame *polFrameWnd = static_cast <CMainFrame*>(AfxGetMainWnd()); 
	/*	
	if (polFrameWnd != NULL)
	{
		if (polFrameWnd->bmIsGridColWidthsSaved)
			polFrameWnd->RestoreGridColWidths(pomGrid);
	}
	// end mne
	*/
	SetSorting();
	pomGrid->LockUpdate(FALSE);
	pomGrid->Redraw();
	pomGrid->SetReadOnly(TRUE);

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

// mne 001023
// set the sorting and col widths the user had chosen previously
void RulesList::SetSorting( bool bpIgnoreActSorting /*=false*/ )
{
	CGXSortInfoArray rlSortInfo;
	int ilStart=0;

	if (!bpIgnoreActSorting && (igRuleSortingCol > -1) )
	{
		rlSortInfo.SetSize(6);       // 1 additional key
		rlSortInfo[0].nRC = igRuleSortingCol;		 // first column to sort is the criterium last used 
		rlSortInfo[0].bCase = FALSE; // not case sensitive
		rlSortInfo[0].sortType = CGXSortInfo::autodetect;   // the grid will determine if the key is a date, numeric or other value 
		if ( bgRuleSortAscend )
			rlSortInfo[0].sortOrder = CGXSortInfo::ascending;
		else
			rlSortInfo[0].sortOrder = CGXSortInfo::descending;
		ilStart = 1;
	}
	else
		rlSortInfo.SetSize(5);       //  if no user sorting active
			
	rlSortInfo[ilStart+0].nRC = 9;		 // ordered by template name first
	rlSortInfo[ilStart+0].bCase = FALSE; // not case sensitive
	rlSortInfo[ilStart+0].sortType = CGXSortInfo::autodetect;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+0].sortOrder = CGXSortInfo::ascending;

	rlSortInfo[ilStart+1].nRC = 2;		 // column EXCL (2) is the first key
	rlSortInfo[ilStart+1].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+1].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+1].sortOrder = CGXSortInfo::descending;
			
	rlSortInfo[ilStart+2].nRC = 14;		 // column EVTY (14) is the second key
	rlSortInfo[ilStart+2].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+2].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+2].sortOrder = CGXSortInfo::ascending;
	
	rlSortInfo[ilStart+3].nRC = 13;		 // column Prio (13) is the third key
	rlSortInfo[ilStart+3].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+3].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+3].sortOrder = CGXSortInfo::descending;    

	rlSortInfo[ilStart+4].nRC = 3;		 // column Shoertname (3) is the last key
	rlSortInfo[ilStart+4].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+4].sortType = CGXSortInfo::alphanumeric;   
	rlSortInfo[ilStart+4].sortOrder = CGXSortInfo::ascending;    

	pomGrid->SortRows(CGXRange().SetTable(), rlSortInfo);
	
	// select rule omDataStringRuleUrno
	for (int ilRow = 1; ilRow <= pomGrid->GetRowCount(); ilRow++)
	{
		if (pomGrid->GetValueRowCol(ilRow, 11) == omDataStringRuleUrno)
		{
			pomGrid->SelectRange(CGXRange(ilRow, 0, ilRow, pomGrid->GetColCount()), TRUE);
			break;
		}
	}

}
// end mne
//---------------------------------------------------------------------------------------------------------------------

bool RulesList::GetExpirationColor ( CString &ropVato, COLORREF &ipExpirationColor ) 
{
	CTimeSpan olTimeSpan0(0,0,0,0);
	CTimeSpan olTimeTillExpiraton;
	bool	  blRet=false;

	olTimeTillExpiraton = GetVatoTimeTillExpiration ( ropVato );

	if ( olTimeTillExpiraton <= olTimeSpan0 )	//  Regel ist abgelaufen
	{
		ipExpirationColor = ogColors[RED_IDX];
		blRet = true;
	}
	else
		if ( imWarnBefore )		//  Zeitraum f�r Warnung vor Ablauf gesetzt ?
		{
			CTimeSpan olWarnBevor(imWarnBefore, 0, 0, 0 );
			if ( olTimeTillExpiraton <= olWarnBevor )
			{
				ipExpirationColor = ogColors[ORANGE_IDX] ; 
				blRet = true;
			}
		}
	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

void RulesList::DisplayOneRule(RecordSet *popRule)
{
	CGXStyle olStyle;
	CString olRuty;
	CString olRueShortName;
	CString olRueName;
	CString olRuePrio;
	CString olRueUrno;
	CString olExcl;
	CString olRust;
	CString olEvty;
	CString olTplUrno;
	CString olTplName;
	CString olRueVato, olRueDBVato, olRueLstu;
	CString olRueVafr;
	CString olValCount;
	COleDateTime Vafr, Vato, olLstu;
	COLORREF  ilExpirationColor, ilBkColor;
		
	if (!popRule)
	{
		return;
	}

	// rule status
	int ilRust = atoi(popRule->Values[igRueRustIdx]);
	if ( ilRust >= 2 )	// don't show archived flights
		return;
	/*
	switch (ilRust)
	{
		case 0:			// deactivated
			olRust = GetString(1499);		
			break;

		case 1:			// activated
			olRust = GetString(1498);		
			break;

		case 2:			// don't show archived flights
			return;
	}
	*/
	// rule type
	int ilRuty = atoi(popRule->Values[igRueRutyIdx]);
	
	// should independent rules be shown ?
	if (ilRuty >= 2 && !bmIsShowIndep)
		return;

	olTplUrno = popRule->Values[igRueUtplIdx];
	olTplName = ogBCD.GetField("TPL", "URNO", olTplUrno, "TNAM");
	if(ogPrivList.GetStat(olTplName) == '-')
		return;
	
	switch (ilRuty)
	{
		case 0:	// single rule
			olRuty = omTxtSingle;
			break;

		case 1: // collective rule
			olRuty = omTxtCollective ;
			break;

		case 2: // flight independent rule (zeitgesteuert)
		case 3: // flight independent rule (bedingungsgesteuert)
			olRuty = omTxtIndependent;
			break;

		default:
			olRuty = CString("?");
			break;
	}
	
	pomGrid->InsertRows(1, 1);
	
	if (popRule->Values[igRueExclIdx] == CString("1"))
	{
		olExcl = CString("X");
	}
	else
	{
		olExcl = CString(" ");
	}

	olRueShortName = popRule->Values[igRueRusnIdx];
	olRueName = popRule->Values[igRueRunaIdx];
	olRuePrio =	popRule->Values[igRuePrioIdx];
	olRueUrno = popRule->Values[igRueUrnoIdx];
	olEvty = popRule->Values[igRueEvtyIdx];
	olRust = popRule->Values[igRueRustIdx];
	
	CCSPtrArray <RecordSet> olValArray;
	ogBCD.GetRecords("VAL", "UVAL", olRueUrno, &olValArray);

	int ilSize = olValArray.GetSize();
	if ( ilSize > 1 )
		olValArray.Sort(&ValComp);
	olValCount.Format("%d", ilSize);

	if (ilSize >= 1)
	{
		olRueDBVato = olValArray[ilSize - 1].Values[igValVatoIdx];
		Vafr = DBStringToOleDateTime(olValArray[0].Values[igValVafrIdx]);
	}
	/*else
	{
		olRueDBVato = ogBCD.GetField("VAL", "UVAL", olRueUrno, "VATO");
		Vafr = DBStringToOleDateTime(ogBCD.GetField("VAL", "UVAL", olRueUrno, "VAFR"));
	}*/
	Vato = DBStringToOleDateTime(olRueDBVato);
	olValArray.DeleteAll();

	if (Vafr.m_status == COleDateTime::valid)
		olRueVafr = Vafr.Format();

	if (Vato.m_status == COleDateTime::valid)  
		olRueVato = Vato.Format();

	pomGrid->SetValueRange(CGXRange(1, 2), olExcl);
	pomGrid->SetValueRange(CGXRange(1, 3), olRueShortName);
	pomGrid->SetValueRange(CGXRange(1, 4), olRueName);
	pomGrid->SetValueRange(CGXRange(1, 5), olRuty);
	pomGrid->SetValueRange(CGXRange(1, 6), olRueVafr);
	pomGrid->SetValueRange(CGXRange(1, 7), olRueVato);
	pomGrid->SetValueRange(CGXRange(1, 8), olValCount);
	pomGrid->SetValueRange(CGXRange(1, 9), olTplName);
	pomGrid->SetValueRange(CGXRange(1, 10), olRust);
	pomGrid->SetValueRange(CGXRange(1, 11), olRueUrno);
	pomGrid->SetValueRange(CGXRange(1, 12), olTplUrno);
	pomGrid->SetValueRange(CGXRange(1, 13), olRuePrio);
	pomGrid->SetValueRange(CGXRange(1, 14), olEvty);

	if ( popRule->Values[igRueLstuIdx].GetLength() < 14 )
		olLstu = DBStringToOleDateTime(popRule->Values[igRueCdatIdx] );
	else
		olLstu = DBStringToOleDateTime(popRule->Values[igRueLstuIdx] );

	if (olLstu.m_status == COleDateTime::valid)
		olRueLstu = olLstu.Format();
	pomGrid->SetValueRange(CGXRange(1, 15), olRueLstu);

	ilBkColor = ogColors[WHITE_IDX];  // default;
	// set row colors
	if (ilRust == 0)	// deactivated: silver
	{
		ilBkColor = ogColors[SILVER_IDX];
	}
	else
	{
		switch ( ilRuty )
		{
			case 0:		// single rule
				if (olEvty == CString("0"))	// turnaround: blue
				{
					ilBkColor = ogColors[PYELLOW_IDX];
				}
				else if(olEvty == CString("1"))	// inbound: green
				{
					ilBkColor = ogColors[PGREEN_IDX];
				}
				else if(olEvty == CString("2"))	// outbound: yellow
				{
					ilBkColor = ogColors[PBLUE_IDX];
				}
				break;
			
			case 1:		// collective rule
				ilBkColor = LROSE;
				break;
			case 2:		// flight independent: white
			case 3:		// flight independent: white
				ilBkColor = ogColors[WHITE_IDX];
				break;
		}
	}

	olStyle.SetInterior(ilBkColor).SetEnabled(FALSE);
	pomGrid->SetStyleRange(CGXRange().SetRows(1), olStyle); 
	
	if (GetExpirationColor(olRueDBVato, ilExpirationColor))
	{
		pomGrid->SetStyleRange ( CGXRange(1,0), CGXStyle().SetInterior(ilExpirationColor)); 
	}
}
//---------------------------------------------------------------------------------------------------------------------

int RulesList::DoModal (CString opRueUrno, long lpTplUrno)
{
	lmSelectedTplUrno = lpTplUrno;
	omDataStringRuleUrno = opRueUrno;

	return CDialog::DoModal();
}
//---------------------------------------------------------------------------------------------------------------------

void RulesList::SetStaticTexts()
{
	SetWindowLangText(this, IDS_RULE_LIST );

	CWnd *pWnd = GetDlgItem(IDC_TPL_TXT);
	if (pWnd != NULL)
	{
		pWnd->SetWindowText(GetString(1452));
	}

	SetDlgItemLangText (this, RULIST_CHB_INDEP, IDS_SHOW_INDEP );

	pWnd = GetDlgItem(IDOK);
	if (pWnd != NULL)
	{
		pWnd->SetWindowText(GetString(243));
	}

	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd != NULL)
	{
		pWnd->SetWindowText(GetString(244));
	}
	omTxtSingle = GetString(1493);
	omTxtCollective = GetString(1494);
	omTxtIndependent = GetString(IDS_KURZ_INDEPENDENT);

	SetDlgItemLangText (this, ID_FILE_PRINT, IDS_PRINT );
	SetDlgItemLangText (this, ID_FILE_PRINT_PREVIEW, IDS_PRINT_PREVIEW );
}
//---------------------------------------------------------------------------------------------------------------------



void RulesList::OnFilePrint() 
{
	// TODO: Add your control notification handler code here
	if (pomActView && pomGrid)
	{
		pomActView->SetGridToPrint ( pomGrid );
		pomActView->SendMessage ( WM_COMMAND, ID_FILE_PRINT, 0 );
	}
	
}

void RulesList::OnFilePrintPreview() 
{
	// TODO: Add your control notification handler code here
	if (pomActView && pomGrid)
	{
		pomActView->SetGridToPrint ( pomGrid );
		pomActView->PreviewGrid ( this );
	}
	
}

void RulesList::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here

	if (pomActView)
		pomActView->SetGridToPrint (0);

}

void RulesList::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect olChildRect, olCliRect;
	int ilWidthDiff, ilHeightDiff;

	GetClientRect ( &olCliRect );
	if ( pomGrid	)
	{
		pomGrid->GetWindowRect ( &olChildRect );
		if ( imLastWidth && imLastHeight )
		{
			ilWidthDiff = olCliRect.Width () - imLastWidth;
			ilHeightDiff = olCliRect.Height () - imLastHeight;
			pomGrid->SetWindowPos( 0, 0, 0, 
										  olChildRect.Width() + ilWidthDiff, 
										  olChildRect.Height () + ilHeightDiff, 
										  SWP_NOMOVE | SWP_NOZORDER );
			pomGrid->Redraw ();
			for ( int i=0; i<5; i ++ )
			{
				pomDlgControls[i].GetWindowRect ( &olChildRect );
				ScreenToClient ( &olChildRect );
				pomDlgControls[i].SetWindowPos( 0, olChildRect.left, 
											   olChildRect.top + ilHeightDiff, 0, 0, 
											   SWP_NOSIZE | SWP_NOZORDER );
			}
			m_Chb_Indep.GetWindowRect ( &olChildRect );
			ScreenToClient ( &olChildRect );
			m_Chb_Indep.SetWindowPos( 0, olChildRect.left, 
									  olChildRect.top + ilHeightDiff, 0, 0, 
									  SWP_NOSIZE | SWP_NOZORDER );
			
			omComboBox.GetWindowRect ( &olChildRect );
			ScreenToClient ( &olChildRect );
			omComboBox.SetWindowPos( 0, olChildRect.left, 
									  olChildRect.top + ilHeightDiff, 0, 0, 
									  SWP_NOSIZE | SWP_NOZORDER );
	
		}

	}
	imLastWidth = olCliRect.Width ();
	imLastHeight = olCliRect.Height ();
}


void RulesList::IniColWidths ()
{
	char pclEntry[81], *ps1, *ps2 ;
	int ilValue;
	
	GetPrivProfString ( "RulesList", "ColWidths", 
					    "20, 30, 22, 100, 250, 22, 90, 90, 20, 100, 22,70,70,300,15,90",
						pclEntry, 81, ogIniFile );
	ps1= pclEntry;
	for ( int i=0; i<=RULESLIST_COL_COUNT; i++ )
	{
		if ( !sscanf ( ps1, "%d", &ilValue ) )
			break;
		pomGrid->SetColWidth ( i, i, ilValue );
		if ( ps2 = strchr ( ps1, ',' ) )
			ps1 = ps2+1;
		else
			break;
	}
}


void RulesList::SaveColWidths ()
{
	char pclEntry[81], pclTemp[11];
	int ilValue; 

	pclEntry[0] = '\0';
	for ( int i=0; i<RULESLIST_COL_COUNT; i++ )
	{
		ilValue = pomGrid->GetColWidth ( i );
		sprintf ( pclTemp, "%d, ", ilValue ) ;
		strcat ( pclEntry, pclTemp );
	}
	ilValue = pomGrid->GetColWidth ( RULESLIST_COL_COUNT );
	sprintf ( pclTemp, "%d", ilValue ) ;
	strcat ( pclEntry, pclTemp );
	WritePrivateProfileString ( "RulesList", "ColWidths", pclEntry, ogIniFile );
	

}
