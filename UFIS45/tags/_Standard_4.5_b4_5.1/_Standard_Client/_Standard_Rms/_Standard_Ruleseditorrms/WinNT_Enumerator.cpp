// WinNT_Enumerator.cpp: implementation of the CWinNT_Enumerator class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <Process.h>
#include <WinNT_Enumerator.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWinNT_Enumerator::CWinNT_Enumerator()
{

}

CWinNT_Enumerator::~CWinNT_Enumerator()
{

}

int CWinNT_Enumerator::InitEnumerate(CMapStringToPtr &MapProc)
{

    psapi = LoadLibrary("PSAPI.DLL");
    vdmdbg = LoadLibrary("VDMDBG.DLL");

	if ( NULL == psapi || NULL == vdmdbg )
        return 0;

	VDMEnumTaskWOWEx = (VDMENUMTASKWOWEX)GetProcAddress(
        (HINSTANCE)vdmdbg, "VDMEnumTaskWOWEx");

	EnumProcesses =(ENUMPROCESSES)GetProcAddress(
			(HINSTANCE)psapi, "EnumProcesses");

	GetModuleFileName = (GETMODULEFILENAME)GetProcAddress(
			(HINSTANCE)psapi, "GetModuleFileNameExA");

	GetModuleBaseName = (GETMODULEBASENAME)GetProcAddress(
			(HINSTANCE)psapi, "GetModuleBaseNameA");

	EnumProcessModules = (ENUMPROCESSMODULES)GetProcAddress(
			(HINSTANCE)psapi, "EnumProcessModules");

	if (
		NULL == VDMEnumTaskWOWEx	||
		NULL == EnumProcesses		|| 
		NULL == GetModuleFileName	|| 
		NULL == GetModuleBaseName	||
		NULL == EnumProcessModules  )
        return 0;

	return 0;
}

BOOL CWinNT_Enumerator::Enumerate(CMapStringToPtr &MapProc)
{
    DWORD process_ids[max_num];
    DWORD num_processes;

    if (
		NULL == VDMEnumTaskWOWEx	||
		NULL == EnumProcesses		|| 
		NULL == GetModuleFileName	|| 
		NULL == GetModuleBaseName	||
		NULL == EnumProcessModules  )
        return false;

    int success = EnumProcesses(process_ids, 
        sizeof(process_ids), 
        &num_processes);

    num_processes /= sizeof(process_ids[0]);


    for ( unsigned i=0; i<num_processes; i++) {

        HANDLE process = OpenProcess(
            PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 
            FALSE, 
            process_ids[i]);

        HMODULE modules[max_num];
        DWORD num_modules;
        char file_name[MAX_PATH];

        EnumProcessModules(process,  
                           modules, 
                           sizeof(modules), 
                           &num_modules);

        num_modules /= sizeof(modules[0]);

        if (GetModuleFileName(process, 
                              modules[0], 
                              file_name, 
                              sizeof(file_name))) 
        {
			MapProc.SetAt(file_name,(void*)(process_ids[i]));			

			GetModuleBaseName(process,
							  modules[0],
							  file_name,
							  sizeof(file_name));

            if ( 0 == _stricmp(file_name, "NTVDM.EXE"))
            {
                // We've got an NT VDM -- enumerate the processes
                // it contains.
                //VDMEnumTaskWOWEx(process_ids[i], show_task, (long)&disp);
				//afxDump << "process_ids[i]:" << process_ids[i] <<"\n";
            }
		

        }
        CloseHandle(process);
    }
	FreeLibrary((HINSTANCE)vdmdbg);
	FreeLibrary((HINSTANCE)psapi);
	return true;


	return 0;
}
