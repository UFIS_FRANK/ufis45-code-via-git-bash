#ifndef _DATASET_H_
#define _DATASET_H_


//#include "ccsglobl.h"
#include <CCSPtrArray.h>
#include <RecordSet.h>

class DataSet
{

public:
	DataSet();
	~DataSet();


//--- Implementation
public:
	int FillLogicRudObject(CString opUrue);
	int FillLogicRpfObject();
	int FillLogicRpqObject();
	int FillLogicRloObject();
	int FillLogicReqObject();
	bool ChangeRudTime(CString opRudUrno, int ipNewStart, int ipNewEnd, int ipType, int ipOnBl, int ipOffBl);
	void CreateRudFromSerList(bool bpIsSCRule, CStringArray &ropSerUrnos, CString opRueUrno, 
							  CString opTplUrno, CString opEvty, CString*popRudAloc=0,
							  bool bpCreateGroup = false, bool bpSendBc=true );
	void CreateRpfEntry(CString opRudUrno, RecordSet *popSef);
	void CreateRpqEntry(CString opRudUrno, CString opUdgr, RecordSet *popSeq);
	void CreateRloEntry(CString opRudUrno, RecordSet *popSel);
	void CreateEquEntry(CString opRudUrno, RecordSet *popSee);
	void CreateRudRecord (RecordSet &ropRudRecord, RecordSet &ropSerRecord, bool bpIsSCRule );
	bool ResetLogicTable ( CString ropTableName );
	bool AddOrChangeRessources( CString opResType, CString opUrno, CString opRudUrno, 
								CString opUdgr, CString opSrvRefUrno, CString opSrvRefCode, 
								bool bpChange, bool bpGroup, int ipAmount=1);
	bool DeleteRessource (CString opResType, CString opUrno ) ;
	bool DeleteRessources(RecordSet &ropRudRecord);

	// Deletes all records from RUD_TMP which are not of the specified type, ie. RUD.DRTY != ipIOT
	// -----------
	// IN: type of records to delete (0 = turnaround, 1 = inbound, 2 = outbound)
	// OUT: number of records left in data object
	int DeleteWrongTypeFromRudTmp(int ipIOT);
	bool ChangeWrongReference(RecordSet *popRud, int ipIOT);
	void ResetOffsetsAndConnections();
	
	bool AppendValue(RecordSet *popRud, CString opField, int ipDiff);

	bool CopyConnections(CMapStringToString  &opRudOldNewMap, /*CCSPtrArray <RecordSet> *popRudList*/CString opNewRueUrno);
	bool OnCopyRue ( CString &ropOldRueUrno, CString &ropNewRueUrno );
	bool OnCopyRud ( CString opRuleResTable, CMapStringToString  &ropOldNewRudUrnos, 
					 CMapStringToString *popOldNewUdgrs=0 );
	bool OnDeleteRue ( CString &ropRueUrno );
	void OnDeleteRuds ( CString opRuleResTable, CStringArray  &ropRudUrnos );
	void ResetLogicResourceTables ();
	bool DeleteOneRule ( CString &ropUrno );
	int	OnDeleteTpl ( CString &ropTplUrno );
	int CountOfStaffRudsForDGR ( CString &ropUdgr, bool bpLogicTables=true );
	bool IsSingleQualiUsedForRud ( CString &ropRPQUrno, CString &ropRPQUrud, 
								   CString &ropSrvRefUrno );
	void RemoveResources(CStringArray &opRudUrnos, CStringList *popUdgrList=0);
	bool DeleteByWhere ( CString opTable, CString opRefField, 
						 CString opRefValue, bool bpDbSave=true );
	bool ChangeRudStartEnd ( RecordSet &ropRudRecord, BOOL bpBecomesFloating );
	void SetRudStartEnd ( RecordSet &ropRudRecord, BOOL bpFloating, 
						  CString olStart, CString olEnd );
	void GetRudStartTime ( RecordSet &ropRudRecord, BOOL &bpFloating, 
						   CString &ropStart );
	void GetRudEndTime ( RecordSet &ropRudRecord, BOOL &bpFloating, 
						 CString &ropEnd );
	int FloatingRudsInDgr ( CString opTable, CString opDgrUrno );
	void SetDgrFloating ( CString opTable, CString opDgrUrno, BOOL &bpFloating );
	void SetAllDbarAndDear ( bool bpAbsolut );
	bool CopyOneRud ( CString opOldRudUrno, CString &ropNewRudUrno );
	bool SaveModifiedRule ( CString opUrue );
	bool SaveRuleChanges ( CString opTable, CStringList &ropOldUrnos );
	bool CheckRules ( CString opFilePath ) ;
	bool IsResourceOk ( CString opTable, CString opGtab, CString opResUrno,
					    CString opResCode, CString opTpl1, CString opTpl2, 
						CString &ropLine );
	bool IsEventOk ( CString opEvent, CString opTpl1, CString opTpl2, 
					 CStringArray &ropLineArr );
	bool SetFieldForDgr ( CString opTable, CString opDgrUrno, 
						  CString opField, CString opValue );
	bool GetFieldForDgr ( CString opTable, CString opDgrUrno, 
						 CString opField, CString &opValue, bool &bpEqual );
	bool CheckRudUtpl ( CString opUtpl );
	bool CheckArchive ( CString opFilePath, CString &ropTplUrno );
protected:
	
	int imOldDuration;


};

long CalcDutyDuration(CString opDebe,CString opDeen,CString opDedu, CString opDbfl,CString opDefl);

extern DataSet ogDataSet;

#endif	// _DATASET_H_