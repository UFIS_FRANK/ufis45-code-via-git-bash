// RegelWerkDoc.cpp : implementation of the CRegelWerkDoc class
//

#include <stdafx.h>
#include <RegelWerk.h>

#include <RegelWerkDoc.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif





IMPLEMENT_DYNCREATE(CRegelWerkDoc, CDocument)

BEGIN_MESSAGE_MAP(CRegelWerkDoc, CDocument)
	//{{AFX_MSG_MAP(CRegelWerkDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CRegelWerkDoc construction/destruction
CRegelWerkDoc::CRegelWerkDoc()
{
}

CRegelWerkDoc::~CRegelWerkDoc()
{
}

BOOL CRegelWerkDoc::OnNewDocument()
{
	SetTitle(GetString(1414));

	if (!CDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CRegelWerkDoc serialization

void CRegelWerkDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
	}
	else
	{
	}
}

/////////////////////////////////////////////////////////////////////////////
// CRegelWerkDoc diagnostics

#ifdef _DEBUG
void CRegelWerkDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRegelWerkDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRegelWerkDoc commands
