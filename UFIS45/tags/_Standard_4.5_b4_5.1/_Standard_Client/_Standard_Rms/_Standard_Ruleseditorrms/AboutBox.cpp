/////////////////////////////////////////////////////////////////////////////////////
// AboutBox - Implementation File

#include <stdafx.h>
#include <AboutBox.h>
#include <ccsglobl.h>
#include <MainFrm.h>
#include <InfoDlg.h>
#include <basicdata.h>
#include <versioninfo.h>
#include <LibGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-------------------------------------------------------------------------------------------------------------------
//					construction
//-------------------------------------------------------------------------------------------------------------------
CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT

}

//-------------------------------------------------------------------------------------------------------------------
//					data exchange, message map
//-------------------------------------------------------------------------------------------------------------------
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	DDX_Control(pDX, IDC_VERSION, m_Version);
	//}}AFX_DATA_MAP
}
//-------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(ABOUT_BTN_RULEINFO, OnBtnRuleinfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-------------------------------------------------------------------------------------------------------------------
//					message handlers
//-------------------------------------------------------------------------------------------------------------------
BOOL CAboutDlg::OnInitDialog() 
{
	CCS_TRY

	CDialog::OnInitDialog();
	SetStaticTexts();
	//  Decide whether Button "Info on Rule" has to be disabled
	CWnd *pWndActive = AfxGetMainWnd();
	CString olCurrRueUrno;
	CMainFrame *polFrameWnd =0;
	BOOL blEnalbe = FALSE;

	if ( pWndActive->GetRuntimeClass() == RUNTIME_CLASS(CMainFrame) )
	{
		polFrameWnd = static_cast< CMainFrame* >(pWndActive);
		olCurrRueUrno = polFrameWnd->GetRueUrno();
		if ( !olCurrRueUrno.IsEmpty() && olCurrRueUrno!=" " && olCurrRueUrno!="-1" )
		{
			blEnalbe = TRUE;
		}
	}
	EnableDlgItem ( this, ABOUT_BTN_RULEINFO, blEnalbe, TRUE );
	
	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));

	CCS_CATCH_ALL
	return TRUE;
}
//-------------------------------------------------------------------------------------------------------------------

void CAboutDlg::OnBtnRuleinfo() 
{
	CWnd *pWndActive = AfxGetMainWnd();
	CString olCurrRueUrno;
	CMainFrame *polFrameWnd =0;

	if ( pWndActive->GetRuntimeClass() == RUNTIME_CLASS(CMainFrame) )
	{
		polFrameWnd = static_cast< CMainFrame* >(pWndActive);
		olCurrRueUrno = polFrameWnd->GetRueUrno();
		if ( !olCurrRueUrno.IsEmpty() && olCurrRueUrno!=" " && olCurrRueUrno!="-1" )
		{
			InfoDlg *polDlg = new InfoDlg(olCurrRueUrno, this);
			polDlg->DoModal();
			delete polDlg;
		}
	}
}



//-------------------------------------------------------------------------------------------------------------------
//					helper functions
//-------------------------------------------------------------------------------------------------------------------
void CAboutDlg::SetStaticTexts()
{
	CWnd *pWnd = NULL;

	// caption
	///SetWindowLangText ( this, "STID", "1600" );
	SetWindowLangText ( this, 1600 );
	// statics
	CString olVersion, olText;
	olVersion.LoadString(129/*"IDS_VERSIONSTRING"*/);
	m_Version.SetWindowText(olVersion);

	VersionInfo olInfo;
	VersionInfo::GetVersionInfo(NULL,olInfo);

	olText.LoadString(166);
	olVersion.Format ( olText, olInfo.omFileVersion);
	olVersion += "  ";
	olVersion += CString(__DATE__);
	pWnd = GetDlgItem(IDC_VERSION2);
	if ( pWnd )
		pWnd->SetWindowText( olVersion );
	
	
	pWnd = GetDlgItem(IDC_COPYRIGHT);
	if ( pWnd )
		pWnd->SetWindowText(GetResString(1485/*"COPYRIGHT_MSG"*/));
	
	// buttons
	pWnd = GetDlgItem(IDOK);
	if ( pWnd )
		pWnd->SetWindowText(GetString(1398/*"OK"*/));
	pWnd = GetDlgItem(ABOUT_BTN_RULEINFO);
	if ( pWnd )
		pWnd->SetWindowText(GetString(1427/*"IDS_REGEL_INFO"*/));

	SetDlgItemLangText ( this, IDC_SERVERTITLE, IDS_SERVERTITLE );
	SetDlgItemLangText ( this, IDC_USERTITLE, IDS_USERTITLE );

	olText.Format("%s / %s",ogBasicData.omUserID,ogCommHandler.pcmReqId);
	SetDlgItemText ( IDC_USER, olText );
	olText.Format("%s / %s",ogCommHandler.pcmHostName,pcgHome);
	SetDlgItemText ( IDC_SERVER, olText );

}
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------