#if !defined(AFX_BARWINDOW_H__2A814343_2F84_11D3_A640_0000C007916B__INCLUDED_)
#define AFX_BARWINDOW_H__2A814343_2F84_11D3_A640_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <RecordSet.h>
#include <CCSGlobl.h>

#define LEFT_EDIT	0
#define RIGHT_EDIT	1

class CCSEdit;

/////////////////////////////////////////////////////////////////////////////
// BarWindow window

class BarWindow : public CWnd
{
// Construction
public:
	BarWindow(CString opUrno, CRect opRect, CWnd *pParent = NULL);
	virtual ~BarWindow();

	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BarWindow)
	//}}AFX_VIRTUAL



// Implementation
public:
	void CreateBarWindow();
	void DrawListBoxes(CRect opRect);
	void DrawEditControls(CRect opRect);
	void DrawReferenceTimeLines(CDC *pDC, CRect opRect);
	void DrawBarTimes(CDC *pDC, CRect opRect);
	void SetListBoxSel();
	void SetCurrentData(CString opRud, CString opEvty);
	void SetTimeIntervals(CString opTsdb, CString opTsde);
	static void SetFollowingTimeIntervals(RecordSet *popSource, int ipDiff);
	void SetControlsEnabled(drool ipLeftEdit = SKIP, drool ipLeftList = SKIP,
							drool ipRightEdit = SKIP, drool ipRightList = SKIP);
	
	static bool IsMovingOK(RecordSet *popRud);
	bool IfBigValueWarnUser(int ipValue);
	void MustLimitOffsetTo24Hours(int &ripValue, CCSEdit *popEdit);

// Attributes
public:
	CWnd *pomParent;
	CRect omRect;

	CComboBox *pomLeftList;
	CComboBox *pomRightList;
	CCSEdit *pomLeftEdit;
	CCSEdit *pomRightEdit;
	POINT	smLeftMinTxtPt;
	POINT	smRightMinTxtPt;

	CString omRudUrno;
	
	int imPrep; // preparation time
	int imWayt;	// way there
	int imDedu; // duration
	int imWayf; // way back
	int imFupt; // follow-up treatment
	
	CString omEvty; 

	int imPrepX;
	int imWaytX;
	int imDeduX;
	int imWayfX;
	int imFuptX;
	int imTextY;
	int imTsdb;
	int imTsde;
	
	bool bmFirstTimeInit;

	int imRudType;	// 0 = turnaround, 1 = inbound, 2 = outbound
	int imRueType;  // 0 = turnaround, 1 = inbound, 2 = outbound

// Generated message map functions
protected:
	//{{AFX_MSG(BarWindow)
	afx_msg void OnSelChangeLeft();
	afx_msg void OnSelChangeRight();
	afx_msg void OnEditKillFocus(WPARAM wParam, LPARAM lParam);
	afx_msg void OnAdaptTime(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BARWINDOW_H__2A814343_2F84_11D3_A640_0000C007916B__INCLUDED_)
