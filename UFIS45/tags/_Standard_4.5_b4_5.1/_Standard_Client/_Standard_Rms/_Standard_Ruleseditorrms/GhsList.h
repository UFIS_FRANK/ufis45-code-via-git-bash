// GhsList.h : header file
//
#if !defined(AFX_GHSLIST_H__C2FA1333_49D6_11D1_B3DF_0000B45A33F5__INCLUDED_)
#define AFX_GHSLIST_H__C2FA1333_49D6_11D1_B3DF_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000




#include <resource.h>
#include <CedaBasicData.h>
#include <CCSBasic.h>
#include <BasicData.h>
#include <GhsListViewer.h>
//#include "Table.h"

class CTableWithGrid;

//-------------------------------------------------------------------------------------------------------
//					defines, enums, structs, etc...
//-------------------------------------------------------------------------------------------------------
struct SER_LINEDATA
{
	int LineNo;
	CString Urno;
	CString Snam;

	SER_LINEDATA()
	{
		LineNo = -1;
		Urno = "";
		Snam = "";
	}
};



//-------------------------------------------------------------------------------------------------------
//					GhsList dialog
//-------------------------------------------------------------------------------------------------------
class GhsList : public CDialog
{

//--- Construction
public:
	GhsList(CWnd* pParent, 
			int ipSelectMode = (LBS_MULTIPLESEL | LBS_EXTENDEDSEL), 
			CString opCalledFrom = CString(""),
			long *lpUrno = NULL);   // standard constructor

	~GhsList();


//--- Dialog Data
	//{{AFX_DATA_MAP(GhsList)
	enum { IDD = IDD_AW_GHS };
	CComboBox	m_SerCBList;
	BOOL	m_Chb_CreateUlnk;
	//}}AFX_DATA
	

//--- Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GhsList)
	public:
	virtual BOOL DestroyWindow();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	//}}AFX_VIRTUAL


//--- Implementation
public:
	void UpdateDisplay();

protected:
	void Create();
	void UpdateComboBox();
	void CreateLine(SER_LINEDATA *prpSer);
	CString Format(CString opSerUrno);
	void SetBdpsState();
	void SetStaticTexts();
	void SendCreateLinkMessage();

	// Generated message map functions
	//{{AFX_MSG(GhsList)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
    afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
	afx_msg void OnView();
	afx_msg void OnSelchangeGhscblist();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point); 
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


//--- Attributes
	bool bmIsViewOpen;
	int imSelectMode;
	long m_nDialogBarHeight;

	CString omCalledFrom;
	CString omSerUrno;
	CString omSelectedSerUrno;
	
	CWnd *pomParent;
	CTableWithGrid *pomTable;
	GhsListViewer omSerListViewer;
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
	CCSPtrArray <SER_LINEDATA> omLines;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GHSLIST_H__C2FA1333_49D6_11D1_B3DF_0000B45A33F5__INCLUDED_)
