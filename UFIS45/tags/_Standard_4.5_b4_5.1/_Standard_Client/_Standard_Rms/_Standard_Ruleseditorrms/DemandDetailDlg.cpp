// DemandDetailDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSedit.h>
#include <regelwerk.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <DemandDetailDlg.h>
#include <ccsddx.h>
#include <CollectivesListDlg.h>
#include <BasicData.h>
#include <Dialog_VorlagenEditor.h>
#include <RulesFormView.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif


//-----------------------------------------------------------------------------------------------------------------------
//						defines, globals, static functions etc.
//-----------------------------------------------------------------------------------------------------------------------

//--- Local function prototype
static void DemandDetailCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);



//-----------------------------------------------------------------------------------------------------------------------
//						construction / destruction
//-----------------------------------------------------------------------------------------------------------------------
DemandDetailDlg::DemandDetailDlg(CString opRudUrno, RulesGanttViewer* pViewer, CWnd* pParent /*=NULL*/)
	: CDialog(DemandDetailDlg::IDD, pParent)
{
	omRudUrno = opRudUrno;

	pomParent = pParent;
	pomGanttViewer = pViewer;
	pomTimesGrid = new CGridFenster(this);
	pomLeftGrid = new CGridFenster(this);
	pomRightGrid = new CGridFenster(this);

	pomBarWnd = NULL;
	
	imRessourceType = 0;
	imDiff = 0;
	bmDestroying = false;
	
	//{{AFX_DATA_INIT(DemandDetailDlg)
	//}}AFX_DATA_INIT

	Create();

	ogDdx.Register(this, RUD_TMP_CHANGE, CString("DemandDetailDlg"), CString("RUD_TMP Changed"), DemandDetailCf);
	ogDdx.Register(this, SGR_NEW, CString("DemandDetailDlg"), CString("SGR New"), DemandDetailCf);
	ogDdx.Register(this, SGR_CHANGE, CString("DemandDetailDlg"), CString("SGR Change"), DemandDetailCf);
	ogDdx.Register(this, SGR_DELETE, CString("DemandDetailDlg"), CString("SGR Delete"), DemandDetailCf);
}
//-----------------------------------------------------------------------------------------------------------------------

DemandDetailDlg::~DemandDetailDlg()
{
	// remove pointer from table in ogDdx
	ogDdx.UnRegister(this, NOTUSED);

	// don't delete pomBarWnd in this class! See: BarWindow::OnDestroy()
	pomBarWnd = NULL;

	delete pomTimesGrid;
	delete pomLeftGrid;
	delete pomRightGrid;
}

    

//-----------------------------------------------------------------------------------------------------------------------
//					create, destroy
//-----------------------------------------------------------------------------------------------------------------------
void DemandDetailDlg::Create()
{
	CDialog::Create(IDD_DEMAND_DETAIL_DLG, pomParent);
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::Destroy()
{
	// take care that closing the dialog doesn't trigger OnEndEditing()
	// bmDestroying = true;

	// pomParent->bmDetailWindowOpen = false;
	DestroyWindow();
	delete this;
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if (pomParent != NULL)
	{
		pomParent->PostMessage(WM_DIALOG_CLOSED, (WPARAM) this);
	}
}



//-----------------------------------------------------------------------------------------------------------------------
//					data exchange, message map	
//-----------------------------------------------------------------------------------------------------------------------
void DemandDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DemandDetailDlg)
	DDX_Control(pDX, DEMDETAIL_COB_VREF, m_Cob_Reference);
	DDX_Control(pDX, IDCLOSE, m_Btn_Close);
	DDX_Control(pDX, TEMPL_COB_ALOD, m_AlocCB);
	DDX_Control(pDX, DEMDETAIL_EDT_SHORTNAME, m_Edt_ShortName);
	DDX_Control(pDX, DEMDETAIL_EDT_NAME, m_Edt_Name);
	DDX_Control(pDX, DEMDETAIL_EDT_SPLITMIN, m_Edt_SplitMin);
	DDX_Control(pDX, DEMDETAIL_EDT_SPLITMAX, m_Edt_SplitMax);
	DDX_Control(pDX, DEMDETAIL_CHB_SPLIT, m_Chb_Splittable);
	DDX_Control(pDX, DEMDETAIL_CHB_PARTIAL, m_Chb_Partial);
	DDX_Control(pDX, DEMDETAIL_CHB_OPERATIVE, m_Chb_Operative);
	DDX_Control(pDX, DEMDETAIL_CHB_CALCULATED, m_Chb_Calculated);
	DDX_Control(pDX, DEMDETAIL_RBT_INBOUND, m_RBt_Inbound);
	DDX_Control(pDX, DEMDETAIL_RBT_OUTBOUND, m_RBt_Outbound);
	DDX_Control(pDX, DEMDETAIL_RBT_TURNAROUND, m_RBt_Turnaround);
	// DDX_Control(pDX, DEMDETAIL_CHB_STANDARD, m_Chb_Standard);
	// DDX_Control(pDX, DEMDETAIL_CHB_CONTRACT, m_Chb_Contractable);
	// DDX_Control(pDX, DEMDETAIL_CHB_ACTI, m_Chb_Activated);
	//}}AFX_DATA_MAP
}
//-----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(DemandDetailDlg, CDialog)
	//{{AFX_MSG_MAP(DemandDetailDlg)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING, OnEndEditing)
	ON_BN_CLICKED(DEMDETAIL_RBT_INBOUND, OnInbound)
	ON_BN_CLICKED(DEMDETAIL_RBT_OUTBOUND, OnOutbound)
	ON_BN_CLICKED(DEMDETAIL_RBT_TURNAROUND, OnTurnaround)
	ON_BN_CLICKED(DEMDETAIL_CHB_CALCULATED, OnChbCalculated)
	ON_BN_CLICKED(DEMDETAIL_CHB_OPERATIVE, OnChbOperative)
	ON_BN_CLICKED(DEMDETAIL_CHB_PARTIAL, OnChbPartial)
	ON_BN_CLICKED(DEMDETAIL_CHB_SPLIT, OnChbSplit)
	ON_BN_CLICKED(IDCLOSE, OnClose)
	ON_WM_DESTROY()
	ON_CBN_SELENDOK(TEMPL_COB_ALOD, OnSelendokCobAlod)
	ON_BN_CLICKED(DEMDETAIL_BTN_COLLECTIVE, OnBtnCollective)
	ON_CBN_SELENDOK(DEMDETAIL_COB_VREF, OnSelendokCobVref)
	ON_EN_CHANGE(DEMDETAIL_EDT_SPLITMAX, OnChangeEdtSplitmax)
	ON_EN_CHANGE(DEMDETAIL_EDT_SPLITMIN, OnChangeEdtSplitmin)
	//}}AFX_MSG_MAP
	ON_MESSAGE(GRID_MESSAGE_RBUTTONCLICK, OnGridRButton)
END_MESSAGE_MAP()



//-----------------------------------------------------------------------------------------------------------------------
//					message handling	
//-----------------------------------------------------------------------------------------------------------------------
BOOL DemandDetailDlg::OnInitDialog() 
{
	CCS_TRY
	

	CDialog::OnInitDialog();
	
	SetStaticTexts();

	pomTimesGrid->SubclassDlgItem(DEMDETAIL_CUS_TIMES, this);
	pomLeftGrid->SubclassDlgItem(DEMDETAIL_CUS_FKT, this);
	pomRightGrid->SubclassDlgItem(DEMDETAIL_CUS_PERM, this);

	pomTimesGrid->Initialize();
	pomLeftGrid->Initialize();
	pomRightGrid->Initialize();

	TablesFirstTimeInit();
	MakeBarWindow();
	IniAlocCB (&m_AlocCB);

	pomFrameWnd = static_cast <CMainFrame*> (AfxGetMainWnd());
	if (::IsWindow(pomFrameWnd->GetSafeHwnd()))
	{
		pomTemplateCombo = pomFrameWnd->m_wndToolBar.pomComboBox;
	}
	else
	{
		pomTemplateCombo = NULL;
	}
	

	CCS_CATCH_ALL

	return TRUE;  
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnCancel()
{
	Destroy();
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnClose() 
{
	Destroy();
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnInbound() 
{
	CCS_TRY

	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	// start and end reference 
	CString olOldDrty = olRecord.Values[igRudDrtyIdx];
	if ( olOldDrty == "2" )
		olRecord.Values[igRudRtdbIdx] = ogInboundReferences[0].Field;
	olRecord.Values[igRudRtdeIdx] = olRecord.Values[igRudRtdbIdx] ;
	olRecord.Values[igRudDrtyIdx] = CString("1");
	olRecord.Values[igRudTsdeIdx] = CString("0");

	// get duration value if there is one in the table
	int ilDedu;
	ilDedu = atoi(pomTimesGrid->GetValueRowCol(1, 2)) * 60;
	if (ilDedu > 0)
	{
		CString olDedu;
		olDedu.Format("%d", ilDedu);
		olRecord.Values[igRudDeduIdx] = olDedu;
	}

	
	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
	SetIOTRadioButtons();

	pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetEnabled(TRUE)
																 .SetReadOnly(FALSE)
																 .SetInterior(WHITE));
	// enable / disable listboxes and edit fields

	// MNE,000914: enabled this again. Why was it taken out ? 
	pomBarWnd->pomLeftList->EnableWindow(TRUE);
	pomBarWnd->pomLeftList->ShowWindow(TRUE);
	pomBarWnd->pomRightList->EnableWindow(FALSE);
	pomBarWnd->pomRightList->ShowWindow(FALSE);
	pomBarWnd->pomLeftEdit->EnableWindow(TRUE);
	pomBarWnd->pomLeftEdit->ShowWindow(TRUE);
	pomBarWnd->pomRightEdit->EnableWindow(FALSE);
	pomBarWnd->pomRightEdit->ShowWindow(FALSE);

	pomBarWnd->SendMessage(WM_ADAPTTIME, 0, (LPARAM)&olRecord );
	pomBarWnd->SetListBoxSel();
	pomBarWnd->Invalidate();

	// NOTE: It is not sufficient to send a RUD_TMP_CHANGE message here, because we need to have more than one 
	//       line redrawn. RUD_CON_CHANGE will cause ChangeViewTo() to be executed, so that the whole gantt is
	//       redrawn [MNE, 990922]
	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);
	
	
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnOutbound() 
{
	CCS_TRY

		
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);
	
	CString olOldDrty = olRecord.Values[igRudDrtyIdx];
	if ( olOldDrty == "1" )
		olRecord.Values[igRudRtdeIdx] = ogOutboundReferences[0].Field;
	olRecord.Values[igRudRtdbIdx] = olRecord.Values[igRudRtdeIdx];
	olRecord.Values[igRudTsdbIdx] = CString("0"); 
	olRecord.Values[igRudDrtyIdx] = CString("2");

	// get duration value if there is one in the table
	int ilDedu;
	ilDedu = atoi(pomTimesGrid->GetValueRowCol(1, 2)) * 60;
	if (ilDedu > 0)
	{
		CString olDedu;
		olDedu.Format("%d", ilDedu);
		olRecord.Values[igRudDeduIdx] = olDedu;
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
	
	SetIOTRadioButtons();

	pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetEnabled(TRUE)
																 .SetReadOnly(FALSE)
																 .SetInterior(WHITE));

	// enable / disable listboxes
	pomBarWnd->pomLeftList->EnableWindow(FALSE);
	pomBarWnd->pomLeftList->ShowWindow(FALSE);
	pomBarWnd->pomRightList->EnableWindow(TRUE);
	pomBarWnd->pomRightList->ShowWindow(TRUE);
	pomBarWnd->pomLeftEdit->EnableWindow(FALSE);
	pomBarWnd->pomLeftEdit->ShowWindow(FALSE);
	pomBarWnd->pomRightEdit->EnableWindow(TRUE);
	pomBarWnd->pomRightEdit->ShowWindow(TRUE);

	pomBarWnd->SendMessage(WM_ADAPTTIME, 0, (LPARAM)&olRecord );
	pomBarWnd->SetListBoxSel();
	pomBarWnd->Invalidate();

	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnTurnaround() 
{
	CCS_TRY


	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);
	
	CString olOldDrty = olRecord.Values[igRudDrtyIdx];

	if ( olOldDrty == "2" )
		olRecord.Values[igRudRtdbIdx] = ogInboundReferences[0].Field;
	if ( olOldDrty == "1" )
		olRecord.Values[igRudRtdeIdx] = ogOutboundReferences[0].Field;
	olRecord.Values[igRudDeduIdx] = CString("0"); 
	olRecord.Values[igRudDrtyIdx] = CString("0");

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

	SetIOTRadioButtons();
	
	pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetEnabled(FALSE)
															     .SetReadOnly(TRUE)
																 .SetInterior(SILVER));
	

	// enable / disable listboxes
	pomBarWnd->pomLeftList->EnableWindow(TRUE);
	pomBarWnd->pomLeftList->ShowWindow(TRUE);
	pomBarWnd->pomRightList->EnableWindow(TRUE);
	pomBarWnd->pomRightList->ShowWindow(TRUE);
	pomBarWnd->pomLeftEdit->EnableWindow(TRUE);
	pomBarWnd->pomLeftEdit->ShowWindow(TRUE);
	pomBarWnd->pomRightEdit->EnableWindow(TRUE);
	pomBarWnd->pomRightEdit->ShowWindow(TRUE);
	
	pomBarWnd->SendMessage(WM_ADAPTTIME, 0, (LPARAM)&olRecord );
	pomBarWnd->SetListBoxSel();
	pomBarWnd->Invalidate();

	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

/*void DemandDetailDlg::OnChbActi() 
{
	CCS_TRY


	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	if (m_Chb_Activated.GetCheck() == CHECKED)
	{
		m_Chb_Activated.SetCheck(UNCHECKED);
		olRecord.Values[igRudFaddIdx] = CString("0");	// deactivated
	}
	else
	{
		m_Chb_Activated.SetCheck(CHECKED);
		olRecord.Values[igRudFaddIdx] = CString("1");	// activated
	}
	
	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


	CCS_CATCH_ALL
}*/
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnChbCalculated() 
{
	CCS_TRY


	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	if (m_Chb_Calculated.GetCheck() == CHECKED)
	{
		m_Chb_Calculated.SetCheck(UNCHECKED);
		olRecord.Values[igRudFncdIdx] = CString("0");	// nominated
	}
	else
	{
		m_Chb_Calculated.SetCheck(CHECKED);
		olRecord.Values[igRudFncdIdx] = CString("1");	// calculated
	}
	
	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

/*void DemandDetailDlg::OnChbContract() 
{
	CCS_TRY


	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	if (m_Chb_Contractable.GetCheck() == CHECKED)
	{
		m_Chb_Contractable.SetCheck(UNCHECKED);
		olRecord.Values[igRudFcndIdx] = CString("0");	// non-contractual
	}
	else
	{
		m_Chb_Contractable.SetCheck(CHECKED);
		olRecord.Values[igRudFcndIdx] = CString("1");	// contractual
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);

	CCS_CATCH_ALL
}*/
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnChbOperative()  
{
	CCS_TRY


	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	if (m_Chb_Operative.GetCheck() == CHECKED)
	{
		m_Chb_Operative.SetCheck(UNCHECKED);
		olRecord.Values[igRudFondIdx] = CString("0");	// operative
	}
	else
	{
		m_Chb_Operative.SetCheck(CHECKED);
		olRecord.Values[igRudFondIdx] = CString("1");	// non-operative
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnChbPartial() 
{
	CCS_TRY

	RecordSet olRecord;

	if (ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "FFPD") == CString("0"))	
		OnBtnCollective();
	else
	{
		ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "UCRU", CString(" "));
		ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "FFPD", CString("0"));
		m_Chb_Partial.SetCheck(UNCHECKED);
	}
	CString olFfpd;
	olFfpd = ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "FFPD" );
	EnableDlgItem(this, DEMDETAIL_BTN_COLLECTIVE, olFfpd=="1", true);
	
	if (ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord) )	
		ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRecord);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnChbSplit() 
{
	CCS_TRY	

		
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	m_Edt_SplitMin.SetWindowText(CString(""));
	m_Edt_SplitMax.SetWindowText(CString(""));

	if (m_Chb_Splittable.GetCheck() == CHECKED)
	{
		m_Chb_Splittable.SetCheck(UNCHECKED);
		olRecord.Values[igRudDideIdx] = CString("0");	// not divisable
		m_Edt_SplitMin.EnableWindow(FALSE);
		m_Edt_SplitMax.EnableWindow(FALSE);
	}
	else
	{
		m_Chb_Splittable.SetCheck(CHECKED);
		olRecord.Values[igRudDideIdx] = CString("1");	// divisable
		CString olDivisor = olRecord.Values[igRudDindIdx];
		m_Edt_SplitMin.EnableWindow(TRUE);
		m_Edt_SplitMax.EnableWindow(TRUE);
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

/*void DemandDetailDlg::OnChbStandard() 
{
	CCS_TRY	
	

	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	if (m_Chb_Standard.GetCheck() == CHECKED)
	{
		m_Chb_Standard.SetCheck(UNCHECKED);
		olRecord.Values[igRudFsadIdx] = CString("1");	// additional
	}
	else
	{
		m_Chb_Standard.SetCheck(CHECKED);
		olRecord.Values[igRudFsadIdx] = CString("0");	// standard
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


	CCS_CATCH_ALL
}*/
//------------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnChangeEdtSplitmax() 
{
	CCS_TRY	
	

	CString olSplitMax;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);
	
	if (m_Chb_Splittable.GetCheck() == CHECKED)
	{
		m_Edt_SplitMax.GetWindowText(olSplitMax);
		olRecord.Values[igRudMaxdIdx].Format("%d", (atoi(olSplitMax) * 60));
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnChangeEdtSplitmin() 
{
	CCS_TRY	
	

	CString olSplitMin;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRecord);

	if (m_Chb_Splittable.GetCheck() == CHECKED)
	{
		m_Edt_SplitMin.GetWindowText(olSplitMin);
		olRecord.Values[igRudMindIdx].Format("%d", (atoi(olSplitMin) * 60));
	}

	ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);


	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------------



// This function does different things depending on the grid it is called from:
//
// 1) TimesGrid: When changing the value of a cell in pomTimesGrid, this function sends a message to 
//				 make the gantt bars change and a second one to adapt the values in BarWindow  
// 
// 2) Ressource Grids: When entering data in the first column of the last (empty) row, set value 
//                     for second column accordingly
//
// 3) Ressource Grids: When changing data, save changes to the corresponding XXX_TMP object
//
void DemandDetailDlg::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
//---------------------------
//---	Times Grid		  ---
//---------------------------
	CGridFenster *polGrid = (CGridFenster*) wParam;
	CELLPOS *prlCellPos = (CELLPOS*) lParam;

	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	RecordSet olRud;
	
	if (polGrid == pomTimesGrid && !bmDestroying)
	{
		CString olCellText;
		olCellText = polGrid->GetValueRowCol(ilRow, ilCol);
		ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRud);

		if (ilCol == 2 && ilRow > 0)
		{
			if (olCellText.IsEmpty() == TRUE || olCellText == CString(" "))
			{
				olCellText = CString("0");
			}

			if (atoi(olCellText) < 0)
			{
				Beep(800, 200);
				pomTimesGrid->Undo();
				return;
			}
			
			if (ilRow == 1)		//  duration
			{
				pomGanttViewer->MoveTargetBars(ilRow - 1, olCellText, &olRud);
				olRud.Values[igRudDeduIdx].Format("%d", atoi(olCellText) * 60);
			}
			else if (ilRow == 2)	// preparation time
			{
				pomGanttViewer->MoveThisBar(ilRow - 1, olCellText, &olRud);
				pomGanttViewer->MoveTargetBars(ilRow - 1, olCellText, &olRud);
				olRud.Values[igRudSutiIdx].Format("%d", atoi(olCellText) * 60);
			}
			else if (ilRow == 3)	// follow-up treatment 
			{
				pomGanttViewer->MoveThisBar(ilRow - 1, olCellText, &olRud);
				pomGanttViewer->MoveTargetBars(ilRow - 1, olCellText, &olRud);
				olRud.Values[igRudSdtiIdx].Format("%d", atoi(olCellText) * 60);
			}
			else if (ilRow == 4)	// way there
			{
				pomGanttViewer->MoveThisBar(ilRow - 1, olCellText, &olRud);
				pomGanttViewer->MoveTargetBars(ilRow - 1, olCellText, &olRud);
				olRud.Values[igRudTtgtIdx].Format("%d", atoi(olCellText) * 60);
			}
			else if (ilRow == 5)	// way back
			{
				pomGanttViewer->MoveThisBar(ilRow - 1, olCellText, &olRud);
				pomGanttViewer->MoveTargetBars(ilRow - 1, olCellText, &olRud);
				olRud.Values[igRudTtgfIdx].Format("%d", atoi(olCellText) * 60);
			}

			bool blRet = ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
			
			// NOTE: It is not sufficient to send a RUD_TMP_CHANGE message here, because we need to have more than one 
			//       line redrawn. RUD_CON_CHANGE will cause ChangeViewTo to be executed, so that the whole gantt is
			//       redrawn [MNE, 990922]
			ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRud);
			
			WPARAM wParam = (WPARAM) NULL;
			LPARAM lParam = (LPARAM) &olRud;
			pomBarWnd->SendMessage(WM_ADAPTTIME, wParam, lParam);
		}
	}

//---------------------------
//---	Left Grid		  ---
//---------------------------
	CString olCode, olName, olUrno, olUdgr, olUrud;
	bool    blStaticGroup;
	if ( (polGrid == pomLeftGrid)  && ( ilRow > 0 ) )
	{
		if (imRessourceType == 0)	// functions 
		{
			if (ilCol == 1)
			{
				CString olPfcUrno;

				olCode = pomLeftGrid->GetValueRowCol(ilRow, 1);
				if ( olCode.IsEmpty () )	// L�schen von Funktionen hier nicht erlaubt
				{							// statt dessen au�erhalb Balken l�schen
					pomLeftGrid->Undo ();
					return;					
				}
				olUrno = pomLeftGrid->GetValueRowCol(ilRow, 3);
				blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
															pomLeftGrid,
												           "PFC",*/ olCode );
				if ( blStaticGroup )
					ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olPfcUrno);
				else
					ogBCD.GetFields("PFC", "FCTC", olCode, "FCTN", "URNO", olName, olPfcUrno);
				pomLeftGrid->SetValueRange(CGXRange(ilRow, 2), olName);
				ogDataSet.AddOrChangeRessources("RPF", olUrno, omRudUrno, olUdgr, 
												olPfcUrno, olCode, true, blStaticGroup);
			}
		}	
		
		if (imRessourceType == 1)	// equipment
		{
			
		}
		
		if (imRessourceType == 2)
		{/*	Links wird doch f�r Locations nichts umgestellt oder ?
			CString olReft, olRloc;

			olCode = pomLeftGrid->GetValueRowCol(ilRow, 1);
			olUrno = pomLeftGrid->GetValueRowCol(ilRow, 3);
			olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");
			olRloc = pomRightGrid->GetValueRowCol(ilRow, 1);
			ogDataSet.AddOrChangeRessources("RLO", olUrno, omRudUrno, olReft, 
											olRloc, true, 1, blStaticGroup);*/
		}
	}


//---------------------------
//---	Right Grid		  ---
//--------------------------- 
	if( (polGrid == pomRightGrid) && ( ilRow > 0 ) && (ilCol == 1) )
	{
		if (imRessourceType == 0)	// qualifications
		{
			// set corresponding name in right column
			CString olPerUrno;
			olCode = pomRightGrid->GetValueRowCol(ilRow, 1);
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			
			if ( olCode.IsEmpty () )	// hag990924
			{
				if ( !olUrno.IsEmpty () && 
					 ogDataSet.DeleteRessource ( "RPQ", olUrno ) )
					pomRightGrid->RemoveRows ( ilRow, ilRow );
				return;	
			}
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
														pomRightGrid,
												       "PER",*/ olCode );
			if ( blStaticGroup )
				ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olPerUrno);
			else
				ogBCD.GetFields("PER", "PRMC", olCode, "PRMN", "URNO", olName, olPerUrno);

			// change RPQ_TMP entry (line contained an entry before)
			if ( !olUrno.IsEmpty() )
			{
				olUdgr = ogBCD.GetField ( "RPQ_TMP", "URNO", olUrno, "UDGR" );
				if ( olUdgr.IsEmpty () )
				{
					olUrud = omRudUrno;
					if ( ogDataSet.IsSingleQualiUsedForRud ( olUrno, olUrud,
															 olPerUrno ) )
					{
						MessageBox ( GetString(RES_ALREADY_SEL) );
						//  vorherigen Code wieder setzen
						ogBCD.GetRecord (  "RPQ_TMP", "URNO", olUrno, olRud );
						if ( GetGroupName ( &olRud, igRpqGtabIdx, 
											igRpqUperIdx, olCode ) )
							olCode = "*"+ olCode;
						else
							olCode = olRud.Values[igRpqQucoIdx];
						pomRightGrid->SetValueRange(CGXRange(ilRow, ilCol),
													olCode);
						return;
					}
				}
				else
					olUrud.Empty ();
				ogDataSet.AddOrChangeRessources("RPQ", olUrno, olUrud, olUdgr, 
												olPerUrno, olCode, true, blStaticGroup);
			}
			// add RPQ_TMP entry (line contained no entry before)
			else
			{	//  neue Qualifikationen sind immer Einzelqualifikationen !
				CString olNewUrno;
				if ( ogDataSet.IsSingleQualiUsedForRud ( olNewUrno, omRudUrno, 
														 olPerUrno ) )
				{
					MessageBox ( GetString(RES_ALREADY_SEL) );
					pomRightGrid->SetValueRange (CGXRange(ilRow,ilCol), "");
					pomRightGrid->DeleteEmptyRow ( ilRow,ilCol );
					return;
				}
				olNewUrno.Format("%d", ogBCD.GetNextUrno());
				ogDataSet.AddOrChangeRessources("RPQ", olNewUrno, omRudUrno, "",
												olPerUrno, olCode, false, blStaticGroup);
				pomRightGrid->SetValueRange(CGXRange(ilRow, 3), olNewUrno);
			}
			pomRightGrid->SetValueRange(CGXRange(ilRow, 2), olName);
		}
		
		if (imRessourceType == 1)	// equipment
		{
			CString olEquUrno;

			olCode = pomRightGrid->GetValueRowCol(ilRow, 1);
			if ( olCode.IsEmpty () )	// L�schen von Funktionen hier nicht erlaubt
			{							// statt dessen au�erhalb Balken l�schen
				pomRightGrid->Undo ();
				return;					
			}
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, pomRightGrid,
												       "EQU",*/ olCode );
			if ( blStaticGroup )
				ogBCD.GetFields("SGR", "GRPN", olCode, "GRDS", "URNO", olName, olEquUrno);
			else
				ogBCD.GetFields("EQU", "GCDE", olCode, "ENAM", "URNO", olName, olEquUrno);
			pomRightGrid->SetValueRange(CGXRange(ilRow, 2), olName);
			ogDataSet.AddOrChangeRessources("REQ", olUrno, omRudUrno, olUdgr, 
											olEquUrno, olCode, true, blStaticGroup);
		}

		if (imRessourceType == 2)
		{
			CString olUrnoToSave, olReft;
			CString olTab, olFldn, olNewValue;
			olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
			olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");

			olNewValue = pomRightGrid->GetValueRowCol(ilRow, 1);
			if ( olNewValue.IsEmpty () )	// L�schen von Locations hier nicht erlaubt
			{								// statt dessen au�erhalb Balken l�schen
				pomRightGrid->Undo ();
				return;					
			}

			if (olReft.GetLength() == 8)
			{
				olTab = olReft.Left(3);
				olFldn = olReft.Right(4);
			}
			blStaticGroup = !IsSingleResourceSelected ( /*ilRow, ilCol, 
														pomRightGrid,
														olTab,*/ olNewValue );
			if ( blStaticGroup )
				olUrnoToSave = ogBCD.GetField("SGR", "GRPN", olNewValue, "URNO");
			else
				olUrnoToSave = ogBCD.GetField(olTab, olFldn, olNewValue, "URNO");

			// olAloUrno = pomLeftGrid->GetValueRowCol(ilRow, 3);
			ogDataSet.AddOrChangeRessources("RLO", olUrno, omRudUrno, olUdgr, olReft, 
											olUrnoToSave, true, blStaticGroup); 
		}
	}

	m_Btn_Close.SetFocus();
	pomTimesGrid->ResetCurrentCell();
}
//------------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnSelendokCobAlod() 
{
	// DALO (default allocation)
	int		ilSel = m_AlocCB.GetCurSel();
	long	llAloUrno = m_AlocCB.GetItemData(ilSel);
	CString	olAloUrno, olAloCode;

	if (llAloUrno != CB_ERR)
	{
		olAloUrno.Format("%d", llAloUrno);
		olAloCode = ogBCD.GetField ( "ALO", "URNO", olAloUrno, "ALOC" );
		ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "ALOC", olAloCode );
	}

	IniVrefCB();
	bool blDsp = !omRefTab.IsEmpty();
	EnableDlgItem ( this, DEMDETAIL_COB_VREF, blDsp, blDsp );
	EnableDlgItem ( this, DEMDETAIL_STA_VREF, blDsp, blDsp );
}



//-----------------------------------------------------------------------------------------------------------------------
//					set methods
//-----------------------------------------------------------------------------------------------------------------------
void DemandDetailDlg::SetTimeValues()
{
	CCS_TRY

	
	//--- get current rud record
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRudRecord);

	//--- set name and shortname 
	int ilValue;
	CString olValue;
	
	pomTimesGrid->LockUpdate(TRUE);

	// DEDU (demand duration)
	ilValue = atoi(olRudRecord.Values[igRudDeduIdx]) / 60;
	olValue.Format("%d", ilValue);
	pomTimesGrid->SetValueRange(CGXRange(1,2), olValue);
	
	// SUTI (setup time)
	ilValue = atoi(olRudRecord.Values[igRudSutiIdx]) / 60;
	olValue.Format("%d", ilValue);
	pomTimesGrid->SetValueRange(CGXRange(2,2), olValue);
	
	// SDTI (setdown time)
	ilValue = atoi(olRudRecord.Values[igRudSdtiIdx]) / 60;
	olValue.Format("%d", ilValue);
	pomTimesGrid->SetValueRange(CGXRange(3,2), olValue);

	// TTGT (time to go to)
	ilValue = atoi(olRudRecord.Values[igRudTtgtIdx]) / 60;
	olValue.Format("%d", ilValue);
	pomTimesGrid->SetValueRange(CGXRange(4,2), olValue);

	// TTGF (time to go from)
	ilValue = atoi(olRudRecord.Values[igRudTtgfIdx]) / 60;
	olValue.Format("%d", ilValue);
	pomTimesGrid->SetValueRange(CGXRange(5,2), olValue);


// *** Values in bar window
	// TSDB (time span at demand begin)
	CString olTsdb;
	ilValue = atoi(olRudRecord.Values[igRudTsdbIdx]) / 60;
	olTsdb.Format("%d", ilValue);

	// TSDE (time span at demand end)
	CString olTsde;
	ilValue = atoi(olRudRecord.Values[igRudTsdeIdx]) / 60;
	olTsde.Format("%d", ilValue);
	
	pomBarWnd->SetTimeIntervals(olTsdb, olTsde);

	if (m_RBt_Turnaround.GetCheck() == CHECKED)
	{
		pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetEnabled(FALSE) 
																 .SetReadOnly(TRUE)
																 .SetInterior(SILVER));
	}
	else
	{
		pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetEnabled(TRUE) 
																	 .SetReadOnly(FALSE)
																	 .SetInterior(WHITE));
	}
	
	// Das wird intern in BarWindow gehandelt !
	/*if (olRudRecord.Values[igRudUpdeIdx].IsEmpty() == FALSE && olRudRecord.Values[igRudUpdeIdx] != CString(" "))
	{
		pomBarWnd->SetControlsEnabled(NO, NO, NO, NO);
	}
	else
	{
		if (m_RBt_Inbound.GetCheck() == CHECKED)
		{
			pomBarWnd->SetControlsEnabled(YES, YES);
		}
		else if (m_RBt_Outbound.GetCheck() == CHECKED)
		{ 
			pomBarWnd->SetControlsEnabled(NO, NO, YES, YES);
		}
	}*/


	pomTimesGrid->LockUpdate(FALSE);
	pomTimesGrid->Redraw();

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetControls()
{
	CCS_TRY

	//--- get current rud record
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	ogBCD.GetRecord("RUD_TMP", "URNO", omRudUrno, olRudRecord);

	//--- set name and shortname 
	CString olTmp;
	olTmp = ogBCD.GetField("SER", "URNO", olRudRecord.Values[igRudUghsIdx], "SNAM");
	m_Edt_Name.SetWindowText(olTmp); 
	olTmp = ogBCD.GetField("SER", "URNO", olRudRecord.Values[igRudUghsIdx], "SECO");
	m_Edt_ShortName.SetWindowText(olTmp); 
	
	//--- set split info
	int ilDuration;
	CString olDuration;
	olTmp = olRudRecord.Values[igRudDideIdx];
	if (olTmp == CString("1"))					// divisable
	{
		m_Chb_Splittable.SetCheck(CHECKED);

		// in the database, times are saved in [sec], so we need to calc the minutes
		ilDuration = atoi(olRudRecord.Values[igRudMindIdx]);	
		olDuration.Format("%d", ilDuration / 60);
		m_Edt_SplitMin.SetWindowText(olDuration);
		m_Edt_SplitMin.EnableWindow(TRUE);

		ilDuration = atoi(olRudRecord.Values[igRudMaxdIdx]);	
		olDuration.Format("%d", ilDuration / 60);
		m_Edt_SplitMax.SetWindowText(olDuration);
		m_Edt_SplitMax.EnableWindow(TRUE);
	}
	else
	{
		m_Chb_Splittable.SetCheck(UNCHECKED);
		m_Edt_SplitMin.SetWindowText(CString(""));
		m_Edt_SplitMin.EnableWindow(FALSE);
		m_Edt_SplitMax.SetWindowText(CString(""));
		m_Edt_SplitMax.EnableWindow(FALSE);
	}


	// SOME OF THE ITEMS ARE COMMENTED OUT. THE CORRESPONDING
	// CONTROLS IN THE DIALOG ARE MADE INVISIBLE. MNE, 001016

	//--- first reset all other check boxes
	// m_Chb_Activated.SetCheck(UNCHECKED);
	// m_Chb_Contractable.SetCheck(UNCHECKED);
	// m_Chb_Standard.SetCheck(UNCHECKED);
	m_Chb_Calculated.SetCheck(UNCHECKED);
	m_Chb_Operative.SetCheck(UNCHECKED);
	m_Chb_Partial.SetCheck(UNCHECKED);
	
	
	//--- then set the ones that need to be set
	/*olTmp = olRudRecord.Values[igRudFaddIdx];
	if (olTmp == CString("0"))					//	deactivated
	{
		m_Chb_Activated.SetCheck(CHECKED);
	}
	
	olTmp = olRudRecord.Values[igRudFcndIdx];
	if (olTmp == CString("1"))					// contractual
	{
		m_Chb_Contractable.SetCheck(CHECKED);
	}
	
	olTmp = olRudRecord.Values[igRudFsadIdx];
	if (olTmp == CString("1"))					// additional = 1
	{
		m_Chb_Standard.SetCheck(CHECKED);
	}*/

	olTmp = olRudRecord.Values[igRudFfpdIdx];
	if (olTmp == CString("1"))					// partial
	{
		m_Chb_Partial.SetCheck(CHECKED);
	}

	olTmp = olRudRecord.Values[igRudFncdIdx];
	if (olTmp == CString("1"))					// calculated
	{
		m_Chb_Calculated.SetCheck(CHECKED);
	}

	olTmp = olRudRecord.Values[igRudFondIdx];
	if (olTmp == CString("1"))					// not operative
	{
		m_Chb_Operative.SetCheck(CHECKED);
	}

	// m_Chb_Standard.EnableWindow(FALSE);
	// m_Chb_Contractable.EnableWindow(FALSE);
	
	m_Chb_Calculated.EnableWindow(FALSE);


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetStaticTexts()
{
	CCS_TRY

	// caption
	SetWindowLangText(this, IDS_RELATIV_DEMANDS);

	// controls
	SetDlgItemLangText(this, DEMDETAIL_STA_SHORTNAME, CODE);
	SetDlgItemLangText(this, DEMDETAIL_STA_NAME, NAME);
	
	SetDlgItemLangText(this, DEMDETAIL_FRA_SERVREL, IDS_SERV_REL);
	SetDlgItemLangText(this, DEMDETAIL_FRA_REFERENCE, IDS_REFERENZ);

	SetDlgItemLangText (this, DEMDETAIL_RBT_INBOUND, IDS_INBOUND);
	SetDlgItemLangText (this, DEMDETAIL_RBT_OUTBOUND, IDS_OUTBOUND);
	SetDlgItemLangText (this, DEMDETAIL_RBT_TURNAROUND, IDS_TURNAROUND);

	SetDlgItemLangText (this, TEMPL_STA_ALOD, IDS_ZUORDNUNGSEINHEIT);
	SetDlgItemLangText (this, DEMDETAIL_CHB_SPLIT, IDS_TEILBAR);

	SetDlgItemLangText (this, DEMDETAIL_CHB_OPERATIVE, IDS_NOT_OPERATIV);
	SetDlgItemLangText (this, DEMDETAIL_CHB_CALCULATED, IDS_ERMITTELT);
	SetDlgItemLangText (this, DEMDETAIL_CHB_PARTIAL, 1002);

	// SetDlgItemLangText ( this, DEMDETAIL_CHB_ACTI, IDS_DEAKTIVIERT );
	// SetDlgItemLangText ( this, DEMDETAIL_CHB_CONTRACT, IDS_VERTRAGLICH );
	// SetDlgItemLangText ( this, DEMDETAIL_CHB_STANDARD, IDS_ZUSATZ );
	
	SetDlgItemLangText (this, DEMDETAIL_STA_SPLITMIN, IDS_SPLITMIN);
	SetDlgItemLangText (this, DEMDETAIL_STA_SPLITMAX, IDS_SPLITMAX);
	SetDlgItemLangText (this, IDCLOSE, 1375);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::InitData(CString opRueUrno, CString opEvty, CString opRudUrno)
{
	omCurrRueUrno = opRueUrno;
	omRudUrno = opRudUrno;
	
	AddItemToUpdList(opRudUrno, "RUD", ITEM_UPD);

	SetControls();
	SetIOTRadioButtons();
	EnableIOTRadioButtons(opEvty);
	SetTimeValues();	// must be called after SetIOTRadioButtons() !!!
	
	pomBarWnd->SetCurrentData(omRudUrno, opEvty);
	
	CString olSerUrno;
	CString olType;
	
	olType = ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "RETY");


	if (olType.GetLength() == 3)
	{
		if (olType.Left(1) == CString("1"))
		{
			SetTablesForStaffView();
			imRessourceType = 0;
		}
		else if(olType.Mid(1,1) == CString("1"))
		{
			SetTablesForEquipmentView();
			imRessourceType = 1;
		}
		else if(olType.Right(1) == CString("1"))
		{
			SetTablesForLocationsView();
			imRessourceType = 2;
		}
	}
	else
	{
		SetTablesForStaffView();	// default
	}
	SelectAloc (&m_AlocCB, omRudUrno);

	olType = ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "FFPD");
	EnableDlgItem(this, DEMDETAIL_BTN_COLLECTIVE, olType=="1", true);

	IniVrefCB();
	bool blDsp = !omRefTab.IsEmpty();
	EnableDlgItem ( this, DEMDETAIL_COB_VREF, blDsp, blDsp );
	EnableDlgItem ( this, DEMDETAIL_STA_VREF, blDsp, blDsp );

	ShowWindow(SW_SHOW);
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetIOTRadioButtons()
{
	CCS_TRY

	CString olBarType;
	olBarType = ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "DRTY");

	// check appropriate radio button
	int ilPrevSel = -1;
	if (m_RBt_Turnaround.GetCheck() == CHECKED)
	{
		ilPrevSel = 0;
	}
	else if(m_RBt_Inbound.GetCheck() == CHECKED)
	{
		ilPrevSel = 1;
	}
	else if(m_RBt_Outbound.GetCheck() == CHECKED)
	{
		ilPrevSel = 2;
	}

	m_RBt_Turnaround.SetCheck(UNCHECKED);
	m_RBt_Inbound.SetCheck(UNCHECKED);
	m_RBt_Outbound.SetCheck(UNCHECKED);


	if (olBarType == "0")	// turnaround
	{
		m_RBt_Turnaround.SetCheck(CHECKED);
		pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetValue("0")
																	 .SetEnabled(FALSE)); 		
		if (ilPrevSel != 0)
		{
			UpdateListEntries(0);
		}
	}
	else if(olBarType == "1")	// inbound
	{
		m_RBt_Inbound.SetCheck(CHECKED);
		pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetEnabled(TRUE)); 
		if (ilPrevSel != 1)
		{
			UpdateListEntries(1);
		}
	}
	else if(olBarType == "2" )		// outbound
	{
		m_RBt_Outbound.SetCheck(CHECKED);
		pomTimesGrid->SetStyleRange(CGXRange().SetRows(1), CGXStyle().SetEnabled(TRUE)); 
		if (ilPrevSel != 2)
		{
			UpdateListEntries(2);
		}
	}

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::EnableIOTRadioButtons(CString opEvty)
{
	if (opEvty == CString("0"))
	{
		m_RBt_Inbound.EnableWindow(TRUE);
		m_RBt_Outbound.EnableWindow(TRUE);
		m_RBt_Turnaround.EnableWindow(TRUE);
	}
	else if (opEvty == CString("1"))
	{
		m_RBt_Inbound.EnableWindow(TRUE);
		m_RBt_Outbound.EnableWindow(FALSE);
		m_RBt_Turnaround.EnableWindow(FALSE);
	}
	else if (opEvty == CString("2"))
	{
		m_RBt_Inbound.EnableWindow(FALSE);
		m_RBt_Outbound.EnableWindow(TRUE);
		m_RBt_Turnaround.EnableWindow(FALSE);
	}
}



//-----------------------------------------------------------------------------------------------------------------------
//					helper methods
//-----------------------------------------------------------------------------------------------------------------------

// Every grid initialization that has to be done only once belongs in this function
// (The rest is handled by the SetTablesForXXXXXView() - functions)
void DemandDetailDlg::TablesFirstTimeInit()
{
	CCS_TRY
	
	CRect olRect;
	int ilWidth ;
	
//--- times grid (left side)
	pomTimesGrid->LockUpdate(TRUE);
	// pomTimesGrid->GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, RGB(255,255,255));
	pomTimesGrid->SetColCount(3);
	pomTimesGrid->SetRowCount(5); //pomTimesGrid->SetRowCount(7); MNE, 990824

	pomTimesGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
	pomTimesGrid->GetParam()->SetNumberedRowHeaders(FALSE); 

	pomTimesGrid->GetParam()->EnableTrackRowHeight(0);
	pomTimesGrid->GetParam()->EnableTrackColWidth(0);
	pomTimesGrid->SetSortingEnabled(false);
	
	pomTimesGrid->SetValueRange(CGXRange(0,2), CString("[min]"));
	pomTimesGrid->SetValueRange(CGXRange(1,1), GetString(1469));	// DEDU
	pomTimesGrid->SetValueRange(CGXRange(1,3), CString("RUD.DEDU"));
	pomTimesGrid->SetValueRange(CGXRange(2,1), GetString(1470));	// SUTI
	pomTimesGrid->SetValueRange(CGXRange(2,3), CString("RUD.SUTI"));
	pomTimesGrid->SetValueRange(CGXRange(3,1), GetString(1471));	// SDTI
	pomTimesGrid->SetValueRange(CGXRange(3,3), CString("RUD.SDTI"));
	pomTimesGrid->SetValueRange(CGXRange(4,1), GetString(1472));	// TTGT
	pomTimesGrid->SetValueRange(CGXRange(4,3), CString("RUD.TTGT"));
	pomTimesGrid->SetValueRange(CGXRange(5,1), GetString(1473));	// TTGF
	pomTimesGrid->SetValueRange(CGXRange(5,3), CString("RUD.TTGF"));

	pomTimesGrid->SetStyleRange(CGXRange(0,0,pomTimesGrid->GetRowCount(), 1), 
									CGXStyle().SetEnabled(FALSE)
									          .SetReadOnly(TRUE));
																								
	pomTimesGrid->SetColWidth(0,0,20);	// header
	pomTimesGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomTimesGrid->SetColWidth(0,0,20);
	pomTimesGrid->SetColWidth(1,1,ilWidth*2 / 3);
	pomTimesGrid->SetColWidth(2,2,ilWidth / 3);

	pomTimesGrid->HideCols(3,3);		// TAB.FLDN (eg. "RUD.DEDU")
	
	pomTimesGrid->SetStyleRange ( CGXRange(1,2), CGXStyle().SetMaxLength(7) );
	pomTimesGrid->SetStyleRange ( CGXRange(2,2,5,2), CGXStyle().SetMaxLength(3) );

	pomTimesGrid->LockUpdate(FALSE);
	pomTimesGrid->Redraw();


//--- left grid (functions, locations)
	pomLeftGrid->RegisterGxCbsDropDown ( true, true );
	pomRightGrid->RegisterGxCbsDropDown ( true, true );

	pomLeftGrid->SetRowCount(1);
	pomLeftGrid->SetColCount(3);
	
	pomLeftGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
	pomLeftGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomLeftGrid->GetParam()->EnableSelection(GX_SELNONE );
	pomLeftGrid->GetParam()->EnableTrackRowHeight(FALSE);
	//hag001122 pomLeftGrid->GetParam()->EnableTrackColWidth(FALSE);
	pomLeftGrid->SetAutoGrow(false);
	
	pomLeftGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomLeftGrid->SetColWidth(0,0,20);
	pomLeftGrid->SetColWidth(1,1,ilWidth / 2);
	pomLeftGrid->SetColWidth(2,2,ilWidth / 2);
	
	pomLeftGrid->HideCols(3,3);
	pomLeftGrid->Redraw();


//--- right grid (qualifications)
	pomRightGrid->SetRowCount(1);
	pomRightGrid->SetColCount(3);

	pomRightGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
	pomRightGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomRightGrid->GetParam()->EnableSelection(GX_SELNONE );	
	pomRightGrid->GetParam()->EnableTrackRowHeight(0); 
	pomRightGrid->SetAutoGrow(true);

	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);
	pomRightGrid->SetColWidth(1,1,ilWidth / 2);
	pomRightGrid->SetColWidth(2,2,ilWidth / 2);
	
	pomRightGrid->HideCols(3,3);
	pomRightGrid->Redraw();

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetTablesForStaffView()
{
	CCS_TRY


	CCSPtrArray <RecordSet> olRightArr;
	CCSPtrArray <RecordSet> olLeftArr;

	ogBCD.GetRecords("RPF_TMP", "URUD", omRudUrno, &olLeftArr);
	ogBCD.GetRecords("RPQ_TMP", "URUD", omRudUrno, &olRightArr);
	
	int ilLeftCount = olLeftArr.GetSize();
	int ilRightCount = olRightArr.GetSize();

	ClearRightTables();

//--- make functions choicelist
	
	CString olItem, olTable, olUdgr;
	CString olChoiceList;
	int     ilRows, ilCols;
	BOOL	ilOk;
	
	olChoiceList = GetChoiceList("PFC", "FCTC");

//--- functions table
	RecordSet olRecord;
	CString olName;
	CString olCode;
	CString olUrno;

	pomLeftGrid->LockUpdate(TRUE);
	pomLeftGrid->InsertRows(1,1);
	
	// reset to staff settings (in case a location was shown before)
	CRect olRect;
	pomLeftGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	int ilWidth = olRect.Width() - 20;
	pomLeftGrid->SetColWidth(0,0,20);
	pomLeftGrid->SetColWidth(1,1,ilWidth / 2);
	pomLeftGrid->SetColWidth(2,2,ilWidth / 2);
	pomLeftGrid->HideCols(2, 2, FALSE);

	pomLeftGrid->SetStyleRange(CGXRange(1, 1), CGXStyle()
										 .SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										 .SetChoiceList(olChoiceList));

	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomLeftGrid->SetValueRange(CGXRange(0,2), GetString(PFC_HEADER));
	pomLeftGrid->SetValueRange(CGXRange(0,3), "Urno");	// RPF urno
	
	//- fill cells with data
	for (int i = 0; i < ilLeftCount; i++)
	{
		// pomLeftGrid->InsertRows(1,1);
		olRecord = olLeftArr[i];

		olUrno = olRecord.Values[igRpfUrnoIdx];
		if ( GetGroupName ( &olRecord, igRpfGtabIdx, igRpfUpfcIdx, olCode ) )	//hag990827
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				//hag990827
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igRpfFccoIdx];
			ogBCD.GetField("PFC", "FCTC", olCode, "FCTN", olName);
		}
		pomLeftGrid->SetValueRange(CGXRange(1,1), olCode);
		pomLeftGrid->SetValueRange(CGXRange(1,2), olName);
		pomLeftGrid->SetValueRange(CGXRange(1,3), olUrno);
	}

	pomLeftGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomLeftGrid->LockUpdate(FALSE);
	pomLeftGrid->Redraw();
	
	olChoiceList = GetChoiceList("PER", "PRMC");

//--- qualifications table
	pomRightGrid->LockUpdate(TRUE);
	pomRightGrid->InsertRows(1, 1);

	// reset staff settings in case locations were shown before
	pomRightGrid->SetAutoGrow(true);
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);
	pomRightGrid->SetColWidth(1,1,ilWidth / 2);
	pomRightGrid->SetColWidth(2,2,ilWidth / 2);
	pomRightGrid->HideCols(2, 2, FALSE);

	pomRightGrid->SetStyleRange(CGXRange(1, 1), CGXStyle()
										 .SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										 .SetChoiceList(olChoiceList));

	//- column names
	pomRightGrid->SetValueRange(CGXRange(0, 1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0, 2), GetString(PER_HEADER));
	pomRightGrid->SetValueRange(CGXRange(0, 3), "Urno");

	//- fill cells with data
	for (i = 0; i < ilRightCount; i++)
	{
		olRecord = olRightArr[i];

		olUrno = olRecord.Values[igRpqUrnoIdx];
		if ( GetGroupName ( &olRecord, igRpqGtabIdx, igRpqUperIdx, olCode ) )	//hag990827
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				//hag990827
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igRpqQucoIdx];
			ogBCD.GetField("PER", "PRMC", olCode, "PRMN", olName);
		}
		
		ilOk = pomRightGrid->SetValueRange(CGXRange(i + 1, 1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 2), olName);
		pomRightGrid->SetValueRange(CGXRange(i + 1, 3), olUrno);
		pomRightGrid->DoAutoGrow(i+1, 1);				//hag990831
		ilRows = pomRightGrid->GetRowCount ();
	}
	//  Zus�tzlich nicht zugeordnete Qualifikationen dieser Demandgroup anzeigen
	ilCols = pomRightGrid->GetColCount ();
	if ( ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "UDGR", olUdgr ) &&
		 !olUdgr.IsEmpty() )
	{
		ogBCD.GetRecords("RPQ_TMP", "UDGR", olUdgr, &olRightArr);
		
		for (i = 0; i < olRightArr.GetSize(); i++)
		{
			olRecord = olRightArr[i];
	
			olUrno = olRecord.Values[igRpqUrnoIdx];
			if ( GetGroupName ( &olRecord, igRpqGtabIdx, igRpqUperIdx, olCode ) )	
			{
				ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);				
				olCode = "*"+ olCode;
			}
			else
			{
				olCode = olRecord.Values[igRpqQucoIdx];
				ogBCD.GetField("PER", "PRMC", olCode, "PRMN", olName);
			}
			ilRows = ilRightCount+i+1;
			ilOk = pomRightGrid->SetValueRange(CGXRange(ilRows, 1), olCode);
			pomRightGrid->SetValueRange(CGXRange(ilRows, 2), olName);
			pomRightGrid->SetValueRange(CGXRange(ilRows, 3), olUrno);
			pomRightGrid->DoAutoGrow(ilRows, 1);				//hag990831
			pomRightGrid->SetStyleRange(CGXRange(ilRows, 1, ilRows, 2 ),
										CGXStyle().SetInterior(ogColors[PYELLOW_IDX]));
	//		olStr.Format("%d",ilRows+1);
	//		pomRightGrid->SetValueRange( CGXRange(ilRows+1, 0), olStr );
		}
		
	}
	
	pomRightGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomRightGrid->LockUpdate(FALSE);
	pomRightGrid->Redraw();

	olLeftArr.DeleteAll();
	olRightArr.DeleteAll();

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetTablesForEquipmentView()
{
	// This functionality has not been implemented yet, so we just disable the grids now


	CCSPtrArray <RecordSet> olRightArr;
	CStringArray			olTypeArray;	
	CString					olChoiceList;
	CRect					olRect;
	int						ilWidth, i;
	RecordSet				olRecord;
	CString					olName;
	CString					olCode;
	CString					olUrno, olType;
	
	ogBCD.GetRecords("REQ_TMP", "URUD", omRudUrno, &olRightArr);
	int ilRightCount = olRightArr.GetSize();

	ClearRightTables();


	//--- location table
	pomRightGrid->SetAutoGrow(false);
	pomRightGrid->LockUpdate(TRUE);
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(0,0,20);			// row header
	pomRightGrid->SetColWidth(1,1,ilWidth / 2);	// code
	pomRightGrid->SetColWidth(2,2,ilWidth / 2);	// name
	pomRightGrid->HideCols(2, 2, FALSE);	// show second column

	//- column names
	pomRightGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0,2), GetString(NAME));
	pomRightGrid->SetValueRange(CGXRange(0,3), CString("Urno"));
	
	//- fill cells with data
	for (i = 0; i < ilRightCount; i++)
	{
		olRecord = olRightArr[i];

		olUrno = olRecord.Values[igReqUrnoIdx];
		olTypeArray.SetAtGrow(i, olRecord.Values[igReqEtypIdx]);
		if ( GetGroupName ( &olRecord, igReqGtabIdx, igReqUequIdx, olCode ) )
		{
			ogBCD.GetField("SGR", "GRPN", olCode, "GRDS", olName);			
			olCode = "*"+ olCode;
		}
		else
		{
			olCode = olRecord.Values[igReqEqcoIdx];
			ogBCD.GetField("EQU", "GCDE", olCode, "ENAM", olName);
		}
		pomRightGrid->InsertRows(i + 1, 1);
		olChoiceList = GetChoiceList("EQU", "GCDE", &olRecord.Values[igReqEtypIdx] );
		pomRightGrid->SetStyleRange(CGXRange(i + 1, 1), CGXStyle()
											.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
											.SetChoiceList(olChoiceList));
		//- fill cell with data
		pomRightGrid->SetValueRange(CGXRange(i+1,1), olCode);
		pomRightGrid->SetValueRange(CGXRange(i+1,2), olName);
		pomRightGrid->SetValueRange(CGXRange(i+1,3), olUrno);
	}

	pomRightGrid->SetStyleRange(CGXRange().SetCols(2), CGXStyle().SetEnabled(FALSE) );
	pomRightGrid->LockUpdate(FALSE);
	pomRightGrid->Redraw();	


	//--- equipment table types
	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(NAME));
	pomLeftGrid->SetValueRange(CGXRange(0,2), "");

	//- fill cells with data
	if (igEqtNameIdx >= 0)		//  Wenn ALO da und gef�llt
	{
		pomLeftGrid->LockUpdate(TRUE);
		for ( i=0; i<ilRightCount; i++ )
		{
			CString olReft, olTable, olCodeField, olDesc;
			olType = olTypeArray.GetAt ( i );
	
			olName = ogBCD.GetField("EQT", "URNO", olType, "NAME");
			pomLeftGrid->InsertRows(i + 1, 1);
			pomLeftGrid->SetValueRange(CGXRange(i+1,1), olName);
			pomLeftGrid->SetValueRange(CGXRange(i+1,2), "");
			pomLeftGrid->SetStyleRange(CGXRange().SetRows(i + 1), CGXStyle().SetEnabled(FALSE));
		}
		pomLeftGrid->GetClientRect(&olRect);
		olRect.DeflateRect(3, 0);
		ilWidth = olRect.Width() - 20;
		pomLeftGrid->SetColWidth(1,1,ilWidth);	// code
		pomLeftGrid->HideCols(2, 2);
		pomLeftGrid->LockUpdate(FALSE);
		pomLeftGrid->Redraw();
	}
	olRightArr.DeleteAll();
	SetDlgItemText ( INDEP_STA_VRGC, GetString(IDS_EQUIPMENT_TYPES) );
	SetDlgItemText ( INDEP_STA_PERM, GetString(EQU_HEADER) );
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SetTablesForLocationsView()
{
	CCS_TRY

	
	CCSPtrArray <RecordSet> olRightArr;
	CString olItem;
	CString olChoiceList;
	CStringArray olReftArray;	
	CRect		 olRect;
	
	ogBCD.GetRecords("RLO_TMP", "URUD", omRudUrno, &olRightArr);

	int i, ilWidth;
	int ilLeftCount = ogBCD.GetDataCount("ALO");
	int ilRightCount = olRightArr.GetSize();

	ClearRightTables();

	//--- location table
	pomRightGrid->LockUpdate(TRUE);
	pomRightGrid->HideCols(2, 3);	// hide cols 2 (unused) and 3 (RLO urno)

	CString olCode, olTable, olCodeField, olUrno;
	ogBCD.GetRecords("RLO_TMP", "URUD", omRudUrno, &olRightArr);

	for (i = 0; i < ilRightCount; i++)
	{
		olUrno = olRightArr[i].Values[igRloUrnoIdx];
		//hag990827
		if ( GetGroupName ( &(olRightArr[i]), igRloGtabIdx, igRloRlocIdx, olCode ) )
			olCode = "*"+ olCode;
		else
			if ( !GetLocationCode(olRightArr[i].Values[igRloRlocIdx], olRightArr[i].Values[igRloReftIdx], olCode) )
			{
				olCode = "???";
			}
		olReftArray.SetAtGrow(i, olRightArr[i].Values[igRloReftIdx]);
		olChoiceList = CString("");
		if (ParseReftFieldEntry(olRightArr[i].Values[igRloReftIdx], olTable, olCodeField))
		{/*		now analogous to FlightIndepRulesView::SetTablesForLocationsView()
			ilSize = ogBCD.GetDataCount(olTable);
			for (j = 0; j < ilSize; j++)
			{
				olItem = ogBCD.GetField(olTable, j, olCodeField);
				olChoiceList += olItem + "\n";
			}
			int ilGrpAnz = AddGroupsToChoiceList ( olChoiceList, olTable ) ;	//hag990826*/
			olChoiceList = GetChoiceList(olTable, olCodeField);
		}
		pomRightGrid->InsertRows(i + 1, 1);
		pomRightGrid->SetStyleRange(CGXRange(i + 1, 1), CGXStyle()
											.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
											.SetChoiceList(olChoiceList)
											.SetValue(olCode));
		pomRightGrid->SetValueRange(CGXRange(i + 1, 3), olUrno);
	}

	//- column names
	pomRightGrid->SetValueRange(CGXRange(0, 1), GetString(CODE));
	pomRightGrid->SetValueRange(CGXRange(0, 2), "");
	pomRightGrid->SetAutoGrow(false);
	pomRightGrid->GetClientRect(&olRect);
	olRect.DeflateRect(3, 0);
	ilWidth = olRect.Width() - 20;
	pomRightGrid->SetColWidth(1,1,ilWidth);
	pomRightGrid->LockUpdate(FALSE);
	pomRightGrid->Redraw();

	//--- location table types
	//- column names
	pomLeftGrid->SetValueRange(CGXRange(0,1), GetString(CODE));
	pomLeftGrid->SetValueRange(CGXRange(0,2), "");

	//- fill cells with data
	//	pomLeftGrid->SetValueRange(CGXRange(1,2), "");
	if ( ilLeftCount > 0 )		//  Wenn ALO da und gef�llt
	{
		pomLeftGrid->LockUpdate(TRUE);
		for ( i=0; i<ilRightCount; i++ )
		{
			CString olReft, olTable, olCodeField, olDesc;
			olReft = olReftArray.GetAt ( i );
	
			olDesc = ogBCD.GetField("ALO", "REFT", olReft, "ALOD" );
			pomLeftGrid->InsertRows(i+1, 1);
			pomLeftGrid->SetValueRange(CGXRange(1,1), olDesc );
			pomLeftGrid->SetValueRange(CGXRange(1,2), "");
			pomLeftGrid->SetStyleRange( CGXRange().SetRows(i+1), CGXStyle().SetEnabled(FALSE) );
		}
		pomLeftGrid->GetClientRect(&olRect);
		olRect.DeflateRect(3, 0);
		ilWidth = olRect.Width() - 20;
		pomLeftGrid->SetColWidth(1,1,ilWidth);
		pomLeftGrid->HideCols(2, 2);	// hide col 2 
		pomLeftGrid->LockUpdate(FALSE);
		pomLeftGrid->Redraw();
	}
	
	olRightArr.DeleteAll();

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::ClearRightTables()
{
	CCS_TRY

	
	// left grid
	int ilCount = pomLeftGrid->GetRowCount();
	if (ilCount > 0)
	{
		pomLeftGrid->RemoveRows(1, ilCount);
	}

	// right grid
	ilCount = pomRightGrid->GetRowCount();
	if (ilCount > 0)
	{
		pomRightGrid->RemoveRows(1, ilCount);
	}


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::MakeBarWindow()
{
	CCS_TRY


	CWnd *polFrame = GetDlgItem(DEMDETAIL_FRA_CHANGE);
	if (::IsWindow(polFrame->GetSafeHwnd()) == TRUE)
	{
		CRect olRect;		
		polFrame->GetWindowRect(olRect);
		ScreenToClient(olRect);
		olRect.top += 10;
		olRect.right -= 4;
		olRect.bottom -= 80;
		olRect.left += 4;
		
		

		pomBarWnd = new BarWindow(omRudUrno, olRect, this);
		
		if (::IsWindow(pomBarWnd->GetSafeHwnd()) == TRUE)
		{
			pomBarWnd->ShowWindow(SW_SHOW);
		}
	}


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::SelectAloc (CComboBox *popAlocCb, CString &ropRudUrno )
{
	int		ilSel = -1;
	CString	olAloCode, olAlod;

	if ( !ropRudUrno.IsEmpty() )
		olAloCode = ogBCD.GetField ( "RUD_TMP", "URNO", ropRudUrno, "ALOC" );
	if (olAloCode.IsEmpty() == FALSE)
	{
		olAlod = ogBCD.GetField("ALO", "ALOC", olAloCode, "ALOD");
	}
	if (olAlod.IsEmpty() == FALSE)
	{
		ilSel = popAlocCb->FindStringExact (-1, olAlod);
	}
	popAlocCb->SetCurSel(ilSel);
}
//---------------------------------------------------------------------------

void DemandDetailDlg::IniAlocCB ( CComboBox *popAlocCb, bool bpAddEmptyString /*=false*/ )
{
	CString olAlod, olUrno;
	int ilCount, i, ilPos ;
	
	ilCount = ogBCD.GetDataCount("ALO");
	for (i = 0; i < ilCount; i++)
	{
		olAlod = ogBCD.GetField("ALO", i, "ALOD");
		olUrno = ogBCD.GetField("ALO", i, "URNO");
		ilPos = popAlocCb->AddString(olAlod);
		popAlocCb->SetItemData(ilPos, atol(olUrno));
	}
	if ( bpAddEmptyString )
	{
		ilPos = popAlocCb->AddString( "" );
		popAlocCb->SetItemData(ilPos, CB_ERR);
	}
}
//---------------------------------------------------------------------------

CString DemandDetailDlg::GetCurrentRud()
{
	return omRudUrno;
}
//---------------------------------------------------------------------------

void DemandDetailDlg::UpdateListEntries(int ipIOT)
{
	//---------------------------
	// ipIOT:
	// == 0: refers to turnaround
	// == 1: refers to inbound
	// == 2: refers to outbound
	//---------------------------
	
	CComboBox *polLeft = NULL;
	CComboBox *polRight = NULL;

	if (pomBarWnd != NULL)
	{
		if (pomBarWnd->pomLeftList != NULL)
		{
			polLeft = pomBarWnd->pomLeftList;
			polLeft->ResetContent();
		}

		if (pomBarWnd->pomRightList != NULL)
		{
			polRight = pomBarWnd->pomRightList;
			polRight->ResetContent();
		}
	}
	
	int ilCount1 = ogInboundReferences.GetSize();
	int ilCount2 = ogOutboundReferences.GetSize();
	
	CString olTmp;
	int ilPos = 0;
	if (ipIOT == 0)		// turnaround
	{
		if (polLeft != NULL)
		{
			for (int i = 0; i < ilCount1; i++)
			{
				polLeft->AddString(ogInboundReferences[i].Name);
				polLeft->SetItemData(i, ogInboundReferences[i].ID);
			}
			polLeft->SetCurSel(0);
		}
		if (polRight != NULL)
		{
			for (int i = 0; i < ilCount2; i++)
			{
				polRight->AddString(ogOutboundReferences[i].Name);
				polRight->SetItemData(i, ogOutboundReferences[i].ID);
			}
			polRight->SetCurSel(0);
		}
	}
	else if (ipIOT == 1)	// inbound
	{
		if (polLeft != NULL)
		{
			for (int i = 0; i < ilCount1; i++)
			{
				polLeft->AddString(ogInboundReferences[i].Name);
				polLeft->SetItemData(i, ogInboundReferences[i].ID);
			}
			polLeft->SetCurSel(0);
		}
		if (polRight != NULL)
		{
			for (int i = 0; i < ilCount1; i++)
			{
				polRight->AddString(ogInboundReferences[i].Name);
				polRight->SetItemData(i, ogInboundReferences[i].ID);
			}
			polRight->SetCurSel(0);
		}
	}
	else if (ipIOT == 2)	// outbound
	{
		if (polLeft != NULL)
		{
			for (int i = 0; i < ilCount2; i++)
			{
				polLeft->AddString(ogOutboundReferences[i].Name);
				polLeft->SetItemData(i, ogOutboundReferences[i].ID);
			}
			polLeft->SetCurSel(0);
		}
		if (polRight != NULL)
		{
			for (int i = 0; i < ilCount2; i++)
			{
				polRight->AddString(ogOutboundReferences[i].Name);
				polRight->SetItemData(i, ogOutboundReferences[i].ID);
			}
			polRight->SetCurSel(0);
		}
	}
		
}

// Combobox 'Reference' Init Msg Handler
void DemandDetailDlg::IniVrefCB()
{
	CString olRudAloc, olAloReft, olDspTxt, olUrno;
	int ilCount, i;

	m_Cob_Reference.ResetContent();
	omRefTab = omRefDsp = omRefVal = "";

	if ( ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "ALOC", olRudAloc ) &&
		 ogBCD.GetField("ALO", "ALOC", olRudAloc, "REFT", olAloReft ) )
	{
		if ( (olAloReft.GetLength()>=3) && 
			 (olAloReft.Left ( 3 ) == "CIC") )
		{
			omRefTab = "CCC";		//  tabelle, die in Referen-Combobox angezeigt wird
			omRefDsp = "CICN";		//  Feld, das in Referen-Combobox angezeigt wird
			omRefVal = "URNO";		//  Feld, auf das im RUD-Satz verwiesen wird
		}
	}
	if ( !omRefTab.IsEmpty() )
	{
		CString olRudVref, olRudReft, olSelectedUrno;
		ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "VREF", olRudVref );
		ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "REFT", olRudReft );
		if ( ( olRudReft.GetLength() >= 8 ) && 
			 ( olRudReft.Right(4) == omRefVal ) &&
			 ( olRudReft.Left(3) == omRefTab ) )	//  gleicher Bezug
			olSelectedUrno = ogBCD.GetField(omRefTab, omRefVal, olRudVref, "URNO" );
		
		ilCount = ogBCD.GetDataCount(omRefTab);
		for (i = 0; i < ilCount; i++)
		{
			olDspTxt = ogBCD.GetField(omRefTab, i, omRefDsp);
			olUrno = ogBCD.GetField(omRefTab, i, "URNO");
			int ilPos = m_Cob_Reference.AddString(olDspTxt);
			m_Cob_Reference.SetItemData(ilPos, atol(olUrno));
			if ( olUrno == olSelectedUrno )
				m_Cob_Reference.SetCurSel ( ilPos );
		}
	}
	else
	{
		ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "VREF", "" );
		ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "REFT", "" );
	}
	bool blDsp = !omRefTab.IsEmpty() ;
	EnableDlgItem ( this, DEMDETAIL_COB_VREF, blDsp, blDsp );
	EnableDlgItem ( this, DEMDETAIL_STA_VREF, blDsp, blDsp );
}
//---------------------------------------------------------------------------

// Combobox 'Reference' SelEndOK Msg Handler
void DemandDetailDlg::OnSelendokCobVref() 
{
	long  ilRefUrno=-1;
	CString olRudReft, olRudVref, olUrno;
	int		ilSel = m_Cob_Reference.GetCurSel();
	if ( ilSel >= 0 )
		ilRefUrno = m_Cob_Reference.GetItemData(ilSel);
	if ( ilRefUrno > 0 )
	{
		olRudReft = omRefTab + "." + omRefVal;
		olUrno.Format ( "%ld", ilRefUrno );
		olRudVref = ogBCD.GetField(omRefTab, "URNO", olUrno, omRefVal );
	}
	ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "VREF", olRudVref );
	ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "REFT", olRudReft );
}
//---------------------------------------------------------------------------

void DemandDetailDlg::OnBtnCollective() 
{
	CString olFfpd, olUcru, olTplUrno ;
	olFfpd = ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "FFPD") ;
	if ( olFfpd != "0" )
		olUcru = ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "UCRU") ;

	if (pomTemplateCombo != NULL)
		GetSelectedTplUrno(*pomTemplateCombo, olTplUrno, false);
	 
	CollectivesListDlg olDlg(olTplUrno, this);
	if ( !olUcru.IsEmpty() )
	{
		olDlg.omRudUrno = olUcru;
		olDlg.omRueUrno = ogBCD.GetField("RUD", "URNO", olUcru, "URUE");
	}
	olDlg.omRudAloc = ogBCD.GetField("RUD_TMP", "URNO", omRudUrno, "ALOC");

	int ilReturn = olDlg.DoModal();
	if (ilReturn != IDCANCEL)
	{
		if (ilReturn == IDOK && olDlg.omRueUrno != CString("-1"))
		{
			CString olCollRudUrno ;
			if ( olDlg.omRudUrno != "-1" )
				olCollRudUrno = olDlg.omRudUrno;
			else
			{
				olCollRudUrno = GetResourceInCollective ( omRudUrno, olDlg.omRueUrno );
			}
			ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "UCRU", olCollRudUrno );	// partial demand
			ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "FFPD", CString("1"));
			m_Chb_Partial.SetCheck(CHECKED);
		}
		else
		{
			MessageBox(GetString(1436), GetString(1437));
			ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "UCRU", CString(" "));			// full demand
			ogBCD.SetField("RUD_TMP", "URNO", omRudUrno, "FFPD", CString("0"));
			m_Chb_Partial.SetCheck(UNCHECKED);
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------

void DemandDetailDlg::OnGridRButton(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
	
	CGridFenster*polGrid = (CGridFenster*) wParam;
	CELLPOS		*prlCellPos = (CELLPOS*) lParam;
	if ( !polGrid || ! prlCellPos || (polGrid !=pomRightGrid)  )
		return;
	ROWCOL ilCol = (ROWCOL) prlCellPos->Col;
	ROWCOL ilRow = (ROWCOL) prlCellPos->Row;
	if ( !omRudUrno.IsEmpty() && !imRessourceType )
	{
		CString		olUrno, olRpqUdgr, olRudUdgr, olUrud;
		RecordSet	olRecord;

		olUrno = pomRightGrid->GetValueRowCol(ilRow, 3);
		if ( !olUrno.IsEmpty () &&	
			 ogBCD.GetRecord ( "RPQ_TMP", "URNO", olUrno, olRecord ) )
		{
			olUrud = olRecord.Values[igRpqUrudIdx];
			olRpqUdgr = olRecord.Values[igRpqUdgrIdx];
			if ( olUrud.IsEmpty () )	//  Gruppenqualifikation wird zugeordnet
			{
				olRecord.Values[igRpqUrudIdx] = omRudUrno;
				olRecord.Values[igRpqUdgrIdx] = "";
			}
			else			//  Einzelqualifikation wird Gruppenqualifikation 
			{
				olRudUdgr = ogBCD.GetField ( "RUD_TMP", "URNO", omRudUrno, "UDGR" );
				olRecord.Values[igRpqUrudIdx] = "";
				olRecord.Values[igRpqUdgrIdx] = olRudUdgr;
			}
			ogBCD.SetRecord ( "RPQ_TMP", "URNO", olUrno, olRecord.Values ) ;
			SetTablesForStaffView();
		}
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------



//  ist opCode in Zelle (ipRow,ipCol) eine einzelne Resource oder eine Gruppe
//  wenn Gruppe, ropCode um Kennzeichnung f�r Gruppe (*) bereinigen
/*
bool DemandDetailDlg::IsSingleResourceSelected ( ROWCOL ipRow, ROWCOL ipCol,
												 CGridFenster *popGrid,
												 CString opTable, CString &ropCode )
{
	if ( !ropCode.IsEmpty() && ropCode[0] == '*' )
	{
		ropCode = ropCode.Right ( ropCode.GetLength()-1 );
		return false;
	}
	return true;
}
*/


//---------------------------------------------------------------------------
//					processing functions
//---------------------------------------------------------------------------
void DemandDetailDlg::ProcessRudTmpChange(RecordSet *popRud)
{
	CCS_TRY
	

	if (pomTimesGrid != NULL && popRud != NULL)
	{
		pomTimesGrid->LockUpdate(TRUE);
		CString olTmp;

		// DEDU (duration)
		int ilTmp = atoi(popRud->Values[igRudDeduIdx]) / 60;
		olTmp.Format("%d", ilTmp);
		pomTimesGrid->SetValueRange(CGXRange(1, 2), olTmp);

		// SUTI (setup time)
		ilTmp = atoi(popRud->Values[igRudSutiIdx]) / 60;
		olTmp.Format("%d", ilTmp);
		pomTimesGrid->SetValueRange(CGXRange(2, 2), olTmp);

		// SDTI (setdown time)
		ilTmp = atoi(popRud->Values[igRudSdtiIdx]) / 60;
		olTmp.Format("%d", ilTmp);
		pomTimesGrid->SetValueRange(CGXRange(3, 2), olTmp);

		// TTGT (time to get there)
		ilTmp = atoi(popRud->Values[igRudTtgtIdx]) / 60;
		olTmp.Format("%d", ilTmp);
		pomTimesGrid->SetValueRange(CGXRange(4, 2), olTmp);

		// TTGF (time to get back)
		ilTmp = atoi(popRud->Values[igRudTtgfIdx]) / 60;
		olTmp.Format("%d", ilTmp);
		pomTimesGrid->SetValueRange(CGXRange(5, 2), olTmp);


		// TSDB (time span at demand begin)
		CString olTsdb;
		ilTmp = atoi(popRud->Values[igRudTsdbIdx]) / 60;
		olTsdb.Format("%d", ilTmp);
		
		// TSDE (time span at demand end)
		CString olTsde;
		ilTmp = atoi(popRud->Values[igRudTsdeIdx]) / 60;
		olTsde.Format("%d", ilTmp);
		
		pomBarWnd->SetTimeIntervals(olTsdb, olTsde);

		pomTimesGrid->LockUpdate(FALSE);
		pomTimesGrid->Redraw();
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------

void DemandDetailDlg::ProcessRudTmpDelete(RecordSet *popRud)
{
	if (popRud->Values[igRudUrnoIdx] == omCurrRueUrno)
	{
		Destroy();	
	}
}
//---------------------------------------------------------------------------

void DemandDetailDlg::ProcessSgrNew(RecordSet *popRecord)
{
	CCS_TRY

	CString olDdxRefTab;		// reference TAB.FLDN from record sent by DDX
	CString olGroupName;		// name of static group sent by DDX
	bool	blSgrFitsActTpl = true;

	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];

	blSgrFitsActTpl = IsSgrForActTpl ( popRecord->Values[igSgrUrnoIdx] );

	CString olChoiceList;

	if ( imRessourceType == 0 )			//  Personal	
	{
		if ( olDdxRefTab=="PFC" )
		{
			// This call will only temporarily update the choicelist in the window
			pomLeftGrid->ModifyAllResChoiceLists ( olGroupName, blSgrFitsActTpl, true );

			// MNE 000917
			// This updates the global choicelists array as well
			/*
			CString olChoiceList;
			olChoiceList = GetChoiceList("PFC", "FCTC");
			if (olChoiceList.IsEmpty() == FALSE)
			{
				if (olChoiceList.Right(1) != "\n")
					olChoiceList += "\n";

				olChoiceList += olGroupName;
				//SetChoiceList("PFC", "FCTC", olChoiceList);
			}*/
			// END MNE

		}
	
		if ( olDdxRefTab=="PER" )
		{
			// This call will only temporarily update the choicelist in the window
			pomRightGrid->ModifyAllResChoiceLists ( olGroupName, blSgrFitsActTpl, true );

			// MNE 000917
			// This updates the global choicelists array as well
			/*
			CString olChoiceList;
			olChoiceList = GetChoiceList("PER", "PRMC");
			if (olChoiceList.IsEmpty() == FALSE)
			{
				if (olChoiceList.Right(1) != "\n")
					olChoiceList += "\n";

				olChoiceList += olGroupName;
				//SetChoiceList("PER", "PRMC", olChoiceList);
			}*/
			// END MNE
		}
	}

	if ( imRessourceType == 2 )			//  Locations	
	{
		//  alle Zeilen checken, ob dargestellte Tabelle==olDdxRefTab
		for ( ROWCOL i=1; i<=pomRightGrid->GetRowCount(); i++ )
		{
			CString olUrno, olReft, olResTable, olCodeField;
			olUrno = pomRightGrid->GetValueRowCol(i, 3);
			if ( !olUrno.IsEmpty() )
				olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");
			if ( ParseReftFieldEntry ( olReft, olResTable, olCodeField ) &&
				 (olResTable==olDdxRefTab) )
				pomRightGrid->ModifyResChoiceList( olGroupName, blSgrFitsActTpl, true, i );
		}
	}
	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, blSgrFitsActTpl );

	CCS_CATCH_ALL	
}
//---------------------------------------------------------------------------

void DemandDetailDlg::ProcessSgrDelete(RecordSet *popRecord)
{
	CString olDdxRefTab;		// reference TAB.FLDN from record sent by DDX
	CString olGroupName;		// name of static group sent by DDX

	olDdxRefTab = popRecord->Values[igSgrTabnIdx];
	olGroupName = popRecord->Values[igSgrGrpnIdx];

	if ( imRessourceType == 0 )			//  Personal	
	{
		if ( olDdxRefTab=="PFC" )
			pomLeftGrid->ModifyAllResChoiceLists ( olGroupName, false, true );
		if ( olDdxRefTab=="PER" )
			pomRightGrid->ModifyAllResChoiceLists ( olGroupName, false, true );
	}
	if ( imRessourceType == 2 )			//  Locations	
	{
		//  alle Zeilen checken, ob dargestellte Tabelle==olDdxRefTab
		for ( ROWCOL i=1; i<=pomRightGrid->GetRowCount(); i++ )
		{
			CString olUrno, olReft, olResTable, olCodeField;
			olUrno = pomRightGrid->GetValueRowCol(i, 3);
			if ( !olUrno.IsEmpty() )
				olReft = ogBCD.GetField("RLO_TMP", "URNO", olUrno, "REFT");
			if ( ParseReftFieldEntry ( olReft, olResTable, olCodeField ) &&
				 (olResTable==olDdxRefTab) )
				pomRightGrid->ModifyResChoiceList( olGroupName, false, true, i );
		}
	}
	ChangeResChoiceLists ( olDdxRefTab, popRecord, true, false );
}



//---------------------------------------------------------------------------
//			implementation of static and global functions
//---------------------------------------------------------------------------
static void DemandDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    DemandDetailDlg *polViewer = (DemandDetailDlg *)popInstance;

	/*if (ipDDXType == RUD_TMP_NEW)
	{
		polViewer->ProcessRudTmpNew((RecordSet *)vpDataPointer);
	}*/
	if (ipDDXType == RUD_TMP_CHANGE)
	{
		polViewer->ProcessRudTmpChange((RecordSet *)vpDataPointer);
	}
	
	if (ipDDXType == RUD_TMP_DELETE)
	{
		polViewer->ProcessRudTmpDelete((RecordSet *)vpDataPointer);
	}
	/*if (ipDDXType == RUE_DELETE)
	{
		polViewer->ProcessRuleDelete((RecordSet *)vpDataPointer);
	}*/
	if ( (ipDDXType==SGR_NEW) || (ipDDXType==SGR_CHANGE) )
		polViewer->ProcessSgrNew((RecordSet*)vpDataPointer);
	if ( ipDDXType==SGR_DELETE )
		polViewer->ProcessSgrDelete((RecordSet*)vpDataPointer);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
