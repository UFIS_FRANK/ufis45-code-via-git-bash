#if !defined(AFX_FLIGHTINDEPRULESVIEW_H__2B6A6851_35F7_11D3_A647_0000C007916B__INCLUDED_)
#define AFX_FLIGHTINDEPRULESVIEW_H__2B6A6851_35F7_11D3_A647_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FlightIndepRulesView.h : header file
//

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif


#include <CCSEdit.h>
#include <CCSDragDropCtrl.h>
#include <RulesFormView.h>
//#include "Table.h"

class CGridFenster;
class CDutyGrid;
class CMainFrame;
class CIndepViewer;
class RulesGantt;
class CCSTimeScale ;

class FlightIndepRulesView : public CRulesFormView
{
public:
	FlightIndepRulesView();           // protected constructor used by dynamic creation
	~FlightIndepRulesView();
	
	DECLARE_DYNCREATE(FlightIndepRulesView)



//--- Implementation
	// message handling (called from MainFrm.cpp)
	void OnLoadRule(CString opRueUrno, CString opTplName, bool bpNoComboChange);
	void OnNewRule();
	void OnSaveRule();
	BOOL OnHelpInfo(HELPINFO* pHelpInfo) ;
//	void OnDelete();
	void OnLeistungen();
	// void OnContractBtn(); // momentarily nnot used, mne 010122
	// void OnGroupBtn();	// momentarily nnot used, mne 010122
	void OnSelEndOK();
	void OnCopy();
	
	// helper functions
	void MakeBarMenu(int itemID, CPoint point);
	void TablesFirstTimeInit();
	void EnableDemandDetailControls(bool bpEnable);
	void FillDemandDetailControls();
	void UpdateServiceTable();
	
	void ClearRessourceTables();
	void SetTablesForStaffView();
	void SetTablesForEquipmentView();
	void SetTablesForLocationsView();

//	bool RemoveRessources(CStringArray &opRudUrnos);
//	bool CopyDemandRequirements();
//	bool CopyRessources(CStringArray &opRudUrnos);

	//bool SaveValidity();
	bool SaveDemandRequirements();
//	bool SaveFunctions();
//	bool SaveQualifications();
//	bool SaveEquipment();
//	bool SaveLocations(); 
	
	bool IsSameTimeInDgr(bool &rbStart, bool &rbEnd);

	bool IsPassFilter(RecordSet *popRud);

	// processing functions
	void ProcessRudTmpNew(RecordSet *popRecord);
	void ProcessRudTmpChange(RecordSet *popRecord);
	void ProcessRudTmpDelete(RecordSet *popRecord);
	CString GetRuleEventType() {return "1";};
	void ProcessSgrNew(RecordSet *popRecord);
	void ProcessSgrDelete(RecordSet *popRecord);
	void AddServices ( CStringArray	&ropSerUrnos );

//	bool IsSingleResourceSelected ( ROWCOL ipRow, ROWCOL ipCol,
//									CGridFenster *popGrid,
//									CString opTable, CString &ropCode );
	bool InitializeCondTable();
	CString ConstructPrioString();
	CString ConstructEVRM();
	void SetStaticTexts();
	void DisplayQualisForRud ( CString &ropRudUrno );
	int GetResRecordsToDisplay( CString opTable,
								CCSPtrArray <RecordSet> *popRecords );

	void ResetResourceTable ( CGridFenster *popGrid );
	void IniTimesGrid ();
	void SetTimeValues ( CString &ropRudUrno );
	long CalcAndDisplayDuration(CString opDebe,CString opDeen,CString opDedu, 
									CString opDbfl,CString opDefl);

	void SetCurrentRudUrno ( CString opRudUrno );
	void DecideTimeControls ();
	bool SetTimeControlsForDgr ( CString &ropDgrUrno );
	void UpdateAllRudsStartEnd ();
	bool UpdateRudStartEnd ( RecordSet &ropRudRecord );
	void CloseGantt ();
	void MakeViewer();
	//void MakeGantt();
	bool OpenGantt ( CString &ropUdgr );
	void CalcTimeScaleRect( LPRECT lpRect );
	void CalcGanttRect( LPRECT lpRect );
	bool IniControlPositions ();
	bool SelectRudBar ( CString &ropRudUrno );
	bool CheckChanges();
	void SetBdpsState();

	void OnDatetimechangeDube();
	void OnDatetimechangeDuen();

	CString CheckValuesDifferent(CCSPtrArray <RecordSet> opRecordList,int ipValueIdx);
	bool IsSameTimeValue(CCSPtrArray <RecordSet> &ropRecordList,
						 int ipValueIdx, CString &ropValue );

	int GetFloatingState(CCSPtrArray <RecordSet> &ropRecordList );
	void DisplaySplitValues ();
	void OnChangeMaxtime();
	void OnChangeMintime();
	int GetSelectedRuds ( CCSPtrArray <RecordSet> &opRudArr );


// attributes
public:
	CGridFenster *pomLeftGrid;
	CGridFenster *pomRightGrid;
	CGridFenster *pomConditionsGrid;
	CDutyGrid *pomDutyGrid;
	CGridFenster *pomTimesGrid;
		
	CCSDragDropCtrl m_DragDropTarget;

//	CTable *pomTable;
	
	CString omCurrRudUrno;
	CString omCurrDgrUrno;

	int	imSelectMode;
	int	imRessourceType;
	int imActiveRow;
	
	bool bmTimeControlAllow;
	bool bmCondControlAllow;
	
	BOOL bmIsFloating;
	
	bool bmSetDebe;
	bool bmSetDeen;

	CRect	omDutyOriginalRect;
	CPoint 	omFloatingPosRuty2, omFloatingPosRuty3;
	CPoint 	omEarliestPosRuty2;
	CCSEdit omOffsetEdit;
	CStatic omMinuteText;

	//{{AFX_DATA(FlightIndepRulesView)
	enum { IDD = INDEP_DLG_WINDOW };
	CButton	omDideChk;
	CCSEdit	omEditSplitMin;
	CCSEdit	omEditSplitMax;
	CComboBox	m_AlocCB;
	CEdit	m_Edt_Descript;
	CCSEdit	m_Dtc_Duen;
	CCSEdit	m_Dtc_Dube;
	CListBox m_List_Services;
	CButton	m_Chb_Acti;
	CCSEdit	m_Edt_RuleShortName;
	CCSEdit	m_Edt_RuleName;
	CString	m_RuleName;
	CString	m_RuleShortName;
	int		imRuty2;
	int		biDisplaySingleDuties;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlightIndepRulesView)
	public:
	virtual void OnInitialUpdate();
	virtual LONG OnDragOver(UINT wParam, LONG lParam);
	virtual LONG OnDrop(UINT wParam, LPARAM lParam);
	virtual void OnRButtonDown(UINT itemID, LONG lParam);
	virtual void OnLButtonDblClk(UINT itemID, LPARAM lParam);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	afx_msg void OnChangedRuty();
	afx_msg void OnGridCurrCellChanged(WPARAM wParam, LPARAM lParam);
	//{{AFX_MSG(FlightIndepRulesView)
	afx_msg void OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMenuDeleteBar();
	afx_msg void OnMenuCreateLink();
	afx_msg void OnMenuDeleteLink();
	afx_msg void OnRadioRuty2();
	afx_msg void OnRadioRuty3();
	afx_msg void OnRadioActivities();
	afx_msg void OnSelendokCobAlod();
	afx_msg void OnFloatingChk();
	afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnDideChk();
	afx_msg void OnFaddChk();
	//}}AFX_MSG
	afx_msg void OnGridButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridRButton(WPARAM wParam, LPARAM lParam);
	afx_msg void OnEditKillFocus(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridResorted(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};



/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLIGHTINDEPRULESVIEW_H__2B6A6851_35F7_11D3_A647_0000C007916B__INCLUDED_)
