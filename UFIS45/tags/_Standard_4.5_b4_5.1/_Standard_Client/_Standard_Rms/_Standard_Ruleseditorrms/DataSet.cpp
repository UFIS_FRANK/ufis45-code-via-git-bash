#include <stdafx.h>

#include <DataSet.h>
#include <CCSDDX.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <BasicData.h>
#include <ccslog.h>
#include <resource.h>
#include <regelwerk.h>
#include <mainfrm.h>
#include <Dialog_VorlagenEditor.h>
#include <WaitDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif

//-----------------------------------------------------------------------------------------------------------------
//					construction / destruction
//-----------------------------------------------------------------------------------------------------------------
DataSet::DataSet()
{
	CCS_TRY
		
	imOldDuration = 0;

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------

DataSet::~DataSet()
{
	/*CCS_TRY

	

	CCS_CATCH_ALL*/
}



//-----------------------------------------------------------------------------------------------------------------
//					data routines
//-----------------------------------------------------------------------------------------------------------------
void DataSet::CreateRpfEntry(CString opRudUrno, RecordSet *popSef)
{
	CCS_TRY

	RecordSet olRpfRecord(ogBCD.GetFieldCount("RPF_TMP"));

	// get an urno for this object (won't be overwritten by ogBCD on inserting the record)
	long llUrno = ogBCD.GetNextUrno();
	olRpfRecord.Values[igRpfUrnoIdx].Format("%d", llUrno);
	
	olRpfRecord.Values[igRpfUrudIdx] = opRudUrno;
	olRpfRecord.Values[igRpfUpfcIdx] = popSef->Values[igSefUpfcIdx];
	olRpfRecord.Values[igRpfFccoIdx] = popSef->Values[igSefFccoIdx];
	olRpfRecord.Values[igRpfFcnoIdx] = CString("1");
	olRpfRecord.Values[igRpfPrioIdx] = popSef->Values[igSefPrioIdx];
	olRpfRecord.Values[igRpfGtabIdx] = popSef->Values[igSefGtabIdx];	//  hag990826

	ogBCD.InsertRecord("RPF_TMP", olRpfRecord);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------

void DataSet::CreateRpqEntry(CString opRudUrno, CString opUdgr, RecordSet *popSeq )
{
	CCS_TRY

	RecordSet olRpqRecord(ogBCD.GetFieldCount("RPQ_TMP"));

	long llUrno = ogBCD.GetNextUrno();
	olRpqRecord.Values[igRpqUrnoIdx].Format("%d", llUrno);
	
	olRpqRecord.Values[igRpqUrudIdx] = opRudUrno;
	olRpqRecord.Values[igRpqUdgrIdx] = opUdgr;
	olRpqRecord.Values[igRpqUperIdx] = popSeq->Values[igSeqUperIdx];
	olRpqRecord.Values[igRpqQucoIdx] = popSeq->Values[igSeqQucoIdx];
	olRpqRecord.Values[igRpqQunoIdx] = popSeq->Values[igSeqQunoIdx];
	olRpqRecord.Values[igRpqPrioIdx] = popSeq->Values[igSeqPrioIdx];
	olRpqRecord.Values[igRpqGtabIdx] = popSeq->Values[igSeqGtabIdx];	//  hag990826

	ogBCD.InsertRecord("RPQ_TMP", olRpqRecord);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------
void DataSet::CreateRloEntry(CString opRudUrno, RecordSet *popSel)
{
	CCS_TRY

	RecordSet olRloRecord(ogBCD.GetFieldCount("RLO_TMP"));

	// get an urno for this object (won't be overwritten by ogBCD on inserting the record)
	long llUrno = ogBCD.GetNextUrno();
	olRloRecord.Values[igRloUrnoIdx].Format("%d", llUrno);
	
	olRloRecord.Values[igRloUrudIdx] = opRudUrno;
	olRloRecord.Values[igRloRlocIdx] = popSel->Values[igSelRlocIdx];
	olRloRecord.Values[igRloReftIdx] = popSel->Values[igSelReftIdx];
	olRloRecord.Values[igRloLonoIdx] = popSel->Values[igSelLonoIdx];
	olRloRecord.Values[igRloPrioIdx] = popSel->Values[igSelPrioIdx];
	olRloRecord.Values[igRloGtabIdx] = popSel->Values[igSelGtabIdx];	//  hag990826

	ogBCD.InsertRecord("RLO_TMP", olRloRecord);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------

// this method creates a new RUD recordset and fills it with default values
void DataSet::CreateRudFromSerList(bool bpIsSCRule, CStringArray &ropSerUrnos, 
								   CString opRueUrno, CString opTplUrno, CString opEvty, 
								   CString*popRudAloc/*=0*/, bool bpCreateGroup /*false*/,
								   bool bpSendBc /*=true*/ )

{
	CCS_TRY
	if (!checkIsNumber(opRueUrno))
	{
		oglogData = "get invalid rule urno when create rud from select list <" + opRueUrno +">";
		WriteInlogFile();
		AfxMessageBox(GetString(1408));
		return;
	}
	if(atol(opRueUrno) > 0)
	{
		CString olSerUrno, olUdgr, olEventType, olUrno;
		olEventType = opEvty;
		int ilSerCount = ropSerUrnos.GetSize();
		int j, k;

		// make group link ?
		long llUrno, llUlnk;
		if (bpCreateGroup)
				llUlnk = ogBCD.GetNextUrno();

		for (int i = 0; i < ilSerCount; i++)
		{
			olSerUrno = ropSerUrnos.GetAt(i);
			RecordSet rlSer(ogBCD.GetFieldCount("SER"));
			
			RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));

			if (ogBCD.GetRecord("SER", "URNO", olSerUrno, rlSer) == true)
			{
				CString olFfis;
				CString olSerType;

				olFfis = rlSer.Values[igSerFfisIdx];
				olSerType = rlSer.Values[igSerSeerIdx];

				// dropping independent service on flight-related rule
				if (olFfis == "1" && bpIsSCRule)	
				{
					if (AfxMessageBox(GetString(1550), MB_YESNO | MB_ICONQUESTION) == IDYES)
					{
						rlSer.Values[igSerFfisIdx] = CString("0");
						rlSer.Values[igSerSeerIdx] = olEventType;
					}
					else
					{
						continue;
					}
				}

				// dropping flight-related service on independent rule
				if(olFfis != "1" && !bpIsSCRule)
				{
					if (AfxMessageBox(GetString(1551), (MB_YESNO | MB_ICONQUESTION)) == IDYES)
					{
						rlSer.Values[igSerFfisIdx] = CString("1");
					}
					else
					{
						continue;
					}
				}

				// check if RUD.DRTY matches RUE.EVTY
				if ((olEventType == "1" && (olSerType == "2" || olSerType == "0")) || 
					(olEventType == "2" && (olSerType == "1"  || olSerType == "0")))
				{
					if (AfxMessageBox(GetString(1570), MB_YESNO) == IDYES)
					{
						rlSer.Values[igSerSeerIdx] = olEventType;
						if (olSerType == CString("0"))
						{
							// set duration on 10 min when turnaround
							rlSer.Values[igSerSdutIdx] = CString("10");
						}
					}
					else
					{
						continue;
					}
				}

				bool blStaff = false;
				bool blEquipment = false;
				bool blLoactions = false;
				
				CCSPtrArray <RecordSet> olQualiArr;
				CCSPtrArray <RecordSet> olFunctArr;
				CCSPtrArray <RecordSet> olEquipArr;
				CCSPtrArray <RecordSet> olLocatArr;
				CCSPtrArray <RecordSet> olRudArr;
				CMapStringToString olSef2RudUrno;

				int ilQualiCount = 0;
				int ilFunctCount = 0;
				int ilEquipCount = 0;
				int ilEqGrpCount = 0;
				int ilLocatCount = 0;
				
				// staff
				olSerType = rlSer.Values[igSerSetyIdx];
				if (olSerType.Left(1) == CString("1"))
				{
					ogBCD.GetRecords("SEF", "USRV", olSerUrno, &olFunctArr);
					ogBCD.GetRecords("SEQ", "USRV", olSerUrno, &olQualiArr);
					
					ilFunctCount = olFunctArr.GetSize();
					ilQualiCount = olQualiArr.GetSize();
				}
				
				// equipment
				if (olSerType.Mid(1, 1) == CString("1"))
				{
					ogBCD.GetRecords("SEE", "USRV", ropSerUrnos.GetAt(i), &olEquipArr);
					ilEquipCount = olEquipArr.GetSize();
				}

				// locations
				if (olSerType.Right(1) == CString("1"))
				{
					ogBCD.GetRecords("SEL", "USRV", ropSerUrnos.GetAt(i), &olLocatArr);
					ilLocatCount = olLocatArr.GetSize();
				}
				olUdgr.Format("%lu", ogBCD.GetNextUrno()); 
				if (igRudUdgrIdx < 0)
				{
					ogLog.Trace("DEBUG", "[DataSet::CreateRudFromSerList]  Invalid DGR Urno: GetFieldIndex(RUD_TMP,UDGR) <0");
				}
				else
				{
					olRecord.Values[igRudUdgrIdx] = olUdgr;
				}

				for (j = 0; j < ilFunctCount; j++)
				{
					// get number of ressources necessary
					RecordSet olRpfRecord(ogBCD.GetFieldCount("SEF"));
					olRpfRecord = olFunctArr[j];
					int ilAmount = atoi(olRpfRecord.Values[igSefFcnoIdx]);
					
					for (k = 0; k < ilAmount; k++)
					{
						// get an urno for this object (won't be overwritten by ogBCD on inserting the record)
						llUrno = ogBCD.GetNextUrno();
						olUrno.Format("%d", llUrno);
						olRecord.Values[igRudUrnoIdx] = olUrno;
						olRecord.Values[igRudUrueIdx] = opRueUrno;				// rule urno
						if ( igRudUtplIdx >= 0 )
						{
							olRecord.Values[igRudUtplIdx] = opTplUrno;				// template urno
						}
						olRecord.Values[igRudRetyIdx] = CString("100");			// ressource type set to function
						if (popRudAloc)
						{
							olRecord.Values[igRudAlocIdx] = *popRudAloc;
						}
						
						// set ULNK if "Create group link" in services list was checked
						if (bpCreateGroup)
						{
							olRecord.Values[igRudUlnkIdx].Format("%d", llUlnk);

						}

						//-- create RPF entry
						if (j < ilFunctCount)
							CreateRpfEntry(olUrno, &olFunctArr[j]);

						CreateRudRecord ( olRecord, rlSer, bpIsSCRule );
						if (ogBCD.InsertRecord("RUD_TMP", olRecord) == false)
						{
							ogLog.Trace("DEBUG", "[DataSet::CreateRudFromSerList]  InsertRecord into Logical table RUD_TMP failed");
						}
						else
						{
							AddItemToUpdList(olUrno, "RUD", ITEM_NEW);

							// save rud record to watch changes
							// ogChangeInfo.AddRud(&olRecord);
						}

						if ( k == 0 )
						{	//  Merken, welche SEF-Urno zu welcher RUD-Urno geh�rt
							//  wenn Amount>1 uninteressant, da dann keine Zuordnung in 
							//  SFQ-Tabelle vorhanden sein kann
							CString olSefUrno;
							olSefUrno = olRpfRecord.Values[igSefUrnoIdx];
							olSef2RudUrno.SetAt(olSefUrno, olUrno );
						}
					}

				}
				
				//  Zuordnung von Qualifikationen zu Funktionen aus Tabelle SFQ
				//  laden. Qualifikationen, die nicht zugeordnet sind bekommen 
				//	eine gemeinsame URNO als im Feld UDGR, die auch alle RUD's
				//  dieses Services im Feld UDGR haben
				CString		olSeqUrno, olSefUrno, olRudUrno;
				int			ilSfqUsef = ogBCD.GetFieldIndex ( "SFQ", "USEF" );
				RecordSet	olSfqRecord(ogBCD.GetFieldCount("SFQ"));
				bool		blAssigned;

				for ( j = 0; j < ilQualiCount; j++)
				{

					RecordSet olSeqRecord(ogBCD.GetFieldCount("SEQ"));
					olSeqRecord = olQualiArr[j];
					olSeqUrno = olSeqRecord.Values[igSeqUrnoIdx];
					
					blAssigned = ogBCD.GetRecord("SFQ", "USEQ", olSeqUrno, olSfqRecord );
					if ( blAssigned )
					{
						olSefUrno = olSfqRecord.Values[ilSfqUsef];
						if ( !olSef2RudUrno.Lookup(olSefUrno, olRudUrno ) )
							blAssigned = false;
					}
														
					int ilAmount = atoi(olSeqRecord.Values[igSeqQunoIdx]);
					

					for (k = 0; k < ilAmount; k++)
					{
						//  blAssigned: Qualifikation ist einer Funktion zugeordnet
						if ( blAssigned )
							CreateRpqEntry(olRudUrno, "", &olSeqRecord); 
						else 
							CreateRpqEntry("", olUdgr, &olSeqRecord); 

						if ( ogBCD.GetRecord( "RUD_TMP", "URNO", olRudUrno, olRecord ) && bpSendBc )
							ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRecord);	
					}	
				}
				
				
				// make RUD_TMP entries for locations		hag990819
				for (j = 0; j < ilLocatCount; j++ )
				{
					// get number of ressources necessary
					RecordSet olRolRecord(ogBCD.GetFieldCount("SEL"));
					olRolRecord = olLocatArr[j];
					int ilAmount = atoi(olRolRecord.Values[igSelLonoIdx]);
					
					for (k = 0; k < ilAmount; k++)
					{
						// get an urno for this object (won't be overwritten by ogBCD on inserting the record)
						long llUrno = ogBCD.GetNextUrno();
						CString olUrno;
						olUrno.Format("%d", llUrno);
						olRecord.Values[igRudUrnoIdx] = olUrno;
						olRecord.Values[igRudUrueIdx] = opRueUrno;				// rule urno
						if ( igRudUtplIdx >= 0 )
						{
							olRecord.Values[igRudUtplIdx] = opTplUrno;				// template urno
						}
						olRecord.Values[igRudRetyIdx] = CString("001");			// ressource type set to location
						if (popRudAloc && (igRudAlocIdx>=0))
						{
							olRecord.Values[igRudAlocIdx] = *popRudAloc;
						} 
						
						//-- create RLO entry
						if (j < ilLocatCount)
						{
							CreateRloEntry(olUrno, &olLocatArr[j]);
						}
						
						CreateRudRecord ( olRecord, rlSer, bpIsSCRule );
						if (ogBCD.InsertRecord("RUD_TMP", olRecord) == false)
						{
							ogLog.Trace("DEBUG", "[DataSet::CreateRudFromSerList]  InsertRecord into Logical table RUD_TMP failed");
						}
						else
						{
							AddItemToUpdList(olUrno, "RUD", ITEM_NEW);
						}
					}
				}

				// make RUD_TMP entries for equipment		hag020124
				for (j = 0; j < ilEquipCount; j++ )
				{	
					// get number of ressources necessary
					RecordSet olSeeRecord(ogBCD.GetFieldCount("SEE"));
					olSeeRecord = olEquipArr[j];
					int ilAmount = atoi(olSeeRecord.Values[igSeeEqnoIdx]);
					
					for (k = 0; k < ilAmount; k++)
					{
						// get an urno for this object (won't be overwritten by ogBCD on inserting the record)
						long llUrno = ogBCD.GetNextUrno();
						CString olUrno;
						olUrno.Format("%d", llUrno);
						olRecord.Values[igRudUrnoIdx] = olUrno;
						olRecord.Values[igRudUrueIdx] = opRueUrno;				// rule urno
						if ( igRudUtplIdx >= 0 )
						{
							olRecord.Values[igRudUtplIdx] = opTplUrno;				// template urno
						}
						olRecord.Values[igRudRetyIdx] = CString("010");			// ressource type set to equipment
						if (popRudAloc && (igRudAlocIdx>=0))
						{
							olRecord.Values[igRudAlocIdx] = *popRudAloc;
						} 
						
						//-- create REQ entry
						if (j < ilEquipCount)
						{
							CreateEquEntry(olUrno, &olSeeRecord );
						}
						
						CreateRudRecord ( olRecord, rlSer, bpIsSCRule );
						if (ogBCD.InsertRecord("RUD_TMP", olRecord) == false)
						{
							ogLog.Trace("DEBUG", "[DataSet::CreateRudFromSerList]  InsertRecord into Logical table RUD_TMP failed");
						}
						else
						{
							AddItemToUpdList(olUrno, "RUD", ITEM_NEW);
						}
					}
				}

				ogBCD.GetRecords("RUD_TMP", "UDGR", olUdgr, &olRudArr);
				for (j=0; bpSendBc && (j<olRudArr.GetSize()); j++ )
				{
					olRecord = olRudArr[j];
					//ogChangeInfo.AddRuds(&olRudArr);
					ogDdx.DataChanged((void *)this,RUD_TMP_NEW,(void *)&olRecord);
				}
				olFunctArr.DeleteAll();
				olQualiArr.DeleteAll();
				olEquipArr.DeleteAll();
				olLocatArr.DeleteAll();
				olRudArr.DeleteAll();
			}
		}
	}


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------

int DataSet::FillLogicRudObject(CString opUrue)
{
	CCS_TRY


	CCSPtrArray <RecordSet> olTmpRecs;	
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));

	bool blErr = ResetLogicTable ( "RUD_TMP" );

	ogBCD.GetRecords("RUD", "URUE", opUrue , &olTmpRecs);
	
	// save records to watch changes
	//ogChangeInfo.AddRuds(&olTmpRecs);

	for (int i = 0; i < olTmpRecs.GetSize(); i++)
	{
		olRudRecord = olTmpRecs[i];
		ogBCD.InsertRecord("RUD_TMP", olRudRecord);
	}
	                                    
	olTmpRecs.DeleteAll();

	CCS_CATCH_ALL


	return ogBCD.GetDataCount("RUD_TMP");	
}
//-----------------------------------------------------------------------------------------------------------------


// This function only works when there is a RUD_TMP object filled with data
// So make sure to call FillLogicRudObject first
int DataSet::FillLogicRpfObject()
{
	CCS_TRY
	
	
	CString olRud;
	CCSPtrArray <RecordSet> olFuncArr;
	RecordSet olRpfRecord(ogBCD.GetFieldCount("RPF_TMP"));

	bool blErr = ResetLogicTable ("RPF_TMP");

	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");

	for (int i = 0; i < ilRudCount; i++)
	{
		olRud = ogBCD.GetField("RUD_TMP", i, "URNO");
		ogBCD.GetRecords("RPF", "URUD", olRud, &olFuncArr);

		int ilSize = olFuncArr.GetSize();
		for (int j = 0; j < ilSize; j++)
		{
			olRpfRecord = olFuncArr[j];
			ogBCD.InsertRecord("RPF_TMP", olRpfRecord);
		}

		olFuncArr.DeleteAll();
	}
	
	

	CCS_CATCH_ALL

	return ogBCD.GetDataCount("RPF_TMP");
}
//-----------------------------------------------------------------------------------------------------------------

int DataSet::FillLogicRpqObject()
{
	CCS_TRY


	CString					olRud, olUdgr;
	CCSPtrArray<RecordSet>	olQualArr;
	CStringList				olUdgrList;
	POSITION				pos;
	RecordSet olRpqRecord(ogBCD.GetFieldCount("RPQ_TMP"));

	int		ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	int		ilSize ;
	bool	blErr = ResetLogicTable ( "RPQ_TMP" );
	
	//  Lade alle zugeordneten (Einzel-)Qualifikationen
	for (int i = 0; i < ilRudCount; i++)
	{
		olRud = ogBCD.GetField("RUD_TMP", i, "URNO");
		olUdgr = ogBCD.GetField("RUD_TMP", i, "UDGR");
		if ( !olUdgrList.Find ( olUdgr ) )
			olUdgrList.AddTail ( olUdgr );
		ogBCD.GetRecords("RPQ", "URUD", olRud, &olQualArr);
		
		ilSize = olQualArr.GetSize();
		for (int j = 0; j < ilSize; j++)
		{
			olRpqRecord = olQualArr[j];
			ogBCD.InsertRecord("RPQ_TMP", olRpqRecord);
		}
	}
	
	//  Lade alle nicht zugeordneten (Gruppen-)Qualifikationen
	pos = olUdgrList.GetHeadPosition ();
	while ( pos )
	{
		olUdgr = olUdgrList.GetNext ( pos );
		if ( !olUdgr.IsEmpty() )
		{
			ogBCD.GetRecords("RPQ", "UDGR", olUdgr, &olQualArr);
			
			ilSize = olQualArr.GetSize();
			for (int j = 0; j < ilSize; j++)
			{
				olRpqRecord = olQualArr[j];
				ogBCD.InsertRecord("RPQ_TMP", olRpqRecord);
			}
		}
	}

	olQualArr.DeleteAll();

	CCS_CATCH_ALL

	return ogBCD.GetDataCount("RPQ_TMP");
}
//-----------------------------------------------------------------------------------------------------------------
int DataSet::FillLogicRloObject()
{
	CCS_TRY
	
	
	CString olRud;
	CCSPtrArray <RecordSet> olLocArr;
	RecordSet olRloRecord(ogBCD.GetFieldCount("RLO_TMP"));

	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	bool blErr = ResetLogicTable ( "RLO_TMP" );

	for (int i = 0; i < ilRudCount; i++)
	{
		olRud = ogBCD.GetField("RUD_TMP", i, "URNO");
		ogBCD.GetRecords("RLO", "URUD", olRud, &olLocArr);

		int ilSize = olLocArr.GetSize();
		for (int j = 0; j < ilSize; j++)
		{
			olRloRecord = olLocArr[j];
			ogBCD.InsertRecord("RLO_TMP", olRloRecord);
		}

		olLocArr.DeleteAll();
	}

	CCS_CATCH_ALL

	return ogBCD.GetDataCount("RLO_TMP");
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::ChangeRudTime(CString opRudUrno, int ipNewStart, int ipNewEnd, int ipType, int ipOnBlock, int ipOffBlock)
{
	//------------------------------------------
	// Don't ask why the resizing part is as it is. 
	// I put it together by trial and error, so if you're not sure what you're doing,
	// you'd better leave it alone...
	//------------------------------------------
	//
	//	ipType - HTCAPTION, HTLEFT, HTRIGHT, tells you what's happening (moving / resizing)
	//
	//------------------------------------------


	// get record
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	bool blErr = ogBCD.GetRecord("RUD_TMP", "URNO", opRudUrno, olRecord);  
	
	int ilDuration = 0;

	if (blErr != false)
	{
		ilDuration = (ipNewEnd - ipNewStart);

		CString olBarType;
		olBarType = olRecord.Values[igRudDrtyIdx];

	//--- MOVING
		if(ipType == HTCAPTION)	
		{
			int ilNewStartInterval = 0;
			int ilNewEndInterval = 0;
			
			// new start interval
			if (olBarType == "0" || olBarType == "1")	// turnaround or onblock
			{
				ilNewStartInterval = ipNewStart - ipOnBlock;
			}
			
			// new end interval
			if (olBarType =="0" || olBarType == "2")	// turnaround or offblock
			{
				ilNewEndInterval = ipNewEnd - ipOffBlock;			
			}
			
			olRecord.Values[igRudTsdbIdx] = ilNewStartInterval;
			olRecord.Values[igRudTsdeIdx] = ilNewEndInterval;

	//--- RESIZING
		}
		else
		{
			int ilSetup = atoi(olRecord.Values[igRudSutiIdx]);
			int ilSetdown = atoi(olRecord.Values[igRudSdtiIdx]);
			int ilWayThere = atoi(olRecord.Values[igRudTtgtIdx]);
			int ilWayBack = atoi(olRecord.Values[igRudTtgfIdx]);

			if (ipType == HTLEFT)	//	<== LEFT 
			{
				// outbound
				if (olBarType == "2")
				{
					ilDuration -= (ilSetup + ilSetdown + ilWayThere + ilWayBack);
					olRecord.Values[igRudDeduIdx].Format("%d", ilDuration);
				}
			}
			else if (ipType == HTRIGHT) 	// RIGHT ==>
			{
				// inbound
				if (olBarType == "1")	
				{
					ilDuration -= (ilSetup + ilSetdown + ilWayThere + ilWayBack);
					olRecord.Values[igRudDeduIdx].Format("%d", ilDuration);
				}
			}
		}

		// write data back to global data object
		ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
		
		// send message that causes gantt to be redrawn
		ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRecord);
	}
	return true;
}
//-----------------------------------------------------------------------------------------------------------------

void DataSet::CreateRudRecord (RecordSet &ropRudRecord, RecordSet &ropSerRecord, bool bpIsSCRule)
{
	ropRudRecord.Values[igRudUghsIdx] = ropSerRecord.Values[igSerUrnoIdx];		// urno of service
	//--- time related stuff
	//ropRudRecord.Values[igRudMaxdIdx].Format("%d", atoi(ropSerRecord.Values[igSerSdutIdx]) * 60);     // maximum duration of demand [min]
	//ropRudRecord.Values[igRudMindIdx].Format("%d", atoi(ropSerRecord.Values[igSerSdutIdx]) * 60);     // minimum duration of demand [min]
	ropRudRecord.Values[igRudMaxdIdx] = "";
	ropRudRecord.Values[igRudMindIdx] = "";

	ropRudRecord.Values[igRudEadbIdx] = CString(" ");   // earliest start time for demand
	ropRudRecord.Values[igRudLadeIdx] = CString(" ");	// latest end time for demand
	ropRudRecord.Values[igRudTsdbIdx] = CString("0");	// space between Onblock and left end of bar
	ropRudRecord.Values[igRudTsdeIdx] = CString("0");	// space between Onblock and right end of bar
	ropRudRecord.Values[igRudTtgtIdx].Format("%d", atoi(ropSerRecord.Values[igSerSwttIdx]) * 60);	// time to get there
	ropRudRecord.Values[igRudTtgfIdx].Format("%d", atoi(ropSerRecord.Values[igSerSwtfIdx]) * 60);   // time to get back
	
	// fill disp with default value t omake sure that lines will be sorted to the end.
	ropRudRecord.Values[igRudDispIdx] = CString("998");
	CString olSuti = "";
	CString olSdti = "";

	olSuti.Format("%d", atoi(ropSerRecord.Values[igSerSsutIdx]) * 60);
	olSdti.Format("%d", atoi(ropSerRecord.Values[igSerSsdtIdx]) * 60);
	ropRudRecord.Values[igRudSutiIdx] = olSuti;  // time interval at start of demand [sec]
	ropRudRecord.Values[igRudSdtiIdx] = olSdti;	// time interval at end of demand [sec]
	ropRudRecord.Values[igRudDbflIdx] = "0";	// flag: demand start time floating
	ropRudRecord.Values[igRudDeflIdx] = "0"; 	// flag: demand end time floating
	
	int ttt = atoi(ropSerRecord.Values[igSerSsutIdx]);
	ttt = atoi(ropSerRecord.Values[igSerSsdtIdx]);

	// single / collective rules only
	if (bpIsSCRule == true)
	{
		ropRudRecord.Values[igRudDebeIdx] = " ";    // demand start time
		ropRudRecord.Values[igRudDeenIdx] = " ";	// demand end time
		ropRudRecord.Values[igRudDbarIdx] = "R";	// flag: demand start time abs/rel
		ropRudRecord.Values[igRudDearIdx] = "R"; 	// flag: demand end time abs/rel
		
		// MNE: Ich bin mir nicht ganz sicher, warum diese Zeiten nur f�r 
		//      flugbezogene Regeln gelten sollten. M.E. gibt es daf�r keinen Grund, 
		//      daher habe ich die Vor- und Nachbereitungszeit nach au�erhalb der 
		//      Klammer verschoben. 
		// ropRudRecord.Values[igRudSutiIdx].Format("%d", atoi(ropSerRecord.Values[igSerSsutIdx]) * 60);  // time interval at start of demand [sec]
		// ropRudRecord.Values[igRudSdtiIdx].Format("%d", atoi(ropSerRecord.Values[igSerSsdtIdx]) * 60);	// time interval at end of demand [sec]
		if(ropSerRecord.Values[igSerSeerIdx] != CString("0"))	
		{
			ropRudRecord.Values[igRudDeduIdx].Format("%d", atoi(ropSerRecord.Values[igSerSdutIdx]) * 60);	// demand duration [sec]
			
			// all times refer to OnBlock
			if (ropSerRecord.Values[igSerSeerIdx] == CString("1"))	
			{
				// ropRudRecord.Values[igRudRtdbIdx] = "STOA";	// reference time for start of demand
				// ropRudRecord.Values[igRudRtdeIdx] = "STOA";	// reference time for end of demand
				ropRudRecord.Values[igRudRtdbIdx] = ogInboundReferences[0].Field;
				ropRudRecord.Values[igRudRtdeIdx] = ogInboundReferences[0].Field;
				ropRudRecord.Values[igRudDrtyIdx] = "1";    // duty type = inbound
			}
			// all times refer to OffBlock
			if (ropSerRecord.Values[igSerSeerIdx] == CString("2"))
			{
				// ropRudRecord.Values[igRudRtdbIdx] = "STOD";	// reference time for start of demand
				// ropRudRecord.Values[igRudRtdeIdx] = "STOD";	// reference time for end of demand
				ropRudRecord.Values[igRudRtdbIdx] = ogOutboundReferences[0].Field;
				ropRudRecord.Values[igRudRtdeIdx] = ogOutboundReferences[0].Field;
				ropRudRecord.Values[igRudDrtyIdx] = "2";    // duty type = outbound
			}
		}
		else
		{
			// times refer to turnaround
			// ropRudRecord.Values[igRudRtdbIdx] = "STOA";	// reference time for start of demand (best inbound time)
			// ropRudRecord.Values[igRudRtdeIdx] = "STOD";	// reference time for end of demand	(best outbound time)
			ropRudRecord.Values[igRudRtdbIdx] = ogInboundReferences[0].Field;	// reference time for start of demand (best inbound time)
			ropRudRecord.Values[igRudRtdeIdx] = ogOutboundReferences[0].Field;	// reference time for end of demand	(best outbound time)
			ropRudRecord.Values[igRudDeduIdx] = "0";	// duration
			ropRudRecord.Values[igRudDrtyIdx] = "0";    // duty type = turnaround
		}
	}
	// flight independent rules only
	else
	{
		CTime	olTime;
		CString olTimeDBString, olFdut;
		int		ilSdut=10;


		olTime = CTime::GetCurrentTime();
		olTimeDBString = CTimeToDBString(olTime, olTime);
		ropRudRecord.Values[igRudDebeIdx] = olTimeDBString;		// demand start time

		olFdut = ropSerRecord.Values[igSerFdutIdx];
		if ( olFdut == "1" )
			ilSdut = atoi(ropSerRecord.Values[igSerSdutIdx]);	// demand duration in min

		CTimeSpan olDutyTime( 0, 0, ilSdut, 0 );
		CTime olEndTime(olTime);
		
		olEndTime += olDutyTime;
		olTimeDBString = CTimeToDBString(olEndTime, olEndTime);
		ropRudRecord.Values[igRudDeenIdx] = olTimeDBString;		// demand end time
		
		olFdut.Format("%d", (olEndTime - olTime).GetTotalSeconds());	// duration
		ropRudRecord.Values[igRudDeduIdx].Format("%d", ilSdut*60 );
		
		ropRudRecord.Values[igRudDbarIdx] = "A";				// flag: demand start time abs/rel
		ropRudRecord.Values[igRudDearIdx] = "A"; 				// flag: demand end time abs/rel
		ropRudRecord.Values[igRudRtdbIdx] = " ";	// reference time for start of demand (best inbound time)
		ropRudRecord.Values[igRudRtdeIdx] = " ";	// reference time for end of demand	(best outbound time)
/*
		ropRudRecord.Values[igRudSutiIdx] = "0";	// time interval at start of demand
		ropRudRecord.Values[igRudSutiIdx] = "0";	// time interval at end of demand
		*/
	}
	
	//-- urnos of related database entries
	ropRudRecord.Values[igRudUcruIdx] = " ";		// urno of collective rule
	ropRudRecord.Values[igRudUdemIdx] = " ";		// urno of full demand
	ropRudRecord.Values[igRudUghcIdx] = " ";		// urno of contract (GHC)
	ropRudRecord.Values[igRudUndeIdx] = " ";		// urno of resulting demand 
	ropRudRecord.Values[igRudUpdeIdx] = " ";		// urno of preceding demand
	ropRudRecord.Values[igRudUproIdx] = " ";		// urno of profile

	//-- dividing demands
	ropRudRecord.Values[igRudDideIdx] = "0";		// flag: divideable
	ropRudRecord.Values[igRudDindIdx] = "1";		// divisor
	
	//-- connected demands
	ropRudRecord.Values[igRudRedeIdx] = "0#0";		// 1st value: incoming connection, separator: #, 2nd value: outgoing connection 
	ropRudRecord.Values[igRudRendIdx] = " ";		// type of connection to resulting demand 
	ropRudRecord.Values[igRudRepdIdx] = " ";		// type of connection to preceding demand 
	ropRudRecord.Values[igRudTsndIdx] = " ";		// time difference to resulting demand
	ropRudRecord.Values[igRudTspdIdx] = " ";		// time difference to preceding demand
	
	//-- some flags
	ropRudRecord.Values[igRudFaddIdx] = '1';		// flag: activated/deactivated (1: active)
	ropRudRecord.Values[igRudFcndIdx] = '0';		// flag: contract (0: non-contractual)
	ropRudRecord.Values[igRudFfpdIdx] = '0';		// flag: full/partial demand (0: full demand)
	ropRudRecord.Values[igRudFncdIdx] = '0';		// flag: nominated/calculated demand (0: nominated)
	ropRudRecord.Values[igRudFondIdx] = '0';		// flag: operative / not operative	(0: operative)
	ropRudRecord.Values[igRudFsadIdx] = '0';		// flag: standard / extra service (0: standard)

	//-- different stuff 
	ropRudRecord.Values[igRudCoduIdx] = "100";	// necessary coverage [Percent]
	ropRudRecord.Values[igRudStdeIdx] = " ";		// status
	ropRudRecord.Values[igRudDecoIdx] = " ";		// condition (expression editor)
	ropRudRecord.Values[igRudLodeIdx] = " ";		// duty location
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::ResetLogicTable (CString opTableName)
{
	CString olUrno;
	int ilCount = ogBCD.GetDataCount(opTableName);
	for (int ilIdx = ilCount - 1; ilIdx >= 0; ilIdx--)
	{
		olUrno = ogBCD.GetField(opTableName, ilIdx, "URNO");
		bool blErr = ogBCD.DeleteRecord(opTableName, "URNO", olUrno);
	}
	ilCount = ogBCD.GetDataCount(opTableName);
	return (ilCount==0);
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::AddOrChangeRessources(CString opResType, CString opUrno, CString opRudUrno, 
									CString opUdgr, CString opSrvRefUrno, CString opSrvRefCode, 
									bool bpChange, bool bpGroup, int ipAmount/*=1*/)
{
	bool blErr = true;

	CCS_TRY
	

	// functions
	if (opResType == CString("RPF"))
	{
		RecordSet olRecord(ogBCD.GetFieldCount("RPF_TMP"));
		
		olRecord.Values[igRpfUrnoIdx] = opUrno;
		olRecord.Values[igRpfUrudIdx] = opRudUrno;
		olRecord.Values[igRpfUpfcIdx] = opSrvRefUrno;
		olRecord.Values[igRpfFcnoIdx] = ipAmount;
		
		olRecord.Values[igRpfGtabIdx] = ( bpGroup ) ? "SGR.GRPN" : "";		//  hag990827
		olRecord.Values[igRpfFccoIdx] = ( bpGroup ) ? "" : opSrvRefCode ;	//  hag990827
		
		if (bpChange == true)
		{
			blErr &= ogBCD.SetRecord("RPF_TMP", "URNO", opUrno, olRecord.Values);
		}
		else
		{
			blErr &= ogBCD.InsertRecord("RPF_TMP", olRecord);
		}

		// send message to applikation
		if (blErr == true && bpChange == true)
		{
			ogDdx.DataChanged((void *)this,RPF_TMP_CHANGE,(void *)&olRecord);
		}
		else if (blErr == true && bpChange == false)
		{
			ogDdx.DataChanged((void *)this,RPF_TMP_NEW,(void *)&olRecord);
		}
	}
	
	// qualifications
	if (opResType == CString("RPQ"))
	{
		RecordSet olRecord(ogBCD.GetFieldCount("RPQ_TMP"));
		
		olRecord.Values[igRpqUrnoIdx] = opUrno;
		olRecord.Values[igRpqUrudIdx] = opRudUrno;
		olRecord.Values[igRpqUperIdx] = opSrvRefUrno;
		olRecord.Values[igRpqQunoIdx] = ipAmount;
		olRecord.Values[igRpqUdgrIdx] = opUdgr;		//  hag990924
		
		olRecord.Values[igRpqGtabIdx] = ( bpGroup ) ? "SGR.GRPN" : "";		//  hag990827
		olRecord.Values[igRpqQucoIdx] = ( bpGroup ) ? "" : opSrvRefCode ;	//  hag990827

		if (bpChange == true)
		{
			blErr &= ogBCD.SetRecord("RPQ_TMP", "URNO", opUrno, olRecord.Values);
		}
		else
		{
			blErr &= ogBCD.InsertRecord("RPQ_TMP", olRecord);
		}

		// send message to application
		if (blErr == true && bpChange == true)
		{
			ogDdx.DataChanged((void *)this,RPQ_TMP_CHANGE,(void *)&olRecord);
		}	
		else if (blErr == true && bpChange == false)
		{
			ogDdx.DataChanged((void *)this,RPQ_TMP_NEW,(void *)&olRecord);
		}
	}
	
	// locations
	if (opResType == CString("RLO"))
	{
		RecordSet olRecord(ogBCD.GetFieldCount("RLO_TMP"));

		olRecord.Values[igRloUrnoIdx] = opUrno;
		olRecord.Values[igRloUrudIdx] = opRudUrno;
		olRecord.Values[igRloReftIdx] = opSrvRefUrno;
		olRecord.Values[igRloRlocIdx] = opSrvRefCode;
		olRecord.Values[igRloLonoIdx] = ipAmount;

		olRecord.Values[igRloGtabIdx] = ( bpGroup ) ? "SGR.GRPN" : "";		//  hag990827

		if (bpChange == true)
		{
			blErr &= ogBCD.SetRecord("RLO_TMP", "URNO", opUrno, olRecord.Values);
		}
		else
		{
			blErr &= ogBCD.InsertRecord("RLO_TMP", olRecord);
		}
		
		// send message to applikation
		if (blErr == true && bpChange == true)
		{
			ogDdx.DataChanged((void *)this,RLO_TMP_CHANGE,(void *)&olRecord);
		}
		else if (blErr == true && bpChange == false)
		{
			ogDdx.DataChanged((void *)this,RLO_TMP_NEW,(void *)&olRecord);
		}
	}

	// equipment
	if (opResType == CString("REQ"))
	{
		CString olEtyp;
		RecordSet olRecord(ogBCD.GetFieldCount("REQ_TMP"));
		
		olRecord.Values[igReqUrnoIdx] = opUrno;
		olRecord.Values[igReqUrudIdx] = opRudUrno;
		olRecord.Values[igReqUequIdx] = opSrvRefUrno;
		olRecord.Values[igReqEqnoIdx] = ipAmount;

		if ( bpGroup )
			olEtyp = ogBCD.GetField ( "SGR", "URNO", opSrvRefUrno, "STYP" ) ;
		else
			olEtyp = ogBCD.GetField ( "EQU", "URNO", opSrvRefUrno, "GKEY" ) ;
		olRecord.Values[igReqEtypIdx] = olEtyp;
		
		olRecord.Values[igReqGtabIdx] = ( bpGroup ) ? "SGR.GRPN" : "";		//  hag990827
		olRecord.Values[igReqEqcoIdx] = ( bpGroup ) ? "" : opSrvRefCode ;	//  hag990827
		
		if (bpChange == true)
		{
			blErr &= ogBCD.SetRecord("REQ_TMP", "URNO", opUrno, olRecord.Values);
		}
		else
		{
			blErr &= ogBCD.InsertRecord("REQ_TMP", olRecord);
		}

		// send message to applikation
		if (blErr == true && bpChange == true)
		{
			ogDdx.DataChanged((void *)this,REQ_TMP_CHANGE,(void *)&olRecord);
		}
		else if (blErr == true && bpChange == false)
		{
			ogDdx.DataChanged((void *)this,REQ_TMP_NEW,(void *)&olRecord);
		}
	}

	CCS_CATCH_ALL

	return blErr;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::DeleteRessource (CString opResType, CString opUrno ) 
{	
	CString		olTable = opResType+"_TMP";
	int			ilDDXType = 0;
	RecordSet	olRecord;
	bool		blOk;
	
	blOk = ogBCD.GetRecord ( olTable, "URNO", opUrno, olRecord );
	if ( ogBCD.DeleteRecord( olTable, "URNO", opUrno ) )
	{
		if ( blOk )
		{
			if ( opResType == "RPF" )
				ilDDXType = RPF_TMP_DELETE;
			else if ( opResType == "RPQ" )
				ilDDXType = RPQ_TMP_DELETE;
			else if ( opResType == "RLO" )
				ilDDXType = RLO_TMP_DELETE;
			if ( ilDDXType )
				ogDdx.DataChanged((void *)this,ilDDXType,(void *)&olRecord);
		}
		return true;
	}
	return false;
}
//-----------------------------------------------------------------------------------------------------------------


// This function deletes entries in RPF_TMP, RPQ_TMP and RLO_TMP
// It should be called after deleting a RUD entry 
bool DataSet::DeleteRessources(RecordSet &ropRudRecord)
{
	bool blErr = true;

	CCS_TRY

	
	int ilCount = 0;
	CString olUrno, olRudUrno, olRudUdgr ;
	CCSPtrArray <RecordSet> olRpfArr;
	CCSPtrArray <RecordSet> olRpqArr;
	CCSPtrArray <RecordSet> olRloArr;
	CCSPtrArray <RecordSet> olReqArr;

	olRudUrno = ropRudRecord.Values[igRudUrnoIdx];
	olRudUdgr = ropRudRecord.Values[igRudUdgrIdx];	

	// functions
	ogBCD.GetRecords("RPF_TMP", "URUD", olRudUrno, &olRpfArr);
	ilCount = olRpfArr.GetSize();
	for (int i = ilCount - 1; i >= 0; i--)
	{
		olUrno = olRpfArr[i].Values[igRpfUrnoIdx];
		blErr &= ogBCD.DeleteRecord("RPF_TMP", "URNO", olUrno);
	}
	
	// qualifications
	ogBCD.GetRecords("RPQ_TMP", "URUD", olRudUrno, &olRpqArr);
	ilCount = olRpqArr.GetSize();
	for (i = ilCount - 1; i >= 0; i--)
	{
		olUrno = olRpqArr[i].Values[igRpqUrnoIdx];
		blErr &= ogBCD.DeleteRecord("RPQ_TMP", "URNO", olUrno);
	}
	//  Wenn letzter RUD_TMP-Record einer DGR gel�scht worden ist, auch
	//  alle RPQ_TMP-Records, die sich auf diese DGR beziehen l�schen
	if (CountOfStaffRudsForDGR(olRudUdgr) == 0)
	{	//  das war der letzte RUD_TMP-Satz aus dieser DGR		
		ogBCD.GetRecords("RPQ_TMP", "UDGR", olRudUdgr, &olRpqArr);
		ilCount = olRpqArr.GetSize();
		for (i = ilCount - 1; i >= 0; i--)
		{
			olUrno = olRpqArr[i].Values[igRpqUrnoIdx];
			blErr &= ogBCD.DeleteRecord("RPQ_TMP", "URNO", olUrno);
		}
	}

	// locations
	ogBCD.GetRecords("RLO_TMP", "URUD", olRudUrno, &olRloArr);
	ilCount = olRloArr.GetSize();
	for (i = ilCount - 1; i >= 0; i--)
	{
		olUrno = olRloArr[i].Values[igRloUrnoIdx];
		blErr &= ogBCD.DeleteRecord("RLO_TMP", "URNO", olUrno);
	}

	// equipment
	ogBCD.GetRecords("REQ_TMP", "URUD", olRudUrno, &olReqArr);
	ilCount = olReqArr.GetSize();
	for (i = ilCount - 1; i >= 0; i--)
	{
		olUrno = olReqArr[i].Values[igReqUrnoIdx];
		blErr &= ogBCD.DeleteRecord("REQ_TMP", "URNO", olUrno);
	}

	olRpfArr.DeleteAll();
	olRpqArr.DeleteAll();
	olRloArr.DeleteAll();
	olReqArr.DeleteAll();

	CCS_CATCH_ALL

	return blErr;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::OnCopyRue (CString &ropOldRueUrno, CString &ropNewRueUrno)
{
	bool blRet = true;

	CCS_TRY

	CString olNewUrno, olOldUrno;
	CCSPtrArray <RecordSet> olRudList;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD_TMP"));
	CMapStringToString  olRudOldNewMap;
	CString olOldUdgr, olNewUdgr;
	CMapStringToString  olUdgrOldNewMap;

	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	
	ogBCD.GetRecords( "RUD_TMP", "URUE", ropOldRueUrno, &olRudList );
	ilRudCount = olRudList.GetSize( );
	
	//  Wenn im Original die Urud oder Udgr leer, 
	//  wird sie auch in der Kopie leer gesetzt
	olRudOldNewMap.SetAt ( "", "" );
	olUdgrOldNewMap.SetAt ( "", "" );

	for (int i = 0; i < ilRudCount; i++)
	{
		olRudRecord = olRudList[i];
		
		if (ropOldRueUrno != olRudRecord.Values[igRudUrueIdx])
		{
			TRACE ( "Found record in RUD_TMP with URUE != omCurrRueUrno\n" );
			blRet = false;
			continue;
		}
		olRudRecord.Values[igRudUrueIdx] = ropNewRueUrno;	// neue Rule-Event Urno eintragen 
		olNewUrno.Format("%d", ogBCD.GetNextUrno());
		olOldUrno = olRudRecord.Values[igRudUrnoIdx];
		//  Urno der Demandgroup neu setzen
		olOldUdgr = olRudRecord.Values[igRudUdgrIdx];
		if ( !olUdgrOldNewMap.Lookup ( olOldUdgr, olNewUdgr ) )
		{
			olNewUdgr.Format("%d", ogBCD.GetNextUrno());
			olUdgrOldNewMap.SetAt ( olOldUdgr, olNewUdgr );
		}

		//  original aus RUD_TMP l�schen
		blRet  &= ogBCD.DeleteRecord( "RUD_TMP", "URNO", olOldUrno );
		olRudRecord.Values[igRudUrnoIdx] = olNewUrno;
		olRudRecord.Values[igRudUdgrIdx] = olNewUdgr;
		blRet  &= ogBCD.InsertRecord( "RUD_TMP", olRudRecord );
		
		olRudOldNewMap.SetAt ( olOldUrno, olNewUrno );
	}
	blRet &= CopyConnections(olRudOldNewMap, /*&olRudList*/ropNewRueUrno);
	blRet &= OnCopyRud ( "RPF_TMP", olRudOldNewMap );
	blRet &= OnCopyRud ( "RPQ_TMP", olRudOldNewMap, &olUdgrOldNewMap );
	blRet &= OnCopyRud ( "REQ_TMP", olRudOldNewMap );
	blRet &= OnCopyRud ( "RLO_TMP", olRudOldNewMap );
	
	olRudList.DeleteAll();

	CCS_CATCH_ALL
	return 	blRet ;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::OnCopyRud(CString opRuleResTable, CMapStringToString &ropOldNewRudUrnos, 
						CMapStringToString *popOldNewUdgrs/*=0*/)
{
	bool blRet = true;

	CCS_TRY

	RecordSet olRuleResRecord(ogBCD.GetFieldCount(opRuleResTable));
	CCSPtrArray<RecordSet> olResList;
	CString olOldRudUrno, olNewRudUrno, olNewUrno, olOldUrno ;
	CString olOldUdgr, olNewUdgr; 
	int		i ;
	int		ilUrudIndex = ogBCD.GetFieldIndex ( opRuleResTable, "URUD" );
	int		ilUrnoIndex = ogBCD.GetFieldIndex ( opRuleResTable, "URNO" );
	int		ilUdgrIndex = -1;
	bool	blToCopy;

	if ( popOldNewUdgrs )
		ilUdgrIndex = ogBCD.GetFieldIndex ( opRuleResTable, "UDGR" );
	
	int ilCount = ogBCD.GetDataCount( opRuleResTable );
	//  alle Datens�tze aus opRuleResTable in Array kopieren
	for ( i = 0; i < ilCount; i++)
	{
		if ( ogBCD.GetRecord(opRuleResTable, i, olRuleResRecord ) )
			olResList.New(olRuleResRecord);
	}

	ilCount = olResList.GetSize( );

	for ( i = 0; i < ilCount; i++)
	{
		// initialize values
		blToCopy = false;
		olNewRudUrno.Empty();
		olNewUdgr.Empty();

		olRuleResRecord = olResList[i];

		olOldRudUrno = olRuleResRecord.Values[ilUrudIndex] ;
		if ( !olOldRudUrno.IsEmpty () &&
			 ropOldNewRudUrnos.Lookup ( olOldRudUrno, olNewRudUrno ) )
		{
			blToCopy = true;
		}
		//  Wenn Map der Udgr's �bergeben und Feld "UDGR" in opRuleResTable
		//  vorhanden
		if ( ilUdgrIndex >= 0 )
		{
			olOldUdgr = olRuleResRecord.Values[ilUdgrIndex];
			if ( !olOldUdgr.IsEmpty() &&
				 popOldNewUdgrs->Lookup ( olOldUdgr, olNewUdgr ) )
			{
				blToCopy = true;
				olRuleResRecord.Values[ilUdgrIndex] = olNewUdgr;
			}
		}
		if ( !blToCopy )
			continue;

		olRuleResRecord.Values[ilUrudIndex] = olNewRudUrno;
		olNewUrno.Format("%d", ogBCD.GetNextUrno());
		olOldUrno = olRuleResRecord.Values[ilUrnoIndex];
		//  original aus opRuleResTable l�schen
		blRet &= ogBCD.DeleteRecord( opRuleResTable, "URNO", olOldUrno );

		olRuleResRecord.Values[ilUrnoIndex] = olNewUrno;
		blRet &= ogBCD.InsertRecord( opRuleResTable, olRuleResRecord );
		
	}
	olResList.DeleteAll ();
	CCS_CATCH_ALL
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::OnDeleteRue ( CString &ropRueUrno )
{
	bool blRet = true;

	CCS_TRY

	CString olUrno, olUdgr ;
	CCSPtrArray<RecordSet> olRudList;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
	CStringArray olRudUrnos;
	CStringList  olUdgrList;
	int ilRudCount;
	
	ogBCD.GetRecords( "RUD", "URUE", ropRueUrno, &olRudList );
	ilRudCount = olRudList.GetSize( );

	for (int i = 0; i < ilRudCount; i++)
	{
		olRudRecord = olRudList[i];
		
		if ( ropRueUrno != olRudRecord.Values[igRudUrueIdx] )
		{
			TRACE ( "Record in RUD gefunden, dessen URUE!= omCurrRueUrno\n" );
			blRet = false;
			continue;
		}
		olUrno = olRudRecord.Values[igRudUrnoIdx];
		olUdgr = olRudRecord.Values[igRudUdgrIdx];
		if ( !olUdgr.IsEmpty() && !olUdgrList.Find(olUdgr) )
			olUdgrList.AddTail (olUdgr);

		blRet  &= ogBCD.DeleteRecord( "RUD", "URNO", olUrno );
		olRudUrnos.Add ( olUrno );
	}
	if ( ilRudCount > 0 )
		blRet  &= ogBCD.Save( "RUD" );
	olRudList.DeleteAll();
	RemoveResources ( olRudUrnos, &olUdgrList );
	blRet &= DeleteByWhere ( "VAL", "UVAL", ropRueUrno );
	
	CCS_CATCH_ALL
	return 	blRet ;
}
//-----------------------------------------------------------------------------------------------------------------

void DataSet::OnDeleteRuds ( CString opRuleResTable, CStringArray  &ropRudUrnos )
{
	CCS_TRY


	int	i;
	int ilCount = ropRudUrnos.GetSize();
	
	//  f�r alle RUD-Urnos die Datens�tze l�schen, f�r die URUD gleich dieser URNO
	for ( i = 0; i < ilCount; i++)
		if ( !DeleteByWhere ( opRuleResTable, "URUD", ropRudUrnos[i], false ) )
			TRACE ( "Error deleting from table %s where URUD=<%s>\n", opRuleResTable, ropRudUrnos[i] );
	if ( ilCount > 0 )
		ogBCD.Save( opRuleResTable ) ;

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------

void DataSet::ResetLogicResourceTables ()
{
	ogDataSet.ResetLogicTable ("RPF_TMP");
	ogDataSet.ResetLogicTable ("RPQ_TMP");
	ogDataSet.ResetLogicTable ("RLO_TMP");
	ogDataSet.ResetLogicTable ("REQ_TMP");
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::DeleteOneRule ( CString &ropUrno )
{
	bool blOk;
	RecordSet olRecord(ogBCD.GetFieldCount("RUE"));
	blOk = ogBCD.GetRecord("RUE", "URNO", ropUrno, olRecord);
	if ( !blOk )
		return false;

	int ilFisu = atoi(olRecord.Values[igRueFisuIdx]);
	
	if (ilFisu == 1)
	{
		// archive used record
		olRecord.Values[igRueRustIdx] = "2";		//  hag990901
		blOk = ogBCD.SetRecord("RUE", "URNO", ropUrno, olRecord.Values, true );
	}
	else
	{
		// delete unused records
		blOk = OnDeleteRue (ropUrno);	//hag990902
		blOk &= ogBCD.DeleteRecord("RUE", "URNO", ropUrno, true );  //hag990817
	}
	return blOk;
}
//-----------------------------------------------------------------------------------------------------------------


//  L�schen aller Regeln, die zu einem Template geh�ren
//  wenn Regel verwendet wird, Regel archivieren
//	Return:	0:	Alle Regeln konnten gel�scht werden
//		Bit	0=1:	Fehler aufgetreten
//		Bit	1=1:	mindestens eine Regel wurde archiviert
int DataSet::OnDeleteTpl ( CString &ropTplUrno )
{
	CCSPtrArray<RecordSet>	olRuleToDelete;
	int						i, ilCount;
	int						ilRet=0;
	CString					olRueUrno;
	bool					blOk=true;
	int						ilRueFisu;

	//  alle Regeln l�schen, die zu einem Template geh�ren
	ogBCD.GetRecords( "RUE", "UTPL", ropTplUrno, &olRuleToDelete );
	ilCount = olRuleToDelete.GetSize();
	for ( i = ilCount - 1; i >= 0; i-- )
	{
		olRueUrno = olRuleToDelete[i].Values[igRueUrnoIdx];
		ilRueFisu = atoi(olRuleToDelete[i].Values[igRueFisuIdx]);
		if ( ilRueFisu == 1 )
			ilRet = 2;	//  diese Regel wird archiviert
		blOk &= DeleteOneRule ( olRueUrno );
	}
	if ( !blOk )
		ilRet |= 1;		//  Fehler aufgetreten
	olRuleToDelete.DeleteAll();

	return ilRet;
}
//-----------------------------------------------------------------------------------------------------------------

//	Bestimme Anzahl von RUD-Records, die zu einer Demandgroup geh�ren
int DataSet::CountOfStaffRudsForDGR ( CString &ropUdgr, bool bpLogicTables/*=true*/ )
{
	CCSPtrArray <RecordSet> olRudArr;
	CString		olRpfTable;
	CString		olRudTable;
	CString		olUrno, olRudUrno;
	int			ilRet = 0;
	RecordSet	olRecord;

	olRpfTable = bpLogicTables ? "RPF_TMP" : "RPF";
	olRudTable = bpLogicTables ? "RUD_TMP" : "RUD";

	ogBCD.GetRecords ( olRudTable, "UDGR", ropUdgr, &olRudArr );
	for ( int i=0; i<olRudArr.GetSize(); i++ )
	{
		olRudUrno = olRudArr[i].Values[igRudUrnoIdx];
		if ( ogBCD.GetRecord( olRpfTable, "URUD", olRudUrno, olRecord ) )
			ilRet ++;

	}
	olRudArr.DeleteAll();
	return ilRet;
}
//-----------------------------------------------------------------------------------------------------------------

//	Test, ob die Qualifikation ropSrvRefUrno bereits f�r den RUD_TMP-Satz mit
//	URNO = ropRPQUrud verwendet wird. 
//	Wenn gefundener Satz URNO = ropRPQUrno (gilt nicht, wir suchen nach doppelten)
bool DataSet::IsSingleQualiUsedForRud(CString &ropRPQUrno, CString &ropRPQUrud, CString &ropSrvRefUrno)
{
	CCSPtrArray<RecordSet>	olRudRecords;
	RecordSet				olRecord;
	bool					blRet = false;

	ogBCD.GetRecords( "RPQ_TMP", "URUD", ropRPQUrud, &olRudRecords );
	//  olRudRecords enth�lt alle Qualifikationen zur Rule-Demand ropRPQUrud
	for ( int i=0; !blRet && (i<olRudRecords.GetSize()); i++ )
	{
		olRecord = olRudRecords[i];
		if ( ( olRecord.Values[igRpqUperIdx] == ropSrvRefUrno )  &&
			 ( olRecord.Values[igRpqUrnoIdx] != ropRPQUrno ) )
			 blRet = true;
	}
	olRudRecords.DeleteAll ();
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------

void DataSet::RemoveResources(CStringArray &opRudUrnos, CStringList *popUdgrList/*=0*/)
{
	CCS_TRY

	OnDeleteRuds ( "RPF", opRudUrnos );
	OnDeleteRuds ( "RPQ", opRudUrnos );
	OnDeleteRuds ( "REQ", opRudUrnos );
	OnDeleteRuds ( "RLO", opRudUrnos );

	if ( popUdgrList )
	{
		POSITION pos=0;
		CString  olUdgr;

		pos = popUdgrList->GetHeadPosition();
		while ( pos )
		{
			olUdgr = popUdgrList->GetNext ( pos );
			if ( !DeleteByWhere ( "RPQ", "UDGR", olUdgr, false ) )
				TRACE ( "Error deleting from table RPQ where UDGR=<%s>\n", olUdgr );
		}
		ogBCD.Save( "RPQ" ) ;
	}

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------


bool DataSet::DeleteByWhere(CString opTable, CString opRefField, CString opRefValue, bool bpDbSave/*=true*/)
{
	// ---------------------------------------------------------------------------
	//  CedaBasicData::DeleteByWhere funktioniert nur auf dem Server (d.h. wirkt sich nicht auf 
	//  logische Objekte aus), Broadcasts f�r lokale Datenhaltung kommen nur an, 
	//  wenn URNO im Where-String vorkommt
	// ---------------------------------------------------------------------------

	CCSPtrArray <RecordSet> olRecArr;
	CString					olUrno;
	bool					blRet = true;
	int						ilUrnoIdx;

	ogBCD.GetRecords(opTable, opRefField, opRefValue, &olRecArr);
	if ( olRecArr.GetSize() > 0 )
	{
		ilUrnoIdx = ogBCD.GetFieldIndex ( opTable, "URNO" );
		if ( ilUrnoIdx >= 0 )
		{
			for ( int i=olRecArr.GetSize()-1; i>=0; i-- )
			{
				olUrno = olRecArr[i].Values[ilUrnoIdx];
				blRet &= ogBCD.DeleteRecord(opTable, "URNO", olUrno );
			}
			if ( bpDbSave )
				blRet &= ogBCD.Save ( opTable );
		}
		else 
			blRet = false;
		olRecArr.DeleteAll();
	}
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::CopyConnections(CMapStringToString &opRudOldNewMap, /*CCSPtrArray <RecordSet> *popRudList*/CString opNewRueUrno)
{
	bool blRet = true;

	CString olOldUrno, olNewUrno;
	CCSPtrArray <RecordSet> olRudArray;
	RecordSet olRud(ogBCD.GetFieldCount("RUD_TMP"));

	ogBCD.GetRecords("RUD_TMP", "URUE", opNewRueUrno, &olRudArray);
	int ilCount = olRudArray.GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		olRud = olRudArray[i];/*->GetAt(i);*/
		
		// change RUD.UNDE
		olOldUrno = olRud.Values[igRudUndeIdx];
		if (olOldUrno.IsEmpty() == FALSE && olOldUrno != CString(" "))
		{
			if (opRudOldNewMap.Lookup(olOldUrno, olNewUrno) != NULL)
			{
				olRud.Values[igRudUndeIdx] = olNewUrno;
			}	
		}
		
		// change RUD.UPDE
		olOldUrno = olRud.Values[igRudUpdeIdx];
		if (olOldUrno.IsEmpty() == FALSE && olOldUrno != CString(" "))
		{
			if (opRudOldNewMap.Lookup(olOldUrno, olNewUrno) != NULL)
			{
				olRud.Values[igRudUpdeIdx] = olNewUrno;
			}	
		}
		
		blRet &= ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
	}
	olRudArray.DeleteAll();
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::ChangeWrongReference(RecordSet *popRud, int ipIOT)
{
	bool blRet = true;
	CString olIOT;
	olIOT.Format("%d", ipIOT);

	// set default value for duration of turnaround
	if (popRud->Values[igRudDrtyIdx] == CString("0"))
	{
		popRud->Values[igRudDeduIdx] = CString("600");
	}

	// set new service type
	popRud->Values[igRudDrtyIdx] = olIOT;

	switch (ipIOT)
	{
		case 1:
			popRud->Values[igRudRtdbIdx] = "ONBL";	// reference time for start of demand
			popRud->Values[igRudRtdeIdx] = "ONBL";	// reference time for end of demand
			break;

		case 2:
			popRud->Values[igRudRtdbIdx] = "OFBL";	// reference time for start of demand
			popRud->Values[igRudRtdeIdx] = "OFBL";	// reference time for end of demand
			break;

		default:
			break;

	}

	blRet &= ogBCD.SetRecord("RUD_TMP", "URNO", popRud->Values[igRudUrnoIdx], popRud->Values);
	
	if (blRet == true)
	{
		AddItemToUpdList(popRud->Values[igRudUrnoIdx], "RUD", ITEM_UPD);	
	}
	
	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------

void DataSet::ResetOffsetsAndConnections()
{
	RecordSet olRud(ogBCD.GetFieldCount("RUD_TMP"));

	int ilCount = ogBCD.GetDataCount("RUD_TMP");
	for (int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("RUD_TMP", i, olRud);

		// reset reference offsets
		olRud.Values[igRudTsdbIdx] = CString("0");
		olRud.Values[igRudTsdeIdx] = CString("0");
		// reset connections
		olRud.Values[igRudUndeIdx] = CString("");
		olRud.Values[igRudUpdeIdx] = CString("");
		olRud.Values[igRudRendIdx] = CString("");
		olRud.Values[igRudRepdIdx] = CString("");
		olRud.Values[igRudTsndIdx] = CString("0");
		olRud.Values[igRudTspdIdx] = CString("0");

		ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
		AddItemToUpdList(olRud.Values[igRudUrnoIdx], "RUD", ITEM_UPD);		
	}
}
//-----------------------------------------------------------------------------------------------------------------------

// REMARK:
//        Deletes all records from RUD_TMP which are not
//        of the specified type, ie. RUD.DRTY != ipIOT
// IN: type of records to delete (0 = turnaround, 1 = inbound, 2 = outbound)
// OUT: number of records left in data object
int DataSet::DeleteWrongTypeFromRudTmp(int ipIOT)
{
	int ilRes = 0;

	CCS_TRY

	RecordSet olRud(ogBCD.GetFieldCount("RUD_TMP"));
	CString olType;
	int ilType;

	int ilCount = ogBCD.GetDataCount("RUD_TMP");

	for (int i = ilCount; i >= 0; i--)
	{
		olType = ogBCD.GetRecord("RUD_TMP", i, olRud);	
		if (olType.IsEmpty() == FALSE)
		{
			ilType = atoi(olType);
			if (ilType != ipIOT)
			{
				ogBCD.DeleteRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx]);
				AddItemToUpdList(olRud.Values[igRudUrnoIdx], "RUD", ITEM_DEL);				
			}
		}
	}
	
	ilRes = ogBCD.GetDataCount("RUD_TMP");

	CCS_CATCH_ALL

	return ilRes;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::ChangeRudStartEnd ( RecordSet &ropRudRecord, BOOL bpBecomesFloating )
{
	CString olStartTime, olEndTime;
	long llDuration;
	int idxStartSource = bpBecomesFloating ? igRudDebeIdx : igRudEadbIdx ;
	int idxEndSource = bpBecomesFloating ? igRudDeenIdx : igRudLadeIdx ;
	int idxStartDest = bpBecomesFloating ? igRudEadbIdx : igRudDebeIdx ;
	int idxEndDest = bpBecomesFloating ? igRudLadeIdx : igRudDeenIdx ;

	ropRudRecord.Values[igRudDbflIdx] = 
		ropRudRecord.Values[igRudDeflIdx] = bpBecomesFloating ? "1" : "0";

	olStartTime = ropRudRecord.Values[idxStartSource];
	olEndTime = ropRudRecord.Values[idxEndSource];

	//  Check, ob Felder g�ltige Zeiten enthalten
	if ( (DBStringToDateTime(olStartTime) == TIMENULL) ||
		 (DBStringToDateTime(olEndTime) == TIMENULL) )
		 return false;
	ropRudRecord.Values[idxStartSource] = "";
	ropRudRecord.Values[idxEndSource] = "";
	ropRudRecord.Values[idxStartDest] = olStartTime;
	ropRudRecord.Values[idxEndDest] = olEndTime;
	//ropRudRecord.Values[igRudDbflIdx] = 
	//	ropRudRecord.Values[igRudDeflIdx] = bpBecomesFloating ? "1" : "0";
	if ( ! bpBecomesFloating )
	{
		llDuration = CalcDutyDuration(olStartTime,olEndTime,"", "0", "0" );
		if ( llDuration >= 0 )
			ropRudRecord.Values[igRudDeduIdx].Format("%ld", llDuration);
		
	}
	return true;
}


//-----------------------------------------------------------------------------------------------------------------

void DataSet::SetRudStartEnd(RecordSet &ropRudRecord, BOOL bpFloating, CString opStart, CString opEnd)
{
	ropRudRecord.Values[igRudDebeIdx] = bpFloating ? "" : opStart;
	ropRudRecord.Values[igRudDeenIdx] = bpFloating ? "" : opEnd;
	ropRudRecord.Values[igRudEadbIdx] = bpFloating ? opStart : "";
	ropRudRecord.Values[igRudLadeIdx] = bpFloating ? opEnd :"" ;
	ropRudRecord.Values[igRudDbflIdx] = 
		ropRudRecord.Values[igRudDeflIdx] = bpFloating ? "1" : "0";
}
//-----------------------------------------------------------------------------------------------------------------

void DataSet::GetRudStartTime(RecordSet &ropRudRecord, BOOL &bpFloating, CString &ropStart)
{
	bpFloating = (ropRudRecord.Values[igRudDbflIdx] == "1");
	if (bpFloating)
	{
		ropStart = ropRudRecord.Values[igRudEadbIdx];
	}
	else
	{
		ropStart = ropRudRecord.Values[igRudDebeIdx];
	}
}
//-----------------------------------------------------------------------------------------------------------------

void DataSet::GetRudEndTime(RecordSet &ropRudRecord, BOOL &bpFloating, CString &ropEnd)
{
	bpFloating = (ropRudRecord.Values[igRudDeflIdx] == "1");
	if (bpFloating)
	{
		ropEnd = ropRudRecord.Values[igRudLadeIdx];
	}
	else
	{
		ropEnd = ropRudRecord.Values[igRudDeenIdx];
	}
}
//-----------------------------------------------------------------------------------------------------------------

//  liefert Anzahl der RUD's in einer DGR, bei denen wenigstens eines der 
//  Flages ("DBFL", "DEFL") auf floating sitzt.
int DataSet::FloatingRudsInDgr ( CString opTable, CString opDgrUrno )
{
	CCSPtrArray<RecordSet>	olDGRRecords;
	int						ilRet=0;

	ogBCD.GetRecords(opTable, "UDGR", opDgrUrno, &olDGRRecords );
	for ( int i=0; i<olDGRRecords.GetSize(); i++ )
	{
		if ( ( olDGRRecords[i].Values[igRudDbflIdx] == "1" ) || 
			 ( olDGRRecords[i].Values[igRudDeflIdx] == "1" ) )
			 ilRet++;
	}
	olDGRRecords.DeleteAll ();
	return ilRet;
}

//-----------------------------------------------------------------------------------------------------------------
void DataSet::SetDgrFloating ( CString opTable, CString opDgrUrno, 
							   BOOL &bpFloating )
{
	CCSPtrArray<RecordSet> olDGRRecords;
	RecordSet olRecord(ogBCD.GetFieldCount(opTable));

	ogBCD.GetRecords(opTable, "UDGR", opDgrUrno, &olDGRRecords );
	for ( int i=0; i<olDGRRecords.GetSize(); i++ )
	{
		olRecord = olDGRRecords[i];
		//olRecord.Values[igRudDbflIdx] = olRecord.Values[igRudDeflIdx] = bpFloating ? "1" : "0";
		ChangeRudStartEnd ( olRecord, bpFloating );
		ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
		AddItemToUpdList(olRecord.Values[igRudUrnoIdx], "RUD", ITEM_UPD);		
	}
	olDGRRecords.DeleteAll ();
}

//-----------------------------------------------------------------------------------------------------------------
void DataSet::SetAllDbarAndDear(bool bpAbsolut)
{
	int ilRecAnz = ogBCD.GetDataCount("RUD_TMP");
	CString olValue = bpAbsolut ? "A" : "R" ;
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));

	for (int i=0; i<ilRecAnz; i++)
	{
		if (ogBCD.GetRecord("RUD_TMP", i, olRecord) && ((olRecord.Values[igRudDbarIdx] != olValue) ||
			   (olRecord.Values[igRudDearIdx] != olValue)))
		{
			olRecord.Values[igRudDbarIdx] = olRecord.Values[igRudDearIdx] = olValue;
			ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
			AddItemToUpdList(olRecord.Values[igRudUrnoIdx], "RUD", ITEM_UPD);
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::AppendValue(RecordSet *popRud, CString opField, int ipDiff)
{
	int ilIdx = ogBCD.GetFieldIndex("RUD_TMP", opField);
	if (ilIdx != -1)
	{
		int ilOldVal = atoi(popRud->Values[ilIdx]);
		popRud->Values[ilIdx].Format("%d", ilOldVal + ipDiff);
		bool blTest = ogBCD.SetRecord("RUD_TMP", "URNO", popRud->Values[igRudUrnoIdx], popRud->Values);
		AddItemToUpdList(popRud->Values[igRudUrnoIdx], "RUD", ITEM_UPD);
		TRACE("[DataSet::AppendValue] %s (%d) in RUD %s has been set to %d (old: %d)\n", opField, ilIdx, popRud->Values[igRudUrnoIdx], ilOldVal + ipDiff, ilOldVal);
	}
	else
	{
		return false;
	}

	return true;
}
//-----------------------------------------------------------------------------------------------------------------

bool DataSet::CopyOneRud ( CString opOldRudUrno, CString &ropNewRudUrno )
{
	RecordSet				olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	CCSPtrArray <RecordSet> olResourceArr;
	RecordSet				olResRecord;
	CString					olUrno;

	if ( !ogBCD.GetRecord ( "RUD_TMP", "URNO", opOldRudUrno, olRecord ) )
		return false;
	ropNewRudUrno.Format ( "%lu", ogBCD.GetNextUrno() );
	olRecord.Values[igRudUrnoIdx] = ropNewRudUrno;
	olRecord.Values[igRudRendIdx] =
	olRecord.Values[igRudRepdIdx] = 
	olRecord.Values[igRudTsndIdx] = 
	olRecord.Values[igRudTspdIdx] = 
	olRecord.Values[igRudUndeIdx] =  
	olRecord.Values[igRudUpdeIdx] = "";

	ogBCD.GetRecords ( "RPF_TMP", "URUD", opOldRudUrno, &olResourceArr );
	for ( int i=0; i<olResourceArr.GetSize(); i++ )
	{	
		olResRecord = olResourceArr[i];
		olUrno.Format ( "%lu", ogBCD.GetNextUrno() );
		olResRecord.Values[igRpfUrnoIdx] = olUrno;
		olResRecord.Values[igRpfUrudIdx] = ropNewRudUrno;
		ogBCD.InsertRecord("RPF_TMP", olResRecord);
	}

	ogBCD.GetRecords ( "RPQ_TMP", "URUD", opOldRudUrno, &olResourceArr );
	for ( i=0; i<olResourceArr.GetSize(); i++ )
	{	
		olResRecord = olResourceArr[i];
		olUrno.Format ( "%lu", ogBCD.GetNextUrno() );
		olResRecord.Values[igRpqUrnoIdx] = olUrno;
		olResRecord.Values[igRpqUrudIdx] = ropNewRudUrno;
		ogBCD.InsertRecord("RPQ_TMP", olResRecord);
	}

	ogBCD.GetRecords ( "RLO_TMP", "URUD", opOldRudUrno, &olResourceArr );
	for ( i=0; i<olResourceArr.GetSize(); i++ )
	{	
		olResRecord = olResourceArr[i];
		olUrno.Format ( "%lu", ogBCD.GetNextUrno() );
		olResRecord.Values[igRloUrnoIdx] = olUrno;
		olResRecord.Values[igRloUrudIdx] = ropNewRudUrno;
		ogBCD.InsertRecord("RLO_TMP", olResRecord);
	}
	ogBCD.GetRecords ( "REQ_TMP", "URUD", opOldRudUrno, &olResourceArr );
	for ( i=0; i<olResourceArr.GetSize(); i++ )
	{	
		olResRecord = olResourceArr[i];
		olUrno.Format ( "%lu", ogBCD.GetNextUrno() );
		olResRecord.Values[igReqUrnoIdx] = olUrno;
		olResRecord.Values[igReqUrudIdx] = ropNewRudUrno;
		ogBCD.InsertRecord("REQ_TMP", olResRecord);
	}
	ogBCD.InsertRecord("RUD_TMP", olRecord);
	AddItemToUpdList(ropNewRudUrno, "RUD", ITEM_NEW);
	ogDdx.DataChanged((void *)this,RUD_TMP_NEW,(void *)&olRecord);
				
	olResourceArr.DeleteAll();
	return true;
}


long CalcDutyDuration(CString opDebe,CString opDeen,CString opDedu, CString opDbfl,CString opDefl)
{

	CTime olBeginTime = TIMENULL;
	CTime olEndTime = TIMENULL;

	olBeginTime = DBStringToDateTime(opDebe);
	olEndTime = DBStringToDateTime(opDeen);
	while(olBeginTime > olEndTime)
	{
		olEndTime += CTimeSpan(1,0,0,0);
	}

	long llDiffSecs = (olEndTime - olBeginTime).GetTotalSeconds();

	if ((opDbfl != "1") && (opDefl != "1"))
	{		
		
		while (llDiffSecs > 86400) // no duties > 24 h
			llDiffSecs -= 86400;
	}
	else
	{
		// duration for floating demands can't be longer than 
		// difference of times given
		long llDuration = atol(opDedu);
		if (llDiffSecs <= llDuration)
		{
			llDiffSecs = -1;
		}
	}
	return llDiffSecs;
}

//-----------------------------------------------------------------------------------------------------------------

void DataSet::CreateEquEntry(CString opRudUrno, RecordSet *popSee)
{
	CCS_TRY

	RecordSet olReqRecord(ogBCD.GetFieldCount("REQ_TMP"));

	// get an urno for this object (won't be overwritten by ogBCD on inserting the record)
	long llUrno = ogBCD.GetNextUrno();
	olReqRecord.Values[igReqUrnoIdx].Format("%d", llUrno);
	
	olReqRecord.Values[igReqUrudIdx] = opRudUrno;
	olReqRecord.Values[igReqEqcoIdx] = popSee->Values[igSeeEqcoIdx];
	olReqRecord.Values[igReqEqnoIdx] = popSee->Values[igSeeEqnoIdx];	//???
	olReqRecord.Values[igReqEtypIdx] = popSee->Values[igSeeEtypIdx];
	olReqRecord.Values[igReqPrioIdx] = popSee->Values[igSeePrioIdx];
	olReqRecord.Values[igReqUequIdx] = popSee->Values[igSeeUequIdx];
	olReqRecord.Values[igReqGtabIdx] = popSee->Values[igSeeGtabIdx];

	ogBCD.InsertRecord("REQ_TMP", olReqRecord);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------
int DataSet::FillLogicReqObject()
{
	CCS_TRY
	
	
	CString olRud;
	CCSPtrArray <RecordSet> olEquArr;
	RecordSet olReqRecord(ogBCD.GetFieldCount("REQ_TMP"));

	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	bool blErr = ResetLogicTable ( "REQ_TMP" );

	for (int i = 0; i < ilRudCount; i++)
	{
		olRud = ogBCD.GetField("RUD_TMP", i, "URNO");
		ogBCD.GetRecords("REQ", "URUD", olRud, &olEquArr);

		int ilSize = olEquArr.GetSize();
		for (int j = 0; j < ilSize; j++)
		{
			olReqRecord = olEquArr[j];
			ogBCD.InsertRecord("REQ_TMP", olReqRecord);
		}

		olEquArr.DeleteAll();
	}

	CCS_CATCH_ALL

	return ogBCD.GetDataCount("REQ_TMP");
}


bool DataSet::SaveModifiedRule ( CString opUrue )
{
	CString		olUrno, olUdgr, olResUrno;
	CStringList	olUdgrList ;
	int			ilSize, ilOld;
	bool blOk = true ;
	CStringList olOldRuds;
	CStringList olOldRpfs;
	CStringList olOldRpqs;
	CStringList olOldRlos;
	CStringList olOldReqs;
	CCSPtrArray <RecordSet> olRecArray;
	RecordSet	olResRecord;
	POSITION	pos;

	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
	
	//  collect all old duty requirements in olOldRuds
	ogBCD.GetRecords("RUD", "URUE", opUrue, &olRecArray );

	for ( ilOld = 0; ilOld < olRecArray.GetSize(); ilOld++ )
	{
		olUdgr =  olRecArray[ilOld].Values[igRudUdgrIdx];	
		olUrno =  olRecArray[ilOld].Values[igRudUrnoIdx];	
		if (olUdgr.IsEmpty() == FALSE && !olUdgrList.Find(olUdgr))
		{
			olUdgrList.AddTail (olUdgr);
		}
		olOldRuds.AddTail ( olUrno );
		//  if function for this RUD exists, add it to olOldRpfs
		if ( ogBCD.GetField( "RPF", "URUD", olUrno, "URNO", olResUrno ) )
			olOldRpfs.AddTail ( olResUrno );
		//  if equipment for this RUD exists, add it to olOldRpfs
		if ( ogBCD.GetField( "REQ", "URUD", olUrno, "URNO", olResUrno ) )
			olOldReqs.AddTail ( olResUrno );
		//  if location for this RUD exists, add it to olOldRpfs
		if ( ogBCD.GetField( "RLO", "URUD", olUrno, "URNO", olResUrno ) )
			olOldRlos.AddTail ( olResUrno );
	}
	olRecArray.DeleteAll ();

	//  Add old qualifications to olOldRpqs
	pos = olOldRuds.GetHeadPosition ();
	while ( pos )
	{
		olUrno = olOldRuds.GetNext ( pos );
		if ( !olUrno.IsEmpty() )
		{
			ogBCD.GetRecords("RPQ", "URUD", olUrno, &olRecArray);
			
			ilSize = olRecArray.GetSize();
			for (int j = 0; j < ilSize; j++)
			{
				olOldRpqs.AddTail ( olRecArray[j].Values[igRpqUrnoIdx] );
			}
			olRecArray.DeleteAll();
		}
	}
	//  Add old group qualifications to olOldRpqs
	pos = olUdgrList.GetHeadPosition ();
	while ( pos )
	{
		olUdgr = olUdgrList.GetNext ( pos );
		if ( !olUdgr.IsEmpty() )
		{
			ogBCD.GetRecords("RPQ", "UDGR", olUdgr, &olRecArray);
			
			ilSize = olRecArray.GetSize();
			for (int j = 0; j < ilSize; j++)
			{
				olOldRpqs.AddTail ( olRecArray[j].Values[igRpqUrnoIdx] );
			}
		}
	}
	olRecArray.DeleteAll();
	blOk = SaveRuleChanges ( "RUD", olOldRuds);
	blOk &= SaveRuleChanges ( "RPF", olOldRpfs);
	blOk &= SaveRuleChanges ( "RPQ", olOldRpqs);
	blOk &= SaveRuleChanges ( "RLO", olOldRlos);
	blOk &= SaveRuleChanges ( "REQ", olOldReqs);

	return blOk;
}	
	
	
bool DataSet::SaveRuleChanges ( CString opTable, CStringList &ropOldUrnos )
{
	CString		olTmpTable, olUrno;
	CString		olMsg;
	int			ilNewRecs, ilNew;
	RecordSet	olRecord;
	int 		ilUrnoIdx = ogBCD.GetFieldIndex ( opTable, "URNO" );
	bool		blOk = true;
	POSITION	pos;

	if ( ilUrnoIdx < 0 )
	{
		olMsg.Format ( "SaveRuleChanges: URNO index not found for table %s", opTable );
		AfxMessageBox ( olMsg );
		return false;
	}
	olTmpTable = opTable + "_TMP";
	
	ilNewRecs = ogBCD.GetDataCount( olTmpTable );
	for ( ilNew = 0; ilNew < ilNewRecs; ilNew++ )
	{
		if ( ogBCD.GetRecord(olTmpTable, ilNew, olRecord ) )
		{
			olUrno = olRecord.Values[ilUrnoIdx];
			pos = ropOldUrnos.Find ( olUrno );
			if ( pos )	
			{	//  record already existed before
				if ( !ogChangeInfo.CheckResourceChanges(opTable, olTmpTable, "URNO", olUrno) )
				{
					if ( !ogBCD.SetRecord ( opTable, "URNO", olUrno, olRecord.Values ) )
					{
						blOk = false;
						olMsg.Format ( "SaveRuleChanges: Error on updating URNO <%s> in table <%s>", olUrno, opTable );
						AfxMessageBox ( olMsg );
					}
					else
						TRACE ( "SaveRuleChanges: Updated <%s> in <%s> successfully\n", olUrno, opTable );
						
				}
				ropOldUrnos.RemoveAt ( pos );
			}
			else
			{
				if ( !ogBCD.InsertRecord ( opTable, olRecord ) )
				{
					blOk = false;
					olMsg.Format ( "SaveRuleChanges: Error on inserting URNO <%s> in table <%s>", olUrno, opTable );
					AfxMessageBox ( olMsg );
				}
				else
					TRACE ( "SaveRuleChanges: Inserted <%s> in <%s> successfully\n", olUrno, opTable );
			}				
		}
	}
	pos = ropOldUrnos.GetHeadPosition ();
	while ( pos )
	{
		olUrno = ropOldUrnos.GetNext ( pos );
		if ( !ogBCD.DeleteRecord( opTable, "URNO", olUrno ) )
		{
			blOk = false;
			olMsg.Format ( "SaveRuleChanges: Error on deleting URNO <%s> from table <%s>", olUrno, opTable );
			AfxMessageBox ( olMsg );
		}
		else
			TRACE ( "SaveRuleChanges: Deleted <%s> from <%s> successfully\n", olUrno, opTable );
	}
	ogBCD.Save ( opTable );
	
	return blOk;
}


bool DataSet::CheckRules ( CString opFilePath ) 
{
	FILE		*fp;
	int			i, ilRuleErrors = 0;
	int			ilServiceErrors = 0;
	int			ilTplCount, ilTpl;
	int			ilRueCount, ilRue;
	int			ilRudCount, ilRud;
	int			ilRpqCount, ilRpq;
	int			ilSerCount, ilSer;
	bool		blFirstInThisTpl;
	CCSPtrArray <RecordSet> olRueRecs;
	CCSPtrArray <RecordSet> olRudRecs;
	CCSPtrArray <RecordSet> olRpqRecs;
	CCSPtrArray <RecordSet> olResRecs;
	RecordSet	olTplRec, olResRecord, olEqtRecord, olSerRecord;
	CString		olTpst, olTnam, olUtpl, olUtpl1, olRusn, olRust, olUrue;
	CString		olRety, olUrud, olLine, olResUrno, olGtab, olUdgr, olUghs ;
	CString		olResTable, olCodeField, olResCode, olEtyp, olReft;
	CStringArray	olLineArr;
	CStringList	olUdgrList;
	CWaitDlg	*polWaitDlg;

	olLine = GetString (IDS_CHECK_WAITMSG);
	
	fp = fopen ( opFilePath, "w" );
	if ( !fp )
		return false;

	polWaitDlg = new CWaitDlg (0, olLine );
	
	ilTplCount = ogBCD.GetDataCount ("TPL" );
	for ( ilTpl=0; ilTpl<ilTplCount; ilTpl++ )
	{
		if ( !ogBCD.GetRecord("TPL", ilTpl, olTplRec) )
			continue;
		olTpst = olTplRec.Values[igTplTpstIdx];
		olTnam = olTplRec.Values[igTplTnamIdx];

		//TODO:  Check whether template may be changed

		if ( (olTpst != "0") && (olTpst != "1") )
		{
			TRACE ( "CheckRules: Template <%s> Status <%s> not checked.\n", olTnam, olTpst );
			continue;
		}
		if ( !CanModifyTemplate ( olTnam ) )
		{
			TRACE ( "CheckRules: Not allowed to modify template <%s>.\n", olTnam );
			continue;
		}
		olUtpl = olTplRec.Values[igTplUrnoIdx];
		olUtpl1 = GetCorrespTplUrno(olUtpl);
		TRACE ( "CheckRules: Checking Template <%s> URNO <%s>...\n", olTnam, olUtpl );
		blFirstInThisTpl = true;
		ogBCD.GetRecords("RUE", "UTPL", olUtpl, &olRueRecs);
		ilRueCount = olRueRecs.GetSize (); 
		for ( ilRue=0; ilRue<ilRueCount; ilRue++ )
		{
			olRusn = olRueRecs[ilRue].Values[igRueRusnIdx];
			olRust = olRueRecs[ilRue].Values[igRueRustIdx];
			if ( (olRust != "0") && (olRust != "1") )
				continue;	
			olLineArr.RemoveAll ();
			olUdgrList.RemoveAll ();

			IsEventOk ( olRueRecs[ilRue].Values[igRueEvttIdx], olUtpl, olUtpl1, olLineArr );
			IsEventOk ( olRueRecs[ilRue].Values[igRueEvtaIdx], olUtpl, olUtpl1, olLineArr );
			IsEventOk ( olRueRecs[ilRue].Values[igRueEvtdIdx], olUtpl, olUtpl1, olLineArr );
			
			olUrue = olRueRecs[ilRue].Values[igRueUrnoIdx];
			ogBCD.GetRecords("RUD", "URUE", olUrue, &olRudRecs);
			ilRudCount = olRudRecs.GetSize();
			for ( ilRud=0; ilRud<ilRudCount; ilRud++ )
			{
				olRety = olRudRecs[ilRud].Values[igRudRetyIdx];
				olUrud = olRudRecs[ilRud].Values[igRudUrnoIdx];
				olUdgr = olRudRecs[ilRud].Values[igRudUdgrIdx];
				olUghs = olRudRecs[ilRud].Values[igRudUghsIdx];
				if ( !ogBCD.GetRecord ( "SER", "URNO", olUghs, olSerRecord ) )
				{	//  check Service
					//  olLine = "Undefined service used";
					olLine = GetString ( IDS_SER_UNDEF );
					olLineArr.Add ( olLine );
				}
				if ( !olUdgr.IsEmpty() && !olUdgrList.Find ( olUdgr ) )
					olUdgrList.AddTail ( olUdgr );
				if ( olRety == "100" )
				{	//  check Function
					if ( !ogBCD.GetRecord ( "RPF", "URUD", olUrud, olResRecord ) )
					{
						//  olLine = "Undefined function used";
						olLine = GetString ( IDS_PFC_UNDEF );
						olLineArr.Add ( olLine );
					}
					else
					{
						olResCode = olResRecord.Values[igRpfFccoIdx];
						olResUrno = olResRecord.Values[igRpfUpfcIdx];
						olGtab = olResRecord.Values[igRpfGtabIdx];

						if ( !IsResourceOk ( "PFC", olGtab, olResUrno, olResCode,
													olUtpl, olUtpl1, olLine ) )
							olLineArr.Add ( olLine );
					}
					//  check Qualifications
					ogBCD.GetRecords ( "RPQ", "URUD", olUrud, &olRpqRecs );
					ilRpqCount = olRpqRecs.GetSize ();
					for ( ilRpq=0; ilRpq<ilRpqCount; ilRpq++ )
					{
						olResCode = olRpqRecs[ilRpq].Values[igRpqQucoIdx];
						olResUrno = olRpqRecs[ilRpq].Values[igRpqUperIdx];
						olGtab = olRpqRecs[ilRpq].Values[igRpqGtabIdx];

						if ( !IsResourceOk ( "PER", olGtab, olResUrno, olResCode,
													olUtpl, olUtpl1, olLine ) )
							olLineArr.Add ( olLine );
					}
					olRpqRecs.DeleteAll ();
				}
				if ( olRety == "010" )
				{	//  check equipment
					if ( !ogBCD.GetRecord ( "REQ", "URUD", olUrud, olResRecord ) )
					{
						//  olLine = "Undefined equipment resource used";
						olLine = GetString ( IDS_EQU_UNDEF );
						olLineArr.Add ( olLine );
					}
					else
					{
						olResCode = olResRecord.Values[igReqEqcoIdx];
						olResUrno = olResRecord.Values[igReqUequIdx];
						olGtab = olResRecord.Values[igReqGtabIdx];
						olEtyp = olResRecord.Values[igReqEtypIdx];

						if ( !IsResourceOk ( "EQU", olGtab, olResUrno, olResCode,
													olUtpl, olUtpl1, olLine ) )
							olLineArr.Add ( olLine );
						if ( !ogBCD.GetRecord ( "EQT", "URNO", olEtyp, olEqtRecord ) )
						{
							// olLine = "Undefined equipement type used";
							olLine = GetString ( IDS_EQT_UNDEF );
							olLineArr.Add ( olLine );
						}
					}
				}
				if ( olRety == "001" )
				{	//  check locations
					olResTable.Empty ();
					if ( ogBCD.GetRecord ( "RLO", "URUD", olUrud, olResRecord ) )
					{
						olReft = olResRecord.Values[igRloReftIdx];
						ParseReftFieldEntry(olReft, olResTable, olCodeField);
					}	
					if ( olResTable.IsEmpty () )
					{
						//  olLine = "Undefined location resource used";
						olLine = GetString ( IDS_LOCATION_UNDEF );
						olLineArr.Add ( olLine );
					}
					else
					{
						olResUrno = olResRecord.Values[igRloRlocIdx];
						olGtab = olResRecord.Values[igRloGtabIdx];
						if ( !IsResourceOk ( olResTable, olGtab, olResUrno, "",
											 olUtpl, olUtpl1, olLine ) )
							olLineArr.Add ( olLine );
					}
				}
			}
			//  check group qualifications
			while ( !olUdgrList.IsEmpty() )
			{
				olUdgr = olUdgrList.RemoveHead ();
				ogBCD.GetRecords ( "RPQ", "UDGR", olUdgr, &olRpqRecs );
				ilRpqCount = olRpqRecs.GetSize ();
				for ( ilRpq=0; ilRpq<ilRpqCount; ilRpq++ )
				{
					olResCode = olRpqRecs[ilRpq].Values[igRpqQucoIdx];
					olResUrno = olRpqRecs[ilRpq].Values[igRpqUperIdx];
					olGtab = olRpqRecs[ilRpq].Values[igRpqGtabIdx];
					if ( !IsResourceOk ( "PER", olGtab, olResUrno, olResCode,
												olUtpl, olUtpl1, olLine ) )
						olLineArr.Add ( olLine );
				}
				olRpqRecs.DeleteAll ();
			}

			olRudRecs.DeleteAll ();
			for ( i=0; i<olLineArr.GetSize(); i++ )
			{
				if ( blFirstInThisTpl )
				{
					//fprintf ( fp, "Inconsistent data found in template <%s>:\n", olTnam );
					olLine = GetString ( IDS_TPL_INCONSISTENT );
					olLine.Replace ( "%s", olTnam );
					fprintf ( fp, "%s\n", olLine );
					blFirstInThisTpl = false;
				}
				olLine = GetString ( IDS_RULE );
				fprintf ( fp, "    %s <%s>: %s\n", olLine, olRusn, olLineArr[i] );	
				ilRuleErrors ++;
			}

		}
		olRueRecs.DeleteAll();
		if ( !blFirstInThisTpl )
		{
			fprintf ( fp, "\n" );
		}
	}

	//  Check Service catalog
	blFirstInThisTpl = true;
	ilSerCount = ogBCD.GetDataCount ("SER" );
	for ( ilSer=0; ilSer<ilSerCount; ilSer++ )
	{
		CString olUrno, olSeco;
		if ( !ogBCD.GetRecord("SER", ilSer, olSerRecord) )
			continue;
		olLineArr.RemoveAll ();
		olUrno = olSerRecord.Values[igSerUrnoIdx];
		olSeco = olSerRecord.Values[igSerSecoIdx];
		//  check functions		
		ogBCD.GetRecords ( "SEF", "USRV", olUrno, &olResRecs );
		for ( i=0; i<olResRecs.GetSize(); i++ )
		{
			olGtab = olResRecs[i].Values[igSefGtabIdx];
			olResUrno = olResRecs[i].Values[igSefUpfcIdx];
			olResCode = olResRecs[i].Values[igSefFccoIdx];
			if ( !IsResourceOk ( "PFC", olGtab, olResUrno, olResCode, "", "", olLine ) )
				olLineArr.Add ( olLine );
		}
		olResRecs.DeleteAll ();

		//  check qualifications
		ogBCD.GetRecords ( "SEQ", "USRV", olUrno, &olResRecs );
		for ( i=0; i<olResRecs.GetSize(); i++ )
		{
			olGtab = olResRecs[i].Values[igSeqGtabIdx];
			olResUrno = olResRecs[i].Values[igSeqUperIdx];
			olResCode = olResRecs[i].Values[igSeqQucoIdx];
			if ( !IsResourceOk ( "PER", olGtab, olResUrno, olResCode, "", "", olLine ) )
				olLineArr.Add ( olLine );
		}
		olResRecs.DeleteAll ();

		//  check equipment
		ogBCD.GetRecords ( "SEE", "USRV", olUrno, &olResRecs );
		for ( i=0; i<olResRecs.GetSize(); i++ )
		{
			olGtab = olResRecs[i].Values[igSeeGtabIdx];
			olResUrno = olResRecs[i].Values[igSeeUequIdx];
			olResCode = olResRecs[i].Values[igSeeEqcoIdx];
			olEtyp = olResRecs[i].Values[igSeeEtypIdx];
			if ( !IsResourceOk ( "EQU", olGtab, olResUrno, olResCode, "", "", olLine ) )
				olLineArr.Add ( olLine );
			if ( !ogBCD.GetRecord ( "EQT", "URNO", olEtyp, olEqtRecord ) )
			{
				olLine = GetString ( IDS_EQT_UNDEF );
				olLineArr.Add ( olLine );
			}
		}
		olResRecs.DeleteAll ();

		//  check locations
		ogBCD.GetRecords ( "SEL", "USRV", olUrno, &olResRecs );
		for ( i=0; i<olResRecs.GetSize(); i++ )
		{
			olReft = olResRecs[i].Values[igSelReftIdx];
			if ( ! ParseReftFieldEntry(olReft, olResTable, olCodeField) )
			{
				olLine = GetString ( IDS_LOCATION_UNDEF );
				olLineArr.Add ( olLine );
			}
			else
			{
				olGtab = olResRecs[i].Values[igSelGtabIdx];
				olResUrno = olResRecs[i].Values[igSelRlocIdx];
				if ( !IsResourceOk ( olResTable, olGtab, olResUrno, "", "", "", olLine ) )
					olLineArr.Add ( olLine );
			}
		}
		olResRecs.DeleteAll ();

		for ( i=0; i<olLineArr.GetSize(); i++ )
		{
			if ( blFirstInThisTpl )
			{
				//fprintf ( fp, "Inconsistent data found in service catalog:\n" );
				olLine = GetString ( IDS_SER_INCONSISTENT );
				fprintf ( fp, "%s\n", olLine );
				blFirstInThisTpl = false;
			}
			olLine = GetString ( IDS_SERVICE );
			fprintf ( fp, "    %s <%s>: %s\n", olLine, olSeco, olLineArr[i] );	
			ilServiceErrors ++;
		}

	}
	fclose ( fp );
	if ( polWaitDlg )	
	{
		polWaitDlg->CloseWindow();
		delete polWaitDlg;
	}
	return ( (ilRuleErrors >0 ) || (ilServiceErrors >0) );
}


bool DataSet::IsResourceOk ( CString opTable, CString opGtab, CString opResUrno,
							 CString opResCode, CString opTpl1, CString opTpl2, 
							 CString &ropLine )
{
	RecordSet	olResource;
	CString		olSgrUtpl, olResType, olAddInfo, olForm;

	ropLine.Empty();
	if ( opGtab.IsEmpty () )
	{
		if ( !ogBCD.GetRecord ( opTable, "URNO", opResUrno, olResource ) )
		{
			olForm = GetString ( IDS_RES_UNDEF );
			//  ropLine.Format ( "Undefined resource <%s> used", opResCode );
			ropLine.Format ( olForm, opResCode );
		}
	}
	else
	{
		if ( !ogBCD.GetRecord ( "SGR", "URNO", opResUrno, olResource ) )
			//  ropLine = "Undefined static group of resources used";
			ropLine = GetString ( IDS_SGR_UNDEF );
		else
			if ( (igSgrUtplIdx>=0) && (!opTpl1.IsEmpty() || !opTpl2.IsEmpty()) )
			{
				olSgrUtpl = olResource.Values[igSgrUtplIdx];
				if ( !olSgrUtpl.IsEmpty() && (olSgrUtpl!=opTpl1) && (olSgrUtpl!=opTpl2) )
				{
					olForm = GetString ( IDS_UTPL_NOTVALID );
					//ropLine.Format ( "Static group <%s> not valid for this template", 
					//				 olResource.Values[igSgrGrpnIdx] );
					ropLine.Format ( olForm, olResource.Values[igSgrGrpnIdx] );
				}
			}
	}
	if ( !ropLine.IsEmpty () )
	{
		if ( opTable=="PFC" )
			olResType = GetString ( PFC_HEADER );
		else if ( opTable=="PER" )
			olResType = GetString ( PER_HEADER );
		else if ( opTable=="EQU" )
			olResType = GetString ( EQU_HEADER );
		else 
			olResType = ogBCD.GetField ( "ALO", "ALOC", opTable, "ALOD" );
		if ( !olResType.IsEmpty () )
		{
			olAddInfo.Format ( " (%s)", olResType );
			ropLine += olAddInfo;
		}
	}	
	return ropLine.IsEmpty();
}


bool DataSet::IsEventOk ( CString opEvent, CString opTpl1, CString opTpl2, 
						  CStringArray &ropLineArr )
{
	CString		olUtpl, olForm, olMatch, olLine, olUrno;
	CString		olValueType, olData, olValue,olFieldName;
	CString		olBTab, olBFld, olRTab, olRFld, olReft;
	CGXGridCore olGrid;
	CGXControl	olCtrl (&olGrid);
	int			ilPos, ilValType, j, ilError;
	CCSPtrArray<RecordSet> olGroups;

	olLine.Empty();
	if ( opEvent.IsEmpty () )
		return true;
	//--- String zerlegen 
	CStringArray olDataArray;
	int ilData = ExtractItemList(opEvent, &olDataArray, ';');

	for(int i = 0; i < ilData; i++)
	{   
		olData = olDataArray[i];
		if (olData.IsEmpty() == FALSE && olData != CString(" "))
		{
			ilPos = olData.Find("=");
			if(ilPos > 0)
			{
				olValueType = olData.Left(1);
				ilValType = atoi (olValueType );
				olFieldName = olData.Mid(2,8);
				olValue = olData.Right(olData.GetLength() - ilPos - 1);
				
				//  get referenced field and table, if existing
				olBTab = olFieldName.Left ( 3 );
				olBFld = olFieldName.Right ( 4 );
				if ( ilValType != 0 )
				{
					olRTab = ogBCD.GetFieldExt ( "TSR", "BTAB", "BFLD", olBTab, olBFld, "RTAB" );
					olRFld = ogBCD.GetFieldExt ( "TSR", "BTAB", "BFLD", olBTab, olBFld, "RFLD" );
					olReft.Format ( "%s.%s", olRTab, olRFld );
				}   
				switch (ilValType)
				{
					case 0:		//  single value
						for ( j = 0; j < ogChoiceLists.GetSize(); j++)
						{	//  search choice list
							if (ogChoiceLists[j].TabFieldName == olFieldName)  
							{	//  search value	
								ilPos = olCtrl.FindStringInChoiceList(olMatch, olValue, 
														ogChoiceLists[j].ValueList, TRUE);
								if ( ilPos < 0 )
								{	
									olForm = GetString ( IDS_SINGLEVAL_UNDEF );
									olLine.Format ( olForm, olValue );
									ropLineArr.Add ( olLine );
								}
							}
						}
						break;
					case 2:		//  static group
						ilError = IDS_GROUP_UNDEF;
						ogBCD.GetRecordsExt ( "SGR", "GRPN", "TABN", olValue, olRTab, &olGroups );
						for ( j=0; j<olGroups.GetSize(); j++ )
						{
							if ( olGroups[j].Values[igSgrApplIdx] != "RULE_AFT" )
								continue;
							if ( (igSgrUtplIdx>=0) && (!opTpl1.IsEmpty() || !opTpl2.IsEmpty()) )
							{	//  check template 
								olUtpl = olGroups[j].Values[igSgrUtplIdx];
								if ( !olUtpl.IsEmpty() && (olUtpl!=opTpl1) && (olUtpl!=opTpl2) )
								{
									ilError = IDS_UTPL_NOTVALID ;
									continue;
								}
							}
							ilError = 0;
							break;
						}
						if ( ilError )
						{
							olForm = GetString ( ilError );
							olLine.Format ( olForm, olValue );
							ropLineArr.Add ( olLine );
						}
						break;
					case 1:		//Dynamic group
						ilError = IDS_GROUP_UNDEF;
						ogBCD.GetRecordsExt ( "DGR", "GNAM", "REFT", olValue, olReft, &olGroups );
						for ( j=0; j<olGroups.GetSize(); j++ )
						{
							if ( (igDgrUtplIdx>=0) && (!opTpl1.IsEmpty() || !opTpl2.IsEmpty()) )
							{	//  check template 
								olUtpl = olGroups[j].Values[igDgrUtplIdx];
								if ( !olUtpl.IsEmpty() && (olUtpl!=opTpl1) && (olUtpl!=opTpl2) )
								{
									ilError = IDS_DGRUTPL_NOTVALID ;
									continue;
								}
							}
							ilError = 0;
							break;
						}
						if ( ilError )
						{
							olForm = GetString ( ilError );
							olLine.Format ( olForm, olValue );
							ropLineArr.Add ( olLine );
						}
						break;
				}
				olGroups.DeleteAll();
			}
		}
	}

	return olLine.IsEmpty();
}

//-----------------------------------------------------------------------------------------------------------------
bool DataSet::SetFieldForDgr ( CString opTable, CString opDgrUrno, 
							   CString opField, CString opValue )
{
	CCSPtrArray<RecordSet> olDGRRecords;
	int ilIdx = -1;
	RecordSet olRecord(ogBCD.GetFieldCount(opTable));

	ilIdx = ogBCD.GetFieldIndex ( opTable, opField );
	if ( ilIdx < 0 )
		return false;
	ogBCD.GetRecords(opTable, "UDGR", opDgrUrno, &olDGRRecords );
	for ( int i=0; i<olDGRRecords.GetSize(); i++ )
	{
		olRecord = olDGRRecords[i];
		olRecord.Values[ilIdx] = opValue;
		ogBCD.SetRecord("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], olRecord.Values);
		AddItemToUpdList(olRecord.Values[igRudUrnoIdx], "RUD", ITEM_UPD);		
	}
	olDGRRecords.DeleteAll ();
	return true;
}

//-----------------------------------------------------------------------------------------------------------------
bool DataSet::GetFieldForDgr ( CString opTable, CString opDgrUrno, 
							 CString opField, CString &opValue, bool &bpEqual )
{
	CCSPtrArray<RecordSet> olDGRRecords;
	int ilIdx = -1;
	CString olActVal;
	RecordSet olRecord(ogBCD.GetFieldCount(opTable));

	ilIdx = ogBCD.GetFieldIndex ( opTable, opField );
	if ( ilIdx < 0 )
		return false;
	bpEqual = true;
	opValue.Empty ();

	ogBCD.GetRecords(opTable, "UDGR", opDgrUrno, &olDGRRecords );
	for ( int i=0; bpEqual&&(i<olDGRRecords.GetSize()); i++ )
	{
		olActVal = olDGRRecords[i].Values[ilIdx];
		if ( i == 0 )
			opValue = olActVal;
		else if ( opValue != olActVal )
		{
			bpEqual = false;
			opValue.Empty ();
		}
	}
	olDGRRecords.DeleteAll ();
	return true;
}


bool DataSet::CheckRudUtpl ( CString opUtpl )
{
	CString olVal;
	BOOL	blRet = true;

	if ( igRudUtplIdx < 0 )
	{	/* Field RUDTAB.UTPL not in DB configuration */
		return blRet;
	}

	TRACE ( "CheckRudUtpl: Start UTPL <%s>\n", opUtpl );
	
	int ilRudCount = ogBCD.GetDataCount("RUD_TMP");
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
	for (int i = 0; i < ilRudCount; i++)
	{
		ogBCD.GetRecord("RUD_TMP", i, olRecord);
		
		olVal = olRecord.Values[igRudUtplIdx];
		if ( atol(olVal) <= 0 )
		{
			blRet = false;
			TRACE ( "Found record URNO <%s> in RUD_TMP with empty UTPL <%s> set to <%s>\n",
					 olRecord.Values[igRudUrnoIdx], olVal, opUtpl );
			ogBCD.SetField("RUD_TMP", "URNO", olRecord.Values[igRudUrnoIdx], 
						   "UTPL", opUtpl, false );
		}
	}
	TRACE ( "CheckRudUtpl: End, return <%d>\n", blRet );
	return blRet ;
}

bool DataSet::CheckArchive ( CString opFilePath, CString &ropTplUrno ) 
{
	FILE		*fp;
	int			ilRueCount, ilRue;
	int			ilRudCount;
	RecordSet   olRueRec;
	CCSPtrArray <RecordSet> olRudRecs;
	CString		olTpst, olTnam, olUtpl, olRusn, olRust, olUrue;
	CString		olUarc, olLine, olLstu, olRuty, olEvty, olTplUrno2 ;
	CWaitDlg	*polWaitDlg;
	char        clLine[192];
	CString		olLastRusn, olUseu ;
	bool		blTakeDuty;
	int			ilWaySum, ilWayT ;
	int			ilPrepT, ilPrepSum  ;
	int			ilDutySum, ilDutyT, i;
	CTime	olTime;

	olTnam = ogBCD.GetField("TPL", "URNO", ropTplUrno, "TNAM");

	olTime = CTime::GetCurrentTime();
	olLine.Format ("Checking rule history for template '%s'", olTnam );
	
	fp = fopen ( opFilePath, "w" );
	if ( !fp )
		return false;
	olTplUrno2 = GetCorrespTplUrno(ropTplUrno);
	polWaitDlg = new CWaitDlg (0, olLine );
	
	ogBCD.SetSort("RUE", "UTPL+,RUSN+,LSTU+", true );

	olLine.Format ( "WHERE UTPL in ('%s','%s')", ropTplUrno, olTplUrno2 );
	ogBCD.Read ( "RUD", olLine );					
	ilRudCount = ogBCD.GetDataCount("RUD");

	fprintf ( fp, "Template: %s          Report Date %s\n", olTnam, olTime.Format("%d.%m.%Y-%H:%M") );

	ilRueCount = ogBCD.GetDataCount("RUE");
	for ( ilRue=0; ilRue<ilRueCount; ilRue++ )
	{
		ogBCD.GetRecord ( "RUE", ilRue, olRueRec );
		olUtpl = olRueRec.Values[igRueUtplIdx];
		olRuty = olRueRec.Values[igRueRutyIdx];
		olEvty = olRueRec.Values[igRueEvtyIdx];
		olUseu = olRueRec.Values[igRueUseuIdx];
		if ( (olRuty!='0') && (olRuty!='2') )
			continue;
		if ( (olUtpl != ropTplUrno) && (olUtpl != olTplUrno2) )
			continue;
		olRusn = olRueRec.Values[igRueRusnIdx];
		olRust = olRueRec.Values[igRueRustIdx];
		olUarc = olRueRec.Values[igRueUarcIdx];
		olUrue = olRueRec.Values[igRueUrnoIdx];
		olLstu = olRueRec.Values[igRueLstuIdx];

		blTakeDuty = ( (olRuty=='2') || (olEvty!='0') );
		
		ogBCD.GetRecordsExt("RUD", "URUE", "RETY", olUrue, "100", &olRudRecs);
		ilRudCount = olRudRecs.GetSize();
		if ( olLastRusn != olRusn )
		{
			fprintf ( fp, "\n" );
			olLastRusn = olRusn;
		}
		ilWaySum = ilPrepSum = ilDutySum = 0;
		for ( i = 0; i<ilRudCount; i++ )
		{
			ilWayT = atoi(olRudRecs[i].Values[igRudTtgtIdx]) + atoi(olRudRecs[i].Values[igRudTtgfIdx]);
			ilWaySum += ilWayT ;
			ilPrepT = atoi(olRudRecs[i].Values[igRudSdtiIdx]) + atoi(olRudRecs[i].Values[igRudSutiIdx]);
			ilPrepSum += ilPrepT ;
			if ( blTakeDuty )
			{
				ilDutyT = atoi(olRudRecs[i].Values[igRudDeduIdx]) ;
				ilDutySum += ilDutyT;
				if (ilDutySum<0)
					blTakeDuty = false;
			}
		}
		ilWaySum /= 60;
		ilPrepSum /= 60;
		if (blTakeDuty )
		{
			ilDutySum /= 60;
			sprintf ( clLine, "%-14s  Changed: <%-14s/%s>  Res. <%2d>  Duty <%4d>  Prep. <%3d>  Way <%3d>  URNO %-10s", 
					  olRusn, olUseu, olLstu, ilRudCount, ilDutySum, ilPrepSum, ilWaySum, olUrue );
		}
		else
			sprintf ( clLine, "%-14s  Changed: <%-14s/%s>  Res. <%2d>  Duty <---->  Prep. <%3d>  Way <%3d>  URNO %-10s", 
					  olRusn, olUseu, olLstu, ilRudCount, ilPrepSum, ilWaySum, olUrue );
		fprintf ( fp, "%s\n", clLine );

		olRudRecs.DeleteAll();
	}
	fclose ( fp );
	
	/* read old values back to RUDTAB Object */
	olLine = "where URUE in (select urno from RUETAB where RUST != '2')";
	ilRue = ogBCD.Read("RUD", olLine );

	if ( polWaitDlg )	
	{
		polWaitDlg->CloseWindow();
		delete polWaitDlg;
	}
	
	return true ;
}

