// RegelWerk.h : main header file for the REGELWERK application
//

#if !defined(AFX_REGELWERK_H__5C27D144_B08A_11D2_AAE6_00001C018CF3__INCLUDED_)
#define AFX_REGELWERK_H__5C27D144_B08A_11D2_AAE6_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif


#include <resource.h>       // main symbols
#include <validitydlg.h>       
#include <cregex.h>


class RecordSet; //hag990827


/////////////////////////////////////////////////////////////////////////////
// CRegelWerkApp:
// See RegelWerk.cpp for the implementation of this class
//



class CRegelWerkApp : public CWinApp
{
public:
	CRegelWerkApp();
	~CRegelWerkApp();

//--- Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegelWerkApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual CDocument* OpenDocumentFile(LPCTSTR lpszFileName);
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL


//--- Implementation
public:
	void SetStatusBarText ( CString &ropText );
	void AddProcID(DWORD dwPID);

protected:
	void SetGlobalFieldIndices();
	void SetLogicalTableFields();

	void MakeChoiceLists();
	bool IsAlreadyInList(CString opList, CString opItem);


private:
	CCSPtrArray <DWORD> omProcList;

	void KillOtherProcesses();
	

	//{{AFX_MSG(CRegelWerkApp)
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


//--- Attributes
	

};

extern CRegelWerkApp theApp;

bool GetLocationCode ( CString &ropUrno, CString &ropReftStr, CString &ropLocCode );
bool ParseReftFieldEntry ( CString &ropReftEntry, CString &ropResTable, 
						   CString &ropResCodeField ) ;
int AddGroupsToChoiceList ( CString &ropChoiceList, CString &ropResTable ) ;
int FindGroupsWithResourceType ( CString opReqRes, CStringList &ropUrnoList );
bool IsResourceTypeInGroup ( CString opReqRes, CString opSgrUnro, 
							 CStringList &ropUrnoList, CStringList &ropInspUrnos );
bool GetGroupName ( RecordSet *popRecord, int ipGTabIndex, int ipResUrnoIndex,
					CString &ropGroupName );
void SetWhereValidString ( CString opTableName, CString &ropWhere );
bool DoValiditiesOverlap ( VALDATA &ropValData1, VALDATA &ropValData2 );
bool GetNameOfTSRRecord ( CString &ropUrno, CString &ropName );
bool FillNamesArray ( CString &ropUrno, CStringArray &ropNames );
bool GetStringForTSRTextEntry ( CString opEntry, CString &ropString );
bool IsAlreadyRunning();

void CreateResourceChoiceLists();
RES_CHOICELIST MakeList(CString opResTab, CString opResField);
CString GetChoiceList(CString opResTab, CString opResField, CString *popType=0 );
//bool SetChoiceList(CString opTab, CString opField, CString opChoiceList);
bool ChangeResChoiceLists ( CString opTable, RecordSet *popRecord, bool bpGroup, bool bpAdd );
bool DoesTableExist ( char *table );
RES_CHOICELIST MakeEquList( CString opResField, CString opType );
bool IsSingleResourceSelected ( CString &ropCode );

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGELWERK_H__5C27D144_B08A_11D2_AAE6_00001C018CF3__INCLUDED_)
