#if !defined(AFX_MULTIEDITDLG_H__8A5CA4E1_B5A8_11D6_96B1_005004BCC8AF__INCLUDED_)
#define AFX_MULTIEDITDLG_H__8A5CA4E1_B5A8_11D6_96B1_005004BCC8AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MultiEditDlg.h : header file
//
#include <ccsedit.h>
/////////////////////////////////////////////////////////////////////////////
// CMultiEditDlg dialog

class CMultiEditDlg : public CDialog
{
// Construction
public:
	CMultiEditDlg(CWnd* pParent = NULL);   // standard constructor
	~CMultiEditDlg();

// Dialog Data
	//{{AFX_DATA(CMultiEditDlg)
	enum { IDD = IDD_MULTIEDIT_DLG };
	CComboBox	omComboBox;
	//}}AFX_DATA
protected: 
	CGridFenster	*pomRulesGrid;
	CGridFenster	*pomServiceGrid;
	CString			omTplUrno ;
	int				imSortRueColum;
	bool			bmSortRueAscend;
	int				imSortSerColum;
	bool			bmSortSerAscend;
	CRowColArray	omSelRueLines;
	CRowColArray	omSelSerLines;
	CMapStringToOb	omSelRueUrnos;
	CMapStringToOb	omSelSerUrnos;
	bool			bmTimesChanged;
	bool			bmDutiesChanged;
	CString			omDefaultAloc;
	CStringList		omRueDutyChanges;
	CStringList		omRueTimeChanges;
	CCSEdit			omRulesListTxt;
	CCSEdit			omServiceListTxt;
	CCSEdit			omRulesTxt;
	CCSEdit			omServiceTxt;
	CCSEdit			omServiceTimes[5];
	CFont			omBigFont;
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMultiEditDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	int DoModal(CString &ropTplUrno);

protected:
	void SetStaticTexts();
	void FillRulesGrid();
	void IniComboBox();
	void FillServiceGrid();
	void DisplayOneRule (RecordSet *popRule);
	void SortRules();
	void SortServices();
	void UpdateUsedFlags();
	void UpdateRulesValues();
	void UpdateServiceValues();
	void EnableButtons();
	bool FillLogicTables();
	void UpdateSelUrnoMap ( CGridFenster *popGrid, CRowColArray &ropSelLines, 
							CMapStringToOb &ropSelUrnos, int ipUrnoIdx );
	bool SaveChanges();
	bool DoSave ();
	bool SaveUsedRuleInDB ( CString &ropRueUrno );
	bool UpdateRuleInDB ( CString &ropRueUrno );
//	bool InsertResForNewRud ( RecordSet &ropRudRecord );
	bool InsertResources ( CString opTable, CString opRefField, CString opRefValue );

	// Generated message map functions
	//{{AFX_MSG(CMultiEditDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnFdut();
	afx_msg void OnFsdt();
	afx_msg void OnFsut();
	afx_msg void OnFwtf();
	afx_msg void OnFwtt();
	afx_msg void OnSelchangeComboBox();
	afx_msg void OnGridClick(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridSelEnd (WPARAM wParam, LPARAM lParam);
	afx_msg void OnSet();
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

void UpdateRudDispValues ( CString opRuleUrno, CString opTable, bool bpSave=false );

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MULTIEDITDLG_H__8A5CA4E1_B5A8_11D6_96B1_005004BCC8AF__INCLUDED_)
