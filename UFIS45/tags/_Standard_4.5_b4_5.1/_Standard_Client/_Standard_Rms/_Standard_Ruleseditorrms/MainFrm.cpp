// MainFrm.cpp : implementation of the CMainFrame class
//

#include <stdafx.h>
#include <AFXPRIV.H>
#include <RegelWerk.h>
#include <ccsglobl.h>
#include <MainFrm.h>
#include <RulesList.h>
#include <FlightIndepRulesView.h>
#include <CollectiveRulesView.h>
#include <RegelWerkView.h>
#include <CedaBasicData.h>
#include <Dialog_VorlagenEditor.h>
#include <process.h>
#include <CCSTime.h>
#include <NewDlg.h>
#include <MultiEditDlg.h>
#include <Dialog_Neuer_Name.h>
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif




//-------------------------------------------------------------------------------------------------------------------
//					statics, defines, etc.
//-------------------------------------------------------------------------------------------------------------------
#define COMBO_BOX_WIDTH  180 
#define TOGGLEBUTTON_WIDTH  100 

//-------------------------------------------------------------------------------------------------------------------

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};



//-------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//-------------------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)


CMainFrame::CMainFrame()
{
	CCS_TRY
	
	bmNewFlag = true;
	bmIsValChanged = false;
	bmIsSCRule = true;
	bmFirstTimeInit = true;
	bmIsGridColWidthsSaved = false;

	//-- validity data
	CString olTmp;
	CTime olTime;

	olTime = CTime::GetCurrentTime();
	olTmp = olTime.Format("%Y%m%d%H%M%S");
	strcpy(rmValData.Vafr, olTmp);


	rmValData.Vato[0] = '\0';
	rmValData.FixF[0] = '\0';
	rmValData.FixT[0] = '\0';
	rmValData.Vtyp[0] = '\0';
	strcpy(rmValData.Freq, CString("1111111"));

	ilLastExtStart = GetTickCount();
	bmMultiEditAllowed = false;
	bmRuleHistoryAllowed = false;

	//
	ilEvty=-1;
	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

CMainFrame::~CMainFrame()
{
	if (rmValData.Excls.GetSize() > 0)
	{
		rmValData.Excls.DeleteAll();
	}
}



//-------------------------------------------------------------------------------------------------------------------
//					message map
//-------------------------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_COMMAND(ID_HELP_FINDER, CFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CFrameWnd::OnHelpFinder)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_CONTRACT_BTN, OnContractBtn)
	ON_COMMAND(VALIDITY, OnValidityBtn)
	ON_COMMAND(ID_FILE_SAVE, OnSaveRule)
	ON_COMMAND(ID_FILE_NEW, OnNewRule)
	ON_COMMAND(MENU_OPT_STATGRP, OnGroupBtn)
	ON_COMMAND(MENU_OPT_SERVLIST, OnLeistungen)
	ON_COMMAND(ID_FILE_OPEN, OnLoadRule)
	ON_COMMAND(ID_VORLAGEN_EDITOR, OnVorlagenEditor)
	ON_CBN_SELENDOK(IDC_SNAP_COMBO, OnSelEndOk)
	ON_COMMAND(ID_EDIT_COPY, OnCopy)
	ON_COMMAND(ID_FILE_DELETE, OnDelete)
	ON_COMMAND(MENU_VIEW_FLIGHTRELATED, OnViewFlightrelated)
	ON_COMMAND(MENU_VIEW_INDEP, OnViewIndep)
	ON_COMMAND(MENU_VIEW_COLLECTIVE, OnViewCollective)
	ON_COMMAND(ID_FILE_SERVICES, OnServicesEditor)
	ON_WM_CLOSE()
	ON_MESSAGE(WM_RULE_SAVED_OK, OnRuleSavedOk)
	ON_MESSAGE(WM_RULE_MODIFIED, OnRuleModified)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateButtons)
	ON_UPDATE_COMMAND_UI(ID_FILE_SERVICES, OnUpdateFileServices)
	ON_UPDATE_COMMAND_UI(MENU_OPT_STATGRP, OnUpdateOptStatgrp)
	ON_COMMAND(ID_EDIT_BATCH, OnMultiEdit)
	ON_UPDATE_COMMAND_UI(ID_EDIT_BATCH, OnUpdateEditBatch)
	ON_WM_HELPINFO()
	ON_UPDATE_COMMAND_UI(ID_FILE_DELETE, OnUpdateButtons)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateButtons)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateButtons)
	ON_UPDATE_COMMAND_UI(MENU_OPT_SERVLIST, OnUpdateButtons)
	ON_COMMAND(ID_FILE_HISTORY, OnFileHistory)
	ON_UPDATE_COMMAND_UI(ID_FILE_HISTORY, OnUpdateFileHistory)
	//}}AFX_MSG_MAP
	// toolbar "tooltip" notification
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
END_MESSAGE_MAP()



//-------------------------------------------------------------------------------------------------------------------
//					creation
//-------------------------------------------------------------------------------------------------------------------
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CFrameWnd::PreCreateWindow(cs))
		return FALSE;
	return TRUE;
}
//-------------------------------------------------------------------------------------------------------------------

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CCS_TRY

	this->SetIcon(AfxGetApp()->LoadIcon(IDI_UFIS) , FALSE);

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	if (m_wndToolBar.Create(this, RBS_FIXEDORDER, 
				WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|CBRS_TOP,
				AFX_IDW_TOOLBAR ) )
	{
		//--- AddToggleButton
		
		// get index
		CRect olRect(0,2,50,27);
		/*m_wndToolBar.pomToggleButton = m_wndToolBar.AddButton ( GetString(1004), BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE, 
												   olRect, &m_wndToolBar, IDC_TOGGLEBUTTON );
		if ( !m_wndToolBar.pomToggleButton )
		{
			TRACE0("Problem with Toggle Button!!");
			return FALSE;
		};*/


		//--- Add ComboBox
		//olRect.left = 55;
		//olRect.right = 280;
		olRect.left = 0;
		olRect.right = 225;
		olRect.bottom = 245;
		m_wndToolBar.pomComboBox = m_wndToolBar.AddComboBox ( WS_CHILD | WS_VISIBLE | WS_VSCROLL | CBS_AUTOHSCROLL | CBS_DROPDOWNLIST | CBS_HASSTRINGS, 
															 olRect, &m_wndToolBar, IDC_SNAP_COMBO  );
		
		if ( !m_wndToolBar.pomComboBox )
		{
			TRACE0("Problem with Combo Box!!");
			return FALSE;
		}

		if ( !m_wndToolBar.LoadToolBar (IDR_MAINFRAME) )
		{
			TRACE0("Failed to create toolbar\n");
			return -1;      // fail to create
		}
	}
	char clEntry[21];
	GetPrivateProfileString ( "REGELWERK", "DISPLAY_MULTIEDIT", "NO", clEntry, 
							  20, pcgConfigPath);
	if ( !strcmp ( clEntry, "YES" ) )
		bmMultiEditAllowed = true;

	GetPrivateProfileString ( "REGELWERK", "DISPLAY_RULEHISTORY", "NO", clEntry, 
							  20, pcgConfigPath);
	if ( !strcmp ( clEntry, "YES" ) )
		bmRuleHistoryAllowed = true;

	//added by MAX
	char myEntry[21];
	bgUseValidityType = false;
	GetPrivateProfileString ( "REGELWERK", "USE_VALIDITY_TYPE", "NO", myEntry, 
							  20, pcgConfigPath);
	if ( !strcmp ( myEntry, "YES" ) )
		bgUseValidityType = true;

	TranslateMenu ();
	m_wndToolBar.pomToolBar->CalculateSizes ();
	m_wndToolBar.pomToolBar->RedrawWindow ();

	ogDdx.Register(this, RUE_NEW, CString("MainFrm"), CString("RUE New"), ProcessRueChange );
	ogDdx.Register(this, RUE_CHANGE, CString("MainFrm"), CString("RUE Change"), ProcessRueChange );
	ogDdx.Register(this, RUE_DELETE, CString("MainFrm"), CString("RUE Delete"), ProcessRueChange );

	CCS_CATCH_ALL

	return 0;
}



//-------------------------------------------------------------------------------------------------------------------
//					message handling
//-------------------------------------------------------------------------------------------------------------------
/*void CMainFrame::OnTogglebutton() 
{
	CCS_TRY


	// check for changes
	if (IsContinue() == false)	
	{
		return;
	}

	if (m_wndToolBar.pomToggleButton != NULL)
	{
		CString olText;
		m_wndToolBar.pomToggleButton->GetWindowText(olText);
		
		CWnd* polStatusBar = GetMessageBar();
		polStatusBar->SetWindowText(CString(""));

		if (olText == GetString(1003))	//--- set to single/collective rule
		{	
			if (SwitchToView(FLIGHTRELATED) == true)
			{
				m_wndToolBar.pomToggleButton->SetWindowText(GetString(1004));
				SetWindowText(GetString(1414));
			}
		}
		else if (olText == GetString(1004))			//--- set to flight independent
		{
			if (SwitchToView(INDEPENDENT) == true)
			{
				m_wndToolBar.pomToggleButton->SetWindowText(GetString(1003));
				SetWindowText(GetString(1413));
			}
		}

		OnNewRule();
	}


	CCS_CATCH_ALL
}*/
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnContractBtn() 
{
	MessageBox(GetString(1486));
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnValidityBtn() 
{
	CCS_TRY

	ValidityDlg olDlg(GetRueUrno(), bmNewFlag, this);
	//add by MAX
	//--- get rule event type
	CView *polActiveView = GetActiveView();
	CString olCurrRueUrno;
	if (polActiveView && 
		polActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView) )
	   )
	{
	   olCurrRueUrno = ((CRulesFormView*)polActiveView)->omCurrRueUrno;
	}
	CString olEvty;
	CString olRuty;
	olEvty = ogBCD.GetField("RUE", "URNO", olCurrRueUrno, "EVTY");
	olRuty = ogBCD.GetField("RUE", "URNO", olCurrRueUrno, "RUTY");
	if (atoi(olEvty) == 0 && atoi(olRuty) == 0)
	{
		rmValData.IsTurnaround = true;
	}
	else
		rmValData.IsTurnaround = false;

	olDlg.SetValData(&rmValData);
	
	// open validity dialog
	if (olDlg.DoModal() == IDOK)
	{
		olDlg.GetValData(rmValData);
	}

	bmIsValChanged = true;	// flag will be evaluated on saving this rule


	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnSaveRule() 
{
	CCS_TRY
	
	
	CView *polActiveView = GetActiveView();

	if (polActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
	{
		CRegelEditorFormView *polView = (CRegelEditorFormView*) polActiveView;
		polView->OnSaveRule();
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
	{
		FlightIndepRulesView *polView = (FlightIndepRulesView*) polActiveView;
		polView->OnSaveRule();
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(CollectiveRulesView)))
	{
		CollectiveRulesView *polView = (CollectiveRulesView*) polActiveView;
		polView->OnSaveRule();
	}
	bmNewFlag = false;
	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnRuleSavedOk(WPARAM wParam, LPARAM lParam)
{
	bmIsRuleSavedOk = true;
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnNewRule(bool bpShowNewDlg/*=true*/) 
{
	CCS_TRY
	

	// check for changes
	if (IsContinue() == false)	
	{
		return;
	}
	
	//ogChangeInfo.SetChangeState(RULE_NEW);


	//-- validity data
	CString olTmp;
	CTime	olTime;
	int		ilRet = IDOK;
	olTime = CTime::GetCurrentTime();
	olTmp = olTime.Format("%Y%m%d%H%M%S");
	strcpy(rmValData.Vafr, olTmp);
	strcpy(rmValData.Vato, "" );

	rmValData.Excls.DeleteAll();
	rmValData.FixF[0] = '\0';
	rmValData.FixT[0] = '\0';
	rmValData.Vtyp[0] = '\0';
	strcpy(rmValData.Freq, CString("1111111"));


	NewDlg *polNewDlg = new NewDlg;
	CView *polActiveView = GetActiveView();

	if (polActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
	{
		polNewDlg->m_WhatNew = 0;
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
	{
		polNewDlg->m_WhatNew = 2;
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(CollectiveRulesView)))
	{
		polNewDlg->m_WhatNew = 1;
	}

	if ( bpShowNewDlg )
		ilRet = polNewDlg->DoModal() ;
	else
		ilRet = IDOK;
	if ( ilRet == IDOK )
	{
		switch (polNewDlg->m_WhatNew)
		{
			case 0:	// SINGLE FLIGHTRELATED RULE
				SwitchToView(FLIGHTRELATED);
				break;

			case 1:	// COLlECTIVE RULE
				SwitchToView(COLLECTIVE);
				break;

			case 2: // FLIGHT INDEPENDENT RULE
				SwitchToView(INDEPENDENT);				
				break;
		}

		bmNewFlag = true;

		CView *polActiveView = GetActiveView();
		
		if (polActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView)))
		{
			CRulesFormView *polView = (CRulesFormView*) polActiveView;
			polView->OnNewRule();
		}
	}
	delete polNewDlg;
	ogChangeInfo.SetChangeState(RULE_NEW);

	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

// calling external exe (static grouping)
//		The binary needs its own section in ceda.ini !
void CMainFrame::OnGroupBtn() 
{
	CCS_TRY
	
	
	char pclGroupingPath[500];
	GetPrivateProfileString("REGELWERK", "GROUPING", "NOTFOUND", pclGroupingPath, sizeof pclGroupingPath, pcgConfigPath);

	if(!strcmp(pclGroupingPath,"NOTFOUND") || strlen(pclGroupingPath) <= 0)
	{
		MessageBox(GetString(1463), GetString(AFX_IDS_APP_TITLE), MB_ICONSTOP);
	}
	else
	{
		//  prevent external exe to start twice (eg. doublecklicking the icon):
		//	timespan between starts must exceed 1000 ms
		DWORD now = GetTickCount();
		if (abs(now-ilLastExtStart) < 1000)
		{
			return;
		}

		
		char pclArgStr[256];
		//char *pclArgs[3];														//  will be eaten by _spawnv()

		_flushall(); // flush all streams before system call

		CString	olTplName;

		//	if (GetSelectedTplUrno(*m_wndToolBar.pomComboBox, olTplUrno, blShowIndep))
		m_wndToolBar.pomComboBox->GetWindowText ( olTplName );

		if ( !olTplName.IsEmpty() )
			sprintf(pclArgStr, "%s %s %s %s \"%s\"", pclGroupingPath, ogAppName, pcgUser, pcgPasswd, olTplName );	
		else
			sprintf(pclArgStr, "%s %s %s %s", pclGroupingPath, ogAppName, pcgUser, pcgPasswd);	// The first parameter ("CHILD")
		

		/*pclArgs[0] = pclArgStr;
		pclArgs[1] = NULL;
		pclArgs[2] = NULL;*/
		
		ilLastExtStart = now;

		// initialize STARTUPINFO struct
		STARTUPINFO Info;
		ZeroMemory(&Info, sizeof(Info));
		Info.dwFlags = STARTF_FORCEONFEEDBACK; // changes mouse cursor 
		Info.cb = sizeof(Info);

		// initialize PROCESS_INFORMATION struct
		PROCESS_INFORMATION *pProcInfo = new PROCESS_INFORMATION;
		ZeroMemory(pProcInfo, sizeof(pProcInfo));

		BOOL ilRet = CreateProcess(NULL,pclArgStr,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE,NULL,NULL,&Info,pProcInfo);
		if (ilRet == FALSE)
		{
			DWORD dwErr = GetLastError();
			char pclErrMsg[1024], pclTmp[1024];
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,dwErr,0,pclTmp,sizeof(pclTmp),NULL);
			sprintf(pclErrMsg, "%s %s %s\n%s", GetString(300), pclGroupingPath, GetString(301), pclTmp);
			AfxMessageBox(pclErrMsg);
		}
		else
		{
			DWORD dwPID = pProcInfo->dwProcessId;
			theApp.AddProcID(dwPID);
		}

		delete pProcInfo;
	}


	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnLeistungen() 
{
	CCS_TRY
	
	
	if (bgIsServicesOpen == false)	// don't open it twice
	{
		CView *polActiveView = GetActiveView();

		if (polActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
		{
			bgIsServicesOpen = true;
			CRegelEditorFormView *polView = (CRegelEditorFormView*) polActiveView;
			polView->OnLeistungen();
		}
		else if (polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
		{
			bgIsServicesOpen = true;
			FlightIndepRulesView *polView = (FlightIndepRulesView*) polActiveView;
			polView->OnLeistungen();
		}
		else if (polActiveView->IsKindOf(RUNTIME_CLASS(CollectiveRulesView)))
		{
			bgIsServicesOpen = true;
			CollectiveRulesView *polView = (CollectiveRulesView*) polActiveView;
			polView->OnLeistungen();
		}
	}


	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------


// calling external exe (services editor)
void CMainFrame::OnServicesEditor() 
{
	CCS_TRY
	
	
	char pclServicesPath[500];
    GetPrivateProfileString("REGELWERK", "SERVICES", "NOTFOUND", pclServicesPath, sizeof pclServicesPath, pcgConfigPath);

	if(!strcmp(pclServicesPath,"NOTFOUND") || strlen(pclServicesPath) <= 0)
	{
		MessageBox(GetString(1512), GetString(AFX_IDS_APP_TITLE), MB_ICONSTOP);
	}
	else
	{
		DWORD now = GetTickCount();
		//  wenn zw. dem letzten Versuch ein externes Programm zu starten und 
		//	jezt weniger als 1000 sec. liegen, nicht nochmal probieren 
		//	(event. Doppelklick!!)
		if ( abs(now-ilLastExtStart) < 1000 )
			return;
		_flushall(); // flush all streams before system call
		char pclArgStr[256];
		sprintf(pclArgStr, "%s %s %s %s", pclServicesPath, ogAppName, pcgUser, pcgPasswd);
		char *pclArgs[3];
		pclArgs[0] = pclArgStr;	// This first parameter will be eaten by _spawnv(). 
		pclArgs[1] = NULL;
		pclArgs[2] = NULL;
		ilLastExtStart = now;
		
		// initialize STARTUPINFO struct
		STARTUPINFO Info;
		ZeroMemory(&Info, sizeof(Info));
		Info.dwFlags = STARTF_FORCEONFEEDBACK; // changes mouse cursor 
		Info.cb = sizeof(Info);
		
		// initialize PROCESS_INFORMATION struct
		PROCESS_INFORMATION *pProcInfo = new PROCESS_INFORMATION;
		ZeroMemory(pProcInfo, sizeof(pProcInfo));
		
		BOOL ilRet = CreateProcess(NULL,pclArgStr,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE,NULL,NULL,&Info,pProcInfo);
		if (ilRet == FALSE)
		{
			DWORD dwErr = GetLastError();
			char pclErrMsg[1024], pclTmp[1024];
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,dwErr,0,pclTmp,sizeof(pclTmp),NULL);
			sprintf(pclErrMsg, "%s %s %s\n%s", GetString(300), pclServicesPath, GetString(301), pclTmp);
			AfxMessageBox(pclErrMsg);
		}
		else
		{
			DWORD dwPID = pProcInfo->dwProcessId;
			theApp.AddProcID(dwPID);
		}

		delete pProcInfo;
	}


	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnLoadRule() 
{
	CCS_TRY

	// check for changes
	if (IsContinue() == false)	
	{
		return;
	}

	//ogChangeInfo.SetChangeState(RULE_CHECK);


	bool blShowIndep = false;
	CString olRuleUrno;
	CView *polActiveView = GetActiveView();

	if (polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
		blShowIndep = true;
	
	//--- open dialog with list of rules		
	RulesList olDialog(blShowIndep, this);
	
	long llTplUrno = -1;
	CString	olTplUrno;

	if (GetSelectedTplUrno(*m_wndToolBar.pomComboBox, olTplUrno, blShowIndep))
	{
		llTplUrno = atol(olTplUrno);
	}

	if (olDialog.DoModal(GetRueUrno(), llTplUrno) == IDOK)
	{
		// some initialization stuff
		CString olRueUrno;
		CString olTplName;
		olRueUrno = olDialog.omDataStringRuleUrno;
		olTplName = olDialog.omTemplateName;

		DoLoadRule(olRueUrno, olTplName);
		ogChangeInfo.SetChangeState(RULE_CHECK);
	}
	

	CCS_CATCH_ALL	
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnSelEndOk()
{
	CCS_TRY

	// check for changes
	if (IsContinue() == false)	
	{
		return;
	}
	
	ogChangeInfo.SetChangeState(RULE_NEW);
	//  Create Resource choice lists for actual template again

	theApp.DoWaitCursor( 1 );
	ogResChoiceLists.DeleteAll();
	CreateResourceChoiceLists();	
	igRuleTopRow = 1;

	theApp.DoWaitCursor( -1 );

	CView *polActiveView = GetActiveView();

	if (polActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
	{
		CRegelEditorFormView *polView = (CRegelEditorFormView*) polActiveView;
		polView->OnSelEndOK();
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
	{
		FlightIndepRulesView *polView = (FlightIndepRulesView*) polActiveView;
		polView->OnSelEndOK();
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(CollectiveRulesView)))
	{
		CollectiveRulesView *polView = (CollectiveRulesView*) polActiveView;
		polView->OnSelEndOK();
	}


	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnVorlagenEditor() 
{
	CCS_TRY

	// check for changes
	if (IsContinue() == false)	
	{
		return;
	}

	CView *polActiveView = GetActiveView();

	if (polActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
	{
		CPasswordDlg olDlg;

		if ( olDlg.DoModal() == IDOK )
		{
			CRegelEditorFormView *polView = (CRegelEditorFormView*) polActiveView;
			polView->OnTemplateEditor();
		}
	}


	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnCopy() 
{
	CCS_TRY


	// check for changes
	if (IsContinue() == false)	
	{
		return;
	}

	ogChangeInfo.SetChangeState(RULE_COPY);


	CView *polActiveView = GetActiveView();

	if (polActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
	{
		CRegelEditorFormView *polView = (CRegelEditorFormView*) polActiveView;
		polView->OnCopy();
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
	{
		FlightIndepRulesView *polView = (FlightIndepRulesView*) polActiveView;
		polView->OnCopy();
	}
	else if (polActiveView->IsKindOf(RUNTIME_CLASS(CollectiveRulesView)))
	{
		CollectiveRulesView *polView = (CollectiveRulesView*) polActiveView;
		polView->OnCopy();
	}


	CCS_CATCH_ALL
}

//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnDelete() 
{
	CCS_TRY

	int ilChoice = MessageBox(GetString(1508), GetString(AFX_IDS_APP_TITLE), MB_YESNO);
	if (ilChoice == IDYES)
	{
		// get urno of current rule
		CString olCurrRueUrno;
		CView *polActiveView = GetActiveView();
		if (polActiveView && 
			polActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView) )
		   )
		{
		   olCurrRueUrno = ((CRulesFormView*)polActiveView)->omCurrRueUrno;
		}
		// set change info
		ogChangeInfo.SetChangeState(RULE_DELETE);
		
		OnNewRule (false);			//  Damit alle angezeigten Felder im View wieder zur�ckgesetzt werden  
		bool blOk = ogDataSet.DeleteOneRule (olCurrRueUrno);
	}


	CCS_CATCH_ALL
}
//-------------------------------------------------------------------------------------------------------------------

/*void CMainFrame::OnAppAbout() 
{
	// TODO: Add your command handler code here
	
}*/
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnViewFlightrelated() 
{
	SwitchToView(FLIGHTRELATED);
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnViewIndep() 
{
	SwitchToView(INDEPENDENT);
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnViewCollective() 
{
	SwitchToView(COLLECTIVE) ;
}
//-------------------------------------------------------------------------------------------------------------------

CString CMainFrame::GetRueUrno()
{
	CString olReturn;
	
	CCS_TRY


/*	if (bmNewFlag == false)
	{*/
		CView *polActiveView = GetActiveView();

		if (polActiveView && 
			polActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView) ) &&
			(((CRulesFormView*)polActiveView)->bmNewFlag == false)  
		   )
		{
		   olReturn = ((CRulesFormView*)polActiveView)->omCurrRueUrno;
		}
	/*}*/
	else
	{
		olReturn = CString("-1");
	}


	CCS_CATCH_ALL


	return olReturn;
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::GetMessageString(UINT nID, CString& rMessage) const
{
	rMessage = GetString(nID);
	// first newline terminates actual string
	rMessage.Replace ( '\n', '\0' );
}



//-------------------------------------------------------------------------------------------------------------------
//					switch views
//-------------------------------------------------------------------------------------------------------------------
bool CMainFrame::SwitchToView(eView nView)
{
	bool blRet = true;
	int ilViewType;

	CCS_TRY

	if ( GetViewType() == nView )
		return true;		/*  actual view already has required type */

	// check for changes
	if (IsContinue() == false)	
	{
		return false;
	}

	CView* pOldActiveView = GetActiveView();
	CView* pNewActiveView = (CView*) GetDlgItem(nView);

	if (pNewActiveView == NULL) 
	{
		switch (nView) 
		{
			case FLIGHTRELATED:
				pNewActiveView = (CView*) new CRegelEditorFormView;
				break;

			case INDEPENDENT:
				pNewActiveView = (CView*) new FlightIndepRulesView;
				break;

			case COLLECTIVE:
				pNewActiveView = (CView*) new CollectiveRulesView;
				break;
		}
		
		CCreateContext context;
		context.m_pCurrentDoc = pOldActiveView->GetDocument();
		pNewActiveView->Create(NULL, NULL, WS_BORDER | WS_CHILD, CFrameWnd::rectDefault, this, nView, &context);
		pNewActiveView->OnInitialUpdate();
	}
	else
		if (pNewActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView)))
		{
			((CRulesFormView*)pNewActiveView)->OnNewRule();
		}


	if (pOldActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
	{
		CRegelEditorFormView *polView = (CRegelEditorFormView*) pOldActiveView;
		polView->SetExclusionRule(false);
		polView->CloseDetailWindows();
	}
		
	SetActiveView(pNewActiveView);
	pNewActiveView->ShowWindow(SW_SHOW);
	pOldActiveView->ShowWindow(SW_HIDE);

	ilViewType = GetViewType (pOldActiveView);
	if ( ilViewType >= 0 )
	{
		pOldActiveView->SetDlgCtrlID(ilViewType);
	}
	pNewActiveView->SetDlgCtrlID(AFX_IDW_PANE_FIRST);
	
	if ( nView == INDEPENDENT )
		SetWindowText(GetString(1413));
	else if ( nView == COLLECTIVE )
		SetWindowText(GetString(1415));
	else
		SetWindowText(GetString(1414));

	RecalcLayout();
	
	CCS_CATCH_ALL

	return blRet;
}

//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::UpdateViewArea()
{
	UpdateWindow();
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::FillTemplateCombo()
{
	CString olName, olUrno, olTpst, olTmp;
	int  ilRecordCount = ogBCD.GetDataCount("TPL");
	int	 ilPos ;

	for(int i = 0; i < ilRecordCount; i++)
	{
 		olName = olTmp = ogBCD.GetField("TPL", i, "TNAM");
        olUrno = ogBCD.GetField("TPL", i, "URNO");
		olTpst =  ogBCD.GetField("TPL", i,"TPST");

		olTmp += CString(" ") + GetString(303);  /* Name (deactivated) */

		if (( olTpst != '2' ) && (m_wndToolBar.pomComboBox->FindStringExact(-1, olTmp) == CB_ERR) &&
			(m_wndToolBar.pomComboBox->FindStringExact(-1, olName) == CB_ERR))
		{	
			if(ogPrivList.GetStat(olName) != '-')
			{
				if ( olTpst == '1' )
					ilPos = m_wndToolBar.pomComboBox->InsertString(0, olName);
				else
					ilPos = m_wndToolBar.pomComboBox->InsertString(0, olTmp);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------------------------------

bool CMainFrame::DoLoadRule(CString &ropRuleUrno, CString &ropTplName)
{
	bmNewFlag = false;

	// set combo box
	int ilOldPos = m_wndToolBar.pomComboBox->GetCurSel();
	/* int ilPos = m_wndToolBar.pomComboBox->FindStringExact(0, ropTplName); */
	int ilPos = FindTplInComboBox(ropTplName );

	// If selection in combo didn't change: set flag  (will be evaluated in InitializeTable())
	bool blNoComboChange;
	if (ilPos != -1 && ilOldPos == ilPos)
	{
		blNoComboChange = true;
	}
	else
	{
		blNoComboChange = false;
		m_wndToolBar.pomComboBox->SetCurSel(ilPos);

		theApp.DoWaitCursor( 1 );
		ogResChoiceLists.DeleteAll();
		CreateResourceChoiceLists();	
		theApp.DoWaitCursor( -1 );
	}
		 
	// Call functions in views
	LoadValidity(ropRuleUrno,rmValData);

	//add by MAX
	//--- get rule event type
	CString olEvty;
	olEvty = ogBCD.GetField("RUE", "URNO", ropRuleUrno, "EVTY");
	if (atoi(olEvty) == 0)
	{
		rmValData.IsTurnaround = true;
	}
	else
		rmValData.IsTurnaround = false;
	 	

	//--- get rule type		(0 = single, 1 = collect, 2 = independent)
	CString olRuty;
	olRuty = ogBCD.GetField("RUE", "URNO", ropRuleUrno, "RUTY");
	int ilRuty = atoi(olRuty);

	CView *polActiveView;
	polActiveView = GetActiveView();

	if ( (ilRuty >= 2 ) && ( ilRuty <= 3) )	// flight independent
	{
		if (!polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
		{
			SwitchToView(INDEPENDENT);
			polActiveView = GetActiveView();
		}

		FlightIndepRulesView *polView = (FlightIndepRulesView*) polActiveView;
		polView->OnLoadRule(ropRuleUrno, ropTplName, blNoComboChange);
	}
	else if (ilRuty == 0)			// single  rule
	{
		if (!polActiveView->IsKindOf(RUNTIME_CLASS(CRegelEditorFormView)))
		{
			SwitchToView(FLIGHTRELATED);
			polActiveView = GetActiveView();
		}

		CRegelEditorFormView *polView = (CRegelEditorFormView*) polActiveView;
		polView->OnLoadRule(ropRuleUrno, ropTplName, blNoComboChange);
	}
	else if (ilRuty == 1)			// collective rule
	{
		if (!polActiveView->IsKindOf(RUNTIME_CLASS(CollectiveRulesView)))
		{
			SwitchToView(COLLECTIVE);
			polActiveView = GetActiveView();
		}

		CollectiveRulesView *polView = (CollectiveRulesView*) polActiveView;
		polView->OnLoadRule(ropRuleUrno, ropTplName, blNoComboChange);
	}
	else
	{
		ogLog.Trace("DEBUG", "[MainFrm::DoLoadRule]  Invalid rule type (%d).", ilRuty);
		return false;
	}
	return true;
}
//-------------------------------------------------------------------------------------------------------------------

BOOL CMainFrame::OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);

	// need to handle both ANSI and UNICODE versions of the message
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	CString strTipText;
	UINT nID = pNMHDR->idFrom;
	if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
		pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
	{
		// idFrom is actually the HWND of the tool
		nID = ::GetDlgCtrlID((HWND)nID);
	}

	if (nID != 0) // will be zero on a separator
	{
		CString olMsg;
		// don't handle the message if no string resource found
		olMsg = GetString ( nID ) ;
		if ( olMsg.IsEmpty() && !olMsg.LoadString(nID) )
			return FALSE;

		// this is the command id, not the button index
		AfxExtractSubString(strTipText, olMsg, 1, '\n');
	}
#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#endif
	*pResult = 0;

	// bring the tooltip window above other popup windows
	::SetWindowPos(pNMHDR->hwndFrom, HWND_TOP, 0, 0, 0, 0,
		SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOMOVE|SWP_NOOWNERZORDER);

	return TRUE;    // message was handled
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::OnClose() 
{
	int ilRet = IDYES;
	CView *polActiveView = GetActiveView();

	if ( polActiveView && 
		 (polActiveView->GetRuntimeClass() == RUNTIME_CLASS(CPreviewView) ) )
		 return;
	ilRet = MessageBox ( GetString(IDS_ASK_FOR_EXIT), GetString(2005),
						 MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 );
	if ( ilRet == IDYES )
	{
		// check for changes
		if (IsContinue() == false)	
		{
			return;
		}	
		CFrameWnd::OnClose();
	}
}



//-------------------------------------------------------------------------------------------------------------------
//					some stuff (diagnostics)
//-------------------------------------------------------------------------------------------------------------------
#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG
//-------------------------------------------------------------------------------------------------------------------

bool CMainFrame::TranslateMenu ()
{
	HMENU			hMenu, hSubMenu;
	BOOL			ok=TRUE;
	CMenu *polMenu = GetMenu ();

	if ( !polMenu || !polMenu->m_hMenu )
		return false;

	hMenu = polMenu->m_hMenu;
	//  Submenu "File"
	hSubMenu = GetSubMenu ( hMenu, 0 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 0, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_FILE) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_NEW, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_NEW, GetString(IDS_M_FILE_NEW) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_OPEN, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_OPEN, GetString(IDS_M_FILE_OPEN) );
		ok &= ModifyMenu( hSubMenu, ID_EDIT_COPY, MF_STRING|MF_BYCOMMAND, 
						  ID_EDIT_COPY, GetString(IDS_M_EDIT_COPY) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_SAVE, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_SAVE, GetString(IDS_M_FILE_SAVE) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_DELETE, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_DELETE, GetString(IDS_M_FILE_DELETE) );
		ok &= ModifyMenu( hSubMenu, ID_EDIT_BATCH, MF_STRING|MF_BYCOMMAND, 
						  ID_EDIT_BATCH, GetString(IDS_M_FILE_BATCH) );
		//ok &= ModifyMenu( hSubMenu, ID_FILE_PRINT, MF_STRING|MF_BYCOMMAND, 
		//				  ID_FILE_PRINT, GetString(IDS_M_FILE_PRINT) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_PRINT_SETUP, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_PRINT_SETUP, GetString(IDS_M_FILE_PRINT_SETUP) );
		ok &= ModifyMenu( hSubMenu, ID_APP_EXIT, MF_STRING|MF_BYCOMMAND, 
						  ID_APP_EXIT, GetString(IDS_M_APP_EXIT) );

		if ( !bmMultiEditAllowed || ogPrivList.GetStat("ID_EDIT_BATCH") == '-' )
			RemoveMenu( hSubMenu, ID_EDIT_BATCH, MF_BYCOMMAND );
	}
	else 
		ok = false;

	//  Submenu "View"
	hSubMenu = GetSubMenu ( hMenu, 1 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 1, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_VIEW) );
		ok &= ModifyMenu( hSubMenu, ID_VIEW_TOOLBAR, MF_STRING|MF_BYCOMMAND, 
						  ID_VIEW_TOOLBAR, GetString(IDS_M_VIEW_TOOLBAR) );
		ok &= ModifyMenu( hSubMenu, ID_VIEW_STATUS_BAR, MF_STRING|MF_BYCOMMAND, 
						  ID_VIEW_STATUS_BAR, GetString(IDS_M_VIEW_STATUS_BAR) );
		ok &= ModifyMenu( hSubMenu, MENU_VIEW_INDEP, MF_STRING|MF_BYCOMMAND, 
						  MENU_VIEW_INDEP, GetString(IDS_M_VIEW_INDEP) );
		ok &= ModifyMenu( hSubMenu, MENU_VIEW_FLIGHTRELATED, MF_STRING|MF_BYCOMMAND, 
						  MENU_VIEW_FLIGHTRELATED, GetString(IDS_M_VIEW_FLIGHTRELATED) );
		ok &= ModifyMenu( hSubMenu, MENU_VIEW_COLLECTIVE, MF_STRING|MF_BYCOMMAND, 
						  MENU_VIEW_COLLECTIVE, GetString(IDS_M_VIEW_COLLECTIVE) );
	}
	else 
		ok = false;

	//  Submenu "Options"
	hSubMenu = GetSubMenu ( hMenu, 2 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 2, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_OPTIONS) );
		ok &= ModifyMenu( hSubMenu, MENU_OPT_STATGRP, MF_STRING|MF_BYCOMMAND, 
						  MENU_OPT_STATGRP, GetString(IDS_M_OPTIONS_STATGRP) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_SERVICES, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_SERVICES, GetString(IDS_M_OPTIONS_SERVICES) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_HISTORY, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_HISTORY, GetString(IDS_M_OPTIONS_HISTORY) );
		if ( ogPrivList.GetStat("ID_FILE_SERVICES") == '-' )
			RemoveMenu( hSubMenu, ID_FILE_SERVICES, MF_BYCOMMAND );

		if ( ogPrivList.GetStat("MENU_OPT_STATGRP") == '-' )
			RemoveMenu( hSubMenu, MENU_OPT_STATGRP, MF_BYCOMMAND );

		if ( !bmRuleHistoryAllowed )
			RemoveMenu( hSubMenu, ID_FILE_HISTORY, MF_BYCOMMAND );
	}
	else 
		ok = false;

	//  Submenu "Help"
	hSubMenu = GetSubMenu ( hMenu, 3 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 3, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_HELP) );
		ok &= ModifyMenu( hSubMenu, ID_HELP_FINDER, MF_STRING|MF_BYCOMMAND, 
						  ID_HELP_FINDER, GetString(IDS_M_HELPFINDER) );
		ok &= ModifyMenu( hSubMenu, ID_APP_ABOUT, MF_STRING|MF_BYCOMMAND, 
						  ID_APP_ABOUT, GetString(IDS_M_APP_ABOUT) );
	}
	else 
		ok = false;

	return (ok!=FALSE);
}
//-------------------------------------------------------------------------------------------------------------------

// The function tries to determine the change state. In case the rule changed, the user is asked
// if the changes should be saved first.
// OUT:     true   -   no changes OR user denied saving changes OR changes were saved successfully
//          false  -   changes could not be saved
bool CMainFrame::IsContinue()
{
	bool blRet = true;

	CView *polActiveView = GetActiveView();
	if ( polActiveView )			//hag010710: to be sure that active control	
		polActiveView->SetFocus();	//	gets a KILLFOCUS-message before saving

	if ( ! CanModifyActualTemplate () )
		return blRet;		/*  User is not allowed to modify rules in this template */
									
	if (ogChangeInfo.IsChanged() == true)
	{
		int ilRetCode = MessageBox(GetString(1574), GetString(2005), MB_YESNOCANCEL);
		switch (ilRetCode)
		{
		case IDYES:
				bmIsRuleSavedOk = false;
				// if rule is saved successfully a windows message (WM_RULE_SAVED_OK) will 
				// be received which causes bmIsRuleSavedOk to be set to true
				OnSaveRule();
				if (bmIsRuleSavedOk == false)
				{
					blRet = false;
				}
				break;

		case IDNO:
				// if "NO" is pressed, we don't save the rule. Changes are discarded and
				// bmIsRuleSavedOk is set to true. 
				bmIsRuleSavedOk = true;
				ogChangeInfo.SetChangeState(RULE_DELETE);	/* to prevent another question "Save Rule?" */
				break;

		case IDCANCEL:
				// We don't save and don't continue. Leave everything as it is.
				blRet = false;
				break;

		default:
				break;
		}
	}

	return blRet;
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::HideServicesListBtn(bool bpHide)
{
	m_wndToolBar.HideServicesListBtn(bpHide);
}
//-------------------------------------------------------------------------------------------------------------------

void CMainFrame::SaveGridColWidths(CGridFenster *popGrid)
{
	rmWidths.RemoveAll();
	for (int i = 0; i < popGrid->GetColCount(); i++)
	{
		rmWidths.Add(popGrid->GetColWidth(i));		
	}

	bmIsGridColWidthsSaved = true;
}
//----------------------------------------------------------------------------------------------------------------------

void CMainFrame::RestoreGridColWidths(CGridFenster *popGrid)
{
	ASSERT(rmWidths.GetSize() <= popGrid->GetColCount());

	if (bmIsGridColWidthsSaved)
	{
		for (int i = 0; i < popGrid->GetColCount(); i++)
		{
			popGrid->SetColWidth(i, i, rmWidths.GetAt(i));
		}
	}
}

int CMainFrame::GetViewType ( CView *popView /*=NULL*/)
{
	CView *polVw ;
	int ilRet=-1;

	polVw = (popView) ? popView : GetActiveView();

	if ( polVw )
	{
		if ( polVw->GetRuntimeClass() == RUNTIME_CLASS(FlightIndepRulesView) )
			ilRet = INDEPENDENT;
		else if ( polVw->GetRuntimeClass() == RUNTIME_CLASS(CollectiveRulesView) )
			ilRet = COLLECTIVE ;
		else if ( polVw->GetRuntimeClass() == RUNTIME_CLASS(CRegelEditorFormView) )
			ilRet = FLIGHTRELATED;
	}
	return ilRet; 
}
//----------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------


BOOL CMainFrame::CanModifyActualTemplate ()
{
	CString olTplName, olTplUrno;
	
	int ilSel = m_wndToolBar.pomComboBox->GetCurSel();
	if (GetSelectedTplUrno(*(m_wndToolBar.pomComboBox), olTplUrno, FALSE))
	{
		CString olTPLString;
		olTplName = ogBCD.GetField("TPL", "URNO", olTplUrno, "TNAM");
		return CanModifyTemplate ( olTplName );
	}	
	else
		return TRUE;
}


void CMainFrame::OnUpdateButtons(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	BOOL blEnabled = CanModifyActualTemplate ();
	pCmdUI->Enable ( blEnabled );
}


BOOL CanModifyTemplate ( CString opTplName )
{
	char clBdpsStat = '1';
	
	clBdpsStat = ogPrivList.GetStat(opTplName) ;
	if ( (clBdpsStat == '0') || (clBdpsStat == '-') )
		return FALSE;
	else
		return TRUE;
}


void CMainFrame::OnUpdateFileServices(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
	
	clBdpsStat = ogPrivList.GetStat("ID_FILE_SERVICES") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		m_wndToolBar.HideToolbarBtn(ID_FILE_SERVICES, true );

}

void CMainFrame::OnUpdateOptStatgrp(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
	
	clBdpsStat = ogPrivList.GetStat("MENU_OPT_STATGRP") ;
	if ( clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		m_wndToolBar.HideToolbarBtn(MENU_OPT_STATGRP, true );

}

void CMainFrame::OnMultiEdit() 
{
	CMultiEditDlg	olDlg (this);
	CString			olTplUrno, olTplName;
	CView *polActiveView = GetActiveView();
	CRulesFormView *polView =0;
	CCSPtrArray <RecordSet>	olRules;
	CString			olNewRuleUrno, olRueRusn, olRueRust;
	
	// check for changes
	if (IsContinue() == false)	
	{
		return;
	}
		
	if (polActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView)))
		polView = (CRulesFormView*) polActiveView;
	
	GetSelectedTplUrno(*m_wndToolBar.pomComboBox, olTplUrno, false );

	olDlg.DoModal(olTplUrno);
	
	// Restore original Rule
	if ( polView )
	{
		olTplName = ogBCD.GetField ( "TPL", "URNO", olTplUrno, "TNAM" );
		if ( !bmNewFlag )
		{	//  possibly the actual rule has been archived
			olRueRusn = ogBCD.GetField ( "RUE", "URNO", polView->omCurrRueUrno, "RUSN" );
			ogBCD.GetRecords ( "RUE", "RUSN", olRueRusn, &olRules );
			for ( int i=0; olNewRuleUrno.IsEmpty() && (i<olRules.GetSize()); i++ )
			{
				olRueRust = olRules[i].Values[igRueRustIdx];
				if ( (olRueRust == '0') || ( olRueRust == '1') )
					olNewRuleUrno = olRules[i].Values[igRueUrnoIdx];
			}
			olRules.DeleteAll();
		}
		if ( !olNewRuleUrno.IsEmpty() )
			DoLoadRule(olNewRuleUrno, olTplName);
		else
			polView->OnNewRule ();
	}
}

void CMainFrame::OnUpdateEditBatch(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	char clBdpsStat = '1';
	BOOL blEnable = TRUE;
	
	clBdpsStat = ogPrivList.GetStat("ID_EDIT_BATCH") ;
	if ( bmMultiEditAllowed && clBdpsStat != '-' ) 
	{
		if ( /*( clBdpsStat == '-' ) || */( clBdpsStat == '0' ) )
			blEnable = FALSE;
		pCmdUI->Enable ( blEnable );		
	}	
	else
		m_wndToolBar.HideToolbarBtn(ID_EDIT_BATCH, true );
	
}

static void ProcessRueChange( void *popInstance, int ipDDXType, 
							  void *vpDataPointer, CString &ropInstanceName)
{
    CMainFrame		*polMainFrm = (CMainFrame *)popInstance;
	
	if ( polMainFrm )
		polMainFrm->SendMessage ( WM_RULE_MODIFIED, ipDDXType, (LPARAM)vpDataPointer );
}


void CMainFrame::OnRuleModified(WPARAM wParam, LPARAM lParam)
{
	UINT			idMsg= 0;
	CString			olTplName, olMsg;
	CString			olCurrRueUrno, olModifiedUrno, olRuleToLoad, olRust ;
    CRulesFormView  *polRuleView ;
	RecordSet		*polRecord = (RecordSet*) lParam;
	CView			*polActiveView = 0;
    
	polActiveView = GetActiveView();
	
	if ( polRecord && polActiveView && 
		 polActiveView->IsKindOf(RUNTIME_CLASS(CRulesFormView) ) )  
	{
		polRuleView = (CRulesFormView*)polActiveView; 
		olCurrRueUrno = polRuleView->omCurrRueUrno;
	
		switch ( wParam )
		{

			case RUE_NEW:
				olModifiedUrno = polRecord->Values[igRueUarcIdx];
				if ( olModifiedUrno == olCurrRueUrno ) // rule has been archived and new version has been saved
				{
					idMsg = IDS_RUE_UPD_OUTSIDE;
					olRuleToLoad = polRecord->Values[igRueUrnoIdx];
				}
				break;
			case RUE_DELETE:
				olModifiedUrno = polRecord->Values[igRueUrnoIdx];
				if ( olModifiedUrno == olCurrRueUrno )
				{
					idMsg = IDS_RUE_DEL_OUTSIDE;
				}
				break;

			case RUE_CHANGE:
				olModifiedUrno = polRecord->Values[igRueUrnoIdx];
				if ( olModifiedUrno == olCurrRueUrno )
				{
					olRust = polRecord->Values[igRueRustIdx];
					if ( olRust == '2' )	// rule has been archived and deleted
						idMsg = IDS_RUE_DEL_OUTSIDE;
					else
					{
						idMsg = IDS_RUE_UPD_OUTSIDE;
						olRuleToLoad = olModifiedUrno;
					}
				}
				break;
		}
		if ( idMsg > 0 ) 
		{
			polRuleView->OnNewRule();
			ogChangeInfo.SetChangeState(RULE_NEW);
			olMsg = GetString ( idMsg );
			AfxMessageBox ( olMsg ); 
			if ( idMsg == IDS_RUE_UPD_OUTSIDE )
			{
				m_wndToolBar.pomComboBox->GetWindowText ( olTplName );
				DoLoadRule(olRuleToLoad, olTplName);
				ogChangeInfo.SetChangeState(RULE_CHECK);
			}
		}
	}	
}

/*	return index of template in templates combo box, no matter whether	*/
/*	deactivated or not													*/
int CMainFrame::FindTplInComboBox(CString &ropTplName )
{
	CString olStrInactive = CString(" ") + GetString(303);  /* Name (deactivated) */
	CString olTplName2; /* added or deleted olStrInactive to/from ropTplName */
	int ilRc = CB_ERR;

	olTplName2 = ropTplName;
	if ( olTplName2.Find ( olStrInactive ) > 0 )
		olTplName2.Replace ( olStrInactive, "") ;
	else
		olTplName2 += olStrInactive;
	ilRc = m_wndToolBar.pomComboBox->FindStringExact(-1, ropTplName);
	if ( ilRc < 0 )
		ilRc = m_wndToolBar.pomComboBox->FindStringExact(-1, olTplName2);
	return ilRc;
}

void CMainFrame::OnFileHistory() 
{
	// TODO: Add your command handler code here
	char pclTmp[128], pclTmp2[1024];
	bool blShowIndep = false;
	CString	olTplUrno;
	CView *polActiveView = GetActiveView();

	// TODO: Add your command handler code here
	GetPrivateProfileString ( ogAppName, "Report", CCSLog::GetTmpPath("\\RuleHistory.txt"), 
							  pclTmp, 128, pcgConfigPath);


	if (polActiveView->IsKindOf(RUNTIME_CLASS(FlightIndepRulesView)))
		blShowIndep = true;
	

	if (GetSelectedTplUrno(*m_wndToolBar.pomComboBox, olTplUrno, blShowIndep))
	{
		ogDataSet.CheckArchive ( pclTmp, olTplUrno );
		char pclArgStr[256], clEditor[128];

		_flushall(); // flush all streams before system call

		GetPrivateProfileString ( ogAppName, "EDITOR", "NOTEPAD.EXE", 
								  clEditor, 128, pcgConfigPath);
		sprintf(pclArgStr, "%s %s", clEditor, pclTmp );	
		
		// initialize STARTUPINFO struct
		STARTUPINFO Info;
		ZeroMemory(&Info, sizeof(Info));
		Info.dwFlags = STARTF_FORCEONFEEDBACK; // changes mouse cursor 
		Info.cb = sizeof(Info);

		// initialize PROCESS_INFORMATION struct
		PROCESS_INFORMATION *pProcInfo = new PROCESS_INFORMATION;
		ZeroMemory(pProcInfo, sizeof(pProcInfo));

		BOOL ilRet = CreateProcess(NULL,pclArgStr,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE,NULL,NULL,&Info,pProcInfo);
		if (ilRet == FALSE)
		{
			DWORD dwErr = GetLastError();
			char pclErrMsg[1024];
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,NULL,dwErr,0,pclTmp2,sizeof(pclTmp2),NULL);
			sprintf(pclErrMsg, "%s %s %s\n%s", GetString(300), clEditor, GetString(301), pclTmp2);
			AfxMessageBox(pclErrMsg);
		}
		else
		{
			DWORD dwPID = pProcInfo->dwProcessId;
			theApp.AddProcID(dwPID);
		}

		delete pProcInfo;
	}

		
}

void CMainFrame::OnUpdateFileHistory(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable ( bmRuleHistoryAllowed );		
}
