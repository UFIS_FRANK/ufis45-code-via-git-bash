// StartDlg.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <StartDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


StartDlg::StartDlg(CWnd* pParent /*=NULL*/)
	: CDialog(StartDlg::IDD, pParent)
{
	pomParent = pParent;
	
	Create();   

	//{{AFX_DATA_INIT(StartDlg)
	//}}AFX_DATA_INIT
}
//------------------------------------------------------------------------------------------------------------------------

void StartDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StartDlg)
	DDX_Control(pDX, IDC_LIST, omListBox);
	//}}AFX_DATA_MAP
}


void StartDlg::Create()
{
	CDialog::Create(IDD_START_DLG, pomParent);

	//SetWindowLangText ( this, "STID", "1652" );
	SetWindowLangText ( this, 1652 );
	ShowWindow(SW_SHOW);
}



BEGIN_MESSAGE_MAP(StartDlg, CDialog)
	//{{AFX_MSG_MAP(StartDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//------------------------------------------------------------------------------------------------------------------------

bool StartDlg::AddString(CString opString)
{
	int ilRet = omListBox.AddString(opString);
	CheckVisibility();
	UpdateWindow();


	if (ilRet == LB_ERR || ilRet == LB_ERRSPACE)
			return false;
	
	return true;
}
//------------------------------------------------------------------------------------------------------------------------

bool StartDlg::InsertString(int ipIndex, CString opString)
{
	int ilRet = 0;
	int ilPos = ipIndex;

	if (ipIndex > omListBox.GetCount())	
	{
		ilPos = omListBox.GetCount();
	}
	
	omListBox.DeleteString(ipIndex);
	ilRet = omListBox.InsertString(ipIndex, opString);
	CheckVisibility();
	UpdateWindow();

	if (ilRet == LB_ERR || ilRet == LB_ERRSPACE)
			return false;
	
	return true;
}
//------------------------------------------------------------------------------------------------------------------------

void StartDlg::CheckVisibility()
{
	int ilCount = omListBox.GetCount();
	if (ilCount > 15)
	{
		omListBox.SetTopIndex(omListBox.GetTopIndex() + 1);
	}
}
//------------------------------------------------------------------------------------------------------------------------

