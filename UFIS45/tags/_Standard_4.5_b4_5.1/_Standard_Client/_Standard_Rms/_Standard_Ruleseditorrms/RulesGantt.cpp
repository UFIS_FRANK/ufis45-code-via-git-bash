// stgantt.cpp : implementation file
//  

#include <stdafx.h>
#include <RegelWerk.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSTimeScale.h>
#include <CCSDragDropCtrl.h>
#include <CCSBar.h>
#include <ccsbchandle.h>
#include <ccsddx.h>
#include <RulesGantt.h>
#include <ccsddx.h>
#include <CedaBasicData.h>
#include <PrivList.h>
#include <CollectivesListDlg.h>
#include <ConnectBars.h>
#include <rulesformview.h>
#include <Dialog_VorlagenEditor.h>
#include <afxcoll.h>
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define INFO ::AfxTrace("%s(%i): ",__FILE__,__LINE__); ::AfxTrace
#else
#define INFO ((void)0)
#endif

//---------------------------------------------------------------------------------------------
//					macro definitions
//---------------------------------------------------------------------------------------------

//--- General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

#define	FIX_TIMESCALE_ROUNDING_ERROR
#ifdef	FIX_TIMESCALE_ROUNDING_ERROR
#define GetX(time)	(pomTimeScale->GetXFromTime(time) + imVerticalScaleWidth)
#define GetCTime(x)	(pomTimeScale->GetTimeFromX((x) - imVerticalScaleWidth))
#else

//--- Macro definition: getting the X-coordinate from the given time
/*#define GetX(time)  (omDisplayStart == omDisplayEnd? -1: \
            (imVerticalScaleWidth + \
            (((time) - omDisplayStart).GetTotalSeconds() * \
            (imWindowWidth - imVerticalScaleWidth) / \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds())))

//--- Macro definition: getting time from the given X-coordinate
#define GetCTime(x)  ((imWindowWidth - imVerticalScaleWidth) == 0? TIMENULL: \
            (omDisplayStart + \
            (time_t)(((x) - imVerticalScaleWidth) * \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds() / \
            (imWindowWidth - imVerticalScaleWidth))))*/
#endif



//---------------------------------------------------------------------------------------------
//					construction / destruction
//---------------------------------------------------------------------------------------------

RulesGantt::RulesGantt(RulesGanttViewer *popViewer, 
    int ipVerticalScaleWidth, int ipVerticalScaleIndent,
    CFont *popVerticalScaleFont, CFont *popGanttChartFont,
    int ipGutterHeight, int ipOverlapHeight, double dpArrowHeightFactor,
    COLORREF lpVerticalScaleTextColor, COLORREF lpVerticalScaleBackgroundColor,
    COLORREF lpHighlightVerticalScaleTextColor, COLORREF lpHighlightVerticalScaleBackgroundColor,
    COLORREF lpGanttChartTextColor, COLORREF lpGanttChartBackgroundColor,
    COLORREF lpHighlightGanttChartTextColor, COLORREF lpHighlightGanttChartBackgroundColor)
{
	CCS_TRY

    SetViewer(popViewer);
    SetVerticalScaleWidth(ipVerticalScaleWidth);
    SetVerticalScaleIndent(ipVerticalScaleIndent);
    SetFonts(MS_SANS6, MS_SANS6);

    SetGutters(ipGutterHeight, ipOverlapHeight);
    SetVerticalScaleColors(lpVerticalScaleTextColor, lpVerticalScaleBackgroundColor,
        lpHighlightVerticalScaleTextColor, lpHighlightVerticalScaleBackgroundColor);
    SetGanttChartColors(lpGanttChartTextColor, lpGanttChartBackgroundColor,
        lpHighlightGanttChartTextColor, lpHighlightGanttChartBackgroundColor);

    // Initialize default values
    omDisplayStart = TIMENULL;
    omDisplayEnd = TIMENULL;
    omCurrentTime = TIMENULL;
    omMarkTimeStart = TIMENULL;
    omMarkTimeEnd = TIMENULL;

    pomStatusBar = NULL;
	pomTimeScale = NULL;
	pomDetailDlg = NULL;
	pomConnectionDlg = NULL;
    pomRegelWerkView = NULL;
	prmContextBar = NULL;

	bmIsFixedScaling = TRUE;    // unessential -- will be set in method SetDisplayWindow()
	bmIsMouseInWindow = FALSE;
    bmIsControlKeyDown = FALSE;
	bmActiveBarSet = FALSE;
	bmContextBarSet = FALSE;
	bmActiveLineSet = FALSE;
	bmEditEnabled = TRUE;
	bmMoveBar = false;

	imWindowWidth = 0;		// unessential -- WM_SIZE will initialize this
	imHighlightLine = -1;
    imCurrentBar = -1;
	dmArrowHeightFactor = dpArrowHeightFactor;
	
	// used for moving and resizing
    SetBorderPrecision(15);
    imResizeMode = HTNOWHERE;

	// this is default, can be changed by calling SetArrowHitWidth()
	imArrowHitWidth = 2;

	// background colors for group links
	rmLinkColors[0] = LIGHTBROWN;
	rmLinkColors[1] = GREENBROWN;
	rmLinkColors[2] = NOT_SO_DARK_BLUE;
	rmLinkColors[3] = AQUA;
	rmLinkColors[4] = MAROON;
	rmLinkColors[5] = OLIVE;
	rmLinkColors[6] = NAVY;
	rmLinkColors[7] = TEAL;
	rmLinkColors[8] = LIME;
	rmLinkColors[9] = ROSE;

	imCurrItem = -1;

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

RulesGantt::~RulesGantt()
{
	CCS_TRY

	if(prmPreviousBar != NULL)
	{
		delete prmPreviousBar;
	}

	prmPreviousBar = NULL;

	
	CCS_CATCH_ALL
}



//---------------------------------------------------------------------------------------------
//						attaching
//---------------------------------------------------------------------------------------------
void RulesGantt::AttachWindow(CRulesFormView *popWnd)
{
	pomRegelWerkView = popWnd;
}




//---------------------------------------------------------------------------------------------
//						creation
//---------------------------------------------------------------------------------------------
BOOL RulesGantt::Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
	
	CCS_TRY

	// Make sure that the viewer is already given
    if (pomViewer == NULL)
        return FALSE;

    // Make sure that all essential style is defined
    // WS_CLIPSIBLINGS is very important for the ListBox so it does not paint outside its window
    dwStyle |= WS_CHILD | WS_VISIBLE/* | WS_HSCROLL*/ | WS_VSCROLL | WS_CLIPSIBLINGS;
    dwStyle |= LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE;
    dwStyle |= LBS_MULTIPLESEL;

    // Create the window
    if (CListBox::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
        return FALSE;

    // Create items according to what's in the Viewer
    ResetContent();


	CRect olRect;
	GetClientRect(&olRect);
	SetHorizontalExtent(olRect.Width() * 2);

    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(); ilLineno++)
    {
        AddString("");
        SetItemHeight(ilLineno, GetLineHeight(ilLineno));
    }
	
	prmPreviousBar = new BARDATA;
	
	
	CCS_CATCH_ALL

    return TRUE;
}



//---------------------------------------------------------------------------------------------
//				message map
//---------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(RulesGantt, CWnd)
	//{{AFX_MSG_MAP(RulesGantt)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_MOUSEMOVE()
    ON_WM_TIMER()
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(CM_GANTT_MENU_EDIT, OnMenuEdit)
	ON_COMMAND(CM_GANTT_MENU_ACTIVATE, OnMenuActivate)
	ON_COMMAND(CM_GANTT_MENU_OPERATIVE, OnMenuOperative)
    ON_COMMAND(CM_GANTT_MENU_PARTIALDEM, OnMenuPartialDemand)
	ON_COMMAND(CM_GANTT_MENU_CALCULATEDDEM, OnMenuCalculatedDemand)
	ON_COMMAND(CM_GANTT_MENU_DELETEBAR, OnMenuDeleteBar)
	ON_COMMAND(CM_GANTT_MENU_COPYBAR, OnMenuCopyBar)
	ON_COMMAND(CM_GANTT_MENU_CONNECTBAR, OnMenuConnectBar)
	ON_COMMAND(CM_GANTT_MENU_REMOVECON, OnMenuRemoveConnection)
	ON_COMMAND(CM_GANTT_MENU_CREATELNK, OnMenuCreateLink)
	ON_COMMAND(CM_GANTT_MENU_DELETELNK, OnMenuDeleteLink)
	ON_COMMAND(CM_GANTT_MENU_CREATEUNIT, OnMenuCreateUnit)
	ON_COMMAND(CM_GANTT_MENU_DELETEUNIT, OnMenuDeleteUnit)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_MESSAGE(WM_DIALOG_CLOSED, OnDetailDlgClosed)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//---------------------------------------------------------------------------------------------
//						message handlers
//---------------------------------------------------------------------------------------------
int RulesGantt::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	
	CCS_TRY

	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//--- register gantt as drop target
    m_DragDropTarget.RegisterTarget(this, this);
	
	EnableToolTips (TRUE);

	CCS_CATCH_ALL

	return 0;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnDestroy() 
{
	m_DragDropTarget.Revoke();
	CListBox::OnDestroy();
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnSize(UINT nType, int cx, int cy)
{
	
	CCS_TRY

    CListBox::OnSize(nType, cx, cy);

    CRect rect;
    GetWindowRect(&rect);   // don't use cx (vertical scroll may less the width of client rect)

    if (bmIsFixedScaling)   // fixed-scaling mode?
    {
        if (imWindowWidth == 0) // first time initialization?
            imWindowWidth = rect.Width();
        omDisplayEnd = GetCTime(rect.Width());   // has to recalculate the right most boundary?
    }
    else    // must be automatic variable-scaling mode
    {
        if (rect.Width() != imWindowWidth) // display window changed?
            InvalidateRect(NULL);   // repaint whole GanttChart
    }
    imWindowWidth = rect.Width();

	
	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

BOOL RulesGantt::OnEraseBkgnd(CDC* pDC)
{
	// Be careful, this may not work if you try to develop horizontal-scrolling in GanttChart.
	// Generally, if the ListBox needs to be repainted, it will send the message WM_ERASEBKGND.
	// But if you allow horizontal scrolling, WM_ERASEBKGND will be called before the new
	// horizontal position could be detected by dc.GetWindowOrg().x in DrawItem().
	// However, this version works fine as long as there is no such scrolling.
	// Then we can assume that the beginning offset will be 0 all the time.

	
	CCS_TRY

    CRect rectErase;
    pDC->GetClipBox(&rectErase);

    // Draw the VerticalScale if the user specifies it to be displayed
    if (imVerticalScaleWidth > 0)
    {
//		int ilx;
        CBrush brush(lmVerticalScaleBackgroundColor);
        CRect rect(0, rectErase.top, imVerticalScaleWidth, rectErase.bottom);
        pDC->FillRect(&rect, &brush);

        // Draw seperator line between VerticalScale and the body of GanttChart
        CPen penBlack(PS_SOLID, 0, /*::GetSysColor(COLOR_WINDOWFRAME)*/GRAY);
        CPen *pOldPen = pDC->SelectObject(&penBlack);
        pDC->MoveTo(imVerticalScaleWidth-2, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-2, rectErase.bottom);
		pDC->SelectObject(pOldPen);
    }

    // Draw the background body of GanttChart
    CBrush brush(lmGanttChartBackgroundColor);
    CRect rect(imVerticalScaleWidth, rectErase.top, rectErase.right, rectErase.bottom);
    pDC->FillRect(&rect, &brush);

    // Draw the vertical current time and marked time lines
    // DrawTimeLines(pDC, rectErase.top, rectErase.bottom);

	CCS_CATCH_ALL

    return TRUE;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnTimer(UINT nIDEvent)
{
	
	CCS_TRY

    if (imResizeMode != HTNOWHERE)  // in SetCapture()?
        return;

    if (nIDEvent == 0)  // the last timer message, clear everything
    {
		bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected lines
        UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
        return;
    }

    CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);
    UpdateGanttChart(point, ::GetKeyState(VK_CONTROL) & 0x8080);
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	
	CCS_CATCH_ALL
}



//---------------------------------------------------------------------------------------------
//						mouse action handling
//---------------------------------------------------------------------------------------------
void RulesGantt::OnMouseMove(UINT nFlags, CPoint point)
{
	CCS_TRY

    /*if (imResizeMode != HTNOWHERE)  // in SetCapture()?
    {
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

	// Checking for dragging a job bar?
	// Notes: Here's the best place for checking if there's a dragging from job bar.
	// We cannot simply regard OnLButtonDown() as the beginning of dragging,
	// since we designed OnLButtonDown() to bring bar to front or to back.
	int itemID = GetItemFromPoint(point);*/

	/*if ((nFlags & MK_LBUTTON) &&itemID != -1 && imCurrentBar != -1)
	{
		// DragServiceBarBegin(itemID, imCurrentBar);
	}*/

	CListBox::OnMouseMove(nFlags, point);
    UpdateGanttChart(point, nFlags & MK_CONTROL);

	imPrevItem = imCurrItem;
	imCurrItem = GetItemFromPoint(point);

	if (bmMoveBar)
	{
		if (imCurrItem > -1)
		{
			if ((imCurrItem != imPrevItem) && (pomViewer->GetBar(imCurrItem, 0) != NULL))
			{
				DrawThickLine(imCurrItem, imPrevItem);
				imCurrItem = imCurrItem;
			}
		}
	}

	


	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnLButtonDown(UINT nFlags, CPoint point)
{
	CCS_TRY

//--- check if there is anything to move or resize
	if (pomViewer->GetRealLineCount() == 0)
	{
		return;
	}

//--- Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL)/* && BeginMovingOrResizing(point)*/)
	{
		// don't call the default after SetCapture()
        return; 
	}

	
//--- moving bars to different lines
	int itemID = GetItemFromPoint(point);
	int ilBarno = GetBarnoFromPoint(itemID, point);
	int ilVertScaleWidth = GetVerticalScaleWidth();


	imCurrItem = itemID;
	imActiveItem = itemID;
	bmMoveBar = false;

	if (itemID > -1)
	{
		if (pomViewer->GetBar(itemID, 0) != NULL)
		{
			if (point.x <= ilVertScaleWidth)
			{
				bmMoveBar = true;
			}
		}
	}

	// MNE TEST
	/*CString test1;
	if (bmMoveBar)
		test1 = "true";
	else
		test1 = "false";
	TRACE("=======================================\n");
	TRACE("[RulesGantt::OnLButtonDown] bmMoveBar: %s, CurrItem: %d, ActiveItem: %d\n", test1, imCurrItem, imActiveItem);*/
	// END TEST


//--- Check if the user clicked on an arrow connecting two bars
	int ilLine = -1;
	int ilArrow = -1;
	CString olArrowID;
	bool blArrowClick = CheckConnectionHit(point, ilLine, ilArrow, olArrowID);

	if (blArrowClick == true)
	{
		CCSPtrArray <ARROWDATA> olTmpArr;
		CString olSource, olTarget;
		int ilType = -1;
		pomViewer->DeselectAllBars();
		pomViewer->SetArrowHighlighted(olArrowID);
		pomViewer->GetCompleteHighlightedArrow(olTmpArr, olSource, olTarget, ilType);
		if (::IsWindow(pomConnectionDlg->GetSafeHwnd()))
		{
			pomConnectionDlg->InitData(olSource, olTarget, ilType);
		}
		olTmpArr.DeleteAll();
	}
	else
	{
		pomViewer->SetArrowHighlighted(CString(""));
		if (::IsWindow(pomConnectionDlg->GetSafeHwnd()))
		{
			pomConnectionDlg->Destroy();
		}
	}
	
	if (ilBarno == -1)
	{
		ilBarno = 0;
	}
	
//--- multiple select when SHIFT is pressed
	/*if ((!(nFlags & MK_SHIFT)) || (ogSelectedRuds.GetSize() > 1))
	{
		pomViewer->DeselectAllBars();
	}*/
	if (!(nFlags & MK_SHIFT))
	{
		pomViewer->DeselectAllBars();
	}
	

	pomViewer->SetBarSelected(itemID, ilBarno, true);	// add current bar to selection array
	

//--- this needs only be done for an existing DetailDlg window
	if (::IsWindow(pomDetailDlg->GetSafeHwnd()) == TRUE)
	{
		// see if there is data for this bar 
		BARDATA *prlBar = pomViewer->GetBar(itemID, ilBarno);
		if(prlBar != NULL)
		{
			CString olRudUrno = prlBar->Urno;
			{
				CString olEvty;
				olEvty = pomRegelWerkView->GetRuleEventType();
				pomDetailDlg->InitData(pomRegelWerkView->omCurrRueUrno, olEvty, olRudUrno);
			}
		}

	
//--- check if left scale was clicked
		if ((ilBarno == -1) && (blArrowClick == false))
		{
			if (itemID != -1)	
			{
				if (point.x <= ilVertScaleWidth)
				{
					// deselect arrows and close connection dialog window
					pomViewer->SetArrowHighlighted("");
					if (::IsWindow(pomConnectionDlg->GetSafeHwnd()))
					{
						pomConnectionDlg->Destroy();
					}
					
					if (ilBarno == -1)
					{
						ilBarno = 0;
					}
					BARDATA *prlBar = pomViewer->GetBar(itemID, ilBarno);
					if (prlBar != NULL)
					{
						pomViewer->DeselectAllBars();
						pomViewer->SetBarSelected(itemID, ilBarno, true);

						CString olEvty;
						olEvty = pomRegelWerkView->GetRuleEventType();
						pomDetailDlg->InitData(pomRegelWerkView->omCurrRueUrno, olEvty, prlBar->Urno);
					}

					return;
				}
			}
		}
	}

    CListBox::OnLButtonDown(nFlags, point);

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CCS_TRY

    /*if (imResizeMode != HTNOWHERE)  // in SetCapture()?
    {
		// This is a fast way to terminate moving or resizing mode
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }*/
	

	// MNE TEST
	/*CString test1;
	if (bmMoveBar)
		test1 = "true";
	else
		test1 = "false";
	TRACE("[RulesGantt::OnLButtonUp] bmMoveBar: %s, CurrItem: %d, ActiveItem: %d\n", test1, imCurrItem, imActiveItem);*/
	// END TEST


	// moving bars vertically (switching lines)
	if (bmMoveBar)
	{
		if (imCurrItem != -1)
		{
			SwitchBars();
		}
	}
	
	bmMoveBar = false;

	CListBox::OnLButtonUp(nFlags, point);

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CCS_TRY

	if( !bmEditEnabled || !pomViewer->GetRealLineCount() )
	{
		return;
	}
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL)/* && BeginMovingOrResizing(point)*/)
	{
        return; // don't call the default after SetCapture()
	}



//---	Check if user clicked on an arrow
	int ilLine = -1;
	int ilArrow = -1;
	int ilType = -1;
	CString olArrowID;
	CString olSourceID, olTargetID;
	bool blArrowClick = CheckConnectionHit(point, ilLine, ilArrow, olArrowID);
	if (blArrowClick == true)
	{
		CCSPtrArray <ARROWDATA> olTmpArr;
		pomViewer->SetArrowHighlighted(olArrowID);
		pomViewer->GetCompleteHighlightedArrow(olTmpArr, olSourceID, olTargetID, ilType);
		olTmpArr.DeleteAll();

		if (::IsWindow(pomConnectionDlg->GetSafeHwnd()) == FALSE)
		{
			pomConnectionDlg = new ConnectBars(pomViewer, this);
		}
		pomConnectionDlg->InitData(olSourceID, olTargetID, ilType);
		pomConnectionDlg->ShowWindow(SW_SHOW);
	}


	

//---	check if user clicked on a bar
	CheckDetailWindow();
    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point);

	if (ilBarno != -1)
	{
		pomViewer->DeselectAllBars(); 
		BARDATA *prlBar = pomViewer->GetBar(itemID, ilBarno);
		if(prlBar != NULL)
		{
			CString olEvty;
			CString olRudUrno;

			olRudUrno = prlBar->Urno;
			olEvty = pomRegelWerkView->GetRuleEventType();

			// watch out: prlBar will be destroyed when calling SetBarSelected !
			pomViewer->SetBarSelected(itemID, ilBarno, true);

			pomDetailDlg->InitData(pomRegelWerkView->omCurrRueUrno, olEvty, olRudUrno);
			pomDetailDlg->ShowWindow(SW_SHOW);
		}
	}


//--- check if left scale was clicked
	if ((ilBarno == -1) && (blArrowClick == false))
	{
		int ilItem = GetItemFromPoint(point);
		if (ilItem != -1)	
		{
			int ilVertScaleWidth = GetVerticalScaleWidth();
			if (point.x <= ilVertScaleWidth)
			{
				// deselect arrows and close connection dialog window
				pomViewer->SetArrowHighlighted("");
				if (::IsWindow(pomConnectionDlg->GetSafeHwnd()))
				{
					pomConnectionDlg->Destroy();
				}

				BARDATA *prlBar = pomViewer->GetBar(itemID, 0);
				if (prlBar != NULL)
				{
					pomViewer->DeselectAllBars();
					pomViewer->SetBarSelected(itemID, 0, true);

					CString olEvty;
					olEvty = pomRegelWerkView->GetRuleEventType();
					pomDetailDlg->InitData(pomViewer->omRuleUrno, olEvty, pomViewer->omLines[ilItem].Urno/*prlBar->Urno*/);
				}
			}
		}
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CCS_TRY


	int itemID = GetItemFromPoint(point);
	int ilBarno = GetBarnoFromPoint(itemID, point);
	bool blIsArrow = false;
	
	//--- always behave as if SHIFT was pressed
	nFlags |= MK_SHIFT;

	
	//--- clicked outside the Gantt window
	if (itemID == -1)
	{
		return;		
	}
	

	//--- clicked on a bar
	if (ilBarno != -1)
	{
		bool blIsSelected = pomViewer->GetBarSelectState(itemID, ilBarno);
		if (blIsSelected == false)
		{
			// multiple select when SHIFT is pressed (max. 2 bars at a time)
			//if ((!(nFlags & MK_SHIFT)) || (ogSelectedRuds.GetSize() > 1))
			if ((!(nFlags & MK_SHIFT)))
			{
				pomViewer->DeselectAllBars();
			}
		}
		
		pomViewer->SetBarSelected(itemID, ilBarno, true);
	}
	else
	{
		//--- clicked directly on connection arrow
		int ilLine = -1;
		int ilArrow = -1;
		CString olArrowID;
		blIsArrow = CheckConnectionHit(point, ilLine, ilArrow, olArrowID);
		if (blIsArrow == true)	
		{
			pomViewer->SetArrowHighlighted(olArrowID);
			pomViewer->DeselectAllBars();
		}
	}
	

	LINEDATA *prlLine = pomViewer->GetLine(itemID);

	if(point.x < imVerticalScaleWidth - 2 ) // Then it's in VerticalScale
	{
		// if there's anything to happen when you right-click into LeftScale, put it here...
	}
	
	if (point.x > imVerticalScaleWidth)
	{
		MakeBarMenu(itemID, nFlags, point, blIsArrow);
	}
	
	CWnd::OnRButtonDown(nFlags, point);


	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnDetailDlgClosed(WPARAM wParam, LPARAM lParam)
{
	//----------------------------------------------------------
	// The DetailDlg sends the WM_DIALOG_CLOSED - message in its
	// OnDestroy() - function, so that we here know when to set 
	// the pointer pomDetailDlg to NULL.
	//----------------------------------------------------------

	CWnd *polFrom = (CWnd *) wParam;
	
	// note : pomDetailDlg points to freed memory at this moment, 
	// is indeed a 'dangling' pointer which boundschecker will notice,
	// but the action done here is the only thing valid to do with such a pointer : set it to NULL !
	if (polFrom == pomDetailDlg)
	{
		pomDetailDlg = NULL;
	}
	else if (polFrom == pomConnectionDlg)
	{
		pomConnectionDlg = NULL;
	}
}
//---------------------------------------------------------------------------------------------

/*void RulesGantt::DragDutyBarBegin(int ipLineno)
{
	m_DragDropSource.CreateDWordData(ipLineno, 1);
	long llRudUrno = atol(pomViewer->omLines[ipLineno].Urno);
	m_DragDropSource.AddDWord(llRudUrno);
	m_DragDropSource.BeginDrag();
}*/
//---------------------------------------------------------------------------------------------

void RulesGantt::DrawThickLine(int itemID, int ilOldLine)
{
	CRect olRect;
	
	
	if (ilOldLine > -1)
	{
		// TRACE("[RulesGantt::DrawThickLine] Repainting line %d\n", ilOldLine);
		RepaintLine(ilOldLine);
	}

	if (GetItemRect(itemID, olRect) != LB_ERR)
	{
		// MNE TEST
		/*CString test1;
		TRACE("[RulesGantt::DrawThickLine] Drawing to line: %d, Rect: t=%d, b=%d\n", 
						itemID, olRect.top, olRect.bottom);*/
		// END TEST

		olRect.bottom -= 10;
		olRect.top = olRect.bottom - 3;

		CDC *pDc = GetDC();
		CBrush *polBlackBrush = new CBrush(BLACK);
		pDc->FillRect(olRect, polBlackBrush);
		delete polBlackBrush;
		ReleaseDC(pDc);
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SwitchBars()
{
	// MNE TEST
	/*CString test1;
	if (bmMoveBar)
		test1 = "true";
	else
		test1 = "false";
	TRACE("[RulesGantt::SwitchBars] bmMoveBar: %s, CurrItem: %d <?> ActiveItem: %d\n", test1, imCurrItem, imActiveItem);
	TRACE("=======================================\n");*/
	// END TEST

	bmMoveBar = false;

	if ((imCurrItem > -1) && (imCurrItem < pomViewer->omLines.GetSize()))
	{
		if (imActiveItem != imCurrItem)
		{
			CCSPtrArray <RecordSet> olRudArr;
			ogBCD.GetRecords("RUD_TMP", "URUE", pomViewer->omRuleUrno, &olRudArr);
			
			bool blUpwards = imCurrItem < imActiveItem ? true : false;

			/*int ilLower = min(pomViewer->omLines[imCurrItem], pomViewer->omLines[imActiveItem]);
			int ilUpper = max(pomViewer->omLines[imCurrItem], pomViewer->omLines[imActiveItem]);*/
			
			int ilDisp ;
			int ilSize = olRudArr.GetSize();
			CString olRud2Move;

			for (int i = 0; i < ilSize; i++)
			{
				ilDisp = atoi(olRudArr[i].Values[igRudDispIdx]);
				
				if (ilDisp == imActiveItem + 1)
				{
					olRud2Move = olRudArr[i].Values[igRudUrnoIdx];
				}

				if (blUpwards)
				{
					if ((ilDisp >= imCurrItem + 1) && (ilDisp < imActiveItem + 1))
					{
						olRudArr[i].Values[igRudDispIdx].Format("%d", ilDisp + 1);
						ogBCD.SetRecord("RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], olRudArr[i].Values);
					}
				}
				else
				{
					if ((ilDisp <= imCurrItem + 1) && (ilDisp > imActiveItem + 1))
					{
						olRudArr[i].Values[igRudDispIdx].Format("%d", ilDisp - 1);
						ogBCD.SetRecord("RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], olRudArr[i].Values);
					}
				}
			}
			
			RecordSet olRud(ogBCD.GetFieldCount("RUD_TMP"));
			ogBCD.GetRecord("RUD_TMP", "URNO", olRud2Move, olRud);
			olRud.Values[igRudDispIdx].Format("%d", imCurrItem + 1);
			ogBCD.SetRecord("RUD_TMP", "URNO", olRud2Move, olRud.Values);

			pomViewer->ChangeViewTo(pomViewer->omRuleUrno);
			olRudArr.DeleteAll();
		}
		else
		{
			RepaintItemHeight(imCurrItem);
		}
	}
}
//---------------------------------------------------------------------------------------------

// Checks if detail dialog has a window and creates it if not
void RulesGantt::CheckDetailWindow()
{
	if (::IsWindow(pomDetailDlg->GetSafeHwnd()) == FALSE)
	{
		pomDetailDlg = new DemandDetailDlg("EMPTY", pomViewer, this);
	}	
}
//---------------------------------------------------------------------------------------------

void RulesGantt::MakeBarMenu(int itemID, UINT nFlags, CPoint point, bool bpIsArrow) 
{
	CCS_TRY
	
	prmContextBar = NULL;

	if(itemID != -1)
	{
		//-- create menu
		CMenu menu;
		menu.CreatePopupMenu();
		
		int ilBarno = GetBarnoFromPoint(itemID, point);
		if (ilBarno != -1 || bpIsArrow == true)	// on a bar or on an arrow ?
		{
			//-- remove all (old) menu items
			for (int i = menu.GetMenuItemCount() - 1; i >= 0; i--)
			{
				menu.RemoveMenu(i, MF_BYPOSITION);
			}
			
			//-- create menu items 
			if (bmEditEnabled)
			{
				menu.AppendMenu(MF_STRING, CM_GANTT_MENU_EDIT, GetString(1476));						// open detail window
			}
			menu.AppendMenu(MF_STRING, CM_GANTT_MENU_DELETEBAR, GetString(131));					// remove duty requirement
			menu.AppendMenu(MF_STRING, CM_GANTT_MENU_COPYBAR, GetString(IDS_COPY) );
			menu.AppendMenu(MF_SEPARATOR);
			menu.AppendMenu(MF_STRING, CM_GANTT_MENU_CONNECTBAR, GetString(1525));				// connect duties
			menu.AppendMenu(MF_STRING, CM_GANTT_MENU_REMOVECON, GetString(1532));				// remove connection
			menu.AppendMenu(MF_SEPARATOR);
			menu.AppendMenu(MF_STRING, CM_GANTT_MENU_CREATELNK, GetString(2003));				// create grouping (Staff - location)
			menu.AppendMenu(MF_STRING, CM_GANTT_MENU_DELETELNK, GetString(2004));				// delete grouping

			
			// first, disable the link and connection stuff
			// (we decide later, which items can be enabled)
			menu.EnableMenuItem(CM_GANTT_MENU_DELETELNK, MF_BYCOMMAND | MF_GRAYED);
			menu.EnableMenuItem(CM_GANTT_MENU_CREATELNK, MF_BYCOMMAND | MF_GRAYED);
			menu.EnableMenuItem(CM_GANTT_MENU_CONNECTBAR, MF_BYCOMMAND | MF_GRAYED);
			menu.EnableMenuItem(CM_GANTT_MENU_REMOVECON, MF_BYCOMMAND | MF_GRAYED);

			
			BARDATA *prlBar = NULL;
			if (ilBarno != -1)
			{
				prlBar = pomViewer->GetBar(itemID, ilBarno);
			}

			if (prlBar != NULL)
			{
				prmContextBar = pomViewer->GetBar(itemID, ilBarno);
				omCurrRudUrno = prlBar->Urno;
				
				// if current bar has a group link 
				// enable this menu item
				CString olLinkUrno;
				olLinkUrno = ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "ULNK");
				if ((olLinkUrno.IsEmpty() == FALSE))
				{
					menu.EnableMenuItem(CM_GANTT_MENU_DELETELNK, MF_BYCOMMAND);	// might be disabled again further down. Watch it !
				}

				if (ogSelectedRuds.GetSize() >= 2)    // at least two bars selected ?
				{
					if (pomViewer->AreBarsLinked() == false) 
					{
						// if not all selected bars are linked to a group yet,
						// the menu item "Create Group" is enabled
						menu.EnableMenuItem(CM_GANTT_MENU_CREATELNK, MF_BYCOMMAND);

						// it doesn't make sense to enable the "Delete Group" menu item if 
						// more than one bar is selected and at least one bar is linked 
						// to a different group than the other ones
						menu.EnableMenuItem(CM_GANTT_MENU_DELETELNK, MF_GRAYED);
					}

					// to create connections (arrows), there must not be 
					// more than two bars selected
					if (ogSelectedRuds.GetSize() == 2)
					{
						if((pomViewer->IsBarConnected(ogSelectedRuds.GetAt(0), ogSelectedRuds.GetAt(1)) == false) || 
							ogSelectedRuds.GetSize())
						{
							menu.EnableMenuItem(CM_GANTT_MENU_CONNECTBAR, MF_BYCOMMAND);
						}
						
						CString olFromBar, olToBar;
						if (pomViewer->GetConnectedBar(omCurrRudUrno, olFromBar, olToBar) == true)
						{
							menu.EnableMenuItem(CM_GANTT_MENU_REMOVECON, MF_BYCOMMAND);
						}
					}
					if ( igRudUsesIdx>=0 )
					{
						if ( pomViewer->AreBarsLinkedByUses() )
						{
							menu.AppendMenu(MF_SEPARATOR);
							menu.AppendMenu(MF_STRING, CM_GANTT_MENU_DELETEUNIT, GetString(IDS_DELETE_UNIT));				
						}
						else
						{
							if ( pomViewer->CanBarsBeLinkedByUses() )
							{
								menu.AppendMenu(MF_SEPARATOR);
								menu.AppendMenu(MF_STRING, CM_GANTT_MENU_CREATEUNIT, GetString(IDS_CREATE_UNIT));				
							}
						}
					}
				}
			}		
			
			if (bpIsArrow == true)
			{
				menu.EnableMenuItem(CM_GANTT_MENU_EDIT, MF_BYCOMMAND | MF_GRAYED);
				menu.EnableMenuItem(CM_GANTT_MENU_DELETEBAR, MF_BYCOMMAND | MF_GRAYED);
				menu.EnableMenuItem(CM_GANTT_MENU_REMOVECON, MF_BYCOMMAND);
			}
			if ((ogSelectedRuds.GetSize()!=1) || bpIsArrow)
			{
				menu.EnableMenuItem(CM_GANTT_MENU_COPYBAR, MF_BYCOMMAND | MF_GRAYED);
			}
			
		}
			
		//-- show menu 
		ClientToScreen(&point);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	}

	CWnd::OnRButtonDown(nFlags, point);

	
	CCS_CATCH_ALL
}



//---------------------------------------------------------------------------------------------
//						context menu
//---------------------------------------------------------------------------------------------
void RulesGantt::OnMenuEdit()
{
	if( prmContextBar && bmEditEnabled )
	{
		CString olRudUrno;
		int ilLineno, ilBarno;

		olRudUrno = prmContextBar->Urno;
		if (pomViewer->FindBarGlobal(olRudUrno, ilLineno, ilBarno) == true)
		{
			pomViewer->DeselectAllBars(); 

			//--- watch out: SetBarSelected will cause the bar to be deleted and remade, so don't use prmContextBar after ! 
			pomViewer->SetBarSelected(ilLineno, ilBarno, true);
			
			CString olEvty;
			olEvty = pomRegelWerkView->GetRuleEventType();

			if (::IsWindow(pomDetailDlg->GetSafeHwnd()) == FALSE)
			{
				pomDetailDlg = new DemandDetailDlg(olRudUrno, pomViewer, this);
				pomDetailDlg->InitData(pomRegelWerkView->omCurrRueUrno, olEvty, olRudUrno);
				pomDetailDlg->ShowWindow(SW_SHOW);
			}
			else
			{
				//--- reset data in DemandDetailDlg
				pomDetailDlg->InitData(pomRegelWerkView->omCurrRueUrno, olEvty, olRudUrno);
			}					
		}
	}	
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnMenuActivate()
{

	if (ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "FADD") == CString("0"))
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FADD", CString("1"));
	}
	else
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FADD", CString("0"));
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnMenuOperative()
{
	if (ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "FOND") == CString("0"))
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FOND", CString("1"));
	}
	else
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FOND", CString("0"));

	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnMenuPartialDemand()
{
	// MNE TEST
	/*CString olTest;
	olTest = ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "FFPD");
	int ilTest = ogBCD.GetDataCount("RUD_TMP");*/
	// END TEST

	if (ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "FFPD") == CString("1"))
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FFPD", CString("0"));

		CString olTplUrno = ogBCD.GetField("RUE", "URNO", pomRegelWerkView->omCurrRueUrno, "UTPL");
		CollectivesListDlg olDlg(olTplUrno, this);
		int ilReturn = olDlg.DoModal();
		if (ilReturn != IDCANCEL)
		{
			if (ilReturn == IDOK && olDlg.omRueUrno != CString("-1"))
			{
				// urno for collective rule has been returned
				ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "UCRU", olDlg.omRueUrno);			
			}
			else
			{
				MessageBox(GetString(1436), GetString(1437));
				ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FFPD", CString("1"));
				ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "UCRU", CString(" "));
			}
		}
	}
	else
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FFPD", CString("1"));
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "UCRU", CString(" "));
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnMenuCalculatedDemand()
{
	if (ogBCD.GetField("RUD_TMP", "URNO", omCurrRudUrno, "FNCD") == CString("0"))
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FNCD", CString("1"));
	}
	else
	{
		ogBCD.SetField("RUD_TMP", "URNO", omCurrRudUrno, "FNCD", CString("0"));
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnMenuDeleteBar()
{
	CCS_TRY

	CString olRudToDelete;

	// get record (we need this for our DDX call)
	RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
		
		// delete selection for this bar
	int ilCount = ogSelectedRuds.GetSize();
	for (int i = ilCount-1; i >=0; i--)
	{
		olRudToDelete = ogSelectedRuds.GetAt(i);
		if ( !ogBCD.GetRecord("RUD_TMP", "URNO", olRudToDelete, olRecord) )
			continue;

		ogSelectedRuds.RemoveAt ( i) ;
		
		// delete connection data in connected bars if existing
		CString olPreviousUrno, olNextUrno;
		pomViewer->GetConnectedBar(olRudToDelete, olPreviousUrno, olNextUrno);
		
		if (olPreviousUrno.IsEmpty() == FALSE)
		{
			RecordSet olPreviousBar(ogBCD.GetFieldCount("RUD_TMP"));
			ogBCD.GetRecord("RUD_TMP", "URNO", olPreviousUrno, olPreviousBar);

			olPreviousBar.Values[igRudUndeIdx] = CString("");
			olPreviousBar.Values[igRudRendIdx] = CString("");
			olPreviousBar.Values[igRudTsndIdx] = CString("");

			ogBCD.SetRecord("RUD_TMP", "URNO", olPreviousUrno, olPreviousBar.Values);
		}

		if (olNextUrno.IsEmpty() == FALSE)
		{
			RecordSet olNextBar(ogBCD.GetFieldCount("RUD_TMP") );
			ogBCD.GetRecord("RUD_TMP", "URNO", olNextUrno, olNextBar);

			olNextBar.Values[igRudUpdeIdx] = CString("");
			olNextBar.Values[igRudRepdIdx] = CString("");
			olNextBar.Values[igRudTspdIdx] = CString("");

			ogBCD.SetRecord("RUD_TMP", "URNO", olNextUrno, olNextBar.Values);
		}

		// check detail dialog
		if (::IsWindow(pomDetailDlg->GetSafeHwnd()))
		{
			if (pomDetailDlg->GetCurrentRud() == olRudToDelete)
			{
				pomDetailDlg->Destroy();
			}
		}

		// tell viewer that the record was deleted
		ogDdx.DataChanged((void *)this,RUD_TMP_DELETE,(void *)&olRecord);
		
		AddItemToUpdList(olRudToDelete, "RUD", ITEM_DEL);
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------


// this is the handler for the context menu item 
// "connect bars" (basis for the arrows between the bars 
// and their moving). 
void RulesGantt::OnMenuConnectBar()
{
	CCSPtrArray <ARROWDATA> olArrowParts;
	CString olSource, olTarget;
	int ilType = -1;

	// create a new dialog object if needed
	if (::IsWindow(pomConnectionDlg->GetSafeHwnd()) == FALSE)
	{
		pomConnectionDlg = new ConnectBars(pomViewer, this);
	}
	
	// either we have two selected bars - type is set to FS as default
	if (ogSelectedRuds.GetSize() >= 2)
	{
		pomConnectionDlg->InitData(ogSelectedRuds.GetAt(0), ogSelectedRuds.GetAt(1), 2);
		pomConnectionDlg->ShowWindow(SW_SHOW);
	}
	// or an existing connection arrow was clicked
	else if (pomViewer->GetCompleteHighlightedArrow(olArrowParts, olSource, olTarget, ilType) == true)
	{
		pomConnectionDlg->InitData(olSource, olTarget, ilType);
		pomConnectionDlg->ShowWindow(SW_SHOW);
	}
	
	// delete array on heap
	if (olArrowParts.GetSize() > 0)
	{
		olArrowParts.DeleteAll();
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::OnMenuRemoveConnection()
{
	CString olSource, olTarget;
	CCSPtrArray <ARROWDATA> olArrowParts;
 	int ilType;

	// either we have a highlighted connection
	if (pomViewer->GetCompleteHighlightedArrow(olArrowParts, olSource, olTarget, ilType) == true)
	{
		pomViewer->DeleteConnection(olSource);
	}

	// or there are two selected bars connected to each other
	if (ogSelectedRuds.GetSize() == 2)
	{
		CString olBarTo, olBarFrom;
		if (pomViewer->GetConnectedBar(ogSelectedRuds.GetAt(0), olBarFrom, olBarTo))
		{
			if (ogSelectedRuds.GetAt(1) == olBarFrom)
			{
				pomViewer->DeleteConnection(olBarFrom);
			}
			else if (ogSelectedRuds.GetAt(1) == olBarTo)
			{
				pomViewer->DeleteConnection(ogSelectedRuds.GetAt(0));
			}
		}
	}
	
	olArrowParts.DeleteAll();
}
//---------------------------------------------------------------------------------------------

// this function creates a connnection between different RUDs
// (eg. a location and a staff member) by filling RUD.ULNK with a new urno
void RulesGantt::OnMenuCreateLink()
{
	long llNewUrno;
	llNewUrno = ogBCD.GetNextUrno();

	RecordSet olRud(ogBCD.GetFieldCount("RUD"));

	for (int i = 0; i < ogSelectedRuds.GetSize(); i++)
	{
		ogBCD.GetRecord("RUD_TMP", "URNO", ogSelectedRuds.GetAt(i), olRud);
		olRud.Values[igRudUlnkIdx].Format("%d", llNewUrno);
		ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
		ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRud);
	}

	// The RUD_CON_CHANGE message causes the whole gantt to be redrawn. We 
	// only send it once to avoid a flickering screen, which might otherwise
	// happen on a slow computer
	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRud);
}
//---------------------------------------------------------------------------------------------

// this function deletes a connnection between different RUDs by emptying RUD.ULNK
void RulesGantt::OnMenuDeleteLink()
{
	if (ogSelectedRuds.GetSize() >= 1)
	{
		RecordSet olRud(ogBCD.GetFieldCount("RUD"));
		ogBCD.GetRecord("RUD_TMP", "URNO", ogSelectedRuds.GetAt(0), olRud);

		if (olRud.Values[igRudUlnkIdx].IsEmpty() == false)
		{
			int j = 0;
			CCSPtrArray <RecordSet> olRudArr;
			ogBCD.GetRecords("RUD_TMP", "ULNK", olRud.Values[igRudUlnkIdx], &olRudArr);
			for (int i = 0; i < olRudArr.GetSize(); i++)
			{
				olRudArr[i].Values[igRudUlnkIdx].Empty();				
				ogBCD.SetRecord("RUD_TMP", "URNO", olRudArr[i].Values[igRudUrnoIdx], olRudArr[i].Values);
			}
			olRudArr.DeleteAll();
		}

		// let viewer check the colors array and remove the entries for the deleted link
		pomViewer->OnDeleteLink(olRud.Values[igRudUlnkIdx]);

		// tell viewer that the records were changed
		ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRud);
	}
}



//---------------------------------------------------------------------------------------------
//						set methods
//---------------------------------------------------------------------------------------------
void RulesGantt::SetViewer(RulesGanttViewer *popViewer)
{
    pomViewer = popViewer;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetVerticalScaleWidth(int ipWidth)
{
    if (ipWidth >= 0)
	{
		imVerticalScaleWidth = ipWidth;
	}
	else
	{
		imVerticalScaleWidth = 0;
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetVerticalScaleIndent(int ipIndent)
{
    imVerticalScaleIndent = ipIndent;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetFonts(int index1, int index2)
{
	CCS_TRY

    pomVerticalScaleFont = &ogMSSansSerif_Bold_8;
    pomGanttChartFont = &ogSmallFonts_Regular_7;
	
    // Calculate the normal height of a bar
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    if (pomGanttChartFont)
        dc.SelectObject(pomGanttChartFont);

	
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm); 
    imBarHeight = tm.tmHeight + tm.tmExternalLeading + 2;
    imLeadingHeight = (tm.tmExternalLeading + 2) / 2;
    dc.DeleteDC();

	
	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetGutters(int ipGutterHeight, int ipOverlapHeight)
{
    imGutterHeight = ipGutterHeight;
    imOverlapHeight = ipOverlapHeight;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
	
	CCS_TRY

    lmVerticalScaleTextColor = lpTextColor;
    lmVerticalScaleBackgroundColor = lpBackgroundColor;
    lmHighlightVerticalScaleTextColor = lpHighlightTextColor;
    lmHighlightVerticalScaleBackgroundColor = lpHighlightBackgroundColor;

	
	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
	
	CCS_TRY

    lmGanttChartTextColor = lpTextColor;
    lmGanttChartBackgroundColor = lpBackgroundColor;
    lmHighlightGanttChartTextColor = lpHighlightTextColor;
    lmHighlightGanttChartBackgroundColor = lpHighlightBackgroundColor;

	
	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetStatusBar(CStatic *popStatusBar)
{
    pomStatusBar = popStatusBar;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetTimeScale(CCSTimeScale *popTimeScale)
{
    pomTimeScale = popTimeScale;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetBorderPrecision(int ipBorderPrecision)
{
    imBorderPreLeft = ipBorderPrecision / 2;
    imBorderPreRight = (ipBorderPrecision + 1) / 2;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetArrowHitWidth(int ipArrowHitWidth)
{
	imArrowHitWidth = ipArrowHitWidth;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling)
{
	CCS_TRY

    omDisplayStart = opDisplayStart;
    omDisplayEnd = opDisplayEnd;
    bmIsFixedScaling = bpFixedScaling;

    if (m_hWnd == NULL)
        imWindowWidth = 0;  // window is still not opened
    else
    {
        CRect rect;
        GetWindowRect(&rect);   // can't use client rect (vertical scroll may less its width)
        imWindowWidth = rect.Width();   // however, this is correct only if window has no border
        RepaintGanttChart();    // redraw the entire screen
    }

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetDisplayStart(CTime opDisplayStart)
{
	CCS_TRY

    if (bmIsFixedScaling)   // must shift both start/end time while keeping the old time span
        omDisplayEnd = opDisplayStart + (omDisplayEnd - omDisplayStart).GetTotalSeconds();
    omDisplayStart = opDisplayStart;
    RepaintGanttChart();    // redraw the entire screen

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetCurrentTime(CTime opCurrentTime)
{                               
    RepaintGanttChart(-1, omCurrentTime, opCurrentTime);
    omCurrentTime = opCurrentTime;
}
//---------------------------------------------------------------------------------------------

void RulesGantt::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
	CCS_TRY

	// Displays two thin vertical lines. Is not used in RegelWerk currently
    RepaintGanttChart(-1, omMarkTimeStart, opMarkTimeStart);
    omMarkTimeStart = opMarkTimeStart;
    RepaintGanttChart(-1, omMarkTimeEnd, opMarkTimeEnd);
    omMarkTimeEnd = opMarkTimeEnd;

	CCS_CATCH_ALL
}




//---------------------------------------------------------------------------------------------
//						check/get methods
//---------------------------------------------------------------------------------------------
bool RulesGantt::CheckIncreasedLineHeight(int itemID)
{
	for (int i = 0; i < pomViewer->omLines[itemID].Arrows.GetSize(); i++)
	{
		if (pomViewer->omLines[itemID].Arrows[i].ArrowType == 4 || pomViewer->omLines[itemID].Arrows[i].ArrowType == 5)
		{
			return true;
		}
	}

	return false;
}
//---------------------------------------------------------------------------------------------

int RulesGantt::GetBarYPos(CRect rcItem, int sourceID, int itemID, int ipBar)
{
	int ilYStart = -1;

	/*if (CheckIncreasedLineHeight(itemID) == false)
	{
		ilYStart = rcItem.top + (rcItem.bottom - rcItem.top) / 2;	
	}
	else
	{

		// ***** TODO: Hier ist noch ein Fehler ! 
		// Wenn zwei Verbindungen an einem Balken h�ngen, kann ich nicht davon ausgehen, da� diejenige
		// zuerst abgearbeitet wird, die auch f�r die Zeilenh�hen�nderung verantwortlich ist.
		// Deswegen schl�gt das folgende if manchmal fehl. Die Information, ob die "�ndernde" Connection 
		// von oben oder von unten kommt, mu� anders gespeichert werden (bereits in MakeArrow() im Viewer)
		//if (itemID < sourceID)
		//{
	    //  	ilYStart = (int)(rcItem.bottom - (rcItem.Height() / 2) - rcItem.Height() / dmArrowHeightFactor / 2);
		//}
		//else
		//{
		//	ilYStart = (int)(rcItem.bottom - rcItem.Height() / dmArrowHeightFactor / 2);
		//}
		
		if (pomViewer->omLines[itemID]ArrowPass == 1)
		{
			ilYStart = (int)(rcItem.bottom - (rcItem.Height() / 2) - rcItem.Height() / dmArrowHeightFactor / 2);
		}
		else if (pomViewer->omLines[itemID]ArrowPass == 2)
		{
			ilYStart = (int)(rcItem.bottom - rcItem.Height() / dmArrowHeightFactor / 2);
		}
		else
		{
			
		}
		
	}*/
	
	if (pomViewer->omLines[itemID].ArrowPass == 0)
	{
		ilYStart = rcItem.top + (rcItem.bottom - rcItem.top) / 2;	
	}
	else if (pomViewer->omLines[itemID].ArrowPass == 1) 
	{
		ilYStart = (int)(rcItem.bottom - (rcItem.Height() / 2) - rcItem.Height() / dmArrowHeightFactor / 2);
	}
	else if (pomViewer->omLines[itemID].ArrowPass == 2)
	{
		ilYStart = (int)(rcItem.bottom - rcItem.Height() / dmArrowHeightFactor / 2);
	}
/*		PRF5468: no longer using CritErr.txt
	else
	{
		CString olTmpStr;
		olTmpStr = "Error in RulesGantt.cpp when computing the Y-values for connections !";
		of_catch << olTmpStr.GetBuffer(0) << endl;
	}
*/	
	return ilYStart;
}
//---------------------------------------------------------------------------------------------

int RulesGantt::GetGanttChartHeight()
{
	int ilHeight = 0;

	
	CCS_TRY

	// Return the display height expected
	// (Be careful, this one may be called before the CListBox was created)

    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(); ilLineno++)
        ilHeight += GetLineHeight(ilLineno);

	
	CCS_CATCH_ALL

    return ilHeight;
}
//---------------------------------------------------------------------------------------------

CRect RulesGantt::GetDrawingRect(CString opLinkUrno, CRect opItem)
{
	CRect olRect = opItem;

	CCS_TRY

	
	CStringArray olLinkedBars;

	pomViewer->GetLinkedBars(opLinkUrno, olLinkedBars);

	BARDATA *prlBar;
	int ilLine, ilBar;
	int ilLeft = 2000, ilRight = 0;
	
	for (int i = 0; i < olLinkedBars.GetSize(); i++)
	{
		if (pomViewer->FindBarGlobal(olLinkedBars.GetAt(i), ilLine, ilBar) == true)
		{
			prlBar = pomViewer->GetBar(ilLine, ilBar);
			if (prlBar != NULL)
			{
				ilLeft = min(ilLeft, GetX(prlBar->Debe));
				ilRight = max(ilRight, GetX(prlBar->Deen));
				
				// TRACE("[GetDrawingRect] left: %d, right: %d\n", GetX(prlBar->Debe), GetX(prlBar->Deen));

				olRect.left = ilLeft - 10;
				olRect.right = ilRight + 10;
				olRect.bottom -= 1;
			}
		}
	}


	CCS_CATCH_ALL

	return olRect;
}
//---------------------------------------------------------------------------------------------

int RulesGantt::GetLineHeight(int ilLineno)
{
	int ilLineHeight = 0;

	if (pomViewer != NULL)
	{
		LINEDATA *polLine = pomViewer->GetLine(ilLineno);
		if (polLine != NULL)
		{
			int ilMaxOverlapLevel = polLine->MaxOverlapLevel;
			int ilArrowPass = pomViewer->GetLine(ilLineno)->ArrowPass;
			if (ilArrowPass != 0)
			{
				ilLineHeight = (int)((imBarHeight + (max(1, ilMaxOverlapLevel) * imOverlapHeight)) * dmArrowHeightFactor);
			}
			else
			{
				ilLineHeight = /*(2 * imGutterHeight) + */imBarHeight + (max(1, ilMaxOverlapLevel) * imOverlapHeight);
			}
		}
	}
	
	// TRACE("[RulesGantt::GetLineHeight] Line %d is %d pixels high", ilLineno, ilLineHeight);
    return ilLineHeight;
}
//---------------------------------------------------------------------------------------------

int RulesGantt::GetVerticalScaleWidth()
{
    
    return imVerticalScaleWidth;
}
//---------------------------------------------------------------------------------------------

// return the item ID of the list box based on the given "point"
int RulesGantt::GetItemFromPoint(CPoint point) const
{
	CCS_TRY


    CRect rcItem;
    for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
		{
            return itemID;
		}
    }


	CCS_CATCH_ALL

    return -1;
}
//---------------------------------------------------------------------------------------------

// Return the bar number of the list box based on the given point
int RulesGantt::GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)const
{
	int ilRet = 0;

	CCS_TRY

    
	if (ipLineno == -1) // this checking was added after some bugs were found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    
	int level1 = -1;
	int level2 = -1;
	if (pomViewer->omLines[ipLineno].ArrowPass == 1)
	{
		// regular lines and lines with arrow passing below bar(s)
		point.y -= rcItem.top + imGutterHeight;	// set point.y to top of line bar
		level1 = (point.y < imBarHeight) ? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
		level2 = (point.y < 0)? -1: point.y / imOverlapHeight;
	}
	else
	{
		// lines with arrow passing above bar(s)
		if ((rcItem.bottom - point.y - imGutterHeight) < imBarHeight)
		{
			level1 = 0;
			level2 = 1;
		}
	}
	
    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x - ipPreLeft);
    CTime time2 = GetCTime(point.x + ipPreRight);

	ilRet = pomViewer->GetBarnoFromTime(ipLineno, time1, time2, level1, level2); 


	CCS_CATCH_ALL

    return ilRet;
}
//---------------------------------------------------------------------------------------------

// Return the hit test area code (determine the location of the cursor).
// Possible codes are HTLEFT, HTRIGHT, HTNOWHERE (out-of-bar), HTCAPTION (bar body)
// 
UINT RulesGantt::GetHit(int ipLineno, int ipBarno, CPoint point)
{
	
	CCS_TRY

    if (ipLineno == -1 || ipBarno == -1)    // this checking was added after some bugs found
        return HTNOWHERE;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);

    // Check against each possible case of hit test areas.
    // Since most people like to drag things to the right, we check right border before left.
    // Be careful, if the bar is very small, the user may not be able to get HTCAPTION or HTLEFT.
    //
    BARDATA *prlBar = pomViewer->GetBar(ipLineno, ipBarno);
    if (IsBetween(prlBar->Deen, time1, time2))
        return HTRIGHT;
    else if (IsBetween(prlBar->Debe, time1, time2))
        return HTLEFT;
    else if (IsOverlapped(time1, time2, prlBar->Debe, prlBar->Deen))
        return HTCAPTION;

	
	CCS_CATCH_ALL

    return HTNOWHERE;
}
//---------------------------------------------------------------------------------------------

bool RulesGantt::CheckConnectionHit(CPoint point, int &ripLine, int &ripArrow, CString &ropArrowID)
{
	CPoint *Point1, *Point2;
	CString olArrowID;

	int ilLineCount = pomViewer->omLines.GetSize();
	for (ripLine = 0; ripLine < ilLineCount; ripLine++)
	{
		int ilArrowCount = pomViewer->omLines[ripLine].Arrows.GetSize();
		for (ripArrow = 0; ripArrow < ilArrowCount; ripArrow++)
		{
			int ilPosCount = pomViewer->omLines[ripLine].Arrows[ripArrow].Positions.GetSize();
			for (int ilPos = 0; ilPos < ilPosCount; ilPos++)
			{
				Point1 = &(pomViewer->omLines[ripLine].Arrows[ripArrow].Positions[ilPos].Start);
				Point2 = &(pomViewer->omLines[ripLine].Arrows[ripArrow].Positions[ilPos].End);
				olArrowID = pomViewer->omLines[ripLine].Arrows[ripArrow].SourceBar;

				if (point.x > (min(Point1->x, Point2->x) - 4) && point.x < (max(Point1->x, Point2->x) + 4))
				{
					if (point.y > (min(Point1->y, Point2->y) - 4) && point.y < (max(Point1->y, Point2->y) + 4))
					{
						ropArrowID = olArrowID;
						return true;
					}
				}
			}
		}
	}
	
	ripLine = -1;
	ripArrow = -1;

	return false;
}



//---------------------------------------------------------------------------------------------
//						drawing methods			
//---------------------------------------------------------------------------------------------
void RulesGantt::DrawDotLines(CDC *pDC, int itemID, const CRect &rcItem)
{
    CBitmap Bitmap;
	CTime olTime, olTmpTime;
	int ilX;

	// select dotted pen
	CPen SepPen(PS_DOT, 1, BLACK);
	COLORREF olOldBkColor = pDC->SetBkColor(SILVER);
	CPen *pOldPen = pDC->SelectObject(&SepPen);
	
	// dotted horizontal lines
	CRect olRect(rcItem);
	int ilHeight = 0;
	CRect olTmpRect = olRect;
	{
		olRect.bottom = olRect.top + GetLineHeight(itemID);
        pDC->MoveTo(olRect.left, olRect.bottom - 1);
        pDC->LineTo(olRect.right, olRect.bottom - 1);
		olRect.top += GetLineHeight(itemID);
	}    
                     
	// vertical lines
	olRect = olTmpRect;
	CPen BlackPen(PS_DOT, 1, GRAY);
	pDC->SetBkColor(GRAY);
	pDC->SelectObject(&BlackPen);
	olTime = pomTimeScale->GetDisplayStartTime();

	// calculate correct X-coordinates for first line
	olTime += CTimeSpan(0, 0, 30, 0);
	ilX = pomTimeScale->GetXFromTime(olTime) + imVerticalScaleWidth + imVerticalScaleIndent;

	// draw lines
	static int Offset = 0;
	for(int i = 0; i < 10; i++)
	{
		olTime += CTimeSpan(0, 1, 0, 0);
		//if (i == 5)
		{
			ilX = pomTimeScale->GetXFromTime(olTime) + imVerticalScaleWidth + imVerticalScaleIndent;
			pDC->MoveTo(ilX, olRect.top);
			pDC->LineTo(ilX, olRect.bottom);
		}
	}

	pDC->SelectObject(pOldPen);	
	pDC->SetBkColor(olOldBkColor);
}
//---------------------------------------------------------------------------------------------

void RulesGantt::RepaintVerticalScale(int ipLineno, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;
	
    CRect rcPaint;
    if (GetItemRect(ipLineno, &rcPaint) != LB_ERR)  // valid item in list box?
    {
        rcPaint.right = imVerticalScaleWidth - 1;
        InvalidateRect(&rcPaint, bpErase);
    }
}
//---------------------------------------------------------------------------------------------

void RulesGantt::RepaintGanttChart(int ipLineno, CTime opStartTime, CTime opEndTime, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
	{
        GetClientRect(&rcPaint);
	}
    else
	{
        GetItemRect(ipLineno, &rcPaint);
	}

    // Update the GanttChart body
    if (opStartTime <= omDisplayStart)
	{
        rcPaint.left = imVerticalScaleIndent; //imVerticalScaleWidth;
	}
    else
	{
        rcPaint.left = (int)GetX(opStartTime);
	}

    // Use the client width if user wants to repaint to the end of timescale
	CString x = omDisplayEnd.Format("%d.%m.%Y-%H:%M");
	CString y = opEndTime.Format("%d.%m.%Y-%H:%M");
    if (opEndTime != TIMENULL && opEndTime <= omDisplayEnd)
	{
        rcPaint.right = (int)GetX(opEndTime) + 1;
	}

    InvalidateRect(&rcPaint, bpErase);
}
//---------------------------------------------------------------------------------------------

void RulesGantt::RepaintItemHeight(int ipLineno)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    // This repaint method will recalculate the item height
    SetItemHeight(ipLineno, GetLineHeight(ipLineno));

    CRect rcItem, rcPaint;
    GetClientRect(&rcPaint);
    GetItemRect(ipLineno, &rcItem);
    rcPaint.top = rcItem.top;
    InvalidateRect(&rcPaint, TRUE);
}
//---------------------------------------------------------------------------------------------

void RulesGantt::RepaintLine(int ipLineno)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcItem, rcPaint;
    GetItemRect(ipLineno, &rcItem);
    InvalidateRect(&rcItem, TRUE);
}
//---------------------------------------------------------------------------------------------

void RulesGantt::ResetItemHeight(int ipLineno)
{
	// MNE, 990924: This method will recalculate the item height but not repaint it immediately
	// I need it for the connection arrow stuff, 'cause the arrows are made after making the lines, 
	// but before actually drawing anything


	if (m_hWnd == NULL) // window hasn't been opened?
        return;
	
	if (ipLineno == -1)
	{
		for (int i = 0; i < pomViewer->GetLineCount(); i++)
		{
			SetItemHeight(i, GetLineHeight(i));
		}
	}
	else
	{
		if (ipLineno >= 0 && ipLineno < pomViewer->GetLineCount())
		{
			SetItemHeight(ipLineno, GetLineHeight(ipLineno));
		}
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    // We have to define MeasureItem() here since MFC places an ASSERT inside
    // the default MeasureItem() of an owner-drawn CListBox.
}
//---------------------------------------------------------------------------------------------

void RulesGantt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// To let the GanttChart know of the changes in TimeScale  automatically,
	// we have to reset the value of omDisplayStart and omDisplayEnd every time.

	omDisplayStart = GetCTime(imVerticalScaleWidth);
	omDisplayEnd = GetCTime(imWindowWidth);
    
	int itemID = lpDrawItemStruct->itemID;
	
    // This optimization will speed up displaying of GanttChart
    // However, it only works because we hadn't displayed any selected or focussed items.
    // (We use the "imCurrentItem" variable instead)
    // So, be careful since this assumption may be wrong in future.
    //
	if (itemID == -1)
        return;
    if (!(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
        return; // nothing more to do if listbox just change its selection and/or focus item

    // Drawing routines start here
    CDC dc;
    BOOL ilTest = dc.Attach(lpDrawItemStruct->hDC);
    CRect rcItem(&lpDrawItemStruct->rcItem);
    CRect rcClip;
    int ilClipType = dc.GetClipBox(&rcClip);
	
    DrawVerticalScale(&dc, itemID, rcItem);
	DrawDotLines(&dc, itemID, rcItem);

    // Draw time lines and bars if necessary
    if (imVerticalScaleWidth <= rcClip.right)
    {
        CRgn rgn;
        CRect rect(imVerticalScaleWidth, rcItem.top, rcItem.right, rcItem.bottom);
        rgn.CreateRectRgnIndirect(&rect);
        dc.SelectClipRgn(&rgn);

		//DrawBackgroundBars(&dc, itemID, rcItem, rcClip); // not used right now
        DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
		DrawLinks(&dc, itemID, rcItem, rcClip);
		DrawConnections(&dc, itemID, rcItem, rcClip);
		DrawGanttChart(&dc, itemID, rcItem, rcClip);
		DrawUnits(&dc, itemID, rcItem, rcClip);

		if (imResizeMode != HTNOWHERE)
            DrawFocusRect(&dc, &omRectResize);

        dc.SelectClipRgn(NULL);

    }

    dc.Detach();
}
//---------------------------------------------------------------------------------------------

void RulesGantt::DrawConnections(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	CPoint olPointArr[4];

	int ilTargetLine = -1; 
	int	ilTargetBar = -1;
	int ilSourceLine = -1; 
	int	ilSourceBar = -1;
	int ilVertX = -1;
	int ilXBar = -1;
	int ilYStart = -1;
	int	ilYEnd = -1;
	int ilY1 = -1;
	
	int ilArrowCount = pomViewer->GetArrowCount(itemID);
	
	CPen *polArrowPenBlack = new CPen(PS_SOLID, 1, BLACK);
	CPen *polArrowPenRed = new CPen(PS_SOLID, 1, RED);
	CPen *polArrowPenGreen = new CPen(PS_SOLID, 1, GREEN);
	CBrush *polArrowBrush = new CBrush(BLACK);

	CPen *polOldPen = pDC->SelectObject(polArrowPenBlack);
	CBrush *polOldBrush = pDC->SelectObject(polArrowBrush);


	for (int i = 0; i < ilArrowCount; i++)
	{
		ARROWDATA *prlArrow = pomViewer->GetArrow(itemID, i);		
		BARDATA *prlBar = pomViewer->GetBar(itemID, 0);	// this is only valid as long as we have one bar per line !!! 
		
		bool blRet = true;
		blRet &= pomViewer->FindBarGlobal(prlArrow->TargetBar, ilTargetLine, ilTargetBar);
		blRet &= pomViewer->FindBarGlobal(prlArrow->SourceBar, ilSourceLine, ilSourceBar);

		if (prlBar != NULL && prlArrow != NULL && blRet)
		{
			CRect olStartCircle;
			CPoint olRoundUp;
			olRoundUp.x = 5;
			olRoundUp.y = 5;
			int ilItemHeight = rcItem.bottom - rcItem.top;

			ilVertX = GetX(prlArrow->XValue);
			ilYStart = GetBarYPos(rcItem, ilSourceLine, itemID, 0);
			
			// set arrow colors
			if (prlArrow->IsHighlighted == true)
			{
				pDC->SelectObject(polArrowPenRed);
			}
			else if (prlArrow->SameRes == true)
			{
				pDC->SelectObject(polArrowPenGreen);
			}
			else
			{
				pDC->SelectObject(polArrowPenBlack);
			}

			// calculate rest of coordinates
			switch (prlArrow->ArrowType)
			{
				case 0:		// start of arrow at left end of bar
					ilXBar = GetX(prlBar->Debe);
					if (ilTargetLine > itemID)
					{
						ilYEnd = rcItem.bottom;
					}
					else
					{
						ilYEnd = rcItem.top;
					}

					pDC->MoveTo(ilXBar, ilYStart);
					pDC->LineTo(ilVertX, ilYStart);
					pomViewer->AddPosData(itemID, i, ilXBar, ilYStart, ilVertX, ilYStart);
					pDC->LineTo(ilVertX, ilYEnd);
					pomViewer->AddPosData(itemID, i, ilVertX, ilYStart, ilVertX, ilYEnd);
					break;

				case 1:		// start of arrow at right end of bar
					ilXBar = GetX(prlBar->Deen);
					ilYEnd = rcItem.bottom;
					if (ilTargetLine > itemID)
					{
						ilYEnd = rcItem.bottom;
					}
					else
					{
						ilYEnd = rcItem.top;
					}
					
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->LineTo(ilVertX, ilYStart);
					pomViewer->AddPosData(itemID, i, ilXBar, ilYStart, ilVertX, ilYStart);
					pDC->LineTo(ilVertX, ilYEnd);
					pomViewer->AddPosData(itemID, i, ilVertX, ilYStart, ilVertX, ilYEnd);
					break;

				case 2:		// arrowhead coming from left
					ilXBar = GetX(prlBar->Debe);
					if (ilSourceLine < itemID)
					{
						ilYEnd = rcItem.top;
					}
					else
					{
						ilYEnd = rcItem.bottom;
					}
					olPointArr[0] = CPoint(ilXBar - 5, ilYStart - 4);
					olPointArr[1] = CPoint(ilXBar, ilYStart);
					olPointArr[2] = CPoint(ilXBar - 5, ilYStart + 4);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->Polygon(olPointArr, 3);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->LineTo(ilVertX, ilYStart);
					pomViewer->AddPosData(itemID, i, ilXBar, ilYStart, ilVertX, ilYStart);
					pDC->LineTo(ilVertX, ilYEnd);
					pomViewer->AddPosData(itemID, i, ilVertX, ilYStart, ilVertX, ilYEnd);
					break;

				case 3:		// arrowhead coming from right
					ilXBar = GetX(prlBar->Deen);
					if (ilSourceLine > itemID)
					{
						ilYEnd = rcItem.bottom;
					}
					else
					{
						ilYEnd = rcItem.top;
					}
					olPointArr[0] = CPoint(ilXBar, ilYStart);
					olPointArr[1] = CPoint(ilXBar + 5, ilYStart - 4);
					olPointArr[2] = CPoint(ilXBar + 5, ilYStart);
					olPointArr[3] = CPoint(ilXBar + 5, ilYStart + 4);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->Polygon(olPointArr, 4);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->LineTo(ilVertX, ilYStart);
					pomViewer->AddPosData(itemID, i, ilXBar, ilYStart, ilVertX, ilYStart);
					pDC->LineTo(ilVertX, ilYEnd);
					pomViewer->AddPosData(itemID, i, ilVertX, ilYStart, ilVertX, ilYEnd);
					break;

				case 4:		// shaft of arrow horizontically passing bar (increased line height), head from left side
					ilXBar = GetX(prlBar->Debe);
					if (ilSourceLine < itemID)
					{
						ilY1 = (int)(rcItem.top + (ilItemHeight - ilItemHeight / dmArrowHeightFactor) / 2);
						ilYEnd = rcItem.top;
					}
					else
					{
						ilY1 = (int)(rcItem.bottom - (ilItemHeight - ilItemHeight / dmArrowHeightFactor) / 2);
						ilYEnd = rcItem.bottom;
					}
					olPointArr[0] = CPoint(ilXBar, ilYStart);
					olPointArr[1] = CPoint(ilXBar - 5, ilYStart - 4);
					olPointArr[2] = CPoint(ilXBar - 5, ilYStart + 4);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->Polygon(olPointArr, 3);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->LineTo(ilXBar - 15, ilYStart);
					pomViewer->AddPosData(itemID, i, ilXBar, ilYStart, ilXBar - 15, ilYStart);
					pDC->LineTo(ilXBar - 15, ilY1);
					pomViewer->AddPosData(itemID, i, ilXBar - 15, ilYStart, ilXBar - 15, ilY1);
					pDC->LineTo(ilVertX, ilY1);
					pomViewer->AddPosData(itemID, i, ilXBar - 15, ilY1, ilVertX, ilY1);
					pDC->LineTo(ilVertX, ilYEnd);
					pomViewer->AddPosData(itemID, i, ilVertX, ilY1, ilVertX, ilYEnd);
					break;

				case 5:		// shaft of arrow horizontically passing bar (increased line height), head from right side
					ilXBar = GetX(prlBar->Deen);
					if (ilSourceLine < itemID)
					{
						ilY1 = (int)(rcItem.top + (ilItemHeight - ilItemHeight / dmArrowHeightFactor) / 2);
						ilYEnd = rcItem.top;
					}
					else
					{
						ilY1 = (int)(rcItem.bottom - (ilItemHeight - ilItemHeight / dmArrowHeightFactor) / 2);
						ilYEnd = rcItem.bottom;
					}
					olPointArr[0] = CPoint(ilXBar, ilYStart);
					olPointArr[1] = CPoint(ilXBar + 5, ilYStart - 4);
					olPointArr[2] = CPoint(ilXBar + 5, ilYStart + 4);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->Polygon(olPointArr, 3);
					pDC->MoveTo(ilXBar, ilYStart);
					pDC->LineTo(ilXBar + 15, ilYStart);
					pomViewer->AddPosData(itemID, i, ilXBar, ilYStart, ilXBar + 15, ilYStart);
					pDC->LineTo(ilXBar + 15, ilY1);
					pomViewer->AddPosData(itemID, i, ilXBar + 15, ilYStart, ilXBar + 15, ilY1);
					pDC->LineTo(ilVertX, ilY1);
					pomViewer->AddPosData(itemID, i, ilXBar + 15, ilY1, ilVertX, ilY1);
					pDC->LineTo(ilVertX, ilYEnd);
					pomViewer->AddPosData(itemID, i, ilVertX, ilY1, ilVertX, ilYEnd);
					break;

				case 6:		// shaft of arrow going vertically through line
					ilYStart = rcItem.top;
					ilYEnd = rcItem.bottom;
					pDC->MoveTo(ilVertX, ilYStart);
					pDC->LineTo(ilVertX, ilYEnd);
					pomViewer->AddPosData(itemID, i, ilVertX, ilYStart, ilVertX, ilYEnd);
					break;

				default:
					break;
			}
		}
	}

	pDC->SelectObject(polOldPen);
	pDC->SelectObject(polOldBrush);

	delete polArrowPenBlack;
	delete polArrowPenRed;
	delete polArrowPenGreen;
	delete polArrowBrush;
}
//---------------------------------------------------------------------------------------------

// this function actually draws the background rectangles for linked groups of RUDs
void RulesGantt::DrawLinks(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	BARDATA *prlBar = pomViewer->GetBar(itemID, 0);	// this is only valid as long as we have one bar per line !!! 
	if (prlBar != NULL)
	{
		if (prlBar->Ulnk.IsEmpty() == FALSE)
		{
			CRect olRect;
			olRect = GetDrawingRect(prlBar->Ulnk, rcItem);
			
			long llColor = pomViewer->GetLinkBkColor(prlBar->Ulnk);

			if (llColor == -1)
			{
				llColor = pomViewer->CreateLinkBkColor(prlBar->Ulnk);
				TRACE("[RulesGantt::DrawLinks] Urno: %s, ColorIdx: %d", prlBar->Ulnk, llColor);
			}
			
			CBrush *polLinkBrush = new CBrush(rmLinkColors[llColor]);
			CBrush *polOldBrush = pDC->SelectObject(polLinkBrush);
			
			pDC->FillRect(olRect, polLinkBrush);

			pDC->SelectObject(polOldBrush);
			delete polLinkBrush;
		}
	}
}
//---------------------------------------------------------------------------------------------

void RulesGantt::DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem)
{
	CPoint point;
	int	ilx;
    ::GetCursorPos(&point);
    ScreenToClient(&point);


    // Check if we need to change the highlight text in VerticalScale
    BOOL blHighlight;
    if (!bmIsMouseInWindow || !rcItem.PtInRect(point))
        blHighlight = FALSE;
    else
    {
        blHighlight = TRUE;
        if (imHighlightLine != itemID)
            RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = itemID;
    }

    // Drawing routines start here
	COLORREF rlTextColor, rlBkColor;
    CString olVertScaleText;
	olVertScaleText = pomViewer->GetColors(itemID, &rlTextColor, &rlBkColor);
    CFont *pOldFont = pDC->SelectObject(pomVerticalScaleFont);

	COLORREF nOldTextColor;
	COLORREF nOldBackgroundColor;
	if (blHighlight)
	{
		nOldTextColor = pDC->SetTextColor(ogColors[WHITE_IDX]);
		nOldBackgroundColor = pDC->SetBkColor(ogColors[DARKBLUE_IDX]);
	}
	else
	{
		nOldTextColor = pDC->SetTextColor(rlTextColor);
		nOldBackgroundColor = pDC->SetBkColor(rlBkColor);
	}

    CRect rect(rcItem);
	CRect textRect = rect;

    int left = rect.left + imVerticalScaleIndent;
	rect.right = imVerticalScaleWidth - 2;
    
	ilx = 2;
	textRect.right = (int)(imVerticalScaleWidth-2);
	textRect.left = ilx;
	textRect.top += 2;
	textRect.bottom -= 2;//1;
	pDC->ExtTextOut(ilx, textRect.top, ETO_CLIPPED | ETO_OPAQUE, &textRect, olVertScaleText, lstrlen(olVertScaleText), NULL);

	CRect olDotRect = textRect;
	olDotRect.top -= 2;
	olDotRect.bottom += 2;
	CPen BlackPen(PS_SOLID/*PS_DOT*/, 1, BLACK);
	CPen *pOldPen = pDC->SelectObject(&BlackPen);
	pDC->MoveTo(olDotRect.left, olDotRect.bottom-1);
	pDC->LineTo(olDotRect.right-1, olDotRect.bottom-1);

	pDC->SetTextColor(nOldTextColor);
	pDC->SetBkColor(nOldBackgroundColor);
	pDC->SelectObject(pOldPen);
}
//---------------------------------------------------------------------------------------------

void RulesGantt::DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;
	
    // Draw each bar if it is in the range of the clipped box
	LINEDATA *prlLine = pomViewer->GetLine(itemID);

	int ilBarCount = pomViewer->GetBarCount(itemID);
    for (int i = 0; i < ilBarCount; i++)
    {
		BARDATA *prlBar = pomViewer->GetBar(itemID, i);

        if (!IsOverlapped(prlBar->Debe, prlBar->Deen, omDisplayStart, omDisplayEnd))
            continue;   // skip bar which is out of time range
		
		CCSPtrArray <CCSBarDecoStruct> olDecoData;
		CCSBarDecoStruct rlBarDeco;
		
		int left = (int)GetX(prlBar->Debe);
        int right = (int)GetX(prlBar->Deen);
		int top = 0;
		int bottom = 0;
		
		// MNE TEST
		/*TRACE("[RulesGantt::DrawGanttChart] Bar: %s, Start: %s (%d), End: %s (%d)\n", 
			prlBar->Urno, (prlBar->Debe).Format("%H:%M:%S"), left, (prlBar->Deen).Format("%H:%M:%S"), right);*/
		// END TEST

		if (prlLine->ArrowPass == 2)	// arrow passing horizontically above bar(s)
		{
			bottom = rcItem.bottom - imGutterHeight - (prlBar->OverlapLevel * imOverlapHeight);
			top = bottom - imBarHeight;
		}
		else	// no arrow passing or passing below bar(s)
		{
			top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
			bottom = top + imBarHeight;
		}
			
		// bar itself
		rlBarDeco.Rect = CRect(left, top, right, bottom);
		rlBarDeco.Type = BD_CCSBAR;
		rlBarDeco.Text = prlBar->Text;
		rlBarDeco.BkColor = prlBar->BkColor;
		rlBarDeco.TextColor = prlBar->TextColor;
		rlBarDeco.HorizontalAlignment = BD_CENTER;
		rlBarDeco.VerticalAlignment = BD_CENTER;
		rlBarDeco.MarkerBrush = prlBar->MarkerBrush;
		rlBarDeco.MarkerType = prlBar->MarkerType;
		rlBarDeco.TextFont = pomGanttChartFont;
		olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
		
		// frame around bar
		rlBarDeco.Type = BD_FRAME;
		right++;
		rlBarDeco.Rect = CRect(left, top, right, bottom);
		rlBarDeco.Text = CString("");
		rlBarDeco.BkColor = BLACK;
		rlBarDeco.FrameType = FRAMERECT;
		olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);

		// lines inside bar
		CPen olPen1(PS_SOLID, 2, GREEN);
		CPen olPen2(PS_SOLID, 2, ROSE);
		
		int ilPrep = 0;
		int ilFupt = 0;
		int ilWayt = 0;
		int ilWayf = 0;
		
		// preparation time
		rlBarDeco.pPen = &olPen1;
		rlBarDeco.Type = BD_LINE;
		ilPrep = GetX(prlBar->Prep);
		if (ilPrep != left)
		{
			rlBarDeco.PointFrom = CPoint(ilPrep, top + 1);
			rlBarDeco.PointTo = CPoint(ilPrep, bottom - 2);
			olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
		}

		// follow-up-treatment
		ilFupt = GetX(prlBar->Fupt);
		if (ilFupt != right - 1)
		{
			rlBarDeco.PointFrom = CPoint(ilFupt, top + 1);
			rlBarDeco.PointTo = CPoint(ilFupt, bottom - 2);
			olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
		}
		
		 // way there
		ilWayt = GetX(prlBar->Wayt);
		rlBarDeco.pPen = &olPen2;
		if (ilWayt != ilPrep)
		{
			rlBarDeco.PointFrom = CPoint(ilWayt, top + 1);
			rlBarDeco.PointTo = CPoint(ilWayt, bottom - 2);
			olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
		}

		// way back
		ilWayf = GetX(prlBar->Wayf);
		if (ilWayf != ilFupt)
		{
			rlBarDeco.PointFrom = CPoint(ilWayf, top + 1);
			rlBarDeco.PointTo = CPoint(ilWayf, bottom - 2);
			olDecoData.NewAt(olDecoData.GetSize(), rlBarDeco);
		}
		
		// corner for partial demand
		/*
		if (prlBar->Ffpd)
		{
			CPoint olPoints[3];

			// triangle in upper right corner
			olPoints[0].x = right;
			olPoints[0].y = top;

			olPoints[1].x = right; 
			olPoints[1].y = bottom-1;
			
			olPoints[2].x = right-(bottom-top-1);
			olPoints[2].y = top;

			CCSBarDecoStruct *prlDeco = new CCSBarDecoStruct;
			prlDeco->Type = BD_REGION;

			CRgn *polRgn = new CRgn;
			polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
			prlDeco->Region = polRgn;

			olDecoData.Add(prlDeco);
		}
		*/
		// MNE TEST
		// TRACE("P: %d, Wt: %d, Wf: %d, F: %d\n", ilPrep, ilWayt, ilWayf, ilFupt);
		// END TEST
		
		if (IsOverlapped(left, right, rcClip.left, rcClip.right))
			CCSGanttBar olPaintBar(pDC,olDecoData);
		olDecoData.DeleteAll();
    }
}
//---------------------------------------------------------------------------------------------

void RulesGantt::DrawTimeLines(CDC *pDC, int top, int bottom)
{
    // Draw current time
    if (omCurrentTime != TIMENULL && IsBetween(omCurrentTime, omDisplayStart, omDisplayEnd))
    {
        CPen penRed(PS_SOLID, 0, RGB(255, 0, 0));
        CPen *pOldPen = pDC->SelectObject(&penRed);

        int x = (int)GetX(omCurrentTime);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time start
    if (omMarkTimeStart != TIMENULL && IsBetween(omMarkTimeStart, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);

        int x = (int)GetX(omMarkTimeStart);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time end
    if (omMarkTimeEnd != TIMENULL && IsBetween(omMarkTimeEnd, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeEnd);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }
}
//---------------------------------------------------------------------------------------------

void RulesGantt::DrawFocusRect(CDC *pDC, const CRect &rect)
{
	CRect rcFocus = rect;
	rcFocus.right++;	// extra pixel to help DrawFocusRect() work correctly
	pDC->DrawFocusRect(&rcFocus);
}



//---------------------------------------------------------------------------------------------
//						updating methods
//---------------------------------------------------------------------------------------------

// Update every necessary GUI elements for each mouse/timer event
void RulesGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
    int itemID = GetItemFromPoint(point);

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            //((CButtonListDialog *)AfxGetMainWnd())->RegisterTimer(this);    // install the timer
        }

        if (itemID != imHighlightLine)  // move to new bar?
            RepaintVerticalScale(itemID, FALSE);
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
            UpdateBarStatus(itemID, -1);    // moving on vertical scale, not a bar
        else
        {
            int ilBarno;
            if (!bpIsControlKeyDown)
                ilBarno = GetBarnoFromPoint(itemID, point);
            else
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
            UpdateBarStatus(itemID, ilBarno);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
    if (bmIsControlKeyDown && !bpIsControlKeyDown)              // no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        ;   // do noting
    else if (GetHit(itemID, imCurrentBar, point) == HTCAPTION) // body?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZE));
    else                                                        // left/right border?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));

    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
}
//---------------------------------------------------------------------------------------------

// Update the frame window's status bar
void RulesGantt::UpdateBarStatus(int ipLineno, int ipBarno)
{
    CString s;

    if (ipLineno == imHighlightLine && ipBarno == imCurrentBar)	// user still be on the old bar?
		return;

    if (ipLineno == -1)	// there is no more bar status?
        s = "";
    else if (ipBarno == -1)	// user move outside the bar?
        s = "";
	else
        s = pomViewer->GetStatusBarText(ipLineno, ipBarno);

	// display status bar
    if (pomStatusBar != NULL)
        pomStatusBar->SetWindowText(s);

    imCurrentBar = ipBarno;
}



//---------------------------------------------------------------------------------------------
//						moving / resizing methods
//---------------------------------------------------------------------------------------------
// Start moving/resizing
bool RulesGantt::BeginMovingOrResizing(CPoint point)
{
    int itemID = GetItemFromPoint(point);
	CClientDC dc(this);
    int ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
    UpdateBarStatus(itemID, ilBarno);

    if (ilBarno != -1)
        imResizeMode = GetHit(itemID, ilBarno, point);

    if (imResizeMode != HTNOWHERE)
    {
        SetCapture();	// deliver all mouse input to this window
        if (imResizeMode == HTCAPTION)
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZE));
        else
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));

        // Calculate size of current bar
        BARDATA *prlBar = pomViewer->GetBar(itemID, ilBarno);
        CRect rcItem;
        GetItemRect(itemID, &rcItem);
        int left = (int)GetX(prlBar->Debe);
        int right = (int)GetX(prlBar->Deen);
        int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;
		bmActiveBarSet = TRUE;  // Save bar data for moving and sizing
		rmActiveBar = *prlBar;	// copy all data of this bar
		
		if(prmPreviousBar != NULL)
		{
			*prmPreviousBar = *prlBar;
		}

		omPointResize = point;
		omRectResize = CRect(left, top, right, bottom);

		// MNE TEST
		/*CTime olS;
		CTime olE;
		olS = GetCTime(omRectResize.left);
		olE = GetCTime(omRectResize.right);*/
		// END TEST
		
		DrawFocusRect(&dc, &omRectResize);	// first time focus rectangle
    }

    return (imResizeMode != HTNOWHERE);
}
//---------------------------------------------------------------------------------------------

// Update the resizing/moving rectangle
bool RulesGantt::OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown)
{
    CClientDC dc(this);
	
    switch (imResizeMode)
    {
    case HTCAPTION: // moving mode
        omRectResize.OffsetRect(point.x - omPointResize.x, 0);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
        break;
    case HTLEFT:    // resize left border
        omRectResize.left = min(point.x, omRectResize.right);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
		break;
    case HTRIGHT:   // resize right border
        omRectResize.right = max(point.x, omRectResize.left);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
        break;
    }

    if (bpIsLButtonDown)    // still resizing/moving?
    {
        DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle
		
		omPointResize = point;
		
		CTime olS;
		CTime olE;
		olS = GetCTime(omRectResize.left);
		olE = GetCTime(omRectResize.right);
		// TRACE("[RulesGantt::OnMovingOrResizing] Rect.left = %d (Zeit: %s), right = %d (Zeit: %s)\n", omRectResize.left, olS.Format("%d.%m.%y - %H:%M:%S"), omRectResize.right, olE.Format("%d.%m.%y - %H:%M:%S"));
		
		int ilOfBl = GetX(pomViewer->omOfBlock);
		int ilOnBl = GetX(pomViewer->omOnBlock);
		ogDataSet.ChangeRudTime(rmActiveBar.Urno, omRectResize.left, omRectResize.right, imResizeMode, ilOnBl, ilOfBl);

		// send DDX message, which causes gantt to be redrawn
		RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
		ogBCD.GetRecord("RUD_TMP", "URNO", rmActiveBar.Urno, olRecord);
		ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRecord);
    }
    else
    {
        imResizeMode = HTNOWHERE;   // no more moving/resizing

		if (bmActiveBarSet)
		{
			// change data in RUD_TMP object
			/*
			// ************ ZUM TESTEN AUSKOMMENTIERT *************
			CTime olS;
			CTime olE;
			olS = GetCTime(omRectResize.left);
			olE = GetCTime(omRectResize.right);
			ogDataSet.ChangeRudTime(blRight, rmActiveBar.Urno, olS, olE, pomViewer->omOnBlock, pomViewer->omOfBlock);*/
			int ilOfBl = (omDisplayStart - pomViewer->omOfBlock).GetTotalSeconds();
			int ilOnBl = (omDisplayStart - pomViewer->omOnBlock).GetTotalSeconds();
			ogDataSet.ChangeRudTime(rmActiveBar.Urno, omRectResize.left, omRectResize.right, imResizeMode, ilOnBl, ilOfBl);
			
			// send DDX message, which causes gantt to be redrawn
			RecordSet olRecord(ogBCD.GetFieldCount("RUD_TMP"));
			ogBCD.GetRecord("RUD_TMP", "URNO", rmActiveBar.Urno, olRecord);
			ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRecord);
			
			// reset temporary data
			bmActiveBarSet = FALSE;
			imCurrentBar = -1;
		}
		
		//--- release mouse capture and reload arrow cursor
        ReleaseCapture();
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    }
    return (imResizeMode != HTNOWHERE);

}



//---------------------------------------------------------------------------------------------
//					drag-and-drop functionalities
//---------------------------------------------------------------------------------------------
void RulesGantt::DragServiceBarBegin(int ipLineno, int ipBarno)
{
	::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);

	char pclDebe[20] = "";

	//--- write bar urno to m_DragDropSource and begin drag
	CString olUrno = (pomViewer->GetBar(ipLineno, ipBarno)->Debe).Format("%d%m%Y%H%M");
	m_DragDropSource.AddDWord(atol(olUrno));
	m_DragDropSource.CreateDWordData(DIT_SERLIST, 1);
	m_DragDropSource.BeginDrag();
	//m_DragDropSource.AddString(olUrno);
}
//---------------------------------------------------------------------------------------------

LONG RulesGantt::OnDragOver(UINT wParam, LONG lParam)
{
    ::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    

	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

	int ilClass = m_DragDropTarget.GetDataClass(); 
	
	if (ilClass == DIT_SERLIST)
	{
		return 0;
	}
	return -1L; 



	/*::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    
    
	if ((omDropPosition.x > 0) && (imVerticalScaleWidth > omDropPosition.x))
	{
		DrawThickLine();
		return 0L;	// could be dropped here 
	}
	
	return -1L;	// cannot accept this object*/
}
//---------------------------------------------------------------------------------------------

LONG RulesGantt::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *polDragDropCtrl = &m_DragDropTarget;
	CStringArray olSerUrnos;
	CString	olTplUrno, olTplDalo, *polAloc=0;

	for (int ilIndex = 0; ilIndex < polDragDropCtrl->GetDataCount(); ilIndex++)
	{
		CString olUrno;
		olUrno.Format("%d", polDragDropCtrl->GetDataDWord(ilIndex));
		if (olUrno != "" && olUrno != " ")
		{
			olSerUrnos.Add(olUrno);
		}
	}
	pomRegelWerkView->AddServices(olSerUrnos);

    return -1L;
}
//---------------------------------------------------------------------------------------------


BOOL RulesGantt::IsDropOnDutyBar(CPoint point)
{
	// Return true if the mouse position in on the vertical scale or a duty bar.
	// Also remember the place where the drop happened in "imDropLineno".

	int ilLineno;
	if ((ilLineno = GetItemFromPoint(point)) == -1)
		return FALSE;	// cannot find a line of this diagram

	if (point.x < imVerticalScaleWidth - 2)
		return TRUE;	// mouse is on the vertical scale

	// If it reaches here, mouse is on the background of the GanttChart.
	return FALSE;
}



//---------------------------------------------------------------------------------------------
//					processing methods
//---------------------------------------------------------------------------------------------
LONG RulesGantt::ProcessDropDutyBar(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	return 0L;
}


int RulesGantt::OnToolHitTest(CPoint point, TOOLINFO* pTI) const
{
	int			itemID = GetItemFromPoint(point);
	LINEDATA	*pslLineDat;
	static int         ilLastToolTip=-1;

	
	if ( ( itemID > -1) && ( point.x < imVerticalScaleWidth ) &&
		 ( itemID < pomViewer->GetLineCount() ) &&
		 ( pslLineDat = pomViewer->GetLine(itemID) ) &&
		 !pslLineDat->Name.IsEmpty() 
	   )
	{
		if ( itemID != ilLastToolTip )
		{
			ilLastToolTip = itemID;
			return -1;
		}
		pTI->lpszText = new char[pslLineDat->Name.GetLength()+1];
		pTI->uFlags = TTF_NOTBUTTON | TTF_IDISHWND;
		strcpy ( pTI->lpszText, pslLineDat->Name );
		pTI->uId = (UINT)m_hWnd;
		pTI->hwnd = ::GetParent(m_hWnd);
		return 1;
    }  
	else 
		return -1;

}

void RulesGantt::EnableEditRud ( bool bpEnable )
{
	bmEditEnabled = bpEnable;
}

void RulesGantt::OnMenuCopyBar()
{
	
	CCS_TRY


	if(omCurrRudUrno != "-1")
	{
		// get record (we need this for our DDX call)
		CString olNewRudUrno;
		if ( ogDataSet.CopyOneRud ( omCurrRudUrno, olNewRudUrno ) )
		{	
			pomViewer->RecalculateDisp();
			pomViewer->ChangeViewTo ( pomRegelWerkView->omCurrRueUrno );
		}
	}

	
	CCS_CATCH_ALL
}

void RulesGantt::OnMenuCreateUnit()
{
	CString olActUses, olUses;
	int		i, ilAnz;
	RecordSet olRud(ogBCD.GetFieldCount("RUD"));

	if ( igRudUsesIdx<0 )
		return ;
	ilAnz = ogSelectedRuds.GetSize();
	//  is there already an USES link in the selected RUDs then use this one
	for (i = 0; olActUses.IsEmpty() && (i<ilAnz); i++)
	{
		olUses = ogBCD.GetField("RUD_TMP", "URNO", ogSelectedRuds.GetAt(i), "USES" );
		if ( !olUses.IsEmpty() )
			olActUses = olUses;
	}
	if ( olActUses.IsEmpty() )
		olActUses.Format("%ld", ogBCD.GetNextUrno() );

	for ( i = 0; i<ilAnz; i++)
	{ 
		ogBCD.GetRecord("RUD_TMP", "URNO", ogSelectedRuds.GetAt(i), olRud);
		olRud.Values[igRudUsesIdx] = olActUses;
		ogBCD.SetRecord("RUD_TMP", "URNO", olRud.Values[igRudUrnoIdx], olRud.Values);
	//	ogDdx.DataChanged((void *)this,RUD_TMP_CHANGE,(void *)&olRud);
	}
	pomViewer->MakeUnits();
	if ( ilAnz > 0 )
		ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRud);
}
//---------------------------------------------------------------------------------------------

// this function deletes a Unit between different RUDs by emptying RUD.USES
void RulesGantt::OnMenuDeleteUnit()
{
	RecordSet	olRud(ogBCD.GetFieldCount("RUD"));
	CString		olUses="", olUrno;

	/* get USES of Unit */
	for (int i = 0; i<ogSelectedRuds.GetSize(); i++)
	{
		olUses = ogBCD.GetField("RUD_TMP", "URNO", ogSelectedRuds.GetAt(i), "USES");
		if ( atol ( olUses ) > 0 )
			break;
	}
	if ( atol ( olUses ) > 0 )
		for (int i = 0; i<ogBCD.GetDataCount("RUD_TMP"); i++)
		{
			ogBCD.GetRecord("RUD_TMP", i, olRud);
			olUrno = olRud.Values[igRudUrnoIdx];
			if ( olRud.Values[igRudUsesIdx] == olUses )
			{
				olRud.Values[igRudUsesIdx].Empty();
				ogBCD.SetRecord("RUD_TMP", "URNO", olUrno, olRud.Values);
			}
		}
	// tell viewer that the records were changed
	pomViewer->MakeUnits();
	ogDdx.DataChanged((void *)this,RUD_CON_CHANGE,(void *)&olRud);
}

//---------------------------------------------------------------------------------------------

// this function actually draws the lines for Units ( RUDs linked by USES )
void RulesGantt::DrawUnits(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	int			ilLineno, ilBarno;
	CPen		olPenRed (PS_SOLID, 2, RED);
    CBrush		olBrushRed(RED);
	UNITDATA	*prlUnit;
	CRect		olBar1Rect, olBar2Rect;
	POINT 		rlStart, rlEnd;

    CPen *pOldPen = pDC->SelectObject(&olPenRed);
    CBrush *pOldBrush = pDC->SelectObject(&olBrushRed);

	for ( int i=0; i<pomViewer->omUnits.GetSize(); i++ )
	{
		prlUnit = &(pomViewer->omUnits[i]);
		if ( !pomViewer->FindBarGlobal(prlUnit->EquBar, ilLineno, ilBarno)  ||
			 !GetBarRect(ilLineno, ilBarno, olBar1Rect ) )
			 continue;
		if ( !pomViewer->FindBarGlobal(prlUnit->StaffBar, ilLineno, ilBarno)  ||
			 !GetBarRect(ilLineno, ilBarno, olBar2Rect ) )
			 continue;
		rlStart.x = (int)GetX(prlUnit->XStart);
		rlEnd.x = (int)GetX(prlUnit->XEnd);
		rlStart.y = olBar1Rect.top + imBarHeight/2;
		rlEnd.y = olBar2Rect.top + imBarHeight/2;

		pDC->MoveTo(rlStart);
		pDC->LineTo(rlEnd);
		pDC->Ellipse( rlStart.x-3, rlStart.y-3, rlStart.x+3, rlStart.y+3 );
		pDC->Ellipse( rlEnd.x-3, rlEnd.y-3, rlEnd.x+3, rlEnd.y+3 );
	}
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}



bool RulesGantt::GetBarRect(int ipLineno, int ipBarno, CRect &ropRect )
{

    // Draw each bar if it is in the range of the clipped box
	LINEDATA *prlLine = pomViewer->GetLine(ipLineno);
	BARDATA *prlBar = pomViewer->GetBar(ipLineno, ipBarno);

	if ( !prlLine || !prlBar )
		return false;

	if ( LB_ERR == GetItemRect(ipLineno, ropRect ) )
		return false;

	ropRect.left = (int)GetX(prlBar->Debe);
	ropRect.right = (int)GetX(prlBar->Deen);
	if (prlLine->ArrowPass == 2)	// arrow passing horizontically above bar(s)
	{
		ropRect.bottom -= imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
		ropRect.top = ropRect.bottom - imBarHeight;
	}
	else	// no arrow passing or passing below bar(s)
	{
		ropRect.top += imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
		ropRect.bottom = ropRect.top + imBarHeight;
	}
	return true;			
}
