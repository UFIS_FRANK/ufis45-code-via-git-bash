#if !defined(AFX_DIALOG_TemplateEDITOR_H__8ADC7D80_B698_11D2_AAF5_00001C018CF3__INCLUDED_)
#define AFX_DIALOG_TemplateEDITOR_H__8ADC7D80_B698_11D2_AAF5_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dialog_TemplateEditor.h : header file

//////////////////////////////////////////////////////////////////////////////////
//
// Dialog_TemplateEditor.h : header file
//
// Notes: The CDialog_TemplateEditor class is derived from the standard CDialog class . 
//         
//        The class offers the possibility to create Templates for the display in the main
//        FormView Window . The Dialog offers all possible fields from the TSR table in a gridform
//        display on the left hand side for the 3 areas ( turnaround, arrival, departure ) .
//         
//        From these lists the apropriate fields can be choosen and will then appear in the corresponding 
//        grid on the right handside .
//
//        The selections can be sorted alphabeticly and lines in the grids can be moved in order to 
//        represent the priority by position .
//
//        The finiched selections are stored in the TPL table .
//
//
// Date : March 1999
//
// Author : EDE 
//
//////////////////////////////////////////////////////////////////////////////////
//
// Modification History:
// =====================
//
//	MNE, Apr 28, 1999:	
//			- removed radio buttons
//			- inserted bitmap buttons (including con-/destruction, message handling, etc.)
//
//
//
///////////////////////////////////////////////////////////////////////////////////

#include <DialogGrid.h>


#define DELETED    -3
#define UNKNOWN    -2
#define UNCHANGED  -1


enum RULE_TYPE { SINGLE, MULTIPLE, INDEPENDENT };

typedef struct Indices
{
	int OldIdx;
	int NewIdx;

	Indices ()
	{
		OldIdx = UNKNOWN;
		NewIdx = UNKNOWN;
	}
} INDICES;


class CDialog_TemplateEditor : public CDialog
{
// Construction
public:
	CDialog_TemplateEditor(CWnd* pParent = NULL);   // standard constructor
	~CDialog_TemplateEditor();	// destruction

	bool InitializeTable(CGridFenster *popLeftTable, CGridFenster *popRightTable ); // Tabellen anlegen
	bool FillTable(CGridFenster *popLeftTable, CGridFenster *popRightTable, const CString &ropWhichOne); // Tabellen f�llen
	

protected:
	void SetEvaluationOrder(int ipEvalOrder);
	void DrawBitmapButtons();
	bool CheckTotalItemCount(int &ipCount, int ipAdd = 0);
	bool GetCurrentUrno(CString &ropUrno);
	bool SaveChanges();
	bool DoSave();
	void ResetTable(CGridFenster *popTable);
	void ResetAllTables();
	void ResizeWindow();
	void IniStatics();
	bool IsRowRemovable(CGridFenster *popGrid, int ipLine);
	void ShowCannotRemoveFieldMsg(CGridFenster *popGrid, int ipLine);

// Dialog Data
	//{{AFX_DATA(CDialog_TemplateEditor)
	enum { IDD = IDD_GENERATE_TMP_DLG };
	CListBox	m_FtypesOutLB;
	CListBox	m_FtypesInLB;
	CComboBox	m_Cob_Relx;
	CComboBox	m_Cob_Alod;
	CButton	m_Chb_Deacti;
	CButton	m_SaveBtn;
	CComboBox	m_TemplateList;
	int			imFlugUnabhaengig;
	BOOL	m_Chb_Deacti_Val;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialog_TemplateEditor)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void OnGridRowDelete(CGridFenster *popLeftGrid, CGridFenster *popRightGrid);
	void OnGridRowAdd(CGridFenster *popLeftGrid, CGridFenster *popRightGrid);
	bool CheckAndChangePrioStrings(CString opFldt, CString opFlda, CString opFldd, int ipEvor);

	CString GetPartialPrioString(int ipTyp, int ipCount, CString opOldStr, CString opNewStr, CString opOldPrioString);
	CString CalcPartialPrioString(int ipCount, CString opTplFldx, CString opRueEvtx);
	


	//---- 6 Grid Auswahl Fenster ----
    CGridFenster *pomLeftArrGrid;
    CGridFenster *pomLeftDepGrid;
    CGridFenster *pomLeftRotGrid;

    CGridFenster *pomRightArrGrid;
    CGridFenster *pomRightDepGrid;
    CGridFenster *pomRightRotGrid;


	//--- bitmap buttons
	CBitmapButton *pomHL2RBtn;
	CBitmapButton *pomVL2RBtn;
	CBitmapButton *pomHR2LBtn;
	CBitmapButton *pomVR2LBtn;


	// CComboBox *pomTemplateListCombo;

	//--- template urno
	CString omCurrentUrno;	// Urno of currently selected template

	CStringArray omList;

	int	  imDefaultHeight;
	int imIdx;

	// Generated message map functions
	//{{AFX_MSG(CDialog_TemplateEditor)
	virtual BOOL OnInitDialog();
	afx_msg void OnNew();
	afx_msg void OnArrAdd();
	afx_msg void OnArrDelete();
	afx_msg void OnRotAdd();
	afx_msg void OnRotDelete();
	afx_msg void OnSelchangeTemplateName();
	afx_msg void OnEditchangeTemplateName();
	afx_msg void OnDropdownTemplateName();
	afx_msg void OnDelete();
	afx_msg void OnSave();
	afx_msg void OnClose();
	afx_msg void OnDepAdd();
	afx_msg void OnDepDelete();
	afx_msg void OnHLeft2Right();
	afx_msg void OnVLeft2Right();
	afx_msg void OnHRight2Left();
	afx_msg void OnVRight2Left();
	afx_msg void OnCopy();
	afx_msg void OnGridFeld(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSelchangeAllocation();
	afx_msg void OnDeactivate();
	afx_msg void OnRadioFlugabhaengig();
	afx_msg void OnSelchangeCobRelx();
	afx_msg void OnSelchangeFtypes();
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()

private:
		
	bool bmChangeFlag;
	int imCurrSel;
	int imFisu;
	bool bmRenamed;

	CString omFldt, omFlda, omFldd;

};

bool GetSelectedTplUrno(CComboBox &ropCb, CString &ropUrno, 
						  BOOL bpIndep, bool bpMeldError = false);
CString SaveCorrespTpl(CString &ropCorrespUrno);
CString GetCorrespTplUrno(CString &ropUrno);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOG_TemplateEDITOR_H__8ADC7D80_B698_11D2_AAF5_00001C018CF3__INCLUDED_)
