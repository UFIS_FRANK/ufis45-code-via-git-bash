# Microsoft Developer Studio Project File - Name="RegelWerk" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=RegelWerk - Win32 WatsonDebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "RegelWerk.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "RegelWerk.mak" CFG="RegelWerk - Win32 WatsonDebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "RegelWerk - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "RegelWerk - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "RegelWerk - Win32 WatsonDebug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "RegelWerk - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\ufis_bin\Release"
# PROP Intermediate_Dir "c:\ufis_intermediate\Regelwerk\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\Release\ufis32.lib C:\Ufis_Bin\ClassLib\Release\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "RegelWerk - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\ufis_bin\Debug"
# PROP Intermediate_Dir "c:\ufis_intermediate\Regelwerk\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /Gi /GR /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib wsock32.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT LINK32 /profile /map

!ELSEIF  "$(CFG)" == "RegelWerk - Win32 WatsonDebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "RegelWerk___Win32_WatsonDebug"
# PROP BASE Intermediate_Dir "RegelWerk___Win32_WatsonDebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\ufis_bin\Debug"
# PROP Intermediate_Dir "c:\ufis_intermediate\Regelwerk\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /Gi /GR /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /GR /GX /Z7 /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Fr /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# SUBTRACT BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib wsock32.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT BASE LINK32 /profile /map
# ADD LINK32 C:\Ufis_Bin\Debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib wsock32.lib /nologo /subsystem:windows /pdb:none /debug /machine:I386

!ENDIF 

# Begin Target

# Name "RegelWerk - Win32 Release"
# Name "RegelWerk - Win32 Debug"
# Name "RegelWerk - Win32 WatsonDebug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\AboutBox.cpp
# End Source File
# Begin Source File

SOURCE=.\BarWindow.cpp
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\ChangeInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\CollectiveRulesView.cpp
# End Source File
# Begin Source File

SOURCE=.\CollectivesListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ComboBoxBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ConnectBars.cpp
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.CPP
# End Source File
# Begin Source File

SOURCE=.\DataSet.cpp
# End Source File
# Begin Source File

SOURCE=.\DDXDDV.CPP
# End Source File
# Begin Source File

SOURCE=.\DemandDetailDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Dialog_ExpressionEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\Dialog_Neuer_Name.cpp
# End Source File
# Begin Source File

SOURCE=.\Dialog_VorlagenEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\DialogGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\DutyGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\FlightIndepRulesView.cpp
# End Source File
# Begin Source File

SOURCE=.\GhsList.cpp
# End Source File
# Begin Source File

SOURCE=.\GhsListView.cpp
# End Source File
# Begin Source File

SOURCE=.\GhsListViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\GhsListViewerPropertySheet.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\IndepViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MultiEditDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RegelWerk.cpp
# End Source File
# Begin Source File

SOURCE=.\RegelWerk.rc
# End Source File
# Begin Source File

SOURCE=.\RegelWerkDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\RegelWerkView.cpp
# End Source File
# Begin Source File

SOURCE=.\RegexEdit.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RelativeTimeScale.cpp
# End Source File
# Begin Source File

SOURCE=.\RulesFormView.cpp
# End Source File
# Begin Source File

SOURCE=.\RulesGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\RulesGanttViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\RulesList.cpp
# End Source File
# Begin Source File

SOURCE=.\ScrollToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\StartDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TABLE.CPP
# End Source File
# Begin Source File

SOURCE=.\TableWithGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\ValidityDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WinNT_Enumerator.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\AboutBox.h
# End Source File
# Begin Source File

SOURCE="..\..\Programme\Microsoft Visual Studio\VC98\MFC\Include\AFX.INL"
# End Source File
# Begin Source File

SOURCE=.\BarWindow.h
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=S:\Ufis43\Cpp\CCSClassLib\ClassLib6_1\CCSDefines.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\ChangeInfo.h
# End Source File
# Begin Source File

SOURCE=.\CollectiveRulesView.h
# End Source File
# Begin Source File

SOURCE=.\CollectivesListDlg.h
# End Source File
# Begin Source File

SOURCE=.\ComboBoxBar.h
# End Source File
# Begin Source File

SOURCE=.\ConnectBars.h
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.H
# End Source File
# Begin Source File

SOURCE=.\DataSet.h
# End Source File
# Begin Source File

SOURCE=.\DDXDDV.H
# End Source File
# Begin Source File

SOURCE=.\DemandDetailDlg.h
# End Source File
# Begin Source File

SOURCE=.\Dialog_ExpressionEditor.h
# End Source File
# Begin Source File

SOURCE=.\Dialog_Neuer_Name.h
# End Source File
# Begin Source File

SOURCE=.\Dialog_VorlagenEditor.h
# End Source File
# Begin Source File

SOURCE=.\DialogGrid.h
# End Source File
# Begin Source File

SOURCE=.\DutyGrid.h
# End Source File
# Begin Source File

SOURCE=.\FlightIndepRulesView.h
# End Source File
# Begin Source File

SOURCE=.\GhsList.h
# End Source File
# Begin Source File

SOURCE=.\GhsListView.h
# End Source File
# Begin Source File

SOURCE=.\GhsListViewer.h
# End Source File
# Begin Source File

SOURCE=.\GhsListViewerPropertySheet.h
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\IndepViewer.h
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MultiEditDlg.h
# End Source File
# Begin Source File

SOURCE=.\NewDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.h
# End Source File
# Begin Source File

SOURCE=.\psapi.h
# End Source File
# Begin Source File

SOURCE=.\RegelWerk.h
# End Source File
# Begin Source File

SOURCE=.\RegelWerkDoc.h
# End Source File
# Begin Source File

SOURCE=.\RegelWerkView.h
# End Source File
# Begin Source File

SOURCE=.\RegexEdit.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\RelativeTimeScale.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RulesFormView.h
# End Source File
# Begin Source File

SOURCE=.\RulesGantt.h
# End Source File
# Begin Source File

SOURCE=.\RulesGanttViewer.h
# End Source File
# Begin Source File

SOURCE=.\RulesList.h
# End Source File
# Begin Source File

SOURCE=.\ScrollToolBar.h
# End Source File
# Begin Source File

SOURCE=.\StartDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\StringConst.h
# End Source File
# Begin Source File

SOURCE=.\TABLE.H
# End Source File
# Begin Source File

SOURCE=.\TableWithGrid.h
# End Source File
# Begin Source File

SOURCE=.\ToolBar_Combo.h
# End Source File
# Begin Source File

SOURCE=.\ValidityDlg.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# Begin Source File

SOURCE=.\WinNT_Enumerator.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00016.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00024.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00026.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00027.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00028.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor2.cur
# End Source File
# Begin Source File

SOURCE=.\res\hl2r_D.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hl2r_F.bmp
# End Source File
# Begin Source File

SOURCE=.\res\HL2R_small.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hl2r_U.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hl2r_X.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hr2l_F.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hr2l_small.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hr2l_U.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hr2l_X.bmp
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\login.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\part_out.bmp
# End Source File
# Begin Source File

SOURCE=.\res\part_turn.bmp
# End Source File
# Begin Source File

SOURCE=.\res\partial_.bmp
# End Source File
# Begin Source File

SOURCE=.\res\RegelWerk.ico
# End Source File
# Begin Source File

SOURCE=.\res\RegelWerk.rc2
# End Source File
# Begin Source File

SOURCE=.\res\RegelWerkDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\right_ba.bmp
# End Source File
# Begin Source File

SOURCE=.\sdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=".\res\tpst=0.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\tpst=1.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\ufislogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ul2lr_vert.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vl2r_btn.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VL2R_D.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vl2r_F.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VL2R_small.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VL2R_U.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VL2R_X.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vr2l_btn.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VR2L_D.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VR2L_F.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vr2l_small.bmp
# End Source File
# Begin Source File

SOURCE=.\res\VR2L_U.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vr2l_X.bmp
# End Source File
# End Group
# End Target
# End Project
