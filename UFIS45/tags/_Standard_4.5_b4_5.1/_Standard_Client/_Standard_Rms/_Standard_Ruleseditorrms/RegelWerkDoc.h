	// RegelWerkDoc.h : interface of the CRegelWerkDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_REGELWERKDOC_H__5C27D14A_B08A_11D2_AAE6_00001C018CF3__INCLUDED_)
#define AFX_REGELWERKDOC_H__5C27D14A_B08A_11D2_AAE6_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CRegelWerkDoc : public CDocument
{
protected: // create from serialization only
	CRegelWerkDoc();
	DECLARE_DYNCREATE(CRegelWerkDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegelWerkDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRegelWerkDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRegelWerkDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGELWERKDOC_H__5C27D14A_B08A_11D2_AAE6_00001C018CF3__INCLUDED_)
