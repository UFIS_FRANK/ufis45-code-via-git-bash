// MultiEditDlg.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <Dialog_VorlagenEditor.h>
#include <mainfrm.h>
#include <validitydlg.h>

#include <MultiEditDlg.h>
#include "BasicData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define COL_RUE_RUSN	2
#define COL_RUE_NAME	3
#define COL_RUE_URNO	4
#define COL_RUE_PRIO	5
#define COL_RUE_EXCL	6
#define COL_RUE_EVTY	7
#define COL_RUE_TYPE	1

#define COL_SER_USED	1
#define COL_SER_CODE	2
#define COL_SER_NAME	3
#define COL_SER_URNO	4
#define COL_SER_SEER	5
/*
void MyTrace ( CString opTable )
{
	CedaObject *prlObject;
	CString olFieldList;

	if ( ogBCD.omObjectMap.Lookup((LPCSTR)opTable, (void *&)prlObject) )
		olFieldList = prlObject->GetFieldList();
	TRACE ( "\nMyTrace: Table <%s> Fields <%s>", opTable, olFieldList );
	ogBCD.TraceData(opTable);
}*/

/////////////////////////////////////////////////////////////////////////////
// CMultiEditDlg dialog


CMultiEditDlg::CMultiEditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMultiEditDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMultiEditDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomRulesGrid = new CGridFenster(this);
	pomServiceGrid = new CGridFenster(this);
	pomRulesGrid->SetSortingEnabled(false);   // sorting of grid is done inside
	pomServiceGrid->SetSortingEnabled(false); // sorting of grid is done inside

	imSortRueColum = imSortSerColum = -1;
	bmSortRueAscend = bmSortSerAscend = true;
	bmTimesChanged = bmDutiesChanged = false;
}

CMultiEditDlg::~CMultiEditDlg()
{
	delete pomRulesGrid;
	delete pomServiceGrid;
	pomRulesGrid = pomServiceGrid = 0;

}


void CMultiEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMultiEditDlg)
	DDX_Control(pDX, IDC_COMBO_BOX, omComboBox);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMultiEditDlg, CDialog)
	//{{AFX_MSG_MAP(CMultiEditDlg)
	ON_BN_CLICKED(ID_ADD, OnAdd)
	ON_BN_CLICKED(ID_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_FDUT, OnFdut)
	ON_BN_CLICKED(IDC_FSDT, OnFsdt)
	ON_BN_CLICKED(IDC_FSUT, OnFsut)
	ON_BN_CLICKED(IDC_FWTF, OnFwtf)
	ON_BN_CLICKED(IDC_FWTT, OnFwtt)
	ON_CBN_SELCHANGE(IDC_COMBO_BOX, OnSelchangeComboBox)
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK, OnGridClick)
	ON_MESSAGE(GRID_MESSAGE_ENDSELECTION, OnGridSelEnd)
	ON_BN_CLICKED(ID_SET, OnSet)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMultiEditDlg message handlers

BOOL CMultiEditDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	//WORD wlSelectMode = GX_SELROW | GX_SELSHIFT | GX_SELKEYBOARD ;
	WORD wlSelectMode = GX_SELFULL & ~GX_SELCELL &~GX_SELCOL;
	// TODO: Add extra initialization here

	omBigFont.CreateStockObject ( SYSTEM_FONT );
	omRulesListTxt.SubclassDlgItem( IDC_RULES_LIST_TXT, this );
	omRulesListTxt.SetEditFont(&omBigFont);
	omRulesListTxt.SetReadOnly();

	omServiceListTxt.SubclassDlgItem( IDC_SERVICE_LIST_TXT, this );
	omServiceListTxt.SetEditFont(&omBigFont);
	omServiceListTxt.SetReadOnly();

	omRulesTxt.SubclassDlgItem( IDC_RULES_TXT, this );
	omRulesTxt.SetEditFont(&omBigFont);
	omRulesTxt.SetReadOnly();

	omServiceTxt.SubclassDlgItem( IDC_SERVICE_TXT, this );
	omServiceTxt.SetEditFont(&omBigFont);
	omServiceTxt.SetReadOnly();

	for  (int i=0; i<5; i++ )
	{
		omServiceTimes[i].SubclassDlgItem(IDC_SDUT+i, this );
		omServiceTimes[i].SetReadOnly();
	}

	//--- Initialize Rules Grid
	BOOL blErr = pomRulesGrid->SubclassDlgItem(IDC_RULES_LIST, this);
	pomRulesGrid->Initialize();

	pomRulesGrid->SetColCount(7);
	pomRulesGrid->SetRowCount(0);

	pomRulesGrid->SetColWidth(0, 0, 20);		// row header
	pomRulesGrid->SetColWidth(COL_RUE_RUSN, COL_RUE_RUSN, 80);	// rule short name
	pomRulesGrid->SetColWidth(COL_RUE_NAME, COL_RUE_NAME, 200);	// rule name
	pomRulesGrid->SetColWidth(COL_RUE_URNO, COL_RUE_URNO, 70);	// rule urno
	pomRulesGrid->SetColWidth(COL_RUE_PRIO, COL_RUE_PRIO, 100);	// prio
	pomRulesGrid->SetColWidth(COL_RUE_EXCL, COL_RUE_EXCL, 15);	// exclude 
	pomRulesGrid->SetColWidth(COL_RUE_EVTY, COL_RUE_EVTY, 15);	// event type (0 = turnaround, 1 = inbound, 2 = outbound)
	pomRulesGrid->SetColWidth(COL_RUE_TYPE, COL_RUE_TYPE, 80);	// type as text

	pomRulesGrid->SetValueRange(CGXRange(0,COL_RUE_RUSN), GetString(KURZNAME)); // short name
	pomRulesGrid->SetValueRange(CGXRange(0,COL_RUE_NAME), GetString(217)); // name
	pomRulesGrid->SetValueRange(CGXRange(0,COL_RUE_URNO), "URNO "); // rule urno
	pomRulesGrid->SetValueRange(CGXRange(0,COL_RUE_PRIO), "PRIO");	 
	pomRulesGrid->SetValueRange(CGXRange(0,COL_RUE_EXCL), "EXCL"); 
	pomRulesGrid->SetValueRange(CGXRange(0,COL_RUE_EVTY), "EVTY"); 
	pomRulesGrid->SetValueRange(CGXRange(0,COL_RUE_TYPE), GetString(IDS_TYPE)); 
	pomRulesGrid->HideCols(COL_RUE_URNO, COL_RUE_EVTY);

	// disable numbering and tracking for rows
	pomRulesGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomRulesGrid->GetParam()->EnableTrackRowHeight(0); 
	pomRulesGrid->GetParam()->EnableSelection ( wlSelectMode );
			
	//--- Initialize Service Grid
	blErr &= pomServiceGrid->SubclassDlgItem(IDC_SERVICE_LIST, this);
	pomServiceGrid->Initialize();
	pomServiceGrid->SetRowCount(0);
	pomServiceGrid->SetColCount(5);

	pomServiceGrid->SetColWidth(0, 0, 20);	// row header
	pomServiceGrid->SetColWidth(COL_SER_USED, COL_SER_USED, 30);	// used checkbox
	pomServiceGrid->SetColWidth(COL_SER_CODE, COL_SER_CODE, 50);	// Code
	pomServiceGrid->SetColWidth(COL_SER_NAME, COL_SER_NAME, 250);	// service name
	pomServiceGrid->SetColWidth(COL_SER_URNO, COL_SER_URNO, 70);	// service urno
	pomServiceGrid->SetColWidth(COL_SER_SEER, COL_SER_SEER, 20);	// service relation
	pomServiceGrid->SetValueRange(CGXRange(0,COL_SER_USED), GetString(IDS_USED)); 
	pomServiceGrid->SetValueRange(CGXRange(0,COL_SER_CODE), GetString(CODE)); 
	pomServiceGrid->SetValueRange(CGXRange(0,COL_SER_NAME), GetString(NAME)); 
	pomServiceGrid->SetValueRange(CGXRange(0,COL_SER_URNO), "URNO");	 
	pomServiceGrid->SetValueRange(CGXRange(0,COL_SER_SEER), "SEER"); 
	pomServiceGrid->HideCols(COL_SER_URNO, COL_SER_SEER);

	// disable numbering and tracking for rows
	pomServiceGrid->GetParam()->SetNumberedRowHeaders(FALSE); 
	pomServiceGrid->GetParam()->EnableTrackRowHeight(0); 
	pomServiceGrid->GetParam()->EnableSelection ( wlSelectMode );

	SetStaticTexts();
	IniComboBox ();
	FillServiceGrid();

	/*EnableDlgItem ( this, IDC_SDUT, FALSE, TRUE );
	EnableDlgItem ( this, IDC_SSDT, FALSE, TRUE );
	EnableDlgItem ( this, IDC_SSUT, FALSE, TRUE );
	EnableDlgItem ( this, IDC_SWTT, FALSE, TRUE );
	EnableDlgItem ( this, IDC_SWTF, FALSE, TRUE );*/
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMultiEditDlg::OnAdd() 
{
	// TODO: Add your control notification handler code here
	CStringArray olSerUrnoArray;
	POSITION	 pos;
	CString		 olSerUrno, olRueUrno, olRuty, olEvty, olTplUrno;
	CObject		 *polObject;
	bool		 blIsSCRule;

	//  Fill Array of selected services URNOS
	pos = omSelSerUrnos.GetStartPosition();
	while ( pos )
	{
		omSelSerUrnos.GetNextAssoc( pos, olSerUrno, polObject );
		olSerUrnoArray.Add ( olSerUrno );
	}
	pos = omSelRueUrnos.GetStartPosition();
	while ( pos )
	{
		omSelRueUrnos.GetNextAssoc( pos, olRueUrno, polObject );
		if ( ogBCD.GetFields("RUE", "URNO", olRueUrno, "RUTY", "EVTY", olRuty, olEvty) )
		{
			olTplUrno = ogBCD.GetField("RUE", "URNO", olRueUrno, "UTPL");
			blIsSCRule = ( (olRuty=="0") || (olRuty=="1") );
			ogDataSet.CreateRudFromSerList (blIsSCRule, olSerUrnoArray, olRueUrno, olTplUrno,
											olEvty, &omDefaultAloc, false, false );
			UpdateRudDispValues ( olRueUrno, "RUD_TMP" );
			bmDutiesChanged = true;
			if ( !omRueDutyChanges.Find ( olRueUrno ) )
				omRueDutyChanges.AddTail ( olRueUrno ) ;
		}	
	}

	UpdateUsedFlags();
	EnableButtons();	
	/*
	MyTrace("RUD_TMP");
	MyTrace("RPF_TMP");
	MyTrace("RPQ_TMP");
	*/
}

void CMultiEditDlg::OnDelete() 
{
	// TODO: Add your control notification handler code here
	POSITION	pos;
	CString		olUrue, olUghs, olUrud, olMsg;
	CCSPtrArray <RecordSet>	olRecords;
	CObject		 *polObject;
	int			i;
	bool		blOk;

	pos = omSelRueUrnos.GetStartPosition();
	while ( pos )
	{
		omSelRueUrnos.GetNextAssoc( pos, olUrue, polObject );
		//  Select all RUD's for this selected rule
		ogBCD.GetRecords("RUD_TMP", "URUE", olUrue, &olRecords );
		for ( i=olRecords.GetSize()-1; i>=0; i-- )
		{
			olUghs = olRecords[i].Values[igRudUghsIdx];
			olUrud = olRecords[i].Values[igRudUrnoIdx];
			if ( omSelSerUrnos.Lookup ( olUghs, polObject ) )
			{	//  RUD comes from a selected service
				blOk = ogBCD.DeleteRecord("RUD_TMP", "URNO", olUrud);
				if ( blOk )
					blOk = ogDataSet.DeleteRessources(olRecords[i]);
				if ( !blOk )
				{
					olMsg.Format ( "Error deleting Service <%s> from rule <%s>", 
								   olUghs, olUrue );
					MessageBox ( olMsg );
				}
				else
					if ( !omRueDutyChanges.Find ( olUrue ) )
						omRueDutyChanges.AddTail ( olUrue ) ;
			}
		}
		olRecords.DeleteAll();
	}
	bmDutiesChanged = true;
	UpdateUsedFlags();
	EnableButtons();
}

void CMultiEditDlg::OnFdut() 
{
	// TODO: Add your control notification handler code here
	BOOL blEndable = IsDlgButtonChecked ( IDC_FDUT );
	/*EnableDlgItem ( this, IDC_SDUT, blEndable, TRUE );*/
	omServiceTimes[0].SetReadOnly(!blEndable);
	omServiceTimes[0].RedrawWindow();
	EnableButtons();
}

void CMultiEditDlg::OnFsdt() 
{
	// TODO: Add your control notification handler code here
	BOOL blEndable = IsDlgButtonChecked ( IDC_FSDT );
	/*EnableDlgItem ( this, IDC_SSDT, blEndable, TRUE );*/
	omServiceTimes[2].SetReadOnly(!blEndable);
	omServiceTimes[2].RedrawWindow();
	EnableButtons();
}

void CMultiEditDlg::OnFsut() 
{
	// TODO: Add your control notification handler code here
	BOOL blEndable = IsDlgButtonChecked ( IDC_FSUT );
	/*EnableDlgItem ( this, IDC_SSUT, blEndable, TRUE );*/
	omServiceTimes[1].SetReadOnly(!blEndable);
	omServiceTimes[1].RedrawWindow();
	EnableButtons();
}

void CMultiEditDlg::OnFwtf() 
{
	// TODO: Add your control notification handler code here
	BOOL blEndable = IsDlgButtonChecked ( IDC_FWTF );
	/*EnableDlgItem ( this, IDC_SWTF, blEndable, TRUE );*/
	omServiceTimes[4].SetReadOnly(!blEndable);
	omServiceTimes[4].RedrawWindow();
	EnableButtons();
}

void CMultiEditDlg::OnFwtt() 
{
	// TODO: Add your control notification handler code here
	BOOL blEndable = IsDlgButtonChecked ( IDC_FWTT );
	/*EnableDlgItem ( this, IDC_SWTT, blEndable, TRUE );*/
	omServiceTimes[3].SetReadOnly(!blEndable);
	omServiceTimes[3].RedrawWindow();
	EnableButtons();
}

void CMultiEditDlg::OnSelchangeComboBox() 
{
	// TODO: Add your control notification handler code here
//----- get template urno
	int ilSel;
	CString olSelection = "*.*";

	if ( SaveChanges() )
	{
		ilSel = omComboBox.GetCurSel();
		if (ilSel >= 0)
		{
			omComboBox.GetLBText( ilSel, olSelection );
			omTplUrno = ogBCD.GetField ( "TPL", "TNAM", olSelection, "URNO" );
			omDefaultAloc = ogBCD.GetField("TPL", "URNO", omTplUrno, "DALO");
		}
		pomRulesGrid->SetTopRow ( 1, GX_SMART, FALSE  );
		FillLogicTables ();
		FillRulesGrid();
		UpdateRulesValues();
		UpdateServiceValues();
		UpdateUsedFlags();
		EnableButtons();
	}
}


void CMultiEditDlg::FillRulesGrid()
{
	CCS_TRY
	int			i;
	RecordSet	olRecord( ogBCD.GetFieldCount("RUE_TMP") );
		
	pomRulesGrid->SetReadOnly(FALSE);
	pomRulesGrid->LockUpdate(TRUE);
	
	// empty grid	
	int ilRowCount = pomRulesGrid->GetRowCount();
	if (ilRowCount > 0)
	{
		pomRulesGrid->RemoveRows(1, ilRowCount);
	}

	for ( i = 0; i < ogBCD.GetDataCount("RUE_TMP"); i++)
	{
		if ( ogBCD.GetRecord("RUE_TMP", i, olRecord ) )
			DisplayOneRule( &olRecord );
	}
	
	// sort grid rows, descending  by priority
	if (pomRulesGrid->GetRowCount() > 0)
	{
		SortRules();	
	}

	pomRulesGrid->LockUpdate(FALSE);
	pomRulesGrid->Redraw();
	pomRulesGrid->SetReadOnly(TRUE);

	CCS_CATCH_ALL
}

void CMultiEditDlg::IniComboBox()
{
	CString olName, olTpst;

	int  i, ilRecordCount = ogBCD.GetDataCount("TPL");
	for( i = 0; i < ilRecordCount; i++)
	{
 		olName = ogBCD.GetField("TPL", i, "TNAM");
		olTpst =  ogBCD.GetField("TPL", i,"TPST");
		if ((olTpst!='2') && CanModifyTemplate ( olName ) &&
			(omComboBox.FindStringExact(-1, olName) == CB_ERR))
		{
			omComboBox.InsertString(0, olName);
		}
	}
	if ( ( omComboBox.GetCount() > 0 ) && !omTplUrno.IsEmpty() )
	{
		olName = ogBCD.GetField("TPL", "URNO", omTplUrno, "TNAM" );
		i = omComboBox.FindStringExact(-1, olName);
		i = max ( i, 0 );
		omComboBox.SetCurSel ( i );
		OnSelchangeComboBox ();
	}
}

int CMultiEditDlg::DoModal(CString &ropTplUrno)
{
	omTplUrno = ropTplUrno;
	return CDialog::DoModal();
}

void CMultiEditDlg::FillServiceGrid()
{
	int			i, ilRecCount;	
	COLORREF	ilBkColor ;
	CString		olSeer;
	CGXStyle    olStyle;
	RecordSet	olRecord(ogBCD.GetFieldCount("SER") );
	
	ilRecCount = ogBCD.GetDataCount ( "SER" );
	for ( i=0; i<ilRecCount; i++ )
	{
		if (ogBCD.GetRecord("SER", i, olRecord ) )
		{
			pomServiceGrid->InsertRows(1, 1);
			pomServiceGrid->SetValueRange( CGXRange(1,COL_SER_CODE),
										   olRecord.Values[igSerSecoIdx] );
			pomServiceGrid->SetValueRange( CGXRange(1,COL_SER_NAME),
										   olRecord.Values[igSerSnamIdx] );
			pomServiceGrid->SetValueRange( CGXRange(1,COL_SER_URNO),
										   olRecord.Values[igSerUrnoIdx] );
			olSeer = olRecord.Values[igSerSeerIdx];
			pomServiceGrid->SetValueRange( CGXRange(1,COL_SER_SEER), olSeer );
			ilBkColor = ogColors[WHITE_IDX];  // default;
			if ( olSeer == "0" )
				ilBkColor = ogColors[PYELLOW_IDX];
			else if(olSeer == "1")	
					ilBkColor = ogColors[PGREEN_IDX];
			else if(olSeer == "2")	
					ilBkColor = ogColors[PBLUE_IDX];

			olStyle.SetInterior(ilBkColor).SetEnabled(FALSE);
			pomServiceGrid->SetStyleRange(CGXRange().SetRows(1), olStyle); 
		}
	}
	ilRecCount = pomServiceGrid->GetRowCount();
	if ( ilRecCount > 0 )
		pomServiceGrid->SetStyleRange ( CGXRange(1,COL_SER_USED,ilRecCount,COL_SER_USED),	
										CGXStyle().SetControl(GX_IDS_CTRL_CHECKBOX3D) );
	// sort grid rows, descending  by servicetype
	if (pomServiceGrid->GetRowCount() > 0)
	{
		SortServices();	
	}

}

void CMultiEditDlg::DisplayOneRule (RecordSet *popRule)
{
	CGXStyle olStyle;
	CString olRueShortName;
	CString olRueName;
	CString olRuePrio;
	CString olRueUrno;
	CString olExcl;
	CString olRust;
	CString olEvty;
	COLORREF  ilBkColor;
	CString olTypeText;
		
	if (!popRule)
		return;

	// rule status
	int ilRust = atoi(popRule->Values[igRueRustIdx]);
	if ( ilRust > 1 )
		return;

	// rule type
	int ilRuty = atoi(popRule->Values[igRueRutyIdx]);
	
	pomRulesGrid->InsertRows(1, 1);
	
	olExcl = (popRule->Values[igRueExclIdx] == "1") ? "X" : " ";
	olRueShortName = popRule->Values[igRueRusnIdx];
	olRueName = popRule->Values[igRueRunaIdx];
	olRuePrio =	popRule->Values[igRuePrioIdx];
	olRueUrno = popRule->Values[igRueUrnoIdx];
	olEvty = popRule->Values[igRueEvtyIdx];
	olRust = popRule->Values[igRueRustIdx];
	
	if ( olExcl == "X" )
		olTypeText = GetString ( IDS_EXCLUDE );
	else 
		switch ( ilRuty )
		{
			case 1: 
				olTypeText = GetString ( IDS_COLLECTIVE );
				break;
			case 0:
				if (olEvty == CString("0"))	// turnaround: blue
				{
					olTypeText = GetString ( IDS_TURNAROUND );
				}
				else if(olEvty == CString("1"))	// inbound: green
				{
					olTypeText = GetString ( IDS_INBOUND );
				}
				else if(olEvty == CString("2"))	// outbound: yellow
				{
					olTypeText = GetString ( IDS_OUTBOUND );
				}
				break;
			case 2:
			case 3:
				olTypeText = GetString ( IDS_FLIGHTINDIPENDENT );
				break;
		}

	pomRulesGrid->SetValueRange(CGXRange(1, COL_RUE_RUSN), olRueShortName);
	pomRulesGrid->SetValueRange(CGXRange(1, COL_RUE_NAME), olRueName);
	pomRulesGrid->SetValueRange(CGXRange(1, COL_RUE_URNO), olRueUrno);
	pomRulesGrid->SetValueRange(CGXRange(1, COL_RUE_PRIO), olRuePrio);
	pomRulesGrid->SetValueRange(CGXRange(1, COL_RUE_EXCL), olExcl);
	pomRulesGrid->SetValueRange(CGXRange(1, COL_RUE_EVTY), olEvty);
	pomRulesGrid->SetValueRange(CGXRange(1, COL_RUE_TYPE), olTypeText);

	ilBkColor = ogColors[WHITE_IDX];  // default;
	// set row colors
	if (ilRust == 0)	// deactivated: silver
	{
		ilBkColor = ogColors[SILVER_IDX];
	}
	else
	{
		switch ( ilRuty )
		{
			case 0:		// single rule
				if (olEvty == CString("0"))	// turnaround: blue
				{
					ilBkColor = ogColors[PYELLOW_IDX];
				}
				else if(olEvty == CString("1"))	// inbound: green
				{
					ilBkColor = ogColors[PGREEN_IDX];
				}
				else if(olEvty == CString("2"))	// outbound: yellow
				{
					ilBkColor = ogColors[PBLUE_IDX];
				}
				break;
			
			case 1:		// collective rule
				ilBkColor = LROSE;
				break;
		}
	}

	olStyle.SetInterior(ilBkColor).SetEnabled(FALSE);
	pomRulesGrid->SetStyleRange(CGXRange().SetRows(1), olStyle); 
}

void CMultiEditDlg::SortRules()
{
	CGXSortInfoArray rlSortInfo;
	int ilStart=0;

	if ( imSortRueColum > -1 )
	{
		rlSortInfo.SetSize(5);       // 1 additional key
		rlSortInfo[0].nRC = imSortRueColum;		 // first column to sort is the criterium last used 
		rlSortInfo[0].bCase = FALSE; // not case sensitive
		rlSortInfo[0].sortType = CGXSortInfo::autodetect;   // the grid will determine if the key is a date, numeric or other value 
		if ( bmSortRueAscend )
			rlSortInfo[0].sortOrder = CGXSortInfo::ascending;
		else
			rlSortInfo[0].sortOrder = CGXSortInfo::descending;
		ilStart = 1;
	}
	else
		rlSortInfo.SetSize(4);       //  if no user sorting active
			
	rlSortInfo[ilStart+0].nRC = COL_RUE_EXCL;	// column EXCL is the first key
	rlSortInfo[ilStart+0].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+0].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+0].sortOrder = CGXSortInfo::descending;
			
	rlSortInfo[ilStart+1].nRC = COL_RUE_EVTY;	// column EVTY is the second key
	rlSortInfo[ilStart+1].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+1].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+1].sortOrder = CGXSortInfo::ascending;
	
	rlSortInfo[ilStart+2].nRC = COL_RUE_PRIO;	// column Prio is the third key
	rlSortInfo[ilStart+2].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+2].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+2].sortOrder = CGXSortInfo::descending;    

	rlSortInfo[ilStart+3].nRC = COL_RUE_RUSN;	// column Shoertname is the last key
	rlSortInfo[ilStart+3].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+3].sortType = CGXSortInfo::alphanumeric;   
	rlSortInfo[ilStart+3].sortOrder = CGXSortInfo::ascending;    

	pomRulesGrid->SortRows(CGXRange().SetTable(), rlSortInfo);
}


void CMultiEditDlg::SortServices()
{
	CGXSortInfoArray rlSortInfo;
	int ilStart=0;

	if ( imSortSerColum > -1 )
	{
		rlSortInfo.SetSize(3);       // 1 additional key
		rlSortInfo[0].nRC = imSortSerColum;		 // first column to sort is the criterium last used 
		rlSortInfo[0].bCase = FALSE; // not case sensitive
		rlSortInfo[0].sortType = CGXSortInfo::autodetect;   // the grid will determine if the key is a date, numeric or other value 
		if ( bmSortSerAscend )
			rlSortInfo[0].sortOrder = CGXSortInfo::ascending;
		else
			rlSortInfo[0].sortOrder = CGXSortInfo::descending;
		ilStart = 1;
	}
	else
		rlSortInfo.SetSize(2);       //  if no user sorting active
			
	rlSortInfo[ilStart+0].nRC = COL_SER_SEER;	// column SEER is the first key
	rlSortInfo[ilStart+0].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+0].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+0].sortOrder = CGXSortInfo::ascending;
			
	rlSortInfo[ilStart+1].nRC = COL_SER_CODE;	// column CODE is the second key
	rlSortInfo[ilStart+1].bCase = TRUE; // not case sensitive
	rlSortInfo[ilStart+1].sortType = CGXSortInfo::alphanumeric;   // the grid will determine if the key is a date, numeric or other value 
	rlSortInfo[ilStart+1].sortOrder = CGXSortInfo::ascending;
	
	pomServiceGrid->SortRows(CGXRange().SetTable(), rlSortInfo);
}

void CMultiEditDlg::SetStaticTexts()
{
	SetWindowLangText(this, IDS_MULTI_EDIT_TIT );
	SetDlgItemLangText(this, IDC_TPL_TXT, IDS_TPL_TXT );
	SetDlgItemLangText(this, IDC_RULES_LIST_TXT, IDS_RULES_TXT );
	SetDlgItemLangText(this, IDC_SERVICE_LIST_TXT, SERVICES );
	SetDlgItemLangText(this, ID_DELETE, IDS_DELFROMRULES_TXT );
	SetDlgItemLangText(this, ID_ADD, IDS_ADDTORULES_TXT );
	SetDlgItemLangText(this, IDCANCEL, CANCEL );
	SetDlgItemLangText(this, IDC_FWTT, IDS_WAYTO_TXT );
	SetDlgItemLangText(this, IDC_FWTF, IDS_WAYBACK_TXT );
	SetDlgItemLangText(this, IDC_FDUT, IDS_DURATION_TXT );
	SetDlgItemLangText(this, IDC_FSUT, IDS_SETUPTIME_TXT );
	SetDlgItemLangText(this, IDC_FSDT, IDS_SETDOWNTIME_TXT );
	SetDlgItemLangText(this, IDC_CHANGE_TXT, IDS_CHANGE_TXT );
	SetDlgItemLangText(this, IDC_DAUER_TXT, IDS_DURATION_COL );
	SetDlgItemLangText(this, IDC_RULES_TXT, IDS_RULES_TXT );
	SetDlgItemLangText(this, IDC_SERVICE_TXT, SERVICES );
	SetDlgItemLangText(this, ID_SET, IDS_SET_VALUES);
	SetDlgItemLangText(this, IDOK, OK );
}

void CMultiEditDlg::UpdateUsedFlags()
{
	CString			olSerUrno, olRueUrno;
	unsigned short	ilValue;
	CStringList		olUghsValues;
	CObject			*polObject;
	int				i;

	pomServiceGrid->SetStyleRange ( CGXRange().SetCols(COL_SER_USED), 
									CGXStyle().SetEnabled(TRUE) ); 
	//  collect all UGHS of selected rules rule demands
	for ( i=0; i<=ogBCD.GetDataCount("RUD_TMP"); i++ )
	{
		olRueUrno = ogBCD.GetField ( "RUD_TMP", i, "URUE" );
		if ( omSelRueUrnos.Lookup ( olRueUrno, polObject ) )
		{
			olSerUrno = ogBCD.GetField ( "RUD_TMP", i, "UGHS" );
			if ( !olUghsValues.Find ( olSerUrno ) )
				olUghsValues.AddTail ( olSerUrno );
		}
	}
	for ( i=1; i<=pomServiceGrid->GetRowCount(); i++ )
	{
		olSerUrno = pomServiceGrid->GetValueRowCol ( i, COL_SER_URNO) ;
		ilValue = (olUghsValues.Find ( olSerUrno)) ? 1 : 0;
		pomServiceGrid->SetValueRange ( CGXRange(i,COL_SER_USED), ilValue );
	}

	pomServiceGrid->SetStyleRange ( CGXRange().SetCols(COL_SER_USED), 
									CGXStyle().SetEnabled(FALSE) ); 

}

void CMultiEditDlg::UpdateRulesValues()
{
	CObject		*polObject;
	int			ilValues[5], ilTmp, i, j;
	bool		blSameValue[5];
	char		clFields[][5] = { "DEDU", "SUTI", "SDTI", "TTGT", "TTGF" };
	int			ilIdx[5];
	CString		olRueUrno, olValue;
	RecordSet	olRecord;
	
	//  initialize arrays
	for ( j=0; j<5; j++ )
	{
		ilIdx[j] = ogBCD.GetFieldIndex ( "RUD", clFields[j] );
		if ( ilIdx[j] < 0 )
		{
			olValue.Format ( "Field <%s> not known in RUDTAB", clFields[j] );
			MessageBox ( olValue );
			return;
		}
		ilValues[j] = -1;
		blSameValue[j] = true;
	}

	for ( i=0; i<ogBCD.GetDataCount("RUD_TMP"); i++ )
	{
		if ( ogBCD.GetRecord("RUD_TMP", i, olRecord ) )
		{
			olRueUrno = olRecord.Values[igRudUrueIdx];
			if ( omSelRueUrnos.Lookup ( olRueUrno, polObject ) )
			{	//  This is a RUD-record of a marked rule
				for ( j=0; j<5; j ++ )
				{
					ilTmp = atoi( olRecord.Values[ilIdx[j]] );
					if ( ilValues[j] < 0 )
						ilValues[j] = atoi( olRecord.Values[ilIdx[j]] );
					else
					{
						if ( blSameValue[j] && (ilTmp >=0) &&
							 (ilTmp!=ilValues[j]) )
							blSameValue[j] = false;
					}
				}
			}
		}
	}
	for ( j=0; j<5; j ++ )
	{
		if ( blSameValue[j] && (ilValues[j]>=0) )
			olValue.Format ( "%d", ilValues[j]/60 );
		else
			olValue.Empty();
		SetDlgItemText ( IDC_SDUT+j, olValue );
	}
}


void CMultiEditDlg::UpdateServiceValues()
{
	CObject		*polObject;
	int			ilValues[5], ilTmp, j;
	bool		blSameValue[5];
	char		clFields[][5] = { "SDUT", "SSUT", "SSDT", "SWTT", "SWTF" };
	int			ilIdx[5];
	CString		olSerUrno, olValue;
	RecordSet	olRecord;
	
	//  initialize arrays
	for ( j=0; j<5; j++ )
	{
		ilIdx[j] = ogBCD.GetFieldIndex ( "SER", clFields[j] );
		if ( ilIdx[j] < 0 )
		{
			olValue.Format ( "Field <%s> not known in SERTAB", clFields[j] );
			MessageBox ( olValue );
			return;
		}
		ilValues[j] = -1;
		blSameValue[j] = true;
	}

	POSITION pos = omSelSerUrnos.GetStartPosition() ;
	while ( pos )
	{
		omSelSerUrnos.GetNextAssoc( pos, olSerUrno, polObject );
		if ( ogBCD.GetRecord("SER", "URNO", olSerUrno, olRecord ) )
		{
			for ( j=0; j<5; j ++ )
			{
				ilTmp = atoi( olRecord.Values[ilIdx[j]] );
				if ( ilValues[j] < 0 )
					ilValues[j] = atoi( olRecord.Values[ilIdx[j]] );
				else
				{
					if ( blSameValue[j] && (ilTmp >=0) &&
						 (ilTmp!=ilValues[j]) )
						blSameValue[j] = false;
				}
			}
		}
	}
	for ( j=0; j<5; j ++ )
	{
		if ( blSameValue[j] && (ilValues[j]>=0) )
			olValue.Format ( "%d", ilValues[j] );
		else
			olValue.Empty();
		SetDlgItemText ( IDC_SDUT2+j, olValue );
	}
}

void CMultiEditDlg::EnableButtons()
{
	BOOL blEnable;
	int  j, ilUsed;
	CString olValue;
	
	//  Button "Add Service" will be active, if services and rules are selected
	if ( (omSelSerLines.GetSize()>0 ) && (omSelRueLines.GetSize()>0) )
		blEnable = TRUE;
	else
		blEnable = FALSE;
	EnableDlgItem ( this, ID_ADD, blEnable, TRUE );

	//  Button "Set Values" will be active, if rules are selected
	//	and any checkbox is checked
	if ( omSelRueLines.GetSize() > 0 )
	{
		for (j=0; !blEnable && (j<5); j ++ )
			if ( IsDlgButtonChecked ( IDC_FDUT+j ) ) 
				blEnable = TRUE;
	}
	EnableDlgItem ( this, ID_SET, blEnable, TRUE );

	//  Button "Save" will be active, if anything is changed
	blEnable = ( bmTimesChanged || bmDutiesChanged );
	EnableDlgItem ( this, ID_SAVE, blEnable, TRUE );

	//  Button "Delete from Rules" will be active, if all marked services are
	//	included in all marked rules
	blEnable = (omSelSerLines.GetSize()>0);
	for ( j=0; blEnable && (j<omSelSerLines.GetSize()); j++ )
	{
		if ( (omSelSerLines[j] > 0) && 
			 (omSelSerLines[j] <= pomServiceGrid->GetRowCount() ) )
		{
			olValue = pomServiceGrid->GetValueRowCol( omSelSerLines[j], COL_SER_USED );
			ilUsed = atoi ( olValue );
			if ( !ilUsed )
				blEnable = FALSE;
		}
	}
	EnableDlgItem ( this, ID_DELETE, blEnable, TRUE );
}

void CMultiEditDlg::OnGridClick(WPARAM wParam, LPARAM lParam) 
{

	ROWCOL			ilRow = ((CELLPOS*)lParam)->Row;
	CGridFenster	*polGrid = (CGridFenster*)wParam;
	
	if( polGrid )
	{
		if (ilRow>0) 
		{
			//polGrid->SelectRange(CGXRange(ilRow, 0, ilRow, polGrid->GetColCount()), TRUE);
		}
		else
		{	
			if ( polGrid == pomRulesGrid )
			{
				if ( imSortRueColum != ((CELLPOS*)lParam)->Col )
				{
					bmSortRueAscend = true;
					imSortRueColum = ((CELLPOS*)lParam)->Col;
				}
				else
					bmSortRueAscend = !bmSortRueAscend ;
				SortRules();
			}
			if ( polGrid == pomServiceGrid )
			{
				if ( imSortSerColum != ((CELLPOS*)lParam)->Col )
				{
					bmSortSerAscend = true;
					imSortSerColum = ((CELLPOS*)lParam)->Col;
				}
				else
					bmSortSerAscend = !bmSortSerAscend ;
				SortServices();
			}
			ROWCOL ilRows = polGrid->GetRowCount(); 
			polGrid->SelectRange(CGXRange().SetRows(0), FALSE );
		}
	}
		
}

void CMultiEditDlg::OnGridSelEnd (WPARAM wParam, LPARAM lParam)
{
	CGridFenster	*polGrid = (CGridFenster*)wParam;

	if( polGrid )
	{
		if ( polGrid == pomRulesGrid )
		{
			pomRulesGrid->GetSelectedRows(omSelRueLines,FALSE,FALSE);
			UpdateSelUrnoMap ( pomRulesGrid, omSelRueLines, omSelRueUrnos, COL_RUE_URNO );
			UpdateRulesValues();
			UpdateUsedFlags ();
		}
		if ( polGrid == pomServiceGrid )
		{
			pomServiceGrid->GetSelectedRows(omSelSerLines,FALSE,FALSE);
			UpdateSelUrnoMap ( pomServiceGrid, omSelSerLines, omSelSerUrnos, COL_SER_URNO );
			UpdateServiceValues();
		}
		EnableButtons();
	}
}


bool CMultiEditDlg::FillLogicTables ()
{
	CCSPtrArray <RecordSet>	olRecords;
	int			i, j, ilCount;
	CString		olTplUrno[2];
	CStringList olRuleUrnos;
	bool		blOk = true;
	RecordSet	olRudRecord(ogBCD.GetFieldCount("RUD") );
	
	blOk = ogDataSet.ResetLogicTable ( "RUE_TMP" );
	blOk &= ogDataSet.ResetLogicTable ( "RUD_TMP" );
	if ( !blOk )
	{
		TRACE ( "CMultiEditDlg::FillLogicTables:  ResetLogicTable failed\n" );
		return false;
	}

	olTplUrno[0] = omTplUrno;
	olTplUrno[1] = GetCorrespTplUrno(omTplUrno);
	
	//  Fill Logic Table: Rules 
	for ( i = 0; i < 2; i++)
	{
		if ( olTplUrno[i].IsEmpty() )
			continue;
		ogBCD.GetRecords("RUE", "UTPL", olTplUrno[i], &olRecords);
		int ilCount = olRecords.GetSize();
		for (j = 0; j < ilCount; j++)
		{
			if ( ogBCD.InsertRecord ( "RUE_TMP", olRecords[j] ) )
				olRuleUrnos.AddTail ( olRecords[j].Values[igRueUrnoIdx] );
			else
				blOk = false;
		}
		olRecords.DeleteAll();
	}
	if ( !blOk )
	{
		TRACE ( "CMultiEditDlg::FillLogicTables:  Filling of RUE_TMP failed\n" );
		blOk = true;
	}

	//  Fill Logic Table: Rules 
	ilCount = ogBCD.GetDataCount("RUD");
	for ( i=0; i<ilCount; i++ )
	{
		if ( ogBCD.GetRecord( "RUD", i,  olRudRecord ) &&
			 olRuleUrnos.Find ( olRudRecord.Values[igRudUrueIdx] ) )
		{
			if ( !ogBCD.InsertRecord ( "RUD_TMP", olRudRecord ) )
				blOk = false;
		}			
	}
	if ( !blOk )
	{
		TRACE ( "CMultiEditDlg::FillLogicTables:  Filling of RUD_TMP failed\n" );
		blOk = true;
	}
	ogDataSet.FillLogicRpfObject();
	ogDataSet.FillLogicRpqObject();
	ogDataSet.FillLogicRloObject();
	ogDataSet.FillLogicReqObject();
	TRACE ( "FillLogicTables: RUE <%d> RUD <%d>  RPF <%d> RPQ <%d> RLO <%d> REQ <%d>\n",
			ogBCD.GetDataCount("RUE_TMP"), ogBCD.GetDataCount("RUD_TMP"), 
			ogBCD.GetDataCount("RPF_TMP"), ogBCD.GetDataCount("RPQ_TMP"), 
			ogBCD.GetDataCount("RLO_TMP"), ogBCD.GetDataCount("REQ_TMP") );
	return blOk;
}

void CMultiEditDlg::UpdateSelUrnoMap ( CGridFenster *popGrid, CRowColArray &ropSelLines, 
									   CMapStringToOb &ropSelUrnos, int ipUrnoIdx )
{
	CString olUrno;
	ROWCOL	ilRow;

	if ( !popGrid )
		return;

	ropSelUrnos.RemoveAll();
	for ( int i=0; i<ropSelLines.GetSize(); i++ )
	{
		ilRow = ropSelLines[i];
		if ( ilRow < 1 )
			continue;	//  head line not allowed
		olUrno = popGrid->GetValueRowCol(ropSelLines[i], ipUrnoIdx );
		if ( !olUrno.IsEmpty() )
			ropSelUrnos.SetAt ( olUrno, (CObject*)1 );
	}
}

void CMultiEditDlg::OnSet() 
{
	// TODO: Add your control notification handler code here
	int		ilTmp, j, i;
	CString olValue, olRueUrno;
	CEdit *polCtrl = 0;
	int		ilValues[5];
	bool	blSaveValue[5];
	char	clFields[][5] = { "DEDU", "SUTI", "SDTI", "TTGT", "TTGF" };
	bool	blActChanged;
	int		ilIdx[5];
	RecordSet	olRecord;
	CObject		*polObject;

	for ( j=0; j<5; j++ )
	{
		//  initialize arrays
		ilIdx[j] = ogBCD.GetFieldIndex ( "RUD", clFields[j] );
		if ( ilIdx[j] < 0 )
		{
			olValue.Format ( "Field <%s> not known in RUDTAB", clFields[j] );
			MessageBox ( olValue );
			return;
		}
		ilValues[j] = -1;
		blSaveValue[j] = false;

		if ( IsDlgButtonChecked ( IDC_FDUT+j ) ) 
		{
			GetDlgItemText ( IDC_SDUT+j, olValue );
			if ( (sscanf( olValue, "%d", &ilTmp )<1) || (ilTmp<0) )
			{
				if ( polCtrl = (CEdit*)GetDlgItem ( IDC_SDUT+j ) )
				{
					polCtrl->SetFocus();
					polCtrl->SetSel(0,-1);
				}
				AfxMessageBox ( GetString(IDS_STRING1437), MB_ICONEXCLAMATION | MB_OK );
				return;
			}
			ilValues[j] = ilTmp * 60;
			blSaveValue[j] = true;
			olValue.Format ( "%d", ilTmp );
			SetDlgItemText ( IDC_SDUT+j, olValue );
		}
	}

	for ( i=0; i<ogBCD.GetDataCount("RUD_TMP"); i++ )
	{
		if ( ogBCD.GetRecord("RUD_TMP", i, olRecord ) )
		{
			olRueUrno = olRecord.Values[igRudUrueIdx];
			if ( omSelRueUrnos.Lookup ( olRueUrno, polObject ) )
			{	//  This is a RUD-record of a marked rule
				blActChanged = false;
				for ( j=0; j<5; j ++ )
				{
					if ( blSaveValue[j] )
					{
						ilTmp = atoi( olRecord.Values[ilIdx[j]] );
						if ( ilTmp != ilValues[j] )
						{
							olRecord.Values[ilIdx[j]].Format("%d", ilValues[j]);
							blActChanged = bmTimesChanged = true;
						}
					}
				}
				if ( blActChanged )
				{
					ogBCD.SetRecord ( "RUD_TMP", "URNO", 
									   olRecord.Values[igRudUrnoIdx], olRecord.Values );
					if ( !omRueTimeChanges.Find ( olRueUrno ) )
						omRueTimeChanges.AddTail ( olRueUrno ) ;
				}
			}
		}
	}
	EnableButtons();
	for ( j=0; j<5; j++ )
		CheckDlgButton( IDC_FDUT+j, 0 );
	OnFdut();
	OnFsdt();
	OnFsut();
	OnFwtf();
	OnFwtt();
}


/*
void CMultiEditDlg::OnSave() 
{
	// TODO: Add your control notification handler code here
	if ( bmTimesChanged || bmDutiesChanged )
	{	
		// Save Changes to DB
		if ( DoSave() )
		{
			bmTimesChanged = bmDutiesChanged = false;
			omRueDutyChanges.RemoveAll () ;
			omRueTimeChanges.RemoveAll () ;
		}
	}
}
*/

void CMultiEditDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	if ( SaveChanges() )
	{
		ogDataSet.ResetLogicTable ( "RUE_TMP" );
		ogDataSet.ResetLogicTable ( "RUD_TMP" );
		ogDataSet.ResetLogicTable ( "RPF_TMP" );
		ogDataSet.ResetLogicTable ( "RPQ_TMP" );
		ogDataSet.ResetLogicTable ( "RLO_TMP" );
		ogDataSet.ResetLogicTable ( "REQ_TMP" );
		CDialog::OnCancel();
	}
}

bool CMultiEditDlg::SaveChanges()
{
	bool blRet = true;

	if (bmTimesChanged || bmDutiesChanged)
	{
		int ilSel = MessageBox(GetString(IDS_STRING1574), GetString(161), MB_YESNO);
		if(ilSel == IDYES)
		{
			blRet = DoSave();
		}
		bmDutiesChanged = false;
		bmTimesChanged = false;
		omRueDutyChanges.RemoveAll () ;
		omRueTimeChanges.RemoveAll () ;
	}
	return blRet;
} 

bool CMultiEditDlg::DoSave()
{	// Save Changes to DB

	POSITION pos;
	CString	 olUrno, olFisu, olMsg, olRusn;
	bool	 blOk = true;
	bool	 blRet = true;

	theApp.BeginWaitCursor ();

	//  Rules, where duty requirements have been added or deleted, are archived,
	//  if necessary (FISU=1). Rules, where only times have been changed are 
	//  modified directly also, if FISU=1
	
	while ( omRueDutyChanges.GetCount () > 0 )
	{
		olUrno = omRueDutyChanges.RemoveHead ();
		olFisu = ogBCD.GetField ( "RUE_TMP", "URNO", olUrno, "FISU" );
		if ( olFisu.Left(1) == CString("1") )
			blOk = SaveUsedRuleInDB ( olUrno );
		else
			blOk = UpdateRuleInDB ( olUrno );
		if ( blOk )
		{
			if ( pos = omRueTimeChanges.Find ( olUrno ) )
				omRueTimeChanges.RemoveAt ( pos );	// not necessary to save twice
		}
		else
		{
			olMsg = GetString ( IDS_SAVE_RULE_ERR );
			olRusn = ogBCD.GetField ( "RUE_TMP", "URNO", olUrno, "RUSN" );
			olMsg.Format ( olMsg, olRusn );
			MessageBox ( olMsg, "CMultiEditDlg::DoSave" );
			blRet = false;
		}

	}
	while ( omRueTimeChanges.GetCount () > 0 )
	{
		olUrno = omRueTimeChanges.GetHead ();
		//  we still need the URNO in omRueTimeChanges inside UpdateRuleInDB
		blOk = UpdateRuleInDB ( olUrno );
		omRueTimeChanges.RemoveHead ();
		if ( !blOk )
		{
			olMsg = GetString ( IDS_SAVE_RULE_ERR );
			olRusn = ogBCD.GetField ( "RUE_TMP", "URNO", olUrno, "RUSN" );
			olMsg.Format ( olMsg, olRusn );
			MessageBox ( olMsg, "CMultiEditDlg::DoSave" );
			blRet = false;
		}
	}
	blOk &= ogBCD.Save ( "RUE" );
	blOk &= ogBCD.Save ( "RUD" );
	blOk &= ogBCD.Save ( "RPF" );
	blOk &= ogBCD.Save ( "RPQ" );
	blOk &= ogBCD.Save ( "RLO" );
	blOk &= ogBCD.Save ( "REQ" );
	
	theApp.EndWaitCursor();
	return blRet;
}

bool CMultiEditDlg::SaveUsedRuleInDB ( CString &ropRueUrno )
{
	long					llNewUrno = ogBCD.GetNextUrno();
	CString					olNewRueUrno, olUrud, olUdgr;
	CCSPtrArray <RecordSet> olRudsOfRule;
	RecordSet				olRudRecord(ogBCD.GetFieldCount("RUD"));
	RecordSet				olRueRecord;
	int						i;
	VALDATA					olValData;
	bool					blOk = true;
	CStringList				olNewUruds, olNewUdgrs;

	// There have been demands created using this rule (Possibility 3)	
	// ==> archive old rue and insert copies for the rest
				
	olNewRueUrno.Format("%ld", llNewUrno);
	blOk = ogDataSet.OnCopyRue(ropRueUrno, olNewRueUrno );

	//  Save DemandRequirements
	ogBCD.GetRecords ( "RUD_TMP", "URUE", olNewRueUrno, &olRudsOfRule );
	for ( i=0; i<olRudsOfRule.GetSize(); i++ )
	{
		olRudRecord = olRudsOfRule[i];
		if ( ogBCD.InsertRecord ( "RUD", olRudRecord ) )
		{
			olUrud = olRudRecord.Values[igRudUrnoIdx];
			olUdgr = olRudRecord.Values[igRudUdgrIdx];
			//blOk = InsertResForNewRud ( olRudRecord );
			olNewUruds.AddTail ( olUrud );
			if ( !olNewUdgrs.Find ( olUdgr ) )
				olNewUdgrs.AddTail ( olUdgr );
		}
		else
			blOk = false;
	}
	olRudsOfRule.DeleteAll ();

	// third: we re-insert the new ressources
	while ( !olNewUruds.IsEmpty() )
	{
		olUrud = olNewUruds.RemoveHead ();
		blOk &= InsertResources ( "RPF", "URUD", olUrud );
		blOk &= InsertResources ( "RPQ", "URUD", olUrud );
		blOk &= InsertResources ( "RLO", "URUD", olUrud );
		blOk &= InsertResources ( "REQ", "URUD", olUrud );
	}
	//  Re-insert group qualifications
	while ( !olNewUdgrs.IsEmpty() )
	{
		olUdgr = olNewUdgrs.RemoveHead ();
		blOk &= InsertResources ( "RPQ", "UDGR", olUdgr );
	}

	if ( LoadValidity( ropRueUrno, olValData ) )
		blOk &= SaveValidity( olNewRueUrno, olValData, true );
	else
		blOk = false;

	if ( ogBCD.GetRecord ("RUE", "URNO", ropRueUrno, olRueRecord ) )
	{
		CString olDate = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
		CString olUser = pcgUser;
 
		olRueRecord.Values[igRueUseuIdx] = olUser;
		olRueRecord.Values[igRueLstuIdx] = olDate;

		olRueRecord.Values[igRueUrnoIdx] = olNewRueUrno;
		olRueRecord.Values[igRueFisuIdx] = "0";
		if ( ogBCD.InsertRecord("RUE", olRueRecord, true) )
		{
			blOk &= ogBCD.SetField("RUE", "URNO", ropRueUrno, "RUST", "2");
			blOk &= ogBCD.SetField("RUE", "URNO", ropRueUrno, "USEU", olUser );
			blOk &= ogBCD.SetField("RUE", "URNO", ropRueUrno, "LSTU", olDate);
			blOk &= ogBCD.SetField("RUE", "URNO", olNewRueUrno, "USEU", olUser );
			blOk &= ogBCD.SetField("RUE", "URNO", olNewRueUrno, "LSTU", olDate);
		}
		else
			blOk = false;
	}
	else
		blOk = false;

	return blOk;
}	

/*
bool CMultiEditDlg::UpdateRuleInDB ( CString &ropRueUrno )
{
	CCSPtrArray <RecordSet> olRudsOfRule;
	RecordSet				olRudRecord(ogBCD.GetFieldCount("RUD"));
	bool					blOk = true;
	CString					olDate;
	CStringArray			olRudUrnoArr;
	CStringList				olUdgrList;
	CString					olUdgr, olUrno;

	ogBCD.GetRecords("RUD_TMP", "URUE", ropRueUrno, &olRudsOfRule);
	for (int k = olRudsOfRule.GetSize() - 1; k >= 0; k--)
	{
		olRudRecord = olRudsOfRule[k];
		olUrno = olRudRecord.Values[igRudUrnoIdx];
		if ( ogBCD.GetField ( "RUD", "URNO", olUrno, "UDGR", olUdgr ) )
			blOk &= ogBCD.SetRecord("RUD", "URNO", olUrno, olRudRecord.Values );
		else
		{	//  new duty requirement
			if ( ogBCD.InsertRecord ( "RUD", olRudRecord ) )
				blOk = InsertResForNewRud ( olRudRecord );
			else
				blOk = false;
		}
	}
	olRudsOfRule.DeleteAll ();
	olDate = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
	blOk &= ogBCD.SetField ( "RUE", "URNO", ropRueUrno, "USEU", pcgUser );
	blOk &= ogBCD.SetField ( "RUE", "URNO", ropRueUrno, "LSTU", olDate );
	return blOk;
}	
*/

/*
bool CMultiEditDlg::InsertResForNewRud ( RecordSet &ropRudRecord )
{
	bool blOk = true;
	CCSPtrArray <RecordSet> olGroupQual;
	RecordSet				olResourceRecord;
	CString					olUrud, olUdgr;
	
	olUrud = ropRudRecord.Values[igRudUrnoIdx];
	olUdgr = ropRudRecord.Values[igRudUdgrIdx];

	//  Save Function
	if ( ogBCD.GetRecord ( "RPF_TMP", "URUD", olUrud, olResourceRecord ) )
		blOk &= ogBCD.InsertRecord ( "RPF", olResourceRecord );
	//  Save Qualifications
	if ( ogBCD.GetRecord ( "RPQ_TMP", "URUD", olUrud, olResourceRecord ) )
		blOk &= ogBCD.InsertRecord ( "RPQ", olResourceRecord );
	//  Save Locations
	if ( ogBCD.GetRecord ( "RLO_TMP", "URUD", olUrud, olResourceRecord ) )
		blOk &= ogBCD.InsertRecord ( "RLO", olResourceRecord );
	//  Save Equipment
	if ( ogBCD.GetRecord ( "REQ_TMP", "URUD", olUrud, olResourceRecord ) )
		blOk &= ogBCD.InsertRecord ( "REQ", olResourceRecord );
	//  Save Groupqualifications
	//if ( !olUdgr.IsEmpty() ) 
	//{
	//	ogBCD.GetRecords ( "RPQ_TMP", "UDGR", olUdgr, &olGroupQual );
	//	for ( int j=0; j<olGroupQual.GetSize(); j++ )
	//	{
	//		blOk &= ogBCD.InsertRecord ( "RPQ", olGroupQual[j] );
	//	}			
	//	olGroupQual.DeleteAll ();
	//}
	return blOk;
}
*/

void UpdateRudDispValues ( CString opRuleUrno, CString opTable, bool bpSave/*=false*/ )
{
	int						ilCount, i, ilMaxDisp = 0, ilDisp;
	CMapStringToString		olDispUrnoMap;
	CStringList				olUrnosToDo;
	CCSPtrArray <RecordSet>	olRecords;
	CString					olUrno, olDisp, olTmp;
	RecordSet				olRecord;

	ogBCD.GetRecords ( opTable, "URUE", opRuleUrno, &olRecords );
	ilCount = olRecords.GetSize ();
	for ( i=0; i<ilCount; i++ )
	{
		olDisp = olRecords[i].Values[igRudDispIdx];
		olUrno = olRecords[i].Values[igRudUrnoIdx];
		if ( olUrno.IsEmpty () )
			continue;
		ilDisp = atoi ( olDisp );
		if ( ( ilDisp<=0 ) || ( ilDisp >= 998 ) )
			olUrnosToDo.AddTail ( olUrno );
		else
		{
			if ( olDispUrnoMap.Lookup ( olDisp, olTmp ) )
				olUrnosToDo.AddTail ( olUrno );
			else
			{
				olDispUrnoMap.SetAt ( olDisp, olUrno );
				ilMaxDisp = max ( ilDisp, ilMaxDisp );
			}
		}
	}
	olRecords.DeleteAll();
	while ( olUrnosToDo.GetCount() > 0 )
	{
		olUrno = olUrnosToDo.RemoveHead ();
		if ( ogBCD.GetRecord ( opTable, "URNO", olUrno, olRecord ) )
		{
			olDisp.Format ( "%d", ++ilMaxDisp );	
			olRecord.Values[igRudDispIdx] = olDisp;
			ogBCD.SetRecord(opTable, "URNO", olUrno, olRecord.Values );
			/*
			if ( opTable == "RUD" )
				ogDdx.DataChanged((void *)&theApp,RUD_CHANGE,(void *)&olRecord);
			else
				if ( opTable == "RUD_TMP" )
					ogDdx.DataChanged((void *)&theApp,RUD_TMP_CHANGE,(void *)&olRecord);
			*/
		}

	}
	if ( bpSave )
		ogBCD.Save ( opTable );
}

/*
bool CMultiEditDlg::UpdateRuleInDB ( CString &ropRueUrno )
{
	CStringArray			olRudUrnoArr;
	CCSPtrArray <RecordSet> olRudsOfRule;
	CStringList				olUdgrList;
	CString					olUdgr, olUrno, olUrud;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
	RecordSet				olResourceRecord;
	bool					blOk = true;

	//  delete all original RUDs and collect URNOs and UDGRs
	ogBCD.GetRecords("RUD", "URUE", ropRueUrno, &olRudsOfRule);
	for (int k = olRudsOfRule.GetSize() - 1; k >= 0; k--)
	{
		olRudRecord = olRudsOfRule[k];
		olUrno = olRudRecord.Values[igRudUrnoIdx];
		olRudUrnoArr.Add(olUrno);
		olUdgr = olRudRecord.Values[igRudUdgrIdx];
		if (olUdgr.IsEmpty() == FALSE && !olUdgrList.Find(olUdgr))
		{
			olUdgrList.AddTail (olUdgr);
		}
		ogBCD.DeleteRecord("RUD", "URNO", olUrno, true);
	}
	olRudsOfRule.DeleteAll();
	
	ogDataSet.RemoveResources(olRudUrnoArr, &olUdgrList);
	olUdgrList.RemoveAll();
	olRudUrnoArr.RemoveAll();

	// second: we re-insert the new RUDs
	ogBCD.GetRecords("RUD_TMP", "URUE", ropRueUrno, &olRudsOfRule);
	for ( k = olRudsOfRule.GetSize() - 1; k >= 0; k--)
	{
		olRudRecord = olRudsOfRule[k];
		olUdgr = olRudRecord.Values[igRudUdgrIdx];
		if (olUdgr.IsEmpty() == FALSE && !olUdgrList.Find(olUdgr))
		{
			olUdgrList.AddTail (olUdgr);
		}
		blOk &= ogBCD.InsertRecord ( "RUD", olRudRecord );
	}
	
	// third: we re-insert the new ressources
	for ( k = olRudsOfRule.GetSize() - 1; k >= 0; k--)
	{
		olUrud = olRudsOfRule[k].Values[igRudUrnoIdx];
		blOk &= InsertResources ( "RPF", "URUD", olUrud );
		blOk &= InsertResources ( "RPQ", "URUD", olUrud );
		blOk &= InsertResources ( "RLO", "URUD", olUrud );
		blOk &= InsertResources ( "REQ", "URUD", olUrud );
	}
	//  Re-insert group qualifications
	while ( !olUdgrList.IsEmpty() )
	{
		olUdgr = olUdgrList.RemoveHead ();
		blOk &= InsertResources ( "RPQ", "UDGR", olUdgr );
	}

	olRudsOfRule.DeleteAll();
	return blOk;
}
*/

bool CMultiEditDlg::InsertResources ( CString opTable, CString opRefField, CString opRefValue )
{
	CCSPtrArray <RecordSet> olResources;
	CString					olTmpTable;
	bool					blOk = true;
	int						ilAdded = 0;
	
	olTmpTable.Format ( "%s_TMP", opTable );
	ogBCD.GetRecords(olTmpTable, opRefField, opRefValue, &olResources );
	for ( int i=0; i<olResources.GetSize(); i++ )
	{
		if ( ogBCD.InsertRecord ( opTable, olResources[i] ) )
			ilAdded ++;
	}
	blOk = ( olResources.GetSize() == ilAdded );
	olResources.DeleteAll();
	return blOk;
}

void CMultiEditDlg::OnOK() 
{
	// TODO: Add extra validation here
	bool blRet = true;
	if (bmTimesChanged || bmDutiesChanged)
	{
		blRet = DoSave();
	}
	if ( blRet )
		CDialog::OnOK();
}

bool CMultiEditDlg::UpdateRuleInDB ( CString &ropRueUrno )
{
	CStringArray			olRudsToDelete;
	CCSPtrArray <RecordSet> olRudsOfRule;
	CStringList				olUdgrToDelete;
	CStringList				olUdgrToAdd;
	CStringList				olUrudsKept;
	CStringList				olNewUruds;
	CString					olUdgr, olUrno, olUrud;
	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
	RecordSet olRudTmpRecord(ogBCD.GetFieldCount("RUD_TMP"));
	bool					blOk = true;
	bool					blTimeChanged = false;

	if ( omRueTimeChanges.Find ( ropRueUrno ) ) 
		blTimeChanged = true;	// prevents from unnecessary calls of SetRecord

	//  delete all no longer needed original RUDs and collect URNOs and UDGRs
	ogBCD.GetRecords("RUD", "URUE", ropRueUrno, &olRudsOfRule);
	for (int k = olRudsOfRule.GetSize() - 1; k >= 0; k--)
	{
		olRudRecord = olRudsOfRule[k];
		olUrno = olRudRecord.Values[igRudUrnoIdx];
		if ( ogBCD.GetRecord ( "RUD_TMP", "URNO", olUrno, olRudTmpRecord ) )
		{
			olUrudsKept.AddTail ( olUrno ); 
			continue;	//  this record is needed further on
		}
		olRudsToDelete.Add(olUrno);
		olUdgr = olRudRecord.Values[igRudUdgrIdx];
		TRACE ( "RUD URNO <%s> UDGR <%s> is not needed any longer\n", olUrno, olUdgr );
		if (olUdgr.IsEmpty() == FALSE && !olUdgrToDelete.Find(olUdgr))
		{
			olUdgrToDelete.AddTail (olUdgr);
		}
		ogBCD.DeleteRecord("RUD", "URNO", olUrno, true);
	}
	olRudsOfRule.DeleteAll();
	
	ogDataSet.RemoveResources(olRudsToDelete, &olUdgrToDelete);
	olUdgrToDelete.RemoveAll();
	olRudsToDelete.RemoveAll();
	//  now unnecessary RUD and resources have been deleted

	// second: we insert the new RUDs and update remaining RUDs
	ogBCD.GetRecords("RUD_TMP", "URUE", ropRueUrno, &olRudsOfRule);
	for ( k = olRudsOfRule.GetSize() - 1; k >= 0; k--)
	{
		olRudRecord = olRudsOfRule[k];
		olUrno = olRudRecord.Values[igRudUrnoIdx];
		if ( olUrudsKept.Find ( olUrno ) )
		{
			if ( blTimeChanged )
			{
				blOk &= ogBCD.SetRecord("RUD", "URNO", olUrno, olRudRecord.Values );
				TRACE ( "RUD URNO <%s> UDGR <%s> is updated\n", olUrno, olUdgr );
			}
		}
		else 
		{
			olNewUruds.AddTail ( olUrno );
			olUdgr = olRudRecord.Values[igRudUdgrIdx];
			if (olUdgr.IsEmpty() == FALSE && !olUdgrToAdd.Find(olUdgr))
			{
				olUdgrToAdd.AddTail (olUdgr);
			}
			blOk &= ogBCD.InsertRecord ( "RUD", olRudRecord );
			TRACE ( "RUD URNO <%s> UDGR <%s> is inserted\n", olUrno, olUdgr );
		}
	}
	olRudsOfRule.DeleteAll();

	// third: we re-insert the new ressources
	while ( !olNewUruds.IsEmpty() )
	{
		olUrud = olNewUruds.RemoveHead ();
		blOk &= InsertResources ( "RPF", "URUD", olUrud );
		blOk &= InsertResources ( "RPQ", "URUD", olUrud );
		blOk &= InsertResources ( "RLO", "URUD", olUrud );
		blOk &= InsertResources ( "REQ", "URUD", olUrud );
	}
	//  Re-insert group qualifications
	while ( !olUdgrToAdd.IsEmpty() )
	{
		olUdgr = olUdgrToAdd.RemoveHead ();
		blOk &= InsertResources ( "RPQ", "UDGR", olUdgr );
	}

	CString olDate = CTime::GetCurrentTime().Format("%Y%m%d%H%M%S");
	CString olUser = pcgUser;
 
	blOk &= ogBCD.SetField("RUE", "URNO", ropRueUrno, "USEU", olUser );
	blOk &= ogBCD.SetField("RUE", "URNO", ropRueUrno, "LSTU", olDate);

	return blOk;
}

