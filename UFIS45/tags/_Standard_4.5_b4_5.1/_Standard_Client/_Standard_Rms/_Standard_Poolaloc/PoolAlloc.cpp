// PoolAlloc.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>

#include <PoolAlloc.h>
#include <PoolAllocDlg.h>

#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <CCSCedaCom.h>
#include <PrivList.h>
#include <LoginDlg.h>
#include <RegisterDlg.h>
#include <CCSLOG.h>
#include <StartDlg.h>
#include <BasicData.h>
#include <GUILng.h>
#include <VersionInfo.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




//---------------------------------------------------------------------------------------------------------------------
//					static functions, makros, etc
//---------------------------------------------------------------------------------------------------------------------
static int CompareRecords(const RecordSet **elem1, const RecordSet **elem2);



//---------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//---------------------------------------------------------------------------------------------------------------------
PoolAllocApp::PoolAllocApp()
{
}
//---------------------------------------------------------------------------------------------------------------------

PoolAllocApp::~PoolAllocApp()
{
	ogChoiceLists.DeleteAll();
	ogRefTables.DeleteAll();
}
//---------------------------------------------------------------------------------------------------------------------

PoolAllocApp theApp;



//---------------------------------------------------------------------------------------------------------------------
//					message map
//---------------------------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(PoolAllocApp, CWinApp)
	//{{AFX_MSG_MAP(PoolAllocApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()



//---------------------------------------------------------------------------------------------------------------------
//					message handlers
//---------------------------------------------------------------------------------------------------------------------
BOOL PoolAllocApp::InitInstance()
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

#ifdef _AFXDLL
    Enable3dControls( );   //Call Enable3dControls
#else
    Enable3dControlsStatic( );  // Call this when linking to MFC statically
#endif

	// Standard initialization

	AfxEnableControlContainer();

	//  SetDialogBkColor();        // Set dialog background color to gray


	GXInit();	// basic grid initialization

	
//--- initialize CEDA
	if (getenv("CEDA") != NULL)
	{
		strcpy(pcgConfigPath, getenv("CEDA"));
	}
	else
	{
		strcpy(pcgConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	
	// get initialization parameters for CCSCedaData
	GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof(pcgTableExt), pcgConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "ZRH", pcgHome, sizeof(pcgHome), pcgConfigPath);
	
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	// comm handler initialization
	ogCommHandler.SetAppName(ogAppName);
    
	bool blErr  = ogCommHandler.Initialize();
	int ilErr = 0;

    if (blErr != true)  
	{
		AfxMessageBox(ogCommHandler.LastError());
        return FALSE;
	}

	VersionInfo olInfo;
	VersionInfo::GetVersionInfo(NULL,olInfo);
	CCSCedaData::bmVersCheck = true;
	strcpy(CCSCedaData::pcmVersion, olInfo.omFileVersion);
	strcpy(CCSCedaData::pcmInternalBuild, olInfo.omPrivateBuild);

//--- path to logfile for ccslog
	char pclTmp[128];
	GetPrivateProfileString(ogAppName, "LOGFILE", CCSLog::GetUfisSystemPath("\\RULESLOG.CFG"), pclTmp, sizeof(pclTmp), pcgConfigPath);
	ogLog.Load(pclTmp);

//--- debug status
	bgDebug = false;
	GetPrivateProfileString(ogAppName, "DEBUG", "OFF", pclTmp, sizeof(pclTmp), pcgConfigPath);
	if (strcmp(pclTmp, "ON") == 0)
	{
		bgDebug = true;
	}


//--- initialize CedaBasicData
	ogBCD.SetTableExtension(pcgTableExt);



//--- read text strings for specified language
	char pclDBParam[128];
	CString olErrorTxt;
	GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof(pclTmp), pcgConfigPath);

	GetPrivateProfileString(ogAppName, "DBParam", "0", pclDBParam, sizeof pclDBParam, pcgConfigPath);
	bool blLangInitOk = pogGUILng->MemberInit(&olErrorTxt, "XXX", "POOLALOC", pcgHome, 
											 pclTmp, pclDBParam);
	/*
	if (strlen(pclTmp) >= 2)
	{
		CString olLang = pclTmp;
		char pclWhere[41];
		olLang = olLang.Left(2);
		strupr(pclTmp);
		if (strstr(pclTmp, "TEST"))
		{
			bgUseResourceStrings = false;
		}

		ogBCD.SetObject("TXT");
		sprintf (pclWhere, "WHERE APPL='POOLALOC'");
		ogBCD.Read("TXT",pclWhere);
		ogBCD.AddKeyMap("TXT", "STID");
	}
	*/

  
//---	commandline parameters
	ogCmdLineArgsArray.RemoveAll();
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	if(olCmdLine.GetLength() == 0)	// set defaults 
	{
		//default for PoolAlloc
		ogCmdLineArgsArray.Add(ogAppName);
		ogCmdLineArgsArray.Add("");
		ogCmdLineArgsArray.Add("");
	}
	else
	{
		int ilParamCount = ExtractItemList(olCmdLine,&ogCmdLineArgsArray, ' ');
		if(ilParamCount != 3)
		{
			MessageBox(NULL,GetString(1487), GetString(AFX_IDS_APP_TITLE), MB_ICONERROR);
			return FALSE;
		}
	}

	//--- Help initialization
	if (!AatHelp::Initialize(ogAppName))
	{
		CString olErrorMsg;
		AatHelp::State elErrorState;
		elErrorState = AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}


//--- Login Dialog
	bool blAutomaticStartUp = false;
#if	0
	CLoginDialog olLoginDlg(pcgHome, ogAppName, NULL);

	if(ogCmdLineArgsArray.GetAt(0) != ogAppName)
	{
		if(olLoginDlg.Login(ogCmdLineArgsArray.GetAt(1),ogCmdLineArgsArray.GetAt(2)) == true)
		{
			blAutomaticStartUp = true;
		}
	}
	
	if (blAutomaticStartUp == false)
	{
		if( olLoginDlg.DoModal() != IDCANCEL)
		{
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				if (olRegisterDlg.DoModal() != IDOK)
				{
					return FALSE;
				}
			}
		}
		else
		{
			return FALSE;
		} 	
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName(ogAppName);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

	if(ogCmdLineArgsArray.GetAt(0) != ogAppName)
	{
		if(olLoginCtrl.DoLoginSilentMode(ogCmdLineArgsArray.GetAt(1),ogCmdLineArgsArray.GetAt(2)) == "OK")
		{
			blAutomaticStartUp = true;
		}
	}
	
	if (blAutomaticStartUp == false)
	{
		if (olLoginCtrl.ShowLoginDialog() != "OK")
		{
			olDummyDlg.DestroyWindow();
			return FALSE;
		}

	}

	strcpy(pcgUser,olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif



//--- read table data into global object
    //make StartDlg
	StartDlg *polDlg = new StartDlg;
	if (blAutomaticStartUp == false)
	{
		polDlg->ShowWindow(SW_SHOW);
	}

	// ALO (allocation Units)
	polDlg->AddString(GetString(1441));
	blErr = ogBCD.SetObject("ALO");
			ogBCD.Read("ALO");
 			ogBCD.AddKeyMap("ALO", "URNO");
	
	// load all tables referred to in ALO
	CString olReft, olTab, olField, olArrTab, olArrField, olSort;
	int ilAloCount = ogBCD.GetDataCount("ALO");
	for (int i = 0; i < ilAloCount; i++)
	{
		olReft = ogBCD.GetField("ALO", i, "REFT");
		if (olReft.GetLength() == 8)
		{
			olTab = olReft.Left(3);
			olField = olReft.Right(4);
		}

		bool blTabFound = false;
		bool blFieldFound = false;
		int ilRefTabCount = ogRefTables.GetSize();
		for (int j = 0; j < ilRefTabCount; j++)
		{	
			olArrTab = ogRefTables[j].RTAB;
			if (olArrTab == olTab)
			{
				blTabFound = true;
				for (int k = 0; k < ogRefTables[j].Fields.GetSize(); k++)
				{
					olArrField = ogRefTables[j].Fields.GetAt(k);
					if (olArrField == olField)
					{
						blFieldFound = true;
						k = ogRefTables[j].Fields.GetSize();
					}
				}
				
				if (blFieldFound == false)
				{
					ogRefTables[j].Fields.Add(olField);	
				}
				j = ilRefTabCount;	// break loop now
			}
		}

		if (blTabFound == false)
		{
			REFTABLE *polRefTab = new REFTABLE;
			polRefTab->RTAB = olTab;
			polRefTab->Fields.Add(olField);
			ogRefTables.Add(polRefTab);
		}
	}
	
	// read all tables referred to in ALO into memory
	for (i = 0; i < ogRefTables.GetSize(); i++)
	{
		CString olFieldList, olTmp, olWhere;
		bool blIsUrno = false;

		for (int j = 0; j < ogRefTables[i].Fields.GetSize(); j++)
		{
			olTmp = ogRefTables[i].Fields.GetAt(j); 
			olFieldList += olTmp;
			if (olTmp == CString("URNO"))
			{
				blIsUrno = true;
			}
		}

		if (blIsUrno == false)
		{
			olFieldList += CString("URNO");
		}
			
		blErr = ogBCD.SetObject(ogRefTables[i].RTAB, olFieldList);
		ogBCD.Read(ogRefTables[i].RTAB);
		olSort = ogRefTables[i].Fields[0];
		if ( !olSort.IsEmpty() )
		{
			olSort += "+";
			ogBCD.SetSort(ogRefTables[i].RTAB, olSort, true );
		}
		ogBCD.AddKeyMap(ogRefTables[i].RTAB, "URNO");
		polDlg->AddString(GetString(1451) + CString (" ") + ogRefTables[i].RTAB);
	}

	// COT (contract types)
	polDlg->AddString(GetString(1447));
	blErr = ogBCD.SetObject("COT");
			ogBCD.Read("COT");
 			ogBCD.AddKeyMap("COT", "URNO");

	// POA (pools allocation)
	polDlg->AddString(GetString(1446));
    blErr = ogBCD.SetObject("POA");
			ogBCD.Read("POA");
 			ogBCD.AddKeyMap("POA", "URNO");

	// POL (pools)
	polDlg->AddString(GetString(1444));
    blErr = ogBCD.SetObject("POL");
	ilErr = ogBCD.Read("POL");
	if (ilErr == FALSE)
	{
		ogBCD.SetSort("POL", "NAME+", true);
	}
	ogBCD.AddKeyMap("POL", "URNO");

	// SGM (static group members)
	polDlg->AddString(GetString(1439));
	blErr = ogBCD.SetObject("SGM");
			ogBCD.Read("SGM");
 			ogBCD.AddKeyMap("SGM", "URNO");
			ogBCD.AddKeyMap("SGM", "USGR");

	// SGR (static groups)
	blErr = ogBCD.SetObject("SGR");
			ogBCD.Read("SGR", "WHERE APPL='OPSS-PM'");
 			ogBCD.AddKeyMap("SGR", "URNO");

	// ORG (organisation unit)
	polDlg->AddString(GetString(1443));
	blErr = ogBCD.SetObject("ORG");
			ogBCD.Read("ORG");
 			ogBCD.AddKeyMap("ORG", "URNO");

	// PER (permits)
	polDlg->AddString(GetString(1465));
	blErr = ogBCD.SetObject("PER");
			ogBCD.Read("PER");
 			ogBCD.AddKeyMap("PER", "URNO");

	// PFC (personnel functions)
	polDlg->AddString(GetString(1464));
	blErr = ogBCD.SetObject("PFC");
			ogBCD.Read("PFC");
 			ogBCD.AddKeyMap("PFC", "URNO");

	// PGP (planning groups)
	polDlg->AddString(GetString(1442));
	blErr = ogBCD.SetObject("PGP");
			ogBCD.Read("PGP");
 			ogBCD.AddKeyMap("PGP", "URNO");

	// WGP (working groups)
	polDlg->AddString(GetString(1440));
	blErr = ogBCD.SetObject("WGP");
			ogBCD.Read("WGP");
 			ogBCD.AddKeyMap("WGP", "URNO");

			

	// do some preparations for our global data object
	polDlg->AddString(GetString(1448));
	SetGlobalFieldIndices();

	
//--- temporary data objects
	CCSPtrArray <FieldSet> olFieldSets;

	// SGR_TMP
	ogBCD.CloneFieldSet("SGR", olFieldSets);
	ogBCD.SetObject("SGR_TMP", olFieldSets);
	ogBCD.AddKeyMap("SGR_TMP", "URNO");
	ogBCD.SetDdxType("SGR_TMP", "IRT", SGR_TMP_NEW);
	ogBCD.SetDdxType("SGR_TMP", "URT", SGR_TMP_CHANGE);
	ogBCD.SetDdxType("SGR_TMP", "DRT", SGR_TMP_DELETE);
	olFieldSets.DeleteAll();

	// SGM_TMP
	ogBCD.CloneFieldSet("SGM", olFieldSets);
	ogBCD.SetObject("SGM_TMP", olFieldSets);
	ogBCD.AddKeyMap("SGM_TMP", "URNO");
	ogBCD.SetDdxType("SGM_TMP", "IRT", SGM_TMP_NEW);
	ogBCD.SetDdxType("SGM_TMP", "URT", SGM_TMP_CHANGE);
	ogBCD.SetDdxType("SGM_TMP", "DRT", SGM_TMP_DELETE);
	olFieldSets.DeleteAll();
	
	FillTmpDataObjects();
	
	// make ChoiceLists for grid comboboxes
	polDlg->AddString(GetString(1449));
	MakeChoiceLists();

	delete polDlg;

	
//--- start application window
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	PoolAllocDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
//--------------------------------------------------------------------------------------------------------------------

void PoolAllocApp::FillTmpDataObjects()
{
	bool blTest;
	CString olTest, olVal;
	CCSPtrArray <RecordSet> olTmpArr;
	RecordSet olSgr(ogBCD.GetFieldCount("SGR"));

	int ilCount = ogBCD.GetDataCount("SGR");
	for (int i = 0; i < ilCount; i++)
	{
		blTest = ogBCD.GetRecord("SGR", i, olSgr);
		blTest &= ogBCD.InsertRecord("SGR_TMP", olSgr);
		
		// mne debug
		TRACE("[PoolAllocApp::FillTmpDataObj] name of group: %s\n", olSgr.Values[igSgrGrpnIdx]);
		// mne debug

		if (blTest)
		{
			ogBCD.GetRecords("SGM", "USGR", olSgr.Values[igSgrUrnoIdx], &olTmpArr);
			for (int j = 0; j < olTmpArr.GetSize(); j++)
			{
				ogBCD.InsertRecord("SGM_TMP", olTmpArr[j]);
			}
			
			// mne 000512
			if (olTmpArr.GetSize() == 0)
			{
				olVal = "0";
			}
			else
			{
				olVal = "1";
			}
			ogRestrictions.SetAt(olSgr.Values[igSgrUrnoIdx], olVal);
			// mne 000512

			olTmpArr.DeleteAll();
		}
	}
}
//--------------------------------------------------------------------------------------------------------------------

int PoolAllocApp::ExitInstance() 
{
	delete CGUILng::TheOne();
	pogGUILng = 0;
	
	AatHelp::ExitApp();

	return CWinApp::ExitInstance();
}


//--------------------------------------------------------------------------------------------------------------------
//					implementation functions
//--------------------------------------------------------------------------------------------------------------------
void PoolAllocApp::GetData(CString opObject, CCSPtrArray <RecordSet> &ropArray, CString opOrderBy)
{
	// ------------------------------------------------
	//  Take care to delete the array in the calling function!
	//
	// REMARK:	The Map is only used to eliminate multiple entries 
	// (CedaBasicData cannot do SELECT DISTINCT calls at this time)
	// ------------------------------------------------

	
	CCSPtrArray <RecordSet> olTmpArr;
	CMapStringToPtr olMap;
  
	int ilCount = ogBCD.GetAllRecords(opObject, olTmpArr);
	igIdx = ogBCD.GetFieldIndex(opObject, opOrderBy);	// this index will be used in our sort-function !

	for (int i = 0; i < ilCount; i++)
	{
		olMap.SetAt(olTmpArr[i].Values[igIdx], &(olTmpArr[i]));
	}
		
	CString olKey;
	RecordSet *polRecord = NULL;
	POSITION pos = olMap.GetStartPosition();
	
	while (pos != NULL)
	{
		olMap.GetNextAssoc(pos, olKey, (void *&) polRecord);
		ropArray.New((* polRecord));
	}

	ropArray.Sort(CompareRecords);

	olTmpArr.DeleteAll();
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocApp::MakeChoiceLists()
{
	bool blRet = true;

	CCSPtrArray <RecordSet> olDataArr;
	CHOICE_LIST rlChoiceList;
	CString olChoiceList;

	GetData("ALO", olDataArr, "ALOD");
	blRet = ogBasicData.MakeChoiceList(&olDataArr, "ALO", "ALOD", olChoiceList);
	rlChoiceList.TabFieldName = "ALO.ALOD";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();

	GetData("COT", olDataArr, "CTRC");
	blRet = ogBasicData.MakeChoiceList(&olDataArr, "COT", "CTRC", olChoiceList);
	rlChoiceList.TabFieldName = "COT.CTRC";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();
	
	GetData("ORG", olDataArr, "DPT1");
	blRet &= ogBasicData.MakeChoiceList(&olDataArr, "ORG", "DPT1", olChoiceList);
	rlChoiceList.TabFieldName = "ORG.DPT1";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();

	GetData("PER", olDataArr, "PRMC");
	blRet = ogBasicData.MakeChoiceList(&olDataArr, "PER", "PRMC", olChoiceList);
	rlChoiceList.TabFieldName = "PER.PRMC";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();

	GetData("PFC", olDataArr, "FCTC");
	blRet = ogBasicData.MakeChoiceList(&olDataArr, "PFC", "FCTC", olChoiceList);
	rlChoiceList.TabFieldName = "PFC.FCTC";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();

	GetData("PGP", olDataArr, "PGPC");
	blRet = ogBasicData.MakeChoiceList(&olDataArr, "PGP", "PGPC", olChoiceList);
	rlChoiceList.TabFieldName = "PGP.PGPC";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();
	
	GetData("POL", olDataArr, "NAME");
	blRet &= ogBasicData.MakeChoiceList(&olDataArr, "POL", "NAME", olChoiceList);
	rlChoiceList.TabFieldName = "POL.NAME";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();

	GetData("WGP", olDataArr, "WGPC");
	blRet = ogBasicData.MakeChoiceList(&olDataArr, "WGP", "WGPC", olChoiceList);
	rlChoiceList.TabFieldName = "WGP.WGPC";
	rlChoiceList.ValueList = olChoiceList;
	ogChoiceLists.New(rlChoiceList);
	olDataArr.DeleteAll();
}
//--------------------------------------------------------------------------------------------------------------------

void PoolAllocApp::SetGlobalFieldIndices()
{
	// To append a field you need to create a global variable in CCSGLOBL


	CCS_TRY
	
	// WGP (workgroups)
	igWgpPgpuIdx = ogBCD.GetFieldIndex("WGP", "PGPU");
	igWgpPrflIdx = ogBCD.GetFieldIndex("WGP", "PRFL");
	igWgpRemaIdx = ogBCD.GetFieldIndex("WGP", "REMA");
	igWgpUrnoIdx = ogBCD.GetFieldIndex("WGP", "URNO");
	igWgpWgpcIdx = ogBCD.GetFieldIndex("WGP", "WGPC");
	igWgpWgpnIdx = ogBCD.GetFieldIndex("WGP", "WGPN");
	// SGR (Static Groups)
	igSgrApplIdx = ogBCD.GetFieldIndex("SGR", "APPL");
	igSgrFldnIdx = ogBCD.GetFieldIndex("SGR", "FLDN");
	igSgrGrdsIdx = ogBCD.GetFieldIndex("SGR", "GRDS");
	igSgrGrpnIdx = ogBCD.GetFieldIndex("SGR", "GRPN");
	igSgrGrsnIdx = ogBCD.GetFieldIndex("SGR", "GRSN");
	igSgrPrflIdx = ogBCD.GetFieldIndex("SGR", "PRFL");
	igSgrTabnIdx = ogBCD.GetFieldIndex("SGR", "TABN");
	igSgrUgtyIdx = ogBCD.GetFieldIndex("SGR", "UGTY");
	igSgrUrnoIdx = ogBCD.GetFieldIndex("SGR", "URNO");
	// SGM (Group Members)
	igSgmPrflIdx = ogBCD.GetFieldIndex("SGM", "PRFL");
	igSgmTabnIdx = ogBCD.GetFieldIndex("SGM", "TABN");
	igSgmUgtyIdx = ogBCD.GetFieldIndex("SGM", "UGTY");
	igSgmUrnoIdx = ogBCD.GetFieldIndex("SGM", "URNO");
	igSgmUsgrIdx = ogBCD.GetFieldIndex("SGM", "USGR");
	igSgmUvalIdx = ogBCD.GetFieldIndex("SGM", "UVAL");
	igSgmValuIdx = ogBCD.GetFieldIndex("SGM", "VALU");
	// POL (pools)
	igPolUrnoIdx = ogBCD.GetFieldIndex("POL", "URNO");
	igPolPoolIdx = ogBCD.GetFieldIndex("POL", "POOL");
	igPolNameIdx = ogBCD.GetFieldIndex("POL", "NAME");
	// POA (pool assignment)
	igPoaUvalIdx = ogBCD.GetFieldIndex("POA", "UVAL");
	igPoaUrnoIdx = ogBCD.GetFieldIndex("POA", "URNO");
	igPoaUpolIdx = ogBCD.GetFieldIndex("POA", "UPOL");
	igPoaReftIdx = ogBCD.GetFieldIndex("POA", "REFT");
	// PGP (planning groups)
	igPgpMaxmIdx = ogBCD.GetFieldIndex("POA", "MAXM");
	igPgpMinmIdx = ogBCD.GetFieldIndex("POA", "MINM");
	igPgpPgpcIdx = ogBCD.GetFieldIndex("POA", "PGPC");
	igPgpPgpmIdx = ogBCD.GetFieldIndex("POA", "PGPM");
	igPgpPgpnIdx = ogBCD.GetFieldIndex("POA", "PGPN");
	igPgpPrflIdx = ogBCD.GetFieldIndex("POA", "PRFL");
	igPgpRemaIdx = ogBCD.GetFieldIndex("POA", "REMA");
	igPgpUrnoIdx = ogBCD.GetFieldIndex("POA", "URNO");
	// PFC (personnel functions)
	igPfcDptcIdx = ogBCD.GetFieldIndex("PFC", "DPTC");
	igPfcFctcIdx = ogBCD.GetFieldIndex("PFC", "FCTC");
	igPfcFctnIdx = ogBCD.GetFieldIndex("PFC", "FCTN");
	igPfcPrflIdx = ogBCD.GetFieldIndex("PFC", "PRFL");
	igPfcRemaIdx = ogBCD.GetFieldIndex("PFC", "REMA");
	igPfcUrnoIdx = ogBCD.GetFieldIndex("PFC", "URNO");
	// PER (permits)
	igPerPrflIdx = ogBCD.GetFieldIndex("PER", "PRFL");
	igPerPrioIdx = ogBCD.GetFieldIndex("PER", "PRIO");
	igPerPrmcIdx = ogBCD.GetFieldIndex("PER", "PRMC");
	igPerPrmnIdx = ogBCD.GetFieldIndex("PER", "PRMN");
	igPerRemaIdx = ogBCD.GetFieldIndex("PER", "REMA");
	igPerUrnoIdx = ogBCD.GetFieldIndex("PER", "URNO");
	// ORG (organisation unit)
	igOrgDpt1Idx = ogBCD.GetFieldIndex("ORG", "DPT1");
	igOrgDpt2Idx = ogBCD.GetFieldIndex("ORG", "DPT2");
	igOrgDptnIdx = ogBCD.GetFieldIndex("ORG", "DPTN");
	igOrgPrflIdx = ogBCD.GetFieldIndex("ORG", "PRFL");
	igOrgRemaIdx = ogBCD.GetFieldIndex("ORG", "REMA");
	igOrgUrnoIdx = ogBCD.GetFieldIndex("ORG", "URNO");
	// COT (contract types)
	igCotCtrcIdx = ogBCD.GetFieldIndex("COT", "CTRC");
	igCotCtrnIdx = ogBCD.GetFieldIndex("COT", "CTRN");
	igCotDptcIdx = ogBCD.GetFieldIndex("COT", "DPTC");
	igCotMislIdx = ogBCD.GetFieldIndex("COT", "MISL");
	igCotMxslIdx = ogBCD.GetFieldIndex("COT", "MXSL");
	igCotNowdIdx = ogBCD.GetFieldIndex("COT", "NOWD");
	igCotPrflIdx = ogBCD.GetFieldIndex("COT", "PRFL");
	igCotRemaIdx = ogBCD.GetFieldIndex("COT", "REMA");
	igCotSbpaIdx = ogBCD.GetFieldIndex("COT", "SBPA");
	igCotUrnoIdx = ogBCD.GetFieldIndex("COT", "URNO");
	igCotWhpwIdx = ogBCD.GetFieldIndex("COT", "WHPW");
	igCotWrkdIdx = ogBCD.GetFieldIndex("COT", "WRKD");
	// ALO (allocation)
	igAloAlocIdx = ogBCD.GetFieldIndex("ALO", "ALOC");
	igAloAlodIdx = ogBCD.GetFieldIndex("ALO", "ALOD");
	igAloAlotIdx = ogBCD.GetFieldIndex("ALO", "ALOT");
	igAloReftIdx = ogBCD.GetFieldIndex("ALO", "REFT");
	igAloUrnoIdx = ogBCD.GetFieldIndex("ALO", "URNO");
	

	CCS_CATCH_ALL
}



//---------------------------------------------------------------------------------------------------------------------
//					implementation of static functions
//---------------------------------------------------------------------------------------------------------------------
static int CompareRecords(const RecordSet **rec1, const RecordSet **rec2)
{
	// you need to set the igIdx variable before calling the sort-function !

	if (igIdx < (*rec1)->Values.GetSize())
	{
		return ((int) strcmp((*rec1)->Values[igIdx], (*rec2)->Values[igIdx]));
	}
	
	return 0;
}
//---------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------



void PoolAllocApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	AatHelp::WinHelp(dwData, nCmd);
}
