// LoginDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSCedadata.h>
#include <CCSCedacom.h>
#include <LoginDlg.h>
#include <PrivList.h>
#include <BasicData.h>
#include <aboutbox.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//----------------------------------------------------------------------------------------------------------------------
//					construction
//----------------------------------------------------------------------------------------------------------------------
CLoginDialog::CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, CWnd* pParent /*=NULL*/)
    : CDialog(CLoginDialog::IDD, pParent)
{
	//-- exception handling
	CCS_TRY

    //{{AFX_DATA_INIT(CLoginDialog)
	//}}AFX_DATA_INIT

	strcpy(pcmHomeAirport,pcpHomeAirport);
	strcpy(pcmAppl,pcpAppl);

	// default text
	INVALID_USERNAME = GetString(WRONG_USER);
	INVALID_APPLICATION = GetString(WRONG_MODUL);
	INVALID_PASSWORD = GetString(WRONG_PASSWORD);
	EXPIRED_USERNAME = GetString(USER_OUT_OF_DATE);
	EXPIRED_APPLICATION = GetString(MODUL_OUT_OF_DATE);
	EXPIRED_WORKSTATION = GetString(WKS_OUT_OF_DATE);
	DISABLED_USERNAME = GetString(USER_NOT_ACTIV);
	DISABLED_APPLICATION = GetString(MODUL_NOT_ACTIV);
	DISABLED_WORKSTATION = GetString(WKS_NOT_ACTIV);
	UNDEFINED_PROFILE = GetString(USER_WITHOUT_PROFILE);
	MESSAGE_BOX_CAPTION = GetString(LOGIN_INCORRECT);
	USERNAME_CAPTION = GetString(USER_TXT);
	PASSWORD_CAPTION = GetString(PASSWORT_TXT);
	OK_CAPTION = GetString(OK);
	CANCEL_CAPTION = GetString(CANCEL);
	WINDOW_CAPTION = GetString(LOGIN_TXT);
	INFO_CAPTION = GetString(IDS_INFO);

	//-- exception handling
	CCS_CATCH_ALL
}



//----------------------------------------------------------------------------------------------------------------------
//					data exchange, message handling
//----------------------------------------------------------------------------------------------------------------------
void CLoginDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoginDialog)
	DDX_Control(pDX, IDC_USERNAME, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);
	DDX_Control(pDX, IDC_USIDCAPTION, m_UsidCaption);
	DDX_Control(pDX, IDC_PASSCAPTION, m_PassCaption);
	DDX_Control(pDX, IDOK, m_OkCaption);
	DDX_Control(pDX, IDCANCEL, m_CancelCaption);
	//}}AFX_DATA_MAP
}
//---------------------------------------------------------------------------------------------------------------------- 

BEGIN_MESSAGE_MAP(CLoginDialog, CDialog)
    //{{AFX_MSG_MAP(CLoginDialog)
    ON_WM_PAINT()
	ON_BN_CLICKED(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------------------------------------------
//					message handling
//----------------------------------------------------------------------------------------------------------------------
void CLoginDialog::OnPaint() 
{
	//-- exception handling
	CCS_TRY

    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap(IDB_LOGIN);
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
    
    // Do not call CDialog::OnPaint() for painting messages

	//-- exception handling
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void CLoginDialog::OnOK() 
{
	//-- exception handling
	CCS_TRY

	bool blRc = true;
	CWnd *polFocusCtrl = &m_UsernameCtrl;

	// get the username
	m_UsernameCtrl.GetWindowText(omUsername);
	m_PasswordCtrl.GetWindowText(omPassword);


	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	char pclUserName[100]="";
	char pclPassword[100]="";

	strcpy(pclUserName, omUsername.GetBuffer(0));
	strcpy(pclPassword, omPassword);

	strcpy(pcgUser,omUsername.GetBuffer(0));
	strcpy(pcgPasswd,omPassword);
	ogBasicData.omUserID = omUsername.GetBuffer(0);

	ogCommHandler.SetUser(omUsername.GetBuffer(0));
	strcpy(CCSCedaData::pcmUser, omUsername.GetBuffer(0));
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);


	blRc = ogPrivList.Login(pcgTableExt,pclUserName,pclPassword,pcmAppl);

	AfxGetApp()->DoWaitCursor(-1);
	
	if( blRc )
	{
		CDialog::OnOK();
	}
	else
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			omErrTxt = INVALID_USERNAME;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			omErrTxt = INVALID_APPLICATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			omErrTxt = INVALID_PASSWORD;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			omErrTxt = EXPIRED_USERNAME;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			omErrTxt = EXPIRED_APPLICATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			omErrTxt = EXPIRED_WORKSTATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			omErrTxt = DISABLED_USERNAME;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			omErrTxt = DISABLED_APPLICATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			omErrTxt = DISABLED_WORKSTATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			omErrTxt = UNDEFINED_PROFILE;
		}
		else {
			omErrTxt = ogPrivList.omErrorMessage;
		}

		MessageBox(omErrTxt,MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
	
		imLoginCount++;

		if( imLoginCount >= MAX_LOGIN )
			OnCancel();
		else
		{
			if( polFocusCtrl != NULL )
				polFocusCtrl->SetFocus(); // set the focus to the offending control
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void CLoginDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}
//----------------------------------------------------------------------------------------------------------------------

BOOL CLoginDialog::OnInitDialog() 
{
	//-- exception handling
	CCS_TRY

	CDialog::OnInitDialog();
	
	imLoginCount = 0;
	SetWindowText(WINDOW_CAPTION);
	m_UsidCaption.SetWindowText(USERNAME_CAPTION);
	m_PassCaption.SetWindowText(PASSWORD_CAPTION);
	m_OkCaption.SetWindowText(OK_CAPTION);
	m_CancelCaption.SetWindowText(CANCEL_CAPTION);
	SetDlgItemText ( ID_APP_ABOUT, INFO_CAPTION );

	m_UsernameCtrl.SetTypeToString("X(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	m_PasswordCtrl.SetTypeToString("X(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);


	m_UsernameCtrl.SetFocus();
//	CDialog::OnInitDialog();

	CString olCaption = GetString(IDS_STRING1001);
	olCaption  += ogCommHandler.pcmRealHostName;
	olCaption  += " / ";
	olCaption  += ogCommHandler.pcmRealHostType;
	SetWindowText(olCaption  );

	int ilY = ::GetSystemMetrics(SM_CYSCREEN);
	CRect olRect, olNewRect;
	GetWindowRect(&olRect);
	int ilHeight = olRect.bottom - olRect.top;
	int ilWidth = olRect.right - olRect.left;
	olNewRect.top = (int)((int)(ilY/2) - (int)(ilHeight/2));
	olNewRect.left = 300;
	olNewRect.bottom = olNewRect.top + ilHeight;
	olNewRect.right = olNewRect.left + ilWidth;

	MoveWindow(&olNewRect);
	
	//-- exception handling
	CCS_CATCH_ALL

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//----------------------------------------------------------------------------------------------------------------------
bool CLoginDialog::Login(const char *pcpUsername, const char *pcpPassword)
{
	//-----------------------------------
	// This function will only be called from beyond this application (eg. when starting it from within another program)
	// A regular login (using the login dialog) will call OnOK() instead
	//-----------------------------------
	bool blRc = true;

	omErrTxt.Empty();

	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	

	char pclUserName[100] = "";
	char pclPassword[100] = "";

	strcpy(pclUserName, pcpUsername);
	strcpy(pclPassword, pcpPassword);

	strcpy(pcgUser,pcpUsername);
	strcpy(pcgPasswd,pcpPassword);

	blRc = ogPrivList.Login(pcgTableExt,pclUserName,pclPassword,pcmAppl);
	

	AfxGetApp()->DoWaitCursor(-1);

	if( ! blRc )
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			omErrTxt = INVALID_USERNAME;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			omErrTxt = INVALID_APPLICATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			omErrTxt = INVALID_PASSWORD;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			omErrTxt = EXPIRED_USERNAME;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			omErrTxt = EXPIRED_APPLICATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			omErrTxt = EXPIRED_WORKSTATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			omErrTxt = DISABLED_USERNAME;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			omErrTxt = DISABLED_APPLICATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			omErrTxt = DISABLED_WORKSTATION;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			omErrTxt = UNDEFINED_PROFILE;
		}
		else 
		{
			omErrTxt = ogPrivList.omErrorMessage;
		}

		MessageBox(omErrTxt,MESSAGE_BOX_CAPTION,MB_ICONINFORMATION);
	}

	return blRc;
}
//----------------------------------------------------------------------------------------------------------------------


void CLoginDialog::SetStaticTexts()
{
	CCS_TRY

	SetWindowText(GetString(1631));

	CWnd *pWnd = GetDlgItem(IDC_USIDCAPTION);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(241));

	pWnd = GetDlgItem(IDC_PASSCAPTION);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(242));

	pWnd = GetDlgItem(IDOK);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1398));

	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(244));


	CCS_CATCH_ALL
}

//----------------------------------------------------------------------------------------------------------------------


void CLoginDialog::OnAppAbout() 
{
	// TODO: Add your control notification handler code here
	CAboutDlg olDlg;

	olDlg.DoModal();
}
