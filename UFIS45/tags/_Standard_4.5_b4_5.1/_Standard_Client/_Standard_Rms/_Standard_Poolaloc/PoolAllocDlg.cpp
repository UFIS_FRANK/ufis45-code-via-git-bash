// PoolAllocDlg.cpp : implementation file
//

#include <stdafx.h>
#include <PoolAlloc.h>
#include <PoolAllocDlg.h>
#include <AboutBox.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <RecordSet.h>
#include <BasicData.h>
#include <GuiLng.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//---------------------------------------------------------------------------------------------------------------------
//					global functions, defines etc.
//---------------------------------------------------------------------------------------------------------------------
char SOH = 3;


//---------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//---------------------------------------------------------------------------------------------------------------------
PoolAllocDlg::PoolAllocDlg(CWnd* pParent /*=NULL*/)
	: CDialog(PoolAllocDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(PoolAllocDlg)
	imNoRestric = 0;
	imRadioBtn = -1;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32

	m_hIcon = AfxGetApp()->LoadIcon(IDI_UFIS);
	
	pomActiveGrid = NULL;

	bmAutoAlloc = false;
	bmRestrict = false;

	bmSorting = false;

	imCurrRow = -1;
	imCurrCol = -1;
}
//---------------------------------------------------------------------------------------------------------------------

PoolAllocDlg::~PoolAllocDlg()
{
	delete pomPoolGrid;
	delete pomRespTable;
	delete pomRespViewer;
}



//---------------------------------------------------------------------------------------------------------------------
//					data exchange, message map
//---------------------------------------------------------------------------------------------------------------------
void PoolAllocDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PoolAllocDlg)
		DDX_Radio(pDX, POLALO_OPT_DEFAULT, imRadioBtn);		
		DDX_Control(pDX, POLALO_OPT_DEFAULT, m_Opt_Default);
		DDX_Control(pDX, POLALO_OPT_ORGUNIT, m_Opt_OrgUnit);
		DDX_Control(pDX, POLALO_OPT_FUNC, m_Opt_Func);
		DDX_Control(pDX, POLALO_OPT_QUALI, m_Opt_Quali);
		DDX_Control(pDX, POLALO_OPT_WKGRP, m_Opt_WkGrp);
		DDX_Control(pDX, POLALO_OPT_PLGRP, m_Opt_PlGrp);
		DDX_Control(pDX, POLALO_OPT_CONTR, m_Opt_Contr);
		DDX_Control(pDX, POLALO_COB_POOLS, m_Cob_Pools);
		DDX_Control(pDX, IDC_ALOLIST, m_AloList);
		DDX_Control(pDX, POLALO_LB_REFITEM, m_GrpMemList);
		DDX_Check(pDX, POLALO_CHB_RESTR, imNoRestric);
		DDX_Control(pDX, POLALO_BTN_ADD, m_Btn_Add);
	//}}AFX_DATA_MAP
}
//---------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(PoolAllocDlg, CDialog)
	//{{AFX_MSG_MAP(PoolAllocDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(POLALO_BTN_ADD, OnAddResp)
	ON_BN_CLICKED(POLALO_OPT_DEFAULT, OnRadioButton)
	ON_BN_CLICKED(POLALO_OPT_ORGUNIT, OnRadioButton)
	ON_BN_CLICKED(POLALO_OPT_FUNC, OnRadioButton)
	ON_BN_CLICKED(POLALO_OPT_QUALI, OnRadioButton)
	ON_BN_CLICKED(POLALO_OPT_WKGRP, OnRadioButton)
	ON_BN_CLICKED(POLALO_OPT_CONTR, OnRadioButton)
	ON_BN_CLICKED(POLALO_OPT_PLGRP, OnRadioButton)
	ON_BN_CLICKED(POLALO_CHB_AUTO, OnNoAutoAloc)
	ON_BN_CLICKED(POLALO_CHB_RESTR, OnNoRestrict)
	ON_CBN_SELCHANGE(POLALO_COB_POOLS, OnSelchangePools)
	ON_CBN_SELCHANGE(IDC_ALOLIST, OnSelChangeAlo)
	ON_CBN_DROPDOWN(POLALO_COB_POOLS, OnDropdownPools)
	ON_MESSAGE(GRID_MESSAGE_STARTEDITING, OnStartEditing)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING, OnEndEditing)
	ON_MESSAGE(GRID_MESSAGE_RCELLCLICK, OnGridRClick)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnRespRBtnDown)
	ON_COMMAND(CM_DELETE, OnDelete)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//---------------------------------------------------------------------------------------------------------------------
//					message handlers
//---------------------------------------------------------------------------------------------------------------------
BOOL PoolAllocDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	imSgmValuLength = ogBCD.GetMaxFieldLength("SGM", "VALU" );
	SetStaticTexts();
	
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		//strAboutMenu.LoadString(IDS_ABOUTBOX);
		strAboutMenu = GetString(101);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	

	
	// set automatic allocation to true
	bmInit = true;
	CButton *pWnd = (CButton*) GetDlgItem(POLALO_CHB_AUTO);
	if (pWnd != NULL)
	{
		pWnd->SetCheck(UNCHECKED);
		bmAutoAlloc = true;
	}

	// set restriction to true
	pWnd = (CButton*) GetDlgItem(POLALO_CHB_RESTR);
	if (pWnd != NULL)
	{
		pWnd->SetCheck(UNCHECKED);
		bmRestrict = true;
	}
	bmInit = false;


	// set default radio button
	bmReset = false;
	int ilPoaCount = ogBCD.GetDataCount("POA");
	if (ilPoaCount > 0)
	{
		CString olReft;
		olReft = ogBCD.GetField("POA", 0, "REFT");
		if (olReft.GetLength() >= 8)
		{
			CString olRefTab;
			olRefTab = olReft.Left(3);

			if (olRefTab == "PFC")
			{
				m_Opt_Func.SetCheck(CHECKED);
			}
			else if (olRefTab == "PER")
			{
				m_Opt_Quali.SetCheck(CHECKED);
			}
			else if (olRefTab == "ORG")
			{
				m_Opt_OrgUnit.SetCheck(CHECKED);
			}
			else if (olRefTab == "COT")
			{
				m_Opt_Contr.SetCheck(CHECKED);
			}
			else if (olRefTab == "WGP")
			{
				m_Opt_WkGrp.SetCheck(CHECKED);
			}
			else if (olRefTab == "PGP")
			{
				m_Opt_PlGrp.SetCheck(CHECKED);
			}
			else
			{
				m_Opt_Default.SetCheck(CHECKED);
			}
			UpdateData();
		}
	}
	else
	{
		m_Opt_Default.SetCheck(CHECKED);
	}

	// fill pools combo
	CString olText;
	long llUrno;
	int ilIdx;
	int ilCount = ogBCD.GetDataCount("POL");
	for (int i = 0; i < ilCount; i++)
	{
		olText = ogBCD.GetField("POL", i, "NAME");
		llUrno = atol(ogBCD.GetField("POL", i, "URNO"));
		if (llUrno > -1)
		{
			ilIdx = m_Cob_Pools.AddString(olText);
			m_Cob_Pools.SetItemData(ilIdx, llUrno);
		}
	}
	
	m_Cob_Pools.SetCurSel(0);
	
	omAloPoolUrno = ogBCD.GetField("ALO", "ALOC", "POOL", "URNO");
	if (omAloPoolUrno.IsEmpty() || omAloPoolUrno == CString(" "))
	{
		// in case there's no record with ALOC="POOL" in ALOTAB exit the program now
		 MessageBox(GetString(1487),GetString(AFX_IDS_APP_TITLE),MB_ICONERROR | MB_OK);	
		 OnClose();
	}
	
	

	// fill list boxes
	FillAloList();
	m_AloList.SetCurSel(0);
	FillGrpMemList(GetSelectedAloUrno());
	
	// initialization of tables
	InitializeTables();
	FillRespTable();
	FillPoolGrid(CString(""), CString(""));

	bmIsChanged = false;
	bmRespChange = false;

	
	return TRUE;  // return TRUE  unless you set the focus to a control
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnOK()
{
	CCS_TRY
	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		
//--- get pools combo data 
	CString olPolUrno, olPolName;
	int ilSel = m_Cob_Pools.GetCurSel();
	if (ilSel != -1)
	{
		olPolUrno.Format("%d", m_Cob_Pools.GetItemData(ilSel));
		m_Cob_Pools.GetLBText(ilSel, olPolName);
	}
	else
	{
		Beep(800, 200);
		return;
	}


//--- POOL grid
	CString olValue, olReft, olUrno, olUval;
	int ilCount = pomPoolGrid->GetRowCount();
	if (ilCount > 0)
	{
		// delete all existing records for this pool
		CCSPtrArray <RecordSet> olTmp;
		ogBCD.GetRecords("POA", "UPOL", olPolUrno, &olTmp);
		int ilDBCount = olTmp.GetSize();
		
		CString olUrno;
		for (int j = ilDBCount - 1; j >= 0; j--)
		{
			olUrno = olTmp[j].Values[igPoaUrnoIdx];
			ogBCD.DeleteRecord("POA", "URNO",  olUrno);
		}
		ogBCD.Save("POA");
		olTmp.DeleteAll();

		// create new records
		RecordSet olPoa(ogBCD.GetFieldCount("POA"));
		olPoa.Values[igPoaUpolIdx] = olPolUrno;

		for (int i = 1; i < ilCount; i++)
		{
			olValue = pomPoolGrid->GetValueRowCol(i, 1);

			if (olValue.IsEmpty() == FALSE)
			{
				olUval = ogBasicData.GetRefUrno(omPoaReft.Left(3), olValue);
				//  Check whether criterion is already used for an other pool
				if ( ogBCD.GetField("POA", "UVAL", olUval, "UPOL", olUrno) )
				{
					CString olCriterion, olPool, olText, olForm;
					olPool = ogBCD.GetField("POL", "URNO", olUrno, "NAME" );
					olForm = GetString ( IDS_DUPLICATE_VALUE );
					// The %s <%s> is already mapped to pool <%s>. It cannot be saved for the current pool <%s>. 
					olCriterion = pomPoolGrid->GetValueRowCol(0, 1);
					olText.Format ( olForm, olCriterion, olValue, olPool, olPolName );
					MessageBox ( olText );
				}
				else
				{
					olPoa.Values[igPoaReftIdx] = omPoaReft;
					olPoa.Values[igPoaUvalIdx] = olUval;
					olPoa.Values[igPoaUrnoIdx] = CString("");

					ogBCD.InsertRecord("POA", olPoa);
				}
			}
		}

		ogBCD.Save("POA");
		FillPoolGrid("", "");

	}

//--- RESP table
	RecordSet olSgr(ogBCD.GetFieldCount("SGR"));

	// make sure, group with that name exists in SGRTAB
	// and - in case group name exists more than once, we get the right one. 
	CString olSgrUrno;
	olSgrUrno = GetSelSgrUrno();

	if (ogBCD.GetRecord("SGR", "URNO", olSgrUrno, olSgr) == false)
	{
		bool blTest = ogBCD.GetRecord("SGR_TMP", "URNO", olSgrUrno, olSgr);
		blTest = ogBCD.InsertRecord("SGR", olSgr);
		blTest = ogBCD.Save("SGR");
	}

	// delete sgm entries for this sgr
	CCSPtrArray <RecordSet> olTmpArr;
	ogBCD.GetRecords("SGM", "USGR", olSgr.Values[igSgrUrnoIdx], &olTmpArr);
	for (int j = olTmpArr.GetSize() - 1; j >= 0; j--)
	{
		ogBCD.DeleteRecord("SGM", "URNO", olTmpArr[j].Values[igSgmUrnoIdx]);
	}
	olTmpArr.DeleteAll();
	ogBCD.Save("SGM");

	//  re-insert sgm entries for this sgr
	CString key, olVal;
	key = GetSelSgrUrno();
	ogRestrictions.Lookup(key, olVal);
	if (atoi(olVal))
	{
		ogBCD.GetRecords("SGM_TMP", "USGR", olSgr.Values[igSgrUrnoIdx], &olTmpArr);
		for (j = 0; j < olTmpArr.GetSize(); j++)
		{
			ogBCD.InsertRecord("SGM", olTmpArr[j]);
		}
		olTmpArr.DeleteAll();
	}
		
	ogBCD.Save("SGM");

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	bmIsChanged = false;
	bmRespChange = false;

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnSelChangeAlo()
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	FillGrpMemList(GetSelectedAloUrno());	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnAddResp()
{
	int ilGrpSel = m_GrpMemList.GetCurSel();
	int ilAloSel = m_AloList.GetCurSel();
	int ilPolSel = m_Cob_Pools.GetCurSel();

	if ((ilGrpSel != LB_ERR) && (ilAloSel != LB_ERR) && (ilPolSel != CB_ERR))
	{
		CString olAlod, olReft;
		m_AloList.GetText(ilAloSel, olAlod);
		
		// check if this ALO.ALOD is already in SGM_TMP list (SGM_TMP.VALU)
		RecordSet olAlo(ogBCD.GetFieldCount("ALO"));
		ogBCD.GetRecord("ALO", "ALOD", olAlod, olAlo);
		
		olReft = olAlo.Values[igAloReftIdx];
		if (olReft.IsEmpty() == TRUE || olReft == CString(" "))
		{
			olReft = "SGR.GRPN"; // ALO.REFT -> SGM.TABN
		}

		CString olGrpMemUrno, olGrpMemText;
		GetSelectedGrpMem(olReft, olGrpMemUrno, olGrpMemText);
			
		// create sgm entry
		CString olSgrUrno;
		olSgrUrno = GetSelSgrUrno();
		CString olRefText, olRefUrno;
		m_GrpMemList.GetText(ilGrpSel, olRefText);
		olRefUrno = ogBCD.GetField(olReft.Left(3), olReft.Right(4), olRefText, "URNO");

		if (!IsAlreadyAssigned(olRefUrno, olSgrUrno))
		{
			ogRestrictions.SetAt(olSgrUrno, "1"); // mne 000512

			RecordSet olSgm(ogBCD.GetFieldCount("SGM_TMP"));
			olSgm.Values[igSgmUrnoIdx].Format("%d", ogBCD.GetNextUrno());
			olSgm.Values[igSgmTabnIdx] = olReft;
			olSgm.Values[igSgmUgtyIdx] = olAlo.Values[igAloUrnoIdx];
			olSgm.Values[igSgmUsgrIdx] = olSgrUrno;
			olSgm.Values[igSgmUvalIdx] = olRefUrno;
			
			CString olValu = olRefText;
			if ( olValu.GetLength() > imSgmValuLength )
			{
				olValu = olValu.Left ( imSgmValuLength-1 );
				olValu += '~';
			}
			olSgm.Values[igSgmValuIdx] = olValu;

			bool blTest = ogBCD.InsertRecord("SGM_TMP", olSgm);
			pomRespViewer->ChangeViewTo(olSgrUrno);

			bmRespChange = true;
		}
		else
		{
			Beep(800, 200);
		}
	}
	else
	{
		Beep(800, 150);
		MessageBox(GetString(316), GetString(57344), MB_OK);
	}
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnDropdownPools()
{
	imCurPool = m_Cob_Pools.GetCurSel();
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnSelchangePools()
{
	if (bmIsChanged || bmRespChange)
	{
		if (MessageBox(GetString(1486), GetString(57344), MB_YESNO) == IDNO)
		{
			if (imCurPool >= 0 && imCurPool < m_Cob_Pools.GetCount())
			{
				m_Cob_Pools.SetCurSel(imCurPool);
			}
			else
			{
				m_Cob_Pools.SetCurSel(0);
			}
			
			return;
		}
		else
		{
			// re-initialize SGM_TMP
			CCSPtrArray <RecordSet> olTmpArr;
			RecordSet olSgr(ogBCD.GetFieldCount("SGR"));
			CString olSgrUrno;

			ClearSgmTmp();
			
			int ilCount = ogBCD.GetDataCount("SGR");
			for (int i = 0; i < ilCount; i++)
			{
				ogBCD.GetRecord("SGR", i, olSgr);	
				ogBCD.GetRecords("SGM", "USGR", olSgr.Values[igSgrUrnoIdx], &olTmpArr);
				for (int j = 0; j < olTmpArr.GetSize(); j++)
				{
					ogBCD.InsertRecord("SGM_TMP", olTmpArr[j]);
				}
			}
			
			olTmpArr.DeleteAll();
		}
	}

	bmIsChanged = false;
	bmRespChange = false;

	FillPoolGrid(CString(""), CString(""));
	FillRespTable();
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::ClearSgmTmp()
{
	RecordSet olSgm(ogBCD.GetFieldCount("SGM_TMP"));
	int ilCount = ogBCD.GetDataCount("SGM_TMP");

	for (int i = ilCount; i >= 0; i--)
	{
		ogBCD.GetRecord("SGM_TMP", i, olSgm);	
		ogBCD.DeleteRecord("SGM_TMP", "URNO", olSgm.Values[igSgmUrnoIdx]);
	}
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnDelete()
{
	if (pomActiveGrid != NULL)
	{
		if (pomActiveGrid->GetRowCount() > 1)
		{
			pomActiveGrid->RemoveRows(imCurrRow, imCurrRow);
		}
	}
	else
	{
		RESPTABLE_LINEDATA *prlLine = pomRespViewer->GetLine(imCurrRow);
		if (prlLine != NULL)
		{
			ogBCD.DeleteRecord("SGM_TMP", "URNO", prlLine->SgmUrno);
			pomRespViewer->ChangeViewTo(GetSelSgrUrno());
		}

		if (ogBCD.GetDataCount("SGM_TMP") == 0)
		{
			ogRestrictions.SetAt(GetSelSgrUrno(), "0");
		}
	}

	bmRespChange = true;
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnGridRClick(WPARAM wParam, LPARAM lParam) 
{
	CCS_TRY


	imCurrRow = (int)((CELLPOS*)lParam)->Row;
	imCurrCol = (int)((CELLPOS*)lParam)->Col;
	int x = (int)((CELLPOS*)lParam)->x;
	int y = (int)((CELLPOS*)lParam)->y;

	pomActiveGrid = (CGridFenster *) wParam;

	CString olText;
	olText = pomActiveGrid->GetValueRowCol(imCurrRow, imCurrCol);
	if (olText.IsEmpty() == FALSE)
	{
		pomActiveGrid->SelectRange(CGXRange(imCurrRow, 0, imCurrRow, pomActiveGrid->GetColCount()), TRUE);
		MakeBarMenu(imCurrRow, CPoint(x, y));
	}
	
		
	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnRespRBtnDown(WPARAM wParam, LPARAM lParam)
{
	int itemID = (int) wParam;
	int ilX = LOWORD(lParam);
	int ilY = HIWORD(lParam);

	if (itemID >= 0)
	{
		pomRespTable->GetCTableListBox()->SetCurSel(itemID);
		pomActiveGrid = NULL;
		imCurrRow = itemID;
		MakeRespMenu(itemID, CPoint(ilX, ilY));
	}
}
//----------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnStartEditing(WPARAM wParam, LPARAM lParam)
{
	bmIsChanged = true;
}
//----------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	if (bmSorting == true)
	{
		// don't run through this function after sorting 
		// (causes infinite loop)
		bmSorting = false;
		return;
	}

	CGridFenster *polGrid = (CGridFenster*) wParam;

	CELLPOS *prlCellPos = (CELLPOS*) lParam;

	int ilCol = (int) prlCellPos->Col;
	int ilRow = (int) prlCellPos->Row;
	
	CString olText, olUrno, olReft;
	
//--- POOLS grid
	if (polGrid == pomPoolGrid)
	{
		if (ilRow > 0)
		{
			olReft = omPoaReft;
			olText = pomPoolGrid->GetValueRowCol(ilRow, 1);

			if (olText.IsEmpty() == FALSE)
			{
				IsItemAlreadyUsed(pomPoolGrid, olText, 1, ilRow);
			}

			if (ilCol == 1)
			{
				pomPoolGrid->DeleteEmptyRow(ilRow, ilCol);
			}
		}
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

LONG PoolAllocDlg::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ogBcHandle.GetBc(wParam);
	return TRUE;
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnCancel() 
{
	if (bmIsChanged || bmRespChange)
	{
		if (MessageBox(GetString(1486), GetString(57344), MB_YESNO) == IDNO)
		{
			return;
		}
	}
	
	ogCommHandler.CleanUpCom();
	
	CDialog::OnCancel();
}
//---------------------------------------------------------------------------------------------------------------------

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
void PoolAllocDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}
//---------------------------------------------------------------------------------------------------------------------

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR PoolAllocDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnRadioButton()
{
	CCS_TRY

	int ilOldBtnState = imRadioBtn;
	UpdateData();

	if (ilOldBtnState != imRadioBtn)
	{
		int ilNewBtnState = imRadioBtn;
		int ilRet = IDYES;

		// ask if changes should be lost
		ilRet = MessageBox(GetString(1363), GetString(1364), MB_YESNO|MB_DEFBUTTON2);
		
		if (ilRet == IDYES)
		{
			DeletePoaEntries();
			m_Cob_Pools.SetCurSel(0);
			SetTableHeader(imRadioBtn);		
		}
		else
		{
			imRadioBtn = ilOldBtnState;
			UpdateData(FALSE);

			switch (imRadioBtn)
			{
			case 0:
				m_Opt_Default.SetFocus();
				break;

			case 1:
				m_Opt_OrgUnit.SetFocus();
				break;

			case 2:
				m_Opt_Func.SetFocus();
				break;

			case 3:
				m_Opt_Quali.SetFocus();
				break;

			case 4:
				m_Opt_Contr.SetFocus();
				break;

			case 5:
				m_Opt_WkGrp.SetFocus();
				break;

			case 6:
				m_Opt_PlGrp.SetFocus();
				break;

			default: 
				break;
			}
		}
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::SetTableHeader(int ipBtn)
{
	CString olTitle;
	CString olState;

	switch (ipBtn)
	{
		case 0:
			m_Opt_Default.GetWindowText(olTitle);
			olState = "POL";
			break;

		case 1:
			m_Opt_OrgUnit.GetWindowText(olTitle);
			olState = "ORG";
			break;

		case 2:
			m_Opt_Func.GetWindowText(olTitle);
			olState = "PFC";
			break;

		case 3:
			m_Opt_Quali.GetWindowText(olTitle);
			olState = "PER";
			break;

		case 4:
			m_Opt_Contr.GetWindowText(olTitle);
			olState = "COT";
			break;

		case 5:
			m_Opt_WkGrp.GetWindowText(olTitle);
			olState = "WGP";
			break;

		case 6:
			m_Opt_PlGrp.GetWindowText(olTitle);
			olState = "PGP";
			break;

		default:
			break;
	}

	FillPoolGrid(olState, olTitle);
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnNoAutoAloc()
{
	// I use bmInit to prevent infinite looping 
	// Otherwise SetCheck() would trigger this function again...


	if (bmInit == false)
	{
		if (bmAutoAlloc == true)
		{
			if(MessageBox(GetString(1357), GetString(1362), MB_YESNO | MB_ICONQUESTION) == IDYES)
			{
				pomPoolGrid->ResetContent();
				DeletePoaEntries();
				bmAutoAlloc = false;
			}
			else
			{
				bmInit = true;
				CButton *polWnd = (CButton*)GetDlgItem(POLALO_CHB_AUTO);
				polWnd->SetCheck(UNCHECKED);
				bmAutoAlloc = true;
				bmInit = false;
			}
		}
		else
		{
			bmInit = true;
			CButton *polWnd = (CButton*)GetDlgItem(POLALO_CHB_AUTO);
			polWnd->SetCheck(UNCHECKED);
			bmAutoAlloc = true;
			
			imRadioBtn = 0;
			UpdateData(FALSE);
			m_Opt_Default.SetFocus();

			bmInit = false;
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::OnNoRestrict()
{
	UpdateData();

	m_AloList.EnableWindow(!imNoRestric);
	m_GrpMemList.EnableWindow(!imNoRestric);
	m_Btn_Add.EnableWindow(!imNoRestric);
	
	CString olSgrUrno;
	olSgrUrno = GetSelSgrUrno();

	if (!imNoRestric)
	{
		ogRestrictions.SetAt(olSgrUrno, "1");
		pomRespViewer->ChangeViewTo(GetSelSgrUrno());
	}
	else
	{
		ogRestrictions.SetAt(olSgrUrno, "0");
		pomRespTable->ResetContent();
	}
}



//---------------------------------------------------------------------------------------------------------------------
//					helper functions
//---------------------------------------------------------------------------------------------------------------------

// one-time initialization stuff for grid and table
void PoolAllocDlg::InitializeTables()
{
	CCS_TRY
	
	int ilLeftColWidth = 15;


//--- first grid
	pomPoolGrid = new CGridFenster(this);
	pomPoolGrid->SubclassDlgItem(POLALO_GRID1, this);
	pomPoolGrid->Initialize();	
	
	pomPoolGrid->GetParam()->SetNumberedColHeaders(FALSE);
	pomPoolGrid->GetParam()->SetNumberedRowHeaders(FALSE);
	pomPoolGrid->GetParam()->EnableTrackRowHeight(FALSE);
	pomPoolGrid->GetParam()->EnableTrackColWidth(FALSE);

	pomPoolGrid->SetRowCount(1);
	pomPoolGrid->SetColCount(3);
	pomPoolGrid->SetValueRange(CGXRange(0, 2), "REFT");
	pomPoolGrid->SetValueRange(CGXRange(0, 2), "SORT_ORDER");

	CRect olRect;
	pomPoolGrid->GetClientRect(&olRect);
	pomPoolGrid->SetColWidth(0, 0, ilLeftColWidth);
	pomPoolGrid->SetColWidth(1, 1, ((int)olRect.Width()) - ilLeftColWidth - 20);
	if (bgDebug == false)
	{
		pomPoolGrid->HideCols(2, 3);
	}

	pomPoolGrid->SetSortingEnabled(false);
	pomPoolGrid->SetAutoGrow(true, true);
	


//--- make ALO list and viewer
	pomRespTable = new Table;
	pomRespViewer = new RespTabViewer;
	pomRespTable->imSelectMode = 0;

	// calculate rect for table
	CWnd *pWnd = GetDlgItem(POLALO_FRA_RESP);
	if (::IsWindow(pWnd->GetSafeHwnd()))
	{
		pWnd->GetWindowRect(olRect);
		ScreenToClient(olRect);
	}
	
	olRect.right -= 20;
	olRect.left += 200;
	olRect.top += 50;
	olRect.bottom -= 20;
	

	pomRespViewer->Attach(pomRespTable);
	
	pomRespTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);
	pomRespViewer->ChangeViewTo(CString(""));
	

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::DeletePoaEntries()
{
	RecordSet olRecord(ogBCD.GetFieldCount("POA"));
	int ilCount = ogBCD.GetDataCount("POA");

	for (int i = ilCount; i >= 0; i--)
	{
		ogBCD.GetRecord("POA", i, olRecord);
		ogBCD.DeleteRecord("POA", "URNO", olRecord.Values[igPoaUrnoIdx], false);
	}
	
	ogBCD.Save("POA");	
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::FillPoolGrid(CString opObject, CString opTitle)
{
	CCS_TRY
	

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		

	pomPoolGrid->ResetContent();

	CString olObject, olTitle;
	CString olPolUrno, olValue;
	CString olReft, olRefUrno, olRefTab, olRefField;
	CCSPtrArray <RecordSet> olPoaArr;
	RecordSet olRefRecord;
	

	// make sure object and title are filled
	CString olTmp;
	if (opObject.IsEmpty() == TRUE)
	{
		FindObjectAndTitle(olObject, olTmp);
	}
	else
	{
		olObject = opObject;
	}

	if (opTitle.IsEmpty() == TRUE)
	{
		olTitle = olTmp;
	}
	else
	{
		olTitle = opTitle;
	}


	int ilIdx = -1;
	if ((ilIdx = m_Cob_Pools.GetCurSel()) != CB_ERR)
	{
		olPolUrno.Format("%d", m_Cob_Pools.GetItemData(ilIdx));
		ogBCD.GetRecords("POA", "UPOL", olPolUrno, &olPoaArr);
		
		int ilCount = olPoaArr.GetSize();
		if (ilCount > 0)
		{
			pomPoolGrid->InsertRows(1, ilCount);

			for (int i = 0; i < ilCount; i++)
			{
				olReft = olPoaArr[i].Values[igPoaReftIdx];
				olRefUrno = olPoaArr[i].Values[igPoaUvalIdx];
				if (olReft.GetLength() >= 8)
				{
					olRefTab = olReft.Left(3);
					olRefField = olReft.Right(4);
					int ilValIndex = ogBCD.GetFieldIndex(olRefTab, olRefField);
					bool blRet = ogBCD.GetRecord(olRefTab, "URNO", olRefUrno, olRefRecord);
					if (blRet == true)
					{
						olValue = olRefRecord.Values[ilValIndex];
					}
					
					pomPoolGrid->SetValueRange(CGXRange(i + 1, 1), olValue);
					pomPoolGrid->SetValueRange(CGXRange(i + 1, 2), olReft);
				}
			}
		}
		olPoaArr.DeleteAll();
		
		olTitle.Replace(CString("&"), CString(""));
		pomPoolGrid->SetValueRange(CGXRange(0, 1), olTitle);
		int ilRowCount = pomPoolGrid->GetRowCount();
		SortPoolGrid();
		
		// pomPoolGrid->InsertRows(max(ilRowCount, 1), 1);
		pomPoolGrid->InsertRows(ilRowCount + 1, 1);
		ilRowCount = pomPoolGrid->GetRowCount();
		ilCount = ogChoiceLists.GetSize();
		for (int i = 0; i < ilCount ; i++)
		{
			if (ogChoiceLists[i].TabFieldName.Left(3) == olObject)
			{
				if (ogChoiceLists[i].ValueList.IsEmpty() == FALSE)
				{
					pomPoolGrid->SetComboBox(ogChoiceLists[i].ValueList, true, true, 1, 1, ilRowCount, 1);
				}

				pomPoolGrid->SetValueRange(CGXRange(ilRowCount, 2), ogChoiceLists[i].TabFieldName);	// set reft
				omPoaReft = ogChoiceLists[i].TabFieldName;
				i = ilCount;	// stop looping
			}
		}
	}
	else
	{
		MessageBox(GetString(300), GetString(301), MB_OK);
	}
	
	pomPoolGrid->Redraw();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));	

	
	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::FillAloList()
{
	CString olChoiceList;
	int ilCount = ogChoiceLists.GetSize();
	for (int i = 0; i < ilCount ; i++)
	{
		if (ogChoiceLists[i].TabFieldName.Left(3) == CString("ALO"))
		{
			if (ogChoiceLists[i].ValueList.IsEmpty() == FALSE)
			{
				olChoiceList = ogChoiceLists[i].ValueList;
			}
			i = ilCount; // stop looping 
		}
	}
		
	CStringArray olListArr;
	ExtractItemList(olChoiceList, &olListArr, '\n');
	for (i = 0; i < olListArr.GetSize();i++)
	{
		m_AloList.AddString(olListArr.GetAt(i));
	}
}
//---------------------------------------------------------------------------------------------------------------------

// This function fills the control POLALO_LB_REFITEM (list box on the lower left)
void PoolAllocDlg::FillGrpMemList(CString opAloUrno)
{
	CCS_TRY

	
	CGXStyle olStyle;
	CString olRefTab, olRefField;
	CString olAloAloc, olAloReft, olAloUrno;
	CString olCurChoiceList;
	CCSPtrArray <RecordSet> olDataArr;
	RecordSet olAlo(ogBCD.GetFieldCount("ALO"));
		
	ogBCD.GetRecord("ALO", "URNO", opAloUrno, olAlo);
	olAloAloc = olAlo.Values[igAloAlocIdx];
	olAloReft = olAlo.Values[igAloReftIdx];
	olAloUrno = olAlo.Values[igAloUrnoIdx];

	m_GrpMemList.ResetContent();

	// add empty line and fill with combo
	if (olAloAloc.IsEmpty() == FALSE)
	{
		
		CString olChoiceList;
		if (olAloReft.GetLength() >= 8)
		{
			// If AloReft is filled, we get our data from the table specified in ALO.REFT.
			// If we don't find the corresponding choice list in memory
			// (i.e. it does not exist yet), we create it and append it to ogChoiceLists
			if (GetChoiceList(&olAlo, olChoiceList) == false)
			{
				CHOICE_LIST rlChoiceList;
				olRefTab = olAloReft.Left(3);
				olRefField = olAloReft.Right(4);
				theApp.GetData(olRefTab, olDataArr, olRefField);
				bool blRet = ogBasicData.MakeChoiceList(&olDataArr, olRefTab, olRefField, olChoiceList);
				rlChoiceList.TabFieldName = olAloReft;
				rlChoiceList.ValueList = olChoiceList;
				ogChoiceLists.New(rlChoiceList);
				olDataArr.DeleteAll();
			}
		}
		else
		{
			// if ALO.REFT is empty, we get our data from SGR
			olAloReft = "SGR.GRPN";
			ogBCD.GetRecords("SGR", "UGTY", olAloUrno, &olDataArr);
			ogBasicData.MakeChoiceList(&olDataArr, "SGR", "GRPN", olChoiceList);
		}

		if (olChoiceList.IsEmpty() == FALSE)
		{
			CStringArray olListArr;
			ExtractItemList(olChoiceList, &olListArr, '\n');

			for (int i = 0; i < olListArr.GetSize();i++)
			{
				// HIER MUSS NOCH WAS GETAN WERDEN:
				// Die bereits in SGM enthaltenen Eintr�ge soillen nicht mehr 
				// in der Combobox zur Auswahl stehen !!!
				// if (!IsUsedAlready(olListArr.GetAt(i)))

				m_GrpMemList.AddString(olListArr.GetAt(i));
			}
		}

		olDataArr.DeleteAll();
	}


	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::FillRespTable()
{
	CCS_TRY

	
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	CString key, olValue;
	key = GetSelSgrUrno();
	ogRestrictions.Lookup(key, olValue);

	RecordSet olSgr(ogBCD.GetFieldCount("SGR"));
	int ilSgmCount = 0;
	int ilSel = m_Cob_Pools.GetCurSel();
	if (ilSel != -1)
	{
		// create sgr entry
		// there might be more entries with the same name, so we have to find that one, 
		// that has the correct ALO.URNO (omAloPoolUrno) in SGR.UGTY
		CString olPolName;
		CString olUrno;

		olUrno = GetSelSgrUrno();
		m_Cob_Pools.GetLBText(ilSel, olPolName);

		if (ogBCD.GetRecord("SGR_TMP", "URNO", olUrno, olSgr) == false)
		{
			olSgr.Values[igSgrUrnoIdx].Format("%d", ogBCD.GetNextUrno());
			olSgr.Values[igSgrUgtyIdx] = omAloPoolUrno;
			olSgr.Values[igSgrApplIdx] = "OPSS-PM";
			olSgr.Values[igSgrGrpnIdx] = olPolName;
			olSgr.Values[igSgrGrdsIdx] = olPolName;
			bool blTest = ogBCD.InsertRecord("SGR_TMP", olSgr);
		}
	}
	
	// mne test
	// CString test;
	// test = olSgr.Values[igSgrUrnoIdx];
	// end test

	pomRespViewer->ChangeViewTo(olSgr.Values[igSgrUrnoIdx]);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));


	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

CString PoolAllocDlg::GetSelectedAloUrno()
{
	CString olRet;

	int ilSel = m_AloList.GetCurSel();

	if (ilSel != LB_ERR)
	{
		CString olAlod;
		m_AloList.GetText(ilSel, olAlod);
		olRet = ogBCD.GetField("ALO", "ALOD", olAlod, "URNO");
	}

	return olRet;
}
//----------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::GetSelectedGrpMem(CString opReft, CString &opGrpMemUrno, CString &opGrpMemText)
{
	int ilSel = m_GrpMemList.GetCurSel();

	if (ilSel != LB_ERR && opReft.GetLength() == 8)
	{
		m_GrpMemList.GetText(ilSel, opGrpMemText);
		opGrpMemUrno = ogBCD.GetField(opReft.Left(3), opReft.Right(4), opGrpMemText, "URNO");
	}
}
//----------------------------------------------------------------------------------------------------------------------

CString PoolAllocDlg::GetSelSgrUrno()
{
	CString olRet;

	int ilSel = m_Cob_Pools.GetCurSel();
	if (ilSel != CB_ERR)
	{
		CString olSelText;
		m_Cob_Pools.GetLBText(ilSel, olSelText);
		olRet = ogBCD.GetFieldExt("SGR_TMP", "GRPN", "UGTY", olSelText, omAloPoolUrno, "URNO");
	}

	return olRet;
}
//----------------------------------------------------------------------------------------------------------------------

bool PoolAllocDlg::GetChoiceList(RecordSet *polAlo, CString &ropChoiceList)
{
	bool blRet = false;

	CString olAloReft, olAloUrno, olAloAloc;

	olAloAloc = polAlo->Values[igAloAlocIdx];
	olAloReft = polAlo->Values[igAloReftIdx];
	olAloUrno = polAlo->Values[igAloUrnoIdx];

	for (int i = 0; i < ogChoiceLists.GetSize(); i++)
	{
		if (ogChoiceLists[i].TabFieldName == olAloReft)
		{
			ropChoiceList = ogChoiceLists[i].ValueList;
			blRet = true;
			break;
		}
	}

	return blRet;
}
//----------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::SortPoolGrid()
{
	bmSorting = true;

	CString olText;
	
	int ilRowCount = (int)pomPoolGrid->GetRowCount();

	if (ilRowCount > 1)
	{
		for (int i = 1; i <= ilRowCount; i++)
		{
			olText = pomPoolGrid->GetValueRowCol(i, 1);
			
			if (olText.IsEmpty() == TRUE)
			{
				pomPoolGrid->SetValueRange(CGXRange(i, 3), "999");
			}
			else
			{
				pomPoolGrid->SetValueRange(CGXRange(i, 3), "0");
			}
		}
		
		CGXSortInfoArray rlSortInfo;
		rlSortInfo.SetSize(3);       // 3 keys
		
		rlSortInfo[0].nRC = 3;	// first sort by prio
		rlSortInfo[0].bCase = TRUE; // not case sensitive
		rlSortInfo[0].sortType = CGXSortInfo::numeric;
		rlSortInfo[0].sortOrder = CGXSortInfo::ascending;
		
		rlSortInfo[1].nRC = 1;	// second sort by text
		rlSortInfo[1].bCase = TRUE; // not case sensitive
		rlSortInfo[1].sortType = CGXSortInfo::alphanumeric;
		rlSortInfo[1].sortOrder = CGXSortInfo::ascending;

		pomPoolGrid->SortRows(CGXRange().SetTable(), rlSortInfo);
	}
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::MakeBarMenu(int itemID, CPoint point) 
{
	CCS_TRY
	

	if ((itemID != -1) && (pomActiveGrid != NULL))
	{
		// create menu
		CMenu menu;
		menu.CreatePopupMenu();
		
		// create menu items 
		menu.AppendMenu(MF_STRING, CM_DELETE, GetString(131));					
			
		// show menu 
		pomActiveGrid->ClientToScreen(&point);
		point.x += 10;
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	}

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::MakeRespMenu(int itemID, CPoint point) 
{
	CCS_TRY
	
	
	// create menu
	CMenu menu;
	menu.CreatePopupMenu();
	
	// create menu items 
	menu.AppendMenu(MF_STRING, CM_DELETE, GetString(131));					
		
	// show menu 
	pomRespTable->GetCTableListBox()->ClientToScreen(&point);
	point.x += 10;
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);

	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------------------------------------------------

bool PoolAllocDlg::IsItemAlreadyUsed(CGridFenster *popGrid, CString opText, int ipCol, int ipCurRow)
{
	bool blRet = false;
	
	CCS_TRY

	
	CString olCompText;
	int ilRowCount = popGrid->GetRowCount();

	for (int i = 1; i < ilRowCount; i++)
	{
		if (i != ipCurRow)
		{
			olCompText = popGrid->GetValueRowCol(i, ipCol);
			if (olCompText.IsEmpty() == FALSE)
			{
				if (olCompText == opText)
				{
					Beep(800, 200);
					popGrid->SetValueRange(CGXRange(ipCurRow, ipCol), CString(""));
					popGrid->DeleteEmptyRow(ipCurRow, ipCol);
					blRet = true;
					i = ilRowCount + 1;	// stop looping
				}
			}
		}
	}	


	CCS_CATCH_ALL

	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

// check if this Urno  is already contained in SGM_TMP list (SGM_TMP.VALU)
bool PoolAllocDlg::IsAlreadyAssigned(CString opUval, CString opSgrUrno)
{
	bool blRet = false;
	CCS_TRY

	CCSPtrArray <RecordSet> olTmpArr;
	ogBCD.GetRecords("SGM_TMP", "USGR", opSgrUrno, &olTmpArr);

	for (int i = 0; i < olTmpArr.GetSize(); i++)
	{
		if (olTmpArr[i].Values[igSgmUvalIdx] == opUval)
		{
			blRet = true;
			break;
		}
	}
	
	olTmpArr.DeleteAll();

	CCS_CATCH_ALL
	return blRet;
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::FindObjectAndTitle(CString &opObject, CString &opTitle)
{
	if (m_Opt_Contr.GetCheck() == TRUE)
	{
		opObject = "COT";
		opTitle = GetString(305);
	}
	else if (m_Opt_Default.GetCheck() == CHECKED)
	{
		opObject = "POL";
		opTitle = GetString(306);
	}
	else if (m_Opt_Func.GetCheck() == CHECKED)
	{
		opObject = "PFC";
		opTitle = GetString(307);
	}
	else if (m_Opt_OrgUnit.GetCheck() == CHECKED)
	{
		opObject = "ORG";
		opTitle = GetString(309);
	}
	else if (m_Opt_PlGrp.GetCheck() == CHECKED)
	{
		opObject = "PGP";
		opTitle = GetString(310);
	}
	else if (m_Opt_Quali.GetCheck() == CHECKED)
	{
		opObject = "PER";
		opTitle = GetString(308);
	}
	else if (m_Opt_WkGrp.GetCheck() == CHECKED)
	{
		opObject = "WGP";
		opTitle = GetString(311);
	}
	else
	{
		opObject = "???";
		opTitle = "???";
	}
}
//---------------------------------------------------------------------------------------------------------------------

void PoolAllocDlg::SetStaticTexts()
{
	m_Opt_Contr.SetWindowText(GetString(305));
	m_Opt_Default.SetWindowText(GetString(306));
	m_Opt_Func.SetWindowText(GetString(307));
	m_Opt_Quali.SetWindowText(GetString(308));
	m_Opt_OrgUnit.SetWindowText(GetString(309));
	m_Opt_PlGrp.SetWindowText(GetString(310));
	m_Opt_WkGrp.SetWindowText(GetString(311));
	
	CWnd *pWnd = GetDlgItem(POLALO_FRA_STAFF);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(304));

	pWnd = GetDlgItem(POLALO_CHB_AUTO);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(1362));

	pWnd = GetDlgItem(POLALO_CHB_RESTR);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(319));

	pWnd = GetDlgItem(POLALO_STA_POOLS);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(313));

	pWnd = GetDlgItem(POLALO_FRA_RESP);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(312));

	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(314));

	pWnd = GetDlgItem(ID_HELP);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(315));

	pWnd = GetDlgItem(IDOK);
	if (pWnd != NULL)
		pWnd->SetWindowText(GetString(245));
}
//----------------------------------------------------------------------------------------------------------------------

/*void PoolAllocDlg::DumpTmpObject(CString opObject)
{
	CString olTable, olTrace;
	olTable = opObject + "_TMP";

	RecordSet olTmp(ogBCD.GetFieldCount(olTable));

	TRACE("---------------------------\n");
	int ilCount = ogBCD.GetDataCount(olTable);
	for (int i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("SGR_TMP", i, olTmp);
		if (olTable == "SGR_TMP")
		{
			olTrace.Format("Urno: %s, Appl: %s, Reft: %s.%s, Grpn:%s, Ugty:%s\n",
				olTmp.Values[igSgrUrnoIdx], olTmp.Values[igSgrApplIdx], olTmp.Values[igSgrTabnIdx], 
				olTmp.Values[igSgrFldnIdx], olTmp.Values[igSgrGrpnIdx], olTmp.Values[igSgrUgtyIdx]);	
		}
		else if (olTable == "SGM_TMP")
		{
			olTrace.Format("Urno: %s, Reft: %s, Uval:%s, Valu: %s, Usgr:%s, Ugty: %s\n",
				olTmp.Values[igSgmUrnoIdx], olTmp.Values[igSgmTabnIdx], olTmp.Values[igSgmUvalIdx], olTmp.Values[igSgmValuIdx],  
				olTmp.Values[igSgmUsgrIdx], olTmp.Values[igSgmUgtyIdx]);	
		}

		TRACE("(%s, %d) %s", olTable, i, olTrace);
	}
	TRACE("---------------------------\n");
}*/
//----------------------------------------------------------------------------------------------------------------------

