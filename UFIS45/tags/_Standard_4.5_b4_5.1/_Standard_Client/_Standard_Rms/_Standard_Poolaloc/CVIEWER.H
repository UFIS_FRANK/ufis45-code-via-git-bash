
#ifndef __CVIEWER_H__
#define __CVIEWER_H__

#include <CedaCfgData.h>
                                 
#ifndef HKEY_CLASSES_ROOT
    #define HKEY_CLASSES_ROOT           (( HKEY ) 0x80000000 )
#endif
#ifndef HKEY_CURRENT_USER
	#define HKEY_CURRENT_USER           (( HKEY ) 0x80000001 )
#endif
#ifndef HKEY_LOCAL_MACHINE
	#define HKEY_LOCAL_MACHINE          (( HKEY ) 0x80000002 )
#endif
#ifndef HKEY_USERS
	#define HKEY_USERS                  (( HKEY ) 0x80000003 )
#endif



class CViewer: public CObject
{
public:
    CViewer(); 
    ~CViewer();
    
public:
	CStringArray omFlightSearchConnection;
	CStringArray omGpeSearchConnection;
	void	SetViewerKey(CString strKey);
	BOOL    CreateView(CString strView, const CStringArray &possibleFilters, bool bpWithDBDelete = true);
	void	GetViews(CStringArray &strArray);
    BOOL    SelectView(CString strView);
	CString SelectView();
    BOOL    DeleteView(CString strView, bool bpWithDBDelete = true);
    
	void	GetFilterPage(CStringArray &strArray);
    void    SetFilter(CString strFilter, const CStringArray &opFilter);
    void	GetFilter(CString strFilter, CStringArray &opFilter);
    
    void    SetSort(CString opSortName, const CStringArray &opValues);
    void	GetSort(CString opSortName, CStringArray &opSort);
    
	void    SetSearch(CString opSearchName, const CStringArray &opValues);
	void    GetSearch(CString opSearchName, CStringArray &opValues);
	bool	GetDispoZeitraumWhereString(CString &ropWhere);

    void    SetGroup(CString strGroup);
	CString GetGroup();
	VIEW_VIEWNAMES * GetActiveView();
	VIEW_TEXTDATA * GetActiveFilter(CString opFilter,char *pcpViewName = NULL);
	void SafeDataToDB(CString opViewName);
	CString GetBaseViewName();
	CString GetViewName();
	void SetModul(CString opName);

private:
	CString	m_BaseViewName;
	CString	m_ViewName;
	CString omModul;
private:
    BOOL    CheckKey(char* pStrKey);
	BOOL	CreateKey(char* pStrKey);
	BOOL	DeleteKey(char* pStrKey);
	BOOL	SetValue(char* pStrKey, char* pStrValue);
	BOOL	GetValue(char* pStrKey, char* pStrValue);
	BOOL	DeleteFilter(CString strView);
};

#endif //__CVIEWER_H__
