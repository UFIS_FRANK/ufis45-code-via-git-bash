// CedaInitModuData.cpp
 
#include <stdafx.h>
#include <CedaInitModuData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const igNoOfPackets = 10;
CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool blRc = true;

	//-- exception handling
	CCS_TRY

//INITMODU//////////////////////////////////////////////////////////////

	CString olInitModuData = GetInitModuTxt();

	
	//INITMODU END//////////////////////////////////////////////////////////


	int ilCount = olInitModuData.GetLength();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);

	int ilCount2 = strlen(pclInitModuData);

	char pclTableExt[65];
	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");


	blRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	if(blRc==false)
	{
		AfxMessageBox(CString("SendInitModu: ") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}

	// ***** TEST
	/* 
	FILE *prlFp = fopen("c:\\Personal\\Test.txt","w");
	if(prlFp!=NULL)
	{
		fprintf(prlFp,"%s",pclInitModuData);
		fclose(prlFp);
	}
	*/
	// ***** TEST

	delete pclInitModuData;
	
	//-- exception handling
	CCS_CATCH_ALL

	return blRc;
}


//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt() 
{
	CString olInitModuData = ogAppName + CString(",InitModu,InitModu,Initialize (InitModu),B,-");

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ButtonList			
	olInitModuData += GetInitModuTxtG();
	
	return olInitModuData;
}
//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxtG() 
{
	CString olStr="";
	return olStr;
}
