// CCSObj.h: interface for the CCSObj class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSOBJ_H__F6245E65_0C5B_11D4_A713_00010204AA63__INCLUDED_)
#define AFX_CCSOBJ_H__F6245E65_0C5B_11D4_A713_00010204AA63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <ccserr.h>


/*
  This class serves as an upper class for new classes. There are two
  main rules you really should follow, if you derive from this class:
  
  It would not make too much sense for methods to always return
  an #int# specifying if the operation succeeded or not. This is
  why we store the error code in a class member. This member must
  be assigned a value by all none-#const# members of any
  derived class via #RC(someCode)#.

  For debugging perposes we require that objects of the
  #CCSObject# hierarchy are capable to print their contents. Therefore,
  the #Dump(..)#-method must be overloaded by all derived classes.
*/


class CCSObj  
{
public:

	// constructor
	CCSObj();
	virtual ~CCSObj();



// Implementation    

    
	// Set return code. Again: Use this method in all none-#const#
    //  methods of derived classes.
    CCSReturnCode RC(const CCSReturnCode& rc);
    
    // This method returns a reference to the object's return code
    // value, which should have been set by allnone-#const# members.
    const CCSReturnCode& RC() const;

    // Dont't forget: dump yourself!
    // Dump yourself. Should be overloaded by all derived classes.
    virtual void Dump(ostream& os, const char *label) const;


// Attributes
	CCSReturnCode omRc;
};

#endif // !defined(AFX_CCSOBJ_H__F6245E65_0C5B_11D4_A713_00010204AA63__INCLUDED_)
