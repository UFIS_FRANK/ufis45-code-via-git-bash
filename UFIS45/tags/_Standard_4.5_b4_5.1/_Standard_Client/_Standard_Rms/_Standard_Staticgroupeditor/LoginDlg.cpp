// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LoginDlg.h"
#include "BasicData.h"
#include "privlist.h"
#include "aboutdlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog


CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CLoginDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CLoginDlg)
	m_UserName = _T("");
	m_PassWord = _T("");
	//}}AFX_DATA_INIT
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoginDlg)
	DDX_Text(pDX, IDC_USER, m_UserName);
	DDX_Text(pDX, IDC_PASSWORD, m_PassWord);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_USER, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);

}

BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
    //{{AFX_MSG_MAP(CLoginDlg)
    ON_WM_PAINT()
	ON_BN_CLICKED(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CLoginDlg message handlers

void CLoginDlg::OnPaint() 
{
    CPaintDC dc(this); // device context for painting

    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
	olDC.DeleteDC( );    
    
    // Do not call CDialog::OnPaint() for painting messages
}

void CLoginDlg::OnOK() 
{
	CString clDmy;

	m_UsernameCtrl.GetWindowText(clDmy);
	sprintf(pcgUser,"%s",clDmy);
	m_PasswordCtrl.GetWindowText(clDmy);
	sprintf(pcgPasswd,"%s",clDmy);


	//strcpy(pcgUser, "UFIS$ADMIN");
	//strcpy(pcgUser, "ttt");
	//strcpy(pcgPasswd, "Passwort");
	
	BOOL blRc = ogPrivList.Login(pcgTableExt,pcgUser,pcgPasswd,ogAppName);

	if (blRc)
	{
		//UpdateData(TRUE);
		CDialog::OnOK();
	}
	else
	{
		MessageBox(ogPrivList.omErrorMessage,GetString(AFX_IDS_APP_TITLE),MB_OK);
	}
}



void CLoginDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
}



BOOL CLoginDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olCaption = GetString(IDS_LOGIN_TITEL);
	olCaption  += ogCommHandler.pcmRealHostName;
	olCaption  += " / ";
	olCaption  += ogCommHandler.pcmRealHostType;
	SetWindowText(olCaption);

	//m_UsernameCtrl.SetTypeToString("x(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	//m_PasswordCtrl.SetTypeToString("x(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);

	m_UsernameCtrl.SetFocus();

	SetDlgItemText ( IDC_PASSWORD_TXT, GetString(IDS_PASSWORT_TXT) );
	SetDlgItemText ( IDC_USER_TXT, GetString(IDS_USER_TXT) );
	olCaption = GetString (IDS_CANCEL);
	SetDlgItemText ( IDCANCEL, olCaption );
	SetDlgItemText ( IDOK, GetString (IDS_OK) );
	SetDlgItemText ( ID_APP_ABOUT, GetString (IDS_INFO) );
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CLoginDlg::OnAppAbout() 
{
	// TODO: Add your control notification handler code here
	CAboutDlg olDlg(this);

	olDlg.DoModal();
}
