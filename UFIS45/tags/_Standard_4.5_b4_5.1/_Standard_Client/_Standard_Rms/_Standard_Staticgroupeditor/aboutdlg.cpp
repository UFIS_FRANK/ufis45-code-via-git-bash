// AboutDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ccsglobl.h"
#include "versioninfo.h"
#include "Grp.h"
#include "AboutDlg.h"
#include <LibGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog


CAboutDlg::CAboutDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAboutDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAboutDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_ABOUT_GROUP, OnAboutGroup)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg message handlers

void CAboutDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CWnd *polWnd;
	CString	olStr, olForm;

	polWnd = GetDlgItem(IDC_UFISVERSION);
	if( polWnd )
	{
		olStr = GetString(IDS_UFISVERSION);
		polWnd->SetWindowText(olStr);
	}
	polWnd = GetDlgItem(IDC_SGRVERSION);
	if(polWnd )
	{
		VersionInfo olInfo;
		VersionInfo::GetVersionInfo(NULL,olInfo);

		olForm.LoadString (IDS_SGRVERSION);
		olStr.Format ( olForm, olInfo.omFileVersion);
		olStr += "  ";
		olStr += CString(__DATE__);
		polWnd->SetWindowText( olStr );
	}
	SetWindowText ( GetString(IDS_ABOUTBOX2) );

	polWnd = GetDlgItem(IDC_ABOUT_GROUP);
	if ( polWnd )
	{
		olStr = GetString(IDS_GROUP_INFO);
		polWnd->SetWindowText(olStr);
		polWnd->EnableWindow ( !omGrpUrno.IsEmpty() );
	}

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CAboutDlg::DoModal ( CString opActGrpUrno/*=""*/ )
{
	omGrpUrno = opActGrpUrno;
	return CDialog::DoModal();
}

//-----------------------------------------------------------------------------------------------------------------------
//						construction / destruction
//-----------------------------------------------------------------------------------------------------------------------
InfoDlg::InfoDlg(CString opGrpUrno, CWnd* pParent /*=NULL*/)
	: CDialog(InfoDlg::IDD, pParent)
{
	pomParent = pParent;
	omGrpUrno = opGrpUrno;

	//{{AFX_DATA_INIT(InfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	//Create();
}
//-----------------------------------------------------------------------------------------------------------------------

InfoDlg::~InfoDlg()
{
	
}


//-----------------------------------------------------------------------------------------------------------------------
//						data exchange, message map
//-----------------------------------------------------------------------------------------------------------------------
void InfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InfoDlg)
	DDX_Control(pDX, IDC_CDAT, m_CdatEdit);
	DDX_Control(pDX, IDC_LSTU_, m_LstuEdit);
	DDX_Control(pDX, IDC_USEU, m_UseuEdit);
	DDX_Control(pDX, IDC_USEC, m_UsecEdit);
	//}}AFX_DATA_MAP
}
//-----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(InfoDlg, CDialog)
	//{{AFX_MSG_MAP(InfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-----------------------------------------------------------------------------------------------------------------------
//						creation
//-----------------------------------------------------------------------------------------------------------------------
void InfoDlg::Create()
{
	// This method is not used as long as we open the dialog with DoModal()

	CDialog::Create(IDD_INFO_DLG, pomParent);
}



//-----------------------------------------------------------------------------------------------------------------------
//						message handling
//-----------------------------------------------------------------------------------------------------------------------
BOOL InfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetStaticTexts();

	//--- exit dialog if no rule urno specified
	if (omGrpUrno.IsEmpty() || omGrpUrno == " ")
	{
		MessageBox(GetString(IDS_NO_GROUP), GetString(AFX_IDS_APP_TITLE));
		EndDialog(0);
	}

	//--- Set window caption 
	CString olText = GetString(IDS_GROUP_INFO) + CString(" ") + ogBCD.GetField("SGR", "URNO", omGrpUrno, "GRPN");
	SetWindowText(olText);
	
	CString olCdat;
	CString olUsec;
	CString olLstu;
	CString olUseu;

	//--- Get values
	RecordSet olRecord(ogBCD.GetFieldCount("SGR"));
	ogBCD.GetRecord("SGR", "URNO", omGrpUrno, olRecord);
	olCdat = olRecord.Values[ogBCD.GetFieldIndex("SGR","CDAT")];
	olUsec = olRecord.Values[ogBCD.GetFieldIndex("SGR","USEC")];
	olLstu = olRecord.Values[ogBCD.GetFieldIndex("SGR","LSTU")];
	olUseu = olRecord.Values[ogBCD.GetFieldIndex("SGR","USEU")];

	//--- format values
	if (olCdat.GetLength() < 14)
	{
		olCdat = CString(GetString(IDS_NA));
	}
	else
	{
		olCdat = DBStringToDateString(olCdat);
	}

	if (olLstu.GetLength() < 14)
	{
		olLstu = CString(GetString(IDS_NA));
	}
	else
	{
		 olLstu = DBStringToDateString(olLstu);
	}

	if ( olUsec.IsEmpty() )
		olUsec = GetString(IDS_NA);
	if ( olUseu.IsEmpty() )
		olUseu = GetString(IDS_NA);
	//--- write values to controls
	m_CdatEdit.SetWindowText(olCdat);
	m_UsecEdit.SetWindowText(olUsec);
	m_LstuEdit.SetWindowText(olLstu);
	m_UseuEdit.SetWindowText(olUseu);

	// UpdateData();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//-----------------------------------------------------------------------------------------------------------------------

void InfoDlg::SetStaticTexts()
{
	CCS_TRY

	
	SetWindowText ( GetString(IDS_ABOUT_GROUP) );

	SetDlgItemText ( IDC_USEC_STATIC, GetString(IDS_CREATED) );
	SetDlgItemText ( IDS_USEU_STATIC, GetString(IDS_LAST_CHANGE) );
	SetDlgItemText ( IDOK, GetString(IDS_OK) );

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

CString DBStringToDateString(CString opStr)
{
	//------------------------------------------------------------------
	// Converts a date string from CCS date format to german date format
	// e.g. "19990812134000"  ==> "12.08.1999 - 13:40:00"
	//------------------------------------------------------------------

	CString olReturn;	
	const CString olError = "";
	
	if (opStr.GetLength() == 14)
	{
		olReturn = opStr.Mid(6, 2) + "." + opStr.Mid(4,2) + "." + opStr.Left(4) + " - " + opStr.Mid(8,2) + ":" + 
						opStr.Mid(10,2) + ":" + opStr.Right(2);
	}
	else
	{
		return olError;
	}

	return olReturn;


}

void CAboutDlg::OnAboutGroup() 
{
	// TODO: Add your control notification handler code here
	InfoDlg *polDlg = new InfoDlg(omGrpUrno, this);
	polDlg->DoModal();
	delete polDlg;
}
