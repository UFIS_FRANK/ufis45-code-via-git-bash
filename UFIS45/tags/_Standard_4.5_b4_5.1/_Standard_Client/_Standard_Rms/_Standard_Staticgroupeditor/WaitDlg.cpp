// WaitDlg.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "WaitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CWaitDlg 


CWaitDlg::CWaitDlg(CWnd* pParent /*=NULL*/,CString csText )
	: CDialog(CWaitDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWaitDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	m_csText = csText;

	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
	CDialog::Create(IDD, pParent);
}


void CWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWaitDlg)
	DDX_Control(pDX, IDC_ANIMATE1, m_oAnimate);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWaitDlg, CDialog)
	//{{AFX_MSG_MAP(CWaitDlg)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CWaitDlg 

BOOL CWaitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Warte Mouse
	AfxGetApp()->DoWaitCursor(1);
	
	// Laden des AVI Files und abspielen
	CAnimateCtrl *pCtrl=(CAnimateCtrl*)GetDlgItem(IDC_ANIMATE1);
	pCtrl->Open(IDR_FILECOPY);
	//pCtrl->Open(IDR_DATACOMP);
	pCtrl->Play(0,-1,-1);
	
	// Default Text setzen
	if (m_csText == "")	m_csText = "Please Wait...";

	SetDlgItemText(IDC_TEXT,m_csText);

	return TRUE;  
}

//***************************************************************************************
//
//***************************************************************************************

void CWaitDlg::OnClose() 
{
	// Warte Mouse
	AfxGetApp()->DoWaitCursor(-1);
	CDialog::OnClose();
}
