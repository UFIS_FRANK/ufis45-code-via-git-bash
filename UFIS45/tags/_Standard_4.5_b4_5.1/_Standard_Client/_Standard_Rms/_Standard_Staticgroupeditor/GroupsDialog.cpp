// GroupsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "Grp.h"
#include "GroupsDialog.h"
#include "STGrid.h"
#include "AboutDlg.h"

//OWN INCLUDES
#include "CCSGlobl.h"
#include "CedaBasicData.h"
#include "InputBox.h"
#include "privlist.h"
#include "basicdata.h"

//EXPERIMENTAL
#include "TreeDialog.h"
 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CGroupsDialog dialog


CGroupsDialog::CGroupsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CGroupsDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGroupsDialog)
	//}}AFX_DATA_INIT
	bmAllocCBEnabled = TRUE;
	bmTplCBEnabled = TRUE;
}


void CGroupsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGroupsDialog)
	DDX_Control(pDX, IDC_COMBOTYPES, omComboTypes);
	DDX_Control(pDX, IDC_UGTY_CB, omAllocCB);
	DDX_Control(pDX, IDC_UTPL_CB, omTplCB);
	DDX_Control(pDX, IDC_NOGROUP, m_NoGroup);
	DDX_Control(pDX, IDC_NOTABLES, m_NoTables);
	DDX_Control(pDX, IDC_COMBOGROUPS, m_ComboGroups);
	DDX_Control(pDX, IDC_COMBOTABLES, m_ComboTables);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGroupsDialog, CDialog)
	//{{AFX_MSG_MAP(CGroupsDialog)
	ON_CBN_CLOSEUP(IDC_COMBOTABLES, OnCloseupCombotables)
	ON_WM_CLOSE()
	ON_CBN_CLOSEUP(IDC_COMBOGROUPS, OnCloseupCombogroups)
	ON_BN_CLICKED(IDNEU, OnNeu)
	ON_BN_CLICKED(IDBEENDEN, OnBeenden)
	ON_BN_CLICKED(IDSUCHEN, OnSuchen)
	ON_BN_CLICKED(IDCANCELCHANGE, OnCancelchange)
	ON_BN_CLICKED(IDSAVE, OnSave)
	ON_BN_CLICKED(IDDELETE, OnDelete)
	ON_BN_CLICKED(IDADDMEMBER, OnAddmember)
	ON_BN_CLICKED(IDDELETEMEMBER, OnDeletemember)
	ON_BN_CLICKED(IDC_TREE, OnTree)
	ON_CBN_SELENDOK(IDC_UGTY_CB, OnSelendokUgtyCb)
	ON_CBN_SELENDOK(IDC_UTPL_CB, OnSelendokUtplCb)
	ON_CBN_KILLFOCUS(IDC_UGTY_CB, OnKillfocusUgtyCb)
	ON_BN_CLICKED(ID_APP_ABOUT, OnAppAbout)
	ON_CBN_CLOSEUP(IDC_COMBOTYPES, OnCloseupCombotypes)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_BCADD,OnBcAdd)
END_MESSAGE_MAP()



LONG CGroupsDialog::OnBcAdd(UINT wParam, LONG /*lParam*/)
{
	ogBcHandle.GetBc(wParam);
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CGroupsDialog message handlers

BOOL CGroupsDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	((CGrpApp *)AfxGetApp())->m_hwndDialog = m_hWnd;
	
	SetWindowText(GetString(AFX_IDS_APP_TITLE));

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0 || stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	else
	{
		ogCommHandler.RegisterBcWindow(this);
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~ INITIALIZE GRID
	olGrdTable = new CSTGrid;
	olGrdTable->SubClassDlgItem(IDC_GRIDAUSWAHL, this);
	olGrdTable->Initialize();
	olGrdTable->SetRowCount(0);		//number of rows
	olGrdTable->SetColCount(5);		//number of cols
	olGrdTable->SetGrdID(1);
	olGrdTable->mDirty=FALSE;
	olGrdTable->SetValueRange(CGXRange(0,0,0,5),"");	
	olGrdTable->GetParam()->EnableTrackRowHeight(FALSE);	//disable rowsizing
	olGrdTable->GetParam()->EnableMoveRows(FALSE);			//disable rowmoving
	
	olGrdTable->GetParam()->EnableSelection(GX_SELROW|GX_SELMULTIPLE|GX_SELSHIFT );
	//olGrdTable->GetParam()->EnableSelection(GX_SELNONE);

	olGrdGroup = new CSTGrid;
	olGrdGroup->SubClassDlgItem(IDC_GRIDGROUP, this);
	olGrdGroup->Initialize();
	olGrdGroup->SetRowCount(0);		//number of rows
	olGrdGroup->SetColCount(5);		//number of cols
	olGrdGroup->SetGrdID(2);
	olGrdGroup->mDirty=FALSE;
	olGrdGroup->SetValueRange(CGXRange(0,0,0,5),"");	
	olGrdGroup->GetParam()->EnableTrackRowHeight(FALSE);	//disable rowsizing
	olGrdGroup->GetParam()->EnableMoveRows(FALSE);			//disable rowmoving
	
	olGrdGroup->GetParam()->EnableSelection(GX_SELROW|GX_SELMULTIPLE|GX_SELSHIFT );
	//olGrdGroup->GetParam()->EnableSelection(GX_SELNONE);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	

	CDC dc;
    dc.CreateCompatibleDC(NULL);
	
	LOGFONT logFont;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
	mGrdFont.SetLogFont(logFont);

	//m_ComboTables.SetFont(&ogCourier_Regular_10);
	//m_ComboGroups.SetFont(&ogCourier_Regular_10);


	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//*** SET LABELS OF ALL CONTROLS
	GetDlgItem(IDC_FRTABLES)->SetWindowText(GetString(IDS_GRP0001));
	GetDlgItem(IDC_FRGROUPS)->SetWindowText(GetString(IDS_GRP0002));
	GetDlgItem(IDC_TEXT)->SetWindowText(GetString(IDS_GRP0003));
	GetDlgItem(IDSUCHEN)->SetWindowText(GetString(IDS_SEARCH));
	GetDlgItem(IDNEU)->SetWindowText(GetString(IDS_NEU));
	GetDlgItem(IDDELETE)->SetWindowText(GetString(IDS_DELETE));
	GetDlgItem(IDCANCELCHANGE)->SetWindowText(GetString(IDS_CANCEL));
	GetDlgItem(IDSAVE)->SetWindowText(GetString(IDS_SAVE));
	GetDlgItem(IDBEENDEN)->SetWindowText(GetString(IDS_EXIT));
	GetDlgItem(IDC_UGTY_TXT)->SetWindowText(GetString(IDS_ZUORDNUNGSEINHEIT));
	SetDlgItemText ( ID_APP_ABOUT, GetString (IDS_INFO) );
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	//*** SET STARTING LOCK-STATE OF CONTROLS
	/*
	GetDlgItem(IDSAVE)->EnableWindow(FALSE);
	GetDlgItem(IDDELETE)->EnableWindow(FALSE);
	GetDlgItem(IDCANCELCHANGE)->EnableWindow(FALSE);
	GetDlgItem(IDNEU)->EnableWindow(FALSE);
	GetDlgItem(IDADDMEMBER)->EnableWindow(FALSE);
	GetDlgItem(IDDELETEMEMBER)->EnableWindow(FALSE);*/
	DecideControls ();   // hag 20011203
	
	m_ActFindCol = 1;
	m_ActFindRow = 1;

	//*** FILL COMBO WITH THE AVAILABLES TABLES
	FillComboTables();
	bmAllocCBEnabled = ( ogCallingAppName != "RULE_AFT");
	bmTplCBEnabled = (ogCallingAppName == "RULE_AFT");

	ogBCD.SetObject("SGR");
	ogBCD.SetObject("SYS");
	ogBCD.SetObject("SGM");

	if ( bmTplCBEnabled && ogBCD.GetFieldIndex("SGR","UTPL")<0 )
	{	// Field UTPL missing in SGRTAB
		bmTplCBEnabled = FALSE;
		ogLog.Trace("SGRTAB","Field UTPL is missing");
	}
	if ( bmAllocCBEnabled )
		IniAlocCB ();
	if ( bmTplCBEnabled )
		IniTplCB ();
	//  disable Alocation Unit ComboBox, when GRP.exe had been called from Regelwerk
	CWnd *polCtrl;
	omAllocCB.EnableWindow ( bmAllocCBEnabled );
	omAllocCB.ShowWindow ( bmAllocCBEnabled );
	
	omTplCB.EnableWindow ( bmTplCBEnabled );
	omTplCB.ShowWindow ( bmTplCBEnabled );

	polCtrl = GetDlgItem ( IDC_UGTY_TXT );
	if ( polCtrl )
	{
		polCtrl->EnableWindow ( bmAllocCBEnabled );
		polCtrl->ShowWindow ( bmAllocCBEnabled );
	}
	polCtrl = GetDlgItem ( IDC_UTPL_TXT );
	if ( polCtrl )
	{
		polCtrl->EnableWindow ( bmTplCBEnabled );
		polCtrl->ShowWindow ( bmTplCBEnabled );
	}
	
	InitializeComboTypes();

	SetWindowPos ( &wndTop, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE  );
	CenterWindow();
	return TRUE;  // return TRUE unless you set the focus to a control

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::PostNcDestroy() 
{
	//*** clean up grid-objects
	delete olGrdTable;
	delete olGrdGroup;

	CDialog::PostNcDestroy();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGroupsDialog::FillComboTables()
{
	// *** FILL COMBOBOX WITH AVAILABLE TABLES

	m_ComboTables.ResetContent();
	m_olLtna.RemoveAll();
 
	int ilDataCount = ogBCD.GetDataCount("TAB");
	CString clRow="";
	CString clShort = "";
	BOOL blRead;

	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		blRead=ogBCD.GetRecord("TAB", i,  olRecord);
		clRow=olRecord.Values[ogBCD.GetFieldIndex("TAB","LNAM")];
		m_ComboTables.AddString(clRow);
		//following works only when combo-sorted=false !
		//*** 20.08.99 SHA ***
		//m_olLtna.Add(olRecord.Values[ogBCD.GetFieldIndex("TAB","LTNA")]);
		clShort = olRecord.Values[ogBCD.GetFieldIndex("TAB","LTNA")];
		clShort = clShort.Left(3);
		m_olLtna.Add(clShort);

		#ifdef _DEBUG	//  hag010605 
			afxDump << "olRecord.Values" << olRecord.Values[ogBCD.GetFieldIndex("TAB","LTNA")] <<"\n";
		#endif
	}
	return 0;
} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGroupsDialog::FillGridTables(CString cTable)
{
	// *** FILL GRID WITH SELECTED TABLE CONTENT

	CString clDmy;
	CString olWhere;

	//*** FILL WITH VALUES
	void *plDummy; 
	if( !ogBCD.omObjectMap.Lookup(m_cTable,plDummy) )
		ogBCD.SetObject(m_cTable);

	if ( ( m_cTable == "EQU" ) )
		olWhere.Format ( "WHERE GKEY=%ld", atol(omSubtypeUrno) );
	ogBCD.Read(m_cTable, olWhere );
	if ( m_oFina.GetSize() > 0 )
		ogBCD.SetSort(cTable,m_oFina[0]+"+",true);

	int ilDataCount=ogBCD.GetDataCount(cTable); //number of rows
	int ilCols=m_oFina.GetSize();

	olGrdTable->SetRowCount(ilDataCount);

	m_iTableRows=ilDataCount;
	clDmy.Format(GetString(IDS_GRP0013),ilDataCount);
	m_NoTables.SetWindowText(clDmy);


	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord(cTable, i,  olRecord);
		
		//for (int iFields=0;iFields<m_oFina.GetSize();iFields++)
		for (int iFields=0;iFields<ilCols;iFields++)
		{
			olGrdTable->SetValueRange(CGXRange(i+1,iFields+1),
				olRecord.Values[ogBCD.GetFieldIndex(cTable,m_oFina[iFields])]);
		}

		olGrdTable->SetValueRange(CGXRange(i+1,ilCols+1),olRecord.Values[ogBCD.GetFieldIndex(cTable,"URNO")]);
	}

	//*** MANIPULATE GRID ***
	//olGrdTable->SetStyleRange(CGXRange(0,0,ilDataCount,ilCols),CGXStyle().SetReadOnly(TRUE));
	if ( ilDataCount>0)
		olGrdTable->SetStyleRange(CGXRange(1,1,ilDataCount,ilCols+1),CGXStyle().SetControl(GX_IDS_CTRL_STATIC));

	return 0;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnCloseupCombotables() 
{

	int iSel=m_ComboTables.GetCurSel();

	if (iSel!=-1)
	{
		AfxGetApp()->DoWaitCursor(1); 

		m_cTableLong=m_olLtna.GetAt(iSel);
		void *plDummy; 
		if( !ogBCD.omObjectMap.Lookup(m_cTableLong,plDummy) )
			ogBCD.SetObject(m_cTableLong);
		//ogBCD.Read(m_cTableLong);
		CreateTableInfo(m_olLtna.GetAt(iSel));

		InitializeComboTypes();
		//*** 20.08.99 SHA ***
		m_cTable = m_cTableLong;
		FillGridTables(m_cTable);

		FillComboGroups(m_olLtna.GetAt(iSel));
		
		olGrdGroup->mDirty = FALSE;

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//*** PRIVILEGES
		/*
		if (ogPrivList.GetStat("GROUPFUNCTIONS")=='1')
		{
			GetDlgItem(IDDELETE)->EnableWindow(FALSE);
			GetDlgItem(IDCANCELCHANGE)->EnableWindow(FALSE);
			GetDlgItem(IDSAVE)->EnableWindow(FALSE);
			GetDlgItem(IDNEU)->EnableWindow(TRUE);
		}*/
		DecideControls ();   // hag 20011203

		
		m_NoGroup.SetWindowText("");

		AfxGetApp()->DoWaitCursor(-1);
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGroupsDialog::CreateTableInfo(CString cTable)
{
	CString clDmy,clValue;
	int ilDataCount;

	m_oFina.RemoveAll();
	m_oHedr.RemoveAll();

	olGrdTable->SetRowCount(0);
	olGrdTable->SetColCount(0);

	olGrdGroup->SetRowCount(0);
	olGrdGroup->SetColCount(0);

	CString clWhere;
	//*** 20.08.99 SHA ***
	//*** WORKAROUND F�R LTNA=APTTAB AND NOT APT ***
	clWhere.Format("WHERE TANA='%s'",cTable);
	//clWhere.Format("WHERE FTNA='%s'",cTable);
	ogBCD.Read("SYS",clWhere);

/*	omFinaToHedr.RemoveAll();
	ilDataCount=ogBCD.GetDataCount("SYS");
	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		BOOL blRead=ogBCD.GetRecord("SYS", i,  olRecord);
		clDmy=olRecord.Values[ogBCD.GetFieldIndex("SYS","FINA")];
		clValue=olRecord.Values[ogBCD.GetFieldIndex("SYS","HEDR")];
		omFinaToHedr.SetAt(clDmy,clValue);
	}
*/

	if (IniAvailable(cTable))
	{
		// *** fields to show from ini-file
		int ilFields=1;
		clDmy.Format("Field%d",ilFields);
		clValue=ReadIni(cTable,clDmy);
		while (clValue!="#")
		{
			if ( ogBCD.GetFieldIndex(cTable,clValue) >= 0 )
				m_oFina.Add(clValue);
			#ifdef _DEBUG	//  hag010605 
				afxDump << clValue <<"\n";
			#endif
			ilFields++;
			clDmy.Format("Field%d",ilFields);
			clValue=ReadIni(cTable,clDmy);
		}
	}
	else 
	{
		CString clWhere,clRow;
		clWhere.Format("WHERE TANA ='%s'",cTable);
		//clWhere.Format("WHERE FTNA='%s'",cTable);	
		ogBCD.Read("SYS",clWhere);
	
		ilDataCount=ogBCD.GetDataCount("SYS");

		for(int i = 0; i < ilDataCount; i++)
		{
			RecordSet olRecord;
			BOOL blRead=ogBCD.GetRecord("SYS", i,  olRecord);
			clRow=olRecord.Values[ogBCD.GetFieldIndex("SYS","FINA")];
			if ( (clRow!="HOPO") && (ogBCD.GetFieldIndex(cTable,clRow)>=0) )
				m_oFina.Add(clRow);
		}
	}

	int ilFinaSize=m_oFina.GetSize();
	m_oHedr.SetSize(ilFinaSize);

	//GRIDS
	olGrdTable->SetColCount(ilFinaSize+1);
	olGrdGroup->SetColCount(ilFinaSize+1);

	//***PLUS 1 FOR THE URNOS AND HIDE THE URNOCOL
	olGrdGroup->HideCols(ilFinaSize+1,ilFinaSize+1);
	olGrdTable->HideCols(ilFinaSize+1,ilFinaSize+1);
	
	for (int ilHedr=0;ilHedr<ilFinaSize;ilHedr++)
	{
		//*** DESCRIPTION FOR COLHEADER ***
		CString clField=ogBCD.GetField("SYS","FINA",m_oFina.GetAt(ilHedr),"LABL");
		if (clField=="")
			//clField=ogBCD.GetField("SYS","FINA",m_oFina.GetAt(ilHedr),"FINA");
			clField=m_oFina.GetAt(ilHedr);

		m_oHedr.Add(clField);
		olGrdTable->SetValueRange(CGXRange(0,ilHedr+1),clField);
		olGrdGroup->SetValueRange(CGXRange(0,ilHedr+1),clField);
	}

	//*** table name
	if ( ilFinaSize>0 )
		m_cTable=ogBCD.GetField("SYS","FINA",m_oFina.GetAt(0),"TANA");
	//m_cTable=ogBCD.GetField("SYS","FINA",m_oFina.GetAt(0),"FTNA");

	return 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CString CGroupsDialog::ReadIni(CString cSection, CString cKey)
{
	// *** Reads ini-value *** 
	CString cValue="";
	char pclValue[100];
	GetPrivateProfileString(cSection, cKey, "#", pclValue, sizeof(pclValue), ogIniFile);
	cValue=pclValue;
	return cValue;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL CGroupsDialog::IniAvailable(CString cTable)
{
	// *** SEARCHES INIFILE AND SECTION
	BOOL bReturn=FALSE;
	CFile oIniFile;

	if (oIniFile.Open(ogIniFile,CFile::modeRead))
	{		bReturn=TRUE;
			oIniFile.Close();	}

	if (bReturn)
		if (ReadIni(cTable,"FIELD1")=="#")
			bReturn=FALSE;
	
	return bReturn;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnClose() 
{	
	//if (MessageBox(LoadStg(IDS_GRP0010),LoadStg(IDS_APPLNAME),MB_ICONQUESTION|MB_YESNO)==IDYES)	
		CDialog::OnClose();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnCancel() 
{	
	int ilRet = IDYES;
	if ( !theApp.bIntern )
		ilRet = MessageBox ( GetString(IDS_ASK_FOR_EXIT), 
							 GetString(AFX_IDS_APP_TITLE),
							 MB_ICONQUESTION|MB_YESNO );
	if ( ilRet == IDYES )
	{
		char pclConfigPath[142];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));


		char pclUseBcProxy[256];
		GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

		//MWO/RRO 25.03.03
		if (stricmp(pclUseBcProxy,"BCSERV") == 0)
		{
			ogCommHandler.UnRegisterBcWindow(this);
		}

		CDialog::OnCancel();
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnOK()
{
	 CWnd* pwndCtrl = GetFocus();

     int ctrl_ID = pwndCtrl->GetDlgCtrlID();

     switch (ctrl_ID) {
         case IDC_COMBOTABLES:
			 OnCloseupCombotables();
			 GetDlgItem(IDC_COMBOGROUPS)->SetFocus();
             break;
         case IDC_COMBOGROUPS:
			 OnCloseupCombogroups();
			 break;
         default:
             break;
     }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGroupsDialog::FillComboGroups(CString cTable)
{
	//*** FILL COMBO WITH EXISTING GROUPS TO ACTUAL TABLE
	CString olWhereSubtype; 

	m_ComboGroups.ResetContent();
	m_oGrpUrnos.RemoveAll();

	CString clWhere, olTplUrno;
	
	if (ogCallingAppName == "*")
		clWhere.Format("WHERE TABN='%s'",cTable,ogCallingAppName);
	else
		clWhere.Format("WHERE TABN='%s' AND APPL='%s'",cTable,ogCallingAppName);

	if ( m_cTable == "EQU" ) 
	{
		olWhereSubtype.Format ( " AND STYP='%s'", omSubtypeUrno );
		clWhere += olWhereSubtype;
	}
	ogBCD.Read("SGR",clWhere);
	ogBCD.SetSort("SGR","GRPN+",true);

	int ilDataCount=ogBCD.GetDataCount("SGR"); //number of rows

	CString clRow; 
	if ( bmTplCBEnabled )
		olTplUrno = GetActTplUrno ();

	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		ogBCD.GetRecord("SGR", i,  olRecord);
		if ( bmTplCBEnabled && 
			 olTplUrno != olRecord.Values[ogBCD.GetFieldIndex("SGR","UTPL")] )
			 continue;
		clRow=olRecord.Values[ogBCD.GetFieldIndex("SGR","GRPN")];
		m_ComboGroups.AddString(clRow);
		
		clRow=olRecord.Values[ogBCD.GetFieldIndex("SGR","URNO")];
		char  c[15];
		sprintf(c,"%s",clRow);
		m_oGrpUrnos.Add(atol(c));
	}

	return 0;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnCloseupCombogroups() 
{
	FillGridGroups();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnNeu() 
{
	CString cNeu=InputBox(GetString(AFX_IDS_APP_TITLE),GetString(IDS_INPUT_NAME),"",32);

	if (cNeu!="")
	{
		CString cDmy;
		long ilUrno=ogBCD.GetNextUrno();
		mActGrpUrno=ilUrno;
		cDmy.Format("%d",ilUrno);

		//***KILL URNOMAPS
		omGrpUrnoMain.RemoveAll();
		omGrpUrnoDel.RemoveAll();
		omGrpUrnoNew.RemoveAll();

		olGrdTable->ClearMarks();

		RecordSet olRecord(ogBCD.GetFieldCount("SGR"));
		olRecord.Values[ogBCD.GetFieldIndex("SGR","TABN")]=m_cTableLong;
		olRecord.Values[ogBCD.GetFieldIndex("SGR","URNO")]=cDmy;
		olRecord.Values[ogBCD.GetFieldIndex("SGR","GRPN")]=cNeu;
		olRecord.Values[ogBCD.GetFieldIndex("SGR","APPL")]=ogCallingAppName;
		int ilIdx = ogBCD.GetFieldIndex("SGR","STYP");
		if ( ( ilIdx >= 0 ) && ( m_cTable == "EQU" ) )
			olRecord.Values[ilIdx] = omSubtypeUrno;
		//*** 25.08.99 SHA ***
		//  hag991022  olRecord.Values[ogBCD.GetFieldIndex("SGR","UVAL")]=" ";
		int iSel = -1;
		DWORD llTplUrno=0;
		
		if ( bmAllocCBEnabled )
			iSel = omAllocCB.GetCurSel ();
		if ( iSel >= 0 )
			cDmy.Format ( "%lu", omAllocCB.GetItemData(iSel) );
		else
			cDmy.Empty ();
		olRecord.Values[ogBCD.GetFieldIndex("SGR","UGTY")]=cDmy;

		if ( bmTplCBEnabled )
		{
			cDmy = GetActTplUrno ();
			olRecord.Values[ogBCD.GetFieldIndex("SGR","UTPL")]=cDmy;
		}
		ogBCD.InsertRecord("SGR",olRecord,TRUE);

		m_ComboGroups.AddString(cNeu);
		m_ComboGroups.SetCurSel(m_ComboGroups.GetCount()-1);

		olGrdGroup->SetRowCount(0);
		m_oGrpUrnos.Add(ilUrno);
		/*
		if (ogPrivList.GetStat("GROUPFUNCTIONS")=='1')
		{
			GetDlgItem(IDDELETE)->EnableWindow(TRUE);
			GetDlgItem(IDADDMEMBER)->EnableWindow(TRUE);
			GetDlgItem(IDDELETEMEMBER)->EnableWindow(TRUE);
		}
		*/
		DecideControls();
	}

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CString CGroupsDialog::InputBox(CString cTitle, CString cText, CString cDefault, long iLen)
{
	CInputBox dlg;
	dlg.Set(cTitle,cText,cDefault, iLen);
	dlg.DoModal();
	CString cBack=dlg.GetInput();
	
	return cBack;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnBeenden() 
{
	int ilRet = IDYES;
	if ( !theApp.bIntern )
		ilRet = MessageBox ( GetString(IDS_ASK_FOR_EXIT), 
							 GetString(AFX_IDS_APP_TITLE),
							 MB_ICONQUESTION|MB_YESNO );
	if ( ilRet == IDYES )
		CDialog::OnOK();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnSuchen() 
{	
	//*** FIND  GIVEN TEXT IN THE LEFT GRID***	
	
	//*** TO FINS TEXT ANYWHERE
	//*** TODO ! ******
	//olGrdTable->SetCurrentCell(m_ActFindRow,m_ActFindCol );//,GX_UPDATENOW|GX_SCROLLINVIEW);

	CString olSearch;
	GetDlgItem(IDC_SUCHTEXT)->GetWindowText(olSearch);

	CFindReplaceDialog *pFindReplaceDlgMfc;

	pFindReplaceDlgMfc = new CFindReplaceDialog;

	GX_FR_STATE* pState = GXGetLastFRState();

	//*** PARAMETERS ***
	pState->pFindReplaceDlg = pFindReplaceDlgMfc;
	pState->bFindOnly =TRUE;
	pState->strFind = olSearch;
	pState->bCase = FALSE;
	pState->bNext = TRUE;
	pState->PrepareFindReplace();

	olGrdTable->FindText(TRUE);

	//ROWCOL nRow, nCol;
	//olGrdTable->GetCurrentCell(nRow, nCol);
	//m_ActFindRow = nRow+1;

	delete pFindReplaceDlgMfc;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGroupsDialog::FillGridGroups()
{
	//*** SHOW ALL MEMBERS OF THE SELECTED GROUP

	int iSel=m_ComboGroups.GetCurSel();

	AfxGetApp()->DoWaitCursor(1);

	olGrdGroup->mDirty = FALSE;

	if (iSel!=-1)
	{
		CString clDmy;

		//***KILL URNOMAPS
		omGrpUrnoNew.RemoveAll();
		//omGrpUrno.RemoveAll();
		omGrpUrnoMain.RemoveAll();
		omGrpUrnoDel.RemoveAll();

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//*** PRIVILEGES
		/*
		if (ogPrivList.GetStat("GROUPFUNCTIONS")=='1')
		{
			GetDlgItem(IDDELETE)->EnableWindow(TRUE);
			GetDlgItem(IDADDMEMBER)->EnableWindow(TRUE);
			GetDlgItem(IDDELETEMEMBER)->EnableWindow(TRUE);
		}
		*/
		//DecideControls ();   // hag 20011203

		olGrdGroup->SetRowCount(0);
		olGrdTable->ClearMarks();
		long ilUrno=m_oGrpUrnos[iSel];
		mActGrpUrno=ilUrno;

		CString clWhere, olUrno;
		//clWhere.Format("WHERE GURN=%d",ilUrno);
		clWhere.Format("WHERE USGR='%d'",ilUrno);
		ogBCD.Read("SGM",clWhere);

		int ilDataCount=ogBCD.GetDataCount("SGM"); //number of rows
		//olGrdGroup->SetRowCount(ilDataCount);

		//***COLLECT THE URNOS
		CStringList olUrnosInGrp;
		//clWhere="WHERE URNO IN (";
		for(int iRow = 0; iRow < ilDataCount; iRow++)
		{
			//RecordSet olRecord;
			//ogBCD.GetRecord("SGM", iRow,  olRecord);
			//clWhere+=olRecord.Values[ogBCD.GetFieldIndex("SGM","UVAL")];
			olUrno = ogBCD.GetField( "SGM", iRow, "UVAL" );
			if( !olUrno.IsEmpty() && !olUrnosInGrp.Find(olUrno ) )
				olUrnosInGrp.AddTail(olUrno);
			else
				olGrdGroup->mDirty=TRUE;
		}
		DecideControls ();   // hag 20011203
		//clWhere+=")";

		//READ THE ENTRIES
		void *plDummy; 
		if( !ogBCD.omObjectMap.Lookup(m_cTable,plDummy) )
		{
			ogBCD.SetObject(m_cTable);
			ogBCD.Read(m_cTable);
		}
		int ilCols=m_oFina.GetSize();
		if ( ilCols>0 )
			ogBCD.SetSort(m_cTable,m_oFina[0]+"+",true);

		ilDataCount = olUrnosInGrp.GetCount() ; //number of all records, rows
		olGrdGroup->SetRowCount( ilDataCount );

		clDmy.Format(GetString(IDS_GRP0013),ilDataCount);
		m_NoGroup.SetWindowText(clDmy);

		POSITION ilPos = olUrnosInGrp.GetHeadPosition();
		int i=0;
		DWORD start,end;
		olGrdTable->LockUpdate();
		olGrdGroup->LockUpdate();
		start = GetTickCount();
		while ( ilPos )
		{
			RecordSet olRecord;
			olUrno = olUrnosInGrp.GetNext(ilPos);
			BOOL blRead=ogBCD.GetRecord(m_cTable, "URNO", olUrno, olRecord);
			if ( !blRead )
			{
				long ilUrno = atol ( olUrno );  //  corrupt group member
				omGrpUrnoDel.SetAt(ilUrno,ilUrno);
				ilDataCount = olGrdGroup->GetRowCount();
				olGrdGroup->RemoveRows ( ilDataCount, ilDataCount );
				continue;
			}
			for (int iFields=0;iFields<ilCols;iFields++)
			{
				olGrdGroup->SetValueRange(CGXRange(i+1,iFields+1),
					olRecord.Values[ogBCD.GetFieldIndex(m_cTable,m_oFina[iFields])]);
			}

			olGrdGroup->SetValueRange(CGXRange(i+1,ilCols+1), olUrno ) ;
			i++;

			olGrdTable->FindRow(olUrno,	0,TRUE,TRUE);

			long ilUrno=atol(olUrno);
			omGrpUrnoMain.SetAt(ilUrno,ilUrno);
		}
		m_iGroupRows = ilDataCount = olGrdGroup->GetRowCount();
		
		olGrdTable->LockUpdate(FALSE);
		olGrdTable->Redraw( ); 
		olGrdGroup->LockUpdate(FALSE);
		olGrdGroup->Redraw( );
		end = GetTickCount();
		end -= start;
		//*** MANIPULATE GRID ***
		//olGrdGroup->SetStyleRange(CGXRange(0,0,ilDataCount,ilCols),CGXStyle().SetReadOnly(TRUE));
		if (ilDataCount>0)
			olGrdGroup->SetStyleRange(CGXRange(1,1,ilDataCount,ilCols+1),CGXStyle().SetControl(GX_IDS_CTRL_STATIC));

		//  select allocation unit
		CString olUgty, olAlod;
		int ilSel = -1;
		olUrno.Format ("%lu", mActGrpUrno );
		if ( bmAllocCBEnabled && 
			 ogBCD.GetField ( "SGR", "URNO", olUrno, "UGTY", olUgty ) && 
			 !olUgty.IsEmpty() && 
			 ogBCD.GetField ( "ALO", "URNO", olUgty, "ALOD", olAlod ) )
			 ilSel = omAllocCB.FindStringExact (-1, olAlod);
		omAllocCB.SetCurSel ( ilSel );

		//  select template
		CString olUtpl, olTnam;
		if ( bmTplCBEnabled )
		{	
			if ( ogBCD.GetField ( "SGR", "URNO", olUrno, "UTPL", olUtpl ) && 
				 !olUtpl.IsEmpty() ) 
				ogBCD.GetField ( "TPL", "URNO", olUtpl, "TNAM", olTnam ) ;
			else
				olTnam = GetString ( IDS_UNIVERSAL );
			ilSel = omTplCB.FindStringExact (-1, olTnam);
			omTplCB.SetCurSel ( ilSel );
		}
	}
	AfxGetApp()->DoWaitCursor(-1);

	return 0;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGroupsDialog::DeleteGroupMember()
{
	//*** DELETE ACTUAL GROUP MEMBER	

	if (mActGrpUrno!=0)
	{

		olGrdGroup->mDirty=TRUE;
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//*** PRIVILEGES
		if (ogPrivList.GetStat("GROUPFUNCTIONS")=='1')
		{
			//GetDlgItem(IDCANCELCHANGE)->EnableWindow(TRUE);
			//GetDlgItem(IDSAVE)->EnableWindow(TRUE);
			DecideControls ();   // hag 20011203
		
			//*** FIND ACTUAL ROW
			CPoint pt;	
			ROWCOL /*iCol,*/iRow,ilRowCount, ilSelRows;
		
			//  hag: Bereich der selektierten Zeilen bestimmen
			CRowColArray olSelRows;
			ilSelRows = olGrdGroup->GetSelectedRows ( olSelRows );
			for ( ROWCOL i=ilSelRows; i>0; i-- )
			{
				iRow = olSelRows[i-1];
				ilRowCount = olGrdGroup->GetRowCount();

				if ( (iRow>0) && (iRow<=ilRowCount) )
				{
					//***GET URNO AND ADD IT TO THE DEL-MAP
					CString clUrno=olGrdGroup->GetValueRowCol(iRow,olGrdGroup->GetColCount());
					char  clDmy[15];
					sprintf(clDmy,"%s",clUrno);
					long ilUrno=atol(clDmy);

					//***ADD TO DEL-MAP
					omGrpUrnoDel.SetAt(ilUrno,ilUrno);
					//***REMOVE FROM URNO-MAP
					omGrpUrnoMain.RemoveKey(ilUrno);

					olGrdGroup->RemoveRows(iRow,iRow);

					olGrdTable->FindRow(clUrno,0,FALSE,FALSE);

					olGrdGroup->SetCurrentCell(0,0);

					//*** CORRECT NUMBER OF ENTRIES ***
					CString clNo;;
					clNo.Format(GetString(IDS_GRP0013),olGrdGroup->GetRowCount() );
					m_NoGroup.SetWindowText(clNo);

				}
			}
		}
	}
	return 0;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnCancelchange() 
{
	//*** LOAD GROUP AGAIN AND KILL THE CHANGES
	if (olGrdGroup->mDirty==TRUE)
	{
		if (MessageBox(GetString(IDS_GRP0011),GetString(AFX_IDS_APP_TITLE),MB_ICONQUESTION|MB_YESNO)==IDYES)	
		{
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//*** PRIVILEGES
			/*
			if (ogPrivList.GetStat("GROUPFUNCTIONS")=='1')
			{
				GetDlgItem(IDCANCELCHANGE)->EnableWindow(FALSE);
				GetDlgItem(IDSAVE)->EnableWindow(FALSE);
			}*/
			olGrdGroup->mDirty=FALSE;
			DecideControls ();   // hag 20011203
			FillGridGroups();
		}
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CGroupsDialog::AddGroupMember()
{
	//*** ADD A NEW GROUP MEMBER	
	
	if (mActGrpUrno!=0)
	{
		olGrdGroup->mDirty=TRUE;
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//*** PRIVILEGES
		if (ogPrivList.GetStat("GROUPFUNCTIONS")=='1')
		{
			//  GetDlgItem(IDCANCELCHANGE)->EnableWindow(TRUE);
			//  GetDlgItem(IDSAVE)->EnableWindow(TRUE);
			DecideControls ();   // hag 20011203

			//*** FIND ACTUAL ROW
			CPoint pt;	
			ROWCOL iRowTable;	//,iCol
			//olGrdTable->GetCurrentCell(iRowTable,iCol);

			//  hag: Bereich der selektierten Zeilen bestimmen
			CRowColArray olSelRows;
			ROWCOL ilRowCount = olGrdTable->GetSelectedRows ( olSelRows );
			for ( ROWCOL i=0; i<ilRowCount; i++ )
			{
				iRowTable = olSelRows[i];
				//***GET URNO AND ADD IT TO THE DEL-MAP
				CString clUrno=olGrdTable->GetValueRowCol(iRowTable,olGrdTable->GetColCount());
				char  clDmy[15];
				sprintf(clDmy,"%s",clUrno);
				long ilUrno=atol(clDmy);

				long ilDmy;

				if (!(omGrpUrnoMain.Lookup(ilUrno,ilDmy)))
				{
					//***ADD TO NEW/ MAP
					omGrpUrnoNew.SetAt(ilUrno,ilUrno);
					omGrpUrnoMain.SetAt(ilUrno,ilUrno);	

					//***REMOVE FROM DEL-MAP
					omGrpUrnoDel.RemoveKey(ilUrno);

					long iCols=olGrdGroup->GetColCount();
					long iRows=olGrdGroup->GetRowCount()+1;
					//olGrdGroup->SetRowCount(iRows);
					olGrdGroup->InsertRows(iRows,1);
					for (int iC=1;iC<=iCols;iC++)
					{
						olGrdGroup->SetValueRange(CGXRange(iRows,iC),olGrdTable->GetValueRowCol(iRowTable,iC));
						
					}

					//olGrdGroup->UpdateInsertRows(iRows,1,GX_UPDATENOW);
					olGrdGroup->SetStyleRange(CGXRange(iRows,1,iRows,iCols),CGXStyle().SetInterior(YELLOW));
					olGrdGroup->SetStyleRange(CGXRange(iRows,1,iRows,iCols),CGXStyle().SetControl(GX_IDS_CTRL_STATIC));

					
					//***MARK THE NEW ENTRY
					olGrdGroup->FindRow(clUrno,0,TRUE,TRUE);
					olGrdTable->FindRow(clUrno,0,TRUE,TRUE);

					//*** CORRECT NUMBER OF ENTRIES ***
					CString clNo;;
					clNo.Format(GetString(IDS_GRP0013),(olGrdGroup->GetRowCount()));
					m_NoGroup.SetWindowText(clNo);

				}
				olGrdTable->SelectRange( CGXRange().SetRows(iRowTable), FALSE, TRUE );
			}
			//olGrdTable->Redraw();
		}
	}
	return 0;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnSave() 
{
	//***SAVE ALL THE CHANGES
	POSITION olPos;
	long ilKey,ilValue;
	BOOL blOk;
	CString olUval, olUsgr, olUrno, olUgty, olUtpl;
	int     i, iSel=-1, ilSgmUvalIdx, ilSgmUrnoIdx;
	CUIntArray olToReinsert;
	CCSPtrArray<RecordSet> olGrpMembers;
	
	AfxGetApp()->DoWaitCursor(1);

	//***FIRST THE DELETE PART

	olUsgr.Format ( "%ld", mActGrpUrno );
	ogBCD.GetRecords("SGM", "USGR", olUsgr, &olGrpMembers );
	ilSgmUvalIdx = ogBCD.GetFieldIndex("SGM","UVAL");
	ilSgmUrnoIdx = ogBCD.GetFieldIndex("SGM","URNO");

	for ( i=0; i<olGrpMembers.GetSize(); i++ )
	{
		olUval = olGrpMembers[i].Values[ilSgmUvalIdx];
		olUrno = olGrpMembers[i].Values[ilSgmUrnoIdx];

		if ( (sscanf ( olUval, "%ld", &ilKey ) >= 1 ) && 
			 omGrpUrnoMain.Lookup( ilKey,ilValue) )
		{	/* Group member remains in group */
			blOk = omGrpUrnoMain.RemoveKey ( ilKey );
			olToReinsert.Add ( ilKey );
		}
		else
		{	/* SGM-record no longer in group or invalid */
			if ( !olUrno.IsEmpty () )
				blOk = ogBCD.DeleteRecord ( "SGM", "URNO", olUrno );
		}
	}
	olGrpMembers.DeleteAll ();
	//*** END DELETE PART *************

	/*  all keys still in omGrpUrnoMain have to be added */
	olPos=omGrpUrnoMain.GetStartPosition();
	while (olPos!=NULL)
	{
		omGrpUrnoMain.GetNextAssoc(olPos,ilKey,ilValue);

		RecordSet olRecord(ogBCD.GetFieldCount("SGM"));

		olUval.Format("%ld",ilKey);
		olRecord.Values[ogBCD.GetFieldIndex("SGM","UVAL")] = olUval;
		olRecord.Values[ogBCD.GetFieldIndex("SGM","USGR")]=olUsgr;
		olRecord.Values[ogBCD.GetFieldIndex("SGM","TABN")]=m_cTable;
		ogBCD.InsertRecord("SGM",olRecord,FALSE);
	}

	ogBCD.Save("SGM");

	//*** END NEW-PART *********************

	omGrpUrnoDel.RemoveAll();
	omGrpUrnoNew.RemoveAll();
	
	/* reinsert all temporarily deleted keys into omGrpUrnoMain */
	for ( i=0; i<olToReinsert.GetSize(); i++ )
	{
		ilKey = olToReinsert[i];
		omGrpUrnoMain.SetAt(ilKey,ilKey);
	}
	
	if ( bmAllocCBEnabled )
	{
		iSel = omAllocCB.GetCurSel ();
		if ( iSel >= 0 )
			olUgty.Format ( "%lu", omAllocCB.GetItemData(iSel) );
		else
			olUgty.Empty ();
		ogBCD.SetField("SGR", "URNO", olUsgr, "UGTY", olUgty, true );
	}
	if ( bmTplCBEnabled )
	{
		olUtpl = GetActTplUrno ( );
		ogBCD.SetField("SGR", "URNO", olUsgr, "UTPL", olUtpl, true );
	}
	if ( m_cTable == "EQU" ) 
		ogBCD.SetField("SGR", "URNO", olUsgr, "STYP", omSubtypeUrno, true );

	olGrdGroup->ClearMarks();
	olGrdGroup->mDirty=FALSE;

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//*** PRIVILEGES
	DecideControls ();   // hag 20011203

	//FillComboGroups(m_cTableLong);

	AfxGetApp()->DoWaitCursor(-1);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnDelete() 
{
	//*** DELETE A COMPLETE GROUP
	CString clDmy,clDmy2;
	CString olUval, olUsgr, olUrno;
	m_ComboGroups.GetWindowText(clDmy2);
	clDmy.Format(GetString(IDS_GRP0012),clDmy2);
		
	if (MessageBox(clDmy,GetString(AFX_IDS_APP_TITLE),MB_ICONEXCLAMATION|MB_YESNOCANCEL)==IDYES)
	{
		AfxGetApp()->DoWaitCursor(1);

		//***DELETE GROUP MEMBERS
		POSITION olPos;
		long ilKey,ilValue;
		olPos=omGrpUrnoMain.GetStartPosition();
		while (olPos!=NULL)
		{
			omGrpUrnoMain.GetNextAssoc(olPos,ilKey,ilValue);
			
			//  hag991201	DeleteByWhere l�st keine Broadcasts aus deshalb ersetzt
			//clDmy.Format("WHERE UVAL=%d AND USGR=%d",ilKey,mActGrpUrno);
			//ogBCD.DeleteByWhere("SGM",clDmy);

			olUval.Format ( "%d", ilKey );
			olUsgr.Format ( "%d", mActGrpUrno );
			olUrno = ogBCD.GetFieldExt( "SGM", "UVAL", "USGR", olUval, olUsgr, "URNO" );
			if ( !olUrno.IsEmpty () )
				ogBCD.DeleteRecord ( "SGM", "URNO", olUrno, true );

		}

		//***DELETE GROUP
		//  hag991201	DeleteByWhere l�st keine Broadcasts aus deshalb ersetzt
		//clDmy.Format("WHERE URNO=%d AND TABN='%s'",mActGrpUrno,m_cTableLong);
		//ogBCD.DeleteByWhere("SGR",clDmy);
		olUrno.Format ("%d", mActGrpUrno );
		ogBCD.DeleteRecord ( "SGR", "URNO", olUrno, true );

		mActGrpUrno=0;
		FillComboGroups(m_cTableLong);

		olGrdGroup->SetRowCount(0);

		olGrdTable->ClearMarks();

		GetDlgItem(IDDELETE)->EnableWindow(FALSE);
		
		AfxGetApp()->DoWaitCursor(-1);

		//*** CORRECT NUMBER OF ENTRIES TO CERO***
		CString clNo;;
		clNo.Format(GetString(IDS_GRP0013),0);
		m_NoGroup.SetWindowText(clNo);
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnAddmember() 
{
	//*** BUTTON ADDGROUPMEMBER
	AddGroupMember();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CGroupsDialog::OnDeletemember() 
{
	//*** BUTTON DELETEGROUPMEMBER	
	DeleteGroupMember();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void CGroupsDialog::OnTree() 
{

	CTreeDialog dlg;
	dlg.DoModal();
	
}

//---------------------------------------------------------------------------

void CGroupsDialog::IniAlocCB ()
{
	CString olAlod, olUrno;
	int ilCount, i;
	
	ogBCD.SetObject("ALO", "ALOD,URNO");
	ogBCD.Read("ALO", "WHERE ALOT='1'" );
	ilCount = ogBCD.GetDataCount("ALO");
	for (i = 0; i < ilCount; i++)
	{
		olAlod = ogBCD.GetField("ALO", i, "ALOD");
		olUrno = ogBCD.GetField("ALO", i, "URNO");
		int ilPos = omAllocCB.AddString(olAlod);
		omAllocCB.SetItemData(ilPos, atol(olUrno));
	}
}

void CGroupsDialog::OnSelendokUgtyCb() 
{
	// TODO: Add your control notification handler code here
	olGrdGroup->mDirty=TRUE;	
	//GetDlgItem(IDCANCELCHANGE)->EnableWindow(TRUE);
	//GetDlgItem(IDSAVE)->EnableWindow(TRUE);
	DecideControls ();   // hag 20011203
}

void CGroupsDialog::OnKillfocusUgtyCb() 
{
	// TODO: Add your control notification handler code here
	int ilSel = omAllocCB.GetCurSel ();
	omAllocCB.SetCurSel (ilSel);
	//GetDlgItem(IDCANCELCHANGE)->EnableWindow(TRUE);
	//GetDlgItem(IDSAVE)->EnableWindow(TRUE);
}

void CGroupsDialog::OnAppAbout() 
{
	// TODO: Add your control notification handler code here
	CString olUrno;
	if ( (m_ComboGroups.GetCurSel() >= 0) && (mActGrpUrno > 0) )
		olUrno.Format("%ld", mActGrpUrno );
	CAboutDlg olDlg(this);
	
	olDlg.DoModal(olUrno);
}

void CGroupsDialog::IniTplCB ()
{
	CString olTnam, olUrno;
	int ilCount, i, ilPos;
	
	ilCount = ogBCD.GetDataCount("TPL");
	if(ogPrivList.GetStat("*Universal") != '-')
	{
		//ilPos = omTplCB.AddString("*Universal");
		ilPos = omTplCB.AddString( GetString(IDS_UNIVERSAL) );
		omTplCB.SetItemData(ilPos, 0 );
	}
	for (i = 0; i < ilCount; i++)
	{
		olTnam = ogBCD.GetField("TPL", i, "TNAM");
		olUrno = ogBCD.GetField("TPL", i, "URNO");
		if(ogPrivList.GetStat(olTnam) != '-')
		{
			ilPos = omTplCB.AddString(olTnam);
			omTplCB.SetItemData(ilPos, atol(olUrno));
		}
	}
	if ( !omStartTpl.IsEmpty() )
	{
		ilPos = omTplCB.FindStringExact ( -1, omStartTpl );
	}
	else
		ilPos = min (0, omTplCB.GetCount()-1);
	if ( ilPos >= 0 )
		omTplCB.SetCurSel( ilPos );
}


void CGroupsDialog::OnSelendokUtplCb() 
{
	// TODO: Add your control notification handler code here
	int	 ilRet, i,iSel=-1;
	char	clStat;
	CWnd *polWnd = GetDlgItem( IDADDMEMBER );

	if ( polWnd && polWnd->IsWindowVisible() && polWnd->IsWindowEnabled() &&
		 (mActGrpUrno>0) )
	{	// Modifications allowed
		CString olTnam = "*Universal";
		CString olTpl = GetActTplUrno ();
		if ( !olTpl.IsEmpty() )
			olTnam = ogBCD.GetField ( "TPL", "URNO", olTpl, "TNAM" );
		//  Modifications in new Template also allowed ?
		clStat = ogPrivList.GetStat(olTnam) ;
		if ( (clStat != '0') && (clStat != '-') )	
		{
			ilRet = MessageBox ( GetString (IDS_CHANGE_TPL), NULL, 
								 MB_ICONQUESTION | MB_YESNO );
			if ( ilRet == IDYES )
			{
				OnSave();
			}
			else 
				mActGrpUrno = 0;
		}
		else
			mActGrpUrno = 0;
	}
	else
		mActGrpUrno = 0;
	FillComboGroups(m_cTableLong);
	if ( mActGrpUrno > 0 )
	{
		for ( i=0; i<m_oGrpUrnos.GetSize(); i++ )
			if ( m_oGrpUrnos[i] == mActGrpUrno )
				iSel = i;
		m_ComboGroups.SetCurSel (iSel);
	}
	else
	{
		olGrdGroup->SetRowCount(0);
		olGrdTable->ClearMarks();
	}
	DecideControls ();
}


CString CGroupsDialog::GetActTplUrno ()
{
	CString	olUtpl;

	if ( bmTplCBEnabled )
	{
		DWORD	llUtpl = 0;
		int		iSel;
		iSel = omTplCB.GetCurSel ();
		if ( iSel >= 0 )
			llUtpl = omTplCB.GetItemData(iSel);
		if ( llUtpl > 0 )
			olUtpl.Format ( "%lu", llUtpl );
	}
	return olUtpl;
}


void CGroupsDialog::DecideControls ()
{
	char	clStat[3];
	CString olTplUrno, olTnam="*Universal";
	char	clNeuStat, clDeleteStat;
	UINT	ilCtrlIds[6] = { IDNEU, IDDELETE, IDCANCELCHANGE, IDSAVE,
							  IDADDMEMBER, IDDELETEMEMBER};
	int     ilSel, i;

	memset ( clStat, 0, 3 );
	clStat[0] = ogPrivList.GetStat("GROUPFUNCTIONS");
	if ( bmTplCBEnabled )
	{
		olTplUrno = GetActTplUrno ();
		if ( !olTplUrno.IsEmpty() )
			olTnam = ogBCD.GetField ( "TPL", "URNO", olTplUrno, "TNAM" );
		clStat[1] = ogPrivList.GetStat(olTnam) ;
	}

	if ( strchr(clStat, '-' ) )		//  all controls have to be hidden
	{
		for ( i=0; i<6; i++ )
			SetCtrlState ( ilCtrlIds[i], '-' );
	}
	else
	{
		clNeuStat = ogPrivList.GetStat("IDNEU") ;
		clDeleteStat = ogPrivList.GetStat("IDDELETE") ;
		if ( strchr(clStat, '0' ) )
		{
			for ( i=2; i<6; i++ )
				SetCtrlState ( ilCtrlIds[i], '0' );
			//  IDNEU and IDDELETE have their own BDPS_SEC-entries
			SetCtrlState ( IDNEU, clNeuStat == '-' ? '-' : '0' );
			SetCtrlState ( IDDELETE, clDeleteStat == '-' ? '-' : '0' );
		}
		else	//  Groupffunctions and Template active
		{
			SetCtrlState ( IDCANCELCHANGE, olGrdGroup->mDirty ? '1' : '0' );
			SetCtrlState ( IDSAVE, olGrdGroup->mDirty ? '1' : '0' );
			ilSel = m_ComboGroups.GetCurSel ();	// actual group ?
			if ( ( ilSel < 0 ) && ( clDeleteStat == '1' ) )			
				clDeleteStat = '0';
			SetCtrlState ( IDDELETE, clDeleteStat );
			SetCtrlState ( IDADDMEMBER, ( ilSel < 0 )  ? '0' : '1' );
			SetCtrlState ( IDDELETEMEMBER, ( ilSel < 0 )  ? '0' : '1' );

			ilSel = m_ComboTables.GetCurSel ();	// actual table
			if ( ( ilSel < 0 ) && ( clNeuStat == '1' ) )			
				clNeuStat = '0';
			SetCtrlState ( IDNEU, clNeuStat );

		}
	}
}


bool CGroupsDialog::SetCtrlState ( UINT ipCtrlId, char cpState )
{
	CWnd *polCtrl;
	bool blRet=false;
	if ( polCtrl = GetDlgItem( ipCtrlId ) )
	{
		polCtrl->ShowWindow ( cpState!='-' ? SW_SHOW : SW_HIDE );
		polCtrl->EnableWindow ( cpState=='1' ? TRUE : FALSE );
		blRet = true;
	}
	return blRet;
}

void CGroupsDialog::OnCloseupCombotypes() 
{
	// TODO: Add your control notification handler code here
	CString olUrno, olOldSubtype = omSubtypeUrno;
	int ilRet = IDNO, ilSel;
	POSITION olPos;
	long ilUrno,ilValue;

	if ( GetSelectedTypeUrno ( omSubtypeUrno ) )
	{
		if ( olOldSubtype != omSubtypeUrno )
		{
			/*	hag20020429: No change of subtype shall be possible 

			if ( mActGrpUrno > 0 )
				ilRet = MessageBox ( GetString(IDS_CHANGE_TYPE), NULL,
									 MB_YESNOCANCEL|MB_ICONQUESTION );
			*/
			switch ( ilRet )
			{
				case IDYES:	
					// Delete Group members and save Group with new STYP	
					olPos=omGrpUrnoMain.GetStartPosition();
					while (olPos!=NULL)
					{	//	keep all members of group as to be deleted
						omGrpUrnoMain.GetNextAssoc(olPos,ilUrno,ilValue);
						if ( ilUrno > 0 )
							omGrpUrnoDel.SetAt(ilUrno,ilUrno);
					}
					omGrpUrnoMain.RemoveAll();
					omGrpUrnoNew.RemoveAll();
					OnSave();					
					FillGridGroups();
					olGrdTable->ClearMarks();
					break;
				case IDNO:
					//  Switch to new subtype don't modify actual group
					mActGrpUrno = 0;
					FillComboGroups(m_cTableLong);
					olGrdGroup->SetRowCount(0);
					olGrdTable->ClearMarks();
					olUrno.Format(GetString(IDS_GRP0013),0 );
					m_NoGroup.SetWindowText(olUrno);
					break;
				case IDCANCEL:
					//  select old subtype again, don't modify anything
					ilUrno = atoi ( olOldSubtype );
					for ( int i=0; (ilSel<0) && (i<omComboTypes.GetCount() ); i++ )
					{
						ilValue = omComboTypes.GetItemData ( i );
						if (ilValue == ilUrno )
							ilSel = i;
					}
					omComboTypes.SetCurSel ( ilSel );
					omSubtypeUrno = olOldSubtype;
					break;
			}

			

			FillGridTables(m_cTable);
		}
	}
				
}
   
void CGroupsDialog::InitializeComboTypes()
{
	if ( m_cTableLong != "EQU" )
	{
		SetCtrlState ( IDC_COMBOTYPES, '-' );
		SetCtrlState ( IDC_TYPE, '-' );
		omSubtypeUrno.Empty();	//  Subtype not used
	}
	else
	{
		SetCtrlState ( IDC_COMBOTYPES, '1' );
		SetCtrlState ( IDC_TYPE, '1' );
		FillComboTypes();
		GetSelectedTypeUrno ( omSubtypeUrno );
	}
}

//*** FILL Combobox IDC_COMBOTYPES WITH EXISTING Equipment Types
void CGroupsDialog::FillComboTypes()
{
	
	CString olUrno, olName;
	int		ilIdx, ilDataCount;
	long	ilUrno;

	omComboTypes.ResetContent();
	
	if ( !ogBCD.SetObject("EQT") )
		return;

	ogBCD.Read("EQT");
	ogBCD.SetSort("EQT","NAME+",true);

	ilDataCount = ogBCD.GetDataCount("EQT"); //number of rows

	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		ogBCD.GetRecord("EQT", i,  olRecord);
		olName = olRecord.Values[ogBCD.GetFieldIndex("EQT","NAME")];
		olUrno = olRecord.Values[ogBCD.GetFieldIndex("EQT","URNO")];
		ilUrno = atol ( olUrno );
		if ( ilUrno <= 0 )
			continue;
		ilIdx = omComboTypes.AddString (olName);
		if (ilIdx >= 0 )
			omComboTypes.SetItemData ( ilIdx, ilUrno );
	}
	if ( omComboTypes.GetCount() > 0 )
		omComboTypes.SetCurSel ( 0 );
	ogBCD.RemoveObject("EQT");
}

/*   Get Urno of Selected EQTTAB-record of Combobox IDC_COMBOTYPES */
bool CGroupsDialog::GetSelectedTypeUrno ( CString &ropSubtypeUrno )
{
	int ilSel = -1;
	bool blOk = false;

	ropSubtypeUrno.Empty();
	if ( omComboTypes.IsWindowVisible() )
		ilSel = omComboTypes.GetCurSel();
	if ( ilSel >= 0 )
	{
		ropSubtypeUrno.Format ( "%ld", omComboTypes.GetItemData( ilSel ) );
		blOk = true;
	}

	return blOk;
}
/*
void CGroupsDialog::OnSave() 
{
	//***SAVE ALL THE CHANGES

	AfxGetApp()->DoWaitCursor(1);

	//***FIRST THE DELETE PART
	POSITION olPos;
	long ilKey,ilValue;
	bool blOk;
	//  CString clDmy;
	CString olUval, olUsgr, olUrno, olUgty, olUtpl;
	int     iSel=-1;

	olPos=omGrpUrnoDel.GetStartPosition();

	CString clWhere;
	clWhere.Format("WHERE USGR='%d'",mActGrpUrno);
	ogBCD.SetObject("SGM");
	ogBCD.Read("SGM",clWhere);

	olUsgr.Format ( "%d", mActGrpUrno );
	while (olPos!=NULL)
	{
		omGrpUrnoDel.GetNextAssoc(olPos,ilKey,ilValue);

		//  hag991201	DeleteByWhere l�st keine Broadcasts aus deshalb ersetzt
		//clDmy.Format("WHERE UVAL=%d AND USGR=%d",ilKey,mActGrpUrno);
		//ogBCD.DeleteByWhere("SGM",clDmy);
		olUval.Format ( "%d", ilKey );
		//olUsgr.Format ( "%d", mActGrpUrno );
		olUrno = ogBCD.GetFieldExt( "SGM", "UVAL", "USGR", olUval, olUsgr, "URNO" );
		if ( !olUrno.IsEmpty () )
			blOk = ogBCD.DeleteRecord ( "SGM", "URNO", olUrno, true );

	}
	omGrpUrnoDel.RemoveAll();
	//*** END DELETE PART *************

	//***NEW PART 
	//RecordSet olRecord(ogBCD.GetFieldCount("SGM"));
	olPos=omGrpUrnoNew.GetStartPosition();
	
	while (olPos!=NULL)
	{
		//afxDump << "ogBCD.GetFieldCount(SGM):" << ogBCD.GetFieldCount("SGM") <<"\n";
		//afxDump << "ogBCD.GetFieldIndex(SGM,VALU):" << ogBCD.GetFieldIndex("SGM","VALU") <<"\n";

		RecordSet olRecord(ogBCD.GetFieldCount("SGM"));
		omGrpUrnoNew.GetNextAssoc(olPos,ilKey,ilValue);
		olUval.Format("%d",ilKey);
		//*** 25.08.99 SHA ***
		//olRecord.Values[ogBCD.GetFieldIndex("SGM","VALU")] = clDmy;
		olRecord.Values[ogBCD.GetFieldIndex("SGM","UVAL")] = olUval;
		//olUrno.Format("%d",mActGrpUrno);
		olRecord.Values[ogBCD.GetFieldIndex("SGM","USGR")]=olUsgr;
		//*** 25.08.99 SHA ***
		olRecord.Values[ogBCD.GetFieldIndex("SGM","TABN")]=m_cTable;
		ogBCD.InsertRecord("SGM",olRecord,FALSE);
	}
	omGrpUrnoNew.RemoveAll();
	//*** END NEW-PART *********************

	ogBCD.Save("SGM");

	if ( bmAllocCBEnabled )
	{
		iSel = omAllocCB.GetCurSel ();
		if ( iSel >= 0 )
			olUgty.Format ( "%lu", omAllocCB.GetItemData(iSel) );
		else
			olUgty.Empty ();
		ogBCD.SetField("SGR", "URNO", olUsgr, "UGTY", olUgty, true );
	}
	if ( bmTplCBEnabled )
	{
		olUtpl = GetActTplUrno ( );
		ogBCD.SetField("SGR", "URNO", olUsgr, "UTPL", olUtpl, true );
	}
	if ( m_cTable == "EQU" ) 
		ogBCD.SetField("SGR", "URNO", olUsgr, "STYP", omSubtypeUrno, true );

	olGrdGroup->ClearMarks();
	olGrdGroup->mDirty=FALSE;

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	DecideControls ();   // hag 20011203

	AfxGetApp()->DoWaitCursor(-1);
}
*/