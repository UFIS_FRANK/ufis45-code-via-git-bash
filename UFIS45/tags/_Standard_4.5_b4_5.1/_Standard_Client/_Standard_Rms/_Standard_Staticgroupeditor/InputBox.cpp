// InputBox.cpp : implementation file
//
#include <tchar.h>

#include "stdafx.h"
#include "Grp.h"
#include "InputBox.h"
#include "BasicData.h"

//OWN
#include "ccsglobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInputBox dialog


CInputBox::CInputBox(CWnd* pParent /*=NULL*/)
	: CDialog(CInputBox::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInputBox)
	//}}AFX_DATA_INIT
}


void CInputBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInputBox)
	DDX_Control(pDX, IDC_INPUT, m_cInput);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInputBox, CDialog)
	//{{AFX_MSG_MAP(CInputBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInputBox message handlers

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int CInputBox::Set(CString cTitle, CString cText, CString cDefault, long iLen)
{
	m_Title=	cTitle;
	m_Text=		cText;
	m_Default=	cDefault;
	m_Length =	iLen;

	return 0;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL CInputBox::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(m_Title);
	GetDlgItem(IDC_TEXT)->SetWindowText(m_Text);
	GetDlgItem(IDC_INPUT)->SetWindowText(m_Default);
	GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_CANCEL));
	GetDlgItem(IDOK)->SetWindowText(GetString(IDS_OK));
	
	m_cInput.LimitText(m_Length);
	
	//GetDlgItem(IDC_TEXT)->SetFont(&ogCourier_Regular_10);
	//GetDlgItem(IDC_INPUT)->SetFont(&ogCourier_Regular_10);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CString CInputBox::GetInput()
{
	return m_Text;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CInputBox::OnOK()
{
	CString olUrno;
	GetDlgItem(IDC_INPUT)->GetWindowText(m_Text);	
	//  don't allow dot in group name for group with APPL='RULE_AFT'
	if ( ( m_Text.Find ( '.' ) >= 0 ) && (ogCallingAppName == "RULE_AFT") )
	{
		MessageBox ( GetString(IDS_NO_DOT_IN_NAME), NULL, MB_ICONEXCLAMATION ); 
		return;
	}
	if ( (ogCallingAppName == "RULE_AFT") && !NameAllowed (m_Text) )
	{
		return;
	}
	//  Test: is new name already used for the actuall APPL
	olUrno = ogBCD.GetFieldExt("SGR", "GRPN", "APPL", m_Text, ogCallingAppName, "URNO" );
	if ( !olUrno.IsEmpty() )
	{
		MessageBox ( GetString(IDS_BAD_NAME), NULL, MB_ICONEXCLAMATION ); 
		return;
	}
	CDialog::OnOK();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CInputBox::OnCancel()
{
	m_Text="";
	CDialog::OnCancel();
}


bool CInputBox::NameAllowed ( CString opText )
{
	CString olCedaString, olOut;

	olCedaString = opText;
	ogBasicData.MakeCedaString ( olCedaString );

	char * ps = _tcsspnp ( opText.GetBuffer(0), olCedaString.GetBuffer(0) );
	if ( ps )
	{
		olOut = GetString ( IDS_INVALID_CHAR );
		olOut.Format ( olOut, ps[0] );
		MessageBox ( olOut, NULL, MB_ICONEXCLAMATION ); 
		return false;
	}
	else
		return true;

}

