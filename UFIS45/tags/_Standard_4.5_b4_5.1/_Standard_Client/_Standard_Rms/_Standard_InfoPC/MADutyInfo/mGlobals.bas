Attribute VB_Name = "mGlobals"
Option Explicit

' globals concerning the connection to CEDA
Public sAppl As String
Public sHopo As String
Public sServer As String
Public sJobTypes As String
Public sTableExt As String
Public sConnectType As String

' globals holding display-information
Public strLastError As String
Public strURNO As String
Public strPENO As String
Public strFINM As String
Public strLANM As String
Public strEINHEIT As String
Public strTTGT As String
Public strFLNO As String
Public strDETY As String
Public strLOGO As String

'remember the UTC-offset
Public sgUTCOffsetHours As Single

