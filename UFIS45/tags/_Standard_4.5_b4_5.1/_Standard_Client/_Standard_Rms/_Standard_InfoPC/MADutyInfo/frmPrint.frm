VERSION 5.00
Begin VB.Form frmPrint 
   BackColor       =   &H80000009&
   Caption         =   "Druck-Formular"
   ClientHeight    =   8970
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11760
   Icon            =   "frmPrint.frx":0000
   LinkTopic       =   "Form3"
   ScaleHeight     =   8970
   ScaleWidth      =   11760
   StartUpPosition =   3  'Windows Default
   Tag             =   "171"
   Visible         =   0   'False
   Begin VB.TextBox txtMitarbeiter 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1050
      Left            =   3015
      MultiLine       =   -1  'True
      TabIndex        =   17
      Text            =   "frmPrint.frx":030A
      Top             =   1935
      Width           =   5745
   End
   Begin VB.Timer Timer1 
      Interval        =   3000
      Left            =   7680
      Top             =   3360
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000009&
      BorderStyle     =   0  'None
      Height          =   1710
      Left            =   195
      Picture         =   "frmPrint.frx":0318
      ScaleHeight     =   1710
      ScaleWidth      =   3270
      TabIndex        =   15
      Top             =   15
      Width           =   3270
   End
   Begin VB.Label lblDate 
      BackColor       =   &H80000009&
      Caption         =   "24.09.2001"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8670
      TabIndex        =   16
      Top             =   1320
      Width           =   2625
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   60
      X2              =   11370
      Y1              =   1845
      Y2              =   1845
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Mitarbeiter:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1290
      TabIndex        =   14
      Top             =   1935
      Width           =   1665
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Einsatztyp:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1245
      TabIndex        =   13
      Top             =   3060
      Width           =   1665
   End
   Begin VB.Label lblEinsatztyp 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Durchgang"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   3015
      TabIndex        =   12
      Top             =   3060
      Width           =   3645
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Zeit von:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1245
      TabIndex        =   11
      Top             =   3510
      Width           =   1665
   End
   Begin VB.Label lblZeitVon 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "12:45"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   3015
      TabIndex        =   10
      Top             =   3510
      Width           =   3615
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Zeit bis:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1245
      TabIndex        =   9
      Top             =   3960
      Width           =   1665
   End
   Begin VB.Label lblZeitBis 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "13:25"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   3015
      TabIndex        =   8
      Top             =   3960
      Width           =   3585
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Einheit:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1245
      TabIndex        =   7
      Top             =   4410
      Width           =   1665
   End
   Begin VB.Label lblEinheit 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Gate: A27"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   3015
      TabIndex        =   6
      Top             =   4410
      Width           =   3645
   End
   Begin VB.Label Label11 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Flug-Nr.:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1245
      TabIndex        =   5
      Top             =   4860
      Width           =   1665
   End
   Begin VB.Label lblFlugNr 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "LH 4711"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   3015
      TabIndex        =   4
      Top             =   4860
      Width           =   1545
   End
   Begin VB.Label Label13 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Wegezeit:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1215
      TabIndex        =   3
      Top             =   5310
      Width           =   1665
   End
   Begin VB.Label lblWegeZeit 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "5 min"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   3015
      TabIndex        =   2
      Top             =   5310
      Width           =   1545
   End
   Begin VB.Label Label15 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Kommentar:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1245
      TabIndex        =   1
      Top             =   5790
      Width           =   1665
   End
   Begin VB.Label lblKommentar 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Bitte Besen nicht vergessen!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1365
      Left            =   3015
      TabIndex        =   0
      Top             =   5790
      Width           =   5745
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
    'setting the caption of the labels
    Label1.Caption = frmEinsatzdaten.lblEmployee.Caption
    Label3.Caption = frmEinsatzdaten.lblJobType.Caption
    Label5.Caption = frmEinsatzdaten.lblTimeFrom.Caption
    Label7.Caption = frmEinsatzdaten.lblTimeTo.Caption
    Label9.Caption = frmEinsatzdaten.lblDepartement.Caption
    Label11.Caption = frmEinsatzdaten.lblFightNo.Caption
    Label13.Caption = frmEinsatzdaten.lblWaytime.Caption
    Label15.Caption = frmEinsatzdaten.lblComment.Caption

    'setting the content
    txtMitarbeiter.Text = frmEinsatzdaten.txtMitarbeiter.Text
    lblEinsatztyp.Caption = frmEinsatzdaten.lblEinsatztyp.Caption
    lblZeitVon.Caption = frmEinsatzdaten.lblZeitVon.Caption
    lblZeitBis.Caption = frmEinsatzdaten.lblZeitBis.Caption
    lblEinheit.Caption = frmEinsatzdaten.lblEinheit.Caption
    lblFlugNr.Caption = frmEinsatzdaten.lblFlugNr.Caption
    lblWegeZeit.Caption = frmEinsatzdaten.lblWegeZeit.Caption
    lblKommentar.Caption = frmEinsatzdaten.lblKommentar.Caption
    lblDate.Caption = Format(Now, "DD.MM.YYYY hh:mm")

    'look if to hide the waytimes
    If frmEinsatzdaten.blWegezeit = False Then
        Label13.Visible = False
        lblWegeZeit.Visible = False
        Label15.Top = Label13.Top
        lblKommentar.Top = lblWegeZeit.Top
    End If

    'load the logo
    If ExistFile(strLOGO) = True Then
        Picture1.Picture = LoadPicture(strLOGO)
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    If Printers.count > 0 Then
        Me.PrintForm
    Else
        MsgBox "Es ist kein Drucker installiert!", vbInformation, "Kein Drucker"
    End If
    Unload Me
End Sub
