VERSION 5.00
Begin VB.Form frmInfo 
   Caption         =   "150"
   ClientHeight    =   2715
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5745
   Enabled         =   0   'False
   Icon            =   "frmInfo.frx":0000
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   5745
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Tag             =   "150"
   Begin VB.TextBox Text1 
      BackColor       =   &H80000004&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1095
      Left            =   1080
      MultiLine       =   -1  'True
      TabIndex        =   3
      Text            =   "frmInfo.frx":030A
      Top             =   810
      Width           =   4470
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "101"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2085
      TabIndex        =   2
      Tag             =   "101"
      Top             =   2115
      Width           =   1575
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000A&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   120
      Picture         =   "frmInfo.frx":0361
      ScaleHeight     =   735
      ScaleWidth      =   735
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
   Begin VB.Timer Timer1 
      Interval        =   3000
      Left            =   240
      Top             =   1080
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "151"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   238
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   435
      Left            =   1080
      TabIndex        =   0
      Tag             =   "151"
      Top             =   240
      Width           =   4455
   End
End
Attribute VB_Name = "frmInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click()
    Me.Hide
    Unload Me
End Sub

Private Sub Form_Load()
    Timer1.Enabled = True
    LoadResStrings Me
    Text1.Text = "Name: " & strFINM & " " & strLANM
End Sub

Private Sub Timer1_Timer()
    Me.Hide
    Unload Me
End Sub
