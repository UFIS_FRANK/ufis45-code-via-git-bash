// ServiceInfoDlg.cpp : implementation file
//

#include <stdafx.h>

#include <CedaBasicData.h>

#include <CCSGlobl.h>
#include <ServiceCatalog.h>
#include <ServiceInfoDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServiceInfoDlg dialog


CServiceInfoDlg::CServiceInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServiceInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CServiceInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomAktService = 0;
}


void CServiceInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServiceInfoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CServiceInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CServiceInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServiceInfoDlg message handlers

int CServiceInfoDlg::DoModal(RecordSet *popActService) 
{
	// TODO: Add your specialized code here and/or call the base class
	pomAktService = popActService;
	return CDialog::DoModal();
}

BOOL CServiceInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	IniStatics ();
	SetInfoFields ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
} 

void CServiceInfoDlg::IniStatics ()
{
	SetDlgItemText ( IDOK, GetString (IDS_OK) );
	SetDlgItemText ( IDC_CDAT_TXT, GetString (IDS_CREATED_ON) );
	SetDlgItemText ( IDC_LSTU_TXT, GetString (IDS_CHANGED_ON) );
	SetDlgItemText ( IDC_USEC_TXT, GetString (IDS_CREATED_BY) );
	SetDlgItemText ( IDC_USEU_TXT, GetString (IDS_CHANGED_BY) );
	SetDlgItemText ( IDC_CHANGED_GRP, GetString (IDS_LAST_CHANGE) );
	SetWindowText ( GetString (IDS_SERVICE_INFO) );
}

void CServiceInfoDlg::SetInfoFields ()
{
	
	CTime olTime ;
	CString olUrno;
	RecordSet olRecord, *polRec;
	bool    blOk=false;
	if ( !pomAktService )
		return;
	olUrno = pomAktService->Values[ogBCD.GetFieldIndex(pcgTableName,"URNO" )];
	if ( !olUrno.IsEmpty() )
		blOk = ogBCD.GetRecord ( "SER", "URNO", olUrno, olRecord );
	if ( blOk )
		polRec = &olRecord;
	else
		polRec = pomAktService;
	CString olWert = polRec->Values[ogBCD.GetFieldIndex(pcgTableName,"CDAT")];
	if ( !olWert.IsEmpty () )
	{
		olTime = DBStringToDateTime(olWert);
		SetDlgItemText ( IDC_CDAT_DATE, olTime.Format( "%d.%m.%Y" ) );
		SetDlgItemText ( IDC_CDAT_TIME, olTime.Format( "%H:%M" ) );
	}
	olWert = polRec->Values[ogBCD.GetFieldIndex(pcgTableName,"LSTU")];
	if ( !olWert.IsEmpty () )
	{
		olTime = DBStringToDateTime(olWert);
		SetDlgItemText ( IDC_LSTU_DATE, olTime.Format( "%d.%m.%Y" ) );
		SetDlgItemText ( IDC_LSTU_TIME, olTime.Format( "%H:%M" ) );
	}
	SetDlgItemText ( IDC_USEC, polRec->Values[ogBCD.GetFieldIndex(pcgTableName,"USEC")] );
	SetDlgItemText ( IDC_USEU, polRec->Values[ogBCD.GetFieldIndex(pcgTableName,"USEU")] );
	
}
