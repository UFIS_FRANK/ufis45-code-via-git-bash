// ServiceList.h: interface for the CServiceList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICELIST_H__75948FD6_4650_11D3_9349_00001C033B5D__INCLUDED_)
#define AFX_SERVICELIST_H__75948FD6_4650_11D3_9349_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <SelectService.h>

class CServiceList : public CSelectService  
{
public:
	CServiceList(CWnd* pParent = NULL);
	virtual ~CServiceList();

// Dialog Data
	//{{AFX_DATA(CServiceList)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServiceList)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
	public:
	BOOL DoModeless ( const char *pcpTabName, RecordSet *popSelectedService );

protected:
	//{{AFX_MSG(CServiceList)
	afx_msg void OnOK() ;
	afx_msg void OnCancel ();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_SERVICELIST_H__75948FD6_4650_11D3_9349_00001C033B5D__INCLUDED_)

