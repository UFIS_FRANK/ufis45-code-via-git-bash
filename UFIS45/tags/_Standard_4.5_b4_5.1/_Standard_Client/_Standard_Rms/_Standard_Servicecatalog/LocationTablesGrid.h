// LocationTablesGrid.h: interface for the CLocationTablesGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOCATIONTABLESGRID_H__711B95F5_3B5A_11D3_933E_00001C033B5D__INCLUDED_)
#define AFX_LOCATIONTABLESGRID_H__711B95F5_3B5A_11D3_933E_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <GridControl.h>

class CLocationTablesGrid : public CTitleGridControl  
{
public:
	CLocationTablesGrid();
	CLocationTablesGrid( CWnd *popParent, UINT nID, CString opTitle );

	virtual ~CLocationTablesGrid();
//  Data members
private:
	CMapStringToString omLocationsList;
//  Implementation
public:
	void ResetValues ();
	void SelectLocation ( CString &ropLocationTable, int pResNo, 
						  bool bpCallLocationGrid =true ) ;
protected:
	bool CreateComboBoxes ( int ipLenthOfChoice );
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	bool DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) ;
//	BOOL InsertBottomRow ();
	void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);	
};

#endif // !defined(AFX_LOCATIONTABLESGRID_H__711B95F5_3B5A_11D3_933E_00001C033B5D__INCLUDED_)
