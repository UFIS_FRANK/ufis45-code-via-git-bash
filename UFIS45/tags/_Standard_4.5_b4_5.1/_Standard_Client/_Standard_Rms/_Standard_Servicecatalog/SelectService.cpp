// SelectService.cpp : implementation file
//

#include <stdafx.h>

#include <ccsglobl.h>
#include <CedaBasicData.h>

#include <basicdata.h>
#include <utilities.h>
#include <GridControl.h>
#include <ServiceCatalog.h>
#include <ServiceCatalogView.h>
#include <SerListViewerPropSheet.h>
#include <SelectService.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
 

/////////////////////////////////////////////////////////////////////////////
// CSelectService dialog


CSelectService::CSelectService(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectService::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectService)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pcmTabName[0] = '\0';
	pomSelectedService = 0;
	imServiceCount = 0;
	pomServiceList = 0;
	imLastWidth = 0;
	imLastHeight = 0;
}


void CSelectService::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, pomDlgButtons[0]);
	DDX_Control(pDX, IDCANCEL, pomDlgButtons[1]);
	DDX_Control(pDX, ID_FILE_PRINT, pomDlgButtons[2]);
	DDX_Control(pDX, ID_EDIT_FIND, pomDlgButtons[3]);
	DDX_Control(pDX, IDC_VIEW, pomDlgButtons[4]);
	//{{AFX_DATA_MAP(CSelectService)
	DDX_Control(pDX, IDC_VIEW_CB, m_ViewCB);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectService, CDialog)
	//{{AFX_MSG_MAP(CSelectService)
	ON_WM_DESTROY()
	ON_BN_CLICKED(ID_FILE_PRINT, OnFilePrint)
	ON_WM_SIZE()
	ON_BN_CLICKED(ID_EDIT_FIND, OnEditFind)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_CBN_SELENDOK(IDC_VIEW_CB, OnSelendokViewCb)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_GRID_LBUTTONDBLCLK, OnGridLbDblClicked)
	ON_MESSAGE(WM_GRID_LBUTTONCLICK, OnGridLbClicked)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectService message handlers

BOOL CSelectService::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	IniStatics ();
	imServiceCount = ogBCD.GetDataCount(pcmTabName);
	IniGrid ();
	IniWindowPosition ( this, "ServiceList" );
	ogSerListViewer.SetViewerKey("SERLIST");
	UpdateComboBox();

	//m_ViewCB.SetCurSel ( 0 );
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


int CSelectService::DoModal ( const char *pcpTabName, RecordSet *popSelectedService )
{
	strcpy ( pcmTabName, pcpTabName );
	pomSelectedService = popSelectedService;
	return CDialog::DoModal();
}


void CSelectService::IniGrid ()
{
	/*
	RecordSet   olRecord;
	bool		blRead;
	CString		olWert, olEintrag, olVato, olVafr, olUrno, olDBVato, olDBVafr;
	DWORD		ilUrno;
	CString		polReferences[3];
	CString		polTypes[2];
	CTime		olTime;
	int			ilWert;

	polReferences[0] = GetString(IDS_TURNAROUND) ;
	polReferences[1] = GetString(IDS_INBOUND) ;	
	polReferences[2] = GetString(IDS_OUTBOUND) ;
	polTypes[0] = GetString(IDS_DEFAULT_VALUES) ;
	polTypes[1] = GetString(IDS_FIXED_VALUES) ;	
*/	
	pomServiceList = new CGridControl ( this, IDC_SERVICELIST, 
										DISPLAYED_SERVICEINFO, imServiceCount );

	pomServiceList->SetValueRange(CGXRange(0,1), GetString(IDS_NAME) );
	pomServiceList->SetValueRange(CGXRange(0,2), GetString(IDS_CODE) );
//	pomServiceList->SetValueRange(CGXRange(0,3), GetString(IDS_KURZNAME) );
	pomServiceList->SetValueRange(CGXRange(0,3), GetString(IDS_ANNEX) );
//	pomServiceList->SetValueRange(CGXRange(0,4), GetString(IDS_INVOICING) );
//	pomServiceList->SetValueRange(CGXRange(0,5), GetString(IDS_ORDER_NO) );
//	pomServiceList->SetValueRange(CGXRange(0,6), GetString(IDS_PREIS) );
	pomServiceList->SetValueRange(CGXRange(0,4), GetString(IDS_BEZUG) );
//	pomServiceList->SetValueRange(CGXRange(0,5), GetString(IDS_LEISTUNGSART) );
	pomServiceList->SetValueRange(CGXRange(0,5), GetString(IDS_VALID_FROM) );
	pomServiceList->SetValueRange(CGXRange(0,6), GetString(IDS_VALID_TO) );
	 
	IniColWidths ();
	FillGrid ();
/*
	for ( int i=0; i<imServiceCount; i++ )
	{
		blRead=ogBCD.GetRecord(pcmTabName, i, olRecord);
		if ( blRead )
		{
			olUrno=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"URNO")];
			if ( !sscanf ( pCHAR(olUrno), "%ld", &ilUrno ) )
				continue;
			pomServiceList->SetStyleRange ( CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SNAM")];
			pomServiceList->SetValueRange(CGXRange(i+1,1), olWert );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SECO")];
			pomServiceList->SetValueRange(CGXRange(i+1,2), olWert );

//			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SSNM")];
//			pomServiceList->SetValueRange(CGXRange(i+1,3), olWert );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SEAX")];
			pomServiceList->SetValueRange(CGXRange(i+1,3), olWert );

//			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SEIV")];
//			pomServiceList->SetValueRange(CGXRange(i+1,4), olWert );

//			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SSAP")];
//			pomServiceList->SetValueRange(CGXRange(i+1,5), olWert );

//			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SEHC")];
//			pomServiceList->SetValueRange(CGXRange(i+1,6), olWert );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SEER")];
			if ( ( sscanf ( pCHAR(olWert), "%d", &ilWert ) >=1 )
				 && ( ilWert>=0) && ( ilWert<=2) )
				olEintrag = polReferences[ilWert];
			else
			{
				if ( olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"FFIS")] == '1' )
					olEintrag = GetString(IDS_FLIGHTINDIPENDENT);
				else
					olEintrag = "";
			}
			pomServiceList->SetValueRange(CGXRange(i+1,4), olEintrag );

//			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"FSET")];
//			if ( sscanf ( pCHAR(olWert), "%d", &ilWert ) && ( ilWert>=0) 
//				 && ( ilWert<=1) )
//				olEintrag = polTypes[ilWert];
//			else
//				olEintrag = "";
//			pomServiceList->SetValueRange(CGXRange(i+1,5), olEintrag );
		
			if ( LoadServiceValidity ( olUrno, olDBVafr, olDBVato ) )
			{
				olTime = DBStringToDateTime(olDBVafr);
				olVafr = olTime.Format ( "%d.%m.%Y %H:%M" );
				olTime = DBStringToDateTime(olDBVato);
				olVato = olTime.Format ( "%d.%m.%Y %H:%M" );
			}
			else
			{
				olVafr.Empty();
				olVato.Empty();
			}
			pomServiceList->SetValueRange(CGXRange(i+1,5), olVafr );
			pomServiceList->SetValueRange(CGXRange(i+1,6), olVato );
		}
	}
	ROWCOL   ilRows = pomServiceList->GetRowCount();
	if ( ilRows > 0 )
	{
		CGXRange olRange;
		olRange.SetRows ( 1, ilRows );
		pomServiceList->SetStyleRange( olRange, CGXStyle().SetEnabled(FALSE) );
	}
	pomServiceList->SetStyleRange( CGXRange().SetTable(), CGXStyle()
									.SetVerticalAlignment(DT_VCENTER)
									.SetReadOnly(TRUE));
	pomServiceList->EnableAutoGrow ( FALSE );
	pomServiceList->GetParam()->EnableSelection ( GX_SELROW );
	pomServiceList->SortTable ( 0, 1 );
	//pomServiceList->SetLbDblClickAction ( WM_COMMAND, IDOK );
*/
}
 
void CSelectService::OnOK() 
{
	// TODO: Add extra validation here
	ROWCOL lRow;
	CString olStr;
	RecordSet   olRecord;
	CGXStyle	olStyle;
	DWORD		ilUrno;
	CRowColArray olSelRows;
	
	if ( pomServiceList->GetSelectedRows(olSelRows, FALSE, FALSE ) != 1 )
	{
		return;
	}
	lRow = olSelRows[0];
	pomServiceList->ComposeStyleRowCol( lRow,1, &olStyle );
	ilUrno = (DWORD)olStyle.GetItemDataPtr();
	if ( !ilUrno )
	{
		TRACE ("Urno=0 in SelectService f�r Zeile%d\n", lRow );
		return;
	}
	else
		olStr.Format( "%ld", ilUrno );
	//if ( !ogBCD.GetRecord(pcmTabName, lRow-1,  olRecord ) )
	if ( !ogBCD.GetRecord(pcmTabName, "URNO", olStr, olRecord ) )
		return;
	*pomSelectedService = olRecord;
		
	PreDestroyWindow ();
	CDialog::OnOK();
}

void CSelectService::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if ( pomServiceList )
	{
		delete pomServiceList;
	}
	pomServiceList = 0;
}

void CSelectService::IniColWidths ()
{
	char pclEntry[81], *ps1, *ps2 ;
	int ilValue;
	CString olEntry;
	
	if ( GetPrivProfString ( "ServiceList", "ColWidths", 
					    //"42, 217, 64, 59, 77, 67, 46, 46, 77, 93, 93",
					    "42, 217, 64, 65, 77, 93, 93",
						pclEntry, 81, ogIniFile ) )
	{
		olEntry = pclEntry;
		if ( olEntry.Replace ( ",", "|" ) != DISPLAYED_SERVICEINFO )
		{	//  entry from an older version found
			strcpy ( pclEntry, "42, 217, 64, 65, 77, 93, 93" ); 
		}
	}
	ps1= pclEntry;
	for ( int i=0; i<=DISPLAYED_SERVICEINFO; i++ )
	{
		if ( !sscanf ( ps1, "%d", &ilValue ) )
			break;
		pomServiceList->SetColWidth ( i, i, ilValue );
		if ( ps2 = strchr ( ps1, ',' ) )
			ps1 = ps2+1;
		else
			break;
	}
}

void CSelectService::SaveColWidths ()
{
	char pclEntry[81], pclTemp[11];
	int ilValue; 

	pclEntry[0] = '\0';
	for ( int i=0; i<DISPLAYED_SERVICEINFO; i++ )
	{
		ilValue = pomServiceList->GetColWidth ( i );
		sprintf ( pclTemp, "%d, ", ilValue ) ;
		strcat ( pclEntry, pclTemp );
	}
	ilValue = pomServiceList->GetColWidth ( DISPLAYED_SERVICEINFO );
	sprintf ( pclTemp, "%d", ilValue ) ;
	strcat ( pclEntry, pclTemp );
	WritePrivateProfileString ( "ServiceList", "ColWidths", pclEntry, ogIniFile );
	

}

void CSelectService::IniStatics ()
{
	SetWindowText ( GetString (IDS_SERVICE_LIST) );
	SetDlgItemText ( IDOK, GetString (IDS_OK) );
	SetDlgItemText ( IDCANCEL, GetString (IDS_CANCEL) );
	SetDlgItemText ( ID_FILE_PRINT, GetString (IDS_PRINT) );
	SetDlgItemText ( ID_EDIT_FIND, GetString (IDS_SEARCH) );
	SetDlgItemText ( IDC_VIEW, GetString (IDS_M_VIEW) );
}

		
void CSelectService::OnFilePrint() 
{
	// TODO: Add your control notification handler code here
	CServiceCatalogView *popView = (CServiceCatalogView*)m_pParentWnd;
	
	if ( popView && pomServiceList )
		popView->PrintGrid ( pomServiceList );
//		popView->PrintPreviewGrid ( pomServiceList );
		
}

void CSelectService::OnCancel() 
{
	// TODO: Add extra cleanup here
	PreDestroyWindow ();
	CDialog::OnCancel();
}

CGridControl* CSelectService::GetServiceList ()
{
	return pomServiceList;
}

void CSelectService::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	CRect olChildRect, olCliRect;
	int ilWidthDiff, ilHeightDiff;

	GetClientRect ( &olCliRect );
	if ( pomServiceList	)
	{
		pomServiceList->GetWindowRect ( &olChildRect );
		if ( imLastWidth && imLastHeight )
		{
			ilWidthDiff = olCliRect.Width () - imLastWidth;
			ilHeightDiff = olCliRect.Height () - imLastHeight;
			pomServiceList->SetWindowPos( 0, 0, 0, 
										  olChildRect.Width() + ilWidthDiff, 
										  olChildRect.Height () + ilHeightDiff, 
										  SWP_NOMOVE | SWP_NOZORDER );
			pomServiceList->Redraw ();

			for ( int i=0; i<5; i ++ )
			{
				pomDlgButtons[i].GetWindowRect ( &olChildRect );
				ScreenToClient ( &olChildRect );
				pomDlgButtons[i].SetWindowPos( 0, olChildRect.left, 
											   olChildRect.top + ilHeightDiff, 0, 0, 
											   SWP_NOSIZE | SWP_NOZORDER );
			}
			m_ViewCB.GetWindowRect ( &olChildRect );
			ScreenToClient ( &olChildRect );
			m_ViewCB.SetWindowPos( 0, olChildRect.left, 
								   olChildRect.top + ilHeightDiff, 0, 0, 
								   SWP_NOSIZE | SWP_NOZORDER );
		}

	}
	imLastWidth = olCliRect.Width ();
	imLastHeight = olCliRect.Height ();
}


void CSelectService::OnEditFind() 
{
	// TODO: Add your control notification handler code here
	if ( pomServiceList	)
		pomServiceList->OnShowFindReplaceDialog(TRUE);
}

void CSelectService::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	OnCancel ();
	//PreDestroyWindow ();
	//CDialog::OnClose();
}

void CSelectService::PreDestroyWindow ()
{
	SaveWindowPosition ( this, "ServiceList" );
	SaveColWidths ();
	if ( pomServiceList && pomServiceList->m_hWnd )
		pomServiceList->DestroyWindow ();
}	

void CSelectService::OnGridLbDblClicked ( WPARAM wparam, LPARAM lparam )
{
	if ( lparam )
	{
		OnGridLbClicked( wparam, lparam );
		OnOK();
	}
}

void CSelectService::OnGridLbClicked( WPARAM wparam, LPARAM lparam )
{
	GRIDNOTIFY rlNotify;
	if ( lparam )
	{
		rlNotify = *((GRIDNOTIFY*)lparam);
		if ( rlNotify.row>0)
		{
			//pomServiceList->SelectRange(CGXRange().SetTable(), FALSE);
			pomServiceList->SelectRange(CGXRange().SetRows(rlNotify.row), TRUE);
		}
	}
}

void CSelectService::OnView() 
{
	// TODO: Add your control notification handler code here
	CString olCaption ;
	olCaption = GetString (IDS_VIEW_SERVICES);
	CSerListViewerPropSheet olDlg(olCaption, this, &ogSerListViewer, 0  );
	
	if(olDlg.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		FillGrid();
	}
	
}

void CSelectService::OnSelendokViewCb() 
{
	// TODO: Add your control notification handler code here
	int ilCurSelIndex = m_ViewCB.GetCurSel();
	CString olCurSelText = "";
	if(ilCurSelIndex!=CB_ERR)
	{
		m_ViewCB.GetLBText( ilCurSelIndex, olCurSelText);
		ogSerListViewer.SelectView(olCurSelText);
		FillGrid();
	}
	
}

void CSelectService::UpdateComboBox()
{
	CCS_TRY

	CStringArray olViewArray;
	int ilIndex = 0, ilSel=0;
	int ilComboIndex = 0;
	CString olActView;
	
	ogSerListViewer.GetViews(olViewArray);
	olActView = ogSerListViewer.GetViewName();

	m_ViewCB.ResetContent();
	for (ilIndex = 0; ilIndex < olViewArray.GetSize(); ilIndex++)
	{	
		//-- Add views to combobox
		ilComboIndex = m_ViewCB.AddString(olViewArray[ilIndex]);
	
		//-- Select the view last added
		/*
		if ( !olActView.IsEmpty() && (olViewArray[ilIndex] == olActView) )
		{
			ilSel = ilComboIndex;
		}*/
	}

	//-- Select the actual view 
	if ( !olActView.IsEmpty() )
	{
		ilSel = m_ViewCB.FindStringExact( -1, olActView );
		ilSel = max ( ilSel, 0 );
	}
	m_ViewCB.SetCurSel(ilSel);
	CCS_CATCH_ALL	
}


void CSelectService::FillGrid ()
{
	RecordSet   olRecord;
	bool		blRead;
	CString		olWert, olEintrag, olVato, olVafr, olUrno, olDBVato, olDBVafr;
	DWORD		ilUrno;
	CString		polReferences[3];
	CString		polTypes[2];
	CTime		olTime;
	int			ilWert;

	theApp.DoWaitCursor ( 1 );

	polReferences[0] = GetString(IDS_TURNAROUND) ;
	polReferences[1] = GetString(IDS_INBOUND) ;	
	polReferences[2] = GetString(IDS_OUTBOUND) ;
	polTypes[0] = GetString(IDS_DEFAULT_VALUES) ;
	polTypes[1] = GetString(IDS_FIXED_VALUES) ;	

	//--- get filter string
	CString olSerUrnos;
	CStringArray olFilter;

	ogSerListViewer.GetFilter("SERLIST", olFilter);
	if(olFilter.GetSize() > 1)
	{
		olSerUrnos = olFilter[1];
	}
	else
	{	//  <Default> - Filter
		for (int i = 0; i < ogBCD.GetDataCount("SER"); i++)
		{
			olSerUrnos +=  ogBCD.GetField ( "SER", i, "URNO") ;
			olSerUrnos +=  " ";
		}
	}
	
	//--- copy parts of filter string into string array
	CStringArray olTmpStr;
	CGXRange olRange;
	int ilCount = ExtractItemList(olSerUrnos, &olTmpStr, ' ');

	pomServiceList->LockUpdate(TRUE);
	pomServiceList->SetRowCount(ilCount); 
	
	if ( ilCount>0)
	{
		olRange.SetRows ( 1, ilCount );
		pomServiceList->SetStyleRange( olRange, CGXStyle().SetReadOnly(FALSE) );
		pomServiceList->SetTopRow ( 1, GX_SMART, FALSE  );
	}
	else
		pomServiceList->SetTopRow ( 0, GX_SMART, FALSE  );

	CString olUrnoStr;
	int		ilActRow=0;

	for(int i = 0; i < ilCount; i++)
	{
		// fill struct
		olUrno = olTmpStr[i];
		olUrno.Remove ( '|');

		blRead=ogBCD.GetRecord(pcmTabName, "URNO", olUrno, olRecord);
		if ( blRead )
		{
			if ( !sscanf ( pCHAR(olUrno), "%ld", &ilUrno ) )
				continue;
			ilActRow++;
			pomServiceList->SetStyleRange ( CGXRange(ilActRow,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SNAM")];
			pomServiceList->SetValueRange(CGXRange(ilActRow,1), olWert );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SECO")];
			pomServiceList->SetValueRange(CGXRange(ilActRow,2), olWert );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SEAX")];
			pomServiceList->SetValueRange(CGXRange(ilActRow,3), olWert );

			olWert=olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"SEER")];
			if ( ( sscanf ( pCHAR(olWert), "%d", &ilWert ) >=1 )
				 && ( ilWert>=0) && ( ilWert<=2) )
				olEintrag = polReferences[ilWert];
			else
			{
				if ( olRecord.Values[ogBCD.GetFieldIndex(pcmTabName,"FFIS")] == '1' )
					olEintrag = GetString(IDS_FLIGHTINDIPENDENT);
				else
					olEintrag = "";
			}
			pomServiceList->SetValueRange(CGXRange(ilActRow,4), olEintrag );

			if ( LoadServiceValidity ( olUrno, olDBVafr, olDBVato ) )
			{
				olTime = DBStringToDateTime(olDBVafr);
				olVafr = olTime.Format ( "%d.%m.%Y %H:%M" );
				olTime = DBStringToDateTime(olDBVato);
				olVato = olTime.Format ( "%d.%m.%Y %H:%M" );
			}
			else
			{
				olVafr.Empty();
				olVato.Empty();
			}
			pomServiceList->SetValueRange(CGXRange(ilActRow,5), olVafr );
			pomServiceList->SetValueRange(CGXRange(ilActRow,6), olVato );
		}

	}
	ROWCOL   ilRows = pomServiceList->GetRowCount();
	if ( ilRows != ilActRow )
	{
		pomServiceList->SetRowCount(ilActRow); 
		ilRows = pomServiceList->GetRowCount();
	}

	if ( ilRows > 0 )
	{
		olRange.SetRows ( 1, ilRows );
		pomServiceList->SetStyleRange( olRange, CGXStyle().SetEnabled(FALSE) );
	}
	pomServiceList->SetStyleRange( CGXRange().SetTable(), CGXStyle()
									.SetVerticalAlignment(DT_VCENTER)
									.SetReadOnly(TRUE));
	pomServiceList->EnableAutoGrow ( FALSE );
	pomServiceList->GetParam()->EnableSelection ( GX_SELROW );
	pomServiceList->ResetSortDir();
	pomServiceList->SortTable ( 0, 1 );
	pomServiceList->LockUpdate ( FALSE );
	theApp.DoWaitCursor ( -1 );
}
