// MainFrm.cpp : implementation of the CMainFrame class
//

#include <stdafx.h>

#include <utilities.h>
#include <ServiceCatalog.h>
#include <CedaBasicData.h>
#include <ServiceCatalogView.h>
#include <MainFrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	// toolbar "tooltip" notification
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY,OnUpdateCopy)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdatePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdatePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_VALIDITY, OnUpdateValidity)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT|TBSTYLE_TRANSPARENT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC ) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	else
		CustomizeToolBar();

	TranslateMenu ();

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	RECT	rect;
	GetWindowRect ( &rect );
}

void CMainFrame::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	int ilRet = IDYES;

	//  Abfrage "Wollen Sie das Programm wirklich beenden?" nur wenn 
	//	Applikation nicht von anderem Programm gestartet worden ist
	if ( !theApp.bmLoginByCommandLine )
		ilRet = LangMsgBox ( m_hWnd, IDS_ASK_FOR_EXIT, AFX_IDS_APP_TITLE,
							 MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 );
	else
	{
		ShowWindow ( SW_SHOWMINIMIZED );
		ilRet = IDNO;
	}
	if ( ilRet == IDYES )
	{
		//  Eventuell gešnderten Datensatz noch speichern
		CServiceCatalogView* polView;
		polView = (CServiceCatalogView*)GetActiveView() ;
		if ( polView && !polView->HandleModifiedService () )
			return;
		CFrameWnd::OnClose ();	
	}
}

void CMainFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	SetWindowText ( ogAppName );
}

void CMainFrame::CustomizeToolBar ()	
{
	UINT ilIDC;
	int  ilStrInd, ilStrLen, ilLastPos;
	char *ps;
	CString olICDText;
	CToolBarCtrl&  bar = m_wndToolBar.GetToolBarCtrl();
	int ilButtonAnz = bar.GetButtonCount(); 


	m_wndToolBar.SetSizes( CSize(80, 45) , CSize(30, 25));
	for ( int i=0; i<ilButtonAnz; i++ )
	{
		ilIDC = m_wndToolBar.GetItemID( i ) ;
		if ( !ilIDC )
			continue;
		olICDText = GetString ( ilIDC );
		if ( olICDText.IsEmpty () )
			return;
		ilStrLen = olICDText.GetLength ();
		ilLastPos = -1;
		while ( ( ( ilStrInd = olICDText.Find( '\n', ilLastPos+1 ) ) > 0 )&&
				( ilStrInd < ilStrLen-1 ) )
			ilLastPos = ilStrInd;
		ps = olICDText.GetBuffer(ilStrLen)+ilLastPos+1;
		m_wndToolBar.SetButtonText( i, ps );
	}
}

void CMainFrame::GetMessageString(UINT nID, CString& rMessage) const
{
	rMessage = GetString(nID);
	// first newline terminates actual string
	rMessage.Replace ( '\n', '\0' );

}

/*
bool CMainFrame::TranslateMenu ()
{
	CMenu *polMenu = GetMenu ();
	CMenu *polSubMenu;
	CString cNewStr;
	MENUITEMINFO slInfo;
	BOOL	ok=TRUE;
	UINT ilItems, ilSubItems, ilItemID;
	char pclItem[11];
	char *pclMenus[4] = { "MENU_POPUP_FILE", "MENU_POPUP_EDIT", "MENU_POPUP_VIEW", 
						  "MENU_POPUP_HELP" };

	if ( !polMenu )
		return false;

	if ( ( ogBCD.GetDataCount("TXT") <= 0 ) && bgUseResourceStrings )
		return false;

	ilItems = polMenu->GetMenuItemCount ();
	for ( UINT i=0; i<ilItems; i++ )
	{
		memset ( &slInfo, 0, sizeof(slInfo) );
		slInfo.fMask = MIIM_TYPE|MIIM_SUBMENU;
		slInfo.cbSize = sizeof ( slInfo );
		ok = polMenu->GetMenuItemInfo( i, &slInfo, TRUE );
		if ( ok && slInfo.hSubMenu )
		{	//  SubMenues
			polSubMenu = polMenu->GetSubMenu ( i ) ;
			if ( slInfo.fType == MFT_STRING )
				//cNewStr = GetString ( "TXID", pclMenus[i] );
				if ( (i>=4) || ! GetString ( "TXID", pclMenus[i], cNewStr ) )
					ok = FALSE;
			ilItemID = (UINT)slInfo.hSubMenu;
		}
		else
		{
			polSubMenu = 0;
			if ( slInfo.fType == MFT_STRING )
			{
				ilItemID = polMenu->GetMenuItemID (i);
				sprintf ( pclItem, "M%ld", ilItemID );
				//cNewStr = GetString ( "STID", pclItem );
				if ( ! GetString ( "STID", pclItem, cNewStr ) )
					ok = FALSE;
			}
		}
		if ( ok && (slInfo.fType == MFT_STRING) )
			ok &= polMenu->ModifyMenu( i, MF_STRING|MF_BYPOSITION, 
									   ilItemID, cNewStr );


		if ( polSubMenu )
		{
			ilSubItems = polSubMenu->GetMenuItemCount ( );
			for ( UINT j=0; j<ilSubItems ; j++ )
			{
				memset ( &slInfo, 0, sizeof(slInfo) );
				slInfo.fMask = MIIM_TYPE|MIIM_SUBMENU;
				slInfo.cbSize = sizeof ( slInfo );
				ok = polSubMenu->GetMenuItemInfo( j, &slInfo, TRUE );
				if ( slInfo.fType != MFT_STRING )
					continue;
				ilItemID = polSubMenu->GetMenuItemID (j);
				sprintf ( pclItem, "M%ld", ilItemID );
				//cNewStr = GetString ( "STID", pclItem );
				if ( ! GetString ( "STID", pclItem, cNewStr ) )
					ok = FALSE;
				if ( ok )
					ok &= polSubMenu->ModifyMenu( j, MF_STRING|MF_BYPOSITION, 
												  ilItemID, cNewStr );
			}
		}
	}
	return true;
}
*/

BOOL CMainFrame::OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);

	// need to handle both ANSI and UNICODE versions of the message
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	//TCHAR szFullText[256];
	CString strTipText;
	UINT nID = pNMHDR->idFrom;
	if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
		pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
	{
		// idFrom is actually the HWND of the tool
		nID = ::GetDlgCtrlID((HWND)nID);
	}

	if (nID != 0) // will be zero on a separator
	{
		CString olMsg;
		// don't handle the message if no string resource found
		olMsg = GetString ( nID ) ;
		if ( olMsg.IsEmpty() && !olMsg.LoadString(nID) )
			return FALSE;

		// this is the command id, not the button index
		AfxExtractSubString(strTipText, olMsg, 1, '\n');
	}
#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#endif
	*pResult = 0;

	// bring the tooltip window above other popup windows
	::SetWindowPos(pNMHDR->hwndFrom, HWND_TOP, 0, 0, 0, 0,
		SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOMOVE|SWP_NOOWNERZORDER);

	return TRUE;    // message was handled
}


bool CMainFrame::TranslateMenu ()
{
	HMENU			hMenu, hSubMenu;
	BOOL			ok=TRUE;
	CMenu *polMenu = GetMenu ();

	if ( !polMenu || !polMenu->m_hMenu )
		return false;

	hMenu = polMenu->m_hMenu;
	//  Submenu "File"
	hSubMenu = GetSubMenu ( hMenu, 0 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 0, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_FILE) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_NEW, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_NEW, GetString(IDS_M_FILE_NEW) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_OPEN, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_OPEN, GetString(IDS_M_FILE_OPEN) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_PRINT_SETUP, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_PRINT_SETUP, GetString(IDS_M_FILE_PRINT_SETUP) );
		ok &= ModifyMenu( hSubMenu, ID_APP_EXIT, MF_STRING|MF_BYCOMMAND, 
						  ID_APP_EXIT, GetString(IDS_M_APP_EXIT) );

		ok &= ModifyMenu( hSubMenu, ID_FILE_SAVE, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_SAVE, GetString(IDS_M_FILE_SAVE) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_PRINT, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_PRINT, GetString(IDS_M_FILE_PRINT) );
		ok &= ModifyMenu( hSubMenu, ID_FILE_PRINT_PREVIEW, MF_STRING|MF_BYCOMMAND, 
						  ID_FILE_PRINT_PREVIEW, GetString(IDS_M_FILE_PRINT_PREVIEW) );
	}
	else 
		ok = false;

	//  Submenu "Edit"
	hSubMenu = GetSubMenu ( hMenu, 1 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 1, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_EDIT) );
		ok &= ModifyMenu( hSubMenu, ID_EDIT_CUT, MF_STRING|MF_BYCOMMAND, 
						  ID_EDIT_CUT, GetString(IDS_M_EDIT_CUT) );
		ok &= ModifyMenu( hSubMenu, ID_EDIT_COPY, MF_STRING|MF_BYCOMMAND, 
						  ID_EDIT_COPY, GetString(IDS_M_EDIT_COPY) );
	}
	else 
		ok = false;

	//  Submenu "View"
	hSubMenu = GetSubMenu ( hMenu, 2 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 2, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_VIEW) );
		ok &= ModifyMenu( hSubMenu, ID_VIEW_TOOLBAR, MF_STRING|MF_BYCOMMAND, 
						  ID_VIEW_TOOLBAR, GetString(IDS_M_VIEW_TOOLBAR) );
		ok &= ModifyMenu( hSubMenu, ID_VIEW_STATUS_BAR, MF_STRING|MF_BYCOMMAND, 
						  ID_VIEW_STATUS_BAR, GetString(IDS_M_VIEW_STATUS_BAR) );
	}
	else 
		ok = false;

	//  Submenu "Help"
	hSubMenu = GetSubMenu ( hMenu, 3 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 3, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_M_HELP) );
		ok &= ModifyMenu( hSubMenu, ID_APP_ABOUT, MF_STRING|MF_BYCOMMAND, 
						  ID_APP_ABOUT, GetString(IDS_M_APP_ABOUT) );
	}
	else 
		ok = false;

	return (ok!=FALSE);
}

void CMainFrame::OnUpdateCut(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_EDIT_CUT, TRUE);
}

void CMainFrame::OnUpdateCopy(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_EDIT_COPY, TRUE);
}

void CMainFrame::OnUpdateOpen(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_FILE_OPEN, TRUE);
}

void CMainFrame::OnUpdateSave(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_FILE_SAVE, TRUE);
}

void CMainFrame::OnUpdateNew(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_FILE_NEW, TRUE);
}

void CMainFrame::OnUpdatePrint(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_FILE_PRINT, TRUE);
}

void CMainFrame::OnUpdatePrintPreview(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_FILE_PRINT_PREVIEW, TRUE);
}

void CMainFrame::OnUpdateValidity(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(FALSE);
	this->HideToolbarBtn(ID_VALIDITY, TRUE);
}

BOOL  CMainFrame::HideToolbarBtn(int ipID, bool bpHide)
{
	CToolBarCtrl&  bar = this->m_wndToolBar.GetToolBarCtrl();

	if ( bar.GetState ( ipID ) == -1 )
		return FALSE;

	BOOL blRet = TRUE;
	bool blTest = bar.IsButtonHidden(ipID);

	if (bpHide && !bar.IsButtonHidden(ipID))
		blRet = bar.HideButton(ipID, TRUE);

	if (!bpHide && bar.IsButtonHidden(ipID))
		blRet = bar.HideButton(ipID, FALSE);
	return blRet;
}

