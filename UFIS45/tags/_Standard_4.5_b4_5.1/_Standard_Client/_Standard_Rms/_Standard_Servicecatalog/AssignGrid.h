// AssignGrid.h: interface for the CAssignGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ASSIGNGRID_H__AF90DFF2_6B5E_11D3_936C_00001C033B5D__INCLUDED_)
#define AFX_ASSIGNGRID_H__AF90DFF2_6B5E_11D3_936C_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <GridControl.h>

struct QUALIZUORDNUNG
{
	CString		 func;
	CStringArray quals;
};

class CAssignGrid : public CTitleGridControl  
{
public:
	CAssignGrid(CWnd *popParent, UINT nID, char * pcpTableName, 
				CServiceCatalogDoc *popDoc );
	virtual ~CAssignGrid();
protected:
	CString omTableName;
	CString omCodeField;
	CString omDisplayField;
//  Implementation
public:
	void ResetValues ();
	bool SetLineValue ( int ipResNo, CString &ropCode );


protected:
	void IniLayOut ();
	bool CreateComboBoxes ();
	bool CreateComboBoxes ( CString &ropChoiceList );
	void DisplayInfo ( ROWCOL nRow, ROWCOL nCol );
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);
	bool DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) ;
	void SetDefaultValues ( ROWCOL nRow );
	void OnModifyCell (ROWCOL nRow, ROWCOL nCol);
	
};


class CAssignFuncGrid : public CAssignGrid  
{
public:
	CAssignFuncGrid(CWnd *popParent, UINT nID, char * pcpTableName, 
					CServiceCatalogDoc *popDoc );
	virtual ~CAssignFuncGrid();
protected:
	CPtrArray *prmZuordnung;
	ROWCOL	  ilLastCurrentRow;
public:
	void SetAssignment( CPtrArray *prpZuordnung );
protected:
	void OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol);
};


class CAssignQualiGrid : public CAssignGrid  
{
public:
	CAssignQualiGrid(CWnd *popParent, UINT nID, char * pcpTableName, 
					 CServiceCatalogDoc *popDoc );
	virtual ~CAssignQualiGrid();
	void SetSelection ( CStringArray &ropSelection );
	void GetSelection ( CStringArray &ropSelection );
};

#endif // !defined(AFX_ASSIGNGRID_H__AF90DFF2_6B5E_11D3_936C_00001C033B5D__INCLUDED_)
