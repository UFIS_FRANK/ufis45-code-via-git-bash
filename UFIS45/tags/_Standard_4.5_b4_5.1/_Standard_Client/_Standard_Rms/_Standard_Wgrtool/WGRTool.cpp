// WGRTool.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <UFIS.h>
#include <ccsglobl.h>
#include <LoginDlg.h>
#include <PrivList.h>
#include <RegisterDlg.h>

#include <AatHelp.h>
#include <WGRTool.h>
#include <WGRToolDlg.h>
#include <CedaBasicData.h>
#include <BasicData.h>
#include <GUILng.h>
#include <VersionInfo.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>
#include <LibGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWGRToolApp

BEGIN_MESSAGE_MAP(CWGRToolApp, CWinApp)
	//{{AFX_MSG_MAP(CWGRToolApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWGRToolApp construction

CWGRToolApp::CWGRToolApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CWGRToolApp object

CWGRToolApp theApp;

bool CWGRToolApp::InitialLoad()
{
	// Versionsinformation ermitteln
	VersionInfo rlInfo;
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT
	CCSCedaData::bmVersCheck = true;
	strncpy(CCSCedaData::pcmVersion,rlInfo.omFileVersion,sizeof(CCSCedaData::pcmVersion));
	CCSCedaData::pcmVersion[sizeof(CCSCedaData::pcmVersion)-1] = '\0';

	strncpy(CCSCedaData::pcmInternalBuild,rlInfo.omPrivateBuild.Right(4),sizeof(CCSCedaData::pcmInternalBuild));
	CCSCedaData::pcmInternalBuild[sizeof(CCSCedaData::pcmInternalBuild)-1] = '\0';

	// initialize configuration data
	ogCfgData.SetTableName(CString(CString("VCD") + CString(pcgTableExt)));
	ogCfgData.ReadCfgData();

	ogBCD.SetTableExtension(CString(pcgTableExt));
	// initialize ALO table access
	bool blOK = ogBCD.SetObject("ALO","REFT,ALOD,ALOC");
	if (!blOK)
		return false;
	ogBCD.Read("ALO");

	// initialize WGR table access
	blOK = ogBCD.SetObject("WGR");
	if (!blOK)
		return false;
	ogBCD.Read("WGR");

#ifdef	WGNTAB_SUPPORT
	// initialize WGN table access
	blOK = ogBCD.SetObject("WGN");
	if (!blOK)
		return false;
	ogBCD.Read("WGN");
#endif
	return true;	
}

void CWGRToolApp::InitializeDefaultView()
{

	CViewer		 olViewer;
	CStringArray olPossibleFilters;

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("ALO");
	olViewer.SetViewerKey("WGRTool");

	CString olLastView = olViewer.SelectView();	// remember the current view

	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");

	CStringArray olAloFilter;
	
	olAloFilter.Add(LoadStg(IDS_STRING423));
	olViewer.SetFilter("ALO",olAloFilter);

	
}

/////////////////////////////////////////////////////////////////////////////
// CWGRToolApp initialization

BOOL CWGRToolApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif


	GXInit();
	 if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }


	if (!AatHelp::Initialize(ogAppName))
	{
		CString olErrorMsg;
		AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}

  	InitFont();
	CreateBrushes();

	// INIT Tablenames and Homeairport
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);

	strcpy(CCSCedaData::pcmTableExt,    pcgTableExt);
	strcpy(CCSCedaData::pcmApplName,    pcgAppName);
    strcpy(CCSCedaData::pcmHomeAirport, pcgHome);

	ogHome = pcgHome;
	ogTableExt = pcgTableExt;

	char pclTmp[128];
	CGUILng* ogGUILng = CGUILng::TheOne();

    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof pclTmp, pclConfigPath);
	ogGUILng->SetLanguage(CString(pclTmp));	

    GetPrivateProfileString(ogAppName, "DBParam", "", pclTmp, sizeof pclTmp, pclConfigPath);
	ogGUILng->SetStoreInDB(CString(pclTmp));	
	
	//INIT END/////////////////////////
#if	0
	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,ogCommHandler.pcmReqId);

	if( olLoginDlg.DoModal() != IDCANCEL )
	{
		ogLoginTime = CTime::GetCurrentTime();
		int ilStartApp = IDOK;
		if(ogPrivList.GetStat("InitModu") == '1')
		{
			RegisterDlg olRegisterDlg;
			ilStartApp = olRegisterDlg.DoModal();
		}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName("WGRTool");
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());
	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}
	else
	{
		ogLoginTime = CTime::GetCurrentTime();
		int ilStartApp = IDOK;

		ogBasicData.omUserID = olLoginCtrl.GetUserName_();
		strcpy(pcgUser,olLoginCtrl.GetUserName_());
		strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());

		ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
		strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

		ogPrivList.Add(olLoginCtrl.GetPrivilegList());

		olDummyDlg.DestroyWindow();
		ilStartApp = IDOK;
#endif
		if(ilStartApp == IDOK)
		{
			InitialLoad();
			InitializeDefaultView();

			CWGRToolDlg dlg;
			m_pMainWnd = &dlg;
			int nResponse = dlg.DoModal();
			if (nResponse == IDOK)
			{
				// TODO: Place code here to handle when the dialog is
				//  dismissed with OK
			}
			else if (nResponse == IDCANCEL)
			{
				// TODO: Place code here to handle when the dialog is
				//  dismissed with Cancel
			}
		}
	}
	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

void CWGRToolApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	AatHelp::WinHelp(dwData, nCmd);
}

//****************************************************************************
// CAboutDlg dialog used for App About
//****************************************************************************

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	DDX_Control(pDX, IDC_STATIC_USER, m_StcUser);
	DDX_Control(pDX, IDC_STATIC_SERVER, m_StcServer);
	DDX_Control(pDX, IDC_STATIC_LOGINTIME, m_StcLogintime);
	DDX_Control(pDX, IDC_COPYRIGHT4, m_Copyright4);
	DDX_Control(pDX, IDC_COPYRIGHT3, m_Copyright3);
	DDX_Control(pDX, IDC_COPYRIGHT2, m_Copyright2);
	DDX_Control(pDX, IDC_COPYRIGHT1, m_Copyright1);
	DDX_Control(pDX, IDC_SERVER,	 m_Server);
	DDX_Control(pDX, IDC_USER,		 m_User);
	DDX_Control(pDX, IDC_LOGINTIME,  m_Logintime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//****************************************************************************
// Daten aufbereiten und darstellen
//****************************************************************************

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Versionsinformation ermitteln
	VersionInfo rlInfo;
//	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),rlInfo);
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT

	CString olVersionString;
	olVersionString.Format("%s %s %s  Compiled: %s",rlInfo.omProductName,rlInfo.omFileVersion,rlInfo.omSpecialBuild,__DATE__);

	// Statics einstellen
	m_Copyright1.SetWindowText(rlInfo.omProductVersion);
	m_Copyright2.SetWindowText(olVersionString);
	m_Copyright3.SetWindowText(rlInfo.omLegalCopyright);
	m_Copyright4.SetWindowText(rlInfo.omCompanyName);
	m_StcServer.SetWindowText(LoadStg(IDS_STATIC_SERVER));
	m_StcUser.SetWindowText(LoadStg(IDS_STATIC_USER));
	m_StcLogintime.SetWindowText(LoadStg(IDS_STATIC_LOGINTIME));

	CString olServer = ogCommHandler.pcmRealHostName;
	olServer  += " / ";
	olServer  += ogCommHandler.pcmRealHostType;
	m_Server.SetWindowText(olServer);

	m_User.SetWindowText(CString(pcgUser));
	m_Logintime.SetWindowText(ogLoginTime.Format("%d.%m.%Y  %H:%M"));

	SetWindowText(LoadStg(IDS_WGRTOOL_ABOUT));


	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

