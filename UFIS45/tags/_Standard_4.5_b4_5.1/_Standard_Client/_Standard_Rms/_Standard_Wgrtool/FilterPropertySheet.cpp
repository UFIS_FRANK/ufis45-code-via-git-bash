// FilterPropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <WGRTool.h>
#include <CCSGlobl.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPropertySheet.h>
#include <StringConst.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FilterPropertySheet
//

FilterPropertySheet::FilterPropertySheet(CString opCalledFrom,CString opCaption, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(opCaption, pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_PsFilterPage);
	pomViewer = popViewer; 

} 

void FilterPropertySheet::LoadDataFromViewer()
{
	if (pomViewer != NULL)
	{ 
		pomViewer->GetFilter("ALO",m_PsFilterPage.omSelectedItems);
	}
}

void FilterPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{

	if (pomViewer != NULL)
	{
		m_PsFilterPage.UpdateData(TRUE);
		pomViewer->SetFilter("ALO",m_PsFilterPage.omSelectedItems);
	}

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

