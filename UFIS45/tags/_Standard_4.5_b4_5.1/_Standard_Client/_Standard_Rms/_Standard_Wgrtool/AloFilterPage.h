#if !defined(AFX_ALOFILTERPAGE_H__291CE195_99F1_11D4_BFE1_00010215BFDE__INCLUDED_)
#define AFX_ALOFILTERPAGE_H__291CE195_99F1_11D4_BFE1_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AloFilterPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAloFilterPage dialog
class CGridFenster;

class CAloFilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CAloFilterPage)

// Construction
public:
	CAloFilterPage();
	~CAloFilterPage();
	
	static bool GetAllFilters(CStringArray& opFilterList);

// Dialog Data
	//{{AFX_DATA(CAloFilterPage)
	enum { IDD = IDD_FILTER_PAGE };
	CButton	m_RemoveButton;
	CButton	m_AddButton;
	CListBox	m_InsertList;
	CListBox	m_ContentList;
	//}}AFX_DATA

	CGridFenster*	pomPossilbeList;
	CGridFenster*	pomSelectedList;
	int				imColCount;
	int				imHideColStart;
	bool			bmIsInitialized;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAloFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAloFilterPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private: // helpers
	static bool	EvaluateTableTypes(CStringArray& opTableTypes);

public:
	// contains the list of possible items
	CStringArray omPossibleItems;
	// contains the list of selected items
	CStringArray omSelectedItems;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALOFILTERPAGE_H__291CE195_99F1_11D4_BFE1_00010215BFDE__INCLUDED_)
