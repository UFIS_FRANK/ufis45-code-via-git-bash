// NewGroupDlg.cpp : implementation file
//

#include <stdafx.h>
#include <WGRTool.h>
#include <NewGroupDlg.h>
#include <BasicData.h>
#include <WGRToolDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewGroupDlg dialog


CNewGroupDlg::CNewGroupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewGroupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewGroupDlg)
	m_GroupDescription = _T("");
	//}}AFX_DATA_INIT
	pomGroups = NULL;
}


void CNewGroupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewGroupDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_STATIC_DESCRIPTION, m_Description);
	DDX_Text(pDX, IDC_EDIT_DESCRIPTION, m_GroupDescription);
	DDV_MaxChars(pDX, m_GroupDescription, 10);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewGroupDlg, CDialog)
	//{{AFX_MSG_MAP(CNewGroupDlg)
	ON_EN_UPDATE(IDC_EDIT_DESCRIPTION, OnUpdateEditDescription)
	ON_EN_CHANGE(IDC_EDIT_DESCRIPTION, OnChangeEditDescription)
	ON_EN_KILLFOCUS(IDC_EDIT_DESCRIPTION, OnKillfocusEditDescription)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewGroupDlg message handlers

BOOL CNewGroupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	m_Description.SetWindowText(LoadStg(IDS_STRING427));

	CWnd *pwlParent = GetParent();
	ASSERT(pwlParent);
//	if (pwlParent->IsKindOf(RUNTIME_CLASS(CWGRToolDlg)))
//	{
		CWGRToolDlg *pDlg = static_cast<CWGRToolDlg *>(pwlParent);
		pomGroups = &pDlg->m_GroupBox;
//	}

	m_OK.EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNewGroupDlg::OnUpdateEditDescription() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_UPDATE flag ORed into the lParam mask.
	
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	if (!m_GroupDescription.GetLength())
		m_OK.EnableWindow(FALSE);
	else if (!pomGroups || pomGroups->FindStringExact(-1,m_GroupDescription) >= 0)
		m_OK.EnableWindow(FALSE);
	else
		m_OK.EnableWindow(TRUE);
}

void CNewGroupDlg::OnChangeEditDescription() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
}

void CNewGroupDlg::OnKillfocusEditDescription() 
{
	// TODO: Add your control notification handler code here
		
}

void CNewGroupDlg::OnOK() 
{
	// we leave this dialogue with OK only, when the new group name doesn't exist in the group table
	UpdateData(TRUE);
	if (pomGroups && pomGroups->FindStringExact(-1,m_GroupDescription) < 0)
		CDialog::OnOK();
}
