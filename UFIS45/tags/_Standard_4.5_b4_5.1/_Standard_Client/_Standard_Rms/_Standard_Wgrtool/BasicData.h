// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSBasicFunc.h>
#include <CCSLog.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>
#include <CCSTime.h>


int GetItemCount(CString olList, char cpTrenner  = ',' );
CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
CString DeleteListItem(CString &opList, CString olItem);
CString LoadStg(UINT nID);
CString SortItemList(CString opSubString, char cpTrenner);

inline int ExtractItemList2(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{
	popStrArray->RemoveAll();
	return ExtractItemList(opSubString,popStrArray,cpTrenner);		
}

#define	ExtractItemList(opSubString,popStrArray,cpTrenner)	ExtractItemList2(opSubString,popStrArray,cpTrenner)		
//#define	ExtractItemList(opSubString,popStrArray)			ExtractItemList2(opSubString,popStrArray,',')		

/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CCSCedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;

	long GetNextUrno(void);
	bool GetNurnos(int ipNrOfUrnos);
	int imNextOrder;


	int GetNextOrderNo();

	char *GetCedaCommand(CString opCmdType);
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();
	bool GetWindowPosition(CRect& rlPos,CString olMonitor);


public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

private:

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
private:
	CDWordArray omUrnos;
};


#endif
