// RecentRecordList.cpp: implementation of the CRecentRecordList class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <ProfileEditor.h>
#include <RecentRecordList.h>

BOOL AFXAPI AfxComparePath(LPCTSTR lpszPath1, LPCTSTR lpszPath2);

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRecentRecordList::CRecentRecordList( LPCTSTR lpszSection, LPCTSTR lpszEntryFormat, int nSize )
	:CRecentFileList( 0, lpszSection, lpszEntryFormat, nSize )
{

}

CRecentRecordList::~CRecentRecordList()
{

}



// Operations
void CRecentRecordList::Add(LPCTSTR lpszPathName)
{
	ASSERT(m_arrNames != NULL);
	ASSERT(lpszPathName != NULL);
	ASSERT(AfxIsValidString(lpszPathName));

	// update the MRU list, if an existing MRU string matches file name
	for (int iMRU = 0; iMRU < m_nSize-1; iMRU++)
	{
		if (AfxComparePath(m_arrNames[iMRU], lpszPathName))
			break;      // iMRU will point to matching entry
	}
	// move MRU strings before this one down
	for (; iMRU > 0; iMRU--)
	{
		ASSERT(iMRU > 0);
		ASSERT(iMRU < m_nSize);
		m_arrNames[iMRU] = m_arrNames[iMRU-1];
	}
	// place this one at the beginning
	m_arrNames[0] = lpszPathName;
}
