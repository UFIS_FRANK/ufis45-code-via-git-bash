// ConditionsGrid.cpp: implementation of the CConditionsGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <CedaBasicData.h>
#include <Ccsptrarray.h>

#include <ProfileEditor.h>
#include <CcsGlobl.h>
#include <ConditionsGrid.h>
#include <basicdata.h>
#include <utilities.h>
#include <Dialog_ExpressionEditor.h>
#include <ProfileEditorView.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConditionsGrid::CConditionsGrid( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows )
	: CGridControl ( popParent, nID, nCols, nRows )
{

}

CConditionsGrid::~CConditionsGrid()
{

}

void CConditionsGrid::IniLayoutForConditions ()
{
	//----------------------------
	// Every grid initialization that has to be done only once goes inside this function
	//----------------------------

	//--- set number of columns
	SetColCount(8);

	//--- set column widths
	CRect olRect;
	GetClientRect(&olRect);

	olRect.DeflateRect(8, 0);

	int ColWidth0 = 15;

	int ColWidth1 = (int)((olRect.Width()- ColWidth0)* 0.25);
	int ColWidth2 = (int)((olRect.Width()- ColWidth0)* 0.2);
	int ColWidth3 = (int)((olRect.Width()- ColWidth0)* 0.25);
	int ColWidth4 = (int)((olRect.Width()- ColWidth0)* 0.05);
	int ColWidth5 = (int)((olRect.Width()- ColWidth0)* 0.25);

	//---  ComboBox inside grid
	RegisterGxCbsDropDown ( true, true );

	SetColWidth(0, 0, ColWidth0);	// header
	SetColWidth(1, 1, ColWidth1);	// field name
	SetColWidth(SIMPLE_VAL, SIMPLE_VAL, ColWidth2);	// simple value
	SetColWidth(DYNAMIC_GRP, DYNAMIC_GRP, ColWidth3);	// dynamic group
	SetColWidth(PUSHBTN_GRP, PUSHBTN_GRP, ColWidth4);	// pushbutton
	SetColWidth(STATIC_GRP, STATIC_GRP, ColWidth5);	// static groups
	// Hidden columns:
	// 6	-	reference extension:	(EXTE from TSR)
	// 7	-   base tables:			XXX.XXXX   (BTAB, BFLD from TSR)
	// 8	-   empty (as of 990820, MNE)
	//references: TAB.FLDN	(RTAB, RFLD from TSR)

	GetParam()->SetNumberedColHeaders(FALSE);                 
	GetParam()->SetNumberedRowHeaders(FALSE); 
	GetParam()->EnableTrackRowHeight(FALSE); 
	GetParam()->EnableTrackColWidth(FALSE);

	ChangeColHeaderStyle(CGXStyle().SetEnabled(FALSE));
	SetStyleRange(CGXRange(0,1), CGXStyle().SetEnabled(FALSE)
											.SetReadOnly(TRUE)
											.SetValue(GetString(IDS_FELD)) );
	SetValueRange(CGXRange(0,SIMPLE_VAL), GetString(IDS_VALUE) );
	SetValueRange(CGXRange(0,DYNAMIC_GRP), GetString(IDS_KURZ_DYN_GRP) );
	SetValueRange(CGXRange(0,STATIC_GRP), GetString(IDS_KURZ_STAT_GRP) );

	EnableSorting (false);	// disable sorting

	//--- these columns are only used to save data 
	HideCols(6, 8); 

	//--- row height can't be changed
	GetParam()->EnableMoveRows(false);

}

void CConditionsGrid::IniGridFromTplString ( CString &opTPLString )
{
	//      Daten aus FLDA oder FLDT oder FLDD aufbereiten
    CStringArray   olStrArray;
    CString olRefTable;
	CString olRefFieldName;
	CString olRealName;
	CString olChoiceList;
	CString olTabName;
	CString olFieldName;
	CString olFieldNameStr;
	CString olRefTabFldStr;
	CString olTsrUrno;
	
	ClearTable();

	int  ilRecCount  = ExtractItemList(opTPLString, &olStrArray, ';');
	if (ilRecCount > 0)
	{
		// go through string array
		InsertRows(1, ilRecCount);
		EnableGridToolTips(TRUE);		//hag990907
		for(int i = 0; i < ilRecCount; i++)
		{
			CString  olText = olStrArray.GetAt(i); 
			if (olText != CString(""))
			{
				int ilPos  = olText.Find('.');
				olTabName = olText.Mid(ilPos + 1, 3);
				olFieldName = olText.Mid(ilPos + 1 + 4, 4);
			}

			olTsrUrno = ogBCD.GetFieldExt("TSR", "BTAB", "BFLD", olTabName, olFieldName, "URNO");
			if ( olTsrUrno.IsEmpty () )
			{
				TRACE ("Record found in TSR where URNO empty.\n");
				continue;
			}
			olRefTable = ogBCD.GetField("TSR", "URNO", olTsrUrno, "RTAB");
			olRefFieldName = ogBCD.GetField("TSR", "URNO", olTsrUrno, "RFLD");
			//olRealName = ogBCD.GetField("TSR", "URNO", olTsrUrno, "NAME");
			if (!GetNameOfTSRRecord ( olTsrUrno, olRealName ) )
				TRACE("Found no name for TSR-Set with Urno=%s !\n", olTsrUrno );

				
		//--- Spalte 7 : Daten in versteckten Columns speichern
			olFieldNameStr = olTabName + '.' + olFieldName;	// base table, base field
			SetValueRange(CGXRange(i + 1, 7), olFieldNameStr);
		
		//--- Spalte 8 : Daten in versteckten Columns speichern
			olRefTabFldStr = olRefTable + '.' + olRefFieldName;	// reference table, reference field
			SetValueRange(CGXRange(i + 1, 8), olRefTabFldStr);
			
		//--- Spalte 1 : [RealName] 
			SetValueRange(CGXRange(i + 1, 1), olRealName);
			SetStyleRange(CGXRange(i + 1, 1), CGXStyle().SetEnabled(FALSE));

		if ( pomWnd && !olRefTable.IsEmpty() )
			RegisterBC ( pomWnd, "CProfileEditorView", olRefTable, 
						 ProcessBCInView, BC_NEW|BC_DELETED );
				
		//--- Spalte SIMPLE_VAL : [Simple Value] Fill ComboBox if reference to another table
			if(!olRefTable.IsEmpty()) 
			{
				int ilChoiceListSize = ogChoiceLists.GetSize();
				for (int j = 0; j < ilChoiceListSize; j++)
				{
					if (ogChoiceLists[j].TabFieldName == olFieldNameStr)
					{
						olChoiceList = ogChoiceLists[j].ValueList;
						if(olChoiceList.IsEmpty() == FALSE)
						{
							CGXStyle olStyle;
							olStyle.SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
								   .SetChoiceList(olChoiceList);
							if ( !ogChoiceLists[j].ToolTipField.IsEmpty() )
								olStyle.SetItemDataPtr( &(ogChoiceLists[j]) ) ;
							SetStyleRange(CGXRange(i  + 1, SIMPLE_VAL), olStyle );
							j = ilChoiceListSize;	// break loop
						}
					}
				}
			}
			else
			{	//  edit-Feld event. mit speziellem Format
				 CString olValType = ogBCD.GetField("TSR", "URNO", olTsrUrno, "TYPE");
				 if ( olValType == "WDAY" )
					 SetSpecialFormat ( i+1, SIMPLE_VAL, olValType );
			}
			olChoiceList.Empty();

		 //--- Spalte DYNAMIC_GRP: 
			int ilDgrGnamIdx = ogBCD.GetFieldIndex ( "DGR", "GNAM" );
			if(!olRefTable.IsEmpty()) 
			{
			    //--- Gruppen suchen f�r die die RefTab eingetragen ist in TABN
				CString  olTabNameComp = olTabName + '.' + olFieldName;
				CCSPtrArray <RecordSet> olRecordArray;
				//ogBCD.GetRecords("DGR", "REFT", olTabNameComp, &olRecordArray);
				ogBCD.GetRecordsExt("DGR", "REFT", "UTPL", olTabNameComp, 
									ogTPLUrno, &olRecordArray);
 				int ilCount = olRecordArray.GetSize ();

				for(int ilRec = 0; (ilDgrGnamIdx>=0)&&(ilRec<ilCount); ilRec++)
				{
					RecordSet olRecSet = olRecordArray.GetAt(ilRec);
					CString olGroupName = olRecSet[ilDgrGnamIdx];
					olChoiceList += olGroupName + "\n";
				}
				olRecordArray.DeleteAll();

				if(!olChoiceList.IsEmpty())
				SetStyleRange(CGXRange(i + 1, DYNAMIC_GRP),
							  CGXStyle().SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
										.SetChoiceList(olChoiceList));
			
			    //--- Aufr�umen
				olRecordArray.DeleteAll();
				olChoiceList.Empty();
			}

		//---    Spalte PUSHBTN_GRP : PushButton setzen
			if(!olRefTable.IsEmpty()) 
			{
			    CString  olRefExtension;
				olRefExtension = ogBCD.GetField("TSR", "URNO", olTsrUrno, "EXTE");

			    //--- Wenn Exte Felder angegeben sind 
			    if(!olRefExtension.IsEmpty())
				{
					//--- Ref Felder  in versteckter Spalte 6 speichern
					SetValueRange(CGXRange(i + 1, 6), olTsrUrno);
					SetStyleRange(CGXRange(i + 1, PUSHBTN_GRP),
								  CGXStyle().SetControl (GX_IDS_CTRL_PUSHBTN)
											.SetChoiceList("...\n"));
			   }

			}
			else
			{
				SetStyleRange(CGXRange(i + 1, PUSHBTN_GRP), CGXStyle().SetEnabled(FALSE));
				SetStyleRange(CGXRange(i + 1, DYNAMIC_GRP), CGXStyle().SetEnabled(FALSE));
			}

		//---    Spalte STATIC_GRP : Statische Gruppierungen f�r  REF - Tabelle
			if(!olRefTable.IsEmpty()) 
			{
				   //--- Gruppen suchen, f�r die in TABN die RefTab eingetragen ist
				CString olTabNameComp = olRefTable;

				CCSPtrArray <RecordSet> olRecordArray;

				ogBCD.GetRecords("SGR", "TABN", olRefTable, &olRecordArray);

 				int ilCount = olRecordArray.GetSize ();

				//--- Combo Box anlegen mit Daten 
				CString  olChoiceList;

				int ilSgrGrpnIdx = ogBCD.GetFieldIndex( "SGR","GRPN" );
				for(int z = 0; (ilSgrGrpnIdx>=0)&&(z<ilCount); z++)
				{
					RecordSet rc = olRecordArray[z];
  					CString olGroupName = rc.Values[ilSgrGrpnIdx];
					olChoiceList += olGroupName + "\n";
				}
				olRecordArray.DeleteAll();

				if(!olChoiceList.IsEmpty())
					SetStyleRange(CGXRange(i + 1, STATIC_GRP),
							   CGXStyle().SetControl(GX_IDS_CTRL_CBS_DROPDOWN)
									 .SetChoiceList(olChoiceList));

				//--- Aufr�umen
				olRecordArray.DeleteAll();
			}
			else
			{
				SetStyleRange(CGXRange(i + 1, STATIC_GRP), CGXStyle().SetEnabled(FALSE));
			}
		}
	}
	
}
		
void CConditionsGrid::DisplayInfo ( ROWCOL nRow, ROWCOL nCol, bool bpToolTip/*=false*/ )
{
   	CString olWert, olAnzeige;
	//CString	olRefTable, olRefCodeField;
	CHOICE_LISTS	*polChoice;
	CGXStyle		olStyle;
	CGXControl		*polControl=0;
	ROWCOL			ilColWithChoiceList;

	ilColWithChoiceList	= (nCol==STATIC_GRP) ? SIMPLE_VAL : nCol;
	ComposeStyleRowCol( nRow, ilColWithChoiceList, &olStyle );
	if ( !olStyle.GetIncludeItemDataPtr () )
		return;
	if ( ( polControl = GetControl( nRow, nCol ) )  &&
		 polControl->GetValue(olWert) )
	{
		polChoice = (CHOICE_LISTS*)olStyle.GetItemDataPtr();
		//if ( polChoice && !olWert.IsEmpty() && 
		//	 ParseReftFieldEntry ( polChoice->RefTabField, olRefTable, olRefCodeField ) )
		//	olAnzeige = ogBCD.GetField( olRefTable, olRefCodeField, olWert, 
		//								polChoice->ToolTipField );
		if ( polChoice && !olWert.IsEmpty() )
		{
			if ( nCol == ilColWithChoiceList )
				olAnzeige = GetToolTip ( polChoice, olWert );
			else
				olAnzeige = GetGrpToolTip ( polChoice, olWert );
		
		}

		//  Nur OnEndEditing den Tooltip setzen, w�hrend des Editierens wird er
		//	eh' nicht angezeigt und au�erdem wird das n�chste Zeichen als erstes
		//	interpretiert
		if ( bpToolTip )
		{
			SetStyleRange( CGXRange(nRow, nCol), 
						   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olAnzeige ));
		}
	}
	SetStatusBarText ( olAnzeige );
}

void CConditionsGrid::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol)
{
	if( nCol != PUSHBTN_GRP)
		return; 
	//--- Expression Editor anlegen
    CString  olAuswahl = GetValueRowCol(nRow, DYNAMIC_GRP ); 
	CString  olUrno;
			
	bool err = ogBCD.GetField("DGR", "GNAM", olAuswahl, "URNO", olUrno);
			
	CString  olTSRUrnoStr;
	
	olTSRUrnoStr = GetValueRowCol(nRow, 6); 
    CString olRef;
	olRef = GetValueRowCol(nRow, 7);
    CDialog_ExpressionEditor olDialog(ogTPLUrno, olUrno, olRef, olTSRUrnoStr, this);
	olDialog.DoModal();

	//--- Text in Auswahl anzeigen 
	if(!olDialog.omDgrName.IsEmpty())
	{
       SetValueRange(CGXRange(nRow, DYNAMIC_GRP ), olDialog.omDgrName);
	}
	
	//--- ComboBox neu aufbauen
 	CCSPtrArray <RecordSet> olRecordArray;
    CString  olRefFieldName;
	CString olChoiceList;
	CString olGroupName;
	RecordSet olRecord;

	olRefFieldName = GetValueRowCol(nRow, 7);
	//ogBCD.GetRecords("DGR", "REFT", olRefFieldName, &olRecordArray);
	ogBCD.GetRecordsExt("DGR", "REFT", "UTPL", olRefFieldName, ogTPLUrno, 
						&olRecordArray);
 	int ilSize = olRecordArray.GetSize ();
    //olRecordArray.Sort(&StringComp);

	int ilDgrGnamIdx = ogBCD.GetFieldIndex ( "DGR", "GNAM" );
	for(int ilLC = 0; ilLC < ilSize; ilLC++) 
	{
		olRecord = olRecordArray.GetAt(ilLC);
		olGroupName = olRecord[ilDgrGnamIdx];
		olChoiceList += olGroupName + "\n";
	}
	olRecordArray.DeleteAll();

	if(!olChoiceList.IsEmpty())
		SetStyleRange ( CGXRange(nRow, DYNAMIC_GRP ), 
						CGXStyle().SetControl(GX_IDS_CTRL_CBS_DROPDOWN) 
							      .SetChoiceList(olChoiceList));
      //--- Aufr�umen
	 olRecordArray.DeleteAll();
}

void CConditionsGrid::ResetValues()
{
	//--------------------------
	//  resets data in visible columns (except column 1)
	//--------------------------

	//--- exception handling
	CCS_TRY


	int ilRowCount;

	ilRowCount = GetRowCount();
	if (ilRowCount > 0)
	{
		SetValueRange( CGXRange(1, 2, ilRowCount, 5),CString(""));
		SetStyleRange( CGXRange(1, 2, ilRowCount, 5), 
					   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, ""));
	}
	//--- exception handling
	CCS_CATCH_ALL
}


void CConditionsGrid::ModifyChoiceList ( CString opTable, bool bpAdd, 
										 RecordSet *popData )
{
	CString olCompareString, olRefTable = opTable+".";
	CString olItem, olRtab, olRfld;
	int ilChangeCol = SIMPLE_VAL;
	int ilCompareCol =  8;	//  spalte mit Referencetable und -field

	

	if ( opTable == "SGR" )
	{
		ilChangeCol = STATIC_GRP;
		olRefTable = popData->Values[ogBCD.GetFieldIndex("SGR","TABN")]+".";
		olItem = popData->Values[ogBCD.GetFieldIndex("SGR","GRPN")];
	}
	else
		if ( opTable == "DGR" )
		{
			ilChangeCol = DYNAMIC_GRP;
			olRefTable = popData->Values[ogBCD.GetFieldIndex("DGR","REFT")];
			ilCompareCol =  7;	//  spalte mit Basetable und -field
			olItem = popData->Values[ogBCD.GetFieldIndex("DGR","GNAM")];
			if ( popData->Values[ogBCD.GetFieldIndex("DGR","UTPL")] != ogTPLUrno )
				return;	//  dynam. Gruppe geh�rt nicht zum Template
		}
	for ( ROWCOL i=1; i<= GetRowCount(); i++ )
	{
		olCompareString = GetValueRowCol(i, ilCompareCol);
		if ( olCompareString.Find ( olRefTable ) >= 0 )		
		{	//  in dieser Zeile ist Auswahlliste der Combobox zu �ndern
			if ( ( ilChangeCol == SIMPLE_VAL ) && 
				 ParseReftFieldEntry ( olCompareString, olRtab, olRfld ) )
			{
				int ilCodeFieldIdx = ogBCD.GetFieldIndex ( olRtab, olRfld ) ;
				if ( ilCodeFieldIdx >= 0 )
					olItem = popData->Values[ilCodeFieldIdx];
			}
			if ( !olItem.IsEmpty() )
				ModifyChoiceListRowCol ( i, ilChangeCol, GX_IDS_CTRL_CBS_DROPDOWN, 
										 olItem, bpAdd );
		}
	}	
}
				 
BOOL CConditionsGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{

	CString olCellText ;
	olCellText = GetValueRowCol(nRow, nCol);

	//  Reset tooltips
	SetStyleRange(CGXRange(nRow, 1, nRow, 5), CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, ""));

	if (nRow && !olCellText.IsEmpty() && ( olCellText != " " ) )
	{
		
		if (nCol == SIMPLE_VAL)
		{
			SetValueRange(CGXRange(nRow, STATIC_GRP), CString(""));
			SetValueRange(CGXRange(nRow, DYNAMIC_GRP), CString(""));

			if (FormatSpecial(nRow, nCol, olCellText))
			{
				SetValueRange(CGXRange(nRow, nCol), olCellText);
			}
			
		}

		if (nCol == STATIC_GRP)
		{
			SetValueRange(CGXRange(nRow, SIMPLE_VAL),  CString(""));
			SetValueRange(CGXRange(nRow, DYNAMIC_GRP),  CString(""));
		}
		
		if (nCol == DYNAMIC_GRP)
		{
			SetValueRange(CGXRange(nRow, SIMPLE_VAL),  CString(""));
			SetValueRange(CGXRange(nRow, STATIC_GRP),  CString(""));
		}
	}

	MarkInValid(nRow, nCol, olCellText);

	return CGridControl::OnEndEditing(nRow, nCol);
}


CString CConditionsGrid::ConstructPrioString()
{
	CString olRet;
	CString olSimple, olStatic, olDynamic;

	for (ROWCOL ilRow = 1; ilRow <= GetRowCount(); ilRow++ )
	{
		olSimple = GetValueRowCol(ilRow, SIMPLE_VAL);
		olStatic = GetValueRowCol(ilRow, STATIC_GRP);
		olDynamic = GetValueRowCol(ilRow, DYNAMIC_GRP);

		if ( !olSimple.IsEmpty() &&  (olSimple != " ") )
			olRet += "3";
		else
			if ( !olStatic.IsEmpty() &&  (olStatic != " ") )
				olRet += "2";
		else
			if ( !olDynamic.IsEmpty() &&  (olDynamic != " ") )
				olRet += "1";
		else
			olRet += "0";
	}

	return olRet;
}

void CConditionsGrid::SetToolTipForValue ( ROWCOL nRow, ROWCOL nCol, CString &ropValue )
{
   	CString			olAnzeige;
	CHOICE_LISTS	*polChoice;
	CGXStyle		olStyle;
	ROWCOL			ilColWithChoiceList;

	ilColWithChoiceList	= (nCol==STATIC_GRP) ? SIMPLE_VAL : nCol;
	ComposeStyleRowCol( nRow, ilColWithChoiceList, &olStyle );
	if ( !olStyle.GetIncludeItemDataPtr () )
		return;
	polChoice = (CHOICE_LISTS*)olStyle.GetItemDataPtr();
	if ( polChoice && !ropValue.IsEmpty() )
	{
		if ( nCol == ilColWithChoiceList )
			olAnzeige = GetToolTip ( polChoice, ropValue );
		else
			olAnzeige = GetGrpToolTip ( polChoice, ropValue );
	}
	SetStyleRange( CGXRange(nRow, nCol), 
				   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olAnzeige ));
}
