#if !defined(AFX_PROFILECHART_H__40A0C751_B393_11D3_93B9_00001C033B5D__INCLUDED_)
#define AFX_PROFILECHART_H__40A0C751_B393_11D3_93B9_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProfileChart.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProfileChart form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include <ccsedit.h>
#include <chartdata.h>

class CGridControl;
class CProfileEditorView;
class CHistogramm;


class CProfileChart : public CFormView
{
public:
	CProfileChart();           // protected constructor used by dynamic creation

	virtual ~CProfileChart();
	DECLARE_DYNCREATE(CProfileChart)

// Form Data
public:
	//{{AFX_DATA(CProfileChart)
	enum { IDD = IDD_PROFILE_CHART };
	CCSEdit	omNameEdit;
	CSliderCtrl	m_ZoomSlider;
	CTabCtrl	omClassesTab;
	CEdit	m_IntervalEdit;
	CSpinButtonCtrl	m_IntervalSpinBtn;
	int		m_Intervall;
	//}}AFX_DATA

protected:
	bool			bmHistogrammVisible;
	bool			bmEditMode;
	CValuesGrid		*pomValueList;		
	CHistogramm		*pomHistogramm;		
	CChartData		*pomDisplayedData;
	int				imCx;
	int				imCySmall;
	int				imCyLarge;
	CProfileEditorView *pomDiagramm;
	float			fmMinFaktorX, fmMaxFaktorX;

// Attributes
public:

// Operations
public:
	BOOL Create( const RECT& rect, CWnd* pParentWnd ) ;
	void Display ( bool bpDisplay );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProfileChart)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

// Implementation
protected:
	void EnableControls ();
	void OnNewClass ( long lpClassUrno );
	void OnDeleteClass ( long lpClassUrno );
	void OnChangeClass ( long lpClassUrno );
	void DisplayClass ( long lpClassUrno );
	void OnSelectTab ( int ipSelected );
	void DisplayDocument ();
	void SaveMofifications();
	void UpdateDisplay (bool bpNeu=true);
	void DecideDisplays(bool bpDataExists);
	bool GetColor ( COLORREF &ripColor );
	void IniGraphicSettings();
	void FillColorCtrl( LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF &ripColor );
	void IniStatics();
	void SetZoomSliderPos ( float fpFaktorX );
	float GetZoomSliderPos ();
	void IniIntervalSpinBtn ();
	void  OnChangePattern ( long lpPatternUrno );

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CProfileChart)
	afx_msg void OnOpenProfile();
	afx_msg void OnKillfocusNameEdit();
	afx_msg void OnKillfocusIntervalEdit();
	afx_msg void OnEditProfile();
	afx_msg void OnSwitchDisplay();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSelchangeClassesTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNewProfile();
	afx_msg void OnSaveProfile();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnMarkColor();
	afx_msg void OnRegColor();
	afx_msg void OnBkColor();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG
	void OnPrepareClosing ( WPARAM wparam, LPARAM lparam );
	void OnRedrawHistogramm( WPARAM wparam, LPARAM lparam );
	void OnCCSEditKillFocus ( WPARAM wparam, LPARAM lparam );
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROFILECHART_H__40A0C751_B393_11D3_93B9_00001C033B5D__INCLUDED_)
