// ProfileEditor.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <AatHelp.h>

#include <ProfileEditor.h>

#include <gxall.h>
#include <CCSGlobl.h>
#include <CCSlog.h>
#include <CCSCedaData.h>
#include <CedaBasicData.h>
#include <VersionInfo.h>

#include <MainFrm.h>
#include <ChildFrm.h>
#include <ProfileEditorDoc.h>
#include <ProfileEditorView.h>
#include <logindlg.h>
#include <InitialLoadDlg.h>
#include <BasicData.h>
#include <OpenProfileDlg.h>
#include <utilities.h>
#include <RecordInfoDlg.h> 
#include <Recentrecordlist.h> 
#include <RegisterDlg.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>
#include <PrivList.h>
#include <LibGlobl.h>


extern const TCHAR _afxFileSection[];
extern const TCHAR _afxFileEntry[]; 

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorApp

BEGIN_MESSAGE_MAP(CProfileEditorApp, CWinApp)
	//{{AFX_MSG_MAP(CProfileEditorApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorApp construction

CProfileEditorApp::CProfileEditorApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CProfileEditorApp object

CProfileEditorApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorApp initialization

BOOL CProfileEditorApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

//	LoadStdProfileSettings();  // Load standard INI file options (including MRU)
	m_pRecentFileList = new CRecentRecordList( _afxFileSection, _afxFileEntry, 4);
	m_pRecentFileList->ReadList();

	AfxEnableControlContainer();

	if (!AfxOleInit())
	{
	//	AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}


	GXInit();

	// Standard CCS-Fonts and Brushes initialization
    InitFont();
	CreateBrushes();
	ReadColorSettings ();


		//--- initialize CEDA

	char pclHomeAirport[_MAX_PATH+1];

	if (getenv("CEDA") != NULL)
	{
		strcpy(pcmConfigPath, getenv("CEDA"));
	}
	else
	{
		strcpy(pcmConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	
	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pcmConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}

	//--- path to logfile for ccslog
	char	pclTmp[128], pclDBParam[11];
	CString olErrorTxt;


	GetPrivateProfileString("ProfileEditor", "LOGFILE", CCSLog::GetUfisSystemPath("\\ProfileEditor.LOG"), pclTmp, sizeof(pclTmp), pcmConfigPath);
	ogLog.Load(pclTmp);

	GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof(pcgTableExt), pcmConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pclHomeAirport, sizeof(pclHomeAirport), pcmConfigPath);

	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pclHomeAirport);
	strcpy(CCSCedaData::pcmApplName, ogAppName);

	VersionInfo olInfo;
	VersionInfo::GetVersionInfo(NULL,olInfo);
	CCSCedaData::bmVersCheck = true;
	strcpy(CCSCedaData::pcmVersion, olInfo.omFileVersion);
	strcpy(CCSCedaData::pcmInternalBuild, olInfo.omPrivateBuild);

	ogCommHandler.SetAppName(ogAppName);
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }

	ogBCD.SetTableExtension(pcgTableExt);

	//  text-Tabelle Lesen
	
	GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof(pclTmp), pcmConfigPath);
   	GetPrivateProfileString(ogAppName, "DBParam", "0", pclDBParam, sizeof pclDBParam, pcmConfigPath);
	bool blLangInitOk = ogGUILng->MemberInit(&olErrorTxt, "XXX", "PROFILE", pclHomeAirport, 
											 pclTmp, pclDBParam );
	//--- Help initialization
	if (!AatHelp::Initialize(ogAppName))
	{
		CString olErrorMsg;
		AatHelp::State elErrorState;
		elErrorState = AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}

	//--- Login Dialog
#if	0
	CLoginDialog olLoginDlg(pcgHome, ogAppName, NULL);

	if ( olLoginDlg.DoModal() == IDCANCEL )
		return FALSE;

	if(ogPrivList.GetStat("InitModu") == '1')
	{
		
		RegisterDlg olRegisterDlg;
		if (olRegisterDlg.DoModal() != IDOK)
		{
			return FALSE;
		}
		
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName("ProfileEditor");
	CString olT = ogInitModuData.GetInitModuTxt();
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());
	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}

	strcpy(pcgUserName,olLoginCtrl.GetUserName_());
	strcpy(pcgPassWord,olLoginCtrl.GetUserPassword());

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif


	LoadData ();
	MakeChoiceLists();	
	
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_PROFILTYPE,
		RUNTIME_CLASS(CProfileEditorDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CProfileEditorView));
	AddDocTemplate(pDocTemplate);

	//TranslateMenu (pDocTemplate->m_hMenuShared, IDR_PROFILTYPE);
	TranslateProfileMenu (pDocTemplate->m_hMenuShared);

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	//::TranslateMenu (pMainFrame->m_hMenuDefault, IDR_MAINFRAME);
	TranslateMainMenu (pMainFrame->m_hMenuDefault );
	
	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();

	return TRUE;
}


CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
	pomAktDocument = 0;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_ABOUT_PROFILE, OnAboutProfile)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CProfileEditorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorApp message handlers


int CProfileEditorApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
	DeleteBrushes();
	SaveColorSettings ();
	ogChoiceLists.DeleteAll();
	delete CGUILng::TheOne();
	ogGUILng = 0;

	ogCommHandler.CleanUpCom();
	AatHelp::ExitApp();

	return CWinApp::ExitInstance();
}

bool CProfileEditorApp::LoadData ()
{
    CInitialLoadDlg			*polLoadDialog = 0;
	CString					olSort, olWhere, olTplFldd, olText, olMsg ;
	int						ilRet, ilRecCount, ilStep, ilTablesToLoad;
	CCSPtrArray <REFTABLE>	olRefTables;
	CMapStringToPtr			olRefTablesMap;
	REFTABLE				*prlTable;
	bool					blRet = true;
//	CString	olLoadOneStr = GetString ( IDS_LOADING_EINZAHL );
//	CString	olLoadMultipleStr = GetString (	IDS_LOADING_MEHRZAHL );

	// *** Load Data from Ceda
    //  nun im Konstruktor: ogBCD.SetTableExtension(pcgTableExt);

	polLoadDialog = new CInitialLoadDlg(CWnd::GetActiveWindow());

	// TPL (templates)
	//polLoadDialog->SetMessage("TPL");		//1444
	polLoadDialog->SetMessage(GetString(IDS_LOAD_TEMPLATES));		
    if ( ogBCD.SetObject("TPL") )
		ilRet = ogBCD.Read("TPL", "WHERE APPL='PROFILE'");
	polLoadDialog->SetProgress(5);

	ilRecCount = ogBCD.GetDataCount ( "TPL" );
	if ( ilRecCount <= 0 )
	{
		//::MessageBox ( 0, "No template for PAX profiles found.", "", MB_OK );
		LangMsgBox ( 0, IDS_NO_TEMPLATE_FOUND, 0 );
		blRet = false;
	}
	else
	{		
		CString			olStr, olBtab, olBfld, olTsrUrno, olRtab, olRfld;
		CStringArray	olRefts;
		int				ilCount, ilInd ;
		BOOL			blFound, blNewField;
		char			pclEntry[21], pclInfoField[11];
	
		// TSR (template source)
		//polLoadDialog->SetMessage("TSR");	//1443
		polLoadDialog->SetMessage(GetString(IDS_LOAD_TPL_SOURCES));		
		if (  ogBCD.SetObject("TSR") )
			ilRet = ogBCD.Read("TSR");
		polLoadDialog->SetProgress(5);

		/*  Beispiel:
		TPL.FLDD:
		1.AFT.HTYP;2.AFT.TTYP;3.AFT.ALC2;4.AFT.ALC3;5.AFT.FLTN;6.AFT.FLNS;7.AFT.ACT3;8.AFT.DES3;9.AFT.VIA3;10.AFT.DOOD
		TSR where rfld='ACT3' or bfld='ACT3';

		BTAB       BFLD       RTAB       RFLD
		---------- ---------- ---------- ----------
		AFT        ACT3       ACT        ACT3
		AZA        ACT3       ACT        ACT3        */

		olTplFldd = ogBCD.GetField( "TPL", 0, "FLDD" );
		ogTPLUrno = ogBCD.GetField( "TPL", 0, "URNO" );

		ilCount = ExtractItemList( olTplFldd, &olRefts, ';' );
		for ( int i=0; i<ilCount; i++ )
		{
			olStr = olRefts[i];
			ilInd = olStr.Find('.', 0 );
			if ( ilInd < 0 ) 
				continue;
			olStr = olStr.Right(olStr.GetLength()-ilInd-1);
			if ( !ParseReftFieldEntry ( olStr, olBtab, olBfld ) )
				continue;
			olTsrUrno = ogBCD.GetFieldExt("TSR", "BTAB", "BFLD", olBtab, olBfld, "URNO" );
			if ( olTsrUrno.IsEmpty () ||
				 !ogBCD.GetFields( "TSR", "URNO", olTsrUrno, "RTAB", "RFLD", 
								   olRtab, olRfld ) )
				continue;
			if ( olRtab.IsEmpty() || olRfld.IsEmpty() )
				continue;
			if ( olBtab == olRtab )	//  hag991207 und tsc:  sonst h�ngt sich das 
				continue;			//  System mit BTab=Rtab='AFT' auf
		
			blFound = olRefTablesMap.Lookup ( olRtab, (void *&)prlTable ) ;
			blNewField = !blFound;
			sprintf ( pclEntry, "%s_Info", olRtab );
			GetPrivateProfileString ( "REGELWERK", pclEntry, "", pclInfoField,
									  11, pcmConfigPath);

			if ( !blFound )
			{
				prlTable = new REFTABLE;
				prlTable->RTAB = olRtab;
				prlTable->Fields.AddTail(olRfld);
				
				//  URNO und InfoFeld beim 1. Mal einf�gen
				if ( olRfld != "URNO" )
					prlTable->Fields.AddTail ( "URNO" ) ;

				if ( pclInfoField[0] && (olRfld!=pclInfoField) )
					prlTable->Fields.AddTail ( pclInfoField ) ;

				olRefTables.Add(prlTable);
				olRefTablesMap.SetAt(olRtab, prlTable );
			}
			else
			{
				if ( !prlTable->Fields.Find( olRfld ) )
				{
					prlTable->Fields.AddTail(olRfld);
					blNewField = TRUE;
				}
			}
			if ( blNewField )
			{
				CHOICE_LISTS olChoice;
				olChoice.TabFieldName = olBtab + CString (".") + olBfld;
				olChoice.RefTabField = olRtab + CString (".") + olRfld;
				olChoice.ToolTipField = pclInfoField;
				ogChoiceLists.New(olChoice);
			}
		}
	}

	ilTablesToLoad = olRefTables.GetSize()+10;
	ilStep = 90 / ilTablesToLoad;

	// SYS (SYSTAB)
	//polLoadDialog->SetMessage("SYS");	//1456
	polLoadDialog->SetMessage(GetString(IDS_LOAD_SYSTAB));		
	if ( ogBCD.SetObject("SYS", "URNO,FELE,FINA,TANA") )
		ilRet = ogBCD.Read("SYS");
	polLoadDialog->SetProgress(ilStep);

	// dynamic groups
	//polLoadDialog->SetMessage("DGR");	//1438
	polLoadDialog->SetMessage(GetString(IDS_LOAD_DYN_GRP));		
	if ( ogBCD.SetObject("DGR") )
	{
		ilRet = ogBCD.Read("DGR");
		SetBCTypes ( "DGR" );
	}
	polLoadDialog->SetProgress(ilStep);

	//--- load all tables referred to by TSR
		// This is how I do it: I use a CCSPtrArray with REFTABLEs {CString olRtab, CStringList Fields}
		// If TSR.RTAB isn't empty, I look if that table name (RTAB) is already in my PtrArray. 
		// If so, I look through the Fields-Array to see if I already gathered the field and add it to the StringArray if
		// necessary.
		// If not, I add a new struct with the table and write the field as first String to the StringArray
		// Besides, the table names will be used to register the necessary DDX functions in our main window later on

	// read all referred tables (TSR) into memory
	for (int i = 0; i < olRefTables.GetSize(); i++)
	{
		CString olFieldList, olTmp, olWhere;
		//char    pclEntry[21], pclInfoField[11];
		POSITION pos;

		//if ( olRefTables[i].Fields.Find( "URNO" ) < 0 )	//  nun oben
		//	olRefTables[i].Fields.AddTail ( "URNO" ) ;

		//sprintf ( pclEntry, "%s_Info", olRefTables[i].RTAB );		//  nun oben
		//GetPrivateProfileString ( "REGELWERK", pclEntry, "", pclInfoField, 11, 
		//						  pcmConfigPath);
		//if ( pclEntry[0] )
		//	olRefTables[i].Fields.AddTail ( pclEntry ) ;

		pos = olRefTables[i].Fields.GetHeadPosition();
		while ( pos )
		{
			olTmp = olRefTables[i].Fields.GetNext(pos); 
			olFieldList += olTmp;
			if ( pos )
				olFieldList += ",";
		}

		//polLoadDialog->SetMessage(olRefTables[i].RTAB);		//1451

		olText = GetString(IDS_LOAD_TABLE) + " ";
		olMsg = olText + olRefTables[i].RTAB;
		polLoadDialog->SetMessage(olMsg);		

		if ( ogBCD.SetObject(olRefTables[i].RTAB, olFieldList) )
		{
			SetWhereValidString ( olRefTables[i].RTAB, olWhere );
			ogBCD.Read(olRefTables[i].RTAB, olWhere);
			SetBCTypes ( olRefTables[i].RTAB );

			olSort = olRefTables[i].Fields.GetHead() ;	
			if ( !olSort.IsEmpty() )
			{
				olSort += "+";
				ogBCD.SetSort(olRefTables[i].RTAB, olSort, true );
			}
		}
		polLoadDialog->SetProgress(ilStep);
	}
	olRefTablesMap.RemoveAll ( );
	olRefTables.DeleteAll ();
	
	// SGR 
	//polLoadDialog->SetMessage("SGR");	//1439
	polLoadDialog->SetMessage(GetString(IDS_LOAD_STAT_GRP));		
	
	if ( ogBCD.SetObject("SGR") )
	{
		ilRet = ogBCD.Read("SGR");
		SetBCTypes ( "SGR" );
		if ( !ilRet )									
			ogBCD.SetSort ("SGR", "GRPN+", true );
	}
	polLoadDialog->SetProgress(ilStep);

	// SGM
	//polLoadDialog->SetMessage("SGM");
	if ( ogBCD.SetObject("SGM") )
		ilRet = ogBCD.Read("SGM");
	polLoadDialog->SetProgress(ilStep);

	// VAL
	//polLoadDialog->SetMessage("VAL");	//1446
	polLoadDialog->SetMessage(GetString(IDS_LOAD_VALTAB));		
	//if ( ogBCD.SetObject("VAL", "", false ) )
	if ( ogBCD.SetObject("VAL" ) )
		ilRet = ogBCD.Read("VAL","WHERE TABN = \'PRO\'" );
	polLoadDialog->SetProgress(ilStep);

	// CCC
	olText = GetString(IDS_LOADING_MEHRZAHL) ;
	olMsg = GetString(IDS_CCC_HEADER)  + olText;
	//polLoadDialog->SetMessage("CCC");
	polLoadDialog->SetMessage(olMsg);
	if ( ogBCD.SetObject("CCC") )
	{
		ilRet = ogBCD.Read("CCC");
		SetBCTypes ( "CCC" );
	}	
	polLoadDialog->SetProgress(ilStep);

	// PRO
	//polLoadDialog->SetMessage("PRO");
	olMsg = GetString(IDS_PROFILES)  + olText;
	polLoadDialog->SetMessage(olMsg);
	if ( ogBCD.SetObject("PRO", "", false ) )
	{
		ilRet = ogBCD.Read("PRO");
		SetBCTypes ( "PRO");
	}
	polLoadDialog->SetProgress(ilStep);

	// PXC
	//polLoadDialog->SetMessage("PXC");
	if ( ogBCD.SetObject("PXC", "", false ) )
	{
		ilRet = ogBCD.Read("PXC");
		SetBCTypes ( "PXC");
	}
	polLoadDialog->SetProgress(ilStep);

	// PXA
	//polLoadDialog->SetMessage("PXA");
	olMsg = GetString(IDS_PXA_HEADER)  + olText;
	polLoadDialog->SetMessage(olMsg);
	if ( ogBCD.SetObject("PXA", "", false ) )
	{
		ilRet = ogBCD.Read("PXA");
		SetBCTypes ( "PXA");
	}
	polLoadDialog->SetProgress(ilStep);

	// VTP
	//polLoadDialog->SetMessage("VTP");
	if ( ogBCD.SetObject("VTP", "", false ) )
		ilRet = ogBCD.Read("VTP");
	polLoadDialog->SetProgress(ilStep);

	polLoadDialog->DestroyWindow ();

	IniGlobalIndices ();
	return blRet;
}

//  Auswahllisten, die bereits in CProfileEditorApp::LoadData angelegt worden
//  sind, nun mit den Daten aus den Referenztabellen f�llen
void CProfileEditorApp::MakeChoiceLists()
{
	CString		olRefTable;
	CString		olRefFieldName;
	CString		olChoiceList;	
	RecordSet	olRecord;
	bool blOk;

	int ilCount = ogChoiceLists.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		blOk = ParseReftFieldEntry ( ogChoiceLists[i].RefTabField, olRefTable,
									 olRefFieldName );
		if(blOk && !olRefTable.IsEmpty()) 
		{	  
			 //--- ChoiceList anlegen mit Daten aus Referenz-Tabelle
			 CedaObject *prlObject;
			 int ilSize = 0;

			 if ( ogBCD.omObjectMap.Lookup((LPCSTR)olRefTable, (void *&)prlObject) )
			 {
				 CString olRefFieldData;
				 int ilFieldIdx = ogBCD.GetFieldIndex(olRefTable, olRefFieldName);
				 ilSize = prlObject->Data.GetSize();
				 
				 olChoiceList.Empty();
				 for(int ilLC = 0; ilLC < ilSize; ilLC++)
				 {
 					olRefFieldData = prlObject->Data[ilLC].Values[ilFieldIdx];
					
					if(!olRefFieldData.IsEmpty())
						   olChoiceList += olRefFieldData + "\n";
				 }
			 }
			 ogChoiceLists[i].ValueList = olChoiceList;
		}
	}
	
}

//////////////////////////////////////////////////////////////////////////////////////

//  IN:		ropReftEntry "Tablename.Fieldname" z.B: PST.PNAM
//  OUT:	ropResTable: z.B: "PST"
//  OUT:	ropResCodeField: z.B: "PNAM"
bool ParseReftFieldEntry ( CString &ropReftEntry, CString &ropResTable, 
						   CString &ropResCodeField ) 
{
	char *pclTemp, *ps;

	pclTemp = new char[ropReftEntry.GetLength() + 1];
	if ( !pclTemp )
		return false;
	strcpy ( pclTemp, (const char*)ropReftEntry );
	if ( ps = strchr ( pclTemp, '.' ) )
	{
		*ps = '\0';
		ropResTable = pclTemp;
		ropResCodeField = ps+1;
	}
	delete pclTemp;
	return (ps!=0);
}

void SetWhereValidString ( CString opTableName, CString &ropWhere )
{
	CString olTime;
	char    pclWhere[101]="";
	CTime	olToday;
	CString olVatoField = ogBCD.GetFieldExt( "SYS", "TANA", "FINA", opTableName, "VATO", "FINA" );
	

	if ( olVatoField == "VATO" )
	{	//  Tabelle 'opTableName' beinhaltet Feld "VATO"
		olToday = CTime::GetCurrentTime();
		olTime = CTimeToDBString(olToday, olToday);
		sprintf ( pclWhere, "WHERE (VATO=' ' OR VATO='' OR VATO>'%s')", (LPCSTR)olTime );
	}
	ropWhere = pclWhere;
}


//  String von TSR.TEXT:
//	'NAME(IDS_XXX)#ACWS(IDS_WINGSPAN)|SEAT(IDS_SITZE)'	-->
//  ropNames[0]= STRG from TXTTAB where TXID='IDS_XXX'
//	ropNames[1]= STRG from TXTTAB where TXID='IDS_WINGSPAN'
//	ropNames[2]= STRG from TXTTAB where TXID='IDS_SITZE'

bool GetNameOfTSRRecord ( CString &ropUrno, CString &ropName )
{
	RecordSet	olRecord;
	CString		olTSRText;
	int			n;
	int			ilTsrTextIdx, ilTsrNameIdx;

	if  ( !ogBCD.GetRecord ( "TSR", "URNO", ropUrno, olRecord ) )
		return false;
	ilTsrTextIdx = ogBCD.GetFieldIndex ( "TSR", "TEXT" );
	ilTsrNameIdx = ogBCD.GetFieldIndex ( "TSR", "NAME" );
	if ( (ilTsrTextIdx<0) || (ilTsrNameIdx<0) )
		return false;
	olTSRText = olRecord.Values[ilTsrTextIdx];
	n = olTSRText.Find('#') ;
	if ( n>0 ) 
	{
		olTSRText = olTSRText.Left ( n-1 );
		if ( GetStringForTSRTextEntry ( olTSRText, ropName ) )
			return true;
	}
	//  keine (vorhandene) TXID in TSR.TEXT gefunden 
	ropName = olRecord.Values[ilTsrNameIdx];
	return true;
}

bool FillNamesArray ( CString &ropUrno, CStringArray &ropNames )
{
	RecordSet		olRecord;
	CStringArray	olIDList;	
	CString			olTSRText, olText, olTSRTextRight;
	int				n;
	int				ilTsrTextIdx, ilTsrNameIdx;

	ropNames.RemoveAll();
	if  ( !ogBCD.GetRecord ( "TSR", "URNO", ropUrno, olRecord ) )
		return false;
	ilTsrTextIdx = ogBCD.GetFieldIndex ( "TSR", "TEXT" );
	ilTsrNameIdx = ogBCD.GetFieldIndex ( "TSR", "NAME" );
	if ( (ilTsrTextIdx<0) || (ilTsrNameIdx<0) )
		return false;
	olTSRText = olRecord.Values[ilTsrTextIdx];
	n = olTSRText.Find('#') ;
	if ( n<=0 ) 
		return false;
	
	CutLeft( olTSRText, olTSRTextRight, "#" );
	if ( !GetStringForTSRTextEntry ( olTSRText, olText ) )
		olText = olRecord.Values[ilTsrNameIdx];
	ropNames.Add ( olText );

	n = ExtractItemList( olTSRTextRight, &olIDList, '|' );
	for ( int i=0; i<n; i++ )
	{
		olTSRText = olIDList[i];
		if ( !GetStringForTSRTextEntry ( olTSRText, olText ) )
			olText = "";
		ropNames.Add ( olText );
	}
	return true;
}

bool GetStringForTSRTextEntry ( CString opEntry, CString &ropString )
{
	char	pclText[41], *ps;
	CString	olStrID, olUrno ;
	
	//if ( ogBCD.GetDataCount("TXT") <= 0 )
	//	return false;

	strcpy ( pclText, opEntry.Left (40) );
	ps = strchr ( pclText, ')' );
	if ( ps )
		*ps = '\0';
	ps = strchr ( pclText, '(' );
	olStrID = ps ? ps+1 : pclText;

	return ogGUILng->GetString(&ropString, "TXID", "APPL", olStrID, "TSRTAB" );
	/*
	olUrno = ogBCD.GetFieldExt( "TXT", "TXID", "APPL", olStrID, "TSRTAB", "URNO" );
	if ( !olUrno.IsEmpty() )
		return ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", ropString ) ;
	else 
		return false;
	*/
}



void IniGlobalIndices ()
{
	// VALTAB
	igValVafrIdx = ogBCD.GetFieldIndex ( "VAL", "VAFR" );
	igValVatoIdx = ogBCD.GetFieldIndex ( "VAL", "VATO" );
	igValFreqIdx = ogBCD.GetFieldIndex ( "VAL", "FREQ" );
	igValTimtIdx = ogBCD.GetFieldIndex ( "VAL", "TIMT" );
	igValTimfIdx = ogBCD.GetFieldIndex ( "VAL", "TIMF" );
	igValUvalIdx = ogBCD.GetFieldIndex ( "VAL", "UVAL" );
	igValApplIdx = ogBCD.GetFieldIndex ( "VAL", "APPL" );
	igValUrnoIdx = ogBCD.GetFieldIndex ( "VAL", "URNO" );
	igValTabnIdx = ogBCD.GetFieldIndex ( "VAL", "TABN" );
	igPxcTtbgIdx = ogBCD.GetFieldIndex ( "PXC", "TTBG" );
	igPxcTtwoIdx = ogBCD.GetFieldIndex ( "PXC", "TTWO" );

}


void CProfileEditorApp::OnFileOpen() 
{
	// TODO: Add your command handler code here
	COpenProfileDlg olDlg(m_pMainWnd);
	if ( olDlg.DoModal()==IDOK )
	{
		CString	olProfileName;
		olProfileName = ogBCD.GetField ( "PRO", "URNO", olDlg.omSelProfileUrno, "NAME" );
		if ( !olProfileName.IsEmpty() )
		{
			OpenDocumentFile(olProfileName);
		}
	}

}



CDocument* CProfileEditorApp::FindDocument(LPCTSTR lpszDocTitle )
{
	POSITION	posTpl = GetFirstDocTemplatePosition() ;
	POSITION	posDoc ;
	CDocument	*polDoc;
	CString olStr;
	while ( posTpl )
	{
		CDocTemplate* pTemplate = GetNextDocTemplate(posTpl);
		if ( pTemplate ) 
		{
			posDoc = pTemplate->GetFirstDocPosition ();
			pTemplate->GetDocString ( olStr, CDocTemplate::docName ); 
			while ( posDoc )
			{
				polDoc = pTemplate->GetNextDoc(posDoc);
				if ( polDoc->GetTitle() == lpszDocTitle )
					return polDoc;
			}
		}
	}
	return 0;
}

CDocument* CProfileEditorApp::OpenDocumentFile(LPCTSTR lpszFileName) 
{
	// TODO: Add your specialized code here and/or call the base class
	// Ist bereits ein View mit diesem Document offen, so aktieviere diesen
	CString olTitle;
	char *ps;
	ps = strrchr ( lpszFileName, '\\' );
	olTitle = ( ps ) ? ps+1 : lpszFileName;

	CDocument* pOpenDocument = FindDocument ( olTitle );

	if (pOpenDocument)
	{
		POSITION pos = pOpenDocument->GetFirstViewPosition();
		if (pos != NULL)
		{
			CView* pView = pOpenDocument->GetNextView(pos); // get first one
			ASSERT_VALID(pView);
			CFrameWnd* pFrame = pView->GetParentFrame();
			if (pFrame != NULL)
				pFrame->ActivateFrame();
			else
				TRACE0("Error: Can not find a frame for document to activate.\n");
			CFrameWnd* pAppFrame;
			if (pFrame != (pAppFrame = (CFrameWnd*)AfxGetApp()->m_pMainWnd))
			{
				ASSERT_KINDOF(CFrameWnd, pAppFrame);
				pAppFrame->ActivateFrame();
			}
		}
		else
		{
			TRACE0("Error: Can not find a view for document to activate.\n");
		}
		return pOpenDocument;
	}
	CDocTemplate* polTemplate = FindTemplate( "Profil" );
	return (polTemplate ? polTemplate->OpenDocumentFile( olTitle ) : 0 );
}

CDocTemplate* CProfileEditorApp::FindTemplate(LPCTSTR lpszDocName )
{
	POSITION	posTpl = GetFirstDocTemplatePosition() ;
	CString		olStr;
	while ( posTpl )
	{
		CDocTemplate* pTemplate = GetNextDocTemplate(posTpl);
		if ( pTemplate && pTemplate->GetDocString ( olStr, CDocTemplate::docName ) &&
			 (olStr==lpszDocName) )
			return pTemplate;
	}
	return 0;
}

bool CProfileEditorApp::IsDocumentPointer (void *pData )
{
	POSITION	posTpl = GetFirstDocTemplatePosition() ;
	POSITION	posDoc ;
	CDocument	*polDoc;

	while ( posTpl )
	{
		CDocTemplate* pTemplate = GetNextDocTemplate(posTpl);
		if ( pTemplate ) 
		{
			posDoc = pTemplate->GetFirstDocPosition ();
			while ( posDoc )
			{
				polDoc = pTemplate->GetNextDoc(posDoc);
				if ( (void*)polDoc == pData )
					return true;
			}
		}
	}
	return false;
}


BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString	olStr, olForm;
	VersionInfo olInfo;

	BOOL blEnable = pomAktDocument && !pomAktDocument->omUrno.IsEmpty();
	EnableDlgItem ( this, IDC_ABOUT_PROFILE, blEnable, TRUE ); 

	SetWindowLangText ( this, IDS_ABOUT_TITLE );
	SetDlgItemLangText ( this, IDC_ABOUT_PROFILE, IDS_PROFILE_INFO );

	
	olStr = GetString(IDS_UFISVERSION);
	SetDlgItemText(IDC_UFIS_VERSION, olStr);

	VersionInfo::GetVersionInfo(NULL,olInfo);

	olForm = GetString(IDS_PROFILEEDITRO_VERSION);
	olStr.Format ( olForm, olInfo.omFileVersion);
	olStr += "  ";
	olStr += CString(__DATE__);
	SetDlgItemText(IDC_PROFILEEDITOR_VERSION, olStr);

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CAboutDlg::DoModal(CProfileEditorDoc *popDoc) 
{
	// TODO: Add your specialized code here and/or call the base class
	pomAktDocument = popDoc;	
	return CDialog::DoModal();
}

void CAboutDlg::OnAboutProfile() 
{
	// TODO: Add your control notification handler code here
	CRecordInfoDlg dlg(this);
	RecordSet		olRecord;
	
	if ( pomAktDocument && !pomAktDocument->omUrno.IsEmpty() &&
		 ogBCD.GetRecord("PRO", "URNO", pomAktDocument->omUrno, olRecord ) )
		dlg.DoModal(&olRecord, "PRO");
	
}

void CProfileEditorApp::OnChangedColors ()
{
	SaveColorSettings ();
	m_pMainWnd->SendMessageToDescendants ( WM_UPDATEDIAGRAM );
}

void CProfileEditorApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	AatHelp::WinHelp(dwData, nCmd);
}
