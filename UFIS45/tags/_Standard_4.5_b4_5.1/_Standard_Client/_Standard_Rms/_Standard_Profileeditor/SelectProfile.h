#if !defined(AFX_SELECTPROFILE_H__9E9F7B03_B6BC_11D3_93BB_00001C033B5D__INCLUDED_)
#define AFX_SELECTPROFILE_H__9E9F7B03_B6BC_11D3_93BB_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectProfile.h : header file
//

class CHistogramm;
class CChartData;

/////////////////////////////////////////////////////////////////////////////
// CSelectProfile dialog

class CSelectProfile : public CDialog
{
// Construction
public:
	CSelectProfile(CWnd* pParent = NULL);   // standard constructor
	~CSelectProfile();

// Dialog Data
	//{{AFX_DATA(CSelectProfile)
	enum { IDD = IDD_SELECT_PROFILE };
	CListBox	m_ProfileLb;
	CString	m_SelectedProfile;
	//}}AFX_DATA
	CString omSelectedUrno;
	CHistogramm		*pomPreview;		
	CChartData		*pomPreviewData;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectProfile)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
public:
	virtual int DoModal(const char* pcpProfileName=0);

// Implementation
protected:
	void IniListBox ();
	void IniStatics ();

	// Generated message map functions
	//{{AFX_MSG(CSelectProfile)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDblclkProlifeLb();
	afx_msg void OnDelete();
	afx_msg void OnCopy();
	afx_msg void OnSelchangeProlifeLb();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTPROFILE_H__9E9F7B03_B6BC_11D3_93BB_00001C033B5D__INCLUDED_)
