#if !defined(AFX_OPENPROFILEDLG_H__828FD5A4_C8CC_11D3_93C7_00001C033B5D__INCLUDED_)
#define AFX_OPENPROFILEDLG_H__828FD5A4_C8CC_11D3_93C7_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OpenProfileDlg.h : header file
//

class CGridControl;

/////////////////////////////////////////////////////////////////////////////
// COpenProfileDlg dialog

class COpenProfileDlg : public CDialog
{
// Construction
public:
	COpenProfileDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COpenProfileDlg)
	enum { IDD = IDD_OPEN_PROFILE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

protected:
	int				imProfilesCount;
	CButton			pomDlgButtons[3];
	CGridControl	*pomProfileList;
	int				imLastWidth;		//  letzte Gr��e der client area
	int				imLastHeight;		//				"
public:
	CString			omSelProfileUrno;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COpenProfileDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void IniGrid ();
	void PreDestroyWindow ();
	void IniStatics();
	void IniColWidths ();

	// Generated message map functions
	//{{AFX_MSG(COpenProfileDlg)
	afx_msg void OnEditFind();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg void OnGridLbDblClicked ( WPARAM wparam, LPARAM lparam );
	afx_msg void OnGridLbClicked( WPARAM wparam, LPARAM lparam );
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPENPROFILEDLG_H__828FD5A4_C8CC_11D3_93C7_00001C033B5D__INCLUDED_)
