// ProfileChart.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <ProfileEditor.h>
#include <ProfileChart.h>
#include <ProfileEditorView.h>
#include <gridcontrol.h>
#include <utilities.h>
#include <ValuesGrid.h>
#include <SelectProfile.h>
#include <ProfileEditorDoc.h>
#include <childfrm.h>
#include <histogramm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProfileChart

IMPLEMENT_DYNCREATE(CProfileChart, CFormView)

CProfileChart::CProfileChart()
	: CFormView(CProfileChart::IDD)
{
	//{{AFX_DATA_INIT(CProfileChart)
	m_Intervall = 10;
	//}}AFX_DATA_INIT
	bmHistogrammVisible = true;
	bmEditMode = false;
	pomValueList = new CValuesGrid;
	pomDisplayedData = 0;
	pomHistogramm = new CHistogramm;
	fmMinFaktorX = 0.25;
	fmMaxFaktorX = 5.0;
}

CProfileChart::~CProfileChart()
{
	if ( pomValueList )
		delete pomValueList;
	if ( pomHistogramm )
		delete pomHistogramm;
}

void CProfileChart::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProfileChart)
	DDX_Control(pDX, IDC_NAME_EDIT, omNameEdit);
	DDX_Control(pDX, IDC_ZOOM_SLIDER, m_ZoomSlider);
	DDX_Control(pDX, IDC_CLASSES_TAB, omClassesTab);
	DDX_Control(pDX, IDC_INTERVAL_EDIT, m_IntervalEdit);
	DDX_Control(pDX, IDC_INTERVAL_SPIN, m_IntervalSpinBtn);
	DDX_Text(pDX, IDC_INTERVAL_EDIT, m_Intervall);
	DDV_MinMaxInt(pDX, m_Intervall, 1, 1200);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProfileChart, CFormView)
	//{{AFX_MSG_MAP(CProfileChart)
	ON_BN_CLICKED(IDC_OPEN_PROFILE, OnOpenProfile)
	ON_EN_KILLFOCUS(IDC_NAME_EDIT, OnKillfocusNameEdit)
	ON_EN_KILLFOCUS(IDC_INTERVAL_EDIT, OnKillfocusIntervalEdit)
	ON_BN_CLICKED(IDC_EDIT_PROFILE, OnEditProfile)
	ON_BN_CLICKED(IDC_SWITCH_DISPLAY, OnSwitchDisplay)
	ON_WM_VSCROLL()
	ON_WM_CREATE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_CLASSES_TAB, OnSelchangeClassesTab)
	ON_BN_CLICKED(IDC_NEW_PROFILE, OnNewProfile)
	ON_BN_CLICKED(IDC_SAVE_PROFILE, OnSaveProfile)
	ON_WM_KILLFOCUS()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_MARK_COLOR, OnMarkColor)
	ON_BN_CLICKED(IDC_REG_COLOR, OnRegColor)
	ON_BN_CLICKED(IDC_BK_COLOR, OnBkColor)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnCCSEditKillFocus)
	ON_MESSAGE(WM_PREPARE_CLOSING,OnPrepareClosing)
	ON_MESSAGE(WM_UPDATEDIAGRAM, OnRedrawHistogramm)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProfileChart diagnostics

#ifdef _DEBUG
void CProfileChart::AssertValid() const
{
	CFormView::AssertValid();
}

void CProfileChart::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProfileChart message handlers

void CProfileChart::OnOpenProfile() 
{
	// TODO: Add your control notification handler code here
	CSelectProfile olDlg;
	bool			blLoaded=false;

	if ( !pomDisplayedData )
		return ;
	if ( olDlg.DoModal (pomDisplayedData->omProfileName) == IDOK )
	{
		//pomDisplayedData->AskForSaveChanges ();
		SaveMofifications();
		pomDisplayedData->omUval = olDlg.omSelectedUrno;

		CChartData *polAktData;
		CProfileEditorDoc	*polDoc = (CProfileEditorDoc*)GetDocument();
		if ( polDoc )
		{	/*	if selected pattern is already used in this document, use it 
				(it could be already modified) */
			for ( int i=0; !blLoaded &&(i<polDoc->omChartData.GetSize()); i++ )
			{	
				polAktData = &(polDoc->omChartData[i]);
				if ( polAktData == pomDisplayedData )
					continue;
				if ( polAktData && (polAktData->omUval == pomDisplayedData->omUval) )
				{
					pomDisplayedData->CopyValues ( (const CChartData&)*polAktData );
					pomDisplayedData->imMinStep = polAktData->imMinStep;
					polAktData->bmPXAModified = polAktData->bmPXAModified;
					pomDisplayedData->bmVTPModified = polAktData->bmVTPModified;
					pomDisplayedData->omProfileName = polAktData->omProfileName;
					blLoaded = true;
				}
			}
		}
		if ( !blLoaded )
		{
			blLoaded = pomDisplayedData->ReadData (pomValueList);
			blLoaded &= pomDisplayedData->CalcDspData ();
		}
		if ( bmEditMode )
			m_Intervall = pomDisplayedData->imMinStep;
		else
			m_Intervall = pomDisplayedData->imDspStep;
		UpdateDisplay ();

		pomDisplayedData->SetModified (PXC_CHANGED);
		UpdateData ( FALSE );
		IniIntervalSpinBtn ();
		SetDlgItemText ( IDC_NAME_EDIT, pomDisplayedData->omProfileName  );
	}
}

void CProfileChart::OnKillfocusNameEdit() 
{
	// TODO: Add your control notification handler code here
	if ( pomDisplayedData )
	{
		CString olText;
		if ( olText != pomDisplayedData->omProfileName )
		{
			pomDisplayedData->omProfileName = olText;
		}
		GetDlgItemText ( IDC_NAME_EDIT, pomDisplayedData->omProfileName );

	}
}

void CProfileChart::OnKillfocusIntervalEdit() 
{
	// TODO: Add your control notification handler code here
	int ilSaveOldValue = m_Intervall;
	
	if ( bmEditMode && pomValueList->IsGridDirty() )
		pomValueList->SaveData ( pomDisplayedData );

	if ( pomDisplayedData )
		ilSaveOldValue = bmEditMode ? pomDisplayedData->imMinStep 
								    : pomDisplayedData->imDspStep;
	bool blOk=true;
	bool blChanged = false;
	if ( UpdateData () && pomDisplayedData )
	{
		blChanged = (ilSaveOldValue != m_Intervall);
		if ( !bmEditMode )
		{
			blOk = pomDisplayedData->SetDspStep ( m_Intervall );
			//pomValueList->DisplayData ( &omData, bmEditMode );
		}
		else
			blOk = pomDisplayedData->SetMinStep ( m_Intervall );
		if ( !blOk )
		{
			m_Intervall = ilSaveOldValue;
			UpdateData(FALSE);
			m_IntervalEdit.SetFocus();
			m_IntervalEdit.SetSel( 0, -1 );
			MessageBox ( GetString(IDS_WRONG_STEP_WIDTH), "", MB_OK );
		}
		else
			if ( blChanged && !bmEditMode )
				UpdateDisplay (false);
		m_IntervalSpinBtn.SetPos ( m_Intervall );
	}
}

void CProfileChart::OnEditProfile() 
{
	// TODO: Add your control notification handler code here
	if ( !pomDisplayedData )
		return ;
	if ( bmEditMode && pomValueList->IsGridDirty() )
	{
		pomValueList->SaveData ( pomDisplayedData );
	}
	bmEditMode = !bmEditMode ;
	if ( !bmEditMode )
		pomDisplayedData->SetDspStep ((pomDisplayedData->imDspStep/pomDisplayedData->imMinStep)*pomDisplayedData->imMinStep);


	DisplayClass ( pomDisplayedData->lmCounterClass );
	EnableControls();
}


void CProfileChart::EnableControls ()
{
	CProfileEditorDoc	*polDoc = (CProfileEditorDoc*)GetDocument();
	bool blDataExist = polDoc && (polDoc->omChartData.GetSize() > 0 );

	EnableDlgItem ( this, IDC_OPEN_PROFILE, true, blDataExist );
	EnableDlgItem ( this, IDC_NEW_PROFILE, bmEditMode, bmEditMode&&blDataExist );
	EnableDlgItem ( this, IDC_SAVE_PROFILE, bmEditMode, bmEditMode&&blDataExist );
	EnableDlgItem ( this, IDC_EDIT_PROFILE, true, blDataExist );
	EnableDlgItem ( this, IDC_NAME_EDIT, bmEditMode, blDataExist );
	EnableDlgItem ( this, IDC_NAME_TXT, true, blDataExist );
	EnableDlgItem ( this, IDC_SWITCH_DISPLAY, true, !bmEditMode&&blDataExist );
	omClassesTab.ShowWindow ( blDataExist ? SW_SHOW : SW_HIDE );
	//pomValueList->ShowWindow ( blDataExist ? SW_SHOW : SW_HIDE );
	DecideDisplays ( blDataExist );
	m_IntervalEdit.ShowWindow ( blDataExist ? SW_SHOW : SW_HIDE );
	m_IntervalSpinBtn.ShowWindow ( blDataExist ? SW_SHOW : SW_HIDE );
	EnableDlgItem ( this, IDC_INTERVAL_TXT, true, blDataExist );


	//SetDlgItemText ( IDC_SWITCH_DISPLAY, bmHistogrammVisible ? "List" : "Histogramm" );
	SetDlgItemLangText ( this, IDC_SWITCH_DISPLAY, 
						 bmHistogrammVisible ? IDS_LIST : IDS_GRAPHIC );
	//SetDlgItemText ( IDC_INTERVAL_TXT, bmEditMode ? "Min. interval" : "actual interval" );
	SetDlgItemLangText ( this, IDC_INTERVAL_TXT, 
						 bmEditMode ? IDS_MIN_INTERVAL : IDS_ACT_INTERVAL );
	//SetDlgItemText ( IDC_EDIT_PROFILE, bmEditMode ? "Display" : "Edit" );
	SetDlgItemLangText ( this, IDC_EDIT_PROFILE, 
						 bmEditMode ? IDS_DISPLAY : IDS_BEARBEITEN );
}

void CProfileChart::OnSwitchDisplay() 
{
	// TODO: Add your control notification handler code here
	bmHistogrammVisible = !bmHistogrammVisible;
	EnableControls ();
	//DecideDisplays(pomDisplayedData!=0);
}


void CProfileChart::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default

	if ( (void*)pScrollBar == &m_IntervalSpinBtn )
	{
		m_Intervall = nPos; 
		UpdateData ( FALSE );
		if ( !bmEditMode )
			OnKillfocusIntervalEdit();
	}	
	CFormView::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CProfileChart::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	CRect olRect;
	CWnd *polWnd;

	omNameEdit.SetTypeToString("X(30)",30,1);		
	omNameEdit.SetBKColor(YELLOW);  
	omNameEdit.SetTextErrColor(RED);

	GetWindowRect ( &olRect );
	imCx = olRect.Width();
	imCyLarge = 
		imCySmall  = olRect.Height();
	if ( polWnd = GetDlgItem ( IDC_HISTOGRAMM_FRAME ) )
	{
		polWnd->GetWindowRect ( &olRect );
		ScreenToClient ( &olRect ); 
		imCySmall = olRect.top + 2;
	}

	pomValueList->Open ( this, IDC_HISTOGRAMM_FRAME );
	pomHistogramm->Open ( this, IDC_HISTOGRAMM_FRAME );
	//pomValueList->ShowWindow ( bmHistogrammVisible ? SW_HIDE : SW_SHOW );
	//pomHistogramm->ShowWindow ( bmHistogrammVisible ? SW_SHOW : SW_HIDE );
	IniGraphicSettings();
	IniStatics();	
	DisplayDocument ();
	EnableControls();
	
	int ilClasses = omClassesTab.GetItemCount();
	Display ( ilClasses>0 );

	RedrawWindow();
}

int CProfileChart::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void CProfileChart::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	CProfileEditorDoc	*polDoc = (CProfileEditorDoc*)GetDocument();
	CDWordArray *polUrnoArray=0;
	if ( polDoc )
	{
		if ( (lHint==HINT_CLASS_DELETED) || (lHint==HINT_CLASS_CREATED) || 
			 (HINT_CLASS_CHANGED)  || (lHint==HINT_PATTERN_CHANGED) )
			polUrnoArray = (CDWordArray*)pHint;
		if ( polUrnoArray )
			for ( int i=0; i<polUrnoArray->GetSize(); i++ )
			{
				if ( lHint==HINT_CLASS_CREATED )
					OnNewClass ( polUrnoArray->ElementAt ( i ) );
				if ( lHint==HINT_CLASS_DELETED ) 
					OnDeleteClass ( polUrnoArray->ElementAt ( i ) );
				if ( lHint==HINT_CLASS_CHANGED ) 
					OnChangeClass ( polUrnoArray->ElementAt ( i ) );
				if ( lHint==HINT_PATTERN_CHANGED ) 
					OnChangePattern ( polUrnoArray->ElementAt ( i ) );
			}
	}
}

void CProfileChart::OnNewClass ( long lpClassUrno )
{
	int ilClasses = omClassesTab.GetItemCount();
	CString	olClassName = GetClassName ( lpClassUrno );

	omClassesTab.InsertItem( TCIF_TEXT|TCIF_PARAM, ilClasses, olClassName, 0, lpClassUrno );
	if ( !pomDisplayedData )
	{
		omClassesTab.SetCurSel ( 0 );
		OnSelectTab ( omClassesTab.GetCurSel() );
	}
	ilClasses = omClassesTab.GetItemCount();
	Display (ilClasses>0) ;
}

void CProfileChart::OnDeleteClass ( long lpClassUrno )
{
	int ilClasses = omClassesTab.GetItemCount();
	int ilSelected = omClassesTab.GetCurSel();
	TCITEM	slItem;
	
	for ( int i=ilClasses-1; i>=0; i-- )
		if ( omClassesTab.GetItem( i, &slItem ) && 
			(slItem.lParam==lpClassUrno) )
			omClassesTab.DeleteItem(i);
	ilClasses = omClassesTab.GetItemCount();
	//if ( ilClasses )
	//	omClassesTab.SetCurSel ( max(0,ilSelected-1) );
	
	omClassesTab.SetCurSel ( ilClasses ? max(0,ilSelected-1) : -1 );
	OnSelectTab ( omClassesTab.GetCurSel() );
	Display (ilClasses>0) ;
}

void CProfileChart::OnChangeClass ( long lpClassUrno )
{
	int ilSelected = omClassesTab.GetCurSel();
	TCITEM	slItem;
	if ( ( ilSelected>=0 ) && omClassesTab.GetItem( ilSelected, &slItem ) &&
		 ( slItem.lParam==lpClassUrno ) 
	   )
		DisplayClass ( lpClassUrno );
}

void CProfileChart::OnSelchangeClassesTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int ilSelected = omClassesTab.GetCurSel( ) ;
	if ( pomDisplayedData && bmEditMode && pomValueList->IsGridDirty () )
		pomValueList->SaveData ( pomDisplayedData );
	OnSelectTab ( ilSelected );
	*pResult = 0;
}

void CProfileChart::DisplayClass ( long lpClassUrno )
{
	CProfileEditorDoc	*polDoc = (CProfileEditorDoc*)GetDocument();
	if ( polDoc )
	{
		int ilIndex = polDoc->FindClass(lpClassUrno) ;
		pomDisplayedData = (ilIndex>=0) ? &(polDoc->omChartData[ilIndex]) : 0;
	}
	else
		pomDisplayedData = 0;
	if ( pomDisplayedData )
	{
		m_Intervall = bmEditMode ? pomDisplayedData->imMinStep 
								 : pomDisplayedData->imDspStep ;
		CString olText;
		olText = GetProfileName ( pomDisplayedData->omUval );
		SetDlgItemText ( IDC_NAME_EDIT, olText );
		UpdateData(FALSE);

		IniIntervalSpinBtn ();
		UpdateDisplay ();
	}	
}

void CProfileChart::IniIntervalSpinBtn ()
{
	m_IntervalSpinBtn.SetPos ( m_Intervall );
	m_IntervalSpinBtn.SetRange ( bmEditMode ? 1 : pomDisplayedData->imMinStep, 1200 );
	m_IntervalSpinBtn.SetBuddy ( &m_IntervalEdit );
	UDACCEL rlAccel;
	rlAccel.nInc = bmEditMode ? 1 : pomDisplayedData->imMinStep;
	rlAccel.nSec = 0;
	m_IntervalSpinBtn.SetAccel( 1, &rlAccel );
}

void CProfileChart::OnSelectTab ( int ipSelected )
{
	TCITEM	slItem;
	long    llClassUrno=-1;
	if ( (ipSelected>=0) && omClassesTab.GetItem ( ipSelected, &slItem ) )
		llClassUrno = slItem.lParam;
	DisplayClass ( llClassUrno );
	EnableControls();

}

void CProfileChart::DisplayDocument ()
{
	omClassesTab.DeleteAllItems ();

	CProfileEditorDoc	*polDoc = (CProfileEditorDoc*)GetDocument();
	if ( polDoc )
	{
		for ( int i=0; i<polDoc->omChartData.GetSize(); i++ )
			OnNewClass ( polDoc->omChartData[i].lmCounterClass );
	}
}

void CProfileChart::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	// TODO: Add your specialized code here and/or call the base class
	RECT rect;
	GetWindowRect(&rect);
	if ( !bActivate )
	{
		OnPrepareClosing ( 0,0 );

	}
	CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

void CProfileChart::OnNewProfile() 
{
	// TODO: Add your control notification handler code here
	if ( pomDisplayedData )
	{
		//pomDisplayedData->AskForSaveChanges ();
		SaveMofifications();

		pomDisplayedData->Reset();
		UpdateDisplay ();
		SetDlgItemText ( IDC_NAME_EDIT, "" );
	}	
		
}

void CProfileChart::OnSaveProfile() 
{
	// TODO: Add your control notification handler code here
	if ( pomDisplayedData )
	{
		if ( bmEditMode && pomValueList->IsGridDirty() )
			pomValueList->SaveData ( pomDisplayedData );
		pomDisplayedData->SavePXA ();
	}
}

void CProfileChart::Display ( bool bpDisplay )
{
	CRect	olFrameRect, olPaneRect;
	int		cy1, cyMin;
	CChildFrame	*polFrame = (CChildFrame*)GetParentFrame();
	
	polFrame->GetWindowRect ( &olFrameRect );
	polFrame->omMainSplitter.GetRowInfo(1, cy1, cyMin );
	CSize olTotal = GetTotalSize ();
	GetClientRect ( olPaneRect );
	cy1 = olFrameRect.Height() - cy1;
	if ( bpDisplay )
		cy1 += olTotal.cy;
	polFrame->SetWindowPos( 0, 0, 0, olFrameRect.Width(), cy1, 
						    SWP_NOMOVE | SWP_NOZORDER  );
	
}

void CProfileChart::OnPrepareClosing ( WPARAM wparam, LPARAM lparam )
{
	if ( bmEditMode && pomValueList->IsGridDirty() )
		pomValueList->SaveData ( pomDisplayedData );
}

void CProfileChart::SaveMofifications()
{
	OnPrepareClosing ( 0, 0 );
	pomDisplayedData->AskForSaveChanges ();
		
}

void CProfileChart::OnKillFocus(CWnd* pNewWnd) 
{
	//static bool blProcessingKillfocus=false;
	CFormView::OnKillFocus(pNewWnd);
	
	SendMessageToDescendants( WM_KILLFOCUS, 0, 0, TRUE );
	// TODO: Add your message handler code here
	/*
	blProcessingKillfocus = true;
	OnPrepareClosing ( 0,0 );
	SendMessageToDescendants( WM_KILLFOCUS, 0, 0, TRUE );
	blProcessingKillfocus = false;
	RECT rect;
	GetWindowRect(&rect);*/
}

void CProfileChart::UpdateDisplay ( bool bpNeu /*=true*/ )
{
	if ( !pomDisplayedData )
		return;
	pomValueList->DisplayData ( pomDisplayedData, bmEditMode );
	if ( !bmEditMode )
	{
		pomHistogramm->imColumnWidth = pomDisplayedData->imDspStep;
		pomHistogramm->DisplayData ( pomDisplayedData, bpNeu );
	}
}

void CProfileChart::DecideDisplays(bool bpDataExists)
{
	bool blDspHisto = !bmEditMode&&bmHistogrammVisible;
	pomValueList->ShowWindow ( bpDataExists&&!blDspHisto ? SW_SHOW : SW_HIDE );
	pomHistogramm->ShowWindow ( bpDataExists&&blDspHisto ? SW_SHOW : SW_HIDE );
	
	//  Show Controls for graphic settings 
	BOOL blShow = bpDataExists&&blDspHisto ;
	EnableDlgItem ( this, IDC_GRAPHSET_GRP, blShow, blShow );
	EnableDlgItem ( this, IDC_COLORS_TXT, blShow, blShow );
	EnableDlgItem ( this, IDC_BK_COLOR, blShow, blShow );
	EnableDlgItem ( this, IDC_REG_COLOR, blShow, blShow );
	EnableDlgItem ( this, IDC_PREVIEW_STAT, blShow, blShow );
	EnableDlgItem ( this, IDC_MARK_COLOR, blShow, blShow );
	EnableDlgItem ( this, IDC_BK_TXT, blShow, blShow );
	EnableDlgItem ( this, IDC_REGULAR_TXT, blShow, blShow );
	EnableDlgItem ( this, IDC_MARKED_TXT, blShow, blShow );
	EnableDlgItem ( this, IDC_TIMESCALE_TXT, blShow, blShow );

	m_ZoomSlider.ShowWindow ( blShow ? SW_SHOW : SW_HIDE );	
}

void CProfileChart::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	if ( pScrollBar && pomHistogramm &&
		( pScrollBar->m_hWnd == m_ZoomSlider.m_hWnd ) )
	{
		//pomHistogramm->fmUnitsX = ((float)m_ZoomSlider.GetPos()) / 100;
		pomHistogramm->fmUnitsX = GetZoomSliderPos ();
		if ( pomDisplayedData )
			pomHistogramm->DisplayData ( pomDisplayedData, false );
	}
	CFormView::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CProfileChart::OnMarkColor() 
{
	// TODO: Add your control notification handler code here
	COLORREF ilColor;
	if ( pomHistogramm )
	{
		ilColor = igMarkedColColor;
		if ( GetColor ( ilColor ) )
		{
			igMarkedColColor = ilColor;
			//pomHistogramm->RedrawWindow ();
			theApp.OnChangedColors();
		}	
	}
}

void CProfileChart::OnRegColor() 
{
	// TODO: Add your control notification handler code here
	COLORREF ilColor;
	if ( pomHistogramm )
	{
		ilColor = igRegularColColor;
		if ( GetColor ( ilColor ) )
		{
			igRegularColColor = ilColor;
			//pomHistogramm->RedrawWindow ();
			theApp.OnChangedColors();
		}	
	}
	
}

void CProfileChart::OnBkColor() 
{
	// TODO: Add your control notification handler code here
	COLORREF ilColor;
	if ( pomHistogramm )
	{
		ilColor = igBkColor;
		if ( GetColor ( ilColor ) )
		{
			igBkColor = ilColor;
			//pomHistogramm->RedrawWindow ();
			theApp.OnChangedColors();
		}	
	}
	
}

bool CProfileChart::GetColor ( COLORREF &ripColor )
{
	COLORREF ilNewColor;
	CColorDialog dlg(ripColor, CC_RGBINIT|CC_PREVENTFULLOPEN, this );
	if ( dlg.DoModal () == IDOK )
	{
		ilNewColor = dlg.GetColor ();
		if ( ilNewColor != ripColor ) 
		{
			ripColor = ilNewColor;
			return true;
		}
	}
	return false;
}

void CProfileChart::IniGraphicSettings()
{
	if ( pomHistogramm )
	{
		int ilPixels = pomHistogramm->omViewPort.Width();
		//float flFactorX = (float)ilPixels / 2000;  // Pixel/minute bei 1500min im Fenster
		//int ilFactorX = (int)(flFactorX*100);
		//m_ZoomSlider.SetRange ( ilFactorX, 500, FALSE );
		//m_ZoomSlider.SetPos ( 100 );

		//  Besser eine logarithmische Unterteilung zu verwenden
		fmMinFaktorX = (float)ilPixels / 2000;
		m_ZoomSlider.SetRange ( 0, 100, FALSE );
		SetZoomSliderPos ( 1 );
	}
}

void CProfileChart::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your message handler code here and/or call default
	if ( nIDCtl == IDC_MARK_COLOR )
		FillColorCtrl ( lpDrawItemStruct, igMarkedColColor );

	if ( nIDCtl == IDC_REG_COLOR )
		FillColorCtrl ( lpDrawItemStruct, igRegularColColor );

	if ( nIDCtl == IDC_BK_COLOR )
		FillColorCtrl ( lpDrawItemStruct, igBkColor );
	
	CFormView::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CProfileChart::FillColorCtrl ( LPDRAWITEMSTRUCT lpDrawItemStruct, 
								    COLORREF &ripColor )
{
	CDC *pCDC = CDC::FromHandle ( lpDrawItemStruct->hDC );
	CRect olRect ( lpDrawItemStruct->rcItem );
	olRect.DeflateRect ( 1, 1 );
	if ( pCDC )
		pCDC->FillSolidRect ( &olRect, ripColor );
}

void CProfileChart::OnRedrawHistogramm( WPARAM wparam, LPARAM lparam )
{
	RedrawWindow ();
	if ( pomHistogramm )
		pomHistogramm->RedrawWindow ();
}

void CProfileChart::IniStatics()
{
	SetDlgItemLangText ( this, IDC_NAME_TXT, IDS_NAME );
	SetDlgItemLangText ( this, IDC_NEW_PROFILE, IDS_NEU );
	SetDlgItemLangText ( this, IDC_SAVE_PROFILE, IDS_SAVE );
	SetDlgItemLangText ( this, IDC_OPEN_PROFILE, IDS_OPEN_TXT );

	SetDlgItemLangText ( this, IDC_GRAPHSET_GRP, IDS_GRAPHIC_SETTINGS );
	SetDlgItemLangText ( this, IDC_TIMESCALE_TXT, IDS_TIMESCALE );
	SetDlgItemLangText ( this, IDC_COLORS_TXT, IDS_COLORS );
	SetDlgItemLangText ( this, IDC_BK_TXT, IDS_BACKGROUND );
	SetDlgItemLangText ( this, IDC_REGULAR_TXT, IDS_REGULAR_COLUMNS );
	SetDlgItemLangText ( this, IDC_MARKED_TXT, IDS_MARKED_COLUMNS );
	SetDlgItemLangText ( this, IDC_PATTERN_GRP, IDS_PXA_HEADER );
}

void CProfileChart::SetZoomSliderPos ( float fpFaktorX )
{
	float flRange = fmMaxFaktorX - fmMinFaktorX;
	float flQuotient, flPos, flExpValue;
	int   ilPos;
	//  WerteBereich f�r fpFaktorX: fmMinFaktorX...fmMinFaktorX
	fpFaktorX = min ( fpFaktorX, fmMaxFaktorX );	
	fpFaktorX = max ( fpFaktorX, fmMinFaktorX );
	flQuotient = ( fpFaktorX - fmMinFaktorX ) / flRange;
	//  WerteBereich f�r flExpValue: 1..10
	flExpValue = 1 + 9*flQuotient;					
	//  WerteBereich f�r flPos: 0..1	
	flPos = (float)log10 (flExpValue);
	//  WerteBereich f�r ilPos: 0..100
	ilPos = (int)(flPos*100);
	m_ZoomSlider.SetPos ( ilPos );
}

float CProfileChart::GetZoomSliderPos ()
{
	float flRange = fmMaxFaktorX - fmMinFaktorX;
	float flQuotient, flPos, flFaktorX, flExpValue;
	int   ilPos;

	//  WerteBereich f�r ilPos: 0..100
	ilPos = m_ZoomSlider.GetPos ();
	//  WerteBereich f�r flPos: 0..1	
	flPos = (float)ilPos / 100;
	//  WerteBereich f�r flExpValue: 1..10
	flExpValue = (float)pow ( 10, flPos );
	flQuotient = (flExpValue-1) / 9;

	flFaktorX = fmMinFaktorX  + flQuotient * flRange;
	return flFaktorX;
}

void CProfileChart::OnCCSEditKillFocus ( WPARAM wparam, LPARAM lparam )
{
	CCSEDITNOTIFY	*prpNotify = (CCSEDITNOTIFY*)lparam;

	if ( !UpdateData() )
	{
		 //prpNotify->SourceControl->SetFocus();
		 return;
	}
	if ( prpNotify->SourceControl == &omNameEdit )
		OnKillfocusNameEdit();
}


void  CProfileChart::OnChangePattern ( long lpPatternUrno )
{
	long llUval;
	if ( pomDisplayedData )
	{
		llUval = atol(pomDisplayedData->omUval);
		if ( llUval == lpPatternUrno )
			UpdateDisplay ( );
	}
}
