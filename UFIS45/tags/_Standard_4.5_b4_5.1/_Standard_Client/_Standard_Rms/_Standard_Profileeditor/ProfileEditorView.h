// ProfileEditorView.h : interface of the CProfileEditorView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROFILEEDITORVIEW_H__697CCAFE_B2F4_11D3_93B8_00001C033B5D__INCLUDED_)
#define AFX_PROFILEEDITORVIEW_H__697CCAFE_B2F4_11D3_93B8_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ccsedit.h>

//  Columns in Classes-Grid
#define DPAX_COL	2
#define DBAG_COL	3
#define TTWO_COL	4
#define TTBG_COL	5


class CGridControl;
class CConditionsGrid;
class CChildFrame;
class CProfileEditorDoc;
class RecordSet;

class CProfileEditorView : public CFormView
{
protected: // create from serialization only
	CProfileEditorView();
	DECLARE_DYNCREATE(CProfileEditorView)

public:
	//{{AFX_DATA(CProfileEditorView)
	enum { IDD = IDD_PROFILEEDITOR_FORM };
	CCSEdit	omNameEdit;
	CCSEdit	m_TotalDistributionEdit;
	CCSEdit	m_DcopEdit;
	CCSEdit	m_DcupEdit;
	CCSEdit	m_DgapEdit;
	CCSEdit	m_DsepEdit;
	float	fmDcop;
	float	fmDcup;
	float	fmDgap;
	float	fmDsep;
	//}}AFX_DATA
	CConditionsGrid	*pomDepartureGrid;
	CGridControl	*pomClassesGrid;
	CChildFrame		*pomParent;

// Attributes
public:
	CProfileEditorDoc* GetDocument();
	bool DisplayDocument ( );
	void ConstructString(CGridControl* popGrid, CString& ropResult); // F�r Speicherung
	void ReConstructString(CConditionsGrid* popGrid, CString &ropString) ; // F�r Laden
	void ProcessCCCChange ( bool bpAdd, RecordSet *popData );
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	void OnEndEditing(WPARAM wParam, LPARAM lParam);
	//{{AFX_VIRTUAL(CProfileEditorView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CProfileEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void IniClassesGrid ();
	void CheckPAXPercentages ();
	void DisplayTotalDistribution ();
	void OnNewClass ( long lpClassUrno );
	void OnDeleteClass ( long lpClassUrno );
	void OnChangeClass ( long lpClassUrno );
	int FindInClassesGrid ( long lpClassUrno );
	void IniStatics();
	void OnDistributionChanged ( int ipChkBoxID );
	void SetBdpsState();

// Generated message map functions
protected:
	//{{AFX_MSG(CProfileEditorView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDcopChk();
	afx_msg void OnDcupChk();
	afx_msg void OnDgapChk();
	afx_msg void OnDsepChk();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnKillfocusRema();
	afx_msg void OnPrstChk();
	afx_msg void OnKillfocusName();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	afx_msg void OnExcludeChb();
	//}}AFX_MSG
	void OnCCSEditKillFocus ( WPARAM wparam, LPARAM lparam );
	void OnPrepareClosing ( WPARAM wparam, LPARAM lparam );
	DECLARE_MESSAGE_MAP()
};


#ifndef _DEBUG  // debug version in ProfileEditorView.cpp
inline CProfileEditorDoc* CProfileEditorView::GetDocument()
   { return (CProfileEditorDoc*)m_pDocument; }
#endif

void ProcessBCInView(void *popInstance, int ipDDXType,
						    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROFILEEDITORVIEW_H__697CCAFE_B2F4_11D3_93B8_00001C033B5D__INCLUDED_)
