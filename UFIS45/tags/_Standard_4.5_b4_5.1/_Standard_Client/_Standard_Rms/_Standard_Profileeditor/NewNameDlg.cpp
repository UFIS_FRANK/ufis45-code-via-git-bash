// Dialog_Neuer_Name.cpp : implementation file
//

#include <stdafx.h>
#include <CedaBasicData.h>

#include <resource.h>
#include <ccsglobl.h>
#include <utilities.h>
#include <NewNameDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewNameDlg dialog


CNewNameDlg::CNewNameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewNameDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewNameDlg)
	m_Name = _T("");
	//}}AFX_DATA_INIT
}


void CNewNameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewNameDlg)
	DDX_Text(pDX, IDC_Aktueller_Name, m_Name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewNameDlg, CDialog)
	//{{AFX_MSG_MAP(CNewNameDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewNameDlg message handlers


void CNewNameDlg::OnOK() 
{
	UpdateData(TRUE) ;
	CString olUrno;

	if( m_Name.IsEmpty() )
	{
		LangMsgBox ( m_hWnd, IDS_PATTERN_NAME_EMPTY, IDS_WRONG_INPUT );
		return;
	}
	//--- Check ob Name schon vorhanden 
	if ( ogBCD.GetField ( "PXA", "PXAN", m_Name, "URNO", olUrno ) )
	{
		LangMsgBox ( m_hWnd, IDS_DUPL_PATTERN_NAME, IDS_WRONG_INPUT );
		return;
	}
	CDialog::OnOK();
}




void CNewNameDlg::OnCancel() 
{
	
	CDialog::OnCancel();
}


//-------------------------------------
//   Focus ver�ndern
//-------------------------------------
BOOL CNewNameDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowLangText ( this, IDS_NAME );

	SetDlgItemLangText ( this, IDC_NAME_TXT, IDS_NEWNAME );
	SetDlgItemLangText ( this, IDOK, IDS_OK );
	SetDlgItemLangText ( this, IDCANCEL, IDS_CANCEL );
	
	CEdit* pEdit = (CEdit*) GetDlgItem(IDC_Aktueller_Name);
    pEdit->SetFocus();


	return FALSE; //TRUE;  // return TRUE unless you set the focus to a control
	                        // EXCEPTION: OCX Property Pages should return FALSE
}
