# Microsoft Developer Studio Project File - Name="ProfileEditor" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=ProfileEditor - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ProfileEditor.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ProfileEditor.mak" CFG="ProfileEditor - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ProfileEditor - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "ProfileEditor - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ProfileEditor - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "C:\Ufis_Bin\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ProfileEditor\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\Release\Ufis32.lib C:\Ufis_Bin\ClassLib\Release\CCSClass.lib Wsock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "ProfileEditor - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\ProfileEditor\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Ufis_Bin\Debug\Ufis32.lib C:\Ufis_Bin\ClassLib\Debug\CCSClass.lib Wsock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ProfileEditor - Win32 Release"
# Name "ProfileEditor - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\ChartData.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\ConditionsGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\DDXDDV.CPP
# End Source File
# Begin Source File

SOURCE=.\Dialog_ExpressionEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\EmptyView.cpp
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\HistoColumn.cpp
# End Source File
# Begin Source File

SOURCE=.\Histogramm.cpp
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\NewNameDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\OpenProfileDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\ProfileChart.cpp
# End Source File
# Begin Source File

SOURCE=.\ProfileEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\ProfileEditor.rc
# End Source File
# Begin Source File

SOURCE=.\ProfileEditorDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ProfileEditorView.cpp
# End Source File
# Begin Source File

SOURCE=.\RecentRecordList.cpp
# End Source File
# Begin Source File

SOURCE=.\RecordInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectProfile.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\utilities.cpp
# End Source File
# Begin Source File

SOURCE=.\ValidityDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ValuesGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\ChartData.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\ConditionsGrid.h
# End Source File
# Begin Source File

SOURCE=.\DDXDDV.H
# End Source File
# Begin Source File

SOURCE=.\Dialog_ExpressionEditor.h
# End Source File
# Begin Source File

SOURCE=.\EmptyView.h
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.h
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\HistoColumn.h
# End Source File
# Begin Source File

SOURCE=.\Histogramm.h
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\NewNameDlg.h
# End Source File
# Begin Source File

SOURCE=.\OpenProfileDlg.h
# End Source File
# Begin Source File

SOURCE=.\ProfileChart.h
# End Source File
# Begin Source File

SOURCE=.\ProfileEditor.h
# End Source File
# Begin Source File

SOURCE=.\ProfileEditorDoc.h
# End Source File
# Begin Source File

SOURCE=.\ProfileEditorView.h
# End Source File
# Begin Source File

SOURCE=.\RecentRecordList.h
# End Source File
# Begin Source File

SOURCE=.\RecordInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SelectProfile.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\utilities.h
# End Source File
# Begin Source File

SOURCE=.\ValidityDlg.h
# End Source File
# Begin Source File

SOURCE=.\ValuesGrid.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\file_ope.bmp
# End Source File
# Begin Source File

SOURCE=.\res\login.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ProfileEditor.ico
# End Source File
# Begin Source File

SOURCE=.\res\ProfileEditor.rc2
# End Source File
# Begin Source File

SOURCE=.\res\ProfileEditorDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\toTXTTAB.txt
# End Source File
# End Target
# End Project
