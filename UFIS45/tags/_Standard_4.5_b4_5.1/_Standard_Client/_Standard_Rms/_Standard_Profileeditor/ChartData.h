// ChartData.h: interface for the CChartData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHARTDATA_H__40A0C752_B393_11D3_93B9_00001C033B5D__INCLUDED_)
#define AFX_CHARTDATA_H__40A0C752_B393_11D3_93B9_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CCSPtrArray.h>

class CValuesGrid;
class CProfileEditorDoc;

struct ValInPeriod
{
	int		start;
	float	val;
	bool	homogen;
};

struct ValInPeriodExt
{
	int		start;
	int		end;
	float	val;
	short	status;
	short	newstatus;
	CString urno;
};

class CChartData  
{
public:
	CChartData(CProfileEditorDoc *popDoc);
	virtual ~CChartData();

//  data members
protected:
//	CProfileEditorDoc *pomDocument;
public:
	CProfileEditorDoc *pomDocument;
	CString	omUval;			 //  Urno des abstrakt. Einfindungsprofils (PXA)
	CString	omUrno;			 //  Urno aus PXCTAB
	CString	omProfileName;
	long	lmCounterClass;  //  Urno der CounterClass (Tabelle CCC)
	bool	bmPXAModified;
	bool	bmVTPModified ;
	bool	bmPXCModified ;

	CString omDBAG;
	CString omDPAX;
	CString omTTWO;
	CString omTTBG;

	int imMinStep;
	int imDspStep;

	int imVtpPberIdx;
	int imVtpPenrIdx;
	int imVtpValuIdx;
	int imVtpUrnoIdx;

	float flEpsHomogen;		//  Schwelle wann Werte nicht mehr homogen sind
	
	CCSPtrArray<ValInPeriod>  omRawData;
	CCSPtrArray<ValInPeriod>  omDspData;
	CCSPtrArray<ValInPeriodExt> omInputData;
 
// Operations
	bool ReadData (CValuesGrid *popGrid=0 );
	bool SetDspStep	( int ipDspStep );
	bool SetMinStep	( int ipMinStep );
	bool CalcDspData ();
	bool CalcRawData ();
	void Reset();
	bool SaveModified ();
	bool SavePXA ();
	bool SaveModifiedPXA ( bool bpUpdate );
	bool SaveModifiedPXC ();
	bool AskForSaveChanges ();
	void SetModified (UINT ipBits);
	bool CheckInput ( bool bpDspError=true );
	void ResetModified (UINT ipBits);
	int AskSaveUsedPXANewName ();
	void CopyValues ( const CChartData& srcData );	
	void operator =( const CChartData& srcData );

};

static int CompareStartAscending (const ValInPeriod **prpValRec1, const ValInPeriod **prpValRec2);
static int CompareStartDescending (const ValInPeriod **prpValRec1, const ValInPeriod **prpValRec2);
static int CompareInputAscending (const ValInPeriodExt **prpValRec1, const ValInPeriodExt **prpValRec2);

CString GetClassName ( long lpClassUrno );
long GetClassUrno ( CString opClassName );
CString GetProfileName ( CString opProfileUrno );

#endif // !defined(AFX_CHARTDATA_H__40A0C752_B393_11D3_93B9_00001C033B5D__INCLUDED_)
