// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
//#include "BasicData.h"
#include <utilities.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

CString ogAppName="ProfileEditor"; // Name of *.exe file of this
const char *pcgAppName = (const char*)ogAppName;
const char pcgIniFile[_MAX_PATH+1]="PROFILEEDITOR.INI";


CCSLog          ogLog(pcgAppName);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, pcgAppName);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
//CBasicData       ogBasicData;
CCSBasic		ogCCSBasic;		// lib
CedaBasicData	ogBCD(pcgHome, &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);
PrivList		ogPrivList;
long  igWhatIfUrno;
CString ogWhatIfKey;
//bool			bgUseResourceStrings = true;
//--- global choice lists for comboboxes
CCSPtrArray <CHOICE_LISTS> ogChoiceLists;

//CedaCfgData ogCfgData;
ofstream of_catch;

char pcgHome[4] = "HAJ";
bool bgNoScroll;
bool bgOnline = true;
bool bgIsButtonListMovable = false;
CString ogTPLUrno;

//CInitialLoadDlg *pogInitialLoad;
char pcgTableExt[10] = "TAB";
char pcgTableName[11]="SER";
char pcgUserName[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
char pcgPassWord[33] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";


int igValVafrIdx;
int igValVatoIdx;
int igValFreqIdx;
int igValTimtIdx;
int igValTimfIdx;
int igValUvalIdx;
int igValApplIdx;
int igValUrnoIdx;
int igValTabnIdx;
int igPxcTtbgIdx;
int igPxcTtwoIdx;

CGUILng* ogGUILng = CGUILng::TheOne();

CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogCourier_Bold_10;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;


CFont ogScalingFonts[4];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;
//  Colors used for Histogramm
COLORREF	igBkColor = SILVER;
COLORREF	igRegularColColor = NAVY;
COLORREF	igMarkedColColor = RED;

CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

char SOH = 1;
char STX = 2;
char ETX = 3;

CStringArray		ogBroadCastTables;
CMapStringToPtr	ogBCTableIndices;

/////////////////////////////////////////////////////////////////////////////
/*
CString GetResString(UINT ID)
{
	CString olRet;
	olRet.LoadString(ID);
	return olRet;
}

CString GetString(UINT ipIDS )
{
	
	//return GetResString ( ipIDS );
	CString olText="???", olUrno, olIDS;
	bool	blFound = false;
	if ( ogBCD.GetDataCount("TXT") > 0 )
	{
		olIDS.Format ("%ld", ipIDS );
		olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, "PROFILE", "URNO" );
		if ( !olUrno.IsEmpty() )
			 blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		if ( !blFound )
		{
			olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, "GENERAL", "URNO" );
			if ( !olUrno.IsEmpty() )
				blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		}
	}
	if ( !blFound && bgUseResourceStrings )
		olText = GetResString ( ipIDS );
	return olText;
}

//--------------------------------
bool GetString ( CString opRefField, CString opIDS, CString &ropText )
{
	CString olUrno;
	bool	blFound = false;
	
	ropText = "???";
	if ( ogBCD.GetDataCount("TXT") > 0 )
	{
		olUrno = ogBCD.GetFieldExt( "TXT", opRefField, "APPL", opIDS, "PROFILE", "URNO" );
		if ( !olUrno.IsEmpty() )
			 blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", ropText ) ;
		if ( !blFound )
		{
			olUrno = ogBCD.GetFieldExt( "TXT", opRefField, "APPL", opIDS, "GENERAL", "URNO" );
			if ( !olUrno.IsEmpty() )
				blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", ropText ) ;
		}
	}
	return blFound;
}
//--------------------------------
//  Get String created by other Application
CString GetStrAppl ( CString opAppl, UINT ipIDS )
{
	CString olText="???", olUrno, olIDS;
	bool	blFound = false;
	if ( ogBCD.GetDataCount("TXT") > 0 )
	{
		olIDS.Format ("%ld", ipIDS );
		olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, opAppl, "URNO" );
		if ( !olUrno.IsEmpty() )
			 blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		if ( !blFound )
		{
			olUrno = ogBCD.GetFieldExt( "TXT", "STID", "APPL", olIDS, "GENERAL", "URNO" );
			if ( !olUrno.IsEmpty() )
				blFound = ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", olText ) ;
		}
	}
	if ( !blFound && bgUseResourceStrings )
		olText = GetResString ( ipIDS );
	return olText;
}
*/
//--------------------------------

CString GetString(UINT nID)
{
	/*CGUILng* */ogGUILng = CGUILng::TheOne();
	return ogGUILng->GetString(nID);
}

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;


// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}

	// create a break job brush pattern
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}


void InitFont() 
{
    CDC dc;
    ASSERT(dc.CreateCompatibleDC(NULL));

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_6.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_7.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Bold_10.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_10.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_9.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts"); /*"MS Sans Serif""Arial"*/
    ASSERT(ogScalingFonts[MS_SANS6].CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS8].CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS12].CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS16].CreateFontIndirect(&logFont));
 
	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont));

    dc.DeleteDC();
}

//  FindGroupsWithResourceType:  Finde alle Gruppen in SGRTAB, in denen 
//								 (u.U. auch rekursiv) Resourcen des gew�nschten
//								 Typs vorkommen. 
//	IN:		opReqRes:		requested resource
//	IN/OUT:	ropUrnoList:	Liste der Urnos der Gruppen, die den Resourcetyp
//							enthalten ( wird erweitert )
//			ropInspUrnos:	
int FindGroupsWithResourceType ( CString opReqRes, CStringList &ropUrnoList )
{
	CStringList olInspUrnos;	//  Liste der Urnos der Gruppen, die bereits �berpr�ft 
								//	wurden 	(um unendliche Rekursion zu vermeiden )		
	int			ilRecAnz = 0, i;
	bool		blFound;
	CString		olSgrUrno;
	
	ilRecAnz = ogBCD.GetDataCount ( "SGR" );
	for ( i=0; i<ilRecAnz; i++ )
	{
		olSgrUrno = ogBCD.GetField( "SGR", i, "URNO" );
		if ( !olSgrUrno.IsEmpty() )
			blFound = IsResourceTypeInGroup ( opReqRes, olSgrUrno, ropUrnoList, 
											  olInspUrnos );
	}
	return ropUrnoList.GetCount ();
}

//  IsResourceTypeInGroup:  Check ob in Finde alle Gruppen in SGRTAB, in denen 
//								 (u.U. auch rekursiv) Resourcen des gew�nschten
//								 Typs vorkommen. 
//	IN:		opReqRes:		requested resource type
//  
//	IN/OUT:	ropUrnoList:	Liste der Urnos der Gruppen, die den Resourcetyp
//							enthalten ( wird erweitert )
//			ropInspUrnos:	Liste der Urnos der Gruppen, die bereits �berpr�ft 
//							wurden 	( wird erweitert, um unendliche Rekursion zu vermeiden )		
bool IsResourceTypeInGroup ( CString opReqRes, CString opSgrUnro, 
							 CStringList &ropUrnoList, CStringList &ropInspUrnos )
{
	CCSPtrArray<RecordSet>	olRecords;
	int						ilAnz = 0, i;
	CString					olTana, olUrno;
	bool					blFound=false;

	//  Wenn Gruppe bereits untersucht worden ist, fertig
	//  return-Wert h�ngt davon ab, ob bei fr�herer Untersuchung die Urno in
	//  ropUrnoList eingetragen worden ist
	if ( ropInspUrnos.Find ( opSgrUnro ) )
		return (ropUrnoList.Find( opSgrUnro ) ? true : false );
	else	//  sofort als untersuchte Gruppe eintragen, damit Rekursion abbrechen kann
		ropInspUrnos.AddTail ( opSgrUnro );
	
	//  Hole alle Datens�tze aus SGMTAB, die zu opSgrUnro geh�ren
	ogBCD.GetRecords( "SGM", "USGR", opSgrUnro, &olRecords );
	ilAnz = olRecords.GetSize ();
	for ( i=0; (i<ilAnz)&&!blFound; i++ )
	{
		olTana = olRecords[i].Values[ogBCD.GetFieldIndex("SGM", "TABN")];
		olUrno = olRecords[i].Values[ogBCD.GetFieldIndex("SGM", "URNO")];
		if ( olTana.Left(3) == opReqRes.Left(3) )
		{
			//  einen Member dieser Gruppe gefunden, der Bedingung erf�llt, also 
			//  Gruppe ggf. in ropUrnoList aufnehmen und fertig
			if ( !ropUrnoList.Find ( opSgrUnro ) )
				ropUrnoList.AddTail ( opSgrUnro );
			blFound = true;
		}
		//  Handelt es sich um ein Member, das selbst statische Gruppe ist ?
		if ( olTana.Left(3) == "SGR" )
			blFound = IsResourceTypeInGroup ( opReqRes, olUrno, ropUrnoList, 
											  ropInspUrnos );
	}
	olRecords.DeleteAll ();
	return blFound;
}


void ReadColorSettings ()
{
	igBkColor = GetPrivateProfileInt ( "HISTOGRAMM", "COLOR_BK", SILVER,  
									   "PROFILEEDITOR.INI" );
	igRegularColColor = GetPrivateProfileInt ( "HISTOGRAMM", "COLOR_REGULAR", 
											   NAVY, "PROFILEEDITOR.INI" );
	igMarkedColColor = GetPrivateProfileInt ( "HISTOGRAMM", "COLOR_MARKED", 
											   NAVY, "PROFILEEDITOR.INI" );
}

void SaveColorSettings ()
{
	WriPrivProfInt ( "HISTOGRAMM", "COLOR_BK", igBkColor, "PROFILEEDITOR.INI" );
	WriPrivProfInt ( "HISTOGRAMM", "COLOR_REGULAR", igRegularColColor,
					 "PROFILEEDITOR.INI" );
	WriPrivProfInt ( "HISTOGRAMM", "COLOR_MARKED", igMarkedColColor,
					 "PROFILEEDITOR.INI" );
}