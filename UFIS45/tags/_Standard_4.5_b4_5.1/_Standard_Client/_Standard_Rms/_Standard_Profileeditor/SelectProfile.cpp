// SelectProfile.cpp : implementation file
//

#include <stdafx.h>

#include <cedabasicdata.h>

#include <ccsglobl.h>
#include <ProfileEditor.h>
#include <SelectProfile.h>
#include <ChartData.h>
#include <utilities.h>
#include <NewNameDlg.h>
#include <basicdata.h>
#include <histogramm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectProfile dialog


CSelectProfile::CSelectProfile(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectProfile::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectProfile)
	m_SelectedProfile = _T("");
	//}}AFX_DATA_INIT
	pomPreviewData = new CChartData(0);
	pomPreview = new CHistogramm;
}

CSelectProfile::~CSelectProfile()
{
	if ( pomPreview )
		delete pomPreview;
	if ( pomPreviewData )
		delete pomPreviewData;
}


void CSelectProfile::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectProfile)
	DDX_Control(pDX, IDC_PROLIFE_LB, m_ProfileLb);
	DDX_LBString(pDX, IDC_PROLIFE_LB, m_SelectedProfile);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectProfile, CDialog)
	//{{AFX_MSG_MAP(CSelectProfile)
	ON_LBN_DBLCLK(IDC_PROLIFE_LB, OnDblclkProlifeLb)
	ON_BN_CLICKED(ID_EDIT_DELETE, OnDelete)
	ON_BN_CLICKED(ID_EDIT_COPY, OnCopy)
	ON_LBN_SELCHANGE(IDC_PROLIFE_LB, OnSelchangeProlifeLb)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectProfile message handlers

BOOL CSelectProfile::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	IniStatics ();
	IniListBox ();	
	pomPreview->Open ( this, IDC_PREVIEW_STAT );
	pomPreview->imColumnWidth = pomPreviewData->imDspStep = 15;
	UpdateData(FALSE);
	OnSelchangeProlifeLb() ;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectProfile::IniListBox ()
{
	int		ilProfileCount=0, i;
	CString	olProfileName;

	ilProfileCount = ogBCD.GetDataCount ( "PXA" );
	for ( i=0; i<ilProfileCount; i++ )
	{
		olProfileName = ogBCD.GetField ( "PXA", i, "PXAN" );
		if ( !olProfileName.IsEmpty() )
			m_ProfileLb.AddString ( olProfileName );
	}
}

void CSelectProfile::OnOK() 
{
	// TODO: Add extra validation here
	CString olUrno;
	CDialog::OnOK();
	if ( ogBCD.GetField ( "PXA", "PXAN", m_SelectedProfile, "URNO", olUrno ) )
		omSelectedUrno = olUrno;
}

void CSelectProfile::OnDblclkProlifeLb() 
{
	// TODO: Add your control notification handler code here
	if ( m_ProfileLb.GetCurSel()>=0)
		OnOK();
}

void CSelectProfile::OnDelete() 
{
	// TODO: Add your control notification handler code here
	int ilIdx = m_ProfileLb.GetCurSel();
	UpdateData();
	if ( ilIdx >= 0 ) 
	{
		CString olUrno, olText;
		CString olUsedByUrnos;
		// olText = "Do you really want to delete check-in pattern %s?";
		
		if ( !ogBCD.GetField ( "PXA", "PXAN", m_SelectedProfile, "URNO", olUrno ) )
		{
			//olText = "Unable to find check-in pattern %s.";
			//olText.Replace ( "%s", m_SelectedProfile );
			//AfxMessageBox ( olText, MB_ICONINFORMATION|MB_OK );
			FileErrMsg ( m_hWnd, IDS_PATTERN_NOT_FOUND, pCHAR(m_SelectedProfile),
						 0, MB_ICONINFORMATION|MB_OK );
			return;
		}
		olUsedByUrnos = ogBCD.GetValueList("PXC", "UPXA", olUrno, "UPRO");
		if ( olUsedByUrnos.IsEmpty() )
		{
			/*
			olText.Replace ( "%s", m_SelectedProfile );
			if ( AfxMessageBox ( olText, MB_ICONQUESTION|MB_YESNO ) ==IDYES )*/
			if ( FileErrMsg ( m_hWnd, IDS_ASK_DEL_PATTERN, pCHAR(m_SelectedProfile), 
							  0, MB_ICONQUESTION|MB_YESNO ) ==IDYES )
			{
				CString olUrno;
				if ( ogBCD.GetField ( "PXA", "PXAN", m_SelectedProfile, "URNO", olUrno ) && 
					 DeleteByWhere("VTP", "UVAL", olUrno ) &&
					 ogBCD.DeleteRecord ( "PXA", "URNO", olUrno, true ) )
					m_ProfileLb.DeleteString ( ilIdx );
			}
		}
		else
		{
			//olText = "Unable to delete check-in pattern %s. It is used by the following profiles:\n";
			olText = GetString ( IDS_DELETE_PATTERN_ERR );
			olText.Replace ( "%s", m_SelectedProfile );
			CStringArray olProUrnos;
			CStringList  olProNames;
			CString		 olProName;
			POSITION	 pos;

			ExtractItemList(olUsedByUrnos, &olProUrnos, ',' );
			for ( int i=0; i<olProUrnos.GetSize(); i++ )
			{
				if ( ogBCD.GetField ( "PRO", "URNO", olProUrnos[i], 
									  "NAME", olProName ) && 
					 !olProNames.Find ( olProName ) )
					olProNames.AddTail ( olProName ) ;
			}
			pos = olProNames.GetHeadPosition ();
			while ( pos )
			{
				olProName = olProNames.GetNext ( pos );
				olText += olProName;
				olText += pos ? ", " : ".";
			}
			AfxMessageBox ( olText, MB_ICONINFORMATION|MB_OK );
		}
	}
}

void CSelectProfile::OnCopy() 
{
	// TODO: Add your control notification handler code here
	RecordSet olPXARec;	
	RecordSet olVTPRec;	
	CString olUrno, olText, olNewName, olNewUrno ;
	bool blOk;
	CNewNameDlg dlg(this);

	int ilIdx = m_ProfileLb.GetCurSel();
	UpdateData();
	if ( ( ilIdx >= 0 ) &&
		 ogBCD.GetField ( "PXA", "PXAN", m_SelectedProfile, "URNO", olUrno ) &&
		 ogBCD.GetRecord ( "PXA", "URNO", olUrno, olPXARec ) )
	{
		bool blNameOk = false;
		int  ilInserted=1;
		do
		{
			if ( dlg.DoModal() == IDOK )
				olNewName = dlg.m_Name;
			else
				return ;
			if ( !olNewName.IsEmpty() &&
				 !ogBCD.GetField ( "PXA", "PXAN", olNewName, "URNO", olNewUrno ) )
				 blNameOk = true;
		}while ( !blNameOk) ;

		BeginWaitCursor();
		DWORD	start, now, diff;
		start = GetTickCount();
		
		olNewUrno.Format ( "%lu", ogBCD.GetNextUrno() );
		olPXARec.Values[ogBCD.GetFieldIndex("PXA","URNO")] = olNewUrno;
		olPXARec.Values[ogBCD.GetFieldIndex("PXA","PXAN")] = olNewName;
		blOk = ogBCD.InsertRecord ( "PXA", olPXARec, true );
		if ( blOk )
		{
			CCSPtrArray<RecordSet> olVtpRecs;

			int ilVtpUrnoIdx = ogBCD.GetFieldIndex ("VTP", "URNO" );
			int ilVtpUvalIdx = ogBCD.GetFieldIndex ("VTP", "UVAL" );
				
			ogBCD.GetRecords ( "VTP", "UVAL", olUrno, &olVtpRecs );
			for ( int i=0; i<olVtpRecs.GetSize(); i++ )
			{
				olVtpRecs[i].Values[ilVtpUrnoIdx].Format ( "%lu", ogBCD.GetNextUrno() );
				olVtpRecs[i].Values[ilVtpUvalIdx] = olNewUrno;
				if (!ogBCD.InsertRecord("VTP", olVtpRecs[i] ) )
					blOk = false;
				else
					ilInserted++;
			}
			olVtpRecs.DeleteAll ();
			blOk &= ogBCD.Save ( "VTP" );
		}
		now = GetTickCount();
		diff = now-start;
		CString olMsg;

		DWORD ilMin, ilSec;
		ilMin = diff/60000;
		ilSec = diff%60000;
		ilSec/=1000;
		olMsg.Format ( "Datensätze eingefügt: %d\nZeit: %d:%02d,%03d", 
					   ilInserted, ilMin, ilSec, diff%1000 );
		EndWaitCursor ();
		//AfxMessageBox ( olMsg, MB_ICONINFORMATION|MB_OK );	Testausgabe
		if ( blOk )
			m_ProfileLb.AddString ( olNewName );

	}
	if ( !blOk )
	//	AfxMessageBox ( "Error on copying selected check-in pattern!", MB_OK );
		LangMsgBox ( m_hWnd, IDS_COPY_PATTERN_ERR );

}

void CSelectProfile::OnSelchangeProlifeLb() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CString olUrno;
	bool blOk = false;
	if ( ogBCD.GetField ( "PXA", "PXAN", m_SelectedProfile, "URNO", olUrno ) )
	{
		pomPreviewData->omUval = olUrno;
		blOk = pomPreviewData->ReadData ();
		blOk &= pomPreviewData->CalcDspData ();
		if ( blOk )
		{
			pomPreview->ShowWindow(SW_SHOW);
			pomPreview->imColumnWidth = pomPreviewData->imDspStep;
			pomPreview->DisplayData ( pomPreviewData );
		}
	}
	if ( !blOk )
	{
		pomPreview->ShowWindow(SW_HIDE);
	}
}


int CSelectProfile::DoModal(const char* pcpProfileName/*=0*/) 
{
	// TODO: Add your specialized code here and/or call the base class
	if ( pcpProfileName )
		m_SelectedProfile = pcpProfileName;	
	return CDialog::DoModal();
}

void CSelectProfile::IniStatics ()
{
	SetDlgItemLangText( this, ID_EDIT_DELETE, IDS_DELETE );
	SetDlgItemLangText( this, ID_EDIT_COPY, IDS_COPY );
	SetDlgItemLangText( this, IDOK, IDS_OK );
	SetDlgItemLangText( this, IDCANCEL, IDS_CANCEL );
	SetWindowLangText ( this, IDS_SELECT_PROFILE_TIT );
}