// TimeScaleDlg.h : header file
//
#ifndef _TIMESCALE_H_
#define _TIMESCALE_H_

#include <TScale.h>

/////////////////////////////////////////////////////////////////////////////
// CTimeScaleDialog dialog

class CTimeScaleDialog : public CDialog
{
// Construction
public:
	CTimeScaleDialog(CWnd* pParent = NULL);	// standard constructor

    BOOL bmNotMoreThan12;
    CTimeScale *pomTS, *pomTS1;
    CTimeSpan omTSI;


    CTime   m_TimeScale;
	int		m_Percent;
	int		m_Hour;
	int		imMaxHours;

// Dialog Data
	//{{AFX_DATA(CTimeScaleDialog)
	enum { IDD = IDD_TIMESCALE };
	CSliderCtrl	m_Height;
	CString	m_startTime;
	CTime	m_startDate;
	int		m_100p;
	int		m_24h;
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	// Generated message map functions
	//{{AFX_MSG(CTimeScaleDialog)
	afx_msg void On12h();
	afx_msg void On24h();
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual void OnOK();
	afx_msg void On6h();
	afx_msg void On8h();
	afx_msg void On4h();
	afx_msg void On10h();
	//}}AFX_MSG

    DECLARE_MESSAGE_MAP()
};
#endif // _TIMESCALE_H_
