// DChange.h : header file
//
#ifndef _DCHANGE_H_
#define _DCHANGE_H_

/////////////////////////////////////////////////////////////////////////////
// DChange dialog

class DChange : public CDialog
{
// Construction
public:
	DChange(CWnd* pParent = NULL,CString opName = "",CTime opAcfr = -1, CTime opActo = -1);   // standard constructor

	CTime m_Acfr;
	CTime m_Acto;
	CString omCaption;

// Dialog Data
protected:
	//{{AFX_DATA(DChange)
	enum { IDD = IDD_CHANGE };
	CString		m_Name;
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DChange)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DChange)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif // _DCHANGE_H_