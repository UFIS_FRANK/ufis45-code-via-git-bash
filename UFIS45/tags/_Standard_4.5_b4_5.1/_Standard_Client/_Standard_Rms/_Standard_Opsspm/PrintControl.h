#ifndef __PRNCTRL_H__
#define __PRNCTRL_H__

#include <StaffTableViewer.h>
#include <FlightScheduleViewer.h>
#include <GateTableViewer.h>
#include <PrmTableViewer.h>

// PrintControl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrintCtrl dialog

typedef struct tagPREPLANHEADER {
	char Team[10];
	char Date[20];
} PREPLANHEADER, *PPREPLANHEADER;

typedef struct tagPREPLANDETAIL {
	char Rng[10];
	char Name[50];
	char Sft[10];
	char Anwesend1[5];
	char Anwesend2[5];
	char Flua_CCI[10];
	char Zugeorduetbis1[10];
	char Zugeorduetbis2[5];
	char Zugeorduetbis3[10];
	char Zugeorduetbis4[5];
	char Zugeorduetbis5[10];
	char Zugeorduetbis6[5];
	char Unterschrift[20];
} PREPLANDETAIL, *PPREPLANDETAIL;

class CPrintCtrl : public CPrintDialog
{
	DECLARE_DYNAMIC(CPrintCtrl)

public:
	//void PreplanPrint( CedaShiftData *shfData );
	// MCU 07.07 new parameter pcpDutyDay 
	void PreplanPrint(CedaShiftData *shfData,char *pcpDutyDay);
	void StaffTablePrint();
	void FlightSchedulePrint();
	void GateTablePrint();
	void PrmTablePrint();

private:
	// MCU 07.07 pcpDutyDay is new parameter
	void PrintPreplanTeam( HDC hdc, CString strTeam, CedaShiftData *shfData,char *pcpDutyDay);
	void PrintPreplanHeader( HDC hdc, PPREPLANHEADER pPPHeader );
	void PrintPreplanDetail( HDC hdc, PPREPLANDETAIL pPPDetail, int nLineNo );

	void PrintStaffTableHeader( HDC hdc );
	void PrintStaffTableDetail( HDC hdc, STAFFTABLE_LINEDATA *pData, int nLineNo );

	void PrintFlightScheduleHeader( HDC hdc );
	void PrintFlightScheduleDetail( HDC hdc, FLIGHTSCHEDULE_LINEDATA *pData, int nLineNo );

	void PrintGateTableHeader( HDC hdc );
	void PrintGateTableDetail( HDC hdc, GATEDATA_LINE *pData, int nLineNo );

	void PrintPrmTableHeader( HDC hdc );
	void PrintPrmTableDetail( HDC hdc, PRMDATA_LINE *pData, int nLineNo );



private:
	int	m_nStartPageX;
	int	m_nStartPageY;

public:
	CCSPtrArray<STAFFTABLE_LINEDATA> omStaffTableLine;
    CCSPtrArray<FLIGHTSCHEDULE_LINEDATA> omFlightScheduleLines;
    CCSPtrArray<GATEDATA_LINE> omGateTableLines;
    CCSPtrArray<PRMDATA_LINE> omPrmTableLines;


public:
	CPrintCtrl(BOOL bPrintSetupOnly,
		// TRUE for Print Setup, FALSE for Print Dialog
		DWORD dwFlags = PD_ALLPAGES | PD_USEDEVMODECOPIES | PD_NOPAGENUMS
			| PD_HIDEPRINTTOFILE | PD_NOSELECTION,
		CWnd* pParentWnd = NULL);
	~CPrintCtrl();
protected:
	//{{AFX_MSG(CPrintCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif //__PRNCTRL_H__