// SelectPoolStaffDlg.h : header file
//
#ifndef _CSELECTPOOLSTAFFDIALOG_H_
#define _CSELECTPOOLSTAFFDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// CSelectPoolStaffDialog dialog

class CSelectPoolStaffDialog : public CDialog
{
// Construction
public:
	CSelectPoolStaffDialog(CWnd* pParent,
		StaffDiagramViewer *popViewer, int ipGroupno);
	void  SetCaptionText(CString opCaptionText); 
// Dialog Data
	CStringArray omStaffList;
	CString omCaptionText;

	//{{AFX_DATA(CSelectPoolStaffDialog)
	enum { IDD = IDD_SELECTPOOLSTAFF_DIALOG };
	CListBox	m_List;
	//}}AFX_DATA

// Output data member
public:
	// The caller may check this array for each selected item on return
	CWordArray omSelectedItems;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectPoolStaffDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	StaffDiagramViewer *pomViewer;
	int imGroupno;

	// Generated message map functions
	//{{AFX_MSG(CSelectPoolStaffDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif // _CSELECTPOOLSTAFFDIALOG_H_