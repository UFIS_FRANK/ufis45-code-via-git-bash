// PrintTerminalSelection.cpp : implementation file
//

#include <stdafx.h>
#include <PrintTerminalSelection.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintTerminalSelection dialog
const int CPrintTerminalSelection::TERMINAL2_SELECTED = 0;
const int CPrintTerminalSelection::TERMINAL3_SELECTED = 1;
const int CPrintTerminalSelection::BOTH_TERMINAL_SELECTED = 2;

CPrintTerminalSelection::CPrintTerminalSelection(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintTerminalSelection::IDD, pParent)
{	
	m_SelectedTerminal = TERMINAL2_SELECTED;
}


void CPrintTerminalSelection::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);	
	DDX_Radio(pDX, IDC_RADIO_TERMINAL2, m_SelectedTerminal);
}


BEGIN_MESSAGE_MAP(CPrintTerminalSelection, CDialog)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintTerminalSelection message handlers

BOOL CPrintTerminalSelection::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if(!omWindowHeaderText.IsEmpty())
		SetWindowText(omWindowHeaderText);
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrintTerminalSelection::OnOK() 
{
	UpdateData(TRUE);	
	CDialog::OnOK();
}

BOOL CPrintTerminalSelection::IsTerminal2Selected()
{	
	return m_SelectedTerminal == TERMINAL2_SELECTED;
}

BOOL CPrintTerminalSelection::IsTerminal3Selected()
{	
	return m_SelectedTerminal == TERMINAL3_SELECTED;
}

BOOL CPrintTerminalSelection::IsBothTerminalSelected()
{	
	return m_SelectedTerminal == BOTH_TERMINAL_SELECTED;
}