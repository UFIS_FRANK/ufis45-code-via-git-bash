// AvailableEmpViewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							On this change, special flight job was changed from type
//							FLT to FLS. The field PRID which contains a word "SPECIAL"
//							is not used any more. The field FLUR which was used for
//							storing shift URNO now moved to the field SHUR.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <ccsddx.h>
#include <AvailableEmpsViewer.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void AvailableStaffTableTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// AvailableEmpViewer
//

AvailableEmpViewer::AvailableEmpViewer(CCSPtrArray<AVAILABLE_EMPS_LINEDATA> *popAvailableEmps)
{
	pomAvailableEmps = NULL;
	if(popAvailableEmps != NULL)
	{
		pomAvailableEmps = popAvailableEmps;
	}
	omSortOrder.Add("TIMEFROM");
	omSortOrder.Add("TEAM");	
    pomTable = NULL;
}

AvailableEmpViewer::~AvailableEmpViewer()
{
    DeleteAll();
}

void AvailableEmpViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}

void AvailableEmpViewer::PrepareFilter()
{
	bmUseAllPools = SetFilterMap("Pool", omCMapForPool, GetString(IDS_ALLSTRING));

	bmUseAllTeams = SetFilterMap("Team", omCMapForTeam, GetString(IDS_ALLSTRING));
	bmEmptyTeamSelected = CheckForEmptyValue(omCMapForTeam, GetString(IDS_NOWORKGROUP));

	bmUseAllShiftCodes = SetFilterMap("Schichtcode", omCMapForShiftCode, GetString(IDS_ALLSTRING));
	bmUseAllRanks = SetFilterMap("Dienstrang", omCMapForRank, GetString(IDS_ALLSTRING));
	bmUseAllPermits = SetFilterMap("Permits", omCMapForPermits, GetString(IDS_ALLSTRING));
}

int AvailableEmpViewer::CompareAvailable(AVAILABLE_EMPS_LINEDATA *prpAvailable1, 
								   AVAILABLE_EMPS_LINEDATA *prpAvailable2)
{
    // Compare in the sort order, from the outermost to the innermost
	int ilWeightDiff = prpAvailable1->Weight - prpAvailable2->Weight;
	if(ilWeightDiff == 0)
	{
		int n = omSortOrder.GetSize();
		for (int i = 0; i < n; i++)
		{
			int ilCompareResult = 0;
			if(omSortOrder[i] == "TEAM")
				ilCompareResult = (strcmp(prpAvailable1->Tmid, prpAvailable2->Tmid)==0)? 0:
					(strcmp(prpAvailable1->Tmid, prpAvailable2->Tmid)>0)? 1: -1;
			else if(omSortOrder[i] == "TIMEFROM") 
				ilCompareResult = (prpAvailable1->Avfr == prpAvailable2->Avfr)? 0:
					(prpAvailable1->Avfr > prpAvailable2->Avfr)? 1: -1;
			// Check the result of this sorting order, return if unequality is found
			if (ilCompareResult != 0)
				return ilCompareResult;
		}
	}
    return ilWeightDiff;
}


void AvailableEmpViewer::UpdateView(const char *pcpViewName)
{
    SelectView(pcpViewName);
	DeleteAll();
	PrepareFilter();
	MakeLines();
	UpdateDisplay();
}




/////////////////////////////////////////////////////////////////////////////
// AvailableEmpViewer -- code specific to this class

void AvailableEmpViewer::MakeLines()
{
	int ilCount = 0;
	if(pomAvailableEmps == NULL)
	{
		return;
	}

	ilCount = pomAvailableEmps->GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		AVAILABLE_EMPS_LINEDATA rlLine = pomAvailableEmps->GetAt(i);
		if(IsPassFilter(&rlLine))
		{
			MakeLine(&rlLine);
		}
	}
}

bool AvailableEmpViewer::IsPassFilter(AVAILABLE_EMPS_LINEDATA *prpLine)
{
	CStringArray olPermits;
	ogBasicData.GetPermitsForPoolJob(ogJobData.GetJobByUrno(prpLine->PoolJobUrno), olPermits);

	return IsPassFilter(prpLine->Pool, prpLine->Tmid, prpLine->Sfca, "", false, olPermits);
}

bool AvailableEmpViewer::IsPassFilter(const char *pcpPoolId, const char *pcpTeamId,
	const char *pcpShiftCode, const char *pcpRank, bool bpIsAbsent, CStringArray &ropPermits)
{
	void *p;
	BOOL blIsPoolOk = bmUseAllPools || omCMapForPool.Lookup(CString(pcpPoolId), p);
	BOOL blIsTeamOk = bmUseAllTeams || (strlen(pcpTeamId) == 0 && bmEmptyTeamSelected) || omCMapForTeam.Lookup(CString(pcpTeamId), p);
	BOOL blIsShiftCodeOk = bmUseAllShiftCodes || omCMapForShiftCode.Lookup(CString(pcpShiftCode), p);
	BOOL blIsRankOk = true; // bmUseAllRanks || omCMapForRank.Lookup(CString(pcpRank), p);
	BOOL blIsPermitOk = bmUseAllPermits;
	int ilNumPermits = ropPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
	{
		blIsPermitOk = omCMapForPermits.Lookup(ropPermits[ilPermit], p);
	}
	if (bpIsAbsent)
	{
		blIsShiftCodeOk = TRUE;
	}
	return (blIsPoolOk && blIsTeamOk && blIsShiftCodeOk && blIsRankOk && blIsPermitOk);
}


void AvailableEmpViewer::MakeLine(AVAILABLE_EMPS_LINEDATA  *prpAvailable)
{
	
	//File the Line-record and Create the Line
    CreateLine(prpAvailable);
}



/////////////////////////////////////////////////////////////////////////////
// AvailableEmpViewer - STAFFTABLE_LINEDATA array maintenance

void AvailableEmpViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int AvailableEmpViewer::CreateLine(AVAILABLE_EMPS_LINEDATA *prpAvailable)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareAvailable(prpAvailable, &omLines[ilLineno]) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAvailable(prpAvailable, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

	AVAILABLE_EMPS_LINEDATA rlAvailableEmp;
	memcpy(&rlAvailableEmp, prpAvailable, sizeof(AVAILABLE_EMPS_LINEDATA));
    omLines.NewAt(ilLineno, rlAvailableEmp);

    return ilLineno;
}

void AvailableEmpViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


/////////////////////////////////////////////////////////////////////////////
// AvailableEmpViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void AvailableEmpViewer::UpdateDisplay()
{
	CString olHeader;
	// "Name|Time|Pool|Group|Shift|Employee Functions/Permits"
	if(!IsPrivateProfileOn("ONE_GLANCE_DEMO","NO"))
	{
		olHeader.Format("%s|%s|%s|%s|%s|%s",
			GetString(IDS_AVEMPS_NAME),GetString(IDS_AVEMPS_TIME),GetString(IDS_AVEMPS_POOL),
			GetString(IDS_AVEMPS_GROUP),GetString(IDS_AVEMPS_SHIFT),GetString(IDS_AVEMPS_FUNCS));
	}
	else
	{
		olHeader.Format("%s|%s|%s|%s|%s|%s|%s",
			GetString(IDS_AVEMPS_NAME),GetString(IDS_AVEMPS_TIME),GetString(IDS_AVEMPS_POOL),
			GetString(IDS_AVEMPS_GROUP),GetString(IDS_AVEMPS_SHIFT),"Bay",GetString(IDS_AVEMPS_FUNCS));
	}

	pomTable->SetHeaderFields(olHeader);

	int ilNameLen = strlen(GetString(IDS_AVEMPS_NAME));
	int ilTimeLen = max(strlen(GetString(IDS_AVEMPS_TIME)),9);
	int ilPoolNameLen = strlen(GetString(IDS_AVEMPS_POOL));
	int ilGroupNameLen = strlen(GetString(IDS_AVEMPS_GROUP));
	int ilShiftLen = strlen(GetString(IDS_AVEMPS_SHIFT));

    // Load filtered and sorted data into the table content
	pomTable->ResetContent();
	int ilNumLines = omLines.GetSize();
    for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		AVAILABLE_EMPS_LINEDATA *prlLine = &omLines[ilLc];

		pomTable->AddTextLine(Format(prlLine), prlLine);
		pomTable->SetTextLineDragEnable(ilLc,FALSE);
		if (ilLc == ilNumLines-1 || prlLine->Weight != omLines[ilLc+1].Weight)
		{
			pomTable->SetTextLineSeparator(ilLc, ST_THICK);
		}

		COLORREF olTextColour = BLACK, olBackgroundColour = WHITE;
		if(prlLine->Weight == EMP_NOT_MATCH_DEMAND)
		{
			olTextColour = RED;
		}
		if(prlLine->OverlapsBreak || prlLine->OverlapConflict || prlLine->OutsideShiftConflict)
		{
			olBackgroundColour = SILVER;
		}
		pomTable->SetTextLineColor(ilLc, olTextColour, olBackgroundColour);

		ilNameLen = max(ilNameLen,(int) strlen(prlLine->Lnam));
		ilPoolNameLen = max(ilPoolNameLen,(int) strlen(prlLine->Pool));
		ilGroupNameLen = max(ilGroupNameLen,(int) strlen(prlLine->Tmid));
		ilShiftLen = max(ilShiftLen,(int) (strlen(prlLine->NonEmpFunction)+strlen(prlLine->Sfca)+1));
    }                                           

	CString olFormatList;

	if(!IsPrivateProfileOn("ONE_GLANCE_DEMO","NO"))
	{
		olFormatList.Format("%d|%d|%d|%d|%d|%d",ilNameLen,ilTimeLen,ilPoolNameLen,ilGroupNameLen,ilShiftLen,MAXFUNCLEN*2);
	}
	else
	{
		olFormatList.Format("%d|%d|%d|%d|%d|%d|%d",ilNameLen,ilTimeLen,ilPoolNameLen,ilGroupNameLen,ilShiftLen,6,MAXFUNCLEN*2);
	}
    pomTable->SetFormatList(olFormatList);

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString AvailableEmpViewer::Format(AVAILABLE_EMPS_LINEDATA *prpLine)
{
	CString olText;

	if(!IsPrivateProfileOn("ONE_GLANCE_DEMO","NO"))
	{
		olText.Format("%s|%s-%s|%s|%s|%s %s|%s/%s|",
				prpLine->Lnam, prpLine->Avfr.Format("%H%M"), prpLine->Avto.Format("%H%M"),
				prpLine->Pool, prpLine->Tmid, prpLine->Sfca, prpLine->NonEmpFunction,
				prpLine->EmpFunctions,prpLine->EmpPermits);
	}
	else
	{
		CString randBay;
		randBay.Format("A%d", (rand() % 10 + 1));
						
		olText.Format("%s|%s-%s|%s|%s|%s %s|%s|%s/%s|",
				prpLine->Lnam, prpLine->Avfr.Format("%H%M"), prpLine->Avto.Format("%H%M"),
				prpLine->Pool, prpLine->Tmid, prpLine->Sfca, prpLine->NonEmpFunction, randBay,
				prpLine->EmpFunctions, prpLine->EmpPermits);
	}

    return olText;
}

static void AvailableStaffTableTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
/*    AvailableEmpViewer *polViewer = (AvailableEmpViewer *)popInstance;

	if(ipDDXType == DEMANDGANTT_SELECT)
	{
		polViewer->ProcessSelectDemand((JOBDATA *)vpDataPointer);
	} */
}


int AvailableEmpViewer::GetLineCount()
{
	return omLines.GetSize();
}