// EquipmentDiagram.cpp : implementation file
// 

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSCedaCom.h>
#include <cxbutton.h>
#include <clientwn.h>
#include <tscale.h>
#include <TimeScaleDlg.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaFlightData.h>
#include <CCSDragDropCtrl.h>
#include <cviewer.h>
#include <EquipmentViewer.h>
#include <EquipmentGantt.h>
#include <EquipmentChart.h>
#include <EquipmentDiagram.h>
#include <TimePacket.h>

#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <BasePropertySheet.h>
#include <EquipmentDiagramGroupPage.h>
#include <EquipmentDiagramPropSheet.h>

#include <BasicData.h>
#include <conflict.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <DruckAuswahl.h>

#include <CedaEquData.h>
#include <CedaEqtData.h>
#include <SearchResults.h>

#include <ButtonList.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

const CString EQUIPMENTDIAGRAM = "EquipmentDiagram";

// Prototypes
static void EquipmentDiagramCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagram

IMPLEMENT_DYNCREATE(EquipmentDiagram, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

EquipmentDiagram::EquipmentDiagram()
{
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	EquipmentDiagram(FALSE,ogBasicData.GetTime());
	bmScrolling = false;
}

EquipmentDiagram::EquipmentDiagram(BOOL bpPrePlanMode,CTime opPrePlanTime)
{
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	omViewer.SetViewerKey(EQUIPMENTDIAGRAM);
    imStartTimeScalePos = 120;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
    
	omPrePlanTime = opPrePlanTime;
	omPrePlanMode = bpPrePlanMode;

	bmIsViewOpen = FALSE;

	CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.EQCH,ogCfgData.rmUserSetup.MONS);

	olRect.top += 40;
	//WS_VISIBLE is commented out to avoid double screen display while resizing - PRF 8712
    ASSERT(Create(NULL, "Equipment Diagramm", WS_HSCROLL | WS_OVERLAPPEDWINDOW /*| WS_VISIBLE*/ | WS_POPUP, olRect,pogMainWnd,NULL,0,NULL));
	SetCaptionText();
	bmScrolling = false;

	BOOL blMinimized = FALSE;
	CRect olTempRect;
	ogBasicData.GetWindowPosition(olTempRect,ogCfgData.rmUserSetup.EQCH,ogCfgData.rmUserSetup.MONS);
	ogBasicData.GetDialogFromReg(olTempRect, COpssPmApp::EQUIPMENT_DIAGRAM_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olTempRect);

	SetWindowPos(&wndTop, olTempRect.left,olTempRect.top, olTempRect.Width(), olTempRect.Height(), SWP_SHOWWINDOW);
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

EquipmentDiagram::~EquipmentDiagram()
{
    omPtrArray.RemoveAll();
}

BEGIN_MESSAGE_MAP(EquipmentDiagram, CFrameWnd)
    //{{AFX_MSG_MAP(EquipmentDiagram)
    ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_MOVE() //PRF 8712
    ON_WM_HSCROLL()
    ON_BN_CLICKED(IDC_PREV, OnNextChart)
    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
    ON_BN_CLICKED(IDC_MABSTAB, OnMabstab)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_ASSIGN, OnAssign)
	ON_BN_CLICKED(IDC_SEARCHBUTTON, OnSearch)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_UPDATE_COMMAND_UI(IDC_ZEIT,OnUpdateUIZeit)
	ON_UPDATE_COMMAND_UI(IDC_MABSTAB,OnUpdateUIMabstab)
	ON_UPDATE_COMMAND_UI(IDC_PRINT,OnUpdateUIPrint)
	ON_UPDATE_COMMAND_UI(IDC_ASSIGN,OnUpdateUIAssign)
	ON_UPDATE_COMMAND_UI(IDC_ANSICHT,OnUpdateUIAnsicht)
	ON_UPDATE_COMMAND_UI(IDC_VIEW,OnUpdateUIView)
    ON_MESSAGE(WM_SELECTDIAGRAM, OnSelectDiagram)
	ON_EN_SETFOCUS(IDC_SEARCHFIELD, OnSetfocusSearchField)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void EquipmentDiagram::PrePlanMode(BOOL bpToSet,CTime opPrePlanTime)
{
	if (bpToSet)
	{
		omPrePlanTime = opPrePlanTime;
		omPrePlanMode = TRUE;

		SetTSStartTime(omPrePlanTime);
		omTimeScale.SetDisplayStartTime(omTSStartTime);
		omTimeScale.Invalidate(TRUE);
		omClientWnd.Invalidate(FALSE);

		// update scroll bar position
		long llTotalMin = CalcTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
	}
	else
	{
		omPrePlanTime = opPrePlanTime;
		omPrePlanMode = FALSE;
	}

	SetCaptionText();  // sets the caption

	// Id 30-Sep-96
	// Force the diagram to redisplay data again when switch to and from preplan mode.
	// This will fix the bug that the diagram confuse the time and need refreshing by a HScroll.
	ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
}


void EquipmentDiagram::SetAllEquipmentAreaButtonsColor(void)
{
	int ilGroupCount = omViewer.GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		SetEquipmentAreaButtonColor(ilGroupno);
	}
}

void EquipmentDiagram::SetEquipmentAreaButtonColor(int ipGroupno)
{
	CTime ilStart = ogBasicData.GetTime();
	CTime ilEnd = ilStart + CTimeSpan(0,3,0,0);
	EquipmentChart *polEquipmentChart;
	int ilColorIndex;

	ilColorIndex = omViewer.GetGroupColorIndex(ipGroupno,ilStart,ilEnd);
	polEquipmentChart = (EquipmentChart *) omPtrArray.GetAt(ipGroupno);
	if (polEquipmentChart != NULL)
	{
		CCSButtonCtrl *prlButton =polEquipmentChart->GetChartButtonPtr();
		if (prlButton != NULL)
		{
			if (ilColorIndex == FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2))
			{
			prlButton->SetColors(::GetSysColor(COLOR_BTNFACE),
				::GetSysColor(COLOR_BTNSHADOW),
				::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else
			{
			prlButton->SetColors(ogColors[ilColorIndex],
				::GetSysColor(COLOR_BTNSHADOW),
				::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
	}
}

void EquipmentDiagram::SetCaptionText(void)
{
	//omCaptionText = CString("Equipment Diagramm - ");
	omCaptionText = GetString(IDS_EQUIPMENTDIAGRAM_CAPTION);
//	if (bgOnline)
//	{
//		//omCaptionText += "  Online";
//		omCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//omCaptionText += "  OFFLINE";
//		omCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}
	SetWindowText(omCaptionText);
}

void EquipmentDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    EquipmentChart *polChart;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (EquipmentChart *) omPtrArray.GetAt(ilIndex);

        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }

        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;

        olChartRect.top = ilLastY;

        ilLastY += polChart->GetHeight();
        olChartRect.bottom = ilLastY;
        
        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) &&
			(olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        
        polChart->MoveWindow(&olChartRect, FALSE);
        polChart->ShowWindow(SW_SHOW);
	}

    omClientWnd.Invalidate(TRUE);
	SetAllEquipmentAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}

void EquipmentDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
    
    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

CListBox *EquipmentDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);

	// Searching for the bottommost chart
	EquipmentChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (EquipmentChart *)omPtrArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}

	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omPtrArray.GetSize()-1))
		return NULL;

	return &((EquipmentChart *)omPtrArray[ilLc])->omGantt;
}

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagram message handlers

int EquipmentDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    omViewer.Attach(this);

    CRect olRect; GetClientRect(&olRect);
   
    // height is 20 point
    omDialogBar.Create(this, IDD_EQUIPMENTDIAGRAM, CBRS_TOP, IDD_EQUIPMENTDIAGRAM);

	CWnd *polWnd = omDialogBar.GetDlgItem(IDC_ANSICHT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = omDialogBar.GetDlgItem(IDC_MABSTAB); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61842));
	}
	polWnd = omDialogBar.GetDlgItem(IDC_ZEIT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32896));
		CTime olCurrTime = ogBasicData.GetLocalTime();
		if(olCurrTime < ogBasicData.GetTimeframeStart() || olCurrTime > ogBasicData.GetTimeframeEnd())
		{
			// cannot set the gantt to the current local time because it is outside of the timeframe
			polWnd->EnableWindow(false);
		}
	}
	polWnd = omDialogBar.GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}

	polWnd = omDialogBar.GetDlgItem(IDC_ASSIGN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61976));
	}

	polWnd = omDialogBar.GetDlgItem(IDC_SEARCHBUTTON); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32874));
	}


	UpdateComboBox();

	CTime olCurrentTime = ogBasicData.GetTime();
    CTime olCurrentUtcTime = ogBasicData.GetUtcTime();
	CTime olTimeframeStart = ogBasicData.GetTimeframeStart();
	CTime olTimeframeEnd = ogBasicData.GetTimeframeEnd();
	
	CTimeScaleDialog* polTSD = NULL;
	if(pogButtonList->pomMapDiagramToScaleDlg->Lookup(EQUIPMENTDIAGRAM,(CObject*&)polTSD) == FALSE)
	{
		// the current time window displayed
		omTSDuration = CTimeSpan(0, 8, 0, 0);
		// intervals on the timescale
		omTSInterval = CTimeSpan(0, 0, 10, 0);
		// start time of the whole gantt chart (ie not just the time window currently displayed)
		omStartTime = olTimeframeStart;
		// duration of the whole gantt chart --> minimum 8 hours (+1 minute to stop divide by zero)
		omDuration = max(olTimeframeEnd - olTimeframeStart,omTSDuration)+CTimeSpan(0,0,1,0);
		// the display start time (typically: current time - 1 hour)
		omTSStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
	}
	else
	{	// the current time window displayed
		omTSDuration = CTimeSpan(0,polTSD->m_Hour,0,0);
		// intervals on the timescale
		omTSInterval = polTSD->omTSI;
		// start time of the whole gantt chart (ie not just the time window currently displayed)
		omStartTime = olTimeframeStart;
		// duration of the whole gantt chart --> minimum 8 hours (+1 minute to stop divide by zero)
		omDuration = max(olTimeframeEnd - olTimeframeStart,omTSDuration)+CTimeSpan(0,0,1,0);
		// the display start time (typically: current time - 1 hour)
		omTSStartTime = polTSD->m_TimeScale;
	}
    
    
    
    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olCurrentTime.GetHour(), olCurrentTime.GetMinute(),        
        olCurrentUtcTime.GetHour(), olCurrentUtcTime.GetMinute()
    );
    omTime.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 172, 6, olRect.right - 92, 23), this);

    sprintf(olBuf, "%02d%02d%02d",
        olCurrentTime.GetDay(), olCurrentTime.GetMonth(), olCurrentTime.GetYear() % 100);
    omDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 6, olRect.right - 8, 23), this);

    sprintf(olBuf, "%02d%02d%02d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 42, ilPos + 80, 59), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "PREVX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 34, olRect.right - (36 + 2), 68), this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    
    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);
    
    olRect.top += 70;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	if (bgIsPreplanMode)
	{
		SetTSStartTime(omPrePlanTime);
		omTimeScale.SetDisplayStartTime(omTSStartTime);
	}

    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = CalcTotalMinutes();
	if(llTotalMin == 0)
	{
		llTotalMin = 1; // prevent divide by zero
	}
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);
	SetTimeBand(ogBasicData.omTimebandStart, ogBasicData.omTimebandEnd);


    EquipmentChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new EquipmentChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "EquipmentChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
        
        omPtrArray.Add(polChart);
        ilLastY += polChart->GetHeight();
    }

	ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
	SetAllEquipmentAreaButtonsColor();
    OnTimer(0);

    m_DragDropTarget.RegisterTarget(this, this);

	// Register DDX call back function
	TRACE("EquipmentDiagram: DDX Registration\n");
	ogCCSDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("EquipmentDiagram"),CString("Update Time Band"), EquipmentDiagramCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("EquipmentDiagram"),CString("Global Date Change"), EquipmentDiagramCf);
	ogCCSDdx.Register(this, PREPLAN_DATE_UPDATE, CString("EquipmentDiagram"),CString("Preplan Date Change"), EquipmentDiagramCf);

    return 0;
}

void EquipmentDiagram::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::EQUIPMENT_DIAGRAM_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogCCSDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("EquipmentDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void EquipmentDiagram::OnClose() 
{
   	pogButtonList->m_wndEquipmentDiagram = NULL;
	pogButtonList->m_EquipmentDiagramButton.Recess(FALSE);
    CFrameWnd::OnClose();
}

void EquipmentDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL EquipmentDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
}

void EquipmentDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 33); dc.LineTo(olRect.right, 33);
    dc.MoveTo(olRect.left, 68); dc.LineTo(olRect.right, 68);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 69); dc.LineTo(olRect.right, 69);
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 33);
    dc.LineTo(imStartTimeScalePos - 2, 69);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 33);
    dc.LineTo(imStartTimeScalePos - 1, 69);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}

void EquipmentDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 34, olRect.right - (36 + 2), 68);
    omTimeScale.MoveWindow(&olTSRect, FALSE);
    
    olRect.top += 70;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	// Update top scale text
	omTimeScale.GetClientRect(&olRect);
//	omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

    PositionChild();
    if(nType != SIZE_MINIMIZED)
    {
        GetWindowRect(&omWindowRect);
    }
	//ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
}

void EquipmentDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    long llTotalMin;
    int ilPos;
	CRect olRect;

    switch (nSBCode)
	{
    case SB_LINEUP :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
        if (ilPos <= 0)
        {
            ilPos = 0;
            SetTSStartTime(omStartTime);
        }
        else
            SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);
		
		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
		ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
        SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
	    break;
    
    case SB_LINEDOWN :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
        if (ilPos >= 1000)
        {
            ilPos = 1000;
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
        }
        else
		{
            SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));
		}

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);

		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
		ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
        SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
		break;
    
    case SB_PAGEUP :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ)
            - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
        if (ilPos <= 0)
        {
            ilPos = 0;
            SetTSStartTime(omStartTime);
        }
        else
		{
            SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));
		}

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);
		
		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
        ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
        SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
		break;
    
    case SB_PAGEDOWN :
        llTotalMin = CalcTotalMinutes();            
        ilPos = GetScrollPos(SB_HORZ)
            + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
        if (ilPos >= 1000)
        {
            ilPos = 1000;
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
        }
        else
            SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);

		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
		ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
        SetScrollPos(SB_HORZ, ilPos, TRUE);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
	    break;
    
    case SB_THUMBTRACK /* pressed, any drag time */:
        llTotalMin = CalcTotalMinutes();
        SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
        omTimeScale.SetDisplayStartTime(omTSStartTime);
        omTimeScale.Invalidate(TRUE);

		omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
		omClientWnd.Invalidate(FALSE);
        
        SetScrollPos(SB_HORZ, nPos, TRUE);
	    break;

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
    case SB_THUMBPOSITION:	// the thumb was just released?
		// Update top scale text
		omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
        ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
		omClientWnd.Invalidate(FALSE);
		return;
////////////////////////////////////////////////////////////////////////

    case SB_TOP:
    case SB_BOTTOM:
    case SB_ENDSCROLL:
	    break;
    }
}

void EquipmentDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void EquipmentDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omPtrArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void EquipmentDiagram::OnFirstChart()
{
	imFirstVisibleChart = 0;
	OnUpdatePrevNext();
	PositionChild();
}

void EquipmentDiagram::OnLastChart()
{
	imFirstVisibleChart = omPtrArray.GetUpperBound();
	OnUpdatePrevNext();
	PositionChild();
}

void EquipmentDiagram::OnTimer(UINT nIDEvent)
{
 	if(bmNoUpdatesNow == FALSE)
	{
		if(nIDEvent == 0)
		{
			char clBuf[16];
			CTime olCurrentTime = ogBasicData.GetTime();

			omTimeScale.UpdateCurrentTimeLine(olCurrentTime);

			CTime olCurrentUtcTime = ogBasicData.GetUtcTime();
			sprintf(clBuf, "%02d%02d/%02d%02dz",
				olCurrentTime.GetHour(), olCurrentTime.GetMinute(),        
				olCurrentUtcTime.GetHour(), olCurrentUtcTime.GetMinute()
			);
			omTime.SetWindowText(clBuf);
			omTime.Invalidate(FALSE);

			sprintf(clBuf, "%02d%02d%02d",
				olCurrentTime.GetDay(), olCurrentTime.GetMonth(), olCurrentTime.GetYear() % 100);
			omDate.SetWindowText(clBuf);
			omDate.Invalidate(FALSE);
			//

			SetAllEquipmentAreaButtonsColor();

			EquipmentChart *polChart;
			for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
			{
				polChart = (EquipmentChart *) omPtrArray.GetAt(ilIndex);
				polChart->GetGanttPtr()->SetCurrentTime(olCurrentTime);
			}
		}
		else if(nIDEvent == AUTOSCROLL_TIMER_EVENT)
		{
			// called during drag&drop, does automatic scrolling when the cursor
			// is outside the main gantt chart
			OnAutoScroll();
		}

		CFrameWnd::OnTimer(nIDEvent);
	}
}

LONG EquipmentDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG EquipmentDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	CTime olTime = CTime((time_t) lParam);

    EquipmentChart *polEquipmentChart = (EquipmentChart *) omPtrArray.GetAt(ipGroupNo);
	if(polEquipmentChart == NULL)
	{
		return 0L;
	}
    EquipmentGantt *polEquipmentGantt = polEquipmentChart -> GetGanttPtr();
	if(polEquipmentGantt == NULL)
	{
		return 0L;
	}

    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            // CreateChild(ipGroupNo);

            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ipGroupNo);
            polEquipmentChart->GetChartButtonPtr()->SetWindowText(olStr);
            polEquipmentChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ipGroupNo);
            polEquipmentChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            polEquipmentChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			SetEquipmentAreaButtonColor(ipGroupNo);
        break;
        
        case UD_DELETEGROUP :
            delete omPtrArray.GetAt(ipGroupNo);
            //omPtrArray.GetAt(ipGroupNo) -> DestroyWindow();
            omPtrArray.RemoveAt(ipGroupNo);
            
            if (ipGroupNo <= imFirstVisibleChart)
			{
                imFirstVisibleChart--;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        // line message
        case UD_INSERTLINE :
            polEquipmentGantt->InsertString(ipLineNo, "");
			polEquipmentGantt->RepaintItemHeight(ipLineNo);
			SetEquipmentAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polEquipmentGantt->RepaintVerticalScale(ipLineNo);
            polEquipmentGantt->RepaintGanttChart(ipLineNo);
			SetEquipmentAreaButtonColor(ipGroupNo);
        break;

        case UD_DELETELINE :
            polEquipmentGantt->DeleteString(ipLineNo);
			SetEquipmentAreaButtonColor(ipGroupNo);
            PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polEquipmentGantt->RepaintItemHeight(ipLineNo);
			SetEquipmentAreaButtonColor(ipGroupNo);
            PositionChild();
        break;
		case UD_PREPLANMODE :
			SetTSStartTime(olTime);
			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
    		omClientWnd.Invalidate(FALSE);
				
			// update scroll bar position
			long llTotalMin = CalcTotalMinutes();
			long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
			nPos = nPos * 1000L / llTotalMin;
			SetScrollPos(SB_HORZ, int(nPos), TRUE);
		break;
    }

    return 0L;
}

LONG EquipmentDiagram::OnSelectDiagram(WPARAM wParam, LPARAM lParam)
{
	EQUIPMENT_SELECTION	*polSelection = reinterpret_cast<EQUIPMENT_SELECTION	*>(lParam);
	if (!polSelection)
		return 0L;


    EquipmentChart *polEquipmentChart = (EquipmentChart *) omPtrArray.GetAt(polSelection->imGroupno);
	if (polEquipmentChart == NULL)
	{
		return 0L;
	}

    EquipmentGantt *polEquipmentGantt = polEquipmentChart -> GetGanttPtr();
	if (polEquipmentGantt == NULL)
	{
		return 0L;
	}

    switch (wParam)
    {
	// select line
    case UD_SELECTLINE :
		{
			EQUIPMENT_LINEDATA *prlLine = omViewer.GetLine(polSelection->imGroupno,polSelection->imLineno);
			if (!prlLine)
				return 0L;

			// try to get first pool job of shift
//			EQUIPMENT_BKBARDATA *prlBkBar  = omViewer.GetBkBar(polSelection->imGroupno,polSelection->imLineno,0);

			// check, if visible chart
			if (polSelection->imGroupno < imFirstVisibleChart)
			{
				imFirstVisibleChart = polSelection->imGroupno;
				OnUpdatePrevNext();
				PositionChild();
			}
			else 
			{
				CRect olRect;
				polEquipmentChart->GetWindowRect(olRect);
				CRect olParentRect;
				omClientWnd.GetWindowRect(olParentRect);
				CRect olResult;
				olRect.NormalizeRect();
				olParentRect.NormalizeRect();

				if (!olResult.IntersectRect(olRect,olParentRect))
				{
					imFirstVisibleChart = polSelection->imGroupno;
					OnUpdatePrevNext();
					PositionChild();
				}
			}

			// maximize chart if necessary
			if (polEquipmentChart->imState == Minimized)
			{
				polEquipmentChart->OnChartButton();
			}

			// scroll vertically to line
			if(polEquipmentGantt->SetSel(polSelection->imLineno) != LB_ERR)
			{
				CRect olItemRect;
				polEquipmentGantt->GetItemRect(polSelection->imLineno, &olItemRect);
				polEquipmentGantt->ClientToScreen(olItemRect);
				::SetCursorPos(olItemRect.left, olItemRect.top);
			}

			// scroll time bar
			long llTotalMin = CalcTotalMinutes();
			long nPos = 0;

		}
	break;
	// select bar
    case UD_SELECTBAR :
		{
		}
    break;

    }

    return 0L;
}

//long EquipmentDiagram::GetScrollPosition(long lpScrollPosMins)
//{
//    long llTotalScrollMins = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
//	long llScrollPos = llTotalScrollMins > 0 ? (lpScrollPosMins / llTotalScrollMins) : lpScrollPosMins;
//	return llScrollPos * 1000;
//}

void EquipmentDiagram::OnUpdatePrevNext(void)
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();

	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);

	if (imFirstVisibleChart == omPtrArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void EquipmentDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.EQCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void EquipmentDiagram::ChangeViewTo(const char *pcpViewName)
{
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	AfxGetApp()->DoWaitCursor(1);
	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		// Id 30-Sep-96
		// This will remove a lot of warning message when the user change view.
		// If we just delete a staff chart, MFC will produce two warning message.
		// First, Revoke not called before the destructor.
		// Second, calling DestroyWindow() in CWnd::~CWnd.
        //delete (EquipmentChart *) omPtrArray.GetAt(ilIndex);
		((EquipmentChart *)omPtrArray[ilIndex])->DestroyWindow();
    }
    omPtrArray.RemoveAll();

    EquipmentChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
        polChart = new EquipmentChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "EquipmentChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);
		
		polChart->GetGanttPtr()->SetCurrentTime(ogBasicData.GetTime());
        
        omPtrArray.Add(polChart);

        ilLastY += polChart->GetHeight();
    }

	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
}

void EquipmentDiagram::OnAnsicht()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();

	EquipmentDiagramPropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
		ChangeViewTo(omViewer.GetViewName());
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void EquipmentDiagram::OnViewSelChange()
{
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
    TRACE("EquipmentDiagram::OnComboBox() [%s]", clText);
	ChangeViewTo(clText);
}

void EquipmentDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
}

void EquipmentDiagram::OnMabstab()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
	//Singapore
	CTimeScaleDialog* polTSD = NULL;
	if(pogButtonList->pomMapDiagramToScaleDlg->Lookup(EQUIPMENTDIAGRAM,(CObject*&)polTSD) == FALSE)
	{
		polTSD = new CTimeScaleDialog(this);    
		polTSD->pomTS = &omTimeScale;
		polTSD->omTSI = omTSInterval;
		polTSD->m_TimeScale = omTSStartTime;		
		polTSD->m_Hour = (int) omTSDuration.GetTotalHours();
		polTSD->imMaxHours = omDuration.GetTotalHours();		
		pogButtonList->pomMapDiagramToScaleDlg->SetAt(EQUIPMENTDIAGRAM,polTSD);
	}
    CTimeScaleDialog olTSD(this);
    olTSD.pomTS = polTSD->pomTS;
	olTSD.omTSI = polTSD->omTSI;
	//olTSD.m_TimeScale = polTSD->m_TimeScale;	
	olTSD.m_TimeScale = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
                atoi(polTSD->m_startTime), 0, 0);
	olTSD.m_Hour = polTSD->m_Hour;
	olTSD.imMaxHours = polTSD->imMaxHours;

    /*CTimeScaleDialog olTSD(this); //Commented Singapore

    olTSD.pomTS = &omTimeScale;
    olTSD.omTSI = omTSInterval;
    olTSD.m_TimeScale = omTSStartTime;
    //olTSD.m_TimeScale = ogBasicData.GetTime();
    olTSD.m_Hour = (int) omTSDuration.GetTotalHours();
	olTSD.imMaxHours = omDuration.GetTotalHours();*/

	if(ogEquipmentIndexes.Chart == MS_SANS6)
	{
	    olTSD.m_Percent = 50;
	}
	else if(ogEquipmentIndexes.Chart == MS_SANS8)
	{
	    olTSD.m_Percent = 75;
	}
	else if(ogEquipmentIndexes.Chart == MS_SANS12)
	{
	    olTSD.m_Percent = 100;
	}
    if (olTSD.DoModal() == IDOK)
    {
		//Singapore
		polTSD->pomTS = olTSD.pomTS;
		polTSD->omTSI = olTSD.omTSI;
		polTSD->m_TimeScale = olTSD.m_TimeScale;	
		polTSD->m_Hour = olTSD.m_Hour;
		polTSD->imMaxHours = olTSD.imMaxHours;
		polTSD->m_startTime = olTSD.m_startTime;

        SetTSStartTime(olTSD.m_TimeScale);
        omTSDuration = CTimeSpan(0, olTSD.m_Hour, 0, 0);
        if(olTSD.m_Percent == 50)
		{
			//ogEquipmentIndexes.VerticalScale = MS_SANS8;
			ogEquipmentIndexes.VerticalScale = MS_SANS8;
			ogEquipmentIndexes.Chart = MS_SANS6;
		}
        else if(olTSD.m_Percent == 75)
		{
			//ogEquipmentIndexes.VerticalScale = MS_SANS12;
			ogEquipmentIndexes.VerticalScale = MS_SANS16;
			ogEquipmentIndexes.Chart = MS_SANS8;
		}
        else if(olTSD.m_Percent == 100)
		{
			//ogEquipmentIndexes.VerticalScale = MS_SANS16;
			ogEquipmentIndexes.VerticalScale = MS_SANS12;
			ogEquipmentIndexes.Chart = MS_SANS12;
		}
		else
		{
			//ogEquipmentIndexes.VerticalScale = MS_SANS8;
			ogEquipmentIndexes.VerticalScale = MS_SANS6;
			ogEquipmentIndexes.Chart = MS_SANS6;
		}
        
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);

		// Update top scale text
		CRect olRect;
	    omTimeScale.GetClientRect(&olRect);
//		omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
        ChangeViewTo(ogCfgData.rmUserSetup.EQCV);

		omClientWnd.Invalidate(FALSE);

		// update scroll bar position
		long llTotalMin = CalcTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
    }
    else
    {
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
    }
}

void EquipmentDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();

	// TODO: Add your command handler code here
	CTime olTime = ogBasicData.GetTime();
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
    
	CRect olRect;
    omTimeScale.GetClientRect(&olRect);
//	omViewer.UpdateManagers(omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
    omClientWnd.Invalidate(FALSE);
		
	// update scroll bar position
    long llTotalMin = CalcTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);
}

void EquipmentDiagram::OnPrint()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	SetFocus();
	omViewer.PrintDiagram(omPtrArray);
}

BOOL EquipmentDiagram::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	TRACE("%d EquipmentDiagram closed\n",clock());
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

CTime EquipmentDiagram::GetTsStartTime()
{
	return omTSStartTime;
}

CTimeSpan EquipmentDiagram::GetTsDuration(void)
{
	return omTSDuration; 
};

////////////////////////////////////////////////////////////////////////
// EquipmentDiagram keyboard handling

void EquipmentDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
	case VK_PRIOR:
		blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		blIsControl? OnLastChart(): OnNextChart();
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void EquipmentDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// EquipmentDiagram -- implementation of DDX call back function

static void EquipmentDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	EquipmentDiagram *polDiagram = (EquipmentDiagram *)popInstance;

	if (ipDDXType == STAFFDIAGRAM_UPDATETIMEBAND)
	{
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE || ipDDXType == PREPLAN_DATE_UPDATE)
	{
		polDiagram->HandleGlobalDateUpdate(*((CTime *) vpDataPointer));
	}
}


void EquipmentDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void EquipmentDiagram::UpdateTimeBand()
{
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		EquipmentChart *polChart = (EquipmentChart *)omPtrArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
}

void EquipmentDiagram::RedisplayAll()
{
	ChangeViewTo(ogCfgData.rmUserSetup.EQCV);
	SetCaptionText();
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void EquipmentDiagram::HandleGlobalDateUpdate(CTime opDate)
{
	//omPrePlanTime = opDate; //Singapore
	omPrePlanTime = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),omTSStartTime.GetHour(),
		                  omTSStartTime.GetMinute(),omTSStartTime.GetSecond()); //Singapore
	SetTSStartTime(omPrePlanTime);
	omTimeScale.SetDisplayStartTime(omTSStartTime);
	omTimeScale.Invalidate(TRUE);
	CRect olRect;
	omTimeScale.GetClientRect(&olRect);
//	omViewer.UpdateManagers(omTSStartTime,omTimeScale.GetTimeFromX(olRect.Width() + 38));

	omClientWnd.Invalidate(FALSE);

	// update scroll bar position
	long llTotalMin = CalcTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	SetCaptionText();
}

// scroll prev/next charts when dragging+dropping over the prev/next buttons
LONG EquipmentDiagram::OnDragOver(UINT wParam, LONG lParam)
{
	CPoint olDropPosition;
	::GetCursorPos(&olDropPosition);

	CRect olRect;
	CWnd *polWnd;

	polWnd = GetDlgItem(IDC_NEXT); 
	if(polWnd != NULL)
	{
		polWnd->GetWindowRect(&olRect);
		if(olRect.PtInRect(olDropPosition))
		{
			AutoScroll(AUTOSCROLL_INITIAL_SPEED);
			return -1L;
		}
	}

	polWnd = GetDlgItem(IDC_PREV);
	if(polWnd != NULL)
	{
		polWnd->GetWindowRect(&olRect);
		if(olRect.PtInRect(olDropPosition))
		{
			AutoScroll(AUTOSCROLL_INITIAL_SPEED);
			return -1L;
		}
	}
	return -1L;	// cannot accept this object
}


// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void EquipmentDiagram::AutoScroll(UINT ipInitialScrollSpeed /* = AUTOSCROLL_INITIAL_SPEED*/)
{
	// if not already scrolling automatically...
	if(!bmScrolling)
	{
		// ... after a short pause (ipInitialScrollSpeed), start scrolling
		bmScrolling = true;
		SetTimer(AUTOSCROLL_TIMER_EVENT, (UINT) ipInitialScrollSpeed, NULL);
	}
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void EquipmentDiagram::OnAutoScroll(void)
{
	bmScrolling = false;

	// if standard cursor then not currently dragging
	if(GetCursor() != AfxGetApp()->LoadStandardCursor(IDC_ARROW))
	{
		// check if the cursor is in the automatic scrolling region
		CPoint olDropPosition;
		::GetCursorPos(&olDropPosition);

		CRect olRect;
		CWnd *polWnd;

		polWnd = GetDlgItem(IDC_NEXT); 
		if(polWnd != NULL)
		{
			polWnd->GetWindowRect(&olRect);
			if(olRect.PtInRect(olDropPosition))
			{
				OnPrevChart();
				bmScrolling = true;
			}
		}

		if(!bmScrolling)
		{
			polWnd = GetDlgItem(IDC_PREV);
			if(polWnd != NULL)
			{
				polWnd->GetWindowRect(&olRect);
				if(olRect.PtInRect(olDropPosition))
				{
					OnNextChart();
					bmScrolling = true;
				}
			}
		}
	}

	if(bmScrolling)
	{
		SetTimer(AUTOSCROLL_TIMER_EVENT, (UINT) AUTOSCROLL_SPEED, NULL);
	}
	else
	{
		// no longer in the scrolling region or left mouse button released
		KillTimer(AUTOSCROLL_TIMER_EVENT);
	}
}

void EquipmentDiagram::OnSearch()
{
	bool blRc = true;
	CString olSearchText, olOrigSearchText;
	CWnd *polWnd = omDialogBar.GetDlgItem(IDC_SEARCHFIELD); 
	if(polWnd != NULL)
	{
		polWnd->GetWindowText(olSearchText);
		if(olSearchText.IsEmpty())
		{
			// please enter the name of a piece of equipment to be search
			MessageBox(GetString(IDS_NOSEARCHTEXT), GetString(IDS_STRING32874), MB_ICONSTOP);
			polWnd->SetFocus();
			blRc = false;
		}
		else
		{
			olOrigSearchText = olSearchText;
			olSearchText.MakeUpper();
		}
	}

	if(blRc)
	{
		AfxGetApp()->DoWaitCursor(1);

		int ilGroupno, ilLineno;

		CCSPtrArray <MATCHEDDATA> olMatches;

		EQUDATA *prlEqu = NULL;
		CString olText;

		int ilNumEqu = ogEquData.omData.GetSize();
		for(int ilEqu = 0; ilEqu < ilNumEqu; ilEqu++)
		{
			prlEqu = &ogEquData.omData[ilEqu];
			CString olEnam = prlEqu->Enam;
			olEnam.MakeUpper();
			if(olEnam.Find(olSearchText) != -1)
			{
				ilGroupno = -1;
				ilLineno = -1;
				while(omViewer.FindGroupAndLine(prlEqu->Urno,ilGroupno,ilLineno))
				{
					MATCHEDDATA *prlMatch = new MATCHEDDATA;

					CString olType = "Unknown";
					EQTDATA *prlEqt = ogEqtData.GetEqtByUrno(prlEqu->Gkey);
					if(prlEqt != NULL)
					{
						olType = prlEqt->Name;
					}
					prlMatch->Text.Format("%s  (%s %s)  (%s %s)",prlEqu->Enam,GetString(IDS_EQ_TYPE),olType,GetString(IDS_EQ_GROUP),omViewer.GetGroupText(ilGroupno));
					prlMatch->Groupno = ilGroupno;
					prlMatch->Lineno = ilLineno;
					olMatches.Add(prlMatch);
				}
			}
		}

		MATCHEDDATA *prlSelectedMatch = NULL;
		prlEqu = NULL;
		long llIndex = -1;
		int ilNumMatches = olMatches.GetSize();
		if(ilNumMatches <= 0)
		{
			CString olTxt;
			olTxt.Format("%s \"%s\"",GetString(IDS_EQSEARCHNOTFOUND),olOrigSearchText);
			MessageBox(olTxt,GetString(IDS_STRING32874), MB_ICONSTOP);
			blRc = false;
		}
		else if(ilNumMatches > 1)
		{
			CSearchResults olDlg;
			for(int ilR = 0; ilR < ilNumMatches; ilR++)
			{
				olDlg.Add(&olMatches[ilR]);
			}
			olDlg.omSearchString = olOrigSearchText;
			if(olDlg.DoModal() != IDOK)
			{
				blRc = false;
			}
			else
			{
				prlSelectedMatch = olDlg.prmMatch;
			}
		}
		else
		{
			prlSelectedMatch = &olMatches[0];
		}

		EQUIPMENT_SELECTION	rolSelection;
		if(blRc && prlSelectedMatch != NULL)
		{
			rolSelection.imGroupno = prlSelectedMatch->Groupno;
			rolSelection.imLineno = prlSelectedMatch->Lineno;
		}
		else
		{
			blRc = false;
		}

		if(blRc)
		{
			LONG lParam = reinterpret_cast<LONG>(&rolSelection);
			OnSelectDiagram(UD_SELECTLINE, lParam);
		}

		olMatches.DeleteAll();
		AfxGetApp()->DoWaitCursor(-1);
	}
}

void EquipmentDiagram::OnAssign()
{
//	CStringArray olReductionList; // not used for the moment
//	CAutoAssignDlg olAutoAssignDlg(ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd(),ALLOCUNITTYPE_EQUIPMENT,olReductionList,this);
//
//	CUIntArray olDemandUrnoList;
//	omViewer.GetDemandList(olDemandUrnoList);
//
//	CStringArray olPoolNames;
//	CString olPoolString; 
//
//	int ilNumPools = ogPolData.omData.GetSize();
//	for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
//	{
//		POLDATA *prlPol = &ogPolData.omData[ilPool];
//		olPoolString.Format("%s#%ld",prlPol->Name,prlPol->Urno);
//		olPoolNames.Add(olPoolString);
//	}
//	CTime olStartTime = (ogBasicData.GetTimeframeStart() > omTSStartTime) ? ogBasicData.GetTimeframeStart() : omTSStartTime;
//	CTime olEndTime = olStartTime + omTSDuration;
//	if(olEndTime > ogBasicData.GetTimeframeEnd())
//	{
//		olEndTime = ogBasicData.GetTimeframeEnd();
//	}
//	olAutoAssignDlg.SetData(olDemandUrnoList,olPoolNames,olStartTime,olEndTime);
//	olAutoAssignDlg.DoModal();
}

void EquipmentDiagram::OnUpdateUIZeit(CCmdUI *pCmdUI)
{
	// check, if current time is inside time frame
	if (!ogBasicData.IsDisplayDateInsideTimeFrame())
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		CWnd *pWnd = omDialogBar.GetDlgItem(IDC_ZEIT);
		if (pWnd)
		{
			ogBasicData.SetWindowStat("EQUIPMENTDIAGRAM IDC_ZEIT",pWnd);
			pCmdUI->Enable(pWnd->IsWindowEnabled());
		}
	}
}

void EquipmentDiagram::OnUpdateUIMabstab(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_MABSTAB);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("EQUIPMENTDIAGRAM IDC_MABSTAB",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void EquipmentDiagram::OnUpdateUIPrint(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_PRINT);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("EQUIPMENTDIAGRAM IDC_PRINT",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void EquipmentDiagram::OnUpdateUIAssign(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_ASSIGN);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("EQUIPMENTDIAGRAM IDC_ASSIGN",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void EquipmentDiagram::OnUpdateUIAnsicht(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_ANSICHT);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("EQUIPMENTDIAGRAM IDC_ANSICHT",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}
void EquipmentDiagram::OnUpdateUIView(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_VIEW);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("EQUIPMENTDIAGRAM IDC_VIEW",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void EquipmentDiagram::OnSetfocusSearchField() 
{
	CEdit *polSearchField = (CEdit *) omDialogBar.GetDlgItem(IDC_SEARCHFIELD);
	if(polSearchField != NULL)
	{
		polSearchField->SetSel(0,-1);
	}
//	SendDlgItemMessage(omDialogBar.hDlg, IDC_SEARCHBUTTON, BM_SETSTYLE, BS_DEFPUSHBUTTON, (LONG)TRUE);
//	CButton *polSearchButton = (CButton *) omDialogBar.GetDlgItem(IDC_SEARCHBUTTON);
//	if(polSearchButton != NULL)
//	{
//		polSearchButton->ModifyStyle((DWORD) 0L, (DWORD) BS_DEFPUSHBUTTON, (UINT) 0);
//	}
}

long EquipmentDiagram::CalcTotalMinutes(void)
{
	long llTotalMinutes = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	if(llTotalMinutes <= 0)
	{
		llTotalMinutes = 1; // prevent divide by zero error
	}
	return llTotalMinutes;
}

//PRF 8712
void EquipmentDiagram::OnMove(int x, int y)
{	
	CFrameWnd::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}