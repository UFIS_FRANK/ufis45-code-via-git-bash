#include <string.h>
#include <cformat.h>

void CFormat::Init()
{
   	omDataBuf = "";
    m_point = '.';
	m_seperate = ',';
}


const void CFormat::SetFormat(char pcSeperate,char pcPoint)
{
    m_point = pcPoint;
    m_seperate = pcSeperate;
}             
             

// Construction/Destruction
CFormat::CFormat()
{
	CFormat::Init();
}
 

CFormat::CFormat(const char *s,const char *fmt)
{
	CFormat::Init();
}


CFormat::CFormat(const int i,const char *fmt)
{
	CFormat::Init();
	omDataBuf = CFormat::Encode((double) i ,fmt);
}                                                                 							


CFormat::CFormat(const int long l,const char *fmt)
{
	CFormat::Init();                             
	omDataBuf = CFormat::Encode((double) l ,fmt);
}


CFormat::CFormat(const float f,const char *fmt)
{
	CFormat::Init();
	omDataBuf = CFormat::Encode((double) f ,fmt);
}


CFormat::CFormat(const double d,const char *fmt)
{
	CFormat::Init();
	omDataBuf = CFormat::Encode((double) d ,fmt);
}
 

CFormat::CFormat(const char *s)
{                   
	CFormat::Init();
    omDataBuf = s;
}


CFormat::~CFormat(void)
{
	TRACE("CFormat::~CFormat called\n");
}

const CString& CFormat::GetBuffer()
{
	return this->omDataBuf;
}


const CString& CFormat::MakeFormat(const char *s,const char *fmt)
{
	char buffer[255];
	
	sprintf(buffer,fmt,s);
    omDataBuf = buffer;
    return this->omDataBuf;	
}


const CString& CFormat::MakeFormat(const int i,const char *fmt)
{   
 
    return CFormat::Encode((double )i,fmt);
}


const CString& CFormat::MakeFormat(const int long l,const char *fmt)
{
   	return CFormat::Encode((double )l,fmt);
}


const CString& CFormat::MakeFormat(const float f,const char *fmt)
{
   	return CFormat::Encode((float )f,fmt);
}  


const CString& CFormat::MakeFormat(const double d,const char *fmt)
{
   	return CFormat::Encode((double )d,fmt);
}  


const CString& CFormat::Encode(const double d,const char *fmt)
{      
	char buffer[255];
	char f[255];
	char *p; 
	int  i,j;
	int  width;
	int  dec;
	char result[255];

	if(strstr(fmt,"%") != NULL)
    {
        strcpy(f,fmt);
        if( 
           ((p = strchr(f,'d')) != NULL) ||
           ((p = strchr(f,'f')) != NULL)
          ) 
          {
             *p = 'g';
             sprintf(buffer,f,d);
          }
          else
            sprintf(buffer,"%g",d);
    }
    else //'Z','9'
    {
        width = strlen(fmt);
	    p = strchr(fmt,m_point) ;

	    if (p != NULL)
	    {
	        p++ ;
	        dec = strlen(p) ;
	    }
	    else
	    {
	        dec = 0 ;
	    }
	    
	    strcpy(result,fmt);
	    
        sprintf(buffer,"%.10g",d) ;
        p = strchr(buffer,'.');
    
        i = 0;
        if(p != NULL )
        {
            i = strlen(p+1);
        	memmove(p,p+1,i);
        	p[i] = '\0';
        }               
        else                         
        {
           p = buffer+strlen(buffer);
        }
           
        if(i < dec) 
        {
            sprintf(&p[i],"%.*d",dec-i,0);
        }
        else
        if(i > dec)
        {
            p[dec] = '\0';
        }

	    for(j=strlen(buffer)-1,i=width-1;i>=0;i--)
	    {
	       	switch(toupper(fmt[i]))
           	{
        		case 'Z':
               	case '9':
               	case '0':
               	    if (j >= 0 )
               	    {
             			result[i] = buffer[j--] ;
             		}
             		else               
             		{
             		   result[i] = '0';		
             		}   
             	    break ;
            }
        }
         
 		//validate buffer
 		for(i=0;i<width;i++)
 		{
 	    	if(result[i]=='0')
 	    	{
 	       		if ( toupper(fmt[i]) == 'Z')
 	        		result[i] = ' ';
 	    	}      
 	    	else
 	    	   	break;   
 		}
 	}
	omDataBuf = result ;
	return this->omDataBuf ;
}
             

// Description:
// Appends the contents of the CCSString argument \fIas\fP to the end of
// the current CCSString.
void CFormat::Concat(const CFormat& as)
{
  	omDataBuf += as.omDataBuf;
}

// Description:
// Appends the contents of the const char* argument \fIs\fP to the end
// of the current CCSString.
void CFormat::Concat(const char* s)
{
  	if(s != NULL)
  	{
      	omDataBuf += s;
  	}
}


void CFormat::Concat(const CString& s)
{
  	if(!s.IsEmpty())
  	{
      	omDataBuf += s;
  	}
}

// Description:
// Return an CFormat formed by joining two CFormat.
CFormat operator+(const CFormat& as, const CFormat& bs)
{   
    CFormat tmp;
    
  	tmp.Concat(as);
  	tmp.Concat(bs);
  	return tmp;
}

// Description:
// Return an CCSString formed by joining an CCSString and a const char*.
CFormat operator+(const CFormat& as, const char* s)
{ 
	CFormat tmp;

  	tmp.Concat(as);
  	tmp.Concat(s);
  	return tmp;
}

// Description:
// Return an CFormat formed by joining a const char* and an CFormat.
CFormat operator+(const char* s, const CFormat& as)
{   
   	CFormat tmp;
   
	tmp.Concat(s);
	tmp.Concat(as);
  	return tmp;
}

CFormat operator+(const CFormat& as,const CString& s)
{
   	CFormat tmp;

	tmp.Concat(as);
	tmp.Concat(s);
	return tmp;
}

CFormat operator+(const CString& s,const CFormat& as)
{   
   	CFormat tmp;
   
	tmp.Concat(s);
	tmp.Concat(as);
  	return tmp;
}

CFormat::operator const char*() const
{
    return omDataBuf;
}
