// ConflictTableSortPage.h : header file
//
#ifndef _CFTBLSO_H_
#define _CFTBLSO_H_
/////////////////////////////////////////////////////////////////////////////
// ConflictTableSortPage dialog

class ConflictTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(ConflictTableSortPage)

// Construction
public:
	ConflictTableSortPage();
	~ConflictTableSortPage();

// Dialog Data
	CStringArray omSortOrders;

	//{{AFX_DATA(ConflictTableSortPage)
	enum { IDD = IDD_CONFLICTTABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(ConflictTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(ConflictTableSortPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimedirection();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
	CBitmap omUpArrow, omDownArrow;
	bool SetDirectionButton(int ipButtonId, bool bpAscending);
public:
	bool bmAscending;
};

#endif // _CFTBLSO_H_
