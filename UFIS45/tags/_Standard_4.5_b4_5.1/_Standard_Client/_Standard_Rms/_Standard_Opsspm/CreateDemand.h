#if !defined(AFX_CREATEDEMAND_H__D08761F1_0B99_11D4_93B4_0050DA1CABCB__INCLUDED_)
#define AFX_CREATEDEMAND_H__D08761F1_0B99_11D4_93B4_0050DA1CABCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CreateDemand.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CreateDemand dialog

class CreateDemand : public CDialog
{
// Construction
public:
	CreateDemand(CWnd* pParent = NULL);   // standard constructor
	CreateDemand(REGN_BKBARDATA *prpBarFlight, CWnd* pParent = NULL, bool bpThirdParty = false);

	CTime omStartTime,omEndTime;
// Dialog Data
	//{{AFX_DATA(CreateDemand)
	enum { IDD = IDD_CREATE_DEMAND };
	CEdit	m_NumRes;
	CComboBox	m_ComboQual;
	CComboBox	m_ComboFunc;
	CTime		m_EndDate;
	CTime		m_EndTime;
	CTime		m_StartDate;
	CTime		m_StartTime;
	CEdit	m_NameDemand;
	CButton	m_CheckHangar;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CreateDemand)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CreateDemand)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	
	REGN_BKBARDATA *prmBarFlight;

	void FillComboQualifications();
	void FillComboFunctions();
	bool IsValidTime(CString opTime, CTime &opTimeFormat);

public:
	bool bmThirdParty;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CREATEDEMAND_H__D08761F1_0B99_11D4_93B4_0050DA1CABCB__INCLUDED_)
