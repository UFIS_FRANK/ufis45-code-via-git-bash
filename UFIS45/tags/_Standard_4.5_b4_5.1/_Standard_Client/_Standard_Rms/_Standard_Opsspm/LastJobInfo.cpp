// LastJobInfo.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <LastJobInfo.h>
#include <CedaJobData.h>

/////////////////////////////////////////////////////////////////////////////
// LastJobInfo dialog


LastJobInfo::LastJobInfo(const long& rlpJobUrno,CWnd* pParent /*=NULL*/)
	: CDialog(LastJobInfo::IDD, pParent)
{	
	lmJobUrno = rlpJobUrno;
}


void LastJobInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LastJobInfo)	
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LastJobInfo, CDialog)
	//{{AFX_MSG_MAP(LastJobInfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LastJobInfo message handlers

BOOL LastJobInfo::OnInitDialog() 
{
	JOBDATA* polJobData = ogJobData.GetJobByUrno(lmJobUrno);
	if(polJobData != NULL)
	{
		if(GetDlgItem(IDC_EDIT_CDAT_DATE) != NULL && GetDlgItem(IDC_EDIT_CDAT_TIME) != NULL &&
		   GetDlgItem(IDC_EDIT_LSTU_DATE) != NULL && GetDlgItem(IDC_EDIT_LSTU_TIME) != NULL &&
		   GetDlgItem(IDC_EDIT_USEC)      != NULL && GetDlgItem(IDC_EDIT_USEU)      != NULL)
		{
			GetDlgItem(IDC_EDIT_CDAT_DATE)->SetWindowText(polJobData->Cdat.Format("%d.%m.%Y"));
			GetDlgItem(IDC_EDIT_CDAT_TIME)->SetWindowText(polJobData->Cdat.Format("%H:%M"));
			GetDlgItem(IDC_EDIT_LSTU_DATE)->SetWindowText(polJobData->Lstu.Format("%d.%m.%Y"));
			GetDlgItem(IDC_EDIT_LSTU_TIME)->SetWindowText(polJobData->Lstu.Format("%H:%M"));
			GetDlgItem(IDC_EDIT_USEC)->SetWindowText(polJobData->Usec);
			GetDlgItem(IDC_EDIT_USEU)->SetWindowText(polJobData->Useu);
		}
	}
	return TRUE; 
}