#if !defined(AFX_FLINDDEMANDTABLE_H__E565A291_0A17_11D4_93B2_0050DA1CABCB__INCLUDED_)
#define AFX_FLINDDEMANDTABLE_H__E565A291_0A17_11D4_93B2_0050DA1CABCB__INCLUDED_

#define DEBUGINFO_MENUITEM 100

#include <FlIndDemandTableViewer.h>
/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTable dialog
class FlIndDemandTableViewer;

class FlIndDemandTable : public CDialog
{
// Construction
public:
	FlIndDemandTable(CWnd* pParent = NULL);   // standard constructor
	~FlIndDemandTable();
	void UpdateView();
	void SetCaptionText(void);

	FlIndDemandTableViewer omViewer;
	FlIndDemandTableViewer omViewerT3; //Singapore
	FlIndDemandTableViewer* pomActiveViewer; //Singapore
	int m_nDialogBarHeight;

	void SetViewerDate();
	void HandleGlobalDateUpdate();
	void TableSelChange(void);
	int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;

	void ProcessEndAssignment();
	void ProcessStartAssignment();
	
	static const CString Terminal2;
	static const CString Terminal3;

private :
    CTable *pomTable; // visual object, the table content
	CTable *pomActiveTable; //Singapore
	CTable *pomTableT3; //Singapore	
	CRect omWindowRect; //PRF 8712
	BOOL bmIsViewOpen;
	static void FlIndDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void UpdateComboBox();
	BOOL FindActiveTableAndViewer(CPoint& ropPoint); //Singapore	
	void CheckScrolling(CPoint& ropPoint); //Singapore
	CMapPtrToPtr omDeleteJobMap;
	CString omActiveTerminal; //Singapore
	BOOL bmIsT2Visible; //Singapore
	BOOL bmIsT3Visible; //Singapore
	CMapPtrToPtr omAcceptConflictsMap, omConflictToJobMap; //Singapore

// Dialog Data
	//{{AFX_DATA(FlIndDemandTable)
	enum { IDD = IDD_FLIND_DEMAND };
	CComboBox	m_Date;
	CCSButtonCtrl m_bTerminal2; //Singapore
	CCSButtonCtrl m_bTerminal3; //Singapore
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlIndDemandTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CCSButtonCtrl *pomAssignButton;

	// Generated message map functions
	//{{AFX_MSG(FlIndDemandTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnView();
	afx_msg void OnPrint();
	afx_msg void OnCloseupViewcombo();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableSelChange(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnTableSetTableWindow(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnTableListHorzScroll(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnTableListVertScroll(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnTableActiveScrollWnd(WPARAM wParam,LPARAM lParam); //Singapore
	afx_msg void OnMenuWorkOn();
	afx_msg void OnMenuFreeEmployees();
	afx_msg void OnUpdateUIAssign(CCmdUI *pCmdUI);
	afx_msg void OnAssign();
	afx_msg void OnButtonT2(); //Singapore
	afx_msg void OnButtonT3(); //Singapore
	afx_msg void OnTableUpdateDataCount(); //Singapore
	afx_msg void OnMenuAcceptConflict(UINT); //Singapore
	afx_msg void OnMenuAvailableEquipment();
	afx_msg void OnMenuDeleteAssignment(UINT nID);
	afx_msg void OnMenuDebugInfo();
	afx_msg void OnMove(int x, int y); //PRF 8712
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	CCSDragDropCtrl m_DragDropControl;
	long			imDropDemandUrno;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLINDDEMANDTABLE_H__E565A291_0A17_11D4_93B2_0050DA1CABCB__INCLUDED_)
