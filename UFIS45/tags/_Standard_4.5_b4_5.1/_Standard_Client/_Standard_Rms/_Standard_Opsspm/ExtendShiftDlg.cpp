// ExtendShiftDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <ccsglobl.h>
#include <ExtendShiftDlg.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <ccsddx.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtendShiftDlg dialog


CExtendShiftDlg::CExtendShiftDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExtendShiftDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExtendShiftDlg)
	m_Employees = _T("");
	m_Text = _T("");
	m_Actod = TIMENULL;
	m_Actot = TIMENULL;
	m_Sub1Sub2 = _T("");
	m_Before = _T("");
	m_After = _T("");
	m_Complete = _T("");
	//}}AFX_DATA_INIT

	omNewShiftEndTime = TIMENULL;
	omMinShiftEndTime = TIMENULL;
	bmRemoveShiftExtension = false;
}


void CExtendShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtendShiftDlg)
	DDX_Text(pDX, IDC_EMPLOYEES, m_Employees);
	DDX_Text(pDX, IDC_TEXT, m_Text);
	DDX_CCSddmmyy(pDX, IDC_SHIFTENDDATE, m_Actod);
	DDX_CCSTime(pDX, IDC_SHIFTENDTIME, m_Actot);
	DDX_Control(pDX, IDC_E_ZCODE3, m_CompleteCtrl);
	DDX_Control(pDX, IDC_E_ZCODE2, m_AfterCtrl);
	DDX_Control(pDX, IDC_E_ZCODE, m_BeforeCtrl);
	DDX_Control(pDX, IDC_SUB1SUB2, m_Sub1Sub2Ctrl);
	DDX_CBString(pDX, IDC_SUB1SUB2, m_Sub1Sub2);
	DDX_Text(pDX, IDC_E_ZCODE, m_Before);
	DDV_MaxChars(pDX, m_Before, 1);
	DDX_Text(pDX, IDC_E_ZCODE2, m_After);
	DDV_MaxChars(pDX, m_After, 1);
	DDX_Text(pDX, IDC_E_ZCODE3, m_Complete);
	DDV_MaxChars(pDX, m_Complete, 1);
	//}}AFX_DATA_MAP


	if(pDX->m_bSaveAndValidate == TRUE)
	{
		if(!CheckDDMMYYValid(m_Actod.Format("%d%m%y")))
		{
			MessageBox(GetString(IDS_STRING61259), NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl(IDC_SHIFTENDDATE);
			pDX->Fail();
		}

		if(!CheckHHMMValid(m_Actot.Format("%H%M")))
		{
			MessageBox(GetString(IDS_STRING61213), NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl(IDC_SHIFTENDTIME);
			pDX->Fail();
		}


		omNewShiftEndTime = CTime(m_Actod.GetYear(), m_Actod.GetMonth(), m_Actod.GetDay(), m_Actot.GetHour(), m_Actot.GetMinute(), m_Actot.GetSecond());

		if(omNewShiftEndTime < omMinShiftEndTime)
		{
			CString olText;
			olText.Format(GetString(IDS_ESD_MINTIMEERR), omMinShiftEndTime.Format("%H:%M %d.%m.%Y"));
			MessageBox(olText, NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl(IDC_SHIFTENDTIME);
			pDX->Fail();
		}
	}
}


BEGIN_MESSAGE_MAP(CExtendShiftDlg, CDialog)
	//{{AFX_MSG_MAP(CExtendShiftDlg)
	ON_BN_CLICKED(IDNO, OnNoButtonClicked)
	ON_EN_CHANGE(IDC_E_ZCODE2, OnChangeEZcode)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtendShiftDlg message handlers

BOOL CExtendShiftDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(bmRemoveShiftExtension)
		SetWindowText(GetString(IDS_ESD_TITLE2));
	else
		SetWindowText(GetString(IDS_ESD_TITLE));
	GetDlgItem(IDOK)->SetWindowText(GetString(IDS_ESD_YES));
	GetDlgItem(IDNO)->SetWindowText(GetString(IDS_ESD_NO));
	GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_ESD_CANCEL));
	GetDlgItem(IDC_TEXT)->SetWindowText(GetString(IDS_ESD_TEXT));
	CWnd *polWnd = GetDlgItem(IDC_S_Vor);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_OVERTIME_BEFORE));
	}

	polWnd = GetDlgItem(IDC_S_Nach);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_OVERTIME_AFTER));
	}

	polWnd = GetDlgItem(IDC_S_Komp);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_OVERTIME_COMPLETE));
	}

	ogBasicData.SetWindowStat("GENERAL SUB1SUB2", GetDlgItem(IDC_SUB1SUB2));

	if((polWnd = GetDlgItem(IDC_SUB1SUB2TITLE)) != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_SUB1SUB2));
		ogBasicData.SetWindowStat("GENERAL SUB1SUB2", polWnd);
	}
	SendDlgItemMessage(IDC_SUB1SUB2,CB_ADDSTRING,0,(LPARAM)(LPCTSTR) GetString(IDS_EMPDLG_SUB1));
	SendDlgItemMessage(IDC_SUB1SUB2,CB_ADDSTRING,0,(LPARAM)(LPCTSTR) GetString(IDS_EMPDLG_SUB2));

	SHIFTDATA *prlShift = NULL, olOldShift;
	bool blInitWithSub2 = false;
	int ilNumJobs = omJobs.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		JOBDATA *prlJob = &omJobs[ilJ];
		if((prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) != NULL)
		{
			if(!strcmp(prlShift->Drs2, "1"))
			{
				blInitWithSub2 = false;
				break;
			}
			else
			{
				blInitWithSub2 = true;
			}
		}
	}
	
	if(blInitWithSub2)
		m_Sub1Sub2 = GetString(IDS_EMPDLG_SUB2);
	else
		m_Sub1Sub2 = GetString(IDS_EMPDLG_SUB1);

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CExtendShiftDlg::OnOK() 
{
	if(UpdateData())
	{
		SHIFTDATA *prlShift = NULL, olOldShift;
		int ilNumJobs = omJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlJob = &omJobs[ilJ];
			if((prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) != NULL)
			{
				if(prlShift->Avta < omNewShiftEndTime || bmRemoveShiftExtension)
				{
					olOldShift = *prlShift;
					prlShift->Avta = omNewShiftEndTime;
					strcpy(prlShift->Drs2, "2");
					strcpy(prlShift->Drs3, m_After);
					if(m_Sub1Sub2 == GetString(IDS_EMPDLG_SUB2))
						strcpy(prlShift->Drs2, "2");
					else
						strcpy(prlShift->Drs2, "1");

					if (ogShiftData.UpdateShift(prlShift,&olOldShift) == RCFailure)
					{
						// "Error on update staff data"
						MessageBox(GetString(IDS_STRING61497) + ogShiftData.LastError());

						// error detected, copy saved old data back
						prlShift->Avta = olOldShift.Avta;
						strcpy(prlShift->Drs2,olOldShift.Drs2);
						strcpy(prlShift->Drs3,olOldShift.Drs3);
					}
					ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
				}
			}
		}

		CDialog::OnOK();
	}
}

void CExtendShiftDlg::AddJob(JOBDATA *prpJob)
{
	omJobs.Add(prpJob);
}


bool CExtendShiftDlg::JobsOutsideTheShift(void)
{
	bool blJobsOutsideShift = false;

	int ilNumJobs = omJobs.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		JOBDATA *prlJob = &omJobs[ilJ];
		JOBDATA *prlPJ = ogJobData.GetJobByUrno(prlJob->Jour);
		if(prlPJ != NULL && prlJob->Acto > prlPJ->Acto)
		{
			blJobsOutsideShift = true;
			break;
		}
	}

	return blJobsOutsideShift;
}

bool CExtendShiftDlg::SetData(CCSPtrArray <JOBDATA> &ropJobs)
{
	bmRemoveShiftExtension = false;
	int ilNumJobs = ropJobs.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		AddJob(&ropJobs[ilJ]);

	bool blRc = JobsOutsideTheShift() && ogBasicData.imExtendShiftOffset >= 0;
	bool blInactiveShiftFound = false, blActiveShiftFound = false;

	if(blRc)
	{
		CTimeSpan olOffset(0,0,ogBasicData.imExtendShiftOffset,0);
		SHIFTDATA *prlShift = NULL;
		JOBDATA *prlPoolJob = NULL, *prlJob = NULL;
		CString olEmpText;
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			prlJob = &omJobs[ilJ];

			JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(prlJob->Jour);
			if((prlPoolJob = ogJobData.GetJobByUrno(prlJob->Jour)) != NULL && 
				(prlShift = ogShiftData.GetShiftByUrno(prlPoolJob->Shur)) != NULL)
			{
				if(strcmp(prlShift->Ross,"A"))
				{
					blInactiveShiftFound = true;
				}
				else
				{
					blActiveShiftFound = true;

					if(prlPoolJob->Acto < prlJob->Acto)
					{
						if(prlJob->Acto > prlPoolJob->Acto)
						{
							olEmpText.Format("%s     %s - %s\r\n", ogEmpData.GetEmpName(prlJob->Ustf), prlPoolJob->Acfr.Format("%H:%M/%d"), prlPoolJob->Acto.Format("%H:%M/%d"));
							m_Employees += olEmpText;
						}
						if(omNewShiftEndTime == TIMENULL || omNewShiftEndTime < prlJob->Acto)
						{
							omNewShiftEndTime = prlJob->Acto;
						}
						if(omMinShiftEndTime == TIMENULL || prlPoolJob->Acto > omMinShiftEndTime)
						{
							omMinShiftEndTime = prlPoolJob->Acto;
						}
					}
					if(ilJ == 0)
					{
						m_Before = prlShift->Drs1;
						m_Complete = prlShift->Drs4;
					}
					else
					{
						if(m_Before != prlShift->Drs1)
							m_Before = "";
						if(m_Complete != prlShift->Drs4)
							m_Complete = "";
					}
				}
			}
		}
		m_Text = GetString(IDS_ESD_TEXT);
		if(blInactiveShiftFound)
		{
			m_Text += GetString(IDS_ESD_INACTIVESHIFT1);
		}
		omNewShiftEndTime += olOffset; // offset defined in ceda.ini EXTENDSHIFTOFFSET
		m_Actod = omNewShiftEndTime;
		m_Actot = omNewShiftEndTime;
		//m_Sub1Sub2 = GetString(IDS_EMPDLG_SUB2);
		m_After = "1";
	}


	if(blRc && (!blActiveShiftFound || !bgOnline))
	{
		MessageBox(GetString(IDS_ESD_INACTIVESHIFT2), GetString(IDS_ESD_TITLE), MB_OK);
		blRc = false;
	}

	return blRc;
}

bool CExtendShiftDlg::SetDataRemoveShiftExtension(JOBDATA *prpPoolJob)
{
	bool blRc = true;
	if(prpPoolJob == NULL)
	{
		blRc = false;
	}
	else
	{
		bmRemoveShiftExtension = true;
		AddJob(prpPoolJob);
		bool blInactiveShiftFound = false, blActiveShiftFound = false;
		CTimeSpan olOffset(0,0,ogBasicData.imExtendShiftOffset,0);
		SHIFTDATA *prlShift = NULL;
		CString olEmpText;
		if((prlShift = ogShiftData.GetShiftByUrno(prpPoolJob->Shur)) != NULL)
		{
			if(strcmp(prlShift->Ross,"A"))
			{
				blActiveShiftFound = false;
			}
			else
			{
				blActiveShiftFound = true;
				olEmpText.Format("%s     %s - %s\r\n", ogEmpData.GetEmpName(prpPoolJob->Ustf), prpPoolJob->Acfr.Format("%H:%M/%d"), prpPoolJob->Acto.Format("%H:%M/%d"));
				m_Employees += olEmpText;
				if(omNewShiftEndTime == TIMENULL || omNewShiftEndTime < prpPoolJob->Plto)
				{
					omNewShiftEndTime = prpPoolJob->Plto;
				}
				if(omMinShiftEndTime == TIMENULL || prpPoolJob->Plto > omMinShiftEndTime)
				{
					omMinShiftEndTime = prpPoolJob->Plto;
				}
				m_Before = prlShift->Drs1;
				m_Complete = prlShift->Drs4;
			}
		}
		m_Text = GetString(IDS_ESD_REMOVEEXTENSIONTEXT);
		omNewShiftEndTime; // offset defined in ceda.ini EXTENDSHIFTOFFSET
		m_Actod = omNewShiftEndTime;
		m_Actot = omNewShiftEndTime;
		//m_Sub1Sub2 = GetString(IDS_EMPDLG_SUB2);
		m_After.Empty();


		if((!blActiveShiftFound || !bgOnline))
		{
			MessageBox(GetString(IDS_ESD_INACTIVESHIFT2), GetString(IDS_ESD_TITLE2), MB_OK);
			blRc = false;
		}
	}

	return blRc;
}

void CExtendShiftDlg::OnNoButtonClicked() 
{
	EndDialog(IDOK);
}

void CExtendShiftDlg::CheckOvertimeValue(UINT ipId)
{
	CWnd *polWnd = GetDlgItem(ipId); 
	if(polWnd != NULL)
	{
		CString olValue;
		polWnd->GetWindowText(olValue);
		if(!olValue.IsEmpty() && olValue.FindOneOf(" 12") < 0)
		{
			polWnd->SetWindowText("");
		}
	}
}

void CExtendShiftDlg::OnChangeEZcode() 
{
	CheckOvertimeValue(IDC_E_ZCODE2);
}
