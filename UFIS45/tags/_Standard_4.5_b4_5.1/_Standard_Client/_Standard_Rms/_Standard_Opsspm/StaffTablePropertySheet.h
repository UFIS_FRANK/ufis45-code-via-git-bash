// StaffTablePropertySheet.h : header file
//

#ifndef _STAFFLPS_H_
#define _STAFFLPS_H_

#include <StaffTableFilterPage.h>
/////////////////////////////////////////////////////////////////////////////
// StaffLPS

class StaffTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	StaffTablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pageRank;	
	FilterPage m_pagePermits;	
	FilterPage m_pageShiftCode;	
	FilterPage m_pageTeam;
	StaffTableSortPage m_pageSort;
	CStaffTableFilterPage m_pageFilter;
	BOOL bmOldHideAbsentEmps, bmOldHideStandbyEmps, bmOldHideFidEmps;
	int imOldShiftTime;
	char tmpStr[100];

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _STAFFLPS_H_
