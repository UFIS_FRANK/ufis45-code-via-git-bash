#if !defined(AFX_AUTOASSIGNDLG_H__84DA93D1_74EE_11D4_96E2_0050DAE32E8C__INCLUDED_)
#define AFX_AUTOASSIGNDLG_H__84DA93D1_74EE_11D4_96E2_0050DAE32E8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AutoAssignDlg.h : header file
//

#include <ccsglobl.h>
#include <cedademanddata.h>
#include <cedajobdata.h>
#include <cedajoddata.h>
#include <cedaflightdata.h>
#include <EquipmentViewer.h>
/////////////////////////////////////////////////////////////////////////////
// CAutoAssignDlg dialog

class CAutoAssignDlg : public CDialog
{
// Construction
public:
	
	void SetData(CUIntArray &ropDemUrnoList, CStringArray &ropResourceGroupArray,CTime opAssignFrom,CTime opAssignTo,int ipDemandType = PERSONNELDEMANDS);

	CAutoAssignDlg(CTime opLoadStart, CTime opLoadEnd, CString opChartType, CStringArray &ropReduction,CWnd* pParent = NULL);   // standard constructor

	bool IsDemPassFilter(DEMANDDATA *prpDem);
	void DisableFlightFilter();
	void ReadDefaultsFromCedaIni();

	CMapStringToPtr omPossiblePools;
	CMapStringToPtr omSelectedPools;
	CMapStringToPtr omPossibleEquipment;
	CMapStringToPtr omSelectedEquipment;


// Dialog Data
	//{{AFX_DATA(CAutoAssignDlg)
	enum { IDD = IDD_AUTOASSIGN };
	CButton	m_PersonnelCheckboxCtrl;
	CButton	m_EquipmentCheckboxCtrl;
	CTabCtrl	m_EmpEquTabCtrl;
	CComboBox	m_ReductionBox;
	CListBox	m_PossibleList;
	CListBox	m_SelectedList;
	BOOL	m_AssignWOAloc;
	BOOL	m_BreakPrio;
	CString	m_Reduction;
	CString	m_ShiftStartBuff;
	CString	m_ShiftEndBuff;
	CString	m_ToTime;
	CString	m_ToDay;
	CString	m_JobBuff;
	CString	m_FromDay;
	CString	m_FromTime;
	CString	m_FlightOrig;
	CString	m_FlightRegn;
	CString	m_FlightNumber;
	CString	m_FlightDest;
	CString	m_FlightAirline;
	CString	m_FlightAircraft;
	CString	m_FlightSuffix;
	BOOL	m_LinkedDemands;
	int		m_DemandType;
	BOOL	m_AssignEquipment;
	BOOL	m_AssignPersonnel;
	UINT	m_Deviation1;
	UINT	m_Deviation2;
	CString	omBreakBuffer;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutoAssignDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAutoAssignDlg)
	virtual void OnCancel();
	afx_msg void OnDelonly();
	afx_msg void OnAll();
	afx_msg void OnOpenonly();
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnPersonnelRadio();
	afx_msg void OnEquipmentRadio();
	afx_msg void OnSelChangeEmpEquTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEquipmentCheckbox();
	afx_msg void OnPersonnelCheckbox();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString omChartType;
	CString omAssignType;
	CTime omLoadEnd;
	CTime omLoadStart;
	CTime omAssignStart;
	CTime omAssignEnd;
	bool bmDisableFlightFilter;

	void SetEquipment(void);
	bool bmDisplayPersonnel, bmDisplayEquipment, bmPersonnelTab;
	void SetDemandTypeFields(void);

	bool bmSetWarning;
//	CStringArray omPossibleItems, omPossibleEquItems;
//	CStringArray omSelectedItems;
	CUIntArray omDemUrnoList;

	bool IsFlightPassFilter(FLIGHTDATA * prpFlight);
	bool FlightSpecified();

	EquipmentDiagramViewer *pomEquipmentViewer;
	bool bmUseEquTypes;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTOASSIGNDLG_H__84DA93D1_74EE_11D4_96E2_0050DAE32E8C__INCLUDED_)
