// Class for Wroes
#ifndef _CEDAWRODATA_H_
#define _CEDAWRODATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct WroDataStruct
{


	char Brca[8];			//   5        C   Kapazitat Warteraum
	CTime Nafr;             //14       C   Nicht verfugbar von
	CTime Nato;             //14       C   Nicht verfugbar bis
	char Resn[42];          //   40       VC2 Grund fur die Sperrung
	char Shgn[4];           //  1        C
	char Tele[12];          //   10       C   Telefonnummer
	char Term[4];           //  1        C   Terminal
	long Urno;             // 10       N   Eindeutige Datensatz-Nr.
	CTime Vafr;            // 14       C   Gultig von
	CTime Vato;            // 14       C   Gultig bis
	char Wnam[8];          //   5        C   Warteraum Name

	// non DB data
	enum State
	{
		NEW,
		CHANGED,
		DELETED,
		UNCHANGED
	};

	State emState;	

	WroDataStruct(void)
	{
		Urno	= 0L;
		memset(Brca,0,sizeof(Brca));
		memset(Resn,0,sizeof(Resn));
		memset(Shgn,0,sizeof(Shgn));
		memset(Tele,0,sizeof(Tele));
		memset(Term,0,sizeof(Term));
		memset(Wnam,0,sizeof(Wnam));
		Nafr	= TIMENULL;
		Nato	= TIMENULL;
		Vafr	= TIMENULL;
		Vato	= TIMENULL;
		emState = NEW; 
		
	}

};

typedef struct WroDataStruct WRODATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaWroData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <WRODATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omWnamMap;
	CString GetTableName(void);
// Operations
public:
	CedaWroData();
	~CedaWroData();

	void ProcessWroBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	WRODATA* GetWroByUrno(long lpUrno);
	long CedaWroData::GetWroUrnoByWnam(const char *pcpName);
	void PrepareData(WRODATA *prpWro);
	WRODATA* CedaWroData::GetWroByWnam(const char *pcpName);
	void CedaWroData::UpdateMaps(WRODATA *prpWro, WRODATA *prpOldWro);
	bool DbInsert(WRODATA *prpWro);
	bool DbUpdate(WRODATA *prpWro);
	bool DbDelete(WRODATA *prpWro);
	CCSReturnCode CedaWroData::ReadWroData();
	CCSReturnCode ReadWroData(CTime,CTime);
	void AddWroRecord(WRODATA *prpData,BOOL bpSendDDX = TRUE);
	BOOL DeleteWroRecord(long lpWro,BOOL IsFromBC = FALSE);
	void CedaWroData::ChangeWroRecord(WRODATA *prpData,BOOL bpSendDDX);

	CString Dump(long lpUrno);

private:
	void AddWroInternal(WRODATA *prpWro);
	void DeleteWroInternal(long lpUrno);
	BOOL SaveWroRecord(WRODATA *prpWro,WRODATA *prpOldWro = NULL);
	void ClearAll();
	void ConvertDatesToUtc(WRODATA *prpWro);
	void ConvertDatesToLocal(WRODATA *prpWro);
};

extern CedaWroData ogWroData;

#endif _CEDAWRODATA_H_
