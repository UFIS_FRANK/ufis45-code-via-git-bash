// CedaDraData.cpp - Class for DRATAB - temporary unpaid absences
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDraData.h>
#include <BasicData.h>

extern CCSDdx ogCCSDdx;
void  ProcessDraCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDraData::CedaDraData()
{                  
    BEGIN_CEDARECINFO(DRADATA, DRADATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Sdac,"SDAC")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DRADATARecInfo)/sizeof(DRADATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DRADATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DRATAB");
    pcmFieldList = "URNO,SDAC";

	ogCCSDdx.Register((void *)this,BC_DRA_CHANGE,CString("DRADATA"), CString("Dra changed"),ProcessDraCf);
	ogCCSDdx.Register((void *)this,BC_DRA_DELETE,CString("DRADATA"), CString("Dra deleted"),ProcessDraCf);
}
 
CedaDraData::~CedaDraData()
{
	TRACE("CedaDraData::~CedaDraData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void CedaDraData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

void  ProcessDraCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaDraData *)popInstance)->ProcessDraBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaDraData::ProcessDraBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDraData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDraData->Selection);
	DRADATA *prlDra = GetDraByUrno(llUrno);
	ogBasicData.LogBroadcast(prlDraData);

	if(ipDDXType == BC_DRA_CHANGE && prlDra != NULL)
	{
		// update
		//ogBasicData.Trace("BC_DRA_CHANGE - UPDATE DRATAB URNO %ld\n",prlDra->Urno);
		GetRecordFromItemList(prlDra,prlDraData->Fields,prlDraData->Data);
		ogCCSDdx.DataChanged((void *)this,DRA_CHANGE,(void *)prlDra);
	}
	else if(ipDDXType == BC_DRA_CHANGE && prlDra == NULL)
	{
		// insert
		DRADATA rlDra;
		GetRecordFromItemList(&rlDra,prlDraData->Fields,prlDraData->Data);
		//ogBasicData.Trace("BC_DRA_CHANGE - INSERT DRATAB URNO %ld\n",rlDra.Urno);
		prlDra = AddDraInternal(rlDra);
		ogCCSDdx.DataChanged((void *)this,DRA_CHANGE,(void *)prlDra);
	}
	else if(ipDDXType == BC_DRA_DELETE && prlDra != NULL)
	{
		// delete
		long llDraUrno = prlDra->Urno;
		//ogBasicData.Trace("BC_DRA_DELETE - DELETE DRATAB URNO %ld\n",llDraUrno);
		DeleteDraInternal(llDraUrno);
		ogCCSDdx.DataChanged((void *)this,DRA_DELETE,(void *)llDraUrno);
	}
}

CCSReturnCode CedaDraData::ReadDraData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    sprintf(pclWhere,"WHERE SDAY BETWEEN '%s' AND '%s'", ogBasicData.omShiftFirstSDAY.Format("%Y%m%d"), 
														 ogBasicData.omShiftLastSDAY.Format("%Y%m%d"));
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDraData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		DRADATA rlDraData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDraData)) == RCSuccess)
			{
				AddDraInternal(rlDraData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDraData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DRADATA), pclWhere);
    return ilRc;
}

CCSReturnCode CedaDraData::DeleteDraRecord(long lpUrno)
{
	CCSReturnCode olRc = RCSuccess;

	char pclFieldList[500], pclData[1000];
	char pclCommand[10] = "DRT";
	char pclSelection[100];
	sprintf(pclSelection," WHERE URNO = '%ld%'",lpUrno);
	if((olRc = CedaAction(pclCommand, pcmTableName, pclFieldList, pclSelection, "", pclData)) == RCSuccess)
	{
		DeleteDraInternal(lpUrno);
		ogCCSDdx.DataChanged((void *)this,DRA_DELETE,(void *)lpUrno);
	}

	return olRc;
}

DRADATA *CedaDraData::AddDraInternal(DRADATA &rrpDra)
{
	DRADATA *prlDra = new DRADATA;
	*prlDra = rrpDra;
	PrepareData(prlDra);
	omData.Add(prlDra);
	omUrnoMap.SetAt((void *)prlDra->Urno,prlDra);
	return prlDra;
}

void CedaDraData::DeleteDraInternal(long lpUrno)
{
	int ilNumDras = omData.GetSize();
	for(int ilDel = (ilNumDras-1); ilDel >= 0; ilDel--)
	{
		DRADATA *prlDra = &omData[ilDel];
		if(prlDra->Urno == lpUrno)
		{
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

void CedaDraData::PrepareData(DRADATA *prpDra)
{
}

DRADATA* CedaDraData::GetDraByUrno(long lpUrno)
{
	DRADATA *prlDra = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlDra);
	return prlDra;
}

CString CedaDraData::GetTableName(void)
{
	return CString(pcmTableName);
}