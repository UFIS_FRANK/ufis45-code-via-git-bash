#ifndef _DATASET_H_
#define _DATASET_H_

// dataset.cpp - Class for handle each user action
//
// Description:
// This module is separated from some parts of viewers in many diagrams and
// expected to contain the routines for removing dependencies among Ceda????Data
// classes.
//
// The concept is simple. If we want a new kind of job creation or any action
// that the user can perform on the system, we create a new method here, in this
// module. Then, if that action has some more consequents actions (assign a job
// to solve a demand will change the color of the flight, for example) we will
// provide that steps here, in this module too.
//
// Some actions may require the user to make an interactive decision. To aid
// such concept, the first parameter of every functions will be "popParentWindow",
// and every function here will return IDOK or IDYES on success, IDCANCEL or IDNO
// if the user cancel this operation. The difference between IDCANCEL and IDNO is
// the user may select IDCANCEL when he/she still want to resume some data entry
// in the previous dialogs if exist. But IDNO selection will totally cancel this
// operation, including the in-process dialogs if exist.
//
// Function in this module:
// (still incomplete and need keep on working).
//
//
// Written by:
// Damkerng Thammathakerngkit   July 6, 1996
//

#include <ccsglobl.h>
#include <CedaDemandData.h>
#include <CedaFlightData.h>
#include <CedaAzaData.h>

#include <UndoManager.h>
#include <UndoClasses.h>

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

/////////////////////////////////////////////////////////////////////////////
// Id: 18 Sep - add a global instance of UndoManager for the whole project
extern UndoManager ogUndoManager;

/////////////////////////////////////////////////////////////////////////////
// DataSet class


// following struct required when creating personnel jobs for corresponding equipment jobs
typedef struct
{
	CTime	Begin;			// max(FastlinkBegin,JobBegin)
	CTime	End;			// min(FastLinkEnd,JobEnd)
	long	PoolJobUrno;
} JOBTIME;


class DataSet: public CCSObject
{
// Job creation routine
public:
	static int CreateJobPool(CWnd *popParentWindow,
		CDWordArray &ropShiftUrnos, const char *pcpPool, const char *pcpAlid = "",
		const char *pcpAloc = "", CTime lpAssignedFrom = TIMENULL, CTime lpAssignedTo = TIMENULL);

	static int CreateGroupDelegation(CWnd *popParentWindow, const char *pcpGroupName,
		const char *pcpPoolName, CDWordArray &ropPoolJobUrnos, CStringArray &ropFunctionCodes,
		CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateJobDetachFromDuty(CWnd *popParentWindow,
		CDWordArray &ropPoolJobUrnos, const char *pcpPool,
		CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateEquipmentJob(CWnd *popParentWindow, long lpEquUrno, long lpDemUrno, 
								  long lpFlightUrno = 0L, long lpRotationUrno = 0L, 
								  CString opAloc ="", CString opAlid ="", bool bpCheckFastLink = true);

	static int CreateEquipmentJobs(CWnd *popParentWindow, const CDWordArray& ropEquUrnos, const long lpFlightUrno, 
		                           const long lpRotationUrno = 0L, const CString& ropAloc ="",const CString& ropAlid ="", 
								   bool bpCheckFastLink = true);

	static int CreateFidEquipmentJob(CWnd *popParentWindow, long lpEquUrno, long lpDemUrno);

	static void HandleFastLink(CWnd *popParentWindow, JOBDATA *prpNewJob);
	static void HandleFastLinkForPersonnel(CWnd *popParentWindow, JOBDATA *prpNewJob);
	static int GetBestEquipmentForDemand(long lpUstf, DEMANDDATA *prpDemand, long &rlpUequ);
	static void HandleFastLinkForEquipment(CWnd *popParentWindow, JOBDATA *prpNewJob);
	static int GetBestEmpForDemand(long lpEquUrno, DEMANDDATA *prpDemand, CCSPtrArray <JOBTIME> &ropJobTimes);

	static int CreateJobFlightFromDuty(CWnd *popParentWindow, CDWordArray &ropDutyUrnos,
				long lpFlightUrno, CString opAloc, CString opAlid, bool bpTeam = false,
				CCSPtrArray <DEMANDDATA> *prpSingleDemands = NULL, CTime opBegin = TIMENULL, CTime opEnd = TIMENULL,
				bool bpCreateWhenFlightHasNoDemands = true, long lpRotationUrno = 0L, 
				bool bpCheckFastLink = true, bool bpAssignWithoutDemands = false, long lpUtpl = 0L, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);

	static int CreateDelegatedFlightJob(CWnd *popParentWindow, CDWordArray &ropDutyUrnos,
				long lpFlightUrno, CString opAloc, CString opAlid, bool bpTeam = false,
				CCSPtrArray <DEMANDDATA> *prpSingleDemands = NULL, CTime opBegin = TIMENULL, CTime opEnd = TIMENULL,
				bool bpCreateWhenFlightHasNoDemands = true, long lpRotationUrno = 0L, 
				bool bpCheckFastLink = true, bool bpAssignWithoutDemands = false, long lpUtpl = 0L, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);

	static int CreateMultiJobsForSingleEmp(CWnd *popParentWindow, long lpPoolJobUrno, CCSPtrArray <DEMANDDATA> &ropDemands, bool bpCheckFastLink = true);

	static void GetTimesForJobWithoutDemand(long lpFlightUrno, CString opAlid, CString opAloc, CTime &ropStart, CTime &ropEnd);

	static int CopyAssignment(CWnd *popParentWindow, JOBDATA *prpJob, CTime opStart, CTime opEnd, JODDATA *prpJod = NULL);

	/*	static int CreateJobRegnFromDuty(CWnd *popParentWindow, CDWordArray &ropDutyUrnos,
		long lpFlightUrno, CString opAloc, BOOL bpUseTurnaround = TRUE); */

	static int CreateJobRegnFromDuty(CWnd *popParentWindow, CDWordArray &ropPoolJobUrnos, 
								 FLIGHTDATA *prpFlight, const char *pcpRegn = NULL, DEMANDDATA *prpDemand = NULL,
								 CTime opFrom = TIMENULL, CTime opTo = TIMENULL);

	static int CreateThirdPartyDemand(FLIGHTDATA *prpFlight, AZADATA *prpAza);

	static int CreateFlightIndependentJobs(CWnd *popParentWindow, CDWordArray &ropPoolJobUrnos, long lpDemandUrno, CTime opStart = TIMENULL, CTime opEnd = TIMENULL);

	static int CreateJobFlightFromShift(CWnd *popParentWindow,
		CDWordArray &ropShiftUrnos, long lpFlightUrno);

	static int CreateJobFlightForGateFromDuty(CWnd *popParentWindow,
		CDWordArray &ropDutyUrnos, const char *pcpGate,
		CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateJobFlightForGateFromShift(CWnd *popParentWindow,
		CDWordArray &ropShiftUrnos, const char *pcpGate,
		CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateJobFlightSpecialFromShift(CWnd *popParentWindow,
		CDWordArray &ropShiftUrnos, long lpFlightUrno);

	static int CreateJobGateAreaFromDuty(CWnd *popParentWindow,
		CDWordArray &ropDutyUrnos, const char *pcpGateArea,
		CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateNormalJob(CWnd *popParentWindow, CString opJobType, long lpPoolJobUrno, 
							  CString opAloc, CString opAlid, CTime opFrom, CTime opTo);
	int CreatePrmJob(CWnd *popParentWindow, long lpPoolJobUrno, CString opAlid, long lpUtpl, long lpUprm, long lpUghs, CTime opFrom, CTime opTo);

	static int CreateNormalJobs(CWnd *popParentWindow, CString opJobType, CDWordArray &ropPoolJobUrnos, CString opAloc, 
								CString opAlid, CTime opStartTime, CTime opEndTime, bool bpTeam = false );

	static int CreateJobCciFromDuty(CWnd *popParentWindow,
		CDWordArray &ropDutyUrnos, const char *pcpCciDesk,
		CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateJobSpecialFromDuty(CWnd *popParentWindow,
		CDWordArray &ropDutyUrnos, const char *pcpDescription,
		CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateEquipmentFastLink(CWnd *popParentWindow, CDWordArray &ropShiftUrnos, long lpEquipmentUrno, CTime lpAssignedFrom, CTime lpAssignedTo);

	static int CreateCommonCheckinCounterJobs(CWnd *popParentWindow, CDWordArray &ropPoolJobUrnos, long lpDemandUrno);

	static int GetFlightsByAlid(CString opAloc, CString opAlid, CCSPtrArray <FLIGHTDATA> &ropFlights, bool bpReset = true);

	static int CreateJobBreak(CWnd *popParentWindow,long lpDutyJobUrno, CTime lpAssignedFrom, CTime lpAssignedTo, BOOL bpIsCoffeeBreak = FALSE);
	static int CreateJobBreak(CWnd *popParentWindow, CCSPtrArray <JOBDATA> &ropPoolJobs, CTime lpAssignedFrom, CTime lpAssignedTo, BOOL bpIsCoffeeBreak = FALSE);


	static int CreateSpecialFlightManagerJob(CWnd *popParentWindow,
		long lpDutyJobUrno, long lpJobUrno);


	static int ReassignJobToNewDemand(CWnd *popParentWindow, long lpJobUrno, long lpNewDemandUrno);

//	static UINT CheckFunctionsAndPermits(DEMANDDATA *prpDemand, long lpPoolJobUrno, DEMANDDATA **pppSelectedDemand);
//	static UINT CheckFunctionsAndPermits(CCSPtrArray <DEMANDDATA> &ropDemands, long lpPoolJobUrno, DEMANDDATA **pppSelectedDemand);

// Job update routines
	static int ReassignFlightForGroup(CWnd *popParentWindow, CCSPtrArray <JOBDATA> &ropCurrentJobs, CCSPtrArray <JOBDATA> &ropNewPoolJobs);
	static int ReassignJob(CWnd *popParentWindow, long lpJobUrno, long lpNewDutyUrno);
	static int ReassignEquipmentJob(CWnd *popParentWindow, long lpJobUrno, long lpNewEquUrno);
	static int ChangeTimeOfPoolJob(long lpUrno, CTime opStart, CTime opEnd);
	static BOOL ConfirmJob(CWnd *popParentWindow,JOBDATA *prpJob);
	static BOOL ConfirmJob(CWnd *popParentWindow,JOBDATA *prpJob, CTime opStart);
	static BOOL ConfirmJob(CWnd *popParentWindow,JOBDATA *prpJob, CString opStatus);
	static BOOL ConfirmJob(CWnd *popParentWindow,JOBDATA *prpJob, CTime opStart, CString opStatus);
	static BOOL ConfirmJob(CWnd *popParentWindow,CCSPtrArray <JOBDATA> &ropJobs);
	static BOOL ConfirmJob(CWnd *popParentWindow,CCSPtrArray <JOBDATA> &ropJobs, CString opStatus);
	static BOOL ConfirmJob(CWnd *popParentWindow,CCSPtrArray <JOBDATA> &ropJobs, CTime opStart);
	static BOOL ConfirmJob(CWnd *popParentWindow,CCSPtrArray <JOBDATA> &ropJobs, CTime opStart, CString opStatus);
	static BOOL SetJobStandby(CWnd *popParent,JOBDATA *prpJob);
	static BOOL SetCoffeeBreak(CWnd *popParentWindow,JOBDATA *prpJob);
	static void FinishJob(CCSPtrArray <JOBDATA> &ropJobs, CTime opEnd = TIMENULL);
	static void FinishJob(JOBDATA *prpJob, CTime opEnd = TIMENULL);

	static int ChangeTimeOfJob(long lpJobUrno,CTime opStart, CTime opEnd,BOOL bpPoolJob);
	static int ChangeTimeOfJob(JOBDATA *prpNewJob, JOBDATA *prpOldJob);
	static bool ChangeGroupJobTimes(CTime opOldGroupBegin, CTime opOldGroupEnd, CTime opNewGroupBegin, CTime opNewGroupEnd, CCSPtrArray <JOBDATA> &ropGroupJobs);

// Job deletion routines
	static int DeleteJob(CWnd *popParentWindow, long lpJobUrno);
	static int DeleteJob(CWnd *popParentWindow, JOBDATA *prpJob);
	static int DeleteJob(CWnd *popParentWindow, CCSPtrArray <JOBDATA> &ropJobs);
	static int DeleteJobPool(CWnd *popParentWindow, long lpJobUrno);
	static int DeleteJobDetach(CWnd *popParentWindow, long lpJobUrno);
	static int DeleteJobTeamDelegation(CWnd *popParentWindow, long lpJobUrno);
	static int DeleteDelegatedFlightJob(CWnd *popParentWindow, long lpJobUrno);

// Demand update routines
	static int ReassignCicDemandAlid(CWnd *popParentWnd,long lpDemandUrno,const CString& ropAlid,bool bpTreatOverlapAsError = true);
	static int ChangeDemandAutoAssignment(CWnd *popParentWindow, long lpDemandUrno,bool bpEnable);
	static int DeactivateDemand(CWnd *popParentWindow, long lpDemandUrno, bool bpDeactivate);

// Public helper routines
public:
	void EmpInformedOfJob(JOBDATA *prpJob);
	void EmpAbsent(JOBDATA *prpJob);
	static CTime GetTimeForNextJobPool(long lpShiftUrno);
	static CTime GetTimeForNextJob(long lpPoolJob);
	bool GetOldValue(long lpFurn, int ipOldValueType, char *pcpOldValue);
	bool GetOldValue(long lpFurn, int ipOldValueType, CString &ropOldValue);
	static void GetOpenDemands(CCSPtrArray <DEMANDDATA> &ropPossibleDemands, CCSPtrArray <DEMANDDATA> &ropOpenDemands, CTime opStart = TIMENULL, CTime opEnd = TIMENULL);
	static bool GetDemandsByUref(CCSPtrArray<DEMANDDATA> &ropDemands, long lpUref, CString opObty);
	static void GetOpenDemandsForFlight(FLIGHTDATA *prpFlight,FLIGHTDATA *prpRotation,CCSPtrArray <DEMANDDATA> &ropDemands,CTime opStart=TIMENULL,CTime opEnd=TIMENULL, const char *pcpAlid="", const char *pcpAloc="", int ipDemandType = PERSONNELDEMANDS, int ipReturnFlightType = ID_NOT_RETURNFLIGHT, bool bpReset=true);
	static void GetOpenDemandsForFlight(FLIGHTDATA *prpFlight,CCSPtrArray <DEMANDDATA> &ropDemands,CTime opStart=TIMENULL,CTime opEnd=TIMENULL, const char *pcpAlid="", const char *pcpAloc="", int ipDemandType = PERSONNELDEMANDS, int ipReturnFlightType = ID_NOT_RETURNFLIGHT, bool bpReset=true);
	static void GetOpenDemandsForRotation(FLIGHTDATA *prpInboundFlight, FLIGHTDATA *prpOutboundFlight, CCSPtrArray <DEMANDDATA> &ropDemands,CTime opStart=TIMENULL,CTime opEnd=TIMENULL, const char *pcpAlid="", const char *pcpAloc="", int ipDemandType = PERSONNELDEMANDS, int ipReturnFlightType = ID_NOT_RETURNFLIGHT, bool bpReset=true);
	static void GetOpenDemandsForRegn(FLIGHTDATA *prpFlight,const char *pcpRegn, CCSPtrArray <DEMANDDATA> &ropDemands,CTime opStart=TIMENULL,CTime opEnd=TIMENULL, const char *pcpAlid="", const char *pcpAloc="", bool bpReset=true);
	static void CreateOldFlightFields(JOBDATA *prpJob, FLIGHTDATA *prpFlight);
	static FLIGHTDATA *GetFlightByDemand(DEMANDDATA *prpDemand);
	void GetJobsForFlight(CString opAllocUnitType, FLIGHTDATA *prpFlight, CCSPtrArray <JOBDATA> &ropJobs, bool bpReset=true);
	void GetDemandsForFlight(CString opAllocUnitType, FLIGHTDATA *prpFlight, CCSPtrArray <DEMANDDATA> &ropDemands, int ipDemandType = ALLDEMANDS, bool bpReset = true);
	static void GetDemandsForRegn(const char * pcpRegn, CCSPtrArray<DEMANDDATA> &ropDemands);
	static void GetRegnDemandsForFlight(FLIGHTDATA* prpFlight, CCSPtrArray<DEMANDDATA> &ropDemands);
	bool GetEmployeeNameFromDemand(CStringArray &ropEmployee, DEMANDDATA* prpDemand);
	void UpdateJobsForDemand(DEMANDDATA *prpOldDemand, DEMANDDATA *prpNewDemand);
	void DeleteJobsForDemand(long lpDemandUrno);
	static CString JobBarText(JOBDATA *prpJob);
	static CString GetFlightJobBarText(JOBDATA *prpJob);
	void CreatePausesForShifts(void);
	void CreatePauseForShift(JOBDATA *prpPoolJob);
	void UpdateJobsWithFlightChanges(FLIGHTDATA *prpFlight);
	void UpdateJobsWithFlightChanges(void);
	static bool DeleteDemandIfExpired(long lpDemandUrno);
	static void GetOpenDemandsForAlid(CCSPtrArray <DEMANDDATA> &ropDemands,CTime opStart=TIMENULL,CTime opEnd=TIMENULL, const char *pcpAlid="", const char *pcpAloc="",bool bpReset=true);
	static long GetUtplByDemand(long lpUdem);
	static long GetUtplByDemand(DEMANDDATA *prpDemand);
	static bool CheckForAlreadyAssignedDemand(CWnd *popParentWindow, long lpDemandUrno);
	static bool ExtendShifts(JOBDATA *prpJob);
	static bool ExtendShifts(CCSPtrArray <JOBDATA> &ropJobs);
	static int RemoveOvertime(long lpPoolJobUrno);
	
	
// Private helper routines
private:
	static void InternalDeletePoolJob(long lpJobUrno, JobCreationInfo *popInfo);
	static void InternalDeleteDetachJob(long lpJobUrno, JobCreationInfo *popInfo);
	static void InternalDeleteNormalJob(long lpJobUrno, JobCreationInfo *popInfo);

	static long GetDemandUrnoForNextJobFlight(long lpFlightUrno, long lpDemandUrno = 0L);
	static DEMANDDATA *GetLastDemand(long lpFlightUrno, const char *pcpAlid="", const char *pcpAloc="");

	static void SetJobStartTime(JOBDATA *prpJob, CTime opStart);
	static BOOL InternalConfirmPoolJob     (long lpJobUrno,CWnd *popParentWindow,const CString& opStatus,BOOL& blAskForConfirmation,JobConfirmInfo *popInfo,CTime opStart);
	static BOOL InternalConfirmDetachedJob (long lpJobUrno,CWnd *popParentWindow,const CString& opStatus,BOOL& blAskForConfirmation,JobConfirmInfo *popInfo,CTime opStart);
	static BOOL InternalConfirmDelegatedJob(long lpJobUrno,CWnd *popParentWindow,const CString& opStatus,BOOL& blAskForConfirmation,JobConfirmInfo *popInfo,CTime opStart);
	static BOOL InternalConfirmNormalJob   (long lpJobUrno,CWnd *popParentWindow,const CString& opStatus,BOOL& blAskForConfirmation,JobConfirmInfo *popInfo,CTime opStart);
public:
	static bool CheckPoolJobForClockOut(CWnd *popParentWindow, JOBDATA *prpPoolJob);
	static bool CheckPoolJobsForClockOut(CWnd *popParentWindow, CDWordArray &ropPoolJobUrnos);
	static bool CheckPoolJobsForClockOut(CWnd *popParentWindow, CCSPtrArray <JOBDATA> &ropPoolJobs);
	static bool CheckShiftsForClockOut(CWnd *popParentWindow, CDWordArray &ropShiftUrnos);
	static bool CheckJobForClockOut(CWnd *popParentWindow, long lpJobUrno);
	static bool CheckJobsForClockOut(CWnd *popParentWindow, CDWordArray &ropJobUrnos);
	static bool CheckJobForClockOut(CWnd *popParentWindow, JOBDATA *prpJob);
	static bool CheckJobsForClockOut(CWnd *popParentWindow, CCSPtrArray <JOBDATA> &ropJobs);
};

extern DataSet ogDataSet;

#endif	// _DATASET_H_
