// CCITable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <AllocData.h>

#include <OpssPm.h>
#include <BasicData.h>
#include <table.h>
#include <CciTableViewer.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <CCITable.h>

#include <CciTableViewer.h>
#include <CciTablePropertySheet.h>

#include <DataSet.h>
#include <dgatejob.h>
#include <ccsddx.h>
#include <CedaCfgData.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <GateTable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <RegnDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <CciDetailWindow.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CCITable dialog

CCITable::CCITable(CWnd* pParent /*=NULL*/)
	: CDialog(CCITable::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCITable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = new CTable;
    pomTable->tempFlag = 2;

    BOOL blMinimized = FALSE; //PRF 8712
    CDialog::Create(CCITable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.CCTB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::CHECKIN_TABLE_WINDOWPOS_REG_KEY,blMinimized);	
	
	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	bmIsViewOpen = FALSE;

	omViewer.Attach(pomTable);
	UpdateView();
	
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

CCITable::~CCITable()
{
	TRACE("CCITable::~CCITable()\n");
	omViewer.Attach(NULL);
	delete pomTable;
}


void CCITable::SetCaptionText(void)
{
	CString olCaption;
	olCaption.Format(GetString(IDS_STRING61296),omViewer.StartTime().Format("%d/%H%M"),omViewer.EndTime().Format("%H%M "));
	SetWindowText(olCaption);
}


void CCITable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCITable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	//}}AFX_DATA_MAP
}

void CCITable::UpdateView()
{
	CString  olViewName = omViewer.GetViewName();
	
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.CCBV;
	}

	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);
	SetCaptionText();

	//Singapore
	OnTableUpdateDataCount();
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();
}

void CCITable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.CCBV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}


CString CCITable::Format(CCIDATA_LINE *prpLine)
{
    CString s;
	
	s =  prpLine->Alid + "|" + prpLine->GroupName + "|" + prpLine->DeskType;
	
	int nLineCount = prpLine->JobData.GetSize();

    for (int i = 0; i < nLineCount; i++)
    {
        CCIDATA_JOB *pJobData = &prpLine->JobData[i];
        s += "|" + pJobData->Name + "|";
		s += pJobData->Acfr.Format("%H%M") + "-";
		s += pJobData->Acto.Format("%H%M");
    }

    return s;
}

BEGIN_MESSAGE_MAP(CCITable, CDialog)
	//{{AFX_MSG_MAP(CCITable)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_CBN_SELCHANGE(IDC_VIEWCOMBO, OnSelchangeViewcombo)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCITable message handlers

int CCITable::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    m_DragDropTarget.RegisterTarget(this, this);
	
	// Register DDX call back function
	TRACE("CCITable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("CCITABLE"),	CString("Redisplay all"), CCITableCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("CCITABLE"),CString("Global Date Change"), CCITableCf);

	return 0;
}

void CCITable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;	
	ogBasicData.WriteDialogToReg(this,COpssPmApp::CHECKIN_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	// Unregister DDX call back function
	TRACE("CciTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

BOOL CCITable::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING32899)); 

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	// TODO: Add extra initialization here
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

   	UpdateComboBox();

	// extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect); //Commented - Singapore PRF 8712

	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCITable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	AfxGetMainWnd()->SendMessage(WM_CCITABLE_EXIT);
   	pogButtonList->m_wndCCITable = NULL;
	pogButtonList->m_CCITableButton.Recess(FALSE);
	DestroyWindow();
}

void CCITable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if (nType != SIZE_MINIMIZED)
	{
		pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
		GetWindowRect(&omWindowRect); //PRF 8712
	}
}

void CCITable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();

	CCITablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
	{
		UpdateView();
	}
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void CCITable::OnSelchangeViewcombo() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	omViewer.SelectView(clText);
	UpdateView();		
}

void CCITable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    //pomTable->GetCTableListBox()->SetFocus();
}

void CCITable::OnPrint() 
{
	omViewer.PrintView();
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();
}

BOOL CCITable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	m_DragDropTarget.Revoke();
	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// CCITable -- implementation of DDX call back function

void CCITable::CCITableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	CCITable *polTable = (CCITable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
		polTable->SetCaptionText();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

//*****
// Id 28-Sep-96
// Dragging from the CCI table is still missing. It was not included in our final
// drag-and-drop list (may be unimportant). But it looks like that we should do
// this some days later. So I leave the comment about dragging below.
//
/////////////////////////////////////////////////////////////////////////////
// CciTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a normal JobCciDesk.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single staff shift) onto any non-empty lines.
//		We will ask the user for the period of time and create a JobCciDesk for
//		this employee. (use the current time as the default).
//	-- drop from DutyBar onto any non-empty lines.
//		We will ask the user for the period of time and create a JobCciDesk for
//		this employee. (use the current time as the default).
//
LONG CCITable::OnDragOver(UINT wParam, LONG lParam)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_DUTYBAR)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	// note : the usage of imDropLineno	between drag over and drop down is safe, 
	// because the viewer will not change his internal list due to a broadcast message
	imDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= imDropLineno && imDropLineno <= omViewer.Lines() - 1))
		return -1L;	// cannot drop on a blank line
	
	return 0L;
}

LONG CCITable::OnDrop(UINT wParam, LONG lParam)
{
	CCIDATA_LINE *plLine = omViewer.GetLine(imDropLineno);
	if (!plLine)
		return -1L;


	bool blTeamAlloc = false; // assigning a whole team ?
	CDWordArray olPoolJobUrnos;
	CTime olBegin = TIMENULL, olEnd = TIMENULL;
	CStringArray olEmpList;

	int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
	if(ilDataCount > 0)
	{
		JOBDATA *prlJob;
		for(int ilC = 0; ilC < ilDataCount; ilC++)
		{
			if((prlJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(ilC))) != NULL)
			{
				olPoolJobUrnos.Add(prlJob->Urno);
				if(olBegin == TIMENULL || prlJob->Acfr < olBegin)
				{
					olBegin = prlJob->Acfr;
				}
				if(olEnd == TIMENULL || prlJob->Acto > olEnd)
				{
					olEnd = prlJob->Acto;
				}
				EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
				if (prlEmp != NULL) 
				{
					olEmpList.Add(ogEmpData.GetEmpName(prlEmp));
				}
			}
		}

		blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false; // assigning a whole team ?
	}

	if (olPoolJobUrnos.GetSize() > 0)
	{
		CString olEmpName = olEmpList.GetSize() > 0 ? olEmpList[0] : "";
		DGateJob olGateJobDialog(this,ALLOCUNITTYPE_CIC,plLine->Alid,olEmpName,olBegin);
		int ilDuration;
		bgModal++;
		if (olGateJobDialog.DoModal(olBegin,ilDuration) == IDOK)
		{
			olBegin = olGateJobDialog.m_Hour;
			ilDuration = olGateJobDialog.m_Duration;
			ogDataSet.CreateNormalJobs(this, JOBCCI, olPoolJobUrnos, ALLOCUNITTYPE_CIC, plLine->Alid, olBegin,olBegin+(ilDuration*60), false);
		}
		bgModal--;
	}

	return 0L;
}


void CCITable::OnClose() 
{
   	pogButtonList->m_wndCCITable = NULL;
	pogButtonList->m_CCITableButton.Recess(FALSE);
	CDialog::OnClose();
}

void CCITable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();	
}

void CCITable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	omViewer.SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	omViewer.SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void CCITable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

LONG CCITable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = omViewer.Lines();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		CCIDATA_LINE *prlLine = omViewer.GetLine(ipItem);
		if (prlLine != NULL)
		{
			BOOL blArrival   = TRUE;
			BOOL blDeparture = TRUE;
			new CciDetailWindow(this, prlLine->Alid);
		}
	}

	return 0L;
}

//Singapore
void CCITable::OnTableUpdateDataCount()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			char olDataCount[10];
			polWnd->EnableWindow();			
			polWnd->SetWindowText(itoa(omViewer.Lines(),olDataCount,10));
		}
	}
}
void CCITable::OnMove(int x, int y)
{	
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}