// tscale.h : header file
//

#ifndef _TIMESCALE_
#define _TIMESCALE_


/////////////////////////////////////////////////////////////////////////////
// CTopScaleIndicator class

class CTopScaleIndicator
{
public:
    COLORREF lmColor;
    CTime omStart, omEnd;
};

/////////////////////////////////////////////////////////////////////////////
// CTimeScale window

class CTimeScale : public CWnd
{
// Construction
public:
    CTimeScale();

// Attributes
public:

// Operations
public:
    void SetDisplayStartTime(CTime opDisplayStart);
    void SetTimeInterval(CTimeSpan opInterval);
    void SetDisplayTimeFrame(CTime opDisplayStart, CTimeSpan opDuration, CTimeSpan opInterval);
    CTimeSpan GetDisplayDuration(void);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for make the TimeScale be able to help
// the other classes which desire to calculate the time backward from the
// given point.
	CTime GetTimeFromX(int ipX);
////////////////////////////////////////////////////////////////////////
    int GetXFromTime(CTime opTime);

    void UpdateCurrentTimeLine(void);
	void UpdateCurrentTimeLine(CTime opTime);
    
    void AddTopScaleIndicator(CTopScaleIndicator *popTSI) { omTSIArray.Add(popTSI); };
    void AddTopScaleIndicator(CTime opStartTime, CTime opEndTime, COLORREF lpColor);
    void DisplayTopScaleIndicator(CDC *popDC);
    void DisplayTopScaleIndicator(void);
    void RemoveAllTopScaleIndicator(void);

	void EnableDisplayCurrentTime(BOOL bpDisplayCurrentTime);

// Implementation
public:
    virtual ~CTimeScale();

protected:
    // Generated message map functions
    //{{AFX_MSG(CTimeScale)
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    
    BOOL TSIPos(int *ipLeft, int *ipRight);

protected:
    CTime omDisplayStart;
    CTimeSpan omInterval;
    double fmIntervalWidth;
    
    int imP0;
    int imP1;
    int imP2;
    int imP3;
    
    CTime omOldCurrentTime;
	CTime omCurrentTime;
    CPtrArray omTSIArray;

	BOOL bmDisplayCurrentTime;

private:
    static CFont *omFont;
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _TIMESCALE_

