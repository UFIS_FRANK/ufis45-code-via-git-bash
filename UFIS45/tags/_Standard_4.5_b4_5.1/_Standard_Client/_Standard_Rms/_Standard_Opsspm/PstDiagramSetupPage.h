#if !defined(AFX_PSTDIAGRAMSETUPPAGE_H__1D4A6A23_C710_11D7_826B_00010215BFE5__INCLUDED_)
#define AFX_PSTDIAGRAMSETUPPAGE_H__1D4A6A23_C710_11D7_826B_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PstDiagramSetupPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PstDiagramSetupPage dialog

class PstDiagramSetupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(PstDiagramSetupPage)
// Construction
public:
	PstDiagramSetupPage();   // standard constructor

// Dialog Data
	//{{AFX_DATA(PstDiagramSetupPage)
	enum { IDD = IDD_PSTDIAGRAM_SETUP_PAGE };
	CButton	m_ConflictsCheckboxCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PstDiagramSetupPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	bool GetConflictsCheckboxState();
	void SetConflictsCheckboxState(bool bpConflictsCheckbox);

private:
	bool bmConflictsCheckbox;


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PstDiagramSetupPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSTDIAGRAMSETUPPAGE_H__1D4A6A23_C710_11D7_826B_00010215BFE5__INCLUDED_)
