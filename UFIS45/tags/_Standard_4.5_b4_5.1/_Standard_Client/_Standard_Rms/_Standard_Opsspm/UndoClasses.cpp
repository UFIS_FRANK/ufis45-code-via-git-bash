#include <stdafx.h>

#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <UndoClasses.h>
#include <CedaDlgData.h>
#include <CedaSprData.h>
#include <DataSet.h>

// this following include files are only for BasicData.h
#include <CCSCedaData.h>
#include <BasicData.h>

extern COfflineDescription ogOfflineDescription;

static void DisplayJobDiffersMsg(CWnd *popParentWnd, const CString &ropUndoText,JOBDATA *prpJob)
{
	popParentWnd->MessageBox(GetString(IDS_STRING33151)
		+ "(" + ropUndoText + ")\n" +
		GetString(IDS_STRING33152),
		NULL, MB_OK | MB_ICONEXCLAMATION);
}

static void DisplayJobDeletedMsg(CWnd *popParentWnd, const CString &ropUndoText)
{
	popParentWnd->MessageBox(GetString(IDS_STRING33151)	+ "(" + ropUndoText + ")\n" + GetString(IDS_STRING33152), NULL, MB_OK | MB_ICONEXCLAMATION);
}

static BOOL IsDifferentJob(JOBDATA *prpJob1,JOBDATA *prpJob2)
{
	if (prpJob1 && prpJob2)
	{
		if (prpJob1->Urno != prpJob2->Urno)
			return TRUE;

		if (strcmp(prpJob1->Peno,prpJob2->Peno) != 0)
			return TRUE;

//		if (prpJob1->Plfr != prpJob2->Plfr)
//			return TRUE;
//		if (prpJob1->Plto != prpJob2->Plto)
//			return TRUE;
//
//		if (prpJob1->Acfr != prpJob2->Acfr)
//			return TRUE;
//		if (prpJob1->Acto != prpJob2->Acto)
//			return TRUE;

		if (prpJob1->Jour != prpJob2->Jour)
			return TRUE;

		if (strcmp(prpJob1->Text,prpJob2->Text) != 0)
			return TRUE;
		if (strcmp(prpJob1->Alid,prpJob2->Alid) != 0)
			return TRUE;
		if (strcmp(prpJob1->Aloc,prpJob2->Aloc) != 0)
			return TRUE;

		if (ogBasicData.GetFlightUrnoForJob(prpJob1) != ogBasicData.GetFlightUrnoForJob(prpJob2))
			return TRUE;
		if (prpJob1->Shur != prpJob2->Shur)
			return TRUE;

		if (strcmp(prpJob1->Stat,prpJob2->Stat) != 0)
			return TRUE;
		if (strcmp(prpJob1->Conf,prpJob2->Conf) != 0)
			return TRUE;
		if (strcmp(prpJob1->Chng,prpJob2->Chng) != 0)
			return TRUE;
		if (strcmp(prpJob1->Ignr,prpJob2->Ignr) != 0)
			return TRUE;

		if (prpJob1->Infm != prpJob2->Infm)
			return TRUE;

		if (strcmp(prpJob1->Jtco,prpJob2->Jtco) != 0)
			return TRUE;


		if (strcmp(prpJob1->Regn,prpJob2->Regn) != 0)
			return TRUE;
		if (strcmp(prpJob1->Act3,prpJob2->Act3) != 0)
			return TRUE;
		if (strcmp(prpJob1->Posi,prpJob2->Posi) != 0)
			return TRUE;
		if (strcmp(prpJob1->Gate,prpJob2->Gate) != 0)
			return TRUE;


		if (prpJob1->Ustf != prpJob2->Ustf)
			return TRUE;
		if (prpJob1->Uaid != prpJob2->Uaid)
			return TRUE;
		if (prpJob1->Ualo != prpJob2->Ualo)
			return TRUE;
		if (prpJob1->Ujty != prpJob2->Ujty)
			return TRUE;
		if (prpJob1->Udel != prpJob2->Udel)
			return TRUE;
		if (prpJob1->Udrd != prpJob2->Udrd)
			return TRUE;

//	NOTE :  Normally we would compare Lstu only, because it marks the time the last update as written to ceda, 
//			but unfortunatly it will only be set from CedaJobData exactly before it is written - 
//			and therefore the transient copy of JOBDATA contains the previous Lstu only !
//			Furthermore, the sections below will be filled only, exactly before the new job is written to ceda
//			and therefore the transient copy of JOBDATA contains no valid entry for these values !
#if	0
		if (strcmp(prpJob1->Usec,prpJob2->Usec) != 0)
			return TRUE;
		if (prpJob1->Cdat != prpJob2->Cdat)
			return TRUE;

		if (strcmp(prpJob1->Useu,prpJob2->Useu) != 0)
			return TRUE;
		if (prpJob1->Lstu != prpJob2->Lstu)
			return TRUE;
#endif

#if	0
		if (prpJob1->ConflictConfirmed != prpJob2->ConflictConfirmed)
			return TRUE;
#endif
		return FALSE;
	}
	else if (prpJob1 || prpJob2)
	{
		return TRUE;
	}
	else
		return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// JobCreationInfo

JobCreationInfo::~JobCreationInfo()
{
	// automatic release memory
	for (int ilStepno = omCreationSteps.GetSize()-1; ilStepno >= 0; ilStepno--)
	{
		omCreationSteps[ilStepno].Jods.DeleteAll();
		omCreationSteps.DeleteAt(ilStepno);
	}
}

const JobCreationInfo &JobCreationInfo::operator =(const JobCreationInfo &ropSourceInfo)
{
	// must allocate another set of data in memory, since destructor will call Delete..()
	for (int ilStepno = 0; ilStepno < ropSourceInfo.omCreationSteps.GetSize(); ilStepno++)
	{
		omCreationSteps.NewAt(ilStepno);
		omCreationSteps[ilStepno].Job = ropSourceInfo.omCreationSteps[ilStepno].Job;
		for (int ilJodno = 0; ilJodno < ropSourceInfo.omCreationSteps[ilStepno].Jods.GetSize(); ilJodno++)
		{
			omCreationSteps[ilStepno].Jods.NewAt(ilJodno,
				ropSourceInfo.omCreationSteps[ilStepno].Jods[ilJodno]);
		}
	}

	return *this;
}

// AddAnotherJob: create another "JobCreationStep"
void JobCreationInfo::AddAnotherJob(JOBDATA *prpJob, BOOL bpAppend)
{
	int ilStepno = (bpAppend)? omCreationSteps.GetSize(): 0;
	omCreationSteps.NewAt(ilStepno);
	omCreationSteps[ilStepno].Job = *prpJob;
}

// AddAnotherJod: create another JODDATA in the last step
void JobCreationInfo::AddAnotherJod(JODDATA *prpJod, BOOL bpAppend)
{
	int ilStepno = (bpAppend)? omCreationSteps.GetSize() - 1: 0;
	int ilJodno = (bpAppend)? omCreationSteps[ilStepno].Jods.GetSize(): 0;
	omCreationSteps[ilStepno].Jods.NewAt(ilJodno, *prpJod);
}

// CreateJobs: create jobs according to the given information in forward order
BOOL JobCreationInfo::CreateJobs(CWnd *popParentWnd, const CString &ropText)
{
	InternalCreateJobs();
	return TRUE;
}

// DeleteJobs: delete jobs according to the given information in backward order
BOOL JobCreationInfo::DeleteJobs(CWnd *popParentWnd, const CString &ropText)
{
	for (int ilStepno = omCreationSteps.GetSize()-1; ilStepno >= 0; ilStepno--)
	{
		for (int ilJodno = omCreationSteps[ilStepno].Jods.GetSize()-1; ilJodno >= 0; ilJodno--)
		{
			// Before delete this job, we should check first if this job is still exist.
			long llJodUrno = omCreationSteps[ilStepno].Jods[ilJodno].Urno;
			if (ogJodData.GetJodByUrno(llJodUrno) == NULL)
			{
//				popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//					+ "(" + ropText + ")\n" +
//					"ist bereits gel�scht (verursacht durch vorherige Undo/Redo Aktion).\n"
//					"Please remove/undo those deletions and try again.",
//					NULL, MB_OK | MB_ICONEXCLAMATION);
				CString olTxt;
				olTxt.Format(GetString(IDS_STRING61415),ropText);
				popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
				// Must recover all records those just have been deleted
				InternalCreateJobs(ilStepno, ilJodno+1);
				return FALSE;
			}
			ogJodData.DeleteJod(llJodUrno);
		}

		// Before delete this job, we should check first if this job is still exist.
		long llJobUrno = omCreationSteps[ilStepno].Job.Urno;
		JOBDATA *prlJob = ogJobData.GetJobByUrno(llJobUrno);
		if (!prlJob)
		{
			DisplayJobDeletedMsg(popParentWnd,ropText);
			return FALSE;
		}
		else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
		{
			return FALSE;
		}
		else if (IsDifferentJob(prlJob,&omCreationSteps[ilStepno].Job))
		{
//			popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//				+ "(" + ropText + ")\n" +
//				"ist bereits gel�scht oder neu zugeteilt(verursacht durch vorherige Undo/Redo Aktion).\n"
//				"Bitte machen Sie diese L�schungen r�ckg�ngig und versuchen es noch einmal.",
//				NULL, MB_OK | MB_ICONEXCLAMATION);
			CString olTxt;
			olTxt.Format(GetString(IDS_STRING61415),ropText);
			popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
			// Must recover all records those just have been deleted
			InternalCreateJobs(ilStepno, 0);
			return FALSE;
		}
		ogJobData.DeleteJob(llJobUrno);
	}
	return TRUE;
}

void JobCreationInfo::InternalCreateJobs(int ipStartStepno, int ipStartJodno)
{
	for (int ilStepno = ipStartStepno; ilStepno < omCreationSteps.GetSize(); ilStepno++)
	{
		for (int ilJodno = ipStartJodno; ilJodno < omCreationSteps[ilStepno].Jods.GetSize(); ilJodno++)
			ogJodData.AddJod(&omCreationSteps[ilStepno].Jods[ilJodno]);
		ipStartJodno = 0;	// restart of JODDATA is only for the first round
		ogJobData.AddJob(&omCreationSteps[ilStepno].Job,"UndoInt");
	}
}

//
JobConfirmInfo::~JobConfirmInfo()
{
	// automatic release memory
	for (int ilStepno = omConfirmSteps.GetSize()-1; ilStepno >= 0; ilStepno--)
	{
		omConfirmSteps.DeleteAt(ilStepno);
	}
}

const JobConfirmInfo &JobConfirmInfo::operator =(const JobConfirmInfo &ropSourceInfo)
{
	// must allocate another set of data in memory, since destructor will call Delete..()
	for (int ilStepno = 0; ilStepno < ropSourceInfo.omConfirmSteps.GetSize(); ilStepno++)
	{
		omConfirmSteps.NewAt(ilStepno);
		omConfirmSteps[ilStepno].rmOldJob = ropSourceInfo.omConfirmSteps[ilStepno].rmOldJob;
		omConfirmSteps[ilStepno].rmNewJob = ropSourceInfo.omConfirmSteps[ilStepno].rmNewJob;
	}

	return *this;
}

// AddAnotherJob: create another "JobConfirmStep"
void JobConfirmInfo::AddAnotherJob(JOBDATA *prpOldJob,JOBDATA *prpNewJob,BOOL bpAppend)
{
	int ilStepno = (bpAppend)? omConfirmSteps.GetSize(): 0;
	omConfirmSteps.NewAt(ilStepno);
	omConfirmSteps[ilStepno].rmOldJob = *prpOldJob;
	omConfirmSteps[ilStepno].rmNewJob = *prpNewJob;
}

BOOL JobConfirmInfo::Undo(CWnd *popParentWnd, const CString &ropText)
{
	for (int ilStepno = omConfirmSteps.GetSize()-1; ilStepno >= 0; ilStepno--)
	{
		// Before undoing or redoing, we should check first if this job is still confirmed.
		JOBDATA *prlJob = ogJobData.GetJobByUrno(omConfirmSteps[ilStepno].rmNewJob.Urno);
		if (!prlJob)
		{
			DisplayJobDeletedMsg(popParentWnd,ropText);
			return FALSE;
		}
		else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
		{
			return FALSE;
		}
		else if (prlJob->Stat[0] != omConfirmSteps[ilStepno].rmNewJob.Stat[0])
		{
			CString olTxt;
			olTxt.Format(GetString(IDS_STRING61415),ropText);
			popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//			popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//				+ "(" + omUndoText + ")\n" +
//				"ist nicht mehr best�tigt (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//				NULL, MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
		else
		{
			JOBDATA rlOldJob = *prlJob;
			prlJob->Stat[0] = omConfirmSteps[ilStepno].rmOldJob.Stat[0];
			prlJob->Acfr	= omConfirmSteps[ilStepno].rmOldJob.Acfr;
			prlJob->Acto	= omConfirmSteps[ilStepno].rmOldJob.Acto;
			if (!::ogJobData.ChangeJobData(prlJob, &rlOldJob, "Undo"))
				return FALSE;
		}
	}

	return TRUE;
}

BOOL JobConfirmInfo::Redo(CWnd *popParentWnd, const CString &ropText)
{
	for (int ilStepno = 0; ilStepno < omConfirmSteps.GetSize(); ilStepno++)
	{
		// Before undoing or redoing, we should check first if this job is still ready to confirm.
		JOBDATA *prlJob = ogJobData.GetJobByUrno(omConfirmSteps[ilStepno].rmOldJob.Urno);
		if (!prlJob)
		{
			DisplayJobDeletedMsg(popParentWnd,ropText);
			return FALSE;
		}
		else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
		{
			return FALSE;
		}
		else if (prlJob->Stat[0] != omConfirmSteps[ilStepno].rmOldJob.Stat[0])
		{
			CString olTxt;
			olTxt.Format(GetString(IDS_STRING61415),ropText);
			popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//			popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//				+ "(" + omUndoText + ")\n" +
//				"ist nicht mehr im Planungs-Status (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//				NULL, MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
		else
		{
			JOBDATA rlOldJob = *prlJob;
			prlJob->Stat[0] = omConfirmSteps[ilStepno].rmNewJob.Stat[0];
			prlJob->Acfr	= omConfirmSteps[ilStepno].rmNewJob.Acfr;
			if (!::ogJobData.ChangeJobData(prlJob, &rlOldJob, "Redo"))
				return FALSE;
		}
	}

	return TRUE;
}

int		JobConfirmInfo::Jobs()
{
	return omConfirmSteps.GetSize();
}

CTime	JobConfirmInfo::MinStartTime()
{
	CTime olStartTime(TIMENULL);
	for (int ilStepno = 0; ilStepno < omConfirmSteps.GetSize(); ilStepno++)
	{
		// Before undoing or redoing, we should check first if this job is still ready to confirm.
		JOBDATA *prpJob = ogJobData.GetJobByUrno(omConfirmSteps[ilStepno].rmOldJob.Urno);
		if (prpJob)
		{
			if (olStartTime == TIMENULL)
				olStartTime = prpJob->Acfr;
			else if (prpJob->Acfr < olStartTime)
				olStartTime = prpJob->Acfr;
		}
	}
	return olStartTime;
}

CTime	JobConfirmInfo::MaxEndTime()
{
	CTime olEndTime(TIMENULL);
	for (int ilStepno = 0; ilStepno < omConfirmSteps.GetSize(); ilStepno++)
	{
		// Before undoing or redoing, we should check first if this job is still ready to confirm.
		JOBDATA *prpJob = ogJobData.GetJobByUrno(omConfirmSteps[ilStepno].rmOldJob.Urno);
		if (prpJob)
		{
			if (olEndTime == TIMENULL)
				olEndTime = prpJob->Acto;
			else if (prpJob->Acto > olEndTime)
				olEndTime = prpJob->Acto;
		}
	}	

	return olEndTime;
}

/////////////////////////////////////////////////////////////////////////////
// Shared routines for Undo/Redo features.
//
// Since most objects which are used for Undo/Redo features will do many
// common things, so I move those actual routines out of these objects and
// put them to a single place, here. Another thing, function prototypes of
// these routines are unnecessary.

// CreatePoolJob: Create a pool job, the attached job may be NULL or of type FMG or DET.
static BOOL CreatePoolJob(CWnd *popParentWnd, const CString &ropUndoText,
	JOBDATA *prpPoolJob, JOBDATA *prpAttachedJob)
{
	if(!ogDataSet.CheckJobForClockOut(popParentWnd, prpPoolJob))
	{
		return FALSE;
	}
	ogJobData.AddJob(prpPoolJob,"UndoNewPJ");
	if (prpAttachedJob != NULL)
	{
		ogJobData.AddJob(prpAttachedJob,"UndoNewDet");
	}

	return TRUE;
}

// DeletePoolJob: Delete a pool job, the attached job may be NULL or of type FMG or DET.
static BOOL DeletePoolJob(CWnd *popParentWnd, const CString &ropUndoText,
	JOBDATA *prpPoolJob, JOBDATA *prpAttachedJob)
{
	// Before undoing, check if the pool job exists
	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(prpPoolJob->Urno);
	if (!prlPoolJob)
	{
		DisplayJobDeletedMsg(popParentWnd,ropUndoText);
		return FALSE;
	}
	if(!ogDataSet.CheckJobForClockOut(popParentWnd, prpPoolJob))
	{
		return FALSE;
	}
	else if (IsDifferentJob(prlPoolJob,prpPoolJob))
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),ropUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + ropUndoText + ")\n" +
//			"ist bereits gel�scht (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schungen r�ckg�ngig.",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;

	}

	// Before undoing, we should check first if there is any jobs refer to this pool job
	// (excluding the given prpAttachedJob)
	CCSPtrArray<JOBDATA> olJobs;
	ogJobData.GetJobsByJour(olJobs, prpPoolJob->Urno);
	if(olJobs.GetSize() != 0)
	{
		if (prpAttachedJob == NULL ||
			(prpAttachedJob != NULL && (olJobs.GetSize() == 1 && olJobs[0].Urno == prpAttachedJob->Urno)))
		{
			CString olTxt;
			olTxt.Format(GetString(IDS_STRING61416),ropUndoText);
			popParentWnd->MessageBox(olTxt, NULL, MB_OK | MB_ICONEXCLAMATION);
	//		popParentWnd->MessageBox(CString("Error: Zu diesem Einsatz\n")
	//			+ "(" + ropUndoText + ")\n" +
	//			"geh�ren noch Zuordnungen.\n"
	//			"Bitte l�schen Sie diese zuerst.",
	//			NULL, MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
	}

	// If we have to undo the flight manager gate area job, we should delete it also
	if (prpAttachedJob == NULL)
	{
		// Check if this job is still exist
		if (ogJobData.GetJobByUrno(prpPoolJob->Urno) == NULL)
		{
			CString olTxt;
			olTxt.Format(GetString(IDS_STRING61415),ropUndoText);
			popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//			popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//				+ "(" + ropUndoText + ")\n" +
//				"ist bereits gel�scht (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//				"Bitte machen Sie zuerst diese L�schungen r�ckg�ngig.",
//				NULL, MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
		ogJobData.DeleteJob(prpPoolJob->Urno);
	}
	else
	{
		// Check if this job is still exist
		if (ogJobData.GetJobByUrno(prpPoolJob->Urno) == NULL ||
			ogJobData.GetJobByUrno(prpAttachedJob->Urno) == NULL)
		{
			CString olTxt;
			olTxt.Format(GetString(IDS_STRING61415),ropUndoText);
			popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//			popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//				+ "(" + ropUndoText + ")\n" +
//				"ist bereits gel�scht (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//				"Bitte machen Sie zuerst diese L�schungen r�ckg�ngig.",
//				NULL, MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
		ogJobData.DeleteJob(prpPoolJob->Urno);
		ogJobData.DeleteJob(prpAttachedJob->Urno);
	}

	return TRUE;
}

static BOOL CreateNormalJob(CWnd *popParentWnd, const CString &ropUndoText,	JOBDATA *prpJob, JODDATA *prpJod)
{
	// Before undoing or redoing, we should check first if the pool job is exist.
	if (ogJobData.GetJobByUrno(prpJob->Jour) == NULL)
	{
//		popParentWnd->MessageBox(CString("Error: Pool-Einsatz f�r diesen Einsatz\n")
//			+ "(" + ropUndoText + ")\n" +
//			"wurde gel�scht (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),ropUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	if(!ogDataSet.CheckJobForClockOut(popParentWnd, prpJob))
	{
		return FALSE;
	}

	ogJobData.AddJob(prpJob,"UndoNewJ");
	if (prpJod != NULL)
	{
		ogJodData.AddJod(prpJod);
	}

	return TRUE;
}

static BOOL DeleteNormalJob(CWnd *popParentWnd, const CString &ropUndoText,
	JOBDATA *prpJob, JODDATA *prpJod)
{
	// Before undoing or redoing, we should check first if this job is still exist.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(prpJob->Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,ropUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (IsDifferentJob(prlJob,prpJob))
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),ropUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + ropUndoText + ")\n" +
//			"wurde gel�scht oder einem anderen Mitarbeiter zugeteilt\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	ogJobData.DeleteJob(prpJob->Urno);
	if (prpJod != NULL)
	{
		ogJodData.DeleteJod(prpJod->Urno);
	}

	return TRUE;
}

// ChangeJob: Change a job. This will be used for every job types, including pool job.
static BOOL ChangeJob(CWnd *popParentWnd, const CString &ropUndoText, JOBDATA *prpJob)
{
	// we have to make changes only on the job which is already in the CedaJobData
	JOBDATA *prlJob = ogJobData.GetJobByUrno(prpJob->Urno);
	if (prlJob == NULL)
	{
		popParentWnd->MessageBox(GetString(IDS_STRING33151)
			+ "(" + ropUndoText + ")\n" +
			GetString(IDS_STRING33152),
			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	JOBDATA rlOldJob = *prlJob;
	*prlJob = *prpJob;
	ogJobData.ChangeJobData(prlJob, &rlOldJob, "UndoChng");
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// UndoCreatePoolJob

UndoCreatePoolJob::UndoCreatePoolJob(const JOBDATA *prpPoolJob)
{
	rmPoolJob = *prpPoolJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmPoolJob.Peno);
	char buf[512];
	//sprintf(buf, "Zuordnen: %s, %s zu %s (%s-%s)",
	sprintf(buf, GetString(IDS_STRING61409),
		ogEmpData.GetEmpName(prlEmp),
		rmPoolJob.Alid,
		rmPoolJob.Acfr.Format("%H%M"),
		rmPoolJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpPoolJob->Urno, omUndoText);
}

CString UndoCreatePoolJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoCreatePoolJob::Undo(CWnd *popParentWnd)
{
	return ::DeletePoolJob(popParentWnd, omUndoText, &rmPoolJob, NULL);
}

BOOL UndoCreatePoolJob::Redo(CWnd *popParentWnd)
{
	return ::CreatePoolJob(popParentWnd, omUndoText, &rmPoolJob, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// UndoChangePoolJob

UndoChangePoolJob::UndoChangePoolJob(const JOBDATA *prpOldPoolJob, const JOBDATA *prpNewPoolJob)
{
	rmOldPoolJob = *prpOldPoolJob;
	rmNewPoolJob = *prpNewPoolJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	// For this moment, we just display a message for time change since that the only one
	// change we can make for this moment.
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmOldPoolJob.Peno);
	char buf[512];
	//sprintf(buf, "Time change for job %s assigned to %s from %s-%s to %s-%s",
	sprintf(buf, GetString(IDS_STRING61410), ogDataSet.JobBarText(&rmOldPoolJob), ogEmpData.GetEmpName(prlEmp),
										rmOldPoolJob.Acfr.Format("%H%M/d"),	rmOldPoolJob.Acto.Format("%H%M/d"),
										rmNewPoolJob.Acfr.Format("%H%M/%d"),rmNewPoolJob.Acto.Format("%H%M/%d"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpNewPoolJob->Urno, omUndoText);
}

CString UndoChangePoolJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoChangePoolJob::Undo(CWnd *popParentWnd)
{
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewPoolJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (IsDifferentJob(prlJob,&rmNewPoolJob))
	{
		DisplayJobDiffersMsg(popParentWnd,omUndoText,&rmNewPoolJob);
		return FALSE;
	}
	else
	{
		return ogJobData.ChangeJobData(&rmOldPoolJob, &rmNewPoolJob, "UndoPJ");
	}
}

BOOL UndoChangePoolJob::Redo(CWnd *popParentWnd)
{
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmOldPoolJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (IsDifferentJob(prlJob,&rmOldPoolJob))
	{
		DisplayJobDiffersMsg(popParentWnd,omUndoText,&rmOldPoolJob);
		return FALSE;
	}
	else
		return ogJobData.ChangeJobData(&rmNewPoolJob, &rmOldPoolJob, "RedoPJ");
}

/////////////////////////////////////////////////////////////////////////////
// UndoCreateFmgJob

UndoCreateFmgJob::UndoCreateFmgJob(const JOBDATA *prpFmgJob, const JOBDATA *prpPoolJob)
{
	rmFmgJob = *prpFmgJob;
	rmPoolJob = *prpPoolJob;

	// get the destination of this assignment
	CString olDestination = (CString(rmFmgJob.Jtco) == JOBFMGATEAREA)?
		ogBasicData.GetGateAreaName(rmFmgJob.Alid):
		ogBasicData.GetTextById(rmFmgJob.Alid);

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmFmgJob.Peno);
	char buf[512];
	//sprintf(buf, "Zuordnen: %s, %s zu %s (%s-%s)",
	sprintf(buf, GetString(IDS_STRING61411),
		ogEmpData.GetEmpName(prlEmp),
//		prlEmp? prlEmp->Rank: "",
		(const char *)olDestination,
		rmFmgJob.Acfr.Format("%H%M"),
		rmFmgJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpFmgJob->Urno, omUndoText);
}

CString UndoCreateFmgJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoCreateFmgJob::Undo(CWnd *popParentWnd)
{
	return ::DeletePoolJob(popParentWnd, omUndoText, &rmPoolJob, &rmFmgJob);
}

BOOL UndoCreateFmgJob::Redo(CWnd *popParentWnd)
{
	return ::CreatePoolJob(popParentWnd, omUndoText, &rmPoolJob, &rmFmgJob);
}

/////////////////////////////////////////////////////////////////////////////
// UndoCreateDetachJob

UndoCreateDetachJob::UndoCreateDetachJob(const JOBDATA *prpDetachJob, const JOBDATA *prpPoolJob)
{
	rmDetachJob = *prpDetachJob;
	rmPoolJob = *prpPoolJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmDetachJob.Peno);
	JOBDATA *prlOriginalPoolJob = ogJobData.GetJobByUrno(prpDetachJob->Jour);
	char buf[512];
	//sprintf(buf, "Abordnen: %s, %s from %s to %s (%s-%s)",
	sprintf(buf, GetString(IDS_STRING61412),
		ogEmpData.GetEmpName(prlEmp),
		prlOriginalPoolJob->Alid,
		rmDetachJob.Alid,
		rmDetachJob.Acfr.Format("%H%M"),
		rmDetachJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpDetachJob->Urno, omUndoText);
}

CString UndoCreateDetachJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoCreateDetachJob::Undo(CWnd *popParentWnd)
{
	return ::DeletePoolJob(popParentWnd, omUndoText, &rmPoolJob, &rmDetachJob);
}

BOOL UndoCreateDetachJob::Redo(CWnd *popParentWnd)
{
	return ::CreatePoolJob(popParentWnd, omUndoText, &rmPoolJob, &rmDetachJob);
}

/////////////////////////////////////////////////////////////////////////////
// UndoCreateGroupDelegation

UndoCreateGroupDelegation::UndoCreateGroupDelegation(const JOBDATA *prpDetachJob, const JOBDATA *prpPoolJob, const DLGDATA *prpDlg)
{
	rmDetachJob = *prpDetachJob;
	rmPoolJob = *prpPoolJob;
	rmDlg = *prpDlg;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmDetachJob.Peno);
	JOBDATA *prlOriginalPoolJob = ogJobData.GetJobByUrno(prpDetachJob->Jour);

	CString olGroupFrom, olGroupTo;
	olGroupFrom.Format("%s/%s",ogBasicData.GetTeamForPoolJob(prlOriginalPoolJob),prlOriginalPoolJob->Alid);
	olGroupTo.Format("%s/%s",prpDlg->Wgpc,prpPoolJob->Alid);

	char buf[512];
	//sprintf(buf, "Abordnen: %s, %s from %s to %s (%s-%s)",
	sprintf(buf, GetString(IDS_STRING61412),
		ogEmpData.GetEmpName(prlEmp),
		olGroupFrom,olGroupTo,
		rmDetachJob.Acfr.Format("%H%M"),
		rmDetachJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpDetachJob->Urno, omUndoText);
}

CString UndoCreateGroupDelegation::UndoText() const
{
	return omUndoText;
}

BOOL UndoCreateGroupDelegation::Undo(CWnd *popParentWnd)
{
	if(!ogDataSet.CheckJobForClockOut(popParentWnd, &rmPoolJob))
		return FALSE;

	ogDlgData.Delete(rmDlg.Urno);
	return ::DeletePoolJob(popParentWnd, omUndoText, &rmPoolJob, &rmDetachJob);
}

BOOL UndoCreateGroupDelegation::Redo(CWnd *popParentWnd)
{
	if(!ogDataSet.CheckJobForClockOut(popParentWnd, &rmPoolJob))
		return FALSE;

	if(ogDlgData.Insert(&rmPoolJob, rmDlg.Wgpc, rmDlg.Fctc) != NULL)
		return ::CreatePoolJob(popParentWnd, omUndoText, &rmPoolJob, &rmDetachJob);
	else
		return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// UndoReassignJob

UndoReassignJob::UndoReassignJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlOldEmp = ogEmpData.GetEmpByPeno(rmOldJob.Peno);
	EMPDATA *prlNewEmp = ogEmpData.GetEmpByPeno(rmNewJob.Peno);
	char buf[512];
	//sprintf(buf, "Einsatz neu zuteilen: von %s, %s zu %s, %s",
	sprintf(buf, GetString(IDS_STRING61413),
		ogEmpData.GetEmpName(prlOldEmp),
		ogEmpData.GetEmpName(prlNewEmp));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpNewJob->Urno, omUndoText);
}

CString UndoReassignJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoReassignJob::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still exist.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmOldJob.Urno);
	// not deleted
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	// and still owned by the same staff
	else if (CString(prlJob->Peno) != rmNewJob.Peno || prlJob->Jour != rmNewJob.Jour || prlJob->Shur != rmNewJob.Shur)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht oder einem anderen Mitarbeiter zugeteilt\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlJobBeforeChange = *prlJob;
		strcpy(prlJob->Peno,rmOldJob.Peno);
		prlJob->Jour = rmOldJob.Jour;
		prlJob->Shur = rmOldJob.Shur;
		prlJob->Infm = rmOldJob.Infm;
		ogJobData.SetAdditionalJobFields(prlJob);
		return ogJobData.ChangeJobData(prlJob, &rlJobBeforeChange, "UndoReass");
	}
}

BOOL UndoReassignJob::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still exist.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	// not deleted
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	// and still owned by the same staff
	else if (CString(prlJob->Peno) != rmOldJob.Peno || prlJob->Jour != rmOldJob.Jour || prlJob->Shur != rmOldJob.Shur)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: This job\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht oder einem anderen Mitarbeiter zugeteilt\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlJobBeforeChange = *prlJob;
		strcpy(prlJob->Peno,rmNewJob.Peno);
		prlJob->Jour = rmNewJob.Jour;
		prlJob->Shur = rmNewJob.Shur;
		prlJob->Infm = rmNewJob.Infm;
		ogJobData.SetAdditionalJobFields(prlJob);
		return ogJobData.ChangeJobData(prlJob, &rlJobBeforeChange, "RedoReass");
	}
}


/////////////////////////////////////////////////////////////////////////////
// UndoReassignEquJob

UndoReassignEquJob::UndoReassignEquJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;

	EQUDATA *prlOldEqu = ogEquData.GetEquByUrno(rmOldJob.Uequ);
	EQUDATA *prlNewEqu = ogEquData.GetEquByUrno(rmNewJob.Uequ);

	omUndoText.Format(GetString(IDS_UNDOREASSIGNEQU), prlOldEqu? prlOldEqu->Enam: "", prlNewEqu? prlNewEqu->Enam: "");
	ogOfflineDescription.SetOfflineDescription(prpNewJob->Urno, omUndoText);
}

CString UndoReassignEquJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoReassignEquJob::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job still exists.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if(prlJob == NULL)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}

	// job has since been reassogned again
	if(prlJob->Uequ != rmNewJob.Uequ)
	{
		CString olTxt;
		// "Error: This Duty\n(%s)\nihas already been deleted (possibly as a result of a previous Undo/Redo).\nPlease undo the deletion before attempting to delete it."
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlJobBeforeChange = *prlJob;
		prlJob->Uequ = rmOldJob.Uequ;
		return ogJobData.ChangeJobData(prlJob, &rlJobBeforeChange, "UndoReassEqu");
	}
}

BOOL UndoReassignEquJob::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job still exists.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if(prlJob == NULL)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}

	// job has since been reassogned again
	if(prlJob->Uequ != rmOldJob.Uequ)
	{
		CString olTxt;
		// "Error: This Duty\n(%s)\nihas already been deleted (possibly as a result of a previous Undo/Redo).\nPlease undo the deletion before attempting to delete it."
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlJobBeforeChange = *prlJob;
		prlJob->Uequ = rmNewJob.Uequ;
		return ogJobData.ChangeJobData(prlJob, &rlJobBeforeChange,"RedoReassEqu");
	}
}

/////////////////////////////////////////////////////////////////////////////
// UndoCreateNormalJob

UndoCreateNormalJob::UndoCreateNormalJob(const CString &ropJobDescription,
	const JOBDATA *prpJob, const JODDATA *prpJod)
{
	bmIsThisJobHasJod = (prpJod != NULL);
	rmJob = *prpJob;
	if (bmIsThisJobHasJod)
	{
		rmJod = *prpJod;
	}

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmJob.Peno);
	char buf[512];
	//sprintf(buf, "Zuordnen: %s, %s f�r %s (%s-%s)",
	sprintf(buf, GetString(IDS_STRING61514),
		ogEmpData.GetEmpName(prlEmp),
		ropJobDescription,
		rmJob.Acfr.Format("%H%M"),
		rmJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpJob->Urno, omUndoText);
}

CString UndoCreateNormalJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoCreateNormalJob::Undo(CWnd *popParentWnd)
{
	return ::DeleteNormalJob(popParentWnd, omUndoText, &rmJob, (bmIsThisJobHasJod? &rmJod: NULL));
}

BOOL UndoCreateNormalJob::Redo(CWnd *popParentWnd)
{
	return ::CreateNormalJob(popParentWnd, omUndoText, &rmJob, (bmIsThisJobHasJod? &rmJod: NULL));
}

/////////////////////////////////////////////////////////////////////////////
// UndoConfirmJob

UndoConfirmJob::UndoConfirmJob(const CString &ropDescription,const JobConfirmInfo& ropJobCreationInfo)
{
	omJobConfirmInfo = ropJobCreationInfo;
	omUndoText = ropDescription;

	int ilNumJobs = ropJobCreationInfo.omConfirmSteps.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		ogOfflineDescription.SetOfflineDescription(ropJobCreationInfo.omConfirmSteps[ilJ].rmOldJob.Urno, omUndoText);
	}
}

CString UndoConfirmJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoConfirmJob::Undo(CWnd *popParentWnd)
{
	return omJobConfirmInfo.Undo(popParentWnd, omUndoText);

}

BOOL UndoConfirmJob::Redo(CWnd *popParentWnd)
{
	return omJobConfirmInfo.Redo(popParentWnd, omUndoText);
}



/////////////////////////////////////////////////////////////////////////////
// UndoSetJobStandby

UndoSetJobStandby::UndoSetJobStandby(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmOldJob.Peno);
	char buf[512];

	//"Einsatz Standby f�r %s, %s (%s-%s)"
	sprintf(buf, GetString(IDS_STRING61208),
		ogEmpData.GetEmpName(prlEmp),
		rmOldJob.Acfr.Format("%H%M"),rmOldJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpOldJob->Urno, omUndoText);
}

CString UndoSetJobStandby::UndoText() const
{
	return omUndoText;
}

BOOL UndoSetJobStandby::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still confirmed.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Ignr[0] != rmNewJob.Ignr[0])
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Ignr[0] = rmOldJob.Ignr[0];
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "UndoStdby");
	}
}

BOOL UndoSetJobStandby::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still ready to confirm.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmOldJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Ignr[0] != rmOldJob.Ignr[0])
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"ist nicht mehr im Planungs-Status (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Ignr[0] = rmNewJob.Ignr[0];
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "RedoStdby");
	}
}



/////////////////////////////////////////////////////////////////////////////
// UndoSetCoffeeBreak

UndoSetCoffeeBreak::UndoSetCoffeeBreak(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmOldJob.Peno);
	char buf[512];

	//"Kaffee Pause f�r %s, %s (%s-%s)"
	sprintf(buf, GetString(IDS_STRING61204),
		ogEmpData.GetEmpName(prlEmp),
		rmOldJob.Acfr.Format("%H%M"),rmOldJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpOldJob->Urno, omUndoText);
}

CString UndoSetCoffeeBreak::UndoText() const
{
	return omUndoText;
}

BOOL UndoSetCoffeeBreak::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still confirmed.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Ignr[0] != rmNewJob.Ignr[0])
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"ist nicht mehr best�tigt (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Ignr[0] = rmOldJob.Ignr[0];
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "UndoCoffee");
	}
}

BOOL UndoSetCoffeeBreak::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still ready to confirm.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmOldJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Ignr[0] != rmOldJob.Ignr[0])
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"ist nicht mehr im Planungs-Status (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Ignr[0] = rmNewJob.Ignr[0];
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "RedoCoffee");
	}
}

/////////////////////////////////////////////////////////////////////////////
// UndoEmpInformedOfJob

UndoEmpInformedOfJob::UndoEmpInformedOfJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmOldJob.Peno);
	char buf[512];

	//"Employee %s, %s informed of job (%s-%s)"
	sprintf(buf, GetString(IDS_UNDO_INFORM),
		ogEmpData.GetEmpName(prlEmp),
		rmOldJob.Acfr.Format("%H%M"),rmOldJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpOldJob->Urno, omUndoText);
}

CString UndoEmpInformedOfJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoEmpInformedOfJob::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still confirmed.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Infm != rmNewJob.Infm)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"ist nicht mehr best�tigt (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Infm = rmOldJob.Infm;
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "UndoEmpInf");
	}
}

BOOL UndoEmpInformedOfJob::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still ready to confirm.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmOldJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Infm != rmOldJob.Infm)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"ist nicht mehr im Planungs-Status (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Infm = rmNewJob.Infm;
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "RedoEmpInf");
	}
}

/////////////////////////////////////////////////////////////////////////////
// UndoFinishJob

UndoFinishJob::UndoFinishJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmOldJob.Peno);
	char buf[512];
	//sprintf(buf, "Einsatz beenden f�r %s, %s (%s-%s)",
	sprintf(buf, GetString(IDS_STRING61418),
		ogEmpData.GetEmpName(prlEmp),
		rmOldJob.Acfr.Format("%H%M"),
		rmOldJob.Acto.Format("%H%M"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpOldJob->Urno, omUndoText);
}

CString UndoFinishJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoFinishJob::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still finished.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Stat[0] != rmNewJob.Stat[0])
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"hat nicht mehr den Status <beendet> (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Stat[0] = rmOldJob.Stat[0];
		prlJob->Acto	= rmOldJob.Acto;
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "UndoEnd");
	}
}

BOOL UndoFinishJob::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still ready to finish.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmOldJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Stat[0] != rmOldJob.Stat[0])
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"hat nicht mehr den Status <best�tigt> (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Stat[0] = rmNewJob.Stat[0];
		prlJob->Acto	= rmNewJob.Acto;
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "RedoEnd");
	}
}

/////////////////////////////////////////////////////////////////////////////
// UndoDeleteJob

UndoDeleteJob::UndoDeleteJob(const CString &ropDescription, JobCreationInfo *popJobCreationInfo)
{
	omJobCreationInfo = *popJobCreationInfo;
	//omUndoText = CString("L�schen ") + ropDescription;
	omUndoText = GetString(IDS_STRING61210) + ropDescription;

	int ilNumJobs = popJobCreationInfo->omCreationSteps.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		ogOfflineDescription.SetOfflineDescription(popJobCreationInfo->omCreationSteps[ilJ].Job.Urno, omUndoText);
	}
}

CString UndoDeleteJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoDeleteJob::Undo(CWnd *popParentWnd)
{
	return omJobCreationInfo.CreateJobs(popParentWnd, omUndoText);
}

BOOL UndoDeleteJob::Redo(CWnd *popParentWnd)
{
	return omJobCreationInfo.DeleteJobs(popParentWnd, omUndoText);
}

/////////////////////////////////////////////////////////////////////////////
// UndoReassignJobToNewDemand

UndoReassignJobToNewDemand::UndoReassignJobToNewDemand(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob, const DEMANDDATA *prpOldDem,  const DEMANDDATA *prpNewDem)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;
	rmOldDem = *prpOldDem;
	rmNewDem = *prpNewDem;

	// check old demand, if not exist, preset to 0, to prevent an error message on undo
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(rmOldDem.Urno);
	if (!prlDemand)
		rmOldDem.Urno = 0l;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(rmOldJob.Peno);
	FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(&rmOldJob);

	// "%s %s Demand Reassignment For Flight %s"
	omUndoText.Format(GetString(IDS_UNDODEMANDREASSIGN),
						ogEmpData.GetEmpName(prlEmp),
						prlFlight? prlFlight->Fnum: "");

	CString olMsg;// "(Job From %s To %s)"
	olMsg.Format(GetString(IDS_INVALIDJOBDEMSTR),rmNewJob.Acfr.Format("%H%M"),rmNewJob.Acto.Format("%H%M"));
	omUndoText += " " + olMsg;

	CCSPtrArray <JODDATA> olJods;
	ogJodData.GetJodsByJob(olJods, prpOldJob->Urno);
	int ilNumJods = olJods.GetSize();
	for(int ilJ = 0; ilJ < ilNumJods; ilJ++)
	{
		ogOfflineDescription.SetOfflineDescription(olJods[ilJ].Urno, omUndoText);
	}
}

CString UndoReassignJobToNewDemand::UndoText() const
{
	return omUndoText;
}

BOOL UndoReassignJobToNewDemand::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job still exists and
	// the old demand still exists
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if(prlJob == NULL)
	{
		// job no longer exists
		popParentWnd->MessageBox(GetString(IDS_UNDODEMANDREASSIGN2),omUndoText, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(rmOldDem.Urno);
	if(rmOldDem.Urno > 0 && prlDemand == NULL)
	{
		// old demand no longer exists
		popParentWnd->MessageBox(GetString(IDS_UNDODEMANDREASSIGN3),omUndoText, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	if(ogJodData.bmUseJodtab)
	{
		JODDATA *prlJod = NULL;
		CCSPtrArray <JODDATA> olJods;
		ogJodData.GetJodsByJob(olJods,rmNewJob.Urno);
		int ilNumJods = olJods.GetSize();
		for(int ilJod = 0; prlJod == NULL && ilJod < ilNumJods; ilJod++)
		{
			JODDATA *prlTmpJod = &olJods[ilJod];
			if(prlTmpJod->Ujob == rmNewJob.Urno && prlTmpJod->Udem == rmNewDem.Urno)
			{
				prlJod = prlTmpJod;
			}
		}
		if(prlJod == NULL)
		{
			// job is no longer assigned to the demand
			popParentWnd->MessageBox(GetString(IDS_UNDODEMANDREASSIGN4),omUndoText, MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}

		if (rmOldDem.Urno)
			ogJodData.ChangeDemu(prlJod,rmOldDem.Urno);
		else if (prlJod)
			ogJodData.DeleteJod(prlJod->Urno);
	}

	JOBDATA rlOldJob = *prlJob;
	prlJob->Acfr = rmOldJob.Acfr;
	prlJob->Acto = rmOldJob.Acto;
	prlJob->Utpl = rmOldJob.Utpl;
	prlJob->Udem = rmOldJob.Udem;
	prlJob->Infm = rmOldJob.Infm;
	ogJobData.SetAdditionalJobFields(prlJob);

	long llOriginalFlightUrno = ogDemandData.GetFlightUrno(prlDemand);
	if(llOriginalFlightUrno != prlJob->Flur)
	{
		FLIGHTDATA *prlOriginalFlight = ogFlightData.GetFlightByUrno(llOriginalFlightUrno);
		if(prlOriginalFlight != NULL)
		{
			prlJob->Flur = llOriginalFlightUrno;
			ogDataSet.CreateOldFlightFields(prlJob, prlOriginalFlight);
		}
	}
	
	ogJobData.ChangeJobData(prlJob, &rlOldJob, "UndoReass2");

	return TRUE;
}

BOOL UndoReassignJobToNewDemand::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job still exists and
	// the old demand still exists
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if(prlJob == NULL)
	{
		// job no longer exists
		popParentWnd->MessageBox(GetString(IDS_UNDODEMANDREASSIGN2),omUndoText, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(rmNewDem.Urno);
	if(prlDemand == NULL)
	{
		// old demand no longer exists
		popParentWnd->MessageBox(GetString(IDS_UNDODEMANDREASSIGN3),omUndoText, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	JODDATA *prlJod = NULL;
	CCSPtrArray <JODDATA> olJods;
	ogJodData.GetJodsByJob(olJods,rmNewJob.Urno);
	int ilNumJods = olJods.GetSize();
	for(int ilJod = 0; prlJod == NULL && ilJod < ilNumJods; ilJod++)
	{
		JODDATA *prlTmpJod = &olJods[ilJod];
		if(prlTmpJod->Ujob == rmNewJob.Urno && prlTmpJod->Udem == rmOldDem.Urno)
		{
			prlJod = prlTmpJod;
		}
	}

	if(prlJod == NULL && rmOldDem.Urno)
	{
		// job is no longer assigned to the demand
		popParentWnd->MessageBox(GetString(IDS_UNDODEMANDREASSIGN4),omUndoText, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	if (prlJod)
		ogJodData.ChangeDemu(prlJod,rmNewDem.Urno);
	else if (prlDemand)
	{
		prlJod = ogJodData.AddJod(prlJob->Urno,prlDemand->Urno);

	}

	JOBDATA rlOldJob = *prlJob;
	prlJob->Acfr = rmNewJob.Acfr;
	prlJob->Acto = rmNewJob.Acto;
	prlJob->Utpl = rmOldJob.Utpl;
	prlJob->Udem = rmOldJob.Udem;
	prlJob->Infm = rmOldJob.Infm;
	ogJobData.SetAdditionalJobFields(prlJob);
	ogJobData.ChangeJobData(prlJob, &rlOldJob, "RedoReass2");

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// UndoChangeTimeOfJob

UndoChangeTimeOfJob::UndoChangeTimeOfJob(JOBDATA *prpOldJob,CTime opNewStart,CTime opNewEnd)
{
	lmUrno = prpOldJob->Urno;
	omOldTime[0] = prpOldJob->Acfr;
	omOldTime[1] = prpOldJob->Acto;
	omNewTime[0] = opNewStart;
	omNewTime[1] = opNewEnd;

	// create and store text which will be displayed in the Undo/Redo dialog in advanced
	// For this moment, we just display a message for time change since that the only one
	// change we can make for this moment.
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpOldJob->Peno);

	char buf[512];
	//sprintf(buf, "Time change for job %s assigned to %s from %s-%s to %s-%s",
	sprintf(buf, GetString(IDS_STRING61410), ogDataSet.JobBarText(prpOldJob), ogEmpData.GetEmpName(prlEmp),
										prpOldJob->Acfr.Format("%H%M/%d"),	prpOldJob->Acto.Format("%H%M/%d"),
										opNewStart.Format("%H%M/%d"),opNewEnd.Format("%H%M/%d"));
	omUndoText = buf;
	ogOfflineDescription.SetOfflineDescription(prpOldJob->Urno, omUndoText);
}

CString UndoChangeTimeOfJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoChangeTimeOfJob::Undo(CWnd *popParentWnd)
{
	// we have to make changes only on the job which is already in the CedaJobData
	JOBDATA *prlJob = ogJobData.GetJobByUrno(lmUrno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Acfr != omNewTime[0] || prlJob->Acto != omNewTime[1])
	{
		popParentWnd->MessageBox(GetString(IDS_STRING33151)
			+ "(" + omUndoText + ")\n" +
			GetString(IDS_STRING33152),
			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
		return ogJobData.ChangeJobTime(lmUrno,omOldTime[0],omOldTime[1],"UndoTim");
}

BOOL UndoChangeTimeOfJob::Redo(CWnd *popParentWnd)
{
	JOBDATA *prlJob = ogJobData.GetJobByUrno(lmUrno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Acfr != omOldTime[0] || prlJob->Acto != omOldTime[1])
	{
		popParentWnd->MessageBox(GetString(IDS_STRING33151)
			+ "(" + omUndoText + ")\n" +
			GetString(IDS_STRING33152),
			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
		return ogJobData.ChangeJobTime(lmUrno,omNewTime[0],omNewTime[1],"RedoTim");
}

/////////////////////////////////////////////////////////////////////////////
// UndoList

UndoList::UndoList(UndoObject *popObj)
{
	omList.Add(popObj);
	omUndoText = popObj->UndoText();
}

UndoList::~UndoList()
{
	omList.DeleteAll();
}

void UndoList::Add(UndoObject *popObj)
{
	omList.Add(popObj);
}

CString UndoList::UndoText() const
{
	return omUndoText;
}

BOOL UndoList::Undo(CWnd *popParentWnd)
{
	for (int i = 0; i < omList.GetSize(); i++)
	{
		if (!omList[i].Undo(popParentWnd))
			return FALSE;
	}

	return TRUE;
}

BOOL UndoList::Redo(CWnd *popParentWnd)
{
	for (int i = 0; i < omList.GetSize(); i++)
	{
		if (!omList[i].Redo(popParentWnd))
			return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// UndoReassignDemandAlid

UndoReassignDemandAlid::UndoReassignDemandAlid(const DEMANDDATA *prpOldDem, const CString& ropNewAlid)
{
	ASSERT(prpOldDem);
	lmDemUrno = prpOldDem->Urno;
	omOldAlid = prpOldDem->Alid;
	omNewAlid = ropNewAlid;

	if (omOldAlid.IsEmpty())
	{
//		omUndoText.Format("assign demand location to %s",omNewAlid);
		omUndoText.Format(GetString(IDS_STRING61962),omNewAlid);
	}
	else
	{
//		omUndoText.Format("reassign demand location from %s to %s",omOldAlid,omNewAlid);
		omUndoText.Format(GetString(IDS_STRING61960),omOldAlid,omNewAlid);
	}
}

UndoReassignDemandAlid::~UndoReassignDemandAlid()
{
	omOldCcaList.DeleteAll();
	omNewCcaList.DeleteAll();
}

UndoReassignDemandAlid::UndoReassignDemandAlid(const UndoReassignDemandAlid& rhs)
{
	*this = rhs;
}

UndoReassignDemandAlid&	UndoReassignDemandAlid::operator=(const UndoReassignDemandAlid& rhs)
{
	if (this != &rhs)
	{
		lmDemUrno = rhs.lmDemUrno;
		omOldAlid = rhs.omOldAlid;
		omNewAlid = rhs.omNewAlid;
		for (int i = 0; i < rhs.omOldCcaList.GetSize(); i++)
		{
			omOldCcaList.New(rhs.omOldCcaList[i]);
		}

		for (i = 0; i < rhs.omNewCcaList.GetSize(); i++)
		{
			omNewCcaList.New(rhs.omNewCcaList[i]);
		}
	}

	return *this;
}

BOOL UndoReassignDemandAlid::AddOldCcaRecord(const CCADATA *prpCca)
{
	ASSERT(prpCca);

	if (prpCca)
	{
		omOldCcaList.New(*prpCca);
		return TRUE;
	}
	else
		return FALSE;
}

BOOL UndoReassignDemandAlid::AddNewCcaRecord(const CCADATA *prpCca)
{
	ASSERT(prpCca);

	if (prpCca)
	{
		omNewCcaList.New(*prpCca);
		return TRUE;
	}
	else
		return FALSE;
}

CString UndoReassignDemandAlid::UndoText() const
{
	return omUndoText;
}

BOOL UndoReassignDemandAlid::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this demand still exists.
	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(lmDemUrno);
	// not deleted
	if (!prlDem)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	// and still assigned to the same location
	else if (CString(prlDem->Alid) != omNewAlid)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61961),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Bedarf\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		BOOL blResult = ogDemandData.AssignAlid(lmDemUrno,omOldAlid,true);
		if (!blResult)
			return blResult;

		// move jobs too
		CCSPtrArray<JOBDATA> olJobs;	
		ogJodData.GetJobsByDemand(olJobs,prlDem->Urno);	
		for (int ilJob = 0; ilJob < olJobs.GetSize(); ilJob++)
		{
			JOBDATA *prlJob = &olJobs[ilJob];
			if (prlJob->Stat[0] == 'P' && strcmp(prlJob->Alid,omNewAlid) == 0)
			{
				JOBDATA rlOldJob = *prlJob;
				strcpy(prlJob->Alid,omOldAlid);
				ogJobData.ChangeJobData(prlJob, &rlOldJob, "AlidFromDem");
			}
		}	
		
		// undo creation of new cca table record, if neccessary
		for (int ilCca = 0; ilCca < omNewCcaList.GetSize(); ilCca++)
		{
			blResult = ogCcaData.DeleteCcaRecord(omNewCcaList[ilCca].Urno);	
			if (!blResult)
				return blResult;
		}

		// redo creation of old cca table record, if neccessary
		for (ilCca = 0; ilCca < omOldCcaList.GetSize(); ilCca++)
		{
			blResult = ogCcaData.AddCcaRecord(omOldCcaList[ilCca]);
			if (!blResult)
				return blResult;
		}

		return blResult;
	}
}

BOOL UndoReassignDemandAlid::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this demand still exists.
	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(lmDemUrno);
	// not deleted
	if (!prlDem)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	// and still assigned to the same location
	else if (CString(prlDem->Alid) != omOldAlid)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61961),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: This demand\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		BOOL blResult = ogDemandData.AssignAlid(lmDemUrno,omNewAlid,true);
		if (!blResult)
			return blResult;

		// move jobs too
		CCSPtrArray<JOBDATA> olJobs;	
		ogJodData.GetJobsByDemand(olJobs,prlDem->Urno);	
		for (int ilJob = 0; ilJob < olJobs.GetSize(); ilJob++)
		{
			JOBDATA *prlJob = &olJobs[ilJob];
			if (prlJob->Stat[0] == 'P' && strcmp(prlJob->Alid,omOldAlid) == 0)
			{
				JOBDATA rlOldJob = *prlJob;
				strcpy(prlJob->Alid,omNewAlid);
				ogJobData.ChangeJobData(prlJob, &rlOldJob, "AlidFromDem");
			}
		}	

		// undo creation of old cca table record, if neccessary
		for (int ilCca = 0; ilCca < omOldCcaList.GetSize(); ilCca++)
		{
			blResult = ogCcaData.DeleteCcaRecord(omOldCcaList[ilCca].Urno);		
			if (!blResult)
				return blResult;
		}

		// redo creation of new cca table record, if neccessary
		for (ilCca = 0; ilCca < omNewCcaList.GetSize(); ilCca++)
		{
			blResult = ogCcaData.AddCcaRecord(omNewCcaList[ilCca]);
			if (!blResult)
				return blResult;
		}

		
		return blResult;
	}
}

/////////////////////////////////////////////////////////////////////////////
// UndoDemandAutoAssignment

UndoDemandAutoAssignment::UndoDemandAutoAssignment(DEMANDDATA *prpOldDem,bool bpNewAutoAssign)
{
	ASSERT(prpOldDem);
	lmDemUrno = prpOldDem->Urno;
	bmOldAutoAssignment = ogDemandData.AutomaticAssignmentEnabled(prpOldDem);
	bmNewAutoAssignment = bpNewAutoAssign;

	if (bmOldAutoAssignment)
	{
//		omUndoText.Format("automatic assignment of demand enabled");
		omUndoText.Format(GetString(IDS_STRING62501));
	}
	else
	{
//		omUndoText.Format("automatic assignment of demand disabled");
		omUndoText.Format(GetString(IDS_STRING62502));
	}
}

CString UndoDemandAutoAssignment::UndoText() const
{
	return omUndoText;
}

BOOL UndoDemandAutoAssignment::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this demand still exists.
	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(lmDemUrno);
	// not deleted
	if (!prlDem)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	// and still assigned to the same location
	else if (ogDemandData.AutomaticAssignmentEnabled(prlDem) != bmNewAutoAssignment)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING62503),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Bedarf\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		BOOL blResult = ogDemandData.EnableAutomaticAssignment(prlDem,bmOldAutoAssignment);
		if (!blResult)
			return blResult;

		return blResult;
	}
}

BOOL UndoDemandAutoAssignment::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this demand still exists.
	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(lmDemUrno);
	// not deleted
	if (!prlDem)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	// and still assigned to the same location
	else if (ogDemandData.AutomaticAssignmentEnabled(prlDem) != bmOldAutoAssignment)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING62503),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: This demand\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		BOOL blResult = ogDemandData.EnableAutomaticAssignment(prlDem,bmNewAutoAssignment);
		if (!blResult)
			return blResult;

		return blResult;
	}
}


/////////////////////////////////////////////////////////////////////////////
// UndoDeactivateDemand

UndoDeactivateDemand::UndoDeactivateDemand(DEMANDDATA *prpOldDem,bool bpNewDeactivateDemand)
{
	ASSERT(prpOldDem);
	lmDemUrno = prpOldDem->Urno;
	bmNewDeactivateDemand = bpNewDeactivateDemand;
	bmOldDeactivateDemand = !bmNewDeactivateDemand;

	omUndoText.Format("%s: %s - %s %s %s %s", (bmOldDeactivateDemand) ? GetString(IDS_UNDODEMANDACTIVE) : GetString(IDS_UNDODEMANDDEACTIVE),
		prpOldDem->Debe.Format("%H%M/%d"),prpOldDem->Deen.Format("%H%M/%d"),
		ogFlightData.GetFlightNum(ogDemandData.GetFlightUrno(prpOldDem)),
		ogDemandData.GetStringForDemandAndResourceType(*prpOldDem),
		ogBasicData.GetDemandBarText(prpOldDem));
}

CString UndoDeactivateDemand::UndoText() const
{
	return omUndoText;
}

BOOL UndoDeactivateDemand::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this demand still exists.
	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(lmDemUrno);
	// not deleted
	if (!prlDem)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	// and still assigned to the same location
	else if (ogDemandData.DemandIsDeactivated(prlDem) != bmNewDeactivateDemand)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING62503),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Bedarf\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		BOOL blResult = ogDemandData.DeactivateDemand(prlDem,bmOldDeactivateDemand);
		if (!blResult)
			return blResult;

		return blResult;
	}
}

BOOL UndoDeactivateDemand::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this demand still exists.
	DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(lmDemUrno);
	// not deleted
	if (!prlDem)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	// and still assigned to the same location
	else if (ogDemandData.DemandIsDeactivated(prlDem) != bmOldDeactivateDemand)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING62503),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: This demand\n")
//			+ "(" + omUndoText + ")\n" +
//			"wurde gel�scht\n(m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n"
//			"Bitte machen Sie zuerst diese L�schung r�ckg�ngig",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		BOOL blResult = ogDemandData.DeactivateDemand(prlDem,bmNewDeactivateDemand);
		if (!blResult)
			return blResult;

		return blResult;
	}
}

/////////////////////////////////////////////////////////////////////////////
// UndoCreateEquipmentJob

static BOOL CreateEquipmentJob(CWnd *popParentWnd, const CString &ropUndoText,	JOBDATA *prpJob, JODDATA *prpJod)
{
	ogJobData.AddJob(prpJob,"UndoEquJob");
	if (prpJod != NULL)
	{
		ogJodData.AddJod(prpJod);
	}

	return TRUE;
}

static BOOL DeleteEquipmentJob(CWnd *popParentWnd, const CString &ropUndoText,	JOBDATA *prpJob, JODDATA *prpJod)
{
	// Before undoing or redoing, we should check first if this job is still exist.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(prpJob->Urno);
	if(!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,ropUndoText);
		return FALSE;
	}
	else if(IsDifferentJob(prlJob,prpJob))
	{
		// "Error: This Duty has already been deleted (possibly as a result of a previous Undo/Redo)."
		// "Please undo the deletion before attempting to delete it."
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),ropUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	ogJobData.DeleteJob(prpJob->Urno);
	if (prpJod != NULL)
	{
		ogJodData.DeleteJod(prpJod->Urno);
	}

	return TRUE;
}

UndoCreateEquipmentJob::UndoCreateEquipmentJob(const JOBDATA *prpJob, const JODDATA *prpJod)
{
	bmIsThisJobHasJod = (prpJod != NULL);
	rmJob = *prpJob;
	if (bmIsThisJobHasJod)
	{
		rmJod = *prpJod;
	}

	CString olFlight = "?", olEqu = "?";
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prpJob->Flur);
	if(prlFlight != NULL)
	{
		olFlight = prlFlight->Fnum;
	}
	EQUDATA *prlEqu = ogEquData.GetEquByUrno(prpJob->Uequ);
	if(prlEqu != NULL)
	{
		olEqu = prlEqu->Enam;
	}

	// Assignment of flight %s to equipment %s
	CString olText;
	olText.Format(GetString(IDS_UNDOEQTEXT),olFlight,olEqu);
	omUndoText.Format("(%s-%s) %s",prpJob->Acfr.Format("%H%M"),prpJob->Acto.Format("%H%M"),olText);
	ogOfflineDescription.SetOfflineDescription(prpJob->Urno, omUndoText);
}

CString UndoCreateEquipmentJob::UndoText() const
{
	return omUndoText;
}

BOOL UndoCreateEquipmentJob::Undo(CWnd *popParentWnd)
{
	return ::DeleteEquipmentJob(popParentWnd, omUndoText, &rmJob, (bmIsThisJobHasJod? &rmJod: NULL));
}

BOOL UndoCreateEquipmentJob::Redo(CWnd *popParentWnd)
{
	return ::CreateEquipmentJob(popParentWnd, omUndoText, &rmJob, (bmIsThisJobHasJod? &rmJod: NULL));
}

/////////////////////////////////////////////////////////////////////////////
// UndoCreateEquipmentFastLink

static BOOL CreateEquipmentFastLinkJob(CWnd *popParentWnd, const CString &ropUndoText,	JOBDATA *prpJob)
{
	ogJobData.AddJob(prpJob,"UndoEFLJob");
	return TRUE;
}

static BOOL DeleteEquipmentFastLinkJob(CWnd *popParentWnd, const CString &ropUndoText,	JOBDATA *prpJob)
{
	// Before undoing or redoing, we should check first if this job is still exist.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(prpJob->Urno);
	if(!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,ropUndoText);
		return FALSE;
	}
	else if(IsDifferentJob(prlJob,prpJob))
	{
		// "Error: This Duty has already been deleted (possibly as a result of a previous Undo/Redo)."
		// "Please undo the deletion before attempting to delete it."
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),ropUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	ogJobData.DeleteJob(prpJob->Urno);
	return TRUE;
}

UndoCreateEquipmentFastLink::UndoCreateEquipmentFastLink(const JOBDATA *prpJob)
{
	rmJob = *prpJob;

	CString olEmp = "?", olEqu = "?";
	EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prpJob->Ustf);
	if(prlEmp != NULL)
	{
		olEmp = ogEmpData.GetEmpName(prlEmp);
	}
	EQUDATA *prlEqu = ogEquData.GetEquByUrno(prpJob->Uequ);
	if(prlEqu != NULL)
	{
		olEqu = prlEqu->Enam;
	}

	// IDS_UNDOEFLTEXT Fast link between employee %s and equipment %s
	CString olText;
	olText.Format(GetString(IDS_UNDOEFLTEXT),olEmp,olEqu);
	omUndoText.Format("(%s-%s) %s",prpJob->Acfr.Format("%H%M"),prpJob->Acto.Format("%H%M"),olText);
	ogOfflineDescription.SetOfflineDescription(prpJob->Urno, omUndoText);
}

CString UndoCreateEquipmentFastLink::UndoText() const
{
	return omUndoText;
}

BOOL UndoCreateEquipmentFastLink::Undo(CWnd *popParentWnd)
{
	return ::DeleteEquipmentFastLinkJob(popParentWnd, omUndoText, &rmJob);
}

BOOL UndoCreateEquipmentFastLink::Redo(CWnd *popParentWnd)
{
	return ::CreateEquipmentFastLinkJob(popParentWnd, omUndoText, &rmJob);
}


/////////////////////////////////////////////////////////////////////////////
// UndoEmpAbsent

UndoEmpAbsent::UndoEmpAbsent(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob)
{
	rmOldJob = *prpOldJob;
	rmNewJob = *prpNewJob;
	omUndoText.Format(GetString(IDS_UNDO_EMPABSENT),ogEmpData.GetEmpName(prpOldJob->Ustf));
	ogOfflineDescription.SetOfflineDescription(prpOldJob->Urno, omUndoText);
}

CString UndoEmpAbsent::UndoText() const
{
	return omUndoText;
}

BOOL UndoEmpAbsent::Undo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still confirmed.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmNewJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Infm != rmNewJob.Infm)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"ist nicht mehr best�tigt (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Infm = rmOldJob.Infm;
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "UndoEmpAbsent");
	}
}

BOOL UndoEmpAbsent::Redo(CWnd *popParentWnd)
{
	// Before undoing or redoing, we should check first if this job is still ready to confirm.
	JOBDATA *prlJob = ogJobData.GetJobByUrno(rmOldJob.Urno);
	if (!prlJob)
	{
		DisplayJobDeletedMsg(popParentWnd,omUndoText);
		return FALSE;
	}
	else if(!ogDataSet.CheckJobForClockOut(popParentWnd, prlJob))
	{
		return FALSE;
	}
	else if (prlJob->Infm != rmOldJob.Infm)
	{
		CString olTxt;
		olTxt.Format(GetString(IDS_STRING61415),omUndoText);
		popParentWnd->MessageBox(olTxt,NULL, MB_OK | MB_ICONEXCLAMATION);
//		popParentWnd->MessageBox(CString("Error: Dieser Einsatz\n")
//			+ "(" + omUndoText + ")\n" +
//			"ist nicht mehr im Planungs-Status (m�glicherweise verursacht durch vorherige Undo/Redo Aktion).\n",
//			NULL, MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}
	else
	{
		JOBDATA rlOldJob = *prlJob;
		prlJob->Infm = rmNewJob.Infm;
		return ogJobData.ChangeJobData(prlJob, &rlOldJob, "RedoEmpInf");
	}
}

COfflineDescription::COfflineDescription(void)
{
}

COfflineDescription::~COfflineDescription(void)
{
	ClearAll();
}

void COfflineDescription::SetOfflineDescription(long lpUrno, CString &ropDescription)
{
	if(!bgOnline)
	{
		OFFLINE_DESCRIPTION *prlOD = new OFFLINE_DESCRIPTION;
		prlOD->Time = ogBasicData.GetTime();
		prlOD->Description = ropDescription;
		prlOD->Urno = lpUrno;
		omOfflineDescriptions.SetAt((void *) lpUrno, prlOD);
	}
}

void COfflineDescription::ClearAll(void)
{
	long llUrno;
	OFFLINE_DESCRIPTION *prlOD = NULL;
	for(POSITION rlPos = omOfflineDescriptions.GetStartPosition(); rlPos != NULL; )
	{
		omOfflineDescriptions.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlOD);
		delete prlOD;
	}
	omOfflineDescriptions.RemoveAll();
}

static int ByTime(const OFFLINE_DESCRIPTION **pppOD1, const OFFLINE_DESCRIPTION **pppOD2);
static int ByTime(const OFFLINE_DESCRIPTION **pppOD1, const OFFLINE_DESCRIPTION **pppOD2)
{
	return (int)((**pppOD2).Time.GetTime() - (**pppOD1).Time.GetTime());
}

void COfflineDescription::GetOfflineDescriptions(CCSPtrArray <OFFLINE_DESCRIPTION> &ropOfflineDescriptions, CStringArray &ropErrors)
{
	CUIntArray olUrnos;
	ogJodData.GetJodsChangedOffline(olUrnos);
	ogJobData.GetJobsChangedOffline(olUrnos);
	OFFLINE_DESCRIPTION *prlOD = NULL;
	CString olError;

	int ilNumUrnos = olUrnos.GetSize();
	for(int ilU = 0; ilU < ilNumUrnos; ilU++)
	{
		if((prlOD = GetOfflineDescription(olUrnos[ilU])) != NULL)
		{
			ropOfflineDescriptions.Add(prlOD);
		}
		else
		{
//			olError.Format("**ERROR** Description for URNO <%ld> not found",olUrnos[ilU]);
//			ropErrors.Add(olError);
		}
	}

	ropOfflineDescriptions.Sort(ByTime);
}

OFFLINE_DESCRIPTION *COfflineDescription::GetOfflineDescription(long lpUrno)
{
	OFFLINE_DESCRIPTION *prlOD = NULL;
	omOfflineDescriptions.Lookup((void *)lpUrno, (void *&) prlOD);
	return prlOD;
}
