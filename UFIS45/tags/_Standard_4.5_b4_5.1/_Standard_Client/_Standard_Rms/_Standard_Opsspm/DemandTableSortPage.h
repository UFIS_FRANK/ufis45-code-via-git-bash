// DemandTableSortPage.h : header file
//
#ifndef _DEMANDTBSO_H_
#define _DEMANDTBSO_H_
/////////////////////////////////////////////////////////////////////////////
// DemandTableSortPage dialog

class DemandTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(DemandTableSortPage)

// Construction
public:
	DemandTableSortPage();
	~DemandTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;

	//{{AFX_DATA(DemandTableSortPage)
	enum { IDD = IDD_DEMANDTABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(DemandTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(DemandTableSortPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnDemEndDirection();
	afx_msg void OnDemStartDirection();
	afx_msg void OnDemTerminalDirection();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
	CBitmap omUpArrow, omDownArrow;
	bool SetDirectionButton(int ipButtonId, bool bpAscending);

public:
	bool bmDemEndAscending, bmDemStartAscending ,bmDemTerminalAscending;

};

#endif // _DEMANDTBSO_H_
