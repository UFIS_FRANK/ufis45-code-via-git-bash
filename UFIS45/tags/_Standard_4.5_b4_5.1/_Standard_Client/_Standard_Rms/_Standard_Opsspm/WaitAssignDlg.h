#if !defined(AFX_WAITASSIGNDLG_H__778002E0_0587_11D4_AC66_00010204C769__INCLUDED_)
#define AFX_WAITASSIGNDLG_H__778002E0_0587_11D4_AC66_00010204C769__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaitAssignDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WaitAssignDlg dialog

class WaitAssignDlg : public CDialog
{
// Construction
public:
	WaitAssignDlg(CWnd* pParent = NULL, CString csText = "");   // standard constructor
	~WaitAssignDlg();

	static void WaitAssignDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void ProcessAflEnd(void);

	void SetAssignCount(DWORD upCount);
	void SetDeleteCount(DWORD upCount);

// Dialog Data
	//{{AFX_DATA(WaitAssignDlg)
	enum { IDD = IDD_WAIT_DIALOG };
	CAnimateCtrl	m_oAnimate;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WaitAssignDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString		m_csText;
	BOOL		bmLoading;
	CString		omTitle;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(WaitAssignDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAITASSIGNDLG_H__778002E0_0587_11D4_AC66_00010204C769__INCLUDED_)
