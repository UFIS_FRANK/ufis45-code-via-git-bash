// staffdia.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSCedaCom.h>
#include <cxbutton.h>
#include <clientwn.h>
#include <tscale.h>
#include <TimeScaleDlg.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CCSDragDropCtrl.h>
#include <cviewer.h>
#include <StaffViewer.h>
#include <StaffGantt.h>
#include <StaffChart.h>
#include <StaffDiagram.h>

#include <FilterPage.h>
#include <StaffDiagramSortPage.h>
#include <BasePropertySheet.h>
#include <StaffDiagramGroupPage.h>
#include <StaffDiagramPropSheet.h>

#include <BasicData.h>
#include <CedaFlightData.h>
#include <conflict.h>
#include <ccsddx.h>
#include <CedaCfgData.h>

#include <CCITable.h>
#include <GateTable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <GateDiagram.h>
#include <CCIDiagram.h>
#include <RegnDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <PrintOrExportToExcel.h>


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const CString STAFFDIA = "StaffDia"; //Singapore

// Prototypes
static void StaffDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// StaffDiagram

IMPLEMENT_DYNCREATE(StaffDiagram, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// limitation on selction of Group count
#define GROUPCOUNT_LIMIT 1000

StaffDiagram::StaffDiagram(void)
{
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	StaffDiagram(FALSE,ogBasicData.GetTime());
	//StaffDiagram(FALSE,ogBasicData.GetTimeframeStart()); // TIMEFRAME
	omTimeBandStartTime = TIMENULL;
	omTimeBandEndTime = TIMENULL;
	m_bAutoMenuEnable = true;
	bmScrolling = false;
	m_wndBreakTable = NULL;
}

StaffDiagram::StaffDiagram(BOOL bpPrePlanMode,CTime opPrePlanTime)
{
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	omViewer.SetViewerKey(STAFFDIA);
    imStartTimeScalePos = 200;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;

	omPrePlanTime = opPrePlanTime;
	omPrePlanMode = bpPrePlanMode;
	m_wndBreakTable = NULL;
	pomTemplate = NULL; //PRF 8704

	bmIsViewOpen = FALSE;

	CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.STCH,ogCfgData.rmUserSetup.MONS);
	/*
	ASSERT(Create(NULL, "", WS_HSCROLL | WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME |
		WS_VISIBLE,	olRect,NULL,NULL,0,NULL));
	*/

	//WS_VISIBLE is commented out to avoid double screen display while resizing - PRF 8712
	olRect.top += 40;
    ASSERT(Create(NULL, "", WS_HSCROLL | WS_OVERLAPPEDWINDOW /*| WS_VISIBLE*/ | WS_POPUP,
		olRect,pogMainWnd,NULL,0,NULL));

	SetCaptionText();

	omTimeBandStartTime = TIMENULL;
	omTimeBandEndTime = TIMENULL;
	m_bAutoMenuEnable = true;
	bmScrolling = false;

	BOOL blMinimized = FALSE;	
	CRect olTempRect;
	ogBasicData.GetWindowPosition(olTempRect,ogCfgData.rmUserSetup.STCH,ogCfgData.rmUserSetup.MONS);
	ogBasicData.GetDialogFromReg(olTempRect, COpssPmApp::EMPLOYEE_DIAGRAM_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olTempRect);

	SetWindowPos(&wndTop, olTempRect.left,olTempRect.top, olTempRect.Width(), olTempRect.Height(), SWP_SHOWWINDOW);
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

void StaffDiagram::SetAllStaffAreaButtonsColor(void)
{
	int ilGroupCount = omViewer.GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		SetStaffAreaButtonColor(ilGroupno);
	}
}

void StaffDiagram::SetStaffAreaButtonColor(int ipGroupno)
{
	if(ipGroupno >= omPtrArray.GetSize())
		return;

	// check if there are conflicts in the next 3 hours
	// if so, set the group button to the relevant colour
	CTime ilStart = ogBasicData.GetTime();
	CTime ilEnd = ilStart + CTimeSpan(0,3,0,0);
	StaffChart *polStaffChart;
	int ilColorIndex;

	ilColorIndex = omViewer.GetGroupColorIndex(ipGroupno,ilStart,ilEnd);
	polStaffChart = (StaffChart *) omPtrArray.GetAt(ipGroupno);
	if (polStaffChart != NULL)
	{
		CCSButtonCtrl *prlButton =polStaffChart->GetChartButtonPtr();
		if (prlButton != NULL)
		{
			if (ilColorIndex == FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2))
			{
				prlButton->SetColors(::GetSysColor(COLOR_BTNFACE),
					::GetSysColor(COLOR_BTNSHADOW),
					::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
			else
			{
				prlButton->SetColors(ogColors[ilColorIndex],
					::GetSysColor(COLOR_BTNSHADOW),
					::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
	}
}

StaffDiagram::~StaffDiagram()
{
    // all CFrameWnd child dies automatically
	//TRACE("StaffDiagram::~StaffDiagram\n");
	/*
	for(int ilLc = 0; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		delete omPtrArray[ilLc];
	}
	*/
    omPtrArray.RemoveAll();
}

void StaffDiagram::DoDataExchange(CDataExchange* pDX)
{
	CFrameWnd::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StaffDiagram)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(StaffDiagram, CFrameWnd)
    //{{AFX_MSG_MAP(StaffDiagram)
    ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
    ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BUTTON_PRINT_PREVIEW,OnPrintPreview) //PRF 8704
	ON_MESSAGE(WM_BEGIN_PRINTING, OnBeginPrinting) //PRF 8704
	ON_MESSAGE(WM_END_PRINTING, OnEndPrinting) //PRF 8704
	ON_MESSAGE(WM_PRINT_PREVIEW, PrintPreview) //PRF 8704
	ON_MESSAGE(WM_PREVIEW_PRINT, OnPreviewPrint) //PRF 8704
    ON_BN_CLICKED(IDC_PREV, OnNextChart)
    ON_BN_CLICKED(IDC_NEXT, OnPrevChart)
    ON_BN_CLICKED(IDC_ARRIVAL, OnArrival)
    ON_BN_CLICKED(IDC_DEPARTURE, OnDeparture)
    ON_BN_CLICKED(IDC_MABSTAB, OnMabstab)
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_MESSAGE(WM_REMEMBERLISTPOSITIONS, OnRememberListPositions)
    ON_MESSAGE(WM_RESTORELISTPOSITIONS, OnRestoreListPositions)
    ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_BN_CLICKED(IDC_HIDETEAMDELEGATIONS, OnHideTeamDelegations)
    ON_BN_CLICKED(IDC_EXPANDGROUP, OnExpandTeam)
    ON_BN_CLICKED(IDC_COMPRESSGROUP, OnCompressTeam)
    ON_BN_CLICKED(IDC_BREAKTABLE, OnBreakTable)
	ON_MESSAGE(WM_BREAKTABLE_EXIT, OnBreakTableExit)
	ON_UPDATE_COMMAND_UI(IDC_ZEIT,OnUpdateUIZeit)
	ON_UPDATE_COMMAND_UI(IDC_MABSTAB,OnUpdateUIMabstab)
	ON_UPDATE_COMMAND_UI(IDC_PRINT,OnUpdateUIPrint)
	ON_UPDATE_COMMAND_UI(IDC_ASSIGN,OnUpdateUIAssign)
	ON_UPDATE_COMMAND_UI(IDC_ANSICHT,OnUpdateUIAnsicht)
	ON_UPDATE_COMMAND_UI(IDC_VIEW,OnUpdateUIView)
    ON_MESSAGE(WM_SELECTDIAGRAM, OnSelectDiagram)
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void StaffDiagram::SetCaptionText(void)
{
	if (omPrePlanMode)
	{
		//omCaptionText = "Mitarbeiter Diagramm - VORPLANUNG f�r " + omPrePlanTime.Format("%d%m%y  ");
		omCaptionText = GetString(IDS_STRING61326) + omPrePlanTime.Format("%d%m%y  ");
	}
	else
	{
		//omCaptionText = CString("Mitarbeiter Diagramm - ");
		omCaptionText = CString(GetString(IDS_STRING61327));
	}
//	if (bgOnline)
//	{
//		//omCaptionText += "  Online";
//		omCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//omCaptionText += "  OFFLINE";
//		omCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}
	SetWindowText(omCaptionText);
}


void StaffDiagram::PrePlanMode(BOOL bpToSet,CTime opPrePlanTime)
{
	if (bpToSet)
	{
		omPrePlanTime = opPrePlanTime;
		omPrePlanMode = TRUE;

		SetTSStartTime(omPrePlanTime);
		omTimeScale.SetDisplayStartTime(omTSStartTime);
		omTimeScale.Invalidate(TRUE);
		omClientWnd.Invalidate(FALSE);

		// update scroll bar position
		long llTotalMin = CalcTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
		ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
	}
	else
	{
		omPrePlanTime = opPrePlanTime;
		omPrePlanMode = FALSE;
	}

	SetCaptionText();

	// Id 30-Sep-96
	// Force the diagram to redisplay data again when switch to and from preplan mode.
	// This will fix the bug that the diagram confuse the time and need refreshing by a HScroll.
//	ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
	// **************** switch only if PreplanMode is enabled

}

void StaffDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    StaffChart *polChart;

    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
        polChart = (StaffChart *) omPtrArray.GetAt(ilIndex);

        if (ilIndex < imFirstVisibleChart)
        {
            polChart->ShowWindow(SW_HIDE);
            continue;
        }

        polChart->GetClientRect(&olChartRect);
        olChartRect.right = olRect.right;

        olChartRect.top = ilLastY;

		//Singapore
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			if(ilIndex == omPtrArray.GetSize() - 1)
			{
                ilLastY += polChart->GetHeight();
			}
			else if(polChart->GetHeight() >= olRect.Height()/2)
			{
				BOOL blExtraCover = TRUE;				
				CRect olTempRect(0,ilLastY,0,olRect.Height()/2);
				for(int j = ilIndex+1; j < omPtrArray.GetSize(); j++)
				{
					StaffChart* polTempChart = (StaffChart *) omPtrArray.GetAt(j);
					if(!((polChart->GetHeight() < olRect.Height()) && ((polTempChart->GetGanttPtr()->GetCount() == 0) || (polTempChart->GetState() == Minimized))))
					{
						blExtraCover = FALSE;
					}
					if(olTempRect.bottom <= olRect.bottom)
					{
						olTempRect.bottom += polTempChart->GetHeight();
					}
					else
					{
						break;
					}
				}
				
				if(blExtraCover == TRUE)
				{
					ilLastY += polChart->GetHeight();
				}
				else
				{
					if(olTempRect.bottom <= olRect.bottom)
					{
						if(polChart->GetHeight() <= olRect.Height())
						{
							ilLastY += polChart->GetHeight();
						}
						else
						{
							ilLastY += (olRect.Height()/2 + olRect.Height() - olTempRect.Height());
						}
					}
					else
					{
				ilLastY += olRect.Height()/2;
			}
				}
			}
			else
			{
				ilLastY += polChart->GetHeight();
			}
		}
		else
		{
			ilLastY += polChart->GetHeight();
		}
        olChartRect.bottom = ilLastY;

        // check
        if ((polChart->GetState() != Minimized) &&
			(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
        {
            olChartRect.bottom = olRect.bottom;
            polChart->SetState(Normal);
        }
        //
        polChart->MoveWindow(&olChartRect, FALSE);
        polChart->ShowWindow(SW_SHOW);
	}

	omClientWnd.Invalidate(TRUE);
	SetAllStaffAreaButtonsColor();
	OnUpdatePrevNext();
	UpdateTimeBand();
}

void StaffDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

CListBox *StaffDiagram::GetBottomMostGantt()
{
	// Check the size of the area for displaying charts.
	// Pichet used "omClientWnd" not the diagram itself, so we will get the size of this window
	CRect olClientRect;
	omClientWnd.GetClientRect(olClientRect);
	omClientWnd.ClientToScreen(olClientRect);

	// Searching for the bottommost chart
	StaffChart *polChart;
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		polChart = (StaffChart *)omPtrArray[ilLc];
		CRect olRect, olChartRect;
		polChart->GetClientRect(olChartRect);
		polChart->ClientToScreen(olChartRect);
		if (!olRect.IntersectRect(&olChartRect, &olClientRect))
			break;
	}

	// Check if the chart we have found is a valid one
	--ilLc;
	if (!(0 <= ilLc && ilLc <= omPtrArray.GetSize()-1))
		return NULL;

	return &((StaffChart *)omPtrArray[ilLc])->omGantt;
}

/////////////////////////////////////////////////////////////////////////////
// StaffDiagram message handlers

int StaffDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;


    omViewer.Attach(this);
    CRect olRect; GetClientRect(&olRect);


    // height is 20 point
    omDialogBar.Create(this, IDD_STAFFDIAGRAM, CBRS_TOP, IDD_STAFFDIAGRAM);


	UpdateComboBox();


	CTime olCurrentTime = ogBasicData.GetTime();
    CTime olCurrentUtcTime = ogBasicData.GetUtcTime();
	CTime olTimeframeStart = ogBasicData.GetTimeframeStart();
	CTime olTimeframeEnd = ogBasicData.GetTimeframeEnd();

	CTimeScaleDialog* polTSD = NULL;
	if(pogButtonList->pomMapDiagramToScaleDlg->Lookup(STAFFDIA,(CObject*&)polTSD) == FALSE)
	{
	    // the current time window displayed
	    omTSDuration = CTimeSpan(0, 8, 0, 0);
	    // intervals on the timescale
        omTSInterval = CTimeSpan(0, 0, 10, 0);
	    // start time of the whole gantt chart (ie not just the time window currently displayed)
        omStartTime = olTimeframeStart;
	    // duration of the whole gantt chart --> minimum 8 hours (+1 minute to stop divide by zero)
        omDuration = max(olTimeframeEnd - olTimeframeStart,omTSDuration)+CTimeSpan(0,0,1,0);
	    // the display start time (typically: current time - 1 hour)
	    omTSStartTime = ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
	}
	else
	{	// the current time window displayed
		omTSDuration = CTimeSpan(0,polTSD->m_Hour,0,0);
		// intervals on the timescale
		omTSInterval = polTSD->omTSI;
		// start time of the whole gantt chart (ie not just the time window currently displayed)
		omStartTime = olTimeframeStart;
		// duration of the whole gantt chart --> minimum 8 hours (+1 minute to stop divide by zero)
		omDuration = max(olTimeframeEnd - olTimeframeStart,omTSDuration)+CTimeSpan(0,0,1,0);
		// the display start time (typically: current time - 1 hour)
		omTSStartTime = polTSD->m_TimeScale;
	}

    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olCurrentTime.GetHour(), olCurrentTime.GetMinute(),
        olCurrentUtcTime.GetHour(), olCurrentUtcTime.GetMinute()
    );
    omTime.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 172, 6, olRect.right - 92, 23), this);

    sprintf(olBuf, "%02d%02d%02d",
        olCurrentTime.GetDay(), olCurrentTime.GetMonth(), olCurrentTime.GetYear() % 100);
    omDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 6, olRect.right - 8, 23), this);

    sprintf(olBuf, "%02d%02d%02d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 42, ilPos + 80, 59), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 34, olRect.right - (36 + 2), 68), this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 70;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);

   	if(bgIsPreplanMode)
	{
		SetTSStartTime(omPrePlanTime);
		omTimeScale.SetDisplayStartTime(omTSStartTime);
	}

    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = CalcTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

	SetTimeBand(ogBasicData.omTimebandStart, ogBasicData.omTimebandEnd);

    StaffChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);

    imFirstVisibleChart = 0;
	OnUpdatePrevNext();
    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);

        polChart = new StaffChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
        polChart->Create(NULL, "StaffChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);

        omPtrArray.Add(polChart);

		//Singapore
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			CRect olClientRect;
			omClientWnd.GetClientRect(&olClientRect);
			if(ilI == omViewer.GetGroupCount() - 1)
			{
                ilLastY += polChart->GetHeight();
			}
			else if(polChart->GetHeight() >= olClientRect.Height()/2)
			{
				ilLastY += olClientRect.Height()/2;
			}
			else
			{
				ilLastY += polChart->GetHeight();
			}
		}
		else
		{
			ilLastY += polChart->GetHeight();
		}
    }

    OnTimer(0);

	// Register DDX call back function
	TRACE("StaffDiagram: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),CString("Redisplay all"), StaffDiagramCf);	// for what-if changes
	ogCCSDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),CString("Update Time Band"), StaffDiagramCf);	// for updating the yellow lines
	ogCCSDdx.Register(this, PREPLAN_DATE_UPDATE, CString("STAFFDIAGRAM"),CString("Preplan Date Change"), StaffDiagramCf); // change the date of the data displayed
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("STAFFDIAGRAM"),CString("Global Date Change"), StaffDiagramCf); // change the date of the data displayed Singapore


	//ogCCSDdx.Register(this, JOB_NEW,CString("STAFFDIAGRAM"), CString("Job New"), StaffDiagramCf);
	//ogCCSDdx.Register(this, JOB_CHANGE,CString("STAFFDIAGRAM"), CString("Job Changed"), StaffDiagramCf);
	ogCCSDdx.Register(this, JOB_DELETE,CString("STAFFDIAGRAM"), CString("Job Deleted"), StaffDiagramCf);

	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CWnd *polWnd = omDialogBar.GetDlgItem(IDC_ANSICHT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = omDialogBar.GetDlgItem(IDC_MABSTAB);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61842));
	}
	polWnd = omDialogBar.GetDlgItem(IDC_ZEIT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32896));
		if(olCurrentTime < ogBasicData.GetTimeframeStart() || olCurrentTime > ogBasicData.GetTimeframeEnd())
		{
			// cannot set the gantt to the current local time because it is outside of the timeframe
			polWnd->EnableWindow(FALSE);
		}
	}
	polWnd = omDialogBar.GetDlgItem(IDC_PRINT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}

	polWnd = omDialogBar.GetDlgItem(IDC_EXPANDGROUP);
	if(polWnd != NULL)
	{
		if(ogBasicData.bmDisplayTeams)
		{
			polWnd->SetWindowText(GetString(IDS_EXPANDGROUP));
		}
		else
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}

	polWnd = omDialogBar.GetDlgItem(IDC_COMPRESSGROUP);
	if(polWnd != NULL)
	{
		if(ogBasicData.bmDisplayTeams)
		{
			polWnd->SetWindowText(GetString(IDS_CONTRACTGROUP));
		}
		else
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}

	CButton *polButton = (CButton *) omDialogBar.GetDlgItem(IDC_HIDETEAMDELEGATIONS);
	if(polButton != NULL)
	{
		if(ogBasicData.bmDisplayTeams)
		{
			polButton->SetWindowText(GetString(IDS_HIDEDELEGATEDTEAMS));
		}
		else
		{
			polButton->ShowWindow(SW_HIDE);
		}
	}

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pom_Date = (CComboBox*) omDialogBar.GetDlgItem(IDC_DATE);

		if(pom_Date != NULL)
		{
			pom_Date->ShowWindow(SW_SHOW);
			pom_Date->EnableWindow();
			CStringArray olTimeframeList;
			ogBasicData.GetTimeframeList(olTimeframeList);
			int ilNumDays = olTimeframeList.GetSize();
			for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
			{
				pom_Date->AddString(olTimeframeList[ilDay]);
			}
			if(ilNumDays > 0)
			{
				if(omStartTime == TIMENULL)
				{
					pom_Date->SetCurSel(ogBasicData.GetDisplayDateOffset());
				}
				else
				{
					pom_Date->SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(ogBasicData.GetDisplayDate()));
				}
			}
		}
	}
	else
	{
		pom_Date = (CComboBox*) omDialogBar.GetDlgItem(IDC_DATE);
		if(pom_Date != NULL)
		{
			pom_Date->ShowWindow(SW_HIDE);
		}
	}

    m_DragDropTarget.RegisterTarget(this, this);

    return 0;
}

void StaffDiagram::OnDestroy()
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::EMPLOYEE_DIAGRAM_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogCCSDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("StaffDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	m_DragDropTarget.Revoke();
	CFrameWnd::OnDestroy();
}

void StaffDiagram::OnClose()
{
	if (m_wndBreakTable)
	{
		m_wndBreakTable->DestroyWindow();
		m_wndBreakTable = NULL;
	}

   	pogButtonList->m_wndStaffDiagram = NULL;
	pogButtonList->m_StaffDiagramButton.Recess(FALSE);
    CFrameWnd::OnClose();
}

void StaffDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL StaffDiagram::OnEraseBkgnd(CDC* pDC)
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);

    return TRUE;

    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void StaffDiagram::OnPaint()
{
    CPaintDC dc(this); // device context for painting

    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);

    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 33); dc.LineTo(olRect.right, 33);
    dc.MoveTo(olRect.left, 68); dc.LineTo(olRect.right, 68);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 69); dc.LineTo(olRect.right, 69);


    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 33);
    dc.LineTo(imStartTimeScalePos - 2, 69);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 33);
    dc.LineTo(imStartTimeScalePos - 1, 69);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void StaffDiagram::OnSize(UINT nType, int cx, int cy)
{
    CFrameWnd::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 33 + 5, olRect.right - 19, (33 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);

    CRect olBB2Rect(olRect.right - 19, 33 + 5, olRect.right - 2, (33 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 34, olRect.right - (36 + 2), 68);
    omTimeScale.MoveWindow(&olTSRect, FALSE);

    olRect.top += 70;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	//PositionChild();
	ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
	SetAllStaffAreaButtonsColor();

	ogBasicData.SetWindowStat("STAFFDIAGRAM IDC_ANSICHT",omDialogBar.GetDlgItem(IDC_ANSICHT));

   if (nType != SIZE_MINIMIZED)
   {
	   GetWindowRect(&omWindowRect);
   }
}

void StaffDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default

    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);

    long llTotalMin;
    int ilPos;

    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");

            llTotalMin = CalcTotalMinutes();
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
            SetScrollPos(SB_HORZ, ilPos, TRUE);
			SelectComboDate(omTSStartTime); //Singapore
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////

        break;

        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = CalcTotalMinutes();
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
			SetScrollPos(SB_HORZ, ilPos, TRUE);
			SelectComboDate(omTSStartTime); //Singapore
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;

        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");

            llTotalMin = CalcTotalMinutes();
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
			SelectComboDate(omTSStartTime); //Singapore
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;

        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");

            llTotalMin = CalcTotalMinutes();
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);

            SetScrollPos(SB_HORZ, ilPos, TRUE);
			SelectComboDate(omTSStartTime); //Singapore
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
            omClientWnd.Invalidate(FALSE);
////////////////////////////////////////////////////////////////////////
        break;

        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = CalcTotalMinutes();

            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
			////ChangeViewTo(omViewer.SelectView());

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);

            SetScrollPos(SB_HORZ, nPos, TRUE);
			SelectComboDate(omTSStartTime); //Singapore

            //char clBuf[64];
            //wsprintf(clBuf, "Thumb Position : %d", nPos);
            //omStatusBar.SetWindowText(clBuf);
        break;

        //case SB_THUMBPOSITION /* released */:
            //OutputDebugString("ThumbPosition\n\r");
        //break;
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This thing should be remove. The technic has been changed to use the
// method Invalidate() not only for the TimeScale, but for the entire
// omClientWnd also.
        case SB_THUMBPOSITION:	// the thumb was just released?
			ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
            omClientWnd.Invalidate(FALSE);

			return;
////////////////////////////////////////////////////////////////////////

        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;

        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }
}

void StaffDiagram::OnPrevChart()
{
    if (imFirstVisibleChart > 0)
    {
        imFirstVisibleChart--;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void StaffDiagram::OnNextChart()
{
    if (imFirstVisibleChart < omPtrArray.GetUpperBound())
    {
        imFirstVisibleChart++;
		OnUpdatePrevNext();
        PositionChild();
    }
}

void StaffDiagram::OnFirstChart()
{
	imFirstVisibleChart = 0;
	OnUpdatePrevNext();
	PositionChild();
}

void StaffDiagram::OnLastChart()
{
	imFirstVisibleChart = omPtrArray.GetUpperBound();
	OnUpdatePrevNext();
	PositionChild();
}

void StaffDiagram::OnTimer(UINT nIDEvent)
{
	if(bmNoUpdatesNow == FALSE)
	{
		if(nIDEvent == 0)
		{
			char clBuf[16];
			CTime olCurrentTime = ogBasicData.GetTime();
			omTimeScale.UpdateCurrentTimeLine(olCurrentTime);

			CTime olCurrentUtcTime = ogBasicData.GetUtcTime();
			sprintf(clBuf, "%02d%02d/%02d%02dz",
				olCurrentTime.GetHour(), olCurrentTime.GetMinute(),
				olCurrentUtcTime.GetHour(), olCurrentUtcTime.GetMinute());
			omTime.SetWindowText(clBuf);
			omTime.Invalidate(FALSE);

			sprintf(clBuf, "%02d%02d%02d",
				olCurrentTime.GetDay(), olCurrentTime.GetMonth(), olCurrentTime.GetYear() % 100);
			omDate.SetWindowText(clBuf);
			omDate.Invalidate(FALSE);
			//

			SetAllStaffAreaButtonsColor();

			StaffChart *polChart;
			for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
			{
				polChart = (StaffChart *) omPtrArray.GetAt(ilIndex);
				polChart->GetGanttPtr()->SetCurrentTime(olCurrentTime);
			}
		}
		else if(nIDEvent == AUTOSCROLL_TIMER_EVENT)
		{
			// called during drag&drop, does automatic scrolling when the cursor
			// is outside the main gantt chart
			OnAutoScroll();
		}

		CFrameWnd::OnTimer(nIDEvent);
	}
}

LONG StaffDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG StaffDiagram::OnRememberListPositions(WPARAM wParam, LPARAM lParam)
{
	RememberListPositions();
	return 0L;
}

LONG StaffDiagram::OnRestoreListPositions(WPARAM wParam, LPARAM lParam)
{
	RestoreListPositions();
	return 0L;
}

// updates that cause lines to be deleted result in the display springing up to
// the top, to rectify this the top index of the list box was remembered and reset
// after the deletion - using the RememberListPositions() and RestoreListPositions()
// the top index will not be reset for each delete but rather at the end of a block
// of deletions eg in ChangeViewTo()
void StaffDiagram::RememberListPositions()
{
	omTopIndexList.RemoveAll();
	for(int ilGroupno = 0; ilGroupno < omPtrArray.GetSize(); ilGroupno++)
	{
		StaffChart *polStaffChart = (StaffChart *) omPtrArray.GetAt(ilGroupno);
		if(polStaffChart == NULL) return;
		StaffGantt *polStaffGantt = polStaffChart -> GetGanttPtr();
		if(polStaffGantt == NULL) return;
		omTopIndexList.Add(polStaffGantt->GetTopIndex());
	}
}

void StaffDiagram::RestoreListPositions()
{
	for(int ilGroupno = 0; ilGroupno < omPtrArray.GetSize(); ilGroupno++)
	{
		if(ilGroupno < omPtrArray.GetSize() && ilGroupno < omTopIndexList.GetSize())
		{
			StaffChart *polStaffChart = (StaffChart *) omPtrArray.GetAt(ilGroupno);
			if(polStaffChart == NULL) return;
			StaffGantt *polStaffGantt = polStaffChart -> GetGanttPtr();
			if(polStaffGantt == NULL) return;
			int ilTopPos = omTopIndexList.GetAt(ilGroupno);
			if(ilTopPos != polStaffGantt->GetTopIndex())
			{
				int ilMaxTopIndex = polStaffGantt->GetCount() - 1;

				if(ilTopPos < 0)
					ilTopPos = 0;
				else if(ilTopPos > ilMaxTopIndex)
					ilTopPos = ilMaxTopIndex;

				polStaffGantt->SetTopIndex(ilTopPos);
			}
		}
	}
	omTopIndexList.RemoveAll();
}

LONG StaffDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
    CString olStr;
    int ilGroupno = HIWORD(lParam);
    int ilLineno = LOWORD(lParam);

	if(wParam == UD_REPAINTVERTICALSCALES)
	{
		StaffChart *polStaffChart = NULL;
		StaffGantt *polStaffGantt = NULL;
		for(int ilGroupno = 0; ilGroupno < omPtrArray.GetSize(); ilGroupno++)
		{
			if((polStaffChart = (StaffChart *) omPtrArray.GetAt(ilGroupno)) != NULL &&
				(polStaffGantt = polStaffChart->GetGanttPtr()) != NULL)
			{
				polStaffGantt->RepaintVerticalScale(-1,true);
			}
		}
		return 0L;
	}

	if(wParam != UD_DELETEGROUP)
	{
		int ilNumGroups = omViewer.GetGroupCount();
		if(ilGroupno >= ilNumGroups)
		{
			return 0L;
		}

		if(wParam != UD_DELETELINE)
		{
			int ilNumLines = omViewer.GetLineCount(ilGroupno);
			if(ilLineno >= ilNumLines)
			{
				return 0L;
			}
		}
	}

	if(ilGroupno >= omPtrArray.GetSize()) return 0L;


	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    StaffChart *polStaffChart = (StaffChart *) omPtrArray.GetAt(ilGroupno);
	if(polStaffChart == NULL) return 0L;
    StaffGantt *polStaffGantt = polStaffChart -> GetGanttPtr();
	if(polStaffGantt == NULL) return 0L;



    switch (wParam)
    {
        // group message
        case UD_INSERTGROUP :
            // CreateChild(ilGroupno);

            if (ilGroupno <= imFirstVisibleChart)
			{
                imFirstVisibleChart++;
				OnUpdatePrevNext();
			}
            PositionChild();
        break;

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ilGroupno);
            polStaffChart->GetChartButtonPtr()->SetWindowText(olStr);
            polStaffChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ilGroupno);
            polStaffChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            polStaffChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			SetStaffAreaButtonColor(ilGroupno);
        break;

        case UD_DELETEGROUP :
			if(ilGroupno < omPtrArray.GetSize())
			{
				delete omPtrArray.GetAt(ilGroupno);
				//omPtrArray.GetAt(ilGroupno) -> DestroyWindow();
				omPtrArray.RemoveAt(ilGroupno);

				if (ilGroupno <= imFirstVisibleChart)
				{
					imFirstVisibleChart--;
					OnUpdatePrevNext();
				}
				PositionChild();
			}
        break;

        // line message
        case UD_INSERTLINE :
            polStaffGantt->InsertString(ilLineno, "");
			polStaffGantt->RepaintItemHeight(ilLineno);
			ilCount = omViewer.GetLineCount(ilGroupno);
			sprintf(clBuf, "%d", ilCount);
			polStaffChart->GetCountTextPtr()->SetWindowText(clBuf);
			polStaffChart->GetCountTextPtr()->Invalidate(TRUE);

			SetStaffAreaButtonColor(ilGroupno);
            PositionChild();
        break;

        case UD_UPDATELINE :
            polStaffGantt->RepaintVerticalScale(ilLineno);
            polStaffGantt->RepaintGanttChart(ilLineno);
			SetStaffAreaButtonColor(ilGroupno);
        break;

        case UD_DELETELINE :
		{
			int ilTopPos = polStaffGantt->GetTopIndex();
            if(polStaffGantt->DeleteString(ilLineno) != LB_ERR)
			{
				if(omTopIndexList.GetSize() <= 0 && ilTopPos < polStaffGantt->GetCount())
				{
					polStaffGantt->SetTopIndex(ilTopPos);
				}
				ilCount = omViewer.GetLineCount(ilGroupno);
				sprintf(clBuf, "%d", ilCount);
				polStaffChart->GetCountTextPtr()->SetWindowText(clBuf);
				polStaffChart->GetCountTextPtr()->Invalidate(TRUE);

				SetStaffAreaButtonColor(ilGroupno);
				PositionChild();
			}
	        break;
		}
        case UD_UPDATELINEHEIGHT :
            polStaffGantt->RepaintItemHeight(ilLineno);
			SetStaffAreaButtonColor(ilGroupno);
            PositionChild();
        break;

		case UD_PREPLANMODE :
			SetTSStartTime(olTime);
			//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);

			omClientWnd.Invalidate(FALSE);

			// update scroll bar position
			long llTotalMin = CalcTotalMinutes();
			long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
			nPos = nPos * 1000L / llTotalMin;
			SetScrollPos(SB_HORZ, int(nPos), TRUE);
		break;
    }

    return 0L;
}

void StaffDiagram::OnUpdatePrevNext(void)
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	if (imFirstVisibleChart == 0)
		GetDlgItem(IDC_NEXT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_NEXT)->EnableWindow(TRUE);

	if (imFirstVisibleChart == omPtrArray.GetUpperBound())
		GetDlgItem(IDC_PREV)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PREV)->EnableWindow(TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void StaffDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	if(polCB != NULL)
	{
		polCB->ResetContent();
		CStringArray olStrArr;
		omViewer.GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			polCB->AddString(olStrArr[ilIndex]);
		}
		CString olViewName = omViewer.GetViewName();
		if (olViewName.IsEmpty() == TRUE)
		{
			olViewName = ogCfgData.rmUserSetup.STCV;
		}

		ilIndex = polCB->FindString(-1,olViewName);

		if (ilIndex != CB_ERR)
		{
			polCB->SetCurSel(ilIndex);
		}
	}
}

void StaffDiagram::ChangeViewTo(const char *pcpViewName,BOOL RememberPositions)
{
	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);

	RememberListPositions();

	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);


	int ilGanttTopIndex = 0;	// use first item to display, if none available
	CListBox *polGantt;
	if ((polGantt = GetBottomMostGantt()) != NULL)
	{
		ilGanttTopIndex = polGantt->GetTopIndex();
	}

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();

	CRect olRect; omTimeScale.GetClientRect(&olRect);
    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	CString olPrevView = omCurrentView;
	omCurrentView = pcpViewName;


    for (int ilIndex = 0; ilIndex < omPtrArray.GetSize(); ilIndex++)
    {
		if (RememberPositions == TRUE)
		{
			int ilState = ((StaffChart *)omPtrArray[ilIndex])->GetState();
			olChartStates.NewAt(ilIndex,ilState);
		}
		// Id 30-Sep-96
		// This will remove a lot of warning message when the user change view.
		// If we just delete a staff chart, MFC will produce two warning message.
		// First, Revoke not called before the destructor.
		// Second, calling DestroyWindow() in CWnd::~CWnd.
        //delete (StaffChart *)omPtrArray.GetAt(ilIndex);
		((StaffChart *)omPtrArray[ilIndex])->DestroyWindow();
    }
    omPtrArray.RemoveAll();

    StaffChart *polChart;
    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);

	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}

	/* Limiting the Group count, PRF 6941 */
	if( GROUPCOUNT_LIMIT < omViewer.GetGroupCount())
	{
		CString olText;
		switch(omViewer.omGroupBy)
		{
		case 0:
			// Please select less than %d pools in the filter.
			olText.Format(GetString(IDS_GROUPCOUNTLIMIT_POOLS), GROUPCOUNT_LIMIT);
			MessageBox(olText, GetString(IDS_STRING61327), MB_OK);
			return ;

		case 1:
			// Please select less than %d work groups in the filter.
			olText.Format(GetString(IDS_GROUPCOUNTLIMIT_WORKGROUPS), GROUPCOUNT_LIMIT);
			MessageBox(olText, GetString(IDS_STRING61327), MB_OK);
		return ;

		case 2:
			// Please select less than %d shift codes in the filter.
			olText.Format(GetString(IDS_GROUPCOUNTLIMIT_SHIFTCODES), GROUPCOUNT_LIMIT);
			MessageBox(olText, GetString(IDS_STRING61327), MB_OK);
			return ;

		default:
			return ;
		}
	}

    int ilLastY = 0;
    for (int ilI = 0; ilI < omViewer.GetGroupCount(); ilI++)
    {
        olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);

        polChart = new StaffChart;
        polChart->SetTimeScale(&omTimeScale);
        polChart->SetViewer(&omViewer, ilI);
        polChart->SetStatusBar(&omStatusBar);
        polChart->SetStartTime(omTSStartTime);
        polChart->SetInterval(omTimeScale.GetDisplayDuration());
		if (ilI < olChartStates.GetSize())
		{
			polChart->SetState(olChartStates[ilI]);
		}
		else
		{
				polChart->SetState(Maximized);
		}

        polChart->Create(NULL, "StaffChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd,
            0 /* IDD_CHART */, NULL);

		polChart->GetGanttPtr()->SetCurrentTime(ogBasicData.GetTime());

        omPtrArray.Add(polChart);

		//Singapore
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			CRect olClientRect;
			omClientWnd.GetClientRect(&olClientRect);
			if(ilI == omViewer.GetGroupCount() -1)
			{
                ilLastY += polChart->GetHeight();
			}
			else if(polChart->GetHeight() >= olClientRect.Height()/2)
			{
				ilLastY += olClientRect.Height()/2;
			}
			else
			{
				ilLastY += polChart->GetHeight();
			}
		}
		else
		{
			ilLastY += polChart->GetHeight();
		}
    }

	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	olChartStates.DeleteAll();
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	if ((polGantt = GetBottomMostGantt()) != NULL)
	{
		polGantt->SetTopIndex(ilGanttTopIndex);
	}


	CButton *polExpandTeam = (CButton *) omDialogBar.GetDlgItem(IDC_EXPANDGROUP);
	if(polExpandTeam != NULL)
	{
		if(omViewer.bmTeamView)
		{
			polExpandTeam->ShowWindow(SW_SHOW);
			polExpandTeam->SetWindowText(GetString(IDS_EXPANDGROUP));
		}
		else
		{
			polExpandTeam->ShowWindow(SW_HIDE);
		}
	}

	CButton *polCompressTeam = (CButton *) omDialogBar.GetDlgItem(IDC_COMPRESSGROUP);
	if(polCompressTeam != NULL)
	{
		if(omViewer.bmTeamView)
		{
			polCompressTeam->ShowWindow(SW_SHOW);
			polCompressTeam->SetWindowText(GetString(IDS_CONTRACTGROUP));
		}
		else
		{
			polCompressTeam->ShowWindow(SW_HIDE);
		}
	}

	CButton *polHideTeamDeleg = (CButton *) omDialogBar.GetDlgItem(IDC_HIDETEAMDELEGATIONS);
	if(polHideTeamDeleg != NULL)
	{
		if(omViewer.bmTeamView)
		{
			polHideTeamDeleg->ShowWindow(SW_SHOW);
			polHideTeamDeleg->SetWindowText(GetString(IDS_HIDEDELEGATEDTEAMS));

			CString olCurrentView = omViewer.GetViewName();
			if(olPrevView != omCurrentView)
			{
				// for compatibility with version 1.19
				CString olHideCompletelyDelegatedTeams = omViewer.GetUserData("HIDEDELEG");
				if (olHideCompletelyDelegatedTeams.IsEmpty())
					omViewer.bmHideCompletelyDelegatedTeams = false;
				else
					omViewer.bmHideCompletelyDelegatedTeams = olHideCompletelyDelegatedTeams == "YES";
				polHideTeamDeleg->SetCheck(omViewer.bmHideCompletelyDelegatedTeams ? 1 : 0);
			}
		}
		else
		{
			polHideTeamDeleg->ShowWindow(SW_HIDE);
		}
	}
	RestoreListPositions();
}

void StaffDiagram::OnAnsicht()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	StaffDiagramPropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
		ChangeViewTo(omViewer.GetViewName());
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void StaffDiagram::OnViewSelChange()
{
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
    TRACE("StaffDiagram::OnComboBox() [%s]", clText);
	ChangeViewTo(clText);
}

void StaffDiagram::OnCloseupView()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void StaffDiagram::OnArrival()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

    static int i = 0;

    if (++i % 2)
        omArrival.Recess(TRUE);
    else
        omArrival.Recess(FALSE);

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// We just want to make sure that
//	omViewer.CreateAllData(omArrival.Recess(), omDeparture.Recess());
////////////////////////////////////////////////////////////////////////
}

void StaffDiagram::OnDeparture()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

    static int i = 0;

    if (++i % 2)
        omDeparture.Recess(TRUE);
    else
        omDeparture.Recess(FALSE);

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// We just want to make sure that
//	omViewer.CreateAllData(omArrival.Recess(), omDeparture.Recess());
////////////////////////////////////////////////////////////////////////
}

void StaffDiagram::OnMabstab()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	//Singapore
	CTimeScaleDialog* polTSD = NULL;
	if(pogButtonList->pomMapDiagramToScaleDlg->Lookup(STAFFDIA,(CObject*&)polTSD) == FALSE)
	{
		polTSD = new CTimeScaleDialog(this);
	    polTSD->pomTS = &omTimeScale;
	    polTSD->omTSI = omTSInterval;
		polTSD->m_TimeScale = omTSStartTime;
		polTSD->m_Hour = (int) omTSDuration.GetTotalHours();
		polTSD->imMaxHours = omDuration.GetTotalHours();
		pogButtonList->pomMapDiagramToScaleDlg->SetAt(STAFFDIA,polTSD);
	}
    CTimeScaleDialog olTSD(this);
    olTSD.pomTS = polTSD->pomTS;
	olTSD.omTSI = polTSD->omTSI;
	//olTSD.m_TimeScale = polTSD->m_TimeScale;
	olTSD.m_TimeScale =  CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
                atoi(polTSD->m_startTime), 0, 0);
	olTSD.m_Hour = polTSD->m_Hour;
	olTSD.imMaxHours = polTSD->imMaxHours;

    /*CTimeScaleDialog olTSD(this); //Commented Singapore
    olTSD.pomTS = &omTimeScale;
    olTSD.omTSI = omTSInterval;
    olTSD.m_TimeScale = omTSStartTime;
    //olTSD.m_TimeScale = ogBasicData.GetTime();
    olTSD.m_Hour = (int) omTSDuration.GetTotalHours();
	olTSD.imMaxHours = omDuration.GetTotalHours();*/

	if(ogStaffIndexes.Chart == MS_SANS6)
	{
	    olTSD.m_Percent = 50;
	}
	else if(ogStaffIndexes.Chart == MS_SANS8)
	{
	    olTSD.m_Percent = 75;
	}
	else if(ogStaffIndexes.Chart == MS_SANS12)
	{
	    olTSD.m_Percent = 100;
	}
    if (olTSD.DoModal() == IDOK)
    {
		//Singapore
		polTSD->pomTS = olTSD.pomTS;
		polTSD->omTSI = olTSD.omTSI;
		polTSD->m_TimeScale = olTSD.m_TimeScale;
		polTSD->m_Hour = olTSD.m_Hour;
		polTSD->imMaxHours = olTSD.imMaxHours;
		polTSD->m_startTime = olTSD.m_startTime;

        SetTSStartTime(olTSD.m_TimeScale);
        omTSDuration = CTimeSpan(0, olTSD.m_Hour, 0, 0);

        if(olTSD.m_Percent == 50)
		{
			ogStaffIndexes.VerticalScale = MS_SANS8;
			ogStaffIndexes.Chart = MS_SANS6;
		}
        else if(olTSD.m_Percent == 75)
		{
			ogStaffIndexes.VerticalScale = MS_SANS16;
			ogStaffIndexes.Chart = MS_SANS8;
		}
        else if(olTSD.m_Percent == 100)
		{
			ogStaffIndexes.VerticalScale = MS_SANS12;
			ogStaffIndexes.Chart = MS_SANS12;
		}
		else
		{
			ogStaffIndexes.VerticalScale = MS_SANS6;
			ogStaffIndexes.Chart = MS_SANS6;
		}
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);

		ChangeViewTo(ogCfgData.rmUserSetup.STCV);

		SelectComboDate(omTSStartTime); //Singapore
        omClientWnd.Invalidate(FALSE);

		// update scroll bar position
		long llTotalMin = CalcTotalMinutes();
		long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
		nPos = nPos * 1000L / llTotalMin;
		SetScrollPos(SB_HORZ, int(nPos), TRUE);
    }
    else
    {
        omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
        omTimeScale.Invalidate(TRUE);
    }
}

void StaffDiagram::OnZeit()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	// TODO: Add your command handler code here
	CTime olTime = ogBasicData.GetTime();
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);

	// update scroll bar position
    long llTotalMin = CalcTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	SelectComboDate(omTSStartTime); //Singapore

	ChangeViewTo(ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
}

void StaffDiagram::OnPrint()
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	// loading of the header bitmap from ceda.ini defined resource
	CPrintOrExportToExcel polDlgPrintOrExportToExcel;
	if(polDlgPrintOrExportToExcel.DoModal() == IDOK)
	{
		if(polDlgPrintOrExportToExcel.IsExportToExcel() == FALSE)
		{
			omViewer.PrintDiagram(omPtrArray);
		}
		else
		{
			omViewer.ExportToExcel(polDlgPrintOrExportToExcel.IsPageBreakEnabled(),polDlgPrintOrExportToExcel.IsColorEnabled());
		}
	}
}

BOOL StaffDiagram::DestroyWindow()
{
	if (m_wndBreakTable)
	{
		m_wndBreakTable->DestroyWindow();
		m_wndBreakTable = NULL;
	}

	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	ogCCSDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// StaffDiagram keyboard handling

void StaffDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_UP:		// move the bottom most gantt chart up/down one line
	case VK_DOWN:
		CListBox *polGantt;
		if ((polGantt = GetBottomMostGantt()) != NULL)
			polGantt->SendMessage(WM_USERKEYDOWN, nChar);
		break;
	case VK_PRIOR:
		blIsControl? OnFirstChart(): OnPrevChart();
		break;
	case VK_NEXT:
		blIsControl? OnLastChart(): OnNextChart();
		break;
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void StaffDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// StaffDiagram -- implementation of DDX call back function

static void StaffDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	StaffDiagram *polDiagram = (StaffDiagram *)popInstance;

	if(ipDDXType == REDISPLAY_ALL)
	{
		polDiagram->RedisplayAll();
	}
	else if(ipDDXType == STAFFDIAGRAM_UPDATETIMEBAND)
	{
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE || ipDDXType == PREPLAN_DATE_UPDATE)
	{
		polDiagram->HandleGlobalDateUpdate(*((CTime *) vpDataPointer));
	}
	//else if(ipDDXType == JOB_NEW || ipDDXType == JOB_CHANGE || ipDDXType == JOB_DELETE)
	else if(ipDDXType == JOB_DELETE)
	{
		polDiagram->HandleUpdatePoolJobForTeam((JOBDATA *)vpDataPointer);
	}
}

void StaffDiagram::HandleUpdatePoolJobForTeam(JOBDATA *prpJob)
{
	if(prpJob != NULL && !strcmp(prpJob->Jtco, JOBPOOL) && omViewer.GetTeamByJobUrno(prpJob->Urno) != NULL)
	{
		ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
		omClientWnd.Invalidate(FALSE);
	}
}

void StaffDiagram::RedisplayAll()
{
	ChangeViewTo(ogCfgData.rmUserSetup.STCV);
	SetCaptionText();
}

void StaffDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void StaffDiagram::UpdateTimeBand()
{
	for (int ilLc = imFirstVisibleChart; ilLc < omPtrArray.GetSize(); ilLc++)
	{
		StaffChart *polChart = (StaffChart *)omPtrArray[ilLc];
		polChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
	}
}



// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void StaffDiagram::HandleGlobalDateUpdate(CTime opDate)
{
	//omPrePlanTime = opDate; //Singapore
	omPrePlanTime = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),omTSStartTime.GetHour(),
		                  omTSStartTime.GetMinute(),omTSStartTime.GetSecond()); //Singapore
	SetTSStartTime(omPrePlanTime);
	omTimeScale.SetDisplayStartTime(omTSStartTime);
	omTimeScale.Invalidate(TRUE);
	omClientWnd.Invalidate(FALSE);

	// update scroll bar position
	long llTotalMin = CalcTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);

	SetCaptionText();
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pom_Date = (CComboBox*) omDialogBar.GetDlgItem(IDC_DATE);
		if(pom_Date != NULL)
		{
			pom_Date->SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(omTSStartTime));
			SetViewerDate();
	        ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
		}
	}
	else
	{
		ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
	}
}



// scroll prev/next charts when dragging+dropping over the prev/next buttons
LONG StaffDiagram::OnDragOver(UINT wParam, LONG lParam)
{
	CPoint olDropPosition;
	::GetCursorPos(&olDropPosition);

	CRect olRect;
	CWnd *polWnd;

	polWnd = GetDlgItem(IDC_NEXT);
	if(polWnd != NULL)
	{
		polWnd->GetWindowRect(&olRect);
		if(olRect.PtInRect(olDropPosition))
		{
			AutoScroll(AUTOSCROLL_INITIAL_SPEED);
			return -1L;
		}
	}

	polWnd = GetDlgItem(IDC_PREV);
	if(polWnd != NULL)
	{
		polWnd->GetWindowRect(&olRect);
		if(olRect.PtInRect(olDropPosition))
		{
			AutoScroll(AUTOSCROLL_INITIAL_SPEED);
			return -1L;
		}
	}
	return -1L;	// cannot accept this object
}


// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void StaffDiagram::AutoScroll(UINT ipInitialScrollSpeed /* = AUTOSCROLL_INITIAL_SPEED*/)
{
	// if not already scrolling automatically...
	if(!bmScrolling)
	{
		// ... after a short pause (ipInitialScrollSpeed), start scrolling
		bmScrolling = true;
		SetTimer(AUTOSCROLL_TIMER_EVENT, (UINT) ipInitialScrollSpeed, NULL);
	}
}

// called during drag&drop, does automatic scrolling when the cursor
// is outside the main gantt chart
void StaffDiagram::OnAutoScroll(void)
{
	bmScrolling = false;

	// if standard cursor then not currently dragging
	if(GetCursor() != AfxGetApp()->LoadStandardCursor(IDC_ARROW))
	{
		// check if the cursor is in the automatic scrolling region
		CPoint olDropPosition;
		::GetCursorPos(&olDropPosition);

		CRect olRect;
		CWnd *polWnd;

		polWnd = GetDlgItem(IDC_NEXT);
		if(polWnd != NULL)
		{
			polWnd->GetWindowRect(&olRect);
			if(olRect.PtInRect(olDropPosition))
			{
				OnPrevChart();
				bmScrolling = true;
			}
		}

		if(!bmScrolling)
		{
			polWnd = GetDlgItem(IDC_PREV);
			if(polWnd != NULL)
			{
				polWnd->GetWindowRect(&olRect);
				if(olRect.PtInRect(olDropPosition))
				{
					OnNextChart();
					bmScrolling = true;
				}
			}
		}
	}

	if(bmScrolling)
	{
		SetTimer(AUTOSCROLL_TIMER_EVENT, (UINT) AUTOSCROLL_SPEED, NULL);
	}
	else
	{
		// no longer in the scrolling region or left mouse button released
		KillTimer(AUTOSCROLL_TIMER_EVENT);
	}
}



void StaffDiagram::OnExpandTeam()
{
	omViewer.CompressTeams(false);
	ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
}


void StaffDiagram::OnCompressTeam()
{
	omViewer.CompressTeams(true);
	ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
}

void StaffDiagram::OnHideTeamDelegations()
{
	omViewer.bmHideCompletelyDelegatedTeams = !omViewer.bmHideCompletelyDelegatedTeams;
	ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
}

void StaffDiagram::OnBreakTable()
{
    // TODO: Add your control notification handler code here
    if (m_wndBreakTable == NULL)  // this window didn't open yet?
    {
		m_wndBreakTable = new BreakTable(NULL,this);
    }
    else    // close window
    {
        if (m_wndBreakTable->DestroyWindow())
		{
			m_wndBreakTable = NULL;
		}
    }
}

LONG StaffDiagram::OnBreakTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (m_wndBreakTable != NULL)  // this window is open ?
    {
		m_wndBreakTable = NULL;
    }

    return 0L;
}

void StaffDiagram::OnUpdateUIZeit(CCmdUI *pCmdUI)
{
	// check, if current time is inside time frame
	if (!ogBasicData.IsDisplayDateInsideTimeFrame())
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		CWnd *pWnd = omDialogBar.GetDlgItem(IDC_ZEIT);
		if (pWnd)
		{
			ogBasicData.SetWindowStat("STAFFDIAGRAM IDC_ZEIT",pWnd);
			pCmdUI->Enable(pWnd->IsWindowEnabled());
		}
	}
}

void StaffDiagram::OnUpdateUIMabstab(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_MABSTAB);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("STAFFDIAGRAM IDC_MABSTAB",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void StaffDiagram::OnUpdateUIPrint(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_PRINT);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("STAFFDIAGRAM IDC_PRINT",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void StaffDiagram::OnUpdateUIAssign(CCmdUI *pCmdUI)
{
//	pCmdUI->Enable(TRUE);
}

void StaffDiagram::OnUpdateUIAnsicht(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_ANSICHT);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("STAFFDIAGRAM IDC_ANSICHT",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

void StaffDiagram::OnUpdateUIView(CCmdUI *pCmdUI)
{
	CWnd *pWnd = omDialogBar.GetDlgItem(IDC_VIEW);
	if (pWnd)
	{
		ogBasicData.SetWindowStat("STAFFDIAGRAM IDC_VIEW",pWnd);
		pCmdUI->Enable(pWnd->IsWindowEnabled());
	}
}

LONG StaffDiagram::OnSelectDiagram(WPARAM wParam, LPARAM lParam)
{
	STAFF_SELECTION	*polSelection = reinterpret_cast<STAFF_SELECTION	*>(lParam);
	if (!polSelection)
		return 0L;

	StaffGantt *polStaffGantt = NULL;
    StaffChart *polStaffChart = (StaffChart *) omPtrArray.GetAt(polSelection->imGroupno);
	if (polStaffChart == NULL || !::IsWindow(polStaffChart->GetSafeHwnd()))
	{
		return 0L;
	}

    switch (wParam)
    {
	// select line
    case UD_SELECTLINE :
		{
			STAFF_LINEDATA *prlLine = omViewer.GetLine(polSelection->imGroupno,polSelection->imLineno);
			if (!prlLine)
				return 0L;

			// maximize chart if necessary
			if (polStaffChart->imState == Minimized)
				polStaffChart->OnChartButton();

			// try to get first pool job of shift
			JOBDATA *prlPJ = ogJobData.GetJobByUrno(polSelection->lmUserData);
			if(prlPJ == NULL)
				return 0L;
			SetTSStartTime(prlPJ->Acfr);

			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);

			// update scroll bar position
			long llTotalMin = CalcTotalMinutes();
			long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
			nPos = nPos * 1000L / llTotalMin;
			SetScrollPos(SB_HORZ, int(nPos), TRUE);

			// scroll horizontally in time
			ChangeViewTo(ogCfgData.rmUserSetup.STCV);

			// when scrolling horizontally the line number may change because the lines are not
			// fixed in the staff gantt chart so recheck the line number
			if(!omViewer.FindDutyBar(prlPJ->Urno,polSelection->imGroupno,polSelection->imLineno, false, true))
				return 0L;

			// scroll vertically to the correct group
			if (polSelection->imGroupno != imFirstVisibleChart)
			{
				imFirstVisibleChart = polSelection->imGroupno;
				OnUpdatePrevNext();
				PositionChild();
			}

			// select the Gantt only after we have scrolled to the desired chart, otherwise the gantt's
			// window handle may be invalid
			polStaffChart = (StaffChart *) omPtrArray.GetAt(polSelection->imGroupno);
			if (polStaffChart == NULL || !::IsWindow(polStaffChart->GetSafeHwnd()))
			{
				return 0L;
			}
			polStaffGantt = polStaffChart -> GetGanttPtr();
			if (polStaffGantt == NULL || !::IsWindow(polStaffGantt->GetSafeHwnd()))
			{
				return 0L;
			}

			// re-search because changing the horiz scroll may have changed the line number of the employee
			int ilGroup = -1, ilLine = -1;
			if(!omViewer.FindDutyBar(polSelection->lmUserData,ilGroup,ilLine))
				return 0L;

			// scroll vertically to line
			if(polStaffGantt->SetSel(ilLine) != LB_ERR)
			{
				CRect olItemRect;
				polStaffGantt->GetItemRect(ilLine, &olItemRect);
				polStaffGantt->ClientToScreen(olItemRect);
				::SetCursorPos(olItemRect.left+10, olItemRect.top);
			}

			omClientWnd.Invalidate(FALSE);
		}
	break;
	// select bar
    case UD_SELECTBAR :
		{
			STAFF_BARDATA *prlBar = omViewer.GetBar(polSelection->imGroupno,polSelection->imLineno,polSelection->imBarno);
			if (!prlBar)
				return 0L;

			// check, if visible chart
			if (polSelection->imGroupno < imFirstVisibleChart)
			{
				imFirstVisibleChart = polSelection->imGroupno;
				OnUpdatePrevNext();
				PositionChild();
			}
			else
			{
				CRect olRect;
				polStaffChart->GetWindowRect(olRect);
				CRect olParentRect;
				omClientWnd.GetWindowRect(olParentRect);
				CRect olResult;
				olRect.NormalizeRect();
				olParentRect.NormalizeRect();

				if (!olResult.IntersectRect(olRect,olParentRect))
				{
					imFirstVisibleChart = polSelection->imGroupno;
					OnUpdatePrevNext();
					PositionChild();
				}
			}

			// maximize chart if necessary
			if (polStaffChart->imState == Minimized)
			{
				polStaffChart->OnChartButton();
			}

			// scroll vertically to line
			polStaffGantt->SetSel(polSelection->imLineno);

			// scroll time bar
			long llTotalMin = CalcTotalMinutes();
			long nPos = (prlBar->StartTime - omStartTime).GetTotalMinutes();
			nPos = nPos * 1000L / llTotalMin;
			if(nPos <= 0) nPos = 1; // prevent scrolling to before the timescale start
			long nOldPos = GetScrollPos(SB_HORZ);
			if (nPos != nOldPos)
			{
				OnHScroll(SB_THUMBTRACK,nPos,NULL);
			}

			// update time band
			SetTimeBand(prlBar->StartTime, prlBar->EndTime);
			UpdateTimeBand();
		}
    break;

    }

    return 0L;
}

long StaffDiagram::CalcTotalMinutes(void)
{
	long llTotalMinutes = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	if(llTotalMinutes <= 0)
	{
		llTotalMinutes = 1; // prevent divide by zero error
	}
	return llTotalMinutes;
}

//Singapore
void StaffDiagram::OnSelchangeDate()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pom_Date = (CComboBox*) omDialogBar.GetDlgItem(IDC_DATE);
		if(pom_Date != NULL)
		{
			SetViewerDate();
			ChangeViewTo(ogCfgData.rmUserSetup.STCV,TRUE);
		}
	}
}

//Singapore
void StaffDiagram::SetViewerDate()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pom_Date = (CComboBox*) omDialogBar.GetDlgItem(IDC_DATE);
		if(pom_Date != NULL)
		{
			CTime olStart = ogBasicData.GetTimeframeStart();
			CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(pom_Date->GetCurSel(),0,0,0);

			//CTime olTimeframeEnd = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
			//omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
			// duration of the whole gantt chart --> minimum 8 hours (+1 minute to stop divide by zero)
			//omDuration = max(olTimeframeEnd - omStartTime,omTSDuration)+CTimeSpan(0,0,1,0);
			// the display start time (typically: current time - 1 hour)
			omTSStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),omTSStartTime.GetHour(),0,0);//omStartTime;//ogBasicData.GetDisplayDate() - ogBasicData.GetDisplayOffset();
			SetTSStartTime(omTSStartTime);
			omTimeScale.SetDisplayStartTime(omTSStartTime);
			omTimeScale.Invalidate(TRUE);
		}
	}
}

//Singapore
void StaffDiagram::SetDate(CTime opDate)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pom_Date = (CComboBox*) omDialogBar.GetDlgItem(IDC_DATE);
		if(pom_Date != NULL)
		{
			pom_Date->SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(opDate));
			OnSelchangeDate();
		}
	}
}

//Singapore
void StaffDiagram::SelectComboDate(CTime opDate)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		pom_Date = (CComboBox*) omDialogBar.GetDlgItem(IDC_DATE);
		if(pom_Date != NULL)
		{
			pom_Date->SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(opDate));
		}
	}
}

//PRF 8712
void StaffDiagram::OnMove(int x, int y)
{	
	CFrameWnd::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}

//PRF 8704
void StaffDiagram::OnPrintPreview()
{
	if (!pomTemplate)
	{
		pomTemplate = new CSingleDocTemplate(
			IDR_MENU_PRINT_PREVIEW,
			NULL,
			RUNTIME_CLASS(CFrameWnd),
			RUNTIME_CLASS(CPrintPreviewView));
		AfxGetApp()->AddDocTemplate(pomTemplate);
	}

	CFrameWnd * pFrameWnd = pomTemplate->CreateNewFrame( NULL,NULL);	
	pomTemplate->InitialUpdateFrame( pFrameWnd, NULL, FALSE);
	pomPrintPreviewView = (CPrintPreviewView*)pFrameWnd->GetActiveView();
	pomPrintPreviewView->pomCallMsgWindow=this;	
	pomPrintPreviewView->SetFrameWindow(pFrameWnd);
	pFrameWnd->SetWindowText(_T("Staff Chart"));
	pomPrintPreviewView->OnFilePrintPreview();
	return;
}

void StaffDiagram::OnBeginPrinting(WPARAM wParam, LPARAM lParam)
{
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;
	int ilMaxPage = 0;

	CString omTargett = CString("Anzeigebegin: ") + CString(omStartTime.Format("%d.%m.%Y"))
		+  "     Ansicht: " + ogCfgData.rmUserSetup.STCV;
	CString olPrintDatee = ogBasicData.GetTime().Format("%d.%m.%Y");
	omViewer.GetCCSPrinter() = new CCSPrint(this,PRINT_LANDSCAPE,80,500,200,
		CString("Mitarbeiterdiagramm"),olPrintDatee,omTargett);

	omViewer.GetCCSPrinter()->pomCdc = polPrintPreviewDcInfo->pomDC;
	omViewer.GetCCSPrinter()->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);
	omViewer.SetupPrintPageInfo();

	polPrintPreviewDcInfo->pomPrintInfo->SetMaxPage(omViewer.GetPrintPageCount());
	polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage = 1;
	bgPrintPreviewMode = TRUE;
	pogPreviewPrintWnd = this;
	pomPrintPreviewView->GetFrameWindow()->BeginModalState();	
}

void StaffDiagram::PrintPreview(WPARAM wParam, LPARAM lParam)
{
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;

	omViewer.GetCCSPrinter()->pomCdc = polPrintPreviewDcInfo->pomDC;
	omViewer.GetCCSPrinter()->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);
	omViewer.GetCCSPrinter()->pomCdc->SetMapMode(MM_ANISOTROPIC);	
	omViewer.GetCCSPrinter()->pomCdc->SetWindowExt(omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(HORZRES),omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(VERTRES));
	omViewer.GetCCSPrinter()->pomCdc->SetViewportExt(omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(HORZRES),omViewer.GetCCSPrinter()->pomCdc->GetDeviceCaps(VERTRES));
	omViewer.PrintPageData(omViewer.GetCCSPrinter(),polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage);
}

void StaffDiagram::OnEndPrinting(WPARAM wParam, LPARAM lParam)
{		
	pomPrintPreviewView->GetFrameWindow()->EndModalState();
	if(omViewer.GetCCSPrinter() != NULL)
	{
		delete omViewer.GetCCSPrinter();
		omViewer.GetCCSPrinter() = NULL;
	}
	pomPrintPreviewView = NULL;
	bgPrintPreviewMode = FALSE;
}

void StaffDiagram::OnPreviewPrint(WPARAM wParam, LPARAM lParam)
{
	OnPrint();
	pogPreviewPrintWnd = NULL;
}
