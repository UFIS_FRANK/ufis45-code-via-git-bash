// CciDiagram.h : header file
//

#ifndef _CCI_DIAGRAM_
#define _CCI_DIAGRAM_

/////////////////////////////////////////////////////////////////////////////
// CCIDiagram frame

#include <clientwn.h>
#include <tscale.h>
#include <CciViewer.h>
#include <CCSButtonCtrl.h>

#define AUTOSCROLL_TIMER_EVENT 2
#define AUTOSCROLL_INITIAL_SPEED 10 // millisecs
#define AUTOSCROLL_SPEED 1000 // millisecs


class CicDemandTable;

class CCIDiagram : public CFrameWnd
{
    DECLARE_DYNCREATE(CCIDiagram)

public:
	CCIDiagram();
    CCIDiagram(BOOL bpPrePlanMode,CTime opPrePlanTime);   

// Attributes
public:

// Operations
public:
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName);
	void OnUpdatePrevNext(void);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void CCIDiagram::SetAllCciAreaButtonsColor(void);
	void SetCciAreaButtonColor(int ipGroupno);
	void SetCaptionText(void);
	BOOL DestroyWindow();
    void OnFirstChart();
    void OnLastChart();
	void SetDemandColor(COLORREF lpColor);
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();

// Internal used only
// Id 25-Sep-96 -- add keyboard handling to the diagram
private:
	CListBox *GetBottomMostGantt();
	CRect omWindowRect; //PRF 8712
// Overrides
public:

// Implementation
protected:
    virtual ~CCIDiagram();

    // Generated message map functions
    //{{AFX_MSG(CCIDiagram)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMove(int x, int y); //PRF 8712
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnNextChart();
    afx_msg void OnPrevChart();
    afx_msg void OnMabstab();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
    afx_msg void OnAnsicht();
	afx_msg void OnViewSelChange();
	afx_msg void OnZeit();
	afx_msg void OnPrint();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateUIZeit(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIMabstab(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIPrint(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAssign(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAnsicht(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIView(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIDemandList(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAllocate(CCmdUI *pCmdUI);
	afx_msg void OnDemandList();
	afx_msg LONG OnDemandTableExit(UINT wParam, LONG lParam);
	afx_msg void OnAssign();
	afx_msg void OnAllocate();
    afx_msg LONG OnDragOver(UINT, LONG); 
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

protected:
    CDialogBar		omDialogBar;
    C2StateButton	omArrival;
    C2StateButton	omDeparture;
    C3DStatic		omTime;
    C3DStatic		omDate;
    C3DStatic		omTSDate;
	CCSButtonCtrl *pomAssignButton;
    
    CTimeScale		omTimeScale, omTimeScale1;
    CBitmapButton	omBB1, omBB2;
    
    CClientWnd		omClientWnd;
    CStatusBar		omStatusBar;
    
    CTime			omStartTime;
    CTimeSpan		omDuration;
    CTime			omTSStartTime;
    CTimeSpan		omTSDuration;
    CTimeSpan		omTSInterval;
    
    CPtrArray		omPtrArray;
    int				imFirstVisibleChart;
    int				imStartTimeScalePos;
    
    CTime			omPrePlanTime;
	CString			omCaptionText;
	BOOL			omPrePlanMode;

    CciDiagramViewer omViewer;
	BOOL			bmIsViewOpen;

	CicDemandTable*	m_wndDemandTable;
	CCSButtonCtrl	*pomDemandTableButton;
	CTime			omTimeBandStartTime, omTimeBandEndTime;

	long CalcTotalMinutes(void);


// Redisplay all methods for DDX call back function
public:
	void RedisplayAll();
	BOOL bmNoUpdatesNow;
	void HandleGlobalDateUpdate(CTime opDate);
	void ProcessEndAssignment();
	void ProcessStartAssignment();

	bool bmScrolling;
	void AutoScroll(UINT ipInitialScrollSpeed = AUTOSCROLL_INITIAL_SPEED);
	void OnAutoScroll(void);
	CCSDragDropCtrl m_DragDropTarget;

private:
	static	void	CciDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
			void	SetDemandColor(COLORREF lpColor,BOOL bpRecess);

private:
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _CCI_DIAGRAM_
