#ifndef __SFVIEWER_H__
#define __SFVIEWER_H__

#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <cviewer.h>
#include <table.h>
#include <CedaJobData.h>
#include <ccsprint.h>
#include <FieldConfigDlg.h>

struct STAFFTABLE_ASSIGNMENT  // each assignment record
{
    CString Alid;   // assigned to gate or flight
	long Urno;      // gate or flight job Urno 
    CTime Acfr;     // assigned from
    CTime Acto;     // assigned until

	STAFFTABLE_ASSIGNMENT()
	{
		Urno = 0;
		Acfr = TIMENULL;
		Acto = TIMENULL;
	}

	STAFFTABLE_ASSIGNMENT(const STAFFTABLE_ASSIGNMENT& rhs)
	{
		*this = rhs;		
	}

	STAFFTABLE_ASSIGNMENT& STAFFTABLE_ASSIGNMENT::operator=(const STAFFTABLE_ASSIGNMENT& rhs)
	{
		if (&rhs != this)
		{
			this->Alid = rhs.Alid;
			this->Urno = rhs.Urno;
			this->Acfr = rhs.Acfr;
			this->Acto = rhs.Acto;
		}
		return *this;
	}
};

struct STAFFTABLE_LINEDATA
{
	CString Name;	// staff name (both Lnam, Fnam)
	CString Peno;	// staff ID of this record
	CString Rank;	// rank of this staff
	CString Tmid;	// the team that this staff is in
	CString Sfca;	// ShiftCode for this staff
	CString Sfcs;	// Old ShiftCode for this staff
	long	ShiftUrno; // Urno of actual JOBPOOL
	CTime Acfr;		// actual start period of time of this shift
	CTime Acto;		// actual ending period of time of this shift

    CCSPtrArray<STAFFTABLE_ASSIGNMENT> Assignment;

	STAFFTABLE_LINEDATA()
	{
		ShiftUrno = 0;
		Acfr = TIMENULL;
		Acto = TIMENULL;
	}

	STAFFTABLE_LINEDATA(const STAFFTABLE_LINEDATA& rhs)
	{
		*this = rhs;		
	}

	STAFFTABLE_LINEDATA& STAFFTABLE_LINEDATA::operator=(const STAFFTABLE_LINEDATA& rhs)
	{
		if (&rhs != this)
		{
			this->Name = rhs.Name;
			this->Peno = rhs.Peno;
			this->Rank = rhs.Rank;
			this->Tmid = rhs.Tmid;
			this->Sfca = rhs.Sfca;
			this->Sfcs = rhs.Sfcs;
			this->ShiftUrno = rhs.ShiftUrno;
			this->Acfr = rhs.Acfr;
			this->Acto = rhs.Acto;
			this->Assignment.DeleteAll();
			STAFFTABLE_ASSIGNMENT olStafftableAssignment;
			for(int i = 0; i < rhs.Assignment.GetSize(); i++)
			{
				olStafftableAssignment = rhs.Assignment[i];
				this->Assignment.NewAt(this->Assignment.GetSize(),olStafftableAssignment);
			}
		}
		return *this;
	}
	
	~STAFFTABLE_LINEDATA()
	{
		Assignment.DeleteAll(); 
	}
};

/////////////////////////////////////////////////////////////////////////////
// StaffTableViewer

class StaffTableViewer: public CViewer
{
// Constructions
public:
    StaffTableViewer();
    ~StaffTableViewer();

    void Attach(CTable *popAttachWnd);
	/* void ChangeViewTo(const char *pcpViewName, CString opDate); */
    void ChangeViewTo(const char *pcpViewName);

// Internal data processing routines
private:
	static void StaffTableTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareGrouping();
    void PrepareFilter();
	void PrepareSorter();
	BOOL IsPassFilter(const char *pcpRank, const char *pcpTeamId,
		const char *pcpShiftCode, CTime opAcfr, CTime opActo, bool bpIsAbsent, CStringArray &ropPermits, 
		bool bpHasFidJobs, bool bpPoolJobSetToAbsent, bool bpEmpStandby);
	int CompareStaff(STAFFTABLE_LINEDATA *prpStaff1, STAFFTABLE_LINEDATA *prpStaff2);
    BOOL IsSameGroup(STAFFTABLE_LINEDATA *prpShift1, STAFFTABLE_LINEDATA *prpShift2);

    void MakeLines();
	void MakeLine(SHIFTDATA *prpShift);
	void MakeAssignments(CCSPtrArray <JOBDATA> &ropJobs,STAFFTABLE_LINEDATA &rrpStaff);
	int CreateAssignment(STAFFTABLE_LINEDATA &rrlStaff, STAFFTABLE_ASSIGNMENT *prpAssignment);
//	CString GetAssignmentString(JOBDATA *prpJob);


// Operations
public:
	void DeleteAll();
	int CreateLine(STAFFTABLE_LINEDATA *prpStaff);
	void DeleteLine(int ipLineno);
	void SetIsFromSearchMode(BOOL bpIsFromSearch);
	BOOL FindShift(long lpShiftUrno, int &rilLineno);

// Window refreshing routines
public:
	BOOL DeleteAssignmentJob(long lpJobUrno);
private:
	void ProcessShiftDelete(long lpUrno);
	void ProcessShiftChange(SHIFTDATA *prpShift);
	void ProcessShiftSelect(SHIFTDATA *prpShift);
	void ProcessShiftChange(JOBDATA *prpJob);
	void ProcessJobDelete(JOBDATA *prpJob);
	void UpdateDisplay();
	CString Format(STAFFTABLE_LINEDATA *prpLine);

// Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "Sfviewer.cpp" (use first sorting)
    CWordArray omSortOrder;     // array of enumerated value -- defined in "Sfviewer.cpp"
	CMapStringToPtr omCMapForShiftCode;
	CMapStringToPtr omCMapForRank;
	CMapStringToPtr omCMapForPermits;
    CMapStringToPtr omCMapForTeam;
    CString omDate;
	BOOL bmIsFromSearch;

	// Attributes
private:
    CTable *pomTable;
	bool bmUseAllShiftCodes ;
	bool bmUseAllRanks;
	bool bmUseAllPermits;
	bool bmUseAllTeams;
	bool bmEmptyTeamSelected;
	bool bmHideAbsentEmps, bmHideFidEmps, bmHideStandbyEmps;
	int imShiftTime;
    CCSPtrArray<STAFFTABLE_LINEDATA> omLines;
	int	 imColumns;
	CString omTerminal; //Singapore
	
// Methods which handle changes (from Data Distributor)
public:
    CTime omStartTime;
	CTime omEndTime;

	CFieldDataArray omHeaderConfig;
	int imNumJobsPerLine;
	int imFirstJobField;
	int imCharWidth;
	int imMaxLines;
	int imMaxJobsPerLine;
	int imMaxLineWidth;
	void PrintViewConfigurable();
	inline int Lines() const {return omLines.GetSize();} //Singapore
	inline void SetTerminal(const CString& ropTerminal){omTerminal = ropTerminal;} //Singapore
	inline CString GetTerminal() const {return omTerminal;} //Singapore
    inline void GetLines(CCSPtrArray<STAFFTABLE_LINEDATA>& opLines) const{opLines = omLines;} //Singapore
	CCSPtrArray<STAFFTABLE_LINEDATA>& GetLines(){return omLines;} //Singapore
	CCSPrint*& GetCCSPrinter() {return pomPrint;}

// Printing functions
private:
	BOOL PrintStaffLine(STAFFTABLE_LINEDATA *prpLine,BOOL bpIsLastLine);
	BOOL PrintStaffLine2(STAFFTABLE_LINEDATA *prpLine);
	BOOL PrintStaffLine3(STAFFTABLE_LINEDATA *prpLine);
	BOOL PrintStaffHeader(CCSPrint *pomPrint);
	CCSPrint *pomPrint;
	CString GetTeamLeaderWorkGroupsForFlight(long lpFlightUrno);

	BOOL PrintStaffHeaderConfigurable(CCSPrint *pomPrint);
	void PrintColumnTitlesConfigurable();
	BOOL PrintStaffLineConfigurable(STAFFTABLE_LINEDATA *prpLine);
	bool ConfigureHeader(void);
	bool CheckForNewPageConfigurable();
	void PrintEndOfPageConfigurable();

	int imFuncLen2,imNameLen2,imWorkGroupLen2,imShiftLen2,imJobLen2,imRegnLen2,imArrTimeLen2,imDepTimeLen2,imGatePosLen2,imServiceLen2;
	int imFuncLen3,imBreakLen3,imNameLen3,imShiftLen3,imJobLen3,imRegnLen3,imArrTimeLen3,imDepTimeLen3,imGatePosLen3,imServiceLen3,imTotalLen3;
	void AddElement(CCSPtrArray <PRINTELEDATA> &ropPrintLine, CString &ropText, int ipLength, int ipAlignment, int ipFrameLeft, int ipFrameRight, int ipFrameTop, int ipFrameBottom, CFont &ropFont);
	void PrintEndOfPage2();
	bool PrintStartOfPage2();
	void PrintEndOfPage3();
	bool PrintStartOfPage3();
public:
	void PrintView();
	void PrintView2();
	void PrintView3();
	void CreateExport(const char *pclFileName, const char *pclSeperator, CString opTitle);
	CString GetJobText(JOBDATA *prpJob);

	void PrintPreview(const int& ripPageNo); //Singapore
};

#endif //__SFVIEWER_H__
