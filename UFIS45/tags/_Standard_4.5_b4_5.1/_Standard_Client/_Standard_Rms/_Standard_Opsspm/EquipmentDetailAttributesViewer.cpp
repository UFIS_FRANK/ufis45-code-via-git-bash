// EquipmentDetailAttributesViewer.cpp : implementation file

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <ccsddx.h>
#include <EquipmentDetailAttributesViewer.h>
#include <BasicData.h>
#include <CCSEdit.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

extern CFont ogMSSansSerif_Regular_6;

EquipmentDetailAttributesViewer::EquipmentDetailAttributesViewer()
{
    pomTable = NULL;
}

EquipmentDetailAttributesViewer::~EquipmentDetailAttributesViewer()
{
	omEqaList.DeleteAll();
}

void EquipmentDetailAttributesViewer::Attach(CCSTable *popTable)
{
    pomTable = popTable;
}

void EquipmentDetailAttributesViewer::UpdateView(long lpEquUrno)
{
	CCSPtrArray <EQADATA> olEqaList;
	ogEqaData.GetEqasByUequ(lpEquUrno, olEqaList);

	omEqaList.DeleteAll();
	int ilNumLines = olEqaList.GetSize();
    for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		EQADATA *prlEqa = new EQADATA;
		if(prlEqa != NULL)
		{
			*prlEqa = olEqaList[ilLc];
			omEqaList.Add(prlEqa);
		}
	}
	UpdateDisplay();
}

void EquipmentDetailAttributesViewer::UpdateDisplay()
{
	pomTable->ResetContent();
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	pomTable->SetShowSelection(true);
//	pomTable->SetLeftDockingRange(1);
//	pomTable->SetRightDockingRange(1);
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogMSSansSerif_Regular_6;

 	rlHeader.Length = 160;
	rlHeader.Text = GetString(IDS_EQDD_ATTRIBUTES);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 325;
	rlHeader.Text = GetString(IDS_EQDD_VALUE);
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();
	
//	pomTable->SetDefaultSeparator();

	
	int ilNumLines = omEqaList.GetSize();
    for (int ilLc = 0; ilLc < ilNumLines; ilLc++)
	{
		EQADATA *prlEqa = &omEqaList[ilLc];

		CCSPtrArray <TABLE_COLUMN> olColList;
		MakeLineData(prlEqa,&olColList);

		pomTable->AddTextLine(olColList, (void *)prlEqa);
		pomTable->SetTextLineDragEnable(ilLc,FALSE);

		olColList.DeleteAll();
    }                                           

	pomTable->SetColumnEditable(0, false);
	pomTable->SetColumnEditable(1, true);

	CCSEDIT_ATTRIB rlColumnAttribute;
	rlColumnAttribute.TextMaxLenght = 40;
	pomTable->SetColumnType(1, rlColumnAttribute);

    // Update the table content in the display
    pomTable->DisplayTable();
}


void EquipmentDetailAttributesViewer::MakeLineData(EQADATA *prpEqa, CCSPtrArray <TABLE_COLUMN> *popColList)
{
	TABLE_COLUMN rlColumnData;
	rlColumnData.VerticalSeparator = SEPA_NONE;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Regular_6;
	rlColumnData.Alignment = COLALIGN_LEFT;

	rlColumnData.Text = prpEqa->Name;
	rlColumnData.BkColor = SILVER;
	rlColumnData.TextColor = BLACK;
	popColList->NewAt(popColList->GetSize(), rlColumnData);

	rlColumnData.Text = prpEqa->Valu;
	rlColumnData.BkColor = WHITE;
	rlColumnData.TextColor = BLACK;
	popColList->NewAt(popColList->GetSize(), rlColumnData);
}




EQADATA *EquipmentDetailAttributesViewer::GetEqaByLine(int ilLineno)
{
	EQADATA *prlEqa = NULL;
	if(ilLineno >= 0 && ilLineno < omEqaList.GetSize())
	{
		prlEqa = &omEqaList[ilLineno];
	}
	return prlEqa;
}

void EquipmentDetailAttributesViewer::UpdateChangedValues(void)
{
	int ilNumLines = omEqaList.GetSize();
    for (int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		EQADATA *prlEqa = GetEqaByLine(ilLine);
		if(prlEqa != NULL)
		{
			EQADATA *prlOldEqa = ogEqaData.GetEqaByUrno(prlEqa->Urno);
			if(prlOldEqa != NULL && strcmp(prlOldEqa->Valu,prlEqa->Valu))
			{
				ogEqaData.UpdateEqa(prlEqa->Urno, prlEqa->Valu);
			}
		}
	}
}

bool EquipmentDetailAttributesViewer::ResetChangedValues(int ipLine)
{
	bool blChanged = false;

	EQADATA *prlEqa = GetEqaByLine(ipLine);
	if(prlEqa != NULL)
	{
		EQADATA *prlOldEqa = ogEqaData.GetEqaByUrno(prlEqa->Urno);
		if(prlOldEqa != NULL)
		{
			if(strcmp(prlOldEqa->Valu,prlEqa->Valu))
			{
				strcpy(prlEqa->Valu,prlOldEqa->Valu);
				blChanged = true;
			}
		}
	}

	return blChanged;
}
