// ConflictTablePropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaAltData.h>
#include <CedaPolData.h>
#include <CedaRnkData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <BasePropertySheet.h>
#include <ConflictTablePropSheet.h>
#include <CedaFlightData.h>
#include <conflict.h>
#include <DemandTableViewer.h>
#include <AllocData.h>
#include <CedaHagData.h>
#include <CedaAltData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ConflictTablePropertySheet
//
ConflictTablePropertySheet::ConflictTablePropertySheet(CWnd* pParentWnd,CViewer *popViewer, UINT iSelectPage) 
: BasePropertySheet(GetString(IDS_STRING61593), pParentWnd, popViewer, iSelectPage)
{
	EnableStackedTabs(FALSE);
	AddPage(&m_pageType);
	AddPage(&m_pageObjectType);
	AddPage(&m_pageSort);
	AddPage(&m_pageAloc);
	AddPage(&m_pagePools);
	AddPage(&m_pageFunctions);

	// Change the caption in tab control of the PropertySheet
	//m_pageType.SetCaption(ID_PAGE_FILTER_CONFLICTTYPE);
	m_pageType.SetCaption(GetString(IDS_STRING61600));

	//m_pageObjectType.SetCaption(ID_PAGE_FILTER_CONFLICTOBJECT);
	m_pageObjectType.SetCaption(GetString(IDS_STRING61601));

	m_pageAloc.SetCaption(GetString(IDS_STRING61875));

	//m_pageAirline.SetCaption(ID_PAGE_FILTER_AIRLINE);
	m_pageAirlines.SetCaption(GetString(IDS_STRING61597));

	m_pageAgents.SetCaption(GetString(IDS_STRING61954));

	m_pagePools.SetCaption(GetString(IDS_CTPS_POOLS));

	m_pageFunctions.SetCaption(GetString(IDS_CTPS_FUNCTIONS));

	// Prepare possible values for each PropertyPage
	ogConflicts.GetAllConflictTypes(m_pageType.omPossibleItems);
	//m_pageObjectType.omPossibleItems.Add("Einsatz");
	m_pageObjectType.omPossibleItems.Add(GetString(IDS_STRING61608));
	//m_pageObjectType.omPossibleItems.Add("Flug");
	m_pageObjectType.omPossibleItems.Add(GetString(IDS_STRING61609));

	// get all possible aloc's
	const char **pclAllocGroupType = DemandTableViewer::omAllocGroupTypes;
	while(*pclAllocGroupType)
	{
		m_pageAloc.omPossibleItems.Add(*pclAllocGroupType);

		// add allocation id pages
		DemandFilterPage *polPage = NULL;
		if (!m_pageAlids.Lookup(*pclAllocGroupType,(void *&)polPage))
		{
			polPage = new DemandFilterPage;
			m_pageAlids.SetAt(*pclAllocGroupType,polPage);

			ALLOCUNIT *pclUnit = ogAllocData.GetGroupTypeByName(*pclAllocGroupType);
			if (pclUnit)
				polPage->SetCaption(pclUnit->Desc);
			else
				polPage->SetCaption(*pclAllocGroupType);

			CCSPtrArray <ALLOCUNIT> olAllocGroups;
			ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
			int ilNumGroups = olAllocGroups.GetSize();
			for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
			{
				ALLOCUNIT *prlGroup= &olAllocGroups[ilGroup];
				polPage->omPossibleItems.Add(prlGroup->Name);
			}
			polPage->bmSelectAllEnabled = true;
		}
		++pclAllocGroupType;
	}
	m_pageAloc.bmSelectAllEnabled = true;

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(m_pageAirlines.omPossibleItems);

	CStringArray rolNames;
	int n = ogHagData.GetNameList(rolNames);
	for (int i = 0; i < n; i++)
	{
		m_pageAgents.omPossibleItems.Add(rolNames[i]);
	}

	m_pageAgents.omPossibleItems.Add(GetString(IDS_NOAGENT));

	m_pageAgents.bmSelectAllEnabled = true;

	CStringArray olPools;
	ogPolData.GetPoolNames(olPools);
	for(int ilPool = 0; ilPool < olPools.GetSize(); ilPool++)
		m_pagePools.omPossibleItems.Add(olPools[ilPool]);
	m_pagePools.bmSelectAllEnabled = true;

	ogRnkData.GetAllRanks(m_pageFunctions.omPossibleItems);
	m_pageFunctions.bmSelectAllEnabled = true;
}

ConflictTablePropertySheet::~ConflictTablePropertySheet()
{
	POSITION pos = m_pageAlids.GetStartPosition();
	while(pos)
	{
		CString olKey;
		DemandFilterPage *polPage = NULL;
		m_pageAlids.GetNextAssoc(pos,olKey,(void *&)polPage);
		if (GetPageIndex(polPage) >= 0)
			RemovePage(polPage);
		delete polPage;
	}
	m_pageAlids.RemoveAll();
}

BEGIN_MESSAGE_MAP(ConflictTablePropertySheet, BasePropertySheet)
	//{{AFX_MSG_MAP(ConflictTablePropertySheet)
    ON_MESSAGE(WM_UPDATE_ALL_PAGES, OnUpdateAllPages)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void ConflictTablePropertySheet::LoadDataFromViewer()
{
	// remove all previously used Airline && Handling agent pages from sheet
	if (GetPageIndex(&m_pageAirlines) > 0)
		RemovePage(&m_pageAirlines);

	if (GetPageIndex(&m_pageAgents) > 0)
		RemovePage(&m_pageAgents);
	
	// remove all previously used Alid - based pages from the sheet
	POSITION pos = m_pageAlids.GetStartPosition();
	while(pos)
	{
		CString olKey;
		DemandFilterPage *polPage = NULL;
		m_pageAlids.GetNextAssoc(pos,olKey,(void *&)polPage);
		if (GetPageIndex(polPage) >= 0)
			RemovePage(polPage);
	}

	// get allocation filter
	pomViewer->GetFilter("Aloc", m_pageAloc.omSelectedItems);
	for (int ilC = 0; ilC < m_pageAloc.omSelectedItems.GetSize();ilC++)
	{
		DemandFilterPage *polPage = NULL;
		if (m_pageAlids.Lookup(m_pageAloc.omSelectedItems[ilC],(void *&)polPage) && polPage != NULL)
		{
			pomViewer->GetFilter(m_pageAloc.omSelectedItems[ilC],polPage->omSelectedItems);
			AddPage(polPage);
		}
	}

	pomViewer->GetFilter("Airline", m_pageAirlines.omSelectedItems);
	pomViewer->GetFilter("ConflictType", m_pageType.omSelectedItems);
	pomViewer->GetFilter("Object Typ", m_pageObjectType.omSelectedItems);
	pomViewer->GetFilter("Pools", m_pagePools.omSelectedItems);
	pomViewer->GetFilter("Functions", m_pageFunctions.omSelectedItems);

	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.bmAscending = pomViewer->GetUserData("TIMEORDER") == "YES" ? true : false;


	for (ilC = 0; ilC < m_pageObjectType.omSelectedItems.GetSize(); ilC++)
	{
		// flight conflicts selected ?
		if (m_pageObjectType.omSelectedItems[ilC] == GetString(IDS_STRING61609))
		{
			AddPage(&m_pageAirlines);
			if (ogBasicData.HandlingAgentsSupported())
			{
				AddPage(&m_pageAgents);
				CStringArray olShortNames;
				pomViewer->GetFilter("Agents",		olShortNames);
				m_pageAgents.omSelectedItems.RemoveAll();
				ogHagData.GetNameList(olShortNames,m_pageAgents.omSelectedItems);
			}
			break;
		}
	}
}

void ConflictTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Aloc", m_pageAloc.omSelectedItems);
	for (int ilC = 0; ilC < m_pageAloc.omSelectedItems.GetSize();ilC++)
	{
		DemandFilterPage *polPage = NULL;
		if (m_pageAlids.Lookup(m_pageAloc.omSelectedItems[ilC],(void *&)polPage) && polPage != NULL)
		{
			pomViewer->SetFilter(m_pageAloc.omSelectedItems[ilC],polPage->omSelectedItems);
		}
	}

	pomViewer->SetFilter("Airline", m_pageAirlines.omSelectedItems);
	pomViewer->SetFilter("ConflictType", m_pageType.omSelectedItems);
	pomViewer->SetFilter("Object Typ", m_pageObjectType.omSelectedItems);
	pomViewer->SetFilter("Pools", m_pagePools.omSelectedItems);
	pomViewer->SetFilter("Functions", m_pageFunctions.omSelectedItems);

	CStringArray olShortNames;
	ogHagData.GetShortNameList(m_pageAgents.omSelectedItems,olShortNames);
	pomViewer->SetFilter("Agents",		olShortNames);

	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetUserData("TIMEORDER",m_pageSort.bmAscending ? "YES" : "NO");

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int ConflictTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedAlocs;
	CStringArray olSelectedTypes;
	CStringArray olSelectedAlids;
	CStringArray olSortOrders;
	CStringArray olSelectedTime;
	CStringArray olSelectedAirlines;
	CStringArray olSelectedAgents;
	CStringArray olSelectedPools;
	CStringArray olSelectedFunctions;

	pomViewer->GetFilter("Aloc", olSelectedAlocs);
	pomViewer->GetFilter("ConflictType", olSelectedTypes);
	pomViewer->GetFilter("Object Typ", olSelectedAlids);
	pomViewer->GetFilter("Airline", olSelectedAirlines);
	pomViewer->GetFilter("Pools", olSelectedPools);
	pomViewer->GetFilter("Functions", olSelectedFunctions);
	pomViewer->GetSort(olSortOrders);

	pomViewer->GetFilter("Agents",		olSelectedAgents);

	CStringArray olShortNames;
	ogHagData.GetShortNameList(m_pageAgents.omSelectedItems,olShortNames);

	bool bmWasAscending = pomViewer->GetUserData("TIMEORDER") == "YES" ? true : false;

	if (!IsIdentical(olSelectedAlocs, m_pageAloc.omSelectedItems) ||
		!IsIdentical(olSelectedTypes, m_pageType.omSelectedItems) ||
		!IsIdentical(olSelectedAlids, m_pageObjectType.omSelectedItems) ||
		!IsIdentical(olSelectedAirlines, m_pageAirlines.omSelectedItems) ||
		!IsIdentical(olSelectedPools, m_pagePools.omSelectedItems) ||
		!IsIdentical(olSelectedFunctions, m_pageFunctions.omSelectedItems) ||
		!IsIdentical(olSelectedAgents, olShortNames) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		bmWasAscending != m_pageSort.bmAscending)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}

LONG ConflictTablePropertySheet::OnUpdateAllPages(UINT wParam,LONG lParam)
{
	CPropertyPage *popSenderPage = reinterpret_cast<CPropertyPage *>(lParam);
	if (popSenderPage == &m_pageAloc)
	{
		bool blSelectAll = false;
		if (m_pageAloc.m_List2.GetCount() == 1)
		{
			CString olName;
			m_pageAloc.m_List2.GetText(0,olName);
			if (olName == GetString(IDS_ALLSTRING))
				blSelectAll = true;
		}

		POSITION pos = m_pageAlids.GetStartPosition();
		while(pos)
		{
			CString olName;
			CPropertyPage *polPage = NULL;
			m_pageAlids.GetNextAssoc(pos,olName,(void *&)polPage);
			if (blSelectAll && GetPageIndex(polPage) < 0)
			{
				AddPage(polPage);
			}
			else if (m_pageAloc.m_List2.FindStringExact(-1,olName) == LB_ERR)
			{
				if (GetPageIndex(polPage) >= 0)
					RemovePage(polPage);
			}
			else if (GetPageIndex(polPage) < 0)
				AddPage(polPage);
		}
	}
	else if (popSenderPage == &m_pageObjectType)
	{
		// flight selected ?
		if (m_pageObjectType.m_List2.FindStringExact(-1,GetString(IDS_STRING61609)) == LB_ERR)
		{
			if (GetPageIndex(&m_pageAirlines) >= 0)
				RemovePage(&m_pageAirlines);

			if (ogBasicData.HandlingAgentsSupported())
			{
				if (GetPageIndex(&m_pageAgents) >= 0)
					RemovePage(&m_pageAgents);
			}
		}
		else
		{
			if (GetPageIndex(&m_pageAirlines) < 0)
				AddPage(&m_pageAirlines);

			if (ogBasicData.HandlingAgentsSupported())
			{
				if (GetPageIndex(&m_pageAgents) < 0)
					AddPage(&m_pageAgents);
			}
		}
	}

	return 0l;
}