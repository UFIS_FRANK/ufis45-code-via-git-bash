// CedaPgpData.h - Class for PGPTAB - Planning Groups

#ifndef _CEDAPGPDATA_H_
#define _CEDAPGPDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

#define PGP_SEPERATOR '|'

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

// Name     Null?    Type
// -------- -------- ----
// CDAT              CHAR(14)
// HOPO              CHAR(3)
// LSTU              CHAR(14)
// MAXM              VARCHAR2(2000)
// MINM              VARCHAR2(2000)
// PGPC              CHAR(5)
// PGPM              VARCHAR2(2000)
// PGPN              VARCHAR2(40)
// PRFL              CHAR(1)
// REMA              VARCHAR2(60)
// TYPE              CHAR(3)
// URNO              CHAR(10)
// USEC              CHAR(32)
// USEU              CHAR(32)

struct PgpDataStruct
{
	long Urno;
	char Pgpc[6];	// Planning Group Name
	char Pgpm[2001];// '|' seperared list of Functions (for example) the first of which is the group leader
	char Type[4];	// Tablename holding values held in Pgpm (eg. 'PFC' = Functions)
	CMapStringToPtr FunctionWeights; // list of functions and their order in Pgpm - used for sorting emps in the order which their functions are defined 

	PgpDataStruct(void)
	{
		Urno = 0L;
		strcpy(Pgpc,"");
		strcpy(Pgpm,"");
		strcpy(Type,"");
	}

	PgpDataStruct& PgpDataStruct::operator=(const PgpDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Pgpc,rhs.Pgpc);
			strcpy(Pgpm,rhs.Pgpm);
			strcpy(Type,rhs.Type);
		}		
		return *this;
	}
};

typedef struct PgpDataStruct PGPDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaPgpData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <PGPDATA> omData;
	CMapPtrToPtr omUrnoMap;

// Operations
public:
	CedaPgpData();
	~CedaPgpData();

	void ProcessPgpBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadPgpData();
	PGPDATA* GetPgpByUrno(long lpUrno);
	CString GetGroupLeaderFunction(long lpUrno);
	CString GetTableName(void);
	void PrepareFunctionWeights(PGPDATA *prpPgp);
	int GetFunctionWeight(long lpUrno, const char *pcpFunction);

private:
	PGPDATA *AddPgpInternal(PGPDATA &rrpPgp);
	void DeletePgpInternal(long lpUrno);
	void ClearAll();
	void PrepareData(PGPDATA *prpPgp);
};

extern CedaPgpData ogPgpData;

#endif _CEDAPGPDATA_H_
