// DruckAuswahl.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <DruckAuswahl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDruckAuswahl dialog


CDruckAuswahl::CDruckAuswahl(CWnd* pParent /*=NULL*/)
	: CDialog(CDruckAuswahl::IDD, pParent)
{                                 
	m_DruckAusw = IDC_GATES;
	//{{AFX_DATA_INIT(CDruckAuswahl)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDruckAuswahl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDruckAuswahl)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDruckAuswahl, CDialog)
	//{{AFX_MSG_MAP(CDruckAuswahl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP() 

/////////////////////////////////////////////////////////////////////////////
// CDruckAuswahl message handlers

void CDruckAuswahl::OnOK() 
{
	// TODO: Add extra validation here
	
	CButton *polRadioButton = (CButton *) GetDlgItem(IDC_GATES);
	if (polRadioButton != NULL)
	{
		if (polRadioButton->GetCheck() == 1)
			m_DruckAusw = 1;
		else
			m_DruckAusw = 2;
	}
	CDialog::OnOK();
}

BOOL CDruckAuswahl::OnInitDialog() 
{
	m_DruckAusw = 0;

	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING32920));

	CWnd *polWnd = GetDlgItem(IDC_GATES); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32921));
	}
	polWnd = GetDlgItem(IDC_FM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32922));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	CButton *polRadioButton = (CButton *) GetDlgItem(IDC_GATES);
	if (polRadioButton != NULL)
	{
		polRadioButton->SetCheck(TRUE);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
