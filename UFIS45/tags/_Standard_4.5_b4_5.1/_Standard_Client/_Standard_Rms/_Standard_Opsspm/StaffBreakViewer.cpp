// staffbreakviewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							In this module, the old implementation will use the field
//							PRID to keep text for a special job and an illness job.
//							Now, these has been moved to the field TEXT.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <StaffBreakViewer.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif



#ifdef	IsOverlapped
#undef	IsOverlapped
#endif

template<class TYPE> bool IsOverlapped(const TYPE& start1,const TYPE& end1,const TYPE& start2,const TYPE& end2)
{
	return start1 <= end2 && start2 <= end1;	
}

/////////////////////////////////////////////////////////////////////////////
// StaffBreakDiagramViewer
//
StaffBreakDiagramViewer::StaffBreakDiagramViewer(const CTimeSpan& ropBreakPrintOffset)
: StaffDiagramViewer()
{
	omBreakPrintOffset = ropBreakPrintOffset;
	bmNoUpdatesNow     = FALSE;
}

StaffBreakDiagramViewer::~StaffBreakDiagramViewer()
{
}


BOOL StaffBreakDiagramViewer::GetBreakJobsNotPrinted(CCSPtrArray<JOBDATA>& ropBreakJobs)
{
	CTime olStartTime = ogBasicData.GetTime();
	CTime olEndTime   = olStartTime + omBreakPrintOffset;

	ogBasicData.Trace("Start Time = %s\n",(const char *)olStartTime.Format("%D:%H:%M"));
	ogBasicData.Trace("End   Time = %s\n",(const char *)olEndTime.Format("%D:%H:%M"));

	for (int ilGroupNo = 0; ilGroupNo < GetGroupCount(); ilGroupNo++)
	{
		for (int ilLineNo = 0; ilLineNo < GetLineCount(ilGroupNo); ilLineNo++)
		{
			STAFF_LINEDATA *prlLineData = GetLine(ilGroupNo,ilLineNo);
			if (prlLineData)
			{
				for (int ilBarNo = 0; ilBarNo < prlLineData->Bars.GetSize(); ilBarNo++)
				{
					STAFF_BARDATA *prlBar = &prlLineData->Bars[ilBarNo];
					if (prlBar)
					{
						if (IsOverlapped(olStartTime,olEndTime,prlBar->StartTime,prlBar->EndTime))
						{
							JOBDATA *prlJobData = ogJobData.GetJobByUrno(prlBar->JobUrno);
							if (prlJobData && strcmp(prlJobData->Jtco,JOBBREAK) == 0)
							{
								EMPDATA *prlEmpData = ogEmpData.GetEmpByPeno(prlJobData->Peno);
								if (prlEmpData)
								{
									ogBasicData.Trace("Name = %s \n",ogEmpData.GetEmpName(prlEmpData));
									ogBasicData.Trace("Pause= %s %s",(const char *)(prlJobData->Acfr.Format("%D:%H:%M")),(const char *)(prlJobData->Acto.Format("%D:%H:%M")));
								}

								if (!prlJobData->Printed)
								{
									ropBreakJobs.New(*prlJobData);
								}
							}
						}
					}
				}
			}
		}
	}
	return TRUE;
}