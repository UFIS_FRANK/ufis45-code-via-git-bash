// SetupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SetupDlg dialog
#ifndef _SETUP_DLG_
#define _SETUP_DLG_

#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <BasicData.h>
#include <CedaCfgData.h>


class CViewer;
class SetupDlg : public CDialog
{
// Construction
public:
	SetupDlg(CWnd* pParent = NULL);   // standard constructor
	~SetupDlg();

	CString		 omMode;
	CString		 omParameters1;
	CString		 omParameters2;
	int			 imCurrMonitors;
	USERSETUPDATA rmUserSetup; 
	CString		 omUser;
	
// Dialog Data
	//{{AFX_DATA(SetupDlg)
	enum { IDD = IDD_SETUP };
	int		m_Attention_RB;
	int		m_CCIChrt_RB;
	int		m_CCITbl_RB;
	int		m_Coverage_RB;
	int		m_Flightplan_RB;
	int		m_FlightJobsTable_RB;
	BOOL	m_Flights_CK;
	int		m_Gatechrt_RB;
	int		m_Gatetbl_RB;
	int		m_Info_RB;
	int		m_Conflict_RB;
	int		m_EqChart_RB;
	int		m_EqTable_RB;
	int		m_Monitors_RB;
	int		m_Preplant_RB;
	int		m_Requesttbl_RB;
	int		m_Resolution_RB;
	int		m_Staffchrt_RB;
	int		m_Stafflst_RB;
	BOOL	m_Turnaround_CK;
	BOOL	m_Shadows_CK;
	int		m_Monitors_RB1;
	int		m_RegnChrt_RB;
	int		m_Posichrt_RB;
	CComboBox	c_Attentiontbl;
	CComboBox	c_Stafflist;
	CComboBox	c_Staffchart;
	CComboBox	c_Requests;
	CComboBox	c_Preplanttbl;
	CComboBox	c_Gatechrt;
	CComboBox	c_Regnchrt;
	CComboBox	c_Poschrt;
	CComboBox	c_Gatebel;
	CComboBox	c_FlightJobsTable;
	CComboBox	c_Flightplan;
	CComboBox	c_conflicttbl;
	CComboBox	c_Ccichart;
	CComboBox	c_Prmtbl;
	CComboBox	c_CciBel;
	CComboBox	c_EquipmentChart;
	CComboBox	c_EquipmentList;
	CComboBox	c_PrmList;
	CString	m_Attentiontbl;
	CString	m_CCIBel;
	CString	m_CCIChrt;
	CString	m_Conflikttbl;
	CString	m_Flightplan;
	CString	m_FlightJobsTable;
	CString	m_Gatebel;
	CString	m_Gatechrt;
	CString	m_EquipmentChart;
	CString	m_EquipmentList;
	CString m_Poschrt;
	CString	m_Regnchrt;
	CString	m_Preplanttbl;
	CString	m_Requests;
	CString	m_Staffchart;
	CString	m_Stafflist;
	CString	m_PrmList;
	int		m_BreakPrintOffset;
	BOOL	m_PrintBreaks;
	CComboBox	c_Demandlist;
	CString	m_Demandlist;
	int		m_Demandlist_RB;
	CComboBox	c_FlIndDemandlist;
	CString	m_FlIndDemandlist;
	int		m_FlIndDemandlist_RB;
	CComboBox	c_CheckinDemandlist;
	CString	m_CheckinDemandlist;
	int		m_CheckinDemandlist_RB;
	//}}AFX_DATA

	void MakeCedaData();
	void MakeSetupStuct();
	int	 ResetDialog();
	int  MakeParameters();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
private:
	
	void UpdateComboBoxes();
	void FillCombosWithDefault();
	bool bmInit;
	void SetUpResolution ();		//hag20000612
	void EvaluateUpdateMode();
	void InitializeUserPreferences();

private:
	CRect omWindowRect; //PRF 8712

// Implementation
protected:
	void RestoreViews();

	// Generated message map functions
	//{{AFX_MSG(SetupDlg)
	virtual BOOL OnInitDialog();
	afx_msg void On1Monitors();
	afx_msg void On2MonitorsRb();
	afx_msg void On3MonitorsRb();
	afx_msg void OnSave();
	afx_msg void OnConflictB();
	afx_msg void OnCancel();
	afx_msg void OnNameConfig();
	afx_msg void OnFunctionColourConfig();
	afx_msg void OnOtherColourConfig();
	afx_msg void OnOtherSetup();
	afx_msg void OnTelexTypeConfig();
	afx_msg void OnFieldConfig();
	afx_msg void OnMove(int x, int y); //PRF 8712
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#endif  // _SETUP_DLG_
