// FlightJobsTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <FlightJobsTableViewer.h>
#include <FlightJobsTable.h>
#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <FlightTableSortPage.h>
#include <BasePropertySheet.h>
#include <FlightJobsTablePropertySheet.h>
#include <BasicData.h>
#include <ButtonList.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <dataset.h>
#include <FlightDetailWindow.h>
#include <cxbutton.h>
#include <process.h> // required for _spawnv
#include <DlgSettings.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTable dialog

FlightJobsTable::FlightJobsTable(CWnd* pParent /*=NULL*/)
	: CDialog(FlightJobsTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(FlightJobsTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

    pomTable = new CTable;
    pomTable->tempFlag = 2;
    pomTable->SetSelectMode(0);

    CDialog::Create(FlightJobsTable::IDD);
    CRect olRect;
    ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.GATB,ogCfgData.rmUserSetup.MONS);
    olRect.top += 40;
    BOOL blMinimized = FALSE;
    ogBasicData.GetDialogFromReg(olRect, COpssPmApp::FLIGHTJOBS_TABLE_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
    bmIsViewOpen = FALSE;

    CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

    omViewer.Attach(pomTable);
    UpdateView();
    if(blMinimized == TRUE)
    {
        ShowWindow(SW_MINIMIZE);		
    }
}

FlightJobsTable::~FlightJobsTable()
{
	TRACE("FlightJobsTable::~FlightJobsTable()\n");
	omViewer.Attach(NULL);
	delete pomTable;	
}

void FlightJobsTable::SetCaptionText(void)
{
	CString olCaptionText = GetString(IDS_FLIGHTJOBSTABLE_TITLE);
	SetWindowText(olCaptionText);
}

void FlightJobsTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightJobsTable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	//}}AFX_DATA_MAP
}

void FlightJobsTable::UpdateView()
{

	CString  olViewName = omViewer.GetViewName();
	olViewName = omViewer.GetViewName();

	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.FJTV;
	}
	
	
	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);
	SetCaptionText();
	
	//Singapore
	OnTableUpdateDataCount();
}

void FlightJobsTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.FJTV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

}

BEGIN_MESSAGE_MAP(FlightJobsTable, CDialog)
	//{{AFX_MSG_MAP(FlightJobsTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
	ON_CBN_SELCHANGE(IDC_VIEWCOMBO, OnSelchangeViewcombo)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_EXPORT, OnExport)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTable message handlers

BOOL FlightJobsTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING32986)); 

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	polWnd = GetDlgItem(IDC_EXPORT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_FJT_EXPORT));
	}
	
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	
	// TODO: Add extra initialization here
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

	UpdateComboBox();

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect); //Commented - Singapore PRF 8712

	m_DragDropTarget.RegisterTarget(this, this);

	// Register DDX call back function
	TRACE("FlightJobsTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("FLIGHTJOBSTABLE"),CString("Redisplay all"), FlightJobsTableCf);	// for what-if changes
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("FLIGHTJOBSTABLE"),CString("Global Date Change"), FlightJobsTableCf); // change the date of the data displayed


	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FlightJobsTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::FLIGHTJOBS_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
    ogCCSDdx.UnRegister(this, NOTUSED);
	CDialog::OnDestroy();
}

void FlightJobsTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
	AfxGetMainWnd()->SendMessage(WM_FLIGHTJOBSTABLE_EXIT);
   	pogButtonList->m_wndFlightJobsTable = NULL;
	pogButtonList->m_FlightJobsTableButton.Recess(FALSE);
}

void FlightJobsTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if (nType != SIZE_MINIMIZED)
	{
		pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
		GetWindowRect(&omWindowRect); //PRF 8712
		//MoveWindow(&omWindowRect);
	}
}

void FlightJobsTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();

	FlightJobsTablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		UpdateView();
	}
	bmIsViewOpen = FALSE;
}

void FlightJobsTable::OnSelchangeViewcombo() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	omViewer.SelectView(clText);
	UpdateView();		
}

void FlightJobsTable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
//    pomTable->GetCTableListBox()->SetFocus();
}

void FlightJobsTable::OnPrint() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();

	omViewer.PrintView();
}

BOOL FlightJobsTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// FlightJobsTable -- implementation of DDX call back function

void FlightJobsTable::FlightJobsTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	FlightJobsTable *polTable = (FlightJobsTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

//*****
/////////////////////////////////////////////////////////////////////////////
// FlightJobsTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a JobFlight.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create normal flight jobs for those staffs.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create a normal flight job for that staff.
//
LONG FlightJobsTable::OnDragOver(UINT wParam, LONG lParam)
{
return -1L;	// cannot interpret this object
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_DUTYBAR)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	int ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= omViewer.Lines() - 1))
		return -1L;	// cannot drop on a blank line

	FLIGHTJOBSTABLE_LINE *prlLine = omViewer.GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropFlightUrno = prlLine->FlightUrno;
	int ilColumn = pomTable->GetLinenoFromPoint(olDropPosition);
	imDropDemandUrno = ilColumn;
	return 0L;
}

LONG FlightJobsTable::OnDrop(UINT wParam, LONG lParam)
{ 
/*	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	int ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= omViewer.Lines() - 1))
		return -1L;	// cannot drop on a blank line

	FLIGHTJOBSTABLE_LINE *prlLine = omViewer.GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropFlightUrno = prlLine->FlightUrno;
	int ilColumn = pomTable->GetColumnnoFromPoint(olDropPosition);
	int ilDemandNo = (ilColumn-6)/2;
	bool blTeamAlloc = false; // assigning a whole team ?

	
	CDWordArray olPoolJobUrnos;
	int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
	if(ilDataCount > 0)
	{
		for(int ilPoolJob = 0; ilPoolJob < ilDataCount; ilPoolJob++)
		{
			long llPoolJobUrno = m_DragDropTarget.GetDataDWord(ilPoolJob);
			olPoolJobUrnos.Add(llPoolJobUrno);
		}
		blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false; // assigning a whole team ?
	}

	CString olGate = ogFlightData.GetGate(imDropFlightUrno);
	if ((ilDemandNo >= 0) && (ilDemandNo < prlLine->Indicators.GetSize()))
	{
		long llDemandUrno = prlLine->Indicators[ilDemandNo].DemandUrno;
		if (ogJodData.GetFirstJobUrnoByDemand(llDemandUrno) == 0L)
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(llDemandUrno);
			if(prlDemand != NULL)
			{
				ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, imDropFlightUrno, ALLOCUNITTYPE_GATE, ogFlightData.GetGate(imDropFlightUrno), blTeamAlloc, prlDemand);
				return 0L;
			}
		}
	}
	// create job flight from duty with (default) demand
	if(ilDataCount > 0)
	{
		ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, imDropFlightUrno, ALLOCUNITTYPE_GATE, ogFlightData.GetGate(imDropFlightUrno), blTeamAlloc);
	}
*/
	return 0L;
}

void FlightJobsTable::OnClose() 
{
   	pogButtonList->m_wndFlightJobsTable = NULL;
	pogButtonList->m_FlightJobsTableButton.Recess(FALSE);
	CDialog::OnClose();
}

void FlightJobsTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();
}

void FlightJobsTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	omViewer.SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	omViewer.SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void FlightJobsTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

void FlightJobsTable::SetDate(CTime opDate)
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(opDate));
	OnSelchangeDate();
}

LONG FlightJobsTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = omViewer.Lines();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		FLIGHTJOBSTABLE_LINE *prlLine = omViewer.GetLine(ipItem);
		if (prlLine != NULL)
		{
			long llArrUrno = 0L;
			long llDepUrno = 0L;
			if(!strcmp(prlLine->Adid,"A"))
				llArrUrno = prlLine->FlightUrno;
			else
				llDepUrno = prlLine->FlightUrno;
			new FlightDetailWindow(this,llArrUrno,llDepUrno,FALSE,"", NULL, PERSONNELDEMANDS);
		}
	}

	return 0L;
}

// On Excel button press ...
void FlightJobsTable::OnExport() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclSeperator[64];
//	char pclFunctions[500];	

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "EXCEL", "DEFAULT",pclExcelPath, sizeof pclExcelPath, pclConfigPath);
	GetPrivateProfileString(pcgAppName, "EXCELSEPARATOR", ";",pclSeperator, sizeof pclSeperator, pclConfigPath);
//	GetPrivateProfileString(pcgAppName, "FLIGHTJOBSLIST_EXPORT_FUNCTIONS", GetString(IDS_DEFAULTEXCELFUNCTIONS),pclFunctions, sizeof pclFunctions, pclConfigPath);

	CString olFunctions;
	if(!ogDlgSettings.GetValue("FlightJobsTable", "ExcelFunctions", olFunctions))
		olFunctions = GetString(IDS_DEFAULTEXCELFUNCTIONS);

	if (strcmp(pclExcelPath, "DEFAULT") == 0)
	{
//		MessageBox(GetString(IDS_STRING992),GetString(IDS_STRING145), MB_ICONERROR);
		return;
	}

	CWaitCursor olWait;

	
	CTime olCurrTime = CTime::GetCurrentTime();
	CString olFileName;
	olFileName.Format("%s\\FlightJobs%s.csv", CCSLog::GetTmpPath(), olCurrTime.Format("%Y%m%d%H%M%S"));
	omViewer.CreateExport(olFileName, pclSeperator, olFunctions);

	bool test = true; //only for testing error
	if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
	{
		if (olFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + olFileName;
			MessageBox(mess, "Error", MB_ICONERROR);
			return;
		}
		if (olFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + olFileName;
			MessageBox(mess, "Error", MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + olFileName;
		MessageBox(mess, "Error", MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, olFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );	// start excel application, load and display the output file !
}

void FlightJobsTable::OnTableUpdateDataCount()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			char olDataCount[10];
			polWnd->EnableWindow();			
			polWnd->SetWindowText(itoa(omViewer.Lines(),olDataCount,10));
		}
	}
}

//PRF 8712
void FlightJobsTable::OnMove(int x, int y)
{
	CDialog::OnMove(x, y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}
