// CreateDemand.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <CCSGlobl.h>
#include <CedaPfcData.h>
#include <CedaPerData.h>
#include <CedaJobData.h>
#include <CedaAzaData.h>
#include <CedaDemandData.h>
#include <CedaFlightData.h>
#include <RegnViewer.h>
#include <BasicData.h>
#include <CreateDemand.h>
#include <DataSet.h>

/////////////////////////////////////////////////////////////////////////////
// CreateDemand dialog
CreateDemand::CreateDemand(CWnd* pParent /*=NULL*/)
	: CDialog(CreateDemand::IDD, pParent)
{
	//{{AFX_DATA_INIT(CreateDemand)
	m_StartDate = NULL;
	m_StartTime = NULL;
	m_EndDate = NULL;
	m_EndTime = NULL;
	//}}AFX_DATA_INIT

}

CreateDemand::CreateDemand(REGN_BKBARDATA* prpBarFlight, CWnd* pParent /*=NULL*/, bool bpThirdParty /* = false */)
	: CDialog(CreateDemand::IDD, pParent)
{

	bmThirdParty = bpThirdParty;
	prmBarFlight = prpBarFlight;
	CDialog::Create(IDD, pParent);
	CenterWindow();

}

void CreateDemand::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_StartDate = omStartTime;
		m_StartTime = omStartTime;
		m_EndDate = omEndTime;
		m_EndTime = omEndTime;
	}

	//{{AFX_DATA_MAP(CreateDemand)
	DDX_Control(pDX, IDC_NUM_RESOURCE, m_NumRes);
	DDX_Control(pDX, IDC_COMBO_QUAL, m_ComboQual);
	DDX_Control(pDX, IDC_COMBO_FUNC, m_ComboFunc);
	DDX_CCSddmmyy(pDX, IDC_STARTDATE, m_StartDate);
	DDX_CCSTime(pDX, IDC_STARTTIME, m_StartTime);
	DDX_CCSddmmyy(pDX, IDC_ENDDATE, m_EndDate);
	DDX_CCSTime(pDX, IDC_ENDTIME, m_EndTime);
	DDX_Control(pDX, IDC_NAME_DEMAND, m_NameDemand);
	DDX_Control(pDX, IDC_CHECK_HANGAR, m_CheckHangar);
	//}}AFX_DATA_MAP


	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Convert the date and time back
		omStartTime = CTime(m_StartDate.GetYear(), m_StartDate.GetMonth(),
			m_StartDate.GetDay(), m_StartTime.GetHour(), m_StartTime.GetMinute(),
			m_StartTime.GetSecond());
		omEndTime = CTime(m_EndDate.GetYear(), m_EndDate.GetMonth(),
			m_EndDate.GetDay(),	m_EndTime.GetHour(), m_EndTime.GetMinute(),
			m_EndTime.GetSecond());
		if (omEndTime <= omStartTime)	// don't allow end time greater than start time
		{
			//CString prompt = "Ung�ltige Zeit";
			CString prompt = GetString(IDS_STRING61213);
			MessageBox(prompt, NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl(IDC_ENDDATE);
			pDX->Fail();
		}
	}
}


BEGIN_MESSAGE_MAP(CreateDemand, CDialog)
	//{{AFX_MSG_MAP(CreateDemand)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CreateDemand message handlers

BOOL CreateDemand::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33157));

	CWnd *polWnd = GetDlgItem(IDC_JOB_NAME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33158));
	}
	polWnd = GetDlgItem(IDC_JOB_START); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33159));
	}
	polWnd = GetDlgItem(IDC_JOB_END); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33160));
	}
	polWnd = GetDlgItem(IDC_JOB_FUNCTIONS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33161));
	}
	polWnd = GetDlgItem(IDC_JOB_QUAL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33162));
	}
	polWnd = GetDlgItem(IDC_JOB_RES); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33163));
	}
	polWnd = GetDlgItem(IDC_CHECK_HANGAR); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33164));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	m_NumRes.SetLimitText(1);
	m_NumRes.SetWindowText("1");

	if(bmThirdParty)
	{
		polWnd = GetDlgItem(IDC_NUM_RESOURCE); 
		if(polWnd != NULL)
		{
			polWnd->EnableWindow(false);
		}
		polWnd = GetDlgItem(IDC_CHECK_HANGAR); 
		if(polWnd != NULL)
		{
			polWnd->EnableWindow(false);
		}
	}

	FillComboQualifications();
	FillComboFunctions();

	//FLIGHTDATA* prlFlight = ogFlightData.GetFlightByUrno(prmBarFlight->FlightUrno);

	m_NameDemand.SetLimitText(6);

	omStartTime = prmBarFlight->StartTime + CTimeSpan(0,0,10,0);
	omEndTime = omStartTime + CTimeSpan(0,0,ogBasicData.imStandardLenghtDemand,0);
	

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CreateDemand::OnOK() 
{
	if(UpdateData(TRUE))
	{
		CString olNameDemand;
		m_NameDemand.GetWindowText(olNameDemand);
		if (olNameDemand.IsEmpty())
		{
			MessageBox(GetString(IDS_CREATEDEMNAME)); // "Demand Name is missing"
			m_NameDemand.SetFocus();
			return;
		}

		CString olNumRes;
		CString olQual;
		CString olFunc;
		int ilCur;
		m_NumRes.GetWindowText(olNumRes);
		if (atoi(olNumRes) > 0 && atoi(olNumRes) < 4)
		{
			if ((ilCur = m_ComboQual.GetCurSel()) > -1)
			{
				long olUrno = (long)m_ComboQual.GetItemData(ilCur);
				PERDATA * prlPer = ogPerData.GetPerByUrno(olUrno);
				for (int i = 0; i < atoi(olNumRes); i++)
				{
					olQual = CString(prlPer->Prmc) + ",";
				}
				olQual = olQual.Mid(0,olQual.GetLength()-1);
			}

			if ((ilCur = m_ComboFunc.GetCurSel()) > -1)
			{
				long olUrno = (long)m_ComboFunc.GetItemData(ilCur);
				PFCDATA * prlPfc = ogPfcData.GetPfcByUrno(olUrno);
				olFunc = prlPfc->Fctc;
				for (int i = 0; i < atoi(olNumRes); i++)
				{
					olFunc = CString(prlPfc->Fctc) + ",";
				}
				olFunc = olFunc.Mid(0,olFunc.GetLength()-1);
			}
		}
		else olNumRes = "0";

		CString olPoty;
		if (m_CheckHangar.GetCheck() == 0)
			olPoty = "T"; // linea
		else
			olPoty = "H"; // hangar



		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prmBarFlight->FlightUrno);
		if(prlFlight == NULL)
		{
			CString olText;
			olText.Format("Error flight URNO %s not found!",prmBarFlight->FlightUrno);
			MessageBox(olText,"");
		}
		else
		{
			AZADATA * prlAza = new AZADATA;
			prlAza->Acbe = omStartTime;
			prlAza->Acen = omEndTime;
			strcpy(prlAza->Acst, "0");
			strcpy(prlAza->Act3,prlFlight->Act3);
			strcpy(prlAza->Azso, "OPSSPM");
			strcpy(prlAza->Func, olFunc);
			strcpy(prlAza->Squa, olQual);
			strcpy(prlAza->Nres, olNumRes);
			strcpy(prlAza->Regn, prlFlight->Regn);
			strcpy(prlAza->Poty, olPoty);
			strcpy(prlAza->Prio, ogBasicData.omPriorityForDemand);
			strcpy(prlAza->Seco, olNameDemand);

			if(bmThirdParty)
			{
				if(ogAzaData.InsertAzaForThirdParty(prlAza))
				{
					ogDataSet.CreateThirdPartyDemand(prlFlight,prlAza);
				}
			}
			else
			{
				ogAzaData.Insert(prlAza);
			}
		}
		
		CDialog::OnOK();
	}
}

bool CreateDemand::IsValidTime(CString opTime, CTime &opTimeFormat)
{
	int Ore, Min;
	if (opTime.GetLength() == 4)
	{	
		Min = atoi(opTime.Mid(2,2));
	}
	else
	if (opTime.GetLength() == 5)
	{
		Min = atoi(opTime.Mid(3,2));
	}
	else return false;

	Ore = atoi(opTime.Mid(0,2));

	if (Ore >= 0 && Ore <= 23 && Min >= 0 && Min <= 59)
	{
		CTime olFlightTime = prmBarFlight->StartTime;
		opTimeFormat = CTime(olFlightTime.GetYear(), 
							 olFlightTime.GetMonth(),
							 olFlightTime.GetDay(),
							 Ore, Min, 0);
		if (olFlightTime.GetDay() != prmBarFlight->EndTime.GetDay() && 
			opTimeFormat + CTimeSpan(1,0,0,0) < prmBarFlight->EndTime + CTimeSpan(0,2,0,0))
		{
			opTimeFormat += CTimeSpan(1,0,0,0);
		}					 
		return true;
	}
	else
		return false;

}

void CreateDemand::FillComboQualifications()
{
	int ilNumPer = ogPerData.omData.GetSize();
	for (int i = 0; i < ilNumPer; i++)
	{
		PERDATA * prlPer = &ogPerData.omData[i];
		int ilItem = m_ComboQual.AddString((CString)prlPer->Prmn);
		if (ilItem >= 0)
			m_ComboQual.SetItemData(ilItem, (DWORD)prlPer->Urno);
	}
}

void CreateDemand::FillComboFunctions()
{
	int ilNumPfc = ogPfcData.omData.GetSize();
	for (int i = 0; i < ilNumPfc; i++)
	{
		PFCDATA * prlPfc = &ogPfcData.omData[i];
		int ilItem = m_ComboFunc.AddString((CString)prlPfc->Fctn);
		if (ilItem >= 0)
			m_ComboFunc.SetItemData(ilItem, (DWORD)prlPfc->Urno);
	}
}


