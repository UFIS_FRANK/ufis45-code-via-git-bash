#if !defined(AFX_SPLITJOBDLG_H__91A30483_408B_11D6_8146_00010215BFE5__INCLUDED_)
#define AFX_SPLITJOBDLG_H__91A30483_408B_11D6_8146_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SplitJobDlg.h : header file
//
#include <CCSGlobl.h>

/////////////////////////////////////////////////////////////////////////////
// CSplitJobDlg dialog

class CSplitJobDlg : public CDialog
{
// Construction
public:
	CSplitJobDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSplitJobDlg)
	enum { IDD = IDD_SPLITJOBDLG };
	CButton	m_SecondJob;
	CButton	m_FirstJob;
	CTime		m_1FromDate;
	CTime		m_1FromTime;
	CTime		m_1ToDate;
	CTime		m_1ToTime;
	CTime		m_2FromDate;
	CTime		m_2FromTime;
	CTime		m_2ToDate;
	CTime		m_2ToTime;
	CString	m_1FromTitle;
	CString	m_1ToTitle;
	CString	m_2FromTitle;
	CString	m_2ToTitle;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplitJobDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSplitJobDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnKillfocus1todate();
	afx_msg void OnKillfocus1totime();
	afx_msg void OnKillfocus2fromdate();
	afx_msg void OnKillfocus2fromtime();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CTime om1From, om1To, om2From, om2To;
	bool bmRc;
	CString omAdditionalText;
	bool bmCanChangeStartTime, bmCanChangeEndTime;

public:
	void InitJobTimes(CString opAdditionalText, CTime opJobStart, CTime opJobEnd, CTime opInitSplitTime = TIMENULL, bool bpCanChangeStartTime = true, bool bpCanChangeEndTime = true);
	void GetJobTimes(CTime &ropJob1Start, CTime &ropJob1End, CTime &ropJob2Start, CTime &ropJob2End);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPLITJOBDLG_H__91A30483_408B_11D6_8146_00010215BFE5__INCLUDED_)
