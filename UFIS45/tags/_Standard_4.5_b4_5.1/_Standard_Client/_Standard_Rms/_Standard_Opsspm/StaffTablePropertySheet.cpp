// stafflps.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaRnkData.h>
#include <CedaPerData.h>
#include <CedaShiftData.h>
#include <CedaShiftTypeData.h>
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <StaffTableSortPage.h>
#include <StaffTableFilterPage.h>
#include <BasePropertySheet.h>
#include <StaffTablePropertySheet.h>
#include <CedaWgpData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StaffTablePropertySheet
//

//StaffTablePropertySheet::StaffTablePropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_STAFF_TABLE, pParentWnd, popViewer, iSelectPage)
StaffTablePropertySheet::StaffTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61588), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageRank);
	AddPage(&m_pagePermits);
	AddPage(&m_pageShiftCode);
	AddPage(&m_pageTeam);
	AddPage(&m_pageSort);
	AddPage(&m_pageFilter);

	// Change the caption in tab control of the PropertySheet
	//m_pageRank.SetCaption(ID_PAGE_FILTER_RANK);
	m_pageRank.SetCaption(GetString(IDS_STRING61604));
	m_pagePermits.SetCaption(GetString(IDS_PERMIT_TAB));
	//m_pageShiftCode.SetCaption(ID_PAGE_FILTER_SHIFTCODE);
	m_pageShiftCode.SetCaption(GetString(IDS_STRING61605));
	//m_pageTeam.SetCaption(ID_PAGE_FILTER_TEAM);
	m_pageTeam.SetCaption(GetString(IDS_STRING61606));

	// Prepare possible values for each PropertyPage
	ogRnkData.GetAllRanks(m_pageRank.omPossibleItems);
	m_pageRank.bmSelectAllEnabled = true;
	ogPerData.GetAllPermits(m_pagePermits.omPossibleItems);
	m_pagePermits.bmSelectAllEnabled = true;
	ogShiftTypes.GetAllShiftTypes(m_pageShiftCode.omPossibleItems);
	m_pageShiftCode.bmSelectAllEnabled = true;
	ogWgpData.GetAllTeams(m_pageTeam.omPossibleItems);
	m_pageTeam.omPossibleItems.Add(GetString(IDS_NOWORKGROUP));
	m_pageTeam.bmSelectAllEnabled = true;

	bmOldHideAbsentEmps = FALSE;
	bmOldHideStandbyEmps = FALSE;
	bmOldHideFidEmps = FALSE;
	imOldShiftTime = 0;
}

void StaffTablePropertySheet::LoadDataFromViewer()
{
	if(!pomViewer->GetFilter("Dienstrang", m_pageRank.omSelectedItems))
	{
		m_pageRank.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Permits", m_pagePermits.omSelectedItems))
	{
		m_pagePermits.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Schichtcode", m_pageShiftCode.omSelectedItems))
	{
		m_pageShiftCode.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Team", m_pageTeam.omSelectedItems))
	{
		m_pageTeam.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}


	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.m_Group = pomViewer->GetGroup()[0] - '0';

	CString olHideAbsentEmps = pomViewer->GetUserData("HIDEABSENTEMPS");
	if (olHideAbsentEmps.IsEmpty())
		m_pageFilter.m_HideAbsentEmps = false;
	else
		m_pageFilter.m_HideAbsentEmps = olHideAbsentEmps == "YES" ? TRUE : FALSE;
	bmOldHideAbsentEmps = m_pageFilter.m_HideAbsentEmps;

	CString olHideFidEmps = pomViewer->GetUserData("HIDEFIDEMPS");
	if (olHideFidEmps.IsEmpty())
		m_pageFilter.m_HideFidEmps = false;
	else
		m_pageFilter.m_HideFidEmps = olHideFidEmps == "YES" ? TRUE : FALSE;
	bmOldHideFidEmps = m_pageFilter.m_HideFidEmps;

	CString olHideStandbyEmps = pomViewer->GetUserData("HIDESTANDBYEMPS");
	if (olHideStandbyEmps.IsEmpty())
		m_pageFilter.m_HideStandbyEmps = false;
	else
		m_pageFilter.m_HideStandbyEmps = olHideStandbyEmps == "YES" ? TRUE : FALSE;
	bmOldHideStandbyEmps = m_pageFilter.m_HideStandbyEmps;

	CString olShiftTime = pomViewer->GetUserData("SHIFTTIME");
	if (olShiftTime.IsEmpty())
		m_pageFilter.m_ShiftTime = 2;
	else
		m_pageFilter.m_ShiftTime = atoi(olShiftTime);
	imOldShiftTime = m_pageFilter.m_ShiftTime;
}

void StaffTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Dienstrang", m_pageRank.omSelectedItems);
	pomViewer->SetFilter("Permits", m_pagePermits.omSelectedItems);
	pomViewer->SetFilter("Schichtcode", m_pageShiftCode.omSelectedItems);
	pomViewer->SetFilter("Team", m_pageTeam.omSelectedItems);
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetGroup(CString(m_pageSort.m_Group + '0'));
//	pomViewer->SetFilter("Zeit", m_pageZeit.omFilterValues);
	pomViewer->SetUserData("HIDEABSENTEMPS",m_pageFilter.m_HideAbsentEmps ? "YES" : "NO");
	pomViewer->SetUserData("HIDEFIDEMPS",m_pageFilter.m_HideFidEmps ? "YES" : "NO");
	pomViewer->SetUserData("HIDESTANDBYEMPS",m_pageFilter.m_HideStandbyEmps ? "YES" : "NO");
	CString olTmp; olTmp.Format("%d", m_pageFilter.m_ShiftTime);
	pomViewer->SetUserData("SHIFTTIME",olTmp);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int StaffTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedRanks;
	CStringArray olSelectedPermits;
	CStringArray olSelectedShifts;
	CStringArray olSelectedTeams;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Dienstrang", olSelectedRanks);
	pomViewer->GetFilter("Permits", olSelectedPermits);
	pomViewer->GetFilter("Schichtcode", olSelectedShifts);
	pomViewer->GetFilter("Team", olSelectedTeams);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();

	if (!IsIdentical(olSelectedRanks, m_pageRank.omSelectedItems) ||
		!IsIdentical(olSelectedPermits, m_pagePermits.omSelectedItems) ||
		!IsIdentical(olSelectedShifts, m_pageShiftCode.omSelectedItems) ||
		!IsIdentical(olSelectedTeams, m_pageTeam.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0') ||
		bmOldHideAbsentEmps != m_pageFilter.m_HideAbsentEmps ||
		bmOldHideFidEmps != m_pageFilter.m_HideFidEmps ||
		bmOldHideStandbyEmps != m_pageFilter.m_HideStandbyEmps ||
		imOldShiftTime != m_pageFilter.m_ShiftTime)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
