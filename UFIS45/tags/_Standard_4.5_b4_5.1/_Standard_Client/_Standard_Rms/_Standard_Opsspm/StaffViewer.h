// StaffViewer.h : header file
//

#ifndef _STVIEWER_H_
#define _STVIEWER_H_

#include <cviewer.h>
#include <CCSPrint.h>
#include <CedaJodData.h>
#include <CedaSwgData.h>
#include <CedaDrgData.h>
#include <CedaPdaData.h>
#include <excel9.h>

// Groupping definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
	GROUP_BY_POOL,
	GROUP_BY_TEAM,
	GROUP_BY_SHIFTCODE
};

struct STAFF_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};
struct STAFF_BARDATA 
{
	long JobUrno;
    CString Text;
	CString StatusText;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
	COLORREF FrameColor;
	bool Selected;
    int MarkerType;
    CBrush *MarkerBrush;
	bool OfflineBar;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<STAFF_INDICATORDATA> Indicators;
	bool Infm; // true if the emp has been informed of the job but the job has not yet started
	bool Ackn; // true if the emp has acknowledged the information
	bool IsPause;
	COLORREF PauseColour;
	bool DifferentFunction;
	int FlightType;
	long FlightUrno;

	STAFF_BARDATA(void)
	{
		JobUrno = 0L;
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		FrameColor = BLACK;
		Infm = false;
		Ackn = false;
		Selected = false;
		IsPause = false;
		OfflineBar = false;
		DifferentFunction = false;
		FlightUrno = 0L;
	}
};
struct STAFF_BKBARDATA
{
	CString Type; // POOLJOB or TEMPABSENCE
    CString Text;
	CString StatusText;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
	COLORREF FrameColor;
	COLORREF TextColor;
	long JobUrno; // Urno of the pool job
    CBrush *MarkerBrush;	// background bar will be only displayed in FRAMENONE, MARKFULL
	bool OfflineBar;

	STAFF_BKBARDATA(void)
	{
		FrameColor = BLACK;
		TextColor = 0L;
		OfflineBar = false;
	}

	STAFF_BKBARDATA& STAFF_BKBARDATA::operator=(const STAFF_BKBARDATA& rhs)
	{
		if (&rhs != this)
		{
			Type = rhs.Type;
			Text = rhs.Text;
			StatusText = rhs.StatusText;
			StartTime = rhs.StartTime;
			EndTime = rhs.EndTime;
			FrameType = rhs.FrameType;
			FrameColor = rhs.FrameColor;
			TextColor = rhs.TextColor;
			JobUrno = rhs.JobUrno;
			MarkerBrush = rhs.MarkerBrush;
			OfflineBar = rhs.OfflineBar;
		}
		return *this;
	}
};


// Contains data about a compressed job (several jobs compressed together to make one) when only the team leader is displayed
struct COMPRESSED_JOBDATA
{
	CCSPtrArray <JOBDATA> OriginalJobs; // list of which jobs were used to make this compressed job
	JOBDATA Data;				// the final result of compressing several jobs together
};


struct TEAM_DATA
{
	CString TeamName;					// team (team) name from either SWGTAB.CODE or DRGTAB.WGPC

	long TeamLeaderPoolJobUrno;			// Pool Job URNO of the team leader
	CString TeamLeaderSortString;		

	long FirstEmpPoolJobUrno;			// Pool Job URNO of the team leader (if there is no team leader then this is the URNO of the first emp)
	CTime Begin;						// Begin of the earliest pool job in this team - for others to be in this team, their shifts must overlap
	CTime End;							// End of the earliest pool job in this team - for others to be in this team, their shifts must overlap
	CString NameOfFirstEmp;				// Last Name + First Name of the first emp

	CTime SortStartDate, SortEndDate;

	// if true then when the team is compressed a '-' will be displayed indicating that the team isn't present for the whole team shift
	bool UnderStaffed;

	// if true then when the team is compressed a '+' will be displayed indicating that the team has employees from other groups delegated to it
	bool OverStaffed;

	// true if only the team leader (or first emp if theres is no team leader) is displayed
	bool IsCompressed;

	// PoolJobs of team members (including the team leader)
	CCSPtrArray <JOBDATA> PoolJobs;

	// several pool jobs compressed together to create one (or more when there are Abordnungen) (when only the team leader is displayed)
	CCSPtrArray <COMPRESSED_JOBDATA> CompressedPoolJobs; 

	// compressed temp absence
	CCSPtrArray <COMPRESSED_JOBDATA> CompressedTempAbsences; 

	// Contains data about a compressed job (several jobs compressed together to make one)
	CCSPtrArray <COMPRESSED_JOBDATA> CompressedJobs;

	int GroupNo;

	TEAM_DATA(void)
	{
		UnderStaffed = false;
		OverStaffed = false;
		TeamName.Empty();
		TeamLeaderPoolJobUrno = 0L;
		FirstEmpPoolJobUrno = 0L;
		IsCompressed = false;
		Begin = TIMENULL;
		End = TIMENULL;
		SortStartDate = TIMENULL;
		SortEndDate = TIMENULL;
		GroupNo = -1;
	}
};


// used for re-displaying teams when pool job updates are received
struct POOLJOBLINE
{
	long PoolJobUrno;
	int Groupno;
	int Lineno;
	int NewGroupno;
	int NewLineno;
	bool SourceTeamIsCompressed;
	bool TargetTeamIsCompressed;

	POOLJOBLINE(void)
	{
		PoolJobUrno = 0;
		Groupno = -1;
		Lineno = -1;
		NewGroupno = -1;
		NewLineno = -1;
		SourceTeamIsCompressed = false;
		TargetTeamIsCompressed = false;
	}
};

struct STAFF_LINEDATA 
{
	long	JobUrno;
	long	StfUrno;
	CTime	ShiftAcfr;
	CTime	ShiftActo;
	CString ShiftStid;
	CString ShiftTmid;
	CString EmpName;
	CString EmpRank;
	CString EmpFunction;
	CString DoubleTourStudent;	// if emp is a double-tour teacher then this contains the student name
	CString SortString;
	bool	TeamFound;			// flag used when creating the team - if true then this pool job's team has been found and will be inserted when the next team is found
	bool    HasPda;
    CString Text;
	COLORREF TextColor;
	bool	TextColorDefined;
    CString StatusText;
	CString MultiFunctions;
    int		MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
	int		GroupFunctionWeight; // function order within a work group type - used for sorting emps within a workgroup
    CCSPtrArray<STAFF_BARDATA> Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<STAFF_BKBARDATA> BkBars;	// background bar

	STAFF_LINEDATA(void)
	{
		JobUrno = -1;
		StfUrno = -1;
		TeamFound = false;
		MaxOverlapLevel = 0;
		GroupFunctionWeight = 9999;
		TextColor = BLACK;
		TextColorDefined = false;
		HasPda = false;
	}

	STAFF_LINEDATA& STAFF_LINEDATA::operator=(const STAFF_LINEDATA& rhs)
	{
		if (&rhs != this)
		{
			JobUrno = rhs.JobUrno;
			StfUrno = rhs.StfUrno;
			ShiftAcfr = rhs.ShiftAcfr;
			ShiftActo = rhs.ShiftActo;
			ShiftStid = rhs.ShiftStid;
			ShiftTmid = rhs.ShiftTmid;
			EmpName = rhs.EmpName;
			EmpRank = rhs.EmpRank;
			EmpFunction = rhs.EmpFunction;
			DoubleTourStudent = rhs.DoubleTourStudent;
			SortString = rhs.SortString;
			TeamFound = rhs.TeamFound;
			HasPda = rhs.HasPda;
			Text = rhs.Text;
			TextColor = rhs.TextColor;
			TextColorDefined = rhs.TextColorDefined;
			StatusText = rhs.StatusText;
			MultiFunctions = rhs.MultiFunctions;
			MaxOverlapLevel = rhs.MaxOverlapLevel;
			GroupFunctionWeight = rhs.GroupFunctionWeight;

			while(Bars.GetSize() > 0)
			{
				Bars[0].Indicators.DeleteAll();
				Bars.DeleteAt(0);
			}
			BkBars.DeleteAll();

			for(int i = 0; i < rhs.Bars.GetSize(); i++)
			{
				Bars.NewAt(i,rhs.Bars[i]);
			}

			for(int j = 0; j < rhs.BkBars.GetSize(); j++)
			{
				BkBars.NewAt(j,rhs.BkBars[j]);
			}
		}		
		return *this;
	}
};
struct STAFF_MANAGERDATA
{
	long JobUrno;			// from JOBCKI.URNO (of the job for FM/AM)
	// these data are for displaying the TopScaleText for each group in StaffDiagram
	CString ShiftStid;
	CString ShiftTmid;
	CString EmpLnam;
};
struct STAFF_GROUPDATA 
{
	CString GroupId;		// Pool ID or Team ID or ShiftCode depends on groupping
	CCSPtrArray<STAFF_MANAGERDATA> Managers;
	// standard data for groups in general Diagram
    CString Text;
    CCSPtrArray<STAFF_LINEDATA> Lines;

	//Singapore Modifictaion
	STAFF_GROUPDATA* pomParentGroupData;
	STAFF_GROUPDATA* pomChildGroupData;

	STAFF_GROUPDATA()
	{
		pomParentGroupData = NULL;
		pomChildGroupData = NULL;
	}
};

struct EXCEL_EXPORT_PAGE_DIMENSIONS
{
	float fmCellHeight;
	float fmCellWidth;
	float fmPoolPrintStartHeight;
	float fmPrintPageWidth;
	float fmPrintPageHeight;
	float fmEmployeePoolPrintWidth;
	float fmPoolJobPrintWidth;
	int   imTimeScaleBreakUpSteps;
	float fmTimeTenMinStepWidth;
	float fmTimeOneMinStepWidth;
	float fmTimeLongLineHeight;
	float fmTimeMediumLineHeight;
	float fmTimeShortLineHeight;
	float fmGanttLineHeight;
	float fmBarHeight;
	bool  bmReset;
	CTime omActualStartTime;
	CTime omActualEndTime;

	EXCEL_EXPORT_PAGE_DIMENSIONS()
	{
		fmCellHeight = 0;
		fmCellWidth = 0;
		fmPoolPrintStartHeight = 0;
		fmPrintPageWidth = 0;
		fmPrintPageHeight = 0;
		fmEmployeePoolPrintWidth = 0;
		fmPoolJobPrintWidth = 0;
		imTimeScaleBreakUpSteps = 0;
		fmTimeTenMinStepWidth = 0;
		fmTimeOneMinStepWidth = 0;
		fmTimeLongLineHeight = 0;
		fmTimeMediumLineHeight = 0;
		fmTimeShortLineHeight = 0;
		fmGanttLineHeight = 0;
		fmBarHeight = 0;
		bmReset = TRUE;
		omActualStartTime = TIMENULL;
		omActualEndTime = TIMENULL;
	}
};

struct STAFF_SELECTION
{
	int imGroupno;
	int imLineno;
	int imBarno;
	long lmUserData;
};

enum enumStfFlightType {STFBAR_ARRIVAL, STFBAR_DEPARTURE, STFBAR_TURNAROUND};

// Attention:
// Element "TimeOrder" in the struct "STAFF_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array STAFF_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the STAFF_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array STAFF_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// StaffDiagramViewer window

class StaffGantt;
typedef struct DrsDataStruct DRSDATA;

class StaffDiagramViewer: public CViewer
{
	friend StaffGantt;

// Constructions
public:
    StaffDiagramViewer();
    ~StaffDiagramViewer();

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);

// Internal data processing routines
private:
	static void StaffDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();
	bool IsPassPoolJobFilter(JOBDATA *prpPoolJob);
	bool IsPassPoolJobFilter(JOBDATA *prpPoolJob, SHIFTDATA *prpShift, EMPDATA *prpEmp, CString &ropTeamName, CString &ropFunction, CStringArray &ropPermits);
	bool IsPassFilter(const char *pcpPoolId, const char *pcpTeamId, 
		const char *pcpShiftCode,const char *pcpRank,bool bpIsAbsent, CStringArray &ropPermits);
	bool IsCompletelyDelegatedPoolJob(JOBDATA *prpPoolJob);
	bool IsInTimeframe(JOBDATA *prpPoolJob);
	int CompareGroup(STAFF_GROUPDATA *prpGroup1, STAFF_GROUPDATA *prpGroup2);
	int CompareManager(STAFF_MANAGERDATA *prpManager1, STAFF_MANAGERDATA *prpManager2);
	CString MakeSortString(STAFF_LINEDATA *prpLine);

	void MakeGroups();
	void MakeLinesAndManagers();
	STAFF_BKBARDATA *MakeBkBars(STAFF_LINEDATA *prpLine, JOBDATA *prpJob, SHIFTDATA *prpShift);
	void MakeCompressedBkBars(STAFF_LINEDATA *prpLine, COMPRESSED_JOBDATA *prpCompressedJob, SHIFTDATA *prpShift);
	void MakeBars();
	bool MakePoolJob(JOBDATA *prpJob, bool bpSort = true);
	void MakeTempAbsence(JOBDATA *prpJob);
	void MakeTempAbsenceBkBar(STAFF_LINEDATA *prpLine, JOBDATA *prpJob);
	void MakeBarData(STAFF_BARDATA *prlBar, JOBDATA *prpJob);
	void MakeBar(int ipGroupno, int ipLineno, JOBDATA *prpJob);
	void MakeBkBarsForPoolJob(int ipGroup, int ipLine, long lpPoolJobUrno);
	void MakeBarsForPoolJob(int ipGroup, int ipLine, long lpPoolJobUrno);

	CString GroupText(const char *pcpGroupId);
	CString LineText(JOBDATA *prpJob, SHIFTDATA *prpShift, EMPDATA *prpEmp);
	CString BarText(JOBDATA *prpJob);
	CString StatusText(JOBDATA *prpJob);

public:

	int FindGroup(const char *pcpPoolId, const char *pcpTeamId, const char *pcpShiftCode);
	BOOL FindManager(long lpUrno, int &ripGroupno, int &ripManagerno);
	BOOL FindCompressedDutyBar(long lpUrno, int &ripGroupno, int &ripLineno, bool blIsPause = false);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool IsNearlyWithIn(CTime opStart1, CTime opEnd1, CTime opStart2, CTime opEnd2);
	BOOL FindGroupAndLine(long lpUrno,int& ripGroupno,int& ripLineno);
	BOOL FindLine(long lpUrno,int ipGroupno,int &ripLineno);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno, bool bpIsPause = false, bool bpSearchWithinTeam = false);
	void UpdateDisplayLineForUrnoChange(long lpOldUrno, TEAM_DATA *prpTeam);

	// Operations
	bool ContainsPausenlage(STAFF_LINEDATA *prpLine);
	bool PausenlageNotAlreadyDrawn(STAFF_LINEDATA *prpLine);
	bool JobBelongsToThisLine(STAFF_LINEDATA *prpLine, long lpPoolJobUrno);
    int GetGroupCount();
    STAFF_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetManagerCount(int ipGroupno);
	STAFF_MANAGERDATA *GetManager(int ipGroupno, int ipManagerno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
    int GetLineCount(int ipGroupno);
    STAFF_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
	CString GetLineStatusText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
	int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    STAFF_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
	STAFF_BKBARDATA *GetBkBarByJobUrno(int ipGroupno, int ipLineno, long lpJobUrno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    STAFF_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	CString GetStatusPoolJobBkBarText(int ipGroupno, int ipLineno, int ipPoolJobBkBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	STAFF_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,int ipIndicatorno);

    void DeleteAll();
    int CreateGroup(STAFF_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
	int CreateManager(int ipGroupno, STAFF_MANAGERDATA *prpManager);
	void DeleteManager(int ipGroupno, int ipManagerno);
    STAFF_LINEDATA *CreateLine(int ipGroupno, STAFF_LINEDATA *prpLine, bool bpSort = true);
	bool IsNotShiftDelegation(JOBDATA *prpJob);
    void DeleteLine(int ipGroupno, int ipLineno);
	void DeleteBarsForLine(STAFF_LINEDATA *prpLine);
    int CreateBkBar(int ipGroupno, int ipLineno, STAFF_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
	void DeleteBkBarsByJob(int ipGroupno, int ipLineno, long lpJobUrno);
    int CreateBar(int ipGroupno, int ipLineno, STAFF_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	STAFF_INDICATORDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
    int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
    int GetPoolJobFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	int PoolJobCount(int ipGroupno, int ipLineno);
	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"

	COLORREF GetColourForDepFlight(long lpFlightUrno);
	COLORREF GetColourForArrFlight(long lpFlightUrno);
	bool DisplayMarker(CString opMarkerType);
// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);

// Attributes used for filtering condition
private:
	CStringArray omSortOrder;
	CMapStringToPtr omCMapForPool;
	CMapStringToPtr omCMapForTeam;
	CMapStringToPtr omCMapForShiftCode;
	CMapStringToPtr omCMapForRank;
	CMapStringToPtr omCMapForPermits;
	CTime omStartTime;
	CTime omEndTime;
	CMapPtrToPtr omUstfMap;
	CMapWordToPtr omMapPageNoToPreviewDataList; //PRF 8704
	EXCEL_EXPORT_PAGE_DIMENSIONS omXlPgDmns;

// Attributes
private:
	CWnd *pomAttachWnd;
	CBrush omBkBrush;
	CBrush omBkTextBrush;
	CBrush omTempAbsenceBrush;
	CBrush omBkPausenlageBrush;
	CBrush omConfirmedBkBrush;
	CBrush omConfirmedBkPausenlageBrush;
	CBrush omFinishedBkBrush;
	CBrush omFinishedBkPausenlageBrush;
	CBrush omAbsenceBkBrush;
	CBrush omAbsenceBkPausenlageBrush;
	CBrush omDelegationBrush;
	CBrush omWIFBkBrush;
    CCSPtrArray <STAFF_GROUPDATA> omGroups;
	bool bmUseAllShiftCodes ;
	bool bmUseAllTeams;
	bool bmEmptyTeamSelected;
	bool bmUseAllPools;
	bool bmUseAllRanks;
	bool bmUseAllPermits;

	CMapPtrToPtr omSelectedBars;
	void SelectBar(STAFF_BARDATA *prpBar, bool bpSelect = true);
	void CheckSelectBar(STAFF_BARDATA *prpBar);
	void DeselectAllBars();
	void SelectedJobDeleted(JOBDATA *prpJob);
	void SelectedJobDeleted(long lpJobUrno);
	int GetSelectedJobs(CCSPtrArray <JOBDATA> &ropJobs);

// Methods which handle changes (from Data Distributor)
private:
	void ProcessJodChange(JODDATA *prpJod);
	void ProcessJobNew(JOBDATA *prpJob);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessJobDelete(JOBDATA *prpJob);
	void ProcessShiftChange(SHIFTDATA *prpShift);
	void ProcessShiftSelect(SHIFTDATA *prpShift);
	void ProcessDrsChange(DRSDATA *prpDrs);
	void ProcessDrsDelete(long lpDrru);
	void ProcessEquipmentFastLinkUpdate(JOBDATA *prpJob);
	void ProcessSwgChange(SWGDATA *prpSwg);
	void ProcessDrgChange(DRGDATA *prpDrg);
	void ProcessPdaChange(PDADATA *prpPda);

	bool ProcessPoolJobUpdate(JOBDATA *prpPoolJob);
	void AddTeamLinesToPoolJobLines(TEAM_DATA *prpTeam, CCSPtrArray <POOLJOBLINE> &ropPoolJobLines);
	void AddPoolJobToPoolJobLines(long lpPoolJobUrno, CCSPtrArray <POOLJOBLINE> &ropPoolJobLines, int ipGroupno = -1, int ipLineno = -1);
	void ResortDisplayLines(long lpPoolJobUrno);
	void UpdateDisplayLinesForPoolJobs(CCSPtrArray <POOLJOBLINE> &ropPoolJobLines);

	bool bmDisableMessaging;
	bool MessagingActivated(void);

	void ProcessRefresh(void);

public:
	BOOL bmNoUpdatesNow;
	BOOL bmIsFirstGroupOnPage;

	static const CString Terminal2;
	static const CString Terminal3;


///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:
	CCSPrint *pomPrint;

	void PrintStaffDiagramHeader(int ipGroupno);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
		CCSPtrArray<PRINTBARDATA> &ropBkBars);
	void PrintManagers(int ipGroupno);
	void PrintPool(CPtrArray &opPtrArray,int ipGroupNo);
	CMapWordToPtr omMapGroupNoToNull; //Singapore
	BOOL GetAllManagers(const int ipGroupno,CString& ropAllManagers /*OUT*/); //PRF 8704
	int  GetMaxOverlapLevel(CCSPtrArray<PRINTBARDATA>& ropPrintLine,
							CCSPtrArray<PRINTBARDATA>& ropBkBars) const; //PRF 8704
	void ClearMapPageNoToPreviewDataList(); //PRF 8704
	void ExcelExportPrintHeader(Shapes& ropShapes, const int& ipPageNo,const int& ipGroupNo, BOOL bpIsColorEnabled);
	void ExcelExportPrintPoolHeader(Shapes& ropShapes, const float& fpNextStartOffset,const int& ipGroupNo, BOOL bpIsColorEnabled);

public:
	void PrintDiagram(CPtrArray &opPtrArray);
	void SetupPrintPageInfo(CCSPrint::PRINT_OPTION epPrintOption = CCSPrint::PRINT_PREVIEW); //PRF 8704
	int  GetPrintPageCount() const{return omMapPageNoToPreviewDataList.GetCount();}
	void PrintPageData(CCSPrint *pomPrint,const int& ripPageNo); //PRF 8704
	void ExportToExcel(BOOL bpIsPageBreakEnabled, BOOL bpIsColorEnabled); //Export To Excel

	bool IsDoubleTourTeacher(int ipGroupno, int ipLineno);
	bool LineIsBottomOfTeam(int ipGroupno, int ipLineno);
	bool LineIsTopOfTeam(int ipGroupno, int ipLineno);
	bool IsTeamLeader(int ipGroupno, int ipLineno);
	bool IsTeamCompressed(int ipGroupno, int ipLineno);

	bool bmTeamView; // display gruppendispo
	bool bmHideCompletelyDelegatedTeams; // Hide emps who are completely delegated to another team
	bool bmHideAbsentEmps, bmHideStandbyEmps, bmHideFidEmps;
	CCSPtrArray <TEAM_DATA> omTeams;
	CMapPtrToPtr omJobUrnoToTeamMap;		// which jobs and pool jobs are in which teams
	CMapStringToPtr omCompressedTeamMap;	// used in ChangeViewTo to remember which teams were compressed

	void DeleteAllTeams();
	void DeleteTeam(TEAM_DATA *prpTeam, bool bpDeleteBars = true , bool bpUpdateLine = true );
	void DeletePoolJobsForTeam(TEAM_DATA *prpTeam, bool bpDeleteBars = true , bool bpUpdateLine = true );
	TEAM_DATA *DeletePoolJobFromTeam(TEAM_DATA *prpTeam, long lpPoolJobUrno, bool bpUpdateLine = true);
	void DeleteCompressedPoolJobsForTeam(TEAM_DATA *prpTeam, bool bpDeleteBars = true , bool bpUpdateLine = true );
	void DeleteCompressedJobsForTeam(TEAM_DATA *prpTeam, bool bpDeleteBars = true , bool bpUpdateLine = true );
	void DeleteCompressedJob(COMPRESSED_JOBDATA *prpCompressedJob, bool bpDeleteBars = true, bool bpUpdateLine = true);

	TEAM_DATA *GetMatchingTeam(JOBDATA *prpPoolJob, CString opTeamName);
	TEAM_DATA *GetTeamByPoolJobUrno(long lpPoolJobUrno);
	TEAM_DATA *GetTeamByJobUrno(long lpJobUrno);
	TEAM_DATA *AddToTeam(JOBDATA *prpPoolJob, CString opTeamName);
	void AddPoolJobToTeam(TEAM_DATA *prpTeam, JOBDATA *prpPoolJob);
	void ResetTeamInfo(TEAM_DATA *prpTeam);
	bool SetTeamInfoForPoolJob(TEAM_DATA *prpTeam, JOBDATA *prpPoolJob);
	TEAM_DATA *AddTeam(JOBDATA *prpPoolJob, CString opTeamName);
	STAFF_LINEDATA *AddCompressedPoolJobToTeam(JOBDATA *prpPoolJob, bool bpAddOnlyWhenNotFound=true);
	bool IsTeamCompressed(long lpJobUrno); 
	bool IsTeamLeader(long lpPoolJobUrno);
	bool AreInSameTeam(long lpPoolJobUrno1, long lpPoolJobUrno2);
	void GetTeamSortTimes(long lpPoolJobUrno, CTime &ropStartTime, CTime &ropEndTime);
	void ExpandOrCompressTeam(int ipGroup, int ipLine);
	void RememberCompressedTeams();
	bool TeamWasCompressed(JOBDATA *prpPoolJob);
	void MakeCompressedJobsForTeam(TEAM_DATA *prpTeam);
	STAFF_LINEDATA *MakeCompressedPoolJobsForTeam(TEAM_DATA *prpTeam, bool bpUpdate = true, long lpOldTeamUrno = 0L);
	void MakeCompressedBarData(STAFF_BARDATA *prlBar, COMPRESSED_JOBDATA *prpJob);
	void MakeCompressedBar(int ipGroupno, int ipLineno, COMPRESSED_JOBDATA *prpJob);
	void AddCompressedJob(TEAM_DATA *prpTeam, JOBDATA *prpJob);
	void AddCompressedPoolJob(TEAM_DATA *prpTeam, JOBDATA *prpPoolJob);
	void AddCompressedTempAbsence(TEAM_DATA *prpTeam, JOBDATA *prpTempAbsence);
	void UpdateCompressedJob(COMPRESSED_JOBDATA *prpCompressedJob, JOBDATA *prpJob);
	COMPRESSED_JOBDATA *FindCompressedJobMatch(TEAM_DATA *prpTeam, JOBDATA *prpJob);
	COMPRESSED_JOBDATA *FindCompressedJob(TEAM_DATA *prpTeam, long lpJobUrno);
	COMPRESSED_JOBDATA *FindCompressedPoolJob(TEAM_DATA *prpTeam, long lpPoolJobUrno);
	BOOL FindCompressedBarByJobUrno(long lpJobUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	JOBDATA *GetCompressedJobByNormalJobUrno(long lpJobUrno);
	JOBDATA *GetCompressedPoolJobByNormalPoolJobUrno(long lpPoolJobUrno);
	long HasPlusMinusButton(TEAM_DATA *prpTeam);
	bool PoolJobIsInCompressedTeam(TEAM_DATA *prpTeam, long lpPoolJobUrno);
	int FindPoolJobLine(long lpPoolJobUrno, int ipGroup);
	void CompressTeams(bool bpCompress);
	void GetPoolJobsInTeam(long lpPoolJobUrno, CCSPtrArray <JOBDATA> &ropTeamPoolJobs, bool blReturnFirstPoolJobOnlyForEachEmp = false);
	CString DumpTeamData(long lpPoolJobUrno);
	bool HandleUpdateCompressedJob(JOBDATA *prpJob);
	bool GetJobsForCompressedJob(long lpJobUrno, CCSPtrArray <JOBDATA> &ropJobs);
	bool GetPoolJobsForCompressedPoolJob(long lpPoolJobUrno, CCSPtrArray <JOBDATA> &ropPoolJobs);
	CString GetPool(int ipGroupno) const; //Singapore
	CCSPrint*& GetCCSPrinter() {return pomPrint;} //PRF 8704

	COLORREF GetTextColour(int ipGroup, int ipLine);
	bool bmTeamLeaderColourDefined;
	COLORREF imTeamLeaderColour;
};

/////////////////////////////////////////////////////////////////////////////

#endif
