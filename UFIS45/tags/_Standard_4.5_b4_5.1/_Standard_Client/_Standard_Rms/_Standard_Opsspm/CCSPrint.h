

#ifndef _CCSPRINT_H_
#define _CCSPRINT_H_
#include <afxpriv.h>

enum egPrintInfo {PRINT_LANDSCAPE,PRINT_PORTRAIT,PRINT_LEFT,PRINT_RIGHT,PRINT_CENTER,
					PRINT_NOFRAME,PRINT_FRAMETHIN,PRINT_FRAMEMEDIUM,PRINT_FRAMETHICK,
					PRINT_SMALL,PRINT_MEDIUM,PRINT_LARGE,
					PRINT_SMALLBOLD,PRINT_MEDIUMBOLD,PRINT_LARGEBOLD};



struct PRINTELEDATA
{
	int Alignment;
	int Length;
	int FrameLeft;
	int FrameRight;
	int FrameTop;
	int FrameBottom;
	CFont *pFont;
	CString Text;
};


struct PRINTBARDATA 
{
    CString Text;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
	BOOL IsShadowBar;
	BOOL IsBackGroundBar;
    int OverlapLevel;    // zero for the top-most level, each level lowered by 4 pixels
	UINT TextAlign;
	
	PRINTBARDATA ()
	{
		TextAlign = TA_CENTER;
	}
};

extern	 CCSPtrArray <PRINTBARDATA> omDefBkBars;

class CCSPrint : public CObject
{
public:
	CCSPrint(CWnd *opParent = NULL);
	CCSPrint(CWnd *opParent,int ipOrientation,
		int ipLineHeight,int ipFirstLine,int ipLeftOffset,
		CString opHeader1 = "",CString opHeader2 = "",CString opHeader3 = "",
		CString opHeader4 = "",
		CString opFooter1 = "",CString opFooter2 = "",CString opFooter3 = "");
	~CCSPrint();

	enum PRINT_OPTION{PRINT_PRINTER,PRINT_PREVIEW};
	// Implementation
	InitializePrinter(int ipOrientation = PRINT_PORTRAIT,
		char *pcpFormName= "A3",int ipPaperSize = DMPAPER_A3);

	BOOL PrintHeader(void);
	BOOL PrintHeader(CString opHeader1,CString opHeader2 = "",CString opHeader3 = "", CString opHeader4 = "");
	void ReadBitmapDimensions(int &ripBitmapWidth, int &ripBitmapHeight);
	BOOL PrintFooter();
	BOOL PrintFooter(CString opFooter1,CString opFooter2 = "");
	BOOL PrintLine(CCSPtrArray <PRINTELEDATA> &rlPrintLine);

//	 CCSPtrArray <PRINTBARDATA> omDefBkBars;
	BOOL PrintGanttLine(CString opText,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
	CCSPtrArray<PRINTBARDATA> &ropDefBkBars = omDefBkBars);
	BOOL PrintGanttBar(PRINTBARDATA &ropBar);
	BOOL PrintBkBar(PRINTBARDATA &ropBa,int ipLineHeight);
	BOOL PrintTimeScale(int ipStartX,int ipEndX,CTime opStartTime,CTime opEndTime,CString opText);
	BOOL PrintGanttHeader(int ipStartX,int ipEndX,CString opText);
	BOOL PrintGanttBottom(void);
	BOOL PrintText(int ipStartX,int ipStartY,int ipEndX,int ipEndY,int ipType,CString opText,BOOL ipUseOffset = FALSE);
	
	BOOL IsPageSelected() const{return bmIsPageSelected;} //Singpaore
	int GetFromPage() const{return imFromPage;} //Singapore
	int GetToPage() const{return imToPage;} //Singapore
	BOOL InitializePrintSetup(PRINT_OPTION epPrintOption = PRINT_PRINTER); //Singapore
	int GetTotalPageCount() const{return imTotalPageCount;} //Singapore
	void EnablePageSelection(BOOL bpEnablePageSelection = TRUE){bmEnablePageSelection = bpEnablePageSelection;} //Singapore

private:
	BOOL SelectFramePen(int ipFrameType);
	void PrintLeft(CRect opRect,CString opText);
	void PrintRight(CRect opRect,CString opText);
	void PrintCenter(CRect opRect,CString opText);
	int  GetXFromTime(CTime opTime);
	BOOL LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette );
	void InitializeDevicePointer();

	// Attributes
public:
	CDC	 omCdc;
	CPreviewDC  omPreviewDc;
	CDC* pomCdc;
	int imLineNo;
	int imPageNo;
	int imMaxLines;
	

	int imLineHeight;
	int imGanttLineHeight;
	int imBarHeight;
	int imBarVerticalTextOffset;
	int imFirstLine;
	int imLineTop;
	int imLeftOffset;
	int imYOffset;
	int imWidth;
	int imHeight;
	int imHourPixels;
	int imHours;

	CFont omSmallFont_Regular;
	CFont omMediumFont_Regular;
	CFont omLargeFont_Regular;
	CFont omSmallFont_UnderlineBold;
	CFont omSmallFont_Bold;
	CFont omMediumFont_Bold;
	CFont omLargeFont_Bold;
	CFont omCCSFont;
	CFont omCourierNormal10;
	CFont omCourierNormal7;


    CPen ThinPen;
    CPen MediumPen;
    CPen ThickPen;
	CPen DottedPen;

	short smPaperSize;
	int imOrientation;
	
	PRINT_OPTION emPrintOption;
private:
	CRgn omRgn;
	CStringArray omHeader;
	CStringArray omFooter;
	CString	     omLogoPath;
	CWnd         *pomParent;
	CBitmap		 omBitmap;
	CDC			omMemDc;
	CBitmap		 omCcsBitmap;
	CDC			omCcsMemDc;
	int imLogPixelsY;
	int imLogPixelsX;
	BOOL bmIsInitialized;
	double dmXFactor;

	int imGanttStartX;
	int imGanttEndX;
	int imGanttStartY;
	int imGanttEndY;

	CTime omStartTime;
	CTime omEndTime;
	HBITMAP		hmBitmap;
	HPALETTE	hmPalette;
	CBrush		omHatchedBrush;

	int imFromPage;//Singapore
	int imToPage; //Singapore
	BOOL bmIsPageSelected;//Singapore
	int imTotalPageCount; //Singapore
	BOOL bmEnablePageSelection; //Singapore
};






#endif


