// cxbutton.cpp : implementation file
//

#include <stdafx.h>
#include <cxbutton.h>


C2StateButton::C2StateButton()
{
    bmIsRecessed = FALSE;
}

void C2StateButton::Recess(BOOL bpIsRecessed)
{
    bmIsRecessed = bpIsRecessed;
    SetState(bmIsRecessed);
}

BOOL C2StateButton::Recess()
{
    return bmIsRecessed;
}


BEGIN_MESSAGE_MAP(C2StateButton, CButton)
    //{{AFX_MSG_MAP(C2StateButton)
    ON_WM_GETDLGCODE()
    ON_WM_LBUTTONUP()
    ON_WM_KILLFOCUS()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// C2StateButton message handlers

UINT C2StateButton::OnGetDlgCode()
{
    // Disallow the button for being a default button.
    // Since Windows has some bugs when tab processing on a a focused button
    // has a discrepancy. (It's usually draw that push button as a default
    // button, and does not clear that button to be normal again.)
    //
    return CButton::OnGetDlgCode() & ~(DLGC_DEFPUSHBUTTON | DLGC_UNDEFPUSHBUTTON);
}

void C2StateButton::OnLButtonUp(UINT nFlags, CPoint point)
{
    CButton::OnLButtonUp(nFlags, point);
    CButton::SetState(bmIsRecessed);    // redraw button
}                                          

void C2StateButton::OnKillFocus(CWnd* pNewWnd)
{
    CButton::SetState(FALSE);   // allows OnKillFocus() to work correctly
    CButton::OnKillFocus(pNewWnd);
    CButton::SetState(bmIsRecessed);    // redraw button
}


