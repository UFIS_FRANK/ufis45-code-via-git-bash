#if !defined(AFX_AUTOASSIGNAZ_H__B6B8CBC3_945A_11D4_B865_00C04F3493A5__INCLUDED_)
#define AFX_AUTOASSIGNAZ_H__B6B8CBC3_945A_11D4_B865_00C04F3493A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AutoAssignAz.h : header file
//
#include <ccsglobl.h>
//#include "cedademanddata.h"
//#include "cedajobdata.h"
//#include "cedajoddata.h"
#include <cedaflightdata.h>
#include <AutoAssignAzViewer.h>
/////////////////////////////////////////////////////////////////////////////
// CAutoAssignAz dialog

class CAutoAssignAz : public CDialog
{
// Construction
public:
	CAutoAssignAz(CTime opLoadStart, CTime opLoadEnd, CString opChartType, CWnd* pParent = NULL);

	void SetData(CStringArray &ropResourceGroupArray, CStringArray &ropRegnGroupArray, CTime opAssignFrom, CTime opAssignTo);
	BOOL GetData();
	BOOL GetData(CStringArray &ropValues);
	void SetData();
	void SetData(CStringArray &ropValues);
	CStringArray omValues;
	AutoAssignAzViewer omViewer;

// Dialog Data
	//{{AFX_DATA(CAutoAssignAz)
	enum { IDD = IDD_AUTOASSIGN_AZ };
	CListBox	m_SelectedListStra;
	CListBox	m_SelectedListRegn;
	CListBox	m_SelectedListEmp;
	CListBox	m_PossibleListStra;
	CListBox	m_PossibleListRegn;
	CListBox	m_PossibleListEmp;
	BOOL	m_AssignWOAloc;
	BOOL	m_BreakPrio;
	CString	m_ToTime;
	CString	m_ToDay;
	CString	m_FromTime;
	CString	m_FromDay;
	CString	m_ShiftEndBuff;
	CString	m_JobBuff;
	CString	m_FlightAircraft;
	CString	m_FlightAirline;
	CString	m_FlightDest;
	CString	m_FlightNumber;
	CString	m_FlightOrig;
	CString	m_FlightRegn;
	CString	m_FlightSuffix;
	CEdit m_RelToCtrl;
	CEdit m_RelFromCtrl;
	CEdit m_ToTimeCtrl;
	CEdit m_ToDayCtrl;
	CEdit m_FromTimeCtrl;
	CEdit m_FromDayCtrl;
	int m_AbsTimeVal;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutoAssignAz)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAutoAssignAz)
	afx_msg void OnAbstime();
	afx_msg void OnReltime();
	afx_msg void OnLeaveman();
	afx_msg void OnOpenonly();
	afx_msg void OnDelonly();
	afx_msg void OnDeleteStra();
	afx_msg void OnDeleteRegn();
	afx_msg void OnDeleteEmp();
	afx_msg void OnAddEmp();
	afx_msg void OnAddRegn();
	afx_msg void OnAddStra();
	afx_msg void OnAll();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	CString omChartType;
	CString omLeaveMan;
	CString omOpenOnly;
	CString omDelOnly;
	CTime omLoadEnd;
	CTime omLoadStart;
	CTime omAssignStart;
	CTime omAssignEnd;


	bool bmSetWarning;
	CStringArray omPossibleItemsEmp;
	CStringArray omSelectedItemsEmp;
	CStringArray omPossibleItemsRegn;
	CStringArray omSelectedItemsRegn;
	CStringArray omPossibleItemsStra;
	CStringArray omSelectedItemsStra;
	//CUIntArray omDemUrnoList;

	bool IsFlightPassFilter(FLIGHTDATA * prpFlight);
	void DeleteJobsLeaveManual();
	void DeleteAllJobs();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTOASSIGNAZ_H__B6B8CBC3_945A_11D4_B865_00C04F3493A5__INCLUDED_)
