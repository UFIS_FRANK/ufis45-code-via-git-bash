// CedaLoaData.cpp - Loading Table - Contains suplimentary data about flights
// eg num passengers, baggage handling info
#include <afxwin.h>
#include "ccsglobl.h"
#include "CCSPtrArray.h"
#include "CCSCedaData.h"
#include "ccsddx.h"
#include "CCSBcHandle.h"
#include "ccsddx.h"
#include "CedaLoaData.h"
#include "BasicData.h"
#include "Resource.h"
#include "CedaFlightData.h"

extern CCSDdx ogCCSDdx;
void  ProcessLoaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaLoaData::CedaLoaData()
{                  
    BEGIN_CEDARECINFO(LOADATA, LoaDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Type,"TYPE")
		FIELD_CHAR_TRIM(Styp,"STYP")
		FIELD_CHAR_TRIM(Sstp,"SSTP")
		FIELD_LONG(Flnu,"FLNU")
		FIELD_CHAR_TRIM(Valu,"VALU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(LoaDataRecInfo)/sizeof(LoaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&LoaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"LOATAB");
    pcmFieldList = "URNO,TYPE,STYP,SSTP,FLNU,VALU";

	ogCCSDdx.Register((void *)this,BC_LOA_CHANGE,CString("LOADATA"), CString("Loa changed"),ProcessLoaCf);
	ogCCSDdx.Register((void *)this,BC_LOA_DELETE,CString("LOADATA"), CString("Loa deleted"),ProcessLoaCf);
}
 
CedaLoaData::~CedaLoaData()
{
	TRACE("CedaLoaData::~CedaLoaData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void  ProcessLoaCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaLoaData *)popInstance)->ProcessLoaBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaLoaData::ProcessLoaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlLoaData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlLoaData->Selection);
	LOADATA *prlLoa = GetLoaByUrno(llUrno);
	ogBasicData.Trace("LOATAB BC Cmd <%s>\nFields <%s>\nData <%s>\nSelection <%s>\n",prlLoaData->Cmd,prlLoaData->Fields,prlLoaData->Data,prlLoaData->Selection);

	if(ipDDXType == BC_LOA_CHANGE && prlLoa != NULL)
	{
		// update
		LOADATA rlLoa;
		ogBasicData.Trace("BC_LOA_CHANGE - UPDATE LOATAB URNO %ld\n",prlLoa->Urno);
		GetRecordFromItemList(&rlLoa,prlLoaData->Fields,prlLoaData->Data);
		if(strcmp(rlLoa.Valu,prlLoa->Valu))
		{
			strcpy(prlLoa->Valu,rlLoa.Valu);
			SendFlightChange(prlLoa->Flnu);
		}
	}
	else if(ipDDXType == BC_LOA_CHANGE && prlLoa == NULL)
	{
		// insert
		LOADATA rlLoa;
		GetRecordFromItemList(&rlLoa,prlLoaData->Fields,prlLoaData->Data);
		ogBasicData.Trace("BC_LOA_CHANGE - INSERT LOATAB URNO %ld\n",rlLoa.Urno);
		AddLoaInternal(rlLoa);
		SendFlightChange(prlLoa->Flnu);
	}
	else if(ipDDXType == BC_LOA_DELETE && prlLoa != NULL)
	{
		// delete
		ogBasicData.Trace("BC_LOA_DELETE - DELETE LOATAB URNO %ld\n",prlLoa->Urno);
		DeleteLoaInternal(prlLoa->Urno);
		SendFlightChange(prlLoa->Flnu);
	}
}

void CedaLoaData::SendFlightChange(long lpUaft)
{
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpUaft);
	if(prlFlight != NULL)
	{
		ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
	}
}

void CedaLoaData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();

	CMapStringToPtr *polSubMap;
	long llFlnu;
	for(POSITION rlPos1 = omFlnuMap.GetStartPosition(); rlPos1 != NULL; )
	{
		omFlnuMap.GetNextAssoc(rlPos1,(void *&)llFlnu,(void *& )polSubMap);
		polSubMap->RemoveAll();
		delete polSubMap;
	}
	omFlnuMap.RemoveAll();
}

bool CedaLoaData::ReadLoaData(CDWordArray &ropFlightUrnos)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT", pclWhere[6000] = "";
	CString olUrno;
	bool blFinished = false;
	int ilFlightCount = 0, ilNumFlights = ropFlightUrnos.GetSize(), ilNumFlightsAdded = 0;

	while(!blFinished)
	{
		ilNumFlightsAdded = 0;
		if(ilNumFlights <= 0)
		{
			sprintf(pclWhere,"WHERE TYPE IN ('PAX','PMX')");
			blFinished = true;
		}
		else
		{
			while(ilNumFlightsAdded < 500 && ilFlightCount < ilNumFlights)
			{
				if(ilNumFlightsAdded == 0)
					strcpy(pclWhere,"WHERE FLNU IN (");
				else
					strcat(pclWhere,",");
				olUrno.Format("'%ld'", ropFlightUrnos[ilFlightCount]);
				strcat(pclWhere, olUrno);
				ilNumFlightsAdded++;
				ilFlightCount++;
			}

			//strcat(pclWhere, ") AND DSSN IN ('USR','KRI')");
			strcat(pclWhere, ") AND DSSN = 'USR'");

			if(ilFlightCount >= ilNumFlights)
				blFinished = true;
		}
		if ((ilRc = CedaAction(pclCom, pclWhere)) != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaLoaData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
		}
		else
		{
			LOADATA rlLoaData;
			for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
			{
				if ((ilRc = GetBufferRecord(ilLc,&rlLoaData)) == RCSuccess)
				{
					AddLoaInternal(rlLoaData);
				}
			}
			ilRc = RCSuccess;
		}
	}
	ogBasicData.Trace("CedaLoaData Read %d Records %s\n",omData.GetSize(),pclWhere);
    return ilRc;
}


void CedaLoaData::AddLoaInternal(LOADATA &rrpLoa)
{
	LOADATA *prlLoa = new LOADATA;
	*prlLoa = rrpLoa;
	omData.Add(prlLoa);
	omUrnoMap.SetAt((void *)prlLoa->Urno,prlLoa);
	AddLoaToFlnuMap(prlLoa);
}


void CedaLoaData::DeleteLoaInternal(long lpUrno)
{
	int ilNumLoas = omData.GetSize();
	for(int ilLoa = (ilNumLoas-1); ilLoa >= 0; ilLoa--)
	{
		if(omData[ilLoa].Urno == lpUrno)
		{
			DeleteLoaFromFlnuMap(&omData[ilLoa]);
			omData.DeleteAt(ilLoa);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

LOADATA* CedaLoaData::GetLoaByUrno(long lpUrno)
{
	LOADATA *prlLoa = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlLoa);
	return prlLoa;
}

CMapStringToPtr *CedaLoaData::GetSubMap(long lpFlnu)
{
	CMapStringToPtr *polSubMap = NULL;
	omFlnuMap.Lookup((void *)lpFlnu, (void *&) polSubMap);
	return polSubMap;
}

void CedaLoaData::AddLoaToFlnuMap(LOADATA *prpLoa)
{
	CString olKey;

	CMapStringToPtr *polSubMap = GetSubMap(prpLoa->Flnu);
	if(polSubMap == NULL)
	{
		polSubMap = new CMapStringToPtr;
		polSubMap->SetAt(olKey,prpLoa);
		omFlnuMap.SetAt((void *)prpLoa->Flnu,polSubMap);
	}
	olKey = MakeKey(prpLoa->Type,prpLoa->Styp,prpLoa->Sstp);
	polSubMap->SetAt(olKey,prpLoa);
}

void CedaLoaData::DeleteLoaFromFlnuMap(LOADATA *prpLoa)
{
	CMapStringToPtr *polSubMap = GetSubMap(prpLoa->Flnu);
	if(polSubMap != NULL)
	{
		CString olKey = MakeKey(prpLoa->Type,prpLoa->Styp,prpLoa->Sstp);
		polSubMap->RemoveKey(olKey);
	}
}

int CedaLoaData::GetValu(long lpFlnu, const char *pcpType, const char *pcpStyp, const char *pcpSstp /* = LOA_SSTP_NOTDEFINED */)
{
	return GetValu(GetSubMap(lpFlnu),pcpType,pcpStyp,pcpSstp);
}

int CedaLoaData::GetValu(CMapStringToPtr *popSubMap, const char *pcpType, const char *pcpStyp, const char *pcpSstp /* = LOA_SSTP_NOTDEFINED */)
{
	int ilValu = 0;
	if(popSubMap != NULL)
	{
		LOADATA *prlLoa;
		CString olKey = MakeKey(pcpType,pcpStyp,pcpSstp);
		if(popSubMap->Lookup(olKey,(void *&) prlLoa))
		{
			ilValu = atoi(prlLoa->Valu);
		}
	}
	return ilValu;
}

CString CedaLoaData::MakeKey(const char *pcpType, const char *pcpStyp, const char *pcpSstp /* = LOA_SSTP_NOTDEFINED */)
{
	CString olKey;
	olKey.Format("<%s><%s><%s>",pcpType,pcpSstp,pcpStyp);
	return olKey;
}


CString CedaLoaData::GetTableName(void)
{
	return CString(pcmTableName);
}

// Uaft = flight URNO, pcpClass = first/business/economy
int CedaLoaData::GetBookedPassengers(long lpUaft, const char *pcpClass)
{
	return GetValu(lpUaft, LOA_PASSENGERS, pcpClass, LOA_BOOKED_LOCAL) + GetValu(lpUaft, LOA_PASSENGERS, pcpClass, LOA_BOOKED_TRANSFER);
}

int CedaLoaData::GetCheckedInPassengers(long lpUaft, const char *pcpClass)
{
	CMapStringToPtr *polSubMap = GetSubMap(lpUaft);
	return GetValu(polSubMap, LOA_PASSENGERS, pcpClass, LOA_CHECKEDIN_LOCAL) + GetValu(polSubMap, LOA_PASSENGERS, pcpClass, LOA_CHECKEDIN_TRANSFER);
}

int CedaLoaData::GetNumSeats(long lpUaft, const char *pcpClass)
{
	return GetValu(lpUaft, LOA_SEATS, pcpClass);
}

bool CedaLoaData::IsOverBooked(long lpUaft, const char *pcpClass)
{
	CMapStringToPtr *polSubMap = GetSubMap(lpUaft);
	return (GetValu(polSubMap, LOA_PASSENGERS, pcpClass, LOA_BOOKED_LOCAL) + GetValu(polSubMap, LOA_PASSENGERS, pcpClass, LOA_BOOKED_TRANSFER)) > GetValu(polSubMap, LOA_SEATS, pcpClass);
}

// return a string format 1stBooked (1stCheckedIn) BusinessB (BusinessC) EconB (EconC)
CString CedaLoaData::GetPaxString(long lpUaft)
{
	CMapStringToPtr *polSubMap = GetSubMap(lpUaft);
	CString olPaxString;
	olPaxString.Format("%d(%d)  %d(%d)  %d(%d)", 
		GetValu(polSubMap, LOA_PASSENGERS, LOA_FIRST, LOA_BOOKED_LOCAL) + GetValu(polSubMap, LOA_PASSENGERS, LOA_FIRST, LOA_BOOKED_TRANSFER),
		GetValu(polSubMap, LOA_SEATS, LOA_FIRST),
		GetValu(polSubMap, LOA_PASSENGERS, LOA_BUSINESS, LOA_BOOKED_LOCAL) + GetValu(polSubMap, LOA_PASSENGERS, LOA_BUSINESS, LOA_BOOKED_TRANSFER),
		GetValu(polSubMap, LOA_SEATS, LOA_BUSINESS),
		GetValu(polSubMap, LOA_PASSENGERS, LOA_ECONOMY, LOA_BOOKED_LOCAL) + GetValu(polSubMap, LOA_PASSENGERS, LOA_ECONOMY, LOA_BOOKED_TRANSFER),
		GetValu(polSubMap, LOA_SEATS, LOA_ECONOMY));

	return olPaxString;
}
