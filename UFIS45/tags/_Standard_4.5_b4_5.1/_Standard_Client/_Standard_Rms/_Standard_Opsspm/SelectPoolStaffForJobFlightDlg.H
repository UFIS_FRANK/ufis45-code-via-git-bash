// SelectPoolStaffForJobFlightDlg.h : header file
//
#ifndef _CSELECTPOOLSTAFFFORJOBFLIGHTDIALOG_H_
#define _CSELECTPOOLSTAFFFORJOBFLIGHTDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// CSelectPoolStaffForJobFlightDialog dialog

class CSelectPoolStaffForJobFlightDialog : public CSelectPoolStaffDialog
{
// Construction
public:
	CSelectPoolStaffForJobFlightDialog(CWnd* pParent,
		StaffDiagramViewer *popViewer, int ipGroupno, long lpFlightUrno);

// Dialog Data
	long lmFlightUrno;
	CString omAloc, omAlid;

	//{{AFX_DATA(CSelectPoolStaffForJobFlightDialog)
	enum { IDD = IDD_SELECTPOOLSTAFF_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectPoolStaffForJobFlightDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelectPoolStaffForJobFlightDialog)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // _CSELECTPOOLSTAFFFORJOBFLIGHTDIALOG_H_