#ifndef _CJOBD_H_
#define _CJOBD_H_

#include <CCSCedaData.h>
#include <CedaDemandData.h>
#define JOB_MAXTEXTLEN 128
// CJD_SELECTIONLEN should be not more than 20.000
#define CJD_SELECTIONLEN 10000

struct JobDataStruct {

    long    Urno;           // Unique Record Number in JOBCKI
    char    Peno[20];       // PK-Number
    CTime   Plfr;           // Planned Begin of Job
    CTime   Plto;           // Planned End of Job
    CTime   Acfr;           // Actual Begin of Job
    CTime   Acto;           // Actual End of Job
	long	Jour;			// URNO of master job
    char    Text[JOB_MAXTEXTLEN+2];       // Text
    long    Flur;           // Flight Unique Record Number
	long	Shur;			// Shift Unique Record Number
    char    Stat[2];        // Status
    char    Conf[2];        // Conflict Status 'P'lanned,'C'onfirmed or 'F'inished
    char    Chng[2];        // Manually changed flag '0' New, '1' Manually Changed
	char	Ignr[2];		// '1' = Ignore this job in calculations/allocations else '0'
	bool	Infm;			// Informed flag: true -> job is planned, the emp has been informed of the job but has not yet started
	bool	Ackn;			//  Acknowledge of Informed flag: true -> job is planned, the emp has been informed of the job but has not yet started
    char    Jtco[12];       // Job Type code
	char	Dety[2];		// demand type

	// following come from flight data and are used to check for conflicts
    char    Regn[13];		// Registration
    char    Act3[4];		// Aircraft Type
	char	Posi[6];		// Position
	char	Gate[6];		// Gate

	// additional data used to read DB model for Ufis43 - then translated to existing fields
	char	DbStat[11];		// Stat=DbStat[0]  Chng=DbStat[1]
	long	Ustf;			// FKEY of Emp in STFTAB -> used to get Peno
	long	Uaid;			// used in conjuction with Ualo to get Alid from one...
	long	Ualo;			// ... of the allocation unit tables via ALOTAB
	long	Ujty;			// FKEY of Job Type in JTYTAB
	long	Udel;			// URNO of basis shift (eg F1) delegation (for pool jobs only)
	long	Udrd;			// URNO of shift deviation (for pool jobs only)

    char    Usec[33];		// Creator
    CTime   Cdat;			// Creation Date
    char    Useu[33];		// Updater
    CTime   Lstu;			// Update Date

	long	Uequ;			// Equipment URNO (EQUTAB.URNO)
	long	Utpl;			// Template URNO (TPLTAB.URNO)
	char	Fcco[11];		// Function Code: PJ = emp main func, Regular Job = dem func/if dem has group of funcs or job w/o dem then use emp main func
	long	Ughs;			// SERTAB.URNO URNO of service for jobs with demands
	long	Uprm;			// DPXTAB Urno for PRM Jobs
	char    Filt[6];          // Filter for Handling Agents
	char	Wgpc[6];		// Workgroup Code - the WG an emp is currently working in, stored in PJs only
	long	Udem;			// DEMTAB.URNO the demand URNO
	char	Aloc[11];		// Type of allocation unit
    char    Alid[15];       // ID of Allocation Unit

	// additional local data
	int		ConflictType;	// Conflict Status
	int		ColorIndex;		// Index in global Color Table
	int		ConflictWeight;
	BOOL	ConflictConfirmed;
	BOOL    DisplayInPrePlanTable;
	char	TmpText[32];	// description of function that updated/created this record -> written to USEU or USEC
	BOOL	Printed;		// Job has already been printed

	JobDataStruct(void) 
	{
		memset(this,'\0',sizeof(*this));
		Urno = 0L;
		strcpy(Peno,"");
		Plfr = TIMENULL;
		Plto = TIMENULL;
		Acfr = TIMENULL;
		Acto = TIMENULL;
		Jour = 0L;
		strcpy(Text,"");
		strcpy(Alid,"");
		strcpy(Aloc,"");
		Flur = 0L;
		Shur = 0L;
		strcpy(Stat,"");
		strcpy(Conf,"");
		strcpy(Chng,"");
		strcpy(Ignr,"");
		Infm = false;
		Ackn = false;
		strcpy(Jtco,"");
		strcpy(Dety,"");
		strcpy(Regn,"");
		strcpy(Act3,"");
		strcpy(Posi,"");
		strcpy(Gate,"");
		strcpy(DbStat,"");
		Ustf = 0L;
		Uaid = 0L;
		Ualo = 0L;
		Ujty = 0L;
		Udel = 0L;
		Udrd = 0L;
		strcpy(Usec,"");
		Cdat = TIMENULL;
		strcpy(Useu,"");
		Lstu = TIMENULL;
		Uequ = 0L;
		Utpl = 0L;
		ConflictType = 0;
		ColorIndex = 0;
		ConflictWeight = 0;
		ConflictConfirmed = FALSE;
		DisplayInPrePlanTable = FALSE;
		strcpy(TmpText,"");
		Printed = FALSE;
		strcpy(Fcco,"");
		Ughs = 0L;
		Uprm = 0L;
		strcpy(Wgpc,"");
		Udem = 0L;
	}


	JobDataStruct& JobDataStruct::operator=(const JobDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Peno,rhs.Peno);
			Plfr = rhs.Plfr;
			Plto = rhs.Plto;
			Acfr = rhs.Acfr;
			Acto = rhs.Acto;
			Jour = rhs.Jour;
			strcpy(Text,rhs.Text);
			strcpy(Alid,rhs.Alid);
			strcpy(Aloc,rhs.Aloc);
			Flur = rhs.Flur;
			Shur = rhs.Shur;
			strcpy(Stat,rhs.Stat);
			strcpy(Conf,rhs.Conf);
			strcpy(Chng,rhs.Chng);
			strcpy(Ignr,rhs.Ignr);
			Infm = rhs.Infm;
			Ackn = rhs.Ackn;
			strcpy(Jtco,rhs.Jtco);
			strcpy(Dety,rhs.Dety);
			strcpy(Regn,rhs.Regn);
			strcpy(Act3,rhs.Act3);
			strcpy(Posi,rhs.Posi);
			strcpy(Gate,rhs.Gate);
			strcpy(DbStat,rhs.DbStat);
			Ustf = rhs.Ustf;
			Uaid = rhs.Uaid;
			Ualo = rhs.Ualo;
			Ujty = rhs.Ujty;
			Udel = rhs.Udel;
			Udrd = rhs.Udrd;
			strcpy(Usec,rhs.Usec);
			Cdat = rhs.Cdat;
			strcpy(Useu,rhs.Useu);
			Lstu = rhs.Lstu;
			Uequ = rhs.Uequ;
			Utpl = rhs.Utpl;
			ConflictType = rhs.ConflictType;
			ColorIndex = rhs.ColorIndex;
			ConflictWeight = rhs.ConflictWeight;
			ConflictConfirmed = rhs.ConflictConfirmed;
			DisplayInPrePlanTable = rhs.DisplayInPrePlanTable;
			strcpy(TmpText,rhs.TmpText);
			Printed = rhs.Printed;
			strcpy(Fcco,rhs.Fcco);
			Ughs = rhs.Ughs;
			Uprm = rhs.Uprm;
			strcpy(Wgpc,rhs.Wgpc);
			Udem = rhs.Udem;
		}		
		return *this;
	}

	friend bool operator==(const JobDataStruct& rrp1,const JobDataStruct& rrp2)
	{
		return
			rrp1.Plfr == rrp2.Plfr &&
			rrp1.Plto == rrp2.Plto &&
			rrp1.Acfr == rrp2.Acfr &&
			rrp1.Acto == rrp2.Acto &&
			rrp1.Jour == rrp2.Jour &&
			!strcmp(rrp1.Text,rrp2.Text) &&
			!strcmp(rrp1.Alid,rrp2.Alid) &&
			!strcmp(rrp1.Aloc,rrp2.Aloc) &&
			rrp1.Flur == rrp2.Flur &&
			rrp1.Shur == rrp2.Shur &&
			!strcmp(rrp1.Stat,rrp2.Stat) &&
			!strcmp(rrp1.Conf,rrp2.Conf) &&
			!strcmp(rrp1.Chng,rrp2.Chng) &&
			!strcmp(rrp1.Ignr,rrp2.Ignr) &&
			rrp1.Infm == rrp2.Infm &&
			rrp1.Ackn == rrp2.Ackn &&
			!strcmp(rrp1.Jtco,rrp2.Jtco) &&
			!strcmp(rrp1.Dety,rrp2.Dety) &&
			!strcmp(rrp1.Regn,rrp2.Regn) &&
			!strcmp(rrp1.Act3,rrp2.Act3) &&
			!strcmp(rrp1.Posi,rrp2.Posi) &&
			!strcmp(rrp1.Gate,rrp2.Gate) &&
			!strcmp(rrp1.DbStat,rrp2.DbStat) &&
			rrp1.Ustf == rrp2.Ustf &&
			rrp1.Uaid == rrp2.Uaid &&
			rrp1.Ualo == rrp2.Ualo &&
			rrp1.Ujty == rrp2.Ujty &&
			rrp1.Udel == rrp2.Udel &&
			rrp1.Udrd == rrp2.Udrd &&
			rrp1.Uequ == rrp2.Uequ &&
			rrp1.Utpl == rrp2.Utpl &&
			rrp1.Printed == rrp2.Printed &&
			!strcmp(rrp1.Fcco,rrp2.Fcco) &&
			rrp1.Ughs == rrp2.Ughs &&
			rrp1.Uprm == rrp2.Uprm &&
			!strcmp(rrp1.Wgpc,rrp2.Wgpc) &&
			rrp1.Udem == rrp2.Udem;
	}

	friend bool operator!=(const JobDataStruct& rrp1,const JobDataStruct& r2)
	{
		return !operator==(rrp1,r2);
	}

};

typedef struct JobDataStruct JOBDATA;
typedef struct JobDataStruct *JOBDATAPTR;
extern enum enumRecordState;

#define JOBFLIGHT				"FLT"	// Connect to Flightdata, DemandData, EmpData (in StaffGanttChart), StaffData
#define JOBFM					"FMJ"	// Will be used in the GateDetailWindow - flight mananger job (for a flight)
#define JOBGATE					"GAT"	// Connect to Gate data (in GateGanttChart), Flightdata, DemandData, (gate job without flight) EmpData (in StaffGanttChart), StaffData
#define JOBPST					"PST"	// 
#define JOBGATEAREA				"GTA"	// Connect to GateArea data (in GateGanttChart), Flightdata, DemandData, (gate area job without flight - NOT USED ???)  EmpData (in StaffGanttChart), StaffData
#define JOBFMGATEAREA			"FMG"	// Flightmanager assigned to gate area (JOBPOOL exist also) (flight manager assigned to gate area without flight)
#define JOBFMCCIAREA			"FMC"	// Flightmanager assigned to CCI area (JOBPOOL exist also) (flight manager assigned to CCI area without flight)
#define JOBFMREGNAREA			"FMR"	// Flightmanager assigned to Regn area (JOBPOOL exist also) (flight manager assigned to Regn area without flight)
#define JOBFMPSTAREA			"FMP"	// Flightmanager assigned to Psotion area (JOBPOOL exist also) (flight manager assigned to Position area without flight)
#define JOBPOOL					"POL"	// EmpData (in StaffGanttChart), StaffData (Blue bar (shift) in MA diagram)
#define JOBDETACH				"DET"	// EmpData (in StaffGanttChart), Original and new Pool, StaffData (detatch pool job - MA loaned to another pool)
#define JOBCCI					"CCI"	// CCIData (in CCIGanttChart), EmpData (in StaffGanttChart), StaffData  (CCI-MA assign - no flight)
#define JOBBREAK				"BRK"	// EmpData (in StaffGanttChart), StaffData (Pause)
#define JOBILLNESS				"ILL"	// EmpData (in StaffGanttChart), StaffData (Krank - NOT USED)
#define JOBSPECIAL				"SPE"	// EmpData (in StaffGanttChart), StaffData (Sondereinsatz - any non-normal job)
#define JOBSTAIRCASE			"SCG"	// Special Job for Staircase Gates, will be displayed (Jobs in staircase)
#define JOBTEMPABSENCE			"TABS"	// EmpData (in StaffGanttChart), StaffData (Grey background bar drawn on top of a pool job) in GateDiagram, // no flight, no demand.
#define JOBTEAMDELEGATION		"DEL"	// the pool job has been delegated to another team
#define	JOBFID					"FID"	// represents a flight independant job
#define JOBFMCICAREA			"FMC"	// Flightmanager assigned to CCI area (JOBPOOL exist also) (flight manager assigned to CCI area without flight)
#define JOBCIC					"CCI"	// CCIData (in CCIGanttChart), EmpData (in StaffGanttChart), StaffData  (CCI-MA assign - no flight)
#define JOBCICSPECIAL			"CCS"	// represent a special CCI Job (Special CCI-MA assign - no flight)
#define	JOBCCC					"CCC"	// represent a common checkin counter job
#define	JOBEQUIPMENTFASTLINK	"EFL"	// represents a fast link on the equipment diagram
#define	JOBEQUIPMENT			"EQU"	// equipment job
#define	JOBDELEGATEDFLIGHT		"DFJ"	// delegated flight job
#define	JOBRESTRICTEDFLIGHT		"RFJ"	// restricted flight job

typedef struct
{
	CCSPtrArray <JOBDATA> Data;
} FASTLINK;


class CedaJobData: public CCSCedaData
{
public:
	CedaJobData();
	~CedaJobData();
	CString Dump(long lpUrno);
	void PrepareFieldList();
	void AddFieldIfItExists(const char *pcpField);
	bool ClearAll();
	bool Read(char *pcpWhere, bool bpReset = true, bool bpSendDdx = false);
	bool ReadJob(long lpJobUrno);
	bool ReadAllJobs(CTime opStartTime, CTime opEndTime);
	bool AddIncomingJob(JOBDATA *prpJob);
	bool AddJob(JOBDATA *prpJob, const char *pcpDscr, BOOL bpSendDDX = TRUE, bool bpUpdateDB = true);
	JOBDATA *AllocNewJob(JOBDATA *prpJob);
	bool AddJobInternal(JOBDATA *prpJob);
	bool JobHasCorrectTemplate(JOBDATA *prpJob);
	bool JobHasFullFieldList(JOBDATA *prpJob);
	bool ChangeJobTime(long lpJobu, CTime opNewStart, CTime opNewEnd, const char *pcpDscr);
	void UpdateMaps(JOBDATA *prpJob, JOBDATA *prpOldJob);
	bool ChangeJobData(JOBDATA *prpJob, JOBDATA *prpOldJob, const char *pcpDscr);
	bool SendToPda(JOBDATA *prpJob);
	bool DeleteJob(long lpUrno,BOOL IsFromBC = FALSE, bool blUpdateDB = true);
	JOBDATA *DeleteJobInternal(long lpUrno);
	void GetJobsWithoutDemands(CCSPtrArray<JOBDATA> &ropJobs, long lpArrUrno, long lpDepUrno, CString opAlid = "", CString opAloc = "", int ipDemType = PERSONNELDEMANDS);
	bool GetJobsByFlur(CCSPtrArray<JOBDATA> &ropJobs, long lpFlur, BOOL bpWantJOBFM=TRUE, CString opAlid = "", CString opAloc = "", int ipDemType = PERSONNELDEMANDS,bool bpForWholeRotation = false,int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	bool IsTurnaroundJob(JOBDATA *prpJob);
	bool IsTurnaroundJob(long lpJobUrno);
	bool GetFmJobsByFlur(CCSPtrArray<JOBDATA> &ropJobs, long lpFlur);
	bool GetJobsByPkno(CCSPtrArray<JOBDATA> &ropJobs, const char *pcpPkno, const char *pcpJobType = "", const char *pcpIgnoreJobType = "");
	void GetJobsByUstf(CCSPtrArray<JOBDATA> &ropJobs, long lpUstf, bool bpReset = true);
	void GetJobsByUdem(CCSPtrArray<JOBDATA> &ropJobs, long lpUdem, bool bpReset = true);
	bool DemandHasJobs(long lpUdem);
	bool JobHasDemand(long lpUjob);
	bool JobHasDemand(JOBDATA *prpJob);
	long GetFirstJobUrnoByDemand(long lpUdem);
	void GetJobsByJour(CCSPtrArray<JOBDATA> &ropJobs, long lpJour, bool bpReset = true);
	void GetJobsByUequ(CCSPtrArray<JOBDATA> &ropJobs, long lpUequ, bool bpReset = true);
	bool GetJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs, const char *pcpAlid = "", const char *pcpAloc = "");
	bool GetSpecialJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs, const char *pcpAlid = "", const char *pcpAloc = "");
	bool GetJobsByPoolJob(CCSPtrArray<JOBDATA> &ropJobs, long lpUrno, bool bpReset = true);
	bool GetJobsByPoolJobAndType(CCSPtrArray<JOBDATA> &ropJobs, long lpPoolJobUrno, char *pcpJobType, bool bpReset = true);
	void GetPoolJobsByUstf(CCSPtrArray<JOBDATA> &ropPoolJobs, long lpUstf, CTime opFrom = TIMENULL, CTime opTo = TIMENULL);
	bool GetJobsByPoolId(CCSPtrArray<JOBDATA> &ropJobs, CString opPoolId);
	bool GetJobsByShur(CCSPtrArray<JOBDATA> &ropJobs, long lpShur, BOOL bpWantPoolJobOnly = TRUE, bool bpReset = true);
	int GetJobsByShurAndType(CCSPtrArray<JOBDATA> &ropJobs, long lpShur, char *pcpJobType  = "", bool bpReset = true);
	JOBDATA  *GetJobByUrno(long lpUrno);
	JOBDATA  *GetMasterJob (JOBDATA  *lpJob);
	JOBDATA  *GetMasterJobByUrno (long lpUrno);
	bool DbInsertJob(JOBDATA *prpJob);
	bool DbInsertJob2(JOBDATA *prpJob);
	bool DbUpdateJob(JOBDATA *prpJob, JOBDATA *prpOldJob = NULL);
	bool DbUpdateJob2(JOBDATA *prpJob, JOBDATA *prpOldJob = NULL);
	bool DbDeleteJob(long lpJobUrno);
	bool DbDeleteJob2(long lpJobUrno);
	bool GetChangedFields(JOBDATA *prpJob, JOBDATA *prpOldJob, char *pcpFieldList, char *pcpData);
	bool GetAllFields(JOBDATA *prpJob, char *pcpFieldList, char *pcpData);
	void GetNewFields(JOBDATA *prpJob, char *pcpFieldList, char *pcpData);
	bool GetFmgJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs,char const *pcpAlid);
	bool GetFmcJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs, char const *pcpAlid);
	bool GetFmrJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs,char const *pcpAlid);
	JOBDATA *GetPoolJobForDetachJob(long lpJobDetachUrno);
	bool GetTeamDelegationsForPoolJob(long lpPoolJobUrno, CCSPtrArray <JOBDATA> &ropJobs, bool bpReset = true);
	bool GetPoolJobsForTeamDelegation(long lpDetachUrno,CCSPtrArray <JOBDATA> &ropJobs, bool bpReset = true);
	bool PoolJobIsDelegated(long lpPoolJobUrno);
	bool PoolJobIsDelegation(JOBDATA *prpPoolJob);
	JOBDATA *GetJobPoolByPreplanData(CString opPeno,CString opAlid,CTime opAcfr,CTime opActo);
	JOBDATA *GetJobPoolByUrno(long lpJobUrno);
	void PrepareDataAfterRead(JOBDATA *prpJob);
	void SetJobFlightFields(void);
	void PrepareDataForWrite(JOBDATA *prpJob);
	void SetAdditionalJobFields(JOBDATA *prpJob);
	void ConvertDatesToUtc(JOBDATA *prpJob);
	void ConvertDatesToLocal(JOBDATA *prpJob);
	bool AutoAssignDemands(CString opFieldList,CString opDataList);
	void AutoAssignDemandsRegn(CString opFieldList,CString opDataList);
	CString GetJobStatusString(JOBDATA *prpJob);
	void InitNewJob(JOBDATA *prpJob, const char *pcpJobType, const char *pcpAlid, const char *pcpAloc,	CTime opFrom, CTime opTo, long lpUdrr, const char *pcpPeno,	long lpFlur = 0L, long lpJour = 0L, const char *pcpText = NULL, const char *pcpDety = NULL, long lpUdem = 0L, bool bpReinitialize = false);
	int GetJobsByType(const char *pcpJobType, CCSPtrArray <JOBDATA> &ropJobs, bool bpReset = true);
	bool JobForUprmExist(const long lpUprm);
	void GetJobsByUprm(CCSPtrArray<JOBDATA> &ropJobs, long lpUprm, bool bpReset = true);
	int GetEquipmentFastLinkJobsByPoolJob(CCSPtrArray<JOBDATA> &ropFastLinkJobs, CCSPtrArray<JOBDATA> &ropPoolJobs);
	int GetEquipmentFastLinkJobsByPoolJob(CCSPtrArray<JOBDATA> &ropFastLinkJobs, long lpPoolJobUrno);
	bool GetEquipmentFastLinkJobsByUequAndTime(CCSPtrArray <FASTLINK> &ropFastLinks, long lpUequ, CTime opStart, CTime opEnd);
	bool GetEquipmentFastLinkJobsByUstfAndTime(CCSPtrArray <FASTLINK> &ropFastLinks, long lpUstf, CTime opStart, CTime opEnd);
	bool FilterFastLinkJobs(CCSPtrArray <JOBDATA> &ropSourceJobs, CCSPtrArray <FASTLINK> &ropFastLinks, CTime opStart, CTime opEnd);
	bool EmpIsFree(long lpUstf, CTime opFrom, CTime opTo);
	void HandleJobTemplateChange(long lpJobUrno, long lpDemandUrno);
	void ProcessJobInsert(void *vpDataPointer);
	void ProcessJobInsert(char *pcpFields, char *pcpData);
	void ProcessJobUpdate(void *vpDataPointer);
	bool ProcessJobUpdate(long lpJobUrno, char *pcpFields, char *pcpData);
	void ProcessJobDelete(void *vpDataPointer);
	void ProcessJobDelete(long lpJobUrno);
	bool ProcessAttachment(CString &ropAttachment);
	void ProcessFlightChange(long lpFlightUrno);
	void ProcessFlightChange(FLIGHTDATA *prpFlight);
	bool DeleteDelegatedFlightJob(long lpUrno, BOOL bpSendDdx = FALSE, bool bpUpdateDB = true);

	void CheckForMissingFlights(void);
	void CheckForMissingFlight(JOBDATA *prpJob, const char *pcpText = NULL, bool bpReadMissingFlight = false);
	void ReadMissingFlights(const char *pcpText);
	CMapPtrToPtr omMissingFlights;
	CUIntArray omJobsWhichHaveMissingFlights; // used for TRACE only
	void CheckForMissingFlightJobs(CDWordArray &ropFlightUrnos);
	CDWordArray omTmpUrnoList;

public:
    CMapPtrToPtr	omUrnoMap;
	CMapStringToPtr omPknoMap;
	CMapPtrToPtr	omFlightMap;
	CMapPtrToPtr	omFlightFmMap;
	CMapPtrToPtr	omUstfMap;
	CMapPtrToPtr	omUdemMap;
	CMapPtrToPtr	omJourMap;
	CMapPtrToPtr	omUequMap;
	CMapPtrToPtr	omShurMap;
	CMapPtrToPtr	omUtplFilterMap;
	CMapPtrToPtr	omUaidMap;
	CMapPtrToPtr	omUprmMap;

	CCSPtrArray <JOBDATA> omData;
	CTime omStartTime, omEndTime;
	char pcmAllExistingFields[500]; // only the fields that really exist in JOBTAB

	bool bmIsOffline, bmNoOfflineChangesYet;
	CCSPtrArray <JOBDATA> omOnlineData;
    CMapPtrToPtr omOnlineUrnoMap;
	CMapPtrToPtr omDeletedOfflineJobs; // points to jobs in omOnlineData that have been deleted in omData whilst offline

	bool WasDeletedOffline(long lpJobUrno); 
	void SetReleaseButton(void);
	JOBDATA *GetOnlineJobByUrno(long lpJobUrno);
	void DeleteFromOnlineData(long lpJobUrno);
	void UpdateOnlineData(JOBDATA *prpJob);
	void SetOffline();
	void SetOnline(bool bpRelease);
	bool JobsChangedOffline(void);
	bool JobChangedOffline(JOBDATA *prpOfflineJob);
	void ReleaseJobsChangedOffline(void);
	void RollbackJobsChangedOffline(void);
	bool Release(CString opReleaseString);
	void CreateBackupData(void);
	void GetJobsChangedOffline(CUIntArray &ropJobUrnos);

	CString GetTableName(void);
	CString GetLastError(void);

	bool bmTransactionActivated;
	CMapPtrToPtr omIrtJobs, omUrtJobs, omDrtJobs;
	void StartTransaction(void);
	void CommitTransaction(void);
	bool AddToIrtTransaction(JOBDATA *prpJob);
	bool AddToUrtTransaction(JOBDATA *prpJob);
	bool AddToDrtTransaction(long lpJobUrno);

	void ProcessJobRelease(void *vpDataPointer);
	void ReloadStaffJobs(CUIntArray &ropStfUrnos, CUIntArray &ropJtyUrnos);

	void ProcessJobsUpdate(void *vpDataPointer);
	void ReReadJobs(CString &ropJobUrnoList);

	bool PoolJobHasFidJobs(long lpPoolJobUrno, bool bpIgnoreFinishedJobs = true);
	bool IsStandby(JOBDATA *prpPoolJob);
	bool IsAbsent(JOBDATA *prpPoolJob);

	bool bmAutoAssignInProgress;
	void SetAutoAssignInProgress();
	void ProcessAflEnd(void *vpDataPointer);
	bool NoAssignmentInProgress(CWnd *polWnd, bool bpDisplayMessage = true);

	void SendJodDdx(JOBDATA *prpJob, JOBDATA *prpOldJob);
	void SendJodDdx(int ipDdxType, long lpUjob, long lpUdem);

	bool FlightHasJobs(long lpFlur);
	bool LoadJobsByUdem(CCSPtrArray <JOBDATA> &ropJobs, CString opUdemList);
};

#endif
