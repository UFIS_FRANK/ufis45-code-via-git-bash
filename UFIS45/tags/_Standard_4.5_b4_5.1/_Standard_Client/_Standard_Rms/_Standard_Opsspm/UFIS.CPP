// cdemandd.cpp - Read and write demand data
//
// Description:
// There is only one PC which connected directly to the CEDA server. In order to
// let everybody in the development team being able to test his/her source code,
// this file contains the routines which will simulate the server if the symbol
// "NOCEDA" was defined (see menu Option/Project/Compiler/Preprocessor).
//
//
// Written by:
// Damkerng Thammathakerngkit   May 10, 1996

#ifdef  NOCEDA
#include <ufis.h>

int WINAPI InitCom(LPSTR HostType, LPSTR CedaHost)
{
    return 0;
}

int WINAPI CallCeda(const LPSTR req_id, const LPSTR dest1, const LPSTR dest2,
      const LPSTR cmd, const LPSTR object, const LPSTR seq, const LPSTR tws, const LPSTR twe,
      const LPSTR selection, const LPSTR fields, const LPSTR data, const LPSTR data_dest)
{
    return 0;
}

int WINAPI CleanupCom(void)
{
    return 0;
}

int WINAPI UfisDllAdmin(const LPSTR parm1, const LPSTR parm2, const LPSTR parm3)
{
    return 0;
}

int WINAPI GetBufferHandle(LPSTR buf_slot, HGLOBAL *pHandle)
{
    return -1;
}

int WINAPI GetResultBuffer(LPSTR pResult, int buflen, LPSTR buf_slot,
      int line_no, int line_cnt, LPSTR item_list)
{
    static char *data[] =
    {
        "117,1,Janet A. McCarthy,21427.27,203/569-1163,1417 S Dearborn Circle,East Hartford,CT,06118",
        "150,1,Robert Watters,24762.85,617/894-4332,1225 Church St.,Waltham,MA,02254",
        "175,1,Doris Dupuy,20203.99,617/679-4071,336 N.E. 23rd St,Fall River,MA,02721",
        "180,1,T Staziola Townsend,7798.34,617/273-2904,1012 C Renault Street,Burlington,MA,01803",
        "240,2,John Hodge,102797.63,412/981-7562,621 Industrial Way,Hermitage,PA,16148",
        "271,2,Howard Schneider,42181.43,215/432-3022,709 South Broad Ave,Allentown,PA,18104",
        "281,2,Jay Kennedy,50521.98,212/246-5348,740 Summer Ave,New York,NY,10019",
        "294,2,Tom Caravello,69118.59,201/529-2831,4821 W. Lunt Ave.,Mahwah,NJ,07430",
        "322,3,E. Faulkner,16004.70,803/776-8662,938 Sherman Way,Columbia,SC,29210",
        "328,3,Keith Sutherland,34088.44,704/788-5693,40-44 Hatteras St,Concord,NC,28025",
        "353,3,Jim Surbrook,27282.52,904/392-2454,975 South Avenue,Gainesville,FL,32610",
        "360,3,Ned Hicks,49292.86,912/232-7995,4109 South Hall,Savanah,GA,31401",
        "366,3,Robert Angelo,26737.83,305/994-8367,117 Saxer Street,Boca Raton,FL,33431",
        "391,3,Kim Baer,122106.33,305/441-1177,8129 E 52nd Ave,Coral Gables,FL,33134",
        "410,4,John Savage,73328.68,513/761-1520,1165 Arapaho Street,Cincinnati,OH,45215",
        "423,4,John Miller,46190.76,701/847-2894,45 Ben Street,Buxton,ND,58218",
        "434,4,Mark Kalbfell,11006.82,417/882-7695,3487 Bardera Street,Springfield,MO,65804",
        "478,4,Sharon Gifford,66836.32,513/489-4400,12 Peter Howe Ave,Cincinnati,OH,45242",
        "482,4,Bill Diaddigo,40290.04,414/272-9290,506 East Howard Terrace,Milwaukee,WI,53202",
        "488,4,Ned Auch,56986.75,314/771-3371,1150 Canal Boulevard,St Louis,MO,63110",
        "514,5,Stan Pipe,53667.21,602/275-8158,1035 Arcade Row,Phoenix,AZ,85034",
        "547,5,Tony Baer,34111.19,915/544-4264,8129 East 154st St.,El Paso,TX,79901",
        "576,5,Ken C Dutcher,36895.99,214/563-7780,938 Broadway Place,Terrell,TX,75160",
        "582,5,Paul Powell,60917.89,602/839-3378,3015 S 18th,Tempe,AZ,85283",
        "594,5,Fuji Retell,8087.56,303/449-7283,205 Fruitwood Street,Boulder,CO,80301",
        "598,5,Carolyn Macioce,17500.91,303/238-9796,1068 Meridian Drive,Lakewood,CO,80215",
        "647,6,Jan Buoniconti,135320.65,213/306-8097,761 Hale Circle,Los Angeles,CA,90066",
        "662,6,K. Raszka,37050.83,619/571-7556,1306 Rural Route #4,San Diego,CA,92111",
        "667,6,Susan K. Overall,97559.62,408/429-9172,555 N. Monroe Street,Santa Cruz,CA,95064",
        "671,6,Bob Wubben,152324.03,503/234-6754,3035 All Saints Ave,Portland,OR,97202",
        "675,6,Ms. Cindy DelGado,103686.90,714/662-2833,817 Concentric Loop,Costa Mesa,CA,92626",
        "679,6,Daniel Small,17119.83,415/668-6466,1330 36th St.,San Francisco,CA,94118"
    };

    if (!(0 <= line_no && line_no < sizeof(data)/sizeof(data[0])))
        return -1;

    strcpy(pResult, data[line_no]);
    return 0;
}

#endif
