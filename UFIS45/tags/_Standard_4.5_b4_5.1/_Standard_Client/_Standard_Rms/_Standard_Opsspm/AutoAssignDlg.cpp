// AutoAssignDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <AutoAssignDlg.h>
#include <Basicdata.h>
#include <WaitAssignDlg.h>
#include <CCSPrint.h>
#include <Table.h>
#include <CCSDragDropCtrl.h>
#include <ButtonList.h>
#include <CedaEqtData.h>
#include <DlgSettings.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/

/////////////////////////////////////////////////////////////////////////////
// CAutoAssignDlg dialog


CAutoAssignDlg::CAutoAssignDlg(CTime opLoadStart, CTime opLoadEnd, CString opChartType, CStringArray &ropReduction,CWnd* pParent)
	: CDialog(CAutoAssignDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAutoAssignDlg)
	m_AssignWOAloc = FALSE;
	m_BreakPrio = TRUE;
	m_Reduction = _T("");
	m_ShiftStartBuff = _T("0");
	m_ShiftEndBuff = _T("0");
	m_ToTime = _T("");
	m_ToDay = _T("");
	m_JobBuff = _T("0");
	m_FromDay = _T("");
	m_FromTime = _T("");
	m_FlightOrig = _T("");
	m_FlightRegn = _T("");
	m_FlightNumber = _T("");
	m_FlightDest = _T("");
	m_FlightAirline = _T("");
	m_FlightAircraft = _T("");
	m_FlightSuffix = _T("");
	m_LinkedDemands = FALSE;
	m_DemandType = 0;
	m_AssignEquipment = FALSE;
	m_AssignPersonnel = TRUE;
	m_Deviation1 = 0;
	m_Deviation2 = 0;
	omBreakBuffer = _T("");
	//}}AFX_DATA_INIT

	omChartType = opChartType;
	omLoadEnd = opLoadEnd;
	omLoadStart = opLoadStart;

	bmDisableFlightFilter = false;
	bmDisplayPersonnel = true;
	bmDisplayEquipment = false;
	bmPersonnelTab = true;

	// display either equipment type or equipment static groups depending on what is selected in the equipment diagram
	pomEquipmentViewer = new EquipmentDiagramViewer();
	pomEquipmentViewer->SetViewerKey("EquipmentDiagram");
	CString olViewName = pomEquipmentViewer->GetViewName();
	if(olViewName.IsEmpty())
	{
		olViewName = ogCfgData.rmUserSetup.EQCV;
	}
	pomEquipmentViewer->SelectView(olViewName);
	bmUseEquTypes = (pomEquipmentViewer->GetGroup() == "EquipmentType") ? true : false;

//	m_ReductionBox.ResetContent();
//	for (int ilRed = 0; ilRed < ropReduction.GetSize(); ilRed++)
//	{
//		m_ReductionBox.AddString(ropReduction[ilRed]);
//	}
}



void CAutoAssignDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutoAssignDlg)
	DDX_Control(pDX, IDC_PERSONNEL_CHECKBOX, m_PersonnelCheckboxCtrl);
	DDX_Control(pDX, IDC_EQUIPMENT_CHECKBOX, m_EquipmentCheckboxCtrl);
	DDX_Control(pDX, IDC_EMPEQUTAB, m_EmpEquTabCtrl);
	DDX_Control(pDX, IDC_REDUCTION, m_ReductionBox);
	DDX_Control(pDX, IDC_POSSIBLELIST, m_PossibleList);
	DDX_Control(pDX, IDC_SELECTEDLIST, m_SelectedList);
	DDX_Check(pDX, IDC_ASSIGNWOALOC, m_AssignWOAloc);
	DDX_Check(pDX, IDC_BREAKPRIO, m_BreakPrio);
	DDX_CBString(pDX, IDC_REDUCTION, m_Reduction);
	DDX_Text(pDX, IDC_SHIFTSTARTBUFF, m_ShiftStartBuff);
	DDX_Text(pDX, IDC_SHIFTENDBUFF, m_ShiftEndBuff);
	DDX_Text(pDX, IDC_TOTIME, m_ToTime);
	DDX_Text(pDX, IDC_TODAY, m_ToDay);
	DDX_Text(pDX, IDC_JOBBUFF, m_JobBuff);
	DDX_Text(pDX, IDC_FROMDAY, m_FromDay);
	DDX_Text(pDX, IDC_FROMTIME, m_FromTime);
	DDX_Text(pDX, IDC_FLIGHTORIG, m_FlightOrig);
	DDX_Text(pDX, IDC_FLIGHTREGN, m_FlightRegn);
	DDX_Text(pDX, IDC_FLIGHTNUMBER, m_FlightNumber);
	DDX_Text(pDX, IDC_FLIGHTDEST, m_FlightDest);
	DDX_Text(pDX, IDC_FLIGHTAIRLINE, m_FlightAirline);
	DDX_Text(pDX, IDC_FLIGHTAIRCRAFT, m_FlightAircraft);
	DDX_Text(pDX, IDC_FLIGHTSUFFIX, m_FlightSuffix);
	DDX_Check(pDX, IDC_LINKEDDEMANDS, m_LinkedDemands);
	DDX_Check(pDX, IDC_EQUIPMENT_CHECKBOX, m_AssignEquipment);
	DDX_Check(pDX, IDC_PERSONNEL_CHECKBOX, m_AssignPersonnel);
	DDX_Text(pDX, IDC_DEVIATION1, m_Deviation1);
	DDV_MinMaxUInt(pDX, m_Deviation1, 0, 99);
	DDX_Text(pDX, IDC_DEVIATION2, m_Deviation2);
	DDV_MinMaxUInt(pDX, m_Deviation2, 0, 99);
	DDX_Text(pDX, IDC_BREAKBUFF, omBreakBuffer);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAutoAssignDlg, CDialog)
	//{{AFX_MSG_MAP(CAutoAssignDlg)
	ON_BN_CLICKED(IDC_DELONLY, OnDelonly)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_OPENONLY, OnOpenonly)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_NOTIFY(TCN_SELCHANGE, IDC_EMPEQUTAB, OnSelChangeEmpEquTab)
	ON_BN_CLICKED(IDC_EQUIPMENT_CHECKBOX, OnEquipmentCheckbox)
	ON_BN_CLICKED(IDC_PERSONNEL_CHECKBOX, OnPersonnelCheckbox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutoAssignDlg message handlers

void CAutoAssignDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CAutoAssignDlg::OnDelonly() 
{
	omAssignType = "DEL";
	OnOK();
	
}

void CAutoAssignDlg::OnAll() 
{
	omAssignType = "ALL";
	OnOK();

}

void CAutoAssignDlg::OnOpenonly() 
{
	omAssignType = "OPEN";
	OnOK();	
}

void CAutoAssignDlg::OnAdd() 
{
	CString olText;

	// Move selected items from left list box to right list box
	for (int ilLc = m_PossibleList.GetCount()-1; ilLc >= 0 ; ilLc--)
	{
		if (!m_PossibleList.GetSel(ilLc))	// unselected item?
			continue;

		m_PossibleList.GetText(ilLc, olText);	// load string to "olText"
		m_SelectedList.AddString(olText);	// move string from left to right box
		m_PossibleList.DeleteString(ilLc);

		long llUrno = 0L;
		if(bmPersonnelTab)
		{
			if(omPossiblePools.Lookup(olText, (void *&) llUrno))
			{
				omPossiblePools.RemoveKey(olText);
				omSelectedPools.SetAt(olText, (void *) llUrno);
			}
		}
		else
		{
			if(omPossibleEquipment.Lookup(olText, (void *&) llUrno))
			{
				omPossibleEquipment.RemoveKey(olText);
				omSelectedEquipment.SetAt(olText, (void *) llUrno);
			}
		}
	}

	
}

void CAutoAssignDlg::OnDelete() 
{
	CString olText;

	for (int ilLc = m_SelectedList.GetCount()-1; ilLc >= 0 ; ilLc--)
	{
		if (!m_SelectedList.GetSel(ilLc))	// unselected item?
			continue;

		m_SelectedList.GetText(ilLc, olText);	// load string to "olText"
		m_PossibleList.AddString(olText);	// move string from left to right box
		m_SelectedList.DeleteString(ilLc);

		long llUrno = 0L;
		if(bmPersonnelTab)
		{
			if(omSelectedPools.Lookup(olText, (void *&) llUrno))
			{
				omSelectedPools.RemoveKey(olText);
				omPossiblePools.SetAt(olText, (void *) llUrno);
			}
		}
		else
		{
			if(omSelectedEquipment.Lookup(olText, (void *&) llUrno))
			{
				omSelectedEquipment.RemoveKey(olText);
				omPossibleEquipment.SetAt(olText, (void *) llUrno);
			}
		}
	}

}

void CAutoAssignDlg::SetEquipment(void)
{
	if(bmDisplayEquipment)
	{
		UpdateData(TRUE);
		omPossibleEquipment.RemoveAll();
		omSelectedEquipment.RemoveAll();
		if(bmUseEquTypes)
		{
			int ilNumEqt = ogEqtData.omData.GetSize();
			for(int ilEqt = 0; ilEqt < ilNumEqt; ilEqt++)
			{
				EQTDATA *prlEqt = &ogEqtData.omData[ilEqt];
				omPossibleEquipment.SetAt(prlEqt->Name, (void *) prlEqt->Urno);
			}
		}
		else
		{
			CCSPtrArray <ALLOCUNIT> olEquipmentGroups;
			ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_EQUIPMENTGROUP,olEquipmentGroups);
			int ilNumGroups = olEquipmentGroups.GetSize();
			for(int ilG = 0; ilG < ilNumGroups; ilG++)
			{
				ALLOCUNIT *prlEquGroup = &olEquipmentGroups[ilG];
				omPossibleEquipment.SetAt(prlEquGroup->Name, (void *) prlEquGroup->Urno);
			}
		}
		long llUrno = 0L;
		CString olText;
		POSITION rlPos;
		m_PossibleList.ResetContent();
		for(rlPos = omPossibleEquipment.GetStartPosition(); rlPos != NULL; )
		{
			omPossibleEquipment.GetNextAssoc(rlPos, olText, (void *&) llUrno);
			m_PossibleList.AddString(olText);
		}

		m_SelectedList.ResetContent();
		for(rlPos = omSelectedEquipment.GetStartPosition(); rlPos != NULL; )
		{
			omSelectedEquipment.GetNextAssoc(rlPos, olText, (void *&) llUrno);
			m_SelectedList.AddString(olText);
		}
		CWnd *polWnd = NULL;
		if((polWnd = GetDlgItem(IDC_RESOURCEGROUPS)) != NULL)
		{
			if(bmUseEquTypes)
				polWnd->SetWindowText(GetString(IDS_AAD_EQUIPMENTTYPES));
			else
				polWnd->SetWindowText(GetString(IDS_AAD_EQUIPMENTGROUPS));
		}
	}
}

void CAutoAssignDlg::SetData(CUIntArray &ropDemUrnoList, CStringArray &ropResourceGroupArray,CTime opAssignFrom,CTime opAssignTo,int ipDemandType /*= PERSONNELDEMANDS*/)
{
	omPossiblePools.RemoveAll();

	CStringArray olTmpArray;
	for(int ilLc = 0; ilLc < ropResourceGroupArray.GetSize();ilLc++)
	{
		ogBasicData.ExtractItemList(ropResourceGroupArray[ilLc], &olTmpArray,'#');
		omPossiblePools.SetAt(olTmpArray[0],(void *) atol(olTmpArray[1]));
	}

	omDemUrnoList.RemoveAll();
	for(ilLc = 0; ilLc < ropDemUrnoList.GetSize();ilLc++)
	{
		omDemUrnoList.Add(ropDemUrnoList[ilLc]);
	}

	bmDisplayPersonnel = (ipDemandType & PERSONNELDEMANDS) ? true : false;
	bmDisplayEquipment = (ipDemandType & EQUIPMENTDEMANDS) ? true : false;
	if(bmDisplayPersonnel)
	{
		m_AssignPersonnel = TRUE;
		m_AssignEquipment = FALSE;
	}
	else
	{
		m_AssignPersonnel = FALSE;
		m_AssignEquipment = TRUE;
	}
	bmPersonnelTab = false;

	m_ToTime = opAssignTo.Format("%H:%M");
	m_ToDay = opAssignTo.Format("%d.%m.%Y");
	m_FromDay = opAssignFrom.Format("%d.%m.%Y");
	m_FromTime = opAssignFrom.Format("%H:%M");
	m_FlightOrig = "";
	m_FlightRegn = "";
	m_FlightNumber = "";
	m_FlightDest = "";
	m_FlightAirline = "";
	m_FlightAircraft = "";
	m_FlightSuffix = "";

	bmSetWarning = false;
}


bool CAutoAssignDlg::IsDemPassFilter(DEMANDDATA * prpDem) 
{
	bool blDemIsOk = true;
	JOBDATA *prlJob = NULL;
	CCSPtrArray <JOBDATA> olJobs;

	bool blOnlyOpen = (omAssignType == "OPEN")?true:false;
	bool blDelOnly = (omAssignType == "DEL")?true:false;

	if(prpDem == NULL)
	{
		return false;
	}

	if (!ogDemandData.AutomaticAssignmentEnabled(prpDem))
		return false;

	ogJodData.GetJobsByDemand(olJobs,prpDem->Urno);

	if(!strcmp(prpDem->Rety,PERSONNEL_DEMAND))
	{
		if(!m_AssignPersonnel)
			return false;
	}
	else if(!strcmp(prpDem->Rety,EQUIPMENT_DEMAND))
	{
		if(!m_AssignEquipment)
		   return false;
	}
	
	int ilNumJobs = olJobs.GetSize();
	if(ilNumJobs > 0 && blOnlyOpen)
	{
		blDemIsOk = false;
	}

	if(blDemIsOk)
	{

		for(int ilJob = 0; ilJob < ilNumJobs && blDemIsOk; ilJob++)
		{
			JOBDATA *prlJob = &olJobs[ilJob];
			if(prlJob->Stat[0] != 'P' || prlJob->Stat[2] == '1' || prlJob->Infm) // planned || fix || Employee informed
			{
				blDemIsOk = false;
			}
		}
	}
	if(blDemIsOk)
	{
		if (!(omAssignStart < prpDem->Deen && omAssignEnd > prpDem->Debe))
		{
			blDemIsOk = false;
		}
	}
	if(blDemIsOk)
	{
		CString olAlid = prpDem->Alid;

		olAlid.TrimRight(); 
		if (m_AssignWOAloc == FALSE && olAlid.IsEmpty())
		{
			blDemIsOk = false;
		}
	}
	if(blDemIsOk && FlightSpecified())
	{
		if(!strcmp(prpDem->Obty,"FID"))
		{
			blDemIsOk = false;
		}
		else
		{
			FLIGHTDATA * prlFlight = NULL;
			CDWordArray olFlights;
			bool blFlightFound = false;

			if(ogDemandData.IsFlightDependingDemand(prpDem->Dety))
			{
				if(prpDem->Ouri != 0L)
					olFlights.Add(prpDem->Ouri);
				if(prpDem->Ouro != 0L)
					olFlights.Add(prpDem->Ouro);
			}
			else
			{
				ogDemandData.GetFlightsFromCciDemand(prpDem,olFlights);
			}
			int ilNumFlights = olFlights.GetSize();
			for(int ilF = 0; ilF < ilNumFlights; ilF++)
			{
				if(IsFlightPassFilter(ogFlightData.GetFlightByUrno(olFlights[ilF])))
				{
					blFlightFound = true;
					break;
				}
			}			
			if(!blFlightFound)
			{
				blDemIsOk = false;
			}
		}
	}

	if(blDemIsOk && ilNumJobs > 0)
	{
		bmSetWarning = true;
	}
	return blDemIsOk;
}

		


bool CAutoAssignDlg::IsFlightPassFilter(FLIGHTDATA * prpFlight) 
{
	bool blOrig = true;
	bool blDest = true;
	bool blAlc = true;
	bool blFltn = true;
	bool blFlts = true;
	bool blAct = true;
	bool blRegn = true;
	CString olTmp;
	
	if(prpFlight == NULL)
	{
		return false;
	}

	if(!m_FlightOrig.IsEmpty())
	{
		if(strcmp(prpFlight->Adid, "A") == 0)
		{
			olTmp = prpFlight->Org3;
			blOrig = (m_FlightOrig == olTmp)?true:false;
/*****************************
			if (!blOrig)
			{
				olTmp = prpFlight->Org4;
				blOrig = (m_FlightOrig == olTmp)?true:false;
			}
***************/
		}
		else
		{
			blOrig = false;
		}
	}
	if(!m_FlightDest.IsEmpty())
	{
		if(strcmp(prpFlight->Adid, "D") == 0)
		{
			olTmp = prpFlight->Des3;
			blDest = (m_FlightDest == olTmp)?true:false;
/************************
			if (!blDest)
			{
				olTmp = prpFlight->Des4;
				blDest = (m_FlightDest == olTmp)?true:false;
			}
	***********************/
		}
		else
		{
			blDest = false;
		}
	}

	if(!m_FlightRegn.IsEmpty())
	{
		olTmp = prpFlight->Regn;
		blRegn = (m_FlightRegn == olTmp)?true:false;
	}

	if(!m_FlightNumber.IsEmpty())
	{
		olTmp = prpFlight->Fltn;
		blFltn = (m_FlightNumber == olTmp)?true:false;
	}

	if(!m_FlightAirline.IsEmpty())
	{
		olTmp = prpFlight->Alc2;
		blAlc = (m_FlightAirline == olTmp)?true:false;
		if(!blAlc)
		{
			olTmp = prpFlight->Alc3;
			blAlc = (m_FlightAirline == olTmp)?true:false;
		}
	}

	if(!m_FlightSuffix.IsEmpty())
	{
		olTmp = prpFlight->Flns;
		blFlts = (m_FlightSuffix == olTmp)?true:false;
	}

	
	if(!m_FlightAircraft.IsEmpty())
	{
		olTmp = prpFlight->Act3;
		blAct = (m_FlightAircraft == olTmp)?true:false;
/***********************
		if(!blAct)
		{
			olTmp = prpFlight->Act5;
			blAct = (m_FlightAircraft == olTmp)?true:false;
		}
	**********************/
	}


	return (blOrig && blDest && blAlc && blFltn && blFlts && blAct && blRegn);

}

bool CAutoAssignDlg::FlightSpecified()
{
	bool blFlightSpecified = false;
	if(!m_FlightOrig.IsEmpty() || !m_FlightDest.IsEmpty() 
		|| !m_FlightRegn.IsEmpty() || !m_FlightNumber.IsEmpty() 
		|| !m_FlightAirline.IsEmpty() || !m_FlightSuffix.IsEmpty() || !m_FlightAircraft.IsEmpty())
	{
		blFlightSpecified = true;
	}

	return blFlightSpecified;
}

void CAutoAssignDlg::SetDemandTypeFields()
{
	CWnd *polWnd = NULL;
	if(!m_AssignPersonnel || !m_AssignEquipment)
	{
		m_EmpEquTabCtrl.ShowWindow(SW_HIDE);
	}
	else
	{
		m_EmpEquTabCtrl.ShowWindow(SW_SHOW);
	}

	int ilStat = (!bmPersonnelTab) ? SW_HIDE : SW_SHOW;
	int ilStat2 = (bmPersonnelTab) ? SW_HIDE : SW_SHOW;
	if((polWnd = GetDlgItem(IDC_SHIFTSTARTBUFF)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_SHIFTSTARTBUFFTEXT)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_SHIFTENDBUFF)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_SHIFTENDBUFFTEXT)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_BREAKBUFF)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_BREAKBUFFTEXT)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_BREAKPRIO)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_LINKEDDEMANDS)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_DEVIATIONTITLE)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_DEVIATION1)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_DEVIATION2)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_DEVIATIONPLUS)) != NULL)
		polWnd->ShowWindow(ilStat);
	if((polWnd = GetDlgItem(IDC_DEVIATIONMINUS)) != NULL)
		polWnd->ShowWindow(ilStat);

	if(bmPersonnelTab)
	{
		if((polWnd = GetDlgItem(IDC_RESOURCEGROUPS)) != NULL)
			polWnd->SetWindowText(GetString(IDS_STRING61938));
		if((polWnd = GetDlgItem(IDC_JOBBUFFTEXT)) != NULL)
			polWnd->SetWindowText(GetString(IDS_STRING61949));
		if((polWnd = GetDlgItem(IDC_ALL)) != NULL)
			polWnd->SetWindowText(GetString(IDS_STRING61951));
		if((polWnd = GetDlgItem(IDC_OPENONLY)) != NULL)
			polWnd->SetWindowText(GetString(IDS_STRING61952));
		if((polWnd = GetDlgItem(IDC_DELONLY)) != NULL)
			polWnd->SetWindowText(GetString(IDS_STRING61953));

/*		if((polWnd = GetDlgItem(IDC_SHIFTENDBUFF)) != NULL)
			polWnd->ShowWindow(SW_SHOW);
		if((polWnd = GetDlgItem(IDC_SHIFTENDBUFFTEXT)) != NULL)
			polWnd->ShowWindow(SW_SHOW);
		if((polWnd = GetDlgItem(IDC_BREAKPRIO)) != NULL)
			polWnd->ShowWindow(SW_SHOW);
		if((polWnd = GetDlgItem(IDC_LINKEDDEMANDS)) != NULL)
			polWnd->ShowWindow(SW_SHOW);
*/
		long llUrno = 0L;
		CString olText;
		POSITION rlPos;
		m_PossibleList.ResetContent();
		for(rlPos = omPossiblePools.GetStartPosition(); rlPos != NULL; )
		{
			omPossiblePools.GetNextAssoc(rlPos, olText, (void *&) llUrno);
			m_PossibleList.AddString(olText);
		}

		m_SelectedList.ResetContent();
		for(rlPos = omSelectedPools.GetStartPosition(); rlPos != NULL; )
		{
			omSelectedPools.GetNextAssoc(rlPos, olText, (void *&) llUrno);
			m_SelectedList.AddString(olText);
		}
	}
	else
	{
		if((polWnd = GetDlgItem(IDC_RESOURCEGROUPS)) != NULL)
		{
			if(bmUseEquTypes)
				polWnd->SetWindowText(GetString(IDS_AAD_EQUIPMENTTYPES));
			else
				polWnd->SetWindowText(GetString(IDS_AAD_EQUIPMENTGROUPS));
		}

//		if((polWnd = GetDlgItem(IDC_JOBBUFFTEXT)) != NULL)
//			polWnd->SetWindowText(GetString(IDS_AAD_BUFFTEXT));
//		if((polWnd = GetDlgItem(IDC_ALL)) != NULL)
//			polWnd->SetWindowText(GetString(IDS_AAD_ALL));
//		if((polWnd = GetDlgItem(IDC_OPENONLY)) != NULL)
//			polWnd->SetWindowText(GetString(IDS_AAD_OPENONLY));
//		if((polWnd = GetDlgItem(IDC_DELONLY)) != NULL)
//			polWnd->SetWindowText(GetString(IDS_AAD_DELETE));

		long llUrno = 0L;
		CString olText;
		POSITION rlPos;
		m_PossibleList.ResetContent();
		for(rlPos = omPossibleEquipment.GetStartPosition(); rlPos != NULL; )
		{
			omPossibleEquipment.GetNextAssoc(rlPos, olText, (void *&) llUrno);
			m_PossibleList.AddString(olText);
		}

		m_SelectedList.ResetContent();
		for(rlPos = omSelectedEquipment.GetStartPosition(); rlPos != NULL; )
		{
			omSelectedEquipment.GetNextAssoc(rlPos, olText, (void *&) llUrno);
			m_SelectedList.AddString(olText);
		}
	}
}

BOOL CAutoAssignDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

//	initialize language specific resources
	SetWindowText(GetString(IDS_STRING61934));

	ReadDefaultsFromCedaIni();
	ogDlgSettings.EnableSaveToDb("AUTOASSIGNDLG"); // causes settings to be read/written to and from the DB
	if(bmDisplayPersonnel && bmDisplayEquipment)
	{
		ogDlgSettings.SetCheckboxValue(this, "AUTOASSIGNDLG", IDC_PERSONNEL_CHECKBOX);
		ogDlgSettings.SetCheckboxValue(this, "AUTOASSIGNDLG", IDC_EQUIPMENT_CHECKBOX);

		CString olValue;
		m_AssignPersonnel = (ogDlgSettings.GetValue("AUTOASSIGNDLG", ogDlgSettings.GetFieldName(IDC_PERSONNEL_CHECKBOX), olValue) && olValue == "1") ? TRUE : FALSE;
		m_AssignEquipment = (ogDlgSettings.GetValue("AUTOASSIGNDLG", ogDlgSettings.GetFieldName(IDC_EQUIPMENT_CHECKBOX), olValue) && olValue == "1") ? TRUE : FALSE;
		if(!m_AssignPersonnel && !m_AssignEquipment)
		{
			bmPersonnelTab = true;
			m_AssignPersonnel = TRUE;
		}
		else
		{
			bmPersonnelTab = (m_AssignPersonnel) ? true : false;
		}
		SetEquipment();
	}
	else if(bmDisplayEquipment)
	{
		m_AssignPersonnel = FALSE;
		m_AssignEquipment = TRUE;
		bmPersonnelTab = false;
		SetEquipment();
	}
	else
	{
		m_AssignPersonnel = TRUE;
		m_AssignEquipment = FALSE;
		bmPersonnelTab = true;
	}

	ogDlgSettings.SetCheckboxValue(this, "AUTOASSIGNDLG", IDC_ASSIGNWOALOC);
	ogDlgSettings.SetCheckboxValue(this, "AUTOASSIGNDLG", IDC_BREAKPRIO);
	ogDlgSettings.SetCheckboxValue(this, "AUTOASSIGNDLG", IDC_LINKEDDEMANDS);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTAIRLINE);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTNUMBER);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTSUFFIX);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTREGN);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTAIRCRAFT);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTORIG);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTDEST);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_JOBBUFF);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_SHIFTSTARTBUFF);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_SHIFTENDBUFF);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_DEVIATION1);
	ogDlgSettings.SetFieldValue(this, "AUTOASSIGNDLG", IDC_DEVIATION2);

	SetDemandTypeFields();

	CWnd *polWnd = GetDlgItem(IDC_TIMEFRAME);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61935));
	}
	
	polWnd = GetDlgItem(IDC_TIMEFROM);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61936));
	}

	polWnd = GetDlgItem(IDC_TIMETO);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61937));
	}

	polWnd = GetDlgItem(IDC_FLIGHTS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61939));
	}

	polWnd = GetDlgItem(IDC_FLIGHTNR);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61940));
	}

	polWnd = GetDlgItem(IDC_REGNTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61941));
	}
	
	polWnd = GetDlgItem(IDC_AIRCRAFTTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61942));
	}
	
	polWnd = GetDlgItem(IDC_ORIGINTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61943));
	}
	
	polWnd = GetDlgItem(IDC_DESTTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61944));
	}
	
	polWnd = GetDlgItem(IDC_PARAMETERS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61945));
	}
	
	polWnd = GetDlgItem(IDC_REDUCTIONTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61946));
	}
	
	polWnd = GetDlgItem(IDC_ASSIGNWOALOC);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61947));
	}
	
	polWnd = GetDlgItem(IDC_BREAKPRIO);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61948));
	}

	polWnd = GetDlgItem(IDC_LINKEDDEMANDS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_LINKEDDEMANDS));
	}
	
	polWnd = GetDlgItem(IDC_SHIFTSTARTBUFFTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_SHIFTSTARTBUFFTEXT));
	}
	
	polWnd = GetDlgItem(IDC_SHIFTENDBUFFTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61950));
	}
	 
	polWnd = GetDlgItem(IDC_BREAKBUFFTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_BREAK_BUFFER));
	}
	//PRF5802:  Initialise break buffer with default value from setup 
	omBreakBuffer.Format ( "%d", ogBasicData.imDefaultBreakBuffer );
	polWnd = GetDlgItem(IDC_BREAKBUFF);
	if (polWnd)
	{
		polWnd->SetWindowText(omBreakBuffer);
	}

	polWnd = GetDlgItem(IDCANCEL);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	if(!bmDisplayPersonnel || !bmDisplayEquipment)
	{
		if((polWnd = GetDlgItem(IDC_PERSONNEL_CHECKBOX)) != NULL)
			polWnd->ShowWindow(SW_HIDE);
		if((polWnd = GetDlgItem(IDC_EQUIPMENT_CHECKBOX)) != NULL)
			polWnd->ShowWindow(SW_HIDE);
		m_EmpEquTabCtrl.ShowWindow(SW_HIDE);
	}
	else
	{
		if((polWnd = GetDlgItem(IDC_PERSONNEL_CHECKBOX)) != NULL)
			polWnd->SetWindowText(GetString(IDS_AAD_PERSONNEL));
		if((polWnd = GetDlgItem(IDC_EQUIPMENT_CHECKBOX)) != NULL)
			polWnd->SetWindowText(GetString(IDS_AAD_EQUIPMENT));
		m_EmpEquTabCtrl.InsertItem(0, GetString(IDS_AAD_PERSONNEL));
		m_EmpEquTabCtrl.InsertItem(1, GetString(IDS_AAD_EQUIPMENT));
	}


	if (bmDisableFlightFilter)
	{
		CWnd *polWnd = GetDlgItem(IDC_FLIGHTAIRLINE);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}

		polWnd = GetDlgItem(IDC_FLIGHTNUMBER);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}

		polWnd = GetDlgItem(IDC_FLIGHTSUFFIX);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
		
		polWnd = GetDlgItem(IDC_FLIGHTREGN);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
		
		polWnd = GetDlgItem(IDC_FLIGHTAIRCRAFT);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
		
		polWnd = GetDlgItem(IDC_FLIGHTORIG);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}

		polWnd = GetDlgItem(IDC_FLIGHTDEST);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
	}

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAutoAssignDlg::OnOK() 
{
	if(!UpdateData(TRUE))
		return;

	AfxGetApp()->DoWaitCursor(1);

	if(bmDisplayPersonnel && bmDisplayEquipment)
	{
		ogDlgSettings.SaveCheckboxValue(this, "AUTOASSIGNDLG", IDC_PERSONNEL_CHECKBOX);
		ogDlgSettings.SaveCheckboxValue(this, "AUTOASSIGNDLG", IDC_EQUIPMENT_CHECKBOX);
	}
	ogDlgSettings.SaveCheckboxValue(this, "AUTOASSIGNDLG", IDC_ASSIGNWOALOC);
	ogDlgSettings.SaveCheckboxValue(this, "AUTOASSIGNDLG", IDC_BREAKPRIO);
	ogDlgSettings.SaveCheckboxValue(this, "AUTOASSIGNDLG", IDC_LINKEDDEMANDS);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTAIRLINE);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTNUMBER);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTSUFFIX);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTREGN);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTAIRCRAFT);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTORIG);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_FLIGHTDEST);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_JOBBUFF);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_SHIFTSTARTBUFF);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_SHIFTENDBUFF);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_DEVIATION1);
	ogDlgSettings.SaveFieldValue(this, "AUTOASSIGNDLG", IDC_DEVIATION2);


	CString olDemList;
	CString olDemUrno;

	CString olDataList;
	CString olFieldList;
	long llDemCount = 0;
	
	CTime olDate = TIMENULL;
	CTime olTime = TIMENULL;


	if(m_FromDay != "" && m_FromTime != "")
	{
		olDate = DateStringToDate(m_FromDay);
		if(olDate == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return;
		}
		
		olTime = HourStringToDate(m_FromTime, olDate);
		if(olTime == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return ;
		}
	}
	omAssignStart = olTime;
	if(m_ToDay != "" && m_ToTime != "")
	{
		olDate = DateStringToDate(m_ToDay);
		if(olDate == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return;
		}
		
		olTime = HourStringToDate(m_ToTime, olDate);
		if(olTime == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return ;
		}
	}
	omAssignEnd = olTime;

	/* Limit Time Span of Automatic Assignment, Prf: 6600  */
	char pclKeyword[100];
	int iLimitTimeSpan=0;
	CString olConfigFileName = ogBasicData.GetConfigFileName();
	strcpy(pclKeyword,"INITAUTOASSIGN_MAX_TIMEFRAME");
	iLimitTimeSpan = GetPrivateProfileInt(pcgAppName, pclKeyword, 48, olConfigFileName);

	if(iLimitTimeSpan <= 1 )
	{
		// Assign correct value for the Key INITAUTOASSIGN_MAX_TIMEFRAME=n in ceda.ini,where n is in hours
		MessageBox(GetString(IDS_AUTOASSIGN_MINLIMIT),GetString(IDS_STRING61934),MB_OK);
		return;
	}

	if((iLimitTimeSpan * 60 * 60) < (difftime(omAssignEnd.GetTime(),omAssignStart.GetTime())))
	{
		CString olText;

		// The time span for automatic allocation should be limited to %d hours
		olText.Format(GetString(IDS_AUTOASSIGNMAX), iLimitTimeSpan);
		MessageBox(olText, GetString(IDS_STRING61934), MB_OK);
		return ;
	}

	if((omAssignEnd > omLoadEnd) || (omAssignStart < omLoadStart) )
	{
		CString olText;

		// the assignment time (From: %s To: %s)\nmust be within the display time (From: %s To: %s)
		olText.Format(GetString(IDS_TIMEOUTSIDETIMEFRAME),omAssignStart.Format("%d.%m.%Y %H:%M"),omAssignEnd.Format("%d.%m.%Y %H:%M"),omLoadStart.Format("%d.%m.%Y %H:%M"),omLoadEnd.Format("%d.%m.%Y %H:%M"));
		MessageBox(olText, GetString(IDS_STRING61934), MB_OK);
		return ;
	}

	if(omAssignEnd < omAssignStart)
	{
		CString olText;

		// the assignment begin (%s) must be before the assignment end (%s)
		olText.Format(GetString(IDS_BEGINBEFOREEND),omAssignStart.Format("%d.%m.%Y %H:%M"),omAssignEnd.Format("%d.%m.%Y %H:%M"));
		MessageBox(olText, GetString(IDS_STRING61934), MB_OK);
		return ;
	}

	DEMANDDATA *prlDemand = NULL;
	CMapPtrToPtr olDemMap;
	void *pvlDummy;
	long llDemBound = 0;
	llDemCount = 0;
	for(int ilLc = 0; ilLc < omDemUrnoList.GetSize(); ilLc++)
	{
		prlDemand = ogDemandData.GetDemandByUrno(omDemUrnoList[ilLc]);
		if(IsDemPassFilter(prlDemand) && !olDemMap.Lookup((void *) prlDemand->Urno,(void *&) pvlDummy))
		{
			olDemMap.SetAt((void *) omDemUrnoList[ilLc], prlDemand); // prevent duplicates

			olDemUrno.Format("'%ld'", omDemUrnoList[ilLc]);
			if(olDemList.IsEmpty())
			{
				olDemList = olDemUrno;
			}
			else
			{
				if(llDemBound > 50)
				{
					olDemList += "|";
					llDemBound = 0;
				}
				else
				{
					olDemList += ",";
				}
				olDemList += olDemUrno;
			}
			llDemCount++;
			llDemBound++;
		}
	}


	// in rule editor some demands may be linked to others which should be allocated at the same time
	if(m_LinkedDemands)
	{
		long llDemandUrno = 0L;
		CUIntArray olAdditionalDemands;
		for(POSITION rlPos = olDemMap.GetStartPosition(); rlPos != NULL; )
		{
			olDemMap.GetNextAssoc(rlPos, (void *&)llDemandUrno, (void *&)prlDemand);
			long llOuri = prlDemand->Ouri, llOuro = prlDemand->Ouro;
			if(llOuri == 0L)
			{
				FLIGHTDATA *prlRot = ogFlightData.GetRotationFlight(llOuro);
				if(prlRot != NULL && ogFlightData.IsArrival(prlRot))
				{
					llOuri = prlRot->Urno;
				}
			}
			if(llOuro == 0L)
			{
				FLIGHTDATA *prlRot = ogFlightData.GetRotationFlight(llOuri);
				if(prlRot != NULL && ogFlightData.IsDeparture(prlRot))
				{
					llOuro = prlRot->Urno;
				}
			}
			ogRudData.GetSameEmpDemandUrnos(prlDemand->Urud, llOuri, llOuro, olAdditionalDemands);
		}
		//omDemUrnoList.Append(olAdditionalDemands);

		for(int ilAD = 0; ilAD < olAdditionalDemands.GetSize(); ilAD++)
		{
			if(!olDemMap.Lookup((void *) olAdditionalDemands[ilAD],(void *&) pvlDummy))
			{
				olDemMap.SetAt((void *) olAdditionalDemands[ilAD], NULL); // prevent duplicates

				olDemUrno.Format("'%ld'", olAdditionalDemands[ilAD]);
				if(olDemList.IsEmpty())
				{
					olDemList = olDemUrno;
				}
				else
				{
					if(llDemBound > 50)
					{
						olDemList += "|";
						llDemBound = 0;
					}
					else
					{
						olDemList += ",";
					}
					olDemList += olDemUrno;
				}
				llDemCount++;
				llDemBound++;
			}
		}
	}

	CString olUrno;
	CString olPoolUrnos, olEquUrnos;
	long llUrno = 0L;
	CString olText;
	POSITION rlPos;

	for(rlPos = omSelectedPools.GetStartPosition(); rlPos != NULL; )
	{
		omSelectedPools.GetNextAssoc(rlPos, olText, (void *&) llUrno);
		olUrno.Format("'%ld'", llUrno);
		if(!olPoolUrnos.IsEmpty())
			olPoolUrnos += ",";
		olPoolUrnos += olUrno;
	}

	for(rlPos = omSelectedEquipment.GetStartPosition(); rlPos != NULL; )
	{
		omSelectedEquipment.GetNextAssoc(rlPos, olText, (void *&) llUrno);
		olUrno.Format("'%ld'", llUrno);
		if(!olEquUrnos.IsEmpty())
			olEquUrnos += ",";
		olEquUrnos += olUrno;
	}


	BOOL bOK = omAssignType == "DEL" ? llDemCount > 0 : llDemCount > 0 && (!olPoolUrnos.IsEmpty() || !olEquUrnos.IsEmpty());
	if (bOK)
	{
		CString olTmpFlag;

		olDataList.Format("%ld;",llDemCount);
		olFieldList += "NODEM,";

		olDataList += olDemList;
		olDataList += ";";
		olFieldList += "DEMANDS,";

		if(!olPoolUrnos.IsEmpty())
		{
			olDataList += olPoolUrnos;
			olDataList += ";";
			olFieldList += "URES,";
		}

		olDataList += "POL";
		olDataList += ";";
		olFieldList += "RESTAB,";

		olDataList += "0";
		olDataList += ";";
		olFieldList += "URED,";
	 
		olTmpFlag = (m_AssignWOAloc == TRUE)?"1":"0";
		olDataList += olTmpFlag;
		olDataList += ";";
		olFieldList += "WOALID,";

		olTmpFlag = (m_BreakPrio == TRUE)?"1":"0";
		olDataList += olTmpFlag;
		olDataList += ";";
		olFieldList += CString("IBREAK,");
	
		olDataList += omAssignType;
		olDataList += ";";
		olFieldList += "ATYP,";

		olDataList += m_ShiftStartBuff;
		olDataList += ";";
		olFieldList += "JSB,";

		olDataList += m_ShiftEndBuff;
		olDataList += ";";
		olFieldList += "JEB,";

		olDataList += m_JobBuff;
		olDataList += ";";
		olFieldList += "JMD,";
		
		olDataList += omBreakBuffer;
		olDataList += ";";
		olFieldList += "DBB,";

		CString olVal;
		if(m_Deviation1 > 0)
			olVal.Format("-%d;", m_Deviation1);
		else
			olVal.Format("%d;", m_Deviation1);
		olDataList += olVal;
		olFieldList += "MINGRPDEV,";

		olVal.Format("%d;", m_Deviation2);
		olDataList += olVal;
		olFieldList += "MAXGRPDEV,";

		if(!olEquUrnos.IsEmpty())
		{
			olDataList += olEquUrnos;
			olDataList += ";";
			olFieldList += (bmUseEquTypes) ? "ETYP," : "ESGR,";
		}

		bool blAssign = ogJobData.AutoAssignDemands(olFieldList,olDataList);

		AfxGetApp()->DoWaitCursor(-1);

		if(blAssign)
		{
//			WaitAssignDlg olWaitDlg(this,"Please wait!");			// show the waitdlg
//			if (omAssignType == "DEL")
//				olWaitDlg.SetDeleteCount(llDemCount);
//			else
//				olWaitDlg.SetAssignCount(llDemCount);
//
//			olWaitDlg.DoModal();
//
//			// force conflict check and diagram update
//			pogButtonList->PostMessage(WM_TIMER,0,0L);	
//
//			// "The demands have been successfully assigned and the update of the chart is in progress."
//			MessageBox(GetString(IDS_AUTOASSIGN_OK),GetString(IDS_STRING61934));

			ogJobData.SetAutoAssignInProgress();
		}
		else
		{
			// "Error Assigning the demands"
			CString olErr;
			olErr.Format("%s\n%s",GetString(IDS_AUTOASSIGN_NOTOK),ogJobData.GetLastError());
			MessageBox(olErr,GetString(IDS_STRING61934));
		}
		CDialog::OnOK();
	}
	else if(omAssignType != "DEL")
	{
		if(llDemCount <= 0)
		{
			// "The are no demands to be assigned"
			MessageBox(GetString(IDS_AUTOASSIGN_NODEMS),GetString(IDS_STRING61934));
		}
		else if(olPoolUrnos.IsEmpty() && olEquUrnos.IsEmpty())
		{
			// "There is no employees/equipment to assign"
			if(omPossibleEquipment.GetCount() <= 0)
				MessageBox(GetString(IDS_AUTOASSIGN_NOEMPS),GetString(IDS_STRING61934));
			else if(omPossibleEquipment.GetCount() <= 0)
				MessageBox(GetString(IDS_AUTOASSIGN_NOEQU),GetString(IDS_STRING61934));
			else
				MessageBox(GetString(IDS_AUTOASSIGN_NOEMPSEQU),GetString(IDS_STRING61934));
		}
		else
		{
			CDialog::OnOK();
		}
	}
	else
	{
		CDialog::OnOK();
	}
}

void CAutoAssignDlg::DisableFlightFilter()
{
	bmDisableFlightFilter = true;
}

void CAutoAssignDlg::ReadDefaultsFromCedaIni()
{
	char pclKeyword[100], pclTmpText[100];
	CString olConfigFileName = ogBasicData.GetConfigFileName();

	m_AssignWOAloc = FALSE;
	strcpy(pclKeyword,"INITAUTOASSIGN_WITHOUT_ALLOCUNIT");
	GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(!stricmp(pclTmpText,"YES"))
	{
		m_AssignWOAloc = TRUE;
	}

	m_BreakPrio = FALSE;
	strcpy(pclKeyword,"INITAUTOASSIGN_BREAK_PRIORITY");
	GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(!stricmp(pclTmpText,"YES"))
	{
		m_BreakPrio = TRUE;
	}

	m_LinkedDemands = FALSE;
	strcpy(pclKeyword,"INITAUTOASSIGN_LINKEDDEMANDS");
	GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(!stricmp(pclTmpText,"YES"))
	{
		m_LinkedDemands = TRUE;
	}

	m_JobBuff = "0";
	strcpy(pclKeyword,"INITAUTOASSIGN_JOB_BUFFER");
	GetPrivateProfileString(pcgAppName, pclKeyword, "0",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(atoi(pclTmpText) > 0)
	{
		m_JobBuff = pclTmpText;
	}

	m_ShiftStartBuff = "0";
	strcpy(pclKeyword,"INITAUTOASSIGN_SHIFTSTART_BUFFER");
	GetPrivateProfileString(pcgAppName, pclKeyword, "0",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(atoi(pclTmpText) > 0)
	{
		m_ShiftStartBuff = pclTmpText;
	}

	m_ShiftEndBuff = "0";
	strcpy(pclKeyword,"INITAUTOASSIGN_SHIFTEND_BUFFER");
	GetPrivateProfileString(pcgAppName, pclKeyword, "0",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(atoi(pclTmpText) > 0)
	{
		m_ShiftEndBuff = pclTmpText;
	}
}

void CAutoAssignDlg::OnSelChangeEmpEquTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	switch(m_EmpEquTabCtrl.GetCurSel())
	{
	case 0:	// emp selected
		bmPersonnelTab = true;
		SetDemandTypeFields();
		break;
	case 1: // equ selected
		bmPersonnelTab = false;
		SetDemandTypeFields();
		break;
	default:
		break;
	}
	
	*pResult = 0;
}

void CAutoAssignDlg::OnEquipmentCheckbox() 
{
	UpdateData(TRUE);
	if(m_AssignPersonnel)
		bmPersonnelTab = true;
	else if(m_AssignEquipment)
		bmPersonnelTab = false;
	else
		bmPersonnelTab = true;
	SetDemandTypeFields();
}

void CAutoAssignDlg::OnPersonnelCheckbox() 
{
	UpdateData(TRUE);
	if(m_AssignPersonnel)
		bmPersonnelTab = true;
	else if(m_AssignEquipment)
		bmPersonnelTab = false;
	else
		bmPersonnelTab = true;
	SetDemandTypeFields();
}
