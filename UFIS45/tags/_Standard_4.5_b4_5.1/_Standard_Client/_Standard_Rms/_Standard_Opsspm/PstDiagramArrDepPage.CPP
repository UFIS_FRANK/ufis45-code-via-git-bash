// PstDiagramArrDepPage.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <PstDiagramArrDepPage.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PstDiagramArrDepPage property page

IMPLEMENT_DYNCREATE(PstDiagramArrDepPage, CPropertyPage)

PstDiagramArrDepPage::PstDiagramArrDepPage() : CPropertyPage(PstDiagramArrDepPage::IDD)
{
	//{{AFX_DATA_INIT(PstDiagramArrDepPage)
	bmArrCheckBox = FALSE;
	bmDepCheckBox = FALSE;
	bmTurnaroundCheckBox = FALSE;
	m_DisplayJobConflicts = FALSE;
	m_OneFlightPerLine = FALSE;
	//}}AFX_DATA_INIT

	lmStoaColour = SILVER;
	lmEtoaColour = WHITE;
	lmTmoaColour = YELLOW;
	lmLandColour = GREEN;
	lmOnblColour = LIME;
	lmStodColour = SILVER;
	lmEtodColour = WHITE;
	lmSlotColour = YELLOW;
	lmOfblColour = LIME;
	lmAirbColour = GREEN;

	omTitle = GetString(IDS_PSTARRDEP_TITLE);

	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

PstDiagramArrDepPage::~PstDiagramArrDepPage()
{
}

void PstDiagramArrDepPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PstDiagramArrDepPage)
	DDX_Check(pDX, IDC_ARRIVAL_CHECKBOX, bmArrCheckBox);
	DDX_Check(pDX, IDC_DEPARTURE_CHECKBOX, bmDepCheckBox);
	DDX_Check(pDX, IDC_TURNAROUND_CHECKBOX, bmTurnaroundCheckBox);
	DDX_Control(pDX, IDC_STACOLOUR, omStoaButton);
	DDX_Control(pDX, IDC_ETACOLOUR, omEtoaButton);
	DDX_Control(pDX, IDC_TMOCOLOUR, omTmoaButton);
	DDX_Control(pDX, IDC_LANDCOLOUR, omLandButton);
	DDX_Control(pDX, IDC_ONBLCOLOUR, omOnblButton);
	DDX_Control(pDX, IDC_STDCOLOUR, omStodButton);
	DDX_Control(pDX, IDC_ETDCOLOUR, omEtodButton);
	DDX_Control(pDX, IDC_SLOTCOLOUR, omSlotButton);
	DDX_Control(pDX, IDC_OFBLCOLOUR, omOfblButton);
	DDX_Control(pDX, IDC_AIRBCOLOUR, omAirbButton);
	DDX_Check(pDX, IDC_DISPLAYJOBCONFLICTS, m_DisplayJobConflicts);
	DDX_Check(pDX, IDC_ONEFLIGHTPERLINE, m_OneFlightPerLine);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
	}
}


BEGIN_MESSAGE_MAP(PstDiagramArrDepPage, CPropertyPage)
	//{{AFX_MSG_MAP(PstDiagramArrDepPage)
	ON_BN_CLICKED(IDC_STACOLOUR, OnStoaColour)
	ON_BN_CLICKED(IDC_ETACOLOUR, OnEtoaColour)
	ON_BN_CLICKED(IDC_TMOCOLOUR, OnTmoaColour)
	ON_BN_CLICKED(IDC_LANDCOLOUR, OnLandColour)
	ON_BN_CLICKED(IDC_ONBLCOLOUR, OnOnblColour)
	ON_BN_CLICKED(IDC_STDCOLOUR, OnStodColour)
	ON_BN_CLICKED(IDC_ETDCOLOUR, OnEtodColour)
	ON_BN_CLICKED(IDC_SLOTCOLOUR, OnSlotColour)
	ON_BN_CLICKED(IDC_OFBLCOLOUR, OnOfblColour)
	ON_BN_CLICKED(IDC_AIRBCOLOUR, OnAirbColour)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PstDiagramArrDepPage message handlers

BOOL PstDiagramArrDepPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

BOOL PstDiagramArrDepPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	CWnd *polWnd = GetDlgItem(IDC_STOATITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_STOA));
	}
	polWnd = GetDlgItem(IDC_ETOATITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_ETOA));
	}
	polWnd = GetDlgItem(IDC_TMOATITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_TMOA));
	}
	polWnd = GetDlgItem(IDC_LANDTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_LAND));
	}
	polWnd = GetDlgItem(IDC_ONBLTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_ONBL));
	}
	polWnd = GetDlgItem(IDC_STODTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_STOD));
	}
	polWnd = GetDlgItem(IDC_ETODTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_ETOD));
	}
	
	polWnd = GetDlgItem(IDC_SLOTTITLE); 
    if(!IsPrivateProfileOn("SHOW_PST_SLOT_MARKER","NO"))
	{
		polWnd->ShowWindow(SW_HIDE);
		omSlotButton.ShowWindow(SW_HIDE);
	}
	else if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_SLOT));
	}
	

	polWnd = GetDlgItem(IDC_OFBLTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_OFBL));
	}
	polWnd = GetDlgItem(IDC_AIRBTITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_AIRB));
	}

	polWnd = GetDlgItem(IDC_ARRIVAL_CHECKBOX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_ARRIVAL));
	}
	polWnd = GetDlgItem(IDC_DEPARTURE_CHECKBOX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_DEPARTURE));
	}
	polWnd = GetDlgItem(IDC_TURNAROUND_CHECKBOX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_TURNAROUND));
	}

	SetColours();

	polWnd = GetDlgItem(IDC_TITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_PSTARRDEP_INTRO));
	}

	// Display Job Conflicts on Flight Bars
	polWnd = GetDlgItem(IDC_DISPLAYJOBCONFLICTS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_DISPLAYJOBCONFLICTS));
	}

	// Display One Flight Per Line
	polWnd = GetDlgItem(IDC_ONEFLIGHTPERLINE); 
	if(polWnd != NULL)
	{
		polWnd->ShowWindow(SW_HIDE);
		polWnd->SetWindowText(GetString(IDS_ONEFLIGHTPERLINE));
		char pclTmpText[512];
		char pclConfigPath[512];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(pcgAppName, "ONEFLIGHTPERLINE",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
		if (!strcmp(pclTmpText,"YES")) {
			polWnd->ShowWindow(SW_SHOW);
		}
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PstDiagramArrDepPage::SetColours()
{
	SetColour(omStoaButton, lmStoaColour);
	SetColour(omEtoaButton, lmEtoaColour);
	SetColour(omTmoaButton, lmTmoaColour);
	SetColour(omLandButton, lmLandColour);
	SetColour(omOnblButton, lmOnblColour);
	SetColour(omStodButton, lmStodColour);
	SetColour(omEtodButton, lmEtodColour);
	SetColour(omSlotButton, lmSlotColour);
	SetColour(omOfblButton, lmOfblColour);
	SetColour(omAirbButton, lmAirbColour);
}

void PstDiagramArrDepPage::SetColour(CCSButtonCtrl &ropButton, COLORREF lpColour)
{
	if (m_hWnd != NULL)
	{
		ropButton.SetColors(lpColour,lpColour,lpColour);
	}
}

void PstDiagramArrDepPage::OnStoaColour() 
{
	ChangeColour(omStoaButton, lmStoaColour);
}

void PstDiagramArrDepPage::OnEtoaColour() 
{
	ChangeColour(omEtoaButton, lmEtoaColour);
}

void PstDiagramArrDepPage::OnTmoaColour() 
{
	ChangeColour(omTmoaButton, lmTmoaColour);
}

void PstDiagramArrDepPage::OnOnblColour() 
{
	ChangeColour(omOnblButton, lmOnblColour);
}

void PstDiagramArrDepPage::OnLandColour() 
{
	ChangeColour(omLandButton, lmLandColour);
}

void PstDiagramArrDepPage::OnStodColour() 
{
	ChangeColour(omStodButton, lmStodColour);
}

void PstDiagramArrDepPage::OnEtodColour() 
{
	ChangeColour(omEtodButton, lmEtodColour);
}
void PstDiagramArrDepPage::OnSlotColour() 
{
	ChangeColour(omSlotButton, lmSlotColour);
}
void PstDiagramArrDepPage::OnOfblColour() 
{
	ChangeColour(omOfblButton, lmOfblColour);
}

void PstDiagramArrDepPage::OnAirbColour() 
{
	ChangeColour(omAirbButton, lmAirbColour);
}

void PstDiagramArrDepPage::ChangeColour(CCSButtonCtrl &ropButton, COLORREF &ropColour)
{
	CColorDialog olColorDlg(ropColour, 0 , this);

	if (olColorDlg.DoModal() == IDOK)
	{
		ropColour = olColorDlg.GetColor();
		SetColour(ropButton, ropColour);
		ropButton.Invalidate(TRUE);
	}
}
