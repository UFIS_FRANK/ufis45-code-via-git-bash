#ifndef __OtherColoursViewer_H__
#define __OtherColoursViewer_H__

#include <CCSPtrArray.h>
#include <CViewer.h>
#include <CCSTable.h>

struct OTHERCOLOURS_LINE
{
	CString		Name;
	COLORREF	Colour;
	bool		Enabled;

	COLORREF	PrevColour;
	bool		PrevEnabled;

	bool		TeamLeaderLine;

	OTHERCOLOURS_LINE(void)
	{
		Colour = SILVER;
		Enabled = false;
		PrevColour = SILVER;
		PrevEnabled = false;
	}
};


struct OTHERCOLOURS_FIELD
{
	CString	Field;
	CString	Name;
	int		Length;
	int		PrintLength;

	OTHERCOLOURS_FIELD(const char *pcpField,const char *pcpName,int ipLength,int ipPrintLength)
	{
		Field = pcpField;
		Name  = pcpName;
		Length= ipLength;
		PrintLength = ipPrintLength; 
	};

	OTHERCOLOURS_FIELD()
	{
		Length		= 0;
		PrintLength = 0;
	};
};


class	CCSTable;
struct	TABLE_COLUMN;

class OtherColoursViewer: public CViewer
{
public:
	OtherColoursViewer();
    ~OtherColoursViewer();

	void Attach(CCSTable *popTable);
	void ChangeViewTo();
	void DeleteAll();
	int  CompareColourName(OTHERCOLOURS_LINE *prpLine1, OTHERCOLOURS_LINE *prpLine2);
//	BOOL IsPassFilter(PFCDATA *prpPfc);
	void MakeLines();
	void MakeLine(OTHERCOLOURS_LINE *prpPfc);
//	void MakeLineForTeamLeader(void);
	void MakeLineData(OTHERCOLOURS_LINE *prpPfc, OTHERCOLOURS_LINE &ropLine);
	int  CreateLine(OTHERCOLOURS_LINE *prpLine);
	void DeleteLine(int ipLineno);
	void UpdateDisplay(void);
	void UpdateTableLine(int ipLine);
	void Format(OTHERCOLOURS_LINE *prpLine, CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	bool SetColumn(const char *pcpFieldName, const char *pcpFieldValue, CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	bool EvaluateTableFields(void);
	int  TableFieldIndex(const CString& ropField);
	int  LineCount() const;
	OTHERCOLOURS_LINE *GetLine(int ipLineNo);
	bool bmDisplayDefinedOnly;
	void SaveLineData(void);

private:
    CCSTable			*pomTable;
  
	CCSPtrArray <OTHERCOLOURS_LINE> omLines;
	CCSPtrArray <OTHERCOLOURS_FIELD> omTableFields;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeader;
	CCSPtrArray <TABLE_COLUMN> omColumns;
	
	

	void CreateTableColumnsAndHeader();
	void GetDefaultColoursConfig(CCSPtrArray <OTHERCOLOURS_LINE> &ropColours);
};

#endif //__BREAKTABLEANDTABLEVIEWER_H__