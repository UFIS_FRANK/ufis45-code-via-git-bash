#if !defined(AFX_OTHERSETUPDLG_H__BBEB88E2_94E6_405D_AFA5_9C2F504DB13A__INCLUDED_)
#define AFX_OTHERSETUPDLG_H__BBEB88E2_94E6_405D_AFA5_9C2F504DB13A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OtherSetupDlg.h : header file
//
#include <CCSButtonCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// COtherSetupDlg dialog

class COtherSetupDlg : public CDialog
{
// Construction
public:
	COtherSetupDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COtherSetupDlg)
	enum { IDD = IDD_OTHERSETUPDLG };
	CStatic	m_FlightJobsExcelTitle2;
	CButton	m_FlightJobsExcelTitle1Ctrl;
	CStatic	m_DiffFuncTextCtrl;
	CCSButtonCtrl m_DiffFuncColourCtrl;
	CButton	m_DiffFuncCtrl;
	BOOL	m_DisplayDiffFuncSymbol;
	CString	m_FlightJobsExcelFunctions;
	CStatic m_FlightJobsExcelTitle2Ctrl;
	CEdit	m_FlightJobsExcelFunctionsCtrl;
	int		imBreakBuffer;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COtherSetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COtherSetupDlg)
	afx_msg void OnSetDiffFuncColour();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void ChangeColour(CCSButtonCtrl &ropButton, COLORREF &ropColour);
	COLORREF lmDiffFuncColour;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OTHERSETUPDLG_H__BBEB88E2_94E6_405D_AFA5_9C2F504DB13A__INCLUDED_)
