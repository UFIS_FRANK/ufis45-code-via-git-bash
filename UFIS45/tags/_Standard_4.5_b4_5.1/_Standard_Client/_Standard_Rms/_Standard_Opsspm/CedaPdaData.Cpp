// CedaPdaData.cpp
 
#include <stdafx.h>
#include <CedaPdaData.h>
#include <opsspm.h>
#include <resource.h>


void ProcessPdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPdaData::CedaPdaData() : CCSCedaData()
{
    BEGIN_CEDARECINFO(PDADATA, PdaDataRecInfo)
		FIELD_CHAR_TRIM	(Pdid,"PDID")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_DATE		(Nafr,"NAFR")
		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Ipad,"IPAD")
		FIELD_LONG		(Ustf,"USTF")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PdaDataRecInfo)/sizeof(PdaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PdaDataRecInfo[i],sizeof(CEDARECINFO)); 
        omRecInfo.Add(prpCedaRecInfo); 
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"PDA");
    sprintf(pcmListOfFields,"PDID,CDAT,LSTU,URNO,USEC,USEU,VAFR,VATO,NAFR,NATO,IPAD,USTF");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();

	Register();
}

//----------------------------------------------------------------------------------------------------


//--REGISTER----------------------------------------------------------------------------------------------

void CedaPdaData::Register(void)
{
	// lli: the correct broadcast object to use is ogCCSDdx
	ogCCSDdx.Register((void *)this,BC_PDA_CHANGE,	CString("PDADATA"), CString("Pda-changed"),	ProcessPdaCf);
	ogCCSDdx.Register((void *)this,BC_PDA_NEW,		CString("PDADATA"), CString("Pda-new"),		ProcessPdaCf);
	ogCCSDdx.Register((void *)this,BC_PDA_DELETE,	CString("PDADATA"), CString("Pda-deleted"),	ProcessPdaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPdaData::~CedaPdaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPdaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omUstfMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogCCSDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPdaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omUstfMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PDADATA *prlPda = new PDADATA;
		if ((ilRc = GetFirstBufferRecord(prlPda)) == true)
		{
			omData.Add(prlPda);//Update omData
			omUrnoMap.SetAt((void *)prlPda->Urno,prlPda);
			if (prlPda->Ustf != 0L)
			{
				omUstfMap.SetAt((void *)prlPda->Ustf,prlPda);
			}
		}
		else
		{
			delete prlPda;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPdaData::Insert(PDADATA *prpPda)
{
	prpPda->IsChanged = DATA_NEW;
	if(Save(prpPda) == false) return false; //Update Database
	InsertInternal(prpPda);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPdaData::InsertInternal(PDADATA *prpPda)
{
	ogCCSDdx.DataChanged((void *)this, PDA_NEW,(void *)prpPda ); //Update Viewer
	omData.Add(prpPda);//Update omData
	omUrnoMap.SetAt((void *)prpPda->Urno,prpPda);
	if (prpPda->Ustf != 0L)
	{
		omUstfMap.SetAt((void *)prpPda->Ustf,prpPda);
	}
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPdaData::Delete(long lpUrno)
{
	PDADATA *prlPda = GetPdaByUrno(lpUrno);
	if (prlPda != NULL)
	{
		prlPda->IsChanged = DATA_DELETED;
		if(Save(prlPda) == false) return false; //Update Database
		DeleteInternal(prlPda);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPdaData::DeleteInternal(PDADATA *prpPda)
{
	ogCCSDdx.DataChanged((void *)this,PDA_DELETE,(void *)prpPda); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPda->Urno);
	if (prpPda->Ustf != 0L)
	{
		omUstfMap.RemoveKey((void *)prpPda->Ustf);
		// lli: when PDA is deleted, send internal PRM_JOB_CHANGE broadcast for jobs related to the PDA
		CCSPtrArray <JOBDATA> olPoolJobs;
		ogJobData.GetJobsByUstf(olPoolJobs,prpPda->Ustf,true);
		int ilNumPoolJobs = olPoolJobs.GetSize();
		for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			ogCCSDdx.DataChanged((void *)this,PRM_JOB_CHANGE,(void *)&olPoolJobs[ilPoolJob]); //Update Viewer
		}
	}
	int ilPdaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPdaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPda->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPdaData::Update(PDADATA *prpPda)
{
	if (GetPdaByUrno(prpPda->Urno) != NULL)
	{
		if (prpPda->IsChanged == DATA_UNCHANGED)
		{
			prpPda->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPda) == false) return false; //Update Database
		UpdateInternal(prpPda);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPdaData::UpdateInternal(PDADATA *prpPda)
{
	PDADATA *prlPda = GetPdaByUrno(prpPda->Urno);
	if (prlPda != NULL)
	{
		if (prlPda->Ustf != 0L)
		{
			omUstfMap.RemoveKey((void *)prlPda->Ustf);
			// lli: when there is an PDA update, send internal job change broadcast message for jobs related the the PDA
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByUstf(olPoolJobs,prlPda->Ustf,true);
			int ilNumPoolJobs = olPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				ogCCSDdx.DataChanged((void *)this,PRM_JOB_CHANGE,(void *)&olPoolJobs[ilPoolJob]); //Update Viewer
			}
		}
		*prlPda = *prpPda; //Update omData
		if (prlPda->Ustf != 0L)
		{
			omUstfMap.SetAt((void *)prlPda->Ustf,prlPda);
		}
		ogCCSDdx.DataChanged((void *)this,PDA_CHANGE,(void *)prlPda); //Update Viewer
	}
    return true;
}


bool CedaPdaData::RemoveFromStaff(long lpUstf)
{
	PDADATA *prlPda = GetPdaByUstf(lpUstf);
	if (prlPda != NULL)
	{
		if (prlPda->Ustf != 0L)
		{
			omUstfMap.RemoveKey((void *)prlPda->Ustf);
			// lli: send internal broadcast to update the Staff Chart
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByUstf(olPoolJobs,prlPda->Ustf,true);
			int ilNumPoolJobs = olPoolJobs.GetSize();
			for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
			{
				ogCCSDdx.DataChanged((void *)this,PRM_JOB_CHANGE,(void *)&olPoolJobs[ilPoolJob]); //Update Viewer
			}
			prlPda->Ustf = 0L;
			prlPda->IsChanged = DATA_CHANGED;
			return (Save(prlPda));
		}
	}
    return false;
}	

bool CedaPdaData::RemoveFromAllPda(long lpUstf)
{

	//CString olListOfData;
	char pclSelection[124];
	char pclData[124] = "";
	char *pclTmpFieldList;

	sprintf(pclData,"%0d",0L);
	pclTmpFieldList = pcmFieldList;
	pcmFieldList = "USTF";

	sprintf(pclSelection,"WHERE USTF = %ld",lpUstf);
	CedaAction("URT",pclSelection,"",pclData);
	pcmFieldList = pclTmpFieldList;

	return false;
}	

//--GET-BY-USTF--------------------------------------------------------------------------------------------

PDADATA *CedaPdaData::GetPdaByUstf(long lpUstf)
{
	PDADATA  *prlPda;
	if (omUstfMap.Lookup((void *)lpUstf,(void *& )prlPda) == TRUE)
	{
		return prlPda;
	}
	return NULL;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PDADATA *CedaPdaData::GetPdaByUrno(long lpUrno)
{
	PDADATA  *prlPda;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPda) == TRUE)
	{
		return prlPda;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

void CedaPdaData::GetAllPdas(CCSPtrArray<PDADATA> &ropPdas)
{
	int ilNumPdas = omData.GetSize();
	for(int ilPda = 0; ilPda < ilNumPdas; ilPda++)
	{
		ropPdas.Add(&omData[ilPda]);
	}
}


//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPdaData::ReadSpecialData(CCSPtrArray<PDADATA> *popPda,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","Pda",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","Pda",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPda != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PDADATA *prpPda = new PDADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPda,CString(pclFieldList))) == true)
			{
				popPda->Add(prpPda);
			}
			else
			{
				delete prpPda;
			}
		}
		if(popPda->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPdaData::Save(PDADATA *prpPda)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPda->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPda->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPda->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPda->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}


//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPdaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPdaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPdaData;
	prlPdaData = (struct BcStruct *) vpDataPointer;
	PDADATA *prlPda;
	if(ipDDXType == BC_PDA_NEW)
	{
		prlPda = new PDADATA;
		GetRecordFromItemList(prlPda,prlPdaData->Fields,prlPdaData->Data);
		InsertInternal(prlPda);
	}
	if(ipDDXType == BC_PDA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPdaData->Selection);
		prlPda = GetPdaByUrno(llUrno);
		if(prlPda != NULL)
		{
			prlPda = new PDADATA;
			GetRecordFromItemList(prlPda,prlPdaData->Fields,prlPdaData->Data);
			UpdateInternal(prlPda);
		}
	}
	if(ipDDXType == BC_PDA_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlPdaData->Selection);

		prlPda = GetPdaByUrno(llUrno);
		if (prlPda != NULL)
		{
			DeleteInternal(prlPda);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
