// CedaCccData.cpp - Class for Airline Types
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaCccData.h>
#include <BasicData.h>

CedaCccData::CedaCccData()
{                  
    BEGIN_CEDARECINFO(CCCDATA, CccDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Cicc,"CICC")
		FIELD_CHAR_TRIM(Cicn,"CICN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(CccDataRecInfo)/sizeof(CccDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CccDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"CCCTAB");
    pcmFieldList = "URNO,CICC,CICN";
}
 
CedaCccData::~CedaCccData()
{
	TRACE("CedaCccData::~CedaCccData called\n");
	ClearAll();
}

void CedaCccData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omCicnMap.RemoveAll();
	omCiccMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaCccData::ReadCccData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];

	strcpy(pclWhere, "");

    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaCccData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		CCCDATA rlCccData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlCccData)) == RCSuccess)
			{
				AddCccInternal(rlCccData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaCccData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(CCCDATA), pclWhere);
    return ilRc;
}


void CedaCccData::AddCccInternal(CCCDATA &rrpCcc)
{
	CCCDATA *prlCcc = new CCCDATA;
	*prlCcc = rrpCcc;
	omData.Add(prlCcc);
	omUrnoMap.SetAt((void *)prlCcc->Urno,prlCcc);
	omCiccMap.SetAt(prlCcc->Cicc,prlCcc);
	omCicnMap.SetAt(prlCcc->Cicn,prlCcc);

}


CCCDATA* CedaCccData::GetCccByUrno(long lpUrno)
{
	CCCDATA *prlCcc = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlCcc);
	return prlCcc;
}

CCCDATA* CedaCccData::GetCccByClass(const CString& ropClass)
{
	CCCDATA *prlCcc = NULL;
	omCiccMap.Lookup(ropClass, (void *&) prlCcc);
	return prlCcc;
}

CCCDATA* CedaCccData::GetCccByName(const CString& ropName)
{
	CCCDATA *prlCcc = NULL;
	omCicnMap.Lookup(ropName, (void *&) prlCcc);
	return prlCcc;
}

CString CedaCccData::GetTableName(void)
{
	return CString(pcmTableName);
}

int CedaCccData::GetCountOfRecords()
{
	return omData.GetSize();
}

int	CedaCccData::GetClassNameList(CStringArray& ropNames)
{
	for (int i = 0;i < omData.GetSize(); i++)
	{
		ropNames.Add(omData[i].Cicn);		
	}

	return omData.GetSize();
}

int	CedaCccData::GetClassList(CStringArray& ropClasses)
{
	for (int i = 0;i < omData.GetSize(); i++)
	{
		ropClasses.Add(omData[i].Cicc);		
	}

	return omData.GetSize();
}

bool CedaCccData::GetClassNameList(const CStringArray& ropClasses,CStringArray& ropNames)
{
	for (int i = 0; i < ropClasses.GetSize(); i++)
	{
		if (ropClasses[i] == GetString(IDS_ALLSTRING))
			ropNames.Add(GetString(IDS_ALLSTRING));
		else if (ropClasses[i] == GetString(IDS_UNDEFINED))
			ropNames.Add(GetString(IDS_UNDEFINED));
		else
		{
			CCCDATA *prlCcc = NULL;
			if (!omCiccMap.Lookup(ropClasses[i],(void *&)prlCcc) || !prlCcc)
				return false;
			ropNames.Add(prlCcc->Cicn);
		}
	}

	return true;
}

bool CedaCccData::GetClassList(const CStringArray& ropNames,CStringArray& ropClasses)
{
	for (int i = 0; i < ropNames.GetSize(); i++)
	{
		if (ropNames[i] == GetString(IDS_ALLSTRING))
			ropClasses.Add(GetString(IDS_ALLSTRING));
		else if (ropNames[i] == GetString(IDS_UNDEFINED))
			ropClasses.Add(GetString(IDS_UNDEFINED));
		else
		{
			CCCDATA *prlCcc = NULL;
			if (!omCicnMap.Lookup(ropNames[i],(void *&)prlCcc) || !prlCcc)
				return false;
			ropClasses.Add(prlCcc->Cicc);
		}
	}

	return true;
}
