// Class for Airline Types
#ifndef _CEDAALTDATA_H_
#define _CEDAALTDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

#define ALT_BANK_CUSTOMER 'K'

struct AltDataStruct
{
	long	Urno;		// Unique Record Number
	char	Alc2[3];	// 2 letter airline code
	char	Alc3[4];	// 3 letter airline code
	char	Cash[2];	// Method of Payment K = Bank, B = Cash else Unknown
	// char	Filt[8];    // Filter for Handling Agent

	AltDataStruct(void)
	{
		Urno = 0L;
		strcpy(Alc2,"");
		strcpy(Alc3,"");
		strcpy(Cash,"");
		//strcpy(Filt,"");
	}
};

typedef struct AltDataStruct ALTDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaAltData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <ALTDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omAlc2Map;
	CMapStringToPtr omAlc3Map;
	CString GetTableName(void);

// Operations
public:
	CedaAltData();
	~CedaAltData();

	CCSReturnCode ReadAltData();
	ALTDATA* GetAltByUrno(long lpUrno);

	ALTDATA* GetAltByAlc2(const char *pcpAlc2);
	ALTDATA* GetAltByAlc3(const char *pcpAlc3);
	CString FormatAlcString(const char *pcpAlc2, const char *pcpAlc3);
	CString FormatAlcString(ALTDATA *prpAlt);
	int GetAlc2Alc3Strings(CStringArray &ropAlc2Alc3Strings);
	bool IsCashCustomer(const char *pcpAlt);
	void GetAirlineList(char *pcpHsna, char *pcpList);
private:
	void AddAltInternal(ALTDATA &rrpAlt);
	void ClearAll();
};


extern CedaAltData ogAltData;
#endif _CEDAALTDATA_H_
