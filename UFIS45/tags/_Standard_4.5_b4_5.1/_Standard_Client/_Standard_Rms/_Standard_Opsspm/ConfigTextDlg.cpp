// ConfigTextDlg.cpp : implementation file
//

#include "stdafx.h"
#include "opsspm.h"
#include "ConfigTextDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigTextDlg dialog


CConfigTextDlg::CConfigTextDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigTextDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConfigTextDlg)
	//}}AFX_DATA_INIT
	bmDeleteOperation = false;
}

CConfigTextDlg::~CConfigTextDlg(void)
{
	DeleteFields();
}

void CConfigTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigTextDlg)
	DDX_Control(pDX, IDC_FIELDLIST, m_FieldCombo);
	DDX_Control(pDX, IDC_RICHEDIT1, m_ResultsList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigTextDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigTextDlg)
	ON_BN_CLICKED(IDC_RESET, OnReset)
	ON_BN_CLICKED(IDC_ADDFIELD, OnAddField)
	ON_NOTIFY(EN_PROTECTED, IDC_RICHEDIT1, OnProtectedResultsList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigTextDlg message handlers

BOOL CConfigTextDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// enable notification that a protected field is being changed
	//m_ResultsList.SetEventMask(m_ResultsList.GetEventMask() | EN_PROTECTED); // for some reason this doesn't work!
	m_ResultsList.SendNotifyMessage(EM_SETEVENTMASK, 0, ENM_PROTECTED); 

	// used to format the fields added to the text string so that they are displayed in blue and are protected
	rmFieldFormat.dwMask = CFM_PROTECTED|CFM_COLOR|CFM_BOLD;
	rmFieldFormat.dwEffects = CFE_PROTECTED|CFE_BOLD;
	rmFieldFormat.crTextColor = RGB(0,0,255);
	rmFieldFormat.cbSize = sizeof(CHARFORMAT);

	int ilNumFields = omFields.GetSize();
	for(int ilF = 0; ilF < ilNumFields; ilF++)
	{
		FIELD *prlField = &omFields[ilF];
		int ilNewLine = m_FieldCombo.AddString(prlField->Text);
		m_FieldCombo.SetItemDataPtr(ilNewLine, prlField);
	}
	if(ilNumFields > 0)
	{
		m_FieldCombo.SetCurSel(0);
	}
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigTextDlg::DeleteFields(void)
{
	omFields.DeleteAll();
}

void CConfigTextDlg::SetField(CString opText)
{
	long llStart, llEnd;
	m_ResultsList.GetSel(llStart, llEnd);

	CString olText = opText + CString(" ");
	m_ResultsList.ReplaceSel(olText);
	llEnd = llStart + olText.GetLength();
	m_ResultsList.SetSel(llStart, llEnd-1);
	m_ResultsList.SetSelectionCharFormat(rmFieldFormat);

	m_ResultsList.SetSel(llEnd,llEnd);
}

void CConfigTextDlg::OnProtectedResultsList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	ENPROTECTED *pEnProtected = reinterpret_cast<ENPROTECTED *>(pNMHDR);

	if(bmDeleteOperation)
	{
		// Returns zero to allow the operation or a nonzero value to prevent it.
		*pResult = 0;
		bmDeleteOperation = false;
	}
	else
	{
		MessageBeep((UINT)-1);
		*pResult = 1;
	}
}

void CConfigTextDlg::OnReset() 
{
	bmDeleteOperation = true;
	m_ResultsList.SetWindowText("");
	m_ResultsList.SetFocus();
}

void CConfigTextDlg::OnAddField() 
{
	CString olText;

	m_ResultsList.GetWindowText(olText);
	int ilLine = m_FieldCombo.GetCurSel();
	if(ilLine != CB_ERR)
	{
		FIELD *prlField = (FIELD *) m_FieldCombo.GetItemDataPtr(ilLine);
		if(prlField != NULL)
		{
			SetField(prlField->Text);
		}
	}
	m_ResultsList.SetFocus();
}

void CConfigTextDlg::OnOK() 
{
	CreateKeylist();
	CDialog::OnOK();
}

void CConfigTextDlg::CreateKeylist()
{
	CString olText;

	CString olField;

	m_ResultsList.GetWindowText(olText);
	int ilLen = olText.GetLength();
	for(int ilC = 0; ilC < ilLen; ilC++)
	{
		if(IsField(ilC, ilC+1))
		{
			olField += olText[ilC];
		}
		else
		{
			if(!olField.IsEmpty())
			{
				omKeys.Add(olField);
				olField.Empty();

				omResult += "%s";
			}
			omResult += olText[ilC];
		}
	}

	if(!olField.IsEmpty())
	{
		omKeys.Add(olField);
		olField.Empty();

		omResult += "%s";
	}
}

bool CConfigTextDlg::IsField(long lpFrom, long lpTo)
{
	bool blIsField = false;

	long llOldFrom, llOldTo;
	m_ResultsList.GetSel(llOldFrom, llOldTo);

	m_ResultsList.SetSel(lpFrom, lpTo);

	CHARFORMAT rlFormat;
	m_ResultsList.GetSelectionCharFormat(rlFormat);
	if((rlFormat.dwMask & CFM_BOLD) == CFM_BOLD && (rlFormat.dwEffects & CFE_BOLD) == CFE_BOLD)
	{
		blIsField = true;
	}

	m_ResultsList.SetSel(llOldFrom, llOldTo);

	return blIsField;
}

//////////////////////////////////////////////////////////////////////
// User operations

void CConfigTextDlg::SetTitle(CString opTitle)
{
	SetWindowText(opTitle);
}

void CConfigTextDlg::AddField(CString opText, CString opKey)
{
	FIELD *prlField = new FIELD;
	prlField->Text = opText;
	prlField->Key = opKey;
	omFields.Add(prlField);
}

// ropText contains a string "TEXT %s MORETEXT %s"
// ropKeys contains keys to access the values that will be written to the '%s'
void CConfigTextDlg::GetResult(CString &ropResult, CStringArray &ropKeys)
{
}
