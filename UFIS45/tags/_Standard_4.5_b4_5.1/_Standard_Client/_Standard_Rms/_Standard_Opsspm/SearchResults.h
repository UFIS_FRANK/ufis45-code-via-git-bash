#if !defined(AFX_SEARCHRESULTS_H__C799A543_F552_11D5_810E_00010215BFE5__INCLUDED_)
#define AFX_SEARCHRESULTS_H__C799A543_F552_11D5_810E_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SearchResults.h : header file
//
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// CSearchResults dialog


struct MatchedData {
	int			Groupno;
	int			Lineno;
	CString		Text;
};
typedef struct MatchedData MATCHEDDATA;



class CSearchResults : public CDialog
{
// Construction

public:
	CSearchResults(CWnd* pParent = NULL);   // standard constructor
    virtual ~CSearchResults();

// Dialog Data
	//{{AFX_DATA(CSearchResults)
	enum { IDD = IDD_SEARCHRESULTSDLG };
	CListBox	m_SearchResultListCtrl;
	CStatic	m_TitleFieldCtrl;
	//}}AFX_DATA

	CCSPtrArray <MATCHEDDATA> omData;
	CString omSearchString;
	MATCHEDDATA *prmMatch;
	void Add(MATCHEDDATA *prpMatch);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchResults)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSearchResults)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkSearchresultslist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHRESULTS_H__C799A543_F552_11D5_810E_00010215BFE5__INCLUDED_)
