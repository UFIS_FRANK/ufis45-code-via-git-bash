// FreeEmpViewer.cpp : implementation file
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							On this change, special flight job was changed from type
//							FLT to FLS. The field PRID which contains a word "SPECIAL"
//							is not used any more. The field FLUR which was used for
//							storing shift URNO now moved to the field SHUR.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <ccsddx.h>
#include <FreeEmpViewer.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void FreeStaffTableTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);


/////////////////////////////////////////////////////////////////////////////
// FreeEmpViewer
//

FreeEmpViewer::FreeEmpViewer(CCSPtrArray<FREEEMPS_LINEDATA> *popFreeEmps)
{
	pomFreeEmps = NULL;
	if(popFreeEmps != NULL)
	{
		pomFreeEmps = popFreeEmps;
	}
	omSortOrder.Add("TIMEFROM");
	omSortOrder.Add("TEAM");	
    pomTable = NULL;
    ogCCSDdx.Register(this, SHIFT_NEW, CString("SFVIEWER"), CString("Shift New"), FreeStaffTableTableCf);
	ogCCSDdx.Register(this, SHIFT_CHANGE, CString("SFVIEWER"), CString("Shift Change"), FreeStaffTableTableCf);
    ogCCSDdx.Register(this, JOB_NEW, CString("SFVIEWER"), CString("Job New"), FreeStaffTableTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("SFVIEWER"), CString("Job Change"), FreeStaffTableTableCf);
}

FreeEmpViewer::~FreeEmpViewer()
{
	ogCCSDdx.UnRegister(this,NOTUSED);
    DeleteAll();
}

void FreeEmpViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}

void FreeEmpViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int i, n;

    GetFilter("Schichtcode", olFilterValues);
    omCMapForShiftCode.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllShiftCodes = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllShiftCodes = true;
		else
			for (i = 0; i < n; i++)
				omCMapForShiftCode[olFilterValues[i]] = NULL;
	}

    GetFilter("Dienstrang", olFilterValues);
    omCMapForRank.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllRanks = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllRanks = true;
		else
			for (i = 0; i < n; i++)
				omCMapForRank[olFilterValues[i]] = NULL;
	}

    GetFilter("Permits", olFilterValues);
    omCMapForPermits.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllPermits = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllPermits = true;
		else
			for (i = 0; i < n; i++)
				omCMapForPermits[olFilterValues[i]] = NULL;
	}

    GetFilter("Team", olFilterValues);
    omCMapForTeam.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllTeams = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllTeams = true;
		else
		{
			for (i = 0; i < n; i++)
				omCMapForTeam[olFilterValues[i]] = NULL;
		}
	}

	GetFilter("Pool", olFilterValues);
	omCMapForPool.RemoveAll();
	n = olFilterValues.GetSize();
	bmUseAllPools = false;
	for (i = 0; i < n; i++)
		omCMapForPool[olFilterValues[i]] = NULL;
}

int FreeEmpViewer::CompareFree(FREEEMPS_LINEDATA *prpFree1, 
								   FREEEMPS_LINEDATA *prpFree2)
{
    // Compare in the sort order, from the outermost to the innermost
	int n = omSortOrder.GetSize();
	for (int i = 0; i < n; i++)
	{
		int ilCompareResult = 0;
		if(omSortOrder[i] == "TEAM")
			ilCompareResult = (strcmp(prpFree1->Tmid, prpFree2->Tmid)==0)? 0:
				(strcmp(prpFree1->Tmid, prpFree2->Tmid)>0)? 1: -1;
		else if(omSortOrder[i] == "TIMEFROM") 
			ilCompareResult = (prpFree1->Avfr == prpFree2->Avfr)? 0:
				(prpFree1->Avfr > prpFree2->Avfr)? 1: -1;
		// Check the result of this sorting order, return if unequality is found
		if (ilCompareResult != 0)
			return ilCompareResult;
	}
    return 0;   
}


void FreeEmpViewer::UpdateView(const char *pcpViewName)
{
    SelectView(pcpViewName);
	DeleteAll();
	PrepareFilter();
	MakeLines();
	UpdateDisplay();
}




/////////////////////////////////////////////////////////////////////////////
// FreeEmpViewer -- code specific to this class

void FreeEmpViewer::MakeLines()
{
	int ilCount = 0;
	if(pomFreeEmps == NULL)
	{
		return;
	}

	ilCount = pomFreeEmps->GetSize();

	for(int i = 0; i < ilCount; i++)
	{
		FREEEMPS_LINEDATA rlLine = pomFreeEmps->GetAt(i);
		if(IsPassFilter(&rlLine))
		{
			MakeLine(&rlLine);
		}
	}
}

bool FreeEmpViewer::IsPassFilter(FREEEMPS_LINEDATA *prpLine)
{
    void *p;
    bool blIsRankOk = bmUseAllRanks || omCMapForRank.Lookup(prpLine->Rank, p);
    bool blIsTeamOk = bmUseAllTeams || omCMapForTeam.Lookup(prpLine->Tmid, p);
    bool blIsShiftCodeOk = bmUseAllShiftCodes || omCMapForShiftCode.Lookup(CString(prpLine->Sfca), p);
    bool blIsPoolOk = bmUseAllPools || strlen(prpLine->Pool) <= 0 || omCMapForPool.Lookup(prpLine->Pool, p);

	CStringArray olPermits;
	ogBasicData.GetPermitsForPoolJob(ogJobData.GetJobByUrno(prpLine->PoolJobUrno), olPermits);
	bool blIsPermitOk = bmUseAllPermits;
	int ilNumPermits = olPermits.GetSize();
	for(int ilPermit = 0; !blIsPermitOk && ilPermit < ilNumPermits; ilPermit++)
	{
		blIsPermitOk = omCMapForPermits.Lookup(olPermits[ilPermit], p) ? true : false;
	}

    return (blIsRankOk && blIsTeamOk && blIsShiftCodeOk && blIsPermitOk && blIsPoolOk);
}


void FreeEmpViewer::MakeLine(FREEEMPS_LINEDATA  *prpFree)
{
	
	//File the Line-record and Create the Line
    CreateLine(prpFree);
}



/////////////////////////////////////////////////////////////////////////////
// FreeEmpViewer - STAFFTABLE_LINEDATA array maintenance

void FreeEmpViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int FreeEmpViewer::CreateLine(FREEEMPS_LINEDATA *prpFree)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareFree(prpFree, &omLines[ilLineno]) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFree(prpFree, &omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

	FREEEMPS_LINEDATA rlFreeEmp;
	memcpy(&rlFreeEmp, prpFree, sizeof(FREEEMPS_LINEDATA));
    omLines.NewAt(ilLineno, rlFreeEmp);

    return ilLineno;
}

void FreeEmpViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}


/////////////////////////////////////////////////////////////////////////////
// FreeEmpViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void FreeEmpViewer::UpdateDisplay()
{
//	pomTable->SetHeaderFields("From|To|Pool|Team|Range|Name");
    pomTable->SetHeaderFields(GetString(IDS_STRING61567));
    pomTable->SetFormatList("7|7|17|10|10|32");

    // Load filtered and sorted data into the table content
	pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
		pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
		pomTable->SetTextLineDragEnable(ilLc,FALSE);
    }                                           

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString FreeEmpViewer::Format(FREEEMPS_LINEDATA *prpLine)
{
    CString s = 
          prpLine->Avfr.Format("%d/%H%M") + "|"
        + prpLine->Avto.Format("%d/%H%M") + "|"
		+ CString(prpLine->Pool) + "|"
		+ CString(prpLine->Tmid) + "|"
		+ CString(prpLine->Rank) + "|"
		+ CString(prpLine->Lnam) + "|";

    return s;
}


static void FreeStaffTableTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    FreeEmpViewer *polViewer = (FreeEmpViewer *)popInstance;

	
    if (ipDDXType == SHIFT_CHANGE)
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
	else
    if (ipDDXType == SHIFT_NEW)
        polViewer->ProcessShiftChange((SHIFTDATA *)vpDataPointer);
		
    if (ipDDXType == JOB_CHANGE)
        polViewer->ProcessShiftChange((JOBDATA *)vpDataPointer);
	else
    if (ipDDXType == JOB_NEW)
        polViewer->ProcessShiftChange((JOBDATA *)vpDataPointer);

}


void FreeEmpViewer::ProcessShiftChange(SHIFTDATA *prpShift)
{
}

void FreeEmpViewer::ProcessShiftChange(JOBDATA *prpJob)
{
}

