// STAFF PERMITS
// Each employee has one or more permits (qualifications), these are
// contained in this table - the employees and permits are linked by:
// STFTAB.URNO = SPETAB.SURN
#ifndef _CEDASPEDATA_H_
#define _CEDASPEDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SpeDataStruct
{
	long	Urno;
	long	Surn;		// URNO in of employee in STFTAB
	char	Code[21];	// Permit Code (Defined in PERTAB)
	CTime	Vpfr;		// Valid From
	CTime	Vpto;		// Valid To (TIMENULL = open ended)
	char	Prio[3];	// Priority for multiple functions where 1 = highest >1 lower

	SpeDataStruct(void)
	{
		Urno = 0L;
		Surn = 0L;
		strcpy(Code,"");
		Vpfr = TIMENULL;
		Vpto = TIMENULL;
		strcpy(Prio,"");
	}
};

typedef struct SpeDataStruct SPEDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaSpeData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray <SPEDATA> omData;
	CMapPtrToPtr omSurnMap;
	CString GetTableName(void);

// Operations
public:
	CedaSpeData();
	~CedaSpeData();

	CCSReturnCode ReadSpeData(void);
	void GetPermitsBySurn(long lpSurn, CStringArray &ropEmpPermits, bool bpReset=true);

private:
	void AddSpeInternal(SPEDATA &rrpSpe);
	void ClearAll();
};


extern CedaSpeData ogSpeData;
#endif _CEDASPEDATA_H_
