// ReassignBreak.h : header file
//
// object can reassign breaks for one or more newly inserted jobs:
// the breaks are only reassigned if they iverlap the new job and if they still fit in the Pausenlage
// Example:
//
// CReassignBreak olReassignedBreaks;  // declaration
// olReassignedBreaks.ReassignBreakForNewJob(&rlJob); // attempt to reassign the break job
// // either
//		olReassignedBreaks.Commit(); // saves the reassigned breaks to the DB
// // or
//		olReassignedBreaks.Rollback(); // undo the changes

#if !defined(AFX_REASSIGNBREAK_H__7CF146E3_CA0E_11D6_81AE_00010215BFE5__INCLUDED_)
#define AFX_REASSIGNBREAK_H__7CF146E3_CA0E_11D6_81AE_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CedaJobData.h>

/////////////////////////////////////////////////////////////////////////////
// CReassignBreak
struct ReassignedBreak
{
	JOBDATA *BreakPtr;
	long JobUrno;

	// old values required to rollback the change if the user presses CANCEL in the conflict window
	CTime OldBreakBegin;
	CTime OldBreakEnd;

	ReassignedBreak(void)
	{
		BreakPtr = NULL;
		JobUrno = 0L;
		OldBreakBegin = TIMENULL;
		OldBreakEnd = TIMENULL;
	}
};

typedef struct ReassignedBreak REASSIGNEDBREAK;

class CReassignBreak
{
public:
	CReassignBreak();
	virtual ~CReassignBreak();

	bool ReassignBreakForNewJob(JOBDATA *prpNewJob);
	void UpdateBreakTime(JOBDATA *prpJob);
	void Rollback(void);
	void Commit(void);

private:
	CCSPtrArray <REASSIGNEDBREAK> omReassignedBreaks;
	bool bmBreakReassignActivated;
	JOBDATA *CanReassignBreak(JOBDATA *prpNewJob, CTime &ropNewBegin, CTime &ropNewEnd);
	void Rollback(REASSIGNEDBREAK *prpReassignedBreak);
};

/////////////////////////////////////////////////////////////////////////////


#endif // !defined(AFX_REASSIGNBREAK_H__7CF146E3_CA0E_11D6_81AE_00010215BFE5__INCLUDED_)
