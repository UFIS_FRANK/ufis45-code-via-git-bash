// AssignJobDetachDlg.h : header file
//
#ifndef _CASSIGNMENTJOBDETACHDIALOG_
#define _CASSIGNMENTJOBDETACHDIALOG_
/////////////////////////////////////////////////////////////////////////////
// CAssignmentJobDetachDialog dialog

class CAssignmentJobDetachDialog : public CAssignmentDialog
{
// Construction
public:
	CAssignmentJobDetachDialog(CWnd* pParent,
		CCSDragDropCtrl *popDragDropCtrl,
		const char *pcpPoolId, const char *pcpPoolName);

// Dialog Data
	CCSDragDropCtrl *pomDragDropCtrl;
	CString omPoolId;
	CString omPoolName;
	//{{AFX_DATA(CAssignmentJobDetachDialog)
	enum { IDD = IDD_ASSIGNMENT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAssignmentJobDetachDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:
	CDWordArray omStartTimeAllowed;	// possible time period for each staff
	CDWordArray omEndTimeAllowed;

	// Generated message map functions
	//{{AFX_MSG(CAssignmentJobDetachDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif //_CASSIGNMENTJOBDETACHDIALOG_
