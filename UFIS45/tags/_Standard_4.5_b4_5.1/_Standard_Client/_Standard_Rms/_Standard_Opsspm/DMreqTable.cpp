// DMreqTable.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <CCSTime.H>
#include <CedaShiftTypeData.h>
#include <EmpDialog.h>
#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaRequestData.h>

#include <ccsddx.h>
#include <CedaAloData.h>
#include <BasicData.h>

#include <DMreqTable.h>

#ifdef _DEBUG
//#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DMreqTable dialog


DMreqTable::DMreqTable(CWnd* pParent,REQUESTDATA *prpRequest,int ipDlgAction)
	: CDialog(DMreqTable::IDD, pParent)
{
	prmRequest = prpRequest;
	imDlgAction = ipDlgAction;

	//{{AFX_DATA_INIT(DMreqTable)
	m_Rdat = CTime(time(NULL));
	m_NameFrom = _T("");;
	m_Rqda = CTime(time(NULL));
	m_NameTo = _T("");;
	m_Text = _T("");;
	//}}AFX_DATA_INIT
}


void DMreqTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DMreqTable)
		DDX_CCSddmmyy(pDX, IDC_RDAT, m_Rdat);
		DDX_Text(pDX, IDC_NAMFROM, m_NameFrom);
		DDV_MaxChars(pDX, m_NameFrom, 30);
		DDX_CCSddmmyy(pDX, IDC_RQDA, m_Rqda);
		DDX_Text(pDX, IDC_NAMTO, m_NameTo);
		DDV_MaxChars(pDX, m_NameTo, 30);
		DDX_Text(pDX, IDC_TEXT, m_Text);
		DDV_MaxChars(pDX, m_Text, 1020);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DMreqTable, CDialog)
	//{{AFX_MSG_MAP(DMreqTable)
	ON_CBN_EDITCHANGE(IDC_NAMFROM, OnEditchangeNamfrom)
	ON_CBN_EDITCHANGE(IDC_NAMTO, OnEditchangeNamto)
	ON_CBN_KILLFOCUS(IDC_NAMFROM, OnKillfocusNamfrom)
	ON_CBN_KILLFOCUS(IDC_NAMTO, OnKillfocusNamto)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DMreqTable message handlers

void DMreqTable::OnOK() 
{
	UpdateData(TRUE);

	strcpy(prmRequest->Cusr,omCusr);
	strcpy(prmRequest->Peno,omPeno);

	prmRequest->Rdat = m_Rdat;
	prmRequest->Rqda = m_Rqda;
	strcpy(prmRequest->Text,m_Text);
	
	CString olErrorMessage;

	if (omCusr.IsEmpty())
	{
//		olErrorMessage = CString("Bitte eingeben von wem der Request erfasst wurde!\n");
		olErrorMessage = GetString(IDS_STRING61263);
	}
	if (omPeno.IsEmpty())
	{
//		olErrorMessage += CString("Bitte eingeben f�r wen der Request bestimmt ist!\n");
		olErrorMessage += GetString(IDS_STRING61264);
	}
	if (! olErrorMessage.IsEmpty())
	{
		MessageBox(olErrorMessage);
		return;
	}
	CDialog::OnOK();
}

BOOL DMreqTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING32998)); 

	CWnd *polWnd = GetDlgItem(IDC_REQREGISTER); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32999));
	}
	polWnd = GetDlgItem(IDC_REQREQ); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61821));
	}
	polWnd = GetDlgItem(IDC_REQDATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33000));
	}
	polWnd = GetDlgItem(IDC_REQEMPLOYEE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32861));
	}
	polWnd = GetDlgItem(IDC_REQTEXT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33001));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}


	if (imDlgAction == DLG_INSERT)
	{
		CTime olCT = ogBasicData.GetTime();
		prmRequest->Rdat = CTime(olCT.GetYear(),olCT.GetMonth(),olCT.GetDay(),
				olCT.GetHour(),olCT.GetMinute(),olCT.GetSecond());
		prmRequest->Rqda = prmRequest->Rdat;
	}
		
	omCusr = prmRequest->Cusr;
	omPeno = prmRequest->Peno;

	m_Rdat = prmRequest->Rdat;
	EMPDATA *prlEmp;
	prlEmp = ogEmpData.GetEmpByPeno(prmRequest->Cusr);
	if (prlEmp != NULL)
	{
		m_NameFrom = ogEmpData.GetEmpName(prlEmp);
	}
	prlEmp = ogEmpData.GetEmpByPeno(prmRequest->Peno);
	if (prlEmp != NULL)
	{
		m_NameTo = ogEmpData.GetEmpName(prlEmp);
	}
	m_Rqda = prmRequest->Rqda;
	m_Text = prmRequest->Text;

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void DMreqTable::OnKillfocusNamfrom() 
{
	CComboBox *polWnd = (CComboBox *) GetDlgItem(IDC_NAMFROM);
	if (polWnd != NULL)
	{
		int ilIndex = polWnd->GetCurSel();
		if (ilIndex == CB_ERR)
			ilIndex = 0;
		EMPDATA *prlEmp = (EMPDATA *)polWnd-> GetItemDataPtr(ilIndex);
		if (prlEmp != (EMPDATA *) -1)
		{
			omCusr = CString(prlEmp->Peno);
		}
		else
		{
			omCusr.Empty();
		}
	}
}

void DMreqTable::OnKillfocusNamto() 
{
	CComboBox *polWnd = (CComboBox *) GetDlgItem(IDC_NAMTO);
	if (polWnd != NULL)
	{
		int ilIndex = polWnd->GetCurSel();
		if (ilIndex == CB_ERR)
			ilIndex = 0;
		EMPDATA *prlEmp = (EMPDATA *)polWnd-> GetItemDataPtr(ilIndex);
		if (prlEmp != (EMPDATA *) -1)
		{
			omPeno = CString(prlEmp->Peno);
		}
		else
		{
			omPeno.Empty();
		}
	}

}


void DMreqTable::OnEditchangeNamfrom() 
{
	char pclBuf1[24];
	char pclBuf2[24];

	CComboBox *polWnd = (CComboBox *) GetDlgItem(IDC_NAMFROM);
	if (polWnd != NULL)
	{
		polWnd->GetWindowText(pclBuf1,20);
		if (strlen(pclBuf1) > 0)
		{
			strcpy(pclBuf2,&pclBuf1[1]);
			pclBuf2[strlen(pclBuf2)+1] = '\0';
			pclBuf2[strlen(pclBuf2)] = *pclBuf1;
		}
		else
		{
			*pclBuf2 = '\0';
		}
		CPoint orCaretPosition = polWnd->GetCaretPos( );

		while (polWnd->DeleteString(0) != CB_ERR);

		CCSPtrArray<EMPDATA> olEmployees;
		ogEmpData.GetEmpsByLanm(olEmployees,pclBuf2);
		for( int ilLc = 0; ilLc < olEmployees.GetSize(); ilLc++)
		{
			CString olName = ogEmpData.GetEmpName(&olEmployees[ilLc]);
			int ilIndex = polWnd->AddString(olName);
			if (ilIndex != CB_ERR)
			{
				polWnd->SetItemDataPtr(ilIndex,(void *)&olEmployees[ilLc]);
			}
		}
		polWnd->SetWindowText(pclBuf2);
		polWnd->ShowDropDown(TRUE);
		polWnd->SetCaretPos(orCaretPosition);
	}
}



void DMreqTable::OnEditchangeNamto() 
{
	char pclBuf1[24];
	char pclBuf2[24];

	CComboBox *polWnd = (CComboBox *) GetDlgItem(IDC_NAMTO);
	if (polWnd != NULL)
	{
		polWnd->GetWindowText(pclBuf1,20);
		if (strlen(pclBuf1) > 0)
		{
			strcpy(pclBuf2,&pclBuf1[1]);
			pclBuf2[strlen(pclBuf2)+1] = '\0';
			pclBuf2[strlen(pclBuf2)] = *pclBuf1;
		}
		else
		{
			*pclBuf2 = '\0';
		}
		CPoint orCaretPosition = polWnd->GetCaretPos( );
		while (polWnd->DeleteString(0) != CB_ERR);

		CCSPtrArray<EMPDATA> olEmployees;
		ogEmpData.GetEmpsByLanm(olEmployees,pclBuf2);
		for( int ilLc = 0; ilLc < olEmployees.GetSize(); ilLc++)
		{
			CString olName = ogEmpData.GetEmpName(&olEmployees[ilLc]);
			int ilIndex = polWnd->AddString(olName);
			if (ilIndex != CB_ERR)
			{
				polWnd->SetItemDataPtr(ilIndex,(void *)&olEmployees[ilLc]);
			}
		}
		polWnd->ShowDropDown(TRUE);
		polWnd->SetWindowText(pclBuf2);
		polWnd->SetCaretPos(orCaretPosition);
	}
}

