// CciDW.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <CCSDragDropCtrl.h>
#include <tscale.h>
#include <CCSPtrArray.h>

#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <cviewer.h>
#include <CciDetailWindow.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <dataset.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <ConflictTable.h>
#include <ccitable.h>
#include <gatetable.h>
#include <ButtonList.h>
#include <ccsddx.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
static void CciDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// CciDetailWindow

IMPLEMENT_DYNCREATE(CciDetailWindow, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//static BOOL GetMaxJobEndTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMaxEndTime)
//{
//	int ilSize = popJobs->GetSize();
//	CTime olTime;
//	for(int ilIndex = 0; ilIndex < ilSize; ilIndex++)
//	{
//		olTime = popJobs[ilIndex].Acto;
//		if (ropMaxEndTime < olTime)
//			ropMaxEndTime = olTime;
//	}
//	return TRUE;
//}
//
//static BOOL GetMinJobStartTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMinStartTime)
//{
//	int ilSize = popJobs->GetSize();
//	CTime olTime;
//	for(int ilIndex = 0; ilIndex < ilSize; ilIndex++)
//	{
//		olTime = popJobs[ilIndex].Acfr;
//		if (ropMinStartTime > olTime)
//			ropMinStartTime = olTime;
//	}
//	return TRUE;
//}

CciDetailWindow::CciDetailWindow()
{
}

CciDetailWindow::CciDetailWindow(CWnd *popParent,const char *pcpAlocAlid)
{
//	pomJobs = NULL;
	strcpy(cmAlocAlid, pcpAlocAlid);
    imStartTimeScalePos = 20;

    Create(NULL, "Cci Detail Window", WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE,
        CRect(0, 56, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN) - 140),
        popParent, NULL, 0, NULL);
	CenterWindow();
	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
}

CciDetailWindow::~CciDetailWindow()
{
//	pomJobs->RemoveAll();
}

BEGIN_MESSAGE_MAP(CciDetailWindow, CFrameWnd)
	//{{AFX_MSG_MAP(CciDetailWindow)
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_WM_ERASEBKGND()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CciDetailWindow message handlers


int CciDetailWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	omViewer.Attach(this);
	omViewer.Init(cmAlocAlid, TRUE);
//	pomJobs = omViewer.GetJobsPtr();

	char clBuf[255];
	//sprintf(clBuf, "Einsatzdaten <%s>", cmAlocAlid);
	sprintf(clBuf, GetString(IDS_STRING61298), cmAlocAlid);
	SetWindowText(clBuf);
    
	omCciDetail.SetAlocAlidPtr(&cmAlocAlid[0]);
	omCciDetail.Create(omCciDetail.IDD, this);

    CRect olRect;
    omCciDetail.GetDlgItem(IDC_BELEGUNG) -> GetWindowRect(&olRect); ScreenToClient(&olRect);
    imBottomPos = olRect.bottom;
    
    CRect olDRect; omCciDetail.GetClientRect(&olDRect);
    CRect olWRect; GetWindowRect(&olWRect);
    omMaxTrackSize = CPoint(olDRect.Width() + 8, olWRect.Height());
    MoveWindow(olWRect.left, olWRect.top, olDRect.Width() + 8, olWRect.Height(), FALSE);

//	// Time block
//    CTime olCurrentTime = ogBasicData.GetTime();
//    CTime olCT = CTime(
//        olCurrentTime.GetYear(), olCurrentTime.GetMonth(), olCurrentTime.GetDay(),
//        olCurrentTime.GetHour(), olCurrentTime.GetMinute(), 0
//    );
//
//	CTime olTime;
//	CTime olMinStartTime = olCT - CTimeSpan(0, 1, 0, 0);
//	CTime olMaxEndTime = olMinStartTime + CTimeSpan(0, 4, 0, 0);
//
//   	if (GetMinStartTime(pomJobs, olTime) == TRUE)
//	{
//		olMinStartTime = olTime;
//	}
//	if (GetMaxEndTime(pomJobs, olTime) == TRUE)
//	{
//		olMaxEndTime = olTime;
//	}

	omTSStartTime = omViewer.GetTimescaleStart() - CTimeSpan(0, 1, 0, 0);
	omTSDuration =  omViewer.GetTimescaleEnd() - omTSStartTime + CTimeSpan(0, 1, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

    olRect.InflateRect(-15, -15);
    omTimeScale.EnableDisplayCurrentTime(FALSE);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(olRect.left, olRect.top, olRect.right, olRect.top + 34),
        this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    omGantt.SetTimeScale(&omTimeScale);
    omGantt.SetViewer(&omViewer, 0 /* GroupNo */);
    omGantt.SetStatusBar(&omStatusBar);
    omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
    omGantt.SetVerticalScaleWidth(-2);
    omGantt.SetFonts(&ogMSSansSerif_Regular_8, &ogSmallFonts_Regular_6);
    //omGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	omGantt.SetGanttChartColors(NAVY, GRAY, YELLOW, GRAY);
    omGantt.Create(0, CRect(olRect.left, olRect.top + 34, olRect.right, olRect.bottom), &omCciDetail);
	omGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	
	// Register DDX call back function
	TRACE("CciDetailWindow: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("CCIDW"), CString("Redisplay all"), CciDetailWindowCf);

	return 0;
}

void CciDetailWindow::OnDestroy() 
{
	if (bgModal == TRUE)
		return;

	// Unregister DDX call back function
	TRACE("CciDetailWindow: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void CciDetailWindow::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL CciDetailWindow::OnEraseBkgnd(CDC* pDC) 
{
    CRect olRect; GetClientRect(&olRect);
    olRect.top = imBottomPos;
    CBrush olBrush(SILVER);
    pDC->FillRect(&olRect, &olBrush);

    return TRUE;
}

////////////////////////////////////////////////////////////////////////
// CciDetailWindow -- implementation of yellow vertical time band lines

static void CciDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	CciDetailWindow *polDetailWindow = (CciDetailWindow *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polDetailWindow->DestroyWindow();
	}
}
