// CicDemandTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>

#include <OpssPm.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <CicDemandTableViewer.h>
#include <CicDemandTable.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <DemandFilterPage.h>
#include <CicDemandTableSortPage.h>
#include <BasePropertySheet.h>
#include <CicDemandTablePropertySheet.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <dataset.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <FlightDetailWindow.h>
#include <CciDemandDetailDlg.h>
#include <ccstable.h>
#include <CicAutoAssignDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

/////////////////////////////////////////////////////////////////////////////
// CicDemandTable dialog

CicDemandTable::CicDemandTable(CWnd* pParent /*=NULL*/)
	: CDialog(CicDemandTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicDemandTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomParentWnd = pParent;
	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
    pomTable->tempFlag = 2;

    CDialog::Create(CicDemandTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.CCDB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

	CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
 //   rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

	pomViewer = new CicDemandTableViewer(NULL);
	pomViewer->Attach(pomTable);
	UpdateView();
}

CicDemandTable::CicDemandTable(CicDemandTableViewer *popViewer,CciDiagramViewer *popParentViewer,CWnd* pParent /*=NULL*/)
	: CDialog(CicDemandTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicDemandTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	if (popViewer)
		pomViewer = popViewer;
	else
		pomViewer = new CicDemandTableViewer(popParentViewer);

	pomParentViewer = popParentViewer;
	pomParentWnd	= pParent;

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
    pomTable->tempFlag = 2;

    CDialog::Create(CicDemandTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.CCDB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

	CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
 //   rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	
	pomViewer->Attach(pomTable);
	UpdateView();
}

CicDemandTable::~CicDemandTable()
{
	TRACE("CicDemandTable::~CicDemandTable()\n");
	pomViewer->Attach(NULL);
	delete pomTable;	
}

void CicDemandTable::SetCaptionText(void)
{
//	CString olCaptionText = CString("Counter Demands: ") + 
//		pomViewer->omStartTime.Format("%d/%H%M") + "-" + 
//			pomViewer->omEndTime.Format("%H%M ");;
	CString olCaptionText = GetString(IDS_STRING61959) + pomViewer->StartTime().Format("%d/%H%M") + "-" + pomViewer->EndTime().Format("%H%M ");;
//	if (bgOnline)
//	{
//		//olCaptionText += "  Online";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//olCaptionText += "  OFFLINE";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}

	SetWindowText(olCaptionText);

}

void CicDemandTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicDemandTable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	//}}AFX_DATA_MAP
}

void CicDemandTable::UpdateView()
{

	CString  olViewName = pomViewer->GetViewName();
	olViewName = pomViewer->GetViewName();

	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.CCDV;
	}
	
	BOOL blIsGrouped = *pomViewer->GetGroup() - '0';
	
	CWaitCursor olWait;
    pomViewer->ChangeViewTo(olViewName);
	SetCaptionText();
	if (blIsGrouped)
	{
		CWnd *polWnd = GetDlgItem(IDC_EXPANDGROUP); 
		if(polWnd != NULL)
		{
			if(ogBasicData.bmDisplayTeams)
			{
				polWnd->ShowWindow(SW_SHOW);
			}
			else
			{
				polWnd->ShowWindow(SW_HIDE);
			}
		}

		polWnd = GetDlgItem(IDC_COMPRESSGROUP); 
		if(polWnd != NULL)
		{
			if(ogBasicData.bmDisplayTeams)
			{
				polWnd->ShowWindow(SW_SHOW);
			}
			else
			{
				polWnd->ShowWindow(SW_HIDE);
			}
		}
		
	}
	else
	{
		CWnd *polWnd = GetDlgItem(IDC_EXPANDGROUP); 
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

		polWnd = GetDlgItem(IDC_COMPRESSGROUP); 
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}

	}
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();
}

void CicDemandTable::UpdateComboBox()

{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	polCB->ResetContent();
	CStringArray olStrArr;
	pomViewer->GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = pomViewer->GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.CCDV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

}

BEGIN_MESSAGE_MAP(CicDemandTable, CDialog)
	//{{AFX_MSG_MAP(CicDemandTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_VIEWCOMBO, OnSelchangeViewcombo)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_BN_CLICKED(IDC_EXPANDGROUP,OnExpandGroup)
	ON_BN_CLICKED(IDC_COMPRESSGROUP,OnCompressGroup)
    ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelchange)
	ON_COMMAND(11, OnMenuWorkOn)
	ON_COMMAND(12, OnMenuFreeEmployees)
	ON_BN_CLICKED(IDC_ALLOCATE,OnAllocate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CicDemandTable message handlers

BOOL CicDemandTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING61870)); 

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}

	polWnd = GetDlgItem(IDC_ALLOCATE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61975));
	}

#if	0	
	polWnd = GetDlgItem(IDC_EXPANDGROUP); 
	if(polWnd != NULL)
	{
		if(ogBasicData.bmDisplayTeams)
		{
			polWnd->SetWindowText(GetString(IDS_EXPANDGROUP));
		}
		else
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}

	polWnd = GetDlgItem(IDC_COMPRESSGROUP); 
	if(polWnd != NULL)
	{
		if(ogBasicData.bmDisplayTeams)
		{
			polWnd->SetWindowText(GetString(IDS_CONTRACTGROUP));
		}
		else
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
#endif

	// TODO: Add extra initialization here
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

	UpdateComboBox();

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    MoveWindow(&rect);

	m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());

	// Register DDX call back function
	TRACE("CicDemandTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("CICDEMANDTABLE"),CString("Redisplay all"), CicDemandTableCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("CICDEMANDTABLE"),CString("Global Date Change"), CicDemandTableCf);
	ogCCSDdx.Register(this, ONLINE_STATUS_CHANGE, CString("CICDEMANDTABLE"),CString("Online/Offline status change"), CicDemandTableCf);

	ogBasicData.SetWindowStat("CICDEMANDTABLE IDC_VIEW",GetDlgItem(IDC_VIEW));
	ogBasicData.SetWindowStat("CICDEMANDTABLE IDC_VIEWCOMBO",GetDlgItem(IDC_VIEWCOMBO));
	ogBasicData.SetWindowStat("CICDEMANDTABLE IDC_PRINT",GetDlgItem(IDC_PRINT));

	if(bgOnline)
	{
		ogBasicData.SetWindowStat("CICDEMANDTABLE IDC_ALLOCATE",GetDlgItem(IDC_ALLOCATE));
	}
	else
	{
		ogBasicData.SetWindowStat('0',GetDlgItem(IDC_ALLOCATE));
	}

	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CicDemandTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("CicDemandTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void CicDemandTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
}

void CicDemandTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
        pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

void CicDemandTable::OnExpandGroup()
{
	CWaitCursor olWait;
	pomViewer->CompressGroups(false);
}

void CicDemandTable::OnCompressGroup()
{
	CWaitCursor olWait;
	pomViewer->CompressGroups(true);
}

void CicDemandTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();

	CStringArray olGroupNames;
	if (pomParentViewer)
	{
		pomParentViewer->GetGroupNames(olGroupNames);
	}

	CicDemandTablePropertySheet dialog(olGroupNames,this, pomViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		UpdateView();
	}
	bmIsViewOpen = FALSE;
}

void CicDemandTable::OnSelchangeViewcombo() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	pomViewer->SelectView(clText);
	UpdateView();		
}

void CicDemandTable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
//    pomTable->GetCTableListBox()->SetFocus();
}

void CicDemandTable::OnPrint() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();

	pomViewer->PrintView();
}

BOOL CicDemandTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	if (pomParentWnd)
	{
		pomParentWnd->SendMessage(WM_CICDEMANDTABLE_EXIT,0,0l);
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// CicDemandTable -- implementation of DDX call back function

void CicDemandTable::CicDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	CicDemandTable *polTable = (CicDemandTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
	else if(ipDDXType == ONLINE_STATUS_CHANGE)
	{
		if(bgOnline)
		{
			ogBasicData.SetWindowStat("CICDEMANDTABLE IDC_ALLOCATE",polTable->GetDlgItem(IDC_ALLOCATE));
		}
		else
		{
			ogBasicData.SetWindowStat('0',polTable->GetDlgItem(IDC_ALLOCATE));
		}
	}
}



//*****
/////////////////////////////////////////////////////////////////////////////
// CicDemandTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a job (flight or non flight dependend).
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create normal flight or flight independent jobs for those staffs.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create a normal flight job for that staff.
//

LONG CicDemandTable::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CDWordArray olSelectedDemandUrnos;
    CListBox *polListBox = pomTable->GetCTableListBox();
	int ilSelCount = 0;
	if (polListBox && (ilSelCount = polListBox->GetSelCount()) > 0)
	{
		if(ilSelCount > 1)
		{
			int *polSelectedLines = new int[polListBox->GetSelCount()];
			polListBox->GetSelItems(polListBox->GetSelCount(),polSelectedLines);

			for (int i = 0; i < polListBox->GetSelCount(); i++)
			{
				CICDEMDATA_LINE *prlLine = (CICDEMDATA_LINE *)pomTable->GetTextLineData(polSelectedLines[i]);
				if (prlLine)
				{
					if (prlLine->IsTeamLine && !prlLine->Expanded)
					{
					}
					else
					{
						olSelectedDemandUrnos.Add(prlLine->DemUrno);
					}
				}
			}
			delete [] polSelectedLines;
		}
		else
		{
			CICDEMDATA_LINE *prlLine = (CICDEMDATA_LINE *)pomTable->GetTextLineData(wParam);
			if (prlLine != NULL)
			{
				TIMEPACKET olTimePacket;
				olTimePacket.StartTime = prlLine->Debe;
				olTimePacket.EndTime   = prlLine->Deen;
				ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);

				if (prlLine->IsTeamLine && !prlLine->Expanded)
				{
				}
				else
				{
					olSelectedDemandUrnos.Add(prlLine->DemUrno);
				}
			}
		}
	}
	int ilNumDemUrnos = olSelectedDemandUrnos.GetSize();
	m_DragDropTarget.CreateDWordData(DIT_CICDEMANDBAR, ilNumDemUrnos);
	for(int ilDU = 0; ilDU < ilNumDemUrnos; ilDU++)
		m_DragDropTarget.AddDWord(olSelectedDemandUrnos[ilDU]);
	m_DragDropTarget.BeginDrag();
	pomTable->GetCTableListBox()->SetSel(-1,FALSE);

	return 0L;
}

LONG CicDemandTable::OnDragOver(UINT wParam, LONG lParam)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_CCIDESK)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	pomTable->GetCTableListBox()->ScreenToClient(&olDropPosition);
	int ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= pomViewer->Lines() - 1))
		return -1L;	// cannot drop on a blank line

	CICDEMDATA_LINE *prlLine = pomViewer->GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropDemandUrno = prlLine->DemUrno;

	return 0L;
}

LONG CicDemandTable::OnDrop(UINT wParam, LONG lParam)
{ 
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	pomTable->GetCTableListBox()->ScreenToClient(&olDropPosition);
	int ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= pomViewer->Lines() - 1))
		return -1L;	// cannot drop on a blank line

	CICDEMDATA_LINE *prlLine = pomViewer->GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropDemandUrno = prlLine->DemUrno;

	int ilDataCount = m_DragDropTarget.GetDataCount();
	ASSERT(ilDataCount == 1);
	
	CString olCciDesk = m_DragDropTarget.GetDataString(0);

	ogDataSet.ReassignCicDemandAlid(this,imDropDemandUrno,olCciDesk);
	
	return 0L;
}

void CicDemandTable::OnClose() 
{
	if (pomParentWnd)
	{
		pomParentWnd->SendMessage(WM_CICDEMANDTABLE_EXIT,0,0l);
	}

	CDialog::OnClose();
}

void CicDemandTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();
}

void CicDemandTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	pomViewer->SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	pomViewer->SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void CicDemandTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

LONG CicDemandTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = pomViewer->Lines();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		CICDEMDATA_LINE *prlLine = pomViewer->GetLine(ipItem);
		if (prlLine != NULL)
		{
			if (!ogDemandData.IsFlightDependingDemand(prlLine->Dety))
			{
				if (prlLine->Dety[0] == CCI_DEMAND)
					new CciDemandDetailDlg(this,prlLine->DemUrno);
			}
			else
			{
				DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prlLine->DemUrno);
				if (prlDemand)
				{
					new FlightDetailWindow(this,prlDemand->Ouro,0l,FALSE,ALLOCUNITTYPE_CIC);
				}
			}
		}
	}

	return 0L;
}

LONG CicDemandTable::OnTableSelchange(UINT wParam,LONG lParam)
{
	int ilIndex = pomTable->GetCurSel();
	if (ilIndex >= 0 && ilIndex < pomViewer->Lines())
	{
		CICDEMDATA_LINE *prlLine = pomViewer->GetLine(ilIndex);
		if (prlLine != NULL)
		{
			TIMEPACKET olTimePacket;
			olTimePacket.StartTime = prlLine->Debe;
			olTimePacket.EndTime   = prlLine->Deen;
			ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);
		}
	}

	return 0l;
}

LONG CicDemandTable::OnTableLButtonDown(UINT ipItem, LONG lpLParam)
{
	return 0l;
}

LONG CicDemandTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam)
{
    CListBox *polListBox = pomTable->GetCTableListBox();
	if (polListBox && polListBox->GetSelCount() > 1)
		return 0L;

	CMenu menu;
	CCSTABLENOTIFY *polNotify = reinterpret_cast<CCSTABLENOTIFY *>(lpLParam);
	ASSERT(polNotify);

	CPoint olPoint(polNotify->Point);

	CICDEMDATA_LINE *prlLine = (CICDEMDATA_LINE *)pomTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		imDropDemandUrno = prlLine->DemUrno;

		int ilLc;
		
		menu.CreatePopupMenu();
		for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
			menu.RemoveMenu(ilLc, MF_BYPOSITION);

		//menu.AppendMenu(MF_STRING,11,"Bearbeiten");	
		menu.AppendMenu(MF_STRING,11,GetString(IDS_STRING61352));	

		//menu.AppendMenu(MF_STRING,12, "Freie Mitarbeiter");	// to find another free employee
//		menu.AppendMenu(MF_STRING,12, GetString(IDS_STRING61376));	// to find another free employee

		ClientToScreen( &olPoint);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
	}

	return 0L;
}

void CicDemandTable::OnMenuWorkOn()
{
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if (prlDemand)
	{
		if (prlDemand->Dety[0] == CCI_DEMAND)
		{
			new CciDemandDetailDlg(this,imDropDemandUrno);
		}
		else if (ogDemandData.IsFlightDependingDemand(prlDemand->Dety))
		{
			new FlightDetailWindow(this,prlDemand->Ouro,0l,FALSE,ALLOCUNITTYPE_CIC);
		}
	}
}

void CicDemandTable::OnMenuFreeEmployees()
{
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(imDropDemandUrno);
	if (!prlDemand)
		return;

	CICDEMDATA_LINE *prlLine = pomViewer->GetLine(prlDemand);
	if (!prlLine)
		return;
}

void CicDemandTable::OnAllocate()
{
	CicAutoAssignDlg olAutoAssignDlg(pomParentViewer,this);

	CDWordArray olSelectedDemandUrnos;
    CListBox *polListBox = pomTable->GetCTableListBox();
	if (polListBox && polListBox->GetSelCount() > 0)
	{
		int *polSelectedLines = new int[polListBox->GetSelCount()];
		polListBox->GetSelItems(polListBox->GetSelCount(),polSelectedLines);

		for (int i = 0; i < polListBox->GetSelCount(); i++)
		{
			CICDEMDATA_LINE *polLine = (CICDEMDATA_LINE *)pomTable->GetTextLineData(polSelectedLines[i]);
			if (polLine)
				olSelectedDemandUrnos.Add(polLine->DemUrno);
		}
		delete [] polSelectedLines;
	}

	olAutoAssignDlg.SetData(pomParentViewer->GetStartTime(),pomParentViewer->GetEndTime(),olSelectedDemandUrnos);
	olAutoAssignDlg.DoModal();
}
