// CedaWroData.cpp: Auto assignment parameters for line maintenence (Reduktionstufen)
//
//////////////////////////////////////////////////////////////////////

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaWroData.h>
#include <BasicData.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CedaWroData::CedaWroData()
{                  
    BEGIN_CEDARECINFO(WRODATA, WRODATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_DATE(Nafr,"NAFR")
		FIELD_DATE(Nato,"NATO")
		FIELD_CHAR_TRIM(Brca,"BRCA")
		FIELD_CHAR_TRIM(Wnam,"WNAM")
		FIELD_CHAR_TRIM(Resn,"RESN")
		FIELD_CHAR_TRIM(Shgn,"SHGN")
		FIELD_CHAR_TRIM(Tele,"TELE")
		FIELD_CHAR_TRIM(Term,"TERM")
		FIELD_DATE(Vafr,"VAFR")
		FIELD_DATE(Vato,"VATO")
		
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(WRODATARecInfo)/sizeof(WRODATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WRODATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"WROTAB");
    pcmFieldList = "URNO,NAFR,NATO,BRCA,WNAM,RESN,SHGN,TELE,TERM,VAFR,VATO";
}
 
CedaWroData::~CedaWroData()
{
	TRACE("CedaWroData::~CedaWroData called\n");
	ClearAll();
}

void CedaWroData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omWnamMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaWroData::ReadWroData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaWroData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		WRODATA rlWRODATA;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlWRODATA)) == RCSuccess)
			{
				AddWroInternal(&rlWRODATA);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaWroData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(WRODATA), pclWhere);
    return ilRc;
}


void CedaWroData::AddWroRecord(WRODATA *prpData,BOOL bpSendDDX)
{
	if (prpData->Urno == 0L)
	{
		prpData->Urno = ogBasicData.GetNextUrno();
	}

	AddWroInternal(prpData);
	DbInsert(prpData);
}

void CedaWroData::ChangeWroRecord(WRODATA *prpData,BOOL bpSendDDX)
{

	if (prpData->Urno == 0L)
	{
		prpData->Urno = ogBasicData.GetNextUrno();
	}

	WRODATA *prlOldWro = GetWroByUrno(prpData->Urno);
	if (prlOldWro != NULL)
	{
		UpdateMaps(prpData,prlOldWro);
	}
	*prlOldWro = *prpData;
	DbUpdate(prlOldWro);
}


void CedaWroData::AddWroInternal(WRODATA *prpWro)
{
	WRODATA *prlWro = new WRODATA;
	*prlWro = *prpWro;


	omData.Add(prlWro);
	omUrnoMap.SetAt((void *)prlWro->Urno,prlWro);
	omWnamMap.SetAt(prlWro->Wnam,prlWro);
}

bool CedaWroData::DbInsert(WRODATA *prpWro)
{
	CCSReturnCode ilRc = RCSuccess;

		char pclSelection[124] = "";
		CString olData;
		char pclCmd[10] = "IRT";
		char pclData[2000];
		
		//PrepareDataForWrite(prpJob);
		//ConvertDatesToUtc(prpJob);
		//GetNewFields(prpJob, pclInsertFieldList, pclData);
		//GetAllFields(prpJob, pclInsertFieldList, pclData);

		//ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclInsertFieldList, pclData);
		MakeCedaData(&omRecInfo,olData,prpWro);
		strcpy(pclData,olData);
		ilRc = CedaAction(pclCmd,pclSelection ,"", pclData);
		if (ilRc != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaPcxData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			ogCCSDdx.DataChanged((void *)this, WRO_NEW,(void *)prpWro);
		}

	//	ConvertDatesToLocal(prpJob);

	return ilRc;
}

bool CedaWroData::DbUpdate(WRODATA *prpWro)
{
	CCSReturnCode ilRc = RCSuccess;

		char pclSelection[124] = "";
		CString olData;
		char pclCmd[10] = "URT";
		char pclData[2000];
		

		//ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclInsertFieldList, pclData);
		sprintf(pclSelection,"WHERE URNO=%ld",prpWro->Urno);
		MakeCedaData(&omRecInfo,olData,prpWro);
		strcpy(pclData,olData);
		ilRc = CedaAction(pclCmd,pclSelection ,"", pclData);
		if (ilRc != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaPcxData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			ogCCSDdx.DataChanged((void *)this,WRO_CHANGE,(void *)prpWro);
		}

	//	ConvertDatesToLocal(prpJob);

	return ilRc;
}
bool CedaWroData::DbDelete(WRODATA *prpWro)
{
	CCSReturnCode ilRc = RCSuccess;

		char pclSelection[124] = "";
		CString olData;
		char pclCmd[10] = "URT";
		char pclData[2000];
		

		//ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclInsertFieldList, pclData);
		sprintf(pclSelection,"WHERE URNO=%ld",prpWro->Urno);
		MakeCedaData(&omRecInfo,olData,prpWro);
		strcpy(pclData,olData);
		ilRc = CedaAction(pclCmd,pclSelection ,"", pclData);
		if (ilRc != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaPcxData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			ogCCSDdx.DataChanged((void *)this,WRO_DELETE,(void *)prpWro);
		}

	//	ConvertDatesToLocal(prpJob);

 return ilRc;
}

void CedaWroData::UpdateMaps(WRODATA *prpWro, WRODATA *prpOldWro)
{
	if(prpWro != NULL && prpOldWro != NULL)
	{

		if(strcmp(prpWro->Wnam,prpOldWro->Wnam))
		{
			omWnamMap.RemoveKey((void * ) prpOldWro->Wnam);
			omWnamMap.SetAt((void *)prpWro->Wnam,prpWro);
		}
	}
}	

WRODATA* CedaWroData::GetWroByUrno(long lpUrno)
{
	WRODATA *prlWro = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlWro);
	return prlWro;
}

WRODATA* CedaWroData::GetWroByWnam(const char *pcpName)
{
	WRODATA *prlWro = NULL;
	omWnamMap.Lookup((void *)pcpName, (void *&) prlWro);
	return prlWro;
}


long CedaWroData::GetWroUrnoByWnam(const char *pcpName)
{
	WRODATA *prlWro = NULL;
	omWnamMap.Lookup((void *)pcpName, (void *&) prlWro);
	return prlWro->Urno;
}

CString CedaWroData::GetTableName(void)
{
	return CString(pcmTableName);
}


