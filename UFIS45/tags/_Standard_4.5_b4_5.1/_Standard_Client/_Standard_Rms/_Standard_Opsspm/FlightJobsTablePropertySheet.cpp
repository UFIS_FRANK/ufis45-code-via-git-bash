// FlightJobsTablePropertySheet.cpp : implementation file
//


// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaAltData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <FlightJobsTableSortPage.h>
#include <BasePropertySheet.h>
#include <FlightJobsTablePropertySheet.h>
#include <CedaFlightData.h>
#include <conflict.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// FlightJobsTablePropertySheet
//

FlightJobsTablePropertySheet::FlightJobsTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_FLIGHTJOBSTABLE), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageAirline);
	AddPage(&m_pageSort);
//	AddPage(&m_pageAloc);


	//m_pageAirline.SetCaption(ID_PAGE_FILTER_AIRLINE);
	m_pageAirline.SetCaption(GetString(IDS_STRING61597));
//	m_pageAloc.SetCaption(GetString(IDS_STRING61876));


	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
	m_pageAirline.bmSelectAllEnabled = true;

//	m_pageAloc.bmSelectAllEnabled = true;
}

void FlightJobsTablePropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("Airline", m_pageAirline.omSelectedItems);
	pomViewer->GetSort(m_pageSort.omSortOrders);
//	pomViewer->GetFilter("Aloc",m_pageAloc.omSelectedItems);
	m_pageSort.bmTime1Ascending = pomViewer->GetUserData("TIME1ORDER") == "YES" ? true : false;
	m_pageSort.bmTime2Ascending = pomViewer->GetUserData("TIME2ORDER") == "YES" ? true : false;
	m_pageSort.m_DisplayArr = pomViewer->GetUserData("DISPLAYARR") == "NO" ? FALSE : TRUE;
	m_pageSort.m_DisplayDep = pomViewer->GetUserData("DISPLAYDEP") == "NO" ? FALSE : TRUE;
}

void FlightJobsTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Airline", m_pageAirline.omSelectedItems);
	pomViewer->SetSort(m_pageSort.omSortOrders);
//	pomViewer->SetFilter("Aloc", m_pageAloc.omSelectedItems);
	pomViewer->SetUserData("TIME1ORDER",m_pageSort.bmTime1Ascending ? "YES" : "NO");
	pomViewer->SetUserData("TIME2ORDER",m_pageSort.bmTime2Ascending ? "YES" : "NO");
	pomViewer->SetUserData("DISPLAYARR",m_pageSort.m_DisplayArr ? "YES" : "NO");
	pomViewer->SetUserData("DISPLAYDEP",m_pageSort.m_DisplayDep ? "YES" : "NO");
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int FlightJobsTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedAlocs;
	CStringArray olSelectedGates;
	CStringArray olSelectedAirlines;
	CStringArray olSortOrders;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Airline", olSelectedAirlines);
	pomViewer->GetFilter("Aloc", olSelectedAlocs);
	pomViewer->GetSort(olSortOrders);

	bool blTime1Ascending = pomViewer->GetUserData("TIME1ORDER") == "YES" ? true : false;
	bool blTime2Ascending = pomViewer->GetUserData("TIME2ORDER") == "YES" ? true : false;
	BOOL blDisplayArr = pomViewer->GetUserData("DISPLAYARR") == "YES" ? TRUE : FALSE;
	BOOL blDisplayDep = pomViewer->GetUserData("DISPLAYDEP") == "YES" ? TRUE : FALSE;

	if (!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
		/*!IsIdentical(olSelectedAlocs, m_pageAloc.omSelectedItems) ||*/
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		m_pageSort.bmTime1Ascending != blTime1Ascending ||
		m_pageSort.bmTime2Ascending != blTime2Ascending ||
		m_pageSort.m_DisplayArr != blDisplayArr ||
		m_pageSort.m_DisplayDep != blDisplayDep)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
