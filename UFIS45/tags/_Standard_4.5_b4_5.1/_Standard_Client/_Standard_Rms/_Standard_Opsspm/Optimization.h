/////////////////////////////////////////////////////////////////////////////
// Optimization dialog
#ifndef _OPTIMIZATION_H_
#define _OPTIMIZATION_H_

#include <CedaOptData.h>

#define VORPLANUNG				1
#define EINSATZPLANUNG			2
#define CCI_PLANUNG				3


    // Meaning of "imLastReturnCode"
    enum {
        RC_SUCCESS = 0,         // Everthing OK
        RC_FAIL = -1,           // General serious error
        RC_COMM_FAIL = -2,      // WINSOCK.DLL error
        RC_INIT_FAIL = -3,      // Open the log file
        RC_CEDA_FAIL = -4,      // CEDA reports an error
        RC_SHUTDOWN = -5,       // CEDA reports shutdown
        RC_ALREADY_INIT = -6,   // InitComm called up for a second line
        RC_NOT_FOUND = -7,      // Data not found (More than 7 applications are running)
        RC_DATA_CUT = -8,       // Data incomplete
        RC_TIMEOUT = -9,        // CEDA reports a time-out
		  RC_NAME_EXIST = -10,		// The What-If already exists
		  RC_NAME_NEW  = -11			// Invalid What-If name (dummy name for new What-if)
    };


class Optimization : public CDialog
{
// Implementation
public:
	void ParasetInsert(OptDataStruct *prpOpt);
	void ParasetDelete(OptDataStruct *prpOpt);
	Optimization(CWnd* pParent = NULL);   // standard constructor
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Optimization)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


protected:
	// Generated message map functions
	//{{AFX_MSG(Optimization)
	virtual BOOL OnInitDialog();
	afx_msg void OnVorp();
	afx_msg void OnEinp();
	afx_msg void OnCcip();
	afx_msg void OnDetail();
	afx_msg void OnAllGates();
	afx_msg void OnAllStaff();
	afx_msg void OnAutomWhatIf();
	afx_msg void OnStartOptimizer();
	afx_msg void OnWifStore();
	afx_msg void OnWifDelete();
	virtual void OnCancel();
	afx_msg void OnKillfocusPara();
	afx_msg void OnTimeReset();
	afx_msg void OnWifStoreto();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int	  CheckWhatifName();
	void  GetFirstListItems();
	void  GetSecondListItems();
	void  GetCheckBoxValues();
	int   GetMainChoice();
	BOOL  StoreWhatIf(BOOL bpDoUpdate);
	void  SetOptrElements(BOOL ipValue);
	int   getNthItemOfField(char *Pstr, char *Ptrenn, int n, char *Presult);
	int   flt_get_nth_field(char *Pstr, char *Ptrenn, int n, char **PPresult);
	int   GetWhatIfLfnr();
	void Optimization::FillParaCombo();

	OPTDATA *prmOpt; 

// Construction
public:
// Dialog Data
	//{{AFX_DATA(Optimization)
	enum { IDD = IDD_OPTIMIZATION };
	CButton	m_SallButton;
	CButton	m_GallButton;
	CComboBox	m_ParaCombo;
	CEdit	m_Name;
	CListBox	m_Staf;
	CListBox	m_Gate;
	CButton	m_Vorp;
	CButton	m_Einp;
	CButton	m_Ccip;
	CTime	m_DafrDate;
	CTime	m_DatoDate;
	CTime	m_TifrTime;
	CTime	m_TitoTime;
	//}}AFX_DATA

protected:

private:
	int igLfndr;

};

#endif _OPTIMIZATION_H_