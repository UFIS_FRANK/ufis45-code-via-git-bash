// stfdiagr.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <EquipmentDiagramGroupPage.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_GROUPKEYS	2
static CString ogGroupKeys[NUMBER_OF_GROUPKEYS] = { "EquipmentGroup", "EquipmentType" };

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramGroupPage property page

IMPLEMENT_DYNCREATE(EquipmentDiagramGroupPage, CPropertyPage)

EquipmentDiagramGroupPage::EquipmentDiagramGroupPage() : CPropertyPage(EquipmentDiagramGroupPage::IDD)
{
	//{{AFX_DATA_INIT(EquipmentDiagramGroupPage)
	m_GroupBy = -1;
	m_NotDisplayUnavailable = false;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING61618);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

EquipmentDiagramGroupPage::~EquipmentDiagramGroupPage()
{
}

void EquipmentDiagramGroupPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_GroupBy = GetGroupId(omGroupBy);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EquipmentDiagramGroupPage)
	DDX_Radio(pDX, IDC_RADIO1, m_GroupBy);
	DDX_Check(pDX, IDC_DISPLAYUNAVAILABLE, m_NotDisplayUnavailable);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omGroupBy = GetGroupKey(m_GroupBy);
	}
}


BEGIN_MESSAGE_MAP(EquipmentDiagramGroupPage, CPropertyPage)
	//{{AFX_MSG_MAP(EquipmentDiagramGroupPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramGroupPage message handlers

BOOL EquipmentDiagramGroupPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return CPropertyPage::OnCommand(wParam, lParam);
}

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramGroupPage -- helper routines

int EquipmentDiagramGroupPage::GetGroupId(const char *pcpGroupKey)
{
	for (int i = 0; i < NUMBER_OF_GROUPKEYS; i++)
		if (ogGroupKeys[i] == pcpGroupKey)
			return i;

	// If there is no groupping matched, assume first groupping
	return 0;
}

CString EquipmentDiagramGroupPage::GetGroupKey(int ipGroupId)
{
	if (0 <= ipGroupId && ipGroupId <= NUMBER_OF_GROUPKEYS-1)
		return ogGroupKeys[ipGroupId];

	return "";	// invalid groupping id
}

BOOL EquipmentDiagramGroupPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	CWnd *polWnd = GetDlgItem(IDC_RADIO1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_EQSTATICGROUP));
	}
	polWnd = GetDlgItem(IDC_RADIO2); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_EQTYPE));
	}
	polWnd = GetDlgItem(IDC_DISPLAYUNAVAILABLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_DISPLAYUNAVAILABLE));
	}

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
