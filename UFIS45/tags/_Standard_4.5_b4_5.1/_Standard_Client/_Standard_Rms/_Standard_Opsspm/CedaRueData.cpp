// CedaRueData.cpp - Class for gates
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaRueData.h>
#include <BasicData.h>
#include <CedaTplData.h>

void  ProcessRueCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaRueData::CedaRueData()
{                  
    BEGIN_CEDARECINFO(RUEDATA, RueDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Runa,"RUNA")
		FIELD_CHAR_TRIM(Rusn,"RUSN")
		FIELD_LONG(Utpl,"UTPL")
		FIELD_CHAR_TRIM(Evrm,"EVRM")
		FIELD_CHAR_TRIM(Rust,"RUST")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(RueDataRecInfo)/sizeof(RueDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RueDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"RUETAB");
    pcmFieldList = "URNO,RUNA,RUSN,UTPL,EVRM,RUST";
	ogCCSDdx.Register((void *)this,BC_RUE_CHANGE,CString("RUEDATA"), CString("Rue changed"),ProcessRueCf);
}
 
CedaRueData::~CedaRueData()
{
	TRACE("CedaRueData::~CedaRueData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void  ProcessRueCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaRueData *)popInstance)->ProcessRueBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaRueData::ProcessRueBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlRueData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlRueData);
	long llUrno = GetUrnoFromSelection(prlRueData->Selection);
	if(llUrno == 0L)
	{
		RUEDATA rlRue;
		GetRecordFromItemList(&rlRue,prlRueData->Fields,prlRueData->Data);
		llUrno = rlRue.Urno;
	}

	if(llUrno != 0L)
	{
		RUEDATA *prlRue = GetRueByUrno(llUrno);

		if(ipDDXType == BC_RUE_CHANGE && prlRue != NULL)
		{
			// update
			GetRecordFromItemList(prlRue,prlRueData->Fields,prlRueData->Data);
		}
		else if(ipDDXType == BC_RUE_CHANGE && prlRue == NULL)
		{
			// insert
			RUEDATA rlRue;
			GetRecordFromItemList(&rlRue,prlRueData->Fields,prlRueData->Data);
			AddRueInternal(rlRue);
		}
	}
}

void CedaRueData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaRueData::ReadRueData(CDWordArray &ropTplUrnos)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	CString olTplUrnos, olTmp;
	int ilNumTplUrnos = ropTplUrnos.GetSize();
	for(int ilTpl = 0; ilTpl < ilNumTplUrnos; ilTpl++)
	{
		if(ilTpl != 0)
			olTplUrnos += ",";
		olTmp.Format("'%d'", ropTplUrnos[ilTpl]);
		olTplUrnos += olTmp;
	}
	///
	if(ogBasicData.IsSinApron1() == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) == 0)
			{
				olTmp.Format(",'%d'",polTplData->Urno);
				olTplUrnos += olTmp;
			}
		}
	}
	else if(ogBasicData.IsSinApron2() == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) == 0)
			{
				olTmp.Format(",'%d'",polTplData->Urno);
				olTplUrnos += olTmp;
			}
		}
	}
	///

	char pclCom[10] = "RT";
    char pclWhere[5000];
	sprintf(pclWhere, "WHERE UTPL IN (%s)", olTplUrnos);
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaRueData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		RUEDATA rlRueData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlRueData)) == RCSuccess)
			{
				AddRueInternal(rlRueData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaRueData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(RUEDATA), pclWhere);
    return ilRc;
}


void CedaRueData::AddRueInternal(RUEDATA &rrpRue)
{
	RUEDATA *prlRue = new RUEDATA;
	*prlRue = rrpRue;
	omData.Add(prlRue);
	omUrnoMap.SetAt((void *)prlRue->Urno,prlRue);
}

RUEDATA* CedaRueData::GetRueByUrno(long lpUrno)
{
	RUEDATA *prlRue = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlRue);
	return prlRue;
}


CString CedaRueData::GetTableName(void)
{
	return CString(pcmTableName);
}


CString CedaRueData::Dump(long lpUrno)
{
	CString olDumpStr;
	RUEDATA *prlRue = GetRueByUrno(lpUrno);
	if(prlRue == NULL)
	{
		olDumpStr.Format("No RUEDATA Found for RUE.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlRue);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}
	}
	return olDumpStr;
}


bool CedaRueData::LoadRuesForExtraDemands(CCSPtrArray <RUDDATA> &ropRuds)
{
	CString olUrnosAdded, olUrno;
	int ilNumRuds = ropRuds.GetSize();

	if(ilNumRuds <= 0)
		return false;

	for(int i = 0; i < ilNumRuds; i++)
	{
		RUDDATA *prlRud = &ropRuds[i];
		if(GetRueByUrno(prlRud->Urue) == NULL)
		{
			olUrno.Format("'%ld'", prlRud->Urue);
			if(olUrnosAdded.Find(olUrno) == -1)
			{
				if(!olUrnosAdded.IsEmpty())
					olUrnosAdded += ",";
				olUrnosAdded += olUrno;
			}
		}
	}


	if(!olUrnosAdded.IsEmpty())
	{
		char pclCom[10] = "RT";
		char clWhere[2000];
		sprintf(clWhere, "WHERE URNO IN (%s)", olUrnosAdded);
		int ilCount = 0;
		bool ilRc = true;
		if((ilRc = CedaAction2(pclCom, clWhere)) != RCSuccess)
		{
			ogBasicData.LogCedaError("CedaRueData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,clWhere);
		}
		else
		{
			RUEDATA rlRueData;
			for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
			{
				if ((ilRc = GetBufferRecord2(ilLc,&rlRueData)) == RCSuccess)
				{
					AddRueInternal(rlRueData);
				}
			}
			ilRc = RCSuccess;
		}
	}
	return true;
}

bool CedaRueData::GetRuesByTemplate(CCSPtrArray <RUEDATA> &ropRues, long lpTplUrno)
{
	RUEDATA  *prlRue;

	int ilCount = omData.GetSize();

	for (int ilLc=0;ilLc<ilCount;ilLc++)
	{
		prlRue = &omData[ilLc];
		if (prlRue->Utpl == lpTplUrno)
		{
			ropRues.Add(prlRue);
		}
	}

    return (ropRues.GetSize() > 0) ? true : false;
}

bool CedaRueData::GetRuesByTemplate(CCSPtrArray <RUEDATA> &ropRues, long lpTplUrno, bool bpStatus)
{
	RUEDATA  *prlRue;

	int ilCount = omData.GetSize();

	for (int ilLc=0;ilLc<ilCount;ilLc++)
	{
		prlRue = &omData[ilLc];
		if (prlRue->Utpl == lpTplUrno)
		{
			if (bpStatus && (strcmp(prlRue->Rust,"1") == 0))
			{
				ropRues.Add(prlRue);
			}
			else if (!bpStatus && (strcmp(prlRue->Rust,"1") != 0))
			{
				ropRues.Add(prlRue);
			}
		}
	}

    return (ropRues.GetSize() > 0) ? true : false;
}
