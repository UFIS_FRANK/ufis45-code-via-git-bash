// DGateJob.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <DGateJob.h>
#include <CCSTime.h>
#include <BasicData.h>
#include <AllocData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define FIRSTRADIOBUTTON (IDC_JGMIN30)
/////////////////////////////////////////////////////////////////////////////
// DGateJob dialog


DGateJob::DGateJob(CWnd* pParent , CString opAloc,CString opAlid,CString opEmpName,CTime opStartDate)
	: CDialog(DGateJob::IDD, pParent)
{

	if (opStartDate == TIMENULL)
	{
		m_Hour = ogBasicData.GetTime();
	}
	else
	{
		m_Date = CTime(opStartDate.GetYear(),opStartDate.GetMonth(),opStartDate.GetDay(),0,0,0);
		CTime olNow = ogBasicData.GetTime();
		m_Hour = m_Date + CTimeSpan(0,olNow.GetHour(),olNow.GetMinute(),0);
	}
	int ilRoundedMinute = (m_Hour.GetMinute() + 4) / 5 * 5;
	m_Hour += CTimeSpan(0, 0, ilRoundedMinute - m_Hour.GetMinute(), 0);

	if (opAloc == ALLOCUNITTYPE_GATE)
		omCaption.Format(GetString(IDS_STRING62517),opAlid);
	else if (opAloc == ALLOCUNITTYPE_CIC) 
		omCaption.Format(GetString(IDS_STRING62518),opAlid);
	else if (opAloc == ALLOCUNITTYPE_PST) 
		omCaption.Format(GetString(IDS_STRING62519),opAlid);
	else if (opAloc == ALLOCUNITTYPE_REGN) 
		omCaption.Format(GetString(IDS_STRING62520),opAlid);
	else 
		omCaption = "";


	//{{AFX_DATA_INIT(DGateJob)
	m_DurationButton = IDC_JGMIN120 - FIRSTRADIOBUTTON;
	m_Employee = opEmpName;
	m_Duration = 120;
	//}}AFX_DATA_INIT
}

BOOL DGateJob::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(omCaption); 

	CWnd *polWnd = GetDlgItem(IDC_GATEEMPLOYEE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32849));
	}
	polWnd = GetDlgItem(IDC_GATEDATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32855));
	}
	polWnd = GetDlgItem(IDC_GATETIME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32891));
	}
	polWnd = GetDlgItem(IDC_GATEDURATION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32890));
	}
	polWnd = GetDlgItem(IDC_GATEFRAME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32919));
	}
	polWnd = GetDlgItem(IDC_GATEMIN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32892));
	}
	polWnd = GetDlgItem(IDC_JGMIN30); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32893));
	}
	polWnd = GetDlgItem(IDC_JGMIN120); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32894));
	}
	polWnd = GetDlgItem(IDC_JGMIN60); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32895));
	}
	polWnd = GetDlgItem(IDC_JGSONST); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32854));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	int ilRoundedMinute = (m_Hour.GetMinute() + 2) / 5 * 5;
	m_Hour += CTimeSpan(0, 0, ilRoundedMinute - m_Hour.GetMinute(), 0);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void DGateJob::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DGateJob)
	DDX_Radio(pDX, IDC_JGMIN30, m_DurationButton);
	DDX_Text(pDX, IDC_EMPLOYEE, m_Employee);
	DDX_CCSTime(pDX, IDC_HOUR, m_Hour);
	DDX_CCSddmmyy(pDX, IDC_DATE, m_Date);
	DDX_Text(pDX, IDC_DURATION, m_Duration);
	DDV_MinMaxInt(pDX, m_Duration, 0, 1440);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DGateJob, CDialog)
	//{{AFX_MSG_MAP(DGateJob)
	ON_BN_CLICKED(IDC_JGMIN120, OnJgmin120)
	ON_BN_CLICKED(IDC_JGMIN30, OnJgmin30)
	ON_BN_CLICKED(IDC_JGMIN60, OnJgmin60)
	ON_BN_CLICKED(IDC_JGSONST, OnJgsonst)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DGateJob message handlers

void DGateJob::OnOK() 
{

	CDialog::OnOK();

	m_Hour = CTime(m_Date.GetYear(), m_Date.GetMonth(),
			m_Date.GetDay(), m_Hour.GetHour(), m_Hour.GetMinute(),
			m_Hour.GetSecond());

}

void DGateJob::OnJgmin120() 
{
	if (UpdateData() == FALSE)
		return;

	m_DurationButton= IDC_JGMIN120 - FIRSTRADIOBUTTON;
	m_Duration = 120;
	UpdateData(FALSE);
}

void DGateJob::OnJgmin30() 
{
	if (UpdateData() == FALSE)
		return;
	m_DurationButton= IDC_JGMIN30 - FIRSTRADIOBUTTON;
	m_Duration = 30;
	UpdateData(FALSE);
	
}

void DGateJob::OnJgmin60() 
{
	if (UpdateData() == FALSE)
		return;
	m_DurationButton= IDC_JGMIN60 - FIRSTRADIOBUTTON;
	m_Duration = 60;
	UpdateData(FALSE);
	
}

void DGateJob::OnJgsonst() 
{
	if (UpdateData() == FALSE)
		return;
	m_DurationButton= IDC_JGSONST - FIRSTRADIOBUTTON;
//	m_Duration = 120;
	UpdateData(FALSE);
	
}


int DGateJob::DoModal(CTime& ropStartTime,int& ripDuration)
{

	int ilRc = CDialog::DoModal();

	ropStartTime = m_Hour;
	ripDuration = m_Duration;
	return ilRc;
}
