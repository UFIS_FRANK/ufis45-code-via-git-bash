#if !defined(AFX_WORKGROUPBACKGROUNDBARS_H__F67F5997_A24D_4302_94B9_9FABEBB5B324__INCLUDED_)
#define AFX_WORKGROUPBACKGROUNDBARS_H__F67F5997_A24D_4302_94B9_9FABEBB5B324__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WorkGroupBackgroundBars.h : header file
//
#define NUMWGBKCOLOURS 16

struct WGBKCOLOURS
{
	CBrush *Colour;
	long Ulnk;

	WGBKCOLOURS(void)
	{
		Ulnk = 0L;
	}
};

/////////////////////////////////////////////////////////////////////////////
// CWorkGroupBackgroundBars window

class CWorkGroupBackgroundBars
{
public:
	CWorkGroupBackgroundBars();
	~CWorkGroupBackgroundBars();
private:
	WGBKCOLOURS omWorkGroupBkColours[NUMWGBKCOLOURS];
public:
	CBrush *GetWorkGroupBkColour(long lpUlnk);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKGROUPBACKGROUNDBARS_H__F67F5997_A24D_4302_94B9_9FABEBB5B324__INCLUDED_)
