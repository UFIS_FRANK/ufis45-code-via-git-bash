// SelPoolDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <SelPoolDlg.h>
#include <Ccsglobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelPoolDlg dialog


CSelPoolDlg::CSelPoolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelPoolDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelPoolDlg)
	m_Title = _T("");
	//}}AFX_DATA_INIT
}


void CSelPoolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelPoolDlg)
	DDX_Control(pDX, IDC_SELPOOL, m_SelPool);
	DDX_Text(pDX, IDC_TITLE, m_Title);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelPoolDlg, CDialog)
	//{{AFX_MSG_MAP(CSelPoolDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelPoolDlg message handlers

BOOL CSelPoolDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33026));
	CWnd *polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	UpdateData(FALSE);
	int ilNumPools = pomPools->GetSize();
	for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
	{
		m_SelPool.AddString(pomPools->GetAt(ilPool));
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelPoolDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		int ilSel = m_SelPool.GetCurSel();
		if(ilSel != LB_ERR && ilSel >= 0 && ilSel < pomPools->GetSize())
		{
			//omPool = pomPools->GetAt(ilSel);
			m_SelPool.GetText(ilSel,omPool);
			CDialog::OnOK();
		}
	}
}
