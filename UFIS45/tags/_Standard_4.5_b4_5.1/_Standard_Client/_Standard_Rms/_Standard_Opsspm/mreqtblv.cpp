// ppviewer.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <ccsddx.h>
#include <CedaAloData.h>
#include <BasicData.h>
#include <CedaRequestData.h>
#include <mreqtblv.h>
#include <dataset.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
    BY_RDAT,
    BY_RQDA,
    BY_USERFROM,
    BY_USERDEST,
	BY_NONE
};


// Local function prototype
static void RequestTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

RequestTableViewer::RequestTableViewer()
{
    SetViewerKey("ReqTab");
    pomTable = NULL;
    ogCCSDdx.Register(this, REQUEST_NEW, CString("MREQTBLV"), CString("Request New"), RequestTableCf);
    ogCCSDdx.Register(this, REQUEST_CHANGE, CString("MREQTBLV"), CString("Request Change"), RequestTableCf);
    ogCCSDdx.Register(this, REQUEST_DELETE, CString("MREQTBLV"), CString("Request Delete"), RequestTableCf);
}

RequestTableViewer::~RequestTableViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void RequestTableViewer::Attach(CTable *popTable,CString opTableType)
{
    pomTable = popTable;
	omTableType = opTableType;
}

void RequestTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.RQSV;
    SelectView(pcpViewName);
    PrepareSorter();
    PrepareGrouping();

    DeleteAll();    // remove everything
    MakeLines();

    UpdateDisplay();
}

void RequestTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

// Create an array of sort order (but using enumerated value for speed up the performance)
    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Erfassungsdatum")
            ilSortOrderEnumeratedValue = BY_RDAT;
        else if (olSortOrder[i] == "Datum")
            ilSortOrderEnumeratedValue = BY_RQDA;
        else if (olSortOrder[i] == "Erfasst von")
            ilSortOrderEnumeratedValue = BY_USERFROM;
        else if (olSortOrder[i] == "Betrifft")
            ilSortOrderEnumeratedValue = BY_USERDEST;
        else
            ilSortOrderEnumeratedValue = BY_NONE;
        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

void RequestTableViewer::PrepareGrouping()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);  // we'll always use the first sorting criteria for grouping
	BOOL blIsGrouped = *CViewer::GetGroup() - '0';

    omGroupBy = BY_NONE;
    if (olSortOrder.GetSize() > 0 && blIsGrouped)
    {
        if (olSortOrder[0] == "Erfassungsdatum")
            omGroupBy = BY_RDAT;
        else if (olSortOrder[0] == "Datum")
            omGroupBy = BY_RQDA;
        else if (olSortOrder[0] == "Erfasst von")
            omGroupBy = BY_USERFROM;
        else if (olSortOrder[0] == "Betrifft")
            omGroupBy = BY_USERDEST;
    }
}

BOOL RequestTableViewer::IsPassFilter(CTime opAcfr,CTime opActo)
{
	BOOL blIsTimeOk = (opAcfr < omEndTime && opActo > omStartTime);

    return (blIsTimeOk);
}


/////////////////////////////////////////////////////////////////////////////
// RequestTableViewer -- code specific to this class

void RequestTableViewer::MakeLines()
{
	CedaRequestData *polData;
	if (omTableType == "R")
		polData = &ogRequests;
	else
		polData = &ogInfos;

	int ilRequestCount = polData->omData.GetSize();
	for (int ilLc = 0; ilLc < ilRequestCount; ilLc++)
	{
		if (omTableType == polData->omData[ilLc].Stat)
			MakeLine(&polData->omData[ilLc]);
	}
}

void RequestTableViewer::MakeLine(REQUESTDATA *prpRequest)
{

//	if ((prpRequest->Acfr < omEndTime) && (prpRequest->Acto > omStartTime))
	{
		REQUEST_LINEDATA rlLine;
		EMPDATA *prlEmpFrom = ogEmpData.GetEmpByPeno(prpRequest->Cusr);
		EMPDATA *prlEmpTo   = ogEmpData.GetEmpByPeno(prpRequest->Peno);

        rlLine.UserFrom =  (prlEmpFrom != NULL ? CString(prlEmpFrom->Lanm) : "");
		rlLine.UserFrom += ",";
        rlLine.UserFrom +=  (prlEmpFrom != NULL ? CString(prlEmpFrom->Finm) : "");

        rlLine.UserTo =  (prlEmpTo != NULL ? CString(prlEmpTo->Lanm) : "");
		rlLine.UserTo += ",";
        rlLine.UserTo +=  (prlEmpTo != NULL ? CString(prlEmpTo->Finm) : "");


		rlLine.Urno = prpRequest->Urno;        
		rlLine.Stat = prpRequest->Stat;        
		rlLine.Rdat = prpRequest->Rdat;		
		rlLine.Cusr = prpRequest->Cusr;       
		rlLine.Adat = prpRequest->Adat;		
		rlLine.Rqda = prpRequest->Rqda;		
		rlLine.Peno = prpRequest->Peno;        
		rlLine.Text = prpRequest->Text;

        CreateLine(rlLine);
    }
}

BOOL RequestTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
        if (lpUrno == omLines[rilLineno].Urno)
            return TRUE;
    return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// RequestTableViewer - REQUEST_LINEDATA array maintenance

void RequestTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


int RequestTableViewer::CompareLine(REQUEST_LINEDATA &rrpLine1,REQUEST_LINEDATA &rrpLine2)
{

    // Compare in the sort order, from the outermost to the innermost
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_RDAT:
            ilCompareResult = (rrpLine1.Rdat == rrpLine2.Rdat)? 0:
                (rrpLine1.Rdat > rrpLine2.Rdat)? 1: -1;
            break;
        case BY_RQDA:
            ilCompareResult = (rrpLine1.Rqda == rrpLine2.Rqda)? 0:
                (rrpLine1.Rqda > rrpLine2.Rqda)? 1: -1;
            break;
        case BY_USERFROM:
            ilCompareResult = (rrpLine1.UserFrom == rrpLine2.UserFrom)? 0:
                (rrpLine1.UserFrom > rrpLine2.UserFrom)? 1: -1;
            break;
        case BY_USERDEST:
            ilCompareResult = (rrpLine1.UserTo == rrpLine2.UserTo)? 0:
                (rrpLine1.UserTo > rrpLine2.UserTo)? 1: -1;
            break;
        // more sorting condition should be here, as case ...
        }

        // Check the result of this sorting order, return if unequality is found
        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   // we can say that these two group are eqaul

}

int RequestTableViewer::CreateLine(REQUEST_LINEDATA &rrpLine)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLine(rrpLine, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rrpLine);

    return ilLineno;
}

void RequestTableViewer::DeleteLine(int ipLineno)
{
    omLines.DeleteAt(ipLineno);
}


/////////////////////////////////////////////////////////////////////////////
// RequestTableViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void RequestTableViewer::UpdateDisplay()
{
    DeleteAll();    // remove everything
    MakeLines();

    // Set table header and format

    //pomTable->SetHeaderFields("Erfasst|von|f�r|Mitarbeiter|Betrifft");
    pomTable->SetHeaderFields(GetString(IDS_STRING61575));
    pomTable->SetFormatList("7|20|7|20|100");

    // Load filtered and sorted data into the table content
    pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
    {
        pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
    }

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString RequestTableViewer::Format(REQUEST_LINEDATA *prpLine)
{

    CString olText = prpLine->Rdat.Format("%d%m%y") + "|";
        olText +=  prpLine->UserFrom + "|";
		olText +=  prpLine->Rqda.Format("%d%m%y") + "|";
        olText +=  prpLine->UserTo + "|";
		olText +=  prpLine->Text;

    return olText;
}


/////////////////////////////////////////////////////////////////////////////
// RequestTableViewer Requests creation methods

static void RequestTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RequestTableViewer *polViewer = (RequestTableViewer *)popInstance;

    if (ipDDXType == REQUEST_NEW)
        polViewer->ProcessRequestChange((REQUESTDATA *)vpDataPointer);
	else
    if (ipDDXType == REQUEST_CHANGE)
        polViewer->ProcessRequestChange((REQUESTDATA *)vpDataPointer);
	else
    if (ipDDXType == REQUEST_DELETE)
        polViewer->ProcessRequestDelete((REQUESTDATA *)vpDataPointer);

}


BOOL RequestTableViewer::FindItemByUrno(REQUESTDATA *prpRequest,int& ripItem)
{
	int ilCount = omLines.GetSize();
    for (ripItem = 0; ripItem < ilCount; ripItem++)
    {
		if (omLines[ripItem].Urno == prpRequest->Urno)
				return TRUE;
	}
	return FALSE;
}

void RequestTableViewer::ProcessRequestDelete(REQUESTDATA *prpRequest)
{
	// If it's not a request (R) or info (I) return
	if (omTableType != prpRequest->Stat)
		return;

	int ilItem;
	if (FindItemByUrno(prpRequest,ilItem))
	{
		{
			omLines.DeleteAt(ilItem);
			pomTable->DeleteTextLine(ilItem);
			pomTable->DisplayTable();
		}
	}
}

void RequestTableViewer::ProcessRequestChange(REQUESTDATA *prpRequest)
{
	// If it's not a request (R) or info (I) return
	if (omTableType != prpRequest->Stat)
		return;

    int ilItem;
    if (FindLine(prpRequest->Urno, ilItem))  // request already displayed ?
	{

		omLines[ilItem].Rdat = prpRequest->Rdat;		
		omLines[ilItem].Cusr = prpRequest->Cusr;       
		omLines[ilItem].Adat = prpRequest->Adat;		
		omLines[ilItem].Rqda = prpRequest->Rqda;		
		omLines[ilItem].Peno = prpRequest->Peno;        
		omLines[ilItem].Text = prpRequest->Text;

        pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
    }
	else
	{
		MakeLine(prpRequest);
		if (FindLine(prpRequest->Urno, ilItem))
		{
			pomTable->InsertTextLine(ilItem,Format(&omLines[ilItem]), &omLines[ilItem]);
			pomTable->DisplayTable();
		}
	}

}

