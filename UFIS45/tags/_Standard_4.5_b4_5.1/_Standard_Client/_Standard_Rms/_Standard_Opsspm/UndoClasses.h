// UndoClasses.h - implementation of the Undo/Redo objects in the project
//
// Description:
// For each Undo/Redo types, assigning some staff from PrePlanTable to the StaffDiagram
// for example, we will implement one class which will be derived from the UndoObject
// to handle it. See the module UndoManager for more explanation.
//
//
// Written by:
// Damkerng Thammathakerngkit	Sep 18, 1996

#ifndef _UNDOCLASSES_H_
#define _UNDOCLASSES_H_

#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaDlgData.h>
#include <CedaCcaData.h>
#include <UndoManager.h>

/////////////////////////////////////////////////////////////////////////////
// JobCreationInfo
//
// This class will be used for storing all information related to one job
// deletion. A single job deletion may not simple as first thought, since
// that job may be a pool job or detach job which may have so many other
// jobs hooked with it. Or else, that job may be a flight job which may has
// some JODDATA associated with.
//
// So this class is designed specially for generic job deletion. Let's say
// that it will contain an array of jobs

class JobCreationInfo: public CObject
{
public:
	~JobCreationInfo();
	const JobCreationInfo &operator =(const JobCreationInfo &ropSourceInfo);

	struct JobCreationStep
	{
		JOBDATA Job;
		CCSPtrArray<JODDATA> Jods;
	};
	CCSPtrArray<JobCreationStep> omCreationSteps;

// Routines for maintaining job creation
// The parameter "bpAppend" is a little bit tricky. Since I want to extend
// the capability of this JobCreationInfo to handle both creation and deletion,
// even though we use only for deletion in this moment.
// This flag will control the direction of the creation of job (from first to last
// or from last to first).
// As a rule of thumb, we should use AddAnother...(..., TRUE) if we like to store information
// about job creation in the order of creating. And we should use AddAnother...(..., FALSE)
// if we like to store information about job creation in the order of deleting (since we
// will delete the job in the backward order).
public:
	void AddAnotherJob(JOBDATA *prpJob, BOOL bpAppend = TRUE);	// create another "JobCreationStep"
	void AddAnotherJod(JODDATA *prpJod, BOOL bpAppend = TRUE);	// create another JODDATA in the last step
	BOOL CreateJobs(CWnd *popParentWnd, const CString &ropText);
	BOOL DeleteJobs(CWnd *popParentWnd, const CString &ropText);

private:
	// this method can be used to recover error from the deletion
	void InternalCreateJobs(int ipStartStepno = 0, int ipStartJodno = 0);
};

/////////////////////////////////////////////////////////////////////////////
// JobConfirmInfo
//
// This class will be used for storing all information related to one job
// confirmation. A single job confirmation may not simple as first thought, since
// that job may be a pool job or detach job which may have so many other
// jobs hooked with it. 
//
// So this class is designed specially for generic job confirmation. Let's say
// that it will contain an array of jobs

class JobConfirmInfo : public CObject
{
public:
	~JobConfirmInfo();
	const JobConfirmInfo &operator =(const JobConfirmInfo &ropSourceInfo);

// Internal data
private:
	struct JobConfirmStep
	{
		JOBDATA rmOldJob;
		JOBDATA rmNewJob;
	};

	CCSPtrArray<JobConfirmStep> omConfirmSteps;

public:
	void AddAnotherJob(JOBDATA *prpOldJob, JOBDATA *prlNewJob,BOOL bpAppend = TRUE);	// create another "JobConfirmStep"
	int		Jobs();
	CTime	MinStartTime();
	CTime	MaxEndTime();
private:
	friend class UndoConfirmJob;
	BOOL Undo(CWnd *popParentWnd, const CString &ropText);
	BOOL Redo(CWnd *popParentWnd, const CString &ropText);

};

/////////////////////////////////////////////////////////////////////////////
// UndoCreatePoolJob

class UndoCreatePoolJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoCreatePoolJob(const JOBDATA *prpPoolJob);
private:
	JOBDATA rmPoolJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoChangePoolJob

class UndoChangePoolJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoChangePoolJob(const JOBDATA *prpOldPoolJob, const JOBDATA *prpNewPoolJob);
private:
	JOBDATA rmOldPoolJob;
	JOBDATA rmNewPoolJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

class UndoChangeTimeOfJob : public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoChangeTimeOfJob(JOBDATA *prpOldJob,CTime opNewStart,CTime opNewEnd);
private:
	long		lmUrno;			// job urno
	CTime		omOldTime[2];	// the old times
	CTime		omNewTime[2];	// the new times
	CString		omUndoText;		// pre-created text which will be displayed in the Undo/Redo dialog
};

class UndoList : public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoList(UndoObject* popObject);
	virtual	~UndoList();
	void	Add(UndoObject *popObject);		
private:
	CCSPtrArray<UndoObject>	omList;
	CString		omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoCreateFmgJob

class UndoCreateFmgJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoCreateFmgJob(const JOBDATA *prpFmgJob, const JOBDATA *prpPoolJob);
private:
	JOBDATA rmFmgJob;
	JOBDATA rmPoolJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoCreateDetachJob

class UndoCreateDetachJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoCreateDetachJob(const JOBDATA *prpDetachJob, const JOBDATA *prpPoolJob);
private:
	JOBDATA rmDetachJob;
	JOBDATA rmPoolJob;
	DLGDATA rmDlg;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};



/////////////////////////////////////////////////////////////////////////////
// UndoCreateGroupDelegation

class UndoCreateGroupDelegation: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoCreateGroupDelegation(const JOBDATA *prpDetachJob, const JOBDATA *prpPoolJob, const DLGDATA *prpDlg);
private:
	JOBDATA rmDetachJob;
	JOBDATA rmPoolJob;
	DLGDATA rmDlg;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};



/////////////////////////////////////////////////////////////////////////////
// UndoReassignJob

class UndoReassignJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoReassignJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob);
private:
	JOBDATA rmOldJob;
	JOBDATA rmNewJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};


/////////////////////////////////////////////////////////////////////////////
// UndoReassignEquJob

class UndoReassignEquJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoReassignEquJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob);
private:
	JOBDATA rmOldJob;
	JOBDATA rmNewJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};


/////////////////////////////////////////////////////////////////////////////
// UndoCreateNormalJob

class UndoCreateNormalJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoCreateNormalJob(const CString &ropJobDescription,
		const JOBDATA *prpJob, const JODDATA *prpJod = NULL);
private:
	BOOL bmIsThisJobHasJod;
	JOBDATA rmJob;
	JODDATA rmJod;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoConfirmJob

class UndoConfirmJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoConfirmJob(const CString &ropDescription,const JobConfirmInfo& ropJobConfirmInfo);
private:
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
	JobConfirmInfo omJobConfirmInfo;
};

/////////////////////////////////////////////////////////////////////////////
// UndoSetJobStandby

class UndoSetJobStandby: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoSetJobStandby(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob);
private:
	JOBDATA rmOldJob;
	JOBDATA rmNewJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoSetCoffeeBreak

class UndoSetCoffeeBreak: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoSetCoffeeBreak(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob);
private:
	JOBDATA rmOldJob;
	JOBDATA rmNewJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoFinishJob

class UndoFinishJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoFinishJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob);
private:
	JOBDATA rmOldJob;
	JOBDATA rmNewJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoEmpInformedOfJob

class UndoEmpInformedOfJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoEmpInformedOfJob(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob);
private:
	JOBDATA rmOldJob;
	JOBDATA rmNewJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoDeleteJob

class UndoDeleteJob: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoDeleteJob(const CString &ropDescription, JobCreationInfo *popJobCreationInfo);
private:
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
	JobCreationInfo omJobCreationInfo;
};

/////////////////////////////////////////////////////////////////////////////
// UndoReassignJobToNewDemand

class UndoReassignJobToNewDemand: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	BOOL Undo(CWnd *popParentWnd);
	BOOL Redo(CWnd *popParentWnd);
	UndoReassignJobToNewDemand(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob, const DEMANDDATA *prpOldDem,  const DEMANDDATA *prpNewDem);
private:
	JOBDATA rmOldJob,rmNewJob;
	DEMANDDATA rmOldDem, rmNewDem;
	CString omUndoText;
};

/////////////////////////////////////////////////////////////////////////////
// UndoReassignDemandAlid

class UndoReassignDemandAlid: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
				UndoReassignDemandAlid(const DEMANDDATA *prpOldDemand,const CString& ropNewAlid);
				UndoReassignDemandAlid(const UndoReassignDemandAlid& rhs);
	UndoReassignDemandAlid&	operator=(const UndoReassignDemandAlid& rhs);
	virtual	~	UndoReassignDemandAlid();
	BOOL		AddOldCcaRecord(const CCADATA *prpCca);
	BOOL		AddNewCcaRecord(const CCADATA *prpCca);
private:
	long					lmDemUrno;
	CCSPtrArray<CCADATA>	omOldCcaList;	
	CCSPtrArray<CCADATA>	omNewCcaList;	
	CString					omOldAlid;
	CString					omNewAlid;
	CString					omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoDemandAutoAssignment

class UndoDemandAutoAssignment: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	BOOL Undo(CWnd *popParentWnd);
	BOOL Redo(CWnd *popParentWnd);
	UndoDemandAutoAssignment(DEMANDDATA *prpOldDemand,bool blNewAutoAssign);
private:
	long	lmDemUrno;
	bool	bmOldAutoAssignment;
	bool	bmNewAutoAssignment;
	CString omUndoText;
};



/////////////////////////////////////////////////////////////////////////////
// UndoDeactivateDemand

class UndoDeactivateDemand: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	BOOL Undo(CWnd *popParentWnd);
	BOOL Redo(CWnd *popParentWnd);
	UndoDeactivateDemand(DEMANDDATA *prpOldDemand,bool blNewDeactivateDemand);
private:
	long	lmDemUrno;
	bool	bmOldDeactivateDemand;
	bool	bmNewDeactivateDemand;
	CString omUndoText;
};


/////////////////////////////////////////////////////////////////////////////
// UndoCreateEquipmentJob

class UndoCreateEquipmentJob: public UndoObject
{
public:
	UndoCreateEquipmentJob(const JOBDATA *prpJob, const JODDATA *prpJod = NULL);
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

private:
	BOOL	bmIsThisJobHasJod;
	JOBDATA rmJob;
	JODDATA rmJod;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};


/////////////////////////////////////////////////////////////////////////////
// UndoCreateEquipmentFastLink

class UndoCreateEquipmentFastLink: public UndoObject
{
public:
	UndoCreateEquipmentFastLink(const JOBDATA *prpJob);
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

private:
	JOBDATA rmJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

/////////////////////////////////////////////////////////////////////////////
// UndoEmpAbsent

class UndoEmpAbsent: public UndoObject
{
public:
	virtual CString UndoText() const;	// return text which will be shown in Undo dialog
	virtual BOOL Undo(CWnd *popParentWnd);	// do the necessary things for undoing
	virtual BOOL Redo(CWnd *popParentWnd);	// do the necessary things for redoing

public:
	UndoEmpAbsent(const JOBDATA *prpOldJob, const JOBDATA *prpNewJob);
private:
	JOBDATA rmOldJob;
	JOBDATA rmNewJob;
	CString omUndoText;	// pre-created text which will be displayed in the Undo/Redo dialog
};

typedef struct
{
	CString Description;
	CTime	Time;
	long	Urno;
} OFFLINE_DESCRIPTION;

class COfflineDescription
{
public:
	COfflineDescription(void);
	~COfflineDescription(void);
	void SetOfflineDescription(long lpUrno, CString &ropDescription);
	void GetOfflineDescriptions(CCSPtrArray <OFFLINE_DESCRIPTION> &ropOfflineDescriptions, CStringArray &ropErrors);
	OFFLINE_DESCRIPTION *GetOfflineDescription(long lpUrno);
	void ClearAll(void);

private:

	CMapPtrToPtr omOfflineDescriptions;
};
extern COfflineDescription ogOfflineDescription;


#endif // _UNDOCLASSES_H_

