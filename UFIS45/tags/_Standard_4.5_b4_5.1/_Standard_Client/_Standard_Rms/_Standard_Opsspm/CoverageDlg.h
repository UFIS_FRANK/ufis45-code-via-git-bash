

#ifndef COVERDLG_H
#define COVERDLG_H


#include <CoverageViewer.h> 
#include <Coverage.h>
#include <CCSTree.h>


//Defines for the main choice
	#define GCI											99
	#define CCI											98

//ID's
	#define IDC_CANCEL								1
	#define IDC_VIEWBUTTON							2
	#define IDC_ACTUALIZE							3
	#define IDC_GCI									4
	#define IDC_CCI									5

class CCoverageDlg : public CDialog
{
// Construction
public:
	CCoverageDlg(CWnd* pParent = NULL);   // standard constructor
	void SetStartTime(CTime opStartTime);
	void Create(CTime opStartTime = TIMENULL);


	// Generated message map functions
	//{{AFX_MSG(CCoverageDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnPaint();
	afx_msg void OnAnsicht();
	afx_msg void OnGCI();
	afx_msg void OnCCI();
	afx_msg void OnActualize();
   afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoverageDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	BOOL CreateDlgButtons();

//Attributes
public:
	//{{AFX_DATA(CCoverageDlg)
	enum { IDD = IDD_COVERDLG };
	CButton	m_TreeGroupBox;
	CButton	m_GraphGroupBox;
	//}}AFX_DATA

protected:
	CButton omCCIButton;
	CButton omGCIButton;

private:
	CoverageViewer	omViewer;
	CCSTree			*pomTree;
	CCoverage		*pomCoverage;
	CTime				omStartTime;
	int				imCoverageSize;
	int				imCoverageWidth;
	int				imCoverageHeight;
	int				imTreeWidth;
	int				imTreeHeight;
	int				imGraphWidth;
	int				imGraphHeight;
	int				imIntervallTime;
	int				imMainChoice;
	CWnd				*omParent;
};

#endif
