#ifndef _PREPLANTTEAM_H_
#define _PREPLANTTEAM_H_

#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CCSDragDropCtrl.h>
#include <PrintControl.h>
#include <PrePlanTableTeamViewer.h>
#include <afxtempl.h>

/////////////////////////////////////////////////////////////////////////////
// PrePlanTeamTable frame

class PrePlanTeamTable: public CDialog
{
// Construction
public:
    PrePlanTeamTable(CWnd* pParent,CString opTeamID = "", CCSPtrArray<PREPLANT_SHIFTDATA> *popTeamMember = NULL, int ipItemHeigth = 10);
	~PrePlanTeamTable();

	CTime GetPrePlanTime(void);
	CCSPtrArray<PREPLANT_SHIFTDATA> *pomTeamMember;
// Private Attributes
private:
    CString omDate;
	CWnd *pomParent;
	CString omTeamID;
	int imItemHeight;

    CTable	omTable;		// visual object, the table content
	long	omContextItem;	// Shift urno of selected line
	CArray<long,long> omAssignUrnos;	// Job urnos of selected line	
    CTime	lmDate;			// the date of the selected and displayed EMPDATA in the table window

	PrePlanTableTeamViewer omViewer;

	// Definition of drag-and-drop object
	CCSDragDropCtrl omDragDropObject;
	void DeleteAssignment(int ipIndex);

// Private Methods
private:
    // Helper functions for displaying data in the table content
    void UpdateView();
	static void PrePlanTeamTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);

// Dialog Data
public:
    int m_nDialogBarHeight;

    //{{AFX_DATA(PrePlanTeamTable)
    enum { IDD = IDD_PREPLANTEAMTABLE };
    CButton m_UpdateButton;
    CButton m_InsertButton;
    BOOL    m_bCompleteTeam;
    //}}AFX_DATA

// Implementation
public:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL DestroyWindow(); 
	void SelectNewTeam(CString opTeamID, CCSPtrArray<PREPLANT_SHIFTDATA> *popTeamMember, int ipItemHeight);

    // Generated message map functions
    //{{AFX_MSG(PrePlanTeamTable)
    virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
    virtual void OnCancel();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblClk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnMenuDelete(UINT nID);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

#endif //_PREPLANTTEAM_H_
