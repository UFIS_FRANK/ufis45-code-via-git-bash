#ifndef __BREAKTABLEVIEWER_H__
#define __BREAKTABLEVIEWER_H__

#include <CCSPtrArray.h>
#include <CViewer.h>
#include <CCSTable.h>

struct BREAKTABLEDATA_LINE
{
	long		ShiftUrno;
	CString		EmpName;

	long		BreakUrno;
	CTime		BreakStart;
	CTime		BreakEnd;

	bool		JobOverlapsBreak;
	bool		JobFound;

	CTime		JobStart;
	CTime		JobEnd;
	CString		JobStatus;
	CString		JobText;

	BREAKTABLEDATA_LINE()
	{
		ShiftUrno			= 0L;
		BreakUrno			= 0L;
		BreakStart			= TIMENULL;
		BreakEnd			= TIMENULL;
		JobOverlapsBreak	= false;
		JobFound			= false;
		JobStart			= TIMENULL;
		JobEnd				= TIMENULL;
	}

//	BREAKTABLEDATA_LINE(const BREAKTABLEDATA_LINE& rhs)
//	{
//		*this = rhs;		
//	}

//	BREAKTABLEDATA_LINE& BREAKTABLEDATA_LINE::operator=(const BREAKTABLEDATA_LINE& rhs)
//	{
//		if (&rhs != this)
//		{
//			ShiftUrno = rhs.ShiftUrno;
//			EmpName = rhs.EmpName;
//			BreakUrno = rhs.BreakUrno;
//			BreakStart = rhs.BreakStart;
//			BreakEnd = rhs.BreakEnd;
//			JobStart = rhs.JobStart;
//			JobEnd = rhs.JobEnd;
//			JobStarted = rhs.JobStarted;
//		}		
//		return *this;
//	}
};

struct BREAKTABLEDATA_FIELD
{
	CString	Field;
	CString	Name;
	int		Length;
	int		PrintLength;

	BREAKTABLEDATA_FIELD(const char *pcpField,const char *pcpName,int ipLength,int ipPrintLength)
	{
		Field = pcpField;
		Name  = pcpName;
		Length= ipLength;
		PrintLength = ipPrintLength; 
	};

	BREAKTABLEDATA_FIELD()
	{
		Length		= 0;
		PrintLength = 0;
	};
};

class	CCSTable;
struct	TABLE_COLUMN;

class BreakTableViewer: public CViewer
{
public:
	BreakTableViewer();
    ~BreakTableViewer();

	bool bmDisplayNonOverlappingOnly;

	void Attach(CCSTable *popTable);
	void ChangeViewTo(const char *pcpViewName);
	void DeleteAll();
	int  CompareBreakStart(BREAKTABLEDATA_LINE *prpLine1, BREAKTABLEDATA_LINE *prpLine2);
	BOOL IsPassFilter(JOBDATA *prpJob);
	bool bmUseAllShiftCodes, bmUseAllRanks, bmUseAllPermits, bmUseAllTeams, bmUseAllPools, bmEmptyTeamSelected;
	CMapStringToPtr omCMapForShiftCode, omCMapForRank, omCMapForPermits, omCMapForTeam, omCMapForPool;
	void PrepareFilter(void);
	bool IsPassFilter(const char *pcpPoolId, const char *pcpTeamId,	const char *pcpShiftCode, const char *pcpRank, bool bpIsAbsent, CStringArray &ropPermits);
	void MakeLines();
	void MakeLine(JOBDATA *prpBreakJob, bool bpUpdateTable = false);
	void MakeLineData(JOBDATA *prpBreakJob, BREAKTABLEDATA_LINE &ropLine);
	void GetNextJobData(JOBDATA *prpBreakJob, BREAKTABLEDATA_LINE &ropLine);
	int  CreateLine(BREAKTABLEDATA_LINE *prpLine);
	void DeleteLine(int ipLineno);
	void UpdateDisplay(void);
	void Format(BREAKTABLEDATA_LINE *prpLine, CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	bool SetColumn(const char *pcpFieldName, const char *pcpFieldValue, CCSPtrArray<TABLE_COLUMN>& rrpColumns);
	bool EvaluateTableFields(void);
	int  TableFieldIndex(const CString& ropField);
	CTime StartTime() const;
	CTime EndTime() const;
	int  LineCount() const;
	void SetStartTime(CTime opStartTime);
	void SetEndTime(CTime opEndTime);
	BREAKTABLEDATA_LINE *GetLine(int ipLineNo);
	void ProcessJobChange(JOBDATA *prpJob);
	void DeleteLinesByShiftUrno(long lpShiftUrno);
	void InsertLinesForShiftUrno(long lpShiftUrno);

private:
	static void BreakTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);

private:
    CCSTable			*pomTable;
  
	CCSPtrArray <BREAKTABLEDATA_LINE> omLines;
	CCSPtrArray <BREAKTABLEDATA_FIELD> omTableFields;

	CTime			omStartTime;
	CTime			omEndTime;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeader;
	CCSPtrArray <TABLE_COLUMN> omColumns;
	void CreateTableColumnsAndHeader();
};

#endif //__BREAKTABLEANDTABLEVIEWER_H__