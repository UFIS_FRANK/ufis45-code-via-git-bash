// PRMFlightSearch.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSTime.h>
#include <PRMFlightSearch.h>
#include <FilterData.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PRMFlightSearch property page

//IMPLEMENT_DYNCREATE(PRMFlightSearch, CPropertyPage)

//int PRMFlightSearch::imLastState = -1;

PRMFlightSearch::PRMFlightSearch(CWnd* pParent )
	: CDialog(PRMFlightSearch::IDD, pParent)
{	
//	imLastState = -1;
	//{{AFX_DATA_INIT(PRMFlightSearch)
	m_Airline = _T("");
	m_Flightno = _T("");
	m_SingleFlight = 1;
	//}}AFX_DATA_INIT
//	m_Date = ogBasicData.GetTime();
//Tab change start
	omTitle = GetString(IDS_STRING61609);
//	m_psp.pszTitle = omTitle;
//	m_psp.dwFlags |= PSP_USETITLE;
//Tab change stop

	//m_SingleFlight = FlightDetailDisplayAsDefault();
}

PRMFlightSearch::PRMFlightSearch()
{
}

PRMFlightSearch::~PRMFlightSearch()
{
}

/***
int PRMFlightSearch::FlightDetailDisplayAsDefault() const
{
	if (imLastState == -1)
	{
		char pclConfigPath[512];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));
		char pclDetailDisplay[512];
		GetPrivateProfileString(pcgAppName, "FLIGHTDETAILDISPLAY", "YES",pclDetailDisplay, sizeof pclDetailDisplay, pclConfigPath);
		if (stricmp(pclDetailDisplay,"YES") != 0)
			imLastState = 0;
		else
			imLastState = 1;
	}

	return imLastState;
}
***/

void PRMFlightSearch::DoDataExchange(CDataExchange* pDX)
{
//	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PRMFlightSearch)
	DDX_Text(pDX, IDC_AIRLINE, m_Airline);
	DDV_MaxChars(pDX, m_Airline, 3);
//	DDX_CCSddmmyy(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Text(pDX, IDC_FLIGHTNR, m_Flightno);
	DDV_MaxChars(pDX, m_Flightno, 5);
	DDX_Radio(pDX,IDC_GANTT_SELECT,m_SingleFlight);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PRMFlightSearch, CDialog)
	//{{AFX_MSG_MAP(PRMFlightSearch)
	ON_EN_KILLFOCUS(IDC_FLIGHTNR, OnKillfocusFlightnr)
	ON_BN_CLICKED(IDC_GANTT_SELECT, OnRadioGanttSelect)
	ON_BN_CLICKED(IDC_DETAIL_DISPLAY, OnRadioDetailDisplay)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PRMFlightSearch message handlers

void PRMFlightSearch::OnKillfocusFlightnr() 
{
	char pclStr[256]="";
	char pclTmp[256]="";
	GetDlgItemText(IDC_FLIGHTNR, pclStr, 100);
	if(strlen(pclStr) != (size_t)0)
	{
		sprintf(pclTmp, "%05s", pclStr);
	}
	SetDlgItemText(IDC_FLIGHTNR, pclTmp);
	
}

BOOL PRMFlightSearch::OnInitDialog() 
{
	char pclStr[256];
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_FLIGHTALT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32973));
	}
	polWnd = GetDlgItem(IDC_FLIGHTNUM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32974));
	}
	polWnd = GetDlgItem(IDC_FLIGHTDATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32855));
	}

	polWnd = GetDlgItem(IDC_FLIGHT_GROUP);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61904));
	}
	polWnd = GetDlgItem(IDC_GANTT_SELECT);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61905));
	}
	polWnd = GetDlgItem(IDC_DETAIL_DISPLAY);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61906));
	}

	m_Airline = CString(ogSearchFilter.rlFlightFilterData.Airline);
	m_Flightno = ogSearchFilter.rlFlightFilterData.Flno;

	
//	if(ogSearchFilter.rlFlightFilterData.Date != -1)
//	{
//		m_Date = ogSearchFilter.rlFlightFilterData.Date;
//	}
//	else
//	{
//		m_Date = ogBasicData.GetTime();
//	}

	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}


	int ilDisplayDateOffset = ogBasicData.GetDisplayDateOffsetByDate(ogSearchFilter.rlFlightFilterData.Date);
	if(ilDisplayDateOffset >= 0 && ilDisplayDateOffset < ilNumDays)
	{
		m_Date.SetCurSel(ilDisplayDateOffset);
	}
	else
	{
		if(ilNumDays > 0)
		{
			m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		}
	}
	
	
	
	sprintf(pclStr, "%s", ogSearchFilter.rlFlightFilterData.Flno);

	UpdateData(FALSE);
	::SetDlgItemText(GetSafeHwnd(),IDC_FLIGHTNR, pclStr);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PRMFlightSearch::OnRadioGanttSelect()
{
	UpdateData(TRUE);
	//imLastState = m_SingleFlight;
}

void PRMFlightSearch::OnRadioDetailDisplay()
{
	UpdateData(TRUE);
	//imLastState = m_SingleFlight;
}