// StaffDetailViewer.h : header file
//

#ifndef _STAFFDV_H_
#define _STAFFDV_H_


#define STAFFDATA	EMPDATA
#define StaffData	STAFFDATA

struct STAFFDETAIL_FASTLINKBKBARDATA
{
	CString StatusText;
    CString Text;
	COLORREF TextColor;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	long JobUrno;
	bool OfflineBar;

	STAFFDETAIL_FASTLINKBKBARDATA(void)
	{
		TextColor		= 0L;
		FrameType		= -1;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		OverlapLevel	= 0;
		OfflineBar	= false;
	}
};

struct STAFFDETAIL_TEMPABSENCEBKBARDATA
{
	CString StatusText;
    CString Text;
	COLORREF TextColor;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	long JobUrno;

	STAFFDETAIL_TEMPABSENCEBKBARDATA(void)
	{
		TextColor		= 0L;
		FrameType		= -1;
		MarkerType		= -1;
		MarkerBrush		= NULL;
		OverlapLevel	= 0;
	}
};

struct STAFFDETAIL_BARDATA 
{
	long StaffUrno;		// from FLTFRA.URNO
	long Urno;
    CString Text;
    // standard data for bars in general GanttLine
	CString StatusText;		// long text used to be displayed at the status bar
    CTime StartTime;
    CTime EndTime;
    int FrameType;
	COLORREF FrameColor;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	bool Infm;				// true if the emp has been informed of a job but the job has not yet started -> display lefthand black dot on the bar
	bool IsPause;
	COLORREF PauseColour;
	bool OfflineBar;
	bool DifferentFunction;

	STAFFDETAIL_BARDATA(void)
	{
		StaffUrno	= 0L;
		Urno		= 0L;
		StartTime	= TIMENULL;
		EndTime		= TIMENULL;
		FrameType	= 0;
		FrameColor	= BLACK;
		MarkerType	= 0;
		MarkerBrush	= NULL;
		OverlapLevel= 0;
		Infm		= false;
		IsPause		= false;
		OfflineBar	= false;
		DifferentFunction = false;
	}
};

struct STAFFDETAIL_LINEDATA 
{
	// standard data for lines in general Diagram's chart
    CString Text;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
	CTime StartTime;
	CTime EndTime;
	long PoolJobUrno;
	bool OfflineBar;

    CCSPtrArray <STAFFDETAIL_BARDATA> Bars;         // keep bars in order of painting (leftmost is the bottommost)
	CCSPtrArray <STAFFDETAIL_BARDATA> BkBars;
	CCSPtrArray <STAFFDETAIL_TEMPABSENCEBKBARDATA> TempAbsenceBkBars;
	CCSPtrArray <STAFFDETAIL_FASTLINKBKBARDATA> FastLinkBkBars;

	STAFFDETAIL_LINEDATA()
	{
		MaxOverlapLevel = 0;
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		PoolJobUrno = 0L;
		OfflineBar = false;
	}

	STAFFDETAIL_LINEDATA& STAFFDETAIL_LINEDATA::operator=(const STAFFDETAIL_LINEDATA& rhs)
	{
		if (&rhs != this)
		{
			Text = rhs.Text;
			MaxOverlapLevel = rhs.MaxOverlapLevel;
			StartTime = rhs.StartTime;
			EndTime = rhs.EndTime;
			PoolJobUrno = rhs.PoolJobUrno;

			int i;

			Bars.DeleteAll();
			for(i = 0; i < rhs.Bars.GetSize(); i++)
			{
				Bars.NewAt(i,rhs.Bars[i]);
			}

			BkBars.DeleteAll();
			for(i = 0; i < rhs.BkBars.GetSize(); i++)
			{
				BkBars.NewAt(i,rhs.BkBars[i]);
			}

			TempAbsenceBkBars.DeleteAll();
			for(i = 0; i < rhs.TempAbsenceBkBars.GetSize(); i++)
			{
				TempAbsenceBkBars.NewAt(i,rhs.TempAbsenceBkBars[i]);
			}

			FastLinkBkBars.DeleteAll();
			for(i = 0; i < rhs.FastLinkBkBars.GetSize(); i++)
			{
				FastLinkBkBars.NewAt(i,rhs.FastLinkBkBars[i]);
			}
		}		
		return *this;
	}

	~STAFFDETAIL_LINEDATA()
	{
		Bars.DeleteAll(); 
		BkBars.DeleteAll(); 
		TempAbsenceBkBars.DeleteAll(); 
		FastLinkBkBars.DeleteAll(); 
	}

};



// Attention:
// Element "TimeOrder" in the struct "STAFFDETAIL_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array STAFFDETAIL_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the STAFFDETAIL_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array STAFFDETAIL_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// StaffDetailViewer window

class StaffDetailViewer: public CViewer
{
// Constructions
public:
	char  lmEmpUrno[24];
    StaffDetailViewer();
    ~StaffDetailViewer();
	void Init(long lpJobPrimaryKey,BOOL bpIsFirstTime);
	void RemoveAll();
	void Attach(CWnd *popAttachWnd);

	// Data Pointer
	EMPDATA *GetEmpPtr(void) { return &omEmp; }
	SHIFTDATA *GetShiftPtr(void) { return &omShift; };
	CCSPtrArray<JOBDATA> *GetJobsPtr(void) { return &omJobs; };
	
	// Line
    int GetLineCount();
    STAFFDETAIL_LINEDATA *GetLine(int ipLineno);
    CString GetLineText(int ipLineno);

	// ForeGround Bar
    int CreateBar(int ipLineno, STAFFDETAIL_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipLineno, int ipBarno);
    int GetBarCount(int ipLineno);
    STAFFDETAIL_BARDATA *GetBar(int ipLineno, int ipBarno);
    int GetBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);

	// BackGround Bar
    int GetBkBarCount(int ipLineno);
    STAFFDETAIL_BARDATA *GetBkBar(int ipLineno, int ipBkBarno);
	int GetBkBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);

	// fast link bk bar
	int GetTempAbsenceBkBarCount(int ipLineno);
	STAFFDETAIL_TEMPABSENCEBKBARDATA *GetTempAbsenceBkBar(int ipLineno, int ipTempAbsenceBkBarno);
	int GetTempAbsenceBkBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);

	// fast link bk bar
	int GetFastLinkBkBarCount(int ipLineno);
	STAFFDETAIL_FASTLINKBKBARDATA *GetFastLinkBkBar(int ipLineno, int ipFastLinkBkBarno);
	int GetFastLinkBkBarnoFromTime(int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);

	CString GetTempAbsenceTextFromTime(CTime opTime1, CTime opTime2);
	
	// Broadcast export function
	void ProcessStaffChange(STAFFDATA *prpStaff);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessRefresh(void);

public:
	long lmJobPrimaryKey;
	BOOL bmIsFirstTime;
	char lmPknoKey[24];
	CWnd *pomAttachWnd;
	EMPDATA omEmp;
	SHIFTDATA omShift;
	CCSPtrArray <JOBDATA> omJobs;
	CCSPtrArray <JOBDATA> omTempAbsences;
	CCSPtrArray <STAFFDETAIL_LINEDATA> omStaffLines;

	CBrush omFastLinkBrush, omTempAbsenceBrush;
	STAFFDETAIL_FASTLINKBKBARDATA* MakeFastLinkBkBar(STAFFDETAIL_LINEDATA *prpLine, JOBDATA *prpJob);
	STAFFDETAIL_TEMPABSENCEBKBARDATA* MakeTempAbsenceBkBar(STAFFDETAIL_LINEDATA *prpLine, JOBDATA *prpJob);
	STAFFDETAIL_LINEDATA *GetFreeLineForTime(CTime opBarStart, CTime opBarEnd);

private:
	void GetOverlappedBarsFromTime(int ipLineno,CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	int GetOverlappingBars(STAFFDETAIL_LINEDATA *prpLine, CTime opStartTime, CTime opEndTime, CCSPtrArray <STAFFDETAIL_BARDATA> &ropOverlappingBars);
};


/////////////////////////////////////////////////////////////////////////////

#endif
