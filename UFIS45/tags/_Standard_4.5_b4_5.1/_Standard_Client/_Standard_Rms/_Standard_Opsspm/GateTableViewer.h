#ifndef __GTVIEWER_H__
#define __GTVIEWER_H__

#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaFlightData.h>

#include <cviewer.h>
#include <table.h>
#include <ccsprint.h>

struct GATEDATA_INDICATOR	// Actually, there is no indicator in the table.
{							// I use this word only for representing the same thing in Gate Diagram
	long JobUrno;
	long DemandUrno;
	CString Name;
	CTime Acfr;
	CTime Acto;
};

struct GATEDATA_LINE	// each line represents one flight (no rotation or turnaround)
{
	long FlightUrno;
	CString Alid;
	CString Fnum;
	CString FlightIofl;
	CTime FlightTime;
	CTime FlightActualTime;
	char  TimeStatus[2];
	CCSPtrArray<GATEDATA_INDICATOR> Indicators;
};

class GateTableViewer: public CViewer
{
// Constructions
public:
    GateTableViewer();
    ~GateTableViewer();

    void Attach(CTable *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CString opDate);
    void ChangeViewTo(const char *pcpViewName);
	CTime StartTime() const;
	CTime EndTime() const;
	int	  Lines() const;
	GATEDATA_LINE*	GetLine(int ipLineNo);
	void  SetStartTime(CTime opStartTime);	
	void  SetEndTime(CTime opEndTime);	

private:
	static void GateTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
    void PrepareFilter();
	void PrepareSorter();
	void PrepareGrouping();
	BOOL IsSameGroup(GATEDATA_LINE *prpGate1,GATEDATA_LINE *prpGate2);

	BOOL IsPassFilter(const char *pcpGate, FLIGHTDATA *prpFlight);
	int CompareGate(GATEDATA_LINE *prpGate1, GATEDATA_LINE *prpGate2);

	void MakeLines();
	void MakeLine(ALLOCUNIT *prpGate);
//	void MakeLine(METAALLOCDATA *prpData);
	void MakeIndicators();
	BOOL FindFlight(long lpFlightUrno, int &ripLineno);
	BOOL FindAssignment(long lpJobUrno, int &ripLineno, int &ripIndicatorno);
	int SearchDemand(JOBDATA *prpJob, int ipLineno);

	void DeleteAll();
	int CreateLine(GATEDATA_LINE *prpGate);
	void DeleteLine(int ipLineno);
	int CreateIndicator(int ipLineno, GATEDATA_INDICATOR *prpIndicator);
	void DeleteIndicator(int ipLineno, int ipIndicatorno);
	void SetFlightData(GATEDATA_LINE &ropLine, FLIGHTDATA *prpFlight);

	void UpdateDisplay();
	CString Format(GATEDATA_LINE *prpLine);

// Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "flviewer.cpp" (use first sorting)
    CWordArray omSortOrder;
	CMapStringToPtr omCMapForGate;
	CMapStringToPtr omCMapForAlcd;

// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<GATEDATA_LINE> omLines;
	CTime omStartTime;
	CTime omEndTime;
    CString omDate;

// DDX callback function processing
private:
	int ProcessJobNew(JOBDATA *prlJob);
	int ProcessJobDelete(JOBDATA *prlJob);
//	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessJobChanges(int ipDDXType,JOBDATA *prpJob);
	void ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight);

// Printing functions
private:
	BOOL PrintGateLine(GATEDATA_LINE *prpLine,BOOL bpIsLastLine);
	BOOL PrintGateHeader(CCSPrint *pomPrint);
	CCSPrint *pomPrint;
public:
	void PrintView();
};

#endif //__GTVIEWER_H__