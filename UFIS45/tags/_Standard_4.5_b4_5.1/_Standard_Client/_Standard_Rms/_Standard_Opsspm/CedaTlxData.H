#ifndef __CEDATLX_DATA__
#define __CEDATLX_DATA__

#define USECLEN 33
#define USEULEN 33
#define PRFLLEN 2
#define TTYPLEN 4
#define TEXTLEN 2001
#define SERELEN 2
#define STATLEN 2
#define ALC3LEN 4
#define FLTNLEN 6
#define FLNSLEN 2
#define BEMELEN 41
#define MSNOLEN 11
#define MSTXLEN 41
#define FKEYLEN 19

const int	FIRST_TELEX = 0;
const int	PREV_TELEX = 1;
const int	THIS_TELEX = 2;
const int	NEXT_TELEX = 3;
const int	LAST_TELEX = 4;


static void CedaTlxDataCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);

#include <CCSGlobl.h>
#include <CCSCedaData.h>
/////////////////////////////////////////////////////////////////////////////
struct TLXDATA {

	long	URNO;
	char	USEC[USECLEN];
	CTime	CDAT;
	char	USEU[USEULEN];
	CTime	LSTU;
	char	PRFL[PRFLLEN];
	char	TTYP[TTYPLEN];
	char	TXT1[TEXTLEN];
	char	TXT2[TEXTLEN];
	CTime	TIME;
	char	SERE[SERELEN];
	char	STAT[STATLEN];
	char	FKEY[FKEYLEN];
	char	ALC3[ALC3LEN];
	char	FLTN[FLTNLEN];
	char	FLNS[FLNSLEN];
	char	BEME[BEMELEN];
	char	MSNO[MSNOLEN];
	char	MSTX[MSTXLEN];

	TLXDATA(void)
	{
		URNO = 0;
		strcpy(USEC,"");
		CDAT = TIMENULL;
		strcpy(USEU,"");
		LSTU = TIMENULL;
		strcpy(PRFL,"");
		strcpy(TTYP,"");
		strcpy(TXT1,"");
		strcpy(TXT2,"");
		TIME = TIMENULL;
		strcpy(SERE,"");
		strcpy(STAT,"");
		strcpy(FKEY,"");
		strcpy(ALC3,"");
		strcpy(FLTN,"");
		strcpy(FLNS,"");
		strcpy(BEME,"");
		strcpy(MSNO,"");
		strcpy(MSTX,"");
	}

};


#define ORA_NOT_FOUND "ORA-01403"

class CedaTlxData: public CCSCedaData
{

// Attributes
public:

	CString GetTableName(void)
	{
		return pcmTableName;
	}

	int		 GetCountOfRecords()
	{
		return omData.GetSize();
	}

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaTlxData();
	~CedaTlxData();

	void ClearAll(void);

	long GetNextUrno( void );
    bool Read(const char *pcpWhere = "");
	bool SendSBC(const char *pcpCommand, const char *pcpFields, const char *pcpData);
	bool NewTelex( TLXDATA *prpTlx );
	bool ResendTelex( TLXDATA *prpTlx );
	bool ForwardTelex( TLXDATA *prpTlx, const char *pcpNewText );
	bool SetStat( const char *pcpStat );
	bool SetFlightNum( const char *pcpAlc3, const char *pcpFltn, const char *pcpFlns );
	bool CheckTelex( const char *pcpTelexText );
	bool Update(const char *pcpText);
	bool Add(TLXDATA *prpTlx);
	bool Insert(TLXDATA *prpTlx);
	void CreateNewTelex(const char *pcpTtyp);
	bool ReadSpecialData(CCSPtrArray<TLXDATA> *popTlx,char *pspWhere,char *pspFieldList,bool ipSYS =true);
	LPCTSTR TranslateText(TLXDATA *prpTlxRec, CString &ropText, bool blFromDb);
	TLXDATA *GetTlxRec( const int ipWhich );

// Attributes
private:
    CCSPtrArray <TLXDATA> omData;
	char pcmNewText[TEXTLEN+100];
	int imCurrTelex;
	CStatic *pomStaticFreeText;
};
extern CedaTlxData ogTlxData;

#endif //__CEDATLX_DATA__
