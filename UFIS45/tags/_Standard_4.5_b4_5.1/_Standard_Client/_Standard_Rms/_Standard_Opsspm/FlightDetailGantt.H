// FlightDetailGantt.h : header file
//

#ifndef _FLIGHTDG_H_
#define _FLIGHTDG_H_

#include <tscale.h>

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

/////////////////////////////////////////////////////////////////////////////
// FlightDetailGantt window
#define FDD_MENUAVAILEMPS			12
#define FDD_MENUDELETEDEMANDS		13
#define FDD_MENUAUTOASSIGN			14
#define FDD_MENUAVAILEQUIP			15
#define FDD_MENUCOPYDEMAND			16
#define FDD_MENUDEACTIVATEDEMAND	17
#define FDD_MENUEMPINFORMED			19
#define FDD_MENUJOBFIX				20
#define FDD_MENUSTARTJOB			21
#define FDD_MENUENDJOB				22
#define FDD_MENUEDITJOB				23
#define FDD_MENUDELETEJOB			24
#define FDD_LASTJOBINFO_MENUITEM    26
#define FDD_MENUADDDEMAND			27

#define FDD_ACCEPT_ALL_CONFLICTS	25
#define FDD_MAXNUMCONFLICTS			50

class FlightDetailGantt: public CListBox
{
// Operations
public:
	FlightDetailGantt::FlightDetailGantt(FlightDetailViewer *popViewer = NULL, int ipGroup = 0,
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 2,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 4, int ipOverlapHeight = 4,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));
	~FlightDetailGantt();
    void SetViewer(FlightDetailViewer *popViewer, int ipGroup);
    void SetVerticalScaleWidth(int ipWidth);
	void SetVerticalScaleIndent(int ipIndent);
    void SetFonts(CFont *popVerticalScaleFont, CFont *popGanttChartFont);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);

    int GetGanttChartHeight();
    int GetLineHeight(int ipMaxOverlapLevel);

    BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
    void SetStatusBar(CStatusBar *popStatusBar);
	void SetTimeScale(CTimeScale *popTimeScale);
    void SetBorderPrecision(int ipBorderPrecision);

    // This group of function will automatically repaint the window
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);
    void SetDisplayStart(CTime opDisplayStart);
    void SetCurrentTime(CTime opCurrentTime);
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);

    void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1,
            CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);

// Attributes
private:
    FlightDetailViewer *pomViewer;
    int imGroupno;              // index to a group in the viewer
    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
    COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;

    CTime omDisplayStart;
    CTime omDisplayEnd;
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
    BOOL bmIsFixedScaling;
        // There are two mode for updating the display window when WM_SIZE was sent,
        // variabled-scaling and fixed-scaling. The mode of this operation will be
        // given in the method SetDisplayWindow().

    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

    CStatusBar *pomStatusBar;   // the status bar where the notification message goes
	CTimeScale *pomTimeScale;	// the time scale for top scale indicators display

    BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
    int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over
	int imBkBarno;				// the current demand bk bar that the user places mouse over

    UINT umResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
    CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing
    int imBorderPreLeft;        // define the sensitivity when user detect bar border
    int imBorderPreRight;       // define the sensitivity when user detect bar border
	FLIGHT_BARDATA rmActiveBar;          // Bar which is currently moved or sized
	BOOL bmActiveBarSet;

	int imContextItem;
	int imContextBarNo;
	FLIGHT_BARDATA rmContextBar;  // Save bar for Context Menu handling
	BOOL bmContextBarSet;
	BOOL bmAllowInsertDemand;

// Implementation
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	 int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;
	void * pmBroadCastInstanceCaller;
private:
    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom);
	void DrawFocusRect(CDC *pDC, const CRect &rect);

protected:
    // Generated message map functions
    //{{AFX_MSG(FlightDetailGantt)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuFreeEmployees();
	afx_msg void OnMenuAvailableEquipment();
	afx_msg void OnMenuCopyDemand();
	afx_msg void OnMenuAddDemand();
	afx_msg void OnMenuDeactivateDemand();
	afx_msg void OnMenuDeleteDemand();
	afx_msg void OnMenuAutomaticAssignment();
	afx_msg void OnMenuJobFix();
    afx_msg void OnMenuJobInform();
	afx_msg void OnMenuStartJob();
    afx_msg void OnMenuEndJob();
    afx_msg void OnMenuEditJob();
    afx_msg void OnMenuDeleteJob();
    afx_msg void OnMenuAcceptConflict(UINT);
	afx_msg void OnMenuLastJobInfo(); //PRF 8998
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()


private:
    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
	void UpdateBarStatus(int ipLineno, int ipBarno, int ipBkBarno);
    BOOL BeginMovingOrResizing(CPoint point);
    BOOL OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);
    int GetItemFromPoint(CPoint point)const;
    int GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
        // Precision is measured in pixel.
        // The purpose of the precision is for detecting more pixel when user want to resize.
        // The acceptable period of time is between [point.x - ipPreLeft, point.x + ipPreRight]
    UINT HitTest(int ipLineno, int ipBarno, CPoint point);
        // HitTest will use the precision defined in "imBorderPrecision"

private:
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
	CMapPtrToPtr omAcceptConflictsMap;

//Pichate
protected:
	void DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem);
	void DrawVerticalLine(CDC *pDC);
	void DrawBkBars(CDC *pDC);

	int GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1)const;


	void DragFlightBegin(FLIGHT_BARDATA *prpBar);
	void DragDemandBarBegin(FLIGHT_BARDATA *prpBar);
	void DragJobBarBegin(FLIGHT_BARDATA *prpBar);
	void DragJobBarBegin(CCSPtrArray <JOBDATA> &ropJobs);
};

/////////////////////////////////////////////////////////////////////////////

#endif
