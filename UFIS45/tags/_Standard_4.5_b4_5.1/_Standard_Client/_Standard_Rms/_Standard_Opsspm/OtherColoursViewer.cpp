// OtherColoursViewer.cpp -- the viewer for Demand Table
//
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaPfcData.h>
#include <OpssPm.h>
#include <ccstable.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <OtherColoursViewer.h>
#include <DlgSettings.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

static struct OTHERCOLOURS_FIELD osfields[]	= 
{
	{	OTHERCOLOURS_FIELD("NAME",	"",		200,	200)},
	{	OTHERCOLOURS_FIELD("COLOUR",	"",  50,  50)	},
	
};

/////////////////////////////////////////////////////////////////////////////
// OtherColoursViewer



OtherColoursViewer::OtherColoursViewer()
{
    pomTable = NULL;

	// must initialize names here because resource handle is not available during static initialization
	if (!osfields[0].Name.GetLength())
	{
		osfields[0].Name = GetString(IDS_FUNCCOLOURS_NAME);
		osfields[1].Name = GetString(IDS_FUNCCOLOURS_COLOUR);		
	}

	bmDisplayDefinedOnly = false;
}

OtherColoursViewer::~OtherColoursViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
	omTableFields.DeleteAll();
	DeleteAll();
}

void OtherColoursViewer::Attach(CCSTable *popTable)
{
	pomTable = popTable;
}

void OtherColoursViewer::ChangeViewTo(void)
{
    DeleteAll();    
	EvaluateTableFields();
    MakeLines();
	UpdateDisplay();
}

void OtherColoursViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int OtherColoursViewer::CompareColourName(OTHERCOLOURS_LINE *prpLine1, OTHERCOLOURS_LINE *prpLine2)
{
	return strcmp(prpLine1->Name, prpLine2->Name);
}

/*
BOOL OtherColoursViewer::IsPassFilter(PFCDATA *prpPfc)
{
	BOOL blRc = TRUE;
	return blRc;
}
*/

void OtherColoursViewer::MakeLines()
{

	CCSPtrArray <OTHERCOLOURS_LINE> omDefaultLines;
    GetDefaultColoursConfig(omDefaultLines);
    int ilColCnt = omDefaultLines.GetSize();
	for(int ilCol = 0; ilCol < ilColCnt; ilCol++)
	{
		OTHERCOLOURS_LINE *prlCol = &omDefaultLines[ilCol];
		//if(IsPassFilter(prlPfc))
		{
			MakeLine(prlCol);
		}
	}

}



void OtherColoursViewer::GetDefaultColoursConfig(CCSPtrArray <OTHERCOLOURS_LINE> &ropColours)
{
	ropColours.RemoveAll();

	OTHERCOLOURS_LINE olColor;

	olColor.Name = CDlgSettings.CKEY_OTHER_COLOURS_PRM_FINALIZED;
	ropColours.NewAt(0,olColor);

	olColor.Name = CDlgSettings.CKEY_OTHER_COLOURS_PRM_LOCATION_LOUNGE;
	ropColours.NewAt(1,olColor);

}



void OtherColoursViewer::MakeLine(OTHERCOLOURS_LINE *prpCol)
{
	OTHERCOLOURS_LINE olLine;
	MakeLineData(prpCol, olLine);
	CreateLine(&olLine);
}


void OtherColoursViewer::MakeLineData(OTHERCOLOURS_LINE *prpCol, OTHERCOLOURS_LINE &ropLine)
{
	ropLine.Name = prpCol->Name;

	CString olColour;
	if(ogDlgSettings.GetValue(CDlgSettings.CTYP_OTHER_COLOURS, prpCol->Name, olColour))
	{
		ropLine.Colour = (COLORREF) atol(olColour);
		ropLine.Enabled = true;
	}
	else
	{
		ropLine.Colour = GRAY;
		ropLine.Enabled = false;
	}
}


void OtherColoursViewer::SaveLineData(void)
{
	ogDlgSettings.EnableSaveToDb(CDlgSettings.CTYP_OTHER_COLOURS);
	ogDlgSettings.DeleteSettingsByDialog(CDlgSettings.CTYP_OTHER_COLOURS);

	CString olColour;
    int ilLineCount = omLines.GetSize();
    for(int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		OTHERCOLOURS_LINE *prlLine = &omLines[ilLineno];
		if(prlLine->Enabled)
		{
			olColour.Format("%ld", (long) prlLine->Colour);
			ogDlgSettings.AddValue(CDlgSettings.CTYP_OTHER_COLOURS,  prlLine->Name, olColour);
		}
	}
}

int OtherColoursViewer::CreateLine(OTHERCOLOURS_LINE *prpLine)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareColourName(prpLine, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareColourName(prpLine, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

    omLines.NewAt(ilLineno, *prpLine);
    return ilLineno;
}

void OtherColoursViewer::DeleteLine(int ipLineno)
{
    omLines.DeleteAt(ipLineno);
}

void OtherColoursViewer::UpdateDisplay()
{
	if(pomTable != NULL)
	{
		pomTable->ResetContent();

		CreateTableColumnsAndHeader();
		pomTable->SetHeaderFields(omHeader);

		pomTable->DisplayTable();

		int ilNumLines = omLines.GetSize(), ilLinesAdded = 0;
		for (int i = 0; i < ilNumLines; i++) 
		{
			OTHERCOLOURS_LINE *prlLine = &omLines[i];
			if(!bmDisplayDefinedOnly || prlLine->Enabled)
			{
				Format(prlLine, omColumns);
				pomTable->AddTextLine(omColumns, prlLine);
				pomTable->SetTextLineColor(i, BLACK, prlLine->Enabled ? WHITE : SILVER);
				if(prlLine->Enabled)
				{
					pomTable->SetTextColumnColor(ilLinesAdded, 1, prlLine->Colour, prlLine->Colour);
				}
				ilLinesAdded++;
			}
		}

		pomTable->DisplayTable();
	}
}

void OtherColoursViewer::CreateTableColumnsAndHeader()
{
	omHeader.DeleteAll();
	omColumns.DeleteAll();
	int ilNumFields = omTableFields.GetSize();
	for (int ilC = 0; ilC < ilNumFields; ilC++)
	{
		omHeader.NewAt(ilC,TABLE_HEADER_COLUMN());
		omHeader[ilC].Text   = omTableFields[ilC].Name;
		omHeader[ilC].Length = omTableFields[ilC].Length;
		omColumns.NewAt(ilC,TABLE_COLUMN());
	}
}

void OtherColoursViewer::UpdateTableLine(int ipLine)
{
	if(pomTable && ipLine >= 0 && ipLine < omLines.GetSize())
	{
		OTHERCOLOURS_LINE *prlLine = &omLines[ipLine];
		Format(&omLines[ipLine], omColumns);
		pomTable->SetTextLineColor(ipLine, BLACK, prlLine->Enabled ? WHITE : SILVER);
		if(prlLine->Enabled)
			pomTable->SetTextColumnColor(ipLine, 1, prlLine->Colour, prlLine->Colour);
		pomTable->DisplayTable();
	}
}

void OtherColoursViewer::Format(OTHERCOLOURS_LINE *prpLine, CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	if (rrpColumns.GetSize() != omTableFields.GetSize())
		return;

	SetColumn("NAME", prpLine->Name, rrpColumns);
	SetColumn("COLOUR", "", rrpColumns);
}

bool OtherColoursViewer::SetColumn(const char *pcpFieldName, const char *pcpFieldValue, CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	int ilIndex = TableFieldIndex(pcpFieldName);
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = pcpFieldValue;
		return true;
	}

	return false;
}

int OtherColoursViewer::LineCount() const
{
	return omLines.GetSize();
}

OTHERCOLOURS_LINE *OtherColoursViewer::GetLine(int ipLineNo)
{
	OTHERCOLOURS_LINE *prlLine = NULL;
	if(bmDisplayDefinedOnly)
	{
		int ilNumLines = omLines.GetSize();
		int ilLineCount = -1;
		for(int ilL = 0; ilL < ilNumLines; ilL++)
		{
			if(omLines[ilL].Enabled)
				ilLineCount++;

			if(ilLineCount == ipLineNo)
			{
				prlLine = &omLines[ilL];
				break;
			}
		}
	}
	else
	{
		if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
			prlLine = &omLines[ipLineNo];
	}

	return prlLine;
}

bool OtherColoursViewer::EvaluateTableFields()
{
	omTableFields.DeleteAll();
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		omTableFields.New(osfields[i]);
	}

	return true;
}

int OtherColoursViewer::TableFieldIndex(const CString& ropField)
{
	for (int i = 0; i < omTableFields.GetSize(); i++)
	{
		if (omTableFields[i].Field == ropField)
			return i;
	}

	return -1;
}

