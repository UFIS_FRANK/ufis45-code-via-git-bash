#if !defined(AFX_SELREPORTDLG_H__D14AAD4B_0F2D_4188_AED1_F76E4222C4FF__INCLUDED_)
#define AFX_SELREPORTDLG_H__D14AAD4B_0F2D_4188_AED1_F76E4222C4FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelReportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelReportDlg dialog

class CSelReportDlg : public CDialog
{
// Construction
public:
	CSelReportDlg(CWnd* pParent = NULL);   // standard constructor
	CSelReportDlg(CString opCaption, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelReportDlg)
	enum { IDD = IDD_SELREPORTDLG };
	CListBox	m_SelReportListCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelReportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelReportDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDoubleClickSelReportList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CStringArray omReports;
	void SetReports(CString &ropReport);
	CString omSelectedReport;
	CString omCaption;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELREPORTDLG_H__D14AAD4B_0F2D_4188_AED1_F76E4222C4FF__INCLUDED_)
