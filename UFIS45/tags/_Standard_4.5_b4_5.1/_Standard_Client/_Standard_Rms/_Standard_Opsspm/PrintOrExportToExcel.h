#if !defined(AFX_PRINTOREXPORTTOEXCEL_H__E765EB61_1DE4_4150_94A5_FF884884A9F1__INCLUDED_)
#define AFX_PRINTOREXPORTTOEXCEL_H__E765EB61_1DE4_4150_94A5_FF884884A9F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintOrExportToExcel.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrintOrExportToExcel dialog

class CPrintOrExportToExcel : public CDialog
{
// Construction
public:
	CPrintOrExportToExcel(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrintOrExportToExcel)
	enum { IDD = IDD_PRINT_EXCEL };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintOrExportToExcel)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	CButton omRadioButton;
	CButton omCheckBoxPageBreak;
	CButton omCheckBoxColorEnabled;
	BOOL bmIsExportToExcel;
	BOOL bmIsPageBreakEnabled;
	BOOL bmIsColorEnabled;

public:
	BOOL IsExportToExcel();
	BOOL IsPageBreakEnabled();
	BOOL IsColorEnabled();

protected:	
	virtual void OnOK();
	virtual BOOL OnInitDialog();

	// Generated message map functions
	//{{AFX_MSG(CPrintOrExportToExcel)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTOREXPORTTOEXCEL_H__E765EB61_1DE4_4150_94A5_FF884884A9F1__INCLUDED_)
