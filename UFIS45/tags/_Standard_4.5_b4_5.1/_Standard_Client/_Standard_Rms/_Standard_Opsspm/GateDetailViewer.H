#ifndef __GATEDV_H__
#define __GATEDV_H__

#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <cviewer.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>

struct GATEDETAIL_ASSIGNMENT  // each assignment record
{
    CString Name;   // assigned to gate or flight
	long Urno;      // flight job Urno 
    CTime Acfr;     // assigned from
    CTime Acto;     // assigned until
};

struct GATEDETAIL_LINEDATA
{
	long	FlightUrno;
	CString Fnum;	// Flight number
	CString Rou1;	// Origin/Destination
	CString Iofl;	// I/O flag
	CTime   FlightTime;	// Stoa/Stod depends on I/O flag

    CCSPtrArray<GATEDETAIL_ASSIGNMENT> Assignment;
};

/////////////////////////////////////////////////////////////////////////////
// GateDetailViewer

class GateDetailViewer: public CViewer
{
// Constructions
public:
    GateDetailViewer();
    ~GateDetailViewer();

    void Attach(CTable *popTable,CTime opStartTime,CTime opEndTime,CString opAlid);
    void ChangeView();

// Internal data processing routines
private:
	void PrepareGrouping();
    void PrepareFilter();
	void PrepareSorter();
	BOOL IsPassFilter(CTime opAcfr);

    void MakeLines();
	void MakeLine(FLIGHTDATA *prpFlight);
	void MakeAssignments(long lpFlightUrno,GATEDETAIL_LINEDATA &rrpFlightLine);
	int CreateAssignment(GATEDETAIL_LINEDATA &rrlStaff, GATEDETAIL_ASSIGNMENT *prpAssignment);

	BOOL FindFlight(long lpFlightUrno, int &rilLineno);

// Operations
public:
	void DeleteAll();
	int CreateLine(GATEDETAIL_LINEDATA *prpStaff);
	void DeleteLine(int ipLineno);
	int	  Lines() const;
	GATEDETAIL_LINEDATA*	GetLine(int ipLineNo);

// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(GATEDETAIL_LINEDATA *prpLine);
	void ProcessFlightChange(FLIGHTDATA *prpFlight);

// Attributes used for filtering condition
private:
    CString omDate;
    CTime omStartTime;
	CTime omEndTime;

// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<GATEDETAIL_LINEDATA> omLines;
	CString omAlid;
// Methods which handle changes (from Data Distributor)
public:
};

#endif //__GATEDV_H__
