// DJobChange.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <DJobChange.h>
#include <CCSTime.h>
#include <CedaEmpData.h>



#ifdef _DEBUG
//#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DJobChange dialog


DJobChange::DJobChange(CWnd* pParent,CString opEmpName,CTime opAcfr,CTime opActo,bool bpAcfrReadOnly,bool bpActoReadOnly)
	: CDialog(DJobChange::IDD, pParent)
{
	prmJob = NULL;
	CStringArray olEmpNames;
	olEmpNames.Add(opEmpName);
	Init(pParent, olEmpNames, opAcfr, opActo, bpAcfrReadOnly, bpActoReadOnly);
}

DJobChange::DJobChange(CWnd* pParent,CStringArray &ropEmpNames,CTime opAcfr,CTime opActo,bool bpAcfrReadOnly,bool bpActoReadOnly)
	: CDialog(DJobChange::IDD, pParent)
{
	prmJob = NULL;
	Init(pParent, ropEmpNames, opAcfr, opActo, bpAcfrReadOnly, bpActoReadOnly);
}

DJobChange::DJobChange(CWnd* pParent, JOBDATA *prpJob, bool bpAcfrReadOnly, bool bpActoReadOnly)
	: CDialog(DJobChange::IDD, pParent)
{
	prmJob = prpJob;
	CString olEmpName = ogEmpData.GetEmpName(prpJob->Ustf);
	CStringArray olEmpNames;
	olEmpNames.Add(olEmpName);
	Init(pParent, olEmpNames, prpJob->Acfr, prpJob->Acto, bpAcfrReadOnly, bpActoReadOnly);
}

void DJobChange::Init(CWnd* pParent,CStringArray &ropEmpNames,CTime opAcfr,CTime opActo,bool bpAcfrReadOnly,bool bpActoReadOnly)
{
	m_Acfr = opAcfr;
	m_Acto = opActo;

	CString olEmpNames;
	int ilNumEmpNames = ropEmpNames.GetSize();
	for(int ilEmpName = 0; ilEmpName < ilNumEmpNames; ilEmpName++)
	{
		if(!olEmpNames.IsEmpty())
		{
			olEmpNames += "\r\n";
		}
		olEmpNames += ropEmpNames[ilEmpName];
	}

	m_EmpName  = olEmpNames;
	m_FromDate = opAcfr;
	m_FromTime = opAcfr;
	m_ToDate   = opActo;
	m_ToTime   = opActo;

	bmAcfrReadOnly = bpAcfrReadOnly;
	bmActoReadOnly = bpActoReadOnly;


	//{{AFX_DATA_INIT(DJobChange)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DJobChange::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DJobChange)
	DDX_Text(pDX, IDC_EMPNAM, m_EmpName);
	DDX_CCSddmmyy(pDX, IDC_FROMDATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROMTIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TODATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TOTIME, m_ToTime);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DJobChange, CDialog)
	//{{AFX_MSG_MAP(DJobChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DJobChange message handlers

BOOL DJobChange::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(prmJob != NULL && !strcmp(prmJob->Jtco,JOBBREAK))
	{
		SetWindowText(GetString(IDS_EDITBREAK)); 
	}
	else
	{
		SetWindowText(GetString(IDS_STRING32904)); 
	}


	CWnd *polWnd = GetDlgItem(IDC_EDITFROM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32845));
	}
	polWnd = GetDlgItem(IDC_EDITTO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32846));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	if (bmAcfrReadOnly)
	{
		polWnd = GetDlgItem(IDC_FROMDATE);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
		polWnd = GetDlgItem(IDC_FROMTIME);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
	}

	if (bmActoReadOnly)
	{
		polWnd = GetDlgItem(IDC_TODATE);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
		polWnd = GetDlgItem(IDC_TOTIME);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
		}
	}

	if(!bmAcfrReadOnly)
	{
		if((polWnd = GetDlgItem(IDC_FROMDATE)) != NULL)
		{
			polWnd->SetFocus();
		}
	}
	else if(!bmActoReadOnly)
	{
		if((polWnd = GetDlgItem(IDC_TODATE)) != NULL)
		{
			polWnd->SetFocus();
		}
	}

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DJobChange::OnOK() 
{
	if(UpdateData(TRUE))
	{
		m_Acfr = CTime (m_FromDate.GetYear(),m_FromDate.GetMonth(),m_FromDate.GetDay(),
						m_FromTime.GetHour(),m_FromTime.GetMinute(),m_FromTime.GetSecond());

		m_Acto = CTime (m_ToDate.GetYear(),m_ToDate.GetMonth(),m_ToDate.GetDay(),
						m_ToTime.GetHour(),m_ToTime.GetMinute(),m_ToTime.GetSecond());

		if (m_Acfr >= m_Acto)
			MessageBox(GetString(IDS_STRING61262));
		else
			EndDialog(IDOK);
	}
}
