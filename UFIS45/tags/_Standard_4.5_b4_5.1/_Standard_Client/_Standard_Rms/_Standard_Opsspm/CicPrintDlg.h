#if !defined(AFX_CICPRINTDLG_H__058B0014_97D5_4B56_9CFF_93884EE34951__INCLUDED_)
#define AFX_CICPRINTDLG_H__058B0014_97D5_4B56_9CFF_93884EE34951__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CicPrintDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCicPrintDlg dialog

class CCicPrintDlg : public CDialog
{
// Construction
public:
	CCicPrintDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCicPrintDlg)
	enum { IDD = IDD_CICPRINTDLG };
	BOOL	m_STA;
	BOOL	m_STD;
	BOOL	m_ETA;
	BOOL	m_ETD;
	BOOL	m_Gate;
	BOOL	m_Arr;
	int		m_EmpSort;
	int		m_FlightSort;
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCicPrintDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCicPrintDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void SetControlText(int ipControlId, int ipStringId);
	CTime omFrom, omTo;

public:
	void SetTime(CTime opFrom, CTime opTo);
	void GetSettings(CTime &ropFrom, CTime &ropTo, bool &rbpSortByShift, bool &rbpSortByStd, bool &rbpDisplaySta, bool &rbpDisplayEta, bool &rbpDisplayStd, bool &rbpDisplayEtd, bool &rbpDisplayGate, bool &rbpDisplayArr);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CICPRINTDLG_H__058B0014_97D5_4B56_9CFF_93884EE34951__INCLUDED_)
