// CciDemandDetailDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <CciDemandDetailDlg.h>
#include <CedaDemandData.h>
#include <CedaFlightData.h>
#include <CedaRueData.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <BasicData.h>
#include <cviewer.h>
#include <FlightDetailWindow.h>
#include <CcsTable.h>


//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif
/////////////////////////////////////////////////////////////////////////////
// CciDemandDetailDlg dialog


CciDemandDetailDlg::CciDemandDetailDlg(CWnd* pParent,long lpDemUrno)
: CDialog()
{
	//{{AFX_DATA_INIT(CciDemandDetailDlg)
	m_RuleName = _T("");
	m_Counter = _T("");
	m_FromDate = _T("");
	m_FromTime = _T("");
	m_Functions = _T("");
	m_Service = _T("");
	m_ToDate = _T("");
	m_Qualifications = _T("");
	m_ToTime = _T("");
	//}}AFX_DATA_INIT

	lmDemUrno = lpDemUrno;

	Create(CciDemandDetailDlg::IDD,pParent);
}


void CciDemandDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CciDemandDetailDlg)
	DDX_Text(pDX, IDC_RULE, m_RuleName);
	DDX_Text(pDX, IDC_COUNTER, m_Counter);
	DDX_Text(pDX, IDC_FROMDATE, m_FromDate);
	DDX_Text(pDX, IDC_FROMTIME, m_FromTime);
	DDX_Text(pDX, IDC_FUNCTIONS, m_Functions);
	DDX_Text(pDX, IDC_SERVICE, m_Service);
	DDX_Text(pDX, IDC_TODATE, m_ToDate);
	DDX_Text(pDX, IDC_QUALIFICATIONS, m_Qualifications);
	DDX_Text(pDX, IDC_TOTIME, m_ToTime);
	//}}AFX_DATA_MAP
}


void CciDemandDetailDlg::UpdateDisplay()
{
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(lmDemUrno);
	if(prlDemand != NULL)
	{
		UpdateTable(prlDemand);

		RUDDATA *prlRudData = ogRudData.GetRudByUrno(prlDemand->Urud);
		if(prlRudData != NULL)
		{
			SERDATA *prlSer = ogSerData.GetSerByUrno(prlRudData->Ughs);
			RUEDATA *prlRue = ogRueData.GetRueByUrno(prlRudData->Urue);
			if(prlRue != NULL)
			{
				m_RuleName = prlRue->Runa;
			}
			if(prlSer != NULL)
			{
				m_Service = prlSer->Snam;
			}
		}
		m_Counter = prlDemand->Alid;
		m_FromDate = prlDemand->Debe.Format("%d.%m.%Y");
		m_FromTime = prlDemand->Debe.Format("%H:%M");
		m_ToDate = prlDemand->Deen.Format("%d.%m.%Y");
		m_ToTime = prlDemand->Deen.Format("%H:%M");
		m_Functions = ogBasicData.GetDemandFunctionsString(prlDemand);
		m_Qualifications = ogBasicData.GetDemandPermitsString(prlDemand);
		UpdateData(FALSE);
	}
}

CString CciDemandDetailDlg::Format(FLIGHTDATA *prpFlight)
{
    CString olText;
	if (!prpFlight)
	    return olText;

	olText = prpFlight->Fnum;

	return olText;
}


BEGIN_MESSAGE_MAP(CciDemandDetailDlg, CDialog)
	//{{AFX_MSG_MAP(CciDemandDetailDlg)
	ON_BN_CLICKED(IDC_SCHLIEBEN, OnSchlieben)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CciDemandDetailDlg message handlers

BOOL CciDemandDetailDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(GetString(IDS_STRING61968));

	SetTitle(IDC_FROMTITLE, IDS_CICDDD_FROM);
	SetTitle(IDC_TOTITLE, IDS_CICDDD_TO);
	SetTitle(IDC_RULETITLE, IDS_CICDDD_RULE);
	SetTitle(IDC_SERVICETITLE, IDS_CICDDD_SERVICE);
	SetTitle(IDC_COUNTERTITLE, IDS_CICDDD_COUNTER);
	SetTitle(IDC_FUNCTIONSTITLE, IDS_CICDDD_FUNCTIONS);
	SetTitle(IDC_QUALIFICATIONSTITLE, IDS_CICDDD_QUALIFICATIONS);

	// Display table content
	CWnd *pWnd = GetDlgItem(IDC_STATIC_FLIGHTS);
	ASSERT(pWnd);

    CRect olRect;
    pWnd->GetWindowRect(&olRect);
	this->ScreenToClient(&olRect);
    CRect olRectTab(olRect);
	olRectTab.InflateRect(-1,-1);
	olRectTab.top += 5;
	olRectTab.bottom += 1;
	int ilMiddle = olRectTab.left + ((olRectTab.right - olRectTab.left) / 2);
	
    omTable1.SetTableData(this, olRectTab.left, ilMiddle, olRectTab.top, olRectTab.bottom);
    omTable2.SetTableData(this, ilMiddle, olRectTab.right, olRectTab.top, olRectTab.bottom);

    UpdateDisplay();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CciDemandDetailDlg::UpdateTable(DEMANDDATA *prpDemand)
{
	// Update the table content in the display
	omTable1.ResetContent();
	omTable2.ResetContent();
	CreateTableColumnsAndHeader();
	omTable1.SetHeaderFields(omHeader);
	omTable1.DisplayTable();
	omTable2.SetHeaderFields(omHeader);
	omTable2.DisplayTable();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	CDWordArray olFlights;
	ogDemandData.GetFlightsFromCciDemand(prpDemand,olFlights);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	for (int ilLc = 0; ilLc < olFlights.GetSize(); ilLc++)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(olFlights[ilLc]);
		if (prlFlight)
		{
			omColumns[0].Text = Format(prlFlight);
			if(ilLc % 2)
			{
				omTable2.InsertTextLine(ilLc, omColumns, prlFlight);
			}
			else
			{
				omTable1.InsertTextLine(ilLc, omColumns, prlFlight);
			}
		}
	}

// test
//	for(int i=0;i< 30;i++)
//	{
//		FLIGHTDATA *prlFlight = &ogFlightData.omData[i];
//		omColumns[0].Text = Format(prlFlight);
//		if(i % 2)
//		{
//			omTable2.InsertTextLine(i, omColumns, prlFlight);
//		}
//		else
//		{
//			omTable1.InsertTextLine(i, omColumns, prlFlight);
//		}
//	}

	omTable1.DisplayTable();
	omTable2.DisplayTable();
}

void CciDemandDetailDlg::CreateTableColumnsAndHeader()
{
	omHeader.DeleteAll();
	omColumns.DeleteAll();
	omHeader.NewAt(0,TABLE_HEADER_COLUMN());
	omHeader[0].Text = GetString(IDS_STRING61967);
	omHeader[0].Length = 260;
	omColumns.NewAt(0,TABLE_COLUMN());
}

void CciDemandDetailDlg::SetTitle(int ipControlId, int ipStringId)
{
	CWnd *polWnd = GetDlgItem(ipControlId);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(ipStringId));
	}
}

void CciDemandDetailDlg::OnSchlieben() 
{
	// TODO: Add your control notification handler code here
	this->DestroyWindow();	
}

void CciDemandDetailDlg::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	CDialog::PostNcDestroy();
	delete this;
}

void CciDemandDetailDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	this->DestroyWindow();	
}

void CciDemandDetailDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

LONG CciDemandDetailDlg::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY*) lpLParam;
	if(polNotify != NULL)
	{
		CCSTable *polTable = (CCSTable *) polNotify->SourceTable;
		if(polTable != NULL)
		{
			FLIGHTDATA *prlFlight = (FLIGHTDATA *)polTable->GetTextLineData(ipItem);
			if(prlFlight != NULL)
			{
				new FlightDetailWindow(this,prlFlight->Urno,0L);
			}
		}
	}
	return 0L;
}
