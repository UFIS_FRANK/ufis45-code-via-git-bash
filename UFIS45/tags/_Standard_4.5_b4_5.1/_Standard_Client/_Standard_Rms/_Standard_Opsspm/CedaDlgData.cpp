// Class for DLGTAB - Group delegations (Abordnungen) may require a different group name
// or the emp may require a different function (ie Guppenfuhrer), these are created for each
// pool job (created as a result of the delegation)
#include <afxwin.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDlgData.h>
#include <BasicData.h>

extern CCSDdx ogCCSDdx;
void  ProcessDlgCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDlgData::CedaDlgData()
{                  
    BEGIN_CEDARECINFO(DLGDATA, DLGDATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Wgpc,"WGPC")
		FIELD_LONG(Ujob,"UJOB")
		FIELD_CHAR_TRIM(Sday,"SDAY")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DLGDATARecInfo)/sizeof(DLGDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DLGDATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DLGTAB");
    pcmFieldList = "URNO,WGPC,UJOB,SDAY,FCTC";

	ogCCSDdx.Register((void *)this,BC_DLG_CHANGE,CString("DLGDATA"), CString("Dlg changed"),ProcessDlgCf);
	ogCCSDdx.Register((void *)this,BC_DLG_DELETE,CString("DLGDATA"), CString("Dlg deleted"),ProcessDlgCf);
}
 
CedaDlgData::~CedaDlgData()
{
	TRACE("CedaDlgData::~CedaDlgData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void CedaDlgData::ClearAll()
{
	omUjobMap.RemoveAll();
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

void  ProcessDlgCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaDlgData *)popInstance)->ProcessDlgBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaDlgData::ProcessDlgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDlgData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDlgData->Selection);
	DLGDATA *prlDlg = GetDlgByUrno(llUrno);
	ogBasicData.LogBroadcast(prlDlgData);

	if(ipDDXType == BC_DLG_CHANGE && prlDlg != NULL)
	{
		// update
		//ogBasicData.Trace("BC_DLG_CHANGE - UPDATE DLGTAB URNO %ld\n",prlDlg->Urno);
		GetRecordFromItemList(prlDlg,prlDlgData->Fields,prlDlgData->Data);
		ogCCSDdx.DataChanged((void *)this,DLG_CHANGE,(void *)prlDlg);
	}
	else if(ipDDXType == BC_DLG_CHANGE && prlDlg == NULL)
	{
		// insert
		DLGDATA rlDlg;
		GetRecordFromItemList(&rlDlg,prlDlgData->Fields,prlDlgData->Data);
		//ogBasicData.Trace("BC_DLG_CHANGE - INSERT DLGTAB URNO %ld\n",rlDlg.Urno);
		prlDlg = AddDlgInternal(rlDlg);
		ogCCSDdx.DataChanged((void *)this,DLG_CHANGE,(void *)prlDlg);
	}
	else if(ipDDXType == BC_DLG_DELETE && prlDlg != NULL)
	{
		// delete
		long llDlgUrno = prlDlg->Urno;
		//ogBasicData.Trace("BC_DLG_DELETE - DELETE DLGTAB URNO %ld\n",llDlgUrno);
		DeleteDlgInternal(llDlgUrno);
		ogCCSDdx.DataChanged((void *)this,DLG_DELETE,(void *)llDlgUrno);
	}
}

CCSReturnCode CedaDlgData::ReadDlgData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    sprintf(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDlgData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		DLGDATA rlDlgData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDlgData)) == RCSuccess)
			{
				AddDlgInternal(rlDlgData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDlgData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DLGDATA), pclWhere);
    return ilRc;
}


DLGDATA *CedaDlgData::AddDlgInternal(DLGDATA &rrpDlg)
{
	DLGDATA *prlDlg = new DLGDATA;
	*prlDlg = rrpDlg;
	PrepareData(prlDlg);
	omData.Add(prlDlg);
	omUrnoMap.SetAt((void *)prlDlg->Urno,prlDlg);
	omUjobMap.SetAt((void *)prlDlg->Ujob,prlDlg);
	return prlDlg;
}

void CedaDlgData::DeleteDlgInternal(long lpUrno)
{
	int ilNumDlgs = omData.GetSize();
	for(int ilDel = (ilNumDlgs-1); ilDel >= 0; ilDel--)
	{
		DLGDATA *prlDlg = &omData[ilDel];
		if(prlDlg->Urno == lpUrno)
		{
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

void CedaDlgData::PrepareData(DLGDATA *prpDlg)
{
}

DLGDATA* CedaDlgData::GetDlgByUrno(long lpUrno)
{
	DLGDATA *prlDlg = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlDlg);
	return prlDlg;
}

CString CedaDlgData::GetTableName(void)
{
	return CString(pcmTableName);
}


DLGDATA *CedaDlgData::GetDlgByUjob(long lpUjob)
{
	DLGDATA *prlDlg = NULL;
	omUjobMap.Lookup((void *)lpUjob,(void *& )prlDlg);
	return prlDlg;
}

DLGDATA *CedaDlgData::Insert(JOBDATA *prpPoolJob, CString opWgpc, CString opFctc)
{
	DLGDATA *prlDlg = NULL;
	if(prpPoolJob != NULL)
	{
		// prepare the data
		DLGDATA rlDlg;
		rlDlg.Urno = ogBasicData.GetNextUrno();
		strcpy(rlDlg.Sday,prpPoolJob->Acfr.Format("%Y%m%d")); // date required for archiving these records
		rlDlg.Ujob = prpPoolJob->Urno;
		strcpy(rlDlg.Wgpc,opWgpc);
		strcpy(rlDlg.Fctc,opFctc);

		// insert into DB
		CString olListOfData;
		char pclSelection[124] = "";
		char pclData[524] = "";
		char pclCmd[10] = "";
		MakeCedaData(&omRecInfo,olListOfData,&rlDlg);
		strcpy(pclData,olListOfData);
		strcpy(pclCmd,"IRT");
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
		if(!CedaAction(pclCmd,"","",pclData))
		{
			ogBasicData.LogCedaError("CedaDlgData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
		else
		{
			prlDlg = AddDlgInternal(rlDlg);
		}
	}

	return prlDlg;
}


void CedaDlgData::Delete(long lpUrno)
{
	char pclSelection[124] = "";
	char pclData[524] = "";
	char pclCmd[10] = "";
	sprintf(pclSelection,"WHERE URNO = '%ld'",lpUrno);
	strcpy(pclCmd,"DRT");
	ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
	if(!CedaAction(pclCmd,"","",pclData))
	{
		ogBasicData.LogCedaError("CedaDlgData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
	}
	else
	{
		DeleteDlgInternal(lpUrno);
	}
}
