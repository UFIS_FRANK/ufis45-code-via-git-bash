// AutoAssignAz.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <AutoAssignAz.h>
#include <Basicdata.h>
#include <CedaPrmData.h>
#include <DataSet.h>
#include <Allocdata.h>
#include <CedaAcrData.h>
#include <Ccsglobl.h>
#include <CCSPrint.h>
#include <Table.h>
#include <CCSDragDropCtrl.h>
#include <ButtonList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define JOBHDL "jobhdl"
#define JOBHDLC "jobhdlc"

/////////////////////////////////////////////////////////////////////////////
// CAutoAssignAz dialog


CAutoAssignAz::CAutoAssignAz(CTime opLoadStart, CTime opLoadEnd, CString opChartType, CWnd* pParent)
	: CDialog(CAutoAssignAz::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAutoAssignAz)
	m_AssignWOAloc = FALSE;
	m_BreakPrio = FALSE;
	m_ToTime = _T("");
	m_ToDay = _T("");
	m_FromTime = _T("");
	m_FromDay = _T("");
	m_ShiftEndBuff = _T("0");
	m_JobBuff = _T("0");
	m_FlightAircraft = _T("");
	m_FlightAirline = _T("");
	m_FlightDest = _T("");
	m_FlightNumber = _T("");
	m_FlightOrig = _T("");
	m_FlightRegn = _T("");
	m_FlightSuffix = _T("");
	m_AbsTimeVal = -1;
	//}}AFX_DATA_INIT

	omChartType = opChartType;
	omLoadEnd = opLoadEnd;
	omLoadStart = opLoadStart;

	omLeaveMan = "0,";
	omOpenOnly = "0,";
	omDelOnly = "0,";

	ogPrmData.ReadPrmData();  // Strategies table for Alitalia

}


void CAutoAssignAz::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutoAssignAz)
	DDX_Control(pDX, IDC_SELECTEDLIST_STRA, m_SelectedListStra);
	DDX_Control(pDX, IDC_SELECTEDLIST_REGN, m_SelectedListRegn);
	DDX_Control(pDX, IDC_SELECTEDLIST_EMP, m_SelectedListEmp);
	DDX_Control(pDX, IDC_POSSIBLELIST_STRA, m_PossibleListStra);
	DDX_Control(pDX, IDC_POSSIBLELIST_REGN, m_PossibleListRegn);
	DDX_Control(pDX, IDC_POSSIBLELIST_EMP, m_PossibleListEmp);
	DDX_Check(pDX, IDC_ASSIGNWOALOC, m_AssignWOAloc);
	DDX_Check(pDX, IDC_BREAKPRIO, m_BreakPrio);
	DDX_Text(pDX, IDC_TOTIME, m_ToTime);
	DDX_Text(pDX, IDC_TODAY, m_ToDay);
	DDX_Text(pDX, IDC_FROMTIME, m_FromTime);
	DDX_Text(pDX, IDC_FROMDAY, m_FromDay);
	DDX_Text(pDX, IDC_SHIFTENDBUFF, m_ShiftEndBuff);
	DDX_Text(pDX, IDC_JOBBUFF, m_JobBuff);
	DDX_Text(pDX, IDC_FLIGHTAIRCRAFT, m_FlightAircraft);
	DDX_Text(pDX, IDC_FLIGHTAIRLINE, m_FlightAirline);
	DDX_Text(pDX, IDC_FLIGHTDEST, m_FlightDest);
	DDX_Text(pDX, IDC_FLIGHTNUMBER, m_FlightNumber);
	DDX_Text(pDX, IDC_FLIGHTORIG, m_FlightOrig);
	DDX_Text(pDX, IDC_FLIGHTREGN, m_FlightRegn);
	DDX_Text(pDX, IDC_FLIGHTSUFFIX, m_FlightSuffix);
	DDX_Radio(pDX, IDC_ABSOLUTETIMESPAN, m_AbsTimeVal);
	DDX_Control(pDX, IDC_RELTO, m_RelToCtrl);
	DDX_Control(pDX, IDC_RELFROM, m_RelFromCtrl);
	DDX_Control(pDX, IDC_TOTIME, m_ToTimeCtrl);
	DDX_Control(pDX, IDC_TODAY, m_ToDayCtrl);
	DDX_Control(pDX, IDC_FROMTIME, m_FromTimeCtrl);
	DDX_Control(pDX, IDC_FROMDAY, m_FromDayCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAutoAssignAz, CDialog)
	//{{AFX_MSG_MAP(CAutoAssignAz)
	ON_BN_CLICKED(IDC_LEAVEMAN, OnLeaveman)
	ON_BN_CLICKED(IDC_OPENONLY, OnOpenonly)
	ON_BN_CLICKED(IDC_DELONLY, OnDelonly)
	ON_BN_CLICKED(IDC_DELETE_STRA, OnDeleteStra)
	ON_BN_CLICKED(IDC_DELETE_REGN, OnDeleteRegn)
	ON_BN_CLICKED(IDC_DELETE_EMP, OnDeleteEmp)
	ON_BN_CLICKED(IDC_ADD_EMP, OnAddEmp)
	ON_BN_CLICKED(IDC_ADD_REGN, OnAddRegn)
	ON_BN_CLICKED(IDC_ADD_STRA, OnAddStra)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_ABSOLUTETIMESPAN, OnAbstime)
	ON_BN_CLICKED(IDC_RELATIVETIMESPAN, OnReltime)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutoAssignAz message handlers
BOOL CAutoAssignAz::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString olTmp;
	CStringArray olTmpArray;
	m_PossibleListEmp.ResetContent();
	m_SelectedListEmp.ResetContent();
	m_PossibleListRegn.ResetContent();
	m_SelectedListRegn.ResetContent();
	m_PossibleListStra.ResetContent();
	m_SelectedListStra.ResetContent();

	SetWindowText(GetString(IDS_AUTOASSIGNAZ));

	CWnd *polWnd = GetDlgItem(IDC_TIMEFRAME);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_TIMESPAN));
	}
	polWnd = GetDlgItem(IDC_ABSOLUTETIMESPAN);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_ABSOLUTETIMESPAN));
	}
	polWnd = GetDlgItem(IDC_RELATIVETIMESPAN);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_RELATIVETIMESPAN));
	}
	polWnd = GetDlgItem(IDC_TIMEFROM);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_TIMEFROM));
	}
	polWnd = GetDlgItem(IDC_TIMETO);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_TIMETO));
	}
	polWnd = GetDlgItem(IDC_STDVOR);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_STDVOR));
	}
	polWnd = GetDlgItem(IDC_SDTNACH);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_STDNACH));
	}
	polWnd = GetDlgItem(IDC_FLIGHTS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_FLIGHTS));
	}
	polWnd = GetDlgItem(IDC_FLIGHTNR);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_FLIGHTNR));
	}
	polWnd = GetDlgItem(IDC_REGNTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_REGNTEXT));
	}
	polWnd = GetDlgItem(IDC_AIRCRAFTTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_AIRCRAFTTEXT));
	}
	polWnd = GetDlgItem(IDC_ORIGINTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_ORIGINTEXT));
	}
	polWnd = GetDlgItem(IDC_DESTTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_DESTTEXT));
	}
	polWnd = GetDlgItem(IDC_PARAMETERS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_PARAMETERS));
	}
	polWnd = GetDlgItem(IDC_ASSIGNWOALOC);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_ASSIGNWOALOC));
	}
	polWnd = GetDlgItem(IDC_BREAKPRIO);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_BREAKPRIO));
	}
	polWnd = GetDlgItem(IDC_JOBBUFFTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_JOBBUFFTEXT));
	}
	polWnd = GetDlgItem(IDC_SHIFTENDBUFFTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_SHIFTENDBUFFTEXT));
	}
	polWnd = GetDlgItem(IDC_RESOURCEGROUPS);
	if (polWnd)
	{
		CString olTmp;
		olTmp.Format("%s %s",GetString(IDS_AAA_RESOURCEGROUP),omViewer.omStaffViewName);
		polWnd->SetWindowText(olTmp);
	}
	polWnd = GetDlgItem(IDC_REGNGROUPS);
	if (polWnd)
	{
		CString olTmp;
		olTmp.Format("%s %s",GetString(IDS_AAA_REGNGROUPS),omViewer.omRegnAreaViewName);
		polWnd->SetWindowText(olTmp);
	}
	polWnd = GetDlgItem(IDC_STRATEGIES);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_STRATEGIES));
	}
	polWnd = GetDlgItem(IDC_ALL);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_ALL));
	}
	polWnd = GetDlgItem(IDC_OPENONLY);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_OPENONLY));
	}
	polWnd = GetDlgItem(IDC_DELONLY);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_DELONLY));
	}
	polWnd = GetDlgItem(IDC_LEAVEMAN);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_LEAVEMAN));
	}
	polWnd = GetDlgItem(IDCANCEL);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_AAA_CANCEL));
	}

	void *pvlDummy;
	for(int ilLc = 0; ilLc < omPossibleItemsEmp.GetSize();ilLc++)
	{
		olTmp = omPossibleItemsEmp[ilLc];
		ogBasicData.ExtractItemList(olTmp, &olTmpArray,'#');
		if(olTmpArray.GetSize() > 1)
		{
			CString olPool = olTmpArray[0];
			if(omViewer.bmUseAllPools || strlen(olPool) <= 0 || omViewer.omPoolMap.Lookup(olPool, pvlDummy))
			{
				m_PossibleListEmp.AddString(olPool);
			}
		}
	}

	for(ilLc = 0; ilLc < omPossibleItemsRegn.GetSize();ilLc++)
	{
		CString olRegnArea = omPossibleItemsRegn[ilLc];
		if(omViewer.bmUseAllRegnAreas || strlen(olRegnArea) <= 0 || omViewer.omRegnAreaMap.Lookup(olRegnArea, pvlDummy))
		{
			m_PossibleListRegn.AddString(olRegnArea);
		}
	}

	for(ilLc = 0; ilLc < omPossibleItemsStra.GetSize();ilLc++)
	{
		m_PossibleListStra.AddString(omPossibleItemsStra[ilLc]);
	}
	
	SetData();
	
	if(m_AbsTimeVal == 0)
	{
		OnAbstime();
	}
	else
	{
		OnReltime();
	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAutoAssignAz::OnLeaveman() 
{
	omLeaveMan = "1,";
	OnOK();	
}

void CAutoAssignAz::OnOpenonly() 
{
	omOpenOnly = "1,";
	OnOK();	
}

void CAutoAssignAz::OnDelonly() 
{
	omDelOnly = "1,";
	OnOK();
}

void CAutoAssignAz::OnAll() 
{
	OnOK();
}

void CAutoAssignAz::DeleteJobsLeaveManual()
{
	// Cancellare tutti i Job (non manuali) relativi alle demand sugli aerei appartenenti ai gruppi di Regn selezionati

	CString olTmpText;
	CCSPtrArray <ALLOCUNIT> olRegns;
	CCSPtrArray<DEMANDDATA> olDemands;
	DEMANDDATA* prlDemand; 
	
 	for (int ilLc = 0; ilLc < m_SelectedListRegn.GetCount(); ilLc++) 
	{
		m_SelectedListRegn.GetText(ilLc, olTmpText);
		// Cercare tutti gli aerei di questo gruppo
		ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_REGNGROUP,olTmpText,olRegns);
		for(int ilRegn = 0; ilRegn < olRegns.GetSize(); ilRegn++)
		{
			ALLOCUNIT *prlRegn = &olRegns[ilRegn];
 			ACRDATA *pAcr = ogAcrData.GetAcrByName(prlRegn->Name);
			ogDataSet.GetDemandsByUref(olDemands, pAcr->Urno, ACRURNO);
			for (ilLc = 0;ilLc < olDemands.GetSize(); ilLc++)
			{
				prlDemand = &olDemands[ilLc];
				if (prlDemand->Deen > omAssignStart && prlDemand->Debe < omAssignEnd)
				{
					// La demand rientra nell'intervallo da riassegnare
					CCSPtrArray<JOBDATA> olJobs;
					ogJodData.GetJobsByDemand(olJobs, prlDemand->Urno);
					int ilNumJobs = olJobs.GetSize();
					for (int ilJob = 0; ilJob < ilNumJobs; ilJob++)
					{
						if(olJobs[ilJob].Stat[0] == 'P' && (olJobs[ilJob].Usec == JOBHDL || olJobs[ilJob].Usec == JOBHDLC))
						{
							ogDataSet.DeleteJob(this, olJobs[ilJob].Urno);
						}
					}
				}
			}
		}
	}
}

void CAutoAssignAz::DeleteAllJobs()
{


}

void CAutoAssignAz::OnAddEmp() 
{
	CString olText;

	// Move selected items from left list box to right list box
	for (int ilLc = m_PossibleListEmp.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_PossibleListEmp.GetSel(ilLc))	// unselected item?
			continue;
		m_PossibleListEmp.GetText(ilLc, olText);	// load string to "olText"
		
		m_SelectedListEmp.AddString(olText);	// move string from left to right box
		m_PossibleListEmp.DeleteString(ilLc);
	}
}

void CAutoAssignAz::OnAddRegn() 
{
	CString olText;

	// Move selected items from left list box to right list box
	for (int ilLc = m_PossibleListRegn.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_PossibleListRegn.GetSel(ilLc))	// unselected item?
			continue;
		m_PossibleListRegn.GetText(ilLc, olText);	// load string to "olText"
		
		m_SelectedListRegn.AddString(olText);	// move string from left to right box
		m_PossibleListRegn.DeleteString(ilLc);
	}
}

void CAutoAssignAz::OnAddStra() 
{
	CString olText;

	// Move selected items from left list box to right list box
	for (int ilLc = m_PossibleListStra.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_PossibleListStra.GetSel(ilLc))	// unselected item?
			continue;
		m_PossibleListStra.GetText(ilLc, olText);	// load string to "olText"
		
		m_SelectedListStra.AddString(olText);	// move string from left to right box
		m_PossibleListStra.DeleteString(ilLc);
	}
}

void CAutoAssignAz::OnDeleteEmp() 
{
	CString olText;

	for (int ilLc = m_SelectedListEmp.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_SelectedListEmp.GetSel(ilLc))	// unselected item?
			continue;
		m_SelectedListEmp.GetText(ilLc, olText);	// load string to "olText"
		
		m_PossibleListEmp.AddString(olText);	// move string from left to right box
		m_SelectedListEmp.DeleteString(ilLc);
	}
}

void CAutoAssignAz::OnDeleteRegn() 
{
	CString olText;

	for (int ilLc = m_SelectedListRegn.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_SelectedListRegn.GetSel(ilLc))	// unselected item?
			continue;
		m_SelectedListRegn.GetText(ilLc, olText);	// load string to "olText"
		
		m_PossibleListRegn.AddString(olText);	// move string from left to right box
		m_SelectedListRegn.DeleteString(ilLc);
	}
}

void CAutoAssignAz::OnDeleteStra() 
{
	CString olText;

	for (int ilLc = m_SelectedListStra.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_SelectedListStra.GetSel(ilLc))	// unselected item?
			continue;
		m_SelectedListStra.GetText(ilLc, olText);	// load string to "olText"
		
		m_PossibleListStra.AddString(olText);	// move string from left to right box
		m_SelectedListStra.DeleteString(ilLc);
	}
}


//"ONROPS"
//
//e la lista dei campi spediti � 
//
//"ACMD,ASFR,ASTO,FLAG0,FLAG1,FLAG2,FLAG3,POOL,REGG,STRAT,SHEB,JOBU"
//
//Dove:
//
//ACMD =  "ONROPS"   // Command name   (per Malpensa sar� diverso)
//ASFR = "%Y%m%d%H%M%S"    // Assign From
//ASTO = "%Y%m%d%H%M%S"    // Assign To
//FLAG0 = "0" // ??????
//FLAG1 = leave manual  "1" oppure "0"   --> non cancellare i jobs creatu manualmente
//FLAG2 = OpenOnly "1" oppure "0" --> assegnare soltanto i demands non ancora allocati
//FLAG3 = DelOnly "1" oppure "0" --> cancella assegnazioni senza crearne nuovi
//POOL   Lista di Pool Job URNOs format: "123445|123344|123578"
//REGG Lista di NAM format "IDAWN|IDAXC|IDAPD"
//STRAT Lista di strategie format "Strategia3|Strategia5|Strategia7"
//SHEB = Shiftend buffer cio� il numnero di minuti da lasciare tra l'ultimo job e il fine del turno di un dipendente
//JOBU = Job buffer cio� il numero di minuti da lasciare tra jobs per un dipendente
void CAutoAssignAz::OnOK() 
{
	if(GetData() && UpdateData(TRUE))
	{
		omAssignStart = ogBasicData.GetLocalTime() + CTimeSpan(0,1,0,0);
		omAssignEnd = omAssignStart + CTimeSpan(0,1,0,0);
		if(m_AbsTimeVal == 0)
		{
			if(!omValues[0].IsEmpty() && !omValues[1].IsEmpty())
			{
				omAssignStart = DBStringToDateTime(omValues[0]);
				omAssignEnd = DBStringToDateTime(omValues[1]);
			}
		}
		else
		{
			if(!omValues[2].IsEmpty() && !omValues[3].IsEmpty())
			{
				omAssignStart = ogBasicData.GetLocalTime() + CTimeSpan(0,atoi(omValues[2]),0,0);
				omAssignEnd = omAssignStart + CTimeSpan(0,atoi(omValues[3]),0,0);
			}
		}

	
		CString olDemList;
		CString olDemUrno;

		CString olDataList;
		CString olFieldList;

		if((omAssignEnd > omLoadEnd) || (omAssignStart < omLoadStart) )
		{
			CString olText;

			// the assignment time (From: %s To: %s)\nmust be within the display time (From: %s To: %s)
			olText.Format(GetString(IDS_TIMEOUTSIDETIMEFRAME),omAssignStart.Format("%d.%m.%Y %H:%M"),omAssignEnd.Format("%d.%m.%Y %H:%M"),omLoadStart.Format("%d.%m.%Y %H:%M"),omLoadEnd.Format("%d.%m.%Y %H:%M"));
			MessageBox(olText, GetString(IDS_AUTOASSIGNAZ), MB_OK);
			return ;
		}

		if(omAssignEnd < omAssignStart)
		{
			CString olText;

			// the assignment begin (%s) must be before the assignment end (%s)
			olText.Format(GetString(IDS_BEGINBEFOREEND),omAssignStart.Format("%d.%m.%Y %H:%M"),omAssignEnd.Format("%d.%m.%Y %H:%M"));
			MessageBox(olText, GetString(IDS_AUTOASSIGNAZ), MB_OK);
			return ;
		}

		// OK fino qui, c'� solo da capire dove va usato omAssignEnd e start

	/*
		llDemCount = 0;
		for(int ilLc = 0; ilLc < omDemUrnoList.GetSize(); ilLc++)
		{
			if(IsDemPassFilter(ogDemandData.GetDemandByUrno(omDemUrnoList[ilLc])))
			{
				olDemUrno.Format("'%ld'", omDemUrnoList[ilLc]);
				if(olDemList.IsEmpty())
				{
					olDemList = olDemUrno;
				}
				else
				{
					olDemList += ",";
					olDemList += olDemUrno;
				}
				llDemCount++;
			}
		}
	*/
		CString olTmpText;
		CString olResUrno;
		CString olResList;
		CString olRegnList;
		CString olStraList;
		
		bool blFound = true;
		CStringArray olTmpArray;
		
		// Lista dei pool
 		for (int ilLc = 0; ilLc < m_SelectedListEmp.GetCount(); ilLc++) 
		{ 
			m_SelectedListEmp.GetText(ilLc, olTmpText);
			if(!olResList.IsEmpty())
			{
				olResList += "|";
			}
			olResList += olTmpText;
		}

		// Lista dei TAM
 		for (ilLc = 0; ilLc < m_SelectedListRegn.GetCount(); ilLc++) 
		{
			m_SelectedListRegn.GetText(ilLc, olTmpText);
			olRegnList += olTmpText;
			if (ilLc < (m_SelectedListRegn.GetCount() -1))
			{
				olRegnList += "|";
			}
		}

		// Lista delle Strategie
 		for (ilLc = 0; ilLc < m_SelectedListStra.GetCount(); ilLc++) 
		{
			m_SelectedListStra.GetText(ilLc, olTmpText);
			olStraList += olTmpText;
			if (ilLc < (m_SelectedListStra.GetCount() -1))
			{
				olStraList += "|";
			}
		}



		// Riempire i dati secondo questo schema

		/* 0 ACMD   Nome del comando                            String
		   1 ASFR   Assegnare da                                YYYYMMDDhhmmss
		   2 ASTO   Assegnare fino                              YYYYMMDDhhmmss
		   3 FLAG0  Flag per Long Term Planning (future use)    1 = Si, 0 = No
		   4 FLAG1  Flag per Lasciare le assegnazioni manuali   1 = Si, 0 = No
		   5 FLAG2  Flag per assegnare solo le open demand      1 = Si, 0 = No
		   6 FLAG3  Flag per Cancellare solamente tutti i job	1 = Si, 0 = No
		   7 POOL   Nomi dei pool                               Stringhe separate da |
		   8 REGG   Nomi dei gruppi di registrazioni            Stringhe separate da |
		   9 STRAT  Strategie (max 2)                           Stringhe separate da |
		   10 SHEB  Shift End Buffer
		   11 JOBU  Job Buffer
		*/

//		if (omDelOnly == "1,")
//		{
//			DeleteAllJobs();
//		}
//		else
		if (m_SelectedListEmp.GetCount() > 0 && m_SelectedListRegn.GetCount() > 0 && m_SelectedListStra.GetCount() > 0)
		{
			CString olTmpFlag;

			olFieldList = "ACMD,ASFR,ASTO,FLAG0,FLAG1,FLAG2,FLAG3,POOL,REGG,STRAT,SHEB,JOBU";

			olDataList += "ONROPS,";   // Command name   (per Malpensa sar� diverso)
			olDataList += omAssignStart.Format("%Y%m%d%H%M%S") + ",";    // Assign From
			olDataList += omAssignEnd.Format("%Y%m%d%H%M%S") + ",";    // Assign To
			olDataList += "0,";
			olDataList += omLeaveMan;
			olDataList += omOpenOnly;
			olDataList += omDelOnly;
			olDataList += olResList + ",";
			olDataList += olRegnList + ",";
			olDataList += olStraList + ",";
			olDataList += m_JobBuff + ",";
			olDataList += m_ShiftEndBuff;


			// Mancano da aggiungere i campi: m_BreakPrio, m_AssignWOAloc
			// e quelli relativi al volo, che per adesso potrebbero anche essere disabilitati
			
//			if (omLeaveMan == "1,")
//			{
//				DeleteJobsLeaveManual();
//			}
//			else
//			if (omOpenOnly == "0,")
//			{
//				DeleteAllJobs();
//			}
			
			ogJobData.AutoAssignDemandsRegn(olFieldList,olDataList);
			// force conflict check and diagram update
			pogButtonList->PostMessage(WM_TIMER,0,0L);	
		
			CDialog::OnOK();
		}
		else
		{
			CString olError;
			// Mandare un messaggio di errore
			if (m_SelectedListEmp.GetCount() <= 0)
			{
				olError += GetString(IDS_AUTOASSAZ_NOEMPS) + CString ("\n");
			}
			if(m_SelectedListRegn.GetCount() <= 0)
			{
				olError += GetString(IDS_AUTOASSAZ_NOREGN) + CString ("\n");
			}
			if(m_SelectedListStra.GetCount() <= 0)
			{
				olError += GetString(IDS_AUTOASSAZ_NOSTRAT) + CString ("\n");
			}
			MessageBox(olError,"",MB_ICONERROR);
		}
	}
}

void CAutoAssignAz::SetData(CStringArray &ropResourceGroupArray, CStringArray &ropRegnGroupArray, CTime opAssignFrom, CTime opAssignTo)
{
	omPossibleItemsEmp.RemoveAll();
	omSelectedItemsEmp.RemoveAll();
	omPossibleItemsRegn.RemoveAll();
	omSelectedItemsRegn.RemoveAll();
	omPossibleItemsStra.RemoveAll();
	omSelectedItemsStra.RemoveAll();

	for(int ilLc = 0; ilLc < ropResourceGroupArray.GetSize();ilLc++)
	{
		omPossibleItemsEmp.Add(ropResourceGroupArray[ilLc]);
	}
	
	for(ilLc = 0; ilLc < ropRegnGroupArray.GetSize();ilLc++)
	{
		omPossibleItemsRegn.Add(ropRegnGroupArray[ilLc]);
	}

	ogPrmData.GetAllPrmNames(omPossibleItemsStra);

//	m_ToTime = opAssignTo.Format("%H:%M");
//	m_ToDay = opAssignTo.Format("%d.%m.%Y");
//	m_FromDay = opAssignFrom.Format("%d.%m.%Y");
//	m_FromTime = opAssignFrom.Format("%H:%M");
	m_FlightOrig = "";
	m_FlightRegn = "";
	m_FlightNumber = "";
	m_FlightDest = "";
	m_FlightAirline = "";
	m_FlightAircraft = "";
	m_FlightSuffix = "";

	bmSetWarning = false;

}


void CAutoAssignAz::OnAbstime() 
{
	m_FromDayCtrl.EnableWindow(TRUE);
	m_FromTimeCtrl.EnableWindow(TRUE);
	m_ToDayCtrl.EnableWindow(TRUE);
	m_ToTimeCtrl.EnableWindow(TRUE);
	m_RelFromCtrl.EnableWindow(FALSE);
	m_RelToCtrl.EnableWindow(FALSE);
}

void CAutoAssignAz::OnReltime() 
{
	m_FromDayCtrl.EnableWindow(FALSE);
	m_FromTimeCtrl.EnableWindow(FALSE);
	m_ToDayCtrl.EnableWindow(FALSE);
	m_ToTimeCtrl.EnableWindow(FALSE);
	m_RelFromCtrl.EnableWindow(TRUE);
	m_RelToCtrl.EnableWindow(TRUE);
}


BOOL CAutoAssignAz::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}

BOOL CAutoAssignAz::GetData(CStringArray &ropValues)
{
	CString olT1, olT2, olT3, olT4;
	CString olResult;
	CTime olDate, olTime;
	m_FromDayCtrl.GetWindowText(olT1);// TIFA From || TIFD From; Date
	m_FromTimeCtrl.GetWindowText(olT2);
	m_ToDayCtrl.GetWindowText(olT3);
	m_ToTimeCtrl.GetWindowText(olT4);
	//Erst mal einen precheck und was leer ist wird 
	//entsprechend gesetzt

	if(olT1.IsEmpty() && olT2.IsEmpty() && olT3.IsEmpty() && olT4.IsEmpty())
	{
		;
	}
	else
	{
		if(olT2.IsEmpty())
		{
			olT2 = CString("00:00");
			m_FromTimeCtrl.SetWindowText(olT2);
		}
		if(olT4.IsEmpty())
		{
			olT4 = CString("23:59");
			m_ToTimeCtrl.SetWindowText(olT4);
		}
		if(olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT1 = CTime::GetCurrentTime().Format("%d.%m.%Y");
			olT3 = olT1;
			m_FromDayCtrl.SetWindowText(olT1);
			m_ToDayCtrl.SetWindowText(olT3);
		}
		if(!olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT3 = olT1;
			m_ToDayCtrl.SetWindowText(olT3);
		}
		if(olT1.IsEmpty() && !olT3.IsEmpty())
		{
			olT1 = olT3;
			m_FromDayCtrl.SetWindowText(olT1);
		}
	}
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olDate != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
		{
			CString olText;
			olText = GetString(IDS_STRING61346); // invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText;
				olText = GetString(IDS_STRING61346); // invalid date
				MessageBox(olText, "", MB_OK);
				return FALSE;//olT1 = " ";
			}
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_ToDayCtrl.GetWindowText(olT1);// TIFA To || TIFD To; Date
	m_ToTimeCtrl.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olDate != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
		{
			CString olText;
			olText = GetString(IDS_STRING61346);// invalid date
			MessageBox(olText, "", MB_OK);
			return FALSE;//olT1 = " ";
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText;
				olText = GetString(IDS_STRING61346); // invalid date
				MessageBox(olText, "", MB_OK);
				return FALSE;//olT1 = " ";
			}
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_RelFromCtrl.GetWindowText(olT1);// RelHBefore
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_RelToCtrl.GetWindowText(olT1);// RelHAfter
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	return true;
}

void CAutoAssignAz::SetData()
{
	CString olTmpTime, olTmpTime2, olTmpTime3;
	char pclTmpText[512];
	CString olConfigFileName = ogBasicData.GetConfigFileName();

	GetPrivateProfileString(pcgAppName, "AUTOASSIGNAZ_REL", "YES",pclTmpText, sizeof pclTmpText, olConfigFileName);
	m_AbsTimeVal = !strcmp(pclTmpText,"YES") ? 1 : 0;

	GetPrivateProfileString(pcgAppName, "AUTOASSIGNAZ_OFFSET", "1",pclTmpText, sizeof pclTmpText, olConfigFileName);
	olTmpTime2 = pclTmpText;
	int ilTimeoffset = atoi(pclTmpText);
	CTime olLoadStartTime = ogBasicData.GetTime();
	olLoadStartTime += CTimeSpan(0,ilTimeoffset,0,0);

	olTmpTime = olLoadStartTime.Format("%Y%m%d%H%M%S");
	omValues.Add(olTmpTime);

	GetPrivateProfileString(pcgAppName, "AUTOASSIGNAZ_DURATION", "3", pclTmpText, sizeof pclTmpText, olConfigFileName);
	olTmpTime3 = pclTmpText;
	int ilTimeduration = atoi(pclTmpText);
	CTime olLoadEndTime = olLoadStartTime + CTimeSpan(0,ilTimeduration,0,-1);

	olTmpTime = olLoadEndTime.Format("%Y%m%d%H%M%S");
	omValues.Add(olTmpTime);


	omValues.Add(olTmpTime2);
	omValues.Add(olTmpTime3);

	SetData(omValues);
}

void CAutoAssignAz::SetData(CStringArray &ropValues)
{
	m_FromDayCtrl.SetWindowText("");// TIFA From || TIFD From; Date
    m_FromTimeCtrl.SetWindowText("");// TIFA From || TIFD From; Time
    m_ToDayCtrl.SetWindowText("");// TIFA To || TIFD To; Date
    m_ToTimeCtrl.SetWindowText("");// TIFA To || TIFD To; Time
    m_RelFromCtrl.SetWindowText("");// RelHBefore
    m_RelToCtrl.SetWindowText("");// RelHAfter
 	bool blFlag = false;
	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		CTime olDate = TIMENULL;
		CString olV = ropValues[i];
		if(!olV.IsEmpty())
			blFlag = true;
		switch(i)
		{
		case 0:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_FromDayCtrl.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_FromTimeCtrl.SetWindowText(olDate.Format("%H:%M"));
				m_FromDay = olDate.Format("%d.%m.%Y");
				m_FromTime = olDate.Format("%H:%M");
			}
			else
			{
				m_FromDayCtrl.SetWindowText(CString(""));
				m_FromTimeCtrl.SetWindowText(CString(""));
			}
			break;
		case 1:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_ToDayCtrl.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_ToTimeCtrl.SetWindowText(olDate.Format("%H:%M"));
				m_ToDay = olDate.Format("%d.%m.%Y");
				m_ToTime = olDate.Format("%H:%M");
			}
			else
			{
				m_ToDayCtrl.SetWindowText(CString(""));
				m_ToTimeCtrl.SetWindowText(CString(""));
			}
			break;
		case 2:
			if(ropValues[i] != CString(" "))
				m_RelFromCtrl.SetWindowText(ropValues[i]);
			break;
		case 3:
			if(ropValues[i] != CString(" "))
				m_RelToCtrl.SetWindowText(ropValues[i]);
			break;

		}
	}
}
