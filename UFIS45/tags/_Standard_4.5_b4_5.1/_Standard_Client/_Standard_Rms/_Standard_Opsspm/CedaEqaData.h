// Class for Eqaipment
#ifndef _CEDAEQADATA_H_
#define _CEDAEQADATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

#define CEDAEQADATA_VALU_LEN 40

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct EqaDataStruct
{
	long	Urno;		// Unique Record Number
	long	Uequ;		// URNO of equipment (EQATAB.URNO)
	char	Name[21];	// name of value
	char	Valu[CEDAEQADATA_VALU_LEN+1];	// value

    char    Usec[33];	// Creator
    CTime   Cdat;		// Creation Date
    char    Useu[33];	// Updater
    CTime   Lstu;		// Update Date

	EqaDataStruct(void)
	{
		Urno = 0L;
		strcpy(Name,"");
		strcpy(Valu,"");
		strcpy(Usec,"");
		Cdat=TIMENULL;
		strcpy(Useu,"");
		Lstu=TIMENULL;
	}
};

typedef struct EqaDataStruct EQADATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaEqaData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <EQADATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUequMap;

// Operations
public:
	CedaEqaData();
	~CedaEqaData();

	CCSReturnCode ReadEqaData();
	EQADATA* GetEqaByUrno(long lpUrno);
	bool GetEqasByUequ(long lpUequ, CCSPtrArray <EQADATA> &ropEqaList, bool bpReset = true);

	CString GetTableName(void);
	CString Dump(long lpUrno);

	EQADATA *InsertEqa(const char *pcpName, const char *pcpValu);
	void UpdateEqa(long lpUrno, const char *pcpValu);
	void DeleteEqa(long lpUrno);
	void ProcessEqaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	bool GetChangedFields(EQADATA *prpEqa, EQADATA *prpOldEqa, char *pcpFieldList, char *pcpData);
	void PrepareDataForWrite(EQADATA *prpEqa);
	EQADATA *AddEqaInternal(EQADATA &rrpEqa);
	void ClearAll();
	void DeleteEqaInternal(EQADATA *prpEqa);
};


extern CedaEqaData ogEqaData;
#endif _CEDAEQADATA_H_
