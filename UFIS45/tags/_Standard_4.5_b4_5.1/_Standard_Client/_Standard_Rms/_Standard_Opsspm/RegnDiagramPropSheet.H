// RegnDiagramPropSheet.h : header file
//

#ifndef _REGDIAPS_H_
#define _REGDIAPS_H_

/////////////////////////////////////////////////////////////////////////////
// RegnDiagramPropertySheet

class RegnDiagramPropertySheet : public BasePropertySheet
{
// Construction
public:
	RegnDiagramPropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pageRegnArea;
	FilterPage m_pageRegnTempl;
	FilterPage m_pageThirdPartyFlights;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _REGDIAPS_H_
