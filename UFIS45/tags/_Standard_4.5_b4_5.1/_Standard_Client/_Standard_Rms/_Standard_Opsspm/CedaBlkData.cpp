// CedaBlkData.cpp - Class for Airline Types
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaBlkData.h>
#include <BasicData.h>
#include <Conflict.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

static int ByStartTime(const BLKBLOCKEDTIMES **pppBlkTime1, const BLKBLOCKEDTIMES **pppBlkTime2);
static int ByStartTime(const BLKBLOCKEDTIMES **pppBlkTime1, const BLKBLOCKEDTIMES **pppBlkTime2)
{
	return (int)((**pppBlkTime1).Tifr.GetTime() - (**pppBlkTime2).Tito.GetTime());
}

void  ProcessBlkCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaBlkData::CedaBlkData()
{                  
    BEGIN_CEDARECINFO(BLKDATA, BlkDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Tabn,"TABN")
		FIELD_CHAR_TRIM(Days,"DAYS")
		FIELD_LONG(Burn,"BURN")
        FIELD_DATE(Nafr,"NAFR")
        FIELD_DATE(Nato,"NATO")
        FIELD_CHAR_TRIM(Tifr,"TIFR")
        FIELD_CHAR_TRIM(Tito,"TITO")
		FIELD_CHAR_TRIM(Resn,"RESN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(BlkDataRecInfo)/sizeof(BlkDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BlkDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this,BC_BLK_CHANGE,CString("BLKDATA"), CString("Blk changed"),ProcessBlkCf);
	ogCCSDdx.Register((void *)this,BC_BLK_DELETE,CString("BLKDATA"), CString("Blk deleted"),ProcessBlkCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"BLKTAB");
    pcmFieldList = "URNO,TABN,DAYS,BURN,NAFR,NATO,TIFR,TITO,RESN";
}
 
CedaBlkData::~CedaBlkData()
{
	ogCCSDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void  ProcessBlkCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaBlkData *)popInstance)->ProcessBlkBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaBlkData::ProcessBlkBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBlkData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlBlkData);

	long llUrno = GetUrnoFromSelection(prlBlkData->Selection);
	if(llUrno == 0L && ipDDXType == BC_BLK_CHANGE)
	{
		BLKDATA rlBlk;
		GetRecordFromItemList(&omRecInfo,&rlBlk,prlBlkData->Fields,prlBlkData->Data);
		llUrno = rlBlk.Urno;
	}
	
	BLKDATA *prlBlk = GetBlkByUrno(llUrno);
	BLKDATA rlOriginalBlk;
	if(ipDDXType == BC_BLK_CHANGE && prlBlk != NULL)
	{
		// update
		ConvertDatesToUtc(*prlBlk);
		GetRecordFromItemList(prlBlk,prlBlkData->Fields,prlBlkData->Data);
		ConvertDatesToLocal(*prlBlk);
		HandleConflictChecks(prlBlk);
		ogCCSDdx.DataChanged((void *)this,BLK_CHANGE,(void *)prlBlk);
	}
	else if(ipDDXType == BC_BLK_CHANGE && prlBlk == NULL)
	{
		// insert
		BLKDATA rlBlk;
		GetRecordFromItemList(&rlBlk,prlBlkData->Fields,prlBlkData->Data);
		prlBlk = AddBlkInternal(rlBlk);
		HandleConflictChecks(prlBlk);
		ogCCSDdx.DataChanged((void *)this,BLK_CHANGE,(void *)prlBlk);
	}
	else if(ipDDXType == BC_BLK_DELETE && prlBlk != NULL)
	{
		// delete
		rlOriginalBlk = *prlBlk;
		DeleteBlkInternal(prlBlk);
		HandleConflictChecks(&rlOriginalBlk);
		ogCCSDdx.DataChanged((void *)this,BLK_DELETE,(void *) &rlOriginalBlk);
	}
}

void CedaBlkData::HandleConflictChecks(BLKDATA *prpBlk)
{
	if(prpBlk != NULL)
	{
		CCSPtrArray <JOBDATA> olJobs;
		if(!strcmp(prpBlk->Tabn, "EQU"))
		{
			ogJobData.GetJobsByUequ(olJobs, prpBlk->Burn);
			int ilNumJobs = olJobs.GetSize();
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				ogConflicts.CheckConflicts(olJobs[ilJ],FALSE,TRUE,TRUE,FALSE);
			}
		}
	}
}

void CedaBlkData::ClearAll()
{
	omUrnoMap.RemoveAll();
	POSITION pos = omTableMap.GetStartPosition();
	while(pos)
	{
		CString olKey;
		CMapPtrToPtr *polTableMap = NULL;
		omTableMap.GetNextAssoc(pos,olKey,(void *&)polTableMap);
		if (polTableMap)
		{
			POSITION pos2 = polTableMap->GetStartPosition();
			while(pos2)
			{
				void *polKey;
				CCSPtrArray<BLKDATA> *polBlocked = NULL;
				polTableMap->GetNextAssoc(pos2,polKey,(void *&)polBlocked);
				delete polBlocked;
			}
			polTableMap->RemoveAll();
			delete polTableMap;
		}
	}
	omTableMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaBlkData::ReadBlkData(const CTime& ropStartTime,const CTime& ropEndTime)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];

//	sprintf(pclWhere, "WHERE NAFR <= '%s' AND '%s' <= NATO",ropEndTime.Format("%Y%m%d%H%M%S"),ropStartTime.Format("%Y%m%d%H%M%S"));

	sprintf(pclWhere, "WHERE NAFR <= '%s'",ropEndTime.Format("%Y%m%d%H%M%S"));

    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaBlkData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		BLKDATA rlBlkData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlBlkData)) == RCSuccess)
			{
				if (rlBlkData.Nato == TIMENULL || ropStartTime <= rlBlkData.Nato)
					AddBlkInternal(rlBlkData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaBlkData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(BLKDATA), pclWhere);
    return ilRc;
}


BLKDATA *CedaBlkData::AddBlkInternal(BLKDATA &rrpBlk)
{
	ConvertDatesToLocal(rrpBlk);

	BLKDATA *prlBlk = new BLKDATA;
	*prlBlk = rrpBlk;
	omData.Add(prlBlk);
	omUrnoMap.SetAt((void *)prlBlk->Urno,prlBlk);

	CMapPtrToPtr *polTableMap = NULL;
	if (!omTableMap.Lookup(prlBlk->Tabn,(void *&)polTableMap) || !polTableMap)
	{
		polTableMap = new CMapPtrToPtr;
		omTableMap.SetAt(prlBlk->Tabn,polTableMap);
	}

	if (polTableMap)
	{
		CCSPtrArray<BLKDATA> *polBlocked = NULL;
		if (!polTableMap->Lookup((void *)prlBlk->Burn,(void *&)polBlocked) || !polBlocked)
		{
			polBlocked = new CCSPtrArray<BLKDATA>;
			polTableMap->SetAt((void *)prlBlk->Burn,polBlocked);
		}

		if (polBlocked)
		{
			polBlocked->Add(prlBlk);
		}
	}

	return prlBlk;
}

void CedaBlkData::DeleteBlkInternal(BLKDATA *prpBlk)
{
	CMapPtrToPtr *polTableMap;
	if(omTableMap.Lookup(prpBlk->Tabn,(void *& )polTableMap))
	{
		CCSPtrArray <BLKDATA> *polBlocked = NULL;
		if (polTableMap->Lookup((void *)prpBlk->Burn,(void *&)polBlocked) && polBlocked)
		{
			int ilNumRecs = polBlocked->GetSize();
			for(int ilR = (ilNumRecs-1); ilR >= 0; ilR--)
			{
				BLKDATA *prlBlk = &((*polBlocked)[ilR]);
				if(prlBlk->Urno == prpBlk->Urno)
				{
					polBlocked->RemoveAt(ilR);
				}
			}
		}
	}

	long llUrno = prpBlk->Urno;
	int ilCount = omData.GetSize();
	for (int ilLc = ilCount-1; ilLc >= 0; ilLc--)
	{
		if (omData[ilLc].Urno == llUrno)
		{
			omUrnoMap.RemoveKey((void *)llUrno);
			omData.DeleteAt(ilLc);
		}
	}
}

BLKDATA* CedaBlkData::GetBlkByUrno(long lpUrno)
{
	BLKDATA *prlBlk = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlBlk);
	return prlBlk;
}

// Return all the BLKTAB records for an object: combination of table name and URNO.
// There can be more than one blocked time per object.
int	CedaBlkData::GetBlocked(const CString& ropTable,long lpUrno,CCSPtrArray<BLKDATA>& ropBlocked)
{
	CMapPtrToPtr *polTableMap = NULL;
	if (omTableMap.Lookup(ropTable,(void *&)polTableMap) && polTableMap)
	{
		CCSPtrArray<BLKDATA> *polBlocked = NULL;
		if (polTableMap->Lookup((void *)lpUrno,(void *&)polBlocked) && polBlocked)
		{
			ropBlocked = *polBlocked;
			return ropBlocked.GetSize();
		}
		else
			return 0;
	}
	else
		return 0;
}

CString CedaBlkData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaBlkData::ConvertDatesToLocal(BLKDATA &rrpBlk)
{
	ogBasicData.ConvertDateToLocal(rrpBlk.Nafr);
	ogBasicData.ConvertDateToLocal(rrpBlk.Nato);
}

void CedaBlkData::ConvertDatesToUtc(BLKDATA &rrpBlk)
{
	ogBasicData.ConvertDateToUtc(rrpBlk.Nafr);
	ogBasicData.ConvertDateToUtc(rrpBlk.Nato);
}

int	CedaBlkData::GetBlockedTimes(BLKDATA *prpBlk,const CTime& ropStartTime,const CTime& ropEndTime,CCSPtrArray<BLKBLOCKEDTIMES>& ropBlockedTimes)
{
	if (!prpBlk)
		return 0;

	CString	olDays(prpBlk->Days);
	CTimeSpan ol1Min = CTimeSpan(0,0,1,0);

	CTime olStartTime = ropStartTime;		
	if (prpBlk->Nafr > olStartTime)
		olStartTime = prpBlk->Nafr;
				
	CTime olEndTime   = ropEndTime;		
	if (prpBlk->Nato != TIMENULL && prpBlk->Nato < olEndTime)
		olEndTime = prpBlk->Nato;

	// convert Tifr and Tito from UTC to local
	CString olLocalTifr, olLocalTito;
	CTime olNow = CTime::GetCurrentTime(); // any day is OK
	if(strlen(prpBlk->Tifr) > 0)
	{
		CTime olTmpTifr = HourStringToDate(CString(prpBlk->Tifr),olNow);
		ogBasicData.ConvertDateToLocal(olTmpTifr);
		olLocalTifr = olTmpTifr.Format("%H%M");
	}
	if(strlen(prpBlk->Tito) > 0)
	{
		CTime olTmpTito = HourStringToDate(CString(prpBlk->Tito),olNow);
		ogBasicData.ConvertDateToLocal(olTmpTito);
		olLocalTito = olTmpTito.Format("%H%M");
	}

	olStartTime = CTime(olStartTime.GetYear(),olStartTime.GetMonth(),olStartTime.GetDay(),0,0,0);
	olEndTime   = CTime(olEndTime.GetYear(),olEndTime.GetMonth(),olEndTime.GetDay(),23,59,59);
	for (CTime olCurrent = olStartTime; olCurrent <= olEndTime; olCurrent += CTimeSpan(1,0,0,0))
	{
		int ilDayOfWeek = olCurrent.GetDayOfWeek() - 1;
		if (ilDayOfWeek == 0)
			ilDayOfWeek = 7;

		if (olDays.Find((char) (ilDayOfWeek + '0')) != -1)
		{
			// if Tifr is empty or before Nafr then use Nafr
			CTime olTifr = prpBlk->Nafr;
			if(!olLocalTifr.IsEmpty())
			{
				CTime olTmpTifr = HourStringToDate(olLocalTifr,olCurrent);
				if(olTmpTifr > olTifr)
				{
					olTifr = olTmpTifr;
				}
			}

			// if Titi is empty or after Nato then use Nato
			CTime olTito = (prpBlk->Nato != TIMENULL) ? prpBlk->Nato : ogBasicData.GetTimeframeEnd();
			if(!olLocalTito.IsEmpty())
			{
				CTime olTmpTito = HourStringToDate(olLocalTito,olCurrent);

				if(olTmpTito < olTifr)
				{
					// if timeTo is before timeFrom then add one day to timeTo - ie the blocking time is before and after midnight
					olTmpTito = HourStringToDate(olLocalTito,olCurrent+CTimeSpan(1,0,0,0));
				}

				if(olTmpTito < olTito)
				{
					olTito = olTmpTito;
				}
			}

			// make sure that the bar times are within the specified timeframe
			if(olTifr < ropStartTime)
				olTifr = ropStartTime;

			if(olTito > ropEndTime)
				olTito = ropEndTime;

			CTime olMinTime = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),0,0,0);
			if(olTifr < olMinTime)
				olTifr = olMinTime;

			CTime olMaxTime = CTime(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),23,59,59);
			if(olTito > olMaxTime)
				olTito = olMaxTime;


			// check if we can merge this bar with existing overlapping bars (they are already ordered by time)
			bool blNotMerged = true;
			int ilNumBarsSoFar = ropBlockedTimes.GetSize();
			for(int ilB = 0; ilB < ilNumBarsSoFar; ilB++)
			{
				BLKBLOCKEDTIMES *prlBar = &ropBlockedTimes[ilB];
				if(IsOverlapped(prlBar->Tifr, prlBar->Tito, olTifr-ol1Min, olTito+ol1Min))
				{
					if(olTifr < prlBar->Tifr)
						prlBar->Tifr = olTifr;
					if(olTito > prlBar->Tito)
						prlBar->Tito = olTito;
					blNotMerged = false;
				}
			}

			if(blNotMerged && olTifr < olTito)
				ropBlockedTimes.New(BLKBLOCKEDTIMES(olTifr,olTito));
		}
	}
	
	return ropBlockedTimes.GetSize();
}

//int	CedaBlkData::GetBlockedTimes(BLKDATA *prpBlk,const CTime& ropStartTime,const CTime& ropEndTime,CCSPtrArray<BLKBLOCKEDTIMES>& ropBlockedTimes)
//{
//	if (!prpBlk)
//		return 0;
//
//	CString	olDays(prpBlk->Days);
//
//	CTime olTimeframeStart	= ogBasicData.GetTimeframeStart();
//	CTime olTimeframeEnd	= ogBasicData.GetTimeframeEnd();
//
//	CTime olStartTime = ropStartTime;		
//	if (prpBlk->Nafr > olStartTime)
//		olStartTime = prpBlk->Nafr;
//				
//	if (olStartTime > olTimeframeStart)
//		olTimeframeStart = olStartTime;
//
//	CTime olEndTime   = ropEndTime;		
//	if (prpBlk->Nato != TIMENULL && prpBlk->Nato < olEndTime)
//		olEndTime = prpBlk->Nato;
//
//	if (olEndTime < olTimeframeEnd)
//		olTimeframeEnd = olEndTime;
//
//
//	// convert Tifr and Tito from UTC to local
//	CString olLocalTifr, olLocalTito;
//	CTime olNow = CTime::GetCurrentTime(); // any day is OK
//	if(strlen(prpBlk->Tifr) > 0)
//	{
//		CTime olTmpTifr = HourStringToDate(CString(prpBlk->Tifr),olNow);
//		ogBasicData.ConvertDateToLocal(olTmpTifr);
//		olLocalTifr = olTmpTifr.Format("%H%M");
//	}
//	if(strlen(prpBlk->Tito) > 0)
//	{
//		CTime olTmpTito = HourStringToDate(CString(prpBlk->Tito),olNow);
//		ogBasicData.ConvertDateToLocal(olTmpTito);
//		olLocalTito = olTmpTito.Format("%H%M");
//	}
//
//	olStartTime = CTime(olStartTime.GetYear(),olStartTime.GetMonth(),olStartTime.GetDay(),0,0,0);
//	olEndTime   = CTime(olEndTime.GetYear(),olEndTime.GetMonth(),olEndTime.GetDay(),23,59,59);
//
//	for (CTime olCurrent = olStartTime; olCurrent <= olEndTime; olCurrent += CTimeSpan(1,0,0,0))
//	{
//		int ilDayOfWeek = olCurrent.GetDayOfWeek() - 1;
//		if (ilDayOfWeek == 0)
//			ilDayOfWeek = 7;
//
//		if (olDays.Find((char) (ilDayOfWeek + '0')) != -1)
//		{
//			// if Tifr is empty or before Nafr then use Nafr
//			CTime olTifr;
//			if (olCurrent.GetMonth() == olTimeframeStart.GetMonth() && olCurrent.GetDay() == olTimeframeStart.GetDay())
//			{
//				olTifr = olTimeframeStart;
//			}
//			else
//			{
//				olTifr = olCurrent;
//			}
//
//			if(strlen(olLocalTifr) > 0)
//			{
//				CTime olTmpTifr = HourStringToDate(CString(olLocalTifr),olCurrent);
//				if(olTmpTifr > olTifr)
//				{
//					olTifr = olTmpTifr;
//				}
//			}
//
//			// if Tito is empty or after Nato then use Nato
//			CTime olTito;
//			CTime olNext = olCurrent + CTimeSpan(1,0,0,0);
//			if (olNext.GetMonth() == olTimeframeEnd.GetMonth() && olNext.GetDay() == olTimeframeEnd.GetDay())
//			{
//				olTito = olTimeframeEnd;
//			}
//			else
//			{
//				olTito = olNext;
//			}
//
//			if(strlen(olLocalTito) > 0)
//			{
//				CTime olTmpTito = HourStringToDate(CString(olLocalTito),olCurrent);
//
//				if(olTmpTito < olTifr)
//				{
//					// if timeTo is before timeFrom then add one day to timeTo - ie the blocking time is before and after midnight
//					olTmpTito = HourStringToDate(CString(olLocalTito),olCurrent+CTimeSpan(1,0,0,0));
//				}
//
//				if(olTmpTito < olTito || olTito < olTifr)
//				{
//					olTito = olTmpTito;
//				}
//			}
//			ropBlockedTimes.New(BLKBLOCKEDTIMES(olTifr,olTito));
//		}
//	}
//	
//	return ropBlockedTimes.GetSize();
//}

BOOL CedaBlkData::IsBlockedAt(BLKDATA *prpBlk,const CTime& ropStart,const CTime& ropEnd)
{
	BOOL blOverlapped = FALSE;

	if (!prpBlk)
		return blOverlapped;

	CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;
	if (GetBlockedTimes(prpBlk,ropStart,ropEnd,olBlockedTimes))	
	{
		for (int ilC = 0; ilC < olBlockedTimes.GetSize(); ilC++)
		{
			if (IsReallyOverlapped(olBlockedTimes[ilC].Tifr,olBlockedTimes[ilC].Tito,ropStart,ropEnd))
			{
				blOverlapped = TRUE;
				break;
			}
		}

		olBlockedTimes.DeleteAll();
	}

	return blOverlapped;
}

// return true if the object is completely blocked within the period opFrom/opTo
bool CedaBlkData::IsCompletelyBlocked(const CString& ropTable, long lpUrno, CTime opFrom, CTime opTo)
{
	bool blIsCompletelyBlocked = false;

	int ilNumBlockedTimes = 0, ilBT = 0;

	CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;

	CCSPtrArray <BLKDATA> olBlocked;
	if(ogBlkData.GetBlocked(ropTable, lpUrno, olBlocked) > 0)
	{
		int ilNumBlocked = olBlocked.GetSize();
		for (int ilC = 0; ilC < ilNumBlocked; ilC ++)
		{
			BLKDATA *prlBlk = &olBlocked[ilC];
			CCSPtrArray <BLKBLOCKEDTIMES> olTmpBlockedTimes;
			if(ogBlkData.GetBlockedTimes(prlBlk,opFrom,opTo,olTmpBlockedTimes) > 0)
			{
				ilNumBlockedTimes = olTmpBlockedTimes.GetSize();
				for(ilBT = 0; ilBT < ilNumBlockedTimes; ilBT++)
				{
					olBlockedTimes.Add(&olTmpBlockedTimes[ilBT]);
				}
			}
		}
	}

	olBlockedTimes.Sort(ByStartTime);
	ilNumBlockedTimes = olBlockedTimes.GetSize();
	CTime olStart = TIMENULL, olEnd = TIMENULL;

	for(ilBT = 0; ilBT < ilNumBlockedTimes; ilBT++)
	{
		BLKBLOCKEDTIMES *prlBt = &olBlockedTimes[ilBT];
		if(olStart == TIMENULL || olEnd == TIMENULL)
		{
			olStart = prlBt->Tifr;
			olEnd = prlBt->Tito;
		}
		else if(IsOverlapped(olStart, olEnd, prlBt->Tifr, prlBt->Tito))
		{
			if(prlBt->Tifr < olStart)
			{
				olStart = prlBt->Tifr;
			}
			if(prlBt->Tito > olEnd)
			{
				olEnd = prlBt->Tito;
			}
		}
	}

	if(IsWithIn(opFrom, opTo, olStart, olEnd))
	{
		blIsCompletelyBlocked = true;
	}

	return blIsCompletelyBlocked;
}
