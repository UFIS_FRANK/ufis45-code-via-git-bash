// PrmDiagramGroupPage.h : header file
//

#ifndef _PRMDIAGR_H_
#define _PRMDIAGR_H_

// Message for notify the PRMDiagramPropertySheet
#define WM_PRMDIAGRAM_GROUPPAGE_CHANGED	(WM_USER + 320)

/////////////////////////////////////////////////////////////////////////////
// PRMDiagramGroupPage dialog

class PRMDiagramGroupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(PRMDiagramGroupPage)

// Construction
public:
	PRMDiagramGroupPage();
	~PRMDiagramGroupPage();

// Dialog Data
	CString omGroupBy;

	//{{AFX_DATA(PRMDiagramGroupPage)
	enum { IDD = IDD_PRMDIAGRAM_GROUP_PAGE };
	int		m_GroupBy;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PRMDiagramGroupPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PRMDiagramGroupPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetGroupId(const char *pcpGroupKey);
	CString GetGroupKey(int ipGroupId);
};

#endif // _PRMDIAGR_H_
