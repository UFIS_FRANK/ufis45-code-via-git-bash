// NewAssignConflictGantt.h : header file
//

#ifndef _NEWASSIGNCONFLICTDLG_H_
#define _NEWASSIGNCONFLICTDLG_H_

//#include "stdafx.h"
//#include "CCSGlobl.h"
//#include "resource.h"
//#include "CCSPtrArray.h"
//#include "CCSCedaData.h"
//#include "CCSDragDropCtrl.h"
//#include "CedaJobData.h"
//#include "CedaFlightData.h"
//#include "CedaEmpData.h"
//#include "CedaShiftData.h"
//#include "gantt.h"
//#include "gbar.h"
//#include "CedaFlightData.h"
//#include "CedaDemandData.h"
//#include "conflict.h"
//#include "ccsddx.h"
//#include "cviewer.h"
//#include "NewAssignConflictWindow.h"
#include <NewAssignConflictViewer.H>
//#include "NewAssignConflictDlg.h"
//#include "CedaJodData.h"
//#include "BasicData.h"
//#include "AllocData.h"
//#include "DataSet.h"
//#include "CedaRudData.h"
//#include "CedaRueData.h"

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

/////////////////////////////////////////////////////////////////////////////
// NewAssignConflictGantt window

class NewAssignConflictGantt: public CListBox
{
// Operations
public:
	NewAssignConflictGantt::NewAssignConflictGantt(NewAssignConflictViewer *popViewer = NULL, int ipGroup = 0,
        int ipVerticalScaleWidth = 0, int ipVerticalScaleIndent = 15,
        CFont *popVerticalScaleFont = NULL, CFont *popGanttChartFont = NULL,
        int ipGutterHeight = 4, int ipOverlapHeight = 4,
        COLORREF lpVerticalScaleTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightVerticalScaleTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightVerticalScaleBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW),
        COLORREF lpGanttChartTextColor = ::GetSysColor(COLOR_BTNTEXT),
        COLORREF lpGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNFACE),
        COLORREF lpHighlightGanttChartTextColor = ::GetSysColor(COLOR_BTNHIGHLIGHT),
        COLORREF lpHighlightGanttChartBackgroundColor = ::GetSysColor(COLOR_BTNSHADOW));
	~NewAssignConflictGantt();
    void SetViewer(NewAssignConflictViewer *popViewer, int ipGroup);
    void SetVerticalScaleWidth(int ipWidth);
	void SetVerticalScaleIndent(int ipIndent);
    void SetFonts(CFont *popVerticalScaleFont, CFont *popGanttChartFont);
    void SetGutters(int ipGutterHeight, int ipOverlapHeight);
    void SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);
    void SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
            COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor);

    int GetGanttChartHeight();
    int GetLineHeight(int ipMaxOverlapLevel);

    BOOL Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID = 0);
	void SetStatusBar(CStatic *popStatusBar);
	void SetTimeScale(CTimeScale *popTimeScale);
    void SetBorderPrecision(int ipBorderPrecision);

    // This group of function will automatically repaint the window
    void SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling = TRUE);
    void SetDisplayStart(CTime opDisplayStart);
    void SetCurrentTime(CTime opCurrentTime);
    void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);

    void RepaintVerticalScale(int ipLineno = -1, BOOL bpErase = TRUE);
    void RepaintGanttChart(int ipLineno = -1,
            CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL, BOOL bpErase = TRUE);
    void RepaintItemHeight(int ipLineno);


// Attributes
private:
    NewAssignConflictViewer *pomViewer;
    int imGroupno;              // index to a group in the viewer
    int imVerticalScaleWidth;
	int imVerticalScaleIndent;	// number of pixels used to indent text in VerticalScale
    CFont *pomVerticalScaleFont;
    CFont *pomGanttChartFont;
    int imBarHeight;            // calculated when SetGanttChartFont()
    int imGutterHeight;         // space between the GanttLine and the topmost-level bar
    int imLeadingHeight;        // space between the bar and the border
    int imOverlapHeight;        // space between the overlapped bar
    COLORREF lmVerticalScaleTextColor;
    COLORREF lmVerticalScaleBackgroundColor;
    COLORREF lmHighlightVerticalScaleTextColor;
    COLORREF lmHighlightVerticalScaleBackgroundColor;
    COLORREF lmGanttChartTextColor;
    COLORREF lmGanttChartBackgroundColor;
    COLORREF lmHighlightGanttChartTextColor;
    COLORREF lmHighlightGanttChartBackgroundColor;

    CTime omDisplayStart;
    CTime omDisplayEnd;
    int imWindowWidth;          // in pixels, for pixel/time ratio (updated by WM_SIZE)
    BOOL bmIsFixedScaling;
        // There are two mode for updating the display window when WM_SIZE was sent,
        // variabled-scaling and fixed-scaling. The mode of this operation will be
        // given in the method SetDisplayWindow().

    CTime omCurrentTime;                    // the red vertical time line
    CTime omMarkTimeStart, omMarkTimeEnd;   // two yellow vertical time lines

    CStatic *pomStatusBar;   // the status bar where the notification message goes
	CTimeScale *pomTimeScale;	// the time scale for top scale indicators display

    BOOL bmIsMouseInWindow;
    BOOL bmIsControlKeyDown;
    int imHighlightLine;        // the current line that painted with highlight color
    int imCurrentBar;           // the current bar that the user places mouse over
	int imBkBarno;				// the current demand bk bar that the user places mouse over

    UINT umResizeMode;          // HTNOWHERE, HTCAPTION (moving), HTLEFT, or HTRIGHT
    CPoint omPointResize;       // the point where moving/resizing begin
    CRect omRectResize;         // current focus rectangle for moving/resizing
    int imBorderPreLeft;        // define the sensitivity when user detect bar border
    int imBorderPreRight;       // define the sensitivity when user detect bar border
	ASSIGNCONFLICT_BARDATA rmActiveBar;          // Bar which is currently moved or sized
	BOOL bmActiveBarSet;

	int imContextItem;
	int imContextBarNo;
	ASSIGNCONFLICT_BARDATA rmContextBar;  // Save bar for Context Menu handling
	BOOL bmContextBarSet;


// Implementation
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	 int OnToolHitTest(CPoint point, TOOLINFO* pTI) const;
	void SetStatusText(const char *pcpText);

private:
    void DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem);
    void DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip);
    void DrawTimeLines(CDC *pDC, int top, int bottom);
	void DrawFocusRect(CDC *pDC, const CRect &rect);

protected:
    // Generated message map functions
    //{{AFX_MSG(NewAssignConflictGantt)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMenuFlightConfirm();
    afx_msg void OnMenuFlightFinish();
    afx_msg void OnMenuFlightWorkOn();
    afx_msg void OnMenuFlightDelete();
    afx_msg void OnMenuFlightAcceptConflict(UINT);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()


private:
    void UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown);
	void UpdateBarStatus(int ipLineno, int ipBarno, int ipBkBarno);
    BOOL BeginMovingOrResizing(CPoint point);
    BOOL OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown);
    int GetItemFromPoint(CPoint point)const;
    int GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1);
        // Precision is measured in pixel.
        // The purpose of the precision is for detecting more pixel when user want to resize.
        // The acceptable period of time is between [point.x - ipPreLeft, point.x + ipPreRight]
    UINT HitTest(int ipLineno, int ipBarno, CPoint point);
        // HitTest will use the precision defined in "imBorderPrecision"

private:
	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;
	CMapPtrToPtr omAcceptConflictsMap;

//Pichate
protected:
	void DrawDotHorizontalLine(CDC *pDC);
	void DrawVerticalLine(CDC *pDC);
	void DrawBkBars(CDC *pDC);

	int GetBkBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1)const;
	CString GetEmpBarTextFromPoint(int ipLineno, CPoint point, int ipPreLeft = 0, int ipPreRight = 1)const;;
	void DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem);

	void DragFlightBegin();
	void DragDemandBarBegin(ASSIGNCONFLICT_BARDATA *prpBar);
	void DragDutyBarBegin(ASSIGNCONFLICT_BARDATA *prpBar);
	bool DrawPoolJobBars(CDC *pDC, int ipLine);
};

/////////////////////////////////////////////////////////////////////////////

#endif
