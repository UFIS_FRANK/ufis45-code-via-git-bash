#ifndef _CCSGLOBL_H_
#define _CCSGLOBL_H_

#include <3dstatic.h>
#include <ccsobj.h>
#include <ccslog.h>
#include <CcsDefines.h>

#include <CCSPtrArray.h>

#define CCSReturnCode bool
#define RCSuccess true
#define RCFailure false


//enum enumRecordState 
//{ 
//DATA_NEW, DATA_CHANGED, DATA_UNCHANGED, DATA_DELETED
//};

enum DLG_ACTION 
{ 
    DLG_INSERT,DLG_UPDATE
   
};  

enum OLD_VALUE_TYPES
{
	OLD_GATE,
	OLD_REGN,
	OLD_ACT3
};

class CedaBasicData;
class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
extern CedaBasicData ogBCD;
extern CCSBcHandle ogCCSBcHandle;
extern CCSCedaCom ogCCSCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;

extern char pcgAppName[];
extern char pcgTableExt[];
extern char pcgHomeAirport[];
extern char cgSelection[];

extern CWnd* pogMainWnd;
extern CWnd* pogPreviewPrintWnd;

// Special time value definition
#ifndef TIMENULL
#define TIMENULL    CTime((time_t)-1)
#endif

#define NUMCONFLICTS 56 //Singapore , earlier it was 50 //earlier it was 55, changed to 56 by MAX
#define FIRSTCONFLICTCOLOR 31
#define MAXCOLORS ((FIRSTCONFLICTCOLOR + 1) + (NUMCONFLICTS * 2))

// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  ::GetSysColor(COLOR_BTNFACE)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)


extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern enum enumRecordState egRecordState;


// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "tscall.cpp" by Pichate May 08,96 18:15
#define IDC_TIMESCALE       0x4001

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "vscale.cpp" by Pichate May 08,96 18:15
#define IDC_VERTICALSCALE   0x4002

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "gbar.cpp" by Pichate May 08,96 18:15
#define IDC_GANTTBAR        0x4003

//#define IDD_DIAGRAM         0x4004
#define IDD_CHART           0x4005
#define IDC_CHARTBUTTON     0x4006
#define IDD_GANTT           0x4007

#define IDC_PREV            0x4008
#define IDC_NEXT            0x4009
#define IDC_ARRIVAL         0x400a
#define IDC_DEPARTURE       0x400b

//***** moved from "ccsglobl.h" by Damkerng 04/30/96 18:30
//***** moved from "ganttcht.cpp" by Pichate May 08,96 18:15
#define IDD_GANTTCHART      0x4101


/////////////////////////////////////////////////////////////////////////////
// Messages

// Message sent from PrePlanTable to the parent window when closed
// Id 24-Sep-96
// Fix some bug here, since CCI diagram and Gate diagram shares the same message number.
// This surely will crash the machine or destroy our memory blocks if the user terminate
// the diagram with Alt-F4.
#define WM_BCADD					(WM_USER + 16)
#define WM_STAFFDIAGRAM_EXIT        (WM_USER + 17)	/* diagrams */
#define WM_GATEDIAGRAM_EXIT         (WM_USER + 18) 
#define WM_CCIDIAGRAM_EXIT          (WM_USER + 19)
#define WM_STAFFTABLE_EXIT          (WM_USER + 20)	/* tables */
#define WM_FLIGHTTABLE_EXIT         (WM_USER + 21) 
#define WM_CCITABLE_EXIT			(WM_USER + 22)
#define WM_GATETABLE_EXIT			(WM_USER + 23)
#define WM_REQUESTTABLE_EXIT		(WM_USER + 24)
#define WM_PEAKTABLE_EXIT		    (WM_USER + 25)
#define WM_COVERAGEEXIT		        (WM_USER + 26)	/* middle group buttons */
#define WM_PREPLANTABLE_EXIT        (WM_USER + 27)
#define WM_ATTENTIONTABLE_EXIT		(WM_USER + 28)
#define WM_CONFLICTTABLE_EXIT		(WM_USER + 29)
#define WM_INFOTABLE_EXIT			(WM_USER + 30)
#define WM_SETUPTABLE_EXIT			(WM_USER + 31)
#define WM_REGNDIAGRAM_EXIT			(WM_USER + 32)
#define WM_FLINDDEMANDTABLE_EXIT	(WM_USER + 33)
#define WM_DEMANDTABLE_EXIT			(WM_USER + 34)
#define WM_CICDEMANDTABLE_EXIT		(WM_USER + 35)
#define WM_EQUIPMENTTABLE_EXIT		(WM_USER + 36)
#define WM_FLIGHTJOBSTABLE_EXIT		(WM_USER + 37)
#define WM_PRMTABLE_EXIT			(WM_USER + 38)


#define WM_PREPLANTEAMTABLE_EXIT	(WM_USER + 50)	/* other details windows */
#define WM_PREPLANSEARCHTABLE_EXIT	(WM_USER + 50)	/* other details windows */

#define	WM_UPDATE_ALL_PAGES			(WM_USER + 51)  /* update all pages	*/

// Messages sent from TimeScale to surrounding window (eg. GateDiagram)
#define WM_TIMEUPDATE               (WM_USER + 101)
#define WM_DRAGENTER				(WM_USER + 102) 
#define WM_DRAGOVER					(WM_USER + 103)
#define WM_DROP						(WM_USER + 104)

// Messages for handling keystrokes in various diagrams
#define WM_USERKEYDOWN				(WM_USER + 151)
#define WM_USERKEYUP				(WM_USER + 152)

// Message and constants which are used to handshake viewer and the attached diagram
#define WM_POSITIONCHILD            (WM_USER + 201)
#define WM_UPDATEDIAGRAM            (WM_USER + 202)
#define UD_UPDATEGROUP                  (WM_USER + 203)
#define UD_DELETELINE                   (WM_USER + 204)
#define UD_DELETEGROUP                  (WM_USER + 205)
#define UD_INSERTLINE                   (WM_USER + 206)
#define UD_INSERTGROUP                  (WM_USER + 207)
#define UD_UPDATELINEHEIGHT             (WM_USER + 208)
#define UD_UPDATELINE					(WM_USER + 209)
#define UD_PREPLANMODE					(WM_USER + 210)

#define	WM_SELECTDIAGRAM			(WM_USER + 220)
#define	UD_SELECTBAR					(WM_USER + 221)	
#define	UD_SELECTLINE					(WM_USER + 222)	

// Messages sent from CTable to surrounding windows (eg. PrePlanDiagram)
#define WM_TABLE_LBUTTONDBLCLK          (WM_USER + 310)     // wParam is the selected item
#define WM_TABLE_LBUTTONDOWN			(WM_USER + 311)     // wParam is the selected item
#define WM_TABLE_RBUTTONDOWN			(WM_USER + 312)     // wParam is the selected item
#define WM_TABLE_RBUTTONDBLCLK          (WM_USER + 313)     // wParam is the selected item
#define WM_TABLE_DRAGBEGIN              (WM_USER + 314)     // wParam is the selected item

#define WM_CCSBUTTON_RBUTTONDOWN		(WM_USER + 315) 
#define WM_TABLE_SETTABLEWINDOW         (WM_USER + 320) //Singapore
#define WM_TABLE_LIST_HORZSCROLL        (WM_USER + 321) //Singapore
#define WM_TABLE_LIST_VERTSCROLL        (WM_USER + 322) //Singapore
#define WM_TABLE_ACTIVE_SCROLLWND       (WM_USER + 323) //Singapore
#define WM_TABLE_UPDATE_DATACOUNT       (WM_USER + 324) //Singapore
#define WM_BEGIN_PRINTING               (WM_USER + 325) //Singapore
#define WM_END_PRINTING                 (WM_USER + 326) //Singapore
#define WM_PRINT_PREVIEW                (WM_USER + 327) //Singapore
#define WM_PREVIEW_PRINT                (WM_USER + 328) //Singapore


// Messages sent from CTree to surrounding windows 
#define WM_TREE_LBUTTONDOWN				(WM_USER + 410)     
#define WM_TREE_LBUTTONDBLCLK           (WM_USER + 411)     
#define WM_TREE_RBUTTONDOWN				(WM_USER + 412)     
#define WM_TREE_RBUTTONDBLCLK			(WM_USER + 413)     // wParam is the selected item
#define WM_TREE_DRAGBEGIN				(WM_USER + 414)     // wParam is the selected item
#define WM_TREE_NEWITEM					(WM_USER + 417)     // wParam is the selected item
#define WM_TREE_SELCHANGE				(WM_USER + 418)     // wParam is the selected item

// Messages sent from CCSEdit to surrounding windows 
#define WM_EDIT_KILLFOCUS				(WM_USER + 510)     

// Messages sent from CWinThread to CMainThread
#define	WM_THREAD_LOST_BC				(WM_USER + 520)

/////////////////////////////////////////////////////////////////////////////
// Forward Declarations

//class CedaAlocData;
//class CedaAlocData;
//class CedaAlocData;
class AllocData;

class CedaFlightData;
class CedaDemandData;
class CedaJobData;
class CedaJodData;

class CedaEmpData;
class CedaShiftData;

class CedaJobData;
class CedaJobData;

class CedaJobData;
class CCSDdx;
class CCSBasicData;
class CCSLog;

/////////////////////////////////////////////////////////////////////////////
// Global Data Buffers

extern AllocData ogAllocData;
//extern CedaAlocData ogGateAreas;
//extern CedaAlocData ogPools;
//extern CedaAlocData ogCciAreas;

extern CedaFlightData ogFlightData;
extern CedaDemandData ogDemandData;
//extern CedaJobData ogFlightDemandJobs;
// MCU 07.05 we need a global Jod object also
extern CedaJodData ogJodData;

extern CedaEmpData ogEmpData;
// MCU 07.07 Shift Data now outside of Preplan Table
extern CedaShiftData ogShiftData;

extern CedaJobData ogJobData;
extern CCSDdx ogCCSDdx;
extern CCSBasicData ogBasicData;
extern CCSLog ogLog;

extern BOOL bgModal;
extern bool bgOnline;
extern BOOL bgIsPreplanMode;
extern BOOL bgPrintPreviewMode;
extern CTime ogPrePlanTime;
extern SYSTEMTIME ogSysTime;
extern bool bgIsPrm;
extern bool bgShowPrmClos;
extern bool bgPrmUseRema; // lli: control flag of using REMARKS which is requested by SATS.
extern bool bgPrmShowFlightNumber; // lli: control flag of showing Flight Number in Staff Chart status bar which is requested by SATS.
extern bool bgPrmShowAssignment; // lli: control flag of showing assignments in PRM List which is requested by SATS.
extern bool bgPrmShowStaStd; // lli: control flag of showing Sta, Std in PRM List which is requested by SATS.
extern bool bgPrmUseStat; // lli: control flag of using Status which is requested by SATS.

// CCS_TRY and CCS_CATCH_ALL ///////////////////////////////////////////////
#ifndef _DEBUG
#define  CCS_TRY try{
#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, "An internal error happened in the modul: %s\n in the line %d.",\
							                 __FILE__, __LINE__);\
						    strcat(pclText, "\nContinuing the operation can cause unwanted effects.\nResume??");\
						    sprintf(pclExcText, "File: %s  ==> Source-Line: %d", __FILE__, __LINE__);\
						    of_catch << pclExcText << endl;\
							if(::MessageBox(NULL, pclText, "Error", (MB_YESNO)) == IDNO)\
							{\
						       ExitProcess(0);\
							}\
						}
#else	// _DEBUG
#define  CCS_TRY
#define  CCS_CATCH_ALL
#endif	// #ifndef _DEBUG

// End CCS_TRY and CCS_CATCH_ALL //////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Font variable

extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogCourier_Bold_10;
extern CFont ogScalingFonts[4];
extern int igFontIndex1;
extern int igFontIndex2;
extern CString ogUsername;

enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, 
     MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

struct FONT_INDEXES
{
	int VerticalScale;
	int Chart;
	FONT_INDEXES(void)
	{VerticalScale=0;Chart=0;}
};

extern FONT_INDEXES ogStaffIndexes;
extern FONT_INDEXES ogGateIndexes;
extern FONT_INDEXES ogCCiIndexes;
extern FONT_INDEXES ogRegnIndexes;
extern FONT_INDEXES ogPstIndexes;
extern FONT_INDEXES ogEquipmentIndexes;

extern int igPreplanSearchCount; //Singapore
extern int igEmployeeSearchCount; //Singapore
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// Drag Information Type

enum {
	DIT_SHIFT,		// source: PrePlanTable
	DIT_DUTYSHIFT,	// source: StaffTable ***** obsoleted, not used, moved to DIT_DUTYBAR *****
	DIT_DUTYBAR,	// source: StaffDiagram (background), FlightMgrDetailWindow, StaffTable
	DIT_JOBBAR,		// source: StaffDiagram (job bar), StaffDetailWindow
	DIT_FLIGHTBAR,	// source: GateDiagram (flight bar), FlightPlan,
					//			GateTable, GateDetailWindow
	DIT_FLIGHT,		// source: FlightDetailWindow (background)
	DIT_DEMANDBAR,	// source: FlightDetailWindow (demand bar)
	DIT_GATE,		// source: GateDiagram (vertical scale)
	DIT_REGN,		// source: RegnDiagram (vertical scale)
	DIT_PST,		// source: PstDiagram (vertical scale)
	DIT_CCIDESK,	// source: CciDiagram (vertical scale), CciDetailWindow, CciTable

	DIT_FMDETAIL,	// source: FlightMgrDetailWindow

	DIT_FID,		// source: FlIndDemandTable
	DIT_CICDEMANDBAR,	// source : CicDemandTable
	DIT_REGNDEMANDBAR, // source regn gantt
	DIT_EQUIPMENT,	// source equipment gantt
	DIT_EQUJOBBAR	// source equipment gantt
};

// DIT 0 - 49
CString GetString(UINT ipStringId);

//4.5.2.65
BOOL IsPrivateProfileOn(CString key,CString defaultValue);

extern const char*	pcgReportName; 
extern char			pcgReportingPath[256];
extern char			pcgMacroPrintCommon[256];
extern char			pcgMacroPrintWork[256];
extern char			pcgMacroPrintBreak[256];

#endif // CCSGLOBL_H
