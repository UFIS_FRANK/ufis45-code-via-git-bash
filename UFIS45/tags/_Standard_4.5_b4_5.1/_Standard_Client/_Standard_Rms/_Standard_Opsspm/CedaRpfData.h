//
// CedaRpfData.h - demand<->function list
//
#ifndef _CEDARPFDATA_H_
#define _CEDARPFDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct RpfDataStruct
{
	long	Urno;		// Unique Record Number
	long	Urud;		// Rule demand URNO (RUDTAB.URNO)
	char	Fcco[10];	// Function Code
	long	Upfc;		// URNO of the function in PFCTAB
};

typedef struct RpfDataStruct RPFDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRpfData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <RPFDATA> omData;
	CMapPtrToPtr omUrudMap;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaRpfData();
	~CedaRpfData();

	bool ReadRpfData(CDWordArray &ropTplUrnos );
	bool ReadRpfDataByUrno(CString &myRpfUrnos);
	RPFDATA *GetRpfByUrno(long lpUrno);
	RPFDATA *GetFunctionByUrud(long lpUrud);
	void ProcessRpfBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	RPFDATA *AddRpfInternal(RPFDATA &rrpRpf);
	void DeleteRpfInternal(long lpUrno);
	void ClearAll();
};


extern CedaRpfData ogRpfData;
#endif _CEDARPFDATA_H_
