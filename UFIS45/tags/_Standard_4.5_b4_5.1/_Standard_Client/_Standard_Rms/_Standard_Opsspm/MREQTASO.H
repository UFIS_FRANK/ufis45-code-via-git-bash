// RequestTableSortPage.h : header file
//
#ifndef _RequestTable_H_
#define _RequestTable_H_
/////////////////////////////////////////////////////////////////////////////
// RequestTableSortPage dialog

class RequestTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(RequestTableSortPage)

// Construction
public:
	RequestTableSortPage();
	~RequestTableSortPage();

// Dialog Data
	CStringArray omSortOrders;

	//{{AFX_DATA(RequestTableSortPage)
	enum { IDD = IDD_REQUESTTABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(RequestTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(RequestTableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _RequestTable_H_
