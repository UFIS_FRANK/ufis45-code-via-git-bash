// CCIDiagramFilterPage.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <FilterPage.h>
#include <CciDiagramFilterPage.h>
#include <Ccsglobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FilterPage property page

CCIDiagramFilterPage::CCIDiagramFilterPage()
{
	//{{AFX_DATA_INIT(CCIDiagramFilterPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmSorted = true; 
}

CCIDiagramFilterPage::~CCIDiagramFilterPage()
{
}

void CCIDiagramFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCIDiagramFilterPage)
	DDX_Control(pDX, IDC_LIST2, m_List2);
	DDX_Control(pDX, IDC_LIST1, m_List1);
	DDX_Control(pDX, IDC_FILTERADD, m_AddButton);
	DDX_Control(pDX, IDC_FILTERREMOVE, m_RemoveButton);
	//}}AFX_DATA_MAP

	// Extended data exchange -- for member variables with Control type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		int i;

		// Do not update window while processing new data
		m_List1.SetRedraw(FALSE);
		m_List2.SetRedraw(FALSE);

		// Initialize all possible items into the left list box
		/*	hag
		m_List1.ResetContent();
		for (i = 0; i < omPossibleItems.GetSize(); i++)
			m_List1.AddString(omPossibleItems[i]);

		// Initialize the selected items into the right list box,
		// also remove the corresponding items from the left list box.
		m_List2.ResetContent();
		for (i = 0; i < omSelectedItems.GetSize(); i++)
		{
			m_List2.AddString(omSelectedItems[i]);
			m_List1.DeleteString(m_List1.FindStringExact(-1, omSelectedItems[i]));
		}
		*/
		m_List1.ResetContent();
		m_List2.ResetContent();
		bool blAllLeft = false, blAllRight = false;
		if ( bmSelectAllEnabled )
		{
			blAllRight = ( ( omSelectedItems.GetSize()>0 ) && 
						   ( omSelectedItems[0]==omAllString ) );
			blAllLeft = !blAllRight;
		}
		if ( blAllRight )
			m_List2.AddString(omAllString);
		else
		{
			if ( blAllLeft )
			{
//				if (bmSorted)
					m_List1.AddString(omAllString);
//				else
//					m_List1.InsertString(m_List1.GetCount(),omAllString);
			}
			for (i = 0; i < omPossibleItems.GetSize(); i++)
			{
//				if (bmSorted)
					m_List1.AddString(omPossibleItems[i]);
//				else
//					m_List1.InsertString(m_List1.GetCount(),omPossibleItems[i]);
			}

			m_List2.ResetContent();
			for (i = 0; i < omSelectedItems.GetSize(); i++)
			{
				if (bmSorted)
					m_List2.AddString(omSelectedItems[i]);
				else
					m_List2.InsertString(m_List2.GetCount(),omSelectedItems[i]);
				m_List1.DeleteString(m_List1.FindStringExact(-1, omSelectedItems[i]));
			}
		}
		// Update the window according to the new data
		m_List1.SetRedraw(TRUE);
		m_List2.SetRedraw(TRUE);
	}

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Copy data from the list box back to the array of string
		omSelectedItems.SetSize(m_List2.GetCount());
		for (int i = 0; i < omSelectedItems.GetSize(); i++)
			m_List2.GetText(i, omSelectedItems[i]);
	}


	// Extended data exchange -- for member variables with Control type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_RemoveButton.EnableWindow(m_bEnabled);
		m_AddButton.EnableWindow(m_bEnabled);
		m_List1.EnableWindow(m_bEnabled);
		m_List2.EnableWindow(m_bEnabled);
	}
}


BEGIN_MESSAGE_MAP(CCIDiagramFilterPage, FilterPage)
	//{{AFX_MSG_MAP(CCIDiagramFilterPage)
	ON_BN_CLICKED(IDC_FILTERADD, OnFilteradd)
	ON_BN_CLICKED(IDC_FILTERREMOVE, OnFilterremove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIDiagramFilterPage message handlers

BOOL CCIDiagramFilterPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
		CancelToClose();
	return FilterPage::OnCommand(wParam, lParam);
}

void CCIDiagramFilterPage::OnFilteradd() 
{
	for (int i = m_List1.GetCount()-1; i >= 0; i--)
	{
		if (!m_List1.GetSel(i))	// unselected item?
			continue;
		CString s;
		m_List1.GetText(i, s);	// load string to "s"
		if ( s == omAllString )
		{
			m_List2.ResetContent(); 
			m_List1.ResetContent(); 
			m_List2.AddString(s);	// move string from left to right box
			break;	//  fertig, mehr als alle geht nicht
		}
		if (bmSorted)
			m_List2.AddString(s);	// move string from left to right box
		else
			m_List2.InsertString(m_List2.GetCount(),s);
		m_List1.DeleteString(i);
	}
	CWnd *pParent = GetParent();
	if (pParent)
		pParent->SendMessage(WM_UPDATE_ALL_PAGES,0,(long)this);
}

void CCIDiagramFilterPage::OnFilterremove() 
{
	// Move selected items from right list box to left list box
	for (int i = m_List2.GetCount()-1; i >= 0; i--)
	{
		if (!m_List2.GetSel(i))	// unselected item?
			continue;
		CString s;
		m_List2.GetText(i, s);	// load string to "s"
		if ( s == omAllString )
		{
			m_List2.ResetContent(); 
			m_List1.ResetContent(); 
			m_List1.AddString(s);	// move string from left to right box
			for (int j = 0; j < omPossibleItems.GetSize(); j++)
				m_List1.AddString(omPossibleItems[j]);
			break;	//  fertig, mehr als alle geht nicht
		}
		if (bmSorted)
			m_List1.AddString(s);	// move string from left to right box
		else
			m_List1.InsertString(m_List1.GetCount(),s);
		m_List2.DeleteString(i);
	}
	CWnd *pParent = GetParent();
	if (pParent)
		pParent->SendMessage(WM_UPDATE_ALL_PAGES,0,(long)this);
}

void CCIDiagramFilterPage::SetSorted(BOOL bpSorted)
{
	bmSorted = bpSorted == TRUE;
}

BOOL CCIDiagramFilterPage::OnInitDialog() 
{
	BOOL blResult = FilterPage::OnInitDialog();

	SetWindowText(GetString(IDS_STRING32945));

	if (!bmSorted)
	{
		CWnd *pListBox = GetDlgItem(IDC_LIST2);
		if (pListBox)
		{
			LONG dwStyle = ::GetWindowLong(pListBox->m_hWnd,GWL_STYLE);
			if (dwStyle & LBS_SORT)
			{
				dwStyle &= ~LBS_SORT;
				::SetWindowLong(pListBox->m_hWnd,GWL_STYLE,dwStyle);
				pListBox->Invalidate();
			}
		}
	}

	return blResult;
}

