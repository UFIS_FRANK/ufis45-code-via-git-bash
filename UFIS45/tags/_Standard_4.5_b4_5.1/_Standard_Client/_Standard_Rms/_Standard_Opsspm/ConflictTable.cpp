// CfTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <resource.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <ccsddx.h>
#include <ConflictConfigTable.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <Conflict.h>
#include <ConflictViewer.h>
#include <ConflictTable.h>
#include <FlightDetailWindow.h>
#include <StaffDetailWindow.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <ConflictTableSortPage.h>
#include <BasePropertySheet.h>
#include <ConflictTablePropSheet.h>
#include <BasicData.h>
#include <CedaCfgData.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <FlightPlan.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <RegnDiagram.h>
#include <StaffTable.h>
#include <GateTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
static void ConflictTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

enum egConflictTableType;

/////////////////////////////////////////////////////////////////////////////
// ConflictTable dialog
ConflictTable::ConflictTable(int ipTableType, ConflictTableViewer* popViewer,CWnd* pParent /*=NULL*/)
	: CDialog(ConflictTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(ConflictTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_nDialogBarHeight = 0;

	pomTable = new CTable;
    pomTable->tempFlag = 2;
	bmTableType = ipTableType;
	bmIsViewOpen = FALSE;

	if (popViewer)
	{
		pomViewer = popViewer;
		bmIsViewerCreated = FALSE;
	}
	else
	{
		pomViewer = new ConflictTableViewer;
		bmIsViewerCreated = TRUE;
	}

    CDialog::Create(ConflictTable::IDD);
	if (bmTableType == CFLTABLE_CONFLICT)
	{
		//CDialog::SetWindowText("Konfliktliste");
		CDialog::SetWindowText(GetString(IDS_STRING61303));
	}
	else
	{
		//CDialog::SetWindowText("Neue Konflikte");
		CDialog::SetWindowText(GetString(IDS_STRING61304));
	}

	CRect olRect;
	if (bmTableType == CFLTABLE_ATTENTION)
		ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.ATTB,ogCfgData.rmUserSetup.MONS);
	else
		ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.CFTB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	BOOL blMinimized = FALSE;
	if (bmTableType == CFLTABLE_ATTENTION)
	{
		ogBasicData.GetDialogFromReg(olRect, COpssPmApp::ATTENTION_TABLE_WINDOWPOS_REG_KEY,blMinimized);
		
		//Multiple monitor setup
		ogBasicData.GetWindowPositionCorrect(olRect);	
	}
	else if(bmTableType == CFLTABLE_CONFLICT)
	{
		ogBasicData.GetDialogFromReg(olRect, COpssPmApp::CONFLICTS_TABLE_WINDOWPOS_REG_KEY,blMinimized);

		//Multiple monitor setup
		ogBasicData.GetWindowPositionCorrect(olRect);
	}
    MoveWindow(&olRect);

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	

	pomViewer->Attach(pomTable);
	pomViewer->SetTableType(bmTableType);
	UpdateView();
	
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

ConflictTable::ConflictTable(int ipTableType, CWnd* pParent /*=NULL*/)
	: CDialog(ConflictTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(ConflictTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_nDialogBarHeight = 0;

	pomTable = new CTable;
    pomTable->tempFlag = 2;
	bmTableType = ipTableType;
	bmIsViewOpen = FALSE;

	pomViewer = new ConflictTableViewer();
	bmIsViewerCreated = TRUE;

    CDialog::Create(ConflictTable::IDD);
	if (bmTableType == CFLTABLE_CONFLICT)
	{
		//CDialog::SetWindowText("Konfliktliste");
		CDialog::SetWindowText(GetString(IDS_STRING61303));
	}
	else
	{
		//CDialog::SetWindowText("Neue Konflikte");
		CDialog::SetWindowText(GetString(IDS_STRING61304));
	}

	CRect olRect;
	if (bmTableType == CFLTABLE_ATTENTION)
		ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.ATTB,ogCfgData.rmUserSetup.MONS);
	else
		ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.CFTB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	BOOL blMinimized = FALSE;
	if(bmTableType == CFLTABLE_ATTENTION)
	{
		ogBasicData.GetDialogFromReg(olRect, COpssPmApp::ATTENTION_TABLE_WINDOWPOS_REG_KEY,blMinimized);

		//Multiple monitor setup
		ogBasicData.GetWindowPositionCorrect(olRect);
	}
	else if(bmTableType == CFLTABLE_CONFLICT)
	{
		ogBasicData.GetDialogFromReg(olRect, COpssPmApp::CONFLICTS_TABLE_WINDOWPOS_REG_KEY,blMinimized);

		//Multiple monitor setup
		ogBasicData.GetWindowPositionCorrect(olRect);
	}
    MoveWindow(&olRect);

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	
	pomViewer->Attach(pomTable);
	pomViewer->SetTableType(bmTableType);
	UpdateView();
	
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

ConflictTable::~ConflictTable()
{
	TRACE("ConflictTable::~ConflictTable()\n");
	if (bmIsViewerCreated)
		delete pomViewer;
	else
		pomViewer->Attach(NULL);

	delete pomTable;	
}

void ConflictTable::UpdateView()
{

	CString  olViewName = pomViewer->GetViewName();
	
	if (olViewName.IsEmpty() == TRUE)
	{
		if (bmTableType == CFLTABLE_ATTENTION)
			olViewName = ogCfgData.rmUserSetup.ATTV;
		else
			olViewName = ogCfgData.rmUserSetup.CONV;
	}

	AfxGetApp()->DoWaitCursor(1);
    pomViewer->ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);

	//Singapore
	OnTableUpdateDataCount();
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();
}

void ConflictTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	polCB->ResetContent();
	CStringArray olStrArr;
	pomViewer->GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = pomViewer->GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		if (bmTableType == CFLTABLE_ATTENTION)
			olViewName = ogCfgData.rmUserSetup.ATTV;
		else
			olViewName = ogCfgData.rmUserSetup.CONV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

}

void ConflictTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ConflictTable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ConflictTable, CDialog)
	//{{AFX_MSG_MAP(ConflictTable)
	ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_CBN_SELCHANGE(IDC_VIEWCOMBO, OnSelchangeViewcombo)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelectionChanged)
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ConflictTable message handlers

BOOL ConflictTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING32914));
	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	
	UpdateComboBox();
 
	CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect); //Commented - Singapore PRF 8712
	
	// Register DDX call back function
	TRACE("ConflictTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("CFTABLE"), CString("Redisplay all"), ConflictTableCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("CFTABLE"),CString("Global Date Change"), ConflictTableCf);


	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ConflictTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;

	if(bmTableType == CFLTABLE_ATTENTION)
	{
		ogBasicData.WriteDialogToReg(this,COpssPmApp::ATTENTION_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	}
	else if(bmTableType == CFLTABLE_CONFLICT)
	{
		ogBasicData.WriteDialogToReg(this,COpssPmApp::CONFLICTS_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	}

	// Unregister DDX call back function
	TRACE("ConflictTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void ConflictTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();

}

void ConflictTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
   {
	   pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
	   GetWindowRect(&omWindowRect); //PRF 8712
   }
}

void ConflictTable::UpdateDisplay()
{
    //pomTable->SetHeaderFields("Typ|Gew.|Objekt|Zuo.E.|Zeit|Beschreibung");
    pomTable->SetHeaderFields(GetString(IDS_STRING61561));
    //pomTable->SetFormatList("15|4|10|10|4|100");
	pomTable->SetFormatList("25|4|10|10|4|90");

	int ilCfltEntryCount = ogConflicts.omData.GetSize();
	for ( int ilLc = 0; ilLc < ilCfltEntryCount; ilLc++)
	{
		CONFLICTENTRY *prlCfltEntry = &ogConflicts.omData[ilLc];
		int ilConflictCount = prlCfltEntry->Data.GetSize();
		for ( int ilCfl = 0; ilCfl < ilConflictCount; ilCfl++)
		{
			if (bmTableType == CFLTABLE_ATTENTION)
			{
				if (prlCfltEntry->Data[ilCfl].AlreadyDisplayed)
				{
					continue;
				}
				else
				{
					prlCfltEntry->Data[ilCfl].AlreadyDisplayed = TRUE;
				}
			}
			pomTable->AddTextLine(Format(prlCfltEntry,ilCfl), &prlCfltEntry->Data[ilCfl]);
		}
	}
	pomTable->DisplayTable();
}

CString ConflictTable::Format(CONFLICTENTRY *prpCfltEntry,int ipCflIndex)
{
	CONFLICTDATA *prlConflict = &prpCfltEntry->Data[ipCflIndex];
	char pclWeight[24];
	sprintf(pclWeight,"%d",ogConflictConfData[prlConflict->Type].Weight);

	CString olAloc,olAlid;
	ogConflicts.GetAllocationFromConflictType(prpCfltEntry,ipCflIndex,olAloc,olAlid);

	CString olLineText = CString(ogConflictConfData[prlConflict->Type].Name) + "|";
	olLineText +=  CString(pclWeight) + "|";
	olLineText +=  prpCfltEntry->NameOfObject + "|";
	olLineText +=  olAlid + "|";
	olLineText +=  prlConflict->TimeOfConflict.Format("%H%M") + "|";
	CString olExtraText = (prlConflict->ExtraText.GetLength()) ? CString("  (") + prlConflict->ExtraText + CString(")") : CString("");
	olLineText +=  ogConflictConfData[prlConflict->Type].Dscr + olExtraText;

	return olLineText;
}

void ConflictTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomTable->GetCTableListBox()->SetFocus();

	ConflictTablePropertySheet dialog(this, pomViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
		UpdateView();
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void ConflictTable::OnSelchangeViewcombo() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
    polCB->GetLBText(polCB->GetCurSel(), clText);
    TRACE("ConflictTable::OnComboBox() [%s]", clText);
	pomViewer->SelectView(clText);
	UpdateView();	
}

void ConflictTable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    //pomTable->GetCTableListBox()->SetFocus();
}

BOOL ConflictTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	switch (bmTableType)
	{
	case CFLTABLE_ATTENTION:
		pogButtonList->SendMessage(WM_ATTENTIONTABLE_EXIT);
		break;
	case CFLTABLE_CONFLICT:
		pogButtonList->SendMessage(WM_CONFLICTTABLE_EXIT);
		break;
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}
 
LONG ConflictTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	// Id 2-Oct-96
	// Add new functionality: double click on a line will open the detail window.
	// Before this modification, we already did this but only in OnMenuWorkOn().
	//
	CONFLICTTABLE_LINEDATA *prlLine = (CONFLICTTABLE_LINEDATA *) pomTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		if(prlLine->TypeOfObject == CFLO_FLIGHT)
		{
			long llArrUrno = (prlLine->ReturnFlightType != ID_DEP_RETURNFLIGHT) ? prlLine->Urno : 0L;
			long llDepUrno = (prlLine->ReturnFlightType == ID_DEP_RETURNFLIGHT) ? prlLine->Urno : 0L;
			new FlightDetailWindow(this, llArrUrno, llDepUrno);
		}
		else if(prlLine->TypeOfObject == CFLO_JOB)
		{
			JOBDATA *prlJob = ogJobData.GetJobByUrno(prlLine->Urno);
			if(prlJob != NULL)
			{
				new StaffDetailWindow(this,prlJob->Jour);
			}
		}
		SetTimeband(prlLine);
	}
	return 0L;
}

LONG ConflictTable::OnTableSelectionChanged(UINT ipItem, LONG lpLParam)
{
	CONFLICTTABLE_LINEDATA *prlLine = (CONFLICTTABLE_LINEDATA *) pomTable->GetTextLineData(ipItem);
	if(prlLine != NULL)
	{
		SetTimeband(prlLine);
	}
	return 0L;
}

void ConflictTable::SetTimeband(CONFLICTTABLE_LINEDATA *prpLine)
{
	if(prpLine != NULL)
	{
		TIMEPACKET olTimePacket;
		olTimePacket.StartTime = TIMENULL;
		olTimePacket.EndTime = TIMENULL;

		JOBDATA *prlJob;
		FLIGHTDATA *prlFlight;
		DEMANDDATA *prlDemand;
		if((prlJob = ogJobData.GetJobByUrno(prpLine->Urno)) != NULL)
		{
			olTimePacket.StartTime = prlJob->Acfr;
			olTimePacket.EndTime = prlJob->Acto;
		}
		else if((prlFlight = ogFlightData.GetFlightByUrno(prpLine->Urno)) != NULL)
		{
		}
		else if((prlDemand = ogDemandData.GetDemandByUrno(prpLine->Urno)) != NULL)
		{
			olTimePacket.StartTime = prlDemand->Debe;
			olTimePacket.EndTime = prlDemand->Deen;
		}
		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
	}
}

////////////////////////////////////////////////////////////////////////
// ConflictTable -- implementation of DDX call back function

static void ConflictTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	ConflictTable *polTable = (ConflictTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

void ConflictTable::OnClose() 
{
	CDialog::OnClose();
}

void ConflictTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();	
}

void ConflictTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	pomViewer->omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	pomViewer->omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void ConflictTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

//Singapore
void ConflictTable::OnTableUpdateDataCount()
{	
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			char olDataCount[10];
			polWnd->EnableWindow();			
			polWnd->SetWindowText(itoa(pomViewer->Lines(),olDataCount,10));
		}
	}
}

//PRF 8712
void ConflictTable::OnMove(int x, int y)
{
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}