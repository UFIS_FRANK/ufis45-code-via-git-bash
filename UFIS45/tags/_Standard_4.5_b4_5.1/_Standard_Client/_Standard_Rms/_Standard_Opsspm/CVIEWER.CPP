#include <stdafx.h>
#include <ccsglobl.h>
#include <winreg.h>
#include <CedaCfgData.h>
#include <cviewer.h> 


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
/////////////////////////////////////////////////////////////////////////////
// CViewer

CViewer::CViewer()
{             
	if( !CheckKey( "Pepper" ) ) 
	{
		CreateKey( "Pepper" );
	}
}
         
CViewer::~CViewer()
{
}
    
BOOL CViewer::CheckKey(char* pStrKey)
{
    HKEY    hKey = HKEY_CURRENT_USER;
    HKEY    hSubKey;
    
    if( RegOpenKey( hKey, "Software", &hSubKey ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
    RegCloseKey( hKey );
    hKey = hSubKey;

	char*	pKey;
	char	cTempStr[100];

	strcpy( cTempStr, pStrKey);
	pKey = strtok( cTempStr, "\\");
	while( pKey != NULL )
	{
		if( RegOpenKey( hKey, pKey, &hSubKey ) != ERROR_SUCCESS )
		{
			RegCloseKey( hKey );
			return FALSE;
		}
        RegCloseKey( hKey );
		hKey = hSubKey;
		pKey = strtok( NULL, "\\");
	}

    return TRUE;
}


BOOL CViewer::CreateKey(char* pStrKey)
{
    HKEY    hKey = HKEY_CURRENT_USER;
    HKEY    hSubKey;
    
    if( RegOpenKey( hKey, "Software", &hSubKey ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
    RegCloseKey( hKey );
    hKey = hSubKey;

	char*	pKey;
	char	cTempStr[100];

	strcpy( cTempStr, pStrKey);
	pKey = strtok( cTempStr, "\\");
	while( pKey != NULL )
	{
		if( RegCreateKey( hKey, pKey, &hSubKey ) != ERROR_SUCCESS )
		{
			RegCloseKey( hKey );
			return FALSE;
		}
        RegCloseKey( hKey );
		hKey = hSubKey;
		pKey = strtok( NULL, "\\");
	}
    return TRUE;

}

BOOL CViewer::SetValue(char* pStrKey, char* pStrValue)
{
	BOOL bRet = TRUE;
    HKEY    hKey = HKEY_CURRENT_USER;
    HKEY    hSubKey;
    
    if( RegOpenKey( hKey, "Software", &hSubKey ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
    RegCloseKey( hKey );
    hKey = hSubKey;

	char*	pKey;
	char	cTempStr[100];

	strcpy( cTempStr, pStrKey);
	pKey = strtok( cTempStr, "\\");
	while( pKey != NULL )
	{
		if( RegOpenKey( hKey, 
						pKey, 
						&hSubKey ) != ERROR_SUCCESS )
		{
			RegCloseKey( hKey );
			return FALSE;
		}
		hKey = hSubKey;
		pKey = strtok( NULL, "\\");
	}

	BYTE *lpData = (BYTE *)malloc( strlen(pStrValue) + 1 );
	strcpy( (char*)lpData, pStrValue );
    if( RegSetValueEx( hKey, 
					   "Value", 
					   NULL, 
					   REG_SZ, 
					   lpData, 
					   strlen(pStrValue) ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
	free((BYTE *)lpData);
	RegCloseKey( hKey );

	return bRet;
}

BOOL CViewer::GetValue(char* pStrKey, char* pStrValue)
{
	BOOL blFound = FALSE;


	return TRUE;
}

void CViewer::SetViewerKey(CString strKey)
{
	CFGDATA rlCfg;
	VIEWDATA rlViewData;
	BOOL blFound = FALSE;
	int ilCount = ogCfgData.omViews.GetSize();

	//Search for the key
	for(int i = 0; i < ilCount; i++)
	{
		if(ogCfgData.omViews[i].Ckey == strKey)
			blFound = TRUE;
	}
	if(blFound == FALSE)
	{
		rlViewData.Ckey = strKey;
		ogCfgData.omViews.NewAt(ogCfgData.omViews.GetSize(), rlViewData);
	}
	m_BaseViewName = strKey;

}

BOOL CViewer::CreateView(CString strView, const CStringArray &possibleFilters,BOOL bpSave)
{
	BOOL blFound = FALSE;
	if( m_BaseViewName.IsEmpty() ) return FALSE;
	int ilCount = ogCfgData.omViews.GetSize();

	//Search for the key
	for(int i = 0; i < ilCount; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			DeleteView(strView,bpSave);
			VIEW_VIEWNAMES rlViewNames;
			VIEW_TYPEDATA rlViewTypeData;
			rlViewNames.ViewName = strView;
			rlViewTypeData.Type = CString("FILTER");
			
			for(int ili = 0; ili < possibleFilters.GetSize(); ili++ ) 
			{
				VIEW_TEXTDATA rlTextData;
				rlTextData.Page = possibleFilters.GetAt(ili);
				rlViewTypeData.omTextData.NewAt(rlViewTypeData.omTextData.GetSize(), rlTextData);
			}
			rlViewNames.omTypeData.NewAt(rlViewNames.omTypeData.GetSize(), rlViewTypeData);
			ogCfgData.omViews[i].omNameData.NewAt(ogCfgData.omViews[i].omNameData.GetSize(), rlViewNames);
			blFound = TRUE;
			i = ilCount; // we break at this point
		}
	}
	return blFound;

}

void CViewer::GetViews(CStringArray &strArray)
{
	int ilCount = ogCfgData.omViews.GetSize();

	strArray.RemoveAll();
	for(int i = 0; i < ilCount; i++)
	{
		CString olBaseViewName = ogCfgData.omViews[i].Ckey;
		if(olBaseViewName == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				CString olViewName = ogCfgData.omViews[i].omNameData[j].ViewName;
				strArray.Add(olViewName);
			}
		}
	}
}

BOOL CViewer::SelectView(CString strView)
{
	BOOL blFound = FALSE;
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == strView)
				{
					m_ViewName = strView;
					blFound = TRUE;
					j = ilC2; // let's break
					i = ilC1;
				}
			}
		}
	}
	return blFound;
}

CString CViewer::SelectView()
{
	if(m_ViewName != CString(""))
		return m_ViewName;
	for(int i = 0; i < ogCfgData.omViews.GetSize(); i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			if(ogCfgData.omViews[i].omNameData.GetSize() > 0)
			{
				return ogCfgData.omViews[i].omNameData[0].ViewName;
			}
		}
	}
	return m_ViewName;
}

BOOL CViewer::DeleteView(CString strView,BOOL bpSave)
{
	BOOL blFound = FALSE;
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j=0; j< ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == strView)
				{
					VIEW_VIEWNAMES *prlViewName;
					prlViewName = &ogCfgData.omViews[i].omNameData[j];
					int ilC3 = prlViewName->omTypeData.GetSize()-1;
					for(int k = ilC3; k >= 0; k--)
					{
						prlViewName->omTypeData[k].omValues.DeleteAll();
						int ilC4 = prlViewName->omTypeData[k].omTextData.GetSize()-1;
						for(int l = ilC4; l >= 0; l--)
						{
							prlViewName->omTypeData[k].omTextData[l].omValues.DeleteAll();
						}
						prlViewName->omTypeData[k].omTextData.DeleteAll();
					}
					ogCfgData.omViews[i].omNameData[j].omTypeData.DeleteAll(); //New
					ogCfgData.omViews[i].omNameData.DeleteAt(j);
					blFound = TRUE;
					if (bpSave == TRUE)
					{
						ogCfgData.DeleteViewFromDiagram(m_BaseViewName, strView);
					}
					j = ilC2;
					i = ilC1; // let's break
				}
			}
		}
	}
	return blFound;
}

BOOL CViewer::DeleteFilter(CString strView)
{
	return TRUE;
}

void CViewer::GetFilterPage(CStringArray &strArray)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;
	strArray.RemoveAll();

	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "FILTER")
		{
			int ilC2 = prlViewName->omTypeData[i].omTextData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				strArray.Add(prlViewName->omTypeData[i].omTextData[j].Page);
			}
		}
	}
}

void CViewer::SetFilter(CString strFilter, const CStringArray &opFilter)
{
	VIEW_TEXTDATA *prlTextData = GetActiveFilter(strFilter);
	if(prlTextData == NULL)
	{
		if((prlTextData = AddToActiveFilter(strFilter)) == NULL)
			return;
	}

	int ilCount = opFilter.GetSize();
	//first we must delete current set filters
	prlTextData->omValues.DeleteAll();
	if(ilCount == 0)
	{
		// set a line indicating that this filter has none of the possible values selected
		// we do this to distinguish between a filter with no values and an old filter
		// that has no values because the opFilter had not been yet defined - in which case 
		// the filter will be ignored when reloaded
		prlTextData->omValues.NewAt(prlTextData->omValues.GetSize(), "@!<>NOTHING<>!@");
	}
	else
	{
		for(int i = 0; i < ilCount; i++)
		{
			prlTextData->omValues.NewAt(prlTextData->omValues.GetSize(), opFilter.GetAt(i));
		}
	}
}

bool CViewer::GetFilter(CString strFilter, CStringArray &opFilter)
{
	bool blFilterFound = false;
	opFilter.RemoveAll();
	VIEW_TEXTDATA *prlTextData;
	prlTextData = GetActiveFilter(strFilter);
	if(prlTextData != NULL)
	{
		blFilterFound = true;
		for(int i = 0; i < prlTextData->omValues.GetSize(); i++)	
		{
			if(prlTextData->omValues[i] != "@!<>NOTHING<>!@")
			{
				// return true - the filter was found but it contained no values.
				opFilter.Add(prlTextData->omValues[i]);
			}
		}
	}
	return blFilterFound;
}

bool CViewer::SetFilterMap(const char *pcpFilterName, CMapStringToPtr &ropFilterMap, const char *pcpAllFilter)
{
	bool blUseAll = true;
	CStringArray olFilterValues;
	int ilNumFilterValues = 0;

	ropFilterMap.RemoveAll();

	if(GetFilter(pcpFilterName, olFilterValues) && 
		(ilNumFilterValues = olFilterValues.GetSize()) > 0 && 
		strcmp(olFilterValues[0],pcpAllFilter))
	{
		blUseAll = false;
		for(int i = 0; i < ilNumFilterValues; i++)
		{
			ropFilterMap[olFilterValues[i]] = NULL;
		}
	}

	return blUseAll;
}

bool CViewer::CheckForEmptyValue(CMapStringToPtr &ropFilterMap, const char *pcpEmptyValue)
{
	return ropFilterMap.RemoveKey(pcpEmptyValue) ? true : false;
}

void CViewer::SetSort(const CStringArray &opSort)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;

	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	rlTypeData.Type = CString("SORT");
	int ilCount = opSort.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		rlTypeData.omValues.NewAt(rlTypeData.omValues.GetSize(), opSort.GetAt(i));
	}
	prlViewName->omTypeData.NewAt(prlViewName->omTypeData.GetSize(), rlTypeData);
}

void CViewer::GetSort(CStringArray &opSort)
{

	opSort.RemoveAll();
	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "SORT")
		{
			int ilC2 = prlViewName->omTypeData[i].omValues.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				opSort.Add(prlViewName->omTypeData[i].omValues[j]);
			}
		}
	}

}

void CViewer::SetGroup(CString strGroup)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	rlTypeData.Type = CString("GROUP");
	rlTypeData.omValues.DeleteAll();
	rlTypeData.omValues.NewAt(rlTypeData.omValues.GetSize(), strGroup);
	prlViewName->omTypeData.NewAt(prlViewName->omTypeData.GetSize(), rlTypeData);


}

CString CViewer::GetGroup()
{
	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return CString("");
	
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "GROUP")
		{
			if(prlViewName->omTypeData[i].omValues.GetSize() > 0)
			{
				return prlViewName->omTypeData[i].omValues[0];
			}
			else
			{
				return CString("");
			}
		}
	}
	return CString("");
}

// added bch - can set user data - ie. user defined data for example
// an extra checkBox on one of the standard property sheet pages
// FOR EXAMPLE: to deselect the team view check box (TEAMVIEW=NO)
// opKey = "TEAMVIEW"
// opUserData = "NO"
void CViewer::SetUserData(CString opKey, CString opUserData)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	rlTypeData.Type = CString(opKey);
	rlTypeData.omValues.DeleteAll();
	rlTypeData.omValues.NewAt(rlTypeData.omValues.GetSize(), opUserData);
	prlViewName->omTypeData.NewAt(prlViewName->omTypeData.GetSize(), rlTypeData);


}

// added bch - can get user data - ie. user defined data for example
// an extra checkBox on one of the standard property sheet pages
// FOR EXAMPLE: if the team view check box is checked (TEAMVIEW=YES)
// opKey = "TEAMVIEW" will be returned
CString CViewer::GetUserData(CString opKey)
{
	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return CString("");
	
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == opKey)
		{
			if(prlViewName->omTypeData[i].omValues.GetSize() > 0)
			{
				return prlViewName->omTypeData[i].omValues[0];
			}
			else
			{
				return CString("");
			}
		}
	}
	return CString("");
}

VIEW_VIEWNAMES * CViewer::GetActiveView()
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == m_ViewName)
				{
					return &ogCfgData.omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////
// MWO: 09.10.1996 finds a filter
VIEW_TEXTDATA * CViewer::GetActiveFilter(CString opFilter)
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == m_ViewName)
				{
					int ilC3 = ogCfgData.omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(ogCfgData.omViews[i].omNameData[j].omTypeData[k].Type == "FILTER")
						{
							int ilC4 = ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == opFilter)
								{
									return &ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}

VIEW_TEXTDATA * CViewer::AddToActiveFilter(CString opFilter)
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == m_ViewName)
				{
					int ilC3 = ogCfgData.omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(ogCfgData.omViews[i].omNameData[j].omTypeData[k].Type == "FILTER")
						{
							VIEW_TEXTDATA *prlTextData = new VIEW_TEXTDATA;
							prlTextData->Page = opFilter;
							int ilIdx = ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData.Add(prlTextData);
							return &ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData[ilIdx];
						}
					}
				}
			}
		}
	}
	return NULL;
}


CString CViewer::GetBaseViewName()
{
	return m_BaseViewName;
}

CString CViewer::GetViewName()
{
	return m_ViewName;
}


void CViewer::SafeDataToDB(CString opViewName)
{
	CCSReturnCode olRc = RCSuccess;
	
	int ilC1 = ogCfgData.omViews.GetSize();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	for(int i = 0; olRc == RCSuccess &&  i < ilC1; i++)
	{
		VIEWDATA *prlCfg = &ogCfgData.omViews[i];
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			olRc = ogCfgData.UpdateViewForDiagram(m_BaseViewName, ogCfgData.omViews[i],opViewName);
			i = ilC1;
		}
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}
