// SearchPropertySheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SearchPropertySheet
#ifndef _SEARCHPROPERTY_SHEET_
#define _SEARCHPROPERTY_SHEET_

#include <FilterData.h>
#include <FlightSearchPage.h>
#include <EmpSearchPage.h>


class SearchPropertySheet : public CPropertySheet
{
	DECLARE_DYNAMIC(SearchPropertySheet)

// Construction
public:
	SearchPropertySheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0, int *pipSelectedPage = NULL);
	SearchPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0, int *pipSelectedPage = NULL);

	FlightSearchPage m_FlightPage;
	EmpSearchPage    m_EmpPage;
	int *pimSelectedPage;
// Attributes
public:
	BOOL	FlightDetailDisplay() const;
	BOOL	EmployeeDetailDisplay() const;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SearchPropertySheet)
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~SearchPropertySheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(SearchPropertySheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	afx_msg void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif //_SEARCHPROPERTY_SHEET_ 