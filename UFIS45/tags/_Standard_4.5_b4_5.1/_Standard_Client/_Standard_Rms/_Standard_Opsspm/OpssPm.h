// OpssPm.h : main header file for the OPSS-PM application
// 
#ifndef _OPSSPMMAIN_
#define _OPSSPMMAIN_

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols

extern BOOL bgIsInitialized;


/////////////////////////////////////////////////////////////////////////////
// COpssPmApp:
// See OpssPm.cpp for the implementation of this class
//

class CMutex;
class COpssPmApp : public CWinApp
{
public:
	COpssPmApp();
	~COpssPmApp();
	void SetLandscape(void);
	void SetPortrait(void);
	void CleanUp(void);
	void SetCurrDate(void);
	void ReadCfgFile(const char *pcpThisAppl);
	static void SetLoadMessage(const char *pcpMessage, int ipProgress = 0);
	static CString GetNumRecsReadString(int ilNumRecs, CString opMsg, CString opTable);
	static void InitDefaultViews();
	bool HandleIsfBroadcasts(void);
	static void InitialLoad(void);

	//PRF 8712	
	static const CString OPSSPM_DIALOG_WINDOWPOS_REG_KEY;

	static const CString EMPLOYEE_DIAGRAM_WINDOWPOS_REG_KEY;
	static const CString GATES_DIAGRAM_WINDOWPOS_REG_KEY;
	static const CString CHECKIN_DIAGRAM_WINDOWPOS_REG_KEY;
	static const CString EQUIPMENT_DIAGRAM_WINDOWPOS_REG_KEY;	
	static const CString REGISTRATION_DIAGRAM_WINDOWPOS_REG_KEY;	
	static const CString POSITION_DIAGRAM_WINDOWPOS_REG_KEY;

	static const CString EMPLOYEE_TABLE_WINDOWPOS_REG_KEY;
	static const CString GATES_TABLE_WINDOWPOS_REG_KEY;
	static const CString CHECKIN_TABLE_WINDOWPOS_REG_KEY;
	static const CString FLIGHTPLAN_TABLE_WINDOWPOS_REG_KEY;
	static const CString DEMAND_TABLE_WINDOWPOS_REG_KEY;
	static const CString EQUIPMENT_TABLE_WINDOWPOS_REG_KEY;	
	static const CString FLIND_TABLE_WINDOWPOS_REG_KEY;
	static const CString FLIGHTJOBS_TABLE_WINDOWPOS_REG_KEY;
	static const CString PREPLANNING_TABLE_WINDOWPOS_REG_KEY;
	static const CString CONFLICTS_TABLE_WINDOWPOS_REG_KEY;
	static const CString ATTENTION_TABLE_WINDOWPOS_REG_KEY;

	static const CString UNDO_DIALOG_WINDOWPOS_REG_KEY;
	static const CString SEARCH_DIALOG_WINDOWPOS_REG_KEY;
	static const CString SETUP_DIALOG_WINDOWPOS_REG_KEY;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COpssPmApp)
	public:
	virtual BOOL InitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(COpssPmApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL IsAlreadyRunning();
private:
	CMutex*	pomMutex;
};


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// InitialLoad dialog

class InitialLoadDlg : public CDialog
{
// Construction
public:
	InitialLoadDlg(CWnd* pParent = NULL);   // standard constructor
	
	~InitialLoadDlg(void);
	void SetProgress(int ipProgress);
	void SetMessage(CString opMessage);

	BOOL DestroyWindow(void);

// Dialog Data
	//{{AFX_DATA(InitialLoadDlg)
	enum { IDD = IDD_INITIALLOAD };
	CProgressCtrl	m_Progress;
	CListBox	m_MsgList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(InitialLoadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(InitialLoadDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

extern InitialLoadDlg *pogInitialLoad;


#endif _OPSSPMMAIN_
