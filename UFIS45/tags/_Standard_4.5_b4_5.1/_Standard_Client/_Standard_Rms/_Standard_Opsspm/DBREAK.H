// dbreak.h : header file
//
#ifndef _CBREAKJOBDIALOG_H_
#define _CBREAKJOBDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// CBreakJobDialog dialog

class CBreakJobDialog : public CDialog
{
// Construction
public:
	CBreakJobDialog(CWnd* pParent,CTime opDate);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBreakJobDialog)
	enum { IDD = IDD_BREAKJOB };
	CEdit	m_DurationCtrl;
	CButton	m_CoffeeBreakCtrl;
	CString	m_Name;
	CTime	m_StartTime;
	CTime	m_StartDate;
	int		m_Duration;
	int		m_PauseType;
	BOOL 	m_CoffeeBreak;
	//}}AFX_DATA

	BOOL	bmPrevCoffeeBreak;
	int		imPrevDuration;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBreakJobDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBreakJobDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusMin();
	afx_msg void OnMin25();
	afx_msg void OnMin45();
	afx_msg void OnMin60();
	virtual void OnOK();
	afx_msg void OnCoffeebreak();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif // _CBREAKJOBDIALOG_H_
