// gaviewer.cpp : implementation file
//  
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							Change the GetFmJobsByPrid() to be GetFmgJobsByAlid().
//							Since we now have a new job type FMJ for flight manager,
//							we don't use the field PRID to check if this pool job
//							is of a flight manager or a normal staff to create
//							managers information for each gate area anymore. Instead,
//							we have a new separate job type FMG to do this function.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <gantt.h>
#include <gbar.h>
#include <CedaFlightData.h>
#include <CCSPtrArray.h>
#include <CedaFlightData.h>
#include <AllocData.h>
#include <cviewer.h>
#include <ccsddx.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <conflict.h>
#include <GateViewer.h>
#include <conflict.h>
#include <ConflictConfigTable.h>
#include <BasicData.h>
#include <DataSet.h>
#include <CedaPaxData.h>
#include <CedaAltData.h>
#include <CedaDpxData.h>
#include <FieldConfigDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

//Singapore
const CString GateDiagramViewer::omTerminal1 = "1";
const CString GateDiagramViewer::omTerminal2 = "2";
const CString GateDiagramViewer::omTerminal3 = "3";
const CString GateDiagramViewer::omTerminal4 = "4";
const CString GateDiagramViewer::omTerminalB = "B";

static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	if ((strcmp((**e1).Jtco,JOBPOOL) != 0) && (strcmp((**e2).Jtco,JOBPOOL) == 0) )
	{
		return -1;
	}
	else
	{
		if ((strcmp((**e1).Jtco,JOBPOOL) == 0) && (strcmp((**e2).Jtco,JOBPOOL) != 0) )
		{
			return 1;
		}
		else
		{
			//if ((**e1).Acfr.GetTime() == (**e2).Acfr.GetTime() == 0)
			
			if ((**e2).Acfr.GetTime() < (**e1).Acfr.GetTime())
				return -1;
			if ((**e2).Acfr.GetTime() > (**e1).Acfr.GetTime())
				return 1;
				return 0;

	//		return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
		}
	}
	
}


static int ByStartTime(const GATE_BARDATA **pppBar1, const GATE_BARDATA **pppBar2);
static int ByStartTime(const GATE_BARDATA **pppBar1, const GATE_BARDATA **pppBar2)
{
	return (int)((**pppBar1).StartTime.GetTime() - (**pppBar2).StartTime.GetTime());
}

/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer
//
GateDiagramViewer::GateDiagramViewer()
{
	pomAttachWnd = NULL;
	omStartTime = TIMENULL;
	omEndTime = TIMENULL;

	ogCCSDdx.Register(this, FLIGHT_DELETE,CString("GAVIEWER"), CString("Flight Delete"), GateDiagramCf);
	ogCCSDdx.Register(this, FLIGHT_CHANGE,CString("GAVIEWER"), CString("Flight Change"), GateDiagramCf);
	ogCCSDdx.Register(this, JOB_NEW,CString("GAVIEWER"), CString("Job New"), GateDiagramCf);
	ogCCSDdx.Register(this, JOB_CHANGE,CString("GAVIEWER"), CString("Job Changed"), GateDiagramCf);
	ogCCSDdx.Register(this, JOB_DELETE,CString("GAVIEWER"), CString("Job Delete"), GateDiagramCf);
	ogCCSDdx.Register(this, DEMAND_NEW,CString("GAVIEWER"), CString("Demand New"), GateDiagramCf);
	ogCCSDdx.Register(this, DEMAND_CHANGE,CString("GAVIEWER"), CString("Demand Changed"), GateDiagramCf);
	ogCCSDdx.Register(this, DEMAND_DELETE,CString("GAVIEWER"), CString("Demand Delete"), GateDiagramCf);
	ogCCSDdx.Register(this, PRM_DEMAND_CHANGE,CString("GAVIEWER"), CString("Prm Demand Changed"), GateDiagramCf);
	ogCCSDdx.Register(this, REDISPLAY_ALL,CString("GAVIEWER"), CString("Redisplay All"), GateDiagramCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("GAVIEWER"), CString("Update Conflict Setup"), GateDiagramCf);
	ogCCSDdx.Register(this, FLIGHT_SELECT_GATEGANTT,CString("GAVIEWER"),CString("Flight Select"), GateDiagramCf);
}

GateDiagramViewer::~GateDiagramViewer()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	DeleteAll();
}

void GateDiagramViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}


void GateDiagramViewer::GateDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	GateDiagramViewer *polViewer = (GateDiagramViewer *)popInstance;

	if (polViewer->bmNoUpdatesNow == FALSE)
	{
		if (ipDDXType == FLIGHT_CHANGE)
		{
			polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);
		}
		else if (ipDDXType == FLIGHT_DELETE)
		{
			polViewer->ProcessFlightDelete((long)vpDataPointer);
		}
		else if (ipDDXType == DEMAND_CHANGE || ipDDXType == DEMAND_DELETE || ipDDXType == DEMAND_NEW)
		{
			polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
		}
		else if (ipDDXType == PRM_DEMAND_CHANGE)
		{
			polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
		}
		else if (ipDDXType == JOB_NEW)
		{
			polViewer->ProcessFmgJobNew((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobNew((JOBDATA *) vpDataPointer);
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == JOB_CHANGE)
		{
			// we have no ProcessFmgJobChange() for this moment, so I just use delete and add.
			polViewer->ProcessFmgJobDelete((JOBDATA *)vpDataPointer);
			polViewer->ProcessFmgJobNew((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobChange((JOBDATA *) vpDataPointer);
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == JOB_DELETE)
		{
			polViewer->ProcessFmgJobDelete((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobDelete((JOBDATA *) vpDataPointer);
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == REDISPLAY_ALL || ipDDXType == UPDATE_CONFLICT_SETUP)
		{
			polViewer->DeleteAll();
			polViewer->ChangeViewTo();
			polViewer->pomAttachWnd->Invalidate();
		}
		else if (ipDDXType == FLIGHT_SELECT_GATEGANTT)
		{
			polViewer->ProcessFlightSelect((FLIGHTDATA *)vpDataPointer);
		}
	}
	else
	{
		// should never reach this point
	//	ASSERT( 0 );
	}
}

void GateDiagramViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prpJob->Flur);
		if(prlFlight != NULL)
		{
			ProcessFlightChange(prlFlight);
		}
	}
}

void GateDiagramViewer::ProcessDemandChange(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		long llFlightUrno = ogDemandData.GetFlightUrno(prpDemand);

		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno);
		if(prlFlight != NULL)
		{
			ProcessFlightChange(prlFlight);
		}
	}
}

void GateDiagramViewer::ProcessFlightChange(FLIGHTDATA *prpFlight, bool bpCheckRotation /*=true*/)
{
	if(prpFlight == NULL)
		return;

	int ilGroupno, ilLineno;
	FLIGHTDATA *prlRotationFlight;
	CString olAlc = ogAltData.FormatAlcString(prpFlight->Alc2,prpFlight->Alc3);;

	DeleteBarsForFlight(prpFlight->Urno);


	if(IsPassFilter(GetString(IDS_STRING61345),prpFlight)) // "Ohne Gate"
	{
		int ilWithoutGateGroup;
		if(FindGroup(GetString(IDS_STRING61345),ilWithoutGateGroup))
		{
			int ilLastLine	  = GetLineCount(ilWithoutGateGroup) - 1;
			int ilUpdatedLine = CreateLinesForFlightsWithoutGates(prpFlight);
			if (ilUpdatedLine != -1)
			{
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilUpdatedLine, ilWithoutGateGroup);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
	}
	if(IsPassFilter(GetString(IDS_ALL_FLIGHTS),prpFlight))
	{
		int ilAllFlightsGroup;
		if(FindGroup(GetString(IDS_ALL_FLIGHTS),ilAllFlightsGroup))
		{
			int ilLastLine	  = GetLineCount(ilAllFlightsGroup) - 1;
			int ilUpdatedLine = CreateLinesForAllFlights(prpFlight);
			if(ilUpdatedLine != -1)
			{
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilUpdatedLine, ilAllFlightsGroup);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
	}
	UpdateDiagramTerminals(prpFlight); //Singapore

	CUIntArray olReturnFlightTypes;
	int ilNumReturnFlightTypes = GetReturnFlightTypes(prpFlight, olReturnFlightTypes, "");
	for(int ilRetFlight = 0; ilRetFlight < ilNumReturnFlightTypes; ilRetFlight++)
	{
		CString olGate = ogFlightData.GetGate(prpFlight,olReturnFlightTypes[ilRetFlight]);
		if(!olGate.IsEmpty())
		{
			ilGroupno = ilLineno = -1;
			while (FindGroupAndLine(olGate,ilGroupno,ilLineno))
			{
				if(IsPassFilter(GetGroup(ilGroupno)->GateAreaId,prpFlight))
				{
					int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
					ogConflicts.CheckConflicts(*prpFlight,FALSE,FALSE,TRUE);
					MakeBar(ilGroupno, ilLineno, prpFlight,false,olReturnFlightTypes[ilRetFlight]);
					if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
						{
							LONG lParam = MAKELONG(ilLineno, ilGroupno);
							if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
							else
								pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
						}
					}
				}
			}
		}

		if(bpCheckRotation && (prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight, olReturnFlightTypes[ilRetFlight])) != NULL)
			ProcessFlightChange(prlRotationFlight, false);
	}
}

void GateDiagramViewer::ProcessFlightDelete(long lpFlightUrno)
{
	DeleteBarsForFlight(lpFlightUrno);
}

void GateDiagramViewer::ProcessFlightSelect(FLIGHTDATA *prpFlight)
{
	if (!prpFlight)
		return;

	GATE_SELECTION	rolSelection;
	if (!FindFlightBar(prpFlight->Urno,rolSelection.imGroupno,rolSelection.imLineno,rolSelection.imBarno, true, prpFlight->ReturnFlightType))
		return;

	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{

		LONG lParam = reinterpret_cast<LONG>(&rolSelection);
		pomAttachWnd->SendMessage(WM_SELECTDIAGRAM,UD_SELECTBAR, lParam);
	}
}

void GateDiagramViewer::ProcessSpecialJobChange(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
		return; // not interested in other jobtypes
	if (!FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		return; // can't do anything

	// Update the Viewer's data
	int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	DeleteBar(ilGroupno, ilLineno, ilBarno);
	MakeSpecialBar(ilGroupno, ilLineno, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(ilLineno, ilGroupno);
		if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}
}

void GateDiagramViewer::ProcessSpecialJobDelete(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
	{
		return; // not interested in other jobtypes
	}
	if (!FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		return; // can't do anything

	// Update the Viewer's data
	int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	DeleteBar(ilGroupno, ilLineno, ilBarno);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(ilLineno, ilGroupno);
		if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}
}

void GateDiagramViewer::ProcessSpecialJobNew(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
	{
		return; // not interested in other jobtypes
	}
	if (FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		ProcessSpecialJobChange(prpJob); // job already there

	// Update the Viewer's data
	//int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	ilGroupno = -1;
	ilLineno = -1;
	while(FindGroupAndLine(prpJob->Alid,ilGroupno,ilLineno))
	{
		int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
		MakeSpecialBar(ilGroupno, ilLineno, prpJob);

		// Notifies the attached window (if exist)
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			LONG lParam = MAKELONG(ilLineno, ilGroupno);
			if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			else
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
		}
	}
}

void GateDiagramViewer::ProcessFmgJobNew(JOBDATA *prpJob)
{
// Id 19-Sep-96
	// Don't interest it if it's not a flight manager gate job
	if (CString(prpJob->Jtco) != JOBFMGATEAREA)
		return;

	int ilGroupno;
	if (!FindGroup(prpJob->Alid, ilGroupno))
        return; // no such group in the viewer buffer right now, just do nothing
// end of Id 19-Sep-96

	// Update the Viewer's data
	MakeManager(ilGroupno, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroupno);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}
}

void GateDiagramViewer::ProcessFmgJobDelete(JOBDATA *prpJob)
{
// Id 19-Sep-96
	// Don't interest it if it's not a flight manager gate job
	if (CString(prpJob->Jtco) != JOBFMGATEAREA)
		return;
// end of Id 19-Sep-96

	int ilGroupno, ilManagerno;
	if (!FindManager(prpJob->Urno, ilGroupno, ilManagerno))
        return; // no such group in the viewer buffer right now, just do nothing

	// Update the Viewer's data
	DeleteManager(ilGroupno, ilManagerno);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroupno);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}
}


// causes MakeBar to be called twice for return flights (return flight is a single record for both the arr and dep)
int GateDiagramViewer::GetReturnFlightTypes(FLIGHTDATA *prpFlight, CUIntArray &ropReturnFlightTypes, CString opAlid /*""*/)
{
	ropReturnFlightTypes.RemoveAll();
	if(prpFlight != NULL)
	{
		if(ogFlightData.IsReturnFlight(prpFlight))
		{
			// add 2 bars for return flights once for inbound once for outbound
			if(opAlid.IsEmpty() || !strcmp(prpFlight->Gta1,opAlid))
				ropReturnFlightTypes.Add(ID_ARR_RETURNFLIGHT);
			if(opAlid.IsEmpty() || !strcmp(prpFlight->Gta1,opAlid))
				ropReturnFlightTypes.Add(ID_DEP_RETURNFLIGHT);
		}
		else
		{
			ropReturnFlightTypes.Add(ID_NOT_RETURNFLIGHT);
		}
	}

	return ropReturnFlightTypes.GetSize();
}

/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer -- Filtering, Sorting, and Grouping
//
// Requirements:
// In the Gate Gantt Diagram we have only one filter, by Gate Areas.
// It will be always groupped and sorted by Gate Areas.
//
// Methods for change the view:
// ChangeViewTo		Change view, reload everything from Ceda????Data
// UpdateManagers	Rebuild the manager lists for all groups
//
// PrepareFilter	Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter	Prepare sort criterias before sorting take place
//
// IsPassFilter		Return TRUE if the given record data satisfies the filter
// CompareGroup		Return 0 or -1 or 1 as the result of comparison of two groups
// CompareLine		Return 0 or -1 or 1 as the result of comparison of two lines
// CompareManager	Return 0 or -1 or 1 as the result of comparison of two managers
//
// Programmer notes:
// At the program start up, we will create a default view for each dialog. This will
// overwrite the <default> view saved in the last time execution.
//
// When the viewer is constructed or everytimes the view changes, we will clear
// everything from the viewer buffer and do the insertion sorting. We will insert
// the record only if it satisfies our filter. Conceptually we will filter the record
// first, then sort in (by insertion sort), and group in.
//
// To let the filter work fast, we will implement a CMapStringToPtr for every filter
// condition. For example, let's say that we have a CStringArray for every possible
// values of a filter. We will have a CMapStringToPtr which could tell us the given
// key value is selected in the filter or not.
//
void GateDiagramViewer::ChangeViewTo(const char *pcpViewName,
	BOOL bpIncludeArrivalFlights, BOOL bpIncludeDepartureFlights,
	CTime opStartTime, CTime opEndTime)
{
	DeleteAll();	// remove everything

	ogCfgData.rmUserSetup.GACV = pcpViewName;
	SelectView(pcpViewName);
	PrepareFilter();
	bmIncludeArrivalFlights = bpIncludeArrivalFlights;
	bmIncludeDepartureFlights = bpIncludeDepartureFlights;
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	ChangeViewTo();
}

void GateDiagramViewer::ChangeViewTo(void)
{
	MakeGroupsAndLines();
	MakeManagers();
	MakeBars();
}

void GateDiagramViewer::UpdateManagers(CTime opStartTime, CTime opEndTime)
{
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
	    while (GetManagerCount(ilGroupno) > 0)
		    DeleteManager(ilGroupno, 0);
	}
	MakeManagers();

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		{
			LONG lParam = MAKELONG(-1, ilGroupno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
		}
	}
}

void GateDiagramViewer::PrepareFilter()
{
	bmUseAllGateAreas = SetFilterMap("Gatebereich", omCMapForGateArea, GetString(IDS_ALLSTRING));
	bmUseAllAlcds = SetFilterMap("Airline", omCMapForAlcd, GetString(IDS_ALLSTRING));
	bmUseAllAgents = SetFilterMap("Agents", omCMapForAgents, GetString(IDS_ALLSTRING));
}

BOOL GateDiagramViewer::IsPassFilter(const char *pcpGateArea)
{
	void *p;
	// there is only one filter, so we just check it with possible gate areas
	return bmUseAllGateAreas || omCMapForGateArea.Lookup(CString(pcpGateArea), p);
}

BOOL GateDiagramViewer::IsPassFilter(const char *pcpGateArea, FLIGHTDATA *prpFlight)
{

	#ifdef	_DEBUG_GV
	FILE *fp = fopen("C:\\tmp\\GateDiaFilter.log","at+");
	ASSERT(fp);
	fprintf(fp,"IsPassFilter called for <%s> IsPrm = %s PRMC=<%s> <%s>/%ld <%s> <%s> <%s/%s> \n",pcpGateArea,
		bgIsPrm ? "TRUE" : "FALSE", prpFlight->Prmc, prpFlight->Flno,prpFlight->Urno,prpFlight->Adid,
		*prpFlight->Adid == 'A' ?    prpFlight->Tifa.Format("%d/%H:%M:%S")  :  prpFlight->Tifd.Format("%d/%H:%M:%S"),
		prpFlight->Alc2,prpFlight->Alc3);
	#endif

	if (bgIsPrm)
	{
		if ( *prpFlight->Prmc != 'Y')
		{
			if (!ogDemandData.HasPrmDemands(prpFlight->Urno))
			{
			#ifdef	_DEBUG_GV
				fprintf(fp,"Flight not used due to PRMC != 'Y' and has no PRM demands\n");
				fclose(fp);
			#endif
			return FALSE;
			}
			else
			{
			#ifdef	_DEBUG_GV
				fprintf(fp,"Flight PRMC not set to 'Y' but has PRM demands\n");
				fclose(fp);
				return TRUE;
			#endif
			}
		}
	}

	CString olAlc = ogAltData.FormatAlcString(prpFlight->Alc2,prpFlight->Alc3);
	void *p;

	BOOL blIsGateAreaOK	= bmUseAllGateAreas || strlen(pcpGateArea) <= 0 || omCMapForGateArea.Lookup(CString(pcpGateArea), p);
    BOOL blIsAlcdOK		= bmUseAllAlcds || omCMapForAlcd.Lookup(CString(olAlc), p);
    BOOL blIsAgentOK	= bmUseAllAgents || (strlen(prpFlight->Hapx) <= 0 && omCMapForAgents.Lookup(GetString(IDS_NOAGENT),p)) || omCMapForAgents.Lookup(CString(prpFlight->Hapx), p);

	#ifdef	_DEBUG_GV
	if (! blIsGateAreaOK ) 
	{
		fprintf(fp,"Flight not used due to GateArea not OK UseAllGateAreas=%s p=%x \n", bmUseAllGateAreas ? "TRUE" : "FALSE", p);
	}
	if (! blIsAlcdOK ) 
	{
		fprintf(fp,"Flight not used due to Alcd not OK bmUseAllAlcds=%s Alc=<%s> p=%x \n", bmUseAllAlcds ? "TRUE" : "FALSE", olAlc, p);
	}
	if (! blIsAgentOK ) 
	{
		fprintf(fp,"Flight not used due to Handling Agent not OK bmUseAllAgents=%s <%s> p=%x \n", bmUseAllAgents ? "TRUE" : "FALSE", prpFlight->Hapx,  p);
	}
	fclose(fp);
	#endif

	return blIsGateAreaOK && blIsAlcdOK && blIsAgentOK;
}

int GateDiagramViewer::CompareGroup(GATE_GROUPDATA *prpGroup1, GATE_GROUPDATA *prpGroup2)
{
	// Groups in GateDiagram always ordered by the displayed text, so let's compare them.
	CString &s1 = prpGroup1->Text;
	CString &s2 = prpGroup2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

int GateDiagramViewer::CompareLine(GATE_LINEDATA *prpLine1, GATE_LINEDATA *prpLine2)
{
	// Lines in GateDiagram always ordered by the displayed text, so let's compare them.
	CString &s1 = prpLine1->Text;
	CString &s2 = prpLine2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

int GateDiagramViewer::CompareManager(GATE_MANAGERDATA *prpManager1, GATE_MANAGERDATA *prpManager2)
{
	// Compare manager -- we will sort them by name
	return  (prpManager1->EmpLnam == prpManager2->EmpLnam)? 0:
		(prpManager1->EmpLnam > prpManager2->EmpLnam)? 1: -1;
}


/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer -- Data for displaying graphical objects
//
// The related graphical class: GateDiagram, GateChart, and GateGantt will use this
// GateViewer as the source of data for displaying GanttLine and GanttBar objects.
//
// We let the previous section handles all filtering, sorting, and grouping. In this
// section, we will provide a set of methods which will help us to create the data
// represent the graphical GanttLine and GanttChart as easiest as possible.
//
// Methods for creating graphical objects:
// MakeGroupsAndLines		Create groups and lines that has to be displayed.
// MakeManagers				Create managers for all groups
// MakeBars					Create bars for all lines in all groups
// MakeManager				Create a manager for the specified group
// MakeBar					Create a flight bar (determine its length with demands)
// MakeIndicators			Create indicators for flight bar
//
// GetDemands				Get all demands for the given flight
//
// ArrivalFlightStatus		Return a string displayed for arrival flight
// DepartureFlightStatus	Return a string displayed for departure flight
// TurnAroundFlightStatus	Return a string displayed for turn-around flight
//
void GateDiagramViewer::MakeGroupsAndLines()
{
	// reset all flights (and shadow bars) to not displayed
	int ilFlightCount = ogFlightData.omData.GetSize();
	for ( int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		ogFlightData.omData[ilLc].IsDisplayed = FALSE;
		ogFlightData.omData[ilLc].IsShadoBarDisplayed = FALSE;
	}

	// ogGateAreas contains all metawerte for gates areas

	// Create a group for each gate area (only areas which satisfy the condition)
	CCSPtrArray <ALLOCUNIT> olGateAreas;
	CCSPtrArray <ALLOCUNIT> olGates;

	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_GATEGROUP,olGateAreas);
	int ilNumGateAreas = olGateAreas.GetSize();
	for(int ilGateArea = 0; ilGateArea < ilNumGateAreas; ilGateArea++)
	{
		ALLOCUNIT *prlGateArea = &olGateAreas[ilGateArea];

		if (!IsPassFilter(prlGateArea->Name))	// this gate area is not in the filter?
			continue;

		// create a chart for each group
        GATE_GROUPDATA rlGroup;
		rlGroup.GateAreaId = prlGateArea->Name;
		rlGroup.Text = prlGateArea->Name;
		int ilGroupno = CreateGroup(&rlGroup);

		ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_GATEGROUP,prlGateArea->Name,olGates);
		int ilNumGates = olGates.GetSize();
		for(int ilGate = 0; ilGate < ilNumGates; ilGate++)
		{
			ALLOCUNIT *prlGate = &olGates[ilGate];
			GATE_LINEDATA rlLine;
			rlLine.Text = prlGate->Name;
			rlLine.IsStairCaseGate = false;
            CreateLine(ilGroupno, &rlLine);
		}
	}

	// create a group containing all flights
	CString olAllFlightsGrp = GetString(IDS_ALL_FLIGHTS);
	imAllFlightsGroup = -1;
	if(IsPassFilter(olAllFlightsGrp))
	{
		GATE_GROUPDATA rlGroup;
		rlGroup.GateAreaId = olAllFlightsGrp; 
		rlGroup.Text = olAllFlightsGrp;
		CreateGroup(&rlGroup);
	}

	// create a group for flights without gates
	CString olWithoutGateName = GetString(IDS_STRING61345); // "Ohne Gate"
	imFlightsWithoutGatesGroup = -1;
	if(IsPassFilter(olWithoutGateName))
	{
		GATE_GROUPDATA rlGroup;
		rlGroup.GateAreaId = olWithoutGateName; 
		rlGroup.Text = olWithoutGateName;
		CreateGroup(&rlGroup);
	}

	// get indexes - don't do this until both are created as inserting can change the indexes
	//Singapore
	CreateTerminalGroups();
	FindGroup(olAllFlightsGrp,imAllFlightsGroup);
	FindGroup(olWithoutGateName,imFlightsWithoutGatesGroup);
}

void GateDiagramViewer::MakeManagers()
{
	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		// Create managers associated with this CCI area
		CCSPtrArray<JOBDATA> olFmgJobs;
		ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ilGroupno)->GateAreaId);
		int ilFmgCount = olFmgJobs.GetSize();
		for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
			MakeManager(ilGroupno, &olFmgJobs[ilLc]);
	}
}

void GateDiagramViewer::MakeBars()
{
	int ilLc;
	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		if(ilGroupno == imFlightsWithoutGatesGroup)
		{
			CreateLinesForFlightsWithoutGates();
		}
		else if(ilGroupno == imAllFlightsGroup)
		{
			CreateLinesForAllFlights();
		}
		//Singapore
		else if(ilGroupno == imT1FlightsGroup || ilGroupno == imT2FlightsGroup ||
			    ilGroupno == imT3FlightsGroup || ilGroupno == imT4FlightsGroup ||
				ilGroupno == imTBFlightsGroup || ilGroupno == imWTFlightsGroup)
		{
			CreateLinesTerminalFlights(ilGroupno);
		}
			   
		else
		{
			GATE_GROUPDATA *prlGroup = GetGroup(ilGroupno);
			FLIGHTDATA *prlFlight = NULL;

			int ilLineCount = GetLineCount(ilGroupno);
			for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
			{
				if ((bmIncludeArrivalFlights) || (bmIncludeDepartureFlights))
				{
					CCSPtrArray<FLIGHTDATA> olFlights;
					CString olGate = GetLineText(ilGroupno, ilLineno);
					ogFlightData.GetFlightsByGate(olFlights, olGate);
					for (ilLc = 0; ilLc < olFlights.GetSize(); ilLc++)
					{
						prlFlight = &olFlights[ilLc];
						if (IsPassFilter(prlGroup->GateAreaId,prlFlight))
						{
							CUIntArray olReturnFlightTypes;
							int ilNumReturnFlightTypes = GetReturnFlightTypes(prlFlight, olReturnFlightTypes, olGate);
							for(int ilF = 0; ilF < ilNumReturnFlightTypes; ilF++)
							{
								MakeBar(ilGroupno, ilLineno, prlFlight, false, olReturnFlightTypes[ilF]);
							}
						}
					}
				}
				CCSPtrArray<JOBDATA> olJobs;
				ogJobData.GetSpecialJobsByAlid(olJobs, GetLineText(ilGroupno, ilLineno), ALLOCUNITTYPE_GATE);
				for (ilLc = 0; ilLc < olJobs.GetSize(); ilLc++)
				{
					MakeSpecialBar(ilGroupno, ilLineno, &olJobs[ilLc]);
				}
			}
		}

	}
}

// if single flight is not NULL then recreate a bar for the single flight
// only, otherwise create bars for all flights without gates
//
//int GateDiagramViewer::CreateLinesForFlightsWithoutGates(FLIGHTDATA *prpSingleFlight /*= NULL*/)
//{
//	int ilLastLineUpdated = -1;
//	int ilGroupno = 0L;
//	if(FindGroup(GetString(IDS_STRING61345),ilGroupno)  && (bmIncludeArrivalFlights || bmIncludeDepartureFlights))
//	{
//		FLIGHTDATA *prlFlight;
//		CCSPtrArray<FLIGHTDATA> olFlights;
//		if(prpSingleFlight == NULL)
//		{
//			ogFlightData.GetFlightsWithoutGates(olFlights);
//		}
//		else
//		{
//			olFlights.Add(prpSingleFlight);
//		}
//
//		int ilNumFlights = olFlights.GetSize();
//
//		if(ilNumFlights > 0)
//		{
//			// Need to find a space on an existing line to insert the new bar.
//			// In order to do this a dummy bar (on a dummy line) is created so
//			// that the same function is used (MakeBar) to create the new bar,
//			// and thus the new bar is garanteed to have the correct length.
//			// This dummy bar is then used to find a space big enough on existing
//			// lines to insert the bar. If no lines are found then a new line is created.
//
//			GATE_LINEDATA *prlLine;
//			int ilLineCount, ilLineno;
//			GATE_LINEDATA rlDummyLine;
//			CString olDummyLineText;
//			olDummyLineText.Format("% 5d",0);
//			rlDummyLine.Text = olDummyLineText;
//			CreateLine(ilGroupno, &rlDummyLine);
//			int ilDummyLineNum;
//
//			for(int ilLc = 0; ilLc < ilNumFlights; ilLc++)
//			{
//				prlFlight = &olFlights[ilLc];
//				BOOL blPassFilter = IsPassFilter("",prlFlight);
//				if(blPassFilter && ((bmIncludeArrivalFlights && ogFlightData.IsArrival(prlFlight))
//					|| (bmIncludeDepartureFlights && ogFlightData.IsDeparture(prlFlight))))
//				{
//					if(FindLine(olDummyLineText, ilGroupno, ilDummyLineNum))
//					{
//						MakeBar(ilGroupno, ilDummyLineNum, prlFlight);
//						GATE_LINEDATA *prlDummyLine = GetLine(ilGroupno, ilDummyLineNum);
//						if(prlDummyLine->Bars.GetSize() > 0) // make sure the dummy bar was created
//						{
//							GATE_BARDATA *prlBarToAdd = GetBar(ilGroupno, ilDummyLineNum, 0);
//
//							int ilSelectedLine = -1;
//							ilLineCount = GetLineCount(ilGroupno);
//							for(ilLineno = 0; ilSelectedLine == -1 && ilLineno < ilLineCount; ilLineno++)
//							{
////								if(prpSingleFlight != NULL) TRACE("Line %d ",ilLineno);
//								prlLine = GetLine(ilGroupno, ilLineno);
//								if(prlLine->Text != olDummyLineText)
//								{
//									int ilNumBars = prlLine->Bars.GetSize();
//									if(ilNumBars <= 0)
//									{
//										ilSelectedLine = ilLineno;
////										if(prpSingleFlight != NULL) TRACE("Line is empty so add");
//									}
//									else
//									{
//										CTimeSpan olOneMinute(0,0,1,0);
////										for(int ilBar2 = 0; prpSingleFlight != NULL && ilBar2 < ilNumBars; ilBar2++)
////										{
////											TRACE("<start %s end %s> ",prlLine->Bars[ilBar2].StartTime.Format("%H%M"),prlLine->Bars[ilBar2].EndTime.Format("%H%M"));
////										}
//
//										if(ilNumBars > 0 && prlLine->Bars[0].StartTime > prlBarToAdd->EndTime)
//										{
//											//  the new bar fits in before the first one
//											ilSelectedLine = ilLineno;
//										}
//
//										for(int ilBar = 0; ilSelectedLine == -1 && ilBar < ilNumBars; ilBar++)
//										{
//											CTime olPrevEnd = prlLine->Bars[ilBar].EndTime;
//											CTime olNextBegin = ((ilBar+1) < ilNumBars) ? prlLine->Bars[ilBar+1].StartTime : (prlBarToAdd->EndTime+olOneMinute);
//											if(prlBarToAdd->StartTime > olPrevEnd && prlBarToAdd->EndTime < olNextBegin)
//											{
//												// the new bar fits in the free space
//												ilSelectedLine = ilLineno;
//											}
//										}
//									}
//								}
////								if(prpSingleFlight != NULL) TRACE("\n");
//							}
//								 
//							if(ilSelectedLine != -1)
//							{
//								// existing line found so add the new bar and sort all bars in this line by duty start
//								MakeBar(ilGroupno, ilSelectedLine, prlFlight);
//								prlLine = GetLine(ilGroupno, ilSelectedLine);
//								prlLine->Bars.Sort(ByStartTime);
//								ilLastLineUpdated = ilSelectedLine - 1; // because we delete line no. 0 later !!!!
//							}	
//							else
//							{
//								// no lines free so create a new one
//								GATE_LINEDATA rlLine;
//								rlLine.Text.Format("% 5d",ilLineCount);
//								rlLine.IsStairCaseGate = false;
//								int ilLineNum = CreateLine(ilGroupno, &rlLine);
//								MakeBar(ilGroupno, ilLineNum, prlFlight);
//								ilLastLineUpdated = ilLineNum - 1;		// because we delete line no. 0 later !!!
//							}
//
//							DeleteBar(ilGroupno, ilDummyLineNum, 0);
//						}
//					}
//				}
//			}
//
//			if (FindLine(olDummyLineText, ilGroupno, ilDummyLineNum))
//			{
//				DeleteLine(ilGroupno, ilDummyLineNum);
//			}
//		}
//	}
//
//
//	return ilLastLineUpdated;
//}
//

void GateDiagramViewer::MakeManager(int ipGroupno, JOBDATA *prpJob)
{
	if (!IsOverlapped(prpJob->Acfr, prpJob->Acto, omStartTime, omEndTime))
		return;
	
	GATE_MANAGERDATA rlFm;
	rlFm.JobUrno = prpJob->Urno;
	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
	rlFm.ShiftStid = prlShift != NULL ? prlShift->Sfca : "";
	rlFm.ShiftTmid = prlShift != NULL ? prlShift->Egrp : "";
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
	rlFm.EmpLnam = CString(prlEmp != NULL ? prlEmp->Lanm : "");
	if (prlShift != NULL)
	{
		if ((prlShift->Avfa < prpJob->Acfr) || (prlShift->Avta > prpJob->Acto))
		{
			rlFm.ShiftStid += "'";
		}
	}
	CreateManager(ipGroupno, &rlFm);
}


// bpIgnoreGate -	if true then a turnaround bar will be created for turnaround flights with turnaround
// demands even if they don't have the same gate (useful for the "All Flights" group)
void GateDiagramViewer::MakeBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, bool bpIgnoreGate /* = false*/, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	bool blIsArrival = (ogFlightData.IsArrival(prpFlight) || ipReturnFlightType == ID_ARR_RETURNFLIGHT);		// Inbound flight
	bool blIsDeparture = (ogFlightData.IsDeparture(prpFlight) || ipReturnFlightType == ID_DEP_RETURNFLIGHT);	// Outbound flight
	bool blDisplayGate = (ipGroupno == imAllFlightsGroup) ? true : false;

	CCSPtrArray<DEMANDDATA> olArrivalDemands;
	CCSPtrArray<DEMANDDATA> olDepartureDemands;

	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight, ipReturnFlightType);
	int ilTypeIdx = (ipReturnFlightType == ID_DEP_RETURNFLIGHT) ? 1 : 0;

	FLIGHTDATA *prlArr = NULL, *prlDep = NULL;
	if(blIsArrival)
	{
		prlArr = prpFlight;
		prlDep = prlRotationFlight;
	}
	else
	{
		prlArr = prlRotationFlight;
		prlDep = prpFlight;
	}

	if(blIsArrival)
	{
		if(!bmIncludeArrivalFlights)
		{
			return;
		}

		// load arrival and turnaround demands
		ogDemandData.GetDemandsByInboundFlight(olArrivalDemands, prpFlight->Urno, "", ALLOCUNITTYPE_GATE);
		if(olArrivalDemands.GetSize() <= 0)
		{
			if (bmIncludeArrivalFlights)
			{
				MakeBarWithoutDemand(ipGroupno,ipLineno,prpFlight,ipReturnFlightType);
				return;
			}
		}

		if(prlRotationFlight != NULL)
		{
			// load departure and turnaround demands
			ogDemandData.GetDemandsByOutboundFlight(olDepartureDemands, prlRotationFlight->Urno, "", ALLOCUNITTYPE_GATE);
		}
	}
	else if (blIsDeparture)
	{
		if(!bmIncludeDepartureFlights)
		{
			return;
		}

		// load departure and turnaround demands
		ogDemandData.GetDemandsByOutboundFlight(olDepartureDemands, prpFlight->Urno, "", ALLOCUNITTYPE_GATE);
		if(olDepartureDemands.GetSize() <= 0)
		{
			if (bmIncludeDepartureFlights)
			{
				MakeBarWithoutDemand(ipGroupno,ipLineno,prpFlight,ipReturnFlightType);
				return;
			}
		}

		if(prlRotationFlight != NULL)
		{
			// load arrival and turnaround demands
			ogDemandData.GetDemandsByInboundFlight(olArrivalDemands, prlRotationFlight->Urno, "", ALLOCUNITTYPE_GATE);
		}
	}
	else	// Invalid data?
	{
		return;
	}


	bool blHasTurnaroundDemands = false;
	int ilNumDemands, ilDem;
	CTime olArrStart = TIMENULL, olArrEnd = TIMENULL;
	CTime olDepStart = TIMENULL, olDepEnd = TIMENULL;
	DEMANDDATA *prlDemand;
	BOOL blIsConfirmedPSM = FALSE;

	ilNumDemands = olArrivalDemands.GetSize();
	for(ilDem = 0; ilDem < ilNumDemands; ilDem++)
	{
		prlDemand = &olArrivalDemands[ilDem];
		blIsConfirmedPSM = ogDpxData.IsConfirmedPSM(prlDemand->Uprm);

		if(olArrStart == TIMENULL || olArrStart > prlDemand->Debe)
		{
			olArrStart = prlDemand->Debe;
		}

		if(olArrEnd == TIMENULL || olArrEnd < prlDemand->Deen)
		{
			olArrEnd = prlDemand->Deen;
		}
	}

	ilNumDemands = olDepartureDemands.GetSize();
	for(ilDem = 0; ilDem < ilNumDemands; ilDem++)
	{
		prlDemand = &olDepartureDemands[ilDem];
		blIsConfirmedPSM = ogDpxData.IsConfirmedPSM(prlDemand->Uprm);

		if(olDepStart == TIMENULL || olDepStart > prlDemand->Debe)
		{
			olDepStart = prlDemand->Debe;
		}

		if(olDepEnd == TIMENULL || olDepEnd < prlDemand->Deen)
		{
			olDepEnd = prlDemand->Deen;
		}

		if(*prlDemand->Dety == TURNAROUND_DEMAND)
		{
			if(olArrStart == TIMENULL || olArrStart > prlDemand->Debe)
			{
				olArrStart = prlDemand->Debe;
			}

		if(olArrEnd == TIMENULL || olArrEnd < prlDemand->Deen)
		{	
			olArrEnd = prlDemand->Deen;
		}	
		if(prlArr != NULL && prlDep != NULL && prlArr->Urno == prlDemand->Ouri && prlDep->Urno == prlDemand->Ouro)
		{	
			blHasTurnaroundDemands = true;
		}	
	}	
	}

	bool blIsTurnaround = false;
	bool blHasSameGate = (ogFlightData.GetGate(prpFlight) == ogFlightData.GetGate(prlRotationFlight) || bpIgnoreGate);

	if(ogBasicData.bmDisplayTurnaroundPositions && prlRotationFlight != NULL && 
		blHasTurnaroundDemands && bmIncludeArrivalFlights && 
		bmIncludeDepartureFlights && blHasSameGate)
		{
		if(blIsArrival)	
		{	
			// don't need to display the arrival if the departure half has turnaround demands
			return;	
		}	
		blIsTurnaround = true;	

		// this is a turnaround so reload the arrival demands without the turnaround demands
		olArrivalDemands.RemoveAll();
		ogDemandData.GetInboundDemandsByInboundFlight(olArrivalDemands, prlRotationFlight->Urno, "", ALLOCUNITTYPE_GATE);
		}


	GATE_BARDATA rlBar;	
	rlBar.IsDelayed = CFST_NOTSET;

	CString olGatChangeText =  ogConflicts.GetAdditionalConflictText(CFI_GATECHANGE,prpFlight);

	BOOL blIsGateCfgUsed;
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, 	getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "GT_BAR_STATUS_TEXT",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (strcmp(pclTmpText,"NO")==0) 
		{
			blIsGateCfgUsed = FALSE;			
		}
	else if(strcmp(pclTmpText,"YES")==0)
		{
			blIsGateCfgUsed = 	TRUE;
		}


	CString olBarText;
	CString olStatusText = GetStatusText(prpFlight, blIsTurnaround ? prlRotationFlight : NULL);

	if (bmIncludeArrivalFlights && blIsArrival && !blIsTurnaround)
	{
		if(blDisplayGate && strlen(prpFlight->Gta1) > 0)
		{
			olBarText = prpFlight->Gta1 + CString("/");
		}
		olBarText += CString(prpFlight->Fnum) + " " + olGatChangeText;

		if(blIsGateCfgUsed)
		{
			olBarText = GetBarText(prpFlight, blIsArrival, blIsTurnaround ? prlRotationFlight : NULL);
			olStatusText = (olStatusText.IsEmpty()) ? olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		}
		else
		{
			olStatusText = olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight);
		}
		rlBar.Text = olBarText;
		//rlBar.StatusText = olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StatusText = olStatusText; 
		rlBar.StartTime = olArrStart;
		rlBar.EndTime = olArrEnd;
		rlBar.IsTmo = prpFlight->IsTmo;
		rlBar.NoDropOnThisBar = FALSE;
		rlBar.FlightType = GATEBAR_ARRIVAL;
	}
	else if (bmIncludeDepartureFlights && blIsDeparture && !blIsTurnaround)
	{
		olBarText = CString(prpFlight->Fnum) + " " + olGatChangeText;
		if(blDisplayGate && strlen(prpFlight->Gtd1) > 0)
		{
			olBarText += CString("/") + prpFlight->Gtd1;
		}

		if(blIsGateCfgUsed)
		{
			olBarText = GetBarText(prpFlight, blIsArrival, blIsTurnaround ? prlRotationFlight : NULL);
			olStatusText = (olStatusText.IsEmpty()) ? olBarText + DepartureFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		}
		else
		{
			olStatusText = olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight);
		}

		rlBar.Text = olBarText;
		//rlBar.StatusText = olBarText + DepartureFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StatusText = olStatusText;
		rlBar.StartTime = olDepStart;
		rlBar.EndTime = olDepEnd;
		rlBar.IsTmo = FALSE;
		rlBar.NoDropOnThisBar = FALSE;
		rlBar.FlightType = GATEBAR_DEPARTURE;
	}
	else if (blIsTurnaround) 
	{
		if(olArrStart == TIMENULL)
			olArrStart = olDepStart;
		if(olArrEnd == TIMENULL)
			olArrEnd = olDepEnd;

		if(blDisplayGate && strlen(prlRotationFlight->Gta1) > 0)
		{
			olBarText = prlRotationFlight->Gta1 + CString("/");
		}
		olBarText += prlRotationFlight->Fnum + CString("/") + prpFlight->Fnum;
		if(blDisplayGate && strlen(prpFlight->Gtd1) > 0)
		{
			olBarText += CString("/") + prpFlight->Gtd1;
		}

		
		if(blIsGateCfgUsed)
		{
			olBarText = GetBarText(prpFlight, blIsArrival, blIsTurnaround ? prlRotationFlight : NULL);
			olStatusText = (olStatusText.IsEmpty()) ? olBarText + TurnAroundFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		}
		else
		{
			olStatusText = olBarText + TurnAroundFlightStatus(prpFlight, prlRotationFlight);
		}

		rlBar.Text = olBarText;
		//rlBar.StatusText = olBarText + TurnAroundFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StatusText = olStatusText;
		rlBar.StartTime = min(olArrStart,olDepStart);
		rlBar.EndTime = min(olArrEnd,olDepEnd);
		rlBar.IsTmo = prpFlight->IsTmo;
		rlBar.NoDropOnThisBar = FALSE;
		rlBar.DepartureUrno = prlRotationFlight->Urno;
		rlBar.FlightType = GATEBAR_TURNAROUND;
	}
	else	// undisplayed data?
	{
		return;
	}

	if (blIsTurnaround)	// Delay only for Departure
	{
		if (prlRotationFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.Delay = prlRotationFlight->Etod - prlRotationFlight->Stod;
			rlBar.IsDelayed = prlRotationFlight->IsDelayed;
		}
	}
	else if (blIsDeparture)	// Delay only for Departure
	{
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
			rlBar.IsDelayed = prpFlight->IsDelayed;
		}
	}
	else
	{
		rlBar.Delay = CTimeSpan(0);
	}

	// Index in global Color Table
	int ilColorIndex;
	BOOL blIsPlanned;
	BOOL blIsInFuture;
	//bool blIsFinished;

	if (blIsTurnaround)
	{
		rlBar.DepReturnFlightType = ogFlightData.GetRotationReturnFlightType(prpFlight,prlRotationFlight,ipReturnFlightType);
		int ilDepTypeIdx = (rlBar.DepReturnFlightType == ID_DEP_RETURNFLIGHT) ? 1 : 0;

		ilColorIndex = ogConflicts.GetRotationConflictColor(prpFlight,prlRotationFlight,ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,ipReturnFlightType);
		blIsPlanned = prpFlight->PstIsPlanned[ilTypeIdx] || prlRotationFlight->PstIsPlanned[ilDepTypeIdx];
		blIsInFuture = ogFlightData.IsArrival(prpFlight) ? prpFlight->PstIsPlanned[ilTypeIdx] : prlRotationFlight->PstIsPlanned[ilDepTypeIdx];
		//blIsFinished = ogBasicData.IsFinished(prpFlight,ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,ipReturnFlightType) && ogBasicData.IsFinished(prlRotationFlight,ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,rlBar.DepReturnFlightType);
	}
	else
	{
		ilColorIndex = ogConflicts.GetFlightConflictColor(prpFlight, ALLOCUNITTYPE_GATE, PERSONNELDEMANDS, ipReturnFlightType);
		blIsPlanned = prpFlight->PstIsPlanned[ilTypeIdx];
		blIsInFuture = prpFlight->PstIsInFuture[ilTypeIdx];
		//blIsFinished = ogBasicData.IsFinished(prpFlight,ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,ipReturnFlightType);
	}

	rlBar.MarkerType = (blIsPlanned)? MARKLEFT: (blIsInFuture)? MARKNONE: MARKFULL;
	rlBar.IsFinished = (blIsPlanned || blIsInFuture) ? false : true;
	rlBar.MarkerBrush = ogBrushs[ilColorIndex];
	rlBar.FlightUrno = prpFlight->Urno;
	rlBar.FrameType = FRAMERECT;
	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_FLIGHT;
	rlBar.ReturnFlightType = ipReturnFlightType;
	rlBar.FrameColor = bgIsPrm && blIsConfirmedPSM == TRUE ? ogBasicData.GetConfirmedPSMColor() : BLACK;
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// Create indicators associated with this flight
	if (blIsTurnaround)
	{
		MakeIndicators(ipGroupno, ipLineno, ilBarno, olArrivalDemands,olDepartureDemands,prpFlight,prlRotationFlight);
	}
	else if(blIsArrival)
	{
		olDepartureDemands.RemoveAll();
		MakeIndicators(ipGroupno, ipLineno, ilBarno, olArrivalDemands,olDepartureDemands,prpFlight,NULL);
	}
	else if(blIsDeparture)
	{
		olArrivalDemands.RemoveAll();
		MakeIndicators(ipGroupno, ipLineno, ilBarno, olArrivalDemands,olDepartureDemands,NULL,prpFlight);
	}

	return;
}

// a flight is set to finished when it is onblock/airborne and all jobs
// have finished and all unassigned demands are later than the current time
// check if there are unassigned gate demands whose end time has now passed
// thus resulting in the flight bar colour being set to finished
void GateDiagramViewer::CheckForFinishedFlights()
{
	int ilNumGroups = omGroups.GetSize();
	for(int ilG = 0; ilG < ilNumGroups; ilG++)
	{
		int ilNumLines = omGroups[ilG].Lines.GetSize();
		for(int ilL = 0; ilL < ilNumLines; ilL++)
		{
			int ilNumBars = omGroups[ilG].Lines[ilL].Bars.GetSize();
			for(int ilB = 0; ilB < ilNumBars; ilB++)
			{
				GATE_BARDATA *prlBar = &omGroups[ilG].Lines[ilL].Bars[ilB];
			    if(!prlBar->IsFinished && prlBar->EndTime < ogBasicData.GetTime())
				{
					ProcessFlightChange(ogFlightData.GetFlightByUrno(prlBar->FlightUrno));
				}
			}
		}
	}
}

void GateDiagramViewer::MakeShadowBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight)
{
//	if (prpFlight->IsShadoBarDisplayed  == TRUE)
//	{
//		return;
//	}
//
//	prpFlight->IsShadoBarDisplayed = TRUE;
//	bool blIsArrival = ogFlightData.IsArrival(prpFlight);// Inbound flight
//	bool blIsDeparture = ogFlightData.IsDeparture(prpFlight);// Outbound flight
//	CCSPtrArray<DEMANDDATA> olArrivalDemands;
//	CTime olArrivalStartTime, olArrivalEndTime;
//	CCSPtrArray<DEMANDDATA> olDepartureDemands;
//	CTime olDepartureStartTime, olDepartureEndTime;
//	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight);
//	CString olBarText;
//
//
//	// Read the range of time of demands associate with the given flight and its rotation.
//	// Skip this flight if GetDemands() returns no demand or there is an invalid demand
//	// time associated with this flight.
//	//
//	if (blIsArrival)
//	{
//		if (!bmIncludeArrivalFlights)
//		{
//			return;
//		}
//		if (GetDemands(prpFlight->Urno,ogFlightData.GetGate(prpFlight),olArrivalDemands,
//			olArrivalStartTime, olArrivalEndTime) == -1)
//		{
//			MakeShadowBarWithoutDemand(ipGroupno,ipLineno,prpFlight);
//			return;
//		}
//		if (prlRotationFlight != NULL)
//		{
//			if (GetDemands(prlRotationFlight->Urno,ogFlightData.GetGate(prlRotationFlight),olDepartureDemands,
//				olDepartureStartTime, olDepartureEndTime) == -1)
//				prlRotationFlight = NULL;
//		}
//	}
//	else if (blIsDeparture)
//	{
//		if (!bmIncludeDepartureFlights)
//		{
//			return;
//		}
//
//		if (GetDemands(prpFlight->Urno,ogFlightData.GetGate(prpFlight),olDepartureDemands,
//			olDepartureStartTime, olDepartureEndTime) == -1)
//		{
//			MakeShadowBarWithoutDemand(ipGroupno,ipLineno,prpFlight);
//			return;
//		}
//		if (prlRotationFlight != NULL)
//		{
//			if (GetDemands(prlRotationFlight->Urno,ogFlightData.GetGate(prlRotationFlight), olArrivalDemands,
//				olArrivalStartTime, olArrivalEndTime) == -1)
//				prlRotationFlight = NULL;
//		}
//	}
//	else	// Invalid data?
//	{
//		return;
//	}
//
//	// Check for a "turnaround" flight
//	BOOL blIsTurnAround = ogFlightData.IsTurnaround(prpFlight);
//	// Prepare non-common fields associated with this flight bar
//	GATE_BARDATA rlBar;
//	rlBar.IsDelayed = CFST_NOTSET;
//
//	if (bmIncludeArrivalFlights && blIsArrival && !blIsTurnAround)
//	{
//		olBarText = prpFlight->Fnum;
//		rlBar.StatusText = ArrivalFlightStatus(prpFlight, prlRotationFlight);
//		rlBar.StartTime = olArrivalStartTime;
//		rlBar.EndTime = olArrivalEndTime;
//		rlBar.IsTmo = prpFlight->IsTmo;
//	}
//	else if (bmIncludeDepartureFlights && blIsDeparture && !blIsTurnAround)
//	{
//		olBarText = prpFlight->Fnum;
//		rlBar.StatusText = DepartureFlightStatus(prpFlight, prlRotationFlight);
//		rlBar.StartTime = olDepartureStartTime;
//		rlBar.EndTime = olDepartureEndTime;
//		rlBar.IsTmo = FALSE;
//
//	}
//	else if (blIsTurnAround && !blIsDeparture)	// avoid duplicated turnaround
//	{
//		olBarText = CString(prpFlight->Fnum) + " -> " + prlRotationFlight->Fnum;
//		rlBar.Text = CString(prpFlight->Fnum) + " -> " + prlRotationFlight->Fnum;
//		rlBar.StatusText = TurnAroundFlightStatus(prpFlight, prlRotationFlight);
//		rlBar.StartTime = min(olArrivalStartTime,olDepartureStartTime);
//		rlBar.EndTime = olDepartureEndTime;
//		rlBar.IsTmo = prpFlight->IsTmo;
//	}
//	else	// undisplayed data?
//	{
//		return;
//	}
//
//	if (blIsTurnAround)	// Delay only for Departure
//	{
//		if (prlRotationFlight->IsDelayed != CFST_NOTSET)
//		{
//			rlBar.Delay = prlRotationFlight->Etod - prlRotationFlight->Stod;
//			rlBar.IsDelayed = prlRotationFlight->IsDelayed;
//		}
//	}
//	else if (blIsDeparture)	// Delay only for Departure
//	{
//		if (prpFlight->IsDelayed != CFST_NOTSET)
//		{
//			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
//			rlBar.IsDelayed = prpFlight->IsDelayed;
//		}
//	}
//	else
//	{
//		rlBar.Delay = CTimeSpan(0);
//	}
//
//	int ilColorIndex = ogConflicts.GetFlightConflictColor(prpFlight, ALLOCUNITTYPE_GATE, PERSONNELDEMANDS);
//	BOOL blIsPlanned;
//	BOOL blIsInFuture;
//
//	if (blIsTurnAround)
//	{
//		//ilColorIndex = prpFlight->ColorIndex;
//		blIsPlanned = prpFlight->GatIsPlanned || prlRotationFlight->GatIsPlanned;
//		//blIsInFuture = ogFlightData.IsArrival(prpFlight) ? prpFlight->isInFuture : prlRotationFlight->isInFuture;
//		blIsInFuture = ogFlightData.IsArrival(prpFlight) ? prpFlight->GatIsInFuture : prlRotationFlight->GatIsInFuture;
//	}
//	else
//	{
//		//ilColorIndex = prpFlight->ColorIndex;
//		blIsPlanned = prpFlight->GatIsPlanned;
//		//blIsInFuture = prpFlight->isInFuture;
//		blIsInFuture = prpFlight->GatIsInFuture;
//	}
//
//	rlBar.NoDropOnThisBar = TRUE;
//	rlBar.MarkerType = (blIsPlanned)? MARKLEFT: (blIsInFuture)? MARKNONE: MARKFULL;
//	rlBar.MarkerBrush = ogBrushs[ilColorIndex];
//	rlBar.FlightUrno = prpFlight->Urno;
//	rlBar.DepartureUrno = blIsTurnAround ? prlRotationFlight->Urno : -1;
//	rlBar.FrameType = FRAMERECT;
//	rlBar.IsShadowBar = FALSE;
//	rlBar.Type = BAR_FLIGHT;
}



void GateDiagramViewer::MakeBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	if (ogBasicData.bmDemandsOnly)
		return;

	int ilTypeIdx = (ipReturnFlightType == ID_DEP_RETURNFLIGHT) ? 1 : 0;

	bool blIsArrival = (ogFlightData.IsArrival(prpFlight) || ipReturnFlightType == ID_ARR_RETURNFLIGHT);// Inbound flight
	bool blIsDeparture = (ogFlightData.IsDeparture(prpFlight) || ipReturnFlightType == ID_DEP_RETURNFLIGHT);// Outbound flight
	CString olGateChangeText = ogConflicts.GetAdditionalConflictText(CFI_GATECHANGE,prpFlight);
	bool blDisplayGate = (ipGroupno == imAllFlightsGroup) ? true : false;

	BOOL blIsGateCfgUsed;
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "GT_BAR_STATUS_TEXT",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (strcmp(pclTmpText,"NO")==0) 
	{
			blIsGateCfgUsed = FALSE;			
	}
	else if(strcmp(pclTmpText,"YES")==0)
	{
			blIsGateCfgUsed = TRUE;
	}


	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight, ipReturnFlightType);

	CString olBarText;
	CString olStatusText = GetStatusText(prpFlight, NULL);

	GATE_BARDATA rlBar;
	rlBar.IsDelayed = CFST_NOTSET;
	if (blIsArrival)
	{
		if (prpFlight->Stoa == TIMENULL)
			return;
		if(blDisplayGate && strlen(prpFlight->Gta1) > 0)
		{
			olBarText = prpFlight->Gta1 + CString("/");
		}
		olBarText += CString(prpFlight->Fnum) + " " + olGateChangeText;

		if(blIsGateCfgUsed)
		{
			olBarText = GetBarText(prpFlight, blIsArrival, NULL);
			olStatusText = (olStatusText.IsEmpty()) ? olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		}
		else
		{
			olStatusText = olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight);
		}

		rlBar.Text = olBarText;
		//rlBar.StatusText = olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StatusText = olStatusText;

		rlBar.StartTime = GetFlightArr(prpFlight, ipReturnFlightType);
		rlBar.EndTime = GetFlightDep(prpFlight, ipReturnFlightType);
		rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = prpFlight->IsTmo;
		rlBar.FlightType = GATEBAR_ARRIVAL;
	}
	else if (blIsDeparture)
	{
		if (prpFlight->Stod == TIMENULL)
			return;
		olBarText = CString(prpFlight->Fnum) + " " + olGateChangeText;
		if(blDisplayGate && strlen(prpFlight->Gtd1) > 0)
		{
			olBarText += CString("/") + prpFlight->Gtd1;
		}

		if(blIsGateCfgUsed)
		{
			olBarText = GetBarText(prpFlight, blIsArrival, NULL);
			olStatusText = (olStatusText.IsEmpty()) ? olBarText + DepartureFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		}
		else
		{
			olStatusText = olBarText + DepartureFlightStatus(prpFlight, prlRotationFlight);
		}

		rlBar.Text = olBarText;
		//rlBar.StatusText = olBarText + DepartureFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StatusText = olStatusText;

		rlBar.StartTime = GetFlightArr(prpFlight, ipReturnFlightType);
		rlBar.EndTime = GetFlightDep(prpFlight, ipReturnFlightType);
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.IsDelayed = prpFlight->IsDelayed;
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
		}
		else
			rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = FALSE;
		rlBar.FlightType = GATEBAR_DEPARTURE;
	}

	rlBar.MarkerType = prpFlight->GatIsPlanned[ilTypeIdx] ? MARKLEFT : prpFlight->GatIsInFuture[ilTypeIdx] ? MARKNONE : MARKFULL;

	rlBar.FlightUrno = prpFlight->Urno;
//	rlBar.DepartureUrno = prpFlight->Rkey;
	rlBar.FrameType = FRAMERECT;
	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_FLIGHT;
	rlBar.NoDropOnThisBar = FALSE;
	rlBar.MarkerBrush = ogBrushs[ogConflicts.GetFlightConflictColor(prpFlight, ALLOCUNITTYPE_GATE, PERSONNELDEMANDS, ipReturnFlightType)];
	rlBar.ReturnFlightType = ipReturnFlightType;
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// Create indicators associated with this flight
	// not here, no demand, no indicator
}


void GateDiagramViewer::MakeShadowBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight)
{
	if (ogBasicData.bmDemandsOnly)
		return;
	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight);

	bool blIsArrival = ogFlightData.IsArrival(prpFlight);		// Inbound flight
	bool blIsDeparture = ogFlightData.IsDeparture(prpFlight);	// Outbound flight

	GATE_BARDATA rlBar;
	rlBar.IsDelayed = CFST_NOTSET;
	if (blIsArrival)
	{
		if (prpFlight->Stoa == TIMENULL)
			return;
		rlBar.Text = prpFlight->Fnum;
		rlBar.StatusText = ArrivalFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = GetFlightArr(prpFlight);
		rlBar.EndTime = GetFlightDep(prpFlight);
		rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = prpFlight->IsTmo;
	}
	else if (blIsDeparture)
	{
		if (prpFlight->Stod == TIMENULL)
			return;
		rlBar.Text = prpFlight->Fnum;
		rlBar.StatusText = DepartureFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = GetFlightArr(prpFlight);
		rlBar.EndTime = GetFlightDep(prpFlight);
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.IsDelayed = prpFlight->IsDelayed;
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
		}
		else
			rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = FALSE;
	}
	rlBar.MarkerType = MARKFULL;
	rlBar.MarkerBrush = ogBrushs[6];

	rlBar.FlightUrno = prpFlight->Urno;
//	rlBar.DepartureUrno = prpFlight->Rkey;
	rlBar.FrameType = FRAMERECT;
	rlBar.NoDropOnThisBar = TRUE;
	rlBar.IsShadowBar = TRUE;
	rlBar.Type = BAR_FLIGHT;
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// Create indicators associated with this flight
	// not here, no demand, no indicator
}



void GateDiagramViewer::MakeIndicators(int ipGroupno, int ipLineno, int ipBarno, 
	CCSPtrArray<DEMANDDATA> &ropArrivalDemands,
	CCSPtrArray<DEMANDDATA> &ropDepartureDemands,
	FLIGHTDATA *prpArrivalFlight,FLIGHTDATA *prpDepartureFlight)
{
	int i;
	long llJobu;
	CCSPtrArray<JOBDATA> olJobs;
	CCSPtrArray<DEMANDDATA> olDemands;

	for (i = 0; i < ropArrivalDemands.GetSize(); i++)	// for each arrival flight?
	{
//		if (strcmp(ogFlightData.GetGate(prpArrivalFlight),ropArrivalDemands[i].Alid) == 0)
		{
			GATE_INDICATORDATA rlIndicator;
			rlIndicator.StartTime = ropArrivalDemands[i].Debe;
			rlIndicator.EndTime = ropArrivalDemands[i].Deen;
			rlIndicator.IsArrival = TRUE;
			// MCU 05.07 we need the Urno of the demand for the job/demand connection
			rlIndicator.DemandUrno = ropArrivalDemands[i].Urno;
			rlIndicator.Color = 0;
			// MCU 07.07 the Indicator Color depends on the Color of the job
			// assigned to this demand, if no job is assigned, the color comes from
			// conflict CFI_NOTCOVERED
			if ((llJobu = ogJodData.GetFirstJobUrnoByDemand(ropArrivalDemands[i].Urno)) != 0)
			{
				JOBDATA *prlJob = ogJobData.GetJobByUrno(llJobu);
				if (prlJob != NULL)
				{
					//rlIndicator.Color = ogColors[prlJob->ColorIndex];
					rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
					rlIndicator.StartTime = prlJob->Acfr;
					rlIndicator.EndTime = prlJob->Acto;
				}
			}
			if (rlIndicator.Color == 0)
			{
				if(ogDemandData.DemandIsDeactivated(&ropArrivalDemands[i]))
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2)];
				}
				else
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_GAT_NOTCOVERED*2)];	// just use simply red for now
					if(bgIsPrm)
					{
						rlIndicator.Color  = ogDpxData.IsConfirmedPSM(ropArrivalDemands[i].Uprm) == TRUE ? ogBasicData.GetConfirmedPSMColor() : rlIndicator.Color;
					}
				}
			}
			CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
		}
	}
	if (prpArrivalFlight != NULL)
	{
		// now we create indicators for jobs without demand for this flight
		olJobs.RemoveAll();
		ogJobData.GetJobsByFlur(olJobs,prpArrivalFlight->Urno,FALSE,"",ALLOCUNITTYPE_GATE); // don't like JOBFM
		for ( i = 0; i < olJobs.GetSize(); i++)
		{
			JOBDATA *prlJob = &olJobs[i];
			if(ogJodData.JobHasDemands(prlJob))
			{
				GATE_INDICATORDATA rlIndicator;
				rlIndicator.StartTime = prlJob->Acfr;
				rlIndicator.EndTime = prlJob->Acto;
				rlIndicator.IsArrival = TRUE;
				rlIndicator.DemandUrno = 0L; // no demand
				//rlIndicator.Color = ogColors[prlJob->ColorIndex];
				rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
				CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
			}
		}
	}
	for (i = 0; i < ropDepartureDemands.GetSize(); i++)	// for each departure flight?
	{
		CString olTest(ropDepartureDemands[i].Alid);
//		if (strcmp(ogFlightData.GetGate(prpDepartureFlight),ropDepartureDemands[i].Alid) == 0)
		{
			GATE_INDICATORDATA rlIndicator;
			rlIndicator.StartTime = ropDepartureDemands[i].Debe;
			rlIndicator.EndTime = ropDepartureDemands[i].Deen;
			rlIndicator.IsArrival = FALSE;
			// MCU 05.07 we need the Urno of the demand for the job/demand connection
			rlIndicator.DemandUrno = ropDepartureDemands[i].Urno;
			rlIndicator.Color = 0;
			// MCU 07.07 the Indicator Color depends on the Color of the job
			// assigned to this demand, if no job is assigned, the color comes from
			// conflict CFI_NOTCOVERED
			if ((llJobu = ogJodData.GetFirstJobUrnoByDemand(ropDepartureDemands[i].Urno)) != 0)
			{
				JOBDATA *prlJob = ogJobData.GetJobByUrno(llJobu);
				if (prlJob != NULL)
				{
					//rlIndicator.Color = ogColors[prlJob->ColorIndex];
					rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
					rlIndicator.StartTime = prlJob->Acfr;
					rlIndicator.EndTime = prlJob->Acto;
				}
			}
			if (rlIndicator.Color == 0)
			{
				if(ogDemandData.DemandIsDeactivated(&ropDepartureDemands[i]))
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2)];
				}
				else
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_GAT_NOTCOVERED*2)];	// just use simply red for now
					if(bgIsPrm)
					{
						rlIndicator.Color  = ogDpxData.IsConfirmedPSM(ropDepartureDemands[i].Uprm) == TRUE ? ogBasicData.GetConfirmedPSMColor() : rlIndicator.Color;
					}
				}
			}
			CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
		}
	}
	if (prpDepartureFlight != NULL)
	{
		// now we create indicators for jobs without demand for this flight
		olJobs.RemoveAll();
		ogJobData.GetJobsByFlur(olJobs,prpDepartureFlight->Urno,FALSE,"",ALLOCUNITTYPE_GATE); // don't like JOBFM
		for ( i = 0; i < olJobs.GetSize(); i++)
		{
			JOBDATA *prlJob = &olJobs[i];
			if(ogJodData.JobHasDemands(prlJob))
			{
				GATE_INDICATORDATA rlIndicator;
				rlIndicator.StartTime = prlJob->Acfr;
				rlIndicator.EndTime = prlJob->Acto;
				rlIndicator.IsArrival = TRUE;
				rlIndicator.DemandUrno = 0L; // no demand
				//rlIndicator.Color = ogColors[prlJob->ColorIndex];
				rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
				CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
			}
		}
	}
}

void GateDiagramViewer::MakeSpecialBar(int ipGroupno, int ipLineno, JOBDATA *prpJob)
{
	GATE_BARDATA rlBar;
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);

	if (prlEmp != NULL)
	{
		CString olEmpNam = ogEmpData.GetEmpName(prlEmp);
		rlBar.Text = olEmpNam;
		rlBar.StatusText = prpJob->Acfr.Format("%H%M") + "-" + prpJob->Acto.Format("%H%M")
			+ olEmpNam; 
	}
	SHIFTDATA *prlShift;
	JOBDATA *prlDutyJob = ogJobData.GetJobByUrno(prpJob->Jour);
	if (prlDutyJob != NULL)
	{
		prlShift = ogShiftData.GetShiftByUrno(prlDutyJob->Shur);
		if (prlShift != NULL)
		{
			rlBar.StatusText += CString(prlShift->Sfca) + " " + prlShift->Egrp;
		}
	}
	rlBar.StartTime = prpJob->Acfr;
	rlBar.EndTime = prpJob->Acto;
	rlBar.MarkerType = (prpJob->Stat[0] == 'P')? MARKLEFT: MARKFULL;
	//rlBar.MarkerBrush = ogBrushs[prpJob->ColorIndex];
	rlBar.MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColor(prpJob)];
	rlBar.FlightUrno = prpJob->Urno;
//	rlBar.DepartureUrno = -1;
	rlBar.FrameType = FRAMERECT;
	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_SPECIAL;
	rlBar.IsTmo = FALSE;
	rlBar.IsDelayed = FALSE;
	rlBar.Delay = CTimeSpan(0);

	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// no Indicators
}

int GateDiagramViewer::GetDemands(long lpFlightUrno, const char *pcpAlid,
								  CCSPtrArray<DEMANDDATA> &ropDemands,
								  CTime &rolStartTime, CTime &rolEndTime)
{
	rolStartTime = TIMENULL;	// set default values
	rolEndTime = TIMENULL;

	ogDemandData.GetDemandsByFlur(ropDemands,lpFlightUrno,pcpAlid,ALLOCUNITTYPE_GATE,PERSONNELDEMANDS);
	int ilDemandCount = ropDemands.GetSize();
	if (ilDemandCount == 0)	// no demand?
		return -1;

	for (int i = 0; i < ilDemandCount; i++)		// scan for the earliest and latest shift
	{
		if (rolStartTime == TIMENULL || rolStartTime > (ropDemands[i].Debe))
			rolStartTime = ropDemands[i].Debe;
		if (rolEndTime == TIMENULL || rolEndTime < (ropDemands[i].Deen))
			rolEndTime = ropDemands[i].Deen;
	}

	if (rolStartTime == TIMENULL || rolEndTime == TIMENULL)
		return -1;
	return 0;	// success
}

CString GateDiagramViewer::ArrivalFlightStatus(FLIGHTDATA *prpFlight,FLIGHTDATA *prpRotationFlight)
{
	CString olText;
	if(prpFlight != NULL)
	{
		CString olEtoa = prpFlight->Etoa != TIMENULL ? CString("ETA ") + prpFlight->Etoa.Format("%H%M") : "";
		CString olOnbl = prpFlight->Onbl != TIMENULL ? CString("ONBL ") + prpFlight->Onbl.Format("%H%M") : "";
		olText.Format("   %s/%s  %s  STA %s %s %s   P:%s G:%s   PAX:%d",
			prpFlight->Regn, prpFlight->Act3,prpFlight->Org3,
			prpFlight->Stoa.Format("%H%M"),olEtoa,olOnbl,
			prpFlight->Psta, ogFlightData.GetGate(prpFlight), prpFlight->Pax1);
	}
	
	if(prpRotationFlight != NULL)
	{
		CString olEtod = prpRotationFlight->Etod != TIMENULL ? CString("ETD ") + prpRotationFlight->Etod.Format("%H%M") : "";
		CString olOfbl = prpRotationFlight->Ofbl != TIMENULL ? CString("OFBL ") + prpRotationFlight->Ofbl.Format("%H%M") : "";
		CString olRotText;
		olRotText.Format("   (%s  STD %s %s %s  P:%s G:%s)",
			prpRotationFlight->Fnum,prpRotationFlight->Stod.Format("%H%M"),olEtod,olOfbl,
			prpRotationFlight->Pstd, ogFlightData.GetGate(prpRotationFlight));
		olText += olRotText;
	}

	return olText;
}

CString GateDiagramViewer::DepartureFlightStatus(FLIGHTDATA *prpFlight,FLIGHTDATA *prpRotationFlight)
{
	CString olText;
	if(prpFlight != NULL)
	{
		CString olEtod = prpFlight->Etod != TIMENULL ? CString("ETD ") + prpFlight->Etod.Format("%H%M") : "";
		CString olOfbl = prpFlight->Ofbl != TIMENULL ? CString("OFBL ") + prpFlight->Ofbl.Format("%H%M") : "";
		olText.Format("   %s/%s  %s %s  STD %s %s %s   P:%s  G:%s   PAX:%s",
			prpFlight->Regn, prpFlight->Act3,
			prpFlight->Des3, prpFlight->Via3,
			prpFlight->Stod.Format("%H%M"),olEtod,olOfbl,
			prpFlight->Pstd, ogFlightData.GetGate(prpFlight),
			ogPaxData.GetPaxString(prpFlight->Urno));
	}
	if(prpRotationFlight != NULL)
	{
		CString olEtoa = prpRotationFlight->Etoa != TIMENULL ? CString("ETA ") + prpRotationFlight->Etoa.Format("%H%M") : "";
		CString olOnbl = prpRotationFlight->Onbl != TIMENULL ? CString("ONBL ") + prpRotationFlight->Onbl.Format("%H%M") : "";
		CString olRotText;
		olRotText.Format("   (%s  STA %s %s %s  P:%s G:%s)",
			prpRotationFlight->Fnum,prpRotationFlight->Stoa.Format("%H%M"),olEtoa,olOnbl,
			prpRotationFlight->Psta, ogFlightData.GetGate(prpRotationFlight));
		olText += olRotText;
	}

	return olText;
}

CString GateDiagramViewer::TurnAroundFlightStatus(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotationFlight)
{
	CString olStatus;
	FLIGHTDATA *prlArr, *prlDep;
	if(ogFlightData.IsArrival(prpFlight))
	{
		prlArr = prpFlight;
		prlDep = prpRotationFlight;
	}
	else
	{
		prlArr = prpRotationFlight;
		prlDep = prpFlight;
	}
	olStatus.Format("%s  -->  %s", ArrivalFlightStatus(prlArr,NULL), DepartureFlightStatus(prlDep,NULL));
	return olStatus;
}

BOOL GateDiagramViewer::FindGroup(const char *pcpGateAreaId, int &ripGroupno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		if (GetGroup(ripGroupno)->GateAreaId == pcpGateAreaId)
			return TRUE;

	return FALSE;
}

// this new version of FindGroupAndLine allows iteration because it is now
// possible to have a flight displayed more than once ie if a gate appears
// in more than one group eg "A01-A03" and "A01-A10". For this function to
// iterate properly, ripGroupno and ropLineno need to be initialized to -1
// - subsequent calls will then increment ripLineno.
BOOL GateDiagramViewer::FindGroupAndLine(const char *pcpGateId, int &ripGroupno, int &ripLineno)
{
	if(ripGroupno == -1)
		ripGroupno = 0;
	if(ripLineno == -1)
		ripLineno = 0;
	else
		ripLineno++;

	int ilNumGroups = GetGroupCount();
	for(; ripGroupno < ilNumGroups; ripGroupno++)
	{
		int ilNumLines = GetLineCount(ripGroupno);
		for(; ripLineno < ilNumLines; ripLineno++)
		{
			CString olLineText = GetLineText( ripGroupno, ripLineno);
			if(olLineText  == pcpGateId)
			{
				return TRUE;
			}
		}
		ripLineno = 0;
	}

    return FALSE;   // there is no such flight
}

BOOL GateDiagramViewer::FindLine(const char *pcpGateId, int ipGroupno, int &ripLineno)
{
	int ilNumLines = GetLineCount(ipGroupno);
	for(ripLineno = 0; ripLineno < ilNumLines; ripLineno++)
	{
		if(GetLineText(ipGroupno, ripLineno) == pcpGateId)
		{
			return TRUE;
		}
	}
	ripLineno = 0;
    return FALSE;
}




//BOOL GateDiagramViewer::FindGroupAndLine(const char *pcpGateId, int &ripGroupno, int &ripLineno)
//{
//	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
//		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
//               if ( GetLineText( ripGroupno, ripLineno) == pcpGateId)
//                    return TRUE;
//
//    return FALSE;   // there is no such flight
//}

BOOL GateDiagramViewer::FindManager(long lpUrno, int &ripGroupno, int &ripManagerno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripManagerno = 0; ripManagerno < GetManagerCount(ripGroupno); ripManagerno++)
			if (GetManager(ripGroupno, ripManagerno)->JobUrno == lpUrno)
				return TRUE;

	return FALSE;
}

BOOL GateDiagramViewer::FindFlightBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno, bool bpFindRotation /* = false */, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	GATE_BARDATA *prlBar;
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
            for (ripBarno = 0; ripBarno < GetBarCount(ripGroupno, ripLineno); ripBarno++)
			{
				prlBar = GetBar(ripGroupno, ripLineno, ripBarno);
				if(ipReturnFlightType == ID_NOT_RETURNFLIGHT)
				{
					if(prlBar->FlightUrno == lpUrno || (bpFindRotation && prlBar->DepartureUrno == lpUrno))
						return TRUE;
				}
				else
				{
					if((prlBar->FlightUrno == lpUrno && prlBar->ReturnFlightType == ipReturnFlightType) || 
						(bpFindRotation && prlBar->DepartureUrno == lpUrno && prlBar->DepReturnFlightType == ipReturnFlightType))
						return TRUE;
				}
			}
		}
	}

    return FALSE;   // there is no such flight
}


/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer -- Viewer basic operations

int GateDiagramViewer::GetGroupCount()
{
    return omGroups.GetSize();
}

GATE_GROUPDATA *GateDiagramViewer::GetGroup(int ipGroupno)
{
    return &omGroups[ipGroupno];
}

CString GateDiagramViewer::GetGroupText(int ipGroupno)
{
    return omGroups[ipGroupno].Text;
}

CString GateDiagramViewer::GetGroupTopScaleText(int ipGroupno)
{
	CString s;
	for (int i = 0; i < GetManagerCount(ipGroupno); i++)
	{
		GATE_MANAGERDATA *prlManager = GetManager(ipGroupno, i);
		s += s.IsEmpty()? "": " / ";
		s += CString(prlManager->EmpLnam) + " " + prlManager->ShiftTmid + " " + prlManager->ShiftStid;
	}
	return s;
}

int GateDiagramViewer::GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd)
{
	int ilColorIndex = FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
	CBrush *prlBrush = ogBrushs[ilColorIndex];
	int ilLineCount = GetLineCount(ipGroupno);
	int ilColour, ilType;
	bool blConfirmed;
	for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ilLineno); ilBarno++)
		{
			GATE_BARDATA *prlBar;
			if ((prlBar = GetBar(ipGroupno, ilLineno, ilBarno)) != NULL)
			{
				if (IsOverlapped(opStart, opEnd, prlBar->StartTime, prlBar->EndTime))
				{
					FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
					if (prlFlight != NULL)
					{
						ogConflicts.GetFlightConflict(prlFlight, ilColour, ilType, blConfirmed, ALLOCUNITTYPE_GATE, PERSONNELDEMANDS);
						if(ilType != CFI_NOCONFLICT && !blConfirmed)
						{
							// valid conflict found
							return FIRSTCONFLICTCOLOR+(CFI_GAT_NOTCOVERED*2);
						}
					}
				}
			}
		}
	}
	return ilColorIndex;
}

int GateDiagramViewer::GetManagerCount(int ipGroupno)
{
    return omGroups[ipGroupno].Managers.GetSize();
}

GATE_MANAGERDATA *GateDiagramViewer::GetManager(int ipGroupno, int ipManagerno)
{
    return &omGroups[ipGroupno].Managers[ipManagerno];
}

int GateDiagramViewer::GetLineCount(int ipGroupno)
{
    return omGroups[ipGroupno].Lines.GetSize();
}

GATE_LINEDATA *GateDiagramViewer::GetLine(int ipGroupno, int ipLineno)
{
    return &omGroups[ipGroupno].Lines[ipLineno];
}

CString GateDiagramViewer::GetLineText(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Text;
}

int GateDiagramViewer::GetMaxOverlapLevel(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].MaxOverlapLevel;
}

int GateDiagramViewer::GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipGroupno, ipLineno) / 3 * 3 + 2;
}

int GateDiagramViewer::GetBarCount(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize();
}

GATE_BARDATA *GateDiagramViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno];
}

CString GateDiagramViewer::GetBarText(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Text;
}

CString GateDiagramViewer::GetBarText(FLIGHTDATA *prpFlight, bool bpIsArrival, FLIGHTDATA *prpRotation /*=NULL*/)
{
	CString olBarText;

	FLIGHTDATA *prlArr = (bpIsArrival) ? prpFlight : prpRotation;
	FLIGHTDATA *prlDep = (bpIsArrival) ? prpRotation : prpFlight;
	if(prlArr && prlDep)
	{
		olBarText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_GTBARTURN, "%s%s%s%s%s%s%s%s%d%s%s%s%s%s%s%d%s%s", 
												prlArr->Regn, prlArr->Act3, prlArr->Fnum, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1, 
												prlDep->Fnum, prlDep->Des3, prlDep->Stod.Format("%H%M"), prlDep->Etod.Format("%H%M"), 
												GetGateText(prlDep), GetPositionText(prlDep),prlDep->Pax1,
												prlArr->Flti, prlDep->Flti);
	}
	else if(prlArr)
	{
		olBarText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_GTBARARR, "%s%s%s%s%s%s%s%s%d%s", 
												prlArr->Fnum, prlArr->Regn, prlArr->Act3, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1,prlArr->Flti);
	}
	else if(prlDep)
	{
		olBarText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_GTBARDEP, "%s%s%s%s%s%s%s%s%d%s", 
												prlDep->Fnum, prlDep->Regn, prlDep->Act3, prlDep->Des3, prlDep->Stod.Format("%H%M"), 
												prlDep->Etod.Format("%H%M"), GetGateText(prlDep), GetPositionText(prlDep), prlDep->Pax1, prlDep->Flti);
	}

	if(olBarText.IsEmpty())
	{
		CString olSlash = "/", olDash = " - ", olBlank = " ";
		CString olPstChangeText1 = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpFlight), olPstChangeText2;
		if(prpRotation != NULL)
			olPstChangeText2 = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpRotation);

		if(bpIsArrival)
		{
			if(!olPstChangeText1.IsEmpty())
				olBarText += olPstChangeText1 + olSlash;
			else if(strlen(prpFlight->Psta) > 0)
				olBarText += prpFlight->Psta + olSlash;

			if(strlen(prpFlight->Gta1) > 0)
				olBarText += prpFlight->Gta1 + olSlash;

			olBarText += prpFlight->Fnum;
			if(prpRotation != NULL)
			{
				olBarText += olDash + prpRotation->Fnum;

				if(strlen(prpRotation->Gtd1) > 0)
					olBarText += olSlash + prpRotation->Gtd1;

				if(!olPstChangeText2.IsEmpty())
					olBarText += olSlash + olPstChangeText2;
				else if(strlen(prpRotation->Pstd) > 0)
					olBarText += olSlash + prpRotation->Pstd;
			}
		}
		else
		{
			if(prpRotation != NULL)
			{
				if(!olPstChangeText2.IsEmpty())
					olBarText += olPstChangeText2 + olSlash;
				else if(strlen(prpRotation->Psta) > 0)
					olBarText += prpRotation->Psta + olSlash;

				if(strlen(prpRotation->Gta1) > 0)
					olBarText += prpRotation->Gta1 + olSlash;

				olBarText += prpRotation->Fnum + olDash;
			}
			olBarText += prpFlight->Fnum;

			if(strlen(prpFlight->Gtd1) > 0)
				olBarText += olSlash + prpFlight->Gtd1;

			if(!olPstChangeText1.IsEmpty())
				olBarText += olSlash + olPstChangeText1;
			else if(strlen(prpFlight->Pstd) > 0)
				olBarText += olSlash + prpFlight->Pstd;
		}
	}

	return olBarText;
}

CString GateDiagramViewer::GetStatusText(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotation)
{
	CString olText;
	bool blIsArrival = ogFlightData.IsArrival(prpFlight);
	FLIGHTDATA *prlArr = (blIsArrival) ? prpFlight : prpRotation;
	FLIGHTDATA *prlDep = (blIsArrival) ? prpRotation : prpFlight;
	if(prlArr && prlDep)
	{
		olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_GTSTATUSTURN, "%s%s%s%s%s%s%s%s%d%s%s%s%s%s%s%d%s%s", 
												prlArr->Regn, prlArr->Act3, prlArr->Fnum, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1,
												prlDep->Fnum, prlDep->Des3, prlDep->Stod.Format("%H%M"), prlDep->Etod.Format("%H%M"), 
												GetGateText(prlDep), GetPositionText(prlDep), prlDep->Pax1,
												prlArr->Flti, prlDep->Flti);
	}
	else if(prlArr)
	{
		olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_GTSTATUSARR, "%s%s%s%s%s%s%s%s%d%s", 
											    prlArr->Fnum, prlArr->Regn, prlArr->Act3, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1,prlArr->Flti);
	}
	else if(prlDep)
	{
		olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_GTSTATUSDEP, "%s%s%s%s%s%s%s%s%d%s", 
												prlDep->Fnum, prlDep->Regn, prlDep->Act3, prlDep->Des3, prlDep->Stod.Format("%H%M"), 
												prlDep->Etod.Format("%H%M"), GetGateText(prlDep), GetPositionText(prlDep), prlDep->Pax1, prlDep->Flti);
	}


	return olText;
}


CString GateDiagramViewer::GetPositionText(FLIGHTDATA *prpFlight)
{
	CString olPstText = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpFlight);
	if(olPstText.IsEmpty()) olPstText = ogFlightData.GetPosition(prpFlight);
	return olPstText;
}

CString GateDiagramViewer::GetGateText(FLIGHTDATA *prpFlight)
{
	CString olGateText = ogConflicts.GetAdditionalConflictText(CFI_GATECHANGE,prpFlight);
	if(olGateText.IsEmpty()) olGateText = ogFlightData.GetGate(prpFlight);
	return olGateText;
}


int GateDiagramViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();
}

GATE_INDICATORDATA *GateDiagramViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators[ipIndicatorno];
}

CString GateDiagramViewer::GetTmo(int ipGroupno, int ipLineno)
{
	CString olTmoText = CString("");
    for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ipLineno); ilBarno++)
	{
		GATE_BARDATA *prlBar;
        if ((prlBar = GetBar(ipGroupno, ipLineno, ilBarno)) != NULL)
		{
			if (prlBar->IsTmo)
			{
				if (olTmoText.IsEmpty())
					olTmoText = CString(" TMO ");
				FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
				if (prlFlight != NULL)
				{
					if (ogFlightData.IsDeparture(prlFlight))
					{
						prlFlight = ogFlightData.GetRotationFlight(prlFlight);
					}
					if (prlFlight != NULL)
					{
						olTmoText += CString(prlFlight->Fnum) + " ";
					}
				}
			}
		}
	}
    return olTmoText;   
}

/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer - GATE_BARDATA array maintenance (overlapping version)

void GateDiagramViewer::DeleteAll()
{
    while (GetGroupCount() > 0)
        DeleteGroup(0);
}

int GateDiagramViewer::CreateGroup(GATE_GROUPDATA *prpGroup)
{
    int ilGroupCount = omGroups.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno)) <= 0)
            break;  // should be inserted before Groups[ilGroupno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = ilGroupCount; ilGroupno > 0; ilGroupno--)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno-1)) >= 0)
            break;  // should be inserted after Groups[ilGroupno-1]
#endif
    
    omGroups.NewAt(ilGroupno, *prpGroup);
    return ilGroupno;
}

void GateDiagramViewer::DeleteGroup(int ipGroupno)
{
    while (GetLineCount(ipGroupno) > 0)
        DeleteLine(ipGroupno, 0);
    while (GetManagerCount(ipGroupno) > 0)
        DeleteManager(ipGroupno, 0);

    omGroups.DeleteAt(ipGroupno);
}

int GateDiagramViewer::CreateManager(int ipGroupno, GATE_MANAGERDATA *prpManager)
{
    int ilManagerCount = omGroups[ipGroupno].Managers.GetSize();
// first we check if this manager already exist
	for ( int ilLc = 0; ilLc < ilManagerCount; ilLc++)
	{
		if (omGroups[ipGroupno].Managers[ilLc].JobUrno == prpManager->JobUrno)
			return ilLc;
	}

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = 0; ilManagerno < ilManagerCount; ilManagerno++)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno)) <= 0)
            break;  // should be inserted before Lines[ilManagerno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = ilManagerCount; ilManagerno > 0; ilManagerno--)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno-1)) >= 0)
            break;  // should be inserted after Lines[ilManagerno-1]
#endif

    omGroups[ipGroupno].Managers.NewAt(ilManagerno, *prpManager);
    GATE_MANAGERDATA *prlManager = &omGroups[ipGroupno].Managers[ilManagerno];
    return ilManagerno;
}

void GateDiagramViewer::DeleteManager(int ipGroupno, int ipManagerno)
{
    omGroups[ipGroupno].Managers.DeleteAt(ipManagerno);
}

int GateDiagramViewer::CreateLine(int ipGroupno, GATE_LINEDATA *prpLine)
{
    int ilLineCount = omGroups[ipGroupno].Lines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno)) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno-1)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    omGroups[ipGroupno].Lines.NewAt(ilLineno, *prpLine);
    GATE_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLineno;
}

void GateDiagramViewer::DeleteLine(int ipGroupno, int ipLineno)
{
	// Id 14-Sep-1996
	// Don't simply call DeleteBar() for sake of efficiency
	GATE_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ipLineno];
	while (prlLine->Bars.GetSize() > 0)
	{
		prlLine->Bars[0].Indicators.DeleteAll();	// delete associated indicators first
		prlLine->Bars.DeleteAt(0);
	}

    omGroups[ipGroupno].Lines.DeleteAt(ipLineno);
}

// When Advise time is set the start of the bar is set to TIFA and the end to STOA+20mins this can result
// in a negative duration which in turn results in multiple bars being displayed on one line in the "All Flights" group
// check for negative duration and reset the bar time if necessary
void GateDiagramViewer::CheckBarDuration(GATE_BARDATA &ropBar)
{
	CTimeSpan olBarLength = ropBar.EndTime - ropBar.StartTime;
	if(olBarLength.GetTotalMinutes() <= 0)
	{
		if(ropBar.FlightType != GATEBAR_DEPARTURE)
			ropBar.EndTime = ropBar.StartTime + CTimeSpan(0,0,20,0);
		else
			ropBar.StartTime = ropBar.EndTime - CTimeSpan(0,1,0,0);
	}
}

int GateDiagramViewer::CreateBar(int ipGroupno, int ipLineno, GATE_BARDATA *prpBar, BOOL bpTopBar)
{
	CheckBarDuration(*prpBar);

    GATE_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prpBar->StartTime, prpBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			GATE_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			GATE_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void GateDiagramViewer::DeleteBarsForFlight(long lpFlightUrno)
{
	int ilGroupno, ilLineno, ilBarno;
	while(FindFlightBar(lpFlightUrno, ilGroupno, ilLineno, ilBarno))
	{
		GATE_BARDATA *prlBar;
		if((prlBar = GetBar(ilGroupno, ilLineno, ilBarno)) != NULL)
		{
			int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
			DeleteBar(ilGroupno, ilLineno, ilBarno);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLineno, ilGroupno);
				if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				else
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
			}
		}
	}
}


void GateDiagramViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
    GATE_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
	GATE_BARDATA *prlBar = &prlLine->Bars[ipBarno];

	// Delete all indicators of this bar
	while (GetIndicatorCount(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteIndicator(ipGroupno, ipLineno, ipBarno, 0);

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prlBar->StartTime, prlBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<GATE_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		GATE_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipGroupno, ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			GATE_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}

int GateDiagramViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, GATE_INDICATORDATA *prpIndicator)
{
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();

    // Search for the position which we want to insert this new indicator
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
	{
		GATE_INDICATORDATA *prlTmpIndicator = GetIndicator(ipGroupno,ipLineno,ipBarno,ilIndicatorno);
		if (prlTmpIndicator == NULL)
			break; // should not happen

		// Trying to place the indicator in the correct position.
		// I don't like this block of code, but it works fine.
		// Somebody should help me revise it.
		// -- Id, 29-Sep-96
		//
		if (prpIndicator->DemandUrno != 0)	// new indicator has a demand
		{
			if (prlTmpIndicator->DemandUrno == 0)
				break;	// should be inseted before a job without demand

			// the job in the buffer has some demand, so we use the old algorithm
			if (prpIndicator->IsArrival)	// new indicator is for arrival demand
			{
				if (!prlTmpIndicator->IsArrival)
					break;	// should be inserted before indicator for departure demand
				if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->DemandUrno < prpIndicator->DemandUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
			}
			else	// new indicator is for departure demand
			{
				if (prlTmpIndicator->IsArrival)
					continue;	// bar in buffer is for arrival demand, step to the next one
				if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->DemandUrno < prpIndicator->DemandUrno)
						break;	// should place indicator in demand URNO order
				}
			}
		}
		else	// new indicator has no demand
		{
			if (prlTmpIndicator->DemandUrno != 0)
				continue;	// bar in buffer has demand, step to the next one
			if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
				break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->JobUrno < prpIndicator->JobUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
		}
	}

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
}

void GateDiagramViewer::DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.DeleteAt(ipIndicatorno);
}


/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer - GATE_BARDATA calculation and searching routines

// Return the bar number of the list box based on the given period [time1, time2]
int GateDiagramViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        GATE_BARDATA *prlBar = GetBar(ipGroupno, ipLineno, i);
        if (opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}


/////////////////////////////////////////////////////////////////////////////
// GateDiagramViewer private helper methods

void GateDiagramViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    GATE_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].StartTime;
			CTime olEndTime = prlLine->Bars[ilBarno].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].StartTime);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}

void GateDiagramViewer::AllowUpdates(BOOL bpNoUpdatesNow)
{
	bmNoUpdatesNow = bpNoUpdatesNow;
}



///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines

void GateDiagramViewer::PrintGateDiagramHeader(int ipGroupno)
{
	CString olGateAreaName = GetGroupText(ipGroupno);
	CString omTarget;
//	omTarget.Format("%s  von: %s  bis: %s",olGateAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	omTarget.Format(GetString(IDS_STRING61285),olGateAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	pomPrint->omCdc.StartPage();
//	pomPrint->PrintHeader(CString("Gatediagramm"),olPrintDate,omTarget);
	pomPrint->PrintHeader(GetString(IDS_STRING61288),olPrintDate,omTarget);
	pomPrint->PrintTimeScale(400,2800,omStartTime,omEndTime,olGateAreaName);
}

void GateDiagramViewer::PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine)
{
	GATE_LINEDATA *prlLine = &omGroups[ipGroupNo].Lines[ipLineno];
	ropPrintLine.DeleteAll();
	int ilBarCount = prlLine->Bars.GetSize();
	for( int i = 0; i < ilBarCount; i++)
	{
		if (IsOverlapped(prlLine->Bars[i].StartTime,prlLine->Bars[i].EndTime,
			omStartTime,omEndTime))
		{
			PRINTBARDATA rlPrintBar;
			rlPrintBar.Text = prlLine->Bars[i].Text;
			rlPrintBar.StartTime = prlLine->Bars[i].StartTime;
			rlPrintBar.EndTime = prlLine->Bars[i].EndTime;
			rlPrintBar.FrameType = prlLine->Bars[i].FrameType;
			rlPrintBar.MarkerType = prlLine->Bars[i].MarkerType;
			rlPrintBar.IsShadowBar = prlLine->Bars[i].IsShadowBar;
			rlPrintBar.OverlapLevel = 0;
			rlPrintBar.IsBackGroundBar = FALSE;
			ropPrintLine.NewAt(ropPrintLine.GetSize(),rlPrintBar);
		}
	}
}


void GateDiagramViewer::PrintManagers(int ipGroupno,int ipYOffset1,int ipYOffset2)
{
	CString olAllManagers;
	CCSPtrArray<JOBDATA> olFmgJobs;
	ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ipGroupno)->GateAreaId);
	int ilFmgCount = olFmgJobs.GetSize();
	for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
	{
		if (IsOverlapped(olFmgJobs[ilLc].Acfr,olFmgJobs[ilLc].Acto,omStartTime,omEndTime ))
		{
			if (olAllManagers.IsEmpty() == FALSE)
			{
				olAllManagers += "   /   ";
			}
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(olFmgJobs[ilLc].Shur);
			olAllManagers += CString((prlShift != NULL ? prlShift->Sfca : "")) + ", ";
			olAllManagers += CString((prlShift != NULL ? prlShift->Egrp : "")) + ", ";
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olFmgJobs[ilLc].Peno);
			olAllManagers += CString(prlEmp != NULL ? prlEmp->Lanm : "");
		} 
	} 
	if (olAllManagers.IsEmpty() == FALSE)
	{
		int ilYOffset = 0;
		if (igFirstGroupOnPage == FALSE)
		{
			pomPrint->PrintText(420,0,800,40,PRINT_SMALLBOLD,"Flightmanager:",TRUE);
			pomPrint->PrintText(420,40,0,130,PRINT_SMALL,olAllManagers,TRUE);
			pomPrint->PrintText(420,0,0,50,PRINT_SMALL,"");
		}
		else
		{
			pomPrint->PrintText(420,ipYOffset1,800,ipYOffset1+40,PRINT_SMALLBOLD,"Flightmanager:");
			pomPrint->PrintText(420,ipYOffset2,0,ipYOffset2+90,PRINT_SMALL,olAllManagers);
		}  
		igFirstGroupOnPage = FALSE;
		/********************
		pomPrint->PrintText(420,ipYOffset1+ilYOffset,800,ipYOffset1+40+ilYOffset,PRINT_SMALLBOLD,"Flightmanager:");
		pomPrint->PrintText(420,ipYOffset2+ilYOffset,2800,ipYOffset2+90+ilYOffset,PRINT_SMALL,olAllManagers);
		******************/
	}
}

void GateDiagramViewer::PrintGateArea(CPtrArray &opPtrArray,int ipGroupNo)
{

	if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
		}
		PrintManagers(ipGroupNo,380,420);
		PrintGateDiagramHeader(ipGroupNo);
	}
	else
	{
		PrintManagers(ipGroupNo,0,0);
		CString olGateAreaName = GetGroupText(ipGroupNo);
		pomPrint->PrintGanttHeader(400,2800,olGateAreaName);
	}

	CCSPtrArray<PRINTBARDATA> ropPrintLine;
	int ilLineCount = GetLineCount(ipGroupNo);
    for( int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
		{
			if ( pomPrint->imPageNo > 0)
			{
				pomPrint->PrintGanttBottom();
				pomPrint->PrintFooter("","");
				pomPrint->omCdc.EndPage();
				igFirstGroupOnPage = TRUE;
			}
			PrintGateDiagramHeader(ipGroupNo);
		}

		PrintPrepareLineData(ipGroupNo,ilLineno,ropPrintLine);
		if (pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine) != TRUE)
		{
			// page full
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
			pomPrint->imLineNo = 0;
			PrintGateDiagramHeader(ipGroupNo);
			// try it again, but don't continue with this line if it don't fit on page again!
			pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine);
		}

	}
	pomPrint->PrintGanttBottom();
	//pomPrint->imLineNo = 999;
}

void GateDiagramViewer::PrintOneFm(SHIFTDATA *prpShift,EMPDATA *prpEmp,JOBDATA *prpJob,BOOL bpIsFirstLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	int ilTopFrame = bpIsFirstLine == TRUE ? PRINT_FRAMETHIN : PRINT_NOFRAME;

	pomPrint->imLineHeight = 82;

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 650;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMETHIN;
	rlElement.FrameTop   = bpIsFirstLine;
	rlElement.FrameBottom= PRINT_FRAMETHIN;
	rlElement.pFont       = &pomPrint->omMediumFont_Regular;
	rlElement.Text       = ogEmpData.GetEmpName(prpEmp);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 120;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;
	rlElement.Text       = prpShift != NULL ? CString(prpShift->Sfca) : "";
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 300;
	rlElement.Text       = prpShift != NULL ? 
								prpJob->Acfr.Format("%H:%M") + "-" +
								prpJob->Acto.Format("%H:%M") : "";
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByPkno(olJobs,prpJob->Peno);
	olJobs.Sort(CompareJobStartTime);

	CString olFlightText;

	for (int ilLc = olJobs.GetSize()-1; ilLc >= 0; ilLc--)
	{
		JOBDATA *prlJob = &olJobs[ilLc];
	    // If it's not a special flightmanager job, ignore it
		if (strcmp(prlJob->Jtco,JOBFM) == 0)
		{
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
			if (prlFlight != NULL)
			{
				if (olFlightText.IsEmpty() == FALSE)
				{
					olFlightText += CString(", ");
				}
				olFlightText += prlFlight->Fnum;
			}
		}

	}
	olJobs.RemoveAll();

	rlElement.Length     = 1550;
	rlElement.Alignment  = PRINT_LEFT;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.Text       = olFlightText;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}

void GateDiagramViewer::PrintFmHeader(int ipGroupNo)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	CString olGateAreaName = GetGroupText(ipGroupNo);
	pomPrint->imLineHeight = 130;

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 2620;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omLargeFont_Bold;
	rlElement.Text       = olGateAreaName;
	
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}


void GateDiagramViewer::PrintFmForGateArea(CPtrArray &opPtrArray,int ipGroupNo)
{
	BOOL blIsFirstLine = FALSE;

	if(pomPrint->imLineTop > pomPrint->imHeight-400)
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->imLineTop = pomPrint->imFirstLine;
			pomPrint->omCdc.StartPage();
			pomPrint->PrintHeader();
		}
	}

	PrintFmHeader(ipGroupNo);
	
	CCSPtrArray<JOBDATA> olFmgJobs;
	ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ipGroupNo)->GateAreaId);
	olFmgJobs.Sort(CompareJobStartTime);
	int ilFmgCount = olFmgJobs.GetSize();
	//for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
	for(int ilLc = ilFmgCount-1; ilLc >= 0; ilLc--)
	{
		if (IsOverlapped(olFmgJobs[ilLc].Acfr,olFmgJobs[ilLc].Acto,omStartTime,omEndTime ))
		{
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(olFmgJobs[ilLc].Shur);
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olFmgJobs[ilLc].Peno);
			if(pomPrint->imLineTop > pomPrint->imHeight-200)
			{
				if ( pomPrint->imPageNo > 0)
				{
					pomPrint->PrintFooter("","");
					pomPrint->omCdc.EndPage();
					pomPrint->imLineTop = pomPrint->imFirstLine;
					pomPrint->omCdc.StartPage();
					pomPrint->PrintHeader();
					blIsFirstLine = TRUE;
				}
			}
			PrintOneFm(prlShift,prlEmp,&olFmgJobs[ilLc],blIsFirstLine);
			blIsFirstLine = FALSE;
		} 
	} 
	
}



void GateDiagramViewer::PrintDiagram(CPtrArray &opPtrArray)
{
	igFirstGroupOnPage = TRUE;
	CString omTarget = CString("Anzeigebegin: ") + CString(omStartTime.Format("%d.%m.%Y"))
		+  "     Ansicht: " + ogCfgData.rmUserSetup.GACV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_PORTRAIT,80,500,200,
		CString("Gatediagramm"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Gatediagramm";	
			pomPrint->omCdc.StartDoc( &rlDocInfo );

			int ilGroupCount = GetGroupCount();
			for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
			{
				PrintGateArea(opPtrArray,ilGroupno);
			}
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

void GateDiagramViewer::PrintFm(CPtrArray &opPtrArray)
{
	igFirstGroupOnPage = TRUE;
	CString omTarget = CString("Ansicht: ") + ogCfgData.rmUserSetup.GACV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_LANDSCAPE,80,500,200,
		CString("Flightmanager"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Flightmanager";	
			pomPrint->imLineHeight = 82;
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->omCdc.StartPage();
			pomPrint->PrintHeader();

			int ilGroupCount = GetGroupCount();
			for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
			{
				PrintFmForGateArea(opPtrArray,ilGroupno);
			}
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

CTime GateDiagramViewer::GetFlightArr(FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{	
	CTime olStartTime;
	if(prpFlight != NULL)
	{
		if(ipReturnFlightType == ID_ARR_RETURNFLIGHT || ogFlightData.IsArrival(prpFlight))
		{
			olStartTime = prpFlight->Tifa;
			if(olStartTime == TIMENULL)
			{
				olStartTime = prpFlight->Stoa;
			}
		}
		else
		{
			olStartTime = prpFlight->Tifd;
			if(olStartTime == TIMENULL)
			{
				olStartTime = prpFlight->Stod;
			}
			olStartTime -= CTimeSpan(0,1,0,0);
		}
	}
	return olStartTime;
}

//	if the AdviceTime is set then this flight is indefinately delayed so use STD/STA
CTime GateDiagramViewer::GetFlightDep(FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	CTime olEndTime;
	if(prpFlight != NULL)
	{
		if(ipReturnFlightType == ID_DEP_RETURNFLIGHT || ogFlightData.IsDeparture(prpFlight))
		{
			olEndTime = prpFlight->Stod;
			if(!ogFlightData.AdviceTimeSet(prpFlight) && prpFlight->Tifd != TIMENULL)
			{
				olEndTime = prpFlight->Tifd;
			}
		}
		else
		{
			olEndTime = prpFlight->Stoa;
			if(!ogFlightData.AdviceTimeSet(prpFlight) && prpFlight->Tifa != TIMENULL)
			{
				olEndTime = prpFlight->Tifa;
			}
			olEndTime += CTimeSpan(0,0,20,0);
		}
	}
	return olEndTime;
}

// get a list of Demand URNOs for all jobs displayed
void GateDiagramViewer::GetDemandList(CUIntArray  &ropDemandList)
{
	for(int ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
	{
		for(int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
		{
            for(int ilBarno = 0; ilBarno < GetBarCount(ilGroupno, ilLineno); ilBarno++)
			{
	            for(int ilIndicatorno = 0; ilIndicatorno < GetIndicatorCount(ilGroupno, ilLineno, ilBarno); ilIndicatorno++)
				{
					GATE_INDICATORDATA *prlIndicator = GetIndicator(ilGroupno, ilLineno, ilBarno, ilIndicatorno);
	                ropDemandList.Add(prlIndicator->DemandUrno);
				}
			}
		}
	}
}



bool GateDiagramViewer::IsGroupWithoutGates(int ipGroupno)
{
	static CString olWithoutGateName = GetString(IDS_STRING61345); // "Ohne Gate"
	if (ipGroupno < 0)
		return false;

	if (olWithoutGateName == GetGroupText(ipGroupno))
		return true;
	else
		return false;

}

int GateDiagramViewer::GetGroupWithoutGates()
{
	int ilSize = GetGroupCount();
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		if (IsGroupWithoutGates(ilC))
			return ilC;
	}
	return -1;
}

int GateDiagramViewer::SortLineOfGroupWithoutGates(int ipLineNo, long lpFlightUrno /* = 0L */)
{
	int ilInsertedBar = -1;
	int ilGroupNo = GetGroupWithoutGates();
	if (ipLineNo >= 0 && ilGroupNo >= 0)
	{
		int ilLineSize = GetLineCount(ilGroupNo);
		ASSERT(ipLineNo < ilLineSize);

		GATE_LINEDATA *prlLine = GetLine(ilGroupNo,ipLineNo);
		if(prlLine != NULL)
		{
			prlLine->Bars.Sort(ByStartTime);	

			if(lpFlightUrno != 0L)
			{
				// return the inserted bar number
				int ilNumBars = prlLine->Bars.GetSize();
				for(int ilBar = 0; ilBar < ilNumBars; ilBar++)
				{
					if(prlLine->Bars[ilBar].FlightUrno == lpFlightUrno)
					{
						ilInsertedBar = ilBar;
						break;
					}
				}
			}
		}
	}
	return ilInsertedBar;
}

COLORREF GateDiagramViewer::GetColourForArrFlight(long lpFlightUrno)
{
	COLORREF rlColour = BLACK;
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpFlightUrno);
	if(!ogFlightData.IsArrivalOrBoth(prlFlight))
	{
		prlFlight = ogFlightData.GetRotationFlight(prlFlight);
	}

	if(prlFlight != NULL)
	{
		int ilReturnFlightType = ogFlightData.IsReturnFlight(prlFlight) ? ID_ARR_RETURNFLIGHT : ID_NOT_RETURNFLIGHT;
		FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight,ilReturnFlightType);
		if(prlFlight->Onbl != TIMENULL)
		{
			rlColour = atol(GetUserData("ONBL"));
		}
		else if(prlFlight->Land != TIMENULL)
		{
			rlColour = atol(GetUserData("LAND"));
		}
		else if(prlRotation != NULL && (prlRotation->Ofbl != TIMENULL || prlRotation->Airb != TIMENULL))
		{
			// departure already offblock or airborne so this must be onbl
			rlColour = atol(GetUserData("ONBL"));
		}
		else if(prlFlight->Tmoa != TIMENULL)
		{
			rlColour = atol(GetUserData("TMOA"));
		}
		else if(prlFlight->Etoa != TIMENULL)
		{
			rlColour = atol(GetUserData("ETOA"));
		}
		else if(prlFlight->Stoa != TIMENULL)
		{
			rlColour = atol(GetUserData("STOA"));
		}
	}

	return rlColour;
}

COLORREF GateDiagramViewer::GetColourForDepFlight(long lpFlightUrno)
{
	COLORREF rlColour = BLACK;
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpFlightUrno);
	if(!ogFlightData.IsDepartureOrBoth(prlFlight))
	{
		prlFlight = ogFlightData.GetRotationFlight(prlFlight);
	}

	if(prlFlight != NULL)
	{
		if(prlFlight->Airb != TIMENULL)
		{
			rlColour = atol(GetUserData("AIRB"));
		}
		else if(prlFlight->Ofbl != TIMENULL)
		{
			rlColour = atol(GetUserData("OFBL"));
		}
		else if(prlFlight->Slot != TIMENULL && IsPrivateProfileOn("SHOW_GT_SLOT_MARKER","NO") )
		{
 			rlColour = atol(GetUserData("SLOT"));
		}
		else if(prlFlight->Etod != TIMENULL)
		{
			rlColour = atol(GetUserData("ETOD"));
		}
		else if(prlFlight->Stod != TIMENULL)
		{
			rlColour = atol(GetUserData("STOD"));
		}
	}

	return rlColour;
}

bool GateDiagramViewer::DisplayMarker(CString opMarkerType)
{
	return (GetUserData(opMarkerType) == "YES") ? true : false;
}


bool GateDiagramViewer::IsAllFlightsGroup(int ipGroupno)
{
	static CString olAllFlightsGroupName = GetString(IDS_ALL_FLIGHTS);
	if (ipGroupno < 0)
		return false;

	if (olAllFlightsGroupName == GetGroupText(ipGroupno))
		return true;
	else
		return false;

}

int GateDiagramViewer::GetAllFlightsGroup()
{
	int ilSize = GetGroupCount();
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		if (IsAllFlightsGroup(ilC))
			return ilC;
	}
	return -1;
}

int GateDiagramViewer::SortLineOfGroupAllFlights(int ipLineNo, long lpFlightUrno /* = 0L */)
{
	int ilInsertedBar = -1;
	int ilGroupNo = GetAllFlightsGroup();
	if (ipLineNo >= 0 && ilGroupNo >= 0)
	{
		int ilLineSize = GetLineCount(ilGroupNo);
		ASSERT(ipLineNo < ilLineSize);

		GATE_LINEDATA *prlLine = GetLine(ilGroupNo,ipLineNo);
		if(prlLine != NULL)
		{
			prlLine->Bars.Sort(ByStartTime);	

			if(lpFlightUrno != 0L)
			{
				// return the inserted bar number
				int ilNumBars = prlLine->Bars.GetSize();
				for(int ilBar = 0; ilBar < ilNumBars; ilBar++)
				{
					if(prlLine->Bars[ilBar].FlightUrno == lpFlightUrno)
					{
						ilInsertedBar = ilBar;
						break;
					}
				}
			}
		}
	}
	return ilInsertedBar;
}

//Singapore
int GateDiagramViewer::SortLineOfGroupFlights(int ipGroupNo,int ipLineNo, long lpFlightUrno /* = 0L */)
{
	int ilInsertedBar = -1;
	if (ipLineNo >= 0 && ipGroupNo >= 0)
	{
		int ilLineSize = GetLineCount(ipGroupNo);
		ASSERT(ipLineNo < ilLineSize);

		GATE_LINEDATA *prlLine = GetLine(ipGroupNo,ipLineNo);
		if(prlLine != NULL)
		{
			prlLine->Bars.Sort(ByStartTime);	

			if(lpFlightUrno != 0L)
			{
				// return the inserted bar number
				int ilNumBars = prlLine->Bars.GetSize();
				for(int ilBar = 0; ilBar < ilNumBars; ilBar++)
				{
					if(prlLine->Bars[ilBar].FlightUrno == lpFlightUrno)
					{
						ilInsertedBar = ilBar;
						break;
					}
				}
			}
		}
	}
	return ilInsertedBar;
}


// if single flight is not NULL then recreate a bar for the single flight
// only, otherwise create bars for all flights without positions
int GateDiagramViewer::CreateLinesForAllFlights(FLIGHTDATA *prpSingleFlight)
{
	int ilLastLineUpdated = -1;
	int ilGroupno = 0L;
	if(FindGroup(GetString(IDS_ALL_FLIGHTS),ilGroupno) && (bmIncludeArrivalFlights || bmIncludeDepartureFlights))
	{
		if(prpSingleFlight == NULL)
		{
			ilLastLineUpdated = CreateGeneralGroupLines(ogFlightData.omData, ilGroupno);
		}
		else
		{
			CCSPtrArray <FLIGHTDATA> olFlights;
			olFlights.Add(prpSingleFlight);
			ilLastLineUpdated = CreateGeneralGroupLines(olFlights, ilGroupno);
		}
	}

	return ilLastLineUpdated;
}

// if single flight is not NULL then recreate a bar for the single flight
// only, otherwise create bars for all flights without positions
int GateDiagramViewer::CreateLinesForFlightsWithoutGates(FLIGHTDATA *prpSingleFlight)
{
	int ilLastLineUpdated = -1;
	int ilGroupno = 0L;
	if(FindGroup(GetString(IDS_STRING61345),ilGroupno) && (bmIncludeArrivalFlights || bmIncludeDepartureFlights))
	{
		CCSPtrArray <FLIGHTDATA> olFlights;
		if(prpSingleFlight == NULL)
		{
			int ilNumFlights = ogFlightData.omData.GetSize();
			for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
			{
				olFlights.Add(&ogFlightData.omData[ilFlight]);
			}
		}
		else
		{
			olFlights.Add(prpSingleFlight);
		}
		ilLastLineUpdated = CreateGeneralGroupLines(olFlights, ilGroupno);
	}

	return ilLastLineUpdated;
}

int GateDiagramViewer::CreateGeneralGroupLines(CCSPtrArray <FLIGHTDATA> &ropFlights, int ipGroupno)
{
	bool blIsAllFlightsGroup = (GetGroup(ipGroupno)->GateAreaId == GetString(IDS_ALL_FLIGHTS));

	int ilLastLineUpdated = -1;
	FLIGHTDATA *prlFlight;
	int ilNumFlights = ropFlights.GetSize();
	if(ilNumFlights > 0)
	{
		// Need to find a space on an existing line to insert the new bar.
		// In order to do this a dummy bar (on a dummy line) is created so
		// that the same function is used (MakeBar) to create the new bar,
		// and thus the new bar is garanteed to have the correct length.
		// This dummy bar is then used to find a space big enough on existing
		// lines to insert the bar. If no lines are found then a new line is created.

		GATE_LINEDATA *prlLine;
		int ilLineCount, ilLineno;
		GATE_LINEDATA rlDummyLine;
		CString olDummyLineText;
		olDummyLineText.Format("% 5d",0);
		rlDummyLine.Text = olDummyLineText;
		CreateLine(ipGroupno, &rlDummyLine);
		int ilDummyLineNum;

		for(int ilLc = 0; ilLc < ilNumFlights; ilLc++)
		{
			prlFlight = &ropFlights[ilLc];

			CUIntArray olReturnFlightTypes;
			int ilNumReturnFlightTypes = GetReturnFlightTypes(prlFlight, olReturnFlightTypes);
			for(int ilF = 0; ilF < ilNumReturnFlightTypes; ilF++)
			{
				CString olGate = ogFlightData.GetGate(prlFlight,olReturnFlightTypes[ilF]);
				
				//Singapore
				bool blTerminalGroupFound = false;
				CString olTerminalGroupName = GetGroupText(ipGroupno);
				if(omListTerminalGroupName.Find(olTerminalGroupName) != NULL)
				{
					blTerminalGroupFound = true;
				}
				//CString olTerminal = ogFlightData.GetTerminal(prlFlight,olReturnFlightTypes[ilF]); //Singapore
				if(blIsAllFlightsGroup || olGate.IsEmpty() || blTerminalGroupFound == true)
				{
					BOOL blPassFilter = IsPassFilter("",prlFlight);					
					if( blPassFilter && ((bmIncludeArrivalFlights && (ogFlightData.IsArrival(prlFlight) || olReturnFlightTypes[ilF] == ID_ARR_RETURNFLIGHT))
						|| (bmIncludeDepartureFlights && (ogFlightData.IsDeparture(prlFlight) || olReturnFlightTypes[ilF] == ID_DEP_RETURNFLIGHT))))
					{
						if(FindLine(olDummyLineText, ipGroupno, ilDummyLineNum))
						{
							MakeBar(ipGroupno, ilDummyLineNum, prlFlight, blIsAllFlightsGroup, olReturnFlightTypes[ilF]);
							GATE_LINEDATA *prlDummyLine = GetLine(ipGroupno, ilDummyLineNum);
							if(prlDummyLine->Bars.GetSize() > 0) // make sure the dummy bar was created
							{
								GATE_BARDATA *prlBarToAdd = GetBar(ipGroupno, ilDummyLineNum, 0);

								int ilSelectedLine = -1;
								ilLineCount = GetLineCount(ipGroupno);
								for(ilLineno = 0; ilSelectedLine == -1 && ilLineno < ilLineCount; ilLineno++)
								{
									prlLine = GetLine(ipGroupno, ilLineno);
									if(prlLine->Text != olDummyLineText)
									{
										int ilNumBars = prlLine->Bars.GetSize();
										if(ilNumBars <= 0)
										{
											ilSelectedLine = ilLineno;
										}
										else
										{
											CTimeSpan olOneMinute(0,0,1,0);

											if(ilNumBars > 0 && prlLine->Bars[0].StartTime > prlBarToAdd->EndTime)
											{
												//  the new bar fits in before the first one
												ilSelectedLine = ilLineno;
											}

											for(int ilBar = 0; ilSelectedLine == -1 && ilBar < ilNumBars; ilBar++)
											{
												CTime olPrevEnd = prlLine->Bars[ilBar].EndTime;
												CTime olNextBegin = ((ilBar+1) < ilNumBars) ? prlLine->Bars[ilBar+1].StartTime : (prlBarToAdd->EndTime+olOneMinute);
												if(prlBarToAdd->StartTime > olPrevEnd && prlBarToAdd->EndTime < olNextBegin)
												{
													// the new bar fits in the free space
													ilSelectedLine = ilLineno;
												}
											}
										}
									}
								}
									 
								if(ilSelectedLine != -1)
								{
									// existing line found so add the new bar and sort all bars in this line by duty start
									MakeBar(ipGroupno, ilSelectedLine, prlFlight, blIsAllFlightsGroup, olReturnFlightTypes[ilF]);
									prlLine = GetLine(ipGroupno, ilSelectedLine);
									prlLine->Bars.Sort(ByStartTime);
									ilLastLineUpdated = ilSelectedLine;
								}	
								else
								{
									// no lines free so create a new one
									GATE_LINEDATA rlLine;
									rlLine.Text.Format("% 5d",ilLineCount);
									rlLine.IsStairCaseGate = false;
									int ilLineNum = CreateLine(ipGroupno, &rlLine);
									MakeBar(ipGroupno, ilLineNum, prlFlight, blIsAllFlightsGroup, olReturnFlightTypes[ilF]);
									ilLastLineUpdated = ilLineNum;
								}
								DeleteBar(ipGroupno, ilDummyLineNum, 0);
							}
						}
					}
				}
			}
		}
		if(FindLine(olDummyLineText, ipGroupno, ilDummyLineNum))
		{
			DeleteLine(ipGroupno, ilDummyLineNum);
			if(ilDummyLineNum < ilLastLineUpdated)
			{
				ilLastLineUpdated--;
			}
		}
	}

	return ilLastLineUpdated;
}

//Singapore
int GateDiagramViewer::CreateLinesTerminalFlights(int ipGroupNo, FLIGHTDATA *prpSingleFlight /*NULL*/)
{
	int ilLastLineUpdated = -1;
	if(ipGroupNo != -1 && (bmIncludeArrivalFlights || bmIncludeDepartureFlights))
	{
		CCSPtrArray <FLIGHTDATA> olFlights;	
		if(prpSingleFlight == NULL)
		{
			int ilNumFlights = ogFlightData.omData.GetSize();
			for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
			{
				if(IsTerminalFlightToAdd(ipGroupNo,&ogFlightData.omData[ilFlight]) == TRUE)
				{
					olFlights.Add(&ogFlightData.omData[ilFlight]);
				}
			}
		}
		else
		{
			if(IsTerminalFlightToAdd(ipGroupNo,prpSingleFlight) == TRUE)
			{
				olFlights.Add(prpSingleFlight);
			}
		}
		ilLastLineUpdated = CreateGeneralGroupLines(olFlights, ipGroupNo);
	}

	return ilLastLineUpdated;
}

//Singapore
BOOL GateDiagramViewer::IsTerminalFlightToAdd(int ipGroupNo,FLIGHTDATA *prpSingleFlight)
{
	BOOL blAddFlight = FALSE;
	CString olTerminalNo;
	CString olGroupName = GetGroupText(ipGroupNo);	
	if(omMapTerminalGroupNameToNo.Lookup(olGroupName,olTerminalNo) == TRUE)
	{
		if(ogFlightData.IsArrival(prpSingleFlight) == true)
		{
			blAddFlight = olTerminalNo.CompareNoCase(prpSingleFlight->Tga1) == 0;
		}
		else if(ogFlightData.IsDeparture(prpSingleFlight) == true)
		{
			blAddFlight = olTerminalNo.CompareNoCase(prpSingleFlight->Tgd1) == 0;
		}
		if(ogFlightData.IsReturnFlight(prpSingleFlight) == true)
		{
			blAddFlight = olTerminalNo.CompareNoCase(prpSingleFlight->Tgd1) == 0;
		}
	}
	return blAddFlight;
}
//Singapore
void GateDiagramViewer::UpdateDiagramTerminals(FLIGHTDATA *prpFlight)
{
	int ilGroupNo = -1;
	int ilLastLine = 0;
	int ilUpdatedLine = 0;
	CString olGroupName;
	POSITION olPosition = NULL;
	olPosition = omListTerminalGroupName.GetHeadPosition();
	while(olPosition != NULL)
	{
		olGroupName = omListTerminalGroupName.GetNext(olPosition);
		if(IsPassFilter(olGroupName,prpFlight))
		{		
			if(FindGroup(olGroupName,ilGroupNo))
			{
				ilLastLine	  = GetLineCount(ilGroupNo) - 1;
				ilUpdatedLine = CreateLinesTerminalFlights(ilGroupNo,prpFlight);
			}
			if(ilUpdatedLine != -1)
			{
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilUpdatedLine, ilGroupNo);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
					ilUpdatedLine = -1;
				}
			}
		}
	}
				
}

void GateDiagramViewer::GetTerminalList(CStringList& ropTerminalList)
{
	CreateTerminalList();
	ropTerminalList.RemoveAll();
	ropTerminalList.AddTail(&omListTerminalGroupName);
}

//Singapore
void GateDiagramViewer::CreateTerminalList()
{	
	omMapTerminalGroupNameToNo.RemoveAll();
	omListTerminalGroupName.RemoveAll();

	char chFlightTerminals[100];
	GetPrivateProfileString(pcgAppName,"FLIGHT_TERMINALS","",chFlightTerminals,sizeof(chFlightTerminals),ogBasicData.GetConfigFileName());
	if(strlen(chFlightTerminals) > 0)
	{
		CString olGroupName;
		char sep[] = ",";
		char* token = NULL;
		token = strtok(chFlightTerminals,sep);
		while(token != NULL)
		{
			olGroupName.Empty();
			if(strcmp(token,omTerminal1) == 0)
			{
				// create a group for flights in terminal T1
				olGroupName = GetString(IDS_STRINGT1FLIGHTS);
				omMapTerminalGroupNameToNo.SetAt(olGroupName,omTerminal1);
			}
			else if(strcmp(token,omTerminal2) == 0)
			{
				// create a group for flights in terminal T2
				olGroupName = GetString(IDS_STRINGT2FLIGHTS);
				omMapTerminalGroupNameToNo.SetAt(olGroupName,omTerminal2);
			}			
			else if(strcmp(token,omTerminal3) == 0)
			{
				// create a group for flights in terminal T3
				olGroupName = GetString(IDS_STRINGT3FLIGHTS);
				omMapTerminalGroupNameToNo.SetAt(olGroupName,omTerminal3);
			}			
			else if(strcmp(token,omTerminal4) == 0)
			{
				// create a group for flights in terminal T4
				olGroupName = GetString(IDS_STRINGT4FLIGHTS);
				omMapTerminalGroupNameToNo.SetAt(olGroupName,omTerminal4);
			}			
			else if(strcmp(token,omTerminalB) == 0)
			{
				// create a group for flights in terminal TB
				olGroupName = GetString(IDS_STRINGTBFLIGHTS);
				omMapTerminalGroupNameToNo.SetAt(olGroupName,omTerminalB);
			}
			else if(strcmp(token," ") == 0)
			{
				// create a group for flights without terminal
				olGroupName = GetString(IDS_STRINGWTFLIGHTS);
				omMapTerminalGroupNameToNo.SetAt(olGroupName,"");				
			}
			omListTerminalGroupName.AddTail(olGroupName);
			token = strtok(NULL, sep);
		}
	}
}
//Singapore
void GateDiagramViewer::CreateTerminalGroups()
{	
	omMapTerminalGroupNameToNo.RemoveAll();
	omListTerminalGroupName.RemoveAll();
	imT1FlightsGroup = -1;
	imT2FlightsGroup = -1;
	imT3FlightsGroup = -1;
	imT4FlightsGroup = -1;
	imTBFlightsGroup = -1;
	imWTFlightsGroup = -1;

	CString olGroupName;
	CreateTerminalList();
	POSITION olPosition = NULL;
	olPosition = omListTerminalGroupName.GetHeadPosition();
	while(olPosition != NULL)
	{
		olGroupName = omListTerminalGroupName.GetNext(olPosition);		
		if(!olGroupName.IsEmpty() && IsPassFilter(olGroupName))
		{
			GATE_GROUPDATA rlGroup;
			rlGroup.GateAreaId = olGroupName; 
			rlGroup.Text = olGroupName;
			CreateGroup(&rlGroup);
		}
	}

	// get indexes - don't do this until both are created as inserting can change the indexes
	if(omListTerminalGroupName.Find(GetString(IDS_STRINGT1FLIGHTS)) != NULL)
	{
		FindGroup(GetString(IDS_STRINGT1FLIGHTS),imT1FlightsGroup);
	}
	if(omListTerminalGroupName.Find(GetString(IDS_STRINGT2FLIGHTS)) != NULL)
	{
		FindGroup(GetString(IDS_STRINGT2FLIGHTS),imT2FlightsGroup);
	}
	if(omListTerminalGroupName.Find(GetString(IDS_STRINGT3FLIGHTS)) != NULL)
	{
		FindGroup(GetString(IDS_STRINGT3FLIGHTS),imT3FlightsGroup);
	}
	if(omListTerminalGroupName.Find(GetString(IDS_STRINGT4FLIGHTS)) != NULL)
	{
		FindGroup(GetString(IDS_STRINGT4FLIGHTS),imT4FlightsGroup);
	}
	if(omListTerminalGroupName.Find(GetString(IDS_STRINGTBFLIGHTS)) != NULL)
	{
		FindGroup(GetString(IDS_STRINGTBFLIGHTS),imTBFlightsGroup);
	}
	if(omListTerminalGroupName.Find(GetString(IDS_STRINGWTFLIGHTS)) != NULL)
	{
		FindGroup(GetString(IDS_STRINGWTFLIGHTS),imWTFlightsGroup);
	}
}