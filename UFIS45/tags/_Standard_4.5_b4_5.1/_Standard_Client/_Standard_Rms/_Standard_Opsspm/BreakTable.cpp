// BreakTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaSprData.h>

#include <OpssPm.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <BreakTableViewer.h>
#include <BreakTable.h>

#include <cviewer.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <dataset.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <FlightDetailWindow.h>
#include <ccstable.h>
#include <StaffDetailWindow.h>
#include <JobTimeHandler.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

/////////////////////////////////////////////////////////////////////////////
// BreakTable dialog

BreakTable::BreakTable(CWnd* pParent /*=NULL*/)
	: CDialog(BreakTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(BreakTable)
	m_DisplayNonOverlappingOnly = FALSE;
	//}}AFX_DATA_INIT

	pomParentWnd = pParent;
	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
    pomTable->tempFlag = 2;

    CDialog::Create(BreakTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,"L","1");
	olRect.top += 40;
    MoveWindow(&olRect);

	CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
	rect.top += m_nDialogBarHeight;
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

	pomViewer = new BreakTableViewer();
	pomViewer->Attach(pomTable);
	pomViewer->SetViewerKey("StaffDia"); // display the same emps that are displayed on the StaffGantt

	UpdateView();
}

BreakTable::BreakTable(BreakTableViewer *popViewer,CWnd* pParent /*=NULL*/)
	: CDialog(BreakTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(BreakTable)
	m_DisplayNonOverlappingOnly = FALSE;
	//}}AFX_DATA_INIT

	if (popViewer)
		pomViewer = popViewer;
	else
		pomViewer = new BreakTableViewer();

	pomParentWnd	= pParent;

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
    pomTable->tempFlag = 2;

    CDialog::Create(BreakTable::IDD);
//    CRect olRect;
//	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.CCDB,ogCfgData.rmUserSetup.MONS);
//	olRect.top += 40;
//    MoveWindow(&olRect);

	CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
	rect.top += m_nDialogBarHeight;
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	
	pomViewer->Attach(pomTable);
	pomViewer->SetViewerKey("StaffDia"); // display the same emps that are displayed on the StaffGantt
	UpdateView();
}

BreakTable::~BreakTable()
{
	TRACE("BreakTable::~BreakTable()\n");
	pomViewer->Attach(NULL);
	delete pomViewer;
	delete pomTable;	
}

void BreakTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BreakTable)
	DDX_Check(pDX, IDC_CANBESTARTED, m_DisplayNonOverlappingOnly);
	DDX_Control(pDX, IDC_DATE, m_Date);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BreakTable, CDialog)
	//{{AFX_MSG_MAP(BreakTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
    ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelchange)
	ON_BN_CLICKED(IDC_CANBESTARTED, OnDisplayNonOverlappingOnly)
	ON_COMMAND(BREAKTABLE_CONFIRMJOB_MENUITEM, OnMenuStaffConfirm)
    ON_CBN_SELCHANGE(IDC_VIEW, UpdateView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// BreakTable message handlers

BOOL BreakTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_BREAKTABLETITLE));


	// TODO: Add extra initialization here
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    MoveWindow(&rect);

	m_DragDropTarget.RegisterTarget(this, this,pomTable->GetCTableListBox());

	// Register DDX call back function
	TRACE("BreakTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("BreakTable"),CString("Redisplay all from What-If"), BreakTableCf);
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("BreakTable"),CString("Global Date Change"), BreakTableCf);


	CWnd *polWnd = GetDlgItem(IDC_CANBESTARTED); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_BREAKTAB_CANBESTARTED));
		//polWnd->SetFocus();
	}

	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}

	// set the list of views
	InitSelectViewComboBox();
	UpdateView();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void BreakTable::InitSelectViewComboBox(void)
{
	UpdateComboBox();
	//ogBasicData.SetWindowStat("BREAK_TABLE IDC_VIEW",GetDlgItem(IDC_VIEW));
}

// set the list of views
void BreakTable::UpdateComboBox(void)
{
	// check if the view box is enabled and displayed depending on the status in BDPSSEC
	CComboBox *polCB = (CComboBox *) GetDlgItem(IDC_VIEW);
	//if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EMPS_DLG IDC_VIEW"))
	if(polCB != NULL)
	{
		polCB->ResetContent();
		CStringArray olStrArr;
		pomViewer->GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			polCB->AddString(olStrArr[ilIndex]);
		}
		CString olViewName = pomViewer->GetViewName();
		if (olViewName.IsEmpty() == TRUE)
		{
			olViewName = ogCfgData.rmUserSetup.STCV;
		}

		ilIndex = polCB->FindString(-1,olViewName);
			
		if (ilIndex != CB_ERR)
		{
			polCB->SetCurSel(ilIndex);
		}
	}
}

// view selected (or initializing)
void BreakTable::UpdateView() 
{
	char pclViewName[200];
	strcpy(pclViewName,GetString(IDS_STRINGDEFAULT));

	// check if the view box is enabled and displayed depending on the status in BDPSSEC
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEW);
	if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EMPS_DLG IDC_VIEW"))
	{
		polCB->GetLBText(polCB->GetCurSel(), pclViewName);
	}

	AfxGetApp()->DoWaitCursor(1);
	pomViewer->ChangeViewTo(pclViewName);
	AfxGetApp()->DoWaitCursor(-1);
}

void BreakTable::OnDestroy() 
{
    ogCCSDdx.UnRegister(this, NOTUSED);
	CDialog::OnDestroy();
}

void BreakTable::OnCancel() 
{
	DestroyWindow();
}

void BreakTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
        pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

BOOL BreakTable::DestroyWindow() 
{
	if (pomParentWnd)
	{
		pomParentWnd->SendMessage(WM_BREAKTABLE_EXIT,0,0l);
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// BreakTable -- implementation of DDX call back function

void BreakTable::BreakTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	BreakTable *polTable = (BreakTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

//*****
/////////////////////////////////////////////////////////////////////////////
// BreakTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a job (flight or non flight dependend).
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create normal flight or flight independent jobs for those staffs.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create a normal flight job for that staff.
//

LONG BreakTable::OnTableDragBegin(UINT wParam, LONG lParam)
{
//	BREAKTABLEDATA_LINE *prlLine = (BREAKTABLEDATA_LINE *)pomTable->GetTextLineData(wParam);
//	if (prlLine != NULL)
//	{
//		TIMEPACKET olTimePacket;
//		olTimePacket.StartTime = prlLine->BreakStart;
//		olTimePacket.EndTime   = prlLine->BreakEnd;
//		ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);
//	}
	return -1L;
}

LONG BreakTable::OnDragOver(UINT wParam, LONG lParam)
{
	return -1L;	// cannot interpret this object
}

LONG BreakTable::OnDrop(UINT wParam, LONG lParam)
{ 
	return -1L;
}

void BreakTable::OnClose() 
{
	if (pomParentWnd)
	{
		pomParentWnd->SendMessage(WM_BREAKTABLE_EXIT,0,0l);
	}

	CDialog::OnClose();
}

void BreakTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();
}

void BreakTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	pomViewer->SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	pomViewer->SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void BreakTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

LONG BreakTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = pomViewer->LineCount();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		BREAKTABLEDATA_LINE *prlLine = pomViewer->GetLine(ipItem);
		if (prlLine != NULL)
		{
			CCSPtrArray <JOBDATA> olPoolJobs;
			ogJobData.GetJobsByShur(olPoolJobs, prlLine->ShiftUrno, TRUE);
			if(olPoolJobs.GetSize() > 0)
			{
				new StaffDetailWindow(this, olPoolJobs[0].Urno);
			}
			else
			{
				CString olText;
				olText.Format("Internal Error - No pool jobs found for the shift URNO %d",prlLine->ShiftUrno);
				MessageBox(olText, "Break Table Error", MB_ICONERROR);
			}
		}
	}

	return 0L;
}

void BreakTable::OnDisplayNonOverlappingOnly()
{
	UpdateData();
	pomViewer->bmDisplayNonOverlappingOnly = m_DisplayNonOverlappingOnly;
	UpdateView();
}

LONG BreakTable::OnTableSelchange(UINT wParam,LONG lParam)
{
	int ilIndex = pomTable->GetCurSel();
	if (ilIndex >= 0 && ilIndex < pomViewer->LineCount())
	{
		BREAKTABLEDATA_LINE *prlLine = pomViewer->GetLine(ilIndex);
		if (prlLine != NULL)
		{
			TIMEPACKET olTimePacket;
			olTimePacket.StartTime = prlLine->BreakStart;
			olTimePacket.EndTime   = prlLine->BreakEnd;
			ogCCSDdx.DataChanged(this,STAFFDIAGRAM_UPDATETIMEBAND,&olTimePacket);
		}
	}

	return 0l;
}

LONG BreakTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam)
{
	omSelectedBreakUrnos.RemoveAll();

    CListBox *polListBox = pomTable->GetCTableListBox();
	int ilSelCount = 0;
	if(polListBox && (ilSelCount = polListBox->GetSelCount()) <= 0)
		return 0L;


	int *polSelectedLines = new int[polListBox->GetSelCount()];
	polListBox->GetSelItems(polListBox->GetSelCount(),polSelectedLines);

	for (int i = 0; i < polListBox->GetSelCount(); i++)
	{
		BREAKTABLEDATA_LINE *prlLine = (BREAKTABLEDATA_LINE *)pomTable->GetTextLineData(polSelectedLines[i]);
		if(prlLine != NULL)
		{
			omSelectedBreakUrnos.Add(prlLine->BreakUrno);
		}
	}
	delete [] polSelectedLines;

	CMenu olMenu;
	CCSTABLENOTIFY *polNotify = reinterpret_cast<CCSTABLENOTIFY *>(lpLParam);
	ASSERT(polNotify);

	CPoint olPoint(polNotify->Point);

	olMenu.CreatePopupMenu();
	olMenu.AppendMenu(MF_STRING,BREAKTABLE_CONFIRMJOB_MENUITEM, GetString(IDS_STRING61353));// Bestätigung

	CCSPtrArray <JOBDATA> olJobs;
	int ilNumUrnos = omSelectedBreakUrnos.GetSize();
	for(int ilBJ = 0; ilBJ < ilNumUrnos; ilBJ++)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(omSelectedBreakUrnos[ilBJ]);
		if(prlJob != NULL)
		{
			olJobs.Add(prlJob);
		}
	}

	if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(olJobs))
		ogBasicData.DisableAllMenuItems(olMenu);

	JobTimeHandler hdl(olJobs,&olMenu,0,1);
	ClientToScreen(&olPoint);
	hdl.TrackPopupMenu(olPoint,this);

	return 0L;
}

void BreakTable::OnMenuStaffConfirm()
{
	CCSPtrArray <JOBDATA> olJobs;
	int ilNumUrnos = omSelectedBreakUrnos.GetSize();
	for(int ilBJ = 0; ilBJ < ilNumUrnos; ilBJ++)
	{
		JOBDATA *prlJob = ogJobData.GetJobByUrno(omSelectedBreakUrnos[ilBJ]);
		if(prlJob != NULL)
		{
			olJobs.Add(prlJob);
		}
	}
	ogDataSet.ConfirmJob(this,olJobs);
}
