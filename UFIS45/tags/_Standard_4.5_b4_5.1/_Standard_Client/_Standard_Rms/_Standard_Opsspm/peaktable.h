// PeakTable.h : header file
//
#ifndef __PEAKTABLE_H__
#define __PEAKTABLE_H__

#include <CedaPeakData.h>
#include <BasicData.h>

class PeakTable : public CDialog
{
// Construction
public:
	PeakTable(CWnd* pParent = NULL,CTime opStart = ogBasicData.GetTime());   // standard constructor
	~PeakTable();
	void UpdateDisplay();
	BOOL PrintPeakLine(CCSPrint *pomPrint,PEAKDATA *prpPeak,BOOL bpIsLastLine);
	BOOL PrintPeakHeader(CCSPrint *pomPrint);
	void ProcessJobChange(JOBDATA *prpJob);

	int m_nDialogBarHeight;

private :
	CCSPrint *pomPrint;

    CTable *pomTable; // visual object, the table content
    CCSPtrArray<PEAKDATA> omLines;
	 CTime omDate;

private:
	CString Format(PEAKDATA *prpPeak);

// Dialog Data
	//{{AFX_DATA(PeakTable)
	enum { IDD = IDD_PEAKTABLE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PeakTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PeakTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPrint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif //__GATETABLE_H__
