// DInfo.h : header file
//
#ifndef _DINFO_H_
#define _DINFO_H_

//=====================================================================================================
// Version 1.13 : bch : 11.10.2000
// Created in response to PRF from IPR in BKK.
// 1. Load Relative timeframe problem --> when the time frame is less than
//    eight hours, the gantt chart timescales were not correctly displayed.
//    Have changed this (in the diagrams) so that per default minimum is 8
//    hours in the diagrams (does not affect what data are loaded in the
//    various CedaXXXData objects. Also in the Ma�tab dialog when less than
//    24 hours are loaded then the 24 hour timescale checkbox is disabled
//    (and the same for 12 hours and 10 hours).
// 2. The date combo boxes are not correctly sorted when there is a month change
// 
// This version also contains on going work for Grupppen Dispo, which
// although not finished should not affect the existing functionality.
//=====================================================================================================
// Version 1.12 : bch: 21/22.09.2000
// This release includes the following changes made for Swissport:
// 1. 21.09.00  Error in CedaFlightData, fields DCD1 and DCD" (delay codes) were 2
//    bytes instead of 3 and thus not big enough.
// 2. 22.09.00  Error in CedaJodData, on deletion of a JOD record, the record is deleted
//    internally in OpssPm, but the pointer to the record's memory in omData was not.
//=====================================================================================================
// Version 1.11 : bch : 05.09.2000
// This release includes the following changes made for Swissport:
// 1. Default conflict time changes: 
// 		Delay 					9  mins
// 		Joboverlap 				9 mins
//		no gate jobs 			60 mins
//		gate change 			90 mins
//		invalid job demand comb.60 mins
//	(updated when RESET_CONFLICTS_TO_DEFAULT=YES in ceda.ini)
// 2. Error: Flight Number was not displayed on the GateGantt status bar for flights without demands.
// 3. Error: GateGannt, Available Employees; those employees whose pools had no restrictions, were not displayed.
// 4. Error: Not all airline 2 letter codes were displayed in the Airline Filter in the Gate Table.
//=====================================================================================================


/////////////////////////////////////////////////////////////////////////////
// DInfo dialog

class DInfo : public CDialog
{
// Construction
public:
	DInfo(CWnd* pParent = NULL);   // standard constructor
	CStringArray *pomInfoList;

// Dialog Data
	//{{AFX_DATA(DInfo)
	enum { IDD = IDD_ABOUTBOX };
	CListBox	m_InfoList;
	CString	m_OpssPmVersion;
	CString	m_Server;
	CString	m_UfisVersion;
	CString	m_User;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DInfo)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DInfo)
	virtual BOOL OnInitDialog();
	afx_msg void OnDebugInfoLog();
	afx_msg void OnCheckDataConsistency();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CWnd *pomParent;
};
#endif // _DINFO_H_
