// StaffDiagramArrDepPage.h : header file
//

#ifndef _STAFFDIAARRDEP_H_
#define _STAFFDIAARRDEP_H_
#include <CCSButtonctrl.h>

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramArrDepPage dialog

class StaffDiagramArrDepPage : public CPropertyPage
{
	DECLARE_DYNCREATE(StaffDiagramArrDepPage)

// Construction
public:
	StaffDiagramArrDepPage();
	~StaffDiagramArrDepPage();

// Dialog Data
	CString omGroupBy;
	void SetColours();

	//{{AFX_DATA(StaffDiagramArrDepPage)
	enum { IDD = IDD_STAFFDIAGRAM_ARRDEP_PAGE };
	BOOL	bmArrCheckBox;
	BOOL	bmDepCheckBox;
	BOOL	bmTurnaroundCheckBox;
	//}}AFX_DATA

	CString omTitle;
	COLORREF lmStoaColour;
	COLORREF lmEtoaColour;
	COLORREF lmTmoaColour;
	COLORREF lmLandColour;
	COLORREF lmOnblColour;
	COLORREF lmStodColour;
	COLORREF lmEtodColour;
	COLORREF lmSlotColour;
	COLORREF lmOfblColour;
	COLORREF lmAirbColour;

	CCSButtonCtrl omStoaButton;
	CCSButtonCtrl omEtoaButton;
	CCSButtonCtrl omTmoaButton;
	CCSButtonCtrl omLandButton;
	CCSButtonCtrl omOnblButton;
	CCSButtonCtrl omStodButton;
	CCSButtonCtrl omEtodButton;
	CCSButtonCtrl omSlotButton;
	CCSButtonCtrl omOfblButton;
	CCSButtonCtrl omAirbButton;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(StaffDiagramArrDepPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(StaffDiagramArrDepPage)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnStoaColour();
	afx_msg void OnEtoaColour();
	afx_msg void OnTmoaColour();
	afx_msg void OnOnblColour();
	afx_msg void OnLandColour();
	afx_msg void OnStodColour();
	afx_msg void OnEtodColour();
	afx_msg void OnSlotColour();
	afx_msg void OnOfblColour();
	afx_msg void OnAirbColour();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	HBRUSH SetColour(CDC* pDC, CWnd* pWnd, long lpControlId, COLORREF lpColour);
	void ChangeColour(long lpControlId, COLORREF &ropColour);

	void SetColour(CCSButtonCtrl &ropButton, COLORREF lpColour);
	void ChangeColour(CCSButtonCtrl &ropButton, COLORREF &ropColour);
};

#endif // _StaffDIAARRDEP_H_
