#if !defined(AFX_CCIDEMANDDETAILDLG_H__CA3D3C53_13B9_11D5_80E4_00010215BFDE__INCLUDED_)
#define AFX_CCIDEMANDDETAILDLG_H__CA3D3C53_13B9_11D5_80E4_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CciDemandDetailDlg.h : header file
//

#include <CcsTable.h>

typedef struct DemandDataStruct	DEMANDDATA;
typedef struct FlightDataStruct	FLIGHTDATA;

/////////////////////////////////////////////////////////////////////////////
// CciDemandDetailDlg dialog

class CciDemandDetailDlg : public CDialog
{
// Construction
public:
	CciDemandDetailDlg(CWnd* pParent,long lpDemUrno);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CciDemandDetailDlg)
	enum { IDD = IDD_CCI_DEMANDDETAIL };
	CString	m_RuleName;
	CString	m_Counter;
	CString	m_FromDate;
	CString	m_FromTime;
	CString	m_Functions;
	CString	m_Service;
	CString	m_ToDate;
	CString	m_Qualifications;
	CString	m_ToTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CciDemandDetailDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CciDemandDetailDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSchlieben();
	afx_msg void OnClose();
	afx_msg void OnDestroy();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
    void		UpdateDisplay();
	CString		Format(FLIGHTDATA *prpFlight);
	CCSTable	omTable1, omTable2;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeader;
	CCSPtrArray <TABLE_COLUMN> omColumns;
	void UpdateTable(DEMANDDATA *prpDemand);
	void CreateTableColumnsAndHeader();
	long		lmDemUrno;	
	void		SetTitle(int ipControlId, int ipStringId);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCIDEMANDDETAILDLG_H__CA3D3C53_13B9_11D5_80E4_00010215BFDE__INCLUDED_)
