#if !defined(AFX_TELEXTYPEDLG_H__AEBD5DF6_D5B4_47B2_8C44_4029702186E2__INCLUDED_)
#define AFX_TELEXTYPEDLG_H__AEBD5DF6_D5B4_47B2_8C44_4029702186E2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TelexTypeDlg.h : header file
//
#define TELEXTYPE_KEY "TelexTypes"
#define TELEXTYPE_TEXT "Load"

/////////////////////////////////////////////////////////////////////////////
// CTelexTypeDlg dialog

class CTelexTypeDlg : public CDialog
{
// Construction
public:
	CTelexTypeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTelexTypeDlg)
	enum { IDD = IDD_TELEXTYPEDLG };
	CListBox	m_TelexTypeListCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTelexTypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTelexTypeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TELEXTYPEDLG_H__AEBD5DF6_D5B4_47B2_8C44_4029702186E2__INCLUDED_)
