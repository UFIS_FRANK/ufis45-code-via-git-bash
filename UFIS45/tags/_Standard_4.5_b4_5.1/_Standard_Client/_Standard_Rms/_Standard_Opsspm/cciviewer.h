// CciViewer.h : header file
//

#ifndef _CCIVIEWER_H_
#define _CCIVIEWER_H_

#include <cviewer.h>
#include <ccsprint.h>
#include <CedaDemandData.h>
#include <CedaCcaData.h>
#include <CedaBlkData.h>
#include <CedaCicData.h>
#include <CedaJodData.h>
#include <CedaRudData.h>
#include <CedaRueData.h>
#include <CedaShiftData.h>
#include <DataSet.h>

struct CCI_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};
struct CCI_BARDATA 
{
    // standard data for bars in general GanttLine
	long JobUrno;
	long FlightUrno;
	bool IsDemand;
    CString Text;
	CString StatusText;		// long text used to be displayed at the status bar
    CTime StartTime;
    CTime EndTime;
    int FrameType;
	COLORREF FrameColor;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	bool Infm; // employee informed
	bool OfflineBar;
	bool DifferentFunction;

	CCSPtrArray<CCI_INDICATORDATA> Indicators;

	CCI_BARDATA(void)
	{
		JobUrno = 0L;
		IsDemand = false;
		DifferentFunction = false;
		OverlapLevel = 0;
	}
};


struct CCI_BKBARDATA
{
	enum	BKBARTYPE
	{
		NONE		= -1,
		BLOCKED		= 0,
		DEDICATED	= 1,
		COMMON		= 2,
		CCA			= 3,	// the sum of DEDICATED + COMMON
		PERSONNEL	= 4,
	};

	BKBARTYPE		Type;
    CString			Text;
    CString			StatusText;
    CTime			StartTime;
    CTime			EndTime;
    CBrush*			MarkerBrush;
	int				OverlapLevel;
	int NumBars; // number of bars assigned to this background bar
	char			Dety[2];
	long			Uaft;
	long			Ualt;
	long			Urno;
	long			Urue;
	long			Udem;
	long			Ulnk;
	COLORREF		FrameColor;

	CCI_BKBARDATA(void)
	{
		FrameColor = BLACK;
		NumBars = 0;
	}
};

struct CCI_LINEDATA 
{
	// standard data for lines in general Diagram's chart
    CString Text;
	long CicUrno;
	CString StatusText;
	CString GroupName;
	CString Telno;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
    CCSPtrArray<CCI_BARDATA>   Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<CCI_BKBARDATA> BlkBkBars;	// Blocked time background bar
    CCSPtrArray<CCI_BKBARDATA> CcaBkBars;	// Cca (location demand) background bar
    CCSPtrArray<CCI_BKBARDATA> DemBkBars;	// Personnel demand background bar
    CCSPtrArray<int>		   TimeOrder;	// maintain overlapped bars, see below
};
struct CCI_MANAGERDATA
{
	long JobUrno;			// from JOBCKI.URNO (of the job for FM/AM)
	// these data are for displaying the TopScaleText for each group in CCIDiagram
	CString ShiftStid;
	CString ShiftTmid;
	CString EmpLnam;
};
struct CCI_GROUPDATA 
{
	CString GroupId;
	CString Alid;			// handy used for dragging and dropping operation
    CCSPtrArray<CCI_MANAGERDATA> Managers;
	// standard data for groups in general Diagram
    CString Text;
    CCSPtrArray<CCI_LINEDATA> Lines[2];	// both left side and right side
};

struct CCI_EXTRAJOB
{
	CString Text;
	CString SortString;
	bool DrawLine;

	CCI_EXTRAJOB(void)
	{
		DrawLine = false;
	}
};

#define CCIVIEWER_FID_PRINT_LEN 22

// struct used for printing staff assignments
struct CCI_PRINTSHIFTS 
{
	SHIFTDATA	*prlShift;
	CString		olMainFunction;
	int			ilMainFunctionIndex; // number denoting the order in which functions were selected in the view (used for sorting)
	CCSPtrArray <JOBDATA> olFlightJobs;
	CCSPtrArray <JOBDATA> olCommonCheckinJobs;
	CCSPtrArray <CCI_EXTRAJOB> olFlightIndependentJobs;
	CCSPtrArray <CCI_EXTRAJOB> olOtherJobs;
	CMapPtrToPtr AllJobs; // used to prevent jobs being added twice (happens when counter displayed twice in different groups)

	CCI_PRINTSHIFTS(void)
	{
		prlShift = NULL;
		ilMainFunctionIndex = -1;
	}

	~CCI_PRINTSHIFTS()
	{
		while(this->olFlightIndependentJobs.GetSize() > 0)
			this->olFlightIndependentJobs.DeleteAt(0);
	}

	bool CCI_PRINTSHIFTS::AddJob(JOBDATA *prpJob, CCSPtrArray <JOBDATA> &ropJobs)
	{
		void *pvlDummy = NULL;
		if(!this->AllJobs.Lookup((void *) prpJob->Urno, (void *&) pvlDummy))
		{
			this->AllJobs.SetAt((void *) prpJob->Urno, NULL);
			ropJobs.Add(prpJob);
			return true;
		}
		return false;
	}

	bool CCI_PRINTSHIFTS::AddJob(JOBDATA *prpJob, CCSPtrArray <CCI_EXTRAJOB> &ropJobs)
	{
		void *pvlDummy = NULL;
		if(!this->AllJobs.Lookup((void *) prpJob->Urno, (void *&) pvlDummy))
		{
			this->AllJobs.SetAt((void *) prpJob->Urno, NULL);
			CString olJobText = ogDataSet.JobBarText(prpJob);
			int ilLineNum = 0, ilTextLen = olJobText.GetLength();
			for(int i = 0; i < ilTextLen; i += CCIVIEWER_FID_PRINT_LEN)
			{
				CCI_EXTRAJOB *prlExtraJob = new CCI_EXTRAJOB;
				prlExtraJob->Text = olJobText.Mid(i,CCIVIEWER_FID_PRINT_LEN);
				prlExtraJob->SortString.Format("%s%d%03d", prpJob->Acfr.Format("%Y%m%d%H%M%S"), prpJob->Urno, ilLineNum++);

				if((i + CCIVIEWER_FID_PRINT_LEN) >= ilTextLen)
					prlExtraJob->DrawLine = true;

				ropJobs.Add(prlExtraJob);
			}
			return true;
		}
		return false;
	}
};

// Attention:
// Element "TimeOrder" in the struct "CCI_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array CCI_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the CCI_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array CCI_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer window

class CciGantt;

class CciDiagramViewer: public CViewer
{
	friend CciGantt;

// Constructions
public:
    CciDiagramViewer();
    ~CciDiagramViewer();

	void Attach(CWnd *popAttachWnd);
	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);
	void UpdateManagers(CTime opStartTime, CTime opEndTime);

// Internal data processing routines
private:
	static void CciDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();

	BOOL IsPassFilter(const char *pcpTerminal);
	BOOL IsPassFilter(const char *pcpTerminal, const char *pcpHall,const char *pcpRegion,const char *pcpLine);
	BOOL IsPassFilter(JOBDATA *prpJob);
	BOOL IsPassFilter(DEMANDDATA *prpDemand);
	BOOL IsPassFilter(CCADATA *prpCca);

	int CompareGroup(CCI_GROUPDATA *prpGroup1, CCI_GROUPDATA *prpGroup2);
	int CompareLine(CCI_LINEDATA *prpLine1, CCI_LINEDATA *prpLine2);
	int CompareManager(CCI_MANAGERDATA *prpManager1, CCI_MANAGERDATA *prpManager2);
	BOOL AssignDemToCca(DEMANDDATA *prpDemand, int &ripGroup, int &ripSide, int &ripLine);

	void MakeGroupsAndLines();
	void MakeManagers();
	void MakeLines(int ipGroupno);
	void MakeBars();
	void MakeBackgroundBars();
	void MakeLinesWhenGroupByTerminal(int ipGroupno);
	void MakeLinesWhenGroupByHall(int ipGroupno);
	void MakeLinesWhenGroupByRegion(int ipGroupno);
	void MakeLinesWhenGroupByLine(int ipGroupno);
	void MakeManager(int ipGroupno, JOBDATA *prpJob);
	void MakeBar(int ipGroupno, int ipSide, int ipLineno, JOBDATA *prpJob, int ipOverlapLevel = -1);

	void MakeLinesForDemandsWithoutCounter();
	int	 MakeLineForDemandWithoutCounter(DEMANDDATA *prpDem);

	CCI_BKBARDATA *MakeCcaBkBar(int ipGroupno, int ipSide, int ipLineno, CCADATA *prpCca);
	CCI_BKBARDATA *MakeCcaBkBar(CCI_LINEDATA *prpLine, CCADATA *prlCca);

	CCI_BKBARDATA *MakeDemBkBar(int ipGroupno, int ipSide, int ipLineno, DEMANDDATA *prpDem, int ipOverlapLevel = -1);
	CCI_BKBARDATA *MakeDemBkBar(CCI_LINEDATA *prpLine, DEMANDDATA *prpDem, int ipOverlapLevel = -1);

	void		   MakeBlkBkBar(CCI_LINEDATA *prpLine, BLKDATA *prlBlk);
	CCI_BKBARDATA *MakeBlkBkBar(CCI_LINEDATA *prpLine,const CTime& ropFrom,const CTime& ropTo,const CString& ropReason);

	CCI_BKBARDATA *FindCcaBkBarForDemBkBar(CCI_LINEDATA *prpLine, CCI_BKBARDATA *prpDemBkBar);
	void MakeDemBkBarForCcaBkBar(CCI_LINEDATA *prpLine, CCI_BKBARDATA *prpCcaBkBar);
	void MakeDemandBar(int ipGroupno, int ipSide, int ipLineno, DEMANDDATA *prpDemand);

	BOOL FindGroup(const char *pcpGroupId, int &ripGroupno);
	BOOL FindFreeDeskForUaftUrue(long lpUaft, long lpUrue, int &rilGroup, int &rilSide, int &rilLine);
	BOOL FindManager(long lpUrno, int &ripGroupno, int &ripManagerno);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripSide, int &ripLineno, int &ripBarno);

	BOOL FindCcaBkBar(long lpUrno, int &ripGroupno, int &ripSide, int &ripLineno, int &ripBarno);
	BOOL FindDemBkBar(long lpUrno, int &ripGroupno, int &ripSide, int &ripLineno, int &ripBarno);

	CString BarText(JOBDATA *prpJob);
	CString StatusText(JOBDATA *prpJob);

// Operations
public:
    int					GetGroupCount();
    CCI_GROUPDATA*		GetGroup(int ipGroupno);
    CString				GetGroupText(int ipGroupno);
    CString				GetGroupTopScaleText(int ipGroupno);
	int					GetManagerCount(int ipGroupno);
    CCI_MANAGERDATA*	GetManager(int ipGroupno, int ipManagerno);
	int					GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
    int					GetLineCount(int ipGroupno);
    CCI_LINEDATA*		GetLine(int ipGroupno, int ipSideno, int ipLineno);
    CString				GetLineText(int ipGroupno, int ipSideno, int ipLineno);
    int					GetMaxOverlapLevel(int ipGroupno, int ipSideno, int ipLineno);
    int					GetBarCount(int ipGroupno, int ipSide, int ipLineno);
    CCI_BARDATA*		GetBar(int ipGroupno, int ipSide, int ipLineno, int ipBarno);
    CString				GetBarText(int ipGroupno, int ipSide, int ipLineno, int ipBarno);
	int					GetBlkBkBarCount(int ipGroupno, int ipSide, int ipLineno);
	CCI_BKBARDATA*		GetBlkBkBar(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno);
	int					GetCcaBkBarCount(int ipGroupno, int ipSide, int ipLineno);
	CCI_BKBARDATA*		GetCcaBkBar(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno);
	CString				GetCcaBkBarText(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno);
	int					GetDemBkBarCount(int ipGroupno, int ipSide, int ipLineno);
	CCI_BKBARDATA*		GetDemBkBar(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno);
	CString				GetDemBkBarText(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno);
	int					GetIndicatorCount(int ipGroupno, int ipSide, int ipLineno, int ipBarno);
	CCI_INDICATORDATA*	GetIndicator(int ipGroupno, int ipSide, int ipLineno, int ipBarno,int ipIndicatorno);

	CTimeSpan om2Mins;

    void				DeleteAll();
    int					CreateGroup(CCI_GROUPDATA *prpGroup);
    void				DeleteGroup(int ipGroupno);
	CCSReturnCode		CreateManagersForGroup(int ipGroupNo);
	void				DeleteCcaBkBar(int ipGroupno, int ipSideno, int ipLineno, int ipBkBarno);
	void				DeleteDemBkBar(int ipGroupno, int ipSideno, int ipLineno, int ipBkBarno);
	int					CreateManager(int ipGroupno, CCI_MANAGERDATA *prpManager);
	void				DeleteManager(int ipGroupno, int ipManagerno);
    int					CreateLine(int ipGroupno, CCI_LINEDATA *prpLine);
    void				DeleteLine(int ipGroupno, int ipLineno);
    int					CreateBar(int ipGroupno, int ipSide, int ipLineno, CCI_BARDATA *prpBar,BOOL bpFrontBar = TRUE);
    void				DeleteBar(int ipGroupno, int ipSide, int ipLineno, int ipBarno);
    int					CreateIndicator(int ipGroupno, int ipSide, int ipLineno, int ipBarno,CCI_INDICATORDATA *prpIndicator);
	void				DeleteIndicator(int ipGroupno, int ipSide, int ipLineno, int ipBarno,int ipIndicator);

    int					GetBarnoFromTime(int ipGroupno, int ipSide, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	int					GetCcaBkBarnoFromTime(int ipGroupno, int ipSide, int ipLineno,	CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	int					GetDemBkBarnoFromTime(int ipGroupno, int ipSide, int ipLineno,	CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	int					GetBlkBkBarnoFromTime(int ipGroupno, int ipSide, int ipLineno,	CTime opTime1, CTime opTime2);
	bool				HasOverlappedBkBars(CCI_LINEDATA *prpLine, CTime opStartTime, CTime opEndTime, int ipLine);

	void				GetDemandList(CUIntArray& ropDemandList);

	CTime				GetStartTime();
	CTime				GetEndTime();
	void				GetGroupNames(CStringArray& ropGroupNames);
	void				GetCountersByGroup(const CString& ropGroupName,CCSPtrArray<CICDATA>& ropCicList); 
	void				SortCounters(CCSPtrArray<CICDATA>& ropCicList);
	BOOL				FindDesk(CString opDesk, int &rilGroupno, int &rilSide, int &rilLineno);
	bool				FindCounter(CString opCounter, int &ripGroup, int &ripSide, int &ripLine);

	BOOL				IsGroupWithoutCounter(int ipGroupno);
	int					GetGroupWithoutCounter();

// Private helper routines
private:
	void GetOverlapLevelForBkBar(CCI_LINEDATA *prpLine, CCI_BKBARDATA *prpNewBkBar);
	void GetOverlappedBarsFromTime(int ipGroupno, int ipSideno, int ipLineno, CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	void GetOverlappedBarsFromTime(CCI_LINEDATA *prpLine, CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	void GetOverlappedBkBarsFromTime(CCSPtrArray <CCI_BKBARDATA> &ropBkBars, CTime opTime1, CTime opTime2, CCSPtrArray <CCI_BKBARDATA> &ropSelectedBkBars);

// Attributes used for filtering condition
private:
	CBrush			omDedicatedCcaBkBrush;
	CBrush			omCommonCcaBkBrush;
	CBrush			omDemandBrush;
	CBrush			omBlockedBrush;

	int				omGroupBy;			// enumerated value -- define in "cciviewer.cpp"
	CMapStringToPtr omCMapForTerminal;
	CMapStringToPtr omCMapForHall;
	CMapStringToPtr omCMapForRegion;
	CMapStringToPtr	omCMapForLine;
	CMapStringToPtr omCMapForAirline;
	CTime			omStartTime;
	CTime			omEndTime;
	bool			bmUseAllAirlines;
	CMapStringToPtr omCMapForFunctions;
	bool			bmUseAllFunctions;

// Attributes
private:
    CCSPtrArray<CCI_GROUPDATA>	omGroups;
	int							imDemandsWithoutCounterGroup;

// Methods for co-working with the CCI Chart (to check if it's groupped by Terminal or not)
public:
	BOOL IsGrouppedByTerminal();
	CWnd *pomAttachWnd;

// Methods which handle changes (from Data Distributor)
private:
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessJobDelete(JOBDATA *prpJob);
	void ProcessJodChange(JODDATA *prpJod);
	void ProcessFmcJobNew(JOBDATA *prpJob);
	void ProcessFmcJobDelete(JOBDATA *prpJob);
	void ProcessCcaChange(CCADATA *prpCca);
	void ProcessCcaDelete(long lpCcaUrno);
	void ProcessDemandChange(DEMANDDATA *prpDem);
	void ProcessDemandDelete(DEMANDDATA *prpDemand);
	void ProcessJodNew(JODDATA *prpJod);

///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:
	CCSPrint *pomPrint;

	void PrintCciDiagramHeader(int ipGroupno);
	BOOL PrintPrepareLineData(int ipGroupNo,int ipSideNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine);
	void PrintManagers(int ipGroupno);
	void PrintCciArea(CPtrArray &opPtrArray,int ipGroupNo);
	BOOL bmIsFirstGroupOnPage;

public:
	void PrintDiagram(CPtrArray &opPtrArray);
	CString GetRuleEventText(long lpRueUrno);
	CString GetRuleEventText(RUEDATA *prpRue);
	CString GetLineForFlight(long lpFlightUrno);

	void PrintView(CTime opFrom, CTime opTo, bool bpSortByShift, bool bpSortByStd, bool bpDisplaySta, bool bpDisplayEta, bool bpDisplayStd, bool bpDisplayEtd, bool bpDisplayGate, bool bpDisplayArr);
	bool PrintStartOfPage(CCSPtrArray <FLIGHTDATA> &ropFlights, int ipMaxFlightsPerLine, bool bpDisplaySta, bool bpDisplayEta, bool bpDisplayStd, bool bpDisplayEtd, bool bpDisplayGate, bool bpDisplayArr);
	void PrintEndOfPage();
	BOOL PrintHeader(CCSPrint *pomPrint);
	int imTotalLen,imCharWidth,imFuncLen,imNameLen,imShiftLen,imBreakLen,imFlightLen,imFidLen,imOtherLen;
	void AddPrintElement(CCSPtrArray <PRINTELEDATA> &ropPrintLine, CString &ropText, int ipLength, 
							CFont &ropFont, int ipAlignment = PRINT_LEFT, 
							int ipFrameLeft = PRINT_FRAMEMEDIUM, int ipFrameRight = PRINT_FRAMEMEDIUM, 
							int ipFrameTop = PRINT_FRAMEMEDIUM, int ipFrameBottom = PRINT_FRAMEMEDIUM);
	bool NoMoreJobsToPrint(CCI_PRINTSHIFTS *prpPrintShift);
};

/////////////////////////////////////////////////////////////////////////////

#endif
