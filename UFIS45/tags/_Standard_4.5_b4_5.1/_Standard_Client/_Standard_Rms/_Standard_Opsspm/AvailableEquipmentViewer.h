///////////////////////////////////////////////////
//

#ifndef _AVAILABLE_EQUIPMENT_VIEWER_
#define _AVAILABLE_EQUIPMENT_VIEWER_

#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <cviewer.h>
#include <table.h>



struct AVAILABLE_EQU_LINEDATA
{
	bool	IsCorrectEquForDemand;
	long	Urno;					// EQU.URNO
	char	Enam[41];				// name/number
	char	Etyp[41];				// type/manufacturer
	char	Gcde[6];				// code
	CString FastLinkEmps;
	CString EquipAttribute;			//Equipment Attribute

	AVAILABLE_EQU_LINEDATA(void)
	{
		IsCorrectEquForDemand = false;
		Urno = 0L;
		strcpy(Etyp,"");
		strcpy(Gcde,"");
	}
};

class AvailableEquipmentViewer : public CViewer
{
public:
	AvailableEquipmentViewer(CCSPtrArray<AVAILABLE_EQU_LINEDATA> *popAvailableEquipment);
	~AvailableEquipmentViewer();

	CCSPtrArray<AVAILABLE_EQU_LINEDATA> *pomAvailableEquipment;
    void Attach(CTable *popAttachWnd);

private:
    void MakeLines();
	void MakeLine(AVAILABLE_EQU_LINEDATA  *prpAvailable);
	int CreateLine(AVAILABLE_EQU_LINEDATA*prpAvailable);
	int CompareAvailable(AVAILABLE_EQU_LINEDATA *prpAvailable1, AVAILABLE_EQU_LINEDATA *prpAvailable2);
	bool bmUseAllEquipmentGroups, bmUseAllEquipmentTypes, bmUseAllEquipmentNames;
	CMapStringToPtr omEquipmentGroupMap, omEquipmentTypeMap, omEquipmentNamesMap;
	CString omGroupBy;
	void PrepareFilter(void);
	bool IsPassFilter(AVAILABLE_EQU_LINEDATA *prpLine);

public:
	void DeleteAll();
	void DeleteLine(int ipLineno);

public:
	void UpdateView(const char *pcpViewName);
	void UpdateDisplay();
	CString Format(AVAILABLE_EQU_LINEDATA *prpLine);
	void ProcessShiftChange(SHIFTDATA *prpShift);
	void ProcessShiftChange(JOBDATA *prpJob);
	int GetLineCount();

// Attributes used for filtering condition
private:
//    CString omDate;
//    CTime omStartTime;
//	CTime omEndTime;
// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<AVAILABLE_EQU_LINEDATA> omLines;

// Methods which handle changes (from Data Distributor)
public:

};
#endif