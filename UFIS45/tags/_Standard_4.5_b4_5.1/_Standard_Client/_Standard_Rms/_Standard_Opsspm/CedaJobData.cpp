#include <stdlib.h>
#include <string.h>
#include <afxwin.h>
#include <ccsGlobl.h>
#include <resource.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaDemandData.h>
#include <CedaJobData.h>
#include <CedaEmpData.h>
#include <CedaFlightData.h>
#include <CedaJodData.h>
#include <table.h>
#include <BasicData.h>
#include <CCSBcHandle.h>
#include <CedaShiftData.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <CedaCfgData.h>
#include <CCSCedaCom.h>
#include <CedaJtyData.h>
#include <DataSet.h>
#include <CedaAloData.h>
#include <waitdlg.h>
#include <ButtonList.h>
#include <CedaRpfData.h>
#include <CedaRudData.h>
#include <CedaTplData.h> //Singapore
#include <PrmConfig.h>

#define USE_RELEASE_HANDLER
//#undef USE_RELEASE_HANDLER

static JODDATA omJod; // required to mimic old style JOD_CHANGE, JOD_NEW, JOD_DELETE


enum enumRecordState; 
extern CCSCedaCom ogCommHandler;

static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2);
static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2)
{
	return (int)((**pppJob1).Acfr.GetTime() - (**pppJob2).Acfr.GetTime());
}

void  ProcessJobCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
void ProcessJobCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_JOB_NEW:
		((CedaJobData *)popInstance)->ProcessJobInsert(vpDataPointer);
		break;
	case BC_JOB_CHANGE:
		((CedaJobData *)popInstance)->ProcessJobUpdate(vpDataPointer);
		break;
	case BC_JOB_DELETE:
		((CedaJobData *)popInstance)->ProcessJobDelete(vpDataPointer);
		break;
	case BC_JOB_REL:
		((CedaJobData *)popInstance)->ProcessJobRelease(vpDataPointer);
		break;
	case BC_JOB_UPD:
		((CedaJobData *)popInstance)->ProcessJobsUpdate(vpDataPointer);
		break;
	case BC_AFLEND_SBC:
		((CedaJobData *)popInstance)->ProcessAflEnd(vpDataPointer);
		break;
	case FLIGHT_CHANGE:
		((CedaJobData *)popInstance)->ProcessFlightChange((FLIGHTDATA *) vpDataPointer);
		break;
	case FLIGHT_DELETE:
		((CedaJobData *)popInstance)->ProcessFlightChange((long) vpDataPointer);
		break;
	default:
		break;
	}
}

void CedaJobData::ProcessFlightChange(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL)
	{
		ProcessFlightChange(prpFlight->Urno);
	}
}

void CedaJobData::ProcessFlightChange(long lpFlightUrno)
{
	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByFlur(olJobs, lpFlightUrno);
	for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
	{
		JOBDATA *prlJob = &olJobs[ilJ];
		ogCCSDdx.DataChanged((void *)this,JOB_CHANGE,(void *)prlJob);
	}
}

void CedaJobData::ProcessJobInsert(void *vpDataPointer)
{
	struct BcStruct *prlJobData;
	prlJobData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJobData);

	long llUrno = GetUrnoFromSelection(prlJobData->Selection);
	if(llUrno == 0L)
	{
		JOBDATA rlJob;
		GetRecordFromItemList(&omRecInfo,&rlJob,prlJobData->Fields,prlJobData->Data);
		llUrno = rlJob.Urno;
	}

	if(llUrno != 0L)
	{
		ProcessJobInsert(prlJobData->Fields, prlJobData->Data);
	}
}

void CedaJobData::ProcessJobInsert(char *pcpFields, char *pcpData)
{
	JOBDATA *prlJob = AllocNewJob(NULL);
	GetRecordFromItemList(&omRecInfo, prlJob, pcpFields, pcpData);
	if (ogBasicData.IsPrmHandlingAgentEnabled())
	{
		if (strcmp(prlJob->Filt,ogPrmConfig.getHandlingAgentShortName()))
		{
			return;
		}
	}

	if(!AddIncomingJob(prlJob))
	{
		delete prlJob;
		prlJob = NULL;
	}
	else
	{
		ogFlightData.HandleMissingFlight(prlJob->Flur);
		ogConflicts.CheckConflicts(*prlJob,FALSE,FALSE,TRUE);
		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
		if (prlFlight != NULL)
		{
			ogConflicts.CheckConflicts(*prlFlight, FALSE, TRUE, TRUE, FALSE);
			//ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
		}
		if(!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBTEMPABSENCE))
		{
			CCSPtrArray<JOBDATA> olStfJobs;
			GetJobsByUstf(olStfJobs,prlJob->Ustf);
			int ilNumStfJobs = olStfJobs.GetSize();
			for(int ilStfJob = 0; ilStfJob < ilNumStfJobs; ilStfJob++)
			{
				ogConflicts.CheckConflicts(olStfJobs[ilStfJob], FALSE, TRUE, TRUE, FALSE);
			}
		}
//		if(ogJodData.bmUseJodtab && prlJob->Udem != 0L)
//		{
//			omJod.Ujob = prlJob->Urno;
//			omJod.Udem = prlJob->Udem;
//			ogCCSDdx.DataChanged((void *)this, JOD_NEW, (void *)&omJod);
//		}

		ogCCSDdx.DataChanged((void *)this,JOB_NEW,(void *)prlJob);
		SendJodDdx(prlJob, NULL);
	}
}

void CedaJobData::CheckForMissingFlights(void)
{
	//omMissingFlights.RemoveAll();
	//omMissingFlights.RemoveAll();
	for(int i = 0; i < omData.GetSize(); i++)
	{
		long llFlur = omData[i].Flur;
		if(llFlur != 0L && ogFlightData.GetFlightByUrno(llFlur) == NULL)
		{
			ogFlightData.HandleMissingFlight(llFlur);
		}
		//CheckForMissingFlight(&omData[i]);
	}
	//ReadMissingFlights("Initial load");
}

void CedaJobData::CheckForMissingFlight(JOBDATA *prpJob, const char *pcpText /* = NULL */, bool bpReadMissingFlight /* = false */)
{
	if(prpJob != NULL && prpJob->Flur != 0L)
	{
		if(ogFlightData.GetFlightByUrno(prpJob->Flur) == NULL)
		{
			omMissingFlights.SetAt((void *) prpJob->Flur, NULL);
			omJobsWhichHaveMissingFlights.Add(prpJob->Urno);
		}
		if(bpReadMissingFlight)
		{
			ReadMissingFlights(pcpText);
		}
	}
}

void CedaJobData::ReadMissingFlights(const char *pcpText)
{
	if(omMissingFlights.GetCount() > 0)
	{
		CString olUrnos, olTmp, olWhere = "WHERE URNO IN (%s)", olComma = ",";
		char pclSelection[2500];
		void *pvlDummy = NULL;
		long llFlur = 0L;

		for(POSITION rlPos = omMissingFlights.GetStartPosition(); rlPos != NULL; )
		{
			omMissingFlights.GetNextAssoc(rlPos, (void *&) llFlur, (void *& ) pvlDummy);
			olTmp.Format("'%d'",llFlur);
			if(!olUrnos.IsEmpty()) olUrnos += olComma;
			olUrnos += olTmp;
			if(olUrnos.GetLength() > 2000)
			{
				ogBasicData.MarkTime();
				sprintf(pclSelection, olWhere, olUrnos);
				ogFlightData.ReadAllFlights(TIMENULL, TIMENULL, pclSelection);
				olUrnos.Empty();
				ogBasicData.SaveTimeTakenForReread(pcpText, pclSelection);
			}
		}
		if(!olUrnos.IsEmpty())
		{
			ogBasicData.MarkTime();
			sprintf(pclSelection, olWhere, olUrnos);
			ogFlightData.ReadAllFlights(TIMENULL, TIMENULL, pclSelection);
			ogBasicData.SaveTimeTakenForReread(pcpText, pclSelection);
		}

		// The following code TRACES the missing flight/Jobs
		JOBDATA *prlJob = NULL;
		FLIGHTDATA *prlFlight = NULL;
		CString olFlightTime;
		int ilNumJobs = omJobsWhichHaveMissingFlights.GetSize();
		for(int ilD = 0; ilD < ilNumJobs; ilD++)
		{
			if((prlJob = GetJobByUrno(omJobsWhichHaveMissingFlights[ilD])) != NULL)
			{
				if((prlFlight = ogFlightData.GetFlightByUrno(prlJob->Flur)) != NULL)
				{
					olFlightTime = (*prlFlight->Adid == 'A') ? prlFlight->Tifa.Format("%H:%M/%d") : prlFlight->Tifd.Format("%H:%M/%d");
//					ogBasicData.Trace("Read Missing Flight %s%s%s ADID <%s> TIME <%s> for Job DEBE <%s> DEEN <%s> <%s> <%s>",
//						prlFlight->Alc2, prlFlight->Fltn, prlFlight->Flns, prlFlight->Adid, olFlightTime,
//						prlJob->Debe.Format("%H:%M/%d"), prlJob->Deen.Format("%H:%M/%d"), GetStringForJobType(prlJob->Dety), prlJob->Aloc);
				}
			}
		}
		omMissingFlights.RemoveAll();
		omJobsWhichHaveMissingFlights.RemoveAll();
	}
}

void CedaJobData::CheckForMissingFlightJobs(CDWordArray &ropFlightUrnos)
{
	CString olData, olTmpStr;
	int ilNumFlightUrnos = ropFlightUrnos.GetSize();
	omTmpUrnoList.RemoveAll();
	for(int i = 0; i < ilNumFlightUrnos; i++)
	{
		if(!olData.IsEmpty())
			olData += ",";
		olTmpStr.Format("'%ld'", (long) ropFlightUrnos[i]);
		olData += olTmpStr;

		if((i != 0 && ((i+1) % 1000) == 0) || i == (ilNumFlightUrnos-1)) // max 1000 URNOs in a WHERE clause
		{
			int ilDataLen = olData.GetLength();
			char *pclUrnoList = new char[ilDataLen + 50];
			char *pclSelection = new char[ilDataLen + 50];
			memset(pclUrnoList, 0, sizeof(pclUrnoList));
			memset(pclSelection, 0, sizeof(pclSelection));

			strcpy(pclUrnoList, olData);

			sprintf(pclSelection, "WHERE UAFT IN (%s)", pclUrnoList);
			Read(pclSelection, false, false);
		
			delete [] pclUrnoList;
			delete [] pclSelection;
			olData.Empty();
		}
	}
}

void CedaJobData::ProcessJobUpdate(void *vpDataPointer)
{
	struct BcStruct *prlJobData;
	prlJobData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJobData);

	long llUrno = GetUrnoFromSelection(prlJobData->Selection);
	if(llUrno == 0L)
	{
		JOBDATA rlJob;
		GetRecordFromItemList(&omRecInfo,&rlJob,prlJobData->Fields,prlJobData->Data);
		llUrno = rlJob.Urno;
	}

	if(llUrno != 0L)
	{
		if(!ProcessJobUpdate(llUrno, prlJobData->Fields, prlJobData->Data))
		{
			JOBDATA rlJob;
			GetRecordFromItemList(&omRecInfo,&rlJob,prlJobData->Fields,prlJobData->Data);

			if(JobHasFullFieldList(&rlJob))
			{
				ProcessJobInsert(prlJobData->Fields, prlJobData->Data);
			}
			else if(JobHasCorrectTemplate(&rlJob) && ((rlJob.Acfr != TIMENULL && IsBetween(rlJob.Acfr, omStartTime, omEndTime)) ||
					(rlJob.Acto != TIMENULL && IsBetween(rlJob.Acto, omStartTime, omEndTime))))
			{
				ReadJob(llUrno);
				JOBDATA *prlJob = GetJobByUrno(llUrno);
				if(prlJob != NULL)
				{
					ogFlightData.HandleMissingFlight(prlJob->Flur);
				}
			}
		}
	}
}

bool CedaJobData::ProcessJobUpdate(long lpJobUrno, char *pcpFields, char *pcpData)
{
	bool blFound = false;

	JOBDATA *prlJob;
	if((prlJob = GetJobByUrno(lpJobUrno)) != NULL)
	{
		blFound = true;
		JOBDATA rlOldJob = *prlJob;

		// before reading the newly broadcasted data into the record found,
		// convert the existing dates to UTC so that PrepareDataAfterRead
		// converts them and any new dates from the broadcast to local
		ConvertDatesToUtc(prlJob);
		GetRecordFromItemList(&omRecInfo,prlJob, pcpFields, pcpData);
		PrepareDataAfterRead(prlJob);
		UpdateMaps(prlJob, &rlOldJob);
		UpdateOnlineData(prlJob);

		if(strcmp(prlJob->Jtco,JOBPOOL) && rlOldJob.Jour != prlJob->Jour)
		{
			// non-pool job and was reassigned from one pool to another
			CCSPtrArray<JOBDATA> olJobs;
			GetJobsByPoolJob(olJobs,rlOldJob.Jour);
			if (olJobs.GetSize() > 0)
			{
				ogConflicts.CheckConflicts(olJobs[0],FALSE,TRUE,FALSE,TRUE);
			}
			olJobs.RemoveAll();
		}
		if(!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBTEMPABSENCE))
		{
			// is a either a pool job or a temp absence
			CCSPtrArray<JOBDATA> olStfJobs;
			GetJobsByUstf(olStfJobs,prlJob->Ustf);
			int ilNumStfJobs = olStfJobs.GetSize();
			for(int ilStfJob = 0; ilStfJob < ilNumStfJobs; ilStfJob++)
			{
				ogConflicts.CheckConflicts(olStfJobs[ilStfJob],FALSE,TRUE,FALSE,FALSE);
			}
		}

		ogConflicts.CheckConflicts(*prlJob, FALSE, FALSE, FALSE, FALSE);
		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
		if (prlFlight != NULL)
		{
			ogConflicts.CheckConflicts(*prlFlight, FALSE, TRUE, TRUE, FALSE);
			//ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
		}
		ogCCSDdx.DataChanged((void *)this,JOB_CHANGE,(void *)prlJob);
		SendJodDdx(prlJob, &rlOldJob);
	}

	return blFound;
}

void CedaJobData::SendJodDdx(JOBDATA *prpJob, JOBDATA *prpOldJob)
{
	if(!ogJodData.bmUseJodtab)
	{
		if(prpJob != NULL && prpOldJob != NULL)
		{
			if(prpOldJob->Udem != prpJob->Udem)
			{
				if(prpOldJob->Udem == 0L && prpJob->Udem != 0L)
				{
					SendJodDdx(JOD_NEW, prpJob->Urno, prpJob->Udem);
				}
				else if(prpOldJob->Udem != 0L && prpJob->Udem == 0L)
				{
					SendJodDdx(JOD_DELETE, prpJob->Urno, prpOldJob->Udem);
				}
				else
				{
					SendJodDdx(JOD_DELETE, prpJob->Urno, prpOldJob->Udem);
					SendJodDdx(JOD_NEW, prpJob->Urno, prpJob->Udem);
				}
			}
		}
		else if(prpJob != NULL)
		{
			if(prpJob->Udem != 0L)
				SendJodDdx(JOD_NEW, prpJob->Urno, prpJob->Udem);
		}
		else if(prpOldJob != NULL)
		{
			if(prpOldJob->Udem != 0L)
				SendJodDdx(JOD_DELETE, prpOldJob->Urno, prpOldJob->Udem);
		}
	}
}

void CedaJobData::SendJodDdx(int ipDdxType, long lpUjob, long lpUdem)
{
	omJod.Ujob = lpUjob;
	omJod.Udem = lpUdem;
	ogCCSDdx.DataChanged((void *)this, ipDdxType,(void *)&omJod);
}

void CedaJobData::ProcessJobDelete(void *vpDataPointer)
{
	struct BcStruct *prlJobData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJobData);
	
	long llUrno = GetUrnoFromSelection(prlJobData->Selection);
	if(llUrno == 0L)
	{
		JOBDATA rlJob;
		GetRecordFromItemList(&omRecInfo,&rlJob,prlJobData->Fields,prlJobData->Data);
		llUrno = rlJob.Urno;
	}

	ProcessJobDelete(llUrno);
}

void CedaJobData::ProcessJobDelete(long lpJobUrno)
{
	JOBDATA *prlJob = NULL;
	if (omUrnoMap.Lookup((void *)lpJobUrno,(void *& )prlJob) == TRUE)
	{
		JOBDATA rlJob = *prlJob;

		CString olJtco(prlJob->Jtco);
		DeleteJob(rlJob.Urno,TRUE);
		DeleteFromOnlineData(rlJob.Urno);

		if (olJtco == JOBPOOL || olJtco == JOBTEMPABSENCE)
		{
			CCSPtrArray<JOBDATA> olStfJobs;
			GetJobsByUstf(olStfJobs,rlJob.Ustf);
			int ilNumStfJobs = olStfJobs.GetSize();
			for(int ilStfJob = 0; ilStfJob < ilNumStfJobs; ilStfJob++)
			{
				ogConflicts.CheckConflicts(olStfJobs[ilStfJob],FALSE,TRUE,FALSE,FALSE);
			}
		}
		SendJodDdx(NULL, &rlJob);
//		if(ogJodData.bmUseJodtab && rlJob.Udem != 0L)
//		{
//			omJod.Ujob = rlJob.Urno;
//			omJod.Udem = rlJob.Udem;
//			ogCCSDdx.DataChanged((void *)this, JOD_NEW, (void *)&omJod);
//		}
	}
}

// broadcast block update:
// selection format: "UJTY1,UJTY2,...,UJTYn"
// data list format: "FROM-TO,STFTAB.URNO1,...,STFTAB.URNOn"
// pool job changes released from polhdl - employee URNOs sent, 
// so delete and reload all pool jobs/break jobs etc on an employee basis
void CedaJobData::ProcessJobRelease(void *vpDataPointer)
{
	struct BcStruct *prlJobData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJobData);
	CString olData = prlJobData->Data;
	CString olSelection = prlJobData->Selection;

	MakeClientString(olData);

	olData.Replace('-',',');

	CStringArray olDataArray;
	ogBasicData.ExtractItemList(olData, &olDataArray);


	CTime olFrom, olTo;
	CString olDate;
	olDate = olDataArray[0];
	if(olDate.GetLength() == 8)
		olDate += "000000";
	StoreDate(olDate, (CTime *)&olFrom);

	olDate = olDataArray[1];
	if(olDate.GetLength() == 8)
		olDate += "235959";
	StoreDate(olDate, (CTime *)&olTo);

	if(olFrom == TIMENULL || olTo == TIMENULL || olFrom > olTo)
	{
		ogBasicData.Trace("Invalid SBC/RELJOB broadcast: Data must contain a date in the format FROM-TO\nData = <%s>", olData);
		return; // invalid data received
	}

	// dates within the timeframe
	if(IsOverlapped(olFrom, olTo, omStartTime, omEndTime))
	{
		if(!ProcessAttachment(prlJobData->Attachment))
		{
			// the selection contains the jobtypes to be reloaded
			WaitDlg olWaitDlg(NULL,GetString(IDS_JOBRELOAD));

			CUIntArray olJtyUrnos;
			ogBasicData.GetUrnoFromString(olJtyUrnos, olSelection);

			int ilMaxSelectionLen = (CJD_SELECTIONLEN - (olSelection.GetLength() + 120)) / 13;

			CUIntArray olStfUrnos;
			int ilNumData = olDataArray.GetSize();
			for(int ilD = 2; ilD < ilNumData; ilD++)
			{
				olStfUrnos.Add(atoi(olDataArray[ilD]));
				if(olStfUrnos.GetSize() >= ilMaxSelectionLen)
				{
					ReloadStaffJobs(olStfUrnos, olJtyUrnos);
					olStfUrnos.RemoveAll();
				}
			}
			if(olStfUrnos.GetSize() > 0)
			{
				ReloadStaffJobs(olStfUrnos, olJtyUrnos);
			}
			olWaitDlg.CloseWindow();
		}
	}
}

void CedaJobData::ReloadStaffJobs(CUIntArray &ropStfUrnos, CUIntArray &ropJtyUrnos)
{
	char pclSelection[CJD_SELECTIONLEN];
	CString olUstfList = ogBasicData.FormatUrnoList(ropStfUrnos); // format from CUIntArray to "'URNO1','URNO2',...,'URNOn'"
	CString olUjtyList = ogBasicData.FormatUrnoList(ropJtyUrnos); // format from CUIntArray to "'URNO1','URNO2',...,'URNOn'"

	int ilNumUjtys = ropJtyUrnos.GetSize();
	CCSPtrArray <JOBDATA> olJobs;
	int ilNumStfUrnos = ropStfUrnos.GetSize();
	for(int ilSU = 0; ilSU < ilNumStfUrnos; ilSU++)
	{
		GetJobsByUstf(olJobs, ropStfUrnos[ilSU]);
		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			JOBDATA *prlJob = &olJobs[ilJ];
			for(int ilJty = 0; ilJty < ilNumUjtys; ilJty++)
			{
				if(prlJob->Ujty == (long) ropJtyUrnos[ilJty])
				{
					ProcessJobDelete(prlJob->Urno);
					break;
				}
			}
		}
	}

	CString olSelection;
	olSelection.Format("WHERE USTF IN (%s) AND UJTY IN (%s) AND ACFR <= '%s' AND ACTO >= '%s'", 
		olUstfList, olUjtyList, omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"));
	if(olSelection.GetLength() >= CJD_SELECTIONLEN)
	{
		ogBasicData.Trace("Error in CedaJobData::ReloadStaffJobs() selection is too long: %d chars / max %d chars",olSelection.GetLength(),CJD_SELECTIONLEN);
	}
	else
	{
		ogBasicData.MarkTime();
		strcpy(pclSelection, olSelection);
		Read(pclSelection, false, true);
		ogBasicData.SaveTimeTakenForReread("RELJOB (Reload Pool Jobs)", pclSelection);
	}
}

// broadcast block update:
// data list format: "FROM,TO,DRT:n,URNO1,...,URNOn,UPD:n,URNO1,...,URNOn"
// job changes released from jobhdl
void CedaJobData::ProcessJobsUpdate(void *vpDataPointer)
{
	struct BcStruct *prlJobData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlJobData);
	CString olData = prlJobData->Data;
	int ilMaxSelectionLen = CJD_SELECTIONLEN - 40;

	// olData = "VON,BIS,DEL:n,URNO1,....,URNOn,UPD:n,URNO1,....,URNOn"
	MakeClientString(olData);

	CStringArray olDataList;
	ogBasicData.ExtractItemList(olData, &olDataList);

	int ilDrtCount = 0;
	bool blDelete = false, blUpdate = false;
	CTime olFrom, olTo;
	StoreDate(olDataList[0], (CTime *)&olFrom);
	StoreDate(olDataList[1], (CTime *)&olTo);
	// dates within the timeframe
	if(IsOverlapped(olFrom, olTo, omStartTime, omEndTime))
	{
		if(!ProcessAttachment(prlJobData->Attachment))
		{
			WaitDlg olWaitDlg(NULL,"Some jobs have been recalculated and are in the process of being reloaded.");

			CString olTmp1, olTmp2, olJobUrnoList;
			long llUrno;
			int ilNumItems = olDataList.GetSize();
			for(int ilItem = 2; ilItem < ilNumItems; ilItem++)
			{
				olTmp1 = olDataList[ilItem];
				if(olTmp1.Find("DEL:") != -1)
				{
					blDelete = true;
					blUpdate = false;
				}
				else if(olTmp1.Find("UPD:") != -1)
				{
					blDelete = false;
					blUpdate = true;
				}
				else if(blDelete)
				{
					llUrno = atol(olTmp1);
					ProcessJobDelete(llUrno);
					ilDrtCount++;
				}
				else if(blUpdate)
				{
					if(!olJobUrnoList.IsEmpty())
					{
						olJobUrnoList += ",";
					}
					olTmp2.Format("'%s'",olTmp1);
					olJobUrnoList += olTmp2;
					if(olJobUrnoList.GetLength() >= ilMaxSelectionLen)
					{
						ReReadJobs(olJobUrnoList);
						olJobUrnoList.Empty();
					}
				}
			}

			if(!olJobUrnoList.IsEmpty())
			{
				ReReadJobs(olJobUrnoList);
			}

			olWaitDlg.CloseWindow();
			ogBasicData.Trace("CedaJobData-ReRead: %d Deleted Records", ilDrtCount);
		}
	}
}

bool CedaJobData::ProcessAttachment(CString &ropAttachment)
{
	bool blSuccess = false;
	if(!ropAttachment.IsEmpty())
	{
		CAttachment olAttachment(ropAttachment);
		if(olAttachment.bmAttachmentValid)
		{
			blSuccess = true;
			for(int ilI = 0; ilI < olAttachment.omInsertDataList.GetSize(); ilI++)
			{
				ProcessJobInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omInsertDataList[ilI].GetBuffer(0));
			}

			for(int ilU = 0; ilU < olAttachment.omUpdateDataList.GetSize(); ilU++)
			{
				JOBDATA rlJob;
				GetRecordFromItemList(&omRecInfo,&rlJob, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				if(!ProcessJobUpdate(rlJob.Urno, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0)))
				{
					ProcessJobInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				}
			}

			for(int ilD = 0; ilD < olAttachment.omDeleteDataList.GetSize(); ilD++)
			{
				ProcessJobDelete(atol(olAttachment.omDeleteDataList[ilD]));
			}
		}
	}
	return blSuccess;
}

void CedaJobData::ReReadJobs(CString &ropJobUrnoList)
{
	ogBasicData.MarkTime();

	// Select data from the database
	int ilLc ;

	sprintf(cgSelection, "WHERE URNO IN (%s)", ropJobUrnoList);
	char pclCom[10] = "RT";
	if (CedaAction2(pclCom, cgSelection) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,cgSelection);
		return;
	}

	CUIntArray olAddedJobUrnos;
	CMapPtrToPtr olOldJobs;
	CUIntArray olNewJobs;
	bool blUpdate = false;
	bool blJobAdded = false;
	bool blRc = true;
	for (ilLc = 0; blRc; ilLc++)
	{
		blJobAdded = false;
		JOBDATA *prlNewJob = new JOBDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlNewJob,CString(pcmFieldList))) == RCSuccess)
		{
			JOBDATA *prlOldJob = GetJobByUrno(prlNewJob->Urno);
			if(prlOldJob != NULL)
			{
				olOldJobs.SetAt((void *) prlOldJob->Urno, NULL);
				DeleteJobInternal(prlOldJob->Urno);
			}
			if(AddIncomingJob(prlNewJob))
			{
				olNewJobs.Add(prlNewJob->Urno);
				blJobAdded = true;
				blUpdate = true;
			}

			if(!ogJodData.JobHasDemands(prlNewJob->Urno))
			{
				olAddedJobUrnos.Add(prlNewJob->Urno);
			}
		}

		if(!blJobAdded)
		{
			delete prlNewJob;
		}
	}


	if(blUpdate)
	{
		ogJodData.ReReadJods(olAddedJobUrnos);

		int ilUrtCount = 0, ilIrtCount = 0;

		JOBDATA *prlNewJob = NULL, *prlOldJob = NULL;
		int ilNumJobs = olNewJobs.GetSize();
		for(int ilD = 0; ilD < ilNumJobs; ilD++)
		{
			if((prlNewJob = GetJobByUrno(olNewJobs[ilD])) != NULL)
			{
				ogConflicts.CheckConflicts(*prlNewJob,FALSE,TRUE,TRUE,FALSE);
				if(olOldJobs.Lookup((void *) prlNewJob->Urno, (void *&) prlOldJob))
				{
					ilUrtCount++;
					ogCCSDdx.DataChanged((void *)this,JOB_CHANGE,(void *)prlNewJob);
				}
				else
				{
					ilIrtCount++;
					ogCCSDdx.DataChanged((void *)this,JOB_NEW,(void *)prlNewJob);
				}
				SendJodDdx(prlNewJob, prlOldJob);
			}
		}

		ogBasicData.Trace("CedaJobData-ReRead: %d Inserted Records %d Updated Records", ilIrtCount, ilUrtCount);
	}
	ogBasicData.SaveTimeTakenForReread("UPDJOB (job updates)", cgSelection);
}

void CedaJobData::SetAutoAssignInProgress()
{
	ogCCSDdx.DataChanged((void *)this, STARTASSIGNMENT, (void *) NULL);
	bmAutoAssignInProgress = true;
	//SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
}

void CedaJobData::ProcessAflEnd(void *vpDataPointer)
{
	struct BcStruct *prlJobData = (struct BcStruct *) vpDataPointer;

	if(!strcmp(prlJobData->ReqId,CCSCedaData::pcmReqId) && bmAutoAssignInProgress)
	{
		// force conflict check and diagram update
		pogButtonList->PostMessage(WM_TIMER,0,0L);	

		// "The demands have been successfully assigned and the update of the chart is in progress."
		pogButtonList->MessageBox(GetString(IDS_AUTOASSIGN_OK),GetString(IDS_STRING61934),MB_OK);

		bmAutoAssignInProgress = false;

		ogCCSDdx.DataChanged((void *) this, AFLEND_SBC, (void *) NULL);
        //SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

bool CedaJobData::NoAssignmentInProgress(CWnd *polWnd, bool bpDisplayMessage /* = true */)
{
	if(bmAutoAssignInProgress && bpDisplayMessage && polWnd != NULL)
	{
		// "Cannot assign demands automatically because another assignment is already in progress."
		polWnd->MessageBox(GetString(IDS_AUTOASSIGN_NOK),GetString(IDS_STRING61934));
	}

	return !bmAutoAssignInProgress;
}

CedaJobData::CedaJobData()
{
    // Create an array of CEDARECINFO for JOBDATA
    BEGIN_CEDARECINFO(JOBDATA, JobDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_LONG(Ustf,"USTF")
        FIELD_LONG(Jour,"JOUR")
        FIELD_DATE(Plfr,"PLFR")
        FIELD_DATE(Plto,"PLTO")
        FIELD_DATE(Acfr,"ACFR")
        FIELD_DATE(Acto,"ACTO")
        FIELD_CHAR_TRIM(Text,"TEXT")
        FIELD_LONG(Uaid,"UAID")
        FIELD_LONG(Ualo,"UALO")
        FIELD_LONG(Flur,"UAFT")
		FIELD_LONG(Shur,"UDSR")
		FIELD_LONG(Udel,"UDEL")
		FIELD_LONG(Udrd,"UDRD")
        FIELD_CHAR(DbStat,0,"STAT")
        FIELD_LONG(Ujty,"UJTY")
        FIELD_CHAR_TRIM(Usec,"USEC")
        FIELD_DATE(Cdat,"CDAT")
        FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_DATE(Lstu,"LSTU")
        FIELD_CHAR_TRIM(Regn,"REGN")
        FIELD_CHAR_TRIM(Act3,"ACT3")
        FIELD_CHAR_TRIM(Gate,"GATE")
        FIELD_CHAR_TRIM(Posi,"POSI")
        FIELD_CHAR_TRIM(Dety,"DETY")
        FIELD_LONG(Uequ,"UEQU")
        FIELD_LONG(Utpl,"UTPL")
        FIELD_CHAR_TRIM(Fcco,"FCCO")
        FIELD_LONG(Ughs,"UGHS")
        FIELD_LONG(Uprm,"UPRM")
        FIELD_CHAR_TRIM(Wgpc,"WGPC")
        FIELD_LONG(Udem,"UDEM")
        FIELD_CHAR_TRIM(Aloc,"ALOC")
        FIELD_CHAR_TRIM(Alid,"ALID")
		FIELD_LONG(Filt,"FILT")
	END_CEDARECINFO


    // Copy the record structure for JOBDATA
    for (int i = 0; i < sizeof(JobDataRecInfo)/sizeof(JobDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpRecInfo = new CEDARECINFO;
		memcpy(prpRecInfo,&JobDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpRecInfo);
	}

	// Initialize table names and field names
    strcpy(pcmTableName,"JOBTAB");

	bmIsOffline = false;
	bmTransactionActivated = false;
	bmAutoAssignInProgress = false;

	ogCCSDdx.Register((void *)this,BC_JOB_NEW,CString("JOBDATA"), CString("BC Job changed"),ProcessJobCf);
	ogCCSDdx.Register((void *)this,BC_JOB_CHANGE,CString("JOBDATA"), CString("BC Job changed"),ProcessJobCf);
	ogCCSDdx.Register((void *)this,BC_JOB_DELETE,CString("JOBDATA"), CString("BC Job deleted"),ProcessJobCf);
	ogCCSDdx.Register((void *)this,BC_JOB_REL,CString("JOBDATA"), CString("Pool Jobs Released"),ProcessJobCf);
	ogCCSDdx.Register((void *)this,BC_JOB_UPD,CString("JOBDATA"), CString("Jobs Auto Assign"),ProcessJobCf);
	ogCCSDdx.Register((void *)this,BC_AFLEND_SBC ,CString("JOBDATA"), CString("AFLEND"), ProcessJobCf);

//	ogCCSDdx.Register((void *)this, FLIGHT_CHANGE, CString("JOBDATA"), CString("Flight changed"), ProcessJobCf);
//	ogCCSDdx.Register((void *)this, FLIGHT_DELETE, CString("JOBDATA"), CString("Flight deleted"), ProcessJobCf);
}

void CedaJobData::PrepareFieldList()
{
	if(pcmFieldList == NULL)
	{
		pcmFieldList = new char[500];

		// pcmFieldList is used for selection records and will be appended with ",0" for each field that does not exist in SYSTAB
		// this results in "0" being written into all non-existant fields, however these zeros are not required for updates
		strcpy(pcmFieldList,"URNO,USTF,JOUR,PLFR,PLTO,ACFR,ACTO,TEXT,UAID,UALO,UAFT,UDSR,UDEL,UDRD,STAT,UJTY,USEC,CDAT,USEU,LSTU,REGN,ACT3,GATE,POSI,DETY");

		AddFieldIfItExists("UEQU");
		AddFieldIfItExists("UTPL");
		AddFieldIfItExists("FCCO");
		AddFieldIfItExists("UGHS");
		AddFieldIfItExists("UPRM");
		AddFieldIfItExists("WGPC");
		AddFieldIfItExists("UDEM");
		AddFieldIfItExists("ALOC");
		AddFieldIfItExists("ALID");
		AddFieldIfItExists("FILT");

		// get a list of fields which really exist
		JOBDATA rlJob;
		char pclData[2000];
		GetAllFields(&rlJob, pcmAllExistingFields, pclData);
	}
}

void CedaJobData::AddFieldIfItExists(const char *pcpField)
{
	if(ogBasicData.DoesFieldExist("JOB", pcpField) )
	{
		CString olField;
		olField.Format(",%s", pcpField);
		strcat(pcmFieldList, olField);
	}
	//else
	//{
		// the "0" is not needed anymore because not existing fields are no longer a problem. 
		// It is fixed in classlib (CCSCedaData::MakeCedaData)
	//	strcat(pcmFieldList,",0");
	//}
}

CedaJobData::~CedaJobData()
{
	ClearAll();
	omRecInfo.DeleteAll();
	if(pcmFieldList != NULL)
		delete pcmFieldList;
}

bool CedaJobData::ClearAll()
{
    omUrnoMap.RemoveAll();

	POSITION rlPos;

	CMapPtrToPtr *pomFlightJobMap;
	for ( rlPos =  omFlightMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omFlightMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )pomFlightJobMap);
		pomFlightJobMap->RemoveAll();
		delete pomFlightJobMap;
	}
	omFlightMap.RemoveAll();

	for ( rlPos =  omFlightFmMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omFlightFmMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )pomFlightJobMap);
		pomFlightJobMap->RemoveAll();
		delete pomFlightJobMap;
	}
	omFlightFmMap.RemoveAll();

	CMapPtrToPtr *polPknoJobMap;
	for(rlPos =  omPknoMap.GetStartPosition(); rlPos != NULL; )
	{
		CString olPkno;
		omPknoMap.GetNextAssoc(rlPos, olPkno,(void *& ) polPknoJobMap);
		polPknoJobMap->RemoveAll();
		delete polPknoJobMap;
	}
	omPknoMap.RemoveAll();

	CMapPtrToPtr *polUstfJobMap;
	long llUstf;
	for(rlPos = omUstfMap.GetStartPosition(); rlPos != NULL; )
	{
		omUstfMap.GetNextAssoc(rlPos,(void *&) llUstf,(void *& ) polUstfJobMap);
		polUstfJobMap->RemoveAll();
		delete polUstfJobMap;
	}
	omUstfMap.RemoveAll();

	CMapPtrToPtr *polUdemJobMap;
	long llUdem;
	for(rlPos = omUdemMap.GetStartPosition(); rlPos != NULL; )
	{
		omUdemMap.GetNextAssoc(rlPos,(void *&) llUdem,(void *& ) polUdemJobMap);
		polUdemJobMap->RemoveAll();
		delete polUdemJobMap;
	}
	omUdemMap.RemoveAll();

	CMapPtrToPtr *polUaidJobMap;
	long llUaid;
	for(rlPos = omUaidMap.GetStartPosition(); rlPos != NULL; )
	{
		omUaidMap.GetNextAssoc(rlPos,(void *&) llUaid,(void *& ) polUaidJobMap);
		polUaidJobMap->RemoveAll();
		delete polUaidJobMap;
	}
	omUaidMap.RemoveAll();

	if (bgIsPrm)
	{
		CMapPtrToPtr *polUprmJobMap;
		long llUprm;
		for(rlPos = omUprmMap.GetStartPosition(); rlPos != NULL; )
		{
			omUprmMap.GetNextAssoc(rlPos,(void *&) llUprm,(void *& ) polUprmJobMap);
			polUprmJobMap->RemoveAll();
			delete polUprmJobMap;
		}
		omUprmMap.RemoveAll();
	}
	CMapPtrToPtr *polJourJobMap;
	long llJour;
	for(rlPos = omJourMap.GetStartPosition(); rlPos != NULL; )
	{
		omJourMap.GetNextAssoc(rlPos,(void *&) llJour,(void *& ) polJourJobMap);
		polJourJobMap->RemoveAll();
		delete polJourJobMap;
	}
	omJourMap.RemoveAll();

	CMapPtrToPtr *polUequJobMap;
	long llUequ;
	for(rlPos = omUequMap.GetStartPosition(); rlPos != NULL; )
	{
		omUequMap.GetNextAssoc(rlPos,(void *&) llUequ,(void *& ) polUequJobMap);
		polUequJobMap->RemoveAll();
		delete polUequJobMap;
	}
	omUequMap.RemoveAll();

	CMapPtrToPtr *polShurJobMap;
	long llShur;
	for(rlPos = omShurMap.GetStartPosition(); rlPos != NULL; )
	{
		omShurMap.GetNextAssoc(rlPos,(void *&) llShur,(void *& ) polShurJobMap);
		polShurJobMap->RemoveAll();
		delete polShurJobMap;
	}
	omShurMap.RemoveAll();

    omData.DeleteAll();

    return true;
}

bool CedaJobData::ReadAllJobs(CTime opStartTime, CTime opEndTime)
{
 	CDWordArray olTplUrnos;
	ogBasicData.GetTemplateFilters(olTplUrnos);
	omUtplFilterMap.RemoveAll();
	for(int ilTpl = 0; ilTpl < olTplUrnos.GetSize(); ilTpl++)
	{
		omUtplFilterMap.SetAt((void *) olTplUrnos[ilTpl],NULL);
	}

	//Singapore
	if(ogBasicData.IsSinApron1() == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) == 0)
			{
				omUtplFilterMap.SetAt((void *)polTplData->Urno,NULL);
			}
		}
	}
	else if(ogBasicData.IsSinApron2() == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) == 0)
			{
				omUtplFilterMap.SetAt((void *)polTplData->Urno,NULL);
			}
		}
	}
    
	CTime olEndPlus2;
	omStartTime = opStartTime + CTimeSpan(0,0,0,1); // PRF 4824
	omEndTime = opEndTime - CTimeSpan(0,0,0,1); // PRF 4824
	olEndPlus2 = omEndTime + CTimeSpan(2,0,0,0);
	//sprintf(pclWhere, "WHERE ACFR <= '%s' AND ACTO >= '%s'",omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"));
	sprintf(cgSelection, "WHERE (ACTO BETWEEN '%s' AND '%s' AND ACFR <= '%s')",omStartTime.Format("%Y%m%d%H%M%S"),
							 olEndPlus2.Format("%Y%m%d%H%M%S"), omEndTime.Format("%Y%m%d%H%M%S"));
	if (ogBasicData.IsPrmHandlingAgentEnabled()) 
	{
		char clPrmSelection[124];

		sprintf(clPrmSelection," AND FILT = '%s' ",ogPrmConfig.getHandlingAgentShortName());
		strcat(cgSelection,clPrmSelection);
	}
	if (bgIsPrm)
	{
		char clPrmSelection[132];
		
		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			sprintf(clPrmSelection," AND FILT = '%s' ",ogPrmConfig.getHandlingAgentShortName());
			strcat(cgSelection,clPrmSelection);
		}
	}
		

	return Read(cgSelection);
}

bool CedaJobData::ReadJob(long lpJobUrno)
{
	bool blRc = false;

	sprintf(cgSelection, "WHERE URNO = '%d'", lpJobUrno);

	JOBDATA *prlJob = NULL;
	if(Read(cgSelection,false,false) && (prlJob = GetJobByUrno(lpJobUrno)) != NULL)
	{
		blRc = true;
		ogConflicts.CheckConflicts(*prlJob,FALSE,FALSE,TRUE);
		ogCCSDdx.DataChanged((void *)this,JOB_NEW,(void *)prlJob);
		SendJodDdx(prlJob, NULL);

		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
		if (prlFlight != NULL)
		{
			ogConflicts.CheckConflicts(*prlFlight);
			ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
		}
		if(!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBTEMPABSENCE))
		{
			CCSPtrArray<JOBDATA> olStfJobs;
			GetJobsByUstf(olStfJobs,prlJob->Ustf);
			int ilNumStfJobs = olStfJobs.GetSize();
			for(int ilStfJob = 0; ilStfJob < ilNumStfJobs; ilStfJob++)
			{
				ogConflicts.CheckConflicts(olStfJobs[ilStfJob],FALSE,TRUE,FALSE,FALSE);
			}
		}
	}

    return blRc;
}

bool CedaJobData::Read(char *pcpWhere, bool bpReset /* = true */, bool bpSendDdx /* = false */)
{
	int ilNumJobsAdded = 0;
	if(bpReset)
	{
		ClearAll();
	}
	PrepareFieldList();

	//ogBasicData.Trace("ReadAllJobs Selection: %s\n",pcpWhere);
	char pclCom[10] = "RT";

    if (CedaAction2(pclCom, pcpWhere) == false)
	{
		ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pcpWhere);
	}
	else
	{
		bool ilRc = true;
		bool blAddJob = false;
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			blAddJob = false;
			JOBDATA *prlJob = AllocNewJob(NULL);
			if ((ilRc = GetBufferRecord2(&omRecInfo,ilLc,prlJob,CString(pcmFieldList))) == true)
			{
				blAddJob = AddIncomingJob(prlJob);
			}

			if(blAddJob)
			{
				omTmpUrnoList.Add((DWORD) prlJob->Urno);
				if(bpSendDdx)
				{
					ogConflicts.CheckConflicts(*prlJob,FALSE,FALSE,TRUE);
					FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
					if (prlFlight != NULL)
					{
						ogConflicts.CheckConflicts(*prlFlight, FALSE, TRUE, TRUE, FALSE);
						//ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
					}
					if(!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBTEMPABSENCE))
					{
						CCSPtrArray<JOBDATA> olStfJobs;
						GetJobsByUstf(olStfJobs,prlJob->Ustf);
						int ilNumStfJobs = olStfJobs.GetSize();
						for(int ilStfJob = 0; ilStfJob < ilNumStfJobs; ilStfJob++)
						{
							ogConflicts.CheckConflicts(olStfJobs[ilStfJob], FALSE, TRUE, TRUE, FALSE);
						}
					}
					ogCCSDdx.DataChanged((void *)this,JOB_NEW,(void *)prlJob);
					SendJodDdx(prlJob, NULL);
				}
				ilNumJobsAdded++;
			}
			else
			{
				delete prlJob;
				prlJob = NULL;
			}
		}
	}

	ogBasicData.Trace("CedaJobData Read %d Records  (Rec Size %d) %s", ilNumJobsAdded, sizeof(JOBDATA), pcpWhere);
    return (ilNumJobsAdded > 0) ? true : false;
}

void CedaJobData::HandleJobTemplateChange(long lpJobUrno, long lpDemandUrno)
{
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(lpDemandUrno);
	JOBDATA *prlJob = GetJobByUrno(lpJobUrno);
	if(prlDemand != NULL && prlJob == NULL)
	{
		// the demand for the job has been found but not the job so load the job
		// (the job was dragged from a demand whose template is NOT loaded in this
		// OpssPm instance and dropped on a demand whose template IS loaded)
		ReadJob(lpJobUrno);
	}
	else if(prlDemand == NULL && prlJob != NULL)
	{
		// job was dragged from a demand whose template IS loaded in this instance
		// and was dropped on a demand whose template is NOT loaded, so delete the job internally
		DeleteJob(lpJobUrno, TRUE); // delete job internally
	}
}

// handle a job that was either read or received as a broadcast
bool CedaJobData::AddIncomingJob(JOBDATA *prpJob)
{
	bool blAddJob = false;

	if(prpJob != NULL && GetJobByUrno(prpJob->Urno) == NULL && JobHasCorrectTemplate(prpJob))
	{
		if(IsOverlapped(prpJob->Acfr, prpJob->Acto, omStartTime, omEndTime) || ogJodData.JobHasDemands(prpJob->Urno) || ogFlightData.GetFlightByUrno(prpJob->Flur) != NULL)
		{
			PrepareDataAfterRead(prpJob);
			if(bmIsOffline)
			{
				if(!WasDeletedOffline(prpJob->Urno))
				{
					blAddJob = AddJobInternal(prpJob);
				}
				UpdateOnlineData(prpJob);
			}
			else
			{
				blAddJob = AddJobInternal(prpJob);
			}
		}
	}

	return blAddJob;
}


// pcpDscr is a short description describing the originator of this insert, if it is empty then the field job->TmpText is used instead
bool CedaJobData::AddJob(JOBDATA *prpJob, const char *pcpDscr, BOOL bpSendDDX, bool bpUpdateDB /* = true */)
{
	bool blRc = false;

	if(prpJob != NULL)
	{
		if(strlen(pcpDscr) > 0)
		{
			strcpy(prpJob->TmpText,pcpDscr);
		}

		JOBDATA *prlJob = AllocNewJob(prpJob);
		bool blAddInternal = AddJobInternal(prlJob);

		if(bpUpdateDB)
		{
			DbInsertJob(prlJob);
		}

		if (bpSendDDX == TRUE && blAddInternal)
		{
			ogConflicts.CheckConflicts(*prlJob, FALSE,TRUE,TRUE);
			ogCCSDdx.DataChanged((void *)this,JOB_NEW,(void *)prlJob);
			SendJodDdx(prlJob, NULL);
		}

		if(!blAddInternal)
			delete prlJob;

		blRc = true;
	}

    return blRc;
}

JOBDATA *CedaJobData::AllocNewJob(JOBDATA *prpJob)
{
	JOBDATA *prlJob = new JOBDATA;
	if(prpJob != NULL)
	{
		*prlJob = *prpJob;
	}
	return prlJob;
}

bool CedaJobData::JobHasCorrectTemplate(JOBDATA *prpJob)
{
	void *p = NULL;
	if(prpJob->Utpl != 0L && !omUtplFilterMap.Lookup((void*)prpJob->Utpl,p))
	{
		return false;
	}
	return true;
}

bool CedaJobData::JobHasFullFieldList(JOBDATA *prpJob)
{
	if(prpJob != NULL && prpJob->Urno != 0L && prpJob->Acfr != TIMENULL && prpJob->Acto != TIMENULL && 
		prpJob->Ujty != 0L && prpJob->Utpl != 0L)
	{
		return true;
	}
	return false;
}


bool CedaJobData::AddJobInternal(JOBDATA *prpJob)
{
	bool blRc = false;

	if(prpJob != NULL)
	{
		if(!JobHasCorrectTemplate(prpJob))
			return blRc;

		omData.Add(prpJob);
		omUrnoMap.SetAt((void *)prpJob->Urno,prpJob);

		long llFlur = ogBasicData.GetFlightUrnoForJob(prpJob);
		if(llFlur != 0L)
		{
			CMapPtrToPtr *polFlightJobMap;

			if (omFlightMap.Lookup((void *) llFlur, (void *&) polFlightJobMap) == TRUE)
			{
				polFlightJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polFlightJobMap = new CMapPtrToPtr;
				polFlightJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omFlightMap.SetAt((void *)llFlur,polFlightJobMap);
			}
		}

		if (strcmp(prpJob->Jtco,JOBFM) == 0)
		{
			long llFlur = ogBasicData.GetFlightUrnoForJob(prpJob);
			CMapPtrToPtr *polFlightJobMap;

			if (omFlightFmMap.Lookup((void *) llFlur, (void *&) polFlightJobMap) == TRUE)
			{
				polFlightJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polFlightJobMap = new CMapPtrToPtr;
				polFlightJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omFlightFmMap.SetAt((void *)llFlur,polFlightJobMap);
			}
		}

		if(strlen(prpJob->Peno) > 0)
		{
			CMapPtrToPtr *polPknoJobMap;
			if(omPknoMap.Lookup((LPCSTR) prpJob->Peno, (void *&) polPknoJobMap) == TRUE)
			{
				polPknoJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polPknoJobMap = new CMapPtrToPtr;
				polPknoJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omPknoMap.SetAt((LPCSTR)prpJob->Peno,polPknoJobMap);
			}
		}

		if(prpJob->Ustf != 0L)
		{
			CMapPtrToPtr *polUstfJobMap;
			if(omUstfMap.Lookup((void *) prpJob->Ustf, (void *&) polUstfJobMap) == TRUE)
			{
				polUstfJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUstfJobMap = new CMapPtrToPtr;
				polUstfJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUstfMap.SetAt((void *) prpJob->Ustf,polUstfJobMap);
			}
		}

		if(prpJob->Udem != 0L)
		{
			CMapPtrToPtr *polUdemJobMap;
			if(omUdemMap.Lookup((void *) prpJob->Udem, (void *&) polUdemJobMap) == TRUE)
			{
				polUdemJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUdemJobMap = new CMapPtrToPtr;
				polUdemJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUdemMap.SetAt((void *) prpJob->Udem,polUdemJobMap);
			}
		}

		if(prpJob->Uaid != 0L)
		{
			CMapPtrToPtr *polUaidJobMap;
			if(omUaidMap.Lookup((void *) prpJob->Uaid, (void *&) polUaidJobMap) == TRUE)
			{
				polUaidJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUaidJobMap = new CMapPtrToPtr;
				polUaidJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUaidMap.SetAt((void *) prpJob->Uaid,polUaidJobMap);
			}
		}

		if (bgIsPrm)
		{
			if(prpJob->Uprm != 0L)
			{
				CMapPtrToPtr *polUprmJobMap;
				if(omUprmMap.Lookup((void *) prpJob->Uprm, (void *&) polUprmJobMap) == TRUE)
				{
					polUprmJobMap->SetAt((void *)prpJob->Urno,prpJob);
				}
				else
				{
					polUprmJobMap = new CMapPtrToPtr;
					polUprmJobMap->SetAt((void *)prpJob->Urno,prpJob);
					omUprmMap.SetAt((void *) prpJob->Uprm,polUprmJobMap);
				}
				// lli: send an internal broadcast so that some status can be updated like JOBS column in request list
				DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpJob->Uprm);
				if (prlDpx != NULL)
				{
					ogCCSDdx.DataChanged((void *)this,DPX_ALLOC,(void *)prlDpx);
				}
			}
		}
		if(prpJob->Jour != 0L)
		{
			CMapPtrToPtr *polJourJobMap;
			if(omJourMap.Lookup((void *) prpJob->Jour, (void *&) polJourJobMap) == TRUE)
			{
				polJourJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polJourJobMap = new CMapPtrToPtr;
				polJourJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omJourMap.SetAt((void *) prpJob->Jour,polJourJobMap);
			}
		}

		if(prpJob->Uequ != 0L)
		{
			CMapPtrToPtr *polUequJobMap;
			if(omUequMap.Lookup((void *) prpJob->Uequ, (void *&) polUequJobMap) == TRUE)
			{
				polUequJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUequJobMap = new CMapPtrToPtr;
				polUequJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUequMap.SetAt((void *) prpJob->Uequ,polUequJobMap);
			}
		}

		if(prpJob->Shur != 0L)
		{
			CMapPtrToPtr *polShurJobMap;
			if(omShurMap.Lookup((void *) prpJob->Shur, (void *&) polShurJobMap) == TRUE)
			{
				polShurJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polShurJobMap = new CMapPtrToPtr;
				polShurJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omShurMap.SetAt((void *) prpJob->Shur,polShurJobMap);
			}
		}

		blRc = true;
	}

    return blRc;
}

void CedaJobData::UpdateMaps(JOBDATA *prpJob, JOBDATA *prpOldJob)
{
	if(prpJob != NULL && prpOldJob != NULL)
	{
		long llFlur = ogBasicData.GetFlightUrnoForJob(prpJob);
		long llOldFlur = ogBasicData.GetFlightUrnoForJob(prpOldJob);
		if(llFlur != llOldFlur)
		{
			CMapPtrToPtr *polFlightJobMap;

			if (omFlightMap.Lookup((void *) llOldFlur, (void *&) polFlightJobMap) == TRUE)
			{
				polFlightJobMap->RemoveKey((void * ) prpJob->Urno);
			}

			if (omFlightMap.Lookup((void *) llFlur, (void *&) polFlightJobMap) == TRUE)
			{
				polFlightJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polFlightJobMap = new CMapPtrToPtr;
				polFlightJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omFlightMap.SetAt((void *)llFlur,polFlightJobMap);
			}
		}


		if(strcmp(prpJob->Peno,prpOldJob->Peno))
		{
			CMapPtrToPtr *polPknoJobMap;

			if (omPknoMap.Lookup((LPCSTR) prpOldJob->Peno, (void *&) polPknoJobMap) == TRUE)
			{
				polPknoJobMap->RemoveKey((void * ) prpJob->Urno);
			}

			if (omPknoMap.Lookup((LPCSTR) prpJob->Peno, (void *&) polPknoJobMap) == TRUE)
			{
				polPknoJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polPknoJobMap = new CMapPtrToPtr;
				polPknoJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omPknoMap.SetAt((LPCSTR)prpJob->Peno,polPknoJobMap);
			}
		}

		if(prpJob->Ustf != prpOldJob->Ustf)
		{
			CMapPtrToPtr *polUstfJobMap;
			if (omUstfMap.Lookup((void *) prpOldJob->Ustf, (void *&) polUstfJobMap) == TRUE)
			{
				polUstfJobMap->RemoveKey((void * ) prpJob->Urno);
			}

			if (omUstfMap.Lookup((void *) prpJob->Ustf, (void *&) polUstfJobMap) == TRUE)
			{
				polUstfJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUstfJobMap = new CMapPtrToPtr;
				polUstfJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUstfMap.SetAt((void *) prpJob->Ustf,polUstfJobMap);
			}
		}

		if(prpJob->Udem != prpOldJob->Udem)
		{
			CMapPtrToPtr *polUdemJobMap;
			if (omUdemMap.Lookup((void *) prpOldJob->Udem, (void *&) polUdemJobMap) == TRUE)
			{
				polUdemJobMap->RemoveKey((void * ) prpJob->Urno);
			}

			if (omUdemMap.Lookup((void *) prpJob->Udem, (void *&) polUdemJobMap) == TRUE)
			{
				polUdemJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUdemJobMap = new CMapPtrToPtr;
				polUdemJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUdemMap.SetAt((void *) prpJob->Udem,polUdemJobMap);
			}
		}

		if(prpJob->Uaid != prpOldJob->Uaid)
		{
			CMapPtrToPtr *polUaidJobMap;
			if (omUaidMap.Lookup((void *) prpOldJob->Uaid, (void *&) polUaidJobMap) == TRUE)
			{
				polUaidJobMap->RemoveKey((void * ) prpJob->Urno);
			}

			if (omUaidMap.Lookup((void *) prpJob->Uaid, (void *&) polUaidJobMap) == TRUE)
			{
				polUaidJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUaidJobMap = new CMapPtrToPtr;
				polUaidJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUaidMap.SetAt((void *) prpJob->Uaid,polUaidJobMap);
			}
		}

		if (bgIsPrm)
		{
			if(prpJob->Uprm != 0L)
			{
				CMapPtrToPtr *polUprmJobMap;
				if(omUprmMap.Lookup((void *) prpJob->Uprm, (void *&) polUprmJobMap) == TRUE)
				{
					polUprmJobMap->SetAt((void *)prpJob->Urno,prpJob);
				}
				else
				{
					polUprmJobMap = new CMapPtrToPtr;
					polUprmJobMap->SetAt((void *)prpJob->Urno,prpJob);
					omUprmMap.SetAt((void *) prpJob->Uprm,polUprmJobMap);
				}
			}
		}

		if(prpJob->Jour != prpOldJob->Jour)
		{
			CMapPtrToPtr *polJourJobMap;
			if (omJourMap.Lookup((void *) prpOldJob->Jour, (void *&) polJourJobMap) == TRUE)
			{
				polJourJobMap->RemoveKey((void * ) prpJob->Urno);
			}
			if (omJourMap.Lookup((void *) prpJob->Jour, (void *&) polJourJobMap) == TRUE)
			{
				polJourJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polJourJobMap = new CMapPtrToPtr;
				polJourJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omJourMap.SetAt((void *) prpJob->Jour,polJourJobMap);
			}
		}

		if(prpJob->Uequ != prpOldJob->Uequ)
		{
			CMapPtrToPtr *polUequJobMap;
			if (omUequMap.Lookup((void *) prpOldJob->Uequ, (void *&) polUequJobMap) == TRUE)
			{
				polUequJobMap->RemoveKey((void * ) prpJob->Urno);
			}
			if (omUequMap.Lookup((void *) prpJob->Uequ, (void *&) polUequJobMap) == TRUE)
			{
				polUequJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polUequJobMap = new CMapPtrToPtr;
				polUequJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omUequMap.SetAt((void *) prpJob->Uequ,polUequJobMap);
			}
		}

		if(prpJob->Shur != prpOldJob->Shur)
		{
			CMapPtrToPtr *polShurJobMap;
			if (omShurMap.Lookup((void *) prpOldJob->Shur, (void *&) polShurJobMap) == TRUE)
			{
				polShurJobMap->RemoveKey((void * ) prpJob->Urno);
			}
			if (omShurMap.Lookup((void *) prpJob->Shur, (void *&) polShurJobMap) == TRUE)
			{
				polShurJobMap->SetAt((void *)prpJob->Urno,prpJob);
			}
			else
			{
				polShurJobMap = new CMapPtrToPtr;
				polShurJobMap->SetAt((void *)prpJob->Urno,prpJob);
				omShurMap.SetAt((void *) prpJob->Shur,polShurJobMap);
			}
		}
	}
}

JOBDATA *CedaJobData::DeleteJobInternal(long lpUrno)
{
	JOBDATA *prlJob = GetJobByUrno(lpUrno);
	if (prlJob != NULL)
	{
		long llFlur = ogBasicData.GetFlightUrnoForJob(prlJob);

		CMapPtrToPtr *pomFlightJobMap;
		if (omFlightMap.Lookup((void *) llFlur, (void *&) pomFlightJobMap) == TRUE)
		{
			pomFlightJobMap->RemoveKey((void * ) lpUrno);
		}
		if (omFlightFmMap.Lookup((void *) llFlur, (void *&) pomFlightJobMap) == TRUE)
		{
			pomFlightJobMap->RemoveKey((void * ) lpUrno);
		}
		CMapPtrToPtr *polPknoJobMap;
		if (omPknoMap.Lookup((LPCSTR) prlJob->Peno, (void *&) polPknoJobMap) == TRUE)
		{
			polPknoJobMap->RemoveKey((void * ) lpUrno);
		}
		CMapPtrToPtr *polUstfJobMap;
		if (omUstfMap.Lookup((void *) prlJob->Ustf, (void *&) polUstfJobMap) == TRUE)
		{
			polUstfJobMap->RemoveKey((void * ) lpUrno);
		}
		CMapPtrToPtr *polUdemJobMap;
		if (omUdemMap.Lookup((void *) prlJob->Udem, (void *&) polUdemJobMap) == TRUE)
		{
			polUdemJobMap->RemoveKey((void * ) lpUrno);
		}
		CMapPtrToPtr *polUaidJobMap;
		if (omUaidMap.Lookup((void *) prlJob->Uaid, (void *&) polUaidJobMap) == TRUE)
		{
			polUaidJobMap->RemoveKey((void * ) lpUrno);
		}
		if (bgIsPrm)
		{
			CMapPtrToPtr *polUprmJobMap;
			if (omUprmMap.Lookup((void *) prlJob->Uprm, (void *&) polUprmJobMap) == TRUE)
			{
				polUprmJobMap->RemoveKey((void * )  prlJob->Urno);
				if (polUprmJobMap->IsEmpty())
				{
					omUprmMap.RemoveKey((void *)  prlJob->Uprm);
					delete polUprmJobMap;
				}
			}
		}
		CMapPtrToPtr *polJourJobMap;
		if (omJourMap.Lookup((void *) prlJob->Jour, (void *&) polJourJobMap) == TRUE)
		{
			polJourJobMap->RemoveKey((void * ) lpUrno);
		}
		CMapPtrToPtr *polUequJobMap;
		if (omUequMap.Lookup((void *) prlJob->Uequ, (void *&) polUequJobMap) == TRUE)
		{
			polUequJobMap->RemoveKey((void * ) lpUrno);
		}
		CMapPtrToPtr *polShurJobMap;
		if (omShurMap.Lookup((void *) prlJob->Shur, (void *&) polShurJobMap) == TRUE)
		{
			polShurJobMap->RemoveKey((void * ) lpUrno);
		}

		omUrnoMap.RemoveKey((void *)lpUrno);
		for(int ilJ = (omData.GetSize() - 1); ilJ >= 0; ilJ--)
		{
			if(omData[ilJ].Urno == lpUrno)
			{
				omData.DeleteAt(ilJ);
			}
		}
	}
	return prlJob;
}

// conversions from new DB model to old
void CedaJobData::PrepareDataAfterRead(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		strcpy(prpJob->Jtco,ogJtyData.GetJtyNameByUrno(prpJob->Ujty));
		strcpy(prpJob->Alid,ogBasicData.GetAlidForJob(prpJob->Uaid,prpJob->Ualo));
		strcpy(prpJob->Aloc,ogAloData.GetAlocByUrno(prpJob->Ualo));
		strcpy(prpJob->Peno,ogEmpData.GetPenoByUrno(prpJob->Ustf));
		prpJob->Stat[0] = prpJob->DbStat[0]; prpJob->Stat[1] = '\0';
		prpJob->Chng[0] = prpJob->DbStat[1]; prpJob->Chng[1] = '\0';
		prpJob->Ignr[0] = prpJob->DbStat[2]; prpJob->Ignr[1] = '\0';
		prpJob->Infm = (prpJob->DbStat[4] == '1' || prpJob->DbStat[4] == 'A') ? true : false;
		prpJob->Ackn = prpJob->DbStat[4] == 'A' ? true : false;
		prpJob->Printed = prpJob->DbStat[5] == '1' ? true : false;

//		if(strlen(prpJob->Alid) <= 0)
//		{
//			ogBasicData.Trace("CedaJobData::PrepareDataAfterRead() Possible invalid UAID <%d> or UALO <%d> in JOBTAB URNO <%d>\n",prpJob->Uaid,prpJob->Ualo,prpJob->Urno);
//		}

		ConvertDatesToLocal(prpJob);
	}
}

// update flight values in jobs so we can detect conflicts
void CedaJobData::SetJobFlightFields(void)
{
	int ilNumJobs = omData.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &omData[ilJob];
		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
		if(prlFlight != NULL)
		{
			ogDataSet.CreateOldFlightFields(prlJob,prlFlight);
		}
	}
}

// conversions from new DB model to old
void CedaJobData::PrepareDataForWrite(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		prpJob->Ujty = ogJtyData.GetJtyUrnoByName(prpJob->Jtco);
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
		if(prlEmp != NULL)
		{
			prpJob->Ustf = prlEmp->Urno;
		}
		prpJob->Uaid = ogBasicData.GetUaidForJob(prpJob->Alid,prpJob->Ualo);
		prpJob->DbStat[0] = strlen(prpJob->Stat)>0 ? prpJob->Stat[0] : ' ';
		prpJob->DbStat[1] = strlen(prpJob->Chng)>0 ? prpJob->Chng[0] : ' ';
		prpJob->DbStat[2] = strlen(prpJob->Ignr)>0 ? prpJob->Ignr[0] : ' ';
		prpJob->DbStat[3] = ' '; // used for CASP process (Softlab)
		if(prpJob->Infm)
		{
			prpJob->DbStat[4] = strcmp(prpJob->Jtco,JOBPOOL) ? '1' : 'A';
		}
		else
		{
			prpJob->DbStat[4] = ' ';
		}
		prpJob->DbStat[5] = prpJob->Printed ? '1' : ' ';
		prpJob->DbStat[6] = ' ';
		prpJob->DbStat[7] = ' ';
		prpJob->DbStat[8] = ' ';
		prpJob->DbStat[9] = ' ';
		prpJob->DbStat[10] = '\0';

		SetAdditionalJobFields(prpJob);

		// set lstu/cdat etc
		CTime olCurrTime = ogBasicData.GetTime();
		CString olUserText;
		olUserText.Format("%s %s %s",ogUsername,prpJob->TmpText,ogBasicData.GetVersion()); // username + version number and function number (which function did the change)

		prpJob->Lstu = olCurrTime;
		strcpy(prpJob->Useu,olUserText.Left(31));

		if(prpJob->Cdat == TIMENULL || strlen(prpJob->Usec) <= 0)
		{
			prpJob->Cdat = olCurrTime;
			strcpy(prpJob->Usec,olUserText.Left(31));
		}
	}
}

void CedaJobData::SetAdditionalJobFields(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		bool blSetFcco = true;
		if(prpJob->Udem != 0L)
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prpJob->Udem);
			if(prlDemand != NULL)
			{
				// SERTAB.URNO URNO of service for jobs with demands
				RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
				if(prlRud != NULL)
				{
					prpJob->Ughs = prlRud->Ughs;
				}

				// Function Code: PJ = emp main func, Regular Job = dem func/if dem has group of funcs or job w/o dem then use emp main func
				if(strcmp(prpJob->Jtco,JOBPOOL))
				{
					RPFDATA *prlRpf = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
					if(prlRpf != NULL)
					{
						strcpy(prpJob->Fcco,prlRpf->Fcco);
						blSetFcco = false;
					}
				}
			}
		}
		if(blSetFcco) // dem has group of funcs or job w/o dem then use emp main func
		{
			// Function Code: PJ = emp main func, Regular Job = dem func/if dem has group of funcs or job w/o dem then use emp main func
			JOBDATA *prlPoolJob = (!strcmp(prpJob->Jtco,JOBPOOL)) ? prpJob : GetJobByUrno(prpJob->Jour);
			strcpy(prpJob->Fcco, ogBasicData.GetFunctionForPoolJob(prlPoolJob));
		}
		if(!strcmp(prpJob->Jtco,JOBPOOL))
		{
			// Workgroup Code - the WG an emp is currently working in, stored in PJs only
			strcpy(prpJob->Wgpc, ogBasicData.GetTeamForPoolJob(prpJob));
		}
	}
}


void CedaJobData::ConvertDatesToUtc(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		ogBasicData.ConvertDateToUtc(prpJob->Plfr);
		ogBasicData.ConvertDateToUtc(prpJob->Plto);
		ogBasicData.ConvertDateToUtc(prpJob->Acfr);
		ogBasicData.ConvertDateToUtc(prpJob->Acto);
		ogBasicData.ConvertDateToUtc(prpJob->Cdat);
		ogBasicData.ConvertDateToUtc(prpJob->Lstu);
	}
}

void CedaJobData::ConvertDatesToLocal(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		ogBasicData.ConvertDateToLocal(prpJob->Plfr);
		ogBasicData.ConvertDateToLocal(prpJob->Plto);
		ogBasicData.ConvertDateToLocal(prpJob->Acfr);
		ogBasicData.ConvertDateToLocal(prpJob->Acto);
		ogBasicData.ConvertDateToLocal(prpJob->Cdat);
		ogBasicData.ConvertDateToLocal(prpJob->Lstu);
	}
}

//////////////////////////////////////////////////////////////////////////////
//////////// update functions  ///////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


// pcpDscr is a short description describing the originator of this update, if it is empty then the field job->TmpText is used instead
bool CedaJobData::ChangeJobTime(long lpJobu, CTime opNewStart, CTime opNewEnd, const char *pcpDscr)
{
	JOBDATA *prlJob = GetJobByUrno(lpJobu);
	if (prlJob != NULL)
	{
		JOBDATA rlOldJob = *prlJob;

		if(strlen(pcpDscr) > 0)
		{
			strcpy(prlJob->TmpText,pcpDscr);
		}

		prlJob->Acfr = opNewStart;
		prlJob->Acto = opNewEnd;

		DbUpdateJob(prlJob, &rlOldJob);

		ogConflicts.CheckConflicts(*prlJob,FALSE,TRUE,TRUE);
		ogCCSDdx.DataChanged((void *)this,JOB_CHANGE,(void *)prlJob);

		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
		if(prlFlight != NULL)
		{
			ogConflicts.CheckConflicts(*prlFlight, FALSE, TRUE, TRUE, FALSE);
		}
	}

	return true;
}

bool CedaJobData::SendToPda(JOBDATA *prpJob)
{
	bool blRc;

	if(bmIsOffline)
	{
		pogButtonList->MessageBox("Cannot send to PDA in offline mode!","Offline-Mode",MB_ICONEXCLAMATION);
		return false;
	}
		
	if (prpJob->Uprm == 0L)
	{
		pogButtonList->MessageBox("This is not a PRM related Job, cannot be sent to PDA","Offline-Mode",MB_ICONEXCLAMATION);
		return false;
	}
	if (prpJob->Ustf == 0L)
	{
		pogButtonList->MessageBox("There is no Employee assigned, Job cannot be sent to PDA","Offline-Mode",MB_ICONEXCLAMATION);
		return false;
	}
	char clSelection[256];
	char clTable[12];
	char clCmd[12];
	char clFieldList[256];
	char clData[256];

	sprintf(clSelection,"%ld,%ld,%c",prpJob->Uprm,prpJob->Ustf,prpJob->Udem != 0L ? '1' : '2' );
	strcpy(clCmd,"SPDA");
	strcpy(clTable,"DPXTAB");
	strcpy(clFieldList,"");
	strcpy(clData,"");
	ogBasicData.LogCedaAction(clTable, clCmd, clSelection, clFieldList, clData);

	if(!(blRc = CedaAction(clCmd, clTable, clFieldList, clSelection, "", clData)))
	{
		ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,clCmd,clFieldList,clTable,clSelection);
	}


	return true;
}

// pcpDscr is a short description describing the originator of this update, if it is empty then the field job->TmpText is used instead
bool CedaJobData::ChangeJobData(JOBDATA *prpJob, JOBDATA *prpOldJob, const char *pcpDscr)
{
	UpdateMaps(prpJob, prpOldJob);

	if(strlen(pcpDscr) > 0)
	{
		strcpy(prpJob->TmpText,pcpDscr);
	}


	JOBDATA *prlJob = GetJobByUrno(prpJob->Urno);
	if(prlJob != NULL)
	{
		DbUpdateJob(prlJob, prpOldJob);

		ogConflicts.CheckConflicts(*prlJob,FALSE,FALSE,TRUE,FALSE);
		ogCCSDdx.DataChanged((void *)this,JOB_CHANGE,(void *)prlJob);
		SendJodDdx(prlJob, prpOldJob);

		if (strcmp(prlJob->Jtco,JOBPOOL) == 0)
		{
			CCSPtrArray<JOBDATA> olJobs;
			GetJobsByPoolJob(olJobs,prlJob->Urno);
			int ilNumJobs = olJobs.GetSize();
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				ogConflicts.CheckConflicts(olJobs[ilJ],FALSE,TRUE,TRUE,FALSE);
			}
		}
		else
		{
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
			if (prlFlight != NULL)
			{
				ogConflicts.CheckConflicts(*prlFlight, FALSE, TRUE, TRUE, FALSE);

				// check if the job was dragged from the demand of one flight to the demand
				// of another flight - ie from one FlightDetailDlg to another
				FLIGHTDATA *prlOldFlight = ogBasicData.GetFlightForJob(prpOldJob);
				if(prlOldFlight != NULL && prlOldFlight->Urno != prlFlight->Urno)
				{
					ogConflicts.CheckConflicts(*prlOldFlight, FALSE, TRUE, TRUE, FALSE);
				}
			}
		}
	}

    return true;
}

bool CedaJobData::DeleteJob(long lpUrno, BOOL bpSendDdx /* = FALSE */, bool bpUpdateDB /* = true */)
{
	// delete the job in display data
	JOBDATA *prlJob = GetJobByUrno(lpUrno);
	if (prlJob != NULL)
	{
		JOBDATA rlJob = *prlJob;
		DeleteJobInternal(lpUrno);
		ogConflicts.DeleteConflictForJob(lpUrno);

		CCSPtrArray<JOBDATA> olJobs;
		GetJobsByPoolJob(olJobs,rlJob.Jour);
		if (olJobs.GetSize() > 0)
		{
			ogConflicts.CheckConflicts(olJobs[0],FALSE,TRUE);
		}
		
		ogCCSDdx.DataChanged((void *)this,JOB_DELETE,(void *)&rlJob);
		if (bgIsPrm)
		{
			if (rlJob.Uprm != 0L)
			{
				DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(rlJob.Uprm);
				if (prlDpx != NULL)
				{
					ogCCSDdx.DataChanged((void *)this,DPX_ALLOC,(void *)prlDpx);
				}
			}
		}
		SendJodDdx(NULL, &rlJob);

		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(&rlJob);
		if (prlFlight != NULL)
		{
			ogConflicts.CheckConflicts(*prlFlight, FALSE, TRUE, TRUE, FALSE);
			//ogConflicts.CheckConflicts(*prlFlight);
			FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight->Urno);
			if(prlRotation != NULL)
			{
				ogConflicts.CheckConflicts(*prlRotation, FALSE, TRUE, TRUE, FALSE);
			}
			//ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
		}
		olJobs.RemoveAll();

		if (bpSendDdx == FALSE && bpUpdateDB)
		{
			DbDeleteJob(rlJob.Urno);
		}
	}

    return true;
}

bool CedaJobData::DbInsertJob(JOBDATA *prpJob)
{
	bool blRc = false;

	if(prpJob != NULL)
	{
		if(bmIsOffline)
		{
			SetReleaseButton();
			blRc = true;
		}
		else
		{
			DbInsertJob2(prpJob);
		}
	}

	return blRc;
}

bool CedaJobData::DbInsertJob2(JOBDATA *prpJob)
{
	bool blRc = false;

	// if transaction, then release all job changes via release handler in a block
	if(prpJob != NULL && (blRc = AddToIrtTransaction(prpJob)) == false)
	{
		char pclSelection[124] = "";
		char pclData[2000] = "";
		char pclCmd[10] = "IRT";
		char pclInsertFieldList[2000] = "";
		
		PrepareDataForWrite(prpJob);
		ConvertDatesToUtc(prpJob);
		//GetNewFields(prpJob, pclInsertFieldList, pclData);
		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			strcpy(prpJob->Filt,ogPrmConfig.getHandlingAgentShortName());
		}
		GetAllFields(prpJob, pclInsertFieldList, pclData);

		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclInsertFieldList, pclData);

		if(!(blRc = CedaAction(pclCmd, pcmTableName, pclInsertFieldList, pclSelection, "", pclData)))
		{
			ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,pclCmd,pclInsertFieldList,pcmTableName,pclSelection);
		}

		ConvertDatesToLocal(prpJob);
	}

	return blRc;
}

bool CedaJobData::DbUpdateJob(JOBDATA *prpJob, JOBDATA *prpOldJob /*=NULL*/)
{
	bool blRc = false;

	if(prpJob != NULL)
	{
		if(bmIsOffline)
		{
			SetReleaseButton();
			blRc = true;
		}
		else
		{
			DbUpdateJob2(prpJob, prpOldJob);
		}
	}

	return blRc;
}

bool CedaJobData::DbUpdateJob2(JOBDATA *prpJob, JOBDATA *prpOldJob /*=NULL*/)
{
	bool blRc = false;

	// if transaction, then release all job changes via release handler in a block
	if(prpJob != NULL && (blRc = AddToUrtTransaction(prpJob)) == false)
	{
		char pclSelection[124] = "";
		char pclData[2000] = "";
		char pclCmd[10] = "URT";
		char pclFieldList[1000];

		PrepareDataForWrite(prpJob);
		ConvertDatesToUtc(prpJob);
		ConvertDatesToUtc(prpOldJob);

		sprintf(pclSelection,"WHERE URNO = '%ld'",prpJob->Urno);

		if(prpOldJob != NULL && prpJob->Utpl == prpOldJob->Utpl && prpJob->Acfr == prpOldJob->Acfr && prpJob->Acto == prpOldJob->Acto && 
			GetChangedFields(prpJob, prpOldJob, pclFieldList, pclData))
		{
			// we have the old record so update JOBTAB only with the fields that have been changed
			ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclFieldList, pclData);
			blRc = CedaAction(pclCmd, pcmTableName, pclFieldList, pclSelection,"", pclData);
		}
		else
		{
			// update all fields in JOBTAB for this record because no old record found, or job time has chaged or template has changed
			if(GetAllFields(prpJob, pclFieldList, pclData))
			{
				ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pclFieldList, pclData);
				blRc = CedaAction(pclCmd, pcmTableName, pclFieldList, pclSelection, "", pclData);
			}
		}
	
		if(!blRc)
		{
			ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,pclCmd,pclFieldList,pcmTableName,pclSelection);
		}

		ConvertDatesToLocal(prpJob);
		ConvertDatesToLocal(prpOldJob);
	}

	return blRc;
}

bool CedaJobData::DbDeleteJob(long lpJobUrno)
{
	bool blRc = false;

	if(bmIsOffline)
	{
		// only add this job to the deletion list if it wasn't inserted offline ie. the online job exists
		if(GetOnlineJobByUrno(lpJobUrno) != NULL)
		{
			omDeletedOfflineJobs.SetAt((void *) lpJobUrno, NULL);
		}
		SetReleaseButton();
		blRc = true;
	}
	else
	{
		DbDeleteJob2(lpJobUrno);
	}

	return blRc;
}

bool CedaJobData::DbDeleteJob2(long lpJobUrno)
{
	bool blRc = false;

	// if transaction, then release all job changes via release handler in a block
	if((blRc = AddToDrtTransaction(lpJobUrno)) == false)
	{
		char pclSelection[124] = "";
		char pclData[2000] = "";
		char pclCmd[10] = "DRT";

		sprintf(pclSelection,"WHERE URNO = '%ld'",lpJobUrno);
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);

		if(!(blRc = CedaAction(pclCmd,pclSelection)))
		{
			ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
		}
	}

	return blRc;
}

bool CedaJobData::AutoAssignDemands(CString opFieldList,CString opDataList)
{
	bool blRc = false;

	char *pclData = NULL;
	char *pclField = NULL;
			
	pclData = new char[opDataList.GetLength()+100];
	strcpy(pclData,opDataList);
	pclField = new char[opFieldList.GetLength()+100];
	strcpy(pclField,opFieldList);

	char pclCom[10] = "AFL";

	ogBasicData.LogCedaAction(pcmTableName, pclCom, "", pclField, pclData);

	if(!CedaAction(pclCom, pcmTableName, pclField, "", "", pclData, "BUF1"))
	{
		// ogBasicData.LogCedaError("CedaJobData Error (AutoAssignDemands)",omLastErrorMessage,pclCom,pclField,pcmTableName,"");
		ogBasicData.Trace("CedaJobData Error (AutoAssignDemands) %s <%s><%s><%s>",omLastErrorMessage,pclCom,pclField,pcmTableName);
		// commented out the line above and added the line below so that QCP/Timeout errors are ignored - need to improve jobhdl performance
		blRc = true;
	}
	else
	{
		blRc = true;
	}
	
	delete [] pclField;
	delete [] pclData;

	return blRc;
}

void CedaJobData::AutoAssignDemandsRegn(CString opFieldList,CString opDataList)
{
	char *pclData = NULL;
	char *pclField = NULL;

	pclData = new char[opDataList.GetLength()+100];
	strcpy(pclData,opDataList);
	pclField = new char[opFieldList.GetLength()+100];
	strcpy(pclField,opFieldList);

	char pclCom[10] = "ONR";

	ogBasicData.LogCedaAction(pcmTableName, pclCom, "", pclField, pclData);

	if(!CedaAction(pclCom, pcmTableName, pclField, "", "", pclData, "BUF1"))
	{
		ogBasicData.LogCedaError("CedaJobData Error (AutoAssignDemandsAlitalia)",omLastErrorMessage,pclCom,pclField,pcmTableName,"");
	}

	delete [] pclField;
	delete [] pclData;
}

// compare prpJob with prpOldJob and return only the fields (pcpFieldList) and data (pcpData) which have changed
bool CedaJobData::GetChangedFields(JOBDATA *prpJob, JOBDATA *prpOldJob, char *pcpFieldList, char *pcpData)
{
	bool blChangesFound = false;

	memset(pcpFieldList,0,sizeof(pcpFieldList));
	memset(pcpData,0,sizeof(pcpData));

	if(prpJob != NULL && prpOldJob != NULL)
	{
		JOBDATA rlJob;
		CString olData;

		memcpy(&rlJob,prpJob,sizeof(JOBDATA));
		MakeCedaData(&omRecInfo,olData,&rlJob);
		CStringArray olNewFieldArray, olNewDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olNewFieldArray);
		ogBasicData.ExtractItemList(olData, &olNewDataArray);
		int ilNumNewFields = olNewFieldArray.GetSize();
		int ilNumNewData = olNewDataArray.GetSize();

		memcpy(&rlJob,prpOldJob,sizeof(JOBDATA));
		MakeCedaData(&omRecInfo,olData,&rlJob);
		CStringArray olOldDataArray;
		ogBasicData.ExtractItemList(olData, &olOldDataArray);
		int ilNumOldData = olOldDataArray.GetSize();

		bool blListsAreEmpty = true;
		if(ilNumNewFields == ilNumNewData && ilNumNewData == ilNumOldData)
		{
			for(int ilD = 0; ilD < ilNumNewData; ilD++)
			{
				if(olNewFieldArray[ilD] == "0") // non-existant field - see comment in PrepareFieldList()
					continue;

				// check if the data item has changed - always write LSTU and USEU whether they change or not
				if(olNewDataArray[ilD] != olOldDataArray[ilD] || olNewFieldArray[ilD] == "LSTU" || olNewFieldArray[ilD] == "USEU")
				{
					CString olOldData = olOldDataArray[ilD];
					CString olNewData = olNewDataArray[ilD];
					if(blListsAreEmpty)
					{
						strcpy(pcpFieldList,olNewFieldArray[ilD]);
						strcpy(pcpData,olNewDataArray[ilD]);
						blListsAreEmpty = false;
					}
					else
					{
						CString olTmp;
						olTmp.Format(",%s",olNewFieldArray[ilD]);
						strcat(pcpFieldList,olTmp);
						olTmp.Format(",%s",olNewDataArray[ilD]);
						strcat(pcpData,olTmp);
					}

					if(olNewDataArray[ilD] != olOldDataArray[ilD] && 
						olNewFieldArray[ilD] != "CDAT" && olNewFieldArray[ilD] != "USEC" &&
						olNewFieldArray[ilD] != "LSTU" && olNewFieldArray[ilD] != "USEU")
					{
						blChangesFound = true;
					}
				}
			}
		}
	}

	return blChangesFound;
}

// get only data list and field list for fields which really exist in JOBTAB
// some fields eg UTPL/UEQU may not exists (not specified in SYSTAB) see comment in PrepareFieldList()
bool CedaJobData::GetAllFields(JOBDATA *prpJob, char *pcpFieldList, char *pcpData)
{
	bool blRc = false;

	memset(pcpFieldList,0,sizeof(pcpFieldList));
	memset(pcpData,0,sizeof(pcpData));

	if(prpJob != NULL)
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prpJob);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();

		bool blListsNotEmpty = false;
		if(ilNumFields == ilNumData)
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				if(olFieldArray[ilD] == "0") // non-existant field - see comment in PrepareFieldList()
					continue;

				if(blListsNotEmpty)
				{
					strcat(pcpFieldList,",");
					strcat(pcpData,",");
				}
				else
				{
					blListsNotEmpty = true;
					blRc = true;
				}

				strcat(pcpFieldList,olFieldArray[ilD]);
				strcat(pcpData,olDataArray[ilD]);
			}
		}
	}

	return blRc;
}

// make pcpData using only the fields specified in pcpFieldList - this is useful when the DB
// table doesn't have all the fields possible eg. UEQU is missing
void CedaJobData::GetNewFields(JOBDATA *prpJob, char *pcpFieldList, char *pcpData)
{
	memset(pcpData,0,sizeof(pcpData));
	memset(pcpFieldList,0,sizeof(pcpFieldList));

	if(prpJob != NULL)
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prpJob);

		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);

		int ilNumFields = omRecInfo.GetSize();
		int ilNumData = olDataArray.GetSize();

		bool blListsAreEmpty = true;
		for(int ilD = 0; ilD < ilNumData && ilD < ilNumFields; ilD++)
		{
			// check if the data item is specified in the given field list
			CEDARECINFO *prlRecInfo = &omRecInfo[ilD];
			if(strstr(pcmFieldList, prlRecInfo->Name) != NULL)
			{
				if(blListsAreEmpty)
				{
					blListsAreEmpty = false;
				}
				else
				{
					strcat(pcpData,",");
					strcat(pcpFieldList,",");
				}
				strcat(pcpData,olDataArray[ilD]);
				strcat(pcpFieldList,olFieldArray[ilD]);
			}
		}
	}
}

void CedaJobData::InitNewJob(JOBDATA *prpJob, 
							 const char *pcpJobType, 
							 const char *pcpAlid, 
							 const char *pcpAloc,
							 CTime opFrom, 
							 CTime opTo, 
							 long lpUdrr, 
							 const char *pcpPeno,
							 long lpFlur			/* = 0L */, 
							 long lpJour			/* = 0L */, 
							 const char *pcpText	/* = NULL */, 
							 const char *pcpDety	/* = NULL */, 
							 long lpUdem,			/* = 0L */
							 bool bpReinitialize	/* = false */)
{
	if(prpJob != NULL)
	{
		if(!bpReinitialize)
		{
			prpJob->Urno = ogBasicData.GetNextUrno();
			prpJob->Plfr = opFrom;
			prpJob->Plto = opTo;
		}
		strcpy(prpJob->Jtco, pcpJobType);

		strcpy(prpJob->Alid, pcpAlid);
		strcpy(prpJob->Aloc,pcpAloc);
		prpJob->Ualo = ogAloData.GetAloUrnoByName(pcpAloc);
		prpJob->Uaid = ogBasicData.GetUaidForJob(prpJob->Alid,prpJob->Ualo);

		prpJob->Acfr = opFrom;
		prpJob->Acto = opTo;

		strcpy(prpJob->Stat, "P");	// default job state is Planned
		strcpy(prpJob->Chng, "0");
		strcpy(prpJob->Ignr, "0");
		prpJob->Infm = false;

		prpJob->Flur = lpFlur;
		if(lpFlur != 0L)
		{
			ogDataSet.CreateOldFlightFields(prpJob,ogFlightData.GetFlightByUrno(lpFlur));
		}

		strcpy(prpJob->Peno, pcpPeno);
		prpJob->Shur = lpUdrr;

		prpJob->Jour = lpJour;


		if(pcpText != NULL)
		{
			strncpy(prpJob->Text,pcpText,JOB_MAXTEXTLEN);
			int ilLen = min(JOB_MAXTEXTLEN,strlen(pcpText));
			prpJob->Text[ilLen] = '\0';
		}
		else
		{
			strcpy(prpJob->Text,"");
		}

		if(pcpDety != NULL)
		{
			prpJob->Dety[0] = pcpDety[0];
			prpJob->Dety[1] = '\0';
		}
		else
		{
			strcpy(prpJob->Dety,"");
		}

		if (bgIsPreplanMode == TRUE)
		{
			prpJob->DisplayInPrePlanTable = TRUE;
		}

		prpJob->Ujty = ogJtyData.GetJtyUrnoByName(prpJob->Jtco);
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
		if(prlEmp != NULL)
		{
			prpJob->Ustf = prlEmp->Urno;
		}

		prpJob->Udem = lpUdem;
	}
}

//////////////////////////////////////////////////////////////////////////////
/////////////////// get functions ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void CedaJobData::GetJobsWithoutDemands(CCSPtrArray<JOBDATA> &ropJobs, long lpArrUrno, long lpDepUrno, CString opAlid, CString opAloc, int ipDemType /*= PERSONNELDEMANDS*/)
{
	JOBDATA  *prlJob;
	CMapPtrToPtr *pomFlightJobMap;

	if(lpArrUrno != 0L)
	{
		if(omFlightMap.Lookup((void *)lpArrUrno,(void *& )pomFlightJobMap) == TRUE)
		{
			for(POSITION rlPos =  pomFlightJobMap->GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				pomFlightJobMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
				//bool blDelegatedFlight =  !strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT) || !strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT);
				if(!strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT))
					continue;
				if (!ogJodData.JobHasDemands(llUrno) && 
					(opAlid.IsEmpty() || opAlid == prlJob->Alid) && 
					(opAloc.IsEmpty() || opAloc == prlJob->Aloc) &&
					ogBasicData.JobIsOfType(prlJob->Urno, ipDemType) && (*prlJob->Dety == '0' || *prlJob->Dety == '1'))
				{
					ropJobs.Add(prlJob);
				}
			}
		}
	}

	else if(lpDepUrno != 0L)
	{
		if(omFlightMap.Lookup((void *)lpDepUrno,(void *& )pomFlightJobMap) == TRUE)
		{
			for(POSITION rlPos =  pomFlightJobMap->GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				pomFlightJobMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
				//bool blDelegatedFlight =  !strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT) || !strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT);
				bool blDelegatedFlight =  !strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT);
				if (!ogJodData.JobHasDemands(llUrno) &&
					(opAlid.IsEmpty() || opAlid == prlJob->Alid) && 
					(opAloc.IsEmpty() || opAloc == prlJob->Aloc || blDelegatedFlight) &&
					ogBasicData.JobIsOfType(prlJob->Urno, ipDemType) && (*prlJob->Dety == '0' || *prlJob->Dety == '2'))
				{
					ropJobs.Add(prlJob);
				}
			}
		}
	}
}


// return either inbound+turnaround jobs for an arrival or outbound+turnaround jobs for a departure
// if bpForWholeRotation is true then return inbound+turnaround+outbound jobs
bool CedaJobData::GetJobsByFlur(CCSPtrArray<JOBDATA> &ropJobs, long lpFlur, BOOL bpWantJOBFM,CString opAlid,CString opAloc,int ipDemType /*= PERSONNELDEMANDS*/,bool bpForWholeRotation /* = false*/,int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	JOBDATA  *prlJob;
	CMapPtrToPtr *pomFlightJobMap;

	if (omFlightMap.Lookup((void *)lpFlur,(void *& )pomFlightJobMap) == TRUE)
	{
		for(POSITION rlPos =  pomFlightJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			pomFlightJobMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
			if(bpWantJOBFM || strcmp(prlJob->Jtco,JOBFM))
			{
				if ((opAlid.IsEmpty() || opAlid == prlJob->Alid) && (opAloc.IsEmpty() || opAloc == prlJob->Aloc) &&
					ogBasicData.JobIsOfType(prlJob->Urno, ipDemType) && 
					((ipReturnFlightType == ID_NOT_RETURNFLIGHT || *prlJob->Dety == '0') ||
					(ipReturnFlightType == ID_ARR_RETURNFLIGHT && *prlJob->Dety == '1') ||
					(ipReturnFlightType == ID_DEP_RETURNFLIGHT && *prlJob->Dety == '2')))
				{
					ropJobs.Add(prlJob);
				}
			}
		}
	}

	// get any turnaround jobs using the rotation flight
	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(lpFlur,ipReturnFlightType);
	if(prlRotationFlight != NULL)
	{
		if (omFlightMap.Lookup((void *)prlRotationFlight->Urno,(void *& )pomFlightJobMap) == TRUE)
		{
			int ilRotationReturnFlightType = ogFlightData.GetRotationReturnFlightType(ogFlightData.GetFlightByUrno(lpFlur), prlRotationFlight, ipReturnFlightType);
			for(POSITION rlPos =  pomFlightJobMap->GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				pomFlightJobMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
				if(bpWantJOBFM || strcmp(prlJob->Jtco,JOBFM))
				{
					// return either inbound+turnaround jobs for an arrival or outbound+turnaround jobs for a departure
					// if bpForWholeRotation is true then return inbound+turnaround+outbound jobs
					if ((opAlid.IsEmpty() || opAlid == prlJob->Alid) &&	(opAloc.IsEmpty() || opAloc == prlJob->Aloc) &&
						(IsTurnaroundJob(prlJob->Urno) || bpForWholeRotation) && ogBasicData.JobIsOfType(prlJob->Urno, ipDemType) &&
						((ilRotationReturnFlightType == ID_NOT_RETURNFLIGHT || *prlJob->Dety == '0') ||
						(ilRotationReturnFlightType == ID_ARR_RETURNFLIGHT && *prlJob->Dety == '1') ||
						(ilRotationReturnFlightType == ID_DEP_RETURNFLIGHT && *prlJob->Dety == '2')))
					{
						ropJobs.Add(prlJob);
					}
				}
			}
		}
	}

    return true;
}

JOBDATA  *CedaJobData::GetMasterJob (JOBDATA  *lpJob) 
{
	if (lpJob != NULL) 
	{
		return GetJobByUrno(lpJob->Jour);
	}
	return NULL;
}

JOBDATA  *CedaJobData::GetMasterJobByUrno (long lpUrno) 
{
	JOBDATA  *lpJob = GetJobByUrno (lpUrno);
	return GetMasterJob (lpJob);
}

JOBDATA  *CedaJobData::GetJobByUrno(long lpUrno)
{
	JOBDATA  *prlJob;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlJob) == TRUE)
	{
		return prlJob;
	}
	return NULL;
}

bool CedaJobData::IsTurnaroundJob(long lpJobUrno)
{
	DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(lpJobUrno);
	return (prlDemand != NULL && ogDemandData.IsTurnaroundDemand(prlDemand->Dety)) ? true : false;
}

bool CedaJobData::GetFmJobsByFlur(CCSPtrArray<JOBDATA> &ropJobs, long lpFlur)
{

	JOBDATA  *prlJob;
    ropJobs.RemoveAll();
	CMapPtrToPtr *pomFlightJobMap;

	if (omFlightFmMap.Lookup((void *)lpFlur,(void *& )pomFlightJobMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  pomFlightJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			pomFlightJobMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
			ropJobs.Add(prlJob);
		}
	}

    return true;
}


bool CedaJobData::GetJobsByPkno(CCSPtrArray<JOBDATA> &ropJobs, const char *pcpPkno, const char *pcpJobType /* = "" */, const char *pcpIgnoreJobType /* = "" */)
{
	JOBDATA  *prlJob;
    ropJobs.RemoveAll();
	CMapPtrToPtr *polPknoJobMap;

	if (omPknoMap.Lookup((LPCSTR)pcpPkno,(void *& )polPknoJobMap) == TRUE)
	{
		bool blAllJobTypes = !strcmp(pcpJobType,"") ? true : false;
		bool blNoIgnore = !strcmp(pcpIgnoreJobType,"") ? true : false;

		long llUrno;
		POSITION rlPos;
		for(rlPos =  polPknoJobMap->GetStartPosition(); rlPos != NULL; )
		{
			polPknoJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			if((blAllJobTypes || !strcmp(prlJob->Jtco,pcpJobType)) &&
				(blNoIgnore || strcmp(prlJob->Jtco,pcpIgnoreJobType)))
			{
				ropJobs.Add(prlJob);
			}
		}
	}

    return true;
}

void CedaJobData::GetJobsByUstf(CCSPtrArray<JOBDATA> &ropJobs, long lpUstf, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob;
	CMapPtrToPtr *polUstfJobMap;

	if(omUstfMap.Lookup((void *)lpUstf,(void *& )polUstfJobMap) == TRUE)
	{
		for(POSITION rlPos = polUstfJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polUstfJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			ropJobs.Add(prlJob);
		}
	}
}

void CedaJobData::GetJobsByUdem(CCSPtrArray<JOBDATA> &ropJobs, long lpUdem, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob;
	CMapPtrToPtr *polUdemJobMap;

	if(omUdemMap.Lookup((void *)lpUdem,(void *& )polUdemJobMap) == TRUE)
	{
		for(POSITION rlPos = polUdemJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polUdemJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			ropJobs.Add(prlJob);
		}
	}
}

// return true if the demand referenced by lpUdem (DEM.URNO) has jobs assigned
bool CedaJobData::DemandHasJobs(long lpUdem)
{
	CMapPtrToPtr *polUdemJobMap;
	return (omUdemMap.Lookup((void *)lpUdem,(void *& )polUdemJobMap) && polUdemJobMap->GetCount() > 0) ? true : false;
}

bool CedaJobData::JobHasDemand(long lpUjob)
{
	return JobHasDemand(GetJobByUrno(lpUjob));
}

bool CedaJobData::JobHasDemand(JOBDATA *prpJob)
{
	return (prpJob != NULL && prpJob->Udem != 0L) ? true : false;
}

long CedaJobData::GetFirstJobUrnoByDemand(long lpUdem)
{
	long  llJobUrno = 0L;
	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByUdem(olJobs, lpUdem);
	if(olJobs.GetSize() > 0)
	{
		llJobUrno = olJobs[0].Urno;
	}

	return llJobUrno;
}

void CedaJobData::GetJobsByJour(CCSPtrArray<JOBDATA> &ropJobs, long lpJour, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob;
	CMapPtrToPtr *polJourJobMap;

	if(omJourMap.Lookup((void *)lpJour,(void *& )polJourJobMap) == TRUE)
	{
		for(POSITION rlPos = polJourJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polJourJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			ropJobs.Add(prlJob);
		}
	}
}

void CedaJobData::GetJobsByUequ(CCSPtrArray<JOBDATA> &ropJobs, long lpUequ, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob;
	CMapPtrToPtr *polUequJobMap;

	if(omUequMap.Lookup((void *)lpUequ,(void *& )polUequJobMap) == TRUE)
	{
		for(POSITION rlPos = polUequJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polUequJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			ropJobs.Add(prlJob);
		}
	}

	ropJobs.Sort(ByJobStartTime);
}

bool CedaJobData::GetJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs, const char *pcpAlid /* = "" */, const char *pcpAloc /* = "" */)
{
    ropJobs.RemoveAll();

	JOBDATA *prlJob = NULL;
	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;
	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;

	if(!blIgnoreAlid)
	{
		long llUrno;
		CMapPtrToPtr *polSingleMap;
		long llUaid = ogBasicData.GetUaidForJob(pcpAlid,ogAloData.GetAloUrnoByName(pcpAloc));
		if(omUaidMap.Lookup((void *&) llUaid, (void *& )polSingleMap) == TRUE)
		{
			for(POSITION rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
				if(blIgnoreAloc || !strcmp(prlJob->Aloc,pcpAloc))
					ropJobs.Add(prlJob);
			}
		}
	}
	else
	{
		int ilNumJobs = omData.GetSize();
		for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			prlJob = &omData[ilJob];

			if(blIgnoreAloc || !strcmp(prlJob->Aloc,pcpAloc))
			{
				ropJobs.Add(prlJob);
			}
		}
	}
    return (ropJobs.GetSize() > 0) ? true : false;
}

bool CedaJobData::GetSpecialJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs, const char *pcpAlid /* = "" */, const char *pcpAloc /* = "" */)
{
    ropJobs.RemoveAll();

	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;
	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;

	int ilNumJobs = omData.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &omData[ilJob];

		if(!strcmp(prlJob->Jtco,JOBSTAIRCASE) && (blIgnoreAlid || !strcmp(prlJob->Alid,pcpAlid)) && (blIgnoreAloc || !strcmp(prlJob->Aloc,pcpAloc)))
		{
			ropJobs.Add(prlJob);
		}
	}
    return (ropJobs.GetSize() > 0) ? true : false;
}

bool CedaJobData::GetJobsByPoolJob(CCSPtrArray<JOBDATA> &ropJobs, long lpPoolJobUrno, bool bpReset /*= true*/)
{
	if(bpReset)
	{
	    ropJobs.RemoveAll();
	}

	CCSPtrArray<JOBDATA> olJobs;
	GetJobsByJour(olJobs, lpPoolJobUrno);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &olJobs[ilJob];
		if(strcmp(prlJob->Jtco,JOBPOOL)  && strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK) && strcmp(prlJob->Jtco,JOBTEMPABSENCE))
		{
			ropJobs.Add(prlJob);
		}
	}

    return true;
}

bool CedaJobData::GetJobsByPoolJobAndType(CCSPtrArray<JOBDATA> &ropJobs, long lpPoolJobUrno, char *pcpJobType, bool bpReset /*= true*/)
{
	if(bpReset)
	    ropJobs.RemoveAll();

	CCSPtrArray<JOBDATA> olJobs;
	GetJobsByJour(olJobs, lpPoolJobUrno);
	for(int ilJob = 0; ilJob < olJobs.GetSize(); ilJob++)
	{
		JOBDATA *prlJob = &olJobs[ilJob];
		if(!strcmp(prlJob->Jtco, pcpJobType))
		{
			ropJobs.Add(prlJob);
		}
	}

    return true;
}

// if opFrom/opTo are not TIMENULL, then return only those pool jobs that encompass opFrom/opTo
void CedaJobData::GetPoolJobsByUstf(CCSPtrArray<JOBDATA> &ropPoolJobs, long lpUstf, CTime opFrom /*= TIMENULL*/, CTime opTo /*= TIMENULL*/)
{
    ropPoolJobs.RemoveAll();

	bool blCheckTime = (opFrom != TIMENULL && opTo != TIMENULL) ? true : false;
	GetJobsByUstf(ropPoolJobs, lpUstf);
	for(int ilJ = (ropPoolJobs.GetSize()-1); ilJ >= 0; ilJ--)
	{
		JOBDATA *prlJob = &ropPoolJobs[ilJ];
		if(strcmp(prlJob->Jtco,JOBPOOL) || (blCheckTime && !IsWithIn(opFrom,opTo,prlJob->Acfr,prlJob->Acto)))
		{
			ropPoolJobs.RemoveAt(ilJ);
		}
	}
}

bool CedaJobData::GetJobsByPoolId(CCSPtrArray<JOBDATA> &ropJobs, CString opPoolId)
{
    ropJobs.RemoveAll();

	int ilMaxCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMaxCount; ilLc++)
	{
		if (omData[ilLc].Alid == opPoolId)
		{
			if (strcmp(omData[ilLc].Jtco,JOBPOOL) == 0)
			{
				ropJobs.Add(&omData[ilLc]);
			}
		}
	}
		
    return true;
}

bool CedaJobData::GetJobsByShur(CCSPtrArray<JOBDATA> &ropJobs, long lpShur, BOOL bpWantPoolJobOnly, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob;
	CMapPtrToPtr *polShurJobMap;

	if(omShurMap.Lookup((void *)lpShur,(void *& )polShurJobMap) == TRUE)
	{
		for(POSITION rlPos = polShurJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polShurJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			if(!bpWantPoolJobOnly || !strcmp(prlJob->Jtco,JOBPOOL))
			{
				ropJobs.Add(prlJob);
			}
		}
	}

//	ropJobs.Sort(ByJobStartTime);
	return true;
}

int CedaJobData::GetJobsByShurAndType(CCSPtrArray<JOBDATA> &ropJobs, long lpShur, char *pcpJobType /* = "" -> ALL JOB TYPES */, bool bpReset /* = true */)
{
	if(bpReset)
		ropJobs.RemoveAll();

	bool blIgnoreJobType = (strlen(pcpJobType) <= 0) ? true : false;
	JOBDATA *prlJob;
	CMapPtrToPtr *polShurJobMap;
	if(omShurMap.Lookup((void *)lpShur,(void *& )polShurJobMap) == TRUE)
	{
		for(POSITION rlPos = polShurJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polShurJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			if(blIgnoreJobType || !strcmp(prlJob->Jtco,pcpJobType))
			{
				ropJobs.Add(prlJob);
			}
		}
	}

	return ropJobs.GetSize();
}

bool CedaJobData::GetFmgJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs,char const *pcpAlid)
{
    ropJobs.RemoveAll();

	int ilMaxCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMaxCount; ilLc++)
	{
		if(!strcmp(omData[ilLc].Jtco,JOBFMGATEAREA) &&	!strcmp(omData[ilLc].Alid,pcpAlid))
		{
			ropJobs.Add(&omData[ilLc]);
		}
	}

    return true;
}

bool CedaJobData::JobForUprmExist(const long lpUprm)
{
	JOBDATA *prlJob;
	return omUprmMap.Lookup((void *)lpUprm,(void *& )prlJob) ? true : false;
}

// lli: make use of the existing omUprmMap to find job by UPRM. 
void CedaJobData::GetJobsByUprm(CCSPtrArray<JOBDATA> &ropJobs, long lpUprm, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob;
	CMapPtrToPtr *polUprmJobMap;

	if(omUprmMap.Lookup((void *)lpUprm,(void *& )polUprmJobMap) == TRUE)
	{
		for(POSITION rlPos = polUprmJobMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polUprmJobMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlJob);
			ropJobs.Add(prlJob);
		}
	}
}

bool CedaJobData::GetFmcJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs, char const *pcpAlid)
{
    ropJobs.RemoveAll();

	int ilMaxCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMaxCount; ilLc++)
	{
		if(!strcmp(omData[ilLc].Jtco,JOBFMCCIAREA) && !strcmp(omData[ilLc].Alid,pcpAlid))
		{
			ropJobs.Add(&omData[ilLc]);
		}
	}

    return true;
}

bool CedaJobData::GetFmrJobsByAlid(CCSPtrArray<JOBDATA> &ropJobs,char const *pcpAlid)
{
    ropJobs.RemoveAll();

	int ilMaxCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMaxCount; ilLc++)
	{
		if(!strcmp(omData[ilLc].Jtco,JOBFMREGNAREA) && !strcmp(omData[ilLc].Alid,pcpAlid))
		{
			ropJobs.Add(&omData[ilLc]);
		}
	}

    return true;
}

JOBDATA *CedaJobData::GetPoolJobForDetachJob(long lpJobDetachUrno)
{
	JOBDATA *prlJobDetach;
	if ((prlJobDetach = GetJobByUrno(lpJobDetachUrno)) == NULL)
	{
		return NULL;
	}

	CCSPtrArray<JOBDATA> olJobs;
	GetJobsByJour(olJobs, prlJobDetach->Jour);
	for (int i = 0; i < olJobs.GetSize(); i++)
	{
		JOBDATA *prlJob = &olJobs[i];
		if (strcmp(prlJob->Jtco,JOBPOOL) == 0)
		{
			if (prlJob->Acfr == prlJobDetach->Acfr &&
				prlJob->Acto == prlJobDetach->Acto)	// exactly the same time?
			{
				return prlJob;
			}
		}
	}
	return NULL;	// not found
}

bool CedaJobData::GetTeamDelegationsForPoolJob(long lpPoolJobUrno, CCSPtrArray <JOBDATA> &ropJobs, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	JOBDATA *prlJob = NULL;
	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByJour(olJobs, lpPoolJobUrno);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		prlJob = &olJobs[ilJ];
		if(!strcmp(prlJob->Jtco, JOBTEAMDELEGATION))
		{
			ropJobs.Add(prlJob);
		}
	}

	return (ropJobs.GetSize() > 0) ? true : false;
}

bool CedaJobData::GetPoolJobsForTeamDelegation(long lpJobDetachUrno,CCSPtrArray <JOBDATA> &ropJobs, bool bpReset)
{
	if (bpReset)
		ropJobs.RemoveAll();

	JOBDATA *prlJobDetach;
	if ((prlJobDetach = GetJobByUrno(lpJobDetachUrno)) == NULL)
	{
		return false;
	}

	JOBDATA *prlJob = GetPoolJobForDetachJob(lpJobDetachUrno);
	if (prlJob)
		ropJobs.Add(prlJob);

	prlJob = GetJobByUrno(prlJobDetach->Jour);
	if (prlJob)
		ropJobs.Add(prlJob);

	return (ropJobs.GetSize() > 0) ? true : false;
}


JOBDATA *CedaJobData::GetJobPoolByPreplanData(CString opPeno,CString opAlid,
												CTime opAcfr,CTime opActo)
{
	int ilJobCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilJobCount; ilLc++)
	{
		if (omData[ilLc].Acfr == opAcfr && omData[ilLc].Acto == opActo)
		{
			if (opPeno == omData[ilLc].Peno)
			{
				if (opAlid ==  omData[ilLc].Alid)
					return &omData[ilLc];
			}
		}
	}
	return NULL;
}


// Return JobPool of the given job (URNO is specified)
JOBDATA *CedaJobData::GetJobPoolByUrno(long lpJobUrno)
{
	JOBDATA *prlJob;
	if ((prlJob = GetJobByUrno(lpJobUrno)) == NULL)
	{
		return NULL;
	}

	return GetJobByUrno(prlJob->Jour);
}

CString CedaJobData::GetJobStatusString(JOBDATA *prpJob)
{
	CString olStatus;

	if(prpJob != NULL)
	{
		switch(prpJob->Stat[0])
		{
		case 'P':
			olStatus = GetString(IDS_JOB_PLANNED);
			break;
		case 'C':
			olStatus = GetString(IDS_JOB_STARTED);
			break;
		case 'F':
			olStatus = GetString(IDS_JOB_FINISHED);
			break;
		default:
			olStatus = "Unknown Job Status!";
		}
	}

	return olStatus;
}


int CedaJobData::GetJobsByType(const char *pcpJobType, CCSPtrArray <JOBDATA> &ropJobs, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropJobs.RemoveAll();
	}

	int ilNumJobs = omData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		if(!strcmp(omData[ilJ].Jtco,pcpJobType))
		{
			ropJobs.Add(&omData[ilJ]);
		}
	}

	return ropJobs.GetSize();
}

int CedaJobData::GetEquipmentFastLinkJobsByPoolJob(CCSPtrArray<JOBDATA> &ropFastLinkJobs, CCSPtrArray<JOBDATA> &ropPoolJobs)
{
	int ilNumPJ = ropPoolJobs.GetSize();
	for(int ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
	{
		GetEquipmentFastLinkJobsByPoolJob(ropFastLinkJobs, ropPoolJobs[ilPJ].Urno);
	}

    return ropFastLinkJobs.GetSize();
}

int CedaJobData::GetEquipmentFastLinkJobsByPoolJob(CCSPtrArray<JOBDATA> &ropFastLinkJobs, long lpPoolJobUrno)
{
	CCSPtrArray<JOBDATA> olJobs;
	GetJobsByJour(olJobs, lpPoolJobUrno);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &olJobs[ilJob];
		if(!strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK))
		{
			ropFastLinkJobs.Add(prlJob);
		}
	}

    return ropFastLinkJobs.GetSize();
}

// get fast link jobs for the specified equipment and time - used to match new equipment jobs
// to personnel demands. Return true if a fast link is found that completely contains opStart/opEnd.
bool CedaJobData::GetEquipmentFastLinkJobsByUequAndTime(CCSPtrArray <FASTLINK> &ropFastLinks, long lpUequ, CTime opStart, CTime opEnd)
{
	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByUequ(olJobs, lpUequ);
	return FilterFastLinkJobs(olJobs, ropFastLinks, opStart, opEnd);
}

// get fast link jobs for the specified employee and time - used to match new equipment jobs
// to personnel demands. Return true if a fast link is found that completely contains opStart/opEnd.
bool CedaJobData::GetEquipmentFastLinkJobsByUstfAndTime(CCSPtrArray <FASTLINK> &ropFastLinks, long lpUstf, CTime opStart, CTime opEnd)
{
	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByUstf(olJobs, lpUstf);
	return FilterFastLinkJobs(olJobs, ropFastLinks, opStart, opEnd);
}

bool CedaJobData::FilterFastLinkJobs(CCSPtrArray <JOBDATA> &ropSourceJobs, CCSPtrArray <FASTLINK> &ropFastLinks, CTime opStart, CTime opEnd)
{
	bool blMatchFound = false; // return true if a fast link is found that completely contains opStart/opEnd
	CCSPtrArray <JOBDATA> olNotFullyOverlappingJobs;

	int ilNumJobs = ropSourceJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &ropSourceJobs[ilJob];
		if(!strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK))
		{
			if(IsWithIn(opStart,opEnd,prlJob->Acfr,prlJob->Acto))
			{
				// fast link that completely overlaps opStart/opEnd
				FASTLINK *prlFastLink = new FASTLINK;
				prlFastLink->Data.Add(prlJob);
				ropFastLinks.Add(prlFastLink);
				blMatchFound = true;
			}
			else if(IsOverlapped(opStart,opEnd,prlJob->Acfr,prlJob->Acto))
			{
				// fast link that partially overlaps opStart/opEnd
				olNotFullyOverlappingJobs.Add(prlJob);
			}
		}
	}

	if(!blMatchFound) // no single fast links found that cover completely opStart/opEnd
	{
		olNotFullyOverlappingJobs.Sort(ByJobStartTime);
		ilNumJobs = olNotFullyOverlappingJobs.GetSize();
		if(ilNumJobs == 1)
		{
			// return the single partially overlapping job
			FASTLINK *prlFastLink = new FASTLINK;
			prlFastLink->Data.Add(&olNotFullyOverlappingJobs[0]);
			ropFastLinks.Add(prlFastLink);
		}
		else
		{
			// return pairs of fast links that together overlap opStart/opEnd
			for(int ilJ1 = 0; ilJ1 < (ilNumJobs-1); ilJ1++)
			{
				JOBDATA *prlJob1 = &olNotFullyOverlappingJobs[ilJ1];
				for(int ilJ2 = (ilJ1+1); ilJ2 < ilNumJobs; ilJ2++)
				{
					JOBDATA *prlJob2 = &olNotFullyOverlappingJobs[ilJ2];
					if(IsOverlapped(prlJob1->Acfr,prlJob1->Acto,prlJob2->Acfr,prlJob2->Acto))
					{
						CTime olStart = (prlJob1->Acfr < prlJob2->Acfr) ? prlJob1->Acfr : prlJob2->Acfr;
						CTime olEnd = (prlJob1->Acto > prlJob2->Acto) ? prlJob1->Acto : prlJob2->Acto;
						if(IsWithIn(opStart,opEnd,olStart,olEnd))
						{
							// pair of fast links that together cover opStart/opEnd
							FASTLINK *prlFastLink = new FASTLINK;
							prlFastLink->Data.Add(prlJob1);
							prlFastLink->Data.Add(prlJob2);
							ropFastLinks.Add(prlFastLink);
						}
					}
				}
			}
		}
	}

    return blMatchFound;
}

//////////////////////////////////////////////////////////////////////////////
////////// helper functions //////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

bool CedaJobData::IsTurnaroundJob(JOBDATA *prpJob)
{
	return (prpJob != NULL) ? IsTurnaroundJob(prpJob->Urno) : false;
}


// return true if the pool job is delegated to another pool or another team
bool CedaJobData::PoolJobIsDelegated(long lpPoolJobUrno)
{
	bool blIsDelegated = false;

	JOBDATA *prlJob = NULL;
	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByJour(olJobs, lpPoolJobUrno);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		prlJob = &olJobs[ilJ];
		if(!strcmp(prlJob->Jtco, JOBTEAMDELEGATION) || !strcmp(prlJob->Jtco, JOBDETACH))
		{
			blIsDelegated = true;
			break;
		}
	}

	return blIsDelegated;
}

// return true if this pool job has been created as the result of a delegation from another pool or team
bool CedaJobData::PoolJobIsDelegation(JOBDATA *prpPoolJob)
{
	bool blIsDelegation = false;

	if(prpPoolJob != NULL)
	{
		JOBDATA *prlParentPoolJob = GetJobByUrno(prpPoolJob->Jour);
		if(prlParentPoolJob != NULL && !strcmp(prlParentPoolJob->Jtco,JOBPOOL))
		{
			blIsDelegation = true;
		}
	}

	return blIsDelegation;
}

bool CedaJobData::EmpIsFree(long lpUstf, CTime opFrom, CTime opTo)
{
	bool blEmpIsFree = true;

	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByUstf(olJobs, lpUstf);
	int ilNumJobs = olJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlJob = &olJobs[ilJob];
		if(strcmp(prlJob->Jtco,JOBPOOL) && strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK) && strcmp(prlJob->Jtco,JOBTEMPABSENCE))
		{
			if(IsOverlapped(prlJob->Acfr, prlJob->Acto, opFrom, opTo))
			{
				blEmpIsFree = false;
				break;
			}
		}
	}

	return blEmpIsFree;
}

CString CedaJobData::Dump(long lpUrno)
{
	CString olDumpStr;
	JOBDATA *prlJob = GetJobByUrno(lpUrno);
	if(prlJob == NULL)
	{
		olDumpStr.Format("No JOBDATA Found for JOB.URNO <%ld>",lpUrno);
	}
	else
	{
		JOBDATA rlJob;
		memcpy(&rlJob,prlJob,sizeof(JOBDATA));
		CString olData;
		MakeCedaData(&omRecInfo,olData,&rlJob);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + "> ";
			}
		}
		CString olExtraFields;
		olExtraFields.Format("  NON-DB: Jtco = <%s> Peno = <%s> Alid = <%s> Aloc = <%s> Chng = <%s> Ignr = <%s> Infm = <%s> Regn = <%s>  OLD VALUES: Act3 = <%s> Posi = <%s> Gate = <%s> ",
			prlJob->Jtco,prlJob->Peno,prlJob->Alid,prlJob->Aloc,prlJob->Chng,prlJob->Ignr,prlJob->Infm?"true":"false",prlJob->Regn,prlJob->Act3,prlJob->Posi,prlJob->Gate);
		olDumpStr += olExtraFields;
	}
	return olDumpStr;
}

CString CedaJobData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaJobData::GetLastError()
{
	return omLastErrorMessage;
}

///////////////////////////////////////////////////////////////////////////
// Offline functions
///////////////////////////////////////////////////////////////////////////

// returns true if the job has been deleted offline
// this function is required in the following situation
// 1. the job is deleted offline
// 2. another OpssPm working online updates the job
// 3. update job broadcast is received by the offline OpssPm
// 4. the updated job should not be re-inserted into the offline data, but the online data should be
bool CedaJobData::WasDeletedOffline(long lpJobUrno)
{
	JOBDATA *prlJob = NULL;
	return omDeletedOfflineJobs.Lookup((void *)lpJobUrno,(void *& )prlJob) ? true : false;
}

JOBDATA *CedaJobData::GetOnlineJobByUrno(long lpJobUrno)
{
	JOBDATA *prlJob = NULL;
	omOnlineUrnoMap.Lookup((void *)lpJobUrno,(void *& )prlJob);
	return prlJob;
}

void CedaJobData::DeleteFromOnlineData(long lpJobUrno)
{
	if(bmIsOffline)
	{
		if(GetOnlineJobByUrno(lpJobUrno) != NULL)
		{
			for(int ilJ = (omOnlineData.GetSize() - 1); ilJ >= 0; ilJ--)
			{
				if(omOnlineData[ilJ].Urno == lpJobUrno)
				{
					omOnlineData.DeleteAt(ilJ);
				}
			}
		}

		// this job was deleted offline and has now been deleted in another instance of
		// opsspm online, so it can be deleted from the list of deleted offline jobs
		JOBDATA *prlJob = NULL;
		if(omDeletedOfflineJobs.Lookup((void *)lpJobUrno,(void *& )prlJob))
		{
			omDeletedOfflineJobs.RemoveKey((void *)lpJobUrno);
		}
	}
}

void CedaJobData::UpdateOnlineData(JOBDATA *prpJob)
{
	if(prpJob != NULL && bmIsOffline)
	{
		JOBDATA *prlJob = NULL;
		if(omOnlineUrnoMap.Lookup((void *)prpJob->Urno,(void *& )prlJob))
		{
			// update
			*prlJob = *prpJob;
		}
		else
		{
			// insert
			JOBDATA *prlJob = AllocNewJob(prpJob);
			omOnlineData.Add(prlJob);
			omOnlineUrnoMap.SetAt((void *)prlJob->Urno,prlJob);
		}
	}
}

// set offline flag and copy all jobs into a backup array
void CedaJobData::SetOffline()
{
	bmIsOffline = true;
	CreateBackupData();
}

void CedaJobData::CreateBackupData()
{
	omDeletedOfflineJobs.RemoveAll();
	omOnlineUrnoMap.RemoveAll();
	omOnlineData.DeleteAll();
	bmNoOfflineChangesYet = true;

	int ilNumJobs = omData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		// will insert all jobs into omOnlineData
		UpdateOnlineData(&omData[ilJ]);
	}
}

void CedaJobData::SetOnline(bool bpRelease)
{
	bmIsOffline = false;

	if(bpRelease)
	{
		ReleaseJobsChangedOffline();
	}
	else
	{
		RollbackJobsChangedOffline();
	}

	omDeletedOfflineJobs.RemoveAll();
	omOnlineUrnoMap.RemoveAll();
	omOnlineData.DeleteAll();
}

void CedaJobData::SetReleaseButton(void)
{
	if(bmNoOfflineChangesYet)
	{
		if(JobsChangedOffline())
		{
			ogCCSDdx.DataChanged((void *)this, OFFLINE_CHANGES, NULL);
			bmNoOfflineChangesYet = false;
		}
	}
}

void CedaJobData::RollbackJobsChangedOffline(void)
{
	// delete offline data
	ClearAll();

	// copy the online data back to main data
	int ilNumJobs = omOnlineData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		JOBDATA *prlJob = AllocNewJob(&omOnlineData[ilJ]);
		AddJobInternal(prlJob);
	}
}

void CedaJobData::ReleaseJobsChangedOffline(void)
{
	JOBDATA *prlOfflineJob = NULL, *prlOnlineJob = NULL;
	void *pvlDummy = NULL;
	long llUrno = 0L;
	CString olNewLine("\n"), olComma(","), olUrno;
	int ilDrtCount = 0, ilIrtCount = 0, ilUrtCount = 0;

	int ilFields = ogBasicData.GetItemCount(CString(pcmAllExistingFields)); 
	CString olIrt; olIrt.Format("*CMD*,%s,IRT,%d,%s\n", pcmTableName, ilFields, CString(pcmAllExistingFields));
	CString olUrt; olUrt.Format("*CMD*,%s,URT,%d,%s,[URNO=:VURNO]\n", pcmTableName, ilFields, CString(pcmAllExistingFields));
	CString olDrt; olDrt.Format("*CMD*,%s,DRT,-1,,[URNO=:VURNO]\n", pcmTableName);

	// release deleted jobs
	for(POSITION rlPos = omDeletedOfflineJobs.GetStartPosition(); rlPos != NULL; )
	{
		omDeletedOfflineJobs.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )pvlDummy);
#ifdef USE_RELEASE_HANDLER
		olUrno.Format("%ld", llUrno);
		olDrt += olUrno + olNewLine;
		ilDrtCount++;
#else
		DbDeleteJob2(llUrno);
#endif

	}
	if(ilDrtCount > 0)
	{
		Release(olDrt);
	}

	// loop through normal data and check for inserts and updates
	CString olData;
	int ilNumJobs = omData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		prlOfflineJob = &omData[ilJ];
		if((prlOnlineJob = GetOnlineJobByUrno(prlOfflineJob->Urno)) != NULL)
		{
			if(*prlOfflineJob != *prlOnlineJob)
			{
#ifdef USE_RELEASE_HANDLER
				PrepareDataForWrite(prlOfflineJob);
				ConvertDatesToUtc(prlOfflineJob);
				olUrno.Format("%ld", prlOfflineJob->Urno);
				MakeCedaData(&omRecInfo,olData,prlOfflineJob);
				olUrt += olData + olComma + olUrno + olNewLine;
				ilUrtCount++;
				ConvertDatesToLocal(prlOfflineJob);
#else
				DbUpdateJob2(prlOfflineJob, prlOnlineJob);
#endif
			}
		}
		else
		{
#ifdef USE_RELEASE_HANDLER
			char pclData[2000] = "";
			char pclInsertFieldList[2000] = "";

			PrepareDataForWrite(prlOfflineJob);
			ConvertDatesToUtc(prlOfflineJob);
			//GetNewFields(prlOfflineJob, pclInsertFieldList, pclData);
			GetAllFields(prlOfflineJob, pclInsertFieldList, pclData);
			olIrt += CString(pclData) + olNewLine;
			ilIrtCount++;
			ConvertDatesToLocal(prlOfflineJob);
#else
			DbInsertJob2(prlOfflineJob);
#endif
		}
	}

	if(ilUrtCount > 0)
	{
		Release(olUrt);
	}
	
	if(ilIrtCount > 0)
	{
		Release(olIrt);
	}

	// recreate the backup data - release button was pressed so the backup
	// data and the offline data should now be the same
	CreateBackupData();
}

bool CedaJobData::Release(CString opReleaseString)
{
	bool blRc = false;

	int ilLen = opReleaseString.GetLength();
	if(ilLen > 0)
	{
		char *pclDataArea = (char *) malloc(ilLen + 1);
		strcpy(pclDataArea, opReleaseString);
		if((blRc = CedaAction("REL","QUICK","",pclDataArea)) == false)
		{
			ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,"REL",pcmFieldList,pcmTableName,"Release()");
		}
		free((char *)pclDataArea);
	}

	return blRc;
}

bool CedaJobData::JobsChangedOffline(void)
{
	if(omDeletedOfflineJobs.GetCount())
	{
		return true;
	}

	int ilNumJobs = omData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		if(JobChangedOffline(&omData[ilJ]))
			return true;
	}

	return false;
}

bool CedaJobData::JobChangedOffline(JOBDATA *prpOfflineJob)
{
	if(prpOfflineJob != NULL)
	{
		JOBDATA *prlOnlineJob = GetOnlineJobByUrno(prpOfflineJob->Urno);
		if(prlOnlineJob == NULL || *prpOfflineJob != *prlOnlineJob)
		{
			return true;
		}
	}

	return false;
}

void CedaJobData::GetJobsChangedOffline(CUIntArray &ropJobUrnos)
{
	JOBDATA *prlOfflineJob, *prlOnlineJob;
	CString olJobText;
	long llUrno;
	void *pvlDummy;

	int ilNumJobs = omData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		prlOfflineJob = &omData[ilJ];
		if((prlOnlineJob = GetOnlineJobByUrno(prlOfflineJob->Urno)) == NULL)
		{
			ropJobUrnos.Add(prlOfflineJob->Urno);
		}
		else if(*prlOfflineJob != *prlOnlineJob)
		{
			ropJobUrnos.Add(prlOfflineJob->Urno);
		}
	}
	for(POSITION rlPos = omDeletedOfflineJobs.GetStartPosition(); rlPos != NULL; )
	{
		omDeletedOfflineJobs.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )pvlDummy);
		if((prlOnlineJob = GetOnlineJobByUrno(llUrno)) != NULL)
		{
			ropJobUrnos.Add(llUrno);
		}
	}
}

// the transaction handling allows you send a block of changes to the release
// handler, to use it:
// 1) Call StartTransaction()
// 2) Do inserts/updates/deletes as usual
// 3) Call CommitTransaction() to release all changes
//
void CedaJobData::StartTransaction(void)
{
	bmTransactionActivated = true;
}

void CedaJobData::CommitTransaction(void)
{
	JOBDATA *prlJob = NULL;
	void *pvlDummy = NULL;
	long llUrno = 0L;
	CString olNewLine("\n"), olComma(","), olUrno;
	int ilDrtCount = 0, ilIrtCount = 0, ilUrtCount = 0;
	POSITION rlPos;

	int ilFields = ogBasicData.GetItemCount(CString(pcmAllExistingFields)); 
	CString olIrt; olIrt.Format("*CMD*,%s,IRT,%d,%s\n", pcmTableName, ilFields, CString(pcmAllExistingFields));
	CString olUrt; olUrt.Format("*CMD*,%s,URT,%d,%s,[URNO=:VURNO]\n", pcmTableName, ilFields, CString(pcmAllExistingFields));
	CString olDrt; olDrt.Format("*CMD*,%s,DRT,-1,,[URNO=:VURNO]\n", pcmTableName);

	// release deleted jobs
	for(rlPos = omDrtJobs.GetStartPosition(); rlPos != NULL; )
	{
		omDrtJobs.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )pvlDummy);
		olUrno.Format("%ld", llUrno);
		olDrt += olUrno + olNewLine;
		ilDrtCount++;
	}
	if(ilDrtCount > 0)
	{
		Release(olDrt);
	}

	for(rlPos = omUrtJobs.GetStartPosition(); rlPos != NULL; )
	{
		char pclData[2000] = "";
		char pclUpdateFieldList[2000] = "";

		omUrtJobs.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
		PrepareDataForWrite(prlJob);
		ConvertDatesToUtc(prlJob);
		olUrno.Format("%ld", prlJob->Urno);
		GetAllFields(prlJob, pclUpdateFieldList, pclData);
		olUrt += CString(pclData) + olComma + olUrno + olNewLine;
		ilUrtCount++;
		ConvertDatesToLocal(prlJob);
	}
	if(ilUrtCount > 0)
	{
		Release(olUrt);
	}

	for(rlPos = omIrtJobs.GetStartPosition(); rlPos != NULL; )
	{
		char pclData[2000] = "";
		char pclInsertFieldList[2000] = "";

		omIrtJobs.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlJob);
		PrepareDataForWrite(prlJob);
		ConvertDatesToUtc(prlJob);
		//GetNewFields(prlJob, pclInsertFieldList, pclData);
		GetAllFields(prlJob, pclInsertFieldList, pclData);
		olIrt += CString(pclData) + olNewLine;
		ilIrtCount++;
		ConvertDatesToLocal(prlJob);
	}
	if(ilIrtCount > 0)
	{
		Release(olIrt);
	}

	bmTransactionActivated = false;
	omDrtJobs.RemoveAll();
	omIrtJobs.RemoveAll();
	omUrtJobs.RemoveAll();
}

bool CedaJobData::AddToIrtTransaction(JOBDATA *prpJob)
{
	bool blAdded = false;
	if(bmTransactionActivated && prpJob != NULL)
	{
		omIrtJobs.SetAt((void *) prpJob->Urno, prpJob);
		blAdded = true;
	}

	return blAdded;
}

bool CedaJobData::AddToUrtTransaction(JOBDATA *prpJob)
{
	bool blAdded = false;
	if(bmTransactionActivated && prpJob != NULL)
	{
		omUrtJobs.SetAt((void *) prpJob->Urno, prpJob);
		blAdded = true;
	}

	return blAdded;
}

bool CedaJobData::AddToDrtTransaction(long lpJobUrno)
{
	bool blAdded = false;
	if(bmTransactionActivated)
	{
		omDrtJobs.SetAt((void *) lpJobUrno, NULL);
		blAdded = true;
	}

	return blAdded;
}

bool CedaJobData::PoolJobHasFidJobs(long lpPoolJobUrno, bool bpIgnoreFinishedJobs /*=true*/)
{
	bool blPoolJobHasFidJobs = false;
	CCSPtrArray <JOBDATA> olJobs;
	GetJobsByPoolJob(olJobs, lpPoolJobUrno);
	for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
	{
		if(!strcmp(olJobs[ilJ].Jtco,JOBFID) &&
			(!bpIgnoreFinishedJobs || olJobs[ilJ].Stat[0] != 'F'))
		{
			blPoolJobHasFidJobs = true;
			break;
		}
	}

	return blPoolJobHasFidJobs;
}

bool CedaJobData::IsStandby(JOBDATA *prpPoolJob)
{
	if(prpPoolJob != NULL && !strcmp(prpPoolJob->Jtco,JOBPOOL) && !strcmp(prpPoolJob->Ignr,"1"))
		return true;
	else
		return false;
}

bool CedaJobData::IsAbsent(JOBDATA *prpPoolJob)
{
	if(prpPoolJob != NULL && !strcmp(prpPoolJob->Jtco,JOBPOOL) && prpPoolJob->Infm)
		return true;
	else
		return false;
}

bool CedaJobData::FlightHasJobs(long lpFlur)
{
	CMapPtrToPtr *polFlightJobMap;
	if(omFlightMap.Lookup((void *) lpFlur, (void *&) polFlightJobMap))
		return true;

	return false;
}

bool CedaJobData::DeleteDelegatedFlightJob(long lpUrno, BOOL bpSendDdx /* = FALSE */, bool bpUpdateDB /* = true */)
{
	// delete the job in display data
	JOBDATA *prlJob = GetJobByUrno(lpUrno);
	if (prlJob != NULL)
	{
		CCSPtrArray <JOBDATA> olJobs;
		CStringArray olUrnos;
		CString olUrnoList, olFormattedUrno;
		ogBasicData.ExtractItemList(prlJob->Text, &olUrnos,',');
		int ilNumUrnos = olUrnos.GetSize(), L, i;
		for(int x = 0; x < ilNumUrnos; x++)
		{
//			if(x > 0) olUrnoList += CString(",");
//			olFormattedUrno.Format("'%s'", olUrnos[x]);
//			olUrnoList += olFormattedUrno;

			JOBDATA *prlJ = GetJobByUrno(atol(olUrnos[x]));
			if(prlJ != NULL)
				olJobs.Add(prlJ);
		}

		bool blRc = false;
		L = olJobs.GetSize();
		for(i = 0; i < L; i++)
		{
			JOBDATA *prlJ = &olJobs[i];
		
			JOBDATA rlJob = *prlJ;
			DeleteJobInternal(rlJob.Urno);
			ogDataSet.DeleteDemandIfExpired(rlJob.Udem);
			ogConflicts.DeleteConflictForJob(rlJob.Urno);

			CCSPtrArray<JOBDATA> olPoolJobs;
			GetJobsByPoolJob(olPoolJobs,rlJob.Jour);
			if (olPoolJobs.GetSize() > 0)
			{
				ogConflicts.CheckConflicts(olPoolJobs[0],FALSE,TRUE);
			}
			
			ogCCSDdx.DataChanged((void *)this,JOB_DELETE,(void *)&rlJob);
			SendJodDdx(NULL, &rlJob);
// MCU 20070424 as a test

	DEMANDDATA *prlOldDemand = ogJodData.GetDemandForJob(prlJob);

	long llJobUrno = prlJob->Urno;

	CCSPtrArray<JODDATA> olJods;
	ogJodData.GetJodsByJob(olJods, llJobUrno);
	int ilNumJods = olJods.GetSize();
	if(ilNumJods > 0)
	{
		for (int ilLc = (ilNumJods-1); ilLc >= 0; ilLc--)
		{
			JODDATA *prlJod = &olJods[ilLc];

			// demands that have been retained after a rule change because
			// they have jobs assigned, are deleted when their jobs are deleted
			if(!ogDataSet.DeleteDemandIfExpired(prlJod->Udem))
			{
				// on undo, cannot recreate the demand deleted so don't ad the JOD
			//	popInfo->AddAnotherJod(prlJod, FALSE);
			}
			ogJodData.DeleteJod(prlJod->Urno);
		}
	}
	else if(prlOldDemand != NULL)
	{
		ogDataSet.DeleteDemandIfExpired(prlOldDemand->Urno);
	}


//			ogJodData.DeleteJodByJobu(rlJob.Urno);
// test end
			if(!strcmp(rlJob.Jtco,JOBPOOL))
			{
				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(rlJob.Shur);
				if(prlShift != NULL)
					ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
			}

			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(&rlJob);
			if (prlFlight != NULL)
			{
				ogConflicts.CheckConflicts(*prlFlight, FALSE, TRUE, TRUE, FALSE);
				//ogConflicts.CheckConflicts(*prlFlight);
				FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight->Urno);
				if(prlRotation != NULL)
				{
					ogConflicts.CheckConflicts(*prlRotation, FALSE, TRUE, TRUE, FALSE);
				}
				//ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
			}
			olPoolJobs.RemoveAll();
		}

		for(int u = 0; u < ilNumUrnos; u++)
		{
			if (bpSendDdx == FALSE && bpUpdateDB)
			{
				char pclSelection[124] = "";
				char pclData[2000] = "";
				char pclCmd[10] = "DRT";

				// first delete the jod
				ogJodData.DeleteJodByJobu(atol(olUrnos[u]));

				sprintf(pclSelection,"WHERE URNO = '%s'",olUrnos[u]);
				ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);

				if(!(blRc = CedaAction(pclCmd,pclSelection)))
				{
					ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,pclCmd,pcmFieldList,pcmTableName,pclSelection);
				}
			}
		}
	}

    return true;
}

bool CedaJobData::LoadJobsByUdem(CCSPtrArray <JOBDATA> &ropJobs, CString opUdemList)
{
	if(opUdemList.IsEmpty())
		return false;

	char pclCom[10] = "RT";
	sprintf(cgSelection, "WHERE UDEM IN (%s)", opUdemList);
	if (CedaAction2(pclCom, cgSelection) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaJobData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,cgSelection);
		return false;
	}
	int ilCount = 0;
	bool blRc = true;
	for(int ilLc = 0; blRc; ilLc++)
	{
		JOBDATA *prlJob = new JOBDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlJob,CString(pcmFieldList))) == RCSuccess)
		{
			ropJobs.Add(prlJob);
		}
	}

	return true;
}
