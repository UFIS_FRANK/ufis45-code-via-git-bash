#if !defined(AFX_LOADTIMESPANDLG_H__5E926A91_3E61_11D3_94EF_00001C018ACE__INCLUDED_)
#define AFX_LOADTIMESPANDLG_H__5E926A91_3E61_11D3_94EF_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LoadTimeSpanDlg.h : header file
//

#include <CCSPtrArray.h>
#include <CCSEdit.h>


/////////////////////////////////////////////////////////////////////////////
// CLoadTimeSpanDlg dialog

class CLoadTimeSpanDlg : public CDialog
{
// Construction
public:
	CLoadTimeSpanDlg(CWnd* pParent ,CStringArray &ropValues,bool bpAbsFlag,bool bpAllFieldsDisables);  

	CLoadTimeSpanDlg::~CLoadTimeSpanDlg();

// Dialog Data
	//{{AFX_DATA(CLoadTimeSpanDlg)
	enum { IDD = IDD_LOADTIMESPANDLG };
	CEdit	m_ToTime;
	CEdit	m_ToDate;
	CEdit	m_RelTo;
	CEdit	m_FromTime;
	CEdit	m_RelFrom;
	CEdit	m_FromDate;
	int		m_AbsTimeVal;
	CListBox	m_PossibleList;
	CListBox	m_SelectedList;
	//}}AFX_DATA

	CStringArray omValues;

	CTime omStartTime;
	CTime omEndTime;

	bool bmAllFieldsDisabled;

	CStringArray omPossibleItems;
	CStringArray omSelectedItems;
	CDWordArray  omTplUrnos;

	BOOL GetData();
	BOOL GetData(CStringArray &ropValues);
	void SetData();
	void SetData(CStringArray &ropValues);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoadTimeSpanDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLoadTimeSpanDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnAbstime();
	afx_msg void OnReltime();
	afx_msg void OnOutofmemorySlider2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRemove();
	afx_msg void OnAdd();
	afx_msg void OnDebugInfo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOADTIMESPANDLG_H__5E926A91_3E61_11D3_94EF_00001C018ACE__INCLUDED_)
