// PrePlanTeamPrintDlg.h : header file
//
#ifndef __PREPPRN_H__
#define __PREPPRN_H__

/////////////////////////////////////////////////////////////////////////////
// PrePlanTeamPrintDialog dialog

class PrePlanTeamPrintDialog : public CDialog
{
// Construction
public:
	PrePlanTeamPrintDialog(CWnd* pParent = NULL);   // standard constructor
	~PrePlanTeamPrintDialog();

public:
	CStringArray *m_TeamArray;
	CStringArray m_TeamSelect;

// Dialog Data
	//{{AFX_DATA(PrePlanTeamPrintDialog)
	enum { IDD = IDD_PREPLANPRINT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PrePlanTeamPrintDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PrePlanTeamPrintDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddteam();
	afx_msg void OnRemoveteam();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif //__PREPPRN_H__
