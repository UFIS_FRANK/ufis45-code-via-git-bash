// CedaSgrData.cpp - Class for Group Records (META)
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaSgrData.h>
#include <BasicData.h>

void ProcessSgrCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaSgrData::CedaSgrData()
{                  
    BEGIN_CEDARECINFO(SGRDATA, SgrDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Grpn,"GRPN")
		FIELD_CHAR_TRIM(Grds,"GRDS")
		FIELD_CHAR_TRIM(Tabn,"TABN")
		FIELD_LONG(Ugty,"UGTY")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SgrDataRecInfo)/sizeof(SgrDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SgrDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_SGR_INSERT, CString("SGRDATA"), CString("Sgr changed"),ProcessSgrCf);
	ogCCSDdx.Register((void *)this, BC_SGR_UPDATE, CString("SGRDATA"), CString("Sgr changed"),ProcessSgrCf);
	ogCCSDdx.Register((void *)this, BC_SGR_DELETE, CString("SGRDATA"), CString("Sgr deleted"),ProcessSgrCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"SGRTAB");
    pcmFieldList = "URNO,GRPN,GRDS,TABN,UGTY";
}


void ProcessSgrCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaSgrData *)popInstance)->ProcessSgrBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaSgrData::ProcessSgrBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSgrData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlSgrData);
	SGRDATA rlSgr, *prlSgr = NULL;
	long llUrno = GetUrnoFromSelection(prlSgrData->Selection);
	GetRecordFromItemList(&rlSgr,prlSgrData->Fields,prlSgrData->Data);
	if(llUrno == 0L) llUrno = rlSgr.Urno;

	switch(ipDDXType)
	{
		case BC_SGR_INSERT:
		{
			if((prlSgr = AddSgrInternal(rlSgr)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, SGR_INSERT, (void *)prlSgr);
			}
			break;
		}
		case BC_SGR_UPDATE:
		{
			if((prlSgr = GetSgrByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlSgr,prlSgrData->Fields,prlSgrData->Data);
				//PrepareDataAfterRead(prlSgr);
				ogCCSDdx.DataChanged((void *)this, SGR_UPDATE, (void *)prlSgr);
			}
			break;
		}
		case BC_SGR_DELETE:
		{
			if((prlSgr = GetSgrByUrno(llUrno)) != NULL)
			{
				DeleteSgrInternal(prlSgr->Urno);
				ogCCSDdx.DataChanged((void *)this, SGR_DELETE, (void *)prlSgr);
			}
			break;
		}
	}
}
 
CedaSgrData::~CedaSgrData()
{
	TRACE("CedaSgrData::~CedaSgrData called\n");
	ClearAll();
}

void CedaSgrData::ClearAll()
{
	omGroupNameMap.RemoveAll();
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaSgrData::ReadSgrData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    //sprintf(pclWhere,"WHERE APPL='%s'",pcgAppName);
	sprintf(pclWhere,"");
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaSgrData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		SGRDATA rlSgrData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlSgrData)) == RCSuccess)
			{
				AddSgrInternal(rlSgrData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaSgrData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SGRDATA), pclWhere);
    return ilRc;
}


SGRDATA *CedaSgrData::AddSgrInternal(SGRDATA &rrpSgr)
{
	SGRDATA *prlSgr = new SGRDATA;
	*prlSgr = rrpSgr;
	omData.Add(prlSgr);
	omUrnoMap.SetAt((void *)prlSgr->Urno,prlSgr);
	omGroupNameMap.SetAt(prlSgr->Grpn,prlSgr);

	return prlSgr;
}

void CedaSgrData::DeleteSgrInternal(long lpUrno)
{
	int ilNumSgrs = omData.GetSize();
	for(int ilSgr = (ilNumSgrs-1); ilSgr >= 0; ilSgr--)
	{
		if(omData[ilSgr].Urno == lpUrno)
		{
			omUrnoMap.RemoveKey((void *)omData[ilSgr].Grpn);
			omData.DeleteAt(ilSgr);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

SGRDATA* CedaSgrData::GetSgrByUrno(long lpUrno)
{
	SGRDATA *prlSgr = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlSgr);

	return prlSgr;
}

// get the groups defined by type
void CedaSgrData::GetSgrByUgty(long lpUgty, CCSPtrArray <SGRDATA> &ropSgrList, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropSgrList.RemoveAll();
	}

	int ilNumGroups = omData.GetSize();
	for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
	{
		SGRDATA *prlSgr = &omData[ilGroup];
		if(prlSgr->Ugty == lpUgty)
		{
			ropSgrList.Add(prlSgr);
		}
	}
}


long CedaSgrData::GetSgrUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	int ilNumSgrs = omData.GetSize();
	for(int ilSgr = 0; llUrno == 0L && ilSgr < ilNumSgrs; ilSgr++)
	{
		if(!strcmp(omData[ilSgr].Grpn,pcpName))
		{
			llUrno = omData[ilSgr].Urno;
		}
	}

	return llUrno;
}


CString CedaSgrData::GetTableName(void)
{
	return CString(pcmTableName);
}

//--ReadSpecialData-------------------------------------------------------------------------------------

bool CedaSgrData::ReadSpecialData(CCSPtrArray<SGRDATA> *popSgr,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSgr != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SGRDATA *prpSgr = new SGRDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSgr,CString(pclFieldList))) == true)
			{
				popSgr->Add(prpSgr);
			}
			else
			{
				delete prpSgr;
			}
		}
		if(popSgr->GetSize() == 0) return false;
	}
    return true;
}

//-------------------------------------------------------------------------------------------------
