///////////////////////////////////////////////////
//

#ifndef _AUTOASSIGNAZ_VIEWER_
#define _AUTOASSIGNAZ_VIEWER_

#include <CCSPtrArray.h>
#include <cviewer.h>


class AutoAssignAzViewer : public CViewer
{
public:
	AutoAssignAzViewer();
	~AutoAssignAzViewer();

	CString omStaffViewName;
	CMapStringToPtr omPoolMap;
	bool bmUseAllPools;

	CString omRegnAreaViewName;
	CMapStringToPtr omRegnAreaMap;
	bool bmUseAllRegnAreas;
};
#endif 