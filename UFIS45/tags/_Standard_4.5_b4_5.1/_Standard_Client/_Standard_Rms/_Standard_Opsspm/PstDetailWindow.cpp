// PstDetailWindow.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <table.h>
#include <PstDetailWindow.h>

#include <CCSPtrArray.h>

#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>

#include <cviewer.h>
#include <PstDetailViewer.h>
#include <CedaPstData.h>
#include <Dataset.h>
#include <FlightDetailWindow.h>

#include <ccsddx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

PstDetailWindow* PstDetailWindow::omCurrent = NULL;

/////////////////////////////////////////////////////////////////////////////
// PstDetailWindow dialog

PstDetailWindow::PstDetailWindow(CWnd* pParent,const char *pcpAlid,
	BOOL bpArrival, BOOL bpDeparture, CTime opStartTime, CTime opEndTime)
	: CDialog(PstDetailWindow::IDD, pParent)
{
	if (omCurrent)
	{
		if (omCurrent->GetSafeHwnd())
			omCurrent->DestroyWindow();
		else
			delete omCurrent;
	}

	omCurrent = this;

	pcmAlid = &cmAlid[0];
	strcpy(pcmAlid, pcpAlid);
	bmArrival = bpArrival;
	bmDeparture = bmDeparture;
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	Create(IDD, pParent);
	CenterWindow(pParent);

	CString olCaptionText;
	//olCaptionText.Format("Pst Info %s von: %s bis: %s",pcpAlid,omStartTime.Format("%H%M"),omEndTime.Format("%H%M"));
	olCaptionText.Format(GetString(IDS_STRING61908),pcpAlid,omStartTime.Format("%H%M"),omEndTime.Format("%H%M"));
	SetWindowText(olCaptionText);
	//
	
	CString olText;
	olText = GetString(IDS_STRING33134);
	olText += pcmAlid;
	olText += " :";
	SetDlgItemText(IDC_ALID1, olText);

	olText  = GetString(IDS_STRING61910);
	olText += pcmAlid;
	SetDlgItemText(IDC_ALID2, olText);

	PSTDATA *prlPst = ogPstData.GetPstByName(pcmAlid);

//	SetDlgItemText(IDC_TELN, clTeln); 

	m_nDialogBarHeight = 60;

	omTable.tempFlag = 2;

    CRect rect;
	GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	
    omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

    omViewer.Attach(&omTable,omStartTime,omEndTime,pcmAlid);

	omViewer.ChangeView();
	//

	//{{AFX_DATA_INIT(PstDetailWindow)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void PstDetailWindow::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PstDetailWindow)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PstDetailWindow, CDialog)
	//{{AFX_MSG_MAP(PstDetailWindow)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PstDetailWindow message handlers

int PstDetailWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// Register DDX call back function
	TRACE("PstDetailWindow: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("PSTDW"),CString("Redisplay all from What-If"), PstDetailWindowCf);	// for what-if changes
	
	return 0;
}

void PstDetailWindow::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("PstDetailWindow: DDX Unregistration %s\n",::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
	omCurrent = NULL;
	delete this;
}

void PstDetailWindow::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
}

void PstDetailWindow::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
    if (nType != SIZE_MINIMIZED)
		omTable.SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

////////////////////////////////////////////////////////////////////////
// PstDetailWindow -- implementation of DDX call back function

void PstDetailWindow::PstDetailWindowCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	PstDetailWindow *polTable = (PstDetailWindow *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
		polTable->DestroyWindow();
}

/////////////////////////////////////////////////////////////////////////////
// PstDetailWindow -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a JobFlightManager (JTCO = "FMJ").
//		(It's DIT_FLIGHTBAR when the user drag from this detail window).
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create JobFlightManagers for employees who are managers.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create JobFlightManagers for employees who are managers.
//

BOOL PstDetailWindow::OnInitDialog() 
{
	CDialog::OnInitDialog();
	

	SetWindowText(GetString(IDS_STRING61909)); 

	CWnd *polWnd = GetDlgItem(IDC_ALID1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32984));
	}
	polWnd = GetDlgItem(IDC_ALID2); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61910));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61322));
	}

	m_DragDropTarget.RegisterTarget(this, this);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/////////////////////////////////////////////////////////////////////////////
// PstDetailWindow -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a JobFlight.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create normal flight jobs for those staffs.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create a normal flight job for that staff.
//
LONG PstDetailWindow::OnDragOver(UINT wParam, LONG lParam)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_DUTYBAR)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	int ilDropLineno = omTable.GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= omViewer.Lines() - 1))
		return -1L;	// cannot drop on a blank line

	PSTDETAIL_LINEDATA *prlLine = omViewer.GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropFlightUrno = prlLine->FlightUrno;
	int ilColumn = omTable.GetLinenoFromPoint(olDropPosition);
	imDropDemandUrno = ilColumn;
	return 0L;
}

LONG PstDetailWindow::OnDrop(UINT wParam, LONG lParam)
{ 
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	int ilDropLineno = omTable.GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= omViewer.Lines() - 1))
		return -1L;	// cannot drop on a blank line

	PSTDETAIL_LINEDATA *prlLine = omViewer.GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropFlightUrno = prlLine->FlightUrno;
	int ilColumn	 = omTable.GetColumnnoFromPoint(olDropPosition);
	int ilDemandNo	 = ilColumn-4;
	bool blTeamAlloc = false; // assigning a whole team ?

	
	CDWordArray olPoolJobUrnos;
	int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
	if(ilDataCount > 0)
	{
		for(int ilPoolJob = 0; ilPoolJob < ilDataCount; ilPoolJob++)
		{
			long llPoolJobUrno = m_DragDropTarget.GetDataDWord(ilPoolJob);
			olPoolJobUrnos.Add(llPoolJobUrno);
		}
		blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false; // assigning a whole team ?
	}

	CString olPosition = ogFlightData.GetPosition(imDropFlightUrno);
/*
	if ((ilDemandNo >= 0) && (ilDemandNo < prlLine->Indicators.GetSize()))
	{
		long llDemandUrno = prlLine->Indicators[ilDemandNo].DemandUrno;
		if (ogJodData.GetFirstJobUrnoByDemand(llDemandUrno) == 0L)
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(llDemandUrno);
			if(prlDemand != NULL)
			{
				ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, imDropFlightUrno, ALLOCUNITTYPE_PST, ogFlightData.GetPosition(imDropFlightUrno), blTeamAlloc, prlDemand);
				return 0L;
			}
		}
	}
*/
	// create job flight from duty with (default) demand
	if(ilDataCount > 0)
	{
		ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, imDropFlightUrno, ALLOCUNITTYPE_PST, ogFlightData.GetPosition(imDropFlightUrno), blTeamAlloc);
	}


	return 0L;
}

LONG PstDetailWindow::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = omViewer.Lines();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		PSTDETAIL_LINEDATA *prlLine = omViewer.GetLine(ipItem);
		if (prlLine != NULL)
		{
			new FlightDetailWindow(this,prlLine->FlightUrno,-1,FALSE,ALLOCUNITTYPE_PST);
		}
	}

	return 0L;
}
