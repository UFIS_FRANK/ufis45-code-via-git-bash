// AbsenceDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <AbsenceDlg.h>
#include <CedaOdaData.h>
#include <CCSGLOBL.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAbsenceDlg dialog


CAbsenceDlg::CAbsenceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAbsenceDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAbsenceDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAbsenceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAbsenceDlg)
	DDX_Control(pDX, IDC_LIST1, m_AbsenceList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAbsenceDlg, CDialog)
	//{{AFX_MSG_MAP(CAbsenceDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAbsenceDlg message handlers
BOOL CAbsenceDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	UpdateData(FALSE);
	int ilIdx;
	int ilCount = ogOdaData.omData.GetSize();
	for(int ilA = 0; ilA < ilCount; ilA++)
	{
		ODADATA *prlOda = &ogOdaData.omData[ilA];
		ilIdx = m_AbsenceList.AddString(prlOda->Sdan);
		if(ilIdx != LB_ERR)
		{
			m_AbsenceList.SetItemData(ilIdx,(unsigned long)prlOda);
		}
	}
	if(ilCount > 0)
	{
		m_AbsenceList.SetCurSel(0);
	}
	SetWindowText(GetString(IDS_STRING32821)); 

	CWnd *polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAbsenceDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		int ilSel = m_AbsenceList.GetCurSel();
		if(ilSel != LB_ERR)
		{
			ODADATA *prlOda = (ODADATA *) m_AbsenceList.GetItemData(ilSel);
			if((DWORD) prlOda != LB_ERR)
			{
				omAbsenceCode = prlOda->Sdac;
			}
		}
		CDialog::OnOK();
	}
}