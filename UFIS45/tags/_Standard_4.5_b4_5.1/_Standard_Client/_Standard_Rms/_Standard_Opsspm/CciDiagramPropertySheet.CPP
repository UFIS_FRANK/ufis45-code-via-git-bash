// ccidiaps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaCicData.h>
#include <CedaAltData.h>
#include <CedaRnkData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <CciDiagramFilterPage.h>
#include <CciDiagramGroupPage.h>
#include <BasePropertySheet.h>
#include <CciDiagramPropertySheet.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIDiagramPropertySheet
//

//CCIDiagramPropertySheet::CCIDiagramPropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_CCI_DIAGRAM, pParentWnd, popViewer, iSelectPage)
CCIDiagramPropertySheet::CCIDiagramPropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61587), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageGroup);
	AddPage(&m_pageHall);
	AddPage(&m_pageRegion);
	AddPage(&m_pageLine);
	AddPage(&m_pageTerminal);
	AddPage(&m_pageAirline);
	AddPage(&m_pageFunctions);

	m_pageHall.SetSorted(false);
	m_pageHall.SetCaption(GetString(IDS_STRING61965));

	m_pageRegion.SetSorted(false);
	m_pageRegion.SetCaption(GetString(IDS_STRING61966));

	m_pageLine.SetSorted(false);
	m_pageLine.SetCaption(GetString(IDS_STRING61956));

	m_pageTerminal.SetSorted(false);
	m_pageTerminal.SetCaption(GetString(IDS_STRING61607));

	m_pageAirline.SetCaption(GetString(IDS_STRING61597));
	m_pageAirline.m_bEnabled = true;

	m_pageFunctions.SetSorted(false);
	m_pageFunctions.SetCaption(GetString(IDS_CICGANTT_FUNCTIONS));
	m_pageFunctions.m_bEnabled = true;

	// Prepare possible values for each PropertyPage
	ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_CICGROUP,m_pageTerminal.omPossibleItems);
	m_pageTerminal.omPossibleItems.Add(GetString(IDS_STRING62514)); // "Without Counter"

	ogCicData.GetHalls(m_pageHall.omPossibleItems);
	m_pageHall.omPossibleItems.Add(GetString(IDS_STRING62514)); // "Without Counter"

	ogCicData.GetRegions(m_pageRegion.omPossibleItems);
	m_pageRegion.omPossibleItems.Add(GetString(IDS_STRING62514)); // "Without Counter"

	ogCicData.GetLines(m_pageLine.omPossibleItems);
	m_pageLine.omPossibleItems.Add(GetString(IDS_STRING62514)); // "Without Counter"

	ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
	m_pageAirline.bmSelectAllEnabled = true;

	ogRnkData.GetAllRanks(m_pageFunctions.omPossibleItems);
	m_pageFunctions.bmSelectAllEnabled = true;
}

void CCIDiagramPropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("Halle",	m_pageTerminal.omSelectedItems);
	pomViewer->GetFilter("Hall",	m_pageHall.omSelectedItems);
	pomViewer->GetFilter("Region",	m_pageRegion.omSelectedItems);
	pomViewer->GetFilter("Line",	m_pageLine.omSelectedItems);
	pomViewer->GetFilter("Airline",	m_pageAirline.omSelectedItems);
	pomViewer->GetFilter("Functions",	m_pageFunctions.omSelectedItems);

	m_pageGroup.omGroupBy = pomViewer->GetGroup();
	UpdateEnableFlagInFilterPages();
}

void CCIDiagramPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Halle",	m_pageTerminal.omSelectedItems);
	pomViewer->SetFilter("Hall",	m_pageHall.omSelectedItems);
	pomViewer->SetFilter("Region",	m_pageRegion.omSelectedItems);
	pomViewer->SetFilter("Line",	m_pageLine.omSelectedItems);
	pomViewer->SetFilter("Airline",	m_pageAirline.omSelectedItems);
	pomViewer->SetFilter("Functions",	m_pageFunctions.omSelectedItems);

	pomViewer->SetGroup(m_pageGroup.omGroupBy);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int CCIDiagramPropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedDeskTypes;
	CStringArray olSelectedTerminals;
	CStringArray olSelectedHalls;
	CStringArray olSelectedRegions;
	CStringArray olSelectedLines;
	CStringArray olSelectedAirlines;
	CStringArray olSelectedFunctions;
	CString olGroupBy;

	pomViewer->GetFilter("Halle", olSelectedTerminals);
	pomViewer->GetFilter("Hall",  olSelectedHalls);
	pomViewer->GetFilter("Region",olSelectedRegions);
	pomViewer->GetFilter("Line",  olSelectedLines);
	pomViewer->GetFilter("Airline",  olSelectedAirlines);
	pomViewer->GetFilter("Functions",  olSelectedFunctions);
	olGroupBy = pomViewer->GetGroup();

	if (!IsIdentical(olSelectedTerminals, m_pageTerminal.omSelectedItems) ||
		!IsIdentical(olSelectedHalls,	  m_pageHall.omSelectedItems) ||
		!IsIdentical(olSelectedRegions,	  m_pageRegion.omSelectedItems) ||
		!IsIdentical(olSelectedLines,	  m_pageLine.omSelectedItems) ||
		!IsIdentical(olSelectedAirlines,  m_pageAirline.omSelectedItems) ||
		!IsIdentical(olSelectedFunctions,  m_pageFunctions.omSelectedItems) ||
		olGroupBy != CString(m_pageGroup.omGroupBy))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}


BEGIN_MESSAGE_MAP(CCIDiagramPropertySheet, BasePropertySheet)
    //{{AFX_MSG_MAP(CCIDiagramPropertySheet)
	ON_MESSAGE(WM_CCIDIAGRAM_GROUPPAGE_CHANGED, OnCciDiagramGroupPageChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIDiagramPropertySheet message handlers

LONG CCIDiagramPropertySheet::OnCciDiagramGroupPageChanged(UINT wParam, LONG lParam)
{
	UpdateEnableFlagInFilterPages();
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// CCIDiagramPropertySheet -- help routines

void CCIDiagramPropertySheet::UpdateEnableFlagInFilterPages()
{
	m_pageTerminal.m_bEnabled = (m_pageGroup.omGroupBy == "Halle");
	m_pageHall.m_bEnabled	  = (m_pageGroup.omGroupBy == "Hall");
	m_pageRegion.m_bEnabled	  = (m_pageGroup.omGroupBy == "Region");
	m_pageLine.m_bEnabled	  = (m_pageGroup.omGroupBy == "Line");
}
