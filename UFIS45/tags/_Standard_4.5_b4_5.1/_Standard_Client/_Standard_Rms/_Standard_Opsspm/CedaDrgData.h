// Class for DRGTAB - Staff Working Group Deviations
// group deviation - the emp is given a new function temporarily so that they can belong to a group

#ifndef _CEDADRGDATA_H_
#define _CEDADRGDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaShiftData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

// Name         Null?    Type
// ------------ -------- ----
// DRRN                  CHAR(1)
// FCTC                  CHAR(5)
// HOPO                  CHAR(3)
// SDAY                  CHAR(8)
// STFU                  CHAR(10)
// URNO         NOT NULL CHAR(10)
// WGPC                  CHAR(5)

 struct DrgDataStruct
{
	long Urno;
	char Wgpc[6];	// Group Code in WGPTAB
	long Stfu;		// URNO of employee in STFTAB
	char Sday[9];	// Date of deviation yyymmdd
	char Fctc[6];	// Function Code
	char Drrn[2];	// 

	DrgDataStruct(void)
	{
		Urno = 0L;
		strcpy(Wgpc,"");
		Stfu = 0L;
		strcpy(Sday,"");
		strcpy(Fctc,"");
		strcpy(Drrn,"");
	}
};

typedef struct DrgDataStruct DRGDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDrgData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DRGDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omStfuMap;
	CString GetTableName(void);

// Operations
public:
	CedaDrgData();
	~CedaDrgData();

	void ProcessDrgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadDrgData();
	DRGDATA* GetDrgByUrno(long lpUrno);
	void PrepareData(DRGDATA *prpDrg);
	DRGDATA *GetDrgByShift(SHIFTDATA *prpShift);
	bool UpdateDrgCodeForShift(SHIFTDATA *prpShift);
	CCSReturnCode UpdateDrg(long lpDrgUrno, const char *pcpWgpc, const char *pcpFctc);
	DRGDATA *InsertDrg(long lpStfu, const char *pcpSday, const char *pcpWgpc, const char *pcpFctc, const char *pcpDrrn);
	CString Dump(long lpUrno);

private:
	DRGDATA *AddDrgInternal(DRGDATA &rrpDrg);
	void DeleteDrgInternal(long lpUrno);
	void ClearAll();
};

extern CedaDrgData ogDrgData;

#endif _CEDADRGDATA_H_
