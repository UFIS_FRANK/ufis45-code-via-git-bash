// FunctionColoursViewer.cpp -- the viewer for Demand Table
//
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaPfcData.h>
#include <OpssPm.h>
#include <ccstable.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <FunctionColoursViewer.h>
#include <DlgSettings.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

static struct FUNCTIONCOLOURS_FIELD osfields[]	= 
{
	{	FUNCTIONCOLOURS_FIELD("FCTC",	"",		50,		50)	},
	{	FUNCTIONCOLOURS_FIELD("COLOUR",	"",		30,		30)	},
	{	FUNCTIONCOLOURS_FIELD("FCTN",	"",		200,	200)},
};

/////////////////////////////////////////////////////////////////////////////
// FunctionColoursViewer

FunctionColoursViewer::FunctionColoursViewer()
{
    pomTable = NULL;

	// must initialize names here because resource handle is not available during static initialization
	if (!osfields[0].Name.GetLength())
	{
		osfields[0].Name = GetString(IDS_FUNCCOLOURS_CODE);
		osfields[1].Name = GetString(IDS_FUNCCOLOURS_COLOUR);
		osfields[2].Name = GetString(IDS_FUNCCOLOURS_NAME);
	}

	bmDisplayDefinedOnly = false;
}

FunctionColoursViewer::~FunctionColoursViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
	omTableFields.DeleteAll();
	DeleteAll();
}

void FunctionColoursViewer::Attach(CCSTable *popTable)
{
	pomTable = popTable;
}

void FunctionColoursViewer::ChangeViewTo(void)
{
    DeleteAll();    
	EvaluateTableFields();
    MakeLines();
	UpdateDisplay();
}

void FunctionColoursViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int FunctionColoursViewer::CompareFunctionCode(FUNCTIONCOLOURS_LINE *prpLine1, FUNCTIONCOLOURS_LINE *prpLine2)
{
	return strcmp(prpLine1->Fctc, prpLine2->Fctc);
}

BOOL FunctionColoursViewer::IsPassFilter(PFCDATA *prpPfc)
{
	BOOL blRc = TRUE;
	return blRc;
}

void FunctionColoursViewer::MakeLines()
{
	int ilNumPfcs = ogPfcData.omData.GetSize();
	for(int ilPfc = 0; ilPfc < ilNumPfcs; ilPfc++)
	{
		PFCDATA *prlPfc = &ogPfcData.omData[ilPfc];
		if(IsPassFilter(prlPfc))
		{
			MakeLine(prlPfc);
		}
	}

	if(ogBasicData.bmDisplayTeams)
	{
		MakeLineForTeamLeader();
	}
}

void FunctionColoursViewer::MakeLine(PFCDATA *prpPfc)
{
	FUNCTIONCOLOURS_LINE olLine;
	MakeLineData(prpPfc, olLine);
	CreateLine(&olLine);
}

void FunctionColoursViewer::MakeLineForTeamLeader(void)
{
	FUNCTIONCOLOURS_LINE olLine;
	olLine.Fctc = GetString(IDS_FUNCCOLOURS_TEAMLEADER1);
	olLine.Fctn = GetString(IDS_FUNCCOLOURS_TEAMLEADER2);
	CString olColour;
	if(ogDlgSettings.GetValue("FUNCTIONCOLOURS", olLine.Fctc, olColour))
	{
		olLine.Colour = (COLORREF) atol(olColour);
		olLine.Enabled = true;
	}
	else
	{
		olLine.Colour = GRAY;
		olLine.Enabled = false;
	}

	if(!bmDisplayDefinedOnly || olLine.Enabled)
	{
		CreateLine(&olLine);
	}
}

void FunctionColoursViewer::MakeLineData(PFCDATA *prpPfc, FUNCTIONCOLOURS_LINE &ropLine)
{
	ropLine.Fctc = prpPfc->Fctc;
	ropLine.Fctn = prpPfc->Fctn;

	CString olColour;
	if(ogDlgSettings.GetValue("FUNCTIONCOLOURS", prpPfc->Fctc, olColour))
	{
		ropLine.Colour = (COLORREF) atol(olColour);
		ropLine.Enabled = true;
	}
	else
	{
		ropLine.Colour = GRAY;
		ropLine.Enabled = false;
	}
}

void FunctionColoursViewer::SaveLineData(void)
{
	ogDlgSettings.EnableSaveToDb("FUNCTIONCOLOURS");
	ogDlgSettings.DeleteSettingsByDialog("FUNCTIONCOLOURS");

	CString olColour;
    int ilLineCount = omLines.GetSize();
    for(int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		FUNCTIONCOLOURS_LINE *prlLine = &omLines[ilLineno];
		if(prlLine->Enabled)
		{
			olColour.Format("%ld", (long) prlLine->Colour);
			ogDlgSettings.AddValue("FUNCTIONCOLOURS", prlLine->Fctc, olColour);
		}
	}
}

int FunctionColoursViewer::CreateLine(FUNCTIONCOLOURS_LINE *prpLine)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareFunctionCode(prpLine, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFunctionCode(prpLine, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

    omLines.NewAt(ilLineno, *prpLine);
    return ilLineno;
}

void FunctionColoursViewer::DeleteLine(int ipLineno)
{
    omLines.DeleteAt(ipLineno);
}

void FunctionColoursViewer::UpdateDisplay()
{
	if(pomTable != NULL)
	{
		pomTable->ResetContent();

		CreateTableColumnsAndHeader();
		pomTable->SetHeaderFields(omHeader);

		pomTable->DisplayTable();

		int ilNumLines = omLines.GetSize(), ilLinesAdded = 0;
		for (int i = 0; i < ilNumLines; i++) 
		{
			FUNCTIONCOLOURS_LINE *prlLine = &omLines[i];
			if(!bmDisplayDefinedOnly || prlLine->Enabled)
			{
				Format(prlLine, omColumns);
				pomTable->AddTextLine(omColumns, prlLine);
				pomTable->SetTextLineColor(i, BLACK, prlLine->Enabled ? WHITE : SILVER);
				if(prlLine->Enabled)
				{
					pomTable->SetTextColumnColor(ilLinesAdded, 1, prlLine->Colour, prlLine->Colour);
				}
				ilLinesAdded++;
			}
		}

		pomTable->DisplayTable();
	}
}

void FunctionColoursViewer::CreateTableColumnsAndHeader()
{
	omHeader.DeleteAll();
	omColumns.DeleteAll();
	int ilNumFields = omTableFields.GetSize();
	for (int ilC = 0; ilC < ilNumFields; ilC++)
	{
		omHeader.NewAt(ilC,TABLE_HEADER_COLUMN());
		omHeader[ilC].Text   = omTableFields[ilC].Name;
		omHeader[ilC].Length = omTableFields[ilC].Length;
		omColumns.NewAt(ilC,TABLE_COLUMN());
	}
}

void FunctionColoursViewer::UpdateTableLine(int ipLine)
{
	if(pomTable && ipLine >= 0 && ipLine < omLines.GetSize())
	{
		FUNCTIONCOLOURS_LINE *prlLine = &omLines[ipLine];
		Format(&omLines[ipLine], omColumns);
		pomTable->SetTextLineColor(ipLine, BLACK, prlLine->Enabled ? WHITE : SILVER);
		if(prlLine->Enabled)
			pomTable->SetTextColumnColor(ipLine, 1, prlLine->Colour, prlLine->Colour);
		pomTable->DisplayTable();
	}
}

void FunctionColoursViewer::Format(FUNCTIONCOLOURS_LINE *prpLine, CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	if (rrpColumns.GetSize() != omTableFields.GetSize())
		return;

	SetColumn("FCTC", prpLine->Fctc, rrpColumns);
	SetColumn("COLOUR", "", rrpColumns);
	SetColumn("FCTN", prpLine->Fctn, rrpColumns);
}

bool FunctionColoursViewer::SetColumn(const char *pcpFieldName, const char *pcpFieldValue, CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	int ilIndex = TableFieldIndex(pcpFieldName);
	if(ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = pcpFieldValue;
		return true;
	}

	return false;
}

int FunctionColoursViewer::LineCount() const
{
	return omLines.GetSize();
}

FUNCTIONCOLOURS_LINE *FunctionColoursViewer::GetLine(int ipLineNo)
{
	FUNCTIONCOLOURS_LINE *prlLine = NULL;
	if(bmDisplayDefinedOnly)
	{
		int ilNumLines = omLines.GetSize();
		int ilLineCount = -1;
		for(int ilL = 0; ilL < ilNumLines; ilL++)
		{
			if(omLines[ilL].Enabled)
				ilLineCount++;

			if(ilLineCount == ipLineNo)
			{
				prlLine = &omLines[ilL];
				break;
			}
		}
	}
	else
	{
		if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
			prlLine = &omLines[ipLineNo];
	}

	return prlLine;
}

bool FunctionColoursViewer::EvaluateTableFields()
{
	omTableFields.DeleteAll();
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		omTableFields.New(osfields[i]);
	}

	return true;
}

int FunctionColoursViewer::TableFieldIndex(const CString& ropField)
{
	for (int i = 0; i < omTableFields.GetSize(); i++)
	{
		if (omTableFields[i].Field == ropField)
			return i;
	}

	return -1;
}

