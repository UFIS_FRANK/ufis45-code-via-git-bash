// StaffViewer.h : header file
//

#ifndef _STBKVIEWER_H_
#define _STBKVIEWER_H_

#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <StaffViewer.h>

/////////////////////////////////////////////////////////////////////////////
// StaffBreakDiagramViewer window

class StaffBreakDiagramViewer: public StaffDiagramViewer
{

// Constructions
public:
    StaffBreakDiagramViewer(const CTimeSpan& ropBreakPrintOffset);
    ~StaffBreakDiagramViewer();

//	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);

// Internal data processing routines
private:

// Operations
public:
	BOOL GetBreakJobsNotPrinted(CCSPtrArray<JOBDATA>& ropBreakJobs);

// Private helper routines
private:

// Attributes used for filtering condition
private:

// Attributes
private:
	CTimeSpan omBreakPrintOffset;

// Methods which handle changes (from Data Distributor)
private:

public:

///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:

public:
};

/////////////////////////////////////////////////////////////////////////////

#endif
