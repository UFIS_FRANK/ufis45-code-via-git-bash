//
// CedaSwgData.cpp - links emps (STFTAB) to their groups (only one of which is currently valid)
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaSwgData.h>
#include <BasicData.h>

static int CompareVpfr(const SWGDATA **pppSwg1, const SWGDATA **pppSwg2);
static int CompareVpfr(const SWGDATA **pppSwg1, const SWGDATA **pppSwg2)
{
	int ilCompareResult = 0;
	if((**pppSwg1).Vpfr>(**pppSwg2).Vpfr)
	{
		ilCompareResult = 1;
	}
	else if((**pppSwg1).Vpfr<(**pppSwg2).Vpfr)
	{
		ilCompareResult = -1;
	}
	return ilCompareResult;
}

void ProcessSwgCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaSwgData::CedaSwgData()
{                  
    BEGIN_CEDARECINFO(SWGDATA, SwgDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Surn,"SURN")
		FIELD_CHAR_TRIM(Code,"CODE")
		FIELD_DATE(Vpfr,"VPFR")
		FIELD_DATE(Vpto,"VPTO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SwgDataRecInfo)/sizeof(SwgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SwgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"SWGTAB");
    pcmFieldList = "URNO,SURN,CODE,VPFR,VPTO";

	ogCCSDdx.Register((void *)this, BC_SWG_INSERT, CString("SWGDATA"), CString("WorkGroup changed"),ProcessSwgCf);
	ogCCSDdx.Register((void *)this, BC_SWG_UPDATE, CString("SWGDATA"), CString("WorkGroup changed"),ProcessSwgCf);
	ogCCSDdx.Register((void *)this, BC_SWG_DELETE, CString("SWGDATA"), CString("WorkGroup deleted"),ProcessSwgCf);
}
 
CedaSwgData::~CedaSwgData()
{
	TRACE("CedaSwgData::~CedaSwgData called\n");
	ClearAll();
}


void ProcessSwgCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaSwgData *)popInstance)->ProcessSwgBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaSwgData::ProcessSwgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSwgData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlSwgData);
	SWGDATA rlSwg, *prlSwg = NULL;
	long llUrno = GetUrnoFromSelection(prlSwgData->Selection);
	GetRecordFromItemList(&rlSwg,prlSwgData->Fields,prlSwgData->Data);
	if(llUrno == 0L) llUrno = rlSwg.Urno;

	switch(ipDDXType)
	{
		case BC_SWG_INSERT:
		{
			if((prlSwg = AddSwgInternal(rlSwg)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, SWG_INSERT, (void *)prlSwg);
			}
			break;
		}
		case BC_SWG_UPDATE:
		{
			if((prlSwg = GetSwgByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlSwg,prlSwgData->Fields,prlSwgData->Data);
				//PrepareDataAfterRead(prlSwg);
				ogCCSDdx.DataChanged((void *)this, SWG_UPDATE, (void *)prlSwg);
			}
			break;
		}
		case BC_SWG_DELETE:
		{
			if((prlSwg = GetSwgByUrno(llUrno)) != NULL)
			{
				SWGDATA rolSwg = *prlSwg;
				DeleteSwgInternal(llUrno);
				ogCCSDdx.DataChanged((void *)this, SWG_DELETE, (void *)&rolSwg);
			}
			break;
		}
	}
}
 
void CedaSwgData::ClearAll()
{
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omSurnMap.GetStartPosition(); rlPos != NULL; )
	{
		omSurnMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUrnoMap.RemoveAll();
	omSurnMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaSwgData::ReadSwgData(void)
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
	CTime olDay = ogBasicData.GetTimeframeStart();

	sprintf ( pclWhere, "WHERE VPTO=' ' OR VPTO>'%s'", olDay.Format("%Y%m%d%H%M%S") );

	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaSwgData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		SWGDATA rlSwgData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlSwgData)) == RCSuccess)
			{
				AddSwgInternal(rlSwgData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaSwgData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SWGDATA), pclWhere);
    return ilRc;
}


SWGDATA *CedaSwgData::AddSwgInternal(SWGDATA &rrpSwg)
{
	SWGDATA *prlSwg = new SWGDATA;
	*prlSwg = rrpSwg;
	omData.Add(prlSwg);
	omUrnoMap.SetAt((void *)prlSwg->Urno,prlSwg);
	CMapPtrToPtr *polSingleMap;
	if(omSurnMap.Lookup((void *) prlSwg->Surn, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlSwg->Urno,prlSwg);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlSwg->Urno,prlSwg);
		omSurnMap.SetAt((void *)prlSwg->Surn,polSingleMap);
	}

	return prlSwg;
}

void CedaSwgData::DeleteSwgInternal(long lpUrno)
{
	SWGDATA *prlSwg = NULL;
	int ilNumSwgs = omData.GetSize();
	for(int ilSwg = (ilNumSwgs-1); ilSwg >= 0; ilSwg--)
	{
		prlSwg = &omData[ilSwg];
		if(prlSwg->Urno == lpUrno)
		{
			omData.DeleteAt(ilSwg);

			CMapPtrToPtr *polSingleMap;
			if(omSurnMap.Lookup((void *) prlSwg->Surn, (void *&) polSingleMap))
			{
				polSingleMap->RemoveKey((void *) prlSwg->Urno);
			}
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

SWGDATA* CedaSwgData::GetSwgByUrno(long lpUrno)
{
	SWGDATA *prlSwg = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlSwg);
	return prlSwg;
}

CString CedaSwgData::GetGroupBySurn(long lpSurn, CTime opDate /* = TIMENULL */)
{
	CString olGroup = "";
	CTime olDate = (opDate != TIMENULL) ? opDate : ogBasicData.GetTime();
	

	CCSPtrArray <SWGDATA> olSwgs;
	GetSwgsBySurn(lpSurn, olSwgs, opDate);
	int ilNumSwgs = olSwgs.GetSize();
	for(int ilSwg = 0; ilSwg < ilNumSwgs; ilSwg++)
	{
		olGroup = olSwgs[ilSwg].Code;
	}
	olSwgs.RemoveAll();
	return olGroup;
}

void CedaSwgData::GetSwgsBySurn(long lpSurn, CCSPtrArray <SWGDATA> &ropSwgs, CTime opDate /*= TIMENULL*/, bool bpReset/*=true*/)
{
	if(bpReset)
	{
	    ropSwgs.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	SWGDATA *prlSwg;

	if(omSurnMap.Lookup((void *)lpSurn,(void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlSwg);
			if(opDate == NULL || (prlSwg->Vpfr != TIMENULL && prlSwg->Vpfr <= opDate &&
				(prlSwg->Vpto == TIMENULL || prlSwg->Vpto > opDate)))
			{
				ropSwgs.Add(prlSwg);
			}
		}
	}

	ropSwgs.Sort(CompareVpfr);
}

CCSReturnCode CedaSwgData::UpdateSwgCodeForSurn(long lpSurn, const char *pcpCode, CTime opDate /*= TIMENULL*/)
{
	CCSReturnCode olRc = RCSuccess;
	CCSPtrArray <SWGDATA> olSwgs;
	GetSwgsBySurn(lpSurn, olSwgs, opDate);
	int ilNumSwgs = olSwgs.GetSize();
	if(ilNumSwgs <= 0 && opDate != TIMENULL)
	{
		if(InsertSwg(lpSurn, pcpCode, opDate) == NULL)
			olRc = RCFailure;
	}
	else
	{
		for(int ilS = 0; ilS < ilNumSwgs; ilS++)
		{
			if((olRc = UpdateSwgCode(olSwgs[ilS].Urno, pcpCode)) != RCSuccess)
				break;
		}
	}

	return olRc;
}

CCSReturnCode CedaSwgData::UpdateSwgCode(long lpSwgUrno, const char *pcpCode)
{
	CCSReturnCode olRc = RCSuccess;

	SWGDATA *prlSwg = GetSwgByUrno(lpSwgUrno);
	if(prlSwg != NULL && strcmp(prlSwg->Code,pcpCode) && strlen(pcpCode) > 0)
	{
		char pclFieldList[100], pclData[100];
		char pclSelection[100];

		strcpy(pclFieldList,"CODE");
		strcpy(pclData,pcpCode);
		sprintf(pclSelection," WHERE URNO = '%ld%'",prlSwg->Urno);

		if((olRc = CedaAction("URT", pcmTableName, pclFieldList, pclSelection,"", pclData)) == RCSuccess)
		{
			strcpy(prlSwg->Code,pcpCode);
			ogCCSDdx.DataChanged((void *)this,SWG_UPDATE,(void *)prlSwg);
		}
	}
	return olRc;
}

SWGDATA *CedaSwgData::InsertSwg(long lpSurn, const char *pcpCode, CTime opDate)
{
	SWGDATA rlSwg, *prlSwg = NULL;
	rlSwg.Urno = ogBasicData.GetNextUrno();
	rlSwg.Surn = lpSurn;
	strcpy(rlSwg.Code, pcpCode);

	CTime olVpfr(opDate.GetYear(), opDate.GetMonth(), opDate.GetDay(), 0, 0, 0);
	rlSwg.Vpfr = olVpfr;
	rlSwg.Vpto = TIMENULL;

	char pclData[5000];
	char pclCommand[10] = "IRT";
	char pclSelection[100] = "";
	CString olListOfData;
	MakeCedaData(&omRecInfo,olListOfData,&rlSwg);
	strcpy(pclData,olListOfData);
	if(CedaAction(pclCommand, pcmTableName, pcmFieldList, pclSelection, "", pclData) == RCSuccess)
	{
		if((prlSwg = AddSwgInternal(rlSwg)) != NULL)
		{
			ogCCSDdx.DataChanged((void *)this, SWG_INSERT, (void *)prlSwg);
		}
	}

	return prlSwg;
}

CString CedaSwgData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaSwgData::DumpForUstf(long lpUstf, CTime opDate)
{
	CString olDumpStr;
	CCSPtrArray <SWGDATA> olSwgs;
	GetSwgsBySurn(lpUstf, olSwgs, opDate);
	int ilNumSwgs = olSwgs.GetSize();
	if(ilNumSwgs <= 0)
	{
		olDumpStr.Format("No SWGDATA Found for STF.URNO <%ld>\n",lpUstf);
	}
	else
	{
		for(int ilS = 0; ilS < ilNumSwgs; ilS++)
		{
			olDumpStr += Dump(olSwgs[ilS].Urno) + CString("\n");
		}
	}
	return olDumpStr;
}

CString CedaSwgData::Dump(long lpUrno)
{
	CString olDumpStr;
	SWGDATA *prlSwg = GetSwgByUrno(lpUrno);
	if(prlSwg == NULL)
	{
		olDumpStr.Format("No SWGDATA Found for SWG.URNO <%ld>\n",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlSwg);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumData = min(olDataArray.GetSize(),olFieldArray.GetSize());
		for(int ilD = 0; ilD < ilNumData; ilD++)
		{
			olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
		}
	}
	return olDumpStr;
}
