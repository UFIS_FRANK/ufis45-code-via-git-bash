// gatdiaps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaTplData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <BasePropertySheet.h>
#include <RegnDiagramPropSheet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RegnDiagramPropertySheet
//

//RegnDiagramPropertySheet::RegnDiagramPropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_REGN_DIAGRAM, pParentWnd, popViewer, iSelectPage)
RegnDiagramPropertySheet::RegnDiagramPropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61319), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageRegnArea);
	AddPage(&m_pageRegnTempl);

	// Change the caption in tab control of the PropertySheet
	//m_pageRegnArea.SetCaption(ID_PAGE_FILTER_REGNAREA);
	m_pageRegnArea.SetCaption(GetString(IDS_STRING61320));
	m_pageRegnTempl.SetCaption(GetString(IDS_STRING32808));

	// Prepare possible values for each PropertyPage
	CCSPtrArray <ALLOCUNIT> olRegnAreas;
	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_REGNGROUP,olRegnAreas);
	int ilNumRegnAreas = olRegnAreas.GetSize();
	for(int ilRegnArea = 0; ilRegnArea < ilNumRegnAreas; ilRegnArea++)
	{
		ALLOCUNIT *prlRegnArea = &olRegnAreas[ilRegnArea];
		m_pageRegnArea.omPossibleItems.Add(prlRegnArea->Name);
	}
	m_pageRegnArea.omPossibleItems.Add(GetString(IDS_THIRDPARTY)); // "Third Party"


/*	CCSPtrArray <TPLDATA> olRegnTempl;
	ogTplData.GetTplByDalo(olRegnTempl,ALLOCUNITTYPE_REGN);
	int ilNumTempl = olRegnTempl.GetSize();
	for(int ilTempl = 0; ilTempl < ilNumTempl; ilTempl++)
	{
		TPLDATA *prlRegnTempl = &olRegnTempl[ilTempl];
		m_pageRegnTempl.omPossibleItems.Add(prlRegnTempl->Tnam);
	}
*/
	ogTplData.GetTplNameByDalo(ALLOCUNITTYPE_REGN,m_pageRegnTempl.omPossibleItems);
	m_pageRegnTempl.omPossibleItems.Add(GetString(IDS_WITHOUT_TEMPLATE));

//	int n = ogRegnAreas.omMetaAllocUnitList.GetSize();
//	for (int i = 0; i < n; i++)
//		m_pageRegnArea.omPossibleItems.Add(ogRegnAreas.omMetaAllocUnitList[i].Alid);
}

void RegnDiagramPropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("RegnArea", m_pageRegnArea.omSelectedItems);
	pomViewer->GetFilter("Template", m_pageRegnTempl.omSelectedItems);
}

void RegnDiagramPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("RegnArea", m_pageRegnArea.omSelectedItems);
	pomViewer->SetFilter("Template", m_pageRegnTempl.omSelectedItems);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int RegnDiagramPropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedRegnAreas;
	
	pomViewer->GetFilter("RegnArea", olSelectedRegnAreas);
	

	if (!IsIdentical(olSelectedRegnAreas, m_pageRegnArea.omSelectedItems))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
