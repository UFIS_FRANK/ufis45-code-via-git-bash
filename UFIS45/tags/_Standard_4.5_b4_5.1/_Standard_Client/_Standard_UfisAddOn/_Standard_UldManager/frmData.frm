VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Begin VB.Form frmData 
   Caption         =   "Form1"
   ClientHeight    =   9960
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15240
   Icon            =   "frmData.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9960
   ScaleWidth      =   15240
   ShowInTaskbar   =   0   'False
   Begin AATLOGINLib.AatLogin ULogin 
      Height          =   690
      Left            =   5895
      TabIndex        =   42
      Top             =   7605
      Width           =   1635
      _Version        =   65536
      _ExtentX        =   2884
      _ExtentY        =   1217
      _StockProps     =   0
   End
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   8010
      Top             =   3810
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin TABLib.TAB tabCfg 
      Height          =   975
      Left            =   5280
      TabIndex        =   31
      Top             =   3810
      Width           =   2235
      _Version        =   65536
      _ExtentX        =   3942
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdFindDep 
      Caption         =   "cmdFindDep"
      Height          =   255
      Left            =   3660
      TabIndex        =   30
      Top             =   3480
      Width           =   1395
   End
   Begin VB.CommandButton cmdGetNextUrno 
      Caption         =   "cmdGetNextUrno"
      Height          =   315
      Left            =   2070
      TabIndex        =   28
      Top             =   8580
      Width           =   1395
   End
   Begin TABLib.TAB tabUrnos 
      Height          =   1395
      Left            =   210
      TabIndex        =   26
      Top             =   8040
      Width           =   1695
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   2461
      _StockProps     =   64
   End
   Begin TABLib.TAB tabReread 
      Height          =   975
      Left            =   120
      TabIndex        =   24
      Top             =   3780
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Left            =   5280
      TabIndex        =   21
      Text            =   "6"
      Top             =   6900
      Width           =   495
   End
   Begin TABLib.TAB tabCompressedView 
      Height          =   1035
      Left            =   120
      TabIndex        =   16
      Top             =   5700
      Width           =   5355
      _Version        =   65536
      _ExtentX        =   9446
      _ExtentY        =   1826
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdSearchInRotations 
      Caption         =   "Search"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1620
      Width           =   1275
   End
   Begin VB.TextBox txtSearch 
      Height          =   285
      Left            =   1440
      TabIndex        =   13
      Top             =   1620
      Width           =   1755
   End
   Begin UFISCOMLib.UfisCom aUfis 
      Left            =   3150
      Top             =   7320
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   767
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdWriteTabsToFile 
      Caption         =   "WriteTabsToFile"
      Height          =   435
      Left            =   180
      TabIndex        =   12
      Top             =   7320
      Width           =   1575
   End
   Begin VB.TextBox txtDateFrom 
      Height          =   315
      Left            =   1320
      TabIndex        =   10
      Top             =   6900
      Width           =   1635
   End
   Begin VB.TextBox txtDateTo 
      Height          =   315
      Left            =   3300
      TabIndex        =   9
      Top             =   6900
      Width           =   1635
   End
   Begin VB.CommandButton cmdChangeDate 
      Caption         =   "&Change Date"
      Height          =   315
      Left            =   180
      TabIndex        =   8
      Top             =   6900
      Width           =   1155
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   1
      Left            =   5220
      TabIndex        =   2
      Tag             =   "PSTTAB;URNO,PNAM; "
      Top             =   300
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   2
      Left            =   7440
      TabIndex        =   5
      Tag             =   "ROTATIONS;URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,LINE;"
      Top             =   300
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   0
      Left            =   120
      TabIndex        =   15
      Tag             =   $"frmData.frx":030A
      Top             =   300
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   3
      Left            =   120
      TabIndex        =   18
      Tag             =   "LOATAB;URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,ULDT,ULDP,RFLD,RTAB,ADDI,RMRK; "
      Top             =   2160
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   4
      Left            =   5220
      TabIndex        =   33
      Tag             =   "ALTTAB;ALC2,ALC3,PRFL;ORDER BY ALC2"
      Top             =   2160
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   5
      Left            =   7875
      TabIndex        =   36
      Tag             =   "ULDTAB;URNO,FLNU,ULDN,TYPE,CONF,CONT,ADDI,ORIG,DEST,DSSN,POSF,TIMF,TIMH,OFLD,TAAC,TFBU,REMA,RESE,CDAT,LSTU,USEC,USEU;"
      Top             =   2160
      Width           =   3375
      _Version        =   65536
      _ExtentX        =   5953
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   6
      Left            =   11520
      TabIndex        =   39
      Tag             =   "REMTAB;URNO,RURN,APPL,RTAB,PURP,TEXT,CDAT,LSTU,USEC,USEU;"
      Top             =   315
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   7
      Left            =   9180
      TabIndex        =   43
      Tag             =   "BDITAB;URNO,FLNU,TIME,TEXT;"
      Top             =   3705
      Width           =   3375
      _Version        =   65536
      _ExtentX        =   5953
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   7
      Left            =   9180
      TabIndex        =   45
      Top             =   3465
      Width           =   435
   End
   Begin VB.Label lblRecCount 
      Caption         =   "BDITAB"
      Height          =   255
      Index           =   7
      Left            =   9660
      TabIndex        =   44
      Top             =   3465
      Width           =   2895
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   6
      Left            =   11520
      TabIndex        =   41
      Top             =   75
      Width           =   435
   End
   Begin VB.Label lblRecCount 
      Caption         =   "REMTAB"
      Height          =   255
      Index           =   6
      Left            =   12000
      TabIndex        =   40
      Top             =   75
      Width           =   4215
   End
   Begin VB.Label lblRecCount 
      Caption         =   "ULDTAB"
      Height          =   255
      Index           =   5
      Left            =   8340
      TabIndex        =   38
      Top             =   1920
      Width           =   2895
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   5
      Left            =   7860
      TabIndex        =   37
      Top             =   1920
      Width           =   435
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   4
      Left            =   5220
      TabIndex        =   35
      Top             =   1920
      Width           =   435
   End
   Begin VB.Label lblRecCount 
      Caption         =   "ALTTAB"
      Height          =   255
      Index           =   4
      Left            =   5700
      TabIndex        =   34
      Top             =   1920
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Config"
      Height          =   285
      Left            =   5280
      TabIndex        =   32
      Top             =   3570
      Width           =   2265
   End
   Begin VB.Label lblUrno 
      Caption         =   "Label10"
      Height          =   315
      Left            =   1620
      TabIndex        =   29
      Top             =   8955
      Width           =   1395
   End
   Begin VB.Label Label9 
      Caption         =   "Next Urno"
      Height          =   195
      Left            =   210
      TabIndex        =   27
      Top             =   7860
      Width           =   1695
   End
   Begin VB.Label Label7 
      Caption         =   "tmp for reread of data"
      Height          =   315
      Left            =   120
      TabIndex        =   25
      Top             =   3480
      Width           =   3435
   End
   Begin VB.Label Label6 
      BackColor       =   &H80000008&
      Caption         =   "For internal use only"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   255
      Left            =   0
      TabIndex        =   23
      Top             =   5040
      Width           =   12675
   End
   Begin VB.Label Label3 
      Caption         =   "do"
      Height          =   195
      Left            =   4980
      TabIndex        =   22
      Top             =   6960
      Width           =   255
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   20
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Label lblRecCount 
      Caption         =   "LOATAB"
      Height          =   255
      Index           =   3
      Left            =   1620
      TabIndex        =   19
      Top             =   1920
      Width           =   2835
   End
   Begin VB.Label Label1 
      Caption         =   "For Compressed View"
      Height          =   315
      Left            =   120
      TabIndex        =   17
      Top             =   5400
      Width           =   5355
   End
   Begin VB.Label Label5 
      Caption         =   "  to"
      Height          =   195
      Left            =   2940
      TabIndex        =   11
      Top             =   6960
      Width           =   375
   End
   Begin VB.Label lblTable 
      Caption         =   "Gantt-Flights"
      Height          =   255
      Index           =   2
      Left            =   7440
      TabIndex        =   7
      Top             =   60
      Width           =   1455
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Label1"
      Height          =   255
      Index           =   2
      Left            =   8940
      TabIndex        =   6
      Top             =   60
      Width           =   2355
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   1
      Left            =   7320
      TabIndex        =   4
      Top             =   60
      Width           =   255
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Label1"
      Height          =   255
      Index           =   1
      Left            =   5520
      TabIndex        =   3
      Top             =   60
      Width           =   1815
   End
   Begin VB.Label lblRecCount 
      Caption         =   "AFTTAB"
      Height          =   255
      Index           =   0
      Left            =   1620
      TabIndex        =   1
      Top             =   60
      Width           =   2835
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   1455
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public sorted As Boolean
Public FullScreen As Boolean

'---------------------------------------------------------------
' inserts a new line into the data tab depending on the logical
' fieldlist.
' Return: The index of the inserted Line
'---------------------------------------------------------------
Public Function InsertEmptyLineForTab(Index As Integer) As Long
    Dim cnt As Long
    Dim i As Long
    Dim strVal As String
    
    cnt = ItemCount(tabData(Index).LogicalFieldList, ",")
    For i = 0 To cnt - 1
        strVal = strVal + ","
    Next i
    strVal = left(strVal, Len(strVal) - 1)
    tabData(Index).InsertTextLine strVal, False
    InsertEmptyLineForTab = tabData(Index).GetLineCount - 1
End Function

Public Sub LoadData()
    Dim myTab As TABLib.TAB
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim arrFields() As String
    Dim ilCount As Integer
    Dim strWhere As String
    Dim i As Long
    Dim fName As String
        
    If strShowHiddenData = "" Then
        Me.top = 99999
        Me.Hide
    Else
        Me.Show
    End If
    
    FullScreen = False
    'frmMain.MainStatusBar.Visible = True 'Just to start the BcProxy but do nothing in the form
    SetServerParameters
    aUfis.CallServer "SBC", "ULDMNG", "1/6", "1/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "ULDMNG", "2/6", "2/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "ULDMNG", "3/6", "3/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "ULDMNG", "4/6", "4/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "ULDMNG", "5/6", "5/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "ULDMNG", "6/6", "6/6", "", 240
    ilTabCnt = 0
    If readFromFile = False Then
        For Each myTab In tabData
            myTab.ResetContent
            'DoEvents
            arr = Split(myTab.Tag, ";")
            If UBound(arr) = 2 Then
                arrFields = Split(arr(1), ",")
                strWhere = arr(2)
                myTab.ResetContent
                myTab.AutoSizeByHeader = True
                myTab.HeaderString = ""
                myTab.HeaderLengthString = ""
                myTab.HeaderString = arr(1)
                myTab.EnableInlineEdit True
                myTab.CedaServerName = strServer
                myTab.CedaPort = "3357"
                myTab.CedaHopo = strHopo
                myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                myTab.CedaTabext = "TAB"
                myTab.CedaUser = strCurrentUser
                myTab.CedaWorkstation = frmData.aUfis.WorkStation
                myTab.CedaSendTimeout = "3"
                myTab.CedaReceiveTimeout = "240"
                myTab.CedaRecordSeparator = Chr(10)
                myTab.CedaIdentifier = "ULD_Mgr"
                myTab.ShowHorzScroller True
                myTab.EnableHeaderSizing True
                For i = 0 To UBound(arrFields)
                    If i = 0 Then
                        myTab.HeaderLengthString = "80,"
                    Else
                        myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
                    End If
                Next i
                myTab.LogicalFieldList = arr(1)
                frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                frmStartup.txtCurrentStatus.Refresh
'Special treatment for AFTTAB
                If arr(0) = "AFTTAB" Then
                    'amended by kkh on 11/04/2008
                    'strWhere = "WHERE (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S','O','OO','OS','SO')"
                    strWhere = "WHERE (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND FTYP IN ('S','O','OO','OS','SO') AND ADID IN ('A','B') OR (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S','O','OO','OS','SO') AND ADID IN ('D','B')"
                End If
'Special treatment for LOADTAB
                If arr(0) = "LOATAB" Then
                    strWhere = "WHERE FLNU IN ("
                    For i = 0 To tabData(0).GetLineCount - 1
                        If i Mod 300 = 0 Then
                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
                                       " AND TYPE='ULD' " '" AND DSSN IN ('CPM','DLS','UCM','USR') "
                            'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                            myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                            frmStartup.txtCurrentStatus.Refresh
                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                            frmStartup.txtDoneStatus.Refresh
                            strWhere = "WHERE FLNU IN ("
                        Else
                            If (i) = tabData(0).GetLineCount - 1 Then
                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
                                       " AND TYPE='ULD' " '" AND DSSN IN ('CPM','DLS','UCM','USR') "
                            Else
                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
                            End If
                        End If
                    Next i
                    'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                    myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If
'Special treatment for ULDTAB
                If arr(0) = "ULDTAB" Then
                    strWhere = "WHERE FLNU IN ("
                    For i = 0 To tabData(0).GetLineCount - 1
                        If i Mod 300 = 0 Then
                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
                            'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                            myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                            frmStartup.txtCurrentStatus.Refresh
                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                            frmStartup.txtDoneStatus.Refresh
                            strWhere = "WHERE FLNU IN ("
                        Else
                            If (i) = tabData(0).GetLineCount - 1 Then
                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
                            Else
                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
                            End If
                        End If
                    Next i
                    'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                    myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If
'Special treatment for BDITAB
                If arr(0) = "BDITAB" Then
                    strWhere = "WHERE FLNU IN ("
                    For i = 0 To tabData(0).GetLineCount - 1
                        If i Mod 300 = 0 Then
                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
                            'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                            myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                            frmStartup.txtCurrentStatus.Refresh
                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                            frmStartup.txtDoneStatus.Refresh
                            strWhere = "WHERE FLNU IN ("
                        Else
                            If (i) = tabData(0).GetLineCount - 1 Then
                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
                            Else
                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
                            End If
                        End If
                    Next i
                    'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                    myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If
'Special treatment for REMTAB
                If arr(0) = "REMTAB" Then
                    strWhere = "WHERE RURN IN ("
                    For i = 0 To tabData(0).GetLineCount - 1
                        If i Mod 300 = 0 Then
                            strWhere = strWhere + "'" + tabData(0).GetFieldValue(i, "URNO") + "')" + _
                                       " AND APPL='ULDMGR' AND RTAB='AFT' AND PURP='REMA'"
                            'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                            myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                            frmStartup.txtCurrentStatus.Refresh
                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                            frmStartup.txtDoneStatus.Refresh
                            strWhere = "WHERE RURN IN ("
                        Else
                            If (i) = tabData(0).GetLineCount - 1 Then
                                strWhere = strWhere + "'" + tabData(0).GetFieldValue(i, "URNO") + "')" + _
                                       " AND APPL='ULDMGR' AND RTAB='AFT' AND PURP='REMA'"
                            Else
                                strWhere = strWhere + "'" + tabData(0).GetFieldValue(i, "URNO") + "',"
                            End If
                        End If
                    Next i
                    'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                    myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If
                'arr(0) <> "ULDTAB" And
                If arr(0) <> "ROTATIONS" And arr(0) <> "LOATAB" And _
                   arr(0) <> "ULDTAB" And arr(0) <> "REMTAB" Then
                    'amended by kkh on 11/04/2008 , APP is missing re-assign value in
                    myTab.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                End If
                myTab.RedrawTab
                lblRecCount(ilTabCnt) = "Table: " + arr(0) + "  Records: " + CStr(myTab.GetLineCount)
                
                ilTabCnt = ilTabCnt + 1
                myTab.AutoSizeByHeader = True
                myTab.AutoSizeColumns
                myTab.myTag = myTab.left & "," & myTab.top & "," & myTab.Height & "," & myTab.Width
                frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                frmStartup.txtDoneStatus.Refresh
            End If
        Next
    Else
        For Each myTab In tabData
            arr = Split(myTab.Tag, ";")
            If UBound(arr) = 2 Then
                arrFields = Split(arr(1), ",")
                'fName = "C:\Ufis\Appl\ULDMGR_" + arr(0) + ".dat"
                fName = UFIS_APPL & "\ULDMGR_" + arr(0) + ".dat"
                myTab.ResetContent
                myTab.AutoSizeByHeader = True
                myTab.HeaderString = ""
                myTab.HeaderLengthString = ""
                myTab.HeaderString = arr(1)
                myTab.EnableInlineEdit True
                myTab.ShowHorzScroller True
                myTab.EnableHeaderSizing True
                For i = 0 To UBound(arrFields)
                    If i = 0 Then
                        myTab.HeaderLengthString = "80,"
                    Else
                        myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
                    End If
                Next i
                myTab.LogicalFieldList = arr(1)
                myTab.readFromFile fName
                myTab.RedrawTab
                lblRecCount(ilTabCnt) = "Table: " + arr(0) + "  Records: " + CStr(myTab.GetLineCount)
                ilTabCnt = ilTabCnt + 1
                myTab.AutoSizeByHeader = True
                myTab.AutoSizeColumns
                myTab.myTag = myTab.left & "," & myTab.top & "," & myTab.Height & "," & myTab.Width
            End If
        Next
    End If
'TO DO: Remove this
'GenerateTestData
'lblRecCount(3) = "Table: " + "LOATAB" + "  Records: " + CStr(tabData(3).GetLineCount)
'END TO DO: Remove this
        
    frmStartup.txtCurrentStatus = "Initialize the data ..."
    frmStartup.txtCurrentStatus.Refresh
    frmStartup.txtDoneStatus = "All data loaded"
    
    For i = 0 To tabData(0).GetLineCount - 1
        tabData(0).SetFieldValues i, "'CPM'", ""
    Next i
    tabCfg.ResetContent
    tabCfg.HeaderLengthString = "100,100"
    tabCfg.HeaderString = "FIELD,VALUE"
    tabCfg.LogicalFieldList = tabCfg.HeaderString
    tabCfg.EnableHeaderSizing True
    tabCfg.AutoSizeByHeader = True
    'tabCfg.readFromFile "C:\Ufis\Appl\ULDMANAGER.cfg"
    tabCfg.readFromFile UFIS_APPL & "\ULDMANAGER.cfg"
    tabCfg.AutoSizeColumns
    
    For i = 0 To tabCfg.GetLineCount - 1
        If tabCfg.GetColumnValue(i, 0) = "ARR_BAGGAGE" Then strARR_BaggageDefault = Val(tabCfg.GetColumnValue(i, 1))
        If tabCfg.GetColumnValue(i, 0) = "ARR_MAIL" Then strARR_MailDefault = Val(tabCfg.GetColumnValue(i, 1))
        If tabCfg.GetColumnValue(i, 0) = "ARR_CARGO" Then strARR_CargoDefault = Val(tabCfg.GetColumnValue(i, 1))
        If tabCfg.GetColumnValue(i, 0) = "DEP_BAGGAGE" Then strDEP_BaggageDefault = Val(tabCfg.GetColumnValue(i, 1))
        If tabCfg.GetColumnValue(i, 0) = "DEP_MAIL" Then strDEP_MailDefault = Val(tabCfg.GetColumnValue(i, 1))
        If tabCfg.GetColumnValue(i, 0) = "DEP_CARGO" Then strDEP_CargoDefault = Val(tabCfg.GetColumnValue(i, 1))
    Next i
    
    tabData(0).Sort "1,3", True, True
    tabData(1).Sort "1", True, True
    tabData(2).ResetContent
    tabData(0).IndexCreate "URNO", 0
    tabData(0).IndexCreate "RKEY", 1
    tabData(0).IndexCreate "FLNO", 4
    tabData(0).IndexCreate "DES3", 20
    tabData(0).SetInternalLineBuffer True
    tabData(3).Sort "1,2", True, True
    tabData(3).IndexCreate "FLNU", 1
    tabData(3).IndexCreate "RTAB", 13
    tabData(3).IndexCreate "RURN", 4
    tabData(3).SetInternalLineBuffer True
    
    tabData(4).ColumnWidthString = "3,4"
    tabData(4).Sort "0,1", True, True
    
    tabData(5).IndexCreate "URNO", 0
    tabData(5).IndexCreate "FLNU", 1
    tabData(5).SetInternalLineBuffer True
    tabData(5).Sort "0,1", True, True
    
    tabData(6).IndexCreate "URNO", 0
    tabData(6).IndexCreate "RURN", 1
    tabData(6).SetInternalLineBuffer True
    tabData(6).Sort "0,1", True, True
    
    tabData(7).IndexCreate "URNO", 0
    tabData(7).IndexCreate "FLNU", 1
    tabData(7).SetInternalLineBuffer True
    tabData(7).Sort "1", True, True
    
    
    tabUrnos.ResetContent
    tabUrnos.HeaderString = "URNO"
    tabUrnos.LogicalFieldList = "URNO"
    tabUrnos.HeaderLengthString = "100"
    tabUrnos.SetFieldSeparator Chr(15)
    
    
    tabReread.ResetContent
    tabReread.AutoSizeByHeader = True
    tabReread.HeaderString = ""
    tabReread.HeaderLengthString = ""
    tabReread.HeaderString = arr(1)
    tabReread.EnableInlineEdit True
    tabReread.CedaServerName = strServer
    tabReread.CedaPort = "3357"
    tabReread.CedaHopo = strHopo
    tabReread.CedaCurrentApplication = "ULD-Manager" & "," & GetApplVersion(True)
    tabReread.CedaTabext = "TAB"
    tabReread.CedaUser = strCurrentUser '"ULDMNG"
    tabReread.CedaWorkstation = aUfis.GetWorkStationName ' "wks104"
    tabReread.CedaSendTimeout = "3"
    tabReread.CedaReceiveTimeout = "240"
    tabReread.CedaRecordSeparator = Chr(10)
    tabReread.CedaIdentifier = "ULD-Manager"
    tabReread.ShowHorzScroller True
    tabReread.EnableHeaderSizing True
    
    
    'MakeRotationData
    tabData(2).AutoSizeByHeader = True
    tabData(2).AutoSizeColumns
    tabCompressedView.myTag = tabCompressedView.left & "," & tabCompressedView.top & "," & tabCompressedView.Height & "," & tabCompressedView.Width
    
    'make RFLD and RTAB in all TAB for LADTAB data empty
    For i = 0 To tabData(3).GetLineCount - 1
        tabData(3).SetFieldValues i, "RFLD,RTAB", ","
    Next i
    
    frmMain.InitConfigData
    If TimesInUTC = False Then
        frmStartup.txtCurrentStatus = "Converting UTC to local ..."
        ChangeAftToLocal
    End If
End Sub

Public Sub MakeRotationData()
    Dim i As Long
    Dim strVal As String
    Dim strTmp As String
    Dim strLine As String
    Dim myDat As Date
    Dim mydat2 As Date
    Dim strNextRkey As String
    Dim strCurrRkey As String
    Dim llCurr As Long
    Dim sss As String
    Dim strNextAdid As String
    Dim strCurrAdid As String
    Dim ilStep As Integer
    
'AFTTAB;
    tabData(2).ResetContent
    tabData(2).HeaderString = "URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,FLTI_A,FLTI_D,VIAL_A,VIAL_D"
    tabData(2).LogicalFieldList = "URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,FLTI_A,FLTI_D,VIAL_A,VIAL_D"
    tabData(2).HeaderLengthString = "80,80,80,180,180,80,80,30,30,30,30,30,30,30,30,30,40,40,50,50"
    tabData(2).EnableHeaderSizing True
    tabData(2).ShowHorzScroller True
    
    llCurr = 0
    While llCurr < tabData(0).GetLineCount - 1
    If 128928325 = tabData(0).GetFieldValue(llCurr, "URNO") Then
        strLine = ""
    End If
'  0    1    2    3    4   5    6    7     8    9    10  11   12   13   14   15   16  17    18   19   20    21  22   23   24   25   26   27
'URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES
'  0     1       2    3    4   5       6     7      8      9      10     11   12   13   14   15       16   17    18      19    20
'URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,FLTI_A, FLTI_D,LINE
        strLine = tabData(0).LogicalFieldList
        strCurrRkey = tabData(0).GetFieldValue(llCurr, "RKEY")
        strCurrAdid = tabData(0).GetFieldValue(llCurr, "ADID")
        If (llCurr + 1) < tabData(0).GetLineCount - 1 Then
            strNextRkey = tabData(0).GetFieldValue(llCurr + 1, "RKEY")
            strNextAdid = tabData(0).GetFieldValue(llCurr + 1, "ADID")
        Else
            strNextRkey = ""
            strNextAdid = ""
        End If
        If strCurrRkey = strNextRkey Then
            'Is a rotation
            ilStep = 2
            If strCurrAdid = "A" And strNextAdid = "D" Then
                strLine = tabData(0).GetFieldValue(llCurr, "URNO") + "," + tabData(0).GetFieldValue(llCurr + 1, "URNO") + ","
                strLine = strLine + tabData(0).GetColumnValue(llCurr, 1) + ","
                strVal = tabData(0).GetFieldValue(llCurr, "TIFA")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strVal = tabData(0).GetFieldValue(llCurr + 1, "TIFD")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLNO") + "," + tabData(0).GetFieldValue(llCurr + 1, "FLNO") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FTYP") + "," + tabData(0).GetFieldValue(llCurr + 1, "FTYP") + ","
                strLine = strLine + strCurrAdid + "," + strNextAdid + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "PSTD") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "TISD") + ",R,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "DES3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FLTI") + ","
            End If
            If strCurrAdid = "D" And strNextAdid = "A" Then
                strLine = tabData(0).GetFieldValue(llCurr + 1, "URNO") + "," + tabData(0).GetFieldValue(llCurr, "URNO") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "RKEY") + ","
                strVal = tabData(0).GetFieldValue(llCurr + 1, "TIFA")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strVal = tabData(0).GetFieldValue(llCurr, "TIFD")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FLNO") + "," + tabData(0).GetFieldValue(llCurr, "FLNO") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FTYP") + "," + tabData(0).GetFieldValue(llCurr, "FTYP") + ","
                strLine = strLine + strNextAdid + "," + strCurrAdid + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "PSTA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "TISA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTD") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISD") + ",R,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "DES3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FLTI") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ","
                
            End If
        Else
            ilStep = 1
            'Is a single flight and not rotation
            If strCurrAdid = "A" Then
                strLine = tabData(0).GetFieldValue(llCurr, "URNO") + "," + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "RKEY") + ","
                strVal = tabData(0).GetFieldValue(llCurr, "TIFA")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                myDat = DateAdd("n", 30, myDat)
                strTmp = Format(myDat, "YYYYMMDDhhmmss") + ","
                strLine = strLine + strTmp
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLNO") + "," + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FTYP") + "," + ","
                strLine = strLine + strCurrAdid + "," + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISA") + ","
                strLine = strLine + ","
                strLine = strLine + ",A,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "DES3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ",,"
            End If
            If strCurrAdid = "D" Then
                strLine = "," 'URNO_A
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "URNO") + "," 'URNO_D
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "RKEY") + "," 'RKEY
                strVal = tabData(0).GetFieldValue(llCurr, "TIFD")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    mydat2 = DateAdd("n", -30, myDat)
                    strTmp = Format(mydat2, "YYYYMMDDhhmmss") + "," + Format(myDat, "YYYYMMDDhhmmss") + "," 'TIFA + TIFD
                Else
                    strTmp = ",,"
                End If
                strLine = strLine + strTmp 'TIFA + TIFD
                strLine = strLine + "," '                                               FLNO_A
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLNO") + "," '        FLNO_D
                strLine = strLine + "," '                                               FTYP_A
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FTYP") + "," '        FTYP_D
                strLine = strLine + "," '                                               ADID_A
                strLine = strLine + strCurrAdid + "," '                                 ADID_D
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTD") + "," '        PSTD
                strLine = strLine + "," '                                               TISA
                'Erst mal alle positionen nach PSTA schieben egal ob departure
                strLine = strLine + "," '                                               PSTA
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISD") + ",D," '      TISD
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "DES3") + ",,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ","
            End If
        End If
        llCurr = llCurr + ilStep
        strLine = strLine + "," + CStr(llCurr)
        tabData(2).InsertTextLine strLine, False
        strLine = ""
    Wend
    tabData(2).Sort "3,4", True, True
    tabData(2).RedrawTab
    lblRecCount(2) = str(tabData(2).GetLineCount) + "Records"
End Sub


Public Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0
    aUfis.HomeAirport = strHopo
    aUfis.InitCom strServer, "CEDA"
    aUfis.Twe = strHopo + "," + strTableExt + "," + strAppl
    aUfis.UserName = strCurrentUser
    aUfis.WorkStation = aUfis.GetWorkStationName
    'aUfis.SetCedaPerameters strAppl, strHopo, strTableExt
    aUfis.SetCedaPerameters strCurrentUser, strHopo, strTableExt
    SetServerParameters
    If aUfis.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = aUfis.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            i = count + 1
            GetUTCOffset = CSng(strUtcArr(1) / 60)
        End If
    Next i
    aUfis.CleanupCom
End Function
'------------------------------------------------------------------------------------
' change the dates to store the new lines for another date in the tabs corresponding
' file
'------------------------------------------------------------------------------------
Private Sub cmdChangeDate_Click()
    Dim i As Long
    Dim j As Long
    Dim datFrom As Date
    Dim datTo As Date
    Dim cnt As Long
    Dim cnt2 As Long
    Dim strTmp As String
    Dim strTmp2 As String
    Dim strDay As String
    Dim strNewDay As String
    Dim strColumns As String
    Dim strValue As String
    Dim currCol As Long
    Dim strOrig As String
    Dim arr() As String
    Dim strDest As String
    
    strColumns = "11,12,13,14,30,31,34,35"
    cnt2 = ItemCount(strColumns, ",")
    cnt = tabData(0).GetLineCount
    For i = 0 To cnt - 1
        For j = 0 To cnt2 - 1
            currCol = CLng(GetRealItem(strColumns, CInt(j), ","))
            strTmp = tabData(0).GetColumnValue(i, currCol)
            strTmp = Replace(strTmp, txtDateFrom, txtDateTo)
            tabData(0).SetColumnValue i, currCol, strTmp
        Next j
    Next i

    cnt = tabData(2).GetLineCount
    For i = 0 To cnt - 1
        strTmp = tabData(2).GetColumnValue(i, 3)
        strTmp = Replace(strTmp, txtDateFrom, txtDateTo)
        tabData(2).SetColumnValue i, 3, strTmp
        strTmp = tabData(2).GetColumnValue(i, 4)
        strTmp = Replace(strTmp, txtDateFrom, txtDateTo)
        tabData(2).SetColumnValue i, 4, strTmp
    Next i
'********** Recalc the FLNO field of LOATAB => LH998/13 get /13 and increment it
    strDay = Mid(txtDateFrom, 7, 2)
    strNewDay = Mid(txtDateTo, 7, 2)
    cnt = tabData(3).GetLineCount - 1
    For i = 0 To cnt
        strTmp = tabData(3).GetFieldValue(i, "FLNO")
        arr = Split(strTmp, "/")
        If UBound(arr) = 1 Then
            strTmp = arr(1)
            If strTmp = strDay Then
                strTmp = strNewDay
                strValue = arr(0) & "/" & strTmp
                tabData(3).SetFieldValues i, "FLNO", strValue
            End If
        End If
    Next i
    tabData(3).Refresh
    datFrom = CedaDateToVb(txtDateFrom)
    datTo = CedaDateToVb(txtDateTo)
    datFrom = DateAdd("d", -1, datFrom)
    datTo = DateAdd("d", -1, datTo)
    txtDateFrom = Format(datFrom, "YYYYMMDD")
    txtDateTo = Format(datTo, "YYYYMMDD")
End Sub


Private Sub cmdGetNextUrno_Click()
    Dim strU As String
    strU = GetNextUrno
    lblUrno = strU
End Sub

Private Sub cmdSearchInRotations_Click()
    Dim i As Long
    Dim strLine As String
    Dim myStart As Long
    Dim blFound As Boolean
    
    blFound = False
    myStart = tabData(0).GetCurrentSelected + 1

    For i = myStart To tabData(0).GetLineCount - 1
        If InStr(1, tabData(0).GetLineValues(i), txtSearch) > 0 Then
            tabData(0).OnVScrollTo i
            tabData(0).SetCurrentSelection i
            i = tabData(0).GetLineCount
            blFound = True
        End If
    Next i
    If blFound = False Then
        tabData(0).SetCurrentSelection -1
        tabData(0).OnVScrollTo 0
    End If
End Sub

Private Sub cmdWriteTabsToFile_Click()
    Dim myTab As TABLib.TAB
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim fName As String
    
    For Each myTab In tabData
        arr = Split(myTab.Tag, ";")
        'fName = "C:\Ufis\Appl\ULDMGR_" + arr(0) + ".dat"
        fName = UFIS_APPL & "\ULDMGR_" + arr(0) + ".dat"
        myTab.WriteToFile fName, False
    Next

End Sub

Private Sub Form_Load()
''    Me.left = 0
'    Me.top = 0
    If strShowHiddenData = "" Then
        Me.Hide
    End If
End Sub
Public Function GetNextUrno() As String
    Dim cnt As Long
    Dim ret As String
    Dim i As Long
    
    cnt = tabUrnos.GetLineCount
    If cnt = 0 Then
        SetServerParameters
        If aUfis.CallServer("GMU", "", "*", "500", "", "120") = 0 Then
            cnt = aUfis.GetBufferCount
            For i = 0 To cnt - 1
                tabUrnos.InsertBuffer aUfis.GetBufferLine(i), ","
            Next i
        End If
    End If
    ret = tabUrnos.GetFieldValue(0, "URNO")
    tabUrnos.DeleteLine 0
    tabUrnos.Refresh
    GetNextUrno = ret
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'tabCfg.WriteToFile "C:\Ufis\Appl\ULDMANAGER.cfg", False
    tabCfg.WriteToFile UFIS_APPL & "\ULDMANAGER.cfg", False
End Sub

Private Sub tabData_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim arr() As String
    If FullScreen = False Then
        tabData(Index).Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
        FullScreen = True
        tabData(Index).ZOrder
    Else
        arr = Split(tabData(Index).myTag, ",")
        tabData(Index).Move CInt(arr(0)), CInt(arr(1)), CInt(arr(3)), CInt(arr(2))
        FullScreen = False
    End If
    Me.Refresh
End Sub

Public Sub ChangeAftToUTC()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    
    cnt = tabData(0).GetLineCount - 1
    fldCnt = ItemCount(strAftTimeFields, ",")
    For i = 0 To cnt
        For j = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(j), ",")
            strVal = tabData(0).GetFieldValue(i, strField)
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                myDat = DateAdd("h", -UTCOffset, myDat)
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                tabData(0).SetFieldValues i, strField, strVal
            End If
        Next j
    Next i
    tabData(0).Refresh
End Sub
Public Sub ChangeAftToLocal()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    
    cnt = tabData(0).GetLineCount - 1
    fldCnt = ItemCount(strAftTimeFields, ",")
    For i = 0 To cnt
        For j = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(j), ",")
            strVal = tabData(0).GetFieldValue(i, strField)
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                myDat = DateAdd("h", UTCOffset, myDat)
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                tabData(0).SetFieldValues i, strField, strVal
            End If
        Next j
    Next i
    tabData(0).Refresh
End Sub

Public Function LoginProcedure() As Boolean
    Dim LoginAnsw As String
    Dim tmpRegStrg As String
    Dim strRet As String
    Set ULogin.UfisComCtrl = aUfis
    ULogin.ApplicationName = "ULD-Manager"
    tmpRegStrg = "ULD-Manager" & ","
    'FKTTAB:  SUBD,FUNC,FUAL,TYPE,STAT
    tmpRegStrg = tmpRegStrg & "InitModu,InitModu,Initialisieren (InitModu),B,-"
    'Append any other Privileges
    'tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
    'tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
    ULogin.RegisterApplicationString = tmpRegStrg
    ULogin.VersionString = GetApplVersion(True)
    ULogin.InfoCaption = "Info about ULD-Manager"
    ULogin.InfoButtonVisible = True
    ULogin.InfoUfisVersion = "UFIS Version 4.5"
    ULogin.InfoAppVersion = CStr("ULD-Manager " + GetApplVersion(True))
    ULogin.InfoCopyright = "� 2003-2005 UFIS Airport Solutions GmbH"
    ULogin.InfoAAT = "UFIS Airport Solutions GmbH"
    strRet = ULogin.ShowLoginDialog()
    
    LoginAnsw = ULogin.GetPrivileges("InitModu")
    If LoginAnsw <> "" And strRet <> "CANCEL" Then LoginProcedure = True Else LoginProcedure = False
End Function


Public Function CallServer(command As String, Object As String, Fields As String, _
                           Data As String, where As String, timeout As String) As Boolean
                           
    Dim blRet As Boolean
    
    blRet = True
    frmMain.ServerSignal(2).FillStyle = vbSolid
    frmMain.ServerSignal(2).FillColor = vbYellow
    frmMain.ServerSignal(2).Refresh
    frmData.SetServerParameters
    If frmData.aUfis.CallServer(command, Object, Fields, Data, where, timeout) <> 0 Then
        blRet = True
    End If
    frmMain.ServerSignal(2).FillColor = vbBlue
    frmMain.ServerSignal(2).Refresh
    CallServer = blRet
End Function
'********************************************************************************
' This sub will generate additional LOATAB Data by random for each flight
' Additionally it will generate Estimated times for a random number of flights
' TO DO: Do not call this Sub when the application development has been finished!!
'********************************************************************************
Public Sub GenerateTestData()
    Dim cntFlights As Long
    Dim cntLoas As Long
    Dim noOfFlights As Long
    Dim noOfLoas As Long
    Dim currIDX As Long
    Dim loaIDX As Long
    Dim strEto As String
    Dim strUrno As String
    Dim i As Long
    Dim j As Long
    Dim strAdid As String

'MyValue = Int((6 * Rnd) + 1)   ' Generate random value between 1 and 6.


    cntLoas = tabData(3).GetLineCount
    cntFlights = tabData(0).GetLineCount
    
    noOfFlights = CLng((cntFlights * Rnd) + 1)
    'First we generate ETA/ETD for a number of flights
    For i = 0 To noOfFlights
        strAdid = tabData(0).GetFieldValue(i, "ADID")
        If strAdid = "A" Then
            strEto = tabData(0).GetFieldValue(i, "STOA")
            tabData(0).SetFieldValues i, "ETAI", strEto
        Else
            strEto = tabData(0).GetFieldValue(i, "STOD")
            tabData(0).SetFieldValues i, "ETDI", strEto
        End If
    Next i
    'Now add additional loatab entries
    noOfFlights = CLng((cntFlights * Rnd) + 1)
    For i = 0 To noOfFlights
        strUrno = tabData(0).GetFieldValue(i, "URNO")
        noOfLoas = CLng((cntLoas * Rnd) + 1) 'Randomize the amount of rows to be duplicated for
                                             ' the current flight
        For j = 0 To noOfLoas
            currIDX = CLng((cntLoas * Rnd) + 1)
            tabData(3).InsertTextLine tabData(3).GetLineValues(currIDX), False
            loaIDX = tabData(3).GetLineCount - 1
            tabData(3).SetFieldValues loaIDX, "FLNU", strUrno
        Next j
    Next i
End Sub

Public Function GetApplVersion(ShowExtended As Boolean) As String
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = ""
    Select Case SysInfo.OSPlatform
        Case 0  'Win32s
        Case 1  'Win 95/98
        Case 2  'Win NT
            If SysInfo.OSVersion > 4 Then
                'Win 2000
                If ShowExtended Then tmpRevis = "0." & tmpRevis
            Else
                'Win NT 4
                If ShowExtended Then
                    tmpMinor = right("0000" & tmpMinor, 2)
                    tmpRevis = right("0000" & tmpRevis, 4)
                End If
            End If
        Case Else
    End Select
    tmpTxt = tmpTxt & tmpMajor & "." & tmpMinor & "." & tmpRevis
    GetApplVersion = tmpTxt
End Function

Public Sub SetServerParameters()
    aUfis.HomeAirport = strHopo
    aUfis.TableExt = strTableExt
    aUfis.Module = "ULD-Manager" & "," & Me.GetApplVersion(True)
    aUfis.Twe = ""
    aUfis.Tws = "0"
    'aUfis.WorkStation = GetWorkStationName()
    aUfis.UserName = strCurrentUser
End Sub





