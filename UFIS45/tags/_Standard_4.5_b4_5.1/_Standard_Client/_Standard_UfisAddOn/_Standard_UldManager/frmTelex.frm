VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTelex 
   Caption         =   "Telex of Selected Entry"
   ClientHeight    =   4995
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6375
   Icon            =   "frmTelex.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4995
   ScaleWidth      =   6375
   ShowInTaskbar   =   0   'False
   Begin TABLib.TAB tabOverview 
      Height          =   1140
      Left            =   540
      TabIndex        =   4
      Top             =   2790
      Width           =   4200
      _Version        =   65536
      _ExtentX        =   7408
      _ExtentY        =   2011
      _StockProps     =   64
   End
   Begin TABLib.TAB TlxTab 
      Height          =   855
      Index           =   0
      Left            =   3630
      TabIndex        =   2
      Top             =   510
      Visible         =   0   'False
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   4680
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10716
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtTelex 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   30
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   30
      Width           =   3165
   End
   Begin TABLib.TAB TlxTab 
      Height          =   855
      Index           =   1
      Left            =   3630
      TabIndex        =   3
      Top             =   1530
      Visible         =   0   'False
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   1508
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private strTLX_URNO As String
Private Sub Form_Load()
    Dim ilLeft As Long
    Dim ilTop As Long
    Dim ilWidth As Long
    Dim ilHeight As Long
    
    txtTelex.FontSize = 8
    txtTelex.FontBold = True 'MyFontBold
    TlxTab(0).ResetContent
    TlxTab(1).ResetContent
    
'Read position from registry and move the window
    ilLeft = GetSetting(AppName:="ULD_Manager_Telex", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:=-1)
    ilTop = GetSetting(AppName:="ULD_Manager_Telex", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:=-1)
    ilWidth = GetSetting(AppName:="ULD_Manager_Telex", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:=-1)
    ilHeight = GetSetting(AppName:="ULD_Manager_Telex", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:=-1)
    If ilTop = -1 Or ilLeft = -1 Or ilWidth = -1 Or ilHeight = -1 Then
    Else
        Me.Move ilLeft, ilTop, ilWidth, ilHeight
    End If
End Sub
Public Sub TabReadTelex(TlxTabUrno As String)
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpSqlKey As String
    Dim LineNo As Long
    If TlxTabUrno <> strTLX_URNO Then
        strTLX_URNO = TlxTabUrno
        tmpText = ""
        tmpData = TlxTab(1).GetLinesByColumnValue(0, TlxTabUrno, 0)
        If tmpData <> "" Then
            LineNo = Val(tmpData)
            tmpText = TlxTab(1).GetColumnValue(LineNo, 1)
        Else
            TlxTab(0).ResetContent
            TlxTab(0).CedaServerName = strServer
            TlxTab(0).CedaPort = "3357"
            TlxTab(0).CedaHopo = strHopo
            TlxTab(0).CedaCurrentApplication = "ULD-Manager" & "," & frmData.GetApplVersion(True)
            TlxTab(0).CedaTabext = "TAB"
            TlxTab(0).CedaUser = strCurrentUser 'GetWindowsUserName()
            TlxTab(0).CedaWorkstation = frmData.aUfis.GetWorkStationName   'GetWorkstationName()
            TlxTab(0).CedaSendTimeout = "3"
            TlxTab(0).CedaReceiveTimeout = "240"
            TlxTab(0).CedaRecordSeparator = Chr(10)
            TlxTab(0).CedaIdentifier = "LoaTab"
            tmpCmd = "RT"
            tmpTable = "TLXTAB"
            tmpFields = "TXT1,TXT2"
            tmpData = ""
            tmpSqlKey = "WHERE URNO=" & strTLX_URNO
            If strServer <> "LOCAL" Then
                Screen.MousePointer = 11
                TlxTab(0).CedaAction tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey
                Screen.MousePointer = 0
                tmpText = TlxTab(0).GetColumnValue(0, 0) & TlxTab(0).GetColumnValue(0, 1)
            End If
            If tmpText = "" Then tmpText = "Telex not found. (TLXTAB.URNO=" & TlxTabUrno & ")"
            TlxTab(1).InsertTextLine TlxTabUrno & "," & tmpText, False
        End If
        tmpText = CleanString(tmpText, FOR_CLIENT, True)
        txtTelex.Text = tmpText
    End If
End Sub

Public Sub ReadTelexes(TlxTabUrnoList As String)
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpText As String
    Dim tmpSqlKey As String
    Dim LineNo As Long
    Dim i As Long
    
    tabOverview.ResetContent
    tabOverview.HeaderString = "URNO,TEXT"
    tabOverview.HeaderLengthString = "100,1000"
    tabOverview.LogicalFieldList = "URNO,TEXT"
    
    tabOverview.top = 0
    tabOverview.left = 0
    tabOverview.Width = Me.ScaleWidth
    txtTelex.left = 0
    txtTelex.top = tabOverview.Height
    txtTelex.Height = Me.ScaleHeight - tabOverview.Height - 30
    
    TlxTab(0).ResetContent
    TlxTab(0).CedaServerName = strServer
    TlxTab(0).CedaPort = "3357"
    TlxTab(0).CedaHopo = strHopo
    TlxTab(0).CedaCurrentApplication = "ULD-Manager" & "," & frmData.GetApplVersion(True)
    TlxTab(0).CedaTabext = "TAB"
    TlxTab(0).CedaUser = strCurrentUser 'GetWindowsUserName()
    TlxTab(0).CedaWorkstation = frmData.aUfis.GetWorkStationName   'GetWorkstationName()
    TlxTab(0).CedaSendTimeout = "3"
    TlxTab(0).CedaReceiveTimeout = "240"
    TlxTab(0).CedaRecordSeparator = Chr(10)
    TlxTab(0).CedaIdentifier = "LoaTab"
    tmpCmd = "RT"
    tmpTable = "TLXTAB"
    tmpFields = "URNO,TXT1,TXT2"
    tmpData = ""
    tmpSqlKey = "WHERE URNO IN (" & TlxTabUrnoList & ")"
    If strServer <> "LOCAL" Then
        Screen.MousePointer = 11
        TlxTab(0).CedaAction tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey
        Screen.MousePointer = 0
        tmpText = TlxTab(0).GetColumnValue(0, 1) & TlxTab(0).GetColumnValue(0, 2)
        For i = 0 To TlxTab(0).GetLineCount - 1
            tabOverview.InsertTextLine TlxTab(0).GetColumnValue(i, 0) & "," & TlxTab(0).GetColumnValue(i, 1) & TlxTab(0).GetColumnValue(i, 2), True
        Next i
    End If
    tabOverview.SetCurrentSelection 0
    If tmpText = "" Then tmpText = "Telex not found. (TLXTAB.URNO=" & TlxTabUrnoList & ")"
     tmpText = CleanString(tmpText, FOR_CLIENT, True)
    txtTelex.Text = tmpText
End Sub



Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim ilLeft As Long
    Dim ilTop As Long
    Dim ilHeight As Long
    Dim ilWidth As Long
    
    ilLeft = Me.left
    ilTop = Me.top
    ilWidth = Me.Width
    ilHeight = Me.Height
    SaveSetting "ULD_Manager_Telex", _
                "Startup", _
                "myLeft", _
                ilLeft
    SaveSetting "ULD_Manager_Telex", _
                "Startup", _
                "myTop", ilTop
    SaveSetting "ULD_Manager_Telex", _
                "Startup", _
                "myWidth", ilWidth
    SaveSetting "ULD_Manager_Telex", _
                "Startup", _
                "myHeight", ilHeight
    If UnloadMode = 0 Then
        Cancel = True
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - txtTelex.left * 2
    
    tabOverview.Width = Me.ScaleWidth
    If NewSize > 15 Then txtTelex.Width = NewSize
    NewSize = Me.ScaleHeight - StatusBar1.Height - 60
    If NewSize > 15 Then txtTelex.Height = NewSize
End Sub

Private Sub tabOverview_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpText As String
    If LineNo >= 0 Then
        tmpText = tabOverview.GetColumnValue(LineNo, 1)
        tmpText = CleanString(tmpText, FOR_CLIENT, True)
        txtTelex.Text = tmpText
    End If
End Sub
