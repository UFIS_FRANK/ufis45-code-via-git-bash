using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

namespace Ufis.Data
{
	/// <summary>
	/// This delegate will be called, when the packet n of m has been received to
	/// ensure async processing of reading.
	/// </summary>
	/// <param name="lpPackage">The number of the package received.</param>
	/// <remarks>none.</remarks>
	public delegate void	PacketReceived(long lpPackage);

	/// <summary>
	/// This interface is implemented in the class <B>UfisComReader</B>.
	/// It provides all interfaces and communication relevant properties
	/// to communicate with the CEDA CDRHDL.
	/// </summary>
	/// <remarks>Please instantiata this interface to start your own
	/// comunication with the CEDA CDRHDL and do not instatiate <B>UfisComReader</B>.
	/// </remarks>
	public interface IUfisComReader
	{
		/// <summary>
		/// Name of the application, which wants to use the CDRHDL.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a CDRHDL request in the CEDA logfiles.</remarks>
		string	ApplicationName
		{
			get;
			set;
		}
		/// <summary>
		/// The communication TCP/IP socket port to the UFIS server.
		/// </summary>
		/// <remarks>Necessary to establish the connection.</remarks>
		string	Port
		{
			get;
			set;
		}
		/// <summary>
		/// The UFIS server name.
		/// </summary>
		/// <remarks>This is necessary to establish the IP connection to the server.
		/// Alternatively you can use the direct TCP/IP adresss to connect.</remarks>
		string	ServerName
		{
			get;
			set;
		}
		/// <summary>
		/// The workstation name where the application is started.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a CDRHDL request in the CEDA logfiles.</remarks>
		string	WorkStation
		{
			get;
			set;
		}
		/// <summary>
		/// Table extention.
		/// </summary>
		/// <remarks>This is alway "TAB".</remarks>
		string	TableExtension
		{
			get;
			set;
		}
		/// <summary>
		/// The application user name.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a CDRHDL request in the CEDA logfiles.</remarks>
		string	UserName
		{
			get;
			set;
		}
		/// <summary>
		/// The home airport, where the project is running.
		/// </summary>
		/// <remarks>Please use this parameter correctly to ensure that you get data
		/// from the database.</remarks>
		string	HomeAirport
		{
			get;
			set;
		}
		/// <summary>
		/// The timeout for the sending part of the request.
		/// </summary>
		/// <remarks>The request will be terminated by a time out if the value expires.</remarks>
		string SendTimeoutSeconds
		{
			get;
			set;
		}
		/// <summary>
		/// The receive time out parameter is necessary to trigger the CDRHDL to set
		/// his own timeout to this value in order to abort database action, which last too long.
		/// </summary>
		/// <remarks>none.</remarks>
		string ReceiveTimeoutSeconds
		{
			get;
			set;
		}
		/// <summary>
		/// The record separator to be used to separate the records from each other.
		/// </summary>
		/// <remarks>none.</remarks>
		string RecordSeparator
		{
			get;
			set;
		}
		/// <summary>
		/// Currently not used.
		/// </summary>
		/// <remarks>none.</remarks>
		string CedaIdentifier
		{
			get;
			set;
		}
		/// <summary>
		/// Version string to ensure downward compatibility.
		/// </summary>
		/// <remarks>none.</remarks>
		string Version
		{
			get;
			set;
		}
		/// <summary>
		/// Build date of the assembly.
		/// </summary>
		/// <remarks>none.</remarks>
		string BuildDate
		{
			get;
		}
		/// <summary>
		/// Size of packets to be transmitted from the UFIS server to the client to
		/// ensure parallel reading and data transporting. This parameter must be read 
		/// from the CEDA.ini in the [GLOBAL] section. A useful value is;
		/// <P>PACKETSIZE=100000</P>
		/// </summary>
		/// <remarks>none.</remarks>
		long   PacketSize
		{
			get;
			set;
		}
		/// <summary>
		/// Currently not used, because CDRHDL does not support this command.
		/// </summary>
		/// <remarks>none.</remarks>
		string	ErrorSimulation
		{
			get;
			set;
		}
		/// <summary>
		/// Due to the fact that the received data can be sent in packages the <B>PacketReceived</B> 
		/// delegate is sent. <B>OnPacketReceived</B> is the corresponding event method.
		/// </summary>
		/// <remarks>none.</remarks>
		PacketReceived	OnPacketReceived
		{
			get;
			set;
		}
		/// <summary>
		/// Returns the last error. This might be an SQL error or a socket error.
		/// </summary>
		/// <returns>The string containing the error message.</returns>
		/// <remarks>none.</remarks>
		string	GetLastError();
		/// <summary>
		/// Call the server by using the settings and requests data from the CEDA CDRHDL.
		/// </summary>
		/// <param name="Command">CEDA router command, e.g. "RT", "URT", "IRT", "GFR" etc.</param>
		/// <param name="Table">The database table.</param>
		/// <param name="Fields">The fields of the database table.</param>
		/// <param name="Data">The data.</param>
		/// <param name="Where">SQL where clause.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>Uses <see cref="FastCedaConnection"/> to read the data.</remarks>
		bool	CedaAction(string Command, string Table, string Fields, string Data, string Where);
		/// <summary>
		/// Return the number of data currently in the read array.
		/// </summary>
		/// <returns>The number of current records.</returns>
		/// <remarks>none.</remarks>
		long	GetBufferCount();
		/// <summary>
		/// Returns the record in a comma separated string at <B>RecordNo</B> from 
		/// the internal buffer.
		/// </summary>
		/// <param name="RecordNo">Line number to get the record.</param>
		/// <returns>The data record in a comma separated string.</returns>
		/// <remarks>none.</remarks>
		string	GetRecord(int RecordNo);
	}

	/// <summary>
	/// Implements the <B>IUfisComReader</B> interface. This class shall not be
	/// instantiated directly.
	/// </summary>
	/// <remarks>This class is used internally by the <see cref="IDatabase"/>
	/// and the <see cref="ITable"/> class to communicate with the UFIS server.
	/// The application developer has nothing to do with this class in a regular case,
	/// bacause he uses <B>IDatabase</B> and <B>ITable</B> to handle the application
	/// data.
	/// <P></P><P></P>
	/// This class is responsible for the pure data handling. The communication to
	/// the server is part of <see cref="FastCedaConnection"/>.
	/// <seealso cref="FastCedaConnection"/>
	/// </remarks>
	internal class UfisComReader : System.ComponentModel.Component, IUfisComReader
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components	 = null;
		private	FastCedaConnection				fastCedaConn = null;
		private	string							version		 = "4.5.0.2";
		private	string							buildDate	 = "20040618";
		private	PacketReceived					onPacketReceived;

		/// <summary>
		/// Name of the application, which wants to use the CDRHDL.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a CDRHDL request in the CEDA logfiles.</remarks>
		public string ApplicationName
		{
			get
			{
				return this.fastCedaConn.omAppl;
			}
			set
			{
				this.fastCedaConn.omAppl = value;
			}
		}

		/// <summary>
		/// The communication TCP/IP socket port to the UFIS server.
		/// </summary>
		/// <remarks>Necessary to establish the connection.</remarks>
		public string Port
		{
			get
			{
				return this.fastCedaConn.omPort;
			}
			set
			{
				this.fastCedaConn.omPort = value;
			}
		}

		/// <summary>
		/// The UFIS server name.
		/// </summary>
		/// <remarks>This is necessary to establish the IP connection to the server.
		/// Alternatively you can use the direct TCP/IP adresss to connect.</remarks>
		public string ServerName
		{
			get
			{
				return this.fastCedaConn.omServer;
			}
			set
			{
				this.fastCedaConn.omServer = value;
			}
		}

		/// <summary>
		/// The workstation name where the application is started.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a CDRHDL request in the CEDA logfiles.</remarks>
		public string WorkStation
		{
			get
			{
				return this.fastCedaConn.omWks;
			}
			set
			{
				this.fastCedaConn.omWks = value;
			}
		}

		/// <summary>
		/// Table extention.
		/// </summary>
		/// <remarks>This is always "TAB".</remarks>
		public string TableExtension
		{
			get
			{
				return this.fastCedaConn.omTabext;
			}
			set
			{
				this.fastCedaConn.omTabext = value;
			}
		}

		/// <summary>
		/// The application user name.
		/// </summary>
		/// <remarks>This must be set in order to see the producer of
		/// a CDRHDL request in the CEDA logfiles.</remarks>
		public string UserName
		{
			get
			{
				return this.fastCedaConn.omUser;
			}
			set
			{
				this.fastCedaConn.omUser = value;
			}
		}

		/// <summary>
		/// The home airport, where the project is running.
		/// </summary>
		/// <remarks>Please use this parameter correctly to ensure that you get data
		/// from the database.</remarks>
		public string HomeAirport
		{
			get
			{
				return this.fastCedaConn.omHopo;
			}
			set
			{
				this.fastCedaConn.omHopo = value;
			}
		}

		/// <summary>
		/// The timeout for the sending part of the request.
		/// </summary>
		/// <remarks>The request will be terminated by a time out if the value expires.</remarks>
		public string SendTimeoutSeconds
		{
			get
			{
				return this.fastCedaConn.imSendTimeout.ToString();
			}
			set
			{
				this.fastCedaConn.imSendTimeout = int.Parse(value);
			}
		}

		/// <summary>
		/// The receive time out parameter is necessary to trigger the CDRHDL to set
		/// his own timeout to this value in order to abort database action, which last too long.
		/// </summary>
		/// <remarks>none.</remarks>
		public string ReceiveTimeoutSeconds
		{
			get
			{
				return this.fastCedaConn.imReceiveTimeout.ToString();
			}
			set
			{
				this.fastCedaConn.imReceiveTimeout = int.Parse(value);
			}
		}

		/// <summary>
		/// The record separator to be used to separate the records from each other.
		/// </summary>
		/// <remarks>none.</remarks>
		public string RecordSeparator
		{
			get
			{
				return this.fastCedaConn.omSepa;
			}
			set
			{
				this.fastCedaConn.omSepa = value;
			}
		}

		/// <summary>
		/// Currently not used.
		/// </summary>
		/// <remarks>none.</remarks>
		public string CedaIdentifier
		{
			get
			{
				return this.fastCedaConn.omIdentifier;
			}
			set
			{
				this.fastCedaConn.omIdentifier = value;
			}
		}

		/// <summary>
		/// Version string to ensure downward compatibility.
		/// </summary>
		/// <remarks>none.</remarks>
		public string Version
		{
			get
			{
				return version;
			}
			set
			{
				this.version = value;
			}
		}

		/// <summary>
		/// Build date of the assembly.
		/// </summary>
		/// <remarks>none.</remarks>
		public string BuildDate
		{
			get
			{
				return this.buildDate;
			}
			set
			{
				this.buildDate = value;
			}
		}

		/// <summary>
		/// Size of packets to be transmitted from the UFIS server to the client to
		/// ensure parallel reading and data transporting. This parameter must be read 
		/// from the CEDA.ini in the [GLOBAL] section. A useful value is;
		/// <P>PACKETSIZE=100000</P>
		/// </summary>
		/// <remarks>none.</remarks>
		public long	PacketSize
		{
			get
			{
				return this.fastCedaConn.lmPacketSize;
			}
			set
			{
				this.fastCedaConn.lmPacketSize = value;
			}
		}

		/// <summary>
		/// Currently not used, because the CDRHDL does not support this command.
		/// </summary>
		/// <remarks>none.</remarks>
		public string ErrorSimulation
		{
			get
			{
				return this.fastCedaConn.omSimErr;
			}
			set
			{
				this.fastCedaConn.omSimErr = value;
			}
		}

		/// <summary>
		/// TO DO
		/// </summary>
		/// <remarks>TO DO</remarks>
		public PacketReceived	OnPacketReceived
		{
			get 
			{
				return this.onPacketReceived;
			}
			set
			{
				this.onPacketReceived = value;
			}
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="container">The container due to the fact that this is a component.</param>
		/// <remarks>Creates a new instance of FastCedaConnection internally.</remarks>
		public UfisComReader(System.ComponentModel.IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			fastCedaConn = new FastCedaConnection(this);
		}

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <remarks>Creates a new instance of FastCedaConnection internally.</remarks>
		public UfisComReader()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			fastCedaConn = new FastCedaConnection(this);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Returns the last error. This might be an SQL error or a socket error.
		/// </summary>
		/// <returns>The string containing the error message.</returns>
		/// <remarks>none.</remarks>
		public string GetLastError() 
		{
			string strResult = this.fastCedaConn.GetError();
			return strResult;
		}

		/// <summary>
		/// Call the server by using the settings and requests data from the CEDA CDRHDL.
		/// </summary>
		/// <param name="Command">CEDA router command, e.g. "RT", "URT", "IRT", "GFR" etc.</param>
		/// <param name="DBTable">The database table.</param>
		/// <param name="DBFields">The fields of the database table.</param>
		/// <param name="DBData">The data.</param>
		/// <param name="DBWhere">SQL where clause.</param>
		/// <returns>true = success, false = failed.</returns>
		/// <remarks>Uses <see cref="FastCedaConnection"/> to read the data.</remarks>
		public bool CedaAction(string Command, string DBTable, string DBFields, string DBData, string DBWhere) 
		{
			bool blRet;
			this.fastCedaConn.omCommand	= Command;
			this.fastCedaConn.omTable		= DBTable;
			this.fastCedaConn.omFields		= DBFields;
			this.fastCedaConn.omWhere		= DBWhere;

			if (this.fastCedaConn.CedaAction() == 0)
			{
				blRet = true;
			}
			else
			{
				blRet = false;
			}
			return blRet;
		}

		/// <summary>
		/// Return the number of data currently in the read array.
		/// </summary>
		/// <returns>The number of current records.</returns>
		/// <remarks>none.</remarks>
		public long GetBufferCount() 
		{
			long llCount;
			llCount = this.fastCedaConn.GetDataCount();
			return llCount;
		}

		/// <summary>
		/// Returns the record in a comma separated string at <B>RecordNo</B> from 
		/// the internal buffer.
		/// </summary>
		/// <param name="RecordNo">Line number to get the record.</param>
		/// <returns>The data record in a comma separated string.</returns>
		/// <remarks>none.</remarks>
		public string GetRecord(int RecordNo) 
		{
			string strResult = "";
			if (RecordNo < this.fastCedaConn.GetDataCount())
			{
				strResult = (string)this.fastCedaConn.omData[RecordNo];
			}

			return strResult;
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}
