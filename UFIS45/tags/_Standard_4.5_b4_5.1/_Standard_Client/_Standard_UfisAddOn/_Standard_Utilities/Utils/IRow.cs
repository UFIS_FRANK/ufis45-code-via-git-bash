using System;
using System.Collections;

namespace Ufis.Data
{
	/// <summary>
	/// IRow represents one record of a database table (<see cref="ITable"/>).
	/// The representation is an interface, which is implemented in <see cref="IRowImp"/>.
	/// To use the functionality, please do not directly instatiate an object from
	/// IRowImp directly. IRow allows to access the data by a stirng and an integer
	/// indexer. You can get the field values as comma separated list.
	/// </summary>
	/// <remarks>
	/// To use the functionality, please do not directly instatiate an object from
	/// IRowImp directly. All ITable access methods work exclusivly with IRow(s).
	/// </remarks>
	public interface IRow
	{
		/// <summary>
		/// Returns whether this instance is Thread safe or not.
		/// <seealso cref="IDatabase.SyncRoot"/>
		/// </summary>
		/// <example>
		/// This sample demonstrate the usage of instances in a multithreaded environment:
		/// <P></P>
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// // Now we generate our threaded instance.
		/// IDatabase myMultiThreadedDB = (IDatabase)myDB.SyncRoot;
		/// bool snc1 = myMultiThreadedDB.IsSyncronized;
		/// bool snc2 = myDB.IsSyncronized;
		/// 
		/// // The result of snc1 = true
		/// // The result of snc2 = false
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		bool IsSyncronized
		{
			get;
		}

		/// <summary>
		/// This returns a thread safe wrapper of an instance.
		/// </summary>
		/// <example>
		/// This sample demonstrate the usage of instances in a multithreaded environment:
		/// <P></P>
		/// <code>
		/// [C#]
		/// IDatabase myDB = UT.GetMemDB();
		/// // Now we generate our threaded instance.
		/// IDatabase myMultiThreadedDB = (IDatabase)myDB.SyncRoot;
		/// bool snc1 = myMultiThreadedDB.IsSyncronized;
		/// bool snc2 = myDB.IsSyncronized;
		/// 
		/// // The result of snc1 = true
		/// // The result of snc2 = false
		/// </code>
		/// </example>
		/// <remarks>none.</remarks>
		object	SyncRoot
		{
			get;
		}

		/// <summary>
		/// Set or gets the sort string. The sort string is an internal string, which
		/// fills the field's data values left adjusted into a string to ensure the
		/// correct sorting according to a phone book.
		/// </summary>
		/// <remarks>
		/// The application developer must not take care about the sort string of a
		/// IRow instance. He simply calls the ITable <see cref="ITable.Sort(string, bool)"/> method.
		/// All other things are done internally.
		/// </remarks>
		string	Sort
		{
			set;
			get;
		}
		/// <summary>
		/// This is the integer based index to access the field value within a ITable
		/// instance. The index must be valid to avoid an overflow exception.
		/// The field value can be set and get.
		/// </summary>
		/// <param name="index">Integer based index of the field.</param>
		/// <remarks>none</remarks>
		/// <example>
		/// The example demonstrate how to create a new IRow and store it in ITable and
		/// also store it in the database.
		/// <code>
		/// ITable tabVal = myDB["VAL"];						// Get the ITable instance from IDatabase
		/// IRow rowVal = tabVal.CreateEmptyRow();				// Create an empty row with all needed structures
		/// rowVal[0] = "STATMGR";
		/// rowVal[1] = currDBRow["URNO"];
		/// rowVal[2] = "1111111";
		/// rowVal[3] = UT.DateTimeToCeda( dtValidFrom.Value);
		/// rowVal[4] = UT.DateTimeToCeda( dtValidTo.Value);
		/// rowVal.Status = State.Created;						// Set the status to new
		/// tabVal.Add(rowVal);									// Add the row to the ITable instance.
		/// tabVal.Save();										// Save the record in the database.
		/// tabVal.ReorganizeIndexes();							// Reorganizes the indexes of the ITable instance.
		/// </code>
		/// </example>
		/// <exception cref="OverflowException">When the index is out of bounds.</exception>
		string this[int index]
		{
			get;
			set;
		}

		/// <summary>
		/// This is the string based index to access the field value within a ITable
		/// instance by it's logical name, which was intitally defined by calling the
		/// IDatabase.<see cref="IDatabase.Bind"/> method. 
		/// The index must be valid otherwise you'll get a null reference.
		/// The field value can be set and get.
		/// </summary>
		/// <param name="field">Integer based index of the field.</param>
		/// <remarks>none</remarks>
		/// <example>
		/// The example demonstrate how to create a new IRow and store it in ITable and
		/// also store it in the database.
		/// <code>
		/// ITable tabVal = myDB["VAL"];						// Get the ITable instance from IDatabase
		/// IRow rowVal = tabVal.CreateEmptyRow();				// Create an empty row with all needed structures
		/// rowVal["APPL"] = "STATMGR";
		/// rowVal["UVAL"] = currDBRow["URNO"];
		/// rowVal["FREQ"] = "1111111";
		/// rowVal["VAFR"] = UT.DateTimeToCeda( dtValidFrom.Value);
		/// rowVal["VATO"] = UT.DateTimeToCeda( dtValidTo.Value);
		/// rowVal.Status = State.Created;						// Set the status to new
		/// tabVal.Add(rowVal);									// Add the row to the ITable instance.
		/// tabVal.Save();										// Save the record in the database.
		/// tabVal.ReorganizeIndexes();							// Reorganizes the indexes of the ITable instance.
		/// </code>
		/// </example>
		string this[string field]
		{
			get;
			set;
		}

		/// <summary>
		/// Copies this row and assigns a new unique record number to the newly created row
		/// </summary>
		/// <returns>IRow as a new row.</returns>
		/// <remarks>Use this method to create a new record based on an existing.</remarks>
		IRow Copy();

		/// <summary>
		/// Copies this row but without assigning a new unique record number
		/// </summary>
		/// <returns>IRow as a new row.</returns>
		/// <remarks>
		///		Use this method to create a new record based on an existing. 
		///		This is a deep copy of the IRow instance.
		///	</remarks>
		IRow CopyRaw();

		/// <summary>
		/// Return or update the field separator for this row. The field separator
		/// is per default set to comma. When you change the field separator of a
		/// row the ITable's FieldSeparator will also be changed, because it makes
		/// no sense to change it for a single row.
		/// </summary>
		/// <remarks>
		/// You should set the field separator for an <see cref="ITable"/> object
		/// once at application start. During runtime it is to handled carefully.
		/// </remarks>
		char FieldSeparator
		{
			set;
			get;
		}

		/// <summary>
		/// Sets the values for the specified fields of this row.
		/// </summary>
		/// <param name="fields">Comma separated list of logical fields.</param>
		/// <param name="values">Comma separated list of field values.</param>
		/// <remarks>
		/// none
		/// </remarks>
		/// <example>
		/// The following example demonstrate the usage of <B>SetFieldValues</B>
		/// <code>
		/// ITable tAdresses = myDatabase["ADRESSES"];
		/// string fields = "ID,Name,Zip,City";
		/// string data   = "10,John,93993,Austin";
		/// IRow row = tAdresses.CreateEmptyRow();				// Create an empty row with all needed structures
		/// row.SetFieldValues(fields, data);
		/// </code>
		/// </example>
		void	SetFieldValues(string fields,string values);

		/// <summary>
		/// Sets the values for all fields of this row.
		/// </summary>
		/// <param name="values">Comma separated list of field values.</param>
		/// <remarks>
		/// none
		/// </remarks>
		/// <example>
		/// The following example demonstrate the usage of <B>SetFieldValues</B>
		/// <code>
		/// ITable tAdresses = myDatabase["ADRESSES"];
		/// string data   = "10,John,93993,Austin";
		/// IRow row = tAdresses.CreateEmptyRow();	// Create an empty row with all needed structures
		/// row.SetFieldValues(data);				// You must be sure that data contains items for all fields
		/// </code>
		/// </example>
		void SetFieldValues(string values);

		/// <summary>
		/// Returns the values for the specified fields of this row as a string.
		/// </summary>
		/// <param name="fields">The field names for the values you are interested in.</param>
		/// <returns>A comma separated string of field data.</returns>
		/// <remarks>none</remarks>
		/// <example>
		/// The following example demonstrate the usage of <B>FieldValues</B>.
		/// <code>
		/// [C#]
		///	IDatabase myDB = UT.GetMemDB();
		///	ITable myAdresses = myDBmyDB.Bind("Adresses", "CADR", "ID,Name,Zip,City", "10,32,10,32", "ID,Name,Zip,City");
		///	IRow row = myAdresses.CreateEmptyRow();
		///	row["ID"]	= "100";
		///	row["Name"] = "John";
		///	row["Zip"]  = "999";
		///	row["City"] = "London";
		///	string vals = row.FieldValues("Name,City");
		///	// vals contains now: "John,London"
		/// </code>
		/// </example>
		string FieldValues(string fields);

		/// <summary>
		/// Returns the values for all fields of this row as a string.
		/// </summary>
		/// <returns>All data values as comma separated string.</returns>
		/// <remarks>none</remarks>
		/// <example>
		/// The following example demonstrate the usage of <B>FieldValues</B>.
		/// <code>
		/// [C#]
		///	IDatabase myDB = UT.GetMemDB();
		///	ITable myAdresses = myDBmyDB.Bind("Adresses", "CADR", "ID,Name,Zip,City", "10,32,10,32", "ID,Name,Zip,City");
		///	IRow row = myAdresses.CreateEmptyRow();
		///	row["ID"]	= "100";
		///	row["Name"] = "John";
		///	row["Zip"]  = "999";
		///	row["City"] = "London";
		///	string vals = row.FieldValues();
		///	// vals contains now: "100,John,999,London"
		/// </code>
		/// </example>
		string FieldValues();

		/// <summary>
		/// Returns the values for all fields of this row in an Arraylist.
		/// </summary>
		/// <returns>All data values of the row in an ArrayList object.</returns>
		/// <remarks>none</remarks>
		ArrayList FieldValueList();

		/// <summary>
		/// Returns the value for the specified field of this row converted to an DateTime object.
		/// </summary>
		/// <param name="name">The name of the field that has to be converted into DateTime.</param>
		/// <returns>A DateTime value.</returns>
		/// <remarks>none</remarks>
		DateTime FieldAsDateTime(string name);

		/// <summary>
		/// Returns the value for the specified field of this row converted to an int object.
		/// </summary>
		/// <param name="name">The name of the field that has to be converted into Integer.</param>
		/// <returns>An integer converted value of the string stored in the row.</returns>
		/// <remarks>none</remarks>
		int FieldAsInt(string name);

		/// <summary>
		/// Returns the value for the specified field of this row converted to an double object
		/// </summary>
		/// <param name="name">The name of the field that has to be converted into double.</param>
		/// <returns>An double converted value of the string stored in the row.</returns>
		/// <remarks>none</remarks>
		double FieldAsDouble(string name);

		/// <summary>
		/// Returns whether this row has been modified or not.
		/// </summary>
		/// <remarks>This cannot be changed. Please use the <see cref="State"/> property instead.</remarks>
		bool Modified
		{
			get;
		}

		/// <summary>
		/// Return or modify the current state of this row.
		/// </summary>
		/// <remarks>Please refer to the <see cref="State"/> enumeration definition.</remarks>
		State Status
		{
			get;
			set;
		}

		/// <summary>
		/// Returns the field count of this row.
		/// </summary>
		/// <remarks>none</remarks>
		int Count
		{
			get;
		}

        /// <summary>
        /// Start Tracking the changes
        /// Note: Current Changes will be still there.
        /// </summary>
        void StartTrackChanges();//AM 20080314

        /// <summary>
        /// Stop Tracking the changes. 
        /// Note: Current Changes will be still there.
        /// </summary>
        void StopTrackChanges();//AM 20080314

        /// <summary>
        /// Clear the field list of changes.
        /// </summary>
        void ClearChgs();//AM 20080314

        /// <summary>
        /// Get the changes for this row
        /// </summary>
        /// <param name="strFldList">Name of the Field with changes separated by separator</param>
        /// <param name="strValList">Related Values with changes (seperated by separator)</param>
        /// <param name="separator">Separator for Field List and Value List</param>
        /// <returns></returns>
        bool GetChgs(out string strFldList, out string strValList, string separator);//AM 20080314 
	}
}
