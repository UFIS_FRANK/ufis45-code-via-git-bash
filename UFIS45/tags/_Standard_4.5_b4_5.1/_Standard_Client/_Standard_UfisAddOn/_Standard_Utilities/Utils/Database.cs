using System;
using System.Reflection;
using Ufis.Utils;

namespace Ufis.Data
{
	/// <summary>
	/// Database class represents the <B>one and only</B> instance of the Database object. 
	/// The application may instantiate only one object. For this purpose the class
	/// is able to detect the existance of it's object. This class may
	/// not be used directly but with the help of Ufis.Utils method 
	/// see <see cref="Ufis.Utils.UT.GetMemDB"/> <I>"Ufis.Utils.GetMemDB()".</I>
	/// </summary>
	/// <remarks>
	/// The application developer has nothing to do with this class. It is an internal
	/// black box of the <B>Ufis.Utils</B> and <B>Ufis.Data</B> namespaces. The onyl
	/// representation a developer sees is an instance of the IDatabase object, which
	/// is managed by the <B>Ufis.Utils.UT</B> class.
	/// </remarks>
	/// <example>
	/// The folloing example demonstrates what the application developer need and must see:
	/// <code>
	/// [C#]
	/// using Ufis.Utils;
	/// using Ufis.Data;
	/// public myClass
	/// {
	///		IDatabase myDB = null;
	///		public myClass()
	///		{
	///			myDB = UT.GetMemDB(); // everything is done for you in UT
	///		}
	/// }
	/// </code>
	/// </example>
	public class Database
	{
		static Assembly	implementation = null;
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <remarks>none.</remarks>
		public Database()
		{
		}
		/// <summary>
		/// Create the one and only member of the <see cref="IDatabaseImp"/> object. If this object already
		/// exists, the existing instance will be retured.
		/// </summary>
		/// <returns>The one and only instance of <see cref="IDatabaseImp"/> internally
		/// and <see cref="IDatabase"/> for the external representation.</returns>
		/// <remarks>This will be called internally to create the one an only
		/// instance of the <see cref="IDatabase"/> object.</remarks>
		static public IDatabase Create()
		{
			if (implementation == null)
			{
				System.Type type = Type.GetType("Ufis.Data.IDatabaseImp");
				implementation = Assembly.GetAssembly(type);
			}

			return (IDatabase)implementation.CreateInstance("Ufis.Data.IDatabaseImp",false);
		}

        static IDatabase db = null;
        /// <summary>
        /// Create the one and only member of the <see cref="ISyncDatabaseImp"/> object. If this object already
        /// exists, the existing instance will be retured.
        /// </summary>
        /// <returns>The one and only instance of <see cref="ISyncDatabaseImp"/> internally
        /// and <see cref="IDatabase"/> for the external representation.</returns>
        /// <remarks>This will be called internally to create the one an only
        /// instance of the <see cref="IDatabase"/> object.</remarks>
        static public IDatabase CreateSync()
        {//AM 20080312
            if (db == null)
                db = new Ufis.Data.ISyncDatabaseImp((IDatabaseImp)Create());
            return db;
        }
	}
}
