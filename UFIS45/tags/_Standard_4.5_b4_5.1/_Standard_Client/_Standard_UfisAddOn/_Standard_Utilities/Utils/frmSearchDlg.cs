using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;

namespace Ufis.Utils
{
	/// <summary>
	/// The class <B>frmSearchDlg</B> Provides a simple form for searching purposes. 
	/// This form does not search.
	/// It simple sets the public member <see cref="frmSearchDlg.SearchText"/>. The application
	/// must perform the search according to it's requirements.
	/// </summary>
	/// <remarks>Use this class to get a search criteria from the user.</remarks>
	public class frmSearchDlg : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button cmdFindNext;
		/// <summary>
		/// SearchText contains the search criteria entered by the user. After the
		/// dialog is closed the caller method must read this member to handle the
		/// further processing of the search.
		/// </summary>
		/// <remarks>
		/// You can set the SearchText from the caller method to predefine the SearchText
		/// or to redisplay the last search text.
		/// </remarks>
		public String SearchText;
		private System.Windows.Forms.TextBox txtFind;
		private System.Windows.Forms.Button cmdClose;
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Constructor. The pSearchText string is used to predefine a search criteria.
		/// </summary>
		/// <param name="pSearchText">Predefined search criteria.</param>
		/// <remarks>none</remarks>
		public frmSearchDlg(String pSearchText)
		{
			SearchText = pSearchText;
			InitializeComponent();
		}
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <remarks>none</remarks>
		public frmSearchDlg()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">Threaded indicator.</param>
		/// <remarks>none.</remarks>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtFind = new System.Windows.Forms.TextBox();
			this.cmdFindNext = new System.Windows.Forms.Button();
			this.cmdClose = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Find what:";
			// 
			// txtFind
			// 
			this.txtFind.Location = new System.Drawing.Point(76, 12);
			this.txtFind.Name = "txtFind";
			this.txtFind.Size = new System.Drawing.Size(300, 20);
			this.txtFind.TabIndex = 1;
			this.txtFind.Text = "";
			// 
			// cmdFindNext
			// 
			this.cmdFindNext.Location = new System.Drawing.Point(113, 44);
			this.cmdFindNext.Name = "cmdFindNext";
			this.cmdFindNext.Size = new System.Drawing.Size(76, 24);
			this.cmdFindNext.TabIndex = 2;
			this.cmdFindNext.Text = "&Find Next";
			this.cmdFindNext.Click += new System.EventHandler(this.cmdFindNext_Click);
			// 
			// cmdClose
			// 
			this.cmdClose.Location = new System.Drawing.Point(195, 44);
			this.cmdClose.Name = "cmdClose";
			this.cmdClose.Size = new System.Drawing.Size(76, 24);
			this.cmdClose.TabIndex = 3;
			this.cmdClose.Text = "&Close";
			this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
			// 
			// button1
			// 
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.button1.Location = new System.Drawing.Point(296, 48);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(72, 24);
			this.button1.TabIndex = 4;
			this.button1.Text = "button1";
			// 
			// frmSearchDlg
			// 
			this.AcceptButton = this.cmdFindNext;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(384, 77);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.cmdClose);
			this.Controls.Add(this.cmdFindNext);
			this.Controls.Add(this.txtFind);
			this.Controls.Add(this.label1);
			this.Name = "frmSearchDlg";
			this.Text = "Find ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmSearchDlg_Closing);
			this.Load += new System.EventHandler(this.frmSearchDlg_Load);
			this.Activated += new System.EventHandler(this.frmSearchDlg_Activated);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmSearchDlg_Activated(object sender, System.EventArgs e)
		{
			txtFind.Text = SearchText;
		}

		private void cmdFindNext_Click(object sender, System.EventArgs e)
		{
			UT.LastSearchText = txtFind.Text;
			this.Close();
		}

		private void frmSearchDlg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
		}

		private void frmSearchDlg_Load(object sender, System.EventArgs e)
		{
		
		}

		private void cmdClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void reportDocument1_InitReport(object sender, System.EventArgs e)
		{
		
		}
	}
}
