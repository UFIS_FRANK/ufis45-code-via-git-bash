#define UseEncoder

using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Ufis.Utils;

namespace Ufis.Data
{
	/// <summary>
	/// This class provides the functionality to access the CDRHSL CEDA process for
	/// data requests. The CDRHDL fetches the data from oracle and sends it directly 
	/// to this class. The advantage is to get the data very fast. After the data has
	/// been received the data strean is extracted into the single sections and the 
	/// application is able to store all data.
	/// </summary>
	internal class FastCedaConnection
	{
		/// <summary>
		/// The application, which request data.
		/// </summary>
		public string			omAppl;
		/// <summary>
		/// The command, which is sent to the CDRHDL to get the data
		/// </summary>
		public string			omCommand;
		/// <summary>
		/// TODO
		/// </summary>
		public string			omConnect;
		/// <summary>
		/// The fields, which are to be fetched from the database.
		/// </summary>
		public string			omFields;
		/// <summary>
		/// The current home airport (configured in the ceda.ini).
		/// </summary>
		public string			omHopo;
		/// <summary>
		/// Currently not used.
		/// </summary>
		public string			omIdentifier;
		/// <summary>
		/// The TCP/IP socket port, which is used for the socket communication with
		/// the CDRHDL.
		/// </summary>
		public string			omPort;
		/// <summary>
		/// The server name, where the application is connected.
		/// </summary>
		public string			omServer;
		/// <summary>
		/// The table extention. This is an historical parameter and is always "TAB".
		/// </summary>
		public string			omTabext;
		/// <summary>
		/// The database table for the select.
		/// </summary>
		public string			omTable;
		/// <summary>
		/// The separator to separate the records.
		/// </summary>
		public string			omSepa;
		/// <summary>
		/// The logged in User of the application.
		/// </summary>
		public string			omUser;
		/// <summary>
		/// The SQL-Where clause.
		/// </summary>
		public string			omWhere;
		/// <summary>
		/// The workstation, where the client application is running.
		/// </summary>
		public string			omWks;
		/// <summary>
		/// Indicates an error, which can occur based on the socket communication layer or
		/// an SQL error due to an incorrect SQL statement.
		/// </summary>
		public	bool			bmError;
		/// <summary>
		///	This is the storage array for the extracted data delivered by the CDRHDL.
		/// </summary>
		public	ArrayList		omData;

		//Timeout parameters
		/// <summary>
		/// Specifies the send timeout parameter in seconds. After this timer has elapsed
		/// and the request (send of the request to the CDRHDL) is not successfully 
		/// transmitted, the socket will abort the request.
		/// </summary>
		public	int				imSendTimeout;
		/// <summary>
		/// The receive timeout will be sent to the CDRHDL. For the case that the database
		/// request initiated by the CDRHDL will elapse this setting, the CDRHDL abort the
		/// request from the client application and will send no data. This is an emergency
		/// exit to avoid not accessible applications due to SQL statements, which kill the
		/// database.
		/// </summary>
		public	int				imReceiveTimeout;
		/// <summary>
		/// The data can be packed so the client can work in parts of the request, during the
		/// CDRHDL fetches the next packet.
		/// </summary>
		public	long			lmPacketSize;
		/// <summary>
		/// TODO
		/// </summary>
		public	IUfisComReader	reader;
		/// <summary>
		/// TODO
		/// </summary>
		public	string			omSimErr;

		//********************************
		// internally used members
		//********************************
		private string			omErrorText;
		private Socket			omSocket;
		private	bool			bmIsConnected;
		private	StringBuilder	omDataBuffer;
		private long			lmCurrentPacketSize;
		private	int				imWindowsCodePage;


		/// <summary>
		/// Calls the CDRCOM via Socket communication accoring to the parameters set in
		/// the class members omCommand, omIdentifier, omTable, omTabext, omHopo, omFields,
		/// omWhere, omUser, omWks, omSepa, omAppl, imReceiveTimeout, lmPacketSize.
		/// The method will call the <see cref="SendData()"/> method to transmit the request and
		/// then the <see cref="ReceiveData()"/> method to receive the data from CDRHDL.
		/// </summary>
		/// <returns>0=Success or -1 for Error. To get the error <see cref="GetError()"/> method.</returns>
		//return 0 for SUCCESS of -1 for Error
		public int CedaAction()
		{
			ClearDataBuffer();
			this.bmError = false;
			if (this.SendData())
			{
				if (this.ReceiveData())
				{
					return 0;
				}
				else
				{
					return this.bmError ? -1 : 0;
				}
			}
			else
			{
				return this.bmError ? -1 : 0;
			}

		}
		/// <summary>
		/// Sends the request with all parameters to the CDRHDL.
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// The parameters which must have been set befor the function can work properly are:
		/// <list type="bullet">
		///		<item><see cref="omCommand"/></item>
		///		<item><see cref="omIdentifier"/></item>
		///		<item><see cref="omTable"/></item>
		///		<item><see cref="omTabext"/></item>
		///		<item><see cref="omHopo"/></item>
		///		<item><see cref="omFields"/></item>
		///		<item><see cref="omWhere"/></item>
		///		<item><see cref="omUser"/></item>
		///		<item><see cref="omWks"/></item>
		///		<item><see cref="omSepa"/></item>
		///		<item><see cref="omAppl"/></item>
		///		<item><see cref="imReceiveTimeout"/></item>
		///		<item><see cref="lmPacketSize"/></item>
		/// </list>
		/// </remarks>
		public bool SendData()
		{
			string olCommStr;
			string olTmpStr = "";
			string olTotPart;
			string olT2;
			long llBytes = 0;

			this.bmError = false;

			olTotPart = "{=TOT=}";

			olTmpStr += "{=CMD=}" + this.omCommand;
			olTmpStr += "{=IDN=}" + this.omIdentifier;
			olTmpStr += "{=TBL=}" + this.omTable;
			olTmpStr += "{=EXT=}" + this.omTabext;
			olTmpStr += "{=HOPO=}" + this.omHopo;
			olTmpStr += "{=FLD=}" + this.omFields;
			olTmpStr += "{=WHE=}" + this.omWhere;
			olTmpStr += "{=USR=}" + this.omUser;
			olTmpStr += "{=WKS=}" + this.omWks;
			olTmpStr += "{=SEPA=}" + this.omSepa;
			olTmpStr += "{=APP=}" + this.omAppl;

			// use packaging ?
			if (this.lmPacketSize > 0)
			{
				olTmpStr += "{=PACK=}" + this.lmPacketSize.ToString();
			}

			// simulate error ?
			if (this.omSimErr.Length > 0)
			{
				olTmpStr += "{=SIMERR=}" + this.omSimErr;
			}


			llBytes = olTmpStr.Length + 16;
			olT2 = llBytes.ToString("D9");
			olCommStr = olTotPart + olT2 + olTmpStr;
			llBytes = olCommStr.Length; 

			if (ConnectToCeda(omServer) != -1)
			{
				if (omSocket != null)
				{
					Encoding olAE = Encoding.GetEncoding(this.imWindowsCodePage);
					string olMsg;
					int ilSend;
					try
					{
						byte[] olBuffer = new byte[olCommStr.Length];
						olAE.GetBytes(olCommStr,0,olCommStr.Length,olBuffer,0);

						ilSend = omSocket.Send(olBuffer,olBuffer.Length, SocketFlags.None);
						return true;
					}
					catch(System.Exception e)
					{
						olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
						System.Diagnostics.Debug.WriteLine(olMsg);
					}
				}
			}
			
			return false;
		}
		/// <summary>
		/// This method receives the data from the CDRHDL. The data, which is expected depends
		/// on the settings of the public members.
		/// </summary>
		/// <returns>true=success, false=failed. Please refer to <see cref="GetError()"/>.</returns>
		/// <remarks>
		/// The parameters which must have been set befor the function can work properly are:
		/// <list type="bullet">
		///		<item><see cref="omCommand"/></item>
		///		<item><see cref="omIdentifier"/></item>
		///		<item><see cref="omTable"/></item>
		///		<item><see cref="omTabext"/></item>
		///		<item><see cref="omHopo"/></item>
		///		<item><see cref="omFields"/></item>
		///		<item><see cref="omWhere"/></item>
		///		<item><see cref="omUser"/></item>
		///		<item><see cref="omWks"/></item>
		///		<item><see cref="omSepa"/></item>
		///		<item><see cref="omAppl"/></item>
		///		<item><see cref="imReceiveTimeout"/></item>
		///		<item><see cref="lmPacketSize"/></item>
		/// </list>
		/// </remarks>
		public bool ReceiveData()
		{
			bool blOK		   = true;	
			bool blCompleted   = false;
			bool blFirstPacket = true;

			string	olMsg;

			if (omSocket == null)
				return false;

			while(blOK && !blCompleted)
			{
				blOK = ReceivePacketData(blFirstPacket,ref blCompleted);
				blFirstPacket = false;
			}

			if (blOK)
			{
				if (this.omDataBuffer.Length > 0)
				{
					ExtractTextLineFast(this.omData, this.omDataBuffer.ToString(),this.omSepa);
				}

				try
				{
					Encoding olAE = Encoding.GetEncoding(this.imWindowsCodePage);
					string olAnswerStr = "{=ACK=}ACK";
					byte[] olBuffer = new byte[olAnswerStr.Length];
					olAE.GetBytes(olAnswerStr,0,olAnswerStr.Length,olBuffer,0);
					omSocket.Send(olBuffer,olBuffer.Length,SocketFlags.None);
				}
				catch(System.Exception e)
				{
					olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
					System.Diagnostics.Debug.WriteLine(olMsg);
					return false;
				}
			}

			ResetConnection();

			return blOK;

		}

		bool ReceivePacketData(bool bpFirstPacket,ref bool bpCompleted)
		{
			string olMsg;

			byte[]	pclBuf = new byte[25001];
			string	pclRetFirst;
			string	pclKeyWord;
			string  pclResult		= "";
			int		ilDataBegin;
			byte[] pclTotalDataBuffer = null;

			int  ilBytes;
			int	 llSize;
			int  llTransferBytes = 0;
			int	 ilReceive = 0;

			Encoding olAE = Encoding.GetEncoding(this.imWindowsCodePage);

			try
			{
				byte[] olBuffer = new byte[100];
				ilReceive = omSocket.Receive(olBuffer, 16, SocketFlags.None);
				pclRetFirst = olAE.GetString(olBuffer,0,16);
			}
			catch(System.Exception e)
			{
				olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
				System.Diagnostics.Debug.WriteLine(olMsg);
				return false;
			}

			pclKeyWord   = "{=TOT=}"; 
			ilDataBegin = GetKeyItem(out pclResult,out llSize, pclRetFirst, pclKeyWord, "{=", true); 
			if (ilDataBegin == -1)
				return false;

			int llTotal = int.Parse(pclResult);
			llTotal -= 16;
			if (llTotal < 0) //take care that it is not negative
			{
				llTotal = 0;
			}

			if (pclTotalDataBuffer == null)
			{
				pclTotalDataBuffer = new byte[llTotal+1];
			}

			try
			{
				ilBytes = omSocket.Receive(pclTotalDataBuffer, llTotal,  SocketFlags.None);
			}
			catch(System.Exception e)
			{
				olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
				System.Diagnostics.Debug.WriteLine(olMsg);
				return false;
			}

			llTransferBytes += ilBytes;

			while (llTransferBytes < llTotal)
			{
				try
				{
					ilBytes = omSocket.Receive(pclTotalDataBuffer,llTransferBytes, (llTotal-llTransferBytes), SocketFlags.None);
					llTransferBytes += ilBytes;
				}
				catch(System.Exception e)
				{
					olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
					System.Diagnostics.Debug.WriteLine(olMsg);
					return false;
				}
			}

			//----------------------------
			// Extract the incomming data
#if	UseEncoder
			// we can't use UTF8Encoder class because this class tries to convert 2 bytes to a single unicode character
			// if there are invalid byte combinations, the UTF8Encoder will ignore this bytes (see UnicodeCategory)
			string olTotalDataBuffer = olAE.GetString(pclTotalDataBuffer,0,llTransferBytes);
#else
			string olTotalDataBuffer = string.Empty;
			StringBuilder builder = new StringBuilder(llTransferBytes);
			byte[]	pair = new byte[2]; 
			pair[1] = 0;
			for (int i = 0; i < llTransferBytes; i++)
			{
				pair[0] = pclTotalDataBuffer[i];
				builder.Append(BitConverter.ToChar(pair,0));
			}
			olTotalDataBuffer = builder.ToString();
#endif
			pclKeyWord = "{=CMD=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omCommand = pclResult;
			} 

			pclKeyWord = "{=IDN=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omIdentifier = pclResult;
			} 

			pclKeyWord = "{=TBL=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omTable = pclResult;
			} 

			pclKeyWord = "{=EXT=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omTabext = pclResult;
			} 

			pclKeyWord = "{=HOPO=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omHopo = pclResult;
			} 

			pclKeyWord = "{=FLD=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omFields = pclResult;
			} 

			pclKeyWord = "{=WHE=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omWhere = pclResult;
			} 

			pclKeyWord = "{=USR=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omUser = pclResult;
			} 

			pclKeyWord = "{=WKS=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omWks  = pclResult;
			} 

			pclKeyWord = "{=APP=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omAppl  = pclResult;
			} 

			pclKeyWord = "{=SEPA=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omSepa  = pclResult;
			} 

			pclKeyWord = "{=ERR=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				omErrorText = pclResult;
				this.bmError = false;
				if (omErrorText.Length > 0)
				{
					this.bmError = true;
				}
			} 

			pclKeyWord = "{=PACK=}"; 
			ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
			if (ilDataBegin != -1) 
			{ 
				this.lmCurrentPacketSize = long.Parse(pclResult);
				if (this.reader.OnPacketReceived != null)
				{
					this.reader.OnPacketReceived(this.lmCurrentPacketSize);					
				}
			}
			else
			{
				this.lmCurrentPacketSize = 0;
			}

			int ilDataLen = 0;
			if (bmError == false)
			{
				pclKeyWord = "{=DAT=}"; 
				ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", false); 
				if (ilDataBegin != -1) 
				{ 
					if (bpFirstPacket)
					{
						ClearDataBuffer();
						ilDataLen = olTotalDataBuffer.Length - ilDataBegin;
						if (ilDataLen > 0)
						{
							this.omDataBuffer.Append(olTotalDataBuffer.Substring(ilDataBegin));
							this.omAppl		  = pclResult;
						}
					}
					else
					{
						ilDataLen = olTotalDataBuffer.Length - ilDataBegin;
						if (ilDataLen > 0)
						{
							this.omDataBuffer.Append(omSepa);
							this.omDataBuffer.Append(olTotalDataBuffer.Substring(ilDataBegin));
						}
					}

					if (this.lmPacketSize <= 0)
					{
						bpCompleted = true;
					}
					else if (lmCurrentPacketSize < this.lmPacketSize)
					{
						bpCompleted = true;
					}
					else
					{
						bpCompleted = false;
					}

				}
				else
				{
					if (this.lmPacketSize <= 0)
					{
						bpCompleted = true;
					}
					else if (lmCurrentPacketSize < this.lmPacketSize)
					{
						bpCompleted = true;
					}
					else
					{
						bpCompleted = false;
					}
				}
			}
			else
			{
				ClearDataBuffer();
			}

			return this.bmError == false;
		}

		public int CedaActionOld()
		{
			string olCommStr;
			string olTmpStr = "";
			string olTotPart;
			string olT2;
			long llBytes = 0;

			byte[]	pclBuf = new byte[25001];
			string	pclRetFirst;
			string	pclKeyWord;
			string  pclResult		= "";
			int		ilDataBegin;
			byte[] pclTotalDataBuffer = null;

			int  ilBytes;
			int	 llSize;
			int  llTransferBytes = 0;

			this.bmError = false;

			olTotPart = "{=TOT=}";

			olTmpStr += "{=CMD=}" + this.omCommand;
			olTmpStr += "{=IDN=}" + this.omIdentifier;
			olTmpStr += "{=TBL=}" + this.omTable;
			olTmpStr += "{=EXT=}" + this.omTabext;
			olTmpStr += "{=HOPO=}" + this.omHopo;
			olTmpStr += "{=FLD=}" + this.omFields;
			olTmpStr += "{=WHE=}" + this.omWhere;
			olTmpStr += "{=USR=}" + this.omUser;
			olTmpStr += "{=WKS=}" + this.omWks;
			olTmpStr += "{=SEPA=}" + this.omSepa;
			olTmpStr += "{=APP=}" + this.omAppl;

			llBytes = olTmpStr.Length + 16;
			olT2 = llBytes.ToString("D9");
			olCommStr = olTotPart + olT2 + olTmpStr;
			llBytes = olCommStr.Length; 

			if (ConnectToCeda(omServer) != -1)
			{
				if (omSocket != null)
				{
					Encoding olAE = Encoding.GetEncoding(this.imWindowsCodePage);
					string olMsg;
					int ilSend;
					int ilReceive;
					try
					{
						byte[] olBuffer = new byte[olCommStr.Length];
						olAE.GetBytes(olCommStr,0,olCommStr.Length,olBuffer,0);

						ilSend = omSocket.Send(olBuffer,olBuffer.Length, SocketFlags.None);
					}
					catch(Exception e)
					{
						olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
						System.Diagnostics.Debug.WriteLine(olMsg);
						return -1;
					}

					try
					{
						byte[] olBuffer = new byte[100];
						ilReceive = omSocket.Receive(olBuffer, 16, SocketFlags.None);
						pclRetFirst = olAE.GetString(olBuffer,0,16);
					}
					catch(Exception e)
					{
						olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
						System.Diagnostics.Debug.WriteLine(olMsg);
						return -1;
					}

					pclKeyWord   = "{=TOT=}"; 
					ilDataBegin = GetKeyItem(out pclResult,out llSize, pclRetFirst, pclKeyWord, "{=", true); 
					if (ilDataBegin == -1)
						return -1;

					int llTotal = int.Parse(pclResult);
					llTotal -= 16;
					if (llTotal < 0) //take care that it is not negative
					{
						llTotal = 0;
					}

					if (pclTotalDataBuffer == null)
					{
						pclTotalDataBuffer = new byte[llTotal+1];
					}

					try
					{
						ilBytes = omSocket.Receive(pclTotalDataBuffer, llTotal,  SocketFlags.None);
					}
					catch(Exception e)
					{
						olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
						System.Diagnostics.Debug.WriteLine(olMsg);
						return -1;
					}

					llTransferBytes += ilBytes;

					while (llTransferBytes < llTotal)
					{
						try
						{
							ilBytes = omSocket.Receive(pclTotalDataBuffer,llTransferBytes, (llTotal-llTransferBytes), SocketFlags.None);
							llTransferBytes += ilBytes;
						}
						catch(Exception e)
						{
							olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
							System.Diagnostics.Debug.WriteLine(olMsg);
							return -1;
						}
					}

					//----------------------------
					// Extract the incomming data
					string olTotalDataBuffer = olAE.GetString(pclTotalDataBuffer,0,llTransferBytes);
					pclKeyWord = "{=CMD=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omCommand = pclResult;
					} 

					pclKeyWord = "{=IDN=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omIdentifier = pclResult;
					} 

					pclKeyWord = "{=TBL=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omTable = pclResult;
					} 

					pclKeyWord = "{=EXT=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omTabext = pclResult;
					} 

					pclKeyWord = "{=HOPO=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omHopo = pclResult;
					} 

					pclKeyWord = "{=FLD=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omFields = pclResult;
					} 

					pclKeyWord = "{=WHE=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omWhere = pclResult;
					} 

					pclKeyWord = "{=USR=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omUser = pclResult;
					} 

					pclKeyWord = "{=WKS=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omWks  = pclResult;
					} 

					pclKeyWord = "{=APP=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omAppl  = pclResult;
					} 

					pclKeyWord = "{=SEPA=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omSepa  = pclResult;
					} 

					pclKeyWord = "{=ERR=}"; 
					ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", true); 
					if (ilDataBegin != -1) 
					{ 
						omErrorText = pclResult;
						this.bmError = false;
						if (omErrorText.Length > 0)
						{
							this.bmError = true;
						}
					} 

					int ilDataLen = 0;
					if (bmError == false)
					{
						pclKeyWord = "{=DAT=}"; 
						ilDataBegin = GetKeyItem(out pclResult, out llSize, olTotalDataBuffer, pclKeyWord, "{=", false); 
						if (ilDataBegin != -1) 
						{ 
							ClearDataBuffer();
							ilDataLen = olTotalDataBuffer.Length - ilDataBegin;
							if (ilDataLen > 0)
							{
								this.omDataBuffer.Append(olTotalDataBuffer.Substring(ilDataBegin));
								this.omAppl		  = pclResult;
								ExtractTextLineFast(this.omData, this.omDataBuffer.ToString(),this.omSepa);
							}
						}
					}
					else
					{
						ClearDataBuffer();
					}
					try
					{
						string olAnswerStr = "{=ACK=}ACK";
						byte[] olBuffer = new byte[olAnswerStr.Length];
						olAE.GetBytes(olAnswerStr,0,olAnswerStr.Length,olBuffer,0);
						omSocket.Send(olBuffer,olBuffer.Length,SocketFlags.None);
					}
					catch(Exception e)
					{
						olMsg = string.Format("CEDA-Socket Error: {0}", e.Message);
						System.Diagnostics.Debug.WriteLine(olMsg);
						return -1;
					}

					// End data extractions
					//----------------------------
					ResetConnection();
				}
			}

			ResetConnection();

			return this.bmError == true ? -1 : 0;
		}

		
		/// <summary>
		/// Returns the error text as a string. For the case of no error has occured, 
		/// an empty string will be returned.
		/// </summary>
		/// <returns>The error string.</returns>
		public string GetError()
		{
			return this.omErrorText;
		}
		/// <summary>
		/// Returns the number of records fetched from the server.
		/// </summary>
		/// <returns></returns>
		public long GetDataCount()
		{
			return this.omData.Count;
		}
		/// <summary>
		/// Return a record for the index from the internal data array.
		/// </summary>
		/// <param name="ipIdx">Index of the record to be returned.</param>
		/// <returns>A comma separated string of data, which represents a record.</returns>
		public string GetDataRecord(int ipIdx)
		{
			if (ipIdx < this.omData.Count)
				return (string)this.omData[ipIdx];
			else
				return "";
		}

		//Returns the whole data buffer with "\n" separator
		string GetDataBuffer()
		{
			return "";
		}

		//frees all allocated memory
		void ClearDataBuffer()
		{
			this.omDataBuffer.Length = 0;
			this.omData.Clear();
		}

		private int ExtractTextLineFast(ArrayList opItems, string pcpLineText,string pcpSepa)
		{
			opItems.AddRange(pcpLineText.Split(pcpSepa.ToCharArray()));
			return opItems.Count;
		}

		//connects to the server in case of empty string 
		// the socket connects to the server specified in omServer
		private int ConnectToCeda(string opConnectionstring)
		{
			int blRet = 0;
			string	olOrigServer = "",olSvr1 = "",olSvr2 = "";
			bool blHas2Servers = false;

			olOrigServer = opConnectionstring;
			int ilTmpPos = olOrigServer.IndexOf('/');
			if (ilTmpPos >= 0)
			{
				olSvr1 = olOrigServer.Substring(0,ilTmpPos);
				++ilTmpPos;
				olSvr2 = olOrigServer.Substring(ilTmpPos);
				blHas2Servers = true;
			}
			else
			{
				olSvr1 = this.omServer;
			}

			this.omSocket = new System.Net.Sockets.Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);

			if (blHas2Servers == true)
			{
				System.Net.IPHostEntry olHostEntry = System.Net.Dns.Resolve(olSvr1);
				System.Net.IPAddress olAddress = olHostEntry.AddressList[0];
				int ilPort = int.Parse(omPort);
				System.Net.IPEndPoint olEndPoint1 = new System.Net.IPEndPoint(olAddress,ilPort);
				omSocket.Connect(olEndPoint1);
			}
			else
			{
				System.Net.IPHostEntry olHostEntry = System.Net.Dns.Resolve(olSvr1);
				System.Net.IPAddress olAddress = olHostEntry.AddressList[0];
				int ilPort = int.Parse(omPort);
				System.Net.IPEndPoint olEndPoint1 = new System.Net.IPEndPoint(olAddress,ilPort);
				omSocket.Connect(olEndPoint1);
			}

			bmIsConnected = true;

			return blRet;

		}

		// Close the established connection
		private void ResetConnection()
		{
			if (this.omSocket != null)
			{
				this.omSocket.Close();
				this.omSocket = null;
				this.bmIsConnected = false;
			}
		}

		//This method is responsible to extract the values behind a keyword
		// in the syntax "{=CMD=}" or "{=HOP=}"
		private int GetKeyItem(out string pcpResultBuff,out int plpResultSize,string pcpTextBuff, string pcpKeyWord, string pcpItemEnd, bool bpCopyData)
		{
			int llDataSize  = 0; 
			int ilDataBegin = -1; 
			int ilDataEnd   = -1; 
			pcpResultBuff   = "";

			ilDataBegin = pcpTextBuff.IndexOf(pcpKeyWord); 

			/* Search the keyword */ 
			if (ilDataBegin >= 0) 
			{ 
				/* Did we find it? Yes. */ 
				ilDataBegin += pcpKeyWord.Length; 
				/* Skip behind the keyword */ 
				ilDataEnd = pcpTextBuff.IndexOf(pcpItemEnd,ilDataBegin); 
				/* Search end of data */ 
				if (ilDataEnd < 0) 
				{ 
					/* End not found? */ 
					ilDataEnd = pcpTextBuff.Length; 
					/* Take the whole string */ 
				} /* end if */ 
				llDataSize = ilDataEnd - ilDataBegin; 
				/* Now calculate the length */ 
				if (bpCopyData == true) 
				{ 
					/* Shall we copy? */ 
					pcpResultBuff = pcpTextBuff.Substring(ilDataBegin,llDataSize); 
					/* Yes, strip out the data */ 
				} /* end if */ 
			} /* end if */ 
			if (bpCopyData == true) 
			{ 
				/* Allowed to set EOS? */ 
				//				pcpResultBuff[llDataSize] = 0x00; 
				/* Yes, terminate string */ 
			} /* end if */ 

			/* Pass the length back */ 
			plpResultSize = llDataSize; 

			/* Return the data's begin */ 
			return ilDataBegin; 
		}
		/// <summary>
		/// TODO
		/// </summary>
		/// <param name="reader"></param>
		public FastCedaConnection(IUfisComReader reader)
		{
			//
			// TODO: Add constructor logic here
			//
			this.reader = reader;	
			omSocket = null;
			bmIsConnected = false;
			omDataBuffer  = new StringBuilder();
			omSepa = "\n";
			imSendTimeout = 240;
			imReceiveTimeout = 120;
			omData = new ArrayList();
			omSimErr = string.Empty;
			imWindowsCodePage = 1252;

			IniFile	iniFile = new IniFile("c:\\ufis\\system\\ceda.ini");

			string tmpValue = iniFile.IniReadValue("GLOBAL","PACKETSIZE");
			if (tmpValue.Length == 0)
			{
				this.lmPacketSize = 0;
			}
			else
			{
				this.lmPacketSize = long.Parse(tmpValue);
			}

			tmpValue = iniFile.IniReadValue("GLOBAL","WINDOWSCODEPAGE");
			if (tmpValue.Length == 0)
			{
				this.imWindowsCodePage = 1252;
			}
			else
			{
				this.imWindowsCodePage = int.Parse(tmpValue);
			}

		}
	}
}
