using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using Ufis.Utils;
using Ufis.Data;
using System.Text;

namespace Ufis.Data
{
	/// <summary>
	/// The EventManager class provides a black box from where external 
	/// data changes are automatically incorporated into the application.
	/// The UFIS suite provides the BCProxy, BCComClient (ActiveX), BCComServer and MQSeries 
	/// in the future as potontial	Broadcast servers. To keep it transparent from 
	/// where the broadcasts are received. This class manages the connection to the external
	/// ATL servers and distributes the broadcasts into the <see cref="Ufis.Data"/> instances
	/// to decide what shall happen with the data change message.
	/// </summary>
	internal class EventManager : System.ComponentModel.Component
	{
		private enum EventTypes
		{
			None	= 0,
			BcProxy = 1,
			BcCom	= 2,
			BcMq	= 4
		};

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#region implementation
		private	BcProxyLib.BcPrxyClass			bcProxy = null;						// BcProxy object reference
		private	BCCOMSERVERLib.BcComAtlClass	bcCom = null;						// BcCom object reference
		private EventTypes						eventTypes = EventTypes.BcProxy;	// for compatibility reasons
		private	StringBuilder					bcData	= new StringBuilder();
		/// <summary>
		/// This event is sent for the delegate <see cref="BcEvent"/>.
		/// </summary>
		/// <remarks>none.</remarks>
		public	event BcEvent OnBcEvent;
		#endregion

		/// <summary>
		/// Delegate which will distribute the broadcast data to the <see cref="Ufis.Data"/> instances.
		/// It distributes all information contained in the BC message as single strings.
		/// </summary>
		/// <param name="pReqId">User ID</param>
		/// <param name="pDest1">TO DO</param>
		/// <param name="pDest2">TO DO</param>
		/// <param name="pCmd">Router command</param>
		/// <param name="pObject">Database table name</param>
		/// <param name="pSeq">TO DO</param>
		/// <param name="pTws">Application relevant field</param>
		/// <param name="pTwe">User,Wks,application</param>
		/// <param name="pSelection">SQL Where statement</param>
		/// <param name="pFields">Database table fields.</param>
		/// <param name="pData">Data</param>
		/// <param name="pBcNum">Broadcast number</param>
		/// <param name="attachment">Broadcast attachment an further information.</param>
		/// <param name="unused">For later usage.</param>
		/// <remarks>none.</remarks>
		public delegate void BcEvent(string pReqId, string pDest1, string pDest2,string pCmd, 
									 string pObject, string pSeq,string pTws, string pTwe, 
									 string pSelection, string pFields, string pData, 
									 string pBcNum,string attachment,string unused);
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="container">Because it is a component.</param>
		/// <remarks>none.</remarks>
		public EventManager(System.ComponentModel.IContainer container)
		{
			//
			// Required for Windows.Forms Class Composition Designer support
			//
			container.Add(this);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Default constructor. Reads from Cede.ini file the settings for the broadcast
		/// data source. The settings in ceda.ini are.
		/// The following broadcast sources are supported:<p></p>
		/// <TABLE id="Table1" cellSpacing="0" cellPadding="0" width="500" border="1" height="10">
		/// 	<TR>
		/// 		<th>
		/// 			Ceda.ini</th>
		/// 		<Th>
		/// 			Description</Th>
		/// 	</TR>
		/// 	<TR>
		/// 		<TD>BCPROXY</TD>
		/// 		<TD>BcProxy is BC source</TD>
		/// 	</TR>
		/// 	<tr>
		/// 		<td>BCMQ</td>
		/// 		<td>MQSeries is BC sourec</td>
		/// 	</tr>
		/// 	<tr>
		/// 		<td>BCCOM</td>
		/// 		<td>BcCom ActiveX control is BC Source</td>
		/// 	</tr>
		/// </TABLE>
		/// </summary>
		public EventManager()
		{
			//
			// Required for Windows.Forms Class Composition Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			Ufis.Utils.IniFile ini = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string bcEventTypes  = ini.IniReadValue("GLOBAL", "BCEVENTTYPE");

			if (bcEventTypes.Length == 0)
			{
				bcEventTypes = "BCPROXY";
				this.eventTypes |= EventTypes.BcProxy;
			}
			else
			{
				this.eventTypes = EventTypes.None;
			}

			if (bcEventTypes.IndexOf("BCMQ") >= 0)
			{
				this.eventTypes |= EventTypes.BcMq;
			}

			if (bcEventTypes.IndexOf("BCCOM") >= 0)
			{
				Process process = Process.GetCurrentProcess();

				this.eventTypes |= EventTypes.BcCom;
				this.bcCom = new BCCOMSERVERLib.BcComAtlClass();
				this.bcCom.SetPID(process.MainModule.ModuleName,process.Id.ToString());
				this.bcCom.OnBc +=new BCCOMSERVERLib._IBcComAtlEvents_OnBcEventHandler(bcCom_OnBc);
			}

			if (bcEventTypes.IndexOf("BCPROXY") >= 0)
			{
				this.eventTypes |= EventTypes.BcProxy;
				this.bcProxy = new BcProxyLib.BcPrxyClass();
				this.bcProxy.OnBcReceive += new BcProxyLib._IBcPrxyEvents_OnBcReceiveEventHandler(bcProxy_OnBcReceive);
			}

			if (bcEventTypes.IndexOf("ALL") >= 0)
			{
				this.eventTypes |= EventTypes.BcProxy;
				this.bcProxy = new BcProxyLib.BcPrxyClass();
				this.bcProxy.OnBcReceive += new BcProxyLib._IBcPrxyEvents_OnBcReceiveEventHandler(bcProxy_OnBcReceive);

				Process process = Process.GetCurrentProcess();

				this.eventTypes |= EventTypes.BcCom;
				this.bcCom = new BCCOMSERVERLib.BcComAtlClass();
				this.bcCom.SetPID(process.MainModule.ModuleName,process.Id.ToString());
				this.bcCom.OnBc +=new BCCOMSERVERLib._IBcComAtlEvents_OnBcEventHandler(bcCom_OnBc);
			}
			 

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (this.IsEventEnabled(EventTypes.BcCom))
				{
					this.bcCom.OnBc -=new BCCOMSERVERLib._IBcComAtlEvents_OnBcEventHandler(bcCom_OnBc);
					this.bcCom.StopBroadcasting();
				}

				if (this.IsEventEnabled(EventTypes.BcProxy))
				{
					if (this.bcProxy != null)
					{
						this.bcProxy.OnBcReceive -= new BcProxyLib._IBcPrxyEvents_OnBcReceiveEventHandler(bcProxy_OnBcReceive);
					}
				}

				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		/// <summary>
		/// Registers a table + command + indicator if own broadcast and the application
		/// at the broadcast ATL server for filtering purposes of broadcasts.
		/// <list type="bullet">
		///		<listheader>Commands</listheader>
		///		<item>IRT</item>
		///		<item>URT</item>
		///		<item>DRT</item>
		///		<item>JOF</item>
		///		<item> .... please inform yourself about the UFIS router commands.</item>
		/// </list>
		/// </summary>
		/// <param name="table">The application registers for this database table broadcasts</param>
		/// <param name="cmd"> and for this command</param>
		/// <param name="ownBc">it incorporates own initiated broadcasts</param>
		/// <param name="appl">The application to register</param>
		/// <example>
		/// Function example:
		/// <code>
		/// [C#]
		/// RegisterObject("AFT", "DRF", true, "FIPS");
		/// </code>
		/// In this case the Table "AFTTAB" with the command "DRF" is incorporated
		/// even if the broadcast was initiated by this application and the registered
		/// application for this case is <B>FIPS</B>.
		/// </example>
		public void RegisterObject(string table,string cmd,int ownBc,string appl)
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				this.bcProxy.RegisterObject(table,cmd,ownBc,appl);
			}

			if (IsEventEnabled(EventTypes.BcCom))
			{
				this.bcCom.SetFilter(table,cmd,ownBc,appl);
			}
		}
		/// <summary>
		/// Unregisters a combination of database table and a router command from the
		/// external ATL server, which is responsible to provide broadcasts.
		/// </summary>
		/// <param name="table">The database table.</param>
		/// <param name="cmd">The router command.</param>
		public void UnregisterObject(string table,string cmd)
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				this.bcProxy.UnregisterObject(table,cmd);
			}
		}
		/// <summary>
		/// Sets a filter range as criteria for broadcasts. An application might be interested
		/// in data for a certain time frame, let's assume from 01.01.2005 to 02.01.2005. 
		/// The application will be interested in data changes for the fields:
		/// <list type="bullet">
		///		<item>STOA for Scheduled arrival</item>
		///		<item>ETAI for estimated arrival</item>
		///		<item>ONBL for onblock</item>
		/// </list>
		/// The application will be interested in field changes explicitly for the time frame between
		/// 01.01.2005 and 02.01.2004. For this purpose the "fromValue" and "toValue" fields 
		/// must be set in the CEDA date and time format, e.g. "20050101000000" and "20050102000000"
		/// </summary>
		/// <param name="table">The database table, which contains the fields.</param>
		/// <param name="fromField">The field "From" as start of the range.</param>
		/// <param name="toField">The field "To" as end of the range.</param>
		/// <param name="fromValue">The "From" value.</param>
		/// <param name="toValue">The "To value"</param>
		/// <example>
		///		<code>
		///		[C#]
		///		SetFilterRange("AFT", "ONBL", "ONBL", "20050101000000", "20050102000000");
		///		SetFilterRange("AFT", "ETAI", "ETAI", "20050101000000", "20050102000000");
		///		SetFilterRange("AFT", "STOA", "STOA", "20050101000000", "20050102000000");
		///		</code>
		/// All the fields will be combined by an "AND" statement and if all criteria fit the
		/// broadcast data, the broadcast will be taken into account. Otherwise the external
		/// ATL broadcast server will filter out the braodcast. 
		/// </example>
		public void SetFilterRange(string table,string fromField,string toField,string fromValue,string toValue)
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				this.bcProxy.SetFilterRange(table,fromField,toField,fromValue,toValue);
			}
		}
		/// <summary>
		/// The BcProxy provides a functionality for internal broadcast spooling. If the
		/// spooling is set, the application will not get any broadcast until the spooling
		/// is switched off. After switching off, all queued broadcasts are sent immediately
		/// to the application.
		/// </summary>
		public void SetSpoolOn()
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				this.bcProxy.SetSpoolOn();
			}

			if (IsEventEnabled(EventTypes.BcCom))
			{
				this.bcCom.StopBroadcasting();
			}
		}
		/// <summary>
		/// The BcProxy provides a functionality for internal broadcast spooling. If the
		/// spooling is set, the application will not get any broadcast until the spooling
		/// is switched off. After switching off, all queued broadcasts are sent immediately
		/// to the application.
		/// </summary>
		public void SetSpoolOff()
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				this.bcProxy.SetSpoolOff();
			}

			if (IsEventEnabled(EventTypes.BcCom))
			{
				this.bcCom.StartBroadcasting();
				this.bcCom.TransmitFilter();
			}
		}


		private bool IsEventEnabled(EventTypes type)
		{
			if ((this.eventTypes & type) == type)
				return true;
			else
				return false;
		}

		private void bcProxy_OnBcReceive(string ReqId, string Dest1, string Dest2, string Cmd, string Object, string Seq, string Tws, string Twe, string Selection, string Fields, string Data, string BcNum)
		{
			if (this.OnBcEvent != null)
			{
				this.OnBcEvent(ReqId,Dest1,Dest2,Cmd,Object,Seq,Tws,Twe,Selection,Fields,Data,BcNum,string.Empty,string.Empty);
			}
		}

		private void bcCom_OnBc(int size, int currPackage, int totalPackages, string strData)
		{
			if (currPackage == 1)
			{
				bcData.Length = 0;
				bcData.Append(strData);
			}
			else
			{
				bcData.Append(strData);
			}

			if (currPackage == totalPackages)
			{
				if (this.OnBcEvent != null)
				{
					string ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum,Attach;
					ReqId = UT.GetKeyItem(bcData.ToString(), "{=USR=}", "{="); 
					Dest1 = UT.GetKeyItem(bcData.ToString(), "{=USR=}", "{="); 
					Dest2 = UT.GetKeyItem(bcData.ToString(), "{=WKS=}", "{="); 
					if (Dest2.Length > 0)
					{
						ReqId = Dest2;
					}

					Cmd = UT.GetKeyItem(bcData.ToString(), "{=CMD=}", "{="); 
					Object = UT.GetKeyItem(bcData.ToString(), "{=TBL=}", "{="); 
					Seq = UT.GetKeyItem(bcData.ToString(), "{=XXX=}", "{="); 
					Tws = UT.GetKeyItem(bcData.ToString(), "{=TWS=}", "{="); 
					Twe = UT.GetKeyItem(bcData.ToString(), "{=TWE=}", "{="); 
					Selection = UT.GetKeyItem(bcData.ToString(), "{=WHE=}", "{="); 
					Fields = UT.GetKeyItem(bcData.ToString(), "{=FLD=}", "{="); 
					Data = UT.GetKeyItem(bcData.ToString(), "{=DAT=}", "{="); 
					BcNum = UT.GetKeyItem(bcData.ToString(), "{=BCNUM=}", "{="); 
					Attach = UT.GetKeyItem(bcData.ToString(), "{=ATTACH=}", "{="); 

					this.OnBcEvent(ReqId,Dest1,Dest2,Cmd,Object,Seq,Tws,Twe,Selection,Fields,Data,BcNum,Attach,string.Empty);
				}
			}
		}
	}
}
