using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using Ufis.Utils;
using Ufis.Data;

namespace TestAvailability
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		#region private variable
		/// <summary>
		/// Used when 'All' condition is not enabled in the GUI. Filtering the block information
		/// for specific resource
		/// </summary>
		private string strBLKWhere = "where BURN = '@@URNO'";
		/// <summary>
		/// Used for loading the EQU resources
		/// </summary>
		private string strEQUWhere = "where TABN like 'EQU%'";
		/// <summary>
		/// Instance of the database
		/// </summary>
		IDatabase myDB;
		/// <summary>
		/// Block table - BLKTAB
		/// </summary>
		ITable tblDataBLK;
		/// <summary>
		/// Resource table - PSTTAB, CICTAB, EQUTAB
		/// </summary>
		ITable tblDataRes;
		/// <summary>
		/// Resource availability table - No database table - Uses PSTTAB as the basis
		/// </summary>
		ITable tblDataResAvailable;
		/// <summary>
		/// Resource blocking table - No database table - Uses BLKTAB as the basis.
		/// </summary>
		ITable tblDataResBlock;
		/// <summary>
		/// Resource availability table - No database table - Uses PSTTAB as the basis
		/// </summary>
		ITable tempTblDataRes;
		/// <summary>
		/// Used when 'All' condition is not enabled in the GUI. Row instance used 
		/// for filtering the specific resource information. 
		/// </summary>
		IRow tempIrow;
		#endregion private variable

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFM;
		private System.Windows.Forms.DateTimePicker dtTO;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtUserName;
		private System.Windows.Forms.TextBox txtHopo;
		private System.Windows.Forms.TextBox txtTableExt;
		private System.Windows.Forms.TextBox txtServer;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label lblCon;
		private System.Windows.Forms.Label lblBlkCnt;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ComboBox cbResTab;
		private AxTABLib.AxTAB tabResource;
		private System.Windows.Forms.Button btnLoad;
		private AxTABLib.AxTAB tabBlock;
		private AxTABLib.AxTAB tabResAvail;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private AxTABLib.AxTAB tabResBlk;
		private System.Windows.Forms.CheckBox cbAll;
		private System.Windows.Forms.CheckBox cbStatus;
		private System.Windows.Forms.Label lblResCnt;
		private System.Windows.Forms.Label lblResAvail;
		private System.Windows.Forms.Label lblResBlock;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region init
		/// <summary>
		/// Initialises the screen components.
		/// </summary>
		private void init() 
		{
//			Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
//			UT.ServerName = myIni.IniReadValue("GLOBAL", "HOSTNAME");
//			UT.Hopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");

			DateTime olFrom, olTo;
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);

			dtFM.Value = olFrom;
			dtTO.Value = olTo;
			dtFM.CustomFormat = "dd.MM.yyyy - HH:mm:ss";
			dtTO.CustomFormat = "dd.MM.yyyy - HH:mm:ss";

			tabBlock.ResetContent();
			tabBlock.ShowHorzScroller(true);
			tabBlock.EnableHeaderSizing(true);
			tabBlock.SetTabFontBold(true);
			tabBlock.LifeStyle = true;
			tabBlock.LineHeight = 16;
			tabBlock.FontName = "Arial";
			tabBlock.FontSize = 14;
			tabBlock.HeaderFontSize = 14;
			tabBlock.AutoSizeByHeader = true;
			tabBlock.HeaderString = "TAB,Res URNO, BLK FM, BLK TO,DAYS";
			tabBlock.LogicalFieldList = "TAB,BURN,NAFR,NATO,DAYS";
			tabBlock.HeaderLengthString = "40,60,90,90,40";

			tabResource.ResetContent();
			tabResource.ShowHorzScroller(true);
			tabResource.EnableHeaderSizing(true);
			tabResource.SetTabFontBold(true);
			tabResource.LifeStyle = true;
			tabResource.LineHeight = 16;
			tabResource.FontName = "Arial";
			tabResource.FontSize = 14;
			tabResource.HeaderFontSize = 14;
			tabResource.AutoSizeByHeader = true;
			tabResource.HeaderString = "Name,URNO,VAFR,VATO";
			tabResource.LogicalFieldList = "NAME,URNO,VAFR,VATO";
			tabResource.HeaderLengthString = "40,60,90,90";

			tabResAvail.ResetContent();
			tabResAvail.ShowHorzScroller(true);
			tabResAvail.EnableHeaderSizing(true);
			tabResAvail.SetTabFontBold(true);
			tabResAvail.LifeStyle = true;
			tabResAvail.LineHeight = 16;
			tabResAvail.FontName = "Arial";
			tabResAvail.FontSize = 14;
			tabResAvail.HeaderFontSize = 14;
			tabResAvail.AutoSizeByHeader = true;
			tabResAvail.HeaderString = "URNO,VAFR,VATO,NAME";
			tabResAvail.LogicalFieldList = "NAME,URNO,VAFR,VATO";
			tabResAvail.HeaderLengthString = "50,60,90,90";

			tabResBlk.ResetContent();
			tabResBlk.ShowHorzScroller(true);
			tabResBlk.EnableHeaderSizing(true);
			tabResBlk.SetTabFontBold(true);
			tabResBlk.LifeStyle = true;
			tabResBlk.LineHeight = 16;
			tabResBlk.FontName = "Arial";
			tabResBlk.FontSize = 14;
			tabResBlk.HeaderFontSize = 14;
			tabResBlk.AutoSizeByHeader = true;
			tabResBlk.HeaderString = "URNO,NAFR,NATO,NAME";
			tabResBlk.LogicalFieldList = "NAME,URNO,NAFR,NATO";
			tabResBlk.HeaderLengthString = "50,60,90,90";
		}
		/// <summary>
		/// This clears the contents of the local variables and also resets the contents 
		/// inside the tables used for displaying the results.
		/// </summary>
		private void ClearFields() 
		{
			if (tblDataBLK != null)
				this.tblDataBLK.Clear();
			if (this.tblDataRes != null)
				this.tblDataRes.Clear();
			if (this.tblDataResAvailable != null)
				this.tblDataResAvailable.Clear();
			if (this.tblDataResBlock != null)
				this.tblDataResBlock.Clear();

			this.lblBlkCnt.Text = ":";
			this.lblResCnt.Text = ":";
			this.lblResAvail.Text = ":";
			this.lblResBlock.Text = ":";

			this.tabBlock.ResetContent();
			this.tabBlock.Refresh();
			this.tabResAvail.ResetContent();
			this.tabResAvail.Refresh();
			this.tabResBlk.ResetContent();
			this.tabResBlk.Refresh();
			this.tabResource.ResetContent();
			this.tabResource.Refresh();
		}
		#endregion init
		#region Handling BLK Data
		/// <summary>
		/// Loads the block data for the resource specified by urno from the database. If the urno 
		/// is "", it loads blocking information for all the resource.
		/// </summary>
		/// <param name="urno">Resource for which blocking information is to be loaded</param>
		private void LoadBlkData(string urno)
		{
			string strTmpQry = "";
			myDB.Unbind("BLK");
			tblDataBLK = myDB.Bind("BLK","BLK","BURN,NAFR,NATO,NAME,DAYS,TIFR,TITO","10,14,14,3,7,4,4","BURN,NAFR,NATO,TABN,DAYS,TIFR,TITO");
			tblDataBLK.Clear();
			tblDataBLK.TimeFields = "NAFR,NATO";
			tblDataBLK.TimeFieldsInitiallyInUtc = true;

			if (!urno.Equals(""))
			{
				strTmpQry = strBLKWhere; 
				strTmpQry = strTmpQry.Replace("@@URNO",urno);
			}
			tblDataBLK.Load(strTmpQry);
		}
		/// <summary>
		/// Populates the Block display section for the resource
		/// </summary>
		private void PoulateBlkData() 
		{
			tabBlock.ResetContent();
			if (tblDataBLK == null)
				return;
			StringBuilder strBuf = new StringBuilder();
			for (int i=0; i<tblDataBLK.Count; i++)
			{
				strBuf.Append(tblDataBLK[i]["NAME"]).Append(",");
				strBuf.Append(tblDataBLK[i]["BURN"]).Append(",");
				strBuf.Append(tblDataBLK[i]["NAFR"]).Append(",");
				strBuf.Append(tblDataBLK[i]["NATO"]).Append(",");
				strBuf.Append(tblDataBLK[i]["DAYS"]).Append("\n");
			}
			tabBlock.InsertBuffer(strBuf.ToString(),"\n");
			tabBlock.Sort("2",true,true);
			tabBlock.Refresh();
			this.lblBlkCnt.Text = ":"+tblDataBLK.Count.ToString();
		}
		#endregion Handling BLK Data
		#region Handling Resource Data
		/// <summary>
		/// Loads the resource validities corresponding to the type specified by tabName.
		/// </summary>
		/// <param name="tabName">Resource type</param>
		private void LoadResource(string tabName)
		{
			if (tabName.Equals("CIC"))
			{
				myDB.Unbind("CIC");
				tblDataRes= myDB.Bind("CIC","CIC","NAME,URNO,VAFR,VATO","5,10,14,14","CNAM,URNO,VAFR,VATO");
				tblDataRes.Clear();
				tblDataRes.TimeFields = "VAFR,VATO";
				tblDataRes.TimeFieldsInitiallyInUtc = true;
				tblDataRes.Load("");
				myDB.Unbind("CIC");
			} 
			else if (tabName.Equals("PST")) 
			{
				myDB.Unbind("PST");
				tblDataRes= myDB.Bind("PST","PST","NAME,URNO,VAFR,VATO","5,10,14,14","PNAM,URNO,VAFR,VATO");
				tblDataRes.Clear();
				tblDataRes.TimeFields = "VAFR,VATO";
				tblDataRes.TimeFieldsInitiallyInUtc = true;
				tblDataRes.Load("");
				myDB.Unbind("PST");
			}
			else if (tabName.Equals("EQU")) 
			{
				myDB.Unbind("EQU");
				tblDataRes= myDB.Bind("EQU","VAL","NAME,URNO,VAFR,VATO","5,10,14,14","FREQ,UVAL,VAFR,VATO");
				tblDataRes.Clear();
				tblDataRes.TimeFields = "VAFR,VATO";
				tblDataRes.TimeFieldsInitiallyInUtc = true;
				tblDataRes.Load(strEQUWhere);
				myDB.Unbind("EQU");
			}
		}
		/// <summary>
		/// Populates the Resource Validity display section 
		/// </summary>
		private void PopulateResource() 
		{
			tabResource.ResetContent();
			if (tblDataRes == null)
				return;
			StringBuilder strBuf = new StringBuilder();
			for (int i=0; i<tblDataRes.Count; i++)
			{
				strBuf.Append(tblDataRes[i]["NAME"]).Append(",");
				strBuf.Append(tblDataRes[i]["URNO"]).Append(",");
				strBuf.Append(tblDataRes[i]["VAFR"]).Append(",");
				strBuf.Append(tblDataRes[i]["VATO"]).Append("\n");
			}
			tabResource.InsertBuffer(strBuf.ToString(),"\n");
			tabResource.Sort("2",true,true);
			tabResource.Refresh();
			this.lblResCnt.Text = ":"+tblDataRes.Count.ToString();
		}
		/// <summary>
		/// Populates the Resource Availability display section
		/// </summary>
		private void PoulateResourceAvailData() 
		{
			tabResAvail.ResetContent();
			if (tblDataResAvailable == null)
				return;
			StringBuilder strBuf = new StringBuilder();
			for (int i=0; i<tblDataResAvailable.Count; i++)
			{
				strBuf.Append(tblDataResAvailable[i]["URNO"]).Append(",");
				strBuf.Append(tblDataResAvailable[i]["VAFR"]).Append(",");
				strBuf.Append(tblDataResAvailable[i]["VATO"]).Append(",");
				strBuf.Append(tblDataResAvailable[i]["NAME"]).Append("\n");
			}
			tabResAvail.InsertBuffer(strBuf.ToString(),"\n");
			tabResAvail.Sort("1",true,true);
			tabResAvail.Refresh();
			this.lblResAvail.Text = ":"+this.tblDataResAvailable.Count.ToString();
		}
		/// <summary>
		/// Populates Resource Block diplay section
		/// </summary>
		private void PoulateResourceBlkData() 
		{
			tabResBlk.ResetContent();
			if (tblDataResBlock == null)
				return;
			StringBuilder strBuf = new StringBuilder();
			for (int i=0; i<tblDataResBlock.Count; i++)
			{
				strBuf.Append(tblDataResBlock[i]["URNO"]).Append(",");
				strBuf.Append(tblDataResBlock[i]["NAFR"]).Append(",");
				strBuf.Append(tblDataResBlock[i]["NATO"]).Append(",");
				strBuf.Append(tblDataResBlock[i]["NAME"]).Append("\n");
			}
			tabResBlk.InsertBuffer(strBuf.ToString(),"\n");
			tabResBlk.Sort("1",true,true);
			tabResBlk.Refresh();
			this.lblResBlock.Text = ":"+this.tblDataResBlock.Count.ToString();
		}
		#endregion Handling Resource Data
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnLoad = new System.Windows.Forms.Button();
			this.dtFM = new System.Windows.Forms.DateTimePicker();
			this.dtTO = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.tabBlock = new AxTABLib.AxTAB();
			this.tabResource = new AxTABLib.AxTAB();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.txtUserName = new System.Windows.Forms.TextBox();
			this.txtHopo = new System.Windows.Forms.TextBox();
			this.txtTableExt = new System.Windows.Forms.TextBox();
			this.txtServer = new System.Windows.Forms.TextBox();
			this.btnConnect = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.lblCon = new System.Windows.Forms.Label();
			this.lblBlkCnt = new System.Windows.Forms.Label();
			this.cbResTab = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.tabResAvail = new AxTABLib.AxTAB();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.tabResBlk = new AxTABLib.AxTAB();
			this.cbAll = new System.Windows.Forms.CheckBox();
			this.cbStatus = new System.Windows.Forms.CheckBox();
			this.lblResCnt = new System.Windows.Forms.Label();
			this.lblResAvail = new System.Windows.Forms.Label();
			this.lblResBlock = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.tabBlock)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResAvail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResBlk)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(128, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "From Date";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(360, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(48, 23);
			this.label2.TabIndex = 3;
			this.label2.Text = "To Date";
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(560, 32);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.TabIndex = 8;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btn_Click);
			// 
			// dtFM
			// 
			this.dtFM.CustomFormat = "";
			this.dtFM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFM.Location = new System.Drawing.Point(200, 72);
			this.dtFM.Name = "dtFM";
			this.dtFM.Size = new System.Drawing.Size(136, 20);
			this.dtFM.TabIndex = 9;
			// 
			// dtTO
			// 
			this.dtTO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTO.Location = new System.Drawing.Point(416, 72);
			this.dtTO.Name = "dtTO";
			this.dtTO.Size = new System.Drawing.Size(136, 20);
			this.dtTO.TabIndex = 10;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 72);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(88, 23);
			this.label5.TabIndex = 13;
			this.label5.Text = "Ref Time frame";
			// 
			// tabBlock
			// 
			this.tabBlock.Location = new System.Drawing.Point(344, 128);
			this.tabBlock.Name = "tabBlock";
			this.tabBlock.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabBlock.OcxState")));
			this.tabBlock.Size = new System.Drawing.Size(328, 232);
			this.tabBlock.TabIndex = 14;
			// 
			// tabResource
			// 
			this.tabResource.Location = new System.Drawing.Point(32, 128);
			this.tabResource.Name = "tabResource";
			this.tabResource.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResource.OcxState")));
			this.tabResource.Size = new System.Drawing.Size(296, 232);
			this.tabResource.TabIndex = 15;
			this.tabResource.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.tabResource_Click);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(344, 104);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(104, 23);
			this.label6.TabIndex = 16;
			this.label6.Text = "Blocked times";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(32, 104);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(104, 23);
			this.label7.TabIndex = 17;
			this.label7.Text = "Resource validity";
			// 
			// txtUserName
			// 
			this.txtUserName.Location = new System.Drawing.Point(72, 32);
			this.txtUserName.Name = "txtUserName";
			this.txtUserName.Size = new System.Drawing.Size(48, 20);
			this.txtUserName.TabIndex = 21;
			this.txtUserName.Text = "SISL1";
			// 
			// txtHopo
			// 
			this.txtHopo.Location = new System.Drawing.Point(128, 32);
			this.txtHopo.Name = "txtHopo";
			this.txtHopo.Size = new System.Drawing.Size(48, 20);
			this.txtHopo.TabIndex = 20;
			this.txtHopo.Text = "SIN";
			// 
			// txtTableExt
			// 
			this.txtTableExt.Location = new System.Drawing.Point(184, 32);
			this.txtTableExt.Name = "txtTableExt";
			this.txtTableExt.Size = new System.Drawing.Size(48, 20);
			this.txtTableExt.TabIndex = 19;
			this.txtTableExt.Text = "TAB";
			// 
			// txtServer
			// 
			this.txtServer.Location = new System.Drawing.Point(16, 32);
			this.txtServer.Name = "txtServer";
			this.txtServer.Size = new System.Drawing.Size(48, 20);
			this.txtServer.TabIndex = 18;
			this.txtServer.Text = "SIN1";
			// 
			// btnConnect
			// 
			this.btnConnect.Location = new System.Drawing.Point(240, 32);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.TabIndex = 22;
			this.btnConnect.Text = "Connect";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(16, 8);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(24, 23);
			this.label8.TabIndex = 23;
			this.label8.Text = "Ser";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(72, 8);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(24, 23);
			this.label9.TabIndex = 24;
			this.label9.Text = "Usr";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(128, 8);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(40, 23);
			this.label10.TabIndex = 25;
			this.label10.Text = "Hopo";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(184, 8);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(40, 23);
			this.label11.TabIndex = 26;
			this.label11.Text = "Tbl Ext";
			// 
			// lblCon
			// 
			this.lblCon.BackColor = System.Drawing.Color.Red;
			this.lblCon.Location = new System.Drawing.Point(328, 32);
			this.lblCon.Name = "lblCon";
			this.lblCon.Size = new System.Drawing.Size(24, 23);
			this.lblCon.TabIndex = 27;
			// 
			// lblBlkCnt
			// 
			this.lblBlkCnt.Location = new System.Drawing.Point(424, 104);
			this.lblBlkCnt.Name = "lblBlkCnt";
			this.lblBlkCnt.Size = new System.Drawing.Size(104, 23);
			this.lblBlkCnt.TabIndex = 28;
			this.lblBlkCnt.Text = ":";
			// 
			// cbResTab
			// 
			this.cbResTab.Items.AddRange(new object[] {
														  "PST",
														  "EQU",
														  "CIC"});
			this.cbResTab.Location = new System.Drawing.Point(432, 32);
			this.cbResTab.Name = "cbResTab";
			this.cbResTab.Size = new System.Drawing.Size(121, 21);
			this.cbResTab.TabIndex = 29;
			this.cbResTab.Text = "PST";
			this.cbResTab.SelectedIndexChanged += new System.EventHandler(this.cbResTab_Changed);
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(360, 32);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(72, 23);
			this.label12.TabIndex = 30;
			this.label12.Text = "Res Table";
			// 
			// tabResAvail
			// 
			this.tabResAvail.Location = new System.Drawing.Point(32, 400);
			this.tabResAvail.Name = "tabResAvail";
			this.tabResAvail.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResAvail.OcxState")));
			this.tabResAvail.Size = new System.Drawing.Size(296, 152);
			this.tabResAvail.TabIndex = 31;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(32, 368);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(112, 23);
			this.label13.TabIndex = 32;
			this.label13.Text = "Resource availability";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(344, 368);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(112, 23);
			this.label14.TabIndex = 34;
			this.label14.Text = "Resource Blocked";
			// 
			// tabResBlk
			// 
			this.tabResBlk.Location = new System.Drawing.Point(344, 400);
			this.tabResBlk.Name = "tabResBlk";
			this.tabResBlk.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResBlk.OcxState")));
			this.tabResBlk.Size = new System.Drawing.Size(328, 152);
			this.tabResBlk.TabIndex = 33;
			// 
			// cbAll
			// 
			this.cbAll.Location = new System.Drawing.Point(560, 72);
			this.cbAll.Name = "cbAll";
			this.cbAll.Size = new System.Drawing.Size(40, 24);
			this.cbAll.TabIndex = 35;
			this.cbAll.Text = "All";
			// 
			// cbStatus
			// 
			this.cbStatus.Location = new System.Drawing.Point(560, 96);
			this.cbStatus.Name = "cbStatus";
			this.cbStatus.Size = new System.Drawing.Size(96, 24);
			this.cbStatus.TabIndex = 36;
			this.cbStatus.Text = "Only Status";
			// 
			// lblResCnt
			// 
			this.lblResCnt.Location = new System.Drawing.Point(128, 104);
			this.lblResCnt.Name = "lblResCnt";
			this.lblResCnt.Size = new System.Drawing.Size(104, 23);
			this.lblResCnt.TabIndex = 37;
			this.lblResCnt.Text = ":";
			// 
			// lblResAvail
			// 
			this.lblResAvail.Location = new System.Drawing.Point(136, 368);
			this.lblResAvail.Name = "lblResAvail";
			this.lblResAvail.Size = new System.Drawing.Size(104, 23);
			this.lblResAvail.TabIndex = 38;
			this.lblResAvail.Text = ":";
			// 
			// lblResBlock
			// 
			this.lblResBlock.Location = new System.Drawing.Point(448, 368);
			this.lblResBlock.Name = "lblResBlock";
			this.lblResBlock.Size = new System.Drawing.Size(104, 23);
			this.lblResBlock.TabIndex = 39;
			this.lblResBlock.Text = ":";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(712, 566);
			this.Controls.Add(this.lblResBlock);
			this.Controls.Add(this.lblResAvail);
			this.Controls.Add(this.lblResCnt);
			this.Controls.Add(this.cbStatus);
			this.Controls.Add(this.cbAll);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.tabResBlk);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.tabResAvail);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.cbResTab);
			this.Controls.Add(this.lblBlkCnt);
			this.Controls.Add(this.lblCon);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.txtUserName);
			this.Controls.Add(this.txtHopo);
			this.Controls.Add(this.txtTableExt);
			this.Controls.Add(this.txtServer);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.tabResource);
			this.Controls.Add(this.tabBlock);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.dtTO);
			this.Controls.Add(this.dtFM);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Name = "Form1";
			this.Text = "Resource Availability / Block Tester";
			this.Load += new System.EventHandler(this.frmLoad);
			((System.ComponentModel.ISupportInitialize)(this.tabBlock)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResAvail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabResBlk)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		/// <summary>
		/// Called when Load button is clicked. Processing is done only if the database connection exist.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btn_Click(object sender, System.EventArgs e)
		{
			if (this.myDB != null && this.myDB.Connected)
			{
				string tabName = cbResTab.SelectedItem.ToString();
				ClearFields();
				if (tabName.Length > 0) 
				{
					LoadResource(tabName);
					PopulateResource();
				}
			}
			else
				MessageBox.Show(this,"Not connected to database");
		}
		/// <summary>
		/// Called when the form is loaded
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmLoad(object sender, System.EventArgs e)
		{
			init();
		}
		/// <summary>
		/// Database connection established
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			if (myDB == null) 
			{
				myDB = UT.GetMemDB();
				bool result = myDB.Connect(txtServer.Text,txtUserName.Text,txtHopo.Text,txtTableExt.Text,"TestContainer");
				if (result)
					this.lblCon.BackColor = System.Drawing.Color.Green;
				else
					this.lblCon.BackColor = System.Drawing.Color.Red;
			} 
			else 
			{
				MessageBox.Show(this,"Already Connected ... ");
			}
		}
		/// <summary>
		/// Fired when the user clicks on a row in the Resource Validity display 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabResource_Click(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			int curItem = this.tabResource.GetCurrentSelected();
			string urno = this.tabResource.GetFieldValue(curItem,"URNO");
			bool status = false;

			myDB.Unbind("RES_BLOCK");
			tblDataResBlock = myDB.Bind("RES_BLOCK","BLK","URNO,NAFR,NATO,NAME","10,14,14,5","URNO,NAFR,NATO,PNAM");
			tblDataResBlock.Clear();

			myDB.Unbind("RES_AVAIL");
			tblDataResAvailable = myDB.Bind("RES_AVAIL","PST","URNO,VAFR,VATO,NAME","10,14,14","URNO,VAFR,VATO,PNAM");
			tblDataResAvailable.Clear();

			myDB.Unbind("TEMP_RES");
			tempTblDataRes = myDB.Bind("TEMP_RES","PST","URNO,VAFR,VATO,NAME","10,14,14,5","URNO,VAFR,VATO,PNAM");
			tempTblDataRes.Clear();

			if (!cbStatus.Checked)
			{
				if (cbAll.Checked) 
				{
					LoadBlkData("");
					PoulateBlkData();
					UT.GetAvailability(ref tblDataRes,"VAFR","VATO","NAME",ref tblDataBLK,"NAFR","NATO",dtFM.Value,dtTO.Value,ref tblDataResAvailable,ref tblDataResBlock);
				} 
				else 
				{
					if (!urno.Equals(""))
					{
						LoadBlkData(urno);
						PoulateBlkData();

						myDB.Unbind("RES_AVAIL");
						tblDataResAvailable = myDB.Bind("RES_AVAIL","PST","URNO,VAFR,VATO,NAME","10,14,14,5","URNO,VAFR,VATO,PNAM");
						for (int k=0; k<tblDataRes.Count; k++)
						{
							if (urno.Equals(tblDataRes[k]["URNO"])) 
							{
								tempIrow = tempTblDataRes.CreateEmptyRow();
								tempIrow["URNO"] = tblDataRes[k]["URNO"];
								tempIrow["VAFR"] = tblDataRes[k]["VAFR"];
								tempIrow["VATO"] = tblDataRes[k]["VATO"];
								tempIrow["NAME"] = tblDataRes[k]["NAME"];
								tempIrow.Status = State.Created;
								tempTblDataRes.Add(tempIrow);
							}
						}
						UT.GetAvailability(ref tempTblDataRes,"VAFR","VATO","NAME",ref tblDataBLK,"NAFR","NATO",dtFM.Value,dtTO.Value,ref tblDataResAvailable,ref tblDataResBlock);
					}
				} 
			}
			else
			{
				LoadBlkData(urno);
				PoulateBlkData();
				for (int k=0; k<tblDataRes.Count; k++)
				{
					if (urno.Equals(tblDataRes[k]["URNO"])) 
					{
						tempIrow = tempTblDataRes.CreateEmptyRow();
						tempIrow["URNO"] = tblDataRes[k]["URNO"];
						tempIrow["VAFR"] = tblDataRes[k]["VAFR"];
						tempIrow["VATO"] = tblDataRes[k]["VATO"];
						tempIrow["NAME"] = tblDataRes[k]["NAME"];
						tempIrow.Status = State.Created;
						tempTblDataRes.Add(tempIrow);
					}
				}
				status = UT.IsAvailable(ref tempTblDataRes,"VAFR","VATO",ref tblDataBLK,"NAFR","NATO",dtFM.Value,dtTO.Value);
				if (status)
					MessageBox.Show(this,"Available");
				else
					MessageBox.Show(this,"Blocked");
			}
			PoulateResourceAvailData();
			PoulateResourceBlkData();
		}

		private void cbResTab_Changed(object sender, System.EventArgs e)
		{
			this.ClearFields();
		}
	}
}
