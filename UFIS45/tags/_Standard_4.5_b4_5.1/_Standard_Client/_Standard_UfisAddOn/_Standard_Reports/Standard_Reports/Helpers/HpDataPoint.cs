﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Standard_Reports.Helpers
{
    public class HpDataPoint
    {

        public static HpDataPoint CreateBarDataPoint(string argument, double value1, double value2, double value3, double value4)
        {
            return new HpDataPoint(argument, double.NaN, value1, value2, value3, value4, double.NaN);
        }

        readonly string argument;
        readonly double value;
        readonly double value1;
        readonly double value2;
        readonly double value3;
        readonly double value4;
        readonly double weight;

        public string Argument { get { return argument; } }
        public double Value { get { return value; } }
        public double Value1 { get { return value1; } }
        public double Value2 { get { return value2; } }
        public double Value3 { get { return value3; } }
        public double Value4 { get { return value4; } }
        public double Weight { get { return weight; } }
        public double LowValue { get { return value1; } }
        public double HighValue { get { return value2; } }
        public double OpenValue { get { return value3; } }
        public double CloseValue { get { return value4; } }

        HpDataPoint(string argument, double value, double value1, double value2, double value3, double value4, double weight)
        {
            this.argument = argument;
            this.value = value;
            this.value1 = value1;
            this.value2 = value2;
            this.value3 = value3;
            this.value4 = value4;
            this.weight = weight;
        }
    }
}
