﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpf.Core;
using System.Windows.Controls;

namespace Standard_Reports.Helpers
{
    public class HpRowDetailContainerControl : ContentControl
    {
        public HpRowDetailContainerControl()
        {
            this.SetDefaultStyleKey(typeof(HpRowDetailContainerControl));
        }
    }
}
