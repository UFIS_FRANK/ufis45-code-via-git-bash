﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using Ufis.Security;

namespace Standard_Reports.Helpers
{
    class HpUser
    {
        public static EntUser ActiveUser { get; set; }
        public static UserPrivileges Privileges { get; set; }
        public static bool DirectAccess { get; set; }
        public static string TLocalUTC { get; set; }
    }    
}
