﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.ComponentModel;

namespace Standard_Reports.Helpers
{
    public class HpParameters : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyname)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
        }

        private string _myParameter = string.Empty;
        public string MyParameter
        {
            get { return _myParameter; }
            set
            {
                _myParameter = value;
                OnPropertyChanged("Parameter");
            }
        }
        private string _ReportFooterString = string.Empty;
        public string ReportFooterString
        {
            get
            {
                return _ReportFooterString;
            }
            set
            {
                _ReportFooterString = value;
                OnPropertyChanged("ReportFooterString");
            }
        }
        private string _GeneratedBy = string.Empty;
        public string GeneratedBy
        {
            get
            {
                return _GeneratedBy;
            }
            set
            {
                _GeneratedBy = value;
                OnPropertyChanged("GeneratedBy");
            }
        }
    }
}
