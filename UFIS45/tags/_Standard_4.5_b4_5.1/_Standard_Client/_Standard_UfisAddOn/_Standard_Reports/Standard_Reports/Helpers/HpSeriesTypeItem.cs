﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Standard_Reports.Helpers 
{
    class HpSeriesTypeItem
    { 
            readonly Type diagramType;
            readonly Type seriesType;
            readonly string seriesName;
            readonly int seriesCount;

            public Type DiagramType { get { return diagramType; } }
            public Type SeriesType { get { return seriesType; } }
            public int SeriesCount { get { return seriesCount; } }

            public HpSeriesTypeItem(Type diagramType, Type seriesType, string seriesName) : this(diagramType, seriesType, seriesName, 1) { }
            public HpSeriesTypeItem(Type diagramType, Type seriesType, string seriesName, int seriesCount)
            {
                this.diagramType = diagramType;
                this.seriesType = seriesType;
                this.seriesName = seriesName;
                this.seriesCount = seriesCount;
            }
            public override string ToString()
            {
                return seriesName;
            }        
    }
}
