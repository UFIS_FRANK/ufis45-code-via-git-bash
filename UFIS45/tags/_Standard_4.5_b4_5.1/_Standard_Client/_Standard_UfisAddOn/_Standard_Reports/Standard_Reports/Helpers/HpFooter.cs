﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace Standard_Reports.Helpers
{
    public class HpFooter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyname)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
        }

        private string _myParameter = string.Empty;
        public string MyParameter
        {
            get { return _myParameter; }
            set
            {
                _myParameter = value;
                OnPropertyChanged("Parameter");
            }
        }
    }
}
