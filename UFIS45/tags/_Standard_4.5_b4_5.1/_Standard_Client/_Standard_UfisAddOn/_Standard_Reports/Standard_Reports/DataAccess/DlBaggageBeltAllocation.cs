﻿using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;
using Standard_Reports.Helpers;
using Standard_Reports.Entities;

namespace Standard_Reports.DataAccess
{
    public class DlBaggageBeltAllocation
    {
        /// <summary>
        /// Load Flight List by date Range for Arrival
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <param name="sFilter">System.String containing sFilter</param> 
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadArrivalFlightListByDateRange(DateTime sFDate, DateTime sTdate, string sFilter)
        {
            const string FIELD_LIST = "[FullFlightNumber],[OriginAirportIATACode],[CallSign],[StandardTimeOfArrival],[PositionOfArrival],[Belt1],[TotalNumberOfBaggages],[NumberOfPassengers2],[TerminalBaggageBelt1]";

            string WHERE_CLAUSE = string.Empty;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(sFDate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(sTdate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            WHERE_CLAUSE = String.Format("WHERE [OperationalType] IN ('O','S','B','Z')  AND [ArrivalDepartureId] = 'A' AND ([StandardTimeOfArrival] BETWEEN '{0}' AND '{1}' OR [BestEstimatedArrivalTime] BETWEEN '{0}' AND '{1}') AND [NatureCode] IN ('{2}') ORDER BY [StandardTimeOfArrival],[BestEstimatedArrivalTime]", sFromDate, sToDate, sFilter);

            EntityCollectionBase<EntDbFlight> objFlightList = null;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE

            };
            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objFlightList;
        }

        /// <summary>
        /// Load Nature List 
        /// </summary>
        /// <param name=""></param>
        /// <returns>EntityCollection for Nature  List</returns>
        public static EntityCollectionBase<EntDbFlightNature> LoadNatureList()
        {
            const string FIELD_LIST = "[Code],[Name]";
            string WHERE_CLAUSE = "ORDER BY [Code]";


            EntityCollectionBase<EntDbFlightNature> objNatureList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlightNature),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objNatureList = dataContext.OpenEntityCollection<EntDbFlightNature>(command);

            return objNatureList;
        }


        private static IList<entlcNature> _natureList;
        private static IList<EntDbFlightNature> objNatureList;
        public static IList<entlcNature> LoadNature()
        {
            if (null == _natureList)
            {
                _natureList = new List<entlcNature>();

                //Get Airline  List 
                objNatureList = DlBaggageBeltAllocation.LoadNatureList();

                foreach (EntDbFlightNature item in objNatureList)
                {
                    _natureList.Add(new entlcNature { Name = item.Name, Code = item.Code });
                }
            }

            return _natureList;
        }
        
    }
}