﻿using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;
using Standard_Reports.Helpers; 

namespace Standard_Reports.DataAccess
{
    public class DlGateUsage
    {
        /// <summary>
        /// Load Schedule or Actual Flight List by date Range for Arrival
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Arrival Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadArrivalFlightListByDateRange(DateTime dtFrom, DateTime dtTo, string sType)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[GateOfArrival1],[GateOfArrival2],[ArrivalScheduleBeginForGate1],[ArrivalScheduleEndForGate1],[ArrivalScheduleBeginForGate2],[ArrivalScheduleEndForGate2]," +
                                        "[ArrivalActualBeginForGate1],[ArrivalActualEndForGate1],[ArrivalActualBeginForGate2],[ArrivalActualEndForGate2]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            switch (sType)
            {
                case "PLAN": WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'A' AND [GateOfArrival1] <> ' ' AND ([ArrivalScheduleBeginForGate1] BETWEEN '{0}' AND '{1}' OR [ArrivalScheduleEndForGate1] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [ArrivalScheduleBeginForGate1] AND [ArrivalScheduleEndForGate1]  OR '{1}' BETWEEN [ArrivalScheduleBeginForGate1] AND [ArrivalScheduleEndForGate1]) ORDER BY [ArrivalScheduleBeginForGate1],[ArrivalScheduleEndForGate1]", sFromDate, sToDate);
                    break;
                case "ACT": WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'A' AND [GateOfArrival1] <> ' ' AND  ([ArrivalActualBeginForGate1] BETWEEN '{0}' AND '{1}' OR [ArrivalActualEndForGate1] BETWEEN '{0}' AND '{1}' OR '{0}' BETWEEN [ArrivalActualBeginForGate1] AND [ArrivalActualEndForGate1]   OR  '{1}' BETWEEN  [ArrivalActualBeginForGate1] AND [ArrivalActualEndForGate1]) ORDER BY [ArrivalActualBeginForGate1],[ArrivalActualEndForGate1]", sFromDate, sToDate);
                    break;
            }

            EntityCollectionBase<EntDbFlight> objArrivalFlightList = null;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE

            };
           
            objArrivalFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objArrivalFlightList;
        }

        /// <summary>
        ///  Load Schedule or Actual Flight List by date Range for Departure
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Departure Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadDepartureFlightListByDateRange(DateTime dtFrom, DateTime dtTo, string sType)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[GateOfDeparture1],[GateOfDeparture2],[DepartureScheduleBeginForGate1],[DepartureScheduleEndForGate1],[DepartureScheduleBeginForGate2],[DepartureScheduleEndForGate2]," +
                                        "[DepartureActualBeginForGate1],[DepartureActualEndForGate1],[DepartureActualBeginForGate2],[DepartureActualEndForGate2]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(dtFrom, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(dtTo, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            switch (sType)
            {
                case "PLAN": WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'D' AND [GateOfDeparture1] <> ' ' AND ([DepartureScheduleBeginForGate1] BETWEEN '{0}' AND '{1}' OR [DepartureScheduleEndForGate1] BETWEEN '{0}' AND '{1}'  OR '{0}' BETWEEN [DepartureScheduleBeginForGate1] AND [DepartureScheduleEndForGate1] OR '{1}'BETWEEN  [DepartureScheduleBeginForGate1] AND [DepartureScheduleEndForGate1]) ORDER BY [DepartureScheduleBeginForGate1],[DepartureScheduleEndForGate1]", sFromDate, sToDate);
                    break;
                case "ACT": WHERE_CLAUSE = String.Format("WHERE [ArrivalDepartureId] = 'D'  AND [GateOfDeparture1] <> ' ' AND ([DepartureActualBeginForGate1] BETWEEN '{0}' AND '{1}' OR [DepartureActualEndForGate1] BETWEEN '{0}' AND '{1}' OR '{0}' BETWEEN [DepartureActualBeginForGate1] AND [DepartureActualEndForGate1]  OR '{1}' BETWEEN [DepartureActualBeginForGate1] AND [DepartureActualEndForGate1] ) ORDER BY [DepartureActualBeginForGate1],[DepartureActualEndForGate1]", sFromDate, sToDate);
                    break;
            }

            EntityCollectionBase<EntDbFlight> objDepartureFlightList = null;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE

            }; 
            objDepartureFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objDepartureFlightList;
        }

        /// <summary>
        /// Get Gate List
        /// </summary>
        /// <param name="sFlightNo">System.string containing sFlightNo</param>
        /// <param name="lFlightUrno">System.string containing sFlightUrno</param>
        /// <returns>Return Entity Collection object of EntDbGate</returns>
        public static EntityCollectionBase<EntDbGate> GetGateList()
        {
            const string FIELD_LIST = "[GateName],[GateType],[BusGate]";
            string WHERE_CLAUSE = String.Format("ORDER BY [GateName]");


            EntityCollectionBase<EntDbGate> objGateList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbGate),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objGateList = dataContext.OpenEntityCollection<EntDbGate>(command);

            return objGateList;
        } 
    }
}