﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data.Ceda;
using Ufis.IO;
using Ufis.Data;
using Ufis.Security;
using Ufis.Security.Ceda;
using Ufis.Broadcast;
using Ufis.Broadcast.Ceda;
using Ufis.Utilities;

namespace Standard_Reports.DataAccess
{
    public class DlUfisData
    {
        public EntityDataContextBase DataContext { get; private set; }
        public IUfisUserAuthentication UserAuthenticationManager { get; private set; }
        public IUfisBroadcast BroadcastManager { get; private set; }
        public ProjectInfo ProjectInfo { get; private set; }

        #region Singleton

        private DlUfisData(string appName)
        {
            string strCedaPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.Ceda);
            IniFile myIni = new IniFile(strCedaPath);
            //read the ini file
            string strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
            string strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
            string strTableExt = myIni.IniReadValue("GLOBAL", "TABLEEXTENSION");
            string strProjectInfo = myIni.IniReadValue("GLOBAL", "UNAUTHORIZED_ACCESS_WARNING");
            string strWindowsCodePage = myIni.IniReadValue("GLOBAL", "WINDOWSCODEPAGE");
            int intWindowsCodePage;

            if (!int.TryParse(strWindowsCodePage, out intWindowsCodePage))
                intWindowsCodePage = 1252; // default is windows-latin

            if (string.IsNullOrEmpty(strServer) || string.IsNullOrEmpty(strHopo) ||
                string.IsNullOrEmpty(strTableExt))
            {
                DataContext = null;
            }
            else
            {
                ConnectionBase connection = new CedaConnection()
                {
                    ApplicationName = appName,
                    ServerList = strServer,
                    HomeAirport = strHopo,
                    TableExtension = strTableExt,
                    WindowsCodePage = intWindowsCodePage,
                    WorkstationName = Environment.MachineName,                    

                };
                try
                {
                    connection.Open();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    DataContext = new CedaEntityDataContext(connection);
                    if (connection.State == ConnectionState.Open)
                    {
                        UserAuthenticationManager = new CedaUserAuthentication(DataContext);
                        BroadcastManager = new CedaBroadcastManager();
                        DataContext.BroadcastManager = BroadcastManager;
                        ProjectInfo = new ProjectInfo(strProjectInfo);
                        DataContext.DefaultTimeZoneInfo = DataContext.GetTimeZoneInfo();                        
                    }
                }
            }
        }

        private static DlUfisData _this;

        public static DlUfisData CreateInstance(string appName)
        {
            if (_this != null)
                throw new InvalidOperationException("An instance has been created before!");

            _this = new DlUfisData(appName);

            return _this;
        }

        public static DlUfisData Current
        {
            get { return _this; }
        }
        #endregion
    }
}
