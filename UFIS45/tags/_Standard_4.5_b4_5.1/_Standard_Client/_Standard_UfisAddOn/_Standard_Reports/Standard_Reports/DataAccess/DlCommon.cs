﻿using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;
using Standard_Reports.Helpers;

namespace Standard_Reports.DataAccess
{
    public class DlCommon
    { 
        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <param name="TXID">System.string containing TXID</param>
        /// <returns>Return Entitycollectionbase containing EntDbParameters</returns>
        public static EntityCollectionBase<EntDbParameter> GetParameterValue(string TXID)
        {
            const string FIELD_LIST = "[Value],[ApplicationName],[CreateDate],[NameOfParameter],[ParameterIdentifier],[ParameterType],[TextIdentifier],[EvaluationMode]";
            string WHERE_CLAUSE = string.Format("WHERE [ParameterIdentifier] = '{0}'", TXID);

            EntityCollectionBase<EntDbParameter> objParameter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbParameter),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE               
            };

            objParameter = dataContext.OpenEntityCollection<EntDbParameter>(command);

            return objParameter;
        }
        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.string containing USID</param>
        /// <returns>Return Entitycollectionbase containing EntDbSecured</returns>
        public static EntityCollectionBase<EntDbSecured> GetLoginUserName(string USID)
        {
            const string FIELD_LIST = "[Name],[Remark],[Status],[Type]";
            string WHERE_CLAUSE = string.Format("WHERE [UserId] = '{0}'", USID);

            EntityCollectionBase<EntDbSecured> objParameter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbSecured),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objParameter = dataContext.OpenEntityCollection<EntDbSecured>(command);

            return objParameter;
        }
    }
}