﻿using System;
using System.Collections.Generic;
using System.Linq; 
using Ufis.Entities;

namespace Standard_Reports.Entities
{
    public class entlcGateUsage : Entity
    {
        private string _Arrival = "";
        public string Arrival
        {
            get { return _Arrival; }
            set
            {
                if (_Arrival != value)
                {
                    _Arrival = value;
                    OnPropertyChanged("Arrival");
                }
            }
        }
        private string _From = "";
        public string From
        {
            get { return _From; }
            set
            {
                if (_From != value)
                {
                    _From = value;
                    OnPropertyChanged("From");
                }
            }
        }

        private string _scheduleGate1="";
        public string ScheduleGate1
        {
            get { return _scheduleGate1; }
            set
            {
                if (_scheduleGate1 != value)
                {
                    _scheduleGate1 = value;
                    OnPropertyChanged("ScheduleGate1");
                }
            }
        }

        private string _scheduleGate2 = "";
        public string ScheduleGate2
        {
            get { return _scheduleGate2; }
            set
            {
                if (_scheduleGate2 != value)
                {
                    _scheduleGate2 = value;
                    OnPropertyChanged("ScheduleGate2");
                }
            }
        }

        private string _actualGate1 = "";
        public string ActualGate1
        {
            get { return _actualGate1; }
            set
            {
                if (_actualGate1 != value)
                {
                    _actualGate1 = value;
                    OnPropertyChanged("ActualGate1");
                }
            }
        }

        private string _actualGate2 = "";
        public string ActualGate2
        {
            get { return _actualGate2; }
            set
            {
                if (_actualGate2 != value)
                {
                    _actualGate2 = value;
                    OnPropertyChanged("ActualGate2");
                }
            }
        } 
   
        private string _Departure = "";
        public string Departure
        {
            get { return _Departure; }
            set
            {
                if (_Departure != value)
                {
                    _Departure = value;
                    OnPropertyChanged("Departure");
                }
            }
        }
     
        private string _To = "";
        public string TO
        {
            get { return _To; }
            set
            {
                if (_To != value)
                {
                    _To = value;
                    OnPropertyChanged("TO");
                }
            }
        } 

        private string _Gate = "";
        public string Gate
        {
            get { return _Gate; }
            set
            {
                if (_Gate != value)
                {
                    _Gate = value;
                    OnPropertyChanged("Gate");
                }
            }
        }  
    }
}
