﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : entlcBeltAllocation.cs

Version         : 1.0.0
Created Date    : 16 - Feb - 2012
Complete Date   : 16 - Feb - 2012
Created By      : Phyoe Khaing Min

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Standard_Reports.Entities
{
    public class entlcBaggageBeltAllocation : Entity
    {

        private string _terminal = "";
        public string Terminal
        {
            get { return _terminal; }
            set
            {
                if (_terminal != value)
                {
                    _terminal = value;
                    OnPropertyChanged("Terminal");
                }
            }
        }

        private string _arrival = "";
        public string Arrival
        {
            get { return _arrival; }
            set
            {
                if (_arrival != value)
                {
                    _arrival = value;
                    OnPropertyChanged("Arrival");
                }
            }
        }

        private string _from = "";
        public string From
        {
            get { return _from; }
            set
            {
                if (_from != value)
                {
                    _from = value;
                    OnPropertyChanged("From");
                }
            }
        }

        private string _sta = "";
        public string STA
        {
            get { return _sta; }
            set
            {
                if (_sta != value)
                {
                    _sta = value;
                    OnPropertyChanged("STA");
                }
            }
        }

        private string _aPos = "";
        public string APos
        {
            get { return _aPos; }
            set
            {
                if (_aPos != value)
                {
                    _aPos = value;
                    OnPropertyChanged("APos");
                }
            }
        }

        private string _belt = "";
        public string Belt
        {
            get { return _belt; }
            set
            {
                if (_belt != value)
                {
                    _belt = value;
                    OnPropertyChanged("Belt");
                }
            }
        }

        private int? _aBag;
        public int? ABag
        {
            get { return _aBag; }
            set
            {
                if (_aBag != value)
                {
                    _aBag = value;
                    OnPropertyChanged("ABag");
                }
            }
        }

        private int? _APax;
        public int? APax
        {
            get { return _APax; }
            set
            {
                if (_APax != value)
                {
                    _APax = value;
                    OnPropertyChanged("APax");
                }
            }
        }

        private string _customRqst = "";
        public string CustomRqst
        {
            get { return _customRqst; }
            set
            {
                if (_customRqst != value)
                {
                    _customRqst = value;
                    OnPropertyChanged("CustomRqst");
                }
            }
        }
        private string _remarks = "";
        public string Remarks
        {
            get { return _remarks; }
            set
            {
                if (_remarks != value)
                {
                    _remarks = value;
                    OnPropertyChanged("Remarks");
                }
            }
        }

        private IList<Object> _natureNames = null;
        public virtual IList<Object> NatureNames
        {
            get { return _natureNames; }
            set
            {
                if (_natureNames != value)
                {
                    _natureNames = value;

                    OnPropertyChanged("NatureNames");
                }
            }
        }

        private IList<Object> _natureCodes = null;
        public virtual IList<Object> NatureCodes
        {
            get { return _natureCodes; }
            set
            {
                if (_natureCodes != value)
                {
                    _natureCodes = value;

                    OnPropertyChanged("NatureCodes");
                }
            }
        }
    }
    public class entlcNature : Entity
    {
        // Fields...
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)

                    _name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                if (_code != value)

                    _code = value;
                OnPropertyChanged("Code");
            }
        }

        public override string ToString()
        {
            return Code + "-" + Name;
        }
    }

}
