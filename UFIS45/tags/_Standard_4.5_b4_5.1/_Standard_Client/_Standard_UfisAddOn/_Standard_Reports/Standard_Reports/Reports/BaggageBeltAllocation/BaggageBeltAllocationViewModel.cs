﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;
using Standard_Reports.Entities;

namespace Standard_Reports.Reports.BaggageBeltAllocation
{
    public class BaggageBeltAllocationViewModel : WorkspaceViewModel
    {
        #region +++ Variable Declaration +++
        public WorkspaceViewModel _workspace;
        IList<entlcBaggageBeltAllocation> _itemSource;
        List<entlcBaggageBeltAllocation> objBaggageBeltAllocation;
        EntityCollectionBase<EntDbFlight> objArrivalFlights; 
        DateTime? date;
        #endregion

        #region +++ Data Manipulation +++
        /// <summary>
        /// Get Flight list by Date Range
        /// </summary>
        /// <param name="sFrom">System.DateTime containing sFrom</param>
        /// <param name="sTo">System.Datetime containing sTo</param>
        /// <param name="sFilter">System.Datetime containing sFilter</param>
        private void GetFlights(DateTime sFrom, DateTime sTo,string sFilter)
        {
            //Get Arrival Flights by date and time range and filter based on Nature Code
            objArrivalFlights = DlBaggageBeltAllocation.LoadArrivalFlightListByDateRange(sFrom, sTo, sFilter);
        }
        /// <summary>
        /// Get Arrival Flight
        /// </summary>
        /// <param name="sFrom">System.DateTime Containing sFrom</param>
        /// <param name="sTo">Sytstem.DateTime containing sTo</param>  
        /// <param name="sFilter">Sytstem.DateTime containing sFilter</param>   
        public void GetBaggageBeltAllocationList(DateTime sFrom, DateTime sTo,string sFilter)
        { 
            GetFlights(sFrom, sTo,sFilter);
            GetArrivalFlighsts();
            ItemSource = objBaggageBeltAllocation;
        }
        /// <summary>
        ///  Do process for Baggage Belt 
        /// </summary>
        private void GetArrivalFlighsts()
        {
            if (objArrivalFlights.Count > 0)
            {
                objBaggageBeltAllocation = new List<entlcBaggageBeltAllocation>();
                foreach (EntDbFlight tmpObjArrivalFlight in objArrivalFlights)
                {

                    entlcBaggageBeltAllocation TmpobjBaggageBeltAllocation = new entlcBaggageBeltAllocation();

                    TmpobjBaggageBeltAllocation.Terminal = tmpObjArrivalFlight.TerminalBaggageBelt1;

                    if (string.IsNullOrEmpty(tmpObjArrivalFlight.FullFlightNumber))
                        TmpobjBaggageBeltAllocation.Arrival = tmpObjArrivalFlight.CallSign;
                    else
                        TmpobjBaggageBeltAllocation.Arrival = tmpObjArrivalFlight.FullFlightNumber;

                    TmpobjBaggageBeltAllocation.From = tmpObjArrivalFlight.OriginAirportIATACode;

                    if (tmpObjArrivalFlight.StandardTimeOfArrival != null)
                    {
                        date = tmpObjArrivalFlight.StandardTimeOfArrival;
                        if (date.HasValue)
                        {
                            TmpobjBaggageBeltAllocation.STA = string.Format("{0:dd/HHmm}", date.Value);
                        }
                    }

                    TmpobjBaggageBeltAllocation.APos = tmpObjArrivalFlight.PositionOfArrival;

                    TmpobjBaggageBeltAllocation.Belt = tmpObjArrivalFlight.Belt1;

                    TmpobjBaggageBeltAllocation.ABag = tmpObjArrivalFlight.TotalNumberOfBaggages;

                    TmpobjBaggageBeltAllocation.APax = tmpObjArrivalFlight.TotalNumberOfPassengers;  

                    objBaggageBeltAllocation.Add(TmpobjBaggageBeltAllocation);
                }
            }
        }

        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlCommon.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }

        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.String containing USID</param>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlCommon.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }
        #endregion

        #region +++ Properties +++
        public IList<entlcBaggageBeltAllocation> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }
        #endregion

    }
}
