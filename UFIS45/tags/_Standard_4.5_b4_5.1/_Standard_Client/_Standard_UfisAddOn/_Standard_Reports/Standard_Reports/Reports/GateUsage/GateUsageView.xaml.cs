﻿using System;
using System.Linq;
using System.Windows;
using System.Threading;
using DevExpress.Xpf.Grid;
using System.Globalization;
using System.Windows.Controls;
using DevExpress.Xpf.Printing;
using System.Collections.Generic;
using Ufis.Data.Ceda;
using Ufis.IO;
using System.Windows.Media.Imaging;
using DevExpress.Xpf.Charts;
using System.Collections;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Ufis.LoginWindow;
using DevExpress.XtraPrinting;
using Standard_Reports.Helpers;
using System.Printing;


namespace Standard_Reports.Reports.GateUsage
{
    /// <summary>
    /// Interaction logic for GateUsageView.xaml
    /// </summary>
    public partial class GateUsageView : UserControl
    {
        #region +++ Variable Declaration +++
        public HpParameters param;
        public HpReportLogo Logo;
        public HpFooter Footer;
        public GateUsageViewModel objViewModel;
        string sFooterString = "";
        DateTime tfDate = new DateTime();
        DateTime ttDate = new DateTime();
        private string strPlanAct = string.Empty;
        private string strRemoteContact = string.Empty;
        private string strArrDep = string.Empty;
        #endregion


        #region +++ Register Properties +++
        // Using a DependencyProperty as the backing store for MyInt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyIntProperty =
            DependencyProperty.Register("MyInt", typeof(int), typeof(GateUsageView), new UIPropertyMetadata(1));
        #endregion

        #region +++ Properties +++
        public int MyInt
        {
            get { return (int)GetValue(MyIntProperty); }
            set { SetValue(MyIntProperty, value); }
        }
        #endregion

        #region +++ WPF Application Events +++
        public GateUsageView()
        {
            //Change UFIS Date format into system.Thread
            CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
            dtpFromDate.SelectedDate = DateTime.Now.Date;
            dtpToDate.SelectedDate = DateTime.Now.Date;
            txtFromTime.Text = "0000";
            txtToTime.Text = "2359";
            FillCustomAxisLabels();
        }

        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if (objViewModel != null)
            {

                if (objViewModel.ItemSource != null)
                {

                    Ufis.Entities.EntUser objEntUser = new Ufis.Entities.EntUser();
                    objEntUser = LoginViewModel.GetLastUser();

                    param = new HpParameters();
                    Footer = new HpFooter();
                    DateTime tDate = dtpFromDate.SelectedDate.Value;
                    string sDate = tDate.ToString("dd.MM.yyyy");
                    DateTime ttDate = dtpToDate.SelectedDate.Value;
                    string stDate = ttDate.ToString("dd.MM.yyyy");

                    sFooterString = objViewModel.GetParameterValue();

                    param.MyParameter = String.Format("Gate Usage Report from {0} {1} to {2} {3} {4} ", sDate, txtFromTime.Text, stDate, txtToTime.Text, HpUser.TLocalUTC == "L" ? "LT" : "UTC");
                    param.ReportFooterString = sFooterString;
                    param.GeneratedBy = string.Format("Printed By : {0}", objViewModel.GetLoginUserName(objEntUser.UserId));

                    ((HpParameters)Resources["paramet"]).MyParameter = param.MyParameter;
                    ((HpParameters)Resources["Footer"]).ReportFooterString = param.ReportFooterString;
                    ((HpParameters)Resources["GenBy"]).GeneratedBy = param.GeneratedBy;

                    string sLogoPath = CedaEnvironment.GetEnvironmentVariable(CedaEnvironment.CedaEnvironmentVariable.UfisSystem);
                    sLogoPath = sLogoPath + "\\stdlogo.bmp";

                    Logo = new HpReportLogo();
                    Logo.RptLogo = new BitmapImage(new Uri(sLogoPath, UriKind.Absolute));
                    ((HpReportLogo)Resources["Logo"]).RptLogo = Logo.RptLogo;

                    SimpleLink link = new SimpleLink();
                    link.DetailCount = 1;
                    link.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
                    link.Landscape = true;
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    link.DetailTemplate = (DataTemplate)Resources["DetailPrintChartTemplate"];
                    link.PageHeaderTemplate = (DataTemplate)Resources["DetailPrintHeaderTemplate"];
                    link.PageFooterTemplate = (DataTemplate)Resources["DetailPrintFooterTemplate"];
                    link.CreateDetail += new EventHandler<DevExpress.Xpf.Printing.CreateAreaEventArgs>(link_CreateDetail);
                    link.CreateDocument(false);
                    link.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    link.ShowPrintPreviewDialog(Window.GetWindow(this));
                }
            }

        }

        void link_CreateDetail(object sender, DevExpress.Xpf.Printing.CreateAreaEventArgs e)
        {
            VisualBrush brush = new VisualBrush(gateUsageChart);
            DrawingVisual visual = new DrawingVisual();
            DrawingContext context = visual.RenderOpen();

            //context.DrawRectangle(brush, null,
            //   new Rect(0, 0, gateUsageChart.ActualWidth, gateUsageChart.ActualHeight));
            //context.Close();

            //RenderTargetBitmap bmp = new RenderTargetBitmap((int)gateUsageChart.ActualWidth,
            //    (int)gateUsageChart.ActualHeight, 96, 96, PixelFormats.Pbgra32);

            context.DrawRectangle(brush, null,
              new Rect(0, 0, 1024, 525));
            context.Close();

            RenderTargetBitmap bmp = new RenderTargetBitmap((int)1024,
                (int)525, 96, 96, PixelFormats.Pbgra32);

            bmp.Render(visual);

            e.Data = bmp;
        }


        private void btnView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtpFromDate.SelectedDate.ToString() != "" && dtpToDate.SelectedDate.ToString() != "" && txtFromTime.Text != "" && txtToTime.Text != "" && dtpToDate.SelectedDate != null && dtpFromDate.SelectedDate != null && txtFromTime.Text.Length >= 4 && txtToTime.Text.Length >= 4)
                {
                    objViewModel = new GateUsageViewModel();
                    string sfDate, stDate;
                    DateTime tfDate = dtpFromDate.SelectedDate.Value;
                    DateTime ttDate = dtpToDate.SelectedDate.Value;
                    sfDate = String.Format("{0:yyyy.MM.dd} {1}", tfDate, txtFromTime.Text.Insert(2, ":"));
                    stDate = String.Format("{0:yyyy.MM.dd} {1}", ttDate, txtToTime.Text.Insert(2, ":"));

                    try
                    {
                        tfDate = Convert.ToDateTime(sfDate);
                        ttDate = Convert.ToDateTime(stDate);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Time value should be between 0000 and 2359.");
                    }

                    if (tfDate > ttDate)
                    {
                        MessageBox.Show("From date time period should be greater than To date time period.");
                    }
                    else
                    {

                        TimeSpan ts = ttDate - tfDate;
                        int totalHours = Convert.ToInt32(ts.TotalMinutes / 60);

                        if (ts.Days >= 1 && ts.Minutes >= 1 || totalHours > 24)
                        {
                            MessageBox.Show("Seleceted time period should not exceed a total of 24 hours.");
                        }
                        else
                        {
                            tfDate.AddHours(Convert.ToDouble(txtFromTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtFromTime.Text.Substring(2, 2))).AddSeconds(0);
                            ttDate.AddHours(Convert.ToDouble(txtToTime.Text.Substring(0, 2))).AddMinutes(Convert.ToDouble(txtToTime.Text.Substring(2, 2))).AddSeconds(59);

                            if (chkPlan.IsChecked.Value)
                                strPlanAct = "PLAN";
                            else if (chkAct.IsChecked.Value)
                                strPlanAct = "ACT";
                            if (chkRemoteContact.IsChecked.Value)
                                strRemoteContact = chkRemoteContact.Content.ToString();
                            else if (chkRemote.IsChecked.Value)
                                strRemoteContact = chkRemote.Content.ToString();
                            else if (chkContact.IsChecked.Value)
                                strRemoteContact = chkContact.Content.ToString();
                            if (chkArrDep.IsChecked.Value) strArrDep = chkArrDep.Content.ToString();
                            else if (chkArr.IsChecked.Value) strArrDep = chkArr.Content.ToString();
                            else if (chkDep.IsChecked.Value) strArrDep = chkDep.Content.ToString();

                            objViewModel.GetGateUsageReportDate(tfDate, ttDate, strPlanAct, strRemoteContact.ToUpper(), strArrDep.ToUpper());
                            CreateGateUsageBarSeries();
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Invalid Date/Time value.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\r{1}\n\r{2}", ex.Message, ex.InnerException, ex.StackTrace));
            }

        }


        private void CreateGateUsageBarSeries()
        {
            HpSeriesTypeItem seriesTypeItem = new HpSeriesTypeItem(typeof(XYDiagram2D), typeof(BarSideBySideStackedSeries2D), "2D Side-By-Side Stacked Bars", 4) as HpSeriesTypeItem;

            if (seriesTypeItem != null)
            {
                gateUsageChart.BeginInit();
                ClearCustomAxisLabels();
                FillCustomAxisLabels();
                gateUsageChartDiagram.Series.Clear();
                try
                {
                    // gateUsageChart.Diagram = (Diagram)Activator.CreateInstance(seriesTypeItem.DiagramType); 

                    ArrayList objSeriesLable = SetSeriesLable();
                    for (int i = 0; i < seriesTypeItem.SeriesCount; i++)
                    {
                        Series series = (Series)Activator.CreateInstance(seriesTypeItem.SeriesType);
                        series.Label = new SeriesLabel();
                        series.DisplayName = objSeriesLable[i].ToString();
                        InitializeSeries(series, i + 1);
                        ISupportStackedGroup supportStackedGroup = series as ISupportStackedGroup;
                        if (supportStackedGroup != null)
                            supportStackedGroup.StackedGroup = i % 2;
                        ISupportTransparency supportTransparency = series as ISupportTransparency;
                        if (supportTransparency != null)
                            supportTransparency.Transparency = 0.3;
                        series.Label.FontSize = 10.00;
                        series.Label.ResolveOverlappingMode = ResolveOverlappingMode.Default;
                        gateUsageChartDiagram.Series.Add(series);

                    }
                }
                finally
                {
                    gateUsageChart.EndInit();
                }
            }

        }

        private ArrayList SetSeriesLable()
        {
            ArrayList objSeriesLable = new ArrayList();
            objSeriesLable.Add("Remote Arrival");
            objSeriesLable.Add("Remote Departure");
            objSeriesLable.Add("Contact Arrival");
            objSeriesLable.Add("Contact Departure");
            return objSeriesLable;
        }

        private void InitializeSeries(Series series, int seriesNumber)
        {
            series.DataSource = objViewModel.ItemSource;
            series.ArgumentDataMember = "Argument";
            series.ValueDataMember = "Value" + seriesNumber.ToString();
        }

        void FillCustomAxisLabels()
        {
            XYDiagram2D diagram = (XYDiagram2D)gateUsageChart.Diagram;
            for (int i = 0; i < 500; i++)
            {
                diagram.AxisY.CustomLabels.Add(new CustomAxisLabel(i, String.Format("{0}", i.ToString())));
            }
        }

        void ClearCustomAxisLabels()
        {
            ((XYDiagram2D)gateUsageChart.Diagram).AxisY.CustomLabels.Clear();
        }
        #endregion

        private void gateUsageChart_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ChartHitInfo hitInfo = gateUsageChart.CalcHitInfo(e.GetPosition(gateUsageChart));

            if (hitInfo != null && hitInfo.SeriesPoint != null)
            {
                SeriesPoint point = hitInfo.SeriesPoint;

                //Gatetooltip_text.Text = string.Format("Series = {0}\n Hour {1:00}00 - {1:00}59\nTotal No.of Flights = {2}",
                //    point.Series.DisplayName, Int32.Parse(point.Argument), point.Value);

                Gatetooltip_text.Text = string.Format("Series = {0}\nTotal No.of Flights = {1}",
                 point.Series.DisplayName, point.Value);

                Gatetooltip.Placement = PlacementMode.Mouse;
                Gatetooltip.IsOpen = true;
                Cursor = Cursors.Hand;
            }
            else
            {
                Gatetooltip.IsOpen = false;
                Cursor = Cursors.Arrow;
            }
        }

        private void gateUsageChart_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Gatetooltip.IsOpen = false;
        }
    }
}