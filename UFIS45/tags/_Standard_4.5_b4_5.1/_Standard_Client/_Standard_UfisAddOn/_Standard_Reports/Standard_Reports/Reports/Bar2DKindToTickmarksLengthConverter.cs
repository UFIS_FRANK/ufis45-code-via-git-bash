using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using DevExpress.Xpf.Charts;

namespace Standard_Reports.Reports
{ 
    public class Bar2DKindToTickmarksLengthConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Bar2DKind bar2DKind = value as Bar2DKind;
            if (bar2DKind != null)
            {
                switch (bar2DKind.Name)
                {
                    case "Glass Cylinder":
                        return 18;
                    case "Quasi-3D Bar":
                        return 9;
                }
            }
            return 5;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
