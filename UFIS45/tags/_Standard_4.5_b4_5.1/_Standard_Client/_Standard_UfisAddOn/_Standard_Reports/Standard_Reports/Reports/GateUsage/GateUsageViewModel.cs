﻿using System;
using System.Linq;
using System.Collections.Generic;

using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using Standard_Reports.DataAccess;
using Standard_Reports.Entities;
using Standard_Reports.Helpers;
using Ufis.Data.Ceda;

namespace Standard_Reports.Reports.GateUsage
{
    public class GateUsageViewModel : WorkspaceViewModel
    {
        #region +++ Variable Declaration +++
        public WorkspaceViewModel _workspace;
        IList<HpDataPoint> _itemSource;
        EntityCollectionBase<EntDbFlight> objArrivalFlights;
        EntityCollectionBase<EntDbFlight> objDepFlights;
        EntityCollectionBase<EntDbGate> objGateList;
        private double[] _RemoteArr;
        private double[] _ContactArr;
        private double[] _RemoteDep;
        private double[] _ContactDep;
        DateTime? dtBegin = new DateTime();
        DateTime? dtEnd = new DateTime();

        List<HpDataPoint> objGateUsageData = new List<HpDataPoint>();

        #endregion

        #region +++ Data Manipulation +++
        /// <summary>
        /// Get Flight list by Date Range
        /// </summary>
        /// <param name="sType">System.Strung containing sType</param> 
        private void GetFlights(string sType)
        {
            //Get Arrival Flight by date and time range
            objArrivalFlights = DlGateUsage.LoadArrivalFlightListByDateRange(DateFrom, DateTo, sType);
            //Get Departure Flight by date and time range
            objDepFlights = DlGateUsage.LoadDepartureFlightListByDateRange(DateFrom, DateTo, sType);
            objGateList = DlGateUsage.GetGateList();
        }

        public void GetGateUsageReportDate(DateTime dtFrom, DateTime dtTo, string sType, string sFilter1, string sFilter2)
        {
            DateFrom = dtFrom;
            DateTo = dtTo; 
            int totalHours = 24;
            _RemoteArr = new double[totalHours];
            _RemoteDep = new double[totalHours];
            _ContactArr = new double[totalHours];
            _ContactDep = new double[totalHours];
            GetFlights(sType);
            switch (sType)
            {
                case "PLAN":
                    {
                        GetScheduleArrivalGateUsageList();
                        GetScheduleDepartureGateUsageList();
                        break;
                    }
                case "ACT":
                    {
                        GetActualArrivalGateUsageList();
                        GetActualDepartureGateUsageList();
                        break;
                    }
            }

            CreateGateUsageChartDataSource(sFilter1, sFilter2);
        }

        /// <summary>
        /// Get Schedule Arrival Flight Data
        /// </summary>
        private string GetScheduleArrivalGateUsageList()
        {
            try
            {

                if (objArrivalFlights.Count > 0)
                {
                    foreach (EntDbFlight tmpArrivalFlights in objArrivalFlights)
                    {
                        if (objGateList != null)
                        {
                            EntDbGate objScheduleGate = objGateList.FirstOrDefault(d => d.GateName == tmpArrivalFlights.GateOfArrival1);

                            if (tmpArrivalFlights.ArrivalScheduleBeginForGate1 != null)
                            {
                                dtBegin = tmpArrivalFlights.ArrivalScheduleBeginForGate1;
                            }
                            if (tmpArrivalFlights.ArrivalScheduleEndForGate1 != null)
                            {
                                dtEnd = tmpArrivalFlights.ArrivalScheduleEndForGate1;
                            }
                            if (dtBegin.HasValue && dtEnd.HasValue)
                            {
                                if (dtBegin <= DateFrom)
                                    dtBegin = DateFrom;
                                else if (dtEnd >= DateTo)
                                    dtEnd = DateTo;

                                Int32 fromX = 0;
                                Int32 toX = 0; 
                                
                                
                                //fromX = (dtBegin.Value.Hour + Convert.ToInt32(dtBegin.Value.Minute / 60)) - DateFrom.Hour;

                                //toX = dtEnd.Value.Hour + Convert.ToInt32(dtEnd.Value.Minute / 60) - DateFrom.Hour;

                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                fromX = dtBegin.Value.Subtract(tempTime).Hours;

                                toX = dtEnd.Value.Subtract(tempTime).Hours;

                                for (int j = fromX; j <= toX; j++)
                                {
                                    if (objScheduleGate.BusGate == "X")
                                        _RemoteArr[j]++;
                                    else _ContactArr[j]++;
                                }
                            }

                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }

        /// <summary>
        /// Get Actual Arrival Flight Data
        /// </summary>
        private string GetActualArrivalGateUsageList()
        {
            try
            {

                if (objArrivalFlights.Count > 0)
                {
                    foreach (EntDbFlight tmpArrivalFlights in objArrivalFlights)
                    {
                        if (objGateList != null)
                        {
                            EntDbGate objActualGate = objGateList.FirstOrDefault(d => d.GateName == tmpArrivalFlights.GateOfArrival1);

                            if (tmpArrivalFlights.ArrivalActualBeginForGate1 != null)
                            {
                                dtBegin = tmpArrivalFlights.ArrivalActualBeginForGate1;
                            }
                            if (tmpArrivalFlights.ArrivalActualEndForGate1 != null)
                            {
                                dtEnd = tmpArrivalFlights.ArrivalActualEndForGate1;
                            }
                            if (dtBegin.HasValue && dtEnd.HasValue)
                            {
                                if (dtBegin <= DateFrom)
                                    dtBegin = DateFrom;
                                else if (dtEnd >= DateTo)
                                    dtEnd = DateTo;

                                Int32 fromX = 0;
                                Int32 toX = 0;

                                DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                fromX = dtBegin.Value.Subtract(tempTime).Hours;

                                toX = dtEnd.Value.Subtract(tempTime).Hours;

                                for (int j = fromX; j <= toX; j++)
                                {
                                    if (objActualGate.BusGate == "X")
                                        _RemoteArr[j]++;
                                    else _ContactArr[j]++;
                                }
                            }

                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }

        /// <summary>
        /// Get Schedule Departure Flight Data
        /// </summary>
        private string GetScheduleDepartureGateUsageList()
        {
            try
            {
                if (objDepFlights.Count > 0)
                {
                    foreach (EntDbFlight tmpDepartureFlights in objDepFlights)
                    {
                        entlcGateUsage objTemGateTiming = new entlcGateUsage();
                        {
                            if (objGateList != null)
                            {
                                EntDbGate objScheduleGate = objGateList.FirstOrDefault(d => d.GateName == tmpDepartureFlights.GateOfDeparture1);

                                if (tmpDepartureFlights.DepartureScheduleBeginForGate1 != null)
                                {
                                    dtBegin = tmpDepartureFlights.DepartureScheduleBeginForGate1;
                                }
                                if (tmpDepartureFlights.DepartureScheduleEndForGate1 != null)
                                {
                                    dtEnd = tmpDepartureFlights.DepartureScheduleEndForGate1;
                                }
                                if (dtBegin.HasValue && dtEnd.HasValue)
                                {
                                    if (dtBegin <= DateFrom)
                                        dtBegin = DateFrom;
                                    else if (dtEnd >= DateTo)
                                        dtEnd = DateTo;

                                    Int32 fromX = 0;
                                    Int32 toX = 0; 

                                    //DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 59, 0);

                                    //fromX = dtBegin.Value.Subtract(DateFrom).Hours + Convert.ToInt32(dtBegin.Value.Subtract(DateFrom).Minutes / 60);

                                    //toX = dtEnd.Value.Subtract(DateFrom).Hours; 

                                    DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                    fromX = dtBegin.Value.Subtract(tempTime).Hours;

                                    toX = dtEnd.Value.Subtract(tempTime).Hours;

                                 
                                    for (int j = fromX; j <= toX; j++)
                                    {
                                        if (objScheduleGate.BusGate == "X")
                                            _RemoteDep[j]++;
                                        else _ContactDep[j]++;
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }

        /// <summary>
        /// Get Actual Departure Flight Data
        /// </summary>
        private string GetActualDepartureGateUsageList()
        {
            try
            {
                if (objDepFlights.Count > 0)
                {
                    foreach (EntDbFlight tmpDepartureFlights in objDepFlights)
                    {
                        entlcGateUsage objTemGateTiming = new entlcGateUsage();
                        {
                            if (objGateList != null)
                            {
                                EntDbGate objScheduleGate = objGateList.FirstOrDefault(d => d.GateName == tmpDepartureFlights.GateOfDeparture1);

                                if (tmpDepartureFlights.DepartureActualBeginForGate1 != null)
                                {
                                    dtBegin = tmpDepartureFlights.DepartureActualBeginForGate1;
                                }
                                if (tmpDepartureFlights.DepartureActualEndForGate1 != null)
                                {
                                    dtEnd = tmpDepartureFlights.DepartureActualEndForGate1;
                                }
                                if (dtBegin.HasValue && dtEnd.HasValue)
                                {
                                    if (dtBegin <= DateFrom)
                                        dtBegin = DateFrom;
                                    else if (dtEnd >= DateTo)
                                        dtEnd = DateTo;

                                    Int32 fromX = 0;
                                    Int32 toX = 0;
                                    //TimeSpan scheduleTime = dtEnd.Value.Subtract(dtBegin.Value);

                                    //fromX = (dtBegin.Value.Hour + Convert.ToInt32(dtBegin.Value.Minute / 60)) - DateFrom.Hour;

                                    //toX = dtEnd.Value.Hour + Convert.ToInt32(dtEnd.Value.Minute / 60) - DateFrom.Hour;

                                    DateTime tempTime = new DateTime(DateFrom.Year, DateFrom.Month, DateFrom.Day, DateFrom.Hour, 0, 0);

                                    fromX = dtBegin.Value.Subtract(tempTime).Hours;

                                    toX = dtEnd.Value.Subtract(tempTime).Hours;

                                    for (int j = fromX; j <= toX; j++)
                                    {
                                        if (objScheduleGate.BusGate == "X")
                                            _RemoteDep[j]++;
                                        else _ContactDep[j]++;
                                    }
                                }

                            }
                        }

                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                string msg = ex.StackTrace;
                return msg;
            }
        }

        private void CreateGateUsageChartDataSource(string sFilter1, string sFilter2)
        { 
            switch (sFilter1)
            {
                case "REMOTE":
                    {
                        switch (sFilter2)
                        {
                            case "ARR":
                                {
                                    for (int i = 0; i < 24; i++)
                                    {
                                       int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        } 
                                            objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), _RemoteArr[i], 0.0, 0.0, 0.0));
                                         
                                    }
                                    break;
                                }
                            case "DEP":
                                {

                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), 0.0, _RemoteDep[i], 0.0, 0.0));
                                    }
                                    
                                    break;
                                }
                            default:
                                {

                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), _RemoteArr[i], _RemoteDep[i], 0.0, 0.0));
                                    }
                                    break;
                                }
                        }
                        break;
                    }
                case "CONTACT":
                    {
                        switch (sFilter2)
                        {
                            case "ARR":
                                {
                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), 0.0, 0.0, _ContactArr[i], 0.0));
                                    }
                                    break;
                                }
                            case "DEP":
                                {
                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), 0.0, 0.0, 0.0, _ContactDep[i]));
                                    }
                                    break;
                                }
                            default:
                                {
                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), 0.0, 0.0, _ContactArr[i], _ContactDep[i]));
                                    }
                                    break;
                                }
                        }
                        break;
                    }
                default:
                    {
                        switch (sFilter2)
                        {
                            case "ARR":
                                {

                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), _RemoteArr[i], 0.0, _ContactArr[i], 0.0));
                                    }
                                    break;
                                }
                            case "DEP":
                                {

                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), 0.0, _RemoteDep[i], 0.0, _ContactDep[i]));
                                    }
                                    
                                    break;
                                }
                            default:
                                {
                                    for (int i = 0; i < 24; i++)
                                    {
                                        int j = i + DateFrom.Hour;

                                        if (j >= 24)
                                        {
                                            j = j - 24;
                                        }
                                        objGateUsageData.Add(HpDataPoint.CreateBarDataPoint(j.ToString(), _RemoteArr[i], _RemoteDep[i], _ContactArr[i], _ContactDep[i]));
                                    }
                                    break;
                                }
                        }
                        break;
                    }
            }

            ItemSource = objGateUsageData;
        }

        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlCommon.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }
        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.String containing USID</param>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlCommon.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }
        #endregion

        #region +++ Properties +++
        public IList<HpDataPoint> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }

        private DateTime DateFrom
        {
            get;
            set;
        }

        private DateTime DateTo
        {
            get;
            set;
        }
        #endregion
    }
}
