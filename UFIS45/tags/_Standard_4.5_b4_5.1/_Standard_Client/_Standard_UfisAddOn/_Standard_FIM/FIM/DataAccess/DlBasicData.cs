﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Ufis.Data.Ceda;

namespace BDPSUIF.DataAccess
{
    public class DlBasicData
    {
        public static EntityCollectionBase<EntDbFlightIdMap> LoadFlightIdMaps()
        {
            const string FIELD_LIST = "[Urno],[DataSourceName],[ArrivalDepartureId]," +
                "[InterfaceCallSign],[CallSign],[ValidFrom],[ValidTo]," +
                "[CreatedBy],[CreationDate],[ModifiedBy],[ModificationDate]";
            const string ORDER_BY = "ORDER BY [DataSourceName],[InterfaceCallSign]";

            EntityCollectionBase<EntDbFlightIdMap> flightIdMaps = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlightIdMap),
                AttributeList = FIELD_LIST,
                EntityWhereClause = ORDER_BY
            };

            flightIdMaps = dataContext.OpenEntityCollection<EntDbFlightIdMap>(command);

            return flightIdMaps;
        }

        private static IList<EntCodeName> _adidList;
        public static IList<EntCodeName> GetADIDList()
        {
            if (_adidList == null)
                _adidList = new List<EntCodeName>()
                    {
                        new EntCodeName() { Code = "A", Name = "Arrival" },
                        new EntCodeName() { Code = "D", Name = "Departure" }
                    };

            return _adidList;
        }
    }
}
