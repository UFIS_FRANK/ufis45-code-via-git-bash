﻿using System;
using System.Collections.Generic;
using System.Linq;
using BDPSUIF.DataAccess;
using Ufis.Data;
using Ufis.Entities;
using Ufis.Utilities;
using System.Reflection;

namespace BDPSUIF.BasicData
{
    public class FlightIdMapViewModel : BasicDataViewModel
    {
        EntityCollectionBase<EntDbFlightIdMap> _flightIdMaps;
        readonly EntDbFlightIdMap _currentRow = new EntDbFlightIdMap();

        public FlightIdMapViewModel(string basicDataName)
        {
            UserDefinedLayouts = DlUserDefinedLayout.GetFilteredView(basicDataName);
            EntityType = typeof(EntDbFlightIdMap);
        }

        public override object CurrentRow
        {
            get
            {
                return _currentRow;
            }

            set
            {
                EntDbFlightIdMap newFlightIdMap = (EntDbFlightIdMap)value;
                newFlightIdMap.CloneEntityAttributes(_currentRow);

                OnPropertyChanged("CurrentRow");
            }
        }

        public override void LoadLayout(EntDbUserDefinedLayout userLayout)
        {
            if (LoadAllData && !IsAllDataLoaded)
            {
                _flightIdMaps = DlBasicData.LoadFlightIdMaps();                
                IsAllDataLoaded = true;
            }

            if (!IsAllDataLoaded)
            {
                //Load the data base on layout
            }

            //Set items source
            ItemsSource = _flightIdMaps;
            DataContext = _flightIdMaps.DataContext;    
        }

        protected override bool IsEntityUnique(BaseEntity entity, PropertyInfo propertyInfo)
        {
            bool bUnique = true;

            EntDbFlightIdMap newFlightIdMap = (EntDbFlightIdMap)entity;
            object objValue = propertyInfo.GetValue(newFlightIdMap, null);
            if (objValue == null)
            {
                bUnique = false;
            }
            else
            {
                if (entity.Urno <= 0) //new row
                {
                    bUnique = !_flightIdMaps.Any(item => objValue.Equals(propertyInfo.GetValue(item, null)));
                }
                else
                {
                    IEnumerable<EntDbFlightIdMap> query = _flightIdMaps.Where(item => objValue.Equals(propertyInfo.GetValue(item, null)));
                    foreach (EntDbFlightIdMap flightIdMap in query)
                    {
                        if (!flightIdMap.Equals(newFlightIdMap))
                        {
                            bUnique = false;
                            break;
                        }
                    }
                }
            }

            return bUnique;
        }

        protected override void SaveEntity(BaseEntity currentEntity, BaseEntity newEntity)
        {
            EntDbFlightIdMap currentFlightIdMap = (EntDbFlightIdMap)currentEntity;
            EntDbFlightIdMap newFlightIdMap = (EntDbFlightIdMap)newEntity;
            newFlightIdMap.CopyEntityAttributes(currentFlightIdMap, true);
            if (currentFlightIdMap.Urno <= 0) //it is a new row
            {
                currentFlightIdMap.SetEntityState(Entity.EntityState.Added);
                currentFlightIdMap.Urno = (int)_flightIdMaps.DataContext.EntityAdapter.GetUniqueId();
            }
            else
            {
                currentFlightIdMap.SetEntityState(Entity.EntityState.Modified);
            }            
            _flightIdMaps.Save(currentFlightIdMap);
        }

        protected override void RemoveEntity(BaseEntity entity)
        {
            EntDbFlightIdMap flightIdMap = (EntDbFlightIdMap)entity;
            _flightIdMaps.Remove(flightIdMap);
        }

        protected override void InsertEntity(object param)
        {
            EntDbFlightIdMap flightIdMap;
            if (param == null)
                flightIdMap = new EntDbFlightIdMap() { ArrivalDepartureId = "A", DataSourceName = "ATC" };
            else
            {
                EntDbFlightIdMap currentFlightIdMap = (EntDbFlightIdMap)param;
                flightIdMap = currentFlightIdMap.CloneEntity(true);
            }

            _flightIdMaps.Insert(0, flightIdMap);
        }

        protected override void DeleteEntity(BaseEntity entity)
        {
            EntDbFlightIdMap flightIdMap = (EntDbFlightIdMap)entity;
            flightIdMap.SetEntityState(Entity.EntityState.Deleted);
            _flightIdMaps.Remove(flightIdMap);
            _flightIdMaps.Save(flightIdMap);
        }

    }
}