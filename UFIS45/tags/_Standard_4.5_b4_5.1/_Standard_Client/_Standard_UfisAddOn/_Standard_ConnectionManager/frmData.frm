VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Begin VB.Form frmData 
   Caption         =   "Form1"
   ClientHeight    =   9960
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   16935
   Icon            =   "frmData.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9960
   ScaleWidth      =   16935
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB tabSubLOA 
      Height          =   1095
      Left            =   11760
      TabIndex        =   72
      Top             =   8760
      Width           =   4935
      _Version        =   65536
      _ExtentX        =   8705
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1005
      Index           =   10
      Left            =   13455
      TabIndex        =   58
      Tag             =   "SRLTAB;URNO,FLNU,AREA,READ,ADID,FLNO,STIM,ETIM,TYPE,TIME,TEXT,LINO,CDAT,LSTU,USEU,USEC;"
      Top             =   3735
      Width           =   3210
      _Version        =   65536
      _ExtentX        =   5662
      _ExtentY        =   1773
      _StockProps     =   64
   End
   Begin AATLOGINLib.AatLogin ULogin 
      Height          =   825
      Left            =   10170
      TabIndex        =   51
      Top             =   8145
      Width           =   1140
      _Version        =   65536
      _ExtentX        =   2011
      _ExtentY        =   1455
      _StockProps     =   0
   End
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   4545
      Top             =   8775
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin TABLib.TAB tabHold 
      Height          =   1005
      Left            =   10860
      TabIndex        =   49
      Top             =   3750
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   1773
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdFindDep 
      Caption         =   "cmdFindDep"
      Height          =   255
      Left            =   3660
      TabIndex        =   48
      Top             =   3480
      Width           =   1395
   End
   Begin VB.CommandButton cmdGetNextUrno 
      Caption         =   "cmdGetNextUrno"
      Height          =   315
      Left            =   8280
      TabIndex        =   40
      Top             =   8865
      Width           =   1395
   End
   Begin TABLib.TAB tabUrnos 
      Height          =   1395
      Left            =   6420
      TabIndex        =   38
      Top             =   8325
      Width           =   1695
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   2461
      _StockProps     =   64
   End
   Begin TABLib.TAB tabCfg 
      Height          =   975
      Left            =   5280
      TabIndex        =   36
      Top             =   3780
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin TABLib.TAB tabReread 
      Height          =   975
      Left            =   120
      TabIndex        =   34
      Top             =   3780
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Left            =   5280
      TabIndex        =   23
      Text            =   "6"
      Top             =   8205
      Width           =   495
   End
   Begin TABLib.TAB tabCompressedView 
      Height          =   1035
      Left            =   120
      TabIndex        =   17
      Top             =   7005
      Width           =   5355
      _Version        =   65536
      _ExtentX        =   9446
      _ExtentY        =   1826
      _StockProps     =   64
   End
   Begin TABLib.TAB tabCurrentBar 
      Height          =   1035
      Left            =   5640
      TabIndex        =   16
      Top             =   7005
      Width           =   4155
      _Version        =   65536
      _ExtentX        =   7329
      _ExtentY        =   1826
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdSearchInRotations 
      Caption         =   "Search"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1620
      Width           =   1275
   End
   Begin VB.TextBox txtSearch 
      Height          =   285
      Left            =   1440
      TabIndex        =   13
      Top             =   1620
      Width           =   1755
   End
   Begin UFISCOMLib.UfisCom aUfis 
      Left            =   3330
      Top             =   8595
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   767
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdWriteTabsToFile 
      Caption         =   "WriteTabsToFile"
      Height          =   435
      Left            =   180
      TabIndex        =   12
      Top             =   8625
      Width           =   1575
   End
   Begin VB.TextBox txtDateFrom 
      Height          =   315
      Left            =   1320
      TabIndex        =   10
      Top             =   8205
      Width           =   1635
   End
   Begin VB.TextBox txtDateTo 
      Height          =   315
      Left            =   3300
      TabIndex        =   9
      Top             =   8205
      Width           =   1635
   End
   Begin VB.CommandButton cmdChangeDate 
      Caption         =   "&Change Date"
      Height          =   315
      Left            =   180
      TabIndex        =   8
      Top             =   8205
      Width           =   1155
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   1
      Left            =   5220
      TabIndex        =   2
      Tag             =   "PSTTAB;URNO,PNAM; "
      Top             =   300
      Width           =   2115
      _Version        =   65536
      _ExtentX        =   3731
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   2
      Left            =   7440
      TabIndex        =   5
      Tag             =   "ROTATIONS;URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,LINE;"
      Top             =   300
      Width           =   1635
      _Version        =   65536
      _ExtentX        =   2884
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   0
      Left            =   120
      TabIndex        =   15
      Tag             =   $"frmData.frx":030A
      Top             =   270
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   3
      Left            =   120
      TabIndex        =   20
      Tag             =   "LOATAB;URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,ADDI,HOPO;"
      Top             =   2160
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabCorrespondingBars 
      Height          =   1035
      Left            =   9840
      TabIndex        =   25
      Top             =   7080
      Width           =   4155
      _Version        =   65536
      _ExtentX        =   7329
      _ExtentY        =   1826
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   4
      Left            =   5280
      TabIndex        =   27
      Tag             =   "ALTTAB;ALC2,ALC3,PRFL;ORDER BY ALC2"
      Top             =   2160
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   5
      Left            =   7860
      TabIndex        =   30
      Tag             =   "CONTAB;URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,CSAM,PSAM,MSAM,CDAT,LSTU,USEC,USEU;ORDER BY ALC2,ALC3,CDAT"
      Top             =   2160
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   6
      Left            =   10560
      TabIndex        =   42
      Tag             =   "LOATABM;URNO,FLNU,FLNO,DSSN,TYPE,STYP,VALU,IDNT,ADDI; "
      Top             =   2160
      Width           =   3075
      _Version        =   65536
      _ExtentX        =   5424
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   7
      Left            =   13680
      TabIndex        =   45
      Tag             =   "LOATABC;URNO,FLNU,FLNO,DSSN,TYPE,STYP,VALU,IDNT,ADDI; "
      Top             =   2160
      Width           =   3075
      _Version        =   65536
      _ExtentX        =   5424
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   8
      Left            =   12645
      TabIndex        =   52
      Tag             =   "ACTTAB;URNO,ACT3,ACT5,SEAF,SEAB,SEAE;ORDER BY ACT3"
      Top             =   315
      Width           =   3435
      _Version        =   65536
      _ExtentX        =   6059
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   9
      Left            =   9180
      TabIndex        =   55
      Tag             =   "ACRTAB;URNO,REGN,SEAF,SEAB,SEAE;"
      Top             =   315
      Width           =   3435
      _Version        =   65536
      _ExtentX        =   6059
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   11
      Left            =   135
      TabIndex        =   61
      Tag             =   "GATTAB;URNO,GNAM;ORDER BY GNAM"
      Top             =   5010
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1005
      Index           =   13
      Left            =   13440
      TabIndex        =   64
      Tag             =   $"frmData.frx":0406
      Top             =   5160
      Width           =   3210
      _Version        =   65536
      _ExtentX        =   5662
      _ExtentY        =   1773
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   1275
      Index           =   12
      Left            =   3000
      TabIndex        =   67
      Tag             =   "AVLTAB;URNO,RURN,FLNO,ADID,DIFF,DIFC,DIFU,DIFY; "
      Top             =   5040
      Width           =   2535
      _Version        =   65536
      _ExtentX        =   4471
      _ExtentY        =   2249
      _StockProps     =   64
   End
   Begin TABLib.TAB tabSRL 
      Height          =   1215
      Left            =   5760
      TabIndex        =   70
      Top             =   5040
      Visible         =   0   'False
      Width           =   2295
      _Version        =   65536
      _ExtentX        =   4048
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Subset of LOATAB (ALTEA)"
      Height          =   255
      Index           =   14
      Left            =   11760
      TabIndex        =   71
      Top             =   8400
      Width           =   2835
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   12
      Left            =   3000
      TabIndex        =   69
      Top             =   4800
      Width           =   555
   End
   Begin VB.Label lblRecCount 
      Caption         =   "AVLTAB"
      Height          =   255
      Index           =   12
      Left            =   3600
      TabIndex        =   68
      Top             =   4800
      Width           =   2055
   End
   Begin VB.Label lblRecCount 
      Caption         =   "CFITAB"
      Height          =   255
      Index           =   13
      Left            =   14400
      TabIndex        =   66
      Top             =   4800
      Width           =   2055
   End
   Begin VB.Label lblTable 
      Caption         =   "Label13"
      Height          =   240
      Index           =   13
      Left            =   13440
      TabIndex        =   65
      Top             =   4800
      Width           =   1005
   End
   Begin VB.Label lblRecCount 
      Caption         =   "GATTAB"
      Height          =   255
      Index           =   11
      Left            =   615
      TabIndex        =   63
      Top             =   4770
      Width           =   2055
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   11
      Left            =   135
      TabIndex        =   62
      Top             =   4770
      Width           =   435
   End
   Begin VB.Label lblRecCount 
      Caption         =   "SRLTAB"
      Height          =   195
      Index           =   10
      Left            =   14490
      TabIndex        =   60
      Top             =   3510
      Width           =   2175
   End
   Begin VB.Label lblTable 
      Caption         =   "Label11"
      Height          =   240
      Index           =   10
      Left            =   13455
      TabIndex        =   59
      Top             =   3510
      Width           =   1005
   End
   Begin VB.Label lblRecCount 
      Caption         =   "ACTRTAB"
      Height          =   255
      Index           =   9
      Left            =   9660
      TabIndex        =   57
      Top             =   90
      Width           =   2775
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   9
      Left            =   9180
      TabIndex        =   56
      Top             =   90
      Width           =   435
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   8
      Left            =   12645
      TabIndex        =   54
      Top             =   90
      Width           =   435
   End
   Begin VB.Label lblRecCount 
      Caption         =   "ACTTAB"
      Height          =   255
      Index           =   8
      Left            =   13125
      TabIndex        =   53
      Top             =   90
      Width           =   2910
   End
   Begin VB.Label Label10 
      Caption         =   "Hold-Flights"
      Height          =   255
      Left            =   10860
      TabIndex        =   50
      Top             =   3510
      Width           =   2535
   End
   Begin VB.Label lblRecCount 
      Caption         =   "LOATAB Cargo"
      Height          =   255
      Index           =   7
      Left            =   15180
      TabIndex        =   47
      Top             =   1920
      Width           =   1575
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   7
      Left            =   13680
      TabIndex        =   46
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Label lblRecCount 
      Caption         =   "LOATAB Mail"
      Height          =   255
      Index           =   6
      Left            =   12060
      TabIndex        =   44
      Top             =   1920
      Width           =   1575
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   6
      Left            =   10560
      TabIndex        =   43
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Label lblUrno 
      Caption         =   "Label10"
      Height          =   315
      Left            =   8280
      TabIndex        =   41
      Top             =   9285
      Width           =   1395
   End
   Begin VB.Label Label9 
      Caption         =   "Next Urno"
      Height          =   195
      Left            =   6420
      TabIndex        =   39
      Top             =   8145
      Width           =   1695
   End
   Begin VB.Label Label8 
      Caption         =   "HUBMAN.cfg"
      Height          =   255
      Left            =   5280
      TabIndex        =   37
      Top             =   3540
      Width           =   2535
   End
   Begin VB.Label Label7 
      Caption         =   "tmp for reread of data"
      Height          =   315
      Left            =   120
      TabIndex        =   35
      Top             =   3480
      Width           =   3435
   End
   Begin VB.Label Label6 
      BackColor       =   &H80000008&
      Caption         =   "For internal use only"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   255
      Left            =   0
      TabIndex        =   33
      Top             =   6345
      Width           =   17415
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   5
      Left            =   7860
      TabIndex        =   32
      Top             =   1920
      Width           =   435
   End
   Begin VB.Label lblRecCount 
      Caption         =   "CONTAB"
      Height          =   255
      Index           =   5
      Left            =   8340
      TabIndex        =   31
      Top             =   1920
      Width           =   2055
   End
   Begin VB.Label lblRecCount 
      Caption         =   "ALTTAB"
      Height          =   255
      Index           =   4
      Left            =   5760
      TabIndex        =   29
      Top             =   1920
      Width           =   2055
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   4
      Left            =   5280
      TabIndex        =   28
      Top             =   1920
      Width           =   435
   End
   Begin VB.Label Label4 
      Caption         =   "Corresponding Bars"
      Height          =   315
      Left            =   9900
      TabIndex        =   26
      Top             =   6705
      Width           =   4035
   End
   Begin VB.Label Label3 
      Caption         =   "do"
      Height          =   195
      Left            =   4980
      TabIndex        =   24
      Top             =   8265
      Width           =   255
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   22
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Label lblRecCount 
      Caption         =   "LOATAB"
      Height          =   255
      Index           =   3
      Left            =   1620
      TabIndex        =   21
      Top             =   1920
      Width           =   2835
   End
   Begin VB.Label Label2 
      Caption         =   "Current Bar"
      Height          =   315
      Left            =   5700
      TabIndex        =   19
      Top             =   6705
      Width           =   4035
   End
   Begin VB.Label Label1 
      Caption         =   "For Compressed View"
      Height          =   315
      Left            =   120
      TabIndex        =   18
      Top             =   6705
      Width           =   5355
   End
   Begin VB.Label Label5 
      Caption         =   "  to"
      Height          =   195
      Left            =   2940
      TabIndex        =   11
      Top             =   8265
      Width           =   375
   End
   Begin VB.Label lblTable 
      Caption         =   "Gantt-Flights"
      Height          =   255
      Index           =   2
      Left            =   7440
      TabIndex        =   7
      Top             =   60
      Width           =   870
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Label1"
      Height          =   255
      Index           =   2
      Left            =   8370
      TabIndex        =   6
      Top             =   45
      Width           =   720
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   1
      Left            =   7320
      TabIndex        =   4
      Top             =   60
      Width           =   255
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Label1"
      Height          =   255
      Index           =   1
      Left            =   5520
      TabIndex        =   3
      Top             =   60
      Width           =   1815
   End
   Begin VB.Label lblRecCount 
      Caption         =   "AFTTAB"
      Height          =   255
      Index           =   0
      Left            =   1620
      TabIndex        =   1
      Top             =   60
      Width           =   2835
   End
   Begin VB.Label lblTable 
      Caption         =   "Label1"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   1455
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public sorted As Boolean
Public FullScreen As Boolean

'igu on 27 Jul 2010
'run the long query with a lot of items in WHERE-IN clause
Private Sub TabRunQueries(TabObject As TABLib.Tab, ByVal DBTable As String, ByVal DBFields As String, _
ByVal FieldName As String, colValues As Collection, Optional ByVal ExtraWhereClause As String = "")
    Dim strWhere As String
    Dim i As Integer
    
    For i = 1 To colValues.count
        strWhere = "WHERE " & FieldName & " IN (" & colValues.Item(i) & ")" & _
            " " & ExtraWhereClause
            
        TabObject.CedaAction "RT", DBTable, DBFields, "", strWhere
    Next i
End Sub

'igu on 27 Jul 2010
'generate value list collection for items in WHERE-IN clause
Private Function GenerateValueListCollection(TabObject As TABLib.Tab, _
ByVal FieldName As String, Optional ByVal Prefix As String = "", Optional Suffix As String = "", _
Optional ByVal MaxNumOfValuesPerClause As Long = 300) As Collection
    
    Dim intCount As Integer
    Dim strValueList As String
    Dim i As Integer, j As Integer
    Dim colValues As Collection
    Dim intCollCount As Integer
    Dim intStart As Integer, intEnd As Integer
    
    Set colValues = New Collection
    
    intCount = TabObject.GetLineCount
    
    intCollCount = Int(intCount / MaxNumOfValuesPerClause) + 1
    For i = 1 To intCollCount
        intStart = MaxNumOfValuesPerClause * (i - 1)
        intEnd = intStart + (MaxNumOfValuesPerClause - 1)
        If intEnd > intCount - 1 Then
            intEnd = intCount - 1
        End If
    
        strValueList = ""
        For j = intStart To intEnd
            strValueList = strValueList & "," & Prefix & TabObject.GetFieldValue(j, FieldName) & Suffix
        Next j
        strValueList = Mid(strValueList, 2)
        
        colValues.Add strValueList
    Next i
    
    Set GenerateValueListCollection = colValues
End Function

Public Sub LoadDataCONTAB()
    tabData(5).ResetContent

    tabData(5).CedaAction "RT", "CONTAB", "URNO,ALC2,ALC3,OAL2,OAL3,PCON,MCON,CCON,CSAM,PSAM,MSAM,CDAT,LSTU,USEC,USEU", "", ""
    tabData(5).SetInternalLineBuffer True
    tabData(5).IndexCreate "URNO", 0
    tabData(5).IndexCreate "ALC2", 1
    tabData(5).IndexCreate "ALC3", 2
    tabData(5).Sort "1,2", True, True
End Sub


Public Sub LoadData()
    Dim myTab As TABLib.Tab
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim arrFields() As String
    Dim ilCount As Integer
    Dim strWhere As String
    Dim i As Long
    Dim fName As String
    Dim strOldValue As String
    Dim ilIdx As Long
    
    Dim colUrnos As Collection  'igu on 27 Jul 2010
    
    If strShowHiddenData = "" Then
        Me.Top = 99999
        Me.Hide
    Else
        Me.Show
    End If
    
    FullScreen = False
    frmMain.MainStatusBar.Visible = True 'Just to start the BcProxy but do nothing in the form
    SetServerParameters
    aUfis.CallServer "SBC", "HUBMNG", "1/6", "1/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "HUBMNG", "2/6", "2/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "HUBMNG", "3/6", "3/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "HUBMNG", "4/6", "4/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "HUBMNG", "5/6", "5/6", "", 240
    SetServerParameters
    aUfis.CallServer "SBC", "HUBMNG", "6/6", "6/6", "", 240
    ilTabCnt = 0
    If readFromFile = False Then
        For Each myTab In tabData
            myTab.ResetContent
            'DoEvents
            arr = Split(myTab.Tag, ";")
            If UBound(arr) = 2 Then
                arrFields = Split(arr(1), ",")
                strWhere = arr(2)
                myTab.ResetContent
                myTab.AutoSizeByHeader = True
                myTab.HeaderString = ""
                myTab.HeaderLengthString = ""
                myTab.HeaderString = arr(1)
                myTab.EnableInlineEdit True
                myTab.CedaServerName = strServer
                myTab.CedaPort = "3357"
                myTab.CedaHopo = strHopo
                myTab.CedaCurrentApplication = "HUB-Manager" & "," & GetApplVersion(True)
                myTab.CedaTabext = "TAB"
                myTab.CedaUser = strCurrentUser
                myTab.CedaWorkstation = aUfis.WorkStation
                myTab.CedaSendTimeout = "3"
                myTab.CedaReceiveTimeout = "240"
                myTab.CedaRecordSeparator = Chr(10)
                myTab.CedaIdentifier = "HUB-Manager"
                'myTab.ShowHorzScroller True
                myTab.ShowHorzScroller False
                myTab.EnableHeaderSizing True
                For i = 0 To UBound(arrFields)
                    If i = 0 Then
                        myTab.HeaderLengthString = "80,"
                    Else
                        myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
                    End If
                Next i
                myTab.LogicalFieldList = arr(1)
                frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                frmStartup.txtCurrentStatus.Refresh
'Special treatment for AFTTAB
                If arr(0) = "AFTTAB" Then
                    'strWhere = "WHERE (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X')"
                    'strWhere = "WHERE (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X') AND DES3 = 'SIN' OR (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X') AND ORG3= 'SIN'"
                    'strWhere = "WHERE (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X') AND ADID IN ('A','B') OR (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X') AND ADID IN ('D','B')" 'igu on 10/01/2012
                    strWhere = "WHERE (((TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND ADID IN ('A','B')) OR " + _
                        "((TIFD BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND ADID IN ('D','B'))) AND " + _
                        "FTYP IN ('S', 'O', 'X')" 'igu on 10/01/2012
                End If
'SRLTAB Handling
                If arr(0) = "SRLTAB" Then
                    myTab.ColumnWidthString = "10,10,1,4,1,13,14,14,5,1,11,12,14,14,32,32"
'                    strWhere = "WHERE FLNU IN ("
'                    For i = 0 To tabData(0).GetLineCount - 1
'                        If i Mod 300 = 0 Then
'                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND USEC='HUBMGR'"
'                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
'                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
'                            frmStartup.txtCurrentStatus.Refresh
'                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
'                            frmStartup.txtDoneStatus.Refresh
'                            strWhere = "WHERE FLNU IN ("
'                        Else
'                            If (i) = tabData(0).GetLineCount - 1 Then
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND USEC='HUBMGR'"
'                            Else
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
'                            End If
'                        End If
'                    Next i
'                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND USEC='HUBMGR'") 'igu on 27 Jul 2010
                    
                    frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If
'kkh on 16/07/2008 P2 Store Decision into CFITAB
'CFITAB Handling
                If arr(0) = "CFITAB" Then
                    'myTab.ColumnWidthString = "10,10,10,10,10,10,10,10,10,10,11"
                    myTab.ColumnWidthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11"
'                    strWhere = "WHERE AURN IN ("
'                    For i = 0 To tabData(0).GetLineCount - 1
'                        If i Mod 300 = 0 Then
'                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND TYPE='HUB'"
'                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
'                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
'                            frmStartup.txtCurrentStatus.Refresh
'                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
'                            frmStartup.txtDoneStatus.Refresh
'                            strWhere = "WHERE AURN IN ("
'                        Else
'                            If (i) = tabData(0).GetLineCount - 1 Then
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ") AND TYPE='HUB'"
'                            Else
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
'                            End If
'                        End If
'                    Next i
'                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "AURN", colUrnos, _
                        "AND TYPE='HUB'") 'igu on 27 Jul 2010
                        
                    frmStartup.txtCurrentStatus = "Loading  " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If
'Customize for GOCC ON 04/03/2008
'AVLTAB Handling
                If (arr(0) = "AVLTAB") Then
                    myTab.ColumnWidthString = "10,10,11,11,11,11,11,11,11,11"
'                    strWhere = "WHERE RURN IN ("
'                    For i = 0 To tabData(0).GetLineCount - 1
'                        If i Mod 300 = 0 Then
'                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
'                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
'                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
'                            frmStartup.txtCurrentStatus.Refresh
'                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
'                            frmStartup.txtDoneStatus.Refresh
'                            strWhere = "WHERE RURN IN ("
'                        Else
'                            If (i) = tabData(0).GetLineCount - 1 Then
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")"
'                            Else
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
'                            End If
'                        End If
'                    Next i
'                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "RURN", colUrnos) 'igu on 27 Jul 2010
                        
                    frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If

'Special treatment for LOADTAB
                If arr(0) = "LOATAB" Then
                    tabData(3).ColumnWidthString = "10,10,10,10,10,10,10,10,10,10,10,10,10"
'                    strWhere = "WHERE FLNU IN ("
'                    For i = 0 To tabData(0).GetLineCount - 1
'                        If i Mod 300 = 0 Then
'                            'strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                                       " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " + _
'                                       " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')"
'                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                            " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F','U') " + _
'                            " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T') AND FLNO <> ' ' order by APC3, ADDI"
'
'                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
'                            frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
'                            frmStartup.txtCurrentStatus.Refresh
'                            frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
'                            frmStartup.txtDoneStatus.Refresh
'                            strWhere = "WHERE FLNU IN ("
'                        Else
'                            If (i) = tabData(0).GetLineCount - 1 Then
'                                'strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                                           " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " + _
'                                           " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')"
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                                           " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F','U') " + _
'                                           " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T') AND FLNO <> ' ' order by APC3, ADDI"
'                            Else
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
'                            End If
'                        End If
'                    Next i
'                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    'Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " & _
                        " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')")  'igu on 27 Jul 2010 'igu on 23/08/2011: ALTEA
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN IN ('PTM','KRI','ALT') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " & _
                        " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')") 'igu on 23/08/2011: ALTEA
                        
                    frmStartup.txtCurrentStatus = "Loading " & arr(0) & " ..."
                    frmStartup.txtCurrentStatus.Refresh
                    frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                    frmStartup.txtDoneStatus.Refresh
                End If
'LOATAB with Mail
                If arr(0) = "LOATABM" Then
                    arr(0) = "LOATAB"
'                    strWhere = "WHERE FLNU IN ("
'                    For i = 0 To tabData(0).GetLineCount - 1
'                        If i Mod 300 = 0 Then
'                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                                       " AND DSSN='HUB' AND TYPE='LOA' AND STYP='M' "
'                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
'                            strWhere = "WHERE FLNU IN ("
'                        Else
'                            If (i) = tabData(0).GetLineCount - 1 Then
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                                           " AND DSSN='HUB' AND TYPE='LOA' AND STYP='M'"
'                            Else
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
'                            End If
'                        End If
'                    Next i
'                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN='HUB' AND TYPE='LOA' AND STYP='M'") 'igu on 27 Jul 2010
                End If
'LOATAB with Cargo
                If arr(0) = "LOATABC" Then
                    arr(0) = "LOATAB"
'                    strWhere = "WHERE FLNU IN ("
'                    For i = 0 To tabData(0).GetLineCount - 1
'                        If i Mod 300 = 0 Then
'                            strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                                       " AND DSSN='HUB' AND TYPE='LOA' AND STYP='C'"
'                            myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
'                            strWhere = "WHERE FLNU IN ("
'                        Else
'                            If (i) = tabData(0).GetLineCount - 1 Then
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ")" + _
'                                           " AND DSSN='HUB' AND TYPE='LOA' AND STYP='C'"
'                            Else
'                                strWhere = strWhere + tabData(0).GetFieldValue(i, "URNO") + ","
'                            End If
'                        End If
'                    Next i
'                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                    Call TabRunQueries(myTab, arr(0), arr(1), "FLNU", colUrnos, _
                        "AND DSSN='HUB' AND TYPE='LOA' AND STYP='C'") 'igu on 27 Jul 2010
                End If
                'kkh on 16/07/2008 P2 Store Decision into CFITAB
                If arr(0) <> "ROTATIONS" And arr(0) <> "LOATAB" And _
                   arr(0) <> "LOATABM" And arr(0) <> "LOATABC" And arr(0) <> "AVLTAB" And arr(0) <> "CFITAB" Then
                    myTab.CedaAction "RT", arr(0), arr(1), "", strWhere
                End If
                myTab.RedrawTab
                lblRecCount(ilTabCnt) = "Table: " + arr(0) + "  Records: " + CStr(myTab.GetLineCount)
                
                ilTabCnt = ilTabCnt + 1
                myTab.AutoSizeByHeader = True
                myTab.AutoSizeColumns
                myTab.myTag = myTab.Left & "," & myTab.Top & "," & myTab.Height & "," & myTab.Width
                frmStartup.txtDoneStatus = arr(0) & " : " & CStr(myTab.GetLineCount) & " records loaded"
                frmStartup.txtDoneStatus.Refresh
                
                'igu on 27 Jul 2010
                'generate the urnos collection
                '-----------------------------------------
                If colUrnos Is Nothing Then
                    Set colUrnos = GenerateValueListCollection(tabData(0), "URNO")
                End If
                '-----------------------------------------
            End If
        Next
    Else
        For Each myTab In tabData
            arr = Split(myTab.Tag, ";")
            If UBound(arr) = 2 Then
                arrFields = Split(arr(1), ",")
                'fName = "C:\Ufis\Appl\CONNMGR_" + arr(0) + ".dat"
                fName = UFIS_APPL & "\CONNMGR_" + arr(0) + ".dat"
                myTab.ResetContent
                myTab.AutoSizeByHeader = True
                myTab.HeaderString = ""
                myTab.HeaderLengthString = ""
                myTab.HeaderString = arr(1)
                myTab.EnableInlineEdit True
                'myTab.ShowHorzScroller True
                myTab.ShowHorzScroller False
                myTab.EnableHeaderSizing True
                For i = 0 To UBound(arrFields)
                    If i = 0 Then
                        myTab.HeaderLengthString = "80,"
                    Else
                        myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
                    End If
                Next i
                myTab.LogicalFieldList = arr(1)
                myTab.readFromFile fName
                myTab.RedrawTab
                lblRecCount(ilTabCnt) = "Table: " + arr(0) + "  Records: " + CStr(myTab.GetLineCount)
                ilTabCnt = ilTabCnt + 1
                myTab.AutoSizeByHeader = True
                myTab.AutoSizeColumns
                myTab.myTag = myTab.Left & "," & myTab.Top & "," & myTab.Height & "," & myTab.Width
            End If
        Next
    End If
        
    frmStartup.txtCurrentStatus = "Initialize the data ..."
    frmStartup.txtCurrentStatus.Refresh
    frmStartup.txtDoneStatus = "All data loaded"
    
    tabCfg.ResetContent
    tabCfg.HeaderLengthString = "100,100"
    tabCfg.HeaderString = "FIELD,VALUE"
    tabCfg.LogicalFieldList = tabCfg.HeaderString
    tabCfg.EnableHeaderSizing True
    tabCfg.AutoSizeByHeader = True
    'tabCfg.readFromFile "C:\Ufis\Appl\HUBMANAGER.cfg"
    tabCfg.readFromFile UFIS_APPL & "\HUBMANAGER.cfg"
    tabCfg.AutoSizeColumns
    
    tabHold.ResetContent
    tabHold.HeaderLengthString = "100,100"
    tabHold.HeaderString = "URNO,STATUS"
    tabHold.LogicalFieldList = tabHold.HeaderString
    tabHold.EnableHeaderSizing True
    tabHold.AutoSizeByHeader = True
    'tabHold.readFromFile "C:\Ufis\Appl\HUB_FLT_HOLD.cfg"
    tabHold.readFromFile UFIS_APPL & "\HUB_FLT_HOLD.cfg"
    tabHold.AutoSizeColumns
    tabHold.SetInternalLineBuffer True
    tabHold.IndexCreate "URNO", 0
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights, take out the sort
    tabData(0).Sort "1,3", True, True
    tabData(1).Sort "1", True, True
    tabData(2).ResetContent
    tabData(0).IndexCreate "URNO", 0
    tabData(0).IndexCreate "RKEY", 1
    tabData(0).IndexCreate "FLNO", 4
    ilIdx = GetRealItemNo(tabData(0).LogicalFieldList, "DES3")
    tabData(0).IndexCreate "DES3", ilIdx
    'kkh 31/07/2008 VIAL
    tabData(0).IndexCreate "VIAL", 38
    tabData(0).SetInternalLineBuffer True
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights, take out the sort
    tabData(3).Sort "1,2,3", True, True
    'tabData(3).Sort "14,3", True, True
    tabData(3).IndexCreate "URNO", 0
    tabData(3).IndexCreate "FLNU", 1
    tabData(3).IndexCreate "RTAB", 12
    tabData(3).SetInternalLineBuffer True
    tabData(4).SetInternalLineBuffer True
    tabData(4).Sort "0,1", True, True
    For i = 0 To tabData(4).GetLineCount - 1
        tabData(4).SetFieldValues i, "PRFL", ""
    Next i
    tabData(4).IndexCreate "ALC2", 0
    tabData(4).IndexCreate "ALC3", 1
    tabUrnos.ResetContent
    tabUrnos.HeaderString = "URNO"
    tabUrnos.LogicalFieldList = "URNO"
    tabUrnos.HeaderLengthString = "100"
    tabUrnos.SetFieldSeparator Chr(15)
    
    
    tabReread.ResetContent
    tabReread.AutoSizeByHeader = True
    tabReread.HeaderString = ""
    tabReread.HeaderLengthString = ""
    tabReread.HeaderString = arr(1)
    tabReread.EnableInlineEdit True
    tabReread.CedaServerName = strServer
    tabReread.CedaPort = "3357"
    tabReread.CedaHopo = strHopo
    tabReread.CedaCurrentApplication = "HUB-Manager" & "," & GetApplVersion(True)
    tabReread.CedaTabext = "TAB"
    tabReread.CedaUser = strCurrentUser '"HUBMNG"
    tabReread.CedaWorkstation = ULogin.GetWorkStationName  'aUfis.GetWorkStationName ' "wks104"
    tabReread.CedaSendTimeout = "3"
    tabReread.CedaReceiveTimeout = "240"
    tabReread.CedaRecordSeparator = Chr(10)
    tabReread.CedaIdentifier = "HUB-Manager"
    'tabReread.ShowHorzScroller True
    tabReread.ShowHorzScroller False
    tabReread.EnableHeaderSizing True
    
    
    'MakeRotationData
    tabData(2).AutoSizeByHeader = True
    tabData(2).AutoSizeColumns
    tabCompressedView.myTag = tabCompressedView.Left & "," & tabCompressedView.Top & "," & tabCompressedView.Height & "," & tabCompressedView.Width
    tabData(5).SetInternalLineBuffer True
    tabData(5).IndexCreate "URNO", 0
    tabData(5).IndexCreate "ALC2", 1
    tabData(5).IndexCreate "ALC3", 2
    tabData(5).Sort "1,2", True, True
    
    tabData(8).IndexCreate "URNO", 0
    tabData(8).IndexCreate "ACT3", 1
    tabData(8).SetInternalLineBuffer True
    tabData(8).Sort "1", True, True
    tabData(9).IndexCreate "URNO", 0
    tabData(9).IndexCreate "REGN", 1
    tabData(9).Sort "1", True, True
    tabData(9).SetInternalLineBuffer True
    
    'make RFLD and RTAB in all TAB for LADTAB data empty
    For i = 0 To tabData(3).GetLineCount - 1
        tabData(3).SetFieldValues i, "RFLD,RTAB", ","
    Next i
    For i = 0 To tabData(6).GetLineCount - 1
        tabData(6).SetFieldValues i, "RFLD,RTAB", ","
    Next i
    For i = 0 To tabData(7).GetLineCount - 1
        tabData(7).SetFieldValues i, "RFLD,RTAB", ","
    Next i
    tabData(6).IndexCreate "URNO", 0
    tabData(7).IndexCreate "URNO", 0
    tabData(6).IndexCreate "FLNU", 1
    tabData(7).IndexCreate "FLNU", 1
    tabData(6).IndexCreate "IDNT", 7 ' Departure flight URNO is stored in this field
    tabData(7).IndexCreate "IDNT", 7
    tabData(6).SetInternalLineBuffer True
    tabData(7).SetInternalLineBuffer True
    
    'Special SRLTAB handling
    'Detect double FLNUs and remove them except of the last one, which is the newest
    tabData(10).Sort "1,12", False, True
    tabData(10).SetInternalLineBuffer True
    strOldValue = ""
    For i = tabData(10).GetLineCount - 1 To 0 Step -1
        If i - 1 >= 0 Then
            strOldValue = tabData(10).GetFieldValue(i, "FLNU")
            If strOldValue = tabData(10).GetFieldValue(i - 1, "FLNU") Then
                tabData(10).DeleteLine (i)
            End If
        End If
    Next
    tabData(10).IndexCreate "FLNU", 1
    
    'kkh AVLTAB
    tabData(12).IndexCreate "RURN", 1
                                         
    'kkh CFITAB
    tabData(13).IndexCreate "URNO", 0
    tabData(13).IndexCreate "AURN", 1
    tabData(13).IndexCreate "DURN", 2
    
    frmMain.InitConfigData
    If TimesInUTC = False Then
        frmStartup.txtCurrentStatus = "Converting UTC to local ..."
        ChageAftToLocal
    End If
End Sub

Public Sub MakeRotationData()
    Dim i As Long
    Dim strVal As String
    Dim strTmp As String
    Dim strLine As String
    Dim myDat As Date
    Dim mydat2 As Date
    Dim strNextRkey As String
    Dim strCurrRkey As String
    Dim llCurr As Long
    Dim sss As String
    Dim strNextAdid As String
    Dim strCurrAdid As String
    Dim ilStep As Integer
    
'AFTTAB;
    tabData(2).ResetContent
    tabData(2).HeaderString = "URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,FLTI_A,FLTI_D,VIAL_A,VIAL_D"
    tabData(2).LogicalFieldList = "URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,FLTI_A,FLTI_D,VIAL_A,VIAL_D"
    tabData(2).HeaderLengthString = "80,80,80,180,180,80,80,30,30,30,30,30,30,30,30,30,40,40,50,50"
    tabData(2).EnableHeaderSizing True
    'tabData(2).ShowHorzScroller True
    tabData(2).ShowHorzScroller False
    
    llCurr = 0
    While llCurr < tabData(0).GetLineCount - 1
    If 128928325 = tabData(0).GetFieldValue(llCurr, "URNO") Then
        strLine = ""
    End If
'  0    1    2    3    4   5    6    7     8    9    10  11   12   13   14   15   16  17    18   19   20    21  22   23   24   25   26   27
'URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES
'  0     1       2    3    4   5       6     7      8      9      10     11   12   13   14   15       16   17    18      19    20
'URNO_A,URNO_D,RKEY,TIFA,TIFD,FLNO_A,FLNO_D,FTYP_A,FTYP_D,ADID_A,ADID_D,PSTA,TISA,PSTD,TISD,ROTATION,ORG3,DES3,FLTI_A, FLTI_D,LINE
        strLine = tabData(0).LogicalFieldList
        strCurrRkey = tabData(0).GetFieldValue(llCurr, "RKEY")
        strCurrAdid = tabData(0).GetFieldValue(llCurr, "ADID")
        If (llCurr + 1) < tabData(0).GetLineCount - 1 Then
            strNextRkey = tabData(0).GetFieldValue(llCurr + 1, "RKEY")
            strNextAdid = tabData(0).GetFieldValue(llCurr + 1, "ADID")
        Else
            strNextRkey = ""
            strNextAdid = ""
        End If
        If strCurrRkey = strNextRkey Then
            'Is a rotation
            ilStep = 2
            If strCurrAdid = "A" And strNextAdid = "D" Then
                strLine = tabData(0).GetFieldValue(llCurr, "URNO") + "," + tabData(0).GetFieldValue(llCurr + 1, "URNO") + ","
                strLine = strLine + tabData(0).GetColumnValue(llCurr, 1) + ","
                strVal = tabData(0).GetFieldValue(llCurr, "TIFA")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strVal = tabData(0).GetFieldValue(llCurr + 1, "TIFD")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLNO") + "," + tabData(0).GetFieldValue(llCurr + 1, "FLNO") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FTYP") + "," + tabData(0).GetFieldValue(llCurr + 1, "FTYP") + ","
                strLine = strLine + strCurrAdid + "," + strNextAdid + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "PSTD") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "TISD") + ",R,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "DES3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FLTI") + ","
            End If
            If strCurrAdid = "D" And strNextAdid = "A" Then
                strLine = tabData(0).GetFieldValue(llCurr + 1, "URNO") + "," + tabData(0).GetFieldValue(llCurr, "URNO") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "RKEY") + ","
                strVal = tabData(0).GetFieldValue(llCurr + 1, "TIFA")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strVal = tabData(0).GetFieldValue(llCurr, "TIFD")
                If strVal <> "" Then
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FLNO") + "," + tabData(0).GetFieldValue(llCurr, "FLNO") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FTYP") + "," + tabData(0).GetFieldValue(llCurr, "FTYP") + ","
                strLine = strLine + strNextAdid + "," + strCurrAdid + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "PSTA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "TISA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTD") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISD") + ",R,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "DES3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr + 1, "FLTI") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ","
                
            End If
        Else
            ilStep = 1
            'Is a single flight and not rotation
            If strCurrAdid = "A" Then
                strLine = tabData(0).GetFieldValue(llCurr, "URNO") + "," + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "RKEY") + ","
                strVal = tabData(0).GetFieldValue(llCurr, "TIFA")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    strTmp = strVal + ","
                Else
                    strTmp = ","
                End If
                strLine = strLine + strTmp
                myDat = DateAdd("n", 30, myDat)
                strTmp = Format(myDat, "YYYYMMDDhhmmss") + ","
                strLine = strLine + strTmp
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLNO") + "," + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FTYP") + "," + ","
                strLine = strLine + strCurrAdid + "," + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTA") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISA") + ","
                strLine = strLine + ","
                strLine = strLine + ",A,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "DES3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ",,"
            End If
            If strCurrAdid = "D" Then
                strLine = "," 'URNO_A
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "URNO") + "," 'URNO_D
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "RKEY") + "," 'RKEY
                strVal = tabData(0).GetFieldValue(llCurr, "TIFD")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    mydat2 = DateAdd("n", -30, myDat)
                    strTmp = Format(mydat2, "YYYYMMDDhhmmss") + "," + Format(myDat, "YYYYMMDDhhmmss") + "," 'TIFA + TIFD
                Else
                    strTmp = ",,"
                End If
                strLine = strLine + strTmp 'TIFA + TIFD
                strLine = strLine + "," '                                               FLNO_A
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLNO") + "," '        FLNO_D
                strLine = strLine + "," '                                               FTYP_A
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FTYP") + "," '        FTYP_D
                strLine = strLine + "," '                                               ADID_A
                strLine = strLine + strCurrAdid + "," '                                 ADID_D
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "PSTD") + "," '        PSTD
                strLine = strLine + "," '                                               TISA
                'Erst mal alle positionen nach PSTA schieben egal ob departure
                strLine = strLine + "," '                                               PSTA
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "TISD") + ",D," '      TISD
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "ORG3") + ","
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "DES3") + ",,"
                strLine = strLine + tabData(0).GetFieldValue(llCurr, "FLTI") + ","
            End If
        End If
        llCurr = llCurr + ilStep
        strLine = strLine + "," + CStr(llCurr)
        tabData(2).InsertTextLine strLine, False
        strLine = ""
    Wend
    tabData(2).Sort "3,4", True, True
    tabData(2).RedrawTab
    lblRecCount(2) = Str(tabData(2).GetLineCount) + "Records"
End Sub


Public Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Long

    GetUTCOffset = 0
    aUfis.HomeAirport = strHopo
    aUfis.InitCom strServer, "CEDA"
    aUfis.Twe = strHopo + "," + strTableExt + "," + strAppl
    aUfis.SetCedaPerameters strAppl, strHopo, strTableExt
    SetServerParameters
    If aUfis.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = aUfis.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            i = count + 1
            GetUTCOffset = CSng(strUtcArr(1) / 60)
        End If
    Next i
    aUfis.CleanupCom
End Function
'------------------------------------------------------------------------------------
' change the dates to store the new lines for another date in the tabs corresponding
' file
'------------------------------------------------------------------------------------
Private Sub cmdChangeDate_Click()
    Dim i As Long
    Dim j As Long
    Dim datFrom As Date
    Dim datTo As Date
    Dim cnt As Long
    Dim cnt2 As Long
    Dim strTmp As String
    Dim strTmp2 As String
    Dim strDay As String
    Dim strNewDay As String
    Dim strColumns As String
    Dim strValue As String
    Dim currCol As Long
    Dim strOrig As String
    Dim arr() As String
    Dim strDest As String
    
    strColumns = "11,12,13,14,30,31,34,35"
    cnt2 = ItemCount(strColumns, ",")
    cnt = tabData(0).GetLineCount
    For i = 0 To cnt - 1
        For j = 0 To cnt2 - 1
            currCol = Val(GetRealItem(strColumns, CInt(j), ","))
            strTmp = tabData(0).GetColumnValue(i, currCol)
            strTmp = Replace(strTmp, txtDateFrom, txtDateTo)
            tabData(0).SetColumnValue i, currCol, strTmp
        Next j
    Next i

    cnt = tabData(2).GetLineCount
    For i = 0 To cnt - 1
        strTmp = tabData(2).GetColumnValue(i, 3)
        strTmp = Replace(strTmp, txtDateFrom, txtDateTo)
        tabData(2).SetColumnValue i, 3, strTmp
        strTmp = tabData(2).GetColumnValue(i, 4)
        strTmp = Replace(strTmp, txtDateFrom, txtDateTo)
        tabData(2).SetColumnValue i, 4, strTmp
    Next i
'********** Recalc the FLNO field of LOATAB => LH998/13 get /13 and increment it
    strDay = Mid(txtDateFrom, 7, 2)
    strNewDay = Mid(txtDateTo, 7, 2)
    cnt = tabData(3).GetLineCount - 1
    For i = 0 To cnt
        strTmp = tabData(3).GetFieldValue(i, "FLNO")
        arr = Split(strTmp, "/")
        If UBound(arr) = 1 Then
            strTmp = arr(1)
            If strTmp = strDay Then
                strTmp = strNewDay
                strValue = arr(0) & "/" & strTmp
                tabData(3).SetFieldValues i, "FLNO", strValue
            End If
        End If
    Next i
    tabData(3).Refresh
    datFrom = CedaDateToVb(txtDateFrom)
    datTo = CedaDateToVb(txtDateTo)
    datFrom = DateAdd("d", -1, datFrom)
    datTo = DateAdd("d", -1, datTo)
    txtDateFrom = Format(datFrom, "YYYYMMDD")
    txtDateTo = Format(datTo, "YYYYMMDD")
End Sub

Private Sub cmdFindDep_Click()
    frmMain.StoreDepartureUrnoForLoaTab tabData(3)
End Sub

Private Sub cmdGetNextUrno_Click()
    Dim strU As String
    strU = GetNextUrno
    lblUrno = strU
End Sub

Private Sub cmdSearchInRotations_Click()
    Dim i As Long
    Dim strLine As String
    Dim myStart As Long
    Dim blFound As Boolean
    
    blFound = False
    myStart = tabData(0).GetCurrentSelected + 1

    For i = myStart To tabData(0).GetLineCount - 1
        If InStr(1, tabData(0).GetLineValues(i), txtSearch) > 0 Then
            tabData(0).OnVScrollTo i
            tabData(0).SetCurrentSelection i
            i = tabData(0).GetLineCount
            blFound = True
        End If
    Next i
    If blFound = False Then
        tabData(0).SetCurrentSelection -1
        tabData(0).OnVScrollTo 0
    End If
End Sub

Private Sub cmdWriteTabsToFile_Click()
    Dim myTab As TABLib.Tab
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim fName As String
    
    For Each myTab In tabData
        arr = Split(myTab.Tag, ";")
        'fName = "C:\Ufis\Appl\CONNMGR_" + arr(0) + ".dat"
        fName = UFIS_APPL & "\CONNMGR_" + arr(0) + ".dat"
        myTab.WriteToFile fName, False
    Next

End Sub

Private Sub Form_Load()
    tabCurrentBar.ResetContent
    tabCurrentBar.HeaderString = "URNO,OLDCOLOR"
    tabCurrentBar.LogicalFieldList = "URNO,OLDCOLOR"
    tabCurrentBar.HeaderLengthString = "100,100"

    tabCorrespondingBars.ResetContent
    tabCorrespondingBars.HeaderString = "URNO,OLDCOLOR"
    tabCorrespondingBars.LogicalFieldList = "URNO,OLDCOLOR"
    tabCorrespondingBars.HeaderLengthString = "100,100"
    If strShowHiddenData = "" Then
        Me.Hide
    End If
    
    'igu on 28/06/2011
    If pblnCopyToSpecialRequirements Then
        tabSRL.CedaServerName = strServer
        tabSRL.CedaPort = "3357"
        tabSRL.CedaHopo = strHopo
        tabSRL.CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
        tabSRL.CedaTabext = "TAB"
        tabSRL.CedaUser = strCurrentUser
        tabSRL.CedaWorkstation = frmData.aUfis.WorkStation
        tabSRL.CedaSendTimeout = "3"
        tabSRL.CedaReceiveTimeout = "240"
        tabSRL.CedaRecordSeparator = Chr(10)
        tabSRL.CedaIdentifier = "HUB-Manager"
    End If
End Sub
Public Function GetNextUrno() As String
    Dim cnt As Long
    Dim ret As String
    Dim i As Long
    
    cnt = tabUrnos.GetLineCount
    If cnt = 0 Then
        SetServerParameters
        If aUfis.CallServer("GMU", "", "*", "200", "", "120") = 0 Then
            cnt = aUfis.GetBufferCount
            For i = 0 To cnt - 1
                tabUrnos.InsertBuffer aUfis.GetBufferLine(i), ","
            Next i
        End If
    End If
    ret = tabUrnos.GetFieldValue(0, "URNO")
    tabUrnos.DeleteLine 0
    tabUrnos.Refresh
    GetNextUrno = ret
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'tabCfg.WriteToFile "C:\Ufis\Appl\HUBMANAGER.cfg", False
    tabCfg.WriteToFile UFIS_APPL & "\HUBMANAGER.cfg", False
End Sub

Private Sub tabCompressedView_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim arr() As String
    If FullScreen = False Then
        tabCompressedView.Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
        FullScreen = True
        tabCompressedView.ZOrder
    Else
        arr = Split(tabCompressedView.myTag, ",")
        tabCompressedView.Move CInt(arr(0)), CInt(arr(1)), CInt(arr(3)), CInt(arr(2))
        FullScreen = False
    End If
    Me.Refresh
End Sub

Private Sub tabData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strFields As String
    Dim strData As String
    
    If LineNo = -1 Then
        tabData(Index).Sort CStr(ColNo), True, True
    End If
    If Index = 0 And LineNo > 0 Then
        strData = "KRIS," + tabData(0).GetFieldValue(LineNo, "URNO")
        frmMain.MyBC_OnBcReceive "", "", "", "URT", "TLXTAB", "", "", "", "", "TTYP,FLNU", strData, "0"
    End If
End Sub

Private Sub tabData_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim arr() As String
    If FullScreen = False Then
        tabData(Index).Move 0, 0, Me.ScaleWidth, Me.ScaleHeight
        FullScreen = True
        tabData(Index).ZOrder
    Else
        arr = Split(tabData(Index).myTag, ",")
        tabData(Index).Move CInt(arr(0)), CInt(arr(1)), CInt(arr(3)), CInt(arr(2))
        FullScreen = False
    End If
    Me.Refresh
End Sub

Public Sub ChangeAftToUTC()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    
    cnt = tabData(0).GetLineCount - 1
    fldCnt = ItemCount(strAftTimeFields, ",")
    For i = 0 To cnt
        For j = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(j), ",")
            strVal = tabData(0).GetFieldValue(i, strField)
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                If TimesInUTC = True Then
                    myDat = DateAdd("h", -UTCOffset, myDat)
                End If
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                tabData(0).SetFieldValues i, strField, strVal
            End If
        Next j
    Next i
    tabData(0).Refresh
End Sub
Public Sub ChageAftToLocal()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    
    cnt = tabData(0).GetLineCount - 1
    fldCnt = ItemCount(strAftTimeFields, ",")
    For i = 0 To cnt
        For j = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(j), ",")
            strVal = tabData(0).GetFieldValue(i, strField)
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                If TimesInUTC = False Then
                    myDat = DateAdd("h", UTCOffset, myDat)
                End If
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                tabData(0).SetFieldValues i, strField, strVal
            End If
        Next j
    Next i
    tabData(0).Refresh
End Sub

Public Function LoginProcedure() As Boolean
    Dim LoginAnsw As String
    Dim tmpRegStrg As String
    Dim strRet As String
    
    Set ULogin.UfisComCtrl = aUfis
    ULogin.ApplicationName = "HUB-Manager"
    tmpRegStrg = "HUB-Manager" & ","
    'FKTTAB:  SUBD,FUNC,FUAL,TYPE,STAT
    tmpRegStrg = tmpRegStrg & "InitModu,InitModu,Initialisieren (InitModu),B,-"
    
    'kkh on 09/07/2008 P2 Security Features for function/button
    'build the register string for BDPS-SEC
    tmpRegStrg = tmpRegStrg & ",General,Load,Load,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Now,Now,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Compressed_Flights,Compressed Flights,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Hourly_Flights,Hourly Flights,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Mail/Cargo,Mail/Cargo,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Plain_Gantt,Plain Gantt,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Filter,Filter,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Gate,Gate,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Connection_Times,Connection Times,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Hourly_Pax,Hourly Pax,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Setup,Setup,B,1"
    tmpRegStrg = tmpRegStrg & ",General,View_Log,View Log,B,1"
    
    tmpRegStrg = tmpRegStrg & ",General,Area_1,Area 1,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Area_2,Area 2,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Area_3,Area 3,B,1"
    tmpRegStrg = tmpRegStrg & ",General,Area_All,Area All,B,1"
    
    tmpRegStrg = tmpRegStrg & ",Screen 3,Telex,Telex,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Print,Print,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Insert,Insert,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Delete,Delete,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Send_Telex,Send Telex,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Excel,Excel,B,1"
    tmpRegStrg = tmpRegStrg & ",Screen 3,Combo_Decision,Combo Decision,C,1"
    
    tmpRegStrg = tmpRegStrg & ",Compressed Flights,Reorganize,Reorganize,B,1"
    tmpRegStrg = tmpRegStrg & ",Compressed Flights,Shrink_to_critical,Shrink to critical,B,1"
    
    'Append any other Privileges
    'tmpRegStrg = tmpRegStrg & ",Entry,Entry,Entry,F,1"
    'tmpRegStrg = tmpRegStrg & ",Feature,Feature,Feature,F,1"
    ULogin.RegisterApplicationString = tmpRegStrg
    ULogin.VersionString = GetApplVersion(True)
    ULogin.InfoCaption = "Info about HUB-Manager"
    ULogin.InfoButtonVisible = True
    ULogin.InfoUfisVersion = "UFIS Version 4.5"
    ULogin.InfoAppVersion = CStr("HUB-Manager " + GetApplVersion(True))
    ULogin.InfoCopyright = "� 2003-2005 UFIS Airport Solutions GmbH"
    ULogin.InfoAAT = "UFIS Airport Solutions GmbH"
    
    
    strRet = ULogin.ShowLoginDialog
    LoginAnsw = ULogin.GetPrivileges("InitModu")
    If LoginAnsw <> "" And strRet <> "CANCEL" Then LoginProcedure = True Else LoginProcedure = False
End Function

Public Function GetApplVersion(ShowExtended As Boolean) As String
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = ""
    Select Case SysInfo.OSPlatform
        Case 0  'Win32s
        Case 1  'Win 95/98
        Case 2  'Win NT
            If SysInfo.OSVersion > 4 Then
                'Win 2000
                If ShowExtended Then tmpRevis = "0." & tmpRevis
            Else
                'Win NT 4
                If ShowExtended Then
                    tmpMinor = Right("0000" & tmpMinor, 2)
                    tmpRevis = Right("0000" & tmpRevis, 4)
                End If
            End If
        Case Else
    End Select
    tmpTxt = tmpTxt & tmpMajor & "." & tmpMinor & "." & tmpRevis
    GetApplVersion = tmpTxt
End Function

Public Sub SetServerParameters()
    aUfis.HomeAirport = strHopo
    aUfis.TableExt = strTableExt
    aUfis.Module = "HUB-Manager" & "," & Me.GetApplVersion(True)
    aUfis.Twe = ""
    aUfis.Tws = "0"
    'aUfis.WorkStation = GetWorkStationName()
    aUfis.UserName = strCurrentUser
End Sub

'---------------------------------------------------------------
' inserts a new line into the data tab depending on the logical
' fieldlist.
' Return: The index of the inserted Line
'---------------------------------------------------------------
Public Function InsertEmptyLineForTab(Index As Integer) As Long
    Dim cnt As Long
    Dim i As Long
    Dim strVal As String
    
    cnt = ItemCount(tabData(Index).LogicalFieldList, ",")
    For i = 0 To cnt - 1
        strVal = strVal + ","
    Next i
    strVal = Left(strVal, Len(strVal) - 1)
    tabData(Index).InsertTextLine strVal, False
    InsertEmptyLineForTab = tabData(Index).GetLineCount - 1
End Function

