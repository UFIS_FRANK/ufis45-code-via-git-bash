VERSION 5.00
Begin VB.Form frmSendTelex 
   Caption         =   "Telex Content"
   ClientHeight    =   6015
   ClientLeft      =   11565
   ClientTop       =   8445
   ClientWidth     =   7425
   Icon            =   "frmSendTelex.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6015
   ScaleWidth      =   7425
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtSendTelex 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   0
      Width           =   3165
   End
End
Attribute VB_Name = "frmSendTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    Form_Resize
End Sub


Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - txtSendTelex.left * 2
    If NewSize > 15 Then txtSendTelex.Width = NewSize
    'NewSize = Me.ScaleHeight - StatusBar1.Height - 60
    If NewSize > 15 Then txtSendTelex.Height = NewSize
End Sub
